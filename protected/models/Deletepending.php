<?php

/**
 * This is the model class for table "{{deletepending}}".
 *
 * The followings are the available columns in table '{{deletepending}}':
 * @property integer $deletepending_id
 * @property string $deletepending_data
 * @property string $deletepending_table
 * @property integer $deletepending_parentid
 * @property integer $user_id
 * @property string $requested_date
 * @property integer $deletepending_status
 */
class Deletepending extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{deletepending}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('deletepending_data, deletepending_table, deletepending_parentid, user_id, requested_date', 'required'),
			array('deletepending_parentid, user_id, deletepending_status', 'numerical', 'integerOnly'=>true),
			array('deletepending_table', 'length', 'max'=>100),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('deletepending_id, deletepending_data, deletepending_table, deletepending_parentid, user_id, requested_date, deletepending_status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'deletepending_id' => 'Deletepending',
			'deletepending_data' => 'Deletepending Data',
			'deletepending_table' => 'Deletepending Table',
			'deletepending_parentid' => 'Deletepending Parentid',
			'user_id' => 'User',
			'requested_date' => 'Requested Date',
			'deletepending_status' => 'Deletepending Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('deletepending_id',$this->deletepending_id);
		$criteria->compare('deletepending_data',$this->deletepending_data,true);
		$criteria->compare('deletepending_table',$this->deletepending_table,true);
		$criteria->compare('deletepending_parentid',$this->deletepending_parentid);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('requested_date',$this->requested_date,true);
		$criteria->compare('deletepending_status',$this->deletepending_status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>false,
		));
	}
        public function pendingsearch()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('deletepending_id',$this->deletepending_id);
		$criteria->compare('deletepending_data',$this->deletepending_data,true);
		$criteria->compare('deletepending_table',$this->deletepending_table,true);
		$criteria->compare('deletepending_parentid',$this->deletepending_parentid);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('requested_date',$this->requested_date,true);
		$criteria->compare('deletepending_status',0);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array('pageSize'=>20),
		));
	}
        public function actionsearch()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('deletepending_id',$this->deletepending_id);
		$criteria->compare('deletepending_data',$this->deletepending_data,true);
		$criteria->compare('deletepending_table',$this->deletepending_table,true);
		$criteria->compare('deletepending_parentid',$this->deletepending_parentid);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('requested_date',$this->requested_date,true);
		//$criteria->compare('deletepending_status',0);
                $criteria->condition = "deletepending_status != 0";

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>false,    
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Deletepending the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
