<?php
/* @var $this DailyWorkTypeController */
/* @var $data DailyWorkType */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('wtid')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->wtid), array('view', 'id'=>$data->wtid)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('work_type')); ?>:</b>
	<?php echo CHtml::encode($data->work_type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('company_id')); ?>:</b>
	<?php echo CHtml::encode($data->company_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('no_of_labour_label')); ?>:</b>
	<?php echo CHtml::encode($data->no_of_labour_label); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('wage_label')); ?>:</b>
	<?php echo CHtml::encode($data->wage_label); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('wage_rate_label')); ?>:</b>
	<?php echo CHtml::encode($data->wage_rate_label); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('helper_label')); ?>:</b>
	<?php echo CHtml::encode($data->helper_label); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('helper_labour_label')); ?>:</b>
	<?php echo CHtml::encode($data->helper_labour_label); ?>
	<br />

	*/ ?>

</div>