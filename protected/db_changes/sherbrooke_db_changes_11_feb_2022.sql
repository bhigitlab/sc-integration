-- disable foreign key checks

SET FOREIGN_KEY_CHECKS=0;
TRUNCATE TABLE jp_additional_bill;
TRUNCATE TABLE jp_billitem;
TRUNCATE TABLE jp_pre_expenses;
TRUNCATE TABLE jp_expenses;
TRUNCATE TABLE jp_purchase_return;
TRUNCATE TABLE jp_purchase_returnitem;
TRUNCATE TABLE jp_reconciliation;
TRUNCATE TABLE jp_purchase_items;
TRUNCATE TABLE jp_purchase;
TRUNCATE TABLE jp_itemestimation;
TRUNCATE TABLE jp_bills;
TRUNCATE TABLE jp_warehouse_transfer_deleted_log;
TRUNCATE TABLE jp_warehousedespatch;
TRUNCATE TABLE jp_warehousedespatch_items;
TRUNCATE TABLE jp_warehousereceipt;
TRUNCATE TABLE jp_warehousereceipt_items;
TRUNCATE TABLE jp_warehousestock;