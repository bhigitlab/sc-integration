<?php
/* @var $this ExpensesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'Albums',
);
?>
<div class="container" id="project">
    <h2>Albums</h2>

    <?php  $this->renderPartial('_search', array('model' => $model)) ?>

    <div class="add-btn">
        <?php
        
        if (Yii::app()->user->role == 1) { ?>
            <button data-toggle="modal" data-target="#addAlbum"  class="createAlbum">Add Album</button>
        <?php } ?>	
    </div>

    <div class="exp-list">
        <?php
        $this->widget('zii.widgets.CListView', array(
            'dataProvider' => $dataProvider,
            'itemView' => '_view',
        ));
        ?>

        <!-- Add Project Popup -->
        <div id="addAlbum" class="modal fade" role="dialog">
            <div class="modal-dialog modal-lg">

            </div>
        </div>


        <!-- Edit Project Popup -->

        <div class="modal fade edit" role="dialog">
            <div class="modal-dialog modal-lg">


            </div>
        </div>

        <script>
            $(document).ready(function () {
                $('.createAlbum').click(function () {
                    // alert('hi');
                    $.ajax({
                        type: "GET",
                        url: "<?php echo $this->createUrl('albums/create') ?>",
                        success: function (response)
                        {
                            $("#addAlbum").html(response);

                        }
                    });
                });
                $(document).delegate('.editAlbum','click', function() {
                //$('.editAlbum').click(function () {
                    // alert("hai");
                    var id = $(this).attr('data-id');
                    //   alert(id);

                    $.ajax({
                        type: "GET",
                        url: "<?php echo $this->createUrl('albums/update&id=') ?>" + id,
                        success: function (response)
                        {
                            $(".edit").html(response);

                        }
                    });

                });

                jQuery(function ($) {
                    $('#album').on('keydown', function (event) {
                        if (event.keyCode == 13) {
                            $("#albumsearch").submit();

                        }


                    });
                });

            });
        </script>		



    </div>
</div>
