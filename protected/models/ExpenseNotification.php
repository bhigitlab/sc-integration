<?php

/**
 * This is the model class for table "{{expense_notification}}".
 *
 * The followings are the available columns in table '{{expense_notification}}':
 * @property integer $id
 * @property integer $project_id
 * @property integer $expense_perc
 * @property double $expense_amount
 * @property double $advance_amount
 * @property string $generated_date
 * @property integer $view_status
 *
 * The followings are the available model relations:
 * @property Projects $project
 */
class ExpenseNotification extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{expense_notification}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('project_id, expense_perc, view_status', 'numerical', 'integerOnly' => true),
			array('project_id,expense_amount, advance_amount', 'required'),
			array('expense_amount, advance_amount', 'numerical'),
			array('generated_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, project_id, expense_perc, expense_amount, advance_amount, generated_date, view_status', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'project' => array(self::BELONGS_TO, 'Projects', 'project_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'project_id' => 'Project',
			'expense_perc' => 'Expense Perc',
			'expense_amount' => 'Expense Amount',
			'advance_amount' => 'Advance Amount',
			'generated_date' => 'Generated Date',
			'view_status' => 'View Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('project_id', $this->project_id);
		$criteria->compare('expense_perc', $this->expense_perc);
		$criteria->compare('expense_amount', $this->expense_amount);
		$criteria->compare('advance_amount', $this->advance_amount);
		$criteria->compare('generated_date', $this->generated_date, true);
		$criteria->compare('view_status', $this->view_status);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'sort' => array(
				'defaultOrder' => 'view_status asc',
			),
		));
	}
	public function notification_search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('project_id', $this->project_id);
		$criteria->compare('expense_perc', $this->expense_perc);
		$criteria->compare('expense_amount', $this->expense_amount);
		$criteria->compare('advance_amount', $this->advance_amount);
		$criteria->compare('generated_date', $this->generated_date, true);
		$criteria->compare('view_status', $this->view_status);
		$criteria->addCondition('view_status <= 2');
		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'pagination' => array('pageSize' => 5,),
			'sort' => array(
				'defaultOrder' => 'view_status asc',
			),
		));
	}


	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ExpenseNotification the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}
}
