<?php
/* @var $this SubcontractorController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Subcontractor',
);
$page = Yii::app()->request->getParam('Subcontractor_page');
if($page != '') {
	Yii::app()->user->setReturnUrl($page);
} else {
	Yii::app()->user->setReturnUrl(0);
}
?>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<style>
 .page-body h3{
    margin:4px 0px;
    color:inherit;
    text-align: left;
}
table.dataTable tbody td {
  padding: 12px 10px;
}
.tooltip-hiden{width:auto;}
.panel{border:1px solid #ddd;}
.panel-heading{background-color:#eee;height:40px;}

</style>
<div class="container" id="vendors">
    <div class="clearfix">
    <div class="add-btn pull-right">
        <?php if((isset(Yii::app()->user->role) && (in_array('/subcontractor/subcontractorcreate', Yii::app()->user->menuauthlist)))){ ?>
<!--                <button data-toggle="modal" data-target="#addSubcontractor"  class="createSubcontractor">Add Sub Contractor</button>-->
                    <button   class="create_contract btn btn-primary">Add Sub Contractor</button>
        <?php } ?>
    </div>
        <h2>Sub Contractors</h2>
    </div>
    <div id='subcontform' style='display:none;'></div>
    
    
</div>

<script type="text/javascript">
$(document).ready(function () {
        
});
</script>
