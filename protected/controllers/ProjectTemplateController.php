<?php
class ProjectTemplateController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
          
        $accessArr = array();
           $accessauthArr = array();
           $controller = Yii::app()->controller->id;
           if(isset(Yii::app()->user->menuall)){
               if (array_key_exists($controller, Yii::app()->user->menuall)) {
                   $accessArr = Yii::app()->user->menuall[$controller];
               } 
           }

           if(isset(Yii::app()->user->menuauth)){
               if (array_key_exists($controller, Yii::app()->user->menuauth)) {
                   $accessauthArr = Yii::app()->user->menuauth[$controller];
               }
           }
           $access_privlg = count($accessauthArr);
           return array(
               array('allow', // allow all users to perform 'index' and 'view' actions
                   'actions' => $accessArr,
                   'users' => array('@'),
                   'expression' => "$access_privlg > 0",
               ),

               array('allow', // allow admin user to perform 'admin' and 'delete' actions
                   'actions' => $accessauthArr,
                   'users' => array('@'),
                   'expression' => "$access_privlg > 0",
               ),
               array(
                'allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('switchTemplate', 'preview','getGalleryMedia'),
                'users' => array('@')
            ),
               array('deny',  // deny all users
                   'users'=>array('*'),
               ),
           );
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
		
		 if(isset($_GET['layout']))
                $this->layout = false;
        $model = new ProjectTemplate;
        $model2 = new ProjectTemplateRevision();
        $error_flag = 0;
        $tblpx = Yii::app()->db->tablePrefix;
        $this->performAjaxValidation($model);
	
        if (isset($_POST['ProjectTemplate'])) { 
			$model->attributes = $_POST['ProjectTemplate'];
            $str = $_POST['template_format'];
            $clean_content = $this->cleanHtmlContent($str);
            $encoded_data =  htmlentities($clean_content);
            $model->template_format = $encoded_data;
            $dom = new DOMDocument();
            libxml_use_internal_errors(true);
            $dom->loadHTML($_POST['template_format']);
            $errors = libxml_get_errors();
        
            if ($errors) {
                Yii::app()->user->setFlash('error', "Invalid template format. Please check!");
                $this->redirect(array('index'));
            } else {
                $this->performAjaxValidation($model);
                if ($model->save()) {
                    $model2->template_version = $encoded_data;
                    $model2->template_id = $model->template_id;
                    if ($model2->save()) {
                        Yii::app()->user->setFlash('error', "Template updated successfully!");
                        $this->redirect(array('index'));
                    }
                }
            }
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */

    public function actionUpdate($id,$type='quotation') {

	$tblpx = Yii::app()->db->tablePrefix;	
		 if(isset($_GET['layout']))
                $this->layout = false;
        $model = $this->loadModel($id);
        $model2 = new ProjectTemplateRevision();
        
        //$count_revisions =ProjectTemplateRevision::model()->findByAttributes(array('template_id' => $id));	
       
        $sql = "SELECT count(*)  "
                . " FROM {$tblpx}project_template_revision "
                . " WHERE template_id=" . $id;
        $count_revisions = Yii::app()->db->createCommand($sql)->queryScalar();
        
        
        if (isset($_POST['ProjectTemplate'])) {
            $model->attributes = $_POST['ProjectTemplate'];
            $str = $_POST['template_format'];
            $clean_content = $this->cleanHtmlContent($str);
            $encoded_data =  htmlentities($clean_content);
            $model->template_format = $encoded_data;
            $dom = new DOMDocument();
            libxml_use_internal_errors(true);
            $dom->loadHTML($_POST['template_format']);
            $errors = libxml_get_errors();
        
            if ($errors) {//print_r($errors);die();
                Yii::app()->user->setFlash('error', "Invalid template format. Please check!");
                $this->redirect(array('index'));
            } else {
                $this->performAjaxValidation($model);
                if ($model->save()) {
                    $model2->template_version = $encoded_data;
                    $model2->template_id = $model->template_id;
                    if ($model2->save()) {
                        Yii::app()->user->setFlash('error', "Template updated successfully!");
                        $this->redirect(array('index'));
                    }
                }
            }   
        }
        //$model->template_format = html_entity_decode($model->template_format);
        if($type =='invoice'){ 
            $this->render('invoice_update', array(
                'model' => $model,
                'count_revisions' => $count_revisions
            ));
        }
        else if($type =='purchase'){
            $this->render('purchase_update', array(
                'model' => $model,
                'count_revisions' => $count_revisions
            ));
        }
        else{
            $this->render('update', array(
                'model' => $model,
                'count_revisions' => $count_revisions
            ));
        }
    }
    public function actionSaveTemplateData($id,$type='quotation'){
        $tblpx = Yii::app()->db->tablePrefix;
        $model = ($id !='')?$this->loadModel($id):new ProjectTemplate();
        $model2 = new ProjectTemplateRevision();
        $response = array();
        $response['success'] = false;
        $response['errors'] = "No data";
        if (Yii::app()->request->isAjaxRequest && isset($_POST['ProjectTemplate'])) {
            $model->attributes =$_POST['ProjectTemplate']; 
            if($type=='quotation'){
                $str = $_POST['ProjectTemplate']['template_format'];
                $clean_content = $this->cleanHtmlContent($str);
                $encoded_data =  htmlentities($clean_content);
                $model->template_format = $encoded_data;
                $dom = new DOMDocument();
                libxml_use_internal_errors(true);
                $dom->loadHTML($_POST['ProjectTemplate']['template_format']);
                $errors = libxml_get_errors();
            
                if ($errors) {
                    Yii::app()->user->setFlash('error', "Invalid template format. Please check!");
                    $response['success'] = false;
                    $response['errors'] = "Invalid template format. Please check!";
                } else {
                    $this->performAjaxValidation($model);
                    if ($model->save()) {
                        $model2->template_version = $encoded_data;
                        $model2->template_id = $model->template_id;
                        if ($model2->save()) {
                            Yii::app()->user->setFlash('success', "Template updated successfully!");
                            $response['success'] = true;
                        }
                    }
                }
            }else if($type =='invoice'){

                $str = $_POST['ProjectTemplate']['template_format_invoice'];
                $clean_content = $this->cleanHtmlContent($str);
                $encoded_data =  htmlentities($clean_content);
                $model->template_format_invoice = $encoded_data;
                $company_error =0;
                if(isset($_POST['ProjectTemplate']['company_id']) && $_POST['ProjectTemplate']['company_id']!=''){
                    $model->company_id =(isset($_POST['ProjectTemplate']['company_id']))?$_POST['ProjectTemplate']['company_id']:'';
                }else{
                   $company_error=1; 
                }
                $dom = new DOMDocument();
                libxml_use_internal_errors(true);
                if(isset($_POST['ProjectTemplate']['template_format_invoice']) && $_POST['ProjectTemplate']['template_format_invoice']!=''){
                    $dom->loadHTML($_POST['ProjectTemplate']['template_format_invoice']);
                    $errors = libxml_get_errors();
                }else{
                    $errors = 1;  
                }
            
                if ($errors || $company_error) {//print_r($errors);die();
                    $response['success'] = false;
                    $response['errors'] = "Invalid template format. Please check!";
                    if($company_error){
                        $response['errors'] = "Please choose company";
                        Yii::app()->user->setFlash('error', "Please choose company");                    
                    }else{
                        Yii::app()->user->setFlash('error', "Invalid template format. Please check!");                    
                    }
                } else {
                    $ee = $this->performAjaxValidation($model);
                    if ($model->save()) {
                        $model2->invoice_template_version = $encoded_data;
                        $model2->template_id = $model->template_id;
                        if ($model2->save()) {
                             Yii::app()->user->setFlash('success', "Template updated successfully!");
                            $response['success'] = true;
                        }else{
                            $response['success'] = false;
                            $response['errors'] = $model2->getErrors();
                        }
                    }
                }
            }else if ($type == 'purchase') {
                $str = $_POST['ProjectTemplate']['template_format_purchase'];
                $clean_content = $this->cleanHtmlContent($str);
                $encoded_data = htmlentities($clean_content);
                $model->template_format_purchase = $encoded_data;
                $company_error = 0;

                if (isset($_POST['ProjectTemplate']['company_id']) && $_POST['ProjectTemplate']['company_id'] != '') {
                    $model->company_id = $_POST['ProjectTemplate']['company_id'];
                } else {
                    $company_error = 1;
                }

                $dom = new DOMDocument();
                libxml_use_internal_errors(true);
                if (isset($_POST['ProjectTemplate']['template_format_purchase']) && $_POST['ProjectTemplate']['template_format_purchase'] != '') {
                    $dom->loadHTML($_POST['ProjectTemplate']['template_format_purchase']);
                    $errors = libxml_get_errors();
                } else {
                    $errors = 1;
                }

                if ($errors || $company_error) {
                    $response['success'] = false;
                    if ($company_error) {
                        $response['errors'] = "Please choose company";
                        Yii::app()->user->setFlash('error', "Please choose company");
                    } else {
                        $response['errors'] = "Invalid template format. Please check!";
                        Yii::app()->user->setFlash('error', "Invalid template format. Please check!");
                    }
                } else {
                    $this->performAjaxValidation($model);
                    if ($model->save()) {
                        $model2->purchase_template_version = $encoded_data;
                        $model2->template_id = $model->template_id;
                        if ($model2->save()) {
                            Yii::app()->user->setFlash('success', "Template updated successfully!");
                            $response['success'] = true;
                        } else {
                            
                            $response['success'] = false;
                            $response['errors'] = $model2->getErrors();
                        }
                    }
                }
            }
            } 
        echo json_encode($response);
        exit;
    }
    
    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
//    public function actionDelete($id) {
//        $this->loadModel($id)->delete();
//
//        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
//        if (!isset($_GET['ajax']))
//            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
//    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $model = new ProjectTemplate('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['ProjectTemplate']))
            $model->attributes = $_GET['ProjectTemplate'];
           
            $this->render('index', array(
                'model' => $model,
                'dataProvider' => $model->search(),
            ));
    }

    public function actionPreview($type='quotation') {
        
        if(isset($_POST['template_data'])){
            $template_data = $_POST['template_data'];
            $sample_template_data = $_POST['sample_template_data'];
            $json_array = json_decode($sample_template_data, true);
            //echo "<pre>test";print_r($sample_template_data);
            $material_specification = "";
            $quotation_galley_images = "";
            if(isset($json_array["material_specification"])){
                foreach ($json_array["material_specification"] as $value) {
                    $material_specification .= '<tr id="material_specification">                              
                    <td class="text_title">
                    '.$value["brand"].' - '.$value["specification"].'
                    </td>                             
                </tr>';
                }
            }

            if(isset($json_array["gallery_images"])) {
      
                $quotation_galley_images = "<table class='item_table'>
                    <tbody> 
                        <tr>
                            <th colspan='2' class='title'>KITCHEN ACCESSORIES</th>
                        </tr>
                        <tr>                
                            <th>Images</th>
                            <th>Name</th>
                        </tr>";                
                       
                        $i=1;
                        foreach ($json_array["gallery_images"] as $value) {
                            $quotation_galley_images .= "<tr>
                            
                            <td>";
                         
                                if ($value['image'] != '') {
                                    $path = $json_array['base_url'] . $value["image"];
                                   // if (file_exists($path)) {
                                        $quotation_galley_images .=  CHtml::tag('img',
                                            array(
                                                'src' => $json_array['base_url'] . $value["image"],
                                                'alt' => '',
                                                'class' => 'pop',
                                                'modal-src' => $json_array['base_url'] .$value["image"],
                                                'width'=>'100px'
                                            )
                                        );
                                    //}
                                } 
                                
                           $quotation_galley_images .="</td>
                            <td>".$value['caption']."</td>
                        </tr>";
                        $i++; } 
                $quotation_galley_images .="</tbody>               
                </table>";
                 } 
                 if($json_array['gst_percentage'] >0) {
                    $total_tax_percent_value = "(".$json_array['gst_percentage']."%)";;
                }else{
                    $total_tax_percent_value = "";
                }


            $item_list = "";
        if($type=='quotation'){      
            $i = 1;
            $secletters = range('A', 'Z');
            $se = 0;
            $subsectionArraySpan = '7';
            $total_price = 0;
            $offer_price = 0;
            foreach ($json_array["item_list"] as $value) {
               
                $item_list .= '<tr>
                       ';
                 $item_list .= '<th colspan="'.$subsectionArraySpan.'" style="background:#c00000;color:#fff;">'.$value["main_section"].'</th>';
                                           
                 $item_list .= '</tr>';
                 $subitemArraySpan = '6';
               
                    $item_list .= '<tr>
                        <th  style="background:#c5d9f1;">'.$i.'</th>';
                        $item_list .= '<th colspan="'.$subitemArraySpan.'" style="background:#c5d9f1;">'.$value['sub_section'].'</th>';
                         
                    $letters = range('a', 'z');
                    $k = 0;
                    $wrktypeitemArray_span = '6';
                   
                        $item_list .= '<tr class="td_group">';
                        $item_list .= '<th>'.$letters[$k].'</th>';
                        $item_list .= '<td colspan="'.$wrktypeitemArray_span.'"><b>'.$value['worktype_label'].'</b></td>';
                           
                        $sum = 0;
                        foreach ($value["item"] as $value4) {
                            
                            
                            $item_list .= '<tr class="td_group">';
                            $item_list .= '<th>'.'*'.'</th>';
                           
                                
                                $item_list .='<td>'.$value4['description'].'</td>';
                                $item_list .='<td>'.$value4['material'].'</td>';
                                $item_list .='<td>'.$value4['unit'].'</td>';
                                
                                $item_list .='<td>'.$value4['quantity'].'</td>';
                                $item_list .='<td class="text-right">'.Controller::money_format_inr($value4['mrp_amount'], 2).'</td>';
                                $item_list .='<td class="text-right">'.Controller::money_format_inr($value4['amount_after_discount'], 2).'</td>';
                                
                                $item_list .='</tr>'; 
                                $total_price += $value4['mrp_amount'];
                                $offer_price += $value4['amount_after_discount'];

                        }  
                        if (!empty($value1["item"])) {
                            
                        } $k++;
                   
                    $i++;
              
               $se++; 
            
            } 
            $item_list_type7 = "";
        }else if($type=='invoice'){//echo "<pre>test";print_r($json_array);die;
                 
            $i = 0;
            foreach ($json_array["item_list"] as $value) {
                $item_list .= '<tr  class="tr_class">';
                $item_list .= '<td>'.($i+1).'</td>';
                $item_list .= '<td>'.$value['description'].'</td>';
                $item_list .= '<td>'.$value['hsn_code'].'</td>';
                $item_list .= '<td>'.number_format($value['sgst'],2).'</td>';
                $item_list .= '<td>'.Controller::money_format_inr($value['sgst_amount'],2,1).'</td>';
                $item_list .= '<td>'.number_format($value['cgst'],2).'</td>';
                $item_list .= '<td>'.Controller::money_format_inr($value['cgst_amount'],2,1).'</td>';
                $item_list .= '<td>'.number_format($value['igst'],2).'</td>';
                $item_list .= '<td>'.Controller::money_format_inr($value['igst_amount'],2,1).'</td>';
                $item_list .= '<td>'.Controller::money_format_inr($value['amount'],2,1).'</td>';
                $item_list .= '<td>'.Controller::money_format_inr($value['tax_amount'],2,1).'</td>';
                $item_list .= '</tr>';
                $i++;
            }
        }
            $modifiedTemplate = preg_replace('/\[\[ITEMTEMPLATE(.*?)ENDITEMTEMPLATE\]\]/s', '', $template_data);


            $result = '';
            $pattern = '/\[\[ITEMTEMPLATE(.*?)ENDITEMTEMPLATE\]\]/s';
            if (preg_match($pattern, $template_data, $matches)) {
                $extractedContent = $matches[1];
            } else {
                $extractedContent = '';
            }

        $grouppattern = '/<td>(\{\w+\})<\/td> <td>(\{\w+\})<\/td>/';   
        $replacements = array();

            $i = 1;
            $secletters = range('A', 'Z');
            $se = 0;
            $subsectionArraySpan = '8';
            $total_price = 0;
            $offer_price = 0;
            if($type=='quotation'){
            foreach ($json_array["item_list"] as $value) {                                                                                           
                       
                        $replacements = array(
                            '{sino}' => $value['main_section'],
                            '{item}' => '',
                            '{length}' => '',
                            '{width}' => '',
                            '{unit}' => '',
                            '{quantity}' => '',
                            '{mrp}' => '',
                            '{discount}' => '',
                            '{total}' => '',
                            '{section-id}' => 'item-section',
    
                        );
                        $html_string = $this->replaceItemlistWithCondition($extractedContent, $replacements);
                        $itemRow = str_replace(array_keys($replacements), array_values($replacements), $html_string);
                        $item_list_type7 .= $itemRow;
                        $replacements = array(
                            '{sino}' => $value['sub_section'],
                            '{item}' => '',
                            '{length}' => '',
                            '{width}' => '',
                            '{unit}' => '',
                            '{quantity}' => '',
                            '{mrp}' => '',
                            '{discount}' => '',
                            '{total}' => '',
                            '{section-id}' => 'item-category',
    
                        );
                        
                        $html_string = $this->replaceItemlistWithCondition($extractedContent, $replacements);
                        $itemRow = str_replace(array_keys($replacements), array_values($replacements), $html_string);
                        $item_list_type7 .= $itemRow;
                    $letters = range('a', 'z');
                    $k = 0;
                    $wrktypeitemArray_span = '7';
                   

                        $replacements = array(
                            '{sino}' => $letters[$k],
                            '{item}' => $value['worktype_label'],
                            '{length}' => '',
                            '{width}' => '',
                            '{unit}' => '',
                            '{quantity}' => '',
                            '{mrp}' => '',
                            '{discount}' => '',
                            '{total}' => '',
                            
        
                        );
                        $html_string = $this->replaceItemlistWithCondition($extractedContent, $replacements);
                        $itemRow = str_replace(array_keys($replacements), array_values($replacements), $html_string);
                        $item_list_type7 .= $itemRow;

                        $sum = 0;
                        $item_count=0;
                        foreach ($value["item"] as $value4) {
                            $item_count++;
                            
                                $total_price += $value4['mrp_amount'];
                                $offer_price += $value4['amount_after_discount'];

                                $replacements = array(
                                    '{sino}' => '*',
                                    '{item}' => $value4['description'],
                                    '{length}' => $value4['length'],
                                    '{width}' => $value4['width'],
                                    '{unit}' => $value4['unit'],
                                    '{quantity}' => $value4['quantity'],
                                    '{mrp}' => $value4['mrp_amount'],
                                    '{discount}' => $value4['amount_after_discount'],
                                    '{total}' => '',
                                    
                
                                );
                                $html_string = $this->replaceItemlistWithCondition($extractedContent, $replacements);
                                $itemRow = str_replace(array_keys($replacements), array_values($replacements), $html_string);
                                $item_list_type7 .= $itemRow;

                        }  
                        
                        if (!empty($value1["item"])) {
                            
                        } 
                        $k++;
                  
                    $i++;
                
               $se++; 
            
            } 
            }else if($type=='invoice'){
                $replacements = array();
$resultItem='';
if(!empty($json_array["item_list"])){
    foreach($json_array["item_list"] as $i => $value){
        $replacements = array(
            '{sino}' => $i+1,
            '{description}' => $value['description'],
            '{hsn_code}' => $value['hsn_code'],
            '{sgst}' => $value['sgst'],
            '{cgst}' => $value['cgst'],
            '{igst}' => $value['igst'],
            '{sgst_amount}' => $value['sgst_amount'],
            '{cgst_amount}' => $value['cgst_amount'],
            '{igst_amount}' => $value['igst_amount'],
            '{amount}' => $value['amount'],
            '{tax_amount}' => $value['tax_amount'],
            '{amount_with_tax}' => $value['tax_amount'],
        );
        $html_string = $this->replaceItemlistWithCondition($extractedContent, $replacements);
                    $itemRow = str_replace(array_keys($replacements), array_values($replacements), $html_string);
                    $resultItem .= $itemRow;  
    }
}
            }
            $total_amoun_withtax = 0;
            $total_amoun_withtax = $offer_price + $json_array['gst_amount'];
            $discount_amount = $total_price - $offer_price;
            $d2r_special_discount =$total_amoun_withtax -($offer_price*.09);
            $d2r_special_discount_inr = Controller::money_format_inr($d2r_special_discount , 2); 
            $pattern = '/{image::\s*([^|]+)\|\|width:\'(\d+px;)\'}/';

            $path = Yii::app()->request->baseUrl . '/uploads/company_assets/media/';

            $modifiedPdfData = preg_replace_callback($pattern, function($matches) use ($path) {
                $image_name = $matches[1];
                $width_value = $matches[2];

                $replacement_image = "<img src='" . $path . "$image_name' alt='$image_name' class='dynamic_media_image' style='width:$width_value;'>";
                return $replacement_image;
            }, $modifiedTemplate);

            if($type=='quotation'){
            $template_data = str_replace(
                array('{base_url}','{terms_and_conditions}','{location_name}', '{client}', 
                '{phone_no}', '{email}','{invoice_no}', '{date_quotation}', 
                '{company_name}', '{company_address}' ,'{company_phone}','{company_email}',
                '{sales_executive_name}', '{sales_executive_phone}'
                , '{sales_executive_designation}','{estimator_name}','{estimator_phone}',
                '{material_specification}', '{quotation_galley_images}', 
                '{total_tax_percent_value}', '{total_tax_inr}', 
                '{item_list}', '{total_price_inr}', '{offer_price_inr}', '{total_amoun_withtax_inr}'
                , '{address}', '{material_discount_inr}', '{item_list_type7}','{discount_percent}', '{item_list_type7_d2r}', '{d2r_special_discount_inr}'
               ),
                array($json_array['base_url'], $json_array['terms_and_conditions'], $json_array['location_name'], $json_array['client'],
                $json_array['phone_no'], $json_array['email'], $json_array['invoice_no'], $json_array['date_quotation'],
                $json_array['company_name'], $json_array['company_address'], $json_array['company_phone'], $json_array['company_email'],
                $json_array['sales_executive_name'], $json_array['sales_executive_phone'], $json_array['sales_executive_designation'], $json_array['estimator_name'],
                $json_array['estimator_phone'], $material_specification, $quotation_galley_images,
                $total_tax_percent_value ,Controller::money_format_inr($json_array['gst_amount'] , 2),
                $item_list, Controller::money_format_inr($total_price, 2), Controller::money_format_inr($offer_price, 2), Controller::money_format_inr($total_amoun_withtax, 2),
                $json_array['address'], Controller::money_format_inr($discount_amount, 2), $item_list_type7, $json_array['discount_percent'], $item_list_type7,  $d2r_special_discount_inr, 
            ), $modifiedPdfData);
        }else if($type=='purchase'){
            $template_data = str_replace(
                array('{base_url}','{terms_and_conditions}','{location_name}', '{client}', 
                '{phone_no}', '{email}','{invoice_no}', '{date_quotation}', 
                '{company_name}', '{company_address}' ,'{company_phone}','{company_email}',
                '{sales_executive_name}', '{sales_executive_phone}'
                , '{sales_executive_designation}','{estimator_name}','{estimator_phone}',
                '{material_specification}', '{quotation_galley_images}', 
                '{total_tax_percent_value}', '{total_tax_inr}', 
                '{item_list}', '{total_price_inr}', '{offer_price_inr}', '{total_amoun_withtax_inr}'
                , '{address}', '{material_discount_inr}', '{item_list_type7}','{discount_percent}', '{item_list_type7_d2r}', '{d2r_special_discount_inr}'
               ),
                array($json_array['base_url'], $json_array['terms_and_conditions'], $json_array['location_name'], $json_array['client'],
                $json_array['phone_no'], $json_array['email'], $json_array['invoice_no'], $json_array['date_quotation'],
                $json_array['company_address'], $json_array['company_phone'], $json_array['company_email'],
                $json_array['sales_executive_name'], $json_array['sales_executive_phone'], $json_array['sales_executive_designation'], $json_array['estimator_name'],
                $json_array['estimator_phone'], $material_specification, $quotation_galley_images,
                $total_tax_percent_value ,Controller::money_format_inr($json_array['gst_amount'] , 2),
                $item_list, Controller::money_format_inr($total_price, 2), Controller::money_format_inr($offer_price, 2), Controller::money_format_inr($total_amoun_withtax, 2),
                $json_array['address'], Controller::money_format_inr($discount_amount, 2), $json_array['discount_percent'],  $d2r_special_discount_inr, 
            ), $modifiedPdfData);
        }
        else{
            $balance_in_words = Yii::app()->Controller->getDisplaywords($json_array['amount']);
            $tax_in_words = Yii::app()->Controller->getDisplaywords($json_array['tax_amount']);

            $template_data = str_replace(
                array('{base_url}','{terms_and_conditions}','{item_list}','{dynamic_item_list}','{company}','{project}','{client}','{date}','{invoice_number}','{payment_stage}','{amount}','{total_amount}','{total_tax}','{round_off}','{grand_total}','{write_off}','{balance}','{app_name}','{company_email}','{company_address}','{company_gst}','{balance_in_words}','{tax_in_words}'
               ),
                array($json_array['base_url'], $json_array['terms_and_conditions'],$item_list,$resultItem, $json_array['company'], $json_array['project'], $json_array['client'], $json_array['date'], $json_array['invoice_number'], $json_array['payment_stage'], $json_array['amount'], $json_array['total_amount'], $json_array['total_tax'], $json_array['round_off'], $json_array['grand_total'], $json_array['write_off'], $json_array['balance'], $json_array['app_name'], $json_array['company_email'], $json_array['company_address'], $json_array['company_gst'],$balance_in_words,$tax_in_words
            ), $modifiedPdfData);
            
            }
            echo $template_data;
            //echo html_entity_decode($template_data);
        } else{
            return;
        }
    }
    public function replaceItemlistWithCondition($extractedContent, $replacements) {
        $grouppattern = '/\[\((.*?)\)\]/';
    
        preg_match_all($grouppattern, $extractedContent, $groupmatches);
    
        foreach ($groupmatches[0] as $match) {
          //  $replaceString = $match;
          $input = str_replace(array("[(", ")]"), '', $match); 
    
            // Check if "length," "width," and "size" are all present
            $variables = preg_match_all('/\{([^}]+)\}/', $match, $matches);
            
           // if (count(array_intersect($requiredVariables, $matches[0])) == count($requiredVariables)) {
            $i=0;
                foreach ($matches[0] as $variable) { 
                    if (!empty($replacements[$variable])) {$i++;
                        $replaceString = str_replace($variable, $replacements[$variable], $input);
                       
                    } else {
                        // If any variable is missing a value, replace it with an empty string
                        $replaceString = str_replace($variable, '', $match);
                    }
                }
           if(count($matches[0]) != $i) {
                // Any required variable is missing, replace with an empty string
                $replaceString = '';
            }
  
            $extractedContent = str_replace($match, $replaceString, $extractedContent);
           
            //$extractedContent = preg_replace('/\[|\]|\(|\)|\s+/', '', $extractedContent);


           
        }//echo $extractedContent;die;
    
        return $extractedContent;
    }

    public function actionSwitchTemplate() {
        $id = $_POST['id'];
        $type = $_POST['type'];
        if($type == 2){

            $data = ProjectTemplateRevision::model()->find(array(
                'condition' => 'template_id = :id',
                'params' => array(':id' => $id),
                'order' => 'template_revision_id  DESC',
                'limit' => 1,
            ));
           
            
        } else{
            $data = ProjectTemplateRevision::model()->find(array(
                'condition' => 'template_id = :id',
                'params' => array(':id' => $id),
                'order' => 'template_revision_id  DESC',
                'offset' => 1,
                'limit' => 1,
            ));
        }
        echo html_entity_decode($data->template_version);
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new ProjectTemplate('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['ProjectTemplate']))
            $model->attributes = $_GET['ProjectTemplate'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model = ProjectTemplate::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'ProjectTemplate-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionGetGalleryMedia(){
        $basePathWithoutProtected = dirname(Yii::app()->basePath);
        $media_image_path = $basePathWithoutProtected . '/uploads/company_assets/media';
        $upload_location = $media_image_path;
        $imageFiles = array();

        // Check if the directory exists
        if (is_dir($upload_location)) {
            // Open the directory
            if ($dh = opendir($upload_location)) {
                // Loop through each file in the directory
                while (($file = readdir($dh)) !== false) {
                    // Skip . and .. directories and non-image files
                    if ($file !== "." && $file !== ".." && (strtolower(pathinfo($file, PATHINFO_EXTENSION)) === 'jpg' || strtolower(pathinfo($file, PATHINFO_EXTENSION)) === 'jpeg' || strtolower(pathinfo($file, PATHINFO_EXTENSION)) === 'png' || strtolower(pathinfo($file, PATHINFO_EXTENSION)) === 'gif')) {
                        // Add the file name to the array
                        $imageFiles[] = $file;
                    }
                }
                // Close the directory handle
                closedir($dh);
            }
        }
        
        // Convert the array to JSON format
        $imageJson = json_encode($imageFiles);
        
        // Output the JSON data
        echo $imageJson;
   }
}
