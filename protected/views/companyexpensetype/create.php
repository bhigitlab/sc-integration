<?php
/* @var $this CompanyExpenseTypeController */
/* @var $model CompanyExpenseType */

$this->breadcrumbs=array(
	'Company Expense Types'=>array('index'),
	'Create',
);
?>

<div class="panel panel-gray">
    <div class="panel-heading form-head">
        <h3 class="panel-title">Add Daily Expense Type</h3>
    </div>
        <?php echo $this->renderPartial('_form', array('model' => $model,'dailyexp'=>0)); ?>
</div>