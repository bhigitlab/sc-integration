<?php

/**
 * This is the model class for table "{{scquotation}}".
 *
 * The followings are the available columns in table '{{scquotation}}':
 * @property integer $scquotation_id
 * @property integer $project_id
 * @property integer $subcontractor_id
 * @property double $scquotation_amount
 * @property string $scquotation_decription
 * @property string $scquotation_date
 * @property integer $created_by
 * @property string $created_date
 * @property integer $updated_by
 * @property string $updated_date
 * @property string approve_status
 */
class Scquotation extends CActiveRecord
{

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{scquotation}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('project_id, subcontractor_id, scquotation_decription, scquotation_date, created_by, created_date,company_id,completion_date,expensehead_id,scquotation_no', 'required'),
            array('project_id, subcontractor_id, created_by, updated_by', 'numerical', 'integerOnly' => true),
            // array('scquotation_amount', 'numerical', 'integerOnly' => true, 'min' => 1),
            array('scquotation_decription', 'length', 'max' => 300),
            array('updated_date,expensehead_id,scquotation_no', 'safe'),
            ['scquotation_no', 'validateUniqueQuotationNo'],
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('scquotation_id, project_id, subcontractor_id, scquotation_amount, scquotation_decription, scquotation_date, created_by, created_date, updated_by, updated_date, company_id, approve_status,type,work_order_date', 'safe', 'on' => 'search'),
        );
    }
    public function validateUniqueQuotationNo($attribute, $params)
{
    // Check if the model is new (i.e., being created)
    if ($this->isNewRecord) {
        // Search for existing records with the same scquotation_no
        $existingRecord = self::model()->findByAttributes(['scquotation_no' => $this->$attribute]);

        // If a record is found, add an error
        if ($existingRecord !== null) {
            $this->addError($attribute, 'This Quotation Number already exists.');
        }
    }
}


    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'projects' => array(self::BELONGS_TO, 'Projects', 'project_id'),
            'subcontractor' => array(self::BELONGS_TO, 'Subcontractor', 'subcontractor_id'),
            'addedBy' => array(self::BELONGS_TO, 'Users', 'created_by'),
            'scQuotationItemCategories' => array(self::HAS_MANY, 'ScQuotationItemCategory', 'sc_quotaion_id'),
            'expensehead' => array(self::BELONGS_TO, 'ExpenseType', 'expensehead_id'),
            'scquotationPaymentEntries' => array(self::HAS_MANY, 'ScquotationPaymentEntries', 'scquotation_id'),
            'subcontractorPayments' => array(self::HAS_MANY, 'SubcontractorPayment', 'quotation_number'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'scquotation_id' => 'Quotation',
            'project_id' => 'Project',
            'subcontractor_id' => 'Sub Contractor',
            'scquotation_amount' => 'Amount',
            'scquotation_decription' => 'Description',
            'scquotation_date' => 'Created Date',
            'created_by' => 'Created By',
            'created_date' => 'Created Date',
            'updated_by' => 'Updated By',
            'updated_date' => 'Updated Date',
            'company_id' => 'Company',
            'approve_status' => 'Approve Status',
            'scquotation_no' => 'SC Quotation No',
            'expensehead_id' => 'Expense Head',
            'completion_date' => 'Completion Date',
            'work_order_date' => 'Work Order Date',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;
        $criteria->with = array('projects');

        $criteria->compare('t.scquotation_id', $this->scquotation_id);
        $criteria->compare('t.project_id', $this->project_id);
        $criteria->compare('t.subcontractor_id', $this->subcontractor_id);
        $criteria->compare('t.scquotation_no', $this->scquotation_no);
        $criteria->compare('t.scquotation_amount', $this->scquotation_amount);
        $criteria->compare('t.scquotation_decription', $this->scquotation_decription, true);
        $criteria->compare('t.scquotation_date', $this->scquotation_date, true);
        $criteria->compare('t.created_by', $this->created_by);
        $criteria->compare('t.updated_by', $this->updated_by);
        $criteria->compare('t.updated_date', $this->updated_date, true);
        $criteria->compare('t.approve_status', $this->approve_status, true);
        $criteria->compare('t.company_id', $this->company_id, true);  // Prefix 'company_id' with 't'
        $criteria->compare('projects.pid', $this->project_id, true);

        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);

        $newQuery = "";
        /*
        foreach ($arrVal as $arr) {
            if ($newQuery) $newQuery .= ' OR';
            $newQuery .= " FIND_IN_SET('" . $arr . "', t.company_id)";
        }
        $criteria->addCondition($newQuery);
        */
        
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            //'pagination' => false,
            'pagination' => array(
                'pageSize' => 10,  // Set the number of items per page
            ),
            'sort' => array(
                'defaultOrder' => 't.scquotation_id DESC',
                'attributes' => array(
                    'project' => array(
                        'asc' => 'projects.name',
                        'desc' => 'projects.name DESC',
                    ),
                    '*',
                ),
            ),
        ));
    }

    public function total_quotation_search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;
        $criteria->with = array('projects');

        $criteria->compare('t.scquotation_id', $this->scquotation_id);
        $criteria->compare('t.project_id', $this->project_id);
        $criteria->compare('t.subcontractor_id', $this->subcontractor_id);
        $criteria->compare('t.scquotation_amount', $this->scquotation_amount);
        $criteria->compare('t.scquotation_decription', $this->scquotation_decription, true);
        $criteria->compare('t.scquotation_date', $this->scquotation_date, true);
        $criteria->compare('t.created_by', $this->created_by);
        $criteria->compare('t.updated_by', $this->updated_by);
        $criteria->compare('t.updated_date', $this->updated_date, true);
        $criteria->compare('t.approve_status', $this->approve_status, true);
        $criteria->compare('t.company_id', $this->company_id, true);  // Prefix 'company_id' with 't'
        $criteria->compare('projects.pid', $this->project_id, true);

        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);

        $newQuery = "";
        /*
        foreach ($arrVal as $arr) {
            if ($newQuery) $newQuery .= ' OR';
            $newQuery .= " FIND_IN_SET('" . $arr . "', t.company_id)";
        }
        $criteria->addCondition($newQuery);
        */
        
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => false,
            
            'sort' => array(
                'defaultOrder' => 't.scquotation_id DESC',
                'attributes' => array(
                    'project' => array(
                        'asc' => 'projects.name',
                        'desc' => 'projects.name DESC',
                    ),
                    '*',
                ),
            ),
        ));
    }


    public function notification_search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;
        $criteria->with = array('projects');

        $criteria->compare('scquotation_id', $this->scquotation_id);
        $criteria->compare('project_id', $this->project_id);
        $criteria->compare('subcontractor_id', $this->subcontractor_id);
        $criteria->compare('scquotation_amount', $this->scquotation_amount);
        $criteria->compare('scquotation_decription', $this->scquotation_decription, true);
        $criteria->compare('scquotation_date', $this->scquotation_date, true);
        $criteria->compare('created_by', $this->created_by);
        // $criteria->compare('created_date',$this->created_date,true);
        $criteria->compare('updated_by', $this->updated_by);
        $criteria->compare('updated_date', $this->updated_date, true);
        // $criteria->compare('type11', $this->type);
        $criteria->compare('approve_status', $this->approve_status, true);
        $criteria->compare('t.company_id', $this->company_id, true);
        $criteria->compare('projects.pid', $this->project_id, true);
       
        $criteria->addCondition("added_viewed_notification IS NULL");
        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        $newQuery = "";
        /* foreach($arrVal as $arr) {
          if ($newQuery) $newQuery .= ' OR';
          $newQuery .= " FIND_IN_SET('".$arr."', t.company_id)";
          }
          $criteria->addCondition($newQuery); */
        //$criteria->order = 'scquotation_id DESC';              

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => false,
            'sort' => array(
                'defaultOrder' => 'scquotation_id DESC',
                'attributes' => array(
                    'project' => array(
                        'asc' => 'projects.name',
                        'desc' => 'projects.name DESC',
                    ),
                    '*',
                ),
            ),
        ));
    }


    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Scquotation the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function getSum($dataProvider)
    {
        $sum = 0;
        foreach ($dataProvider->getData() as $item) {
            $sum += $item->scquotation_amount;
        }
        return $sum;
    }

    public function getExpenseType($quotation_id)
    {
        $quotation_item_model = ScquotationItems::model()->findByAttributes(array('scquotation_id' => $quotation_id));
        if (!empty($quotation_item_model->expensehead_id)) {
            $expense_type = ExpenseType::model()->findByPk($quotation_item_model->expensehead_id);
        }
        return !empty($expense_type) ? $expense_type->type_id : '';
    }

    public function getCategories($id)
    {
        $items = ScQuotationItemCategory::model()->findAllByAttributes(array('sc_quotaion_id' => $id));
        return $items;
    }

    public function getQuotationItems($id, $catgory_id = false)
    {
        $condition = 'scquotation_id = ' . $id;
        if (!empty($catgory_id)) {
            $condition .= ' AND item_category_id =' . $catgory_id;
        } else {
            $condition .= ' AND item_category_id IS NULL';
        }
        $items = ScquotationItems::model()->findAll(array('condition' => $condition));
        return $items;
    }

    public function getExpensehead($model)
    {
        if (!empty($model->expensehead_id)) {
            $expense_head = $model->expensehead->type_name;
        } else {
            $expense_head = '';
        }
        return $expense_head;
    }

    public function getTotalQuotationAmount($quotation_id)
    {
        $amount_sum = ScquotationItems::model()->find(array(
            'select' => 'scquotation_id, SUM(item_amount) as item_amount',
            'condition' => 'scquotation_id=:scquotation_id',
            'params' => array(':scquotation_id' => $quotation_id)
        ));
        return isset($amount_sum['item_amount']) ? $amount_sum['item_amount'] : 0;
    }
}
