<?php
/* @var $this ExpensesController */
/* @var $data Expenses */
if ($index == 0) {

?>

    <thead class="entry-table sticky-thead">
        <tr>
            <th>Sl No.</th>
            <th>Company</th>
            <th>Project</th>
            <th>Bill Number</th>
            <th>Invoice Number</th>
            <th>Expense Head</th>
            <th>Vendor</th>
            <th>Expense Type</th>
            <th>Petty Cash By</th>
            <th>Bank</th>
            <th>Cheque No</th>
            <th>Description</th>
            <th>Amount</th>
            <th>Tax</th>
            <th>Total</th>
            <th>Amount Paid</th>
            <th>TDS</th>
            <th>Receipt Type</th>
            <th>Receipt</th>
            <th>Purchase Type</th>
            <th>Amount To Beneficiary</th>
        </tr>
    </thead>
<?php
}
?>
<tr>
    <td class="left_fix"><?php echo $index + 1; ?></td>
    <td class="left_fix">
        <?php
        $company = Company::model()->findByPk($data->company_id);
        if ($company)
            echo $company->name;
        ?>
    </td>
    <td class="left_fix">
        <?php
        $project = Projects::model()->findByPk($data->projectid);
        if ($project)
            echo $project->name;
        ?>
    </td>
    <td>
        <?php
        $bill = Bills::model()->findByPk($data->bill_id);
        if ($bill)
            echo '<span class="bill_view" id="' . $data->bill_id . '"style="color: #337ab7;
            cursor: pointer;">' . $bill->bill_number . '</span>';
        ?>
    </td>
    <td>
        <?php
        $invoice = Invoice::model()->findByPk($data->invoice_id);
        //if($invoice)
        //echo $invoice->invoice_id;
        ?>
    </td>
    <td>
        <?php
        $exphead = ExpenseType::model()->findByPk($data->exptype);
        if ($exphead)
            echo $exphead->type_name;
        ?>
    </td>
    <td>
        <?php
        $vendor = Vendors::model()->findByPk($data->vendor_id);
        if ($vendor)
            echo $vendor->name;
        ?>
    </td>
    <td>
        <?php
        $exptype = Status::model()->findByPk($data->expense_type);
        if ($exptype)
            echo $exptype->caption;
        ?>
    </td>
    <td>
        <?php
        $pettycash = Users::model()->findByPk($data->employee_id);
        if ($pettycash)
            echo $pettycash->first_name . " " . $pettycash->last_name;
        ?>
    </td>
    <td>
        <?php
        $bank = Bank::model()->findByPk($data->bank_id);
        if ($bank)
            echo $bank->bank_id;
        ?>
    </td>
    <td><?php echo $data->cheque_no; ?></td>
    <td><?php echo $data->description; ?></td>
    <td class="text-right"><?php echo Controller::money_format_inr($data->expense_amount, 2); ?></td>
    <td class="text-right"><?php echo Controller::money_format_inr($data->expense_cgst + $data->expense_sgst + $data->expense_igst, 2); ?></td>
    <td class="text-right"><?php echo Controller::money_format_inr($data->amount, 2); ?></td>
    <td class="text-right"><?php echo Controller::money_format_inr($data->paid, 2); ?></td>
    <td class="text-right"><?php echo Controller::money_format_inr($data->expense_tds, 2); ?></td>
    <td>
        <?php
        $exptype = Status::model()->findByPk($data->payment_type);
        if ($exptype)
            echo $exptype->caption;
        ?>
    </td>
    <td class="text-right"><?php echo Controller::money_format_inr($data->receipt, 2); ?></td>
    <td>
        <?php
        if ($data->purchase_type == 1)
            echo "Credit";
        else if ($data->purchase_type == 2)
            echo "Full Paid";
        else if ($data->purchase_type == 3)
            echo "Partially Paid";
        ?>
    </td>
    <td class="text-right"><?php echo Controller::money_format_inr($data->paidamount, 2); ?></td>
</tr>