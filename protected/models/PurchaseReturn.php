<?php

/**
 * This is the model class for table "{{purchase_return}}".
 *
 * The followings are the available columns in table '{{purchase_return}}':
 * @property integer $return_id
 * @property integer $bill_id
 * @property string $return_number
 * @property string $return_date
 * @property double $return_amount
 * @property double $return_taxamount
 * @property double $return_discountamount
 * @property double $return_totalamount
 * @property integer $company_id
 * @property integer $created_by
 * @property string $created_date
 * @property string $updated_date
 * @property integer $return_status
 * @property string $remarks
 * @property integer $vendor_id
 */
class PurchaseReturn extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{purchase_return}}';
    }

    public $vendor_name;
    
    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('return_number, return_date, created_by', 'required'),
            array('bill_id, company_id, created_by, return_status, vendor_id', 'numerical', 'integerOnly'=>true),
            array('return_amount, return_taxamount, return_discountamount, return_totalamount', 'numerical'),
            array('created_date, updated_date, remarks', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('return_id, bill_id, return_number, return_date, return_amount, return_taxamount, return_discountamount, return_totalamount, company_id, created_by, created_date, updated_date, return_status, remarks, vendor_id', 'safe', 'on'=>'search'),
            array('vendor_name', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'return_id' => 'Return',
            'bill_id' => 'Bill',
            'return_number' => 'Return Number',
            'return_date' => 'Return Date',
            'return_amount' => 'Return Amount',
            'return_taxamount' => 'Return Taxamount',
            'return_discountamount' => 'Return Discountamount',
            'return_totalamount' => 'Return Totalamount',
            'company_id' => 'Company',
            'created_by' => 'Created By',
            'created_date' => 'Created Date',
            'updated_date' => 'Updated Date',
            'return_status' => 'Return Status',
            'remarks' => 'Remarks',
            'vendor_id' => 'Vendor Name',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('return_id', $this->return_id);
        $criteria->compare('bill_id', $this->bill_id);
        $criteria->compare('return_number', $this->return_number, true);
        $criteria->compare('return_date', $this->return_date, true);
        $criteria->compare('return_amount', $this->return_amount);
        $criteria->compare('return_taxamount', $this->return_taxamount);
        $criteria->compare('return_discountamount', $this->return_discountamount);
        $criteria->compare('return_totalamount', $this->return_totalamount);
        $criteria->compare('company_id', $this->company_id);
        $criteria->compare('created_by', $this->created_by);
        $criteria->compare('created_date', $this->created_date, true);
        $criteria->compare('updated_date', $this->updated_date, true);
        $criteria->compare('return_status', $this->return_status);
        $criteria->compare('remarks', $this->remarks, true);
        $criteria->compare('vendor_id', $this->vendor_id);

        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        $newQuery = "";
        foreach ($arrVal as $arr) {
            if ($newQuery) $newQuery .= ' OR';
            $newQuery .= " FIND_IN_SET('".$arr."', company_id)";
        }
        $criteria->addCondition($newQuery);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return PurchaseReturn the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}
