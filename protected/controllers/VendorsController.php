<?php

class VendorsController extends Controller
{

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        /*return array(
          array('allow', // allow all users to perform 'index' and 'view' actions
          'actions' => array('newlist', 'dailyEntries', 'savetoexcel', 'vendordetails', 'savedata', 'handsonDelete',
          'vendorData', 'paymentReport', 'savePaymentreport', 'progressSave', 'savetopdf1', 'savetoexcel1', 'getDailyVendorDetails', 'getDataByDate', 'addDailyentries', 'updateDailyentries', 'Ajaxcall', 'deletdailyvendors'),
          'users' => array('@'),
          'expression' => 'yii::app()->user->role<=3',
          ),
          array('allow', // allow admin user to perform 'admin' and 'delete' actions
          'actions' => array('getCompanyCondition', 'update'),
          'users' => array('@'),
          'expression' => 'yii::app()->user->role<=2',
          ),
          array('allow', // allow admin user to perform 'admin' and 'delete' actions
          'actions' => array('delete'),
          'users' => array('@'),
          'expression' => 'yii::app()->user->role==1',
          ),
          array('deny', // deny all users
          'users' => array('*'),
          ),
          ); */

        $accessArr = array();
        $accessauthArr = array();
        $controller = Yii::app()->controller->id;
        if (isset(Yii::app()->user->menuall)) {
            if (array_key_exists($controller, Yii::app()->user->menuall)) {
                $accessArr = Yii::app()->user->menuall[$controller];
            }
        }

        if (isset(Yii::app()->user->menuauth)) {
            if (array_key_exists($controller, Yii::app()->user->menuauth)) {
                $accessauthArr = Yii::app()->user->menuauth[$controller];
            }
        }
        $access_privlg = count($accessauthArr);
        return array(
            array(
                'allow', // allow all users to perform 'index' and 'view' actions
                'actions' => $accessArr,
                'users' => array('@'),
                'expression' => "$access_privlg > 0",
            ),
            array(
                'allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => $accessauthArr,
                'users' => array('@'),
                'expression' => "$access_privlg > 0",
            ),
            
            array(
                'allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('ageingvendorReport'),
                'users' => array('*'),
            ),
            array(
                'deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $tblpx = Yii::app()->db->tablePrefix;
        if (isset($_GET['layout']))
            $this->layout = false;
        $model = new Vendors;
        // Uncomment the following line if AJAX validation is needed

        $this->performAjaxValidation($model);
        $model->status = 1;


        if (isset($_POST['Vendors'])) {

            //print_r($_POST['Vendors']); die();

            $model->attributes = $_POST['Vendors'];
            $model->created_by = yii::app()->user->id;
            $model->created_date = date('Y-m-d H:i:s');
            if (!empty($_POST['Vendors']['company_id'])) {
                $company = implode(",", $_POST['Vendors']['company_id']);
                $model->company_id = $company;
            } else {
                $model->company_id = NULL;
            }
            if ($model->save()) {
                Yii::app()->user->setFlash('success', "Vendor created successfully. ");
                $vendor_id = Yii::app()->db->getLastInsertID();
            }

            if (isset($_POST['type']) && count($_POST['type'])) {

                $type = $_POST['type'];


                foreach ($type as $ty) {

                    $query2[] = '(null, ' . $vendor_id . ', ' . $ty . ')';
                }

                $insertassign2 = 'insert into ' . $tblpx . 'vendor_exptype values ' . implode(',', $query2);

                Yii::app()->db->createCommand($insertassign2)->query();
            }

            $this->redirect(array('newList'));
        }



        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        if (isset($_GET['layout']))
            $this->layout = false;
        $model = $this->loadModel($id);
        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);
        if (isset($_POST['Vendors'])) {
            $model->attributes = $_POST['Vendors'];
            if (!empty($_POST['Vendors']['company_id'])) {
                $company = implode(",", $_POST['Vendors']['company_id']);
                $model->company_id = $company;
            } else {
                $model->company_id = NULL;
            }

            if ($model->save()) {
                Yii::app()->user->setFlash('success', "Vendor updated successfully. ");
                $vendor_id = $id;
                $query_part = array();
                $tbl_px = Yii::app()->db->tablePrefix;

                if (isset($_POST['type']) && count($_POST['type'])) {

                    $type = $_POST['type'];

                    $userArr = array();
                    $res = Yii::app()->db->createCommand("SELECT type_id from " . $tbl_px . "vendor_exptype WHERE vendor_id = " . $vendor_id)->queryAll();
                    foreach ($res as $value) {
                        if (!in_array($value['type_id'], $_POST['type'])) {
                            Yii::app()->db->createCommand('delete from ' . $tbl_px . 'vendor_exptype where vendor_id=' . $vendor_id . ' AND type_id =' . $value['type_id'])->query();
                        }
                        $userArr[] = $value['type_id'];
                    }
                    foreach ($type as $ty) {
                        if (!in_array($ty, $userArr)) {
                            $query2[] = '(null, ' . $vendor_id . ', ' . $ty . ')';
                        }
                    }
                    if (!empty($query2)) {
                        $insertassign2 = 'insert into ' . $tbl_px . 'vendor_exptype values ' . implode(',', $query2);
                        Yii::app()->db->createCommand($insertassign2)->query();
                    }
                } else {
                    if (empty($_POST['type'])) {
                        Yii::app()->db->createCommand('delete from ' . $tbl_px . 'vendor_exptype where vendor_id=' . $id)->query();
                    }
                }

                //$this->redirect(array('newList'));
                if (Yii::app()->user->returnUrl != 0) {
                    $this->redirect(array('newlist', 'Vendors_page' => Yii::app()->user->returnUrl));
                } else {
                    $this->redirect(array('newlist'));
                }
            }
        }



        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $dataProvider = new CActiveDataProvider('Vendors');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model = new Vendors('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Vendors']))
            $model->attributes = $_GET['Vendors'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Vendors the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Vendors::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    public function loadDailyModel($id)
    {
        $model = Dailyvendors::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    public function actionNewlist()
    {

        $model = new Vendors('search');

        $model->unsetAttributes();  // clear any default values

        if (isset($_GET['Vendors']))
            $model->attributes = $_GET['Vendors'];



        if (Yii::app()->user->role == 1) {



            $this->render('newlist', array(
                'model' => $model,
                'dataProvider' => $model->search(),
            ));
        } else {

            $this->render('newlist', array(
                'model' => $model,
                'dataProvider' => $model->search(Yii::app()->user->id),
            ));
        }
    }

    public function actionDailyEntries()
    {
        $model = new Dailyvendors;
        $id = Yii::app()->user->id;
        $tblpx = Yii::app()->db->tablePrefix;
        $currDate = date('Y-m-d', strtotime(date("Y-m-d")));
        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        $newQuery = "";
        foreach ($arrVal as $arr) {
            if ($newQuery)
                $newQuery .= ' OR';
            $newQuery .= " FIND_IN_SET('" . $arr . "', d.company_id)";
        }
        $vendorData = Yii::app()->db->createCommand("SELECT d.*,d.created_date as created,v.*,ba.*,st.*,d.description as vendor_description,pr.name as project_name,d.company_id as company_id FROM " . $tblpx . "dailyvendors  d
                        LEFT JOIN " . $tblpx . "vendors v ON v.vendor_id = d.vendor_id
                        LEFT JOIN " . $tblpx . "bank ba ON d.bank = ba.bank_id
                        LEFT JOIN " . $tblpx . "status st ON d.payment_type = st.sid
                        LEFT JOIN " . $tblpx . "projects pr ON d.project_id = pr.pid
                        WHERE d.date = '" . $currDate . "' AND (" . $newQuery . ")")->queryAll();
        $this->render('vendorbook', array('model' => $model, 'id' => $id, 'newmodel' => $vendorData));
    }

    /**
     * Performs the AJAX validation.
     * @param Vendors $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'vendors-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionVendordetails()
    {

        $tblpx = Yii::app()->db->tablePrefix;
        $userid = Yii::app()->user->id;
        $cadate = date("Y-m-d", strtotime($_REQUEST['cadate']));
        /* ---------------Mod By Rajisha------------------------- */
        $lastdate = Yii::app()->db->createCommand("SELECT date FROM " . $tblpx . "dailyvendors WHERE 1 ORDER BY date DESC LIMIT 1")->queryRow(); //created_by='".Yii::app()->user->id."'
        /* ---------------Mod By Rajisha------------------------- */
        /* if(Yii::app()->user->role==1){
          $where='AND 1=1';
          }else{
          $where="AND dv.created_by='".Yii::app()->user->id."'";
          } */

        $qry = "SELECT v.name,dv.amount,dv.description,dv.daily_v_id, dv.payment_type, dv.bank, dv.cheque_no, bk.bank_name,st.caption
               FROM " . $tblpx . "dailyvendors as dv
               left join " . $tblpx . "vendors as v on v.vendor_id=dv.vendor_id left join " . $tblpx . "status as st on st.sid=dv.payment_type left join " . $tblpx . "bank as bk on bk.bank_id=dv.bank  where dv.date='" . $cadate . "'";

        $data = Yii::app()->db->createCommand($qry)->queryAll();

        $data1 = array();
        if ($data) {
            $data1 = array();
            $amount = '';
            foreach ($data as $dt) {

                $res = array($dt['daily_v_id'], $dt['name'], $dt['caption'], $dt['bank_name'], $dt['cheque_no'], $dt['amount'], $dt['description']);
                array_push($data1, $res);
            }
            echo json_encode(array('result' => $data1, 'lastdate' => isset($lastdate['date']) ? date("d-M-Y", strtotime($lastdate['date'])) : ''));
        } else {
            $data1 = array();
            $res = array("", "", "", "");
            array_push($data1, $res);
            echo json_encode(array("result" => $data1, 'lastdate' => isset($lastdate['date']) ? date("d-M-Y", strtotime($lastdate['date'])) : ''));
        }
    }

    public function actionSavedata()
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $userid = Yii::app()->user->id;

        if ($userid != "") {
            if (!empty($_POST['data'])) {
                $data = $_POST['data'];
                foreach ($data as $dt) {
                    if (isset($dt) && $dt[5] > 0) {
                        $exp_id = $dt[0];
                        $proj_name = $dt[1];
                        $project_id_q = Yii::app()->db->createCommand("select `vendor_id` from " . $tblpx . "vendors where `name`='" . addslashes($proj_name) . "'")->queryAll();

                        foreach ($project_id_q as $proj) {
                            $proj_id = $proj['vendor_id'];
                        } //echo "select exp_id from " . $tblpx . "expenses where exp_id=$exp_id";

                        $type = Yii::app()->db->createCommand("select `sid` from " . $tblpx . "status where `caption`='" . addslashes($dt[2]) . "'")->queryRow();

                        $bank = Yii::app()->db->createCommand("select `bank_id` from " . $tblpx . "bank where `bank_name`='" . addslashes($dt[3]) . "'")->queryRow();


                        //print_r($type);
                        //exit();

                        $exp_found = Yii::app()->db->createCommand("select daily_v_id from " . $tblpx . "dailyvendors where daily_v_id='$exp_id'")->queryAll();
                        if (count($exp_found) == 0) {
                            $expense_model = new Dailyvendors();
                            $expense_model->vendor_id = $proj_id;

                            $expense_model->payment_type = $type['sid'];
                            $expense_model->bank = $bank['bank_id'];
                            $expense_model->cheque_no = $dt[4];

                            $expense_model->amount = $dt[5];
                            $expense_model->description = $dt[6];
                            $expense_model->date = date("Y-m-d", strtotime($_POST['date']));
                            $expense_model->created_by = NULL;
                            $expense_model->created_date = date('Y-m-d H:i:s');
                            $expense_model->updated_by = NULL;
                            $expense_model->save(false);
                        } else {


                            $expense_model = $this->loadDailyModel($exp_id);
                            $expense_model->vendor_id = $proj_id;

                            $expense_model->payment_type = $type['sid'];
                            $expense_model->bank = $bank['bank_id'];
                            $expense_model->cheque_no = $dt[4];


                            $expense_model->amount = $dt[5];
                            $expense_model->description = $dt[6];
                            $expense_model->date = date("Y-m-d", strtotime($_POST['date']));
                            //$expense_model->created_date = date('Y-m-d');
                            $expense_model->updated_by = NULL;
                            $expense_model->updated_date = date('Y-m-d H:i:s');
                            $expense_model->save(false);
                        }
                    } else {
                        echo json_encode(array("result" => "error"));
                    }
                }
                ////Insertion end
                //echo json_encode(array("result" => "ok"));
            }
            echo json_encode(array("result" => "ok"));
        } else {
            echo json_encode(array("result" => "empty"));
        }
    }

    public function actionHandsonDelete()
    {

        $id = $_POST['id'];
        //print_r($id);
        $tblpx = Yii::app()->db->tablePrefix;
        $query = Yii::app()->db->createCommand('DELETE FROM {$tblpx}dailyvendors WHERE daily_v_id=' . $id)->execute();
        if ($query) {
            $query1 = Yii::app()->db->createCommand("UPDATE {$tblpx}db_changes SET `status`=2 WHERE data_id = '" . $id . "' AND status=0 AND table_name='{$tblpx}dailyvendors'")->execute();
            $queryval = Yii::app()->db->createCommand('SELECT count(*) FROM  ' . $tblpx . 'db_changes WHERE data_id=' . $id . ' AND status=1 AND table_name="{$tblpx}dailyvendors"')->queryScalar();
            if ($queryval) {
                $query2 = Yii::app()->db->createCommand("UPDATE {$tblpx}db_changes SET `status`=0 WHERE data_id = '" . $id . "' AND action='DELETE' AND table_name='{$tblpx}dailyvendors'")->execute();
            }
        }
        $out = array(
            'result' => 'ok'
        );
        echo json_encode($out);
    }

    public function actionVendorData()
    {
        //print_r($_POST['cadate']);
        $tblpx = Yii::app()->db->tablePrefix;
        if (isset($_REQUEST['cadate']))
            $cadate = date("Y-m-d", strtotime($_REQUEST['cadate']));
        /* $qry = "SELECT v.name,dv.amount,dv.description,dv.daily_v_id
          FROM " . $tblpx . "dailyvendors as dv
          left join " . $tblpx . "vendors as v on v.vendor_id=dv.vendor_id where dv.date='".$cadate."'"; */

        $qry = "SELECT v.name,dv.amount,dv.description,dv.daily_v_id, dv.payment_type, dv.bank, dv.cheque_no, bk.bank_name,st.caption
               FROM " . $tblpx . "dailyvendors as dv
               left join " . $tblpx . "vendors as v on v.vendor_id=dv.vendor_id left join " . $tblpx . "status as st on st.sid=dv.payment_type left join " . $tblpx . "bank as bk on bk.bank_id=dv.bank  where dv.date='" . $cadate . "'";

        /*  AND dv.created_by='".Yii::app()->user->id."'" */
        $data = Yii::app()->db->createCommand($qry)->queryAll();

        $data1 = array();
        if ($data) {
            $data1 = array();
            $amount = '';
            foreach ($data as $dt) {

                $res = array($dt['daily_v_id'], $dt['name'], $dt['caption'], $dt['bank_name'], $dt['cheque_no'], $dt['amount'], $dt['description']);
                array_push($data1, $res);
            }
            echo json_encode(array('result' => $data1));
        } else {
            $data1 = array();
            $res = array("", "", "", "");
            array_push($data1, $res);
            echo json_encode(array("result" => $data1));
        }
    }

    public function actionPaymentReport()
    {

        $tblpx  = Yii::app()->db->tablePrefix;
        $date_from      = "";
        $date_to        = "";
        $total_vendor_Amount = 0;
        if (isset($_REQUEST['vendor_id']) && !empty($_REQUEST['vendor_id'])) {
            $vendor_id = $_REQUEST['vendor_id'];
            if (!empty($_REQUEST['company_id'])) {
                $company_id = $_REQUEST['company_id'];
            } else {
                $company_id = "";
            }
            $model      = Vendors::model()->findByPk($vendor_id);
            $date_from  = $_REQUEST['date_from'];
            $date_to    = $_REQUEST['date_to'];
            $condition1 = "";
            $condition2 = "";
            $condition3 = "";
            $condition4 = "";
            $condition5 = "";
            $condition6 = "";
            $condition7 = "";
            $condition8 = "";
            $condition6a = "";
            $condition9 = "";
            $condition10="";
            $total_vendor_Amount=0;
            if (!empty($date_from) && !empty($date_to)) {

                $condition1 = " AND ex.date BETWEEN '" . date("Y-m-d", strtotime($date_from)) . "' AND '" . date("Y-m-d", strtotime($date_to)) . "'";
                $condition2 = " AND date BETWEEN '" . date("Y-m-d", strtotime($date_from)) . "' AND '" . date("Y-m-d", strtotime($date_to)) . "'";
                $condition3 = " AND bill_date BETWEEN '" . date("Y-m-d", strtotime($date_from)) . "' AND '" . date("Y-m-d", strtotime($date_to)) . "'";
                $condition4 = " AND return_date BETWEEN '" . date("Y-m-d", strtotime($date_from)) . "' AND '" . date("Y-m-d", strtotime($date_to)) . "'";
                $condition9 = " AND b.bill_date BETWEEN '" . date("Y-m-d", strtotime($date_from)) . "' AND '" . date("Y-m-d", strtotime($date_to)) . "'";
                $condition10 = " AND e.date BETWEEN '" . date("Y-m-d", strtotime($date_from)) . "' AND '" . date("Y-m-d", strtotime($date_to)) . "'";
            } else if (!empty($date_from) && empty($date_to)) {

                $condition1 = " AND ex.date >= '" . date("Y-m-d", strtotime($date_from)) . "'";
                $condition2 = " AND date >= '" . date("Y-m-d", strtotime($date_from)) . "'";
                $condition3 = " AND bill_date >= '" . date("Y-m-d", strtotime($date_from)) . "'";
                $condition9 = " AND b.bill_date >= '" . date("Y-m-d", strtotime($date_from)) . "'";
                $condition10 = " AND e.date BETWEEN '" . date("Y-m-d", strtotime($date_from)) . "' AND '" . date("Y-m-d", strtotime($date_to)) . "'";
                
                $condition4 = " AND return_date >= '" . date("Y-m-d", strtotime($date_from)) . "'";
            } else if (empty($date_from) && !empty($date_to)) {

                $condition1 = " AND ex.date <= '" . date("Y-m-d", strtotime($date_to)) . "'";
                $condition2 = " AND date <= '" . date("Y-m-d", strtotime($date_to)) . "'";
                $condition9 = " AND b.bill_date <= '" . date("Y-m-d", strtotime($date_to)) . "'";
                $condition10 = " AND e.date BETWEEN '" . date("Y-m-d", strtotime($date_from)) . "' AND '" . date("Y-m-d", strtotime($date_to)) . "'";
                $condition3 = " AND bill_date <= '" . date("Y-m-d", strtotime($date_to)) . "'";
                $condition4 = " AND return_date <= '" . date("Y-m-d", strtotime($date_to)) . "'";
            } else {

                $condition1 = "";
                $condition2 = "";
                $condition3 = "";
                $condition4 = "";
                $condition5 = "";
                $condition6 = "";
                $condition7 = "";
                $condition8 = "";
                $condition6a = "";
                $condition9 = "";
                $condition10="";
            }
            $user       = Users::model()->findByPk(Yii::app()->user->id);
            $arrVal     = explode(',', $user->company_id);
            $newQuery1  = "";
            $newQuery2  = "";
            $newQuery3  = "";
            $newQuery4  = "";
            $newQuery5  = "";
            $newQuery6 = "";
            $newQuery7 = "";
            $newQuery8 = "";
            $newQuery6a = "";
            if (!empty($_REQUEST['company_id'])) {
                foreach ($arrVal as $arr) {
                    if ($newQuery1) $newQuery1 .= ' OR';
                    if ($newQuery2) $newQuery2 .= ' OR';
                    if ($newQuery3) $newQuery3 .= ' OR';
                    if ($newQuery4) $newQuery4 .= ' OR';
                    if ($newQuery5) $newQuery5 .= ' OR';
                    $newQuery1 .= " FIND_IN_SET('" . $arr . "', ex.company_id)";
                    $newQuery2 .= " FIND_IN_SET('" . $arr . "', company_id)";
                    $newQuery3 .= " FIND_IN_SET('" . $arr . "', t.company_id)";
                    $newQuery4 .= " FIND_IN_SET('" . $arr . "', r.company_id)";
                    $newQuery5 .= " FIND_IN_SET('" . $arr . "', r.company_id)";
                }
                $condition1 .= " AND ex.company_id = " . $_REQUEST['company_id'] . "";
                $condition2 .= " AND company_id = " . $_REQUEST['company_id'] . "";
                $condition3 .= " AND t.company_id = " . $_REQUEST['company_id'] . "";
                $condition4 .= " AND r.company_id = " . $_REQUEST['company_id'] . "";
                $condition5 .= " AND e.company_id = " . $_REQUEST['company_id'] . "";
            } else {
                foreach ($arrVal as $arr) {
                    if ($newQuery1) $newQuery1 .= ' OR';
                    if ($newQuery2) $newQuery2 .= ' OR';
                    if ($newQuery3) $newQuery3 .= ' OR';
                    if ($newQuery4) $newQuery4 .= ' OR';
                    if ($newQuery5) $newQuery5 .= ' OR';
                    $newQuery1 .= " FIND_IN_SET('" . $arr . "', ex.company_id)";
                    $newQuery2 .= " FIND_IN_SET('" . $arr . "', company_id)";
                    $newQuery3 .= " FIND_IN_SET('" . $arr . "', t.company_id)";
                    $newQuery4 .= " FIND_IN_SET('" . $arr . "', r.company_id)";
                    $newQuery5 .= " FIND_IN_SET('" . $arr . "', e.company_id)";
                }
                $condition1 .= " AND (" . $newQuery1 . ")";
                $condition2 .= " AND (" . $newQuery2 . ")";
                $condition3 .= " AND (" . $newQuery3 . ")";
                $condition4 .= " AND (" . $newQuery4 . ")";
                $condition5 .= " AND (" . $newQuery5 . ")";
            }
            Yii::app()->db->createCommand('SET SQL_BIG_SELECTS=1')->execute();
            // recon return
            $recon_return_sql1 = "SELECT r.*,e.* "
                . " FROM " . $tblpx . "reconciliation r "
                . " join " . $tblpx . "expenses e "
                . " on r.reconciliation_parentid =e.exp_id  "
                . " where  r.reconciliation_status='0' "
                . " and e.vendor_id='" . $vendor_id . "' "
                . " and r.reconciliation_payment ='Purchase Return'";
            $reconciliation_data = Yii::app()->db->createCommand($recon_return_sql1)->queryAll();

            //recon Dyabook summary 
            $recon_daysum_sql1 = "SELECT r.*,e.* "
                . " FROM " . $tblpx . "reconciliation r "
                . " join " . $tblpx . "expenses e "
                . " on r.reconciliation_parentid =e.exp_id  "
                . " where  r.reconciliation_status='0' "
                . " and e.vendor_id='" . $vendor_id . "' ";
            $reconciliation_data1 = Yii::app()->db->createCommand($recon_daysum_sql1)->queryAll();

            //direct v payment recon

            $recon_direct_sql1 = "SELECT r.*,d.* "
                . " FROM " . $tblpx . "reconciliation r "
                . " join " . $tblpx . "dailyvendors d "
                . " on r.reconciliation_parentid =d.daily_v_id  "
                . " where  r.reconciliation_status='0' "
                . " and d.vendor_id='" . $vendor_id . "' "
                . " and r.reconciliation_payment ='Vendor Payment'";
            $reconciliation_data2 = Yii::app()->db->createCommand($recon_direct_sql1)->queryAll();


            $cheque_array = array();
            foreach ($reconciliation_data as  $data) {
                $value = $data['cheque_no'];
                if ($value != '') {
                    $recon_bill = $value;
                    array_push($cheque_array, $recon_bill);
                }
            }
            $cheque_array1 = implode(',', $cheque_array);

            $cheque_array_day_sum = array();
            foreach ($reconciliation_data1 as  $data) {
                $value = $data['cheque_no'];
                if ($value != '') {
                    $recon_bill = "'{$value}'";
                    array_push($cheque_array_day_sum, $recon_bill);
                }
            }

            $cheque_array_day_sum1 = implode(',', $cheque_array_day_sum);

                $daybookSql1     = "SELECT p.name as projectname, a.projectcount,"
                    . "  b.totalamount, c.totalpaid, d.totaltds, "
                    . " e.totalpaidtovendor, f.sumtotalamount, g.sumtotalpaid, "
                    . " h.sumtotaltds, i.sumtotalpaidtovendor, bl.bill_number "
                    . " as billnumber, bl.bill_totalamount, ex.* "
                    . " FROM {$tblpx}expenses ex"
                    . " LEFT JOIN {$tblpx}projects p "
                    . "ON ex.projectid = p.pid"
                    . " LEFT JOIN {$tblpx}bills bl "
                    . " ON ex.bill_id = bl.bill_id"
                    . " LEFT JOIN (SELECT projectid, count(*) "
                    . " as projectcount FROM {$tblpx}expenses "
                    . " WHERE type = 73 AND vendor_id = {$vendor_id}" . $condition2 . " "
                    . " GROUP BY projectid) a "
                    . " ON a.projectid = ex.projectid"
                    . " LEFT JOIN (SELECT projectid, SUM(amount) "
                    . " as totalamount FROM {$tblpx}expenses "
                    . " WHERE type = 73 AND vendor_id = {$vendor_id}" . $condition2 . " "
                    . " AND reconciliation_status=0 "
                    . " GROUP BY projectid) b ON b.projectid = ex.projectid"
                    . " LEFT JOIN (SELECT projectid, SUM(paid) as totalpaid "
                    . " FROM {$tblpx}expenses WHERE type = 73 "
                    . " AND vendor_id = {$vendor_id}" . $condition2 . " AND  "
                    . " reconciliation_status=0 GROUP BY projectid) c "
                    . " ON c.projectid = ex.projectid"
                    . " LEFT JOIN (SELECT projectid, SUM(expense_tds) "
                    . " as totaltds FROM {$tblpx}expenses "
                    . " WHERE type = 73 AND vendor_id = {$vendor_id}" . $condition2 . " "
                    . " AND reconciliation_status=0 "
                    . " GROUP BY projectid) d "
                    . " ON d.projectid = ex.projectid"
                    . " LEFT JOIN (SELECT projectid, SUM(paidamount) "
                    . " as totalpaidtovendor FROM {$tblpx}expenses "
                    . " WHERE type = 73 AND vendor_id = {$vendor_id}" . $condition2 . ""
                    . "  AND reconciliation_status= 0 "
                    . " GROUP BY projectid) e ON e.projectid = ex.projectid"
                    . " LEFT JOIN (SELECT projectid, SUM(amount) as sumtotalamount "
                    . " FROM {$tblpx}expenses WHERE type = 73 AND "
                    . " vendor_id = {$vendor_id}" . $condition2 . " AND"
                    . " reconciliation_status=0) f ON 1 = 1"
                    . " LEFT JOIN (SELECT projectid, SUM(paid) "
                    . " as sumtotalpaid FROM {$tblpx}expenses "
                    . " WHERE type = 73 AND vendor_id = {$vendor_id}" . $condition2 . ""
                    . "  AND reconciliation_status=0) g"
                    . "  ON 1 = 1"
                    . " LEFT JOIN (SELECT projectid, SUM(expense_tds) "
                    . " as sumtotaltds FROM {$tblpx}expenses "
                    . " WHERE type = 73 AND vendor_id = {$vendor_id}" . $condition2 . " "
                    . " AND reconciliation_status=0) h ON 1 = 1"
                    . " LEFT JOIN (SELECT projectid, SUM(paidamount) "
                    . " as sumtotalpaidtovendor FROM {$tblpx}expenses "
                    . " WHERE type = 73 AND vendor_id = {$vendor_id}" . $condition2 . ""
                    . "  AND reconciliation_status=0) i ON 1 = 1"
                    . " WHERE ex.type = 73 AND ex.vendor_id = {$vendor_id}" . $condition1 . " AND ex.reconciliation_status=0 ORDER BY ex.projectid";
                $daybookPayment1 = Yii::app()->db->createCommand($daybookSql1)->queryAll();
                $daybookSql = "
    SELECT 
        p.name as projectname, 
        a.projectcount, 
        b.totalamount, 
        c.totalpaid, 
        d.totaltds, 
        e.totalpaidtovendor, 
        f.sumtotalamount,
        z.sumtotalvendoramount, 
        g.sumtotalpaid, 
        h.sumtotaltds, 
        i.sumtotalpaidtovendor, 
        bl.bill_number as billnumber, 
        bl.bill_totalamount, 
        y.sumtotalamountpaidtovendor,
        ex.* 
    FROM 
        {$tblpx}expenses ex
    LEFT JOIN {$tblpx}projects p ON ex.projectid = p.pid
    LEFT JOIN {$tblpx}bills bl ON ex.bill_id = bl.bill_id
    LEFT JOIN (
        SELECT 
            projectid, 
            COUNT(*) as projectcount 
        FROM 
            {$tblpx}expenses 
        WHERE 
            type = 73 AND vendor_id = {$vendor_id} {$condition2} 
        GROUP BY 
            projectid
    ) a ON a.projectid = ex.projectid
    LEFT JOIN (
        SELECT 
            projectid, 
            SUM(amount) as totalamount 
        FROM 
            {$tblpx}expenses 
        WHERE 
            type = 73 AND vendor_id = {$vendor_id} {$condition2} 
            AND (reconciliation_status=1 OR reconciliation_status IS NULL) 
        GROUP BY 
            projectid
    ) b ON b.projectid = ex.projectid
    LEFT JOIN (
        SELECT 
            projectid, 
            SUM(paid) as totalpaid 
        FROM 
            {$tblpx}expenses 
        WHERE 
            type = 73 AND vendor_id = {$vendor_id} {$condition2} 
            AND (reconciliation_status=1 OR reconciliation_status IS NULL) 
        GROUP BY 
            projectid
    ) c ON c.projectid = ex.projectid
    LEFT JOIN (
        SELECT 
            projectid, 
            SUM(expense_tds) as totaltds 
        FROM 
            {$tblpx}expenses 
        WHERE 
            type = 73 AND vendor_id = {$vendor_id} {$condition2} 
            AND (reconciliation_status=1 OR reconciliation_status IS NULL) 
        GROUP BY 
            projectid
    ) d ON d.projectid = ex.projectid
    LEFT JOIN (
        SELECT 
            projectid, 
            SUM(paidamount) as totalpaidtovendor 
        FROM 
            {$tblpx}expenses 
        WHERE 
            expense_type != 0 AND type = 73 AND vendor_id = {$vendor_id} {$condition2} 
            AND (reconciliation_status=1 OR reconciliation_status IS NULL) 
        GROUP BY 
            projectid
    ) e ON e.projectid = ex.projectid
    LEFT JOIN (
        SELECT 
    sub.projectid, 
    SUM(sub.bill_totalamount) AS sumtotalamount
FROM (
    SELECT 
        b.bill_id, 
        pu.project_id AS projectid, 
        b.bill_totalamount
    FROM 
        {$tblpx}bills b
    LEFT JOIN 
        {$tblpx}purchase pu ON b.purchase_id = pu.p_id
    LEFT JOIN 
        {$tblpx}expenses e ON e.bill_id = b.bill_id
    WHERE 
        pu.vendor_id = {$vendor_id} {$condition9}
        AND (e.reconciliation_status = 1 OR e.reconciliation_status IS NULL)
        AND e.type = 73
    GROUP BY 
        b.bill_id, pu.project_id, b.bill_totalamount
) sub
GROUP BY 
    sub.projectid
    ) f ON f.projectid = ex.projectid
    LEFT JOIN (
        SELECT 
            unique_bills.projectid, 
            SUM(unique_bills.bill_amount) as sumtotalvendoramount
        FROM (
            SELECT 
                DISTINCT e.bill_id, 
                e.projectid, 
                SUM(e.amount) as bill_amount
            FROM 
                {$tblpx}expenses e
            WHERE 
                e.vendor_id = {$vendor_id} {$condition10}  
                AND (e.reconciliation_status=1 OR e.reconciliation_status IS NULL) 
                AND e.type=73 
            GROUP BY 
                e.bill_id, e.projectid
        ) as unique_bills
        GROUP BY 
            unique_bills.projectid
    ) z ON z.projectid = ex.projectid
    LEFT JOIN (
        SELECT 
            projectid, 
            SUM(paid) as sumtotalpaid 
        FROM 
            {$tblpx}expenses 
        WHERE 
            type = 73 AND vendor_id = {$vendor_id} {$condition2} 
            AND (reconciliation_status=1 OR reconciliation_status IS NULL) 
        GROUP BY 
            projectid
    ) g ON g.projectid = ex.projectid
    LEFT JOIN (
        SELECT 
            projectid, 
            SUM(expense_tds) as sumtotaltds 
        FROM 
            {$tblpx}expenses 
        WHERE 
            type = 73 AND vendor_id = {$vendor_id} {$condition2} 
            AND (reconciliation_status=1 OR reconciliation_status IS NULL) 
        GROUP BY 
            projectid
    ) h ON h.projectid = ex.projectid
    LEFT JOIN (
        SELECT 
            projectid, 
            SUM(paidamount) as sumtotalpaidtovendor 
        FROM 
            {$tblpx}expenses 
        WHERE 
            expense_type != 0 AND type = 73 AND vendor_id = {$vendor_id} {$condition2} 
            AND (reconciliation_status=1 OR reconciliation_status IS NULL) 
        GROUP BY 
            projectid
    ) i ON i.projectid = ex.projectid
    LEFT JOIN (
        SELECT 
            projectid, 
            SUM(paidamount) as sumtotalamountpaidtovendor 
        FROM 
            {$tblpx}expenses 
        WHERE 
            expense_type != 0 AND type = 73 AND expense_type != '107' AND vendor_id = {$vendor_id} {$condition2} 
            AND (reconciliation_status=1 OR reconciliation_status IS NULL) 
        GROUP BY 
            vendor_id
    ) y ON y.projectid = ex.projectid
    WHERE 
        ex.type = 73 
        AND ex.vendor_id = {$vendor_id} {$condition1} 
        AND (ex.reconciliation_status=1 OR ex.reconciliation_status IS NULL) 
    ORDER BY 
        ex.projectid";

     $daybook_total_sql = "
SELECT 
    e.bill_id, 
    e.exp_id,
    e.projectid
FROM 
    {$tblpx}expenses e
WHERE 
    e.vendor_id = {$vendor_id} {$condition10}   
    AND (e.reconciliation_status = 1 OR e.reconciliation_status IS NULL) 
    AND e.type = 73 
    AND (
        e.bill_id IS NULL
        OR e.exp_id IN (
            SELECT MIN(exp_id) 
            FROM {$tblpx}expenses 
            WHERE vendor_id = {$vendor_id} {$condition10}
              AND (reconciliation_status = 1 OR reconciliation_status IS NULL)
              AND type = 73
              AND bill_id IS NOT NULL
            GROUP BY bill_id
        )
    )
";
  
                  $daybookPayment = Yii::app()->db->createCommand($daybookSql)->queryAll();
        
            //echo "<pre>";print_r($daybookPayment);exit;
            $daybook_total = Yii::app()->db->createCommand($daybook_total_sql)->queryAll();
                $total_vendor_Amount =0;
                if(!empty($daybook_total)){
                    foreach($daybook_total as $daybook_total_entries){
                        $exp_model = Expenses::Model()->findByPk($daybook_total_entries["exp_id"]);
                        if(!empty($exp_model)){
                            $total_vendor_Amount += $exp_model->amount;
                        }
                    }
                }
    
            //die($total_vendor_Amount);

              
            $daybookSql_pending     = "SELECT p.name as projectname, a.projectcount,"
                . " b.totalamount, c.totalpaid, d.totaltds, e.totalpaidtovendor, "
                . " f.sumtotalamount, g.sumtotalpaid, h.sumtotaltds, i.sumtotalpaidtovendor,"
                . "  bl.bill_number as billnumber, bl.bill_totalamount, ex.* "
                . " FROM {$tblpx}expenses ex"
                . " LEFT JOIN {$tblpx}projects p ON ex.projectid = p.pid"
                . " LEFT JOIN {$tblpx}bills bl ON ex.bill_id = bl.bill_id"
                . " LEFT JOIN (SELECT projectid, count(*) as projectcount "
                . " FROM {$tblpx}expenses WHERE type = 73 "
                . " AND vendor_id = {$vendor_id}" . $condition2 . " "
                . " GROUP BY projectid) a ON a.projectid = ex.projectid"
                . " LEFT JOIN (SELECT projectid, SUM(amount) as totalamount "
                . " FROM {$tblpx}expenses WHERE type = 73 "
                . " AND vendor_id = {$vendor_id}" . $condition2 . " GROUP BY projectid) b"
                . " ON b.projectid = ex.projectid"
                . " LEFT JOIN (SELECT projectid, SUM(paid) as totalpaid "
                . " FROM {$tblpx}expenses WHERE type = 73 "
                . " AND vendor_id = {$vendor_id}" . $condition2 . " GROUP BY projectid) c "
                . " ON c.projectid = ex.projectid"
                . " LEFT JOIN (SELECT projectid, SUM(expense_tds) as totaltds "
                . " FROM {$tblpx}expenses WHERE type = 73 "
                . " AND vendor_id = {$vendor_id}" . $condition2 . " "
                . " GROUP BY projectid) d ON d.projectid = ex.projectid"
                . " LEFT JOIN (SELECT projectid, SUM(paidamount) as totalpaidtovendor "
                . " FROM {$tblpx}expenses WHERE type = 73 "
                . " AND vendor_id = {$vendor_id}" . $condition2 . " "
                . " GROUP BY projectid) e ON e.projectid = ex.projectid"
                . " LEFT JOIN (SELECT projectid, SUM(amount) as sumtotalamount "
                . " FROM {$tblpx}expenses WHERE type = 73 "
                . " AND vendor_id = {$vendor_id}" . $condition2 . ") f ON 1 = 1"
                . " LEFT JOIN (SELECT projectid, SUM(paid) as sumtotalpaid "
                . " FROM {$tblpx}expenses WHERE type = 73 "
                . " AND vendor_id = {$vendor_id}" . $condition2 . ") g ON 1 = 1"
                . " LEFT JOIN (SELECT projectid, SUM(expense_tds) as sumtotaltds "
                . " FROM {$tblpx}expenses WHERE type = 73 "
                . " AND vendor_id = {$vendor_id}" . $condition2 . ") h ON 1 = 1"
                . " LEFT JOIN (SELECT projectid, SUM(paidamount) as sumtotalpaidtovendor "
                . " FROM {$tblpx}expenses WHERE type = 73 "
                . " AND vendor_id = {$vendor_id}" . $condition2 . ") i ON 1 = 1"
                . " WHERE ex.type = 73 AND ex.vendor_id = {$vendor_id}" . $condition1 . ""
                . "  ORDER BY ex.projectid";
            $daybookPayment_pending = Yii::app()->db->createCommand($daybookSql_pending)->queryAll();

            $cheque_direct_array = array();
            foreach ($reconciliation_data2 as  $data) {
                $value = "'{$data['cheque_no']}'";
                if ($value != '') {
                    $recon_bill = $value;
                    array_push($cheque_direct_array, $recon_bill);
                }
            }
            $cheque_direct_array1 = implode(',', $cheque_direct_array);
            if (($cheque_direct_array1)) {
                $newQuery7 .= " ex.cheque_no IN(    $cheque_direct_array1)";
                $condition7 .= " AND " . $newQuery7 . "";
                $newQuery8 .= " ex.cheque_no  NOT IN(    $cheque_direct_array1) OR ex.cheque_no IS NULL";
                $condition8 .= " AND " . $newQuery8 . "";
                
            }
            $dailyvSql1 = "SELECT ex.*, a.sumtotalamount, a.sumtdsamount,"
                    . " a.sumpaidamount FROM {$tblpx}dailyvendors ex"
                    . " LEFT JOIN (SELECT SUM(IFNULL(amount, 0) + IFNULL(tax_amount, 0)) "
                    . " as sumtotalamount, SUM(IFNULL(tds_amount, 0)) "
                    . " as sumtdsamount, SUM(IFNULL(paidamount, 0)) "
                    . " as sumpaidamount FROM {$tblpx}dailyvendors "
                    . " WHERE vendor_id = {$vendor_id}" . $condition2 . " "
                    . " AND reconciliation_status= 0 ) a "
                    . " ON 1 = 1"
                    . " WHERE ex.vendor_id = {$vendor_id} AND reconciliation_status= 0" . $condition1 ;

            $dailyvPayment1  = Yii::app()->db->createCommand($dailyvSql1)->queryAll();
            $dailyvSql      = "SELECT ex.*, a.sumtotalamount, a.sumtdsamount,"
                    . "  a.sumpaidamount "
                    . " FROM {$tblpx}dailyvendors ex"
                    . " LEFT JOIN (SELECT SUM(IFNULL(amount, 0) + IFNULL(tax_amount, 0)) "
                    . " as sumtotalamount, SUM(IFNULL(tds_amount, 0)) "
                    . " as sumtdsamount, SUM(IFNULL(paidamount, 0)) "
                    . " as sumpaidamount FROM {$tblpx}dailyvendors "
                    . " WHERE vendor_id = {$vendor_id}" . $condition2 . " "
                    . " AND (reconciliation_status IS NULL OR reconciliation_status=1)"
                    . " AND vendor_id={$vendor_id}) a "
                    . " ON 1 = 1"
                    . " WHERE ex.vendor_id = {$vendor_id} AND (reconciliation_status IS NULL 
                        OR reconciliation_status=1)" . $condition1 
                    . " AND ex.vendor_id={$vendor_id}";
            $dailyvPayment  = Yii::app()->db->createCommand($dailyvSql)->queryAll();

            $paymentData = $this->setDaybookData($vendor_id, $daybookPayment_pending, $condition3);
            $paymentSum     = 0;
            $miscellaneouspaymentData = "";
            $miscellaneouspaymentSum = 0;


            $expreceipt_return1sql = "select * from " . $tblpx . "expenses ex"
                . " WHERE amount !=0 AND vendor_id=" . $vendor_id . " "
                . " AND type = '72' " . $condition1 . " "
                . " AND return_id IS NOT NULL "
                . " AND (" . $newQuery2 . ") AND ex.reconciliation_status =0 "
                . " ORDER BY date";
            $expreceipt_return1 = Yii::app()->db->createCommand($expreceipt_return1sql)->queryAll();
            
            $expreceipt_returnsql = "select * from " . $tblpx . "expenses ex"
                . " WHERE amount !=0 AND vendor_id=" . $vendor_id . ""
                . "  AND type = '72' " . $condition1 . " AND return_id IS NOT NULL "
                . " AND (" . $newQuery2 . ") AND (ex.reconciliation_status =1 OR ex.reconciliation_status IS NULL)"
                . " ORDER BY date";                
            $expreceipt_return = Yii::app()->db->createCommand($expreceipt_returnsql)->queryAll();
            $defect_sql = "SELECT return_id,return_amount,return_date,"
                . " return_number,w.warehouse_name,r.warehousereceipt_no,"
                . " b.bill_number,p.vendor_id "
                . " FROM jp_defect_return d "
                . " LEFT JOIN jp_warehousereceipt r "
                . " ON d.receipt_id=r.warehousereceipt_id "
                . " LEFT JOIN jp_bills b ON b.bill_id = r.warehousereceipt_bill_id "
                . " LEFT JOIN jp_purchase p ON p.p_id=b.purchase_id "
                . " LEFT JOIN jp_warehouse w "
                . " ON w.warehouse_id =r.warehousereceipt_warehouseid "
                . " WHERE p.vendor_id = " . $vendor_id. " ".$condition4;
               
            $defect_return = Yii::app()->db->createCommand($defect_sql)->queryAll();
        } else {            
            $date_from  = isset($_REQUEST['date_from'])?$_REQUEST['date_from']:"";
            $date_to    = isset($_REQUEST['date_to'])?$_REQUEST['date_to']:"";
            $datecondition3 = "AND '1=1'";
            $datecondition1 = $datecondition2 =  "AND '1=1'";
            if (!empty($date_from) && !empty($date_to)) {

                $datecondition1 = " AND e.date BETWEEN '" . date("Y-m-d", strtotime($date_from)) . "' AND '" . date("Y-m-d", strtotime($date_to)) . "'";
                $datecondition2 = " AND d.date BETWEEN '" . date("Y-m-d", strtotime($date_from)) . "' AND '" . date("Y-m-d", strtotime($date_to)) . "'";
                $datecondition3 = " AND DATE(b.bill_date) BETWEEN '" . date("Y-m-d", strtotime($date_from)) . "' AND '" . date("Y-m-d", strtotime($date_to)) . "'";                
            } else if (!empty($date_from) && empty($date_to)) {

                $datecondition1 = " AND e.date >= '" . date("Y-m-d", strtotime($date_from)) . "'";
                $datecondition2 = " AND d.date >= '" . date("Y-m-d", strtotime($date_from)) . "'";
                $datecondition3 = " AND DATE(b.bill_date) >= '" . date("Y-m-d", strtotime($date_from)) . "'";
            } else if (empty($date_from) && !empty($date_to)) {
                $datecondition1 = " AND e.date <= '" . date("Y-m-d", strtotime($date_to)) . "'";
                $datecondition2 = " AND d.date <= '" . date("Y-m-d", strtotime($date_to)) . "'";
                $datecondition3 = " AND DATE(b.bill_date) <= '" . date("Y-m-d", strtotime($date_to)) . "'";
            } else {
                $datecondition1 = "AND '1=1'";
                $datecondition2 = "AND '1=1'";
                $datecondition3 = "AND '1=1'";
            }
                
            $vendor_id      = 0;
            $company_id     = "";
            $sqlData        = "SELECT v.vendor_id,v.name, a.billtotal, a.billadditionaltotal, "
                . " b.daybookpaid, c.vendorpaid,d.daybookcredit FROM {$tblpx}vendors v"
                . " LEFT JOIN (SELECT p.vendor_id, SUM(IFNULL(b.bill_totalamount, 0))"
                . " AS billtotal,   SUM(IFNULL(b.bill_additionalcharge, 0)) "
                . " AS billadditionaltotal "
                . " FROM {$tblpx}bills b "
                . " LEFT JOIN {$tblpx}purchase p ON b.purchase_id = p.p_id "
                . " WHERE (" . $this->getCompanyCondition("b") . ") ".$datecondition3
                . " GROUP BY p.vendor_id) a ON a.vendor_id = v.vendor_id"
                . " LEFT JOIN (SELECT e.vendor_id, SUM(IFNULL(e.paidamount, 0)) "
                . " as daybookpaid FROM {$tblpx}expenses e "
                . " WHERE e.type = 73 AND e.expense_type!=107 AND e.expense_type != 0 AND  (e.reconciliation_status=1 OR e.reconciliation_status IS NULL) AND (" . $this->getCompanyCondition("e") . ") ".$datecondition1
                . " GROUP BY e.vendor_id) b "
                . " ON b.vendor_id = v.vendor_id"
                . " LEFT JOIN (SELECT e.vendor_id, SUM(IFNULL(e.amount, 0)) "
                . " as daybookcredit FROM {$tblpx}expenses e "
                . " WHERE e.type = 73 AND e.expense_type=0 AND (" . $this->getCompanyCondition("e") . ") ".$datecondition1
                . " GROUP BY e.vendor_id) d "
                . " ON d.vendor_id = v.vendor_id"
                . " LEFT JOIN (SELECT d.vendor_id, SUM(IFNULL(d.amount, 0) + IFNULL(d.tax_amount, 0))"
                . " as vendorpaid FROM {$tblpx}dailyvendors d "
                . " WHERE (d.reconciliation_status IS NULL OR d.reconciliation_status=1) AND (" . $this->getCompanyCondition("d") . ") ".$datecondition2
                . " GROUP BY d.vendor_id) c "
                . " ON c.vendor_id = v.vendor_id"
                . " WHERE (" . $this->getCompanyCondition("v") . ") "
                . " AND v.vendor_type != 2 ORDER BY v.name";
            //die($sqlData);
            $miscellaneous_sql_data = "SELECT v.name, a.billtotal, b.daybookpaid, c.vendorpaid "
                . " FROM {$tblpx}vendors v"
                . " LEFT JOIN (SELECT p.vendor_id, SUM(IFNULL(b.bill_totalamount, 0))"
                . "  as billtotal FROM {$tblpx}bills b "
                . " LEFT JOIN {$tblpx}purchase p "
                . " ON b.purchase_id = p.p_id "
                . " WHERE (" . $this->getCompanyCondition("b") . ") " .$datecondition3
                . " GROUP BY p.vendor_id) a ON a.vendor_id = v.vendor_id"
                . " LEFT JOIN (SELECT e.vendor_id, SUM(IFNULL(e.paid, 0)) "
                . " as daybookpaid FROM {$tblpx}expenses e "
                . " WHERE e.type = 73 AND (" . $this->getCompanyCondition("e") . ") " .$datecondition1
                . " GROUP BY e.vendor_id) b ON b.vendor_id = v.vendor_id"
                . " LEFT JOIN (SELECT d.vendor_id, SUM(IFNULL(d.amount, 0) + IFNULL(d.tax_amount, 0))"
                . "  as vendorpaid FROM {$tblpx}dailyvendors d "
                . " WHERE (" . $this->getCompanyCondition("d") . ") ".$datecondition2
                . " GROUP BY d.vendor_id) c ON c.vendor_id = v.vendor_id"
                . " WHERE (" . $this->getCompanyCondition("v") . ") "
                . " AND v.vendor_type = 2 ORDER BY v.name";
               // die($miscellaneous_sql_data);
            $sqlTotal       = "SELECT SUM(a.billtotal) as billsum, SUM(b.daybookpaid) "
                . " as daybooksum, SUM(b.daybookcredit) as daybookcreditsum , "
                . " SUM(c.vendorpaid) as vendorsum "
                . " FROM {$tblpx}vendors v"
                . " LEFT JOIN (SELECT p.vendor_id, SUM(IFNULL(b.bill_totalamount, 0) + IFNULL(b.bill_additionalcharge,0)) "
                . " as billtotal FROM {$tblpx}bills b "
                . " LEFT JOIN {$tblpx}purchase p ON b.purchase_id = p.p_id "
                . " WHERE (" . $this->getCompanyCondition("b") . ") "
                . " GROUP BY p.vendor_id) a ON a.vendor_id = v.vendor_id"
                . " LEFT JOIN (SELECT e.vendor_id, SUM(IFNULL(e.paid, 0)) "
                . " as daybookpaid ,SUM(IFNULL(e.amount, 0))  as daybookcredit "
                . " FROM {$tblpx}expenses e "
                . " WHERE e.type = 73 AND (" . $this->getCompanyCondition("e") . ") "
                . " GROUP BY e.vendor_id) b "
                . " ON b.vendor_id = v.vendor_id"
                . " LEFT JOIN (SELECT d.vendor_id, SUM(IFNULL(d.amount, 0) + IFNULL(d.tax_amount, 0))"
                . " as vendorpaid FROM {$tblpx}dailyvendors d "
                . " WHERE (" . $this->getCompanyCondition("d") . ")  "
                . " GROUP BY d.vendor_id) c ON c.vendor_id = v.vendor_id"
                . " WHERE (" . $this->getCompanyCondition("v") . ") "
                . " AND v.vendor_type != 2";
            $miscellaneousSqlTotal       = "SELECT SUM(a.billtotal) as billsum, SUM(b.daybookpaid) "
                . " as daybooksum, SUM(c.vendorpaid) as vendorsum "
                . " FROM {$tblpx}vendors v"
                . " LEFT JOIN (SELECT p.vendor_id, SUM(IFNULL(b.bill_totalamount, 0))"
                . " as billtotal FROM {$tblpx}bills b LEFT JOIN {$tblpx}purchase p "
                . " ON b.purchase_id = p.p_id "
                . " WHERE (" . $this->getCompanyCondition("b") . ") " .$datecondition3
                . " GROUP BY p.vendor_id) a ON a.vendor_id = v.vendor_id"
                . " LEFT JOIN (SELECT e.vendor_id, SUM(IFNULL(e.paid, 0)) "
                . " as daybookpaid FROM {$tblpx}expenses e "
                . " WHERE e.type = 73 AND (" . $this->getCompanyCondition("e") . ") ".$datecondition1
                . " GROUP BY e.vendor_id) b ON b.vendor_id = v.vendor_id"
                . " LEFT JOIN (SELECT d.vendor_id, SUM(IFNULL(d.amount, 0) + IFNULL(d.tax_amount, 0))"
                . " as vendorpaid FROM {$tblpx}dailyvendors d "
                . " WHERE (" . $this->getCompanyCondition("d") . ") ".$datecondition2
                . " GROUP BY d.vendor_id) c ON c.vendor_id = v.vendor_id"
                . " WHERE (" . $this->getCompanyCondition("v") . ") AND v.vendor_type = 2";
            $paymentData    = Yii::app()->db->createCommand($sqlData)->queryAll();
            //echo "<pre>";print_r($paymentData);exit;
            //die($miscellaneous_sql_data);
            
            $miscellaneouspaymentData    = Yii::app()->db->createCommand($miscellaneous_sql_data)->queryAll();
            $paymentSum     = Yii::app()->db->createCommand($sqlTotal)->queryRow();
            $miscellaneouspaymentSum     = Yii::app()->db->createCommand($miscellaneousSqlTotal)->queryRow();
            
            $daybookPayment = array();
            $daybookPayment1 = array();
            $daybookPayment_pending = array();
            $model          = "";
            $dailyvPayment  = array();
            $dailyvPayment1  = array();
            $expreceipt_return = array();
            $expreceipt_return1 = array();
            $reconciliation_data = array();
            $reconciliation_data1 = array();
            $defect_return = array();
        }
        $render_datas = array(
            'model' => $model,
            'vendor_id'     =>  $vendor_id,
            'company_id' => $company_id,
            'paymentData'       => $paymentData,
            'paymentSum'      => $paymentSum,
            'date_from'        => $date_from,
            'date_to'   => $date_to,
            'daybookPayment'    => $daybookPayment,
            'daybookPayment1' => $daybookPayment1,
            'dailyvPayment' => $dailyvPayment,
            'dailyvPayment1'  => $dailyvPayment1,
            'miscellaneouspaymentData' => $miscellaneouspaymentData,
            'miscellaneouspaymentSum' => $miscellaneouspaymentSum,
            'expreceipt_return' => $expreceipt_return,
            'expreceipt_return1' => $expreceipt_return1,
            'reconciliation_data' => $reconciliation_data,
            'reconciliation_data1' => $reconciliation_data1,
            'daybookPayment_pending ' => $daybookPayment_pending,
            'defect_return'=>$defect_return,
            'total_vendor_Amount'=>$total_vendor_Amount,//Invoice AMount
        );
        if (filter_input(INPUT_GET, 'export') == 'pdf') {
            $this->logo = $this->realpath_logo;
            $mPDF1 = Yii::app()->ePdf->mPDF();
            $mPDF1 = Yii::app()->ePdf->mPDF('', 'A4');
            $sql = 'SELECT template_name '
                . 'FROM jp_quotation_template '
                . 'WHERE status="1"';
            $selectedtemplate =Yii::app()->db->createCommand($sql)->queryScalar();
            
            $template = $this->getTemplate($selectedtemplate);
            $mPDF1->SetHTMLHeader($template['header']);
            $mPDF1->SetHTMLFooter($template['footer']);
            $mPDF1->AddPage('','', '', '', '', 0,0,50, 40, 10,0);
            $mPDF1->WriteHTML($this->renderPartial('paymentreport', $render_datas, true));
            if ($_GET["view_type"] == "ledger") {
                $mPDF1->Output('LEDGER VIEW.pdf', 'D');
            } else {
                $mPDF1->Output('VENDOR-PAYMENT-REPORT.pdf', 'D');                
            }
        } else {
            if(isset($_REQUEST['aging']) && $_REQUEST['aging']==1){
                $where = ' where 1=1 ';
                $date_from = '';
                $date_to = '';
                $vendor_id  = '';
                $day_interval = '';
                $day_range = '';
                if (isset($_POST['Bills'])) {           

                    $date_from   = $_POST['Bills']['date_from'];
                    $date_to     = $_POST['Bills']['date_to'];
                    $pro_id      = $_POST['Bills']['project'];
                    $vendor_id   = $_POST['Bills']['vendor'];
                    $pu_num      = $_POST['Bills']['pu_num'];
                    $bill_num    = $_POST['Bills']['bill_num'];
                    $company_id  = $_POST['company_id'];
                    $day_interval = $_POST['Bills']['day_interval'];
                    $day_range =  isset($_POST['Bills']['day_range'])?$_POST['Bills']['day_range']:"";
                    $item_spec   = '';
                   
                  
                    if (!empty($day_interval)) {
                        $day_range = "";
                        $date_from = "";
                        $date_to = "";
                        $where .= " AND bill_date < DATE_SUB(NOW(), INTERVAL $day_interval DAY)";
                    }
        
                    if (!empty($day_range)) {
                        $day_interval = "";
                        $date_from = "";
                        $date_to = "";
                        if($day_range == 1){
                            $where .= " AND bill_date < DATE_SUB(NOW(), INTERVAL 1 DAY) AND bill_date > DATE_SUB(NOW(), INTERVAL 15 DAY) ";
                        }else if($day_range == 2){
                            $where .= " AND bill_date < DATE_SUB(NOW(), INTERVAL 15 DAY) AND bill_date > DATE_SUB(NOW(), INTERVAL 30 DAY) ";
                        }else if($day_range == 3){
                            $where .= " AND bill_date < DATE_SUB(NOW(), INTERVAL 30 DAY) AND bill_date > DATE_SUB(NOW(), INTERVAL 45 DAY) ";
                        }else{
                            $where .= " AND bill_date < DATE_SUB(NOW(), INTERVAL 45 DAY)";
                        }               
                    }
                    if ($date_from != '' && $date_to != '') {
                        $day_interval = '';
                        $day_range = '';
                        $where .= " and date(bill_date) BETWEEN '" . $date_from . "' and '" . $date_to . "' ";
                    }
                   
                    if ($vendor_id != '') {
                        $where .= ' and ven.vendor_id = ' . $vendor_id;
                    }
                   
                    if (!empty($date_from) && empty($date_to)) {
                        $day_range = '';
                        $day_interval = '';
                        $where .= " AND date(bill_date) >= '" . $date_from . "'";
                    }
                    if (!empty($date_to) && empty($date_from)) {
                        $day_range = '';
                        $day_interval = '';
                        $where .= " AND date(bill_date) <= '" . $date_to . "'";
                    }
                    
                    if (!empty($company_id)) {
                        $where .= " AND bill.company_id=" . $company_id . " ";
                    } else {
                        $where .= " AND (" . $newQuery . ")";
                    }
                   
                   
                } else {
                    $date_from = date("Y-m-") . "01";
                    $date_to = date("Y-m-d");
                    if (!empty($date_from)) {
                        $where .= " AND date(bill_date) >= '" . $date_from . "'";
                    }
                    if (!empty($date_to)) {
                        $where .= " AND date(bill_date) <= '" . $date_to . "'";
                    }
                }
        
                $bill_list = array();
        $bills = "";
        $condition = "";
        $command = Yii::app()->db->createCommand()->select('b.bill_id as billId');
        $command->from($this->tableNameAcc('expenses', 0) . ' as e');
        $command->leftJoin($this->tableNameAcc('bills', 0) . ' as b', 'e.bill_id = b.bill_id ');
        $command->where("e.type = 73 AND b.bill_totalamount IS NOT NULL AND b.purchase_id IS NOT NULL ");
        $billsArray = $command->queryAll();

        if (!empty($billsArray)) {
            foreach ($billsArray as $entry) {
                if ($entry['billId'] != "") {
                    $bill_list[] = $entry['billId'];
                }
            }
            $bills = implode(',', $bill_list);
            $condition = "AND b.bill_id NOT IN ($bills)";
        }
        $sql = "(SELECT b.bill_id as bill_id,b.bill_number as bill_number,b.bill_date as bill_date,"
            . " v.name as name,v.payment_date_range as  payment_date_range,'vendor' as type "
            . " FROM jp_bills b LEFT JOIN jp_purchase p "
            . " ON b.purchase_id = p.p_id "
            . "LEFT JOIN jp_vendors v ON v.vendor_id = p.vendor_id  "
            . "WHERE 1=1 $condition) UNION ALL ( SELECT 
            sb.id AS bill_id,
            sb.bill_number AS bill_number,
            sb.date AS bill_date,
            s.subcontractor_name AS name,
            NULL AS payment_date_range,
            'subcontractor' AS type 
        FROM 
            jp_subcontractorbill sb 
        LEFT JOIN 
            jp_subcontractor_payment sp ON sp.sc_bill_id = sb.id 
        LEFT JOIN 
            jp_scquotation sq ON sq.scquotation_id = sb.scquotation_id
        LEFT JOIN 
            jp_subcontractor s ON s.subcontractor_id = sq.subcontractor_id 
        GROUP BY 
            sb.id,s.subcontractor_name
        HAVING 
            COALESCE(SUM(sp.amount), 0) = 0 OR COALESCE(SUM(sp.amount), 0) < MAX(sb.total_amount))";
            $model = Yii::app()->db->createCommand($sql)->queryAll();
            $dataProvider1   = new CSqlDataProvider($sql, array(
                'keyField' => 'bill_id',
                'id' => 'bill_id',          
                'totalItemCount' => count($model),          
                'pagination' => false,
            ));
                $this->render('vendoragingreport', array(
                    'model' => $model,
                    'dataProvider1' => $dataProvider1,
                    'date_from' => $date_from,
                    'date_to' => $date_to,
                    
                    'vendor_id' => $vendor_id,
                    
                    'company_id' => $company_id,
                    
                    
                    'day_interval'=>$day_interval,
                    'day_range'=>$day_range
                ));
            } else if(isset($_REQUEST['aging']) && $_REQUEST['aging']==2){
                $this->render('vendoragingreportbydate');
            } else{
                $this->render('paymentreport', $render_datas);
            }
            
        }
    }
    public function actionAgeingvendorReport(){
        //$where = ' where 1=1 ';
                $where ='';
                $scwhere= ' where 1=1 ';
                $date_from = '';
                
                $date_to = '';
               
                $vendor_id  = '';
                $day_interval = '';
                $day_range = '';
                $tblpx  = Yii::app()->db->tablePrefix;
                $user = Users::model()->findByPk(Yii::app()->user->id);
                $arrVal = explode(',', $user->company_id);
            $newQuery = "";
            $newQuery1= "";
            $type='';
            foreach ($arrVal as $arr) {
                if ($newQuery) $newQuery .= ' OR';
                if ($newQuery1) $newQuery1 .= ' OR';
                $newQuery .= " FIND_IN_SET('" . $arr . "', b.company_id)";
                $newQuery1 .= " FIND_IN_SET('" . $arr . "', sq.company_id)";
            }
                if (isset($_GET['Bills'])) {            

                    $date_from   = $_GET['Bills']['date_from'];
                    $date_to     = $_GET['Bills']['date_to'];
                    $type = isset($_GET['Bills']['client_type'])?$_GET['Bills']['client_type']:'';
                    $vendor_id   = isset($_GET['Bills']['vendor'])? $_GET['Bills']['vendor']:'';
                    $subcontractor_id  = isset($_GET['Bills']['subcontractor_id'])?$_GET['Bills']['subcontractor_id']:"";
                    
                    
                    $day_interval = isset($_GET['Bills']['day_interval']) ?$_GET['Bills']['day_interval']:'' ;
                    $day_range =  isset($_GET['Bills']['day_range'])?$_GET['Bills']['day_range']:"";
                    $item_spec   = '';
                   
                  
                    if (!empty($day_interval)) {
                        $day_range = "";
                        $date_from = "";
                        $date_to = "";
                        $where .= " AND b.bill_date < DATE_SUB(NOW(), INTERVAL $day_interval DAY)";
                        $scwhere .= " AND sb.date < DATE_SUB(NOW(), INTERVAL $day_interval DAY)";
                    }
        
                    if (!empty($day_range)) {
                        $day_interval = "";
                        $date_from = "";
                        $date_to = "";
                        if($day_range == 1){
                            $where .= " AND b.bill_date < DATE_SUB(NOW(), INTERVAL 1 DAY) AND b.bill_date > DATE_SUB(NOW(), INTERVAL 15 DAY) ";
                            $scwhere .= " AND sb.date < DATE_SUB(NOW(), INTERVAL 1 DAY) AND sb.date > DATE_SUB(NOW(), INTERVAL 15 DAY) ";
                        }else if($day_range == 2){
                            $where .= " AND b.bill_date < DATE_SUB(NOW(), INTERVAL 15 DAY) AND b.bill_date > DATE_SUB(NOW(), INTERVAL 30 DAY) ";
                            $scwhere .= " AND sb.date < DATE_SUB(NOW(), INTERVAL 15 DAY) AND sb.date > DATE_SUB(NOW(), INTERVAL 30 DAY) ";
                        
                        }else if($day_range == 3){
                            $where .= " AND b.bill_date < DATE_SUB(NOW(), INTERVAL 30 DAY) AND b.bill_date > DATE_SUB(NOW(), INTERVAL 45 DAY) ";
                            $scwhere .= " AND sb.date < DATE_SUB(NOW(), INTERVAL 30 DAY) AND sb.date > DATE_SUB(NOW(), INTERVAL 45 DAY) ";
                        
                        }else{
                            $where .= " AND b.bill_date < DATE_SUB(NOW(), INTERVAL 45 DAY)";
                            $scwhere  .= " AND sb.date < DATE_SUB(NOW(), INTERVAL 45 DAY)";
                        
                        }               
                    }
                    if ($date_from != '' && $date_to != '') {
                        $day_interval = '';
                        $day_range = '';
                        $where .= " and b.bill_date BETWEEN '" . $date_from . "' and '" . $date_to . "' ";
                        $scwhere .=" and sb.date BETWEEN '" . $date_from . "' and '" . $date_to . "' ";
                    }
                   
                    if ($vendor_id != '') {
                        $where .= ' and v.vendor_id = ' . $vendor_id;
                    }
                    if ($subcontractor_id != '') {
                        $scwhere .= ' and s.subcontractor_id = ' . $subcontractor_id;
                    }
                   
                    if (!empty($date_from) && empty($date_to)) {
                        $day_range = '';
                        $day_interval = '';
                        $where .= " AND b.bill_date >= '" . $date_from . "'";
                         $scwhere .=" AND sb.date >= '" . $date_from . "'";
                    }
                    if (!empty($date_to) && empty($date_from)) {
                        $day_range = '';
                        $day_interval = '';
                        $where .= " AND b.bill_date <= '" . $date_to . "'";
                        $scwhere  .= " AND sb.date <= '" . $date_to . "'";
                    }
                    
                    if (!empty($company_id)) {
                        $where .= " AND b.company_id=" . $company_id . " ";
                        $scwhere .= " AND sq.company_id=" . $company_id . " ";
                    } else {
                        $where .= " AND (" . $newQuery . ")";
                        $scwhere .= " AND (" . $newQuery1 . ")";
                    }
                   
                   
                } else {
                    $date_from = date("Y-m-") . "01";
                    $date_to = date("Y-m-d");
                    if (!empty($date_from)) {
                        $where .= " AND b.bill_date >= '" . $date_from . "'";
                        $scwhere .= " AND sb.date >= '" . $date_from . "'";
                    }
                    if (!empty($date_to)) {
                        $where .= " AND b.bill_date <= '" . $date_to . "'";
                        $scwhere .= " AND sb.date <= '" . $date_to . "'";
                    }
                }
        
                $bill_list = array();
                $bills = "";
                $condition = "";
                $command = Yii::app()->db->createCommand()->select('b.bill_id as billId');
                $command->from($this->tableNameAcc('expenses', 0) . ' as e');
                $command->leftJoin($this->tableNameAcc('bills', 0) . ' as b', 'e.bill_id = b.bill_id ');
                $command->where("e.type = 73 AND b.bill_totalamount IS NOT NULL AND b.purchase_id IS NOT NULL ");
                $billsArray = $command->queryAll();

                if (!empty($billsArray)) {
                    foreach ($billsArray as $entry) {
                        if ($entry['billId'] != "") {
                            $bill_list[] = $entry['billId'];
                        }
                    }
                    $bills = implode(',', $bill_list);
                    $condition = "AND b.bill_id NOT IN ($bills)";
                }
        
            $sql = "(SELECT b.bill_id as bill_id,b.bill_number as bill_number,b.bill_date as bill_date,"
                . " v.name as name,v.payment_date_range as  payment_date_range,'vendor' as type "
                . " FROM jp_bills b LEFT JOIN jp_purchase p "
                . " ON b.purchase_id = p.p_id "
                . "LEFT JOIN jp_vendors v ON v.vendor_id = p.vendor_id  "
                . "WHERE 1=1 $condition $where) UNION ALL ( SELECT 
                sb.id AS bill_id,
                sb.bill_number AS bill_number,
                sb.date AS bill_date,
                s.subcontractor_name AS name,
                NULL AS payment_date_range,
                'subcontractor' AS type 
                FROM 
                    jp_subcontractorbill sb 
                LEFT JOIN 
                    jp_subcontractor_payment sp ON sp.sc_bill_id = sb.id 
                LEFT JOIN 
                    jp_scquotation sq ON sq.scquotation_id = sb.scquotation_id
                LEFT JOIN 
                    jp_subcontractor s ON s.subcontractor_id = sq.subcontractor_id  $scwhere 
                GROUP BY 
                    sb.id,s.subcontractor_name
                HAVING 
                    COALESCE(SUM(sp.amount), 0) = 0 OR COALESCE(SUM(sp.amount), 0) < MAX(sb.total_amount))";
                    if($type== 1){
                        $sql = "SELECT b.bill_id as bill_id,b.bill_number as bill_number,b.bill_date as bill_date,"
                        . " v.name as name,v.payment_date_range as  payment_date_range,'vendor' as type "
                        . " FROM jp_bills b LEFT JOIN jp_purchase p "
                        . " ON b.purchase_id = p.p_id "
                        . "LEFT JOIN jp_vendors v ON v.vendor_id = p.vendor_id  "
                        . "WHERE 1=1 $condition $where";
                    }else if($type== 2){
                        $sql = "(SELECT 
                sb.id AS bill_id,
                sb.bill_number AS bill_number,
                sb.date AS bill_date,
                s.subcontractor_name AS name,
                NULL AS payment_date_range,
                'subcontractor' AS type 
                FROM 
                    jp_subcontractorbill sb 
                LEFT JOIN 
                    jp_subcontractor_payment sp ON sp.sc_bill_id = sb.id 
                LEFT JOIN 
                    jp_scquotation sq ON sq.scquotation_id = sb.scquotation_id
                LEFT JOIN 
                    jp_subcontractor s ON s.subcontractor_id = sq.subcontractor_id  $scwhere 
                GROUP BY 
                    sb.id,s.subcontractor_name
                HAVING 
                    COALESCE(SUM(sp.amount), 0) = 0 OR COALESCE(SUM(sp.amount), 0) < MAX(sb.total_amount))";
                }
                    $model = Yii::app()->db->createCommand($sql)->queryAll();
                $aging_array = Yii::app()->db->createCommand($sql)->queryAll();
                // die($sql);
                $dataProvider1   = new CSqlDataProvider($sql, array(
                    'keyField' => 'bill_id',
                    'id' => 'bill_id',          
                    'totalItemCount' => count($model),          
                    'pagination' => false,
                ));
                $this->render('vendoragingreport', array(
                    'model' => $model,
                    'dataProvider1' => $dataProvider1,
                    'date_from' => $date_from,
                    'date_to' => $date_to,
                    'aging_array'=>$aging_array,
                    'vendor_id' => $vendor_id,
                                    
                                     
                    'day_interval'=>$day_interval,
                    'day_range'=>$day_range
                ));

    }

    public function setDaybookData($vendor_id, $daybook_entries, $condition)
    {
        $bills = '';
        if (!empty($daybook_entries)) {
            $bill_list = array();

            foreach ($daybook_entries as $entry) {
                if ($entry['bill_id'] != "") {
                    $bill_list[] = $entry['bill_id'];
                }
            }

            $bills = implode(',', $bill_list);
            // echo "<pre>";
            // print_r($bills);
            // exit;
            $bills = rtrim($bills, ',');
            $bills = ltrim($bills, ',');
        }
        $criteria = new CDbCriteria();
        $criteria->select = 't.*,p.*';
        $criteria->join = 'LEFT JOIN ' . $this->tableName('Purchase') . ' p ON t.purchase_id = p.p_id';
        $criteria->condition = 'p.vendor_id = "' . $vendor_id . '"';
        if (!empty($bills)) {
            // die("1");
            $criteria->condition .= ' AND t.bill_id NOT IN (' . $bills . ')';
        }
        if (!empty($condition)) {
            $criteria->condition .= $condition;
        }

        $bills = Bills::model()->findAll($criteria);

        $result_data = array();
        $amount = $additional_amount = 0;
        foreach ($bills as $bill) {
            if (!array_key_exists($bill['project_id'], $result_data)) {
                $amount = $bill['bill_totalamount'];
                $additional_amount = $bill['bill_additionalcharge'];
                $project_model = Projects::model()->findByPk($bill['project_id']);
                $result_data[$bill['project_id']]['project_name'] = $project_model->name;
                $result_data[$bill['project_id']]['total_amount'] = $amount;
                $result_data[$bill['project_id']]['additional_tot_amount'] = $additional_amount;
                $result_data[$bill['project_id']]['datas'][] = $bill->attributes;
            } else {

                $amount =  $result_data[$bill['project_id']]['total_amount'] + $bill['bill_totalamount'];
                $additional_amount += $bill['bill_additionalcharge'];
                $result_data[$bill['project_id']]['total_amount'] = $amount;
                $result_data[$bill['project_id']]['additional_tot_amount'] = $additional_amount;
                array_push($result_data[$bill['project_id']]['datas'], $bill->attributes);
            }
        }
        return $result_data;
    }

    public function actionSavePaymentreport($vendor_id, $company_id)
    {

        $tblpx      = Yii::app()->db->tablePrefix;
        if (!empty($_REQUEST['company_id'])) {
            $company_id = $_REQUEST['company_id'];
        } else {
            $company_id = "";
        }
        $model      = Vendors::model()->findByPk($vendor_id);
        $date_from  = $_REQUEST['date_from'];
        $date_to    = $_REQUEST['date_to'];
        if (!empty($date_from) && !empty($date_to)) {
            $condition1 = " AND ex.date BETWEEN '" . date("Y-m-d", strtotime($date_from)) . "' AND '" . date("Y-m-d", strtotime($date_to)) . "'";
            $condition2 = " AND date BETWEEN '" . date("Y-m-d", strtotime($date_from)) . "' AND '" . date("Y-m-d", strtotime($date_to)) . "'";
            $condition3 = " AND bill_date BETWEEN '" . date("Y-m-d", strtotime($date_from)) . "' AND '" . date("Y-m-d", strtotime($date_to)) . "'";
        } else if (!empty($date_from) && empty($date_to)) {
            $condition1 = " AND ex.date >= '" . date("Y-m-d", strtotime($date_from)) . "'";
            $condition2 = " AND date >= '" . date("Y-m-d", strtotime($date_from)) . "'";
            $condition3 = " AND bill_date >= '" . date("Y-m-d", strtotime($date_from)) . "'";
        } else if (empty($date_from) && !empty($date_to)) {
            $condition1 = " AND ex.date <= '" . date("Y-m-d", strtotime($date_to)) . "'";
            $condition2 = " AND date <= '" . date("Y-m-d", strtotime($date_to)) . "'";
            $condition3 = " AND bill_date <= '" . date("Y-m-d", strtotime($date_to)) . "'";
        } else {
            $condition1 = "";
            $condition2 = "";
            $condition3 = "";
        }
        $user       = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal     = explode(',', $user->company_id);
        $newQuery1  = "";
        $newQuery2  = "";
        $newQuery3  = "";
        if (!empty($_POST['company_id'])) {
            $condition1 .= " AND ex.company_id = " . $_POST['company_id'] . "";
            $condition2 .= " AND company_id = " . $_POST['company_id'] . "";
            $condition3 .= " AND t.company_id = " . $_POST['company_id'] . "";
        } else {
            foreach ($arrVal as $arr) {
                if ($newQuery1) $newQuery1 .= ' OR';
                if ($newQuery2) $newQuery2 .= ' OR';
                if ($newQuery3) $newQuery3 .= ' OR';
                $newQuery1 .= " FIND_IN_SET('" . $arr . "', ex.company_id)";
                $newQuery2 .= " FIND_IN_SET('" . $arr . "', company_id)";
                $newQuery3 .= " FIND_IN_SET('" . $arr . "', t.company_id)";
            }
            $condition1 .= " AND (" . $newQuery1 . ")";
            $condition2 .= " AND (" . $newQuery2 . ")";
            $condition3 .= " AND (" . $newQuery3 . ")";
        }
        Yii::app()->db->createCommand('SET SQL_BIG_SELECTS=1')->execute();
        $daybookSql     = "SELECT p.name as projectname, a.projectcount, b.totalamount, c.totalpaid, d.totaltds, e.totalpaidtovendor, f.sumtotalamount, g.sumtotalpaid, h.sumtotaltds, i.sumtotalpaidtovendor, bl.bill_number as billnumber, bl.bill_totalamount, ex.* FROM {$tblpx}expenses ex
                            LEFT JOIN {$tblpx}projects p ON ex.projectid = p.pid
                            LEFT JOIN {$tblpx}bills bl ON ex.bill_id = bl.bill_id
                            LEFT JOIN (SELECT projectid, count(*) as projectcount FROM {$tblpx}expenses WHERE type = 73 AND vendor_id = {$vendor_id}" . $condition2 . " GROUP BY projectid) a ON a.projectid = ex.projectid
                            LEFT JOIN (SELECT projectid, SUM(amount) as totalamount FROM {$tblpx}expenses WHERE type = 73 AND vendor_id = {$vendor_id}" . $condition2 . " GROUP BY projectid) b ON b.projectid = ex.projectid
                            LEFT JOIN (SELECT projectid, SUM(paid) as totalpaid FROM {$tblpx}expenses WHERE type = 73 AND vendor_id = {$vendor_id}" . $condition2 . " GROUP BY projectid) c ON c.projectid = ex.projectid
                            LEFT JOIN (SELECT projectid, SUM(expense_tds) as totaltds FROM {$tblpx}expenses WHERE type = 73 AND vendor_id = {$vendor_id}" . $condition2 . " GROUP BY projectid) d ON d.projectid = ex.projectid
                            LEFT JOIN (SELECT projectid, SUM(paidamount) as totalpaidtovendor FROM {$tblpx}expenses WHERE type = 73 AND vendor_id = {$vendor_id}" . $condition2 . " GROUP BY projectid) e ON e.projectid = ex.projectid
                            LEFT JOIN (SELECT projectid, SUM(amount) as sumtotalamount FROM {$tblpx}expenses WHERE type = 73 AND vendor_id = {$vendor_id}" . $condition2 . ") f ON 1 = 1
                            LEFT JOIN (SELECT projectid, SUM(paid) as sumtotalpaid FROM {$tblpx}expenses WHERE type = 73 AND vendor_id = {$vendor_id}" . $condition2 . ") g ON 1 = 1
                            LEFT JOIN (SELECT projectid, SUM(expense_tds) as sumtotaltds FROM {$tblpx}expenses WHERE type = 73 AND vendor_id = {$vendor_id}" . $condition2 . ") h ON 1 = 1
                            LEFT JOIN (SELECT projectid, SUM(paidamount) as sumtotalpaidtovendor FROM {$tblpx}expenses WHERE type = 73 AND vendor_id = {$vendor_id}" . $condition2 . ") i ON 1 = 1
                            WHERE ex.type = 73 AND ex.vendor_id = {$vendor_id}" . $condition1 . " ORDER BY ex.projectid";
        $dailyvSql      = "SELECT ex.*, a.sumtotalamount, a.sumtdsamount, a.sumpaidamount FROM {$tblpx}dailyvendors ex
                            LEFT JOIN (SELECT SUM(IFNULL(amount, 0) + IFNULL(tax_amount, 0)) as sumtotalamount, SUM(IFNULL(tds_amount, 0)) as sumtdsamount, SUM(IFNULL(paidamount, 0)) as sumpaidamount FROM {$tblpx}dailyvendors WHERE vendor_id = {$vendor_id}" . $condition2 . ") a ON 1 = 1
                            WHERE ex.vendor_id = {$vendor_id}" . $condition1 . "";
        $daybookPayment = Yii::app()->db->createCommand($daybookSql)->queryAll();
        $dailyvPayment  = Yii::app()->db->createCommand($dailyvSql)->queryAll();
        $paymentData = $this->setDaybookData($vendor_id, $daybookPayment, $condition3);
        $mPDF1 = Yii::app()->ePdf->mPDF();
        $mPDF1 = Yii::app()->ePdf->mPDF('', 'A4');
        $mPDF1->WriteHTML($stylesheet, 1);
        $mPDF1->WriteHTML($this->renderPartial('vendorpayment', array(
            'vendor_id' => $vendor_id, 'model' => $model, 'company_id' => $company_id, 'date_from' => $date_from, 'date_to' => $date_to, 'daybookPayment' => $daybookPayment, 'dailyvPayment' => $dailyvPayment, 'paymentData' => $paymentData
        ), true));
        $mPDF1->Output('Vendorpayment_' . $model["name"] . '.pdf', 'D');
    }

    public function actionProgressSave()
    {

        $tblpx = Yii::app()->db->tablePrefix;
        $userid = Yii::app()->user->id;

        if ($userid != "") {
            $vend_id = '';
            $type_id = '';
            $bank_id = '';
            $vendor = $_REQUEST['vendor'];
            $vendor_id = Yii::app()->db->createCommand("select `vendor_id` from " . $tblpx . "vendors where `name`='$vendor'")->queryAll();

            foreach ($vendor_id as $vend) {
                $vend_id = $vend['vendor_id'];
            }

            //print_r($_REQUEST);

            $type = Yii::app()->db->createCommand("select `sid` from " . $tblpx . "status where `caption`='" . $_REQUEST['type'] . "'")->queryAll();
            foreach ($type as $ty) {
                $type_id = $ty['sid'];
            }

            $bank = Yii::app()->db->createCommand("select `bank_id` from " . $tblpx . "bank where `bank_name`='" . $_REQUEST['bank'] . "'")->queryAll();
            foreach ($bank as $bk) {
                $bank_id = $bk['bank_id'];
            }


            if (!empty($_REQUEST['amount']) && $_REQUEST['amount'] > 0) {
                $vendor_model = new Dailyvendors();
                $vendor_model->vendor_id = $vend_id;
                $vendor_model->payment_type = $type_id;
                $vendor_model->bank = $bank_id;
                $vendor_model->cheque_no = $_REQUEST['cheque'];

                $vendor_model->description = $_REQUEST['desc'];
                $vendor_model->amount = $_REQUEST['amount'];
                $vendor_model->date = date("Y-m-d", strtotime($_REQUEST['date']));
                $vendor_model->created_by = Yii::app()->user->id;
                $vendor_model->created_date = date('Y-m-d H:i:s');
                $vendor_model->updated_by = Yii::app()->user->id;
                $vendor_model->save(false);
            } else {

                echo json_encode(array("result" => "fail"));
            }

            echo json_encode(array("result" => "ok", "id" => Yii::app()->db->getLastInsertId()));
        } else {
            echo json_encode(array("result" => "empty"));
        }
    }

    public function actionSavetoExcel($vendor_id, $company_id)
    {   
        if($vendor_id !=0)
        {
            $tblpx  = Yii::app()->db->tablePrefix;
        $date_from      = "";
        $date_to        = "";
        $total_vendor_Amount = 0;
        if (isset($_REQUEST['vendor_id']) && !empty($_REQUEST['vendor_id'])) {
            $vendor_id = $_REQUEST['vendor_id'];
            if (!empty($_REQUEST['company_id'])) {
                $company_id = $_REQUEST['company_id'];
            } else {
                $company_id = "";
            }
            $model      = Vendors::model()->findByPk($vendor_id);
            $date_from  = $_REQUEST['date_from'];
            $date_to    = $_REQUEST['date_to'];
            $condition1 = "";
            $condition2 = "";
            $condition3 = "";
            $condition4 = "";
            $condition5 = "";
            $condition6 = "";
            $condition7 = "";
            $condition8 = "";
            $condition6a = "";
            $condition9 = "";
            $condition10="";
            $total_vendor_Amount=0;
            if (!empty($date_from) && !empty($date_to)) {

                $condition1 = " AND ex.date BETWEEN '" . date("Y-m-d", strtotime($date_from)) . "' AND '" . date("Y-m-d", strtotime($date_to)) . "'";
                $condition2 = " AND date BETWEEN '" . date("Y-m-d", strtotime($date_from)) . "' AND '" . date("Y-m-d", strtotime($date_to)) . "'";
                $condition3 = " AND bill_date BETWEEN '" . date("Y-m-d", strtotime($date_from)) . "' AND '" . date("Y-m-d", strtotime($date_to)) . "'";
                $condition4 = " AND return_date BETWEEN '" . date("Y-m-d", strtotime($date_from)) . "' AND '" . date("Y-m-d", strtotime($date_to)) . "'";
                $condition9 = " AND b.bill_date BETWEEN '" . date("Y-m-d", strtotime($date_from)) . "' AND '" . date("Y-m-d", strtotime($date_to)) . "'";
                $condition10 = " AND e.date BETWEEN '" . date("Y-m-d", strtotime($date_from)) . "' AND '" . date("Y-m-d", strtotime($date_to)) . "'";
            } else if (!empty($date_from) && empty($date_to)) {

                $condition1 = " AND ex.date >= '" . date("Y-m-d", strtotime($date_from)) . "'";
                $condition2 = " AND date >= '" . date("Y-m-d", strtotime($date_from)) . "'";
                $condition3 = " AND bill_date >= '" . date("Y-m-d", strtotime($date_from)) . "'";
                $condition9 = " AND b.bill_date >= '" . date("Y-m-d", strtotime($date_from)) . "'";
                $condition10 = " AND e.date BETWEEN '" . date("Y-m-d", strtotime($date_from)) . "' AND '" . date("Y-m-d", strtotime($date_to)) . "'";
                
                $condition4 = " AND return_date >= '" . date("Y-m-d", strtotime($date_from)) . "'";
            } else if (empty($date_from) && !empty($date_to)) {

                $condition1 = " AND ex.date <= '" . date("Y-m-d", strtotime($date_to)) . "'";
                $condition2 = " AND date <= '" . date("Y-m-d", strtotime($date_to)) . "'";
                $condition9 = " AND b.bill_date <= '" . date("Y-m-d", strtotime($date_to)) . "'";
                $condition10 = " AND e.date BETWEEN '" . date("Y-m-d", strtotime($date_from)) . "' AND '" . date("Y-m-d", strtotime($date_to)) . "'";
                $condition3 = " AND bill_date <= '" . date("Y-m-d", strtotime($date_to)) . "'";
                $condition4 = " AND return_date <= '" . date("Y-m-d", strtotime($date_to)) . "'";
            } else {

                $condition1 = "";
                $condition2 = "";
                $condition3 = "";
                $condition4 = "";
                $condition5 = "";
                $condition6 = "";
                $condition7 = "";
                $condition8 = "";
                $condition6a = "";
                $condition9 = "";
                $condition10="";
            }
            $user       = Users::model()->findByPk(Yii::app()->user->id);
            $arrVal     = explode(',', $user->company_id);
            $newQuery1  = "";
            $newQuery2  = "";
            $newQuery3  = "";
            $newQuery4  = "";
            $newQuery5  = "";
            $newQuery6 = "";
            $newQuery7 = "";
            $newQuery8 = "";
            $newQuery6a = "";
            if (!empty($_REQUEST['company_id'])) {
                foreach ($arrVal as $arr) {
                    if ($newQuery1) $newQuery1 .= ' OR';
                    if ($newQuery2) $newQuery2 .= ' OR';
                    if ($newQuery3) $newQuery3 .= ' OR';
                    if ($newQuery4) $newQuery4 .= ' OR';
                    if ($newQuery5) $newQuery5 .= ' OR';
                    $newQuery1 .= " FIND_IN_SET('" . $arr . "', ex.company_id)";
                    $newQuery2 .= " FIND_IN_SET('" . $arr . "', company_id)";
                    $newQuery3 .= " FIND_IN_SET('" . $arr . "', t.company_id)";
                    $newQuery4 .= " FIND_IN_SET('" . $arr . "', r.company_id)";
                    $newQuery5 .= " FIND_IN_SET('" . $arr . "', r.company_id)";
                }
                $condition1 .= " AND ex.company_id = " . $_REQUEST['company_id'] . "";
                $condition2 .= " AND company_id = " . $_REQUEST['company_id'] . "";
                $condition3 .= " AND t.company_id = " . $_REQUEST['company_id'] . "";
                $condition4 .= " AND r.company_id = " . $_REQUEST['company_id'] . "";
                $condition5 .= " AND e.company_id = " . $_REQUEST['company_id'] . "";
            } else {
                foreach ($arrVal as $arr) {
                    if ($newQuery1) $newQuery1 .= ' OR';
                    if ($newQuery2) $newQuery2 .= ' OR';
                    if ($newQuery3) $newQuery3 .= ' OR';
                    if ($newQuery4) $newQuery4 .= ' OR';
                    if ($newQuery5) $newQuery5 .= ' OR';
                    $newQuery1 .= " FIND_IN_SET('" . $arr . "', ex.company_id)";
                    $newQuery2 .= " FIND_IN_SET('" . $arr . "', company_id)";
                    $newQuery3 .= " FIND_IN_SET('" . $arr . "', t.company_id)";
                    $newQuery4 .= " FIND_IN_SET('" . $arr . "', r.company_id)";
                    $newQuery5 .= " FIND_IN_SET('" . $arr . "', e.company_id)";
                }
                $condition1 .= " AND (" . $newQuery1 . ")";
                $condition2 .= " AND (" . $newQuery2 . ")";
                $condition3 .= " AND (" . $newQuery3 . ")";
                $condition4 .= " AND (" . $newQuery4 . ")";
                $condition5 .= " AND (" . $newQuery5 . ")";
            }
            Yii::app()->db->createCommand('SET SQL_BIG_SELECTS=1')->execute();
            // recon return
            $recon_return_sql1 = "SELECT r.*,e.* "
                . " FROM " . $tblpx . "reconciliation r "
                . " join " . $tblpx . "expenses e "
                . " on r.reconciliation_parentid =e.exp_id  "
                . " where  r.reconciliation_status='0' "
                . " and e.vendor_id='" . $vendor_id . "' "
                . " and r.reconciliation_payment ='Purchase Return'";
            $reconciliation_data = Yii::app()->db->createCommand($recon_return_sql1)->queryAll();

            //recon Dyabook summary 
            $recon_daysum_sql1 = "SELECT r.*,e.* "
                . " FROM " . $tblpx . "reconciliation r "
                . " join " . $tblpx . "expenses e "
                . " on r.reconciliation_parentid =e.exp_id  "
                . " where  r.reconciliation_status='0' "
                . " and e.vendor_id='" . $vendor_id . "' ";
            $reconciliation_data1 = Yii::app()->db->createCommand($recon_daysum_sql1)->queryAll();

            //direct v payment recon

            $recon_direct_sql1 = "SELECT r.*,d.* "
                . " FROM " . $tblpx . "reconciliation r "
                . " join " . $tblpx . "dailyvendors d "
                . " on r.reconciliation_parentid =d.daily_v_id  "
                . " where  r.reconciliation_status='0' "
                . " and d.vendor_id='" . $vendor_id . "' "
                . " and r.reconciliation_payment ='Vendor Payment'";
            $reconciliation_data2 = Yii::app()->db->createCommand($recon_direct_sql1)->queryAll();


            $cheque_array = array();
            foreach ($reconciliation_data as  $data) {
                $value = $data['cheque_no'];
                if ($value != '') {
                    $recon_bill = $value;
                    array_push($cheque_array, $recon_bill);
                }
            }
            $cheque_array1 = implode(',', $cheque_array);

            $cheque_array_day_sum = array();
            foreach ($reconciliation_data1 as  $data) {
                $value = $data['cheque_no'];
                if ($value != '') {
                    $recon_bill = "'{$value}'";
                    array_push($cheque_array_day_sum, $recon_bill);
                }
            }

            $cheque_array_day_sum1 = implode(',', $cheque_array_day_sum);

                $daybookSql1     = "SELECT p.name as projectname, a.projectcount,"
                    . "  b.totalamount, c.totalpaid, d.totaltds, "
                    . " e.totalpaidtovendor, f.sumtotalamount, g.sumtotalpaid, "
                    . " h.sumtotaltds, i.sumtotalpaidtovendor, bl.bill_number "
                    . " as billnumber, bl.bill_totalamount, ex.* "
                    . " FROM {$tblpx}expenses ex"
                    . " LEFT JOIN {$tblpx}projects p "
                    . "ON ex.projectid = p.pid"
                    . " LEFT JOIN {$tblpx}bills bl "
                    . " ON ex.bill_id = bl.bill_id"
                    . " LEFT JOIN (SELECT projectid, count(*) "
                    . " as projectcount FROM {$tblpx}expenses "
                    . " WHERE type = 73 AND vendor_id = {$vendor_id}" . $condition2 . " "
                    . " GROUP BY projectid) a "
                    . " ON a.projectid = ex.projectid"
                    . " LEFT JOIN (SELECT projectid, SUM(amount) "
                    . " as totalamount FROM {$tblpx}expenses "
                    . " WHERE type = 73 AND vendor_id = {$vendor_id}" . $condition2 . " "
                    . " AND reconciliation_status=0 "
                    . " GROUP BY projectid) b ON b.projectid = ex.projectid"
                    . " LEFT JOIN (SELECT projectid, SUM(paid) as totalpaid "
                    . " FROM {$tblpx}expenses WHERE type = 73 "
                    . " AND vendor_id = {$vendor_id}" . $condition2 . " AND  "
                    . " reconciliation_status=0 GROUP BY projectid) c "
                    . " ON c.projectid = ex.projectid"
                    . " LEFT JOIN (SELECT projectid, SUM(expense_tds) "
                    . " as totaltds FROM {$tblpx}expenses "
                    . " WHERE type = 73 AND vendor_id = {$vendor_id}" . $condition2 . " "
                    . " AND reconciliation_status=0 "
                    . " GROUP BY projectid) d "
                    . " ON d.projectid = ex.projectid"
                    . " LEFT JOIN (SELECT projectid, SUM(paidamount) "
                    . " as totalpaidtovendor FROM {$tblpx}expenses "
                    . " WHERE type = 73 AND vendor_id = {$vendor_id}" . $condition2 . ""
                    . "  AND reconciliation_status= 0 "
                    . " GROUP BY projectid) e ON e.projectid = ex.projectid"
                    . " LEFT JOIN (SELECT projectid, SUM(amount) as sumtotalamount "
                    . " FROM {$tblpx}expenses WHERE type = 73 AND "
                    . " vendor_id = {$vendor_id}" . $condition2 . " AND"
                    . " reconciliation_status=0) f ON 1 = 1"
                    . " LEFT JOIN (SELECT projectid, SUM(paid) "
                    . " as sumtotalpaid FROM {$tblpx}expenses "
                    . " WHERE type = 73 AND vendor_id = {$vendor_id}" . $condition2 . ""
                    . "  AND reconciliation_status=0) g"
                    . "  ON 1 = 1"
                    . " LEFT JOIN (SELECT projectid, SUM(expense_tds) "
                    . " as sumtotaltds FROM {$tblpx}expenses "
                    . " WHERE type = 73 AND vendor_id = {$vendor_id}" . $condition2 . " "
                    . " AND reconciliation_status=0) h ON 1 = 1"
                    . " LEFT JOIN (SELECT projectid, SUM(paidamount) "
                    . " as sumtotalpaidtovendor FROM {$tblpx}expenses "
                    . " WHERE type = 73 AND vendor_id = {$vendor_id}" . $condition2 . ""
                    . "  AND reconciliation_status=0) i ON 1 = 1"
                    . " WHERE ex.type = 73 AND ex.vendor_id = {$vendor_id}" . $condition1 . " AND ex.reconciliation_status=0 ORDER BY ex.projectid";
                $daybookPayment1 = Yii::app()->db->createCommand($daybookSql1)->queryAll();
                $daybookSql = "
                SELECT 
                p.name as projectname, 
                a.projectcount, 
                b.totalamount, 
                c.totalpaid, 
                d.totaltds, 
                e.totalpaidtovendor, 
                f.sumtotalamount,
                z.sumtotalvendoramount, 
                g.sumtotalpaid, 
                h.sumtotaltds, 
                i.sumtotalpaidtovendor, 
                bl.bill_number as billnumber, 
                bl.bill_totalamount, 
                y.sumtotalamountpaidtovendor,
                ex.* 
            FROM 
                {$tblpx}expenses ex
            LEFT JOIN {$tblpx}projects p ON ex.projectid = p.pid
            LEFT JOIN {$tblpx}bills bl ON ex.bill_id = bl.bill_id
            LEFT JOIN (
                SELECT 
                    projectid, 
                    COUNT(*) as projectcount 
                FROM 
                    {$tblpx}expenses 
                WHERE 
                    type = 73 AND vendor_id = {$vendor_id} {$condition2} 
                GROUP BY 
                    projectid
            ) a ON a.projectid = ex.projectid
            LEFT JOIN (
                SELECT 
                    projectid, 
                    SUM(amount) as totalamount 
                FROM 
                    {$tblpx}expenses 
                WHERE 
                    type = 73 AND vendor_id = {$vendor_id} {$condition2} 
                    AND (reconciliation_status=1 OR reconciliation_status IS NULL) 
                GROUP BY 
                    projectid
            ) b ON b.projectid = ex.projectid
            LEFT JOIN (
                SELECT 
                    projectid, 
                    SUM(paid) as totalpaid 
                FROM 
                    {$tblpx}expenses 
                WHERE 
                    type = 73 AND vendor_id = {$vendor_id} {$condition2} 
                    AND (reconciliation_status=1 OR reconciliation_status IS NULL) 
                GROUP BY 
                    projectid
            ) c ON c.projectid = ex.projectid
            LEFT JOIN (
                SELECT 
                    projectid, 
                    SUM(expense_tds) as totaltds 
                FROM 
                    {$tblpx}expenses 
                WHERE 
                    type = 73 AND vendor_id = {$vendor_id} {$condition2} 
                    AND (reconciliation_status=1 OR reconciliation_status IS NULL) 
                GROUP BY 
                    projectid
            ) d ON d.projectid = ex.projectid
            LEFT JOIN (
                SELECT 
                    projectid, 
                    SUM(paidamount) as totalpaidtovendor 
                FROM 
                    {$tblpx}expenses 
                WHERE 
                    expense_type != 0 AND type = 73 AND vendor_id = {$vendor_id} {$condition2} 
                    AND (reconciliation_status=1 OR reconciliation_status IS NULL) 
                GROUP BY 
                    projectid
            ) e ON e.projectid = ex.projectid
            LEFT JOIN (
                SELECT 
                    pu.project_id as projectid, 
                    SUM(b.bill_totalamount) as sumtotalamount
                FROM 
                    {$tblpx}bills b
                LEFT JOIN {$tblpx}purchase pu ON b.purchase_id = pu.p_id
                LEFT JOIN {$tblpx}projects p ON pu.project_id = p.pid
                LEFT JOIN {$tblpx}expenses e ON e.bill_id = b.bill_id
                WHERE 
                    pu.vendor_id = {$vendor_id} {$condition9}
                    AND (e.reconciliation_status=1 OR e.reconciliation_status IS NULL) AND e.type=73 
                GROUP BY 
                    pu.project_id
            ) f ON f.projectid = ex.projectid
            LEFT JOIN (
                SELECT 
                    unique_bills.projectid, 
                    SUM(unique_bills.bill_amount) as sumtotalvendoramount
                FROM (
                    SELECT 
                        DISTINCT e.bill_id, 
                        e.projectid, 
                        SUM(e.amount) as bill_amount
                    FROM 
                        {$tblpx}expenses e
                    WHERE 
                        e.vendor_id = {$vendor_id} {$condition10}  
                        AND (e.reconciliation_status=1 OR e.reconciliation_status IS NULL) 
                        AND e.type=73 
                    GROUP BY 
                        e.bill_id, e.projectid
                ) as unique_bills
                GROUP BY 
                    unique_bills.projectid
            ) z ON z.projectid = ex.projectid
            LEFT JOIN (
                SELECT 
                    projectid, 
                    SUM(paid) as sumtotalpaid 
                FROM 
                    {$tblpx}expenses 
                WHERE 
                    type = 73 AND vendor_id = {$vendor_id} {$condition2} 
                    AND (reconciliation_status=1 OR reconciliation_status IS NULL) 
                GROUP BY 
                    projectid
            ) g ON g.projectid = ex.projectid
            LEFT JOIN (
                SELECT 
                    projectid, 
                    SUM(expense_tds) as sumtotaltds 
                FROM 
                    {$tblpx}expenses 
                WHERE 
                    type = 73 AND vendor_id = {$vendor_id} {$condition2} 
                    AND (reconciliation_status=1 OR reconciliation_status IS NULL) 
                GROUP BY 
                    projectid
            ) h ON h.projectid = ex.projectid
            LEFT JOIN (
                SELECT 
                    projectid, 
                    SUM(paidamount) as sumtotalpaidtovendor 
                FROM 
                    {$tblpx}expenses 
                WHERE 
                    expense_type != 0 AND type = 73 AND vendor_id = {$vendor_id} {$condition2} 
                    AND (reconciliation_status=1 OR reconciliation_status IS NULL) 
                GROUP BY 
                    projectid
            ) i ON i.projectid = ex.projectid
            LEFT JOIN (
                SELECT 
                    projectid, 
                    SUM(paidamount) as sumtotalamountpaidtovendor 
                FROM 
                    {$tblpx}expenses 
                WHERE 
                    expense_type != 0 AND type = 73 AND expense_type != '107' AND vendor_id = {$vendor_id} {$condition2} 
                    AND (reconciliation_status=1 OR reconciliation_status IS NULL) 
                GROUP BY 
                    vendor_id
            ) y ON y.projectid = ex.projectid
            WHERE 
                ex.type = 73 
                AND ex.vendor_id = {$vendor_id} {$condition1} 
                AND (ex.reconciliation_status=1 OR ex.reconciliation_status IS NULL) 
            ORDER BY 
                ex.projectid";

            $daybook_total_sql ="SELECT 
                DISTINCT e.bill_id, e.exp_id,
                e.projectid
                FROM 
                {$tblpx}expenses e
            WHERE 
                e.vendor_id = {$vendor_id} {$condition10}   
                AND (e.reconciliation_status=1 OR e.reconciliation_status IS NULL) 
                AND e.type=73 
            GROUP BY 
                e.bill_id, e.projectid";   
                  $daybookPayment = Yii::app()->db->createCommand($daybookSql)->queryAll();
        
            //echo "<pre>";print_r($daybookPayment);exit;
            $daybook_total = Yii::app()->db->createCommand($daybook_total_sql)->queryAll();
                $total_vendor_Amount =0;
                if(!empty($daybook_total)){
                    foreach($daybook_total as $daybook_total_entries){
                        $exp_model = Expenses::Model()->findByPk($daybook_total_entries["exp_id"]);
                        if(!empty($exp_model)){
                            $total_vendor_Amount += $exp_model->amount;
                        }
                    }
                }
    
            //die($total_vendor_Amount);

              
            $daybookSql_pending     = "SELECT p.name as projectname, a.projectcount,"
                . " b.totalamount, c.totalpaid, d.totaltds, e.totalpaidtovendor, "
                . " f.sumtotalamount, g.sumtotalpaid, h.sumtotaltds, i.sumtotalpaidtovendor,"
                . "  bl.bill_number as billnumber, bl.bill_totalamount, ex.* "
                . " FROM {$tblpx}expenses ex"
                . " LEFT JOIN {$tblpx}projects p ON ex.projectid = p.pid"
                . " LEFT JOIN {$tblpx}bills bl ON ex.bill_id = bl.bill_id"
                . " LEFT JOIN (SELECT projectid, count(*) as projectcount "
                . " FROM {$tblpx}expenses WHERE type = 73 "
                . " AND vendor_id = {$vendor_id}" . $condition2 . " "
                . " GROUP BY projectid) a ON a.projectid = ex.projectid"
                . " LEFT JOIN (SELECT projectid, SUM(amount) as totalamount "
                . " FROM {$tblpx}expenses WHERE type = 73 "
                . " AND vendor_id = {$vendor_id}" . $condition2 . " GROUP BY projectid) b"
                . " ON b.projectid = ex.projectid"
                . " LEFT JOIN (SELECT projectid, SUM(paid) as totalpaid "
                . " FROM {$tblpx}expenses WHERE type = 73 "
                . " AND vendor_id = {$vendor_id}" . $condition2 . " GROUP BY projectid) c "
                . " ON c.projectid = ex.projectid"
                . " LEFT JOIN (SELECT projectid, SUM(expense_tds) as totaltds "
                . " FROM {$tblpx}expenses WHERE type = 73 "
                . " AND vendor_id = {$vendor_id}" . $condition2 . " "
                . " GROUP BY projectid) d ON d.projectid = ex.projectid"
                . " LEFT JOIN (SELECT projectid, SUM(paidamount) as totalpaidtovendor "
                . " FROM {$tblpx}expenses WHERE type = 73 "
                . " AND vendor_id = {$vendor_id}" . $condition2 . " "
                . " GROUP BY projectid) e ON e.projectid = ex.projectid"
                . " LEFT JOIN (SELECT projectid, SUM(amount) as sumtotalamount "
                . " FROM {$tblpx}expenses WHERE type = 73 "
                . " AND vendor_id = {$vendor_id}" . $condition2 . ") f ON 1 = 1"
                . " LEFT JOIN (SELECT projectid, SUM(paid) as sumtotalpaid "
                . " FROM {$tblpx}expenses WHERE type = 73 "
                . " AND vendor_id = {$vendor_id}" . $condition2 . ") g ON 1 = 1"
                . " LEFT JOIN (SELECT projectid, SUM(expense_tds) as sumtotaltds "
                . " FROM {$tblpx}expenses WHERE type = 73 "
                . " AND vendor_id = {$vendor_id}" . $condition2 . ") h ON 1 = 1"
                . " LEFT JOIN (SELECT projectid, SUM(paidamount) as sumtotalpaidtovendor "
                . " FROM {$tblpx}expenses WHERE type = 73 "
                . " AND vendor_id = {$vendor_id}" . $condition2 . ") i ON 1 = 1"
                . " WHERE ex.type = 73 AND ex.vendor_id = {$vendor_id}" . $condition1 . ""
                . "  ORDER BY ex.projectid";
            $daybookPayment_pending = Yii::app()->db->createCommand($daybookSql_pending)->queryAll();

            $cheque_direct_array = array();
            foreach ($reconciliation_data2 as  $data) {
                $value = "'{$data['cheque_no']}'";
                if ($value != '') {
                    $recon_bill = $value;
                    array_push($cheque_direct_array, $recon_bill);
                }
            }
            $cheque_direct_array1 = implode(',', $cheque_direct_array);
            if (($cheque_direct_array1)) {
                $newQuery7 .= " ex.cheque_no IN(    $cheque_direct_array1)";
                $condition7 .= " AND " . $newQuery7 . "";
                $newQuery8 .= " ex.cheque_no  NOT IN(    $cheque_direct_array1) OR ex.cheque_no IS NULL";
                $condition8 .= " AND " . $newQuery8 . "";
                
            }
            $dailyvSql1 = "SELECT ex.*, a.sumtotalamount, a.sumtdsamount,"
                    . " a.sumpaidamount FROM {$tblpx}dailyvendors ex"
                    . " LEFT JOIN (SELECT SUM(IFNULL(amount, 0) + IFNULL(tax_amount, 0)) "
                    . " as sumtotalamount, SUM(IFNULL(tds_amount, 0)) "
                    . " as sumtdsamount, SUM(IFNULL(paidamount, 0)) "
                    . " as sumpaidamount FROM {$tblpx}dailyvendors "
                    . " WHERE vendor_id = {$vendor_id}" . $condition2 . " "
                    . " AND reconciliation_status= 0 ) a "
                    . " ON 1 = 1"
                    . " WHERE ex.vendor_id = {$vendor_id} AND reconciliation_status= 0" . $condition1 ;

            $dailyvPayment1  = Yii::app()->db->createCommand($dailyvSql1)->queryAll();
            $dailyvSql      = "SELECT ex.*, a.sumtotalamount, a.sumtdsamount,"
                    . "  a.sumpaidamount "
                    . " FROM {$tblpx}dailyvendors ex"
                    . " LEFT JOIN (SELECT SUM(IFNULL(amount, 0) + IFNULL(tax_amount, 0)) "
                    . " as sumtotalamount, SUM(IFNULL(tds_amount, 0)) "
                    . " as sumtdsamount, SUM(IFNULL(paidamount, 0)) "
                    . " as sumpaidamount FROM {$tblpx}dailyvendors "
                    . " WHERE vendor_id = {$vendor_id}" . $condition2 . " "
                    . " AND (reconciliation_status IS NULL OR reconciliation_status=1)"
                    . " AND vendor_id={$vendor_id}) a "
                    . " ON 1 = 1"
                    . " WHERE ex.vendor_id = {$vendor_id} AND (reconciliation_status IS NULL 
                        OR reconciliation_status=1)" . $condition1 
                    . " AND ex.vendor_id={$vendor_id}";
            $dailyvPayment  = Yii::app()->db->createCommand($dailyvSql)->queryAll();

            $paymentData = $this->setDaybookData($vendor_id, $daybookPayment_pending, $condition3);
            $paymentSum     = 0;
            $miscellaneouspaymentData = "";
            $miscellaneouspaymentSum = 0;


            $expreceipt_return1sql = "select * from " . $tblpx . "expenses ex"
                . " WHERE amount !=0 AND vendor_id=" . $vendor_id . " "
                . " AND type = '72' " . $condition1 . " "
                . " AND return_id IS NOT NULL "
                . " AND (" . $newQuery2 . ") AND ex.reconciliation_status =0 "
                . " ORDER BY date";
            $expreceipt_return1 = Yii::app()->db->createCommand($expreceipt_return1sql)->queryAll();
            
            $expreceipt_returnsql = "select * from " . $tblpx . "expenses ex"
                . " WHERE amount !=0 AND vendor_id=" . $vendor_id . ""
                . "  AND type = '72' " . $condition1 . " AND return_id IS NOT NULL "
                . " AND (" . $newQuery2 . ") AND (ex.reconciliation_status =1 OR ex.reconciliation_status IS NULL)"
                . " ORDER BY date";                
            $expreceipt_return = Yii::app()->db->createCommand($expreceipt_returnsql)->queryAll();
            $defect_sql = "SELECT return_id,return_amount,return_date,"
                . " return_number,w.warehouse_name,r.warehousereceipt_no,"
                . " b.bill_number,p.vendor_id "
                . " FROM jp_defect_return d "
                . " LEFT JOIN jp_warehousereceipt r "
                . " ON d.receipt_id=r.warehousereceipt_id "
                . " LEFT JOIN jp_bills b ON b.bill_id = r.warehousereceipt_bill_id "
                . " LEFT JOIN jp_purchase p ON p.p_id=b.purchase_id "
                . " LEFT JOIN jp_warehouse w "
                . " ON w.warehouse_id =r.warehousereceipt_warehouseid "
                . " WHERE p.vendor_id = " . $vendor_id. " ".$condition4;
               
            $defect_return = Yii::app()->db->createCommand($defect_sql)->queryAll();
        } else {            
            $date_from  = isset($_REQUEST['date_from'])?$_REQUEST['date_from']:"";
            $date_to    = isset($_REQUEST['date_to'])?$_REQUEST['date_to']:"";
            $datecondition3 = "AND '1=1'";
            $datecondition1 = $datecondition2 =  "AND '1=1'";
            if (!empty($date_from) && !empty($date_to)) {

                $datecondition1 = " AND e.date BETWEEN '" . date("Y-m-d", strtotime($date_from)) . "' AND '" . date("Y-m-d", strtotime($date_to)) . "'";
                $datecondition2 = " AND d.date BETWEEN '" . date("Y-m-d", strtotime($date_from)) . "' AND '" . date("Y-m-d", strtotime($date_to)) . "'";
                $datecondition3 = " AND b.bill_date BETWEEN '" . date("Y-m-d", strtotime($date_from)) . "' AND '" . date("Y-m-d", strtotime($date_to)) . "'";                
            } else if (!empty($date_from) && empty($date_to)) {

                $datecondition1 = " AND e.date >= '" . date("Y-m-d", strtotime($date_from)) . "'";
                $datecondition2 = " AND d.date >= '" . date("Y-m-d", strtotime($date_from)) . "'";
                $datecondition3 = " AND b.bill_date >= '" . date("Y-m-d", strtotime($date_from)) . "'";
            } else if (empty($date_from) && !empty($date_to)) {
                $datecondition1 = " AND e.date <= '" . date("Y-m-d", strtotime($date_to)) . "'";
                $datecondition2 = " AND d.date <= '" . date("Y-m-d", strtotime($date_to)) . "'";
                $datecondition3 = " AND b.bill_date <= '" . date("Y-m-d", strtotime($date_to)) . "'";
            } else {
                $datecondition1 = "AND '1=1'";
                $datecondition2 = "AND '1=1'";
                $datecondition3 = "AND '1=1'";
            }
                
            $vendor_id      = 0;
            $company_id     = "";
            $sqlData        = "SELECT v.vendor_id,v.name, a.billtotal, a.billadditionaltotal, "
                . " b.daybookpaid, c.vendorpaid,d.daybookcredit FROM {$tblpx}vendors v"
                . " LEFT JOIN (SELECT p.vendor_id, SUM(IFNULL(b.bill_totalamount, 0))"
                . " AS billtotal,   SUM(IFNULL(b.bill_additionalcharge, 0)) "
                . " AS billadditionaltotal "
                . " FROM {$tblpx}bills b "
                . " LEFT JOIN {$tblpx}purchase p ON b.purchase_id = p.p_id "
                . " WHERE (" . $this->getCompanyCondition("b") . ") ".$datecondition3
                . " GROUP BY p.vendor_id) a ON a.vendor_id = v.vendor_id"
                . " LEFT JOIN (SELECT e.vendor_id, SUM(IFNULL(e.paidamount, 0)) "
                . " as daybookpaid FROM {$tblpx}expenses e "
                . " WHERE e.type = 73 AND e.expense_type!=107 AND e.expense_type != 0 AND  (e.reconciliation_status=1 OR e.reconciliation_status IS NULL) AND (" . $this->getCompanyCondition("e") . ") ".$datecondition1
                . " GROUP BY e.vendor_id) b "
                . " ON b.vendor_id = v.vendor_id"
                . " LEFT JOIN (SELECT e.vendor_id, SUM(IFNULL(e.amount, 0)) "
                . " as daybookcredit FROM {$tblpx}expenses e "
                . " WHERE e.type = 73 AND e.expense_type=0 AND (" . $this->getCompanyCondition("e") . ") ".$datecondition1
                . " GROUP BY e.vendor_id) d "
                . " ON d.vendor_id = v.vendor_id"
                . " LEFT JOIN (SELECT d.vendor_id, SUM(IFNULL(d.amount, 0) + IFNULL(d.tax_amount, 0))"
                . " as vendorpaid FROM {$tblpx}dailyvendors d "
                . " WHERE (d.reconciliation_status IS NULL OR d.reconciliation_status=1) AND (" . $this->getCompanyCondition("d") . ") ".$datecondition2
                . " GROUP BY d.vendor_id) c "
                . " ON c.vendor_id = v.vendor_id"
                . " WHERE (" . $this->getCompanyCondition("v") . ") "
                . " AND v.vendor_type != 2 ORDER BY v.name";

            $miscellaneous_sql_data = "SELECT v.name, a.billtotal, b.daybookpaid, c.vendorpaid "
                . " FROM {$tblpx}vendors v"
                . " LEFT JOIN (SELECT p.vendor_id, SUM(IFNULL(b.bill_totalamount, 0))"
                . "  as billtotal FROM {$tblpx}bills b "
                . " LEFT JOIN {$tblpx}purchase p "
                . " ON b.purchase_id = p.p_id "
                . " WHERE (" . $this->getCompanyCondition("b") . ") "
                . " GROUP BY p.vendor_id) a ON a.vendor_id = v.vendor_id"
                . " LEFT JOIN (SELECT e.vendor_id, SUM(IFNULL(e.paid, 0)) "
                . " as daybookpaid FROM {$tblpx}expenses e "
                . " WHERE e.type = 73 AND (" . $this->getCompanyCondition("e") . ") "
                . " GROUP BY e.vendor_id) b ON b.vendor_id = v.vendor_id"
                . " LEFT JOIN (SELECT d.vendor_id, SUM(IFNULL(d.amount, 0) + IFNULL(d.tax_amount, 0))"
                . "  as vendorpaid FROM {$tblpx}dailyvendors d "
                . " WHERE (" . $this->getCompanyCondition("d") . ") "
                . " GROUP BY d.vendor_id) c ON c.vendor_id = v.vendor_id"
                . " WHERE (" . $this->getCompanyCondition("v") . ") "
                . " AND v.vendor_type = 2 ORDER BY v.name";
            $sqlTotal       = "SELECT SUM(a.billtotal) as billsum, SUM(b.daybookpaid) "
                . " as daybooksum, SUM(b.daybookcredit) as daybookcreditsum , "
                . " SUM(c.vendorpaid) as vendorsum "
                . " FROM {$tblpx}vendors v"
                . " LEFT JOIN (SELECT p.vendor_id, SUM(IFNULL(b.bill_totalamount, 0) + IFNULL(b.bill_additionalcharge,0)) "
                . " as billtotal FROM {$tblpx}bills b "
                . " LEFT JOIN {$tblpx}purchase p ON b.purchase_id = p.p_id "
                . " WHERE (" . $this->getCompanyCondition("b") . ") "
                . " GROUP BY p.vendor_id) a ON a.vendor_id = v.vendor_id"
                . " LEFT JOIN (SELECT e.vendor_id, SUM(IFNULL(e.paid, 0)) "
                . " as daybookpaid ,SUM(IFNULL(e.amount, 0))  as daybookcredit "
                . " FROM {$tblpx}expenses e "
                . " WHERE e.type = 73 AND (" . $this->getCompanyCondition("e") . ") "
                . " GROUP BY e.vendor_id) b "
                . " ON b.vendor_id = v.vendor_id"
                . " LEFT JOIN (SELECT d.vendor_id, SUM(IFNULL(d.amount, 0) + IFNULL(d.tax_amount, 0))"
                . " as vendorpaid FROM {$tblpx}dailyvendors d "
                . " WHERE (" . $this->getCompanyCondition("d") . ")  "
                . " GROUP BY d.vendor_id) c ON c.vendor_id = v.vendor_id"
                . " WHERE (" . $this->getCompanyCondition("v") . ") "
                . " AND v.vendor_type != 2";
            $miscellaneousSqlTotal       = "SELECT SUM(a.billtotal) as billsum, SUM(b.daybookpaid) "
                . " as daybooksum, SUM(c.vendorpaid) as vendorsum "
                . " FROM {$tblpx}vendors v"
                . " LEFT JOIN (SELECT p.vendor_id, SUM(IFNULL(b.bill_totalamount, 0))"
                . " as billtotal FROM {$tblpx}bills b LEFT JOIN {$tblpx}purchase p "
                . " ON b.purchase_id = p.p_id "
                . " WHERE (" . $this->getCompanyCondition("b") . ") "
                . " GROUP BY p.vendor_id) a ON a.vendor_id = v.vendor_id"
                . " LEFT JOIN (SELECT e.vendor_id, SUM(IFNULL(e.paid, 0)) "
                . " as daybookpaid FROM {$tblpx}expenses e "
                . " WHERE e.type = 73 AND (" . $this->getCompanyCondition("e") . ") "
                . " GROUP BY e.vendor_id) b ON b.vendor_id = v.vendor_id"
                . " LEFT JOIN (SELECT d.vendor_id, SUM(IFNULL(d.amount, 0) + IFNULL(d.tax_amount, 0))"
                . " as vendorpaid FROM {$tblpx}dailyvendors d "
                . " WHERE (" . $this->getCompanyCondition("d") . ") "
                . " GROUP BY d.vendor_id) c ON c.vendor_id = v.vendor_id"
                . " WHERE (" . $this->getCompanyCondition("v") . ") AND v.vendor_type = 2";
            $paymentData    = Yii::app()->db->createCommand($sqlData)->queryAll();
            //echo "<pre>";print_r($paymentData);exit;

            $miscellaneouspaymentData    = Yii::app()->db->createCommand($miscellaneous_sql_data)->queryAll();
            $paymentSum     = Yii::app()->db->createCommand($sqlTotal)->queryRow();
            $miscellaneouspaymentSum     = Yii::app()->db->createCommand($miscellaneousSqlTotal)->queryRow();
            
            $daybookPayment = array();
            $daybookPayment1 = array();
            $daybookPayment_pending = array();
            $model          = "";
            $dailyvPayment  = array();
            $dailyvPayment1  = array();
            $expreceipt_return = array();
            $expreceipt_return1 = array();
            $reconciliation_data = array();
            $reconciliation_data1 = array();
            $defect_return = array();
        }
        $render_datas = array(
            'model' => $model,
            'vendor_id'     =>  $vendor_id,
            'company_id' => $company_id,
            'paymentData'       => $paymentData,
            'paymentSum'      => $paymentSum,
            'date_from'        => $date_from,
            'date_to'   => $date_to,
            'daybookPayment'    => $daybookPayment,
            'daybookPayment1' => $daybookPayment1,
            'dailyvPayment' => $dailyvPayment,
            'dailyvPayment1'  => $dailyvPayment1,
            'miscellaneouspaymentData' => $miscellaneouspaymentData,
            'miscellaneouspaymentSum' => $miscellaneouspaymentSum,
            'expreceipt_return' => $expreceipt_return,
            'expreceipt_return1' => $expreceipt_return1,
            'reconciliation_data' => $reconciliation_data,
            'reconciliation_data1' => $reconciliation_data1,
            'daybookPayment_pending ' => $daybookPayment_pending,
            'defect_return'=>$defect_return,
            'total_vendor_Amount'=>$total_vendor_Amount,//Invoice AMount
        );
            $exported_data = $this->renderPartial('paymentreport', $render_datas, true);
            $export_filename = 'Vendor Payment Report' . date('d_M_Y') ;

            // ExportHelper::exportGridAsCSV($exported_data, $export_filename);
            require_once (Yii::app()->basePath . '/components/simple_html_dom.php');
            $html = str_get_html($exported_data); // give this your HTML string
            header('Content-type: application/ms-excel');
            header('Content-Disposition: attachment; filename=' . $export_filename . '.csv');

            $fp = fopen("php://output", "w");
            if (!empty($date_from) && !empty($date_to)) {
                $heading = ['Data filtered between: ' . date('d-m-Y', strtotime($date_from)) . ' and ' . date('d-m-Y', strtotime($date_to))];
                fputcsv($fp, $heading);
            }elseif(!empty($datefrom)) {
                 $heading = ['Data From: ' . date('d-m-Y', strtotime($date_from)) ];
                fputcsv($fp, $heading);
                
            } elseif (!empty($date_to)) {
                    $heading= ['Data Upto :'.date('d-m-Y', strtotime($date_to))];
                    fputcsv($fp, $heading);
            } else {
                    // No date range to display
            }

            // Add an empty row for spacing
            fputcsv($fp, []);

            $contents = $html->find('tr');

            foreach ($contents as $element) {
                $td = array();
                foreach ($element->find('th') as $row) {
                    if (strpos(trim($row->class), 'actions') === false && strpos(trim($row->class), 'checker') === false) {
                        $td [] = str_replace('&nbsp;', '',$row->plaintext);
                    }
                }
                if (!empty($td)) {
                    fputcsv($fp, $td);
                }

                $td = array();
                foreach ($element->find('td') as $row) {
                    if (strpos(trim($row->class), 'actions') === false && strpos(trim($row->class), 'checker') === false) {
                        $td [] = str_replace('&nbsp;', '', $row->plaintext);
                    }
                }
                if (!empty($td)) {
                    fputcsv($fp, $td);
                }
            }

            fclose($fp);
            exit;


        

        
        }else{
            /**All vendors starts  */
        $tblpx  = Yii::app()->db->tablePrefix;
        $date_from      = "";
        $date_to        = "";
        $total_vendor_Amount = 0;
        if (isset($_REQUEST['vendor_id']) && !empty($_REQUEST['vendor_id'])) {
            $vendor_id = $_REQUEST['vendor_id'];
            if (!empty($_REQUEST['company_id'])) {
                $company_id = $_REQUEST['company_id'];
            } else {
                $company_id = "";
            }
            $model      = Vendors::model()->findByPk($vendor_id);
            $date_from  = $_REQUEST['date_from'];
            $date_to    = $_REQUEST['date_to'];
            $condition1 = "";
            $condition2 = "";
            $condition3 = "";
            $condition4 = "";
            $condition5 = "";
            $condition6 = "";
            $condition7 = "";
            $condition8 = "";
            $condition6a = "";
            $condition9 = "";
            $condition10="";
            $total_vendor_Amount=0;
            if (!empty($date_from) && !empty($date_to)) {

                $condition1 = " AND ex.date BETWEEN '" . date("Y-m-d", strtotime($date_from)) . "' AND '" . date("Y-m-d", strtotime($date_to)) . "'";
                $condition2 = " AND date BETWEEN '" . date("Y-m-d", strtotime($date_from)) . "' AND '" . date("Y-m-d", strtotime($date_to)) . "'";
                $condition3 = " AND bill_date BETWEEN '" . date("Y-m-d", strtotime($date_from)) . "' AND '" . date("Y-m-d", strtotime($date_to)) . "'";
                $condition4 = " AND return_date BETWEEN '" . date("Y-m-d", strtotime($date_from)) . "' AND '" . date("Y-m-d", strtotime($date_to)) . "'";
                $condition9 = " AND b.bill_date BETWEEN '" . date("Y-m-d", strtotime($date_from)) . "' AND '" . date("Y-m-d", strtotime($date_to)) . "'";
                $condition10 = " AND e.date BETWEEN '" . date("Y-m-d", strtotime($date_from)) . "' AND '" . date("Y-m-d", strtotime($date_to)) . "'";
            } else if (!empty($date_from) && empty($date_to)) {

                $condition1 = " AND ex.date >= '" . date("Y-m-d", strtotime($date_from)) . "'";
                $condition2 = " AND date >= '" . date("Y-m-d", strtotime($date_from)) . "'";
                $condition3 = " AND bill_date >= '" . date("Y-m-d", strtotime($date_from)) . "'";
                $condition9 = " AND b.bill_date >= '" . date("Y-m-d", strtotime($date_from)) . "'";
                $condition10 = " AND e.date BETWEEN '" . date("Y-m-d", strtotime($date_from)) . "' AND '" . date("Y-m-d", strtotime($date_to)) . "'";
                
                $condition4 = " AND return_date >= '" . date("Y-m-d", strtotime($date_from)) . "'";
            } else if (empty($date_from) && !empty($date_to)) {

                $condition1 = " AND ex.date <= '" . date("Y-m-d", strtotime($date_to)) . "'";
                $condition2 = " AND date <= '" . date("Y-m-d", strtotime($date_to)) . "'";
                $condition9 = " AND b.bill_date <= '" . date("Y-m-d", strtotime($date_to)) . "'";
                $condition10 = " AND e.date BETWEEN '" . date("Y-m-d", strtotime($date_from)) . "' AND '" . date("Y-m-d", strtotime($date_to)) . "'";
                $condition3 = " AND bill_date <= '" . date("Y-m-d", strtotime($date_to)) . "'";
                $condition4 = " AND return_date <= '" . date("Y-m-d", strtotime($date_to)) . "'";
            } else {

                $condition1 = "";
                $condition2 = "";
                $condition3 = "";
                $condition4 = "";
                $condition5 = "";
                $condition6 = "";
                $condition7 = "";
                $condition8 = "";
                $condition6a = "";
                $condition9 = "";
                $condition10="";
            }
            $user       = Users::model()->findByPk(Yii::app()->user->id);
            $arrVal     = explode(',', $user->company_id);
            $newQuery1  = "";
            $newQuery2  = "";
            $newQuery3  = "";
            $newQuery4  = "";
            $newQuery5  = "";
            $newQuery6 = "";
            $newQuery7 = "";
            $newQuery8 = "";
            $newQuery6a = "";
            if (!empty($_REQUEST['company_id'])) {
                foreach ($arrVal as $arr) {
                    if ($newQuery1) $newQuery1 .= ' OR';
                    if ($newQuery2) $newQuery2 .= ' OR';
                    if ($newQuery3) $newQuery3 .= ' OR';
                    if ($newQuery4) $newQuery4 .= ' OR';
                    if ($newQuery5) $newQuery5 .= ' OR';
                    $newQuery1 .= " FIND_IN_SET('" . $arr . "', ex.company_id)";
                    $newQuery2 .= " FIND_IN_SET('" . $arr . "', company_id)";
                    $newQuery3 .= " FIND_IN_SET('" . $arr . "', t.company_id)";
                    $newQuery4 .= " FIND_IN_SET('" . $arr . "', r.company_id)";
                    $newQuery5 .= " FIND_IN_SET('" . $arr . "', r.company_id)";
                }
                $condition1 .= " AND ex.company_id = " . $_REQUEST['company_id'] . "";
                $condition2 .= " AND company_id = " . $_REQUEST['company_id'] . "";
                $condition3 .= " AND t.company_id = " . $_REQUEST['company_id'] . "";
                $condition4 .= " AND r.company_id = " . $_REQUEST['company_id'] . "";
                $condition5 .= " AND e.company_id = " . $_REQUEST['company_id'] . "";
            } else {
                foreach ($arrVal as $arr) {
                    if ($newQuery1) $newQuery1 .= ' OR';
                    if ($newQuery2) $newQuery2 .= ' OR';
                    if ($newQuery3) $newQuery3 .= ' OR';
                    if ($newQuery4) $newQuery4 .= ' OR';
                    if ($newQuery5) $newQuery5 .= ' OR';
                    $newQuery1 .= " FIND_IN_SET('" . $arr . "', ex.company_id)";
                    $newQuery2 .= " FIND_IN_SET('" . $arr . "', company_id)";
                    $newQuery3 .= " FIND_IN_SET('" . $arr . "', t.company_id)";
                    $newQuery4 .= " FIND_IN_SET('" . $arr . "', r.company_id)";
                    $newQuery5 .= " FIND_IN_SET('" . $arr . "', e.company_id)";
                }
                $condition1 .= " AND (" . $newQuery1 . ")";
                $condition2 .= " AND (" . $newQuery2 . ")";
                $condition3 .= " AND (" . $newQuery3 . ")";
                $condition4 .= " AND (" . $newQuery4 . ")";
                $condition5 .= " AND (" . $newQuery5 . ")";
            }
            Yii::app()->db->createCommand('SET SQL_BIG_SELECTS=1')->execute();
            // recon return
            $recon_return_sql1 = "SELECT r.*,e.* "
                . " FROM " . $tblpx . "reconciliation r "
                . " join " . $tblpx . "expenses e "
                . " on r.reconciliation_parentid =e.exp_id  "
                . " where  r.reconciliation_status='0' "
                . " and e.vendor_id='" . $vendor_id . "' "
                . " and r.reconciliation_payment ='Purchase Return'";
            $reconciliation_data = Yii::app()->db->createCommand($recon_return_sql1)->queryAll();

            //recon Dyabook summary 
            $recon_daysum_sql1 = "SELECT r.*,e.* "
                . " FROM " . $tblpx . "reconciliation r "
                . " join " . $tblpx . "expenses e "
                . " on r.reconciliation_parentid =e.exp_id  "
                . " where  r.reconciliation_status='0' "
                . " and e.vendor_id='" . $vendor_id . "' ";
            $reconciliation_data1 = Yii::app()->db->createCommand($recon_daysum_sql1)->queryAll();

            //direct v payment recon

            $recon_direct_sql1 = "SELECT r.*,d.* "
                . " FROM " . $tblpx . "reconciliation r "
                . " join " . $tblpx . "dailyvendors d "
                . " on r.reconciliation_parentid =d.daily_v_id  "
                . " where  r.reconciliation_status='0' "
                . " and d.vendor_id='" . $vendor_id . "' "
                . " and r.reconciliation_payment ='Vendor Payment'";
            $reconciliation_data2 = Yii::app()->db->createCommand($recon_direct_sql1)->queryAll();


            $cheque_array = array();
            foreach ($reconciliation_data as  $data) {
                $value = $data['cheque_no'];
                if ($value != '') {
                    $recon_bill = $value;
                    array_push($cheque_array, $recon_bill);
                }
            }
            $cheque_array1 = implode(',', $cheque_array);

            $cheque_array_day_sum = array();
            foreach ($reconciliation_data1 as  $data) {
                $value = $data['cheque_no'];
                if ($value != '') {
                    $recon_bill = "'{$value}'";
                    array_push($cheque_array_day_sum, $recon_bill);
                }
            }

            $cheque_array_day_sum1 = implode(',', $cheque_array_day_sum);

                $daybookSql1     = "SELECT p.name as projectname, a.projectcount,"
                    . "  b.totalamount, c.totalpaid, d.totaltds, "
                    . " e.totalpaidtovendor, f.sumtotalamount, g.sumtotalpaid, "
                    . " h.sumtotaltds, i.sumtotalpaidtovendor, bl.bill_number "
                    . " as billnumber, bl.bill_totalamount, ex.* "
                    . " FROM {$tblpx}expenses ex"
                    . " LEFT JOIN {$tblpx}projects p "
                    . "ON ex.projectid = p.pid"
                    . " LEFT JOIN {$tblpx}bills bl "
                    . " ON ex.bill_id = bl.bill_id"
                    . " LEFT JOIN (SELECT projectid, count(*) "
                    . " as projectcount FROM {$tblpx}expenses "
                    . " WHERE type = 73 AND vendor_id = {$vendor_id}" . $condition2 . " "
                    . " GROUP BY projectid) a "
                    . " ON a.projectid = ex.projectid"
                    . " LEFT JOIN (SELECT projectid, SUM(amount) "
                    . " as totalamount FROM {$tblpx}expenses "
                    . " WHERE type = 73 AND vendor_id = {$vendor_id}" . $condition2 . " "
                    . " AND reconciliation_status=0 "
                    . " GROUP BY projectid) b ON b.projectid = ex.projectid"
                    . " LEFT JOIN (SELECT projectid, SUM(paid) as totalpaid "
                    . " FROM {$tblpx}expenses WHERE type = 73 "
                    . " AND vendor_id = {$vendor_id}" . $condition2 . " AND  "
                    . " reconciliation_status=0 GROUP BY projectid) c "
                    . " ON c.projectid = ex.projectid"
                    . " LEFT JOIN (SELECT projectid, SUM(expense_tds) "
                    . " as totaltds FROM {$tblpx}expenses "
                    . " WHERE type = 73 AND vendor_id = {$vendor_id}" . $condition2 . " "
                    . " AND reconciliation_status=0 "
                    . " GROUP BY projectid) d "
                    . " ON d.projectid = ex.projectid"
                    . " LEFT JOIN (SELECT projectid, SUM(paidamount) "
                    . " as totalpaidtovendor FROM {$tblpx}expenses "
                    . " WHERE type = 73 AND vendor_id = {$vendor_id}" . $condition2 . ""
                    . "  AND reconciliation_status= 0 "
                    . " GROUP BY projectid) e ON e.projectid = ex.projectid"
                    . " LEFT JOIN (SELECT projectid, SUM(amount) as sumtotalamount "
                    . " FROM {$tblpx}expenses WHERE type = 73 AND "
                    . " vendor_id = {$vendor_id}" . $condition2 . " AND"
                    . " reconciliation_status=0) f ON 1 = 1"
                    . " LEFT JOIN (SELECT projectid, SUM(paid) "
                    . " as sumtotalpaid FROM {$tblpx}expenses "
                    . " WHERE type = 73 AND vendor_id = {$vendor_id}" . $condition2 . ""
                    . "  AND reconciliation_status=0) g"
                    . "  ON 1 = 1"
                    . " LEFT JOIN (SELECT projectid, SUM(expense_tds) "
                    . " as sumtotaltds FROM {$tblpx}expenses "
                    . " WHERE type = 73 AND vendor_id = {$vendor_id}" . $condition2 . " "
                    . " AND reconciliation_status=0) h ON 1 = 1"
                    . " LEFT JOIN (SELECT projectid, SUM(paidamount) "
                    . " as sumtotalpaidtovendor FROM {$tblpx}expenses "
                    . " WHERE type = 73 AND vendor_id = {$vendor_id}" . $condition2 . ""
                    . "  AND reconciliation_status=0) i ON 1 = 1"
                    . " WHERE ex.type = 73 AND ex.vendor_id = {$vendor_id}" . $condition1 . " AND ex.reconciliation_status=0 ORDER BY ex.projectid";
                $daybookPayment1 = Yii::app()->db->createCommand($daybookSql1)->queryAll();
                $daybookSql = "
    SELECT 
        p.name as projectname, 
        a.projectcount, 
        b.totalamount, 
        c.totalpaid, 
        d.totaltds, 
        e.totalpaidtovendor, 
        f.sumtotalamount,
        z.sumtotalvendoramount, 
        g.sumtotalpaid, 
        h.sumtotaltds, 
        i.sumtotalpaidtovendor, 
        bl.bill_number as billnumber, 
        bl.bill_totalamount, 
        y.sumtotalamountpaidtovendor,
        ex.* 
    FROM 
        {$tblpx}expenses ex
    LEFT JOIN {$tblpx}projects p ON ex.projectid = p.pid
    LEFT JOIN {$tblpx}bills bl ON ex.bill_id = bl.bill_id
    LEFT JOIN (
        SELECT 
            projectid, 
            COUNT(*) as projectcount 
        FROM 
            {$tblpx}expenses 
        WHERE 
            type = 73 AND vendor_id = {$vendor_id} {$condition2} 
        GROUP BY 
            projectid
    ) a ON a.projectid = ex.projectid
    LEFT JOIN (
        SELECT 
            projectid, 
            SUM(amount) as totalamount 
        FROM 
            {$tblpx}expenses 
        WHERE 
            type = 73 AND vendor_id = {$vendor_id} {$condition2} 
            AND (reconciliation_status=1 OR reconciliation_status IS NULL) 
        GROUP BY 
            projectid
    ) b ON b.projectid = ex.projectid
    LEFT JOIN (
        SELECT 
            projectid, 
            SUM(paid) as totalpaid 
        FROM 
            {$tblpx}expenses 
        WHERE 
            type = 73 AND vendor_id = {$vendor_id} {$condition2} 
            AND (reconciliation_status=1 OR reconciliation_status IS NULL) 
        GROUP BY 
            projectid
    ) c ON c.projectid = ex.projectid
    LEFT JOIN (
        SELECT 
            projectid, 
            SUM(expense_tds) as totaltds 
        FROM 
            {$tblpx}expenses 
        WHERE 
            type = 73 AND vendor_id = {$vendor_id} {$condition2} 
            AND (reconciliation_status=1 OR reconciliation_status IS NULL) 
        GROUP BY 
            projectid
    ) d ON d.projectid = ex.projectid
    LEFT JOIN (
        SELECT 
            projectid, 
            SUM(paidamount) as totalpaidtovendor 
        FROM 
            {$tblpx}expenses 
        WHERE 
            expense_type != 0 AND type = 73 AND vendor_id = {$vendor_id} {$condition2} 
            AND (reconciliation_status=1 OR reconciliation_status IS NULL) 
        GROUP BY 
            projectid
    ) e ON e.projectid = ex.projectid
    LEFT JOIN (
        SELECT 
            pu.project_id as projectid, 
            SUM(b.bill_totalamount) as sumtotalamount
        FROM 
            {$tblpx}bills b
        LEFT JOIN {$tblpx}purchase pu ON b.purchase_id = pu.p_id
        LEFT JOIN {$tblpx}projects p ON pu.project_id = p.pid
        LEFT JOIN {$tblpx}expenses e ON e.bill_id = b.bill_id
        WHERE 
            pu.vendor_id = {$vendor_id} {$condition9}
            AND (e.reconciliation_status=1 OR e.reconciliation_status IS NULL) AND e.type=73 
        GROUP BY 
            pu.project_id
    ) f ON f.projectid = ex.projectid
    LEFT JOIN (
        SELECT 
            unique_bills.projectid, 
            SUM(unique_bills.bill_amount) as sumtotalvendoramount
        FROM (
            SELECT 
                DISTINCT e.bill_id, 
                e.projectid, 
                SUM(e.amount) as bill_amount
            FROM 
                {$tblpx}expenses e
            WHERE 
                e.vendor_id = {$vendor_id} {$condition10}  
                AND (e.reconciliation_status=1 OR e.reconciliation_status IS NULL) 
                AND e.type=73 
            GROUP BY 
                e.bill_id, e.projectid
        ) as unique_bills
        GROUP BY 
            unique_bills.projectid
    ) z ON z.projectid = ex.projectid
    LEFT JOIN (
        SELECT 
            projectid, 
            SUM(paid) as sumtotalpaid 
        FROM 
            {$tblpx}expenses 
        WHERE 
            type = 73 AND vendor_id = {$vendor_id} {$condition2} 
            AND (reconciliation_status=1 OR reconciliation_status IS NULL) 
        GROUP BY 
            projectid
    ) g ON g.projectid = ex.projectid
    LEFT JOIN (
        SELECT 
            projectid, 
            SUM(expense_tds) as sumtotaltds 
        FROM 
            {$tblpx}expenses 
        WHERE 
            type = 73 AND vendor_id = {$vendor_id} {$condition2} 
            AND (reconciliation_status=1 OR reconciliation_status IS NULL) 
        GROUP BY 
            projectid
    ) h ON h.projectid = ex.projectid
    LEFT JOIN (
        SELECT 
            projectid, 
            SUM(paidamount) as sumtotalpaidtovendor 
        FROM 
            {$tblpx}expenses 
        WHERE 
            expense_type != 0 AND type = 73 AND vendor_id = {$vendor_id} {$condition2} 
            AND (reconciliation_status=1 OR reconciliation_status IS NULL) 
        GROUP BY 
            projectid
    ) i ON i.projectid = ex.projectid
    LEFT JOIN (
        SELECT 
            projectid, 
            SUM(paidamount) as sumtotalamountpaidtovendor 
        FROM 
            {$tblpx}expenses 
        WHERE 
            expense_type != 0 AND type = 73 AND expense_type != '107' AND vendor_id = {$vendor_id} {$condition2} 
            AND (reconciliation_status=1 OR reconciliation_status IS NULL) 
        GROUP BY 
            vendor_id
    ) y ON y.projectid = ex.projectid
    WHERE 
        ex.type = 73 
        AND ex.vendor_id = {$vendor_id} {$condition1} 
        AND (ex.reconciliation_status=1 OR ex.reconciliation_status IS NULL) 
    ORDER BY 
        ex.projectid";

     $daybook_total_sql ="SELECT 
                DISTINCT e.bill_id, e.exp_id,
                e.projectid
                FROM 
                {$tblpx}expenses e
            WHERE 
                e.vendor_id = {$vendor_id} {$condition10}   
                AND (e.reconciliation_status=1 OR e.reconciliation_status IS NULL) 
                AND e.type=73 
            GROUP BY 
                e.bill_id, e.projectid";   
                  $daybookPayment = Yii::app()->db->createCommand($daybookSql)->queryAll();
        
            //echo "<pre>";print_r($daybookPayment);exit;
            $daybook_total = Yii::app()->db->createCommand($daybook_total_sql)->queryAll();
                $total_vendor_Amount =0;
                if(!empty($daybook_total)){
                    foreach($daybook_total as $daybook_total_entries){
                        $exp_model = Expenses::Model()->findByPk($daybook_total_entries["exp_id"]);
                        if(!empty($exp_model)){
                            $total_vendor_Amount += $exp_model->amount;
                        }
                    }
                }
    
            //die($total_vendor_Amount);

              
            $daybookSql_pending     = "SELECT p.name as projectname, a.projectcount,"
                . " b.totalamount, c.totalpaid, d.totaltds, e.totalpaidtovendor, "
                . " f.sumtotalamount, g.sumtotalpaid, h.sumtotaltds, i.sumtotalpaidtovendor,"
                . "  bl.bill_number as billnumber, bl.bill_totalamount, ex.* "
                . " FROM {$tblpx}expenses ex"
                . " LEFT JOIN {$tblpx}projects p ON ex.projectid = p.pid"
                . " LEFT JOIN {$tblpx}bills bl ON ex.bill_id = bl.bill_id"
                . " LEFT JOIN (SELECT projectid, count(*) as projectcount "
                . " FROM {$tblpx}expenses WHERE type = 73 "
                . " AND vendor_id = {$vendor_id}" . $condition2 . " "
                . " GROUP BY projectid) a ON a.projectid = ex.projectid"
                . " LEFT JOIN (SELECT projectid, SUM(amount) as totalamount "
                . " FROM {$tblpx}expenses WHERE type = 73 "
                . " AND vendor_id = {$vendor_id}" . $condition2 . " GROUP BY projectid) b"
                . " ON b.projectid = ex.projectid"
                . " LEFT JOIN (SELECT projectid, SUM(paid) as totalpaid "
                . " FROM {$tblpx}expenses WHERE type = 73 "
                . " AND vendor_id = {$vendor_id}" . $condition2 . " GROUP BY projectid) c "
                . " ON c.projectid = ex.projectid"
                . " LEFT JOIN (SELECT projectid, SUM(expense_tds) as totaltds "
                . " FROM {$tblpx}expenses WHERE type = 73 "
                . " AND vendor_id = {$vendor_id}" . $condition2 . " "
                . " GROUP BY projectid) d ON d.projectid = ex.projectid"
                . " LEFT JOIN (SELECT projectid, SUM(paidamount) as totalpaidtovendor "
                . " FROM {$tblpx}expenses WHERE type = 73 "
                . " AND vendor_id = {$vendor_id}" . $condition2 . " "
                . " GROUP BY projectid) e ON e.projectid = ex.projectid"
                . " LEFT JOIN (SELECT projectid, SUM(amount) as sumtotalamount "
                . " FROM {$tblpx}expenses WHERE type = 73 "
                . " AND vendor_id = {$vendor_id}" . $condition2 . ") f ON 1 = 1"
                . " LEFT JOIN (SELECT projectid, SUM(paid) as sumtotalpaid "
                . " FROM {$tblpx}expenses WHERE type = 73 "
                . " AND vendor_id = {$vendor_id}" . $condition2 . ") g ON 1 = 1"
                . " LEFT JOIN (SELECT projectid, SUM(expense_tds) as sumtotaltds "
                . " FROM {$tblpx}expenses WHERE type = 73 "
                . " AND vendor_id = {$vendor_id}" . $condition2 . ") h ON 1 = 1"
                . " LEFT JOIN (SELECT projectid, SUM(paidamount) as sumtotalpaidtovendor "
                . " FROM {$tblpx}expenses WHERE type = 73 "
                . " AND vendor_id = {$vendor_id}" . $condition2 . ") i ON 1 = 1"
                . " WHERE ex.type = 73 AND ex.vendor_id = {$vendor_id}" . $condition1 . ""
                . "  ORDER BY ex.projectid";
            $daybookPayment_pending = Yii::app()->db->createCommand($daybookSql_pending)->queryAll();

            $cheque_direct_array = array();
            foreach ($reconciliation_data2 as  $data) {
                $value = "'{$data['cheque_no']}'";
                if ($value != '') {
                    $recon_bill = $value;
                    array_push($cheque_direct_array, $recon_bill);
                }
            }
            $cheque_direct_array1 = implode(',', $cheque_direct_array);
            if (($cheque_direct_array1)) {
                $newQuery7 .= " ex.cheque_no IN(    $cheque_direct_array1)";
                $condition7 .= " AND " . $newQuery7 . "";
                $newQuery8 .= " ex.cheque_no  NOT IN(    $cheque_direct_array1) OR ex.cheque_no IS NULL";
                $condition8 .= " AND " . $newQuery8 . "";
                
            }
            $dailyvSql1 = "SELECT ex.*, a.sumtotalamount, a.sumtdsamount,"
                    . " a.sumpaidamount FROM {$tblpx}dailyvendors ex"
                    . " LEFT JOIN (SELECT SUM(IFNULL(amount, 0) + IFNULL(tax_amount, 0)) "
                    . " as sumtotalamount, SUM(IFNULL(tds_amount, 0)) "
                    . " as sumtdsamount, SUM(IFNULL(paidamount, 0)) "
                    . " as sumpaidamount FROM {$tblpx}dailyvendors "
                    . " WHERE vendor_id = {$vendor_id}" . $condition2 . " "
                    . " AND reconciliation_status= 0 ) a "
                    . " ON 1 = 1"
                    . " WHERE ex.vendor_id = {$vendor_id} AND reconciliation_status= 0" . $condition1 ;

            $dailyvPayment1  = Yii::app()->db->createCommand($dailyvSql1)->queryAll();
            $dailyvSql      = "SELECT ex.*, a.sumtotalamount, a.sumtdsamount,"
                    . "  a.sumpaidamount "
                    . " FROM {$tblpx}dailyvendors ex"
                    . " LEFT JOIN (SELECT SUM(IFNULL(amount, 0) + IFNULL(tax_amount, 0)) "
                    . " as sumtotalamount, SUM(IFNULL(tds_amount, 0)) "
                    . " as sumtdsamount, SUM(IFNULL(paidamount, 0)) "
                    . " as sumpaidamount FROM {$tblpx}dailyvendors "
                    . " WHERE vendor_id = {$vendor_id}" . $condition2 . " "
                    . " AND (reconciliation_status IS NULL OR reconciliation_status=1)"
                    . " AND vendor_id={$vendor_id}) a "
                    . " ON 1 = 1"
                    . " WHERE ex.vendor_id = {$vendor_id} AND (reconciliation_status IS NULL 
                        OR reconciliation_status=1)" . $condition1 
                    . " AND ex.vendor_id={$vendor_id}";
            $dailyvPayment  = Yii::app()->db->createCommand($dailyvSql)->queryAll();

            $paymentData = $this->setDaybookData($vendor_id, $daybookPayment_pending, $condition3);
            $paymentSum     = 0;
            $miscellaneouspaymentData = "";
            $miscellaneouspaymentSum = 0;


            $expreceipt_return1sql = "select * from " . $tblpx . "expenses ex"
                . " WHERE amount !=0 AND vendor_id=" . $vendor_id . " "
                . " AND type = '72' " . $condition1 . " "
                . " AND return_id IS NOT NULL "
                . " AND (" . $newQuery2 . ") AND ex.reconciliation_status =0 "
                . " ORDER BY date";
            $expreceipt_return1 = Yii::app()->db->createCommand($expreceipt_return1sql)->queryAll();
            
            $expreceipt_returnsql = "select * from " . $tblpx . "expenses ex"
                . " WHERE amount !=0 AND vendor_id=" . $vendor_id . ""
                . "  AND type = '72' " . $condition1 . " AND return_id IS NOT NULL "
                . " AND (" . $newQuery2 . ") AND (ex.reconciliation_status =1 OR ex.reconciliation_status IS NULL)"
                . " ORDER BY date";                
            $expreceipt_return = Yii::app()->db->createCommand($expreceipt_returnsql)->queryAll();
            $defect_sql = "SELECT return_id,return_amount,return_date,"
                . " return_number,w.warehouse_name,r.warehousereceipt_no,"
                . " b.bill_number,p.vendor_id "
                . " FROM jp_defect_return d "
                . " LEFT JOIN jp_warehousereceipt r "
                . " ON d.receipt_id=r.warehousereceipt_id "
                . " LEFT JOIN jp_bills b ON b.bill_id = r.warehousereceipt_bill_id "
                . " LEFT JOIN jp_purchase p ON p.p_id=b.purchase_id "
                . " LEFT JOIN jp_warehouse w "
                . " ON w.warehouse_id =r.warehousereceipt_warehouseid "
                . " WHERE p.vendor_id = " . $vendor_id. " ".$condition4;
               
            $defect_return = Yii::app()->db->createCommand($defect_sql)->queryAll();
        } else {            
            $date_from  = isset($_REQUEST['date_from'])?$_REQUEST['date_from']:"";
            $date_to    = isset($_REQUEST['date_to'])?$_REQUEST['date_to']:"";
            $datecondition3 = "AND '1=1'";
            $datecondition1 = $datecondition2 =  "AND '1=1'";
            if (!empty($date_from) && !empty($date_to)) {

                $datecondition1 = " AND e.date BETWEEN '" . date("Y-m-d", strtotime($date_from)) . "' AND '" . date("Y-m-d", strtotime($date_to)) . "'";
                $datecondition2 = " AND d.date BETWEEN '" . date("Y-m-d", strtotime($date_from)) . "' AND '" . date("Y-m-d", strtotime($date_to)) . "'";
                $datecondition3 = " AND b.bill_date BETWEEN '" . date("Y-m-d", strtotime($date_from)) . "' AND '" . date("Y-m-d", strtotime($date_to)) . "'";                
            } else if (!empty($date_from) && empty($date_to)) {

                $datecondition1 = " AND e.date >= '" . date("Y-m-d", strtotime($date_from)) . "'";
                $datecondition2 = " AND d.date >= '" . date("Y-m-d", strtotime($date_from)) . "'";
                $datecondition3 = " AND b.bill_date >= '" . date("Y-m-d", strtotime($date_from)) . "'";
            } else if (empty($date_from) && !empty($date_to)) {
                $datecondition1 = " AND e.date <= '" . date("Y-m-d", strtotime($date_to)) . "'";
                $datecondition2 = " AND d.date <= '" . date("Y-m-d", strtotime($date_to)) . "'";
                $datecondition3 = " AND b.bill_date <= '" . date("Y-m-d", strtotime($date_to)) . "'";
            } else {
                $datecondition1 = "AND '1=1'";
                $datecondition2 = "AND '1=1'";
                $datecondition3 = "AND '1=1'";
            }
                
            $vendor_id      = 0;
            $company_id     = "";
            $sqlData        = "SELECT v.vendor_id,v.name, a.billtotal, a.billadditionaltotal, "
                . " b.daybookpaid, c.vendorpaid,d.daybookcredit FROM {$tblpx}vendors v"
                . " LEFT JOIN (SELECT p.vendor_id, SUM(IFNULL(b.bill_totalamount, 0))"
                . " AS billtotal,   SUM(IFNULL(b.bill_additionalcharge, 0)) "
                . " AS billadditionaltotal "
                . " FROM {$tblpx}bills b "
                . " LEFT JOIN {$tblpx}purchase p ON b.purchase_id = p.p_id "
                . " WHERE (" . $this->getCompanyCondition("b") . ") ".$datecondition3
                . " GROUP BY p.vendor_id) a ON a.vendor_id = v.vendor_id"
                . " LEFT JOIN (SELECT e.vendor_id, SUM(IFNULL(e.paidamount, 0)) "
                . " as daybookpaid FROM {$tblpx}expenses e "
                . " WHERE e.type = 73 AND e.expense_type!=107 AND e.expense_type != 0 AND  (e.reconciliation_status=1 OR e.reconciliation_status IS NULL) AND (" . $this->getCompanyCondition("e") . ") ".$datecondition1
                . " GROUP BY e.vendor_id) b "
                . " ON b.vendor_id = v.vendor_id"
                . " LEFT JOIN (SELECT e.vendor_id, SUM(IFNULL(e.amount, 0)) "
                . " as daybookcredit FROM {$tblpx}expenses e "
                . " WHERE e.type = 73 AND e.expense_type=0 AND (" . $this->getCompanyCondition("e") . ") ".$datecondition1
                . " GROUP BY e.vendor_id) d "
                . " ON d.vendor_id = v.vendor_id"
                . " LEFT JOIN (SELECT d.vendor_id, SUM(IFNULL(d.amount, 0) + IFNULL(d.tax_amount, 0))"
                . " as vendorpaid FROM {$tblpx}dailyvendors d "
                . " WHERE (d.reconciliation_status IS NULL OR d.reconciliation_status=1) AND (" . $this->getCompanyCondition("d") . ") ".$datecondition2
                . " GROUP BY d.vendor_id) c "
                . " ON c.vendor_id = v.vendor_id"
                . " WHERE (" . $this->getCompanyCondition("v") . ") "
                . " AND v.vendor_type != 2 ORDER BY v.name";

            $miscellaneous_sql_data = "SELECT v.name, a.billtotal, b.daybookpaid, c.vendorpaid "
                . " FROM {$tblpx}vendors v"
                . " LEFT JOIN (SELECT p.vendor_id, SUM(IFNULL(b.bill_totalamount, 0))"
                . "  as billtotal FROM {$tblpx}bills b "
                . " LEFT JOIN {$tblpx}purchase p "
                . " ON b.purchase_id = p.p_id "
                . " WHERE (" . $this->getCompanyCondition("b") . ") "
                . " GROUP BY p.vendor_id) a ON a.vendor_id = v.vendor_id"
                . " LEFT JOIN (SELECT e.vendor_id, SUM(IFNULL(e.paid, 0)) "
                . " as daybookpaid FROM {$tblpx}expenses e "
                . " WHERE e.type = 73 AND (" . $this->getCompanyCondition("e") . ") "
                . " GROUP BY e.vendor_id) b ON b.vendor_id = v.vendor_id"
                . " LEFT JOIN (SELECT d.vendor_id, SUM(IFNULL(d.amount, 0) + IFNULL(d.tax_amount, 0))"
                . "  as vendorpaid FROM {$tblpx}dailyvendors d "
                . " WHERE (" . $this->getCompanyCondition("d") . ") "
                . " GROUP BY d.vendor_id) c ON c.vendor_id = v.vendor_id"
                . " WHERE (" . $this->getCompanyCondition("v") . ") "
                . " AND v.vendor_type = 2 ORDER BY v.name";
            $sqlTotal       = "SELECT SUM(a.billtotal) as billsum, SUM(b.daybookpaid) "
                . " as daybooksum, SUM(b.daybookcredit) as daybookcreditsum , "
                . " SUM(c.vendorpaid) as vendorsum "
                . " FROM {$tblpx}vendors v"
                . " LEFT JOIN (SELECT p.vendor_id, SUM(IFNULL(b.bill_totalamount, 0) + IFNULL(b.bill_additionalcharge,0)) "
                . " as billtotal FROM {$tblpx}bills b "
                . " LEFT JOIN {$tblpx}purchase p ON b.purchase_id = p.p_id "
                . " WHERE (" . $this->getCompanyCondition("b") . ") "
                . " GROUP BY p.vendor_id) a ON a.vendor_id = v.vendor_id"
                . " LEFT JOIN (SELECT e.vendor_id, SUM(IFNULL(e.paid, 0)) "
                . " as daybookpaid ,SUM(IFNULL(e.amount, 0))  as daybookcredit "
                . " FROM {$tblpx}expenses e "
                . " WHERE e.type = 73 AND (" . $this->getCompanyCondition("e") . ") "
                . " GROUP BY e.vendor_id) b "
                . " ON b.vendor_id = v.vendor_id"
                . " LEFT JOIN (SELECT d.vendor_id, SUM(IFNULL(d.amount, 0) + IFNULL(d.tax_amount, 0))"
                . " as vendorpaid FROM {$tblpx}dailyvendors d "
                . " WHERE (" . $this->getCompanyCondition("d") . ")  "
                . " GROUP BY d.vendor_id) c ON c.vendor_id = v.vendor_id"
                . " WHERE (" . $this->getCompanyCondition("v") . ") "
                . " AND v.vendor_type != 2";
            $miscellaneousSqlTotal       = "SELECT SUM(a.billtotal) as billsum, SUM(b.daybookpaid) "
                . " as daybooksum, SUM(c.vendorpaid) as vendorsum "
                . " FROM {$tblpx}vendors v"
                . " LEFT JOIN (SELECT p.vendor_id, SUM(IFNULL(b.bill_totalamount, 0))"
                . " as billtotal FROM {$tblpx}bills b LEFT JOIN {$tblpx}purchase p "
                . " ON b.purchase_id = p.p_id "
                . " WHERE (" . $this->getCompanyCondition("b") . ") "
                . " GROUP BY p.vendor_id) a ON a.vendor_id = v.vendor_id"
                . " LEFT JOIN (SELECT e.vendor_id, SUM(IFNULL(e.paid, 0)) "
                . " as daybookpaid FROM {$tblpx}expenses e "
                . " WHERE e.type = 73 AND (" . $this->getCompanyCondition("e") . ") "
                . " GROUP BY e.vendor_id) b ON b.vendor_id = v.vendor_id"
                . " LEFT JOIN (SELECT d.vendor_id, SUM(IFNULL(d.amount, 0) + IFNULL(d.tax_amount, 0))"
                . " as vendorpaid FROM {$tblpx}dailyvendors d "
                . " WHERE (" . $this->getCompanyCondition("d") . ") "
                . " GROUP BY d.vendor_id) c ON c.vendor_id = v.vendor_id"
                . " WHERE (" . $this->getCompanyCondition("v") . ") AND v.vendor_type = 2";
            $paymentData    = Yii::app()->db->createCommand($sqlData)->queryAll();
            //echo "<pre>";print_r($paymentData);exit;

            $miscellaneouspaymentData    = Yii::app()->db->createCommand($miscellaneous_sql_data)->queryAll();
            $paymentSum     = Yii::app()->db->createCommand($sqlTotal)->queryRow();
            $miscellaneouspaymentSum     = Yii::app()->db->createCommand($miscellaneousSqlTotal)->queryRow();
            
            $daybookPayment = array();
            $daybookPayment1 = array();
            $daybookPayment_pending = array();
            $model          = "";
            $dailyvPayment  = array();
            $dailyvPayment1  = array();
            $expreceipt_return = array();
            $expreceipt_return1 = array();
            $reconciliation_data = array();
            $reconciliation_data1 = array();
            $defect_return = array();
        }
        $render_datas = array(
            'model' => $model,
            'vendor_id'     =>  $vendor_id,
            'company_id' => $company_id,
            'paymentData'       => $paymentData,
            'paymentSum'      => $paymentSum,
            'date_from'        => $date_from,
            'date_to'   => $date_to,
            'daybookPayment'    => $daybookPayment,
            'daybookPayment1' => $daybookPayment1,
            'dailyvPayment' => $dailyvPayment,
            'dailyvPayment1'  => $dailyvPayment1,
            'miscellaneouspaymentData' => $miscellaneouspaymentData,
            'miscellaneouspaymentSum' => $miscellaneouspaymentSum,
            'expreceipt_return' => $expreceipt_return,
            'expreceipt_return1' => $expreceipt_return1,
            'reconciliation_data' => $reconciliation_data,
            'reconciliation_data1' => $reconciliation_data1,
            'daybookPayment_pending ' => $daybookPayment_pending,
            'defect_return'=>$defect_return,
            'total_vendor_Amount'=>$total_vendor_Amount,//Invoice AMount
        );
            $exported_data = $this->renderPartial('paymentreport', $render_datas, true);
            $export_filename = 'Vendor Payment Report' . date('d_M_Y') ;
            $date_from  = isset($_REQUEST['date_from'])?$_REQUEST['date_from']:"";
            $date_to    = isset($_REQUEST['date_to'])?$_REQUEST['date_to']:"";
            $date_range='';
           
            

            // ExportHelper::exportGridAsCSV($exported_data, $export_filename);
            require_once (Yii::app()->basePath . '/components/simple_html_dom.php');
            $html = str_get_html($exported_data); // give this your HTML string
            header('Content-type: application/ms-excel');
            header('Content-Disposition: attachment; filename=' . $export_filename . '.csv');
            

            $fp = fopen("php://output", "w");
             if (!empty($date_from) && !empty($date_to)) {
                $heading = ['Data filtered between: ' . date('d-m-Y', strtotime($date_from)) . ' and ' . date('d-m-Y', strtotime($date_to))];
                fputcsv($fp, $heading);
            }elseif(!empty($datefrom)) {
                 $heading = ['Data From: ' . date('d-m-Y', strtotime($date_from)) ];
                fputcsv($fp, $heading);
                
            } elseif (!empty($date_to)) {
                    $heading= ['Data Upto :'.date('d-m-Y', strtotime($date_to))];
                    fputcsv($fp, $heading);
            } else {
                    // No date range to display
            }

            // Add an empty row for spacing
            fputcsv($fp, []);

            $contents = $html->find('tr');

            foreach ($contents as $element) {
                $td = array();
                foreach ($element->find('th') as $row) {
                    if (strpos(trim($row->class), 'actions') === false && strpos(trim($row->class), 'checker') === false) {
                        $td [] = str_replace('&nbsp;', '',$row->plaintext);
                    }
                }
                if (!empty($td)) {
                    fputcsv($fp, $td);
                }

                $td = array();
                foreach ($element->find('td') as $row) {
                    if (strpos(trim($row->class), 'actions') === false && strpos(trim($row->class), 'checker') === false) {
                        $td [] = str_replace('&nbsp;', '', $row->plaintext);
                    }
                }
                if (!empty($td)) {
                    fputcsv($fp, $td);
                }
            }

            fclose($fp);
            exit;


             /**All vendors  ends*/

        }
        
        
    }

    public function actionSavetopdf1()
    {
        $this->logo = $this->realpath_logo;
        $tblpx = Yii::app()->db->tablePrefix;
        $sqlData        = "SELECT v.name, a.billtotal, b.daybookpaid, c.vendorpaid FROM {$tblpx}vendors v
                            LEFT JOIN (SELECT p.vendor_id, SUM(IFNULL(b.bill_totalamount, 0)) as billtotal FROM {$tblpx}bills b LEFT JOIN {$tblpx}purchase p ON b.purchase_id = p.p_id WHERE (" . $this->getCompanyCondition("b") . ") GROUP BY p.vendor_id) a ON a.vendor_id = v.vendor_id
                            LEFT JOIN (SELECT e.vendor_id, SUM(IFNULL(e.paid, 0)) as daybookpaid FROM {$tblpx}expenses e WHERE e.type = 73 AND (" . $this->getCompanyCondition("e") . ") GROUP BY e.vendor_id) b ON b.vendor_id = v.vendor_id
                            LEFT JOIN (SELECT d.vendor_id, SUM(IFNULL(d.amount, 0) + IFNULL(d.tax_amount, 0)) as vendorpaid FROM {$tblpx}dailyvendors d WHERE (" . $this->getCompanyCondition("d") . ") GROUP BY d.vendor_id) c ON c.vendor_id = v.vendor_id
                            WHERE (" . $this->getCompanyCondition("v") . ") ORDER BY v.name";
        $sqlTotal       = "SELECT SUM(a.billtotal) as billsum, SUM(b.daybookpaid) as daybooksum, SUM(c.vendorpaid) as vendorsum FROM {$tblpx}vendors v
                            LEFT JOIN (SELECT p.vendor_id, SUM(IFNULL(b.bill_totalamount, 0)) as billtotal FROM {$tblpx}bills b LEFT JOIN {$tblpx}purchase p ON b.purchase_id = p.p_id WHERE (" . $this->getCompanyCondition("b") . ") GROUP BY p.vendor_id) a ON a.vendor_id = v.vendor_id
                            LEFT JOIN (SELECT e.vendor_id, SUM(IFNULL(e.paid, 0)) as daybookpaid FROM {$tblpx}expenses e WHERE e.type = 73 AND (" . $this->getCompanyCondition("e") . ") GROUP BY e.vendor_id) b ON b.vendor_id = v.vendor_id
                            LEFT JOIN (SELECT d.vendor_id, SUM(IFNULL(d.amount, 0) + IFNULL(d.tax_amount, 0)) as vendorpaid FROM {$tblpx}dailyvendors d WHERE (" . $this->getCompanyCondition("d") . ") GROUP BY d.vendor_id) c ON c.vendor_id = v.vendor_id
                            WHERE (" . $this->getCompanyCondition("v") . ")";
        $paymentData    = Yii::app()->db->createCommand($sqlData)->queryAll();
        $paymentSum     = Yii::app()->db->createCommand($sqlTotal)->queryRow();

        $mPDF1 = Yii::app()->ePdf->mPDF();
        $mPDF1 = Yii::app()->ePdf->mPDF('', 'A4');
        $sql = 'SELECT template_name '
                . 'FROM jp_quotation_template '
                . 'WHERE status="1"';
        $selectedtemplate =Yii::app()->db->createCommand($sql)->queryScalar();
        
        $template = $this->getTemplate($selectedtemplate);
        $mPDF1->SetHTMLHeader($template['header']);
        $mPDF1->SetHTMLFooter($template['footer']);
        $mPDF1->AddPage('','', '', '', '', 0,0,52, 30, 10,0);

        $mPDF1->WriteHTML($this->renderPartial('paymentreportpdf', array(
            'paymentData' => $paymentData, 'paymentSum' => $paymentSum,
        ), true));
        $filename = "Vendor payements";
        $mPDF1->Output($filename . '.pdf', 'D');
    }

    public function actionSavetoexcel1()
    {

        $tblpx          = Yii::app()->db->tablePrefix;
        $sqlData        = "SELECT v.name, a.billtotal, b.daybookpaid, c.vendorpaid FROM {$tblpx}vendors v
                            LEFT JOIN (SELECT p.vendor_id, SUM(IFNULL(b.bill_totalamount, 0)) as billtotal FROM {$tblpx}bills b LEFT JOIN {$tblpx}purchase p ON b.purchase_id = p.p_id WHERE (" . $this->getCompanyCondition("b") . ") GROUP BY p.vendor_id) a ON a.vendor_id = v.vendor_id
                            LEFT JOIN (SELECT e.vendor_id, SUM(IFNULL(e.paid, 0)) as daybookpaid FROM {$tblpx}expenses e WHERE e.type = 73 AND (" . $this->getCompanyCondition("e") . ") GROUP BY e.vendor_id) b ON b.vendor_id = v.vendor_id
                            LEFT JOIN (SELECT d.vendor_id, SUM(IFNULL(d.amount, 0) + IFNULL(d.tax_amount, 0)) as vendorpaid FROM {$tblpx}dailyvendors d WHERE (" . $this->getCompanyCondition("d") . ") GROUP BY d.vendor_id) c ON c.vendor_id = v.vendor_id
                            WHERE (" . $this->getCompanyCondition("v") . ") ORDER BY v.name";
        $sqlTotal       = "SELECT SUM(a.billtotal) as billsum, SUM(b.daybookpaid) as daybooksum, SUM(c.vendorpaid) as vendorsum FROM {$tblpx}vendors v
                            LEFT JOIN (SELECT p.vendor_id, SUM(IFNULL(b.bill_totalamount, 0)) as billtotal FROM {$tblpx}bills b LEFT JOIN {$tblpx}purchase p ON b.purchase_id = p.p_id WHERE (" . $this->getCompanyCondition("b") . ") GROUP BY p.vendor_id) a ON a.vendor_id = v.vendor_id
                            LEFT JOIN (SELECT e.vendor_id, SUM(IFNULL(e.paid, 0)) as daybookpaid FROM {$tblpx}expenses e WHERE e.type = 73 AND (" . $this->getCompanyCondition("e") . ") GROUP BY e.vendor_id) b ON b.vendor_id = v.vendor_id
                            LEFT JOIN (SELECT d.vendor_id, SUM(IFNULL(d.amount, 0) + IFNULL(d.tax_amount, 0)) as vendorpaid FROM {$tblpx}dailyvendors d WHERE (" . $this->getCompanyCondition("d") . ") GROUP BY d.vendor_id) c ON c.vendor_id = v.vendor_id
                            WHERE (" . $this->getCompanyCondition("v") . ")";
        $paymentData    = Yii::app()->db->createCommand($sqlData)->queryAll();
        $paymentSum     = Yii::app()->db->createCommand($sqlTotal)->queryRow();

        $arraylabel = array('sl no', 'Vendor name', 'Invoice', 'Payments', 'Balance');
        $finaldata  = array();

        $i          = 0;
        $sumTotal           = $paymentSum["daybooksum"] + $paymentSum["vendorsum"];
        $balanceSum         = $paymentSum["billsum"] - $sumTotal;
        $finaldata[$i][]    = '';
        $finaldata[$i][]    = 'Total';
        $finaldata[$i][]    = Controller::money_format_inr($paymentSum["billsum"], 2);
        $finaldata[$i][]    = Controller::money_format_inr($sumTotal, 2);
        $finaldata[$i][]    = Controller::money_format_inr($balanceSum, 2);
        $i          = 1;
        foreach ($paymentData as $paymentList) {
            $totalPayment       = $paymentList["daybookpaid"] + $paymentList["vendorpaid"];
            $balancePaid        = $paymentList["billtotal"] - $totalPayment;
            $finaldata[$i][]    = $i;
            $finaldata[$i][]    = $paymentList['name'];
            $finaldata[$i][]    = Controller::money_format_inr($paymentList['billtotal'], 2);
            $finaldata[$i][]    = Controller::money_format_inr($totalPayment, 2);
            $finaldata[$i][]    = Controller::money_format_inr($balancePaid, 2);
            $i++;
        }



        Yii::import('ext.ECSVExport');

        $csv = new ECSVExport($finaldata);

        $csv->setHeaders($arraylabel);
        $csv->setToAppend();
        $contentdata = $csv->toCSV();
        $csvfilename = 'vendor_payments' . date('d_M_Y') . '.csv';
        Yii::app()->getRequest()->sendFile($csvfilename, $contentdata, "text/csv", false);
        exit();
    }

    public function actiongetDailyVendorDetails()
    {
        $dailyvendorid = $_REQUEST["dailyvendorid"];
        $tblpx = Yii::app()->db->tablePrefix;
        $vendStatus = $_REQUEST["vendStatus"];
        $tblpx = Yii::app()->db->tablePrefix;
        $expenseTbl = "" . $tblpx . "dailyvendors";
        if ($vendStatus == 0) {
            $expenseTbl = "" . $tblpx . "pre_dailyvendors";
        }

        $paymentData = Yii::app()->db->createCommand("SELECT * FROM " . $expenseTbl . " WHERE daily_v_id = " . $dailyvendorid)->queryRow();

        $result["daily_v_id"] = $paymentData["daily_v_id"];
        $result["paydate"] = $paymentData["date"];
        $result["vendor_id"] = $paymentData["vendor_id"];
        $result["amount"] = $paymentData["amount"];
        $result["description"] = $paymentData["description"];
        $result["payment_type"] = $paymentData["payment_type"];
        $result["bank"] = $paymentData["bank"];
        $result["cheque_no"] = $paymentData["cheque_no"];
        $result["sgst"] = $paymentData["sgst"];
        $result["sgst_amount"] = $paymentData["sgst_amount"];
        $result["cgst"] = $paymentData["cgst"];
        $result["cgst_amount"] = $paymentData["cgst_amount"];
        $result["igst"] = $paymentData["igst"];
        $result["igst_amount"] = $paymentData["igst_amount"];
        $result["tds"] = isset($paymentData["tds"]) ? $paymentData["tds"] : 0;
        $result["tds_amount"] = isset($paymentData["tds_amount"]) ? $paymentData["tds_amount"] : 0;
        $result["paidamount"] = $paymentData["paidamount"];
        $result["tax_amount"] = $paymentData["tax_amount"];
        $result["project_id"] = $paymentData["project_id"];
        $result["company"] = $paymentData["company_id"];
        $result["purchase_id"] = $paymentData["purchase_id"];
        echo json_encode($result);
    }

    public function actionGetDataByDate()
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $newDate = date('Y-m-d', strtotime($_POST['date']));
        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        $newQuery = "";
        foreach ($arrVal as $arr) {
            if ($newQuery)
                $newQuery .= ' OR';
            $newQuery .= " FIND_IN_SET('" . $arr . "', d.company_id)";
        }
        $vendorData = Yii::app()->db->createCommand("SELECT d.*,d.created_date as created,v.*,ba.*,st.*,d.description as vendor_description,pr.name as project_name,d.company_id as company_id FROM " . $tblpx . "dailyvendors  d
                        LEFT JOIN " . $tblpx . "vendors v ON v.vendor_id = d.vendor_id
                        LEFT JOIN " . $tblpx . "bank ba ON d.bank = ba.bank_id
                        LEFT JOIN " . $tblpx . "status st ON d.payment_type = st.sid
                        LEFT JOIN " . $tblpx . "projects pr ON d.project_id = pr.pid
                        WHERE d.date = '" . $newDate . "' AND (" . $newQuery . ")")->queryAll();
        $client = $this->renderPartial('dailyentries_list', array('newmodel' => $vendorData));
        return $client;
    }

    public function actionAddDailyentries()
    {
        $model = new Dailyvendors;
        $reconmodel = new Reconciliation;
        $tblpx = Yii::app()->db->tablePrefix;
        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        $newQuery = "";
        foreach ($arrVal as $arr) {
            if ($newQuery)
                $newQuery .= ' OR';
            $newQuery .= " FIND_IN_SET('" . $arr . "', company_id)";
        }
        // expense alert mail
        $allocated_budget = array();
        if (isset($_GET['Dailyvendors']['project_id']) && $_GET['Dailyvendors']['project_id'] != NULL) {
            $project_id = $_GET['Dailyvendors']['project_id'];
            $project_model = Projects::model()->findByPk($_GET['Dailyvendors']['project_id']);
            $company_model = Company::model()->findByPk($_GET['Dailyvendors']['company_id']);
                        
            $expense_amount = Yii::app()->db->createCommand("SELECT SUM(amount) as amount FROM " . $tblpx . "expenses WHERE projectid = " . $project_id . " AND subcontractor_id IS NULL")->queryRow();
            $sub_amount = Yii::app()->db->createCommand("SELECT SUM( IFNULL(amount, 0) + IFNULL(sgst_amount, 0) + IFNULL(cgst_amount, 0) + IFNULL(igst_amount, 0)) as amount FROM " . $tblpx . "subcontractor_payment WHERE project_id = " . $project_id . " AND approve_status ='Yes'")->queryRow();
            $vendor_amount = Yii::app()->db->createCommand("SELECT SUM( IFNULL(amount, 0) + IFNULL(sgst_amount, 0) + IFNULL(cgst_amount, 0) + IFNULL(igst_amount, 0)) as amount FROM " . $tblpx . "dailyvendors WHERE project_id = " . $project_id . " AND project_id IS NOT NULL")->queryRow();

            $project_id = $_GET['Dailyvendors']['project_id'];
            if ($project_model->profit_margin != NULL && $project_model->project_quote != NULL && $company_model->expenses_percentage != NULL && $company_model->expenses_email != NULL) {
                $project_expense = (100 - ($project_model->profit_margin)) * (($project_model->project_quote) / 100);
                $amount = $_GET['Dailyvendors']['amount'] + $_GET['Dailyvendors']['tax_amount'] + $expense_amount['amount'] + $sub_amount['amount'] + $vendor_amount['amount'];
                $expenses_percentage = explode(",", $company_model->expenses_percentage);
                $percentage_array = array();
                foreach ($expenses_percentage as $key => $percentage) {
                    $final_amount = ($project_expense * $percentage / 100);
                    if ($amount >= $final_amount) {
                        $percentage_array[] = $percentage;
                    }
                }
                if (!empty($percentage_array)) {
                    $percentage = max($percentage_array);
                    if ($project_model->expense_percentage != $percentage) {
                        $allocated_budget['payment_alert'] = array('percentage' => $percentage, 'amount' => $amount, 'profit_margin' => $project_model->profit_margin, 'project_quote' => $project_model->project_quote, 'project_id' => $_GET['Dailyvendors']['project_id']);
                        $this->savePaymentAlert($allocated_budget['payment_alert']);
                    }
                    $amt_value = $_GET['Dailyvendors']['amount'] + $_GET['Dailyvendors']['tax_amount'];
                    $expense_alert = $this->saveExpenseNotifications($project_model, $percentage_array, $amt_value);
                    if (!empty($expense_alert)) {
                        $allocated_budget['expense_advance_alert'] = $expense_alert;
                    }
                } else {
                    $budget_percentage = NULL;
                }
                $update2 = Yii::app()->db->createCommand()->update($tblpx . 'projects', array('expense_amount' => $amount), 'pid=:pid', array(':pid' => $project_id));
            }
        }

        if (isset($_GET['Dailyvendors'])) {
            $model->attributes = $_GET['Dailyvendors'];
            $model->date = date('Y-m-d', strtotime($_GET['Dailyvendors']['date']));
            $model->created_by = Yii::app()->user->id;
            $model->created_date = date('Y-m-d');
           //$model->updated_date = date('Y-m-d');
            $model->reconciliation_status = NULL;
            $expense_totalamount = ($_GET['Dailyvendors']['sgst_amount'] + $_GET['Dailyvendors']['cgst_amount'] + $_GET['Dailyvendors']['igst_amount']) + $_GET['Dailyvendors']['amount'];
            $reconmodel->reconciliation_payment = "Vendor Payment";
            $reconmodel->reconciliation_amount = $_GET['Dailyvendors']['amount'] + $_GET['Dailyvendors']['tax_amount'];
            if ($_GET['Dailyvendors']['payment_type'] == 89) {
                $model->cheque_no = NULL;
                $model->bank = NULL;
            } else {
                $model->reconciliation_status = 0;
                $model->cheque_no = $_GET['Dailyvendors']['cheque_no'];
                $model->bank = (isset($_GET['Dailyvendors']['bank']) ? $_GET['Dailyvendors']['bank'] : "");
                $reconmodel->reconciliation_table = $tblpx . "dailyvendors";
                $reconmodel->reconciliation_paymentdate = date('Y-m-d', strtotime($_GET['Dailyvendors']['date']));
                $reconmodel->reconciliation_bank = (isset($_GET['Dailyvendors']['bank']) ? $_GET['Dailyvendors']['bank'] : "");
                $reconmodel->reconciliation_chequeno = $_GET['Dailyvendors']['cheque_no'];
                $reconmodel->created_date = date("Y-m-d H:i:s");
                $reconmodel->reconciliation_status = 0;
                $reconmodel->company_id = $_GET['Dailyvendors']['company_id'];
            }
            if ($model->save()) {
                $lastInsExpId = Yii::app()->db->getLastInsertID();
                if (!empty($allocated_budget)) {
                    $this->projectexpensealert($allocated_budget, $lastInsExpId, 'dailyvendors');
                }
                $reconmodel->reconciliation_parentid = $lastInsExpId;
                if ($_GET['Dailyvendors']['payment_type'] == 88) {
                    $reconmodel->save();
                }
                $payDate = date('Y-m-d', strtotime($_GET['Dailyvendors']['date']));

                $user = Users::model()->findByPk(Yii::app()->user->id);
                $arrVal = explode(',', $user->company_id);
                $newQuery = "";
                foreach ($arrVal as $arr) {
                    if ($newQuery)
                        $newQuery .= ' OR';
                    $newQuery .= " FIND_IN_SET('" . $arr . "', d.company_id)";
                }

                $vendorData = Yii::app()->db->createCommand("SELECT d.*,d.created_date as created,v.*,ba.*,st.*,d.description as vendor_description,pr.name as project_name,d.company_id as company_id FROM " . $tblpx . "dailyvendors  d
                        LEFT JOIN " . $tblpx . "vendors v ON v.vendor_id = d.vendor_id
                        LEFT JOIN " . $tblpx . "bank ba ON d.bank = ba.bank_id
                        LEFT JOIN " . $tblpx . "status st ON d.payment_type = st.sid
                        LEFT JOIN " . $tblpx . "projects pr ON d.project_id = pr.pid
                        WHERE d.date = '" . $payDate . "' AND (" . $newQuery . ")")->queryAll();
                // $client = $this->renderPartial('dailyentries_list', array('newmodel' => $vendorData));
                echo  $lastInsExpId;
                // return $client;
            } else {
                $error_message =   $this->setErrorMessage($model->getErrors());
                if ($error_message == 'Limit  Exceeded') {
                    $result_ = array('status' =>5,'error_message'=>$error_message);
                    echo json_encode($result_);
                    exit;
                } else {
                    $result_ = array('status' =>1,'error_message'=>$error_message);
                    echo json_encode($result_);
                    exit;

                }
            }
        }
    }

    public function setErrorMessage($modelErrors)
    {
        if (!empty($modelErrors)) {
            foreach ($modelErrors as $key => $value) {
                return $value[0];
            }
        }
    }

    public function actionUpdateDailyentries() {
        $id = $_GET['Dailyvendors']['txtDailyVendorId'];
        $model = Dailyvendors::model()->findByPk($id);
        $date = $_GET['Dailyvendors']['date'];
        $premodel = new PreDailyvendors;
        $reconmodel = Reconciliation::model()->findByAttributes(array(
            'reconciliation_parentid' => $id,
            'reconciliation_payment'=>'Vendor Payment'
        ));
        $tblpx = Yii::app()->db->tablePrefix;
        $created_by = $model->created_by;
        $created_date = $model->created_date;
        $budget_percentage = $model->budget_percentage;
        $model->updated_by = Yii::app()->user->role;
        $model->updated_date = date('Y-m-d', strtotime($date));
        $description = $_GET['Dailyvendors']['description'];
        $transaction = Yii::app()->db->beginTransaction();
        try {
            if (isset($_GET['Dailyvendors'])) {
                if (Yii::app()->user->role == 1) {
                    $model->attributes = $_GET['Dailyvendors'];
                    $model->date = date('Y-m-d', strtotime($date));
                    if ($model->attributes['payment_type'] == 88) {

                        $reconmodel->reconciliation_bank = $model->bank;
                        $reconmodel->reconciliation_chequeno = $model->cheque_no;
                        $reconmodel->reconciliation_status = 0;
                        $reconmodel->company_id = $model->attributes['company_id'];

                        $reconmodel->reconciliation_amount = $_GET['Dailyvendors']['amount'];
                        $reconmodel->reconciliation_payment = "Vendor Payment";
                        
                    }
                    
                    if (!$model->save()) {
                        $success_status = 0;
                        throw new Exception(json_encode($model->getErrors()));
                    } else {
                        $success_status = 1;
                        if ($model->payment_type == 88) {                           
                            if (!$reconmodel->save()) {                                
                                $success_status = 0;
                                throw new Exception(json_encode($reconmodel->getErrors()));
                            } else {
                               
                                $success_status = 1;
                            }
                        }
                    }
                } else {

                    $model->approval_status = 0;
                    $premodel->attributes = $_GET['Dailyvendors'];
                    $premodel->ref_id = $id;

                    $sql = "SELECT count(*) as count,MAX(daily_v_id) as lastId "
                            . " FROM " . $this->tableNameAcc('pre_dailyvendors', 0) . " "
                            . " WHERE ref_id='" . $id . "'";

                    $followupdata = Yii::app()->db->createCommand($sql)->queryRow();
                    if ($followupdata['count'] > 0) {
                        $premodel->followup_id = $followupdata['lastId'];
                    } else {
                        $premodel->followup_id = NULL;
                    }


                    $premodel->record_grop_id = date('Y-m-d H:i:s');
                    $premodel->approval_status = 0;
                    $premodel->record_action = "update";
                    $premodel->approve_notify_status = 1;
                    $premodel->created_by = $created_by;
                    $premodel->created_date = $created_date;
                    $premodel->updated_by = Yii::app()->user->id;
                    $premodel->updated_date = date('Y-m-d H:i:s');
                    $premodel->description = $description;
                    $premodel->date = date('Y-m-d', strtotime($date));

                    if (!$model->save()) {

                        $success_status = 0;
                        throw new Exception(json_encode($model->getErrors()));
                    } else {

                        $success_status = 1;
                        if (!$premodel->save()) {
                            $success_status = 0;
                            throw new Exception(json_encode($premodel->getErrors()));
                        } else {
                            $success_status = 1;
                        }
                    }
                }
            }
            $transaction->commit();
        } catch (Exception $error) {
            $transaction->rollBack();
            $success_status = 0;
            $error = $error->getMessage();
        } finally {
            if ($success_status == 1) {
                Yii::app()->user->setFlash('success', "Daybook data updated ");
                $result_ = '';
                echo $result_;
                exit;
            } else {
                Yii::app()->user->setFlash('error', $error);
                $result_ = 1;
                echo json_encode($result_);
                exit;
            }
        }
    }

    public function actionAjaxcall()
    {
        echo json_encode('success');
    }

    public function actionDeletdailyvendors()
    {
        $data = Dailyvendors::model()->findByPk($_POST['vendId']);
        $tblpx = Yii::app()->db->tablePrefix;
        if ($data) {
            $newmodel = new JpLog('search');
            $newmodel->log_data = json_encode($data->attributes);
            $newmodel->log_action = 2;
            $newmodel->log_table = $tblpx . "dailyvendors";
            $newmodel->log_datetime = date('Y-m-d H:i:s');
            $newmodel->log_action_by = Yii::app()->user->id;
            $newmodel->company_id = $data->company_id;
            if ($newmodel->save()) {
                $model = Dailyvendors::model()->deleteAll(array("condition" => "daily_v_id=" . $_POST['vendId'] . ""));
                Reconciliation::model()->deleteAll(array("condition" => "reconciliation_parentid=" . $_POST['vendId'] . ""));
                $payDate = date('Y-m-d', strtotime($_POST['vendors_date']));

                $user = Users::model()->findByPk(Yii::app()->user->id);
                $arrVal = explode(',', $user->company_id);
                $newQuery = "";
                foreach ($arrVal as $arr) {
                    if ($newQuery)
                        $newQuery .= ' OR';
                    $newQuery .= " FIND_IN_SET('" . $arr . "', d.company_id)";
                }

                $vendorData = Yii::app()->db->createCommand("SELECT d.*,d.created_date as created,v.*,ba.*,st.*,d.description as vendor_description,pr.name as project_name,d.company_id as company_id FROM " . $tblpx . "dailyvendors  d
                        LEFT JOIN " . $tblpx . "vendors v ON v.vendor_id = d.vendor_id
                        LEFT JOIN " . $tblpx . "bank ba ON d.bank = ba.bank_id
                        LEFT JOIN " . $tblpx . "status st ON d.payment_type = st.sid
                        LEFT JOIN " . $tblpx . "projects pr ON d.project_id = pr.pid
                        WHERE d.date = '" . $payDate . "' AND (" . $newQuery . ")")->queryAll();
                $client = $this->renderPartial('dailyentries_list', array('newmodel' => $vendorData));
                return $client;
            } else {
                echo 1;
            }
        }
    }


    public function actiondynamicproject()
    {
        
        $comId   = $_POST["comId"];
        $projects = array();
        $bank          = array();
        $selection="";

        $criteria = new CDbCriteria();
        $criteria->select= '*';
        $criteria->condition = "FIND_IN_SET(" . $comId . ", company_id)";

        $pmodel   = Projects::model()->findAll($criteria);
        $projects = CHtml::listData($pmodel,'pid','name');
        $pempty = array('empty'=>'-Select Project-'); 
        $projecData =  CHtml::listOptions($selection,$projects , $pempty);

        $bmodel = Bank::model()->findAll($criteria);
        $bank = CHtml::listData($bmodel,'bank_id','bank_name');
        $bempty = array('empty'=>'-Select Bank-'); 
        $bankData =  CHtml::listOptions($selection,$bank , $bempty);  
        
        echo json_encode(array('project' => $projecData, 'bank' => $bankData));

    }
    private function getCompanyCondition($prefix)
    {
        $user       = Users::model()->findByPk(Yii::app()->user->id);

        $arrVal     = explode(',', $user->company_id);
        $newQuery   = "";
        $prefix     = !empty($prefix) ? $prefix . "." : "";
        foreach ($arrVal as $arr) {
            if ($newQuery) $newQuery .= ' OR';
            $newQuery .= " FIND_IN_SET('" . $arr . "', {$prefix}company_id)";
        }
        return $newQuery;
    }
    public function actionDeleteconfirmation()
    {
        $expId      = $_REQUEST["expId"];
        $date       = $_REQUEST["expense_date"];
        $status     = $_REQUEST["status"];
        $delId      = $_REQUEST["delId"];
        $action     = $_REQUEST["action"];
        $tblpx      = Yii::app()->db->tablePrefix;
        $success    = "";
        if ($status == 1) {
            $deletedata = new Deletepending;
            $model      = Dailyvendors::model()->findByPk($expId);
            $deletedata->deletepending_table    = $tblpx . "dailyvendors";
            $deletedata->deletepending_data     = json_encode($model->attributes);
            $deletedata->deletepending_parentid = $expId;
            $deletedata->user_id                = Yii::app()->user->id;
            $deletedata->requested_date         = date("Y-m-d");
            if ($deletedata->save()) {
                $success = "Yes";
            } else {
                echo 1;
            }
        } else if ($status == 2) {
            $model  = Deletepending::model()->findByPk($delId);
            if ($model->delete()) {
                $success = "Yes";
            } else {
                echo 1;
            }
        }

        if ($success == "Yes") {
            $expDate    = date('Y-m-d', strtotime($_REQUEST['expense_date']));
            $user       = Users::model()->findByPk(Yii::app()->user->id);
            $arrVal     = explode(',', $user->company_id);
            $newQuery   = "";
            foreach ($arrVal as $arr) {
                if ($newQuery)
                    $newQuery .= ' OR';
                $newQuery .= " FIND_IN_SET('" . $arr . "', d.company_id)";
            }
            $vendorData = Yii::app()->db->createCommand("SELECT d.*,d.created_date as created,d.created_date as created,v.*,ba.*,st.*,d.description as vendor_description,pr.name as project_name,d.company_id as company_id FROM " . $tblpx . "dailyvendors  d
                            LEFT JOIN " . $tblpx . "vendors v ON v.vendor_id = d.vendor_id
                            LEFT JOIN " . $tblpx . "bank ba ON d.bank = ba.bank_id
                            LEFT JOIN " . $tblpx . "status st ON d.payment_type = st.sid
                            LEFT JOIN " . $tblpx . "projects pr ON d.project_id = pr.pid
                            WHERE d.date = '" . $expDate . "' AND (" . $newQuery . ")")->queryAll();
            $client     = $this->renderPartial('dailyentries_list', array('newmodel' => $vendorData));
            return $client;
        }
    }
    public function actionVendorpaymentsbyuser()
    {
        $tblpx      = Yii::app()->db->tablePrefix;
        $currUser   = Yii::app()->user->id;
        $model      = new Dailyvendors;
        $model->unsetAttributes();
        $model->created_by = $currUser;
        if (isset($_GET["Dailyvendors"])) {
            $model->fromdate   = $_REQUEST["Dailyvendors"]["fromdate"];
            $model->todate     = $_REQUEST["Dailyvendors"]["todate"];
        }
        $this->render('dailyvendorsbyuser', array(
            'model' => $model,
            'dataProvider' => $model->usersearch(),
        ));
    }
    public function actionDuplicateentry()
    {
        $tblpx  = Yii::app()->db->tablePrefix;
        $model  = new Dailyvendors;
        $entry  = Yii::app()->db->createCommand("SELECT  count(d.daily_v_id) as rowcount, d.* FROM {$tblpx}dailyvendors d
                    GROUP BY d.vendor_id, d.date, d.amount, d.description
                    HAVING COUNT(d.daily_v_id) > 1")->queryAll();
        $this->render('duplicateentry', array(
            'model' => $model,
            'duplicateentry' => $entry,
        ));
    }
    public function actionGetduplicateentries()
    {
        $vendor     = $_REQUEST["vendor"];
        $date       = $_REQUEST["date"];
        $amount     = $_REQUEST["amount"];
        $description = $_REQUEST["description"];
        $tblpx      = Yii::app()->db->tablePrefix;
        $duplicate  = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}dailyvendors WHERE vendor_id = '{$vendor}' AND date = '{$date}' AND  amount = '{$amount}' AND description='{$description}' ")->queryAll();
        $result     = '<button type="button" class="close"  aria-hidden="true">×</button><h4>Vendor Payment</h4><table class="table table-responsive table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Sl No.</th>
                                    <th>Company</th>
                                    <th>Project</th>
                                    <th>Vendor</th>
                                    <th>Description</th>            
                                    <th>Transaction Type</th>
                                    <th>Bank</th>
                                    <th>Cheque Number</th>
                                    <th>Amount</th>
                                    <th>Tax</th>
                                    <th>TDS Amount</th>
                                    <th>Paid Amount</th>
                                </tr>  
                            </thead>
                            <tbody>';
        $i = 1;
        foreach ($duplicate as $entry) {
            $result .= '         <tr>
                                    <td>' . $i . '</td>
                                    <td>';
            $company = Company::model()->findByPk($entry["company_id"]);
            if ($company)
                $result .= $company->name;
            $result .= '             </td>
                                    <td>';
            $project = Projects::model()->findByPk($entry["project_id"]);
            if ($project)
                $result .= $project->name;
            $result .= '             </td>
                                    <td>';
            $vendor = Vendors::model()->findByPk($entry["vendor_id"]);
            if ($vendor)
                $result .= $vendor->name;
            $result .= '             </td>
                                    <td>' . $entry["description"] . '</td>
                                    <td>';
            $exptype = Status::model()->findByPk($entry["payment_type"]);
            if ($exptype)
                $result .= $exptype->caption;
            $result .= '             </td>
                                    <td>';
            $bank = Bank::model()->findByPk($entry["bank"]);
            if ($bank)
                $result .= $bank->bank_name;
            $result .= '             </td>
                                    <td>' . $entry["cheque_no"] . '</td>
                                    <td class="text-right">' . Controller::money_format_inr($entry["amount"], 2) . '</td>
                                    <td class="text-right">' . Controller::money_format_inr($entry["tax_amount"], 2) . '</td>
                                    <td class="text-right">' . Controller::money_format_inr($entry["tds_amount"], 2) . '</td>
                                    <td class="text-right">' . Controller::money_format_inr($entry["paidamount"], 2) . '</td>
                                </tr>';
            $i++;
        }
        $result     .= '     </tbody>
                       </table>';
        echo $result;
    }
    public function actionGetAllData()
    {
        $payDate    = date('Y-m-d', strtotime($_REQUEST['date']));
        $tblpx      = Yii::app()->db->tablePrefix;
        $user       = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal     = explode(',', $user->company_id);
        $newQuery   = "";
        foreach ($arrVal as $arr) {
            if ($newQuery)
                $newQuery .= ' OR';
            $newQuery .= " FIND_IN_SET('" . $arr . "', d.company_id)";
        }
        $vendorData = Yii::app()->db->createCommand("SELECT d.*,d.created_date as created,v.*,ba.*,st.*,d.description as vendor_description,pr.name as project_name,d.company_id as company_id FROM " . $tblpx . "dailyvendors  d
                LEFT JOIN " . $tblpx . "vendors v ON v.vendor_id = d.vendor_id
                LEFT JOIN " . $tblpx . "bank ba ON d.bank = ba.bank_id
                LEFT JOIN " . $tblpx . "status st ON d.payment_type = st.sid
                LEFT JOIN " . $tblpx . "projects pr ON d.project_id = pr.pid
                WHERE d.date = '" . $payDate . "' AND (" . $newQuery . ")")->queryAll();
        $client = $this->renderPartial('dailyentries_list', array('newmodel' => $vendorData));
        return $client;
    }

    public function actiondynamicpurchase()
    {
        
        $comId   = $_POST["comId"];
        $projectId   = $_POST["projectId"];
        $vendorId   = $_POST["vendorId"];
        $purchaseId = $_POST["purchaseId"];
        $po_array = array();        
        $selection="";
        if($purchaseId !=""){
            $selection=$purchaseId;
        }
        
        if($vendorId !="" && $projectId !="" && $comId !=""){
            $sql = "SELECT * FROM jp_purchase p LEFT JOIN jp_bills b "
            . " ON p.p_id=b.purchase_id "
            . " WHERE p.purchase_status='saved' AND "
            . " `vendor_id` =".$vendorId." AND `project_id` =".$projectId 
            . " AND p.company_id=".$comId;
            $po_array = Yii::app()->db->createCommand($sql)->queryAll();
        }
        $po_data = CHtml::listData($po_array, 'p_id', 'purchase_no');
        $pempty = array('empty'=>'-Select Purchase-'); 
        $poData =  CHtml::listOptions($selection,$po_data , $pempty);

        echo json_encode(array('purchase' => $poData));

    }

    public function actiondeletevendor($id){
        $model = $this->loadModel($id);
        $transaction = Yii::app()->db->beginTransaction();
        try {
            if (!$model->delete()) {
                $success_status = 0;
                throw new Exception(json_encode($model->getErrors()));
            } else {
                $success_status = 1;
            }
            $transaction->commit();
        } catch (Exception $error) {
            $transaction->rollBack();
            $success_status = 0;
        } finally {
            if ($success_status == 1) {
                echo json_encode(array('response' => 'success', 'msg' => 'Data Deleted Successfully'));
            } else {
                if ($error->errorInfo[1] == 1451) {
                    echo json_encode(array('response' => 'warning', 'msg' => 'Cannot Delete ! This record alredy in use.'));
                } else {
                    echo json_encode(array('response' => 'error', 'msg' => 'Some Error Occured'));
                }
            }
        }
    }
}
