<?php
/* @var $this BankController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'Banks',
);
$page = Yii::app()->request->getParam('Bank_page');
if ($page != '') {
    Yii::app()->user->setReturnUrl($page);
} else {
    Yii::app()->user->setReturnUrl(0);
}
/*$this->menu=array(
	array('label'=>'Create Bank', 'url'=>array('create')),
	array('label'=>'Manage Bank', 'url'=>array('admin')),
);*/
?>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<div class="container" id="expensetype">
    <div class="loading-overlay">
        <span class="fa fa-spinner fa-3x fa-spin"></span>
    </div>
    <div class="clearfix">
        <div class="add-btn pull-right">
            <a style="cursor:pointer; " class="save_btn" href="<?php echo Yii::app()->createUrl('bank/savetopdf') ?>">SAVE AS PDF</a>
            <a style="cursor:pointer;margin-right: 3px; " class="save_btn" href="<?php echo Yii::app()->createUrl('bank/savetoexcel') ?>">SAVE AS EXCEL</a>

            <?php
            if (isset(Yii::app()->user->role) && (in_array('/bank/createbank', Yii::app()->user->menuauthlist))) {
            ?>
                <a class="button addbank">Add Bank</a>
            <?php } ?>
        </div>
        <h2>Bank</h2>
    </div>
    <div id="addbank" style="display:none;"></div>



    <?php //$this->renderPartial('_newsearch', array('model' => $model)) 
    ?>


    <div class="row">
        <div class="col-md-12">
            <?php $this->widget('zii.widgets.CListView', array(
                'dataProvider' => $dataProvider,
                'itemView' => '_newview',
                'template' => '<div>{sorter}</div><div class=""><table cellpadding="10" id="banktab" class="table">{items}</table></div>',

                'ajaxUpdate' => false,
            )); ?>
        </div>

        <!-- Add Expense type Popup -->
        <div id="addBank" class="modal fade" role="dialog">
            <div class="modal-dialog modal-lg">

            </div>
        </div>


        <!-- Edit Expense type Popup -->

        <div class="modal fade edit" role="dialog">
            <div class="modal-dialog modal-lg">


            </div>
        </div>
        <?php
        Yii::app()->clientScript->registerScript('myjavascript', '
             $(document).ready(function(){

               $("#banktab").dataTable( {
                    "scrollY": "300px",
                    "scrollCollapse": true,
                   
                     "columnDefs"        : [       
                        { 
                            "searchable"    : false, 
                            "targets"       : [0,2] 
                        },
                    ],


                } );




                });

            ');
        ?>
        <script>
            function closeaction() {
                $('#addbank').slideUp(500);
            }

            $(document).delegate('.editbank', 'click', function() {
                var id = $(this).attr('data-id');
                $('.loading-overlay').addClass('is-active');
                $.ajax({
                    type: "GET",
                    url: "<?php echo $this->createUrl('bank/update&layout=1&id=') ?>" + id,
                    success: function(response) {
                        $("#addbank").html(response).slideDown();
                        $('.loading-overlay').removeClass('is-active');
                    }
                });

            });

            $(document).ready(function() {

                $('.addbank').click(function() {
                    $('.loading-overlay').addClass('is-active');
                    var id = $(this).attr('data-id');
                    $.ajax({
                        type: "POST",
                        //dataType: "JSON",
                        url: "<?php echo $this->createUrl('bank/create&layout=1') ?>",
                        success: function(response) {
                            $('#addbank').html(response).slideDown();
                        },
                    });
                });
            });
            $(document).ajaxComplete(function() {
                $('.loading-overlay').removeClass('is-active');
                $('#loading').hide();
            });
        </script>



    </div>
</div>
<style>
    .savepdf {
        background-color: #6a8ec7;
        border: 1px solid #6a8ec8;
        color: #fff;
        padding: 5px;
    }

    .save_btn {
        margin-top: 15px;
    }

    .editable {
        float: right;
        cursor: pointer;
    }

    .add-btn a.button {
        margin-right: 5px;
    }

    .page-body h3 {
        margin: 4px 0px;
        color: inherit;
        text-align: left;
    }

    .panel {
        border: 1px solid #ddd;
    }

    .panel-heading {
        background-color: #eee;
        height: 40px;
    }

    .errorMessage1 {
        color: red;
    }
</style>