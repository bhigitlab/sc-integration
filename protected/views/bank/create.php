<?php
/* @var $this BankController */
/* @var $model Bank */

$this->breadcrumbs=array(
	'Banks'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Bank', 'url'=>array('index')),
	array('label'=>'Manage Bank', 'url'=>array('admin')),
);
?>

<div class="panel panel-gray">
    <div class="panel-heading form-head">
        <h3 class="panel-title">Add Bank</h3>
    </div>
        <?php echo $this->renderPartial('_form', array('model' => $model)); ?>
</div>