<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/message/css/jquery.message.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/message/js/jquery.message.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/tableHeadFixer.js"></script>
<script src="<?php echo Yii::app()->baseUrl . '/ckeditor/ckeditor.js'; ?>"></script>

<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'sales-invoice-form',
	'enableClientValidation' => true,
	'clientOptions' => array('validateOnChange' => true),
	'enableAjaxValidation' => false,
)); ?>

<div class="row">
	<div class="col-md-2">
		<div class="form-group">
			<input type="hidden" name="SalesInvoice[id]" id="SalesInvoice_id" value="<?php echo $model->id; ?>">
			<?php echo $form->labelEx($model, 'company_id'); ?>
			<?php
			echo $form->dropDownList($model, 'company_id', CHtml::listData(Company::model()->findAll(array('condition' => 'id IN (' . $user_companies . ')')), 'id', 'name'), array('ajax' => array(
				'type' => 'POST',
				'url' => Yii::app()->createUrl('projects/dynamicproject'),
				'dataType' => 'JSON',
				'success' => 'js:function(data)'
					. '{'
					. '    $("#SalesInvoice_project_id").html(data.projects_list);'
					. '    $("#SalesInvoice_project_id").val("");'
					. '}',
				'data' => array('company_id' => 'js:this.value'),
			), 'class' => 'form-control js-example-basic-single field_change require', 'empty' => '-Select Company-', 'style' => 'width:100%'));
			?>
			<?php echo $form->error($model, 'company_id'); ?>
		</div>
	</div>
	<div class="col-md-2">
		<div class="form-group">
			<?php echo $form->labelEx($model, 'project_id'); ?>
			<?php
			echo $form->dropDownList($model, 'project_id', CHtml::listData(Projects::model()->findAll(array('condition' => 'company_id IN (' . $user_companies . ')')), 'pid', 'name'), array('ajax' => array(
				'type' => 'POST',
				'url' => Yii::app()->createUrl('salesInvoice/getClient'),
				'dataType' => 'JSON',
				'success' => 'js:function(data)'
					. '{'
					. '    $("#client_name").text(data.client_name);'
					. '    $("#SalesInvoice_client_id").text(data.client_id);'
					. '}',
				'data' => array('project_id' => 'js:this.value'),
			), 'class' => 'form-control js-example-basic-single require', 'empty' => '-Select Project-', 'style' => 'width:100%'));
			?>
			<?php echo $form->error($model, 'project_id'); ?>
		</div>
	</div>
	<div class="col-md-2">
		<div class="form-group">
			<?php
			$client_name = '';
			if (!$model->isNewRecord) {
				$client_name = $model->client->name;
			}
			echo $form->labelEx($model, 'client_name'); ?>
			<input type="hidden" name="SalesInvoice[client_id]" id="SalesInvoice_client_id" class="">
			<div class="" id="client_name"><?php echo $client_name; ?></div>
			<?php echo $form->error($model, 'client_name'); ?>
		</div>
	</div>
	<div class="col-md-2">
		<div class="form-group">
			<?php echo $form->labelEx($model, 'invoice_date'); ?>
			<?php
			if ($model->isNewRecord)
				$model->invoice_date = date('Y-m-d');
			echo $form->textField($model, 'invoice_date', array('class' => 'form-control require')); ?>
			<?php echo $form->error($model, 'invoice_date'); ?>
		</div>
	</div>

	<div class="col-md-2">
		<div class="form-group">
			<?php echo $form->labelEx($model, 'invoice_number'); ?>
			<?php
			if ($model->isNewRecord)
				$model->invoice_number = $model->getMaxId();
			echo $form->textField($model, 'invoice_number',  array('class' => 'form-control require')); ?>
			<?php echo $form->error($model, 'invoice_number'); ?>
		</div>
	</div>
</div>
<?php $this->endWidget(); ?>
<?php
if (!$model->isNewRecord) {
?>
	<div id="items_holder">
		<div class="quotationmaindiv" id="items_list">
			<?php $this->renderPartial('_items_form', array('model' => $model, 'user_companies' => $user_companies, 'main_item_model' => $main_item_model, 'sub_item_model' => $sub_item_model)); ?>
		</div>
	</div>
<?php
}
?>
<script>
	$(function() {
		$("#SalesInvoice_invoice_date").datepicker({
			dateFormat: 'dd-mm-yy'
		});

	});
	$(document).ready(function() {
		setTimeout(function() {
			$('.flash-holder').fadeOut('2000');
		}, 1000);
		$(".js-example-basic-single").select2();
		$("#sales-invoice-form :input").change(function() {
			if (this.name == 'SalesInvoice[company_id]') {
				$('#SalesInvoice_project_id').val('');
				$('#client_name').text('');
			}
			var allRequired = true;
			var $inputs = $('#sales-invoice-form :input').filter('.require');
			var values = {};
			$inputs.each(function() {
				if ($(this).val() == '') {
					allRequired = false;
				}
				values[this.name] = $(this).val();
			});
			if (allRequired) {
				$("#sales-invoice-form").submit();
			}
		});
	});
	$(document).on('click', '.edit_sub_item', function() {
		var id = $(this).attr('id');
		$('.loading-overlay').addClass('is-active');
		$.ajax({
			type: "POST",
			data: {
				id: id,
			},
			dataType: 'JSON',
			url: "<?php echo Yii::app()->createUrl("salesInvoice/getItemById") ?>",
			success: function(data) {
				$.each(CKEDITOR.instances, function(i) {
					var editor = CKEDITOR.instances[i];
					editor.destroy();
				});
				$('.addItem').addClass('show');
				$('#SalesInvoiceSubItem_id').val(data.id);
				$("textarea#SalesInvoiceSubItem_item_description_" + data.invoice_item_id + '-0').val(data.item_description);
				CKEDITOR.replaceAll('text_editor');
				$('#SalesInvoiceSubItem_item_type_' + data.invoice_item_id + '-0').val(data.item_type);
				if (data.item_type === '1') {
					$('#sales-invoice-sub-item-form0').find('#rate_0,#quantity_0').show();
					$('#sales-invoice-sub-item-form0').find('#length_0,#width_0,#height_0').hide();
				} else if (data.item_type === '2') {
					$('#sales-invoice-sub-item-form0').find('#length_0,#width_0,#height_0,#rate_0,#quantity_0').hide();
					$('#sales-invoice-sub-item-form0').find('#length_0,#width_0,#height_0,#rate_0,#quantity_0').val('');
				} else if (data.item_type === '3') {
					$('#sales-invoice-sub-item-form0').find('#rate_0,#quantity_0,#length_0,#width_0').show();
					$('#sales-invoice-sub-item-form0').find('#height_0').hide();
					$('#sales-invoice-sub-item-form0').find('#height_0').val('');
				} else {
					$('#sales-invoice-sub-item-form0').find('#rate_0,#quantity_0,#length_0,#width_0,#height_0').show();
				}
				$('#SalesInvoiceSubItem_amount_' + data.invoice_item_id + '-0').val(data.amount);
				$('#SalesInvoiceSubItem_quantity_' + data.invoice_item_id + '-0').val(data.quantity);
				$('#SalesInvoiceSubItem_rate_' + data.invoice_item_id + '-0').val(data.rate);
				$('#SalesInvoiceSubItem_length_' + data.invoice_item_id + '-0').val(data.length);
				$('#SalesInvoiceSubItem_width_' + data.invoice_item_id + '-0').val(data.width);
				$('#SalesInvoiceSubItem_height_' + data.invoice_item_id + '-0').val(data.height);
				$('#SalesInvoiceSubItem_unit_' + data.invoice_item_id + '-0').val(data.height);
			}
		})
	})
	$(document).on('click', '.delete_item', function(e) {
		var answer = confirm("Are you sure you want to delete?");
		if (answer) {
			var rowId = $(this).attr("id");
			$('.loading-overlay').addClass('is-active');
			$.ajax({
				type: "POST",
				data: {
					id: rowId,
				},
				dataType: 'JSON',
				url: "<?php echo Yii::app()->createUrl("salesInvoice/deleteSubItem") ?>",
				success: function(data) {
					if (data.status === 1) {
						location.reload();
					}


				}
			})
		}

	})
</script>
<script>
	CKEDITOR.replaceAll('text_editor');
	$.each(CKEDITOR.instances, function(i) {
		var editor = CKEDITOR.instances[i];
		editor.on('blur', function() {
			var field_name = $(this).attr("name");
			var description_content = $('#cke_' + field_name).find('iframe').contents().find(".cke_editable").html();
			var field_values = field_name.split('-');
			if (field_values[0] === 'SalesInvoiceItem_main_description') {
				var lastChar = field_name[field_name.length - 1];
				$('#' + field_name).text(description_content);
				if (field_values[1] !== '' && field_values[1] !== undefined) {
					var form_id = 'sales-invoice-item-form' + lastChar;
					var title_value = $('#SalesInvoiceItem_main_title-' + lastChar).val();
				} else {
					var form_id = 'sales-invoice-item-form';
					var title_value = $('#SalesInvoiceItem_main_title-' + lastChar).val();
				}
				itemFieldChange(form_id);
			}
		});
	});
	$('.categ_toggle').click(function() {
		$(".toggle-caret").toggleClass(" fa-angle-up  fa-angle-down");
	})


	$(document).ready(function() {
		var current_id = 0;

		$('.add_new_row').click(function(e) {
			var row_id = this.id.split('-');
			var regex = /^(.+?)(\d+)$/i;
			e.preventDefault();
			var cloneIndex = $(".item_div-" + row_id[1]).length;
			$.each(CKEDITOR.instances, function(i) {
				var editor = CKEDITOR.instances[i];
				editor.destroy();
			});
			$("#item_div1-" + row_id[1]).clone(true, true)
				.appendTo(".clone_section-" + row_id[1])
				.attr("id", "item_div" + cloneIndex)
				.find("*")
				.each(function() {
					var id = this.id || "";
					var match = id.match(regex) || [];
					if (match.length == 3) {
						this.id = match[1] + (cloneIndex);
						$('#' + this.id).val('');

					}
				})
			CKEDITOR.replaceAll('text_editor');
		});
		$(document).on('change', '.item_form :input', function() {
			var form_id = $(this).parents("form").attr("id");
			itemFieldChange(form_id);
		});

		$(document).on('change', '.item_type', function() {
			var id = $(this).attr('id');
			var form_id = $(this).parents("form").attr("id");
			var field_name = id.split('-');
			var type_val = $(this).val();
			if (field_name[1] !== '') {
				$('#SalesInvoiceSubItem_amount-' + field_name[1]).val(parseFloat(0).toFixed(2));

				if (type_val === '1') {
					$('#' + form_id).find('#rate_' + field_name[1] + ',#quantity_' + field_name[1] + '').show();
					$('#' + form_id).find('#length_' + field_name[1] + ',#width_' + field_name[1] + ',#height_' + field_name[1] + '').hide();
					$('#' + form_id).find('#length_' + field_name[1] + ',#width_' + field_name[1] + ',#height_' + field_name[1] + '').val('');
				} else if (type_val === '2') {
					$('#' + form_id).find('#length_' + field_name[1] + ',#width_' + field_name[1] + ',#height_' + field_name[1] + ',#rate_' + field_name[1] + ',#quantity_' + field_name[1] + '').hide();
					$('#' + form_id).find('#length_' + field_name[1] + ',#width_' + field_name[1] + ',#height_' + field_name[1] + ',#rate_' + field_name[1] + ',#quantity_' + field_name[1] + '').val('');
				} else if (type_val === '3') {
					$('#' + form_id).find('#rate_' + field_name[1] + ',#quantity_' + field_name[1] + ',#length_' + field_name[1] + ',#width_' + field_name[1] + '').show();
					$('#' + form_id).find('#height_' + field_name[1] + '').hide();
					//$('#height_' + field_name[1] + '').addClass('dsfsd');
				} else {
					$('#' + form_id).find('#rate_' + field_name[1] + ',#quantity_' + field_name[1] + ',#length_' + field_name[1] + ',#width_' + field_name[1] + ',#height_' + field_name[1] + '').show();
				}
			}
		});
		$('.calculate').keypress(function(event) {
			return isNumber(event, this)
		});

		$(document).on('change', '.calculate', function() {
			var id = $(this).attr('id');
			var field_name = id.split('-');
			var main_item_id = $(this).attr('main-id');
			var amount = 0;
			if (field_name[1] !== '') {

				var quantity = $('#SalesInvoiceSubItem_quantity_' + main_item_id + '-' + field_name[1]).val();
				var rate = $('#SalesInvoiceSubItem_rate_' + main_item_id + '-' + field_name[1]).val();
				var length = $('#SalesInvoiceSubItem_length_' + main_item_id + '-' + field_name[1]).val();
				var width = $('#SalesInvoiceSubItem_width_' + main_item_id + '-' + field_name[1]).val();
				var height = $('#SalesInvoiceSubItem_height_' + main_item_id + '-' + field_name[1]).val();
				var type = $('#SalesInvoiceSubItem_item_type_' + main_item_id + '-' + field_name[1]).val();
				if (type === '1') {
					amount = quantity * rate;
				} else if (type === '2') {
					amount = 0;
				} else if (type === '3') {
					amount = quantity * rate * length * width;
				} else if (type === '4') {
					amount = quantity * rate * length * width * height;
				}

				$('#SalesInvoiceSubItem_amount_' + main_item_id + '-' + field_name[1]).val(parseFloat(amount).toFixed(2));
			}


		})
		$(document).on('change', '.tax_items', function() {
			var val = $(this).val();
			if (val > 100) {
				$().toastmessage('showErrorToast', "This percentage must be less than 100%");
				$(this).val('');
			}
		});
		$("#SalesInvoice_cgst_p, #SalesInvoice_sgst_p, #SalesInvoice_igst_p, #SalesInvoice_cess_p, #SalesInvoice_round_off").blur(function() {
			var amount = parseFloat($("#SalesInvoice_amount").val());
			var cess = parseFloat($("#SalesInvoice_cess_p").val());
			var sgst = parseFloat($("#SalesInvoice_sgst_p").val());
			var cgst = parseFloat($("#SalesInvoice_cgst_p").val());
			var igst = parseFloat($("#SalesInvoice_igst_p").val());
			var roundOff = parseFloat($("#SalesInvoice_round_off").val());
			var new_amount = 0;
			console.log(amount);
			var sgst_amount = (sgst / 100) * amount;
			var cgst_amount = (cgst / 100) * amount;
			var igst_amount = (igst / 100) * amount;
			var cess_amount = (cess / 100) * amount;

			if (isNaN(sgst_amount)) sgst_amount = 0;
			if (isNaN(cgst_amount)) cgst_amount = 0;
			if (isNaN(igst_amount)) igst_amount = 0;
			if (isNaN(cess_amount)) cess_amount = 0;
			if (isNaN(roundOff)) roundOff = 0;
			if (isNaN(amount)) amount = 0;
			var tax_amount = sgst_amount + cgst_amount + igst_amount;
			var total_amount = amount + tax_amount + cess_amount + (roundOff);
			$("#SalesInvoice_sgst_amount").val(sgst_amount.toFixed(2));
			$("#sgst_amount").text(sgst_amount.toFixed(2));
			$("#SalesInvoice_cgst_amount").val(cgst_amount.toFixed(2));
			$("#cgst_amount").text(cgst_amount.toFixed(2));
			$("#SalesInvoice_igst_amount").val(igst_amount.toFixed(2));
			$("#igst_amount").text(igst_amount.toFixed(2));
			$("#SalesInvoice_cess_amount").val(cess_amount.toFixed(2));
			$("#cess_amount").text(cess_amount.toFixed(2));
			$("#total_amount_val").html(total_amount.toFixed(2));
			$("#SalesInvoice_total_amount").val(total_amount.toFixed(2));

		});
	});

	function itemFieldChange(form_id) {

		var allRequired = true;

		var $inputs = $('#' + form_id + ' :input').filter('.require');
		var values = {};
		$inputs.each(function() {
			if ($(this).val() == '') {
				allRequired = false;
			}
			values[this.name] = $(this).val();
		});
		if (allRequired) {
			var form_key = form_id.toString().slice(-1);
			$('.loading-overlay').addClass('is-active');
			$.ajax({
				type: "POST",
				data: $('#' + form_id).serialize(),
				dataType: 'JSON',
				url: "<?php echo Yii::app()->createUrl("salesInvoice/addItem") ?>",
				success: function(data) {
					if (data.status === 1) {
						$('#categ_title_' + form_key).text(data.model.main_title);
						$().toastmessage('showSuccessToast', data.message);
						if (data.model.main_title != "" && data.model.main_description != "") {
							console.log("data here");
							location.reload();
							$('.loading-overlay').addClass('is-active');
						} else {
							console.log("no data !!!");
						}

					} else {
						$().toastmessage('showWarningToast', data.message);
					}
				}
			})
			// var form_data = $("#" + form_id).serialize();
			// console.log(form_data);
			// $("#" + form_id).submit();
		} else {
			$("#" + form_id).find('.text-danger').removeClass('hide');
		}
	}

	function isNumber(evt, element) {
		var charCode = (evt.which) ? evt.which : event.keyCode
		if (
			(charCode != 46 || $(element).val().indexOf('.') != -1) && // “.” CHECK DOT, AND ONLY ONE.
			(charCode < 48 || charCode > 57))
			return false;
		return true;
	}
	$(document).ajaxComplete(function() {
		$('.loading-overlay').removeClass('is-active');
		$('#loading').hide();
	});
</script>