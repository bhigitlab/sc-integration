<?php

/**
 * This is the model class for table "{{defect_returnitem}}".
 *
 * The followings are the available columns in table '{{defect_returnitem}}':
 * @property integer $returnitem_id
 * @property integer $return_id
 * @property integer $receiptitem_id
 * @property string $returnitem_description
 * @property double $returnitem_quantity
 * @property string $returnitem_unit
 * @property double $returnitem_rate
 * @property double $returnitem_amount
 * @property integer $category_id
 * @property string $remark
 * @property string $created_date
 * @property integer $created_by
 */
class DefectReturnitem extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{defect_returnitem}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('return_id, returnitem_quantity, returnitem_unit, returnitem_rate, returnitem_amount, created_date, created_by', 'required'),
            array('return_id, receiptitem_id, category_id, created_by', 'numerical', 'integerOnly'=>true),
            array('returnitem_quantity, returnitem_rate, returnitem_amount', 'numerical'),
            array('returnitem_unit', 'length', 'max'=>20),
            array('remark', 'length', 'max'=>100),
            array('returnitem_description', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('returnitem_id, return_id, receiptitem_id, returnitem_description, returnitem_quantity, returnitem_unit, returnitem_rate, returnitem_amount, category_id, remark, created_date, created_by', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'returnitem_id' => 'Returnitem',
            'return_id' => 'Return',
            'receiptitem_id' => 'Receiptitem',
            'returnitem_description' => 'Returnitem Description',
            'returnitem_quantity' => 'Returnitem Quantity',
            'returnitem_unit' => 'Returnitem Unit',
            'returnitem_rate' => 'Returnitem Rate',
            'returnitem_amount' => 'Returnitem Amount',
            'category_id' => 'Category',
            'remark' => 'Remark',
            'created_date' => 'Created Date',
            'created_by' => 'Created By',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('returnitem_id',$this->returnitem_id);
        $criteria->compare('return_id',$this->return_id);
        $criteria->compare('receiptitem_id',$this->receiptitem_id);
        $criteria->compare('returnitem_description',$this->returnitem_description,true);
        $criteria->compare('returnitem_quantity',$this->returnitem_quantity);
        $criteria->compare('returnitem_unit',$this->returnitem_unit,true);
        $criteria->compare('returnitem_rate',$this->returnitem_rate);
        $criteria->compare('returnitem_amount',$this->returnitem_amount);
        $criteria->compare('category_id',$this->category_id);
        $criteria->compare('remark',$this->remark,true);
        $criteria->compare('created_date',$this->created_date,true);
        $criteria->compare('created_by',$this->created_by);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return DefectReturnitem the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}