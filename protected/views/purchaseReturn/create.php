<?php
/* @var $this BillsController */
/* @var $model Bills */

$this->breadcrumbs = array(
    'Bills' => array('index'),
    'Create',
);

$this->menu = array(
    //array('label'=>'List Bills', 'url'=>array('index')),
    //array('label'=>'Manage Bills', 'url'=>array('admin')),
);
?>

<div class="container" id="expense">
    <div class="loading-overlay">
        <span class="fa fa-spinner fa-3x fa-spin"></span>
    </div>
    <div class="expenses-heading sub-heading mb-10">
        <h3>Purchase Return</h3>
        <button href="<?php echo Yii::app()->createAbsoluteUrl('PurchaseReturn/admin'); ?>" class="btn btn-info "
            id="list-purchase-return">List Purchase Return</button>
        <div id="loading"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/ajax-loader.gif"></div>
    </div>
    <?php $this->renderPartial('_form', array('model' => $model)); ?>
</div>
<script>
    $(document).ready(function () {
        $('#loading').hide();
        $("#list-purchase-return").click(function () {
            let location = $("#list-purchase-return").attr("href");
            window.location.href = location;
        })
    });
</script>