<div class="alert alert-success get_prevdata alert-dismissible fade in" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
    <div id="parent">
        <table style="width:100%;" id="pre_fixtable">
            <thead>
                <tr>
                    <th>Sl.No</th>
                    <th>Project</th>
                    <th>Bill No</th>
                    <th>Purchase No</th>
                    <th>Bill Date</th>
                    <th>Vendor</th>
                    <th>Quantity</th>
                    <th>Unit</th>
                    <th>HSN Code</th>
                    <th>Rate</th>
                    <th>Discount(%)</th>
                    <th>Tax Slab</th>
                    <th>Amount</th>
                </tr>
            <thead>
            <tbody>
                <tr>
                    <td colspan="9">
                        <p>
                            <?php echo ucwords($category['category_name']) . $brand . '-' . ucwords($specification['specification']) . ' Previous "' . count($previous_bill_data) . '" ​Purchase Rate " ' ?>
                        </p>
                    </td>
                </tr>
                <?php
                foreach ($previous_bill_data as $key => $value) {
                    $category_id = $_REQUEST['category_id'];
                    ?>
                
                    <tr class="getprevious" style="cursor:pointer;" 
                        data-id="<?php
                        echo $category_id . ',
                        ' . $value['billitem_quantity'] . ',
                        ' . $value['billitem_unit'] . ',
                        ' . $value['billitem_hsn_code'] . ',
                        ' . $value['billitem_rate'] . ',
                        ' . $value['billitem_amount'] . ',
                        ' . $value['billitem_taxamount'] . ', 
                        ' . $value['billitem_discountpercent'] . ', 
                        ' . $value['billitem_discountamount'] . ', 
                        ' . $value['billitem_cgst'] . ', 
                        ' . $value['billitem_cgstpercent'] . ', 
                        ' . $value['billitem_sgst'] . ', 
                        ' . $value['billitem_sgstpercent'] . ', 
                        ' . $value['billitem_igst'] . ', 
                        ' . $value['billitem_igstpercent'] . ', 
                        ' . $value['billitem_taxslab'] . ', 
                        ' . $value['batch']
                        ?>">
                        <td><?php echo ($key + 1) ?> </td>
                        <td><?php echo $value['project'] ?></td>
                        <td><?php echo $value['bill_number'] ?></td>
                        <td><?php echo $value['purchase_no'] ?></td>
                        <td>
                            <?php echo date("Y-m-d", strtotime($value['bill_date'])) ?>
                        </td>
                        <td><?php echo $value['vendor'] ?></td>
                        <td><?php echo $value['billitem_quantity'] ?></td>
                        <td><?php echo $value['billitem_unit'] ?></td>
                        <td><?php echo $value['billitem_hsn_code'] ?></td>
                        <td><?php echo $value['billitem_rate'] ?></td>
                        <td><?php echo $value['billitem_discountpercent'] ?></td>
                        <td><?php echo $value['billitem_taxslab'] ?></td>
                        <td><?php echo $value['billitem_amount'] ?></td>
                    </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
    </div>
</div>
