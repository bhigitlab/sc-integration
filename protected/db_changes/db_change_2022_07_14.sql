UPDATE `jp_expenses` SET `reconciliation_status`= NULL WHERE `payment_type` = 89 AND `reconciliation_status` = 0;

UPDATE `jp_expenses` SET `reconciliation_status`= NULL WHERE `expense_type` = 89 AND `reconciliation_status` = 0;

UPDATE `jp_dailyexpense` SET `reconciliation_status`=NULL WHERE `expense_type` = 89 AND `reconciliation_status` = 0;

UPDATE `jp_dailyvendors` SET `reconciliation_status`= NULL WHERE `payment_type` = 89 AND `reconciliation_status` = 0;

UPDATE `jp_subcontractor_payment`  SET `reconciliation_status`= NULL WHERE `payment_type` = 89 AND `reconciliation_status` = 0;