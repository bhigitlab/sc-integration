<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.js"></script>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/message/css/jquery.message.css">
<script type="text/javascript"
	src="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/message/js/jquery.message.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/tableHeadFixer.js"></script>
<script>
	var shim = (function () {
		document.createElement('datalist');
	})();
</script>

<script>
	$(function () {
		$("#datepicker").datepicker({
			dateFormat: 'dd-mm-yy'
		});
		$("#delivery_date").datepicker({
			dateFormat: 'dd-mm-yy'
		});

	});
</script>

<div class="container">

	<div class="invoicemaindiv">
		<h3 class="purchase-title">Purchase Orders</h3>
		<div id="msg_box"></div>
		<div class="entries-wrapper">
			<div class="row">
				<div class="col-xs-12">
					<div class="heading-title">Details</div>
					<div class="dotted-line"></div>
				</div>
			</div>
			<form id="pdfvals1" method="post" action="<?php echo $this->createAbsoluteUrl('purchase/addpurchase'); ?>">
				<input type="hidden" name="remove" id="remove" value="">
				<input type="hidden" name="purchase_id" id="purchase_id" value="<?php echo $model->p_id; ?>">
				<div class="block_purchase">
					<div class="row_block" style="margin-left: -16px;">
						<div class="col-xs-12 col-sm-4 col-md-3">
							<div class="form-group">
								<label>COMPANY : </label>
								<?php
								$user = Users::model()->findByPk(Yii::app()->user->id);
								$arrVal = explode(',', $user->company_id);
								$newQuery = "";
								foreach ($arrVal as $arr) {
									if ($newQuery)
										$newQuery .= ' OR';
									$newQuery .= " FIND_IN_SET('" . $arr . "', id)";
								}
								$companyInfo = Company::model()->findAll(array('condition' => $newQuery));
								?>
								<select name="company_id"
									class="inputs target company change_val js-example-basic-single" id="company_id"
									style="width:190px">
									<option value="">Choose Company</option>
									<?php
									foreach ($companyInfo as $key => $value) {
										?>
										<option value="<?php echo $value['id'] ?>" <?php echo ($value['id'] == $model->company_id) ? 'selected' : ''; ?>>
											<?php echo $value['name']; ?>
										</option>
										<?php
									}
									?>
								</select>
							</div>
						</div>
						<div class="col-xs-12 col-sm-4 col-md-3">
							<div class="form-group">
								<label>PROJECT : </label>
								<select name="project" class="inputs target project" id="project" style="width:190px">
									<option value="">Choose Project</option>
									<?php
									foreach ($project as $key => $value) {
										?>
										<option value="<?php echo $value['pid']; ?>" <?php echo ($value['pid'] == $model->project_id) ? 'selected' : ''; ?>>
											<?php echo $value['name']; ?>
										</option>
										<?php
									}
									?>

								</select>
							</div>
						</div>

						<div class="col-xs-12 col-sm-4 col-md-3">
							<div class="form-group">

								<label>EXPENSE HEAD : </label>
								<select name="expense_head" class="inputs target expense_head" id="expense_head"
									style="width:190px">
									<option value="">Choose Expense Head</option>
									<?php
									foreach ($expense_head as $value) {
										?>
										<option value="<?php echo $value['type_id']; ?>" <?php echo ($value['type_id'] == $model->expensehead_id) ? 'selected' : ''; ?>>
											<?php echo $value['type_name']; ?>
										</option>
										<?php
									}
									?>
								</select>
							</div>
						</div>

						<div class="col-xs-12 col-sm-4 col-md-3">
							<div class="form-group">

								<label>VENDOR : </label>
								<select name="vendor" class="inputs target vendor" id="vendor" style="width:190px">
									<option value="">Choose Vendor</option>
									<?php
									foreach ($vendor as $key => $value) {
										?>
										<option value="<?php echo $value['vendorid']; ?>" <?php echo ($value['vendorid'] == $model->vendor_id) ? 'selected' : ''; ?>>
											<?php echo $value['vendorname']; ?>
										</option>
										<?php
									}
									?>
								</select>
							</div>
						</div>
						<div class="col-xs-12 col-sm-3 col-md-2">
							<div class="form-group">
								<label>PURCHASE NO : </label>
								<!-- onkeypress="filterDigits(event)" -->
								<input type="text" required
									value="<?php echo ((isset($model->purchase_no) && $model->purchase_no != '') ? $model->purchase_no : ''); ?>"
									class="txtBox inputs target check_type purchaseno form-control" name="purchaseno"
									id="purchaseno" placeholder="">
							</div>
						</div>
						<div class="col-xs-12 col-sm-3 col-md-2">
							<div class="form-group">
								<label>DATE : </label>
								<input type="text"
									value="<?php echo ((isset($model->purchase_date) && $model->purchase_date != '') ? date("d-m-Y", strtotime($model->purchase_date)) : date("d-m-Y")); ?>"
									id="datepicker" class="txtBox date inputs target form-control" name="date"
									placeholder="Please click to edit">
							</div>
						</div>

						<div class="col-xs-12 col-sm-4 col-md-3">
							<div class="form-group">
								<label>EXPECTED DELIVERY DATE : </label>
								<input type="text"
									value="<?php echo ((isset($model->expected_delivery_date) && $model->expected_delivery_date != '') ? date("d-m-Y", strtotime($model->expected_delivery_date)) : date("d-m-Y")); ?>"
									class="txtBox inputs target check_type delivery_date form-control"
									name="delivery_date" id="delivery_date" placeholder="">
							</div>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
				<br>
				<div id="msg"></div>
				<div id="previous_details"
					style="position: fixed;top: 94px; left: 70px; right: 70px; z-index: 2; max-width:1150px; margin:0 auto;">
				</div>

				<div class="purchase_items">
					<div class="row">
						<div class="col-xs-12">
							<div class="heading-title">Add/Edit Purchase Item</div>
							<div class="dotted-line"></div>
						</div>
					</div>
					<div>
						<div class="row">
							<div class="col-md-9">
								<div class="row">
									<div class="purchaseitem col-md-3 form-group">
										<div id="select_div">
											<label>Specification:</label>
											<select style="width:200px;" class="txtBox specification description "
												id="description" name="description[]">
												<option value="">Select one</option>
												<?php
												foreach ($specification as $key => $value) {
													?>
													<option value="<?php echo $value['id']; ?>">
														<?php echo $value['data']; ?>
													</option>
												<?php } ?>
												<option value="other">Other</option>
											</select>
										</div>
									</div>
									<div class="purchaseitem length_div  col-md-2  form-group">
										<label>Length:</label>
										<input type="text"
											class="inputs target txtBox length allownumericdecimal form-control"
											id="length" name="length[]" placeholder="" /></td>
									</div>
									<div class="purchaseitem quantity_div  col-md-2  form-group">
										<label>Multiplying Quantity:</label>
										<input type="text"
											class="inputs target txtBox quantity allownumericdecimal  form-control"
											id="quantity" name="quantity[]" placeholder="Quantity" /></td>
									</div>
									<div class="purchaseitem quantity_div col-md-2  form-group">
										<label>Effective Quantity:</label>
										<input type="text"
											class="inputs target txtBox effective_quantity allownumericdecimal  form-control"
											id="effective_quantity" name="effective_quantity[]" placeholder=""
											readonly=ture />
									</div>
									<div class="purchaseitem col-md-1  form-group">
										<label>Units:</label>
										<div id="item_unit" class="item_unit padding-box  form-control">&nbsp;&nbsp;
										</div>
									</div>
									<div class="purchaseitem col-md-2  form-group">
										<label>Rate:</label>
										<input type="text"
											class="inputs target txtBox rate allownumericdecimal  form-control"
											id="rate" name="rate[]" placeholder="Rate" />
									</div>
								</div>
								<div class="row">
									<div class="clearfix"></div>
									<div class="purchaseitem col-md-2">
										<div class="form-group">
											<label>Dis amount :</label>
											<input type="text"
												class="inputs target small_class txtBox sgst allownumericdecimal form-control calculation"
												id="dis_amount" name="dis_amount" placeholder="" />
											<div id="disp" class="padding-box">0.00</div>(%)
										</div>
									</div>
									<div class="purchaseitem col-md-1">
										<div id="select_div" class="form-group">
											<label class="nowrap">Tax Slab:</label>
											<?php
											$datas = TaxSlabs::model()->getAllDatas();
											?>
											<select class="form-control txtBox tax_slab" id="tax_slab"
												name="tax_slab[]">
												<option value="">Select one</option>
												<?php
												foreach ($datas as $value) {
													if ($value['set_default'] == 1) {
														echo ' <option data-id="' . $value['id'] . '" value="' . $value['tax_slab_value'] . '" selected>' . $value['tax_slab_value'] . '%</option>';
													} else {
														echo ' <option  data-id="' . $value['id'] . '" value="' . $value['tax_slab_value'] . '">' . $value['tax_slab_value'] . '%</option>';
													}
												}
												?>
											</select>
										</div>
									</div>
									<div class="purchaseitem gsts col-md-2">
										<div class="form-group">
											<label>SGST :</label>
											<input type="text"
												class="inputs target small_class txtBox sgstp allownumericdecimal form-control gst_percentage"
												id="sgstp" name="sgstp" placeholder="" />
											<div id="sgst_amount" class="padding-box">0.00</div>
										</div>
									</div>
									<div class="purchaseitem gsts col-md-2">
										<div class="form-group">
											<label>CGST :</label>
											<input type="text"
												class="inputs target txtBox cgstp small_class allownumericdecimal form-control"
												id="cgstp" name="cgstp" placeholder="" />
											<div id="cgst_amount" class="padding-box">0.00</div>
										</div>
									</div>
									<div class="purchaseitem gsts col-md-2">
										<div class="form-group">
											<label>IGST :</label>
											<input type="text"
												class="inputs target txtBox igstp  small_class allownumericdecimal form-control"
												id="igstp" name="igstp" placeholder="" />
											<div id="igst_amount" class="padding-box">0.00</div>
										</div>
									</div>
									<div class="purchaseitem col-md-3">
										<label>Remarks:</label>
										<input type="text" class="txtBox form-control" id="remarks" name="remark[]"
											placeholder="Remark" />
									</div>
								</div>
							</div>
							<div class="col-md-3 text-right">
								<div class="list-container">
									<div class="purchaseitem list-item">
										<label>Amount:</label>
										<div id="item_amount" class="padding-box">0.00</div>
									</div>
									<div class="list-item">
										<label>Discount Amount:</label>
										<div id="disc_amount" class="padding-box">0.00</div>
									</div>
									<div class="list-item">
										<label>Tax Amount: </label>
										<div id="tax_amount" class="padding-box">0.00</div>
									</div>
									<div class="list-item">
										<label>Total Amount: </label>
										<div id="total_amount" class="padding-box">0.00</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xs-12 text-right margin-bottom-10">
							<input type="button" class="item_save btn btn-info btn-sm" id="0" value="Save">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<div id="table-scroll" class="table-scroll">
							<div id="faux-table" class="faux-table" aria="hidden"></div>
							<div class="table-wrap">
								<div class="table-responsive">
									<table border="1" class="table main-table" id="main-table">
										<thead class="entry-table">
											<tr>
												<th class="text_align">Sl.No</th>
												<th class="text_align">Specification</th>
												<th class="text_align">Length</th>
												<th class="text_align">Quantity</th>
												<th class="text_align">Unit</th>
												<th class="text_align">Rate</th>
												<th class="text_align">Amount</th>
												<th>Tax Slab (%)</th>
												<th>SGST Amount</th>
												<th>SGST (%)</th>
												<th>CGST Amount</th>
												<th>CGST (%)</th>
												<th>IGST Amount</th>
												<th>IGST (%)</th>
												<th>Discount (%)</th>
												<th class="text_align">Discount Amount</th>
												<th>Tax Amount</th>
												<th class="text_align">Total</th>
												<th class="text_align">Remark</th>
												<th></th>
											</tr>
										</thead>
										<tbody class="addrow">
											<?php

											$i = 1;
											$tot_qty = 0;
											foreach ($item_model as $new) {
												$tot_qty += $new['quantity'];

												$tblpx = Yii::app()->db->tablePrefix;
												$descsql = "SELECT * FROM {$tblpx}specification WHERE id=" . $new['category_id'] . "";
												$desctiptions = Yii::app()->db->createCommand($descsql)->queryRow();
												$parent_sql = "SELECT * FROM {$tblpx}purchase_category WHERE id='" . $desctiptions['cat_id'] . "'";
												$parent_category = Yii::app()->db->createCommand($parent_sql)->queryRow();
												if ($desctiptions['brand_id'] != NULL) {
													$brand_details = Yii::app()->db->createCommand("SELECT brand_name FROM {$tblpx}brand WHERE id=" . $desctiptions['brand_id'] . "")->queryRow();
													$brand = '-' . $brand_details['brand_name'];
												} else {
													$brand = '';
												}
												if ($new['category_id'] == 0) {

													$spc_details = $new['remark'];
													$spc_id = 'other';
												} else {
													$spc_details = $parent_category['category_name'] . $brand . ' - ' . $desctiptions['specification'];
													$spc_id = $new['category_id'];
												}

												?>


												<tr class="tr_class" id="item_<?php echo $new['item_id']; ?>">

													<td>
														<div id="item_sl_no">
															<?php echo $i; ?>
														</div>
													</td>
													<td>
														<div class="item_description" id="<?php echo $spc_id; ?>">
															<?php echo $spc_details; ?>
														</div>
													</td>
													<td>
														<div class="item_length" id="unit">
															<?php echo $new['item_length']; ?>
														</div>
													</td>
													<td>
														<div class="text-right" id="unit">
															<?php echo $new['quantity']; ?>
														</div>
													</td>
													<td>
														<div class="unit" id="unit">
															<?php echo $new['unit']; ?>
														</div>
													</td>
													<?php
													$class = "";
													$style = "";
													if ($new['permission_status'] == 'not_approved') {
														$class = "rate_highlight";
														$style = "style='cursor:pointer'";
													} else {
														$class = "";
														$style = "";
													}
													?>
													<td class="<?php echo $class; ?>" <?php echo $style; ?>
														id='<?php echo $new['item_id']; ?>'>
														<div class="text-right" id="rate">
															<?php echo number_format($new['rate'], 2, '.', ''); ?>
														</div>
													</td>
													<td>
														<div class="amount text-right" id="amount"
															style="max-width: -3px !important;">
															<?php echo number_format($new['amount'], 2, '.', ''); ?>
														</div>
													</td>



													<td>
														<div class="amount text-right" id="tax_slab"
															style="max-width: -3px !important;">
															<?php echo number_format($new['tax_slab'], 2, '.', ''); ?>
														</div>
													</td>
													<td>
														<div class="amount text-right" id="sgst_amount"
															style="max-width: -3px !important;">
															<?php echo number_format($new['sgst_amount'], 2, '.', ''); ?>
														</div>
													</td>
													<td>
														<div class="amount text-right" id="sgst_percentage"
															style="max-width: -3px !important;">
															<?php echo number_format($new['sgst_percentage'], 2, '.', ''); ?>
														</div>
													</td>
													<td>
														<div class="amount text-right" id="cgst_amount"
															style="max-width: -3px !important;">
															<?php echo number_format($new['cgst_amount'], 2, '.', ''); ?>
														</div>
													</td>
													<td>
														<div class="amount text-right" id="cgst_percentage"
															style="max-width: -3px !important;">
															<?php echo number_format($new['cgst_percentage'], 2, '.', ''); ?>
														</div>
													</td>
													<td>
														<div class="amount text-right" id="igst_amount"
															style="max-width: -3px !important;">
															<?php echo number_format($new['igst_amount'], 2, '.', ''); ?>
														</div>
													</td>
													<td>
														<div class="amount text-right" id="igst_percentage"
															style="max-width: -3px !important;">
															<?php echo number_format($new['igst_percentage'], 2, '.', ''); ?>
														</div>
													</td>
													<td>
														<div class="amount text-right" id="discount_percentage"
															style="max-width: -3px !important;">
															<?php echo number_format($new['discount_percentage'], 2, '.', ''); ?>
														</div>
													</td>
													<td>
														<div class="amount text-right" id="discount_amount"
															style="max-width: -3px !important;">
															<?php echo number_format($new['discount_amount'], 2, '.', ''); ?>
														</div>
													</td>
													<td>
														<div class="amount text-right" id="tax_amount"
															style="max-width: -3px !important;">
															<?php echo number_format($new['tax_amount'], 2, '.', ''); ?>
														</div>
													</td>

													<td>
														<div class="amount text-right" id="total_amount"
															style="max-width: -3px !important;">
															<?php echo number_format(($new['amount'] + $new['tax_amount'] - $new['discount_amount']), 2, '.', ''); ?>
														</div>
													</td>

													<td>
														<div class="item_description" id="remark">
															<?php echo $new['remark']; ?>
														</div>
													</td>
													<td width="70"><span class="icon icon-options-vertical popover-test"
															data-toggle="popover" data-placement="left" type="button"
															data-html="true" style="cursor: pointer;"></span>
														<div class="popover-content hide">
															<ul class="tooltip-hiden">
																<li><a href="" id='<?php echo $new['item_id']; ?>'
																		class="removebtn btn btn-xs btn-default">Delete</a>
																</li>
																<li><a href="" id='<?php echo $new['item_id']; ?>'
																		class="btn btn-xs btn-default edit_item">Edit</a>
																</li>
																<?php if ($new['permission_status'] == 'not_approved' && Yii::app()->user->role == 1) { ?>
																	<li><a href="" id='<?php echo $new['item_id']; ?>'
																			class="btn btn-xs btn-default approve_item approveoption_<?php echo $new['item_id']; ?>">Approve</a>
																	</li>
																<?php } ?>

															</ul>
														</div>
													</td>
												</tr>

												<?php
												$i++;
											}
											?>
										</tbody>
										<tfoot class="entry-table">
											<tr>
												<th colspan="15" class="text-right">TOTAL: </th>
												<th class="text-right">
													<div id="discount_total">
														<?php echo Yii::app()->Controller->money_format_inr((($total_discount_value['discount_amount']) ? $total_discount_value['discount_amount'] : "0.00"), 2, 1) ?>
													</div>
												</th>
												<th class="text-right">
													<div id="tax_total">
														<?php echo Yii::app()->Controller->money_format_inr(($total_tax_amount['tax_amount'] ? $total_tax_amount['tax_amount'] : "0.00"), 2, 1) ?>
													</div>
												</th>
												<th class="text-right">
													<div id="amount_total">
														<?php echo Yii::app()->Controller->money_format_inr(($grand_total_amount ? $grand_total_amount : "0.00"), 2, 1) ?>
													</div>
												</th>
												<th></th>
												<th></th>
											</tr>
											<tr>
												<th colspan="18" class="text-right">GRAND TOTAL:
													<span id="grand_total">
														<?php echo Yii::app()->Controller->money_format_inr(($grand_total_amount ? $grand_total_amount : "0.00"), 2, 1) ?>
													</span>
												</th>
												<th></th>
												<th></th>
											</tr>
										</tfoot>
									</table>
								</div>
							</div>
						</div>

						<input type="hidden" name="final_amount" id="final_amount"
							value="<?php echo ($model->total_amount == '') ? '0.00' : number_format($model->total_amount, 2, '.', ''); ?>">
						<input type="hidden" name="item_amount_temp" id="item_amount_temp" value="0">
					</div>
				</div>

				<div style="padding-right: 0px;"
					class="col-md-offset-9 col-md-3 col-sm-offset-8 col-sm-4 col-xs-offset-6 col-xs-6">

					<input type="hidden" value="<?php echo ((isset($subtot) && $subtot != '') ? $subtot : ''); ?>"
						class="txtBox pastweek" readonly=ture name="subtot" /></td>
					<input type="hidden" value="<?php echo ((isset($grand) && $grand != '') ? $grand : ''); ?>"
						class="txtBox pastweek grand" name="grand" readonly=true />



					<?php //  echo $model->total_amount; 
					?>

				</div>

				<br><br>
				<!--<center><input type="submit" name="invoice" value="Submit" class="form_submit1"></center>-->
				<center>
					<!--<input type="button" name="purchase" id="purchase_btn" value="Submit" class="form_submit1">-->
				</center>
			</form>
		</div>
	</div>
</div>
<script>
	$(function () {
		$("#length, #quantity").keydown(function (event) {


			if (event.shiftKey == true) {
				event.preventDefault();
			}

			if ((event.keyCode >= 48 && event.keyCode <= 57) ||
				(event.keyCode >= 96 && event.keyCode <= 105) ||
				event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 ||
				event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190 ||
				event.keyCode == 110) {

			} else {
				event.preventDefault();
			}

			if ($(this).val().indexOf('.') !== -1 && event.keyCode == 190)
				event.preventDefault();
			//if a decimal has been added, disable the "."-button

		});
	});



	var purchase_id = <?php echo $model->p_id; ?>;

	$(document).ready(function () {
		$(".js-example-basic-single").select2();
		$(".project").select2();
		$(".vendor").select2();
		$(".expense_head").select2();
	});

	$(document).ready(function () {
		$('#description').first().focus();
	});

	$("#expense_head").change(function () {
		var val = $(this).val();
		//if(val == '') {
		$("#vendor").html('<option value="">Select Vendor</option>');
		//} else {
		$.ajax({
			url: '<?php echo Yii::app()->createUrl('purchase/dynamicvendor'); ?>',
			method: 'POST',
			data: {
				exp_id: val
			},
			dataType: "json",
			success: function (response) {
				$("#vendor").html(response.html);
			}

		})
		//}
	})



	$("#project").change(function () {
		var val = $(this).val();
		//if(val == '') {
		$("#vendor").html('<option value="">Select Vendor</option>');
		$("#expense_head").html('<option value="">Select Expense Head</option>');
		//} else {
		$.ajax({
			url: '<?php echo Yii::app()->createUrl('purchase/dynamicexpensehead'); ?>',
			method: 'POST',
			data: {
				project_id: val
			},
			dataType: "json",
			success: function (response) {
				if (response.msg != "") {
					alert(response.msg);
				}
				if (response.status == 'success') {
					$("#expense_head").html(response.html);
					$("#expense_head").focus();
					$("#vendor").html('<option value="">Select Vendor</option>');
				} else {
					$("#expense_head").html(response.html);
					$("#expense_head").focus();
					$("#vendor").html('<option value="">Select Vendor</option>');
				}
			}

		})
		//}
	})

	jQuery.extend(jQuery.expr[':'], {
		focusable: function (el, index, selector) {
			return $(el).is('button, :input, [tabindex]');
		}
	});

	$(document).on('keypress', 'input,select', function (e) {
		if (e.which == 13) {
			e.preventDefault();
			// Get all focusable elements on the page
			var $canfocus = $(':focusable');
			var index = $canfocus.index(document.activeElement) + 1;
			if (index >= $canfocus.length) index = 0;
			$canfocus.eq(index).focus();
		}
	});
</script>



<script>
	$(document).ready(function () {
		$().toastmessage({
			sticky: false,
			position: 'top-right',
			inEffectDuration: 1000,
			stayTime: 3000,
			closeText: '<i class="icon ion-close-round"></i>',
		});
		$(".remark").addClass('remark_edit');
	});



	// calculation

	$("#quantity, #rate, #length").blur(function () {
		var quantity = parseFloat($("#quantity").val());
		var rate = parseFloat($("#rate").val());
		var length = parseFloat($("#length").val());
		var amount = quantity * rate * length;
		if (isNaN(amount)) amount = 0;
		var effective_quantity = parseFloat(length) * parseFloat(quantity);
		if (isNaN(effective_quantity)) effective_quantity = 0;
		$('#effective_quantity').val(effective_quantity);
		$("#item_amount").html(amount.toFixed(2));
	});


	$(function () {
		$(document).on('click', '.removebtn', function (e) {
			e.preventDefault();
			element = $(this);


			var item_id = $(this).attr('id');
			if (item_id == '0') {
				$('input[name="remove"]').val('removed');
				$(this).closest('tr').find('input[name="amount[]"]').each(function () {
					$(this).closest('tr').remove();
					var re = this.value;
					var v1 = $('input[name="subtot"]').val();
					var v2 = $('input[name="grand"]').val();
					if (re == '') {
						$('input[name="subtot"]').val(v1);
						$('input[name="grand"]').val(v2);

					} else {
						$('input[name="subtot"]').val(eval(v1) - eval(re));
						$('input[name="grand"]').val(eval(v2) - eval(re));

					}
				});
			} else {
				var answer = confirm("Are you sure you want to delete?");
				if (answer) {
					var item_id = $(this).attr('id');
					var $tds = $(this).closest('tr').find('td');
					var amount = $tds.eq(5).text();
					var re = amount;
					var j = parseFloat(amount)
					var v1 = $('input[name="subtot"]').val();
					var v2 = $('input[name="grand"]').val();
					var t = $('#grand_total').text();
					var gamount = parseFloat(t);
					if (j == '') {
						$('input[name="subtot"]').val(gamount);
						$('input[name="grand"]').val(gamount);

					} else {
						$('input[name="subtot"]').val((eval(gamount) - eval(j)).toFixed(2));
						$('input[name="grand"]').val((eval(gamount) - eval(j)).toFixed(2));


					}
					var subtot = $('input[name="subtot"]').val();
					var grand = $('input[name="grand"]').val();
					var data = {
						'purchase_id': purchase_id,
						'item_id': item_id,
						'grand': grand,
						'subtot': subtot
					};
					$.ajax({
						method: "GET",
						async: false,
						data: {
							data: data
						},
						dataType: "json",
						url: '<?php echo Yii::app()->createUrl('purchase/removepurchaseitem2'); ?>',
						success: function (result) {

							if (result.response == 'success') {
								console.log(result);
								$('.addrow').html(result.html);
								$('#amount_total').text(result.grand_total);
								$('#discount_total').text(result.discount_amount);
								$('#tax_total').text(result.tax_amount);
								$('#grand_total').text(result.grand_total_amount);
								$('#tot_qtyy').text(result.tot_qty);
								$('#final_amount').val(result.grand_total);

								// $('#grand_total').text(result.grand_total);
								$().toastmessage('showSuccessToast', "" + result.msg + "");
							} else {
								$().toastmessage('showErrorToast', "" + result.msg + "");
							}
							$('#description').val('').trigger('change');
							$('#remarks').val('');
							$('#length').val('');
							var quantity = $('#quantity').val('');
							var unit = $('#item_unit').text('');
							var rate = $('#rate').val('');
							var effective_quantity = $('#effective_quantity').val('');
							var amount = $('#item_amount').html('');
							var grnd_total = $('#grand_total').text();
							if (grnd_total = '') {
								$('#tot_qtyy').text('');
							}

							$(".item_save").attr('value', 'Save');
							$(".item_save").attr('id', 0);
							$('#description').select2('focus');
						}
					});

				} else {

					return false;
				}

			}





		});



		$(document).on("click", ".addcolumn, .removebtn", function () {
			//alert('hi');

			$("table.table  input[name='sl_No[]']").each(function (index, element) {
				$(element).val(index + 1);
				$('.sl_No').html(index + 1);
			});
		});






		$(".inputSwitch span").on("click", function () {

			var $this = $(this);

			$this.hide().siblings("input").val($this.text()).show();

		});

		$(".inputSwitch input").bind('blur', function () {

			var $this = $(this);

			$(this).attr('value', $(this).val());

			$this.hide().siblings("span").text($this.val()).show();

		}).hide();

	});



	$("#rate").keydown(function (event) {

		if (event.shiftKey == true) {
			event.preventDefault();
		}

		if ((event.keyCode >= 48 && event.keyCode <= 57) ||
			(event.keyCode >= 96 && event.keyCode <= 105) ||
			event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 ||
			event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190 || event.keyCode == 110 || event.keyCode == 13) {
			var splitfield = $(this).val().split(".");
			if (splitfield[1].length >= 2 && event.keyCode != 8 && event.keyCode != 0 && event.keyCode != 13) {
				event.preventDefault();
			}
		} else {
			event.preventDefault();
		}

		if ($(this).val().indexOf('.') !== -1 && (event.keyCode == 190 || event.keyCode == 110))
			event.preventDefault();
		//if a decimal has been added, disable the "."-button




	});



	$('.other').click(function () {
		if (this.checked) {
			$('#autoUpdate').fadeIn('slow');
			$('#select_div').hide();
		} else {
			$('#autoUpdate').fadeOut('slow');
			$('#select_div').show();
		}

	})


	$(document).on("change", ".other", function () {
		if (this.checked) {
			$(this).closest('tr').find('#autoUpdate').fadeIn('slow');
			$(this).closest('tr').find('#select_div').hide();
		} else {
			$(this).closest('tr').find('#autoUpdate').fadeOut('slow');
			$(this).closest('tr').find('#select_div').show();
		}
	});


	$(".specification").select2({
		ajax: {
			url: '<?php echo Yii::app()->createAbsoluteUrl('purchase/searchtransaction2'); ?>',
			type: "POST",
			dataType: 'json',
			delay: 0,
			data: function (params) {
				return {
					searchTerm: params.term // search term
				};
			},
			processResults: function (response) {
				$('#previous_details').html(response.html);
				$("#previous_details").show();
				$("#pre_fixtable3").tableHeadFixer();
				$('#item_unit').text(response.unit);
				return {
					results: response.data
				};
			},
			cache: true
		}
	});

	$('.specification').change(function () {
		var category_id = $(this).val();
		if (category_id == 'other') {
			$('#remarks').focus();
		} else {
			$('#length').focus();

		}
	});



	$('.specification').change(function () {
		var element = $(this);
		var category_id = $(this).val();
		$.ajax({
			url: '<?php echo Yii::app()->createAbsoluteUrl('purchase/previoustransaction2'); ?>',
			type: 'GET',
			dataType: 'json',
			data: {
				data: category_id
			},
			success: function (result) {
				//					if(result.status == 1) {
				//					$('#previous_details').html(result.html); 
				//					
				//					$("#previous_details").show();
				//                                        $("#pre_fixtable").tableHeadFixer();
				//				   } else {
				//					$('#previous_details').html(result.html); 
				//                                        $("#pre_fixtable").tableHeadFixer();
				//				 }
				//				 $('#item_unit').text(result.unit);
				//				 if(category_id == 'other'){ 
				//					 $('#remarks').focus();
				//				} else if(category_id == '' ) {
				//					$('.js-example-basic-single').select2('focus');
				//				} else {
				//					$('#quantity').focus(); 
				//					}
				if (result.status == 1) {
					$('#previous_details').html(result.html);
					$("#previous_details").show();
					$("#pre_fixtable").tableHeadFixer();
				} else {
					$('#previous_details').html(result.html);
					$("#pre_fixtable").tableHeadFixer();
				}
				$('#item_unit').text(result.unit);
				if (category_id == 'other') {
					$('#remarks').focus();
				} else if (category_id == '') {
					$('.js-example-basic-single').select2('focus');
				} else {
					$('#length').focus();
				}

			}

		})

	})


	$('.description').change(function () {
		var value = $(this).val();
		//alert(value);	
		if (value == 'other') {
			$('.remark').css("display", "inline-block");
			$('#remark').focus();
		} else {
			$('.remark').css("display", "none");

		}
	})
</script>


<script>
	/* Neethu  */

	$(document).on("change", "#project", function () {
		var element = $(this);
		var purchase_id = $("#purchase_id").val();
		var default_date = $(".date").val();
		var project = $(this).val();
		var vendor = $('#vendor').val();
		var purchaseno = $('#purchaseno').val();
		var expense_head = $('#expense_head').val();
		var company = $('#company_id').val();
		var delivery_date = $(".delivery_date").val();

		if ((moment(default_date, 'DD-MM-YYYY', true).isValid())) {
			if (project == '' || vendor == '' || default_date == '' || purchaseno == '' || expense_head == '') {

			} else {
				$.ajax({
					method: "GET",
					async: true,
					data: {
						purchase_id: purchase_id,
						purchaseno: purchaseno,
						default_date: default_date,
						project: project,
						vendor: vendor,
						expense_head: expense_head,
						company: company,
						delivery_date: delivery_date
					},
					dataType: "json",
					url: '<?php echo Yii::app()->createUrl('purchase/createnewpurchase2'); ?>',
					success: function (result) {
						if (result.response == 'success') {
							$(".purchase_items").removeClass('checkek_edit');
							$('.js-example-basic-single').select2('focus');
							$("#purchase_id").val(result.p_id);
							$().toastmessage('showSuccessToast', "" + result.msg + "");
						} else {
							$().toastmessage('showErrorToast', "" + result.msg + "");
							element.val('');
						}
						$("#expense_head").select2("focus");
					}
				});
			}
		} else {
			$(this).focus();
			$().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
			$(this).focus();
		}
	});

	$(document).on("change", "#vendor", function () {
		var element = $(this);
		var purchase_id = $("#purchase_id").val();
		var default_date = $(".date").val();
		var vendor = $(this).val();
		var expense_head = $('#expense_head').val();
		var project = $('#project').val();
		var purchaseno = $('#purchaseno').val();
		var company = $('#company_id').val();
		var delivery_date = $(".delivery_date").val();

		if ((moment(default_date, 'DD-MM-YYYY', true).isValid())) {

			if (project == '' || vendor == '' || default_date == '' || purchaseno == '' || expense_head == '') {
				$.ajax({
					method: "GET",
					data: {
						purchase_id: 'test'
					},
					dataType: "json",
					url: '<?php echo Yii::app()->createUrl('purchase/test'); ?>',
					success: function (result) {
						$(".date").focus();
					}
				});

			} else {
				$.ajax({
					method: "GET",
					data: {
						purchase_id: purchase_id,
						purchaseno: purchaseno,
						default_date: default_date,
						project: project,
						vendor: vendor,
						expense_head: expense_head,
						company: company,
						delivery_date: delivery_date
					},
					dataType: "json",
					url: '<?php echo Yii::app()->createUrl('purchase/createnewpurchase2'); ?>',
					success: function (result) {
						if (result.response == 'success') {
							$(".purchase_items").removeClass('checkek_edit');
							$('.js-example-basic-single').select2('focus');
							$("#purchase_id").val(result.p_id);
							$().toastmessage('showSuccessToast', "" + result.msg + "");
						} else {
							$().toastmessage('showErrorToast', "" + result.msg + "");
						}
						$(".date").focus();
					}
				});

			}

		} else {
			$(this).focus();
			$().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
			$(this).focus();
		}

	});


	$(document).on("change", "#expense_head", function () {
		var element = $(this);
		var purchase_id = $("#purchase_id").val();
		var default_date = $(".date").val();
		var expense_head = $(this).val();
		var company = $('#company_id').val();
		var project = $('#project').val();
		var purchaseno = $('#purchaseno').val();
		var vendor = $('#vendor').val();
		var delivery_date = $(".delivery_date").val();
		if ((moment(default_date, 'DD-MM-YYYY', true).isValid())) {
			if (project == '' || vendor == '' || default_date == '' || purchaseno == '' || expense_head == '') {
				$.ajax({
					method: "GET",
					data: {
						purchase_id: 'test'
					},
					dataType: "json",
					url: '<?php echo Yii::app()->createUrl('purchase/test'); ?>',
					success: function (result) {
						$("#vendor").select2("focus");
					}
				});

			} else {
				$.ajax({
					method: "GET",
					data: {
						purchase_id: purchase_id,
						purchaseno: purchaseno,
						default_date: default_date,
						project: project,
						vendor: vendor,
						expense_head: expense_head,
						company: company,
						delivery_date: delivery_date
					},
					dataType: "json",
					url: '<?php echo Yii::app()->createUrl('purchase/createnewpurchase2'); ?>',
					success: function (result) {
						if (result.response == 'success') {
							$(".purchase_items").removeClass('checkek_edit');
							$('.js-example-basic-single').select2('focus');
							$("#purchase_id").val(result.p_id);
							$().toastmessage('showSuccessToast', "" + result.msg + "");
						} else {
							$().toastmessage('showErrorToast', "" + result.msg + "");
						}

						$("#vendor").select2("focus");
					}
				});

			}

		} else {
			$(this).focus();
			$().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
			$(this).focus();
		}

	});


	$(document).on("change", ".date", function () {
		var element = $(this);
		var purchase_id = $("#purchase_id").val();
		var default_date = $(this).val();
		var project = $('#project').val();
		var vendor = $('#vendor').val();
		var purchaseno = $('#purchaseno').val();
		var expense_head = $('#expense_head').val();
		var company = $('#company_id').val();
		var delivery_date = $(".delivery_date").val();
		if ((moment(default_date, 'DD-MM-YYYY', true).isValid())) {
			if (project == '' || vendor == '' || default_date == '' || purchaseno == '' || expense_head == '') {

			} else {
				$.ajax({
					method: "GET",
					async: false,
					data: {
						purchase_id: purchase_id,
						purchaseno: purchaseno,
						default_date: default_date,
						project: project,
						vendor: vendor,
						expense_head: expense_head,
						company: company,
						delivery_date: delivery_date
					},
					dataType: "json",
					url: '<?php echo Yii::app()->createUrl('purchase/createnewpurchase2'); ?>',
					success: function (result) {
						if (result.response == 'success') {
							$(".purchase_items").removeClass('checkek_edit');
							$('.js-example-basic-single').select2('focus');
							$("#purchase_id").val(result.p_id);
							$().toastmessage('showSuccessToast', "" + result.msg + "");
						} else {
							$().toastmessage('showErrorToast', "" + result.msg + "");
						}
					}

				});

			}
		} else {
			$(this).focus();
			$().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
			$(this).focus();
		}

		$('#purchaseno').focus();

	});


	$(document).on("blur", "#purchaseno", function (event) {
		var element = $(this);
		var purchase_id = $("#purchase_id").val();
		var default_date = $(".date").val();
		var purchaseno = $(this).val();
		var expense_head = $('#expense_head').val();
		var company = $('#company_id').val();
		var delivery_date = $(".delivery_date").val();

		if ((moment(default_date, 'DD-MM-YYYY', true).isValid())) {
			if (purchaseno == '') {
				event.preventDefault();
			} else {
				var project = $('#project').val();
				var vendor = $('#vendor').val();
				var date = $('.date').val();
				if (project == '' || vendor == '' || default_date == '' || expense_head == '') {

				} else {
					$.ajax({
						method: "GET",
						data: {
							purchase_id: purchase_id,
							purchaseno: purchaseno,
							default_date: default_date,
							project: project,
							vendor: vendor,
							expense_head: expense_head,
							company: company,
							delivery_date: delivery_date
						},
						dataType: "json",
						url: '<?php echo Yii::app()->createUrl('purchase/createnewpurchase2'); ?>',
						success: function (result) {
							if (result.response == 'success') {
								$(".purchase_items").removeClass('checkek_edit');
								$("#purchase_id").val(result.p_id);
								$().toastmessage('showSuccessToast', "" + result.msg + "");
							} else {
								$().toastmessage('showErrorToast', "" + result.msg + "");
							}
							$('#description').select2('focus');
							$('.js-example-basic-single').select2('focus');
						}

					});



				}
			}

		} else {
			$(this).focus();
			$().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
			$(this).focus();
		}

	});



	$("#purchaseno").keypress(function (event) {
		if (event.keyCode == 13) {
			$("#purchaseno").blur();
		}
	});

	$("#date").keypress(function (event) {
		if (event.keyCode == 13) {
			if ($(this).val()) {
				$("#purchaseno").focus();
			}
		}
	});

	$(".date").keyup(function (event) {
		if (event.keyCode == 13) {
			$(".date").click();
		}
	});

	$("#vendor").keyup(function (event) {
		if (event.keyCode == 13) {
			$("#vendor").click();
		}
	});

	$("#project").keyup(function (event) {
		if (event.keyCode == 13) {
			$("#project").click();
		}
	});


	var sl_no = 1;
	var howMany = 0;
	$('.item_save').click(function () {

		$("#previous_details").hide();

		var element = $(this);
		var item_id = $(this).attr('id');

		if (item_id == 0) {

			// add

			var description = $('.specification').val();
			var remark = $('#remarks').val();
			var length = $('#length').val();
			var quantity = $('#quantity').val();
			var unit = $('#item_unit').text();
			var rate = $('#rate').val();
			var amount = $('#item_amount').html();
			var rowCount = $('.table .addrow tr').length;
			var dis_amount = $('#dis_amount').val();
			var disp = $('#disp').html();
			var cgstp = $('#cgstp').val();
			var cgst_amount = $('#cgst_amount').html();
			var sgstp = $('#sgstp').val();
			var sgst_amount = $('#sgst_amount').html();
			var igstp = $('#igstp').val();
			var igst_amount = $('#igst_amount').html();
			var tax_amount = $('#tax_amount').html();
			var item_amount = $('#item_amount').html()
			var discount_amount = $('#disc_amount').html();
			var total_amount = $('#total_amount').html();
			var tax_slab = $("#tax_slab").val();

			var project = $('#project').val();
			var vendor = $('#vendor').val();
			var date = $(".date").val();
			var purchaseno = $('#purchaseno').val();
			var expense_head = $('#expense_head').val();

			if (project == '' || vendor == '' || date == '' || purchaseno == '' || expense_head == '') {
				$().toastmessage('showErrorToast', "Please enter purchase details");
			} else {
				if ((moment(date, 'DD-MM-YYYY', true).isValid())) {
					if ((description == '' && remark == '') || length == '' || quantity == '' || rate == '' || amount == 0) {
						if (amount == 0) {
							$().toastmessage('showErrorToast', "Please fill valid quantity and rate");
						} else {
							$().toastmessage('showErrorToast', "Please fill the details");
						}
					} else {
						howMany += 1;
						if (howMany == 1) {
							var subtot = $('#grand_total').text();
							var grand = $('#grand_total').text();
							var data = {
								'sl_no': rowCount,
								'length': length,
								'quantity': quantity,
								'description': description,
								'unit': unit,
								'rate': rate,
								'amount': amount,
								'remark': remark,
								'purchase_id': purchase_id,
								'grand': grand,
								'subtot': subtot,
								'dis_amount': dis_amount,
								'disp': disp,
								'sgstp': sgstp,
								'sgst_amount': sgst_amount,
								'cgstp': cgstp,
								'cgst_amount': cgst_amount,
								'igstp': igstp,
								'igst_amount': igst_amount,
								'tax_amount': tax_amount,
								'discount_amount': discount_amount,
								'total_amount': total_amount,
								'tax_slab': tax_slab,
							};
							$.ajax({
								url: '<?php echo Yii::app()->createAbsoluteUrl('purchase/purchaseitem2'); ?>',
								type: 'GET',
								dataType: 'json',
								data: {
									data: data
								},
								success: function (response) {

									if (response.response == 'success') {
										$('#amount_total').text(response.grand_total);
										$('#discount_total').text(response.discount_total);
										$('#tax_total').text(response.tax_total);
										$('#grand_total').text(response.grand_total);
										$('#grand_total').text(response.final_amount);
										$('#tot_qtyy').text(response.total_qty);
										$('#final_amount').val(response.final_amount);
										$().toastmessage('showSuccessToast', "" + response.msg + "");
										$('.addrow').append(response.html);
									} else {
										$().toastmessage('showErrorToast', "" + response.msg + "");
									}
									howMany = 0;
									$('#description').val('').trigger('change');
									$('#remarks').val('');
									$('#length').val('');
									$('#dis_amount').val('');
									$('#sgstp').val('');
									$('#cgstp').val('');
									$('#igstp').val('');
									$('#disp').html('');
									$('#sgst_amount').html('');
									$('#cgst_amount').html('');
									$('#igst_amount').html('');
									$('#disc_amount').html('');
									$('#tax_amount').html('');
									$('#total_amount').html('');
									var quantity = $('#quantity').val('');
									var unit = $('#item_unit').text('');
									var rate = $('#rate').val('');
									var effective_quantity = $('#effective_quantity').val('');
									var amount = $('#item_amount').html('');
									$('#description').select2('focus');
								}
							});

							$('.js-example-basic-single').select2('focus');
						}
					}

				} else {
					$(this).focus();
					$().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
					$(this).focus();
				}
			}



		} else {
			// update 


			var description = $('.specification').val();
			var remark = $('#remarks').val();
			var length = $('#length').val();
			var quantity = $('#quantity').val();
			var unit = $('#item_unit').text();
			var rate = $('#rate').val();
			var amount = $('#item_amount').html();
			var project = $('#project').val();
			var vendor = $('#vendor').val();
			var date = $(".date").val();
			var purchaseno = $('#purchaseno').val();
			var expense_head = $('#expense_head').val();
			var dis_amount = $('#dis_amount').val();
			var disp = $('#disp').html();
			var cgstp = $('#cgstp').val();
			var cgst_amount = $('#cgst_amount').html();
			var sgstp = $('#sgstp').val();
			var sgst_amount = $('#sgst_amount').html();
			var igstp = $('#igstp').val();
			var igst_amount = $('#igst_amount').html();
			var tax_amount = $('#tax_amount').html();
			var item_amount = $('#item_amount').html()
			var discount_amount = $('#disc_amount').html();
			var total_amount = $('#total_amount').html();
			var tax_slab = $("#tax_slab").val();
			if (project == '' || vendor == '' || date == '' || purchaseno == '' || expense_head == '') {
				$().toastmessage('showErrorToast', "Please enter purchase details");
			} else {
				if ((moment(date, 'DD-MM-YYYY', true).isValid())) {
					if ((description == '' && remark == '') || length == '' || quantity == '' || rate == '' || amount == 0) {
						if (amount == 0) {
							$().toastmessage('showErrorToast', "Please fill valid quantity and rate");
						} else {
							$().toastmessage('showErrorToast', "Please fill the details");
						}
					} else {

						howMany += 1;
						if (howMany == 1) {
							var subtot = $('#grand_total').text();
							var grand = $('#grand_total').text();
							var data = {
								'item_id': item_id,
								'sl_no': sl_no,
								'length': length,
								'quantity': quantity,
								'description': description,
								'unit': unit,
								'rate': rate,
								'amount': amount,
								'remark': remark,
								'purchase_id': purchase_id,
								'grand': grand,
								'subtot': subtot,
								'dis_amount': dis_amount,
								'disp': disp,
								'sgstp': sgstp,
								'sgst_amount': sgst_amount,
								'cgstp': cgstp,
								'cgst_amount': cgst_amount,
								'igstp': igstp,
								'igst_amount': igst_amount,
								'tax_amount': tax_amount,
								'discount_amount': discount_amount,
								'total_amount': total_amount,
								'tax_slab': tax_slab,
							};
							$.ajax({
								url: '<?php echo Yii::app()->createAbsoluteUrl('purchase/updatepurchaseitem2'); ?>',
								type: 'GET',
								dataType: 'json',
								data: {
									data: data
								},
								success: function (response) {

									element.closest('tr').hide();
									if (response.response == 'success') {
										console.log(response);
										$('#amount_total').text(response.grand_total);
										$('#discount_total').text(response.discount_total);
										$('#tax_total').text(response.tax_total);
										$('#grand_total').text(response.grand_total);
										$('#tot_qtyy').text(response.tot_qty);
										$('#final_amount').val(response.final_amount);
										$().toastmessage('showSuccessToast', "" + response.msg + "");
										// $('#grand_total').text(response.final_amount);
										$('.addrow').html(response.html);
									} else {
										$().toastmessage('showErrorToast', "" + response.msg + "");
									}
									howMany = 0;
									$('#description').val('').trigger('change');
									$('#remarks').val('');
									$('#length').val('');
									var quantity = $('#quantity').val('');
									var unit = $('#item_unit').text('');
									var rate = $('#rate').val('');
									var effective_quantity = $('#effective_quantity').val('');
									var amount = $('#item_amount').html('');
									$(".item_save").attr('value', 'Save');
									$(".item_save").attr('id', 0);
									$('#description').select2('focus');
								}
							});

							$('.js-example-basic-single').select2('focus');
						}
					}

				} else {
					$(this).focus();
					$().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
					$(this).focus();
				}
			}

		}




	});



	$(document).on("change", "#quantity, #length", function () {
		var length_val = $('#length').val();
		var quantity_val = $('#quantity').val();
		var length = "";
		var quantity = "";
		if (length_val == "") {
			length = 1;
		} else {
			length = length_val;
		}
		if (quantity_val == "") {
			quantity = 1;
		} else {
			quantity = quantity_val;
		}
		var effective_quantity = parseFloat(length) * parseFloat(quantity);
		if (isNaN(effective_quantity)) effective_quantity = 0;
		$('#effective_quantity').val(effective_quantity);

	});
	$("#length").keyup(function (event) {
		event.preventDefault();
		if (event.keyCode == 13) {
			$("#quantity").focus();
		}
	});
	$("#quantity").keyup(function (event) {
		event.preventDefault();
		if (event.keyCode == 13) {
			$("#effective_quantity").focus();
		}
	});
	$(document).on('click', '.edit_item', function (e) {
		e.preventDefault();
		var item_id = $(this).attr('id');
		var $tds = $(this).closest('tr').find('td');
		var description = $tds.eq(1).text();
		var length = $tds.eq(2).text().trim();
		var quantity = $tds.eq(3).text();
		var unit = $tds.eq(4).text();
		var rate = $tds.eq(5).text();
		var amount = $tds.eq(6).text();
		var remarks = $tds.eq(18).text();
		var tax_slab = $tds.eq(7).text();
		tax_slab = Math.round(tax_slab);
		var sgst_amount = $tds.eq(8).text();
		var sgstp = $tds.eq(9).text();
		var cgst_amount = $tds.eq(10).text();
		var cgstp = $tds.eq(11).text();
		var igst_amount = $tds.eq(12).text();
		var igstp = $tds.eq(13).text();
		var disc_amount = $tds.eq(15).text();
		var disc_perc = $tds.eq(14).text();
		var tax_amount = $tds.eq(16).text();
		var des_id = $(this).closest('tr').find('.item_description').attr('id');
		var effective_quantity = parseFloat(length) * parseFloat(quantity);
		if (isNaN(effective_quantity)) effective_quantity = 0;
		$('#effective_quantity').val(effective_quantity);
		<?php $abc; ?>
		if (des_id == '0' || des_id == 'other') {
			$('#remarks').val(description.trim());
			$('.remark').css("display", "inline-block");
			$('#remark').focus();
			$('#remarks').focus();
			$('#description').val('other').trigger('change');
		} else {
			$('#description').val(des_id).trigger('change');
			$('.remark').css("display", "none");
			$('.js-example-basic-single').select2('focus');
		}
		$('#remarks').val(remarks.trim());
		$('#length').val(parseFloat(length));
		$('#quantity').val(parseFloat(quantity));
		$('#item_unit').text(unit);
		$('#item_amount').html(parseFloat(amount));
		$('#item_amount_temp').val(parseFloat(amount));
		$('#rate').val(parseFloat(rate));
		$('#dis_amount').val(parseFloat(disc_amount));
		$('#disp').html(parseFloat(disc_perc));
		$('#tax_slab').val(tax_slab).trigger('change');
		$('#sgstp').val(parseFloat(sgstp));
		$('#sgst_amount').html(parseFloat(sgst_amount).toFixed(2));
		$('#cgstp').val(parseFloat(cgstp));
		$('#cgst_amount').html(parseFloat(cgst_amount).toFixed(2));
		$('#igstp').val(parseFloat(igstp));
		$('#igst_amount').html(parseFloat(igst_amount).toFixed(2));
		$('#tax_amount').html(parseFloat(tax_amount))
		$('#disc_amount').html(parseFloat(disc_amount))
		var totalamount = (parseFloat(amount) + parseFloat(tax_amount)) - parseFloat(disc_amount);
		$('#total_amount').html(parseFloat(totalamount).toFixed(2));
		$(".item_save").attr('value', 'Update');
		$(".item_save").attr('id', item_id);
		$(".item_save").attr('id', item_id);
	})


	$('.item_save').keypress(function (e) {
		if (e.keyCode == 13) {
			$('.item_save').click();
		}
	});

	// approve items

	$(document).on('click', '.approve_item', function (e) {
		e.preventDefault();
		var element = $(this);
		var item_id = $(this).attr('id');
		$.ajax({
			url: '<?php echo Yii::app()->createAbsoluteUrl('purchase/permissionapprove'); ?>',
			type: 'POST',
			dataType: 'json',
			data: {
				item_id: item_id
			},
			success: function (response) {
				if (response.response == 'success') {
					$(".approveoption_" + item_id).hide();
					element.closest('tr').find('.rate_highlight').removeClass('rate_highlight');
					$().toastmessage('showSuccessToast', "" + response.msg + "");
				} else if (response.response == 'warning') {
					$(".approveoption_" + item_id).hide();
					element.closest('tr').find('.rate_highlight').removeClass('rate_highlight');
					$().toastmessage('showWarningToast', "" + response.msg + "");
				} else {
					$().toastmessage('showErrorToast', "" + response.msg + "");
				}
			}
		});
	});

	$("#previous_details").hide();

	$(document).on('mouseover', 'td.rate_highlight', function (e) {
		var item_id = $(this).attr('id');
		$.ajax({
			url: '<?php echo Yii::app()->createAbsoluteUrl('purchase/previousratedetails'); ?>',
			type: 'GET',
			dataType: 'json',
			data: {
				item_id: item_id
			},
			success: function (result) {
				if (result.status == 1) {
					$('#previous_details').html(result.html);
					$("#previous_details").show();
					$("#pre_fixtable2").tableHeadFixer();
				} else {
					$('#previous_details').html(result.html);
					$("#previous_details").hide();
				}
			}
		});
	});
	$(document).on('mouseout', '.rate_highlight', function (e) {
		$("#previous_details").hide();
	});
	$("#purchaseno").keyup(function () {
		if (this.value.match(/[^a-zA-Z0-9.:]/g)) {
			this.value = this.value.replace(/[^a-zA-Z0-9.:\-/]/g, '');
		}
	});

	$(document).on('click', '.getprevious', function () {
		var id = $(this).attr('data-id');
		var res = id.split(",");
		var amount = parseFloat(res[4]);
		$('#description').val(res[0]).trigger('change.select2');
		$('#quantity').val(res[1]);
		$('#item_unit').html(res[2]);
		$('#rate').val(res[3]);
		$('#item_amount').text(amount.toFixed(2));
		$('#length').val(res[5]);
		$('#remarks').val(res[6]);
		var total = (res[1] * res[3]);
		if (isNaN(total)) total = 0;
		$('#dis_amount').val(res[7]);
		$("#disp").html(res[8]);
		$('#tax_slab').val(res[9]);
		$('#sgstp').val(res[11]);
		$('#sgst_amount').html(res[10]);
		$('#cgstp').val(res[13]);
		$('#cgst_amount').html(res[12]);
		$('#igstp').val(res[15]);
		$('#igst_amount').html(res[14]);
		$('#effective_quantity').val((res[1] * res[5]));
		$('#previousvalue').text(total.toFixed(2));
	})
	$("#quantity, #rate,#dis_amount, #sgstp, #cgstp, #igstp").blur(function () {
		var quantity = parseFloat($("#effective_quantity").val());
		var rate = parseFloat($("#rate").val());
		var amount = quantity * rate;
		var new_amount = 0;
		var dis_amount = parseFloat($("#dis_amount").val());
		var sgst = parseFloat($("#sgstp").val());
		if (sgst > 100 || sgst < 0) {
			$().toastmessage('showErrorToast', "Percentage value should be between 0 and 100");
			$("#sgstp").val(0);
			sgst = 0;
		}
		var cgst = parseFloat($("#cgstp").val());
		if (cgst > 100 || cgst < 0) {
			$().toastmessage('showErrorToast', "Percentage value should be between 0 and 100");
			$("#cgstp").val(0);
			cgst = 0;
		}
		var igst = parseFloat($("#igstp").val());
		if (igst > 100 || igst < 0) {
			$().toastmessage('showErrorToast', "Percentage value should be between 0 and 100");
			$("#igstp").val(0);
			igst = 0;
		}
		if (isNaN(dis_amount)) dis_amount = 0;
		if (isNaN(amount)) amount = 0;
		new_amount = amount - dis_amount;
		if (isNaN(new_amount)) new_amount = 0;
		var sgst_amount = (sgst / 100) * new_amount;
		var cgst_amount = (cgst / 100) * new_amount;
		var igst_amount = (igst / 100) * new_amount;
		if (isNaN(sgst_amount)) sgst_amount = 0;
		if (isNaN(cgst_amount)) cgst_amount = 0;
		if (isNaN(igst_amount)) igst_amount = 0;
		var tax_amount = sgst_amount + cgst_amount + igst_amount;
		var total_amount = new_amount + tax_amount;
		var disp = (dis_amount / amount) * 100;
		if (isNaN(disp)) disp = 0;
		$("#disp").html(disp.toFixed(2));
		$("#disc_amount").html(dis_amount.toFixed(2));
		$("#item_amount").html(amount.toFixed(2));
		$("#sgst_amount").html(sgst_amount.toFixed(2));
		$("#cgst_amount").html(cgst_amount.toFixed(2));
		$("#igst_amount").html(igst_amount.toFixed(2));
		$("#tax_amount").html(tax_amount.toFixed(2));
		$("#total_amount").html(total_amount.toFixed(2));
	});
</script>

<style>
	.table-scroll {
		position: relative;
		max-width: 1280px;
		width: 100%;
		margin: auto;
		display: table;
	}

	.table-wrap {
		width: 100%;
		display: block;
		overflow: auto;
		position: relative;
		z-index: 1;
		border: 1px solid #ddd;
	}

	.table-wrap.fixedON,
	.table-wrap.fixedON table,
	.faux-table table {
		height: 380px;
		/* match heights*/
	}

	.table-scroll table {
		width: 100%;
		margin: auto;
		border-collapse: separate;
		border-spacing: 0;
		border: 1px solid #ddd;
	}

	.table-scroll th,
	.table-scroll td {
		padding: 5px 10px;
		border: 1px solid #ddd;
		background: #fff;
		vertical-align: top;
	}

	.faux-table table {
		position: absolute;
		top: 0;
		left: 0;
		width: 100%;
		pointer-events: none;
	}

	.faux-table table tbody {
		visibility: hidden;
	}

	/* shrink cells in cloned table so that the table height is exactly 300px so that the header and footer appear fixed */
	.faux-table table tbody th,
	.faux-table table tbody td {
		padding-top: 0;
		padding-bottom: 0;
		border-top: none;
		border-bottom: none;
		line-height: 0.1;
	}

	.faux-table table tbody tr+tr th,
	.faux-table tbody tr+tr td {
		line-height: 0;
	}

	.faux-table thead th,
	.faux-table tfoot th,
	.faux-table tfoot td,
	.table-wrap thead th,
	.table-wrap tfoot th,
	.table-wrap tfoot td {
		background: #eee;
	}

	.faux-table {
		position: absolute;
		top: 0;
		right: 0;
		left: 0;
		bottom: 0;
		overflow-y: scroll;
	}

	.faux-table thead,
	.faux-table tfoot,
	.faux-table thead th,
	.faux-table tfoot th,
	.faux-table tfoot td {
		position: relative;
		z-index: 2;
	}

	/* ie bug */
	.table-scroll table thead tr,
	.table-scroll table thead tr th,
	.table-scroll table tfoot tr,
	.table-scroll table tfoot tr th,
	.table-scroll table tfoot tr td {
		height: 1px;
	}
</style>
<script>
	(function () {
		var mainTable = document.getElementById("main-table");
		var tableHeight = mainTable.offsetHeight;
		if (tableHeight > 380) {
			var fauxTable = document.getElementById("faux-table");
			document.getElementById("table-wrap").className += ' ' + 'fixedON';
			var clonedElement = mainTable.cloneNode(true);
			clonedElement.id = "";
			fauxTable.appendChild(clonedElement);
		}
	})();
</script>
<script>
	$(".popover-test").popover({
		html: true,
		content: function () {
			//return $('#popover-content').html();
			return $(this).next('.popover-content').html();
		}
	});
	$('[data-toggle=popover]').on('click', function (e) {
		$('[data-toggle=popover]').not(this).popover('hide');
	});
	$('body').on('hidden.bs.popover', function (e) {
		$(e.target).data("bs.popover").inState.click = false;
	});
	$('body').on('click', function (e) {
		$('[data-toggle=popover]').each(function () {
			// hide any open popovers when the anywhere else in the body is clicked
			if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
				$(this).popover('hide');
			}
		});
	});

	$(document).ajaxComplete(function () {
		$(".popover-test").popover({
			html: true,
			content: function () {
				return $(this).next('.popover-content').html();
			}
		});


	});

	$(document).on("change", ".delivery_date", function () {
		var element = $(this);
		var purchase_id = $("#purchase_id").val();
		var default_date = $(".date").val();
		var delivery_date = $(this).val();
		var project = $('#project').val();
		var vendor = $('#vendor').val();
		var purchaseno = $('#purchaseno').val();
		var expense_head = $('#expense_head').val();
		var company = $('#company_id').val();
		if ((moment(default_date, 'DD-MM-YYYY', true).isValid())) {
			if (project == '' || vendor == '' || default_date == '' || purchaseno == '' || expense_head == '') {

			} else {
				$.ajax({
					method: "GET",
					async: false,
					data: {
						purchase_id: purchase_id,
						purchaseno: purchaseno,
						default_date: default_date,
						project: project,
						vendor: vendor,
						expense_head: expense_head,
						company: company,
						delivery_date: delivery_date
					},
					dataType: "json",
					url: '<?php echo Yii::app()->createUrl('purchase/createnewpurchase2'); ?>',
					success: function (result) {
						if (result.response == 'success') {
							$(".purchase_items").removeClass('checkek_edit');
							$('.js-example-basic-single').select2('focus');
							$("#purchase_id").val(result.p_id);
							$().toastmessage('showSuccessToast', "" + result.msg + "");
						} else {
							$().toastmessage('showErrorToast', "" + result.msg + "");
						}
					}

				});

			}
		} else {
			$(this).focus();
			$().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
			$(this).focus();
		}

		$('#purchaseno').focus();

	});
</script>
<style>
	.tooltip-hiden {
		width: auto
	}
</style>