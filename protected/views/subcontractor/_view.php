<?php
/* @var $this SubcontractorController */
/* @var $data Subcontractor */
?>

<?php
$tblpx = Yii::app()->db->tablePrefix;
$id = $data->subcontractor_id;
?>

<?php if ($index == 0) { ?>
    <thead class="entry-table">
        <tr>
            <th style="width:20px;">Sl No.</th>
            <th>Subcontractor Name</th>
            <th>Company</th>
            <th>Phone</th>
            <th>Email ID</th>
            <?php if ($fifth_column_width) { ?>
                <th>Expense Head</th>
            <?php } ?>
            <?php if ((isset(Yii::app()->user->role) && ((in_array('/subcontractor/subcontractoredit', Yii::app()->user->menuauthlist))))) { ?>
                <th style="width:40px;">Action</th>
            <?php } ?>
        </tr>
    </thead>
    <?php
}
$style = "";
$title = "";
if ($data['delete_approve_status'] == 1) {
    $style = "style='background-color:#f8cbcb !important'";
    $title = "Delete Request Sent!";
}
$expense_head_content = Subcontractor::model()->getExpensetypes($data->subcontractor_id);
$max_length = 150;
$expense_head_hover_content = "";
if (strlen($expense_head_content) > $max_length) {
    $expense_head_hover_content = 'title="' . CHtml::encode($expense_head_content) . '"';
    $expense_head_short_content = substr($expense_head_content, 0, $max_length - 3) . "..."; // Subtract 3 to make space for "..."
} else {
    $expense_head_short_content = $expense_head_content;
}
?>
<tr <?php echo $style ?> title="<?php echo $title ?>">
    <td style="width:20px;">
        <?php echo $index + 1; ?>
    </td>
    <td>
        <?php echo CHtml::encode($data->subcontractor_name); ?>
    </td>
    <td>
        <?php
        $companyname = array();
        separator:
        $arrVal = explode(',', $data->company_id);
        foreach ($arrVal as $arr) {
            $value = Company::model()->findByPk($arr);
            array_push($companyname, $value['name']);
        }
        echo implode(', ', $companyname);
        ?>
    </td>
    <td>
        <?php echo CHtml::encode($data->subcontractor_phone); ?>
    </td>
    <td>
        <?php echo CHtml::encode($data->subcontractor_email); ?>
    </td>
    <?php if ($fifth_column_width) { ?>
        <td <?= $expense_head_hover_content ?>>
            <?= $expense_head_short_content; ?>
        </td>
    <?php } ?>

    <td class="wrap">
        <?php if ((isset(Yii::app()->user->role) && ((in_array('/subcontractor/subcontractoredit', Yii::app()->user->menuauthlist))))) { ?>
            <a class="fa fa-edit editSubcontractor" data-id="<?php echo $id; ?>"></a>
        <?php } ?>
        <?php if ((isset(Yii::app()->user->role) && ((in_array('/subcontractor/subcontractordelete', Yii::app()->user->menuauthlist))))) { ?>
            <?php
            if ($data['delete_approve_status'] == 0) { ?>
                <a class="fa fa-trash deleteSubcontractor" data-id="<?php echo $id; ?>"></a>
            <?php } ?>
        <?php } ?>
    </td>

</tr>