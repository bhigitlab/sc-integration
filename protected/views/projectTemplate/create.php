<?php
/* @var $this ClientsController */
/* @var $model Clients */

$this->breadcrumbs=array(
	'Clients'=>array('index'),
	'Create',
);

?>

<div class="panel panel-gray">
    <div class="panel-heading form-head">
        <h3 class="panel-title">Add Template</h3>
    </div>
        <?php echo $this->renderPartial('_form', array('model' => $model)); ?>
</div>
