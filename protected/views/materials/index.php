<?php
/* @var $this ExpensesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'Materials',
);
$page = Yii::app()->request->getParam('materials_page');
if ($page != '') {
    Yii::app()->user->setReturnUrl($page);
} else {
    Yii::app()->user->setReturnUrl(0);
}
?>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<div class="container" id="materials">
    <div class="loading-overlay">
        <span class="fa fa-spinner fa-3x fa-spin"></span>
    </div>
    <div class="clearfix">
        <div class="add-btn pull-right">
                <a class="button addmaterials">Add materials</a>
        </div>
        <h2>materials</h2>
    </div>
    <div id="addmaterials" style="display:none;"></div>
    <div id="errMsg"></div><br><br>
    <div class="row">
        <div class="col-md-12">
		<?php $this->widget('zii.widgets.CListView', array(
			'dataProvider'=>$dataProvider,
			'itemView'=>'_view',
			'template' => '<div class="container1"><table cellpadding="10" id="materialtbl" class="table">{items}</table></div>',
                'ajaxUpdate' => false,
		)); ?>
        </div>

        <?php
        Yii::app()->clientScript->registerScript('myjavascript', '
             $(document).ready(function(){
               $("#materialtbl").dataTable( {
                    "scrollY": "300px",
                    "scrollCollapse": true,
                   
                     "columnDefs"        : [       
                        { 
                            "searchable"    : false, 
                            "targets"       : [0,2] 
                        },
                        { "bSortable": false, "aTargets": [-1]  }
                    ],
                } );
                });

            ');
        ?>

        <script>
            function closeaction() {
                $('#addmaterials').slideUp(500);
                location.reload();
            }

            $(document).ready(function() {
                $('.addmaterials').click(function() {
                    $('.loading-overlay').addClass('is-active');
                    var id = $(this).attr('data-id');
                    $.ajax({
                        type: "POST",
                        url: "<?php echo $this->createUrl('materials/create&layout=1') ?>",
                        success: function(response) {
                            $('#addmaterials').html(response).slideDown();
                        },
                    });
                });
                $('.editProject').click(function() {
                    $('.loading-overlay').addClass('is-active');
                    var id = $(this).attr('data-id');
                    $.ajax({
                        type: "POST",
                        url: "<?php echo $this->createUrl('materials/update&layout=1&id=') ?>" + id,
                        success: function(response) {
                            $('.loading-overlay').removeClass('is-active');
                            $('#addmaterials').html(response).slideDown();
                        },
                    });
                });
                $('.deleteProject').click(function() {
					if (confirm("Are you sure you want to delete this project?")) {
						$('.loading-overlay').addClass('is-active');
						var id = $(this).attr('data-id');
						$.ajax({
							type: "POST",
							url: "<?php echo $this->createUrl('materials/delete&layout=1&id=') ?>" + id,
							dataType: 'json',
							success: function(response) {
								console.log();
								$('html, body').animate({ scrollTop: 0 }, 'slow');
								$("#errMsg").show()
									.html('<div class="alert alert-'+response.success+'">'+response.message+'</div>')
									.fadeOut(10000);
								setTimeout(function () {                        
									location.reload(true);
								}, 1000);
							},
							error: function() {
								$('.loading-overlay').removeClass('is-active');
								alert('There was an error deleting the project. Please try again.');
							}
						});
					}
				});



                jQuery(function($) {
                    $('#addmaterialstype').on('keydown', function(event) {
                        if (event.keyCode == 13) {
                            $("#materialssearch").submit();
                        }
                    });
                });

            });
            $(document).ajaxComplete(function() {
                $('.loading-overlay').removeClass('is-active');
                $('#loading').hide();
            });
        </script>
    </div>
</div>
<style>

    .page-body h3 {
        margin: 4px 0px;
        color: inherit;
        text-align: left;
    }

    .panel {
        border: 1px solid #ddd;
    }

    .panel-heading {
        background-color: #eee;
        height: 40px;
    }
</style>