<style>
.required_label::after {
    content: " *"; /* Adding an asterisk as content */
    color: red; /* Setting the color to red */
}
</style>
<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'section-form',
    'enableAjaxValidation' => true,
    'enableClientValidation' => true,
    //'action' => Yii::app()->createAbsoluteUrl("salesquotation/salesQuotation/addquotationitems"),
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
    'clientOptions' => array(
        'validateOnSubmit' => true,
        'validateOnChange' => true,
        'validateOnType' => false,
    ),
        ));
?>
<div class="worktype_label_data">
    <div class="row worktype_label_row">      
        <div class="form-group col-xs-12 col-md-2">
            <label class="required_label">Work Type </label> 
            <?php echo $form->hiddenField($main_wtype_model, 'id', array('id' => 'worktype_label_id')); ?>                     
            <?php
            echo $form->textField($main_wtype_model, 'worktype_label', array('class' => 'form-control require', 'id' => 'worktype_label', 'placeholder' => 'Work Type'));
            ?>
            <div class="errorMessage"></div>
        </div>
        <div class="form-group col-xs-12 col-md-2">
            <label>Category&nbsp;</label>
            <?php
            echo $form->dropDownList($main_wtype_model, 'master_cat_id',
                    CHtml::listData(QuotationCategoryMaster::model()->findAll(), 'id', 'name'),
                    array('class' => 'form-control js-example-basic-single field_change require QuotationGenWorktype_master_cat_id', 'empty' => '-Select Category-', 'style' => 'width:100%'));
            ?>
            <?php echo CHtml::hiddenField('category_id_data', '', array('id' => 'category_id_data')); ?>
        </div>
        <div class="form-group col-xs-12 col-md-2">
            <label>Unit&nbsp;</label>
            <?php
            echo $form->dropDownList($main_wtype_model, 'unit', CHtml::listData(Unit::model()->findAll(), 'id', 'unit_name'), array('class' => 'form-control js-example-basic-single   require ', 'empty' => '-Select Unit-', 'name' => 'unit', 'style' => 'width:100%', 'id' => 'wrktype_unit')
            );
            ?>
        </div>
        <div class="form-group col-xs-12 col-md-2">
            <label>Quantity&nbsp;</label>
            <?php echo $form->textField($main_wtype_model, 'quantity', array('class' => 'form-control allownumericdecimal quantity_sub', 'autocomplete' => 'off', 'placeholder' => 'qty', 'name' => 'quantity', 'id' => 'wrktype_quantity')); ?>
            <?php echo $form->error($main_wtype_model, 'quantity'); ?>
        </div>
        <?php 
        $activeProjectTemplate = $this->getActiveTemplate();
        if ($activeProjectTemplate != 'TYPE-5') {
        ?>
        <div class="form-group col-xs-12 col-md-2">
            <label>Quantity Number&nbsp;</label>
            <?php echo $form->textField($main_wtype_model, 'quantity_nos', array('class' => 'form-control allownumericdecimal ', 'autocomplete' => 'off', 'placeholder' => 'qty nos', 'name' => 'quantity_nos', 'id' => 'wrktype_quantity_nos')); ?>
            <?php echo $form->error($main_wtype_model, 'quantity_nos'); ?>
        </div>
        <?php } ?>
        <div class="form-group col-xs-12 col-md-2">
            <label>MRP&nbsp;</label>
            <?php echo $form->textField($main_wtype_model, 'mrp', array('class' => 'form-control allownumericdecimal ', 'autocomplete' => 'off', 'placeholder' => 'MRP', 'name' => 'mrp', 'id' => 'wrktype_mrp')); ?>
            <?php echo $form->error($main_wtype_model, 'mrp'); ?>
        </div>
        <div class="form-group col-xs-12 col-md-2">
            <label>Amount After Discount&nbsp;</label>
            <?php echo $form->textField($main_wtype_model, 'amount_after_discount', array('class' => 'form-control allownumericdecimal amountafter-discount', 'autocomplete' => 'off', 'name' => 'amount_after_discount', 'placeholder' => 'amt after disc', 'id' => 'wrktype_amount_after_discount')); ?>
            <?php echo $form->error($main_wtype_model, 'amount_after_discount'); ?>
        </div>
        <div class="form-group col-xs-12 col-md-5">
            <label>Description &nbsp;</label>
            <textarea class="form-control mb-2" name="description" id="wrktype_desc" rows="1"></textarea>
        </div>
        <br><br><br>
        <div class="sub_worktype_selector" id="quotation_item"></div>
    </div>

</div>
<div class="panel-footer save-btnHold text-center button-panel">
    <button type="button" class = 'btn btn-info additem'>Save</button>
    <button type="button" class ='btn btn-default btn_close'>Close</button>
</div>
<?php $this->endWidget(); ?>
<style>
    .row.worktype_label_row > .form-group col-xs-12 col-md-2{
        height:67px;
    }
</style>