<?php
/* @var $this MaterialRequisitionController */
/* @var $model MaterialRequisition */
/* @var $form CActiveForm */
?>
<script
    src="<?php echo $this->customAssets('https://code.jquery.com/jquery-1.12.4.js', '/js/jquery-1.12.4.js'); ?>"></script>
<script
    src="<?php echo $this->customAssets('https://code.jquery.com/ui/1.12.1/jquery-ui.js', '/js/jquery-ui.js'); ?>"></script>

<div class="container">
    <div class="header-container">
        <h3>Add Material Requisition</h3>
        <?php echo CHtml::link('Back', array('MaterialRequisition/index'), array('class' => 'btn btn-info', 'id' => 'materialrequest')); ?>
    </div>
    <div>
        <p id="success_message" style="display:none;font-size: 15px;color: green;font-weight: bold;">Successfully
            created..</p>
        <p id="error_message" style="display:none;font-size: 15px;color: red;font-weight: bold;">Something went wrong..
        </p>
        <div class="entries-wrapper">
            <div class="row">
                <div class="col-xs-12">
                    <div class="heading-title">Details</div>
                    <div class="dotted-line"></div>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-xs-12 col-md-3">
                    <label>Company</label>
                    <select name="MaterialRequisition[company_id]" class="inputs target project form-control"
                        id="company_id" required>
                        <option value="">Choose Company</option>
                        <?php
                        foreach ($company as $key => $value) {
                            ?>
                            <option value="<?php echo $value['id']; ?>">
                                <?php echo $value['name']; ?>
                            </option>
                        <?php } ?>
                    </select>
                </div>
                <div class="form-group col-xs-12 col-md-3">
                    <label>Project</label>
                    <select name="MaterialRequisition[project_id]" class="inputs target project form-control"
                        id="project_id">
                        <option value="">Choose Project</option>
                        <?php
                        foreach ($project as $key => $value) {
                            ?>
                            <option value="<?php echo $value['pid']; ?>">
                                <?php echo $value['name']; ?>
                            </option>
                        <?php } ?>
                    </select>
                </div>

                <div class="form-group col-xs-12 col-md-3">
                    <label>Task</label>
                    <input type="text" name="MaterialRequisition[task_description]" value=""
                        id="requisition_task_description" class="form-control">

                </div>
                <div class="form-group col-xs-12 col-md-3">
                    <label>Req.No</label>
                    <input type="text" name="MaterialRequisition[requisition_no]" id="requisition_no"
                        value="<?= $requisition_no ?>" readonly class="form-control">
                </div>
                <div class="col-xs-12 text-right">
                    <button type="submit" class="btn btn-primary" id="create_mr">Create</button>
                </div>
            </div>

            <div id="hide" style="display:none">
                <div class="row">
                    <div class="form-group col-xs-12 col-md-3">
                        <label>Material</label>
                        <input type="hidden" id="material_req_id">
                        <input type="hidden" id="type" name="type" value=<?= $type ?>>
                        <!-- <input type="text" class="form-control" id="requisition_item"> -->
                        <select class="form-control req-item" id="requisition_item">
                            <option value="">Select Item</option>
                            <?php
                            foreach ($account_items as $key => $value) {
                                ?>
                                <option value="<?php echo $value['id']; ?>">
                                    <?php echo $value['data']; ?>
                                </option>
                            <?php } ?>
                            <option value="other">Other</option>
                        </select>
                        <div class="errorMessage" id="item_em_" style="display:none"></div>
                    </div>
                    <div class="form-group col-xs-12 col-md-3" id="item_name_id" style="display:none">
                        <label>Item Name</label>
                        <input type="text" class="form-control" id="requisition_item_name">
                        <div class="errorMessage" id="item_name_em_" style="display:none"></div>
                    </div>
                    <div class="form-group col-xs-12 col-md-3">
                        <label>Unit</label>
                        <select class="form-control" id="requisition_unit">
                            <option value="">Select Unit</option>
                        </select>
                        <div class="errorMessage" id="unit_em_" style="display:none"></div>
                    </div>
                    <div class="form-group col-xs-12 col-md-3">
                        <label>Quantity</label>
                        <input type="number" class="form-control" id="requisition_quantity">
                        <div class="errorMessage" id="quantity_em_" style="display:none"></div>
                    </div>
                    <div class="form-group col-xs-12 col-md-3">
                        <label>Date</label>
                        <input type="text" id="requisition_date" class="form-control">
                        <div class="errorMessage" id="date_em_" style="display:none"></div>
                    </div>
                    <div class="form-group col-xs-12 col-md-3">
                        <label>Remarks </label>
                        <textarea rows="2" cols="20" class="form-control" autocomplete="off"
                            id="requisition_remarks"></textarea>
                        <div class="errorMessage" id="remarks_em_" style="display:none"></div>
                    </div>
                    <div class="col-xs-12 text-right">
                        <input type="button" class="btn btn-primary btn-sm" value="Add" id="add_item">
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div id="table_div"></div>
                </div>
            </div>
            <div class="row" id="create_cancel" style="display:none">
                <div class="col-xs-12 text-right">
                    <?php
                    if ($type == 1) {
                        echo CHtml::link('Create', array('MaterialRequisition/index'), array('class' => 'btn btn-primary'));
                    } else {
                        echo CHtml::link('Create', array('MaterialRequisition/index'), array('class' => 'btn btn-primary'));
                    } ?>
                    <input type="button" class="btn btn-default" value="Cancel" id="cancel_mr">
                </div>
            </div>
        </div>
    </div>
</div>
<!-- main table -->
<?php $task_id = ''; ?>
<script>
    $("#create_mr").click(function () {
        var req_no = $('#requisition_no').val();
        var project_id = $('#project_id').val();
        var task_id = $('#requisition_task_description').val();
        var company_id = $('#company_id').val();
        var type = $('#type').val();
        $.ajax({
            "type": "POST",
            "url": "<?php echo Yii::app()->createUrl('MaterialRequisition/create', array('id' => $task_id, 'type' => $type, )); ?>",
            "dataType": "json",
            "data": {
                req_no: req_no,
                project_id: project_id,
                company_id: company_id,
                task_id: task_id
            },
            "success": function (data) {
                if (data.status == 1) {
                    $('#hide').show();
                    $('#create_mr').hide();
                    $('#material_req_id').val(data.mr_id);
                } else {
                    $("#error_message").fadeIn().delay(1000).fadeOut();
                }
            }
        });

    });

    $("#add_item").click(function () {
        var requisition_item = $('#requisition_item').val();
        var requisition_unit = $('#requisition_unit').val();
        var requisition_quantity = $('#requisition_quantity').val();
        var requisition_date = $('#requisition_date').val();
        var requisition_remarks = $('#requisition_remarks').val();
        var material_req_id = $('#material_req_id').val();
        var requisition_item_name = $('#requisition_item_name').val();

        if (requisition_item != "" && requisition_unit != "" && requisition_quantity != "" && requisition_date != "" && requisition_remarks != "") {
            $("#item_em_").hide();
            $("#unit_em_").hide();
            $("#quantity_em_").hide();
            $("#date_em_").hide();
            $("#remarks_em_").hide();
            $.ajax({
                "type": "POST",

                "url": "<?php echo Yii::app()->createUrl('MaterialRequisition/adddItem'); ?>",
                "dataType": "json",
                "data": {
                    requisition_item: requisition_item,
                    requisition_unit: requisition_unit,
                    requisition_quantity: requisition_quantity,
                    requisition_date: requisition_date,
                    requisition_remarks: requisition_remarks,
                    material_req_id: material_req_id,
                    requisition_item_name: requisition_item_name
                },
                "success": function (data) {

                    $("#success_message").fadeIn().delay(1000).fadeOut();
                    $('#requisition_item').val("");
                    $('#requisition_unit').val("");
                    $('#requisition_quantity').val("");
                    $('#requisition_date').val("");
                    $('#requisition_remarks').val("");
                    $('#table_div').html(data);
                    $('#create_cancel').show();
                }
            });
        } else {
            if (requisition_item == "") {
                $("#item_em_").show();
                $("#item_em_").html('Item cannot be blank.');
            }
            if (requisition_unit == "") {
                $("#unit_em_").show();
                $("#unit_em_").html('Unit cannot be blank.');
            }
            if (requisition_quantity == "") {
                $("#quantity_em_").show();
                $("#quantity_em_").html('Quantity cannot be blank.');
            }
            if (requisition_date == "") {
                $("#date_em_").show();
                $("#date_em_").html('Quantity cannot be blank.');
            }
            if (requisition_remarks == "") {
                $("#remarks_em_").show();
                $("#remarks_em_").html('Remark cannot be blank.');
            }
            if (requisition_item_name == "") {
                $('#item_name_em_').show();
                $("#item_name_em_").html('item name cannot be blank.');
            }
        }
    });

    $("#cancel_mr").click(function () {
        if (!confirm('Are you sure you want to cancel this MR?') === false) {
            var mr_id = $('#material_req_id').val();
            $.ajax({
                "type": "POST",

                "url": "<?php echo Yii::app()->createUrl('MaterialRequisition/deleteMR'); ?>",
                "dataType": "json",
                "data": {
                    mr_id: mr_id,
                },
                "success": function (data) {

                    location.reload();
                }
            });
        }

    });

    $('.req-item').change(function () {
        var item_id = $('.req-item').val();
        if (item_id == 'other') {
            $('#item_name_id').show();
        } else {
            $('#item_name_id').hide();
        }


        $.ajax({
            url: '<?php echo Yii::app()->createUrl('MaterialRequisition/getRelatedUnit'); ?>',
            method: 'POST',
            data: {
                item_id: item_id
            },
            dataType: "json",
            success: function (response) {
                $("#requisition_unit").html(response.html);
            }
        })
    })

    $('#requisition_date').datepicker({
        dateFormat: 'dd-M-y',
    });
    $("#company_id").change(function () {
        var val = $(this).val();
        $("#project_id").html('<option value="">Select Project</option>');
        $.ajax({
            url: '<?php echo Yii::app()->createUrl('invoice/dynamicproject'); ?>',
            method: 'POST',
            data: {
                company_id: val
            },
            dataType: "json",
            success: function (response) {
                if (response.status == 'success') {
                    $("#project_id").html(response.html);
                } else {
                    $("#project_id").html(response.html);
                }
            }

        })
    })
    $("#company_id").change(function () {
        var company_id = $("#company_id").val();
        var material_req_id = $("#requisition_no").val();
        //alert(material_req_id);
        $.ajax({
            'url': '<?php echo Yii::app()->createUrl('MaterialRequisition/getDynamicMaterialReqNo'); ?>',
            'data': {
                'company_id': company_id,
                'material_req_id': material_req_id,
            },
            'method': 'POST',
            'dataType': 'JSON',
            'success': function (response) {
                if (response.status == 1) {
                    $("#requisition_no").val(response.mr_no);
                    $("#requisition_no").attr('readonly', 'readonly');
                } else {
                    $("#requisition_no").val(material_req_id);
                }


            }
        })
    })
</script>