<?php
/* @var $this ExpensesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'Vendors',
);
$page = Yii::app()->request->getParam('Vendors_page');
if ($page != '') {
    Yii::app()->user->setReturnUrl($page);
} else {
    Yii::app()->user->setReturnUrl(0);
}
?>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>

<div class="container" id="vendors">
    <div class="loading-overlay">
        <span class="fa fa-spinner fa-3x fa-spin"></span>
    </div>
    <div class="sub-heading margin-bottom-10">
        <h2>Company</h2>
        <?php if (isset(Yii::app()->user->role) && (in_array('/company/create', Yii::app()->user->menuauthlist))) { ?>
            <button class="createcomp btn btn-primary">Add Company</button>
        <?php } ?>
    </div>
    <div id="companyform" style="display:none;"></div>

    <div id="errMsg"></div>
    <div class="">
        <?php $this->widget('zii.widgets.CListView', array(
            'dataProvider' => $dataProvider,
            'itemView' => '_view',
            'template' => '<div class=""><table cellpadding="10" class="table" id="companytble">{items}</table></div>',
            'ajaxUpdate' => false,
        )); ?>


        <?php
        Yii::app()->clientScript->registerScript('myjavascript', '
     $(document).ready(function(){
     
       $("#companytble").dataTable( {
            "scrollY": "300px",
            "scrollCollapse": true,
             "scrollX": true,
            // "paging": false,
            "columnDefs"        : [       
                { 
                    "searchable"    : false, 
                    "targets"       : [0] 
                },{ "bSortable": false, "aTargets": [-1]  }
                
            ],
            
            
	} );
	
	
      
        
	});
        
    ');
        ?>

        <script>
            function closeaction() {
                $('#companyform').slideUp();
            }

            function editcomp(comp_id) {
                $("html, body").animate({
                    scrollTop: 0
                }, "slow");
                var id = comp_id;
                $('.loading-overlay').addClass('is-active');
                $.ajax({
                    type: "GET",
                    url: "<?php echo $this->createUrl('company/update&layout=1&id=') ?>" + id,
                    success: function (response) {
                        $("#companyform").html(response).slideDown();
                        $('.loading-overlay').removeClass('is-active');

                    }
                });

            };
            $(document).ready(function () {
                $('.createcomp').click(function () {
                    $('.loading-overlay').addClass('is-active');
                    $.ajax({
                        type: "GET",
                        url: "<?php echo $this->createUrl('company/create&layout=1') ?>",
                        success: function (response) {
                            $("#companyform").html(response).slideDown();

                        }
                    });
                });




                jQuery(function ($) {
                    $('#vendor').on('keydown', function (event) {
                        if (event.keyCode == 13) {
                            $("#vendorssearch").submit();




                        }


                    });
                });

            });
            $(document).ajaxComplete(function () {
                $('.loading-overlay').removeClass('is-active');
                $('#loading').hide();
            });

            function deletCompany(comp_id) {
                if (confirm("Do you want to remove this item?")) {
                    var id = comp_id;
                    $.ajax({
                        type: "POST",
                        dataType: "json",
                        url: "<?php echo $this->createUrl('company/removecompany&id=') ?>" + id,
                        success: function (data) {
                            $("#errMsg").show()
                                .html('<div class="alert alert-' + data.response + '">' + data.msg + '</div>')
                                .fadeOut(10000);
                            setTimeout(function () {
                                location.reload(true);
                            }, 3000);
                        }
                    });
                }
            };
        </script>



    </div>
</div>

<style>
    .page-body h3 {
        margin: 4px 0px;
        color: inherit;
        text-align: left;
    }

    .panel {
        border: 1px solid #ddd;
    }

    .panel-heading {
        background-color: #eee;
        height: 40px;
    }
</style>