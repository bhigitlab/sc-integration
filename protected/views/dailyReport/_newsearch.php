<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<div class="page_filter clearfix custom-form-style">
    <?php 
    // Calculate default date_from (previous Saturday) and date_to (upcoming Friday)
    $currentDate = date('d-m-Y');
    $previousSaturday = date('d-m-Y', strtotime('last Saturday', strtotime($currentDate)));
    if (date('l', strtotime($currentDate)) === 'Saturday') {
        $previousSaturday = $currentDate;
    }
    $upcomingFriday = date('d-m-Y', strtotime('next Friday', strtotime($currentDate)));
    if (date('l', strtotime($currentDate)) === 'Friday') {
        $upcomingFriday = $currentDate;
    }

    // Set defaults if GET parameters are empty
    $datefrom = isset($_GET['date_from']) && $_GET['date_from'] !== '' ? $_GET['date_from'] : $previousSaturday;
    $dateto = isset($_GET['date_to']) && $_GET['date_to'] !== '' ? $_GET['date_to'] : $upcomingFriday;

    $form = $this->beginWidget('CActiveForm', array(
        'action' => Yii::app()->createUrl($this->route),
        'method' => 'get',
    ));
    ?>
    <div class="row">
        <div class="form-group col-xs-12 col-sm-3">
            <label>Sub Contractor</label>
            <select id="subcontractor_id" class="select_box" name="subcontractor_id">
                <option value="">Select subcontractor</option>
                <?php
                if (!empty($subcontractor)) {
                    foreach ($subcontractor as $key => $value) { ?>
                        <option value="<?php echo $value['subcontractor_id']; ?>"
                            <?php echo ($value['subcontractor_id'] == $model->subcontractor_id) ? 'selected' : ''; ?>>
                            <?php echo $value['subcontractor_name']; ?>
                        </option>
                    <?php }
                } ?>
            </select>
        </div>
        <div class="form-group col-xs-12 col-sm-3">
            <label>Project</label>
            <select  id="projectid" class="select_box" name="project_id" class="sel">
                <option value="">Select project</option>
                <?php
                if (!empty($project)) {
                    foreach ($project as $key => $value) { ?>
                        <option value="<?php echo $value['pid']; ?>"
                            <?php echo ($value['pid'] == $model->projectid) ? 'selected' : ''; ?>>
                            <?php echo $value['name']; ?>
                        </option>
                    <?php }
                } ?>
            </select>
        </div>
        <div class="form-group col-xs-12 col-sm-3">
            <label>From</label>
            <?php echo CHtml::textField('date_from', $datefrom, array(
                'placeholder' => 'DD-MM-YYYY',
                'class' => 'form-control',
            )); ?>
        </div>
        <div class="form-group col-xs-12 col-sm-3">
            <label>To</label>
            <?php echo CHtml::textField('date_to', $dateto, array(
                'placeholder' => 'DD-MM-YYYY',
                'class' => 'form-control',
            )); ?>
        </div>
        <div class="form-group col-xs-12 text-right">
                <input name="yt0" value="Go" type="submit" class="btn btn-sm btn-primary">
                <input id="reset" name="yt1" value="Clear" type="reset" class="btn btn-sm btn-default">
        </div>
    </div>
    <?php $this->endWidget(); ?>
</div>

<script>
    $('#reset').click(function () {
        location.href = '<?php echo $this->createUrl('weeklypayment'); ?>';
    });

    $(document).ready(function () {
        // Initialize Select2
        $(".select_box").select2();

        // Initialize Datepickers
        $("#date_from").datepicker({
            dateFormat: 'dd-mm-yy',
            defaultDate: '<?php echo $datefrom; ?>'
        });
        $("#date_to").datepicker({
            dateFormat: 'dd-mm-yy',
            defaultDate: '<?php echo $dateto; ?>'
        });

        // Set min and max dates dynamically
        $("#date_from").change(function () {
            $("#date_to").datepicker('option', 'minDate', $(this).val());
        });
        $("#date_to").change(function () {
            $("#date_from").datepicker('option', 'maxDate', $(this).val());
        });
    });
</script>

<style>
    .search-form {
        background-color: #fafafa;
        padding: 10px;
        box-shadow: 0px 0px 2px 0px rgba(0, 0, 0, 0.1);
        margin-bottom: 10px;
    }

    .search-form input, .search-form select {
        margin-bottom: 10px;
    }
</style>
