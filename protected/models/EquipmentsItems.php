<?php

/**
 * This is the model class for table "{{equipments_items}}".
 *
 * The followings are the available columns in table '{{equipments_items}}':
 * @property integer $id
 * @property integer $equipment_entry_id
 * @property integer $pms_template_equipment_id
 * @property integer $equipments
 * @property integer $quantity
 * @property string $unit
 *
 * The followings are the available model relations:
 * @property EquipmentsEntries $equipmentEntry
 * @property Equipments $equipments0
 */
class EquipmentsItems extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{equipments_items}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('equipment_entry_id, equipments, quantity,rate,amount,unit', 'required'),
			array('equipment_entry_id, equipments, quantity', 'numerical', 'integerOnly'=>true),
			array('unit', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, equipment_entry_id, equipments, quantity,rate,amount, unit', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'equipmentEntry' => array(self::BELONGS_TO, 'EquipmentsEntries', 'equipment_entry_id'),
			'equipments0' => array(self::BELONGS_TO, 'Equipments', 'equipments'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'equipment_entry_id' => 'Equipment Entry',
			'equipments' => 'Equipments',
			'quantity' => 'Quantity',
			'unit' => 'Unit',
			'rate'=>'Rate',
			'amount'=>'Amount',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('equipment_entry_id',$this->equipment_entry_id);
		$criteria->compare('equipments',$this->equipments);
		$criteria->compare('quantity',$this->quantity);
		$criteria->compare('unit',$this->unit,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return EquipmentsItems the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
