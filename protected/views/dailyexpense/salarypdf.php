<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/pdfstyle.css">

<?php
    $company_address = Company::model()->findBypk(($company_id)?$company_id:Yii::app()->user->company_id);	
?>
<h3 class="text-center">Salary Report</h3> 
<div class="container-fluid pdf_spacing">
            
            <table class="table table-bordered table-striped" border="1">

                <thead>
                    <tr>
                        <th>Sl No.</th>
                        <th>Employee Name</th>
                        <th>Company</th>
                        <th align="right" style="text-align:right">Salary Advance</th>
                        <th align="right" style="text-align:right">Salary Paid</th>
                        <th align="right" style="text-align:right">Total</th>
                    </tr>
                    <?php
                     $tblpx = Yii::app()->db->tablePrefix;
                     $user = Users::model()->findByPk(Yii::app()->user->id);
                     $arrVal = explode(',', $user->company_id);
                     $newQuery1 = "";
                     foreach($arrVal as $arr) {
                        if ($newQuery1) $newQuery1 .= ' OR';
                        $newQuery1 .= " FIND_IN_SET('".$arr."', ".$tblpx."dailyexpense.company_id)";
                     }
                    /***************** */
                    $t_advance = 0;
                    $t_salary =0;
                    $t_total = 0;
                    //$employee = Yii::app()->db->createCommand("SELECT * FROM ".$tblpx."users WHERE  company_id =".Yii::app()->user->company_id." AND user_type NOT IN (1) ORDER BY userid DESC")->queryAll();
                    $employee = Yii::app()->db->createCommand("SELECT *,".$tblpx."users.company_id as companyid FROM ".$tblpx."dailyexpense LEFT JOIN ".$tblpx."users ON ".$tblpx."dailyexpense.employee_id = ".$tblpx."users.userid LEFT JOIN ".$tblpx."company_expense_type ON ".$tblpx."dailyexpense.exp_type_id = ".$tblpx."company_expense_type.company_exp_id  WHERE (".$newQuery1.") AND ".$tblpx."dailyexpense.exp_type=73 AND (".$tblpx."company_expense_type.name='SALARY ADVANCE' OR ".$tblpx."company_expense_type.name='SALARY') AND ".$tblpx."dailyexpense.dailyexpense_paidamount > 0 AND ".$tblpx."users.user_type NOT IN (1) GROUP BY ".$tblpx."users.userid")->queryAll();
                    foreach($employee as $key=> $value) {
                     
                      $advance_salary = Yii::app()->db->createCommand("SELECT SUM(".$tblpx."dailyexpense.dailyexpense_paidamount) as amount FROM ".$tblpx."dailyexpense LEFT JOIN ".$tblpx."users ON ".$tblpx."dailyexpense.employee_id= ".$tblpx."users.userid LEFT JOIN ".$tblpx."company_expense_type ON ".$tblpx."dailyexpense.exp_type_id = ".$tblpx."company_expense_type.company_exp_id   WHERE ".$tblpx."dailyexpense.exp_type =73 AND ".$tblpx."dailyexpense.employee_id = ".$value['userid']." AND (".$newQuery1.") AND ".$tblpx."company_expense_type.name='SALARY ADVANCE'")->queryRow();
                      
                      $salary = Yii::app()->db->createCommand("SELECT SUM(".$tblpx."dailyexpense.dailyexpense_paidamount) as amount FROM ".$tblpx."dailyexpense LEFT JOIN ".$tblpx."users ON ".$tblpx."dailyexpense.employee_id= ".$tblpx."users.userid LEFT JOIN ".$tblpx."company_expense_type ON ".$tblpx."dailyexpense.exp_type_id = ".$tblpx."company_expense_type.company_exp_id   WHERE ".$tblpx."dailyexpense.exp_type =73 AND ".$tblpx."dailyexpense.employee_id = ".$value['userid']." AND (".$newQuery1.") AND ".$tblpx."company_expense_type.name='SALARY'")->queryRow();
                      
                      
                      $t_advance += $advance_salary['amount']; 
                      $t_salary += $salary['amount'];
                      $t_total += $t_advance+$t_salary;
                    }
                    /***************** */
                    ?>
                    <tr>
                         <td colspan="3" align="right"><b>Total</b></td>
                         <td align="right"><?php  echo Controller::money_format_inr($t_advance,2); //echo Controller::money_format_inr($totalcredit,0,".",""); ?></td>
                         <td align="right"><?php  echo Controller::money_format_inr($t_salary,2); //echo Controller::money_format_inr($totaldebit,0,".",""); ?></td>
                         <td align="right"><?php  echo Controller::money_format_inr($t_advance+$t_salary,2);//echo Controller::money_format_inr($totalremaining,0,".",""); ?></td>
                     </tr>
                </thead>
                         <tbody>
                <?php 
               
                    
                  $t_advance = 0;
                  $t_salary =0;
                  $t_total = 0;
                  //$employee = Yii::app()->db->createCommand("SELECT * FROM ".$tblpx."users WHERE  company_id =".Yii::app()->user->company_id." AND user_type NOT IN (1) ORDER BY userid DESC")->queryAll();
                  $employee = Yii::app()->db->createCommand("SELECT *,".$tblpx."users.company_id as companyid FROM ".$tblpx."dailyexpense LEFT JOIN ".$tblpx."users ON ".$tblpx."dailyexpense.employee_id = ".$tblpx."users.userid LEFT JOIN ".$tblpx."company_expense_type ON ".$tblpx."dailyexpense.exp_type_id = ".$tblpx."company_expense_type.company_exp_id  WHERE (".$newQuery1.") AND ".$tblpx."dailyexpense.exp_type=73 AND (".$tblpx."company_expense_type.name='SALARY ADVANCE' OR ".$tblpx."company_expense_type.name='SALARY') AND ".$tblpx."dailyexpense.dailyexpense_paidamount > 0 AND ".$tblpx."users.user_type NOT IN (1) GROUP BY ".$tblpx."users.userid")->queryAll();
                  foreach($employee as $key=> $value) {
                   
                    $advance_salary = Yii::app()->db->createCommand("SELECT SUM(".$tblpx."dailyexpense.dailyexpense_paidamount) as amount FROM ".$tblpx."dailyexpense LEFT JOIN ".$tblpx."users ON ".$tblpx."dailyexpense.employee_id= ".$tblpx."users.userid LEFT JOIN ".$tblpx."company_expense_type ON ".$tblpx."dailyexpense.exp_type_id = ".$tblpx."company_expense_type.company_exp_id   WHERE ".$tblpx."dailyexpense.exp_type =73 AND ".$tblpx."dailyexpense.employee_id = ".$value['userid']." AND (".$newQuery1.") AND ".$tblpx."company_expense_type.name='SALARY ADVANCE'")->queryRow();
                    
                    $salary = Yii::app()->db->createCommand("SELECT SUM(".$tblpx."dailyexpense.dailyexpense_paidamount) as amount FROM ".$tblpx."dailyexpense LEFT JOIN ".$tblpx."users ON ".$tblpx."dailyexpense.employee_id= ".$tblpx."users.userid LEFT JOIN ".$tblpx."company_expense_type ON ".$tblpx."dailyexpense.exp_type_id = ".$tblpx."company_expense_type.company_exp_id   WHERE ".$tblpx."dailyexpense.exp_type =73 AND ".$tblpx."dailyexpense.employee_id = ".$value['userid']." AND (".$newQuery1.") AND ".$tblpx."company_expense_type.name='SALARY'")->queryRow();
                    
                    
                    $t_advance += $advance_salary['amount']; 
                    $t_salary += $salary['amount'];
                    $t_total += $t_advance+$t_salary;
                ?>
                <tr>
                    <td><?php echo $key+1; ?></td>
                    <td><?php echo $value['first_name'].' '.$value['last_name']; ?></td>
                    <td>
                        <?php
                        $companyname = array();
                        $user = Users::model()->findByPk(Yii::app()->user->id);
                        $comVal = explode(',', $user->company_id);
                        $arrVal = explode(',', $value['companyid']);
                        foreach($arrVal as $arr) {
                           if(in_array($arr, $comVal)){ 
                            $value = Company::model()->findByPk($arr);
                            if($value){
                                array_push($companyname, $value->name);
                            }
                           }
                        }
                        echo implode(', ', $companyname);
                        ?>
                    </td>
                    <td align="right"><?php echo Controller::money_format_inr($advance_salary['amount'],2) ; ?></td>
                    <td align="right"><?php echo Controller::money_format_inr($salary['amount'],2) ; ?></td>
                    <td align="right"><?php echo Controller::money_format_inr($advance_salary['amount']+$salary['amount'],2) ; ?></td>
                </tr>
                
                  <?php }  ?>
<!--             
-->                     
                    <!-- <tr>
                         <td colspan="3" align="right"><b>Total</b></td>
                         <td align="right"><?php  echo Controller::money_format_inr($t_advance,2); //echo Controller::money_format_inr($totalcredit,0,".",""); ?></td>
                         <td align="right"><?php  echo Controller::money_format_inr($t_salary,2); //echo Controller::money_format_inr($totaldebit,0,".",""); ?></td>
                         <td align="right"><?php  echo Controller::money_format_inr($t_advance+$t_salary,2);//echo Controller::money_format_inr($totalremaining,0,".",""); ?></td>
                     </tr> -->
                 </tbody>

            </table>

        </div>

