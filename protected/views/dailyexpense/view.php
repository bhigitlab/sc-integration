<?php
/* @var $this MaterialRequisitionController */
/* @var $model MaterialRequisition */

$this->breadcrumbs = array(
    'Material Requisitions' => array('index'),
    $model->dailyexp_id,
);
$companyInfo = Company::model()->findByPk($model->company_id);
// echo "<pre>";print_r($model);exit;
$bill='';
$bill_no='Nil';
if(!empty($model->bill_id)){
	
	$bill_no=$model->bill_id;
}
?>

<div class="container">
<div class="loading-overlay">
        <span class="fa fa-spinner fa-3x fa-spin"></span>
  </div>
    
	<div class="expenses-heading">
                <div class="clearfix">
                  <!-- remove addentries class -->
                  <button
                    type="button"
                    class="btn btn-info pull-right mt-0 mb-10 dailyexpense_btn"
                    href="<?php echo $this->createAbsoluteUrl('dailyexpense/expenses') ?>"
                    id="dailyformbutton"
                  >Daily Expense </button>
                  <h3>Daily Expense Details</h3>
                  <div id="loading" style="display: none"><img src="/themes/jpvcms/images/ajax-loader.gif" /></div>
                </div>
    </div>
   
 
 <!--PO Items model-->
    <div class="entries-wrapper">
		<div class="row">
        <div class="col-xs-12">
        <div class="heading-title">Daily Expense ID#<?php echo $model->dailyexp_id; ?></div>
        <div class="dotted-line"></div>
        </div>
    </div>
        <div class="row">
            <div class="form-group col-xs-12 col-md-3">
                <div>
                    <label>Company:</label>
                    <div><?php  echo $companyInfo->name ?></div>								
                </div>
            </div>

            <div class="form-group col-xs-12 col-md-3">
                
                    <label>Bill No:</label>
                    <div><?php  echo $bill_no ; ?></div>								
                
            </div>

            <div class="form-group col-xs-12 col-md-3">
               
                    <label>Transaction Head:</label>
                    <div>
                        <?php 
							$expense_head = '';
							$exp_head= ExpenseType::Model()->findByPk($model->expensehead_id);
							if(!empty($exp_head)){
								$expense_head=$exp_head->type_name;
							}
                            echo $expense_head;
                        ?>
                    </div>
               
            </div>
            <div class="form-group col-xs-12 col-md-3">
               
                    <label>Transaction Type:</label>
                    <div id="po_bill">
						<?php 
							$expense_type = '';
							
							if($model->expense_type == '103'){
								$expense_type="Petty Cash Payment";
							}
							if($model->expense_type == '88'){
								$expense_type="Cheque/Online Payment";
							}
							if($model->expense_type == '89'){
								$expense_type="Cash";
							}
                            echo $expense_type;
                        ?>
                       
                       
                    </div>
               
            </div> 
			 <div class="form-group col-xs-12 col-md-3">
               
                    <label>Amount Paid:</label>
                    <div>
                        <?php 
							echo ($model->dailyexpense_paidamount != '') ? Controller::money_format_inr($model->dailyexpense_paidamount, 2) : '0.00'; 
                        ?>
                    </div>
                
            </div>
			 <div class="form-group col-xs-12 col-md-3">
               
                    <label>Petty Cash User</label>
                    <div>
                       <?php 
							$pettyname = '';
							$petty_head= Users::Model()->findByPk($model->employee_id);
							if(!empty($petty_head)){
								$pettyname= $petty_head->first_name.' '.$petty_head->last_name;
							}
                            echo $pettyname;
                        ?>
                      
                    </div>
                
            </div>
           

            <div class="form-group col-xs-12 col-md-3">
               
                    <label>Created By:</label>
                    <div>
                       <?php 
							$username = '';
							$user_head= Users::Model()->findByPk($model->user_id);
							if(!empty($user_head)){
								$username= $user_head->first_name.' '.$user_head->last_name;
							}
                            echo $username;
                        ?>
                      
                    </div>
                
            </div>

             <div class="form-group col-xs-12 col-md-3">
               
                    <label>  Date:</label>
                    <div>
                    <?php echo  date('d-m-Y', strtotime($model->date)); ?>
                    </div>
                
            </div>
        </div><br>
       

    </div>    

   
   

</div>
</div>
 
 


<style>
   
</style>
<script>
    
     function reloadParent() {
            location.reload();
    }
    $(document).ready(function() {
        $('#poModal .clsbtn').on('click', function () {
           
            console.log('Modal is now hidden');
            window.parent.location.reload();
        });
    $('#addPoButton,#editPoButton').click(function() {
        var mrId = $("#mrId").val();
        var baseUrl = '<?php echo Yii::app()->baseUrl; ?>';
        var url = baseUrl + '/index.php?r=materialRequisition/loadPoModal&mrId=' + mrId;
        $('.loading-overlay').addClass('is-active');
        // Perform AJAX request
        $.get(url, function(data) {
            // Load response into modal body
            $('#poModal .modal-body').html(data);
            // Show modal
            $('#poModal').modal('show');
            // Initialize select2 after the modal is shown and the content is loaded
            $('#poModal .select2').select2({
                dropdownParent: $("#poModal")
            });
            $('.loading-overlay').removeClass('is-active');
        }).fail(function() {
            $('#poModal .modal-body').html('<p>Error loading information</p>');
        });
    });

    $('#billModal .clsbtn').on('click', function () {
           
           console.log('Modal is now hidden');
           window.parent.location.reload();
       });
   $('#addBillButton,#editBillButton').click(function() {
       var mrId = $("#mrId").val();
       var baseUrl = '<?php echo Yii::app()->baseUrl; ?>';
       var url = baseUrl + '/index.php?r=bills/loadbillModal&mrId=' + mrId;
       $('.loading-overlay').addClass('is-active');
        $.get(url, function(data) {
          
          $('#billModal').modal('show');
           $('#billModal .modal-body').html(data);
           
           
            $('#billModal .select2').select2({
               dropdownParent: $("#billModal")
           });
           $('.loading-overlay').removeClass('is-active');

       }).fail(function() {
           $('#billModal .modal-body').html('<p>Error loading information</p>');
       });
   });


          

    });
	$(".dailyexpense_btn").click(function(){
        var redirecturl = $(this).attr("href");
        window.location.href=redirecturl;
    });
   
        
</script>