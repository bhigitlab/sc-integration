<?php
/* @var $this WithdrawalsController */
/* @var $model Withdrawals */

$this->breadcrumbs=array(
	'Withdrawals'=>array('index'),
	$model->exp_id,
);

$this->menu=array(
	array('label'=>'List Withdrawals', 'url'=>array('index')),
	array('label'=>'Create Withdrawals', 'url'=>array('create')),
	array('label'=>'Update Withdrawals', 'url'=>array('update', 'id'=>$model->exp_id)),
	array('label'=>'Delete Withdrawals', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->exp_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Withdrawals', 'url'=>array('admin')),
);
?>

<h1>View Withdrawals #<?php echo $model->exp_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'exp_id',
		'projectid',
		'userid',
		'date',
		'amount',
		'description',
		'type',
		'created_by',
		'created_date',
		'updated_by',
		'updated_date',
	),
)); ?>
