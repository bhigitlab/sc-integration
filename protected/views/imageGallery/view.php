<?php
/* @var $this ImageGalleryController */
/* @var $model ImageGallery */

?>
<div class="container">
    <h4>View : <?php  echo isset($data->projectid) ? ($model->project->name." -- ".$model->image) : ""; ?> </h4>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="list_item"><span>Project :</span><?php echo isset($data->projectid) ? $model->project->name : ""; ?></div>
        <br>
        <div class="list_item">
            <div style="width: 400px; height: 350px;" class="fileupload-new thumbnail"><img src="<?php echo Yii::app()->request->baseUrl . '/uploads/image/' . $model->image; ?>" alt="" /></div>  
        </div>
        <div class="list_item">
            <?php echo $model->image; ?>
        </div>
    </div>

</div>
</div>
