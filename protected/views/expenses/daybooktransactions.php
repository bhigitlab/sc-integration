<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<?php
    $company_address = Company::model()->findBypk(($company_id)?$company_id:Yii::app()->user->company_id);	
?>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/pdfstyle.css">
<h3 class="text-center">Day Book Transactions</h3>
<table border="1" style="width:100%;border:1px solid gray;margin:0px 30px;">

    <thead>
        <tr>
          
             <th>Sl No.</th>
            <th>Date</th>
            <th>Project Name</th>
            <th>Company</th>
            <th>Vendor</th>
            <th>Subcontractor</th>
            <th>Description</th>
            <th>Transaction Type</th>
            <th>Purchase Type</th>

            <th>Credit Amount</th>
            <th>Debit Amount</th>
            <th>Paid Amount</th>
            
            
   
        </tr>   
    </thead>
    <tbody>
        
       
        <?php
        $i = 0;
        if ($model == NULL) {
            echo '<tr><td colspan="2">No records Found</td></tr>';
        } else { ?>          
            <tr>
        <td class="text-right" colspan="8"><b>Grand Total : </b></td>
        <td class="text-right"></td>
        <td class="text-right"><?php echo Controller::money_format_inr(Yii::app()->user->grandtotalcredit[0]['sumreceived'],2,1); ?></td>
        <td class="text-right"><?php echo Controller::money_format_inr(Yii::app()->user->grandtotaldebit[0]['sumamt'],2,1); ?></td>
        <td class="text-right"><?php echo Controller::money_format_inr(Yii::app()->user->grandtotaldebit[0]['sumpaid'],2,1); ?></td>
        <td>
    </tr>
            
            <?php
           foreach ($model as $data) {
                $i++;
                ?>
         <?php
$id = $data['exp_id'];
$type = $data['type'];
$tr_type = "";

if ($type == 72) { //receipt
    $ptype = $data['payment_type'];
    $sql = Status::model()->find(array(
        'select' => array('caption'),
        "condition" => "sid='$ptype'",
    ));
    $tr_type = $sql['caption'];
} else { // expense
    $etype = $data['expensetype_id'];
    $sql = ExpenseType::model()->find(array(
        'select' => array('type_name'),
        "condition" => "type_id='$etype'",
    ));
    
    $ptype = $data['expense_type'];
    $sql = Status::model()->find(array(
        'select' => array('caption'),
        "condition" => "sid='$ptype'",
    ));
    
    $tr_type = $sql['caption'];
}
?>

     <tr>
       
        <td class="text-right"  ><?php echo $i; ?></td>

        <td style="width:80px;"><?php
            if ($data['date'] != "") {
                echo date("d-M-Y", strtotime($data['date']));
            }
            ?></td>

        <td><?php echo $data['project_name']; ?></td> 
        
        <td>
            <?php
            $company = Company::model()->findByPk($data['companyid']);
            echo $company->name;
            ?>
        </td>
        
        <td><?php
            if ($data['type'] == 73) {
                echo $data['vendor_name'];
            } else {
                echo "---";
            };
            ?></td>
        <td>
        <?php
        if(!empty($data['subcontractor_id'])) {
            $scpayment = SubcontractorPayment::model()->findByPk($data['subcontractor_id']);
            if($scpayment)
                echo $scpayment->subcontractor->subcontractor_name;
        }
        ?>
        <td><?php echo $data['description']; ?></td>

        <td><?php echo $tr_type; ?></td>


        <td class="text-right">
            <?php if ($data['type'] == 73) { ?> <?php
                 if ($data['purchase_type'] == 2) {
                    echo 'Full Paid';
                } elseif ($data['purchase_type'] == 1) {
                    echo 'Credit';
                } elseif ($data['purchase_type'] == 3) {
                    echo 'Partially Paid';
                } else {
                    echo '';
                }
             } else {
                if ($data['purchase_type'] == 2) {
                    echo 'Full Paid';
                } elseif ($data['purchase_type'] == 1) {
                    echo 'Credit';
                } elseif ($data['purchase_type'] == 3) {
                    echo 'Partially Paid';
                } else {
                    echo '';
                }
            } ?>
        </td>
        <td class="text-right">
        <?php if ($data['type'] == 72) { 
              if ($data['purchase_type'] == 1) {?>
                
                <?php echo ($data['amount'] ? Controller::money_format_inr($data['amount'], 2, 1) : 0); ?>
               

           <?php }else{?>
           
                <?php echo ($data['receipt'] ? Controller::money_format_inr($data['receipt'], 2, 1) : 0); ?>
           

          <?php   }
        } 
        ?>
        </td>
        <td>
        <?php if ($data['type'] == 73) { 
            if ($data['purchase_type'] == 1) {
            ?>
            <?php echo ($data['amount'] ? Controller::money_format_inr($data['amount'], 2,1) : 0); ?>
            

           <?php }else{?>
           
                <?php echo ($data['paid'] ? Controller::money_format_inr($data['paid'], 2, 1) : 0); ?>
           
            <?php
            } } 
        ?>
        </td>
        <td class="text-right"><?php
            if ($data['type'] == 73) {
                echo ($data['paid'] ? Controller::money_format_inr($data['paid'], 2) : 0);
            }
            ?></td>

        
</tr> 
                <?php
            }
        }
        ?>
    </tbody>
</table>

