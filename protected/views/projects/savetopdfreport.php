<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/pdfstyle.css">

<?php
    $company_address = Company::model()->findBypk(Yii::app()->user->company_id);	
?>
    
    <div class="container-fluid" style="margin:0px 30px;">
        <br>
        <h3 class="text-center mt-3">Project Report</h3>
        <?php
        $this->widget('zii.widgets.CListView', array(
            'dataProvider' => $model->search(),
            'itemView' => '_newexpenselistview', 'template' => '<table class="table table-bordered table-striped" border="1">{items}</table>',
        ));
        ?>
    </div>
