ALTER TABLE `jp_expense_type` CHANGE `wage_label` `wage_label` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;
ALTER TABLE `jp_expense_type` CHANGE `wage_rate_label` `wage_rate_label` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;
ALTER TABLE `jp_expense_type` CHANGE `helper_label` `helper_label` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;
ALTER TABLE `jp_expense_type` CHANGE `helper_labour_label` `helper_labour_label` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;
ALTER TABLE `jp_expense_type` CHANGE `lump_sum_label` `lump_sum_label` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;
ALTER TABLE `jp_expense_type` CHANGE `labour_wage_label` `labour_wage_label` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;
ALTER TABLE `jp_expense_type` CHANGE `helper_wage_label` `helper_wage_label` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;
ALTER TABLE `jp_expense_type` ADD `expense_category` ENUM('1','2') NULL DEFAULT NULL COMMENT '1=>Direct Expense\r\n2=>Indirect Expense' AFTER `labour_wage_status`;
ALTER TABLE `jp_expense_type` CHANGE `expense_category` `expense_category` ENUM('1','2') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL COMMENT '1=>Direct Expense\r\n2=>Indirect Expense';