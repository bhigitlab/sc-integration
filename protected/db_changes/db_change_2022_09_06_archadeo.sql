SET @rank=0;
UPDATE jp_quotation SET `inv_no` = CONCAT('22/ARCH/SQ-', LPAD(@rank:=@rank+1, 3, '0')); 
SET @rank=0;
UPDATE jp_scquotation SET `scquotation_no` = CONCAT('22/ARCH/SCQ-', LPAD(@rank:=@rank+1, 3, '0'));
SET @rank=0;
UPDATE jp_invoice SET `inv_no` = CONCAT('INV-22/23-', LPAD(@rank:=@rank+1, 3, '0'));
