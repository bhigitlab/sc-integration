ALTER TABLE `jp_sales_quotation` CHANGE `mrp` `mrp` FLOAT(15,2) NOT NULL, CHANGE `amount_after_discount` `amount_after_discount` FLOAT(15,2) NOT NULL;

ALTER TABLE `jp_quotation_revision` CHANGE `total_after_additionl_discount` `total_after_additionl_discount` FLOAT(15,2) NULL DEFAULT NULL;