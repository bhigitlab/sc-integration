<?php
$tblpx= Yii::app()->db->tablePrefix;
$company = Yii::app()->user->company_id;
$companyInfo = Company::model()->findByPK($company);
$companyName = $companyInfo["name"];
$companyGst = $companyInfo["company_gstnum"];
$item_model = WarehousedespatchItems::model()->findAll(array("condition" => "warehousedespatch_id = '$model->warehousedespatch_id'"));
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Despatch Pdf</title>
		<link href="https://fonts.googleapis.com/css?family=Diplomata+SC|Limelight" rel="stylesheet">
		<style>
			.container{max-width: 850px;margin:0 auto;}
			table{width: 100%;border-collapse:collapse;font-size: 12px;}
			table td, table th{padding: 8px 0px;}
			.item_table td, .item_table th{border: 1px solid #222;padding: 8px;text-align:center;}
		</style>
	</head>
	<body>

		<div class="container">
			<table>
				<tr>
					<td>GSTIN:<b> <?php echo $companyGst; ?></b></td>
<!--					<td><b>4/SAP/STR/F/353</b></td>-->
					<td style="text-align:right">Phone:<b> <?php echo $companyInfo['phone']; ?></b></td>
				</tr>
			</table>
			<div style="text-transform:uppercase;text-align:center;">
				<h2 style="margin: 5px;font-family: 'Limelight', cursive;"><?php echo $companyName; ?></h2>
				<h4 style="margin:0px;"><?php //echo $companyInfo['address']; ?><?php  echo $warehouse_address;?></h4>
			</div>
			<table>
				<tr>
					<td>NO.<b style="color: red;font-size: 18px;"> <?php echo $model->warehousedespatch_no ?></b></td>
					<td style="text-align:right">
						Date:<b> <?php echo date("d-m-Y", strtotime($model->warehousedespatch_date)); ?></b><br/> 	<?php  if($model->warehousedespatch_through!="" ) { ?>Through:<b><?php echo $model->warehousedespatch_through ?> </b><?php } ?>
					</td>
				</tr>
                                <tr><td colspan="2"><h4 style="text-transform:uppercase;">Despatch Note:</h4><br>Stock of Goods despatched to: <b><?php echo $model->warehousedespatch_vendor; ?></b></td></tr>
			</table>
			<table class="item_table">
				<tr>
					<th style="width: 40px;">Sl No</th>
					<th>Description</th>
					<th style="width: 100px;">Dimension</th>
					<th style="width: 100px;">Batch</th>
					<th style="width: 100px;">Unit/Size</th>
					<th style="width: 100px;">No of pieces</th>
					<th style="width: 150px;">Remarks</th>
				</tr>
                                <?php
                                if(!empty($item_model)){
                                    foreach($item_model as $key=> $values){
                                        $specification = Yii::app()->db->createCommand("SELECT id, parent_id,brand_id, specification, unit FROM {$tblpx}purchase_category WHERE id=" . $values['warehousedespatch_itemid'] . "")->queryRow();
                                        $parent_category = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}purchase_category WHERE id='" . $specification['parent_id'] . "'")->queryRow();

                                        if ($specification['brand_id'] != NULL) {
                                            $brand_details = Yii::app()->db->createCommand("SELECT brand_name FROM {$tblpx}brand WHERE id=" . $specification['brand_id'] . "")->queryRow();
                                            $brand = '-' . $brand_details['brand_name'];
                                        } else {
                                            $brand = '';
                                        }
                                        $spc_details = $parent_category['category_name'] . $brand . ' - ' . $specification['specification'];
                                ?>
				<tr>
					<td><?php echo $key+1; ?></td>
					<td style="text-align:justify;"><?php echo $spc_details; ?></td>
					<td><?php echo ($values['warehousedespatch_batch'] !='')?$values['warehousedespatch_batch']:"N/A";?></td>
					<td><?php echo $values['warehousedespatch_batch'];?></td>
					<td><?php echo $values['warehousedespatch_unit'];?></td>
					<td><?php echo $values['warehousedespatch_quantity'];?></td>
					<td><?php echo $values['warehousedespatch_remarks'];?></td>
				</tr>
                                <?php
                                }
                                ?>
                                <?php } else{
                                   ?>
                                <tr>
                                    <td colspan="5">No Item found</td>
                                </tr>
                                <?php
                                } ?>
                                <tr>
                                    <td colspan="2" style="padding-left:0px;text-align:left;border:none;">Dispatch Clerk <b><?php echo $model->clerk->first_name.' '.$model->clerk->last_name ?></b></td>
					<td colspan="5" style="text-align:right;padding-right:0px;border:none;">For <b style="text-transform:uppercase;"><?php echo $companyName; ?></b></td>
				</tr>
			</table>

		</div>

	</body>
</html>
