<?php if (!empty($equipmentsItems)):?>
    <div id="dynamic-equipment-rows">
        <?php foreach ($equipmentsItems as $index => $detail): ?>
            

            <div class="row dynamic-row">

                <!-- Equipment Dropdown -->
                <div class="form-group col-xs-12 col-md-2">
                    <label>Equipment</label>
                    <select name=" EquipmentsItems[equipments][<?php echo $index; ?>][equipments]" class="equipments form-control select2">
                        <option value="" selected disabled>Select Equipment</option>
                        <?php foreach ($equipment as $id => $equipmentName){ ?>
                            <option value="<?php echo CHtml::encode($id); ?>" <?php echo (isset($detail) && $detail->equipments == $id) ? 'selected' : ''; ?>>
                                <?php echo CHtml::encode($equipmentName); ?>
                            </option>
                        <?php } ?>
                    </select>
                </div>
                <div class="form-group col-xs-12 col-md-2">
                    <label>Unit</label>
                    <?php
                    $unitData = CHtml::listData(Unit::model()->findAll(), 'id', 'unit_name');
                    $selectedUnit = isset($detail->unit) ? $detail->unit : ''; // Use $detail->unit instead of $model->unit
                    ?>
                    <select name=" EquipmentsItems[equipments][<?php echo $index; ?>][unit]" class="form-control">
                        <option value="">-- Select Unit --</option>
                        <?php foreach ($unitData as $id => $unit_name): ?>
                            <option value="<?php echo CHtml::encode($id); ?>" 
                                <?php echo ($id == $selectedUnit) ? 'selected' : ''; ?>>
                                <?php echo CHtml::encode($unit_name); ?>
                            </option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="form-group col-xs-12 col-md-2">
                    <label>Quantity</label>
                    <input type="number" class="equipment_qty form-control" 
                        name=" EquipmentsItems[equipments][<?php echo $index; ?>][quantity]" 
                        min="0" 
                        value="<?php echo CHtml::encode($detail->quantity); ?>" 
                        onchange="calculateAmount(<?php echo $index; ?>)">
                        <span class="error_count"></span>
                </div>
                <!-- Equipment Rate Dropdown -->
                <div class="form-group col-xs-12 col-md-2">
                    <label>Rate</label>
                    <input type="number" class="equipment_rate form-control" 
                        name=" EquipmentsItems[equipments][<?php echo $index; ?>][rate]" 
                        min="0" 
                        value="<?php echo CHtml::encode($detail->rate); ?>" 
                        onchange="calculateAmount(<?php echo $index; ?>)">
                </div>

                <!-- Equipment Quantity Field -->
                

                <!-- Equipment Amount Field -->
                <div class="form-group col-xs-12 col-md-2">
                    <label>Amount</label>
                    <input type="number" class="equipment_amount form-control" 
                        name=" EquipmentsItems[equipments][<?php echo $index; ?>][amount]" 
                        min="0" 
                        value="<?php echo CHtml::encode($detail->amount); ?>" 
                        readonly>
                </div>

                <input type="hidden" name="EquipmentsItems[equipments][<?php echo $index; ?>][id]" value="<?php echo CHtml::encode($detail->id); ?>">

                <div class="col-xs-12 col-md-2">
                    <button type="button" class="add-btn btn btn-info btn-sm">+</button>
                    <button type="button" class="remove-btn btn btn-danger btn-sm">-</button>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
<?php endif; ?>
