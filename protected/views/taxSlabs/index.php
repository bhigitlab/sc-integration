<?php
/* @var $this TaxSlabsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
	'Tax Slabs',
);

$this->menu = array(
	array('label' => 'Create TaxSlabs', 'url' => array('create')),
	array('label' => 'Manage TaxSlabs', 'url' => array('admin')),
);
?>

<h1>Tax Slabs</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider' => $dataProvider,
	'itemView' => '_view',
)); ?>