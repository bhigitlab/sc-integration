<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/purchase.css">
<?php

$company = Company::model()->findBypk($model->company_id);
?>
<style>
    table {
        border-collapse: collapse;
        width: 100%;
        font-family: Arial, Helvetica, sans-serif;

    }

    .details,
    .info-table {
        border: 1px solid #ccc;
        padding: 2rem;
    }

    .details td,
    .details th,
    .info-table td,
    .info-table th {
        padding: 5px 10px;
        font-size: .85rem;
    }

    .info-table {
        margin-bottom: 10px;
    }

    .text-center {
        text-align: center;
    }

    .img-hold {
        width: 10%;
    }

    .companyhead {
        font-size: 1rem;
        font-weight: bold;
        margin-top: 0px;
        color: #789AD1;
    }

    .table-header th,
    .details {

        font-size: .75rem;
        padding-left: 4px;
        padding-right: 4px;
    }

    .medium-font {
        font-size: .85rem;
    }

    .addrow th,
    .addrow td {
        font-size: .75rem;
        border: 1px solid #333;
        border-collapse: collapse;
        padding-left: 4px;
        padding-right: 4px;
    }

    .text-right {
        text-align: right;
    }

    .w-50 {
        width: 50%;
    }

    .text-sm {
        font-size: 11px;
    }

    .dotted-border-bottom {
        border-bottom: 2px dotted black;
    }

    .w-100 {
        width: 100%;
    }

    .border-right {
        border-right: 1px solid #ccc;
    }

    .border-0 {
        border-right: 1px solid #fff !important;
    }

    .header-border {
        border-bottom: 1px solid #000;
    }
</style>

<div class="container">
    <table border=0>
        <tbody>
            <tr>
                <th>&nbsp;</th>
                <th class="text-center">
                    <h3>PURCHASE ORDER</h3>
                </th>
            </tr>
            <tr></tr>
        </tbody>
    </table>

    <table class="details">
        <tbody>
            <tr>
                <td class="border-bottom" colspan="2">
                    <b>PURCHASE NO : <?php echo isset($model->purchase_no) ? $model->purchase_no : ''; ?></b>
                </td>
                <td class="text-right border-bottom">
                    <p>Date:<?php echo date("d-m-Y", strtotime($model->purchase_date)); ?></p>
                </td>
            </tr>
            <tr>
                <td class="border-right">
                    <b><?php echo isset($vendor->name) ? $vendor->name : '' ?></b>
                    <p><?php echo (isset($vendor->address)) ? $vendor->address : '';  ?></p>
                    <p>Phone: <?php echo isset($vendor->phone) ? $vendor->phone : ''; ?></p>
                    <p><b>GST NO :<?php echo (isset($vendor->gst_no)) ? $vendor->gst_no : ''; ?><b></p>
                </td>
                <td class="border-right">
                    <b class="header-border">Delivery Address</b><br><br>
                    <p><?php echo isset($model->shipping_address) ? nl2br($model->shipping_address) : '-'; ?></p>
                    <p>PH:<?php echo isset($model->contact_no) ? $model->contact_no : '-'; ?> </p>
                </td>
                <td>

                    <p>PROJECT:<?php echo isset($project->name) ? $project->name : '' ?></p>
                    <p>EXPENSE HEAD :<?php echo (isset($expense_head->type_name)) ? $expense_head->type_name : ''; ?></p>
                </td>
            </tr>

        </tbody>
    </table>
</div>
<div class="content">
    <div class="wrapper">
        <div class="holder">
            <table class="table pdf-table">
                <thead>
                    <tr>
                        <th>SI</th>
                        <th>Description of Goods</th>
                        <th width="20px">HSN Code</th>
                        <th>Quantity</th>
                        <th>Unit Price</th>
                        <th width="30px">Taxable Amount</th>
                        <th width="30px;">GST (%)</th>
                        <th width="20px">GST Amount</th>
                        <th>Total</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $total_amount = 0;
                    $cgst_sum = $sgst_sum = 0;
                    if (!empty($itemmodel)) {
                        $i = 1;
                        $discount_total = $tax_total_amount = $quantity_total = 0;
                        foreach ($itemmodel as $value) {
                    ?>
                            <tr>
                                <td colspan="9" class="categ_td border-bottom"><?php echo !empty($value['category_title']) ? $value['category_title'] : '' ?></td>
                            </tr>
                            <?php

                            foreach ($value as $item) {
                                if (isset($item['specification'])) {
                                    $discount_total += $item['disc'];
                                    $tax_total_amount += $item['tot_tax'];
                                    $amount_ = ($item['quantity'] * $item['rate']) - $item['disc'];
                                    if ($item['amount'] != $amount_) {
                                        $item['amount'] = $amount_;
                                    }
                                    $total_tax_perc = $item['sgstp'] + $item['cgstp'] + $item['igstp'];
                                    $amount_total = $item['amount'] + $item['tot_tax'];
                                    $quantity_total += $item['quantity'];
                                    if (!empty($item['cgst']))
                                        $cgst_sum += $item['cgst'];
                                    if (!empty($item['sgst']))
                                        $sgst_sum += $item['sgst'];
                            ?>
                                    <tr>
                                        <td><?php echo $i; ?></td>
                                        <td><?php echo $item['specification']; ?> </td>
                                        <td><?php echo $item['hsn_code']; ?></td>
                                        <td><?php echo $item['quantity'] . ' ' . $item['unit']; ?></td>
                                        <td class="text-right"><?php echo Yii::app()->Controller->money_format_inr($item['rate'], 2, 1); ?></td>
                                        <td class="text-right"><?php echo Yii::app()->Controller->money_format_inr($item['amount'], 2, 1); ?></td>
                                        <td><?php echo $total_tax_perc; ?></td>
                                        <td class="text-right"><?php echo Yii::app()->Controller->money_format_inr($item['tot_tax'], 2, 1); ?></td>
                                        <td class="text-left"><?php echo Yii::app()->Controller->money_format_inr($amount_total, 2, 1); ?></td>
                                    </tr>
                            <?php
                                    $i++;
                                }
                            }
                            ?>
                    <?php
                        }
                    }
                    ?>
                </tbody>
            </table>
        </div>
        <table class="table foot-table w-100">
            <tbody>
                <tr>
                    <?php
                    if ($model->inclusive_gst == 1) {
                    ?>
                        <td width="82%">*Inclusive of GST</td>

                    <?php
                    }
                    ?>

                    <td class="text-right"><b>Total:</b></td>
                    <td class="text-right"><b><?php echo isset($model->total_amount) ? Yii::app()->Controller->money_format_inr((($model->total_amount + $tax_total_amount) - $discount_total), 2, 1) : '' ?></b>&nbsp;&nbsp;</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<table class="table details border-top-0">
    <tbody>
        <tr>
            <td colspan="9" class="text-right">
                <p class="text-capitalize"><?php echo Yii::app()->Controller->displaywords((($model->total_amount + $tax_total_amount) - $discount_total)) ?></p>
            </td>
        </tr>
        <tr>
            <th>
                <label>Taxable Amount:</label>
                <?php echo isset($model->total_amount) ? Yii::app()->Controller->money_format_inr($model->total_amount, 2, 1) : '' ?>
            </th>
            <th></th>
            <th>
                <label>CGST:</label>
                <?php echo  Yii::app()->Controller->money_format_inr($cgst_sum, 2, 1); ?>
            </th>
            <th></th>
            <th>
                <label>SGST:</label>
                <?php echo  Yii::app()->Controller->money_format_inr($sgst_sum, 2, 1); ?>
            </th>
            <th></th>
            <th></th>
            <th class="text-right">
                <h4>Total Amount : <?php echo isset($model->total_amount) ? Yii::app()->Controller->money_format_inr((($model->total_amount + $tax_total_amount) - $discount_total), 2, 1) : '' ?></h4>
            </th>
        </tr>
    </tbody>
</table>
<table class="mt-2 w-100">
    <tbody>
        <tr>
            <td width="50%" class="valign-top">
                <div class="text-sm note-list">
                    <?php echo $model->purchase_description; ?>
                </div>
            </td>
            <td class="text-right valign-top">
                <div class="text-md text-right"><b>Authorized Signatory,</b></div> <br> <br><br>
                <div class="text-md text-right"><b>For <?php echo Yii::app()->name; ?></b></div>
            </td>
        </tr>
    </tbody>
</table>