<?php
$arrVal='';
$sql="";
if($type == 1){
  $sql="";
  if(!empty($template_id) && !empty($scquotation_id)){
    $sql = "SELECT DISTINCT
            t.worktype_id as labour_id,
            t.worktype_name,
            t.template_id,
            COALESCE(sq.labour_rate, t.worktype_rate) AS labour_rate
        FROM jp_labour_template_worktype t
        LEFT JOIN ( 
            SELECT id, labour_id, template_id, labour_rate
            FROM jp_sc_quot_labour_template 
            WHERE sc_quot_id = $scquotation_id
              AND template_id = $template_id
        ) sq
        ON sq.template_id = t.template_id 
           AND sq.labour_id = t.worktype_id
        WHERE t.template_id = $template_id";
       // die($sql);

  $arrVal = Yii::app()->db->createCommand($sql)->queryAll();
  } 
  if($arrVal){ 
  ?>
  <p style="text-align:center">**The below details shown as per sc quotation no</p>
 <table class="table" style="margin-bottom:0px;">
            <thead class="entry-table">
            <tr>
                <th>Labour label</th>
                <th>Labour Rate</th>
                </tr>
            </thead>
            <tbody>                    
                <?php
    foreach ($arrVal as $val) {
    
        $labour_data = Yii::app()->db->createCommand("SELECT * FROM `jp_labour_worktype` WHERE `type_id` = " . $val['labour_id'])->queryRow();
        ?>
        <tr>
                <td><?php echo $labour_data['worktype']; ?></td>
                <td class="text-right"><?php echo Controller::money_format_inr($val['labour_rate'],2); ?></td>
    </tr>
            
        <?php
    } ?>
    </tbody>
        </table>
        <?php
 } 
}elseif($type ==2){ 
 
  if(!empty($template_id) && !empty($scquotation_id)){
   
    $sql="";
    $sql="SELECT DISTINCT
    wk.worktype_id as labour_id,
    wk.worktype_name,
    wk.template_id,
    COALESCE(pl.labour_rate, wk.worktype_rate) AS labour_rate
FROM 
    jp_labour_template_worktype wk
LEFT JOIN 
    (SELECT labour_id, template_id, labour_rate -- Subquery without aggregation to get labour_rate for each worktype_id
     FROM jp_project_labour 
     WHERE project_id = " . $project . " AND template_id=" . $template_id . ") pl
ON 
    pl.template_id = wk.template_id 
    AND pl.labour_id = wk.worktype_id -- Join on both template_id and worktype_id
WHERE 
    wk.template_id = " . $template_id . ";";
    $arrVal = Yii::app()->db->createCommand($sql)->queryAll();
    } 
    
    if($arrVal){ 
    ?>
    <p style="text-align:center">**The below details are shown as per template selected in project</p>
   <table class="table" style="margin-bottom:0px;">
              <thead>
              <tr>
                  <th>Labour label</th>
                  <th>Labour Rate</th>
                  </tr>
              </thead>
              <tbody>                    
                  <?php
      foreach ($arrVal as $val) {
      
        $labour_data = Yii::app()->db->createCommand("SELECT * FROM `jp_labour_worktype` WHERE `type_id` = " . $val['labour_id'])->queryRow();
      ?>
      <tr>
        <td><?php echo $labour_data['worktype']; ?></td>
        <td class="text-right"><?php echo Controller::money_format_inr($val['labour_rate'],2); ?></td>
      </tr>
              
          <?php
      } ?>
      </tbody>
      </table>
    <?php
  }
}
?>