<style>
    .dataTables_filter {
        display: none;
    }

    #spectable {
        display: none;
    }
</style>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.dataTables.min.js"></script>
<script type="text/javascript"
    src="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/validation/jquery.validate.js"></script>
<script type="text/javascript"
    src="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/validation/additional-methods.js"></script>

<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.js"></script>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/jquery.dataTables.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.dataTables.min.js"></script>



<?php
$user = Users::model()->findByPk(Yii::app()->user->id);
$arrVal = explode(',', $user->company_id);
$newQuery1 = "";
foreach ($arrVal as $arr) {
    if ($newQuery1)
        $newQuery1 .= ' OR';
    $newQuery1 .= " FIND_IN_SET('" . $arr . "', company_id)";
}
function fetchCategoryTree($parent = 'NULL', $spacing = '', $user_tree_array = '')
{
    $user = Users::model()->findByPk(Yii::app()->user->id);
    $arrVal = explode(',', $user->company_id);
    $newQuery1 = "";
    $condition = "parent_id IS NULL";
    foreach ($arrVal as $arr) {
        if ($newQuery1)
            $newQuery1 .= ' OR';
        $newQuery1 .= " FIND_IN_SET('" . $arr . "', company_id)";
    }
    if (!is_array($user_tree_array))
        $user_tree_array = array();
    $tblpx = Yii::app()->db->tablePrefix;


    if ($parent != 'NULL') {
        $condition = "parent_id='" . $parent . "'";
    }

    $category = Yii::app()->db->createCommand("SELECT id , category_name "
        . "FROM {$tblpx}purchase_category "
        . "WHERE " . $condition)->queryAll();
    foreach ($category as $key => $value) {
        $user_tree_array[] = array("id" => $value['id'], "name" => $spacing . $value['category_name']);
        $user_tree_array = fetchCategoryTree($value['id'], $spacing . '&nbsp;&nbsp; -', $user_tree_array);
    }

    return $user_tree_array;
}

$categoryList = fetchCategoryTree();
$tblpx = Yii::app()->db->tablePrefix;
$unit = Yii::app()->db->createCommand("SELECT id , unit_name FROM {$tblpx}unit")->queryAll();
$brand = Yii::app()->db->createCommand("SELECT id , brand_name FROM {$tblpx}brand WHERE (" . $newQuery1 . ")")->queryAll();

$user = Users::model()->findByPk(Yii::app()->user->id);
$arrVal = explode(',', $user->company_id);
$newQuery = "";
foreach ($arrVal as $arr) {
    if ($newQuery)
        $newQuery .= ' OR';
    $newQuery .= " FIND_IN_SET('" . $arr . "', id)";
}
$company = Company::model()->findAll(array('condition' => $newQuery));


$this->breadcrumbs = array(
    'Purchase List',
)
    ?>
<div class="container custom-form-style" id="project">

    <div class="loading-overlay">
        <span class="fa fa-spinner fa-3x fa-spin"></span>
    </div>
    <?php // $this->renderPartial('_search', array('model' => $model)) 
    ?>
    <div class="add-btn">

    </div>
    <div class="dpdwntab">
        <div class="expenses-heading header-container">
            <h2>Purchase Items</h2>
            <ul class="nav nav-pills btn-container" id="myTab">
                <li class="active"><a data-toggle="tab" href="#viewitem" class="btnadd table-view">Category List</a>
                </li>
                <li><a data-toggle="tab" href="#viewsubcatitem" class="btnadd table-view">Sub Category List</a></li>
                <li><a data-toggle="tab" href="#viewbrand" class="btnadd table-view">Brand List</a></li>
                <li><a data-toggle="tab" href="#viewspec" class="btnadd table-view">Specification List</a></li>
                <li><a data-toggle="tab" href="#viewunit" class="btnadd table-view">Unit Convertion</a></li>
                <li><a data-toggle="tab" href="#importItem" class="btnadd table-view">Import Purchase Item</a></li>
            </ul>
        </div>
        <div id="errMsg"></div>
        <?php
        foreach (Yii::app()->user->getFlashes() as $key => $message) {
            echo '<div class="flash-' . $key . ' alert alert-success flash_msg" style=" width:335px;"> <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>' . $message . "</div>\n";
        }
        ?>

        <div class="tab-content">
            <div id="viewitem" class="tab-pane in active">
                <div class="entries-wrapper ">
                    <div class="row">
                        <div class="col-xs-12 sub-heading">
                            <div class="heading-title">Category List</div>
                            <a class="btn btn-info btn-sm catadd">Add Category</a>
                        </div>
                    </div>
                    <div class="padding-bottom-5" id="category_body" style="display:none;">
                        <span class="error_message"></span>
                        <?php
                        if (isset(Yii::app()->user->role) && (in_array('/purchaseCategory/manageitem', Yii::app()->user->menuauthlist))) {
                            ?>
                            <?php
                            $form = $this->beginWidget('CActiveForm', array(
                                'id' => 'purchase-category-form',
                                //'htmlOptions' => array('class' => 'form-inline',),
                                'action' => Yii::app()->createUrl('PurchaseCategory/create'),
                                'enableAjaxValidation' => true,
                                'enableClientValidation' => true,
                                'clientOptions' => array(
                                    'validateOnSubmit' => true,
                                    'validateOnChange' => true,
                                    'validateOnType' => false,
                                ),
                            ));
                            ?>
                            <div class="row">
                                <div class="col-xs-12 form-group">
                                    <div class="dotted-line"></div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="form-group display_error">
                                        <label>Category Name <span class="errorMessage">*</span></label>
                                        <input type="text" class="form-control" id="PurchaseCategory_category_name"
                                            name="PurchaseCategory[category_name]" placeholder="Category Name">
                                        <?php echo $form->error($model, 'category_name'); ?>
                                        <input type="hidden" id="category_id" name="category_id">
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="form-group display_error">
                                        <label>Company <span class="errorMessage">*</span></label>
                                        <select name="PurchaseCategory[company_id][]" id="PurchaseCategory_company_id"
                                            multiple="multiple" class="form-control js-example-basic-multiple">
                                            <option value="">Choose Company</option>
                                            <?php
                                            foreach ($company as $key => $value) {
                                                ?>
                                                <option value="<?php echo $value['id']; ?>">
                                                    <?php echo $value['name']; ?>
                                                </option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                        <?php echo $form->error($model, 'company_id'); ?>
                                    </div>
                                </div>
                                <div class="col-md-6 form-group">
                                    <label>&nbsp;</label>
                                    <div class="text-right text-sm-left">
                                        <?php echo CHtml::submitButton($model->isNewRecord ? 'Save' : 'Save', array('class' => 'btn btn-info btn-sm')); ?>
                                        <?php echo CHtml::resetButton('Close', array('onclick' => 'closeaction(this,event);', 'class' => 'btn btn-default btn-sm')); ?>
                                    </div>
                                </div>
                            </div>
                            <?php $this->endWidget(); ?>
                        <?php } ?>
                    </div>
                </div>
                <div class="cat_common">
                    <div id="msg_box"></div>
                    <div class="exp-list_cat">
                        <?php
                        $this->widget('zii.widgets.CListView', array(
                            'dataProvider' => $dataProvider,
                            'itemView' => '_newview',
                            'template' => '<div>{sorter}</div><div class="container1"><table cellpadding="10" id="itemtable" class="table  list-view sorter">{items}</table></div>',
                        ));
                        ?>

                    </div>
                </div>
            </div>
            <!--Rev changes-->
            <div id="viewsubcatitem" class="tab-pane fade">
                <div class="entries-wrapper">
                    <div class="row">
                        <div class="col-xs-12 sub-heading">
                            <div class="heading-title">SubCategory</div>
                            <a class="btn btn-info btn-sm subcatadd">Add Sub Category</a>
                        </div>
                    </div>
                    <div class="padding-bottom-5" id="sub_category_body" style="display:none;">
                        <span class="error_message"></span>
                        <?php
                        if (isset(Yii::app()->user->role) && (in_array('/purchaseCategory/manageitem', Yii::app()->user->menuauthlist))) {
                            ?>
                            <?php
                            $form = $this->beginWidget('CActiveForm', array(
                                'id' => 'purchase-sub-category-form',
                                //'htmlOptions' => array('class' => 'form-inline',),
                                'action' => Yii::app()->createUrl('PurchaseSubCategoryName/Create'),
                                'enableAjaxValidation' => true,
                                'enableClientValidation' => true,
                                'clientOptions' => array(
                                    'validateOnSubmit' => true,
                                    'validateOnChange' => true,
                                    'validateOnType' => false,
                                ),
                            ));
                            ?>
                            <div class="row">
                                <div class="col-xs-12 form-group">
                                    <div class="dotted-line"></div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="form-group display_error">
                                        <label>Sub Category Name <span class="errorMessage">*</span></label>
                                        <input type="text" class="form-control" id="PurchaseSubCategory_sub_category_name"
                                            name="PurchaseSubCategory[sub_category_name]" placeholder="Sub Category Name">
                                        <?php echo $form->error($model5, 'sub_category_name'); ?>
                                        <input type="hidden" id="sub_category_id" name="sub_category_id">
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-4">
                                    <div class="form-group display_error">
                                        <label>Company <span class="errorMessage">*</span></label>
                                        <select name="PurchaseSubCategory[company_id][]" id="PurchaseSubCategory_company_id"
                                            multiple="multiple" class="form-control js-example-basic-multiple">
                                            <option value="">Choose Company</option>
                                            <?php
                                            foreach ($company as $key => $value) {
                                                ?>
                                                <option value="<?php echo $value['id']; ?>">
                                                    <?php echo $value['name']; ?>
                                                </option>
                                                <?php
                                            }
                                            ?>

                                        </select>
                                        <?php echo $form->error($model5, 'company_id'); ?>
                                    </div>
                                </div>
                                <div class="col-md-3 form-group">
                                    <label>&nbsp;</label>
                                    <div class="text-right text-sm-left">
                                        <?php echo CHtml::submitButton($model5->isNewRecord ? 'Save' : 'Save', array('class' => 'btn btn-sm btn-info')); ?>
                                        <?php echo CHtml::resetButton('Close', array('onclick' => 'closeaction3(this,event);', 'class' => 'btn btn-default btn-sm')); ?>
                                    </div>
                                </div>
                            </div>
                            <?php $this->endWidget(); ?>
                        <?php } ?>
                    </div>
                </div>


                <div class="cat_common">
                    <div id="msg_box"></div>
                    <div class="exp-list_cat">
                        <?php
                        $this->widget('zii.widgets.CListView', array(
                            'dataProvider' => $dataProvider4,
                            'itemView' => '_newsubview',
                            'template' => '<div>{sorter}</div><div class="container1"><table cellpadding="10" id="subitemtable" class="table  list-view sorter">{items}</table></div>',
                        ));
                        ?>

                    </div>
                </div>
            </div>
            <!--Rev changes ends-->

            <div id="viewbrand" class="tab-pane fade">
                <div class="entries-wrapper">
                    <div class="row">
                        <div class="col-xs-12 sub-heading">
                            <div class="heading-title">Brand List</div>
                            <a class="btn btn-info btn-sm brandadd">Add Brand</a>
                        </div>
                    </div>
                    <div class="padding-bottom-5" id="brand_body" style="display:none;">
                        <?php
                        if (isset(Yii::app()->user->role) && (in_array('/purchaseCategory/manageitem', Yii::app()->user->menuauthlist))) {
                            ?>
                            <?php
                            $form = $this->beginWidget('CActiveForm', array(
                                'id' => 'brand-form',
                                'action' => Yii::app()->createUrl('Brand/create'),
                                'enableAjaxValidation' => true,
                                'enableClientValidation' => true,
                                'clientOptions' => array(
                                    'validateOnSubmit' => true,
                                    'validateOnChange' => true,
                                    'validateOnType' => false,
                                ),
                            ));
                            ?>
                            <div class="row">
                                <div class="col-xs-12 form-group">
                                    <div class="dotted-line"></div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="form-group display_error">
                                        <label>Brand <span class="errorMessage">*</span></label>
                                        <input type="text" class="form-control" id="Brand_brand_name"
                                            name="Brand[brand_name]" placeholder="Brand">
                                        <?php echo $form->error($model2, 'brand_name'); ?>
                                        <input type="hidden" id="brand_id" name="brand_id">
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="form-group display_error">
                                        <label>Company <span class="errorMessage">*</span></label>
                                        <select name="Brand[company_id][]" id="Brand_company_id" multiple="multiple"
                                            class="form-control js-example-basic-multiple">
                                            <option value="">Choose Company</option>
                                            <?php
                                            foreach ($company as $key => $value) {
                                                ?>
                                                <option value="<?php echo $value['id']; ?>">
                                                    <?php echo $value['name']; ?>
                                                </option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                        <?php echo $form->error($model2, 'company_id'); ?>
                                    </div>
                                </div>
                                <div class="col-md-3 form-group">
                                    <label>&nbsp;</label>
                                    <div class="text-right text-sm-left">
                                        <?php echo CHtml::submitButton($model2->isNewRecord ? 'Save' : 'Save', array('class' => 'btn btn-sm btn-info')); ?>
                                        <?php echo CHtml::resetButton('Close', array('onclick' => 'closeaction1(this,event);', 'class' => 'btn btn-default btn-sm')); ?>
                                        <?php $this->endWidget(); ?>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>

                <div class="cat_common">
                    <div id="msg_box"></div>
                    <div class="exp-list_cat">
                        <?php
                        $this->widget('zii.widgets.CListView', array(
                            'dataProvider' => $dataProvider3,
                            'itemView' => '_brand',
                            'template' => '<div>{sorter}</div><div class="container1"><table id="brandtable" cellpadding="10" class="table  list-view sorter">{items}</table></div>',
                        ));
                        ?>

                    </div>

                </div>
            </div>
            <div id="viewspec" class="tab-pane fade">
                <div class="entries-wrapper">
                    <div class="row">
                        <div class="col-xs-12 sub-heading">
                            <div class="heading-title">Specification List</div>
                            <a class="btn btn-info btn-sm specadd">Add Specification</a>
                        </div>
                    </div>
                    <div class="padding-bottom-5" id="spec_body" style="display:none;">
                        <?php if (isset(Yii::app()->user->role) && (in_array('/purchaseCategory/manageitem', Yii::app()->user->menuauthlist))) { ?>
                            <?php
                            $form = $this->beginWidget('CActiveForm', array(
                                'id' => 'purchase-specification',
                                'htmlOptions' => array('enctype' => 'multipart/form-data'),
                                'action' => Yii::app()->createUrl('PurchaseCategory/createspecification'),
                                'enableAjaxValidation' => false,
                                'enableClientValidation' => true,
                                'clientOptions' => array(
                                    'validateOnSubmit' => true,
                                    'validateOnChange' => true,
                                    'validateOnType' => false,
                                ),
                            )); ?>
                            <div class="row">
                                <div class="col-xs-12 form-group">
                                    <div class="dotted-line"></div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="form-group display_error">
                                        <label>Category</label>
                                        <select name="PurchaseSpecification[cat_id]" id="PurchaseSpecification_cat_id"
                                            onchange="duplicateExists()"
                                            class="form-control PurchaseSpecification_cat_id select_js">
                                            <option value="">Choose Category</option>
                                            <?php
                                            $categoryList = PurchaseCategory::model()->findAll();
                                            foreach ($categoryList as $key => $value) {
                                                $selected = '';
                                                if ($value['id'] == isset(Yii::app()->user->cat_id) ? Yii::app()->user->cat_id : '') {
                                                    $selected = 'selected';
                                                } ?>
                                                <option value="<?php echo $value['id']; ?>" <?php echo $selected ?>>
                                                    <?php echo $value['category_name']; ?>
                                                </option>
                                            <?php } ?>
                                        </select>
                                        <?php echo $form->error($model3, 'cat_id'); ?>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="form-group display_error">
                                        <label>Sub Category</label>
                                        <select name="PurchaseSpecification[sub_cat_id]"
                                            id="PurchaseSpecification_sub_cat_id" onchange="duplicateExists()"
                                            class="form-control PurchaseSpecification_sub_cat_id select_js">
                                            <option value="">Choose SubCategory</option>
                                            <?php
                                            $subcategoryList = PurchaseSubCategory::model()->findAll();
                                            foreach ($subcategoryList as $key => $value) {
                                                $selected = '';
                                                if ($value['id'] == isset(Yii::app()->user->sub_cat_id) ? Yii::app()->user->sub_cat_id : '') {
                                                    $selected = 'selected';
                                                }
                                                ?>
                                                <option value="<?php echo $value['id']; ?>" <?php echo $selected ?>>
                                                    <?php echo $value['sub_category_name']; ?>
                                                </option>
                                                <?php
                                            }
                                            ?>

                                        </select>
                                        <?php echo $form->error($model3, 'sub_cat_id'); ?>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="form-group display_error">
                                        <label>Brand</label>
                                        <select name="PurchaseSpecification[brand_id]" id="PurchaseSpecification_brand_id"
                                            onchange="duplicateExists()" class="form-control select_js">
                                            <option value="">Choose Brand</option>
                                            <?php
                                            foreach ($brand as $key => $value) {
                                                $selected = '';
                                                if ($value['id'] == isset(Yii::app()->user->brand_id) ? Yii::app()->user->brand_id : '') {
                                                    $selected = 'selected';
                                                }
                                                ?>
                                                <option value="<?php echo $value['id']; ?>" <?php echo $selected ?>>
                                                    <?php echo $value['brand_name']; ?>
                                                </option>
                                                <?php
                                            }
                                            ?>

                                        </select>
                                        <?php echo $form->error($model3, 'brand_id'); ?>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="form-group">
                                        <label>Specification Type</label>
                                        <select name="PurchaseSpecification[specification_type]"
                                            id="PurchaseSpecification_specification_type" onchange="duplicateExists()"
                                            class="PurchaseSpecification_specification_type form-control select_js">
                                            <option value="O">By Quantity</option>
                                            <option value="A">By Length</option>
                                            <option value="G">By Width * Height</option>
                                        </select>
                                        <?php echo $form->error($model3, 'specification_type'); ?>
                                    </div>
                                </div>
                                <div id="dieno" class="specoption">
                                    <div class="col-md-3 col-sm-4">
                                        <div class="form-group">
                                            <label>Die No</label>
                                            <input type="text" class="form-control" id="PurchaseSpecification_dieno"
                                                name="PurchaseSpecification[dieno]" placeholder="Profile/Die No"
                                                value="<?php echo isset(Yii::app()->user->dieno) ? Yii::app()->user->dieno : ''; ?>">
                                            <?php echo $form->error($model3, 'dieno'); ?>
                                        </div>
                                    </div>
                                </div>
                                <div id="specification" class="specoption">
                                    <div class="col-md-3 col-sm-4">
                                        <div class="form-group">
                                            <label>Specification</label>
                                            <input type="text" class="form-control" id="Specification_specification"
                                                onchange="duplicateExists()" name="PurchaseSpecification[specification]"
                                                placeholder="Specification">
                                            <?php echo $form->error($model3, 'specification'); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="form-group display_error">
                                        <label>Unit</label>
                                        <select name="PurchaseSpecification[unit]" id="PurchaseSpecification_unit"
                                            class="form-control">
                                            <option value="">Choose Unit</option>
                                            <?php
                                            foreach ($unit as $key => $value) {
                                                $selected = '';
                                                if ($value['unit_name'] == isset(Yii::app()->user->unit) ? Yii::app()->user->unit : '') {
                                                    $selected = 'selected';
                                                }
                                                ?>
                                                <option value="<?php echo $value['unit_name']; ?>" <?php echo $selected ?>>
                                                    <?php echo $value['unit_name']; ?>
                                                </option>
                                                <?php
                                            } ?>

                                        </select>
                                        <?php echo $form->error($model3, 'unit'); ?>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="form-group display_error">
                                        <label>HSN Code</label>
                                        <input type="text" class="form-control" id="PurchaseSpecification_hsn_code"
                                            name="PurchaseSpecification[hsn_code]" placeholder="HSN Code"
                                            value="<?php echo isset(Yii::app()->user->hsn_code) ? Yii::app()->user->hsn_code : ''; ?>">
                                        <?php echo $form->error($model3, 'hsn_code'); ?>
                                        <input type="hidden" id="hsn_code" name="hsn_code">
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="form-group display_error">
                                        <label>Company</label>
                                        <select name="PurchaseSpecification[company_id][]"
                                            id="PurchaseSpecification_company_id" multiple="multiple"
                                            class="form-control js-example-basic-multiple PurchaseSpecificationcompany">
                                            <option value="">Choose Company</option>
                                            <?php
                                            foreach ($company as $key => $value) {
                                                $selected = '';
                                                if (!empty(Yii::app()->session['company_id'])) {
                                                    if (in_array($value['id'], Yii::app()->session['company_id'])) {
                                                        $selected = 'selected';
                                                    }
                                                } ?>
                                                <option value="<?php echo $value['id']; ?>" <?php echo $selected ?>>
                                                    <?php echo $value['name']; ?>
                                                </option>
                                                <?php
                                            } ?>
                                        </select>
                                        <?php echo $form->error($model3, 'company_id'); ?>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="form-group">
                                        <label>File</label>
                                        <?php echo $form->fileField($model3, 'filename', array('class' => 'form-control')); ?>
                                        <?php echo $form->error($model3, 'filename'); ?>
                                        <div class="img_msg"></div>
                                    </div>
                                </div>
                                <input type="hidden" id="specification_id" name="specification_id">
                                <div class="col-xs-12 form-group text-right">
                                    <?php echo CHtml::submitButton('Save', array('class' => 'btn btn-sm btn-info btn-submit createspec')); ?>
                                    <?php echo CHtml::resetButton('Close', array('onclick' => 'closeaction2(this,event);', 'class' => 'btn btn-default btn-sm')); ?>
                                </div>
                            </div>
                            <?php $this->endWidget(); ?>
                        <?php } ?>
                    </div>
                </div>
                <div id="common">
                    <div id="msg_box"></div>
                    <div class="exp-list_spc">
                        <?php $this->renderPartial('_specification', array('model' => $model3)); ?>
                    </div>
                </div>
            </div>
            <div id="viewunit" class="tab-pane in fade unit-conversion-form">
                <div class="entries-wrapper padding-bottom-5">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="heading-title">Unit Conversion</div>
                        </div>
                    </div>
                    <span class="error_message"></span>
                    <?php
                    $form = $this->beginWidget('CActiveForm', array(
                        'id' => 'purchase-category-unit-form',
                        // 'htmlOptions' => array('class' => 'form-inline', ),
                        'action' => Yii::app()->createUrl('UnitConversion/createunitconvertion'),
                        //'enableAjaxValidation' => true,
                        'enableClientValidation' => true,
                        'clientOptions' => array(
                            'validateOnSubmit' => true,
                            'validateOnChange' => true,
                            'validateOnType' => false,
                        ),
                    ));
                    ?>
                    <div class="row">
                        <div class="col-xs-12 form-group">
                            <div class="dotted-line"></div>
                        </div>
                        <div class="col-md-3 col-sm-6">
                            <div class="form-group">
                                <label>Item</label>
                                <select name="UnitConversion[item_ids]" id="purchase_items"
                                    class="form-control purchase_items select_js">
                                    <option value="">Choose Item</option>
                                    <?php foreach ($purchaseitemlist as $key => $value) {
                                        ?>
                                        <option value="<?php echo $value['id']; ?>">
                                            <?php echo $value['data']; ?>
                                        </option>
                                        <?php
                                    } ?>
                                </select>
                                <?php echo $form->hiddenField($ucmodel, 'item_id', array('type' => "hidden", 'size' => 2, 'maxlength' => 2)); ?>
                                <?php echo $form->error($ucmodel, 'item_id'); ?>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6">
                            <div class="form-group">
                                <label>Stock Unit</label>
                                <input type="text" class="form-control" id="purchaseitem_baseunit"
                                    name="UnitConversion[base_unit]" placeholder="Stock Unit" readonly>
                                <input type="hidden" class="form-control" id="purchaseitem_baseunit_id"
                                    name="UnitConversion[base_unit_id]" value=''>

                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6">
                            <div class="form-group">
                                <label>Unit to</label>
                                <?php
                                echo $form->dropDownList($model4, 'unit_name', array('' => 'Please choose Unit To'), array('class' => 'form-control name select_js'));
                                ?>
                                <?php echo $form->error($model4, 'unit_name'); ?>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6">
                            <div class="form-group">
                                <label>Conversion Factor</label>
                                <?php echo $form->textField($ucmodel, 'conversion_factor', array('class' => 'form-control', 'placeholder' => 'Conversion Factor')); ?>
                                <div><small style='color:red' ;>
                                        <?php echo 'Conversion Factor = Base Unit / Other Unit'; ?>
                                    </small></div>
                                <?php echo $form->error($ucmodel, 'conversion_factor'); ?>
                            </div>
                        </div>
                        <div class="col-sm-12 text-right">
                            <input type="hidden" class="form-control" id="unit_convertion_id"
                                name="UnitConversion[unit_convertion_id]" value=''>
                            <?php echo CHtml::submitButton($model->isNewRecord ? 'Save' : 'Save', array("class" => "btn btn-info btn-sm")); ?>
                            <?php echo CHtml::resetButton('Cancel', array("class" => "btn_cancel btn btn-default btn-sm")); ?>
                        </div>
                    </div>
                    <?php $this->endWidget(); ?>
                </div>
                <div class="cat_common">
                    <div id="msg_box"></div>
                    <div class="exp-list_cat">
                        <?php

                        // echo count($dataProvider4->getModels());exit;
                        $this->widget('zii.widgets.CListView', array(
                            'dataProvider' => $ucmodel->search(),
                            'enablePagination' => false,
                            'itemView' => '_unitconversion',
                            'template' => '<div>{sorter}</div><div class="container1"><table id="unittable" cellpadding="10" class="table  list-view sorter">{items}</table></div>',
                        ));
                        ?>
                    </div>
                </div>
            </div>
            <div id="importItem" class="tab-pane fade">
                <?php
                $Importmodel = new ImportForm();
                $Importmodel2 = new PurchaseItemsImport('search');
                $Importmodel2->unsetAttributes();

                if (isset($_GET['PurchaseItemsImport'])) {
                    $Importmodel2->attributes = $_GET['PurchaseItemsImport'];
                }
                echo $this->renderPartial("importpurchaseitems", array('model' => $Importmodel, 'model2' => $Importmodel2));
                ?>
            </div>
        </div>
    </div>
</div>



<div id='FlickrGallery'></div>




<script>
    function closeaction() {
        $("#category_body").hide();
        $('#PurchaseCategory_company_id').val('').trigger('change');
    }

    function closeaction1() {
        $("#brand_body").hide();
        $('#Brand_company_id').val('').trigger('change');
    }

    function closeaction2() {
        $("#spec_body").hide();
    }
    function closeaction3() {
        $("#sub_category_body").hide();
        $('#PurchaseSubCategory_company_id').val('').trigger('change');
    }
    $(document).ready(function () {
        $(".select_js").select2();
        //$('.js-example-basic-multiple').select2();
        $('.js-example-basic-multiple').select2({
            width: '100%',
            placeholder: "Company",
        });
    });
    $(document).ready(function () {

        $('a[data-toggle="tab"]').on('click', function (e) {
            window.localStorage.removeItem("activeTab");
            window.localStorage.setItem('activeTab', $(e.target).attr('href'));
        });
        var activeTab = window.localStorage.getItem('activeTab');
        if (activeTab) {
            if (activeTab == "#viewspec") {
                var cat = "<?php echo isset(Yii::app()->user->cat_id) ? Yii::app()->user->cat_id : ''; ?>";
                if (cat != "") {
                    $("#spec_body").show();
                }
            }
            $('#myTab a[href="' + activeTab + '"]').tab('show');
        }
        $(".catadd").click(function () {
            $("#category_body").show();
        });
        $(".subcatadd").click(function () {
            $("#sub_category_body").show();
        });
        $(".brandadd").click(function () {
            $("#brand_body").show();
        });
        $(".specadd").click(function () {
            $("#spec_body").show();
        });
    });

    $("#PurchaseSpecification_specification_type").change(function () {
        var specType = $("#PurchaseSpecification_specification_type").val();
        if (specType == "A") {
            $("#dieno").css("display", "inline");
            $("#specification").css("display", "none");
            $("#Specification_specification").val("");
        } else {
            $("#dieno").css("display", "none");
            $("#specification").css("display", "inline");
            $("#Specification_specification").val("");
        }
    });
</script>

<?php
Yii::app()->clientScript->registerScript('myjavascript', '
     $(document).ready(function(){
     
       $("#itemtable").dataTable( {
            "scrollY": "300px",
            "scrollX": true,
            "scrollCollapse": true,
            authWidth:false,
           
            "columnDefs"        : [       
                { 
                    "searchable"    : false, 
                    "targets"       : [0] 
                },
                { "bSortable": false, "aTargets": [-1]  }
            ]
            
        });

        $("#subitemtable").dataTable( {
            "scrollY": "300px",
            "scrollX": true,
            "scrollCollapse": true,
           
            "columnDefs"        : [       
                { 
                    "searchable"    : false, 
                    "targets"       : [0] 
                },
                { "bSortable": false, "aTargets": [-1]  }
            ]
            
        });
        
	    $("#brandtable").dataTable( {
            "scrollY": "300px",
            "scrollX": true,
            "scrollCollapse": true,
            "columnDefs"        : [       
                { 
                    "searchable"    : false, 
                    "targets"       : [0] 
                },
                { "bSortable": false, "aTargets": [-1]  }
            ]
        } );
        
	    $("#spectable").dataTable( {
            "scrollY": "300px",
            "scrollX": true,
            "scrollCollapse": true,
            "columnDefs"        : [       
                { 
                    "searchable"    : false, 
                    "targets"       : [0] 
                },
                { "bSortable": false, "aTargets": [-1]  }
            ],
           
            "initComplete": function(){ 
                $("#spectable").show(); 
            }
            
        } );

        $("#unittable").dataTable( {
            "scrollY": "300px",
            "scrollCollapse": true,
            "scrollX": true,
            "columnDefs"        : [       
                { 
                    "searchable"    : false, 
                    "targets"       : [1,2,3,4,5],
                    "orderable": false,
                    "visible": true
                },
            ],                         
    } );
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();

    var table = $("#spectable");
    $("#cat_id").on( "change", function () {                          
        var cat_name = $(this).val().trim();;
        table
            .columns( 3 )
            .search( cat_name )
            .draw();
    } );

    $("#spec_brand_id").on( "change", function () {                           
        var brand = $(this).val().trim();      
        table
            .columns( 4 )
            .search( brand )
            .draw();
    } );

    $("#Specification_type").on( "change", function () {                    
        var spec_type = $(this).val();        
        table
            .columns( 5 )
            .search( spec_type )
            .draw();
    } );

    $("#spec_id").on( "change", function () {                    
        var spec = $(this).val().trim();    
        table
            .columns( 6 )
            .search( spec )
            .draw();
    } );
        
    $("#Specification_hsn_code").on( "keyup", function () {                    
        var hsn = $(this).val();
        table
            .columns( 7 )
            .search( hsn )
            .draw();
    } );
    $("#clear").on( "click", function () {
        $("#cat_id,#spec_brand_id,#spec_id").val("").trigger("change");
        $("#Specification_type").val("By Quantity").trigger("change");
        var selectedText=" ";
        table
            .columns([ 3,4,5,6,7 ] )
            .search( selectedText )
            .draw();
    } );

    MergeGridCells();
        $("a[data-toggle=\'tab\']").on("shown.bs.tab", function(e){
            $($.fn.dataTable.tables(true)).DataTable()
               .columns.adjust();
         });
        
	});
        
    ');
?>

<script>
    $("#cat_search").on("keyup", function () {
        var value = $(this).val().toLowerCase();
        $("#itemtable tr").filter(function () {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });
    $("#sub_cat_search").on("keyup", function () {
        var value = $(this).val().toLowerCase();
        $("#subitemtable tr").filter(function () {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });

    $("#brand_search").on("keyup", function () {
        var value = $(this).val().toLowerCase();
        $("#brandtable tr").filter(function () {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });

    $("#unit_search").on("keyup", function () {
        var value = $(this).val().toLowerCase();
        $("#unittable tr").filter(function () {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });


    $("#spec_search").on("keyup", function () {
        var value = $(this).val().toLowerCase();
        $("#spectable tr").filter(function () {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });


    function MergeGridCells() {
        var dimension_cells = new Array();
        var dimension_col = null;
        var columnCount = 1;
        for (dimension_col = 0; dimension_col <= columnCount; dimension_col++) {
            var first_instance = null;
            var rowspan = 1;
            $("#unittable").find('tr').each(function () {
                var dimension_td = $(this).find('td:nth-child(' + dimension_col + ')');
                if (first_instance === null) {
                    first_instance = dimension_td;
                } else if (dimension_td.text() === first_instance.text()) {
                    dimension_td.attr('hidden', true);
                    ++rowspan;
                    first_instance.attr('rowspan', rowspan);
                } else {
                    first_instance = dimension_td;
                    rowspan = 1;
                    first_instance.attr('rowspan', rowspan);
                }
            });
        }
    }
    $("#Specification_specification").keyup(function () {
        $(this).val($(this).val().replace(/^\s+/, ""));
    });
    $("#Brand_brand_name").keyup(function () {
        $(this).val($(this).val().replace(/^\s+/, ""));
    });
    $("#PurchaseCategory_category_name").keyup(function () {
        $(this).val($(this).val().replace(/^\s+/, ""));
    });
    $("#PurchaseSubCategory_sub_category_name").keyup(function () {
        $(this).val($(this).val().replace(/^\s+/, ""));
    });
    $(".editItems").click(function (e) {
        var catId = $(this).attr("data-id");
        $("#category_id").val(catId);
        $("#btn_category").text("Update");
        $('.loading-overlay').addClass('is-active');
        $.ajax({
            url: '<?php echo Yii::app()->createAbsoluteUrl('purchaseCategory/getItemForEdit'); ?>',
            type: 'GET',
            data: {
                category: catId
            },
            async: true,
            success: function (response) {
                var result = JSON.parse(response);
                if (result["company_id"] != null) {
                    var array = result["company_id"].split(',');
                    $('#PurchaseCategory_company_id').val(array);
                    $('#PurchaseCategory_company_id').trigger('change');
                }
                $("#PurchaseCategory_parent_id").val(result["parent"]);
                $("#PurchaseCategory_category_name").val(result["name"]);
                $("#PurchaseCategory_hsn_code").val(result["hsn"]);
                $("#category_body").slideDown();

            }
        });
    });
    $(".editsubItems").click(function (e) {
        var catId = $(this).attr("data-id");
        $("#sub_category_id").val(catId);
        $("#btn_category").text("Update");
        $('.loading-overlay').addClass('is-active');
        $.ajax({
            url: '<?php echo Yii::app()->createAbsoluteUrl('PurchaseSubCategoryName/update') ?>',
            type: 'GET',
            data: {
                sub_category_id: catId
            },
            async: true,
            success: function (response) {
                var result = JSON.parse(response);
                if (result["company_id"] != null) {
                    var array = result["company_id"].split(',');
                    $("#PurchaseSubCategory_company_id").val(array);
                    $("#PurchaseSubCategory_company_id").trigger('change');

                }
                $("#PurchaseSubCategory_sub_category_name").val(result["sub_category_name"]);
                $("#sub_category_body").slideDown();
            }
        });


    });


    $(".editBrands").click(function (e) {
        var bndId = $(this).attr("data-id");
        $("#brand_id").val(bndId);
        $("#btn_brand").text("Update");
        $('.loading-overlay').addClass('is-active');
        $.ajax({
            url: '<?php echo Yii::app()->createAbsoluteUrl('brand/update'); ?>',
            type: 'GET',
            data: {
                brand: bndId
            },
            async: true,
            success: function (response) {
                var result = JSON.parse(response);
                var array = result["company_id"].split(',');
                $("#brand_id").val(result["id"]);
                $("#Brand_brand_name").val(result["name"]);
                $('#Brand_company_id').val(array);
                $('#Brand_company_id').trigger('change');
                $("#brand_body").slideDown();

            }
        });
    });

    $(document).on("click", ".editSpecification", function (e) {
        var specificationId = $(this).attr("data-id");
        $("#specification_id").val(specificationId);
        $("#btnSpecification").text("Update");
        $('.loading-overlay').addClass('is-active');
        $.ajax({
            url: '<?php echo Yii::app()->createAbsoluteUrl('purchaseCategory/getSpecificationForEdit'); ?>',
            type: 'GET',
            data: {
                specification: specificationId
            },
            async: true,
            success: function (response) {
                var result = JSON.parse(response);
                var parent = result["parent"];
                var brand = result["brand"];
                var subcategory = result["subcategory"];
                var type = result["type"];
                var specification = result["specification"];
                var dieno = result["dieno"];
                var unit = result["unit"];
                var array = result["company_id"];
                if (result["company_id"]) {
                    var array = result["company_id"].split(',');
                }
                var hsn_code = result["hsn_code"];
                console.log(array);
                $(".PurchaseSpecification_cat_id").val(parent).trigger('change');
                $(".PurchaseSpecification_cat_id").val(parent).focus();
                $(".PurchaseSpecification_sub_cat_id").val(subcategory).trigger('change');
                $("#PurchaseSpecification_brand_id").val(brand).trigger('change');
                $("#PurchaseSpecification_specification_type").val(type).trigger('change');
                // $("#PurchaseSpecification_specification_type").val(type);
                $("#PurchaseSpecification_hsn_code").val(hsn_code);
                $('.PurchaseSpecificationcompany').val(array);
                $('.PurchaseSpecificationcompany').trigger('change');
                if (type == "A") {
                    $("#dieno").show();
                    $("#dieno").css("display", "inline");
                    $("#specification").hide();
                    $("#PurchaseSpecification_dieno").val(dieno);
                    $("#Specification_specification").val(dieno);
                } else {
                    $("#dieno").hide();
                    $("#specification").show();
                    $("#specification").css("display", "inline");
                    $("#Specification_specification").val(specification);
                    $("#PurchaseSpecification_dieno").val("");
                    //$("#PurchaseCategory_length").val("");
                }
                $("#PurchaseSpecification_unit").val(unit);
                $("#spec_body").slideDown();
                $("#spec_body").find("input, select, textarea").first().focus();
            }
        });
    });
    $(document).on("click", ".deleteSpecification", function (e) {
        var spec_id = $(this).attr("data-id");
        if (confirm("Do you want to remove this item?")) {
            $('.loading-overlay').addClass('is-active');
            $.ajax({
                url: '<?php echo Yii::app()->createAbsoluteUrl('purchaseCategory/removespecification'); ?>',
                type: 'GET',
                data: {
                    specification: spec_id
                },
                success: function (data) {
                    if (data == 1) {
                        $("#errMsg").show()
                            .html('<div class="alert alert-danger">Failed! Please try again..</div>')
                            .fadeOut(10000);
                        return false;
                    } else if (data == 2) {
                        $("#errMsg").show()
                            .html("<div class='alert alert-warning'>Can't Delete.Already in use.</div>")
                            .fadeOut(10000);
                        return false;
                    }
                    else {
                        $("#errMsg").show()
                            .html('<div class="alert alert-success">Successfully removed the item.</div>')
                            .fadeOut(3000, function () {
                                window.location.reload();
                            });
                        // $(".exp-list_spc").html(data);
                        //$("#spectable").show();

                    }
                }
            });
        } else {
            return false;
        }
    });
    $(document).on("change", "#purchase_items", function (e) {
        e.preventDefault();
        var purchase_itemid = $(this).children("option:selected").val();
        // var conversion_unit_id = '';
        $("#UnitConversion_item_id").val(purchase_itemid);
        $.ajax({
            url: '<?php echo Yii::app()->createAbsoluteUrl('purchaseCategory/getUnit'); ?>',
            type: 'GET',
            data: {
                purchase_itemid: purchase_itemid
            },
            async: true,
            success: function (response) {
                var result = JSON.parse(response);
                $("#purchaseitem_baseunit").val(result["Unit_name"]);
                //$("#purchaseitem_baseunit_id").val(result["Unit_id"]);
                $("#purchaseitem_baseunit_id").val(result["Unit_name"]);
                $('select[id="Unit_unit_name"]').empty();
                $('select[id="Unit_unit_name"]').append('<option value="">Please choose Unit To</option>');
                $.each(result["Unit_list"], function (key, value) {
                    $('select[id="Unit_unit_name"]').append('<option value="' + value.value + '">' + value.value + '</option>');
                });

            }
        });

    });
    $('.flash_msg').fadeOut(4000);
    $(document).ajaxComplete(function () {
        $('.loading-overlay').removeClass('is-active');
        $('#loading').hide();
    });

    $(document).on("click", ".priority_check", function (e) {
        e.preventDefault();
        var priority_check = $(this).attr('id');
        var priority_check_arr = priority_check.split("_");
        var priority_check_uc_id = priority_check_arr[4];
        var priority_check_uc_item_id = priority_check_arr[2];
        var priority_check_val = $('#prioritycheck_item_' + priority_check_uc_item_id + '_data_' + priority_check_uc_id).val();
        var priority_check_status = this.checked;

        $.ajax({
            url: '<?php echo Yii::app()->createAbsoluteUrl('UnitConversion/prioritychange'); ?>',
            type: 'POST',
            data: {
                'priority_check_uc_id': priority_check_uc_id,
                'priority_check_uc_item_id': priority_check_uc_item_id,
                'priority_check_status': priority_check_status
            },
            async: true,
            success: function (response) {
                var result = JSON.parse(response);
                var new_id = result["new_item_unitprioritycheck_id"];
                var old_id = result["old_item_unitpriorityuncheck_id"];
                if (result["priority_check_status"] == 'true') {
                    $('#prioritycheck_item_' + priority_check_uc_item_id + '_data_' + new_id).val(result["newpriority"]);
                    $('#prioritycheck_item_' + priority_check_uc_item_id + '_data_' + new_id).prop('checked', true);
                    $('#prioritycheck_item_' + priority_check_uc_item_id + '_data_' + old_id).val(result["oldpriority"]);
                    $('#prioritycheck_item_' + priority_check_uc_item_id + '_data_' + old_id).prop('checked', false);

                } else {
                    if (new_id != old_id) {
                        $('#prioritycheck_item_' + priority_check_uc_item_id + '_data_' + new_id).val(result["newpriority"]);
                        $('#prioritycheck_item_' + priority_check_uc_item_id + '_data_' + new_id).prop('checked', true);
                        $('#prioritycheck_item_' + priority_check_uc_item_id + '_data_' + old_id).val(result["oldpriority"]);
                        $('#prioritycheck_item_' + priority_check_uc_item_id + '_data_' + old_id).prop('checked', false);

                    } else {
                        alert("Please Select atleast one unit for the item.");
                    }

                }
            }
        });

    });

    $(".editunit").click(function (e) {
        var unitId = $(this).attr("data-id");
        var item_name = $('.item_name_' + unitId).text().trim();
        var item_id = $('#purchase_item_id_' + unitId).val();
        var base_unit_id = $('#base_unit_hid_' + unitId).val();
        var base_unit_name = $('.base_unit_' + unitId).text().trim();
        var conversion_factor = $('#conversion_factor_' + unitId).text().trim();
        var conversion_unit_id = $('#conversion_unit_' + unitId).val();
        var data = {
            unitId: unitId,
            item_id: item_id,
            base_unit_id: base_unit_id,
            conversion_unit_id: conversion_unit_id
        };

        $.ajax({
            method: "GET",
            data: {
                data: data
            },
            dataType: "json",
            url: '<?php echo Yii::app()->createUrl('purchaseCategory/CheckEditUnitconversion'); ?>',
            success: function (result) {

                if (result.response == 'success') {

                    $('#purchase_items').val(item_id).trigger('change');
                    //$("#purchase_items option[value='"+item_id+"']").attr('selected', true);                        
                    $("#UnitConversion_conversion_factor").val(conversion_factor);
                    $("#unit_convertion_id").val(unitId);
                    $("#UnitConversion_item_id").val(item_id);
                    $.ajax({
                        url: '<?php echo Yii::app()->createAbsoluteUrl('purchaseCategory/getUnit'); ?>',
                        type: 'GET',
                        data: {
                            purchase_itemid: item_id,
                            conversion_unit_id: conversion_unit_id
                        },
                        async: true,
                        success: function (response) {
                            var result = JSON.parse(response);
                            $("#purchaseitem_baseunit").val(result["Unit_name"]);
                            $("#purchaseitem_baseunit_id").val(result["Unit_name"]);
                            $('select[id="Unit_unit_name"]').empty();
                            $('select[id="Unit_unit_name"]').append('<option value="">Please choose Unit To</option>');
                            $.each(result["Unit_list"], function (key, value) {
                                $('select[id="Unit_unit_name"]').append('<option value="' + value.value + '">' + value.value + '</option>');
                            });
                            $("#Unit_unit_name option[value='" + conversion_unit_id + "']").attr("selected", "selected");

                        }
                    });
                } else {
                    alert(result.msg);
                }

            }
        });


    });
</script>

<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                        class="sr-only">Close</span></button>
                <img src="" id="imagepreview" width="400px">
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" defer="defer">
    $(document).on("click", ".pop", function () {
        var src = $(this).attr('modal-src');
        $('#imagepreview').attr('src', src);
        $('#imagemodal').modal('show');
    });



    $("#PurchaseSpecification_cat_id,#PurchaseSpecification_brand_id,#PurchaseSpecification_specification_type,#PurchaseSpecification_unit,#PurchaseSpecification_hsn_code,#PurchaseSpecification_company_id").change(function () {
        $("#Specification_specification").keyup();
        var data = $("#purchase-specification").serialize();
        $.ajax({
            type: "POST",
            data: data,
            url: "<?php echo Yii::app()->createAbsoluteUrl('PurchaseCategory/getSpecificationDetails') ?>",
            success: function (response) {
                console.log("Stored in session sucessfully");
            }
        })
    })
    $(".filter_toggle").click(function () {
        $(".filter_form").toggle();
    })

    function deletCategory(elem, id) {

        if (confirm("Do you want to remove this item?")) {
            $('.loading-overlay').addClass('is-active');
            $.ajax({
                url: '<?php echo Yii::app()->createAbsoluteUrl('purchaseCategory/removecategory'); ?>',
                type: 'POST',
                dataType: 'json',
                data: {
                    catId: id
                },
                success: function (data) {
                    $("#errMsg").show()
                        .html('<div class="alert alert-' + data.response + '">' + data.msg + '</div>')
                        .fadeOut(10000);
                    setTimeout(function () {
                        location.reload(true);
                    }, 3000);

                }
            });
        } else {
            return false;
        }

    }
    function deletSubCategory(elem, id) {
        if (confirm("Do you want to remove this subcategory?")) {
            $('.loading-overlay').addClass('is-active');
            $.ajax({
                url: '<?php echo Yii::app()->createAbsoluteUrl('PurchaseSubCategoryName/removesubcategory') ?>',
                type: 'POST',
                dataType: 'json',
                data: {
                    catId: id
                },
                success: function (data) {
                    $("#errMsg").show().html('<div class="alert alert-' + data.response + '">' + data.msg + '</div>').fadeOut(10000);
                    setTimeout(function () {
                        location.reload(true);
                    }, 3000);
                }
            });
        } else {
            return false;
        }
    }

    function onlySpecialchars(str) {
        var regex = /^[^a-zA-Z0-9]+$/;
        var message = "";
        var matchedAuthors = regex.test(str);

        if (matchedAuthors) {
            var message = "Special characters not allowed";
        }
        return message;
    }


    $("#PurchaseCategory_category_name,#Brand_brand_name,#Specification_specification,#PurchaseSpecification_hsn_code").change(function () {
        var message = onlySpecialchars(this.value);
        $(this).siblings(".errorMessage").show().html(message).addClass('d-block');
        var disabled = false;
        var errorcount = $(".errorMessage").not(":empty").addClass("hhyu");

        if (errorcount > 0) {
            var disabled = true;
        }
        $(this).parents("form").find('.btn-info').attr('disabled', disabled)

    });

    $(".btn_cancel").click(function () {
        $('#Unit_unit_name').val('').trigger('change.select2');
        $('#purchase_items').val('').trigger('change.select2');
    })

    $('#Specification_filename').change(function () {
        var ext = $('#Specification_filename').val().split('.').pop().toLowerCase();
        var message = "";
        var disabled = false;
        if ($.inArray(ext, ['png', 'jpg', 'jpeg']) == -1) {
            var message = "Accept only jpg ,jpeg and png format!";
            var disabled = true;
        }
        $(this).siblings(".errorMessage").text(message).addClass("d-block");
        $(".createspec").attr('disabled', disabled);
    });

    $("#purchase-specification").on('submit', function (e) {
        var type = $("#PurchaseSpecification_specification_type").val();
        if (type == "A") {
            if ($("#PurchaseSpecification_dieno").val() == "") {
                $("#PurchaseSpecification_dieno").siblings(".errorMessage").text("Profile/dieno cannot be blank").addClass("d-block");
                e.preventDefault();
            }
        } else {
            if ($("#Specification_specification").val() == "") {
                $("#Specification_specification").siblings(".errorMessage").text("Specification cannot be blank").addClass("d-block");
                e.preventDefault();
            }
        }

    })

    function duplicateExists() {
        var specification = $("#Specification_specification").val().trim();
        var brand = $('#PurchaseSpecification_brand_id').val();
        var subcategory = $("#PurchaseSpecification_sub_cat_id").val();
        var category = $('#PurchaseSpecification_cat_id').val();
        //alert(specification); 
        if (specification != '') {
            $.ajax({
                url: '<?php echo Yii::app()->createAbsoluteUrl('purchaseCategory/checkDuplicateSpecification'); ?>',
                type: 'POST',
                data: {
                    specification: specification,
                    brand: brand,
                    category: category,
                    subcategory: subcategory

                },
                success: function (data) {
                    console.log(data);
                    var disabled = false;
                    var message = ""
                    if (data > 0) {
                        var disabled = true;
                        var message = "Already exist!";
                    }
                    $("#Specification_specification").siblings(".errorMessage").text(message).addClass("d-block");
                    $(".createspec").attr('disabled', disabled);

                }
            });

        }

    }
</script>