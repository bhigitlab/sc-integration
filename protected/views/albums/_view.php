

<?php
/* @var $this AlbumsController */
/* @var $data Albums */
?>


<div class="row">
	<div class="col-md-4 col-sm-3 col-xs-3">
		<div class="list_item"><span>Title : </span><?php echo CHtml::encode($data->title); ?></div>
		
		<div class="list_item"><span>Description : </span><?php echo CHtml::encode($data->description); ?></div>
	</div>
	<div class="col-md-4 col-sm-4 col-xs-4">
            <div class="list_item"><span>Project : </span><?php echo CHtml::encode($data->project->name); ?></div>
            	
	</div>
    
	<?php  
        if (Yii::app()->user->role == 1) {
		?>
	<div class="col-md-4 col-sm-1 col-xs-1">
		<span class="fa fa-edit editAlbum" data-toggle="modal" data-target=".edit" data-id="<?php echo $data->album_id; ?>"></span>
	</div>
	<?php
		}
		?>
</div>
