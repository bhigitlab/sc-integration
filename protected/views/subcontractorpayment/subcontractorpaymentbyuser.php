<?php
$this->breadcrumbs=array(
	'performa invoice',)
?>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/tableHeadFixer.js"></script>
<div class="container" id="project">
    <div class="sub-heading mb-10">
        <h3>Subcontractor Payment By User</h3>
    </div>
    <?php $this->renderPartial('_newpaymentbyusersearch', array('model' => $model)) ?>
    <div class="" id="newlist">
    <?php $this->widget('zii.widgets.CListView', array(
            'dataProvider'=>$dataProvider,
            'itemView' => '_newpaymentbyuserview', 'template' => '<div>{summary}{sorter}</div><div class="table-resposive" id="parent"><table class="table" id="fixTable">{items}</table></div>{pager}',
            'ajaxUpdate' => false,
    )); ?>
    </div>
</div>
<style>
    #parent {max-height: 60vh;border:1px solid #ddd;border-top:0px;border-right:0px;}
</style>
<script>
$(document).ready(function() {
		$("#fixTable").tableHeadFixer({'head' : true});
});
$(function () {
    $("#SubcontractorPayment_fromdate").datepicker({dateFormat: 'dd-mm-yy'});
});
$(function () {
    $("#SubcontractorPayment_todate").datepicker({dateFormat: 'dd-mm-yy'});
});
$("#clearForm").click(function() {
    window.location.href = "<?php echo Yii::app()->createAbsoluteUrl("subcontractorpayment/subcontractorpaymentbyuser") ?>";
});
</script>
