<?php setlocale(LC_MONETARY, 'en_IN'); ?>
<div class="container" id="project">

    <br/><br/>
    <div id="financialreport1" >

        <br/>

        <h2 style="text-align: center;">DAILY EXPENSE REPORT</h1>

        <div class="filtercls">
            <?php echo CHtml::beginForm(); ?>

            <div class="currentyear">
                <?php
                $currentdayYear = date('Y');


                $yearFrom = 2010;
                $yearsRange = range($yearFrom, $currentdayYear);
                $yearsar = array_combine($yearsRange, $yearsRange);
                echo CHtml::dropdownlist('year', $currentYear, $yearsar, array('class' => 'form-control yearselect'));
                ?>
            </div>

            <div class="currentmonth">
                <?php
                //$currentmonth = date('m');
                // set the month array
                $formattedMonthArray = array(
                    "01" => "January", "02" => "February", "03" => "March", "04" => "April",
                    "05" => "May", "06" => "June", "07" => "July", "08" => "August",
                    "09" => "September", "10" => "October", "11" => "November", "12" => "December",
                );

                echo CHtml::dropdownlist('month', $currentmonth, $formattedMonthArray, array('class' => 'form-control yearselect'));
                ?> 
            </div>

            <div class="currentmonth">
                <?php echo CHtml::submitButton('Submit', array('buttonType' => 'submit', 'name' => 'monthlyreport', 'class' => 'btn btn-primary')); ?>
            </div>
            <?php echo CHtml::endForm(); ?>

        </div>
        <div class="pull-right">
            <a style="cursor:pointer; " id="download" class="save_btn" href="<?php echo Yii::app()->createUrl('expenses/savetopdfmonthly', array('year' => $currentYear, 'month' => $currentmonth)) ?>">SAVE AS PDF</a>
            <a style="cursor:pointer; " id="download" class="save_btn" href="<?php echo Yii::app()->createUrl('expenses/savetoexcelmonthly', array('year' => $currentYear, 'month' => $currentmonth)) ?>">SAVE AS EXCEL</a>
        </div>

        <br/><br/>

        <h2></h2>
        <div class="tblwrap scrollTable">
            <div class="container2">
                <table class="table table-bordered  tab2">

                    <thead>

                        <tr>
                            <th style="display:none;"></th>
                            <th>Sl No.</th>
                            <th>Date</th>
                            <th>Expense Type</th>
                            <th>Receipt Type</th>
                            <th>Receipt</th>
                            <th>Payment</th>
                            <th>Description</th>
<!--                            <th>User</th>-->
                            <!--th>Data Entry</th-->

                        </tr>
                    </thead>

                    <tbody>


<?php
$i = 0;
$total = 0;
$receiptamt = 0;
$paymentamt = 0;
$type_exp = '';
$flag = 0;
if ($dailyexpenses == NULL) {
    echo '<tr><td colspan="7">No records Found</td></tr>';
} else {
    foreach ($dailyexpenses as $daily) {
        $i++;
        $type = $daily['expense_name'];

        $tblpx = Yii::app()->db->tablePrefix;
        $totalqry1 = "SELECT sum(amount) as sum FROM " . $tblpx . "dailyexpense  as e
							left join " . $tblpx . "company_expense_type as p on p.company_exp_id=e.exp_type_id
							WHERE YEAR(e.date)='$currentYear' AND MONTH(e.date)='$currentmonth' AND p.name = '" . $type . "' AND e.exp_type = 0 order by company_exp_id";
        $sql1 = Yii::app()->db->createCommand($totalqry1)->queryRow();
        //print_r($sql1);

        $totalqry2 = "SELECT sum(amount) as sum FROM " . $tblpx . "dailyexpense  as e
							left join " . $tblpx . "company_expense_type as p on p.company_exp_id=e.exp_type_id
							WHERE YEAR(e.date)='$currentYear' AND MONTH(e.date)='$currentmonth' AND p.name = '" . $type . "' AND e.exp_type = 1 order by company_exp_id";
        $sql2 = Yii::app()->db->createCommand($totalqry2)->queryRow();
        //print_r($sql2);



        if ($type_exp != $type) {

            $type_exp = $type;
            $total = $total + $daily['amount'];

            $flag++;
            if ($flag != 1) {
                ?>


                                        <tr>	
                                            <td colspan="3"></td>
                                            <td  align="right">Sub Total</td>
                                            <td align="right"><?php echo $sum1; ?></td>
                                            <td align="right"><?php echo $sum2; ?></td>
                                            <td></td>
                                           
                                        </tr>


                <?php
            }
            ?>
            <?php
            $sum1 = money_format('%!.0n', $sql1['sum']);
            $sum2 = money_format('%!.0n', $sql2['sum']);
            ?>
                                    <tr>
                                        <td colspan="7"><b><?php echo $daily['expense_name']; ?></b></td>
                                    </tr>


                                    <?php
                                }
                                ?>
                                <tr>
                                    <th style="display:none;"></th>
                                    <td><?= $i++ ?></td>
                                    <td><?php echo date('d-M-Y', strtotime($daily['date'])); ?></td>
                                <?php if ($daily['expensetype'] == 1) { ?>
                                        <td><?php echo $daily['expense_name']; ?></td>
                                <?php } else { ?>
                                        <td></td>
        <?php } ?>
        <?php if ($daily['expensetype'] == 0) { ?>

                                        <td><?php echo $daily['expense_name']; ?></td>
                                    <?php } else { ?>
                                        <td></td>
                                    <?php } ?>
                                    <?php if ($daily['expensetype'] == 0) { ?>
                                        <td align="right"><?php
                            $amount = money_format('%!.0n', $daily['amount']);
                            echo $amount;
                            //echo $daily['amount'];
                            $receiptamt = $receiptamt + $daily['amount'];
                                        ?></td>
                                    <?php } else { ?>
                                        <td></td>
                                        <?php } ?>
                                        <?php if ($daily['expensetype'] == 1) { ?>
                                        <td align="right"><?php
                                            $amount = money_format('%!.0n', $daily['amount']);
                                            echo $amount;
                                            //echo $daily['amount'];
                                            $paymentamt = $paymentamt + $daily['amount'];
                                            ?></td>
                                    <?php } else { ?>
                                        <td></td>
                                        <?php } ?>

                                    <td align="left"><?php echo $daily['description']; ?></td>
<!--                                    <td align="left"><?php // echo $daily['first_name']; ?></td>-->
                                    <!--td align="left"><?php echo $daily['data_entry']; ?></td-->

                                </tr>
                                    <?php
                                    $newexp = $daily['expense_name'];

                                    $tblpx = Yii::app()->db->tablePrefix;
                                    $newtotalqry1 = "SELECT sum(amount) as sum FROM " . $tblpx . "dailyexpense  as e
							left join " . $tblpx . "company_expense_type as p on p.company_exp_id=e.exp_type_id
							WHERE YEAR(e.date)='$currentYear' AND MONTH(e.date)='$currentmonth' AND p.name = '" . $newexp . "' AND e.exp_type = 0 order by company_exp_id";
                                    $newsql1 = Yii::app()->db->createCommand($newtotalqry1)->queryRow();
                                    //print_r($sql1);

                                    $newtotalqry2 = "SELECT sum(amount) as sum FROM " . $tblpx . "dailyexpense  as e
							left join " . $tblpx . "company_expense_type as p on p.company_exp_id=e.exp_type_id
							WHERE YEAR(e.date)='$currentYear' AND MONTH(e.date)='$currentmonth' AND p.name = '" . $newexp . "' AND e.exp_type = 1 order by company_exp_id";
                                    $newsql2 = Yii::app()->db->createCommand($newtotalqry2)->queryRow();

                                    $newsum1 = money_format('%!.0n', $newsql1['sum']);
                                    $newsum2 = money_format('%!.0n', $newsql2['sum']);
                                }
                                ?>
                            <tr>	
                                <td colspan="3"></td>
                                <td  align="right">Sub Total</td>
                                <td align="right"><?php echo $newsum1; ?></td>
                                <td align="right"><?php echo $newsum2; ?></td>
                                <td></td>
                              
                            </tr> 
    <?php
}
?>

                        


                    </tbody>
					<tfoot>
					<tr>
                            <th colspan="3"></th>
                            <th  align="right">Total</th>
                            <th align="right"><?= money_format('%!.0n', $receiptamt); ?></th>
                            <th align="right"><?= money_format('%!.0n', $paymentamt); ?></th>
                            <th></th>
                           
                        </tr>
					</tfoot>	




                </table>
            </div><br>






    </div>





</div>

<style type="text/css">

    .filtercls,.currentyear,.currentmonth{
        float: left;
        padding: 10px;
    }
</style>

<script>
    $('.container1').scroll(function () {
        //$('.container1').css("height","300px");
        con1('.container1', '.tab1 tbody th', '.tab1 thead th');
    });
    $('.container2').scroll(function () {
        con1('.container2', '.tab2 tbody th', '.tab2 thead th');
    });

    function con1(e, f, g) {
        'use strict';
        var container = document.querySelector(e);
        var table = document.querySelector('table');
        var leftHeaders = [].concat.apply([], document.querySelectorAll(f));
        var topHeaders = [].concat.apply([], document.querySelectorAll(g));
        var topLeft = document.createElement('div');
        var computed = window.getComputedStyle(topHeaders[0]);
        container.appendChild(topLeft);
        topLeft.classList.add('top-left');
        topLeft.style.width = computed.width;
        topLeft.style.height = computed.height;

        container.addEventListener('scroll', function (e) {
            var x = container.scrollLeft;
            var y = container.scrollTop;

            leftHeaders.forEach(function (leftHeader) {
                leftHeader.style.transform = translate(x, 0);
            });
            topHeaders.forEach(function (topHeader, i) {
                if (i === 0) {
                    topHeader.style.transform = translate(0, y);
                } else {
                    topHeader.style.transform = translate(0, y);
                }
            });
            topLeft.style.transform = translate(x, y);
        });

        function translate(x, y) {
            return 'translate(' + x + 'px, ' + y + 'px)';
        }


    }
    $(document).ready(function () {
        var tabHeight = $('.tab1').height();
        if (tabHeight < 150) {
            $('.container1').css('min-height', tabHeight + 'px');
        } else
        {
            $('.container1').css('min-height', '300px');
        }
        var tabHeight2 = $('.tab2').height();
        if (tabHeight2 < 150) {
            $('.container2').css('min-height', tabHeight2 + 'px');
        } else
        {
            $('.container2').css('min-height', '300px');
        }
    });
</script>

<style>
 .container1 ,.container2{
        width: 100%;
        height: 150px;
        overflow: auto;
        position: relative;
    }
   .container3{
        width: 100%;
        height: 300px;
        overflow: auto;
        position: relative;
    }

th,
.top-left {
	background: #eee;
}
.top-left {
	box-sizing: border-box;
	position: absolute;
	left: 0;
	border-right: 1px solid #ccc;
	border-bottom: 1px solid #ccc;
}

table {
   
	border-spacing: 0;
	position: absolute;
   
}
th,
td {
	border-right: 1px solid #ccc !important;

}

</style>

