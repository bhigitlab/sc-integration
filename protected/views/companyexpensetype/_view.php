<?php
/* @var $this ProjectsController */
/* @var $data Projects */
?>

<?php
if ($index == 0) {
?>
    <thead>
        <tr>
            <th><input type="checkbox" id="selectAll"></th>
            <th>SI No</th>
            <th>Name</th>
            <th>Expense Type</th>
            <?php
            if (isset(Yii::app()->user->role) && (in_array('/companyexpensetype/update', Yii::app()->user->menuauthlist))) {
            ?>
                <th>Action</th>
            <?php } ?>
        </tr>
    </thead>
<?php } ?>
<tr>
    <?php
     if ($data->status == 1 && ($data->name != "PETTY CASH ISSUED" && $data->name != "PETTY CASH REFUND")) {
    ?>
    <td style="width: 50px;"><input type="checkbox" name="selectedItems[]" class="recordCheckbox" value="<?php echo $data->company_exp_id?>"></td>
    <?php } else{ ?>
     <td></td>
    <?php } ?>
    <td style="width: 50px;"><?php echo $index + 1; ?></td>
    <td><span><?php echo CHtml::encode($data->name); ?></span></td>


    <td>
        <?php if ($data->type == 0) {
            echo "Receipt";
        } elseif ($data->type == 1) {
            echo "Payment";
        } elseif ($data->type == 2) {
            echo "Both";
        } ?>

    </td>
    <?php
    if (isset(Yii::app()->user->role) && (in_array('/companyexpensetype/update', Yii::app()->user->menuauthlist))) {
    ?>
        <td style="width: 50px;">
            <?php
            if ($data->status == 1 && ($data->name != "PETTY CASH ISSUED" && $data->name != "PETTY CASH REFUND")) {
            ?>
                <a data-id="<?php echo $data->company_exp_id; ?>" data-target=".edit" class="fa fa-edit editcmpexpdaily"></a>
                <a data-id="<?php echo $data->company_exp_id; ?>" data-target=".delete" class="fa fa-trash deletecmpexpdaily"></a>
     
            <?php } ?>
        </td>
    <?php } ?>
</tr>