<?php
/* @var $this SubcontractorbillController */
/* @var $model Subcontractorbill */
/* @var $form CActiveForm */
?>
<?php if($is_update){ ?>
    <div class="alert alert-success hide"></div>
<div class="alert alert-danger hide"></div>

<div class="entries-wrapper">
    <div class="row">
                    <div class="col-xs-12">
                      <div class="heading-title">Update Subcontractor Bill</div>
                      <div class="dotted-line"></div>
                    </div>
    </div>

    <div id="loading" style="display:none;"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/ajax-loader.gif"></div>

    
        <?php } ?>
    <div class="form">

    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'subcontractorbill-form',        
        'enableAjaxValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
        ),
    )); ?>

    <?php
    $user       = Users::model()->findByPk(Yii::app()->user->id);
    $arrVal     = explode(',', $user->company_id);
    $newQuery   = "";
    foreach ($arrVal as $arr) {
        if ($newQuery) $newQuery .= ' OR';
        $newQuery .= " FIND_IN_SET('" . $arr . "', id)";
    }
    if($is_update){
    $scquotation        = Scquotation::model()->findByPk($model->scquotation_id);
    $project_id         = $scquotation["project_id"];
    $subcontractor_id   = $scquotation["subcontractor_id"];
    $tblpx              = Yii::app()->db->tablePrefix;
    ?> <input type="hidden" name="bill_id" id="bill_id" value="<?= $model->id;?> ">   
    <?php }
    ?>
    
    <div class="row">
        <div class="form-group col-xs-12 col-md-3">
            
                <?php echo $form->labelEx($model, 'company_id'); ?>
                <?php echo $form->dropDownList($model, 'company_id', CHtml::listData(Company::model()->findAll(array(
                    'select' => array('id, name'),
                    "condition" => '(' . $newQuery . ')',
                    'order' => 'id',
                    'distinct' => true
                )), 'id', 'name'), array('class' => 'form-control company_id', 'empty' => '-Select Company-')); ?>
                <?php echo $form->error($model, 'company_id'); ?>
            
        </div>
        <div class="form-group col-xs-12 col-md-3">
            
                <label for="Subcontractorbill_project" class="required projectlabel">Project <span class="required">*</span></label>
                <select class="form-control" id="project_id">
                    <option value="">Select Project</option>
                    <?php
                    if($is_update){
                            $company_id     = $model->company_id;
                            $projectdata    = Projects::model()->findAll(array('select' => 'pid,name', 'condition' => "(FIND_IN_SET('" . $company_id . "', company_id))"));
                            foreach ($projectdata as $data) {
                                if ($data->pid == $project_id)
                                    $selected = " selected";
                                else
                                    $selected = "";
                                echo "<option value='" . $data->pid . "'" . $selected . ">" . $data->name . "</option>";
                            }
                        }
                            ?>
                </select>
                <div class="errorMessage" id="projectErrorMsg"></div>
            
        </div>
        <div class="form-group col-xs-12 col-md-3">
           
                <label for="Subcontractorbill_subcontractor" class="required subcontractorlabel">Subcontractor <span class="required">*</span></label>
                <select class="form-control" id="subcontractor_id">
                    <option value="">Select Subcontractor</option>
                    <?php
                    if($is_update){
                            $subcontractor  = Yii::app()->db->createCommand("SELECT sc.subcontractor_id, sc.subcontractor_name FROM {$tblpx}subcontractor sc
                                        LEFT JOIN {$tblpx}scquotation sq ON sc.subcontractor_id = sq.subcontractor_id
                                        WHERE sq.project_id = '{$project_id}' AND sq.company_id = '{$company_id}' GROUP BY sc.subcontractor_id")->queryAll();

                            foreach ($subcontractor as $data) {
                                if ($data["subcontractor_id"] == $subcontractor_id)
                                    $selected = " selected";
                                else
                                    $selected = "";
                                echo "<option value='" . $data["subcontractor_id"] . "'" . $selected . ">" . $data["subcontractor_name"] . "</option>";
                            }
                        }
                            ?>
                </select>
                <div class="errorMessage" id="subcontractorErrorMsg"></div>
           
        </div>
        <div class="form-group col-xs-12 col-md-3">
            
                <?php echo $form->labelEx($model, 'scquotation_id'); ?>
                <select class="form-control" name="Subcontractorbill[scquotation_id]" id="Subcontractorbill_scquotation_id">
                    <option value="">Select Quotation</option>
                    <?php
                    if($is_update){
                            $quotationdata  = Scquotation::model()->findAll(array('select' => 'scquotation_id,scquotation_no,scquotation_decription', 'condition' => "project_id = '" . $project_id . "' AND subcontractor_id = '" . $subcontractor_id . "' AND company_id = '" . $company_id . "'"));

                            foreach ($quotationdata as $data) {
                                if ($data->scquotation_id == $model->scquotation_id)
                                    $selected = " selected";
                                else
                                    $selected = "";
                                echo "<option value='" . $data->scquotation_id . "'" . $selected . ">" . $data->scquotation_no . "</option>";
                            }
                        }
                            ?>
                </select>
                <?php echo $form->error($model, 'scquotation_id'); ?>
           
        </div>
       <div class="form-group col-xs-12 col-md-3">
           
                <?php echo $form->labelEx($model, 'bill_number'); ?>
                <?php echo $form->textField($model, 'bill_number', array('size' => 60, 'maxlength' => 100, 'class' => 'form-control')); ?>
                <?php echo $form->error($model, 'bill_number'); ?>
           
        </div>
        <div class="form-group col-xs-12 col-md-3">
           
                <?php echo $form->labelEx($model, 'date'); ?>
                <?php echo $form->textField($model, 'date', array('class' => 'form-control', 'autocomplete' => 'off')); ?>
                <?php echo $form->error($model, 'date'); ?>
            
        </div>
        <div class="form-group col-xs-12 col-md-3">
                                        
                <?php                         
                echo $form->labelEx($model,'total_amount'); ?> 
                <?php echo $form->textField($model, 'total_amount', array('class' => 'form-control total_amount', 'autocomplete' => 'off','readonly'=>'readonly')); ?>                                                  
           
        </div>
        <div class="form-group col-xs-12 col-md-3">
            
                <?php echo $form->labelEx($model, 'remarks'); ?>
              <?php echo $form->textArea($model, 'remarks', array(
                    'size' => 60, 
                    'maxlength' => 200, 
                    'class' => 'form-control', 
                    'rows' => 3, 
                    'cols' => 50
                )); ?>
                <?php echo $form->error($model, 'remarks'); ?>
            
        </div>
       <div class="form-group col-xs-12 col-md-3">
           
                <?php echo $form->labelEx($model, 'requested_by'); ?>
                <?php echo $form->textField($model, 'requested_by', array('size' => 60, 'maxlength' => 100, 'class' => 'form-control')); ?>
                <?php echo $form->error($model, 'requested_by'); ?>
          
        </div>
    </div>
    <div class="row" id="quotation_items" name="quotation_items">
    
    </div>
    <div class="row">
        <div class="form-group col-xs-12 text-right">
            
                <label for="Subcontractorbill_button">&nbsp;</label>
                <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-primary', 'id' => 'submitButton')); ?>
               <?php if(!$is_update){ ?>
                 <input type="reset" value="Reset" class="btn btn-default" id="reset" />
                <?php } ?>
                <?php //echo CHtml::ResetButton('Reset', array('style'=>'margin-right:3px;','class'=>'btn btn-default')); 
                ?>
           
        </div>
    </div>
   
                <?php $this->endWidget(); ?>

</div><!-- form -->
<?php if($is_update){ ?>
    </div>
    <?php } ?>
   
<script type="text/javascript">
    $(document).ready(function() {
        $("#Subcontractorbill_company_id").select2();
        $("#project_id").select2();
        $("#subcontractor_id").select2();
        $("#Subcontractorbill_scquotation_id").select2();
        $("#Subcontractorbillitem_quotationitem_id").select2();
        $(function() {
            $("#Subcontractorbill_date").datepicker({
                dateFormat: 'dd-mm-yy'
            });
        });
//
<?php if( $is_update){ ?>
var quotation_id = $("#Subcontractorbill_scquotation_id").val();
            var subcontractor_id = $("#subcontractor_id").val();
            var project_id = $("#project_id").val();
            var company_id = $("#Subcontractorbill_company_id").val();
            var bill_id =$("#bill_id").val();
            $.ajax({
                url: "<?php echo $this->createUrl('subcontractorbill/getscquotationitems') ?>",
                data: {
                    "scquotation_id": quotation_id,
                    "is_update":"1",
                    "bill_id": bill_id,
                },
                type: "POST",
                success: function(data) {
                    $("#quotation_items").html(data);
                }
            });
       <?php } ?>
        //
        $("#Subcontractorbill_company_id").change(function() {
            $('#loading').show();
            var company_id = $(this).val();
            $.ajax({
                url: "<?php echo $this->createUrl('subcontractorbill/getprojectbycompany') ?>",
                data: {
                    "company_id": company_id
                },
                type: "POST",
                success: function(data) {
                    $("#project_id").html(data);
                }
            });
        });
        $("#project_id").change(function() {
            $('#loading').show();
            var project_id = $(this).val();
            var company_id = $("#Subcontractorbill_company_id").val();
            $.ajax({
                url: "<?php echo $this->createUrl('subcontractorbill/getsubcontractorbyproject') ?>",
                data: {
                    "company_id": company_id,
                    "project_id": project_id
                },
                type: "POST",
                success: function(data) {
                    $("#subcontractor_id").html(data);
                }
            });
        });
        $("#subcontractor_id").change(function() {
            $('#loading').show();
            var subcontractor_id = $(this).val();
            var project_id = $("#project_id").val();
            var company_id = $("#Subcontractorbill_company_id").val();
            $.ajax({
                url: "<?php echo $this->createUrl('subcontractorbill/getquotationsbyproject') ?>",
                data: {
                    "company_id": company_id,
                    "project_id": project_id,
                    "subcontractor_id": subcontractor_id
                },
                type: "POST",
                success: function(data) {
                    $("#Subcontractorbill_scquotation_id").html(data);
                }
            });
        });

        $("#Subcontractorbill_scquotation_id").change(function() {
            $('#loading').show();
            var quotation_id = $("#Subcontractorbill_scquotation_id").val();
           // alert(quotation_id);
            var subcontractor_id = $("#subcontractor_id").val();
            var project_id = $("#project_id").val();
            var company_id = $("#Subcontractorbill_company_id").val();
            $.ajax({
                url: "<?php echo $this->createUrl('subcontractorbill/getscquotationitems') ?>",
                data: {
                    "scquotation_id": quotation_id,
                    "is_update":0,
                },
                type: "POST",
                success: function(data) {
                    $("#quotation_items").html(data);
                    $.ajax({
                url: "<?php echo $this->createUrl('subcontractorbill/getscquotationbillnumber') ?>",
                data: {
                    "company_id": company_id,
                    "project_id": project_id,
                    "subcontractor_id": subcontractor_id,
                    "scquotation_id": quotation_id
                },
                type: "POST",
                success: function(data) {
                    if(data !== ''){
                    $("#Subcontractorbill_bill_number").val(data);
                    $("#Subcontractorbill_bill_number").attr('title',data);
                    $("#Subcontractorbill_bill_number").attr('readonly','readonly');
                    }
                   
                }
            });
                }
            });
        });

        $("#Subcontractorbill_stage_id").change(function() {
            $('#loading').show();
            var quotation_id = $("#Subcontractorbill_scquotation_id").val();
            var stage_id = $(this).val();
            
            $.ajax({
                url: "<?php echo $this->createUrl('subcontractorbill/GetpaymentstageAmount') ?>",
                data: {
                    "stage_id": stage_id,                    
                    "quotation_id":quotation_id
                },
                type: "POST",
                success: function(data) {
                    $(".total_amount").val(data);
                }
            });
        });
      

        
        $("#submitButton").click(function() {
            var project_id = $("#project_id").val();
            var subcontractor_id = $("#subcontractor_id").val();
            if (project_id == "") {
                $("#projectErrorMsg").text("Project cannot be blank.");
                $(".projectlabel").css("color", "red");
            }
            if (subcontractor_id == "") {
                $("#subcontractorErrorMsg").text("Subcontractor cannot be blank.");
                $(".subcontractorlabel").css("color", "red");
            }
        });
    });
    $('#reset').on('click', function() {
        $("#Subcontractorbill_company_id").val('').trigger('change');
        $("#project_id").val('').trigger('change');
        $("#subcontractor_id").val('').trigger('change');
        $("#Subcontractorbill_scquotation_id").val('').trigger('change');
        $("#Subcontractorbillitem_quotationitem_id").val('').trigger('change');
        $("#Subcontractorbill_remarks").html('');
    });
    $(document).ajaxComplete(function() {
        $('.loading-overlay').removeClass('is-active');
        $('#loading').hide();
    });

    $("#Subcontractorbill_bill_number").change(function() {
            $('#loading').show();
            var bill_number = $("#Subcontractorbill_bill_number").val();
           $.ajax({
                url: "<?php echo $this->createUrl('subcontractorbill/billnumberexistance') ?>",
                data: {
                    "bill_number": bill_number,
                },
                type: "POST",
                dataType:"json",
                success: function(data) {
                    if(data>0){
                        if (confirm("Bill number already exist.Do you want to continue?")) {
                            $("#Subcontractorbill_bill_number").val(bill_number);
                        }else{
                            $("#Subcontractorbill_bill_number").val('');
                        }
                    }
                   
                }
            });
        });
        $("#billView").click(function(){
    let redirecturl=$(this).attr("href");
    window.location.href=redirecturl;
  });
</script>