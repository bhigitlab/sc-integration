<div id="items_holder" class="position-relative items_holder panel">
    <div class="panel-heading">
        <h5 class="bold">Sub Section</h5>
    </div>
    <div class="quotationmaindiv panel-body" id="items_list">
        <div class="add_new_sec ">                        
            <div class="h-head">
                <div class="row">
                    <div class="col-md-2">
                        <label class="bold"></label>   
                        <input type="hidden" id="category_label_id" value="">                     
                        <?php
                        echo CHtml::textField('', $main_cat_model['category_label'], array('class' => 'form-control require', 'id' => 'category_label', 'placeholder' => 'Sub Section'));
                        
                        ?>
                        <div class="errorMessage"></div>
                    </div>                                    
                    <div class="col-md-2">
                        <?php
                        echo CHtml::dropDownList('',$main_cat_model['unit'], 
                        CHtml::listData(Unit::model()->findAll(), 'id', 'unit_name'), 
                        array('class' => 'form-control js-example-basic-single   require ', 
                        'empty' => '-Select Unit-', 'name' => 'unit', 'style' => 'width:100%', 'id' => 'cat_unit'));
                        ?>
                    </div>
                    <div class="col-md-1">
                        <?php echo CHtml::textField('',$main_cat_model['quantity'], array('class' => 'form-control allownumericdecimal quantity_sub', 'autocomplete' => 'off', 'placeholder' => 'qty', 'name' => 'quantity', 'id' => 'cat_quantity')); ?>
                       
                    </div>
                    <div class="col-md-1">
                        <?php echo CHtml::textField('',$main_cat_model['quantity_nos'], array('class' => 'form-control allownumericdecimal ', 'autocomplete' => 'off', 'placeholder' => 'qty nos', 'name' => 'quantity_nos', 'id' => 'cat_quantity_nos')); ?>
                        
                    </div>
                    <div class="col-md-1">
                        <?php echo CHtml::textField('',$main_cat_model['mrp'], array('class' => 'form-control allownumericdecimal ', 'autocomplete' => 'off', 'placeholder' => 'MRP', 'name' => 'mrp', 'id' => 'cat_mrp')); ?>
                        
                    </div>
                    <div class="col-md-2">
                        <?php echo CHtml::textField('',$main_cat_model['amount_after_discount'], array('class' => 'form-control allownumericdecimal amountafter-discount', 'autocomplete' => 'off', 'name' => 'amount_after_discount', 'placeholder' => 'amt after disc', 'id' => 'cat_amount_after_discount')); ?>
                        
                    </div>
                </div>
            </div>
        </div>
    </div> 
    <div class="add_wrktype toggle_btn1" title="Add Sub Section">        
        <i class="toggle-caret fa fa-plus"></i>
    </div> 
    <div class="worktype_label_sec px-3 mt-2" style="display:none;">
        <div class=" panel panel-default">
            <div class="panel-heading position-relative">
                <div class="clearfix">
                    <div class="pull-left">
                        <h5>Add Sub Items</h5>
                    </div>
                </div>
            </div>
            <div class="panel-body" style="background: #f8fbff;">
                <?php
                echo $this->renderPartial('_worktype_label_sec',
                    array('main_wtype_model' => $main_wtype_model, 'qid' => $qid, 
                    'rev' => $rev, 'status' => $status))
                ?>
                <div class="clearfix">
                    <div class="pull-right">
                        <button class="btn btn-primary btn-sm add_more_wtype">Add more Sub Item</button>
                        <button class="btn btn-danger btn-sm remove_wtype hide">Remove</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="" id="quotation_item_edit"></div>                                      
</div>