<?php
/* @var $this LogController */
/* @var $model JpLog */

$this->breadcrumbs=array(
	'Jp Logs'=>array('index'),
	'Manage',
);


Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#jp-log-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<style type="text/css">
@media (min-width: 1024px) {
    .navbar-right {
        float: right !important;
        margin-right: -20px;
    } 
}
</style>
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <h2>Delete Log</h2>
            <?php /*$this->widget('zii.widgets.grid.CGridView', array(
                    'id'=>'jp-log-grid',
                    'dataProvider'=>$model->search(),
                    'filter'=>$model,
                    'columns'=>array(
                            'log_id',
                            'log_data',
                            'log_table',
                            'log_primary_key',
                            'log_datetime',
                            'log_action',
                            /*
                            'log_action_by',
                            */
                            /*array(
                                    'class'=>'CButtonColumn',
                            ),
                    ),
            ));*/ ?>
            <div id="actionlog">
                <?php $this->widget('zii.widgets.CListView', array(
                    'dataProvider'=>$model->search(),
                    //'itemView'=>'_view',
                    'itemView' => '_deleteview', 'template' => '<div>{summary}{sorter}</div><div class="tblwrap scrollTable"><div class="container1"><table cellpadding="10" class="table  list-view sorter">{items}</table></div></div>{pager}',
                )); ?>
            </div>
        </div>
    </div>
</div>

