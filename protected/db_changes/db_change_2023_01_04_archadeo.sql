DELETE FROM `jp_unit_conversion` WHERE `item_id` NOT IN(SELECT `id` FROM `jp_specification`);

UPDATE `jp_unit_conversion`, `jp_specification` SET `jp_unit_conversion`.`conversion_unit` = `jp_specification`.`unit` WHERE `jp_unit_conversion`.`item_id` = `jp_specification`.id AND `jp_specification`.`unit` != `jp_unit_conversion`.`conversion_unit` ;

UPDATE `jp_billitem`, `jp_specification` SET `jp_billitem`.`billitem_unit` = `jp_specification`.`unit` WHERE jp_billitem.category_id = jp_specification.id AND jp_billitem.billitem_unit='Select Unit' ;

UPDATE `jp_billitem`, `jp_specification` SET `jp_billitem`.`billitem_unit` = `jp_specification`.`unit` WHERE jp_billitem.category_id = jp_specification.id AND jp_billitem.billitem_unit= "";

