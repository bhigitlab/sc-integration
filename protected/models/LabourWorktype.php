<?php

/**
 * This is the model class for table "{{labour_worktype}}".
 *
 * The followings are the available columns in table '{{labour_worktype}}':
 * @property integer $type_id
 * @property string $worktype
 * @property double $rate
 * @property integer $is_active
 */
class LabourWorktype extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{labour_worktype}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('worktype, rate,short_keyword	', 'required'),
            array('is_active', 'numerical', 'integerOnly'=>true),
            array('rate', 'numerical'),
            array('worktype', 'length', 'max'=>200),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('type_id, worktype, rate', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
           // 'type_id' => 'Type',
            'worktype' => 'Work Type',
            'rate' => 'Rate',
            'short_keyword'=> 'Short Keyword',
          //  'is_active' => 'Is Active',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('type_id',$this->type_id);
        $criteria->compare('worktype',$this->worktype,true);
        $criteria->compare('rate',$this->rate);
        $criteria->compare('is_active',$this->is_active);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination' => false
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return LabourWorktype the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}