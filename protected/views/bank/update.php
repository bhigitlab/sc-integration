<?php
/* @var $this BankController */
/* @var $model Bank */

$this->breadcrumbs=array(
	'Banks'=>array('index'),
	$model->bank_id=>array('view','id'=>$model->bank_id),
	'Update',
);
?>

<div class="panel panel-gray">
    <div class="panel-heading form-head">
        <h3 class="panel-title">Edit Bank</h3>
    </div>
        <?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
</div>