<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>


<?php
$tblpx = Yii::app()->db->tablePrefix;
$billId = $model['bill_id'];
$bill_details = Yii::app()->db->createCommand("select * FROM " . $tblpx . "bills WHERE bill_id=" . $billId)->queryRow();
$vendor = Vendors::model()->findByAttributes(array('vendor_id' => $model['vendor_id']));

?>

<div class="container">
    <div class="invoicemaindiv">
        <div class="header-container">
            <h3>PURCHASE RETURN DETAILS</h3>
            <div class="btn-container">
                <a id="download" class="save_btn2 pdf_excel btn btn-primary" title="SAVE AS PDF">
                    <i class="fa fa-file-pdf-o button-icon" aria-hidden="true"></i>
                </a>
                <a id="download" class="excelbtn2 pdf_excel btn btn-primary pull-right" title="SAVE AS EXCEL">
                    <i class="fa fa-file-excel-o button-icon" aria-hidden="true"></i>
                </a>
            </div>
        </div>
        <?php
        $id = $model['bill_id'];
        ?>


        <form id="pdfvals2" method="post" action="">
            <div class="row bill_view">
                <div class="col-md-2 col-sm-6 small_box"><label>BILL No:</label>
                    <?php echo $bill_details['bill_number']; ?>
                </div>
                <div class="col-md-2 col-sm-6 small_box"><label>RETURN No:</label>
                    <?php echo $model['return_number']; ?>
                </div>
                <div class="col-md-2 col-sm-6 small_box"><label>DATE:</label>
                    <?php echo date("d-m-Y", strtotime($model['return_date'])); ?>
                </div>
                <div class="col-md-2 col-sm-6 small_box"><label>Vendor:</label> <?php if ($vendor !== null): ?>
                        <?php echo $vendor->name; ?>
                    <?php else: ?>
                        Vendor not found
                    <?php endif; ?>
                </div>
            </div>

            <div class="table-responsive">
                <table class="table table-inverse">
                    <thead class="entry-table">
                        <tr>
                            <th>Sl.No</th>
                            <th>Specifications/Remark</th>
                            <th>Quantity</th>
                            <th>Unit</th>
                            <th>Rate</th>
                            <th>Amount</th>
                            <th>Discount</th>
                            <th>Discount (%)</th>
                            <th>CGST</th>
                            <th>CGST (%)</th>
                            <th>SGST</th>
                            <th>SGST (%)</th>
                            <th>IGST</th>
                            <th>IGST (%)</th>
                            <th>Total Tax</th>
                            <th>Total Tax (%)</th>
                            <th>Total</th>
                        </tr>
                    </thead>
                    <tbody class="addrow">
                        <?php
                        $i = 1;
                        foreach ($newmodel as $new) {
                            //extract($new);
                            if ($new['category_id']) {
                                $categorymodel = Specification::model()->findByPk($new['category_id']);
                                if ($categorymodel)
                                    $categoryName = $categorymodel->specification;
                                else
                                    $categoryName = "Not available";
                            } else {
                                $categoryName = $new['remark'];
                            }
                            ?>
                            <tr>
                                <td>
                                    <?php echo $i; ?>
                                </td>
                                <td>
                                    <?php echo $categoryName; ?>
                                </td>
                                <td class="quantity">
                                    <?php echo $new['returnitem_quantity']; ?>
                                </td>
                                <td>
                                    <?php echo $new['returnitem_unit'];
                                    ; ?>
                                </td>
                                <td class="rate">
                                    <?php echo number_format((float) $new['returnitem_rate'], 2, '.', ''); ?>
                                </td>
                                <td>
                                    <?php echo Controller::money_format_inr($new['returnitem_amount'], 2, 1); ?>
                                </td>
                                <td>
                                    <?php echo Controller::money_format_inr($new['returnitem_discountamount'], 2, 1); ?>
                                </td>
                                <td>
                                    <?php echo $new['returnitem_discountpercent']; ?>
                                </td>
                                <td>
                                    <?php echo Controller::money_format_inr($new['returnitem_cgst'], 2, 1); ?>
                                </td>
                                <td>
                                    <?php echo $new['returnitem_cgstpercent']; ?>
                                </td>
                                <td>
                                    <?php echo Controller::money_format_inr($new['returnitem_sgst'], 2, 1); ?>
                                </td>
                                <td>
                                    <?php echo $new['returnitem_sgstpercent']; ?>
                                </td>
                                <td>
                                    <?php echo Controller::money_format_inr($new['returnitem_igst'], 2, 1); ?>
                                </td>
                                <td>
                                    <?php echo $new['returnitem_igstpercent']; ?>
                                </td>
                                <td>
                                    <?php echo Controller::money_format_inr($new['returnitem_taxamount'], 2, 1); ?>
                                </td>
                                <td>
                                    <?php echo $new['returnitem_taxpercent']; ?>
                                </td>
                                <td>
                                    <?php echo Controller::money_format_inr(($new['returnitem_amount'] - $new['returnitem_discountamount']) + $new['returnitem_taxamount'], 2, 1); ?>
                                </td>
                            </tr>

                            <?php
                            $i++;
                        }
                        ?>
                    </tbody>
                </table>
            </div>
            <div class="row-data">
                <label>Remarks:</label>
                <span>
                    <?php echo CHtml::encode($model['remarks']); ?>
                </span>
            </div>
            <div class="row">
                <div
                    class="col-md-offset-8 col-md-4 col-sm-offset-8 col-sm-4 col-xs-offset-6 col-xs-6 text-right amount_values">
                    <div class="row-data"><label>Amount:</label> <span>
                            <?php echo Controller::money_format_inr($model['return_amount'], 2, 1); ?>
                        </span></div>
                    <div class="row-data"><label>Discount Amount:</label> <span>
                            <?php echo Controller::money_format_inr($model['return_discountamount'], 2, 1); ?>
                        </span></div>
                    <div class="row-data"><label>Tax Amount</label> <span>
                            <?php echo Controller::money_format_inr($model['return_taxamount'], 2, 1); ?>
                        </span></div>
                    <div class="row-data"><label>Total Amount</label> <span>
                            <?php echo Controller::money_format_inr($model['return_totalamount'], 2, 1); ?>
                        </span></div>
                </div>
            </div>
        </form>
    </div>
    <script>

        $('.save_btn2').click(function () {
            $("form#pdfvals2").attr('action', "<?php echo $this->createAbsoluteUrl('purchaseReturn/returnpdf', array('id' => $model->return_id)); ?>");
            $("form#pdfvals2").submit();
        });

        $('.excelbtn2').click(function () {

            $("form#pdfvals2").attr('action', "<?php echo $this->createAbsoluteUrl('purchaseReturn/returncsv', array('id' => $model->return_id)); ?>");
            $("form#pdfvals2").submit();

        });



    </script>

    <?php $url = Yii::app()->createAbsoluteUrl("invoice/getclientByProject"); ?>
    <?php Yii::app()->clientScript->registerScript('myscript', '
    $(document).ready(function(){
    $("#project").change(function(){
    var pro_id= $("#project").val();
    $.ajax({
       url: "' . $url . '",
        data: {"id": pro_id}, 
        //dataType: "json",
        type: "POST",
        success:function(data){
       // alert(data);
           $("#billto").val(data); 
            
        }
        
});

});
$(".flash-success").animate({opacity: 1.0}, 5000).fadeOut("slow");
  
    });
        '); ?>


</div>