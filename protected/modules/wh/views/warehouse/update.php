<?php
/* @var $this WarehouseController */
/* @var $model Warehouse */

$this->breadcrumbs = array(
    'Warehouses' => array('index'),
    $model->warehouse_id => array('view', 'id' => $model->warehouse_id),
    'Update',
);
?>
<div class="entries-wrapper">
    <div class="row">
        <div class="col-xs-12">
            <div class="heading-title">Edit Warehouse</div>
            <div class="dotted-line"></div>
        </div>
    </div>
    <?php
    $this->renderPartial('_form', array(
        'model' => $model,
        'auto_receipt_to_warehouse_status' => $auto_receipt_to_warehouse_status,
        'view_stocks_in_other_warehouse_status' => $view_stocks_in_other_warehouse_status
    ));
    ?>
</div>