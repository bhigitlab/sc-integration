
<?php
/* @var $this ProjectTypeController */
/* @var $model ProjectType */

$this->breadcrumbs=array(
	'User Roles'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);
/*$this->menu=array(
	array('label'=>'List UserRoles', 'url'=>array('index')),
	array('label'=>'Create UserRoles', 'url'=>array('create')),
); */
?>

<div class="modal-dialog modal-sm">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" onclick="javascript:window.location.reload()">&times;</button>
			<h4 class="modal-title">Edit User Role</h4>
		</div>
		<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
	</div>
	
	
</div>
