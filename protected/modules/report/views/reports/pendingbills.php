<?php if (filter_input(INPUT_GET, 'export')) { ?>
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/pdfstyle.css">

<?php } ?>
<div class="container">
    <div class="expenses-heading">
        
        <?php if (!filter_input(INPUT_GET, 'export')) { ?>
            <div class="clearfix">
                <button type="button" id="excel-btn" class="save_btn btn btn-info pull-right mt-0 mb-10 margin-right-5 button-icon" title="SAVE AS EXCEL" href="<?php echo Yii::app()->request->requestUri . "&export=csv"; ?>"
                    style="text-decoration: none; color: white;" class="btn btn-info">
                    <i class="fa fa-file-excel-o button-icon" aria-hidden="true"></i>
                </button>
                <button type="button" id="pdf-btn" class="btn btn-info pull-right mt-0 mb-10 margin-right-5" title="SAVE AS PDF" href="<?php echo Yii::app()->request->requestUri . "&export=pdf"; ?>"
                    style="text-decoration: none; color: white;" class="btn btn-info">
                    <i class="fa fa-file-pdf-o button-icon" aria-hidden="true"></i>
                </button>
                <h3>Pending Bills</h3>
            </div>
            
        <?php } ?>
    </div>
   
    <div class="table-wrapper margin-top-10" >
        <div id="table-wrapper"  class="table-height-adj">
            <table class="table total-table total-table-new" id="parent1">
                <thead class="entry-table">
                    <tr class="first-thead-row">
                        <th>Project</th>
                        <th>Date</th>
                        <th>Bill Number / Description</th>
                        <th width="20%">Total Amount</th>
                        <?php if (!filter_input(INPUT_GET, 'export')) { ?>
                            <th></th>
                        <?php } ?>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $sum = 0;
                    $projectid_array = array();
                    //echo "<pre>";print_R($data);die;
                    
                    $counts = array_count_values(
                        array_filter(array_column($data, 'project_id'))
                    );

                    ob_start();


                    foreach ($data as $key => $value) {
                        if (isset($counts[$value['project_id']])) {
                            $sum += $value['bill_totalamount'];
                            ?>
                            <tr>
                                <?php
                                $project_id = $value['project_id'];
                                if (!in_array($project_id, $projectid_array)) {
                                    array_push($projectid_array, $project_id); ?>
                                    <td
                                        rowspan="<?php echo isset($counts[$value['project_id']]) ? $counts[$value['project_id']] : 1; ?>">
                                        <?php
                                        $project = Projects::model()->findByPk($value['project_id']);
                                        echo $project->name;
                                        ?>
                                    </td>
                                <?php } ?>
                                <td>
                                    <?php echo Yii::app()->controller->changeDateForamt($value['bill_date']); ?>
                                </td>
                                <td>
                                    <?php
                                    $purchase = Purchase::model()->findByPk($value['purchase_id']);
                                    if (filter_input(INPUT_GET, 'export')) {
                                        echo $value['bill_number'];
                                    } else {
                                        if (isset($purchase->purchase_no) && ($purchase->purchase_no != NULL)) {

                                            echo CHtml::link(
                                                $value['bill_number'],
                                                'index.php?r=bills/view&id=' . $value['bill_id'],
                                                array('class' => 'link', 'target' => '_blank')
                                            );

                                        } else {
                                            echo CHtml::link(
                                                $value['bill_number'],
                                                'index.php?r=bills/billview&id=' . $value['bill_id'],
                                                array('class' => 'link', 'target' => '_blank')
                                            );
                                        }
                                    }

                                    ?>
                                </td>
                                <td class="text-right">
                                    <?php
                                    $tblpx = Yii::app()->db->tablePrefix;
                                    $data1 = Yii::app()->db->createCommand("SELECT SUM(billitem_amount) as bill_amount FROM {$tblpx}billitem WHERE bill_id=" . $value['bill_id'] . "")->queryRow();
                                    $data2 = Yii::app()->db->createCommand("SELECT SUM(billitem_taxamount) as bill_taxamount FROM {$tblpx}billitem WHERE bill_id=" . $value['bill_id'] . "")->queryRow();
                                    $data3 = Yii::app()->db->createCommand("SELECT SUM(billitem_discountamount) as bill_discountamount FROM {$tblpx}billitem WHERE bill_id=" . $value['bill_id'] . "")->queryRow();
                                    $addcharges = Yii::app()->db->createCommand("SELECT SUM(amount) as amount FROM " . $tblpx . "additional_bill WHERE `bill_id`=" . $value['bill_id'])->queryRow();
                                    $roundoff = Yii::app()->db->createCommand("SELECT round_off as round_off FROM {$tblpx}bills WHERE bill_id=" . $value['bill_id'] . "")->queryRow();

                                    $sql = "SELECT SUM(billitem_sgst)+SUM(billitem_cgst)+SUM(billitem_igst) "
                                        . " AS total_tax FROM `jp_billitem` "
                                        . " WHERE bill_id=" . $value['bill_id'];
                                    $total_tax = Yii::app()->db->createCommand($sql)->queryScalar();

                                    $net_amount = ($data1['bill_amount'] + $total_tax) - $data3['bill_discountamount'];
                                    $total_amount = ($net_amount + $addcharges['amount']) + $roundoff['round_off'];
                                    echo Yii::app()->Controller->money_format_inr($total_amount, 2); ?>
                                </td>
                                <?php
                                if (!filter_input(INPUT_GET, 'export')) { ?>
                                    <td width="10%">
                                        <?php
                                        echo CHtml::link(
                                            'Add Payment',
                                            'index.php?r=expenses/dailyEntriesofpending&bill_id=1,' . $value['bill_id'],
                                            array('class' => 'btn btn-default btn-sm', 'target' => '_blank')
                                        );
                                        ?>
                                    </td>
                                <?php } ?>
                            </tr>

                            <?php
                        }
                    }
                    $pending_bills = ob_get_contents();
                    ob_end_clean();
                    ?>
                    <?php
                    echo $pending_bills;

                    ?>
                    </tbody>

                    <tfoot class="entry-table">
                    <tr >
                        <th colspan="3" class="text-right"><b>Total</b></th>
                        <th class="text-right total_1">
                            <b>
                                <?php echo Yii::app()->Controller->money_format_inr($sum, 2); ?>
                            </b>
                        </th>
                        <th></th>
                    </tr>
                    </tfoot>
                
            </table>
        </div>
    </div>
    <br>
</div>
<script>
    $("#excel-btn").click(function(){
        var redirectUrl = $(this).attr("href");
        window.location.href=redirectUrl;
    });
    $("#pdf-btn").click(function(){
        var redirectUrl = $(this).attr("href");
        window.location.href=redirectUrl;
    });
</script>    