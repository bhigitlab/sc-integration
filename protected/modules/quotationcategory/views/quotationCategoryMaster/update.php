<?php
/* @var $this QuotationCategoryMasterController */
/* @var $model QuotationCategoryMaster */

$this->breadcrumbs = array(
	'Quotation Category Masters' => array('index'),
	$model->name => array('view', 'id' => $model->id),
	'Update',
);

// $this->menu=array(
// 	array('label'=>'List QuotationCategoryMaster', 'url'=>array('index')),
// 	array('label'=>'Create QuotationCategoryMaster', 'url'=>array('create')),
// 	array('label'=>'View QuotationCategoryMaster', 'url'=>array('view', 'id'=>$model->id)),
// 	array('label'=>'Manage QuotationCategoryMaster', 'url'=>array('admin')),
// );
// 
?>

<h1>Update QuotationCategoryMaster</h1>

<?php $this->renderPartial('_form', array('model' => $model)); ?>