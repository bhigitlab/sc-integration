<?php

class MaterialRequisitionController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array(
                'allow',  // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view'),
                'users' => array('*'),
            ),
            array(
                'allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array(
                    'create', 'update', 'adddItem', 'deleteMR',
                    'getRelatedUnit', 'getMRItems', 'updateItem',
                    'approveentry', 'reject_mr','addPurchaseData','getAccountsMr','delete','getDynamicMaterialReqNo','loadPoModal','loadPoItemsModal','loadbillModal','refreshButton','addPo','refreshItemButton','deleteMRData'
                ),
                'users' => array('@'),
            ),
            array(
                'allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('admin'),
            ),
            array(
                'deny',  // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $data = MaterialRequisitionItems::model()->findAll(array("condition" => "mr_id = '" . $id . "'", "order" => "id DESC"));
        $itemid='';
        $pms_api_integration_model=ApiSettings::model()->findByPk(1);
        $pms_api_integration =$pms_api_integration_model->api_integration_settings;
        $attributes = array('project_id' => null);
        
        $material_req_item_arr = [];
        foreach ($data as $req_items) {
            $item_id = $req_items->material_item_id;
            if ($item_id == 0) {
                $item_name =    $req_items->material_item_name;
            } else {
                
                $item_name = $req_items->material_item_name;
            }
            $item_id = $req_items->material_item_id;
            
            $po_id=$req_items->po_id;
            $purchase_no='';
            $po_no = Purchase::model()->findByPk($po_id);
            if(!empty($po_no)){
                $purchase_no=$po_no->purchase_no;
            }
            $unit="";
            $unit =$req_items->item_unit;
            $unit_det = Unit::Model()->findByPk($req_items->item_unit);
            if(!empty($unit_det)){
                $unit =$unit_det->unit_name; 
            }

            array_push($material_req_item_arr, [
                'item_name' => $item_name,
                'item_unit' => $unit,
                'item_quantity' => $req_items->item_quantity,
                'item_req_date' => $req_items->item_req_date,
                'item_remarks' => $req_items->item_remarks,
                'mr_item'=>$data,
                'mr_id'=>$id,
                'mr_item_id'=>$item_id,
                'item_po_id'=>$req_items->po_id,
                'item_po_no'=>$purchase_no,
                'items_table_id'=>$req_items->id
            ]);
        }

        $this->render('view', array(
            'model' => $this->loadModel($id),
            'material_req_item_arr' => $material_req_item_arr
        ));
    }

    public function actionAddPo($id)
    {
        $data = MaterialRequisitionItems::model()->findAll(array("condition" => "mr_id = '" . $id . "'", "order" => "id DESC"));
        $itemid='';
        $pms_api_integration_model=ApiSettings::model()->findByPk(1);
        $pms_api_integration =$pms_api_integration_model->api_integration_settings;
        $attributes = array('project_id' => null);
        
        $material_req_item_arr = [];
        foreach ($data as $req_items) {
            $item_id = $req_items->material_item_id;

            if ($item_id == '') {
                $item_name =  '';
            } else {
                $acc_material_model = Materials::model()->findByPk($item_id);
                   
                $item_name = $acc_material_model->material_name;
            }
            $po_id=$req_items->po_id;
            $purchase_no='';
            $po_no = Purchase::model()->findByPk($po_id);
            if(!empty($po_no)){
                $purchase_no=$po_no->purchase_no;
            }
            $unit="";
            $unit =$req_items->item_unit;
            $unit_det = Unit::Model()->findByPk($req_items->item_unit);
            if(!empty($unit_det)){
                $unit =$unit_det->unit_name; 
            }
        
            array_push($material_req_item_arr, [
                'item_name' => $item_name,
                'item_unit' => $unit,
                'item_quantity' => $req_items->item_quantity,
                'item_req_date' => $req_items->item_req_date,
                'item_remarks' => $req_items->item_remarks,
                'mr_item'=>$data,
                'mr_id'=>$id,
                'item_table_unique_id'=>$req_items->id,
                'item_pms_material_name'=>$req_items->pms_material_name,
                'mr_item_id'=>$item_id,
                'item_po_id'=>$req_items->po_id,
                'item_po_no'=>$purchase_no,
                'items_table_id'=>$req_items->id,
                'items_bill_id'=>$req_items->bill_id
            ]);
        }

        $this->render('add_po', array(
            'model' => $this->loadModel($id),
            'material_req_item_arr' => $material_req_item_arr,
            'pms_api_integration' =>$pms_api_integration,
        ));
    }


    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate($id='',$type)
    {
        $model = new MaterialRequisition('search');
        $model->unsetAttributes();
        $tblpx = Yii::app()->db->tablePrefix;
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['req_no'])) {
            $model = new MaterialRequisition;
            $model->project_id = $_POST['project_id'];
            $model->company_id = $_POST['company_id'];
            $model->task_description = $_POST['task_id'];
            $model->requisition_no = $_POST['req_no'];
            $model->requisition_status = 1;
            $model->type_from = 2;
            $model->created_by = yii::app()->user->id;
            $model->created_date = date('Y-m-d');
            $model->updated_by = yii::app()->user->id;
            $model->updated_date = date('Y-m-d');
            if ($model->save()) {
                $json_data = array('status' => 1, 'mr_id' => $model->requisition_id);
                echo  json_encode($json_data);
                exit;
            }else{
                $json_data = array('status' =>2);
                echo  json_encode($json_data);
                exit; 
            }
        }
        
        $task = Tasks::model()->findByPK($id);
        $mr_count = count(MaterialRequisition::model()->findAll()) + 1;
        $unqid = str_pad($mr_count, 6, '0', STR_PAD_LEFT);
        $requisition_no = 'MR' . $unqid;

        $account_items = $this->getAccountItems();
        $unit_sql = "SELECT * FROM jp_unit";
        $unit_list = Yii::app()->db->createCommand($unit_sql)->queryAll();

        // $mr_for_task = MaterialRequisition::model()->findAll(array('condition' => 'requisition_task_id=' . $id));

        $mr_for_task ='';

        if (isset($_GET['MaterialRequisition']))
            $model->attributes = $_GET['MaterialRequisition'];

        $project = Yii::app()->db->createCommand("SELECT pid, name  FROM {$tblpx}projects ORDER BY name")->queryAll();
        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
         $newQuery = "";
        foreach ($arrVal as $arr) {
            if ($newQuery) $newQuery .= ' OR';
                $newQuery .= " FIND_IN_SET('" . $arr . "', id)";
            }
        $company = Company::model()->findAll(array('condition' => $newQuery));
        $this->render('create', array(
            'model' => $model, 
            'project' => $project,
            'requisition_no' => $requisition_no, 
            'project_id' => '',
            'mr_for_task' => $mr_for_task,
            'account_items' => $account_items,
            'unit_list' => $unit_list,
            'company'=>$company,
            'type'=>$type
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);
        
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['MaterialRequisition'])) {
            $model->attributes = $_POST['MaterialRequisition'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->requisition_id));
        }
        $mr = $task = MaterialRequisition::model()->findByPK($id);
        // $task = Tasks::model()->findByPK($mr->requisition_task_id);
        // $task_name = $task->title;
        $project = Projects::model()->findByAttributes(array('pid' => $model->project_id));
        $project_name = $project->name;
        $account_items = $this->getAccountItems();
        $unit_sql = "SELECT * FROM jp_unit";
        $unit_list = Yii::app()->db->createCommand($unit_sql)->queryAll();
        $data = MaterialRequisitionItems::model()->findAll(array("condition" => "mr_id = '" . $id . "'", "order" => "id DESC"));
        $material_req_item_arr = [];
        foreach ($data as $req_items) {
            $item_id = $req_items->item_id;
            if ($item_id == 0) {
                $item_name =    $req_items->item_name;
            } else {
                $specsql = "SELECT id, cat_id,brand_id, specification, unit "
                    . " FROM jp_specification "
                    . " WHERE id=" . $item_id . "";
                $specification  = Yii::app()->db->createCommand($specsql)->queryRow();
                $cat_sql = "SELECT * FROM jp_purchase_category "
                    . " WHERE id='" . $specification['cat_id'] . "'";
                $parent_category = Yii::app()->db->createCommand($cat_sql)->queryRow();

                if ($specification['brand_id'] != NULL) {
                    $brand_sql = "SELECT brand_name "
                        . " FROM jp_brand "
                        . " WHERE id=" . $specification['brand_id'] . "";
                    $brand_details = Yii::app()->db->createCommand($brand_sql)->queryRow();
                    $brand = '-' . $brand_details['brand_name'];
                } else {
                    $brand = '';
                }
                $item_name = $parent_category['category_name'] . $brand . '-' . $specification['specification'];
            }

            array_push($material_req_item_arr, [
                'item_name' => $item_name,
                'item_unit' => $req_items->item_unit,
                'item_quantity' => $req_items->item_quantity,
                'item_req_date' => $req_items->item_req_date,
                'item_remarks' => $req_items->item_remarks

            ]);
        }

        $items_model = new MaterialRequisitionItems('search');

        $this->render('update', array(
            'model' => $model,
            // 'task_name' => $task_name,
            'project_name' => $project_name,
            'project_id' => $model->project_id,
            // 'task_name' => $task_name,
            // 'task_id' => $task->tskid,
            'account_items' => $account_items,
            'unit_list' => $unit_list,
            'material_req_item_arr' => $material_req_item_arr,
            'items_model' => $items_model,
            'id' => $id

        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    
    public function actionIndex()
    {        
        $model = new MaterialRequisition('search');
        $pms_api_integration_model=ApiSettings::model()->findByPk(1);
        $pms_api_integration =$pms_api_integration_model->api_integration_settings;
        $model->unsetAttributes();
        if (isset($_GET['MaterialRequisition']))
            $model->attributes = $_GET['MaterialRequisition'];
        $this->render('index', array(
            'model' => $model,
            'pms_api_integration'=>$pms_api_integration
        ));
    }


    public function actionGetAccountsMr()
    {        
        $model = new MaterialRequisition('searchaccounts');
        $model->unsetAttributes();
        if (isset($_GET['MaterialRequisition']))
            $model->attributes = $_GET['MaterialRequisition'];
        $this->render('index1', array(
            'model' => $model,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model = new MaterialRequisition('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['MaterialRequisition']))
            $model->attributes = $_GET['MaterialRequisition'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        $model = MaterialRequisition::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'material-requisition-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
    public function actionadddItem()
    {
        $model = new MaterialRequisitionItems();
        $model->mr_id = $_POST['material_req_id'];
        $material_item_name= $_POST['requisition_item_name'];
        if ($_POST['requisition_item'] == 'other') {

            $model->material_item_id = 0;
        } else {
            $model->material_item_id = $_POST['requisition_item'];
        }
        if($material_item_name == ''){
            $material = Materials::model()->findByPk($_POST['requisition_item']);
            if(!empty($material)){
                $material_item_name=$material->material_name;
            }

        }

        $model->material_item_name =$material_item_name;
        
        $model->item_unit = $_POST['requisition_unit'];
        $model->item_quantity = $_POST['requisition_quantity'];
        $model->item_req_date = date('Y-m-d', strtotime($_POST['requisition_date']));
        $model->item_remarks = $_POST['requisition_remarks'];
        $model->created_by = yii::app()->user->id;
        $model->created_date = date('Y-m-d');
        $model->updated_by = yii::app()->user->id;
        $model->updated_date = date('Y-m-d');
        $model->save();
        $data = MaterialRequisitionItems::model()->findAll(array("condition" => "mr_id = '" . $_POST['material_req_id'] . "'", "order" => "id DESC"));
        $material_req_item_arr = [];
        foreach ($data as $req_items) {
            $item_id = $req_items->material_item_id;
            if ($item_id == 0) {
                $item_name =    $req_items->material_item_name;
            } else {
                $item_name =    $req_items->material_item_name;
            }

            array_push($material_req_item_arr, [
                'item_name' => $item_name,
                'item_unit' => $req_items->item_unit,
                'item_quantity' => $req_items->item_quantity,
                'item_req_date' => $req_items->item_req_date,
                'item_remarks' => $req_items->item_remarks

            ]);
        }

        $result = $this->renderPartial('material_req_items', array(
            'model' => $model,
            'data' => $material_req_item_arr
        ), true);
        echo json_encode($result);
    }
    public function actiondeleteMR()
    {
        $mr_id = $_POST['mr_id'];
        MaterialRequisitionItems::model()->deleteAll('mr_id = :mr_id', array(':mr_id' => $mr_id));
        MaterialRequisition::model()->deleteAll('requisition_id = :mr_id', array(':mr_id' => $mr_id));

        $json_data = array('status' => 1);
        echo  json_encode($json_data);
    }
    public function actiondeleteMRData($mr_id)
    {
       $mr_id=$_REQUEST['mr_id'];
        $attributes = array('mr_id'=>$mr_id);
        $mr=MaterialRequisition::model()->findByPk($mr_id);
        if(!empty($mr)){
            if(!empty($mr->purchase_id)){
                $json_data = array('status' => 0, 'message' => 'Cannot delete: linked to a purchase order.');
                echo json_encode($json_data);
                return; // Stop further execution
                exit;
            }else{
                    // If no items with po_id IS NOT NULL, proceed with deletion
                MaterialRequisitionItems::model()->deleteAll('mr_id = :mr_id', array(':mr_id' => $mr_id));
                MaterialRequisition::model()->deleteAll('requisition_id = :mr_id', array(':mr_id' => $mr_id));

                $json_data = array('status' => 1, 'message' => 'Successfully deleted.');
                    echo json_encode($json_data);
                    return;
                }
        }
        $json_data = array('status' => 2, 'message' => 'Successfully deleted.');
        echo json_encode($json_data);
        return;
        
        

               
    }
    public function getAccountItems()
    {
        $data = array();
    $final = array();

    $material_sql = "SELECT * "
        . " FROM jp_materials ";
    $materials = Yii::app()->db->createCommand($material_sql)->queryAll();

    foreach ($materials as $key => $value) {
        

        $final[$key]['data'] = ucwords($value['material_name']);
        $final[$key]['id'] = $value['id'];
    }

    return $final;
    }
    public function GetParent($id)
    {

        $category = Yii::app()->db->createCommand("SELECT parent_id, id , category_name FROM jp_purchase_category WHERE id=" . $id . "")->queryRow();
        return $category;
    }
    public function actiongetRelatedUnit()
    {
        $item_id = $_POST['item_id'];
        $html['html'] = '';
        $html['unit'] = '';
        if ($item_id != '' && $item_id != 'other') {
            $unit_purch_list = Yii::app()->db->createCommand("SELECT material_unit FROM jp_materials WHERE id=" . $item_id)->queryRow();
            
            if ($unit_purch_list) {
                $material_units = explode(',', $unit_purch_list['material_unit']);
                $units_placeholder = implode(',', array_fill(0, count($material_units), '?'));
                $unit_sql = "SELECT id,unit_name FROM jp_unit WHERE id IN ($units_placeholder)";
                $unit_list = Yii::app()->db->createCommand($unit_sql)->queryAll(true, $material_units);

                $html['html'] .= '<option value="" >Select Unit</option>';
                foreach ($unit_list as $key => $value) {
                    $html['html'] .= '<option value="' . $value['id'] . '" >' . $value['unit_name'] . '</option>';
                }

                $html['unit'] = implode(',', array_column($unit_list, 'unit_name'));
            } else {
                $html['html'] .= '<option value="" >Select Unit</option>';
            }
        } else {
            $unit_sql = "SELECT * FROM jp_unit";
            $unit_list = Yii::app()->db->createCommand($unit_sql)->queryAll();
            $html['html'] .= '<option value="" >Select Unit</option>';
            foreach ($unit_list as $key => $value) {
                $html['html'] .= '<option value="' . $value['id'] . '" >' . $value['unit_name'] . '</option>';
            }
        }
        echo json_encode($html);
    }

    public function actiongetMRItems()
    {
        if (!empty($_POST['id'])) {
            $id = $_POST['id'];
            $model = MaterialRequisitionItems::model()->findByPk($id);
            if (!empty($model)) {
                $model_array = array(
                    'stat' => 1, 'id' => $model->id,
                    'item_id' => $model->item_id, 'item_name' => $model->item_name,
                    'item_unit' => $model->item_unit,
                    'item_date' => date('d-M-y', strtotime($model->item_req_date)),
                    'item_qty' => $model->item_quantity,
                    'item_remarks' => $model->item_remarks

                );
            } else {
                $model_array = array('stat' => 0);
            }
        } else {
            $model_array = array('stat' => 0);
        }
        echo  json_encode($model_array);
        exit;
    }
    public function actionupdateItem()
    {

        $id = $_POST['id'];

        if ($id == "") {
            $model = new MaterialRequisitionItems;
            $type = 0;
            $model->created_by = yii::app()->user->id;
            $model->created_date = date('Y-m-d H:i:s');
        } else {
            $model = MaterialRequisitionItems::model()->findByPk($id);
            $type = 1;
            $model->updated_by = yii::app()->user->id;
            $model->updated_date = date('Y-m-d H:i:s');
        }

        if ($_POST['requisition_item'] == 'other') {

            $model->item_id = 0;
        } else {
            $model->item_id = $_POST['requisition_item'];
        }
        $model->mr_id = $_POST['material_req_id'];
        $model->item_name = $_POST['requisition_item_name'];
        $model->item_unit = $_POST['requisition_unit'];
        $model->item_quantity = $_POST['requisition_quantity'];
        $model->item_req_date = date('Y-m-d', strtotime($_POST['requisition_date']));
        $model->item_remarks = $_POST['requisition_remarks'];


        if ($model->save()) {
            $response = array('status' => 1, 'type' => $type);
        } else {
            $response = array('status' => 0, 'type' => $type);
        }
        echo json_encode($response);
    }
    public function actionapproveEntry()
    {
        $id = $_POST['id'];
        $model = MaterialRequisition::model()->findByPk($id);
        $model->requisition_status = 1;
        $model->approved_rejected_by = yii::app()->user->id;
        if ($model->save()) {
            $response = array('status' => 1, 'message' => "Succcessfully Approved");
        } else {
            $response = array('status' => 0, 'message' => 'An error occured');
        }
        echo json_encode($response);
    }
    public function actionreject_mr()
    {

        if (isset($_POST['reason'])) {

            $id = $_POST['mr_id'];
            $model = MaterialRequisition::model()->findByPk($id);
            $model->requisition_status = 2;
            $model->reject_reason = $_POST['reason'];
            $model->approved_rejected_by = yii::app()->user->id;
            if ($model->save()) {
                $response = array('status' => 1, 'message' => "Rejected Succcessfully");
            } else {
                $response = array('status' => 0, 'message' => "Something went wrong");
            }
            echo  json_encode($response);
            exit;
        }


        $result = $this->renderPartial('_reject_mr', array(
            'id' => $_POST['id']
        ), true);
        echo json_encode($result);
    }

    public function actionaddPurchaseData(){
        $id = $_POST['req_id'];
        $model = MaterialRequisition::model()->findByPk($id);
        $model->purchase_id = $_POST['po_number'];
        if ($model->save()) {
            $response = array('status' => 'success', 'message' => "Po Added Succcessfully");
        } else {
            $response = array('status' => 'danger', 'message' => "Something went wrong");
        }
        echo  json_encode($response);
        exit;
    }
    public function actionGetDynamicMaterialReqNo(){
        $mr_no='';
        $status=0;
         if(isset($_POST['company_id'])){
            
            $tblpx = Yii::app()->db->tablePrefix;
            $company_id=$_POST['company_id'];
            $company = Company::model()->findByPk($company_id);
            if(!empty($company->material_requisition_fomat )){
                $material_requisition_fomat =trim($company["material_requisition_fomat"]);
                $general_mr_format_arr = explode('/',$material_requisition_fomat);
                // print_r($general_mr_format_arr);exit;
                $org_prefix = $general_mr_format_arr[0];
                $prefix_key = 0;
                $mr_key= '';
                $current_key ='';
                $status = 1;
                foreach($general_mr_format_arr as $key =>$value){
                    
                    if($value == '{mr_sequence}'){
                        $mr_key = $key;
                    }else if ($value == '{year}'){
                        $current_key =$key;
                    }
                }
                
                $current_year = date('Y');
                $previous_year = date('Y') -1;
                $next_year = date('Y') +1;
                
                $where = '';
                if($current_key == 2){
                    $where .= " AND `requisition_no` LIKE '%$current_year'";
                }else if($current_key == 1){
                    $where .= " AND `requisition_no` LIKE '%$current_year%'";
                }
                $sql = "SELECT * from pms_material_requisition  WHERE `requisition_no` LIKE '$org_prefix%' $where  AND  company_id = ". $company_id." ORDER BY `requisition_id` DESC LIMIT 1;" ;
                //echo "<pre>";print_r($sql);exit;
                $previous_mr_no='';
                $mr_seq_no = 1;
                $mr_seq_prefix ='MR-' ;
                $previous_data = Yii::app()->db->createCommand($sql)->queryRow();
                if( !empty($previous_data) ){
                    $previous_mr_no = $previous_data['requisition_no'];
                    $prefix_arr = explode('/', $previous_mr_no);
                    $prefix = $prefix_arr[0];
                    $previous_prefix_seq_no = $prefix_arr[$mr_key];
                    $mr_split = explode('-',$previous_prefix_seq_no);
                    $previous_prefix_seq_no_val =$mr_split[1];
                    $mr_seq_no =  $previous_prefix_seq_no_val + 1;
                                   
                }
                $digit = strlen((string) $mr_seq_no);
               
                if ($digit == 1) {
                    $mr_no =$mr_seq_prefix.'00' .$mr_seq_no;
                } else if ($digit == 2) {
                    $mr_no = $mr_seq_prefix.'0' . $mr_seq_no;
                } else {
                    $mr_no =$mr_seq_prefix.$mr_seq_no;
                }
                    $status=1;
              
               
                $new_invoice = str_replace('{year}',$current_year,$material_requisition_fomat);
                $new_mr_no = str_replace('{mr_sequence}',$mr_no,$new_invoice);
                //die($new_mr_no);
                $mr_no = $new_mr_no;
                
            } 
            
            

            $result = array( "status"=>$status,"mr_no"=>$mr_no);
            echo json_encode( $result);

         }
        
    }
    public function actionLoadPoModal($mrId='',$selectedItems=''){
        $selectedItems='';
       $this->layout = '//layouts/blank';
       $model='';
       $pid = '';
        $vendor = '';
        $project = '';
        $expense_head = '';
       $mrId=$_GET['mrId'];
       if( isset($_GET['mrId']) ){
        if(isset($_GET['selectedItems'])){
            $selectedItems=$_GET['selectedItems'];
        }
        $mrId=$_GET['mrId'];
        
      
        $materialreq = MaterialRequisition::model()->findByPk($mrId) ;
        // if(!empty($materialreq->purchase_id)){
        // $purchasemodel = Purchase::model()->findByPk($materialreq->purchase_id);
        // $tblpx  = Yii::app()->db->tablePrefix;
        // $pid =$materialreq->purchase_id;
        // $model =  Purchase::model()->find(array("condition" => "p_id = '$materialreq->purchase_id'"));
        // $tblpx      = Yii::app()->db->tablePrefix;
        // $projects = Projects::model()->findByPk($materialreq->project_id);
        // $arrVal = explode(',', $projects->company_id);
        // $newQuery = "";
        // $newQuery1 = "";
        // $newQuery2 = "";
        // $newQuery3 = "";
        // foreach ($arrVal as $arr) {
        //     if ($newQuery)
        //         $newQuery .= ' OR';
        //     if ($newQuery1)
        //         $newQuery1 .= ' OR';
        //     if ($newQuery1)
        //         $newQuery2 .= ' OR';
        //     if ($newQuery1)
        //         $newQuery3 .= ' OR';
        //     $newQuery .= " FIND_IN_SET('" . $arr . "', company_id)";
        //     $newQuery1 .= " FIND_IN_SET('" . $arr . "', p.company_id)";
        //     $newQuery2 .= " FIND_IN_SET('" . $arr . "', {$tblpx}expense_type.company_id)";
        //     $newQuery3 .= " FIND_IN_SET('" . $arr . "', v.company_id)";
        // }
        
        // //echo "<pre>"; print_r($project);exit;
        // $vendor = array();
        // $expense_head = array();
        // if (isset($model->project_id)) {
        //     $expense_head = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}project_exptype LEFT JOIN {$tblpx}expense_type ON {$tblpx}project_exptype.type_id={$tblpx}expense_type.type_id WHERE {$tblpx}project_exptype.project_id=" . $model->project_id)->queryAll();
        // }
        // if (isset($model->expensehead_id)) {
        //     $vendor = Yii::app()->db->createCommand("SELECT v.vendor_id as vendorid, v.name as vendorname FROM " . $tblpx . "vendors v LEFT JOIN " . $tblpx . "vendor_exptype vet ON v.vendor_id = vet.vendor_id WHERE vet.type_id = " . $model->expensehead_id . " AND ($newQuery3)")->queryAll();
        // }
        
        // $this->render('//purchase/_purchase_details_form', array(
        //     'model' => $model,
        //     'p_id' => $pid,
        //     'vendor' => $vendor,
        //     'project' => $projects,
        //     'expense_head' => $expense_head,
        //     'mrId'=>$mrId,
        //     'selectedItems'=>$selectedItems
        // ));
        // }else{
            
            $purchaseDataProvider = Purchase::model()->search(); // You may adjust the search criteria as per your requirements
            if( isset($_GET['mrId']) ){
                if(isset($_GET['selectedItems'])){
                    $selectedItems=$_GET['selectedItems'];
                }
                $mrId=$_GET['mrId'];
                $materialreq = MaterialRequisition::model()->findByPk($mrId) ;
                $project = Projects::model()->findByPk($materialreq->project_id);
               
            }
            $this->render('//purchase/_purchase_details_form', array('purchaseDataProvider' => $purchaseDataProvider,
            'model' => $model,
            'pid' => $pid,
            'vendor' => $vendor,
            'project' => $project,
            'expense_head' => $expense_head,
            'mrId'=>$mrId,
            'selectedItems'=>$selectedItems
            ));
        //}
       }
       
    
    }
    public function actionRefreshButton(){
       
        $attributes = array('project_id' => null);
        $materialRequisitions = MaterialRequisition::model()->findAllByAttributes($attributes);
            if(!empty($materialRequisitions)){
                //echo "<pre>";print_r($materialRequisitions);exit;
                foreach($materialRequisitions as $materialRequisition){
                    $pms_project_id_from_mr = $materialRequisition->requisition_project_id;
                    $acc_project_model = Projects::model()->findByAttributes(array('pms_project_id' => $pms_project_id_from_mr));
                   
                    if ($acc_project_model !== null) {
                       
                        $acc_project_id = $acc_project_model->pid;
                        $materialRequisition->project_id = $acc_project_id;
                        $materialRequisition->update();
                    }
                }
            }
        $this->redirect(Yii::app()->createUrl('MaterialRequisition/index'));
    }
    public function actionRefreshItemButton($mrId=''){
        if(isset($_REQUEST['mrId']) && !empty($_REQUEST['mrId'])){
            $mrId= $_REQUEST['mrId'];
            $attributes = array('material_item_id' => null ,'mr_id'=>$mrId);
            $materialRequisitions = MaterialRequisitionItems::model()->findAllByAttributes($attributes);
            if(!empty($materialRequisitions)){
                foreach($materialRequisitions as $materialRequisition){
                    $pms_material_id_from_mr = $materialRequisition->pms_material_id;
                    $acc_material_model = Materials::model()->findByAttributes(array('pms_material_id' => $pms_material_id_from_mr));
                    
                    $pms_unit_id_from_mr = $materialRequisition->pms_unit;
                    $acc_unit_model = Unit::model()->findByAttributes(array('pms_unit_id' => $pms_unit_id_from_mr));
                    if ($acc_material_model !== null) {
                       
                        $acc_material_id = $acc_material_model->id;
                        $materialRequisition->material_item_id = $acc_material_id;
                        $materialRequisition->update();
                    }  
                    $acc_material_unit_id='';
                    if ($acc_unit_model !== null) {
                       
                        $acc_material_unit_id = $acc_unit_model->id;
                        if(!empty($acc_material_unit_id)){
                            $materialRequisition->item_unit = $acc_material_unit_id;
                            $materialRequisition->update();
                        }
                    }   
            }    }   
            $this->redirect(array('MaterialRequisition/addPo', 'id' => $mrId));
        }
    }

    
}
