<?php

class EquipmentsController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		  $accessArr = array();
        $accessauthArr = array();
        $controller = Yii::app()->controller->id;
        if (isset(Yii::app()->user->menuall)) {
            if (array_key_exists($controller, Yii::app()->user->menuall)) {
                $accessArr = Yii::app()->user->menuall[$controller];
            }
        }

        if (isset(Yii::app()->user->menuauth)) {
            if (array_key_exists($controller, Yii::app()->user->menuauth)) {
                $accessauthArr = Yii::app()->user->menuauth[$controller];
            }
        }
        $access_privlg = count($accessauthArr);
        return array(
            array(
                'allow', // allow all users to perform 'index' and 'view' actions
                'actions' => $accessArr,
                'users' => array('@'),
                'expression' => "$access_privlg > 0",
            ),

			array(
                'allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('delete', 'ratedecline','Viewremarks','addremarks','Dynamicexpensehead','Dynamicvendor','additionalCharge','deleteadditionalCharge','Dynamiccompany','loadPoItemsModal','getProjectByMrId'),
                'users' => array('*'),
            ),
            array(
                'deny',  // deny all users
                'users' => array('*'),
            ),
        );
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model = new Equipments; // Assuming Equipment is your model for equipment

		if (isset($_POST['Equipments'])) {
			$model->attributes = $_POST['Equipments'];

			// Check if equipment_unit is set and is an array
			if (isset($model->equipment_unit) && is_array($model->equipment_unit)) {
				// Convert the array to a comma-separated string
				$model->equipment_unit = implode(',', $model->equipment_unit);
			}

			if ($model->save()) {
				//API integration
				$pms_api_integration=ApiSettings::model()->pmsIntegrationStatus();
                if($pms_api_integration ==1){
                    $request = [
                        'origin' => 'coms',
                        'equipment_id' =>$model->id,
                        'name' => $model->equipment_name,
                        'coms_api_log_id' => $model->id,
                        'units' => $model->equipment_unit,
                    ];

                    $slug = "api/create-equipment";
                    $method="POST";
                    $globalapi_response = GlobalApiCall::callApi($slug, $method, $request);
                }
				$this->redirect(array('index'));
			}
		}

		$this->renderPartial('create', array(
			'model' => $model,
		));
	}
	
	public function actionCheckDuplicateEquipmentName() {
		if (Yii::app()->request->isPostRequest && isset($_POST['equipment_name'])) {
			$equipmentName = $_POST['equipment_name'];
			$isDuplicate = Equipments::model()->exists('equipment_name=:equipment_name', array(':equipment_name' => $equipmentName));
			
			// Ensure proper JSON response format
			echo CJSON::encode(array('isDuplicate' => $isDuplicate));
			Yii::app()->end();
		}
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model = $this->loadModel($id); 

		if (isset($_POST['Equipments'])) {
			$old_equipment_unit = $model->equipment_unit;
			$model->attributes = $_POST['Equipments'];

			// Check if equipment_unit is set and is an array
			if (isset($model->equipment_unit) && is_array($model->equipment_unit)) {
				// Convert the array to a comma-separated string
				$model->equipment_unit = implode(',', $model->equipment_unit);
			}

			if ($model->save()) {
				//API integration
				$pms_api_integration=ApiSettings::model()->pmsIntegrationStatus();
				if ($_POST['execute_api'] == 1) {
				if($pms_api_integration ==1){
					$request = [
						'origin' => 'coms',
                        'equipment_id' =>$model->id,
                        'name' => $model->equipment_name,
                        'units' => $model->equipment_unit,
						'pms_equipment_id'=>$model->pms_equipment_id
					];

					if (!empty($model->pms_equipment_id )) {

						$new_equipment_unit = $model->equipment_unit; 
						if (!is_array($new_equipment_unit)) {
							if (is_string($new_equipment_unit)) {
								$new_equipment_unit = explode(',', $new_equipment_unit);
							} else {
								$new_equipment_unit = [];
							}
						}
						if (!is_array($old_equipment_unit)) {
							if (is_string($old_equipment_unit)) {
								$old_equipment_unit = explode(',', $old_equipment_unit);
							} else {
								$old_equipment_unit = [];
							}
						}
						$remove_unit = array_diff($old_equipment_unit, $new_equipment_unit);
						$add_unit = array_diff($new_equipment_unit, $old_equipment_unit);

						$remove_unit = implode(',', $remove_unit);
						$add_unit = implode(',', $add_unit);
						
						$removeUnit=explode(',', $remove_unit);
						$addUnit=explode(',', $add_unit);
						$request['remove_units'] = implode(',', $removeUnit);
						$request['add_units'] = implode(',', $addUnit);
						$slug = "api/update-equipment";
						
					} else {
						$slug = "api/create-equipment";
					}
					$method="POST";
					$globalapi_response = GlobalApiCall::callApi($slug, $method, $request);
				}
			    }
				$this->redirect(array('index'));
			}
		}

		// Ensure equipment_unit is handled as an array for multiple selection in the form
		if (is_string($model->equipment_unit)) {
			$model->equipment_unit = explode(',', $model->equipment_unit);
		} else {
			$model->equipment_unit = [];
		}

		$this->render('update', array(
			'model' => $model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$model = $this->loadModel($id);

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if ($model->delete()) {
			$response = [
				'success' => 'success',
				'message' => 'Record deleted successfully.'
			];
		} else {
			throw new Exception(json_encode($model->getErrors()));
		
		}
		echo json_encode($response);
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new Equipments('search');
		$this->render('index',array(
			'dataProvider'=>$dataProvider->search(),
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Equipments('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Equipments']))
			$model->attributes=$_GET['Equipments'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Equipments the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Equipments::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Equipments $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='equipments-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
