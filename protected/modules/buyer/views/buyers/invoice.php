<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.js"></script>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/message/css/jquery.message.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/message/js/jquery.message.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
<script>
    var shim = (function() {
        document.createElement('datalist');
    })();
</script>
<style>
    .lefttdiv {
        float: left;
    }
</style>
<script>
    $(function() {
        $("#datepicker").datepicker({
            dateFormat: 'dd-mm-yy'
        }).datepicker("setDate", new Date());

    });
</script>
<style>
    .dropdown {
        position: relative;
        display: inline-block;
    }

    .dropdown::before {
        position: absolute;
        content: " \2193";
        top: 0px;
        right: -8px;
        height: 20px;
        width: 20px;
    }

    button#caret {
        border: none;
        background: none;
        position: absolute;
        top: 0px;
        right: 0px;
    }

    .invoicemaindiv th,
    .invoicemaindiv td {
        padding: 10px;
        text-align: left;
    }

    .addcolumn {
        cursor: pointer;
        background-color: black;
        border: none;
        color: white;
        text-align: center;
        text-decoration: none;
        margin: 2px;
        font-size: 30px;
        width: 40px;
    }

    .invoicemaindiv .pull-right,
    .invoicemaindiv .pull-left {
        float: none !important;
    }


    .checkek_edit {
        pointer-events: none;
    }



    .add_selection {
        background: #000;
    }




    .toast-container {
        width: 350px;
    }

    .toast-position-top-right {
        top: 57px;
        right: 6px;
    }

    .toast-item-close {
        background-image: none;
        cursor: pointer;
        width: 12px;
        height: 12px;
        text-align: center;
        border-radius: 2px;
    }

    .toast-item-image {
        font-size: 24px;
    }

    .toast-item-close:hover {
        color: red;
    }

    .toast-item {
        border: transparent;
        border-radius: 3px;
        font-size: 10px;
        opacity: 1;
        background-color: rgba(34, 45, 50, 0.8);
    }

    .toast-item-wrapper p {
        margin: 0px 5px 0px 42px;
        font-size: 14px;
        text-align: justify;
    }


    .toast-type-success {
        background-color: #00A65A;
        border-color: #00A65A;
    }

    .toast-type-error {
        background-color: #DD4B39;
        border-color: #DD4B39;
    }

    .toast-type-notice {
        background-color: #00C0EF;
        border-color: #00C0EF;
    }

    .toast-type-warning {
        background-color: #F39C12;
        border-color: #F39C12;
    }

    .span_class {
        min-width: 70px;
        display: inline-block;
    }

    .purchase-title {
        border-bottom: 1px solid #ddd;
    }

    .purchase_items h3 {
        font-size: 18px;
        color: #333;
        margin: 0px;
        padding: 0px;
        text-align: left;
        padding-bottom: 10px;
    }

    .purchase_items h4 {
        margin: 0px 15px 10px 15px !important;
        line-height: 24px;
        background-color: #fafafa;
        font-size: 16px;
    }

    .form-fields .form-control {
        height: 28px;
        display: inline-block;
        padding: 6px 3px;
        width: 60%;
    }

    .purchaseitem .form-control {
        height: 28px;
        display: inline-block;
        padding: 6px 3px;
    }

    .purchaseitem label {
        display: block;
    }

    .purchase_items {
        padding: 15px;
        box-shadow: 0 0 13px 1px rgba(0, 0, 0, 0.25);
        margin-bottom: 15px;
    }

    .purchaseitem {
        display: inline-block;
        margin-right: 20px;
    }

    .purchaseitem last-child {
        margin-right: 0px;
    }

    .purchaseitem label {
        font-size: 12px;
    }

    .remark {
        display: none;
    }

    .padding-box {
        padding: 3px 0px;
        min-height: 17px;
        display: inline-block;
    }

    th {
        height: auto;
    }

    .quantity,
    .rate,
    .small_class {
        max-width: 80px;
    }

    .p_block {
        margin-bottom: 10px;
        padding: 0px 15px;
    }

    .gsts input {
        width: 50px;
    }

    .hiden {
        display: none;
    }

    .amt_sec {
        float: right;
    }

    .amt_sec:after,
    .padding-box:after {
        display: block;
        content: '';
        clear: both;
    }

    .client_data {
        margin-top: 6px;
    }

    *:focus {
        border: 1px solid #333;
        box-shadow: 0 0 6px 1px rgba(0, 0, 0, 0.25);
    }

    .toottip-hiden {
        width: auto;
    }

    .form-fields {
        margin-bottom: 10px;
    }

    @media(max-width: 767px) {
        .purchaseitem {
            display: block;
            padding-bottom: 5px;
            margin-right: 0px;
        }

        .quantity,
        .rate,
        .small_class {
            width: auto !important;
        }

        .p_block {
            margin-bottom: 0px;
        }

        .hiden {
            display: block !important;
        }

        .padding-box {
            float: right;
        }

        #tax_amount,
        #item_amount {
            display: inline-block;
            float: none;
            text-align: right;
        }

        span.select2.select2-container.select2-container--default.select2-container--focus {
            width: auto !important;
        }

        span.select2.select2-container.select2-container--default.select2-container--below {
            width: auto !important;
        }

        .form-fields .form-control {
            width: 100%;
        }
    }

    @media(min-width: 767px) {
        .invoicemaindiv .pull-right {
            float: right !important;
        }

        .invoicemaindiv .pull-left {
            float: left !important;
        }
    }
</style>

<div class="container">

    <div class="invoicemaindiv">

        <h2 class="purchase-title">Buyer Invoice</h2>
        <br>
        <div id="msg_box"></div>
        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'pdfvals1',
            'enableAjaxValidation' => false,
            'action' => 'buyer/buyers/addinvoice',
        ));
        ?>
        <input type="hidden" name="remove" id="remove" value="">
        <input type="hidden" name="invoice_id" id="invoice_id" value="0">
        <div class="row form-fields">
            <div class="col-md-3 col-sm-6">
                <label>COMPANY : </label>
                <?php
                echo $form->dropDownList($model, 'company_id', $company_list, array('class' => 'inputs target company_id form-control', 'empty' => '-Select Company-'));
                ?>
            </div>
            <div class="col-md-3 col-sm-6">
                <label for="project">Buyer</label>
                <?php
                echo $form->dropDownList($model, 'buyer_id', $buyer_list, array('class' => 'inputs target company_id form-control', 'empty' => '-Select Buyer-'));
                ?>
            </div>
            <div class="col-md-3 col-sm-6">
                <label>PROJECT : </label>
                <?php
                echo $form->dropDownList($model, 'project_id', $buyer_list, array('class' => 'inputs target company_id form-control', 'empty' => '-Select Project-', 'id' => 'project_id'));
                ?>
                <input type="hidden" id="BuyerInvoice_project_id" name="BuyerInvoice[project_id]">
            </div>
            <div class="col-md-3 col-sm-6">
                <label for="project">Apartment No</label>
                <?php
                echo $form->dropDownList($model, 'flat_id', $buyer_list, array('class' => 'inputs target company_id form-control', 'empty' => '-Select Apartment No-', 'id' => 'flat_id'));
                ?>
                <input type="hidden" id="BuyerInvoice_flat_id" name="BuyerInvoice[flat_id]">
            </div>
        </div>
        <div class="row form-fields">
            <div class="col-md-3 col-sm-6">
                <label>DATE : </label>
                <input type="text" value="<?php echo ((isset($date) && $date != '') ? date("d-m-Y", strtotime($date)) : ''); ?>" id="datepicker" class="txtBox date inputs target form-control" name="BuyerInvoice[date]" placeholder="Please click to edit">
            </div>
            <div class="col-md-3 col-sm-6">
                <label>Invoice No : </label>
                <!-- onkeypress="filterDigits(event)" -->
                <?php echo $form->textField($model, 'invoice_no', array('class' => 'txtBox inputs target check_type inv_no form-control', 'id' => 'inv_no_')); ?>
                <input type="hidden" id="BuyerInvoice_invoice_no" name="BuyerInvoice[invoice_no]">
            </div>
        </div>

        <div class="clearfix"></div>
        <br><br>
        <div id="msg"></div>

        <div id="previous_details" style="position: fixed;top: 94px; left: 70px; right: 70px; z-index: 2; max-width:1150px; margin:0 auto;"></div>

        <div class="purchase_items">
            <h4>Invoice Entries</h4>
            <div class="p_block clearfix">
                <div class="purchaseitem ">
                    <label>Description:</label>
                    <input type="text" class="inputs target txtBox description form-control" id="description" name="BuyerInvoiceList[description][]" placeholder="" /></td>
                </div>
                <div class="purchaseitem ">
                    <label>Transaction type:</label>
                    <?php
                    $transaction_type = array('1' => 'Sales', '2' => 'Journal');
                    echo $form->dropDownList($invoicelist, 'transaction_type', $transaction_type, array('class' => 'form-control js-example-basic-single', 'empty' => '-Select Transaction Type-'));
                    ?>
                </div>
                <div class="purchaseitem quantity_div">
                    <label>Amount:</label>
                    <input type="text" class="inputs target txtBox amount allownumericdecimal form-control" id="amount" name="BuyerInvoiceList[amount][]" placeholder="" /></td>
                </div>
                <div class="purchaseitem amt_sec">
                    <label>&nbsp;</label>
                    <b>Amount: </b>
                    <div id="item_amount" class="padding-box">0.00</div>
                </div>
            </div>
            <div class="p_block clearfix">

                <div class="purchaseitem gsts">
                    <label>SGST (%):</label>
                    <input type="text" class="inputs target calculation small_class txtBox sgst allownumericdecimal form-control" id="sgst" name="BuyerInvoiceList[sgst_p][]" placeholder="" />
                    <div id="sgst_amount" class="padding-box">0.00</div>
                </div>

                <div class="purchaseitem gsts">
                    <label>CGST (%):</label>
                    <input type="text" class="inputs target calculation txtBox cgst small_class allownumericdecimal form-control" id="cgst" name="BuyerInvoiceList[cgst_p][]" placeholder="" />
                    <div id="cgst_amount" class="padding-box">0.00</div>
                </div>

                <div class="purchaseitem gsts">
                    <label>IGST (%):</label>
                    <input type="text" class="inputs target calculation txtBox igst  small_class allownumericdecimal form-control" id="igst" name="BuyerInvoiceList[igst_p][]" placeholder="" />
                    <div id="igst_amount" class="padding-box">0.00</div>
                </div>
                <div class="purchaseitem quantity_div">
                    <label>Discount Amount:</label>
                    <input type="text" class="inputs target txtBox disc_amount allownumericdecimal form-control" id="discount" name="BuyerInvoiceList[discount_amount][]" placeholder="" /></td>
                </div>
                <div class="purchaseitem amt_sec">
                    <label>&nbsp;</label>
                    <b>Total Amount:</b>
                    <div id="subtotal_amt" class="padding-box">0.00</div>
                </div>
                <div class="purchaseitem amt_sec">
                    <label>&nbsp;</label>
                    <b>Tax Amount: </b>
                    <div id="tax_amount" class="padding-box">0.00</div>
                </div>
            </div>
            <div class="p_block clearfix">
                <div class="purchaseitem">
                    <input type="button" class="item_save btn btn-info" id="0" value="Save">
                </div>
            </div>
        </div>
    </div>


    <div id="table-scroll" class="table-scroll">
        <div id="faux-table" class="faux-table" aria="hidden"></div>
        <div id="table-wrap" class="table-wrap">
            <div class="table-responsive">
                <table border="1" class="table">
                    <thead>
                        <tr>
                            <th>Sl.No</th>
                            <th>Description</th>
                            <th>Transaction Type</th>
                            <th>SGST (%)</th>
                            <th>SGST Amount</th>
                            <th>CGST (%)</th>
                            <th>CGST Amount</th>
                            <th>IGST (%)</th>
                            <th>IGST Amount</th>
                            <th>Amount</th>
                            <th>Tax Amount</th>
                            <th>Discount Amount</th>
                            <th>Subtotal Amount</th>


                            <th></th>
                        </tr>
                    </thead>
                    <tbody class="addrow">

                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th colspan="9" class="text-right">TOTAL: </th>
                            <th class="text-right">
                                <div id="grand_total">0.00</div>
                            </th>
                            <th class="text-right">
                                <div id="grand_taxamount">0.00
                            </th>
                            <th class="text-right">
                                <div id="grand_disc">0.00
                            </th>
                            <th></th>
                            <th></th>
                        </tr>

                        <tr>
                            <th colspan="12" class="text-right">GRAND TOTAL: <div id="net_amount">0.00</div>
                            </th>
                            <th></th>
                            <th></th>
                        </tr>
                    </tfoot>

                </table>
            </div>


        </div>
    </div>


    <div style="padding-right: 0px;" class="col-md-offset-9 col-md-3 col-sm-offset-8 col-sm-4 col-xs-offset-6 col-xs-6">

        <input type="hidden" value="<?php echo ((isset($subtot) && $subtot != '') ? $subtot : ''); ?>" class="txtBox pastweek" readonly=ture name="subtot" /></td>
        <input type="hidden" value="<?php echo ((isset($grand) && $grand != '') ? $grand : ''); ?>" class="txtBox pastweek grand" name="grand" readonly=true />

    </div>
    <br><br>
    <?php $this->endWidget(); ?>
</div>



<input type="hidden" name="final_amount" id="final_amount" value="0">

<input type="hidden" name="item_amount_temp" id="item_amount_temp" value="0">

<input type="hidden" name="final_tax" id="final_tax" value="0">

<input type="hidden" name="item_tax_temp" id="item_tax_temp" value="0">


<style>
    .error_message {
        color: red;
    }

    a.pdf_excel {
        background-color: #6a8ec7;
        display: inline-block;
        padding: 8px;
        color: #fff;
        border: 1px solid #6a8ec8;
    }
</style>
<?php $getBuyerList = Yii::app()->createAbsoluteUrl("/buyer/default/getBuyers"); ?>
<?php $getprojecstUrl = Yii::app()->createAbsoluteUrl("/buyer/default/getProjects"); ?>
<?php $getProjectAndFlats = Yii::app()->createAbsoluteUrl("/buyer/default/getProjectAndFlats"); ?>
<?php $generateInvoiceIdUrl = Yii::app()->createAbsoluteUrl("/buyer/default/generateInvoice"); ?>
<?php $createInvoiceUrl = Yii::app()->createAbsoluteUrl("/buyer/buyers/createInvoice");
$addInvoiceItemUrl = Yii::app()->createAbsoluteUrl("/buyer/buyers/addInvoiceItem");
$updateInvoiceItemUrl = Yii::app()->createAbsoluteUrl("/buyer/buyers/updateInvoiceItem");
$removeItemUrl = Yii::app()->createAbsoluteUrl("/buyer/buyers/removeinvoiceItem");
?>

<script type="text/javascript">
    /**************************** */
    $(document).ready(function() {
        getProjectData();
        $("#BuyerInvoice_company_id").change(function() {
            var company_id = $("#BuyerInvoice_company_id").val();
            $("#BuyerInvoice_buyer_id").val('').trigger('change.select2');
            $("#project_id").val('').trigger('change.select2');
            $("#BuyerInvoice_project_id").val('');
            $("#flat_id").val('').trigger('change.select2');
            $("#BuyerInvoice_flat_id").val('');
            if (company_id !== "") {
                getBuyerList(company_id);
                getProjectData();
                saveInvoice();
            }
        });
        $("#BuyerInvoice_buyer_id").change(function() {
            var buyer_id = $("#BuyerInvoice_buyer_id").val();
            var company_id = $("#BuyerInvoice_company_id").val();
            if (buyer_id !== "") {
                setProjectFlatData(buyer_id, company_id);

            }
        });
        $("#flat_id").change(function() {
            var flat_id = $(this).val();
            $('#BuyerInvoice_flat_id').val(flat_id);
            saveInvoice();
        });
        $('.date').change(function() {
            saveInvoice();
        });
        $('.item_save').click(function() {
            $("#previous_details").hide();
            var element = $(this);
            var item_id = $(this).attr('id');
            if (item_id == 0) {
                var description = $('#description').val();
                var cgst = $('#cgst').val();
                var cgst_amount = $('#cgst_amount').html();
                var sgst = $('#sgst').val();
                var sgst_amount = $('#sgst_amount').html();
                var igst = $('#igst').val();
                var igst_amount = $('#igst_amount').html();
                var tax_amount = $('#tax_amount').html();
                var subtotal_amount = $('#subtotal_amt').text();
                var disc_amt = $('#discount').val();
                var amount = $('#amount').val();
                var transaction_type = $('#BuyerInvoiceList_transaction_type').val();

                var company = $('#BuyerInvoice_company_id').val();
                var project = $('input[name="BuyerInvoice[project_id]"]').val();
                var date = $('input[name="BuyerInvoice[date]"]').val();
                var inv_no = $('input[name="BuyerInvoice[invoice_no]"]').val();
                var buyer_id = $('input[name="BuyerInvoice[buyer_id]"]').val();
                var flat_id = $('input[name="BuyerInvoice[flat_id]"]').val();
                var rowCount = $('.table .addrow tr').length;


                if (company == '' || buyer_id == '' || project == '' || flat_id == '' || date == '' || inv_no == '') {
                    $().toastmessage('showErrorToast', "Please enter sales details");

                } else {
                    if ((moment(date, 'DD-MM-YYYY', true).isValid())) {

                        if (description != '' && amount != '' && amount > 0) {
                            howMany += 1;
                            if (howMany == 1) {
                                var invoice_id = $('input[name="invoice_id"]').val();
                                // var subtot = $('#grand_total').text();
                                var grand = $('#grand_total').text();
                                var grand_tax = $('#grand_taxamount').text();
                                var data = {
                                    'sl_no': rowCount,
                                    'description': description,
                                    'amount': amount,
                                    'sgst': sgst,
                                    'sgst_amount': sgst_amount,
                                    'cgst': cgst,
                                    'cgst_amount': cgst_amount,
                                    'igst': igst,
                                    'igst_amount': igst_amount,
                                    'tax_amount': tax_amount,
                                    'invoice_id': invoice_id,
                                    'grand': grand,
                                    'subtotal_amount': subtotal_amount,
                                    'disc_amt': disc_amt,
                                    'grand_tax': grand_tax,
                                    'transaction_type': transaction_type
                                };
                                $.ajax({
                                    url: '<?php echo $addInvoiceItemUrl; ?>',
                                    type: 'POST',
                                    dataType: 'json',
                                    data: {
                                        data: data
                                    },
                                    success: function(response) {
                                        if (response.response == 'success') {
                                            $('#grand_total').html(response.final_amount);
                                            $('#grand_disc').html(response.disc_total);
                                            $('#grand_taxamount').html(response.final_tax);
                                            $('#net_amount').html(response.grand_total);
                                            $().toastmessage('showSuccessToast', "" + response.msg + "");
                                            $('.addrow').html(response.html);
                                            $('#invoice_div').text(response.invoice_no);
                                            $("#invoice_div").load(location.href + " #invoice_div");
                                        } else {
                                            $().toastmessage('showErrorToast', "" + response.msg + "");

                                        }
                                        howMany = 0;
                                        //$('#description').val('');
                                        $('#description').val('').trigger('change');
                                        $('#remarks').val('');
                                        $('#quantity').val('');
                                        $('#unit').val('');
                                        $('#amount').val('');
                                        $('#item_amount').html('');
                                        $('#tax_amount').html('0.00');
                                        $('#sgst').val('');
                                        $('#sgst_amount').html('0.00');
                                        $('#cgst').val('');
                                        $('#cgst_amount').html('0.00');
                                        $('#igst').val('');
                                        $('#igst_amount').html('0.00');
                                        $('#description').focus();
                                        $('#subtotal_amt').html('0.00');
                                        $('#discount').val('');
                                    }
                                });

                            }
                        } else {
                            $().toastmessage('showErrorToast', "Please fill the description and amount");
                        }
                    } else {
                        $(this).focus();
                        $().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
                        $(this).focus();
                    }
                }
            } else {
                // update 
                var description = $('#description').val();
                var cgst = $('#cgst').val();
                var cgst_amount = $('#cgst_amount').html();
                var sgst = $('#sgst').val();
                var sgst_amount = $('#sgst_amount').html();
                var igst = $('#igst').val();
                var igst_amount = $('#igst_amount').html();
                var tax_amount = $('#tax_amount').html();
                var amount = $('#amount').val();
                var subtotal_amount = $('#subtotal_amt').text();
                var disc_amt = $('#discount').val();
                var transaction_type = $('#BuyerInvoiceList_transaction_type').val();

                var company = $('#BuyerInvoice_company_id').val();
                var project = $('input[name="BuyerInvoice[project_id]"]').val();
                var date = $('input[name="BuyerInvoice[date]"]').val();
                var inv_no = $('input[name="BuyerInvoice[invoice_no]"]').val();
                var buyer_id = $('input[name="BuyerInvoice[buyer_id]"]').val();
                var flat_id = $('input[name="BuyerInvoice[flat_id]"]').val();
                var company = $('#company').val();
                if (company == '' || buyer_id == '' || project == '' || flat_id == '' || date == '' || inv_no == '') {
                    $().toastmessage('showErrorToast', "Please enter invoice details");
                } else {

                    if ((moment(date, 'DD-MM-YYYY', true).isValid())) {
                        if (description != '' && amount != '' && amount > 0) {
                            howMany += 1;
                            if (howMany == 1) {
                                var invoice_id = $('input[name="invoice_id"]').val();
                                // var subtot = $('#grand_total').text();
                                var grand = $('#grand_total').text();
                                var grand_tax = $('#grand_taxamount').text();
                                var data = {
                                    'item_id': item_id,
                                    'sl_no': sl_no,
                                    'description': description,
                                    'amount': amount,
                                    'sgst': sgst,
                                    'sgst_amount': sgst_amount,
                                    'cgst': cgst,
                                    'cgst_amount': cgst_amount,
                                    'igst': igst,
                                    'igst_amount': igst_amount,
                                    'tax_amount': tax_amount,
                                    'invoice_id': invoice_id,
                                    'grand': grand,
                                    'disc_amt': disc_amt,
                                    'subtotal_amount': subtotal_amount,
                                    'grand_tax': grand_tax,
                                    'transaction_type': transaction_type
                                };
                                $.ajax({
                                    url: '<?php echo $updateInvoiceItemUrl; ?>',
                                    type: 'POST',
                                    dataType: 'json',
                                    data: {
                                        data: data
                                    },
                                    success: function(response) {
                                        if (response.response == 'success') {
                                            $('#grand_total').html(response.final_amount);
                                            $('#grand_taxamount').html(response.final_tax);
                                            $('#net_amount').html(response.grand_total);
                                            $().toastmessage('showSuccessToast', "" + response.msg + "");
                                            $('.addrow').html(response.html);
                                        } else {
                                            $().toastmessage('showErrorToast', "" + response.msg + "");
                                        }
                                        $('#description').val('').trigger('change');
                                        $('#BuyerInvoiceList_transaction_type').val('').trigger('change');
                                        $('#remarks').val('');
                                        $('#quantity').val('');
                                        $('#unit').val('');
                                        $('#amount').val('');
                                        $('#item_amount').html('');
                                        $('#tax_amount').html('0.00');
                                        $('#sgst').val('');
                                        $('#sgst_amount').html('0.00');
                                        $('#cgst').val('');
                                        $('#cgst_amount').html('0.00');
                                        $('#igst').val('');
                                        $('#igst_amount').html('0.00');
                                        $('#description').focus();
                                        $('#item_amount_temp').val(0);
                                        $('#item_tax_temp').val(0);
                                        $(".item_save").attr('value', 'Save');
                                        $(".item_save").attr('id', 0);
                                        //$(".js-example-basic-single").select2("val", "");
                                        $('#description').focus();
                                        $('#subtotal_amt').html('0.00');
                                        $('#discount').val('');
                                        howMany = 0;
                                    }
                                });
                            }
                        } else {
                            $().toastmessage('showErrorToast', "Please fill the description and valid rate");
                        }
                    } else {
                        $(this).focus();
                        $().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
                        $(this).focus();
                    }
                }
            }
        });
    });


    function getBuyerList(company_id) {
        $.ajax({
            url: "<?php echo $getBuyerList; ?>",
            data: {
                "company_id": company_id
            },
            type: "POST",
            dataType: 'json',
            success: function(data) {
                if (company_id === "") {
                    $("#BuyerInvoice_buyer_id").val('').trigger('change.select2');
                } else {
                    $('#BuyerInvoice_buyer_id').empty();
                    $("#BuyerInvoice_buyer_id").prepend("<option value='' selected='selected'>-Select Buyer-</option>");
                    $.each(data.buyer_list, function(val, text) {
                        $('#BuyerInvoice_buyer_id').append($('<option></option>').val(val).html(text))
                    });

                }
            }
        });
    }

    function getProjectData() {
        var company_id = $("#BuyerInvoice_company_id").val();

        $.ajax({
            url: "<?php echo $getprojecstUrl; ?>",
            data: {
                "company_id": company_id
            },
            type: "POST",
            dataType: 'json',
            success: function(data) {

                if (company_id === "") {
                    $("#BuyerInvoice_project_id").val('').trigger('change.select2');
                } else {
                    $.each(data.project_list, function(val, text) {
                        $('#project_id').append($('<option></option>').val(val).html(text))
                    });
                }
            }
        });

    }

    function setProjectFlatData(buyer_id, company_id) {
        invoiceGenerate();
        $.ajax({
            url: "<?php echo $getProjectAndFlats; ?>",
            data: {
                "buyer_id": buyer_id,
                "company_id": company_id
            },
            type: "POST",
            dataType: 'json',
            success: function(data) {
                if (buyer_id === "") {
                    $("#project_id").val('').trigger('change.select2');
                    $('#BuyerInvoice_project_id').val('');
                    $("#flat_id").val('').trigger('change.select2');
                    $("#BuyerTransactions_flat_id").val('');
                } else {
                    $("#project_id").val(data.project_id).trigger('change.select2');
                    $('#BuyerInvoice_project_id').val(data.project_id);
                    $("#project_id").attr("disabled", true);
                    if (data.flat_count === 1) {
                        $.each(data.flat_list, function(val, text) {
                            $('#flat_id').append($('<option selected="selected"></option>').val(val).html(text))
                            $("#BuyerInvoice_flat_id").val(val);
                        });
                        $("#flat_id").attr("disabled", true);
                    } else {
                        $('#flat_id').empty();
                        $("#flat_id").prepend("<option value='' selected='selected'>-Select Apartment No-</option>");
                        $("#flat_id").attr("disabled", false);
                        $.each(data.flat_list, function(val, text) {
                            $('#flat_id').append($('<option></option>').val(val).html(text))
                        });
                    }
                    saveInvoice();
                }
            }
        });
    }

    function invoiceGenerate() {

        $.ajax({
            url: "<?php echo $generateInvoiceIdUrl; ?>",
            data: {},
            type: "POST",
            dataType: 'json',
            success: function(data) {
                $('#inv_no_').val(data.invoice_id);
                $('#BuyerInvoice_invoice_no').val(data.invoice_id);
                $("#inv_no_").attr("disabled", true);

            }
        });


    }

    function saveInvoice() {
        var company = $('#BuyerInvoice_company_id').val();
        var buyer_id = $('#BuyerInvoice_buyer_id').val();
        var project = $('#BuyerInvoice_project_id').val();
        var flat_id = $('#BuyerInvoice_flat_id').val();
        var default_date = $(".date").val();
        var inv_no = $('#BuyerInvoice_invoice_no').val();
        var invoice_id = $('#invoice_id').val();
        if ((moment(default_date, 'DD-MM-YYYY', true).isValid())) {
            if (buyer_id != '' && project != '' && flat_id != '' && default_date != '' && inv_no != '' && company != '') {
                $.ajax({
                    method: "POST",
                    async: false,
                    data: {
                        company: company,
                        buyer_id: buyer_id,
                        project: project,
                        flat_id: flat_id,
                        default_date: default_date,
                        inv_no: inv_no,
                        invoice_id: invoice_id
                    },
                    dataType: "json",
                    url: "<?php echo $createInvoiceUrl; ?>",
                    success: function(result) {
                        if (result.response == 'success') {
                            $(".purchase_items").removeClass('checkek_edit');
                            $('.js-example-basic-single').select2('focus');
                            $("#invoice_id").val(result.invoice_id);
                            $().toastmessage('showSuccessToast', "" + result.msg + "");
                        } else {
                            $().toastmessage('showErrorToast', "" + result.msg + "");
                            element.val('');
                        }

                    }
                });
            } else {
                console.log(company, buyer_id, project, flat_id, default_date, inv_no);
            }

        } else {
            $(this).focus();
            $().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
            $(this).focus();
        }
    }
    /*********************************** */
</script>


<script>
    jQuery.extend(jQuery.expr[':'], {
        focusable: function(el, index, selector) {
            return $(el).is('button, :input, [tabindex]');
        }
    });

    $(document).on('keypress', 'input,select', function(e) {
        if (e.which == 13) {
            e.preventDefault();
            // Get all focusable elements on the page
            var $canfocus = $(':focusable');
            var index = $canfocus.index(document.activeElement) + 1;
            if (index >= $canfocus.length) index = 0;
            $canfocus.eq(index).focus();
        }
    });

    $(document).ready(function() {
        $(".popover-test").popover({
            html: true,
            content: function() {
                return $(this).next('.popover-content').html();
            }
        });
        $('[data-toggle=popover]').on('click', function(e) {
            $('[data-toggle=popover]').not(this).popover('hide');
        });
        $('body').on('hidden.bs.popover', function(e) {
            $(e.target).data("bs.popover").inState.click = false;
        });
        $('body').on('click', function(e) {
            $('[data-toggle=popover]').each(function() {
                // hide any open popovers when the anywhere else in the body is clicked
                if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                    $(this).popover('hide');
                }
            });
        });
        $(document).ajaxComplete(function() {
            $(".popover-test").popover({
                html: true,
                content: function() {
                    return $(this).next('.popover-content').html();
                }
            });
        });

        $(".js-example-basic-single").select2();
        $(".project").select2();
        $(".client").select2();
        $(".company_id").select2();

    });








    $(document).ready(function() {
        $('select').first().focus();
        //$('.js-example-basic-single').select2('focus');
        $("BuyerInvoice_invoice_no").keyup(function() {
            if (this.value.match(/[^a-zA-Z0-9]/g)) {
                this.value = this.value.replace(/[^a-zA-Z0-9\-/]/g, '');
            }
        });
    });
</script>



<script>
    $(document).ready(function() {
        $().toastmessage({
            sticky: false,
            position: 'top-right',
            inEffectDuration: 1000,
            stayTime: 3000,
            closeText: '<i class="icon ion-close-round"></i>',
        });

        //$('.check_class').hide();

        $(".purchase_items").addClass('checkek_edit');


    });

    $(document).on('click', '.removebtn', function(e) {
        e.preventDefault();
        element = $(this);
        var item_id = $(this).attr('id');
        var answer = confirm("Are you sure you want to delete?");
        if (answer) {
            var item_id = $(this).attr('id');
            var invoice_id = $("#invoice_id").val();
            var subtot = $('input[name="subtot"]').val();
            var grand = $('input[name="grand"]').val();
            var tax_grand = $('#grand_taxamount').text();
            var data = {
                'invoice_id': invoice_id,
                'item_id': item_id,
                'grand': grand,
                'subtot': subtot,
                'tax_grand': tax_grand
            };
            $.ajax({
                method: "GET",
                async: false,
                data: {
                    data: data
                },
                dataType: "json",
                url: '<?php echo $removeItemUrl; ?>',
                success: function(response) {
                    if (response.response == 'success') {
                        $('#grand_total').html(response.final_amount);
                        $('#grand_taxamount').html(response.final_tax);
                        $('#net_amount').html(response.grand_total);
                        $().toastmessage('showSuccessToast', "" + response.msg + "");
                        $('.addrow').html(response.html);
                    } else {
                        $().toastmessage('showErrorToast', "" + response.msg + "");
                    }
                }
            });

        } else {

            return false;
        }








    });



    $(document).on("click", ".addcolumn, .removebtn", function() {
        //alert('hi');

        $("table.table  input[name='sl_No[]']").each(function(index, element) {
            $(element).val(index + 1);
            $('.sl_No').html(index + 1);
        });
    });






    $(".inputSwitch span").on("click", function() {

        var $this = $(this);

        $this.hide().siblings("input").val($this.text()).show();

    });

    $(".inputSwitch input").bind('blur', function() {

        var $this = $(this);

        $(this).attr('value', $(this).val());

        $this.hide().siblings("span").text($this.val()).show();

    }).hide();


    $(".allownumericdecimal").keydown(function(event) {



        if (event.shiftKey == true) {
            event.preventDefault();
        }

        if ((event.keyCode >= 48 && event.keyCode <= 57) ||
            (event.keyCode >= 96 && event.keyCode <= 105) ||
            event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 ||
            event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190 || event.keyCode == 110 || event.keyCode == 13) {
            var splitfield = $(this).val().split(".");
            if ((splitfield[1] !== undefined && splitfield[1].length >= 2) && event.keyCode != 8 && event.keyCode != 0 && event.keyCode != 13) {
                event.preventDefault();
            }
        } else {
            event.preventDefault();
        }

        if ($(this).val().indexOf('.') !== -1 && (event.keyCode == 190 || event.keyCode == 110))
            event.preventDefault();
        //if a decimal has been added, disable the "."-button





    });







    $('.other').click(function() {
        if (this.checked) {


            $('#autoUpdate').fadeIn('slow');
            $('#select_div').hide();
        } else {

            $('#autoUpdate').fadeOut('slow');
            $('#select_div').show();
        }

    })


    $(document).on("change", ".other", function() {
        if (this.checked) {
            $(this).closest('tr').find('#autoUpdate').fadeIn('slow');
            $(this).closest('tr').find('#select_div').hide();
        } else {
            $(this).closest('tr').find('#autoUpdate').fadeOut('slow');
            $(this).closest('tr').find('#select_div').show();
        }
    });
</script>


<script>
    /* Neethu  */






    $("#purchaseno").keypress(function(event) {
        if (event.keyCode == 13) {
            //$('#description').select2('focus');
            $("#purchaseno").blur();
        }
        //$('.js-example-basic-single').select2('focus');
    });

    $("#date").keypress(function(event) {
        if (event.keyCode == 13) {
            if ($(this).val()) {
                $("BuyerInvoice_invoice_no").focus();
            }
        }
    });

    $(".date").keyup(function(event) {
        if (event.keyCode == 13) {
            $(".date").click();

        }
    });

    $("#vendor").keyup(function(event) {
        if (event.keyCode == 13) {
            $("#vendor").click();
        }
    });

    $("#project").keyup(function(event) {
        if (event.keyCode == 13) {
            $("#project").click();
        }
    });


    var sl_no = 1;
    var howMany = 0;






    $(document).on('click', '.edit_item', function(e) {
        e.preventDefault();


        $('.remark').css("display", "inline-block");
        $('#remark').focus();

        var item_id = $(this).attr('id');
        var $tds = $(this).closest('tr').find('td');
        var description = $tds.eq(1).text();
        var type = $tds.eq(2).text();

        var sgst = $tds.eq(3).text();
        var sgst_amount = $tds.eq(4).text();
        var cgst = $tds.eq(5).text();
        var cgst_amount = $tds.eq(6).text();

        var igst = $tds.eq(7).text();
        var igst_amount = $tds.eq(8).text();
        var amount = $tds.eq(9).text();
        var tax_amount = $tds.eq(10).text();
        var discount = $tds.eq(11).text();
        var subtotal_amt = $tds.eq(12).text();
        $('#description').val(description.trim());
        $("#BuyerInvoiceList_transaction_type").val(type);
        $('#item_amount').html(parseFloat(amount))
        $('#item_amount_temp').val(parseFloat(amount))
        $('#amount').val(parseFloat(amount))
        $('#discount').val(parseFloat(discount))
        $('#tax_amount').html(parseFloat(tax_amount))
        $('#subtotal_amt').html(parseFloat(subtotal_amt))
        $('#item_tax_temp').val(parseFloat(tax_amount))
        $('#cgst').val(cgst.trim());
        $('#cgst_amount').text(cgst_amount)
        $('#sgst').val(sgst.trim())
        $('#sgst_amount').text(sgst_amount)
        $('#igst').val(igst.trim())
        $('#igst_amount').text(igst_amount)

        $(".item_save").attr('value', 'Update');
        $(".item_save").attr('id', item_id);
        $(".item_save").attr('id', item_id);
        $('#description').focus();


    })





    $('.item_save').keypress(function(e) {
        if (e.keyCode == 13) {
            $('.item_save').click();
        }
    });


    function filterDigits(eventInstance) {
        eventInstance = eventInstance || window.event;
        key = eventInstance.keyCode || eventInstance.which;
        if ((47 < key) && (key < 58) || key == 8) {
            return true;
        } else {
            if (eventInstance.preventDefault)
                eventInstance.preventDefault();
            eventInstance.returnValue = false;
            return false;
        } //if
    }

    // tax calculation

    $("#amount, #sgst, #cgst, #igst,#discount").blur(function() {
        var amount = parseFloat($("#amount").val());
        var sgst = parseFloat($("#sgst").val());
        var cgst = parseFloat($("#cgst").val());
        var igst = parseFloat($("#igst").val());
        var discount_amt = parseFloat($("#discount").val());
        var new_amount = 0;

        var sgst_amount = (sgst / 100) * amount;
        var cgst_amount = (cgst / 100) * amount;
        var igst_amount = (igst / 100) * amount;

        if (isNaN(sgst_amount)) sgst_amount = 0;
        if (isNaN(cgst_amount)) cgst_amount = 0;
        if (isNaN(igst_amount)) igst_amount = 0;
        if (isNaN(discount_amt)) discount_amt = 0;
        if (isNaN(amount)) amount = 0;

        var tax_amount = sgst_amount + cgst_amount + igst_amount;

        var total_amount = (amount + tax_amount) - (discount_amt);

        $("#sgst_amount").html(sgst_amount.toFixed(2));
        $("#cgst_amount").html(cgst_amount.toFixed(2));
        $("#igst_amount").html(igst_amount.toFixed(2));
        $("#item_amount").html(amount.toFixed(2));
        $("#tax_amount").html(tax_amount.toFixed(2));
        $("#total_amount").html(total_amount.toFixed(2));
        $("#subtotal_amt").html(total_amount.toFixed(2));

    });

    $('.calculation').keyup(function(event) {
        var val = $(this).val();
        if (val > 100) {
            $().toastmessage('showErrorToast', "This percentage must be less than 100%");
            $(this).val('');
        }
    })

    $("#company_id").change(function() {
        var val = $(this).val();
        $("#project").html('<option value="">Select Project</option>');
        $.ajax({
            url: '<?php echo Yii::app()->createUrl('invoice/dynamicproject'); ?>',
            method: 'POST',
            data: {
                company_id: val
            },
            dataType: "json",
            success: function(response) {
                if (response.status == 'success') {
                    $("#project").html(response.html);
                } else {
                    $("#project").html(response.html);
                }
            }

        })
    })
</script>

<style>
    .table-scroll {
        position: relative;
        max-width: 1280px;
        width: 100%;
        margin: auto;
        display: table;
        overflow: auto;
    }

    .table-wrap {
        width: 100%;
        display: block;
        overflow: auto;
        position: relative;
        z-index: 1;
        border: 1px solid #ddd;
    }

    .table-wrap.fixedON,
    .table-wrap.fixedON table,
    .faux-table table {
        height: 380px;
        /* match heights*/
    }

    .table-scroll table {
        width: 100%;
        margin: auto;
        border-collapse: separate;
        border-spacing: 0;
        border: 1px solid #ddd;
    }

    .table-scroll th,
    .table-scroll td {
        padding: 5px 10px;
        border: 1px solid #ddd;
        background: #fff;
        vertical-align: top;
    }

    .faux-table table {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        pointer-events: none;
    }

    .faux-table table tbody {
        visibility: hidden;
    }

    /* shrink cells in cloned table so that the table height is exactly 300px so that the header and footer appear fixed */
    .faux-table table tbody th,
    .faux-table table tbody td {
        padding-top: 0;
        padding-bottom: 0;
        border-top: none;
        border-bottom: none;
        line-height: 0.1;
    }

    .faux-table table tbody tr+tr th,
    .faux-table tbody tr+tr td {
        line-height: 0;
    }

    .faux-table thead th,
    .faux-table tfoot th,
    .faux-table tfoot td,
    .table-wrap thead th,
    .table-wrap tfoot th,
    .table-wrap tfoot td {
        background: #eee;
    }

    .faux-table {
        position: absolute;
        top: 0;
        right: 0;
        left: 0;
        bottom: 0;
        overflow-y: scroll;
    }

    .faux-table thead,
    .faux-table tfoot,
    .faux-table thead th,
    .faux-table tfoot th,
    .faux-table tfoot td {
        position: relative;
        z-index: 2;
    }

    /* ie bug */
    .table-scroll table thead tr,
    .table-scroll table thead tr th,
    .table-scroll table tfoot tr,
    .table-scroll table tfoot tr th,
    .table-scroll table tfoot tr td {
        height: 1px;
    }
</style>
<script>
    (function() {
        var mainTable = document.getElementById("main-table");
        if (mainTable !== null) {
            var tableHeight = mainTable.offsetHeight;
            if (tableHeight > 380) {
                var fauxTable = document.getElementById("faux-table");
                document.getElementById("table-wrap").className += ' ' + 'fixedON';
                var clonedElement = mainTable.cloneNode(true);
                clonedElement.id = "";
                fauxTable.appendChild(clonedElement);
            }
        }
    })();
</script>