<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/validation/jquery.validate.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/validation/additional-methods.js"></script>

<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/message/css/jquery.message.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/message/js/jquery.message.js"></script>
<style>
    .lefttdiv {
        float: left;
    }

    .purchase-title {
        border-bottom: 1px solid #ddd;
    }

    .purchase_items h3 {
        font-size: 18px;
        color: #333;
        margin: 0px;
        padding: 0px;
        text-align: left;
        padding-bottom: 10px;
    }

    .purchase_items {
        padding: 15px;
        box-shadow: 0 0 13px 1px rgba(0, 0, 0, 0.25);
    }

    .purchaseitem {
        display: inline-block;
        margin-right: 20px;
    }

    .purchaseitem last-child {
        margin-right: 0px;
    }

    .purchaseitem label {
        font-size: 12px;
    }

    .remark {
        display: none;
    }

    .padding-box {
        padding: 3px 0px;
        min-height: 17px;
        display: inline-block;
    }

    th {
        height: auto;
    }

    .quantity,
    .rate {
        max-width: 80px;
    }

    .text_align {

        text-align: center;
    }


    *:focus {
        border: 1px solid #333;
        box-shadow: 0 0 6px 1px rgba(0, 0, 0, 0.25);
    }

    .text-right {
        text-align: right;
    }

    .light_bg_grey {
        background: #88888838 !important;
    }
    table.item_table tbody tr.catehead td {
        background-color: #c5c5dd;
        font-weight: 600;
        color: #000;
        padding: 15px 10px;
    }
</style>
<script>
    $(function () {
        $("#datepicker").datepicker({
            dateFormat: 'dd-mm-yy'
        }).datepicker("setDate", new Date());

    });
</script>

<br /><br />


<div class="container">
    <div class="loading-overlay">
        <span class="fa fa-spinner fa-3x fa-spin"></span>
    </div>
    <div class="invoicemaindiv">
        <div id="loading"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/ajax-loader.gif"></div>
        <div class="pull-right">
            <a style="cursor: pointer" id="download" class="pdfbtn1 pdf_excel">
                <div class="save_pdf"></div>SAVE AS PDF
            </a>
            <a  data-id="<?php echo $qtid; ?>"  data-rev="<?php echo $rev_no; ?>" style="cursor: pointer" id="sendmail" class="pdf_excel sendmail">
                SEND MAIL
            </a>
        </div>

        <h2 class="purchase-title">Quotation</h2>
        <br>
        <div id="errormessage"></div>
        <div id="send_form" style="display:none;">
            <div class="panel panel-gray">
                <div class="panel-heading form-head">
                    <h3 class="panel-title">Send Mail</h3>
                </div>
                <form id="form_send">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-6">
                                <input type="hidden" name="quotation_id" id="quotation_id" value="<?php echo $qtid; ?>">
                                <input type="hidden" name="revision_no" id="revision_no" value="<?php echo $rev_no; ?>">
                                <input type="hidden" name="pdf_path" id="pdf_path" value="">
                                <input type="hidden" name="pdf_name" id="pdf_name" value="">
                                <div class="form-group">
                                    <label for="ProjectType_project_type" class="required">To</label>
                                    <input id="mail_to" name="mail_to" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">

                                <div class="form-group">
                                    <label for="ProjectType_project_type" class="required">Message</label>
                                    <textarea id="mail_content" name="mail_content" class="form-control"></textarea>
                                </div>
                            </div>
                            <?php
                            if (!empty($settings['po_email_from'])) {
                                ?>
                                <div class="col-md-6">
                                    <input type="checkbox" class="form-check-input change_val " checked='true' id="send_copy" name="send_copy">
                                    <label class="form-check-label" for="inclusive">Send copy to (<?php echo $settings['po_email_from']; ?>)
                                    </label>
                                </div>
                            <?php } ?>
                            <div class="col-md-6">
                                <label style="display:block;">&nbsp;</label>
                                <button class="btn btn-info btn-sm send_btn">Send</button>
                                <?php echo CHtml::ResetButton('Close', array('onclick' => 'closeaction(this,event);', 'class' => 'btn btn-sm')); ?>
                                <div id="loader1" style="display:none;"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/ajax-loader.gif"></div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <form id="pdfvals1" method="post" action="">
            <input type="hidden" name="purchaseview">
            <div class="row">
                <div class="col-md-2">
                    <label>COMPANY : </label>
                    <?php
                    $company = Company::model()->findByPk($model->company_id);
                    echo $company['name'];
                    ?>
                </div>
                <div class="col-md-2">
                    <label>CLIENT : </label>
                    <?php echo $model->client_name; ?>
                </div>
                <div class="col-md-2">
                    <label>DATE : </label>
                    <?php echo isset($model->date_quotation) ? date('d-m-Y', strtotime($model->date_quotation)) : ''; ?>
                </div>
                <div class="col-md-2">
                    <label>Quotation No : </label>
                    <?php echo isset($model->invoice_no) ? $model->invoice_no : ''; ?>
                </div>
                <div class="col-md-2">
                    <label>Revision No : </label>
                    <?php echo isset($_GET['rev_no']) ? $_GET['rev_no'] : ''; ?>
                </div>
            </div>
            <div class="clearfix"></div>
            <br><br>
            <div id="table-scroll" class="table-scroll">
                <div id="faux-table" class="faux-table" aria="hidden"></div>
                <div id="table-wrap" class="table-wrap">
                    <div class="table-responsive">
                        <table cellpadding="10" class="item_table">
                            <thead>
                                <tr>
                                    <th>Sl No</th>
                                    <th>Work Type</th>
                                    <th colspan="2">Using Materials</th>
                                    <th>Description</th>
                                    <th>Qty</th>
                                    <th>Rate</th>
                                    <th>Amount After Discount </th>

                                </tr>
                            </thead>
                            <tbody>

                                <?php
                                $amount_afetr_discount = 0;
                                $amount_afetr_discount_extra = 0;
                                if (!empty($view_datas)) {
                                    $i = 1;
                                    $mrp = 0;
                                    $amount_afetr_discount = 0;
                                    $previous_cat = '';
                                    foreach ($view_datas as $view_data) {
                                        $datas = $view_data['items_list'];
                                        $sub_items = $view_data['sub_items'];
                                        $category_id = $view_data['category_details']['category_id'];
                                        $category_name = QuotationCategoryMaster::model()->findByPk($category_id);
                                        $category_label = $view_data['category_details']['category_label'] != '' ? $view_data['category_details']['category_label'] : $category_name['name'];
                                        $i = 1;
                                        ?>
                                        <tr class="catehead">
                                            <td colspan="8">
                                                <b><?php echo $category_label; ?></b>
                                        </tr>
                                        <?php
                                        foreach ($datas as $key => $value) {
                                            $work_type_label = QuotationWorktypeMaster::model()->findByPk($value['work_type']);
                                            if ($work_type_label['template_id'] == 1) {
                                                ?>
                                                <tr>
                                                    <td></td>
                                                    <td></td>
                                                    <td>SHUTTER WORK</td>
                                                    <td>CARCASS BOX WORK</td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                            <?php } ?>
                                            <tr>
                                                <td><?php echo $i; ?></td>
                                                <td>
                                                    <?php echo ($value['worktype_label'] != '') ? $value['worktype_label'] : $work_type_label['name']; ?></td>
                                                <?php
                                                $finish_data = Controller::getFinishData();
                                                if ($work_type_label['template_id'] == 1) {
                                                    $sh_description = $value['shutterwork_description'];
                                                    $css_description = $value['carcass_description'];
                                                    $item_master = QuotationItemMaster::model()->findByPk($value['item_id']);
                                                    ?>
                                                    <td>
                                                        <span>
                                                            <?php
                                                            $specification = Specification::model()->findByPk($item_master['shutter_material_id']);
                                                            $brand = Brand::model()->findByPk($specification ['brand_id']);

                                                            echo $brand['brand_name'] . ' - ' . $specification ['specification'];
                                                            ?>
                                                        </span><br>
                                                        <span>
                                                            <?php echo $value['shutterwork_finish']; ?>

                                                        </span>
                                                    </td>
                                                    <td>
                                                        <span>
                                                            <?php
                                                            $specification = Specification::model()->findByPk($item_master['carcass_material_id']);
                                                            $brand = Brand::model()->findByPk($specification ['brand_id']);

                                                            echo $brand['brand_name'] . ' - ' . $specification ['specification'];
                                                            ?>   
                                                        </span><br>
                                                        <span>
                                                            <?php echo $value['carcass_finish']; ?>
                                                        </span>
                                                    </td>
                                                <?php } else if (3 == $work_type_label['template_id']) {
                                                    ?>
                                                    <td><span>
                                                            <?php echo $value['material']; ?>
                                                        </span></td>
                                                    <td><span>
                                                            <?php echo $value['finish']; ?>
                                                        </span></td>
                                                <?php } else { ?>
                                                    <td></td>
                                                    <td></td>
                                                <?php } ?>
                                                <?php if ($work_type_label['template_id'] == 1) { ?>
                                                    <td>
                                                        SHUTTER :  <span> <?php echo $sh_description ?></span><br>
                                                        CARCASS :  <span><?php echo $css_description ?></span>
                                                    </td>
                                                <?php } else { ?>
                                                    <td><?php echo $value['description']; ?></td>
                                                <?php } ?>
                                                <td><?php echo $value['quantity_nos'] ?></td>
                                                <td class="text-right"><?php echo $value['mrp'] ?></td>
                                                <td class="text-right"><?php echo $value['amount_after_discount'] ?></td>

                                            </tr>
                                            <?php
                                            $mrp = $mrp + $value['mrp'];
                                            $amount_afetr_discount = $amount_afetr_discount + $value['amount_after_discount'];
                                            $i++;
                                        }
                                        if(!empty($sub_items)){
                                        
                                            foreach ($sub_items as $key1 => $value) {
                                                $work_type_label = QuotationWorktypeMaster::model()->findByPk($value['work_type']);
                                                if($key1 ==0){
                                                ?>
                                                <tr>
                                                    <td colspan="8">
                                                        <b><?php echo $value['subitem_label']; ?></b>
                                                    </td>
                                                </tr>
                                                <?php
                                                }
                                                if ($work_type_label['template_id'] == 1) {
                                                    ?>
                                                    <tr>
                                                        <td></td>
                                                        <td></td>
                                                        <td>SHUTTER WORK</td>
                                                        <td>CARCASS BOX WORK</td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                <?php } ?>
                                                <tr>
                                                    <td><?php echo $i; ?></td>
                                                    <td>
                                                        <?php echo ($value['worktype_label'] != '') ? $value['worktype_label'] : $work_type_label['name']; ?></td>
                                                    <?php
                                                    $finish_data = Controller::getFinishData();
                                                    if ($work_type_label['template_id'] == 1) {
                                                        $sh_description = $value['shutterwork_description'];
                                                        $css_description = $value['carcass_description'];
                                                        $item_master = QuotationItemMaster::model()->findByPk($value['item_id']);
                                                        ?>
                                                        <td>
                                                            <span>
                                                                <?php 
                                                                $specification = Specification::model()->findByPk($item_master['shutter_material_id']);
                                                                $brand = Brand::model()->findByPk($specification ['brand_id']);
    
                                                                echo $brand['brand_name'] . ' - ' . $specification ['specification']; 
                                                                ?>
                                                            </span><br>
                                                            <span>
                                                                <?php echo $value['shutterwork_finish']; ?>

                                                            </span>
                                                        </td>
                                                        <td>
                                                            <span>
                                                                <?php 
                                                                $specification = Specification::model()->findByPk($item_master['carcass_material_id']);
                                                                $brand = Brand::model()->findByPk($specification ['brand_id']);
    
                                                                echo $brand['brand_name'] . ' - ' . $specification ['specification'];
                                                                ?>   
                                                            </span><br>
                                                            <span>
                                                                <?php echo $value['carcass_finish']; ?>
                                                            </span>
                                                        </td>
                                                    <?php } else if (3 == $work_type_label['template_id']) {
                                                        ?>
                                                        <td><span>
                                                                <?php echo $value['material']; ?>
                                                            </span></td>
                                                        <td><span>
                                                                <?php echo $value['finish']; ?>
                                                            </span></td>
                                                    <?php } else { ?>
                                                        <td></td>
                                                        <td></td>
                                                    <?php } ?>
                                                    <?php if ($work_type_label['template_id'] == 1) { ?>
                                                        <td>
                                                            SHUTTER :  <span> <?php echo $sh_description ?></span><br>
                                                            CARCASS :  <span><?php echo $css_description ?></span>
                                                        </td>
                                                    <?php } else { ?>
                                                        <td><?php echo $value['description']; ?></td>
                                                    <?php } ?>
                                                    <td><?php echo $value['quantity_nos'] ?></td>
                                                    <td class="text-right"><?php echo $value['mrp'] ?></td>
                                                    <td class="text-right"><?php echo $value['amount_after_discount'] ?></td>

                                                </tr>
                                                <?php
                                                $mrp = $mrp + $value['mrp'];
                                                $amount_afetr_discount = $amount_afetr_discount + $value['amount_after_discount'];
                                                $i++;
                                            }
                                        }
                                        $previous_cat = $category_id;
                                    }
                                    ?>
                                    <tr class="ddsss">
                                        <td colspan="7" class="text-right">Total Amount :</td>
                                        <td class="text-right"><?php echo Yii::app()->Controller->money_format_inr($mrp, 2) ?></td>
                                    </tr>
                                    <tr class="ddsss">
                                        <td colspan="7" class="text-right">Amount After Discount:</td>
                                        <td class="text-right"><?php echo Yii::app()->Controller->money_format_inr($amount_afetr_discount, 2) ?></td>
                                    </tr>

                                <?php } if (!empty($extra_work)) { ?>
                                    <tr>
                                        <td colspan="9">Extra Work</td>
                                    </tr>

                                    <?php
                                    $j = 1;
                                    $mrp_extra = 0;
                                    $amount_afetr_discount_extra = 0;
                                    foreach ($extra_work as $key => $value) {
                                        ?>
                                        <tr>
                                            <td> <?php echo $j; ?></td>
                                            <td colspan="3"></td>
                                            <td><?php echo $value['description']; ?></td>
                                            <td><?php echo $value['quantity_nos']; ?> </td>
                                            <td class="text-right"> <?php echo $value['mrp']; ?></td>
                                            <td class="text-right"><?php echo $value['amount_after_discount']; ?> </td>
                                        </tr>
                                        <?php
                                        $mrp_extra = $mrp_extra + $value['mrp'];
                                        $amount_afetr_discount_extra = $amount_afetr_discount_extra + $value['amount_after_discount'];
                                        $j++;
                                    }
                                    ?>
                                    <tr>
                                        <td colspan="7" class="text-right">Total Amount :</td>
                                        <td class="text-right"><?php echo Yii::app()->Controller->money_format_inr($mrp_extra, 2) ?></td>
                                    </tr>
                                    <tr>
                                        <td colspan="7" class="text-right">Amount After Discount :</td>
                                        <td class="text-right"><?php echo Yii::app()->Controller->money_format_inr($amount_afetr_discount_extra, 2) ?></td>
                                    </tr>
                                <?php } ?>
                                <tr>
                                    <th colspan="7"  class="text-right">Total Taxable Amount :</th>
                                    <td class="text-right"><?php echo Yii::app()->Controller->money_format_inr(($amount_afetr_discount + $amount_afetr_discount_extra ), 2) ?></td>
                                </tr>
                                <tr>
                                    <th colspan="7" class="text-right">Total Tax Amount :</th>
                                    <td class="text-right"><?php echo Yii::app()->Controller->money_format_inr($total_tax, 2) ?></td>
                                </tr>
                                <tr>
                                    <th colspan="7" class="text-right">Grand Total :</th>
                                    <td class="text-right"><?php echo Yii::app()->Controller->money_format_inr(($amount_afetr_discount + $amount_afetr_discount_extra + $total_tax), 2) ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div style="padding-right: 0px;" class="col-md-offset-9 col-md-3 col-sm-offset-8 col-sm-4 col-xs-offset-6 col-xs-6">
            </div>
            <br><br>
        </form>
    </div>
    <style>
        .error_message {
            color: red;
        }

        a.pdf_excel {
            background-color: #6a8ec7;
            display: inline-block;
            padding: 8px;
            color: #fff;
            border: 1px solid #6a8ec8;
        }

        .invoicemaindiv th,
        .invoicemaindiv td {
            padding: 10px;
        }
    </style>


    <script>

        $(document).ready(function () {
            $('#loading').hide();
        })
        function closeaction() {
            $('#send_form').slideUp(500);
        }

        $('#sendmail').click(function () {
            $('#send_form').slideDown();
        });

        $("#form_send").validate({
            rules: {
                mail_content: {
                    required: true,
                },
                mail_to: {
                    required: true,
                    email: true
                },
            },
            messages: {
                mail_content: {
                    required: "Please enter mesage",
                },
                mail_to: {
                    required: "Please enter email",
                    email: "Please enter valid email"
                }
            },
        });

        $('.send_btn').click(function (e) {
            e.preventDefault();
            var data = $('#form_send').serialize();
            if ($("#form_send").valid()) {
                $('#loader').css('display', 'inline-block');
                if ($("#send_copy").prop('checked') == true) {
                    var send_copy = 1;
                } else {
                    var send_copy = 0;
                }
                var qid = $('#quotation_id').val();
                var revision_no = $('#revision_no').val();
                var mail_to = $('#mail_to').val();
                var mail_content = $('#mail_content').val();
                var pdf_path = $('#pdf_path').val();
                var pdf_name = $('#pdf_name').val();
                $('.send_btn').prop('disabled', true)
                $('.loading-overlay').addClass('is-active');
                $.ajax({
                    url: '<?php echo Yii::app()->createAbsoluteUrl('salesquotation/salesQuotation/sendattachment'); ?>',
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        qid: qid,
                        revision_no: revision_no,
                        mail_to: mail_to,
                        mail_content: mail_content,
                        pdf_path: pdf_path,
                        pdf_name: pdf_name,
                        send_copy: send_copy
                    },
                    success: function (response) {
                        if (response.status == '1') {
                            $('#loader').hide();
                            $('.send_btn').prop('disabled', false)
                            $('#send_form').slideUp(500);
                            $("#form_send").trigger('reset');
                            $("#errormessage").show()
                                    .html('<div class="alert alert-success">' + response.message + '</div>')
                                    .fadeOut(10000);
                        } else {
                            $('#loader').hide();
                            $('.send_btn').prop('disabled', false)
                            $("#errormessage").show()
                                    .html('<div class="alert alert-danger">' + response.message + '</div>')
                                    .fadeOut(10000);
                        }
                    }
                })
            }
        })

        $(".sendmail").click(function () {
            var q_id = $(this).attr("data-id");
            var revision_no = $(this).attr("data-rev");
            $('#loading').show();
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: '<?php echo Yii::app()->createUrl('salesquotation/salesQuotation/pdfgeneration'); ?>',
                data: {
                    q_id: q_id,
                    revision_no: revision_no,
                },
                success: function (response) {
                    $('#pdf_path').val(response.path);
                    $('#pdf_name').val(response.name);
                }
            })
        })
        $(function () {
            $('.pdfbtn1').click(function () {
                $("form#pdfvals1").attr('action', "<?php echo $this->createAbsoluteUrl('SalesQuotation/SaveQuotation', array('qid' => $model['id'], 'rev_no' => $rev_no));
                                ?>");
                $("form#pdfvals1").submit();
            });
        });


    </script>

</div>

<style>
    .table-scroll {
        position: relative;
        max-width: 1280px;
        width: 100%;
        margin: auto;
        display: table;
    }

    .table-wrap {
        width: 100%;
        display: block;
        overflow: auto;
        position: relative;
        z-index: 1;
        border: 1px solid #ddd;
    }

    .table-wrap.fixedON,
    .table-wrap.fixedON table,
    .faux-table table {
        height: 380px;
        /* match heights*/
    }

    .table-scroll table {
        width: 100%;
        margin: auto;
        border-collapse: collapse;
        border-spacing: 0;
        border: 1px solid #ddd;
    }

    .table-scroll th,
    .table-scroll td {
        padding: 5px 10px;
        border: 1px solid #ddd;
        background: #fff;
        vertical-align: top;
    }

    .faux-table table {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        pointer-events: none;
    }

    .faux-table table tbody {
        visibility: hidden;
    }

    /* shrink cells in cloned table so that the table height is exactly 300px so that the header and footer appear fixed */
    .faux-table table tbody th,
    .faux-table table tbody td {
        padding-top: 0;
        padding-bottom: 0;
        border-top: none;
        border-bottom: none;
        line-height: 0.1;
    }

    .faux-table table tbody tr+tr th,
    .faux-table tbody tr+tr td {
        line-height: 0;
    }

    .faux-table thead th,
    .faux-table tfoot th,
    .faux-table tfoot td,
    .table-wrap thead th,
    .table-wrap tfoot th,
    .table-wrap tfoot td {
        background: #eee;
    }

    .faux-table {
        position: absolute;
        top: 0;
        right: 0;
        left: 0;
        bottom: 0;
        overflow-y: scroll;
    }

    .faux-table thead,
    .faux-table tfoot,
    .faux-table thead th,
    .faux-table tfoot th,
    .faux-table tfoot td {
        position: relative;
        z-index: 2;
    }

    /* ie bug */
    .table-scroll table thead tr,
    .table-scroll table thead tr th,
    .table-scroll table tfoot tr,
    .table-scroll table tfoot tr th,
    .table-scroll table tfoot tr td {
        height: 1px;
    }
</style>
<script>
    (function () {
        var mainTable = document.getElementById("main-table");
        if (mainTable !== null) {
            var tableHeight = mainTable.offsetHeight;
            if (tableHeight > 380) {
                text - right
                var fauxTable = document.getElementById("faux-table");
                document.getElementById("table-wrap").className += ' ' + 'fixedON';
                var clonedElement = mainTable.cloneNode(true);
                clonedElement.id = "";
                fauxTable.appendChild(clonedElement);
            }
        }
    })();

    $(document).ajaxComplete(function () {
        $('.loading-overlay').removeClass('is-active');
        $('#loading').hide();
        $('#loader1').hide();
    });
</script>