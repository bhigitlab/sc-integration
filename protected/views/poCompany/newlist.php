<?php
/* @var $this ExpensesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'Vendors',
);
$page = Yii::app()->request->getParam('Vendors_page');
if ($page != '') {
    Yii::app()->user->setReturnUrl($page);
} else {
    Yii::app()->user->setReturnUrl(0);
}
?>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<div class="container" id="vendors">
    <div class="sub-heading margin-bottom-10">
        <h2>PO Company</h2>
        <?php if (isset(Yii::app()->user->role) && (in_array('/poCompany/create', Yii::app()->user->menuauthlist))) { ?>
            <button class="createpocom btn btn-info">Add Company</button>
        <?php } ?>
    </div>
    <div id="pocomform" style="display:none;"></div>
    <?php $this->renderPartial('_search', array('model' => $model)) ?>


    <div class="">
        <?php $this->widget('zii.widgets.CListView', array(
            'dataProvider' => $dataProvider,
            'itemView' => '_view',
            'template' => '<div class=""><table cellpadding="10" class="table" id="pocompanttbl">{items}</table></div>',
            'ajaxUpdate' => false,
        )); ?>

        <!-- Add Project Popup -->
        <div id="addVendors" class="modal fade" role="dialog">
            <div class="modal-dialog modal-lg">

            </div>
        </div>


        <!-- Edit Project Popup -->

        <div class="modal fade edit" role="dialog">
            <div class="modal-dialog">


            </div>
        </div>
        <?php
        Yii::app()->clientScript->registerScript('myjavascript', '
             $(document).ready(function(){
                $("#pocompanttbl").dataTable( {
                    "scrollY": "300px",
                    "scrollCollapse": true,
                    "paging": false,
                     "columnDefs"        : [       
                        { 
                            "searchable"    : false, 
                            "targets"       : [0,2] 
                        },
                        { "bSortable": false, "aTargets": [-1]  }
                    ],
                } );
                var table = $("#pocompanttbl").DataTable();
                $("#PoCompany_name").on( "keyup", function () {
                var name = $(this).val();
                    table
                        .columns( 1 )
                        .search( name )
                        .draw();
                } );
               
                $("#clear").on( "click", function () {
                var selectedText=" ";
                    table
                        .columns( 1 )
                        .search( selectedText )
                        .draw();
                } );
               
                
                
            });

            ');
        ?>
        <script>
            function closeaction() {
                $('#pocomform').slideUp();
            }
            $(document).ready(function () {
                $('.createpocom').click(function () {
                    $.ajax({
                        type: "GET",
                        url: "<?php echo $this->createUrl('PoCompany/create&layout=1') ?>",
                        success: function (response) {
                            $("#pocomform").html(response).slideDown();

                        }
                    });
                });
     /*$('.createVendors').click(function () {
              
         $.ajax({
               type: "GET",
               url: "<?php echo $this->createUrl('PoCompany/create') ?>",
                success: function (response) {
                    $("#addVendors").html(response);

                }
            });
     });* /
            $(document).delegate('.editVendors', 'click', function () {
                //$('.editVendors').click(function () {
                // alert("hai");
                var id = $(this).attr('data-id');
                //   alert(id);

                $.ajax({
                    type: "GET",
                    url: "<?php echo $this->createUrl('PoCompany/update&layout=1&id=') ?>" + id,
                    success: function (response) {
                        //$(".edit").html(response);
                        $("#pocomform").html(response).slideDown();

                    }
                });

            });

            jQuery(function ($) {
                $('#vendor').on('keydown', function (event) {
                    if (event.keyCode == 13) {
                        $("#vendorssearch").submit();

                /*alert('key up');
                var name=$('input[name=project]').val();
                alert(name);
        
                   $.ajax({
                           type: "POST",
                           data : {name:name},
                           url: "<?php echo $this->createUrl('vendors/vendorList') ?>",

                    });  * /


            }
        
           
        });
});

});	 
        </script>



    </div>
</div>

<style>
    .page-body h3 {
        margin: 4px 0px;
        color: inherit;
        text-align: left;
    }

    .panel {
        border: 1px solid #ddd;
    }

    .panel-heading {
        background-color: #eee;
        height: 40px;
    }
</style>