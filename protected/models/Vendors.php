<?php

/**
 * This is the model class for table "{{vendors}}".
 *
 * The followings are the available columns in table '{{vendors}}':
 * @property integer $vendor_id
 * @property string $name
 * @property string $phone
 * @property string $email_id
 * @property integer $status
 * @property string $description
 * @property string $address
 * @property string $gst_no
 * @property integer $payment_date_range
 * @property string $type
 * @property string $pan_no
 * @property string $created_date
 * @property integer $created_by
 *
 * The followings are the available model relations:
 * @property Dailyvendors[] $dailyvendors
 * @property Projectbook[] $projectbooks
 * @property VendorExptype[] $vendorExptypes
 */
class Vendors extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{vendors}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, status, description, address, created_by, company_id', 'required'),
			array('status, created_by', 'numerical', 'integerOnly' => true),
			array('name, email_id, gst_no', 'length', 'max' => 100),
			array('phone', 'length', 'max' => 15, 'min' => 10),
			array('payment_date_range', 'numerical', 'integerOnly' => true,'min'=>0),
			array('phone', 'numerical'),
			array('type', 'length', 'max'=>1),
			array('pan_no', 'length', 'max'=>300),
			array('email_id', 'email'),
			array('gst_no', 'match', 'pattern' => '{^[0-9]{2}[A-Z]{5}[0-9]{4}[A-Z]{1}[1-9A-Z]{1}Z[0-9A-Z]{1}$}',
                    'message' => 'Invalid GST number'),
			array('created_date, company_id,vendor_type', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('vendor_id, name, phone, email_id, status, description, address, gst_no, payment_date_range, type, pan_no,created_date, created_by,company_id', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'dailyvendors' => array(self::HAS_MANY, 'Dailyvendors', 'vendor_id'),
			'projectbooks' => array(self::HAS_MANY, 'Projectbook', 'vendor'),
			'vendorExptypes' => array(self::HAS_MANY, 'VendorExptype', 'vendor_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'vendor_id' => 'Vendor',
			'name' => 'Name',
			'phone' => 'Phone',
			'email_id' => 'Email',
			'status' => 'Status',
			'description' => 'Description',
			'address' => 'Address',
			'gst_no' => 'Gst No',
			'created_date' => 'Created Date',
			'created_by' => 'Created By',
			'company_id' => 'Company',
			'vendor_type' => 'Mark as miscellaneous vendor',
			'payment_date_range' => 'Payment Date Range',
            'type' => 'Type',
            'pan_no' => 'Pan No',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('vendor_id', $this->vendor_id);
		$criteria->compare('name', $this->name, true);
		$criteria->compare('phone', $this->phone, true);
		$criteria->compare('email_id', $this->email_id, true);
		$criteria->compare('status', $this->status);
		$criteria->compare('description', $this->description, true);
		$criteria->compare('address', $this->address, true);
		$criteria->compare('gst_no', $this->gst_no, true);
		$criteria->compare('payment_date_range',$this->payment_date_range);
        $criteria->compare('type',$this->type,true);
        $criteria->compare('pan_no',$this->pan_no,true);
		$criteria->compare('created_date', $this->created_date, true);
		$criteria->compare('created_by', $this->created_by);
		$criteria->compare('company_id', $this->company_id);
		$user = Users::model()->findByPk(Yii::app()->user->id);
		$arrVal = explode(',', $user->company_id);
		$newQuery = "";
		foreach ($arrVal as $arr) {
			if ($newQuery) $newQuery .= ' OR';
			$newQuery .= " FIND_IN_SET('" . $arr . "', company_id)";
		}
		$criteria->addCondition($newQuery);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'pagination' => false
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Vendors the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function getExpensetypes($id)
	{
		$tblpx = Yii::app()->db->tablePrefix;
		$exp = Yii::app()->db->createCommand()
			->select(['ex.type_name'])
			->from($tblpx . 'vendors as v')
			->leftJoin($tblpx . 'vendor_exptype ve', 'v.vendor_id = ve.vendor_id')
			->leftJoin($tblpx . 'expense_type ex', 'ex.type_id = ve.type_id')
			->where('v.vendor_id=:vendor_id', array(':vendor_id' => $id))
			->queryAll();
		foreach ($exp as $exptype) {
			$type[] = $exptype['type_name'];
		}
		$usrnames = "";
		if ($type != NULL) {
			$usrnames = implode(', ', $type);
		}
		return $usrnames;
	}

	public function setVendorLedgerViewArray($daybookPayment, $dailyvPayment, $purchase_return)
	{
		$daybookPayments = !empty($daybookPayment) ? $daybookPayment : array();
		$dailyvPayments = !empty($dailyvPayment) ? $dailyvPayment : array();
		$purchase_returns = !empty($purchase_return) ? $purchase_return : array();
		$paymentDatas = !empty($paymentData) ? $paymentData : array();
		$final_array = array();
		foreach ($daybookPayments as $daybookPayment) {
			$daybookPayment['key'] = 1;
			$final_array[] = $daybookPayment;
		}


		foreach ($dailyvPayments as $dailyvPayment) {
			$dailyvPayment['key'] = 2;
			$final_array[] = $dailyvPayment;
		}

		foreach ($purchase_returns as $purchase_return) {
			foreach ($purchase_return as $purchase_return_type) {
				$purchase_return_type['key'] = 3;
				$final_array[] = $purchase_return_type;
			}
		}

		foreach ($paymentDatas as $paymentData) {
			$datas = $paymentData['datas'];
			foreach ($datas as $data) {
				$data['date'] = $data['bill_date'];
				$data['key'] = 4;
				$final_array[] = $data;
			}
		}
		$date = array_column($final_array, 'date');
		array_multisort($date, SORT_ASC, $final_array);
		return $final_array;
	}
	public function setVendorLedgerViewArray1($daybookPayment1, $dailyvPayment1, $purchase_return, $paymentData)
	{
		$daybookPayments = !empty($daybookPayment1) ? $daybookPayment1 : array();
		$dailyvPayments = !empty($dailyvPayment1) ? $dailyvPayment1 : array();
		$purchase_returns = !empty($purchase_return) ? $purchase_return : array();
		$paymentDatas = !empty($paymentData) ? $paymentData : array();
		$final_array = array();
		foreach ($daybookPayments as $daybookPayment) {
			$daybookPayment['key'] = 1;
			$final_array[] = $daybookPayment;
		}


		foreach ($dailyvPayments as $dailyvPayment) {
			$dailyvPayment['key'] = 2;
			$final_array[] = $dailyvPayment;
		}

		foreach ($purchase_returns as $purchase_return) {
			foreach ($purchase_return as $purchase_return_type) {
				$purchase_return_type['key'] = 3;
				$final_array[] = $purchase_return_type;
			}
		}

		foreach ($paymentDatas as $paymentData) {
			$datas = $paymentData['datas'];
			foreach ($datas as $data) {
				$data['date'] = $data['bill_date'];
				$data['key'] = 4;
				$final_array[] = $data;
			}
		}
		$date = array_column($final_array, 'date');
		array_multisort($date, SORT_ASC, $final_array);
		return $final_array;
	}

	public function getinvoiceSum($vendor_id,$date_from,$date_to){

		$tblpx  = Yii::app()->db->tablePrefix;
		$condition1 = "";
		$condition2 = "";
		$user       = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal     = explode(',', $user->company_id);
		$newQuery1  = "";
		$newQuery2  = "";
		foreach ($arrVal as $arr) {			
			if ($newQuery1) $newQuery1 .= ' OR';			
			if ($newQuery2) $newQuery2 .= ' OR';			
			$newQuery1 .= " FIND_IN_SET('" . $arr . "', company_id)";
			$newQuery2 .= " FIND_IN_SET('" . $arr . "', b.company_id)";			
		}		
		$condition1 .= " AND (" . $newQuery1 . ")";		
		$condition2 .= " AND (" . $newQuery2 . ")";	

		$datecondition3 = "AND '1=1'";
            $datecondition1 = " AND '1=1'";
            if (!empty($date_from) && !empty($date_to)) {

                $datecondition1 = " AND date BETWEEN '" . date("Y-m-d", strtotime($date_from)) . "' AND '" . date("Y-m-d", strtotime($date_to)) . "'";
                
            } else if (!empty($date_from) && empty($date_to)) {

                $datecondition1 = " AND date >= '" . date("Y-m-d", strtotime($date_from)) . "'";
                
            } else if (empty($date_from) && !empty($date_to)) {

                $datecondition1 = " AND date <= '" . date("Y-m-d", strtotime($date_to)) . "'";
                
            } else {

                $datecondition1 = "AND '1=1'";
             }
			 $datecondition4 = " AND '1=1'";
            if (!empty($date_from) && !empty($date_to)) {

                $datecondition4 = " AND DATE(b.bill_date) BETWEEN '" . date("Y-m-d", strtotime($date_from)) . "' AND '" . date("Y-m-d", strtotime($date_to)) . "'";
                
            } else if (!empty($date_from) && empty($date_to)) {

                $datecondition4 = " AND DATE(b.bill_date) >= '" . date("Y-m-d", strtotime($date_from)) . "'";
                
            } else if (empty($date_from) && !empty($date_to)) {

                $datecondition4 = " AND DATE(b.bill_date) <= '" . date("Y-m-d", strtotime($date_to)) . "'";
                
            } else {

                $datecondition4 = "AND '1=1'";
             }


			 $daybooksql = "
			 SELECT 
				 SUM(totamount) as sumtotalamount 
			 FROM (
				 SELECT 
					 MAX(
						 CASE 
							 WHEN ex.bill_id IS NOT NULL THEN ex.amount
							 ELSE 0 
						 END
					 ) as totamount 
				 FROM {$tblpx}expenses ex 
				 LEFT JOIN {$tblpx}bills b ON ex.bill_id = b.bill_id 
				 WHERE 
					 ex.type = 73
					 AND ex.vendor_id = {$vendor_id} 
					 {$condition2} 
					 {$datecondition1} 
					 AND (ex.reconciliation_status = 1 OR ex.reconciliation_status IS NULL)
				 GROUP BY 
					 ex.projectid, 
					 ex.bill_id 
					
			 ) as subquery";


			 $total_vendor_Amount=0;
			 $daybook_total_sql = "
SELECT 
    e.bill_id, 
    e.exp_id,
    e.projectid
FROM 
    {$tblpx}expenses e
WHERE 
    e.vendor_id = {$vendor_id} {$condition1} {$datecondition1}  
    AND (e.reconciliation_status = 1 OR e.reconciliation_status IS NULL) 
    AND e.type = 73 
    AND (
        e.bill_id IS NULL
        OR e.exp_id IN (
            SELECT MIN(exp_id) 
            FROM {$tblpx}expenses 
            WHERE vendor_id = {$vendor_id} {$condition1}
              AND (reconciliation_status = 1 OR reconciliation_status IS NULL)
              AND type = 73
              AND bill_id IS NOT NULL
            GROUP BY bill_id
        )
    )
";
 
				   
					 
			 
						 $daybook_total = Yii::app()->db->createCommand($daybook_total_sql)->queryAll();
							 $total_vendor_Amount =0;
							 if(!empty($daybook_total)){
								 foreach($daybook_total as $daybook_total_entries){
									 $exp_model = Expenses::Model()->findByPk($daybook_total_entries["exp_id"]);
									 if(!empty($exp_model)){
										 $total_vendor_Amount += $exp_model->amount;
									 }
								 }
							 }
							 $daybookPayment=$total_vendor_Amount;
		//$daybookPayment = Yii::app()->db->createCommand($daybooksql)->queryScalar();
			// die($daybooksql);
			
		$expreceipt_returnsql = "select * from " . $tblpx . "expenses "
            . " WHERE amount !=0 AND vendor_id=" . $vendor_id . ""
            . "  AND type = '72' " . $condition1 . $datecondition1 . " AND return_id IS NOT NULL "
            . "  AND (reconciliation_status =1 OR reconciliation_status IS NULL)"
            . " ORDER BY date";
        $expreceipt_return = Yii::app()->db->createCommand($expreceipt_returnsql)->queryAll();

		$purchase_return = Projects::model()->setPurchaseReturn($expreceipt_return);
		$credit_datas = isset($purchase_return['credit']) ? $purchase_return['credit'] : array();
		$other_datas = isset($purchase_return['other']) ? $purchase_return['other'] : array();
		$sum_ret = 0;
		$credit_sum1 = 0;
		$other_sum1 = 0;
		
		foreach ($credit_datas as $return_data) {
			$credit_sum1 +=   $return_data['amount'];
		}
		foreach ($other_datas as $return_data1) {
			$other_sum1 +=   $return_data1['paidamount'];
		}
		$sum_ret =    $credit_sum1 + $other_sum1;
		
		
		$dayinvtotal = $daybookPayment;
		$retinvtotal = ("-" . $sum_ret);
		$directinvtotal = 0.00;

		$overallinvtotal = $dayinvtotal + $retinvtotal  + $directinvtotal;
		return $overallinvtotal;
	}
}
