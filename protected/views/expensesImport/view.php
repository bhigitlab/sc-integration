<?php
/* @var $this ExpensesImportController */
/* @var $model ExpensesImport */

$this->breadcrumbs=array(
	'Expenses Imports'=>array('index'),
	$model->exp_id,
);

$this->menu=array(
	array('label'=>'List ExpensesImport', 'url'=>array('index')),
	array('label'=>'Create ExpensesImport', 'url'=>array('create')),
	array('label'=>'Update ExpensesImport', 'url'=>array('update', 'id'=>$model->exp_id)),
	array('label'=>'Delete ExpensesImport', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->exp_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage ExpensesImport', 'url'=>array('admin')),
);
?>

<h1>View ExpensesImport #<?php echo $model->exp_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'exp_id',
		'projectid',
		'userid',
		'date',
		'amount',
		'description',
		'type',
		'exptype',
		'vendor_id',
		'payment_type',
		'works_done',
		'materials',
		'created_by',
		'created_date',
		'updated_by',
		'updated_date',
		'purchase_type',
		'paid',
	),
)); ?>
