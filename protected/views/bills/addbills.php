<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.js"></script>

<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/message/css/jquery.message.css">
<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/purchase.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/message/js/jquery.message.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/moment.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/tableHeadFixer.js"></script>
<script>
	var shim = (function() {
		document.createElement('datalist');
	})();
</script>
<style>
	.lefttdiv {
		float: left;
	}

	.extracharge {
		float: left;
	}

	.extracharge h4 {
		position: relative;
	}

	.addcharge {
		position: absolute;
		top: 2px;
		right: -25px;
		cursor: pointer;
	}

	.extracharge div {
		margin-bottom: 10px;
	}

	.addRow input.textbox {
		width: 200px;
		display: inline-block;
		margin-right: 10px;
	}

	.delbil,
	.deleteitem {

		color: #000;
		background-color: #eee;
		padding: 3px;
		cursor: pointer;
	}

	.addRow label {
		display: inline-block;
	}

	.base {
		display: none;
	}
</style>
<script>
	$(function() {
		$("#datepicker").datepicker({
			dateFormat: 'dd-mm-yy'
		}).datepicker("setDate", new Date());

	});
</script>
<style>
	.dropdown {
		position: relative;
		display: inline-block;
	}

	.dropdown::before {
		position: absolute;
		content: " \2193";
		top: 0px;
		right: -8px;
		height: 20px;
		width: 20px;
	}

	button#caret {
		border: none;
		background: none;
		position: absolute;
		top: 0px;
		right: 0px;
	}

	.invoicemaindiv th,
	.invoicemaindiv td {
		padding: 10px;
		text-align: left;
	}

	.addcolumn {
		cursor: pointer;
		background-color: black;
		border: none;
		color: white;
		text-align: center;
		text-decoration: none;
		margin: 2px;
		font-size: 30px;
		width: 40px;
	}

	.invoicemaindiv .pull-right,
	.invoicemaindiv .pull-left {
		float: none !important;
	}

	.add_selection {
		background: #000;
	}

	.toast-container {
		width: 350px;
	}

	.toast-position-top-right {
		top: 57px;
		right: 6px;
	}

	.toast-item-close {
		background-image: none;
		cursor: pointer;
		width: 12px;
		height: 12px;
		text-align: center;
		border-radius: 2px;
	}

	.toast-item-image {
		font-size: 24px;
	}

	.toast-item-close:hover {
		color: red;
	}

	.toast-item {
		border: transparent;
		border-radius: 3px;
		font-size: 10px;
		opacity: 1;
		background-color: rgba(34, 45, 50, 0.8);
	}

	.toast-item-wrapper p {
		margin: 0px 5px 0px 42px;
		font-size: 14px;
		text-align: justify;
	}


	.toast-type-success {
		background-color: #00A65A;
		border-color: #00A65A;
	}

	.toast-type-error {
		background-color: #DD4B39;
		border-color: #DD4B39;
	}

	.toast-type-notice {
		background-color: #00C0EF;
		border-color: #00C0EF;
	}

	.toast-type-warning {
		background-color: #F39C12;
		border-color: #F39C12;
	}

	.span_class {
		min-width: 70px;
		display: inline-block;
	}

	.form-fields .form-control {
		height: 28px;
		display: inline-block;
		padding: 6px 3px;
		width: 60%;
	}

	.remark {
		display: none;
	}

	th {
		height: auto;
	}

	.quantity,
	.rate,
	.small_class {
		max-width: 100%;
	}

	.amt_sec {
		float: right;
	}

	.amt_sec:after,
	.padding-box:after {
		display: block;
		content: '';
		clear: both;
	}

	.client_data {
		margin-top: 6px;
	}

	*:focus {
		border: 1px solid #333;
		box-shadow: 0 0 6px 1px rgba(0, 0, 0, 0.25);
	}

	#parent,
	#parent2 {
		max-height: 150px;
	}

	.tooltip-hiden {
		width: auto;
	}

	.mb-1 {
		margin-bottom: 5px;
	}

	@media(max-width: 767px) {

		.quantity,
		.rate,
		.small_class {
			width: auto !important;
		}

		.padding-box {
			float: right;
		}

		#tax_amount,
		#item_amount {
			display: inline-block;
			float: none;
			text-align: right;
		}

		/* span.select2.select2-container.select2-container--default.select2-container--focus {
			width: auto !important;
		}

		span.select2.select2-container.select2-container--default.select2-container--below {
			width: auto !important;
		} */

		.form-fields .form-control {
			width: 100%;
		}
	}

	@media(min-width: 768px) {
		.invoicemaindiv .pull-right {
			float: right !important;
		}

		.invoicemaindiv .pull-left {
			float: left !important;
		}

		.main-form span.select2.select2-container,.main-form span.select2.select2-container.select2-container--default.select2-container--focus,.main-form .form-control {
			width: 190px !important;
		}

		.main-form span.select2.select2-container.select2-container--default.select2-container--below {
			width: 190px !important;
		}
	}

	.pre_content {
		max-height: 100px;
		overflow-y: auto;
	}

	.pre_content p {
		font-size: 14px;
	}

	.select2-dropdown {
		z-index: 1;
	}
</style>
<?php
$general_settings_for_auto_receipt = Controller::generalSettingsForAutoReceiptToProjectWarehouse($settings = 1);
?>
<div class="container">
	<div class="loading-overlay">
		<span class="fa fa-spinner fa-3x fa-spin"></span>
	</div>
	<div class="invoicemaindiv">
			<?php $this->renderPartial('_bill_details_form',array('model' => $model,
			'model2' => $model2,
			'project' => $project,
			'vendor' => $vendor,
			'newmodel' => $newmodel,
			'specification' => $specification,));?>

<?php $this->renderPartial('_bill_items_form',array('model' => $model,
			'model2' => $model2,
			'project' => $project,
			'vendor' => $vendor,
			'newmodel' => $newmodel,
			'specification' => $specification,));?>
			
	</div>


	<div id="table-scroll" class="table-scroll">
		<div id="faux-table" class="faux-table" aria="hidden"></div>
		<div id="table-wrap" class="table-wrap">
			<div class="table-responsive">
				<table border="1" class="table">
					<thead>
						<tr>
							<th>Sl.No</th>
							<th>Description</th>
							<th>Quantity</th>
							<th>Unit</th>
							<th>Rate</th>
							<th>Batch</th>
							<th>HSN Code</th>
							<th>Base Quantity</th>
							<th>Base Unit</th>
							<th>Base Rate</th>
							<th>Tax Slab (%)</th>
							<th>SGST Amount</th>
							<th>SGST (%)</th>
							<th>CGST Amount</th>
							<th>CGST (%)</th>
							<th>IGST Amount</th>
							<th>IGST (%)</th>
							<th>Discount (%)</th>
							<th>Discount Amount</th>
							<th>Amount</th>
							<th>Tax Amount</th>


							<th></th>
						</tr>
					</thead>
					<tbody class="addrow">

						</tr>
					</tbody>
					<tfoot>
						<tr>
							<th colspan="18" class="text-right">TOTAL: </th>
							<th class="text-right">
								<div id="discount_total">0.00</div>
							</th>
							<th class="text-right">
								<div id="amount_total">0.00
							</th>
							<th class="text-right">
								<div id="tax_total">0.00
							</th>
							<th></th>
						</tr>
						<tr rowspan="2">
							<th colspan="19" class="text-right">Round Off:
								<input type="text" name="bill_round_off" class="text-right" id="bill_round_off" value="<?php echo $model['round_off'] != '' ? $model['round_off'] : 0; ?>">
							</th>
							<th colspan="3"></th>
						</tr>
						<tr>
							<th colspan="19" class="text-right">GRAND TOTAL:
								<input type="text" name="grand_total" id="grand_total" value="0.00" class="text-left" style="border: none !important;background-color: #eeeeee;" readonly=true> </th>
							<th colspan="3">
								<div class="additional_total" style="display:none;">
									<label class="inline">Additional Charges:</label>
									<span id="totaladditional_charge">0</span>
								</div>
							</th>
						</tr>
					</tfoot>
				</table>
			</div>


		</div>
	</div>

	<div style="padding-right: 0px;" class="col-md-offset-9 col-md-3 col-sm-offset-8 col-sm-4 col-xs-offset-6 col-xs-6">

		<input type="hidden" value="<?php echo ((isset($subtot) && $subtot != '') ? $subtot : ''); ?>" class="txtBox pastweek" readonly=ture name="subtot" /></td>
		<input type="hidden" value="<?php echo ((isset($grand) && $grand != '') ? $grand : ''); ?>" class="txtBox pastweek grand" name="grand" readonly=true />



	</div>

	</form>
	<div class="addRow clearfix">
		<div class="extracharge">
			<h4>Additional Charges <span class="icon icon-plus addcharge"></span></h4>



			<span class="errorcharge" style="color: red;"></span>

			<div class="chargeadd" style="display:none;">
				<input type="text" placeholder="Charge Category" name="packagecharge" id="labelcat" class="form-control textbox" />
				<input type="number" placeholder="Enter Amount" name="packagecharge" id="currentval" class="form-control textbox currentval" />
				<input type="button" class=" btn btn-sm btn-primary savecharge" value="save" />
			</div>
			<div id="addtional_list"></div>
		</div>
	</div>
	<br><br>
    <div class="row addRow">
        <div class="col-sm-12 text-center"><?php echo CHtml::Button($model->isNewRecord ? 'Submit' : 'Save', array('class' => 'buttonsubmit btn btn-default', 'id' => 'buttonsubmit', 'disabled' => true)); ?></div>
    </div>
</div>



<input type="hidden" name="final_amount" id="final_amount" value="0">

<input type="hidden" name="item_amount_temp" id="item_amount_temp" value="0">

<input type="hidden" name="final_tax" id="final_tax" value="0">

<input type="hidden" name="item_tax_temp" id="item_tax_temp" value="0">
<input type="hidden" name="bill_id" id="bill_id" value="0">

<style>
	.error_message {
		color: red;
	}

	a.pdf_excel {
		background-color: #6a8ec7;
		display: inline-block;
		padding: 8px;
		color: #fff;
		border: 1px solid #6a8ec8;
	}
</style>
<?php $addbillurl = Yii::app()->createAbsoluteUrl("bills/additionalbill1"); ?>
<?php $delbillurl = Yii::app()->createAbsoluteUrl("bills/deletebill2"); ?>


<script>
	$(document).ready(function() {
		$('#loading').hide();
		$('.addcharge').click(function() {
			$(".chargeadd").toggle();
			$(this).toggleClass("icon-minus hidecharge");
		});

		$(".savecharge").on('keyup keypress keydown click', function(e) {
			var billid = $("#bill_id").val();
			if (billid != 0) {
				var label = $("#labelcat").val();
				var currentval = $("#currentval").val();
				var totalamount = parseFloat($("#amount_total").text());

				var addtot = parseFloat($("#totaladditional_charge").text());


				if (billid == '' || totalamount == '0.00') {

					$('.errorcharge').html('Please add item');

				} else if (label != '' && currentval != '') {
					$(".savecharge").attr('disabled', 'disabled');
					$('.loading-overlay').addClass('is-active');
					$.ajax({
						url: "<?php echo $addbillurl; ?>",
						data: {
							category: label,
							amount: currentval,
							billid: billid,
							totalamount: totalamount
						},
						type: "POST",
						dataType: 'json',
						success: function(data) {
							$(".savecharge").removeAttr('disabled');
							if (data != '') {
								$("#addtional_list").append("<div class='display_additional'>" + label + " : " + "<span class='totalprice'>" + parseFloat(currentval).toFixed(2) + "</span> &nbsp;&nbsp&nbsp;<span class='icon icon-trash deleteitem' title='Delete' id=" + data.itemid + "></span> " + "</div>");
								$(".additional_total").show();
								$('#labelcat').val('');
								$('#currentval').val('');
								if (isNaN(addtot)) {
									var totaladd = parseFloat(currentval);
								} else {
									var totaladd = parseFloat(addtot) + parseFloat(currentval);
									totaladd = parseFloat(totaladd);
								}
								var totalamount = $("#amount_total").text();
								$('.additional_charge').show();
								$("#totaladditional_charge").text(totaladd);

								var tot = parseFloat(currentval) + parseFloat(totalamount);
								$("#grand_total").val(data.amount.toFixed(2));


							}
						}
					});
				} else {

					if (label == '' || currentval == '') {
						$('.errorcharge').html('Both fields are required');
					}

				}
			} else {
				$('.errorcharge').html('Please add item');
			}
		});

		$(document).on("click", ".deleteitem", function(event) {
			var billid = $("#bill_id").val();
			$(this).parent().remove();
			var id = $(this).attr('id');
			$('.loading-overlay').addClass('is-active');
			$.ajax({
				url: "<?php echo $delbillurl; ?>",
				data: {
					id: id,
					billid: billid
				},
				type: "POST",
				dataType: 'json',
				success: function(data) {
					$("#totaladditional_charge").text(data.additional);
					//var amount = data.bill_amount.toFixed(2);
					//console.log(amount);
					$("#grand_total").val(data.bill_amount);
				}
			});
		});

		$(".popover-test").popover({
			html: true,
			content: function() {
				return $(this).next('.popover-content').html();
			}
		});
		$('[data-toggle=popover]').on('click', function(e) {
			$('[data-toggle=popover]').not(this).popover('hide');
		});
		$('body').on('hidden.bs.popover', function(e) {
			$(e.target).data("bs.popover").inState.click = false;
		});
		$('body').on('click', function(e) {
			$('[data-toggle=popover]').each(function() {
				// hide any open popovers when the anywhere else in the body is clicked
				if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
					$(this).popover('hide');
				}
			});
		});
		$(document).ajaxComplete(function() {
			$(".popover-test").popover({
				html: true,
				content: function() {
					return $(this).next('.popover-content').html();
				}
			});

		});
		$(".js-example-basic-single").select2();
		$(".project").select2();
		$(".vendor").select2();
		$("#Warehousestock_warehousestock_warehouseid").select2();
		$(".expense_head").select2();
		$(".company_id").select2();
		$(".tax_slab").select2();
	});

	jQuery.extend(jQuery.expr[':'], {
		focusable: function(el, index, selector) {
			return $(el).is('button, :input, [tabindex]');
		}
	});

	$(document).on('keypress', 'input,select', function(e) {
		if (e.which == 13) {
			e.preventDefault();
			var $canfocus = $(':focusable');
			var index = $canfocus.index(document.activeElement) + 1;
			if (index >= $canfocus.length) index = 0;
			$canfocus.eq(index).focus();
		}
	});

	
	$(document).ready(function() {
		$("#inv_no").keyup(function() {
			if (this.value.match(/[^a-zA-Z0-9]/g)) {
				this.value = this.value.replace(/[^a-zA-Z0-9\-/]/g, '');
			}
		});
	});
	
	$(document).ready(function() {
		$().toastmessage({
			sticky: false,
			position: 'top-right',
			inEffectDuration: 1000,
			stayTime: 3000,
			closeText: '<i class="icon ion-close-round"></i>',
		});

		$(".bill_items").addClass('checkek_edit');
	});


	


	// delete

	$(document).on('click', '.removebtn', function(e) {
		e.preventDefault();
		element = $(this);
		var item_id = $(this).attr('id');
		if (item_id != 0) {
			var answer = confirm("Are you sure you want to delete?");
			if (answer) {
				var bill_id = $("#bill_id").val();
				var data = {
					'bill_id': bill_id,
					'item_id': item_id
				};
				$('.loading-overlay').addClass('is-active');
				$.ajax({
					method: "GET",
					async: false,
					data: {
						data: data
					},
					dataType: "json",
					url: '<?php echo Yii::app()->createUrl('bills/removebillitem'); ?>',
					success: function(response) {
						if (response.response == 'success') {
							element.closest('tr').remove();
							$('#discount_total').html(response.discount_total);
							$('#amount_total').html(response.amount_total);
							$('#tax_total').html(response.tax_total);
							$('#grand_total').val(response.grand_total);
							$().toastmessage('showSuccessToast', "" + response.msg + "");
							$('.addrow').html(response.html);
							$('.item_save').attr("id", "0");
							if (response.total_quantity == 0) {
                                $('#buttonsubmit').prop('disabled', true);
                            } else {
                                $('#buttonsubmit').prop('disabled', false);
                            }
						} else {
							$().toastmessage('showErrorToast', "" + response.msg + "");
						}

						$('#description').val('').trigger('change');
						$('#quantity').val('');
						$('#unit').val('');
						$('#rate').val('');
						$('#dis_amount').val('');
						$('#disp').html('0.00');
						$('#item_amount').html('0.00');
						$('#tax_amount').html('0.00');
						$('#sgstp').val('');
						$('#sgst_amount').html('0.00');
						$('#cgstp').val('');
						$('#cgst_amount').html('0.00');
						$('#igstp').val('');
						$('#igst_amount').html('0.00');
						$('#disc_amount').html('0.00');
						$('#total_amount').html('0.00');
						$('#description').focus();
						$('.item_save').attr("id", "0");
						$('#tax_slab').val('18').trigger('change');
					}
				});

			} else {
				return false;
			}
		}
	});



	$(document).on("click", ".addcolumn, .removebtn", function() {

		$("table.table  input[name='sl_No[]']").each(function(index, element) {
			$(element).val(index + 1);
			$('.sl_No').html(index + 1);
		});
	});

	$(".inputSwitch span").on("click", function() {

		var $this = $(this);

		$this.hide().siblings("input").val($this.text()).show();

	});

	$(".inputSwitch input").bind('blur', function() {

		var $this = $(this);

		$(this).attr('value', $(this).val());

		$this.hide().siblings("span").text($this.val()).show();

	}).hide();


	$(".allownumericdecimal").keydown(function(event) {



		if (event.shiftKey == true) {
			event.preventDefault();
		}

		if ((event.keyCode >= 48 && event.keyCode <= 57) ||
			(event.keyCode >= 96 && event.keyCode <= 105) ||
			event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 ||
			event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190 || event.keyCode == 110 || event.keyCode == 13) {
			var splitfield = $(this).val().split(".");
		} else {
			event.preventDefault();
		}

		if ($(this).val().indexOf('.') !== -1 && (event.keyCode == 190 || event.keyCode == 110))
			event.preventDefault();
	});


	
	// edit


	$(document).on('click', '.edit_item', function(e) {
		e.preventDefault();
		var item_id = $(this).attr('id');
		var $tds = $(this).closest('tr').find('td');
		var description = $tds.eq(1).text();
		var quantity = $tds.eq(2).text();
		var unit = $tds.eq(3).text();
		var rate = $tds.eq(4).text();
		var bibatch = $tds.eq(5).text();
		var hsn_code = $tds.eq(6).text();
		var purchaseitem_quantity = $tds.eq(7).text();
		var purchaseitem_unit = $tds.eq(8).text();
		var purchaseitem_rate = $tds.eq(9).text();
		var tax_slab = $tds.eq(10).text();
		tax_slab = Math.round(tax_slab);
		var sgst_amount = $tds.eq(11).text();
		var sgstp = $tds.eq(12).text();
		var cgst_amount = $tds.eq(13).text();
		var cgstp = $tds.eq(14).text();
		var igst_amount = $tds.eq(15).text();
		var igstp = $tds.eq(16).text();
		var disp = $tds.eq(17).text();
		var dis_amount = $tds.eq(18).text();
		var amount = $tds.eq(19).text();
		var tax_amount = $tds.eq(20).text();
		var bill_id = $("#bill_id").val();
		var des_id = $('.item_description').attr("id");
		var data = {
			bill_id: bill_id,
			description: des_id,
			bibatch: bibatch,
			billitem: item_id
		};



		$.ajax({
			method: "GET",
			data: {
				data: data
			},
			dataType: "json",
			url: '<?php echo Yii::app()->createUrl('wh/warehousereceipt/CheckEditBillDespatch'); ?>',
			success: function(result) {

				if (result.response == 'success') {

					if (des_id == '0' || des_id == 'other') {
						$('#remarks').val(description.trim());
						$('.remark').css("display", "inline-block");
						$('#remark').focus();
						$('#remarks').focus();
						$('#description').val('other').trigger('change');
					} else {

						$('#description').val(des_id).trigger('change');
						$('.remark').css("display", "none");
						$('.js-example-basic-single').select2('focus');
					}

					if (purchaseitem_unit == unit) {
						$('.base').hide();
					} else {
						$('.base').show();
					}



					$('#purchase_quantity').val(parseFloat(quantity));
					$('#purchase_unit').val(unit.trim());
					$('#bibatch').val(bibatch.trim());
					$('#hsn_code').val(hsn_code.trim());
					$('#item_amount').html(parseFloat(amount));
					$('#purchase_rate').val(parseFloat(rate));
					$('#tax_slab').val(tax_slab).trigger('change');
					$('#quantity').val(parseFloat(purchaseitem_quantity));
					$('#item_unit').val(parseFloat(purchaseitem_unit));
					$('#rate').val(parseFloat(purchaseitem_rate));
					$('#purchaseitem_unit_hidden').val(unit.trim());

					$('#tax_amount').html(parseFloat(tax_amount))
					$('#disc_amount').html(parseFloat(dis_amount))
					var totalamount = (parseFloat(amount) + parseFloat(tax_amount)) - parseFloat(dis_amount);
					$('#total_amount').html(parseFloat(totalamount).toFixed(2));

					$('#dis_amount').val(parseFloat(dis_amount));
					$('#disp').html(parseFloat(disp));
					$('#sgstp').val(parseFloat(sgstp));
					$('#sgst_amount').html(parseFloat(sgst_amount));
					$('#cgstp').val(parseFloat(cgstp));
					$('#cgst_amount').html(parseFloat(cgst_amount));
					$('#igstp').val(parseFloat(igstp));
					$('#igst_amount').html(parseFloat(igst_amount));
					$(".item_save").attr('value', 'Update');
					$(".item_save").attr('id', item_id);
					$(".item_save").attr('id', item_id);
					$('#description').focus();

				} else {
					$().toastmessage('showErrorToast', "" + result.msg + "");
				}
			}
		});
	})


	


	function filterDigits(eventInstance) {
		eventInstance = eventInstance || window.event;
		key = eventInstance.keyCode || eventInstance.which;
		if ((47 < key) && (key < 58) || key == 8) {
			return true;
		} else {
			if (eventInstance.preventDefault)
				eventInstance.preventDefault();
			eventInstance.returnValue = false;
			return false;
		}
	}


	// calculation


	
	$("#company_id").change(function() {
		$('#loading').show();
		var val = $(this).val();
		$("#project").html('<option value="">Select Project</option>');
		$.ajax({
			url: '<?php echo Yii::app()->createUrl('bills/dynamicproject'); ?>',
			method: 'POST',
			data: {
				company_id: val
			},
			dataType: "json",
			success: function(response) {
				if (response.status == 'success') {
					$("#project").html(response.html);
				} else {
					$("#project").html(response.html);
				}
			}

		})
	})
</script>

<script>
	(function() {
		var mainTable = document.getElementById("main-table");
		if (mainTable !== null) {
			var tableHeight = mainTable.offsetHeight;
			if (tableHeight > 380) {
				var fauxTable = document.getElementById("faux-table");
				document.getElementById("table-wrap").className += ' ' + 'fixedON';
				var clonedElement = mainTable.cloneNode(true);
				clonedElement.id = "";
				fauxTable.appendChild(clonedElement);
			}
		}
	})();
	$(document).ajaxComplete(function() {
		$('.loading-overlay').removeClass('is-active');
		$('#loading').hide();
	});
	$(document).on('blur', '#quantity', function(e) {
		var newbase_quantity = $('#quantity').val();
		var amount = $('#item_amount').text();
		var base_rate = amount / newbase_quantity;
		base_rate = parseFloat(base_rate).toFixed(2);
		$('#rate').val(base_rate);
	});
	$("#buttonsubmit").keypress(function(e) {
            if (e.keyCode == 13) {
                $('.buttonsubmit').click();
            }
        });

        $(".buttonsubmit").click(function() {
            var general_settings_for_auto_receipt = <?php echo $general_settings_for_auto_receipt; ?>;
            if (general_settings_for_auto_receipt == 1) {
                var answer = confirm("Are you sure you want to auto receipt this item?");
                if (answer) {
					var data = $("#pdfvals1").serialize();					
					$.ajax({
						url: '<?php echo Yii::app()->createUrl('bills/autoReceiptBillWithoutPo'); ?>',
						method: 'POST',
						data: data,						
						success: function(response) {       
							$().toastmessage('showSuccessToast', 'Warehouse Receipt Added Successfully!');
							setTimeout(function(){
							window.location.reload(1);
							}, 2000);
						}
					})                   
                } else {
                    return false;
                }
            } else{
				var data = $("#pdfvals1").serialize();					
				$.ajax({
					url: '<?php echo Yii::app()->createAbsoluteUrl('bills/autoReceiptBillWithoutPo'); ?>',
					method: 'POST',
					data:  data,
					dataType: 'json',											
					success: function(response) {
						console.log(response);
						$().toastmessage('showSuccessToast', 'Billed details added successfully!');	
						setTimeout(function(){
							window.location.reload(1);
						}, 2000);						
					}
				}) 
			}
        });

   
</script>