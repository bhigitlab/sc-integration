
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/agacpdfstyle.css">
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/d2rtablestyles.css" />
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/qgeneratorpdfstyle.css">

<div class="bg_doted">
    <!--page 1 -->
    <?php 
    $real_theme_asset_url = realpath(Yii::app()->basePath . "/../themes/assets/"); 
    $alternative_image_path = Yii::app()->theme->basePath . '/images/quotation-file/logo.png';
    $app_root = YiiBase::getPathOfAlias('webroot');
    $site_hosturl = str_replace('/index.php','', Yii::app()->createAbsoluteUrl('/'));
    $image_base_url=Yii::app()->theme->baseUrl .'/assets/default/logo.png'; 
    $theme_asset_url = $site_hosturl."/themes/assets/" ;
    if(file_exists(Yii::app()->theme->basePath .'/images/quotation-file/logo.png')){
        $image_url=Yii::app()->theme->baseUrl .'/images/quotation-file/logo.png';
    }else if( file_exists($app_root . "/themes/assets/logo.png") ){
        $image_url=$theme_asset_url .'logo.png';
    }else{
        $image_url=$theme_asset_url .'default-logo.png';
    }
   
    ?>
    <?php if($is_top_image ==0){ ?>
    <div class="banner_holder">
        <div id="content">
            <img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/quotation-file/firstpage.png" alt="img" height="1300" />
        </div>
        <div id="background"></div>
    </div>
    <?php } ?>
    <!-- page 2 -->
    <div class="p-5">
        <div class="red_border p_2 detail_sec">
            <table class="detail_table bg-white">
                <tr>
                    <!-- client -->
                    <td width="150" class="bg_light_grey text_title border-bottom pb_1">CLIENT</td>
                    <!-- details -->
                    <td class="bg_light_grey border-bottom">
                        <div class="text_value"><?php echo $client; ?></div>
                        <?php $location_name = Location::model()->findByPk($model->location_id); ?>
                        <div class="text_value">
                            <?php
                            echo (isset($model->address) && $model->address !== '')? $model->address:$location_name['name'];
                            ?>
                        </div>
                        <div><span class="text_title">Ph: </span><span class="text_value"><?php echo $model->phone_no; ?></span></div>
                    </td>
                    <!-- AGAC LOGO AND CONTACT DETAILS -->
                    <td width="50%" align="right" class="text-right" rowspan="5">
                        <img src="<?php echo $image_url; ?>" alt="Agac" height="75" />
                        <div class="text_value mt_2">
                            <div>Agac Interiors</div>
                            <div>Near Suzuki Bike showroom</div>
                            <div>Kuttoor, Thiruvalla</div>
                        </div>
                        <div class="mt_2">
                            <span class="text_title">Ph: </span>
                            <span class="text_value">+91 9400485915</span>
                        </div>
                        <div class="mt_2">
                            <span class="text_title">Email- </span>
                            <a class="text_value" href="mailto:office1agac@gmail.com">office1agac@gmail.com</a>
                        </div>
                        <div class="mt_2">
                            <a class="text_value" href="www.agacinteriors.com" target="_blak">www.agacinteriors.com</a>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="bg_light_grey text_title border-bottom">REF. NO</td>
                    <td class="text_value bg_light_grey border-bottom"><?php echo isset($model->invoice_no) ? $model->invoice_no : ''; ?></td>
                </tr>
                <tr>
                    <td class="text_title bg_light_grey border-bottom">DATE</td>
                    <td class="text_value bg_light_grey border-bottom"><?php echo isset($model->date_quotation) ? date('d-m-Y', strtotime($model->date_quotation)) : ''; ?></td>
                </tr>
                <tr>
                    <td class="text_title bg_light_grey">SUBJECT</td>
                    <td class="text_value bg_light_grey pb_1">ESTIMATION FOR RESIDENTIAL INTERIOR WORK ,<?php echo $location_name['name']; ?></td>
                </tr>
                <tr>
                    <td class="text_title bg_light_grey"></td>
                    <td class="text_value bg_light_grey pb_1"></td>
                </tr>
            </table>
        </div>
    </div>

    <!-- page 3 -->
    <div class="dot_bg">
        <div class="table_h">
            <table class="item_table">
        <!-- <table cellpadding="10" class="table item_table"> -->
            <thead>
                <tr>
                    <th colspan="7"></th>
                </tr>
            </thead>
            <tbody>
                <tr class="thead">

                    <th>SL NO</th>
                    <th>ITEM DESCRIPTION</th>
                    <th>MATERIAL</th>
                    <th>UNIT</th>
                    <th>QTY</th>                                    
                    <th>MRP AMOUNT</th>
                    <th>DISCOUNT AMOUNT</th>
                    

                </tr>
                <?php
                $sql = 'SELECT * FROM `jp_quotation_section` '
                        . 'WHERE `qtn_id` = ' . $qtid
                        . ' AND revision_no = "' . $rev_no . '"';
                $sectionArray = Yii::app()->db->createCommand($sql)->queryAll();
                $i = 1;
                foreach ($sectionArray as $key => $value) {
                    $sql = 'SELECT * FROM `jp_quotation_gen_category` '
                            . 'WHERE `qid` = ' . $qtid . ' AND `section_id` = ' . $value["id"]
                            . ' AND revision_no = "' . $rev_no . '"';
                    $subsectionArray = Yii::app()->db->createCommand($sql)->queryAll();
                    $unit = Unit::model()->findByPk($value['unit']);
                    $secsum = Controller::getSectionSum($value['id'], $qtid, $rev_no);
                    if($secsum > 0){
                    ?>
                    <tr>
                        <th colspan="<?php echo empty($subsectionArray) ? '2' : '7' ?>" style="background:#c00000;color:#fff;"><?php echo $value['section_name'] ?></th>
                        <?php if (empty($subsectionArray)) { ?>
                            <td style="background:#c00000;color:#fff;"></td>
                            <td style="background:#c00000;color:#fff;"><?php echo isset($unit) ? $unit->unit_name : "" ?></td>
                            <td style="background:#c00000;color:#fff;"><?php echo $value['quantity_nos'] ?></td>
                            <td style="background:#c00000;color:#fff;"><?php echo $value['mrp'] ?></td>
                            <td style="background:#c00000;color:#fff;"><?php echo $value['amount_after_discount'] ?></td>
                            
                        <?php } ?>
                        
                    </tr>
                    <?php
                    }
                    foreach ($subsectionArray as $key => $value1) {
                        $sql = 'SELECT * FROM `jp_sales_quotation` WHERE `parent_type` = 0 '
                                . ' AND `parent_id` = ' . $value1['id']
                                . ' AND revision_no="' . $rev_no . '" AND `deleted_status` = 1';
                        $itemArray = Yii::app()->db->createCommand($sql)->queryAll();

                        $sql = 'SELECT * FROM `jp_quotation_gen_worktype` '
                                . ' WHERE `qid` = ' . $value1["qid"]
                                . ' AND `section_id` = ' . $value1["section_id"]
                                . ' AND `category_label_id` = ' . $value1["id"]
                                . ' AND revision_no = "' . $rev_no . '"';
                        $subitemArray = Yii::app()->db->createCommand($sql)->queryAll();
                        $unit = Unit::model()->findByPk($value1['unit']);
                        ?>
                        <tr>
                            <th  style="background:#c5d9f1;"><?php echo $i; ?></th>
                            <th colspan="<?php echo (empty($itemArray)) && (empty($subitemArray)) ? '1' : '6' ?>" style="background:#c5d9f1;"><?php echo $value1['category_label'] ?></th>
                            <?php if ((empty($itemArray)) && (empty($subitemArray))) { ?>
                                <td  style="background:#c5d9f1;"></td>
                                <td  style="background:#c5d9f1;"><?php echo isset($unit) ? $unit->unit_name : "" ?></td>
                                <td  style="background:#c5d9f1;"><?php echo $value1['quantity_nos'] ?></td>
                                <td  style="background:#c5d9f1;"><?php echo $value1['mrp'] ?></td>
                                <td  style="background:#c5d9f1;"><?php echo $value1['amount_after_discount'] ?></td>
                                
                            <?php } ?>                                                                                        
                        </tr>

                        <?php
                        foreach ($itemArray as $key => $value2) {
                            ?>
                            <tr>
                                <td><?php echo '*' ?></td>
                                <td colspan="6"><?php echo $value2['worktype_label'] ?></td>
                            </tr>

                        <?php }
                        ?>
                        <?php
                        $letters = range('a', 'z');
                        $k = 0;
                        foreach ($subitemArray as $key => $value3) {
                            $sql = 'SELECT * FROM `jp_sales_quotation` '
                                    . 'WHERE `parent_type` = 1 AND `parent_id` = ' . $value3['id']
                                    . ' AND revision_no="' . $rev_no . '" AND `deleted_status` = 1';
                            $wrktypeitemArray = Yii::app()->db->createCommand($sql)->queryAll();
                            $unit = Unit::model()->findByPk($value3['unit']);
                            ?>
                            <tr class="td_group">
                                <th><?php echo $letters[$k]; ?></th>
                                <td colspan="<?php echo empty($wrktypeitemArray) ? '1' : '6' ?>"><b><?php echo $value3['worktype_label'] ?></b></td>
                                <?php if (empty($wrktypeitemArray)) { ?>
                                    <td><?php echo isset($unit) ? $unit->unit_name : "" ?></td>
                                    <td><?php echo $value3['quantity_nos'] ?></td>
                                    <td><?php echo $value3['mrp'] ?></td>
                                    <td><?php echo $value3['amount_after_discount'] ?></td>                                                    
                                <?php } ?>
                                
                            </tr>
                            <?php if ($value3['description'] != "") { ?>
                                <tr class="td_group">
                                    <th>*</th>
                                    <td colspan="6"><?php echo $value3['description'] ?></td>
                                    
                                </tr>
                                <?php
                            }
                            $sum = 0;
                            foreach ($wrktypeitemArray as $key => $value4) {
                                $sum += $value4['amount_after_discount'];
                                $unit = Unit::model()->findByPk($value4['unit']);

                                $desc = ($value4['description'] !="")?" - ".$value4['description']:"";
                                if($value4['description']=="" && ($value4['shutterwork_description'] !="" || $value4['carcass_description']!="")){
                                    $desc = " - <b>Shutter:</b> ".$value4['shutterwork_description']." , <b>Carcass:</b> ".$value4['carcass_description'];
                                }
                                 
                                ?>
                                <tr class="td_group">
                                    <th><?php echo '*' ?></th>
                                    <td>
                                        <?php 
                                        if($value4['item_name']!=""){
                                            echo $value4['item_name'];
                                        }else{
                                            echo $value4['worktype_label'].$desc;
                                        }                                                                                
                                        ?>
                                    </td>
                                    <td><?php 
                                    $item_master = QuotationItemMaster::model()->findByPk($value4['item_id']);
                                    
                                    if ($value4['shutterwork_material']) {
                                        $shutter_material_id = $item_master['shutter_material_id'];                                        
                                        $specification = Specification::model()->findByPk($shutter_material_id);
                                        $brand = Brand::model()->findByPk($specification ['brand_id']);
                                        $category = PurchaseCategory::model()->findByPk($specification ['cat_id']);
                                        $smaterial_name = $category["category_name"].'-'.$brand["brand_name"].'-'.$specification["specification"];
                                    }
                        
                                    if ($value4['carcass_material']) {
                                        $carcass_material_id = $item_master['carcass_material_id'];
                                        $specification = Specification::model()->findByPk($carcass_material_id);
                                        $brand = Brand::model()->findByPk($specification ['brand_id']);
                                        $category = PurchaseCategory::model()->findByPk($specification ['cat_id']);
                                        $cmaterial_name = $category["category_name"].'-'.$brand["brand_name"].'-'.$specification["specification"];
                                    }
                                    if ($value4['material']) {
                                        $material_ids = $item_master['material_ids'];
                                        $specification = Specification::model()->findByPk($material_ids);
                                        $brand = Brand::model()->findByPk($specification ['brand_id']);
                                        $category = PurchaseCategory::model()->findByPk($specification ['cat_id']);
                                        $material_name = $category["category_name"].'-'.$brand["brand_name"].'-'.$specification["specification"];
                                    }

                                     

                                    if ($value4['shutterwork_material']) {
                                        if($value4['shutterwork_material'] == $value4['carcass_material']){
                                            echo $smaterial_name;
                                        }else{
                                            echo $smaterial_name. " , " .$cmaterial_name;
                                        }
                                        
                                    } 

                                    if ($value4["material"]) {
                                        echo $material_name;
                                    }
                                    
                                    
                                    
                                    ?></td>
                                    <td><?php echo isset($unit) ? $unit->unit_name : "" ?></td>
                                    <td><?php echo $value4['quantity_nos'] ?></td>
                                    <td class="text-right"><?php echo $value4['mrp'] ?></td>
                                    <td class="text-right"><?php echo $value4['amount_after_discount'] ?></td>
                                    
                                </tr>

                                <?php
                            }
                            if (!empty($wrktypeitemArray)) {
                                ?>
                                <?php
                            } $k++;
                        }
                        ?>
                        <?php
                        $i++;
                        ?>
                        
                        <?php
                    }
                    ?>
                
                <?php } ?>
                
                <tr>
                    <th colspan="6" class="text-right">TOTAL PRICE</th>
                    <th class="text-right">
                        <?php
                        $total_amount = Controller::getMrpSum($qtid, $rev_no,NULL);
                        echo Controller::money_format_inr($total_amount, 2);
                        ?>
                    </th>
                </tr>
                <tr>
                    <th colspan="6" class="text-right"> OFFER PRICE</th>
                    <th class="text-right">
                        <?php
                        $total_amount = Controller::getQuotationSum($qtid, $rev_no,NULL);
                        echo Controller::money_format_inr($total_amount, 2);
                        ?>
                    </th>
                </tr>
                <tr>
                    <?php
                    $sql = 'SELECT * FROM jp_quotation_revision '
                        . ' WHERE qid='.$qtid
                        . ' AND revision_no="'.$rev_no.'"';
                    $revision_model = Yii::app()->db->createCommand($sql)->queryRow();
                    $total_tax = $revision_model['sgst_amount']+
                    $revision_model['cgst_amount']+
                    $revision_model['igst_amount'];

                    $total_tax_percent = $revision_model['sgst_percent']+
                    $revision_model['cgst_percent']+
                    $revision_model['igst_percent'];
                    $total_amoun_withtax = $total_amount + $total_tax;
                    ?>
                    <th colspan="6" class="text-right">
                        GST  <?php if($total_tax_percent >0) {
                        echo "(".$total_tax_percent."%)";
                        }else{
                        echo "";
                        } ?>
                    </th>
                    <th  class="text-right">
                        <?php
                        echo Controller::money_format_inr($total_tax, 2);
                        ?>
                    </th>
                </tr>
                <tr>
                    <th colspan="6" class="text-right">TOTAL AMOUNT(INCLUDING GST)</th>
                    <th class="text-right">
                        <?php                                         
                        echo Controller::money_format_inr($total_amoun_withtax , 2);                                        
                        ?>                                        
                    </th>
                </tr>                                                                
                <tr>
                    <th colspan="7" style="color:#f00;font-size:14px">
                        <h5><b>*Quotation valid only for one month.</b></h5>
                        <h5><b>*Material Price changes will affect the rate</b></h5>
                    </th>
                </tr>
            </tbody>
        </table>
        </div>
    </div>



      <!-- <pagebreak /> -->
    <!-- page 4 -->
    <!-- MATERIAL DESCRIPTION -->
    <pagebreak>
    <div class="dot_bg ">
        <div class="table_h">
        <?php
            $sql = "SELECT * FROM `jp_sales_quotation_gallery` WHERE qtid=".$qtid;
            $images = Yii::app()->db->createCommand($sql)->queryAll();
            if(!empty($images)) {
        ?>
        <table class="item_table">
            <tbody> 
                <tr>
                    <th colspan="2" class="title">KITCHEN ACCESSORIES</th>
                </tr>
                <tr>                
                    <th>Images</th>
                    <th>Name</th>
                </tr>                
                <?php
                $i=1;
                foreach ($images as $key => $value) { ?>
                <tr>
                    
                    <td>
                        <?php 
                        if ($value["image"] != '') {
                            $path = realpath(Yii::app()->basePath . '/../uploads/quotation_gallery/' . $value["image"]);
                            if (file_exists($path)) {
                                echo CHtml::tag('img',
                                    array(
                                        'src' => Yii::app()->request->baseUrl . "/uploads/quotation_gallery/" . $value["image"],
                                        'alt' => '',
                                        'class' => 'pop',
                                        'modal-src' => Yii::app()->request->baseUrl . "/uploads/quotation_gallery/" . $value["image"],
                                        'width'=>'100px'
                                    )
                                );
                            }
                        } 
                        ?>
                    </td>
                    <td><?php echo $value['caption']?></td>
                </tr>
                <?php $i++; } ?> 
            </tbody>               
        </table>
        <?php } ?>
<br>
            <table class="item_table">
                <tbody>
                    <tr>
                        <th colspan="3" class="title">MATERIAL SPECIFICATION</th>
                    </tr>
                    <tr>
                        <th>CATEGORY</th>
                        <th>BRAND</th>
                        <th>SPECIFICATION</th>                        
                    </tr>
                    <?php 
                    
                    foreach ($materials as $key => $material) {                                                
                        $specification = Specification::model()->findByPk($material);
                        $brand = Brand::model()->findByPk($specification ['brand_id']);
                        $category = PurchaseCategory::model()->findByPk($specification ['cat_id']);                                                
                    ?>
                        <tr>                              
                            <td class="text_title">
                                <?php echo $category["category_name"]; ?>
                            </td>                             
                            <td class="text_title">
                                <?php echo $brand["brand_name"]; ?>                                
                            </td>                           
                            <td class="text_title">
                                <?php echo $specification["specification"]; ?>
                            </td>
                        </tr>
                    <?php  } ?>
                </tbody>
            </table>
            <br /><br />
            <!-- ITEM NOT INCLUDED IN THE ESTIMATE -->
            <table class="item_table no-border">
                <tbody>
                    <tr>
                        <th class="title">ITEM NOT INCLUDED IN THE ESTIMATE</th>
                    </tr>
                    <tr>
                        <td>* Electrical Appliances</td>
                    </tr>
                    <tr>
                        <td>* Plumping and Electrical work</td>
                    </tr>
                    <tr>
                        <td>* Civil work if any</td>
                    </tr>
                    <tr>
                        <td>* Demolishing work</td>
                    </tr>
                    <tr>
                        <td>* Putty, Painting and Tile work</td>
                    </tr>
                </tbody>
            </table>
            <table class="item_table no-border">
                <tbody>
                    <tr>
                        <th colspan="3" class="title">SERVICES</th>
                    </tr>
                    <tr>
                        <td>* We will attend your services within 2 days from the date of service request</td>
                    </tr>
                    <tr>
                        <td>
                            * Replacement and repair of products/ product parts which are not covered under warranty will be
                            charged extra.
                        </td>
                    </tr>
                    <tr>
                        <td>* Service charges will be chargeable as per the estimate</td>
                    </tr>
                </tbody>
            </table>
            <br /><br />
            <!-- ABOUT OUR PRODUCT WARRANTY -->
            <table class="item_table no-border">
                <tbody>
                    <tr>
                        <th colspan="3" class="title">ABOUT OUR PRODUCT WARRANTY</th>
                    </tr>
                    <tr>
                        <th>AGAC INTERIORS Provides Warranty for the product as follows:</th>
                    </tr>
                    <tr>
                        <td>* 5 Year service warranty for all laminated marine ply under normal usage.</td>
                    </tr>
                    <tr>
                        <td>* 5 Year replacement warranty for all laminated marine ply wood under normal usage.</td>
                    </tr>
                    <tr>
                        <td>
                            * 5 Year service warranty for hard wares (Hinges and sliders) against mechanical and manufacturing defects Provided by the manufacturer.
                        </td>
                    </tr>
                    <tr>
                        <td>
                            * 15-year replacement warranty for kitchen basket against mechanical and manufacturing defects provide by the manufacturer
                        </td>
                    </tr>
                    <tr>
                        <td>* Products which are not included in the above doesnt come under the scope of warranty claim.</td>
                    </tr>
                    <tr>
                        <td>* The client has to submit the original bill (estimation and cash receipt copy) during the time of warranty claim</td>
                    </tr>
                    <tr>
                        <td>* Above mentioned warranty and services will be given only after full settlement of payment</td>
                    </tr>
                </tbody>
            </table>
            <br>
            <table class="item_table no-border">
                <thead>
                    <tr>
                        <th class="subtitle">AGAC INTERIORS Warranty will not be applicable on:</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>* Alteration or modification of the product.</td>
                    </tr>
                    <tr>
                        <td>* Third party fabric, upholstery, foam, accessory or third-party material applied to products</td>
                    </tr>
                    <tr>
                        <td>* Damage caused by a carrier other than AGAC interiors</td>
                    </tr>
                    <tr>
                        <td><b><u>Damage caused by water, includes: -</u></b></td>
                    </tr>
                    
                        <tr>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;* Due to flood or any other natural calamity,</td>
                        </tr>
                        <tr>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;* Water leakage (without proper maintenance) in sink or wash area</td>
                        </tr>
                    <tr>
                        <td>*  Damages due to rough use are not include.</td>
                    </tr>
                    <tr>
                        <td>* Product not installed by AGAC INTERIORS</td>
                    </tr>
                </tbody>
            </table>
            <br /><br />
            <!-- TERMS AND CONDITIONS -->
            <table class="no-border terms_table">
                <tbody>
                    <tr>
                        <th class="title">TERMS AND CONDITIONS</th>
                    </tr>
                    <tr>
                        <td>
                            <?php
                            $terms = $terms = Yii::app()->db->createCommand("SELECT * FROM `jp_terms_conditions` WHERE `template_id` = 1")->queryRow();
                            echo $terms['terms_and_conditions'];
                            ?>
                        </td>
                    </tr>
                </tbody>
            </table>
            <br /><br />
            <table class="item_table no-border">
                <tbody>
                    
                    <tr>
                        <th class="title">WHY CHOOSE AGAC INTERIORS?</th>
                    </tr>
                    <tr>
                        <td>TRUSTED BY CLIENTS SINCE 2011</td>
                    </tr>
                    <tr>
                        <td>HIGH QUALITY PRODUCT</td>
                    </tr>
                    <tr>
                        <td>ON TIME DELIVERY</td>
                    </tr>
                    <tr>
                        <td>100% CUSTOMER SATISFACTION</td>
                    </tr>
                    <tr>
                        <td>LATEST TECHNOLOGY</td>
                    </tr>
                    <tr>
                        <td>LIFETIME WARRANTY AND SERVICE</td>
                    </tr>
                </tbody>
            </table>
            <br /><br />
            <table class="item_table no-border">
                <tbody>
                    <tr>
                        <th colspan="2" class="title">BANK DETAILS</th>
                    </tr>
                    <tr>
                        <td width="100" class="text_title">Name:</td>
                        <td>AGAC Interiors</td>
                    </tr>
                    <tr>
                        <td class="text_title">Bank:</td>
                        <td>Canara Bank</td>
                    </tr>
                    <tr>
                        <td class="text_title">Branch:</td>
                        <td>Mallapally</td>
                    </tr>
                    <tr>
                        <td class="text_title">A/c no:</td>
                        <td>2358201000195</td>
                    </tr>
                    <tr>
                        <td class="text_title">IFSC Code:</td>
                        <td>CNRB0002358</td>
                    </tr>
                    <tr>
                        <td class="text_title">GST NO:</td>
                        <td>32AZAPA7570JIZL</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

