<?php

class WarehousereceiptController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';
    public $tblpx = 'jp_';

    public $sl_no = null;




    public function init()
    {
        $this->tblpx = Yii::app()->db->tablePrefix;
    }
    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array(
                'allow',  // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view'),
                'users' => array('@'),
            ),
            array(
                'allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'deletedWarehouseView', 'update', 'getItemCategory', 'getParent', 'savewarehousereceipt', 'ajax', 'createnewreceipt', 'receiptitem', 'getUnit', 'updatereceiptitem', 'removeitem', 'saveaspdf', 'saveasexcel', 'getitem', 'DespatchPurchaseBilllist', 'getBatch', 'getUnits', 'StockList', 'CheckEditBillDespatch', 'GetItemsByPurchaseOrDespatch', 'getallunits', 'savereceiptitem', 'updatereceiptitems', 'getBillOrDespatchId', 'getTransferType', 'warehoueReceiptDelete', 'getUnitconversionFactor','getDynamicReceipt','getDynamicReceiptBasedOnDispatch'),
                'users' => array('@'),
            ),
            array(
                'allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('admin'),
            ),
            array(
                'deny',  // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($receiptid)
    {
        $warehouse_address = "";
        $model = $this->loadModel($receiptid);
        $warehousemodel = Warehouse::model()->findByPk($model->warehousereceipt_warehouseid);
        if ($warehousemodel === null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        } else {
            $warehouse_address = $warehousemodel->warehouse_address;
        }

        if (filter_input(INPUT_GET, 'export') == 'pdf') {
            $this->logo = $this->realpath_logo;
            $mPDF1 = Yii::app()->ePdf->mPDF();
            $mPDF1 = Yii::app()->ePdf->mPDF('', 'A4');
                
            $sql = 'SELECT template_name '
                    . 'FROM jp_quotation_template '
                    . 'WHERE status="1"';
            $selectedtemplate =Yii::app()->db->createCommand($sql)->queryScalar();
                
            $template = $this->getTemplate($selectedtemplate);                
            $mPDF1->SetHTMLFooter($template['footer']);                
            $sql = 'SELECT template_name '
                        . 'FROM jp_quotation_template '
                        . 'WHERE status="1"';
			$selectedtemplate =Yii::app()->db->createCommand($sql)->queryScalar();
			if ($selectedtemplate == 'template1'){
				$mPDF1->AddPage('','', '', '', '', 0,0,10, 30, 10,0);
			}else{
                $mPDF1->SetHTMLHeader($template['header']);
				$mPDF1->AddPage('','', '', '', '', 0,0,40, 30, 10,0);
			}
            $mPDF1->WriteHTML($this->renderPartial('view', array(
                'model' => $model, 'warehouse_address' => $warehouse_address
            ), true));
            $mPDF1->Output('Receipt.pdf', 'D');
        } elseif (filter_input(INPUT_GET, 'export') == 'csv') {
            $exported_data = $this->renderPartial('view', array(
                'model' => $model, 'warehouse_address' => $warehouse_address
            ), true);
            $export_filename = 'Receipt' . date('Ymd_His');
            ExportHelper::exportGridAsCSV($exported_data, $export_filename);
        } else {
            $this->render('view', array(
                'model' => $model, 'warehouse_address' => $warehouse_address
            ));
        }
    }
    public function actiondeletedWarehouseView()
    {
        $warehouse_address = "";
        $receiptid = $_GET['receiptid'];
        $model = WarehouseTransferDeletedLog::model()->findByAttributes(array('warehouse_transfer_type_deleted_id' => $receiptid, 'warehouse_transfer_type_delete' => 1));
        if ($model != '') {
            $warehousemodel = Warehouse::model()->findByPk($model->warehouseid_to);
        } else {
            throw new CHttpException(404, 'The requested page does not exist.');
        }
        if ($warehousemodel === null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        } else {
            $warehouse_address = $warehousemodel->warehouse_address;
        }

        $this->render('deleted_warehousereceipt_view', array(
            'model' => $model, 'warehouse_address' => $warehouse_address
        ));

        // $this->render('view',array(
        // 	'model'=>$this->loadModel($receiptid),
        // ));
    }
    public function actionsavereceiptitem()
    {
        $model = new Warehousereceipt;
        $project = Yii::app()->db->createCommand("SELECT DISTINCT pid,name FROM {$this->tblpx}projects WHERE company_id=" . Yii::app()->user->company_id . " ORDER BY name")->queryAll();
        $despatches = Yii::app()->db->createCommand("SELECT DISTINCT warehousedespatch_id,warehousedespatch_no FROM {$this->tblpx}warehousedespatch ORDER BY warehousedespatch_id")->queryAll();
        $bills = Yii::app()->db->createCommand("SELECT DISTINCT bill_id,bill_number FROM {$this->tblpx}bills ORDER BY bill_id")->queryAll();

        $specification = $this->actionGetItemCategory($parent = 0, $spacing = '', $user_tree_array = '');
        if (isset($_POST['warehousereceipt_id'])) {
            Yii::app()->user->setFlash('success', "Warehouse Receipt details added successfully!");
            $this->redirect(array('index'));
        } else {
            Yii::app()->user->setFlash('error', "Some problem occured for Details added in warehouse Stock.");
            $this->redirect(array('index'));
        }

        $this->render('create', array(
            'model' => $model,
            'dataProvider' => $model->search(Yii::app()->user->id),
            'specification' => $specification,
            'project' => $project,
            'despatches' => $despatches,
            'bills' => $bills
        ));
    }
    public function actionupdatereceiptitems()
    {

        $receiptid = $_POST['warehousereceipt_id'];
        $model = $this->loadModel($receiptid);
        $warehouse_id = $model->warehousereceipt_warehouseid;
        $warehouseid_from = $model->warehousereceipt_warehouseid_from;
        $warehousereceiptItem_check = WarehousereceiptItems::model()->findAll(array('condition' => 'warehousereceipt_id = "' . $receiptid . '"'));
        if (count($warehousereceiptItem_check) == 0) {
            $readonly = false;
        } else {
            $readonly = true;
        }
        if (empty($model)) {
            $this->redirect(array('warehousereceipt/index'));
        }
        $project = Yii::app()->db->createCommand("SELECT DISTINCT pid,name FROM {$this->tblpx}projects WHERE company_id=" . Yii::app()->user->company_id . " ORDER BY name")->queryAll();
        $item_model = WarehousereceiptItems::model()->findAll(array("condition" => "warehousereceipt_id = '$receiptid'"));
        $DespatchPurchaseBilllist = $this->GetDespatchPurchaseBilllist($warehouse_id, $warehouseid_from);
        $specification = $this->actionGetItemCategory($parent = 0, $spacing = '', $user_tree_array = '');
        if (isset($_POST['warehousereceipt_id'])) {
            Yii::app()->user->setFlash('success', "Warehouse Receipt details added successfully!");
            $this->redirect(array('index'));
        } else {
            Yii::app()->user->setFlash('error', "Some problem occured for Details added in warehouse Stock.");
            $this->redirect(array('index'));
        }

        $this->render('update', array(
            'model' => $model,
            'dataProvider' => $model->search(Yii::app()->user->id),
            'specification' => $specification,
            'DespatchPurchaseBilllist' => $DespatchPurchaseBilllist,
            'item_model' => $item_model,
            'project' => $project,
            'readonly' => $readonly
        ));
    }


    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {

        $model = new Warehousereceipt;
        $project = Yii::app()->db->createCommand("SELECT DISTINCT pid,name FROM {$this->tblpx}projects WHERE company_id=" . Yii::app()->user->company_id . " ORDER BY name")->queryAll();
        $despatches = Yii::app()->db->createCommand("SELECT DISTINCT warehousedespatch_id,warehousedespatch_no FROM {$this->tblpx}warehousedespatch ORDER BY warehousedespatch_id")->queryAll();
        $bills = Yii::app()->db->createCommand("SELECT DISTINCT bill_id,bill_number FROM {$this->tblpx}bills ORDER BY bill_id")->queryAll();

        $specification = $this->actionGetItemCategory($parent = 0, $spacing = '', $user_tree_array = '');       
        $this->render('create', array(
            'model' => $model,
            'dataProvider' => $model->search(Yii::app()->user->id),
            'specification' => $specification,
            'project' => $project,
            'despatches' => $despatches,
            'bills' => $bills
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($receiptid)
    {

        $model = $this->loadModel($receiptid);
        $warehouse_id = $model->warehousereceipt_warehouseid;
        $warehouseid_from = $model->warehousereceipt_warehouseid_from;
        $warehousereceiptItem_check = WarehousereceiptItems::model()->findAll(array('condition' => 'warehousereceipt_id = "' . $receiptid . '"'));
        if (count($warehousereceiptItem_check) == 0) {
            $readonly = false;
        } else {
            $readonly = true;
        }
        if (empty($model)) {
            $this->redirect(array('warehousereceipt/index'));
        }
        $project = Yii::app()->db->createCommand("SELECT DISTINCT pid,name FROM {$this->tblpx}projects WHERE company_id=" . Yii::app()->user->company_id . " ORDER BY name")->queryAll();
        $item_model = WarehousereceiptItems::model()->findAll(array("condition" => "warehousereceipt_id = '$receiptid' AND warehousereceipt_transfer_type_item_id IS NULL"));
        $DespatchPurchaseBilllist = $this->GetDespatchPurchaseBilllist($warehouse_id, $warehouseid_from);
        $specification = $this->actionGetItemCategory($parent = 0, $spacing = '', $user_tree_array = '');

        $this->render('update', array(
            'model' => $model,
            'dataProvider' => $model->search(Yii::app()->user->id),
            'specification' => $specification,
            'DespatchPurchaseBilllist' => $DespatchPurchaseBilllist,
            'item_model' => $item_model,
            'project' => $project,
            'readonly' => $readonly
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $specification = '';
        $specification = $this->actionGetItemCategory($parent = 0, $spacing = '', $user_tree_array = '');



        $model = new Warehousereceipt('search');

        $query = "";
        if (Yii::app()->user->role != 1) {
            $query = " AND FIND_IN_SET(" . Yii::app()->user->id . ", assigned_to)";
        } else {
            $query = "";
        }
        $warehouse = Yii::app()->db->createCommand("SELECT DISTINCT warehouse_id,warehouse_name FROM {$this->tblpx}warehouse WHERE company_id=" . Yii::app()->user->company_id . " " . $query . " ORDER BY warehouse_id")->queryAll();
        $vendor = Yii::app()->db->createCommand("SELECT DISTINCT vendor_id,name FROM {$this->tblpx}vendors WHERE company_id=" . Yii::app()->user->company_id . " ORDER BY vendor_id")->queryAll();
        $clerk = Yii::app()->db->createCommand("SELECT DISTINCT userid,first_name,last_name FROM {$this->tblpx}users WHERE company_id=" . Yii::app()->user->company_id . " ORDER BY userid")->queryAll();
        $model->unsetAttributes();  // clear any default values
        $sql1 = 'SELECT DISTINCT `warehousereceipt_id`,`warehousereceipt_no`,
                            `warehousereceipt_despatch_id`, `warehousereceipt_bill_id`,
                            `warehousereceipt_warehouseid`,`warehousereceipt_warehouseid_from`,
                            `warehousereceipt_clerk`,`warehousereceipt_transfer_type`,delete_status,warehousereceipt_date 
                        FROM `jp_warehousereceipt`';
        $sql2 = 'SELECT DISTINCT `warehouse_transfer_type_deleted_id`,
                                `warehouse_transfer_type_deleted_no`,`warehousereceipt_despatch_id`,
                                `warehousereceipt_bill_id`,`warehouseid_to`,
                                `warehouseid_from`,`warehousereceipt_clerk`,`warehousereceipt_transfer_type`,1 as delete_status,warehouse_transfer_date 
                        FROM `jp_warehouse_transfer_deleted_log`';





        if (isset($_GET['warehouse']) || isset($_GET['transfer_type']) || isset($_GET['bill_or_despatch_id']) || isset($_GET['warehouse_from']) || isset($_GET['vendor_id']) || isset($_GET['clerk']) || isset($_GET['receipt_no']) || isset($_GET['item_id'])) {
            $model->warehousereceipt_warehouseid = $_GET['warehouse'];
            $model->warehousereceipt_transfer_type = $_GET['transfer_type'];
            $model->warehousereceipt_warehouseid_from = $_GET['warehouse_from'];
            $model->warehousereceipt_clerk = $_GET['clerk'];
            $model->warehousereceipt_no = $_GET['receipt_no'];
            $model->date_from = $_GET['date_from'];
            $model->date_to = $_GET['date_to'];
            if (isset($_GET['warehouse']) && !empty($_GET['warehouse'])) {
                $sql1 .= ' WHERE warehousereceipt_warehouseid =' . $_GET['warehouse'];
                $sql2 .= ' WHERE warehouseid_to =' . $_GET['warehouse'];
            } else {
                $sql1 .= ' WHERE 1=1 ';
                $sql2 .= ' WHERE 1=1 ';
            }
            if (isset($_GET['warehouse_from']) && !empty($_GET['warehouse_from'])) {
                $sql1 .= ' AND warehousereceipt_warehouseid_from =' . $_GET['warehouse_from'];
                $sql2 .= ' AND warehouseid_from =' . $_GET['warehouse_from'];
            } else {
                $sql1 .= ' AND 1=1 ';
                $sql2 .= ' AND 1=1 ';
            }
            if (isset($_GET['clerk']) && !empty($_GET['clerk'])) {
                $sql1 .= ' AND warehousereceipt_clerk =' . $_GET['clerk'];
                $sql2 .= ' AND warehousereceipt_clerk =' . $_GET['clerk'];
            } else {
                $sql1 .= ' AND 1=1 ';
                $sql2 .= ' AND 1=1 ';
            }
            if (isset($_GET['receipt_no']) && !empty($_GET['receipt_no'])) {
                $sql1 .= ' AND warehousereceipt_no ="' . $_GET['receipt_no'] . '"';
                $sql2 .= ' AND warehouse_transfer_type_deleted_no ="' . $_GET['receipt_no'] . '"';
            } else {
                $sql1 .= ' AND 1=1 ';
                $sql2 .= ' AND 1=1 ';
            }

            if (isset($_GET['date_from']) || isset($_GET['date_to'])) {
                if ((isset($_GET['date_from']) && !empty($_GET['date_from'])) && ((isset($_GET['date_to']) && empty($_GET['date_to'])) || !isset($_GET['date_to']))) {
                    $sql1 .= ' AND warehousereceipt_date >="' . date('Y-m-d', strtotime($_GET['date_from'])) . '"';
                    $sql2 .= ' AND warehouse_transfer_date >="' . date('Y-m-d', strtotime($_GET['date_from'])) . '"';
                } else if ((isset($_GET['date_to']) && !empty($_GET['date_to'])) && ((isset($_GET['date_from']) && empty($_GET['date_from'])) || !isset($_GET['date_from']))) {
                    $sql1 .= ' AND warehousereceipt_date <="' . date('Y-m-d', strtotime($_GET['date_to'])) . '"';
                    $sql2 .= ' AND warehouse_transfer_date <="' . date('Y-m-d', strtotime($_GET['date_to'])) . '"';
                } else if ((isset($_GET['date_to']) && !empty($_GET['date_to'])) && (isset($_GET['date_from']) && !empty($_GET['date_from']))) {
                    $sql1 .= ' AND warehousereceipt_date >="' . date('Y-m-d', strtotime($_GET['date_from'])) . '" AND warehousereceipt_date <="' . date('Y-m-d', strtotime($_GET['date_to'])) . '"';
                    $sql2 .= ' AND warehouse_transfer_date >="' . date('Y-m-d', strtotime($_GET['date_from'])) . '" AND warehouse_transfer_date <="' . date('Y-m-d', strtotime($_GET['date_to'])) . '"';
                } else {
                    $sql1 .= ' AND 1=1 ';
                    $sql2 .= ' AND 1=1 ';
                }
            } else {
                $sql1 .= ' AND 1=1 ';
                $sql2 .= ' AND 1=1 ';
            }
            if (isset($_GET['transfer_type']) && !empty($_GET['transfer_type'])) {
                $sql1 .= ' AND warehousereceipt_transfer_type ="' . $_GET['transfer_type'] . '"';
                $sql2 .= ' AND warehousereceipt_transfer_type ="' . $_GET['transfer_type'] . '"';
                $bill_or_despatch_id = isset($_GET['bill_or_despatch_id']) ? $_GET['bill_or_despatch_id'] : "";
                if ($_GET['transfer_type'] == 1) {
                    if ($bill_or_despatch_id == '') {
                        $sql1 .= ' AND 1=1 ';
                        $sql2 .= ' AND 1=1 ';
                    } else {
                        $sql1 .= ' AND warehousereceipt_bill_id ="' . $bill_or_despatch_id . '"';
                        $sql2 .= ' AND warehousereceipt_bill_id ="' . $bill_or_despatch_id . '"';
                    }
                    $model->warehousereceipt_bill_id = isset($_GET['bill_or_despatch_id']) ? $_GET['bill_or_despatch_id'] : "";
                } elseif ($_GET['transfer_type'] == 2) {
                    if ($bill_or_despatch_id == '') {
                        $sql1 .= ' AND 1=1 ';
                        $sql2 .= ' AND 1=1 ';
                    } else {
                        $sql1 .= ' AND warehousereceipt_despatch_id ="' . $bill_or_despatch_id . '"';
                        $sql2 .= ' AND warehousereceipt_despatch_id ="' . $bill_or_despatch_id . '"';
                    }
                    $model->warehousereceipt_despatch_id = isset($_GET['bill_or_despatch_id']) ? $_GET['bill_or_despatch_id'] : "";
                } else {
                }
            }
        } else {
            $sql1 .= ' WHERE 1';
            $sql2 .= ' WHERE 1';
        }

        $transfer_type = isset($_GET['transfer_type']) ? $_GET['transfer_type'] : "";
        $bill_or_despatch_id = isset($_GET['bill_or_despatch_id']) ? $_GET['bill_or_despatch_id'] : "";
        if (isset($_GET['item_id']) && !empty($_GET['item_id'])) {
            $item_id = $_GET['item_id'];
            $sql = 'SELECT * FROM (' . $sql1 . ' UNION ' . $sql2 . ') as new
                                INNER JOIN ' . $this->tblpx . 'warehousereceipt_items as wrt
                                ON new.warehousereceipt_id= wrt.warehousereceipt_id
                                WHERE wrt.warehousereceipt_itemid="' . $item_id . '" OR  wrt.warehousereceipt_transfer_type_item_id="' . $item_id . '"
                                ORDER BY new.warehousereceipt_id DESC,new.warehousereceipt_date DESC';
        } else {
            $item_id = '';
            $sql = $sql1 . ' UNION ' . $sql2 . ' ORDER BY warehousereceipt_id DESC,warehousereceipt_date DESC';
        }

        $warehousereceipt_details = Yii::app()->db->createCommand($sql)->queryAll();
        $warehousereceipt_count = count($warehousereceipt_details);
        $dataProvider = new CSqlDataProvider($sql, array(
            'keyField' => 'warehousereceipt_id',
            'totalItemCount' => $warehousereceipt_count,
            //'sort' => array('defaultOrder' => 'warehousereceipt_id DESC',),
            'pagination' => array('pageSize' => 10,),
        ));



        $this->render('index', array(
            'model' => $model,
            'dataProvider' => $dataProvider,
            'warehouse' => $warehouse,
            'transfer_type' => $transfer_type,
            'bill_or_despatch_id' => $bill_or_despatch_id,
            'vendor' => $vendor,
            'clerk' => $clerk,
            'specification'  => $specification,
            'item_id' => $item_id
        ));
    }
    public function actiongetTransferType()
    {

        $bill_or_despatch_id = isset($_POST['bill_or_despatch_id']) ? $_POST['bill_or_despatch_id'] : '';
        $bill_or_despatch_data = isset($_POST['bill_or_despatch_data']) ? $_POST['bill_or_despatch_data'] : '';
        $bill_model_sql = "SELECT * FROM {$this->tblpx}bills WHERE bill_id =" . $bill_or_despatch_id . " AND bill_number ='" . $bill_or_despatch_data . "'";
        $bill_model =  Yii::app()->db->createCommand($bill_model_sql)->queryAll();
        $despatch_model_sql = "SELECT * FROM {$this->tblpx}warehousedespatch WHERE warehousedespatch_id =" . $bill_or_despatch_id . " AND warehousedespatch_no ='" . $bill_or_despatch_data . "'";
        $despatch_model =  Yii::app()->db->createCommand($despatch_model_sql)->queryAll();
        if (!empty($bill_model)) {
            $transfer_type = 1;
        } elseif (!empty($despatch_model)) {
            $transfer_type = 2;
        } else {
            $transfer_type = '';
        }
        $transfer_type_arr = ['1' => 'Purchase', '2' => 'Despatch'];
        echo CHtml::dropDownList(
            'transfer_type',
            $transfer_type,
            $transfer_type_arr,
            array('empty' => 'Choose Transfer Type', 'class' => 'inputs target transfer_type')
        );
    }
    public function actionwarehoueReceiptDelete()
    {
        $warehouse_receipt_id = $_GET['receiptid'];
        $warehousereceipt_items = array();
        $warehousereceipt_details = array();
        $warehouserec_details = array();
        $mappedData=array();
        $warehouse_receipt_details = Warehousereceipt::model()->findByPk($warehouse_receipt_id);
        /*log changes **/
        $mappedData = [
            'warehousereceipt_id' => $warehouse_receipt_details->warehousereceipt_id,
            'warehousereceipt_no' => $warehouse_receipt_details->warehousereceipt_no,
            'warehousereceipt_date' => $warehouse_receipt_details->warehousereceipt_date,
            'warehousereceipt_remark' => $warehouse_receipt_details->warehousereceipt_remark,
            'warehousereceipt_purchasebill_project' => $warehouse_receipt_details->warehousereceipt_purchasebill_project,
            'warehousereceipt_despatch_id' => $warehouse_receipt_details->warehousereceipt_despatch_id,
            'warehousereceipt_bill_id' => $warehouse_receipt_details->warehousereceipt_bill_id,
            'warehousereceipt_warehouseid' => $warehouse_receipt_details->warehousereceipt_warehouseid,
            'warehousereceipt_warehouseid_from' => $warehouse_receipt_details->warehousereceipt_warehouseid_from,
            'warehousereceipt_clerk' => $warehouse_receipt_details->warehousereceipt_clerk,
            'warehousereceipt_quantity' => $warehouse_receipt_details->warehousereceipt_quantity,
            'warehousereceipt_transfer_type' => $warehouse_receipt_details->warehousereceipt_transfer_type,
            'company_id' => $warehouse_receipt_details->company_id,
            'created_by' => $warehouse_receipt_details->created_by,
            'created_date' => $warehouse_receipt_details->created_date,
            'updated_by' => $warehouse_receipt_details->updated_by,
            'updated_date' => $warehouse_receipt_details->updated_date,
            'delete_status' => $warehouse_receipt_details->delete_status,
            'approve_status' => $warehouse_receipt_details->approve_status,
        ];
        /* log changes ends **/
        

        $warehousereceipt_details['warehouse_transfer_type_delete'] = 1; // 1=> delete from warehouse receipt , 2=> delete from warehouse despatch
        $warehousereceipt_details['warehouse_transfer_type_deleted_id'] = $warehouse_receipt_id;
        $warehousereceipt_details['warehouse_transfer_type_deleted_no'] = $warehouse_receipt_details->warehousereceipt_no;
        $warehousereceipt_details['warehouse_transfer_date'] = $warehouse_receipt_details->warehousereceipt_date;
        $warehousereceipt_details['warehouse_transfer_remark'] = $warehouse_receipt_details->warehousereceipt_remark;
        $warehousereceipt_details['warehousereceipt_transfer_type'] = $warehouse_receipt_details->warehousereceipt_transfer_type;
        $warehousereceipt_details['warehousereceipt_despatch_id'] = $warehouse_receipt_details->warehousereceipt_despatch_id;
        $warehousereceipt_details['warehousereceipt_bill_id'] = $warehouse_receipt_details->warehousereceipt_bill_id;
        $warehousereceipt_details['warehouseid_to'] = $warehouse_receipt_details->warehousereceipt_warehouseid;
        $warehousereceipt_details['warehouseid_from'] = $warehouse_receipt_details->warehousereceipt_warehouseid_from;
        $warehousereceipt_details['warehousereceipt_clerk'] = $warehouse_receipt_details->warehousereceipt_clerk;
        if ($warehouse_receipt_details->warehousereceipt_despatch_id == '') {
            $warehousereceipt_details['warehouse_despatch_date'] = null;
            $warehousereceipt_details['warehouse_despatch_eta_date'] = null;
        } else {
            $warehouse_despatch_details = Warehousedespatch::model()->findByPk($warehouse_receipt_details->warehousereceipt_despatch_id);
            $warehousereceipt_details['warehouse_despatch_date'] = $warehouse_despatch_details->warehousedespatch_date;
            $warehousereceipt_details['warehouse_despatch_eta_date'] = $warehouse_despatch_details->warehouse_eta_date;
        }

        $warehouse_receipt_item_details_arr = array(
            'select' => array('*'), 'condition' => 'warehousereceipt_id ="' . $warehouse_receipt_id . '"',
            'order' => 'item_id DESC'
        );

        $warehouse_receipt_item_details = WarehousereceiptItems::model()->findAll($warehouse_receipt_item_details_arr);
        if (!empty($warehouse_receipt_item_details)) {
            foreach ($warehouse_receipt_item_details as $value) {
                $data['item_id'] = $value['item_id'];
                $data['warehousereceipt_itemid'] = $value['warehousereceipt_itemid'];
                $data['warehousereceipt_unit'] = $value['warehousereceipt_unit'];
                $data['warehousereceipt_batch'] = $value['warehousereceipt_batch'];
                $data['warehousereceipt_rate'] = $value['warehousereceipt_rate'];
                $data['warehousereceipt_quantity'] = $value['warehousereceipt_quantity'];
                $data['warehousereceipt_accepted_quantity'] = $value['warehousereceipt_accepted_quantity'];
                $data['warehousereceipt_rejected_quantity'] = $value['warehousereceipt_rejected_quantity'];
                $data['warehousereceipt_baseunit_quantity'] = $value['warehousereceipt_baseunit_quantity'];
                $data['warehousereceipt_baseunit_accepted_quantity'] = $value['warehousereceipt_baseunit_accepted_quantity'];
                $data['warehousereceipt_baseunit_accepted_effective_quantity'] = $value['warehousereceipt_baseunit_accepted_effective_quantity'];
                $data['warehousereceipt_baseunit_rejected_quantity'] = $value['warehousereceipt_baseunit_rejected_quantity'];
                array_push($warehousereceipt_items, $data);
            }
            $warehouse_stock_item_qty_change_status = $this->warehouseStockItemQtyChangeStatus($warehousereceipt_details, $warehousereceipt_items, $mappedData);
            if ($warehouse_stock_item_qty_change_status == "true") {
                $warehouse_transaction_deleted_log_insert_status = $this->warehouseTransactionDeletedLogInsert($warehousereceipt_details, $warehousereceipt_items, $mappedData);
                if ($warehouse_transaction_deleted_log_insert_status == "true") {
                    $warehouse_receipt_item_delete_status = $this->warehoueReceiptDeleteItems($warehouse_receipt_id);
                    if ($warehouse_receipt_item_delete_status == "true") {
                        $warehouse_receipt_delete_status = $this->warehoueReceiptDelete($warehouse_receipt_id);
                        if ($warehouse_receipt_delete_status == "true") {
                            echo json_encode(array('response' => 'success', 'msg' => 'Receipt item Deleted successfully'));
                        } else {
                            echo json_encode(array('response' => 'error', 'msg' => 'Some Problem occurs.'));
                        }
                    } else {
                        echo json_encode(array('response' => 'error', 'msg' => 'Some Problem occurs.'));
                    }
                } else {
                    echo json_encode(array('response' => 'error', 'msg' => 'Some Problem occurs.'));
                }
            } else {
                echo json_encode(array('response' => 'error', 'msg' => 'The received Items are Already Despatched.'));
            }
        } else {
            $warehouse_transaction_deleted_log_insert_status = $this->warehouseTransactionDeletedLogInsert($warehousereceipt_details, $warehousereceipt_items, $mappedData);
            if ($warehouse_transaction_deleted_log_insert_status == "true") {
                $warehouse_receipt_delete_status = $this->warehoueReceiptDelete($warehouse_receipt_id);
                if ($warehouse_receipt_delete_status == "true") {
                    echo json_encode(array('response' => 'success', 'msg' => 'Receipt item Deleted successfully'));
                } else {
                    echo json_encode(array('response' => 'error', 'msg' => 'Some Problem occurs.'));
                }
            } else {
                echo json_encode(array('response' => 'error', 'msg' => 'Some Problem occurs.'));
            }
        }
    }
    public function warehouseStockItemQtyChangeStatus($warehousereceipt_details, $warehousereceipt_items, $mappedData='')
    {
        $warehousestock['warehouse_id'] = $warehousereceipt_details['warehouseid_to'];
        $warehouse_stock_item_status_check_arr = array();
        foreach ($warehousereceipt_items as $value) {
            $warehousestock['itemid'] = $value['warehousereceipt_itemid'];
            $warehousestock['batch'] = $value['warehousereceipt_batch'];
            $warehousestock['rate'] = $value['warehousereceipt_rate'];
            $warehousestock['quantity'] = $value['warehousereceipt_baseunit_accepted_effective_quantity'];
            $warehouse_stock_item_status = $this->warehouseStockItemStatus($warehousestock);
            if ($warehouse_stock_item_status == "true") {
                $warehouse_stock_item_status_check_arr[] = "true";
            } else {
                $warehouse_stock_item_status_check_arr[] = "false";
            }
        }
        if (in_array("false", $warehouse_stock_item_status_check_arr)) {
            return "false";
        } else {
            $warehouse_stock_item_quantity_delete_status = $this->warehouseStockItemQuantityDeleteStatus($warehousereceipt_details, $warehousereceipt_items, $mappedData);
            if ($warehouse_stock_item_quantity_delete_status == "true") {
                return "true";
            } else {
                return "false";
            }
        }
    }

    public function warehouseStockItemStatus($warehousestock)
    {

        $warehousereceipt_warehouseid = $warehousestock['warehouse_id'];
        $stock_item = $warehousestock['itemid'];
        $batch = $warehousestock['batch'];
        $rate = $warehousestock['rate'];
        $deleted_quantity = $warehousestock['quantity'];

        if ($batch == null) {
            $stock_check = Yii::app()->db->createCommand("SELECT warehousestock_stock_quantity,warehousestock_stock_quantity,rate,warehousestock_id,warehousestock_initial_quantity FROM {$this->tblpx}warehousestock WHERE warehousestock_warehouseid=" . $warehousereceipt_warehouseid . " AND warehousestock_itemid=" . $stock_item . " AND (batch IS Null OR batch ='" . $batch . "') AND rate  = $rate ")->queryRow();
        } else {
            $stock_check = Yii::app()->db->createCommand("SELECT warehousestock_stock_quantity,rate,warehousestock_id,warehousestock_initial_quantity FROM {$this->tblpx}warehousestock WHERE warehousestock_warehouseid=" . $warehousereceipt_warehouseid . " AND warehousestock_itemid=" . $stock_item . " AND batch = '" . $batch . "' AND rate = $rate ")->queryRow();
        }
        if (!empty($stock_check)) {
            if ($batch == null) {
                $item_receipt_check = Yii::app()->db->createCommand("SELECT SUM(warehousereceipt_baseunit_accepted_effective_quantity)as quantity FROM {$this->tblpx}warehousereceipt_items WHERE warehousereceipt_warehouseid=" . $warehousereceipt_warehouseid . " AND warehousereceipt_itemid=" . $stock_item . " AND (warehousereceipt_batch IS Null OR warehousereceipt_batch ='" . $batch . "') AND warehousereceipt_rate = $rate ")->queryRow();
                $item_despatch_check = Yii::app()->db->createCommand("SELECT SUM(warehousedespatch_baseunit_quantity)as quantity FROM {$this->tblpx}warehousedespatch_items WHERE warehousedespatch_warehouseid=" . $warehousereceipt_warehouseid . " AND warehousedespatch_itemid=" . $stock_item . " AND (warehousedespatch_batch IS Null OR warehousedespatch_batch ='" . $batch . "') AND warehousedespatch_rate = $rate ")->queryRow();
            } else {
                $item_receipt_check = Yii::app()->db->createCommand("SELECT SUM(warehousereceipt_baseunit_accepted_effective_quantity)as quantity FROM {$this->tblpx}warehousereceipt_items WHERE warehousereceipt_warehouseid=" . $warehousereceipt_warehouseid . " AND warehousereceipt_itemid=" . $stock_item . " AND warehousereceipt_batch='" . $batch . "' AND warehousereceipt_rate= $rate ")->queryRow();
                $item_despatch_check = Yii::app()->db->createCommand("SELECT SUM(warehousedespatch_baseunit_quantity)as quantity FROM {$this->tblpx}warehousedespatch_items WHERE warehousedespatch_warehouseid=" . $warehousereceipt_warehouseid . " AND warehousedespatch_itemid=" . $stock_item . " AND warehousedespatch_batch='" . $batch . "' AND warehousedespatch_rate = $rate ")->queryRow();
            }
            if ($item_receipt_check['quantity'] == "") {
                $item_receipt_quantity_check = 0;
            } else {
                $item_receipt_quantity_check = round($item_receipt_check['quantity'],2) - $deleted_quantity;
            }
            if ($item_despatch_check['quantity'] == "") {
                $item_despatch_quantity_check = 0;
            } else {
                $item_despatch_quantity_check = $item_despatch_check['quantity'];
            }
        } else {
            $item_receipt_quantity_check = 0;
            $item_despatch_quantity_check = 0;
        }
        if ($item_receipt_quantity_check < $item_despatch_quantity_check) {
            return "false";
        } else {
            return "true";
        }
    }


    public function warehouseStockItemQuantityDeleteStatus($warehousereceipt_details, $warehousereceipt_items, $mappedData='')
    {

        $warehousereceipt_warehouseid = $warehousereceipt_details['warehouseid_to'];
        $warehouse_stock_item_delete_status_check_arr = array();
        foreach ($warehousereceipt_items as $value) {
            $stock_item = $value['warehousereceipt_itemid'];
            $batch = $value['warehousereceipt_batch'];
            $rate = $value['warehousereceipt_rate'];
            $deleted_quantity = $value['warehousereceipt_baseunit_accepted_effective_quantity'];
            if ($batch == null) {
                $stock = Yii::app()->db->createCommand("SELECT warehousestock_stock_quantity,warehousestock_stock_quantity,rate,warehousestock_id,warehousestock_initial_quantity FROM {$this->tblpx}warehousestock WHERE warehousestock_warehouseid=" . $warehousereceipt_warehouseid . " AND warehousestock_itemid=" . $stock_item . " AND (batch IS Null OR batch ='" . $batch . "') AND rate = $rate ")->queryRow();
            } else {
                $stock = Yii::app()->db->createCommand("SELECT warehousestock_stock_quantity,rate,warehousestock_id,warehousestock_initial_quantity FROM {$this->tblpx}warehousestock WHERE warehousestock_warehouseid=" . $warehousereceipt_warehouseid . " AND warehousestock_itemid=" . $stock_item . " AND batch = '" . $batch . "' AND rate = $rate ")->queryRow();
            }
            if (!empty($stock)) {
                if ($batch == null) {
                    $item_receipt = Yii::app()->db->createCommand("SELECT SUM(warehousereceipt_baseunit_accepted_effective_quantity)as quantity FROM {$this->tblpx}warehousereceipt_items WHERE warehousereceipt_warehouseid=" . $warehousereceipt_warehouseid . " AND warehousereceipt_itemid=" . $stock_item . " AND (warehousereceipt_batch IS Null OR warehousereceipt_batch ='" . $batch . "') AND warehousereceipt_rate = $rate ")->queryRow();
                    $item_despatch = Yii::app()->db->createCommand("SELECT SUM(warehousedespatch_baseunit_quantity)as quantity FROM {$this->tblpx}warehousedespatch_items WHERE warehousedespatch_warehouseid=" . $warehousereceipt_warehouseid . " AND warehousedespatch_itemid=" . $stock_item . " AND (warehousedespatch_batch IS Null OR warehousedespatch_batch ='" . $batch . "') AND warehousedespatch_rate = $rate ")->queryRow();
                } else {
                    $item_receipt = Yii::app()->db->createCommand("SELECT SUM(warehousereceipt_baseunit_accepted_effective_quantity)as quantity FROM {$this->tblpx}warehousereceipt_items WHERE warehousereceipt_warehouseid=" . $warehousereceipt_warehouseid . " AND warehousereceipt_itemid=" . $stock_item . " AND warehousereceipt_batch='" . $batch . "' AND warehousereceipt_rate = $rate ")->queryRow();
                    $item_despatch = Yii::app()->db->createCommand("SELECT SUM(warehousedespatch_baseunit_quantity)as quantity FROM {$this->tblpx}warehousedespatch_items WHERE warehousedespatch_warehouseid=" . $warehousereceipt_warehouseid . " AND warehousedespatch_itemid=" . $stock_item . " AND warehousedespatch_batch='" . $batch . "' AND warehousedespatch_rate = $rate ")->queryRow();
                }
                if ($item_receipt['quantity'] == "") {
                    $item_receipt_quantity = 0;
                } else {
                    $item_receipt_quantity = $item_receipt['quantity'] - $deleted_quantity;
                }
                if ($item_despatch['quantity'] == "") {
                    $item_despatch_quantity = 0;
                } else {
                    $item_despatch_quantity = $item_despatch['quantity'];
                }
                $final_quantity = ($stock['warehousestock_initial_quantity'] + $item_receipt_quantity) - $item_despatch_quantity;
                $model2 = Warehousestock::model()->findByPk($stock['warehousestock_id']);
                $model2->warehousestock_stock_quantity = $final_quantity;
                if ($model2->save(false)) {
                    $warehouse_stock_item_delete_status_check_arr[] = "true";
                } else {
                    $warehouse_stock_item_delete_status_check_arr[] = "false";
                }
            }
        }
        if (in_array("false", $warehouse_stock_item_delete_status_check_arr)) {
            return "false";
        } else {
            return "true";
        }
    }
    public function warehouseTransactionDeletedLogInsert($warehousereceipt_details, $warehousereceipt_items, $mappedData='')
    {

        $warehouse_transfer_type_delete = $warehousereceipt_details['warehouse_transfer_type_delete'];
        $warehouse_transfer_type_deleted_id = $warehousereceipt_details['warehouse_transfer_type_deleted_id'];
        $warehouse_transfer_type_deleted_no = $warehousereceipt_details['warehouse_transfer_type_deleted_no'];
        $warehouse_transfer_date = $warehousereceipt_details['warehouse_transfer_date'];
        $warehouse_transfer_remark = $warehousereceipt_details['warehouse_transfer_remark'];
        $warehousereceipt_transfer_type = $warehousereceipt_details['warehousereceipt_transfer_type'];
        $warehousereceipt_despatch_id = $warehousereceipt_details['warehousereceipt_despatch_id'];
        $warehousereceipt_bill_id = $warehousereceipt_details['warehousereceipt_bill_id'];
        $warehouseid_to = $warehousereceipt_details['warehouseid_to'];
        $warehouseid_from = $warehousereceipt_details['warehouseid_from'];
        $warehousereceipt_clerk = $warehousereceipt_details['warehousereceipt_clerk'];
        $warehouse_despatch_date = $warehousereceipt_details['warehouse_despatch_date'];
        $warehouse_despatch_eta_date = $warehousereceipt_details['warehouse_despatch_eta_date'];
        $warehouse_transfer_delete_created_date = date("Y-m-d H:i:s");
        $warehouse_transfer_delete_created_by = Yii::app()->user->id;
        $warehousereceipt_items_json = json_encode($warehousereceipt_items);

        $warehouse_transaction_deletedlog_insert_sql =  "INSERT INTO {$this->tblpx}warehouse_transfer_deleted_log 
                                                                    (warehouse_transfer_type_delete,
                                                                    warehouse_transfer_type_deleted_id,
                                                                    warehouse_transfer_type_deleted_no,
                                                                    warehouse_transfer_date,
                                                                    warehouse_transfer_remark,
                                                                    warehousereceipt_transfer_type,
                                                                    warehousereceipt_despatch_id,
                                                                    warehousereceipt_bill_id,
                                                                    warehouseid_to,warehouseid_from,
                                                                    warehousereceipt_clerk,
                                                                    warehouse_despatch_date,
                                                                    warehouse_despatch_eta_date,
                                                                    warehouse_transfer_items,
                                                                    created_date,
                                                                    created_by)
										                VALUES (:warehouse_transfer_type_delete,
                                                                :warehouse_transfer_type_deleted_id,
                                                                :warehouse_transfer_type_deleted_no,
                                                                :warehouse_transfer_date,
                                                                :warehouse_transfer_remark,
                                                                :warehousereceipt_transfer_type,
                                                                :warehousereceipt_despatch_id,
                                                                :warehousereceipt_bill_id,
                                                                :warehouseid_to,:warehouseid_from,
                                                                :warehousereceipt_clerk,
                                                                :warehouse_despatch_date,
                                                                :warehouse_despatch_eta_date,
                                                                :warehouse_transfer_items,
                                                                :created_date,
                                                                :created_by
                                                                )";
        $warehouse_transaction_deletedlog_insert_parameters = array(
            ":warehouse_transfer_type_delete" => $warehouse_transfer_type_delete,
            ":warehouse_transfer_type_deleted_id" => $warehouse_transfer_type_deleted_id,
            ":warehouse_transfer_type_deleted_no" => $warehouse_transfer_type_deleted_no,
            ":warehouse_transfer_date" => $warehouse_transfer_date,
            ":warehouse_transfer_remark" => $warehouse_transfer_remark,
            ":warehousereceipt_transfer_type" => $warehousereceipt_transfer_type,
            ":warehousereceipt_despatch_id" => $warehousereceipt_despatch_id,
            ":warehousereceipt_bill_id" => $warehousereceipt_bill_id,
            ":warehouseid_to" => $warehouseid_to,
            ":warehouseid_from" => $warehouseid_from,
            ":warehousereceipt_clerk" => $warehousereceipt_clerk,
            ":warehouse_despatch_date" => $warehouse_despatch_date,
            ":warehouse_despatch_eta_date" => $warehouse_despatch_eta_date,
            ":warehouse_transfer_items" => $warehousereceipt_items_json,
            ":created_date" => $warehouse_transfer_delete_created_date,
            ":created_by" => $warehouse_transfer_delete_created_by,
        );
        $warehouse_transaction_deletedlog_insert = Yii::app()->db->createCommand($warehouse_transaction_deletedlog_insert_sql)->execute($warehouse_transaction_deletedlog_insert_parameters);
        if ($warehouse_transaction_deletedlog_insert){
            $tblpx        = Yii::app()->db->tablePrefix;
            $newmodel = new JpLog('search');
            $newmodel->log_data = json_encode( $mappedData);
            $newmodel->log_action = 2;
            $newmodel->log_table = $tblpx . "warehousereceipt";
            $newmodel->log_datetime = date('Y-m-d H:i:s');
            $newmodel->log_action_by = Yii::app()->user->id;
            //$newmodel->company_id = $data->company_id;
            if ($newmodel->save()) {
               
            }
            return "true";
        }
        else{
            return "false";
        }
           
    }
    public function warehoueReceiptDelete($warehouse_receipt_id)
    {

        $del_warehousereceipt = Yii::app()->db->createCommand()->delete($this->tblpx . 'warehousereceipt', 'warehousereceipt_id=:id', array(':id' => $warehouse_receipt_id));
        if ($del_warehousereceipt)
            return "true";
        else
            return "false";
    }
    public function warehoueReceiptDeleteItems($warehouse_receipt_id)
    {

        $del_warehousereceipt_items = Yii::app()->db->createCommand()->delete($this->tblpx . 'warehousereceipt_items', 'warehousereceipt_id=:id', array(':id' => $warehouse_receipt_id));
        if ($del_warehousereceipt_items)
            return "true";
        else
            return "false";
    }

    public function actiongetBillOrDespatchId()
    {
        $transfer_type = isset($_POST['transfer_type']) ? $_POST['transfer_type'] : '';
        $result_array = array();
        $bill_number_arr = array();
        $despatch_number_arr = array();
        $bill_number_arr = Warehousereceipt::model()->findAll(
            array(
                'select' => array('*'), 'condition' => 'warehousereceipt_despatch_id IS NULL AND warehousereceipt_transfer_type = 1',
                'order' => 'warehousereceipt_id DESC'
            )
        );
        $despatch_number_arr = Warehousereceipt::model()->findAll(
            array(
                'select' => array('*'), 'condition' => 'warehousereceipt_bill_id IS NULL AND warehousereceipt_transfer_type = 2',
                'order' => 'warehousereceipt_id DESC'
            )
        );
        if ($transfer_type == 1 || $transfer_type == '') {
            foreach ($bill_number_arr as $purchase) {
                if (!empty($purchase['warehousereceipt_bill_id'])) {
                    $bill_model = Bills::model()->findByPk($purchase['warehousereceipt_bill_id']);
                    if (!empty($bill_model)) {
                        $bill_num = $bill_model['bill_number'];
                    } else {
                        $bill_num = '';
                    }
                }

                $data = array('id' => $purchase['warehousereceipt_bill_id'], 'text' => $bill_num, 'group' => 'Purchase');
                array_push($result_array, $data);
            }
        }
        if ($transfer_type == 2 || $transfer_type == '') {
            foreach ($despatch_number_arr as $despatch) {
                if (!empty($despatch['warehousereceipt_despatch_id'])) {
                    $despatch_model = Warehousedespatch::model()->findByPk($despatch['warehousereceipt_despatch_id']);
                    if (!empty($despatch_model)) {
                        $despatch_num = $despatch_model['warehousedespatch_no'];
                    } else {
                        $despatch_num = '';
                    }
                }

                $data = array('id' => $despatch['warehousereceipt_despatch_id'], 'text' => $despatch_num, 'group' => 'Despatch');
                array_push($result_array, $data);
            }
        }

        $bill_or_despatch_id = '';
        $bill_or_despatch_id_arr = CHtml::listData($result_array, 'id', 'text', 'group');
        echo CHtml::dropDownList(
            'bill_or_despatch_id',
            $bill_or_despatch_id,
            $bill_or_despatch_id_arr,
            array(
                'empty' => 'Choose Bill/ Despatch Number', 'class' => 'inputs target bill_or_despatch_id',
                'ajax' => array(
                    'url' => array('warehousereceipt/getTransferType'),
                    'type' => 'POST',
                    'data' => array('bill_or_despatch_id' => 'js:this.value', 'bill_or_despatch_data' => 'js:$(\'#bill_or_despatch_id option:selected\').text()'),
                    'update' => '#transfer_type',

                )
            )
        );
    }
    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model = new Warehousereceipt('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Warehousereceipt']))
            $model->attributes = $_GET['Warehousereceipt'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }


    public function actionGetItemCategory($parent = 0, $spacing = '', $user_tree_array = '')
    {
        if (!is_array($user_tree_array))
            $user_tree_array = array();
        $data = array();
        $final = array();

        $spec_sql = "SELECT id, specification, brand_id, cat_id"
            . " FROM {$this->tblpx}specification "
            . " WHERE  company_id=" . Yii::app()->user->company_id . "";
        $specification = Yii::app()->db->createCommand($spec_sql)->queryAll();


        foreach ($specification as $key => $value) {
            if ($value['brand_id'] != NULL) {
                $brand_details = Yii::app()->db->createCommand("SELECT brand_name FROM {$this->tblpx}brand WHERE id=" . $value['brand_id'] . "")->queryRow();
                $brand = '-' . ucfirst($brand_details['brand_name']);
            } else {
                $brand = '';
            }

            if ($value['cat_id'] != '') {
                $result = $this->actionGetParent($value['cat_id']);
                $final[$key]['data'] = ucfirst($result['category_name']) . $brand . '-' . ucfirst($value['specification']);
            } else {
                $final[$key]['data'] = $brand . '-' . $value['specification'];
            }



            $final[$key]['id'] = $value['id'];
        }
        return $final;
    }

    public function actionGetItemCategory2($parent = 0, $spacing = '', $user_tree_array = '', $warehouse)
    {
        if (!is_array($user_tree_array))
            $user_tree_array = array();
        $data = array();
        $final = array();
        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        $newQuery = "";
        foreach ($arrVal as $arr) {
            if ($newQuery)
                $newQuery .= ' OR';
            $newQuery .= " FIND_IN_SET('" . $arr . "', b.company_id)";
        }

        $spec_sql = "SELECT DISTINCT a.id, a.specification,"
            . " a.brand_id, a.cat_id,b.warehousestock_itemid "
            . " FROM {$this->tblpx}specification as a "
            . " INNER JOIN {$this->tblpx}warehousestock b "
            . " on a.id=b.warehousestock_itemid  "
            . " WHERE b.warehousestock_warehouseid =" . $warehouse . " "
            . " AND (".$newQuery.")";
        $specification = Yii::app()->db->createCommand($spec_sql)->queryAll();
        

        foreach ($specification as $key => $value) {
            if ($value['brand_id'] != NULL) {
                $brand_details = Yii::app()->db->createCommand("SELECT brand_name FROM {$this->tblpx}brand WHERE id=" . $value['brand_id'] . "")->queryRow();
                $brand = '-' . ucfirst($brand_details['brand_name']);
            } else {
                $brand = '';
            }

            if ($value['cat_id'] != '') {
                $result = $this->actionGetParent($value['cat_id']);
                $final[$key]['data'] = ucfirst($result['category_name']) . $brand . '-' . ucfirst($value['specification']);
            } else {
                $final[$key]['data'] = $brand . '-' . $value['specification'];
            }



            $final[$key]['id'] = $value['id'];
        }
        return $final;
    }

    public function actionGetParent($id)
    {

        $category = Yii::app()->db->createCommand("SELECT parent_id, id , category_name FROM {$this->tblpx}purchase_category WHERE id=" . $id)->queryRow();

        return $category;
    }

    public function actionajax()
    {
        echo json_encode('success');
    }
    public function actionDespatchPurchaseBilllist()
    {

        $warehouse_id = $_REQUEST['warehouse'];
        $warehouse_from = $_REQUEST['warehouse_from'];
        $html['despatches'] = '';
        $html['bills'] = '';
        $bill_items = [];
        $despatch_items = [];
        $despatches_final_list = array();
        $bills_final_list = array();

        if ($warehouse_id != '' && $warehouse_from != '') {

            $despatches = Yii::app()->db->createCommand("SELECT DISTINCT warehousedespatch_id,warehousedespatch_no FROM {$this->tblpx}warehousedespatch WHERE warehousedespatch_warehouseid =" . $warehouse_from . " AND warehousedespatch_warehouseid_to=" . $warehouse_id . " ORDER BY warehousedespatch_id")->queryAll();

            $bills_sql = "SELECT DISTINCT b.bill_id,b.bill_number,
                                b.purchase_id,p.project_id,p.vendor_id,v.name
                                FROM {$this->tblpx}bills as b INNER JOIN {$this->tblpx}purchase as p
                                ON b.purchase_id = p.p_id
                                INNER JOIN {$this->tblpx}vendors as v 
                                ON p.vendor_id = v.vendor_id 
                                WHERE warehouse_id =" . $warehouse_id . " ORDER BY b.bill_id";
            $bills = Yii::app()->db->createCommand($bills_sql)->queryAll();
        } elseif ($warehouse_id != '') {

            //$despatches = Yii::app()->db->createCommand("SELECT DISTINCT warehousedespatch_id,warehousedespatch_no FROM {$this->tblpx }warehousedespatch WHERE warehousedespatch_warehouseid =" . $warehouse_id . " AND  warehousedespatch_project Is Null ORDER BY warehousedespatch_id")->queryAll();
            $despatches = array();
            $bills_sql = "SELECT DISTINCT b.bill_id,b.bill_number,
                                b.purchase_id,p.project_id,p.vendor_id,v.name
                                FROM {$this->tblpx}bills as b 
                                INNER JOIN {$this->tblpx}purchase as p
                                ON b.purchase_id = p.p_id
                                INNER JOIN {$this->tblpx}vendors as v 
                                ON p.vendor_id = v.vendor_id 
                                WHERE b.warehouse_id =" . $warehouse_id . " ORDER BY b.bill_id";
            $bills = Yii::app()->db->createCommand($bills_sql)->queryAll();
        } else {
            $despatches = array();
            $bills = array();
        }
        if (count($despatches) != 0) {
            foreach ($despatches as $despatcheskey => $despatchesvalue) {
                $despatch_items = Yii::app()->db->createCommand("SELECT * FROM {$this->tblpx}warehousedespatch_items WHERE warehousedespatch_id =" . $despatchesvalue['warehousedespatch_id'] . "")->queryAll();
                $warehousedespatch_model = Warehousereceipt::model()->findAll(array("condition" => "warehousereceipt_despatch_id = '" . $despatchesvalue['warehousedespatch_id'] . "'"));
                if (count($despatch_items) != 0 && count($warehousedespatch_model) == 0) {
                    $despatches_final_list[$despatcheskey]['id'] = $despatchesvalue['warehousedespatch_id'];
                    $despatches_final_list[$despatcheskey]['value'] = $despatchesvalue['warehousedespatch_no'];
                }
            }
        }
        if (count($bills) != 0) {
            foreach ($bills as $billskey => $billsvalue) {
                $bill_items = Yii::app()->db->createCommand("SELECT * FROM {$this->tblpx}billitem WHERE bill_id =" . $billsvalue['bill_id'] . "")->queryAll();
                $warehousereceipt_model = Warehousereceipt::model()->findAll(array("condition" => "warehousereceipt_bill_id = '" . $billsvalue['bill_id'] . "' AND delete_status = 1"));
                if (count($bill_items) != 0 && count($warehousereceipt_model) == 0) {
                    $bills_final_list[$billskey]['id'] = $billsvalue['bill_id'];
                    $bills_final_list[$billskey]['value'] = $billsvalue['bill_number'] . " (" . $billsvalue['name'] . ")";
                }
            }
        }
        $html['despatches'] = $despatches_final_list;
        $html['bills'] = $bills_final_list;
        echo json_encode($html);
    }

    public function GetDespatchPurchaseBilllist($warehouse_id, $warehouse_from)
    {

        $warehouse_id = $warehouse_id;
        $warehouse_from = $warehouse_from;
        $html['despatches'] = '';
        $html['bills'] = '';
        $bill_items = [];
        $despatch_items = [];
        $despatches_final_list = array();
        $bills_final_list = array();
        if ($warehouse_id != '' && $warehouse_from != '') {


            $despatches = Yii::app()->db->createCommand("SELECT DISTINCT warehousedespatch_id,warehousedespatch_no FROM {$this->tblpx}warehousedespatch WHERE warehousedespatch_warehouseid =" . $warehouse_from . " AND warehousedespatch_warehouseid_to=" . $warehouse_id . " ORDER BY warehousedespatch_id")->queryAll();
            $bills_sql = "SELECT DISTINCT b.bill_id,b.bill_number,
                                b.purchase_id,p.project_id,p.vendor_id,v.name
                                FROM {$this->tblpx}bills as b INNER JOIN {$this->tblpx}purchase as p
                                ON b.purchase_id = p.p_id
                                INNER JOIN {$this->tblpx}vendors as v 
                                ON p.vendor_id = v.vendor_id 
                                WHERE warehouse_id =" . $warehouse_id . " ORDER BY b.bill_id";
            $bills = Yii::app()->db->createCommand($bills_sql)->queryAll();
        } elseif ($warehouse_id != '') {


            //$despatches = Yii::app()->db->createCommand("SELECT DISTINCT warehousedespatch_id,warehousedespatch_no FROM {$this->tblpx }warehousedespatch WHERE warehousedespatch_warehouseid =" . $warehouse_id . " AND  warehousedespatch_project Is Null ORDER BY warehousedespatch_id")->queryAll();
            $despatches = array();
            $bills_sql = "SELECT DISTINCT b.bill_id,b.bill_number,
                                b.purchase_id,p.project_id,p.vendor_id,v.name
                                FROM {$this->tblpx}bills as b INNER JOIN {$this->tblpx}purchase as p
                                ON b.purchase_id = p.p_id
                                INNER JOIN {$this->tblpx}vendors as v 
                                ON p.vendor_id = v.vendor_id 
                                WHERE warehouse_id =" . $warehouse_id . " ORDER BY b.bill_id";
            $bills = Yii::app()->db->createCommand($bills_sql)->queryAll();
        } else {
            $despatches = array();
            $bills = array();
        }
        if (count($despatches) != 0) {
            foreach ($despatches as $despatcheskey => $despatchesvalue) {
                $despatch_items = Yii::app()->db->createCommand("SELECT * FROM {$this->tblpx}warehousedespatch_items WHERE warehousedespatch_id =" . $despatchesvalue['warehousedespatch_id'] . "")->queryAll();
                if (count($despatch_items) != 0) {
                    $despatches_final_list[$despatcheskey]['id'] = $despatchesvalue['warehousedespatch_id'];
                    $despatches_final_list[$despatcheskey]['value'] = $despatchesvalue['warehousedespatch_no'];
                }
            }
        }
        if (count($bills) != 0) {
            foreach ($bills as $billskey => $billsvalue) {
                $bill_items = Yii::app()->db->createCommand("SELECT * FROM {$this->tblpx}billitem WHERE bill_id =" . $billsvalue['bill_id'] . "")->queryAll();
                if (count($bill_items) != 0) {
                    $bills_final_list[$billskey]['id'] = $billsvalue['bill_id'];
                    $bills_final_list[$billskey]['value'] = $billsvalue['bill_number'] . " (" . $billsvalue['name'] . ")";
                }
            }
        }
        $html['despatches'] = $despatches_final_list;
        $html['bills'] = $bills_final_list;
        return $html;
    }
    public function actiongetBatch()
    {
        $category_id        = $_GET['data'];
        $warehouse_id       = $_GET['warehouse_id'];
        $dispatch_no       = $_GET['dispatch_no'];
        $purchase_bill_no       = $_GET['purchase_bill_no'];

        $final_list = '';
        if ($dispatch_no != "") {
            $ItemUnitlist = Yii::app()->db->createCommand("SELECT DISTINCT item_id,warehousedespatch_id,warehousedespatch_batch
                                                        FROM {$this->tblpx}warehousedespatch_items
                                                        WHERE warehousedespatch_id =" . $dispatch_no . " AND warehousedespatch_itemid =" . $category_id . " ORDER BY warehousedespatch_id")->queryAll();
            foreach ($ItemUnitlist as $key => $value) {
                $final_list[$key]['id'] = "dispatch_" . $value['item_id'];
                $final_list[$key]['value'] = $value['warehousedespatch_batch'];
            }
        } elseif ($purchase_bill_no != "") {
            $ItemUnitlist = Yii::app()->db->createCommand("SELECT DISTINCT billitem_id,bill_id,batch
                                                                FROM {$this->tblpx}billitem 
                                                                WHERE bill_id =" . $purchase_bill_no . " AND category_id =" . $category_id . " ORDER BY bill_id")->queryAll();
            foreach ($ItemUnitlist as $key => $value) {
                $final_list[$key]['id'] = "bill_" . $value['billitem_id'];
                $final_list[$key]['value'] = $value['batch'];
            }
        } else {
            $uc_item_count  = Warehousestock::model()->countByAttributes(array(
                'warehousestock_warehouseid' => $warehouse_id,
                'warehousestock_itemid' => $category_id,
            ));
            if ($uc_item_count != 0) {
                $ItemUnitlist           = Warehousestock::model()->findAll(
                    array("condition" => "warehousestock_warehouseid = $warehouse_id AND warehousestock_itemid= $category_id", "order" => "warehousestock_id")
                );

                $final_list = array();

                foreach ($ItemUnitlist as $key => $value) {
                    $final_list[$key]['id'] = $value['warehousestock_id'];
                    $final_list[$key]['value'] = $value['batch'];
                }
            }
        }


        $warehouseStock["Batchlist"] =  $final_list;
        $warehouseStock_dis           = json_encode($warehouseStock);
        echo $warehouseStock_dis;
        //print_r($final_list);
        //echo $uc_item_count;


    }
    public function actiongetUnits()
    {

        $html = [];
        $html['unit'] = '';
        $html['units'] = '';
        $html['status'] = '';

        if (isset($_REQUEST['data'])) {
            $specification_sql = "SELECT id, cat_id, brand_id, specification, unit FROM {$this->tblpx}specification WHERE id=" . $_REQUEST['data'] . "";
            $specification = Yii::app()->db->createCommand($specification_sql)->queryRow();
            if ($specification['unit'] != NULL) {

                $unit  = $specification['unit'];
            } else {
                $unit = '';
            }
        } else {
            $unit = '';
        }

        if (!empty($unit)) {
            $base_unit = $unit;
        } else {
            $base_unit = '';
        }
        $html['base_unit'] = $base_unit;
        if ($base_unit != '') {
            $uc_item_count  = UnitConversion::model()->countByAttributes(array(
                'item_id' => $_REQUEST['data'],
            ));
            if ($uc_item_count > 1) {
                $html['units']  = $this->GetItemunitlist($_REQUEST['data']);
                $html['unit']  = $html['units'];
            } else {
                if (!empty($unit)) {
                    $html['unit'] .= $base_unit;
                    $html['units'] .= $base_unit;
                } else {
                    $html['unit'] .= '';
                    $html['units'] = 'Unit';
                }
            }
        } else {
            $html['unit'] .= '';
            $html['units'] = 'Unit';
        }
        //$html['base_unit_name'] = $this->GetItemunitName($base_unit);
        $html['base_unit_name'] = $base_unit;
        $html['status'] .= true;
        echo json_encode($html);
    }
    public function GetItemunitlist($purchase_itemid)
    {
        $purchase_itemid    = $purchase_itemid;
        $final_list = array();
        $ItemUnitlist           = UnitConversion::model()->findAll(
            array("condition" => "item_id =  $purchase_itemid", "order" => "id")
        );
        $final_list = array();
        foreach ($ItemUnitlist as $key => $value) {
            $final_list[$key]['id'] = $value['conversion_unit'];
            //$final_list[$key]['value'] = $this->GetItemunitCode($value['conversion_unit']);
            $final_list[$key]['value'] = $value['conversion_unit'];
        }
        return $final_list;
    }
    public function GetItemunitCode($unit_id)
    {
        $unit_id            = $unit_id;
        $ItemUnit           = Unit::model()->findByPk($unit_id);
        if ($ItemUnit["unit_code"] != '') {
            $ItemUnit_code      = $ItemUnit["unit_code"];
        } else {
            $ItemUnit_code      = $ItemUnit["unit_name"];
        }

        return $ItemUnit_code;
    }
    public function GetItemunitName($unit_id)
    {
        $unit_id            = $unit_id;
        $ItemUnit           = Unit::model()->findByPk($unit_id);
        $ItemUnit_name      = $ItemUnit["unit_name"];
        return $ItemUnit_name;
    }
    public function GetItemunitID($unit_name)
    {
        $unit_name  = trim($unit_name);
        $ItemUnit   = Unit::model()->findAll(
            array("condition" => "unit_name ='" . $unit_name . "' OR unit_code ='" . $unit_name . "'", "order" => "id")
        );
        foreach ($ItemUnit as $key => $value) {
            $ItemUnit_id = $value["id"];
        }
        return $ItemUnit_id;
    }
    public function actionStockList()
    {

        $dispatch_no = $_REQUEST['dispatch_no'];
        $purchase_bill_no = $_REQUEST['purchase_bill_no'];
        $warehouse_id = $_REQUEST['warehouse'];
        $html['StockItems'] = '';
        $final_list = array();
        if ($dispatch_no != '') {
            $specification_sql = "SELECT pc.id, pc.specification, pc.brand_id,
                                         pc.parent_id,wd.warehousedespatch_id 
                                   FROM {$this->tblpx}purchase_category as pc 
                                   INNER JOIN {$this->tblpx}warehousedespatch_items as wd
                                   ON pc.id = wd.warehousedespatch_itemid
                                   WHERE spec_flag='Y' AND company_id=" . Yii::app()->user->company_id . " AND  wd.warehousedespatch_id ='" . $dispatch_no . "'";
            $specification = Yii::app()->db->createCommand($specification_sql)->queryAll();
            foreach ($specification as $key => $value) {
                if ($value['brand_id'] != NULL) {
                    $brand_details = Yii::app()->db->createCommand("SELECT brand_name FROM {$this->tblpx}brand WHERE id=" . $value['brand_id'] . "")->queryRow();
                    $brand = '-' . ucfirst($brand_details['brand_name']);
                } else {
                    $brand = '';
                }

                if ($value['parent_id'] != '') {
                    $result = $this->actionGetParent($value['parent_id']);
                    $final[$key]['data'] = ucfirst($result['category_name']) . $brand . '-' . ucfirst($value['specification']);
                } else {
                    $final[$key]['data'] = $brand . '-' . $value['specification'];
                }

                $final[$key]['id'] = $value['id'];
            }
        } elseif ($purchase_bill_no != '') {
            $specification_sql = "SELECT pc.id, pc.specification, pc.brand_id,
                                         pc.parent_id,bi.bill_id 
                                   FROM {$this->tblpx}purchase_category as pc 
                                   INNER JOIN {$this->tblpx}billitem as bi
                                   ON pc.id = bi.category_id
                                   WHERE spec_flag='Y' AND company_id=" . Yii::app()->user->company_id . " AND  bi.bill_id ='" . $purchase_bill_no . "'";

            $specification = Yii::app()->db->createCommand($specification_sql)->queryAll();
            foreach ($specification as $key => $value) {

                if ($value['brand_id'] != NULL) {
                    $brand_details = Yii::app()->db->createCommand("SELECT brand_name FROM {$this->tblpx}brand WHERE id=" . $value['brand_id'] . "")->queryRow();
                    $brand = '-' . ucfirst($brand_details['brand_name']);
                } else {
                    $brand = '';
                }

                if ($value['parent_id'] != '') {
                    $result = $this->actionGetParent($value['parent_id']);
                    $final[$key]['data'] = ucfirst($result['category_name']) . $brand . '-' . ucfirst($value['specification']);
                } else {
                    $final[$key]['data'] = $brand . '-' . $value['specification'];
                }



                $final[$key]['id'] = $value['id'];
            }
        } else {
            $final = array();
            $specification = Yii::app()->db->createCommand("SELECT id, specification, brand_id, parent_id FROM {$this->tblpx}purchase_category WHERE spec_flag='Y' AND company_id=" . Yii::app()->user->company_id . "")->queryAll();
            foreach ($specification as $key => $value) {
                $Warehouse_item_count  = Warehousestock::model()->countByAttributes(array(
                    'warehousestock_warehouseid' => $warehouse_id,
                    'warehousestock_itemid' => $value['id'],
                ));
                if ($Warehouse_item_count != 0) {
                    if ($value['brand_id'] != NULL) {
                        $brand_details = Yii::app()->db->createCommand("SELECT brand_name FROM {$this->tblpx}brand WHERE id=" . $value['brand_id'] . "")->queryRow();
                        $brand = '-' . ucfirst($brand_details['brand_name']);
                    } else {
                        $brand = '';
                    }

                    if ($value['parent_id'] != '') {
                        $result = $this->actionGetParent($value['parent_id']);
                        $final[$key]['data'] = ucfirst($result['category_name']) . $brand . '-' . ucfirst($value['specification']);
                    } else {
                        $final[$key]['data'] = $brand . '-' . $value['specification'];
                    }



                    $final[$key]['id'] = $value['id'];
                }
            }
        }
        $html['StockItems'] = $final;
        echo json_encode($html);
    }
    public function actionCreatenewreceipt()
    {

        $receipt_id = $_REQUEST['receipt_id'];
        $warehousereceiptItem_check = [];

        if (isset($_REQUEST['receipt_no']) && isset($_REQUEST['default_date']) && isset($_REQUEST['vendor']) && isset($_REQUEST['warehouse']) && isset($_REQUEST['clerk'])) {
            if ($_REQUEST['purchase_bill_no'] != "") {
                $purchase_bill_no = $_REQUEST['purchase_bill_no'];
                $purchasebill_project_details = '';
                $purchase_project_data = "SELECT DISTINCT b.bill_id,b.bill_number, b.purchase_id,p.project_id 
                FROM {$this->tblpx}bills as b INNER JOIN {$this->tblpx}purchase as p ON b.purchase_id = p.p_id
                WHERE b.bill_id ='" . $purchase_bill_no . "'";
                $purchasebill_project_details = Yii::app()->db->createCommand($purchase_project_data)->queryRow();
                if ($purchasebill_project_details != '') {
                    $project_id = $purchasebill_project_details['project_id'];
                } else {
                    $project_id = Null;
                }
            } else {
                $project_id = Null;
            }

            if ($receipt_id == 0) {

                $res = Warehousereceipt::model()->findAll(array('condition' => 'warehousereceipt_no = "' . $_REQUEST['receipt_no'] . '"'));
                if (empty($res)) {
                    $model = new Warehousereceipt;
                    $model->warehousereceipt_warehouseid = $_REQUEST['warehouse'];
                    $model->warehousereceipt_date = date('Y-m-d',  strtotime($_REQUEST['default_date']));
                    $model->warehousereceipt_no = $_REQUEST['receipt_no'];
                    $model->warehousereceipt_remark = $_REQUEST['vendor'];
                    $model->warehousereceipt_warehouseid_from = $_REQUEST['warehouse_from'];
                    $model->warehousereceipt_purchasebill_project = $project_id;
                    $model->warehousereceipt_despatch_id = $_REQUEST['dispatch_no'];
                    $model->warehousereceipt_bill_id = $_REQUEST['purchase_bill_no'];
                    $model->warehousereceipt_clerk = $_REQUEST['clerk'];
                    $model->warehousereceipt_transfer_type = $_REQUEST['transfer_type'];
                    $model->company_id = Yii::app()->user->company_id;
                    $model->updated_by = yii::app()->user->id;
                    $model->updated_date = date('Y-m-d');
                    $model->created_date = date('Y-m-d');
                    $model->created_by = Yii::app()->user->id;
                    $model->save(false);
                    $last_id = $model->warehousereceipt_id;
                    echo json_encode(array('response' => 'success', 'msg' => 'Warehouse receipt added successfully', 'receipt_id' => $last_id));
                } else {
                    echo json_encode(array('response' => 'error', 'msg' => 'Receipt no already exist'));
                }
            } else {
                $warehousereceiptItem_check = WarehousereceiptItems::model()->findAll(array('condition' => 'warehousereceipt_id = "' . $receipt_id . '"'));
                $res = Warehousereceipt::model()->findAll(array('condition' => 'warehousereceipt_no = "' . $_REQUEST['receipt_no'] . '" AND warehousereceipt_id !=' . $receipt_id . ''));
                if (empty($res)) {
                    $model = Warehousereceipt::model()->findByPk($receipt_id);
                    $model->warehousereceipt_warehouseid = $_REQUEST['warehouse'];
                    $model->warehousereceipt_date = date('Y-m-d',  strtotime($_REQUEST['default_date']));
                    $model->warehousereceipt_no = $_REQUEST['receipt_no'];
                    $model->warehousereceipt_remark = $_REQUEST['vendor'];
                    $model->warehousereceipt_transfer_type = $_REQUEST['transfer_type'];
                    $model->warehousereceipt_warehouseid_from = $_REQUEST['warehouse_from'];

                    if (count($warehousereceiptItem_check) == 0) {
                        $model->warehousereceipt_purchasebill_project = $project_id;
                        $model->warehousereceipt_despatch_id = $_REQUEST['dispatch_no'];
                        $model->warehousereceipt_bill_id = $_REQUEST['purchase_bill_no'];
                        $readonly = "false";
                    } else {
                        $readonly = "true";
                    }
                    $model->warehousereceipt_clerk = $_REQUEST['clerk'];
                    $model->company_id = Yii::app()->user->company_id;
                    $model->updated_by = yii::app()->user->id;
                    $model->updated_date = date('Y-m-d');
                    if ($model->save(false)) {
                        echo json_encode(array('response' => 'success', 'msg' => 'Warehouse receipt updated successfully', 'receipt_id' => $receipt_id, 'readonly' => $readonly));
                    } else {
                        echo json_encode(array('response' => 'error', 'msg' => 'Please enter all details', 'readonly' => $readonly));
                    }
                } else {
                    echo json_encode(array('response' => 'error', 'msg' => 'Receipt no already exit', 'readonly' => $readonly));
                }
            }
        }
    }
    public function unitConversionToBase($item_conversion_unit, $item_base_unit, $stock_item, $quantity_in_conversion_unit)
    {

        if ($item_conversion_unit != '' && $item_base_unit != '' && $stock_item != '') {
            $itemUnitConversion = Yii::app()->db->createCommand("SELECT * FROM {$this->tblpx}unit_conversion WHERE item_id='" . $stock_item . "' AND base_unit='" . $item_base_unit . "' AND conversion_unit='" . $item_conversion_unit . "'")->queryRow();
            if ($itemUnitConversion['id'] != '') {
                $item_conversion_factor = $itemUnitConversion['conversion_factor'];
                $item_conversion_id = $itemUnitConversion['id'];
            } else {
                $item_conversion_factor = 1;
                $item_conversion_id = "";
            }
            $baseunit_quantity = $quantity_in_conversion_unit / $item_conversion_factor;
        } else {
            $item_conversion_factor = 1;
            $item_conversion_id = '';
            $baseunit_quantity = $quantity_in_conversion_unit / $item_conversion_factor;
        }
        $result['item_conversion_unit'] = $item_conversion_unit;
        $result['item_base_unit'] = $item_base_unit;
        $result['item_conversion_factor'] = $item_conversion_factor;
        $result['item_conversion_id'] = $item_conversion_id;
        $result['baseunit_quantity'] = $baseunit_quantity;
        return $result;
    }


    public function getreceipt_stock($warehousereceipt_warehouseid, $stock_item, $rate, $batch)
    {

        $stock = "SELECT warehousestock_stock_quantity,warehousestock_stock_quantity,rate,warehousestock_id,warehousestock_initial_quantity FROM {$this->tblpx}warehousestock WHERE warehousestock_warehouseid=" . $warehousereceipt_warehouseid . " AND warehousestock_itemid=" . $stock_item . "  AND rate = $rate";


        if ($batch == null) {
            $stock .= " AND (batch IS Null OR batch ='" . $batch . "')";
        } else {
            $stock .= " AND batch = '" . $batch . "' ";
        }

        $stock_check = Yii::app()->db->createCommand($stock)->queryRow();
        return $stock_check;
    }

    public function getreceipt_data($warehousereceipt_warehouseid, $stock_item, $rate, $batch)
    {

        $item_receipt_sql = "SELECT SUM(warehousereceipt_baseunit_accepted_effective_quantity)as quantity FROM {$this->tblpx}warehousereceipt_items WHERE warehousereceipt_warehouseid=" . $warehousereceipt_warehouseid . " AND warehousereceipt_itemid=" . $stock_item . "  AND warehousereceipt_rate = $rate ";

        if ($batch == null) {
            $item_receipt_sql .= " AND (warehousereceipt_batch IS Null OR warehousereceipt_batch ='" . $batch . "')";
        } else {
            $item_receipt_sql .= " AND warehousereceipt_batch='" . $batch . "' ";
        }

        $item_receipt = Yii::app()->db->createCommand($item_receipt_sql)->queryRow();
        return $item_receipt;
    }

    public function getdespatch_data($warehousereceipt_warehouseid, $stock_item, $rate, $batch)
    {

        $item_despatch_sql = "SELECT SUM(warehousedespatch_baseunit_quantity)as quantity FROM {$this->tblpx}warehousedespatch_items WHERE warehousedespatch_warehouseid=" . $warehousereceipt_warehouseid . " AND warehousedespatch_itemid=" . $stock_item . "  AND warehousedespatch_rate = $rate ";

        if ($batch == null) {
            $item_despatch_sql .= " AND (warehousedespatch_batch IS Null OR warehousedespatch_batch ='" . $batch . "')";
        } else {
            $item_despatch_sql .= " AND warehousedespatch_batch='" . $batch . "'";
        }

        $item_despatch = Yii::app()->db->createCommand($item_despatch_sql)->queryRow();

        return $item_despatch;
    }


    public function usedqty_receiptcheck($item_id, $purchase_bill_no, $dispatch_no, $transfer_type)
    {
        $transfer_query = "SELECT SUM(wri.warehousereceipt_baseunit_effective_quantity) as warehousestock_total_quantity_received,
                    SUM(wri.warehousereceipt_baseunit_accepted_effective_quantity) as warehousestock_total_accepted_quantity,
                    SUM(wri.warehousereceipt_baseunit_rejected_effective_quantity) as warehousestock_total_rejected_quantity,
                    wri.warehousereceipt_batch,wri.warehousereceipt_itemid,wr.warehousereceipt_transfer_type,wri.warehousereceipt_transfer_type_item_id
                    FROM " . $this->tblpx . "warehousereceipt_items as wri
                    INNER JOIN " . $this->tblpx . "warehousereceipt as wr 
                    ON wri.warehousereceipt_id = wr.warehousereceipt_id
                    WHERE wri.warehousereceipt_transfer_type_item_id='" . $item_id . "'";
        if ($transfer_type == 1) {
            $transfer_query .= "AND wr.warehousereceipt_bill_id='" . $purchase_bill_no . "'";
        } else {
            $transfer_query .= "AND wr.warehousereceipt_despatch_id='" . $dispatch_no . "'";
        }


        $used_quantity_check = Yii::app()->db->createCommand($transfer_query)->queryRow();

        return $used_quantity_check;
    }


    public function actionreceiptitem()
    {

        $result['html'] = '';
        $warehouseStockId = '';
        $total_item_quantity = 0;
        $warehousereceipt_id = 0;
        $transfer_type = isset($_REQUEST['transfer_type']) ? $_REQUEST['transfer_type'] : "";
        $warehousereceipt_warehouseid = isset($_REQUEST['Warehousereceipt']['warehousereceipt_warehouseid']) ? $_REQUEST['Warehousereceipt']['warehousereceipt_warehouseid'] : "";
        $warehousereceipt_warehouseid_from = isset($_REQUEST['Warehousereceipt']['warehousereceipt_warehouseid_from']) ? $_REQUEST['Warehousereceipt']['warehousereceipt_warehouseid_from'] : NULL;
        $remaining_quantity = 0;
        $warehouse_receipt_item_length = null;
        if ($_REQUEST['warehousereceipt_id'] == 0) {
            echo json_encode(array('response' => 'error', 'msg' => 'Please enter Warehouse receipt details'));
        } else {

            extract($_REQUEST);
            if (isset($sl_no)) {

                $item_id = $this->inputval('item_id');
                $stock_item = $this->inputval('stock_item');
                $transfer_type_item_id = $this->inputval('item_id');
                $rate = $this->inputval('rates', 0);
                $transferablequantity = $this->inputval('transferablequantity');
                $item_unit_id = $this->inputval('item_unit_id');
                $item_unit_name = $_REQUEST['item_unit_id'];
                $item_base_unit = $this->inputval('item_unit_base_unit');
                $batch = $this->inputval('batch');
                $receivedquantity = $this->inputval('receivedquantity');
                $accepted_quantity = $this->inputval('accepted_quantity');
                $rejected_quantity = $this->inputval('rejected_quantity');
                $remark_data = $this->inputval('remark_data');
                $jono = $this->inputval('jono');
                $unitconversiondata =  $this->unitConversionToBase($item_unit_id, $item_base_unit, $stock_item, $transferablequantity);
                $item_conversion_id = $unitconversiondata['item_conversion_id'];
                $itemqty = $this->inputval('itemqty');
                $itemunit = $this->inputval('itemunit');
                $itemrate = $this->inputval('itemrate');
                $dispatch_no = $this->inputval('dispatch_no');
                $purchase_bill_no = $this->inputval('purchase_bill_no');

                if ($transfer_type == 1) {

                    $transfer_type_name = "Billed ";
                    $billitem_purchase_type = $this->inputval('billitem_purchase_type', "");
                    $warehouse_receipt_item_length = $this->inputval('warehouse_receipt_item_length', "");
                    $warehouse_receipt_item_width = $this->inputval('warehouse_receipt_item_width', "");
                    $warehouse_receipt_item_height =  $this->inputval('warehouse_receipt_item_height', "");

                    if ($billitem_purchase_type != '') {

                        if ($billitem_purchase_type == 'A') {
                            $dimension = null;
                        } else {
                            if ($billitem_purchase_type == 'G') {
                                $dimension = $warehouse_receipt_item_width . " x " . $warehouse_receipt_item_height;
                            } else {
                                $dimension = null;
                            }
                        }
                    } else {
                        $dimension = null;
                    }
                } else {
                    $transfer_type_name = "Despatched ";
                    $purchase_type = $this->inputval('purchase_type', "");
                    $item_dimension = $this->inputval('item_dimension', "");
                    $item_dimension_category = $this->inputval('item_dimension_category', 1);
                    $dimension = $item_dimension;
                }

                if ($item_unit_id != $item_base_unit) {

                    $receivedquantity_baseunit_quantity = $receivedquantity;
                    $transferablequantity_baseunit_quantity = $transferablequantity;
                    $accepted_quantity_baseunit_quantity = $accepted_quantity;
                    $rejected_quantity_baseunit_quantity = $rejected_quantity;
                    $total_base_unit_quantity = $accepted_quantity_baseunit_quantity + $rejected_quantity_baseunit_quantity;
                    if ($transfer_type != 2) {
                        $base_rate = $rate;
                    } else {
                        $base_rate = $rate;
                    }
                } else {

                    $receivedquantity_baseunit_quantity = $receivedquantity;
                    $transferablequantity_baseunit_quantity = $transferablequantity;
                    $accepted_quantity_baseunit_quantity = $accepted_quantity;
                    $rejected_quantity_baseunit_quantity = $rejected_quantity;
                    $total_base_unit_quantity = $accepted_quantity_baseunit_quantity + $rejected_quantity_baseunit_quantity;
                    $item_receivedquantity_UnitConversion = $this->unitConversionToBase($item_unit_id, $item_base_unit, $stock_item, $receivedquantity);
                    $item_conversion_id = $item_receivedquantity_UnitConversion['item_conversion_id'];
                    $base_rate = $rate;
                }

                $warehousereceipt_accepted_effective_quantity = $accepted_quantity;
                $warehousereceipt_baseunit_accepted_effective_quantity = $accepted_quantity_baseunit_quantity;
                $warehousereceipt_effective_quantity = $receivedquantity;
                $warehousereceipt_baseunit_effective_quantity = $receivedquantity_baseunit_quantity;
                $warehousereceipt_rejected_effective_quantity = $rejected_quantity;
                $warehousereceipt_baseunit_rejected_effective_quantity = $rejected_quantity_baseunit_quantity;
                $transferable_baseunit_effective_quantity = $transferablequantity_baseunit_quantity;
                $total_base_unit_effective_quantity = $total_base_unit_quantity;

                if ($transfer_type != "" && $transfer_type_item_id != NULL && $item_id != NULL) {

                    $used_quantity_check =  $this->usedqty_receiptcheck($item_id, $purchase_bill_no, $dispatch_no, $transfer_type);
                    $used_quantitycheck = isset($used_quantity_check['warehousestock_total_quantity_received']) ? $used_quantity_check['warehousestock_total_quantity_received'] : 0;
                    $used_quantity = $used_quantitycheck + $warehousereceipt_baseunit_effective_quantity;
                    $remaining_quantity = $transferable_baseunit_effective_quantity - $used_quantitycheck;
                } else {

                    $used_quantity = $warehousereceipt_baseunit_effective_quantity;
                    $remaining_quantity = $transferable_baseunit_effective_quantity;
                }



                if ($transferable_baseunit_effective_quantity < $used_quantity) {
                    echo json_encode(array('response' => 'error', 'msg' => 'The ' . $transfer_type_name . " quantity is already received"));
                } else {
                    if ($transferable_baseunit_effective_quantity < $warehousereceipt_baseunit_effective_quantity) {

                        echo json_encode(array('response' => 'error', 'msg' => 'The entered quantity must be less than ' . $transferablequantity_baseunit_quantity . "" . $item_unit_name));
                    } else {
                        if ($warehousereceipt_baseunit_effective_quantity < $total_base_unit_effective_quantity) {

                            echo json_encode(array('response' => 'error', 'msg' => 'Accepted Quantity AND Rejected Quantity must be less than the Received quantity entered'));
                        } else {

                            $item_model = new WarehousereceiptItems();
                            $item_model->warehousereceipt_id = $warehousereceipt_id;
                            $item_model->warehousereceipt_quantity = $receivedquantity;
                            $item_model->dimension = $dimension;
                            $item_model->length = $warehouse_receipt_item_length;
                            $item_model->warehousereceipt_accepted_quantity = $accepted_quantity;
                            $item_model->warehousereceipt_accepted_effective_quantity = $warehousereceipt_accepted_effective_quantity;
                            $item_model->warehousereceipt_rejected_quantity = $rejected_quantity;
                            $item_model->warehousereceipt_baseunit_accepted_quantity = $accepted_quantity_baseunit_quantity;
                            $item_model->warehousereceipt_baseunit_accepted_effective_quantity = $warehousereceipt_baseunit_accepted_effective_quantity;
                            $item_model->warehousereceipt_baseunit_quantity = $receivedquantity_baseunit_quantity;
                            $item_model->warehousereceipt_baseunit_rejected_quantity = $rejected_quantity_baseunit_quantity;
                            $item_model->warehousereceipt_unitConversion_id = $item_conversion_id;
                            $item_model->warehousereceipt_effective_quantity = $warehousereceipt_effective_quantity;
                            $item_model->warehousereceipt_baseunit_effective_quantity = $warehousereceipt_baseunit_effective_quantity;
                            $item_model->warehousereceipt_rejected_effective_quantity = $warehousereceipt_rejected_effective_quantity;
                            $item_model->warehousereceipt_baseunit_rejected_effective_quantity = $warehousereceipt_baseunit_rejected_effective_quantity;
                            $item_model->warehousereceipt_rate = $rate;
                            $item_model->warehousereceipt_transfer_type_item_id = $item_id;
                            $item_model->warehousereceipt_unit = $item_unit_name;
                            $item_model->warehousereceipt_batch = $batch;
                            $item_model->remark_data = $remark_data;
                            $item_model->warehousereceipt_jono = $jono;
                            $item_model->warehousereceipt_warehouseid = $warehousereceipt_warehouseid;
                            $item_model->warehousereceipt_itemid = $stock_item;
                            $item_model->warehousereceipt_itemunit = $itemunit;
                            $item_model->warehousereceipt_itemqty = $itemqty;
                            $item_model->warehousereceipt_itemrate = $itemrate;
                            $item_model->created_by = Yii::app()->user->id;
                            $item_model->created_date = date('Y-m-d H:i:s');
                            $item_model->updated_by = Yii::app()->user->id;
                            $item_model->updated_date = date('Y-m-d H:i:s');
                            $transaction = Yii::app()->db->beginTransaction();
                            try {
                                if ($item_model->save()) {

                                    $warehouse_receipt_item_id = Yii::app()->db->getLastInsertID();
                                    $newdata = array(

                                        'item_id'  => $warehouse_receipt_item_id,
                                        'warehousereceipt_id' => $warehousereceipt_id,
                                        'warehousereceipt_warehouseid' => $warehousereceipt_warehouseid,
                                        'warehousereceipt_itemid' => $stock_item,
                                        'warehousereceipt_quantity' => $receivedquantity,
                                        'warehousereceipt_rate' => $rate,
                                        'warehousereceipt_unit' => $item_unit_name,
                                        'warehousereceipt_unitConversion_id' => $item_conversion_id,
                                        'warehousereceipt_batch' => $batch,
                                        'dimension' => $dimension,
                                        'length' => $warehouse_receipt_item_length
                                    );

                                    $module = 'warehousereceipt';
                                    $recid = $warehouse_receipt_item_id;
                                    $actionlog = 'add';
                                    $olddata = array();

                                    $log_storing_response = $this->Logcreate($newdata, $olddata, $module, $recid, $actionlog);

                                    if ($log_storing_response['error'] === true) {
                                        throw new Exception($logdata['message']);
                                    }

                                    $stock = $this->getreceipt_stock($warehousereceipt_warehouseid, $stock_item, $rate, $batch);
                                    if (!empty($stock)) {
                                        $item_receipt = $this->getreceipt_data($warehousereceipt_warehouseid, $stock_item, $rate, $batch);
                                        $item_despatch = $this->getdespatch_data($warehousereceipt_warehouseid, $stock_item, $rate, $batch);

                                        if ($item_receipt['quantity'] == "") {
                                            $item_receipt_quantity = 0;
                                        } else {
                                            $item_receipt_quantity = $item_receipt['quantity'];
                                        }
                                        if ($item_despatch['quantity'] == "") {
                                            $item_despatch_quantity = 0;
                                        } else {
                                            $item_despatch_quantity = $item_despatch['quantity'];
                                        }


                                        $final_quantity = ($stock['warehousestock_initial_quantity'] + $item_receipt_quantity) - $item_despatch_quantity;
                                        $model2 = Warehousestock::model()->findByPk($stock['warehousestock_id']);
                                        $model2->warehousestock_stock_quantity = $final_quantity;
                                        $model2->warehousereceipt_itemid = $stock_item;
                                        $model2->dimension = $dimension;
                                        $model2->rate = $base_rate;
                                        $model2->updated_date = date('Y-m-d H:i:s');
                                        if ($model2->save(false)) {
                                        } else {
                                            throw new Exception('An error occured in add');
                                        }
                                    } else {


                                        $model2 = new Warehousestock;
                                        $model2->warehousestock_warehouseid = $warehousereceipt_warehouseid;
                                        $model2->warehousestock_date = date('Y-m-d');
                                        $model2->warehousereceipt_itemid = $stock_item;
                                        $model2->warehousestock_itemid = $stock_item;
                                        $model2->dimension = $dimension;
                                        $model2->warehousestock_stock_quantity = $warehousereceipt_baseunit_accepted_effective_quantity;
                                        $model2->rate = $base_rate;
                                        $model2->batch = $batch;
                                        $model2->warehousestock_unit = $item_base_unit;
                                        $model2->warehousestock_status = 1;
                                        $model2->created_by = yii::app()->user->id;
                                        $model2->created_date = date('Y-m-d H:i:s');
                                        $model2->company_id = Yii::app()->user->company_id;
                                        $model2->updated_by = yii::app()->user->id;
                                        $model2->updated_date = date('Y-m-d H:i:s');
                                        if ($model2->save(false)) {
                                        } else {
                                            throw new Exception('An error occured in add');
                                        }
                                    }

                                    $item_quantity = Yii::app()->db->createCommand("SELECT SUM(warehousereceipt_baseunit_quantity) as quantity FROM {$this->tblpx}warehousereceipt_items WHERE warehousereceipt_id=" . $warehousereceipt_id . "")->queryScalar();
                                    $model = Warehousereceipt::model()->findByPk($warehousereceipt_id);
                                    $model->warehousereceipt_quantity = $item_quantity;
                                    $model->company_id = Yii::app()->user->company_id;
                                    if ($model->save(false)) {
                                    } else {
                                        throw new Exception('An error occured in add');
                                    }

                                    $last_id = $item_model->item_id;
                                    $result = '';
                                    if ($transfer_type != "" && $transfer_type_item_id == NULL && $item_id == NULL) {
                                        $receipt_details = Yii::app()->db->createCommand(" SELECT * FROM {$this->tblpx}warehousereceipt_items  WHERE warehousereceipt_id=" . $warehousereceipt_id . " AND warehousereceipt_transfer_type_item_id IS NULL")->queryAll();
                                        foreach ($receipt_details as $key => $values) {

                                            $specification = Yii::app()->db->createCommand("SELECT id, cat_id,brand_id, specification, unit FROM {$this->tblpx}specification WHERE id=" . $values['warehousereceipt_itemid'] . "")->queryRow();
                                            $parent_category = Yii::app()->db->createCommand("SELECT * FROM {$this->tblpx}purchase_category WHERE id='" . $specification['cat_id'] . "'")->queryRow();

                                            if ($specification['brand_id'] != NULL) {
                                                $brand_details = Yii::app()->db->createCommand("SELECT brand_name FROM {$this->tblpx}brand WHERE id=" . $specification['brand_id'] . "")->queryRow();
                                                $brand = '-' . $brand_details['brand_name'];
                                            } else {
                                                $brand = '';
                                            }

                                            $spc_details = $parent_category['category_name'] . $brand . ' - ' . $specification['specification'];
                                            $spc_id = $specification['id'];
                                            $warehousereceipt_unit = $values['warehousereceipt_unit'];
                                            $warehousereceipt_itemunit = $values['warehousereceipt_itemunit'];

                                            if ($warehousereceipt_unit != $warehousereceipt_itemunit) {
                                                if ($values['warehousereceipt_itemqty'] > 0) {
                                                    $quantity_data = $values['warehousereceipt_itemqty'];
                                                } else {
                                                    $quantity_data = $values['warehousereceipt_quantity'];
                                                }

                                                if ($values['warehousereceipt_itemrate'] > 0) {
                                                    $rate_data = $values['warehousereceipt_itemrate'];
                                                } else {
                                                    $rate_data = $values['warehousereceipt_rate'];
                                                }

                                                if ($values['warehousereceipt_itemunit'] != '') {
                                                    $unit_data = $values['warehousereceipt_itemunit'];
                                                } else {
                                                    $unit_data = $values['warehousereceipt_unit'];
                                                }
                                                $quantity_base_data = $values['warehousereceipt_quantity'];
                                                $rate_base_data = $values['warehousereceipt_rate'];
                                                $unit_base_data = $values['warehousereceipt_unit'];
                                                $additional_amount = $quantity_data * $rate_data;
                                            } else {

                                                $quantity_data = $values['warehousereceipt_quantity'];
                                                $rate_data = $values['warehousereceipt_rate'];
                                                $unit_data =  $values['warehousereceipt_unit'];
                                                $unit_base_data = $values['warehousereceipt_unit'];
                                                $quantity_base_data = $values['warehousereceipt_quantity'];
                                                $rate_base_data = $values['warehousereceipt_rate'];
                                                $additional_amount = $values['warehousereceipt_quantity'] * $values['warehousereceipt_rate'];
                                            }
                                            $result .= '<tr>';
                                            $result .= '<td><div id="item_sl_no">' . ($key + 1) . '</div></td>';
                                            $result .= '<td><div class="item_description" id="' . $spc_id . '">' . $spc_details . '</div> </td>';
                                            $result .= '<td><div class="item_description" id="batch' . $values['item_id'] . '">' . $values['warehousereceipt_batch'] . '</div> </td>';
                                            $result .= '<td> <div class="unit" id="unit' . $values['item_id'] . '"> ' . $unit_data . '</div> </td>';
                                            $result .= '<td> <div class="" id="quantity' . $values['item_id'] . '"> ' . $quantity_data . '</div> </td>';
                                            $result .= '<td> <div class="" id="rate' . $values['item_id'] . '"> ' . Controller::money_format_inr($rate_data, 2, 1) . '</div> </td>';
                                            // $result .= '<td> <div class="unit" id="jono' . $values['item_id'] . '"> ' . $values['warehousereceipt_jono'] . '</div> </td>';
                                            $result .= '<td> <div class="item_qty_base" id="item_qty_base' . $values['item_id'] . '"> ' . $quantity_base_data  . '</div> </td>';
                                            $result .= '<td> <div class="item_unit_base" id="item_unit_base' . $values['item_id'] . '"> ' . $unit_base_data . '</div> </td>';
                                            $result .= '<td> <div class="item_rate_base" id="item_rate_base' . $values['item_id'] . '"> ' . $rate_base_data . '</div> </td>';
                                            $result .= '<td> <div class="unit" id="amount' . $values['item_id'] . '"> ' . Controller::money_format_inr($additional_amount, 2, 1) . '</div> </td>';

                                            $result .= '<td width="70"><span class="icon icon-options-vertical popover-test" data-toggle="popover"   data-placement="left" type="button" data-html="true" style="cursor: pointer;" ></span>'
                                                . '<div class ="popover-content hide"> '
                                                . '<ul class="tooltip-hiden">'
                                                . '<li><a href="#" id=' . $values['item_id'] . ' class="btn btn-default btn-xs removebtn">Delete</a></li> '
                                                . '<li><a href="" id=' . $values['item_id'] . ' class="btn btn-default btn-xs edit_item" style="margin-left:3px">Edit</a></li>';
                                            $result .= '</ul>';
                                            $result .= '</div> ';
                                            $result .= '</td>';

                                            $result .= '</tr>';
                                        }
                                    } else {
                                        $result = '';
                                    }

                                    echo json_encode(array('response' => 'success', 'msg' => 'Receipt item saved successfully', 'html' => $result, 'warehouse_receipt_item_id' => $warehouse_receipt_item_id, 'remaining_quantity' => $remaining_quantity));
                                } else {
                                    throw new Exception('An error occured in add');

                                    echo json_encode(array('response' => 'error', 'msg' => 'Some problem occured'));
                                }
                                $transaction->commit();
                            } catch (Exception $error) {

                                $transaction->rollBack();
                                $result = array('status' => '0', 'msg' => $error->getMessage());

                                echo json_encode(array('response' => 'error', 'msg' => 'Some problem occured'));
                            }
                        }
                    }
                }
            } else {
                $sl_no = '';
                $stock_item = '';
                $transferablequantity = '';
                $transfer_type_item_id = '';
                $rate = '';
                $transferablequantity = '';
                $item_unit_id = '';
                $item_unit_base_unit = '';
                $batch = NULL;
                $receivedquantity = '';
                $accepted_quantity = '';
                $rejected_quantity = '';
                $jono = '';
            }
        }
    }







    public function actionreceiptitem11()
    {
        $data = $_REQUEST['data'];
        $result['html'] = '';
        $warehouseStockId = '';
        $total_item_quantity = 0;


        if ($data['receipt_id'] == 0) {
            echo json_encode(array('response' => 'error', 'msg' => 'Please enter Warehouse receipt details'));
        } else {
            if (isset($data['sl_No'])) {
                $sl_No = $data['sl_No'];
            } else {
                $sl_No = '';
            }
            if (isset($data['stock_item'])) {
                $stock_item = $data['stock_item'];
            } else {
                $stock_item = '';
            }

            if (isset($data['quantity'])) {
                $quantity = $data['quantity'];
            } else {
                $quantity = 0;
            }
            if (isset($data['accepted_quantity'])) {
                $accepted_quantity = $data['accepted_quantity'];
            } else {
                $accepted_quantity = 0;
            }
            if (isset($data['rejected_quantity'])) {
                $rejected_quantity = $data['rejected_quantity'];
            } else {
                $rejected_quantity = 0;
            }

            if (isset($data['unit'])) {
                $unit = $this->GetItemunitName($data['unit']);
                $unit_id = $data['unit'];
            } else {
                $unit = '';
                $unit_id = '';
            }
            $receipt = Warehousereceipt::model()->findByPk($data['receipt_id']);
            $itemspecification = Yii::app()->db->createCommand("SELECT id, parent_id, brand_id, specification, unit FROM {$this->tblpx}purchase_category WHERE id=" . $stock_item . "")->queryRow();
            $itemunit = Yii::app()->db->createCommand("SELECT id, unit_name FROM {$this->tblpx}unit WHERE id=" . $itemspecification['unit'] . "")->queryRow();
            if (!empty($itemunit)) {
                $item_base_unit = $itemunit['id'];
            } else {
                $item_base_unit = '';
            }
            if (isset($data['batch'])) {
                $full_batch = trim($data['batch']);
                $batch_details = explode("_", $data['batch']);
                $Itembatch_from = isset($batch_details[0]) ? $batch_details[0] : "";
                $Itembatch_id = isset($batch_details[1]) ? $batch_details[1] : $data['batch'];
                $billitem_count = Billitem::model()->countByAttributes(array(
                    'billitem_id' => $Itembatch_id,
                ));
                $warehousedespatchItemsItems_count = WarehousedespatchItems::model()->countByAttributes(array(
                    'item_id' => $Itembatch_id,
                ));
                if ($billitem_count != 0  && $Itembatch_from == 'bill') {
                    $billitem           = Billitem::model()->findByPk($Itembatch_id);
                    $billitem_batch      = $billitem["batch"];
                    $billitem_quantity      = $billitem["billitem_quantity"];
                    $billitem_rate      = $billitem["billitem_rate"];
                    $billitem_unit      = $this->GetItemunitID($billitem["billitem_unit"]);
                    $billitemUnitConversion = $this->unitConversionToBase($billitem_unit, $item_base_unit, $stock_item, $billitem_quantity);
                    $billitem_conversion_factor = $billitemUnitConversion['item_conversion_factor'];
                    $billitem_baseunit_quantity = $billitemUnitConversion['baseunit_quantity'];
                    $batch = $billitem_batch;
                    $receipt_type = "billno_" . $Itembatch_id;
                    $actual_item_quantity = $billitem_baseunit_quantity;
                    $actual_item_rate = $billitem_rate * $billitem_conversion_factor;
                    $actual_item_amount = $actual_item_quantity * $actual_item_rate;
                } elseif ($warehousedespatchItemsItems_count != 0 && $Itembatch_from == 'dispatch') {
                    $warehousedespatchItems = WarehousedespatchItems::model()->findByPk($Itembatch_id);
                    $warehousedespatchItems_batch = $warehousedespatchItems["warehousedespatch_batch"];
                    $batch = $warehousedespatchItems_batch;
                    $receipt_type = "warehousedespatchno_" . $Itembatch_id;
                    if ($batch == null) {
                        $warehouseStockDetail = Yii::app()->db->createCommand("SELECT warehousestock_stock_quantity,rate,warehousestock_id,warehousestock_initial_quantity FROM {$this->tblpx}warehousestock WHERE warehousestock_warehouseid=" . $receipt->warehousereceipt_warehouseid . " AND warehousestock_itemid=" . $stock_item . " AND batch = null")->queryRow();
                    } else {
                        $warehouseStockDetail = Yii::app()->db->createCommand("SELECT warehousestock_stock_quantity,rate,warehousestock_id,warehousestock_initial_quantity FROM {$this->tblpx}warehousestock WHERE warehousestock_warehouseid=" . $receipt->warehousereceipt_warehouseid . " AND warehousestock_itemid=" . $stock_item . " AND batch = '" . $batch . "'")->queryRow();
                    }
                    $actual_item_quantity = $warehousedespatchItems["warehousedespatch_baseunit_quantity"];
                    $actual_item_rate = $warehouseStockDetail['rate'];
                    $actual_item_amount = $actual_item_quantity * $actual_item_rate;
                } else {
                    $warehouseStockId = $Itembatch_id;
                    $warehouseStock = Warehousestock::model()->findByPk($warehouseStockId);
                    $batch = $warehouseStock['batch'];
                    $receipt_type = "warehouseStockId_" . $Itembatch_id;
                    $warehouseStock_rate = $warehouseStock['rate'];
                    $actual_item_unit = $unit_id;
                    $actualItemQuantityUnitConversion = $this->unitConversionToBase($actual_item_unit, $item_base_unit, $stock_item, $quantity);
                    $actual_item_conversion_factor = $actualItemQuantityUnitConversion['item_conversion_factor'];
                    $actual_item_quantity = $actualItemQuantityUnitConversion['baseunit_quantity'];
                    $warehouseStock_quantity = $warehouseStock['warehousestock_stock_quantity'];
                    $actual_item_rate = $warehouseStock_rate;
                    $actual_item_amount = $actual_item_quantity * $actual_item_rate;
                }
            } else {
                $full_batch = '';
                $batch = null;
                $receipt_type = null;
                $Itembatch_id = '';
            }
            $item_conversion_unit = $unit_id;
            $warehousereceiptQuantityUnitConversion = $this->unitConversionToBase($item_conversion_unit, $item_base_unit, $stock_item, $quantity);
            $item_conversion_factor = $warehousereceiptQuantityUnitConversion['item_conversion_factor'];
            $item_conversion_id = $warehousereceiptQuantityUnitConversion['item_conversion_id'];
            $item_available_for_receive = $actual_item_quantity * $item_conversion_factor;
            $warehousereceipt_baseunit_quantity = $warehousereceiptQuantityUnitConversion['baseunit_quantity'];
            $warehousereceipt_baseunit_unit = $warehousereceiptQuantityUnitConversion['item_base_unit'];
            $warehousereceipt_AcceptedQuantityUnitConversion = $this->unitConversionToBase($item_conversion_unit, $item_base_unit, $stock_item, $accepted_quantity);
            $warehousereceipt_baseunit_accepted_quantity = $warehousereceipt_AcceptedQuantityUnitConversion['baseunit_quantity'];
            $warehousereceipt_baseunit_accepted_quantity_unit = $warehousereceipt_AcceptedQuantityUnitConversion['item_base_unit'];
            if ($rejected_quantity == 0) {
                $warehousereceipt_baseunit_rejected_quantity = 0;
            } else {
                $warehousereceipt_RejectedQuantityUnitConversion = $this->unitConversionToBase($item_conversion_unit, $item_base_unit, $stock_item, $rejected_quantity);
                $warehousereceipt_baseunit_rejected_quantity = $warehousereceipt_RejectedQuantityUnitConversion['baseunit_quantity'];
                $warehousereceipt_baseunit_rejected_quantity_unit = $warehousereceipt_RejectedQuantityUnitConversion['item_base_unit'];
            }
            if ($Itembatch_from == 'bill' || $Itembatch_from == 'dispatch') {
                if ($batch == null) {
                    $used_quantity_check = Yii::app()->db->createCommand("SELECT 
                        SUM(warehousereceipt_baseunit_quantity) as warehousestock_total_quantity_received,
                        SUM(warehousereceipt_baseunit_accepted_effective_quantity) as warehousestock_total_accepted_quantity,
                        SUM(warehousereceipt_baseunit_rejected_quantity) as warehousestock_total_rejected_quantity,
                        warehousereceipt_batch,warehousereceipt_itemid
                        FROM {$this->tblpx}warehousereceipt_items 
                        WHERE warehousereceipt_itemid=" . $stock_item . " AND (warehousereceipt_batch IS Null OR warehousereceipt_batch ='" . $batch . "')")->queryRow();
                    $used_quantity = isset($used_quantity_check['warehousestock_total_quantity_received']) ? $used_quantity_check['warehousestock_total_quantity_received'] : 0;
                    $used_quantity = $used_quantity + $warehousereceipt_baseunit_quantity;
                } else {
                    $used_quantity_check = Yii::app()->db->createCommand("SELECT 
                        SUM(warehousereceipt_baseunit_quantity) as warehousestock_total_quantity_received,
                        SUM(warehousereceipt_baseunit_accepted_effective_quantity) as warehousestock_total_accepted_quantity,
                        SUM(warehousereceipt_baseunit_rejected_quantity) as warehousestock_total_rejected_quantity,
                        warehousereceipt_batch,warehousereceipt_itemid
                        FROM {$this->tblpx}warehousereceipt_items 
                        WHERE warehousereceipt_itemid=" . $stock_item . " AND warehousereceipt_batch='" . $batch . "'")->queryRow();
                    $used_quantity = isset($used_quantity_check['warehousestock_total_quantity_received']) ? $used_quantity_check['warehousestock_total_quantity_received'] : 0;
                    $used_quantity = $used_quantity + $warehousereceipt_baseunit_quantity;
                }
            } else {
                $used_quantity = $actual_item_quantity;
            }
            if (isset($data['jono'])) {
                $jono = $data['jono'];
            } else {
                $jono = '';
            }
            $total_quantity = $accepted_quantity + $rejected_quantity;
            if ($total_quantity != $quantity) {
                $rejected_quantity = $quantity - $accepted_quantity;
                $total_quantity = $accepted_quantity + $rejected_quantity;
            }



            if ($used_quantity <= $actual_item_quantity) {
                if ($warehousereceipt_baseunit_quantity <= $actual_item_quantity) {
                    if ($accepted_quantity <= $total_quantity) {


                        $item_model = new WarehousereceiptItems();
                        $item_model->warehousereceipt_id = $data['receipt_id'];
                        $item_model->warehousereceipt_quantity = $quantity;
                        $item_model->warehousereceipt_accepted_quantity = $accepted_quantity;
                        $item_model->warehousereceipt_rejected_quantity = $rejected_quantity;
                        $item_model->warehousereceipt_baseunit_accepted_quantity = $warehousereceipt_baseunit_accepted_quantity;
                        $item_model->warehousereceipt_baseunit_quantity = $warehousereceipt_baseunit_quantity;
                        $item_model->warehousereceipt_baseunit_rejected_quantity = $warehousereceipt_baseunit_rejected_quantity;
                        $item_model->warehousereceipt_unitConversion_id = $item_conversion_id;
                        $item_model->warehousereceipt_unit = $unit;
                        $item_model->warehousereceipt_batch = $batch;
                        $item_model->receipt_type = $receipt_type;
                        $item_model->warehousereceipt_jono = $jono;
                        $item_model->warehousereceipt_warehouseid = $receipt->warehousereceipt_warehouseid;
                        $item_model->warehousereceipt_itemid = $stock_item;
                        $item_model->created_by = Yii::app()->user->id;
                        $item_model->created_date = date('Y-m-d');
                        $item_model->updated_by = Yii::app()->user->id;
                        $item_model->updated_date = date('Y-m-d');
                        if ($item_model->save()) {
                            $stock_item_quantity = 0;
                            $stock_item_amount = 0;
                            if ($batch == null) {
                                $stock = Yii::app()->db->createCommand("SELECT SUM(warehousestock_stock_quantity) as warehousestock_total_quantity,warehousestock_stock_quantity,rate,warehousestock_id,warehousestock_initial_quantity FROM {$this->tblpx}warehousestock WHERE warehousestock_warehouseid=" . $receipt->warehousereceipt_warehouseid . " AND warehousestock_itemid=" . $stock_item . " AND (batch IS Null OR batch ='" . $batch . "')")->queryRow();
                                $stock_item_quantity = $stock['warehousestock_total_quantity'];
                                $stock_item_amount = ($stock['warehousestock_total_quantity'] * $stock['rate']);
                            } else {
                                $stock = Yii::app()->db->createCommand("SELECT warehousestock_stock_quantity,rate,warehousestock_id,warehousestock_initial_quantity FROM {$this->tblpx}warehousestock WHERE warehousestock_warehouseid=" . $receipt->warehousereceipt_warehouseid . " AND warehousestock_itemid=" . $stock_item . " AND batch = '" . $batch . "'")->queryRow();
                                $stock_item_quantity = $stock['warehousestock_stock_quantity'];
                                $stock_item_amount = ($stock['warehousestock_stock_quantity'] * $stock['rate']);
                            }
                            $total_item_quantity = $stock_item_quantity +  $actual_item_quantity;
                            $total_item_amount = $stock_item_amount +  $actual_item_amount;
                            $weighted_avg_rate = $total_item_amount / $total_item_quantity;
                            if (!empty($stock)) {

                                if ($batch == null) {
                                    $item_receipt = Yii::app()->db->createCommand("SELECT SUM(warehousereceipt_baseunit_accepted_effective_quantity)as quantity FROM {$this->tblpx}warehousereceipt_items WHERE warehousereceipt_warehouseid=" . $receipt->warehousereceipt_warehouseid . " AND warehousereceipt_itemid=" . $stock_item . " AND (warehousereceipt_batch IS Null OR warehousereceipt_batch ='" . $batch . "') AND receipt_type='" . $receipt_type . "'")->queryRow();
                                    $item_despatch = Yii::app()->db->createCommand("SELECT SUM(warehousedespatch_baseunit_quantity)as quantity FROM {$this->tblpx}warehousedespatch_items WHERE warehousedespatch_warehouseid=" . $receipt->warehousereceipt_warehouseid . " AND warehousedespatch_itemid=" . $stock_item . " AND (warehousedespatch_batch IS Null OR warehousedespatch_batch ='" . $batch . "')")->queryRow();
                                } else {
                                    $item_receipt = Yii::app()->db->createCommand("SELECT SUM(warehousereceipt_baseunit_accepted_effective_quantity)as quantity FROM {$this->tblpx}warehousereceipt_items WHERE warehousereceipt_warehouseid=" . $receipt->warehousereceipt_warehouseid . " AND warehousereceipt_itemid=" . $stock_item . " AND warehousereceipt_batch='" . $batch . "' AND receipt_type='" . $receipt_type . "'")->queryRow();
                                    $item_despatch = Yii::app()->db->createCommand("SELECT SUM(warehousedespatch_baseunit_quantity)as quantity FROM {$this->tblpx}warehousedespatch_items WHERE warehousedespatch_warehouseid=" . $receipt->warehousereceipt_warehouseid . " AND warehousedespatch_itemid=" . $stock_item . " AND warehousedespatch_batch='" . $batch . "'")->queryRow();
                                }
                                if ($item_receipt['quantity'] == "") {
                                    $item_receipt_quantity = 0;
                                } else {
                                    $item_receipt_quantity = $item_receipt['quantity'];
                                }
                                if ($item_despatch['quantity'] == "") {
                                    $item_despatch_quantity = 0;
                                } else {
                                    $item_despatch_quantity = $item_despatch['quantity'];
                                }
                                $final_quantity = ($stock['warehousestock_initial_quantity'] + $item_receipt_quantity) - $item_despatch_quantity;
                                $model2 = Warehousestock::model()->findByPk($stock['warehousestock_id']);
                                $model2->warehousestock_stock_quantity = $final_quantity;
                                $model2->rate = $weighted_avg_rate;
                                $model2->save(false);
                            } else {

                                $model2 = new Warehousestock;
                                $model2->warehousestock_warehouseid = $receipt->warehousereceipt_warehouseid;
                                $model2->warehousestock_date = date('Y-m-d');
                                $model2->warehousestock_itemid = $stock_item;
                                $model2->warehousestock_stock_quantity = $warehousereceipt_baseunit_accepted_quantity;
                                //$model2->warehousestock_initial_quantity = $warehousereceipt_baseunit_accepted_quantity;
                                $model2->rate = $actual_item_rate;
                                $model2->batch = $batch;
                                //$model2->warehousestock_unit = $this->GetItemunitName($item_base_unit);
                                $model2->warehousestock_unit = $item_base_unit;
                                $model2->warehousestock_status = 1;
                                $model2->created_by = yii::app()->user->id;
                                $model2->created_date = date('Y-m-d');
                                $model2->company_id = Yii::app()->user->company_id;
                                $model2->updated_by = yii::app()->user->id;
                                $model2->updated_date = date('Y-m-d');
                                $model2->save(false);
                            }

                            $item_quantity = Yii::app()->db->createCommand("SELECT SUM(warehousereceipt_quantity) as quantity FROM {$this->tblpx}warehousereceipt_items WHERE warehousereceipt_id=" . $data['receipt_id'] . "")->queryScalar();
                            $model = Warehousereceipt::model()->findByPk($data['receipt_id']);
                            $model->warehousereceipt_quantity = $item_quantity;
                            $model->company_id = Yii::app()->user->company_id;
                            $model->save(false);
                            $last_id = $item_model->item_id;
                            $result = '';
                            $receipt_details = Yii::app()->db->createCommand(" SELECT * FROM {$this->tblpx}warehousereceipt_items  WHERE warehousereceipt_id=" . $data['receipt_id'] . "")->queryAll();
                            foreach ($receipt_details as $key => $values) {

                                $specification = Yii::app()->db->createCommand("SELECT id, cat_id,brand_id, specification, unit FROM {$this->tblpx}specification WHERE id=" . $values['warehousereceipt_itemid'] . "")->queryRow();
                                $parent_category = Yii::app()->db->createCommand("SELECT * FROM {$this->tblpx}purchase_category WHERE id='" . $specification['cat_id'] . "'")->queryRow();

                                if ($specification['brand_id'] != NULL) {
                                    $brand_details = Yii::app()->db->createCommand("SELECT brand_name FROM {$this->tblpx}brand WHERE id=" . $specification['brand_id'] . "")->queryRow();
                                    $brand = '-' . $brand_details['brand_name'];
                                } else {
                                    $brand = '';
                                }

                                $spc_details = $parent_category['category_name'] . $brand . ' - ' . $specification['specification'];
                                $spc_id = $specification['id'];


                                $result .= '<tr>';
                                $result .= '<td><div id="item_sl_no">' . ($key + 1) . '</div></td>';
                                $result .= '<td><div class="item_description" id="' . $spc_id . '">' . $spc_details . '</div> </td>';
                                $result .= '<td><div class="item_description" id="batch' . $values['item_id'] . '">' . $values['warehousereceipt_batch'] . '</div> </td>';
                                $result .= '<td> <div class="unit" id="unit' . $values['item_id'] . '"> ' . $values['warehousereceipt_unit'] . '</div> </td>';
                                $result .= '<td> <div class="" id="quantity' . $values['item_id'] . '"> ' . $values['warehousereceipt_quantity'] . '</div> </td>';
                                $result .= '<td> <div class="" id="rate' . $values['item_id'] . '"> ' . Controller::money_format_inr($values['warehousereceipt_rate'], 2, 1) . '</div> </td>';
                                //$result .= '<td> <div class="unit" id="jono' . $values['item_id'] . '"> ' . $values['warehousereceipt_jono'] . '</div> </td>';
                                $result .= '<td width="70"><span class="icon icon-options-vertical popover-test" data-toggle="popover"   data-placement="left" type="button" data-html="true" style="cursor: pointer;" ></span>'
                                    . '<div class ="popover-content hide"> '
                                    . '<ul class="tooltip-hiden">'
                                    . '<li><a href="#" id=' . $values['item_id'] . ' class="btn btn-default btn-xs removebtn">Delete</a></li> '
                                    . '<li><a href="" id=' . $values['item_id'] . ' class="btn btn-default btn-xs edit_item" style="margin-left:3px">Edit</a></li>';
                                $result .= '</ul>';
                                $result .= '</div> ';
                                $result .= '</td>';

                                $result .= '</tr>';
                            }

                            echo json_encode(array('response' => 'success', 'msg' => 'Receipt item saved successfully', 'html' => $result));
                        } else {
                            echo json_encode(array('response' => 'error', 'msg' => 'Some problem occured'));
                        }
                    } else {
                        echo json_encode(array('response' => 'error', 'msg' => 'Accepted Quantity must be less than the quantity entered'));
                    }
                } else {
                    echo json_encode(array('response' => 'error', 'msg' => 'The entered quantity must be less than ' . $item_available_for_receive . "" . $unit));
                }
            } else {
                echo json_encode(array('response' => 'error', 'msg' => 'The Item of ' . $item_available_for_receive . "" . $unit . ' is already received'));
            }
        }
    }

    public function actiongetUnit()
    {

        $html['unit'] = '';
        $html['status'] = '';
        $specification = Yii::app()->db->createCommand("SELECT id, parent_id, brand_id, specification, unit FROM {$this->tblpx}purchase_category WHERE id=" . $_REQUEST['data'] . "")->queryRow();
        //$unit = Yii::app()->db->createCommand("SELECT id, unit_name FROM {$this->tblpx }unit WHERE id=" . $specification['unit'] . "")->queryRow();
        if (!empty($specification['unit'])) {
            $html['unit'] .= $specification['unit'];
        } else {
            $html['unit'] .= '';
        }
        $html['status'] .= true;
        echo json_encode($html);
    }
    public function actionCheckEditBillDespatch()
    {
        $warehouseReceipt_checkSql = '';

        $data = isset($_REQUEST['data']) ? $_REQUEST['data'] : "";
        if ($data != "") {
            if (isset($data['bill_id']) && !empty($data['bill_id'])) {
                $bill_id = $data['bill_id'];
            } else {
                $bill_id = "";
            }
            if (isset($data['billitem']) && !empty($data['billitem'])) {
                $billitem = $data['billitem'];
            } else {
                $billitem = "";
            }
            if (isset($data['description']) && !empty($data['description'])) {
                $bill_item_id = $data['description'];
            } else {
                $bill_item_id = "";
            }
            if (isset($data['bibatch']) && !empty($data['bibatch'])) {
                $bibatch = trim($data['bibatch']);
            } else {
                $bibatch = "";
            }

            if (isset($data['despatch_id']) && !empty($data['despatch_id'])) {
                $dispatch_id = $data['despatch_id'];
            } else {
                $dispatch_id = "";
            }
            if (isset($data['dispatchitem']) && !empty($data['dispatchitem'])) {
                $dispatchitem = $data['dispatchitem'];
            } else {
                $dispatchitem = "";
            }
            if (isset($data['stock_item']) && !empty($data['stock_item'])) {
                $dispatch_item_id = $data['stock_item'];
            } else {
                $dispatch_item_id = "";
            }
            if (isset($data['batch']) && !empty($data['batch'])) {
                $dispatch_batch = trim($data['batch']);
            } else {
                $dispatch_batch = "";
            }
        } else {
            $bill_id = '';
            $bill_item_id = '';
            $bibatch = '';
            $dispatch_id = '';
            $dispatch_item_id = '';
            $dispatch_batch = '';
        }



        if ($dispatch_id != '' && $dispatch_item_id != '') {


            $receipt_type = $dispatchitem;
            $warehouseReceipt_checkSql = "SELECT wr.`warehousereceipt_id`,wr.`warehousereceipt_no`,wr.`warehousereceipt_purchasebill_project`,
                                        wr.`warehousereceipt_despatch_id`,wr.`warehousereceipt_bill_id`,wr.`warehousereceipt_warehouseid`,
                                        wri.item_id,wri.warehousereceipt_itemid,wri.warehousereceipt_batch,
                                        wri.warehousereceipt_baseunit_accepted_effective_quantity,wri.warehousereceipt_baseunit_quantity,
                                        wri.warehousereceipt_baseunit_rejected_quantity 
                                        FROM {$this->tblpx}warehousereceipt as wr 
                                        INNER JOIN {$this->tblpx}warehousereceipt_items as wri 
                                        ON wr.`warehousereceipt_id` = wri.warehousereceipt_id";

            if ($dispatch_batch != "") {
                $warehouseReceipt_checkSql .= " WHERE wr.`warehousereceipt_despatch_id` ='" . $dispatch_id . "' AND  wri.warehousereceipt_itemid ='" . $dispatch_item_id . "' AND  wri.warehousereceipt_batch ='" . $dispatch_batch . "' AND wr.delete_status = 1";
            } else {
                $warehouseReceipt_checkSql .= " WHERE wr.`warehousereceipt_despatch_id` ='" . $dispatch_id . "' AND  wri.warehousereceipt_itemid ='" . $dispatch_item_id . "' AND  (wri.warehousereceipt_batch ='" . $dispatch_batch . "' OR wri.warehousereceipt_batch IS NULL) AND wr.delete_status = 1";
            }
            if ($receipt_type != "") {
                $warehouseReceipt_checkSql .= " AND  wri.warehousereceipt_transfer_type_item_id ='" . $receipt_type . "'";
            } else {
                $warehouseReceipt_checkSql .= " AND  (wri.warehousereceipt_transfer_type_item_id ='" . $receipt_type . "' OR wri.warehousereceipt_transfer_type_item_id IS NULL)";
            }
        } else {

            if ($bill_id != '' && $bill_item_id != '') {

                $receipt_type = $billitem;
                $warehouseReceipt_checkSql = "SELECT wr.`warehousereceipt_id`,wr.`warehousereceipt_no`,wr.`warehousereceipt_purchasebill_project`,
                                    wr.`warehousereceipt_despatch_id`,wr.`warehousereceipt_bill_id`,wr.`warehousereceipt_warehouseid`,
                                    wri.item_id,wri.warehousereceipt_itemid,wri.warehousereceipt_batch,
                                    wri.warehousereceipt_baseunit_accepted_effective_quantity,wri.warehousereceipt_baseunit_quantity,
                                    wri.warehousereceipt_baseunit_rejected_quantity 
                                    FROM {$this->tblpx}warehousereceipt as wr 
                                    INNER JOIN {$this->tblpx}warehousereceipt_items as wri 
                                    ON wr.`warehousereceipt_id` = wri.warehousereceipt_id";


                if ($dispatch_batch != "") {

                    $warehouseReceipt_checkSql .= " WHERE wr.`warehousereceipt_bill_id` ='" . $bill_id . "' AND  wri.warehousereceipt_itemid ='" . $bill_item_id . "' AND  wri.warehousereceipt_batch ='" . $bibatch . "' AND wr.delete_status = 1";
                } else {

                    $warehouseReceipt_checkSql .= " WHERE wr.`warehousereceipt_bill_id` ='" . $bill_id . "' AND  wri.warehousereceipt_itemid ='" . $bill_item_id . "' AND  (wri.warehousereceipt_batch ='" . $bibatch . "' OR wri.warehousereceipt_batch IS NULL) AND wr.delete_status = 1";
                }
                if ($receipt_type != "") {

                    $warehouseReceipt_checkSql .= " AND  wri.warehousereceipt_transfer_type_item_id ='" . $receipt_type . "'";
                } else {

                    $warehouseReceipt_checkSql .= " AND  (wri.warehousereceipt_transfer_type_item_id ='" . $receipt_type . "' OR wri.warehousereceipt_transfer_type_item_id IS NULL)";
                }
            }
        }

        $warehouseReceipt_check = Yii::app()->db->createCommand($warehouseReceipt_checkSql)->queryRow();
        if ($warehouseReceipt_check != '') {
            echo json_encode(array('response' => 'error', 'msg' => 'This Item is not editable.Because it already transfered to warehouse'));
        } else {
            echo json_encode(array('response' => 'success'));
        }
    }
    public function actionGetItemsByPurchaseOrDespatch()
    {
        $purchase_bill_no = $_GET['purchase_bill_no'];
        $dispatch_no = $_GET['dispatch_no'];
        $receipt_id = $_GET['receipt_id'];
        $transfer_type = $_GET['transfer_type'];
        $client = "";
        $item_Data = "";
        $item_category = "";
        $items = "";
        $received_type = "";

        if ($purchase_bill_no != '') {

            $received_type = "bill";
            $bills = Bills::model()->findByPk($purchase_bill_no);
            $itemdatasql = "SELECT bi.*,pi.item_id,pi.purchase_id,pi.item_width,pi.item_height,pi.item_length,p.purchase_type
            FROM " . $this->tableNameAcc('billitem', 0) . " as bi
            LEFT JOIN " . $this->tableNameAcc('purchase_items', 0) . " as pi
            ON bi.purchaseitem_id = pi.item_id
            LEFT JOIN " . $this->tableNameAcc('purchase', 0) . " as p
            ON pi.purchase_id = p.p_id
            WHERE bi.bill_id = " . $purchase_bill_no . " ORDER BY bi.billitem_id";
            $item_Data = Yii::app()->db->createCommand($itemdatasql)->queryAll();
            $items = $this->renderPartial('warehousereceipt_items', array(
                'item_Data' => $item_Data, 'item_category' => $bills, 'received_type' => $received_type, 'receipt_id' => $receipt_id
            ));
        } else {

            if ($dispatch_no != '') {
                $received_type = "dispatch";
                $Warehousedespatch = Warehousedespatch::model()->findByPk($dispatch_no);
                $item_Data = Yii::app()->db->createCommand("SELECT * FROM " . $this->tableNameAcc('warehousedespatch_items', 0) . " WHERE warehousedespatch_id = " . $dispatch_no . " ORDER BY item_id")->queryAll();
                $items = $this->renderPartial('warehousereceipt_items', array(
                    'item_Data' => $item_Data, 'item_category' => $Warehousedespatch, 'received_type' => $received_type, 'receipt_id' => $receipt_id
                ));
            }
        }

        echo $items;
    }

    public function check_stock_exists($warehousereceipt_warehouseid, $stock_item, $rate, $batch, $dimension)
    {

        $stock_check_sql = "SELECT warehousestock_stock_quantity,warehousestock_stock_quantity,rate,warehousestock_id,warehousestock_initial_quantity FROM {$this->tblpx}warehousestock WHERE warehousestock_warehouseid=" . $warehousereceipt_warehouseid . " AND warehousestock_itemid=" . $stock_item . " AND rate = $rate  ";

        if ($batch == null) {
            $stock_check_sql .= " AND (batch IS Null OR batch ='" . $batch . "')";
        } else {
            $stock_check_sql .= "  AND batch = '" . $batch . "'";
        }
        if ($dimension == null) {
            $stock_check_sql .= " AND (dimension IS Null OR dimension ='" . $dimension . "')";
        } else {
            $stock_check_sql .= "  AND dimension LIKE '%" . $dimension . "%'";
        }


        $stock_check = Yii::app()->db->createCommand($stock_check_sql)->queryRow();

        return $stock_check;
    }

    public function usedqty_check($item_id, $transfer_type, $purchase_bill_no, $dispatch_no)
    {

        $transfer_query = "SELECT SUM(wri.warehousereceipt_baseunit_effective_quantity) as warehousestock_total_quantity_received,
                    SUM(wri.warehousereceipt_baseunit_accepted_effective_quantity) as warehousestock_total_accepted_quantity,
                    SUM(wri.warehousereceipt_baseunit_rejected_effective_quantity) as warehousestock_total_rejected_quantity,
                    wri.warehousereceipt_batch,wri.warehousereceipt_itemid,wr.warehousereceipt_transfer_type,wri.warehousereceipt_transfer_type_item_id
                    FROM " . $this->tblpx . "warehousereceipt_items as wri
                    INNER JOIN " . $this->tblpx . "warehousereceipt as wr 
                    ON wri.warehousereceipt_id = wr.warehousereceipt_id
                    WHERE wri.warehousereceipt_transfer_type_item_id='" . $item_id . "'";

        if ($transfer_type == 1) {
            $transfer_query .= "AND wr.warehousereceipt_bill_id='" . $purchase_bill_no . "'";
        } else {
            $transfer_query .= "AND wr.warehousereceipt_despatch_id='" . $dispatch_no . "'";
        }

        $usedqty = Yii::app()->db->createCommand($transfer_query)->queryRow();

        return $usedqty;
    }

    public function receipt_data($warehousereceipt_warehouseid, $stock_item, $rate, $batch, $dimension, $accepted_quantity_baseunit_quantity, $previous_accepted_qty)
    {

        $item_receipt_check_sql = "SELECT SUM(warehousereceipt_baseunit_accepted_effective_quantity)as quantity FROM {$this->tblpx}warehousereceipt_items WHERE warehousereceipt_warehouseid=" . $warehousereceipt_warehouseid . " AND warehousereceipt_itemid=" . $stock_item . " AND warehousereceipt_rate = $rate ";
        if ($batch == null) {
            $item_receipt_check_sql .= " AND (warehousereceipt_batch IS Null OR warehousereceipt_batch ='" . $batch . "')";
        } else {
            $item_receipt_check_sql .= "  AND warehousereceipt_batch='" . $batch . "'";
        }

        if ($dimension == null) {
            $item_receipt_check_sql .= " AND (dimension IS Null OR dimension ='" . $dimension . "')";
        } else {
            $item_receipt_check_sql .= "  AND dimension LIKE '%" . $dimension . "%'";
        }

        $item_receipt_check =  Yii::app()->db->createCommand($item_receipt_check_sql)->queryRow();


        if ($item_receipt_check['quantity'] == "") {
            $item_receipt_quantity_check = 0;
        } else {
            $item_receipt_quantity_check = ($item_receipt_check['quantity'] + $accepted_quantity_baseunit_quantity) - $previous_accepted_qty;
        }

        return $item_receipt_quantity_check;
    }

    public function despatch_data($warehousereceipt_warehouseid, $stock_item, $rate, $batch, $dimension)
    {

        $item_despatch_check_sql = "SELECT SUM(warehousedespatch_baseunit_quantity)as quantity FROM {$this->tblpx}warehousedespatch_items WHERE warehousedespatch_warehouseid=" . $warehousereceipt_warehouseid . " AND warehousedespatch_itemid=" . $stock_item . " AND warehousedespatch_rate = $rate ";
        if ($batch == null) {
            $item_despatch_check_sql .= " AND (warehousedespatch_batch IS Null OR warehousedespatch_batch ='" . $batch . "')";
        } else {
            $item_despatch_check_sql .= "  AND warehousedespatch_batch='" . $batch . "'";
        }

        if ($dimension == null) {
            $item_despatch_check_sql .= " AND (dimension IS Null OR dimension ='" . $dimension . "')";
        } else {
            $item_despatch_check_sql .= "  AND dimension LIKE '%" . $dimension . "%'";
        }

        $item_despatch_check = Yii::app()->db->createCommand($item_despatch_check_sql)->queryRow();

        if ($item_despatch_check['quantity'] == "") {
            $item_despatch_quantity_check = 0;
        } else {
            $item_despatch_quantity_check = $item_despatch_check['quantity'];
        }

        return $item_despatch_quantity_check;
    }


    public function getstock_data($warehousereceipt_warehouseid, $stock_item, $rate, $batch, $dimension)
    {
        $stock_sql = "SELECT warehousestock_stock_quantity,warehousestock_stock_quantity,rate,warehousestock_id,warehousestock_initial_quantity FROM {$this->tblpx}warehousestock WHERE warehousestock_warehouseid=" . $warehousereceipt_warehouseid . " AND warehousestock_itemid=" . $stock_item . " AND rate = $rate ";
        if ($batch == null) {
            $stock_sql .= " AND (batch IS Null OR batch ='" . $batch . "')";
        } else {
            $stock_sql .= "  AND batch = '" . $batch . "'";
        }
        if ($dimension == null) {
            $stock_sql .= " AND (dimension IS Null OR dimension ='" . $dimension . "')";
        } else {
            $stock_sql .= "  AND dimension LIKE '%" . $dimension . "%'";
        }

        $stock = Yii::app()->db->createCommand($stock_sql)->queryRow();

        return $stock;
    }

    public function getreceiptstock_data($warehousereceipt_warehouseid, $stock_item, $rate, $batch, $dimension)
    {

        $item_receipt_sql1 = "SELECT SUM(warehousereceipt_baseunit_accepted_effective_quantity)as quantity FROM {$this->tblpx}warehousereceipt_items WHERE warehousereceipt_warehouseid=" . $warehousereceipt_warehouseid . " AND warehousereceipt_itemid=" . $stock_item . " AND warehousereceipt_rate = $rate  ";

        if ($batch == null) {
            $item_receipt_sql1 .= " AND (warehousereceipt_batch IS Null OR warehousereceipt_batch ='" . $batch . "')";
        } else {
            $item_receipt_sql1 .= "  AND warehousereceipt_batch='" . $batch . "'";
        }
        if ($dimension == null) {
            $item_receipt_sql1 .= " AND (dimension IS Null OR dimension ='" . $dimension . "')";
        } else {
            $item_receipt_sql1 .= "  AND dimension LIKE '%" . $dimension . "%'";
        }

        $item_receipt = Yii::app()->db->createCommand($item_receipt_sql1)->queryRow();

        if ($item_receipt['quantity'] == "") {

            $item_receipt_quantity = 0;
        } else {

            $item_receipt_quantity = $item_receipt['quantity'];
        }
        return $item_receipt_quantity;
    }


    public function getdespatchstock_data($warehousereceipt_warehouseid, $stock_item, $rate, $batch, $dimension)
    {
        $item_despatch_sql = "SELECT SUM(warehousedespatch_baseunit_quantity)as quantity FROM {$this->tblpx}warehousedespatch_items WHERE warehousedespatch_warehouseid=" . $warehousereceipt_warehouseid . " AND warehousedespatch_itemid=" . $stock_item . " AND warehousedespatch_rate = $rate ";

        if ($batch == null) {
            $item_despatch_sql .= " AND (warehousedespatch_batch IS Null OR warehousedespatch_batch ='" . $batch . "')";
        } else {
            $item_despatch_sql .= "  AND warehousedespatch_batch='" . $batch . "'";
        }
        if ($dimension == null) {
            $item_despatch_sql .= " AND (dimension IS Null OR dimension ='" . $dimension . "')";
        } else {
            $item_despatch_sql .= "  AND dimension LIKE '%" . $dimension . "%'";
        }
        $item_despatch = Yii::app()->db->createCommand($item_despatch_sql)->queryRow();

        if ($item_despatch['quantity'] == "") {
            $item_despatch_quantity = 0;
        } else {
            $item_despatch_quantity = $item_despatch['quantity'];
        }
        return $item_despatch_quantity;
    }

    public function actionupdatereceiptitem()
    {

        $result['html'] = '';
        $warehouseStockId = '';
        $total_item_quantity = 0;
        $warehousereceipt_id = 0;
        $transfer_type = isset($_REQUEST['transfer_type']) ? $_REQUEST['transfer_type'] : "";
        $warehousereceipt_warehouseid = isset($_REQUEST['Warehousereceipt']['warehousereceipt_warehouseid']) ? $_REQUEST['Warehousereceipt']['warehousereceipt_warehouseid'] : "";
        $warehousereceipt_warehouseid_from = isset($_REQUEST['Warehousereceipt']['warehousereceipt_warehouseid_from']) ? $_REQUEST['Warehousereceipt']['warehousereceipt_warehouseid_from'] : NULL;
        $remaining_quantity = 0;
        $warehouse_receipt_item_length = null;
        $warehouse_receipt_item_id = isset($_REQUEST['warehouse_receipt_item_id']) ? $_REQUEST['warehouse_receipt_item_id'] : NULL;



        if ($_REQUEST['warehousereceipt_id'] == 0) {
            echo json_encode(array('response' => 'error', 'msg' => 'Please enter Warehouse receipt details'));
        } else {
            extract($_REQUEST);

            if (isset($sl_no)) {

                $item_id = $this->inputval('item_id');
                $stock_item = $this->inputval('stock_item');
                $transfer_type_item_id = $this->inputval('item_id');
                $rate = $this->inputval('rates', 0);
                $transferablequantity = $this->inputval('transferablequantity');
                $item_unit_id = $this->inputval('item_unit_id');
                $item_unit_name = $this->inputval('item_unit_id');
                $item_base_unit = $this->inputval('item_unit_base_unit');
                $batch = $this->inputval('batch');
                $receivedquantity = $this->inputval('receivedquantity');
                $accepted_quantity = $this->inputval('accepted_quantity');
                $rejected_quantity = $this->inputval('rejected_quantity');
                $remark_data = $this->inputval('remark_data');
                $jono = $this->inputval('jono');
                $unitconversiondata =  $this->unitConversionToBase($item_unit_id, $item_base_unit, $stock_item, $transferablequantity);
                $item_conversion_id = $unitconversiondata['item_conversion_id'];
                $itemqty = $this->inputval('itemqty');
                $itemunit = $this->inputval('itemunit');
                $itemrate = $this->inputval('itemrate');
                $dispatch_no = $this->inputval('dispatch_no');
                $purchase_bill_no = $this->inputval('purchase_bill_no');

                if ($transfer_type == 1) {
                    $transfer_type_name = "Billed ";
                    $billitem_purchase_type = $this->inputval('billitem_purchase_type', "");
                    $warehouse_receipt_item_length = $this->inputval('warehouse_receipt_item_length', "");
                    $warehouse_receipt_item_width = $this->inputval('warehouse_receipt_item_width', "");
                    $warehouse_receipt_item_height = $this->inputval('warehouse_receipt_item_height', "");

                    if ($billitem_purchase_type != '') {
                        if ($billitem_purchase_type == 'A') {
                            $dimension = null;
                            $item_dimension_category = 1;
                        } else {
                            if ($billitem_purchase_type == 'G') {
                                $dimension = $warehouse_receipt_item_width . " x " . $warehouse_receipt_item_height;
                                $item_dimension_category = 1;
                            } else {
                                $dimension = null;
                                $item_dimension_category = 1;
                            }
                        }
                    } else {
                        $dimension = null;
                    }
                } else {
                    $transfer_type_name = "Despatched ";
                    $purchase_type = $this->inputval('purchase_type', "");
                    $item_dimension = $this->inputval('item_dimension', "");
                    $item_dimension_category = $this->inputval('item_dimension_category', 1);
                    $dimension = $item_dimension;
                }

                if ($item_unit_id != $item_base_unit) {

                    $receivedquantity_baseunit_quantity = $receivedquantity;
                    $transferablequantity_baseunit_quantity = $transferablequantity;
                    $accepted_quantity_baseunit_quantity = $accepted_quantity;
                    $rejected_quantity_baseunit_quantity = $rejected_quantity;
                    $total_base_unit_quantity = $accepted_quantity_baseunit_quantity + $rejected_quantity_baseunit_quantity;
                    if ($transfer_type != 2) {
                        $base_rate = $rate;
                    } else {
                        $base_rate = $rate;
                    }
                } else {

                    $receivedquantity_baseunit_quantity = $receivedquantity;
                    $transferablequantity_baseunit_quantity = $transferablequantity;
                    $accepted_quantity_baseunit_quantity = $accepted_quantity;
                    $rejected_quantity_baseunit_quantity = $rejected_quantity;
                    $total_base_unit_quantity = $accepted_quantity_baseunit_quantity + $rejected_quantity_baseunit_quantity;
                    $item_receivedquantity_UnitConversion = $this->unitConversionToBase($item_unit_id, $item_base_unit, $stock_item, $receivedquantity);
                    $item_conversion_id = $item_receivedquantity_UnitConversion['item_conversion_id'];
                    $base_rate = $rate;
                }
                $warehousereceipt_accepted_effective_quantity = $accepted_quantity;
                $warehousereceipt_baseunit_accepted_effective_quantity = $accepted_quantity_baseunit_quantity;
                $warehousereceipt_effective_quantity = $receivedquantity;
                $warehousereceipt_baseunit_effective_quantity = $receivedquantity_baseunit_quantity;
                $warehousereceipt_rejected_effective_quantity = $rejected_quantity;
                $warehousereceipt_baseunit_rejected_effective_quantity = $rejected_quantity_baseunit_quantity;
                $transferable_baseunit_effective_quantity = $transferablequantity_baseunit_quantity;
                $total_base_unit_effective_quantity = $total_base_unit_quantity;



                if ($transfer_type != "" && $transfer_type_item_id != NULL && $item_id != NULL) {

                    $used_quantity_check = $this->usedqty_check($item_id, $transfer_type, $purchase_bill_no, $dispatch_no);
                    $used_quantitycheck = isset($used_quantity_check['warehousestock_total_quantity_received']) ? $used_quantity_check['warehousestock_total_quantity_received'] : 0;
                    $used_quantity = $used_quantitycheck + $warehousereceipt_baseunit_effective_quantity;
                } else {

                    $used_quantity = $warehousereceipt_baseunit_effective_quantity;
                }



                if ($warehouse_receipt_item_id != "" || $warehouse_receipt_item_id != 0) {
                    $receipt = Warehousereceipt::model()->findByPk($warehousereceipt_id);
                    $item_model = WarehousereceiptItems::model()->findByPk($warehouse_receipt_item_id);
                    $olditem = $item_model->warehousereceipt_itemid;

                    $previous_qty = $item_model->warehousereceipt_baseunit_effective_quantity;
                    $previous_accepted_qty = $item_model->warehousereceipt_baseunit_accepted_effective_quantity;
                    $used_quantity =  $used_quantity - $previous_qty;

                    if ($transfer_type != "" && $transfer_type_item_id != NULL && $item_id != NULL) {
                        $remaining_quantity = ($transferable_baseunit_effective_quantity - $used_quantitycheck) + $previous_qty;
                    } else {
                        $remaining_quantity = $transferable_baseunit_effective_quantity + $previous_qty;
                    }





                    if ($transferable_baseunit_effective_quantity < $used_quantity) {
                        echo json_encode(array('response' => 'error', 'msg' => 'The ' . $transfer_type_name . " quantity is already received"));
                    } else {
                        if ($transferable_baseunit_effective_quantity < $warehousereceipt_baseunit_effective_quantity) {
                            echo json_encode(array('response' => 'error', 'msg' => 'The entered quantity must be less than ' . $transferablequantity_baseunit_quantity . "" . $item_unit_name));
                        } else {
                            if ($warehousereceipt_baseunit_effective_quantity < $total_base_unit_effective_quantity) {
                                echo json_encode(array('response' => 'error', 'msg' => 'Accepted Quantity AND Rejected Quantity must be less than the Received quantity entered'));
                            } else {


                                $is_stock_exists = $this->check_stock_exists($warehousereceipt_warehouseid, $stock_item, $rate, $batch, $dimension);

                                if (!empty($is_stock_exists)) {
                                    $item_receipt_quantity_check = $this->receipt_data($warehousereceipt_warehouseid, $stock_item, $rate, $batch, $dimension, $accepted_quantity_baseunit_quantity, $previous_accepted_qty);

                                    $item_despatch_quantity_check = $this->despatch_data($warehousereceipt_warehouseid, $stock_item, $rate, $batch, $dimension);
                                } else {
                                    $item_receipt_quantity_check = 0;
                                    $item_despatch_quantity_check = 0;
                                }

                                if ($item_receipt_quantity_check < $item_despatch_quantity_check) {
                                    echo json_encode(array('response' => 'error', 'msg' => 'This Item is already despatched.'));
                                } else {

                                    $old_rate =  WarehousereceiptItems::model()->findByAttributes(array('item_id' => $warehouse_receipt_item_id, 'warehousereceipt_id' => $warehousereceipt_id, 'warehousereceipt_itemid' => $stock_item));
                                    $olderate_val = $old_rate['warehousereceipt_rate'];
                                    $old_qty = $old_rate['warehousereceipt_quantity'];

                                    if ($olderate_val != '') {
                                        $item_receipt_quantity_old = $this->getreceiptstock_data($warehousereceipt_warehouseid, $stock_item, $olderate_val, $batch, $dimension);

                                        $item_despatch_quantity_old = $this->getdespatchstock_data($warehousereceipt_warehouseid, $stock_item, $olderate_val, $batch, $dimension);
                                    }

                                    $old_rate_item =  WarehousereceiptItems::model()->findByAttributes(array('item_id' => $warehouse_receipt_item_id, 'warehousereceipt_id' => $warehousereceipt_id, 'warehousereceipt_itemid' => $olditem));
                                    $olderate_valitem = $old_rate_item['warehousereceipt_rate'];
                                    $old_qtyitem = $old_rate_item['warehousereceipt_quantity'];

                                    if ($olderate_valitem != '') {
                                        $item_receipt_quantity_olditem = $this->getreceiptstock_data($warehousereceipt_warehouseid, $olditem, $olderate_valitem, $batch, $dimension);

                                        $item_despatch_quantity_olditem = $this->getdespatchstock_data($warehousereceipt_warehouseid, $olditem, $olderate_valitem, $batch, $dimension);
                                    }

                                    $item_model->warehousereceipt_id = $warehousereceipt_id;
                                    $item_model->warehousereceipt_quantity = $receivedquantity;
                                    $item_model->dimension = $dimension;
                                    $item_model->length = $warehouse_receipt_item_length;
                                    $item_model->warehousereceipt_accepted_quantity = $accepted_quantity;
                                    $item_model->warehousereceipt_accepted_effective_quantity = $warehousereceipt_accepted_effective_quantity;
                                    $item_model->warehousereceipt_rejected_quantity = $rejected_quantity;
                                    $item_model->warehousereceipt_baseunit_accepted_quantity = $accepted_quantity_baseunit_quantity;
                                    $item_model->warehousereceipt_baseunit_accepted_effective_quantity = $warehousereceipt_baseunit_accepted_effective_quantity;
                                    $item_model->warehousereceipt_baseunit_quantity = $receivedquantity_baseunit_quantity;
                                    $item_model->warehousereceipt_baseunit_rejected_quantity = $rejected_quantity_baseunit_quantity;
                                    $item_model->warehousereceipt_unitConversion_id = $item_conversion_id;
                                    $item_model->warehousereceipt_effective_quantity = $warehousereceipt_effective_quantity;
                                    $item_model->warehousereceipt_baseunit_effective_quantity = $warehousereceipt_baseunit_effective_quantity;
                                    $item_model->warehousereceipt_rejected_effective_quantity = $warehousereceipt_rejected_effective_quantity;
                                    $item_model->warehousereceipt_baseunit_rejected_effective_quantity = $warehousereceipt_baseunit_rejected_effective_quantity;
                                    $item_model->warehousereceipt_rate = $rate;
                                    $item_model->warehousereceipt_transfer_type_item_id = $item_id;
                                    $item_model->warehousereceipt_unit = $item_unit_name;
                                    $item_model->warehousereceipt_batch = $batch;
                                    $item_model->remark_data = $remark_data;
                                    $item_model->warehousereceipt_jono = $jono;
                                    $item_model->warehousereceipt_warehouseid = $warehousereceipt_warehouseid;
                                    $item_model->warehousereceipt_itemid = $stock_item;
                                    $item_model->warehousereceipt_itemunit = $itemunit;
                                    $item_model->warehousereceipt_itemqty = $itemqty;
                                    $item_model->warehousereceipt_itemrate = $itemrate;
                                    $item_model->created_by = Yii::app()->user->id;
                                    $item_model->created_date = date('Y-m-d H:i:s');
                                    $item_model->updated_by = Yii::app()->user->id;
                                    $item_model->updated_date = date('Y-m-d H:i:s');



                                    $oldreceiptdata = WarehousereceiptItems::model()->findByPk($warehouse_receipt_item_id);

                                    $transaction = Yii::app()->db->beginTransaction();
                                    try {
                                        if ($item_model->save()) {

                                            $newdata = array(
                                                'item_id'  => $warehouse_receipt_item_id,
                                                'warehousereceipt_id' => $warehousereceipt_id,
                                                'warehousereceipt_itemid' => $stock_item,
                                                'warehousereceipt_quantity' => $receivedquantity,
                                                'warehousereceipt_rate' => $rate,
                                                'warehousereceipt_unit' => $item_unit_name,
                                                'warehousereceipt_unitConversion_id' => $item_conversion_id,
                                                'warehousereceipt_batch' => $batch,
                                                'dimension' => $dimension,
                                                'length' => $warehouse_receipt_item_length
                                            );

                                            $module = 'warehousereceipt';
                                            $recid = $warehouse_receipt_item_id;
                                            $actionlog = 'update';
                                            $olddata = array(
                                                'item_id'  => $oldreceiptdata['item_id'],
                                                'warehousereceipt_id' => $oldreceiptdata['warehousereceipt_id'],
                                                'warehousereceipt_itemid' => $oldreceiptdata['warehousereceipt_itemid'],
                                                'warehousereceipt_quantity' => $oldreceiptdata['warehousereceipt_quantity'],
                                                'warehousereceipt_rate' => $oldreceiptdata['warehousereceipt_rate'],
                                                'warehousereceipt_unit' => $oldreceiptdata['warehousereceipt_unit'],
                                                'warehousereceipt_batch' => $oldreceiptdata['warehousereceipt_batch']
                                            );

                                            $log_storing_response = $this->Logcreate($newdata, $olddata, $module, $recid, $actionlog);

                                            if ($log_storing_response['error'] === true) {
                                                throw new Exception('An error occured');
                                            }

                                            if ($olderate_val != '') {
                                                $rateval =  $olderate_val;
                                            } else {
                                                $rateval =  $rate;
                                            }

                                            $stock = $this->getstock_data($warehousereceipt_warehouseid, $stock_item, $rateval, $batch, $dimension);

                                            if (!empty($stock)) {

                                                $item_receipt_quantity = $this->getreceiptstock_data($warehousereceipt_warehouseid, $stock_item, $rate, $batch, $dimension);

                                                $item_despatch_quantity = $this->getdespatchstock_data($warehousereceipt_warehouseid, $stock_item, $rate, $batch, $dimension);

                                                if ($olderate_val != $rate) {
                                                    $final_quantity = ($stock['warehousestock_initial_quantity'] + $item_receipt_quantity_old - $old_qty) - $item_despatch_quantity_old;
                                                    $updaterate = $olderate_val;
                                                } else {
                                                    $final_quantity = ($stock['warehousestock_initial_quantity'] + $item_receipt_quantity) - $item_despatch_quantity;
                                                    $updaterate = $base_rate;
                                                }

                                                $model2 = Warehousestock::model()->findByPk($stock['warehousestock_id']);
                                                $model2->warehousestock_stock_quantity = $final_quantity;
                                                $model2->dimension = $dimension;
                                                $model2->rate = $updaterate;
                                                $model2->updated_date = date('Y-m-d H:i:s');
                                                if ($model2->save(false)) {
                                                } else {

                                                    throw new Exception('An error occured in updating');
                                                }

                                                if ($olderate_val != $rate) {


                                                    $item_dimension_category = Warehousestock::model()->countByAttributes(array(
                                                        'warehousestock_warehouseid' => $warehousereceipt_warehouseid,
                                                        'warehousestock_itemid' => $stock_item,
                                                        'batch' => $batch,
                                                        'rate' => $rate,
                                                    ));
                                                    $item_dimension_category_count = $item_dimension_category + 1;
                                                    $model2 = new Warehousestock;
                                                    $model2->warehousestock_warehouseid = $warehousereceipt_warehouseid;
                                                    $model2->warehousestock_date = date('Y-m-d');
                                                    $model2->warehousestock_itemid = $stock_item;
                                                    $model2->warehousestock_itemid_dimension_category = $item_dimension_category_count;
                                                    $model2->dimension = $dimension;
                                                    $model2->warehousestock_stock_quantity = $warehousereceipt_baseunit_accepted_effective_quantity;
                                                    $model2->rate = $base_rate;
                                                    $model2->batch = $batch;
                                                    $model2->warehousestock_unit = $item_base_unit;
                                                    $model2->warehousestock_status = 1;
                                                    $model2->created_by = yii::app()->user->id;
                                                    $model2->created_date = date('Y-m-d H:i:s');
                                                    $model2->company_id = Yii::app()->user->company_id;
                                                    $model2->updated_by = yii::app()->user->id;
                                                    $model2->updated_date = date('Y-m-d H:i:s');
                                                    if ($model2->save(false)) {
                                                    } else {

                                                        throw new Exception('An error occured in updating');
                                                    }
                                                }
                                            } else {

                                                if ($olderate_valitem != '') {
                                                    $rateval =  $olderate_valitem;
                                                } else {
                                                    $rateval =  $rate;
                                                }

                                                $stock = $this->getstock_data($warehousereceipt_warehouseid, $olditem, $rateval, $batch, $dimension);

                                                if (!empty($stock)) {

                                                    $final_quantity = ($stock['warehousestock_initial_quantity'] + $item_receipt_quantity_olditem - $old_qtyitem) - $item_despatch_quantity_olditem;
                                                    $updaterate = $olderate_valitem;

                                                    $model2 = Warehousestock::model()->findByPk($stock['warehousestock_id']);
                                                    $model2->warehousestock_stock_quantity = $final_quantity;
                                                    $model2->dimension = $dimension;
                                                    $model2->rate = $updaterate;
                                                    $model2->updated_date = date('Y-m-d H:i:s');
                                                    if ($model2->save(false)) {
                                                    } else {

                                                        throw new Exception('An error occured in updating');
                                                    }
                                                }

                                                $item_dimension_category = Warehousestock::model()->countByAttributes(array(
                                                    'warehousestock_warehouseid' => $warehousereceipt_warehouseid,
                                                    'warehousestock_itemid' => $stock_item,
                                                    'batch' => $batch,
                                                    'rate' => $rate,
                                                ));
                                                $item_dimension_category_count = $item_dimension_category + 1;
                                                $model2 = new Warehousestock;
                                                $model2->warehousestock_warehouseid = $warehousereceipt_warehouseid;
                                                $model2->warehousestock_date = date('Y-m-d');
                                                $model2->warehousestock_itemid = $stock_item;
                                                $model2->warehousestock_itemid_dimension_category = $item_dimension_category_count;
                                                $model2->dimension = $dimension;
                                                $model2->warehousestock_stock_quantity = $warehousereceipt_baseunit_accepted_effective_quantity;
                                                $model2->rate = $base_rate;
                                                $model2->batch = $batch;
                                                $model2->warehousestock_unit = $item_base_unit;
                                                $model2->warehousestock_status = 1;
                                                $model2->created_by = yii::app()->user->id;
                                                $model2->created_date = date('Y-m-d H:i:s');
                                                $model2->company_id = Yii::app()->user->company_id;
                                                $model2->updated_by = yii::app()->user->id;
                                                $model2->updated_date = date('Y-m-d H:i:s');
                                                if ($model2->save(false)) {
                                                } else {

                                                    throw new Exception('An error occured in updating');
                                                }
                                            }

                                            $item_quantity = Yii::app()->db->createCommand("SELECT SUM(warehousereceipt_baseunit_quantity) as quantity FROM {$this->tblpx}warehousereceipt_items WHERE warehousereceipt_id=" . $warehousereceipt_id . "")->queryScalar();

                                            $model = Warehousereceipt::model()->findByPk($warehousereceipt_id);
                                            $model->warehousereceipt_quantity = $item_quantity;
                                            $model->company_id = Yii::app()->user->company_id;
                                            if ($model->save(false)) {
                                            } else {

                                                throw new Exception('An error occured in updating');
                                            }
                                            $last_id = $item_model->item_id;
                                            $result = '';
                                            $result = '';


                                            if ($transfer_type != "" && $transfer_type_item_id == NULL && $item_id == NULL) {
                                                $receipt_details = Yii::app()->db->createCommand(" SELECT * FROM {$this->tblpx}warehousereceipt_items  WHERE warehousereceipt_id=" . $warehousereceipt_id . " AND warehousereceipt_transfer_type_item_id IS NULL")->queryAll();
                                                foreach ($receipt_details as $key => $values) {

                                                    $specification = Yii::app()->db->createCommand("SELECT id, cat_id,brand_id, specification, unit FROM {$this->tblpx}specification WHERE id=" . $values['warehousereceipt_itemid'] . "")->queryRow();
                                                    $parent_category = Yii::app()->db->createCommand("SELECT * FROM {$this->tblpx}purchase_category WHERE id='" . $specification['cat_id'] . "'")->queryRow();

                                                    if ($specification['brand_id'] != NULL) {
                                                        $brand_details = Yii::app()->db->createCommand("SELECT brand_name FROM {$this->tblpx}brand WHERE id=" . $specification['brand_id'] . "")->queryRow();
                                                        $brand = '-' . $brand_details['brand_name'];
                                                    } else {
                                                        $brand = '';
                                                    }

                                                    $spc_details = $parent_category['category_name'] . $brand . ' - ' . $specification['specification'];
                                                    $spc_id = $specification['id'];
                                                    $warehousereceipt_unit = $values['warehousereceipt_unit'];
                                                    $warehousereceipt_itemunit = $values['warehousereceipt_itemunit'];

                                                    if ($warehousereceipt_unit != $warehousereceipt_itemunit) {
                                                        if ($values['warehousereceipt_itemqty'] > 0) {
                                                            $quantity_data = $values['warehousereceipt_itemqty'];
                                                        } else {
                                                            $quantity_data = $values['warehousereceipt_quantity'];
                                                        }

                                                        if ($values['warehousereceipt_itemrate'] > 0) {
                                                            $rate_data = $values['warehousereceipt_itemrate'];
                                                        } else {
                                                            $rate_data = $values['warehousereceipt_rate'];
                                                        }

                                                        if ($values['warehousereceipt_itemunit'] != '') {
                                                            $unit_data = $values['warehousereceipt_itemunit'];
                                                        } else {
                                                            $unit_data = $values['warehousereceipt_unit'];
                                                        }
                                                        $quantity_base_data = $values['warehousereceipt_quantity'];
                                                        $rate_base_data = $values['warehousereceipt_rate'];
                                                        $unit_base_data = $values['warehousereceipt_unit'];
                                                        $additional_amount = $quantity_data * $rate_data;
                                                    } else {

                                                        $quantity_data = $values['warehousereceipt_quantity'];
                                                        $rate_data = $values['warehousereceipt_rate'];
                                                        $unit_data =  $values['warehousereceipt_unit'];
                                                        $unit_base_data = $values['warehousereceipt_unit'];
                                                        $quantity_base_data = $values['warehousereceipt_quantity'];
                                                        $rate_base_data = $values['warehousereceipt_rate'];
                                                        $additional_amount = $values['warehousereceipt_quantity'] * $values['warehousereceipt_rate'];
                                                    }


                                                    $result .= '<tr>';
                                                    $result .= '<td><div id="item_sl_no">' . ($key + 1) . '</div></td>';
                                                    $result .= '<td><div class="item_description" id="' . $spc_id . '">' . $spc_details . '</div> </td>';
                                                    $result .= '<td><div class="item_description" id="batch' . $values['item_id'] . '">' .  $values['warehousereceipt_batch']  . '</div> </td>';
                                                    $result .= '<td> <div class="unit" id="unit' . $values['item_id'] . '"> ' . $unit_data . '</div> </td>';
                                                    $result .= '<td> <div class="" id="quantity' . $values['item_id'] . '"> ' . $quantity_data . '</div> </td>';
                                                    $result .= '<td> <div class="" id="rate' . $values['item_id'] . '"> ' . Controller::money_format_inr($rate_data, 2, 1) . '</div> </td>';
                                                    // $result .= '<td> <div class="unit" id="jono' . $values['item_id'] . '"> ' . $values['warehousereceipt_jono'] . '</div> </td>';
                                                    $result .= '<td> <div class="item_qty_base" id="item_qty_base' . $values['item_id'] . '"> ' . $quantity_base_data . '</div> </td>';
                                                    $result .= '<td> <div class="item_unit_base" id="item_unit_base' . $values['item_id'] . '"> ' .  $unit_base_data . '</div> </td>';
                                                    $result .= '<td> <div class="item_rate_base" id="item_rate_base' . $values['item_id'] . '"> ' . $rate_base_data  . '</div> </td>';
                                                    $result .= '<td> <div class="unit" id="amount' . $values['item_id'] . '"> ' . Controller::money_format_inr($additional_amount, 2, 1) . '</div> </td>';
                                                    $result .= '<td width="70"><span class="icon icon-options-vertical popover-test" data-toggle="popover"   data-placement="left" type="button" data-html="true" style="cursor: pointer;" ></span>'
                                                        . '<div class ="popover-content hide"> '
                                                        . '<ul class="tooltip-hiden">'
                                                        . '<li><a href="#" id=' . $values['item_id'] . ' class="btn btn-default btn-xs removebtn">Delete</a></li> '
                                                        . '<li><a href="" id=' . $values['item_id'] . ' class="btn btn-default btn-xs edit_item" style="margin-left:3px">Edit</a></li>';
                                                    $result .= '</ul>';
                                                    $result .= '</div> ';
                                                    $result .= '</td>';

                                                    $result .= '</tr>';
                                                }
                                            } else {
                                                $result = '';
                                            }

                                            echo json_encode(array('response' => 'success', 'msg' => 'Receipt item Updated successfully', 'html' => $result, 'warehouse_receipt_item_id' => $warehouse_receipt_item_id, 'remaining_quantity' => $remaining_quantity));
                                        } else {
                                            throw new Exception('An error occured in updating');
                                            echo json_encode(array('response' => 'error', 'msg' => 'Some problem occured'));
                                        }

                                        $transaction->commit();
                                    } catch (Exception $error) {
                                        $transaction->rollBack();
                                        $result = array('status' => '0', 'msg' => $error->getMessage());

                                        echo json_encode(array('response' => 'error', 'msg' => $error->getMessage()));
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                $sl_no = '';
                $stock_item = '';
                $transferablequantity = '';
                $transfer_type_item_id = '';
                $rate = '';
                $transferablequantity = '';
                $item_unit_id = '';
                $item_unit_base_unit = '';
                $batch = NULL;
                $receivedquantity = '';
                $accepted_quantity = '';
                $rejected_quantity = '';
                $jono = '';
            }
        }
    }



    public function actionremoveitem()
    {
        $data = $_REQUEST['data'];

        $item_detail = Yii::app()->db->createCommand(" SELECT * FROM {$this->tblpx}warehousereceipt_items  WHERE item_id=" . $data['item_id'] . "")->queryRow();
        $item_id = $item_detail['warehousereceipt_itemid'];
        $warehouseid = $item_detail['warehousereceipt_warehouseid'];
        $batch = $item_detail['warehousereceipt_batch'];
        $rate = $item_detail['warehousereceipt_rate'];


        $del = Yii::app()->db->createCommand()->delete($this->tblpx . 'warehousereceipt_items', 'item_id=:id', array(':id' => $data['item_id']));
        $transaction = Yii::app()->db->beginTransaction();
        try {
            if ($del) {

                $newdata = array(
                    'item_id'  =>  $item_id,
                    'warehousereceipt_id' => $item_detail['warehousereceipt_id'],
                    'warehousereceipt_itemid' => $item_detail['warehousereceipt_itemid'],
                    'warehousereceipt_quantity' => $item_detail['warehousereceipt_quantity'],
                    'warehousereceipt_rate' => $rate,
                    'warehousereceipt_unit' => $item_detail['warehousereceipt_unit'],
                    'warehousereceipt_unitConversion_id' => $item_detail['warehousereceipt_unitConversion_id'],
                    'warehousereceipt_batch' => $batch,
                    'dimension' => $item_detail['dimension'],
                    'length' => $item_detail['length']
                );

                $module = 'warehousereceipt';
                $recid = $item_id;
                $actionlog = 'delete';
                $olddata = array();
                $logdata = $this->Logcreate($newdata, $olddata, $module, $recid, $actionlog);

                if ($batch == null) {
                    $stock = Yii::app()->db->createCommand("SELECT warehousestock_stock_quantity,warehousestock_stock_quantity,rate,warehousestock_id,warehousestock_initial_quantity FROM {$this->tblpx}warehousestock WHERE warehousestock_warehouseid=" . $warehouseid . " AND warehousestock_itemid=" . $item_id . " AND (batch IS Null OR batch ='" . $batch . "') AND rate = $rate ")->queryRow();
                } else {
                    $stock = Yii::app()->db->createCommand("SELECT warehousestock_stock_quantity,rate,warehousestock_id,warehousestock_initial_quantity FROM {$this->tblpx}warehousestock WHERE warehousestock_warehouseid=" . $warehouseid . " AND warehousestock_itemid=" . $item_id . " AND batch = '" . $batch . "' AND rate = $rate")->queryRow();
                }
                if (!empty($stock)) {

                    if ($batch == null) {
                        $item_receipt = Yii::app()->db->createCommand("SELECT SUM(warehousereceipt_baseunit_accepted_effective_quantity)as quantity FROM {$this->tblpx}warehousereceipt_items WHERE warehousereceipt_warehouseid=" . $warehouseid . " AND warehousereceipt_itemid=" . $item_id . " AND (warehousereceipt_batch IS Null OR warehousereceipt_batch ='" . $batch . "') AND warehousereceipt_rate = $rate ")->queryRow();
                        $item_despatch = Yii::app()->db->createCommand("SELECT SUM(warehousedespatch_baseunit_quantity)as quantity FROM {$this->tblpx}warehousedespatch_items WHERE warehousedespatch_warehouseid=" . $warehouseid . " AND warehousedespatch_itemid=" . $item_id . " AND (warehousedespatch_batch IS Null OR warehousedespatch_batch ='" . $batch . "') AND warehousedespatch_rate = $rate ")->queryRow();
                    } else {
                        $item_receipt = Yii::app()->db->createCommand("SELECT SUM(warehousereceipt_baseunit_accepted_effective_quantity)as quantity FROM {$this->tblpx}warehousereceipt_items WHERE warehousereceipt_warehouseid=" . $warehouseid . " AND warehousereceipt_itemid=" . $item_id . " AND warehousereceipt_batch='" . $batch . "' AND warehousereceipt_rate = $rate ")->queryRow();
                        $item_despatch = Yii::app()->db->createCommand("SELECT SUM(warehousedespatch_baseunit_quantity)as quantity FROM {$this->tblpx}warehousedespatch_items WHERE warehousedespatch_warehouseid=" . $warehouseid . " AND warehousedespatch_itemid=" . $item_id . " AND warehousedespatch_batch='" . $batch . "' AND warehousedespatch_rate = $rate ")->queryRow();
                    }
                    if ($item_receipt['quantity'] == "") {
                        $item_receipt_quantity = 0;
                    } else {
                        $item_receipt_quantity = $item_receipt['quantity'];
                    }
                    if ($item_despatch['quantity'] == "") {
                        $item_despatch_quantity = 0;
                    } else {
                        $item_despatch_quantity = $item_despatch['quantity'];
                    }

                    $final_quantity = ($stock['warehousestock_initial_quantity'] + $item_receipt['quantity']) - $item_despatch['quantity'];
                    $model2 = Warehousestock::model()->findByPk($stock['warehousestock_id']);
                    $model2->warehousestock_stock_quantity = $final_quantity;
                    if ($model2->save(false)) {
                        $warehousestock_check = Warehousestock::model()->findByPk($stock['warehousestock_id']);
                        if ($warehousestock_check->warehousestock_stock_quantity == 0) {
                            $del = Yii::app()->db->createCommand()->delete($this->tblpx . 'warehousestock', 'warehousestock_id=:warehousestock_id', array(':warehousestock_id' => $stock['warehousestock_id']));
                        }
                    } else {
                        throw new Exception('An error occured in delete');
                    }
                }


                $result = '';
                $receipt_details = Yii::app()->db->createCommand(" SELECT * FROM {$this->tblpx}warehousereceipt_items  WHERE warehousereceipt_id=" . $data['receipt_id'] . " AND warehousereceipt_transfer_type_item_id IS NULL")->queryAll();
                foreach ($receipt_details as $key => $values) {

                    $specification = Yii::app()->db->createCommand("SELECT id, cat_id,brand_id, specification, unit FROM {$this->tblpx}specification WHERE id=" . $values['warehousereceipt_itemid'] . "")->queryRow();
                    $parent_category = Yii::app()->db->createCommand("SELECT * FROM {$this->tblpx}purchase_category WHERE id='" . $specification['cat_id'] . "'")->queryRow();

                    if ($specification['brand_id'] != NULL) {
                        $brand_details = Yii::app()->db->createCommand("SELECT brand_name FROM {$this->tblpx}brand WHERE id=" . $specification['brand_id'] . "")->queryRow();
                        $brand = '-' . $brand_details['brand_name'];
                    } else {
                        $brand = '';
                    }

                    $spc_details = $parent_category['category_name'] . $brand . ' - ' . $specification['specification'];
                    $spc_id = $specification['id'];

                    $warehousereceipt_unit = $values['warehousereceipt_unit'];
                    $warehousereceipt_itemunit = $values['warehousereceipt_itemunit'];

                    if ($warehousereceipt_unit != $warehousereceipt_itemunit) {

                        $quantity_base_data = $values['warehousereceipt_quantity'];
                        $rate_base_data = $values['warehousereceipt_rate'];
                        $unit_base_data = $values['warehousereceipt_itemunit'];
                        $quantity_data = $values['warehousereceipt_itemqty'];
                        $rate_data = $values['warehousereceipt_itemrate'];
                        $unit_data = $values['warehousereceipt_unit'];
                        $additional_amount = $values['warehousereceipt_itemqty'] * $values['warehousereceipt_itemrate'];
                    } else {

                        $quantity_base_data = 0;
                        $rate_base_data = 0;
                        $unit_base_data = $values['warehousereceipt_itemunit'];
                        $quantity_data = $values['warehousereceipt_itemqty'];
                        $rate_data = $values['warehousereceipt_itemrate'];
                        $unit_data =  $values['warehousereceipt_itemunit'];
                        $additional_amount = $values['warehousereceipt_itemqty'] * $values['warehousereceipt_itemrate'];
                    }


                    $result .= '<tr>';
                    $result .= '<td><div id="item_sl_no">' . ($key + 1) . '</div></td>';
                    $result .= '<td><div class="item_description" id="' . $spc_id . '">' . $spc_details . '</div> </td>';
                    $result .= '<td><div class="item_description" id="batch' . $values['item_id'] . '">' . $values['warehousereceipt_batch'] . '</div> </td>';
                    $result .= '<td> <div class="unit" id="unit' . $values['item_id'] . '"> ' . $unit_data . '</div> </td>';
                    $result .= '<td> <div class="" id="quantity' . $values['item_id'] . '"> ' . $quantity_data . '</div> </td>';
                    $result .= '<td> <div class="" id="rate' . $values['item_id'] . '"> ' . Controller::money_format_inr($rate_data, 2, 1) . '</div> </td>';
                    // $result .= '<td> <div class="unit" id="jono' . $values['item_id'] . '"> ' . $values['warehousereceipt_jono'] . '</div> </td>';
                    $result .= '<td> <div class="item_qty_base" id="item_qty_base' . $values['item_id'] . '"> ' . $quantity_base_data  . '</div> </td>';
                    $result .= '<td> <div class="item_unit_base" id="item_unit_base' . $values['item_id'] . '"> ' . $unit_base_data . '</div> </td>';
                    $result .= '<td> <div class="item_rate_base" id="item_rate_base' . $values['item_id'] . '"> ' . $rate_base_data . '</div> </td>';
                    $result .= '<td> <div class="unit" id="amount' . $values['item_id'] . '"> ' . Controller::money_format_inr($additional_amount, 2, 1) . '</div> </td>';

                    $result .= '<td width="70"><span class="icon icon-options-vertical popover-test" data-toggle="popover"   data-placement="left" type="button" data-html="true" style="cursor: pointer;" ></span>'
                        . '<div class ="popover-content hide"> '
                        . '<ul class="tooltip-hiden">'
                        . '<li><a href="#" id=' . $values['item_id'] . ' class="btn btn-default btn-xs removebtn">Delete</a></li> '
                        . '<li><a href="" id=' . $values['item_id'] . ' class="btn btn-default btn-xs edit_item" style="margin-left:3px">Edit</a></li>';
                    $result .= '</ul>';
                    $result .= '</div> ';
                    $result .= '</td>';

                    $result .= '</tr>';
                }
                echo json_encode(array('response' => 'success', 'msg' => 'Item deleted successfully', 'html' => $result));
            } else {
                throw new Exception('An error occured in delete');

                echo json_encode(array('response' => 'error', 'msg' => 'Some problem occured'));
            }
            $transaction->commit();
        } catch (Exception $error) {
            $transaction->rollBack();
            echo json_encode(array('response' => 'error', 'msg' => $error->getMessage()));
        }
    }
    public function actionremoveitem111()
    {
        $data = $_REQUEST['data'];

        $item_detail = Yii::app()->db->createCommand(" SELECT * FROM {$this->tblpx}warehousereceipt_items  WHERE item_id=" . $data['item_id'] . "")->queryRow();
        $item_id = $item_detail['warehousereceipt_itemid'];
        $warehouseid = $item_detail['warehousereceipt_warehouseid'];
        $del = Yii::app()->db->createCommand()->delete($this->tblpx . 'warehousereceipt_items', 'item_id=:id', array(':id' => $data['item_id']));
        if ($del) {

            $stock = Yii::app()->db->createCommand("SELECT warehousestock_stock_quantity,warehousestock_id,warehousestock_initial_quantity FROM {$this->tblpx}warehousestock WHERE warehousestock_warehouseid=" . $warehouseid . " AND warehousestock_itemid=" . $item_id . "")->queryRow();
            if (!empty($stock)) {
                //$item_quantity = Yii::app()->db->createCommand("SELECT SUM(warehousedespatch_quantity)as quantity FROM {$this->tblpx }warehousereceipt_items WHERE warehousedespatch_warehouseid=" .$warehouseid. " AND warehousedespatch_itemid=".$item_id."")->queryRow();
                //$previous_quantity = $stock['warehousestock_initial_quantity']- $item_quantity['quantity'];
                //$final_quantity = $previous_quantity;
                if ($batch == null) {
                    $item_receipt = Yii::app()->db->createCommand("SELECT SUM(warehousereceipt_baseunit_accepted_effective_quantity)as quantity FROM {$this->tblpx}warehousereceipt_items WHERE warehousereceipt_warehouseid=" . $receipt->warehousereceipt_warehouseid . " AND warehousereceipt_itemid=" . $stock_item . " AND (warehousereceipt_batch IS Null OR warehousereceipt_batch ='" . $batch . "') AND receipt_type='" . $receipt_type . "'")->queryRow();
                    $item_despatch = Yii::app()->db->createCommand("SELECT SUM(warehousedespatch_baseunit_quantity)as quantity FROM {$this->tblpx}warehousedespatch_items WHERE warehousedespatch_warehouseid=" . $receipt->warehousereceipt_warehouseid . " AND warehousedespatch_itemid=" . $stock_item . " AND (warehousedespatch_batch IS Null OR warehousedespatch_batch ='" . $batch . "')")->queryRow();
                } else {
                    $item_receipt = Yii::app()->db->createCommand("SELECT SUM(warehousereceipt_baseunit_accepted_effective_quantity)as quantity FROM {$this->tblpx}warehousereceipt_items WHERE warehousereceipt_warehouseid=" . $receipt->warehousereceipt_warehouseid . " AND warehousereceipt_itemid=" . $stock_item . " AND warehousereceipt_batch='" . $batch . "' AND receipt_type='" . $receipt_type . "'")->queryRow();
                    $item_despatch = Yii::app()->db->createCommand("SELECT SUM(warehousedespatch_baseunit_quantity)as quantity FROM {$this->tblpx}warehousedespatch_items WHERE warehousedespatch_warehouseid=" . $receipt->warehousereceipt_warehouseid . " AND warehousedespatch_itemid=" . $stock_item . " AND warehousedespatch_batch='" . $batch . "'")->queryRow();
                }
                if ($item_receipt['quantity'] == "") {
                    $item_receipt_quantity = 0;
                } else {
                    $item_receipt_quantity = $item_receipt['quantity'];
                }
                if ($item_despatch['quantity'] == "") {
                    $item_despatch_quantity = 0;
                } else {
                    $item_despatch_quantity = $item_despatch['quantity'];
                }

                $final_quantity = ($stock['warehousestock_initial_quantity'] + $item_receipt['quantity']) - $item_despatch['quantity'];
                $model2 = Warehousestock::model()->findByPk($stock['warehousestock_id']);
                $model2->warehousestock_stock_quantity = $final_quantity;
                $model2->save(false);
            }


            $result = '';
            $receipt_details = Yii::app()->db->createCommand(" SELECT * FROM {$this->tblpx}warehousereceipt_items  WHERE warehousereceipt_id=" . $data['receipt_id'] . "")->queryAll();
            foreach ($receipt_details as $key => $values) {

                $specification = Yii::app()->db->createCommand("SELECT id, cat_id,brand_id, specification, unit FROM {$this->tblpx}specification WHERE id=" . $values['warehousereceipt_itemid'] . "")->queryRow();
                $parent_category = Yii::app()->db->createCommand("SELECT * FROM {$this->tblpx}purchase_category WHERE id='" . $specification['cat_id'] . "'")->queryRow();

                if ($specification['brand_id'] != NULL) {
                    $brand_details = Yii::app()->db->createCommand("SELECT brand_name FROM {$this->tblpx}brand WHERE id=" . $specification['brand_id'] . "")->queryRow();
                    $brand = '-' . $brand_details['brand_name'];
                } else {
                    $brand = '';
                }

                $spc_details = $parent_category['category_name'] . $brand . ' - ' . $specification['specification'];
                $spc_id = $specification['id'];

                $result .= '<tr>';
                $result .= '<td><div id="item_sl_no">' . ($key + 1) . '</div></td>';
                $result .= '<td><div class="item_description" id="' . $spc_id . '">' . $spc_details . '</div> </td>';
                $result .= '<td><div class="item_description" id="batch' . $values['item_id'] . '">' . $values['warehousereceipt_batch'] . '</div> </td>';
                $result .= '<td> <div class="unit" id="unit' . $values['item_id'] . '"> ' . $values['warehousereceipt_unit'] . '</div> </td>';
                $result .= '<td> <div class="" id="quantity' . $values['item_id'] . '"> ' . $values['warehousereceipt_quantity'] . '</div> </td>';
                $result .= '<td> <div class="" id="rate' . $values['item_id'] . '"> ' . Controller::money_format_inr($values['warehousereceipt_rate'], 2, 1) . '</div> </td>';
                // $result .= '<td> <div class="unit" id="jono' . $values['item_id'] . '"> ' . $values['warehousereceipt_jono'] . '</div> </td>';
                $result .= '<td width="70"><span class="icon icon-options-vertical popover-test" data-toggle="popover"   data-placement="left" type="button" data-html="true" style="cursor: pointer;" ></span>'
                    . '<div class ="popover-content hide"> '
                    . '<ul class="tooltip-hiden">'
                    . '<li><a href="#" id=' . $values['item_id'] . ' class="btn btn-default btn-xs removebtn">Delete</a></li> '
                    . '<li><a href="" id=' . $values['item_id'] . ' class="btn btn-default btn-xs edit_item" style="margin-left:3px">Edit</a></li>';
                $result .= '</ul>';
                $result .= '</div> ';
                $result .= '</td>';

                $result .= '</tr>';
            }
            echo json_encode(array('response' => 'success', 'msg' => 'Item deleted successfully', 'html' => $result));
        } else {
            echo json_encode(array('response' => 'error', 'msg' => 'Some problem occured'));
        }
    }



    public function actiongetitem()
    {
        $warehouse = $_POST['id'];

        $specification = $this->actionGetItemCategory2($parent = 0, $spacing = '', $user_tree_array = '', $warehouse);
        $html['html'] = '';
        $html['status'] = '';
        if (!empty($specification)) {
            $html['html'] .= '<option value="">Select Item</option>';
            foreach ($specification as $key => $value) {
                $html['html'] .= '<option value="' . $value['id'] . '">' . $value['data'] . '</option>';
                $html['status'] = 'success';
            }
        } else {
            $html['status'] = 'no_success';
            $html['html'] .= '<option value="">Select Item</option>';
        }
        echo json_encode($html);
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Warehousereceipt the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Warehousereceipt::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Warehousereceipt $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'warehousereceipt-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
    public function actiongetAllUnits()
    {
        $units    = Unit::model()->findAll();
        $unitOptions = "<option value=''>-Select Unit-</option>";
        foreach ($units as $uData) {
            $unitOptions  .= "<option value='" . $uData["id"] . "'>" . $uData["unit_name"] . "</option>";
        }
        echo $unitOptions;
    }

    public function actiongetUnitconversionFactor()
    {
        if (isset($_POST)) {
            extract($_POST);
            $conversion_value = 0;
            $model = UnitConversion::model()->findByAttributes(array('base_unit' => $base_unit, 'conversion_unit' => $purchase_unit, 'item_id' => $item_id));

            $conversion_value = $model['conversion_factor'];

            echo $conversion_value;
        }
    }

    public function getArchWarehouseReceiptNo(){
        $sql = "SELECT count(*)  FROM `jp_warehousereceipt` ";  
        $counter = Yii::app()->db->createCommand($sql)->queryScalar();
        $futureDate=date('y', strtotime('+1 year'));
        if($counter == 0){
            $reno = str_pad($counter+1,3,"0",STR_PAD_LEFT);
            $prNo = date('y')."/ARCH/GRN-".$reno;
        }else{
            $sql = "SELECT warehousereceipt_no "
                . " FROM `jp_warehousereceipt` "
                . " ORDER BY warehousereceipt_id "
                . " DESC LIMIT 1";
            $last_po_no = Yii::app()->db->createCommand($sql)->queryScalar();
            
			$poArray = explode('-',$last_po_no);
			$next_po = str_pad($poArray[1]+1,3,"0",STR_PAD_LEFT);
            $prNo = date('y')."/ARCH/GRN-".$next_po;
			
        }

        return $prNo;

    }
    public function actionGetDynamicReceipt(){
        $wrno='';
        $status=0;
         if(isset($_POST['purchase_bill_no'])){
            $purchase_bill_no=$_POST['purchase_bill_no'];
            
            $tblpx = Yii::app()->db->tablePrefix;
            $bills = Bills::model()->findByPk($purchase_bill_no);
            $company_id=$bills->company_id;
            $company = Company::model()->findByPk($company_id);
            $org_prefix_slash='';
            if(!empty($company->warehouse_receipt_format )){
                $warehouse_receipt_format =trim($company["warehouse_receipt_format"]);
                $general_wr_format_arr = explode('/',$warehouse_receipt_format);
                $status = 1;
                $org_prefix = $general_wr_format_arr[0];
                $org_prefix_slash = $general_wr_format_arr[0].'/';
                $prefix_key = 0;
                $wr_key= '';
                $current_key ='';
                foreach($general_wr_format_arr as $key =>$value){
                    
                    if($value == '{grn_sequence}'){
                        $wr_key = $key;
                    }else if ($value == '{year}'){
                        $current_key =$key;
                    }
                }
                
                $current_year = date('Y');
                $previous_year = date('Y') -1;
                $next_year = date('Y') +1;
                
                $where = '';
                if($current_key == 2){
                    $where .= " AND `warehousereceipt_no` LIKE '%$current_year'";
                }else if($current_key == 1){
                    $where .= " AND `warehousereceipt_no` LIKE '%$current_year%'";
                }
                $sql = "SELECT * from ".$tblpx."warehousereceipt  WHERE `warehousereceipt_no` LIKE '$org_prefix_slash%' $where ORDER BY `warehousereceipt_id` DESC LIMIT 1;" ;
                //echo "<pre>";print_r($sql);exit;
                $previous_wr_no='';
                $wr_seq_no = 1;
                $wr_seq_prefix ='GRN-' ;
                $previous_data = Yii::app()->db->createCommand($sql)->queryRow();
                if( !empty($previous_data) ){
                    $previous_wr_no = $previous_data['warehousereceipt_no'];
                    $prefix_arr = explode('/', $previous_wr_no);
                    $prefix = $prefix_arr[0];
                    $previous_prefix_seq_no = $prefix_arr[$wr_key];
                    $wr_split = explode('-',$previous_prefix_seq_no);
                    $previous_prefix_seq_no_val =$wr_split[1];
                    $wr_seq_no =  $previous_prefix_seq_no_val + 1;
                                   
                }
                $digit = strlen((string) $wr_seq_no);
               
                if ($digit == 1) {
                    $wr_no =$wr_seq_prefix.'00' .$wr_seq_no;
                } else if ($digit == 2) {
                    $wr_no = $wr_seq_prefix.'0' . $wr_seq_no;
                } else {
                    $wr_no =$wr_seq_prefix.$wr_seq_no;
                }
                    $status=1;
              
               
                $new_invoice = str_replace('{year}',$current_year,$warehouse_receipt_format);
                $new_wr_no = str_replace('{grn_sequence}',$wr_no,$new_invoice);
                //die($new_wr_no);
                $wrno = $new_wr_no;
                
            } 
            
            

            $result = array( "status"=>$status,"receipt_no"=>$wrno);
            echo json_encode( $result);

         }
         
           
    }   
    public function actionGetDynamicReceiptBasedOnDispatch(){
        $wrno='';
        $status=0;
        $warehouse_from='';
        $org_prefix_slash='';
         if(isset($_POST['dispatch_no'])){
            $dispatch_no=$_POST['dispatch_no'];
            if(isset($_POST['warehouse_from'])){
                $warehouse_from = $_POST['warehouse_from'];
            }
            
            $tblpx = Yii::app()->db->tablePrefix;
            $warehousemodel = Warehouse::model()->findByPk($warehouse_from);
            if(!empty($warehousemodel)){
                $status = 1;
                $company_id=$warehousemodel->company_id;
                $company = Company::model()->findByPk($company_id);
                if(!empty($company->warehouse_receipt_format )){
                    $warehouse_receipt_format =trim($company["warehouse_receipt_format"]);
                    $general_wr_format_arr = explode('/',$warehouse_receipt_format);
                    // print_r($general_wr_format_arr);exit;
                    $org_prefix = $general_wr_format_arr[0];
                    $org_prefix_slash=$general_wr_format_arr[0].'/';
                    $prefix_key = 0;
                    $wr_key= '';
                    $current_key ='';
                    foreach($general_wr_format_arr as $key =>$value){
                        
                        if($value == '{grn_sequence}'){
                            $wr_key = $key;
                        }else if ($value == '{year}'){
                            $current_key =$key;
                        }
                    }
                    
                    $current_year = date('Y');
                    $previous_year = date('Y') -1;
                    $next_year = date('Y') +1;
                    
                    $where = '';
                    if($current_key == 2){
                        $where .= " AND `warehousereceipt_no` LIKE '%$current_year'";
                    }else if($current_key == 1){
                        $where .= " AND `warehousereceipt_no` LIKE '%$current_year%'";
                    }
                    $sql = "SELECT * from ".$tblpx."warehousereceipt  WHERE `warehousereceipt_no` LIKE '$org_prefix_slash%' $where ORDER BY `warehousereceipt_id` DESC LIMIT 1;" ;
                    //echo "<pre>";print_r($sql);exit;
                    $previous_wr_no='';
                    $wr_seq_no = 1;
                    $wr_seq_prefix ='GRN-' ;
                    $previous_data = Yii::app()->db->createCommand($sql)->queryRow();
                    if( !empty($previous_data) ){
                        $previous_wr_no = $previous_data['warehousereceipt_no'];
                        $prefix_arr = explode('/', $previous_wr_no);
                        $prefix = $prefix_arr[0];
                        $previous_prefix_seq_no = $prefix_arr[$wr_key];
                        $wr_split = explode('-',$previous_prefix_seq_no);
                        $previous_prefix_seq_no_val =$wr_split[1];
                        $wr_seq_no =  $previous_prefix_seq_no_val + 1;
                                       
                    }
                    $digit = strlen((string) $wr_seq_no);
                   
                    if ($digit == 1) {
                        $wr_no =$wr_seq_prefix.'00' .$wr_seq_no;
                    } else if ($digit == 2) {
                        $wr_no = $wr_seq_prefix.'0' . $wr_seq_no;
                    } else {
                        $wr_no =$wr_seq_prefix.$wr_seq_no;
                    }
                        $status=1;
                  
                   
                    $new_invoice = str_replace('{year}',$current_year,$warehouse_receipt_format);
                    $new_wr_no = str_replace('{grn_sequence}',$wr_no,$new_invoice);
                    //die($new_wr_no);
                    $wrno = $new_wr_no;
                    
                } 
                
            }
                                    
            $result = array( "status"=>$status,"receipt_no"=>$wrno);
            echo json_encode( $result);

         }
         
           
    }   


}
