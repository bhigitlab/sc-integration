<?php

/**
 * This is the model class for table "expense_type".
 *
 * The followings are the available columns in table 'expense_type':
 * @property integer $type_id
 * @property string $type_name
 */
class ExpenseType extends CActiveRecord
{

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{expense_type}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('type_name, expense_type,expense_category', 'required'),
            array('expense_category', 'required', 'message' => 'Please select an expense category.', 'on' => 'create'),  // Apply required only for creation
            array('type_name', 'filter', 'filter' => 'trim'),
            array('type_name', 'length', 'max' => 30),
            array('type_name', 'unique', 'message' => '{attribute} already exists.'),
            array('template_type,labour_label, wage_label, wage_rate_label, helper_label, helper_labour_label,lump_sum_label,labour_status,wage_status,wagerate_status,helper_status,helperlabour_status,lump_sum_status,labour_wage_label,helper_wage_label,helper_wage_status,labour_wage_status,expense_category', 'safe'),
           // array('template_type', 'checkTemplateDatas'),
            //array('type_name', 'checkexist'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('type_id, type_name, expense_type,labour_label, wage_label, wage_rate_label, helper_label, helper_labour_label,lump_sum_label,labour_status,wage_status,wagerate_status,helper_status,helperlabour_status,lump_sum_status,helper_wage_label,labour_wage_label,helper_wage_status,labour_wage_status', 'safe', 'on' => 'search'),
        );
    }

    public function checkTemplateDatas()
    {
        if ($this->template_type == 1) {
            if (empty($this->labour_label)) {
                $this->addError("labour_label", "Cannot be blank");
            }
            if (empty($this->wage_label)) {
                $this->addError("wage_label", "Cannot be blank");
            }
            if (empty($this->wage_rate_label)) {
                $this->addError("wage_rate_label", "Cannot be blank");
            }
        } elseif ($this->template_type == 2) {
            if (empty($this->labour_label)) {
                $this->addError("labour_label", "Cannot be blank");
            }

            if (empty($this->wage_rate_label)) {
                $this->addError("wage_rate_label", "Cannot be blank");
            }
            if (empty($this->helper_label)) {
                $this->addError("helper_label", "Cannot be blank");
            }
            if (empty($this->helper_labour_label)) {
                $this->addError("helper_labour_label", "Cannot be blank");
            }
            if (empty($this->labour_wage_label)) {
                $this->addError("labour_wage_label", "Cannot be blank");
            }
            if (empty($this->helper_wage_label)) {
                $this->addError("helper_wage_label", "Cannot be blank");
            }
        } elseif ($this->template_type == 3) {
            if (empty($this->labour_label)) {
                $this->addError("labour_label", "Cannot be blank");
            }

            if (empty($this->wage_rate_label)) {
                $this->addError("wage_rate_label", "Cannot be blank");
            }
            if (empty($this->helper_label)) {
                $this->addError("helper_label", "Cannot be blank");
            }
            if (empty($this->wage_label)) {
                $this->addError("wage_label", "Cannot be blank");
            }
        } elseif ($this->template_type == 4) {
            if (empty($this->lump_sum_label)) {
                $this->addError("lump_sum_label", "Cannot be blank");
            }
        }
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'status' => array(self::BELONGS_TO, 'Status', 'expense_type'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'type_id' => 'Type',
            'type_name' => 'Type Name',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('type_id', $this->type_id);
        $criteria->compare('type_name', $this->type_name, true);
        $criteria->compare('expense_type', $this->expense_type, true);

        $criteria->compare('labour_label', $this->labour_label, true);
        $criteria->compare('wage_label', $this->wage_label, true);
        $criteria->compare('wage_rate_label', $this->wage_rate_label, true);
        $criteria->compare('helper_label', $this->helper_label, true);
        $criteria->compare('helper_labour_label', $this->helper_labour_label, true);
        $criteria->compare('lump_sum_label', $this->lump_sum_label, true);
        $criteria->compare('labour_status', $this->labour_status, true);
        $criteria->compare('wage_status', $this->wage_status, true);
        $criteria->compare('wagerate_status', $this->wagerate_status, true);
        $criteria->compare('helper_status', $this->helper_status, true);
        $criteria->compare('helperlabour_status', $this->helperlabour_status, true);
        $criteria->compare('lump_sum_status', $this->lump_sum_status, true);
        $criteria->compare('labour_wage_label', $this->labour_wage_label, true);
        $criteria->compare('helper_wage_label', $this->helper_wage_label, true);
        $criteria->compare('labour_wage_status', $this->labour_wage_status, true);
        $criteria->compare('helper_wage_status', $this->helper_wage_status, true);

        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        $newQuery = "";
        foreach ($arrVal as $arr) {
            if ($newQuery) $newQuery .= ' OR';
            $newQuery .= " FIND_IN_SET('" . $arr . "', company_id)";
        }
        $criteria->addCondition($newQuery);
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => array('defaultOrder' => 'type_name ASC',),
            'pagination' => false,
        ));
    }

    public function checkexist($attribute, $params)
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $id = ExpenseType::model()->findByAttributes(array('type_id' => $this->type_id));
        $strVal = "1,2,3";
        $arrVal = explode(',', $strVal);
        $sql = "";
        foreach ($arrVal as $arr) {
            $sql .= "select * from jp_expense_type where type_name='" . $this->type_name . "' and  find_in_set('" . $arr . "',company_id) <> 0 UNION ";
        }
        $newQuery = mb_substr($sql, 0, -7);



        if (!empty($id)) {
            $name = ExpenseType::model()->findByAttributes(array('type_name' => $this->type_name), array('condition' => 'type_id != ' . $this->type_id . ' AND CONCAT( ",", `company_id` , "," ) REGEXP ",(' . $company_id . '),"'));
            if (!empty($name)) {
                $this->addError("type_name", ("Expense head already existsggg"));
            }
        } else {
            $name = Yii::app()->db->createCommand($newQuery)->queryAll();
            if (!empty($name)) {
                $this->addError("type_name", ("Expense Head already exists"));
            }
        }
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return ExpenseType the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
