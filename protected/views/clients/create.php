<?php
/* @var $this ClientsController */
/* @var $model Clients */

$this->breadcrumbs = array(
    'Clients' => array('index'),
    'Create',
);

/*$this->menu=array(
    array('label'=>'List Clients', 'url'=>array('index')),
    array('label'=>'Manage Clients', 'url'=>array('admin')),
);*/
?>
<div class="entries-wrapper">
    <div class="row">
        <div class="col-xs-12">
            <div class="heading-title">Add Client</div>
            <div class="dotted-line"></div>
        </div>
    </div>
    <?php echo $this->renderPartial('_form', array('model' => $model)); ?>
</div>
