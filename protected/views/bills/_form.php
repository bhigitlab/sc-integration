<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.min.js"></script>

<style type="text/css">
    table.total-table {
        font-size: 14px;
    }

    table.total-table div {
        text-align: right;
    }

    table.table .form-control {
        padding: 1px 1px;
        font-size: inherit;
        min-width: 70px;
    }

    table.table tr.pitems td div {
        padding-top: 8px;
    }

    table.table tr.pitems td div .fa {
        color: #060;
    }

    .formError .formErrorArrow div {
        display: none !important;
    }

    .formErrorContent {
        background-color: #333333 !important;
        border: 1px solid #ddd !important;
    }

    .bold {
        font-weight: bold;
    }



    *:focus {
        border: 1px solid #333;
        box-shadow: 0 0 6px 1px rgba(0, 0, 0, 0.25);
    }

    .addRow,
    .table-responsive {
        margin-bottom: 10px;
    }


    .addRow input.textbox {
        width: 200px;
        display: inline-block;
        margin-right: 10px;
    }

    .delbil,
    .deleteitem {

        color: #000;
        background-color: #eee;
        padding: 3px;
        cursor: pointer;
    }

    .addRow label {
        display: inline-block;
    }

    .extracharge .select2 {
        width: 100% !important;
    }

    .base {
        display: none;
    }
</style>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/jquery.validationEngine.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/languages/jquery.validationEngine-en.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/validationEngine.jquery.min.css" />
<?php if (Yii::app()->user->hasFlash('success')) : ?><br>
    <div class="example1consolesucces">
        <?php echo Yii::app()->user->getFlash('success'); ?>
    </div>.select();
<?php endif; ?>
<div class="form">
    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'bills-form',
        'enableAjaxValidation' => false,
    ));
    $general_settings_for_auto_receipt = Controller::generalSettingsForAutoReceiptToProjectWarehouse($settings = 1);

    ?>
    <div class="">
        <div class="" id="alert"></div>
    </div>
    <div class="clearfix">
        <div class="box_holder">
            <div class="row ">
                <div class="col-sm-4 col-lg-3 margin-top-5">
                    <?php echo $form->labelEx($model, 'company_id'); ?>
                    <?php
                    $user = Users::model()->findByPk(Yii::app()->user->id);
                    $arrVal = explode(',', $user->company_id);
                    $newQuery = "";
                    foreach ($arrVal as $arr) {
                        if ($newQuery) $newQuery .= ' OR';
                        $newQuery .= " FIND_IN_SET('" . $arr . "', id)";
                    }
                    echo $form->dropDownList($model, 'company_id', CHtml::listData(Company::model()->findAll(array(
                        'select' => array('id, name'),
                        "condition" => '(' . $newQuery . ')',
                        'order' => 'id',
                        'distinct' => true
                    )), 'id', 'name'), array('class' => 'form-control js-example-basic-single validate[required]', 'empty' => '-Select Company-'));
                    ?>
                    <?php echo $form->error($model, 'company_id'); ?>
                </div>
                <div class="col-sm-4 col-lg-3 margin-top-5">                    
                    <?php                    
                    $command = Yii::app()->db->createCommand();
                    $command->select('p.p_id,p.purchase_no, 
                        (CASE e.purchase_type  
                        WHEN 3 THEN "PARTIALLY BILLED" 
                        WHEN 2 THEN "FULLY BILLED" 
                        ELSE "NOT BILLED"
                        END) As purchase_type');
                    $command->from($this->tableNameAcc('purchase', 0) . ' as p');
                    $command->leftJoin($this->tableNameAcc('bills', 0) . ' as b', 
                            'p.p_id=b.purchase_id ');
                    $command->leftJoin($this->tableNameAcc('expenses', 0) . ' as e', 
                            'e.bill_id=b.bill_id ');
                    $command->where('p.purchase_status = "saved" AND '
                            . 'p.purchase_no IS NOT NULL');
                    $command->order('p.p_id');
                    
                    $purchase = $command->queryAll();
                    $data = CHtml::listData($purchase, 'p_id', 'purchase_no' 
                            ,'purchase_type');
                    
                    echo $form->labelEx($model, 'purchase_id');                
                    echo $form->dropDownList($model,'purchase_id',$data, 
                            array('class' => 'form-control js-example-basic-single '
                                . 'validate[required] purchasechange',
                             'empty' => '-Select Purchase-'
                            )
                        ); 
                    ?>
                    <?php echo $form->error($model, 'purchase_id'); ?>
                </div>
                <div class="col-sm-4 col-lg-3 margin-top-5">
                    <?php echo $form->labelEx($model, 'bill_number'); ?>
                    <?php echo $form->textField($model, 'bill_number', array('class' => 'form-control validate[required]', 'readonly' => !$model->isNewRecord)); ?>
                    <?php echo $form->error($model, 'bill_number'); ?>
                </div>
                <div class="col-sm-4 col-lg-3 margin-top-5">
                    <?php echo $form->labelEx($model, 'bill_date'); ?>
                    <?php echo CHtml::activeTextField($model, 'bill_date', array('readonly' => 'true', "value" => (($model->isNewRecord) ? date('d-M-Y') : date('d-M-Y', strtotime($model->bill_date))), 'size' => 10, 'class' => 'form-control')); ?>
                    <?php
                    $this->widget('application.extensions.calendar.SCalendar', array(
                        'inputField' => 'Bills_bill_date',
                        'ifFormat' => '%d-%b-%Y',
                    ));
                    ?>
                    <?php echo $form->error($model, 'bill_date'); ?>
                </div>

                <div class="col-sm-4 col-lg-3 margin-top-5 warehouse_list">
                    <label for="warehousestock_warehouseid" class="required">Warehouse <span class="required">*</span></label>
                    <?php

                    $data = CHtml::listData(Controller::getCategoryOptions(), 'id', 'text', 'group');
                    ?>

                    <?php
                    echo CHtml::activeDropDownList($model2, 'warehousestock_warehouseid', $data, array(
                        'empty' => '-Choose a Warehouse-',
                        'class' => 'form-control mandatory js-example-basic-single'
                    ));
                    ?>

                    <div class="errorMessage" id="Warehousestock_warehousestock_warehouseid_em_" style='display:none;'></div>

                </div>
            </div>
        </div>
        <div id="client" class="mt"><?php echo ((!$model->isNewRecord) ? $client : '') ?></div>
        <div class="extracharge" style="display:none;">
            <h4>Additional Charges <span class="icon icon-plus addcharge"></span></h4>
            <span class="errorcharge" style="color: red;"></span>
            <div class="chargeadd box_holder" style="display:none;">
                <div class="row">
                    <div class="col-md-2">
                        <input type="text" placeholder="Charge Category" name="packagecharge" id="labelcat" class="form-control textbox" />
                    </div>
                    <div class="col-md-2">
                        <input type="number" placeholder="Enter Amount" name="packagecharge" id="currentval" class="form-control textbox currentval" />
                    </div>
                    <div class="col-md-2">
                        <input type="button" class="btn btn-sm btn-primary savecharge" value="save" />
                    </div>
                </div>
            </div>
            <div id="addtional_list"></div>
        </div>
        <div class="extracharge" style="display:none;">
            <?php echo $this->renderPartial('_additional_item_form', array()); ?>
        </div>
        <div class="row">
            <div class="new_items"></div>
        </div>
        <div class="data-amnts box_holder">
            <div>
                <label class="inline">Amount:</label>
                <span id="Bills_bill_amount"> <?php echo $model->bill_amount ? Yii::app()->Controller->money_format_inr($model->bill_amount, 2) : 0; ?></span>
            </div>
            <div>
                <label class="inline">Discount Amount:</label>
                <span id="Bills_bill_discountamount"> <?php echo $model->bill_discountamount ? Yii::app()->Controller->money_format_inr($model->bill_discountamount, 2) : 0; ?></span>
            </div>
            <div>
                <label class="inline">Tax Amount:</label>
                <span id="Bills_bill_taxamount"> <?php echo $model->bill_taxamount ? Yii::app()->Controller->money_format_inr($model->bill_taxamount, 2) : 0; ?></span>
            </div>
            <div class="additional_charge">
                <label class="inline">Additional charges :</label>
                <span id="totaladditional_charge"><?= (!empty($addcharges)) ? $addcharges['amount'] : 0; ?></span>
            </div>
            <div>
                <label class="inline">Round Off:</label>
                <input type="text" name="Bills[round_off]" id="Bills_round_off" value="<?php echo $model->round_off ? $model->round_off : 0; ?>" style="width: 100px; border-radius: 05px;" class="text-right">
            </div>
            <div>
                <label class="inline">Total Amount:</label>
                <input type="text" name="Bills[bill_totalamount]" id="Bills_bill_totalamount" value="<?php echo $model->bill_totalamount ? Yii::app()->Controller->money_format_inr($model->bill_totalamount, 2) : 0; ?>" style="border:none;width:100px;" class="text-right" readonly=true>
            </div>
        </div>
        <input type="hidden" name="billid" id="billid" value="" />
        <div class="row addRow">
            <div class="col-sm-12 text-center"><?php echo CHtml::Button($model->isNewRecord ? 'Create' : 'Save', array('class' => 'buttonsubmit btn btn-primary', 'id' => 'buttonsubmit')); ?></div>
        </div>
        <?php $this->endWidget(); ?>
        <!-- form -->

        <script>
            <?php $addUrl = Yii::app()->createAbsoluteUrl("bills/addItemsForBills"); ?>
            <?php $valUrl = Yii::app()->createAbsoluteUrl("bills/validateBillNumber"); ?>
            <?php $reloadUrl = Yii::app()->createAbsoluteUrl("bills/updateBillsOnReload"); ?>
            <?php $addbillurl = Yii::app()->createAbsoluteUrl("bills/additionalbill"); ?>
            <?php $delbillurl = Yii::app()->createAbsoluteUrl("bills/deletebill"); ?>

            $(".js-example-basic-single").select2();
            $(".tax_slab").select2();


            $(document).ready(function() {
                $('select').first().focus();
                $('#Bills_round_off').change(function() {
                    var roundOff = parseFloat(this.value);
                    var amount = $("#Bills_bill_amount").text().replace(/,/g, '');
                    amount = parseFloat(amount);
                    var disc_amount = parseFloat($("#Bills_bill_discountamount").text());
                    var tax_amount = $("#Bills_bill_taxamount").text().replace(/,/g, '');
                    tax_amount = parseFloat(tax_amount);
                    var additional_charge = $("#totaladditional_charge").text().replace(/,/g, '');
                    additional_charge = parseFloat(additional_charge);
                    disc_amount = isNaN(disc_amount) ? 0 : disc_amount;
                    tax_amount = isNaN(tax_amount) ? 0 : tax_amount;
                    additional_charge = isNaN(additional_charge) ? 0 : additional_charge;
                    var total_amount = (amount + tax_amount + (roundOff) + additional_charge) - disc_amount;
                    $("#Bills_bill_totalamount").val(parseFloat(total_amount).toFixed(2));
                });
            });

            $(document).ready(function() {
                $(document).ajaxComplete(function() {
                    $(".tax_slab").select2();
                });
                $("#bills-form").validationEngine({
                    'custom_error_messages': {
                        'custom[number]': {
                            'message': 'Invalid number'
                        }
                    }
                });
                $(".purchasechange").change(function() {
                    $(".data-amnts, .extracharge").show();
                });
                $('.addcharge').click(function() {
                    $(".chargeadd").toggle();
                    $(this).toggleClass("icon-minus hidecharge");

                });

                $(".savecharge").on('keyup keypress keydown click', function(e) {
                    var label = $("#labelcat").val();
                    var currentval = $("#currentval").val();
                    var billid = $("#billid").val();
                    var totalamount = parseFloat($("#Bills_bill_totalamount").val());


                    if (billid == '') {

                        $('.errorcharge').html('Please add item');

                    } else if (label != '' && currentval != '') {
                        $('.loading-overlay').addClass('is-active');
                        $(".savecharge").attr('disabled', 'disabled');
                        $.ajax({
                            url: "<?php echo $addbillurl; ?>",
                            data: {
                                category: label,
                                amount: currentval,
                                billid: billid
                            },
                            type: "POST",
                            dataType: 'json',
                            success: function(data) {

                                $(".savecharge").removeAttr('disabled');
                                if (data != '') {
                                    $("#addtional_list").append("<div>" + label + " : " + "<span class='totalprice'>" + parseFloat(currentval).toFixed(2) + "</span> &nbsp;&nbsp&nbsp;<span class='icon icon-trash deleteitem' title='Delete' id=" + data.itemid + "></span> " + "</div>");
                                    $(".additional_total").show();
                                    $('#labelcat').val('');
                                    $('#currentval').val('');
                                    $("#totaladditional_charge").text(data.amount);
                                    var tot = parseFloat(totalamount) + parseFloat(currentval);
                                    tot = parseFloat(tot).toFixed(2);
                                    $("#Bills_bill_totalamount").val(tot);
                                    $(".deleteitem").click(function() {

                                        var ftotalamount = parseFloat($("#Bills_bill_totalamount").val());
                                        $(this).parent().remove();
                                        var billid = $("#billid").val();
                                        var id = $(this).attr('id');
                                        $('.loading-overlay').addClass('is-active');
                                        $.ajax({
                                            url: "<?php echo $delbillurl; ?>",
                                            data: {
                                                id: id,
                                                billid: billid
                                            },
                                            type: "POST",
                                            dataType: 'json',
                                            success: function(data) {
                                                $(".savecharge").removeAttr('disabled');
                                                var tot = parseFloat(data.amount).toFixed(2);
                                                var tot1 = parseFloat(data.additional).toFixed(2);
                                                $("#Bills_bill_totalamount").val(tot);
                                                $("#totaladditional_charge").text(tot1);
                                            }
                                        });



                                    });


                                }



                            }
                        });
                    } else {

                        if (label == '' || currentval == '') {
                            $('.errorcharge').html('Both fields are required');
                        }

                    }
                });
                $("#Bills_company_id").focus();

                $(document).on("change", "#Bills_company_id", function() {
                    var val = $(this).val()
                    $('#loading').show();
                    if (val != '') {
                        $.ajax({
                            method: "POST",
                            dataType: "json",
                            url: '<?php echo Yii::app()->createUrl('bills/ajax'); ?>',
                            success: function(result) {
                                $("#Bills_purchase_id").select2("focus");
                            }
                        });
                    }
                });


                var billCookie = getCookie("billid");
                var billNoCookie = getCookie("billnumber");
                if (billCookie) {
                    $.ajax({
                        url: "<?php echo $reloadUrl; ?>",
                        data: {
                            "billid": billCookie,
                            "billnumber": billNoCookie
                        },
                        type: "POST",
                        success: function(data) {
                            if (data == 1) {
                                setCookie("billid", "", -1);
                                setCookie("billnumber", "", -1);
                            }
                        }
                    });
                }

                $("body").on('change', '.table tr.pitems input,.table tr.pitems select.purchase_unitvendor, .table tr.pitems select.tax_slab', function(e) {

                    var rowId = $(this).parents("td").attr("id");
                    var aStat;

                    if (($(this).hasClass("biquantity")) || ($(this).hasClass("birate")) || ($(this).hasClass("purchase_unitvendor"))) {
                        var type = 1;
                        getconversionFactor(rowId, type);

                    }
                    if (($(this).hasClass("basequantity"))) {
                        var type = 2;
                        getconversionFactor(rowId, type);
                    }


                    var billNo = $("#Bills_bill_number").val();
                    var quantity = parseFloat($("#biquantity" + rowId).val());
                    var amount = parseFloat($("#biamount" + rowId).text());
                    var damount = parseFloat($("#damount" + rowId).val());
                    var dpercent = parseFloat($("#dpercent" + rowId).text());
                    var newDAmt = amount * (dpercent / 100);
                    var warehouseid = $('#Warehousestock_warehousestock_warehouseid').val();
                    if (newDAmt != 0)
                        newDAmt = newDAmt.toFixed(2);
                    var availQty = parseFloat($("#availablequantity" + rowId).val());
                    var crId = $(this).attr("id");
                    if(($(this).hasClass("biquantity")) ){   
                        if (quantity <= 0) {
                        $("#alert").addClass("alert alert-danger").html("<strong>Quantity must be greater than or equal to zero<strong>");
                        $("#biquantity" + rowId).val(0);
                        return false;
                       }
                    }
                    if (amount < damount && (amount != 0 || quantity == 0)) {
                        $("#alert").addClass("alert alert-danger").html("<strong>Discount amount must be less than the amount<strong>");
                        $("#damount" + rowId).val(newDAmt);
                    } else if (billNo == "") {
                        $("#alert").addClass("alert alert-danger").html("<strong>Please enter bill number<strong>");
                        this.checked = false;
                        return false;
                    } else if (warehouseid == "") {
                        $("#alert").addClass("alert alert-danger").html("<strong>Please Choose Warehouse<strong>");
                        this.checked = false;
                        return false;
                    } else {
                        $("#alert").removeClass("alert alert-danger").html("");

                        validateBillNumber(billNo, rowId, 2, crId);
                    }
                });

                $('#bills-form').on('keypress', ':input', function(event) {
                    if (event.keyCode == 13) {
                        var thisId = $(this).attr("id");
                        var thisVal = $(this).val();

                        if (thisId == "Bills_purchase_id" && thisVal == "") {

                        } else {
                            var inputs = $(this).parents("form").eq(0).find(":input");
                            var idx = inputs.index(this);

                            if (idx == inputs.length - 1) {

                            } else {
                                inputs[idx + 1].focus();
                                inputs[idx + 1].select();
                            }
                            return false;
                        }
                    }
                });
                $("#Bills_bill_date").keypress(function(e) {
                    if (e.keyCode == 13) {
                        $("#biquantity0").focus();
                        $("#biquantity0").select();
                    }
                });
                $('#bills-form').on('keypress', '.lnext', function(e) {

                    if (e.keyCode == 13) {
                        var rowId = parseInt($(this).parent().attr("id"));
                        var totrows = $("#totrows").val();
                        var nextId = rowId + 1;
                        if (nextId == totrows) {
                            var billNo = $("#Bills_bill_number").val();
                            if (billNo == "")
                                $("#Bills_bill_number").focus()
                            else
                                $("#buttonsubmit").focus();
                        } else {
                            $("#biquantity" + nextId).focus();
                            $("#biquantity" + nextId).select();
                        }
                    }
                });
                $("#buttonsubmit").keypress(function(e) {
                    if (e.keyCode == 13) {
                        $('.buttonsubmit').click();
                    }
                });

                $(".buttonsubmit").click(function() {



                    var Warehouse_id = $('#Warehousestock_warehousestock_warehouseid').val();
                    var general_settings_for_auto_receipt = <?php echo $general_settings_for_auto_receipt; ?>;
                    if (general_settings_for_auto_receipt == 1) {
                        var answer = confirm("Are you sure you want to auto receipt this item?");
                        if (answer) {
                            if (Warehouse_id == "") {
                                $('#Warehousestock_warehousestock_warehouseid_em_').show();
                                $('#Warehousestock_warehousestock_warehouseid_em_').html('Warehouse is required');
                            } else {
                                $('#Warehousestock_warehousestock_warehouseid_em_').hide();
                                $('#Warehousestock_warehousestock_warehouseid_em_').html('');

                            }
                            var warehouse_error = $('#Warehousestock_warehousestock_warehouseid_em_').text();
                            if (warehouse_error == "") {
                                $("#bills-form").submit();
                            }
                        } else {
                            return false;
                        }
                    } else {
                        if (Warehouse_id == "") {
                            $('#Warehousestock_warehousestock_warehouseid_em_').show();
                            $('#Warehousestock_warehousestock_warehouseid_em_').html('Warehouse is required');
                        } else {
                            $('#Warehousestock_warehousestock_warehouseid_em_').hide();
                            $('#Warehousestock_warehousestock_warehouseid_em_').html('');

                        }
                        var warehouse_error = $('#Warehousestock_warehousestock_warehouseid_em_').text();
                        if (warehouse_error == "") {
                            $("#bills-form").submit();
                        }
                    }
                })


                $("#Bills_bill_number").change(function() {
                    var billNo = $("#Bills_bill_number").val();
                    var crId = "";
                    $('#loading').show();
                    $.ajax({
                        url: "<?php echo $valUrl; ?>",
                        data: {
                            "billno": billNo
                        },
                        type: "GET",
                        success: function(data) {
                            if (data == 1) {
                                $("#alert").addClass("alert alert-danger").html("<strong>Bill number already exist. Please enter a new bill number.<strong>");

                                $('#buttonsubmit').prop('disabled', true);
                                return false;
                            } else {
                                $("#alert").removeClass("alert alert-danger").html("");
                                $('#buttonsubmit').prop('disabled', true);
                                var totrows = parseInt($("#totrows").val());
                                for (var i = 0; i < totrows; i++) {
                                    var biQuantity = parseFloat($("#biquantity" + i).val());
                                    if (biQuantity > 0) {
                                        validateBillNumber(billNo, i, 2, crId);
                                    }
                                }
                            }
                        }
                    });
                });

            });



            function setCookie(cname, cvalue, exdays) {
                var d = new Date();
                d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
                var expires = "expires=" + d.toUTCString();
                document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
            }

            function getCookie(cname) {
                var name = cname + "=";
                var decodedCookie = decodeURIComponent(document.cookie);
                var ca = decodedCookie.split(';');
                for (var i = 0; i < ca.length; i++) {
                    var c = ca[i];
                    while (c.charAt(0) == ' ') {
                        c = c.substring(1);
                    }
                    if (c.indexOf(name) == 0) {
                        return c.substring(name.length, c.length);
                    }
                }
                return "";
            }

            function addItemToBills(rowId, aStat, crId) {

                $(".formError").hide();

                var purchaseId = $("#Bills_purchase_id").val();
                var billitembatch = $("#bibatch" + rowId).val();
                var company_id = $("#Bills_company_id").val();
                var billNumber = $("#Bills_bill_number").val();
                var billDate = $("#Bills_bill_date").val();
                var totrows = parseInt($("#totrows").val());
                var billId = $("#billid").val();
                var itemId = parseInt($("#ids" + rowId).val());
                var quantity = parseFloat($("#biquantity" + rowId).val());
                quantity = isNaN(quantity) ? 0 : quantity;
                $("#biquantity" + rowId).val(quantity);
                var unit = $("#purchase_unitvendor" + rowId + " option:selected").text();
                var rate = parseFloat($("#birate" + rowId).val());
                var orRate = parseFloat($("#orrate" + rowId).val());
                var hsn_code = $("#hsn_code" + rowId).text();
                var purch_unit = $("#biunit" + rowId).text();

                var Warehouse_id = $('#Warehousestock_warehousestock_warehouseid').val();
                if (Warehouse_id == "") {
                    $('#Warehousestock_warehousestock_warehouseid_em_').show();
                    $('#Warehousestock_warehousestock_warehouseid_em_').html('Warehouse is required');
                } else {
                    $('#Warehousestock_warehousestock_warehouseid_em_').hide();
                    $('#Warehousestock_warehousestock_warehouseid_em_').html('');
                }

                rate = isNaN(rate) ? orRate : rate;
                if (rate < 0)
                    rate = orRate;
                if (quantity % 1 == 0)
                    quantity = quantity;
                else
                    quantity = quantity.toFixed(2);
                $("#biquantity" + rowId).val(quantity);
                if (rate != 0)
                    rate = parseFloat(rate.toFixed(2));
                $("#birate" + rowId).val(rate);
                if (crId == "biquantity" + rowId) {
                    $("#birate" + rowId).select();
                }


                if (purch_unit != unit) {
                    var purchasequantity = $("#basequantity" + rowId).val();
                    var purchaseunit = $.trim(purch_unit);
                    var purchaserate = $("#baserate" + rowId).text();
                } else {
                    var purchasequantity = quantity;
                    var purchaseunit = $.trim(purch_unit);
                    var purchaserate = rate;
                }


                var amount = parseFloat($("#biamount" + rowId).text());
                var damount = parseFloat($("#damount" + rowId).val());
                damount = isNaN(damount) ? 0 : damount;
                if (damount < 0)
                    damount = 0;
                if (damount != 0)
                    damount = parseFloat(damount.toFixed(2));
                $("#damount" + rowId).val(damount);
                if (crId == "birate" + rowId) {
                    $("#damount" + rowId).select();
                }


                var dpercent = parseFloat($("#dpercent" + rowId).text());
                var cgstpercent = parseFloat($("#cgstpercent" + rowId).val());
                cgstpercent = isNaN(cgstpercent) ? 0 : cgstpercent;
                if (cgstpercent < 0)
                    cgstpercent = 0;
                if (cgstpercent != 0)
                    cgstpercent = parseFloat(cgstpercent.toFixed(2));
                $("#cgstpercent" + rowId).val(cgstpercent);
                if (crId == "damount" + rowId) {
                    $("#cgstpercent" + rowId).select();
                }
                var cgst = parseFloat($("#cgst" + rowId).text());
                var sgstpercent = parseFloat($("#sgstpercent" + rowId).val());
                sgstpercent = isNaN(sgstpercent) ? 0 : sgstpercent;
                if (sgstpercent < 0)
                    sgstpercent = 0;
                if (sgstpercent != 0)
                    sgstpercent = parseFloat(sgstpercent.toFixed(2));
                $("#sgstpercent" + rowId).val(sgstpercent);
                if (crId == "cgst" + rowId) {
                    $("#sgstpercent" + rowId).select();
                }

                var igstpercent = parseFloat($("#igstpercent" + rowId).val());
                igstpercent = isNaN(igstpercent) ? 0 : igstpercent;
                if (igstpercent < 0)
                    igstpercent = 0;
                if (igstpercent != 0)
                    igstpercent = parseFloat(igstpercent.toFixed(2));
                $("#igstpercent" + rowId).val(igstpercent);
                if (crId == "igstpercent" + rowId) {
                    $("#igstpercent" + rowId).select();
                }

                var sgst = parseFloat($("#sgst" + rowId).text());
                var igst = parseFloat($("#igst" + rowId).text());
                var taxtotal = parseFloat($("#taxamount" + rowId).text());
                var taxpercent = parseFloat($("#taxpercent" + rowId).text());
                var totalamount = parseFloat($("#totalamount" + rowId).text());
                var categoryId = $("#category" + rowId).val();
                var billTotal = parseFloat($("#Bills_bill_amount").text());
                var billDiscount = parseFloat($("#Bills_bill_discountamount").text());
                var billTax = parseFloat($("#Bills_bill_taxamount").text());
                var billGTotal = parseFloat($("#Bills_bill_totalamount").val());
                var categoryName = $("#bicategoryname" + rowId).text();
                var availQty = parseFloat($("#availablequantity" + rowId).val());
                var billItem = $("#billitem" + rowId).val();
                var actualquantity = parseFloat($("#actualquantity" + rowId).text());
                var tax_slab = parseFloat($("#tax_slab" + rowId).val());

                //alert(categoryId);
                var length = $("#bilength" + rowId).text();
                var width = $("#biwidth" + rowId).text();
                var height = $("#biheight" + rowId).text();
                var purchase_type = $("#purchase_type").val();

                if (quantity > 0) {

                    if (purchase_type == 'A') {

                        var newAmt = (quantity * rate * length).toFixed(2);
                    } else if (purchase_type == 'G') {

                        var newAmt = (quantity * rate * width * height).toFixed(2);
                    } else if (purchase_type == 'O') {

                        var newAmt = (quantity * rate).toFixed(2);
                    }
                    var newDp = ((damount / newAmt) * 100);
                    if (newDp % 1 !== 0)
                        newDp = newDp.toFixed(2);

                    var newAmtD = newAmt - damount;
                    var newCgst = ((cgstpercent / 100) * newAmtD);
                    if (newCgst % 1 !== 0)
                        newCgst = newCgst.toFixed(2);
                    var newSgst = ((sgstpercent / 100) * newAmtD);
                    if (newSgst % 1 !== 0)
                        newSgst = newSgst.toFixed(2);

                    var newIgst = ((igstpercent / 100) * newAmtD);
                    if (newIgst % 1 !== 0)
                        newIgst = newIgst.toFixed(2);

                    var newTotalTax = parseFloat(newCgst) + parseFloat(newSgst) + parseFloat(newIgst);
                    newTotalTax = newTotalTax.toFixed(2);
                    var newTaxP = ((newTotalTax / newAmtD) * 100);
                    if (newTaxP % 1 !== 0)
                        newTaxP = newTaxP.toFixed(2);
                    if (rate == 0) {

                    }
                    newAmt = isNaN(newAmt) ? 0 : newAmt;
                    newDp = isNaN(newDp) ? 0 : newDp;
                    newCgst = isNaN(newCgst) ? 0 : newCgst;
                    newSgst = isNaN(newSgst) ? 0 : newSgst;
                    newIgst = isNaN(newIgst) ? 0 : newIgst;
                    newTaxP = isNaN(newTaxP) ? 0 : newTaxP;

                    var newTotal = parseFloat(newAmtD) + parseFloat(newTotalTax);
                    newTotal = isNaN(newTotal) ? 0 : newTotal;
                    newTotal = newTotal.toFixed(2);
                } else {
                    var newAmt = 0;
                    var newDp = 0;
                    var newAmtD = 0;
                    var newCgst = 0;
                    var newSgst = 0;
                    var newIgst = 0;
                    var newTotalTax = 0;
                    var newTaxP = 0;
                    var newTotal = 0;
                }


                $("#biamount" + rowId).text(newAmt ? newAmt : 0);
                $("#dpercent" + rowId).text(newDp ? newDp : 0);
                $("#cgst" + rowId).text(newCgst ? newCgst : 0);
                $("#sgst" + rowId).text(newSgst ? newSgst : 0);
                $("#igst" + rowId).text(newIgst ? newIgst : 0);
                $("#taxamount" + rowId).text(newTotalTax ? newTotalTax : 0);
                $("#taxpercent" + rowId).text(newTaxP ? newTaxP : 0);
                $("#totalamount" + rowId).text(newTotal ? newTotal : 0);
                var billNTotal = 0;
                var billNDiscount = 0;
                var billNTax = 0;
                var billNGTotal = 0;
                for (var i = 0; i < totrows; i++) {
                    if ($("#biquantity" + i).val() > 0) {
                        billNTotal = billNTotal + parseFloat($("#biamount" + i).text());
                        billNDiscount = billNDiscount + parseFloat($("#damount" + i).val());
                        billNTax = billNTax + parseFloat($("#taxamount" + i).text());
                        billNGTotal = billNGTotal + parseFloat($("#totalamount" + i).text());
                    }
                }

                if (quantity == 0 && billItem == "") {
                    var opStatus = 0;
                } else if (quantity == 0 && billItem != "") {
                    var opStatus = 2;
                } else if (quantity > 0 && billItem == "") {
                    var opStatus = 1;
                } else if (quantity > 0 && billItem != "") {
                    var opStatus = 3;
                } else {
                    var opStatus = 2;
                }
                var tqty = parseFloat($("#tqty").val());
                var btqty = parseFloat($("#btqty").val());
                var newQT = 0;
                for (var i = 0; i < totrows; i++) {
                    newQT = newQT + parseFloat($("#biquantity" + i).val());
                }
                var newTotalQty = btqty + newQT;
                if (tqty > newTotalQty)
                    var purchaseStatus = 94;
                else
                    var purchaseStatus = 93;
                $('.loading-overlay').addClass('is-active');
                $.ajax({
                    url: "<?php echo $addUrl; ?>",
                    data: {
                        "pid": purchaseId,
                        "billnumber": billNumber,
                        "billitembatch": billitembatch,
                        "billdate": billDate,
                        "billamount": billNTotal,
                        "billdiscount": billNDiscount,
                        "billtax": billNTax,
                        "billtotal": billNGTotal,
                        "itemid": itemId,
                        "quantity": quantity,
                        "unit": unit,
                        "rate": rate,
                        "purchasequantity": purchasequantity,
                        "purchaseunit": purchaseunit,
                        "purchaserate": purchaserate,
                        "amount": newAmt,
                        "damount": damount,
                        "dpercent": newDp,
                        "cgst": newCgst,
                        "cgstpercent": cgstpercent,
                        "sgst": newSgst,
                        "sgstpercent": sgstpercent,
                        "igst": newIgst,
                        "igstpercent": igstpercent,
                        "totaltax": newTotalTax,
                        "totaltaxp": newTaxP,
                        "totalamount": newTotal,
                        "billid": billId,
                        "astat": opStatus,
                        "categoryid": categoryId,
                        "categoryname": categoryName,
                        "availqty": availQty,
                        "billitem": billItem,
                        "actualquantity": actualquantity,
                        "purchasestatus": purchaseStatus,
                        "company_id": company_id,
                        "tax_slab": tax_slab,
                        "hsn_code": hsn_code,
                        "length": length,
                        "width": width,
                        "height": height,
                        "Warehouse_id": Warehouse_id
                    },
                    type: "POST",
                    success: function(data) {
                        var result = JSON.parse(data);
                        if (result.amount !== '') {
                            $('#Bills_bill_amount').text(parseFloat(result.amount).toFixed(2));
                        }
                        if (result.disc_amount !== '') {
                            $('#Bills_bill_discountamount').text(parseFloat(result.disc_amount).toFixed(2));
                        }
                        if (result.tax_amount !== '') {
                            $('#Bills_bill_taxamount').text(parseFloat(result.tax_amount).toFixed(2));
                        }
                        if (result.tot_amount !== '') {
                            $('#Bills_bill_totalamount').val(parseFloat(result.tot_amount).toFixed(2));
                        }
                        $("#billid").val(result[0]);
                        $("#billitem" + rowId).val(result[1]);
                        $("#savedquantity" + rowId).val(quantity);
                        setCookie("billid", result[0], 1);
                        setCookie("billnumber", billNumber, 1);
                        if (quantity > 0) {
                            $("#tickmark" + rowId).html('<i class="fa fa-check" aria-hidden="true"></i>');
                            $('#buttonsubmit').prop('disabled', false);
                        } else {
                            $("#tickmark" + rowId).html("");
                            $('#buttonsubmit').prop('disabled', true);
                        }
                    },
                    error: function() {
                        $('#buttonsubmit').prop('disabled', false);
                        $("#save_status" + rowId).val(2);
                    }
                });
            }

            function validateBillNumber(billNo, rowId, stat, crId) {
                $('#loading').show();
                $.ajax({
                    url: "<?php echo $valUrl; ?>",
                    data: {
                        "billno": billNo
                    },
                    type: "GET",
                    success: function(data) {
                        if (data == 1) {
                            $("#alert").addClass("alert alert-danger").html("<strong>Bill number already exist. Please enter a new bill number.<strong>");
                            $(".chkitem").prop('checked', false);

                            return false;
                        } else if (data == 2) {
                            $("#alert").removeClass("alert alert-danger").html("");
                            $("#Bills_bill_number").attr("readonly", true)
                            var aStat;
                            if (stat == 1) {
                                if ($("input[name='chkitem[" + rowId + "]']").is(':checked')) {
                                    aStat = 1;
                                } else {
                                    aStat = 2;
                                }
                                addItemToBills(rowId, aStat, crId);
                            } else if (stat == 2) {
                                if ($("input[name='chkitem[" + rowId + "]']").is(':checked')) {
                                    aStat = 3
                                } else {
                                    aStat = 4;
                                }
                                addItemToBills(rowId, aStat, crId);
                            }
                        }
                    }
                });
            }

            $("#Bills_company_id").change(function() {
                var val = $(this).val();
                $('#loading').show();
                $("#Bills_purchase_id").html('<option value="">Select Project</option>');
                $.ajax({
                    url: '<?php echo Yii::app()->createUrl('bills/dynamicpurchase'); ?>',
                    method: 'POST',
                    data: {
                        company_id: val
                    },
                    dataType: "json",
                    success: function(response) {
                        if (response.status == 'success') {
                            $("#Bills_purchase_id").html(response.html);
                        } else {
                            $("#Bills_purchase_id").html(response.html);
                        }
                    }

                })
            })
            $(document).ajaxComplete(function() {
                $('.loading-overlay').removeClass('is-active');
                $('#loading').hide();
            });

            function getconversionFactor(rowId, type) {

                var itemId = parseInt($("#category" + rowId).val());
                var base_unit = $.trim($("#biunit" + rowId).text());
                var amt = $("#biamount" + rowId).text();
                var quantity = parseFloat($("#biquantity" + rowId).val());
                quantity = isNaN(quantity) ? 0 : quantity;
                var purchase_unit = $("#purchase_unitvendor" + rowId + " option:selected").text();
                var rate = parseFloat($("#birate" + rowId).val());
                var orRate = parseFloat($("#orrate" + rowId).val());
                rate = isNaN(rate) ? orRate : rate;
                if (rate < 0)
                    rate = orRate;
                if (quantity % 1 == 0)
                    quantity = quantity;
                else
                    quantity = quantity.toFixed(4);
                if (rate != 0)
                    rate = parseFloat(rate.toFixed(2));

                var length = $("#bilength" + rowId).text();
                var width = $("#biwidth" + rowId).text();
                var height = $("#biheight" + rowId).text();
                var purchase_type = $("#purchase_type").val();
                var baseqty = parseFloat($("#basequantity" + rowId).val());
                var baserate = 0;



                if (quantity > 0) {
                    if (purchase_type == 'A') {
                        var amt = (quantity * rate * length).toFixed(2);
                    } else if (purchase_type == 'G') {
                        var amt = (quantity * rate * width * height).toFixed(2);
                    } else if (purchase_type == 'O') {
                        var amt = (quantity * rate).toFixed(2);
                    }
                } else {
                    amt = 0;
                }

                if (purchase_unit !== '' && base_unit !== '' && itemId !== "") {
                    if (base_unit != purchase_unit) {

                        $.ajax({
                            method: "POST",
                            data: {
                                'purchase_unit': purchase_unit,
                                'base_unit': base_unit,
                                'item_id': itemId
                            },
                            dataType: "json",
                            url: '<?php echo Yii::app()->createAbsoluteUrl('bills/getUnitconversionFactor'); ?>',
                            success: function(result) {

                                if (type == 1) {
                                    var base_quantity = parseFloat(result * quantity).toFixed(2);
                                } else {
                                    var base_quantity = baseqty;
                                }

                                if (isNaN(base_quantity))
                                    base_quantity = 0;
                                if (base_quantity != 0) {
                                    baserate = amt / base_quantity;
                                    baserate = baserate.toFixed(2);
                                    if (isNaN(baserate))
                                        baserate = 0;
                                }
                                $("#basequantity" + rowId).attr("readonly", false);
                                $("#basequantity" + rowId).val(base_quantity);
                                $("#baserate" + rowId).text(baserate);
                            }
                        });
                    } else {

                        var baseval = 0;
                        $("#basequantity" + rowId).attr("readonly", true);
                        $("#basequantity" + rowId).val(quantity);
                        $("#baserate" + rowId).text(rate);
                    }

                }

            }
        </script>
        <?php $url = Yii::app()->createAbsoluteUrl("bills/getItemsByPurchase"); ?>
        <?php Yii::app()->clientScript->registerScript('myscript', '
$(document).ready(function(){
    var p_id = $("#Bills_purchase_id").val();
    if(p_id != "") {
        getAllItems(p_id);
    }
    $("#Bills_purchase_id").change(function(){
        $(".loading-overlay").addClass("is-active");
        var p_id= $("#Bills_purchase_id").val();
        if(p_id == "")
            p_id = 0;
        getAllItems(p_id);      
    });
});
function getAllItems(p_id) {
        $.ajax({
           url: "' . $url . '",
            data: {"id": p_id}, 
            //dataType: "json",
            type: "GET",
            success:function(data){
                //alert(data);
                $("#client").html(data);
                $("#Bills_bill_number").focus(); 
                //var amount = $("#aj-amount").val();
                //alert(amount);
                //$("#Bills_bill_amount").val(amount);
                //$("#Bills_bill_totalamount").val(amount);
            }
        });
}
        '); ?>