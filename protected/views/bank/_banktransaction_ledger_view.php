<?php if (filter_input(INPUT_GET, 'export')) { ?>
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/pdfstyle.css">
<?php } ?>
<?php

$date = array_column($data_array, 'payment_date');
array_multisort($date, SORT_ASC, $data_array);

$company_address = Company::model()->find(array('order' => ' id ASC'));

?>
<div class="table-responsive" id="table-wrapper">
    <table class="table ledger_table">
        <thead class="entry-table sticky-thead">
            <tr>
                <th colspan="7">
                    <div>
                        <?php echo isset($company_address->name) ? $company_address->name : ''; ?>
                    </div>
                    <div>
                        <?php echo isset($company_address->address) ? $company_address->address : ''; ?>
                    </div>
                </th>
            </tr>
            <tr>
                <th width="10%">Date</th>
                <th>Reff</th>
                <th width="10%">Cheque</th>
                <th width="10%">Narration</th>
                <th width="20%">Bank Invoice</th>
                <th width="20%">Bank Expense</th>
                <th width="20%">Bank Balance</th>
            </tr>
        </thead>
        <tbody> 
            <?php
            $newQuery = "";
            $user = Users::model()->findByPk(Yii::app()->user->id);
            $arrVal = explode(',', $user->company_id);
            foreach ($arrVal as $arr) {
                if ($newQuery)
                    $newQuery .= ' OR';
                $newQuery .= " FIND_IN_SET('" . $arr . "', company_id)";
            }
            if (isset($_REQUEST["bank_id"]) && $_REQUEST["bank_id"] !== '') {
                $bank_id = $_REQUEST["bank_id"];
                $sql = "SELECT * FROM `jp_cashbalance` "
                    . " WHERE bank_id =$bank_id AND (" . $newQuery . ") ";
            } else {
                $sql = "SELECT * FROM `jp_cashbalance` "
                    . " WHERE bank_id IS NOT NULL AND (" . $newQuery . ") ";
            }
            $cash_blance = Yii::app()->db->createCommand($sql)->queryRow();
            $opening_balance = floatval($cash_blance['cashbalance_opening_balance']);
            $balanceledger_opening = $opening_balance;
            //echo "<pre>"; print_r($cash_blance) ;exit;
            if (isset($_REQUEST['date_from']) && ($_REQUEST['date_from']) != '') {
                /**closing balance based on filter starts **/
                $tblpx = Yii::app()->db->tablePrefix;
                if (isset($_REQUEST["bank_id"]) && $_REQUEST["bank_id"] !== '') {
                    $bankid = $_REQUEST["bank_id"];
                    $sql = "SELECT * FROM `jp_cashbalance` "
                        . " WHERE bank_id=$bankid AND (" . $newQuery . ") ";
                } else {
                    $sql = "SELECT * FROM `jp_cashbalance` "
                        . " WHERE bank_id IS NOT NULL AND (" . $newQuery . ") ";
                }

                $cash_blance = Yii::app()->db->createCommand($sql)->queryRow();
                //echo "<pre>"; print_r($sql);exit;
                $where = '';
                if (!empty($date_from)) {
                    $where .= " AND (`date` >= '" . $cash_blance['cashbalance_date'] . "' and `date`<'" . $date_from . "') ";
                }
                $bankQuery = "";
                if (isset($_REQUEST["bank_id"]) && $_REQUEST["bank_id"] !== '') {
                    $bank_id = $_REQUEST["bank_id"];
                    $bankQuery .= "AND bank_id=$bank_id";
                }

                $data_array_for_cls_bal = array();
                $sql = "SELECT * FROM {$tblpx}expenses WHERE (" . $newQuery . ") " . $where . $bankQuery . " AND (expense_type=88 OR payment_type=88) AND subcontractor_id IS NULL  AND reconciliation_status=1";  // OR expense_type=103              
                $daybook_data = Yii::app()->db->createCommand($sql)->queryAll();
                //echo "<pre>";print_r($daybook_data);exit;
                foreach ($daybook_data as $key => $value) {
                    $data_array_for_cls_bal[$key]['payment_type'] = 'Daybook';
                    $data_array_for_cls_bal[$key]['expense_amount'] = $value['paid'];
                    $data_array_for_cls_bal[$key]['receipt_amount'] = $value['receipt'];
                    $data_array_for_cls_bal[$key]['payment_date'] = $value['date'];
                    if ($value['bank_id'] != '') {
                        $bank_det = Bank::model()->findByPk($value['bank_id']);
                        if (!empty($bank_det)) {
                            $data_array_for_cls_bal[$key]['bank'] = $bank_det->bank_name;

                        }
                        $data_array_for_cls_bal[$key]['cheque_no'] = $value['cheque_no'];

                    }

                    if ($value['type'] == 73) {
                        $vendor = Vendors::model()->findByPk($value['vendor_id']);
                        $vendor_name = $vendor['name'];
                        if ($value['bill_id'] != NULL) {
                            $bills = Bills::model()->findByPk($value['bill_id']);
                            $expense_head = ExpenseType::model()->findByPk($value['exptype']);
                            $bill_thead = $bills['bill_number'] . ' / ' . $expense_head['type_name'];
                        } else {
                            $expense_head = ExpenseType::model()->findByPk($value['exptype']);
                            $bill_thead = $expense_head['type_name'];
                        }
                    } else {
                        $vendor_name = '';
                        $bill_thead = '';
                    }
                    $data_array_for_cls_bal[$key]['vendor'] = $vendor_name;
                    $data_array_for_cls_bal[$key]['description'] = $value['description'];
                    $data_array_for_cls_bal[$key]['bill_exphead'] = $bill_thead;
                    $data_array_for_cls_bal[$key]['company_id'] = $value['company_id'];
                }
                // echo "<pre>";print_r($data_array_for_cls_bal);exit;
                // dailyexpense
            
                $where_con = "";

                if (!empty($date_from)) {
                    $where_con .= " `date` >= '" . $cash_blance['cashbalance_date'] . "' and `date`<'" . $date_from . "' AND ";
                }            // receipt
                $sql = "SELECT * FROM {$tblpx}dailyexpense "
                    . " WHERE $where_con (dailyexpense_receipt_type=88 AND "
                    . " dailyexpense_chequeno IS NOT NULL " . $bankQuery
                    . " AND reconciliation_status = 1 "
                    . " AND company_id=" . $cash_blance['company_id'] . ")  "
                    . " OR (dailyexpense_receipt_type=88 "
                    . " AND expensehead_type=4  AND "
                    . " company_id=" . $cash_blance['company_id'] . ") AND "
                    . " parent_status='1' AND "
                    . " exp_type=72 ";

                if (isset($cash_blance['company_id'])) {
                    $dailyexpense_data = Yii::app()->db->createCommand($sql)->queryAll();
                    //echo "<pre>";print_r($dailyexpense_data);exit;
                } else {
                    $dailyexpense_data = array();
                }

                $a = count($daybook_data);

                foreach ($dailyexpense_data as $key => $value) {
                    $data_array_for_cls_bal[$a]['payment_type'] = 'Dailyexpense';
                    $data_array_for_cls_bal[$a]['expense_amount'] = $value['dailyexpense_paidamount'];
                    $data_array_for_cls_bal[$a]['receipt_amount'] = $value['dailyexpense_receipt'];
                    $data_array_for_cls_bal[$a]['payment_date'] = $value['date'];
                    if ($value['bank_id'] != '') {
                        $bank_det = Bank::model()->findByPk($value['bank_id']);
                        if (!empty($bank_det)) {
                            $data_array_for_cls_bal[$a]['bank'] = $bank_det->bank_name;

                        }
                        $data_array_for_cls_bal[$a]['cheque_no'] = $value['dailyexpense_chequeno'];

                    }
                    if ($value["dailyexpense_type"] == "deposit") {
                        $depositId = $value["expensehead_id"];
                        $deposit = Deposit::model()->findByPk($depositId);
                        $expense_head = $deposit->deposit_name;
                    } else if ($value["dailyexpense_type"] == "expense") {
                        $expense = Companyexpensetype::model()->findByPk($value['expensehead_id']);
                        $expense_head = isset($expense->name) ? $expense->name : "";
                    } else {
                        $receipt = Companyexpensetype::model()->findByPk($value['exp_type_id']);
                        $expense_head = isset($receipt->name) ? $receipt->name : "";
                    }
                    if ($value['bill_id'] != NULL) {
                        $bill_thead = $value['bill_id'] . ' / ' . $expense_head;
                    } else {
                        $bill_thead = $expense_head;
                    }

                    $data_array_for_cls_bal[$a]['vendor'] = '';
                    $data_array_for_cls_bal[$a]['description'] = $value['description'];
                    $data_array_for_cls_bal[$a]['bill_exphead'] = $bill_thead;
                    $data_array_for_cls_bal[$a]['company_id'] = $value['company_id'];
                    $a++;
                }

                //expense
                $sql = "SELECT * FROM {$tblpx}dailyexpense "
                    . " WHERE $where_con (expense_type=88) AND exp_type=73 "
                    . " AND parent_status='1' "
                    . $bankQuery
                    . " AND dailyexpense_chequeno IS NOT NULL "
                    . " AND reconciliation_status = 1 "
                    . " AND company_id=" . $cash_blance['company_id']
                    . " $bankQuery";
                if (isset($cash_blance['company_id'])) {
                    $dailyexpense_data1 = Yii::app()->db->createCommand($sql)->queryAll();
                } else {
                    $dailyexpense_data1 = array();
                }

                $a1 = count($daybook_data) + count($dailyexpense_data);
                //echo "<pre>";print_r($dailyexpense_data1);exit;
                foreach ($dailyexpense_data1 as $key => $value) {
                    $data_array_for_cls_bal[$a1]['payment_type'] = 'Dailyexpense';
                    $data_array_for_cls_bal[$a1]['expense_amount'] = $value['dailyexpense_paidamount'];
                    $data_array_for_cls_bal[$a1]['receipt_amount'] = $value['dailyexpense_receipt'];
                    $data_array_for_cls_bal[$a1]['payment_date'] = $value['date'];
                    if ($value["dailyexpense_type"] == "deposit") {
                        $depositId = $value["expensehead_id"];
                        $deposit = Deposit::model()->findByPk($depositId);
                        $expense_head = $deposit->deposit_name;
                    } else if ($value["dailyexpense_type"] == "expense") {
                        $expense = Companyexpensetype::model()->findByPk($value['expensehead_id']);
                        $expense_head = isset($expense->name) ? $expense->name : "";
                    } else {
                        $receipt = Companyexpensetype::model()->findByPk($value['exp_type_id']);
                        $expense_head = isset($receipt->name) ? $receipt->name : "";
                    }
                    if ($value['bank_id'] != '') {
                        $bank_det = Bank::model()->findByPk($value['bank_id']);
                        if (!empty($bank_det)) {
                            $data_array_for_cls_bal[$a1]['bank'] = $bank_det->bank_name;

                        }
                        $data_array_for_cls_bal[$a1]['cheque_no'] = $value['dailyexpense_chequeno'];

                    }
                    if ($value['bill_id'] != NULL) {
                        $bill_thead = $value['bill_id'] . ' / ' . $expense_head;
                    } else {
                        $bill_thead = $expense_head;
                    }

                    $data_array_for_cls_bal[$a1]['vendor'] = '';
                    $data_array_for_cls_bal[$a1]['description'] = $value['description'];
                    $data_array_for_cls_bal[$a1]['bill_exphead'] = $bill_thead;
                    $data_array_for_cls_bal[$a1]['company_id'] = $value['company_id'];
                    $a1++;
                }


                // Vendor payment
                $bankQuery1 = "";
                if (isset($_REQUEST["bank_id"]) && $_REQUEST["bank_id"] !== '') {
                    $bank_id = $_REQUEST["bank_id"];
                    $bankQuery1 .= "AND bank=$bank_id";
                }

                $vendorpayment_data = array();
                $sql = "SELECT * FROM {$tblpx}dailyvendors 
        WHERE (" . $newQuery . ") " . $where . $bankQuery1 . " 
        AND payment_type=88 AND reconciliation_status = 1";

                $vendorpayment_data = Yii::app()->db->createCommand($sql)->queryAll();
                // echo "<pre>";
                // print_r($vendorpayment_data);exit;
                $b = count($daybook_data) + count($dailyexpense_data) + count($dailyexpense_data1);
                foreach ($vendorpayment_data as $key => $value) {
                    $data_array_for_cls_bal[$b]['payment_type'] = 'Vendor  Payment';
                    $data_array_for_cls_bal[$b]['expense_amount'] = $value['amount'] + $value['tax_amount'];
                    $data_array_for_cls_bal[$b]['receipt_amount'] = '';
                    $data_array_for_cls_bal[$b]['payment_date'] = $value['date'];
                    $vendor = Vendors::model()->findByPk($value['vendor_id']);
                    $vendor_name = isset($vendor->name) ? $vendor->name : '';
                    $data_array_for_cls_bal[$b]['vendor'] = $vendor_name;
                    $data_array_for_cls_bal[$b]['description'] = $value['description'];
                    $data_array_for_cls_bal[$b]['bill_exphead'] = '';
                    $data_array_for_cls_bal[$b]['company_id'] = $value['company_id'];
                    if ($value['bank'] != '') {
                        $bank_det = Bank::model()->findByPk($value['bank']);
                        if (!empty($bank_det)) {
                            $data_array_for_cls_bal[$b]['bank'] = $bank_det->bank_name;

                        }
                        $data_array_for_cls_bal[$b]['cheque_no'] = $value['cheque_no'];

                    }
                    $b++;
                }

                // Subcontractor payment
                $subcontractorpayment_data = array();
                $sql = "SELECT * FROM {$tblpx}subcontractor_payment 
                WHERE (" . $newQuery . ") " . $where . " AND payment_type=88 
                AND approve_status ='Yes' AND bank IS NOT NULL AND cheque_no IS NOT NULL " . $bankQuery1 . " AND reconciliation_status = 1";

                $subcontractorpayment_data = Yii::app()->db->createCommand($sql)->queryAll();
                $c = count($daybook_data) + count($dailyexpense_data) + count($dailyexpense_data1) + count($vendorpayment_data);
                //echo "<pre>";print_r($sql);exit;
                foreach ($subcontractorpayment_data as $key => $value) {
                    $data_array_for_cls_bal[$c]['payment_type'] = 'Subcontractor  Payment';
                    $data_array_for_cls_bal[$c]['expense_amount'] = $value['amount'] + $value['tax_amount'];
                    $data_array_for_cls_bal[$c]['receipt_amount'] = '';
                    $data_array_for_cls_bal[$c]['payment_date'] = $value['date'];
                    $data_array_for_cls_bal[$c]['vendor'] = '';
                    $data_array_for_cls_bal[$c]['description'] = $value['description'];
                    $data_array_for_cls_bal[$c]['bill_exphead'] = '';
                    $data_array_for_cls_bal[$c]['company_id'] = $value['company_id'];
                    if ($value['bank'] != '') {
                        $bank_det = Bank::model()->findByPk($value['bank']);
                        if (!empty($bank_det)) {
                            $data_array_for_cls_bal[$c]['bank'] = $bank_det->bank_name;
                        }
                        $data_array_for_cls_bal[$c]['cheque_no'] = $value['cheque_no'];
                    }
                    $c++;
                }
                //echo "<pre>";print_r($data_array_for_cls_bal);exit;
                $buyer_module = GeneralSettings::model()->checkBuyerModule();
                if ($buyer_module) {
                    $buyer_transactions = BuyerTransactions::model()->findAll(array('condition' => '(' . $newQuery . ') ' . $where));
                    $buyer_transactions_cash = BuyerTransactions::model()->findAll(array('condition' => '(' . $newQuery . ') ' . $where . ' AND transaction_type = 89'));
                    $buyer_transactions_bank = BuyerTransactions::model()->findAll(array('condition' => '(' . $newQuery . ') ' . $where . $bankQuery . ' AND transaction_type = 88'));
                    foreach ($buyer_transactions_bank as $key => $value) {
                        $data_array_for_cls_bal[$c]['payment_type'] = 'Buyer Transactions';
                        if ($value['transaction_for'] == 1) {
                            $data_array_for_cls_bal[$c]['receipt_amount'] = $value['total_amount'];
                            $data_array_for_cls_bal[$c]['expense_amount'] = 0;
                        } elseif ($value['transaction_for'] == 2) {
                            $data_array_for_cls_bal[$c]['receipt_amount'] = $value['total_amount'];
                            $data_array_for_cls_bal[$c]['expense_amount'] = 0;
                        } elseif ($value['transaction_for'] == 3) {
                            $data_array_for_cls_bal[$c]['expense_amount'] = $value['total_amount'];
                            $data_array_for_cls_bal[$c]['receipt_amount'] = 0;
                        } else {
                            $data_array_for_cls_bal[$c]['expense_amount'] = 0;
                            $data_array_for_cls_bal[$c]['receipt_amount'] = 0;
                        }
                        if ($value['bank_id'] != '') {
                            $bank_det = Bank::model()->findByPk($value['bank_id']);
                            if (!empty($bank_det)) {
                                $data_array_for_cls_bal[$c]['bank'] = $bank_det->bank_name;

                            }
                            $data_array_for_cls_bal[$c]['cheque_no'] = $value['cheque_no'];

                        }
                        $data_array_for_cls_bal[$c]['payment_date'] = $value['date'];
                        $data_array_for_cls_bal[$c]['vendor'] = '';
                        $data_array_for_cls_bal[$c]['description'] = $value['description'];
                        $data_array_for_cls_bal[$c]['bill_exphead'] = $value['transaction_no'];
                        $data_array_for_cls_bal[$c]['company_id'] = $value['company_id'];
                        $c++;
                    }
                }
                //echo "<pre>";print_r($data_array_for_cls_bal);exit;
                //Find the next opening balance based on the above array
                foreach ($data_array_for_cls_bal as $key => $val) {
                    if ($val["receipt_amount"] != '') {
                        $balanceledger_opening += $val["receipt_amount"];
                    } else {
                        $balanceledger_opening -= $val["expense_amount"];
                    }
                }
                $opening_balance = floatval($balanceledger_opening);

                /**closing balance based on filter ends **/
            }
            $balanceledger = $opening_balance;
            ?>
            <tr>
                <td width="10%">
                    <?php echo Yii::app()->controller->changeDateForamt($cash_blance['cashbalance_date']); ?>
                </td>
                <td ></td>
                <td width="10%"></td>
                <td width="10%">Opening Balance</td>
                <td width="20%" class="text-right">
                    <?php echo Yii::app()->Controller->money_format_inr($opening_balance, 2) ?>
                </td>
                <td width="20%" class="text-right"></td>
                <td width="20%" class="text-right">
                    <?php echo Yii::app()->Controller->money_format_inr($balanceledger, 2) ?>
                </td>
            </tr>
            <?php
            $receipt_sum = 0;
            $expense_sum = 0;

            foreach ($data_array as $key => $value) {
                if ($value['receipt_amount'] != "") { //invoice
                    $balanceledger += $value['receipt_amount'];
                } else { //expense
                    $balanceledger -= $value['expense_amount'];
                }

                $receipt_sum += $value['receipt_amount'];
                $expense_sum += $value['expense_amount'];
                ?>

                <tr>
                    <td width="10%">
                        <?php echo Yii::app()->controller->changeDateForamt($value['payment_date']); ?>
                    </td>
                    <td >
                        <?php echo $value['bill_exphead'] ?>
                    </td>
                    <td width="10%">
                        <?php echo $value['cheque_no'] ?>
                    </td>
                    <td width="10%">
                        <?php echo $value['description'] ?>
                    </td>
                    <td width="20%" class="text-right">
                        <?php echo Yii::app()->Controller->money_format_inr($value['receipt_amount'], 2) ?>
                    </td>
                    <td width="20%" class="text-right">
                        <?php echo Yii::app()->Controller->money_format_inr($value['expense_amount'], 2) ?>
                    </td>
                    <td width="20%" class="text-right">
                        <?php echo Yii::app()->Controller->money_format_inr($balanceledger, 2) ?>
                    </td>
                </tr>
            <?php } ?>
        </tbody>
        <tfoot class="entry-table sticky-tfoot">
            <tr>
                <th colspan="4" width="40%"></th>
                <th width="20%" class="text-right">
                    <?php echo Yii::app()->Controller->money_format_inr(($receipt_sum + $opening_balance), 2) ?>
                </th>
                <th width="20%" class="text-right">
                    <?php echo Yii::app()->Controller->money_format_inr($expense_sum, 2) ?>
                </th>
                <th width="20%"></th>
            </tr>
            <tr>
                <th colspan="6" class="text-right" width="80%">Closing Balance:</th>
                <th class="text-right" width="20%">
                    <?php echo Yii::app()->Controller->money_format_inr($balanceledger, 2) ?>
                </th>
            </tr>
        </tfoot>
    </table>
</div>
