<?php
/* @var $this MaterialsController */
/* @var $model Materials */

$this->breadcrumbs=array(
	'Materials'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);
?>
<div class="panel panel-gray">
    <div class="panel-heading form-head">
        <h3 class="panel-title">Add Materials</h3>
    </div>
    <?php echo $this->renderPartial('_form', array('model' => $model)); ?>
</div>
