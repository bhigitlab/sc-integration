-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jul 23, 2021 at 02:16 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `scv2_new`
--

-- --------------------------------------------------------

--
-- Table structure for table `jp_pre_dailyvendors`
--

CREATE TABLE `jp_pre_dailyvendors` (
  `daily_v_id` int(11) NOT NULL,
  `ref_id` int(11) NOT NULL,
  `followup_id` int(11) DEFAULT NULL,
  `record_grop_id` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `vendor_id` int(11) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `amount` float(15,2) NOT NULL,
  `description` text NOT NULL,
  `date` date NOT NULL,
  `payment_type` smallint(6) DEFAULT NULL,
  `bank` int(11) DEFAULT NULL,
  `cheque_no` varchar(100) DEFAULT NULL,
  `sgst` float DEFAULT NULL,
  `sgst_amount` float DEFAULT NULL,
  `cgst` float DEFAULT NULL,
  `cgst_amount` float DEFAULT NULL,
  `igst` float DEFAULT NULL,
  `igst_amount` float DEFAULT NULL,
  `tds` float DEFAULT NULL,
  `tds_amount` double DEFAULT NULL,
  `tax_amount` float DEFAULT NULL,
  `paidamount` double DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `company_id` int(11) DEFAULT NULL,
  `reconciliation_status` int(11) DEFAULT NULL,
  `reconciliation_date` date DEFAULT NULL,
  `budget_percentage` float DEFAULT NULL,
  `approval_status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '0=>Pending,1=>Approved,2=>Rejected',
  `approved_by` int(11) NOT NULL,
  `record_action` varchar(300) NOT NULL,
  `approve_notify_status` enum('0','1') NOT NULL DEFAULT '1' COMMENT '0=>no.not visible to admin,1=>yes visible to admin',
  `cancelled_by` int(11) DEFAULT NULL,
  `cancelled_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jp_pre_dailyvendors`
--

INSERT INTO `jp_pre_dailyvendors` (`daily_v_id`, `ref_id`, `followup_id`, `record_grop_id`, `vendor_id`, `project_id`, `amount`, `description`, `date`, `payment_type`, `bank`, `cheque_no`, `sgst`, `sgst_amount`, `cgst`, `cgst_amount`, `igst`, `igst_amount`, `tds`, `tds_amount`, `tax_amount`, `paidamount`, `created_by`, `created_date`, `updated_by`, `updated_date`, `company_id`, `reconciliation_status`, `reconciliation_date`, `budget_percentage`, `approval_status`, `approved_by`, `record_action`, `approve_notify_status`, `cancelled_by`, `cancelled_at`) VALUES
(1, 2, NULL, '2021-07-23 08:37:06', 1, 2, 58.00, 'tttes', '2021-07-23', 89, NULL, '', NULL, 0, NULL, 0, NULL, 0, 0, 0, 0, 58, 101, '2021-07-22 18:30:00', 101, '2021-07-23 08:37:06', 1, NULL, NULL, NULL, '2', 0, 'update', '0', 101, '2021-07-23 14:07:16');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `jp_pre_dailyvendors`
--
ALTER TABLE `jp_pre_dailyvendors`
  ADD PRIMARY KEY (`daily_v_id`),
  ADD KEY `dailyexpense_vendor_fk` (`vendor_id`),
  ADD KEY `dailyexpense_project_fk` (`project_id`),
  ADD KEY `dailyexpense_createdby_fk` (`created_by`),
  ADD KEY `dailyexpense_updatedby_fk` (`updated_by`),
  ADD KEY `dailyexpense_approvedby_fk` (`approved_by`),
  ADD KEY `dailyexpense_cancelledby_fk` (`cancelled_by`),
  ADD KEY `dailyexpense_company_fk3` (`company_id`),
  ADD KEY `dailyexpense_bank_fk1` (`bank`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `jp_pre_dailyvendors`
--
ALTER TABLE `jp_pre_dailyvendors`
  MODIFY `daily_v_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `jp_pre_dailyvendors`
--
ALTER TABLE `jp_pre_dailyvendors`
  ADD CONSTRAINT `dailyexpense_bank_fk1` FOREIGN KEY (`bank`) REFERENCES `jp_bank` (`bank_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `dailyexpense_cancelledby_fk` FOREIGN KEY (`cancelled_by`) REFERENCES `jp_users` (`userid`),
  ADD CONSTRAINT `dailyexpense_company_fk3` FOREIGN KEY (`company_id`) REFERENCES `jp_company` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `dailyexpense_createdby_fk` FOREIGN KEY (`created_by`) REFERENCES `jp_users` (`userid`),
  ADD CONSTRAINT `dailyexpense_project_fk` FOREIGN KEY (`project_id`) REFERENCES `jp_projects` (`pid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `dailyexpense_updatedby_fk` FOREIGN KEY (`updated_by`) REFERENCES `jp_users` (`userid`),
  ADD CONSTRAINT `dailyexpense_vendor_fk` FOREIGN KEY (`vendor_id`) REFERENCES `jp_vendors` (`vendor_id`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
