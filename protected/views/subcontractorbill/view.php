<?php
/* @var $this SubcontractorbillController */
/* @var $model Subcontractorbill */

$this->breadcrumbs = array(
    'Subcontractorbills' => array('index'),
    $model->id,
);

?>
<div class="container" id="vendors">
    <div class="header-container">
        <h3>Subcontractor Bill</h3>
        <div class="btn-container">
            <?php if ((isset(Yii::app()->user->role) && (in_array('/subcontractorbill/exporttopdf', Yii::app()->user->menuauthlist)))) { ?>
                <a href="<?php echo $this->createUrl('subcontractorbill/exporttopdf', array('id' => $model->id)); ?>"
                    class="btn btn-primary"><i class="fa fa-file-pdf-o button-icon" aria-hidden="true" title="SAVE AS PDF"></i></a>
            <?php } ?>
            <?php if ((isset(Yii::app()->user->role) && (in_array('/subcontractorbill/exporttoexcel', Yii::app()->user->menuauthlist)))) { ?>
                <a href="<?php echo $this->createUrl('subcontractorbill/exporttoexcel', array('id' => $model->id)); ?>"
                    class="btn btn-primary" title="SAVE AS EXCEL"><i class="fa fa-file-excel-o button-icon" aria-hidden="true"></i></a>
            <?php } ?>
        </div>
    </div>
    <div class="page_filter">
        <div class="row">
            <div class="col-sm-2">
                <div class="form-group">
                    <label><b>COMPANY:</b></label><br>
                    <?php echo $model->company->name; ?>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="form-group">
                    <label><b>PROJECT:</b></label><br>
                    <?php
                    echo Projects::model()->findByPk($model->quotation->project_id)->name;
                    ?>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="form-group">
                    <label><b>SUB CONTRACTOR:</b></label><br>
                    <?php
                    echo Subcontractor::model()->findByPk($model->quotation->subcontractor_id)->subcontractor_name;
                    ?>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="form-group">
                    <label><b>QUOTATION:</b></label><br>
                    <?php echo $model->quotation->scquotation_decription; ?>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="form-group">
                    <label><b>STAGE:</b></label><br>
                    <?php
                    if ($model->stage_id != "") {
                        $stage = ScPaymentStage::model()->findByPk($model->stage_id);
                        echo $stage['payment_stage'];
                    }

                    ?>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="form-group">
                    <label><b>BILL NUMBER:</b></label><br>
                    <?php echo $model->bill_number; ?>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="form-group">
                    <label><b>DATE:</b></label><br>
                    <?php echo date("d-m-Y", strtotime($model->date)); ?>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="form-group">
                    <label><b>REMARKS:</b></label><br>
                    <div class="text-left">
                        <?php echo $model->remarks; ?>
                    </div>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="form-group">
                    <label><b>REQUESTED BY:</b></label><br>
                    <div class="text-left">
                        <?php echo $model->requested_by; ?>
                    </div>
                </div>
            </div>
            <div class="col-sm-2" style="width:auto;">
                <div class="form-group">
                    <label><b>AMOUNT:</b></label><br>
                    <div class="text-right">
                        <?php echo Controller::money_format_inr($model->total_amount, 2); ?>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>