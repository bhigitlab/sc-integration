<?php
/* @var $this WithdrawalsController */
/* @var $model Withdrawals */
/* @var $form CActiveForm */
?>

<div class="modal-body">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'withdrawals-form',
	'enableAjaxValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true,
            'validateOnType' => false,),
)); ?>

	<div class="clearfix">
	<div class="">
		<div class="row addRow">					
		<div class="col-md-6">
		<?php //echo $form->errorSummary($model); ?>
        
       
		<?php echo $form->labelEx($model,'userid'); ?>
		<?php 

             $tblpx = Yii::app()->db->tablePrefix;
             if(Yii::app()->user->role==1){
                                $sql="SELECT `t`.`userid`, concat_ws(' ',`t`.`first_name`, `t`.`last_name`) as full_name,"
                                    . "`{$tblpx}user_roles`.`role` "
                                    . "FROM `{$tblpx}users` `t` JOIN {$tblpx}user_roles ON {$tblpx}user_roles.id= t.user_type "
                                    . "WHERE status=0  ORDER BY user_type,full_name ASC";
                }elseif(Yii::app()->user->role==2){
                            $sql="SELECT `t`.`userid`, concat_ws(' ',`t`.`first_name`, `t`.`last_name`) as full_name,"
                                    . "`{$tblpx}user_roles`.`role` "
                                    . "FROM `{$tblpx}users` `t` JOIN {$tblpx}user_roles ON {$tblpx}user_roles.id= t.user_type "
                                    . "WHERE status=0  AND userid IN (SELECT userid FROM {$tblpx}project_assign WHERE projectid in (SELECT projectid FROM {$tblpx}project_assign WHERE userid=".Yii::app()->user->id." ))  ORDER BY user_type,full_name ASC";
              }else{
                            $sql="SELECT `t`.`userid`, concat_ws(' ',`t`.`first_name`, `t`.`last_name`) as full_name,"
                                    . "`{$tblpx}user_roles`.`role` "
                                    . "FROM `{$tblpx}users` `t` JOIN {$tblpx}user_roles ON {$tblpx}user_roles.id= t.user_type "
                                    . "WHERE status=0  and t.userid=".Yii::app()->user->id."  ORDER BY user_type,full_name ASC";
              }   
               $result = Yii::app()->db->createCommand($sql)->queryAll();
            $listdata = CHtml::listData($result, 'userid', 'full_name','role');
                 echo $form->dropDownList($model, 'userid', $listdata, array('empty' => 'Select a user', 
                  'ajax' => array(
                      'type' => 'POST',
                     'dataType' => 'json',
                      'url' => CController::createUrl('projects/getprojects'),
                    //  'update'=>'#Withdrawals_projectid_new', 
                    'data' => array('userid' => 'js:this.value'),
                       'success' => 'function(data) {
                          //alert(data.projects);
                                $("#Withdrawals_projectid").html(data.projects);
                            }'

                      )));

        
                ?>
		<?php echo $form->error($model,'userid'); ?>
		</div>
        <div class="col-md-6">
            <?php echo $form->labelEx($model, 'projectid'); ?>
            <?php        
 
               if(isset($model->userid) && trim($model->userid)!=""){
 
                    echo $form->dropDownList($model, 'projectid', CHtml::listData(Projects::model()->findAll(array(

                             'select' => array('pid, name'),
                             "condition"=> 'pid in (select projectid from jp_project_assign where userid='.$model->userid.')',
                             'order' => 'name',
                             'distinct' => true
                         )), 'pid', 'name'), array('empty' => '-----------', ));	
               }
               else
               {/* CHtml::listData(Projects::model()->findAll(array(
                             'select' => array('pid, name'),                            
                             'order' => 'name',
                             'distinct' => true
                         )), 'pid', 'name') */
                    echo $form->dropDownList($model, 'projectid', array(), array('empty' => '-----------',));	
               }        


            ?>
            <?php echo $form->error($model, 'projectid'); ?>
			</div>
        <div class="col-md-6">
                <?php echo $form->labelEx($model, 'date'); ?>
                <?php echo CHtml::activeTextField($model, 'date', array('size'=>10,'value'=> (($model->isNewRecord) ? date('d-M-Y'): date('d-M-Y',strtotime($model->date))))); ?>
                    <?php
                    $this->widget('application.extensions.calendar.SCalendar', array(
                        'inputField' => 'Withdrawals_date',
                        'ifFormat' => '%d-%b-%Y',
                    ));
                    ?>
                <?php echo $form->error($model, 'date'); ?>
            </div>
        <div class="col-md-6">
                <?php echo $form->labelEx($model, 'amount'); ?>
                <?php echo $form->textField($model, 'amount',array('size'=>10)); ?>
                <?php echo $form->error($model, 'amount'); ?>
           </div>
          
        <div class="col-md-6">
                <?php echo $form->labelEx($model, 'type'); ?>
                <?php
                    echo $form->dropDownList($model, 'type', CHtml::listData(Status::model()->findAll(array(
                                        'select' => array('sid,caption'),
                                        'condition' => 'status_type="withdrawal"',
                                        'order' => 'caption',
                                        'distinct' => true
                                    )), 'sid', 'caption'), array('empty' => 'Choose a type'));
                    ?>
                <?php echo $form->error($model, 'type'); ?>
            </div>
        <div class="col-md-6">
                <?php echo $form->labelEx($model, 'details'); ?>
                <?php
                    echo $form->dropDownList($model, 'details', CHtml::listData(Status::model()->findAll(array(
                                        'select' => array('sid,caption'),
                                        'condition' => 'status_type="withdrawal_details"',
                                        'order' => 'caption',
                                        'distinct' => true
                                    )), 'sid', 'caption'), array('empty' => 'Choose a type'));
                    ?>
                <?php echo $form->error($model, 'details'); ?>
           </div>
        <div class="col-md-12"> 
            <?php echo $form->labelEx($model, 'description'); ?>
            <?php echo $form->textArea($model, 'description', array('rows' => 6, 'cols' =>40)); ?>
            <?php echo $form->error($model, 'description'); ?>
        
		</div>
	</div>
	
</div>
</div>
<div class="modal-footer save-btnHold">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
		 <?php if(!$model->isNewRecord){?> 
			<?php if(Yii::app()->user->role==1){?> 
 <a class="btn del-btn" href="<?php echo $this->createUrl('withdrawals/deletewithdrawal', array('id' => $model->exp_id )) ?>" class="deletebtn" 
							onclick="return confirm('Are you sure that you want to delete this?');">Delete </a>   
		 <?php } ?>   	         
		 <?php } ?>                		 
		<button data-dismiss="modal" onclick="javascript:window.location.reload()">Close</button>
	</div>
<?php $this->endWidget(); ?>
</div>