<?php
$units = Unit::model()->findAll();
?>


<div class="purchase_items" id="Add_purchase_form">
    <div class="row">
        <div class="col-xs-12">
            <div class="heading-title">Additional Items</div>
            <div class="dotted-line"></div>
        </div>
    </div>

    <div class="row">
        <div class="form-group col-xs-12 col-md-2">
            <div id="select_div">
                <label>Purchase Item: <small id="item_baseunit_data" class="d-none text-blue font-weight-normal">(Base
                        Unit- <span class="item_unit_span" id="item_unit_additional_data"> </span>)</small></label>
                <select class="form-control" id="additional_item" name="additional_item">
                    <option value="">Select one</option>
                    <?php
                    foreach ($specification as $key => $value) {
                        ?>
                        <option value="<?php echo $value['id']; ?>">
                            <?php echo $value['data']; ?>
                        </option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <div class="form-group col-xs-12 col-md-2 remark">
            <div id="select_div">
                <label>Remarks:</label>
                <input type="text" class="form-control" id="remarks" name="remark[]" placeholder="Remark" />
            </div>
        </div>

        <div class="form-group col-xs-12 col-md-2">
            <div id="select_div">
                <label>Batch:</label>
                <input type="text" class="form-control" id="bibatch" name="bibatch" placeholder="Batch" />
            </div>
        </div>
        <div class="form-group col-xs-12 col-md-2">
            <label>Units/Size: <small id="item_conversion_data" class="d-none text-blue font-weight-normal">(Conv
                    Fact-<span class="item_unit_span" id="item_unit_additional_conv"> </span>)</small></label>
            <select class="form-control item_unit basedata" id="item_unit_additional_purchase" name="unit_purchase">
                <option value="">Unit</option>

                <?php
                if (!empty($units)) {
                    foreach ($units as $unit) {
                        echo '<option value="' . $unit['unit_name'] . '">' . $unit['unit_name'] . '</option>';
                    }
                }
                ?>
            </select>
            <input type="hidden" id="additional_item_base_unit" class="additional_item_base_unit" value=""
                name="additional_item_base_unit" />
        </div>

        <div class="form-group col-xs-12 col-md-2 quantity_div">
            <label>Quantity:</label>
            <input type="text" class="inputs target quantity allownumericdecimal form-control" id="quantity"
                name="quantity[]" value="0" placeholder="" />
        </div>
        <div class="form-group col-xs-12 col-md-2 quantity_div">
            <label>Rate:</label>
            <input type="text" class="inputs target rate additional_rate allownumericdecimal form-control" id="rate"
                value="0" name="rate" placeholder="" />
        </div>

        <div class="form-group col-xs-12 col-md-2 basequantity base-data">
            <div id="select_div">
                <label>Base Quantity:</label>
                <input type="text" class="inputs target quantity allownumericdecimal form-control basedata"
                    id="basequantity" name="basequantity" placeholder="Base Qty" />
            </div>
        </div>
        <div class="form-group col-xs-12 col-md-2 baseunit base-data">
            <div id="select_div">
                <label>Base unit:</label>
                <input type="text" class="inputs target quantity allownumericdecimal form-control basedata"
                    id="item_unit_additional" name="unit" placeholder="Base Unit" readonly />
            </div>
        </div>
        <div class="form-group col-xs-12 col-md-2 baserate base-data">
            <div id="select_div">
                <label>Base Rate:</label>
                <input type="text" class="inputs target quantity allownumericdecimal form-control basedata"
                    id="baserate" name="baserate" placeholder="Base Rate" readonly />
            </div>
        </div>

        <div class="form-group col-xs-12 col-md-2 quantity_div">
            <label>Amount:</label>
            <input type="text" class="inputs target amount allownumericdecimal form-control" id="amount" value="0"
                name="amount" placeholder="" style="width:80px;" />
        </div>
        <input type="hidden" class="inputs target jono_additional form-control" id="jono_additional"
            name="jono_additional" placeholder="" />
    </div>
    <div class="row">
        <div class="form-group col-xs-12 text-right">
            <button type="button" class="btn btn-primary item_save" id="0">Add</button>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $("#additional_item").select2();
        $('#additional_item').change(function () {
            var category_id = $("#additional_item").val();
            $('.base-data').hide();
            if (category_id == 'other') {
                $('.remark').css("display", "inline-block");
                $('#remarks').focus();
            } else if (category_id == '') {
                $('.js-example-basic-single').select2('focus');
                $('.remark').css("display", "none");
            } else {
                $('#quantity').focus();
                $('.remark').css("display", "none");
            }

            if (category_id != '') {

                $.ajax({
                    url: '<?php echo Yii::app()->createAbsoluteUrl('wh/warehousereceipt/getUnits'); ?>',
                    type: 'GET',
                    dataType: 'json',
                    data: {
                        data: category_id
                    },
                    success: function (result) {

                        if (result.status == 1) {

                            $("#additional_item_base_unit").val(result.base_unit);
                            if (result.base_unit_name != '') {
                                $("#item_baseunit_data").show();
                                $("#item_unit_additional_data").html(result.base_unit_name);
                            }
                            $("#item_unit_additional_span").html(result.base_unit_name);
                            $("#item_unit_additional_purchase").val(result.base_unit);
                            $("#item_unit_additional").val(result.base_unit);
                            $('select[id="item_unit_additional_purchase"]').empty();
                            if (result.unit == result.base_unit) {
                                $('.base-data').hide();
                                $('select[id="item_unit_additional_purchase"]').append('<option value="' + result.unit + '" selected>' + result.units + '</option>');
                            } else {

                                $.each(result["unit"], function (key, value) {
                                    if (result.base_unit == value.value) {
                                        var selected = 'selected';
                                    } else {
                                        var selected = '';
                                    }
                                    $('select[id="item_unit_additional_purchase"]').append('<option value="' + value.id + '" ' + selected + '>' + value.value + '</option>');
                                });
                            }
                        }
                        $('#quantity').focus();
                    }
                });
            } else {

                $.ajax({
                    url: '<?php echo Yii::app()->createAbsoluteUrl('wh/warehousereceipt/getAllUnits'); ?>',
                    type: 'GET',
                    data: {
                        data: category_id
                    },
                    success: function (result) {
                        $("#item_unit_additional_purchase").html(result);
                    }
                });
            }
        });

        $('#item_unit_additional_purchase').change(function () {
            getconversionfactor();
        });
        $('#quantity,#rate').change(function () {
            getconversionfactor();
        });

        function getconversionfactor() {

            var itemId = parseInt($("#additional_item").val());
            var base_unit = $("#item_unit_additional").val();
            var quantity = parseFloat($("#quantity").val());
            quantity = isNaN(quantity) ? 0 : quantity;
            var purchase_unit = $("#item_unit_additional_purchase option:selected").text();
            quantity = quantity.toFixed(4);
            var base_quantityold = parseFloat($("#basequantity").val());
            var baserate = 0;
            var rate = $("#rate").val();


            if (purchase_unit !== '' && base_unit !== '' && itemId !== "") {

                if (base_unit != purchase_unit) {
                    $('.base-data').show();
                    $.ajax({
                        method: "POST",
                        data: {
                            'purchase_unit': purchase_unit,
                            'base_unit': base_unit,
                            'item_id': itemId
                        },
                        dataType: "json",
                        url: '<?php echo Yii::app()->createAbsoluteUrl('warehousereceipt/getUnitconversionFactor'); ?>',
                        success: function (result) {
                            var amt = $("#amount").val();
                            if (result != '') {
                                $("#item_conversion_data").show();
                                $("#item_unit_additional_conv").html(result);
                            }
                            var baseqty = result * quantity;
                            if (isNaN(baseqty))
                                baseqty = 0;
                            if (baseqty != 0) {
                                baserate = amt / baseqty;
                                baserate = baserate.toFixed(2);
                                if (isNaN(baserate))
                                    baserate = 0;
                            }

                            $("#basequantity").val(baseqty);
                            $("#baserate").val(baserate);

                        }
                    });
                } else {
                    $("#item_conversion_data").hide();
                    $('.base-data').hide();
                    var baseval = 0;
                    $("#basequantity").val(quantity);
                    $("#baserate").val(rate);
                }
            }
        }




        $('#basequantity').blur(function () {

            var itemId = parseInt($("#additional_item").val());
            var purchase_unit = $("#item_unit_additional_purchase option:selected").text();
            var base_unit = $("#item_unit_additional").val();
            var basequantity = parseFloat($("#basequantity").val());
            var amt = $("#amount").val();
            if (purchase_unit !== '' && base_unit !== '' && itemId !== "") {
                if (base_unit != purchase_unit) {
                    base_quantity = basequantity;
                    if (isNaN(base_quantity))
                        base_quantity = 0;
                    if (base_quantity != 0) {
                        baserate = amt / base_quantity;
                        baserate = baserate.toFixed(2);
                        if (isNaN(baserate))
                            baserate = 0;
                    }
                    $("#basequantity").val(base_quantity);
                    $("#baserate").val(baserate);
                }

            }

        });

    });
</script>