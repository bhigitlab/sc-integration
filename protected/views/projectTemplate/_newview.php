<?php
/* @var $this ProjectsController */
/* @var $data Projects */
?>


<?php 
if ($index == 0) {
    ?>

    <thead>
        <tr>
            <th style="width:40px;">Sl No.</th>
            <th>Name</th>
            <th>Description</th>
     
            <th>Status</th>            
            <?php
            if(isset(Yii::app()->user->role) && (in_array('/projectTemplate/update', Yii::app()->user->menuauthlist))){
            ?>
            <th  style="width:40px;">Action</th>
            <?php } ?>
        </tr>   
    </thead>
            <?php
        }
        ?>
        
        <tr>
            <td style="width:40px;"><?php echo $index + 1; ?></td>
            <td><?php echo CHtml::encode($data->template_name); ?></td>
            <td><?php echo CHtml::encode($data->description); ?></td>

            <td><?php echo CHtml::encode($data->status)? "Active" : "Inactive"; ?></td>
            
            <td  style="width:40px;">
            <?php
            if(isset(Yii::app()->user->role) && (in_array('/projectTemplate/update', Yii::app()->user->menuauthlist))){
            ?>
                <a class="fa fa-edit editTemplate"  data-id="<?php echo $data->template_id; ?>" onclick="editTemplate(<?php echo $data->template_id; ?>)"></a>
            <?php } ?>   
                <?php //echo $form->checkBox($value, 'status', array('class' => 'template_check', 'data-id' => $value->template_id)); ?>
        
                <input id="ytProjectTemplate_status" type="hidden" value="0" name="ProjectTemplate[status]">
                <?php
                if(isset(Yii::app()->user->role) && (in_array('/dashboard/changeProjectTemplate', Yii::app()->user->menuauthlist))){
                ?>
                    <input class="project_template_check" data-id="<?php echo $data->template_id; ?>" name="ProjectTemplate[status]" id="ProjectTemplate_status" value="1" type="checkbox" <?php echo ($data->status) ? "checked" : '';?>> 
                <?php } ?>  

            </td>
          
        </tr>
		

