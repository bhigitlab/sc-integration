<div id="addremark" class="panel panel-gray" style="display:none;position: fixed;top: 15%; bottom: 25%;right: 0px; z-index: 6;width:500px; margin:0 auto;background-color: #fff;height: 450px;
    overflow: auto;">
        <div class="panel-heading form-head">
            <h3 class="panel-title">Remark For Purchase No: <span class="po_no"></span></h3>
            <button type="button" onclick="closeaction(this)" class="close" data-dismiss="alert" aria-label="Close" ><span aria-hidden="true">×</span></button>
        </div>
        <div class="panel-body">
            <div class="clearfix">
                <a class="addnow" style="display:none;">Add Remark</a>
                <div class="remarkadd">
                    
                    <div class="form-group">
                        <label>Add Remark</label>
                        <textarea type="text" name="remark" id="remark" class="form-control"></textarea>
                    </div> 
                    <div class="text-right">
                        <input type="submit" value="Save" id="remarkSubmit" class="btn btn-sm btn-info"/>
                        <input type="button" value="Close" class="closermrk btn  btn-sm btn-default" style="display:none;"/>
                    </div>
                </div>
                <input type="hidden" name="txtPurchaseId" id="txtPurchaseId" value=""/>
                <h4 class="head_remarks">Remarks</h4>
                <div class="all_remarks">
                    <div id="remarkList">
                    </div>
                </div>
                <br>
                
                <div class="decline_comment">
                    
                </div>
                <br>
            </div>
        </div>
    </div>