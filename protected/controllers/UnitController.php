<?php

class UnitController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		/* return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update', 'newlist', 'checkunit'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);*/
            
            $accessArr = array();
           $accessauthArr = array();
           $controller = Yii::app()->controller->id;
           if(isset(Yii::app()->user->menuall)){
               if (array_key_exists($controller, Yii::app()->user->menuall)) {
                   $accessArr = Yii::app()->user->menuall[$controller];
               } 
           }

           if(isset(Yii::app()->user->menuauth)){
               if (array_key_exists($controller, Yii::app()->user->menuauth)) {
                   $accessauthArr = Yii::app()->user->menuauth[$controller];
               }
           }
           $access_privlg = count($accessauthArr);
           return array(
               array('allow', // allow all users to perform 'index' and 'view' actions
                   'actions' => $accessArr,
                   'users' => array('@'),
                   'expression' => "$access_privlg > 0",
               ),

               array('allow', // allow admin user to perform 'admin' and 'delete' actions
                   'actions' => $accessauthArr,
                   'users' => array('@'),
                   'expression' => "$access_privlg > 0",
               ),
               array('deny',  // deny all users
                   'users'=>array('*'),
               ),
           );
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}
	
	
	public function actionCheckunit()
	{
		$tblpx = Yii::app()->db->tablePrefix;
	   $specification = Yii::app()->db->createCommand("SELECT id FROM {$tblpx}unit WHERE unit_name ='".$_REQUEST['unit']."' AND company_id=".Yii::app()->user->company_id."")->queryRow();
	   if($specification) {
		echo 'false';
			} else {
				echo 'true';
			}
	}
	
	
	public function actionNewlist()
	{
		$model = new Unit();
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Unit']))
			$model->attributes=$_GET['Unit'];
			
		$this->render('newlist', array(
			'model' => $model,
		   'dataProvider' => $model->search(),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		
		if (isset($_REQUEST['data'])) {
			if($_REQUEST['id']==""){
				$model=new Unit;
				$this->performAjaxValidation($model);
				$model->unsetAttributes();
				$model->unit_name 	 = $_REQUEST['data'];
				$model->created_by = Yii::app()->user->id;
				$model->created_date =  date('Y-m-d');
				$model->company_id   = Yii::app()->user->company_id;
			}else{
				$model=$this->loadModel($_REQUEST['id']);
				$this->performAjaxValidation($model);
				$model->unit_name 	 = $_REQUEST['data'];
			}
            if ($model->save()) {
				$pms_api_integration=ApiSettings::model()->pmsIntegrationStatus();
				
				if ($_REQUEST['executeApiValue'] == 1) {
					if($pms_api_integration ==1){
						$request = [
							'origin' => 'coms',
							'unit_id' =>$model->id,
							'name' => $model->unit_name,
							'coms_api_log_id' => $model->id,
						];
						if (!empty($model->pms_unit_id )) {
							$slug="api/update-unit";
							$request['pms_unit_id'] = $model->pms_unit_id;
						} else {
							$slug="api/create-unit";
						}
						
						$method="POST";
						$globalapi_response = GlobalApiCall::callApi($slug, $method, $request);
					}
			    }
				$model=new Unit;
				echo $this->renderPartial("_ajaxlist", array('model' => $model, 'dataProvider' => $model->search()));
			} 
        }
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Unit']))
		{
			$model->attributes=$_POST['Unit'];
			$model->company_id   = Yii::app()->user->company_id;
			if($model->save())
			{
				$pms_api_integration=ApiSettings::model()->pmsIntegrationStatus();
				
				if ($_POST['executeApiValue'] == 1) {
					if($pms_api_integration ==1){
						$request = [
							'origin' => 'coms',
							'unit_id' =>$model->id,
							'name' => $model->unit_name,
							'coms_api_log_id' => $model->id,
							'pms_unit_id'=>$model->pms_unit_id,
						];
						
						if (!empty($model->pms_unit_id )) {
							$slug="api/update-unit";
							$request['pms_unit_id'] = $model->pms_unit_id;
						} else {
							$slug="api/create-unit";
						}
						
						$method="POST";
							
						$globalapi_response = GlobalApiCall::callApi($slug, $method, $request);
					}
				}
				$this->redirect(array('view','id'=>$model->id));
			}	
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new Unit('search');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Unit('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Unit']))
			$model->attributes=$_GET['Unit'];

		$this->render('admin',array(
			'model'=>$model,  'dataProvider' => $model->search(Yii::app()->user->id),
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Unit the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Unit::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Unit $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='unit-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
