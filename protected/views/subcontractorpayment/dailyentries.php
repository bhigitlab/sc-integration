<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.js"></script>
<?php
$this->breadcrumbs = array(
    'Subcontractors' => array('index'),
    $model->subcontractor_id => array('view', 'id' => $model->subcontractor_id),
    'Update',
);
$user = Users::model()->findByPk(Yii::app()->user->id);
$arrVal = explode(',', $user->company_id);
$newQuery1 = "";
$newQuery = "";
foreach ($arrVal as $arr) {
    if ($newQuery1) $newQuery1 .= ' OR';
    if ($newQuery) $newQuery .= ' OR';
    $newQuery1 .= " FIND_IN_SET('" . $arr . "', company_id)";
    $newQuery .= " FIND_IN_SET('" . $arr . "', id)";
}
$quotationQuery = 'approve_status = "Yes" AND scquotation_no IS NOT NULL';
?>
<style type="text/css">
    .pending_reconil{
        background-color: #5bc0de3b;
        border: 1px solid #5bc0de3b;
        width: 12px;
        height: 12px; 
        margin-right:10px;
    }
    *:focus {
        border: 1px solid #333;
        box-shadow: 0 0 6px 1px rgba(0, 0, 0, 0.25);
    }

    .permission_style {
        background-color: #f8cbcb;
    }

    .permission_style td:first-child {
        background-color: #fff;
    }

    .tooltip-hiden {
        width: auto;
    }

    .row_block .elem_block.gsts input {
        width: 60px;
        display: inline-block;
    }

    .row_block .elem_block.amountfld input {
        width: 100px;
    }

    #amountdetails label {
        height: 30px !important;
        font-size: 10px !important;
    }
    .stage_amount{
        border:none;
    }
    .stage_amount:focus{
        box-shadow:none !important;
    }

    @media(min-width: 992px) {
        .amounts {
            width: 90px;
        }
    }

    .vertical-td {
        vertical-align: middle !important;
    }

    .deleteclass {
        background-color: #efca92;
    }

    #SubcontractorPayment_payment_quotation_status label {
        display: inline-block;
    }
    .allpaylist{
        margin-top:15px;
        margin-left:5px;
    }
</style>
<div class="container" id="expense">
    <div class="loading-overlay">
        <span class="fa fa-spinner fa-3x fa-spin"></span>
    </div>
    <div class="expenses-heading">
        <div class="clearfix">
            <?php if ((isset(Yii::app()->user->role) && (in_array('/subcontractorpayment/paymentcreate', Yii::app()->user->menuauthlist)))) { ?>
                <a  class="btn btn-info pull-right  allpaylist" href="index.php?r=subcontractorpayment/allentries" id="dailyalllist">All Entries</a>
              
                <button type="button" class="btn btn-info pull-right addentries collapsed" data-toggle="collapse" data-target="#dailyform" id="dailyformbutton"></button>
                 <?php } ?>
            <h2>SUBCONTRACTOR PAYMENT ENTRIES</h2>
            <div id="loading"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/ajax-loader.gif"></div>
        </div>
    </div>
    <div class="daybook form" id="daybookform">
        <?php $form = $this->beginWidget('CActiveForm', array(
            'id' => 'dailyexpense-form',
            'enableAjaxValidation' => false,
        )); ?>
        <div class="clearfix">
            <div class="datepicker">
                <div class="page_filter clearfix">
                    <div class="filter_elem">
                        <label>Entry Date: </label>
                        <?php echo CHtml::activeTextField($model, 'date', array('style' => 'height:25px;', 'size' => 10, 'id' => 'SubcontractorPayment_date', 'value' => date('d-m-Y'), 'onchange' => 'changedate()', 'class' => 'form-control')); ?>
                    </div>
                    <div class="filter_elem links_hold">
                        <a href="#" id="previous" class="link">Previous</a> |
                        <a href="#" id="current" class="link">Current</a> |
                        <a href="#" id="next" class="link">Next</a>
                    </div>
                    <?php
                    $tblpx        = Yii::app()->db->tablePrefix;
                    $expenseData  = Yii::app()->db->createCommand("select MAX(date) as entry_date FROM " . $tblpx . "subcontractor_payment")->queryRow();
                    ?>
                    <div class="pull-right"><span id="lastentry">Last Entry date : <span id="entrydate"><?php echo date("d-m-Y", strtotime($expenseData["entry_date"])); ?></span></span></div>
                </div>
            </div>
        </div>
        <div class="">
            <div id="errormessage"></div>
        </div>
        <div class="block_hold shdow_box clearfix collapse" id="dailyform">
            <div>
                <h4 class="section_title">Details</h4>
                <div class="row">
                    <div class="col-md-2 col-sm-6">
                        <div class="form-group">
                            <label for="vendor">Company</label>
                            <?php
                            echo $form->dropDownList($model, 'company_id', CHtml::listData(Company::model()->findAll(array(
                                'select' => array('id, name'),
                                'order' => 'name ASC',
                                'condition' => '(' . $newQuery . ')',
                                'distinct' => true,
                            )), 'id', 'name'), array('class' => 'form-control js-example-basic-single', 'style' => 'width: 100%;', 'empty' => '-Select Company-'));
                            ?>

                        </div>
                    </div>
                    <div class="col-md-2 col-sm-6">
                        <div class="form-group">
                            <label for="vendor">Subcontractor</label>
                            <?php
                            echo $form->dropDownList($model, 'subcontractor_id', CHtml::listData(Subcontractor::model()->findAll(array(
                                'select' => array('subcontractor_id, subcontractor_name'),
                                'order' => 'subcontractor_name ASC',
                                'condition' => '(' . $newQuery1 . ')',
                                'distinct' => true,
                            )), 'subcontractor_id', 'subcontractor_name'), array('class' => 'form-control js-example-basic-single', 'style' => 'width: 100%;', 'empty' => '-Select Subcontractor-'));
                            ?>
                        </div>
                    </div>
                    <div class="col-md-1 col-sm-3 gsts">
                        <div class="form-group">
                            <label for="cheque">SGST %</label>
                            <input type="text" class="form-control amountvalidation percentage" id="SubcontractorPayment_sgst" name="SubcontractorPayment[sgst]" />
                        </div>
                    </div>
                    <div class="col-md-1 col-sm-3 gsts">
                        <div class="form-group">
                            <label for="cheque">CGST %</label>
                            <input type="text" class="form-control amountvalidation percentage" id="SubcontractorPayment_cgst" name="SubcontractorPayment[cgst]" />
                        </div>
                    </div>
                    <div class="col-md-1 col-sm-3 gsts">
                        <div class="form-group">
                            <label for="cheque">IGST %</label>
                            <input type="text" class="form-control amountvalidation percentage" id="SubcontractorPayment_igst" name="SubcontractorPayment[igst]" />
                        </div>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
            <input type="hidden" name="txtRows" id="txtRows" value="1">
            <input type="hidden" name="txtCurrentRows" id="txtCurrentRows" value="1">
            <input type="hidden" name="txtCount" id="txtCount" value="0">
            <div class="label-height-0" id="amountdetails">
                <div class="daybook-inner paymentrow" id="1">
                    <h4 class="section_title">Amount Details</h4>

                    <div class="project_items" id="project_items1">
                        <div class="row">
                            <div class="col-md-2 col-sm-3">
                                <div class="form-group">
                                    <label for="vendor nowrap" style="white-space:nowrap">Payment Against Quotation</label>
                                    <?php
                                    echo $form->dropDownList($model, 'project_id', array('1' => 'Yes', '2' => 'No'), array('class' => 'form-control payment_against_qtn_status', 'style' => 'width: 100%;',  'id' => 'SubcontractorPayment_payment_quotation_status1', 'data-id' => 1));
                                    ?>
                                </div>
                            </div>
                            <div class="col-md-2 col-sm-6">
                                <div class="form-group">
                                    <label for="vendor">Project</label>
                                    <?php
                                    echo $form->dropDownList($model, 'project_id', CHtml::listData(Projects::model()->findAll(array(
                                        'select' => array('pid, name'),
                                        'order' => 'name ASC',
                                        'condition' => '(' . $newQuery1 . ')',
                                        'distinct' => true,
                                    )), 'pid', 'name'), array('class' => 'form-control js-example-basic-single singleproject project_list', 'style' => 'width: 100%;', 'empty' => '-Select Project-', 'id' => 'SubcontractorPayment_project_id1', 'data-id' => 1));
                                    ?>
                                </div>
                            </div>
                            <div class="col-md-2 col-sm-6 qtn_number_div">
                                <div class="form-group">
                                    <label for="receiptType" class="wrap">Quotation No</label>
                                    <?php
                                    echo $form->dropDownList($model, 'quotation_number', 
                                            CHtml::listData(Scquotation::model()->findAll(array(
                                        'select' => array('scquotation_id, scquotation_no'),
                                        'order' => 'scquotation_no ASC',
                                        'condition' => '(' . $quotationQuery . ')',
                                        'distinct' => true,
                                    )), 'scquotation_id', 'scquotation_no'), array('class' => 'sc_quotation_number form-control js-example-basic-single qtn_list', 'id' => 'SubcontractorPayment_quotation_number1', 'style' => 'width: 100%;', 'empty' => '-Select Quotation Number-'));
                                    ?>
                                </div>
                            </div>
                            <div class="col-md-1 col-sm-3 payment_stage_div hide" data-count="0">
                                <div class="form-group">
                                    <label for="receiptType" class="wrap">Payment Stage</label>
                                    <?php
                                    echo $form->dropDownList($model, 'stage_id', 
                                            CHtml::listData(ScPaymentStage::model()->findAll(array(
                                        'select' => array('id, payment_stage'),
                                        'order' => 'id ASC',                                        
                                        'distinct' => true,
                                    )), 'id', 'payment_stage'), array('class' => 'sc_payment_stage form-control js-example-basic-single stage_list', 'id' => 'SubcontractorPayment_stage1', 'style' => 'width: 100px;', 'empty' => '-Select Payment Stage-'));
                                    ?>
                                    Balance:
                                    <input type="text" class="stage_amount" readonly/>
                                </div>
                            </div>
                            <div class="col-md-1 col-sm-3 amountfld">
                                <div class="min-height-65px form-group">
                                    <label>Amount</label>
                                    <input type="text" class="form-control amountvalidation sub_paymentamount" id="SubcontractorPayment_amount1" name="SubcontractorPayment[amount]" data-id='1'  autocomplete="off"/>
                                </div>
                            </div>
                            <div class="col-md-1 col-sm-3 gsts amounts">
                                <div class="min-height-65px form-group">
                                    <label class="d-block">SGST</label>
                                    <span class="gstvalue txtSgst" id="txtSgst1"></span>
                                </div>
                            </div>
                            <div class="col-md-1 col-sm-3 gsts amounts">
                                <div class="min-height-65px form-group">
                                    <label class="d-block">CGST</label>
                                    <span class="gstvalue txtCgst inlineblock" id="txtCgst1" ></span>
                                </div>
                            </div>
                            <div class="col-md-1 col-sm-3 gsts amounts">
                                <div class="min-height-65px form-group">
                                    <label class="d-block">IGST</label>
                                    <span class="gstvalue txtIgst" id="txtIgst1"></span>
                                </div>
                            </div>
                            <div class="col-md-1 col-sm-3 amounts">
                                <input type="hidden" class="form-control amountvalidation" readonly="true" id="SubcontractorPayment_tax_amount" name="SubcontractorPayment[tax_amount]" />
                                <div class="min-height-65px form-group">
                                    <label class="d-block">Total Tax</label>
                                    <span class="gstvalue txtgstTotal" id="txtgstTotal1"></span>
                                </div>
                            </div>
                            <div class="col-md-2 col-sm-3 amountlong">
                                <div class="min-height-65px form-group">
                                    <label class="d-block">Total Amount</label>
                                    <span class="gstvalue txtTotal" id="txtTotal1"></span>
                                </div>
                            </div>
                            <div class="col-md-1 col-sm-3 gsts">
                                <div class="form-group">
                                    <label>TDS (%)</label>
                                    <input type="text" class="form-control amountvalidation percentage tdsrate" id="SubcontractorPayment_tds1" name="SubcontractorPayment[tds]" data-id='1' />
                                    <span class="gstvalue txtTds" id="txtTds1"></span>
                                    <input type="hidden" class="form-control amountvalidation percentage" id="SubcontractorPayment_tds_amount1" name="SubcontractorPayment[tds_amount]" data-id='1' />
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-3 amountlong">
                                <div class="form-group">
                                    <label>Amount To Subcontractor</label>
                                    <span class="gstvalue txtPaidAmount" id="txtPaidAmount1"></span>
                                    <input type="hidden" class="form-control amountvalidation percentage" id="SubcontractorPayment_paidamount1" name="SubcontractorPayment[paidamount]" data-id='1' />
                                </div>
                            </div>
                            <input type="hidden" class="permission" name="SubcontractorPayment[approve_status]" id="txtPermission1" value="" />
                            <input type="hidden" class="paymentid 65656" name="SubcontractorPayment[payment_id]" id="Payment_id1" value="" />
                        </div>
                    </div>
                    <div class="addmoresection"></div>
                    <div id="errorMessage1" class="rowerror errorMessage"></div>
                </div>
                <div class="mt">
                    <span style="font-weight:bold;">Grand Total: </span>
                    <span class="gstvalue" style="display:inline-block;" id="txtgrandtot">0.00</span>&nbsp;&nbsp;
                    <span style="font-weight:bold;">Total TDS: </span>
                    <span class="gstvalue" style="display:inline-block;" id="txttdsamount">0.00</span>&nbsp;&nbsp;
                    <span style="font-weight:bold;">Total Amount To Subcontractor: </span>
                    <span class="gstvalue" style="display:inline-block;" id="txtamountPaid">0.00</span>
                </div>
            </div>
            <div class="daybook-inner">
                <h4 class="section_title">Transaction Details</h4>
                <div class="row">
                    <div class="col-md-2 col-sm-6">
                        <div class="form-group">
                            <label for="receiptType">Transaction Type</label>
                            <?php
                            echo $form->dropDownList($model, 'payment_type', CHtml::listData(Status::model()->findAll(array(
                                'select' => array('sid, caption'),
                                "condition" => 'status_type = "payment_type"',
                                'order' => 'sid',
                                'distinct' => true,
                            )), 'sid', 'caption'), array('class' => 'form-control js-example-basic-single', 
                            'style' => 'width: 100%;', 'empty' => '-Select Transaction Type-'));
                            
                            ?>
                        </div>
                    </div>
                    <div class="col-md-2 employee_box">
                        <div class="form-group">
                            <label for="bank">Petty Cash User</label>
                            <?php
                            echo $form->dropDownList($model, 'employee_id', CHtml::listData(Users::model()->findAll(array(
                                                'select' => array('userid, concat_ws(" ",first_name,last_name) as first_name'),
                                                'order' => 'first_name ASC',
                                                'condition' => '(' . $newQuery1 . ') AND user_type NOT IN (1)',
                                                'distinct' => true,
                                            )), 'userid', 'first_name'), array('class' => 'form-control js-example-basic-single', 'empty' => '-Select Employee-', 'style' => 'width:100%'));
                            ?>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-6 hiden_box" style="display:none;">
                        <div class="form-group">
                            <label for="bank">Bank</label>
                            <?php
                            echo $form->dropDownList($model, 'bank', CHtml::listData(Bank::model()->findAll(array(
                                'select' => array('bank_id, bank_name'),
                                'order' => 'bank_name ASC',
                                'condition' => '(' . $newQuery1 . ')',
                                'distinct' => true,
                            )), 'bank_id', 'bank_name'), array('class' => 'form-control js-example-basic-single', 'style' => 'width: 100%;', 'empty' => '-Select Bank-'));
                            ?>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 hiden_box" style="display:none;">
                        <div class="form-group">
                            <label for="cheque">Cheque No/ Transaction ID</label>
                            <input type="text" class="form-control" id="SubcontractorPayment_cheque_no" name="SubcontractorPayment[cheque_no]" />
                            <div class="chq_error text-danger"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <h4 class="section_title">Other Details</h4>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea type="text" class="form-control" id="SubcontractorPayment_description" name="SubcontractorPayment[description]"></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <input type="hidden" name="SubcontractorPayment[txtPaymentId]" value="" id="txtPaymentId" />
                <div class="submit-button text-right">
                    <div class="form-group">
                        <button type="button" class="btn btn-info" id="buttonsubmit">ADD</button>
                        <button type="button" class="btn btn-default" id="btnReset">RESET</button>
                    </div>
                </div>
            </div>
        </div>
        <input type="hidden" name="SubcontractorPayment[approve_status]" id="approve_status" value="Yes">
        <?php $this->endWidget(); ?>
    </div>
    <div class="daybook">
        <div class="">
            <div id="daybook-entry">
                <ul class="legend">
                    <li><span class="approval_needed"></span> Approval Needed</li>
                    <li><span class="declined"></span> Declined</li>
                    <li><span class="pending_reconil"></span> UnReconciled</li> 
                </ul>
                <?php $this->renderPartial('dailyentries_list', array('newmodel' => $newmodel,)); ?>
            </div>
        </div>
    </div>
</div>
<?php $projectUrl   = Yii::app()->createAbsoluteUrl("subcontractorpayment/dynamicProject"); ?>
<?php $expenseProjects   = Yii::app()->createAbsoluteUrl("subcontractorpayment/expenseProject"); ?>
<?php $subcontractorUrl   = Yii::app()->createAbsoluteUrl("subcontractorpayment/dynamicSubcontractor"); ?>
<?php $getUrl      = Yii::app()->createAbsoluteUrl("subcontractorpayment/getDataByDate"); ?>
<?php $payment_url  = Yii::app()->createAbsoluteUrl("subcontractorpayment/getTotalFromPayment"); ?>
<?php $getSubContractor  = Yii::app()->createAbsoluteUrl("subcontractorpayment/getPaymentDetails"); ?>
<?php $getProjectRow  = Yii::app()->createAbsoluteUrl("subcontractorpayment/getProjectRow"); ?>
<?php $validateSubcontractorLimit = Yii::app()->createAbsoluteUrl("subcontractorpayment/validateSubcontractorLimit"); ?>
<?php $addSubcontractorPayment = Yii::app()->createAbsoluteUrl("subcontractorpayment/addSubcontractorPayment"); ?>
<?php $updateSubcontractorPayment = Yii::app()->createAbsoluteUrl("subcontractorpayment/updateSubcontractorPayment"); ?>
<?php $quotationListUrl   = Yii::app()->createAbsoluteUrl("subcontractorpayment/getQuotations"); ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-dateFormat/1.0/jquery.dateFormat.js"></script>
<?php
$company = Company::model()->findByPk(Yii::app()->user->company_id);
?>
<?php if (Yii::app()->user->hasFlash('success')) : ?><br>
    <div class="example1consolesucces">
        <?php echo Yii::app()->user->getFlash('success'); ?>
    </div>
<?php endif; ?>
<script>
    var secionNode = '<div class="row_block sdfsdfsdf">' +
        '<div class="elem_block">' +
        '<div class="form-group">' +
        '<label for="vendor">Project</label>' +
        '<select class="form-control js-example-basic-single multi_projects" name="project_name[]" id="" onchange="myfunc(this)" onclick="exithere(this)">' + '\
                                    <option  value="">-Select Project-</option>' +

        '</select>' +
        '</div>' +
        '</div>' +
        '<div class="elem_block">' +
        '<div class="form-group">' +
        '<label>Amount</label>' +
        '<input type="text" class="form-control amountvalidation sub_paymentamount" name="SubcontractorPaymentamount[]" onkeypress ="nextSectionode(this, event)"/>' +
        '</div>' +
        '</div>' +
        '<div class="elem_block gsts">' +
        '<div class="form-group">' +
        '<label>SGST</label>' +
        '<span  class="gstvalue"></span>' +
        '</div>' +
        '</div>' +
        '<div class="elem_block gsts">' +
        '<div class="form-group">' +
        '<label>CGST</label>' +
        '<span  class="gstvalue"></span>' +
        '</div>' +
        '</div>' +
        '<div class="elem_block gsts">' +
        '<div class="form-group">' +
        '<label>IGST</label>' +
        '<span  class="gstvalue"></span>' +
        '</div>' +
        '</div>' +
        '<div class="elem_block totalamnt">' +
        '<div class="form-group">' +
        '<label></label>' +
        '<span class="gstvalue" style="display:inline-block;"><b>Total Tax:</b></span>' +
        '<span class="gstvalue" style="display:inline-block;"><b>Total:</b></span>' +
        '</div>' +
        '<div class="elem_block" id="errorMessage1">' +
        '</div>' +
        '</div>';
    $('#dailyform').on('focus', '.sub_paymentamount, .singleproject', function() {
        var rowId = $(this).attr("data-id");
        $("#txtCurrentRows").val(rowId);
    });
    $('#dailyform').on('blur', '.tdsrate', function() {
        var projectRow = $("#txtRows").val();
        var currentRow = $("#txtCurrentRows").val();
        var projectId = $("#SubcontractorPayment_project_id" + currentRow).val();
        var subcon = $("#SubcontractorPayment_subcontractor_id").val();
        var company = $("#SubcontractorPayment_company_id").val();
        var newRow = parseInt(projectRow) + parseInt(1);
        var totRow1 = $("#txtRows").val();

        var j;
        var projectids = "";
        for (j = 1; j <= totRow1; j++) {
            var project = $("#SubcontractorPayment_project_id" + j).val();
            if (project != "") {
                projectids = projectids + "" + project + ",";
            }
        }
        projectids = JSON.stringify(projectids);

        if (projectId != "") {

            $('.js-example-basic-single').select2("destroy");
            totRow1++;
            var id_val = totRow1;

            var $project_data = $('#project_items1');
            var nextHtml = $project_data.clone();
            nextHtml.attr('id', 'project_items' + id_val);
            nextHtml.attr('class', 'mt clonesec');
            $('.paymentrow').append(nextHtml);
            $('.js-example-basic-single').select2();
            $("#SubcontractorPayment_project_id" + newRow).focus();
            $("#txtRows").val(newRow);
            $("#txtCurrentRows").val(newRow);
            changeFieldIds(id_val);

        }
    });
    $('#dailyform').on('keyup', '#SubcontractorPayment_sgst, #SubcontractorPayment_cgst, #SubcontractorPayment_igst, .sub_paymentamount, .tdsrate', function() {
        validateRows();
        getCalculations();
        validateSubcontractorLimit();
    });
    $('#dailyform').on('keydown', '.sub_paymentamount', function(e) {
        var thisClass = $(this).attr("class");
        var thisDataId = $(this).attr("data-id");
        if (thisClass.indexOf("sub_paymentamount") > -1) {
            if (e.which == 13) {
                $("#SubcontractorPayment_tds" + thisDataId).focus()
            }
        }
    });
    $('#dailyform').on('change', '.sub_paymentamount', function(e) {
        var stage_amount = parseFloat($(".stage_amount").val());
        var sub_paymentamount = parseFloat($(".sub_paymentamount").val());
        var message = "";

        if((stage_amount !="") && (sub_paymentamount > stage_amount)){
            $("#buttonsubmit").attr('disabled', true);
            var message = "Amount must be less than "+stage_amount; 
            $("#errormessage").show().html("<div class='alert alert-danger' role='alert'>"+message+"</div>");
            $('html, body').animate({
            scrollTop: $("#errormessage").offset().top- 80
            }, '100');
            return false;           
        }else{
            $("#buttonsubmit").attr('disabled', false);
        }
        
    })
    $('#SubcontractorPayment_quotation_number1').change(function() {
        var quotation_num = this.value;
        if (quotation_num !== '') {
            validateSubcontractorLimit();
        }
    });
    $('#dailyform').on('keydown', '.tdsrate', function(e) {
        var thisClass = $(this).attr("class");
        if (thisClass.indexOf("tdsrate") > -1) {
            if (e.which == 13) {
                var thisDataId = $(this).attr("data-id");
                var newDataId = thisDataId + 1;
                var currProject = $("#SubcontractorPayment_project_id" + thisDataId).val();
                if (currProject == "") {
                    setTimeout(
                        function() {
                            $("#SubcontractorPayment_payment_type").select2('focus');
                        },
                        100);
                } else {
                    $(this).blur();
                    $("#SubcontractorPayment_project_id" + newDataId).select2('focus');
                }
            }
        }
    });
    $('#dailyform').on('change', '.singleproject', function() {
        var totRow1 = $("#txtRows").val();
        var currRow1 = $("#txtCurrentRows").val();
        var currentId = $(this).attr("id").slice(-1);
        var project1 = $("#SubcontractorPayment_project_id" + currRow1).val();
        var amount1 = $("#SubcontractorPayment_amount" + currRow1).val();
        if (project1 == "") {
            $("#SubcontractorPayment_amount" + currRow1).val("");
            $("#txtSgst" + currRow1).text("");
            $("#txtCgst" + currRow1).text("");
            $("#txtIgst" + currRow1).text("");
            $("#txtgstTotal" + currRow1).text("");
            $("#txtTotal" + currRow1).text("");
            $("#errorMessage" + currRow1).html("");
            $("#SubcontractorPayment_tds" + currRow1).val("");
            $("#txtTds" + currRow1).text("");
            $("#SubcontractorPayment_tds_amount" + currRow1).val("");
            $("#txtPaidAmount" + currRow1).text("");
            $("#SubcontractorPayment_paidamount" + currRow1).val("");
        }
        getCalculations();
        validateSubcontractorLimit();
        getQuotations(currentId);
        setTimeout(
            function() {
                $("#SubcontractorPayment_amount" + currRow1).focus();
            },
            100);

    });
    $('input[name="SubcontractorPayment[payment_quotation_status]"]').change(function() {
        if (this.value === '2') {
            $('.qtn_number_div').addClass('hide');
        } else {
            $('.qtn_number_div').addClass('show');
        }
        var expId = $("#SubcontractorPayment_subcontractor_id").val();
        var company = $("#SubcontractorPayment_company_id").val();
        if (expId != "")
            getProjectList(expId, 0, company);
    });
    $('#dailyform').on('click', '#buttonsubmit', function(e) {
        e.preventDefault();
        $("#buttonsubmit").attr('disabled', true);
        var subPId = $("#txtPaymentId").val();
        var company = $("#SubcontractorPayment_company_id").val();
        var subcon = $("#SubcontractorPayment_subcontractor_id").val();
        var pType = $("#SubcontractorPayment_payment_type").val();
        var qtn_status = $(".payment_against_qtn_status").val();
        
        var bank = "";
        var cheque = "";

        if (pType == 88) {
            var bank = $("#SubcontractorPayment_bank").val();
            var cheque = $("#SubcontractorPayment_cheque_no").val();
        }
        var description = $("#SubcontractorPayment_description").val();
        var scqnumber = $("#SubcontractorPayment_quotation_number1").val();
        if (company == "" || subcon == "" || pType == "" || description == "") {
            
            $("#errormessage").show().html("<div class='alert alert-danger' role='alert'>Please enter all mandatory fields</div>");
            $('html, body').animate({
            scrollTop: $("#errormessage").offset().top- 80
            }, '100');
            return false;
        } else {
            if(qtn_status == 1){
                if(scqnumber == ""){
                    $("#errormessage").show().html("<div class='alert alert-danger' role='alert'>Please enter quotation number</div>");
                    $('html, body').animate({
                    scrollTop: $("#errormessage").offset().top- 80
                    }, '100');
                    return false;
                }else{
                    var stage_count = $(".payment_stage_div").attr('data-count');
                    if( stage_count > 0 ) {
                        if($('.sc_payment_stage').val()==""){                            
                            $("#errormessage").show().html("<div class='alert alert-danger' role='alert'>Please enter all mandatory fields</div>");
                            $('html, body').animate({
                                scrollTop: $("#errormessage").offset().top- 80
                            }, '100');
                        return false;
                        }
                    }
                }                                
            }
            $('.loading-overlay').addClass('is-active');
            if (pType == 88) {
                if (bank == "" || cheque == "") {
                    $("#errormessage").show().html("<div class='alert alert-danger' role='alert'>Please enter all mandatory fields</div>");
                    $('html, body').animate({
                    scrollTop: $("#errormessage").offset().top- 80
                    }, '100');
                    return false;
                } else {
                    $("#errormessage").html("");
                    addSubcontractorPayment();
                    $("#buttonsubmit").attr('disabled', true);
                    $(".project_items").siblings(".clonesec").remove();
                }
            } else {
                $("#errormessage").html("");
                addSubcontractorPayment();
                $("#buttonsubmit").attr('disabled', true);
                $(".project_items").siblings(".clonesec").remove();
            }
        }
    });

    function myfunc(elm) {
        $(elm).closest(".row_block").find('input').focus();
    }

    function nextSectionode(elm, e) {
        if (!e) e = window.event;
        var keyCode = e.keyCode || e.which;
        if (keyCode == '13') {}
    }

    function exithere(elm) {
        if ($(elm).closest(".row_block").find('.multi_projects').val() == '') {
            $("#SubcontractorPayment_payment_type").focus();
        } else {
            $(elm).closest(".row_block").find('input').focus();
        }

    }

    function changeFieldIds(id_val) {
        $('#project_items' + id_val).find('.payment_against_qtn_status').prop('id', 'SubcontractorPayment_payment_quotation_status' + id_val);
        $('#project_items' + id_val).find('.project_list').prop('id', 'SubcontractorPayment_project_id' + id_val);
        $('#project_items' + id_val).find('.qtn_list').prop('id', 'SubcontractorPayment_quotation_number' + id_val);
        $('#project_items' + id_val).find('.stage_list').prop('id', 'SubcontractorPayment_stage' + id_val);
        $("#project_items" + id_val).find('[name="SubcontractorPayment[amount]"]').prop('id', 'SubcontractorPayment_amount' + id_val);
        $("#project_items" + id_val).find('.txtSgst').prop('id', 'txtSgst' + id_val);
        $("#project_items" + id_val).find('.txtCgst').prop('id', 'txtCgst' + id_val);
        $("#project_items" + id_val).find('.txtIgst').prop('id', 'txtIgst' + id_val);
        $("#project_items" + id_val).find('.txtgstTotal').prop('id', 'txtgstTotal' + id_val);
        $("#project_items" + id_val).find('[name="SubcontractorPayment[tax_amount]"]').prop('id', 'SubcontractorPayment_tax_amount' + id_val);
        $("#project_items" + id_val).find('.txtTotal').prop('id', 'txtTotal' + id_val);
        $("#project_items" + id_val).find('[name="SubcontractorPayment[tds]"]').prop('id', 'SubcontractorPayment_tds' + id_val);
        $("#project_items" + id_val).find('[name="SubcontractorPayment[tds_amount]"]').prop('id', 'SubcontractorPayment_tds_amount' + id_val);
        $("#project_items" + id_val).find('.txtTds').prop('id', 'txtTds' + id_val);
        $("#project_items" + id_val).find('.txtPaidAmount').prop('id', 'txtPaidAmount' + id_val);
        $("#project_items" + id_val).find('[name="SubcontractorPayment[paidamount]"]').prop('id', 'SubcontractorPayment_paidamount' + id_val);
        $("#project_items" + id_val).find('[name="SubcontractorPayment[approve_status]"]').prop('id', 'txtPermission' + id_val);
        $("#project_items" + id_val).find('[name="SubcontractorPayment[payment_id]"]').prop('id', 'Payment_id' + id_val);
        //set values to zero   
        $('#SubcontractorPayment_project_id' + id_val).val('').trigger('change.select2');
        $('#SubcontractorPayment_quotation_number' + id_val).val('').trigger('change.select2');
        $('SubcontractorPayment_stage' + id_val).val('').trigger('change.select2');
        $("#SubcontractorPayment_amount" + id_val).val('');
        $("#txtSgst" + id_val).text('');
        $("#txtCgst" + id_val).text('');
        $("#txtIgst" + id_val).text('');
        $("#txtgstTotal" + id_val).text('');
        $("#txtTotal" + id_val).text('');
        $("#SubcontractorPayment_tds" + id_val).val('');
        $("#txtTds" + id_val).text('');
        $("#txtPaidAmount" + id_val).text('');
        $("#txtPermission" + id_val).val('');
        $("#Payment_id1" + id_val).val('');


    }
    $(".js-example-basic-single").select2();

    var limit = '<?php echo $company['subcontractor_limit']; ?>';
    $(document).ready(function() {
        $('#loading').hide();
        $('#dailyform').on('shown.bs.collapse', function() {
            $('select').first().focus();
        });
        if ($("#SubcontractorPayment_payment_type").val() == 88) {
            $(".hiden_box").show();
            $("#SubcontractorPayment_cheque_no").prop("readonly", false);
            $("#SubcontractorPayment_bank").prop("readonly", false);
        }

    });

    $(function() {
        $("#SubcontractorPayment_date").datepicker({
            dateFormat: 'dd-mm-yy',
            maxDate: new Date()
        });
    });
    $(document).ready(function() {
        $('select').first().focus();
    });
    $(document).on('change', '.payment_against_qtn_status', function(e) {
        var expId = $("#SubcontractorPayment_subcontractor_id").val();
        var company = $("#SubcontractorPayment_company_id").val();
        var currentId = $(this).attr("id").slice(-1);
        if (expId != "")
            getProjectList2(expId, 0, company, currentId);
    });
</script>
<script type="text/javascript">
    function getFullData(newDate) {
        $('.loading-overlay').addClass('is-active');
        $.ajax({
            url: "<?php echo $getUrl; ?>",
            data: {
                date: newDate
            },
            type: "POST",
            success: function(data) {
                $("#newlist").html(data);
            }
        });
    }

    function getTotalFromPayment(subcontractor_id, project_id, amount, txtPaymentId, company_id) {
        $('#loading').show();
        $.ajax({
            url: "<?php echo $payment_url; ?>",
            data: {
                subcontractor_id: subcontractor_id,
                project_id: project_id,
                amount: amount,
                id: txtPaymentId,
                company_id: company_id
            },
            type: "GET",
            dataType: "JSON",
            success: function(data) {
                if (data.status == 0) {
                    $('#buttonsubmit').attr("disabled", false);
                    $("#errormessage").show()
                        .html('<div class="alert alert-danger">' + data.limit + '% limit exceeded</div>')
                        .fadeOut(10000);
                        $('html, body').animate({
                        scrollTop: $("#errormessage").offset().top- 80
                        }, '100');
                    $('#approve_status').val('No');
                } else if (data.status == 1) {
                    $('#buttonsubmit').attr("disabled", false);
                    $('#approve_status').val('Yes');
                } else if (data.status == 2) {
                    $('#buttonsubmit').attr("disabled", true);
                    $('#approve_status').val('Yes');
                    $("#errormessage").show()
                        .html('<div class="alert alert-danger">Permission needed</div>')
                        .fadeOut(10000);
                        $('html, body').animate({
                        scrollTop: $("#errormessage").offset().top- 80
                        }, '100');
                } else if (data.status == 3) {
                    $('#buttonsubmit').attr("disabled", true);
                    $('#approve_status').val('Yes');
                    $("#errormessage").show()
                        .html('<div class="alert alert-danger">Amount greater than total amount</div>')
                        .fadeOut(10000);
                        $('html, body').animate({
                        scrollTop: $("#errormessage").offset().top- 80
                        }, '100');
                }
            }
        });
    }

    function changedate() {
        var cDate = $("#SubcontractorPayment_date").val();
        getFullData(cDate);
        validateDateSelection(cDate);
        resetForm(cDate);
    }

    function getProjectList(expId, project, company) {
        $('#loading').show();
        $.ajax({
            url: "<?php echo $projectUrl; ?>",
            data: {
                "expid": expId,
                "company": company
            },
            success: function(data) {
                $(".singleproject").html(data);
                $(".sub_paymentamount").val("");
                $(".gstvalue").val("");
                $(".gstvalue").text("");
                $('div.paymentrow').not(':eq(0)').remove();
                $("#txtRows").val(1);
                $("#txtCurrentRows").val(1);
                $("#txtCount").val(0);
                $(".rowerror").html("");
                $('div.rowerror').not(':eq(0)').remove();
                $(".tdsrate").val();
            }
        });
    }

    function getSubcontractorList(comId, company) {
        $('#loading').show();
        $.ajax({
            url: "<?php echo $subcontractorUrl; ?>",
            data: {
                "comId": comId
            },
            type: "POST",
            dataType: 'json',
            success: function(data) {
                $("#SubcontractorPayment_subcontractor_id").html(data.subcontractor);
                $("#SubcontractorPayment_bank").html(data.bank)
                if (company != 0)
                    $("#SubcontractorPayment_subcontractor_id").val(company);
                $("#SubcontractorPayment_subcontractor_id").select2("focus");
            }
        });
    }

    function getQuotations(currentId) {
        $('#loading').show();
        var subcontId = $('#SubcontractorPayment_subcontractor_id').val();
        var company = $('#SubcontractorPayment_company_id').val();
        var project_id = $('#SubcontractorPayment_project_id' + currentId).val();
        if (subcontId !== '' && company !== '' && project_id !== '') {
            $.ajax({
                url: "<?php echo $quotationListUrl; ?>",
                data: {
                    "subconId": subcontId,
                    "companyId": company,
                    "project_id": project_id
                },
                type: "POST",
                dataType: 'json',
                success: function(data) {
                    $("#SubcontractorPayment_quotation_number" + currentId).html(data.quotations);

                }
            });
        }

    }

    function getQuotations2(subcontId, company, project_id, quot_id, type) {
        $('#loading').show();
        if (subcontId !== '' && company !== '' && project_id !== '') {
            $.ajax({
                url: "<?php echo $quotationListUrl; ?>",
                data: {
                    "subconId": subcontId,
                    "companyId": company,
                    "project_id": project_id
                },
                type: "POST",
                dataType: 'json',
                success: function(data) {
                    $("#SubcontractorPayment_quotation_number" + type).html(data.quotations);
                    $('#SubcontractorPayment_quotation_number' + type).val(quot_id).trigger('change.select2');

                }
            });
        }

    }

    function getSubcontractorList2(comId, company, bnk) {
        $('#loading').show();
        $.ajax({
            url: "<?php echo $subcontractorUrl; ?>",
            data: {
                "comId": comId
            },
            type: "POST",
            dataType: "json",
            success: function(data) {
                $("#SubcontractorPayment_subcontractor_id").html(data.subcontractor);
                $("#SubcontractorPayment_bank").html(data.bank);
                if (company != 0) {
                    $("#SubcontractorPayment_subcontractor_id").val(company);
                }
                if (bnk != 0) {
                    $("#SubcontractorPayment_bank").val(bnk);
                }
            }
        });
    }


    function getProjectList2(expId, project, company, type) {
        $('#loading').show();
        var radio_button_value = $('#SubcontractorPayment_payment_quotation_status' + type).val();
        if (radio_button_value === '1') {
            $('#SubcontractorPayment_quotation_number' + type).parents('.qtn_number_div').show();
            $.ajax({
                url: "<?php echo $projectUrl; ?>",
                data: {
                    "expid": expId,
                    "company": company
                },
                success: function(data) {
                    $("#SubcontractorPayment_project_id" + type).html(data);
                    if (project != 0)
                        $("#SubcontractorPayment_project_id" + type).val(project);
                }
            });
        } else if (radio_button_value === '2') {
            $.ajax({
                url: "<?php echo $expenseProjects; ?>",
                data: {
                    "expid": expId,
                    "company": company
                },
                success: function(data) {
                    $("#SubcontractorPayment_project_id" + type).html(data);
                    $("#SubcontractorPayment_quotation_number" + type).parents('.qtn_number_div').hide();
                    if (project != 0)
                        $("#SubcontractorPayment_project_id" + type).val(project);
                }
            });
        }

    }
    $("#SubcontractorPayment_company_id").change(function() {
        var expId = $("#SubcontractorPayment_subcontractor_id").val();
        var company = $("#SubcontractorPayment_company_id").val();
        if (expId != "")
            getProjectList(expId, 0, company);
    });
    $("#SubcontractorPayment_subcontractor_id").change(function() {
        var expId = $("#SubcontractorPayment_subcontractor_id").val();
        var company = $("#SubcontractorPayment_company_id").val();
        var expId = expId ? expId : 0;
        getProjectList(expId, 0, company);
        $(".payment_against_qtn_status").trigger("change");
        setTimeout(
            function() {
                $("#SubcontractorPayment_sgst").focus();
            },
            200);
    });
    $(function() {
        var $inp = $('input:text');
        $inp.bind('keydown', function(e) {
            var key = e.which;
            if (key == 13) {
                e.preventDefault();
                var nxtIdx = $inp.index(this) + 1;
                $(":input:text:eq(" + nxtIdx + ")").focus();
            }
        });
    });
    $('#dailyform').on('blur', '#SubcontractorPayment_igst', function(e) {
        setTimeout(
            function() {
                $('#SubcontractorPayment_project_id1').focus();
            },
            100);
    });
    $("#SubcontractorPayment_company_id").change(function() {
        var comId = $("#SubcontractorPayment_company_id").val();
        var comId = comId ? comId : 0;
        getSubcontractorList(comId, 0);
        $("#SubcontractorPayment_subcontractor_id").attr("disabled", false);
        $('#SubcontractorPayment_project_id').attr("disabled", true);
    });

    $(document).ready(function() {
        $("#SubcontractorPayment_bank").attr("disabled", true);
        $("#SubcontractorPayment_cheque_no").attr("readonly", true);
        $("a#next").css("pointer-events", "none").css("color", "#ccccc");
        $("#SubcontractorPayment_subcontractor_id").focus();
        $("#SubcontractorPayment_project_id").attr("disabled", true);
        $("#SubcontractorPayment_subcontractor_id").attr("disabled", true);

        $("body").on("click", ".row-daybook", function(e) {
            var reconStatus = $(this).parents("td").attr("data-status");
            if(reconStatus==1){
                alert("Can't update !Reconciled Entry");
                return;
            }
            $('.loading-overlay').addClass('is-active');
            $('html, body').animate({
                scrollTop: 0
            }, 'slow');
            $(".daybook .collapse").addClass("in");
            $(".addentries").removeClass("collapsed");
            if ($(".daybook .collapse").css('display') == 'visible') {
                $(".daybook .collapse").css({
                    "height": "auto"
                });
            } else {
                $(".collapse").css({
                    "height": ""
                });
            }
            $(".popover").removeClass("in");
            var rowId = $(this).attr("id");
           // var expId = $("#paymentid" + rowId).val();
            var expenseId = $(this).parents("tr").attr('data-id');
            var expId = $(this).parents("tr").attr('id');
            var expStatus = $(this).parents("tr").attr('data-status');




            $("#buttonsubmit").text("UPDATE");
            $("#txtPaymentId").val(expenseId);
            $('input[id="SubcontractorPayment_payment_quotation_status_0"]').val(1);
            $('input[id="SubcontractorPayment_payment_quotation_status_1"]').val(2);
            $.ajax({
                url: "<?php echo $getSubContractor; ?>",
                data: {
                    "paymentid": expId,
                    "expstatus": expStatus
                },
                type: "POST",
                success: function(data) {
                    
                    $('.loading-overlay').removeClass('is-active');
                    $(".gstvalue").css("display", "inline-block");
                    var result = JSON.parse(data);
                    if (result["payment_quotation_status"] == 1) {
                        $("#SubcontractorPayment_payment_quotation_status_0").prop("checked", true);
                    } else if (result["payment_quotation_status"] == 2) {
                        $("#SubcontractorPayment_payment_quotation_status_1").prop("checked", true);
                    }
                    $("#SubcontractorPayment_company_id").select2('focus');
                    $("#SubcontractorPayment_project_id").attr("disabled", false);
                    
                    if (!result["subcontractor_id"]) {                        
                        $("#SubcontractorPayment_subcontractor_id").val("");
                        $("#SubcontractorPayment_project_id").val("");
                        $("#SubcontractorPayment_description").val("");
                        $("#SubcontractorPayment_payment_type").val("");
                        $("#SubcontractorPayment_employee_id").val("");
                        $("#SubcontractorPayment_amount").val("");
                        $("#SubcontractorPayment_sgst").val("");
                        $("#SubcontractorPayment_sgst_amount").val("");
                        $("#SubcontractorPayment_cgst").val("");
                        $("#SubcontractorPayment_cgst_amount").val("");
                        $("#SubcontractorPayment_igst").val("");
                        $("#SubcontractorPayment_igst_amount").val("");
                        $("#SubcontractorPayment_tax_amount").val("");
                        if (result["payment_type"] == 88) {
                            $(".hiden_box").show();
                            $("#SubcontractorPayment_bank").attr("disabled", false);
                            $("#SubcontractorPayment_bank").val(result["bank"]);
                            $("#SubcontractorPayment_cheque_no").attr("readonly", false);
                            $("#SubcontractorPayment_cheque_no").val(result["cheque_no"]);
                        } else {
                            $(".hiden_box").hide();
                            $("#SubcontractorPayment_bank").val("");
                            $("#SubcontractorPayment_cheque_no").val("");
                            $("#SubcontractorPayment_bank").attr("disabled", true);
                            $("#SubcontractorPayment_cheque_no").attr("readonly", true);
                        }
                    } else {
                       
                        if (result["uniqueid"]) {                            
                            if (result.final_result.length > 1) {
                                
                                $('.js-example-basic-single').select2("destroy");
                                for (i = 2; i <= result.final_result.length; i++) {
                                    console.log(result.final_result[i - 1]);
                                    var id_val = i;
                                    var $project_data = $('#project_items1');
                                    var nextHtml = $project_data.clone();
                                    nextHtml.attr('id', 'project_items' + id_val);
                                    $project_data.after(nextHtml);
                                    //change ids                                
                                    changeFieldIds(id_val);
                                    
                                    //change values                                
                                    getProjectList2(result["subcontractor_id"], result.final_result[i - 1].project_id, result["company"], id_val);
                                    $('#SubcontractorPayment_project_id' + id_val).val(result.final_result[i - 1].project_id).trigger('change.select2');
                                    getQuotations2(result["subcontractor_id"], result["company"], result.final_result[i - 1].project_id, result.final_result[i - 1].quotation_number, id_val);
                                    $('#SubcontractorPayment_quotation_number' + id_val).val(result.final_result[i - 1].quotation_number).trigger('change.select2');
                                    $('#SubcontractorPayment_payment_quotation_status' + id_val).val(result.final_result[i - 1].payment_quotation_status);
                                    if ((result.final_result[i - 1].payment_quotation_status) === '2') {
                                        $('#SubcontractorPayment_quotation_number' + id_val).parents('.qtn_number_div').hide();
                                    }
                                    $("#SubcontractorPayment_amount" + id_val).val(result.final_result[i - 1].amount);
                                    $("#txtSgst" + id_val).text(parseFloat(result.final_result[i - 1].sgst_amount).toFixed(2));
                                    $("#txtCgst" + id_val).text(parseFloat(result.final_result[i - 1].cgst_amount).toFixed(2));
                                    $("#txtIgst" + id_val).text(parseFloat(result.final_result[i - 1].igst_amount).toFixed(2));
                                    $("#txtgstTotal" + id_val).text(parseFloat(result.final_result[i - 1].tax_amount).toFixed(2));
                                    $("#txtTotal" + id_val).text(parseFloat(result.final_result[i - 1].paidamount).toFixed(2));
                                    $("#SubcontractorPayment_tds" + id_val).val(parseFloat(result.final_result[i - 1].tds).toFixed(2));
                                    $("#txtTds" + id_val).val(parseFloat(result.final_result[i - 1].tds_amount).toFixed(2));
                                    $("#txtPaidAmount" + id_val).text(parseFloat(result.final_result[i - 1].paidamount).toFixed(2));
                                    $("#txtPermission" + id_val).val(result.final_result[i - 1].approve_status);
                                    $("#Payment_id" + id_val).val(result.final_result[i - 1].payment_id);
                                    

                                }
                                $('.js-example-basic-single').select2();


                            }
                            console.log(result);
                            $("#SubcontractorPayment_amount1").val(result.final_result[0].amount);
                            $('#SubcontractorPayment_payment_quotation_status1').val(result.final_result[0].payment_quotation_status);
                            if ((result.final_result[0].payment_quotation_status) === '2') {
                                $('#SubcontractorPayment_quotation_number1').parents('.qtn_number_div').hide();
                            }
                            $("#txtSgst1").text(parseFloat(result.final_result[0].sgst_amount).toFixed(2));
                            $("#txtCgst1").text(parseFloat(result.final_result[0].cgst_amount).toFixed(2));
                            $("#txtIgst1").text(parseFloat(result.final_result[0].igst_amount).toFixed(2));
                            $("#txtgstTotal1").text(parseFloat(result.final_result[0].tax_amount).toFixed(2));
                            $("#txtTotal1").text(parseFloat(result.final_result[0].paidamount).toFixed(2));
                            $("#SubcontractorPayment_tds1").val(parseFloat(result.final_result[0].tds).toFixed(2));
                            $("#txtTds1").val(parseFloat(result.final_result[0].tds_amount).toFixed(2));
                            $("#txtPaidAmount1").text(parseFloat(result.final_result[0].paidamount).toFixed(2));
                            $("#txtamountPaid").text(parseFloat(result["total"]).toFixed(2));
                            $("#txttdsamount").text(parseFloat(result.tds_amount_total).toFixed(2));

                            $('#SubcontractorPayment_company_id').val(result["company"]).trigger('change.select2');
                            $('#SubcontractorPayment_subcontractor_id').attr("disabled", false);
                            $('#SubcontractorPayment_subcontractor_id').val(result["subcontractor_id"]).trigger('change.select2');
                            getProjectList2(result["subcontractor_id"], result.final_result[0].project_id, result["company"], 1);

                            $('#SubcontractorPayment_project_id1').val(result.final_result[0].project_id).trigger('change.select2');
                            $('#txtPermission1').val(result.final_result[0].approve_status);
                            $('#Payment_id1').val(result.final_result[0].payment_id);
                            getQuotations2(result["subcontractor_id"], result["company"], result.final_result[0].project_id, result.final_result[0].quotation_number, 1);
                            $('#SubcontractorPayment_quotation_number1').val(result.final_result[0].quotation_number).trigger('change.select2');
                            $('#SubcontractorPayment_sgst').val(result["sgst"]);
                            $('#SubcontractorPayment_cgst').val(result["cgst"]);
                            $('#SubcontractorPayment_igst').val(result["igst"]);
                            $('#txtRows').val(result["totalrow"]);
                            $('#txtCount').val(result["totalrow"]);
                            $('#txtgrandtot').text(result["total"]);
                            $('#SubcontractorPayment_payment_type').val(result["payment_type"]).trigger('change.select2');
                            $('#SubcontractorPayment_employee_id').val(result["employee_id"]).trigger('change.select2');
                            if (result["payment_type"] == 88) {
                                $(".hiden_box").show();
                                $("#SubcontractorPayment_bank").attr("disabled", false);
                                $('#SubcontractorPayment_bank').val(result["bank"]).trigger('change.select2');
                                $("#SubcontractorPayment_cheque_no").attr("readonly", false);
                                $("#SubcontractorPayment_cheque_no").val(result["cheque_no"]);
                            } else {
                                $(".hiden_box").hide();
                                $('#SubcontractorPayment_bank').val('').trigger('change.select2');
                                $("#SubcontractorPayment_cheque_no").val("");
                                $("#SubcontractorPayment_bank").attr("disabled", true);
                                $("#SubcontractorPayment_cheque_no").attr("readonly", true);
                            }
                            $("#SubcontractorPayment_description").val(result["description"]);
                        } else {
                            alert("abc");
                            $('#SubcontractorPayment_subcontractor_id').val(result["subcontractor_id"]).trigger('change.select2');
                            $('#SubcontractorPayment_company_id').val(result["company"]).trigger('change.select2');
                            getProjectList2(result["subcontractor_id"], result["project_id"], result["company"], 1);
                            var bnk = (result["bank"] != '') ? result["bank"] : 0;
                            getSubcontractorList2(result["company"], result["subcontractor_id"], bnk);
                            $("#SubcontractorPayment_project_id").attr("disabled", false);
                            $("#SubcontractorPayment_subcontractor_id").attr("disabled", false);
                            $("#SubcontractorPayment_description").val(result["description"]);
                            $('#SubcontractorPayment_payment_type').val(result["payment_type"]).trigger('change.select2');
                            $("#SubcontractorPayment_amount1").val(result["amount"]);
                            $("#SubcontractorPayment_sgst").val(result["sgst"]);
                            $('#SubcontractorPayment_quotation_number1').val(result["quotation_number"]).trigger('change.select2');
                            $("#txtSgst1").text(result["sgst_amount"]);
                            $("#SubcontractorPayment_cgst1").val(result["cgst"]);
                            $("#txtCgst1").text(result["cgst_amount"]);
                            $("#SubcontractorPayment_igst1").val(result["igst"]);
                            $("#txtIgst1").text(result["igst_amount"]);
                            $("#txtgstTotal1").html("" + result["tax_amount"] + " ");
                            var total = (Number(result["amount"]) + Number(result["tax_amount"])).toFixed(2);
                            $("#txtTotal1").html("<b>Total: </b> " + total + " ");
                            $("#txtPermission1").val(result["permission"]);
                            $("#Payment_id1").val(result["payment_id"]);
                            if (result["payment_type"] == 88) {
                                $(".hiden_box").show();
                                $("#SubcontractorPayment_bank").attr("disabled", false);
                                $('#SubcontractorPayment_bank').val(result["bank"]).trigger('change.select2');
                                $("#SubcontractorPayment_cheque_no").attr("readonly", false);
                                $("#SubcontractorPayment_cheque_no").val(result["cheque_no"]);
                            } else {
                                $(".hiden_box").hide();
                                $('#SubcontractorPayment_bank').val('').trigger('change.select2');
                                $("#SubcontractorPayment_cheque_no").val("");
                                $("#SubcontractorPayment_bank").attr("disabled", true);
                                $("#SubcontractorPayment_cheque_no").attr("readonly", true);
                            }
                        }
                    }
                }
            });
        });


        $(".amountvalidation").keydown(function(event) {

            if (event.shiftKey == true) {
                event.preventDefault();
            }

            if ((event.keyCode >= 48 && event.keyCode <= 57) ||
                (event.keyCode >= 96 && event.keyCode <= 105) ||
                event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 ||
                event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190 || event.keyCode == 110 || event.keyCode == 13) {
                var splitfield = $(this).val().split(".");
                if ((splitfield[1] !== undefined && splitfield[1].length >= 2) && event.keyCode != 8 && event.keyCode != 0 && event.keyCode != 13) {
                    event.preventDefault();
                }
            } else {
                event.preventDefault();
            }

            if ($(this).val().indexOf('.') !== -1 && (event.keyCode == 190 || event.keyCode == 110))
                event.preventDefault();
        });

        $(document).on('click', '.delete_confirmation', function(e) {
            var rowId = $(this).attr("id");
            var expId = $("#paymentid" + rowId).val();
            var date = $("#SubcontractorPayment_date").val();
            var answer = confirm("Are you sure you want to delete?");
            if (answer) {
                $('.loading-overlay').addClass('is-active');
                $.ajax({
                    type: "GET",
                    data: {
                        expId: expId,
                        expense_date: date,
                        status: 1,
                        delId: "",
                        action: 4
                    },
                    url: "<?php echo Yii::app()->createUrl("subcontractorpayment/deleteconfirmation") ?>",
                    success: function(data) {
                        $('.loading-overlay').removeClass('is-active');
                        $("#newlist").html(data);
                        if (data == 1) {
                            $("#errormessage").show()
                                .html('<div class="alert alert-danger"><strong>Failed!</strong> This request unable to process now. Please try again later.</div>')
                                .fadeOut(5000);
                                $('html, body').animate({
                                scrollTop: $("#errormessage").offset().top- 80
                                }, '100');
                            return false;
                        } else {
                            $("#errormessage").show()
                                .html('<div class="alert alert-success"><strong>Success!</strong> Request for deleting this row is send to admin. Please wait for response.</div>')
                                .fadeOut(5000);
                                $('html, body').animate({
                                scrollTop: $("#errormessage").offset().top- 80
                                }, '100');
                        }
                    }
                });
            } else {
                return false;
            }
        });
        $(document).on('click', '.delete_restore', function(e) {
            var rowId = $(this).attr("id");
            var delId = $(this).attr("data-id");
            var expId = $("#paymentid" + rowId).val();
            var date = $("#SubcontractorPayment_date").val();
            var answer = confirm("Are you sure you want to restore?");
            if (answer) {
                $('.loading-overlay').addClass('is-active');
                $.ajax({
                    type: "POST",
                    data: {
                        expId: expId,
                        expense_date: date,
                        status: 2,
                        delId: delId,
                        action: 4
                    },
                    url: "<?php echo Yii::app()->createUrl("subcontractorpayment/deleteconfirmation") ?>",
                    success: function(data) {
                        $('.loading-overlay').removeClass('is-active');
                        $("#newlist").html(data);
                        if (data == 1) {
                            $("#errormessage").show()
                                .html('<div class="alert alert-danger"><strong>Failed!</strong> This request unable to process now. Please try again later.</div>')
                                .fadeOut(5000);
                                $('html, body').animate({
                                scrollTop: $("#errormessage").offset().top- 80
                                }, '100');
                            return false;
                        } else {
                            $("#errormessage").show()
                                .html('<div class="alert alert-success"><strong>Success!</strong> Restored data successfully.</div>')
                                .fadeOut(5000);
                                $('html, body').animate({
                                scrollTop: $("#errormessage").offset().top- 80
                                }, '100');
                        }
                    }
                });
            } else {
                return false;
            }
        });
        $(document).on('click', '.delete_row', function(e) {
            var reconStatus = $(this).parents("td").attr("data-status");
            if(reconStatus==1){
                alert("Can't delete !Reconciled Entry");
                return;
            }
            
            var answer = confirm("Are you sure you want to delete?");
            if (answer) {
                $('.loading-overlay').addClass('is-active');
                var rowId = $(this).attr("id");
                var expId = $("#paymentid" + rowId).val();
                var date = $("#SubcontractorPayment_date").val();
                $.ajax({
                    type: "GET",
                    data: {
                        expId: expId,
                        payment_date: date
                    },
                    url: "<?php echo Yii::app()->createUrl("subcontractorpayment/deletdailyentry") ?>",
                    success: function(data) {
                        $('.loading-overlay').removeClass('is-active');
                        if (data == 1) {
                            $("#errormessage").show()
                                .html('<div class="alert alert-warning"><strong>Failed !</strong> please try again.</div>')
                                .fadeOut(5000);
                                $('html, body').animate({
                                scrollTop: $("#errormessage").offset().top- 80
                                }, '100');
                            $("#buttonsubmit").attr('disabled', false);
                            return false;
                        } else {
                            $("#errormessage").show()
                                .html('<div class="alert alert-success"><strong>Success!</strong> Row Deleted.</div>')
                                .fadeOut(5000);
                                $('html, body').animate({
                                scrollTop: $("#errormessage").offset().top- 80
                                }, '100');
                            $("#newlist").html(data);
                        }
                    }
                })
            }

        })
        $(document).on('click', '.permission_item', function(e) {
            var answer = confirm("Are you sure you want to approve?");
            if (answer) {
                e.preventDefault();
                var element = $(this);
                var item_id = $(this).attr('id');
                element.closest('.tooltip-hiden').css('pointer-events', 'none');
                $('#loading').show();
                $.ajax({
                    url: '<?php echo  Yii::app()->createAbsoluteUrl('subcontractorpayment/permissionapprove'); ?>',
                    type: 'GET',
                    dataType: 'json',
                    data: {
                        item_id: item_id
                    },
                    success: function(response) {
                        $(".popover").removeClass("in");
                        element.closest('.tooltip-hiden').css('pointer-events', '');
                        element.closest('.permission_item').removeAttr('disabled');
                        element.closest('.row-daybook').removeAttr('disabled');
                        element.closest('.delete_row').removeAttr('disabled');
                        if (response.response == 'success') {
                            $(".approveoption_" + item_id).hide();
                            element.closest('tr').removeClass('permission_style');
                            element.closest('td').removeClass('permission_style');
                            $("#errormessage").show()
                                .html('<div class="alert alert-success"><strong>Success!</strong>' + response.msg + '</div>')
                                .fadeOut(5000);
                                $('html, body').animate({
                                scrollTop: $("#errormessage").offset().top- 80
                                }, '100');
                        } else if (response.response == 'warning') {
                            $(".approveoption_" + item_id).hide();
                            element.closest('tr').removeClass('permission_style');
                            $("#errormessage").show()
                                .html('<div class="alert alert-warning"><strong>Failed !</strong>' + response.msg + '</div>')
                                .fadeOut(5000);
                                $('html, body').animate({
                                scrollTop: $("#errormessage").offset().top- 80
                                }, '100');
                        } else {
                            $("#errormessage").show()
                                .html('<div class="alert alert-error"><strong>Failed !</strong>' + response.msg + '</div>')
                                .fadeOut(5000);
                                $('html, body').animate({
                                scrollTop: $("#errormessage").offset().top- 80
                                }, '100');
                        }
                    }
                });
            }
        });
        $("#previous").click(function(e) {
            var cDate = $("#SubcontractorPayment_date").val();

            cDate = cDate.split("-").reverse().join("-");
            var newDate = new Date(cDate);
            var prDate = new Date(newDate.getFullYear(), newDate.getMonth(), newDate.getDate() - 1);
            var newPrDate = $.format.date(prDate, "dd-MM-yyyy");
            $("#SubcontractorPayment_date").val(newPrDate);
            getFullData(newPrDate);
            validateDateSelection(newPrDate);
            resetForm(newPrDate);
        });
        $("#current").click(function() {
            var cDate = new Date();
            var newDate = $.format.date(cDate, "dd-MM-yyyy");
            $("#SubcontractorPayment_date").val(newDate);
            getFullData(newDate);
            validateDateSelection(newDate);
            resetForm(newDate);
        });
        $("#next").click(function() {
            var cDate = $("#SubcontractorPayment_date").val();
            cDate = cDate.split("-").reverse().join("-");
            var newDate = new Date(cDate);
            var nxtDate = new Date(newDate.getFullYear(), newDate.getMonth(), newDate.getDate() + 1);
            var newNxtDate = $.format.date(nxtDate, "dd-MM-yyyy");
            $("#SubcontractorPayment_date").val(newNxtDate);
            getFullData(newNxtDate);
            validateDateSelection(newNxtDate);
            resetForm(newNxtDate);
        });
        $("#SubcontractorPayment_payment_type").change(function() {
            var receiptType = $("#SubcontractorPayment_payment_type").val();
            if (receiptType == 88) {
                $("#SubcontractorPayment_cheque_no").attr("readonly", false);
                $("#SubcontractorPayment_bank").attr("disabled", false);
                $(".employee_box").hide();
                $(".hiden_box").show();
                setTimeout(
                    function() {
                        $("#SubcontractorPayment_bank").select2("focus");
                    },
                    100);

            } else {
                if (receiptType == 103) {
                    $(".employee_box").show();
                }else{
                    $(".employee_box").hide();
                }
                $("#SubcontractorPayment_cheque_no").attr("readonly", true);
                $("#SubcontractorPayment_bank").attr("disabled", true);
                $(".hiden_box").hide();
                setTimeout(
                    function() {
                        $("#SubcontractorPayment_description").focus();
                    },
                    100);
            }
        });
        $("#SubcontractorPayment_bank").change(function() {
            setTimeout(
                function() {
                    $("#SubcontractorPayment_cheque_no").focus();
                },
                100);
        });
        $("#SubcontractorPayment_cheque_no").keydown(function(e) {
            if (e.which == 13) {
                $("#SubcontractorPayment_description").focus();
            }
        });
        $("#txtPurchaseType").change(function() {
            var purchaseType = $("#txtPurchaseType").val();
            var amount = $("#txtTotal").val();
            if (purchaseType == 1) {
                $("#txtPaid").val(0);
                $("#txtPaid").attr("readOnly", true);
            } else if (purchaseType == 2) {
                $("#txtPaid").val(amount);
                $("#txtPaid").attr("readOnly", true);
            } else if (purchaseType == 3) {
                $("#txtPaid").val(0);
                $("#txtPaid").attr("readOnly", false);
            } else {
                $("#txtPaid").val("0");
                $("#txtPaid").attr("readOnly", false);
            }
        });
        $("#btnReset").click(function() {
            $("#txtPaymentId").val("");
            $("#buttonsubmit").text("ADD");
            $('#dailyexpense-form').find('input, select, textarea').not("#SubcontractorPayment_date").val('');
            $("#Dailyexpense_bill_id").val("");
            $("#Dailyexpense_expensehead_id").val("");
            $("#Dailyexpense_vendor_id").html('<option value="">-Select Vendor-</option>');
            $("#Dailyexpense_vendor_id").attr("disabled", true);
            $('#SubcontractorPayment_subcontractor_id').val('').trigger('change.select2');
            $('#SubcontractorPayment_project_id').val('').trigger('change.select2');
            $('#SubcontractorPayment_payment_type').val('').trigger('change.select2');
            $('#SubcontractorPayment_employee_id').val('').trigger('change.select2');
            $('#SubcontractorPayment_bank').val('').trigger('change.select2');

            $("#txtSgst1").text("");
            $("#txtCgst1").text("");
            $("#txtIgst1").text("");
            $("#txtgstTotal").text("");
            $("#txtTotal1").text("");
            $(".hiden_box").hide();
        });
        $('input').on("keypress", function(e) {
            if (e.keyCode == 13) {
                var inputs = $(this).parents("form").eq(0).find(":input");
                var idx = inputs.index(this);

                if (idx == inputs.length - 1) {
                    inputs[0].select()
                } else {
                    inputs[idx + 1].focus();
                    inputs[idx + 1].select();
                }
                return false;
            }
        });
    });

    function validateDateSelection(cDate) {
        var selDate = cDate;
        var currDate = new Date();
        currDate = $.format.date(currDate, "dd-MM-yyyy");
        if (selDate == currDate) {
            $("a#next").css("pointer-events", "none").css("color", "#ccccc");
        } else {
            $("a#next").css("pointer-events", "visible").css("color", "#337ab7");
        }
        $("#btnReset").click();
        $("#txtPaymentId").val("");
        $("#SubcontractorPayment_date").val(selDate);
        $("#Dailyexpense_vendor_id").html('<option value="">-Select Vendor-</option>');
        $("#Dailyexpense_vendor_id").attr("disabled", true);
    }

    function validateData() {
        var billNumber = $("#Dailyexpense_bill_id").val();
        var expenseType = $("#Dailyexpense_expensehead_id").val();
        var vendorId = $("#Dailyexpense_vendor_id").val();
        var amount = $("#txtAmount").val();
        var sgstP = $("#txtSgstp").val();
        var sgst = $("#txtSgst").val();
        var cgstP = $("#txtCgstp").val();
        var cgst = $("#txtCgst").val();
        var total = $("#txtTotal").val();
        var receiptType = $("#Dailyexpense_dailyexpense_receipt_type").val();
        var receipt = $("#txtReceipt").val();
        var purchaseType = $("#txtPurchaseType").val();
        var paid = $("#txtPaid").val();
        var currId = $(this).attr(id);
        if (expenseType != "" || expenseType != 0) {
            $("#Dailyexpense_dailyexpense_receipt_type").attr("disabled", true);
            $("#txtReceipt").attr("readonly", true);
            $("#Dailyexpense_vendor_id").val("");
            $("#Dailyexpense_vendor_id").attr("disabled", false);
            $("#txtAmount").attr("readonly", false);
            $("#txtSgstp").attr("readonly", false);
            $("#txtSgst").attr("readonly", false);
            $("#txtCgstp").attr("readonly", false);
            $("#txtCgst").attr("readonly", false);
            $("#txtTotal").attr("readonly", false);
        }
        if (currId == "Dailyexpense_expensehead_id" && (expenseType == "" || expenseType == 0)) {
            $("#Dailyexpense_dailyexpense_receipt_type").attr("disabled", false);
            $("#txtReceipt").attr("readonly", false);
        }
    }

    function getCalculations() {
        var sgstp = $("#SubcontractorPayment_sgst").val();
        var cgstp = $("#SubcontractorPayment_cgst").val();
        var igstp = $("#SubcontractorPayment_igst").val();
        var totRow = $("#txtRows").val();
        var currRow = $("#txtCurrentRows").val();
        var totAmt = 0;
        var totTax = 0;
        var totTds = 0;
        var totPaid = 0;
        var i;
        for (i = 1; i <= totRow; i++) {
            var project = $("#SubcontractorPayment_project_id" + i).val();
            if (project != "") {
                var amount = ($("#SubcontractorPayment_amount" + i).val()) ? parseFloat($("#SubcontractorPayment_amount" + i).val()) : 0;
                var sgst = parseFloat((sgstp / 100) * amount);
                var cgst = parseFloat((cgstp / 100) * amount);
                var igst = parseFloat((igstp / 100) * amount);
                var totalTax = parseFloat(sgst + cgst + igst);
                var rowTotal = parseFloat(amount + totalTax);
                var tds = ($("#SubcontractorPayment_tds" + i).val()) ? parseFloat($("#SubcontractorPayment_tds" + i).val()) : 0;
                var tdsamnt = parseFloat((tds / 100) * rowTotal);
                var paidamnt = parseFloat(rowTotal - tdsamnt);
                totAmt = parseFloat(totAmt + rowTotal);
                totTax = parseFloat(totTax + totalTax);
                totTds = totTds + tdsamnt;
                totPaid = totPaid + paidamnt;
                $(".gstvalue").css("display", "inline-block");
                $("#txtSgst" + i).text(sgst.toFixed(2));
                $("#txtCgst" + i).text(cgst.toFixed(2));
                $("#txtIgst" + i).text(igst.toFixed(2));
                $("#txtgstTotal" + i).html("" + totalTax.toFixed(2));
                $("#txtTotal" + i).html("" + rowTotal.toFixed(2));
                $("#txtTds" + i).text(tdsamnt.toFixed(2));
                $("#SubcontractorPayment_tds_amount" + i).val(tdsamnt.toFixed(2));
                $("#txtPaidAmount" + i).html(paidamnt.toFixed(2));
                $("#SubcontractorPayment_paidamount" + i).val(paidamnt.toFixed(2));
            }
        }
        $("#txtgrandtot").text(totAmt.toFixed(2));
        $("#txttdsamount").text(totTds.toFixed(2));
        $("#txtamountPaid").text(totPaid.toFixed(2));
    }

    function validateRows() {
        var totRow1 = $("#txtRows").val();
        var j;
        for (j = 1; j <= totRow1; j++) {
            var project = $("#SubcontractorPayment_project_id" + j).val();
            if (project != "") {
                var amount1 = $("#SubcontractorPayment_amount" + j).val();
                if (amount1 == "" || amount1 <= 0) {
                    $("#errorMessage" + j).html("<div>Amount column must not be empty.</div>");
                    $("#errormessage").show().html("<div class='alert alert-danger' role='alert'>All amount field must be greater than 0.</div>");
                    $('html, body').animate({
                    scrollTop: $("#errormessage").offset().top- 80
                    }, '100');
                    $("#buttonsubmit").attr("disabled", true);
                } else {
                    $("#errorMessage" + j).html("");
                    $("#buttonsubmit").attr("disabled", false);
                    $("#errormessage").html("");
                }
            }
        }
    }

    function validateSubcontractorLimit() {
        var company = $("#SubcontractorPayment_company_id").val();
        var subcon = $("#SubcontractorPayment_subcontractor_id").val();
        var sgstp = $("#SubcontractorPayment_sgst").val();
        var cgstp = $("#SubcontractorPayment_cgst").val();
        var igstp = $("#SubcontractorPayment_igst").val();
        var totRow = $("#txtRows").val();
        var currRow = $("#txtCurrentRows").val();
        var paymentId = $("#txtPaymentId").val();
        var quotation_num = $('#SubcontractorPayment_quotation_number1').val();
        var status;
        if (paymentId == "")
            status = "add";
        else
            status = "update";
        var i;
        var count = 0;
        for (i = 1; i <= totRow; i++) {
            var project = $("#SubcontractorPayment_project_id" + i).val();
            if (project != "") {
                var amount = ($("#SubcontractorPayment_amount" + i).val()) ? parseFloat($("#SubcontractorPayment_amount" + i).val()) : 0;
                var sgst = parseFloat((sgstp / 100) * amount);
                var cgst = parseFloat((cgstp / 100) * amount);
                var igst = parseFloat((igstp / 100) * amount);
                var totalTax = parseFloat(sgst + cgst + igst);
                var rowTotal = parseFloat(amount + totalTax);
                $('#loading').show();
                $.ajax({
                    url: "<?php echo $validateSubcontractorLimit; ?>",
                    data: {
                        "company": company,
                        "subcon": subcon,
                        "totRow": totRow,
                        "project": project,
                        "rowTotal": rowTotal,
                        "status": status,
                        "rownum": i,
                        "paymentId": paymentId,
                        "quotation_num": quotation_num
                    },
                    type: "GET",
                    success: function(data) {
                        var result = JSON.parse(data);
                        if (result["message"] != 1) {

                            $("#errorMessage" + result["rownum"]).html("<div id='error_div' class='show'>" + result["message"] + "</div>");
                            $("#txtPermission" + result["rownum"]).val("No");
                        } else {

                            $("#error_div").addClass("hide");
                            $("#error_div").removeClass("show");
                            $("#txtPermission" + result["rownum"]).val("Yes");
                        }
                        $("#txtCount").val(count);
                    }
                });
            }
        }
    }

    function submitButtonStatus() {
        setTimeout(
            function() {
                var errCount = $("#txtCount").val();
                if (errCount > 0) {
                    $("#buttonsubmit").attr("disabled", true);
                } else {
                    $("#buttonsubmit").attr("disabled", false);
                }
            },
            200);
    }

    function addSubcontractorPayment() {
        var date = $("#SubcontractorPayment_date").val();
        var company = $("#SubcontractorPayment_company_id").val();
        var subcon = $("#SubcontractorPayment_subcontractor_id").val();
        var sgstp = $("#SubcontractorPayment_sgst").val();
        var cgstp = $("#SubcontractorPayment_cgst").val();
        var igstp = $("#SubcontractorPayment_igst").val();
        var pType = $("#SubcontractorPayment_payment_type").val();
        var paymentId = $("#txtPaymentId").val();
        var employee_id = $("#SubcontractorPayment_employee_id").val();

        if (paymentId == "") {
            localStorage.setItem("action", "add");
            $url = "<?php echo $addSubcontractorPayment; ?>";
            
        } else {
            localStorage.setItem("action", "update");
            $url = "<?php echo $updateSubcontractorPayment; ?>";
            
        }
        if (pType == 88) {
            var bank = $("#SubcontractorPayment_bank").val();
            var cheque = $("#SubcontractorPayment_cheque_no").val();
        } else {
            var bank = "";
            var cheque = "";
        }
        var description = $("#SubcontractorPayment_description").val();

        var totRow = $("#txtRows").val();
        var currRow = $("#txtCurrentRows").val();
        var totAmt = 0;
        var totTax = 0;
        var totTds = 0;
        var totPaid = 0;
        var projects = new Array();
        var amount = new Array();
        var sgst = new Array();
        var cgst = new Array();
        var igst = new Array();
        var totalTax = new Array();
        var rowTotal = new Array();
        var permission = new Array();
        var tds = new Array();
        var tdsamount = new Array();
        var rowpaid = new Array();
        var quotation_no = new Array();
        var payment_type = new Array();
        var refIds = new Array();
        var stage = new Array();

        var count = 0;
        for (var i = 1; i <= totRow; i++) {
            var project = $("#SubcontractorPayment_project_id" + i).val();

            if (project != "") {
                projects[count] = $("#SubcontractorPayment_project_id" + i).val();
                payment_type[count] = $("#SubcontractorPayment_payment_quotation_status" + i).val();
                quotation_no[count] = $("#SubcontractorPayment_quotation_number" + i).val();
                stage[count] = $("#SubcontractorPayment_stage" + i).val();
                amount[count] = ($("#SubcontractorPayment_amount" + i).val()) ? parseFloat($("#SubcontractorPayment_amount" + i).val()) : 0;
                sgst[count] = parseFloat((sgstp / 100) * amount[count]);
                cgst[count] = parseFloat((cgstp / 100) * amount[count]);
                igst[count] = parseFloat((igstp / 100) * amount[count]);
                totalTax[count] = parseFloat(sgst[count] + cgst[count] + igst[count]);
                rowTotal[count] = parseFloat(amount[count] + totalTax[count]);
                tds[count] = ($("#SubcontractorPayment_tds" + i).val()) ? parseFloat($("#SubcontractorPayment_tds" + i).val()) : 0;
                tdsamount[count] = parseFloat((tds[count] / 100) * rowTotal[count]).toFixed(2);
                rowpaid[count] = parseFloat(rowTotal[count] - tdsamount[count]).toFixed(2);
                permission[count] = $("#txtPermission" + i).val();
                refIds[count] = $("#Payment_id" + i).val();
                if (permission[count] == "Yes") {
                    totAmt = parseFloat(totAmt) + parseFloat(rowTotal[count]);
                    totPaid = parseFloat(totPaid) + parseFloat(rowpaid[count]);
                }
                totTax = parseFloat(totTax + totalTax[count]);
                count = count + 1;
            }
        }
        $.ajax({
            url: $url,
            data: {
                "date": date,
                "company_id": company,
                "subcontractor_id": subcon,
                "sgst": sgstp,
                "cgst": cgstp,
                "igst": igstp,
                "payment_type": pType, //cash or cheque
                "bank": bank,
                "cheque_no": cheque,
                "description": description,
                "projects": projects,
                "amount": amount,
                "sgst_amount": sgst,
                "cgst_amount": cgst,
                "igst_amount": igst,
                "totalTax": totalTax,
                "rowTotal": rowTotal,
                "totAmt": totAmt,
                "totTax": totTax,
                "count": count,
                "permission": permission,
                "paymentId": paymentId,
                "tds": tds,
                "tdsamount": tdsamount,
                "rowpaid": rowpaid,
                "totPaid": totPaid,
                "payment_mode": payment_type, //w or w/o quoattion
                "quotation_no": quotation_no,
                "refIds":refIds,
                "stage":stage,
                "employee_id":employee_id
            },
            type: "GET",
            dataType: "JSON",
            success: function(data) {

                $('.loading-overlay').removeClass('is-active');
                if (data.status == "Edit") {
                    $("#errormessage").show()
                        .html('<div class="alert alert-success"><strong>Request Send !</strong> Update request send to admin for approval.ID#' + data.id + '</div>')
                        .fadeOut(5000);
                        $('html, body').animate({
                        scrollTop: $("#errormessage").offset().top- 80
                        }, '100');
                    $.ajax({
                        type: "POST",
                        data: {
                            date: date
                        },
                        url: "<?php echo Yii::app()->createUrl("subcontractorpayment/getAllData") ?>",
                        success: function(result) {
                            $("#buttonsubmit").text("ADD");
                            var currAction = localStorage.getItem("action");
                            if (currAction == "add") {
                                var today = new Date();
                                var dd = today.getDate();
                                var mm = today.getMonth() + 1; //January is 0!
                                var yyyy = today.getFullYear();
                                if (dd < 10)
                                    dd = '0' + dd
                                if (mm < 10)
                                    mm = '0' + mm
                                today = dd + '-' + mm + '-' + yyyy;
                                $("#entrydate").text(date);
                            } else if (currAction == "update") {

                            }
                            $("#SubcontractorPayment_date").val(date);
                            $('#SubcontractorPayment_company_id').val("").trigger('change.select2');
                            $('#SubcontractorPayment_subcontractor_id').val("").trigger('change.select2');
                            $('.singleproject').val("").trigger('change.select2');
                            $('.tdsrate').val("");
                            $('#SubcontractorPayment_bank').val("").trigger('change.select2');
                            $('#SubcontractorPayment_cheque_no').val("");
                            $(".permission").val("");
                            $(".hiden_box").hide();
                            $("#SubcontractorPayment_payment_type").val("").trigger('change.select2');
                            $("#SubcontractorPayment_employee_id").val("").trigger('change.select2');
                            $(".sub_paymentamount").val("");
                            $(".gstvalue").val("");
                            $(".gstvalue").text("");
                            $('div.paymentrow').not(':eq(0)').remove();
                            $("#txtRows").val(1);
                            $("#txtCurrentRows").val(1);
                            $("#txtCount").val(0);
                            $(".rowerror").html("");
                            $('div.rowerror').not(':eq(0)').remove();
                            $("#SubcontractorPayment_description").val("");
                            $("#SubcontractorPayment_quotation_number1").val("");
                            $("#SubcontractorPayment_company_id").focus();
                            $("#newlist").html(result);
                        }
                    });
                } else if (data.status == 1 || data.status == 5) {
                    if (data.status == 1) {
                        var message = data.error_message;
                    } else if (data.status == 5) {
                        $('html, body').animate({
                            scrollTop: $("#dailyexpense-form").offset().top
                        }, 200);
                        var message = "Transaction Unsuccessfull. User has insufficient balance!";
                    }
                    $("#errormessage").show()
                        .html('<div class="alert alert-warning font-weight-bold"><i class="fa fa-exclamation-circle pr-2 fa-lg"></i>' + message + '</div>')
                        .fadeOut(15000);
                        $('html, body').animate({
                        scrollTop: $("#errormessage").offset().top- 80
                        }, '100');
                    $("#buttonsubmit").attr('disabled', false);
                    return false;
                } else {
                    console.log('sdfsdfsdf');
                    $("#buttonsubmit").text("ADD");
                    var currAction = localStorage.getItem("action");
                    if (currAction == "add") {
                        $("#errormessage").show()
                            .html('<div class="alert alert-success"><strong>Successfully !</strong> record added.ID#' + data.id + '</div>')
                            .fadeOut(5000);
                            $('html, body').animate({
                            scrollTop: $("#errormessage").offset().top- 80
                            }, '100');
                        var today = new Date();
                        var dd = today.getDate();
                        var mm = today.getMonth() + 1; //January is 0!
                        var yyyy = today.getFullYear();
                        if (dd < 10)
                            dd = '0' + dd
                        if (mm < 10)
                            mm = '0' + mm
                        today = dd + '-' + mm + '-' + yyyy;
                        $.ajax({
                            type: "POST",
                            data: {
                                date: date
                            },
                            url: "<?php echo Yii::app()->createUrl("subcontractorpayment/getAllData") ?>",
                            success: function(result) {
                                $("#buttonsubmit").text("ADD");
                                var currAction = localStorage.getItem("action");
                                if (currAction == "add") {
                                    var today = new Date();
                                    var dd = today.getDate();
                                    var mm = today.getMonth() + 1; //January is 0!
                                    var yyyy = today.getFullYear();
                                    if (dd < 10)
                                        dd = '0' + dd
                                    if (mm < 10)
                                        mm = '0' + mm
                                    today = dd + '-' + mm + '-' + yyyy;
                                    $("#entrydate").text(date);
                                } else if (currAction == "update") {

                                }
                                $("#SubcontractorPayment_date").val(date);
                                $('#SubcontractorPayment_company_id').val("").trigger('change.select2');
                                $('#SubcontractorPayment_subcontractor_id').val("").trigger('change.select2');
                                $('.singleproject').val("").trigger('change.select2');
                                $('.tdsrate').val("");
                                $("#SubcontractorPayment_sgst").val("");
                                $("#SubcontractorPayment_cgst").val("");
                                $("#SubcontractorPayment_igst").val("");
                                $("#SubcontractorPayment_quotation_number1").val("").trigger('change.select2');
                                $('#SubcontractorPayment_bank').val("").trigger('change.select2');
                                $('#SubcontractorPayment_cheque_no').val("");
                                $(".permission").val("");
                                $(".hiden_box").hide();
                                $("#SubcontractorPayment_payment_type").val("").trigger('change.select2');
                                $("#SubcontractorPayment_employee_id").val("").trigger('change.select2');
                                $(".sub_paymentamount").val("");
                                $(".gstvalue").val("");
                                $(".gstvalue").text("");
                                $('div.paymentrow').not(':eq(0)').remove();
                                $("#txtRows").val(1);
                                $("#txtCurrentRows").val(1);
                                $("#txtCount").val(0);
                                $(".rowerror").html("");
                                $('div.rowerror').not(':eq(0)').remove();
                                $("#SubcontractorPayment_description").val("");
                                $("#SubcontractorPayment_quotation_number1").val("");
                                $("select.qtn_list").val("").trigger('change.select2');
                                
                                $("select.stage_list").val("").trigger('change.select2');
                                $("#SubcontractorPayment_company_id").focus();
                                $("#newlist").html(result);
                            }
                        });
                        $("#entrydate").text(date);
                    } else if (currAction == "update") {
                        $("#errormessage").show()
                            .html('<div class="alert alert-success"><strong>Successfully !</strong> record updated.</div>')
                            .fadeOut(5000);
                            $('html, body').animate({
                            scrollTop: $("#errormessage").offset().top- 80
                            }, '100');
                    }
                    $("#SubcontractorPayment_date").val(date);
                    $('#SubcontractorPayment_company_id').val("").trigger('change.select2');
                    $('#SubcontractorPayment_subcontractor_id').val("").trigger('change.select2');
                    $('.singleproject').val("").trigger('change.select2');
                    $('.tdsrate').val("");
                    $('#SubcontractorPayment_bank').val("").trigger('change.select2');
                    $('#SubcontractorPayment_cheque_no').val("");
                    $(".permission").val("");
                    $(".hiden_box").hide();
                    $("#SubcontractorPayment_payment_type").val("").trigger('change.select2');
                    $("#SubcontractorPayment_employee_id").val("").trigger('change.select2');
                    $(".sub_paymentamount").val("");
                    $(".gstvalue").val("");
                    $(".gstvalue").text("");
                    $('div.paymentrow').not(':eq(0)').remove();
                    $("#txtRows").val(1);
                    $("#txtCurrentRows").val(1);
                    $("#txtCount").val(0);
                    $(".rowerror").html("");
                    $('div.rowerror').not(':eq(0)').remove();
                    $("#SubcontractorPayment_description").val("");
                    $("#SubcontractorPayment_company_id").focus();
                    var today = new Date();
                    var dd = today.getDate();
                    var mm = today.getMonth() + 1; //January is 0!
                    var yyyy = today.getFullYear();
                    if (dd < 10)
                        dd = '0' + dd
                    if (mm < 10)
                        mm = '0' + mm
                    today = dd + '-' + mm + '-' + yyyy;
                    $.ajax({
                        type: "POST",
                        data: {
                            date: date
                        },
                        url: "<?php echo Yii::app()->createUrl("subcontractorpayment/getAllData") ?>",
                        success: function(result) {
                            $("#buttonsubmit").text("ADD");
                            var currAction = localStorage.getItem("action");
                            if (currAction == "add") {
                                var today = new Date();
                                var dd = today.getDate();
                                var mm = today.getMonth() + 1; //January is 0!
                                var yyyy = today.getFullYear();
                                if (dd < 10)
                                    dd = '0' + dd
                                if (mm < 10)
                                    mm = '0' + mm
                                today = dd + '-' + mm + '-' + yyyy;
                                $("#entrydate").text(date);
                            } else if (currAction == "update") {

                            }
                            $("#SubcontractorPayment_date").val(date);
                            $('#SubcontractorPayment_company_id').val("").trigger('change.select2');
                            $('#SubcontractorPayment_subcontractor_id').val("").trigger('change.select2');
                            $('.singleproject').val("").trigger('change.select2');
                            $('.tdsrate').val("");
                            $('#SubcontractorPayment_bank').val("").trigger('change.select2');
                            $('#SubcontractorPayment_cheque_no').val("");
                            $(".permission").val("");
                            $(".hiden_box").hide();
                            $("#SubcontractorPayment_payment_type").val("").trigger('change.select2');
                            $("#SubcontractorPayment_employee_id").val("").trigger('change.select2');
                            $(".sub_paymentamount").val("");
                            $(".gstvalue").val("");
                            $(".gstvalue").text("");
                            $('div.paymentrow').not(':eq(0)').remove();
                            $("#txtRows").val(1);
                            $("#txtCurrentRows").val(1);
                            $("#txtCount").val(0);
                            $(".rowerror").html("");
                            $('div.rowerror').not(':eq(0)').remove();
                            $("#SubcontractorPayment_description").val("");
                            $("#SubcontractorPayment_quotation_number1").val("");
                            $("#SubcontractorPayment_sgst").val("");
                            $("#SubcontractorPayment_cgst").val("");
                            $("#SubcontractorPayment_igst").val("");
                            $("select.qtn_list").val("").trigger('change.select2');
                             $("select.stage_list").val("").trigger('change.select2');
                            $("#SubcontractorPayment_company_id").focus();
                            $("#newlist").html(result);
                        }
                    });
                    $("#entrydate").text(date);
                }
                $('#txtPaymentId').val("");

                
            },
            complete:function(){
                setTimeout(function () {                    
                    location.reload(true);
                }, 2000);
            }
        });
    }

    function resetForm(newDate) {
        document.getElementById("dailyexpense-form").reset();
        $("#SubcontractorPayment_date").val(newDate);
        $('#SubcontractorPayment_company_id').val("").trigger('change.select2');
        $('#SubcontractorPayment_subcontractor_id').val("").trigger('change.select2');
        $('.gstvalue').text("");
        $('#SubcontractorPayment_bank').val("").trigger('change.select2');
        $('#SubcontractorPayment_cheque_no').val("");
        $(".sub_paymentamount").val("");
        $(".permission").val("");
        $("#SubcontractorPayment_subcontractor_id").focus();
        $("#buttonsubmit").attr('disabled', false);
        $(".hiden_box").hide();
        $(".addmoresection").html("");
        $("#SubcontractorPayment_payment_type").val("").trigger('change.select2');
        $("#SubcontractorPayment_employee_id").val("").trigger('change.select2');
        $("#SubcontractorPayment_project_id1").val("").trigger('change.select2');
        $("#SubcontractorPayment_quotation_number1").val("").trigger('change.select2');
        $(".tdsrate").val("");
        $("input[type='hidden']").val("");
        $('input[id="SubcontractorPayment_payment_quotation_status_0"]').val(1);
        $('input[id="SubcontractorPayment_payment_quotation_status_1"]').val(2);

        $('#txtPaymentId').val("");
        $("#errormessage").html("");
        var buttonClass = $("#dailyformbutton").attr("class");
        if (buttonClass.indexOf("collapsed") == -1) {
            $(".addentries").click();
        }
    }
    $(function() {
        $("#SubcontractorPayment_date").datepicker({
            maxDate: new Date()
        });
    });
    $(document).ajaxComplete(function() {
        $('.loading-overlay').removeClass('is-active');
        $('#loading').hide();
        if ($('#errormessage').is(':empty')){
            $("#buttonsubmit").attr('disabled', false);
        }
        
    });
    $("#SubcontractorPayment_cheque_no").change(function (e) {        
        var cheque_no = $(this).val();
        var bank = $("#SubcontractorPayment_bank").val();
        $.ajax({
            type:'POST',               
            data: {
                cheque_no: cheque_no, 
                bank:bank                   
            },
            url: "<?php echo Yii::app()->createUrl("expenses/testChequeNumber") ?>",
            success: function (data) {
                $('.chq_error').html(data);
                return;                
            }
        })
    });

    $(".sc_quotation_number").change(function(){
        var elem = $(this);
        var id =$(this).val();
        var entry_date = $("#SubcontractorPayment_date").val();
        $.ajax({
            type:'POST',
            dataType:'json',
            data:{
                scquotation_id:id,
                entry_date:entry_date
            },
            url:"<?php echo Yii::app()->createUrl("subcontractorpayment/checkQuotationDate") ?>",
            success:function(response){
                
                if(response.response=="error"){  
                    $("#buttonsubmit").attr('disabled', true); 
                    console.log('err');
                    $("#errormessage").show()
                    .html('<div class="alert alert-danger">'+response.msg+'</div>');
                                       
                }else{
                    console.log('success');
                    $("#errormessage").hide();
                    $("#buttonsubmit").attr('disabled', false);
                    if(response.stage != ""){
                        $(elem).parents(".qtn_number_div").siblings(".payment_stage_div").attr("data-count",response.stage_count)
                        $(elem).parents(".qtn_number_div").siblings(".payment_stage_div").removeClass("hide");
                        $(elem).parents(".qtn_number_div").siblings(".payment_stage_div").find(".sc_payment_stage").html(response.stage);
                    }else{
                        $(elem).parents(".qtn_number_div").siblings(".payment_stage_div").addClass("hide");
                        $(elem).parents(".qtn_number_div").siblings(".payment_stage_div").find(".sc_payment_stage").html(response.stage);                        
                    }
                    $("#SubcontractorPayment_sgst").val(response.sgst);
                    $("#SubcontractorPayment_cgst").val(response.cgst);
                    $("#SubcontractorPayment_igst").val(response.igst);

                    
                }
            }
        })
       
    })

    $(".sc_payment_stage").change(function(){
        var elem =$(this);
        var id = $(this).val();        
        $.ajax({
            type:'POST',            
            data:{
                id:id                
            },
            url:"<?php echo Yii::app()->createUrl("subcontractorpayment/getstageamount") ?>",
            success:function(response){
                $(elem).siblings(".stage_amount").val(response);
                $('.sub_paymentamount').trigger('change');
                console.log(response)
            }
        })
    })
    
</script>