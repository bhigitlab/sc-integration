<?php
/* @var $this WarehousestockController */
/* @var $model Warehousestock */

$this->breadcrumbs=array(
	'Warehousestocks'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Warehousestock', 'url'=>array('index')),
	array('label'=>'Create Warehousestock', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#warehousestock-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Warehousestocks</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'warehousestock-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'warehousestock_id',
		'warehousestock_warehouseid',
		'warehousestock_itemid',
		'warehousestock_date',
		'warehousestock_stock_quantity',
		'warehousestock_unit',
		/*
		'warehousestock_status',
		'created_date',
		'created_by',
		'updated_date',
		'updated_by',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
