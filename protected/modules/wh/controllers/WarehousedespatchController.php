<?php

class WarehousedespatchController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array(
                'allow',  // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view'),
                'users' => array('@'),
            ),
            array(
                'allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'saveDespatchOrAutoReceipt', 'chechAvailableQuantity', 'CheckEditDespatch', 'update', 'getItemCategory', 'getParent', 'ajax', 'createnewdespatch', 'despatchitem', 'getUnits', 'getUnit', 'updatedespatchitem', 'removeitem', 'saveaspdf', 'saveasexcel', 'getBatch', 'getDimensions', 'getUnitconversionFactor'),
                'users' => array('@'),
            ),
            array(
                'allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('admin'),
            ),
            array(
                'deny',  // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($despatchid)
    {
        $warehouse_address = "";
        $model = $this->loadModel($despatchid);
        $warehousemodel = Warehouse::model()->findByPk($model->warehousedespatch_warehouseid);
        if ($warehousemodel === null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        } else {
            $warehouse_address = $warehousemodel->warehouse_address;
        }

        if (filter_input(INPUT_GET, 'export') == 'pdf') {
            $mPDF1 = Yii::app()->ePdf->mPDF();
            $mPDF1 = Yii::app()->ePdf->mPDF('', 'A4');
            $mPDF1->WriteHTML($this->renderPartial('view', array(
                'model' => $model, 'warehouse_address' => $warehouse_address
            ), true));
            $mPDF1->Output('Receipt.pdf', 'D');

        } elseif (filter_input(INPUT_GET, 'export') == 'csv') {
            $exported_data = $this->renderPartial('view', array(
                'model' => $model, 'warehouse_address' => $warehouse_address
            ), true);
            $export_filename = 'Receipt' . date('Ymd_His');
            ExportHelper::exportGridAsCSV($exported_data, $export_filename);
            
        } else {
            $this->render('view', array(
                'model' => $model, 'warehouse_address' => $warehouse_address
            ));
        }
    }
    public function actionCheckEditDespatch()
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $despatch_id = $_GET['despatch_id'];
        $warehouseReceipt_checkSql = "SELECT * 
                                    FROM " . $tblpx . "warehousereceipt 
                                    WHERE `warehousereceipt_despatch_id` ='" . $despatch_id . "' AND delete_status = 1";
        $warehouseReceipt_check = Yii::app()->db->createCommand($warehouseReceipt_checkSql)->queryRow();
        if ($warehouseReceipt_check != '') {
            echo json_encode(array('response' => 'error', 'msg' => 'This Item is not editable.Because it already transfered to warehouse'));
        } else {
            echo json_encode(array('response' => 'success'));
        }
    }

    public function actionsaveaspdf($id)
    {

        $warehouse_address = "";
        $model = $this->loadModel($id);
        $warehousemodel = Warehouse::model()->findByPk($model->warehousedespatch_warehouseid);
        if ($warehousemodel === null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        } else {
            $warehouse_address = $warehousemodel->warehouse_address;
        }

        $mPDF1 = Yii::app()->ePdf->mPDF();

        $mPDF1 = Yii::app()->ePdf->mPDF('', 'A4');

        $mPDF1->WriteHTML($stylesheet, 1);

        $mPDF1->WriteHTML($this->renderPartial('pdfview', array('model' => $this->loadModel($id), 'warehouse_address' => $warehouse_address), true));
        $mPDF1->Output('Despatch.pdf', 'D');
    }

    public function actionajax()
    {
        echo json_encode('success');
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $model = new Warehousedespatch;
        $project = Yii::app()->db->createCommand("SELECT DISTINCT pid,name FROM {$tblpx}projects WHERE company_id=" . Yii::app()->user->company_id . " ORDER BY name")->queryAll();

        $specification = $this->actionGetItemCategory($parent = 0, $spacing = '', $user_tree_array = '');
        $this->render('create', array(
            'model' => $model,
            'dataProvider' => $model->search(Yii::app()->user->id),
            'specification' => $specification,
            'project' => $project
        ));
    }

    public function actionGetItemCategory($parent = 0, $spacing = '', $user_tree_array = '')
    {
        if (!is_array($user_tree_array))
            $user_tree_array = array();
        $data = array();
        $final = array();
        $tblpx = Yii::app()->db->tablePrefix;
        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        $newQuery = "";
        foreach ($arrVal as $arr) {
            if ($newQuery)
                $newQuery .= ' OR';
            $newQuery .= " FIND_IN_SET('" . $arr . "', company_id)";
        }
        $spec_sql = "SELECT id, specification, brand_id, cat_id "
            . " FROM {$tblpx}specification "
            . " WHERE (".$newQuery .") ";
        $specification = Yii::app()->db->createCommand($spec_sql)->queryAll();
        
        foreach ($specification as $key => $value) {
            if ($value['brand_id'] != NULL) {
                $brand_details = Yii::app()->db->createCommand("SELECT brand_name FROM {$tblpx}brand WHERE id=" . $value['brand_id'] . "")->queryRow();
                $brand = '-' . ucfirst($brand_details['brand_name']);
            } else {
                $brand = '';
            }

            if ($value['cat_id'] != '') {
                $result = $this->actionGetParent($value['cat_id']);
                $final[$key]['data'] = ucfirst($result['category_name']) . $brand . '-' . ucfirst($value['specification']);
            } else {
                $final[$key]['data'] = $brand . '-' . $value['specification'];
            }



            $final[$key]['id'] = $value['id'];
        }
       
        return $final;
    }
    public function updateGetItemCategory($parent = 0, $spacing = '', $user_tree_array = '', $warehouse_from, $warehouse_to)
    {
        $tblpx = Yii::app()->db->tablePrefix;
        if (!is_array($user_tree_array))
            $user_tree_array = array();
        $data = array();
        $final = array();
        $stock_item_arr = array();
        $stock_datas_sql  = "SELECT * FROM {$tblpx}warehousestock WHERE warehousestock_warehouseid=" . $warehouse_from . "";
        $stock_datas = Yii::app()->db->createCommand($stock_datas_sql)->queryAll();
        foreach ($stock_datas as $key => $value) {
            $stock_item_arr[] = $value['warehousestock_itemid'];
        }


        $spec_sql = "SELECT id, specification, brand_id, cat_id "
            . " FROM {$tblpx}specification "
            . " WHERE  company_id=" . Yii::app()->user->company_id . "";
        $specification = Yii::app()->db->createCommand($spec_sql)->queryAll();


        foreach ($specification as $key => $value) {
            if ($value['brand_id'] != NULL) {
                $brand_details = Yii::app()->db->createCommand("SELECT brand_name FROM {$tblpx}brand WHERE id=" . $value['brand_id'] . "")->queryRow();
                $brand = '-' . ucfirst($brand_details['brand_name']);
            } else {
                $brand = '';
            }
            if (in_array($value['id'], $stock_item_arr)) {
                if ($value['cat_id'] != '') {
                    $result = $this->actionGetParent($value['cat_id']);
                    $final[$key]['data'] = ucfirst($result['category_name']) . $brand . '-' . ucfirst($value['specification']);
                } else {
                    $final[$key]['data'] = $brand . '-' . $value['specification'];
                }
                $final[$key]['id'] = $value['id'];
            }
        }
        // echo "<pre>";print_r($final);exit();

        return $final;
    }
    public function actionGetParent($id)
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $category = Yii::app()->db->createCommand("SELECT parent_id, id , category_name FROM {$tblpx}purchase_category WHERE id=" . $id)->queryRow();

        return $category;
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($despatchid)
    {
        $model = $this->loadModel($despatchid);
        $tblpx = Yii::app()->db->tablePrefix;
        if (empty($model)) {
            $this->redirect(array('warehousedespatch/index'));
        }
        $project = Yii::app()->db->createCommand("SELECT DISTINCT pid,name FROM {$tblpx}projects WHERE company_id=" . Yii::app()->user->company_id . " ORDER BY name")->queryAll();
        $warehouse_from = $model->warehousedespatch_warehouseid;
        $warehouse_to = $model->warehousedespatch_warehouseid_to;
        $specification = $this->updateGetItemCategory($parent = 0, $spacing = '', $user_tree_array = '', $warehouse_from, $warehouse_to);


        $item_model_sql = "SELECT wdi.*,ws.warehousestock_id 
                            FROM " . $this->tableNameAcc('warehousedespatch_items', 0) . " as wdi INNER JOIN " . $this->tableNameAcc('warehousestock', 0) . " as ws ON wdi.warehousedespatch_warehouseid = ws.warehousestock_warehouseid 
                            AND wdi.warehousedespatch_itemid = ws.warehousestock_itemid 
                            AND wdi.warehousestock_itemid_dimension_category = ws.warehousestock_itemid_dimension_category 
                            AND wdi.warehousedespatch_rate = ws.rate WHERE wdi.warehousedespatch_id ='" . $despatchid . "'
                            AND ((wdi.warehousedespatch_batch IS NULL AND ws.batch IS NULL) OR wdi.warehousedespatch_batch = ws.batch)";

        $item_model = Yii::app()->db->createCommand($item_model_sql)->queryAll();

        $this->render('update', array(
            'model' => $model,
            'dataProvider' => $model->search(Yii::app()->user->id),
            'specification' => $specification,
            'item_model' => $item_model,
            'project' => $project
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $specification = '';
        $specification = $this->actionGetItemCategory($parent = 0, $spacing = '', $user_tree_array = '');

        $model = new Warehousedespatch('search');
        $tblpx = Yii::app()->db->tablePrefix;
        $query = "";
        if (Yii::app()->user->role != 1) {
            $query = " AND FIND_IN_SET(" . Yii::app()->user->id . ", assigned_to)";
        } else {
            $query = "";
        }
        $warehouse_sql = "SELECT DISTINCT warehouse_id,warehouse_name 
                                    FROM {$tblpx}warehouse 
                                    WHERE company_id=" . Yii::app()->user->company_id . "  " . $query . " 
                                    ORDER BY warehouse_id";
        $warehouse = Yii::app()->db->createCommand($warehouse_sql)->queryAll();
        $vendor_sql = "SELECT DISTINCT vendor_id,name 
                                FROM {$tblpx}vendors 
                                WHERE company_id=" . Yii::app()->user->company_id . " 
                                ORDER BY vendor_id";
        $vendor = Yii::app()->db->createCommand($vendor_sql)->queryAll();
        $clerk_sql = "SELECT DISTINCT userid,first_name,last_name 
                                FROM {$tblpx}users 
                                WHERE company_id=" . Yii::app()->user->company_id . " 
                                ORDER BY userid";
        $clerk = Yii::app()->db->createCommand($clerk_sql)->queryAll();
        $model->unsetAttributes();  // clear any default values
        $sql = "SELECT DISTINCT wtda.`warehousedespatch_id`,wtda.* FROM `warehouse_transfer_details_all` as wtda ";
        $sql1 = " WHERE 1=1";
        if (isset($_GET['warehouse']) || isset($_GET['warehouse_to']) || isset($_GET['vendor_id']) || isset($_GET['clerk']) || isset($_GET['despatch_no']) || isset($_GET['item_id'])) {
            $model->warehousedespatch_warehouseid = $_GET['warehouse'];
            $model->warehousedespatch_warehouseid_to = $_GET['warehouse_to'];
            $model->warehousedespatch_clerk = $_GET['clerk'];
            $model->warehousedespatch_no = $_GET['despatch_no'];
            $model->date_from = $_GET['date_from'];
            $model->date_to = $_GET['date_to'];
            if (isset($_GET['warehouse']) && !empty($_GET['warehouse'])) {
                $sql1 .= ' AND wtda.warehousedespatch_warehouseid =' . $_GET['warehouse'];
            }
            if (isset($_GET['warehouse_to']) && !empty($_GET['warehouse_to'])) {
                $sql1 .= ' AND ((wtda.warehousedespatch_warehouseid_to =' . $_GET['warehouse_to'] . ' AND ((wtda.delete_status =0) OR (wtda.delete_status =1 AND wtda.edit_status =1))) OR (wtda.warehouseid_to =' . $_GET['warehouse_to'] . ' AND wtda.delete_status =1))';
            }
            if (isset($_GET['clerk']) && !empty($_GET['clerk'])) {
                $sql1 .= ' AND wtda.warehousedespatch_clerk =' . $_GET['clerk'];
            }
            if (isset($_GET['despatch_no']) && !empty($_GET['despatch_no'])) {
                $sql1 .= ' AND wtda.warehousedespatch_no ="' . $_GET['despatch_no'] . '"';
            }
            if (isset($_GET['date_from']) && !empty($_GET['date_from'])) {
                $date_from = date('Y-m-d', strtotime($_GET['date_from']));
            }
            if (isset($_GET['date_to']) && !empty($_GET['date_to'])) {
                $date_to = date('Y-m-d', strtotime($_GET['date_to']));
            }
            if (!empty($date_from) || !empty($date_to)) {
                if ((isset($date_from) && !empty($date_from)) && ((isset($date_to) && empty($date_to)) || !isset($date_to))) {
                    $sql1 .= ' AND ((wtda.warehousedespatch_date >="' . $date_from . '" AND ((wtda.delete_status =0) OR (wtda.delete_status =1 AND wtda.edit_status =1))) 
                                            OR 
                                        (wtda.warehouse_despatch_date >="' . $date_from . '" AND wtda.delete_status =1))';
                } else if ((isset($date_to) && !empty($date_to)) && ((isset($date_from) && empty($date_from)) || !isset($date_from))) {
                    $sql1 .= ' AND ((wtda.warehousedespatch_date <="' . $date_to . '" AND ((wtda.delete_status =0) OR (wtda.delete_status =1 AND wtda.edit_status =1))) 
                                        OR 
                                    (wtda.warehouse_despatch_date <="' . $date_to . '" AND wtda.delete_status =1))';
                } else if ((isset($date_to) && !empty($date_to)) && (isset($date_from) && !empty($date_from))) {
                    $sql1 .= ' AND ((wtda.warehousedespatch_date >="' . $date_from . '" AND wtda.warehousedespatch_date <="' . $date_to . '" AND ((wtda.delete_status =0) OR (wtda.delete_status =1 AND wtda.edit_status =1))) 
                                        OR 
                                    (wtda.warehouse_despatch_date >="' . $date_from . '" AND wtda.warehouse_despatch_date <="' . $date_to . '" AND wtda.delete_status =1))';
                } else {
                    $sql1 .= ' AND 1=1 ';
                }
            } else {
                $sql1 .= ' AND 1=1 ';
            }
            if (isset($_GET['item_id']) && !empty($_GET['item_id'])) {
                $item_id = $_GET['item_id'];
                $sql2 = " INNER JOIN {$tblpx}warehousedespatch_items as wdt
                                    ON wtda.warehousedespatch_id = wdt.warehousedespatch_id";
                $sql1 .= ' AND wdt.warehousedespatch_itemid="' . $item_id . '"';
            }
        }
        $sql3 = ' ORDER BY wtda.warehousedespatch_id DESC,wtda.all_warehousereceipt_id DESC';
        if (isset($_GET['item_id']) && !empty($_GET['item_id'])) {
            $item_id = $_GET['item_id'];
            $main_sql = $sql . $sql2 . $sql1 . $sql3;
        } else {
            $item_id = '';
            $main_sql = $sql . $sql1 . $sql3;
        }

        
        $warehousereceipt_details = Yii::app()->db->createCommand($main_sql)->queryAll();
        $warehousereceipt_count = count($warehousereceipt_details);
        $dataProvider = new CSqlDataProvider($main_sql, array(
            'keyField' => 'warehousedespatch_id',
            'totalItemCount' => $warehousereceipt_count,
            'pagination' => array('pageSize' => 10,),
        ));

        $this->render('index', array(
            'model' => $model,
            'specification'  => $specification,
            'dataProvider' => $dataProvider,
            'warehouse' => $warehouse,
            'vendor' => $vendor,
            'clerk' => $clerk,
            'item_id' => $item_id,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model = new Warehousedespatch('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Warehousedespatch']))
            $model->attributes = $_GET['Warehousedespatch'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }


    public function actioncreatenewdespatch()
    {
        $despatch_id = $_REQUEST['despatch_id'];

        if (isset($_REQUEST['despatch_no']) && isset($_REQUEST['default_date']) && isset($_REQUEST['vendor']) && isset($_REQUEST['warehouse']) && isset($_REQUEST['clerk']) && isset($_REQUEST['warehouse_to']) && isset($_REQUEST['eta_date'])) {
            if ($despatch_id == 0) {
                $tblpx = Yii::app()->db->tablePrefix;
                $res = Warehousedespatch::model()->findAll(array('condition' => 'warehousedespatch_no = "' . $_REQUEST['despatch_no'] . '"'));
                if (empty($res)) {
                    $model = new Warehousedespatch;
                    $model->warehousedespatch_warehouseid = $_REQUEST['warehouse'];
                    $model->warehousedespatch_warehouseid_to = $_REQUEST['warehouse_to'];
                    $model->warehousedespatch_date = date('Y-m-d',  strtotime($_REQUEST['default_date']));
                    $model->warehousedespatch_no = $_REQUEST['despatch_no'];
                    $model->warehousedespatch_vendor = $_REQUEST['vendor'];
                    // $model->warehousedespatch_project = $_REQUEST['project'];
                    $model->warehousedespatch_clerk = $_REQUEST['clerk'];
                    $model->warehousedespatch_through = isset($_REQUEST['through']) ? $_REQUEST['through'] : "";
                    $model->warehouse_eta_date = date('Y-m-d', strtotime($_REQUEST['eta_date']));
                    $model->company_id = Yii::app()->user->company_id;
                    $model->updated_by = yii::app()->user->id;
                    $model->updated_date = date('Y-m-d');
                    $model->created_date = date('Y-m-d');
                    $model->created_by = Yii::app()->user->id;
                    $model->save(false);
                    $last_id = $model->warehousedespatch_id;
                    echo json_encode(array('response' => 'success', 'msg' => 'Warehouse Despatch added successfully', 'despatch_id' => $last_id));
                } else {
                    echo json_encode(array('response' => 'error', 'msg' => 'Despatch no already exist'));
                }
            } else {
                $res = Warehousedespatch::model()->findAll(array('condition' => 'warehousedespatch_no = "' . $_REQUEST['despatch_no'] . '" AND warehousedespatch_id !=' . $despatch_id . ''));
                if (empty($res)) {
                    $model = Warehousedespatch::model()->findByPk($despatch_id);
                    $model->warehousedespatch_warehouseid = $_REQUEST['warehouse'];
                    $model->warehousedespatch_warehouseid_to = $_REQUEST['warehouse_to'];
                    $model->warehousedespatch_date = date('Y-m-d',  strtotime($_REQUEST['default_date']));
                    $model->warehousedespatch_no = $_REQUEST['despatch_no'];
                    $model->warehousedespatch_vendor = $_REQUEST['vendor'];
                    // $model->warehousedespatch_project = $_REQUEST['project'];
                    $model->warehousedespatch_clerk = $_REQUEST['clerk'];
                    $model->warehousedespatch_through = isset($_REQUEST['through']) ? $_REQUEST['through'] : "";
                    $model->warehouse_eta_date = date('Y-m-d', strtotime($_REQUEST['eta_date']));
                    $model->company_id = Yii::app()->user->company_id;
                    $model->updated_by = yii::app()->user->id;
                    $model->updated_date = date('Y-m-d');

                    if ($model->save(false)) {
                        echo json_encode(array('response' => 'success', 'msg' => 'Warehouse Despatch updated successfully', 'despatch_id' => $despatch_id));
                    } else {
                        echo json_encode(array('response' => 'error', 'msg' => 'Please enter all details'));
                    }
                } else {
                    echo json_encode(array('response' => 'error', 'msg' => 'Despatch no already exist'));
                }
            }
        }
    }
    public function unitConversionToBase($item_conversion_unit, $item_base_unit, $stock_item, $quantity_in_conversion_unit)
    {
        $tblpx = Yii::app()->db->tablePrefix;
        if ($item_conversion_unit != '' && $item_base_unit != '' && $stock_item != '') {
            $itemUnitConversion = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}unit_conversion WHERE item_id='" . $stock_item . "' AND base_unit='" . $item_base_unit . "' AND conversion_unit='" . $item_conversion_unit . "'")->queryRow();
            if ($itemUnitConversion['id'] != '') {
                $item_conversion_factor = $itemUnitConversion['conversion_factor'];
                $item_conversion_id = $itemUnitConversion['id'];
            } else {
                $item_conversion_factor = 1;
                $item_conversion_id = "";
            }
            $baseunit_quantity = $quantity_in_conversion_unit / $item_conversion_factor;
        } else {
            $item_conversion_factor = 1;
            $item_conversion_id = '';
            $baseunit_quantity = $quantity_in_conversion_unit / $item_conversion_factor;
        }
        $result['item_conversion_unit'] = $item_conversion_unit;
        $result['item_base_unit'] = $item_base_unit;
        $result['item_conversion_factor'] = $item_conversion_factor;
        $result['item_conversion_id'] = $item_conversion_id;
        $result['baseunit_quantity'] = $baseunit_quantity;
        return $result;
    }


    public function getItemreceipt($warehousedespatch_warehouseid, $stock_item, $batch, $rate, $dimension)
    {

        $item_receipt_sql = "SELECT SUM(warehousereceipt_baseunit_accepted_effective_quantity)as quantity FROM " . $this->tableNameAcc('warehousereceipt_items', 0) . " WHERE warehousereceipt_warehouseid=" . $warehousedespatch_warehouseid . " AND warehousereceipt_itemid=" . $stock_item . " AND warehousereceipt_rate = $rate ";

        if ($batch == null) {
            $item_receipt_sql .= "AND (warehousereceipt_batch IS Null OR warehousereceipt_batch ='" . $batch . "') ";
        } else {
            $item_receipt_sql .= "AND warehousereceipt_batch='" . $batch . "' ";
        }

        if ($dimension == null) {
            $item_receipt_sql .= " AND (dimension IS Null OR dimension ='')";
        } else {
            $item_receipt_sql .= " AND dimension LIKE '%" . $dimension . "%'";
        }

        $item_receipt =  Yii::app()->db->createCommand($item_receipt_sql)->queryRow();
        return $item_receipt;
    }


    public function getItemdespatch($warehousedespatch_warehouseid, $stock_item, $batch, $rate, $dimension)
    {

        $item_despatch_sql = '';
        $item_despatch_sql .= "SELECT SUM(warehousedespatch_baseunit_quantity)as quantity FROM " . $this->tableNameAcc('warehousedespatch_items', 0) . " WHERE warehousedespatch_warehouseid=" . $warehousedespatch_warehouseid . " AND warehousedespatch_itemid=" . $stock_item . "  AND warehousedespatch_rate = $rate ";

        if ($batch == null) {
            $item_despatch_sql .= "AND (warehousedespatch_batch IS Null OR warehousedespatch_batch ='" . $batch . "')";
        } else {
            $item_despatch_sql .= " AND warehousedespatch_batch='" . $batch . "'";
        }
        if ($dimension == null) {
            $item_despatch_sql .= " AND (dimension IS Null OR dimension ='')";
        } else {
            $item_despatch_sql .= " AND dimension LIKE '%" . $dimension . "%'";
        }

        $item_despatch = Yii::app()->db->createCommand($item_despatch_sql)->queryRow();
        return $item_despatch;
    }





    public function actiondespatchitem()
    {
        $data = $_REQUEST['data'];
        $result['html'] = '';
        $tblpx = Yii::app()->db->tablePrefix;
        if ($data['despatch_id'] == 0) {
            echo json_encode(array('response' => 'error', 'msg' => 'Please enter Warehouse despatch details'));
        } else {
            if (isset($data['sl_No'])) {
                $sl_No = $data['sl_No'];
            } else {
                $sl_No = '';
            }
            if (isset($data['stock_item'])) {
                $stock_item = $data['stock_item'];
            } else {
                $stock_item = '';
            }

            if (isset($data['quantity'])) {
                $quantity = $data['quantity'];
            } else {
                $quantity = '';
            }

            if (isset($data['base_quantity'])) {
                $base_quantity = $data['base_quantity'];
            } else {
                $base_quantity = '';
            }

            if (isset($data['base_unit'])) {
                $base_unit = $data['base_unit'];
            } else {
                $base_unit = '';
            }

            if (isset($data['base_rate'])) {
                $base_rate = $data['base_rate'];
            } else {
                $base_rate = '';
            }

            if (isset($data['base_amount'])) {
                $base_amount = $data['base_amount'];
            } else {
                $base_amount = '';
            }

            if (isset($data['dimension'])) {
                $warehousestock_itemid_dimension_category = ($data['dimension'] != "") ? $data['dimension'] : 1;
            } else {
                $warehousestock_itemid_dimension_category = 1;
            }

            if (isset($data['batch'])) {

                $warehouseStockId = trim($data['batch']);
                $warehouseStock = Warehousestock::model()->findByPk($warehouseStockId);
                $batch = $warehouseStock['batch'];
                $rate = $warehouseStock['rate'];
                $dimension = $warehouseStock['dimension'];
            } else {

                $batch = null;
                $dimension = null;
                $warehouseStockId = '';
                $rate = '';
            }



            if (isset($data['unit'])) {

                $unit = $data['unit'];
                $unit_id = $data['unit'];
            } else {
                $unit = '';
                $unit_id = '';
            }
            if (isset($data['remark'])) {
                $remark = $data['remark'];
            } else {
                $remark = '';
            }
            $item_conversion_unit = $unit_id;
            $itemspecification = Yii::app()->db->createCommand("SELECT id, parent_id, brand_id, specification, unit FROM {$tblpx}purchase_category WHERE id=" . $stock_item . "")->queryRow();
            $itemunit =  $itemspecification['unit'];
            if (!empty($itemunit)) {
                $item_base_unit = $itemunit;
            } else {
                $item_base_unit = '';
            }
            if ($item_conversion_unit != '' && $item_base_unit != '' && $stock_item != '') {

                if ($item_conversion_unit != $item_base_unit) {
                    $warehousedespatch_baseunit_quantity = $base_quantity;
                } else {
                    $warehousedespatch_baseunit_quantity = $quantity;
                }
            } else {
                $warehousedespatch_baseunit_quantity = $quantity;
            }

            $unitconversiondata =  $this->unitConversionToBase($item_conversion_unit, $item_base_unit, $stock_item, $quantity);
            $item_conversion_id = $unitconversiondata['item_conversion_id'];

            $Warehousestock_quantity_val = Warehousestock::model()->findByPk($warehouseStockId);
            $Warehousestock_quantity      = $Warehousestock_quantity_val["warehousestock_stock_quantity"];
            $available_quantity = $Warehousestock_quantity;


            if ($warehousedespatch_baseunit_quantity <= $Warehousestock_quantity) {
                $receipt = Warehousedespatch::model()->findByPk($data['despatch_id']);
                $item_model = new WarehousedespatchItems();
                $item_model->warehousedespatch_id = $data['despatch_id'];
                $item_model->warehousedespatch_quantity = $quantity;
                $item_model->warehousedespatch_baseunit_quantity = $warehousedespatch_baseunit_quantity;
                $item_model->warehousedespatch_unitConversion_id = $item_conversion_id;
                $item_model->warehousedespatch_batch = $batch;
                $item_model->warehousestock_itemid_dimension_category = $warehousestock_itemid_dimension_category;
                $item_model->dimension = $dimension;
                $item_model->warehousedespatch_rate = $rate;
                $item_model->warehousedespatch_unit = $unit;
                $item_model->warehousedespatch_remarks = $remark;
                $item_model->warehousedespatch_warehouseid = $receipt->warehousedespatch_warehouseid;
                $item_model->warehousedespatch_itemid = $stock_item;
                $item_model->warehousedespatch_baseqty = $base_quantity;
                $item_model->warehousedespatch_baseunit = $base_unit;
                $item_model->warehousedespatch_baserate = $base_rate;
                $item_model->warehousedespatch_amount = $base_amount;
                $item_model->created_by = Yii::app()->user->id;
                $item_model->created_date = date('Y-m-d H:i:s');
                $item_model->updated_by = Yii::app()->user->id;
                $item_model->updated_date = date('Y-m-d H:i:s');
                $transaction = Yii::app()->db->beginTransaction();
                try {
                    if ($item_model->save()) {
                        $despatch_item_id = $item_model->item_id;
                        $final_quantity = 0;
                        $stock_sql = "SELECT warehousestock_stock_quantity,warehousestock_id,warehousestock_initial_quantity FROM {$tblpx}warehousestock WHERE warehousestock_id='" . $warehouseStockId . "'";
                        $stock = Yii::app()->db->createCommand($stock_sql)->queryRow();

                        if (!empty($stock)) {


                            $item_receipt = $this->getItemreceipt($receipt->warehousedespatch_warehouseid, $stock_item, $batch, $rate, $dimension);


                            $item_despatch = $this->getItemdespatch($receipt->warehousedespatch_warehouseid, $stock_item, $batch, $rate, $dimension);





                            if ($item_receipt['quantity'] == "") {

                                $item_receipt_quantity = 0;
                            } else {

                                $item_receipt_quantity = $item_receipt['quantity'];
                            }
                            if ($item_despatch['quantity'] == "") {

                                $item_despatch_quantity = 0;
                            } else {

                                $item_despatch_quantity = $item_despatch['quantity'];
                            }

                            $final_quantity = ($stock['warehousestock_initial_quantity'] + $item_receipt['quantity']) - $item_despatch['quantity'];

                            $model2 = Warehousestock::model()->findByPk($stock['warehousestock_id']);
                            $model2->warehousestock_stock_quantity = $final_quantity;

                            if ($model2->save(false)) {
                                $receipt_default_date = $receipt->warehousedespatch_date;
                                $receipt_vendor = 'System generated Receipt Note';
                                $receipt_warehouse_to = $receipt->warehousedespatch_warehouseid_to;
                                $receipt_warehouse_from = $receipt->warehousedespatch_warehouseid;
                                $receipt_clerk = $receipt->warehousedespatch_clerk;
                                $receipt_no = 'DR-' . $receipt->warehousedespatch_no;
                                $receipt_transfer_type = '2';
                                $dispatch_id = $data['despatch_id'];
                            } else {

                                throw new Exception('Some problem occured in warehouse despatch');
                            }
                        } else {

                            throw new Exception('Item is not available for Despatch');
                        }

                        $item_quantity = Yii::app()->db->createCommand("SELECT SUM(warehousedespatch_baseunit_quantity) as quantity FROM {$tblpx}warehousedespatch_items WHERE warehousedespatch_id=" . $data['despatch_id'] . "")->queryScalar();
                        $model = Warehousedespatch::model()->findByPk($data['despatch_id']);
                        $model->warehousedespatch_quantity = $item_quantity;
                        $model->company_id = Yii::app()->user->company_id;
                        if ($model->save(false)) {
                        } else {
                            throw new Exception('An error occured in update');
                        }
                        $last_id = $item_model->item_id;
                        $result = '';
                        $receipt_sql = "SELECT wdi.*,ws.warehousestock_id 
                            FROM " . $this->tableNameAcc('warehousedespatch_items', 0) . " as wdi 
                            INNER JOIN  " . $this->tableNameAcc('warehousestock', 0) . " as ws 
                            ON wdi.warehousedespatch_warehouseid = ws.warehousestock_warehouseid 
                            AND wdi.warehousedespatch_itemid = ws.warehousestock_itemid 
                            AND wdi.warehousestock_itemid_dimension_category = ws.warehousestock_itemid_dimension_category 
                            AND wdi.warehousedespatch_rate = ws.rate WHERE wdi.warehousedespatch_id='" . $data['despatch_id'] . "'
                            AND ((wdi.warehousedespatch_batch IS NULL AND ws.batch IS NULL) OR wdi.warehousedespatch_batch = ws.batch)";
                        $receipt_details = Yii::app()->db->createCommand($receipt_sql)->queryAll();


                        foreach ($receipt_details as $key => $values) {
                            $sql = "SELECT id, cat_id,brand_id, specification, unit "
                                    . " FROM {$tblpx}specification "
                                    . " WHERE id=" . $values['warehousedespatch_itemid'];
                            $specification = Yii::app()->db->createCommand($sql)->queryRow();
                            $sql = "SELECT * FROM {$tblpx}purchase_category "
                                    . " WHERE id='" . $specification['cat_id'] . "'";
                            $parent_category = Yii::app()->db->createCommand($sql)->queryRow();

                            if ($specification['brand_id'] != NULL) {
                                $brand_details = Yii::app()->db->createCommand("SELECT brand_name FROM {$tblpx}brand WHERE id=" . $specification['brand_id'] . "")->queryRow();
                                $brand = '-' . $brand_details['brand_name'];
                            } else {
                                $brand = '';
                            }

                            $batch_value = $values['warehousedespatch_batch'] . '@' . $values['warehousedespatch_rate'];

                            $spc_details = $parent_category['category_name'] . $brand . ' - ' . $specification['specification'];
                            $spc_id = $specification['id'];

                            $result .= '<tr>';
                            $result .= '<td><div id="item_sl_no">' . ($key + 1) . '</div></td>';
                            $result .= '<td><div class="item_description" id="' . $spc_id . '">' . $spc_details . '</div> </td>';
                            $result .= '<td><div class="dimension" id="dimension' . $spc_id . '">' . $values['dimension'] . '<input type="hidden" id="warehousestock_itemid_dimension_category' . ($key + 1) . '" name="warehousestock_itemid_dimension_category" value="' . $values['warehousestock_itemid_dimension_category'] . '"></div> </td>';
                            $result .= '<td><div class="item_batch" id="batch' . $spc_id . '">' . $batch_value  . '<input type="hidden" id="warehousestock_id' . ($key + 1) . '" name="warehousestock_id" value="' . $values['warehousestock_id'] . '"></div> </td>';
                            
                            $result .= '<td> <div class="" id="quantity' . $spc_id . '"> ' . $values['warehousedespatch_quantity'] . '</div> </td>';
                            $result .= '<td> <div class="unit" id="unit' . $spc_id . '"> ' . $values['warehousedespatch_unit'] . '<input type="hidden" id="item_unit_id' . ($key + 1) . '" name="item_unit_id" value="' . $this->GetItemunitID($values['warehousedespatch_unit']) . '"></div> </td>';
                            $result .= '<td> <div class="base_qty" id="base_qty' . $spc_id . '"> ' . $values['warehousedespatch_baseqty'] . '</div> </td>';
                            $result .= '<td> <div class="base_unit" id="base_unit' . $spc_id . '"> ' . $values['warehousedespatch_baseunit'] . '</div> </td>';
                            $result .= '<td> <div class="base_rate" id="base_rate' . $spc_id . '"> ' . $values['warehousedespatch_baserate'] . '</div> </td>';
                            $result .= '<td><div class="base_amount" id="amount' . $spc_id . '">' . $values['warehousedespatch_amount'] . '</div></td>';
                            $result .= '<td> <div class="unit" id="remarks' . $spc_id . '"> ' . $values['warehousedespatch_remarks'] . '</div> </td>';
                            $result .= '<td width="70"><span class="icon icon-options-vertical popover-test" data-toggle="popover"   data-placement="left" type="button" data-html="true" style="cursor: pointer;" ></span>'
                                . '<div class ="popover-content hide"> '
                                . '<ul class="tooltip-hiden">'
                                . '<li><a href="#" id=' . $values['item_id'] . ' class="btn btn-default btn-xs removebtn">Delete</a></li> '
                                . '<li><a href="" id=' . $values['item_id'] . ' class="btn btn-default btn-xs edit_item" style="margin-left:3px">Edit</a></li>';
                            $result .= '</ul>';
                            $result .= '</div> ';
                            $result .= '</td>';

                            $result .= '</tr>';
                        }

                        echo json_encode(array('response' => 'success', 'msg' => 'Despatch item saved successfully', 'html' => $result));
                    } else {
                        throw new Exception('An error occured in add');
                        echo json_encode(array('response' => 'error', 'msg' => 'Some problem occured'));
                    }
                    $transaction->commit();
                } catch (Exception $error) {
                    $transaction->rollBack();
                    $result = array('status' => '0', 'msg' => $error->getMessage());

                    echo json_encode(array('response' => 'error', 'msg' => $error->getMessage()));
                }
            } else {
                echo json_encode(array('response' => 'error', 'msg' => 'Available quantity in stock = ' . $available_quantity . ' ' . $unit));
            }
        }
    }
    public function actionchechAvailableQuantity()
    {
        $data = $_REQUEST['data'];
        $result['html'] = '';
        $tblpx = Yii::app()->db->tablePrefix;


        if (isset($data['stock_item'])) {
            $stock_item = $data['stock_item'];
        } else {
            $stock_item = '';
        }

        if (isset($data['quantity'])) {
            $quantity = ($data['quantity'] != '' || $data['quantity'] != NULL) ? $data['quantity'] : 0;
        } else {
            $quantity = '';
        }
        if (isset($data['batch'])) {
            $warehouseStockId = trim($data['batch']);
            $warehouseStock = Warehousestock::model()->findByPk($warehouseStockId);
            $batch = $warehouseStock['batch'];
            $rate = $warehouseStock['rate'];
        } else {
            $batch = null;
            $warehouseStockId = '';
            $rate = '';
        }
        if (isset($data['unit'])) {

            $unit = $data['unit'];
            $unit_id = $data['unit'];
        } else {
            $unit = '';
            $unit_id = '';
        }
        if (isset($data['remark'])) {
            $remark = $data['remark'];
        } else {
            $remark = '';
        }

        $item_conversion_unit = $unit_id;
        $itemspecification = Yii::app()->db->createCommand("SELECT id, parent_id, brand_id, specification, unit FROM {$tblpx}purchase_category WHERE id=" . $stock_item . "")->queryRow();

        $itemunit = $itemspecification['unit'];
        if (!empty($itemunit)) {
            $item_base_unit = $itemunit;
        } else {
            $item_base_unit = '';
        }
        if ($item_conversion_unit != '' && $item_base_unit != '' && $stock_item != '') {
            $itemUnitConversion = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}unit_conversion WHERE item_id='" . $stock_item . "' AND base_unit='" . $item_base_unit . "' AND conversion_unit='" . $item_conversion_unit . "'")->queryRow();
            if ($itemUnitConversion['id'] != '') {
                $item_conversion_factor = $itemUnitConversion['conversion_factor'];
                $item_conversion_id = $itemUnitConversion['id'];
            } else {
                $item_conversion_factor = 1;
                $item_conversion_id = "";
            }
        } else {
            $item_conversion_factor = 1;
            $item_conversion_id = '';
        }

        $Warehousestock_quantity_val = Warehousestock::model()->findByPk($warehouseStockId);
        $Warehousestock_quantity      = $Warehousestock_quantity_val["warehousestock_stock_quantity"];

        if (isset($data['despatch_item_id']) && $data['despatch_item_id'] != 0) {

            $despatch_item_id = $data['despatch_item_id'];
            if ($item_conversion_unit != $item_base_unit) {
                $available_quantity = ($Warehousestock_quantity / $item_conversion_factor) + $quantity;
            } else {
                $available_quantity = ($Warehousestock_quantity * $item_conversion_factor) + $quantity;
            }
        } else {

            $despatch_item_id = '';
            if ($item_conversion_unit != $item_base_unit) {
                $available_quantity = $Warehousestock_quantity / $item_conversion_factor;
            } else {
                $available_quantity = $Warehousestock_quantity * $item_conversion_factor;
            }
        }
        echo json_encode(array('response' => 'success', 'available_quantity' => $available_quantity . ' ' . $unit, 'availableQuantity' => $available_quantity));
    }
    public function actiongetUnit()
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $html['unit'] = '';
        $html['status'] = '';
        $specification = Yii::app()->db->createCommand("SELECT id, parent_id, brand_id, specification, unit FROM {$tblpx}purchase_category WHERE id=" . $_REQUEST['data'] . "")->queryRow();

        $unit = $specification['unit'];
        if (!empty($unit)) {
            $html['unit'] .= $unit;
        } else {
            $html['unit'] .= '';
        }
        $html['status'] .= true;
        echo json_encode($html);
    }
    public function actiongetUnits()
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $html = [];
        $html['unit'] = '';
        $html['units'] = '';
        $html['status'] = '';
        $sql = "SELECT id, cat_id, brand_id, specification, unit "
                . " FROM {$tblpx}specification WHERE id=" . $_REQUEST['data'];
        $specification = Yii::app()->db->createCommand($sql)->queryRow();

        $unit =  $specification['unit'];
        if (!empty($unit)) {
            $base_unit = $unit;
        } else {
            $base_unit = '';
        }
        $html['base_unit'] = $base_unit;
        if ($base_unit != '') {

            $uc_item_count  = UnitConversion::model()->countByAttributes(array(
                'item_id' => $_REQUEST['data'],
            ));

            if ($uc_item_count > 1) {
                $html['units']  = $this->GetItemunitlist($_REQUEST['data']);
                $html['unit']  = $html['units'];
            } else {
                if (!empty($unit)) {
                    $html['unit'] .= $base_unit;
                    $html['units'] .= $base_unit;
                } else {
                    $html['unit'] .= '';
                    $html['units'] = 'Unit';
                }
            }
        } else {
            $html['unit'] .= '';
            $html['units'] = 'Unit';
        }
        $html['status'] .= true;
        echo json_encode($html);
    }


    public function GetItemunitlist($purchase_itemid)
    {
        $purchase_itemid    = $purchase_itemid;

        $final_list = array();
        $ItemUnitlist           = UnitConversion::model()->findAll(
            array("condition" => "item_id =  $purchase_itemid", "order" => "id")
        );
        $final_list = array();
        foreach ($ItemUnitlist as $key => $value) {
            $final_list[$key]['id'] = $value['conversion_unit'];
            $final_list[$key]['value'] = $value['conversion_unit'];
        }

        return $final_list;
    }
    public function GetItemunitCode($unit_id)
    {
        $unit_id            = $unit_id;
        $ItemUnit           = Unit::model()->findByPk($unit_id);
        if ($ItemUnit["unit_code"] != '') {
            $ItemUnit_code      = $ItemUnit["unit_code"];
        } else {
            $ItemUnit_code      = $ItemUnit["unit_name"];
        }

        return $ItemUnit_code;
    }
    public function GetItemunitName($unit_id)
    {
        $unit_id            = $unit_id;
        $ItemUnit           = Unit::model()->findByPk($unit_id);
        $ItemUnit_name      = $ItemUnit["unit_name"];
        return $ItemUnit_name;
    }
    public function GetItemunitID($unit_name)
    {
        $unit_name  = trim($unit_name);
        $ItemUnit   = Unit::model()->findAll(
            array("condition" => "unit_name ='" . $unit_name . "' OR unit_code ='" . $unit_name . "'", "order" => "id")
        );
        $ItemUnit_id ='';
        foreach ($ItemUnit as $key => $value) {
            $ItemUnit_id = $value["id"];
        }
        return $ItemUnit_id;
    }
    public function actiongetBatch()
    {
        $category_id        = $_GET['data'];
        $dimension_val        = ($_GET['dimension_val'] != '') ? $_GET['dimension_val'] : 1;
        $warehouse_id       = $_GET['warehouse_id'];
        $final_list = array();
        $batch_array = array();
        $uc_item_count  = Warehousestock::model()->countByAttributes(array(
            'warehousestock_warehouseid' => $warehouse_id,
            'warehousestock_itemid' => $category_id,
            'warehousestock_itemid_dimension_category' => $dimension_val,
        ));

        if ($uc_item_count != 0) {
            $con_arr = "warehousestock_warehouseid = $warehouse_id AND warehousestock_itemid = $category_id AND warehousestock_itemid_dimension_category = $dimension_val";

            $ItemUnitlist           = Warehousestock::model()->findAll(
                array("condition" => $con_arr, "order" => "warehousestock_id")
            );

            foreach ($ItemUnitlist as $key => $value) {
                $batch_rate = $value['batch'] . '@' . $value['rate'];
                if (!array_key_exists($batch_rate, $batch_array)) {
                    $batch_array[$batch_rate]['id'] = $value['warehousestock_id'];
                    $final_list[$key]['id'] = $value['warehousestock_id'];
                    $final_list[$key]['value'] = $batch_rate;
                    $final_list[$key]['rate'] = $value['rate'];
                }
            }
        }
        $warehouseStock["Batchlist"] =  $final_list;
        $warehouseStock_dis           = json_encode($warehouseStock);
        echo $warehouseStock_dis;
    }


    public function actiongetDimensions()
    {
        $category_id        = $_GET['data'];
        $warehouse_id       = $_GET['warehouse_id'];
        $final_list = array();
        $dimension_array = array();
        $uc_item_count  = Warehousestock::model()->countByAttributes(array(
            'warehousestock_warehouseid' => $warehouse_id,
            'warehousestock_itemid' => $category_id,
        ));
        if ($uc_item_count != 0) {
            $ItemUnitlist           = Warehousestock::model()->findAll(
                array("condition" => "warehousestock_warehouseid = $warehouse_id AND warehousestock_itemid= $category_id", "order" => "warehousestock_id")
            );

            $final_list = array();
            foreach ($ItemUnitlist as $key => $value) { 
                if($value['dimension'] !=""){
                    $final_list[$key]['id'] = $value['warehousestock_itemid_dimension_category'];
                    $final_list[$key]['value'] = $value['dimension']; 
                }
                           
            }
        }
        $warehouseStock["dimensionlist"] =  $final_list;
        $warehouseStock_dis           = json_encode($warehouseStock);
        echo $warehouseStock_dis;
    }
    public function actionupdatedespatchitem()
    {
        $data = $_REQUEST['data'];
        $result['html'] = '';
        $tblpx = Yii::app()->db->tablePrefix;

        if (isset($data['sl_No'])) {
            $sl_No = $data['sl_No'];
        } else {
            $sl_No = '';
        }
        if (isset($data['stock_item'])) {
            $stock_item = $data['stock_item'];
        } else {
            $stock_item = '';
        }

        if (isset($data['quantity'])) {
            $quantity = $data['quantity'];
        } else {
            $quantity = '';
        }

        if (isset($data['base_quantity'])) {
            $base_quantity = $data['base_quantity'];
        } else {
            $base_quantity = '';
        }

        if (isset($data['base_unit'])) {
            $base_unit = $data['base_unit'];
        } else {
            $base_unit = '';
        }

        if (isset($data['base_rate'])) {
            $base_rate = $data['base_rate'];
        } else {
            $base_rate = '';
        }
        if (isset($data['base_amount'])) {
            $base_amount = $data['base_amount'];
        } else {
            $base_amount = '';
        }


        if (isset($data['dimension'])) {
            $warehousestock_itemid_dimension_category = $data['dimension'];
        } else {
            $warehousestock_itemid_dimension_category = 1;
        }
        if (isset($data['batch'])) {
            $warehouseStockId = trim($data['batch']);
            $warehouseStock = Warehousestock::model()->findByPk($warehouseStockId);
            $batch = $warehouseStock['batch'];
            $rate = $warehouseStock['rate'];
            $dimension = $warehouseStock['dimension'];
        } else {
            $batch = null;
            $dimension = null;
            $warehouseStockId = '';
            $rate = '';
        }


        if (isset($data['unit'])) {
            $unit = $data['unit'];
            $unit_id = $data['unit'];
        } else {
            $unit = '';
            $unit_id = '';
        }
        if (isset($data['remark'])) {
            $remark = $data['remark'];
        } else {
            $remark = '';
        }
        $item_conversion_unit = $unit_id;
        $itemspecification = Yii::app()->db->createCommand("SELECT id, parent_id, brand_id, specification, unit FROM {$tblpx}purchase_category WHERE id=" . $stock_item . "")->queryRow();
        $itemunit =  $itemspecification['unit'];
        if (!empty($itemunit)) {
            $item_base_unit = $itemunit;
        } else {
            $item_base_unit = '';
        }
        if ($item_conversion_unit != '' && $item_base_unit != '' && $stock_item != '') {

            if ($item_conversion_unit != $item_base_unit) {
                $warehousedespatch_baseunit_quantity = $base_quantity;
            } else {
                $warehousedespatch_baseunit_quantity = $quantity;
            }
        } else {
            $warehousedespatch_baseunit_quantity = $quantity;
        }

        $itemUnitConversion = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}unit_conversion WHERE item_id='" . $stock_item . "' AND base_unit='" . $item_base_unit . "' AND conversion_unit='" . $item_conversion_unit . "'")->queryRow();
        $item_conversion_id = $itemUnitConversion['id'];

        if (!empty($data) && $data['item_id'] != 0) {
            $despatch_item_id = $data['item_id'];
            $receipt = Warehousedespatch::model()->findByPk($data['despatch_id']);
            $item_model = WarehousedespatchItems::model()->findByPk($data['item_id']);
            $Warehousestock_quantity_val = Warehousestock::model()->findByPk($warehouseStockId);
            if ($batch == null) {
                $item_receipt_sql = "SELECT SUM(warehousereceipt_baseunit_accepted_effective_quantity)as quantity FROM {$tblpx}warehousereceipt_items WHERE warehousereceipt_warehouseid=" . $receipt->warehousedespatch_warehouseid . " AND warehousereceipt_itemid=" . $stock_item . " AND (warehousereceipt_batch IS Null OR warehousereceipt_batch ='" . $batch . "') AND warehousereceipt_rate  = $rate ";
                $item_despatch_sql = "SELECT SUM(warehousedespatch_baseunit_quantity)as quantity FROM {$tblpx}warehousedespatch_items WHERE warehousedespatch_warehouseid=" . $receipt->warehousedespatch_warehouseid . " AND warehousedespatch_itemid=" . $stock_item . " AND (warehousedespatch_batch IS Null OR warehousedespatch_batch ='" . $batch . "') AND warehousedespatch_rate = $rate ";
                $item_receipt1 = Yii::app()->db->createCommand($item_receipt_sql)->queryRow();
                $item_despatch1 = Yii::app()->db->createCommand($item_despatch_sql)->queryRow();
            } else {
                $item_receipt_sql = "SELECT SUM(warehousereceipt_baseunit_accepted_effective_quantity)as quantity FROM {$tblpx}warehousereceipt_items WHERE warehousereceipt_warehouseid=" . $receipt->warehousedespatch_warehouseid . " AND warehousereceipt_itemid=" . $stock_item . " AND warehousereceipt_batch='" . $batch . "' AND warehousereceipt_rate = $rate ";
                $item_despatch_sql = "SELECT SUM(warehousedespatch_baseunit_quantity)as quantity FROM {$tblpx}warehousedespatch_items WHERE warehousedespatch_warehouseid=" . $receipt->warehousedespatch_warehouseid . " AND warehousedespatch_itemid=" . $stock_item . " AND warehousedespatch_batch='" . $batch . "' AND warehousedespatch_rate = $rate ";
                $item_receipt1 = Yii::app()->db->createCommand($item_receipt_sql)->queryRow();
                $item_despatch1 = Yii::app()->db->createCommand($item_despatch_sql)->queryRow();
            }
            if ($item_receipt1['quantity'] == "") {
                $item_receipt_quantity = 0;
            } else {
                $item_receipt_quantity = $item_receipt1['quantity'];
            }
            if ($item_despatch1['quantity'] == "") {
                $item_despatch_quantity = 0;
            } else {
                $item_despatch_quantity = $item_despatch1['quantity'];
            }
            $Warehousestock_quantity      = ($Warehousestock_quantity_val['warehousestock_initial_quantity'] + $item_receipt1['quantity']) - ($item_despatch1['quantity'] - $item_model['warehousedespatch_quantity']);
            $available_quantity = $Warehousestock_quantity;
            if ($warehousedespatch_baseunit_quantity <= $Warehousestock_quantity) {

                $previous_qty = $item_model->warehousedespatch_quantity;
                $item_model->warehousedespatch_id = $data['despatch_id'];
                $item_model->warehousedespatch_quantity = $quantity;
                $item_model->warehousedespatch_baseunit_quantity = $warehousedespatch_baseunit_quantity;
                $item_model->warehousedespatch_unitConversion_id = $item_conversion_id;
                $item_model->warehousedespatch_batch = $batch;
                $item_model->warehousestock_itemid_dimension_category = $warehousestock_itemid_dimension_category;
                $item_model->dimension = $dimension;
                $item_model->warehousedespatch_rate = $rate;
                $item_model->warehousedespatch_unit = $unit;
                $item_model->warehousedespatch_remarks = $remark;
                $item_model->warehousedespatch_warehouseid = $receipt->warehousedespatch_warehouseid;
                $item_model->warehousedespatch_itemid = $stock_item;
                $item_model->warehousedespatch_baseqty = $base_quantity;
                $item_model->warehousedespatch_baseunit = $base_unit;
                $item_model->warehousedespatch_baserate = $base_rate;
                $item_model->warehousedespatch_amount = $base_amount;
                $item_model->created_by = Yii::app()->user->id;
                $item_model->created_date = date('Y-m-d H:i:s');
                $item_model->updated_by = Yii::app()->user->id;
                $item_model->updated_date = date('Y-m-d H:i:s');
                $transaction = Yii::app()->db->beginTransaction();
                try {
                    if ($item_model->save()) {
                        $stock = Yii::app()->db->createCommand("SELECT warehousestock_stock_quantity,warehousestock_id,warehousestock_initial_quantity FROM {$tblpx}warehousestock WHERE warehousestock_id='" . $warehouseStockId . "'")->queryRow();
                        if (!empty($stock)) {
                            $item_receipt = $this->getItemreceipt($receipt->warehousedespatch_warehouseid, $stock_item, $batch, $rate, $dimension);
                            $item_despatch = $this->getItemdespatch($receipt->warehousedespatch_warehouseid, $stock_item, $batch, $rate, $dimension);

                            if ($item_receipt['quantity'] == "") {
                                $item_receipt_quantity = 0;
                            } else {
                                $item_receipt_quantity = $item_receipt['quantity'];
                            }
                            if ($item_despatch['quantity'] == "") {
                                $item_despatch_quantity = 0;
                            } else {
                                $item_despatch_quantity = $item_despatch['quantity'];
                            }

                            $final_quantity = ($stock['warehousestock_initial_quantity'] + $item_receipt['quantity']) - $item_despatch['quantity'];

                            $model2 = Warehousestock::model()->findByPk($stock['warehousestock_id']);
                            $model2->warehousestock_stock_quantity = $final_quantity;
                            if ($model2->save(false)) {
                                $receipt_default_date = $receipt->warehousedespatch_date;
                                $receipt_vendor = 'System generated Receipt Note';
                                $receipt_warehouse_to = $receipt->warehousedespatch_warehouseid_to;
                                $receipt_warehouse_from = $receipt->warehousedespatch_warehouseid;
                                $receipt_clerk = $receipt->warehousedespatch_clerk;
                                $receipt_no = 'DR-' . $receipt->warehousedespatch_no;
                                $receipt_transfer_type = '2';
                                $dispatch_id = $data['despatch_id'];
                            } else {
                                throw new Exception('Some problem occured in warehouse despatch');
                            }
                        } else {
                            throw new Exception('Item is not available for despatch');
                        }



                        $item_quantity = Yii::app()->db->createCommand("SELECT SUM(warehousedespatch_baseunit_quantity) as quantity FROM {$tblpx}warehousedespatch_items WHERE warehousedespatch_id=" . $data['despatch_id'] . "")->queryScalar();
                        $model = Warehousedespatch::model()->findByPk($data['despatch_id']);
                        $model->warehousedespatch_quantity = $item_quantity;
                        $model->company_id = Yii::app()->user->company_id;
                        if ($model->save(false)) {
                        } else {
                            throw new Exception('An error occured in update');
                        }
                        $last_id = $item_model->item_id;
                        $result = '';
                        $receipt_sql = "SELECT wdi.*,ws.warehousestock_id 
                            FROM " . $this->tableNameAcc('warehousedespatch_items', 0) . " as wdi 
                            INNER JOIN " . $this->tableNameAcc('warehousestock', 0) . " as ws 
                            ON wdi.warehousedespatch_warehouseid = ws.warehousestock_warehouseid 
                            AND wdi.warehousedespatch_itemid = ws.warehousestock_itemid 
                            AND wdi.warehousestock_itemid_dimension_category = ws.warehousestock_itemid_dimension_category 
                            AND wdi.warehousedespatch_rate = ws.rate WHERE wdi.warehousedespatch_id='" . $data['despatch_id'] . "'
                            AND ((wdi.warehousedespatch_batch IS NULL AND ws.batch IS NULL) OR wdi.warehousedespatch_batch = ws.batch)";
                        $receipt_details = Yii::app()->db->createCommand($receipt_sql)->queryAll();
                        foreach ($receipt_details as $key => $values) {
                            $sql = "SELECT id, cat_id,brand_id, specification, unit "
                                    . " FROM {$tblpx}specification "
                                    . " WHERE id=" . $values['warehousedespatch_itemid'];
                            $specification = Yii::app()->db->createCommand($sql)->queryRow();
                            $sql = "SELECT * FROM {$tblpx}purchase_category "
                                    . " WHERE id='" . $specification['cat_id'] . "'";
                            $parent_category = Yii::app()->db->createCommand($sql)->queryRow();

                            if ($specification['brand_id'] != NULL) {
                                $brand_details = Yii::app()->db->createCommand("SELECT brand_name FROM {$tblpx}brand WHERE id=" . $specification['brand_id'] . "")->queryRow();
                                $brand = '-' . $brand_details['brand_name'];
                            } else {
                                $brand = '';
                            }
                            $batch_value = $values['warehousedespatch_batch'] . '@' . $values['warehousedespatch_rate'];
                            $spc_details = $parent_category['category_name'] . $brand . ' - ' . $specification['specification'];
                            $spc_id = $specification['id'];

                            $result .= '<tr>';
                            $result .= '<td><div id="item_sl_no">' . ($key + 1) . '</div></td>';
                            $result .= '<td><div class="item_description" id="' . $spc_id . '">' . $spc_details . '</div> </td>';
                            //$result .= '<td><div class="item_description" id="'+$spc_id+'">'.ucwords($description).'</div> </td>';
                            $result .= '<td><div class="dimension" id="dimension' . $spc_id . '">' . $values['dimension'] . '<input type="hidden" id="warehousestock_itemid_dimension_category' . ($key + 1) . '" name="warehousestock_itemid_dimension_category" value="' . $values['warehousestock_itemid_dimension_category'] . '"></div> </td>';
                            $result .= '<td><div class="item_batch" id="batch' . $spc_id . '">' . $batch_value . '<input type="hidden" id="warehousestock_id' . ($key + 1) . '" name="warehousestock_id" value="' . $values['warehousestock_id'] . '"></div> </td>';
                            $result .= '<td> <div class="" id="quantity' . $spc_id . '"> ' . $values['warehousedespatch_quantity'] . '</div> </td>';
                            $result .= '<td> <div class="unit" id="unit' . $spc_id . '"> ' . $values['warehousedespatch_unit'] . '<input type="hidden" id="item_unit_id' . ($key + 1) . '" name="item_unit_id" value="' . $this->GetItemunitID($values['warehousedespatch_unit']) . '"></div> </td>';
                            $result .= '<td> <div class="base_qty" id="base_qty' . $spc_id . '"> ' . $values['warehousedespatch_baseqty'] . '</div> </td>';
                            $result .= '<td> <div class="base_unit" id="base_unit' . $spc_id . '"> ' . $values['warehousedespatch_baseunit'] . '</div> </td>';
                            $result .= '<td> <div class="base_rate" id="base_rate' . $spc_id . '"> ' . $values['warehousedespatch_baserate'] . '</div> </td>';
                            $result .= '<td> <div class="unit" id="remarks' . $spc_id . '"> ' . $values['warehousedespatch_remarks'] . '</div> </td>';
                            $result .= '<td width="70"><span class="icon icon-options-vertical popover-test" data-toggle="popover"   data-placement="left" type="button" data-html="true" style="cursor: pointer;" ></span>'
                                . '<div class ="popover-content hide"> '
                                . '<ul class="tooltip-hiden">'
                                . '<li><a href="#" id=' . $values['item_id'] . ' class="btn btn-default btn-xs removebtn">Delete</a></li> '
                                . '<li><a href="" id=' . $values['item_id'] . ' class="btn btn-default btn-xs edit_item" style="margin-left:3px">Edit</a></li>';
                            $result .= '</ul>';
                            $result .= '</div> ';
                            $result .= '</td>';

                            $result .= '</tr>';
                        }

                        echo json_encode(array('response' => 'success', 'msg' => 'Receipt item saved successfully', 'html' => $result));
                    } else {
                        throw new Exception('An error occured in update');
                        echo json_encode(array('response' => 'error', 'msg' => 'Some problem occured'));
                    }
                    $transaction->commit();
                } catch (Exception $error) {
                    $transaction->rollBack();
                    $result = array('status' => '0', 'msg' => $error->getMessage());

                    echo json_encode(array('response' => 'error', 'msg' => $error->getMessage()));
                }
            } else {
                echo json_encode(array('response' => 'error', 'msg' => 'Available quantity in stock = ' . $available_quantity . ' ' . $unit));
            }
        }
    }

    public function actionremoveitem()
    {
        $data = $_REQUEST['data'];
        $tblpx = Yii::app()->db->tablePrefix;
        $item_detail = Yii::app()->db->createCommand(" SELECT * FROM {$tblpx}warehousedespatch_items  WHERE item_id=" . $data['item_id'] . "")->queryRow();
        $detail=WarehousedespatchItems::model()->findByPk($data['item_id']);
        $item_id = $item_detail['warehousedespatch_itemid'];
        $warehousestock_itemid_dimension_category = $item_detail['warehousestock_itemid_dimension_category'];
        $dimension = $item_detail['dimension'];
        $warehouseid = $item_detail['warehousedespatch_warehouseid'];
        $batch = $item_detail['warehousedespatch_batch'];
        $rate = $item_detail['warehousedespatch_rate'];
        $quantity = $item_detail['warehousedespatch_baseunit_quantity'];
        $transfer_type = $data['transfer_type'];
        $warehousedespatch_item_id = $data['item_id'];
        $warehousedespatch_id = $data['despatch_id'];
        $warehousedespatchfrom_final_qty = 0;
        $warehousedespatchto_final_qty = 0;
        $del = Yii::app()->db->createCommand()->delete($tblpx . 'warehousedespatch_items', 'item_id=:id', array(':id' => $data['item_id']));
        $newmodel = new JpLog('search');
        $newmodel->log_data = json_encode( $detail->attributes);
        $newmodel->log_action = 2;
        $newmodel->log_table = $tblpx . "warehousedespatch_items";
        $newmodel->log_datetime = date('Y-m-d H:i:s');
        $newmodel->log_action_by = Yii::app()->user->id;
        //$newmodel->company_id = $detail->company_id;
         if ($newmodel->save()) {
         }
        if ($del) {

           
            $stock_sql = "SELECT warehousestock_stock_quantity,warehousestock_stock_quantity,rate,warehousestock_id,warehousestock_initial_quantity FROM {$tblpx}warehousestock WHERE warehousestock_warehouseid=" . $warehouseid . " AND warehousestock_itemid=" . $item_id . " AND rate = $rate ";
            if ($batch == null) {
                $stock_sql .= " AND (batch IS Null OR batch ='" . $batch . "')";
            } else {
                $stock_sql .= " AND batch = '" . $batch . "'";
            }
            if ($dimension == null) {
                $stock_sql .= " AND (dimension IS Null OR dimension ='" . $dimension . "') AND warehousestock_itemid_dimension_category = '" . $warehousestock_itemid_dimension_category . "'";
            } else {
                $stock_sql .= " AND dimension = '" . $dimension . "' AND warehousestock_itemid_dimension_category ='" . $warehousestock_itemid_dimension_category . "'";
            }
            $stock = Yii::app()->db->createCommand($stock_sql)->queryRow();

            if (!empty($stock)) {
                if ($batch == null) {
                    $item_receipt_sql = "SELECT SUM(warehousereceipt_baseunit_accepted_effective_quantity)as quantity FROM {$tblpx}warehousereceipt_items WHERE warehousereceipt_warehouseid=" . $warehouseid . " AND warehousereceipt_itemid=" . $item_id . " AND (warehousereceipt_batch IS Null OR warehousereceipt_batch ='" . $batch . "') AND warehousereceipt_rate  LIKE '%" . $rate . "%'";
                    $item_despatch_sql = "SELECT SUM(warehousedespatch_baseunit_quantity)as quantity FROM {$tblpx}warehousedespatch_items WHERE warehousedespatch_warehouseid=" . $warehouseid . " AND warehousedespatch_itemid=" . $item_id . " AND (warehousedespatch_batch IS Null OR warehousedespatch_batch ='" . $batch . "') AND warehousedespatch_rate = $rate ";
                } else {
                    $item_receipt_sql = "SELECT SUM(warehousereceipt_baseunit_accepted_effective_quantity)as quantity FROM {$tblpx}warehousereceipt_items WHERE warehousereceipt_warehouseid=" . $warehouseid . " AND warehousereceipt_itemid=" . $item_id . " AND warehousereceipt_batch='" . $batch . "' AND warehousereceipt_rate = $rate ";
                    $item_despatch_sql = "SELECT SUM(warehousedespatch_baseunit_quantity)as quantity FROM {$tblpx}warehousedespatch_items WHERE warehousedespatch_warehouseid=" . $warehouseid . " AND warehousedespatch_itemid=" . $item_id . " AND warehousedespatch_batch='" . $batch . "' AND warehousedespatch_rate = $rate ";
                }
                if ($dimension == null) {
                    $item_receipt_sql .= " AND (dimension IS Null OR dimension ='')";
                    $item_despatch_sql .= " AND (dimension IS Null OR dimension ='')";
                } else {
                    $item_receipt_sql .= " AND dimension LIKE '%" . $dimension . "%'";
                    $item_despatch_sql .= " AND dimension LIKE '%" . $dimension . "%'";
                }
                $item_receipt = Yii::app()->db->createCommand($item_receipt_sql)->queryRow();
                $item_despatch = Yii::app()->db->createCommand($item_despatch_sql)->queryRow();
                $final_quantity = ($stock['warehousestock_initial_quantity'] + $item_receipt['quantity']) - $item_despatch['quantity'];
                $warehousedespatchfrom_final_qty_detail = Yii::app()->db->createCommand(" SELECT SUM(warehousedespatch_baseunit_quantity)as quantity FROM {$tblpx}warehousedespatch_items  WHERE warehousedespatch_id=" . $warehousedespatch_id . "")->queryRow();
                $warehousedespatchfrom_final_qty = (isset($warehousedespatchfrom_final_qty_detail['quantity']) && $warehousedespatchfrom_final_qty_detail['quantity'] != '') ? $warehousedespatchfrom_final_qty_detail['quantity'] : 0;
                $warehousedespatchfrom_details = Warehousedespatch::model()->findByPk($warehousedespatch_id);
                $warehousedespatchfrom_details->warehousedespatch_quantity = $warehousedespatchfrom_final_qty;
                $warehousedespatchfrom_details->save(false);

                $model2 = Warehousestock::model()->findByPk($stock['warehousestock_id']);
                $model2->warehousestock_stock_quantity = $final_quantity;
                if ($model2->save(false)) {
                    $warehousestock_check = Warehousestock::model()->findByPk($stock['warehousestock_id']);
                    if ($warehousestock_check->warehousestock_stock_quantity == 0) {
                        $del = Yii::app()->db->createCommand()->delete($tblpx . 'warehousestock', 'warehousestock_id=:warehousestock_id', array(':warehousestock_id' => $stock['warehousestock_id']));
                    }
                    $general_settings_for_auto_receipt = Controller::generalSettingsForAutoReceiptToProjectWarehouse($settings = 1);
                    if ($general_settings_for_auto_receipt == 1) {
                        $warehouse_to = $data['warehouse_to'];
                        $auto_receipt_warehouse_delete_check = WarehousereceiptItems::model()->find(array(
                            'select' => '*',
                            'condition' => 'warehousereceipt_transfer_type_item_id = :warehousereceipt_transfer_type_item_id',
                            'params' => array(':warehousereceipt_transfer_type_item_id' => $warehousedespatch_item_id),
                        ));
                        if (isset($auto_receipt_warehouse_delete_check['warehousereceipt_transfer_type_item_id']) &&  $auto_receipt_warehouse_delete_check['warehousereceipt_transfer_type_item_id'] != "") {
                            $auto_receipt_to_warehouse_item_Delete = Yii::app()->db->createCommand()->delete($tblpx . 'warehousereceipt_items', 'warehousereceipt_transfer_type_item_id=:warehousereceipt_transfer_type_item_id', array(':warehousereceipt_transfer_type_item_id' => $warehousedespatch_item_id));
                            $stock2_sql = "SELECT warehousestock_stock_quantity,warehousestock_stock_quantity,rate,warehousestock_id,warehousestock_initial_quantity FROM {$tblpx}warehousestock WHERE warehousestock_warehouseid=" . $warehouse_to . " AND warehousestock_itemid=" . $item_id . " AND rate = $rate ";
                            if ($batch == null) {
                                $stock2_sql .= " AND (batch IS Null OR batch ='" . $batch . "')";
                            } else {
                                $stock2_sql .= " AND batch = '" . $batch . "'";
                            }
                            if ($dimension == null) {
                                $stock2_sql .= " AND (dimension IS Null OR dimension ='" . $dimension . "') AND warehousestock_itemid_dimension_category = '" . $warehousestock_itemid_dimension_category . "'";
                            } else {
                                $stock2_sql .= " AND dimension = '" . $dimension . "' AND warehousestock_itemid_dimension_category ='" . $warehousestock_itemid_dimension_category . "'";
                            }
                            $stock2 = Yii::app()->db->createCommand($stock_sql)->queryRow();

                            if (!empty($stock2)) {
                                if ($batch == null) {
                                    $item_receipt2_sql = "SELECT SUM(warehousereceipt_baseunit_accepted_effective_quantity)as quantity FROM {$tblpx}warehousereceipt_items WHERE warehousereceipt_warehouseid=" . $warehouse_to . " AND warehousereceipt_itemid=" . $item_id . " AND (warehousereceipt_batch IS Null OR warehousereceipt_batch ='" . $batch . "') AND warehousereceipt_rate  LIKE '%" . $rate . "%'";
                                    $item_despatch2_sql = "SELECT SUM(warehousedespatch_baseunit_quantity)as quantity FROM {$tblpx}warehousedespatch_items WHERE warehousedespatch_warehouseid=" . $warehouse_to . " AND warehousedespatch_itemid=" . $item_id . " AND (warehousedespatch_batch IS Null OR warehousedespatch_batch ='" . $batch . "') AND warehousedespatch_rate = $rate ";
                                } else {
                                    $item_receipt2_sql = "SELECT SUM(warehousereceipt_baseunit_accepted_effective_quantity)as quantity FROM {$tblpx}warehousereceipt_items WHERE warehousereceipt_warehouseid=" . $warehouse_to . " AND warehousereceipt_itemid=" . $item_id . " AND warehousereceipt_batch='" . $batch . "' AND warehousereceipt_rate = $rate ";
                                    $item_despatch2_sql = "SELECT SUM(warehousedespatch_baseunit_quantity)as quantity FROM {$tblpx}warehousedespatch_items WHERE warehousedespatch_warehouseid=" . $warehouse_to . " AND warehousedespatch_itemid=" . $item_id . " AND warehousedespatch_batch='" . $batch . "' AND warehousedespatch_rate = $rate ";
                                }
                                if ($dimension == null) {
                                    $item_receipt2_sql .= " AND (dimension IS Null OR dimension ='')";
                                    $item_despatch2_sql .= " AND (dimension IS Null OR dimension ='')";
                                } else {
                                    $item_receipt2_sql .= " AND dimension LIKE '%" . $dimension . "%'";
                                    $item_despatch2_sql .= " AND dimension LIKE '%" . $dimension . "%'";
                                }
                                $item_receipt2 = Yii::app()->db->createCommand($item_receipt_sql)->queryRow();
                                $item_despatch2 = Yii::app()->db->createCommand($item_despatch_sql)->queryRow();
                                $final_quantity2 = ($stock2['warehousestock_initial_quantity'] + $item_receipt2['quantity']) - $item_despatch2['quantity'];
                                $warehousedespatchto_final_qty_detail = Yii::app()->db->createCommand(" SELECT SUM(warehousedespatch_baseunit_quantity)as quantity FROM {$tblpx}warehousedespatch_items  WHERE warehousedespatch_id=" . $warehousedespatch_id . "")->queryRow();
                                $warehousedespatchto_final_qty = (isset($warehousedespatchto_final_qty_detail['quantity']) && $warehousedespatchto_final_qty_detail['quantity'] != '') ? $warehousedespatchto_final_qty_detail['quantity'] : 0;
                                $warehousedespatchto_details = Warehousedespatch::model()->findByPk($warehousedespatch_id);
                                $warehousedespatchto_details->warehousedespatch_quantity = $warehousedespatchto_final_qty;
                                $warehousedespatchto_details->save(false);
                                $model3 = Warehousestock::model()->findByPk($stock2['warehousestock_id']);
                                $model3->warehousestock_stock_quantity = $final_quantity2;
                                if ($model3->save(false)) {
                                    $warehousestock_check2 = Warehousestock::model()->findByPk($stock2['warehousestock_id']);
                                    if ($warehousestock_check2->warehousestock_stock_quantity == 0) {
                                        $del2 = Yii::app()->db->createCommand()->delete($tblpx . 'warehousestock', 'warehousestock_id=:warehousestock_id', array(':warehousestock_id' => $stock2['warehousestock_id']));
                                    }
                                }
                            }
                        }
                    } else {
                        $auto_receipt_to_warehouse_item_Delete = '';
                    }
                }
            }

            $result = '';
            $receipt_sql = "SELECT wdi.*,ws.warehousestock_id 
                            FROM " . $this->tableNameAcc('warehousedespatch_items', 0) . " as wdi 
                            INNER JOIN  " . $this->tableNameAcc('warehousestock', 0) . " as ws 
                            ON wdi.warehousedespatch_warehouseid = ws.warehousestock_warehouseid 
                            AND wdi.warehousedespatch_itemid = ws.warehousestock_itemid 
                            AND wdi.warehousestock_itemid_dimension_category = ws.warehousestock_itemid_dimension_category 
                            AND wdi.warehousedespatch_rate = ws.rate WHERE wdi.warehousedespatch_id='" . $data['despatch_id'] . "'
                            AND ((wdi.warehousedespatch_batch IS NULL AND ws.batch IS NULL) OR wdi.warehousedespatch_batch = ws.batch)";
            $receipt_details = Yii::app()->db->createCommand($receipt_sql)->queryAll();
            foreach ($receipt_details as $key => $values) {
                $sql = "SELECT id, cat_id,brand_id, specification, unit "
                        . " FROM {$tblpx}specification "
                        . " WHERE id=" . $values['warehousedespatch_itemid'];
                $specification = Yii::app()->db->createCommand($sql)->queryRow();
                $sql = "SELECT * FROM {$tblpx}purchase_category "
                        . " WHERE id='" . $specification['cat_id'] . "'";
                $parent_category = Yii::app()->db->createCommand($sql)->queryRow();

                if ($specification['brand_id'] != NULL) {
                    $brand_details = Yii::app()->db->createCommand("SELECT brand_name FROM {$tblpx}brand WHERE id=" . $specification['brand_id'] . "")->queryRow();
                    $brand = '-' . $brand_details['brand_name'];
                } else {
                    $brand = '';
                }
                $batch_value = $values['warehousedespatch_batch'] . '@' . $values['warehousedespatch_rate'];
                $spc_details = $parent_category['category_name'] . $brand . ' - ' . $specification['specification'];
                $spc_id = $specification['id'];

                $result .= '<tr>';
                $result .= '<td><div id="item_sl_no">' . ($key + 1) . '</div></td>';
                $result .= '<td><div class="item_description" id="' . $spc_id . '">' . $spc_details . '</div> </td>';
                //$result .= '<td><div class="item_description" id="'+$spc_id+'">'.ucwords($description).'</div> </td>';
                $result .= '<td><div class="dimension" id="dimension' . $spc_id . '">' . $values['dimension'] . '<input type="hidden" id="warehousestock_itemid_dimension_category' . ($key + 1) . '" name="warehousestock_itemid_dimension_category" value="' . $values['warehousestock_itemid_dimension_category'] . '"></div> </td>';
                $result .= '<td><div class="item_batch" id="batch' . $spc_id . '">' . $batch_value . '<input type="hidden" id="warehousestock_id' . ($key + 1) . '" name="warehousestock_id" value="' . $values['warehousestock_id'] . '"></div> </td>';
                $result .= '<td> <div class="" id="quantity' . $spc_id . '"> ' . $values['warehousedespatch_quantity'] . '</div> </td>';
                $result .= '<td> <div class="unit" id="unit' . $spc_id . '"> ' . $values['warehousedespatch_unit'] . '<input type="hidden" id="item_unit_id' . ($key + 1) . '" name="item_unit_id" value="' . $this->GetItemunitID($values['warehousedespatch_unit']) . '"></div> </td>';
                $result .= '<td> <div class="base_qty" id="base_qty' . $spc_id . '"> ' . $values['warehousedespatch_baseqty'] . '</div> </td>';
                $result .= '<td> <div class="base_unit" id="base_unit' . $spc_id . '"> ' . $values['warehousedespatch_baseunit'] . '</div> </td>';
                $result .= '<td> <div class="base_rate" id="base_rate' . $spc_id . '"> ' . $values['warehousedespatch_baserate'] . '</div> </td>';
                $result .= '<td> <div class="unit" id="remarks' . $spc_id . '"> ' . $values['warehousedespatch_remarks'] . '</div> </td>';
                $result .= '<td width="70"><span class="icon icon-options-vertical popover-test" data-toggle="popover"   data-placement="left" type="button" data-html="true" style="cursor: pointer;" ></span>'
                    . '<div class ="popover-content hide"> '
                    . '<ul class="tooltip-hiden">'
                    . '<li><a href="#" id=' . $values['item_id'] . ' class="btn btn-default btn-xs removebtn">Delete</a></li> '
                    . '<li><a href="" id=' . $values['item_id'] . ' class="btn btn-default btn-xs edit_item" style="margin-left:3px">Edit</a></li>';
                $result .= '</ul>';
                $result .= '</div> ';
                $result .= '</td>';

                $result .= '</tr>';
            }
            echo json_encode(array('response' => 'success', 'msg' => 'Item deleted successfully', 'html' => $result, 'warehousedespatch_qty' => $warehousedespatchfrom_final_qty));
        } else {
            echo json_encode(array('response' => 'error', 'msg' => 'Some problem occured'));
        }
    }
    public function actionsaveDespatchOrAutoReceipt()
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $warehousedespatch_id = $_POST['warehousedespatch_id'];
        $general_settings_for_auto_receipt = Controller::generalSettingsForAutoReceiptToProjectWarehouse($settings = 1);
        if ($general_settings_for_auto_receipt == 1) {
            $auto_receipt_despatch_details = Warehousedespatch::model()->findByPk($warehousedespatch_id);
            $receipt_default_date = $auto_receipt_despatch_details->warehousedespatch_date;
            $receipt_vendor = 'System generated Receipt Note';
            $receipt_warehouse_to = $auto_receipt_despatch_details->warehousedespatch_warehouseid_to;
            $receipt_warehouse_from = $auto_receipt_despatch_details->warehousedespatch_warehouseid;
            $receipt_clerk = $auto_receipt_despatch_details->warehousedespatch_clerk;
            $receipt_no = 'DR-' . $auto_receipt_despatch_details->warehousedespatch_no;
            $receipt_transfer_type = '2';
            $dispatch_id = $warehousedespatch_id;
            /*  if(Yii::app()->user->role == 1){
                    $auto_receipt_to_warehouse = Controller::autoReceiptToWarehouse($receipt_warehouse_to,$receipt_warehouse_from,$receipt_default_date,$receipt_vendor,$receipt_clerk,$receipt_no,$receipt_transfer_type,$dispatch_id);
                    if($auto_receipt_to_warehouse == "true"){
                        $msg_status = 'success';
                        $msg = "Warehouse Despatch details added successfully!";
                    }else{
                        $msg_status = 'error';
                        $msg = "Some problem occured in Auto receipt!";
                    }
                }else{
                    if(Yii::app()->user->role == 2 || Yii::app()->user->role == 5 || Yii::app()->user->role == 3){ */
            $auto_receipt_warehouse_check = Controller::autoReceiptWarehouseCheck($receipt_warehouse_to, $receipt_warehouse_from, $receipt_clerk);
            if ($auto_receipt_warehouse_check == "true") {
                $auto_receipt_to_warehouse = Controller::autoReceiptToWarehouse($receipt_warehouse_to, $receipt_warehouse_from, $receipt_default_date, $receipt_vendor, $receipt_clerk, $receipt_no, $receipt_transfer_type, $dispatch_id);
                if ($auto_receipt_to_warehouse == "true") {
                    $msg_status = 'success';
                    $msg = "Warehouse Despatch details added successfully!";
                } else {
                    $msg_status = 'error';
                    $msg = "Some problem occured in Auto receipt!";
                }
            } else {
                $msg_status = 'error';
                $msg = "<span class='errorContent'>Auto receipt is not possible.</span>";
            }
            /*    }
                } */
        } else {
            $msg_status = 'success';
            $msg = "Warehouse Despatch details added successfully!";
            // dummy create button
        }
        Yii::app()->user->setFlash($msg_status, $msg);
        $this->redirect(array('index'));
    }
    public function actionsaveasexcel($id)
    {
        $model = $this->loadModel($id);
        $tblpx = Yii::app()->db->tablePrefix;
        $item_model = WarehousedespatchItems::model()->findAll(array("condition" => "warehousedespatch_id = '$model->warehousedespatch_id'"));
        $arraylabel = array('Receipt No', 'Stock of Goods Despatched To', 'Date', 'Clerk');
        $finaldata = array();
        $finaldata[0][] = $model->warehousedespatch_no;
        $finaldata[0][] = $model->warehousedespatchWarehouseidTo->warehouse_name;
        //$finaldata[0][] = $model->projectname($model->warehousedespatch_project);
        $finaldata[0][] = date("d-m-Y", strtotime($model->warehousedespatch_date));
        $finaldata[0][] = $model->clerk->first_name . ' ' . $model->clerk->last_name;

        $finaldata[1][] = '';
        $finaldata[1][] = '';
        $finaldata[1][] = '';
        $finaldata[1][] = '';

        $finaldata[2][] = 'Sl.No';
        $finaldata[2][] = 'Item';
        $finaldata[2][] = 'Dimension';
        $finaldata[2][] = 'Batch';
        $finaldata[2][] = 'Unit/Size';
        $finaldata[2][] = 'No of pieces';
        $finaldata[2][] = 'Remarks';

        if (!empty($item_model)) {
            foreach ($item_model as $k => $values) {
                $specification = Yii::app()->db->createCommand("SELECT id, parent_id,brand_id, specification, unit FROM {$tblpx}purchase_category WHERE id=" . $values['warehousedespatch_itemid'] . "")->queryRow();
                $parent_category = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}purchase_category WHERE id='" . $specification['parent_id'] . "'")->queryRow();

                if ($specification['brand_id'] != NULL) {
                    $brand_details = Yii::app()->db->createCommand("SELECT brand_name FROM {$tblpx}brand WHERE id=" . $specification['brand_id'] . "")->queryRow();
                    $brand = '-' . $brand_details['brand_name'];
                } else {
                    $brand = '';
                }

                $spc_details = $parent_category['category_name'] . $brand . ' - ' . $specification['specification'];


                $finaldata[$k + 3][] = $k + 1;
                $finaldata[$k + 3][] = $spc_details;
                $finaldata[$k + 3][] = ($values['dimension'] != '') ? $values['dimension'] : "N/A";
                $finaldata[$k + 3][] = $values['warehousedespatch_batch'];
                $finaldata[$k + 3][] = $values['warehousedespatch_unit'];
                $finaldata[$k + 3][] = $values['warehousedespatch_quantity'];
                $finaldata[$k + 3][] = $values['warehousedespatch_remarks'];
                $x = $k + 3;
            }
        }
        Yii::import('ext.ECSVExport');
        $csv = new ECSVExport($finaldata);
        $csv->setHeaders($arraylabel);
        $csv->setToAppend();
        $contentdata = $csv->toCSV();
        $csvfilename = 'Despatch' . date('d_M_Y') . '.csv';
        Yii::app()->getRequest()->sendFile($csvfilename, $contentdata, "text/csv", false);
        exit();
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Warehousedespatch the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Warehousedespatch::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Warehousedespatch $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'warehousedespatch-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actiongetUnitconversionFactor()
    {
        if (isset($_POST)) {
            extract($_POST);
            $conversion_value = 0;
            $model = UnitConversion::model()->findByAttributes(array('base_unit' => $base_unit, 'conversion_unit' => $purchase_unit, 'item_id' => $item_id));
            $conversion_value = $model['conversion_factor'];
            echo $conversion_value;
        }
    }
}
