<?php
/* @var $this MaterialEstimationController */
/* @var $data MaterialEstimation */

if ($index == 0) { ?>
    <thead>
        <tr>
            <th>SI No</th>
            <th>Project ID</th>
            <th>Material</th>
            <th>Specification</th>
            <th>Remarks</th>
            <th>Quantity</th>
            <th>Unit</th>
            <th>Rate</th>
            <th>Amount</th>
            <th>Action</th>
        </tr>
    </thead>
<?php } ?>

<tr>
    <td style="width: 50px;"><?php echo $index + 1; ?></td>
    <td>
        <?php
        $projectModel = Projects::model()->findByPk($data['project_id']);
        echo !empty($projectModel) ? CHtml::encode($projectModel->name) : "";
        ?>
    </td>
    <td>
        <?php
        $materialModel = Materials::model()->findByPk($data->material);
        echo !empty($materialModel) ? CHtml::encode($materialModel->material_name) : "";
        ?>
    </td>
    <td>
        <?php
        $specificationModel = Specification::model()->findByPk($data->specification);
        echo !empty($specificationModel) ? CHtml::encode($specificationModel->specification) : "";
        ?>
    </td>
    <td>
        <span><?php echo CHtml::encode($data->remarks); ?></span>
    </td>
    <td>
        <span><?php echo CHtml::encode($data->quantity); ?></span>
    </td>
    <td>
    <span>
        <?php 
        $unitModel = Unit::model()->findByPk($data->unit);
        echo !empty($unitModel) ? CHtml::encode($unitModel->unit_name) : "Unit not found"; 
        ?>
    </span>
    </td>
    <td>
        <span><?php echo CHtml::encode($data->rate); ?></span>
    </td>
    <td>
        <span><?php echo CHtml::encode($data->amount); ?></span>
    </td>
    <td style="width: 50px;">
        <a class="fa fa-edit editProject" data-toggle="modal" data-target=".edit" data-id="<?php echo $data->id; ?>" data-estimation="material_estimation" data-project_id="<?php echo $data->project_id; ?>"></a>
        <a class="fa fa-trash deleteProject" data-toggle="modal" data-target=".delete" data-id="<?php echo $data->id; ?>" data-estimation="material_estimation"></a>
    </td>
</tr>

