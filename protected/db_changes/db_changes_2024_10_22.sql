ALTER TABLE `jp_dailyreport` ADD `remarks` VARCHAR(255) NULL AFTER `pms_labour_template`;

ALTER TABLE `jp_dailyreport` CHANGE `approve_status` `approve_status` ENUM('1','2','3','4','5') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL COMMENT '1=>approve, 2=>not approve,3=>approved but cost not include in project report,4=>partial request 5=>Reject';

UPDATE jp_dailyreport dr JOIN jp_projects p ON dr.projectid = p.pid SET dr.labour_template = p.labour_template WHERE dr.default_labour_type = 2;


ALTER TABLE jp_dailyreport ADD pms_wpr_status ENUM('1','2') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '1' COMMENT '1=>request generated in accounts, 2=>change effected in pms';

