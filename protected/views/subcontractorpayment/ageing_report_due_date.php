<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/tableHeadFixer.js"></script>

<?php 
$tblpx  = Yii::app()->db->tablePrefix;
$where = ' where 1=1 '; 
$sql="SELECT 
            s.*
        FROM 
            jp_subcontractorbill sb 
        
        LEFT JOIN 
            jp_scquotation sq ON sq.scquotation_id = sb.scquotation_id
        LEFT JOIN 
            jp_subcontractor s ON s.subcontractor_id = sq.subcontractor_id $where  
        GROUP BY 
            s.subcontractor_id order by s.subcontractor_name ASC"  ;  
            

$aging_array = Yii::app()->db->createCommand($sql)->queryAll();
?>
<div class="container">
    <div class="clearfix">
        <br>
    <h5 class="text-center"><?php echo Yii::app()->name; ?></h5>
        <h2 class="text-center" style="padding-top: 3px;">Subcontractor Aging Summary By Bill Due Date</h2>
        <h5 class="text-center">As of <?php echo date ("d/m/Y")?></h5>
        <a href="<?php echo Yii::app()->createUrl("subcontractorpayment/report")?>" class="pull-right save_btn mt-2  save_btn btn btn-info pull-right mt-0 mb-10 margin-right-5">Subcontractor Payment Report</a>
    </div>
    <br>
    <?php if(count($aging_array) > 0){ ?>
            <div class="clearfix">
                <div class="pull-right">Total <?php echo count($aging_array) ?> records</div>
            </div>
            <?php } ?>
        <div id="parent">
            
            <table class="table" id="fixTable">
                <thead>
                    <tr>
                        <th>Subcontractor Name</th>
                        <th>Current</th>
                        <th>1-15 Days</th>
                        <th>16-30 Days</th>
                        <th>31-45 Days</th>
                        <th>>45 Days</th>
                        <th>Total</th> 
                                               
                    </tr>
                </thead>
                <tbody>
                    <?php                     
                    $tblpx  = Yii::app()->db->tablePrefix; 
                    $total_current_amount  = 0;
                    $total_amount2  = 0;
                    $total_amount3  = 0;
                    $total_amount4  = 0;
                    $total_amount5  = 0; 
                    $total_amount_main  = 0;                                        
                    foreach ($aging_array as $value) { 
                        if($value['subcontractor_id']!=""){
                        ?>
                        <tr>
                            <td><?php echo $value['subcontractor_name']; ?></td>                            
                            <td class="text-right">
                                <?php                                
                               $where = " AND date = CURDATE()";


                                $sql = "SELECT SUM(total_amount) as amount"
                                . " FROM {$tblpx}subcontractorbill as bill "
                                . " left join {$tblpx}scquotation as pu "
                                . " on pu.scquotation_id = bill.scquotation_id "
                                . " WHERE pu.subcontractor_id= ".$value['subcontractor_id'] 
                                . " $where"; 
                                
                                
                                $data = Yii::app()->db->createCommand($sql)->queryRow();
                               // echo "<pre>";print_r($data);exit;
                                $paidamount = Controller::getSubcontractorPaidBillAmount(1,$value['subcontractor_id']);
                                $amount1 = ($data['amount'])-$paidamount;
                                echo Controller::money_format_inr((($data['amount'])-$paidamount),2);
                                ?>
                                
                            </td>
                            <td class="text-right">
                                <?php
                                $where = " AND date < DATE_SUB(NOW(), INTERVAL 1 DAY) AND date > DATE_SUB(NOW(), INTERVAL 15 DAY) ";
                                $sql = "SELECT SUM(total_amount) as amount"
                                . " FROM {$tblpx}subcontractorbill as bill "
                                . " left join {$tblpx}scquotation as pu "
                                . " on pu.scquotation_id = bill.scquotation_id "
                                . " WHERE pu.subcontractor_id= ".$value['subcontractor_id'] 
                                . " $where"; 
                                $data = Yii::app()->db->createCommand($sql)->queryRow();
                                $paidamount = Controller::getSubcontractorPaidBillAmount(2,$value['subcontractor_id']);
                                $amount2 = ($data['amount'])-$paidamount;
                                echo Controller::money_format_inr((($data['amount'])-$paidamount),2);
                                ?>
                            </td>
                            <td class="text-right">
                            <?php
                                $where = " AND date < DATE_SUB(NOW(), INTERVAL 16 DAY) AND date > DATE_SUB(NOW(), INTERVAL 30 DAY) ";
                                $sql = "SELECT SUM(total_amount) as amount"
                                . " FROM {$tblpx}subcontractorbill as bill "
                                . " left join {$tblpx}scquotation as pu "
                                . " on pu.scquotation_id = bill.scquotation_id "
                                . " WHERE pu.subcontractor_id= ".$value['subcontractor_id'] 
                                . " $where"; 
                                $data = Yii::app()->db->createCommand($sql)->queryRow();
                                $paidamount = Controller::getSubcontractorPaidBillAmount(3,$value['subcontractor_id']);
                                $amount3 = ($data['amount'])-$paidamount;
                                echo Controller::money_format_inr((($data['amount'])-$paidamount),2);
                                ?>
                            </td>
                            <td class="text-right">
                                <?php
                                $where = " AND date < DATE_SUB(NOW(), INTERVAL 31 DAY) AND date > DATE_SUB(NOW(), INTERVAL 45 DAY) ";
                                $sql = "SELECT SUM(total_amount) as amount"
                                . " FROM {$tblpx}subcontractorbill as bill "
                                . " left join {$tblpx}scquotation as pu "
                                . " on pu.scquotation_id = bill.scquotation_id "
                                . " WHERE pu.subcontractor_id= ".$value['subcontractor_id'] 
                                . " $where"; 
                                $data = Yii::app()->db->createCommand($sql)->queryRow();
                                $paidamount = Controller::getSubcontractorPaidBillAmount(4,$value['subcontractor_id']);
                                $amount4 = ($data['amount'])-$paidamount;
                                echo Controller::money_format_inr((($data['amount'])-$paidamount),2);
                                ?></td>
                            <td class="text-right">
                            <?php
                                $where = " AND date < DATE_SUB(NOW(), INTERVAL 45 DAY)";
                                $sql = "SELECT SUM(total_amount) as amount"
                                . " FROM {$tblpx}subcontractorbill as bill "
                                . " left join {$tblpx}scquotation as pu "
                                . " on pu.scquotation_id = bill.scquotation_id "
                                . " WHERE pu.subcontractor_id= ".$value['subcontractor_id'] 
                                . " $where"; 
                                $data = Yii::app()->db->createCommand($sql)->queryRow();
                                $paidamount = Controller::getSubcontractorPaidBillAmount(5,$value['subcontractor_id']);
                                $amount5 = ($data['amount'])-$paidamount;
                                echo Controller::money_format_inr((($data['amount'])-$paidamount),2);
                                ?>
                            </td>
                            <td class="text-right">
                            <?php 
                            $total_amount = $amount2 + $amount3 + $amount4 +$amount5;
                            echo Controller::money_format_inr(($total_amount),2);
                            ?>
                            </td>
                        </tr>
                    <?php 
                    $total_current_amount = $total_current_amount+$amount1;
                    $total_amount2 = $total_amount2 + $amount2;
                    $total_amount3 = $total_amount3 + $amount3;
                    $total_amount4 = $total_amount4 + $amount4;
                    $total_amount5 = $total_amount5 + $amount5;
                    $total_amount_main = $total_amount_main +  $total_amount;

                    } } ?>
                    <tr>
                        <td> <b>Total</b> </td>
                        <td class="text-right"><?php echo  Controller::money_format_inr(($total_current_amount),2)  ?></td>
                        <td class="text-right"><?php echo  Controller::money_format_inr(($total_amount2),2)  ?></td>
                        <td class="text-right"><?php echo  Controller::money_format_inr(($total_amount3),2)  ?></td>
                        <td class="text-right"><?php echo  Controller::money_format_inr(($total_amount4),2)  ?></td>
                        <td class="text-right"><?php echo  Controller::money_format_inr(($total_amount5),2)  ?></td>
                        <td class="text-right"><?php echo  Controller::money_format_inr(($total_amount_main),2)  ?></td>
                    </tr>
                </tbody>
            </table>
        </div>
    
</div>
<script>
    $("#fixTable").tableHeadFixer({
        'foot': true,
        'head': true
    });
</script>
<style>
    #parent {
        max-height: 400px;
        overflow:auto;
    }
</style>
