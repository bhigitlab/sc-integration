
<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/pdfstyle.css">

<?php
    $company_address = Company::model()->findBypk(Yii::app()->user->company_id);
?>
  
<br>
<h3 class="text-center mt-3">Labour Report</h3>
<?php
if(!empty($final_array)){
?>
 <div class="d-flex">
<b class="margin-bottom-0 " style="color:red";>
                    **NOTE: REG refers to Regular  And OT refers to Overtime
            </b>
</div>
<div class="table-wrapper  margin-top-20">
<table border="1" style="width:100%;border:1px solid gray;margin:0px 30px">
    <thead class="entry-table">
            <tr>
                <th>SI No</th>
                <th>Subcontractor</th>
                <th>Project</th>
                
                <th>Approved HeadCount</th>
                
                <th>REG/OT Expense</th>
                <th>Total Expense</th>
                <th>Paid Amount</th>
                <th>Payment to be done</th>
                
            </tr>
        </thead>
        <tbody>
        <?php
        $total_amount = 0;
        foreach($final_array as $key => $data){
            if ($data['total_advance'] > 0 || $data['total_payment'] > 0 || $data['total_unapprove'] > 0 || $data['total_paid'] > 0) {
                $total_amount += $data['total_paid'];
                    ?>
                <tr>
                    <th><?php echo $key+1; ?></th>
                    <th><?php echo isset($data['subcontractor'])?$data['subcontractor']:'No Subcontractor';; ?></th>
                    <th>Total</th>
                    <th></th>
                   
                    
                    <th></th>
                    <th class="text-right"><?php echo Yii::app()->Controller->money_format_inr($data['total_payment'],2); ?></th>
                     <th class="text-right"><?php echo Yii::app()->Controller->money_format_inr($data['total_advance'],2); ?></th>
                    <th class="text-right"><?php echo Yii::app()->Controller->money_format_inr($data['total_paid'],2); ?></th>
                    
                    </tr>
                    <?php
                    if(isset($data['payment']) && !empty($data['payment'])){
                        foreach($data['payment'] as $key=> $values){
                    ?>
                    <tr>
                    <td></td>
                    <td></td>
                    <td><?php echo $values['project']; ?> </td>
                    
                    <td style="max-width: 30px; word-wrap: break-word; white-space: normal;">           <div>
                                            <?php 
                                            $list_counts_arr=array(); 
                                            if(!empty($values['approved_reg_headcount'])){
                                                $list_counts_arr=explode(',',$values['approved_reg_headcount']);

                                            }
                                            if(!empty($values['approved_reg_headcount'])){
                                                echo '<b>REG:</b> <ul>';
                                                foreach($list_counts_arr as $arr){
                                                echo '<li>'.$arr.'</li>';
                                                
                                                }
                                                echo "</ul>";

                                            }else{
                                                echo "";
                                            }   ?>
                                            </div>
                                                    <?php  if (!empty($values['approved_reg_headcount']) && !empty($values['approved_reg_headcount'])) {
                                                        echo '<div style="border-bottom: 1px solid #ccc; margin: 4px 0;"></div>'; 
                                                    }?>
                                                    
                                                   <div>
                                                        <?php 
                                                 $list_ot_counts_arr=array(); 
                                                if(!empty($values['approved_ot_headcount'])){
                                                    $list_ot_counts_arr=explode(',',$values['approved_ot_headcount']);

                                                }
                                                if(!empty($values['approved_ot_headcount'])){
                                                    echo '<b class="pink-txt-color">OT:</b> <ul>';
                                                    foreach($list_ot_counts_arr as $ot_arr){
                                                        echo "<li>".$ot_arr."</li>";
                                                    }

                                                    echo "<ul>";
                                                }
                                             ?>
                                            </div>
                        </span>
                    </td>
                    
                    <td>
                        <div class="text-right "><?php echo !empty($values['approved_reg_expense']) ?  '<b>REG:</b>'.Yii::app()->Controller->money_format_inr($values['approved_reg_expense'],2) . ' ' : ''; ?></div>
                                                    <?php  if (!empty($values['approved_reg_expense']) && !empty($values['approved_ot_expense'])) {
                                                        echo '<div style="border-bottom: 1px solid #ccc; margin: 4px 0;"></div>'; 
                                                    }?>
                                                    
                                                    <div class="text-right ">
                                                    <?php echo !empty($values['approved_ot_expense']) ? '<b>OT:</b>'. Yii::app()->Controller->money_format_inr($values['approved_ot_expense'],2) : ''; ?>
                                                    </div>


                    </td>
                    <td class="text-right"><?php echo Yii::app()->Controller->money_format_inr($values['payment'],2); ?></td>
                    <td class="text-right"><?php echo Yii::app()->Controller->money_format_inr($values['advance'],2); ?></td>
                    <td class="text-right"><?php echo Yii::app()->Controller->money_format_inr($values['paid'],2); ?></td>
                    
                    </tr>
                    <?php
                 } 
                }
            }
        } ?>
        </tbody>
        <tfoot>
            <tr>
        <th colspan="7" class="text-right"> Total</th>
       <th  class="text-right"><?php echo Yii::app()->Controller->money_format_inr($total_amount,2); ?></th>
       <!-- <th colspan="3" class="text-right"> </th> -->
       
            </tr>
        </tfoot>
</table>
</div>
  <!-- Separate Advance Details Table -->
   <h3 class="text-center mt-3">Advance Payment Details</h3>
    
 
       <div class="table-wrapper margin-top-20">
        <table border="1" style="width:100%;border:1px solid gray;margin:0px 30px">
            <thead class="entry-table">
                <tr>
                    <th>SI No</th>
                    <th>Subcontractor</th>
                    <th>Date</th>
                    <th>Description</th>
                    <th>Project</th>
                    
                    <th>Advance Amount</th>
                    
                </tr>
            </thead>
            <tbody>
                <?php
                $advance_count = 1;
                $i =0;
                foreach ($final_array as $data) { 
                    if ($data['total_advance'] > 0 || $data['total_payment'] > 0 || $data['total_unapprove'] > 0 || $data['total_paid'] > 0) {
            
                        ?>
                        <tr>
                            <th><?php echo $i + 1; ?></th>
                            <th><?php echo isset($data['subcontractor'])?$data['subcontractor']:'No Subcontractor'; ?></th>
                            
                            <th></th>
                            <th></th>
                            <th class="text-right ">Total</th>
                            <th class="text-right "><?php echo Yii::app()->Controller->money_format_inr($data['total_advance'],2); ?></th>
                        </tr>
                    
                        <?php if (isset($data['payment']) && !empty($data['payment'])) {
                             $k=0;
                        foreach ($data['payment'] as $payment_key => $values) {
                            
                            if (!empty($values['advance_details'])) {
                               
                                foreach ($values['advance_details'] as $advance) {
                                ?>
                                    <tr>
                                        <td class="text-right"><?php echo $k+1; ?></td>
                                        <td></td>
                                         <td><?php echo date('d-m-Y',strtotime($advance['date'])); ?></td>
                                          <td><?php echo $advance['description']; ?></td>
                                        <td><?php echo $values['project']; ?></td>
                                       
                                        <td class="text-right"><?php echo isset($data['subcontractor'])? Yii::app()->Controller->money_format_inr($advance['amount'], 2):"0.00"; ?></td>
                                       
                                    </tr>
                                    <?php           $k++;
                                }
                                
                            }
                            
                        }
                        }
                        $i++;
                    }
                } ?>
            </tbody>
        </table>
            </div><br><br>

<?php }else{
            echo "No data found";
        } ?>
