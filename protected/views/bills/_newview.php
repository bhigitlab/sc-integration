
<?php
$id = $data['bill_id'];
$tblpx = Yii::app()->db->tablePrefix;
$query = Yii::app()->db->createCommand("select * from {$tblpx}bills where bill_id=$id")->queryAll();
//$id= $data['project_id'];
//$sql=Yii::app()->db->createCommand("select {$tblpx}clients.name from {$tblpx}clients
// left join {$tblpx}projects ON {$tblpx}projects.client_id={$tblpx}clients.cid 
//where {$tblpx}projects.pid=$id")->queryRow();

//print_r($query);
if ($index == 0) { ?>
    <thead class="entry-table">
        <tr>
            <?php if ((isset(Yii::app()->user->role) && (in_array('/bills/view', Yii::app()->user->menuauthlist)) || (in_array('/bills/update', Yii::app()->user->menuauthlist)) || (in_array('/bills/billview', Yii::app()->user->menuauthlist)) || (in_array('/bills/editbill', Yii::app()->user->menuauthlist)))) { ?>
                <th></th>
            <?php } ?>
            <th>Sl No</th>
            <th>#ID</th>
            <th>Purchase Number</th>
            <th>Bill Number</th>
            <th>Warehouse</th>
            <th>Company</th>
            <th>Project</th>
            <th>Expense head</th>
            <th>Vendor</th>
            <th>Bill Date</th>
            <th>Created Date</th>
            <th>Amount</th>
            <th>Total Amount</th>
        </tr>
    </thead>
<?php }
$sql ="SELECT a.*, a.category_id as categoryid, b.*,a.category_id as pcategory,b.category_id as bcategory,a.remark as premark,b.remark as bremark FROM {$tblpx}purchase_items a LEFT JOIN {$tblpx}billitem b ON a.item_id = b.purchaseitem_id and b.bill_id = " . $id . " WHERE a.purchase_id = " . $data["purchase_id"] . "  GROUP by a.item_id";
$items_data = Yii::app()->db->createCommand($sql)->queryAll();

foreach ($items_data as $item_) {
    if ($item_['approve_status'] && $item_['approve_check'] == 0) {
        $row_bg_status = '1';
    }
}
if (empty($row_bg_status)) {
    $row_bg_status = Billitem::model()->findAll(array("condition" => "bill_id = $id AND rate_approve = 1 AND approve_check=0 "));
}
?>
<tr style="<?php echo (isset($row_bg_status) && !empty($row_bg_status)) ? 'background-color:#ecb4b4;' : '' ?>">
    <?php
    if ((isset(Yii::app()->user->role) && (in_array('/bills/view', Yii::app()->user->menuauthlist)) || (in_array('/bills/update', Yii::app()->user->menuauthlist)) || (in_array('/bills/billview', Yii::app()->user->menuauthlist)) || (in_array('/bills/editbill', Yii::app()->user->menuauthlist)))) {
        $class = '';
        if ($data["purchase_id"] != NULL) {
            $purchase = Purchase::model()->findByPk($data["purchase_id"]);
            if ($purchase->type == 'bill') {
                if ((in_array('/bills/billview', Yii::app()->user->menuauthlist)) || (in_array('/bills/editbill', Yii::app()->user->menuauthlist))) {
                    $class = 'icon-options-vertical';
                } else {
                    $class = '';
                }
            } else {
                if ((in_array('/bills/view', Yii::app()->user->menuauthlist)) || (in_array('/bills/update', Yii::app()->user->menuauthlist))) {
                    $class = 'icon-options-vertical';
                } else {
                    $class = '';
                }
            }
        }
        ?>
        <td>
            <span class="icon <?php echo $class; ?> popover-test" data-toggle="popover" data-placement="right" type="button"
                data-html="true" style="cursor: pointer;"></span>
            <div class="popover-content hide">
                <ul class="tooltip-hiden">
                    <?php if ($data["purchase_id"] != NULL) {
                        $purchase = Purchase::model()->findByPk($data["purchase_id"]);
                        $daybook_entry_count = Bills::model()->DaybookStatus($data["bill_id"]);
                        if ($purchase->type == 'bill') {
                            ?>
                            <?php
                            if ((in_array('/bills/billview', Yii::app()->user->menuauthlist))) {
                                ?>
                                <li><a href="<?php echo Yii::app()->createAbsoluteUrl('bills/billview', array('id' => $data["bill_id"])); ?>"
                                        class="btn btn-xs btn-default">View</a></li>
                            <?php } ?>
                            <?php
                            if ((in_array('/bills/editbill', Yii::app()->user->menuauthlist) && $daybook_entry_count == 0)) {
                                ?>
                                <li><a href="<?php echo Yii::app()->createAbsoluteUrl('bills/Editbill', array('id' => $data["bill_id"])); ?>"
                                        class="btn btn-xs btn-default">Edit</a></li>
                            <?php } ?>
                            <?php
                        } else {
                            ?>
                            <?php
                            if ((in_array('/bills/view', Yii::app()->user->menuauthlist))) {
                                ?>
                                <li><a href="<?php echo Yii::app()->createAbsoluteUrl('bills/view', array('id' => $data["bill_id"])); ?>"
                                        class="btn btn-xs btn-default">View</a></li>
                            <?php } ?>
                            <?php
                            if ((in_array('/bills/update', Yii::app()->user->menuauthlist) && $daybook_entry_count == 0)) {
                                ?>
                                <li><a href="<?php echo Yii::app()->createAbsoluteUrl('bills/update', array('id' => $data["bill_id"])); ?>"
                                        class="btn btn-xs btn-default">Edit</a></li>
                            <?php } ?>
                            <?php 
                            if(!empty($row_bg_status)){?>
                            <li><button id="<?php echo $data["bill_id"]; ?>" class="btn btn-xs btn-default approve_quant_rate approveoption_<?php echo $data["bill_id"];  ?> "><?php echo 'Approve'; ?></button></li>



                            <?php  }
                            ?>
                        <?php } ?>
                        <li>
                            <a href="#" id=<?php echo $data["bill_id"]; ?>
                                class="btn btn-xs btn-default delete_purchasebill mt-2">Delete</a>
                        </li>
                    <?php } ?>
                </ul>
            </div>
        </td>
    <?php } ?>
    <td>
        <?php echo $index + 1; ?>
    </td>
    <td><b>#
            <?php echo $data['bill_id'] ?>
        </b></td>
    <td>
        <?php
        if ($data["purchase_id"] != NULL) {
            $purchase = Purchase::model()->findByPk($data["purchase_id"]);
            echo $purchase->purchase_no;
        }
        ?>
    </td>
    <td>
        <?php echo $data["bill_number"]; ?>
    </td>
    <td>
        <?php echo $data->warehousename($data->warehouse_id); ?>
    </td>
    <td>
        <?php
        $company = Company::model()->findByPk($data["company_id"]);
        echo $company['name'];
        ?>
    </td>
    <td>
        <?php
        if ($data["purchase_id"] != NULL) {
            $tblpx = Yii::app()->db->tablePrefix;
            $purchase_data = Yii::app()->db->createCommand("SELECT {$tblpx}projects.name as project_name,{$tblpx}vendors.name as vendor_name FROM {$tblpx}purchase LEFT JOIN {$tblpx}projects ON {$tblpx}purchase.project_id = {$tblpx}projects.pid LEFT JOIN {$tblpx}vendors ON {$tblpx}purchase.vendor_id={$tblpx}vendors.vendor_id  WHERE {$tblpx}purchase.p_id = " . $data["purchase_id"] . "")->queryRow();
            echo $purchase_data['project_name'];
        }
        ?>
    </td>
    <td>
        <?php
        if ($data["purchase_id"] != NULL) {
            $tblpx = Yii::app()->db->tablePrefix;
            $expense_data = Yii::app()->db->createCommand("SELECT {$tblpx}expense_type.type_name as type_name FROM {$tblpx}expense_type LEFT JOIN {$tblpx}purchase ON {$tblpx}purchase.expensehead_id = {$tblpx}expense_type.type_id  WHERE {$tblpx}purchase.p_id = " . $data["purchase_id"] . "")->queryRow();
            echo $expense_data['type_name'];
        }
        ?>
    </td>
    <td>
        <?php
        if ($data["purchase_id"] != NULL) {
            $tblpx = Yii::app()->db->tablePrefix;
            $purchase_data = Yii::app()->db->createCommand("SELECT {$tblpx}projects.name as project_name,{$tblpx}vendors.name as vendor_name FROM {$tblpx}purchase LEFT JOIN {$tblpx}projects ON {$tblpx}purchase.project_id = {$tblpx}projects.pid LEFT JOIN {$tblpx}vendors ON {$tblpx}purchase.vendor_id={$tblpx}vendors.vendor_id  WHERE {$tblpx}purchase.p_id = " . $data["purchase_id"] . "")->queryRow();
            echo $purchase_data['vendor_name'];
        }
        ?>
    </td>
    <td>
        <?php echo date("d-m-Y", strtotime($data["bill_date"])); ?>
    </td>
    <td>
        <?php echo date("d-m-Y", strtotime($data["created_date"])); ?>
    </td>
    <td class="text-right">
        <?php echo ($data["bill_amount"] != '') ? Controller::money_format_inr($data["bill_amount"], 2, 1) : '0.00'; ?>
    </td>
    <?php
    $tot_amount = 0;
    $addcharges = Yii::app()->db->createCommand("SELECT ROUND(SUM(amount), 2) as amount FROM " . $tblpx . "additional_bill WHERE `bill_id`=" . $data->bill_id)->queryRow();
    if (!empty($addcharges)) {
        $tot_amount = $addcharges['amount'] + $data->bill_totalamount;
    }

    ?>
    <td class="text-right">
        <?php
        echo ($tot_amount != '') ? Controller::money_format_inr($tot_amount, 2, 1) : '0.00';
        ?>

    </td>
</tr>
<script>
    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });
    
   

    
</script>