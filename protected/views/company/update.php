<?php
/* @var $this ProjectsController */
/* @var $model Projects */

$this->breadcrumbs=array(
	'Company'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);
/*
$this->menu=array(
	array('label'=>'List Projects', 'url'=>array('index')),
	array('label'=>'Create Projects', 'url'=>array('create')),
	array('label'=>'View Projects', 'url'=>array('view', 'id'=>$model->pid)),
	array('label'=>'Manage Projects', 'url'=>array('admin')),
); */
?>
<div class="entries-wrapper margin-bottom-10">
    <div class="row">
        <div class="col-xs-12">
            <div class="heading-title">Edit Company</div>
            <div class="dotted-line"></div>
        </div>
    </div>
    <?php echo $this->renderPartial('_form', array('model' => $model)); ?>
</div>

