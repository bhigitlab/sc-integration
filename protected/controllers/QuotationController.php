<?php

class QuotationController extends Controller
{

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        $accessArr = array();
        $accessauthArr = array();
        $controller = Yii::app()->controller->id;
        if (isset(Yii::app()->user->menuall)) {
            if (array_key_exists($controller, Yii::app()->user->menuall)) {
                $accessArr = Yii::app()->user->menuall[$controller];
            }
        }

        if (isset(Yii::app()->user->menuauth)) {
            if (array_key_exists($controller, Yii::app()->user->menuauth)) {
                $accessauthArr = Yii::app()->user->menuauth[$controller];
            }
        }
        $access_privlg = count($accessauthArr);
        return array(
            array(
                'allow', // allow all users to perform 'index' and 'view' actions
                'actions' => $accessArr,
                'users' => array('@'),
                'expression' => "$access_privlg > 0",
            ),
            array(
                'allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => $accessauthArr,
                'users' => array('@'),
                'expression' => "$access_privlg > 0",
            ),
            array(
                'allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('converttoinvoice','addPaymentStage','getPaymentStage','deletequotation','deletePaymentStage','quotationupdate','exportQuotation1'),
                'users' => array('*'),
            ),
            array(
                'deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $model = new Quotation;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Quotation'])) {
            $model->attributes = $_POST['Quotation'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->quotation_id));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Quotation'])) {
            $model->attributes = $_POST['Quotation'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->quotation_id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $dataProvider = new CActiveDataProvider('Quotation');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model = new Quotation('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Quotation'])) {
            $model->attributes = $_GET['Quotation'];
            $model->fromdate = $_GET['Quotation']['fromdate'];
            $model->todate = $_GET['Quotation']['todate'];
        }

        $this->render('quotationlist', array(
            'model' => $model, 'dataProvider' => $model->search(),
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Quotation the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Quotation::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Quotation $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'quotation-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionUpdatequotation($qtid)
    {
        $model = Quotation::model()->find(array("condition" => "quotation_id = '$qtid'"));
        if (empty($model)) {
            $this->redirect(array('quotation/admin'));
        }
        $item_model = QuotationList::model()->findAll('qt_id=' . $qtid, array('order' => 'id DESC'));
        $tblpx = Yii::app()->db->tablePrefix;

        $payment_stage = Yii::app()->db->createCommand("SELECT max(serial_number) AS serial_number FROM {$tblpx}payment_stage WHERE quotation_id ='".$qtid."'")->queryRow();
        $serial_number = $payment_stage['serial_number'];
        
        if($serial_number==''){
            $serial_number = 1;
        }else{
            $serial_number++;
        }
        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        $newQuery = "";
        foreach ($arrVal as $arr) {
            if ($newQuery)
                $newQuery .= ' OR';
            $newQuery .= " FIND_IN_SET('" . $arr . "', company_id)";
        }
        $project = Yii::app()->db->createCommand("SELECT DISTINCT pid, name  FROM {$tblpx}projects WHERE company_id=" . Yii::app()->user->company_id . " ORDER BY name")->queryAll();
        $client = Yii::app()->db->createCommand("SELECT DISTINCT cid, name FROM {$tblpx}clients WHERE (" . $newQuery . ") ORDER BY name")->queryAll();

        $this->render('quotation_update', array(
            'model' => $model,
            'item_model' => $item_model,
            'quotation_id' => $qtid,
            'project' => $project,
            'client' => $client,
            'serial_number' => $serial_number
        ));
    }

    public function actionViewquotation($qtid)
    {
        $model = Quotation::model()->find(array("condition" => "quotation_id='$qtid'"));
        $item_model = QuotationList::model()->findAll(array("condition" => "qt_id = '$qtid'"));
        if (!empty($model)) {
            $project = Projects::model()->findByPK($model->project_id);
            $client = Clients::model()->findByPK($model->client_id);
            if (isset($_GET['exportpdf'])) {
                $this->logo = $this->realpath_logo;
                $pdfdata = $this->renderPartial('quotationview', array(
                    'model' => $model,
                    'itemmodel' => $item_model,
                    'project' => $project,
                    'client' => $client,
                    'quotation_id' => $qtid
                ),true);
                $filename = 'Quotation' . '.pdf';
                $mPDF1 = Yii::app()->ePdf->mPDF();
                $mPDF1 = Yii::app()->ePdf->mPDF('', 'A4');
                
                $sql = 'SELECT template_name '
                        . 'FROM jp_quotation_template '
                        . 'WHERE status="1"';
                $selectedtemplate =Yii::app()->db->createCommand($sql)->queryScalar();
                
                $template = $this->getTemplate($selectedtemplate);
                $mPDF1->SetHTMLHeader($template['header']);
                $mPDF1->SetHTMLFooter($template['footer']);
                $mPDF1->AddPage('','', '', '', '', 0,0,15, 45, 10,0);                
                $mPDF1->WriteHTML($pdfdata);
                $mPDF1->Output($filename, 'D');
            
        }else{
            $this->render('quotationview', array(
                'model' => $model,
                'itemmodel' => $item_model,
                'project' => $project,
                'client' => $client,
                'quotation_id' => $qtid
            ));
        }

        } else {
            $this->redirect(array('quotation/admin'));
        }
    }
    public function actionExportQuotation1($qid){
        
        $model = Quotation::model()->find(array("condition" => "quotation_id='$qid'"));
        $item_model = QuotationList::model()->findAll(array("condition" => "qt_id = '$qid'"));
        $project = Projects::model()->findByPK($model->project_id);
        $client = Clients::model()->findByPK($model->client_id);
        $company = Company::model()->findByPk($model->company_id);
        $arraylabel = array('Company', 'Project', 'Client', 'Date', 'Quotation No','Project Cost');
        $finaldata = array();

        $finaldata[0][] = $company['name'];
        $finaldata[0][] = isset($project->name) ? $project->name : '';
        $finaldata[0][] = $client->name;
        $finaldata[0][] = $model->date;
        $finaldata[0][] = $model->inv_no;
        $finaldata[0][]=$model->total_project_cost;

        $finaldata[1][] = '';
        $finaldata[1][] = '';
        $finaldata[1][] = '';
        $finaldata[1][] = '';
        $finaldata[1][] = '';

        $finaldata[2][] = '';
        $finaldata[2][] = '';
        $finaldata[2][] = 'Payment Stages';
        $finaldata[2][] = '';
        $finaldata[2][] = '';
         
        $finaldata[3][] = 'SI';
        $finaldata[3][] = 'Stage';
        $finaldata[3][] = 'Stage Percentage';
        $finaldata[3][] = 'Amount';
        $finaldata[3][] = 'Invoice Status';
        $i =4;
        $payment_stage = Yii::app()->db->createCommand("SELECT * FROM `jp_payment_stage` WHERE `quotation_id` = ".$qid."  ORDER BY `serial_number` ASC ")->queryAll();
        if(!empty($payment_stage)){
            $k=1;
            foreach ($payment_stage as $key => $value) {
                $finaldata[$i][] = $value['serial_number'];
                $finaldata[$i][] = $value['payment_stage'];
                $finaldata[$i][] = number_format($value['stage_percent'],2,'.','');
                $finaldata[$i][] = $value['stage_amount'];
                $finaldata[$i][] = ($value['invoice_status']==0)?'Pending':'Invoice Generated';
                $k++;
                $i++;
            }
        }
        
       
        //print_r($finaldata); exit();
        Yii::import('ext.ECSVExport');

        $csv = new ECSVExport($finaldata);
        $csv->setHeaders($arraylabel);
        $csv->setToAppend();
        $contentdata = $csv->toCSV();
        $csvfilename = 'Quotation' . date('d_M_Y') . '.csv';
        Yii::app()->getRequest()->sendFile($csvfilename, $contentdata, "text/csv", false);
        exit();

    }

    public function actionSavetopdf()
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $project_id = $fromdate = $todate = ''; //echo $_REQUEST['fromdate'];exit;
        if (!empty($_REQUEST['fromdate'])) {
            $from_date = $_REQUEST['fromdate'];
            $fromdate = date('Y-m-d', strtotime($from_date));
        }
        if (!empty($_REQUEST['todate'])) {
            $to_date = $_REQUEST['todate'];
            $todate = date('Y-m-d', strtotime($to_date));
        }
        if (!empty($_REQUEST['project_id'])) {
            $project_id = $_REQUEST['project_id'];
        }
        $model = new Quotation();
        $model->fromdate = $fromdate;
        $model->todate = $todate;
        $model->project_id = $project_id;
        $expense_entries = $model->pdfsearch();            //echo "<pre>";print_r($expense_entries);exit;

        $mPDF1 = Yii::app()->ePdf->mPDF();

        $mPDF1 = Yii::app()->ePdf->mPDF('', 'A4');

        $mPDF1->WriteHTML($stylesheet, 1);
        $mPDF1->WriteHTML($this->renderPartial('savetopdf', array(
            'expense_entries' => $expense_entries, 'project_id' => $bill_number, 'from_date' => $fromdate, 'to_date' => $todate
        ), true));

        $mPDF1->Output('Quotation' . '.pdf', 'D');
    }

    public function actionSavetoexcel()
    {

        $tblpx = Yii::app()->db->tablePrefix;
        $details = "";
        $arraylabel = array('Quotation Number', 'Client', 'Date', 'Sub Total', 'Tax Total', 'Total Amount');
        $finaldata = array();
        $model = new Quotation();
        $project_id = $fromdate = $todate = '';
        if (!empty($_REQUEST['fromdate'])) {
            $from_date = $_REQUEST['fromdate'];
            $fromdate = date('Y-m-d', strtotime($from_date));
        }
        if (!empty($_REQUEST['todate'])) {
            $to_date = $_REQUEST['todate'];
            $todate = date('Y-m-d', strtotime($to_date));
        }
        if (!empty($_REQUEST['project_id'])) {
            $project_id = $_REQUEST['project_id'];
        }

        $model->fromdate = $fromdate;
        $model->todate = $todate;
        $model->project_id = $project_id;
        $expense_entries = $model->pdfsearch();
        $k = 0;
        if (!empty($expense_entries)) {
            foreach ($expense_entries as $data) {
                $pmodel = Clients::model()->findBypk($data['client_id']);
                $clientname = $pmodel->name;
                $finaldata[$k][] = $data['inv_no'];
                $finaldata[$k][] = $clientname;
                $finaldata[$k][] = $data['date'];
                $finaldata[$k][] = $data['subtotal'];
                $finaldata[$k][] = $data['tax_amount'];
                $finaldata[$k][] = $data['amount'] + $data['tax_amount'];

                $k++;
            }
        }



        Yii::import('ext.ECSVExport');

        $csv = new ECSVExport($finaldata);
        $csv->setHeaders($arraylabel);
        $csv->setToAppend();
        $contentdata = $csv->toCSV();
        $csvfilename = 'Quotation' . date('d_M_Y') . '.csv';
        Yii::app()->getRequest()->sendFile($csvfilename, $contentdata, "text/csv", false);
        exit();
    }

    public function actionAddquotation()
    { 
        $model = new Quotation;
        $newmodel = new QuotationList;
        $tblpx = Yii::app()->db->tablePrefix;
        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        $newQuery = "";
        foreach ($arrVal as $arr) {
            if ($newQuery)
                $newQuery .= ' OR';
            $newQuery .= " FIND_IN_SET('" . $arr . "', company_id)";
        }
        $project = Yii::app()->db->createCommand("SELECT DISTINCT pid, name  FROM {$tblpx}projects WHERE company_id=" . Yii::app()->user->company_id . " ORDER BY name")->queryAll();
        $client = Yii::app()->db->createCommand("SELECT DISTINCT cid, name FROM {$tblpx}clients WHERE (" . $newQuery . ") ORDER BY name")->queryAll();
        $this->render('quotation', array(
            'model' => $model,
            'newmodel' => $newmodel,
            'project' => $project,
            'client' => $client,
        ));
    }
    public function actionQuotationupdate(){
       //echo"<pre>"; print_r($_POST);exit;
       if($_SERVER["REQUEST_METHOD"] == "POST"){
         $quotation_id = $_POST["quotation_id"];
         $model = Quotation::model()->findByPk($quotation_id);
         $date = date("Y-m-d", strtotime($_POST["date"]));
         if ($model !== null) {
            $model->attributes = $_POST; // Assign all POST data to model attributes
            $model->date =  $date;         
            // Save the model
            if ($model->save()) {
              echo "1";
            } else {
                // Handle save errors
               echo  "2";
                print_r($model->getErrors()); // Print errors for debugging
            }
        } else {
            // Handle model not found
           echo "3";
        }
       }
        
    }

    public function actiongetClient()
    {
        if (!empty($_POST["project_id"])) {
            $tblpx = Yii::app()->db->tablePrefix;
            $sql = Yii::app()->db->createCommand("SELECT {$tblpx}clients.* FROM {$tblpx}projects INNER JOIN {$tblpx}clients ON {$tblpx}clients.cid= {$tblpx}projects.client_id WHERE {$tblpx}projects.pid=" . $_POST["project_id"] . " AND {$tblpx}clients.company_id=" . Yii::app()->user->company_id . "")->queryRow();
            if (!empty($sql)) {
                $result['id'] = $sql['cid'];
                $result['name'] = $sql['name'];
                $result['status'] = 'success';
            } else {
                $result['status'] = 'error';
            }
            echo json_encode($result);
        }
    }

    public function actionAjaxdate()
    {
        echo json_encode('success');
    }

    public function actionCreatenewquotation()
    {
        $quotation_id = $_REQUEST['quotation_id'];
        if (isset($_REQUEST['inv_no']) && isset($_REQUEST['default_date']) && isset($_REQUEST['client']) && isset($_REQUEST['project'])) {
            if ($quotation_id == 0) {
                $tblpx = Yii::app()->db->tablePrefix;
                $res = Quotation::model()->findAll(array('condition' => 'inv_no = "' . $_REQUEST['inv_no'] . '"'));
                if (empty($res)) {
                    $model = new Quotation;
                    $model->inv_no = $_REQUEST['inv_no'];
                    $model->project_id = $_REQUEST['project'];
                    $model->client_id = $_REQUEST['client'];
                    $model->date = date('Y-m-d', strtotime($_REQUEST['default_date']));
                    $model->created_date = date('Y-m-d');
                    $model->created_by = Yii::app()->user->id;
                    $model->company_id = $_REQUEST['company'];
                    $model->total_project_cost = $_REQUEST['project_cost'];
                    $model->save(false);
                    $last_id = $model->quotation_id;
                    echo json_encode(array('response' => 'success', 'msg' => 'Quotation added successfully', 'quotation_id' => $last_id));
                } else {
                    echo json_encode(array('response' => 'error', 'msg' => 'Quotation number already exist'));
                }
            } else {

                $model = Quotation::model()->findByPk($quotation_id);
                $model->inv_no = $_REQUEST['inv_no'];
                $model->project_id = $_REQUEST['project'];
                $model->client_id = $_REQUEST['client'];
                $model->date = date('Y-m-d', strtotime($_REQUEST['default_date']));
                $model->created_date = date('Y-m-d');
                $model->company_id = $_REQUEST['company'];
                $model->created_by = Yii::app()->user->id;
                $model->total_project_cost = $_REQUEST['project_cost'];
                if ($model->save(false)) {
                    echo json_encode(array('response' => 'success', 'msg' => 'Quotation updated successfully', 'quotation_id' => $quotation_id));
                } else {
                    echo json_encode(array('response' => 'error', 'msg' => 'Please enter all details'));
                }                
            }
        }
    }

    public function actionQuotationitem()
    {
        $data = $_REQUEST['data'];

        $result = '';
        // $result['html'] = '';
        $html = '';
        if ($data['quotation_id'] == 0) {
            echo json_encode(array('response' => 'error', 'msg' => 'Please enter quotation details'));
        } else {
            if (isset($data['description'])) {
                $description = $data['description'];
            } else {
                $description = '';
            }

            if (isset($data['quantity'])) {
                $quantity = $data['quantity'];
            } else {
                $quantity = '';
            }
            if (isset($data['unit'])) {
                $unit = $data['unit'];
            } else {
                $unit = '';
            }
            if (isset($data['rate'])) {
                $rate = $data['rate'];
            } else {
                $rate = '';
            }
            if (isset($data['amount'])) {
                $amount = $data['amount'];
            } else {
                $amount = '';
            }

            if (isset($data['sgst'])) {
                $sgst = $data['sgst'];
            } else {
                $sgst = '';
            }

            if (isset($data['sgst_amount'])) {
                $sgst_amount = $data['sgst_amount'];
            } else {
                $sgst_amount = '';
            }

            if (isset($data['cgst'])) {
                $cgst = $data['cgst'];
            } else {
                $cgst = '';
            }

            if (isset($data['cgst_amount'])) {
                $cgst_amount = $data['cgst_amount'];
            } else {
                $cgst_amount = '';
            }

            if (isset($data['igst'])) {
                $igst = $data['igst'];
            } else {
                $igst = '';
            }

            if (isset($data['igst_amount'])) {
                $igst_amount = $data['igst_amount'];
            } else {
                $igst_amount = '';
            }

            if (isset($data['tax_amount'])) {
                $tax_amount = $data['tax_amount'];
            } else {
                $tax_amount = '';
            }
            if (isset($data['hsn_code'])) {
                $hsn_code = $data['hsn_code'];
            } else {
                $hsn_code = '';
            }

            $item_model = new QuotationList();
            $item_model->quantity = $quantity;
            $item_model->qt_id = $data['quotation_id'];
            $item_model->unit = $unit;
            $item_model->rate = $rate;
            $item_model->amount = $amount;
            $item_model->description = $data['description'];
            $item_model->tax_amount = $tax_amount;
            $item_model->cgst = $cgst;
            $item_model->cgst_amount = $cgst_amount;
            $item_model->sgst = $sgst;
            $item_model->sgst_amount = $sgst_amount;
            $item_model->igst = $igst;
            $item_model->igst_amount = $igst_amount;
            $item_model->hsn_code = $hsn_code;
            if ($item_model->save(false)) {
                $last_id = $item_model->id;
                $tblpx = Yii::app()->db->tablePrefix;
                $data1 = Yii::app()->db->createCommand("SELECT SUM(amount) as qt_amount FROM {$tblpx}quotation_list WHERE qt_id=" . $data['quotation_id'] . "")->queryRow();
                $data2 = Yii::app()->db->createCommand("SELECT SUM(tax_amount) as qt_taxamount FROM {$tblpx}quotation_list WHERE qt_id=" . $data['quotation_id'] . "")->queryRow();
                $model = Quotation::model()->findByPk($data['quotation_id']);
                $model->amount = $data1['qt_amount'];
                $model->tax_amount = $data2['qt_taxamount'];
                $model->subtotal = $data2['qt_taxamount'];
                $grand_total = $data1['qt_amount'] + $data2['qt_taxamount'];
                if ($model->save(false)) {
                    $result = '';
                    $bill_details = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}quotation_list  WHERE qt_id=" . $data['quotation_id'] . " ORDER BY id desc")->queryAll();
                    foreach ($bill_details as $key => $values) {
                        $result .= '<tr>';
                        $result .= '<td><div id="item_sl_no">' . ($key + 1) . '</div></td>';
                        $result .= '<td><div class="item_description">' . $values['description'] . '</div> </td>';
                        $result .= '<td><div class="hsn_code">' . $values['hsn_code'] . '</div> </td>';
                        $result .= '<td class="text-right"> <div class="" id="unit"> ' . $values['quantity'] . '</div> </td>';
                        $result .= '<td> <div class="unit" id="unit"> ' . $values['unit'] . '</div> </td>';
                        $result .= '<td class="text-right"><div class="" id="rate">' . number_format($values['rate'], 2, '.', '') . '</div></td>';
                        $result .= '<td class="text-right">' . number_format($values['sgst'], 2) . '</td>';
                        $result .= '<td class="text-right">' . number_format($values['sgst_amount'], 2, '.', '') . '</td>';
                        $result .= '<td class="text-right">' . number_format($values['cgst'], 2) . '</td>';
                        $result .= '<td class="text-right">' . number_format($values['cgst_amount'], 2, '.', '') . '</td>';
                        $result .= '<td class="text-right">' . number_format($values['igst'], 2) . '</td>';
                        $result .= '<td class="text-right">' . number_format($values['igst_amount'], 2, '.', '') . '</td>';
                        $result .= '<td class="text-right">' . number_format($values['tax_amount'], 2, '.', '') . '</td>';
                        $result .= '<td class="text-right">' . number_format($values['amount'], 2, '.', '') . '</td>';


                        //$result .= '<td style="width: 60px;"><a href="#" id='.$values['id'].' class="removebtn"><span class="fa fa-trash"></span> </a> <a href="" id='.$values['id'].' class="edit_item"><span class="fa fa-edit"></span> </a></td>';
                        $result .= '<td width="60"><span class="icon icon-options-vertical popover-test" data-toggle="popover"   data-placement="left" type="button" data-html="true" style="cursor: pointer;" ></span>
                                                            <div class="popover-content hide">
                                                                <ul class="tooltip-hiden">
                                                                        <li><a href="#" id=' . $values['id'] . ' class="btn btn-xs btn-default removebtn">Delete</a></li> <li><a href="" id=' . $values['id'] . ' class="btn btn-xs btn-default edit_item">Edit</a></li></ul></div></td>';

                        $result .= '</tr>';
                    }
                    echo json_encode(array('response' => 'success', 'msg' => 'Quotation item update successfully', 'html' => $result, 'amount_total' => number_format($data1['qt_amount'], 2), 'tax_total' => number_format($data2['qt_taxamount'], 2), 'grand_total' => number_format($grand_total, 2)));
                }
            } else {
                echo json_encode(array('response' => 'error', 'msg' => 'Some problem occured'));
            }
        }
    }

    public function actionUpdatesquotationitem()
    {
        $data = $_REQUEST['data'];
        if (isset($data['description'])) {
            $description = $data['description'];
        } else {
            $description = '';
        }

        if (isset($data['quantity'])) {
            $quantity = $data['quantity'];
        } else {
            $quantity = '';
        }
        if (isset($data['unit'])) {
            $unit = $data['unit'];
        } else {
            $unit = '';
        }
        if (isset($data['rate'])) {
            $rate = $data['rate'];
        } else {
            $rate = '';
        }
        if (isset($data['amount'])) {
            $amount = $data['amount'];
        } else {
            $amount = '';
        }

        if (isset($data['sgst'])) {
            $sgst = $data['sgst'];
        } else {
            $sgst = '';
        }

        if (isset($data['sgst_amount'])) {
            $sgst_amount = $data['sgst_amount'];
        } else {
            $sgst_amount = '';
        }

        if (isset($data['cgst'])) {
            $cgst = $data['cgst'];
        } else {
            $cgst = '';
        }

        if (isset($data['cgst_amount'])) {
            $cgst_amount = $data['cgst_amount'];
        } else {
            $cgst_amount = '';
        }

        if (isset($data['igst'])) {
            $igst = $data['igst'];
        } else {
            $igst = '';
        }

        if (isset($data['igst_amount'])) {
            $igst_amount = $data['igst_amount'];
        } else {
            $igst_amount = '';
        }

        if (isset($data['tax_amount'])) {
            $tax_amount = $data['tax_amount'];
        } else {
            $tax_amount = '';
        }
        if (isset($data['hsn_code'])) {
            $hsn_code = $data['hsn_code'];
        } else {
            $hsn_code = '';
        }
        $item_model = QuotationList::model()->findByPk($data['item_id']);
        $item_model->quantity = $quantity;
        $item_model->unit = $unit;
        $item_model->rate = $rate;
        $item_model->amount = $amount;
        $item_model->description = $data['description'];
        $item_model->tax_amount = $tax_amount;
        $item_model->cgst = $cgst;
        $item_model->cgst_amount = $cgst_amount;
        $item_model->sgst = $sgst;
        $item_model->sgst_amount = $sgst_amount;
        $item_model->igst = $igst;
        $item_model->igst_amount = $igst_amount;
        $item_model->hsn_code = $hsn_code;

        if ($item_model->save(false)) {
            $tblpx = Yii::app()->db->tablePrefix;
            $data1 = Yii::app()->db->createCommand("SELECT SUM(amount) as qt_amount FROM {$tblpx}quotation_list WHERE qt_id=" . $data['quotation_id'] . "")->queryRow();
            $data2 = Yii::app()->db->createCommand("SELECT SUM(tax_amount) as qt_taxamount FROM {$tblpx}quotation_list WHERE qt_id=" . $data['quotation_id'] . "")->queryRow();
            $model = Quotation::model()->findByPk($data['quotation_id']);
            $model->amount = $data1['qt_amount'];
            $model->tax_amount = $data2['qt_taxamount'];
            $model->subtotal = $data2['qt_taxamount'];
            $grand_total = $data1['qt_amount'] + $data2['qt_taxamount'];
            if ($model->save(false)) {
                $result = '';
                $bill_details = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}quotation_list  WHERE qt_id=" . $data['quotation_id'] . " ORDER BY id desc")->queryAll();
                foreach ($bill_details as $key => $values) {
                    $result .= '<tr>';
                    $result .= '<td><div id="item_sl_no">' . ($key + 1) . '</div></td>';
                    $result .= '<td><div class="item_description">' . $values['description'] . '</div> </td>';
                    $result .= '<td><div class="hsn_code">' . $values['hsn_code'] . '</div> </td>';
                    $result .= '<td class="text-right"> <div class="" id="unit"> ' . $values['quantity'] . '</div> </td>';
                    $result .= '<td> <div class="unit" id="unit"> ' . $values['unit'] . '</div> </td>';
                    $result .= '<td class="text-right"><div class="" id="rate">' . number_format($values['rate'], 2, '.', '') . '</div></td>';
                    $result .= '<td class="text-right">' . number_format($values['sgst'], 2) . '</td>';
                    $result .= '<td class="text-right">' . number_format($values['sgst_amount'], 2, '.', '') . '</td>';
                    $result .= '<td class="text-right">' . number_format($values['cgst'], 2) . '</td>';
                    $result .= '<td class="text-right">' . number_format($values['cgst_amount'], 2, '.', '') . '</td>';
                    $result .= '<td class="text-right">' . number_format($values['igst'], 2) . '</td>';
                    $result .= '<td class="text-right">' . number_format($values['igst_amount'], 2, '.', '') . '</td>';
                    $result .= '<td class="text-right">' . number_format($values['tax_amount'], 2, '.', '') . '</td>';
                    $result .= '<td class="text-right">' . number_format($values['amount'], 2, '.', '') . '</td>';


                    //$result .= '<td style="width: 60px;"><a href="#" id='.$values['id'].' class="removebtn"><span class="fa fa-trash"></span> </a> <a href="" id='.$values['id'].' class="edit_item"><span class="fa fa-edit"></span> </a></td>';
                    $result .= '<td width="60"><span class="icon icon-options-vertical popover-test" data-toggle="popover"   data-placement="left" type="button" data-html="true" style="cursor: pointer;" ></span>
                                                            <div class="popover-content hide">
                                                                <ul class="tooltip-hiden">
                                                                       
                                                                        <li><a href="#" id=' . $values['id'] . ' class="btn btn-xs btn-default removebtn">Delete</a></li> <li><a href="" id=' . $values['id'] . '  class="btn btn-xs btn-default edit_item">Edit</a></li></ul></div></td>';

                    $result .= '</tr>';
                }
                echo json_encode(array('response' => 'success', 'msg' => 'Quotation item update successfully', 'html' => $result, 'amount_total' => number_format($data1['qt_amount'], 2), 'tax_total' => number_format($data2['qt_taxamount'], 2), 'grand_total' => number_format($grand_total, 2)));
            }
        } else {
            echo json_encode(array('response' => 'error', 'msg' => 'Some problem occured'));
        }
    }

    public function actionRemovequotationitem()
    {
        $data = $_REQUEST['data'];
        $tblpx = Yii::app()->db->tablePrefix;
        $del = Yii::app()->db->createCommand()->delete($tblpx . 'quotation_list', 'id=:id', array(':id' => $data['item_id']));
        if ($del) {
            $data1 = Yii::app()->db->createCommand("SELECT SUM(amount) as qt_amount FROM {$tblpx}quotation_list WHERE qt_id=" . $data['quotation_id'] . "")->queryRow();
            $data2 = Yii::app()->db->createCommand("SELECT SUM(tax_amount) as qt_taxamount FROM {$tblpx}quotation_list WHERE qt_id=" . $data['quotation_id'] . "")->queryRow();
            $model = Quotation::model()->findByPk($data['quotation_id']);
            $model->amount = (isset($data1['qt_amount'])) ? $data1['qt_amount'] : 0;
            $model->tax_amount = (isset($data2['qt_taxamount'])) ? $data2['qt_taxamount'] : 0;
            $model->subtotal = (isset($data1['qt_amount'])) ? $data1['qt_amount'] : 0;
            //$model->company_id = Yii::app()->user->company_id;
            $grand_total = $data1['qt_amount'] + $data2['qt_taxamount'];
            $model->save();
            $result = '';
            $bill_details = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}quotation_list  WHERE qt_id=" . $data['quotation_id'] . " ORDER BY id desc")->queryAll();
            foreach ($bill_details as $key => $values) {
                $result .= '<tr>';
                $result .= '<td><div id="item_sl_no">' . ($key + 1) . '</div></td>';
                $result .= '<td><div class="item_description">' . $values['description'] . '</div> </td>';
                $result .= '<td class="text-right"> <div class="" id="unit"> ' . $values['quantity'] . '</div> </td>';
                $result .= '<td> <div class="unit" id="unit"> ' . $values['unit'] . '</div> </td>';
                $result .= '<td class="text-right"><div class="" id="rate">' . number_format($values['rate'], 2, '.', '') . '</div></td>';
                $result .= '<td class="text-right">' . number_format($values['sgst'], 2) . '</td>';
                $result .= '<td class="text-right">' . number_format($values['sgst_amount'], 2, '.', '') . '</td>';
                $result .= '<td class="text-right">' . number_format($values['cgst'], 2) . '</td>';
                $result .= '<td class="text-right">' . number_format($values['cgst_amount'], 2, '.', '') . '</td>';
                $result .= '<td class="text-right">' . number_format($values['igst'], 2) . '</td>';
                $result .= '<td class="text-right">' . number_format($values['igst_amount'], 2, '.', '') . '</td>';
                $result .= '<td class="text-right">' . number_format($values['tax_amount'], 2, '.', '') . '</td>';
                $result .= '<td class="text-right">' . number_format($values['amount'], 2, '.', '') . '</td>';



                //$result .= '<td style="width: 60px;"><a href="#" id='.$values['id'].' class="removebtn"><span class="fa fa-trash"></span> </a> <a href="" id='.$values['id'].' class="edit_item"><span class="fa fa-edit"></span> </a></td>';
                $result .= '<td width="60"><span class="icon icon-options-vertical popover-test" data-toggle="popover"   data-placement="left" type="button" data-html="true" style="cursor: pointer;" ></span>
                                                            <div class="popover-content hide">
                                                                <ul class="tooltip-hiden">
                                                                        <li><a href="#" id=' . $values['id'] . ' class="btn btn-xs btn-default removebtn">Delete</a></li> <li><a href="" id=' . $values['id'] . '  class="btn btn-xs btn-default edit_item">Edit</a></li></ul></div></td>';

                $result .= '</tr>';
            }
            echo json_encode(array('response' => 'success', 'msg' => 'Quotation item deleted successfully', 'html' => $result, 'amount_total' => number_format($data1['qt_amount'], 2), 'tax_total' => number_format($data2['qt_taxamount'], 2), 'grand_total' => number_format($grand_total, 2)));
        } else {
            echo json_encode(array('response' => 'error', 'msg' => 'Some problem occured'));
        }
    }

    public function actionSaveQuotation($qid)
    {
        $this->logo = $this->realpath_logo;
        $model = Quotation::model()->find(array("condition" => "quotation_id='$qid'"));
        $item_model = QuotationList::model()->findAll(array("condition" => "qt_id = '$qid'"));
        $project = Projects::model()->findByPK($model->project_id);
        $client = Clients::model()->findByPK($model->client_id);

        $compid = Yii::app()->user->company_id;
        $compmodel = Company::model()->findBypk($compid);
        $address = $compmodel->address;

        $mPDF1 = Yii::app()->ePdf->mPDF();
        $mPDF1 = Yii::app()->ePdf->mPDF('', 'A4');
               
        $pdf  = 'quotationpdf';
        $sql = 'SELECT template_name '
                . 'FROM jp_quotation_template '
                . 'WHERE status="1"';
        $selectedtemplate =Yii::app()->db->createCommand($sql)->queryScalar();
        
        $template = $this->getTemplate($selectedtemplate);
        $mPDF1->SetHTMLHeader($template['header']);
        $mPDF1->SetHTMLFooter($template['footer']);
        $mPDF1->AddPage('','', '', '', '', 0,0,40, 30, 10,0);

        $mPDF1->WriteHTML($this->renderPartial($pdf, array(
            'model' => $model,
            'address' => $address,
            'itemmodel' => $item_model,
            'project' => $project['name'],
            'client' => $client['name']
        ), true));

        $mPDF1->Output('Quotation.pdf', 'D');
    }

    public function actionExportQuotation($qid)
    {
        $model = Quotation::model()->find(array("condition" => "quotation_id='$qid'"));
        $item_model = QuotationList::model()->findAll(array("condition" => "qt_id = '$qid'"));
        $project = Projects::model()->findByPK($model->project_id);
        $client = Clients::model()->findByPK($model->client_id);
        $company = Company::model()->findByPk($model->company_id);
        $arraylabel = array('Company', 'Project', 'Client', 'Date', 'Quotation No');
        $finaldata = array();

        $finaldata[0][] = $company['name'];
        $finaldata[0][] = isset($project->name) ? $project->name : '';
        $finaldata[0][] = $client->name;
        $finaldata[0][] = $model->date;
        $finaldata[0][] = $model->inv_no;

        $finaldata[1][] = '';
        $finaldata[1][] = '';
        $finaldata[1][] = '';
        $finaldata[1][] = '';
        $finaldata[1][] = '';

        $finaldata[2][] = '';
        $finaldata[2][] = '';
        $finaldata[2][] = '';
        $finaldata[2][] = '';
        $finaldata[2][] = '';

        $finaldata[3][] = '';
        $finaldata[3][] = '';
        $finaldata[3][] = '';
        $finaldata[3][] = '';
        $finaldata[3][] = '';

        $finaldata[4][] = 'Sl.No';
        $finaldata[4][] = 'Description';
        $finaldata[4][] = 'Quantity';
        $finaldata[4][] = 'Unit';
        $finaldata[4][] = 'Rate';
        $finaldata[4][] = 'SGST';
        $finaldata[4][] = 'SGST Amount';
        $finaldata[4][] = 'CGST';
        $finaldata[4][] = 'CGST Amount';
        $finaldata[4][] = 'IGST';
        $finaldata[4][] = 'IGST Amount';
        $finaldata[4][] = 'Tax Amount';
        //			$finaldata[4][] = 'IGST';
        //			$finaldata[4][] = 'IGST Amount';
        $finaldata[4][] = 'Amount';
        //$finaldata[4][] = 'Tax Amount';
        $x = '';
        foreach ($item_model as $key => $value) {
            $finaldata[$key + 5][] = $key + 1;
            $finaldata[$key + 5][] = $value['description'];
            $finaldata[$key + 5][] = $value['quantity'];
            $finaldata[$key + 5][] = $value['unit'];
            $finaldata[$key + 5][] = $value['rate'];
            $finaldata[$key + 5][] = $value['sgst'];
            $finaldata[$key + 5][] = $value['sgst_amount'];
            $finaldata[$key + 5][] = $value['cgst'];
            $finaldata[$key + 5][] = $value['cgst_amount'];
            $finaldata[$key + 5][] = $value['igst'];
            $finaldata[$key + 5][] = $value['igst_amount'];
            $finaldata[$key + 5][] = $value['tax_amount'];
            $finaldata[$key + 5][] = $value['amount'];
            //
            $x = $key + 5;
        }

        $finaldata[$x + 1][] = '';
        $finaldata[$x + 1][] = '';
        $finaldata[$x + 1][] = '';
        $finaldata[$x + 1][] = '';
        $finaldata[$x + 1][] = '';
        $finaldata[$x + 1][] = '';
        $finaldata[$x + 1][] = '';
        $finaldata[$x + 1][] = '';
        $finaldata[$x + 1][] = '';
        $finaldata[$x + 1][] = '';
        $finaldata[$x + 1][] = '';
        $finaldata[$x + 1][] = '';
        $finaldata[$x + 1][] = '';
        $finaldata[$x + 1][] = '';
        $finaldata[$x + 1][] = '';
        $finaldata[$x + 1][] = '';

        $finaldata[$x + 2][] = '';
        $finaldata[$x + 2][] = '';
        $finaldata[$x + 2][] = '';
        $finaldata[$x + 2][] = '';
        $finaldata[$x + 2][] = '';
        $finaldata[$x + 2][] = '';
        $finaldata[$x + 2][] = '';
        $finaldata[$x + 2][] = '';
        $finaldata[$x + 2][] = '';
        $finaldata[$x + 2][] = '';
        $finaldata[$x + 2][] = 'Total';
        $finaldata[$x + 2][] = $model->tax_amount;
        $finaldata[$x + 2][] = $model->amount;
        //$finaldata[$x+2][] = $model->tax_amount;

        $finaldata[$x + 3][] = '';
        $finaldata[$x + 3][] = '';
        $finaldata[$x + 3][] = '';
        $finaldata[$x + 3][] = '';
        $finaldata[$x + 3][] = '';
        $finaldata[$x + 3][] = '';
        $finaldata[$x + 3][] = '';
        $finaldata[$x + 3][] = '';
        $finaldata[$x + 3][] = '';
        $finaldata[$x + 3][] = '';
        $finaldata[$x + 3][] = '';
        $finaldata[$x + 3][] = 'Grand Totals';
        $finaldata[$x + 3][] = $model->amount + $model->tax_amount;

        //print_r($finaldata); exit();
        Yii::import('ext.ECSVExport');

        $csv = new ECSVExport($finaldata);
        $csv->setHeaders($arraylabel);
        $csv->setToAppend();
        $contentdata = $csv->toCSV();
        $csvfilename = 'Quotation' . date('d_M_Y') . '.csv';
        Yii::app()->getRequest()->sendFile($csvfilename, $contentdata, "text/csv", false);
        exit();
    }

    public function actiongetProject()
    {

        $activeProjectTemplate = $this->getActiveTemplate();                       
        $company_id = $_POST["client_id"];
        $project_id = isset($_POST["project_id"])?$_POST["project_id"]:'';
        $tblpx = Yii::app()->db->tablePrefix;
        $html = array();
        $html['projectlist'] = '';
        $html['status'] = '';
        $html['qtn_no']=""; 
        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        $newQuery = "";
        foreach ($arrVal as $arr) {
            if ($newQuery)
                $newQuery .= ' OR';
            $newQuery .= " FIND_IN_SET('" . $arr . "', {$tblpx}clients.company_id)";
        }

        $sql = "SELECT * FROM {$tblpx}clients WHERE FIND_IN_SET(" . $company_id . ",company_id)";
        $expense_type = Yii::app()->db->createCommand($sql)->queryAll();
        $projectsql = "SELECT {$tblpx}projects.* FROM {$tblpx}clients "
            . "INNER JOIN {$tblpx}projects ON {$tblpx}clients.cid= {$tblpx}projects.client_id "
            . "WHERE {$tblpx}clients.cid =" . $_POST["client_id"] . " AND (" . $newQuery . ")";
        $projects = Yii::app()->db->createCommand($projectsql)->queryAll();

        if (!empty($projects)) {
            $html['projectlist'] = '<option value="">Select Project</option>';
            foreach ($projects as $key => $value) {
                $selected = "";
                if($value['pid']==$project_id){
                    $selected = "selected";
                }
                $html['projectlist'] .= '<option value="' . $value['pid'] . '" '.$selected.'>' . $value['name'] . '</option> ';
                $html['status'] = 'success';
            }
        } else {
            $html['status'] = 'no_success';
            $html['projectlist'] .= '<option value="">No Project</option>';
        }
        if ($activeProjectTemplate == 'TYPE-4' && ENV =='production') {
            $qtnNo =$this->getArchQuotationNo();            
            $html['qtn_no'] = $qtnNo;            
        }
        
        echo json_encode($html);
    }

    public function actiongetProject1()
    {
        if (!empty($_POST["client_id"])) {
            $tblpx = Yii::app()->db->tablePrefix;
            $user = Users::model()->findByPk(Yii::app()->user->id);
            $arrVal = explode(',', $user->company_id);
            $newQuery = "";
            foreach ($arrVal as $arr) {
                if ($newQuery)
                    $newQuery .= ' OR';
                $newQuery .= " FIND_IN_SET('" . $arr . "', {$tblpx}clients.company_id)";
            }
            $sql = Yii::app()->db->createCommand("SELECT {$tblpx}projects.* FROM {$tblpx}clients INNER JOIN {$tblpx}projects ON {$tblpx}clients.cid= {$tblpx}projects.client_id WHERE {$tblpx}clients.cid=" . $_POST["client_id"] . " AND (" . $newQuery . ")")->queryRow();
            if (!empty($sql)) {
                $result['id'] = $sql['pid'];
                $result['name'] = $sql['name'];
                $result['status'] = 'success';
            } else {
                $result['status'] = 'error';
            }
            echo json_encode($result);
        }
    }

    public function actiondynamicclient()
    {
        $company_id = $_POST['company_id'];
        $tblpx = Yii::app()->db->tablePrefix;
        $html = array();
        $html['html'] = '';
        $html['status'] = '';
        $expense_type = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}clients WHERE FIND_IN_SET(" . $company_id . ",company_id)")->queryAll();
        if (!empty($expense_type)) {
            $html['html'] .= '<option value="">Select Client</option>';
            foreach ($expense_type as $key => $value) {
                $html['html'] .= '<option value="' . $value['cid'] . '">' . $value['name'] . '</option>';
                $html['status'] = 'success';
            }
        } else {
            $html['status'] = 'no_success';
            $html['html'] .= '<option value="">Select Client</option>';
        }
        echo json_encode($html);
    }

    public function actionQspermission()
    {
        $item_id = $_REQUEST["item_id"];
        $status = $_REQUEST["status"];
        $model = QuotationList::model()->findByPk($item_id);
        if ($status == 1)
            $model->quotationitem_status = 1;
        else if ($status == 2)
            $model->quotationitem_status = 0;
        if ($model->save()) {
            echo 1;
        } else {
            echo 2;
        }
    }

    public function actionconvertToInvoice()
    {
        $quotation_id = $_REQUEST['qtid'];
        $result =  $message = '';
        if (!empty($quotation_id)) {
            $quotation_model = Quotation::model()->findByPk($quotation_id);

            $payment_stage = PaymentStage::model()->findByAttributes(array('quotation_id' => $quotation_id,'id'=>$_REQUEST['id']));
                if (!empty($quotation_model)) {
                    $invoice_model = new Invoice();
                    $invoice_model->project_id = $quotation_model->project_id;
                    $invoice_model->client_id = $quotation_model->client_id;
                    $invoice_model->date = $quotation_model->date;
                    $invoice_model->amount = $quotation_model->amount;
                    $invoice_model->created_by = $quotation_model->created_by;
                    $invoice_model->created_date = date('Y-m-d H:i:s');
                    $invoice_model->inv_no = $payment_stage['id'].'/'.$this->getInvoiceNumber();                    
                    $invoice_model->subtotal = $payment_stage->stage_amount;
                    $invoice_model->amount = $payment_stage->stage_amount;
                    $invoice_model->company_id = $quotation_model->company_id;
                    $invoice_model->stage_id = $payment_stage['id'];
                    $invoice_model->invoice_status = 'draft';
                    $invoice_model->type = 'quantity_rate';
                    if ($invoice_model->save()) {
                        $payment_stage->invoice_status = "1";
                        $payment_stage->save();
                        Yii::app()->user->setFlash('success', "Successfully Converted");                                                
                    } else{
                        Yii::app()->user->setFlash('error', 'An error ocured');
                    }
                }
                
        }
        
    }

    public function actionaddPaymentStage(){  
        $stage_amount = isset($_POST['payment_stage_amount'] )?$_POST['payment_stage_amount']:'';        
        $model = new PaymentStage;
        
        if($_POST['id'] !=""){
            $model = PaymentStage::model()->findByPk($_POST['id']);
        }
        $model->attributes = $_POST;
        $model->stage_amount = $stage_amount;
        $model->serial_number = isset($_POST['serial_number'])?$_POST['serial_number']:'';
        $model->created_date = date('Y-m-d');
        $model->created_by = Yii::app()->user->id;
        
        if($model->save()){
            echo json_encode(array('msg' => 'Stage Added'));
        }else{
            
            echo json_encode(array('msg' => 'Error Occured'));
        }
        
    }

    public function actiongetPaymentStage(){
        $model = PaymentStage::model()->findByPk($_POST['id']);
        echo json_encode(array('id' => $model->id,
            'payment_stage'=>$model->payment_stage,
            'stage_percent'=>$model->stage_percent,
            'stage_amount'=>$model->stage_amount,
            'serial_number'=>$model->serial_number
            )
        );
    }

                    
    public function actiondeletequotation(){
        $model = $this->loadModel($_REQUEST['id']);
        $model->amount = $model->total_project_cost;
        $quotationId=$_REQUEST['id'];
        $tblpx = Yii::app()->db->tablePrefix;
	    $transaction = Yii::app()->db->beginTransaction();
        try {
			if (Yii::app()->user->role != 1) {
                $paymentStages = PaymentStage::model()->findAllByAttributes(['quotation_id' => $quotationId]);
                   // echo "<pre>";print_r()
                   $invoiceExists='';
                    $payment_stage_id= array();
                    $invoiceExists = false; // Initialize invoice existence flag

                if (!empty($paymentStages)) {
                    foreach ($paymentStages as $stage) {
                        // Check if any invoice exists for the current stage
                        $exist = Invoice::model()->findByAttributes(['stage_id' => $stage->id]);
                        if (!empty($exist)) {
                            $invoiceExists = true; // Invoice exists for at least one stage
                            break; // No need to check further
                        }
                    }
                }
                  
                if ($invoiceExists) {
                        // Update status to 3
                        $success_status=3;
                         echo json_encode(array('response' => 'warning', 'msg' => 'Cannot Delete ! This record alredy in use.'));
                         exit;
                    }else{    
                    $deletedata = new Deletepending;				
                    $deletedata->deletepending_table    = $tblpx . "quotation";
                    $deletedata->deletepending_data     = json_encode($model->attributes);
                    $deletedata->deletepending_parentid = $_REQUEST['id'];
                    $deletedata->user_id                = Yii::app()->user->id;
                    $deletedata->requested_date         = date("Y-m-d");
                    if ($deletedata->save()) {
                        if(is_null($model->amount)){
                            $model->amount="0.00";
                        }
                        if(is_null($model->subtotal)){
                            $model->subtotal="0.00";
                        }
        
                        $model->delete_approve_status=1;
                        if (!$model->save()) {
                            $success_status = 0;
                            throw new Exception(json_encode($model->getErrors()));
                        } else {
                            $success_status = 2;					
                        }
                    } else {
                        $success_status = 0;
                        throw new Exception(json_encode($deletedata->getErrors()));
                    }
                }
			}else{


				if (!$model->delete()) {
					$success_status = 0;
					throw new Exception(json_encode($model->getErrors()));

				} else {
                    $paymentStages = PaymentStage::model()->findAllByAttributes(['quotation_id' => $quotationId]);
                   // echo "<pre>";print_r()
                   $invoiceExists='';
                    $payment_stage_id= array();
                    $invoiceExists = false; // Initialize invoice existence flag

                if (!empty($paymentStages)) {
                    foreach ($paymentStages as $stage) {
                        // Check if any invoice exists for the current stage
                        $exist = Invoice::model()->findByAttributes(['stage_id' => $stage->id]);
                        if (!empty($exist)) {
                            $invoiceExists = true; // Invoice exists for at least one stage
                            break; // No need to check further
                        }
                    }
                }
                  
                    if ($invoiceExists) {
                        // Update status to 3
                        $success_status=3;
                         echo json_encode(array('response' => 'warning', 'msg' => 'Cannot Delete ! This record alredy in use.'));
                         exit;
                    }else{
                     $newmodel = new JpLog('search');
                    $newmodel->log_data = json_encode($model->attributes);
                    $newmodel->log_action = 2;
                    $newmodel->log_table = $tblpx . "quotation";
                    $newmodel->log_datetime = date('Y-m-d H:i:s');
                    $newmodel->log_action_by = Yii::app()->user->id;
                    $newmodel->company_id = $model->company_id;
                    $newmodel->save();
					$success_status = 1;
							

                    }
                   			
				}
			}

            $transaction->commit();
        } catch (Exception $error) {
            $transaction->rollBack();
            $success_status = 0;
        } finally {
           
            if ($success_status == 1) {
                echo json_encode(array('response' => 'success', 'msg' => 'Quotation Deleted Successfully'));
				
            }else if ($success_status == 2) {
				echo json_encode(array('response' => 'success', 'msg' => 'Quotation Delete Request Sent Successfully'));
			} else {
                if (isset($error->errorInfo) && ($error->errorInfo[1] == 1451)) {
                    echo json_encode(array('response' => 'warning', 'msg' => 'Cannot Delete ! This record alredy in use.'));
                } else {
                    echo json_encode(array('response' => 'error', 'msg' => 'Some Error Occured'));
                }
            }
        }
    }

    public function actiondeletePaymentStage(){
        $model = PaymentStage::model()->findByPk($_REQUEST['id']);
	
        $tblpx = Yii::app()->db->tablePrefix;
	    $transaction = Yii::app()->db->beginTransaction();
        try {
			if (Yii::app()->user->role != 1) {
				$deletedata = new Deletepending;				
				$deletedata->deletepending_table    = $tblpx . "payment_stage";
				$deletedata->deletepending_data     = json_encode($model->attributes);
				$deletedata->deletepending_parentid = $_REQUEST['id'];
				$deletedata->user_id                = Yii::app()->user->id;
				$deletedata->requested_date         = date("Y-m-d");
				if ($deletedata->save()) {
					
					$model->delete_approve_status=1;
					if (!$model->save()) {
						$success_status = 0;
						throw new Exception(json_encode($model->getErrors()));
					} else {
						$success_status = 2;					
					}
				} else {
					$success_status = 0;
					throw new Exception(json_encode($deletedata->getErrors()));
				}
			}else{

				if (!$model->delete()) {
					$success_status = 0;
					throw new Exception(json_encode($model->getErrors()));
				} else {
					$success_status = 1;
										
				}
			}

            $transaction->commit();
        } catch (Exception $error) {
            $transaction->rollBack();
            $success_status = 0;
        } finally {
            if ($success_status == 1) {
                echo json_encode(array('response' => 'success', 'msg' => 'Payment Stage Deleted Successfully'));
				
            }else if ($success_status == 2) {
				echo json_encode(array('response' => 'success', 'msg' => 'Payment Stage Delete Request Sent Successfully'));
			} else {
                if (isset($error->errorInfo) && ($error->errorInfo[1] == 1451)) {
                    echo json_encode(array('response' => 'warning', 'msg' => 'Cannot Delete ! This record alredy in use.'));
                } else {
                    echo json_encode(array('response' => 'error', 'msg' => 'Some Error Occured'));
                }
            }
        }
    }

    public function getArchQuotationNo(){
        $sql = "SELECT count(*)  FROM `jp_quotation`";  
        $counter = Yii::app()->db->createCommand($sql)->queryScalar();
        if($counter == 0){
            $quotationno = str_pad($counter+1,3,"0",STR_PAD_LEFT);
            $qtnNo = date('y')."/ARCH/SQ-".$quotationno;
        }else{
            $last_qn_no = Yii::app()->db->createCommand("SELECT inv_no FROM `jp_quotation` ORDER BY quotation_id DESC LIMIT 1")->queryScalar();

			$invArray = explode('-',$last_qn_no);
			
			$next_inv = str_pad($invArray[1]+1,3,"0",STR_PAD_LEFT);
			$qtnNo = date('y')."/ARCH/SQ-".$next_inv;
        }
        return $qtnNo;
    }
     
}
