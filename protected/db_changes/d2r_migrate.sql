INSERT INTO `jp_bills` (`bill_id`, `purchase_id`, `bill_number`, `bill_date`, `bill_amount`, `bill_taxamount`, `bill_discountamount`, `bill_totalamount`, `bill_additionalcharge`, `company_id`, `bill_status`, `round_off`, `created_by`, `created_date`, `updated_date`) 
SELECT `bill_id`, `purchase_id`, `bill_number`, `bill_date`, `bill_amount`, `bill_taxamount`, `bill_discountamount`, `bill_totalamount`, `bill_additionalcharge`, `company_id`, `bill_status`, `round_off`, `created_by`, `created_date`, `updated_date` FROM d2raccouns_live_2021.jp_bills;

INSERT INTO final_sc_version2_db.jp_billitem (`billitem_id`, `bill_id`, `purchaseitem_id`, `billitem_description`, `billitem_quantity`, `billitem_unit`, `billitem_hsn_code`, `billitem_rate`, `billitem_taxslab`, `billitem_amount`, `billitem_taxpercent`, `category_id`, `remark`, `billitem_taxamount`, `billitem_discountpercent`, `billitem_cgst`, `billitem_cgstpercent`, `billitem_sgst`, `billitem_sgstpercent`, `billitem_igst`, `billitem_igstpercent`, `billitem_discountamount`, `approve_status`, `rate_approve`, `created_date`) 
SELECT  `billitem_id`, `bill_id`, `purchaseitem_id`, `billitem_description`, `billitem_quantity`, `billitem_unit`, `billitem_hsn_code`, `billitem_rate`, `billitem_taxslab`, `billitem_amount`, `billitem_taxpercent`, `category_id`, `remark`, `billitem_taxamount`, `billitem_discountpercent`, `billitem_cgst`, `billitem_cgstpercent`, `billitem_sgst`, `billitem_sgstpercent`, `billitem_igst`, `billitem_igstpercent`, `billitem_discountamount`, `approve_status`, `rate_approve`, `created_date` FROM d2raccouns_live_2021.jp_billitem;

INSERT INTO final_sc_version2_db.jp_additional_bill SELECT * FROM d2raccouns_live_2021.jp_additional_bill; 
INSERT INTO final_sc_version2_db.jp_bank SELECT * FROM d2raccouns_live_2021.jp_bank; 

INSERT INTO final_sc_version2_db.jp_brand SELECT * FROM d2raccouns_live_2021.jp_brand; 

INSERT INTO final_sc_version2_db.jp_clients SELECT * FROM d2raccouns_live_2021.jp_clients; 

INSERT INTO final_sc_version2_db.jp_company (`id`, `name`, `description`, `address`, `pincode`, `phone`, `email_id`, `company_gstnum`, `company_tolerance`, `company_popermission`, `po_email_userid`, `subco_email_userid`, `subcontractor_limit`, `auto_purchaseno`, `purchase_amount`, `expenses_email`, `expenses_percentage`, `invoice_email_userid`, `company_updateduration`, `purchaseorder_limit`, `created_by`, `created_date`)
 SELECT   `id`, `name`, `description`, `address`, `pincode`, `phone`, `email_id`, `company_gstnum`, `company_tolerance`, `company_popermission`, `po_email_userid`, `subco_email_userid`, `subcontractor_limit`, `auto_purchaseno`, `purchase_amount`, `expenses_email`, `expenses_percentage`, `invoice_email_userid`, `company_updateduration`, `purchaseorder_limit`, `created_by`, `created_date` FROM d2raccouns_live_2021.jp_company;

INSERT INTO final_sc_version2_db.jp_company_expense_type SELECT * FROM d2raccouns_live_2021.jp_company_expense_type; 

INSERT INTO final_sc_version2_db.jp_deposit SELECT * FROM d2raccouns_live_2021.jp_deposit; 

INSERT INTO final_sc_version2_db.jp_expense_type (`type_id`, `type_name`, `expense_type`, `company_id`) SELECT `type_id`, `type_name`, `expense_type`, `company_id` FROM d2raccouns_live_2021.jp_expense_type;
 


INSERT INTO final_sc_version2_db.jp_general_settings (`id`,`po_email_from`,`buyer_module`,`updated_by`,`updated_date`) SELECT `id`,`po_email_from`,`buyer_module`,`updated_by`,`updated_date` FROM d2raccouns_live_2021.jp_general_settings; 

INSERT INTO final_sc_version2_db.jp_profile_menu_settings SELECT * FROM d2raccouns_live_2021.jp_profile_menu_settings; 

INSERT INTO final_sc_version2_db.jp_projects (`pid`, `name`, `completion_date`, `start_date`, `billable`, `status`, `description`, `created_date`, `created_by`, `updated_date`, `updated_by`, `client_id`, `project_status`, `project_type`, `project_category`, `project_duration`, `work_type_id`, `sqft`, `sqft_rate`, `percentage`, `contract`, `bill_amount`, `remarks`, `site`, `billed_to_client`, `tot_expense`, `tot_receipt`, `tot_paid_to_vendor`, `company_id`, `profit_margin`, `project_quote`, `expense_percentage`, `expense_amount`, `auto_update`, `number_of_flats`, `flat_names`) 
SELECT `pid`, `name`, `completion_date`, `start_date`, `billable`, `status`, `description`, `created_date`, `created_by`, `updated_date`, `updated_by`, `client_id`, `project_status`, `project_type`, `project_category`, `project_duration`, `work_type_id`, `sqft`, `sqft_rate`, `percentage`, `contract`, `bill_amount`, `remarks`, `site`, `billed_to_client`, `tot_expense`, `tot_receipt`, `tot_paid_to_vendor`, `company_id`, `profit_margin`, `project_quote`, `expense_percentage`, `expense_amount`, `auto_update`, `number_of_flats`, `flat_names` FROM d2raccouns_live_2021.jp_projects; 


INSERT INTO final_sc_version2_db.jp_project_exptype SELECT * FROM d2raccouns_live_2021.jp_project_exptype;
INSERT INTO final_sc_version2_db.jp_project_type SELECT * FROM d2raccouns_live_2021.jp_project_type;

INSERT INTO final_sc_version2_db.jp_purchase (`p_id`, `purchase_no`, `vendor_id`, `project_id`, `expensehead_id`, `sub_total`, `total_amount`, `contact_no`, `shipping_address`, `purchase_status`, `purchase_date`, `purchase_billing_status`, `company_id`, `mail_status`, `po_companyid`, `type`, `permission_status`, `unbilled_amount`, `purchase_description`, `inclusive_gst`, `created_by`, `created_date`) SELECT `p_id`, `purchase_no`, `vendor_id`, `project_id`, `expensehead_id`, `sub_total`, `total_amount`, `contact_no`, `shipping_address`, `purchase_status`, `purchase_date`, `purchase_billing_status`, `company_id`, `mail_status`, `po_companyid`, `type`, `permission_status`, `unbilled_amount`, `purchase_description`, `inclusive_gst`, `created_by`, `created_date` FROM d2raccouns_live_2021.jp_purchase;

INSERT INTO final_sc_version2_db.jp_purchase_category (`id`, `parent_id`, `brand_id`, `category_name`, `specification`, `hsn_code`, `unit`, `spec_flag`, `type`, `company_id`, `created_by`, `created_date`, `spec_status`) SELECT `id`, `parent_id`, `brand_id`, `category_name`, `specification`, `hsn_code`, `unit`, `spec_flag`, `type`, `company_id`, `created_by`, `created_date`, `spec_status` FROM d2raccouns_live_2021.jp_purchase_category;

INSERT INTO final_sc_version2_db.jp_purchase_items (`item_id`, `purchase_id`, `description`, `quantity`, `unit`, `hsn_code`, `rate`, `amount`, `bill_id`, `category_id`, `remark`, `item_status`, `permission_status`, `created_by`, `created_date`, `or_description`, `or_quantity`, `or_unit`, `or_rate`, `or_amount`, `tax_slab`, `discount_percentage`, `discount_amount`, `tax_amount`, `tax_perc`, `cgst_amount`, `cgst_percentage`, `igst_amount`, `igst_percentage`, `sgst_amount`, `sgst_percentage`) SELECT `item_id`, `purchase_id`, `description`, `quantity`, `unit`, `hsn_code`, `rate`, `amount`, `bill_id`, `category_id`, `remark`, `item_status`, `permission_status`, `created_by`, `created_date`, `or_description`, `or_quantity`, `or_unit`, `or_rate`, `or_amount`, `tax_slab`, `discount_percentage`, `discount_amount`, `tax_amount`, `tax_perc`, `cgst_amount`, `cgst_percentage`, `igst_amount`, `igst_percentage`, `sgst_amount`, `sgst_percentage` FROM d2raccouns_live_2021.jp_purchase_items;

INSERT INTO final_sc_version2_db.jp_status SELECT * FROM d2raccouns_live_2021.jp_status;


INSERT INTO final_sc_version2_db.jp_subcontractor SELECT * FROM d2raccouns_live_2021.jp_subcontractor;

INSERT INTO final_sc_version2_db.jp_subcontractor_exptype SELECT * FROM d2raccouns_live_2021.jp_subcontractor_exptype;

INSERT INTO final_sc_version2_db.jp_tax_slabs SELECT * FROM d2raccouns_live_2021.jp_tax_slabs;

INSERT INTO final_sc_version2_db.jp_unit (`id`, `unit_name`, `company_id`, `created_by`, `created_date`) SELECT `id`, `unit_name`, `company_id`, `created_by`, `created_date` FROM d2raccouns_live_2021.jp_unit;

INSERT INTO final_sc_version2_db.jp_users (`userid`, `user_type`, `first_name`, `last_name`, `username`, `password`, `email`, `phonenumber`, `reporting_person`, `last_modified`, `reg_date`, `reg_ip`, `activation_key`, `email_activation`, `status`, `accesscard_id`, `client_id`, `designation`, `company_id`) SELECT `userid`, `user_type`, `first_name`, `last_name`, `username`, `password`, `email`, `phonenumber`, `reporting_person`, `last_modified`, `reg_date`, `reg_ip`, `activation_key`, `email_activation`, `status`, `accesscard_id`, `client_id`, `designation`, `company_id` FROM d2raccouns_live_2021.jp_users;

INSERT INTO final_sc_version2_db.jp_user_roles SELECT * FROM d2raccouns_live_2021.jp_user_roles;
INSERT INTO final_sc_version2_db.jp_vendors SELECT * FROM d2raccouns_live_2021.jp_vendors;
INSERT INTO final_sc_version2_db.jp_vendor_exptype SELECT * FROM d2raccouns_live_2021.jp_vendor_exptype;
INSERT INTO final_sc_version2_db.pms_assignpro_work_type SELECT * FROM d2raccouns_live_2021.pms_assignpro_work_type;
INSERT INTO final_sc_version2_db.project_assign SELECT * FROM d2raccouns_live_2021.project_assign;

INSERT INTO final_sc_version2_db.pms_work_type SELECT * FROM d2raccouns_live_2021.pms_work_type;

INSERT INTO `jp_general_settings` (`id`, `po_email_from`, `buyer_module`, `description`, `status`, `updated_by`, `updated_date`) VALUES (NULL, 'pc1@bluehorizoninfotech.com\r\n1', '1', '', '0', NULL, NULL);



INSERT INTO final_sc_version2_db.jp_quotation_category_master SELECT * FROM d2raccouns_live_2021.jp_quotation_category_master;
INSERT INTO final_sc_version2_db.jp_quotation_finish_master SELECT * FROM d2raccouns_live_2021.jp_quotation_finish_master;
INSERT INTO final_sc_version2_db.jp_quotation_worktype_master SELECT * FROM d2raccouns_live_2021.jp_quotation_worktype_master;
INSERT INTO final_sc_version2_db.jp_quotation_item_master (`id`, `quotation_category_id`, `work_type_id`, `worktype_label`, `work_type_description`, `image`, `shutter_material_id`, `shutter_finish_id`, `carcass_material_id`, `carcass_finish_id`, `shutterwork_description`, `caracoss_description`, `description`, `rate`, `profit`, `status`, `created_by`, `created_date`) SELECT `id`, `quotation_category_id`, `work_type_id`, `worktype_label`, `work_type_description`, `image`, `shutter_material_id`, `shutter_finish_id`, `carcass_material_id`, `carcass_finish_id`, `shutterwork_description`, `caracoss_description`, `description`, `rate`, `profit`, `status`, `created_by`, `created_date` FROM d2raccouns_live_2021.jp_quotation_item_master;

INSERT INTO final_sc_version2_db.jp_sales_quotation_master (`id`, `company_id`, `client_name`, `address`, `phone_no`, `email`, `date_quotation`, `invoice_no`, `created_by`, `created_date`) SELECT `id`, `company_id`, `client_name`, `address`, `phone_no`, `email`, `date_quotation`, `invoice_no`, `created_by`, `created_date` FROM d2raccouns_live_2021.jp_sales_quotation_master;

INSERT INTO final_sc_version2_db.jp_sales_quotation (`id`, `master_id`, `category_id`, `category_label`, `work_type`, `shutterwork_description`, `carcass_description`, `description`, `worktype_label`, `unit`, `quantity`, `quantity_nos`, `mrp`, `amount_after_discount`, `status`, `created_by`, `created_date`) SELECT `id`, `master_id`, `category_id`, `category_label`, `work_type`, `shutterwork_description`, `carcass_description`, `description`, `worktype_label`, `unit`, `quantity`, `quantity_nos`, `mrp`, `amount_after_discount`, `status`, `created_by`, `created_date` FROM d2raccouns_live_2021.jp_sales_quotation;

