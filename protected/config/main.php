<?php
ob_start();
define('EMAILFROM', "info@bhiapp.com");

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');
// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
    'theme' => 'jpvcms',
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => (defined('APP_NAME')?APP_NAME:"BHI COST Management System"),
    // preloading 'log' component
    'preload' => array('log'),
    //Yii::app()->theme = 'mortalkombat',
    'defaultController' => 'dashboard/index',
    // autoloading model and component classes
    'import' => array(
        'application.models.*',
        'application.components.*',
        'application.extensions.phpmailer.JPhpMailer',
        'application.extensions.libraries.Google.*',
        'application.modules.menu.models.*',
        'application.modules.buyer.models.*',
        'application.modules.wh.models.*',
        'application.modules.report.models.*',
        'application.extensions.yiinfinite-scroll.YiinfiniteScroller',
        'application.modules.salesexecutive.models.*',
        'application.modules.quotationitem.models.*',
        'application.modules.quotationcategory.models.*',
        'application.modules.worktype.models.*',
        'application.modules.finish.models.*',
        'application.modules.salesquotation.models.*',
        'application.modules.location.models.*',
        'ext.easyimage.EasyImage',
    ),
    'modules' => array(
        // uncomment the following to enable the Gii tool

        'gii' => array(
            'class' => 'system.gii.GiiModule',
            'password' => 'pass',
            // If removed, Gii defaults to localhost only. Edit carefully to taste.
            'ipFilters' => array('127.0.0.1', '::1'),
        ),

        'menu',
        'buyer',
        'wh',
        'report',
        'finish',
        'worktype',
        'quotationitem',
        'quotationcategory',
        'salesquotation',
        'salesexecutive',
        'location'
    ),
    // application components
    'components' => array(
        'user' => array(
            // enable cookie-based authentication
            'allowAutoLogin' => true,
            'authTimeout' => 1800, // 60*60*0.5 logout after half an hours of inactivity
            'loginUrl' => array('/site/login'),
        ),
        'session' => array(
            'timeout' => 1800,
        ),
        'easyImage' => array(
            'class' => 'application.extensions.easyimage.EasyImage',            
          ),
        // uncomment the following to enable URLs in path-format
        /*
          'urlManager'=>array(
          'urlFormat'=>'path',
          'rules'=>array(
          '<controller:\w+>/<id:\d+>'=>'<controller>/view',
          '<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
          '<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
          ),
          ),
         */
        // uncomment the following to use a MySQL database
        'db' => require "database.php",
        /**/
        'errorHandler' => array(
            // use 'site/error' action to display errors
            'errorAction' => YII_DEBUG ? null : 'site/error',
        ),
        'log' => require "errorLogSettings.php",
        'ePdf' => array(
            'class' => 'ext.yii-pdf.EYiiPdf',
            'params' => array(
                'mpdf' => array(
                    'librarySourcePath' => 'application.vendors.mpdf.*',
                    'constants' => array(
                        '_MPDF_TEMP_PATH' => Yii::getPathOfAlias('application.runtime'),
                    ),
                    'class' => 'mpdf', // the literal class filename to be loaded from the vendors folder
                    'fontDir' => Yii::getPathOfAlias('webroot.themes.jpvcms.fonts'), // Path to the font directory
                    'fontdata' => array(
                        
                        'freeserif' => array(
                            'R' => "FreeSerif.ttf",
                            'B' => "FreeSerifBold.ttf",
                            'I' => "FreeSerifItalic.ttf",
                            'BI' => "FreeSerifBoldItalic.ttf",
                            'useOTL' => 0xFF,
                            'useKashida' => 75,
                        ),
                        // Add other font configurations as needed
                    ),
                    'autoScriptToLang' => true, // Set autoScriptToLang to true
                    'autoLangToFont' => true, 
                    'default_charset' => 'UTF-8',
                ),
                'HTML2PDF' => array(
                    'librarySourcePath' => 'application.vendors.html2pdf.*',
                    'classFile' => 'html2pdf.class.php', // For adding to Yii::$classMap
                )
            ),
        ),
        'groupgridview' => array(
            'class' => 'ext.groupgridview.GroupGridView',
        ),
    ),
    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params' => array(
        'website_name' => 'http://www.jpventures.in/',
        //Google Login Api
        'client_id' => '39123662358-a4ppaf2i5vgp709h7tb8vnime0a563in.apps.googleusercontent.com',
        'client_secret' => 'AIzaSyB3JxS1O-z7uLGq08kiwZQopq6DW-K9CUQ',
        'outgoing_mail_id' => 'info@bhiapp.com', //Yii::app()->params['outgoing_mail_id'];
        'default_domain_authcheck' => 'bluehorizoninfotech.com',
        // this is used in contact page
        'enc_key' => '6[Q375Vyb+U3+5>',
        'adminEmail' => 'contact@bluehorizoninfotech.com',
        'supportEmail' => 'support@bluehorizoninfotech.com',
        'mail_footer_msg' => 'Regards, <br /> Blue Horizon Infotech support team.<hr /> Please do not reply to this message. Mail sent to this address cannot be answered. ',
    ),
);
