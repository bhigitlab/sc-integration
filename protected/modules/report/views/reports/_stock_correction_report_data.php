<div class="table-responsive">
<table class="table table-bordered" id="fixtable">
    <thead>
        <thead class="entry-table sticky-thead">
            <th>Warehouse</th>
            <th>Item</th>
            <th>Rate</th>
            <th>Dimension</th>
            <th>Batch</th>
            <th>Opening Stock</th>
            <th>Type</th>
            <th>Quantity</th>
            <th>Value</th>
            <th>Closing Stock</th>
            <th>Date</th>
            </tr>
        </thead>
    <tbody class="addrow">
        <?php
        $i = 1;
        $rateArry = array();
        if (!empty($result_data)) {
            foreach ($result_data as $view_data) {
                $warehouse_id = $view_data['warehouse'];
                if (!empty($warehouse_id)) {
                    $warehouse = Warehouse::model()->findByPk($warehouse_id);
                    $item = $view_data['item'];
                    $i = 0;
                    $j = 0;
                    ?>

                    <?php
                    foreach ($item as $key => $value) {
                        $itemId = $key;
                        $details = $value['details'];
                        $ratecount = $value['count'];
                        $count1 = 0;
                        foreach ($details as $key => $detail) {
                            $count1 += count($detail);
                        }
                        ?>
                        <tr>
                            <?php if ($i == 0) { ?>
                                <td rowspan="<?php echo $view_data['count'] ?>">
                                    <?php echo $warehouse['warehouse_name']; ?>
                                </td>
                            <?php } ?>

                            <td rowspan="<?php echo $count1 ?>">
                                <?php echo Controller::itemName($itemId); ?>
                            </td>
                            <?php
                            foreach ($details as $key => $detail) {
                                $count1 += count($detail);
                                $rate = $key;
                                ?>

                                <td rowspan="<?php echo count($detail); ?>">
                                    <?php echo $rate; ?>
                                </td>
                                <?php
                                $i = 0;
                                foreach ($detail as $key => $data) {
                                    if ($i == 0) {
                                        $used_sql = 'SELECT * FROM `pms_acc_wpr_item_consumed` '
                                            . ' WHERE `warehouse_id`=' . $data['warehouse']
                                            . ' AND `item_id`=' . $data['item']
                                            . '  AND`item_rate` =' . $data['rate'];
                                        $used_qty_in_pms = Yii::app()->db->createCommand($used_sql)->queryRow();
                                        $used_qty = !empty($used_qty_in_pms) ? $used_qty_in_pms['item_qty'] : 0;
                                        $x = $this->getOpeningStockQuantity($warehouse_id, $itemId, $data['created_date'], $rate);
                                    }
                                    $i++;
                                }
                                $cqty = $x - $used_qty;
                                foreach ($detail as $key => $data) {
                                    ?>
                                    <td>
                                        <?php echo $data['dimension']; ?>
                                    </td>
                                    <td>
                                        <?php echo isset($data['batch']) ? $data['batch'] : ""; ?>
                                    </td>

                                    <td>
                                        <?php echo $cqty; ?>
                                    </td>
                                    <td>
                                        <?php
                                        if (array_key_exists("warehousereceipt_id", $data)) {
                                            $type = 'Receipt';
                                        } else if (array_key_exists("warehousedespatch_id", $data)) {
                                            $type = 'Despatch';
                                        } else {
                                            $type = 'Stock correction';
                                        }
                                        echo $type;
                                        ?>
                                    </td>
                                    <td>
                                        <?php

                                        if ($type == 'Stock correction' && isset($data['correction_id'])) {
                                            $sql = "SELECT stock_quantity FROM `jp_warehouse_stock_correction` "
                                                . " WHERE warehouse_id=" . $warehouse_id
                                                . " AND Item_Id=" . $itemId
                                                . " AND rate=" . $data['rate'] . " AND id = " . $data['correction_id'];
                                            $latest = Yii::app()->db->createCommand($sql)->queryScalar();


                                            $sql = "SELECT stock_quantity FROM `jp_warehouse_stock_correction` "
                                                . " WHERE warehouse_id=" . $warehouse_id
                                                . " AND Item_Id=" . $itemId
                                                . " AND rate=" . $data['rate'] . "  AND id = (select max(id) "
                                                . " from jp_warehouse_stock_correction where id < " . $data['correction_id'] . ")";
                                            $prev = Yii::app()->db->createCommand($sql)->queryScalar();
                                            if ($prev != "") {
                                                $stockqty = $latest - $prev;
                                            } else {
                                                $stockqty = $data['qty'];
                                            }


                                        } else {
                                            $stockqty = $this->getQuantity($warehouse_id, $itemId, $data['created_date'], $type);
                                        }

                                        echo $stockqty;
                                        ?>
                                    </td>
                                    <td>


                                    </td>
                                    <td>
                                        <?php
                                        if ($type == 'Receipt') {
                                            $closing_quantity = $cqty + $stockqty;
                                        } else if ($type == 'Despatch') {
                                            $closing_quantity = $cqty - $stockqty;
                                        } else {
                                            $closing_quantity = $cqty + $stockqty;
                                        }

                                        $cqty = $closing_quantity;
                                        echo $closing_quantity;
                                        ?>



                                    </td>
                                    <td width="100px;" class="nowrap">
                                        <?php echo date('d-M-Y  h:i:s', strtotime($data['created_date'])); ?>
                                    </td>
                                </tr>
                            <?php } ?>


                            <?php
                            }

                            $i++;
                    }
                    ?>


                    <?php
                }
            }
        }
        ?>

    </tbody>
</table>
</div>
<script>
    $(document).ready(function () {
        // $("#fixtable").tableHeadFixer({
        //     'left': false,
        //     'foot': true,
        //     'head': true
        // });
        $('[data-toggle="popover-hover"]').popover({
            html: true,
            container: "body",
            trigger: 'hover',
            placement: 'bottom',
            content: function () {

                return '<img src="' + $(this).data('img') + '" style="height:auto;"/>';
            }
        });
    });



</script>