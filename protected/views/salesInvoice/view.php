<?php
if (isset($_GET['exportpdf'])) {
    $company_address = Company::model()->findBypk($model->company_id);
    ?>
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/pdfstyle.css">
    <?php
}
?>
<div class="container">
    <?php
    if (isset($_GET['exportpdf'])) {
        ?>
        <table border=0>
            <tbody>
                <tr>
                    <td class="img-hold">
                        <?php 
                        CHtml::image($this->logo, Yii::app()->name, 
                        array('style' => 'max-height:45px;'));
                        ?>
                    </td>
                    <td class="text-center">
                        <h1><?php echo Yii::app()->name ?></h1>
                    </td>
                </tr>
            </tbody>
        </table>
        <table border=0>
            <tbody>
                <tr>
                    <td class="text-center">
                        <p><?php echo ((isset($company_address['address']) && !empty($company_address['address'])) ? ($company_address['address']) : ""); ?><br>
                            PIN : <?php echo ((isset($company_address['pincode']) && !empty($company_address['pincode'])) ? ($company_address['pincode']) : ""); ?>,&nbsp;
                            PHONE : <?php echo ((isset($company_address['phone']) && !empty($company_address['phone'])) ? ($company_address['phone']) : ""); ?><br>
                            EMAIL : <?php echo ((isset($company_address['email_id']) && !empty($company_address['email_id'])) ? ($company_address['email_id']) : ""); ?> <br>
                            GST NO: <?php echo isset($company_address["company_gstnum"]) ? $company_address["company_gstnum"] : ''; ?></p><br>
                    </td>
                </tr>
            </tbody>
        </table>
        <h4 class="page_heading">GST INVOICE</h4>
        <table class="details">
            <tr>
                <td><label>PROJECT : </label><b><?php echo isset($model->project->name) ? $model->project->name : ''; ?></b></td>
                <td><label>CLIENT : </label><b> <?php echo isset($model->client->name) ? $model->client->name : ''; ?></td>
            </tr>
            <tr>
                <td><label>Site : </label><b>MARADU</b></td>
                <td><label>Tel : </label><b>9072654431</b></td>
            </tr>
            <tr>
                <td><label>ADDRESS : </label><b>Iyattil Junction,chittor Road Eranakulam, 682011</b></td>
                <td><label>GST NO : </label><b><?php echo (isset($model->invoice_number)) ? $model->invoice_number : ''; ?></b></td>
            </tr>
        </table>
        <br>
    <?php } else { ?>
        <div class="invoicemaindiv">
            <?php
            echo CHtml::link('SAVE AS PDF', Yii::app()->request->requestUri . "&exportpdf=1", array('class' => 'pdf_excel pull-right'));
            echo CHtml::link('SAVE AS EXCEL', Yii::app()->request->requestUri . "&exportexcel=1", array('class' => 'pdf_excel pull-right '));
            ?>
            <h2 class="purchase-title">View Invoice #<?php echo $model->invoice_number ?></h2>
        </div>
        <div class="row item_view">
            <div class="col-md-3">
                <label>COMPANY : </label>
                <?php
                echo $model->company->name;
                ?>
            </div>
            <div class="col-md-3">
                <label>PROJECT : </label>
                <?php echo isset($model->project->name) ? $model->project->name : ''; ?>
            </div>
            <div class="col-md-3">
                <label>CLIENT : </label>
                <?php echo isset($model->client->name) ? $model->client->name : ''; ?>
            </div>
            <div class="col-md-3">
                <label>DATE : </label>
                <?php echo isset($model->invoice_date) ? date('d-m-Y', strtotime($model->invoice_date)) : ''; ?>
            </div>
            <div class="col-md-3">
                <label>INVOICE No : </label>
                <?php echo isset($model->invoice_number) ? $model->invoice_number : ''; ?>
            </div>
        </div>

    <?php } ?>

    <div class="clearfix"></div>
    <div id="table-scroll" class="table-scroll">
        <div id="faux-table" class="faux-table" aria="hidden"></div>
        <div id="table-wrap" class="table-wrap">
            <div class="table-responsive">
                <table border="1" class="table">
                    <thead>
                        <tr bgcolor="#ddd">
                            <th>Sl.No</th>
                            <?php
                            if (isset($_GET['exportpdf'])) {
                                ?>
                                <th>Description</th>
                                <?php
                            } else {
                                ?>
                                <th>Title</th>
                                <th>Type</th>
                                <th>Dimesion</th>
                            <?php }
                            ?>

                            <th>Quantity</th>
                            <th>Unit</th>
                            <th>Rate</th>
                            <th>Amount</th>
                        </tr>
                    </thead>
                    <tbody class="addrow">
                        <?php
                        $i = 1;
                        $k = 0;
                        foreach ($invoice_categories as $invoice_category) {

                            $category_items = $invoice_category->salesInvoiceSubItems;
                            $sub_item_count = count($category_items);
                            $datas = $invoice_category->attributes;
                            extract($datas);
                            if (isset($_GET['exportpdf']) && $sub_item_count > 0) {
                                $k++;
                                ?>
                                <tr bgcolor="#add8e6">
                                    <th colspan="6" class="text-left border-0"><?php echo sprintf('%02s', $k) . '. ' . $main_title; ?></th>
                                </tr>
                                <?php
                            }

                            foreach ($category_items as $item) {
                                $item_data = $item->attributes;
                                extract($item_data);
                                ?>
                                <tr class="tr_class">
                                    <td><?php echo $i; ?></td>
                                    <?php
                                    if (isset($_GET['exportpdf'])) {
                                        ?>
                                        <td><?php echo $item_description; ?></td>
                                        <?php
                                    } else {
                                        ?>
                                        <td><?php echo $main_title; ?></td>
                                        <td><?php echo SalesInvoiceSubItem::model()->getType($item->item_type); ?></td>
                                        <td><?php echo SalesInvoiceSubItem::model()->getDimension($item->attributes); ?></td>
                                    <?php } ?>

                                    <td><?php echo $quantity; ?></td>
                                    <td><?php echo SalesInvoiceSubItem::model()->getUnit($item->attributes); ?></td>
                                    <td><?php echo $rate; ?></td>
                                    <td><?php echo $amount; ?></td>
                                </tr>
                                <?php
                                $i++;
                            }
                            ?>

                            <?php
                        }
                        ?>
                    </tbody>
                </table>

            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('.excelbtn1').click(function () {
            $("form#pdfvals1").attr('action', "<?php echo $this->createAbsoluteUrl('purchase/exportexcel'); ?>");
            $("form#pdfvals1").submit();

        });
    });
</script>