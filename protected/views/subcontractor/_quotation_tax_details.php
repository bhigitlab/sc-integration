<div id="tax_div">
        <div class="row">
            <div class="col-xs-12">
            <div class="heading-title">Tax Details</div>
            <div class="dotted-line"></div>
            </div>
        </div>
        <div class="panel-body append_div">            
            <div class="row"> 
                <form class="stage-form" method="POST">                      
                    <div class="form-group no-col">
                        <label>SGST(%):</label>
                        <div class="input-info">
                            <input type="text" class="sgst_percent form-control w-50p" value="<?php echo $scquotation_model->sgst_percent; ?>" data-qid = "<?php echo $scquotation_model->scquotation_id; ?>" />
                            <span class="sgst_amount value-label"><?php 
                                $amount = $scquotation_model->getTotalQuotationAmount($scquotation_model->scquotation_id);
                                $sgst_amount = ($scquotation_model->sgst_percent / 100) * $amount;
                                echo $sgst_amount;
                                ?>
                            </span>
                        </div>
                                                
                    </div>
                    <div class="form-group no-col">
                        <label>CGST(%):</label>
                        <div class="input-info">
                            <input type="text" class="cgst_percent form-control w-50p" value="<?php echo $scquotation_model->cgst_percent; ?>" data-qid = "<?php echo $scquotation_model->scquotation_id; ?>"/>
                            <span class="cgst_amount value-label">
                                <?php                             
                                $cgst_amount = ($scquotation_model->cgst_percent / 100) * $amount;
                                echo $cgst_amount;
                                ?>
                            </span>
                        </div>
                            
                    </div>
                    <div class="form-group no-col">
                        <label>IGST(%):</label>
                        <div class="input-info">
                            <input type="text" class="igst_percent form-control w-50p" value="<?php echo $scquotation_model->igst_percent; ?>" data-qid = "<?php echo $scquotation_model->scquotation_id; ?>"/>
                            <div class="igst_amount value-label">
                                <?php                             
                                $igst_amount = ($scquotation_model->igst_percent / 100) * $amount;
                                echo $igst_amount;
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group no-col">
                        <label>Total Tax</label>
                        <input type="text" class="form-control" value="<?php echo Controller::money_format_inr(($sgst_amount+$cgst_amount+$igst_amount), 2, 1);   ?>" readonly/> 
                                             
                    </div>                    
                </form>
            </div>
        </div>        
    </div>
    <br>
     