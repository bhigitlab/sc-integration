<?php

class UnitConversionController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout = '//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array(
				'allow',  // allow all users to perform 'index' and 'view' actions
				'actions' => array('index', 'view'),
				'users' => array('*'),
			),
			array(
				'allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions' => array('create', 'update', 'createunitconvertion', 'admin', 'prioritychange'),
				'users' => array('@'),
			),
			array(
				'allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions' => array('admin', 'delete'),
				'users' => array('admin'),
			),
			array(
				'deny',  // deny all users
				'users' => array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view', array(
			'model' => $this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model = new UnitConversion;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if (isset($_POST['UnitConversion'])) {
			$model->attributes = $_POST['UnitConversion'];
			if ($model->save())
				$this->redirect(array('view', 'id' => $model->id));
		}

		$this->render('create', array(
			'model' => $model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model = $this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if (isset($_POST['UnitConversion'])) {
			$model->attributes = $_POST['UnitConversion'];
			if ($model->save())
				$this->redirect(array('view', 'id' => $model->id));
		}

		$this->render('update', array(
			'model' => $model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if (!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider = new CActiveDataProvider('UnitConversion');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model = new UnitConversion('search');
		$model->unsetAttributes();  // clear any default values
		if (isset($_GET['UnitConversion']))
			$model->attributes = $_GET['UnitConversion'];

		$this->render('admin', array(
			'model' => $model,
		));
	}
	public function actioncreateunitconvertion()
	{
		$model = new UnitConversion;
		$model2 = new Brand();
		$model3 = new PurchaseCategory();
		$model4 = new Unit();
		$base_conversion_factor = 1;


		$tblpx = Yii::app()->db->tablePrefix;
		if (isset($_POST['UnitConversion']) && isset($_POST['Unit'])) {
			if ($_POST['UnitConversion']['unit_convertion_id'] == '' || $_POST['UnitConversion']['unit_convertion_id'] == 0) {


				$item_id	=	$_POST['UnitConversion']['item_ids'];
				$model->item_id = $_POST['UnitConversion']['item_ids'];
				$model->base_unit = $_POST['UnitConversion']['base_unit_id'];
				$model->conversion_factor = $_POST['UnitConversion']['conversion_factor'];
				$model->conversion_unit = $_POST['Unit']['unit_name'];
				$model->created_date = date('Y-m-d H:i:s');

				$itemexist_unitconvertion = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}unit_conversion WHERE item_id=" . $item_id)->queryRow();


				if (isset($itemexist_unitconvertion) && $itemexist_unitconvertion == '') {

					$model->priority = '1';
					$addbaseunitSql =  "INSERT INTO {$tblpx}unit_conversion(item_id,base_unit,conversion_factor,conversion_unit)
										VALUES (:item_id, :base_unit,:conversion_factor,:conversion_unit)";
					$addbaseunitparameters = array(
						":item_id" => $_POST['UnitConversion']['item_ids'], ":base_unit" => $_POST['UnitConversion']['base_unit_id'],
						":conversion_factor" => $base_conversion_factor, ":conversion_unit" => $_POST['UnitConversion']['base_unit_id']
					);
					$addbaseunit = Yii::app()->db->createCommand($addbaseunitSql)->execute($addbaseunitparameters);
				} else {

					$model->priority = '0';
				}
				$uc_duplicateEntry_count  = UnitConversion::model()->findAllByAttributes(array(
					'item_id' => $item_id,
					'base_unit' => $_POST['UnitConversion']['base_unit_id'],
					'conversion_unit' => $_POST['Unit']['unit_name'],
				));
				if (count($uc_duplicateEntry_count) == 0) {
					if ($model->save()) {


						Yii::app()->user->setFlash('success', "Unit Conversion added Successfully");
						$this->redirect(array('purchaseCategory/newList'));
					}
				} else {
					Yii::app()->user->setFlash('error', "Same Conversion Unit is already exist for this Item.");
				}
			} else {
				// die("2");
				$id = $_POST['UnitConversion']['unit_convertion_id'];
				$model = $this->loadModel($id);
				$this->performAjaxValidation($model);
				$model->item_id = $_POST['UnitConversion']['item_ids'];
				$model->base_unit = $_POST['UnitConversion']['base_unit_id'];
				$model->conversion_factor = $_POST['UnitConversion']['conversion_factor'];
				$model->conversion_unit = $_POST['Unit']['unit_name'];
				$uc_duplicateEntry_count  = UnitConversion::model()->findAllByAttributes(array(
					'item_id' => $_POST['UnitConversion']['item_ids'],
					'base_unit' => $_POST['UnitConversion']['base_unit_id'],
					'conversion_factor' => "'" . $_POST['UnitConversion']['conversion_factor'] . "'",
					'conversion_unit' => $_POST['Unit']['unit_name'],
				));
				if (count($uc_duplicateEntry_count) <= 1) {

					if ($model->save()) {
						Yii::app()->user->setFlash('success', "Updated Successfully");
						$this->redirect(array('purchaseCategory/newList'));
					}
				} else {
					Yii::app()->user->setFlash('error', "Same Conversion Unit is already exist for this Item.");
				}
			}
		}
		$this->redirect(array('purchaseCategory/newList'));
	}
	public function actionprioritychange()
	{
		$tblpx = Yii::app()->db->tablePrefix;
		$old_itemcheck_unitpriority_id = 0;
		$newpriority	= '';
		$oldpriority	= '';
		if (isset($_POST['priority_check_uc_id']) && isset($_POST['priority_check_uc_item_id']) && isset($_POST['priority_check_status'])) {
			$uc_id 						= $_POST['priority_check_uc_id'];
			$priority_check_result["new_item_unitprioritycheck_id"]  = $uc_id;
			$uc_item_id				 	= $_POST['priority_check_uc_item_id'];
			$priority_check_status		= $_POST['priority_check_status'];
			$model = $this->loadModel($uc_id);
			if ($priority_check_status == 'true') {
				$itemcheck_unitconvertion 	= Yii::app()->db->createCommand("SELECT * FROM {$tblpx}unit_conversion WHERE `priority`='1' AND item_id=" . $uc_item_id)->queryRow();
				$old_itemcheck_unitpriority_id	= $itemcheck_unitconvertion['id'];
				if ($old_itemcheck_unitpriority_id != 0) {
					$model2 = $this->loadModel($old_itemcheck_unitpriority_id);
					$priority_check_result["old_item_unitpriorityuncheck_id"]  = $old_itemcheck_unitpriority_id;
					$model->priority = '1';
					$newpriority	= '1';
					if ($model->save()) {
						$model2->priority = '0';
						$oldpriority	= '0';
						$model2->save();
					}
				}
			} else {
				$itemcheck_unitconvertion 	= Yii::app()->db->createCommand("SELECT * FROM {$tblpx}unit_conversion WHERE base_unit = conversion_unit AND item_id=" . $uc_item_id)->queryRow();
				$priority_check_result["old_item_unitpriorityuncheck_id"]  = $uc_id;
				$uc_id_new	=	$itemcheck_unitconvertion['id'];
				if ($uc_id != $uc_id_new) {
					$priority_check_result["new_item_unitprioritycheck_id"]  = $uc_id_new;
					$model3 = $this->loadModel($uc_id_new);
					$model3->priority = '1';
					$newpriority	= '1';
					if ($model3->save()) {
						$model->priority = '0';
						$oldpriority	= '0';
						$model->save();
					}
				} else {
					$newpriority = 1;
					$oldpriority = 0;
				}
			}
			$priority_check_result["priority_check_status"]  = $priority_check_status;
			$priority_check_result["newpriority"]  = $newpriority;
			$priority_check_result["oldpriority"]  = $oldpriority;
		}
		//$this->redirect(array('purchaseCategory/newList'));

		$prioritycheck_result           = json_encode($priority_check_result);
		echo $prioritycheck_result;
	}
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return UnitConversion the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model = UnitConversion::model()->findByPk($id);
		if ($model === null)
			throw new CHttpException(404, 'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param UnitConversion $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'unit-conversion-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
