

<?php
/* @var $this AlbumsController */
/* @var $model Albums */
/* @var $form CActiveForm */
?>


<h5>Filter By :</h5>
<?php
$form = $this->beginWidget('CActiveForm', array(
    'action' => Yii::app()->createUrl($this->route),
    'method' => 'get',
        ));
?>
<div class="filter">
    <div class="row ">

		<div class="col-md-2 col-sm-5 col-xs-5">
		<?php echo $form->label($model,'title'); ?>
		<?php echo $form->textField($model,'title',array('size'=>20,'maxlength'=>20)); ?>
		</div>
        <div class="col-md-2 col-sm-5 col-xs-5">					
            <?php echo $form->label($model, 'projectid'); ?>
            <?php
            echo $form->dropDownList($model, 'projectid', CHtml::listData(Projects::model()->findAll(array('order'=>'name ASC')), 'pid', 'name'), array('empty' => '-----------', 'id' => 'projectid'));
            ?>
        </div>
        <div class="col-md-2 col-sm-3 col-xs-3">	

				<label>&nbsp;</label>
				<div class= "text-left">
                <?php echo CHtml::submitButton('Go'); ?>
                <?php echo CHtml::resetButton('Clear', array('onclick' => 'javascript:location.href="' . $this->createUrl('admin') . '"')); ?>
            </div>
        </div>

    </div>

</div>
<?php $this->endWidget(); ?>
