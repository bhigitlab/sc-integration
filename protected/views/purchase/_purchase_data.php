<tr>
    <?php
    $class = "";
    $style = "";
    $item_data =Yii::app()->db->createCommand("SELECT permission_status FROM jp_purchase_items WHERE item_id=" . $last_id)->queryRow();
    if ($item_data['permission_status'] == 'not_approved') {
        $class = "rate_highlight";
        $style = "style='cursor:pointer'";
    } else {
        $class = "";
        $style = "";
    }
    ?> 
    <td>
        <div id="item_sl_no"><?php echo ($data['sl_no'] + 1) ?></div>
    </td>
    <td>
        <div class="item_description" id="<?php echo $spc_id ?>"><?php echo $descriptions ?></div>
    </td>
    <td>
        <div class="item_hsn"><?php echo $data['hsn_code']; ?></div>
    </td>
    <?php $approval_type =Yii::app()->db->createCommand("SELECT estimation_approval_type FROM jp_purchase_items WHERE item_id=" . $last_id)->queryRow();
				?>
    <td class="text-right <?php echo (isset($approval_type['estimation_approval_type']) && in_array($approval_type['estimation_approval_type'], [2, 3]))?$class:''; ?>" id="<?php echo  $last_id ?>" >
        <div class="" id="quantity"><?php echo $data['quantity']; ?></div>
    </td>
    <td>
        <div class="unit" id="unit"> <?php echo $data['unit']; ?></div>
    </td>
    <td class="text-right <?php echo (isset($approval_type['estimation_approval_type']) && in_array($approval_type['estimation_approval_type'], [2]))?'':$class; ?>" id="<?php echo  $last_id ?>" <?php echo $style ?> >
        <div class="" id="rate"><?php echo  Yii::app()->Controller->money_format_inr($data['rate'], 2) ?></div>
    </td>


    <td class="text-right">
        <div class="" id="base_qty"><?php echo  $data['base_qty']; ?></div>
    </td>
    <td>
        <div class="unit" id="base_unit"> <?php echo  $data['base_unit']; ?></div>
    </td>
    <td class="text-right <?php $class . $style ?>" id="<?php echo $last_id ?>">
        <div class="" id=" base_rate"><?php echo  number_format(floatval($data['base_rate']), 2, '.', ''); ?></div>
    </td>

    <td class="text-right">
        <div class="" id="amount"> <?php echo Yii::app()->Controller->money_format_inr($data['amount'], 2) ?></div>
    </td>
    <td class="text-right">
        <div class=""> <?php echo $data['tax_slab'] ?></div>
    </td>
    <td class="text-right">
        <div class=""><?php echo Yii::app()->Controller->money_format_inr($imodel['sgst_amount'], 2) ?></div>
    </td>
    <td class="text-right">
        <div class=""> <?php echo Yii::app()->Controller->money_format_inr($imodel['sgst_percentage'], 2) ?></div>
    </td>
    <td class="text-right">
        <div class=""><?php Yii::app()->Controller->money_format_inr($imodel['cgst_amount']) ?></div>
    </td>
    <td class="text-right">
        <div class=""><?php echo number_format($imodel['cgst_percentage'], 2, '.', '') ?></div>
    </td>
    <td class="text-right">
        <div class=""><?php echo number_format($imodel['igst_amount'], 2) ?></div>
    </td>
    <td class="text-right">
        <div class=""><?php echo number_format($imodel['igst_percentage'], 2, '.', '') ?></div>
    </td>
    <td class="text-right">
        <div class=""><?php echo number_format($imodel['discount_percentage'], 2, '.', '') ?></div>
    </td>
    <td class="text-right">
        <div class=""><?php echo number_format($imodel['discount_amount'], 2) ?></div>
    </td>

    <td class="text-right">
        <div class=""> <?php echo number_format($data['tax_amount'], 2, '.', '') ?></div>
    </td>
    <td class="text-right">
        <div class=""> <?php echo Yii::app()->Controller->money_format_inr((($data['tax_amount'] + $data['amount']) - $data['discount_amount']), 2) ?></div>
    </td>
    <td width="70"><span class="icon icon-options-vertical popover-test" data-toggle="popover" data-placement="left" type="button" data-html="true" style="cursor: pointer;"></span>
        <div class="popover-content hide">
            <ul class="tooltip-hiden">
                <li><a href="#" id='<?php echo $last_id ?>' class="btn btn-default btn-xs removebtn">Delete</a></li>
                <li><a href="" id='<?php echo  $last_id ?>' class="btn btn-default btn-xs edit_item" style="margin-left:3px">Edit</a></li>
                <?php if ($permission_status == 2 && Yii::app()->user->role == 1) { ?>
                    <li><a href="" id='<?php echo $last_id ?>' class="btn btn-default btn-xs approve_item approveoption_<?php echo  $last_id ?>" style="margin-left:3px">
                    <?php
						$approval_type_value = isset($approval_type['estimation_approval_type']) ? $approval_type['estimation_approval_type'] : null;
						if ($approval_type_value == 2) {
					    	echo 'Quantity Approve';
						} elseif ($approval_type_value == 3) {
							echo 'Rate & Quantity Approve';
						} else{
						    echo 'Rate Approve';
						}
					?>
                    </a></li>
                <?php } ?>
            </ul>
        </div>
    </td>
</tr>
<script>
    function initializePopovers() {
    $(".popover-test").popover({
        html: true,
        content: function() {
            return $(this).next('.popover-content').html();
        }
    });
}

// Reinitialize popovers when a new item is added or updated
$(document).on('ajaxComplete', function() {
    initializePopovers();
});

// Close other popovers when one is clicked
$(document).on('click', '[data-toggle=popover]', function(e) {
    $('[data-toggle=popover]').not(this).popover('hide');
});

// Ensure popovers close on clicking outside
$(document).on('click', 'body', function(e) {
    $('[data-toggle=popover]').each(function() {
        if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
            $(this).popover('hide');
        }
    });
});

// Initial call on document ready
$(document).ready(function() {
    initializePopovers();
});

    $(document).ready(function() {
    $(".popover-test").popover({
        html: true,
        content: function() {
            return $(this).next('.popover-content').html();
        }
    });

    // Close other popovers when one is clicked
    $('[data-toggle=popover]').on('click', function (e) {
        $('[data-toggle=popover]').not(this).popover('hide');
    });

    // Ensure popovers close on click outside
    $('body').on('click', function (e) {
        $('[data-toggle=popover]').each(function () {
            if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                $(this).popover('hide');
            }
        });
    });
});
$(document).off('click', '.removebtn').on('click', '.removebtn', function (e) {
        e.preventDefault();
        element = $(this);
        
        var item_id = $(this).attr('id');
        if (item_id == '0') {
            $('input[name="remove"]').val('removed');
            $(this).closest('tr').find('input[name="amount[]"]').each(function() {
                $(this).closest('tr').remove();
                var re = this.value;
                var v1 = $('input[name="subtot"]').val();
                var v2 = $('input[name="grand"]').val();
                if (re == '') {
                    $('input[name="subtot"]').val(v1);
                    $('input[name="grand"]').val(v2);
                    $('#grand_total').text(v2.toFixed(2));
                } else {
                    $('input[name="subtot"]').val(eval(v1) - eval(re));
                    $('input[name="grand"]').val(eval(v2) - eval(re));
                    $('#grand_total').text((eval(v2) - eval(re)).toFixed(2));
                }
            });
        } else {
            var answer = confirm("Are you sure you want to delete?");
            if (answer) {
                var item_id = $(this).attr('id');
                var $tds = $(this).closest('tr').find('td');
                var amount = $tds.eq(15).text();
                var re = amount;
                var j = parseFloat(amount)
                var v1 = $('input[name="subtot"]').val();
                var v2 = $('input[name="grand"]').val();
                var t = $('#grand_total').text();
                var gamount = parseFloat(t);
                if (j == '') {
                    $('input[name="subtot"]').val(gamount);
                    $('input[name="grand"]').val(gamount);
                    $('#grand_total').text(gamount);
                } else {
                    $('input[name="subtot"]').val((eval(gamount) - eval(j)).toFixed(2));
                    $('input[name="grand"]').val((eval(gamount) - eval(j)).toFixed(2));
                    $('#grand_total').text((eval(gamount) - eval(j)).toFixed(2));
                }
                var purchase_id = $("#purchase_id").val();
                var subtot = $('input[name="subtot"]').val();
                var grand = $('input[name="grand"]').val();
                var data = {
                    'purchase_id': purchase_id,
                    'item_id': item_id,
                    'grand': grand,
                    'subtot': subtot
                };
                $('.loading-overlay').addClass('is-active');
                $.ajax({
                    method: "GET",
                    async: false,
                    data: {
                        data: data
                    },
                    dataType: "json",
                    url: '<?php echo Yii::app()->createUrl('purchase/removepurchaseitem'); ?>',
                    success: function(result) {
                        if (result.response == 'success') {
                            $('.addrow').html(result.html);
                            $('#final_amount').val(result.grand_total);
                            $('#amount_total').text(result.grand_total);
                            $('#discount_total').text(result.discount_total);
                            $('#tax_total').text(result.tax_total);
                            $('#grand_total').text(result.grand_total_amount);
                            $(".toastmessage").addClass("alert alert-success").html(""+result.msg+"").fadeIn().delay(4000).fadeOut();
                            // $().toastmessage('showSuccessToast', "" + result.msg + "");
                            if (result.mail_send == 'Y') {
                                $('.mail_send').show();
                            } else {
                                $('.mail_send').hide();
                            }
                        } else {
                            $(".toastmessage").addClass("alert alert-danger").html(""+result.msg+"").fadeIn().delay(4000).fadeOut();
                            // $().toastmessage('showErrorToast', "" + result.msg + "");
                        }
                        $('#description').val('').trigger('change');
                        $('#remarks').val('');
                        var quantity = $('#quantity').val('');
                        var unit = $('#item_unit').text('');
                        var rate = $('#rate').val('');
                        var amount = $('#item_amount').html('');
                        $('#hsn_code').text('');
                        $('#total_amount').text('');
                        $(".item_save").attr('value', 'Save');
                        $(".item_save").attr('id', 0);
                        $('#description').select2('focus');
                        $('#previousvalue').text('0');
                    }
                });
            } else {
                return false;
            }
        }
    });
    $(document).on("click", ".addcolumn, .removebtn", function() {
        $("table.table  input[name='sl_No[]']").each(function(index, element) {
            $(element).val(index + 1);
            $('.sl_No').html(index + 1);
        });
    });

    </script>