CREATE TABLE `jp_equipments` (`id` INT(11) NOT NULL AUTO_INCREMENT , `equipment_name` VARCHAR(255) NOT NULL , `equipment_unit` VARCHAR(255) NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;

INSERT INTO `jp_menu` (`menu_id`, `menu_name`, `parent_id`, `status`, `controller`, `action`, `params`, `showmenu`, `show_list`, `related`) VALUES ('826', 'delete', '822', '0', 'equipments', 'delete', '', '2', '1', NULL), ('825', 'update', '822', '0', 'equipments', 'update', '', '2', '1', NULL), ('824', 'create', '822', '0', 'equipments', 'create', '', '2', '1', NULL), ('823', 'Listing', '822', '0', 'equipments', 'index', '', '2', '1', NULL), ('822', 'Equipments', '0', '0', 'equipments', '', '', '0', '1', NULL);

CREATE TABLE `jp_materials` (`id` INT(11) NOT NULL AUTO_INCREMENT , `material_name` VARCHAR(255) NOT NULL , `material_unit` VARCHAR(255) NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;

INSERT INTO `jp_menu` (`menu_id`, `menu_name`, `parent_id`, `status`, `controller`, `action`, `params`, `showmenu`, `show_list`, `related`) VALUES ('831', 'delete', '827', '0', 'materials', 'delete', '', '2', '1', NULL), ('830', 'update', '827', '0', 'materials', 'update', '', '2', '1', NULL), ('829', 'create', '827', '0', 'materials', 'create', '', '2', '1', NULL), ('828', 'Listing', '827', '0', 'materials', 'index', '', '2', '1', NULL), ('827', 'Materials', '0', '0', 'materials', '', '', '0', '1', NULL);

CREATE TABLE `jp_material_entries` (`id` INT NOT NULL AUTO_INCREMENT , `project_id` INT(11) NULL , `company_id` INT(11) NULL , `subcontractor_id` INT(11) NULL , `expensehead_id` INT(11) NULL , `description` VARCHAR(255) NULL , `status` BOOLEAN NULL , `Date` DATE NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;
ALTER TABLE `jp_material_entries`
  ADD KEY `INDEXsubcontract` (`subcontractor_id`),
  ADD KEY `INDEXproject` (`project_id`);

ALTER TABLE `jp_material_entries` ADD CONSTRAINT `fk_subcontractor_id` FOREIGN KEY (`subcontractor_id`) REFERENCES `jp_subcontractor`(`subcontractor_id`) ON DELETE RESTRICT ON UPDATE CASCADE; ALTER TABLE `jp_material_entries` ADD CONSTRAINT `fk_project_id` FOREIGN KEY (`project_id`) REFERENCES `jp_projects`(`pid`) ON DELETE RESTRICT ON UPDATE CASCADE;

CREATE TABLE `jp_material_items` (`id` INT NOT NULL AUTO_INCREMENT , `material_entry_id` INT(11) NOT NULL , `material` INT(11) NOT NULL , `quantity` INT(11) NOT NULL , `unit` VARCHAR(255) NOT NULL , PRIMARY KEY (`id`), INDEX `INDEXmaterialentryid` (`material_entry_id`), INDEX `INDEXmaterial` (`material`)) ENGINE = InnoDB;

ALTER TABLE `jp_material_items` ADD CONSTRAINT `fk_material_entry_id` FOREIGN KEY (`material_entry_id`) REFERENCES `jp_material_entries`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE; ALTER TABLE `jp_material_items` ADD CONSTRAINT `fk_material_id` FOREIGN KEY (`material`) REFERENCES `jp_materials`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

INSERT INTO `jp_menu` (`menu_id`, `menu_name`, `parent_id`, `status`, `controller`, `action`, `params`, `showmenu`, `show_list`, `related`) VALUES ('833', 'Material Entries', '0', '0', 'materialEntries', '', '', '0', '1', NULL);

INSERT INTO `jp_menu` (`menu_id`, `menu_name`, `parent_id`, `status`, `controller`, `action`, `params`, `showmenu`, `show_list`, `related`) VALUES ('834', 'Material Entries', '3', '0', 'materialEntries', 'create', '', '2', '1', NULL);

CREATE TABLE `jp_equipments_entries` (`id` INT NOT NULL AUTO_INCREMENT , `project_id` INT(11) NULL , `company_id` INT(11) NULL , `subcontractor_id` INT(11) NULL , `expensehead_id` INT(11) NULL , `description` VARCHAR(255) NULL , `status` BOOLEAN NULL , `Date` DATE NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;

ALTER TABLE `jp_equipments_entries` ADD KEY `INDEXsubcontract` (`subcontractor_id`), ADD KEY `INDEXproject` (`project_id`);

ALTER TABLE `jp_equipments_entries` ADD CONSTRAINT `fk_sub_contractor_id` FOREIGN KEY (`subcontractor_id`) REFERENCES `jp_subcontractor`(`subcontractor_id`) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `jp_equipments_entries` ADD CONSTRAINT `fk_project_id_1` FOREIGN KEY (`project_id`) REFERENCES `jp_projects`(`pid`) ON DELETE RESTRICT ON UPDATE CASCADE;

CREATE TABLE `jp_equipments_items` (`id` INT NOT NULL AUTO_INCREMENT , `equipment_entry_id` INT(11) NOT NULL , `equipments` INT(11) NOT NULL , `quantity` INT(11) NOT NULL , `unit` VARCHAR(255) NOT NULL , PRIMARY KEY (`id`), INDEX `INDEXequipmententryid` (`equipment_entry_id`), INDEX `INDEXequipments` (`equipments`)) ENGINE = InnoDB;

ALTER TABLE `jp_equipments_items` ADD CONSTRAINT `fk_equipment_entry_id` FOREIGN KEY (`equipment_entry_id`) REFERENCES `jp_equipments_entries`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `jp_equipments_items` ADD CONSTRAINT `fk_equipment_id` FOREIGN KEY (`equipments`) REFERENCES `jp_equipments`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

INSERT INTO `jp_menu` (`menu_id`, `menu_name`, `parent_id`, `status`, `controller`, `action`, `params`, `showmenu`, `show_list`, `related`) VALUES ('835', 'Equipments Entries', '0', '0', 'equipmentsEntries', '', '', '0', '1', NULL);

INSERT INTO `jp_menu` (`menu_id`, `menu_name`, `parent_id`, `status`, `controller`, `action`, `params`, `showmenu`, `show_list`, `related`) VALUES ('836', 'Equipments Entries', '3', '0', 'equipmentsEntries', 'create', '', '2', '1', NULL);

CREATE TABLE `jp_material_estimation` (`id` INT NOT NULL AUTO_INCREMENT , `project_id` INT NOT NULL , `specification` VARCHAR(255) NULL DEFAULT NULL , `quantity` VARCHAR(255) NULL DEFAULT NULL , `unit` VARCHAR(255) NULL DEFAULT NULL , `rate` FLOAT NULL DEFAULT NULL , `amount` FLOAT NULL DEFAULT NULL , `remarks` VARCHAR(255) NULL DEFAULT NULL , `created_at` DATE NULL DEFAULT NULL , PRIMARY KEY (`id`), INDEX `projectIndex` (`project_id`)) ENGINE = InnoDB;

ALTER TABLE jp_material_estimation
ADD COLUMN `material` VARCHAR(255) NULL DEFAULT NULL AFTER `project_id`;

ALTER TABLE `jp_material_estimation` ADD CONSTRAINT `fk_project_id_2` FOREIGN KEY (`project_id`) REFERENCES `jp_projects`(`pid`) ON DELETE RESTRICT ON UPDATE CASCADE;

CREATE TABLE `jp_labour_estimation` (`id` INT NOT NULL AUTO_INCREMENT , `project_id` INT NOT NULL , `labour` VARCHAR(255) NULL , `quantity` INT NULL , `unit` VARCHAR(255) NULL , `rate` FLOAT NULL , `amount` FLOAT NULL , `remark` VARCHAR(255) NULL , `created_at` DATE NULL , PRIMARY KEY (`id`), INDEX `INDEXprojectid` (`project_id`)) ENGINE = InnoDB;

ALTER TABLE `jp_labour_estimation` ADD CONSTRAINT `fk_project_id_3` FOREIGN KEY (`project_id`) REFERENCES `jp_projects`(`pid`) ON DELETE RESTRICT ON UPDATE CASCADE;

CREATE TABLE `jp_equipments_estimation` (`id` INT NOT NULL AUTO_INCREMENT , `project_id` INT NOT NULL , `equipment` VARCHAR(255) NULL , `quantity` INT NULL , `unit` VARCHAR(255) NULL , `rate` FLOAT NULL , `amount` FLOAT NULL , `remark` VARCHAR(255) NULL , PRIMARY KEY (`id`), INDEX `INDEXprojectid1` (`project_id`)) ENGINE = InnoDB;

ALTER TABLE `jp_equipments_estimation` ADD CONSTRAINT `fk_project_id_4` FOREIGN KEY (`project_id`) REFERENCES `jp_projects`(`pid`) ON DELETE RESTRICT ON UPDATE CASCADE;

INSERT INTO `jp_menu` (`menu_id`, `menu_name`, `parent_id`, `status`, `controller`, `action`, `params`, `showmenu`, `show_list`, `related`) VALUES ('837', 'Matereial Estimation', '0', '0', 'MaterialEstimation', '', '', '0', '1', NULL);

INSERT INTO `jp_menu` (`menu_id`, `menu_name`, `parent_id`, `status`, `controller`, `action`, `params`, `showmenu`, `show_list`, `related`) VALUES ('838', 'Material Estimation', '3', '0', 'MaterialEstimation', 'index', '', '2', '1', NULL);

ALTER TABLE jp_equipments_estimation CHANGE remark remarks VARCHAR(255);
ALTER TABLE jp_labour_estimation CHANGE remark remarks VARCHAR(255);


ALTER TABLE jp_itemestimation
MODIFY COLUMN `itemestimation_quantity` INT NULL;

ALTER TABLE jp_itemestimation MODIFY COLUMN `category_id` INT NULL;

ALTER TABLE `jp_itemestimation` CHANGE `itemestimation_status` `itemestimation_status` INT(11) NOT NULL COMMENT 'deafult=1 ,approve=2,quantity change=3';