<?php
/* @var $this ExpensesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'Transactions',
);
?>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/tableHeadFixer.js"></script>

<div class="container" id="expense">
    <div class="expenses-heading header-container">
        <h3>Day Book Transactions</h3>
        <div class="btn-container secdiv">
            <button type="button" class="btn btn-info">
                <a href="<?php echo Yii::app()->createUrl('expenses/savetoexcel1', array('user_id' => $model->userid, 'projectid' => $model->projectid, 'type' => $model->type, 'fromdate' => (isset($_REQUEST['Expenses']['fromdate'])) ? $_REQUEST['Expenses']['fromdate'] : '', 'todate' => (isset($_REQUEST['Expenses']['todate'])) ? $_REQUEST['Expenses']['todate'] : '', 'company_id' => $company_id, 'vendor_id' => $model->vendor_id, 'purchase_type' => $model->purchase_type, 'expense_type' => $model->expense_type, 'subcontractor_id' => $model->subcontractor_id)); ?>"
                    style="text-decoration: none; color: white;" title="SAVE AS EXCEL">
                    <i class="fa fa-file-excel-o button-icon" aria-hidden="true"></i>
                </a>
            </button>
            <button type="button" class="btn btn-info">
                <a href="<?php echo Yii::app()->createUrl('expenses/savetopdf1', array('user_id' => $model->userid, 'projectid' => $model->projectid, 'type' => $model->type, 'fromdate' => (isset($_REQUEST['Expenses']['fromdate'])) ? $_REQUEST['Expenses']['fromdate'] : '', 'todate' => (isset($_REQUEST['Expenses']['todate'])) ? $_REQUEST['Expenses']['todate'] : '', 'company_id' => $company_id, 'vendor_id' => $model->vendor_id, 'purchase_type' => $model->purchase_type, 'expense_type' => $model->expense_type, 'subcontractor_id' => $model->subcontractor_id)); ?>"
                    style="text-decoration: none; color: white;" title="SAVE AS EXCEL">
                    <i class="fa fa-file-pdf-o button-icon" aria-hidden="true"></i>
                </a>
            </button>
            <button type="button" class="btn btn-info">
                <a href="<?php echo Yii::app()->createUrl('expenses/newlist&ledger=1', array('fromdate' => $fromdate, 'todate' => $todate)); ?>"
                    style="text-decoration: none; color: white;">
                    Ledger View
                </a>
            </button>
        </div>
    </div>

    <div class="search-form">
        <?php $this->renderPartial('_newsearch', array('model' => $model, 'company_id' => $company_id, 'fromdate' => $fromdate, 'todate' => $todate)) ?>
    </div>
    <div class="modal fade edit" role="dialog">
        <div class="modal-dialog modal-lg">
        </div>
    </div>

    <div>
        <?php
        $this->widget('zii.widgets.CListView', array(
            'dataProvider' => $dataProvider,
            'itemView' => '_newview1',
            'template' => '<div>{summary}{sorter}</div><div id="parent"><table cellpadding="10" class="table  list-view sorter" id="fixTable">{items}</table></div>{pager}',
            'emptyText' => '<br><div class="report-table-wrapper"><table cellpadding="10" class="table total-table"><thead class="entry-table">
                <th>Sl No.</th>
                <th>Date</th>
                <th>Project Name</th>
                <th>Vendor</th>
                <th>Subcontractor</th>
                <th>Description</th>
                <th>Transaction Type</th>
                <th>Purchase Type</th>
                <th>Credit Amount</th>
                <th>Debit Amount</th>
                <th>Paid Amount</th>
                </thead>
                <tr><td colspan="11" class="text-center">No results found</td></tr></table></div>',
            'viewData' => array(
                'grantotal' => $grandtotal,
                'granddebitamount' => $granddebitamount,
            ),
        ));
        ?>
    </div>
</div>

<style>
    .savepdf {
        background-color: #6a8ec7;
        border: 1px solid #6a8ec8;
        color: #fff;
        padding: 5px;
    }

    #parent {
        max-height: 500px;
    }

    table thead th {
        background-color: #eee;
    }

    .table {
        margin-bottom: 0px;
    }

    .table>thead>tr>th,
    .table>tbody>tr>th,
    .table>tfoot>tr>th,
    .table>thead>tr>td,
    .table>tbody>tr>td,
    .table>tfoot>tr>td {
        border-bottom: 0px;
    }
</style>

<script>
    $(document).on('click', '.editDaybook', function () {
        $('.loading-overlay').removeClass('is-active');
        var id = $(this).attr('data-id');
        $.ajax({
            type: "GET",
            url: "<?php echo $this->createUrl('expenses/updatedaybook&id=') ?>" + id,
            success: function (response) {
                $(".edit").html(response);
            }
        });
    });

    $(document).on('click', '.deletedaybook', function () {
        var id = $(this).attr('data-id');
        $('.loading-overlay').addClass('is-active');
        if (confirm("Are you sure?")) {
            $.ajax({
                type: "POST",
                dataType: "json",
                url: "<?php echo $this->createUrl('expenses/deletedaybookentry&id=') ?>" + id,
                success: function (response) {
                    if (response.response == 'success') {
                        alert('Deleted Successfully');
                        location.reload();
                    } else {
                        alert("Error Occured.. Please Try again! ");
                    }
                }
            });
        } else {
            return false;
        }
    });


    $(function () {
        $("#parent").css('max-height', $(window).height() - $("#myNavbar").height() - $(".page_filter").height() + $(".summary").height() - (2 * $('footer').height()));
        $("#fixTable").tableHeadFixer({
            'left': false,
            'foot': true,
            'head': true
        });
    });
    $(document).ajaxComplete(function () {
        $('.loading-overlay').removeClass('is-active');
        $('#loading').hide();
    });
    $('.bill_view').click(function () {
        var id = $(this).attr('id');
        url = "<?php echo Yii::app()->createUrl('bills/view&id=') ?>" + id;
        window.location.replace(url);
    });
    $('.return_view').click(function () {
        var id = $(this).attr('id');
        url = "<?php echo Yii::app()->createUrl('PurchaseReturn/view&id=') ?>" + id;
        window.location.replace(url);
    });
</script>