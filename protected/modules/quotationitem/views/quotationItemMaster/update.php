<?php
/* @var $this QuotationItemMasterController */
/* @var $model QuotationItemMaster */

$this->breadcrumbs=array(
	'Quotation Item Masters'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List QuotationItemMaster', 'url'=>array('index')),
	array('label'=>'Create QuotationItemMaster', 'url'=>array('create')),
	array('label'=>'View QuotationItemMaster', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage QuotationItemMaster', 'url'=>array('admin')),
);
?>

<h1>Update QuotationItemMaster <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>