<?php

/**
 * This is the model class for table "{{warehouse}}".
 *
 * The followings are the available columns in table '{{warehouse}}':
 * @property integer $warehouse_id
 * @property string $warehouse_name
 * @property string $warehouse_place
 * @property string $warehouse_address
 * @property integer $project_id
 * @property integer $warehouse_incharge
 * @property string $status
 * @property string $created_date
 * @property integer $created_by
 * @property string $updated_date
 * @property integer $updated_by
 * @property integer $auto_debit
 */
class Warehouse extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{warehouse}}';
	}
	public $relation_to_project;
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('warehouse_name, warehouse_place, warehouse_address, warehouse_incharge, created_date, created_by, updated_date, updated_by, assigned_to', 'required'),
			array('project_id', 'required', 'on' => 'project_required'),
			array('auto_debit,warehouse_incharge, created_by, updated_by', 'numerical', 'integerOnly' => true),
			array('warehouse_name, warehouse_place', 'length', 'max' => 200),
			array('warehouse_name','unique'),
			array('status', 'length', 'max' => 1),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('warehouse_id,relation_to_project, warehouse_name, warehouse_place, warehouse_address, project_id, warehouse_incharge, status, created_date, created_by, updated_date, updated_by, assigned_to', 'safe', 'on' => 'search'),
			// array('project_id', 'checkproject_id'),

		);
	}
	public function checkproject_id()
	{

		if ($this->relation_to_project == '1') {
			if (empty($this->project_id)) {
				$this->addError("project_id", 'Project cannot be blank.');
			}
		}
	}
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'createdBy' => array(self::BELONGS_TO, 'Users', 'created_by'),
			'updatedBy' => array(self::BELONGS_TO, 'Users', 'updated_by'),
			'incharge' => array(self::BELONGS_TO, 'Users', 'warehouse_incharge'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'warehouse_id' => 'Warehouse',
			'warehouse_name' => 'Name',
			'warehouse_place' => 'Place',
			'warehouse_address' => 'Address',
			'project_id' => 'Project',
			'relation_to_project' => 'Relation To Project',
			'warehouse_incharge' => 'In-charge',
			'status' => 'Status',
			'created_date' => 'Created Date',
			'created_by' => 'Created By',
			'updated_date' => 'Updated Date',
			'updated_by' => 'Updated By',
			'assigned_to' => 'Assigned To',
			'auto_debit' => 'Auto Debit'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('warehouse_id', $this->warehouse_id);
		$criteria->compare('warehouse_name', $this->warehouse_name, true);
		$criteria->compare('warehouse_place', $this->warehouse_place, true);
		$criteria->compare('warehouse_address', $this->warehouse_address, true);
		$criteria->compare('project_id', $this->project_id);
		$criteria->compare('warehouse_incharge', $this->warehouse_incharge);
		$criteria->compare('status', $this->status, true);
		$criteria->compare('created_date', $this->created_date, true);
		$criteria->compare('created_by', $this->created_by);
		$criteria->compare('updated_date', $this->updated_date, true);
		$criteria->compare('updated_by', $this->updated_by);
		$criteria->compare('assigned_to', $this->assigned_to);
		$criteria->compare('auto_debit', $this->auto_debit);
		$criteria->order = 'warehouse_id DESC';

		$newQuery = "";
		if (Yii::app()->user->role != 1) {
			$newQuery = " FIND_IN_SET(" . Yii::app()->user->id . ", assigned_to)";
		}
		$criteria->addCondition($newQuery);
		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'pagination' => false,
		));
	}

	public function searchUnassigned()
    {
        $criteria = new CDbCriteria;

        // Add the condition for 'assigned_to' being NULL
        $criteria->addCondition('assigned_to IS NULL');

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
			'pagination' => false,
        ));
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Warehouse the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function warehouseStockCheck($id)
	{
		$criteria =  new CDbCriteria();
		$criteria->addCondition('warehousestock_warehouseid=:warehousestock_warehouseid');
		$criteria->params = array(':warehousestock_warehouseid' => $id);
		$stock_model = Warehousestock::model()->findAll($criteria);
		if (!empty($stock_model)) {
			return 2; //readonly
		} else {
			return 1; //can edit
		}
	}
	public function assignedUser($user_id)
	{
		$assigned_user_details = Users::model()->findByPk($user_id);
		if (!empty($assigned_user_details)) {
			$assigned_user = $assigned_user_details->first_name . ' ' . $assigned_user_details->last_name;
		} else {
			$assigned_user = '';
		}

		return $assigned_user;
	}
}
