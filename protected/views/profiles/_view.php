<?php
/* @var $this ProfilesController */
/* @var $data Profiles */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('pid')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->pid), array('view', 'id'=>$data->pid)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('profile_name')); ?>:</b>
	<?php echo CHtml::encode($data->profile_name); ?>
	<br />


</div>