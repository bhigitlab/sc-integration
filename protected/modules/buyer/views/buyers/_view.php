<?php
if ($index == 0) {
?>

	<thead>
		<tr>
			<th style="width:40px;">Sl No.</th>
			<th>Buyer</th>
			<th>Company</th>
			<th>Phone</th>
			<th>Email ID</th>
			<th>Client Type</th>
			<th>Project</th>
			<th>Status</th>
			<th>Address</th>
			<?php
			if (isset(Yii::app()->user->role) && (in_array('/buyer/buyers/update', Yii::app()->user->menuauthlist))) {
			?>
				<th style="width:40px;">Action</th>
			<?php } ?>
		</tr>
	</thead>
<?php
}
?>

<tr>
	<td style="width:40px;"><?php echo $index + 1; ?></td>
	<td><?php echo CHtml::encode($data->name); ?></td>
	<td>
		<?php
		$companyname = array();
		$arrVal = explode(',', $data->company);
		foreach ($arrVal as $arr) {
			$value = Company::model()->findByPk($arr);
			array_push($companyname, $value['name']);
		}
		echo implode(', ', $companyname);
		?>
	</td>
	<td><?php echo CHtml::encode($data->phone); ?></td>
	<td><?php echo CHtml::encode($data->email_id); ?></td>
	<td><?php echo CHtml::encode($data->buyerType['project_type']); ?></td>
	<td><?php echo CHtml::encode($data->project0['name']); ?></td>
	<td><?php echo CHtml::encode($data->status0['caption']); ?></td>
	<td><?php echo CHtml::encode($data->address); ?></td>
	<?php
	if (isset(Yii::app()->user->role) && (in_array('/buyer/buyers/update', Yii::app()->user->menuauthlist))) {
	?>
		<td style="width:40px;"><a class="fa fa-edit editClient" data-id="<?php echo $data->id; ?>" onclick="editclient(<?php echo $data->id; ?>)"></a></td>
	<?php } ?>
</tr>