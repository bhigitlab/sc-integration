<?php

/**
 * This is the model class for table "pms_acc_wpr_item_consumed".
 *
 * The followings are the available columns in table 'pms_acc_wpr_item_consumed':
 * @property integer $id
 * @property integer $warehouse_id
 * @property integer $item_id
 * @property double $item_rate
 * @property integer $item_qty
 * @property integer $status
 * @property integer $item_added_by
 * @property integer $item_approved_by
 * @property integer $wpr_id
 * @property integer $pms_project_id
 * @property integer $coms_project_id
 * @property integer $coms_material_id
 * @property integer $pms_material_id
 * @property integer $pms_template_material_id
 * @property integer $type_from
 * @property string $created_date
 * @property string $updated_date
 * @property string $pms_task_name
 */
class PmsAccWprItemConsumed extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pms_acc_wpr_item_consumed';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('item_qty, item_added_by, wpr_id', 'required'),
			array('warehouse_id, item_id, item_qty, status, item_added_by, item_approved_by, wpr_id, pms_project_id, coms_project_id, coms_material_id, pms_material_id, pms_template_material_id, type_from', 'numerical', 'integerOnly'=>true),
			array('item_rate', 'numerical'),
			array('pms_task_name', 'length', 'max'=>255),
			array('created_date, updated_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, warehouse_id, item_id, item_rate,item_amount, item_qty, status, item_added_by, item_approved_by, wpr_id, pms_project_id, coms_project_id, coms_material_id, pms_material_id, pms_template_material_id, type_from, created_date, updated_date, pms_task_name', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'warehouse_id' => 'Warehouse',
			'item_id' => 'Specification',
			'item_rate' => 'Rate',
			'item_qty' => 'Requested Quantity',
			'item_amount'=>'Amount',
			'status' => 'Status',
			'item_added_by' => 'Item Added By',
			'item_approved_by' => 'Item Approved By',
			'wpr_id' => 'Wpr',
			'pms_project_id' => 'Pms Project',
			'coms_project_id' => 'Coms Project',
			'coms_material_id' => 'Material',
			'pms_material_id' => 'Material',
			'pms_template_material_id' => 'Pms Template Material',
			'type_from' => 'Type From',
			'created_date' => 'Created Date',
			'updated_date' => 'Updated Date',
			'pms_task_name' => 'Pms Task Name',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('warehouse_id',$this->warehouse_id);
		$criteria->compare('item_id',$this->item_id);
		$criteria->compare('item_rate',$this->item_rate);
		$criteria->compare('item_qty',$this->item_qty);
		$criteria->compare('item_amount',$this->item_qty);
		$criteria->compare('status',$this->status);
		$criteria->compare('item_added_by',$this->item_added_by);
		$criteria->compare('item_approved_by',$this->item_approved_by);
		$criteria->compare('wpr_id',$this->wpr_id);
		$criteria->compare('pms_project_id',$this->pms_project_id);
		$criteria->compare('coms_project_id',$this->coms_project_id);
		$criteria->compare('coms_material_id',$this->coms_material_id);
		$criteria->compare('pms_material_id',$this->pms_material_id);
		$criteria->compare('pms_template_material_id',$this->pms_template_material_id);
		$criteria->compare('type_from',$this->type_from);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('updated_date',$this->updated_date,true);
		$criteria->compare('pms_task_name',$this->pms_task_name,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PmsAccWprItemConsumed the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
