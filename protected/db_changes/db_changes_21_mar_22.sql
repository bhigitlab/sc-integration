ALTER TABLE `jp_expenses` ADD `duplicate_ignore_status` ENUM('0','1') NOT NULL DEFAULT '0' COMMENT '0=>Not Ignored, 1=>Ignored' AFTER `approval_status`, ADD `duplicate_delete_status` ENUM('0','1') NOT NULL DEFAULT '0' COMMENT '0=>Not Deleted, 1=>Deleted' AFTER `duplicate_ignore_status`;

ALTER TABLE `jp_bills` ADD `duplicate_ignore_status` ENUM('0','1') NOT NULL DEFAULT '0' COMMENT '0=>Not Ignored, 1=>Ignored' AFTER `warehouse_id`, ADD `duplicate_delete_status` ENUM('0','1') NOT NULL DEFAULT '0' COMMENT '0=>Not Deleted, 1=>Deleted' AFTER `duplicate_ignore_status`;

ALTER TABLE `jp_dailyvendors` ADD `duplicate_ignore_status` ENUM('0','1') NOT NULL DEFAULT '0' COMMENT '0=>Not Ignored, 1=>Ignored' AFTER `approval_status`, ADD `duplicate_delete_status` ENUM('0','1') NOT NULL DEFAULT '0' COMMENT '0=>Not Deleted, 1=>Deleted' AFTER `duplicate_ignore_status`;


ALTER TABLE `jp_dailyexpense` ADD `duplicate_ignore_status` ENUM('0','1') NOT NULL DEFAULT '0' COMMENT '0=>Not Ignored, 1=>Ignored' AFTER `approval_status`, ADD `duplicate_delete_status` ENUM('0','1') NOT NULL DEFAULT '0' COMMENT '0=>Not Deleted, 1=>Deleted' AFTER `duplicate_ignore_status`;


ALTER TABLE `jp_subcontractor_payment` ADD `duplicate_ignore_status` ENUM('0','1') NOT NULL DEFAULT '0' COMMENT '0=>Not Ignored, 1=>Ignored' AFTER `approval_status`, ADD `duplicate_delete_status` ENUM('0','1') NOT NULL DEFAULT '0' COMMENT '0=>Not Deleted, 1=>Deleted' AFTER `duplicate_ignore_status`;