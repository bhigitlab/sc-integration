<?php

class DepositController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		/* return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','newlist'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','Savetopdf','Savetoexcel','update2'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		); */
            
            $accessArr = array();
           $accessauthArr = array();
           $controller = Yii::app()->controller->id;
           if(isset(Yii::app()->user->menuall)){
               if (array_key_exists($controller, Yii::app()->user->menuall)) {
                   $accessArr = Yii::app()->user->menuall[$controller];
               } 
           }

           if(isset(Yii::app()->user->menuauth)){
               if (array_key_exists($controller, Yii::app()->user->menuauth)) {
                   $accessauthArr = Yii::app()->user->menuauth[$controller];
               }
           }
           $access_privlg = count($accessauthArr);
           return array(
               array('allow', // allow all users to perform 'index' and 'view' actions
                   'actions' => $accessArr,
                   'users' => array('@'),
                   'expression' => "$access_privlg > 0",
               ),

               array('allow', // allow admin user to perform 'admin' and 'delete' actions
                   'actions' => $accessauthArr,
                   'users' => array('@'),
                   'expression' => "$access_privlg > 0",
               ),
               array('deny',  // deny all users
                   'users'=>array('*'),
               ),
           );
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Deposit;

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['Deposit']))
		{
			$model->attributes=$_POST['Deposit'];
			$model->company_id   = Yii::app()->user->company_id;
			if($model->save())
				$this->redirect(array('newlist'));
		}
                 if(isset($_GET['layout']))
                $this->layout = false;

		$this->render('create',array(
			'model'=>$model,
		));
	}
	
	public function actionNewlist() {

        $model = new Deposit('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Deposit']))
            $model->attributes = $_GET['Deposit'];
        
        $this->render('newlist', array(
            'model' => $model,
            'dataProvider' => $model->search(),
        ));
    }

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['Deposit']))
		{
			$model->attributes=$_POST['Deposit'];
			$model->company_id   = Yii::app()->user->company_id;
			if($model->save())
				//$this->redirect(array('newlist'));
				if(Yii::app()->user->returnUrl !=0){
					 $this->redirect(array('newlist', 'Deposit_page' => Yii::app()->user->returnUrl));
				 } else {
					 $this->redirect(array('newlist'));
				 }
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}
        public function actionUpdate2()
	{
           
            $id = $_POST['id'];
            $value = $_POST['value'];
            $model=$this->loadModel($id);
            $model->deposit_name = $value;

            // Uncomment the following line if AJAX validation is needed
            // $this->performAjaxValidation($model);
            if($model->save())
            {
                    $data = null;
                    print_r(json_encode($data));
            }else
            {
                    $error = $model->getErrors(); 
                    print_r(json_encode($error['deposit_name']));
            }
        }

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Deposit');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Deposit('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Deposit']))
			$model->attributes=$_GET['Deposit'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}
	
	
	public function actionSavetopdf() {
		$this->logo = $this->realpath_logo;
        $tblpx = Yii::app()->db->tablePrefix;
        $qry   = "SELECT * FROM " . $tblpx . "deposit";
        $data  = Yii::app()->db->createCommand($qry)->queryAll();
        $mPDF1 = Yii::app()->ePdf->mPDF();
        $mPDF1 = Yii::app()->ePdf->mPDF('', 'A4');
        $sql = 'SELECT template_name '
                . 'FROM jp_quotation_template '
                . 'WHERE status="1"';
        $selectedtemplate =Yii::app()->db->createCommand($sql)->queryScalar();
        
        $template = $this->getTemplate($selectedtemplate);
        $mPDF1->SetHTMLHeader($template['header']);
        $mPDF1->SetHTMLFooter($template['footer']);
        $mPDF1->AddPage('','', '', '', '', 0,0,40, 30, 10,0);

        $mPDF1->WriteHTML($this->renderPartial('deposittopdf', array(
            'model' => $data,
        ), true));
        $filename = "Deposit List";
        $mPDF1->Output($filename . '.pdf', 'D');
    }
    public function actionSavetoexcel() {
        $tblpx       = Yii::app()->db->tablePrefix;
        $qry         = "SELECT * FROM " . $tblpx . "deposit";
        $data        = Yii::app()->db->createCommand($qry)->queryAll();
        $finaldata   = array();
        $arraylabel  = array('Sl No.', 'Deposit Name');        
        $i           = 1;
        $finaldata   = array();
        foreach ($data as $key => $datavalue) {
            $finaldata[$key][] = $i;
            $finaldata[$key][] = $datavalue['deposit_name'];            
            $i++;
        }
        Yii::import('ext.ECSVExport');
        $csv         = new ECSVExport($finaldata);
        $csv->setHeaders($arraylabel);
        $csv->setToAppend();
        $contentdata = $csv->toCSV();
        $csvfilename = 'deposit_list.csv';
        Yii::app()->getRequest()->sendFile($csvfilename, $contentdata, "text/csv", false);
        exit();
    }

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Deposit the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Deposit::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Deposit $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='deposit-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
