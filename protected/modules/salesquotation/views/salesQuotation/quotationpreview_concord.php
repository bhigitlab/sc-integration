<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/validation/jquery.validate.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/validation/additional-methods.js"></script>

<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/message/css/jquery.message.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/message/js/jquery.message.js"></script>
<style>
    .lefttdiv {
        float: left;
    }

    .purchase-title {
        border-bottom: 1px solid #ddd;
    }

    .purchase_items h3 {
        font-size: 18px;
        color: #333;
        margin: 0px;
        padding: 0px;
        text-align: left;
        padding-bottom: 10px;
    }

    .purchase_items {
        padding: 15px;
        box-shadow: 0 0 13px 1px rgba(0, 0, 0, 0.25);
    }

    .purchaseitem {
        display: inline-block;
        margin-right: 20px;
    }

    .purchaseitem last-child {
        margin-right: 0px;
    }

    .purchaseitem label {
        font-size: 12px;
    }

    .remark {
        display: none;
    }

    .padding-box {
        padding: 3px 0px;
        min-height: 17px;
        display: inline-block;
    }

    th {
        height: auto;
    }

    .quantity,
    .rate {
        max-width: 80px;
    }

    .text_align {

        text-align: center;
    }


    *:focus {
        border: 1px solid #333;
        box-shadow: 0 0 6px 1px rgba(0, 0, 0, 0.25);
    }

    .text-right {
        text-align: right;
    }

    .light_bg_grey {
        background: #88888838 !important;
    }
    table.item_table tbody tr.catehead td {
        background-color: #c5c5dd;
        font-weight: 600;
        color: #000;
        padding: 15px 10px;
    }
</style>
<script>
    $(function () {
        $("#datepicker").datepicker({
            dateFormat: 'dd-mm-yy'
        }).datepicker("setDate", new Date());

    });
</script>
<div>    
    <div class="invoicemaindiv">                
        <form id="pdfvals1" method="post" action="">
            <input type="hidden" name="purchaseview">
            <div class="row">
                <div class="col-md-2">
                    <label>COMPANY : </label>
                    <div>
                        <?php
                        $company = Company::model()->findByPk($model->company_id);
                        echo $company['name'];
                        ?>
                    </div>
                </div>
                <div class="col-md-2">
                    <label>CLIENT : </label>
                    <div>
                        <?php echo $model->client_name; ?>
                    </div>
                </div>
                <div class="col-md-2">
                    <label>DATE : </label>
                    <div>
                        <?php echo isset($model->date_quotation) ? date('d-m-Y', strtotime($model->date_quotation)) : ''; ?>
                    </div>
                </div>
                <div class="col-md-2">
                    <label>Quotation No : </label>
                    <div>
                        <?php echo isset($model->invoice_no) ? $model->invoice_no : ''; ?>
                    </div>
                </div>
                <div class="col-md-2">
                    <label>Revision No : </label>
                    <div>
                        <?php echo "REV-".$rev_no ?>
                    </div>
                </div>
            </div>
            
            <div class="clearfix"></div>
            <br><br>
            <div id="table-scroll" class="table-scroll">
                <div id="faux-table" class="faux-table" aria="hidden"></div>
                <div id="table-wrap" class="table-wrap">
                    <div class="table-responsive">
                        <table cellpadding="10" class="item_table">
                            <thead>
                            <tr>

                                <th>S.NO</th>
                                <th>DESCRIPTION OF ITEM</th>                                    
                                <th>HSN CODE</th>                                    
                                <th>UNIT</th>
                                <th>QTY</th>                                    
                                <th>RATE</th>
                                <th>AMOUNT</th>                                    
                            </tr>
                            </thead>
                            <tbody>
                                <?php
                                $new_rev = $rev_no;
                                $rev_no = "REV-".$rev_no."";
                                
                                $sql = 'SELECT * FROM `jp_quotation_section` '
                                        . 'WHERE `qtn_id` = ' . $qtid
                                        . ' AND revision_no = "' . $rev_no . '"';
                                    
                                $sectionArray = Yii::app()->db->createCommand($sql)->queryAll();
                                $letters = range('A', 'Z');
                                $k = 0;
                                $sum = 0;                                
                                foreach ($sectionArray as $key => $value1) {
                                    $sql = 'SELECT * FROM `jp_sales_quotation` WHERE `parent_type` = 0 '
                                                . ' AND `parent_id` = ' . $value1['id']
                                                . ' AND revision_no="' . $rev_no . '" AND `deleted_status` = 1';
                                        $itemArray = Yii::app()->db->createCommand($sql)->queryAll();

                                        $sql = 'SELECT * FROM `jp_quotation_gen_category` '
                                        . 'WHERE `qid` = ' . $qtid . ' AND `section_id` = ' . $value1["id"]
                                        . ' AND revision_no = "' . $rev_no . '"';
                                        $subsectionArray = Yii::app()->db->createCommand($sql)->queryAll();
                                        $unit = Unit::model()->findByPk($value1['unit']);
                                    ?>
                                    <tr>
                                        <th  style="background:#c5d9f1;"><?php echo $letters[$k]; ?></th>
                                        <th  style="background:#c5d9f1;"><?php echo $value1['section_name'] ?></th>                                        
                                        <td  style="background:#c5d9f1;"><?php echo $value1['hsn_code'] ?></td>                                                                                                                    
                                        <td  style="background:#c5d9f1;"><?php echo isset($unit) ? $unit->unit_name : "" ?></td>
                                        <td  style="background:#c5d9f1;"><?php echo $value1['quantity']; ?></td>
                                        <td  style="background:#c5d9f1;" class="text-right"><?php echo $value1['mrp'] ?></td>
                                        <td  style="background:#c5d9f1;" class="text-right"><?php echo $value1['amount_after_discount'] ?></td>
                                        
                                    </tr>
                                    <?php
                                    
                                    $j= 1;
                                    $total_after_discount_sum = 0;
                                    foreach ($itemArray as $key => $value) {
                                        $sum += $value['amount_after_discount'];
                                        $unit = Unit::model()->findByPk($value['unit']);
                                        ?>
                                        <tr class="td_group">
                                            <td><?php echo $j ?></td>
                                            <td><?php echo $value['worktype_label'] ?></td>
                                            <td><?php echo $value['hsn_code'] ?></td>
                                            <td><?php echo isset($unit) ? $unit->unit_name : "" ?></td>
                                            <td><?php echo $value['quantity']; ?></td>
                                            <td class="text-right"><?php echo Controller::money_format_inr($value['mrp'],2) ?></td>
                                            <td class="text-right"><?php echo Controller::money_format_inr($value['amount_after_discount'],2) ?></td>
                                            
                                        </tr>
                                    <?php $j++; } ?>

                                    <?php 
                                    $i = 1;                                                                           
                                    foreach ($subsectionArray as $key => $value) {  
                                        $sum += $value['amount_after_discount'];                                                                              
                                        $unit = Unit::model()->findByPk($value['unit']);
                                        ?>
                                        <tr>                                        
                                            <td><?php echo $i; ?></th>
                                            <td><?php echo $value['category_label'] ?></th>                                            
                                            <td><?php echo $value['hsn_code'] ?></td>                                                
                                            <td><?php echo isset($unit) ? $unit->unit_name : "" ?></td>
                                            <td><?php echo ($model->template_type == '5') ? $value['quantity']:$value['quantity_nos']; ?></td>
                                            <td class="text-right"><?php echo Controller::money_format_inr($value['mrp'],2) ?></td>
                                            <td class="text-right"><?php echo Controller::money_format_inr($value['amount_after_discount'],2) ?></td>
                                                
                                            
                                        </tr>                                                                                
                                        <?php
                                        $i++;
                                    }
                                    ?>
                                    <tr>
                                        <td style="background:#c0c0c0;"></td>
                                        <th style="background:#c0c0c0;" class="text-center">
                                        Total Cost for <?php echo $value1['section_name'];?>
                                        </th>
                                        <td style="background:#c0c0c0;"></td>
                                        <td style="background:#c0c0c0;"></td>
                                        <td style="background:#c0c0c0;"></td>
                                        <td style="background:#c0c0c0;"></td>
                                        <th class="text-right"  style="background:#c0c0c0;">
                                            <?php echo Controller::getMainSectionSum($value1['id'], $qtid, $rev_no); ?>
                                        </th>
                                    </tr>
                                <?php
                                
                                $k++;} ?>
                            </tbody> 
                            <tfoot>
                                <tr>
                                    <th colspan="6" class="text-right">MRP</th>
                                    <th class="text-right">
                                        <h5><?php
                                        $total_amount = Controller::getMrpSum($qtid, $rev_no,NULL);
                                        echo Controller::money_format_inr($total_amount, 2);
                                        ?></h5>
                                    </th>
                                </tr>
                                <tr>
                                    <th colspan="6" class="text-right">Amount After Discount</th>
                                    <th class="text-right">
                                        <h5><?php
                                        $total_amount = $sum;
                                        echo Controller::money_format_inr($total_amount, 2);
                                        ?></h5>
                                    </th>
                                </tr>
                                <tr>
                                    <th colspan="6" class="text-right">Amount After GST</th>
                                    <th  class="text-right">
                                    <h5><?php
                                        $sql = 'SELECT * FROM jp_quotation_revision '
                                        . ' WHERE qid='.$qtid
                                        . ' AND revision_no="'.$rev_no.'"';
                                        $revision_model = Yii::app()->db->createCommand($sql)->queryRow();
                                        $total_tax = $revision_model['sgst_amount']+
                                        $revision_model['cgst_amount']+
                                        $revision_model['igst_amount'];
                                        $total_amoun_withtax = $total_amount + $total_tax;
                                        echo Controller::money_format_inr($total_amoun_withtax, 2);
                                        ?></h5>
                                    </th>
                                </tr>
                                <tr>
                                    <th colspan="6" class="text-right">Total Amount After Discount</th>
                                    <th class="text-right">
                                        <h5><?php
                                        $total_after_discount = $total_amoun_withtax - $revision_model['discount_amount'];
                                        echo Controller::money_format_inr($total_after_discount , 2);                                        
                                        ?></h5>
                                    </th>
                                </tr>
                                <tr>
                                    <th colspan="6" class="text-right">Amount after additional discount</th>
                                    <th class="text-right">
                                        <h5><?php
                                        $addition_discount_amount = is_null($revision_model['total_after_additionl_discount']) ? $total_after_discount  :$revision_model['total_after_additionl_discount'];
                                        echo Controller::money_format_inr($addition_discount_amount,2); 
                                        ?></h5>
                                    </th>
                                </tr>
                                
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
            
        </form>
    </div>
    <style>
        .error_message {
            color: red;
        }

        a.pdf_excel {
            background-color: #6a8ec7;
            display: inline-block;
            padding: 8px;
            color: #fff;
            border: 1px solid #6a8ec8;
        }

        .invoicemaindiv th,
        .invoicemaindiv td {
            padding: 10px;
        }
    </style>


    <script>

        $(document).ready(function () {
            $('#loading').hide();
        })
        function closeaction() {
            $('#send_form').slideUp(500);
        }

        $('#sendmail').click(function () {
            $('#send_form').slideDown();
        });

        $("#form_send").validate({
            rules: {
                mail_content: {
                    required: true,
                },
                mail_to: {
                    required: true,
                    email: true
                },
            },
            messages: {
                mail_content: {
                    required: "Please enter mesage",
                },
                mail_to: {
                    required: "Please enter email",
                    email: "Please enter valid email"
                }
            },
        });

        $('.send_btn').click(function (e) {
            e.preventDefault();
            var data = $('#form_send').serialize();
            if ($("#form_send").valid()) {
                $('#loader').css('display', 'inline-block');
                if ($("#send_copy").prop('checked') == true) {
                    var send_copy = 1;
                } else {
                    var send_copy = 0;
                }
                var qid = $('#quotation_id').val();
                var revision_no = $('#revision_no').val();
                var mail_to = $('#mail_to').val();
                var mail_content = $('#mail_content').val();
                var pdf_path = $('#pdf_path').val();
                var pdf_name = $('#pdf_name').val();
                $('.send_btn').prop('disabled', true)
                $('.loading-overlay').addClass('is-active');
                $.ajax({
                    url: '<?php echo Yii::app()->createAbsoluteUrl('salesquotation/salesQuotation/sendattachment'); ?>',
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        qid: qid,
                        revision_no: revision_no,
                        mail_to: mail_to,
                        mail_content: mail_content,
                        pdf_path: pdf_path,
                        pdf_name: pdf_name,
                        send_copy: send_copy
                    },
                    success: function (response) {
                        if (response.status == '1') {
                            $('#loader').hide();
                            $('.send_btn').prop('disabled', false)
                            $('#send_form').slideUp(500);
                            $("#form_send").trigger('reset');
                            $("#errormessage").show()
                                    .html('<div class="alert alert-success">' + response.message + '</div>')
                                    .fadeOut(10000);
                        } else {
                            $('#loader').hide();
                            $('.send_btn').prop('disabled', false)
                            $("#errormessage").show()
                                    .html('<div class="alert alert-danger">' + response.message + '</div>')
                                    .fadeOut(10000);
                        }
                    }
                })
            }
        })

        $(".sendmail").click(function () {
            var q_id = $(this).attr("data-id");
            var revision_no = $(this).attr("data-rev");
            $('#loading').show();
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: '<?php echo Yii::app()->createUrl('salesquotation/salesQuotation/pdfgeneration'); ?>',
                data: {
                    q_id: q_id,
                    revision_no: revision_no,
                },
                success: function (response) {
                    $('#pdf_path').val(response.path);
                    $('#pdf_name').val(response.name);
                }
            })
        })
        $(function () {
            $('.pdfbtn1').click(function () {
                $("form#pdfvals1").attr('action', "<?php echo $this->createAbsoluteUrl('SalesQuotation/SaveQuotation', array('qid' => $model['id'], 'rev_no' => $rev_no));
                                ?>");
                $("form#pdfvals1").submit();
            });
        });


    </script>

</div>

<style>
    .table-scroll {
        position: relative;
        max-width: 1280px;
        width: 100%;
        margin: auto;
        display: table;
    }

    .table-wrap {
        width: 100%;
        display: block;
        overflow: auto;
        position: relative;
        z-index: 1;
        border: 1px solid #ddd;
    }

    .table-wrap.fixedON,
    .table-wrap.fixedON table,
    .faux-table table {
        height: 380px;
        /* match heights*/
    }

    .table-scroll table {
        width: 100%;
        margin: auto;
        border-collapse: collapse;
        border-spacing: 0;
        border: 1px solid #ddd;
    }

    .table-scroll th,
    .table-scroll td {
        padding: 5px 10px;
        border: 1px solid #ddd;
        background: #fff;
        vertical-align: middle;
    }
    h5{
        margin: 10px 0px !important;
    }

    .faux-table table {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        pointer-events: none;
    }

    .faux-table table tbody {
        visibility: hidden;
    }

    /* shrink cells in cloned table so that the table height is exactly 300px so that the header and footer appear fixed */
    .faux-table table tbody th,
    .faux-table table tbody td {
        padding-top: 0;
        padding-bottom: 0;
        border-top: none;
        border-bottom: none;
        line-height: 0.1;
    }

    .faux-table table tbody tr+tr th,
    .faux-table tbody tr+tr td {
        line-height: 0;
    }

    .faux-table thead th,
    .faux-table tfoot th,
    .faux-table tfoot td,
    .table-wrap thead th,
    .table-wrap tfoot th,
    .table-wrap tfoot td {
        background: #eee;
    }

    .faux-table {
        position: absolute;
        top: 0;
        right: 0;
        left: 0;
        bottom: 0;
        overflow-y: scroll;
    }

    .faux-table thead,
    .faux-table tfoot,
    .faux-table thead th,
    .faux-table tfoot th,
    .faux-table tfoot td {
        position: relative;
        z-index: 2;
    }

    /* ie bug */
    .table-scroll table thead tr,
    .table-scroll table thead tr th,
    .table-scroll table tfoot tr,
    .table-scroll table tfoot tr th,
    .table-scroll table tfoot tr td {
        height: 1px;
    }
</style>
<script>
    (function () {
        var mainTable = document.getElementById("main-table");
        if (mainTable !== null) {
            var tableHeight = mainTable.offsetHeight;
            if (tableHeight > 380) {
                text - right
                var fauxTable = document.getElementById("faux-table");
                document.getElementById("table-wrap").className += ' ' + 'fixedON';
                var clonedElement = mainTable.cloneNode(true);
                clonedElement.id = "";
                fauxTable.appendChild(clonedElement);
            }
        }
    })();

    $(document).ajaxComplete(function () {
        $('.loading-overlay').removeClass('is-active');
        $('#loading').hide();
        $('#loader1').hide();
    });
</script>