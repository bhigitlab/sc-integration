<?php

if (!empty($data)) {
    $html['html'] .= '<div class="alert alert-success get_prevdata  alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
			<div id="parent">
                            <table style="width:100%;" id="pre_fixtable">
                                <thead>
                                <tr>
                                    <th style="text-align:center">Sl.No</th>
				    <th style="text-align:center;' . $shidden . '">Shutter Material</th>
                                    <th style="text-align:center;' . $shidden . '">Carcass Material</th>
                                    <th style="text-align:center;' . $shidden . '">Shutter Finish</th>
                                    <th style="text-align:center;' . $shidden . '">Carcass Finish</th>
                                    <th style="text-align:center;' . $additional_hidden . '">Material</th>
                                    <th style="text-align:center;' . $additional_hidden . '">Finish</th>
                                    <th style="text-align:center;' . $shidden . '">Shutter Description</th>
                                    <th style="text-align:center;' . $shidden . '">Carcass Description</th>
                                    <th style="text-align:center; ' . $deschidden . '">Description</th></tr><thead>
                                    <tbody>
                                    <tr><td colspan="2"><p>Item Details</p></td></tr>';
    $html['html'] .= '<tr class="getprevious" style="cursor:pointer;">
                                    <td style="text-align:center;">1</td>
				    <td style="text-align:center;' . $shidden . '">' . (isset($sh_materialdata) ? $sh_materialdata : '') . '</td>
				    <td style="text-align:center;' . $shidden . '">' . (isset($cc_materialdata) ? $cc_materialdata : '') . '</td>
				    <td style="text-align:center;' . $shidden . '">' . (isset($sh_finish) ? $sh_finish : '') . '</td>
				    <td style="text-align:center;' . $shidden . '">' . (isset($cc_finish) ? $cc_finish : '') . '</td>
                                    <td style="text-align:center;' . $additional_hidden . '">' . (isset($material) ? $material : '') . '</td>
				    <td style="text-align:center;' . $additional_hidden . '">' . (isset($finish_data) ? $finish_data : '') . '</td>
				    <td style="text-align:center;' . $shidden . '">' . (isset($data[0]['shutterwork_description']) ? $data[0]['shutterwork_description'] : '') . '</td>
				    <td style="text-align:center;' . $shidden . '">' . (isset($data[0]['caracoss_description']) ? $data[0]['caracoss_description'] : '') . '</td>
				    <td style="text-align:center; ' . $deschidden . '">' . (isset($data[0]['description']) ? $data[0]['description'] : '') . '</td></tr>';
    $html['html'] .= '</tbody></table></div></div>';
    $html['status'] .= true;
} else {
    $html['html'] .= '';
    $html['status'] .= false;
}
$html['shutterdesc'] .= '';
if (isset($data[0]['shutterwork_description'])) {
    $html['shutterdesc'] .= $data[0]['shutterwork_description'];
}
$html['carcassdesc'] .= '';
if (isset($data[0]['caracoss_description'])) {
    $html['carcassdesc'] .= $data[0]['caracoss_description'];
}
$html['shuttermaterial'] .= '';
if (isset($sh_materialdata)) {
    $html['shuttermaterial'] .= $sh_materialdata;
}
$html['carcasmaterial'] .= '';
if (isset($cc_materialdata)) {
    $html['carcasmaterial'] .= $cc_materialdata;
}
$html['shutterfinish'] .= '';
if (isset($sh_finish)) {
    $html['shutterfinish'] .= $sh_finish;
}
$html['carcassfinish'] .= '';
if (isset($cc_finish)) {
    $html['carcassfinish'] .= $cc_finish;
}
$html['material'] .= '';
if (isset($material)) {
    $html['material'] .= $material;
}
$html['finish'] .= '';
if (isset($finish_data)) {
    $html['finish'] .= $finish_data;
}
$html['description'] .= '';
if (isset($data[0]['description'])) {
    $html['description'] .= $data[0]['description'];
}
$html['item_id'] .= '';
if (isset($data[0]['id'])) {
    $html['item_id'] .= $data[0]['id'];
}
echo json_encode($html);
