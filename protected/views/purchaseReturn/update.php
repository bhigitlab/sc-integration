<?php
/* @var $this BillsController */
/* @var $model Bills */

$this->breadcrumbs = array(
    'Bills' => array('index'),
    $model->return_id => array('view', 'id' => $model->return_id),
    'Update',
);

$this->menu = array(
    //array('label'=>'List Bills', 'url'=>array('index')),
    //array('label'=>'Create Bills', 'url'=>array('create')),
    //array('label'=>'View Bills', 'url'=>array('view', 'id'=>$model->bill_id)),
    //array('label'=>'Manage Bills', 'url'=>array('admin')),
);
?>

<div class="container" id="expense">
    <div class="loading-overlay">
        <span class="fa fa-spinner fa-3x fa-spin"></span>
    </div>
    <div class="expenses-heading sub-heading mb-10">
        <h3>Update Purchase Return</h3>
        <a href="<?php echo Yii::app()->createAbsoluteUrl('purchaseReturn/admin'); ?>" class="button btn btn-info"
            title="List Bills">List Purchase Return</a>
        <div id="loading"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/ajax-loader.gif"></div>
    </div>
    <?php $this->renderPartial('_updateform', array('model' => $model, 'client' => $client, 'vendorName' => $vendorName, )); ?>
</div>
<script>
    $(document).ready(function () {
        $('#loading').hide();
    });
</script>