<?php
if (Yii::app()->user->role != 1) {
    $warehouse = Warehouse::model()->findAll(array('condition' => 'FIND_IN_SET(' . Yii::app()->user->id . ', assigned_to)'));
} else {
    $warehouse = Warehouse::model()->findAll();
}
?>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">

<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.js"></script>

<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/message/css/jquery.message.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/message/js/jquery.message.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/tableHeadFixer.js"></script>

<!--<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/fonts/glyphicons-halflings-regular.eot">
<link rel="stylesheet" href="<?php //echo Yii::app()->theme->baseUrl;   
                                ?>/plugins/smoke/css/smoke.min.css">
<script type="text/javascript" src="<?php // echo Yii::app()->theme->baseUrl;   
                                    ?>/plugins/smoke/js/smoke.min.js"></script>-->
<script>
    var shim = (function() {
        document.createElement('datalist');
    })();
</script>
<style>
    .lefttdiv {
        float: left;
    }

    .select2 {
        width: 100%;
    }
</style>
<script>
    $(function() {
        //        $("#datepicker").datepicker({dateFormat: 'dd-mm-yy'}).datepicker("setDate", new Date());
        //        $( "#datepicker" ).datepicker({  maxDate: new Date() });
        $("#datepicker").datepicker({
            maxDate: new Date(),
            dateFormat: 'dd-mm-yy'
        }).datepicker("setDate", new Date());
    });
</script>
<style>
    .dropdown {
        position: relative;
        display: inline-block;
    }

    .dropdown::before {
        position: absolute;
        content: " \2193";
        top: 0px;
        right: -8px;
        height: 20px;
        width: 20px;
    }

    button#caret {
        border: none;
        background: none;
        position: absolute;
        top: 0px;
        right: 0px;
    }

    .invoicemaindiv th,
    .invoicemaindiv td {
        padding: 10px;
        text-align: left;
    }

    .addcolumn {
        cursor: pointer;
        background-color: black;
        border: none;
        color: white;
        text-align: center;
        text-decoration: none;
        margin: 2px;
        font-size: 30px;
        width: 40px;
    }

    .invoicemaindiv .pull-right,
    .invoicemaindiv .pull-left {
        float: none !important;
    }

    #parent,
    #parent2,
    #parent3 {
        max-height: 150px;
    }

    .select2.select2-container.select2-container--default.select2-container--above,
    .select2.select2-container.select2-container--default.select2-container--below,
    .select2.select2-container.select2-container--default.select2-container--focus {
        width: 200px !important;
    }

    @media(min-width: 767px) {
        .invoicemaindiv .pull-right {
            float: right !important;
        }

        .invoicemaindiv .pull-left {
            float: left !important;
        }
    }

    /*      .checkek_edit{
                    pointer-events: none;
             }*/



    .add_selection {
        background: #000;
    }




    .toast-container {
        width: 350px;
    }

    .toast-position-top-right {
        top: 57px;
        right: 6px;
    }

    .toast-item-close {
        background-image: none;
        cursor: pointer;
        width: 12px;
        height: 12px;
        text-align: center;
        border-radius: 2px;
    }

    .toast-item-image {
        font-size: 24px;
    }

    .toast-item-close:hover {
        color: red;
    }

    .toast-item {
        border: transparent;
        border-radius: 3px;
        font-size: 10px;
        opacity: 1;
        background-color: rgba(34, 45, 50, 0.8);
    }

    .toast-item-wrapper p {
        margin: 0px 5px 0px 42px;
        font-size: 14px;
        text-align: justify;
    }


    .toast-type-success {
        background-color: #00A65A;
        border-color: #00A65A;
    }

    .toast-type-error {
        background-color: #DD4B39;
        border-color: #DD4B39;
    }

    .toast-type-notice {
        background-color: #00C0EF;
        border-color: #00C0EF;
    }

    .toast-type-warning {
        background-color: #F39C12;
        border-color: #F39C12;
    }

    .span_class {
        min-width: 70px;
        display: inline-block;
    }

    .purchase-title {
        border-bottom: 1px solid #ddd;
    }

    .purchase_items h3 {
        font-size: 18px;
        color: #333;
        margin: 0px;
        padding: 0px;
        text-align: left;
        padding-bottom: 10px;
    }

    .purchase_items {
        /*padding:15px;
        box-shadow:0 0 4px 1px rgba(0,0,0,0.25);*/
    }

    .purchaseitem {
        display: inline-block;
        margin-right: 20px;
    }

    .purchaseitem last-child {
        margin-right: 0px;
    }

    .purchaseitem label {
        font-size: 12px;
    }

    .remark {
        display: none;
    }

    .padding-box {
        padding: 3px 0px;
        min-height: 17px;
        display: inline-block;
    }

    th {
        height: auto;
    }

    .quantity,
    .rate {
        max-width: 80px;
    }

    *:focus {
        border: 1px solid #333;
        box-shadow: 0 0 6px 1px rgba(0, 0, 0, 0.25);
    }

    .rate_highlight {
        background: #DD1035 !important;
        color: #fff;
    }

    .block_purchase {

        padding-left: 15px;

    }

    .block_hold {
        padding-bottom: 15px;
    }
</style>

<div class="container">

    <div class="invoicemaindiv">
        <div class='clearfix'>
            <div class="pull-left">
                <h2 class="">Stock Opening Balance</h2>
            </div>
        </div>
        <div id="msg_box"></div>
        <!-- ---------------------------->
        <div class="row_block custom-form-style">
            <div class="row">
                <div class="col-md-3 col-sm-6 elem_block">
                    <div class="form-group">
                        <label>WAREHOUSE : </label>
                        <select name="warehouse" class="inputs target warehouse" id="warehouse" style="width:190px">
                            <option value="">Choose Warehouse</option>
                            <?php
                            foreach ($warehouse as $key => $value) {
                            ?>
                                <option value="<?php echo $value['warehouse_id']; ?>"><?php echo $value['warehouse_name']; ?></option>
                            <?php
                            }
                            ?>

                        </select>
                    </div>
                </div>

                <div class="col-md-3 col-sm-6 elem_block">
                    <div class="form-group">
                        <label>DATE : </label>
                        <input type="text" value="<?php echo ((isset($date) && $date != '') ? date("d-m-Y", strtotime($date)) : ''); ?>" id="datepicker" class="txtBox date inputs target form-control" name="warehouse_date_master" placeholder="Please click to edit" readonly="true">
                    </div>
                </div>
            </div>
        </div>
        <!-- ---------------------------->

        <form id="warehousestock" method="post" action="#">
            <input type="hidden" name="remove" id="remove" value="">
            <input type="hidden" name="warehousestock_id" id="warehousestock_id" value="0">
            <div class="block_hold clearfix" id="warehouseform">
                <div class="block_purchase">
                    <div class="row_block" style="margin-left: -32px;">





                    </div>
                </div>


                <div class="clearfix"></div>
                <div id="msg"></div>

                <div class="purchase_items stock-ob-form">
                    <div class="clearfix">
                        <div class="row">
                            <div class="col-md-3 col-sm-6 purchaseitem input-w-100">
                                <div id="select_div">
                                    <label>Specification:</label>
                                    <select style="width:100%;" class="txtBox stock_item" id="stock_item" name="stock_item">
                                        <option value="">Select one</option>
                                        <?php
                                        foreach ($specification as $key => $value) {
                                        ?>
                                            <option value="<?php echo $value['id']; ?>"><?php echo $value['data']; ?></option>
                                        <?php } ?>
                                        <!--					<option value="other">Other</option>-->
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 purchaseitem">
                                <label>Units:</label>
                                <div id="stock_unit" class="stock_unit padding-box">&nbsp;&nbsp;</div>
                            </div>
                            <!--			<div class="purchaseitem remark">
                                                            <label>Remarks:</label>
                                                            <input type="text" class="txtBox " id="remarks" name="remark[]" placeholder="Remark"/>
                                                    </div>-->
                            <div class="col-md-3 col-sm-6 purchaseitem batch_div">
                                <label>Batch:</label>
                                <input type="text" class="inputs target txtBox batch" id="batch" name="batch[]" placeholder="" /></td>
                            </div>
                            <div class="col-md-3 col-sm-6 purchaseitem quantity_div">
                                <label>Quantity:</label>
                                <input type="text" class="inputs target txtBox quantity allownumericdecimal" id="quantity" name="quantity[]" value='0' placeholder="" /></td>
                            </div>
                            <div class="col-md-3 col-sm-6 purchaseitem rate_div">
                                <label>Rate:</label>
                                <input type="text" class="inputs target txtBox rate allownumericdecimal" id="rate" name="rate[]" value='0' placeholder="" /></td>
                            </div>
                            <div class="col-md-3 col-sm-6 purchaseitem amount_div">
                                <label>Amount:</label>
                                <input type="text" class="inputs target txtBox amount allownumericdecimal" id="amount" name="amount[]" placeholder="" /></td>
                            </div>
                        </div>
                        <div class="purchaseitem pull-right">
                            <input type="button" class="item_save" id="0" value="Save">
                        </div>
                    </div>
        </form>
    </div>
</div>
</div>
<?php $this->renderPartial('_list_data', array('list' => $list,)); ?>

</div>



<input type="hidden" name="final_amount" id="final_amount" value="0">

<input type="hidden" name="item_amount_temp" id="item_amount_temp" value="0">


<style>
    .error_message {
        color: red;
    }

    a.pdf_excel {
        background-color: #6a8ec7;
        display: inline-block;
        padding: 8px;
        color: #fff;
        border: 1px solid #6a8ec8;
    }

    .batch {
        width: 90px;
    }
</style>



<script>
    jQuery.extend(jQuery.expr[':'], {
        focusable: function(el, index, selector) {

            return $(el).is('button, :input, [tabindex]');
        }
    });

    $(document).on('keypress', 'input,select', function(e) {
        if (e.which == 13) {
            e.preventDefault();
            // Get all focusable elements on the page
            var $canfocus = $(':focusable');
            var index = $canfocus.index(document.activeElement) + 1;
            if (index >= $canfocus.length)
                index = 0;
            $canfocus.eq(index).focus();
        }
    });
    $(document).on('blur', '#rate', function(e) {
        e.preventDefault();
        var quantity = $("#quantity").val();
        var rate = $("#rate").val();
        var amount = quantity * rate;
        $("#amount").val(amount);
    });
    $(document).on('blur', '#quantity', function(e) {
        e.preventDefault();
        var quantity = $("#quantity").val();
        var rate = $("#rate").val();
        var amount = quantity * rate;
        $("#amount").val(amount);
    });

    $(document).ready(function() {
        $('#warehouse').focus();
        $(".js-example-basic-single").select2();
        $(".warehouse").select2();
        $(".stock_item").select2();
        var quantity = $("#quantity").val();
        var rate = $("#rate").val();
        var amount = quantity * rate;
        $("#amount").val(amount);
    });
</script>



<script>
    $("#warehouse").change(function() {
        var val = $(this).val();
        if (val == '') {
            $('#warehouse').select2('focus');
            $('#stock_item').val('').trigger('change');
            $('#quantity').val('');
            $('#stock_unit').text('');
            $("#warehousestock_id").val(0);
        } else {
            $.ajax({
                method: "GET",
                dataType: "json",
                url: '<?php echo Yii::app()->createUrl('wh/warehousestock/ajax'); ?>',
                success: function(result) {
                    $('#datepicker').focus();
                    // $('#stock_item').select2('focus');
                }
            });
        }
    })

    $(".inputSwitch span").on("click", function() {

        var $this = $(this);

        $this.hide().siblings("input").val($this.text()).show();

    });

    $(".inputSwitch input").bind('blur', function() {

        var $this = $(this);

        $(this).attr('value', $(this).val());

        $this.hide().siblings("span").text($this.val()).show();

    }).hide();




    $('#stock_item').change(function() {
        var element = $(this);
        if ($(this).val() != '') {
            var category_id = $(this).val();
            $.ajax({
                url: '<?php echo Yii::app()->createAbsoluteUrl('wh/warehousestock/getunit'); ?>',
                type: 'GET',
                dataType: 'json',
                data: {
                    data: category_id
                },
                success: function(result) {
                    $("#stock_unit").html(result.unit);
                    $(".batch").focus();
                }

            })
        } else {
            $('#stock_item').select2('focus');
        }

    })

    $('#datepicker').change(function() {
        $('#stock_item').select2('focus');
    });
</script>


<script>
    $("#date").keypress(function(event) {
        if (event.keyCode == 13) {
            if ($(this).val()) {
                $("#purchaseno").focus();
            }
        }
    });

    $(".date").keyup(function(event) {
        if (event.keyCode == 13) {
            $(".date").click();
        }
    });

    var sl_no = 1;
    var howMany = 0;
    $('.item_save').click(function() {
        var element = $(this);
        var item_id = $(this).attr('id');
        if (item_id == 0) {
            // add
            var stock_item = $('.stock_item').val();
            var unit = $('#stock_unit').html();
            var quantity = $('#quantity').val();
            var batch = $('#batch').val();
            var warehouse = $('#warehouse').val();
            var date = $(".date").val();
            var rowCount = $('.table .addrow tr').length;
            var rate = $("#rate").val();
            if (warehouse == '' || date == '' || stock_item == '' || quantity == 0 || unit == '' || rate == 0) {
                $().toastmessage('showErrorToast', "Please enter warehouse stock details");
            } else {
                if ((moment(date, 'DD-MM-YYYY', true).isValid())) {
                    howMany += 1;
                    if (howMany == 1) {
                        var stock_id = $("#warehousestock_id").val();
                        var data = {
                            'sl_no': rowCount,
                            'quantity': quantity,
                            'rate': rate,
                            'batch': batch,
                            'stock_item': stock_item,
                            'unit': unit,
                            'stock_id': stock_id,
                            'warehouse': warehouse,
                            'date': date
                        };
                        $.ajax({
                            url: '<?php echo Yii::app()->createAbsoluteUrl('wh/warehousestock/savewarehousestock'); ?>',
                            type: 'GET',
                            //dataType:'json',
                            data: {
                                data: data
                            },
                            success: function(response) {
                                if (response == 1) {
                                    $().toastmessage('showErrorToast', "Sorry some problem occured!");
                                } else if (response == 2) {
                                    $().toastmessage('showErrorToast', "Warehouse stock already exists!");
                                } else {
                                    $().toastmessage('showSuccessToast', "Warehouse stock added successfully.");
                                    $("#newlist").html(response);
                                    $("#myTable").dataTable({});
                                }
                                howMany = 0;
                                $('#stock_item').val('').trigger('change');
                                var quantity = $('#quantity').val(0);
                                $('#batch').val('');
                                $('#rate').val(0);
                                $('#amount').val(0);
                                $('#stock_unit').text('');
                                $('#stock_item').select2('focus');
                            }
                        });

                        $('.js-example-basic-single').select2('focus');
                    }
                    //}

                } else {

                    $(this).focus();
                    $().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
                    $(this).focus();
                }
            }
        } else {
            // update
            var stock_item = $('.stock_item').val();
            var unit = $('#stock_unit').html();
            var quantity = $('#quantity').val();
            var warehouse = $('#warehouse').val();
            var date = $(".date").val();
            var rate = $("#rate").val();
            var batch = $('#batch').val();

            var rowCount = $('.table .addrow tr').length;
            if (warehouse == '' || date == '' || stock_item == '' || quantity == 0 || unit == '' || rate == 0) {
                $().toastmessage('showErrorToast', "Please enter warehouse stock details");
            } else {

                if ((moment(date, 'DD-MM-YYYY', true).isValid())) {
                    howMany += 1;
                    if (howMany == 1) {
                        var stock_id = $("#warehousestock_id").val();
                        var data = {
                            'sl_no': rowCount,
                            'quantity': quantity,
                            'batch': batch,
                            'rate': rate,
                            'stock_item': stock_item,
                            'unit': unit,
                            'stock_id': stock_id,
                            'warehouse': warehouse,
                            'date': date
                        };
                        $.ajax({
                            url: '<?php echo Yii::app()->createAbsoluteUrl('wh/warehousestock/savewarehousestock'); ?>',
                            type: 'GET',
                            data: {
                                data: data
                            },
                            success: function(response) {
                                if (response == 1) {
                                    $().toastmessage('showErrorToast', "Sorry some problem occured");
                                } else if (response == 2) {
                                    $().toastmessage('showErrorToast', "Warehouse stock already exists!");
                                } else {
                                    $().toastmessage('showSuccessToast', "Warehouse stock updated successfully");
                                    $("#newlist").html(response);
                                    $("#myTable").dataTable({});
                                }
                                howMany = 0;
                                $('#stock_item').val('').trigger('change');
                                var quantity = $('#quantity').val(0);
                                $('#stock_unit').text('');
                                $('#batch').val('');
                                $('#rate').val(0);
                                $('#amount').val(0);
                                $('#stock_item').select2('focus');
                                $("#warehousestock_id").val(0);
                            }
                        });

                        $('.js-example-basic-single').select2('focus');
                    }
                    //}
                } else {
                    $(this).focus();
                    $().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
                    $(this).focus();
                }
            }
        }
    });





    $(document).on('click', '.edit_item', function(e) {
        $("#warehousestock .collapse").addClass("in");
        $(".stock").removeClass("collapsed");
        if ($(".collapse").css('display') == 'visible') {
            $(".collapse").css({
                "height": "auto"
            });
        } else {
            $(".collapse").css({
                "height": ""
            });
        }
        e.preventDefault();
        var item_id = $(this).attr('data-id');
        $('#warehousestock_id').val(item_id);
        var $tds = $(this).closest('tr').find('td');
        var unit = $tds.eq(3).text();
        var date = $tds.eq(4).text();
        var quantity = $tds.eq(5).text();
        $abc = $(this).closest('tr').find('.item_description').attr('id');
        var warehouseid = $(this).closest('tr').find('.warehouseid').attr('id');
        var itemid = $(this).closest('tr').find('.itemid').attr('id');
        <?php $abc; ?>
        $('#warehouse').val(warehouseid).trigger('change');
        $('#stock_item').val(itemid).trigger('change');
        $('#quantity').val(parseFloat(quantity));
        $('#stock_unit').text(unit);
        $('#datepicker').val(date);
        $('#warehousestock_id').val(item_id);
        $(".item_save").attr('value', 'Update');
        $(".item_save").attr('id', item_id);
        $(".item_save").attr('id', item_id);
    });


    $('.item_save').keypress(function(e) {
        if (e.keyCode == 13) {
            $('.item_save').click();
        }
    });


    $(document).on('click', '.deletestock', function() {
        var answer = confirm("Are you sure you want to delete?");
        if (answer) {
            var id = $(this).attr('data-id');
            $.ajax({
                type: "POST",
                url: "<?php echo $this->createUrl('wh/warehousestock/delete&id=') ?>" + id,
                success: function(result) {
                    $("#warehousestock").trigger("reset");
                    $("#warehouse").val('').trigger('change');
                    $("#stock_item").val('').trigger('change');
                    $("#addclient").slideUp();
                    if (result == 1) {

                    } else {
                        $("#newlist").html(result);
                        $("#myTable").dataTable({});
                    }
                }
            });
        } else {
            return false;
        }
    });
    $("[data-toggle='collapse']").click(function() {
        $("#warehouse").val("").trigger('change.select2');
        $("#stock_item").val("").trigger('change.select2');
        $("#quantity").val("");
        $("#stock_unit").text("");
        $("#datepicker").val("");
        $("#datepicker").datepicker({
            maxDate: new Date(),
            dateFormat: 'dd-mm-yy'
        }).datepicker("setDate", new Date());
        $(".item_save").attr("id", "0").val("Save");
    });
</script>

<style>
    .table-scroll {
        position: relative;
        max-width: 1280px;
        width: 100%;
        margin: auto;
        display: table;
    }

    .table-wrap {
        width: 100%;
        display: block;
        overflow: auto;
        position: relative;
        z-index: 1;
        border: 1px solid #ddd;
    }

    .table-wrap.fixedON,
    .table-wrap.fixedON table,
    .faux-table table {
        height: 380px;
        /* match heights*/
    }

    .table-scroll table {
        width: 100%;
        margin: auto;
        border-collapse: separate;
        border-spacing: 0;
        border: 1px solid #ddd;
    }

    .table-scroll th,
    .table-scroll td {
        padding: 5px 10px;
        border: 1px solid #ddd;
        background: #fff;
        vertical-align: top;
    }

    .faux-table table {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        pointer-events: none;
    }

    .faux-table table tbody {
        visibility: hidden;
    }

    /* shrink cells in cloned table so that the table height is exactly 300px so that the header and footer appear fixed */
    .faux-table table tbody th,
    .faux-table table tbody td {
        padding-top: 0;
        padding-bottom: 0;
        border-top: none;
        border-bottom: none;
        line-height: 0.1;
    }

    .faux-table table tbody tr+tr th,
    .faux-table tbody tr+tr td {
        line-height: 0;
    }

    .faux-table thead th,
    .faux-table tfoot th,
    .faux-table tfoot td,
    .table-wrap thead th,
    .table-wrap tfoot th,
    .table-wrap tfoot td {
        background: #eee;
    }

    .faux-table {
        position: absolute;
        top: 0;
        right: 0;
        left: 0;
        bottom: 0;
        overflow-y: scroll;
    }

    .faux-table thead,
    .faux-table tfoot,
    .faux-table thead th,
    .faux-table tfoot th,
    .faux-table tfoot td {
        position: relative;
        z-index: 2;
    }

    /* ie bug */
    .table-scroll table thead tr,
    .table-scroll table thead tr th,
    .table-scroll table tfoot tr,
    .table-scroll table tfoot tr th,
    .table-scroll table tfoot tr td {
        height: 1px;
    }
</style>
<?php
Yii::app()->clientScript->registerScript('myjavascript', '
     $(document).ready(function(){
     ajaxdatatable();
    function ajaxdatatable(){
       $("#myTable").dataTable( {
	  } );
     }

	});


    ');
?>
<style>
    .tooltip-hiden {
        width: auto
    }

    .dataTables_wrapper.no-footer .dataTables_scrollBody {
        border-bottom: 0px;
    }
</style>