<?php

/* --- money format issue fix for windows   */
if (!function_exists('money_format')) {

    function money_format($a, $b) {
        return number_format($b, 2);
    }

}

/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController {

    /**
     * @var string the default layout for the controller view. Defaults to '//layouts/column1',
     * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
     */
    public $layout = '//layouts/column1';

    /**
     * @var array context menu items. This property will be assigned to {@link CMenu::items}.
     */
    public $menu = array();

    /**
     * @var array the breadcrumbs of the current page. The value of this property will
     * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
     * for more details on how to specify this property.
     */
    public $breadcrumbs = array();
    public $logo = '';
    public $favicon = '';
    public $realpath_logo = '';

    public function __construct($id, $module = null) {

        if( !Yii::app()->user->isGuest && !isset(Yii::app()->user->menuauth)){
            $this->menuPermissionSet();
        }
    
        parent::__construct($id, $module);
        $this->getLogoAndFavicon();
    }

    public function menuPermissionSet(){
        $tbl = Yii::app()->db->tablePrefix;

        // set access rules in session for all users
        $user_id = Yii::app()->user->id;
        $session = new CHttpSession;
        $session->open();

        Yii::app()->user->setState("menuall",array());
        Yii::app()->user->setState("menualllist",array());
        Yii::app()->user->setState("menuauth",array());
        Yii::app()->user->setState("menuauthlist",array());

        
        //$controller =  Yii::app()->controller->id;
        $sql = "SELECT distinct(controller) as controller 
        FROM {$tbl}menu WHERE status = 0 
        AND showmenu = 0 AND parent_id = 0";
        $ctrls = Yii::app()->db->createCommand($sql)->queryAll();
        
        $newarr1 = array();
        $newarr3 = array();
        $menualllist = array();
        $menuauthlist = array();
        if (!empty($ctrls)) {


            $i = 0;
            foreach ($ctrls as $ctrl) {
                $controller = $ctrl['controller'];
                $menuitems_all = Yii::app()->db->createCommand(
                    "SELECT * FROM {$tbl}menu WHERE status = 0 
                    AND showmenu = 0 AND parent_id != 0 
                    AND controller = '$controller'")->queryAll();


                //$controller = lcfirst($controller);

                foreach ($menuitems_all as $menuall) {

                    $ctrller = $menuall['controller'];
                    $action = $menuall['action'];
                   // echo $ctrller."--".$action."<br>";
                    // if($ctrller == $controller){
                    $controller = lcfirst($ctrller);
                    $action = lcfirst($action);
                    $newarr1[$controller][$i] = $action;

                    $menuitem = '/' . $controller . '/' . $action;
                    array_push($menualllist, $menuitem);

                    // }
                    $i++;
                }
            }
            //        echo "<pre>";
            //        print_r($menualllist);die;

        }
        // $session['menuall'] = $newarr1;
        // $session['menualllist'] = $menualllist;
        // set access rules in session for all users \\//\\//\\//


        // set access rules for authenticated users
        $session = new CHttpSession;
        $session->open();

        $user_id = Yii::app()->user->id;

        //$controller =  Yii::app()->controller->id;
        $sql = "SELECT distinct(controller) as controller 
        FROM {$tbl}menu 
        WHERE status = 0 
        AND showmenu = 0 
        AND parent_id = 0";

        $ctrls = Yii::app()->db->createCommand($sql)->queryAll();
        //echo "<pre>" ;print_r($ctrls);exit;


        if (!empty($ctrls)) {

            $i = 0;

            foreach ($ctrls as $ctrl) {

                $controller = $ctrl['controller'];

                $sql="SELECT * FROM {$tbl}menu_permissions mp 
                LEFT JOIN {$tbl}menu me ON 
                mp.menu_id = me.menu_id  
                WHERE mp.user_id = '$user_id' 
                AND me.status = 0 AND me.showmenu = 2 
                AND me.parent_id != 0 
                AND me.controller = '$controller'";
                $menuitems_auth = Yii::app()->db->createCommand(
                    $sql)->queryAll();

                $actionArr  = array();
                foreach ($menuitems_auth as $menuall) {

                    $ctrller = $menuall['controller'];
                    $action = $menuall['action'];
                    //echo $ctrller."--".$action."<br>";
                    //if($ctrller == $controller){
                    $controller = lcfirst($controller);
                    $action = lcfirst($action);
                    $newarr3[$controller][$i] = $action;



                    $menuitem = '/' . $controller . '/' . $action;
                    array_push($menuauthlist, $menuitem);

                    $i++;
                    //}

                }
            }

            //echo "<pre>";          
            //print_r($menuauthlist);
            //echo "</pre>"; die;


        }
        // set access rules fro authenticated users \\//\\//\\//\\//


        Yii::app()->user->setState("menuauthlist", $menuauthlist);
        Yii::app()->user->setState("menuauth", $newarr3);
        Yii::app()->user->setState("menuall", $newarr1);
        Yii::app()->user->setState("menualllist", $menualllist);
    }

    public function getLogoAndFavicon() {
        $app_root = YiiBase::getPathOfAlias('webroot');
        $site_hosturl = str_replace('/index.php','', Yii::app()->createAbsoluteUrl('/'));
        $theme_asset_url = $site_hosturl."/themes/assets/" ;
        $real_theme_asset_url = realpath(Yii::app()->basePath . "/../themes/assets/"); 

        $this->logo = $theme_asset_url . 'company-logo.png';
        $this->realpath_logo =$real_theme_asset_url . '/company-logo.png';    

        if (file_exists($app_root . "/themes/assets/logo.png")) {
            $this->logo = $theme_asset_url . 'logo.png';
            $this->realpath_logo =$real_theme_asset_url . '/logo.png'; 
        }

        $this->favicon = $theme_asset_url . 'default-favicon.ico';
        if (file_exists($app_root . "/themes/assets/favicon.ico")) {
            $this->favicon = $theme_asset_url . 'favicon.ico';
        }

        return array('logo' => $this->logo, 'favicon' => $this->favicon,'realpath_logo'=>$this->realpath_logo);
    }

    public static function money_format_inr($input = 0, $decimals = 0, $opt1 = "", $opt2 = "") {
        //$input = round($input);

        if ($input == '' || $input == NULL) {
            $input = 0;
        }

        $input = $input;
        $decimals = intval($decimals);

        $dec = "";
        $negative_sign = "";

        $negative_pos = strpos($input, "-");

        if ($negative_pos === FALSE) {
            $negative_sign = "";
        } else {
            $negative_sign = "-";
            $input = str_replace("-", "", $input);
        }

        $pos = strpos($input, ".");
        if ($pos === FALSE) {
            //no decimals
            $dec = "00";
        } else {
            //decimals
            $dec = substr(round(substr($input, $pos), 2), 1);
            //$dec   = substr(substr($input, $pos), 2, 1);
            $input = substr($input, 0, $pos);
        }
        $num = substr($input, -3);    // get the last 3 digits
        $input = substr($input, 0, -3); // omit the last 3 digits already stored in $num
        // loop the process - further get digits 2 by 2
        while (strlen($input) > 0) {
            $num = substr($input, -2) . "," . $num;
            $input = substr($input, 0, -2);
        }
        // return $num.str_replace("0.",".",number_format($dec,2));

        $dec = floatval($dec);

        if ($opt1 == 1) {
            return $num . ltrim(number_format($dec, $decimals, ".", ""), "0");
        } else {
            return $negative_sign . $num . ltrim(number_format($dec, $decimals, ".", ""), "0");
        }
    }

    public function sendSMTPMail($maildata) {
        try {
            $mail = new JPhpMailer(true); // create a new object
            $mail->IsSMTP(); // enable SMTP
            // $mail->SMTPDebug = 2; // debugging: 1 = errors and messages, 2 = messages only
            $mail->SMTPAuth = SMTP_AUTH; // authentication enabled
            $mail->SMTPSecure = SMTP_SECURE; // secure transfer enabled REQUIRED for Gmail
            $mail->Host = SMTP_HOST;
            $mail->Port = SMTP_PORT; // or 587
            $mail->IsHTML(true);
            $mail->Username = SMTP_USERNAME;
            $mail->Password = SMTP_PASSWORD;
            $settings_model = GeneralSettings::model()->findByPk(1);
            $email_from = EMAILFROM;
            if (!empty($settings_model['po_email_from'])) {
                $email_from = $settings_model['po_email_from'];
                if (isset($maildata['send_copy_status'])) {
                    if ($maildata['send_copy_status'] == 1)
                        $mail->AddCC($email_from);
                }
            }
            $mail->SetFrom($email_from, Yii::app()->name);
            $mail->Subject = $maildata['subject'];
            $mail->Body = $maildata['message'];
            $mail->AddAddress($maildata['mail_to']);

            if (isset($maildata['attachment'])) {
                foreach ($maildata['attachment'] as $attachement) {
                    $mail->AddAttachment($attachement[0], $attachement[1]);
                }
            }
            $mail->send();
            $result = array('status' => 1, 'message' => 'Mail sent successfully');
        } catch (phpmailerException $e) {
            $result = array('status' => 0, 'message' => $e->getMessage());
        } finally {
            return $result;
        }
    }

    public function tableNameAcc($name, $isModelName = true) {

        if ($isModelName == true) {
            return $name::model()->tableSchema->rawName;
        } else {
            $tbl_prefix = Yii::app()->db->tablePrefix;
            return $tbl_prefix . $name;
        }
    }

    public function tableName($model_name) {
        return $model_name::model()->tableSchema->name;
    }

    public function appDate($format = 'Y-m-d H:i:s') {
        if (!defined('APP_DATE') or APP_DATE == 'actual_system_date') {
            return date($format);
        } else {
            return date($format, strtotime(APP_DATE));
        }
    }

    public function getInvoiceNumber() {
        $month = date('m');
        $months = array("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L");
        $prefix = '';
        $invoice_no = '';
        $i = 1;
        foreach ($months as $key => $value) {
            if ($month == $i) {
                $prefix = $value;
            }
            $i++;
        }
        $maxData = Yii::app()->db->createCommand("SELECT MAX(CAST(invoice_id as signed)) as largenumber FROM " . $this->tableName('Invoice') . " WHERE MONTH(date) = MONTH(CURRENT_DATE())
        AND YEAR(date) = YEAR(CURRENT_DATE())")->queryRow();
        $maxNo = $maxData["largenumber"];
        $newPONo = $maxNo + 1;

        $prNo = $newPONo;

        $digit = strlen((string) $prNo);
        //$this->countDigit($prNo);
        if ($digit == 1) {
            $invoice_no = $prefix . '000' . $prNo;
        } else if ($digit == 2) {
            $invoice_no = $prefix . '00' . $prNo;
        } else {
            $invoice_no = $prefix . $prNo;
        }
        return $invoice_no;
    }

    public function GetItemCategory() {
        $user_tree_array = array();
        $data = array();
        $final = array();
        $tblpx = Yii::app()->db->tablePrefix;
        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        $newQuery = "";
        foreach ($arrVal as $arr) {
            if ($newQuery)
                $newQuery .= ' OR';
            $newQuery .= " FIND_IN_SET('" . $arr . "', company_id)";
        }
        $specsql = "SELECT id, specification, brand_id, cat_id, sub_cat_id "
                . " FROM {$tblpx}specification "
                . " WHERE  (" . $newQuery . ") AND spec_status = '1'";
        $specification = Yii::app()->db->createCommand($specsql)->queryAll();
        foreach ($specification as $key => $value) {
            $brand = '';
            $category='';
            if ($value['brand_id'] != NULL) {
                $sql = "SELECT brand_name FROM {$tblpx}brand WHERE id=" . $value['brand_id'];
                $brand_details = Yii::app()->db->createCommand($sql)->queryRow();
                $brand = '-' . ucwords($brand_details['brand_name']);
            }
            if($value['cat_id'] != ""){
                $result = $this->GetParent($value['cat_id']);
                $final[$key]['data'] = ucwords($result['category_name']) . $brand . '-' . ucwords($value['specification']);
                $final[$key]['id'] = $value['id'];
                $category=ucwords($result['category_name']);
            }
            if($value['sub_cat_id'] != ""){
                $result = $this->actionGetSubcategory($value['sub_cat_id']);
                $final[$key]['data'] = ucwords($category).'-'. ucwords($result['sub_category_name']) . $brand . '-' . ucwords($value['specification']);
                $final[$key]['id'] = $value['id'];
            }   
            
        }
        return $final;
    }

    public function GetParent($id) {
        $tblpx = Yii::app()->db->tablePrefix;
        $category = Yii::app()->db->createCommand("SELECT parent_id, id , category_name FROM {$tblpx}purchase_category WHERE id=" . $id . "")->queryRow();
        return $category;
    }
    public function actionGetSubcategory($id){
        $tblpx = Yii::app()->db->tablePrefix;
        $subcategory = Yii::app()->db->createCommand("SELECT  id , sub_category_name FROM {$tblpx}purchase_sub_category WHERE id=" . $id . "")->queryRow();
        return $subcategory;
    }

    public function saveExpenseNotifications($project, $expense_percentages, $expense_amount) {
        $expense_percentage = 0;
        $result = array();
        if (!empty($project) && !empty($expense_percentages)) {
            $expense_total = (int) ($project['tot_expense'] + $expense_amount);
            $advance_total = (int) $project['tot_receipt'];
            if (!empty($advance_total) && $advance_total != 0)
                $expense_percentage = round(($expense_total / $advance_total) * 100);

            $percentage_array = array();
            foreach ($expense_percentages as $percentage) {
                if ($expense_percentage >= $percentage) {
                    $percentage_array[] = $percentage;
                }
            }
            if (!empty($percentage_array)) {
                $percentage = max($percentage_array);
                $check_exist = ExpenseNotification::model()->findByAttributes(array('project_id' => $project->pid, 'expense_perc' => $percentage));
                if (empty($check_exist)) {
                    $expense_notification_model = new ExpenseNotification();
                    $expense_notification_model->project_id = $project->pid;
                    $expense_notification_model->expense_perc = $percentage;
                    $expense_notification_model->expense_amount = $expense_total;
                    $expense_notification_model->advance_amount = $advance_total;
                    $expense_notification_model->generated_date = date('Y-m-d');
                    $expense_notification_model->view_status = 1;
                    $balance = $advance_total - $expense_total;
                    if ($expense_notification_model->save()) {
                        $result['expense_alert_data'] = array('expense_perc' => $percentage, 'expense_amount' => $expense_total, 'advance_amount' => $advance_total, 'balance' => $balance);
                    }
                }
            }
            if ($expense_total > $advance_total) {
                $check_exist = ExpenseNotification::model()->findByAttributes(array('project_id' => $project['pid'], 'expense_perc' => NULL, 'expense_amount' => $expense_total));
                if (empty($check_exist)) {
                    $expense_exceed_model = new ExpenseNotification();
                    $expense_exceed_model->project_id = $project['pid'];
                    $expense_exceed_model->expense_amount = $expense_total;
                    $expense_exceed_model->advance_amount = $advance_total;
                    $expense_exceed_model->generated_date = date('Y-m-d');
                    $expense_exceed_model->view_status = 1;
                    $balance = $advance_total - $expense_total;
                    if ($expense_exceed_model->save()) {
                        $result['expense_advance_alert_data'] = array('expense_amount' => $expense_total, 'advance_amount' => $advance_total, 'balance' => $balance);
                    }
                }
            }
        }
        return $result;
    }

    public function projectexpensealert($allocated_budget, $last_id, $type) {

        $tblpx = Yii::app()->db->tablePrefix;
        if ($type == 'daybook') {
            $query = Expenses::model()->findByPk($last_id);
            $subject = "" . Yii::app()->name . " :Daybook Payment Alert!";
        } elseif ($type == 'dailyvendors') {
            $query = Dailyvendors::model()->findByPk($last_id);
            $subject = "Vendor Payment Alert!";
        } elseif ($type = "subcontractor_payment") {
            $query = SubcontractorPayment::model()->find(array("condition" => "payment_id='$last_id'"));
            $subject = "Subcontractor Payment Alert!";
        }

        $company_model = Company::model()->findByPk($query['company_id']);
        $emails = explode(",", $company_model->expenses_email);

        $mail = new JPhpMailer();

        $headers = "" . Yii::app()->name . "";
        $bodyContent = "<p>Hi</p>";
        if (isset($allocated_budget['payment_alert'])) {
            extract($allocated_budget['payment_alert']);
            $project = Projects::model()->findByPk($project_id);
            $bodyContent .= "<p> Payment limit " . $percentage . "% exceed, Please find the details.</p>";
            $bodyContent .= "<p>Project : " . $project->name . "</p><p>Amount : " . $this->money_format_inr($amount) . "</p>";
            $bodyContent .= "<p>Profit Percentage : " . $profit_margin . "</p>";
            $bodyContent .= "<p>Project Quote : " . $project_quote . "</p>";
        }
        if (isset($allocated_budget['expense_advance_alert']['expense_alert_data'])) {
            extract($allocated_budget['expense_advance_alert']['expense_alert_data']);
            $bodyContent .= "<p><b>Expense reached " . $expense_perc . "% of advance,</b></p>";
            $bodyContent .= "<p>Expense Amount : " . $this->money_format_inr($expense_amount) . "</p>";
            $bodyContent .= "<p>Advance Amount : " . $this->money_format_inr($advance_amount) . "</p>";
            $bodyContent .= "<p>Balance Amount : " . $this->money_format_inr($balance) . "</p>";
        }
        if (isset($allocated_budget['expense_advance_alert']['expense_advance_alert_data'])) {
            extract($allocated_budget['expense_advance_alert']['expense_advance_alert_data']);
            $bodyContent .= "<p><b>Expense exceeds advance,</b></p>";
            $bodyContent .= "<p>Expense Amount : " . $this->money_format_inr($expense_amount) . "</p>";
            $bodyContent .= "<p>Advance Amount : " . $this->money_format_inr($advance_amount) . "</p>";
            $bodyContent .= "<p>Balance Amount : " . $this->money_format_inr($balance) . "</p>";
        }
        $bodyContent .= "<br><br><p>Regards,</p><p>" . Yii::app()->name . "</p>";
        $server = (($_SERVER['HTTP_HOST'] == 'localhost' || $_SERVER['HTTP_HOST'] == 'bhi.localhost.com') ? '0' : '1');
        if ($server == 0) {
            $mail->IsSMTP();
            $mail->Host = SMTP_HOST;
            $mail->SMTPSecure = SMTP_SECURE;
            $mail->SMTPAuth = true;
            $mail->Username = SMTP_USERNAME;
            $mail->Password = SMTP_PASSWORD;
        }
        $mail->setFrom(EMAILFROM, Yii::app()->name);
        if (!empty($emails)) {
            foreach ($emails as $key => $value) {
                if ($value != '') {
                    $mail->addAddress($value);
                }
            }
        } else {
            $user = Users::model()->findByPk(Yii::app()->user->id);
            $mail->addAddress($user['email']);
        }
        $mail->isHTML(true);
        $mail->Subject = $subject;
        $mail->Body = $bodyContent;
        if ($mail->Send()) {
            if (isset($allocated_budget['payment_alert'])) {
                if ($type == 'daybook') {
                    $update = Yii::app()->db->createCommand()->update($this->tableName('Expenses'), array('budget_percentage' => $percentage), 'exp_id=:exp_id', array(':exp_id' => $last_id));
                } elseif ($type == 'dailyvendors') {
                    $update = Yii::app()->db->createCommand()->update($this->tableName('Dailyvendors'), array('budget_percentage' => $percentage), 'daily_v_id=:daily_v_id', array(':daily_v_id' => $last_id));
                } elseif ($type == 'subcontractor_payment') {
                    $update = Yii::app()->db->createCommand()->update($this->tableName('SubcontractorPayment'), array('budget_percentage' => $percentage), 'payment_id=:payment_id', array(':payment_id' => $last_id));
                }
                $update2 = Yii::app()->db->createCommand()->update($this->tableName('Projects'), array('expense_percentage' => $percentage, 'expense_amount' => $amount), 'pid=:pid', array(':pid' => $project_id));
            }
        }
    }

    public function displaywords($number) {
        $no = (int) floor($number);
        $point = (int) round(($number - $no) * 100);
        $hundred = null;
        $digits_1 = strlen($no);
        $i = 0;
        $str = array();
        $words = array(
            '0' => '', '1' => 'one', '2' => 'two',
            '3' => 'three', '4' => 'four', '5' => 'five', '6' => 'six',
            '7' => 'seven', '8' => 'eight', '9' => 'nine',
            '10' => 'ten', '11' => 'eleven', '12' => 'twelve',
            '13' => 'thirteen', '14' => 'fourteen',
            '15' => 'fifteen', '16' => 'sixteen', '17' => 'seventeen',
            '18' => 'eighteen', '19' => 'nineteen', '20' => 'twenty',
            '30' => 'thirty', '40' => 'forty', '50' => 'fifty',
            '60' => 'sixty', '70' => 'seventy',
            '80' => 'eighty', '90' => 'ninety'
        );
        $digits = array('', 'hundred', 'thousand', 'lakh', 'crore');
        while ($i < $digits_1) {
            $divider = ($i == 2) ? 10 : 100;
            $number = floor($no % $divider);
            $no = floor($no / $divider);
            $i += ($divider == 10) ? 1 : 2;

            if ($number) {
                $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
                $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
                $str[] = ($number < 21) ? $words[$number] .
                        " " . $digits[$counter] . $plural . " " . $hundred :
                        $words[floor($number / 10) * 10]
                        . " " . $words[$number % 10] . " "
                        . $digits[$counter] . $plural . " " . $hundred;
            } else
                $str[] = null;
        }
        $str = array_reverse($str);
        $result = implode('', $str);

        if ($point > 20) {
            $points = ($point) ?
                    "" . $words[floor($point / 10) * 10] . " " .
                    $words[$point = $point % 10] : '';
        } else {
            $points = $words[$point];
        }
        if ($points != '') {
            echo "Rupees  " . $result . "and  " . $points . " Paise Only";
        } else {

            echo $result . "Rupees Only";
        }
    }

    public function getDisplaywords($number) {
        $no = (int) floor($number);
        $point = (int) round(($number - $no) * 100);
        $hundred = null;
        $digits_1 = strlen($no);
        $i = 0;
        $str = array();
        $words = array(
            '0' => '', '1' => 'one', '2' => 'two',
            '3' => 'three', '4' => 'four', '5' => 'five', '6' => 'six',
            '7' => 'seven', '8' => 'eight', '9' => 'nine',
            '10' => 'ten', '11' => 'eleven', '12' => 'twelve',
            '13' => 'thirteen', '14' => 'fourteen',
            '15' => 'fifteen', '16' => 'sixteen', '17' => 'seventeen',
            '18' => 'eighteen', '19' => 'nineteen', '20' => 'twenty',
            '30' => 'thirty', '40' => 'forty', '50' => 'fifty',
            '60' => 'sixty', '70' => 'seventy',
            '80' => 'eighty', '90' => 'ninety'
        );
        $digits = array('', 'hundred', 'thousand', 'lakh', 'crore');
        while ($i < $digits_1) {
            $divider = ($i == 2) ? 10 : 100;
            $number = floor($no % $divider);
            $no = floor($no / $divider);
            $i += ($divider == 10) ? 1 : 2;

            if ($number) {
                $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
                $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
                $str[] = ($number < 21) ? $words[$number] .
                        " " . $digits[$counter] . $plural . " " . $hundred :
                        $words[floor($number / 10) * 10]
                        . " " . $words[$number % 10] . " "
                        . $digits[$counter] . $plural . " " . $hundred;
            } else
                $str[] = null;
        }
        $str = array_reverse($str);
        $result = implode('', $str);

        if ($point > 20) {
            $points = ($point) ?
                    "" . $words[floor($point / 10) * 10] . " " .
                    $words[$point = $point % 10] : '';
        } else {
            $points = $words[$point];
        }
        if ($points != '') {
            return "Rupees  " . ucwords($result) . "and  " . ucwords($points) . " Paise Only";
        } else {

            return ucwords($result) . "Rupees Only";
        }
    }

    public function savePaymentAlert($data) {
        if (!empty($data)) {
            $check_exist = ExpenseNotification::model()->findByAttributes(array('project_id' => $data['project_id'], 'expense_perc' => $data['percentage'], 'expense_amount' => $data['amount'], 'notification_type' => 2));
            if (empty($check_exist)) {
                $notification_model = new ExpenseNotification();
                $notification_model->project_id = $data['project_id'];
                $notification_model->expense_perc = $data['percentage'];
                $notification_model->expense_amount = $data['amount'];
                $notification_model->advance_amount = 0;
                $notification_model->generated_date = date('Y-m-d');
                $notification_model->view_status = 1;
                $notification_model->notification_type = 2;
                $notification_model->save();
            }
        }
    }

    public static function changeDateForamt($date_value) {
        if (!empty($date_value)) {
            return date('d-m-Y', strtotime($date_value));
        } else {
            return '';
        }
    }

    public function getCategoryOptions($type = false) {
        $result_array = array();
        $project_name = '';
        $commom_warehouse = array();
        $project_warehouse = array();
        if (!empty($type)) {
            if ($type == 1) {
                $commom_warehouse = Warehouse::model()->findAll(['select' => array('warehouse_id', 'warehouse_name'), 'condition' => 'FIND_IN_SET(' . Yii::app()->user->id . ',assigned_to) AND project_id IS NULL']);

                $project_warehouse = Warehouse::model()->findAll(
                        array(
                            'select' => array('warehouse_id', 'warehouse_name', 'project_id'), 'condition' => 'FIND_IN_SET(' . Yii::app()->user->id . ',assigned_to) AND project_id IS NOT NULL',
                            'order' => 'warehouse_id DESC'
                        )
                );
                // echo '<pre>';print_r($project_warehouse);exit;
            }
        } else {
            $commom_warehouse = Warehouse::model()->findAll(
                    array(
                        'select' => array('warehouse_id', 'warehouse_name'), 'condition' => 'project_id IS NULL',
                        'order' => 'warehouse_id DESC'
                    )
            );
            $project_warehouse = Warehouse::model()->findAll(
                    array(
                        'select' => array('warehouse_id', 'warehouse_name', 'project_id'), 'condition' => 'project_id IS NOT NULL',
                        'order' => 'warehouse_id DESC'
                    )
            );
        }
        foreach ($commom_warehouse as $common) {
            $data = array('id' => $common['warehouse_id'], 'text' => $common['warehouse_name'], 'group' => 'General');
            array_push($result_array, $data);
        }
        foreach ($project_warehouse as $project_ware) {
            if (!empty($project_ware['project_id'])) {
                $project_model = Projects::model()->findByPk($project_ware['project_id']);
                if (!empty($project_model)) {
                    $project_name = $project_model['name'];
                }
            }

            $data = array('id' => $project_ware['warehouse_id'], 'text' => $project_ware['warehouse_name'], 'group' => $project_name);
            array_push($result_array, $data);
        }


        return $result_array;
    }

    public function getCategoryBillAndDespatch($transfer_type) {
        $transfer_type = $transfer_type;
        $result_array = array();
        $bill_number_arr = array();
        $despatch_number_arr = array();
        $bill_number_arr = Warehousereceipt::model()->findAll(
                array(
                    'select' => array('*'), 'condition' => 'warehousereceipt_despatch_id IS NULL AND warehousereceipt_transfer_type = 1',
                    'order' => 'warehousereceipt_id DESC'
                )
        );
        $despatch_number_arr = Warehousereceipt::model()->findAll(
                array(
                    'select' => array('*'), 'condition' => 'warehousereceipt_bill_id IS NULL AND warehousereceipt_transfer_type = 2',
                    'order' => 'warehousereceipt_id DESC'
                )
        );
        if ($transfer_type == 1 || $transfer_type == '') {

            foreach ($bill_number_arr as $purchase) {
                $bill_num = '';
                if (!empty($purchase['warehousereceipt_bill_id'])) {
                    $bill_model = Bills::model()->findByPk($purchase['warehousereceipt_bill_id']);

                    if (!empty($bill_model)) {
                        $bill_num = $bill_model['bill_number'];
                    } else {
                        $bill_num = '';
                    }
                }

                $data = array('id' => $purchase['warehousereceipt_bill_id'], 'text' => $bill_num, 'group' => 'Purchase');
                array_push($result_array, $data);
            }
        }
        if ($transfer_type == 2 || $transfer_type == '') {
            foreach ($despatch_number_arr as $despatch) {
                $despatch_num = '';
                if (!empty($despatch['warehousereceipt_despatch_id'])) {
                    $despatch_model = Warehousedespatch::model()->findByPk($despatch['warehousereceipt_despatch_id']);
                    if (!empty($despatch_model)) {
                        $despatch_num = $despatch_model['warehousedespatch_no'];
                    } else {
                        $despatch_num = '';
                    }
                }

                $data = array('id' => $despatch['warehousereceipt_despatch_id'], 'text' => $despatch_num, 'group' => 'Despatch');
                array_push($result_array, $data);
            }
        }


        return $result_array;
    }

    public function billWithOrWithoutPO($purchase_id) {
        $purchase_details = Purchase::model()->findByPk($purchase_id);
        $purchase_type = isset($purchase_details->type) ? $purchase_details->type : '';
        return $purchase_type;
    }

    public function generalSettingsForAutoReceiptToProjectWarehouse($settings) {
        $auto_receipt_to_projectWarehouse = GeneralSettings::model()->findByPk($settings);
        $auto_receipt_to_projectWarehouse_status = isset($auto_receipt_to_projectWarehouse["status"]) ? $auto_receipt_to_projectWarehouse["status"] : '';
        return $auto_receipt_to_projectWarehouse_status;
    }

    public function GetStockItemunit($category_id) {
        $category = Specification::model()->findByPk($category_id);
        $categoryUnit = $category["unit"];
        return $categoryUnit;
    }

    public function StockItemunitlist() {
        $final_list = array();
        $ItemUnitlist = Unit::model()->findAll(
                array("order" => "id")
        );
        $final_list = array();
        foreach ($ItemUnitlist as $key => $value) {
            $final_list[$key]['id'] = $value['id'];
            $final_list[$key]['unit_name'] = $value['unit_name'];
        }
        return $final_list;
    }

    public function generalSettingsForFunctionsActiveOrInactive($settings_id) {
        $active_or_inactive = GeneralSettings::model()->findByPk($settings_id);
        $active_or_inactive_status = isset($active_or_inactive["status"]) ? $active_or_inactive["status"] : '';
        return $active_or_inactive_status;
    }

    public function ItemunitID($unit_name) {
        $unit_name = trim($unit_name);
        $ItemUnit = Unit::model()->findAll(
                array("condition" => "unit_name ='" . $unit_name . "' OR unit_code ='" . $unit_name . "'", "order" => "id")
        );

        $ItemUnit_id = '';
        foreach ($ItemUnit as $key => $value) {
            $ItemUnit_id = $value["id"];
        }
        return $ItemUnit_id;
    }

    public function Stockcheckpriority($item_id, $base_unit) {
        $prior_units = [];
        $tblpx = Yii::app()->db->tablePrefix;

        $conArray = array('condition' => 'item_id = ' . $item_id . ' AND base_unit ="' . $base_unit . '" AND priority ="1"');
        $unitconversion_prior = UnitConversion::model()->findAll($conArray);
        if (count($unitconversion_prior) != 0) {
            foreach ($unitconversion_prior as $key => $value) {
                $prior_units['conversion_factor'] = $value['conversion_factor'];
                $prior_units['conversion_unit'] = $value['conversion_unit'];
            }
        }
        return $prior_units;
    }

    public function getUnits($purchase_itemid) {

        $html = [];
        $html['unit'] = '';
        $html['units'] = '';
        $html['status'] = '';
        $specification_sql = "SELECT id, cat_id, brand_id, specification, unit FROM " . $this->tableNameAcc('specification', 0) . " WHERE id=" . $purchase_itemid . "";
        $specification = Yii::app()->db->createCommand($specification_sql)->queryRow();
        if ($specification['unit'] != NULL) {
            $unit = $specification['unit'];
        } else {
            $unit = '';
        }
        if (!empty($unit)) {
            $base_unit = $unit;
        } else {
            $base_unit = '';
        }
        $html['base_unit'] = $base_unit;
        if ($base_unit != '') {
            $uc_item_count = UnitConversion::model()->countByAttributes(array(
                'item_id' => $purchase_itemid,
            ));
            if ($uc_item_count > 1) {
                $html['units'] = $this->Itemunitlist($purchase_itemid);
                $html['unit'] = $html['units'];
            } else {
                if (!empty($unit)) {
                    $html['unit'] .= $base_unit;

                    $base_unit_name = $base_unit;
                    $html['units'] = [0 => ['id' => $base_unit, 'value' => $base_unit_name]];
                } else {
                    $html['unit'] .= '';
                    $html['units'] = [0 => ['id' => '', 'value' => 'Unit']];
                }
            }
        } else {
            $html['unit'] .= '';
            $html['units'] = 'Unit';
        }
        $html['status'] .= true;
        return $html;
    }

    public function ItemunitCode($unit_id) {
        $unit_id = $unit_id;
        $ItemUnit = Unit::model()->findByPk($unit_id);
        if ($ItemUnit["unit_code"] != '') {
            $ItemUnit_code = $ItemUnit["unit_code"];
        } else {
            $ItemUnit_code = $ItemUnit["unit_name"];
        }

        return $ItemUnit_code;
    }

    public function ItemunitName($unit_id) {
        $unit_id = $unit_id;
        $ItemUnit = Unit::model()->findByPk($unit_id);
        $ItemUnit_name = $ItemUnit["unit_name"];
        return $ItemUnit_name;
    }

    public function autoReceiptToWarehouse($receipt_warehouse_to, $receipt_warehouse_from, $receipt_default_date, $receipt_vendor, $receipt_clerk, $receipt_no, $receipt_transfer_type, $bill_or_despatch_id) {
        $auto_receipt_to_warehouse = '';
        if ($bill_or_despatch_id != '' && $receipt_default_date != '' && $receipt_no != '' && $receipt_vendor != '' && $receipt_warehouse_to != '' && $receipt_clerk != '') {
            if ($receipt_transfer_type == 1) {
                $res = Warehousereceipt::model()->findAll(array('condition' => 'warehousereceipt_bill_id = "' . $bill_or_despatch_id . '" AND warehousereceipt_warehouseid = "' . $receipt_warehouse_to . '" AND warehousereceipt_warehouseid_from IS NULL AND warehousereceipt_transfer_type = "' . $receipt_transfer_type . '"'));
            } else {
                $res = Warehousereceipt::model()->findAll(array('condition' => 'warehousereceipt_bill_id = "' . $bill_or_despatch_id . '" AND warehousereceipt_warehouseid = "' . $receipt_warehouse_to . '" AND warehousereceipt_warehouseid_from = "' . $receipt_warehouse_from . '" AND warehousereceipt_transfer_type = "' . $receipt_transfer_type . '"'));
            }
            if (empty($res)) {
                $tblpx = Yii::app()->db->tablePrefix;
                $receipt_model = new Warehousereceipt;
                $receipt_model->warehousereceipt_warehouseid = $receipt_warehouse_to;
                $receipt_model->warehousereceipt_date = date('Y-m-d', strtotime(date('Y-m-d')));
                $receipt_model->warehousereceipt_no = $receipt_no;
                $receipt_model->warehousereceipt_remark = $receipt_vendor;
                $receipt_model->warehousereceipt_warehouseid_from = $receipt_warehouse_from;
                if ($receipt_transfer_type == 1) {
                    $receipt_model->warehousereceipt_bill_id = $bill_or_despatch_id;
                } else {
                    $receipt_model->warehousereceipt_despatch_id = $bill_or_despatch_id;
                }
                $receipt_model->warehousereceipt_clerk = $receipt_clerk;
                $receipt_model->warehousereceipt_transfer_type = $receipt_transfer_type;
                $receipt_model->company_id = Yii::app()->user->company_id;
                $receipt_model->updated_by = yii::app()->user->id;
                $receipt_model->updated_date = date('Y-m-d');
                $receipt_model->created_date = date('Y-m-d');
                $receipt_model->created_by = Yii::app()->user->id;
                if ($receipt_model->save(false)) {
                    $auto_receipt_id = $receipt_model->warehousereceipt_id;
                    if ($receipt_transfer_type == 1) {
                        $auto_receipt_bill_or_despatch_item_ids = Billitem::model()->findAll(
                                array(
                                    'select' => array('*'), 'condition' => 'bill_id ="' . $bill_or_despatch_id . '"',
                                    'order' => 'billitem_id DESC'
                                )
                        );
                        if (!empty($auto_receipt_bill_or_despatch_item_ids)) {
                            foreach ($auto_receipt_bill_or_despatch_item_ids as $value) {
                                $bill_or_despatch_item_id = $value['billitem_id'];
                                $auto_receipt_to_warehouse_item = $this->autoReceiptToWarehouseItem($auto_receipt_id, $bill_or_despatch_id, $receipt_transfer_type, $receipt_warehouse_to, $receipt_warehouse_from, $bill_or_despatch_item_id);
                            }
                            if ($auto_receipt_to_warehouse_item == "true") {
                                $auto_receipt_to_warehouse = "true";
                                return $auto_receipt_to_warehouse;
                            } else {
                                $auto_receipt_to_warehouse = "false";
                                return $auto_receipt_to_warehouse;
                            }
                        } else {
                            $auto_receipt_to_warehouse = "false";
                        }
                    } else {
                        $auto_receipt_bill_or_despatch_item_ids = WarehousedespatchItems::model()->findAll(
                                array(
                                    'select' => array('*'), 'condition' => 'warehousedespatch_id ="' . $bill_or_despatch_id . '"',
                                    'order' => 'item_id DESC'
                                )
                        );
                        if (!empty($auto_receipt_bill_or_despatch_item_ids)) {
                            foreach ($auto_receipt_bill_or_despatch_item_ids as $value) {
                                $bill_or_despatch_item_id = $value['item_id'];
                                $auto_receipt_to_warehouse_item = $this->autoReceiptToWarehouseItem($auto_receipt_id, $bill_or_despatch_id, $receipt_transfer_type, $receipt_warehouse_to, $receipt_warehouse_from, $bill_or_despatch_item_id);
                            }
                            if ($auto_receipt_to_warehouse_item == "true") {
                                $auto_receipt_to_warehouse = "true";
                                return $auto_receipt_to_warehouse;
                            } else {
                                $auto_receipt_to_warehouse = "false";
                                return $auto_receipt_to_warehouse;
                            }
                        } else {
                            $auto_receipt_to_warehouse = "false";
                        }
                    }
                } else {
                    $auto_receipt_to_warehouse = "false";
                    return $auto_receipt_to_warehouse;
                }
            } else {
                foreach ($res as $value) {
                    $auto_receipt_id = $value['warehousereceipt_id'];
                }
                if ($bill_or_despatch_id != '' && $receipt_default_date != '' && $receipt_no != '' && $receipt_vendor != '' && $receipt_warehouse_to != '' && $receipt_clerk != '') {
                    $tblpx = Yii::app()->db->tablePrefix;
                    $receipt_model = Warehousereceipt::model()->findByPk($auto_receipt_id);
                    $receipt_model->warehousereceipt_warehouseid = $receipt_warehouse_to;
                    $receipt_model->warehousereceipt_date = date('Y-m-d', strtotime($receipt_default_date));
                    $receipt_model->warehousereceipt_no = $receipt_no;
                    $receipt_model->warehousereceipt_remark = $receipt_vendor;
                    $receipt_model->warehousereceipt_warehouseid_from = $receipt_warehouse_from;
                    if ($receipt_transfer_type == 1) {
                        $receipt_model->warehousereceipt_bill_id = $bill_or_despatch_id;
                    } else {
                        $receipt_model->warehousereceipt_despatch_id = $bill_or_despatch_id;
                    }
                    $receipt_model->warehousereceipt_clerk = $receipt_clerk;
                    $receipt_model->warehousereceipt_transfer_type = $receipt_transfer_type;
                    $receipt_model->company_id = Yii::app()->user->company_id;
                    $receipt_model->updated_by = yii::app()->user->id;
                    $receipt_model->updated_date = date('Y-m-d');
                    $receipt_model->created_date = date('Y-m-d');
                    $receipt_model->created_by = Yii::app()->user->id;
                    if ($receipt_model->save(false)) {
                        $auto_receipt_id = $receipt_model->warehousereceipt_id;
                        if ($receipt_transfer_type == 1) {
                            $auto_receipt_bill_or_despatch_item_ids = Billitem::model()->findAll(
                                    array(
                                        'select' => array('*'), 'condition' => 'bill_id ="' . $bill_or_despatch_id . '"',
                                        'order' => 'billitem_id DESC'
                                    )
                            );

                            if (!empty($auto_receipt_bill_or_despatch_item_ids)) {
                                foreach ($auto_receipt_bill_or_despatch_item_ids as $value) {
                                    $bill_or_despatch_item_id = $value['billitem_id'];
                                    $auto_receipt_to_warehouse_item = $this->autoReceiptToWarehouseItem($auto_receipt_id, $bill_or_despatch_id, $receipt_transfer_type, $receipt_warehouse_to, $receipt_warehouse_from, $bill_or_despatch_item_id);
                                }
                                if ($auto_receipt_to_warehouse_item == "true") {
                                    $auto_receipt_to_warehouse = "true";
                                    return $auto_receipt_to_warehouse;
                                } else {
                                    $auto_receipt_to_warehouse = "false";
                                    return $auto_receipt_to_warehouse;
                                }
                            } else {
                                $auto_receipt_to_warehouse = "false";
                            }
                        } else {
                            $auto_receipt_bill_or_despatch_item_ids = WarehousedespatchItems::model()->findAll(
                                    array(
                                        'select' => array('*'), 'condition' => 'warehousedespatch_id ="' . $bill_or_despatch_id . '"',
                                        'order' => 'item_id DESC'
                                    )
                            );
                            if (!empty($auto_receipt_bill_or_despatch_item_ids)) {
                                foreach ($auto_receipt_bill_or_despatch_item_ids as $value) {
                                    $bill_or_despatch_item_id = $value['item_id'];
                                    $auto_receipt_to_warehouse_item = $this->autoReceiptToWarehouseItem($auto_receipt_id, $bill_or_despatch_id, $receipt_transfer_type, $receipt_warehouse_to, $receipt_warehouse_from, $bill_or_despatch_item_id);
                                }
                                if ($auto_receipt_to_warehouse_item == "true") {
                                    $auto_receipt_to_warehouse = "true";
                                    return $auto_receipt_to_warehouse;
                                } else {
                                    $auto_receipt_to_warehouse = "false";
                                    return $auto_receipt_to_warehouse;
                                }
                            } else {
                                $auto_receipt_to_warehouse = "false";
                            }
                        }
                    } else {
                        $auto_receipt_to_warehouse = "false";
                        return $auto_receipt_to_warehouse;
                    }
                } else {
                    $auto_receipt_to_warehouse = "false";
                    return $auto_receipt_to_warehouse;
                }
            }
        } else {
            $auto_receipt_to_warehouse = "false";
            return $auto_receipt_to_warehouse;
        }
        // return $ItemUnit_name;
    }

    public function autoReceiptToWarehouseItem($auto_receipt_id, $bill_or_despatch_id, $receipt_transfer_type, $receipt_warehouse_to, $receipt_warehouse_from, $bill_or_despatch_item_id) {
        $result['html'] = '';
        $warehouseStockId = '';
        $warehouse_receipt_item_id = '';
        $total_item_quantity = 0;
        $warehousereceipt_id = 0;
        $auto_receipt_to_warehouse_Item_saved_status = "";
        $tblpx = Yii::app()->db->tablePrefix;
        $transfer_type = isset($receipt_transfer_type) ? $receipt_transfer_type : NULL;
        $warehousereceipt_warehouseid = isset($receipt_warehouse_to) ? $receipt_warehouse_to : NULL;
        $warehousereceipt_warehouseid_from = isset($receipt_warehouse_from) ? $receipt_warehouse_from : NULL;
        $remaining_quantity = 0;
        if ($transfer_type == 1) {
            $transfer_type_name = "Billed ";
        } else {
            $transfer_type_name = "Despatched ";
        }
        $warehousereceipt_id = ($auto_receipt_id != '' || $auto_receipt_id != 0 || $auto_receipt_id != NULL) ? $auto_receipt_id : 0;
        if ($warehousereceipt_id == 0) {
            $auto_receipt_to_warehouse_Item_saved_status = "false";
            return $auto_receipt_to_warehouse_Item_saved_status;
        } else {
            $item_id = (isset($bill_or_despatch_item_id) && $bill_or_despatch_item_id != '') ? $bill_or_despatch_item_id : NULL;
            $transfer_type_item_id = (isset($bill_or_despatch_item_id) && $bill_or_despatch_item_id != '') ? $bill_or_despatch_item_id : NULL;
            if ($transfer_type == 1) {
                $bill_or_despatch_item_model_sql = "SELECT bi.*,pi.item_id,pi.purchase_id,pi.item_width,pi.item_height,pi.item_length,p.purchase_type
                                                            FROM " . $tblpx . "billitem as bi
                                                            LEFT JOIN " . $tblpx . "purchase_items as pi
                                                            ON bi.purchaseitem_id = pi.item_id
                                                            LEFT JOIN jp_purchase as p
                                                            ON pi.purchase_id = p.p_id
                                                            WHERE bi.bill_id ='" . $bill_or_despatch_id . "' AND bi.billitem_id ='" . $bill_or_despatch_item_id . "'";

                $bill_or_despatch_item_model = Yii::app()->db->createCommand($bill_or_despatch_item_model_sql)->queryRow();
                $stock_item = $bill_or_despatch_item_model['category_id'];
                $rate = $bill_or_despatch_item_model['billitem_rate'];
                $transferablequantity = $bill_or_despatch_item_model['billitem_quantity'];
                $item_unit_name = $bill_or_despatch_item_model['billitem_unit'];
                $batch = $bill_or_despatch_item_model['batch'];
                $receivedquantity = $bill_or_despatch_item_model['billitem_quantity'];
                $accepted_quantity = $bill_or_despatch_item_model['billitem_quantity'];
                $effective_quantity = ($bill_or_despatch_item_model['item_length'] != '') ? $bill_or_despatch_item_model['item_length'] : 1;
                $item_length = $bill_or_despatch_item_model['item_length'];
                $item_width = $bill_or_despatch_item_model['item_width'];
                $item_height = $bill_or_despatch_item_model['item_height'];
                if ($item_width != '' && $item_height != '' && $item_width != null && $item_height != null) {
                    $dimension = $item_width . " x " . $item_height;
                } elseif ($item_length != '' && $item_length != null) {
                    $dimension = null;
                } else {
                    $dimension = null;
                }
            } else {
                $bill_or_despatch_item_model = WarehousedespatchItems::model()->find(array("condition" => "warehousedespatch_id ='" . $bill_or_despatch_id . "' AND item_id ='" . $bill_or_despatch_item_id . "'"));
                $stock_item = $bill_or_despatch_item_model->warehousedespatch_itemid;
                $rate = $bill_or_despatch_item_model->warehousedespatch_rate;
                $transferablequantity = $bill_or_despatch_item_model->warehousedespatch_quantity;
                $item_unit_name = $bill_or_despatch_item_model->warehousedespatch_unit;
                $batch = $bill_or_despatch_item_model->warehousedespatch_batch;
                $receivedquantity = $bill_or_despatch_item_model->warehousedespatch_quantity;
                $accepted_quantity = $bill_or_despatch_item_model->warehousedespatch_quantity;
                $effective_quantity = 1;
                $item_length = null;
                $item_width = '';
                $item_height = '';
                $dimension = $bill_or_despatch_item_model->dimension;
            }
            $item_unit_conversion='';
            if (!empty($bill_or_despatch_item_model)) {
              
                $item_unit_id = $this->GetItemunitID($item_unit_name);
                if(empty($item_unit_id)){
                    $item_sp = Specification::model()->findByPK($stock_item);
                    if(!empty($item_sp)){
                         $item_unit_id =$item_sp->unit;
                    }
                }
                if(!empty( $item_unit_id)){
                    $item_unit_conversion_Array = array('condition' => 'item_id = ' . $stock_item . ' AND conversion_unit ="' . $item_unit_id.'"');
                    $item_unit_conversion = UnitConversion::model()->find($item_unit_conversion_Array);
                    

                }
                if (!empty($item_unit_conversion)) {
                        $item_base_unit = $item_unit_conversion->base_unit;
                } else {
                        $item_base_unit = $item_unit_id;
                }
                
                $rejected_quantity = 0;
                $jono = NULL;
                $autoreceipt_item_model = WarehousereceiptItems::model()->find(array("condition" => "warehousereceipt_id ='" . $auto_receipt_id . "' AND warehousereceipt_transfer_type_item_id ='" . $transfer_type_item_id . "'"));
                if (empty($autoreceipt_item_model)) {
                    if ($item_unit_id != $item_base_unit) {
                        $item_receivedquantity_UnitConversion = $this->unitConversionToBase($item_unit_id, $item_base_unit, $stock_item, $receivedquantity);
                        $item_transferablequantity_UnitConversion = $this->unitConversionToBase($item_unit_id, $item_base_unit, $stock_item, $transferablequantity);
                        $item_accepted_quantity_UnitConversion = $this->unitConversionToBase($item_unit_id, $item_base_unit, $stock_item, $accepted_quantity);
                        $item_rejected_quantity_UnitConversion = $this->unitConversionToBase($item_unit_id, $item_base_unit, $stock_item, $rejected_quantity);
                        $receivedquantity_conversion_factor = $item_receivedquantity_UnitConversion['item_conversion_factor'];
                        $receivedquantity_baseunit_quantity = $item_receivedquantity_UnitConversion['baseunit_quantity'];
                        $transferablequantity_conversion_factor = $item_transferablequantity_UnitConversion['item_conversion_factor'];
                        $transferablequantity_baseunit_quantity = $item_transferablequantity_UnitConversion['baseunit_quantity'];
                        $accepted_quantity_conversion_factor = $item_accepted_quantity_UnitConversion['item_conversion_factor'];
                        $accepted_quantity_baseunit_quantity = $item_accepted_quantity_UnitConversion['baseunit_quantity'];
                        $rejected_quantity_conversion_factor = $item_rejected_quantity_UnitConversion['item_conversion_factor'];
                        $rejected_quantity_baseunit_quantity = $item_rejected_quantity_UnitConversion['baseunit_quantity'];
                        $item_conversion_id = $item_receivedquantity_UnitConversion['item_conversion_id'];
                        if ($item_length != '' && $item_length != null) {
                            $effective_quantity_UnitConversion = $this->unitConversionToBase($item_unit_id, $item_base_unit, $stock_item, $effective_quantity);
                            $effective_quantity_conversion_factor = $effective_quantity_UnitConversion['item_conversion_factor'];
                            $effective_quantity_baseunit_quantity = $effective_quantity_UnitConversion['baseunit_quantity'];
                            $receivedquantity_conversion_factor = 1;
                            $receivedquantity_baseunit_quantity = $receivedquantity;
                            $transferablequantity_conversion_factor = 1;
                            $transferablequantity_baseunit_quantity = $transferablequantity;
                            $accepted_quantity_conversion_factor = 1;
                            $accepted_quantity_baseunit_quantity = $accepted_quantity;
                            $rejected_quantity_conversion_factor = 1;
                            $rejected_quantity_baseunit_quantity = $rejected_quantity;
                        } else {
                            $effective_quantity_conversion_factor = 1;
                            $effective_quantity_baseunit_quantity = 1;
                        }
                        $total_base_unit_quantity = $accepted_quantity_baseunit_quantity + $rejected_quantity_baseunit_quantity;
                        if ($transfer_type != 2) {
                            $base_rate = $accepted_quantity_conversion_factor * $rate;
                        } else {
                            $base_rate = $rate;
                        }
                    } else {
                        $conversion_factor = $transferablequantity_conversion_factor = $accepted_quantity_conversion_factor = $rejected_quantity_conversion_factor = 1;
                        $receivedquantity_baseunit_quantity = $receivedquantity;
                        $transferablequantity_baseunit_quantity = $transferablequantity;
                        $accepted_quantity_baseunit_quantity = $accepted_quantity;
                        $effective_quantity_baseunit_quantity = $effective_quantity;
                        $accepted_quantity_conversion_factor = 1;
                        $rejected_quantity_baseunit_quantity = $rejected_quantity;
                        $total_base_unit_quantity = $accepted_quantity_baseunit_quantity + $rejected_quantity_baseunit_quantity;
                        $item_receivedquantity_UnitConversion = $this->unitConversionToBase($item_unit_id, $item_base_unit, $stock_item, $receivedquantity);
                        $item_conversion_id = $item_receivedquantity_UnitConversion['item_conversion_id'];
                        $base_rate = $accepted_quantity_conversion_factor * $rate;
                    }
                    $warehousereceipt_accepted_effective_quantity = $accepted_quantity * $effective_quantity;
                    $warehousereceipt_baseunit_accepted_effective_quantity = $accepted_quantity_baseunit_quantity * $effective_quantity_baseunit_quantity;
                    $warehousereceipt_effective_quantity = $receivedquantity * $effective_quantity;
                    $warehousereceipt_baseunit_effective_quantity = $receivedquantity_baseunit_quantity * $effective_quantity_baseunit_quantity;
                    $warehousereceipt_rejected_effective_quantity = $rejected_quantity * $effective_quantity;
                    $warehousereceipt_baseunit_rejected_effective_quantity = $rejected_quantity_baseunit_quantity * $effective_quantity_baseunit_quantity;
                    $transferable_baseunit_effective_quantity = $transferablequantity_baseunit_quantity * $effective_quantity_baseunit_quantity;
                    $total_base_unit_effective_quantity = $total_base_unit_quantity * $effective_quantity_baseunit_quantity;

                    //echo $total_base_unit_quantity;
                    if ($transfer_type != "" && $transfer_type_item_id != NULL && $item_id != NULL) {
                        $used_quantity_check = Yii::app()->db->createCommand("SELECT SUM(wri.warehousereceipt_baseunit_effective_quantity) as warehousestock_total_quantity_received,
                                                                            SUM(wri.warehousereceipt_baseunit_accepted_effective_quantity) as warehousestock_total_accepted_quantity,
                                                                            SUM(wri.warehousereceipt_baseunit_rejected_effective_quantity) as warehousestock_total_rejected_quantity,
                                                                            wri.warehousereceipt_batch,wri.warehousereceipt_itemid,wr.warehousereceipt_transfer_type,wri.warehousereceipt_transfer_type_item_id
                                                                            FROM " . $tblpx . "warehousereceipt_items as wri
                                                                            INNER JOIN " . $tblpx . "warehousereceipt as wr 
                                                                            ON wri.warehousereceipt_id = wr.warehousereceipt_id
                                                                            WHERE wri.warehousereceipt_transfer_type_item_id='" . $item_id . "'")->queryRow();
                        $used_quantitycheck = isset($used_quantity_check['warehousestock_total_quantity_received']) ? $used_quantity_check['warehousestock_total_quantity_received'] : 0;
                        $used_quantity = $used_quantitycheck + $warehousereceipt_baseunit_effective_quantity;
                        $remaining_quantity = $transferable_baseunit_effective_quantity - $used_quantitycheck;
                    } else {
                        $used_quantity = $warehousereceipt_baseunit_effective_quantity;
                        $remaining_quantity = $transferable_baseunit_effective_quantity;
                    }

                    //echo $remaining_quantity;
                    if ($transferable_baseunit_effective_quantity > $used_quantity) {
                        $auto_receipt_to_warehouse_Item_saved_status = "false";
                    } else {
                        if ($transferable_baseunit_effective_quantity < $warehousereceipt_baseunit_effective_quantity) {
                            $auto_receipt_to_warehouse_Item_saved_status = "false";
                        } else {
                            if ($warehousereceipt_baseunit_effective_quantity < $total_base_unit_effective_quantity) {
                                $auto_receipt_to_warehouse_Item_saved_status = "false";
                            } else {

                                $item_model = new WarehousereceiptItems();
                                $item_model->warehousereceipt_id = $warehousereceipt_id;
                                $item_model->warehousereceipt_quantity = $receivedquantity;
                                $item_model->dimension = $dimension;
                                $item_model->length = $item_length;
                                $item_model->warehousereceipt_accepted_quantity = $accepted_quantity;
                                $item_model->warehousereceipt_accepted_effective_quantity = $warehousereceipt_accepted_effective_quantity;
                                $item_model->warehousereceipt_rejected_quantity = $rejected_quantity;
                                $item_model->warehousereceipt_baseunit_accepted_quantity = $accepted_quantity_baseunit_quantity;
                                $item_model->warehousereceipt_baseunit_accepted_effective_quantity = $warehousereceipt_baseunit_accepted_effective_quantity;
                                $item_model->warehousereceipt_baseunit_quantity = $receivedquantity_baseunit_quantity;
                                $item_model->warehousereceipt_baseunit_rejected_quantity = $rejected_quantity_baseunit_quantity;
                                $item_model->warehousereceipt_unitConversion_id = $item_conversion_id;
                                $item_model->warehousereceipt_effective_quantity = $warehousereceipt_effective_quantity;
                                $item_model->warehousereceipt_baseunit_effective_quantity = $warehousereceipt_baseunit_effective_quantity;
                                $item_model->warehousereceipt_rejected_effective_quantity = $warehousereceipt_rejected_effective_quantity;
                                $item_model->warehousereceipt_baseunit_rejected_effective_quantity = $warehousereceipt_baseunit_rejected_effective_quantity;
                                $item_model->warehousereceipt_rate = $rate;
                                $item_model->warehousereceipt_transfer_type_item_id = $item_id;
                                $item_model->warehousereceipt_unit = $item_unit_name;
                                $item_model->warehousereceipt_batch = $batch;
                                $item_model->warehousereceipt_jono = $jono;
                                $item_model->warehousereceipt_warehouseid = $warehousereceipt_warehouseid;
                                $item_model->warehousereceipt_itemid = $stock_item;
                                $item_model->warehousereceipt_itemqty = $accepted_quantity;
                                $item_model->warehousereceipt_itemrate = $rate;
                                $item_model->created_by = Yii::app()->user->id;
                                $item_model->created_date = date('Y-m-d');
                                $item_model->updated_by = Yii::app()->user->id;
                                $item_model->updated_date = date('Y-m-d');

                                if ($item_model->save()) {
                                    $warehouse_receipt_item_id = Yii::app()->db->getLastInsertID();
                                    $stock_sql = "SELECT warehousestock_stock_quantity,warehousestock_stock_quantity,rate,warehousestock_id,warehousestock_initial_quantity FROM {$tblpx}warehousestock WHERE warehousestock_warehouseid=" . $warehousereceipt_warehouseid . " AND warehousestock_itemid=" . $stock_item . " AND rate = $rate ";
                                    if ($batch == null) {
                                        $stock_sql .= " AND (batch IS Null OR batch ='" . $batch . "')";
                                    } else {
                                        $stock_sql .= "  AND batch = '" . $batch . "'";
                                    }
                                    if ($dimension == null) {
                                        $stock_sql .= " AND (dimension IS Null OR dimension ='" . $dimension . "')";
                                    } else {
                                        $stock_sql .= "  AND dimension LIKE '%" . $dimension . "%'";
                                    }
                                    // echo $stock_sql;exit();
                                    $stock = Yii::app()->db->createCommand($stock_sql)->queryRow();

                                    if (!empty($stock)) {

                                        $item_receipt_sql = "SELECT SUM(warehousereceipt_baseunit_accepted_effective_quantity)as quantity FROM {$tblpx}warehousereceipt_items WHERE warehousereceipt_warehouseid=" . $warehousereceipt_warehouseid . " AND warehousereceipt_itemid=" . $stock_item . " AND warehousereceipt_rate  = $rate ";
                                        $item_despatch_sql = "SELECT SUM(warehousedespatch_baseunit_quantity)as quantity FROM {$tblpx}warehousedespatch_items WHERE warehousedespatch_warehouseid=" . $warehousereceipt_warehouseid . " AND warehousedespatch_itemid=" . $stock_item . " AND warehousedespatch_rate = $rate ";
                                        if ($batch == null) {
                                            $item_receipt_sql .= " AND (warehousereceipt_batch IS Null OR warehousereceipt_batch ='" . $batch . "')";
                                            $item_despatch_sql .= " AND (warehousedespatch_batch IS Null OR warehousedespatch_batch ='" . $batch . "')";
                                        } else {
                                            $item_receipt_sql .= "  AND warehousereceipt_batch='" . $batch . "'";
                                            $item_despatch_sql .= "  AND warehousedespatch_batch='" . $batch . "'";
                                        }
                                        if ($dimension == null) {
                                            $item_receipt_sql .= " AND (dimension IS Null OR dimension ='" . $dimension . "')";
                                            $item_despatch_sql .= " AND (dimension IS Null OR dimension ='" . $dimension . "')";
                                        } else {
                                            $item_receipt_sql .= "  AND dimension LIKE '%" . $dimension . "%'";
                                            $item_despatch_sql .= "  AND dimension LIKE '%" . $dimension . "%'";
                                        }
                                        $item_receipt = Yii::app()->db->createCommand($item_receipt_sql)->queryRow();
                                        $item_despatch = Yii::app()->db->createCommand($item_despatch_sql)->queryRow();

                                        if ($item_receipt['quantity'] == "") {
                                            $item_receipt_quantity = 0;
                                        } else {
                                            $item_receipt_quantity = $item_receipt['quantity'];
                                        }
                                        if ($item_despatch['quantity'] == "") {
                                            $item_despatch_quantity = 0;
                                        } else {
                                            $item_despatch_quantity = $item_despatch['quantity'];
                                        }
                                        $final_quantity = ($stock['warehousestock_initial_quantity'] + $item_receipt_quantity) - $item_despatch_quantity;
                                        $model2 = Warehousestock::model()->findByPk($stock['warehousestock_id']);
                                        $model2->warehousestock_stock_quantity = $final_quantity;
                                        $model2->dimension = $dimension;
                                        $model2->rate = $base_rate;
                                        if ($model2->save(false)) {
                                            $auto_receipt_to_warehouse_Item_saved_status = "true";
                                        } else {
                                            $del = Yii::app()->db->createCommand()->delete($tblpx . 'warehousereceipt_items', 'item_id=:id', array(':id' => $warehouse_receipt_item_id));
                                            $auto_receipt_to_warehouse_Item_saved_status = "false";
                                        }
                                    } else {
                                        $item_dimension_category = Warehousestock::model()->countByAttributes(array(
                                            'warehousestock_warehouseid' => $warehousereceipt_warehouseid,
                                            'warehousestock_itemid' => $stock_item,
                                            'batch' => $batch,
                                            'rate' => $base_rate,
                                        ));
                                        $item_dimension_category_count = $item_dimension_category + 1;

                                        $model2 = new Warehousestock;
                                        $model2->warehousestock_warehouseid = $warehousereceipt_warehouseid;
                                        $model2->warehousestock_date = date('Y-m-d');
                                        $model2->warehousestock_itemid = $stock_item;
                                        $model2->warehousestock_itemid_dimension_category = $item_dimension_category_count;
                                        $model2->dimension = $dimension;
                                        $model2->warehousestock_stock_quantity = $warehousereceipt_baseunit_accepted_effective_quantity;
                                        $model2->rate = $base_rate;
                                        $model2->warehousereceipt_itemid = $stock_item;
                                        $model2->batch = $batch;
                                        
                                            $unit='';
                                            $unit=$item_base_unit;
                                            
                                        
                                            $unit_det=$this->GetItemunitName($item_base_unit);
                                            if(!empty($unit_det)){
                                                    $unit=$this->GetItemunitName($item_base_unit);
                                            }else{
                                                $item_sp = Specification::model()->findByPK($stock_item);
                                                if(!empty($item_sp)){
                                                    $unit=$item_sp->unit;
                                                }       

                                            }
                                            
                                            $model2->warehousestock_unit = $unit ;
                                        $model2->warehousestock_status = 1;
                                        $model2->created_by = yii::app()->user->id;
                                        $model2->created_date = date('Y-m-d');
                                        $model2->company_id = Yii::app()->user->company_id;
                                        $model2->updated_by = yii::app()->user->id;
                                        $model2->updated_date = date('Y-m-d');
                                        if ($model2->save(false)) {
                                            $auto_receipt_to_warehouse_Item_saved_status = "true";
                                        } else {
                                            $del = Yii::app()->db->createCommand()->delete($tblpx . 'warehousereceipt_items', 'item_id=:id', array(':id' => $warehouse_receipt_item_id));
                                            $auto_receipt_to_warehouse_Item_saved_status = "false";
                                        }
                                    }

                                    $item_quantity = Yii::app()->db->createCommand("SELECT SUM(warehousereceipt_baseunit_quantity) as quantity FROM {$tblpx}warehousereceipt_items WHERE warehousereceipt_id=" . $warehousereceipt_id . "")->queryScalar();
                                    $model = Warehousereceipt::model()->findByPk($warehousereceipt_id);
                                    $model->warehousereceipt_quantity = $item_quantity;
                                    $model->company_id = Yii::app()->user->company_id;
                                    if ($model->save(false)) {
                                        $auto_receipt_to_warehouse_Item_saved_status = "true";
                                    } else {
                                        $auto_receipt_to_warehouse_Item_saved_status = "false";
                                    }
                                }
                            }
                        }
                    }
                    return $auto_receipt_to_warehouse_Item_saved_status;
                } else {
                    $warehouse_receipt_item_id = $autoreceipt_item_model->item_id;
                    if ($item_unit_id != $item_base_unit) {
                        $item_receivedquantity_UnitConversion = $this->unitConversionToBase($item_unit_id, $item_base_unit, $stock_item, $receivedquantity);
                        $item_transferablequantity_UnitConversion = $this->unitConversionToBase($item_unit_id, $item_base_unit, $stock_item, $transferablequantity);
                        $item_accepted_quantity_UnitConversion = $this->unitConversionToBase($item_unit_id, $item_base_unit, $stock_item, $accepted_quantity);
                        $item_rejected_quantity_UnitConversion = $this->unitConversionToBase($item_unit_id, $item_base_unit, $stock_item, $rejected_quantity);

                        $receivedquantity_conversion_factor = $item_receivedquantity_UnitConversion['item_conversion_factor'];
                        $receivedquantity_baseunit_quantity = $item_receivedquantity_UnitConversion['baseunit_quantity'];
                        $transferablequantity_conversion_factor = $item_transferablequantity_UnitConversion['item_conversion_factor'];
                        $transferablequantity_baseunit_quantity = $item_transferablequantity_UnitConversion['baseunit_quantity'];
                        $accepted_quantity_conversion_factor = $item_accepted_quantity_UnitConversion['item_conversion_factor'];
                        $accepted_quantity_baseunit_quantity = $item_accepted_quantity_UnitConversion['baseunit_quantity'];
                        $rejected_quantity_conversion_factor = $item_rejected_quantity_UnitConversion['item_conversion_factor'];
                        $rejected_quantity_baseunit_quantity = $item_rejected_quantity_UnitConversion['baseunit_quantity'];
                        $item_conversion_id = $item_receivedquantity_UnitConversion['item_conversion_id'];
                        $total_base_unit_quantity = $accepted_quantity_baseunit_quantity + $rejected_quantity_baseunit_quantity;
                        if ($transfer_type != 2) {
                            $base_rate = $accepted_quantity_conversion_factor * $rate;
                        } else {
                            $base_rate = $rate;
                        }
                    } else {
                        $conversion_factor = $transferablequantity_conversion_factor = $accepted_quantity_conversion_factor = $rejected_quantity_conversion_factor = 1;
                        $receivedquantity_baseunit_quantity = $receivedquantity;
                        $transferablequantity_baseunit_quantity = $transferablequantity;
                        $accepted_quantity_baseunit_quantity = $accepted_quantity;
                        $accepted_quantity_conversion_factor = 1;
                        $rejected_quantity_baseunit_quantity = $rejected_quantity;
                        $total_base_unit_quantity = $accepted_quantity_baseunit_quantity + $rejected_quantity_baseunit_quantity;
                        $item_receivedquantity_UnitConversion = $this->unitConversionToBase($item_unit_id, $item_base_unit, $stock_item, $receivedquantity);
                        $item_conversion_id = $item_receivedquantity_UnitConversion['item_conversion_id'];
                        $base_rate = $accepted_quantity_conversion_factor * $rate;
                    }
                    if ($transfer_type != "" && $transfer_type_item_id != NULL && $item_id != NULL) {

                        $used_quantity_check = Yii::app()->db->createCommand("SELECT SUM(wri.warehousereceipt_baseunit_quantity) as warehousestock_total_quantity_received,
                                                                        SUM(wri.warehousereceipt_baseunit_accepted_effective_quantity) as warehousestock_total_accepted_quantity,
                                                                        SUM(wri.warehousereceipt_baseunit_rejected_quantity) as warehousestock_total_rejected_quantity,
                                                                        wri.warehousereceipt_batch,wri.warehousereceipt_itemid,wr.warehousereceipt_transfer_type,wri.warehousereceipt_transfer_type_item_id
                                                                        FROM " . $tblpx . "warehousereceipt_items as wri
                                                                        INNER JOIN " . $tblpx . "warehousereceipt as wr 
                                                                        ON wri.warehousereceipt_id = wr.warehousereceipt_id
                                                                        WHERE wri.warehousereceipt_transfer_type_item_id='" . $item_id . "'")->queryRow();
                        $used_quantitycheck = isset($used_quantity_check['warehousestock_total_quantity_received']) ? $used_quantity_check['warehousestock_total_quantity_received'] : 0;
                        $used_quantity = $used_quantitycheck + $receivedquantity_baseunit_quantity;
                    } else {
                        $used_quantity = $receivedquantity_baseunit_quantity;
                    }
                    if ($warehouse_receipt_item_id != "" || $warehouse_receipt_item_id != 0) {
                        $receipt = Warehousereceipt::model()->findByPk($warehousereceipt_id);
                        $item_model = WarehousereceiptItems::model()->findByPk($warehouse_receipt_item_id);
                        $previous_qty = $item_model->warehousereceipt_baseunit_quantity;
                        $used_quantity = $used_quantity - $previous_qty;
                        if ($transfer_type != "" && $transfer_type_item_id != NULL && $item_id != NULL) {
                            $remaining_quantity = ($transferablequantity_baseunit_quantity - $used_quantitycheck) + $previous_qty;
                        } else {
                            $remaining_quantity = $transferablequantity_baseunit_quantity + $previous_qty;
                        }

                        if ($transferablequantity_baseunit_quantity < $used_quantity) {
                            $auto_receipt_to_warehouse_Item_saved_status = "false";
                        } else {
                            if ($transferablequantity_baseunit_quantity < $receivedquantity_baseunit_quantity) {
                                $auto_receipt_to_warehouse_Item_saved_status = "false";
                            } else {
                                if ($receivedquantity_baseunit_quantity < $total_base_unit_quantity) {
                                    $auto_receipt_to_warehouse_Item_saved_status = "false";
                                } else {

                                    $item_model->warehousereceipt_id = $warehousereceipt_id;
                                    $item_model->warehousereceipt_quantity = $receivedquantity;
                                    $item_model->warehousereceipt_accepted_quantity = $accepted_quantity;
                                    $item_model->warehousereceipt_rejected_quantity = $rejected_quantity;
                                    $item_model->warehousereceipt_baseunit_accepted_quantity = $accepted_quantity_baseunit_quantity;
                                    $item_model->warehousereceipt_baseunit_quantity = $receivedquantity_baseunit_quantity;
                                    $item_model->warehousereceipt_baseunit_rejected_quantity = $rejected_quantity_baseunit_quantity;
                                    $item_model->warehousereceipt_unitConversion_id = $item_conversion_id;
                                    $item_model->warehousereceipt_rate = $rate;
                                    $item_model->warehousereceipt_transfer_type_item_id = $item_id;
                                    $item_model->warehousereceipt_unit = $item_unit_name;
                                    $item_model->warehousereceipt_batch = $batch;
                                    $item_model->warehousereceipt_jono = $jono;
                                    $item_model->warehousereceipt_warehouseid = $warehousereceipt_warehouseid;
                                    $item_model->warehousereceipt_itemid = $stock_item;
                                    $item_model->created_by = Yii::app()->user->id;
                                    $item_model->created_date = date('Y-m-d');
                                    $item_model->updated_by = Yii::app()->user->id;
                                    $item_model->updated_date = date('Y-m-d');

                                    if ($item_model->save()) {
                                        $stock_sql = "SELECT warehousestock_stock_quantity,warehousestock_stock_quantity,rate,warehousestock_id,warehousestock_initial_quantity FROM {$tblpx}warehousestock WHERE warehousestock_warehouseid=" . $warehousereceipt_warehouseid . " AND warehousestock_itemid=" . $stock_item . " AND rate = $rate ";
                                        if ($batch == null) {
                                            $stock_sql .= " AND (batch IS Null OR batch ='" . $batch . "')";
                                        } else {
                                            $stock_sql .= "  AND batch = '" . $batch . "'";
                                        }
                                        if ($dimension == null) {
                                            $stock_sql .= " AND (dimension IS Null OR dimension ='" . $dimension . "')";
                                        } else {
                                            $stock_sql .= "  AND dimension LIKE '%" . $dimension . "%'";
                                        }
                                        // echo $stock_sql;exit();
                                        $stock = Yii::app()->db->createCommand($stock_sql)->queryRow();
                                        if (!empty($stock)) {

                                            $item_receipt_sql = "SELECT SUM(warehousereceipt_baseunit_accepted_effective_quantity)as quantity FROM {$tblpx}warehousereceipt_items WHERE warehousereceipt_warehouseid=" . $warehousereceipt_warehouseid . " AND warehousereceipt_itemid=" . $stock_item . " AND warehousereceipt_rate = $rate ";
                                            $item_despatch_sql = "SELECT SUM(warehousedespatch_baseunit_quantity)as quantity FROM {$tblpx}warehousedespatch_items WHERE warehousedespatch_warehouseid=" . $warehousereceipt_warehouseid . " AND warehousedespatch_itemid=" . $stock_item . " AND warehousedespatch_rate = $rate ";
                                            if ($batch == null) {
                                                $item_receipt_sql .= " AND (warehousereceipt_batch IS Null OR warehousereceipt_batch ='" . $batch . "')";
                                                $item_despatch_sql .= " AND (warehousedespatch_batch IS Null OR warehousedespatch_batch ='" . $batch . "')";
                                            } else {
                                                $item_receipt_sql .= "  AND warehousereceipt_batch='" . $batch . "'";
                                                $item_despatch_sql .= "  AND warehousedespatch_batch='" . $batch . "'";
                                            }
                                            if ($dimension == null) {
                                                $item_receipt_sql .= " AND (dimension IS Null OR dimension ='" . $dimension . "')";
                                                $item_despatch_sql .= " AND (dimension IS Null OR dimension ='" . $dimension . "')";
                                            } else {
                                                $item_receipt_sql .= "  AND dimension LIKE '%" . $dimension . "%'";
                                                $item_despatch_sql .= "  AND dimension LIKE '%" . $dimension . "%'";
                                            }
                                            $item_receipt = Yii::app()->db->createCommand($item_receipt_sql)->queryRow();
                                            $item_despatch = Yii::app()->db->createCommand($item_despatch_sql)->queryRow();

                                            if ($item_receipt['quantity'] == "") {
                                                $item_receipt_quantity = 0;
                                            } else {
                                                $item_receipt_quantity = $item_receipt['quantity'];
                                            }
                                            if ($item_despatch['quantity'] == "") {
                                                $item_despatch_quantity = 0;
                                            } else {
                                                $item_despatch_quantity = $item_despatch['quantity'];
                                            }
                                            $final_quantity = ($stock['warehousestock_initial_quantity'] + $item_receipt_quantity) - $item_despatch_quantity;
                                            $model2 = Warehousestock::model()->findByPk($stock['warehousestock_id']);
                                            $model2->warehousestock_stock_quantity = $final_quantity;
                                            $model2->rate = $base_rate;
                                            if ($model2->save(false)) {
                                                $auto_receipt_to_warehouse_Item_saved_status = "true";
                                            } else {
                                                $auto_receipt_to_warehouse_Item_saved_status = "false";
                                                $del = Yii::app()->db->createCommand()->delete($tblpx . 'warehousereceipt_items', 'item_id=:id', array(':id' => $warehouse_receipt_item_id));
                                            }
                                        } else {
                                            $item_dimension_category = Warehousestock::model()->countByAttributes(array(
                                                'warehousestock_warehouseid' => $warehousereceipt_warehouseid,
                                                'warehousestock_itemid' => $stock_item,
                                                'batch' => $batch,
                                                'rate' => $base_rate,
                                            ));
                                            $item_dimension_category_count = $item_dimension_category + 1;
                                            $model2 = new Warehousestock;
                                            $model2->warehousestock_warehouseid = $warehousereceipt_warehouseid;
                                            $model2->warehousestock_date = date('Y-m-d');
                                            $model2->warehousestock_itemid = $stock_item;
                                            $model2->warehousestock_itemid_dimension_category = $item_dimension_category_count;
                                            $model2->warehousestock_stock_quantity = $accepted_quantity_baseunit_quantity;
                                            $model2->rate = $base_rate;
                                            $model2->batch = $batch;
                                            $unit='';
                                            $unit=$item_base_unit;
                                            
                                        
                                            $unit_det=$this->GetItemunitName($item_base_unit);
                                            if(!empty($unit_det)){
                                                    $unit=$this->GetItemunitName($item_base_unit);
                                            }else{
                                                $item_sp = Specification::model()->findByPK($stock_item);
                                                if(!empty($item_sp)){
                                                    $unit=$item_sp->unit;
                                                }       

                                            }
                                            
                                            $model2->warehousestock_unit = $unit ;
                                            $model2->warehousestock_status = 1;
                                            $model2->created_by = yii::app()->user->id;
                                            $model2->created_date = date('Y-m-d');
                                            $model2->company_id = Yii::app()->user->company_id;
                                            $model2->updated_by = yii::app()->user->id;
                                            $model2->updated_date = date('Y-m-d');
                                            if ($model2->save(false)) {
                                                $auto_receipt_to_warehouse_Item_saved_status = "true";
                                            } else {
                                                $auto_receipt_to_warehouse_Item_saved_status = "false";
                                                $del = Yii::app()->db->createCommand()->delete($tblpx . 'warehousereceipt_items', 'item_id=:id', array(':id' => $warehouse_receipt_item_id));
                                            }
                                        }

                                        $item_quantity = Yii::app()->db->createCommand("SELECT SUM(warehousereceipt_baseunit_quantity) as quantity FROM {$tblpx}warehousereceipt_items WHERE warehousereceipt_id=" . $warehousereceipt_id . "")->queryScalar();
                                        $model = Warehousereceipt::model()->findByPk($warehousereceipt_id);
                                        $model->warehousereceipt_quantity = $item_quantity;
                                        $model->company_id = Yii::app()->user->company_id;
                                        if ($model->save(false)) {
                                            $auto_receipt_to_warehouse_Item_saved_status = "true";
                                        } else {
                                            $auto_receipt_to_warehouse_Item_saved_status = "false";
                                        }
                                    }
                                }
                            }
                        }
                    }
                    return $auto_receipt_to_warehouse_Item_saved_status;
                }
                return $auto_receipt_to_warehouse_Item_saved_status;
            } else {
                return $auto_receipt_to_warehouse_Item_saved_status;
            }
        }
    }
    public function GetItemunitID($unit_name)
	{
		$unit_name = trim($unit_name);
		$ItemUnit = Unit::model()->findAll(
			array("condition" => "unit_name ='" . $unit_name . "' OR unit_code ='" . $unit_name . "'", "order" => "id")
		);
		$ItemUnit_id = 0;
		if(!empty($ItemUnit)){
			foreach ($ItemUnit as $key => $value) {
				$ItemUnit_id = $value["id"];
			}
		}
		
		return $ItemUnit_id;
	}

    public function inputval($index, $default = null, $method = 'post') {
        if ($method == 'post') {
            if (isset($_POST[$index])) {
                return $_POST[$index];
            }
        } else {

            if (isset($_REQUEST[$index])) {
                return $_REQUEST[$index];
            }
        }

        return $default;
    }

    public function autoReceiptWarehouseCheck($receipt_warehouse_to, $receipt_warehouse_from, $receipt_clerk) {
        $ids_array = array();
        $result = '';
        $receipt_warehouse_to_check = '';
        $receipt_warehouse_to_in_charge = '';
        $receipt_warehouse_to_assaign_to = '';
        $receipt_warehouse_to_assaign_to_arr = array();
        $receipt_warehouse_from_check = '';
        $receipt_warehouse_from_in_charge = '';
        $receipt_warehouse_from_assaign_to = '';
        $receipt_warehouse_from_assaign_to_arr = array();
        $warehouse_data = Warehouse::model()->findAll(
                array(
                    'select' => array('warehouse_id', 'warehouse_incharge', 'assigned_to'), 'condition' => 'project_id IS NOT NULL',
                    'order' => 'warehouse_id DESC'
                )
        );
        if (!empty($warehouse_ids)) {
            foreach ($warehouse_ids as $warehouse) {
                array_push($ids_array, $warehouse['warehouse_id']);
            }
        }
        if ($receipt_warehouse_from != '') {
            $receipt_warehouse_to_check = Warehouse::model()->findByPk($receipt_warehouse_to);
            $receipt_warehouse_to_in_charge = $receipt_warehouse_to_check['warehouse_incharge'];
            $receipt_warehouse_to_assaign_to = $receipt_warehouse_to_check['assigned_to'];
            $receipt_warehouse_to_assaign_to_arr = (explode(",", $receipt_warehouse_to_assaign_to));
            $receipt_warehouse_from_check = Warehouse::model()->findByPk($receipt_warehouse_from);
            $receipt_warehouse_from_in_charge = $receipt_warehouse_from_check['warehouse_incharge'];
            $receipt_warehouse_from_assaign_to = $receipt_warehouse_from_check['assigned_to'];
            $receipt_warehouse_from_assaign_to_arr = (explode(",", $receipt_warehouse_from_assaign_to));
            if ((($receipt_warehouse_to_in_charge == yii::app()->user->id) && ($receipt_warehouse_from_in_charge == yii::app()->user->id)) || (in_array(yii::app()->user->id, $receipt_warehouse_to_assaign_to_arr) && (in_array(yii::app()->user->id, $receipt_warehouse_from_assaign_to_arr))) ||($receipt_warehouse_to_in_charge == yii::app()->user->id) && (in_array(yii::app()->user->id, $receipt_warehouse_from_assaign_to_arr)) || (in_array(yii::app()->user->id, $receipt_warehouse_to_assaign_to_arr) && ($receipt_warehouse_from_in_charge == yii::app()->user->id))){
                $result = "true";
            } else {
                $result = "false";
            }
            return $result;
        } else {
            $receipt_warehouse_to_check = Warehouse::model()->findByPk($receipt_warehouse_to);
            $receipt_warehouse_to_in_charge = $receipt_warehouse_to_check['warehouse_incharge'];
            $receipt_warehouse_to_assaign_to = $receipt_warehouse_to_check['assigned_to'];
            $receipt_warehouse_to_assaign_to_arr = (explode(",", $receipt_warehouse_to_assaign_to));
            if (($receipt_warehouse_to_in_charge == yii::app()->user->id) || in_array(yii::app()->user->id, $receipt_warehouse_to_assaign_to_arr)) {
                $result = "true";
            } else {
                $result = "false";
            }
            return $result;
        }
    }

    public function unitConversionToBase($item_conversion_unit, $item_base_unit, $stock_item, $quantity_in_conversion_unit) {
        $tblpx = Yii::app()->db->tablePrefix;
        if ($item_conversion_unit != '' && $item_base_unit != '' && $stock_item != '') {
            $itemUnitConversion = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}unit_conversion WHERE item_id='" . $stock_item . "' AND base_unit='" . $item_base_unit . "' AND conversion_unit='" . $item_conversion_unit . "'")->queryRow();
            if ($itemUnitConversion['id'] != '') {
                $item_conversion_factor = $itemUnitConversion['conversion_factor'];
                $item_conversion_id = $itemUnitConversion['id'];
            } else {
                $item_conversion_factor = 1;
                $item_conversion_id = "";
            }
            $baseunit_quantity = $quantity_in_conversion_unit / $item_conversion_factor;
        } else {
            $item_conversion_factor = 1;
            $item_conversion_id = '';
            $baseunit_quantity = $quantity_in_conversion_unit / $item_conversion_factor;
        }
        $result['item_conversion_unit'] = $item_conversion_unit;
        $result['item_base_unit'] = $item_base_unit;
        $result['item_conversion_factor'] = $item_conversion_factor;
        $result['item_conversion_id'] = $item_conversion_id;
        $result['baseunit_quantity'] = $baseunit_quantity;
        return $result;
    }

    public function checkBillApprovalStatus($bill_id) {
        $bill_item_approoved_status_arr = array();
        $bill_item_approoved_status_details = Billitem::model()->findAll(
                array(
                    'select' => array('*'), 'condition' => 'bill_id ="' . $bill_id . '"',
                    'order' => 'billitem_id DESC'
                )
        );
        if (!empty($bill_item_approoved_status_details)) {
            foreach ($bill_item_approoved_status_details as $value) {
                if ($value['approve_status'] == '' || $value['approve_status'] == 1) {
                    $approve_status = 1;
                    array_push($bill_item_approoved_status_arr, $approve_status);
                } else {
                    $approve_status = 0;
                    array_push($bill_item_approoved_status_arr, $approve_status);
                }
            }
        }
        if (!empty($bill_item_approoved_status_arr) && !in_array(0, $bill_item_approoved_status_arr)) {
            $bill_approve_status = "true";
        } else {
            $bill_approve_status = "false";
        }
        return $bill_approve_status;
    }

    public function Logcreate($newdata, $olddata, $module, $recid, $action) {
        $response = array('error' => false, $data = '');
        if ($action == 'add' || $action == 'delete') {
            $logdata = json_encode($newdata);
        } else {

            $old_data = array_diff_assoc($olddata, $newdata);
            $modified_data = array_diff_assoc($newdata, $olddata);
            $changes = array($old_data, $modified_data);
            $logdata = json_encode($changes);
            $count = count($changes[0]) + count($changes[1]);
            if ($count == 0) {

                $response['message'] = 'nochange';
                return $response;
            }
        }

        $model = new JpLog;
        $model->log_data = $logdata;
        $model->log_table = $module;
        $model->log_primary_key = $recid;
        $model->log_datetime = date('Y-m-d H:i:s');
        $model->log_action = $action;
        $model->log_action_by = Yii::app()->user->id;
        $model->company_id = Yii::app()->user->company_id;

        if ($model->save()) {
            $response['message'] = 'success';
            $response['id'] = Yii::app()->db->getLastInsertID();
        } else {

            $response = array('error' => true, 'message' => "Error while {$action} to log");
            return $response;
        }

        return $response;
    }

    public function addWarehouseCategoryOptions($warehouse_arr, $warehouse) {
        $result_array = array();
        $project_name = '';
        $warehouse_common_model = array();
        $warehouse_project_model = array();
        $result_array = $warehouse_arr;
        foreach ($warehouse_arr as $value) {
            if ($value['id'] == $warehouse) {

                return $result_array;
            } else {
                $warehouse_common_model_sql = "SELECT * FROM " . $this->tableNameAcc('warehouse', 0) . " WHERE warehouse_id =" . $warehouse . " AND (project_id IS NULL OR project_id='')";
                $warehouse_common_model = Yii::app()->db->createCommand($warehouse_common_model_sql)->queryAll();
                $warehouse_project_model_sql = "SELECT * FROM " . $this->tableNameAcc('warehouse', 0) . " WHERE warehouse_id =" . $warehouse . " AND project_id IS NOT NULL";
                $warehouse_project_model = Yii::app()->db->createCommand($warehouse_project_model_sql)->queryAll();
                if (empty($warehouse_common_model)) {
                    foreach ($warehouse_project_model as $project_ware) {
                        if (!empty($project_ware['project_id'])) {
                            $project_model = Projects::model()->findByPk($project_ware['project_id']);
                            if (!empty($project_model)) {
                                $project_name = $project_model['name'];
                            }
                        }

                        $data = array('id' => $project_ware['warehouse_id'], 'text' => $project_ware['warehouse_name'], 'group' => $project_name);
                        array_push($result_array, $data);
                    }

                    return $result_array;
                } else {
                    foreach ($warehouse_common_model as $common) {
                        $data = array('id' => $common['warehouse_id'], 'text' => $common['warehouse_name'], 'group' => 'General');
                        array_push($result_array, $data);
                    }

                    return $result_array;
                }
            }
        }
    }

    public function itemName($item_id) {

        $purchase_category_model = Specification::model()->findByPk($item_id);
        if ($purchase_category_model['brand_id'] != NULL) {
            $brand_sql = "SELECT brand_name FROM " . $this->tableNameAcc('brand', 0) . " WHERE id=" . $purchase_category_model['brand_id'] . "";
            $brand_details = Yii::app()->db->createCommand($brand_sql)->queryRow();
            $brand = '-' . ucfirst($brand_details['brand_name']);
        } else {
            $brand = '';
        }

        if ($purchase_category_model['cat_id'] != '') {
            $result_sql = "SELECT parent_id, id , category_name FROM " . $this->tableNameAcc('purchase_category', 0) . " WHERE id=" . $purchase_category_model['cat_id'] . "";
            $result = Yii::app()->db->createCommand($result_sql)->queryRow();
            $item_name = ucfirst($result['category_name']) . $brand . '-' . ucfirst($purchase_category_model['specification']);
        } else {
            $item_name = $brand . '-' . $purchase_category_model['specification'];
        }
        return $item_name;
    }

    public function Itemunitlist($purchase_itemid) {
        $purchase_itemid = $purchase_itemid;
        $final_list = array();
        $ItemUnitlist = UnitConversion::model()->findAll(
                array("condition" => "item_id =  $purchase_itemid", "order" => "id")
        );
        $final_list = array();
        foreach ($ItemUnitlist as $key => $value) {
            $final_list[$key]['id'] = $value['conversion_unit'];
            $final_list[$key]['value'] = $value['conversion_unit'];
        }
        return $final_list;
    }

    public function actiongetFinalData($data, $approval_status, $id, $tbl, $modelName) {
        
        $countsql = "SELECT COUNT(*) as count"
                . " FROM " . $tbl
                . " WHERE `ref_id` =" . $data[$id]
                . " AND `approval_status` = '0'";

        $count = Yii::app()->db->createCommand($countsql)->queryRow();

        $sql = "SELECT COUNT(*) as count,"
                . " MAX($id) as max "
                . " FROM " . $tbl
                . " WHERE `ref_id` = " . $data[$id]
                . " AND `approval_status` != '2' "
                . " ORDER BY `$id` ASC";
        $refDataCount = Yii::app()->db->createCommand($sql)->queryRow();
       
        if ($refDataCount['count'] > 0) {            
            $finaldata = $modelName::model()->findByPk($refDataCount['max']);
            $approval_status = $finaldata['approval_status'];
        }
        
        if ($data['approval_status'] == 0) {            
            $tblpx = Yii::app()->db->tablePrefix;
            $select = "t.*,t.created_date as created,v.*,ba.*,"
                    . "st.*,t.description as vendor_description,"
                    . " pr.name as project_name,t.company_id as company_id ";

            if ($modelName == 'PreExpenses') {
                $select = "t.*,pr.name as pname, b.bill_number as billno,"
                        . " et.type_name as typename,v.name vname,ba.bank_name as bankname,"
                        . " i.inv_no as invoice_no,scp.subcontractor_id as scid";
            }
            
            $where = "t.approval_status!='2' AND ref_id='" . $data[$id] . "'";

            $command = Yii::app()->db->createCommand()->select($select);
            $command->from($tbl . ' as t');

            $command->leftJoin($this->tableNameAcc('vendors', 0) . ' as v', 'v.vendor_id = t.vendor_id ');
            if ($modelName == 'PreExpenses') {
                $command->leftJoin($this->tableNameAcc('bank', 0) . ' as ba', 't.bank_id = ba.bank_id');
                $command->leftJoin($this->tableNameAcc('projects', 0) . ' as pr', 't.projectid = pr.pid');
                $command->leftJoin($this->tableNameAcc('bills', 0) . ' as b', 't.bill_id = b.bill_id');
                $command->leftJoin($this->tableNameAcc('expense_type', 0) . ' as et', 't.exptype = et.type_id');
                $command->leftJoin($this->tableNameAcc('invoice', 0) . ' as i', 't.invoice_id = i.invoice_id');
                $command->leftJoin($this->tableNameAcc('subcontractor_payment', 0) . ' as scp', 't.subcontractor_id = scp.payment_id');
            } else if ($modelName == 'PreDailyvendors') {
                $command->leftJoin($this->tableNameAcc('bank', 0) . ' as ba', 't.bank = ba.bank_id');
                $command->leftJoin($this->tableNameAcc('status', 0) . ' as st', 't.payment_type = st.sid');
                $command->leftJoin($this->tableNameAcc('projects', 0) . ' as pr', 't.project_id = pr.pid');
            }
            $command->where($where);
            $command->order('t.'.$id .' DESC');
            $command->limit(1);
            $data = $command->queryRow(true);
            
        }
        
        return (array('data' => $data, 'approval_status' => $approval_status,'count'=>$count));
    }


    public function actiongetDailyExpFinalData($data, $approval_status, $id, $tbl, $modelName) {
       
        $countsql = "SELECT COUNT(*) as count"
                . " FROM " . $tbl
                . " WHERE `ref_id` =" . $data[$id]
                . " AND `approval_status` = '0'";

        $count = Yii::app()->db->createCommand($countsql)->queryRow();

        $sql = "SELECT COUNT(*) as count,"
                . " MAX($id) as max "
                . " FROM " . $tbl
                . " WHERE `ref_id` = " . $data[$id]
                . " AND `approval_status` != '2' "
                . " ORDER BY `$id` ASC";
        $refDataCount = Yii::app()->db->createCommand($sql)->queryRow();       

        if ($refDataCount['count'] > 0) {            
            $finaldata = $modelName::model()->findByPk($refDataCount['max']);
            $approval_status = $finaldata['approval_status'];
        }
        
        if ($data['approval_status'] == 0) {            
            $tblpx = Yii::app()->db->tablePrefix;

            $select = "t.*, t.bill_id as billno, et.name "
            . " as typename,v.name vname,ba.bank_name as bankname ";                         
            $where = "t.approval_status!='2' AND ref_id='" . $data[$id] . "'";

            $command = Yii::app()->db->createCommand()->select($select);
            $command->from($tbl . ' as t');
            $command->leftJoin($this->tableNameAcc('bills', 0) . ' as b', 't.bill_id = b.bill_id ');            
            $command->leftJoin($this->tableNameAcc('company_expense_type', 0) . ' as et', 't.expensehead_id = et.company_exp_id');
            $command->leftJoin($this->tableNameAcc('company_expense_type', 0) . ' as v', 't.dailyexpense_receipt_head = v.company_exp_id');
            $command->leftJoin($this->tableNameAcc('bank', 0) . ' as ba', 't.bank_id = ba.bank_id');            
            $command->where($where);
            $command->order('t.'.$id .' DESC');
            $command->limit(1);
            $data = $command->queryRow(true);            
        }
        
        return (array('data' => $data, 'approval_status' => $approval_status,'count'=>$count));
    }

    public function actiongetScFinalData($data, $approval_status, $id, $tbl, $modelName) {               
        $countsql = "SELECT COUNT(*) as count"
                . " FROM " . $tbl
                . " WHERE `ref_id` =" . $data[$id]
                . " AND `approval_status` = '0'";

        $count = Yii::app()->db->createCommand($countsql)->queryRow();

        $sql = "SELECT COUNT(*) as count,"
                . " MAX($id) as max "
                . " FROM " . $tbl
                . " WHERE `ref_id` = " . $data[$id]
                . " AND `approval_status` != '2' "
                . " ORDER BY `$id` ASC";
        $refDataCount = Yii::app()->db->createCommand($sql)->queryRow();
       
        if ($refDataCount['count'] > 0) {            
            $finaldata = $modelName::model()->findByPk($refDataCount['max']);
            $approval_status = $finaldata['approval_status'];
        }
        
        if ($data['approval_status'] == 0) {
            
            $tblpx = Yii::app()->db->tablePrefix;

            $select = "t.*,t.created_date as created, s.*,pr.*,ba.*,st.*,t.description as payment_description,t.company_id as company_id "; 
            $where = "t.approval_status!='2' AND ref_id='" . $data[$id] . "'";

            $command = Yii::app()->db->createCommand()->select($select);
            $command->from($tbl . ' as t');
            $command->join($this->tableNameAcc('subcontractor', 0) . ' as s', 's.subcontractor_id = t.subcontractor_id ');  
            $command->leftJoin($this->tableNameAcc('projects', 0) . ' as pr', 'pr.pid = t.project_id ');                        
            $command->leftJoin($this->tableNameAcc('status', 0) . ' as st', 't.payment_type = st.sid');
            $command->leftJoin($this->tableNameAcc('bank', 0) . ' as ba', 't.bank = ba.bank_id');            
            $command->where($where);
            $command->order('t.'.$id .' DESC');
            $command->limit(1);            

            $data = $command->queryRow(true);                     
        }
        
        return (array('data' => $data, 'approval_status' => $approval_status,'count'=>$count));
    }


    public function getModelName($table_name) {
                
        $table_split = explode("_",$table_name);
        $model = ucfirst($table_split[0]);
        
        if(count($table_split)>1){
            $model = ucfirst($table_split[0]).ucfirst($table_split[1]);
        }
        
        return $model;
    }


    public function actiongetDailyReportFinalData($data, $approval_status, $id, $tbl, $modelName) {
               
        $countsql = "SELECT COUNT(*) as count"
                . " FROM " . $tbl
                . " WHERE `ref_id` =" . $data[$id]
                . " AND `approval_status` = '0'";

        $count = Yii::app()->db->createCommand($countsql)->queryRow();

        $sql = "SELECT COUNT(*) as count,"
                . " MAX($id) as max "
                . " FROM " . $tbl
                . " WHERE `ref_id` = " . $data[$id]
                . " AND `approval_status` != '2' "
                . " ORDER BY `$id` ASC";
        $refDataCount = Yii::app()->db->createCommand($sql)->queryRow();
       

        if ($refDataCount['count'] > 0) {            
            $finaldata = $modelName::model()->findByPk($refDataCount['max']);
            $approval_status = $finaldata['approval_status'];
        }
        
        if ($data['approval_status'] == 0) {
            
            $tblpx = Yii::app()->db->tablePrefix;
            $select = "t.*, p.name as pname,s.subcontractor_name as subname,exp.type_name ";                         
            $where = "t.approval_status!='2' AND ref_id='" . $data[$id] . "'";

            $command = Yii::app()->db->createCommand()->select($select);
            $command->from($tbl . ' as t');
            $command->join($this->tableNameAcc('subcontractor', 0) . ' as s', 's.subcontractor_id = t.subcontractor_id ');  
            $command->leftJoin($this->tableNameAcc('projects', 0) . ' as p', 'p.pid = t.projectid ');                        
            $command->leftJoin($this->tableNameAcc('expense_type', 0) . ' as exp', 't.expensehead_id = exp.type_id');                        
            $command->where($where);
            $command->order('t.'.$id .' DESC');
            $command->limit(1);            
            $data = $command->queryRow(true);         
            
        }
        
        return (array('data' => $data, 'approval_status' => $approval_status,'count'=>$count));
    }

    public function getPendingBills() {
        $pendingBills =array();
        $bill_list = array();
        $bills = "";

        $command = Yii::app()->db->createCommand()->select('b.bill_id as billId');
        $command->from($this->tableNameAcc('expenses', 0) . ' as e');
        $command->leftJoin($this->tableNameAcc('bills', 0) . ' as b', 'e.bill_id = b.bill_id ');

        $tblpx = Yii::app()->db->tablePrefix;
        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        $newQuery2 = "";
        foreach ($arrVal as $arr) {   
            if ($newQuery2)
                $newQuery2 .= ' OR';        
            $newQuery2 .= " FIND_IN_SET('" . $arr . "', b.company_id)";    
        }

        $command->where("e.type = 73 AND b.bill_totalamount IS NOT NULL AND b.purchase_id IS NOT NULL AND $newQuery2");
        $billsArray = $command->queryAll();

        if (!empty($billsArray)) {
            foreach ($billsArray as $entry) {
                if ($entry['billId'] != "") {
                    $bill_list[] = $entry['billId'];
                }
            }
            $bills = implode(',', $bill_list);
        }
        
       
        $command = Yii::app()->db->createCommand()->select('b.*,p.*');
        $command->from($this->tableNameAcc('bills', 0) . ' as b');
        if (!empty($bills)) {
            $command->where(' b.bill_id NOT IN (' . $bills . ') AND p.project_id !="' . null . '"');
        }
        $command->leftjoin($this->tableNameAcc('purchase',0). ' as p','p.p_id = b.purchase_id');
        $command->order('p.project_id' .' ASC');
        $pendingBills = $command->queryAll();
        

        return $pendingBills;
    }
    
    public function generateQuotationNumber() {
        $criteria = new CDbCriteria();
        $criteria->select = "invoice_no";
        $criteria->order = 'id DESC';
        $criteria->limit = 1;
        $quotation = SalesQuotationMaster::model()->findAll($criteria);
        $quotation_no = isset($quotation['0']['invoice_no']) ? $quotation['0']['invoice_no'] : '';
        if ($quotation_no) {
            $value2 = $quotation_no;
            $strArray = explode('-', $value2);
            $value2 = $strArray[2];
            $value2 = $value2 + 1; //Incrementing numeric part
            $value2 = 'QUT-' . date('Y') . '-' . sprintf('%03s', $value2); //concatenating incremented value
            $value = $value2;
        } else {
            $value2 = 'QUT-' . date('Y') . '-001';
            $value = $value2;
        }
        return $value;
    }

    public static function getBrand() {
        static $brand;
        if (!isset($brand)) {
            $brand = CHtml::listData(Brand::model()->findAll(array('order' => 'id ASC')), 'id', 'brand_name');
        }
        return $brand;
    }

    public function getMaterialNames($materialids) {
        if ($materialids) {
            $user_tree_array = array();
            $data = array();
            $final = array();
            $user = Users::model()->findByPk(Yii::app()->user->id);
            $arrVal = explode(',', $user->company_id);
            $newQuery = "";
            foreach ($arrVal as $arr) {
                if ($newQuery)
                    $newQuery .= ' OR';
                $newQuery .= " FIND_IN_SET('" . $arr . "', company_id)";
            }
            $Criteria = new CDbCriteria();
            $Criteria->select = "id, specification, brand_id, cat_id";
            $Criteria->condition = "id IN($materialids) AND (" . $newQuery . ")";
            $specification = Specification::model()->findAll($Criteria);
            foreach ($specification as $key => $value) {
                $category = PurchaseCategory::model()->findByPk($value['cat_id']);
                $brand = Brand::model()->findByPk($value['brand_id']);
                array_push($final, ucwords($category['category_name']."-".$brand['brand_name']."-".$value['specification']));
            }
            $final_names = implode(',', $final);
            return $final_names;
        }
    }

    public static function getFinishData() {
        static $finish;
        if (!isset($finish)) {
            $finish = CHtml::listData(QuotationFinishMaster::model()->findAll(array('order' => 'id ASC')), 'id', 'name');
        }
        return $finish;
    }

    public function getParentDetails($id) {
        $model = PurchaseCategory::model()->findByPk($id);
        return $model;
    }

    public static function getLocationData() {
        static $location;
        if (!isset($location)) {
            $location = CHtml::listData(Location::model()->findAll(array('order' => 'id ASC')), 'id', 'name');
        }
        return $location;
    }
     public static function getLocationById($location_id){
        static $locations;
        if(!isset($location)){
            $locations = CHtml::listData(Location::model()->findAll(array('order' => 'id ASC')), 'id', 'name');
    
        }
        if (isset($locations[$location_id])) {
            return $locations[$location_id];
        } else {
            return null; // Company not found
        }
     }
    public static function getCompanyName($companyId){
        static $companies;
        if (!isset($companies)) {
            $companies = CHtml::listData(Company::model()->findAll(), 'id', 'name');
        }
        if (isset($companies[$companyId])) {
            return $companies[$companyId];
        } else {
            return null; // Company not found
        }
    }
    
    public static function getUnitName($unitId){
        static $units;
        if (!isset($units)) {
            $units = CHtml::listData(Unit::model()->findAll(), 'id', 'unit_name');
        }
        if (isset($units[$unitId])) {
            return $units[$unitId];
        } else {
            return null; // Company not found
        }
    }
    public static function getPurchaseItemsForPermission($purchase_id, $permission_status){
        $items = PurchaseItems::model()->findAll(array(
            "condition" => "purchase_id = :purchase_id AND permission_status = :permission_status",
            "params" => array(":purchase_id" => $purchase_id, ":permission_status" => $permission_status)
        ));
        return $items;
    }



    public function getTemplate($selectedtemplate) {
        $header = "";
        $footer = "";
        $current_year = date("Y");
        $company = Company::model()->findBypk(Yii::app()->user->company_id);
        $company_name=(isset($company['name']) && !empty($company['name'])) ? ($company['name']) : "";
        $address = (isset($company['address']) && !empty($company['address'])) ? ($company['address']) : "";
        $pin = (isset($company['pincode']) && !empty($company['pincode'])) ? ($company['pincode']) : "";
        $phone = (isset($company['phone']) && !empty($company['phone'])) ? ( $company['phone']) : "";
        $email = (isset($company['email_id']) && !empty($company['email_id'])) ? ( $company['email_id']) : "";
        $gst = "GST NO: " . isset($company["company_gstnum"]) ?
                $company["company_gstnum"] : '';

        $logo = CHtml::image($this->logo, Yii::app()->name,
                        array('style' => 'max-height:45px;'));
        $activeProjectTemplate = $this->getActiveTemplate();
        if ($selectedtemplate == 'template1' && $activeProjectTemplate != 'TYPE-6') {
            $header = '<div style="padding:20px;padding-bottom:200px;height: 40mm;">
                        <table border=0 style="margin-bottom:200px">
                                <tbody>
                                    <tr>
                                        <td class="img-hold"> ' . $logo . '</td>
                                        <td class="text-center">
                                        <h1></h1>
                                        </td>
                                    </tr>                                
                                    <tr>
                                        <td></td>
                                        <td class="text-center">
                                            <div>' . $address . '<div>
                                            <div> PIN : ' . $pin . ',&nbsp;
                                                PHONE : ' . $phone . '<div>
                                            <div>  EMAIL : ' . $email . ' <div>                       
                                            <div>  GST NO: ' . $gst . '</div> 
                                            <div>  <br><br><br><br><br><br><br><br><br><br><br><br><br></div> 
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            
                            </div>';
            $footer = '<div style="padding: 5px 20px 20px;text-align:center;font-size:10px;">
                        Powered by bluehorizoninfotech.com<br>
                        Copyright © '.$current_year.' by Blue Horizon Infotech, Cochin. All Rights Reserved.
                        </div>';
                        
        } else if ($selectedtemplate == 'template1' && $activeProjectTemplate == 'TYPE-6') {
            $header = '<div style="padding:20px;">
                        <table border=0 >
                            <tbody>
                                <tr>
                                    <td class="img-hold"> ' . $logo . '</td>
                                    <td class="text-center">
                                        
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>';
            $footer = '<div style="padding: 5px 20px 20px;text-align:center;font-size:10px;">
                        Powered by bluehorizoninfotech.com<br>
                        Copyright © '.$current_year.' by Blue Horizon Infotech, Cochin. All Rights Reserved.
                        </div>';
        } else if ($selectedtemplate == 'template2') {
            $header = '<div style="border-bottom: 2px solid #000000;padding:0px 20px;">
                    <table width="100%">
                        <tr>
                            <td width="9%" rowspan="2">' . $logo . '</td>
                            <td width="44%"><h2>R-A-LAB</h2></td>
                            <td width="44%">&nbsp;</td>
                        </tr>
                        <tr>            
                            <td width="44%"><div style="font-size:15px">Ramees Ali + teamLab</div></td>
                            <td width="44%" align="right"><div style="font-size:15px">' . $gst . '</div></td>
                        </tr>
                        <br>
                    </table>
                </div>';

            $footer = '<div style="background-color:#000;">
                        <table width="100%">
                            <tr>
                                <td width="33%" style="color:#fff;">
                                    <div>office:</div>        
                                    <div>Opp. Indian Oil
                                    Vayapparapadi , Manjeri
                                    Malappuram 676121
                                    mail@ralab.in</div> 
                                </td>
                                <td width="44%" align="center"></td>
                                <td width="22%">' . $logo . '</td>
                            </tr>
                            <tr>
                            <td width="33%" style="color:#fff;">GSTIN: 32AUZPP4647B1ZC</td>
                            <td width="44%" align="center"></td>
                            <td width="22%" style="color:#fff;">www.ralab.in</td>
                            </tr>
                        </table>
                    </div><div style="padding: 5px 20px 20px;text-align:center;background-color:#000;color:#fff;font-size:10px;">
                    Powered by bluehorizoninfotech.com<br>
                    Copyright © '.$current_year.' by Blue Horizon Infotech, Cochin. All Rights Reserved.
                    </div>';
        } else if ($selectedtemplate == 'template3' && $activeProjectTemplate == 'TYPE-6') {
            $header = '<div style="border-bottom: 2px solid #000000;padding:0px 20px;">
                        <table width="100%">
                        <tr>
                            <td width="9%" rowspan="2">' . $logo . '</td>
                            <td width="44%"></td>
                            <td width="44%"></td>
                        </tr>
                        <tr>            
                            <td width="44%"><div style="font-size:15px"></div></td>
                            <td width="44%" align="right">
                                <div style="font-size:15px">' . $gst . '</div>
                            </td>
                        </tr>
                        <br>
                        </table>
                    </div>';

            $footer = '<div style="background-color:#fff;">
            <table width="100%">
                <tr>
                    <td width="44%" style="color:#000;">  
                        <div><b>  BILLING ADDRESS  </b><div>  
                        <div><b>' . $company_name . ',</b><div>                                  
                        <div>' . $address . '<div>
                        <div> PIN : ' . $pin . '</div>
                        <div>   PHONE : ' . $phone . '<div>
                        <div>  EMAIL : ' . $email . ' <div>                       
                        <div>  GST NO: ' . $gst . '</div> 
                    </td>
                    <td width="33%" align="center"></td>
                    <td width="22%">' . $logo . '</td>
                </tr>
            </table>
        </div>
        <div style="padding: 5px 20px 20px;text-align:center;background-color:#fff;color:#000;font-size:10px;">
            Powered by bluehorizoninfotech.com<br>
            Copyright © '.$current_year.' by Blue Horizon Infotech, Cochin. All Rights Reserved.
        </div>';
          }else {
            
            $header = '<div style="border-bottom: 2px solid #000000;padding:0px 20px;">
                            <table width="100%">
                                <tr>
                                    <td width="9%" rowspan="2">' . $logo . '</td>
                                    <td width="44%"></td>
                                    <td width="44%"></td>
                                </tr>
                                <tr>            
                                    <td width="44%"><div style="font-size:15px"></div></td>
                                    <td width="44%" align="right">
                                        <div style="font-size:15px">' . $gst . '</div>
                                    </td>
                                </tr>
                                <br>
                            </table>
                        </div>';

            $footer = '<div style="background-color:#fff;">
                            <table width="100%">
                                <tr>
                                    <td width="44%" style="color:#000;">                                    
                                        <div>' . $address . '<div>
                                        <div> PIN : ' . $pin . '</div>
                                        <div>   PHONE : ' . $phone . '<div>
                                        <div>  EMAIL : ' . $email . ' <div>                       
                                        <div>  GST NO: ' . $gst . '</div> 
                                    </td>
                                    <td width="33%" align="center"></td>
                                    <td width="22%">' . $logo . '</td>
                                </tr>
                            </table>
                        </div>
                        <div style="padding: 5px 20px 20px;text-align:center;background-color:#fff;color:#000;font-size:10px;">
                            Powered by bluehorizoninfotech.com<br>
                            Copyright © '.$current_year.' by Blue Horizon Infotech, Cochin. All Rights Reserved.
                        </div>';
        }
        return (array('header' => $header, 'footer' => $footer));
    }
    public function getRevisionno($qid){
        
        $sql = "SELECT revision_no "
            . " FROM `jp_quotation_revision` " 
            . " WHERE `qid` = ".$qid
            . " ORDER BY id DESC LIMIT 1";
            
        $revision_no =  Yii::app()->db->createCommand($sql)->queryScalar();
        if($revision_no == ""){
            $revision_no =  Yii::app()->db->createCommand()
                ->select('revision_no')
                ->from($this->tableNameAcc('sales_quotation_master', 0))
                ->where('id="' . $qid . '"')
                
                ->queryScalar();                
        }
        
        $latest_revision_num = substr($revision_no, strrpos($revision_no, '-' )+1);
        if($latest_revision_num == ''){
           
            $revision_no =  Yii::app()->db->createCommand()
                ->select('revision_no')
                ->from($this->tableNameAcc('sales_quotation_master', 0))
                ->where('id="' . $qid . '"')
                
                ->queryScalar(); 
                
            $latest_revision_num = substr($revision_no, strrpos($revision_no, '-' )+1);
            $latest_revision_num = intval($latest_revision_num);
        }
        $rev_no = $latest_revision_num+1;
        return $rev_no;
    }

    public static function checkDuplicateEntry($cheque_no, $bank) {

        $dailyexpense_sql = "SELECT `dailyexpense_chequeno` "
                . "FROM `jp_dailyexpense` WHERE bank_id=" . $bank;
        $dailyvendor_sql = "SELECT `cheque_no` "
                . "FROM `jp_dailyvendors` WHERE bank=" . $bank;
        $sc_cheque_sql = "SELECT `cheque_no` "
                . "FROM `jp_subcontractor_payment` WHERE bank=" . $bank;

        $final_cheque_array_sql = "SELECT cheque_no 
                FROM `jp_expenses` 
                WHERE bank_id=" . $bank 
                . " UNION " . $dailyexpense_sql  
                . " UNION " . $dailyvendor_sql 
                . " UNION " . $sc_cheque_sql;

        $chequeArray = Yii::app()->db->createCommand($final_cheque_array_sql)->queryAll();
        
        $payment_cheque_array = array();
        foreach ($chequeArray as $data) {
            $value = "{$data['cheque_no']}";
            if ($value != '') {
                array_push($payment_cheque_array, $value);
            }
        }

        if (in_array($cheque_no, $payment_cheque_array)) {
            return 'Cheque number already exist';
        }
    }

    public function getVendorAmount(){
        $tblpx = Yii::app()->db->tablePrefix;
        $sqlData        = "SELECT v.vendor_id,v.name, a.billtotal, a.billadditionaltotal, "
                . " b.daybookpaid, c.vendorpaid,d.daybookcredit FROM {$tblpx}vendors v"
                . " LEFT JOIN (SELECT p.vendor_id, SUM(IFNULL(b.bill_totalamount, 0))"
                . " AS billtotal,   SUM(IFNULL(b.bill_additionalcharge, 0)) "
                . " AS billadditionaltotal "
                . " FROM {$tblpx}bills b "
                . " LEFT JOIN {$tblpx}purchase p ON b.purchase_id = p.p_id "
                . " WHERE (" . $this->getCompanyCondition("b") . ") "
                . " GROUP BY p.vendor_id) a ON a.vendor_id = v.vendor_id"
                . " LEFT JOIN (SELECT e.vendor_id, SUM(IFNULL(e.paid, 0)) "
                . " as daybookpaid FROM {$tblpx}expenses e "
                . " WHERE e.type = 73 AND (" . $this->getCompanyCondition("e") . ") "
                . " GROUP BY e.vendor_id) b "
                . " ON b.vendor_id = v.vendor_id"
                . " LEFT JOIN (SELECT e.vendor_id, SUM(IFNULL(e.amount, 0)) "
                . " as daybookcredit FROM {$tblpx}expenses e "
                . " WHERE e.type = 73 AND e.expense_type=0 AND (" . $this->getCompanyCondition("e") . ") "
                . " GROUP BY e.vendor_id) d "
                . " ON d.vendor_id = v.vendor_id"
                . " LEFT JOIN (SELECT d.vendor_id, SUM(IFNULL(d.amount, 0) + IFNULL(d.tax_amount, 0))"
                . " as vendorpaid FROM {$tblpx}dailyvendors d "
                . " WHERE (d.reconciliation_status IS NULL OR d.reconciliation_status=1) AND (" . $this->getCompanyCondition("d") . ") "
                . " GROUP BY d.vendor_id) c "
                . " ON c.vendor_id = v.vendor_id"
                . " WHERE (" . $this->getCompanyCondition("v") . ") "
                . " AND v.vendor_type != 2 ORDER BY v.name";
                $paymentData    = Yii::app()->db->createCommand($sqlData)->queryAll();
                $finalinvoice_sum = 0;
                $finalpayment_sum = 0;
                $finalbalance = 0;
                foreach ($paymentData as $paymentList) {                            
                    if (!empty($paymentList['billadditionaltotal'])) {
                        $paymentList["billtotal"] += $paymentList['billadditionaltotal'];
                    }
                    $totalPayment = $paymentList["daybookpaid"] + $paymentList["vendorpaid"];
                    $balancePaid = $paymentList["billtotal"] - $totalPayment;
                    $invoice = Yii::app()->Controller->money_format_inr($paymentList['billtotal'], 2);
                    $datefrom  = isset($_REQUEST['date_from'])?$_REQUEST['date_from']:"";
                    $dateto    = isset($_REQUEST['date_to'])?$_REQUEST['date_to']:"";
                    $invoiceSum = Vendors::model()->getinvoiceSum($paymentList['vendor_id'],$datefrom,$dateto);
                    $payment = $totalPayment;
                    $finalinvoice_sum += $invoiceSum;
                    $finalpayment_sum += (str_replace(",", "", $payment));
                    $finalbalance = ($finalinvoice_sum - $finalpayment_sum);
                }
                echo $finalbalance;
    }
    
    private function getCompanyCondition($prefix)
    {
        $user       = Users::model()->findByPk(Yii::app()->user->id);

        $arrVal     = explode(',', $user->company_id);
        $newQuery   = "";
        $prefix     = !empty($prefix) ? $prefix . "." : "";
        foreach ($arrVal as $arr) {
            if ($newQuery) $newQuery .= ' OR';
            $newQuery .= " FIND_IN_SET('" . $arr . "', {$prefix}company_id)";
        }
        return $newQuery;
    }

    public function getScPayment(){

        $tblpx = Yii::app()->db->tablePrefix;
        $newQuery = "";
        $newQuery_1 = "";
        $newQuery1 = "";
        $newQuery2 = "";
        $newQuery3 = "";

        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        foreach ($arrVal as $arr) {
            if ($newQuery) $newQuery .= ' OR';
            if ($newQuery_1) $newQuery_1 .= ' OR';
            $newQuery .= " FIND_IN_SET('" . $arr . "', id)";
            $newQuery_1 .= " FIND_IN_SET('" . $arr . "', company_id)";
        }


        foreach ($arrVal as $arr) {
            if ($newQuery1) $newQuery1 .= ' OR';
            if ($newQuery2) $newQuery2 .= ' OR';
            if ($newQuery3) $newQuery3 .= ' OR';
            $newQuery1 .= " FIND_IN_SET('" . $arr . "', {$tblpx}scquotation.company_id)";
            $newQuery2 .= " FIND_IN_SET('" . $arr . "', {$tblpx}subcontractor_payment.company_id)";
            $newQuery3 .= " FIND_IN_SET('" . $arr . "', {$tblpx}subcontractor.company_id)";
        }


        $final_amount = 0;

        $scqsql = "SELECT * FROM " . $this->tableNameAcc('subcontractor', 0) . ""
            . " INNER JOIN " . $this->tableNameAcc('scquotation', 0) . ""
            . " ON {$tblpx}subcontractor.subcontractor_id={$tblpx}scquotation.subcontractor_id "
            . " LEFT JOIN {$tblpx}projects ON {$tblpx}scquotation.project_id= {$tblpx}projects.pid "
            . " WHERE (" . $newQuery3 . ") GROUP BY {$tblpx}scquotation.subcontractor_id,"
            . " {$tblpx}scquotation.project_id "
            . " ORDER BY {$tblpx}scquotation.scquotation_id";
            
        $scquotation = Yii::app()->db->createCommand($scqsql)->queryAll();
        if (!empty($scquotation)) {
            foreach ($scquotation as $data) {
                $scqamtsql = "SELECT SUM(scquotation_amount) "
                        . " AS scquotation_amount  FROM `jp_scquotation` "
                        . " WHERE subcontractor_id='" . $data['subcontractor_id'] . "'"
                        . " AND project_id= '" . $data['project_id'] . "'";
                $scquotation_amount = Yii::app()->db->createCommand($scqamtsql)->queryRow();

                $paymntsql = "SELECT SUM(amount) as payment_amount  "
                        . " FROM `jp_subcontractor_payment` "
                        . " WHERE subcontractor_id='" . $data['subcontractor_id'] . "' "
                        . " and project_id= '" . $data['project_id'] . "'";

                $payment_amount = Yii::app()->db->createCommand($paymntsql)->queryRow();

                $qamount=$scquotation_amount['scquotation_amount'];
                $pamount=$payment_amount['payment_amount'];
                $balance = $qamount-$pamount;                    
                $final_amount += $balance;
            }
        }
        echo $final_amount;

    }

    public function getActiveTemplate() { 
        $sql = "SELECT template_name FROM `jp_project_template` WHERE status='1'";
        $activeTemplate = Yii::app()->db->createCommand($sql)->queryScalar();
        return $activeTemplate;

    }
    public function getActiveTemplateID() { 
        $sql = "SELECT template_id FROM `jp_project_template` WHERE status='1'";
        $activeTemplateId = Yii::app()->db->createCommand($sql)->queryScalar();
        return $activeTemplateId;

    }
    public function getActiveTemplateIDWithCompany($company) { 
        $sql = "SELECT template_id FROM `jp_project_template` WHERE status='1' AND company_id='".$company."'";
        $activeTemplateId = Yii::app()->db->createCommand($sql)->queryScalar();
        return $activeTemplateId;

    }

    public function checkPOAutoIncrement(){
        $count = Yii::app()->db->createCommand("SELECT COUNT(*) as count FROM jp_purchase  WHERE purchase_no IS NOT NULL")->queryScalar();
        if($count==0){
            $nextPO = '1778';
        }  else{
            $lastPo = Yii::app()->db->createCommand("SELECT purchase_no FROM `jp_purchase` 
            WHERE purchase_no IS NOT NULL ORDER BY `p_id` DESC LIMIT 1")->queryScalar();
            $year = date('Y');
            $poNum =  trim($lastPo,"AMZ/PO-/".$year);
            $nextPO = $poNum +1;
        }
        return $nextPO ;    
    }

    public function getExpenseHead($bill_id){
        $sql= 'SELECT expensehead_id FROM jp_purchase p '
                . ' LEFT JOIN jp_bills b ON p.p_id =b.purchase_id '
                . ' WHERE b.bill_id='.$bill_id;
        $expid = Yii::app()->db->createCommand($sql)->queryScalar();
        $expensehead = ExpenseType::model()->findByPk($expid);
        return $expensehead['type_name'];
    }

    public function getConcordQuotationSum($qtid, $revision,$status) {
        $sum = 0;
        $condition = 'revision_no = "' . $revision . '"';
        if($status  != NULL && $status ==1){
            $condition = 'revision_no IN( ' . $revision . ')'; 
        }

        $sql = 'SELECT * FROM `jp_quotation_section` '
                . ' WHERE `qtn_id` = ' . $qtid
                . ' AND '.$condition;
                
        $sectionArray = Yii::app()->db->createCommand($sql)->queryAll();
        foreach ($sectionArray as $value) {
            //sub sec sum
            $sql = 'SELECT * FROM `jp_quotation_gen_category` '
            . ' WHERE `qid` = ' . $qtid
            . ' AND `section_id` = ' . $value['id'];

            $subsectionArray = Yii::app()->db->createCommand($sql)->queryAll();
            foreach ($subsectionArray as $value) {
                $sum += $value['amount_after_discount'];
            }
            // sub item sum

            $sql = 'SELECT SUM(amount_after_discount) '
                        . ' as  amount_after_discount'
                        . ' FROM `jp_sales_quotation` WHERE `parent_type` = 0 '
                        . ' AND `parent_id` = ' . $value['id']
                        . ' AND `deleted_status` = 1';
            $wrktypeitem = Yii::app()->db->createCommand($sql)->queryRow();
            foreach ($wrktypeitem as $value1) {
                $sum += $value1;
            }

        }
        return $sum;

    }

    public function getQuotationSum($qtid, $revision, $status, $mrp_sum = 0) {
        $db = Yii::app()->db;
        $sum = 0;
    
        // Build condition based on the revision and status
        if ($status !== NULL && $status == 1) {
            $condition = 'revision_no IN (' . $revision . ')';
        } else {
            $condition = 'revision_no = :revision';
        }
    
        // Fetch section IDs
        $sql = 'SELECT id FROM `jp_quotation_section` WHERE `qtn_id` = :qtid AND ' . $condition;
        $command = $db->createCommand($sql);
        $command->bindValue(':qtid', $qtid);
        if ($status !== 1) {
            $command->bindValue(':revision', $revision);
        }
    
        $sectionArray = $command->queryAll();
    
        if (!empty($sectionArray)) {
            foreach ($sectionArray as $value) {
                if (!$mrp_sum) {
                    $sum += $this->getSectionSum($value['id'], $qtid, $revision);
                } else {
                    $sum += $this->getSectionSumForMrp($value['id'], $qtid, $revision);
                }
            }
        }
        $sales_quotation_master_model = SalesQuotationMaster::Model()->findByPk($qtid);
        if(!empty($sales_quotation_master_model)){
            $sales_quotation_master_model->total_amount_after_discount=$sum;
            $sales_quotation_master_model->save();
        }
        return $sum;
    }


    public function getMrpSum($qtid, $revision, $status) {
        // Create a new CDbCriteria instance
        $criteria = new CDbCriteria();
        $criteria->select = 'SUM(mrp) as mrpsum';
        $criteria->addCondition('deleted_status = 1');
        $criteria->addCondition('master_id = :qtid');
        $criteria->params[':qtid'] = $qtid;

        if ($status != NULL && $status == 1) {
            // When status is 1, use addInCondition for multiple revisions
            $criteria->addInCondition('revision_no', explode(',', $revision));
        } else {
            // Otherwise, use addCondition for a single revision
            $criteria->addCondition('revision_no = :revision');
            $criteria->params[':revision'] = $revision;
        }

        // Execute the query using Yii's Active Record
        $result = SalesQuotation::model()->find($criteria);
        return $result ? $result->mrpsum : 0;
    }
    public function getSectionSum($id, $qtid, $revision) {
        // Check subsection count
        $sql = 'SELECT COUNT(*) FROM `jp_quotation_gen_category` WHERE `qid` = :qtid AND `section_id` = :id';
        $subsectionCount = Yii::app()->db->createCommand($sql)
            ->bindValue(':qtid', $qtid)
            ->bindValue(':id', $id)
            ->queryScalar();
    
        // Initialize sum
        $sum = 0;
    
        // If no subsections, get the section amount directly
        if ($subsectionCount == 0) {
            $sql = 'SELECT amount_after_discount FROM `jp_quotation_section` WHERE `qtn_id` = :qtid AND id = :id';
            $sum = Yii::app()->db->createCommand($sql)
                ->bindValue(':qtid', $qtid)
                ->bindValue(':id', $id)
                ->queryScalar();
        } else {
            // Get subsections and their sums
            $sql = 'SELECT id FROM `jp_quotation_gen_category` WHERE `qid` = :qtid AND `section_id` = :id';
            $subsectionArray = Yii::app()->db->createCommand($sql)
                ->bindValue(':qtid', $qtid)
                ->bindValue(':id', $id)
                ->queryAll();
    
            foreach ($subsectionArray as $value) {
                $sum += $this->getSubSectionSum($value['id'], $qtid, $id, $revision);
            }
    
            // If sum is still zero, get the section amount directly
            if ($sum == 0) {
                $sql = 'SELECT amount_after_discount FROM `jp_quotation_section` WHERE `qtn_id` = :qtid AND id = :id';
                $sum = Yii::app()->db->createCommand($sql)
                    ->bindValue(':qtid', $qtid)
                    ->bindValue(':id', $id)
                    ->queryScalar();
            }
        }
    
        return $sum;
    }
    
    public function getSubSectionSum($id, $qtid, $sectionId, $revision) {
        // Check subitem count
        $sql = 'SELECT COUNT(*) FROM `jp_quotation_gen_worktype` WHERE `qid` = :qtid AND `section_id` = :sectionId AND `category_label_id` = :id';
        $subitemCount = Yii::app()->db->createCommand($sql)
            ->bindValue(':qtid', $qtid)
            ->bindValue(':sectionId', $sectionId)
            ->bindValue(':id', $id)
            ->queryScalar();
    
        // Initialize sum
        $sum = 0;
    
        // If no subitems, get the subcategory amount directly
        if ($subitemCount == 0) {
            $sql = 'SELECT amount_after_discount FROM `jp_quotation_gen_category` WHERE `qid` = :qtid AND id = :id';
            $sum = Yii::app()->db->createCommand($sql)
                ->bindValue(':qtid', $qtid)
                ->bindValue(':id', $id)
                ->queryScalar();
        } else {
            // Get subitems and their amounts
            $sql = 'SELECT id, amount_after_discount FROM `jp_quotation_gen_worktype` WHERE `qid` = :qtid AND `section_id` = :sectionId AND `category_label_id` = :id';
            $subitemArray = Yii::app()->db->createCommand($sql)
                ->bindValue(':qtid', $qtid)
                ->bindValue(':sectionId', $sectionId)
                ->bindValue(':id', $id)
                ->queryAll();
    
            foreach ($subitemArray as $value) {
                $sql = 'SELECT COUNT(*) AS count, SUM(amount_after_discount) AS amount_after_discount FROM `jp_sales_quotation` WHERE `parent_type` = 1 AND `parent_id` = :parentId AND `deleted_status` = 1';
                $wrktypeitem = Yii::app()->db->createCommand($sql)
                    ->bindValue(':parentId', $value['id'])
                    ->queryRow();
    
                if ($wrktypeitem['count'] > 0) {
                    $sum += $wrktypeitem['amount_after_discount'];
                } else {
                    $sum += $value['amount_after_discount'];
                }
            }
        }
    
        return $sum;
    }
    
    public function getSectionSumForMrp($id, $qtid, $revision) {
        $db = Yii::app()->db;
    
        // Check if there are any subsections
        $sql = 'SELECT COUNT(*) FROM `jp_quotation_gen_category` WHERE `qid` = :qtid AND `section_id` = :id';
        $subsectionCount = $db->createCommand($sql)
            ->bindValue(':qtid', $qtid)
            ->bindValue(':id', $id)
            ->queryScalar();
    
        if ($subsectionCount == 0) {
            // If no subsections, get the mrp from `jp_quotation_section`
            $sql = 'SELECT mrp FROM `jp_quotation_section` WHERE `qtn_id` = :qtid AND `id` = :id';
            return $db->createCommand($sql)
                ->bindValue(':qtid', $qtid)
                ->bindValue(':id', $id)
                ->queryScalar();
        } else {
            // If there are subsections, get their IDs and sum their MRP values recursively
            $sql = 'SELECT id FROM `jp_quotation_gen_category` WHERE `qid` = :qtid AND `section_id` = :id';
            $subsectionArray = $db->createCommand($sql)
                ->bindValue(':qtid', $qtid)
                ->bindValue(':id', $id)
                ->queryAll();
    
            $sum = 0;
            foreach ($subsectionArray as $value) {
                $sum += $this->getSubSectionSumForMrp($value['id'], $qtid, $id, $revision);
            }
    
            if ($sum == 0) {
                $sql = 'SELECT mrp FROM `jp_quotation_section` WHERE `qtn_id` = :qtid AND `id` = :id';
                return $db->createCommand($sql)
                    ->bindValue(':qtid', $qtid)
                    ->bindValue(':id', $id)
                    ->queryScalar();
            }
    
            return $sum;
        }
    }
    
    public function getSubSectionSumForMrp($id, $qtid, $sectionId, $revision) {
        $db = Yii::app()->db;
    
        // Check if there are any subitems
        $sql = 'SELECT COUNT(*) FROM `jp_quotation_gen_worktype` WHERE `qid` = :qtid AND `section_id` = :sectionId AND `category_label_id` = :id';
        $subitemCount = $db->createCommand($sql)
            ->bindValue(':qtid', $qtid)
            ->bindValue(':sectionId', $sectionId)
            ->bindValue(':id', $id)
            ->queryScalar();
    
        if ($subitemCount == 0) {
            // If no subitems, get the mrp from `jp_quotation_gen_category`
            $sql = 'SELECT mrp FROM `jp_quotation_gen_category` WHERE `qid` = :qtid AND id = :id';
            return $db->createCommand($sql)
                ->bindValue(':qtid', $qtid)
                ->bindValue(':id', $id)
                ->queryScalar();
        } else {
            // If there are subitems, get their IDs and MRP values
            $sql = 'SELECT id, mrp FROM `jp_quotation_gen_worktype` WHERE `qid` = :qtid AND `section_id` = :sectionId AND `category_label_id` = :id';
            $subitemArray = $db->createCommand($sql)
                ->bindValue(':qtid', $qtid)
                ->bindValue(':sectionId', $sectionId)
                ->bindValue(':id', $id)
                ->queryAll();
    
            $sum = 0;
            foreach ($subitemArray as $value) {
                // Check if there are sales quotations for each subitem
                $sql = 'SELECT COUNT(*) AS count, SUM(mrp) AS mrp FROM `jp_sales_quotation` WHERE `parent_type` = 1 AND `parent_id` = :parentId AND `deleted_status` = 1';
                $wrktypeitem = $db->createCommand($sql)
                    ->bindValue(':parentId', $value['id'])
                    ->queryRow();
    
                if ($wrktypeitem['count'] > 0) {
                    $sum += $wrktypeitem['mrp'];
                } else {
                    $sum += $value['mrp'];
                }
            }
            return $sum;
        }
    }
        
      
    public function getSubItemSum($qtid, $parentId, $revision) {
        $sql = 'SELECT SUM(amount_after_discount) '
            . ' as  amount_after_discount'
            . ' FROM `jp_sales_quotation` WHERE master_id='.$qtid
            . ' AND `parent_type` = 1 '
            . ' AND `parent_id` = ' . $parentId
            . ' AND `deleted_status` = 1';
        $subItemsum = Yii::app()->db->createCommand($sql)->queryScalar();
        return $subItemsum;

    }

    public function customAssets($file_location, $localFilePath = null)
	{
		if (defined('ASSETS_SOURCES') and ASSETS_SOURCES === 'offline') {
			return Yii::app()->theme->baseUrl . $localFilePath;
		} else {
			return $file_location;
		}
	}

    public function getAssignProjectTableName(){
        $sql = "SELECT table_name FROM information_schema.tables "
                . " WHERE table_schema = '"._DB."' "
                . " AND table_name = 'pms_assignpro_work_type'";
        $res = Yii::app()->db->createCommand($sql)->queryScalar();;
        $tableName = ($res !==FALSE) ? 'pms_assignpro_work_type':'jp_assignpro_work_type';
        return $tableName;
    }

    public function getPendingDeliveries(){
        
        $pending_deliveries_array = array();
        $whbillids = array(); 
        $pending_deliveries = array();
        $sql = "SELECT warehousereceipt_bill_id "
            . " FROM jp_warehousereceipt "
            . " WHERE warehousereceipt_transfer_type=1";
        $wh_receipt_data = Yii::app()->db->createCommand($sql)->queryAll();
        foreach ($wh_receipt_data as $key => $value) {
            if($value['warehousereceipt_bill_id']!=""){
                $whbillids[] = $value['warehousereceipt_bill_id'];
            }
        }

        $wh_receipt_billid = implode(",", $whbillids);
        if($wh_receipt_billid !=""){
            $sql="SELECT * "
                . " FROM `jp_bills` WHERE "
                . " bill_id NOT IN($wh_receipt_billid)";
            $pending_deliveries = Yii::app()->db->createCommand($sql)->queryAll();
        }

        $pending_count = count($pending_deliveries);
        if(!empty($pending_deliveries)){            
            foreach ($pending_deliveries as $key => $value) {            
                $warehouse = Warehouse::model()->findByPk($value['warehouse_id']);
                $warehouse_name =  isset($warehouse['warehouse_name'])?$warehouse['warehouse_name']:"Nil";
                $pending_deliveries_array[$warehouse_name][]=$value;
            }
        }

        return array(
            'pending_count'=>$pending_count,
            'pending_deliveries_array'=>$pending_deliveries_array);
    }

    public function getMRStatus($pid){
        $sql = "SELECT `requisition_status` "
        . " FROM `pms_material_requisition` "
        . " WHERE `purchase_id` =".$pid;
        $mr_status = Yii::app()->db->createCommand($sql)->queryScalar();
        echo ($mr_status=="")?"No MR":$mr_status;
    }

    public function getMainSectionSum($id, $qtid, $revision){
        $sum = 0;
        $sql = 'SELECT * FROM `jp_quotation_gen_category` '
                . ' WHERE `qid` = ' . $qtid
                . ' AND `section_id` = ' . $id;
                
        $subsectionArray = Yii::app()->db->createCommand($sql)->queryAll();
        foreach ($subsectionArray as $value) {
            $sum += $this->getSubSectionSum($value['id'], $qtid, $id, $revision);
        }
        $sql = 'SELECT SUM(amount_after_discount) '
            . ' as  amount_after_discount'
            . ' FROM `jp_sales_quotation` WHERE master_id='.$qtid
            . ' AND `parent_type` = 0 '
            . ' AND `parent_id` = ' . $id
            . ' AND `deleted_status` = 1';
           
        $subItemsum = Yii::app()->db->createCommand($sql)->queryScalar();
        $sum += $subItemsum;

        echo Controller::money_format_inr($sum,2);
    }

    public function getSubSectionMrp($id, $qtid, $sectionId, $revision) {
        $sql = 'SELECT count(*) FROM `jp_quotation_gen_worktype` '
                . ' WHERE `qid` = ' . $qtid
                . ' AND `section_id` = ' . $sectionId
                . ' AND `category_label_id` = ' . $id;
        $subitemCount = Yii::app()->db->createCommand($sql)->queryScalar();

        $sum = 0;
        if ($subitemCount == 0) {
            $sql = 'SELECT mrp  '
                    . ' FROM `jp_quotation_gen_category` '
                    . ' WHERE `qid` = ' . $qtid . ' AND id=' . $id;
            $sum = Yii::app()->db->createCommand($sql)->queryScalar();
        } else {
            $sql = 'SELECT * FROM `jp_quotation_gen_worktype` '
                    . ' WHERE `qid` = ' . $qtid . ' AND `section_id` = ' . $sectionId
                    . ' AND `category_label_id` = ' . $id;
            $subitemArray = Yii::app()->db->createCommand($sql)->queryAll();

            foreach ($subitemArray as $value) {
                $sql = 'SELECT count(*)as count , SUM(mrp) '
                        . ' as  mrp'
                        . ' FROM `jp_sales_quotation` WHERE `parent_type` = 1 '
                        . ' AND `parent_id` = ' . $value['id']
                        . ' AND `deleted_status` = 1';
                $wrktypeitem = Yii::app()->db->createCommand($sql)->queryRow();

                $sum += $value['mrp'];
                if ($wrktypeitem['count'] > 0) {

                    $sum += $wrktypeitem['mrp'];
                }
            }
        }
        return $sum;
    }

    public function getDeliveryStatus($pid){

        $deliver_status = "Not Received";
        $sql = "SELECT SUM(quantity) as pquantity "
            . " FROM `jp_purchase_items` WHERE `purchase_id` = ".$pid;
        $purchaseitem_qty = Yii::app()->db->createCommand($sql)->queryScalar();


        $sql = "SELECT bi.bill_id,bi.billitem_id "
        . " FROM jp_billitem bi "
        . " LEFT JOIN jp_bills b ON b.bill_id=bi.bill_id "
        . " LEFT JOIN jp_purchase p ON b.purchase_id=p.p_id "
        . " WHERE p.p_id = ".$pid ." GROUP BY bi.bill_id";
        $bill_array = yii::app()->db->createCommand($sql)->queryAll();

        $bill_list = array();
        if (!empty($bill_array)) {
            foreach ($bill_array as $entry) {                
                $bill_list[] = $entry['bill_id'];                
            }
            $bill_ids = implode(',', $bill_list);
            $sql = "SELECT SUM(`warehousereceipt_quantity`) "
                . " FROM `jp_warehousereceipt` "
                . " WHERE warehousereceipt_bill_id "
                . " IN($bill_ids)";
            $receiptqty = Yii::app()->db->createCommand($sql)->queryScalar();
            if($receiptqty == 0){
                $deliver_status = "Not Received";
            }else if($purchaseitem_qty > $receiptqty){
                $deliver_status = "Partially Received";
            }else if($purchaseitem_qty == $receiptqty){
                $deliver_status = "Fully Received";
            }            
        }
        echo $deliver_status;                

    }

    public function getDashboardData($currentMonthStart, $currentMonthEnd, $previousMonthStart, $previousMonthEnd,$companyId,$range) {

        
        // Fetch accounts payable data
        $payableData = $this->getPayableDashboard($currentMonthStart,$currentMonthEnd,$previousMonthStart,$previousMonthEnd,$companyId);
        // Fetch accounts receivable data
        $receivableData = $this->getPaymentReceivable($currentMonthStart,$currentMonthEnd,$previousMonthStart,$previousMonthEnd,$companyId);
        //Cashflow
        $cashFlowData=$this->getCashFlow($currentMonthStart,$currentMonthEnd,$previousMonthStart,$previousMonthEnd,$companyId);
        //netprofit
        $netProfitData=$this->getNetProfit($currentMonthStart,$currentMonthEnd,$previousMonthStart,$previousMonthEnd,$companyId);
        //Gross Profit
        $grossProfitData=$this->getGrossprofit($currentMonthStart,$currentMonthEnd,$previousMonthStart,$previousMonthEnd,$companyId);
        // Merge the arrays
        $range1=array();
        $range1=["range"=>$range];
        $dashboardData = array_merge($payableData,$receivableData,$cashFlowData,$grossProfitData,$netProfitData,$range1);
        return $dashboardData;
    }

    public function getSetDate($currentMonthStart, $range)
    {
        $intervals = [];
        $startDate = new DateTime($currentMonthStart);
        $start = clone $startDate;
        if ($range == 30) { // 6 data points with 5-day gap
            for ($i = 0; $i < 30; $i++) {
                $start = clone $startDate; // Clone to preserve original value
                $end = clone $startDate;
                $end->modify('+0 days');
    
                $intervals[] = [
                    'start' => $start->format('Y-m-d'),
                    'end' => $end->format('Y-m-d')
                ];
    
                $startDate->modify('+1 days'); // Move start date ahead by 6 days
            }
        } elseif ($range == 6) { // 6 data points with 1-month gap
            for ($i = 0; $i < 6; $i++) {
                $start = clone $startDate;
                $end = clone $startDate;
                $end->modify('+1 month')->modify('-1 day');
    
                $intervals[] = [
                    'start' => $start->format('Y-m-01'),
                    'end' => $end->format('Y-m-t')
                ];
    
                $startDate->modify('+1 month'); // Move start date ahead by 1 month
            }
        } elseif ($range == 1) { // 6 data points with 2-month gap
            for ($i = 0; $i < 6; $i++) {
                $start = clone $startDate;
                $end = clone $startDate;
                $end->modify('+2 months')->modify('-1 day');
    
                $intervals[] = [
                    'start' => $start->format('Y-m-01'),
                    'end' => $end->format('Y-m-t')
                ];
    
                $startDate->modify('+2 months'); // Move start date ahead by 2 months
            }
        }
        // echo '<pre>';print_r($intervals);exit;
        return $intervals;
    }

    public function getVendorAgingSummary(){
        $bill_list = array();
        $bills = "";
        $condition = "";
        $command = Yii::app()->db->createCommand()->select('b.bill_id as billId');
        $command->from($this->tableNameAcc('expenses', 0) . ' as e');
        $command->leftJoin($this->tableNameAcc('bills', 0) . ' as b', 'e.bill_id = b.bill_id ');
        $command->where("e.type = 73 AND b.bill_totalamount IS NOT NULL AND b.purchase_id IS NOT NULL ");
        $billsArray = $command->queryAll();

        if (!empty($billsArray)) {
            foreach ($billsArray as $entry) {
                if ($entry['billId'] != "") {
                    $bill_list[] = $entry['billId'];
                }
            }
            $bills = implode(',', $bill_list);
            $condition = "AND b.bill_id NOT IN ($bills)";
        }
        $sql = "(SELECT b.bill_id as bill_id,b.bill_number as bill_number,b.bill_date as bill_date,"
            . " v.name as name,v.payment_date_range as  payment_date_range,'vendor' as type "
            . " FROM jp_bills b LEFT JOIN jp_purchase p "
            . " ON b.purchase_id = p.p_id "
            . "LEFT JOIN jp_vendors v ON v.vendor_id = p.vendor_id  "
            . "WHERE 1=1 $condition) UNION ALL ( SELECT 
            sb.id AS bill_id,
            sb.bill_number AS bill_number,
            sb.date AS bill_date,
            s.subcontractor_name AS name,
            NULL AS payment_date_range,
            'subcontractor' AS type 
        FROM 
            jp_subcontractorbill sb 
        LEFT JOIN 
            jp_subcontractor_payment sp ON sp.sc_bill_id = sb.id 
        LEFT JOIN 
            jp_scquotation sq ON sq.scquotation_id = sb.scquotation_id
        LEFT JOIN 
            jp_subcontractor s ON s.subcontractor_id = sq.subcontractor_id 
        GROUP BY 
            sb.id,s.subcontractor_name
        HAVING 
            COALESCE(SUM(sp.amount), 0) = 0 OR COALESCE(SUM(sp.amount), 0) < MAX(sb.total_amount))";
            
        $aging_array = Yii::app()->db->createCommand($sql)->queryAll();
        return $aging_array;
    }
    //payable line
    public function getLineGraphData($currentMonthStart, $currentMonthEnd,$companyId,$range){
        $dateSet = $this->getSetDate($currentMonthStart,$range);
        // echo '<pre>';print_r($dateSet);exit;
        $payableDataGraph = [];
        for ($i = 0; $i < count($dateSet); $i++) {

            $startMonth = date('M', strtotime($dateSet[$i]['start']));
            $endMonth = date('M', strtotime($dateSet[$i]['end']));
            $formattedDate = $startMonth . '-' . $endMonth;

            $payableDataGraph[] = [
                'start' => $dateSet[$i]['start'],
                'date' => date('M', strtotime($dateSet[$i]['start'])),
                'end'   => $dateSet[$i]['end'],
                'data'  => $this->getPayableDashboardLine($dateSet[$i]['start'], $dateSet[$i]['end'], $companyId),
                'range'=> $range,
                'year' => $formattedDate,
            ];
        }
        return $payableDataGraph;
        // echo '<pre>';print_r($payableDataGraph);exit;
    }
    //receivable line 
    public function getLineGraphReceivable($currentMonthStart, $currentMonthEnd,$companyId,$range){
        $dateSet = $this->getSetDate($currentMonthStart,$range);
            
       // echo '<pre>';print_r($dateSet);exit;
        $receivableDataGraph = [];
        for ($i = 0; $i < count($dateSet); $i++) {

            $startMonth = date('M', strtotime($dateSet[$i]['start']));
            $endMonth = date('M', strtotime($dateSet[$i]['end']));
            $formattedDate = $startMonth . '-' . $endMonth;

            $receivableDataGraph[] = [
                'start' => $dateSet[$i]['start'],
                'date' => date('M', strtotime($dateSet[$i]['start'])),
                'end'   => $dateSet[$i]['end'],
                'data'  => $this->getPaymentReceivableLine($dateSet[$i]['start'], $dateSet[$i]['end'], $companyId),
                'range'=> $range,
                'year' => $formattedDate,
            ];
        }
        return $receivableDataGraph;
        // echo '<pre>';print_r($receivableDataGraph);exit;
    }
    //cashflow line
    public function getLineGraphCashFlow($currentMonthStart, $currentMonthEnd,$companyId,$range){
        $dateSet = $this->getSetDate($currentMonthStart,$range);
        // echo '<pre>';print_r($dateSet);exit;

        $cashflowDataGraph = [];
        for ($i = 0; $i < count($dateSet); $i++) {
            $startMonth = date('M', strtotime($dateSet[$i]['start']));
            $endMonth = date('M', strtotime($dateSet[$i]['end']));
            $formattedDate = $startMonth . '-' . $endMonth;

            $cashflowDataGraph[] = [
                'start' => $dateSet[$i]['start'],
                'date' => date('M', strtotime($dateSet[$i]['start'])),
                'end'   => $dateSet[$i]['end'],
                'data'  => $this->getCashFlowLine($dateSet[$i]['start'], $dateSet[$i]['end'], $companyId),
                'range'=> $range,
                'year' => $formattedDate,
            ];
        }
        return $cashflowDataGraph;
        // echo '<pre>';print_r($receivableDataGraph);exit;
    }
    //Net profit line 
    public function getLineGraphNetProfit($currentMonthStart, $currentMonthEnd,$companyId,$range){
        $dateSet = $this->getSetDate($currentMonthStart,$range);
        // echo '<pre>';print_r($dateSet);exit;
        

        $profitDataGraph = [];
        for ($i = 0; $i < count($dateSet); $i++) {
            $startMonth = date('M', strtotime($dateSet[$i]['start']));
        $endMonth = date('M', strtotime($dateSet[$i]['end']));
        $formattedDate = $startMonth . '-' . $endMonth;
            $profitDataGraph[] = [
                'start' => $dateSet[$i]['start'],
                'date' => date('M', strtotime($dateSet[$i]['start'])),
                'end'   => $dateSet[$i]['end'],
                'data'  => $this->getNetProfitLine($dateSet[$i]['start'], $dateSet[$i]['end'], $companyId),
                'range'=> $range,
                'year' => $formattedDate,
            ];
        }
        return $profitDataGraph;
        // echo '<pre>';print_r($receivableDataGraph);exit;
    }
    //gross profit
    public function getLineGraphGrossProfit($currentMonthStart, $currentMonthEnd,$companyId,$range){
        $dateSet = $this->getSetDate($currentMonthStart,$range);

        // echo '<pre>';print_r($dateSet);exit;
        $grossDataGraph = [];
        for ($i = 0; $i < count($dateSet); $i++) {
            $startMonth = date('M', strtotime($dateSet[$i]['start']));
        $endMonth = date('M', strtotime($dateSet[$i]['end']));
        $formattedDate = $startMonth . '-' . $endMonth;
            $grossDataGraph[] = [
                'start' => $dateSet[$i]['start'],
                'date' => date('M', strtotime($dateSet[$i]['start'])),
                'end'   => $dateSet[$i]['end'],
                'data'  => $this->getGrossprofitLine($dateSet[$i]['start'], $dateSet[$i]['end'], $companyId),
                'range'=> $range,
                'year' => $formattedDate,
            ];
        }
        return $grossDataGraph;
        // echo '<pre>';print_r($receivableDataGraph);exit;
    }
    public function getCashFlowLine($currentMonthStart, $currentMonthEnd,$companyId)
    {
        // Define date ranges
        $where='';
        $tblpx = Yii::app()->db->tablePrefix;
        $where = " WHERE {$tblpx}expenses.type = 72"; 
        if (!empty($companyId)) {
            $company=$companyId;
        }
        else{
            $company='';
        }
        $receipts = $this->getDaybookTransctionsDashboard($where,$company);
        $totalReceiptAmountCurrentMonth = 0;
        $totalReceiptAmountPreviousMonth = 0;
        
        // Loop through receipts to calculate totals
        foreach ($receipts as $receipt) {
            $receiptDate = $receipt['date']; // Assuming 'date' is in Y-m-d format
            $receiptAmount = (float)$receipt['receipt']; // Convert to float to ensure proper calculation
        
            if ($receiptDate >= $currentMonthStart && $receiptDate <= $currentMonthEnd) {
                $totalReceiptAmountCurrentMonth += $receiptAmount;
            }
        }
        // echo'<pre>';print_r($receipts);exit;
        // Fetch data 
        
        
        // echo '<pre>';print_r($dayBookPayments);exit;
        $totalDayBookPaymentAmountCurrentMonth = 0;
        $totalDayBookPaymentAmountPreviousMonth = 0;
        $totalDayBookPaymentAmountCurrentMonth = $this->getDaybookPaymentDashboard($currentMonthStart,$currentMonthEnd,$company);
        // $totalDayBookPaymentAmountPreviousMonth = $this->getDaybookPaymentDashboard($previousMonthStart,$previousMonthEnd,$company);

        // echo'<pre>';print_r($totalDayBookPaymentAmountCurrentMonth);exit;
        // print_r($totaldayBookPaymentAmount);exit;
        $criteria = new CDbCriteria();

        if (!empty($companyId)) {
            $criteria->addCondition("company_id = :companyId");
            $criteria->params[':companyId'] = $companyId;
            $newQuery = "";
            // foreach ($arrVal as $arr) {
                $newQuery .= " FIND_IN_SET('" . $companyId . "', company_id)";
            // }
        }
        else{
            $user = Users::model()->findByPk(Yii::app()->user->id);
            $arrVal = explode(',', $user->company_id);
            $newQuery = "";
            foreach ($arrVal as $arr) {
                if ($newQuery) $newQuery .= ' OR';
                $newQuery .= " FIND_IN_SET('" . $arr . "', company_id)";
            }
        }
        
        $dailyExpenses = Yii::app()->db->createCommand("SELECT SUM(amount) as total_amount FROM {$tblpx}dailyexpense WHERE date BETWEEN '" .  $currentMonthStart . "' AND '" . $currentMonthEnd . "' AND expense_type IN(88,89) AND (" . $newQuery . ")")->queryRow();

        // $dailyExpensesPrevious = Yii::app()->db->createCommand("SELECT SUM(amount) as total_amount FROM {$tblpx}dailyexpense WHERE date BETWEEN '" .  $previousMonthStart . "' AND '" . $previousMonthEnd . "' AND expense_type IN(88,89) AND (" . $newQuery . ")")->queryRow();

        $totalDailyExpenseCurrentMonth=0;
        $totalDailyExpensePreviousMonth=0;
        $totalDailyExpenseCurrentMonth = $dailyExpenses['total_amount'];
        // $totalDailyExpensePreviousMonth = $dailyExpensesPrevious['total_amount'];
        

        // Initialize totals
        $currentMonthTotal = $totalReceiptAmountCurrentMonth-($totalDayBookPaymentAmountCurrentMonth+$totalDailyExpenseCurrentMonth);

        // $previousMonthTotal = $totalReceiptAmountPreviousMonth-( $totalDayBookPaymentAmountPreviousMonth+$totalDailyExpensePreviousMonth);

        // echo'<pre>';print_r($totalDailyExpenseCurrentMonth);exit;
        // Format the output
        $dataValues = [
            [
                'name' => 'Cash Flow',
                'current' => $currentMonthTotal,
                // 'previous' => $previousMonthTotal
            ]
        ];
        // Return or use $dataValues as needed
        return $currentMonthTotal;
    }
    public function getNetProfitLine($currentMonthStart, $currentMonthEnd,$companyId){
        $criteria = new CDbCriteria();
        $tblpx = Yii::app()->db->tablePrefix;
        // if (!empty($companyId)) {
        //     $criteria->addCondition("company_id = :companyId");
        //     $criteria->params[':companyId'] = $companyId;
        // }
        if (!empty($companyId)) {
            $criteria->addCondition("company_id = :companyId");
            $criteria->params[':companyId'] = $companyId;
            $newQuery = "";
            // foreach ($arrVal as $arr) {
                $newQuery .= " FIND_IN_SET('" . $companyId . "', company_id)";
            // }
        }
        else{
            $user = Users::model()->findByPk(Yii::app()->user->id);
            $arrVal = explode(',', $user->company_id);
            $newQuery = "";
            foreach ($arrVal as $arr) {
                if ($newQuery) $newQuery .= ' OR';
                $newQuery .= " FIND_IN_SET('" . $arr . "', company_id)";
            }
        }

        $dailyExpenses = Yii::app()->db->createCommand("SELECT SUM(amount) as total_amount FROM {$tblpx}dailyexpense WHERE date BETWEEN '" .  $currentMonthStart . "' AND '" . $currentMonthEnd . "' AND expense_type IN(88,89) AND (" . $newQuery . ")")->queryRow();

        // $dailyExpensesPrevious = Yii::app()->db->createCommand("SELECT SUM(amount) as total_amount FROM {$tblpx}dailyexpense WHERE date BETWEEN '" .  $previousMonthStart . "' AND '" . $previousMonthEnd . "' AND expense_type IN(88,89) AND (" . $newQuery . ")")->queryRow();

        $totalDailyExpenseCurrentMonth=0;
        $totalDailyExpensePreviousMonth=0;
        $totalDailyExpenseCurrentMonth = $dailyExpenses['total_amount'];
        // $totalDailyExpensePreviousMonth = $dailyExpensesPrevious['total_amount'];
        // Define date ranges for current and previous months
    
        $grossProfit = $this->getGrossprofitLine($currentMonthStart,$currentMonthEnd,$companyId);
        // $currentGrossProfit = $grossProfit[0]['current'];
        // $previousGrossProfit = $grossProfit[0]['previous'];

        $currentNetProfit = $grossProfit - $totalDailyExpenseCurrentMonth;
        // $previousNetProfit = $previousGrossProfit - $totalDailyExpensePreviousMonth;
    
        // Prepare data values
        // $dataValues = [
        //     [
        //         'name' => 'Net Profit',
        //         'current' => $currentNetProfit,
        //         'previous' => $previousNetProfit
        //     ]
        // ];
        return $currentNetProfit;


        
    }
    public function getGrossprofitLine($currentMonthStart, $currentMonthEnd,$companyId)
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $criteria = new CDbCriteria();
        if (!empty($companyId)) {
            $criteria->addCondition("company_id = :companyId");
            $criteria->params[':companyId'] = $companyId;
        }
        // Retrieve all records
        $salesCurrent = $this->getSalesInvoice($currentMonthStart,$currentMonthEnd,$companyId);
        // $salesPrevious = $this->getSalesInvoice($previousMonthStart,$previousMonthEnd,$companyId);
        $billsCurrent = $this->getSalesBill($currentMonthStart,$currentMonthEnd,$companyId);
        // $billsPrevious = $this->getSalesBill($previousMonthStart,$previousMonthEnd,$companyId);
        
        $quotationCurrent = $this->getScquotation($currentMonthStart,$currentMonthEnd,$companyId);
        // $quotationPrevious = $this->getScquotation($previousMonthStart,$previousMonthEnd,$companyId);
        

        // Initialize totals
        $currentMonthTotalSales = 0;
        $previousMonthTotalSales = 0;
        $currentMonthTotalBills = 0;
        $previousMonthTotalBills = 0;
        $currentMonthTotalSub = 0;
        $previousMonthTotalSub = 0;
        
        // Calculate gross profit
        $currentMonthGrossProfit = $salesCurrent - ($billsCurrent + $quotationCurrent);
        // $previousMonthGrossProfit = $salesPrevious - ($billsPrevious + $quotationPrevious);

        // Format the result
        // $dataValues = [
        //     [
        //         'name' => 'Gross Profit',
        //         'current' => $currentMonthGrossProfit,
        //         'previous' => $previousMonthGrossProfit
        //     ]
        // ];
        return $currentMonthGrossProfit;
    }

    public function getCashFlow($currentMonthStart,$currentMonthEnd,$previousMonthStart,$previousMonthEnd,$companyId)
    {
        // Define date ranges
        $where='';
        $tblpx = Yii::app()->db->tablePrefix;
        $where = " WHERE {$tblpx}expenses.type = 72"; 
        if (!empty($companyId)) {
            $company=$companyId;
        }
        else{
            $company='';
        }
        $receipts = $this->getDaybookTransctionsDashboard($where,$company);
        $totalReceiptAmountCurrentMonth = 0;
        $totalReceiptAmountPreviousMonth = 0;
        
        // Loop through receipts to calculate totals
        foreach ($receipts as $receipt) {
            $receiptDate = $receipt['date']; // Assuming 'date' is in Y-m-d format
            $receiptAmount = (float)$receipt['receipt']; // Convert to float to ensure proper calculation
        
            if ($receiptDate >= $currentMonthStart && $receiptDate <= $currentMonthEnd) {
                $totalReceiptAmountCurrentMonth += $receiptAmount;
            } elseif ($receiptDate >= $previousMonthStart && $receiptDate <= $previousMonthEnd) {
                $totalReceiptAmountPreviousMonth += $receiptAmount;
            }
        }
        // echo'<pre>';print_r($receipts);exit;
        // Fetch data 
        
        
        // echo '<pre>';print_r($dayBookPayments);exit;
        $totalDayBookPaymentAmountCurrentMonth = 0;
        $totalDayBookPaymentAmountPreviousMonth = 0;
        $totalDayBookPaymentAmountCurrentMonth = $this->getDaybookPaymentDashboard($currentMonthStart,$currentMonthEnd,$company);
        $totalDayBookPaymentAmountPreviousMonth = $this->getDaybookPaymentDashboard($previousMonthStart,$previousMonthEnd,$company);

        // echo'<pre>';print_r($totalDayBookPaymentAmountCurrentMonth);exit;
        // print_r($totaldayBookPaymentAmount);exit;
        $criteria = new CDbCriteria();

        if (!empty($companyId)) {
            $criteria->addCondition("company_id = :companyId");
            $criteria->params[':companyId'] = $companyId;
            $newQuery = "";
            // foreach ($arrVal as $arr) {
                $newQuery .= " FIND_IN_SET('" . $companyId . "', company_id)";
            // }
        }
        else{
            $user = Users::model()->findByPk(Yii::app()->user->id);
            $arrVal = explode(',', $user->company_id);
            $newQuery = "";
            foreach ($arrVal as $arr) {
                if ($newQuery) $newQuery .= ' OR';
                $newQuery .= " FIND_IN_SET('" . $arr . "', company_id)";
            }
        }
        
        $dailyExpenses = Yii::app()->db->createCommand("SELECT SUM(amount) as total_amount FROM {$tblpx}dailyexpense WHERE date BETWEEN '" .  $currentMonthStart . "' AND '" . $currentMonthEnd . "' AND expense_type IN(88,89) AND (" . $newQuery . ")")->queryRow();

        $dailyExpensesPrevious = Yii::app()->db->createCommand("SELECT SUM(amount) as total_amount FROM {$tblpx}dailyexpense WHERE date BETWEEN '" .  $previousMonthStart . "' AND '" . $previousMonthEnd . "' AND expense_type IN(88,89) AND (" . $newQuery . ")")->queryRow();

        $totalDailyExpenseCurrentMonth=0;
        $totalDailyExpensePreviousMonth=0;
        $totalDailyExpenseCurrentMonth = $dailyExpenses['total_amount'];
        $totalDailyExpensePreviousMonth = $dailyExpensesPrevious['total_amount'];
        

        // Initialize totals
        $currentMonthTotal = $totalReceiptAmountCurrentMonth-($totalDayBookPaymentAmountCurrentMonth+$totalDailyExpenseCurrentMonth);

        $previousMonthTotal = $totalReceiptAmountPreviousMonth-( $totalDayBookPaymentAmountPreviousMonth+$totalDailyExpensePreviousMonth);

        // echo'<pre>';print_r($totalDailyExpenseCurrentMonth);exit;
        // Format the output
        $dataValues = [
            [
                'name' => 'Cash Flow',
                'current' => $currentMonthTotal,
                'previous' => $previousMonthTotal
            ]
        ];
        // Return or use $dataValues as needed
        return $dataValues;
    }
    public function getNetProfit($currentMonthStart,$currentMonthEnd,$previousMonthStart,$previousMonthEnd,$companyId){
        $criteria = new CDbCriteria();
        $tblpx = Yii::app()->db->tablePrefix;
        // if (!empty($companyId)) {
        //     $criteria->addCondition("company_id = :companyId");
        //     $criteria->params[':companyId'] = $companyId;
        // }
        if (!empty($companyId)) {
            $criteria->addCondition("company_id = :companyId");
            $criteria->params[':companyId'] = $companyId;
            $newQuery = "";
            // foreach ($arrVal as $arr) {
                $newQuery .= " FIND_IN_SET('" . $companyId . "', company_id)";
            // }
        }
        else{
            $user = Users::model()->findByPk(Yii::app()->user->id);
            $arrVal = explode(',', $user->company_id);
            $newQuery = "";
            foreach ($arrVal as $arr) {
                if ($newQuery) $newQuery .= ' OR';
                $newQuery .= " FIND_IN_SET('" . $arr . "', company_id)";
            }
        }

        $dailyExpenses = Yii::app()->db->createCommand("SELECT SUM(amount) as total_amount FROM {$tblpx}dailyexpense WHERE date BETWEEN '" .  $currentMonthStart . "' AND '" . $currentMonthEnd . "' AND expense_type IN(88,89) AND (" . $newQuery . ")")->queryRow();

        $dailyExpensesPrevious = Yii::app()->db->createCommand("SELECT SUM(amount) as total_amount FROM {$tblpx}dailyexpense WHERE date BETWEEN '" .  $previousMonthStart . "' AND '" . $previousMonthEnd . "' AND expense_type IN(88,89) AND (" . $newQuery . ")")->queryRow();

        $totalDailyExpenseCurrentMonth=0;
        $totalDailyExpensePreviousMonth=0;
        $totalDailyExpenseCurrentMonth = $dailyExpenses['total_amount'];
        $totalDailyExpensePreviousMonth = $dailyExpensesPrevious['total_amount'];
        // Define date ranges for current and previous months
    
        $grossProfit = $this->getGrossprofit($currentMonthStart,$currentMonthEnd,$previousMonthStart,$previousMonthEnd,$companyId);
        $currentGrossProfit = $grossProfit[0]['current'];
        $previousGrossProfit = $grossProfit[0]['previous'];

        $currentNetProfit = $currentGrossProfit - $totalDailyExpenseCurrentMonth;
        $previousNetProfit = $previousGrossProfit - $totalDailyExpensePreviousMonth;
    
        // Prepare data values
        $dataValues = [
            [
                'name' => 'Net Profit',
                'current' => $currentNetProfit,
                'previous' => $previousNetProfit
            ]
        ];
        return $dataValues;


        
    }
    public function getGrossprofit($currentMonthStart,$currentMonthEnd,$previousMonthStart,$previousMonthEnd,$companyId)
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $criteria = new CDbCriteria();
        if (!empty($companyId)) {
            $criteria->addCondition("company_id = :companyId");
            $criteria->params[':companyId'] = $companyId;
        }
        // Retrieve all records
        $salesCurrent = $this->getSalesInvoice($currentMonthStart,$currentMonthEnd,$companyId);
        $salesPrevious = $this->getSalesInvoice($previousMonthStart,$previousMonthEnd,$companyId);
        $billsCurrent = $this->getSalesBill($currentMonthStart,$currentMonthEnd,$companyId);
        $billsPrevious = $this->getSalesBill($previousMonthStart,$previousMonthEnd,$companyId);
        
        $quotationCurrent = $this->getScquotation($currentMonthStart,$currentMonthEnd,$companyId);
        $quotationPrevious = $this->getScquotation($previousMonthStart,$previousMonthEnd,$companyId);
        

        // Initialize totals
        $currentMonthTotalSales = 0;
        $previousMonthTotalSales = 0;
        $currentMonthTotalBills = 0;
        $previousMonthTotalBills = 0;
        $currentMonthTotalSub = 0;
        $previousMonthTotalSub = 0;
        
        // Calculate gross profit
        $currentMonthGrossProfit = $salesCurrent - ($billsCurrent + $quotationCurrent);
        $previousMonthGrossProfit = $salesPrevious - ($billsPrevious + $quotationPrevious);

        // Format the result
        $dataValues = [
            [
                'name' => 'Gross Profit',
                'current' => $currentMonthGrossProfit,
                'previous' => $previousMonthGrossProfit
            ]
        ];

        return $dataValues;
    }

    public function getSalesInvoice($date_from,$date_to,$companyId){
        $tblpx = Yii::app()->db->tablePrefix;
        $newQuery1='';
        $newQuery2='';
        $newQuery3='';
        $where2='';
        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        if (!empty($companyId)) {
            $newQuery1 .= " FIND_IN_SET('" . $companyId . "', {$tblpx}expenses.company_id)";
            $newQuery2 .= " FIND_IN_SET('" . $companyId . "', company_id)";
            $newQuery3 .= " FIND_IN_SET('" . $companyId . "', {$tblpx}bills.company_id)";
        } else {
            $companyId = "";
            foreach ($arrVal as $arr) {
                if ($newQuery1) $newQuery1 .= ' OR';
                if ($newQuery2) $newQuery2 .= ' OR';
                if ($newQuery3) $newQuery3 .= ' OR';
                $newQuery1 .= " FIND_IN_SET('" . $arr . "', {$tblpx}expenses.company_id)";
                $newQuery2 .= " FIND_IN_SET('" . $arr . "', company_id)";
                $newQuery3 .= " FIND_IN_SET('" . $arr . "', {$tblpx}bills.company_id)";
            }
        }
        $where2 .= " AND {$tblpx}invoice.date between '" . $date_from . "' and '" . $date_to . "'";

        $invoicesql = "SELECT SUM(amount) AS amount, SUM(tax_amount) AS tax_amount FROM {$tblpx}invoice "
        . " WHERE "
        . "invoice_status ='saved' AND (" . $newQuery2 . ") " . $where2 . "";
        // die($invoicesql);
        $sales = Yii::app()->db->createCommand($invoicesql)->queryRow();
        $total_amount=$sales['tax_amount']+$sales['amount'];
        // echo '<pre>';print_r($total_amount);exit;
        
        return $total_amount;
    }

    public function getSalesBill($date_from,$date_to,$companyId){
        $bill_amount=0;
        $newQuery = "";
        $newQuery1 = "";
        $newQuery3="";
        $newQuery4="";
        $tblpx = Yii::app()->db->tablePrefix;
            if (empty($companyId)) {
                $user = Users::model()->findByPk(Yii::app()->user->id);
                $arrVal = explode(',', $user->company_id);
                
                foreach ($arrVal as $arr) {
                    if ($newQuery) $newQuery .= ' OR';
                    if ($newQuery1) $newQuery1 .= ' OR';
                     if ($newQuery3) $newQuery3 .= ' OR';
                    if ($newQuery4) $newQuery4 .= ' OR';
                    $newQuery .= " FIND_IN_SET('" . $arr . "', company_id)";
                    $newQuery1 .= " FIND_IN_SET('" . $arr . "', {$tblpx}invoice.company_id)";
                    $newQuery3 .= " FIND_IN_SET('" . $arr . "', E.company_id)";
                    $newQuery4 .= " FIND_IN_SET('" . $arr . "', jp_purchase.company_id)";
                }
            } else {
                $newQuery = " FIND_IN_SET('" . $companyId . "', company_id)";
                $newQuery1 = " FIND_IN_SET('" . $companyId . "', {$tblpx}invoice.company_id)";
            $newQuery3 .= " FIND_IN_SET('" . $companyId . "', E.company_id)";
                $newQuery4 .= " FIND_IN_SET('" . $companyId . "', jp_purchase.company_id)";
            }
       /* Purchase Bill Starts*/
       $unreconciled_datasql = "SELECT O.*, E.*  FROM jp_reconciliation O "
            . " JOIN jp_expenses E ON O.reconciliation_parentid = E.exp_id  "
            . " WHERE O.reconciliation_status=0 and (" . $newQuery3 . ") ";
        $unreconciled_data = Yii::app()->db->createCommand($unreconciled_datasql)->queryAll();

        $bill_array = array();

        foreach ($unreconciled_data as  $data) {                
            $recon_bill = "'{$data["bill_id"]}'";                
            array_push($bill_array, $recon_bill);
        }
       
        $bill_array1 = implode(',', $bill_array);

        $pendingsql = 'SELECT bill_id FROM jp_expenses '
        . ' WHERE  '
        . ' reconciliation_status =1 OR  reconciliation_status IS NULL';
        $pending_bills_array = Yii::app()->db->createCommand($pendingsql)->queryAll();

        $pending_bills =array();
        foreach ($pending_bills_array as  $data) {                
            $billid = "'{$data["bill_id"]}'";                
            array_push($pending_bills, $billid);
        }
        $pending_bills1 = implode(',', $pending_bills);
        if ($bill_array1) {
            $bill_condition = " 1=1";
            $bill_condition1 = " 1=1";
            if(!empty($pending_bills1)){
                $bill_condition = " bill_id IN($pending_bills1)";
                $bill_condition1 = "bill_id NOT IN($pending_bills1)";
            }

            $purchasesql = "SELECT * FROM " . $tblpx . "bills "
            . " LEFT JOIN " . $tblpx . "purchase "
            . " ON " . $tblpx . "bills.purchase_id = " . $tblpx . "purchase.p_id "
            . " WHERE " . $tblpx . "bills.purchase_id IS NOT NULL "
            . " AND (" . $newQuery4 . ") "
            . " AND bill_id NOT IN ($bill_array1) "
            . " AND " . $bill_condition
            . " AND " . $tblpx . "bills.bill_date BETWEEN '" . $date_from . "' AND '" . $date_to . "' ";
                  
            $purchase = Yii::app()->db->createCommand($purchasesql)->queryAll(); //reconcil

            $purchase1sql = "SELECT * FROM " . $tblpx . "bills "
              . " LEFT JOIN " . $tblpx . "purchase "
              . " ON " . $tblpx . "bills.purchase_id = " . $tblpx . "purchase.p_id "
              . " WHERE " . $tblpx . "bills.purchase_id IS NOT NULL "
              . " AND (" . $newQuery4 . ") "
              . " AND " . $bill_condition1
              . " AND " . $tblpx . "bills.bill_date BETWEEN '" . $date_from . "' AND '" . $date_to . "' ";
                    
            $purchase1 = Yii::app()->db->createCommand($purchase1sql)->queryAll();
        }
        else {

            $bill_condition = " 1=1";
            $bill_condition1 = " 1=1";
            if(!empty($pending_bills1)){
                $bill_condition = " bill_id IN($pending_bills1)";
                $bill_condition1 = "bill_id NOT IN($pending_bills1)";
            }

            $purchasesql = "SELECT * FROM " . $tblpx . "bills "
             . " LEFT JOIN " . $tblpx . "purchase "
             . " ON " . $tblpx . "bills.purchase_id = " . $tblpx . "purchase.p_id "
             . " WHERE " . $tblpx . "bills.purchase_id IS NOT NULL "
             . " AND (" . $newQuery4 . ") "
             . " AND " . $bill_condition
             . " AND " . $tblpx . "bills.bill_date BETWEEN '" . $date_from . "' AND '" . $date_to . "' ";

            $purchase = Yii::app()->db->createCommand($purchasesql)->queryAll();

            $purchase1sql = "SELECT * FROM " . $tblpx . "bills "
              . " LEFT JOIN " . $tblpx . "purchase "
              . " ON " . $tblpx . "bills.purchase_id = " . $tblpx . "purchase.p_id "
              . " WHERE " . $tblpx . "bills.purchase_id IS NOT NULL "
              . " AND (" . $newQuery4 . ") "
              . " AND " . $bill_condition1
              . " AND " . $tblpx . "bills.bill_date BETWEEN '" . $date_from . "' AND '" . $date_to . "' ";

            $purchase1 = Yii::app()->db->createCommand($purchase1sql)->queryAll(); //un reconcil
        }

        $bill_amount = 0;
        $purchase_list = Projects::model()->groupPurchaseBillItems($purchase);
        //$expense_types_listing = Projects::model()->setExpenseTypesListingofPurchaseBill($purchase_list);
        // echo '<pre>';print_r($purchase_list);exit;
            foreach ($purchase_list as $purchase) {
                $count = count($purchase);
                $count = ($count - 2);
                $index = 1;
                foreach ($purchase as $key => $value) {
                     if (is_array($value)) {
                        $amount =  $value['bill_totalamount'] + $value['bill_additionalcharge'];
                        $vendor = Vendors::model()->findByPk($value['vendor_id']);
                        $bill_amount += $amount; 
                    }
                 $index++;
                }
            }
        // echo ($bill_amount);exit;
        return $bill_amount;
    }
    public function getScquotation($date_from,$date_to,$companyId){
        /** Subcontractor Payment starts */
        $newQuery = "";
        $newQuery1 = "";
        $newQuery3="";
        $newQuery4="";
        $tblpx = Yii::app()->db->tablePrefix;
        if (empty($companyId)) {
            $user = Users::model()->findByPk(Yii::app()->user->id);
            $arrVal = explode(',', $user->company_id);
            
            foreach ($arrVal as $arr) {
                if ($newQuery) $newQuery .= ' OR';
                if ($newQuery1) $newQuery1 .= ' OR';
                 if ($newQuery3) $newQuery3 .= ' OR';
                if ($newQuery4) $newQuery4 .= ' OR';
                $newQuery .= " FIND_IN_SET('" . $arr . "', company_id)";
                $newQuery1 .= " FIND_IN_SET('" . $arr . "', {$tblpx}invoice.company_id)";
                $newQuery3 .= " FIND_IN_SET('" . $arr . "', E.company_id)";
                $newQuery4 .= " FIND_IN_SET('" . $arr . "', jp_purchase.company_id)";
            }
        } else {
            $newQuery = " FIND_IN_SET('" . $companyId . "', company_id)";
            $newQuery1 = " FIND_IN_SET('" . $companyId . "', {$tblpx}invoice.company_id)";
        $newQuery3 .= " FIND_IN_SET('" . $companyId . "', E.company_id)";
            $newQuery4 .= " FIND_IN_SET('" . $companyId . "', jp_purchase.company_id)";
        }
        $tblpx = Yii::app()->db->tablePrefix;
        $check_array = array();
        $daybook_subcontractor_expenseamount = 0;
        
        $daybook_sub_expensesql = "SELECT * FROM " . $tblpx . "expenses "
                        . " WHERE amount != 0 "
                        . " AND type = '73' AND exptype IS NULL "
                        . " AND vendor_id IS NULL AND bill_id IS NULL "
                        . " AND (" . $newQuery . ") "
                        . " AND (reconciliation_status IS NULL OR reconciliation_status = 1) "
                        . " AND date BETWEEN '" . $date_from . "' AND '" . $date_to . "' "
                        . " ORDER BY date";
        $daybook_expense = Yii::app()->db->createCommand( $daybook_sub_expensesql)->queryAll();
        // $subcontractor_payment_lists = Projects::model()->setSubcontractorPayment($daybook_expense);
        // echo "<pre>";print_r( $subcontractor_payment_lists);exit;
        foreach ($daybook_expense as $data) {
            $payment = SubcontractorPayment::model()->findByPk($data['subcontractor_id']);
            // echo "<pre>";print_r( $payment);exit;
            if ($payment['payment_quotation_status'] == 1 && $payment['sc_bill_id'] !='' ) {
                $bills_det= Subcontractorbill::Model()->findByPk( $payment['sc_bill_id']);
                $daybook_subcontractor_expenseamount+= $bills_det['total_amount'];
            }
        }
            // foreach ($subcontractor_payment_lists as $daybook_expense) {
            // $scpayment_group_count = count($daybook_expense) - 1;
            // $sc_index = 1;
            // foreach ($daybook_expense as $newdata) {
            //     if (is_array($newdata)) {
                    
            //         $daybook_subcontractor_expenseamount+=$newdata['paid'];
            //         $tblpx = Yii::app()->db->tablePrefix;
            //         $payment = SubcontractorPayment::model()->findByPk($newdata['subcontractor_id']);
            //         $query = "";
            //         $subcontractor_id = '';
            //         if (isset($payment->subcontractor_id)) {
            //             $query = ' AND b.subcontractor_id=' . $payment->subcontractor_id . '';
            //             $subcontractor_id = $payment->subcontractor_id;
            //         }
            //         $sub_total1 = Yii::app()->db->createCommand("select SUM(a.paidamount) as paid_amount,count(a.exp_id) as total_count,b.* from jp_expenses a LEFT JOIN jp_subcontractor_payment b ON a.subcontractor_id=b.payment_id WHERE a.amount !=0 AND a.projectid=" . $newdata['projectid'] . " AND a.type = '73' AND a.exptype IS NULL AND a.vendor_id IS NULL AND a.bill_id IS NULL AND a.date ='" . $newdata['date'] . "' " . $query . " GROUP BY a.date,b.subcontractor_id")->queryRow();
            //         $check_data = $newdata['date'] . '-' . $subcontractor_id;
            //         if (in_array($check_data, $check_array)) {
            //             $flag = 0;
            //             $row_count = "";
            //         } else {
            //             $flag = 1;
            //             $row_count = $sub_total1['total_count'];
            //         }
            //         array_push($check_array, $check_data);
                   
            //                 $payment = SubcontractorPayment::model()->findByPk($newdata['subcontractor_id']);
            //                 if (isset($payment->subcontractor_id)) {
            //                     $subcontractor_data = Subcontractor::model()->findByPk($payment->subcontractor_id);
            //                 }
                           
            //         $sc_index++;
            //     }
            // }
        // }
        return $daybook_subcontractor_expenseamount;
    }

    public function getAccountBalanceSummary($currentMonthStart,$currentMonthEnd,$currentMonthName,$previousMonthStart, $previousMonthEnd,$previousMonthName,$twoMonthsAgoStart,$twoMonthsAgoEnd,$twoMonthsAgoName,$companyId){
        $currentYear = date('Y');
        $currentMonth = date('m');
        
        $result= array();
        $current_account_balance = Controller::getAccountBalance($currentMonthStart,$currentMonthEnd,$currentMonthName,$companyId);
        $previous_account_balance = Controller::getAccountBalance($previousMonthStart,$previousMonthEnd,$previousMonthName,$companyId);
        $twomonths_ago_account_balance = Controller::getAccountBalance($twoMonthsAgoStart,$twoMonthsAgoEnd,$twoMonthsAgoName,$companyId);
        $result = array_merge(
            $current_account_balance, 
            $previous_account_balance, 
            $twomonths_ago_account_balance
        );
        //  echo "<pre>";print_r($previous_account_balance);exit;
       return $result;

    }

    public  static function getAccountBalance($date_from,$date_to,$month,$companyId){

        $tblpx  = Yii::app()->db->tablePrefix;
        // $month = date("M", strtotime($date_from));
       
        $cash_inhand_cashbalance=0;
        $bank_closing_balance=0;
        $result=array();
        if ($date_from != '' || $date_to != '') {
            $newQuery = "";
            $newQuery2 = "";
            $user = Users::model()->findByPk(Yii::app()->user->id);
            $arrVal = explode(',', $user->company_id);
            $companyCond='';
            if(!empty($companyId)){
                // $newQuery .= ' AND ';
                $newQuery .= " FIND_IN_SET('" . $companyId . "', {$tblpx}cashbalance.company_id)";
                $companyCond=" AND DATE_FORMAT({$tblpx}company.created_date, '%Y-%m') <= DATE_FORMAT('".$date_from."', '%Y-%m')";
            }
            else{
                foreach ($arrVal as $arr) {
                    if ($newQuery)
                        $newQuery .= ' OR';
                    $newQuery .= " FIND_IN_SET('" . $arr . "', {$tblpx}cashbalance.company_id)";
                }
            }
            
            $sql = "SELECT {$tblpx}status.caption,{$tblpx}cashbalance.cashbalance_type,{$tblpx}cashbalance.bank_id,{$tblpx}bank.bank_name,{$tblpx}cashbalance.cashbalance_opening_balance,{$tblpx}cashbalance.cashbalance_deposit,{$tblpx}cashbalance.cashbalance_withdrawal,{$tblpx}cashbalance.cashbalance_closing_balance,{$tblpx}cashbalance.cashbalance_date,{$tblpx}cashbalance.company_id FROM {$tblpx}cashbalance LEFT JOIN {$tblpx}company on {$tblpx}company.id={$tblpx}cashbalance.company_id LEFT JOIN {$tblpx}bank ON {$tblpx}cashbalance.bank_id={$tblpx}bank.bank_id LEFT JOIN {$tblpx}status ON {$tblpx}cashbalance.cashbalance_type={$tblpx}status.sid WHERE (" . $newQuery . ")" .
            $companyCond. "ORDER BY bank_id ASC";
            
            $cash_blance = Yii::app()->db->createCommand($sql)->queryAll();
                foreach ($cash_blance as $key => $value) {

                    $tblpx = Yii::app()->db->tablePrefix;
                    $where_bank = '';
                    $where_hand = '';
                                
                    $opening_hand = '';
                    $opening_bank = '';
                    if (!empty($date_from) && !empty($date_to)) {
                        $opening_date = date('Y-m-d', strtotime('-1 day', strtotime($date_from)));
                                
                        $where_bank .= " AND reconciliation_date between '" . $date_from . "' and '" . $date_to . "'";
                        $where_hand .= " `date` between '" . $date_from . "' and '" . $date_to . "' AND ";
                        $opening_hand .= " AND date <= '" . $opening_date . "'";
                        $opening_bank .= " AND reconciliation_date <= '" . $opening_date . "'";
                    } else {
                        if (!empty($date_from)) {
                            $opening_date = date('Y-m-d', strtotime('-1 day', strtotime($_REQUEST['date_from'])));
                            $date_from = date('Y-m-d', strtotime($date_from));
                                        
                            $where_bank .= " AND reconciliation_date between '" . $date_from . "' and '" . $current_date . "'";
                            $where_hand .= " `date` between '" . $date_from . "' and '" . $current_date . "' AND ";
                            $opening_hand .= " AND date <= '" . $opening_date . "'";
                            $opening_bank .= " AND reconciliation_date <= '" . $opening_date . "'";
                        }
                        if (!empty($date_to)) {
                            $opening_date = $value['cashbalance_date'];                                        
                            $date_to = date('Y-m-d', strtotime($date_to));
                                        
                            $where_bank .= " AND reconciliation_date between '" . $value['cashbalance_date'] . "' and '" . $date_to . "'";
                            $where_hand .= " `date` between '" . $value['cashbalance_date'] . "' and '" . $date_to . "' AND ";
                            $opening_hand .= " AND date <= '" . $opening_date . "'";
                            $opening_bank .= " AND reconciliation_date <= '" . $opening_date . "'";
                        }
                    }

                               

                    if ($value['bank_id'] != '') {
                                    // deposit
                        $reconcil = "reconciliation_status = 1 ".$where_bank;
                        $unreconcil = "reconciliation_status = 0  "
                                        . " AND reconciliation_date IS NULL";

                        $sql = "SELECT SUM(receipt) as total_amount "
                                        . " FROM {$tblpx}expenses "
                                        . " WHERE " . $reconcil . " AND payment_type=88 "
                                        . " AND bank_id=" . $value['bank_id'] . " AND type=72 "
                                        . " AND company_id=" . $value['company_id'];
                                        

                        $daybook = Yii::app()->db->createCommand($sql)->queryRow();                                    
                                    
                        $sql = "SELECT SUM(dailyexpense_receipt) as total_amount "
                                        . " FROM {$tblpx}dailyexpense "
                                        . " WHERE (" . $reconcil 
                                        . " AND dailyexpense_receipt_type=88 "
                                        . " AND bank_id=" . $value['bank_id'].")"
                                        . " OR (" . $unreconcil 
                                        . "  AND dailyexpense_receipt_type=89 "
                                        . " AND bank_id=" . $value['bank_id'].""
                                        . " AND expensehead_type=5) AND  parent_status='1' "                
                                        . " AND exp_type=72"
                                        . " AND company_id=" .  $value['company_id'];  

                                                                        
                        $dailyexpense = Yii::app()->db->createCommand($sql)->queryRow();

                        $daybook_check = isset($daybook['total_amount'])?$daybook['total_amount']:0;
                          
                        $dailyexpense_check = isset($dailyexpense['total_amount'])?$dailyexpense['total_amount']:0;                                      
                        $deposit = $daybook_check + $dailyexpense_check;


                                    //withdrawal

                        $sql = "SELECT SUM((IFNULL(paid, 0))  - IFNULL(expense_tds, 0)) as total_amount "
                                        . " FROM {$tblpx}expenses "
                                        . " WHERE " . $reconcil . " AND expense_type=88 "
                                        . " AND bank_id=" . $value['bank_id']. " AND type=73  "
                                        . " AND company_id=" . $value['company_id'];
                                        
                        $daybook_with = Yii::app()->db->createCommand($sql)->queryRow();

                        $sql = "SELECT SUM(dailyexpense_paidamount) as total_amount "
                          . " FROM {$tblpx}dailyexpense "
                                        . " WHERE company_id=" . $value['company_id'] 
                                        . " AND parent_status='1' AND exp_type=73 AND "
                                        . " (" . $reconcil 
                                        . " AND (expense_type=88 ) AND bank_id=" . $value['bank_id'].") "
                                        . " AND dailyexpense_chequeno IS NOT NULL "
                                        ; 

                                       
                        $dailyexpense_with = Yii::app()->db->createCommand($sql)->queryRow();

                        
                        $daybook_with_amount = isset( $daybook_with['total_amount'])? $daybook_with['total_amount']:0;
                          
                        $dailyexpense_with_amount = isset($dailyexpense_with['total_amount'])?$dailyexpense_with['total_amount']:0;  
                       // echo "<pre>" ;print_r($dailyexpense_with_amount);exit;

                        $sql = "SELECT SUM( (IFNULL(amount, 0) + IFNULL(tax_amount, 0))- IFNULL(tds_amount,0) ) "
                                        . " as total_amount FROM {$tblpx}dailyvendors "
                                        . " WHERE " . $reconcil . " AND payment_type=88  AND "
                                        . " bank=" . $value['bank_id'] 
                                        . " AND company_id=" . $value['company_id'];                                    
                        $dailyvendors = Yii::app()->db->createCommand($sql)->queryRow();

                                    $sql = "SELECT SUM( (IFNULL(amount, 0) + IFNULL(tax_amount, 0))- IFNULL(tds_amount,0)) "
                                        . " as total_amount FROM {$tblpx}subcontractor_payment "
                                        . " WHERE " . $reconcil . " AND payment_type=88  "
                                        . " AND bank=" . $value['bank_id']
                                        . " AND company_id=" . $value['company_id'];                                    
                        $subcontractor = Yii::app()->db->createCommand($sql)->queryRow();

                        $dailyvendors_check = isset($dailyvendors['total_amount'])?$dailyvendors['total_amount']:0;
                          
                        $subcontractor_check = isset($subcontractor['total_amount'])?$subcontractor['total_amount']:0;   
                         
                        $withdrawal = $dailyvendors_check + $daybook_with_amount + $dailyexpense_with_amount;

                                    // opening balance calculation
                                    // deposit
                                    $sql = "SELECT SUM(receipt) as total_amount "
                                            . " FROM {$tblpx}expenses "
                                            . " WHERE reconciliation_status = 1 " . $opening_bank 
                                            . " AND payment_type=88 AND bank_id=" . $value['bank_id'] 
                                            . " AND type=72 AND company_id=" . $value['company_id'];
                                    $daybook_opening = Yii::app()->db->createCommand($sql)->queryRow();
                                                    


                                    $sql = "SELECT SUM(dailyexpense_receipt) as total_amount "
                                            . " FROM {$tblpx}dailyexpense "
                                            . " WHERE reconciliation_status = 1 " . $opening_bank 
                                            . " AND dailyexpense_receipt_type=88 "
                                            . " AND bank_id=" . $value['bank_id'] 
                                            . " AND exp_type=72 AND company_id=" . $value['company_id'];
                        $dailyexpense_opening = Yii::app()->db->createCommand($sql)->queryRow();
                        
                        $daybook_check_opening = isset($daybook_opening['total_amount'])?$daybook_opening['total_amount']:0;
                          
                        $dailyexpense_check_opening = isset($dailyexpense_opening['total_amount'])?$dailyexpense_opening['total_amount']:0;   
                        $deposit_opening = $daybook_check_opening + $dailyexpense_check_opening;

                                    
                                    //withdrawal
                                    $sql = "SELECT SUM((IFNULL(paid, 0))  - IFNULL(expense_tds, 0)) as total_amount "
                                            . " FROM {$tblpx}expenses "
                                            . " WHERE reconciliation_status = 1 " . $opening_bank 
                                            . " AND expense_type=88 AND bank_id=" . $value['bank_id'] 
                                            . " AND type=73 AND company_id=" . $value['company_id'];
                         $daybook_with_opening = Yii::app()->db->createCommand($sql)->queryRow();
                        
                                    $sql = "SELECT SUM(dailyexpense_paidamount) as total_amount "
                                            . " FROM {$tblpx}dailyexpense "
                                            . " WHERE reconciliation_status = 1 " . $opening_bank 
                                            . " AND expense_type=88 AND bank_id=" . $value['bank_id'] 
                                            . " AND exp_type=73 AND company_id=" . $value['company_id'];
                                    $dailyexpense_with_opening = Yii::app()->db->createCommand($sql)->queryRow();

                                    $daybook_with_amount_opening = isset($daybook_with_opening['total_amount'])?$daybook_with_opening['total_amount']:0;
                          
                                    $dailyexpense_with_amount_opening = isset($dailyexpense_with_opening['total_amount'])?$dailyexpense_with_opening['total_amount']:0;   

                                    $sql = "SELECT SUM( (IFNULL(amount, 0) + IFNULL(tax_amount, 0))- IFNULL(tds_amount,0) ) "
                                            . " as total_amount FROM {$tblpx}dailyvendors "
                                            . " WHERE reconciliation_status = 1 " . $opening_bank 
                                            . " AND payment_type=88  AND bank=" . $value['bank_id'] 
                                            . " AND company_id=" . $value['company_id'];
                                    $dailyvendors_opening = Yii::app()->db->createCommand($sql)->queryRow();

                                   
                                    $dailyvendors_check_opening = isset($dailyvendors_opening['total_amount'])?$dailyvendors_opening['total_amount']:0; 

                                    $withdrawal_opening = $dailyvendors_check_opening + $daybook_with_amount_opening + $dailyexpense_with_amount_opening;

                                    // $opening_balance = ($deposit_opening + $value['cashbalance_opening_balance']) - $withdrawal_opening;
                                    //new changes
                                    $opening_date = date('Y-m-d', strtotime('-1 day', strtotime($date_from)));
                                    $opening_bank = " AND reconciliation_date <= '" . $opening_date . "'";
                                    //deposit opening               
                                    $bopendeposit = CashBalanceHelper::depositOpening($opening_bank, $value['bank_id'], $value['company_id']);
                                    $deposit_opening = $bopendeposit['daybook_check_opening'] + $bopendeposit['dailyexpense_check_opening'];

                                    //withdraw opening
                                    $bopenwithdraw = CashBalanceHelper::withdrawOpening($opening_bank, $value['bank_id'], $value['company_id']);
                                    $withdrawal_opening = $bopenwithdraw['dailyvendors_check_opening'] + $bopenwithdraw['daybook_with_amount_opening'] +
                                            $bopenwithdraw['dailyexpense_with_amount_opening'];

                                    // $opening_balance = ($deposit_opening + floatval($value['cashbalance_opening_balance'])) - $withdrawal_opening;
                                    $opening_balance =  floatval($value['cashbalance_opening_balance']);
                                    if($value["cashbalance_type"]=="99"){
                                       
                                         $bank_closing_balance +=floatval(($opening_balance + $deposit) - $withdrawal);
                                    }
                    } else {

                                    //deposit
                                    $sql = "SELECT SUM(receipt) as total_amount "
                                            . " FROM {$tblpx}expenses WHERE $where_hand payment_type=89 AND "
                                            . " type=72 AND company_id=" . $value['company_id'];                                    
                                    $daybook_cah = Yii::app()->db->createCommand($sql)->queryRow();
                                    $daybook_cash = isset($daybook_cah['total_amount'])?$daybook_cah['total_amount']:0;
                                   
                                    $sql = "SELECT SUM(dailyexpense_receipt) as total_amount "
                                        . " FROM {$tblpx}dailyexpense "
                                        . " WHERE  $where_hand (dailyexpense_receipt_type=89 AND "
                                        . " dailyexpense_chequeno IS NULL "
                                        . " AND reconciliation_status IS NULL "
                                        . " AND company_id=" . $value['company_id'].")  "
                                        . " OR (dailyexpense_receipt_type=88 "
                                        . " AND expensehead_type=4  AND "
                                        . " company_id=" . $value['company_id'].") AND "
                                        . " parent_status='1' AND "
                                        . " exp_type=72 ";                                    
                                    $dailyexpense_cah = Yii::app()->db->createCommand($sql)->queryRow();
                                    $dailyexpense_cash = isset($dailyexpense_cah['total_amount'])?$dailyexpense_cah['total_amount']:0;
                                    
                                    $deposit = $daybook_cash + $dailyexpense_cash;
                                    // withdrawal

                                    $sql = "SELECT SUM( (IFNULL(paid, 0))  - IFNULL(expense_tds, 0)) as total_amount "
                                            . " FROM {$tblpx}expenses "
                                            . " WHERE $where_hand (expense_type=89) "
                                            . " AND type=73 "   
                                            . " AND company_id=" . $value['company_id'];                                            
                                    $daybook_with = Yii::app()->db->createCommand($sql)->queryRow();
                                    $daybook_with_amount = isset($daybook_with['total_amount'])?$daybook_with['total_amount']:0;
                                    
                                    $sql = "SELECT SUM(dailyexpense_paidamount) "
                                        . " as total_amount "
                                        . " FROM {$tblpx}dailyexpense "
                                        . " WHERE $where_hand (expense_type=89) AND exp_type=73 "
                                        . " AND parent_status='1' " 
                                        . " AND dailyexpense_chequeno IS NULL "
                                        . " AND reconciliation_status IS NULL "
                                        . " AND company_id=" . $value['company_id'];                                                                        
                                    $dailyexpense_with = Yii::app()->db->createCommand($sql)->queryRow();
                                    $dailyexpense_with_amount = isset($dailyexpense_with['total_amount'])?$dailyexpense_with['total_amount']:0;

                                    $sql = "SELECT SUM( (IFNULL(amount, 0) + IFNULL(tax_amount, 0))-IFNULL(tds_amount, 0) ) "
                                        . " as total_amount FROM {$tblpx}dailyvendors"
                                        . " WHERE  $where_hand payment_type=89 " 
                                        . " AND company_id=" . $value['company_id'];
                                                                       
                                    $dailyvendors_cah = Yii::app()->db->createCommand($sql)->queryRow();

                                    $dailyvendors_cash = isset($dailyvendors_cah['total_amount'])?$dailyvendors_cah['total_amount']:0;

                                    $sql = "SELECT SUM( IFNULL(paidamount, 0) - IFNULL(tds_amount, 0) ) "
                                            . " as total_amount FROM {$tblpx}subcontractor_payment "
                                            . " WHERE $where_hand payment_type=89 " 
                                            . " AND company_id=" . $value['company_id'] ;
                                    
                                    $subcontractor_cah = Yii::app()->db->createCommand($sql)->queryRow();
                                    $subcontractor_cash = isset($subcontractor_cah['total_amount'])?$subcontractor_cah['total_amount']:0;                                    

                                    $withdrawal = $dailyvendors_cash + $daybook_with_amount + $dailyexpense_with_amount;
                                    $buyer_module = GeneralSettings::model()->checkBuyerModule();
                                    if ($buyer_module) {
                                        foreach ($buyer_transactions_cash as $buyer_transaction) {
                                            if ($buyer_transaction['transaction_for'] == 3) {
                                                $withdrawal += $buyer_transaction['total_amount'];
                                            } elseif ($buyer_transaction['transaction_for'] == 1) {
                                                $deposit += $buyer_transaction['total_amount'];
                                            }
                                        }
                                        foreach ($buyer_transactions_bank as $buyer_transaction) {
                                            if ($buyer_transaction['transaction_for'] == 3) {
                                                $withdrawal += $buyer_transaction['total_amount'];
                                            } elseif ($buyer_transaction['transaction_for'] == 1) {
                                                $deposit += $buyer_transaction['total_amount'];
                                            }
                                        }
                                    }




                                    // opening balance calculation
                                    // deposit
                                    $sql = "SELECT SUM(receipt) as total_amount "
                                            . " FROM {$tblpx}expenses "
                                            . " WHERE payment_type=89 AND type=72 " . $opening_hand 
                                            . " AND company_id=" . $value['company_id'];
                                    $daybook_cah_opening = Yii::app()->db->createCommand($sql)->queryRow();
                                    $daybook_cash_opening = isset($daybook_cah_opening['total_amount'])?$daybook_cah_opening['total_amount']:0;
                                    
                                    $sql ="SELECT SUM(dailyexpense_receipt) as total_amount "
                                            . " FROM {$tblpx}dailyexpense "
                                            . " WHERE (dailyexpense_receipt_type=89) "
                                            . " AND exp_type=72 " . $opening_hand 
                                            . " AND company_id=" . $value['company_id'];
                                    $dailyexpense_cah_opening = Yii::app()->db->createCommand($sql)->queryRow();
                                    $dailyexpense_cash_opening = isset($dailyexpense_cah_opening['total_amount'])?$dailyexpense_cah_opening['total_amount']:0;
                                    $deposit_opening = $daybook_cash_opening + $dailyexpense_cash_opening;

                                    $sql = "SELECT SUM((IFNULL(paid, 0))  - IFNULL(expense_tds, 0)) as total_amount FROM {$tblpx}expenses "
                                            . " WHERE expense_type=89 " // OR expense_type=103)
                                            . " AND type=73 " . $opening_hand 
                                            . " AND company_id=" . $value['company_id'];
                                    $daybook_with_opening = Yii::app()->db->createCommand($sql)->queryRow();
                                    $daybook_with_amount_opening = isset($daybook_with_opening['total_amount'])?$daybook_with_opening['total_amount']:0;
                                    
                                    $sql = "SELECT SUM(dailyexpense_paidamount) as total_amount "
                                            . " FROM {$tblpx}dailyexpense "
                                            . " WHERE (expense_type=89) AND exp_type=73 " . $opening_hand 
                                            . " AND company_id=" . $value['company_id'];
                                    $dailyexpense_with_opening = Yii::app()->db->createCommand($sql)->queryRow();
                                    $dailyexpense_with_amount_opening = isset($dailyexpense_with_opening['total_amount'])?$dailyexpense_with_opening['total_amount']:0;

                                    $sql = "SELECT SUM( (IFNULL(amount, 0) + IFNULL(tax_amount, 0))- IFNULL(tds_amount,0) ) "
                                            . " as total_amount FROM {$tblpx}dailyvendors "
                                            . " WHERE payment_type=89 " . $opening_hand 
                                            . " AND company_id=" . $value['company_id'];
                                    $dailyvendors_cah_opening = Yii::app()->db->createCommand($sql)->queryRow();
                                    $dailyvendors_cash_opening = isset($dailyvendors_cah_opening['total_amount'])?$dailyvendors_cah_opening['total_amount']:0;
                                    $withdrawal_opening = $dailyvendors_cash_opening + $daybook_with_amount_opening + $dailyexpense_with_amount_opening;

                                    // $opening_balance = ($deposit_opening + $value['cashbalance_opening_balance']) - $withdrawal_opening;
                                    $opening_hand = " AND date <= '" . $opening_date . "'";
                                    //deposit opening               
                                    $copendeposit = CashBalanceHelper::cdepositOpening($opening_hand, $value['company_id']);
                                    $deposit_opening = $copendeposit['daybook_cash_opening'] + $copendeposit['dailyexpense_cash_opening'];

                                    //withdrwa opening               
                                    $copenwithdraw = CashBalanceHelper::cwithdrawOpening($opening_hand, $value['company_id']);
                                    $withdrawal_opening = floatval($copenwithdraw['dailyvendors_cash_opening']) + floatval($copenwithdraw['dailyexpense_with_amount_opening']) + floatval($copenwithdraw['daybook_with_amount_opening']);

                                    // $opening_balance = ($deposit_opening + floatval($value['cashbalance_opening_balance'])) - $withdrawal_opening;
                                    $opening_balance = floatval($value['cashbalance_opening_balance']);
                                    
                                    if($value["cashbalance_type"]=="100"){
                                        $cash_inhand_cashbalance +=floatval(($opening_balance + $deposit) - $withdrawal);

                                    }
                                   
                    }
             
                }
                $where1 = '';
                $where2='';
                if (!empty($companyId) ) {
                    $where1 .= " AND {$tblpx}expenses.company_id = " . $companyId . "";
                    $where2 .= " AND {$tblpx}subcontractor_payment.company_id = " . $companyId . "";
                }
                $daybook_amount=0;
                $daybook_sql = "SELECT * ," . $tblpx . "expenses.description as dbookdesc "
                . " FROM " . $tblpx . "expenses LEFT JOIN " . $tblpx . "users "
                . " ON " . $tblpx . "expenses.employee_id= " . $tblpx . "users.userid "
                . " LEFT JOIN " . $tblpx . "projects "
                . " ON " . $tblpx . "projects.pid = " . $tblpx . "expenses.projectid "
                . " WHERE " . $tblpx . "expenses.type = 73  "
                . " AND " . $tblpx . "expenses.expense_type=103 " . $where1 . " "
                . " GROUP BY " . $tblpx . "expenses.projectid";
            $daybook = Yii::app()->db->createCommand($daybook_sql)->queryAll();
            foreach ($daybook as $value) {
                // echo '<pre>';print_r($value);exit;
                if (!empty($value['pid'])) {
                    $daybook_details = Yii::app()->db->createCommand("
                        SELECT * 
                        FROM " . $tblpx . "expenses 
                        LEFT JOIN " . $tblpx . "users 
                        ON " . $tblpx . "expenses.employee_id = " . $tblpx . "users.userid 
                        WHERE " . $tblpx . "expenses.type = 73  
                        AND " . $tblpx . "expenses.expense_type = 103 
                        AND " . $tblpx . "expenses.projectid = :projectid 
                        ORDER BY " . $tblpx . "expenses.date ASC
                    ")->bindParam(':projectid', $value['pid'])->queryAll();
                }
				$i = 0;
				$item_amount = 0;
                if(!empty($daybook_details)){
                    foreach ($daybook_details as $newsql) {
                        $item_amount += $newsql['paid'];
                    }
                }
                
                $daybook_amount += $item_amount;
            }
                $pettycash = Controller::Pettycashdashboard($date_from,$date_to,$companyId);
                $dailyexpense_amount = 0;
                $dailyexpense_exp_amount=0;
                $dailyexpense_refunds=0;
                $daybook_amount = 0;
                if (isset($pettycash['dailyexpense']) && is_array($pettycash['dailyexpense'])) {
                    foreach ($pettycash['dailyexpense'] as $expense) {
                      //  $dailyexpense_amount += isset($expense['dailyexpense_amount']) ? $expense['dailyexpense_amount'] : 0;
                      $dailyexpense_amount += isset($expense['dailyexpense_paidamount']) ? $expense['dailyexpense_paidamount'] : 0;
                        // $dailyexpense_exp_amount += isset($expense['dailyexpense_paidamount']) ? $expense['dailyexpense_paidamount'] : 0;
                        $dailyexpense_refunds += isset($expense['dailyexpense_receipt']) ? $expense['dailyexpense_receipt'] : 0;
                    }
                 //  echo '<pre>';print_r($dailyexpense_refunds);exit;
                }
                if(!empty($pettycash)){
                    if(!empty($pettycash['daybook'])){
                        foreach ($pettycash['daybook'] as $expense) {
                            $tblpx = Yii::app()->db->tablePrefix;
                            
                            $daybook_details = Yii::app()->db->createCommand("SELECT * FROM " . $tblpx . "expenses LEFT JOIN " . $tblpx . "users ON " . $tblpx . "expenses.employee_id= " . $tblpx . "users.userid WHERE " . $tblpx . "expenses.type = 73  AND " . $tblpx . "expenses.expense_type=103 " . $where1 . " AND " . $tblpx . "expenses.projectid = " . $value['pid'] . " order by " . $tblpx . "expenses.date asc")->queryAll();
                            $i = 0;
                            $item_amount = 0;
                            foreach ($daybook_details as $newsql) {
                                $item_amount += $newsql['paid'];
                            }
                            $daybook_amount += $item_amount;
                        }

                    }
                    if(!empty($pettycash['dailyexpense_exp'])){
                        foreach ($pettycash['dailyexpense_exp'] as $value) {
                            $dailyexpense_exp_amount += $value['dailyexpense_paidamount'];
                        }
                    }
                   

                }
            //    echo "<pre>";print_r($pettycash);exit;
                
                $scp_sql = "SELECT * ," . $tblpx . "subcontractor_payment.description as description "
                . " FROM " . $tblpx . "subcontractor_payment LEFT JOIN " . $tblpx . "users "
                . " ON " . $tblpx . "subcontractor_payment.employee_id= " . $tblpx . "users.userid "
                . " LEFT JOIN " . $tblpx . "projects "
                . " ON " . $tblpx . "projects.pid = " . $tblpx . "subcontractor_payment.project_id   WHERE  " . $tblpx . "subcontractor_payment.payment_type ='103' ". $where2 . "
               
                 GROUP BY " . $tblpx . "subcontractor_payment.project_id";  
               //  die( $scp_sql)        ;      
               $scpayments = Yii::app()->db->createCommand($scp_sql)->queryAll();
               $scpayment_amount=0;
               foreach ($scpayments as $scpayment) {
                $scpayment_amount += $scpayment['paidamount'];
                }
                $pettyCash=$dailyexpense_amount - ($daybook_amount +$dailyexpense_exp_amount + $scpayment_amount + $dailyexpense_refunds);

            // echo '<pre>';print_r($pettyCash);exit;
                $result[]=[
                    "month"=>$month,
                    "Bank"=>$bank_closing_balance,
                    "Cash"=>$cash_inhand_cashbalance,
                    "Petty"=>$pettyCash,
                    
                ];
        
                
        }
        return $result;
    }

     public static function Pettycashdashboard($date_from,$date_to,$companyId)
    {
        // print_r($date_to);exit;
        $tblpx = Yii::app()->db->tablePrefix;
        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        $newQuery1 = "";
        $newQuery2 = "";
        $newQuery3 = "";
        foreach ($arrVal as $arr) {
            if ($newQuery1) $newQuery1 .= ' OR';
            if ($newQuery2) $newQuery2 .= ' OR';
            if ($newQuery3) $newQuery3 .= ' OR';
            $newQuery1 .= " FIND_IN_SET('" . $arr . "', " . $tblpx . "dailyexpense.company_id)";
            $newQuery2 .= " FIND_IN_SET('" . $arr . "', " . $tblpx . "expenses.company_id)";
            $newQuery3 .= " FIND_IN_SET('" . $arr . "', " . $tblpx . "subcontractor_payment.company_id)";
        }

        $dailyexpense = array();
        $employee_id = '';

        $company_id = '';
        $daybook = array();
        $dailyexpense_refund = array();
        $dailyexpense_exp = array();
        $scpayment = array();
        // if (isset($_REQUEST['employee_id']) && !empty($_REQUEST['employee_id'])) {
            $where = '';
            $where1 = '';
            $where2 = '';
            $employee_id = '';
            
            $company_id = '';
            $chquery1 = "";
            $chquery2 = "";
            $chcondition1 = "";
            $chcondition2 = "";
            
            if (isset($companyId) && !empty($companyId)) {
                $company_id = $companyId;
                $where .= " AND {$tblpx}dailyexpense.company_id = " . $companyId . "";
                $where1 .= " AND {$tblpx}expenses.company_id = " . $companyId . "";
                $where2 .= " AND {$tblpx}subcontractor_payment.company_id = " . $companyId . "";
            } else {
                $where .= " AND (" . $newQuery1 . ")";
                $where1 .= " AND (" . $newQuery2 . ")";
                $where2 .= " AND (" . $newQuery3 . ")";
            }
            if (!empty($date_from) && !empty($date_to)) {
                $date_from = date('Y-m-d', strtotime($date_from));
                $date_to = date('Y-m-d', strtotime($date_to));
                $where .= " AND {$tblpx}dailyexpense.date between '" . $date_from . "' and '" . $date_to . "'";
                $where1 .= " AND {$tblpx}expenses.date between '" . $date_from . "' and '" . $date_to . "'";
                $where2 .= " AND {$tblpx}subcontractor_payment.date between '" . $date_from . "' and '" . $date_to . "'";
            } else {
                if (!empty($date_from)) {
                    $date_from = date('Y-m-d', strtotime($date_from));
                    $where .= " AND {$tblpx}dailyexpense.date >= '" . $date_from . "'";
                    $where1 .= " AND {$tblpx}expenses.date >= '" . $date_from . "'";
                    $where2 .= " AND {$tblpx}subcontractor_payment.date >= '" . $date_to . "'";
                }
                if (!empty($date_to)) {
                    $date_to = date('Y-m-d', strtotime($date_to));
                    $where .= " AND {$tblpx}dailyexpense.date <= '" . $date_to . "'";
                    $where1 .= " AND {$tblpx}expenses.date <= '" . $date_to . "'";
                    $where2 .= " AND {$tblpx}subcontractor_payment.date <= '" . $date_to . "'";
                }
            }


            //petty cash advance
            $reconcilsql = "SELECT *  FROM `jp_reconciliation` "
                . " WHERE `reconciliation_status`='0' "
                . " and reconciliation_payment='Dailyexpense Payment' and "
                . "reconciliation_table='jp_dailyexpense'";
            $reconcil_dexpense_payment_data = Yii::app()->db->createCommand($reconcilsql)->queryAll();

            $payment_cheque_array = array();
            foreach ($reconcil_dexpense_payment_data as  $data) {

                $value = "'{$data['reconciliation_chequeno']}'";
                if ($value != '') {
                    $recon_data = $value;
                    array_push($payment_cheque_array, $recon_data);
                }
            }
            $payment_cheque_array1 = implode(',', $payment_cheque_array);
            if (($payment_cheque_array1)) {
                $chquery1 = "(dailyexpense_chequeno NOT IN ( $payment_cheque_array1) OR dailyexpense_chequeno IS NULL)";
                $chcondition1 = " AND " . $chquery1 . "";
            }

            $petty_issued_id = Yii::app()->db->createCommand("SELECT `company_exp_id` 
            FROM `jp_company_expense_type` WHERE  UPPER(`name`)='PETTY CASH ISSUED'")->queryScalar();
            $dailyexpense = array();
            if($petty_issued_id){
                $dailyexpensesql = "SELECT * FROM " . $tblpx . "dailyexpense "
                . " LEFT JOIN " . $tblpx . "users "
                . " ON " . $tblpx . "dailyexpense.employee_id= " . $tblpx . "users.userid "
                . " WHERE " . $tblpx . "dailyexpense.exp_type = 73 "
                . " AND " . $tblpx . "dailyexpense.expense_type IN('89','88') "  . $chcondition1 . "  "
                . " AND " . $tblpx . "dailyexpense.exp_type_id = $petty_issued_id " . $where . " "
                 
                . " ORDER BY " . $tblpx . "dailyexpense.date ASC";
            $dailyexpense = Yii::app()->db->createCommand($dailyexpensesql)->queryAll();
            }
            //petty cash refund
            $reconcil_receipt_sql = "SELECT *  FROM `jp_reconciliation` "
                . " WHERE `reconciliation_status`='0' "
                . " and reconciliation_payment='Dailyexpense Receipt' and "
                . "reconciliation_table='jp_dailyexpense'";
            $reconcil_dexpense_receipt_data = Yii::app()->db->createCommand($reconcil_receipt_sql)->queryAll();
            $receipt_cheque_array = array();
            foreach ($reconcil_dexpense_receipt_data as  $receiptdata) {
                $receiptvalue = "'{$receiptdata['reconciliation_chequeno']}'";
                if ($receiptvalue != '') {
                    $recon_receipt_data = $receiptvalue;
                    array_push($receipt_cheque_array, $recon_receipt_data);
                }
            }
            $receipt_cheque_array1 = implode(',', $receipt_cheque_array);
            if (($receipt_cheque_array1)) {
                $chquery2 = "(dailyexpense_chequeno NOT IN ( $receipt_cheque_array1) OR dailyexpense_chequeno IS NULL)";
                $chcondition2 = " AND " . $chquery2 . "";
            }

            $petty_refund_id = Yii::app()->db->createCommand("SELECT `company_exp_id` 
            FROM `jp_company_expense_type` WHERE  UPPER(`name`)='PETTY CASH REFUND'")->queryScalar();
            $dailyexpense_refund=array();
            if($petty_refund_id){
                $dexp_refund_sql = "SELECT * FROM " . $tblpx . "dailyexpense "
                    . " LEFT JOIN " . $tblpx . "users "
                    . " ON " . $tblpx . "dailyexpense.employee_id= " . $tblpx . "users.userid "
                    . " WHERE " . $tblpx . "dailyexpense.exp_type = 72 "
                    . " AND " . $tblpx . "dailyexpense.dailyexpense_receipt_type IN('88','89') "  . $chcondition2 . "  "
                    . " AND " . $tblpx . "dailyexpense.exp_type_id = $petty_refund_id " . $where . " "
                    
                    . " ORDER BY " . $tblpx . "dailyexpense.date ASC";

                $dailyexpense_refund = Yii::app()->db->createCommand($dexp_refund_sql)->queryAll();
            }

            //petty cash expense
            $daybook_sql = "SELECT * ," . $tblpx . "expenses.description as dbookdesc "
            . " FROM " . $tblpx . "expenses LEFT JOIN " . $tblpx . "users "
            . " ON " . $tblpx . "expenses.employee_id= " . $tblpx . "users.userid "
            . " LEFT JOIN " . $tblpx . "projects "
            . " ON " . $tblpx . "projects.pid = " . $tblpx . "expenses.projectid "
            . " WHERE " . $tblpx . "expenses.type = 73  "
            . " AND " . $tblpx . "expenses.expense_type=103 " . $where1 . " "

            . " GROUP BY " . $tblpx . "expenses.projectid";
            $daybook = Yii::app()->db->createCommand($daybook_sql)->queryAll();
            $dailyexpense_exp_sql = "SELECT * FROM " . $tblpx . "dailyexpense "
            . " LEFT JOIN " . $tblpx . "users "
            . " ON " . $tblpx . "dailyexpense.employee_id= " . $tblpx . "users.userid "
            . " WHERE (" . $tblpx . "dailyexpense.dailyexpense_receipt_type = 103 "
            . " OR  " . $tblpx . "dailyexpense.expense_type = 103 ) " . $where . " "
           
            . " ORDER BY " . $tblpx . "dailyexpense.date ASC";

            $dailyexpense_exp = Yii::app()->db->createCommand($dailyexpense_exp_sql)->queryAll();
            $scp_sql = "SELECT * ," . $tblpx . "subcontractor_payment.description as description "
            . " FROM " . $tblpx . "subcontractor_payment "
            . " LEFT JOIN " . $tblpx . "users "
            . " ON " . $tblpx . "subcontractor_payment.employee_id= " . $tblpx . "users.userid "
            . " LEFT JOIN " . $tblpx . "projects "
            . " ON " . $tblpx . "projects.pid = " . $tblpx . "subcontractor_payment.project_id "
            . " WHERE " . $tblpx . "subcontractor_payment.payment_type=103 " . $where2 ;

                //. " GROUP BY " . $tblpx . "subcontractor_payment.project_id";                
            $scpayment = Yii::app()->db->createCommand($scp_sql)->queryAll();

           


        $render_datas = array(
            'employee_id' => $employee_id,
            'dailyexpense' => $dailyexpense,
            'date_from' => $date_from,
            'date_to' => $date_to,
            'daybook' => $daybook,
            'dailyexpense_refund' => $dailyexpense_refund,
            'company_id' => $company_id,
            'dailyexpense_exp' => $dailyexpense_exp,
            'scpayment'=>$scpayment,
            'where' =>$where,
            'where1'=>$where1,
            'where2' =>$where2,
            'petty_issued_id'=>$petty_issued_id,
            'petty_refund_id'=>$petty_refund_id
        );
        // echo '<pre>';print_r($render_datas);exit;
        return $render_datas;
        // $this->render('pettycashreport', $render_datas);
    }
    public function getSalesGraph($currentMonthStart,$currentMonthEnd, $currentMonthName,$previousMonthStart,$previousMonthEnd,$previousMonthName,$twoMonthsAgoStart,$twoMonthsAgoEnd,$twoMonthsAgoName,$companyId)
    {
        $tblpx = Yii::app()->db->tablePrefix;
    
        // Get current and previous months' start and end dates
        // e.g., "November"
        $criteria = new CDbCriteria();
        $invoicesql = "SELECT * FROM {$tblpx}invoice WHERE invoice_status = 'saved'";
        
        if (!empty($companyId)) {
            $invoicesql .= " AND company_id = :companyId";
        }
        
        $command = Yii::app()->db->createCommand($invoicesql);
        
        if (!empty($companyId)) {
            $command->bindParam(":companyId", $companyId, PDO::PARAM_INT);
        }
        
        $invoices = $command->queryAll();
        // Initialize totals
        $currentMonthTotal = 0;
        $previousMonthTotal = 0;
        $twoMonthsAgoTotal = 0;
        // Loop through invoices to calculate totals
        foreach ($invoices as $invoice) {
            $invoiceDate = $invoice['date']; 
            $invoiceAmount = isset($invoice['amount']) ? (float)$invoice['amount'] : 0; 
            $taxAmount = isset($invoice['tax_amount']) ? (float)$invoice['tax_amount'] : 0;
    
            if ($invoiceDate >= $currentMonthStart && $invoiceDate <= $currentMonthEnd) {
                $currentMonthTotal += $invoiceAmount + $taxAmount;
            } elseif ($invoiceDate >= $previousMonthStart && $invoiceDate <= $previousMonthEnd) {
                $previousMonthTotal += $invoiceAmount + $taxAmount; 
            } elseif ($invoiceDate >= $twoMonthsAgoStart && $invoiceDate <= $twoMonthsAgoEnd) {
                $twoMonthsAgoTotal += $invoiceAmount + $taxAmount;
            }
        }
    
        // Return totals and month names as an array
        $result = [
            'current_month' => [
                'month' => $currentMonthName,
                'sales' => $currentMonthTotal,
            ],
            'previous_month' => [
                'month' => $previousMonthName,
                'sales' => $previousMonthTotal,
            ],
            'two_months_ago' => [
                'month' => $twoMonthsAgoName,
                'sales' => $twoMonthsAgoTotal,
            ],
        ];
        // echo'<pre>';print_r($result);exit;
        return $result;
    }
    
    public function getProfitLossGraph($month, $companyId)
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $result = [];

        if ($month == 5) {
            // Last 6 months including the current month
            $count = 5;
            $months = [];
            for ($i = $count; $i >= 0; $i--) {
                $months[] = date('Y-m', strtotime("-{$i} month"));
            }

            foreach ($months as $month) {
                $date_from = $month . '-01';
                $date_to = date('Y-m-t', strtotime($date_from));

                $companyCondition = $companyId ? " AND company_id = :companyId" : "";

                // Income
                $total_receipt = Yii::app()->db->createCommand("
                    SELECT SUM(receipt) as total_amount 
                    FROM {$tblpx}expenses 
                    WHERE date BETWEEN :date_from AND :date_to AND payment_type IN(88,89) AND type=72 AND return_id IS NULL {$companyCondition}
                ")->queryScalar(array_filter([':date_from' => $date_from, ':date_to' => $date_to, ':companyId' => $companyId]));

                $total_invoice = Yii::app()->db->createCommand("
                    SELECT SUM({$tblpx}invoice.amount) + SUM({$tblpx}invoice.tax_amount) as total_amount 
                    FROM {$tblpx}invoice 
                    WHERE date BETWEEN :date_from AND :date_to AND invoice_status = 'saved' {$companyCondition}
                ")->queryScalar(array_filter([':date_from' => $date_from, ':date_to' => $date_to, ':companyId' => $companyId]));

                $income = $total_invoice;

                // Expenses
                $total_expense = Yii::app()->db->createCommand("
                    SELECT SUM(paidamount) as total_amount 
                    FROM {$tblpx}expenses 
                    WHERE date BETWEEN :date_from AND :date_to AND expense_type IN(88,89) AND type=73 {$companyCondition}
                ")->queryScalar(array_filter([':date_from' => $date_from, ':date_to' => $date_to, ':companyId' => $companyId]));

                $total_daily_expense = Yii::app()->db->createCommand("
                    SELECT SUM(amount) as total_amount 
                    FROM {$tblpx}dailyexpense 
                    WHERE date BETWEEN :date_from AND :date_to AND expense_type IN(88,89) {$companyCondition}
                ")->queryScalar(array_filter([':date_from' => $date_from, ':date_to' => $date_to, ':companyId' => $companyId]));

                $expenses = $total_expense + $total_daily_expense;
                // echo($expenses);exit;
                // Add to result array
                $result[] = [
                    'year' => date('M', strtotime($date_from)),
                    'income' => round($income, 2),
                    'expenses' => round($expenses, 2),
                ];
            }
        } elseif ($month == 1) {
            // Last 6 years
            $count = 5; // 6 years (current year and the last 5 years)
            $years = [];
            for ($i = $count; $i >= 0; $i--) {
                $years[] = date('Y', strtotime("-{$i} year"));
            }

            foreach ($years as $year) {
                $date_from = $year . '-01-01'; // Start of the year
                $date_to = $year . '-12-31'; // End of the year

                $companyCondition = $companyId ? " AND company_id = :companyId" : "";

                // Income
                $total_receipt = Yii::app()->db->createCommand("
                    SELECT SUM(receipt) as total_amount 
                    FROM {$tblpx}expenses 
                    WHERE date BETWEEN :date_from AND :date_to AND payment_type IN(88,89) AND type=72 AND return_id IS NULL {$companyCondition}
                ")->queryScalar(array_filter([':date_from' => $date_from, ':date_to' => $date_to, ':companyId' => $companyId]));

                $total_invoice = Yii::app()->db->createCommand("
                    SELECT SUM({$tblpx}invoice.amount) + SUM({$tblpx}invoice.tax_amount) as total_amount 
                    FROM {$tblpx}invoice 
                    WHERE date BETWEEN :date_from AND :date_to AND invoice_status = 'saved' {$companyCondition}
                ")->queryScalar(array_filter([':date_from' => $date_from, ':date_to' => $date_to, ':companyId' => $companyId]));

                $income = $total_invoice;

                // Expenses
                $total_expense = Yii::app()->db->createCommand("
                    SELECT SUM(paidamount) as total_amount 
                    FROM {$tblpx}expenses 
                    WHERE date BETWEEN :date_from AND :date_to AND expense_type IN(88,89) AND type=73 {$companyCondition}
                ")->queryScalar(array_filter([':date_from' => $date_from, ':date_to' => $date_to, ':companyId' => $companyId]));

                $total_daily_expense = Yii::app()->db->createCommand("
                    SELECT SUM(amount) as total_amount 
                    FROM {$tblpx}dailyvendors 
                    WHERE date BETWEEN :date_from AND :date_to AND payment_type IN(88,89) {$companyCondition}
                ")->queryScalar(array_filter([':date_from' => $date_from, ':date_to' => $date_to, ':companyId' => $companyId]));

                $expenses = $total_expense + $total_daily_expense;
                // Add to result array
                $result[] = [
                    'year' => $year, // Use year for the label
                    'income' => round($income, 2),
                    'expenses' => round($expenses, 2),
                ];
            }
        }

        return $result;
    }

    
    
    public function getProfitLossTable() {
        $date_from = date("Y-") . "01-" . "01";
        $date_to = date("Y-m-d");
        $tblpx = Yii::app()->db->tablePrefix;
        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        $result = [];
        // Loop through each company and calculate totals
        foreach ($arrVal as $arr) {
            // Prepare the query parts for the current company
            $newQuery = "FIND_IN_SET('" . $arr . "', company_id)";
            $newQuery1 = "FIND_IN_SET('" . $arr . "', {$tblpx}invoice.company_id)";
            $newQuery3 = "FIND_IN_SET('" . $arr . "', E.company_id)";
            $newQuery4 = "FIND_IN_SET('" . $arr . "', jp_purchase.company_id)";
    
            // Retrieve data for each company
            
            // Daily Expenses for the company
            $daily_expenses = Yii::app()->db->createCommand("
                SELECT SUM(amount) as total_amount 
                FROM {$tblpx}dailyexpense 
                WHERE date BETWEEN :date_from AND :date_to AND expense_type IN(88,89) AND ($newQuery)
            ")->queryScalar([':date_from' => $date_from, ':date_to' => $date_to]);
            
            // Total Receipts for the company
            $total_receipt = Yii::app()->db->createCommand("
                SELECT SUM(receipt) as total_amount 
                FROM {$tblpx}expenses 
                WHERE date BETWEEN :date_from AND :date_to AND payment_type IN(88,89) AND type=72 AND ($newQuery) AND return_id IS NULL
            ")->queryScalar([':date_from' => $date_from, ':date_to' => $date_to]);
    
            // Total Invoices for the company
            $total_invoice = Yii::app()->db->createCommand("
                SELECT SUM({$tblpx}invoice.amount) + SUM({$tblpx}invoice.tax_amount) as total_amount 
                FROM {$tblpx}invoice 
                LEFT JOIN {$tblpx}inv_list ON {$tblpx}invoice.invoice_id = {$tblpx}inv_list.inv_id 
                WHERE {$tblpx}invoice.date BETWEEN :date_from AND :date_to AND {$tblpx}invoice.invoice_status ='saved' AND {$newQuery1}
            ")->queryScalar([':date_from' => $date_from, ':date_to' => $date_to]);
            
            // Total Expenses for the company
            $total_expense = Yii::app()->db->createCommand("
                SELECT SUM(paidamount) as total_amount 
                FROM {$tblpx}expenses 
                WHERE date BETWEEN :date_from AND :date_to AND expense_type IN(88,89) AND type=73 AND ($newQuery) 
            ")->queryScalar([':date_from' => $date_from, ':date_to' => $date_to]);
            
            // Total Daily Vendor Expenses for the company
            $total_daily_expense = Yii::app()->db->createCommand("
                SELECT SUM(amount) as total_amount 
                FROM {$tblpx}dailyvendors 
                WHERE date BETWEEN :date_from AND :date_to AND payment_type IN(88,89) AND ($newQuery)
            ")->queryScalar([':date_from' => $date_from, ':date_to' => $date_to]);
            
            // Total Deposit for the company (same as total receipt)
            $total_deposit = $total_receipt; 
    
            // Purchase Return for the company
            $purchase_return = Yii::app()->db->createCommand("
                SELECT SUM(receipt) as total_amount 
                FROM {$tblpx}expenses 
                WHERE date BETWEEN :date_from AND :date_to AND payment_type IN(88,89) AND type=72 AND ($newQuery) AND return_id IS NOT NULL
            ")->queryScalar([':date_from' => $date_from, ':date_to' => $date_to]);
    
            // Add the company-wise results to the result array
            $result[] = [
                'Company' => $arr,
                'Total Receipt' => $total_receipt,
                'Total Invoice' => $total_invoice,
                'Total Expense' => $total_expense,
                'Total Daily Expense' => $total_daily_expense,
                'Total Deposit' => $total_deposit,
                'Purchase Return' => $purchase_return,
            ];
        }
    
        return $result;
    }
    
    

    public function getExpenseBalance($startOfMonth, $endOfMonth, $companyId)
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition('amount IS NOT NULL');

        if (!empty($companyId)) {
            $criteria->addCondition('company_id = :companyId');
            $criteria->params[':companyId'] = $companyId;
        }

        $expenseHeads = ExpenseType::model()->findAll();
        $directExpenses = [
            'category' => 'Direct Expense',
            'value' => 0,
            'subData' => [],
        ];
        $indirectExpenses = [
            'category' => 'Indirect Expense',
            'value' => 0,
            'subData' => [],
        ];

        // Fetch Labour data
        foreach ($expenseHeads as $expenseHead) {
            $criteria = new CDbCriteria();
            $criteria->addCondition('amount IS NOT NULL');
            $criteria->addCondition('expensehead_id = :expensehead_id');
            $criteria->params = [':expensehead_id' => $expenseHead->type_id];

            if (!empty($companyId)) {
                $criteria->addCondition('company_id = :companyId');
                $criteria->params[':companyId'] = $companyId;
            }

            $criteria->addCondition('DATE(date) BETWEEN :startOfMonth AND :endOfMonth');
            $criteria->params[':startOfMonth'] = $startOfMonth;
            $criteria->params[':endOfMonth'] = $endOfMonth;

            $labours = Dailyreport::model()->findAll($criteria);
            $totalLabourAmount = 0;
            foreach ($labours as $labour) {
                $totalLabourAmount += $labour->amount;
            }

            $expenseData = [
                'category' => $expenseHead->type_name,
                'value' => $totalLabourAmount,
            ];

            if ($expenseHead->expense_category == 1 && $totalLabourAmount > 0) {
                $directExpenses['subData'][] = $expenseData;
                $directExpenses['value'] += $totalLabourAmount;
            } elseif ($expenseHead->expense_category == 2 && $totalLabourAmount > 0) {
                $indirectExpenses['subData'][] = $expenseData;
                $indirectExpenses['value'] += $totalLabourAmount;
            }
        }

        // Subcontractor data
        foreach ($expenseHeads as $expenseHead) {
            $totalSubAmount = 0;
            $subcontractorExpenses = Scquotation::model()->findAllByAttributes(['expensehead_id' => $expenseHead->type_id]);
            if (!empty($subcontractorExpenses)) {
                foreach ($subcontractorExpenses as $subcontractorExpense) {
                    $criteria = new CDbCriteria();
                    $criteria->addCondition('subcontractor_id = :subcontractor_id');
                    if (!empty($companyId)) {
                        $criteria->addCondition('company_id = :companyId');
                        $criteria->params[':companyId'] = $companyId;
                    }
                    $criteria->addCondition('DATE(date) BETWEEN :startOfMonth AND :endOfMonth');
                    $criteria->params[':subcontractor_id'] = $subcontractorExpense->subcontractor_id;
                    $criteria->params[':startOfMonth'] = $startOfMonth;
                    $criteria->params[':endOfMonth'] = $endOfMonth;

                    $subcontractors = SubcontractorPayment::model()->findAll($criteria);
                    foreach ($subcontractors as $subcontractor) {
                        $totalSubAmount += $subcontractor->amount;
                    }
                }
            }

            if ($totalSubAmount > 0) {
                $expenseData = [
                    'category' => $expenseHead->type_name,
                    'value' => $totalSubAmount,
                ];

                if ($expenseHead->expense_category == 1 && $totalSubAmount > 0) {
                    $directExpenses['subData'][] = $expenseData;
                    $directExpenses['value'] += $totalSubAmount;
                } elseif ($expenseHead->expense_category == 2 && $totalSubAmount > 0) {
                    $indirectExpenses['subData'][] = $expenseData;
                    $indirectExpenses['value'] += $totalSubAmount;
                }
            }
        }

        // Day Book
        foreach ($expenseHeads as $expenseHead) {
            $criteria = new CDbCriteria();
            $criteria->addCondition('type = 72');
            $criteria->addCondition('exptype = :exptype');
            $criteria->params = [':exptype' => $expenseHead->type_id];

            if (!empty($companyId)) {
                $criteria->addCondition('company_id = :companyId');
                $criteria->params[':companyId'] = $companyId;
            }

            $criteria->addCondition('DATE(date) BETWEEN :startOfMonth AND :endOfMonth');
            $criteria->params[':startOfMonth'] = $startOfMonth;
            $criteria->params[':endOfMonth'] = $endOfMonth;

            $dailyEntries = Expenses::model()->findAll($criteria);
            $totalDailyEntryAmount = 0;
            foreach ($dailyEntries as $dailyEntry) {
                $totalDailyEntryAmount += $dailyEntry->amount;
            }

            $expenseData = [
                'category' => $expenseHead->type_name,
                'value' => $totalDailyEntryAmount,
            ];

            if ($expenseHead->expense_category == 1 && $totalDailyEntryAmount > 0) {
                $directExpenses['subData'][] = $expenseData;
                $directExpenses['value'] += $totalDailyEntryAmount;
            } elseif ($expenseHead->expense_category == 2 && $totalDailyEntryAmount > 0) {
                $indirectExpenses['subData'][] = $expenseData;
                $indirectExpenses['value'] += $totalDailyEntryAmount;
            }
        }

        $result = [$directExpenses, $indirectExpenses];
        return $result;
    }
    
    public function getPayableDashboardLine($currentMonthStart,$currentMonthEnd,$companyId) {
        
        $bill_list = array();
        $bills = "";
        $condition = "";
        $newQuery='';
        $newQuery1='';
        $command = Yii::app()->db->createCommand()->select('b.bill_id as billId');
        $command->from($this->tableNameAcc('expenses', 0) . ' as e');
        $command->leftJoin($this->tableNameAcc('bills', 0) . ' as b', 'e.bill_id = b.bill_id');
        $command->where("e.type = 73 AND b.bill_totalamount IS NOT NULL AND b.purchase_id IS NOT NULL");
        $billsArray = $command->queryAll();
    
        if (!empty($billsArray)) {
            foreach ($billsArray as $entry) {
                if ($entry['billId'] != "") {
                    $bill_list[] = $entry['billId'];
                }
            }
            $bills = implode(',', $bill_list);
            $condition = "AND b.bill_id NOT IN ($bills)";
        }
        $newQuery .= " b.bill_date between '".$currentMonthStart."' and '".$currentMonthEnd."'";
        $newQuery1 .= " sb.date between '".$currentMonthStart."' and '".$currentMonthEnd."'";
        if (!empty($companyId)) {
            $newQuery .= " AND FIND_IN_SET('" . $companyId . "', b.company_id)";
            $newQuery1 .= " AND FIND_IN_SET('" . $companyId . "', sb.company_id)";
            $sql = "(SELECT b.bill_id as bill_id, 
                        b.bill_number as bill_number, 
                        b.bill_date as bill_date, 
                        COALESCE((b.bill_totalamount- COALESCE(e.paidamount,0)),0) as bill_amount,
                        v.name as name, 
                        v.payment_date_range as payment_date_range, 
                        b.company_id as company_id, 
                        'vendor' as type 
                    FROM jp_bills b 
                    LEFT JOIN jp_purchase p ON b.purchase_id = p.p_id 
                    LEFT JOIN jp_vendors v ON v.vendor_id = p.vendor_id
                    LEFT JOIN jp_expenses e ON e.bill_id=b.bill_id 
                    WHERE 1=1 AND".$newQuery."  GROUP BY b.bill_id HAVING COALESCE(SUM(e.paidamount), 0) = 0 OR COALESCE(SUM(e.paidamount), 0) < MAX(b.bill_totalamount)) 
                    UNION ALL 
                    (SELECT sb.id AS bill_id, 
                            sb.bill_number AS bill_number, 
                            sb.date AS bill_date, 
                            (sb.total_amount - sp.amount) AS bill_amount,  -- Include bill_amount for subcontractor bills
                            s.subcontractor_name AS name, 
                            NULL AS payment_date_range, 
                            sb.company_id AS company_id, 
                            'subcontractor' AS type 
                    FROM jp_subcontractorbill sb 
                    LEFT JOIN jp_subcontractor_payment sp ON sp.sc_bill_id = sb.id 
                    LEFT JOIN jp_scquotation sq ON sq.scquotation_id = sb.scquotation_id 
                    LEFT JOIN jp_subcontractor s ON s.subcontractor_id = sq.subcontractor_id where ".$newQuery1."
                    GROUP BY sb.id, s.subcontractor_name 
                    HAVING COALESCE(SUM(sp.amount), 0) = 0 OR COALESCE(SUM(sp.amount), 0) < MAX(sb.total_amount))";
    // die($sql);
            $aging_array = Yii::app()->db->createCommand($sql)->queryAll();
            // echo'<pre>';print_r($aging_array);exit;
        }
        else {
            $sql = "(SELECT b.bill_id as bill_id, 
                            b.bill_number as bill_number, 
                            b.bill_date as bill_date, 
                            b.bill_amount as bill_amount,  -- Include bill_amount for vendor bills
                            v.name as name, 
                            v.payment_date_range as payment_date_range, 
                            'vendor' as type 
                     FROM jp_bills b 
                     LEFT JOIN jp_purchase p ON b.purchase_id = p.p_id 
                     LEFT JOIN jp_vendors v ON v.vendor_id = p.vendor_id 
                     WHERE 1=1 AND  $newQuery $condition) 
                     UNION ALL 
                     (SELECT sb.id AS bill_id, 
                             sb.bill_number AS bill_number, 
                             sb.date AS bill_date, 
                             sb.total_amount AS bill_amount,  -- Include bill_amount for subcontractor bills
                             s.subcontractor_name AS name, 
                             NULL AS payment_date_range, 
                             'subcontractor' AS type 
                      FROM jp_subcontractorbill sb 
                      LEFT JOIN jp_subcontractor_payment sp ON sp.sc_bill_id = sb.id 
                      LEFT JOIN jp_scquotation sq ON sq.scquotation_id = sb.scquotation_id 
                      LEFT JOIN jp_subcontractor s ON s.subcontractor_id = sq.subcontractor_id where $newQuery1
                      GROUP BY sb.id, s.subcontractor_name 
                      HAVING COALESCE(SUM(sp.amount), 0) = 0 OR COALESCE(SUM(sp.amount), 0) < MAX(sb.total_amount))";
        
            $aging_array = Yii::app()->db->createCommand($sql)->queryAll();
        }
        // $total_amount = array_sum($aging_array['bill_amount']);
        $total_amount = array_sum(array_column($aging_array, 'bill_amount'));
        return $total_amount;
    }
    public function getPayableDashboard($currentMonthStart,$currentMonthEnd,$previousMonthStart,$previousMonthEnd,$companyId) {
        
        $bill_list = array();
        $bills = "";
        $condition = "";
        $newQuery='';
        $newQuery1='';
        $command = Yii::app()->db->createCommand()->select('b.bill_id as billId');
        $command->from($this->tableNameAcc('expenses', 0) . ' as e');
        $command->leftJoin($this->tableNameAcc('bills', 0) . ' as b', 'e.bill_id = b.bill_id');
        $command->where("e.type = 73 AND b.bill_totalamount IS NOT NULL AND b.purchase_id IS NOT NULL");
        $billsArray = $command->queryAll();
    
        if (!empty($billsArray)) {
            foreach ($billsArray as $entry) {
                if ($entry['billId'] != "") {
                    $bill_list[] = $entry['billId'];
                }
            }
            $bills = implode(',', $bill_list);
            $condition = "AND b.bill_id NOT IN ($bills)";
        }
        if (!empty($companyId)) {
            $newQuery .= " FIND_IN_SET('" . $companyId . "', b.company_id)";
            $newQuery1 .= " FIND_IN_SET('" . $companyId . "', sb.company_id)";
            $sql = "(SELECT b.bill_id as bill_id, 
                        b.bill_number as bill_number, 
                        b.bill_date as bill_date, 
                        COALESCE((b.bill_totalamount- COALESCE(e.paidamount,0)),0) as bill_amount,
                        v.name as name, 
                        v.payment_date_range as payment_date_range, 
                        b.company_id as company_id, 
                        'vendor' as type 
                    FROM jp_bills b 
                    LEFT JOIN jp_purchase p ON b.purchase_id = p.p_id 
                    LEFT JOIN jp_vendors v ON v.vendor_id = p.vendor_id
                    LEFT JOIN jp_expenses e ON e.bill_id=b.bill_id 
                    WHERE 1=1 AND".$newQuery."  GROUP BY b.bill_id HAVING COALESCE(SUM(e.paidamount), 0) = 0 OR COALESCE(SUM(e.paidamount), 0) < MAX(b.bill_totalamount)) 
                    UNION ALL 
                    (SELECT sb.id AS bill_id, 
                            sb.bill_number AS bill_number, 
                            sb.date AS bill_date, 
                            (sb.total_amount - sp.amount) AS bill_amount,  -- Include bill_amount for subcontractor bills
                            s.subcontractor_name AS name, 
                            NULL AS payment_date_range, 
                            sb.company_id AS company_id, 
                            'subcontractor' AS type 
                    FROM jp_subcontractorbill sb 
                    LEFT JOIN jp_subcontractor_payment sp ON sp.sc_bill_id = sb.id 
                    LEFT JOIN jp_scquotation sq ON sq.scquotation_id = sb.scquotation_id 
                    LEFT JOIN jp_subcontractor s ON s.subcontractor_id = sq.subcontractor_id where ".$newQuery1."
                    GROUP BY sb.id, s.subcontractor_name 
                    HAVING COALESCE(SUM(sp.amount), 0) = 0 OR COALESCE(SUM(sp.amount), 0) < MAX(sb.total_amount))";
    
            $aging_array = Yii::app()->db->createCommand($sql)->queryAll();
            // echo'<pre>';print_r($aging_array);exit;
        }
        else {
            $sql = "(SELECT b.bill_id as bill_id, 
                            b.bill_number as bill_number, 
                            b.bill_date as bill_date, 
                            b.bill_amount as bill_amount,  -- Include bill_amount for vendor bills
                            v.name as name, 
                            v.payment_date_range as payment_date_range, 
                            'vendor' as type 
                     FROM jp_bills b 
                     LEFT JOIN jp_purchase p ON b.purchase_id = p.p_id 
                     LEFT JOIN jp_vendors v ON v.vendor_id = p.vendor_id 
                     WHERE 1=1 $condition) 
                     UNION ALL 
                     (SELECT sb.id AS bill_id, 
                             sb.bill_number AS bill_number, 
                             sb.date AS bill_date, 
                             sb.total_amount AS bill_amount,  -- Include bill_amount for subcontractor bills
                             s.subcontractor_name AS name, 
                             NULL AS payment_date_range, 
                             'subcontractor' AS type 
                      FROM jp_subcontractorbill sb 
                      LEFT JOIN jp_subcontractor_payment sp ON sp.sc_bill_id = sb.id 
                      LEFT JOIN jp_scquotation sq ON sq.scquotation_id = sb.scquotation_id 
                      LEFT JOIN jp_subcontractor s ON s.subcontractor_id = sq.subcontractor_id 
                      GROUP BY sb.id, s.subcontractor_name 
                      HAVING COALESCE(SUM(sp.amount), 0) = 0 OR COALESCE(SUM(sp.amount), 0) < MAX(sb.total_amount))";
        
            $aging_array = Yii::app()->db->createCommand($sql)->queryAll();
        }
    
        // Initialize totals
        $currentMonthTotal = 0;
        $previousMonthTotal = 0;
        // echo'<pre>';print_r($companyId);exit;
        foreach ($aging_array as $aging) {
            $billDate = date('Y-m-d', strtotime($aging['bill_date'])); // Convert the bill date to 'Y-m-d' format
            // echo'<pre>';print_r($billDate);exit;
            $billAmount = isset($aging['bill_amount']) ? $aging['bill_amount'] : 0;
            
            // Check if the bill date falls within the current month
            if ($billDate >= $currentMonthStart && $billDate <= $currentMonthEnd) {
                $currentMonthTotal += $billAmount;
            }
        
            // Check if the bill date falls within the previous month
            if ($billDate >= $previousMonthStart && $billDate <= $previousMonthEnd) {
                $previousMonthTotal += $billAmount;
            }
        }

        $dataValues = [
            [
                'name' => 'Accounts Payable',
                'current' => $currentMonthTotal,
                'previous' => $previousMonthTotal
            ]
        ];

        return $dataValues;
    }
    
    public function getProjectAgingSummary(){
        $inv_list = array();
        $invoice = "";
        $condition = "";
        $command = Yii::app()->db->createCommand()->select('i.invoice_id as invoiceId');
        $command->from($this->tableNameAcc('expenses', 0) . ' as e');
        $command->leftJoin($this->tableNameAcc('invoice', 0) . ' as i', 'e.invoice_id = i.invoice_id ');
        $command->where("e.type = 72 AND i.subtotal IS NOT NULL");
        $invArray = $command->queryAll();

        if (!empty($invArray)) {
            foreach ($invArray as $entry) {
                if ($entry['invoiceId'] != "") {
                    $inv_list[] = $entry['invoiceId'];
                }
            }
            $invoice = implode(',', $inv_list);
            $condition = "AND i.invoice_id NOT IN ($invoice)";
        }
        $sql = "SELECT i.inv_no,i.date,"
            . " p.name "
            . " FROM jp_invoice i LEFT JOIN jp_projects p "
            . " ON i.project_id = p.pid "        
            . "WHERE 1=1 $condition";
        
            
        $aging_array = Yii::app()->db->createCommand($sql)->queryAll();
        return $aging_array;
    }

    public function getPaymentReceivable($currentMonthStart,$currentMonthEnd,$previousMonthStart,$previousMonthEnd,$companyId){
        $inv_list = array();
        $invoice = "";
        $condition = "";
        $command = Yii::app()->db->createCommand()->select('i.invoice_id as invoiceId');
        $command->from($this->tableNameAcc('expenses', 0) . ' as e');
        $command->leftJoin($this->tableNameAcc('invoice', 0) . ' as i', 'e.invoice_id = i.invoice_id ');
        $command->where("e.type = 72 AND i.subtotal IS NOT NULL");
        $invArray = $command->queryAll();
        if (!empty($invArray)) {
            foreach ($invArray as $entry) {
                if ($entry['invoiceId'] != "") {
                    $inv_list[] = $entry['invoiceId'];
                }
            }
            $invoice = implode(',', $inv_list);
            $condition = "AND i.invoice_id NOT IN ($invoice)";
        }
        
            $newQuery="";
            $sql = "SELECT 
            b.invoice_id AS invoice_id, 
            b.inv_no AS inv_no, 
            b.date AS date, 
            COALESCE((b.amount - COALESCE(SUM(e.receipt), 0)), 0) AS amount, 
            b.company_id AS company_id
        FROM jp_invoice b 
        LEFT JOIN jp_expenses e ON e.invoice_id = b.invoice_id 
        WHERE 1=1 ";

        if (!empty($newQuery)) {
            $sql .= " AND " . $newQuery; 
        }

        if (!empty($companyId)) {
            $sql .= " AND b.company_id = :companyId"; 
        }

        $sql .= " GROUP BY b.invoice_id, b.inv_no, b.date, b.amount, b.company_id 
                HAVING COALESCE(SUM(e.receipt), 0) = 0 OR COALESCE(SUM(e.receipt), 0) < b.amount";

        $command = Yii::app()->db->createCommand($sql);

        if (!empty($companyId)) {
            $command->bindParam(':companyId', $companyId, PDO::PARAM_INT);
        }

           
        $aging_array = $command->queryAll();
        // echo '<pre>';print_r($aging_array);exit;
         // Initialize totals
         $currentMonthTotal = 0;
         $previousMonthTotal = 0;
    
         // Get current and previous month range
         
        foreach ($aging_array as $aging) {
            // $invoice = Invoice::model()->find('inv_no=:inv_no', array(':inv_no' => $aging['inv_no']));
            
            if ($invoice !== null) {
                $invoiceDate = $aging['date']; // Assuming `bill_date` is stored in YYYY-MM-DD format
                $invoiceAmount = $aging['amount'];
                // Check if the bill belongs to the current month
                if ($invoiceDate >= $currentMonthStart && $invoiceDate <= $currentMonthEnd) {
                    $currentMonthTotal += $invoiceAmount;
                }
                // Check if the bill belongs to the previous month
                elseif ($invoiceDate >= $previousMonthStart && $invoiceDate <= $previousMonthEnd) {
                    $previousMonthTotal += $invoiceAmount;
                }
            }
        }
    
        // Prepare the data to be returned
        $dataValues = [
            [
                'name' => 'Accounts Receivable',
                'current' => $currentMonthTotal,
                'previous' => $previousMonthTotal
            ]
        ];
        return $dataValues;
    }
    public function getPaymentReceivableLine($currentMonthStart, $currentMonthEnd,$companyId){
        $inv_list = array();
        $invoice = "";
        $condition = "";
        $command = Yii::app()->db->createCommand()->select('i.invoice_id as invoiceId');
        $command->from($this->tableNameAcc('expenses', 0) . ' as e');
        $command->leftJoin($this->tableNameAcc('invoice', 0) . ' as i', 'e.invoice_id = i.invoice_id ');
        $command->where("e.type = 72 AND i.subtotal IS NOT NULL");
        $invArray = $command->queryAll();

        if (!empty($invArray)) {
            foreach ($invArray as $entry) { 
                if ($entry['invoiceId'] != "") {
                    $inv_list[] = $entry['invoiceId'];
                }
            }
            $invoice = implode(',', $inv_list);
            $condition = "AND i.invoice_id NOT IN ($invoice)";
        }
        $newQuery="";
        $sql = "SELECT 
        b.invoice_id AS invoice_id, 
        b.inv_no AS inv_no, 
        b.date AS date, 
        COALESCE((b.amount - COALESCE(SUM(e.receipt), 0)), 0) AS amount, 
        b.company_id AS company_id
    FROM jp_invoice b 
    LEFT JOIN jp_expenses e ON e.invoice_id = b.invoice_id 
    WHERE 1=1 ";

    if (!empty($newQuery)) {
        $sql .= " AND " . $newQuery; 
    }

    if (!empty($companyId)) {
        $sql .= " AND b.company_id = :companyId"; 
    }

    $sql .= " GROUP BY b.invoice_id, b.inv_no, b.date, b.amount, b.company_id 
            HAVING COALESCE(SUM(e.receipt), 0) = 0 OR COALESCE(SUM(e.receipt), 0) < b.amount";

    $command = Yii::app()->db->createCommand($sql);

    if (!empty($companyId)) {
        $command->bindParam(':companyId', $companyId, PDO::PARAM_INT);
    }
            
        $aging_array = $command->queryAll();
        
         // Initialize totals
         $currentMonthTotal = 0;
         $previousMonthTotal = 0;
    
         // Get current and previous month range
         
        foreach ($aging_array as $aging) {
            // $invoice = Invoice::model()->find('inv_no=:inv_no', array(':inv_no' => $aging['inv_no']));
            
            if ($invoice !== null) {
                $invoiceDate = $aging['date']; // Assuming `bill_date` is stored in YYYY-MM-DD format
                $invoiceAmount = $aging['amount'];
                // Check if the bill belongs to the current month
                if ($invoiceDate >= $currentMonthStart && $invoiceDate <= $currentMonthEnd) {
                    $currentMonthTotal += $invoiceAmount;
                }
                // Check if the bill belongs to the previous month
                // elseif ($invoiceDate >= $previousMonthStart && $invoiceDate <= $previousMonthEnd) {
                //     $previousMonthTotal += $invoiceAmount;
                // }
            }
        }
    
        // Prepare the data to be returned
        // $dataValues = [
        //     [
        //         // 'name' => 'Accounts Receivable',
        //         'current' => $currentMonthTotal,
        //         // 'previous' => $previousMonthTotal
        //     ]
        // ];
        return $currentMonthTotal;
    }

    public function getAccepetedAmount($billId){
        
        $sql = "SELECT * FROM `jp_bills`  b "
            . " LEFT JOIN jp_warehousereceipt wr "
            . " ON b.bill_id=wr.warehousereceipt_bill_id "            
            . " WHERE b.bill_id = ".$billId;                       
        $bill_receipt_id= Yii::app()->db->createCommand($sql)->queryRow();

        if($bill_receipt_id['warehousereceipt_id'])
        $sql ="SELECT  SUM(`warehousereceipt_rate`*`warehousereceipt_accepted_quantity`) "
            . " AS `accepted_amount`"
            . " FROM `jp_warehousereceipt_items` "
            . " WHERE `warehousereceipt_id` = ".$bill_receipt_id['warehousereceipt_id'];                                           
        $receipt_amount=Yii::app()->db->createCommand($sql)->queryScalar();

        $sql = "SELECT SUM(paid) FROM `jp_expenses` WHERE bill_id=".$billId;
        $paidamount=Yii::app()->db->createCommand($sql)->queryScalar();
        
        return array('receipt_amount'=>number_format((float)$receipt_amount + 
        $bill_receipt_id['bill_taxamount'], 2, '.', ''),
        'paidamount'=>$paidamount);
    }

    public function getPaidBillAmount($data,$vendor_id){

        $bill_list = array();
        $bills = "";
        $tblpx  = Yii::app()->db->tablePrefix;
        $where = "";
        if($data==1){
            $where = " AND bill_date = DATE_SUB(NOW(), INTERVAL 0 DAY)";
        } else if($data==2){
            $where = " AND bill_date < DATE_SUB(NOW(), INTERVAL 1 DAY) AND bill_date > DATE_SUB(NOW(), INTERVAL 15 DAY) ";
        }else if($data==3){
            $where = " AND bill_date < DATE_SUB(NOW(), INTERVAL 16 DAY) AND bill_date > DATE_SUB(NOW(), INTERVAL 30 DAY) ";
        } else if($data==4){
            $where = " AND bill_date < DATE_SUB(NOW(), INTERVAL 31 DAY) AND bill_date > DATE_SUB(NOW(), INTERVAL 45 DAY) ";
        }else if($data==5){
            $where = " AND bill_date < DATE_SUB(NOW(), INTERVAL 45 DAY)";
        }

        $sql = "SELECT bill_id "
            . " FROM {$tblpx}bills as bill "
            . " left join {$tblpx}purchase as pu "
            . " on pu.p_id = bill.purchase_id "
            . " WHERE pu.vendor_id= ".$vendor_id 
            . " $where";
        $data = Yii::app()->db->createCommand($sql)->queryAll();
               
        if (!empty($data)) {                                    
            foreach ($data as $entry) {
                if ($entry['bill_id'] != "") {
                    $bill_list[] = $entry['bill_id'];
                }                    
            }
            $bills = implode(',', $bill_list);
        }

        $condition = "1=1";
        $expense_amount=0;
        // AND ( expense_type='107' OR expense_type = '0' ) =>credit
        if (!empty($bills)) {
            $condition = "bill_id IN (" . $bills . ")";
            $expense_amount = Yii::app()->db->createCommand("SELECT SUM(`paidamount`) FROM `jp_expenses` WHERE $condition")->queryScalar();
        }           
        return $expense_amount;        
    }

    public function getPaidInvoiceAmount($data,$project_id){

        $inv_list = array();
        $invoice = "";
        $tblpx  = Yii::app()->db->tablePrefix;
        $where = "";
        if($data==1){
            $where = " AND `date` = DATE_SUB(NOW(), INTERVAL 0 DAY)";
        } else if($data==2){
            $where = " AND `date` < DATE_SUB(NOW(), INTERVAL 1 DAY) AND `date` >= DATE_SUB(NOW(), INTERVAL 15 DAY) ";
        }else if($data==3){
            $where = " AND `date` < DATE_SUB(NOW(), INTERVAL 16 DAY) AND `date` > DATE_SUB(NOW(), INTERVAL 30 DAY) ";
        } else if($data==4){
            $where = " AND `date` < DATE_SUB(NOW(), INTERVAL 31 DAY) AND `date` > DATE_SUB(NOW(), INTERVAL 45 DAY) ";
        }else if($data==5){
            $where = " AND `date` < DATE_SUB(NOW(), INTERVAL 45 DAY)";
        }

        $sql = "SELECT invoice_id "
                . " FROM {$tblpx}invoice as i "
                . " WHERE `invoice_status` = 'saved' AND "
                . " i.project_id= ".$project_id 
                . " $where";  
        
        $data = Yii::app()->db->createCommand($sql)->queryAll();
               
        if (!empty($data)) {                                    
            foreach ($data as $entry) {
                if ($entry['invoice_id'] != "") {
                    $inv_list[] = $entry['invoice_id'];
                }                    
            }
            $invoice = implode(',', $inv_list);
        }

        $condition = "1=1";
        $expense_amount=0;
        if (!empty($invoice)) {
            $condition = "invoice_id IN (" . $invoice . ")";
            $expense_amount = Yii::app()->db->createCommand("SELECT SUM(`paidamount`) FROM `jp_expenses` WHERE $condition")->queryScalar();
        }           
        return $expense_amount;        
    }
    public function setSCQuotationsList($items_model, $model, $quotation_categories)
    {
        $items = array();
        foreach ($items_model as $item) {
            $items[] = $item->attributes;
        }

        $lists = array();
        if (!empty($quotation_categories)) {
            foreach ($quotation_categories as $quotation_category) {
                $data_items = array();
                $keys = array_keys(array_column($items, 'item_category_id'), $quotation_category->id);
                foreach ($keys as $key) {
                    array_push($data_items, $items[$key]);
                }
                $data = array('category_details' => $quotation_category->attributes, 'items_list' => $data_items);
                array_push($lists, $data);
            }
        } else {
            $data_items = array();
            $data = array();
            foreach ($items as $item) {
                array_push($data_items, $item);
                $data = array('category_details' => '', 'items_list' => $data_items);
            }
            if (!empty($data)) {
                array_push($lists, $data);
            }
        }
        return $lists;
    }


    public function getScBillPaymentStatus($id){
        $scbill = Subcontractorbill::model()->findBypk($id);
        $bill_amount = $scbill['total_amount'];
        $sql = "SELECT SUM(amount) FROM `jp_subcontractor_payment` "
                . " WHERE sc_bill_id=".$id;
        $paidamount = Yii::app()->db->createCommand($sql)->queryScalar();

        if($paidamount==0){
            $status =  "Pending";
        }else if($paidamount < $bill_amount){
            $status = "Partially Paid";
        }else if($paidamount == $bill_amount){
            $status =  "Fully Paid";
        }else{
            $status =  "";
        }

        return $status;
    }
    public static function getAllowedApps() {
        if (!Yii::app()->user->isGuest && (defined('LOGIN_USER_TABLE') && LOGIN_USER_TABLE != '')) {
            $query = "SELECT system_access_types FROM "
                    . "`global_users` WHERE  global_user_id = '" . Yii::app()->user->id . "'";
            $command = Yii::app()->db->createCommand($query);
            $result = $command->queryRow();
            $apps = strtolower($result['system_access_types']);
            if ($apps != '' and $apps !== null) {
                $allowed_apps = explode(',', $apps);
            } else {
                return false;
            }
            return $allowed_apps;
        }
        return false;
    }

    function cleanHtmlContent($html){
      
        $html = preg_replace('/<script\b[^>]*>(.*?)<\/script>/is', '', $html);
        $html = preg_replace('/\bon[a-z]+\s*=\s*"[^"]*"/i', '', $html);
        $html = preg_replace('/<a\b[^>]*\bhref=["\']javascript:(.*?)["\'][^>]*>(.*?)<\/a>/is', '', $html);

        return $html;
    }
    function getDaybookTransctions($where){
        $data= array();
        $tblpx  = Yii::app()->db->tablePrefix;
        $sql="SELECT *,{$tblpx}expenses.type as exp_type_id,{$tblpx}expenses.description as des ,{$tblpx}vendors.name as vendor_name,{$tblpx}projects.name as project_name, {$tblpx}expenses.expense_type as exp_type,{$tblpx}expenses.company_id as companyid FROM 
            `{$tblpx}expenses` left join {$tblpx}projects on projectid = pid 
            left join {$tblpx}users on {$tblpx}users.userid = {$tblpx}expenses.userid 
            join {$tblpx}status on sid = type left join {$tblpx}vendors on {$tblpx}vendors.vendor_id = {$tblpx}expenses.vendor_id 
            left join {$tblpx}expense_type on exptype = type_id " . $where . " ORDER BY exp_id desc";
        $data = Yii::app()->db->createCommand($sql)->queryAll();
       
        return $data;
    }
    
    function getDaybookTransctionsDashboard($where,$company){
        
        $data= array();
        $tblpx  = Yii::app()->db->tablePrefix;
        $where = !empty($where) ? $where . " AND {$tblpx}expenses.company_id = :company_id" : "WHERE {$tblpx}expenses.company_id = :company_id";

        $sql = "SELECT receipt, paid, date, {$tblpx}expenses.type as exp_type_id, {$tblpx}expenses.description as des, {$tblpx}vendors.name as vendor_name, {$tblpx}projects.name as project_name, {$tblpx}expenses.expense_type as exp_type, {$tblpx}expenses.company_id as companyid
                FROM {$tblpx}expenses
                LEFT JOIN {$tblpx}projects ON projectid = pid
                LEFT JOIN {$tblpx}users ON {$tblpx}users.userid = {$tblpx}expenses.userid
                JOIN {$tblpx}status ON sid = type
                LEFT JOIN {$tblpx}vendors ON {$tblpx}vendors.vendor_id = {$tblpx}expenses.vendor_id
                LEFT JOIN {$tblpx}expense_type ON exptype = type_id
                $where
                ORDER BY exp_id DESC";

        $data = Yii::app()->db->createCommand($sql)
            ->bindParam(':company_id', $company, PDO::PARAM_INT)
            ->queryAll();
        return $data;
    }

    function getDaybookPaymentDashboard($currentMonthStart,$currentMonthEnd,$company){
        
        $data = array();
        $tblpx  = Yii::app()->db->tablePrefix;
        $date_from = $currentMonthStart;
        $date_to = $currentMonthEnd;
        $exp_cal=0;
       
        if (!empty($company)) {
            // $criteria->addCondition("company_id = :companyId");
            // $criteria->params[':companyId'] = $companyId;
            $newQuery = "";
            $newQuery1 = "";
            $newQuery3 = "";
            $newQuery4 = "";
            // foreach ($arrVal as $arr) {
                $newQuery .= " FIND_IN_SET('" . $company . "', company_id)";
                    $newQuery1 .= " FIND_IN_SET('" . $company . "', {$tblpx}invoice.company_id)";
                    $newQuery3 .= " FIND_IN_SET('" . $company . "', E.company_id)";
                    $newQuery4 .= " FIND_IN_SET('" . $company . "', jp_purchase.company_id)";
            $company_det=Company::Model()->findByPk($company );
            $exp_cal =$company_det->expense_calculation;
            // }
        }
        else{
            $user = Users::model()->findByPk(Yii::app()->user->id);
                $arrVal = explode(',', $user->company_id);
                $newQuery = "";
                $newQuery1 = "";
                $newQuery3="";
                $newQuery4="";

                foreach ($arrVal as $arr) {
                    if ($newQuery) $newQuery .= ' OR';
                    if ($newQuery1) $newQuery1 .= ' OR';
                     if ($newQuery3) $newQuery3 .= ' OR';
                    if ($newQuery4) $newQuery4 .= ' OR';
                    $newQuery .= " FIND_IN_SET('" . $arr . "', company_id)";
                    $newQuery1 .= " FIND_IN_SET('" . $arr . "', {$tblpx}invoice.company_id)";
                    $newQuery3 .= " FIND_IN_SET('" . $arr . "', E.company_id)";
                    $newQuery4 .= " FIND_IN_SET('" . $arr . "', jp_purchase.company_id)";
                }
                $company =$arrVal["0"];
        }
         //die($sql);
         $daybook_expense_tot_amount=0;
         $vendor_sql="SELECT SUM(amount)as total_amount FROM {$tblpx}dailyvendors WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND payment_type IN(88,89) AND (" . $newQuery . ")";
         
         $daybooksql = "SELECT * FROM " . $tblpx . "expenses "
            . " WHERE amount!=0" 
            . " AND exptype IS NOT NULL AND vendor_id IS NOT NULL "
            . " AND bill_id IS NULL AND (" . $newQuery . ") "
            . " AND (reconciliation_status IS NULL OR reconciliation_status=1) "
            . " AND date BETWEEN '" . $date_from . "' AND '" . $date_to . "' "
            . " ORDER BY date";
         $daybook_expense_only = Yii::app()->db->createCommand($daybooksql)->queryAll();
        //  echo "<pre>"; print_r($daybook_expense);exit;
         if(!empty($daybook_expense_only)){
             foreach ($daybook_expense_only as $newsql) {
                $daybook_expense_tot_amount += $newsql['paidamount'];
             }
         }
         /** Subcontractor Payment starts */
         $check_array = array();
         $daybook_subcontractor_expenseamount = 0;
         
         $daybook_sub_expensesql = "SELECT * FROM " . $tblpx . "expenses "
                        . " WHERE amount != 0 "
                        . " AND type = '73' AND exptype IS NULL "
                        . " AND vendor_id IS NULL AND bill_id IS NULL AND (" . $newQuery . ") "
                        . " AND (reconciliation_status IS NULL OR reconciliation_status = 1) "
                        . " AND date BETWEEN '" . $date_from . "' AND '" . $date_to . "' "
                        . " ORDER BY date";

         $daybook_expense = Yii::app()->db->createCommand( $daybook_sub_expensesql)->queryAll();
         $subcontractor_payment_lists = Projects::model()->setSubcontractorPayment($daybook_expense);
        //  echo "<pre>";print_r( $subcontractor_payment_lists);exit;
        
             foreach ($subcontractor_payment_lists as $daybook_expense) {
             $scpayment_group_count = count($daybook_expense) - 1;
             $sc_index = 1;
             foreach ($daybook_expense as $newdata) {
                 if (is_array($newdata)) {
                     
                     $daybook_subcontractor_expenseamount+=$newdata['paid'];
                     $tblpx = Yii::app()->db->tablePrefix;
                     $payment = SubcontractorPayment::model()->findByPk($newdata['subcontractor_id']);
                     $query = "";
                     $subcontractor_id = '';
                     if (isset($payment->subcontractor_id)) {
                         $query = ' AND b.subcontractor_id=' . $payment->subcontractor_id . '';
                         $subcontractor_id = $payment->subcontractor_id;
                     }
                     $sub_total1 = Yii::app()->db->createCommand("
                            SELECT SUM(a.paidamount) AS paid_amount, 
                                COUNT(a.exp_id) AS total_count, 
                                b.* 
                            FROM jp_expenses a 
                            LEFT JOIN jp_subcontractor_payment b 
                            ON a.subcontractor_id = b.payment_id 
                            WHERE a.amount != 0 
                            AND a.projectid = " . $newdata['projectid'] . " 
                            AND a.type = '73' 
                            AND a.exptype IS NULL 
                            AND a.vendor_id IS NULL 
                            AND a.bill_id IS NULL 
                            AND a.date BETWEEN '" . $date_from . "' AND '" . $date_to . "' 
                            " . $query . " 
                            GROUP BY a.date, b.subcontractor_id
                        ")->queryRow();
                     $check_data = $newdata['date'] . '-' . $subcontractor_id;
                     if (in_array($check_data, $check_array)) {
                         $flag = 0;
                         $row_count = "";
                     } else {
                         $flag = 1;
                         $row_count = $sub_total1['total_count'];
                     }
                     array_push($check_array, $check_data);
                    
                             $payment = SubcontractorPayment::model()->findByPk($newdata['subcontractor_id']);
                             if (isset($payment->subcontractor_id)) {
                                 $subcontractor_data = Subcontractor::model()->findByPk($payment->subcontractor_id);
                             }
                            
                     $sc_index++;
                 }
             }
         }
         $bill_amount=0;
         //echo $daybook_subcontractor_expenseamount;exit;
        
         /** Subcontractor payment ends */
        //  echo($daybook_subcontractor_expenseamount);exit;
        // echo ($daybook_expense_tot_amount);exit;
        /* Purchase Bill Starts*/
        $unreconciled_datasql = "SELECT O.*, E.*  FROM jp_reconciliation O "
             . " JOIN jp_expenses E ON O.reconciliation_parentid = E.exp_id  "
             . " WHERE O.reconciliation_status=0 and (" . $newQuery3 . ") ";
         $unreconciled_data = Yii::app()->db->createCommand($unreconciled_datasql)->queryAll();

         $bill_array = array();

         foreach ($unreconciled_data as  $data) {                
             $recon_bill = "'{$data["bill_id"]}'";                
             array_push($bill_array, $recon_bill);
         }
        
         $bill_array1 = implode(',', $bill_array);

         $pendingsql = 'SELECT bill_id FROM jp_expenses '
         . ' WHERE  '
         . ' reconciliation_status =1 OR  reconciliation_status IS NULL';
         $pending_bills_array = Yii::app()->db->createCommand($pendingsql)->queryAll();

         $pending_bills =array();
         foreach ($pending_bills_array as $data) {                
             $billid = "'{$data["bill_id"]}'";                
             array_push($pending_bills, $billid);
         }
         $pending_bills1 = implode(',', $pending_bills);
         if ($bill_array1) {
             $bill_condition = " 1=1";
             $bill_condition1 = " 1=1";
             if(!empty($pending_bills1)){
                 $bill_condition = " bill_id IN($pending_bills1)";
                 $bill_condition1 = "bill_id NOT IN($pending_bills1)";
             }

             $purchasesql = "SELECT * FROM " . $tblpx . "bills "
             . " LEFT JOIN " . $tblpx . "purchase "
             . " ON " . $tblpx . "bills.purchase_id = " . $tblpx . "purchase.p_id "
             . " WHERE " . $tblpx . "bills.purchase_id IS NOT NULL "
             . " AND (" . $newQuery4 . ") "
             . " AND bill_id NOT IN ($bill_array1) "
             . " AND " . $bill_condition
             . " AND " . $tblpx . "bills.bill_date BETWEEN '" . $date_from . "' AND '" . $date_to . "' ";
                    
             $purchase = Yii::app()->db->createCommand($purchasesql)->queryAll(); //reconcil


             $purchase1sql = "SELECT * FROM " . $tblpx . "bills "
              . " LEFT JOIN " . $tblpx . "purchase "
              . " ON " . $tblpx . "bills.purchase_id = " . $tblpx . "purchase.p_id "
              . " WHERE " . $tblpx . "bills.purchase_id IS NOT NULL "
              . " AND (" . $newQuery4 . ") "
              . " AND " . $bill_condition1
              . " AND " . $tblpx . "bills.bill_date BETWEEN '" . $date_from . "' AND '" . $date_to . "' ";
                    
             $purchase1 = Yii::app()->db->createCommand($purchase1sql)->queryAll(); //un reconcil
         } else {

             $bill_condition = " 1=1";
             $bill_condition1 = " 1=1";
             if(!empty($pending_bills1)){
                 $bill_condition = " bill_id IN($pending_bills1)";
                 $bill_condition1 = "bill_id NOT IN($pending_bills1)";
             }

             $purchasesql = "SELECT * FROM " . $tblpx . "bills "
             . " LEFT JOIN " . $tblpx . "purchase "
             . " ON " . $tblpx . "bills.purchase_id = " . $tblpx . "purchase.p_id "
             . " WHERE " . $tblpx . "bills.purchase_id IS NOT NULL "
             . " AND (" . $newQuery4 . ") "
             . " AND " . $bill_condition
             . " AND " . $tblpx . "bills.bill_date BETWEEN '" . $date_from . "' AND '" . $date_to . "' ";

             $purchase = Yii::app()->db->createCommand($purchasesql)->queryAll();

             $purchase1sql = "SELECT * FROM " . $tblpx . "bills "
              . " LEFT JOIN " . $tblpx . "purchase "
              . " ON " . $tblpx . "bills.purchase_id = " . $tblpx . "purchase.p_id "
              . " WHERE " . $tblpx . "bills.purchase_id IS NOT NULL "
              . " AND (" . $newQuery4 . ") "
              . " AND " . $bill_condition1
              . " AND " . $tblpx . "bills.bill_date BETWEEN '" . $date_from . "' AND '" . $date_to . "' ";
             $purchase1 = Yii::app()->db->createCommand($purchase1sql)->queryAll(); //un reconcil
         }
         $bill_amount = 0;
         $purchase_list = Projects::model()->groupPurchaseBillItems($purchase);
         //$expense_types_listing = Projects::model()->setExpenseTypesListingofPurchaseBill($purchase_list);
         $daybookpurchase = "SELECT SUM(paid) AS amount FROM " . $tblpx . "expenses "
         . " WHERE amount!=0" 
         . " AND exptype IS NOT NULL AND vendor_id IS NOT NULL "
         . " AND bill_id IS NOT NULL AND (" . $newQuery . ") "
         . " AND (reconciliation_status IS NULL OR reconciliation_status=1) "
         . " AND date BETWEEN '" . $date_from . "' AND '" . $date_to . "' "
         . " ORDER BY date";
         $daybook_purchase_list = Yii::app()->db->createCommand($daybookpurchase)->queryRow();
        //  echo '<pre>';print_r($daybook_purchase_list[]);exit;
            //  foreach ($daybook_purchase_list as $purchase) {
            //      $count = count($purchase);
            //      $count = ($count - 2);
            //      $index = 1;
                //  foreach ($purchase as $key => $value) {
                    //   if (is_array($value)) {
                        //  $amount =  $purchase['paid'] ;
                         $bill_amount = $daybook_purchase_list['amount']; 
                    //  }
                //   $index++;
                //  }
            //  }

         /*Purchase Bill Ends*/
         $vendor_details = Yii::app()->db->createCommand($vendor_sql)->queryRow();
         
         $laboursql = "
         SELECT 
             SUM(amount) AS total_amount
         FROM 
             jp_dailyreport
         WHERE 
             approval_status = '1'
             AND approve_status = 1
             AND (subcontractor_id IS NULL OR subcontractor_id = -1)
             AND " . $newQuery . "
             AND date BETWEEN '" . $date_from . "' AND '" . $date_to . "'";
         
         $command = Yii::app()->db->createCommand($laboursql);
         $result = $command->queryRow();

         $total_labour_amount = isset($result['total_amount']) ? $result['total_amount'] : 0;
        
         if($exp_cal==1){
             $total_expense = $daybook_expense_tot_amount+$daybook_subcontractor_expenseamount+$total_labour_amount;
         }else{
             $total_expense = $daybook_expense_tot_amount+$daybook_subcontractor_expenseamount+$bill_amount+$total_labour_amount;
         }
         

    //  die($total_expense);
        return $total_expense;
    }

    public static function formattedcommonDate($date){
        $formattedDate = DATE('d-m-Y',strtotime($date));
        return $formattedDate;
    }
    public static function updateDynamicMaterialReqNo($mrId='',$po_id=''){
        $mr_no='';       $status=0;
        $mr_Mod=MaterialRequisition::model()->findByPk($mrId);
        if(!empty($mr_Mod)){
            if(empty($mr_Mod->requisition_no)){
                $purchase_model =Purchase::model()->findByPk($po_id);
                if(!empty($po_id)){
                    $company_id=$purchase_model->company_id;
                    $tblpx = Yii::app()->db->tablePrefix;
                    $company = Company::model()->findByPk($company_id);
            if(!empty($company->material_requisition_fomat )){
                $material_requisition_fomat =trim($company["material_requisition_fomat"]);
                $general_mr_format_arr = explode('/',$material_requisition_fomat);
                // print_r($general_mr_format_arr);exit;
                $org_prefix = $general_mr_format_arr[0];
                $prefix_key = 0;
                $mr_key= '';
                $current_key ='';
                $status = 1;
                foreach($general_mr_format_arr as $key =>$value){
                    
                    if($value == '{mr_sequence}'){
                        $mr_key = $key;
                    }else if ($value == '{year}'){
                        $current_key =$key;
                    }
                }
                
                $current_year = date('Y');
                $previous_year = date('Y') -1;
                $next_year = date('Y') +1;
                
                $where = '';
                if($current_key == 2){
                    $where .= " AND `requisition_no` LIKE '%$current_year'";
                }else if($current_key == 1){
                    $where .= " AND `requisition_no` LIKE '%$current_year%'";
                }
                $sql = "SELECT * from pms_material_requisition  WHERE `requisition_no` LIKE '$org_prefix%' $where   AND  company_id = ". $company_id. " ORDER BY `requisition_id` DESC LIMIT 1;" ;
                //echo "<pre>";print_r($sql);exit;
                $previous_mr_no='';
                $mr_seq_no = 1;
                $mr_seq_prefix ='MR-' ;
                $previous_data = Yii::app()->db->createCommand($sql)->queryRow();
                if( !empty($previous_data) ){
                    $previous_mr_no = $previous_data['requisition_no'];
                    $prefix_arr = explode('/', $previous_mr_no);
                    $prefix = $prefix_arr[0];
                    $previous_prefix_seq_no = $prefix_arr[$mr_key];
                    $mr_split = explode('-',$previous_prefix_seq_no);
                    $previous_prefix_seq_no_val =$mr_split[1];
                    $mr_seq_no =  $previous_prefix_seq_no_val + 1;
                                   
                }
                $digit = strlen((string) $mr_seq_no);
               
                if ($digit == 1) {
                    $mr_no =$mr_seq_prefix.'00' .$mr_seq_no;
                } else if ($digit == 2) {
                    $mr_no = $mr_seq_prefix.'0' . $mr_seq_no;
                } else {
                    $mr_no =$mr_seq_prefix.$mr_seq_no;
                }
                    $status=1;
              
               
                $new_invoice = str_replace('{year}',$current_year,$material_requisition_fomat);
                $new_mr_no = str_replace('{mr_sequence}',$mr_no,$new_invoice);
                //die($new_mr_no);
                $mr_no = $new_mr_no;
                $materialRequisition = MaterialRequisition::model()->findByPk($mrId);
                $materialRequisition->requisition_no=$mr_no;
                $materialRequisition->update();
                
            }else{
            $mr_count = count(MaterialRequisition::model()->findAll()) + 1;
            $unqid = str_pad($mr_count, 6, '0', STR_PAD_LEFT);
            $requisition_no = 'MR' . $unqid;
            $materialRequisition = MaterialRequisition::model()->findByPk($mrId);
            $materialRequisition->requisition_no=$requisition_no;
                $materialRequisition->update();
                }

                }
            }   
        }
    }
    public function getSubcontractorPaidBillAmount($data,$subcontractor_id){

        $bill_list = array();
        $bills = "";
        $tblpx  = Yii::app()->db->tablePrefix;
        $where = "";
        if($data==1){
            $where = " AND bill.date = DATE_SUB(CURDATE(), INTERVAL 0 DAY)";
        } else if($data==2){
            $where = " AND bill.date < DATE_SUB(CURDATE(), INTERVAL 1 DAY) AND bill.date > DATE_SUB(CURDATE(), INTERVAL 15 DAY) ";
        }else if($data==3){
            $where = " AND bill.date < DATE_SUB(CURDATE(), INTERVAL 16 DAY) AND bill.date > DATE_SUB(CURDATE(), INTERVAL 30 DAY) ";
        } else if($data==4){
            $where = " AND bill.date < DATE_SUB(CURDATE(), INTERVAL 31 DAY) AND bill.date > DATE_SUB(CURDATE(), INTERVAL 45 DAY) ";
        }else if($data==5){
            $where = " AND bill.date < DATE_SUB(CURDATE(), INTERVAL 45 DAY)";
        }

       
        $sql = "SELECT bill.id as bill_id"
            . " FROM {$tblpx}subcontractorbill as bill "
            . " left join {$tblpx}scquotation as pu "
            . " on pu.scquotation_id = bill.scquotation_id "
            . " WHERE pu.subcontractor_id= ".$subcontractor_id
            . " $where"; 
        $data = Yii::app()->db->createCommand($sql)->queryAll();
               
        if (!empty($data)) {                                    
            foreach ($data as $entry) {
                if ($entry['bill_id'] != "") {
                    $bill_list[] = $entry['bill_id'];
                }                    
            }
            $bills = implode(',', $bill_list);
        }

        $condition = "1=1";
        $expense_amount=0;
        if (!empty($bills)) {
            $condition = "sc_bill_id IN (" . $bills . ")";
            $expense_amount = Yii::app()->db->createCommand("SELECT SUM(`paidamount`) FROM `jp_subcontractor_payment` WHERE $condition")->queryScalar();
        }           
        return $expense_amount;        
    }
}
