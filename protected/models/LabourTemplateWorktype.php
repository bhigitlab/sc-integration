<?php

/**
 * This is the model class for table "{{labour_template_worktype}}".
 *
 * The followings are the available columns in table '{{labour_template_worktype}}':
 * @property integer $id
 * @property integer $worktype_id
 * @property double $worktype_rate
 * @property integer $template_id
 * @property string $worktype_name
 * @property string $created_at
 * @property string $updated_at
 *
 * The followings are the available model relations:
 * @property LabourTemplate $template
 * @property LabourWorktype $worktype
 */
class LabourTemplateWorktype extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{labour_template_worktype}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('worktype_id, worktype_rate, template_id, worktype_name, created_at, updated_at', 'required'),
            array('worktype_id, template_id', 'numerical', 'integerOnly'=>true),
            array('worktype_rate', 'numerical'),
            array('worktype_name', 'length', 'max'=>250),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, worktype_id, worktype_rate, template_id, worktype_name, created_at, updated_at', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'template' => array(self::BELONGS_TO, 'LabourTemplate', 'template_id'),
            'worktype' => array(self::BELONGS_TO, 'LabourWorktype', 'worktype_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'worktype_id' => 'Worktype',
            'worktype_rate' => 'Worktype Rate',
            'template_id' => 'Template',
            'worktype_name' => 'Worktype Name',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('worktype_id',$this->worktype_id);
        $criteria->compare('worktype_rate',$this->worktype_rate);
        $criteria->compare('template_id',$this->template_id);
        $criteria->compare('worktype_name',$this->worktype_name,true);
        $criteria->compare('created_at',$this->created_at,true);
        $criteria->compare('updated_at',$this->updated_at,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination' => false
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return LabourTemplateWorktype the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}