<?php
/* @var $this BankController */
/* @var $model Bank */
/* @var $form CActiveForm */
?>

<div class="">
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id'=>'deposit-form',
        'enableAjaxValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true,
            'validateOnType' => false,)
    ));
    ?>

    <!-- Popup content-->



    <div class="panel-body">

            <div class="row ">

                <div class="col-md-4">
                    <?php echo $form->labelEx($model,'deposit_name'); ?>
                    <?php echo $form->textField($model,'deposit_name',array('size'=>60,'maxlength'=>100,'class'=>'form-control')); ?>
                    <?php echo $form->error($model,'deposit_name'); ?>
                </div>

            <div class="col-md-4 save-btnHold">
                <label style="display:block;">&nbsp;</label>
                <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'btn btn-info btn-sm')); ?> 
                <?php if (Yii::app()->user->role == 1) {
                	if(!$model->isNewRecord){ /*
                 ?> 
                    <a class="btn del-btn" href="<?php echo $this->createUrl('projects/deleteprojects', array('id' => $model->type_id)) ?>" class="deletebtn" onclick="return confirm('Are you sure that you want to delete?');">Delete </a>                           
                <?php */ }} ?>
<!--                <button data-dismiss="modal" onclick="javascript:window.location.reload()">Close</button>-->
            <?php if(!$model->isNewRecord){
                  
                  echo CHtml::ResetButton('Close', array('onclick'=>'closeaction(this,event);','class'=>'btnbtn-sm'));  
                    } 
                    else{
                       echo CHtml::ResetButton('Reset', array('style'=>'margin-right: 3px;','class'=>'btn btn-default btn-sm')); 
                       echo CHtml::ResetButton('Close', array('onclick'=>'closeaction(this,event);','class'=>'btn btn-sm'));  
                    }
                ?>
            </div>
        </div>
            
    </div>

<?php $this->endWidget(); ?>
</div><!-- form -->

    
    <script>
        $(document).ready(function () {

            //select all checkboxes
            $("#select_all").change(function () {  //"select all" change
                var status = this.checked; // "select all" checked status
                $('.checkbox').each(function () { //iterate all listed checkbox items
                    this.checked = status; //change ".checkbox" checked status
                });
            });

            $('.checkbox').change(function () { //".checkbox" change
                //uncheck "select all", if one of the listed checkbox item is unchecked
                if (this.checked == false) { //if this item is unchecked
                    $("#select_all")[0].checked = false; //change "select all" checked status to false
                }

                //check "select all" if all checkbox items are checked
                if ($('.checkbox:checked').length == $('.checkbox').length) {
                    $("#select_all")[0].checked = true; //change "select all" checked status to true
                }
            });


        });


    </script>
