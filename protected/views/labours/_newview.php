<?php
/* @var $this ProjectsController */
/* @var $data Projects */
?>
<style>
    .legend {
        list-style: none;
        padding-left: 0px;

    }

    .legend li {
        float: left;
        margin-right: 10px;
    }

    .legend span {
        border: 1px solid #ccc;
        float: left;
        width: 12px;
        height: 12px;
        margin: 2px;
    }

    .legend .project_not_mapped {
        background-color: #A9E9EC;
        border: 2px solid #f8cbcb;
        margin-right: 4px;
    }
</style>
<?php
if ($index == 0) {
    ?>
   
    <?php 
$pms_api_integration=ApiSettings::model()->pmsIntegrationStatus();
if($pms_api_integration==1){
?>
      <ul class="legend">
            <li><span class="project_not_mapped"></span>Labour Not Mapped In Integration</li>    
    </ul>
    <?php } ?>
    <thead class="entry-table">
    <tr> 
    <?php 
        if(isset(Yii::app()->user->role) && (in_array('/labours/update', Yii::app()->user->menuauthlist))){
        ?>
        <th>Action</th>
        <?php } ?>
        <th>SI No</th>
        <th>Work Type</th>
        <th>Rate</th>
         <th>Short Keyword</th>
        
    </tr>   
    </thead>
<?php } ?>
<?php
 if($data->pms_labour_id !=='0'){
        $styleval= '';
    }else{
        $styleval='background-color:#A9E9EC;';
    }             
?>
<tr style="<?php echo $styleval; ?>">
    <td style="width: 50px;">
        <?php 
            if(isset(Yii::app()->user->role) && (in_array('/labours/update', Yii::app()->user->menuauthlist))){
            ?>        
           <a class="fa fa-edit editProject" data-toggle="modal" data-target=".edit" data-id="<?php echo $data->type_id; ?>"></a>
         <?php } ?>
         <?php
            $pms_api_integration=ApiSettings::model()->pmsIntegrationStatus();

            if ($pms_api_integration != 1) {
                if (isset(Yii::app()->user->role) && in_array('/labours/delete', Yii::app()->user->menuauthlist)) {
                    ?>
                    <a class="fa fa-trash deleteProject" data-toggle="modal" data-target=".delete" data-id="<?php echo $data->type_id; ?>"></a>
                    <?php
                }
            }
            ?>

        </td> 
        <td style="width: 50px;"><?php echo $index+1; ?></td>
        <td>
            <span><?php echo $data->worktype ?></span>            
        </td>        
        <td>
        <span><?php echo $data->rate ?></span>   
        </td>
        <td>
        <span><?php echo $data->short_keyword ?></span>   
        </td>
               
    </tr>  
