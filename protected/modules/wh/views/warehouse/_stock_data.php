<div class="col-xs-12">
    <table class="table">
        <thead class="entry-table">
            <tr>
                <th>Item Name</th>
                <th>Rate</th>
                <th>Actual Quantity</th>
                <th>Correction</th>
                <th>Revised Quantity</th>
                <th>Remarks</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($stockData as $key => $value) {

                $used_qty_in_pms = Yii::app()->db->createCommand('SELECT SUM(item_qty) as item_qty_sum FROM `pms_acc_wpr_item_consumed` WHERE `warehouse_id`=' . $value['warehousestock_warehouseid'] . ' AND `item_id`=' . $value['warehousestock_itemid'] . '  AND`item_rate` =' . $value['rate'])->queryRow();
                $used_qty = !empty($used_qty_in_pms) ? $used_qty_in_pms['item_qty_sum'] : 0;
                ?>
                <tr>
                    <td class="purchase_item_id" data-item="<?php echo $value['warehousestock_itemid'] ?>">
                        <?php echo Controller::itemName($value['warehousestock_itemid']) ?>
                    </td>
                    <td class="rate">
                        <?php echo $value['rate'] ?>
                    </td>
                    <td class="actual_quantity">
                        <?php echo ($value['warehousestock_stock_quantity'] - $used_qty) ?>
                    </td>
                    <?php
                    $sql = "SELECT stock_quantity FROM `jp_warehouse_stock_correction` "
                        . " WHERE warehouse_id=" . $value['warehousestock_warehouseid']
                        . " AND Item_Id=" . $value['warehousestock_itemid']
                        . " AND rate=" . $value['rate'] . " AND approval_status='1' "
                        . " ORDER BY `id` DESC LIMIT 1";
                    $stock_quantity = Yii::app()->db->createCommand($sql)->queryScalar();
                    ?>
                    <td><input type="number" class="form-control stock_quantity"
                            value="<?php echo ($stock_quantity != "") ? $stock_quantity : ($value['warehousestock_stock_quantity'] - $used_qty) ?>">
                    </td>
                    <td>
                        <?php echo $stock_quantity ?>
                    </td>
                    <?php
                    echo CHtml::hiddenField('warehousestock_stock_quantity', $value['warehousestock_stock_quantity'], array('class' => 'receipt_quantity'));
                    echo CHtml::hiddenField('warehousestock_warehouseid', $value['warehousestock_warehouseid'], array('class' => 'wh_id'));
                    ?>
                    <td>
                        <textarea rows="1" class="form-control remarks"></textarea>
                    </td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
<div class="col-xs-12 text-right">
    <?php echo CHtml::button('Submit', array('class' => 'btn_submit btn btn-info')) ?>
</div>

<script>
    $(".btn_submit").click(function () {
        var sec = [];
        $('.stock_item table tbody tr').each(function () {
            var warehouse_id = $(this).find(".wh_id").val();
            var Item_Id = $(this).find(".purchase_item_id").attr("data-item");
            var stock_quantity = $(this).find(".stock_quantity").val();
            var wh_receipt_quantity = $(this).find(".receipt_quantity").val();
            var wh_rate = $(this).find(".rate").text();
            var remarks = $(this).find(".remarks").val();

            if (stock_quantity != wh_receipt_quantity) {
                sec.push({
                    warehouse_id: warehouse_id,
                    Item_Id: Item_Id,
                    stock_quantity: stock_quantity,
                    wh_receipt_quantity: wh_receipt_quantity,
                    wh_rate: wh_rate,
                    remarks: remarks

                });
            }
        })
        var data = JSON.stringify(sec);
        $.ajax({
            type: "POST",
            dataType: "json",
            contentType: 'application/json',
            data: { "data": sec },
            url: "<?php echo $this->createUrl('warehouse/addStockCorrection&data=') ?>" + data,
            success: function (reponse) {

                alert(reponse.msg)
                location.reload();
            }
        })
        $(this).attr('disabled', true);
    })
</script>