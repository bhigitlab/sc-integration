<?php
/* @var $this LogController */
/* @var $model JpLog */

$this->breadcrumbs=array(
	'Jp Logs'=>array('index'),
	$model->log_id,
);

$this->menu=array(
	array('label'=>'List JpLog', 'url'=>array('index')),
	array('label'=>'Create JpLog', 'url'=>array('create')),
	array('label'=>'Update JpLog', 'url'=>array('update', 'id'=>$model->log_id)),
	array('label'=>'Delete JpLog', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->log_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage JpLog', 'url'=>array('admin')),
);
?>

<h1>View JpLog #<?php echo $model->log_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'log_id',
		'log_data',
		'log_table',
		'log_primary_key',
		'log_datetime',
		'log_action',
		'log_action_by',
	),
)); ?>
