<?php
$condition1 = "";
if (isset($_REQUEST['Specification']) && $_REQUEST['Specification']['brand_id'] != "") {
    $condition1 .= $condition1 != "" ? ' AND ' : ' WHERE ';
    $condition1 .= ' brand_id=' . $_REQUEST['Specification']['brand_id'];
}
if (isset($_REQUEST['Specification']) && $_REQUEST['Specification']['specification'] != "") {
    $condition1 .= $condition1 != "" ? ' AND ' : ' WHERE ';
    $condition1 .= " specification LIKE '" . $_REQUEST['Specification']['specification'] . "'";
}
if (isset($_REQUEST['Specification']) && $_REQUEST['Specification']['specification_type'] != "") {
    $condition1 .= $condition1 != "" ? ' AND ' : ' WHERE ';
    $condition1 .= ' specification_type="' . $_REQUEST['Specification']['specification_type'] . '"';
}
if (isset($_REQUEST['Specification']) && $_REQUEST['Specification']['id'] != "") {
    $condition1 .= $condition1 != "" ? ' AND ' : ' WHERE ';
    $condition1 .= ' id=' . $_REQUEST['Specification']['id'];
}

$sql = 'SELECT DISTINCT  cat_id FROM `jp_specification` ' . $condition1;
$spec_catId = Yii::app()->db->createCommand($sql)->queryAll();
$catIds = implode(',', array_column($spec_catId, 'cat_id'));

$catCondition = ($catIds != "") ? 'id IN (' . $catIds . ')' : "";

$subsql = 'SELECT DISTINCT sub_cat_id FROM `jp_specification`' . $condition1;
$spec_subcatId = Yii::app()->db->createCommand($subsql)->queryAll();
$subcatIds = implode(',', array_column($spec_subcatId, 'sub_cat_id'));
if (!is_array($subcatIds)) {
    $subcatIds = explode(',', $subcatIds);
}
$validIds = array_filter($subcatIds, 'is_numeric');
$subcatCondition = (!empty($validIds)) ? 'id IN (' . implode(',', $validIds) . ')' : null;

$condition2 = "";
if (isset($_REQUEST['Specification']) && $_REQUEST['Specification']['cat_id'] != "") {
    $condition2 .= 'AND cat_id=' . $_REQUEST['Specification']['cat_id'];
}
if (isset($_REQUEST['Specification']) && $_REQUEST['Specification']['specification'] != "") {
    $condition2 .= " AND specification LIKE '" . $_REQUEST['Specification']['specification'] . "'";
}
if (isset($_REQUEST['Specification']) && $_REQUEST['Specification']['specification_type'] != "") {
    $condition2 .= ' AND specification_type="' . $_REQUEST['Specification']['specification_type'] . '"';
}
if (isset($_REQUEST['Specification']) && $_REQUEST['Specification']['id'] != "") {
    $condition2 .= ' AND id=' . $_REQUEST['Specification']['id'];
}

$sql = 'SELECT DISTINCT  brand_id FROM `jp_specification` WHERE brand_id IS NOT NULL ' . $condition2;
$spec_brandId = Yii::app()->db->createCommand($sql)->queryAll();
$brandIds = implode(',', array_column($spec_brandId, 'brand_id'));
$brandCondition = ($brandIds != "") ? 'id IN (' . $brandIds . ')' : "";


$condition3 = "";
if (isset($_REQUEST['Specification']) && $_REQUEST['Specification']['cat_id'] != "") {
    $condition3 .= $condition3 != "" ? ' AND ' : '';
    $condition3 .= ' cat_id=' . $_REQUEST['Specification']['cat_id'];
}
if (isset($_REQUEST['Specification']) && $_REQUEST['Specification']['brand_id'] != "") {
    $condition3 .= $condition3 != "" ? ' AND ' : '';
    $condition3 .= '  brand_id=' . $_REQUEST['Specification']['brand_id'];
}
if (isset($_REQUEST['Specification']) && $_REQUEST['Specification']['specification_type'] != "") {
    $condition3 .= $condition3 != "" ? ' AND ' : '';
    $condition3 .= ' specification_type="' . $_REQUEST['Specification']['specification_type'] . '"';
}
if (isset($_REQUEST['Specification']) && $_REQUEST['Specification']['id'] != "") {
    $condition3 .= $condition3 != "" ? ' AND ' : '';
    $condition3 .= ' id=' . $_REQUEST['Specification']['id'];
}
?>
<div class="table-responsive">
    <?php
    $this->widget('zii.widgets.grid.CGridView', array(
        'id' => 'purchase-category-grid',
        'itemsCssClass' => 'table spectable',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'columns' => array(
            array('class' => 'IndexColumn', 'header' => 'Sl.No.', ),
            array(
                'name' => 'id',
                'type' => 'raw',
                'value' => '$data->id',
                'header' => '#ID  <i class="fa fa-fw fa-sort"></i>',
                'htmlOptions' => array('class' => 'snocol'),
                // 'filterHtmlOptions' => array('style' => 'width:70px'),
            ),
            array(
                'value' => function ($data, $row) {

                    if ($data->filename != '') {
                        $path = realpath(Yii::app()->basePath . '/../uploads/purchase_category/' . $data->filename);
                        if (file_exists($path)) {
                            echo CHtml::tag(
                                'img',
                                array(
                                    'src' => Yii::app()->request->baseUrl . "/uploads/purchase_category/thumbnail/" . $data->filename,
                                    'alt' => '',
                                    'class' => 'pop',
                                    'modal-src' => Yii::app()->request->baseUrl . "/uploads/purchase_category/" . $data->filename
                                )
                            );
                        } else {
                            echo CHtml::tag(
                                'img',
                                array(
                                    'src' => Yii::app()->request->baseUrl . "/uploads/purchase_category/thumbnail/no_img.png",
                                    'alt' => 'no_img',
                                    'class' => 'pop',
                                    'modal-src' => Yii::app()->request->baseUrl . "/uploads/purchase_category/no_img.png"
                                )
                            );
                        }
                    } else {
                        echo CHtml::tag(
                            'img',
                            array(
                                'src' => Yii::app()->request->baseUrl . "/uploads/purchase_category/thumbnail/no_img.png",
                                'alt' => 'no_img',
                                'class' => 'pop',
                                'modal-src' => Yii::app()->request->baseUrl . "/uploads/purchase_category/no_img.png"
                            )
                        );
                    }
                },
                'header' => 'Image',
                'htmlOptions' => array('class' => 'imd_div2')
            ),
            array(
                'name' => 'cat_id',
                'type' => 'raw',
                'value' => '$data->cat["category_name"]',
                'filter' =>
                    CHtml::activeDropDownList(
                        $model,
                        'cat_id',
                        CHtml::listData(PurchaseCategory::model()->findAll(
                            array(
                                'select' => array('id,category_name'),
                                'condition' => $catCondition,
                                'order' => 'category_name',
                                'distinct' => true
                            )
                        ), "id", "category_name"),
                        array('empty' => 'Select Category', 'class' => 'form-control select_js')
                    ),
                'header' => 'Category  <i class="fa fa-fw fa-sort"></i>',
                'htmlOptions' => array('class' => 'snocol')
            ),
            array(
                'name' => 'sub_cat_id',
                'type' => 'raw',
                'value' => '$data->sub["sub_category_name"]',
                'filter' =>
                    CHtml::activeDropDownList(
                        $model,
                        'sub_cat_id',
                        CHtml::listData(PurchaseSubCategory::model()->findAll(
                            array(
                                'select' => array('id,sub_category_name'),
                                'condition' => $subcatCondition,
                                'order' => 'sub_category_name',
                                'distinct' => true
                            )
                        ), "id", "sub_category_name"),
                        array('empty' => 'Select Sub Category', 'class' => 'form-control select_js')
                    ),
                'header' => 'Sub Category  <i class="fa fa-fw fa-sort"></i>',
                'htmlOptions' => array('class' => 'snocol')
            ),
            array(
                'name' => 'brand_id',
                'type' => 'raw',
                'value' => function ($data, $row) {
                    $brand = Brand::model()->findByPk($data->brand_id);
                    return ($data->brand_id == '') ? '' : $brand['brand_name'];
                },
                'filter' => CHtml::activeDropDownList(
                    $model,
                    'brand_id',
                    CHtml::listData(Brand::model()->findAll(
                        array(
                            'select' => array('id,brand_name'),
                            'condition' => $brandCondition,
                            'order' => 'brand_name',
                            'distinct' => true
                        )
                    ), "id", "brand_name"),
                    array('empty' => 'Select Brand', 'class' => 'form-control select_js')
                ),
                'header' => 'Brand  <i class="fa fa-fw fa-sort"></i>',
            ),
            array(
                'value' => function ($data, $row) {
                    if ($data["specification_type"] == "A") {
                        echo "By Length";
                    } else if ($data["specification_type"] == "G") {
                        echo "By Width * Height";
                    } else {
                        echo "By Quantity";
                    }
                },
                'filter' => CHtml::activeDropDownList(
                    $model,
                    'specification_type',
                    array(
                        "O" => "By Quantity",
                        "A" => "By Length",
                        "G" => "By Width * Height"
                    ),
                    array('empty' => 'Select Value')
                ),

                'header' => 'Type',
                'htmlOptions' => array('class' => 'snocol')
            ),
            array(
                'name' => 'specification',
                'type' => 'raw',
                'value' => '$data->specification',
                'filter' => CHtml::activeDropDownList(
                    $model,
                    'specification',
                    CHtml::listData(Specification::model()->findAll(
                        array(
                            'select' => array('specification,specification'),
                            'condition' => $condition3,
                            'order' => 'specification',
                            'distinct' => true
                        )
                    ), "specification", "specification"),
                    array('empty' => 'Select Specification', 'class' => 'form-control select_js')
                ),
                'header' => 'Specification <i class="fa fa-fw fa-sort"></i>',
                'htmlOptions' => array('class' => 'snocol')
            ),
            array(
                'name' => 'hsn_code',
                'value' => '$data->hsn_code',
                'header' => 'HSN Code  <i class="fa fa-fw fa-sort"></i>',
                'htmlOptions' => array('class' => 'snocol')
            ),
            array(
                'value' => function ($data, $row) {
                    $companyname = array();
                    $arrVal = explode(',', $data->company_id);
                    foreach ($arrVal as $arr) {
                        $value = Company::model()->findByPk($arr);
                        if ($value !== NULL) {
                            array_push($companyname, $value['name']);
                        }
                    }
                    echo rtrim(trim(implode(', ', $companyname)), ",");
                },
                'header' => 'Company',
                'htmlOptions' => array('class' => 'snocol')
            ),
            array(
                'value' => function ($data, $row) {
                    echo date('d-m-Y', strtotime($data->created_date));
                },
                'header' => 'Created Date',
                'htmlOptions' => array('class' => 'snocol')
            ),
            array(
                'type' => 'raw',
                'visible' => isset(Yii::app()->user->role) && (in_array('/purchaseCategory/manageitem', Yii::app()->user->menuauthlist)),
                'value' => function ($data) {
                    return '<span class="edit_purchase edit_option_' . $data->id . '" id="' . $data->id . '" data-toggle="tooltip" title="Edit"><span class="fa fa-edit editSpecification" data-toggle="modal" data-target=".edit" data-id="' . $data->id . '"></span></span>
                            <span class="delete_purchase delete_option_' . $data->id . '" id="' . $data->id . '" data-toggle="tooltip" title="Delete"><span class="fa fa-trash deleteSpecification" data-toggle="modal" data-target=".delete" data-id="' . $data->id . '"></span></span>';
                },
                'filter' => CHtml::link('<i class="fa fa-refresh"></i>', $this->createUrl('purchaseCategory/newList'), array('class' => 'refresh_filter')),
                'header' => '',
                'htmlOptions' => array('class' => 'nowrap')
            ),
        ),

        'pager' => array(
            'class' => 'CustomPagerGrid',
            'header' => '',
        ),
        'pagerCssClass' => 'pager',
    ));
    ?>
</div>