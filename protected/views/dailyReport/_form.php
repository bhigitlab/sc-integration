<?php
/* @var $this DailyReportController */
/* @var $model Dailyreport */
/* @var $form CActiveForm */

        
//print_r($completion_date);
 ?>
     <?php if(Yii::app()->user->hasFlash('success')):?><br>
    <div class="example1consolesucces">
        <?php echo Yii::app()->user->getFlash('success'); ?>
    </div>
<?php endif; ?>

<div class="form ">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'dailyreport-form',
	'enableAjaxValidation'=>false,
)); ?>
 

    <div class="clearfix">
            <div class="row addRow">					
                <div class="col-md-4">
		<?php echo $form->labelEx($model,'projectid'); ?>
                <?php
                if(Yii::app()->user->role == 1){
                    echo $form->dropDownList($model, 'projectid', CHtml::listData(Projects::model()->findAll(array(
                                                'select' => array('pid, name'),
                                                //"condition" => 'pid in (select projectid from project_assign where userid=' . Yii::app()->user->id . ')',
                                                'order' => 'name',
                                                'distinct' => true
                                            )), 'pid', 'name'), array('class'=>'form-control','style'=>'width:200px;','empty' => '-----------'));
                                                
                }else{
                    echo $form->dropDownList($model, 'projectid', CHtml::listData(Projects::model()->findAll(array(
                                                'select' => array('pid, name'),
                                                "condition" => 'pid in (select projectid from jp_project_assign where userid=' . Yii::app()->user->id . ')',
                                                'order' => 'name',
                                                'distinct' => true
                                            )), 'pid', 'name'), array('class'=>'form-control','style'=>'width:200px;','empty' => '-----------'));
                                                
                }
                ?>
		
		<?php echo $form->error($model,'projectid'); ?>
                </div>
						
                <div class="col-md-4">
                    <label>Completion date:</label><input type="text" id="comple_date" class="form-control" style="width:200px;" readonly="true" value = "<?php echo ((!$model->isNewRecord) ? date('d-M-Y', strtotime($completion_date)) : '') ?>" />
                </div>
           
                <div class="col-md-4">
		 <?php echo $form->labelEx($model, 'book_date'); ?>
                    <?php echo CHtml::activeTextField($model, 'book_date', array('readonly' => 'true',"value" => (($model->isNewRecord) ? date('d-M-Y') : date('d-M-Y', strtotime($model->book_date))), 'size' => 10,'class'=>'form-control','style'=>'width:200px;')); ?>
                    <?php
                    $this->widget('application.extensions.calendar.SCalendar', array(
                        'inputField' => 'Dailyreport_book_date',
                        'ifFormat' => '%d-%b-%Y',
                    ));
                    ?>
                <?php echo $form->error($model,'book_date'); ?>
                </div>
            </div>
            <div class="row addRow">
                <div class="col-md-4 textArea-box">
                    <?php echo $form->labelEx($model,'works_done'); ?>
                    <?php echo $form->textArea($model,'works_done',array('class'=>'form-control','style'=>'width:300px;','rows' => 3, 'cols' => 50)); ?>
                    <?php echo $form->error($model,'works_done'); ?>
                </div>
                 <div class="col-md-4 textArea-box">
                    <?php echo $form->labelEx($model,'materials_unlade'); ?>
                    <?php echo $form->textArea($model,'materials_unlade',array('class'=>'form-control','style'=>'width:300px;','rows' => 3, 'cols' => 50)); ?>
                    <?php echo $form->error($model,'materials_unlade'); ?>
                </div>
                 <div class="col-md-4 textArea-box">
                    <?php echo $form->labelEx($model,'book_description'); ?>
                    <?php echo $form->textArea($model,'book_description',array('class'=>'form-control','style'=>'width:300px;','rows' => 3, 'cols' => 50)); ?>
                    <?php echo $form->error($model,'book_description'); ?>
                 </div>
            </div>

            <div class="row addRow">
                <div class="col-md-3">
                    <?php echo CHtml::submitButton($model->isNewRecord ? 'Submit' : 'Submit',array('class' => 'btn pull-left')); ?>
                </div>
                <div class="col-md-8"></div>
            </div>
        </div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<?php Yii::app()->clientScript->registerCss('myscript','
    .row{
        padding-left:25px;
    }
    .form-control{
        padding:5px;
        width:150px;
   }
   row .buttons{
       paddding-left:100px;
   }
   .addRow {
  padding: 0px;
   }
   .dailyreport_form{
       width:300px;
       margin :0px auto;
   }
   .flash-success{
   color:#fff;
   padding:5px;
   padding: 5px;
  height: 35px;
  width: 330px;
  border: solid #32CD32;
  background-color: #32CD32;
   }
 @media (max-width:767px){
    .dailyreport_form{
       width:300px;
       margin :0px auto;
   }
}  

@media (min-width:768px){
    .dailyreport_form{
       width:500px;
       margin :0px auto;
   }
}
.example1consolesucces {
color: white;
background: #247B17;
padding: 5px;
width:200px;
}
  
   ');
?>
<?php $url = Yii::app()->createAbsoluteUrl("dailyReport/completiondate");?>
<?php Yii::app()->clientScript->registerScript('myscript','
    $(document).ready(function(){
    $("#Dailyreport_projectid").change(function(){
    var pro_id= $("#Dailyreport_projectid").val();
    $.ajax({
       url: "'.$url.'",
        data: {"id": pro_id}, 
        //dataType: "json",
        type: "POST",
        success:function(data){
        //alert(data);
            document.getElementById("comple_date").value=data; 
            
        }
        
});

});
$(".flash-success").animate({opacity: 1.0}, 5000).fadeOut("slow");
  
    });
        ');?>