ALTER TABLE pms_acc_wpr_item_consumed ADD pms_project_id INT(11) NULL AFTER wpr_id, ADD coms_project_id INT(11) NULL AFTER pms_project_id;
ALTER TABLE pms_acc_wpr_item_consumed ADD pms_material_id INT(11) NULL AFTER coms_project_id, ADD pms_template_material_id INT(11) NULL AFTER pms_material_id;
ALTER TABLE pms_acc_wpr_item_consumed ADD coms_material_id INT(11) NULL AFTER coms_project_id;
ALTER TABLE pms_acc_wpr_item_consumed ADD type_from INT(11) NOT NULL DEFAULT '1' COMMENT '1=>accounts,2=>pms' AFTER pms_template_material_id;
ALTER TABLE pms_acc_wpr_item_consumed CHANGE item_id item_id INT(11) NULL;
ALTER TABLE pms_acc_wpr_item_consumed CHANGE warehouse_id warehouse_id INT(11) NULL;
ALTER TABLE pms_acc_wpr_item_consumed CHANGE item_rate item_rate FLOAT NULL;