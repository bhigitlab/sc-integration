<div class="purchase_items panel panel-default">
    <div class="panel-heading">
        <h3>Payment Entries</h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-6">
                <div class="form add-paymenentry">
                    <?php $form = $this->beginWidget('CActiveForm', array(
                        'id' => 'scquotation-payment-entries-_form-form',
                        'enableAjaxValidation' => false,
                    ));
                    ?>
                    <input type="hidden" name="ScquotationPaymentEntries[scquotation_id]" id="ScquotationPaymentEntries_scquotation_id" value="<?php echo $scquotation_model->scquotation_id; ?>">
                    <input type="hidden" name="ScquotationPaymentEntries[id]" id="ScquotationPaymentEntries_id" value="<?php echo $model->id; ?>">
                    <div class="row">
                        <div class="col-md-4">
                            <label>Payment:</label>
                            <?php echo $form->textField($model, 'payment_title', array('class' => '  form-control')); ?>

                        </div>
                        <div class="col-md-4">
                            <label>Amount:</label>
                            <?php echo $form->textField($model, 'amount', array('class' => 'allownumericdecimal  form-control')); ?>
                        </div>
                        <div class="col-md-4">
                            <?php echo CHtml::Button('Add', array('class' => 'btn btn-info add_payment')); ?>
                        </div>
                    </div>
                    <?php $this->endWidget(); ?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="payment_list">
                    <?php
                    foreach ($payment_entries as $entry) {
                    ?>
                        <div class="payment_entries">
                            <table>
                                <tbody>
                                    <td><span id="title_<?php echo $entry->id; ?>"><?php echo $entry->payment_title; ?></span></td>
                                    <td><span id="amount_<?php echo $entry->id; ?>"><?php echo $entry->amount; ?> </span></td>
                                    <td class="text-right">
                                        <span><a id="<?php echo $entry->id; ?>" data-attr-qtn="<?php echo $entry->scquotation_id; ?>" class="edit_payment_entry"><i class="fa fa-edit"></i></a></span>
                                        <span><a id="<?php echo $entry->id; ?>" class="delete_payment_entry"><i class="fa fa-trash"></i></a></span>
                                    </td>
                                </tbody>
                            </table>

                        </div>
                    <?php
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('.add_payment').click(function() {
            $('.loading-overlay').addClass('is-active');
            var form_data = $('form#scquotation-payment-entries-_form-form').serialize();
            $.ajax({
                url: '<?php echo Yii::app()->createUrl('subcontractor/addpayments'); ?>',
                method: 'POST',
                data: form_data,
                dataType: "json",
                success: function(response) {
                    if (response.status == '1') {
                        $('#scquotation-payment-entries-_form-form')[0].reset();
                        $().toastmessage('showSuccessToast', "" + response.message + "");
                        $('.payment_list').html(response.list);
                    } else if (response.status == '0') {
                        $().toastmessage('showErrorToast', "An error occured");
                    }
                }

            })
        });
        $(document).on('click', '.edit_payment_entry', function(e) {
            var row_id = this.id;
            var title = $('#title_' + row_id).text();
            var amount = parseInt($('#amount_' + row_id).text());
            var qtn_id = $(this).attr("data-attr-qtn");
            $('#ScquotationPaymentEntries_scquotation_id').val(qtn_id);
            $('#ScquotationPaymentEntries_id').val(row_id);
            $('#ScquotationPaymentEntries_payment_title').val(title);
            $('#ScquotationPaymentEntries_amount').val(amount);
        })
        $(document).on('click', '.delete_payment_entry', function(e) {
            var row_id = this.id;
            var answer = confirm("Are you sure you want to delete?");
            if (answer) {
                $('.loading-overlay').addClass('is-active');
                $.ajax({
                    method: "GET",
                    async: false,
                    data: {
                        id: row_id
                    },
                    dataType: "json",
                    url: '<?php echo Yii::app()->createUrl('subcontractor/removepaymententry'); ?>',
                    success: function(response) {
                        if (response.status == '1') {
                            $().toastmessage('showSuccessToast', "" + response.message + "");
                            $('.payment_list').html(response.list);
                        } else if (response.status == '0') {
                            $().toastmessage('showErrorToast', "An error occured");
                        }
                    }
                });
            } else {
                return false;
            }
        });
    });
</script>