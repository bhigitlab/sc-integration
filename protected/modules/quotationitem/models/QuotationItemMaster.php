<?php

/**
 * This is the model class for table "{{quotation_item_master}}".
 *
 * The followings are the available columns in table '{{quotation_item_master}}':
 * @property integer $id
 * @property integer $quotation_category_id
 * @property integer $work_type_id
 * @property string $worktype_label
 * @property string $work_type_description
 * @property string $image
 * @property integer $shutter_material_id
 * @property integer $shutter_finish_id
 * @property integer $carcass_material_id
 * @property integer $carcass_finish_id
 * @property string $description
 * @property double $rate
 * @property double $profit
 * @property integer $status
 * @property integer $created_by
 * @property string $created_date
 *
 * The followings are the available model relations:
 * @property QuotationCategoryMaster $quotationCategory
 * @property QuotationWorktypeMaster $workType
 * @property QuotationFinishMaster $finish
 */
class QuotationItemMaster extends CActiveRecord {

    public $image;
    public $additional_image;

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return '{{quotation_item_master}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('quotation_category_id, work_type_id, worktype_label,rate, profit, created_by, created_date', 'required'),
            array('quotation_category_id, work_type_id,shutter_finish_id,carcass_finish_id, status, created_by', 'numerical', 'integerOnly' => true),
            array('rate, profit', 'numerical'),
            array('worktype_label, work_type_description,description', 'length', 'max' => 255),
            array('worktype_label', 'unique'),
            array('image', 'file', 'types'=>'pdf,png,jpg' ,'allowEmpty' => true, 'maxFiles'=>'1'),
            array('additional_image', 'file', 'types'=>'pdf,png,jpg' ,'allowEmpty' => true, 'maxFiles'=>'1'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, quotation_category_id, work_type_id, worktype_label, work_type_description,additional_image, image, shutter_material_id, shutter_finish_id,carcass_material_id,carcass_finish_id, description, rate, profit, discount,status, created_by, created_date,updated_date,updated_by', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'quotationCategory' => array(self::BELONGS_TO, 'QuotationCategoryMaster', 'quotation_category_id'),
            'workType' => array(self::BELONGS_TO, 'QuotationWorktypeMaster', 'work_type_id'),
            'shutter_finish_id' => array(self::BELONGS_TO, 'QuotationFinishMaster', 'finish_id'),
            'carcass_finish_id' => array(self::BELONGS_TO, 'QuotationFinishMaster', 'finish_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'quotation_category_id' => 'Quotation Category',
            'work_type_id' => 'Work Type',
            'worktype_label' => 'Worktype Label',
            'work_type_description' => 'Work Type Description',
            'image' => 'Image',
            'additional_image' =>'Additional Image',
            'shutter_material_id' => 'Shutter Material',
            'shutter_finish_id' => 'Shutter Finish',
            'carcass_material_id' => 'Carcass Material',
            'carcass_finish_id' => 'Carcass Finish',
            'description' => 'Description',
            'rate' => 'Rate',
            'profit' => 'Profit',
            'discount' => 'Discount',
            'status' => 'Status',
            'created_by' => 'Created By',
            'created_date' => 'Created Date',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;
        $criteria->compare('id', $this->id);
        $criteria->compare('quotation_category_id', $this->quotation_category_id);
        $criteria->compare('work_type_id', $this->work_type_id);
        $criteria->compare('worktype_label', $this->worktype_label, true);
        $criteria->compare('work_type_description', $this->work_type_description, true);
        $criteria->compare('image', $this->image, true);
        $criteria->compare('additional_image', $this->additional_image, true);
        $criteria->compare('shutter_material_id', $this->shutter_material_id);
        $criteria->compare('shutter_finish_id', $this->shutter_finish_id);
        $criteria->compare('carcass_material_id', $this->carcass_material_id);
        $criteria->compare('carcass_finish_id', $this->carcass_finish_id);
        $criteria->compare('description', $this->description, true);
        $criteria->compare('rate', $this->rate);
        $criteria->compare('profit', $this->profit);
        $criteria->compare('discount', $this->discount);
        $criteria->compare('status', $this->status);
        $criteria->compare('created_by', $this->created_by);
        $criteria->compare('created_date', $this->created_date, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return QuotationItemMaster the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
