<link href="https://fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined" rel="stylesheet">
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/importstyle.css">
<?php
/* @var $this ClientsImportController */
/* @var $model ClientsImport */

$this->breadcrumbs = array(
    'Clients Imports' => array('index'),
    'Manage',
);



Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#clients-import-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<div class="modal fade" id="empModal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header clearfix">
                <h4 class="modal-title pull-left">Update Details</h4>
                <button type="button" class="close pull-right" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">

            </div>

        </div>
    </div>
</div>


<?php
Yii::app()->clientScript->registerScript('myjquery', "
$('.deleteimportproject').click(function(){
	var ids = $.fn.yiiGridView.getChecked('import-project-grid','selectedIds');
	if(ids != '')
	{
		var answer = confirm ('Are you sure you want to delete?');	
		if (answer)
		{		
					$.ajax({
						type: 'POST',
						dataType: 'json',
						url:'" . Yii::app()->createUrl("PurchaseCategory/deleteimports") . "',
						data:{ids:ids},
						success:function(response){
								
							if(response.response == 'success')
							{
							alert('Deleted Successfully');
                            location.reload();
							// window.parent.$.fn.yiiGridView.update('import-project-grid');
							}else
							{
							alert('Error Occured.. Please Try Again');
							}
						},
					});
					
		}else{	
		return false;
		}
	
	}else{
		alert('Please select at least one record!');
	
	}
				
				
});

");
?>
<div>
    <div class="entries-wrapper">
        <div class="row">
            <div class="col-xs-12 sub-heading">
                <div class="heading-title">Import Items</div>
                <?= CHtml::link(' <i class="fa fa-download font-size-16" aria-hidden="true"></i> Sample CSV', $url = $this->createAbsoluteUrl('purchaseCategory/downloadsample'), array('class' => 'btn btn-info btn-sm sample_btn', 'title' => 'Download Sample CSV ')) ?>
            </div>
        </div>
    </div>
    <div class="row">
        <?php
        $yourprovider = $model2->search();
        $imported_item_count = count($yourprovider->getData()); ?>
        <div class="<?= $imported_item_count == 0 ? 'col-md-12' : 'col-md-4' ?>  margin-top-20">
            <div class="form">
                <?php
                $form = $this->beginWidget('CActiveForm', array(
                    'id' => 'service-form',
                    'action' => Yii::app()->createUrl('/purchaseCategory/importcsv'),
                    'enableAjaxValidation' => false,
                    'method' => 'post',
                    'htmlOptions' => array(
                        'enctype' => 'multipart/form-data',
                        'class' => 'form-container'
                    )
                )); ?>
                <div class="align-center padding-top-30">
                    <div class="upload-files-container">
                        <div class="drag-file-area">
                            <span class="material-icons-outlined upload-icon"> file_upload </span>
                            <h6 class="dynamic-message"> Drag & drop any file here </h6>
                            <label class="label"> or <span class="browse-files">
                                    <?php echo $form->fileField($model, 'file', array('class' => 'default-file-input')); ?>

                                    <span class="browse-files-text">browse file</span>
                                    <span>from device</span>
                                </span> </label>
                        </div>
                        <span class="cannot-upload-message"> <span class="material-icons-outlined">error</span> Please
                            select a file first <span class="material-icons-outlined cancel-alert-button">cancel</span>
                        </span>
                        <div class="file-block">
                            <div class="file-info"> <span class="material-icons-outlined file-icon">description</span>
                                <span class="file-name"> </span> | <span class="file-size"> </span>
                            </div>
                            <span class="material-icons remove-file-icon">delete</span>
                            <div class="progress-bar"> </div>
                        </div>
                        <button type="submit" class="upload-button"> Upload </button>
                    </div>
                </div>
                <?php $this->endWidget(); ?>
            </div>
        </div>
        <?php
        $yourprovider = $model2->search();
        if ($imported_item_count > 0) { ?>
            <div class="col-md-8 margin-top-20">
                <?php
                $form = $this->beginWidget('CActiveForm', array(
                    'action' => Yii::app()->createUrl('/purchaseCategory/saveItems'),
                    'enableAjaxValidation' => false,
                    'method' => 'post'
                )); ?>
                <?php echo CHtml::submitButton('Save Item', array('name' => 'Save', 'class' => 'btn btn-primary btn-sm saveitemimport')); ?>
                <?php echo CHtml::submitButton('Delete Item', array('name' => 'Delete', 'class' => 'btn btn-danger btn-sm deleteimportproject')); ?>

                <?php
                $this->widget('zii.widgets.grid.CGridView', array(
                    'id' => 'import-project-grid',
                    'htmlOptions' => array('style' => 'overflow:auto;'),
                    'itemsCssClass' => 'table',
                    'dataProvider' => $model2->search(),
                    // 'filter'=>$model2,
                    'pager' => array(
                        'class' => 'CustomPagerGrid',
                        'header' => '',
                    ),
                    'pagerCssClass' => 'pager',
                    'columns' => array(
                        array(
                            'name' => 'check',
                            'id' => 'selectedIds',
                            'value' => '$data->import_id',
                            'class' => 'CCheckBoxColumn',
                            'selectableRows' => '100',
                        ),
                        array(
                            'value' => '$data->import_id',
                            'headerHtmlOptions' => array('style' => 'display:none'),
                            'htmlOptions' => array('style' => 'display:none', 'class' => 'rowId'),
                            'filterHtmlOptions' => array('style' => 'display:none'),
                        ),
                        array(
                            'header' => 'S No.',
                            'headerHtmlOptions' => array('width' => '10%'),
                            'htmlOptions' => array('width' => '10%'),
                            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
                        ),
                        array(
                            'header' => 'Category',
                            'value' => 'CHtml::tag("span", array("title" => $data->main_cat_exist == 0 ? "Main Category does not exist Please Click Here To Add it." : ""), $data->main_category)',
                            'cssClassExpression' => '$data->main_cat_exist == 0 ? "text-danger add_new_cat" : ""',
                            'type' => 'raw',

                        ),
                        array(
                            'header' => 'Sub Category',
                            'value' => 'CHtml::tag("span", array("title" => $data->sub_cat_exist == 0 ? "Sub Category does not exist Please Click Here To Add it." : ""), $data->sub_category)',
                            'cssClassExpression' => '$data->sub_cat_exist == 0 ? "text-danger add_new_sub" : ""',
                            'type' => 'raw',
                        ),
                        array(
                            'header' => 'Brand',
                            'value' => '$data->brand',
                            'value' => 'CHtml::tag("span", array("title" => $data->brand_exist == 0 ? "Given Brand does not exist Please Click Here To Add it." : ""), $data->brand)',
                            'cssClassExpression' => '$data->brand_exist == 0 ? "text-danger add_new_brand" : ""',
                            'type' => 'raw',
                        ),
                        array(
                            'header' => 'Type',
                            'value' => '$data->specification_type',
                        ),
                        array(
                            'header' => 'Specification',
                            'value' => '$data->specification',
                        ),
                        array(
                            'header' => 'Unit',
                            'value' => '$data->unit',
                        ),
                        array(
                            'header' => 'HSN Code',
                            'value' => '$data->hsn_code',
                        ),
                        array(
                            'header' => 'Company',
                            'value' => 'CHtml::tag("span", array("title" => $data->company_exist == 0 ? "Given Company does not exist Please Edit it" : ""), $data->company)',
                            'cssClassExpression' => '$data->company_exist == 0 ? "text-danger" : ""',
                            'type' => 'raw',
                        ),
                        array(
                            'header' => 'Status',
                            'value' => '',
                            'value' => function ($data) {
                                if ($data->spec_exist == '1') {
                                    echo 'Specification Already Exist';
                                } else {
                                    echo '-';
                                }
                            },
                        ),
                        array(
                            'class' => 'CButtonColumn',
                            'template' => '<a href="javascript:void(0)" onclick="updateaction(this)">Edit</a>'
                        ),
                    ),
                )); ?>
                <?php $this->endWidget(); ?>

                <?php //$this->endWidget(); ?>

            </div>
        <?php } ?>
    </div>
    <script>
        $('.action_menu').hide();
        function actionList(curElem) {
            $(curElem).closest("tr").siblings().find(".action_menu").hide();
            $(curElem).children(".action_menu").slideToggle("fast");
        };

        $(document).click(function (e) {
            if ($(e.target).closest('.tableaction').length > 0 || $(e.target).closest('.action_menu').length > 0) return;
            $('.action_menu').hide();
        });

        function updateaction(elem) {
            var rowid = $(elem).closest("tr").find(".rowId").text();
            var url = "<?php echo $this->createUrl('PurchaseCategory/updateImport&id=') ?>" + rowid;
            $.ajax({
                type: "POST",
                data: {
                    id: rowid
                },
                url: url,
                success: function (response) {
                    $('.modal-body').html(response);
                    $('#empModal').modal('show');
                }
            });
            // window.location.href = url;
        };
        function deleteaction(elem) {
            var rowid = $(elem).closest("tr").find(".rowId").text();
            var url = "<?php echo $this->createUrl('clientsImport/delete&id=') ?>" + rowid;
            if (!confirm('Are you sure you want to delete this item?')) return false;
            var afterDelete = function () { };
            $.fn.yiiGridView.update('import-project-grid', {
                type: 'POST',
                url: url,
                success: function (data) {
                    $.fn.yiiGridView.update('import-project-grid');
                    afterDelete(elem, true, data);
                },
                error: function (XHR) {
                    return afterDelete(elem, false, XHR);
                }
            });
            return false;
        };  
    </script>

    <style>
        div.form fieldset {
            border: 1px solid #DDD;
            padding: 10px;
            margin: 0 0 10px 0;
            -moz-border-radius: 7px;
        }
    </style>
    <script>
        var isAdvancedUpload = function () {
            var div = document.createElement('div');
            return (('draggable' in div) || ('ondragstart' in div && 'ondrop' in div)) && 'FormData' in window && 'FileReader' in window;
        }();

        let draggableFileArea = document.querySelector(".drag-file-area");
        let browseFileText = document.querySelector(".browse-files");
        let uploadIcon = document.querySelector(".upload-icon");
        let dragDropText = document.querySelector(".dynamic-message");
        let fileInput = document.querySelector(".default-file-input");
        let cannotUploadMessage = document.querySelector(".cannot-upload-message");
        let cancelAlertButton = document.querySelector(".cancel-alert-button");
        let uploadedFile = document.querySelector(".file-block");
        let fileName = document.querySelector(".file-name");
        let fileSize = document.querySelector(".file-size");
        let progressBar = document.querySelector(".progress-bar");
        let removeFileButton = document.querySelector(".remove-file-icon");
        let uploadButton = document.querySelector(".upload-button");
        let fileFlag = 0;

        $(document).on("click", ".default-file-input", function () {
            fileInput.value = '';
            console.log(fileInput.value);
        })
        // fileInput.addEventListener("click", () => {

        // });
        $(document).on("change", ".default-file-input", function (e) {
            // fileInput.addEventListener("change", e => {
            console.log(" > " + fileInput.value)
            uploadIcon.innerHTML = 'check_circle';
            dragDropText.innerHTML = 'File Dropped Successfully!';
            // document.querySelector(".label").innerHTML = `drag & drop or <span class="browse-files"> <input type="file" class="default-file-input" style=""/> <span class="browse-files-text" style="top: 0;"> browse file</span></span>`;
            uploadButton.innerHTML = `Upload`;
            fileName.innerHTML = fileInput.files[0].name;
            fileSize.innerHTML = (fileInput.files[0].size / 1024).toFixed(1) + " KB";
            uploadedFile.style.cssText = "display: flex;";
            progressBar.style.width = 0;
            fileFlag = 0;
        });
        removeFileButton.addEventListener("click", () => {
            uploadedFile.style.cssText = "display: none;";
            fileInput.value = '';
            fileName.innerHTML = '';
            fileSize.innerHTML = '';
            uploadIcon.innerHTML = 'file_upload';
            dragDropText.innerHTML = 'Drag & drop any file here';
            // document.querySelector(".label").innerHTML = `or <span class="browse-files"> <input type="file" class="default-file-input"/> <span class="browse-files-text">browse file</span> <span>from device</span> </span>`;
            uploadButton.innerHTML = `Upload`;
            location.reload();
        });

        /*uploadButton.addEventListener("click", () => {
            let isFileUploaded = fileInput.value;
            if(isFileUploaded != '') {
                if (fileFlag == 0) {
                    fileFlag = 1;
                    var width = 0;
                    var id = setInterval(frame, 50);
                    function frame() {
                            if (width >= 390) {
                            clearInterval(id);
                            uploadButton.innerHTML = `<span class="material-icons-outlined upload-button-icon"> check_circle </span> Uploaded`;
                            } else {
                            width += 5;
                            progressBar.style.width = width + "px";
                            }
                    }
                    }
            } else {
                cannotUploadMessage.style.cssText = "display: flex; animation: fadeIn linear 1.5s;";
            }
        });
        
        cancelAlertButton.addEventListener("click", () => {
            cannotUploadMessage.style.cssText = "display: none;";
        });
        */
        if (isAdvancedUpload) {
            ["drag", "dragstart", "dragend", "dragover", "dragenter", "dragleave", "drop"].forEach(evt =>
                draggableFileArea.addEventListener(evt, e => {
                    e.preventDefault();
                    e.stopPropagation();
                })
            );

            ["dragover", "dragenter"].forEach(evt => {
                draggableFileArea.addEventListener(evt, e => {
                    e.preventDefault();
                    e.stopPropagation();
                    uploadIcon.innerHTML = 'file_download';
                    dragDropText.innerHTML = 'Drop your file here!';
                });
            });

            draggableFileArea.addEventListener("drop", e => {
                uploadIcon.innerHTML = 'check_circle';
                dragDropText.innerHTML = 'File Dropped Successfully!';
                document.querySelector(".label").innerHTML = `drag & drop or <span class="browse-files"> <input type="file" class="default-file-input" style=""/> <span class="browse-files-text" style="top: -23px; left: -20px;"> browse file</span> </span>`;
                uploadButton.innerHTML = `Upload`;

                let files = e.dataTransfer.files;
                fileInput.files = files;
                console.log(files[0].name + " " + files[0].size);
                console.log(document.querySelector(".default-file-input").value);
                fileName.innerHTML = files[0].name;
                fileSize.innerHTML = (files[0].size / 1024).toFixed(1) + " KB";
                uploadedFile.style.cssText = "display: flex;";
                progressBar.style.width = 0;
                fileFlag = 0;
            });
        }

        $(document).on("click", ".add_new_cat", function () {
            var category = $(this).text();
            var answer = confirm("Category doesn't exist.Do you want to continue?");
            if (answer) {
                $.ajax({
                    type: "POST",
                    data: {
                        category: category
                    },
                    url: "<?php echo $this->createUrl('PurchaseCategory/createnewcategory') ?>",
                    success: function (response) {
                        $("#import-project-grid").load(location.href + " #import-project-grid>*", "");
                        //$("#viewitem").load(location.href + " #viewitem>*", "");                 
                        //$('#myTab a[href="#viewitem"]').tab('show');
                        localStorage.setItem('activeTab', "#viewitem");
                        location.reload();
                    }
                });
            }
        })

        $(document).on("click", ".add_new_brand", function () {
            var brand = $(this).text();
            var answer = confirm("Brand Not Exist.Do you want to continue?");
            if (answer) {
                $.ajax({
                    type: "POST",
                    data: {
                        brand: brand
                    },
                    url: "<?php echo $this->createUrl('PurchaseCategory/createnewbrand') ?>",
                    success: function (response) {
                        $("#import-project-grid").load(location.href + " #import-project-grid>*", "");
                        //$('#myTab a[href="#viewbrand"]').tab('show');
                        localStorage.setItem('activeTab', '#viewbrand');

                        location.reload();
                    }
                });
            }
        })


        $(document).on("click", ".add_new_sub", function () {
            var category = $(this).text();
            var parent_category = $(this).parents("tr").find("td:nth-child(4)").text();
            var answer = confirm(" Sub Category doesn't exist. Do you want to continue?");
            if (answer) {
                $.ajax({
                    type: "POST",
                    data: {
                        category: category,
                        parent_category: parent_category
                    },
                    dataType: 'json',
                    url: "<?php echo $this->createUrl('PurchaseCategory/createnewsubcategory') ?>",
                    success: function (response) {
                        if (response.response == 'success') {
                            $("#import-project-grid").load(location.href + " #import-project-grid>*", "");

                            localStorage.setItem('activeTab', '#viewsubcatitem');

                            location.reload();
                            alert('Sub category added successfully');
                        } else if (response.response == 'error') {
                            alert("Some Error Occured");
                        } else if (response.response == 'warning') {
                            alert("'" + response.sub_cat_check + "' Already Exist ");
                        }

                    }
                });
            }
        })

        $(function () {
            var selectedIds = new Set(); // Store selected checkboxes across pages

            // Initially hide buttons
            $(".saveitemimport,.deleteimportproject").css('visibility', 'hidden');

            // Handle individual checkbox change
            $(document).on("change", "table input[type=checkbox][name='selectedIds[]']", function () {
                var id = $(this).val();

                if ($(this).is(":checked")) {
                    selectedIds.add(id);
                } else {
                    selectedIds.delete(id);
                }

                updateCheckAllState(); // Sync "Check All" checkbox state
                toggleButtons(); // Update button visibility
            });

            // Handle "Check All" checkbox
            $(document).on("change", "table input[type=checkbox][name='selectedIds_all']", function () {
                var isChecked = $(this).is(":checked");

                $("table input[type=checkbox][name='selectedIds[]']").each(function () {
                    var id = $(this).val();
                    if (isChecked) {
                        selectedIds.add(id);
                    } else {
                        selectedIds.delete(id);
                    }
                    $(this).prop("checked", isChecked);
                });

                toggleButtons();
            });

            // Update "Check All" checkbox based on individual checkbox selections
            function updateCheckAllState() {
                var allCheckboxes = $("table input[type=checkbox][name='selectedIds[]']");
                var checkedCheckboxes = allCheckboxes.filter(":checked").length;
                var totalCheckboxes = allCheckboxes.length;

                $("table input[type=checkbox][name='selectedIds_all']").prop("checked", checkedCheckboxes === totalCheckboxes);
            }

            // Toggle button visibility based on the number of selected checkboxes
            function toggleButtons() {
                if (selectedIds.size > 0) {
                    $(".saveitemimport,.deleteimportproject").css('visibility', 'visible');
                } else {
                    $(".saveitemimport,.deleteimportproject").css('visibility', 'hidden');
                }
            }
        });




    </script>