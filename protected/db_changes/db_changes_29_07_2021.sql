-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jul 29, 2021 at 01:15 PM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.0.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--


--

-- --------------------------------------------------------

--
-- Table structure for table `jp_location`
--

CREATE TABLE `jp_location` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '1 = Active, 0 = Inactive	',
  `created_by` int(11) DEFAULT NULL COMMENT 'From user table',
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jp_location`
--

INSERT INTO `jp_location` (`id`, `name`, `status`, `created_by`, `created_date`) VALUES
(9, 'kottayam', 1, 9, '2021-06-22 00:00:00'),
(29, 'calicut', 1, 9, '2021-06-22 00:00:00'),
(30, 'idukki', 1, 9, '2021-06-22 00:00:00'),
(31, 'malappuram', 1, 9, '2021-06-22 00:00:00'),
(35, 'kasargod', 1, 9, '2021-06-22 00:00:00'),
(36, 'pathanamthitta', 1, 9, '2021-06-22 00:00:00'),
(37, 'Alappuzha', 1, 9, '2021-06-22 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `jp_quotation_category_master`
--

CREATE TABLE `jp_quotation_category_master` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL COMMENT 'If any subcategory',
  `name` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '1 = Active, 0 = Inactive',
  `created_by` int(11) DEFAULT NULL COMMENT 'From user table',
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jp_quotation_category_master`
--

INSERT INTO `jp_quotation_category_master` (`id`, `parent_id`, `name`, `status`, `created_by`, `created_date`) VALUES
(6, 0, 'Modular Kitchen', 1, 1, '0000-00-00 00:00:00'),
(36, 0, 'Tv Unit', 1, 1, '0000-00-00 00:00:00'),
(37, 0, 'Living Room', 1, 1, '0000-00-00 00:00:00'),
(38, 0, 'Bed Room', 1, 1, '0000-00-00 00:00:00'),
(39, NULL, 'Hall', 1, NULL, '2021-06-30 14:43:29');

-- --------------------------------------------------------

--
-- Table structure for table `jp_quotation_finish_master`
--

CREATE TABLE `jp_quotation_finish_master` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '1 = Active, 0 = Inactive',
  `created_by` int(11) DEFAULT NULL COMMENT 'From user table',
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jp_quotation_finish_master`
--

INSERT INTO `jp_quotation_finish_master` (`id`, `name`, `status`, `created_by`, `created_date`) VALUES
(1, 'Glossy Laminate', 1, 1, '2021-03-18 06:13:09'),
(2, 'Marino laminates test', 1, 9, '2021-05-18 00:00:00'),
(3, 'Glossy Laminate', 1, 1, '0000-00-00 00:00:00'),
(4, 'Marino laminates', 1, 1, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `jp_quotation_item_master`
--

CREATE TABLE `jp_quotation_item_master` (
  `id` int(11) NOT NULL,
  `quotation_category_id` int(11) NOT NULL,
  `work_type_id` int(11) NOT NULL,
  `worktype_label` varchar(255) NOT NULL,
  `work_type_description` varchar(255) DEFAULT NULL,
  `image` varchar(500) DEFAULT NULL,
  `shutter_material_id` varchar(250) DEFAULT NULL,
  `shutter_finish_id` int(11) DEFAULT NULL,
  `carcass_material_id` varchar(250) DEFAULT NULL,
  `carcass_finish_id` int(11) DEFAULT NULL,
  `material_ids` varchar(250) DEFAULT NULL,
  `finish_id` int(11) DEFAULT NULL,
  `shutterwork_description` varchar(255) DEFAULT NULL,
  `caracoss_description` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `rate` float(20,2) NOT NULL,
  `profit` float(20,2) NOT NULL,
  `discount` float(20,2) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '1 = Active, 0 = Inactive',
  `created_by` int(11) NOT NULL COMMENT 'From user table',
  `created_date` datetime NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



--
-- Table structure for table `jp_quotation_worktype_master`
--

CREATE TABLE `jp_quotation_worktype_master` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '1 = Active, 0 = Inactive',
  `created_by` int(11) DEFAULT NULL COMMENT 'From user table',
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jp_quotation_worktype_master`
--

INSERT INTO `jp_quotation_worktype_master` (`id`, `name`, `status`, `created_by`, `created_date`) VALUES
(1, 'Box work', 1, 1, '2021-03-12 04:12:15'),
(2, 'Ledge Work', 1, 1, '2021-03-12 00:00:00'),
(3, 'Panelling Work', 1, 1, '2021-03-12 00:00:00'),
(4, 'Additional Work', 1, 9, '2021-06-28 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `jp_salesexecutive`
--

CREATE TABLE `jp_salesexecutive` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '1 = Active, 0 = Inactive ',
  `created_by` int(11) DEFAULT NULL COMMENT 'From user table',
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jp_salesexecutive`
--

INSERT INTO `jp_salesexecutive` (`id`, `name`, `status`, `created_by`, `created_date`) VALUES
(3, 'ramesh123', 1, 9, '2021-06-25 00:00:00'),
(4, 'sunil', 1, 9, '2021-06-28 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `jp_sales_quotation`
--

CREATE TABLE `jp_sales_quotation` (
  `id` int(11) NOT NULL,
  `master_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `item_id` int(11) DEFAULT NULL,
  `category_label` varchar(255) NOT NULL,
  `work_type` int(11) DEFAULT NULL,
  `shutterwork_material` varchar(255) DEFAULT NULL,
  `shutterwork_finish` varchar(255) DEFAULT NULL,
  `carcass_material` varchar(255) DEFAULT NULL,
  `carcass_finish` varchar(255) DEFAULT NULL,
  `material` varchar(255) DEFAULT NULL,
  `finish` varchar(255) DEFAULT NULL,
  `shutterwork_description` varchar(255) DEFAULT NULL,
  `carcass_description` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `worktype_label` varchar(255) DEFAULT NULL,
  `unit` int(11) NOT NULL,
  `quantity` float NOT NULL,
  `quantity_nos` float NOT NULL,
  `mrp` float(10,2) NOT NULL,
  `amount_after_discount` float(10,2) NOT NULL,
  `revision_no` VARCHAR(120) DEFAULT NULL ,
  `status` int(11) DEFAULT NULL,
  `deleted_status` INT NOT NULL DEFAULT '1' COMMENT '1 = not deleted, 0 = deleted',
  `revision_remarks` VARCHAR(255) NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


--
-- Table structure for table `jp_sales_quotation_master`
--

CREATE TABLE `jp_sales_quotation_master` (
  `id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `client_name` varchar(50) DEFAULT NULL,
  `address` text,
  `phone_no` varchar(50) DEFAULT NULL,
  `email` varchar(70) DEFAULT NULL,
  `location_id` int(11) DEFAULT NULL,
  `sales_executive_id` int(11) DEFAULT NULL,
  `date_quotation` date NOT NULL,
  `invoice_no` varchar(120) DEFAULT NULL,
  `revision_no` varchar(120) DEFAULT NULL, 
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `jp_menu` CHANGE `controller` `controller` VARCHAR(250) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;

INSERT INTO `jp_menu` (`menu_id`, `menu_name`, `parent_id`, `status`, `controller`, `action`, `params`, `showmenu`, `show_list`, `related`) VALUES 
(763, 'List', '762', '0', 'salesexecutive/salesexecutive', 'index', '', '2', '1', NULL),
(762, 'Sales Executive', '0', '0', 'salesexecutive/salesexecutive', '', '', '0', '1', NULL),
(761, 'List', '760', '0', 'location/location', 'index', '', '2', '1', NULL), 
(760, 'Location', '0', '0', 'location/location', '', '', '0', '1', NULL), 
(759, 'List', '758', '0', 'worktype/quotationWorktype', 'index', '', '2', '1', NULL),
(758, 'Quotation Worktype', '0', '0', 'worktype/quotationWorktype', '', '', '0', '1', NULL),
(757, 'List', '756', '0', 'finish/quotationFinish', 'index', '', '2', '1', NULL),
(756, 'Quotation Finish', '0', '0', 'finish/quotationFinish', '', '', '0', '1', NULL),
(755, 'List', '754', '0', 'salesquotation/salesQuotation', 'index', '', '2', '1', NULL),
(754, 'Sales Quotation', '0', '0', 'salesquotation/salesQuotation', '', '', '0', '1', NULL), 
(753, 'List', '752', '0', 'quotationitem/quotationItemMaster', 'index', '', '2', '1', NULL), 
(752, 'Quotation Item', '0', '0', 'quotationitem/quotationItemMaster', '', '', '0', '1', NULL),
(751, 'List', '750', '0', 'quotationcategory/quotationCategoryMaster', 'index', '', '2', '1', NULL), 
(750, 'Quotation Category', '0', '0', 'quotationcategory/quotationCategoryMaster', '', '', '0', '1', NULL),
(749, 'Sales Quotation Report', '561', '0', 'salesquotation/salesQuotation', 'report', '', '2', '1', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `jp_location`
--
ALTER TABLE `jp_location`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jp_quotation_category_master`
--
ALTER TABLE `jp_quotation_category_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jp_quotation_finish_master`
--
ALTER TABLE `jp_quotation_finish_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jp_quotation_item_master`
--
ALTER TABLE `jp_quotation_item_master`
  ADD PRIMARY KEY (`id`),
  ADD KEY `shutter_finish_id` (`shutter_finish_id`),
  ADD KEY `shutter_material_id` (`shutter_material_id`),
  ADD KEY `carcass_material_id` (`carcass_material_id`),
  ADD KEY `carcass_finish_id` (`carcass_finish_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `quotation_category_id` (`quotation_category_id`),
  ADD KEY `work_type_id` (`work_type_id`),
  ADD KEY `finish_id` (`finish_id`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `jp_quotation_worktype_master`
--
ALTER TABLE `jp_quotation_worktype_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jp_salesexecutive`
--
ALTER TABLE `jp_salesexecutive`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jp_sales_quotation`
--
ALTER TABLE `jp_sales_quotation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `master_id` (`master_id`),
  ADD KEY `category_id` (`category_id`),
  ADD KEY `work_type` (`work_type`),
  ADD KEY `unit` (`unit`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `item_id` (`item_id`);

--
-- Indexes for table `jp_sales_quotation_master`
--
ALTER TABLE `jp_sales_quotation_master`
  ADD PRIMARY KEY (`id`),
  ADD KEY `company_id` (`company_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `location_id` (`location_id`),
  ADD KEY `sales_executive_id` (`sales_executive_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `jp_location`
--
ALTER TABLE `jp_location`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `jp_quotation_category_master`
--
ALTER TABLE `jp_quotation_category_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `jp_quotation_finish_master`
--
ALTER TABLE `jp_quotation_finish_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `jp_quotation_item_master`
--
ALTER TABLE `jp_quotation_item_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `jp_quotation_worktype_master`
--
ALTER TABLE `jp_quotation_worktype_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `jp_salesexecutive`
--
ALTER TABLE `jp_salesexecutive`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `jp_sales_quotation`
--
ALTER TABLE `jp_sales_quotation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=158;

--
-- AUTO_INCREMENT for table `jp_sales_quotation_master`
--
ALTER TABLE `jp_sales_quotation_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `jp_quotation_item_master`
--
ALTER TABLE `jp_quotation_item_master`
  ADD CONSTRAINT `jp_quotation_item_master_ibfk_1` FOREIGN KEY (`quotation_category_id`) REFERENCES `jp_quotation_category_master` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `jp_quotation_item_master_ibfk_10` FOREIGN KEY (`finish_id`) REFERENCES `jp_quotation_finish_master` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `jp_quotation_item_master_ibfk_11` FOREIGN KEY (`updated_by`) REFERENCES `jp_users` (`userid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `jp_quotation_item_master_ibfk_2` FOREIGN KEY (`work_type_id`) REFERENCES `jp_quotation_worktype_master` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `jp_quotation_item_master_ibfk_3` FOREIGN KEY (`shutter_finish_id`) REFERENCES `jp_quotation_finish_master` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `jp_quotation_item_master_ibfk_6` FOREIGN KEY (`carcass_finish_id`) REFERENCES `jp_quotation_finish_master` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `jp_quotation_item_master_ibfk_7` FOREIGN KEY (`created_by`) REFERENCES `jp_users` (`userid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `jp_quotation_item_master_ibfk_8` FOREIGN KEY (`quotation_category_id`) REFERENCES `jp_quotation_category_master` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `jp_quotation_item_master_ibfk_9` FOREIGN KEY (`work_type_id`) REFERENCES `jp_quotation_worktype_master` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `jp_sales_quotation`
--
ALTER TABLE `jp_sales_quotation`
  ADD CONSTRAINT `jp_sales_quotation_ibfk_1` FOREIGN KEY (`master_id`) REFERENCES `jp_sales_quotation_master` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `jp_sales_quotation_ibfk_2` FOREIGN KEY (`category_id`) REFERENCES `jp_quotation_category_master` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `jp_sales_quotation_ibfk_3` FOREIGN KEY (`work_type`) REFERENCES `jp_quotation_worktype_master` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `jp_sales_quotation_ibfk_4` FOREIGN KEY (`unit`) REFERENCES `jp_unit` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `jp_sales_quotation_ibfk_5` FOREIGN KEY (`created_by`) REFERENCES `jp_users` (`userid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `jp_sales_quotation_ibfk_6` FOREIGN KEY (`item_id`) REFERENCES `jp_quotation_item_master` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `jp_sales_quotation_master`
--
ALTER TABLE `jp_sales_quotation_master`
  ADD CONSTRAINT `jp_sales_quotation_master_ibfk_1` FOREIGN KEY (`company_id`) REFERENCES `jp_company` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `jp_sales_quotation_master_ibfk_4` FOREIGN KEY (`created_by`) REFERENCES `jp_users` (`userid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `jp_sales_quotation_master_ibfk_5` FOREIGN KEY (`location_id`) REFERENCES `jp_location` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `jp_sales_quotation_master_ibfk_6` FOREIGN KEY (`sales_executive_id`) REFERENCES `jp_salesexecutive` (`id`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;