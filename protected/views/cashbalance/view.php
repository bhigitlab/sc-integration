<?php
/* @var $this CashbalanceController */
/* @var $model Cashbalance */

$this->breadcrumbs=array(
	'Cashbalances'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Cashbalance', 'url'=>array('index')),
	array('label'=>'Create Cashbalance', 'url'=>array('create')),
	array('label'=>'Update Cashbalance', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Cashbalance', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Cashbalance', 'url'=>array('admin')),
);
?>

<h1>View Cashbalance #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'cashbalance_type',
		'bank_id',
		'cashbalance_opening_balance',
		'cashbalance_deposit',
		'cashbalance_withdrawal',
		'cashbalance_closing_balance',
		'created_by',
		'created_date',
	),
)); ?>
