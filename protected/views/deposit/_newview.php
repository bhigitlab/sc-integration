
<?php
if ($index == 0) {
    ?>
    <thead>
    <tr> 
        <th>No</th>
        <th>Deposit Name</th>
        <th style="display: none;"></th>          
    </tr>   
    </thead>
<?php } ?>
    <tr> 
        <td style="width: 50px;"><?php echo $index+1; ?></td>
        
        <td  class="text_black" data-value="<?php echo CHtml::encode($data->deposit_name); ?>" data-id="<?php echo $data->deposit_id; ?>" >
            <span class="list_item"><?php echo CHtml::encode($data->deposit_name); ?></span>
            <?php
            if(isset(Yii::app()->user->role) && (in_array('/deposit/update', Yii::app()->user->menuauthlist))){
            ?>
            <span class="fa fa-edit editable" onclick="editaction(this,event);" data-id="<?php echo $data->deposit_id; ?>" data-value="<?php echo CHtml::encode($data->deposit_name); ?>"></span>
            <?php } ?>
        </td>
        
        <td class='editelement test' style="display:none;">
        <input  class='deposit_name singletextbox' type = 'text'  name='project_type' data-value="<?php echo CHtml::encode($data->deposit_name); ?>">
        <a class='save btn btn-info btn-xs' onclick="saveaction(this,event);" type = 'submit' data-id="<?php echo $data->deposit_id; ?>" >Save</a>
        <a class='cancel btn btn-default btn-xs' onclick="cancelaction(this,event);">Cancel</a>
        <br>
        <span class='errorMessage1'></span>
       
        
        </td>		
    
    </tr>  

    
