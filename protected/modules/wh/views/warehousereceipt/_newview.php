<!--<div id="msg_box"></div>-->
<?php
if ($data['delete_status'] == 0) {
    $warehousereceipt_no = !empty($data['warehousereceipt_no']) ? CHtml::link($data['warehousereceipt_no'], 'index.php?r=wh/warehousereceipt/view&receiptid=' . $data['warehousereceipt_id'], array('class' => 'link', 'target' => '_blank')) : '';
    $warehousereceipt = Warehousereceipt::model()->findByPk($data['warehousereceipt_id']);
    $warehouse_to = $warehousereceipt->warehouse['warehouse_name'];
    $warehouse_from = $warehousereceipt->warehousereceiptwarehouseidfrom['warehouse_name'];
    $clerk = $warehousereceipt->clerk['first_name'] . ' ' . $warehousereceipt->clerk['last_name'];
    if ($data['warehousereceipt_transfer_type'] == 1) {
        $transfer_type = "Purchase";
        $purchase_id = $warehousereceipt->warehousereceiptBillId['purchase_id'];
        $bill_check = Controller::billWithOrWithoutPO($purchase_id);
        if ($bill_check == 'po') {
            $bill_or_despatch_num = !empty($warehousereceipt->warehousereceiptBillId['bill_number']) ? CHtml::link($warehousereceipt->warehousereceiptBillId['bill_number'], 'index.php?r=bills/view&id=' . $warehousereceipt->warehousereceipt_bill_id, array('class' => 'link', 'target' => '_blank')) : '';
        } else {
            $bill_or_despatch_num = !empty($warehousereceipt->warehousereceiptBillId['bill_number']) ? CHtml::link($warehousereceipt->warehousereceiptBillId['bill_number'], 'index.php?r=bills/billview&id=' . $warehousereceipt->warehousereceipt_bill_id, array('class' => 'link', 'target' => '_blank')) : '';
        }
    } elseif ($data['warehousereceipt_transfer_type'] == 2) {
        $transfer_type = "Despatch";
        $bill_or_despatch_num = !empty($warehousereceipt->warehousereceiptDespatch['warehousedespatch_no']) ? CHtml::link($warehousereceipt->warehousereceiptDespatch['warehousedespatch_no'], 'index.php?r=wh/warehousedespatch/view&despatchid=' . $warehousereceipt->warehousereceipt_despatch_id, array('class' => 'link', 'target' => '_blank')) : '';
    } else {
        $transfer_type = "-";
        $bill_or_despatch_num = "-";
    }
} else {
    //  $warehousereceipt_no_link = !empty($data['warehousereceipt_no'])?CHtml::link($data['warehousereceipt_no'], 'index.php?r=warehousereceipt/deletedWarehouseView&receiptid='.$data['warehousereceipt_id'], array('class' => 'link','target'=>'_blank')):'';
    $warehousereceipt_no_link = !empty($data['warehousereceipt_no']) ? CHtml::link("(Deleted)", 'index.php?r=wh/warehousereceipt/deletedWarehouseView&receiptid=' . $data['warehousereceipt_id'], array('class' => 'link_deleted', 'target' => '_blank')) : '';
    $warehousereceipt_no = "<span style='color: red;font-size: 12px;'>" . $warehousereceipt_no_link . "  </span>";
    $warehousereceipt = WarehouseTransferDeletedLog::model()->findByAttributes(array('warehouse_transfer_type_deleted_id' => $data['warehousereceipt_id'], 'warehouse_transfer_type_delete' => 1));
    $warehouse_to = $warehousereceipt->warehouseidTo['warehouse_name'];
    $warehouse_from = $warehousereceipt->warehouseidFrom['warehouse_name'];
    $clerk = $warehousereceipt->clerk['first_name'] . ' ' . $warehousereceipt->clerk['last_name'];
    if ($data['warehousereceipt_transfer_type'] == 1) {
        $transfer_type = "Purchase";
        $bill_id_details = $warehousereceipt->warehousereceiptBillIdDetails($warehousereceipt->warehousereceipt_bill_id);
        $bill_check = Controller::billWithOrWithoutPO($bill_id_details['purchase_id']);
        if ($bill_check == 'po') {
            $bill_or_despatch_num = !empty($bill_id_details['bill_number']) ? CHtml::link($bill_id_details['bill_number'], 'index.php?r=bills/view&id=' . $warehousereceipt->warehousereceipt_bill_id, array('class' => 'link', 'target' => '_blank')) : '';
        } else {
            $bill_or_despatch_num = !empty($bill_id_details['bill_number']) ? CHtml::link($bill_id_details['bill_number'], 'index.php?r=bills/billview&id=' . $warehousereceipt->warehousereceipt_bill_id, array('class' => 'link', 'target' => '_blank')) : '';
        }
    } elseif ($data['warehousereceipt_transfer_type'] == 2) {
        $transfer_type = "Despatch";
        $despatch_id_details = $warehousereceipt->warehousereceiptDespatchDetails($warehousereceipt->warehousereceipt_despatch_id);
        $bill_or_despatch_num = !empty($despatch_id_details['warehousedespatch_no']) ? CHtml::link($despatch_id_details['warehousedespatch_no'], 'index.php?r=wh/warehousedespatch/view&despatchid=' . $warehousereceipt->warehousereceipt_despatch_id, array('class' => 'link', 'target' => '_blank')) : '';
    } else {
        $transfer_type = "-";
        $bill_or_despatch_num = "-";
    }
}

if ($index == 0) {
?>
    <thead class="entry-table sticky-thead">
        <tr>
            <th></th>
            <th>No</th>
            <th>Receipt No</th>
            <th>Transfer Type</th>
            <th>Warehouse Name</th>
            <th>Bill/ Despatch Number</th>
            <th>Goods received from</th>
            <th>Clerk</th>
            <th>Date</th>
            
        </tr>
    </thead>
<?php } ?>




<tr id="tr_<?php echo $data['warehousereceipt_id']; ?>">
    <td>
        <span class="icon icon-options-vertical popover-test" data-toggle="popover" data-placement="right" type="button" data-html="true" style="cursor: pointer;"></span>
        <div class="popover-content hide">
            <ul class="tooltip-hiden">
                <?php if ($data['delete_status'] == 0) {  ?>
                    <li>
                        <a href="index.php?r=wh/warehousereceipt/view&receiptid=<?php echo $data['warehousereceipt_id']; ?>" id="<?php echo $data['warehousereceipt_id']; ?>" class="view_purchase btn btn-xs btn-default" title="View">View</a>
                    </li>
                    <?php
                    if ((isset(Yii::app()->user->role) && (in_array('/wh/warehousereceipt/update', Yii::app()->user->menuauthlist)))) {
                    ?>
                    <li>
                        <a href="index.php?r=wh/warehousereceipt/update&receiptid=<?php echo $data['warehousereceipt_id']; ?>" id="<?php echo $data['warehousereceipt_id']; ?>" data-id="<?php echo $data['warehousereceipt_id']; ?>" class=" edit_purchase btn btn-xs btn-default" title="Edit">Edit</a>
                    </li>
                    <?php
                    }
                    if ((isset(Yii::app()->user->role) && (in_array('/wh/warehousereceipt/warehoueReceiptDelete', Yii::app()->user->menuauthlist)))) {
                    ?>
                    <li>
                        <a href="#" id="<?php echo $data['warehousereceipt_id']; ?>" data-id="<?php echo $data['warehousereceipt_id']; ?>" class=" delete_warehousereceipt btn btn-xs btn-default" title="Delete">Delete</a>
                    </li>
                    <?php } ?>
                <?php } else { ?>
                    <li>
                        <a href="index.php?r=wh/warehousereceipt/deletedWarehouseView&receiptid=<?php echo $data['warehousereceipt_id']; ?>" id="<?php echo $data['warehousereceipt_id']; ?>" class="view_purchase btn btn-xs btn-default" title="View">View</a>
                    </li>

                <?php } ?>
            </ul>
        </div>
    </td>
    <td class="sl_no text-right"><?php echo $index + 1; ?></td>
    <td><?php echo $warehousereceipt_no; ?></td>
    <td><?php echo $transfer_type;  ?></td>
    <td><?php echo $warehouse_to; ?></td>
    <td><?php echo $bill_or_despatch_num;  ?></td>
    <td><?php echo $warehouse_from;  ?></td>
    <td><?php echo $clerk;  ?></td>
    <td class="wrap"><?php echo  date("d-m-Y", strtotime($data['warehousereceipt_date'])); ?></td>
    
</tr>