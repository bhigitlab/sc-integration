<?php
/* @var $this UnitConversionController */
/* @var $model UnitConversion */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'item_id'); ?>
		<?php echo $form->textField($model,'item_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'base_unit'); ?>
		<?php echo $form->textField($model,'base_unit'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'conversion_factor'); ?>
		<?php echo $form->textField($model,'conversion_factor'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'conversion_unit'); ?>
		<?php echo $form->textField($model,'conversion_unit'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created_date'); ?>
		<?php echo $form->textField($model,'created_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'priority'); ?>
		<?php echo $form->textField($model,'priority',array('size'=>1,'maxlength'=>1)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->