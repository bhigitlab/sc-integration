<?php
/* @var $this ImageGalleryController */
/* @var $model ImageGallery */
/* @var $form CActiveForm */
?>


<h5>Filter By :</h5>
<?php
$form = $this->beginWidget('CActiveForm', array(
    'action' => Yii::app()->createUrl($this->route),
    'method' => 'get',
        ));
?>
<div class="filter">
    <div class="row ">
		
		
		 <div class="col-md-2 col-sm-5 col-xs-5">					
            <?php echo $form->label($model, 'projectid'); ?>
            <?php
            echo $form->dropDownList($model, 'projectid', CHtml::listData(Projects::model()->findAll(array('order'=>'name ASC')), 'pid', 'name'),
             array('empty' => '-----------', 'ajax' => array(
                           'type' => 'POST',
                           'dataType' => 'json',
                           'url' => CController::createUrl('/ImageGallery/getalbum'),
                           'data' => array('id' => 'js:this.value'),
                           'success' => 'function(data) {
                       $("#ImageGallery_albumid").html("");
                       $("#ImageGallery_albumid").html(data.albums);
                    }')));
                    
                    
            ?>
        </div>

        <div class="col-md-2 col-sm-5 col-xs-5">					
           <?php echo $form->labelEx($model, 'albumid'); ?>
                <?php
                echo $form->dropDownList($model, 'albumid', array(), array('empty' => '-----------'))
                ?>
        </div>
        <div class="col-md-2 col-sm-3 col-xs-3">	

				<label>&nbsp;</label>
				<div class= "text-left">
                <?php echo CHtml::submitButton('Go'); ?>
                <?php echo CHtml::resetButton('Clear', array('onclick' => 'javascript:location.href="' . $this->createUrl('admin') . '"')); ?>
            </div>
        </div>

    </div>

</div>
<?php $this->endWidget(); ?>
