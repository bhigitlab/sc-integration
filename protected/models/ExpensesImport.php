<?php

/**
 * This is the model class for table "{{expenses_import}}".
 *
 * The followings are the available columns in table '{{expenses_import}}':
 * @property integer $exp_id
 * @property string $projectid
 * @property string $userid
 * @property string $date
 * @property double $amount
 * @property string $description
 * @property string $type
 * @property string $exptype
 * @property string $vendor_id
 * @property string $payment_type
 * @property string $works_done
 * @property string $materials
 * @property integer $created_by
 * @property string $created_date
 * @property integer $updated_by
 * @property string $updated_date
 * @property string $purchase_type
 * @property double $paid
 * @property string $import_status
 */
class ExpensesImport extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{expenses_import}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('created_by, updated_by', 'numerical', 'integerOnly'=>true),
			array('amount, paid', 'numerical'),
			array('projectid, userid, type, exptype, vendor_id, payment_type, purchase_type', 'length', 'max'=>50),
			array('description, import_status', 'length', 'max'=>300),
			array('works_done, materials', 'length', 'max'=>100),
			array('exp_date, created_date, updated_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('exp_id, projectid, userid, exp_date, amount, description, type, exptype, vendor_id, payment_type, works_done, materials, created_by, created_date, updated_by, updated_date, purchase_type, paid, import_status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'exp_id' => 'Exp',
			'projectid' => 'Projectid',
			'userid' => 'Userid',
			'exp_date' => 'Date',
			'amount' => 'Amount',
			'description' => 'Description',
			'type' => 'Type',
			'exptype' => 'Exptype',
			'vendor_id' => 'Vendor',
			'payment_type' => 'Payment Type',
			'works_done' => 'Works Done',
			'materials' => 'Materials',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
			'updated_by' => 'Updated By',
			'updated_date' => 'Updated Date',
			'purchase_type' => 'Purchase Type',
			'paid' => 'Paid',
			'import_status' => 'Import Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
                
		$criteria->compare('exp_id',$this->exp_id);
		$criteria->compare('projectid',$this->projectid,true);
		$criteria->compare('userid',$this->userid,true);
		$criteria->compare('exp_date',$this->exp_date,true);
		$criteria->compare('amount',$this->amount);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('exptype',$this->exptype,true);
		$criteria->compare('vendor_id',$this->vendor_id,true);
		$criteria->compare('payment_type',$this->payment_type,true);
		$criteria->compare('works_done',$this->works_done,true);
		$criteria->compare('materials',$this->materials,true);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('updated_by',$this->updated_by);
		$criteria->compare('updated_date',$this->updated_date,true);
		$criteria->compare('purchase_type',$this->purchase_type,true);
		$criteria->compare('paid',$this->paid);
		$criteria->compare('import_status',$this->import_status,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,'pagination' => array('pageSize' => 100),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ExpensesImport the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
