<?php

/**
 * This is the model class for table "{{voucher_items}}".
 *
 * The followings are the available columns in table '{{voucher_items}}':
 * @property integer $id
 * @property integer $voucher_id
 * @property string $worker_label
 * @property integer $no_workers
 * @property string $description
 * @property string $working_time_size
 * @property string $net_quantity
 * @property integer $unit
 * @property integer $created_by
 * @property string $created_date
 * @property integer $updated_by
 * @property string $updated_date
 *
 * The followings are the available model relations:
 * @property Users $createdBy
 * @property Users $updatedBy
 * @property Vouchers $voucher
 * @property Unit $unit0
 */
class VoucherItems extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{voucher_items}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('voucher_id, no_workers, unit, created_by, updated_by', 'numerical', 'integerOnly' => true),
			array('voucher_id,description, working_time_size, net_quantity', 'required'),
			array('worker_label, working_time_size, net_quantity', 'length', 'max' => 100),
			array('description, created_date, updated_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, voucher_id, worker_label, no_workers, description, working_time_size, net_quantity, unit, created_by, created_date, updated_by, updated_date', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'createdBy' => array(self::BELONGS_TO, 'Users', 'created_by'),
			'updatedBy' => array(self::BELONGS_TO, 'Users', 'updated_by'),
			'voucher' => array(self::BELONGS_TO, 'Vouchers', 'voucher_id'),
			'unit0' => array(self::BELONGS_TO, 'Unit', 'unit'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'voucher_id' => 'Voucher',
			'worker_label' => 'Worker Label',
			'no_workers' => 'No Workers',
			'description' => 'Description',
			'working_time_size' => 'Working Time/Size',
			'net_quantity' => 'Net Quantity',
			'unit' => 'Unit',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
			'updated_by' => 'Updated By',
			'updated_date' => 'Updated Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('voucher_id', $this->voucher_id);
		$criteria->compare('worker_label', $this->worker_label, true);
		$criteria->compare('no_workers', $this->no_workers);
		$criteria->compare('description', $this->description, true);
		$criteria->compare('working_time_size', $this->working_time_size, true);
		$criteria->compare('net_quantity', $this->net_quantity, true);
		$criteria->compare('unit', $this->unit);
		$criteria->compare('created_by', $this->created_by);
		$criteria->compare('created_date', $this->created_date, true);
		$criteria->compare('updated_by', $this->updated_by);
		$criteria->compare('updated_date', $this->updated_date, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return VoucherItems the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}
}
