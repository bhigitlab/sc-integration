<?php
$tblpx = Yii::app()->db->tablePrefix;
$newQuery1 = "";
$newQuery2 = "";
$user = Users::model()->findByPk(Yii::app()->user->id);
$arrVal = explode(',', $user->company_id);
foreach($arrVal as $arr) {
    if ($newQuery1) $newQuery1 .= ' OR';
    if ($newQuery2) $newQuery2 .= ' OR';
    $newQuery1 .= " FIND_IN_SET('".$arr."', id)";
    $newQuery2 .= " FIND_IN_SET('".$arr."', company_id)";
}
?>
<h5>Filter By :</h5>

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>
<div class="filter">
    <div class="row ">
	<div class="col-md-2 col-sm-5 col-xs-5">
		<?php echo $form->label($model, 'projectid'); ?>
            <?php
            if (Yii::app()->user->role == 1) {

                    echo $form->dropDownList($model, 'projectid', CHtml::listData(Projects::model()->findAll(array(
                                        'select' => array('pid, name'),
                                        'condition' => '('.$newQuery2.')',
                                        'order' => 'name',
                                        'distinct' => true
                                    )), 'pid', 'name'), array('empty' => '-----------', 'id' => 'projectid'));
                
            } else {

                echo $form->dropDownList($model, 'projectid', CHtml::listData(Projects::model()->findAll(array(
                                    'select' => array('pid, name'),
                                    'order' => 'name',
                                    'condition' => '('.$newQuery2.')',
                                    "condition" => 'pid in (select projectid from jp_project_assign where userid=' . Yii::app()->user->id . ')',
                                    'distinct' => true
                                )), 'pid', 'name'), array('empty' => 'select project', 'id' => 'projectid'));
            }
            ?>
	</div>
        <div class="col-md-2 col-sm-5 col-xs-5">
            <?php echo $form->label($model, 'company_id'); ?>
        <?php

                echo $form->dropDownList($model, 'company_id', CHtml::listData(Company::model()->findAll(array(
                                    'select' => array('id, name'),
                                    'order' => 'id',
                                    'condition' => '('.$newQuery1.')',
                                    'distinct' => true
                                )), 'id', 'name'), array('empty' => 'select company', 'id' => 'company_id'));
            ?>
        </div>
        <div class="col-md-3 col-sm-5 col-xs-5 date">						
            <?php echo $form->label($model, 'date'); ?>
            <?php echo CHtml::activeTextField($model, 'fromdate', array('size' => 10, 'value' => isset($_REQUEST['Dailyreport']['fromdate']) ? $_REQUEST['Dailyreport']['fromdate'] : '')); ?>
            <?php
            $this->widget('application.extensions.calendar.SCalendar', array(
                'inputField' => 'Dailyreport_fromdate',
                'ifFormat' => '%d-%b-%Y',
            ));
            ?> to
            <?php echo CHtml::activeTextField($model, 'todate', array('size' => 10, 'value' => isset($_REQUEST['Dailyreport']['todate']) ? $_REQUEST['Dailyreport']['todate'] : '')); ?>
            <?php
            $this->widget('application.extensions.calendar.SCalendar', array(
                'inputField' => 'Dailyreport_todate',
                'ifFormat' => '%d-%b-%Y',
            ));
            ?>

        </div>
	

	<div class="col-md-2 col-sm-3 col-xs-3">
            <label>&nbsp;</label>
            <div class= "text-left">
		<?php echo CHtml::submitButton('Search'); ?>
                <?php echo CHtml::resetButton('Clear', array('onclick' => 'javascript:location.href="' . $this->createUrl('DailyReport/admin') . '"')); ?>
	</div>
        </div>

    </div>

</div>
<?php $this->endWidget(); ?>
