

<?php
/* @var $this ProjectTypeController */
/* @var $model ProjectType */
/* @var $form CActiveForm */
?>

<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'user-roles-form',
    'enableAjaxValidation' => true,
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
        'validateOnChange' => true,
        'validateOnType' => false,),
        ));
?>

<div class="panel-body">
    <div class="row addRow">
        <div class="col-md-6">
            <?php echo $form->labelEx($model, 'role'); ?>
            <?php echo $form->textField($model, 'role', array('size' => 50, 'maxlength' => 50, 'class' => 'form-control')); ?>
            <?php echo $form->error($model, 'role'); ?>
        </div>
        <div class="col-md-6 save-btnHold">
            <label>&nbsp;</label>
            <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-sm btn-info'));
            ?>
            &nbsp;
            <?php
            echo CHtml::ResetButton('Close', array('onclick' => 'closeaction(this,event);', 'class' => 'btn btn-sm'));
            ?>      
        </div>
    </div>

</div><!-- form -->

<?php $this->endWidget(); ?>

