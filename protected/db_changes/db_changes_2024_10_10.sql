ALTER TABLE jp_dailyreport CHANGE approve_status approve_status ENUM('1','2','3','4') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL COMMENT '1=>approve, 2=>not approve,3=>approved but cost not include in project report,4=>partial request';

ALTER TABLE jp_dailyreport ADD request_from ENUM('1','2') NOT NULL DEFAULT '1' COMMENT '1=>from accounts ,2=>from pms' AFTER default_labour_type, ADD pms_project_id INT(11) NULL DEFAULT NULL AFTER request_from, ADD pms_wpr_id INT(11) NULL DEFAULT NULL AFTER pms_project_id;

ALTER TABLE jp_dailyreport ADD pms_status ENUM('1','2') NOT NULL DEFAULT '1' COMMENT '1=>request generated in accounts,2=>change effected in pms' AFTER pms_wpr_id;

ALTER TABLE jp_daily_report_labour_details ADD pms_labour_id INT(11) NULL DEFAULT NULL AFTER worktype;
ALTER TABLE jp_daily_report_labour_details CHANGE labour_id labour_id INT(11) NULL;

ALTER TABLE jp_dailyreport CHANGE amount amount FLOAT(20,2) NOT NULL DEFAULT '0';
ALTER TABLE jp_dailyreport CHANGE labour_wage labour_wage FLOAT(20,2) NOT NULL DEFAULT '0';
ALTER TABLE jp_dailyreport CHANGE helper_wage helper_wage FLOAT(20,2) NOT NULL DEFAULT '0';

ALTER TABLE jp_labour_template ADD pms_labour_template_id INT(11) NOT NULL DEFAULT '0' AFTER labour_type;