<?php
    if(!empty($mrId)){
        $materialreq = MaterialRequisition::model()->findByPk($mrId) ;
       
        if(!empty($materialreq)){
            if(!empty($itemId)){
                $table_id = MaterialRequisitionItems::Model()->findByPk($itemId);
                if(!empty($table_id)){
                    $purchasemodel = Purchase::model()->findByPk($table_id->po_id);
        if(!empty($purchasemodel)){
                }
            }
        
        ?>
         
        <?php }
        } ?>
                              
<?php  
    }
    ?>
        

<!-- <div class="container"> -->
        <div class="clearfix"></div>
        <div id="msg"></div>
        <div id="previous_details"></div>
        <!-- <div class="purchase_items entries-wrapper"> -->
            <div class="row">
                <div class="col-xs-12">
                      <div class="heading-title">Add/Edit Purchase Item</div>
                      <div class="dotted-line"></div>
                </div>
            </div>
            <div class="row d-flex">
                    <div class="col-xs-12 col-md-9">
                        <div class="row">
                            <input type = "hidden" id ="mrId" value="<?php echo isset($mrId)?$mrId:''?>">
                            <?php 
                            $project_id='';
                            if(!empty($mrId)){
                                if(!empty($itemId)){
                                    $table_id = MaterialRequisitionItems::Model()->findByPk($itemId);
                                    if(!empty($table_id)){
                                        $mr_val= MaterialRequisition::Model()->findByPk($table_id->mr_id);
                                        if(!empty($mr_val)){
                                            $project_id=$mr_val->project_id;
                                        }
                                    }
                                }
                                
                                
                                ?>
                                <input type = "hidden" id ="project" value="<?php echo isset($project_id)?$project_id:''?>">
                            <?php }
                            ?>
                            <input type = "hidden" id ="itemId" value="<?php echo isset($itemId)?$itemId:''?>">
                            <input type="hidden" class="date" value="<?php echo isset($purchasemodel->purchase_date) ? date('d-m-Y', strtotime($purchasemodel->purchase_date)) : ''; ?>">


                            <input type="hidden" name="purchase_id" id="purchase_id" value="<?php echo isset($purchasemodel->p_id)? $purchasemodel->p_id:0 ; ?>">
                            
                                
                                <div id="select_div " class="purchaseitem select2-width  form-group col-xs-12 col-md-4">
                                        <label>Specification:</label>
                                        <select class="txtBox form-control  specification description selecboxwidth" id="description" name="description[]" >
                                            <option value="">Select one</option>
                                            <?php
                                            foreach ($specification as $key => $value) {
                                            ?>
                                                <option value="<?php echo $value['id']; ?>"><?php echo $value['data']; ?></option>
                                            <?php } ?>
                                            <option value="other">Other</option>
                                        </select>
                                        <input type="hidden" id="itemid_val" name="itemid_val" value="">

                               
                                </div>
                            
                                <div class="purchaseitem form-group col-xs-12 col-md-2">
                                    <label class="text-nowrap">HSN Code </label>
                                    <p id="hsn_code" class="hsn_code form-group col-xs-12 col-md-2">&nbsp;&nbsp;</p>
                                </div>
                            
                            <div class="purchaseitem remark form-group col-xs-12 col-md-2">
                                
                                    <label>Remarks:</label>
                                    <input type="text" class="txtBox form-control" id="remarks" name="remark[]" placeholder="Remark" />
                                
                            </div>
                            <div class="purchaseitem quantity_div form-group col-xs-12 col-md-2">
                               
                                    <label>Quantity:</label>
                                    <input type="text" class="inputs target txtBox quantity allownumericdecimal form-control max-w-100" id="quantity" name="quantity[]" placeholder="" /></td>
                                    <div id="remaining_estimated_qty" class="padding-box estimation-details"></div>
                                    <input type="hidden" id="remaining_estimation_qty" name="remaining_estimation_qty" value="">
                               
                            </div>
                            <div class="purchaseitem form-group col-xs-12 col-md-2">
                               
                                    <label>Units:</label>
                                    <select class="txtBox  form-control item_unit w-100 mobile-w-200" id="item_unit" name="item_unit">

                                    </select>
                                    <input type="hidden" name="unitval" id="unitval">
                                    <!-- <div id="item_unit" class="item_unit padding-box">&nbsp;&nbsp;</div> -->
                               
                            </div>

                        </div>
                        <div class="row">
                           
                            <div class="purchaseitem base-data form-group col-xs-12 col-md-2">
                                <label>Base Quantity:</label>
                                <input type="text" class="inputs target txtBox base_qty allownumericdecimal form-control" id="base_qty" name="base_qty[]" placeholder="" />
                            </div>
                            <div class="purchaseitem base-data form-group col-xs-12 col-md-2">
                                <label>Base Unit:</label>
                                <input type=" text" class="inputs target txtBox base_unit allownumericdecimal form-control" id="base_unit" name="base_unit[]" placeholder="" readonly="true" />
                            </div>
                            <div class="purchaseitem base-data form-group col-xs-12 col-md-2">
                                <label>Base Rate:</label>
                                <input type="text" class="inputs target txtBox base_rate allownumericdecimal form-control" id="base_rate" name="base_rate[]" placeholder="" readonly="true" />
                            </div>
                        </div>
                        <div class="row">
                             <div class="purchaseitem form-group col-xs-12 col-md-2">
                                
                                    <label>Rate:</label>
                                    <input type="text" class="inputs target txtBox rate allownumericdecimal form-control max-w-100" id="rate" name="rate[]" placeholder="" />
                                    <div id="estimated_price" class="padding-box estimation-details"></div>
                                
                            </div>
                            <div class="purchaseitem form-group col-xs-12 col-md-2">
                               
                                    <label>Discount amount :</label>
                                    <input type="text" class="inputs target small_class txtBox sgst allownumericdecimal form-control calculation" id="dis_amount" name="dis_amount" placeholder=""  />
                                    <span id="disp" class="value-label-bottom">0.00</span>(%)
                               
                            </div>
							<div class="clearfix d-tab-block"></div>
                            <div class="purchaseitem form-group col-md-2" id="select_div">
                               
                                    <label>Tax Slab:</label>
                                    <?php
                                    $datas = TaxSlabs::model()->getAllDatas();
                                    ?>
                                    <select class="form-control txtBox tax_slab" id="tax_slab" name="tax_slab[]">
                                        <option value="">Select one</option>
                                        <?php
                                        foreach ($datas as  $value) {
                                            if ($value['set_default'] == 1) {
                                                echo ' <option data-id="' . $value['id'] . '" value="' . $value['tax_slab_value']  . '" selected>' . $value['tax_slab_value'] . '%</option>';
                                            } else {
                                                echo ' <option  data-id="' . $value['id'] . '" value="' . $value['tax_slab_value'] . '">' . $value['tax_slab_value'] . '%</option>';
                                            }
                                        }
                                        ?>
                                    </select>
											<span class="d-mobile-block">&nbsp;</span>
                                
                            </div>
                            <div class="purchaseitem gsts form-group col-md-2">
                                
                                    <label>SGST :</label>
                                    <input type="text" class="inputs target small_class txtBox sgstp allownumericdecimal form-control gst_percentage" id="sgstp" name="sgstp" placeholder="" />
                                    <span id="sgst_amount" class=" value-label-bottom">0.00</span>
                               
                            </div>
                            <div class="purchaseitem gsts form-group col-md-2">
                                
                                    <label>CGST :</label>
                                    <input type="text" class="inputs target txtBox cgstp small_class allownumericdecimal form-control" id="cgstp" name="cgstp" placeholder="" />
                                    <span id="cgst_amount" class="value-label-bottom">0.00</span>
                                
                            </div>
                            <div class="purchaseitem gsts form-group col-md-2">
                                
                                    <label>IGST :</label>
                                    <input type="text" class="inputs target txtBox igstp  small_class allownumericdecimal form-control" id="igstp" name="igstp" placeholder="" />
                                    <span id="igst_amount" class="value-label-bottom">0.00</span>
                               
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-3 bottom-flex">
                        <div class="form-group pull-right total-value-wrapper margin-top-18">
                            <div class="input-info margin-bottom-10">
                            <label class="final-section-label w-100p" >Amount:</label>
                            <span id="item_amount" class="total-value-label w-100p">0.00</span>
                            </div>
                            <div class="input-info margin-bottom-10">
                                <label class="final-section-label w-100p">Discount Amount :</label>
                                <span id="disc_amount" class="total-value-label w-100p">0.00</span>
                            </div>
                            <div class="input-info margin-bottom-10">
                                <label class="final-section-label w-100p">Tax Amount: </label>
                                <span id="tax_amount" class="total-value-label w-100p">0.00</span>
                            </div>
                            <div class="input-info ">
                                <label class="final-section-label w-100p">Total Amount: </label>
                                <span id="total_amount" class="total-value-label w-100p">0.00</span>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="row">
                <div class="purchaseitem form-group col-xs-12 col-md-6">
                        <span id="item_baseunit_data" class="d-none text-blue"><i>Base Unit : </i><span class="txtBox item_unit_span" id="item_unit_additional_data"> </span></span>&nbsp;&nbsp;
                        <span id="item_conversion_data" class="d-none text-blue"><i>Conv Fact : </i><span class="txtBox item_unit_span" id="item_unit_additional_conv"> </span></span>
                </div>
                <div class="form-group col-xs-12 col-md-6 text-right">
                     <input type="button" class="item_save btn btn-info" id="0" value="Save">
                </div>
                <div class="blink hide"><span>Item already added</span></div>
                
                <div class="toastmessage" style="top: 58px;right: 6px;width: 350px;position: fixed;"></div>
            </div>
            <!-- ui changes ends -->
           
            
  
        <!-- </div> -->
<!-- </div> -->
    <style>
    .base-data {

        display: none;
    }
    .p-0{
        padding:0px;
    }
    
    .addRow input.textbox {
		width: 200px;
		display: inline-block;
		margin-right: 10px;
	}
</style>
<script>
 $(document).ready(function(){
    $('.specification').select2();
 }) ;  
 $('.calculation').keyup(function(event) {
        var amount = parseFloat($('#item_amount').html());
        var val = $(this).val();
        if (amount < val) {
            $(".toastmessage").addClass("alert alert-danger").html("Discount should be less than the amount").fadeIn().delay(4000).fadeOut();            
            $(this).val('');
        }
    })
    $("#quantity, #rate,#dis_amount, #sgstp, #cgstp, #igstp").on("blur change", function() {
        var quantity = parseFloat($("#quantity").val());
        var rate = parseFloat($("#rate").val());
        var amount = quantity * rate;
        var new_amount = 0;
        var dis_amount = parseFloat($("#dis_amount").val());
        var sgst = parseFloat($("#sgstp").val());

        if (sgst > 100 || sgst < 0) {
            $(".toastmessage").addClass("alert alert-danger").html("Percentage value should be between 0 and 100").fadeIn().delay(4000).fadeOut();
            // $().toastmessage('showErrorToast', "Percentage value should be between 0 and 100");
            $("#sgstp").val(0);
            sgst = 0;
        }
        var cgst = parseFloat($("#cgstp").val());
        if (cgst > 100 || cgst < 0) {
            $(".toastmessage").addClass("alert alert-danger").html("Percentage value should be between 0 and 100").fadeIn().delay(4000).fadeOut();
            // $().toastmessage('showErrorToast', "Percentage value should be between 0 and 100");
            $("#cgstp").val(0);
            cgst = 0;
        }
        var igst = parseFloat($("#igstp").val());
        if (igst > 100 || igst < 0) {
            $(".toastmessage").addClass("alert alert-danger").html("Percentage value should be between 0 and 100").fadeIn().delay(4000).fadeOut();
            // $().toastmessage('showErrorToast', "Percentage value should be between 0 and 100");
            $("#igstp").val(0);
            igst = 0;
        }
        if (isNaN(dis_amount)) dis_amount = 0;
        if (isNaN(amount)) amount = 0;
        new_amount = amount - dis_amount;
        if (isNaN(new_amount)) new_amount = 0;
        var sgst_amount = (sgst / 100) * new_amount;
        var cgst_amount = (cgst / 100) * new_amount;
        var igst_amount = (igst / 100) * new_amount;
        if (isNaN(sgst_amount)) sgst_amount = 0;
        if (isNaN(cgst_amount)) cgst_amount = 0;
        if (isNaN(igst_amount)) igst_amount = 0;
        var tax_amount = sgst_amount + cgst_amount + igst_amount;
        var total_amount = new_amount + tax_amount;
        var disp = (dis_amount / amount) * 100;
        if (isNaN(disp)) disp = 0;
        $("#disp").html(disp.toFixed(2));
        $("#disc_amount").html(dis_amount.toFixed(2));
        $("#item_amount").html(amount.toFixed(2));
        $("#sgst_amount").html(sgst_amount.toFixed(2));
        $("#cgst_amount").html(cgst_amount.toFixed(2));
        $("#igst_amount").html(igst_amount.toFixed(2));
        $("#tax_amount").html(tax_amount.toFixed(2));
        $("#total_amount").html(total_amount.toFixed(2));
       // debugger;
    });
    $(document).on('click', '.removebtn', function(e) {
        e.preventDefault();
        element = $(this);
        var item_id = $(this).attr('id');
        if (item_id == '0') {
            $('input[name="remove"]').val('removed');
            $(this).closest('tr').find('input[name="amount[]"]').each(function() {
                $(this).closest('tr').remove();
                var re = this.value;
                var v1 = $('input[name="subtot"]').val();
                var v2 = $('input[name="grand"]').val();
                if (re == '') {
                    $('input[name="subtot"]').val(v1);
                    $('input[name="grand"]').val(v2);
                    $('#grand_total').text(v2.toFixed(2));
                } else {
                    $('input[name="subtot"]').val(eval(v1) - eval(re));
                    $('input[name="grand"]').val(eval(v2) - eval(re));
                    $('#grand_total').text((eval(v2) - eval(re)).toFixed(2));
                }
            });
        } else {
            var answer = confirm("Are you sure you want to delete?");
            if (answer) {
                var item_id = $(this).attr('id');
                var $tds = $(this).closest('tr').find('td');
                var amount = $tds.eq(15).text();
                var re = amount;
                var j = parseFloat(amount)
                var v1 = $('input[name="subtot"]').val();
                var v2 = $('input[name="grand"]').val();
                var t = $('#grand_total').text();
                var gamount = parseFloat(t);
                if (j == '') {
                    $('input[name="subtot"]').val(gamount);
                    $('input[name="grand"]').val(gamount);
                    $('#grand_total').text(gamount);
                } else {
                    $('input[name="subtot"]').val((eval(gamount) - eval(j)).toFixed(2));
                    $('input[name="grand"]').val((eval(gamount) - eval(j)).toFixed(2));
                    $('#grand_total').text((eval(gamount) - eval(j)).toFixed(2));
                }
                var purchase_id = $("#purchase_id").val();
                var subtot = $('input[name="subtot"]').val();
                var grand = $('input[name="grand"]').val();
                var data = {
                    'purchase_id': purchase_id,
                    'item_id': item_id,
                    'grand': grand,
                    'subtot': subtot
                };
                $('.loading-overlay').addClass('is-active');
                $.ajax({
                    method: "GET",
                    async: false,
                    data: {
                        data: data
                    },
                    dataType: "json",
                    url: '<?php echo Yii::app()->createUrl('purchase/removepurchaseitem'); ?>',
                    success: function(result) {
                        if (result.response == 'success') {
                            $('.addrow').html(result.html);
                            $('#final_amount').val(result.grand_total);
                            $('#amount_total').text(result.grand_total);
                            $('#discount_total').text(result.discount_total);
                            $('#tax_total').text(result.tax_total);
                            $('#grand_total').text(result.grand_total_amount);
                            $(".toastmessage").addClass("alert alert-success").html(""+result.msg+"").fadeIn().delay(4000).fadeOut();
                            // $().toastmessage('showSuccessToast', "" + result.msg + "");
                            if (result.mail_send == 'Y') {
                                $('.mail_send').show();
                            } else {
                                $('.mail_send').hide();
                            }
                        } else {
                            $(".toastmessage").addClass("alert alert-danger").html(""+result.msg+"").fadeIn().delay(4000).fadeOut();
                            // $().toastmessage('showErrorToast', "" + result.msg + "");
                        }
                        $('#description').val('').trigger('change');
                        $('#remarks').val('');
                        var quantity = $('#quantity').val('');
                        var unit = $('#item_unit').text('');
                        var rate = $('#rate').val('');
                        var amount = $('#item_amount').html('');
                        $('#hsn_code').text('');
                        $('#total_amount').text('');
                        $(".item_save").attr('value', 'Save');
                        $(".item_save").attr('id', 0);
                        $('#description').select2('focus');
                        $('#previousvalue').text('0');
                    }
                });
            } else {
                return false;
            }
        }
    });
    $(document).on("click", ".addcolumn, .removebtn", function() {
        $("table.table  input[name='sl_No[]']").each(function(index, element) {
            $(element).val(index + 1);
            $('.sl_No').html(index + 1);
        });
    });
    $(".inputSwitch span").on("click", function() {
        var $this = $(this);
        $this.hide().siblings("input").val($this.text()).show();
    });
    $(".inputSwitch input").bind('blur', function() {
        var $this = $(this);
        $(this).attr('value', $(this).val());
        $this.hide().siblings("span").text($this.val()).show();
    }).hide();
    $(".allownumericdecimal").keydown(function(event) {
        if (event.shiftKey == true) {
            event.preventDefault();
        }
        if ((event.keyCode >= 48 && event.keyCode <= 57) ||
            (event.keyCode >= 96 && event.keyCode <= 105) ||
            event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 ||
            event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190 || event.keyCode == 110 || event.keyCode == 13) {
            var splitfield = $(this).val().split(".");
            if ((splitfield[1] !== undefined && splitfield[1].length >= 2) && event.keyCode != 8 && event.keyCode != 0 && event.keyCode != 13) {
                event.preventDefault();
            }
        } else {
            event.preventDefault();
        }
        if ($(this).val().indexOf('.') !== -1 && (event.keyCode == 190 || event.keyCode == 110))
            event.preventDefault();
    });
    $('.other').click(function() {
        if (this.checked) {
            $('#autoUpdate').fadeIn('slow');
            $('#select_div').hide();
        } else {
            $('#autoUpdate').fadeOut('slow');
            $('#select_div').show();
        }
    })
    $(document).on("change", ".other", function() {
        if (this.checked) {
            $(this).closest('tr').find('#autoUpdate').fadeIn('slow');
            $(this).closest('tr').find('#select_div').hide();
        } else {
            $(this).closest('tr').find('#autoUpdate').fadeOut('slow');
            $(this).closest('tr').find('#select_div').show();
        }
    });
    $(".specification").select2({
        ajax: {
            url: '<?php echo  Yii::app()->createAbsoluteUrl('purchase/testransaction'); ?>',
            type: "POST",
            dataType: 'json',
            delay: 0,
            data: function(params) {
                return {
                    searchTerm: params.term
                };
            },
            processResults: function(response) {
                $('#previous_details').html(response.html);
                $("#previous_details").show();
                // $("#pre_fixtable3").tableHeadFixer();
                $('#item_unit').text(response.unit);
                $('#hsn_code').text(response.hsn_code);
                return {
                    results: response.data
                };
            },
            cache: true
        }
    });
    $('.specification').change(function() {
        $('#loading').show();
        var element = $(this);
        var category_id = $(this).val();
        var purchase_id = $('#purchase_id').val();
        var itemid_val = $(this).siblings("#itemid_val").val();
        var project_id =$("#project").val();
        $('#quantity').val('');
        $('#rate').val('');
        $('#item_amount').text('0.00');
        $.ajax({
            url: '<?php echo  Yii::app()->createAbsoluteUrl('purchase/previoustransaction'); ?>',
            type: 'GET',
            dataType: 'json',
            data: {
                data: category_id,
                purchase_id: purchase_id,
                itemid_val: itemid_val,
                project_id:project_id,
            },
            success: function(result) {
                if (result.status == 1) {
                    $('#previous_details').html(result.html);
                    $("#previous_details").show();
                    // $("#pre_fixtable").tableHeadFixer();
                    //item estimation
                    var estimated_rate =$("#estimated_rate").val();
                    var estimated_quantity = $("#estimated_quantity").val();
                    var total_po_quantity = $("#total_po_quantity").val();
                    var remaining_qty =0;
                    if(parseFloat(estimated_quantity)>parseFloat(total_po_quantity)){
                        remaining_qty =parseFloat(estimated_quantity) - parseFloat(total_po_quantity);
                    }
                    if(result.estimated_quantity !=''){
                        $("#remaining_estimated_qty").html('Estimated Quantity: '+estimated_quantity);
                        $("#estimated_price").html('Estimated Rate: '+estimated_rate);
                    }else{
                        $("#remaining_estimated_qty").html('');
                        $("#estimated_price").html('');
                    }
                    $("#remaining_estimation_qty").val(estimated_quantity);
                
                } else {
                    $('#previous_details').html(result.html);
                    // $("#pre_fixtable").tableHeadFixer();
                    if(result.estimated_quantity !=''){
                        $("#remaining_estimated_qty").html('Estimated Quantity: '+result.estimated_quantity);
                        $("#estimated_price").html('Estimated Rate: '+result.estimated_rate);
                    }else{
                        $("#remaining_estimated_qty").html('');
                        $("#estimated_price").html('');
                    }
                    $("#remaining_estimation_qty").val(result.estimated_quantity);
                    
                }
                $("#base_unit").val(result.base_unit);
                $('select[id="item_unit"]').empty();
                $("#item_baseunit_data").show();
                $("#item_unit_additional_data").html(result.base_unit);
                var unitval = $.trim($("#unitval").val());
                if (unitval != '') {
                    if (result.unit == result.base_unit) {
                        $('select[id="item_unit"]').append('<option value="' + result.unit + '" selected>' + result.units + '</option>');
                    } else {


                        $.each(result["unit"], function(key, value) {
                            if (unitval == value.value) {
                                var selected = 'selected';
                            } else {
                                var selected = '';
                            }
                            $('select[id="item_unit"]').append('<option value="' + value.id + '" ' + selected + '>' + value.value + '</option>');
                        });
                    }

                } else {
                    if (result.unit == result.base_unit) {


                        $('select[id="item_unit"]').append('<option value="' + result.unit + '" selected>' + result.units + '</option>');
                    } else {
                        $.each(result["unit"], function(key, value) {
                            if (result.base_unit == value.value) {
                                var selected = 'selected';
                            } else {
                                var selected = '';
                            }
                            $('select[id="item_unit"]').append('<option value="' + value.id + '" ' + selected + '>' + value.value + '</option>');
                        });
                    }

                }
                // $('#item_unit').val(result.unit);
                $('#hsn_code').text(result.hsn_code);
                if (category_id == 'other') {
                    $('.remark').css("display", "inline-block");
                    $('#remarks').focus();
                } else if (category_id == '') {
                    $('.js-example-basic-single').select2('focus');
                    $('.remark').css("display", "none");
                } else {
                    $('#quantity').focus();
                    $('.remark').css("display", "none");
                }
                if (result.exist == 1) {
                    $('.blink').removeClass('hide');
                    $('.blink').addClass('d-inline');
                } else {
                    $('.blink').addClass('hide');
                }
            }
        })
    })
     var sl_no = 1;
    var howMany = 0;
    $('.item_save').click(function() {
       
        $("#previous_details").hide();
        var mrId='';
        mrId=$("#mrId").val();
        var itemId='';
        itemId=$("#itemId").val();
        var element = $(this);
        var item_id = $(this).attr('id');
        var project='';
        
        if (item_id == 0) {
            var description = $('.specification').val();
            var remark = $('#remarks').val();
            var quantity = $('#quantity').val();
            var unit = $('#item_unit').text();
            var hsn_code = $('#hsn_code').text();
            var rate = $('#rate').val();
            var amount = $('#item_amount').html();
            var project = $('#project').val();
           // alert(project);
            var vendor = $('#vendor').val();
            var date = $(".date").val();
            var purchaseno = $('#purchaseno').val();
            var expense_head = $('#expense_head').val();
            var company = $('#company_id').val();
            var rowCount = $('.table .addrow tr').length;
            var dis_amount = $('#dis_amount').val();
            var disp = $('#disp').html();
            var cgstp = $('#cgstp').val();
            var cgst_amount = $('#cgst_amount').html();
            var sgstp = $('#sgstp').val();
            var sgst_amount = $('#sgst_amount').html();
            var igstp = $('#igstp').val();
            var igst_amount = $('#igst_amount').html();
            var tax_amount = $('#tax_amount').html();
            var item_amount = $('#item_amount').html()
            var discount_amount = $('#disc_amount').html();
            var total_amount = $('#total_amount').html();
            var tax_slab = $("#tax_slab").val();
            var base_qty = $('#base_qty').val();
            var base_unit = $('#base_unit').val();
            var base_rate = $('#base_rate').val();
            var remaining_estimation_qty = $("#remaining_estimation_qty").val();
            <?php
                if(!empty($mrId)){
                    $materialreq = MaterialRequisition::model()->findByPk($mrId) ;
                    if(!empty($materialreq)){
                    $purchasemodel = Purchase::model()->findByPk($materialreq->purchase_id);
                    if(!empty($purchasemodel)){
                    ?>
                     project = <?php echo isset($purchasemodel->project_id)? $purchasemodel->project_id:'' ; ?>;
                     vendor =<?php echo isset($purchasemodel->vendor_id)? $purchasemodel->vendor_id:'' ; ?>;
                    
                     expense_head =<?php echo isset($purchasemodel->expensehead_id)? $purchasemodel->expensehead_id:'' ; ?>   ;
                     company =<?php echo isset($purchasemodel->expensehead_id)? $purchasemodel->expensehead_id:'' ; ?>   ; 
                    company = <?php echo isset($purchasemodel->company_id)? $purchasemodel->company_id:'' ; ?>;
                    <?php }
                    } ?>
                              
         <?php  }
            ?>
             console.log("Date value: ", date);   
            if (project == '' || vendor == '' || date == '' || purchaseno == '' || expense_head == '' || company == '') {
                $(".toastmessage").addClass("alert alert-danger").html("Please enter purchase details").fadeIn().delay(4000).fadeOut();
                // $().toastmessage('showErrorToast', "Please enter purchase details");
            } else {
                if ((moment(date, 'DD-MM-YYYY', true).isValid())) {

                    if ((description == '' && remark == '') || quantity == '' || rate == '' || amount == 0) {
                        if (amount == 0) {
                            $(".toastmessage").addClass("alert alert-danger").html("Please fill valid quantity and rate").fadeIn().delay(4000).fadeOut();
                            // $().toastmessage('showErrorToast', "Please fill valid quantity and rate");
                        } else {
                            $(".toastmessage").addClass("alert alert-danger").html("Please fill the details").fadeIn().delay(4000).fadeOut();
                            // $().toastmessage('showErrorToast', "Please fill the details");
                        }
                    } else {
                        howMany += 1;
                        if (howMany == 1) {
                            $('#loading').show();
                            
                            var purchase_id = $('input[name="purchase_id"]').val();
                            var subtot = $('#grand_total').text();
                            var grand = $('#grand_total').text();
                            var previousvalue = $('#previousvalue').text();
                            
                            var data = {
                                'sl_no': rowCount,
                                'quantity': quantity,
                                'description': description,
                                'unit': unit,
                                'rate': rate,
                                'amount': amount,
                                'remark': remark,
                                'purchase_id': purchase_id,
                                'grand': grand,
                                'subtot': subtot,
                                'previousvalue': previousvalue,
                                'hsn_code': hsn_code,
                                'dis_amount': dis_amount,
                                'disp': disp,
                                'sgstp': sgstp,
                                'sgst_amount': sgst_amount,
                                'cgstp': cgstp,
                                'cgst_amount': cgst_amount,
                                'igstp': igstp,
                                'igst_amount': igst_amount,
                                'tax_amount': tax_amount,
                                'discount_amount': discount_amount,
                                'total_amount': total_amount,
                                'tax_slab': tax_slab,
                                'base_qty': base_qty,
                                'base_unit': base_unit,
                                'base_rate': base_rate,
                                'project':project,
                                'remaining_estimation_qty':remaining_estimation_qty
                            };
                            $.ajax({
                                url: '<?php echo  Yii::app()->createAbsoluteUrl('purchase/purchaseitem'); ?>',
                                type: 'GET',
                                dataType: 'json',
                                data: {
                                    data: data
                                },
                                success: function(response) {
                                    $('#loading').hide();
                                    if (response.response == 'success') {
                                        $('#final_amount').val(response.final_amount);
                                        $('#amount_total').text(response.grand_total);
                                        $('#discount_total').text(response.discount_total);
                                        $('#tax_total').text(response.tax_total);
                                        $('#grand_total').text(response.grand_total);
                                        $(".toastmessage").addClass("alert alert-success").html("" + response.msg + "").fadeIn().delay(4000).fadeOut();
                                        // $().toastmessage('showSuccessToast', "" + response.msg + "");
                                        $('.addrow').append(response.html);
                                        
                                        if (response.mail_send == 'Y') {
                                            $('.mail_send').show();
                                        } else {
                                            $('.mail_send').hide();
                                        }
                                    } else {
                                        $(".toastmessage").addClass("alert alert-danger").html("" + response.msg + "").fadeIn().delay(4000).fadeOut();
                                        // $().toastmessage('showErrorToast', "" + response.msg + "");
                                    }
                                    howMany = 0;
                                    $('#description').val('').trigger('change');
                                    $('#remarks').val('');
                                    var quantity = $('#quantity').val('');
                                    var unit = $('#item_unit').text('');
                                    var rate = $('#rate').val('');
                                    var amount = $('#item_amount').html('');
                                    $('#hsn_code').text('');
                                    $('#previousvalue').text('0');
                                    $('#description').select2('focus');
                                    $('#dis_amount').val('');
                                    $('#disp').html('0.00');
                                    $('#item_amount').html('0.00');
                                    $('#tax_amount').html('0.00');
                                    $('#sgstp').val('');
                                    $('#sgst_amount').html('0.00');
                                    $('#cgstp').val('');
                                    $('#cgst_amount').html('0.00');
                                    $('#igstp').val('');
                                    $('#igst_amount').html('0.00');
                                    $('#disc_amount').html('0.00');
                                    $('#total_amount').html('0.00');
                                    $('#tax_slab').val("18").trigger('change');
                                    $('.base-data').hide();
                                    $('#item_baseunit_data').hide();
                                    $('#item_conversion_data').hide();
                                    $("#unitval").val('');
                                }
                            });

                            // $('.js-example-basic-single').select2('focus');
                        }
                    }

                } else {

                    $(this).focus();
                    $(".toastmessage").addClass("alert alert-danger").html("Please enter valid date DD-MM-YYYY format").fadeIn().delay(4000).fadeOut();
                    // $().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
                    $(this).focus();
                }
            }
        } else {
            var description = $('.specification').val();
            var remark = $('#remarks').val();
            var quantity = $('#quantity').val();
            var unit = $('#item_unit').text();
            var hsn_code = $('#hsn_code').text();
            var rate = $('#rate').val();
            var amount = $('#item_amount').html();
            var project = $('#project').val();
            var vendor = $('#vendor').val();
            var date = $(".date").val();
            var purchaseno = $('#purchaseno').val();
            var expense_head = $('#expense_head').val();
            var company = $('#company_id').val();
            var dis_amount = $('#dis_amount').val();
            var disp = $('#disp').html();
            var cgstp = $('#cgstp').val();
            var cgst_amount = $('#cgst_amount').html();
            var sgstp = $('#sgstp').val();
            var sgst_amount = $('#sgst_amount').html();
            var igstp = $('#igstp').val();
            var igst_amount = $('#igst_amount').html();
            var tax_amount = $('#tax_amount').html();
            var item_amount = $('#item_amount').html()
            var discount_amount = $('#disc_amount').html();
            var total_amount = $('#total_amount').html();
            var tax_slab = $("#tax_slab").val();
            var base_qty = $('#base_qty').val();
            var base_unit = $('#base_unit').val();
            var base_rate = $('#base_rate').val();
            if (project == '' || vendor == '' || date == '' || purchaseno == '' || expense_head == '' || company == '') {
                $(".toastmessage").addClass("alert alert-danger").html("Please enter purchase details").fadeIn().delay(4000).fadeOut();
                // $().toastmessage('showErrorToast', "Please enter purchase details");
            } else {

                if ((moment(date, 'DD-MM-YYYY', true).isValid())) {
                    if ((description == '' && remark == '') || quantity == '' || rate == '' || amount == 0) {
                        if (amount == 0) {
                            $(".toastmessage").addClass("alert alert-danger").html("Please fill valid quantity and rate").fadeIn().delay(4000).fadeOut();
                            // $().toastmessage('showErrorToast', "Please fill valid quantity and rate");
                        } else {
                            $(".toastmessage").addClass("alert alert-danger").html("Please fill the details").fadeIn().delay(4000).fadeOut();
                            // $().toastmessage('showErrorToast', "Please fill the details");
                        }
                    } else {
                        howMany += 1;
                        if (howMany == 1) {
                            $('#loading').show();
                            var purchase_id = $('input[name="purchase_id"]').val();
                            var subtot = $('#grand_total').text();
                            var grand = $('#grand_total').text();
                            var previousvalue = $('#previousvalue').text();
                            var data = {
                                'item_id': item_id,
                                'sl_no': sl_no,
                                'quantity': quantity,
                                'description': description,
                                'unit': unit,
                                'rate': rate,
                                'amount': amount,
                                'remark': remark,
                                'purchase_id': purchase_id,
                                'grand': grand,
                                'subtot': subtot,
                                'previousvalue': previousvalue,
                                'hsn_code': hsn_code,
                                'dis_amount': dis_amount,
                                'disp': disp,
                                'sgstp': sgstp,
                                'sgst_amount': sgst_amount,
                                'cgstp': cgstp,
                                'cgst_amount': cgst_amount,
                                'igstp': igstp,
                                'igst_amount': igst_amount,
                                'tax_amount': tax_amount,
                                'discount_amount': discount_amount,
                                'total_amount': total_amount,
                                'tax_slab': tax_slab,
                                'base_qty': base_qty,
                                'base_unit': base_unit,
                                'base_rate': base_rate,
                                'project':project
                            };
                            $('.loading-overlay').addClass('is-active');
                            $.ajax({
                                url: '<?php echo  Yii::app()->createAbsoluteUrl('purchase/updatepurchaseitem'); ?>',
                                type: 'GET',
                                dataType: 'json',
                                data: {
                                    data: data
                                },
                                success: function(response) {
                                    $('#loading').hide();
                                    if (response.response == 'success') {
                                        $('#final_amount').val(response.final_amount);
                                        $('#amount_total').text(response.grand_total);
                                        $('#discount_total').text(response.discount_total);
                                        $('#tax_total').text(response.tax_total);
                                        $('#grand_total').text(response.grand_total);
                                        $(".toastmessage").addClass("alert alert-success").html("" + response.msg + "").fadeIn().delay(4000).fadeOut();
                                        // $().toastmessage('showSuccessToast', "" + response.msg + "");
                                        $('.addrow').html(response.html);
                                        
                                        if (response.mail_send == 'Y') {
                                            $('.mail_send').show();
                                        } else {
                                            $('.mail_send').hide();
                                        }
                                    } else {
                                        $(".toastmessage").addClass("alert alert-danger").html("" + response.msg + "").fadeIn().delay(4000).fadeOut();
                                        // $().toastmessage('showErrorToast', "" + response.msg + "");
                                    }
                                    howMany = 0;
                                    $('#description').val('').trigger('change');
                                    $('#remarks').val('');
                                    var quantity = $('#quantity').val('');
                                    var unit = $('#item_unit').text('');
                                    var rate = $('#rate').val('');
                                    var amount = $('#item_amount').html('');
                                    $(".item_save").attr('value', 'Save');
                                    $(".item_save").attr('id', 0);
                                    $('#previousvalue').text('0');
                                    $('#description').select2('focus');
                                    $('#hsn_code').text('');
                                    $('#dis_amount').val('');
                                    $('#disp').html('0.00');
                                    $('#item_amount').html('0.00');
                                    $('#tax_amount').html('0.00');
                                    $('#sgstp').val('');
                                    $('#sgst_amount').html('0.00');
                                    $('#cgstp').val('');
                                    $('#cgst_amount').html('0.00');
                                    $('#igstp').val('');
                                    $('#igst_amount').html('0.00');
                                    $('#disc_amount').html('0.00');
                                    $('#total_amount').html('0.00');
                                    $('#tax_slab').val("18").trigger('change');
                                    $('.base-data').hide();
                                    $('#item_baseunit_data').hide();
                                    $('#item_conversion_data').hide();
                                    $("#unitval").val('');
                                }
                            });

                            $('.js-example-basic-single').select2('focus');
                        }
                    }

                } else {
                    $(this).focus();
                    $(".toastmessage").addClass("alert alert-danger").html("Please enter valid date DD-MM-YYYY format").fadeIn().delay(4000).fadeOut();
                    // $().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
                    $(this).focus();
                }



            }


        }




    });
    $(document).on('click', '.edit_item', function(e) {
        e.preventDefault();
        $('.loading-overlay').addClass('is-active');

        $('.remark').css("display", "inline-block");
        $('#remark').focus();

        var item_id = $(this).attr('id');
        var $tds = $(this).closest('tr').find('td');
        var description = $tds.eq(1).text();
        var hsn_code = $tds.eq(2).text();
        var quantity = $tds.eq(3).text();
        var unit = $tds.eq(4).text();
        var rate = $tds.eq(5).text();
        var base_qty = $.trim($tds.eq(6).text());
        var base_unit = $.trim($tds.eq(7).text());
        var base_rate = $.trim($tds.eq(8).text());
        var amount = $tds.eq(9).text();
        var tax_slab = $tds.eq(10).text();
        var sgst_amount = $tds.eq(11).text();
        var sgstp = $tds.eq(12).text();
        var cgst_amount = $tds.eq(13).text();
        var cgstp = $tds.eq(14).text();
        var igst_amount = $tds.eq(15).text();
        var igstp = $tds.eq(16).text();
        var disc_perc = $tds.eq(17).text();
        var disc_amount = $tds.eq(18).text();
        var tax_amount = $tds.eq(19).text();
        $abc = $(this).closest('tr').find('.item_description').attr('id');
        var des_id = $(this).closest('tr').find('.item_description').attr('id');
        tax_slab = Math.round(tax_slab);

        <?php $abc; ?>

        if (des_id == '0' || des_id == 'other') {
            $('#remarks').val(description);
            $('.remark').css("display", "inline-block");

            $('#remark').focus();
            $('#remarks').focus();
            $('#description').val('other').trigger('change');
        } else {
            $('#description').val(des_id).trigger('change');
            $('.remark').css("display", "none");
            $('.js-example-basic-single').select2('focus');
        }
        $('#quantity').val(parseFloat(quantity))
        $('#hsn_code').text(hsn_code)
        $('#item_amount').html(parseFloat(amount))
        $('#item_amount_temp').val(parseFloat(amount))
        $('#rate').val(parseFloat(rate))
        $(".item_save").attr('value', 'Update');
        $(".item_save").attr('id', item_id);
        $(".item_save").attr('id', item_id);
        $('#item_unit').val(unit);
        $('#base_qty').val(base_qty);
        $('#base_unit').val(base_unit);
        $('#base_rate').val(base_rate);
        $('#dis_amount').val(parseFloat(disc_amount));
        $('#disp').html(parseFloat(disc_perc));
        $('#tax_slab').val(tax_slab).trigger('change');
        $('#sgstp').val(parseFloat(sgstp));
        $('#sgst_amount').html(parseFloat(sgst_amount).toFixed(2));
        $('#cgstp').val(parseFloat(cgstp));
        $('#cgst_amount').html(parseFloat(cgst_amount).toFixed(2));
        $('#igstp').val(parseFloat(igstp));
        $('#igst_amount').html(parseFloat(igst_amount).toFixed(2));
        $('#tax_amount').html(parseFloat(tax_amount))
        $('#disc_amount').html(parseFloat(disc_amount))
        var totalamount = (parseFloat(amount) + parseFloat(tax_amount)) - parseFloat(disc_amount);
        $('#total_amount').html(parseFloat(totalamount).toFixed(2));
        $('.loading-overlay').removeClass('is-active');
    })


    $('.item_save').keypress(function(e) {
        if (e.keyCode == 13) {
            $('.item_save').click();
        }
    });
    $(document).on('mouseover', '.rate_highlight', function(e) {
        var item_id = $(this).attr('id');
        $('#loading-overlay').addClass('is-active');
        $.ajax({
            url: '<?php echo  Yii::app()->createAbsoluteUrl('purchase/previousratedetails'); ?>',
            type: 'GET',
            dataType: 'json',
            data: {
                item_id: item_id
            },
            success: function(result) {
                if (result.status == 1) {
                    $('#previous_details').html(result.html);
                    $("#previous_details").show();
                    // $("#pre_fixtable2").tableHeadFixer();
                    rate_highlight
                } else {
                    $('#previous_details').html(result.html);
                    $("#previous_details").hide();
                }
            }
        })
    })
    $(document).on('mouseout', '.rate_highlight', function(e) {
        $("#previous_details").hide();
    });

    $(document).on('click', '.getprevious', function() {
        var id = $(this).attr('data-id');
        var res = id.split(",");
        var amount = parseFloat(res[4]);
        var tax_value = $('#tax_amount').text();
        if (isNaN(tax_value)) tax_value = 0;
        var disc_value = $('#disc_amount').text();
        if (isNaN(disc_value)) disc_value = 0;
        $('#description').val(res[0]).trigger('change.select2');
        $('#quantity').val(res[1]);
        $('#item_unit').val(res[2]);
        $('#rate').val(res[3]);
        $('#item_amount').text(amount.toFixed(2));
        $('#remarks').val(res[6]);
        $('#dis_amount').val(res[7]);
        $("#disp").html(res[8]);
		$('#tax_slab').val(res[9]);
        $('#sgst_amount').html(res[10]);
        $('#sgstp').val(res[11]).trigger('change');
        $('#cgst_amount').html(res[12]);
		$('#cgstp').val(res[13]).trigger('change');
		$('#igst_amount').html(res[14]);
		$('#igstp').val(res[15]);
        $('#hsn_code').html(res[16]);
        
        
        var total = (res[1] * res[3]); 
        if (isNaN(total)) total = 0;
        var final_amount = parseFloat(total) - parseFloat(disc_value);
        $('#previousvalue').text(total.toFixed(2));
        $('#total_amount').text(final_amount.toFixed(2));
        $('#effective_quantity').val((res[1]*res[5]));
        var type = 1;
        getconversionfactor(type);

    })

    $('#item_unit').change(function() {
        var type = 2;
        getconversionfactor(type);
    });
    $('#quantity,#rate').blur(function() {
        var type = 3;
        getconversionfactor(type);
    });

    function getconversionfactor(type) {

        var itemId = parseInt($("#description").val());
        var base_unit = $("#base_unit").val();
        var amt = $("#item_amount").text();
        var rate = $("#rate").val();
        var quantity = parseFloat($("#quantity").val());
        quantity = isNaN(quantity) ? 0 : quantity;
        var purchase_unit = $("#item_unit option:selected").text();
        quantity = quantity.toFixed(2);
        var basequantity = parseFloat($("#base_qty").val());
        var baserate = 0;
        if (purchase_unit !== '' && base_unit !== '' && itemId !== "") {

            if (base_unit != purchase_unit) {
                // $('.base-data').show();
                $('.base-data').css('display', 'inline-block');
                $.ajax({
                    method: "POST",
                    data: {
                        'purchase_unit': purchase_unit,
                        'base_unit': base_unit,
                        'item_id': itemId
                    },
                    dataType: "json",
                    url: '<?php echo Yii::app()->createAbsoluteUrl('purchase/getUnitconversionFactor'); ?>',
                    success: function(result) {
                        if (result != '') {
                            $("#item_conversion_data").show();
                            $("#item_unit_additional_conv").html(result);
                        }

                        if (type == 2) {
                            base_quantity = result * quantity;
                            base_quantity = base_quantity.toFixed(2);
                        } else if (type == 1) {
                            base_quantity = result * quantity;
                            base_quantity = base_quantity.toFixed(2);
                        } else {
                            base_quantity = basequantity.toFixed(2);
                        }

                        if (isNaN(base_quantity))
                            base_quantity = 0;
                        if (base_quantity != 0) {
                            baserate = amt / base_quantity;
                            baserate = baserate.toFixed(2);
                            if (isNaN(baserate))
                                baserate = 0;
                        }

                        $("#base_qty").val(base_quantity);
                        $("#base_rate").val(baserate);

                    }
                });
            } else {
                $("#item_conversion_data").hide();
                $('.base-data').hide();
                var baseval = 0;
                $("#base_qty").val(quantity);
                $("#base_rate").val(rate);
            }
        }
    }
    $("#rate,#purchase_rate").blur(function() {
                    // alert("jsadhjksd");
        if($(this).parents(".row").find(".sgstp").val() == "" && 
        $(this).parents(".row").find(".cgstp").val() ==""){
            $("#tax_slab").trigger("change");
         }
		            
    });               
    $("#tax_slab").change(function(){                    
        var tax_slab = $(this).val();
         var gst = parseFloat(tax_slab)/2;
        $(this).parents(".row").find(".sgstp").val(gst).trigger("blur");
        $(this).parents(".row").find(".cgstp").val(gst).trigger("blur");
     })
    $(document).on("change",".tax_slab",function(){  
          //alert('hi');               
            var tax_slab = $(this).val();
            var gst = tax_slab/2;
            $(this).parents("tr").find(".sgstp").val(gst).trigger("blur");
            $(this).parents("tr").find(".cgstp").val(gst).trigger("blur");
        });
   
    $('#base_qty').blur(function() {

        var itemId = parseInt($("#description").val());
        var base_unit = $("#base_unit").val();
        var purchase_unit = $("#item_unit option:selected").text();
        var quantity = parseFloat($("#quantity").val());
        quantity = isNaN(quantity) ? 0 : quantity;
        quantity = quantity.toFixed(2);
        var base_quantity = parseFloat($("#base_qty").val());
        var baserate = 0;
        var amt = $("#item_amount").text();
        var rate = $("#rate").val();
        if (purchase_unit !== '' && base_unit !== '' && itemId !== "") {

            if (base_unit != purchase_unit) {
                base_quantity = base_quantity;
                if (isNaN(base_quantity))
                    base_quantity = 0;
                if (base_quantity != 0) {

                    baserate = amt / base_quantity;
                    baserate = baserate.toFixed(2);
                    if (isNaN(baserate))
                        baserate = 0;
                }
                $("#base_qty").val(base_quantity);
                $("#base_rate").val(baserate);
            }

        } else {
            $("#base_qty").val(quantity);
            $("#base_rate").val(rate);
        }

    });

    $('#mail_send').click(function(e) {
        $('.loading-overlay').addClass('is-active');
        var purchase_id = $("#purchase_id").val();
        e.preventDefault();
        $('#loader').show();
        $.ajax({
            url: '<?php echo  Yii::app()->createAbsoluteUrl('purchase/permissionmail'); ?>',
            type: 'GET',
            dataType: 'json',
            data: {
                purchase_id: purchase_id
            },
            success: function(result) {
                if (result == 'success') {
                    $('#loader').hide();
                    $('.mail_send').hide();
                    location.href = '<?php echo  Yii::app()->createAbsoluteUrl('purchase/admin'); ?>';
                } else if (result == 'warning') {
                    $(".toastmessage").addClass("alert alert-warning").html("PO notification Email ID not set").fadeIn().delay(4000).fadeOut();
                    // $().toastmessage('showWarningToast', "PO notification Email ID not set");
                    $('#loader').hide();
                } else {
                    $('#loader').hide();
                    $('.mail_send').hide();
                }
            }
        });
    })


</script>