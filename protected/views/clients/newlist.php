<?php
/* @var $this ExpensesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'Clients',
);
$page = Yii::app()->request->getParam('Clients_page');
if ($page != '') {
    Yii::app()->user->setReturnUrl($page);
} else {
    Yii::app()->user->setReturnUrl(0);
}
$active_modules = ApiSettings::model()->activeModules();
$crm_integration_status = in_array('crm', $active_modules);
$pms_integration_status = in_array('pms', $active_modules);
$integration_status = $pms_integration_status || $crm_integration_status;
$client_map_legend = '';
if ($integration_status) {
    $client_map_legend = '
        $(".dataTables_filter").wrap("<div class=\'custom-table-head-wrap\'></div>");
        $(".custom-table-head-wrap").prepend("<div class=\'legend status-legend\'><span class=\'project_not_mapped\'></span> Unmapped Clients</div>");
    ';
}
?>

<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<div class="container" id="project">
    <div class="loading-overlay">
        <span class="fa fa-spinner fa-3x fa-spin"></span>
    </div>
    <div class="sub-heading margin-bottom-10">
        <h2>Clients</h2>
        <?php
        if ($crm_integration_status != '1' && isset(Yii::app()->user->role) && (in_array('/clients/createclients', Yii::app()->user->menuauthlist)) && !ANCAPMS_LINKED) { ?>
            <a class="createclient btn btn-primary">Add Client</a>
        <?php } ?>
    </div>

    <div id="errMsg"></div>
    <div id="clientform" style="display:none;"></div>
    <div class="">
        <?php
        $this->widget('zii.widgets.CListView', array(
            'dataProvider' => $dataProvider,
            'itemView' => '_newview',
            'template' => '<div class=""><table cellpadding="10" class="table" id="clientab">{items}</table></div>',
            'ajaxUpdate' => false,
            'viewData' => array('integration_status' => $integration_status), // Passing a constant value
        ));
        ?>
        <?php
        Yii::app()->clientScript->registerScript('myjavascript', '
             $(document).ready(function(){
               $("#clientab").dataTable( {
                    "scrollY": "300px",
                    "scrollX": true,
                    "scrollCollapse": true,
                   
                     "columnDefs"        : [       
                        { 
                            "searchable"    : false, 
                            "targets"       : [0,3] 
                        },
                        { "bSortable": false, "aTargets": [-1]  }
                    ],
                } );
                ' . $client_map_legend . '
                });

            ');
        ?>

        <script>
            jQuery(function ($) {
                $('#client').on('keydown', function (event) {
                    if (event.keyCode == 13) {
                        $("#clientsearch").submit();
                    }
                });
            });

            function closeaction() {
                $('#clientform').slideUp();
            }

            function editclient(client_id) {
                $('.loading-overlay').addClass('is-active');
                var id = client_id;
                $.ajax({
                    type: "GET",
                    url: "<?php echo $this->createUrl('clients/update&layout=1&id=') ?>" + id,
                    success: function (response) {
                        $('.loading-overlay').removeClass('is-active');
                        $("#clientform").html(response).slideDown();

                    }
                });

            };
            $(document).ready(function () {
                $('.createclient').click(function () {
                    $('.loading-overlay').addClass('is-active');
                    $.ajax({
                        type: "GET",
                        url: "<?php echo $this->createUrl('clients/create&layout=1') ?>",
                        success: function (response) {
                            $("#clientform").html(response).slideDown();
                            var role = "<?php echo Yii::app()->user->role; ?>";
                            if (role == 3)
                                $("#Clients_company_id_all").click();
                        }
                    });
                });
            });
            $(document).ajaxComplete(function () {
                console.log('ss');
                $('.loading-overlay').removeClass('is-active');
                $('#loading').hide();
            });

            function deletClient(elem, id) {

                if (confirm("Do you want to remove this item?")) {
                    $('.loading-overlay').addClass('is-active');
                    $.ajax({
                        url: '<?php echo Yii::app()->createAbsoluteUrl('clients/removeclient'); ?>',
                        type: 'POST',
                        dataType: 'json',
                        data: {
                            cid: id
                        },
                        success: function (data) {
                            console.log(data);
                            $("#errMsg").show()
                                .html('<div class="alert alert-' + data.response + '">' + data.msg + '</div>')
                                .fadeOut(10000);
                            setTimeout(function () {
                                location.reload(true);
                            }, 3000);

                        }
                    });
                } else {
                    return false;
                }

            }
        </script>



    </div>
</div>



<style>
    .page-body h3 {
        margin: 4px 0px;
        color: inherit;
        text-align: left;
    }

    .panel {
        border: 1px solid #ddd;
    }

    .panel-heading {
        background-color: #eee;
        height: 40px;
    }

    input[type="checkbox"] {
        margin: 11px 3px 0;
    }

    .nonClickable {
        pointer-events: none;
        /* Disable pointer events */
        cursor: not-allowed;
        /* Set cursor to "not-allowed" */
    }
</style>