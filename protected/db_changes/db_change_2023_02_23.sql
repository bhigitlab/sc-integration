
ALTER TABLE `jp_dailyvendors` ADD `purchase_id` INT(11) NULL DEFAULT NULL AFTER `project_id`;
ALTER TABLE `jp_pre_dailyvendors` ADD `purchase_id` INT(11) NULL DEFAULT NULL AFTER `project_id`;

INSERT INTO `jp_menu` (`menu_id`, `menu_name`, `parent_id`, `status`, `controller`, `action`, `params`, `showmenu`, `show_list`, `related`) VALUES ('786', 'dynamicpurchase', '208', '0', 'vendors', 'dynamicpurchase', '', '0', '0', NULL);

ALTER TABLE `jp_pre_dailyvendors` CHANGE `approved_by` `approved_by` INT(11) NULL DEFAULT NULL;

ALTER TABLE `jp_pre_dailyvendors` CHANGE `record_action` `record_action` VARCHAR(300) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;

ALTER TABLE `jp_pre_dailyvendors` CHANGE `cancelled_at` `cancelled_at` DATETIME NULL DEFAULT NULL;
