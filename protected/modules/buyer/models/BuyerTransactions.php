<?php

/**
 * This is the model class for table "{{buyer_transactions}}".
 *
 * The followings are the available columns in table '{{buyer_transactions}}':
 * @property integer $id
 * @property integer $company_id
 * @property integer $project_id
 * @property integer $buyer_id
 * @property integer $flat_id
 * @property integer $transaction_for
 * @property string $invoice_no
 * @property integer $transaction_type

 * @property double $total_amount
 * @property double $amount
 * @property double $sgst_p
 * @property double $sgst_val
 * @property double $cgst_p
 * @property double $cgst_val
 * @property double $igst_p
 * @property double $igst_val
 * @property string $description
 * @property double $tds_p
 * @property double $tds_val
 * @property integer $bank_id
 * @property string $cheque_no
 * @property integer $created_by
 * @property integer $updated_by
 * @property string $created_date
 * @property string $updated_date
 *
 * The followings are the available model relations:
 * @property LedgerList $bank
 * @property Buyers $buyer
 * @property Company $company
 * @property Users $createdBy
 * @property ProjectFlatNumbers $flat
 * @property LedgerList $fromTransactionHead
 * @property Projects $project
 * @property LedgerList $toTransactionHead
 * @property Users $updatedBy
 */
class BuyerTransactions extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{buyer_transactions}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('company_id,project_id, buyer_id, flat_id, transaction_for,amount', 'required'),
			array('company_id, project_id, buyer_id, flat_id, transaction_for, transaction_type,  bank_id, created_by, updated_by', 'numerical', 'integerOnly' => true),
			array('total_amount,amount, sgst_p, sgst_val, cgst_p, cgst_val, igst_p, igst_val, tds_p, tds_val', 'numerical'),
			array('invoice_no, cheque_no', 'length', 'max' => 100),
			array('description, created_date, updated_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, company_id, project_id, buyer_id, flat_id, transaction_for, invoice_no, transaction_type,  total_amount, sgst_p, sgst_val, cgst_p, cgst_val, igst_p, igst_val, description, tds_p, tds_val, bank_id, cheque_no, created_by, updated_by, created_date, updated_date', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'company' => array(self::BELONGS_TO, 'Company', 'company_id'),
			'project' => array(self::BELONGS_TO, 'Projects', 'project_id'),
			'buyer' => array(self::BELONGS_TO, 'Buyers', 'buyer_id'),
			'flat' => array(self::BELONGS_TO, 'ProjectFlatNumbers', 'flat_id'),
			'transactionHead' => array(self::BELONGS_TO, 'Status', 'transaction_type'),

			'bank' => array(self::BELONGS_TO, 'Bank', 'bank_id'),
			'createdBy' => array(self::BELONGS_TO, 'Users', 'created_by'),
			'updatedBy' => array(self::BELONGS_TO, 'Users', 'updated_by'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'company_id' => 'Company',
			'project_id' => 'Project',
			'buyer_id' => 'Buyer',
			'flat_id' => 'Flat',
			'transaction_for' => 'Transaction Type',
			'invoice_no' => 'Invoice No',
			'transaction_type' => 'From Transaction Head',

			'total_amount' => 'total_amount',
			'amount' => 'Amount',
			'sgst_p' => 'Sgst P',
			'sgst_val' => 'Sgst Val',
			'cgst_p' => 'Cgst P',
			'cgst_val' => 'Cgst Val',
			'igst_p' => 'Igst P',
			'igst_val' => 'Igst Val',
			'description' => 'Description',
			'tds_p' => 'Tds P',
			'tds_val' => 'Tds Val',
			'bank_id' => 'Bank',
			'cheque_no' => 'Cheque No',
			'created_by' => 'Created By',
			'updated_by' => 'Updated By',
			'created_date' => 'Created Date',
			'updated_date' => 'Updated Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('company_id', $this->company_id);
		$criteria->compare('project_id', $this->project_id);
		$criteria->compare('buyer_id', $this->buyer_id);
		$criteria->compare('flat_id', $this->flat_id);
		$criteria->compare('transaction_for', $this->transaction_for);
		$criteria->compare('invoice_no', $this->invoice_no, true);
		$criteria->compare('transaction_type', $this->transaction_type);

		$criteria->compare('total_amount', $this->total_amount);
		$criteria->compare('amount', $this->amount);
		$criteria->compare('sgst_p', $this->sgst_p);
		$criteria->compare('sgst_val', $this->sgst_val);
		$criteria->compare('cgst_p', $this->cgst_p);
		$criteria->compare('cgst_val', $this->cgst_val);
		$criteria->compare('igst_p', $this->igst_p);
		$criteria->compare('igst_val', $this->igst_val);
		$criteria->compare('description', $this->description, true);
		$criteria->compare('tds_p', $this->tds_p);
		$criteria->compare('tds_val', $this->tds_val);
		$criteria->compare('bank_id', $this->bank_id);
		$criteria->compare('cheque_no', $this->cheque_no, true);
		$criteria->compare('created_by', $this->created_by);
		$criteria->compare('updated_by', $this->updated_by);
		$criteria->compare('created_date', $this->created_date, true);
		$criteria->compare('updated_date', $this->updated_date, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return BuyerTransactions the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function getInvoiceNo($inv_no)
	{
		if (!empty($inv_no)) {
			$buyer_invoice_model = BuyerInvoice::model()->findByPk($inv_no);
			return $buyer_invoice_model->invoice_no;
		}
	}

	// public function getTransactionHead($data)
	// {
	// 	$transaction_to_head = $transaction_from_head = '';
	// 	if ($data->transaction_for == '1' || $data->transaction_for == '2') {

	// 		$transaction_to_head = $status_model->caption;
	// 	} else {
	// 		$status_model = Status::model()->findByPk($data->transaction_type);
	// 		$transaction_from_head = $status_model->caption;


	// 		$transaction_to_head = $expense_type_model->type_name;
	// 	}
	// 	return array('to_head' => $transaction_to_head, 'from_head' => $transaction_from_head);
	// }

	public function getBuyerTransactions()
	{
		$final_array = array();
		$datas = BuyerTransactions::model()->findAll();
		foreach ($datas as $data) {
			$expense = $receipt = $advance = 0;
			if (!array_key_exists($data['buyer_id'], $final_array)) {
				$final_array[$data['buyer_id']]['buyer_name'] = $data->buyer->name;
				$final_array[$data['buyer_id']]['company'] = $data->company->name;
				$final_array[$data['buyer_id']]['project'] = $data->project->name;
				if ($data->transaction_for == 1) {
					$advance = $data->total_amount;
				} elseif ($data->transaction_for == 2) {
					$receipt = $data->total_amount;
				} elseif ($data->transaction_for == 3) {
					$expense = $data->total_amount;
				}
				$final_array[$data['buyer_id']]['expense'] = $expense;
				$final_array[$data['buyer_id']]['receipt'] = $receipt;
				$final_array[$data['buyer_id']]['advance'] = $advance;
			} else {
				if ($data->transaction_for == 1) {
					$advance = $data->total_amount;
				} elseif ($data->transaction_for == 2) {
					$receipt = $data->total_amount;
				} elseif ($data->transaction_for == 3) {
					$expense = $data->total_amount;
				}
				$final_array[$data['buyer_id']]['expense'] += $expense;
				$final_array[$data['buyer_id']]['receipt'] += $receipt;
				$final_array[$data['buyer_id']]['advance'] += $advance;
			}
		}
		return $final_array;
	}
}
