<?php
	$tblpx= Yii::app()->db->tablePrefix;
	$company = Yii::app()->user->company_id;
	$companyInfo = Company::model()->findByPK($company);
	$companyName = $companyInfo["name"];
	$companyGst = $companyInfo["company_gstnum"];
	$item_model = WarehousereceiptItems::model()->findAll(array("condition" => "warehousereceipt_id = '$model->warehousereceipt_id'"));
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Receipt Pdf</title>
		<link href="https://fonts.googleapis.com/css?family=Diplomata+SC|Limelight" rel="stylesheet">
		<style>
			.container{
				max-width: 850px;
				margin:0 auto;
			}
			table{width: 100%;border-collapse:collapse;
				font-size: 12px;
			}
			table td, table th{
				padding: 8px 0px;
			}
			.item_table td, .item_table th{
				border: 1px solid #222;
				padding: 8px;
				text-align:center;
			}
			.item_table tr:last-child td{
				border: 1px solid transparent !important;
			}
			h2{
				font-family: 'Limelight', cursive;
			}
			h2,h4{
				text-align:center;
				text-transform:uppercase;
			}
			td.ph_text,td.date_text,td.company_text{
				text-align:right;
			}
			b.receipt_text{
				color: red;
				font-size: 18px;
			}			
			b.text-uppercase{
				text-transform:uppercase;			
			}
		</style>
	</head>
	<body>
		<div class="container">
			<table>
				<tr>
					<td>GSTIN:<b> <?php echo isset($companyGst)?$companyGst:""; ?></b></td>					
					<td class="ph_text" colspan="4">Phone:<b> <?php echo isset($companyInfo['phone'])?$companyInfo['phone']:""; ?></b></td>
				</tr>
			</table>			
			<div>
				<h2><?php echo $companyInfo['name'] ?></h2>
				<h4><?php  echo $warehouse_address;?></h4>
			</div>
			<table>
				<tr>
					<td>RECEIPT NOTE NO.<b class="receipt_text"> <?php echo $model->warehousereceipt_no; ?></b></td>
					<td class="date_text">Date:<b> <?php echo date("d-m-Y", strtotime($model->warehousereceipt_date)); ?></b></td>
				</tr>	
				<tr>
					<td colspan="2">Stock of Goods Received From: <b><?php echo ($model->warehousereceiptwarehouseidfrom['warehouse_name'] !="")?$model->warehousereceiptwarehouseidfrom['warehouse_name']:$model->warehousereceiptBillId['bill_number']; ?></b>
					</td>
				</tr>
			</table>
			<table class="item_table">
				<tr>
					<th style="width: 40px;">Sl No</th>
					<th>Item</th>
					<th style="width: 100px;">Dimension</th>
					<th style="width: 100px;">Batch</th>
					<th style="width: 100px;">Unit/Size</th>
					<th style="width: 100px;">Quantity</th>
					<th style="width: 100px;">Rate</th>
					<th style="width: 100px;">Amount</th>
					<th style="width: 100px;">Accepted Quantity</th>
					<th style="width: 100px;">Rejected Quantity</th>
				</tr>
				<?php
					if(!empty($item_model)){
					foreach($item_model as $key=> $values){
					$specification = Yii::app()->db->createCommand("SELECT id, parent_id,brand_id, specification, unit FROM {$tblpx}purchase_category WHERE id=" . $values['warehousereceipt_itemid'] . "")->queryRow();
					$parent_category = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}purchase_category WHERE id='" . $specification['parent_id'] . "'")->queryRow();

					if ($specification['brand_id'] != NULL) {
					$brand_details = Yii::app()->db->createCommand("SELECT brand_name FROM {$tblpx}brand WHERE id=" . $specification['brand_id'] . "")->queryRow();
					$brand = '-' . $brand_details['brand_name'];
					} else {
					$brand = '';
					}
					$spc_details = $parent_category['category_name'] . $brand . ' - ' . $specification['specification'];
					if($values['dimension'] != null){
						$multiplying_quantity_arr = explode(" x ",$values['dimension']);
						$multiplying_quantity = $multiplying_quantity_arr['0'] * $multiplying_quantity_arr['1'];
					}else{
						$multiplying_quantity =1;
					}
					$amount = $values['warehousereceipt_quantity']*$values['warehousereceipt_rate']*$multiplying_quantity;
				?>
				<tr>
					<td><?php echo ($key+1); ?></td>
					<td style="text-align:justify;"><?php echo $spc_details; ?></td>
					<td><?php echo ($values['dimension'] !='')?"(". $values['dimension']. ")":"N/A"; ?></td>
					<td><?php echo $values['warehousereceipt_batch']; ?></td>
					<td><?php echo $values['warehousereceipt_unit']; ?></td>
					<td><?php echo $values['warehousereceipt_quantity']; ?></td>
					<td><?php echo Controller::money_format_inr($values['warehousereceipt_rate'], 2, 1); ?></td>
					<td><?php echo Controller::money_format_inr($amount, 2, 1); ?></td>
					<td><?php echo $values['warehousereceipt_accepted_quantity']; ?></td>
					<td><?php echo $values['warehousereceipt_rejected_quantity']; ?></td>			
				</tr>
				<?php
					}
				?>
				<?php
					} else{
				?>	
				<tr>
					<td colspan="10">No Item found</td>
				</tr>
				<?php
					} 
				?>				
			</table>
			<table>
				<tr>
					<td>Receipt Clerk:<b><?php echo $model->clerk->first_name.' '.$model->clerk->last_name ?></b></td>					
					<td colspan="4" class="company_text">For <b class="text-uppercase"><?php echo $companyName; ?></b></td>
				</tr>
			</table>		
		</div>
	</body>
</html>
