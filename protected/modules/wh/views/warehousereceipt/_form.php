<?php
/* @var $this WarehousereceiptController */
/* @var $model Warehousereceipt */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'warehousereceipt-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'warehousereceipt_no'); ?>
		<?php echo $form->textField($model,'warehousereceipt_no'); ?>
		<?php echo $form->error($model,'warehousereceipt_no'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'warehousereceipt_date'); ?>
		<?php echo $form->textField($model,'warehousereceipt_date'); ?>
		<?php echo $form->error($model,'warehousereceipt_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'warehousereceipt_vendorid'); ?>
		<?php echo $form->textField($model,'warehousereceipt_vendorid'); ?>
		<?php echo $form->error($model,'warehousereceipt_vendorid'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'warehousereceipt_stockid'); ?>
		<?php echo $form->textField($model,'warehousereceipt_stockid'); ?>
		<?php echo $form->error($model,'warehousereceipt_stockid'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'warehousereceipt_clark'); ?>
		<?php echo $form->textField($model,'warehousereceipt_clark'); ?>
		<?php echo $form->error($model,'warehousereceipt_clark'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'created_by'); ?>
		<?php echo $form->textField($model,'created_by'); ?>
		<?php echo $form->error($model,'created_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'created_date'); ?>
		<?php echo $form->textField($model,'created_date'); ?>
		<?php echo $form->error($model,'created_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'updated_by'); ?>
		<?php echo $form->textField($model,'updated_by'); ?>
		<?php echo $form->error($model,'updated_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'updated_date'); ?>
		<?php echo $form->textField($model,'updated_date'); ?>
		<?php echo $form->error($model,'updated_date'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->