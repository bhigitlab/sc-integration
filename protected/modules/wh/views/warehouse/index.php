<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">

<?php
/* @var $this ExpensesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'Clients',
);
$page = Yii::app()->request->getParam('Clients_page');
Yii::app()->user->setReturnUrl(0);

if ($page != '') {
    Yii::app()->user->setReturnUrl($page);
}
?>


<div class="container" id="project">
    <div class="expenses-heading header-container">
        <h2>Warehouse</h2>
        <div class="btn-container">
            <?php if ((isset(Yii::app()->user->role) && (in_array('/wh/warehouse/create', Yii::app()->user->menuauthlist)))) { ?>
                <a class="btn btn-info addclient">Add Warehouse</a>
                <a href="index.php?r=wh/warehouse/unassignedwarehouse" class="btn btn-info unassigned">Unassigned
                    Warehouse</a>
            <?php } ?>
        </div>
    </div>
    <div id="addclient" style="display:none;">

    </div>
    <div id="msg"></div>
    <div class="table-wrapper margin-top-20">
        <?php
        $this->widget('zii.widgets.CListView', array(
            'dataProvider' => $dataProvider,
            'itemView' => '_view',
            'template' => '<div>{sorter}</div>
            <div>
            <table cellpadding="10" id="myTable" class="table">{items}</table></div>',
        ));
        ?>
    </div>
    <script>
        function closeaction() {
            $('#addclient').slideUp('slow');
        }
        $(document).ready(function () {


            $('.addclient').click(function () {

                $.ajax({
                    type: "POST",
                    url: "<?php echo $this->createUrl('warehouse/create&layout=1') ?>",
                    success: function (response) {
                        $("#addclient").html(response).slideDown();
                    }
                });
            });

            $(document).delegate('.editWarehouse', 'click', function () {
                var id = $(this).attr('data-id');
                $.ajax({
                    type: "POST",
                    url: "<?php echo $this->createUrl('warehouse/update&id=') ?>" + id,
                    success: function (response) {
                        $("#addclient").html(response).slideDown();
                    }
                });
            });

            $(document).delegate('.deleteWarehouse', 'click', function () {
                var answer = confirm("Are you sure you want to delete?");
                if (answer) {
                    var id = $(this).attr('data-id');
                    $.ajax({
                        type: "POST",
                        dataType: "json",
                        url: "<?php echo $this->createUrl('warehouse/delete&id=') ?>" + id,
                        success: function (response) {
                            $("#msg").html("<div class='alert alert-" + response.response + "'>" + response.msg + "</div>");
                            setTimeout(() => {
                                location.reload();
                            }, 2000);

                        }
                    });
                }
            });

            jQuery(function ($) {
                $('#client').on('keydown', function (event) {
                    if (event.keyCode == 13) {
                        $("#clientsearch").submit();
                    }
                });
            });
        });
    </script>
<?php
    Yii::app()->clientScript->registerScript('myjavascript', '
    $(document).ready(function() {
        $("#myTable").dataTable({
            "scrollY": "300px",
            "scrollX": true,
            "scrollCollapse": true,
            "columnDefs": [
                { "targets": [0], "searchable": false },
                { "bSortable": false, "aTargets": [-1] }
            ],
        });
    });
    ');
?>