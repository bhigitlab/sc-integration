<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$this->breadcrumbs = array(
    'Purchase List',
)
    ?>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/message/css/jquery.message.css">
<script type="text/javascript"
    src="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/message/js/jquery.message.js"></script>

<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/tableHeadFixer.js"></script>
<div class="container" id="project">
    <div class="sub-heading mb-10">
        <h3>Warehouse Despatch</h3>
        <a href="<?php echo Yii::app()->createAbsoluteUrl('wh/warehousedespatch/create') ?>"
            class="button btn btn-info btn-sm">Add Despatch</a>
    </div>
    <?php $this->renderPartial('_newsearch', array('model' => $model, 'warehouse' => $warehouse, 'vendor' => $vendor, 'clerk' => $clerk, 'specification' => $specification, 'item_id' => $item_id)) ?>
    <?php if (Yii::app()->user->hasFlash('success')): ?>
        <div class="info"
            style="width: 500px;margin-left: 400px;padding: 8px;color: green;font-size: 20px;font-weight: bold; padding-top: 46px;">
            <?php echo Yii::app()->user->getFlash('success'); ?>
        </div>
    <?php endif; ?>


    <?php if (Yii::app()->user->hasFlash('error')): ?>
        <div class="info"
            style="width: 500px;margin-left: 400px;padding: 8px;color: red;font-size: 20px;font-weight: bold; padding-top: 46px;">
            <?php echo Yii::app()->user->getFlash('error'); ?>
        </div>
    <?php endif; ?>


    <div style="">
        <div id="msg_box"></div>
        <div class="exp-list">
            <?php $this->widget('zii.widgets.CListView', array(
                'dataProvider' => $dataProvider,
                'itemView' => '_newview',
                'template' => '<div class="table-wrapper"><div class="sub-heading">{sorter}<div class="text-right margin-left-auto">{summary}</div></div><div class="report-table-wrapper" id="parent"><table cellpadding="10" class="table  list-view " id="fixtable">{items}</table></div>{pager}',
                'emptyText' => '<table cellpadding="10" class="table"><thead class="entry-table">
            <tr>
            <th>No</th>
            <th>Receipt No</th>
            <th>Warehouse Name</th>
            <th>Goods Despatched To</th>
            <th>Clerk</th>
            <th>Date</th>
            <th></th>
             </tr></thead><tr><td colspan="13" class="text-center">No results found</td></tr></table>'
            )); ?>

        </div>
    </div>

    <style>
        #fixtable thead th,
        #fixtable tfoot th {
            background-color: #eee;
            z-index: 1;
        }

        .table a.link {
            color: #015986db;
        }

        .toast-type-error {
            background-color: #B32B2B;
        }

        .toast-type-success {
            background-color: #00A65A;
        }

        a.button {
            background-color: #6a8ec7;
            display: block;
            padding: 5px;
            color: #fff;
            cursor: pointer;
            float: right;
            border: 1px solid #6a8ec8;
        }

        .bill_class {
            background: #FF6347 !important;
        }

        .exp-list {
            padding: 0px;
        }

        .page-body h3 {
            margin: 4px 0px;
            color: inherit;
            text-align: left;
        }

        tfoot th {
            background-color: #eee;
        }

        .panel {
            border: 1px solid #ddd;
            box-shadow: 0px 0px 12px 1px rgba(0, 0, 0, 0.25);
        }

        .panel-heading {
            background-color: #6A8EC7;
            height: 40px;
            color: #fff;
        }

        .panel label {
            display: block;
        }

        .head_remarks {
            padding-left: 5px;
        }

        .ind_remarks {
            padding: 5px;
            font-size: 13px;
        }

        .ind_remarks p:first-child {
            font-size: 11px;
        }

        .ind_remarks p:last-child {
            background: #f6f6f6;
            padding: 5px;
            margin-top: -5px;
        }

        .pur_remark {
            font-weight: 600;
            vertical-align: middle !important;
            white-space: nowrap;
            position: relative;
        }

        .cursor-pointer {
            cursor: pointer;
        }

        .pur_remark .icon {}

        .form-head {
            position: relative;
        }



        #addremark .all_remarks {
            overflow: auto;
            margin-top: 5px;
            max-height: 150px;
            border-top: 1px solid #ddd;
        }

        .badge1 {
            /*color: #555;background-color: #ccc;*/
            position: absolute;
            right: 0px;
            top: 0px;
            color: #0093dd;
            font-weight: 900;
            font-size: 11px;
        }

        .addnow {
            cursor: pointer;
            font-size: 14px;
            float: right;
        }

        .highlight {
            background-color: #DD1035;
            color: #fff;
        }

        #parent {
            max-height: 350px;
        }

        #parent .table {
            margin-bottom: 0px;
        }

        .list-view .sorter li {
            margin: 0px !important;
        }

        .list-view .pager {
            margin: 30px 0 0 0;
        }

        .toast-container {
            width: 350px;
        }

        .toast-position-top-right {
            top: 57px;
            right: 6px;
        }

        .toast-item-close {
            background-image: none;
            cursor: pointer;
            width: 12px;
            height: 12px;
            text-align: center;
            border-radius: 2px;
        }

        .toast-item-image {
            font-size: 24px;
        }

        .toast-item-close:hover {
            color: red;
        }

        .toast-item {
            border: transparent;
            border-radius: 3px;
            font-size: 10px;
            opacity: 1;
            background-color: rgba(34, 45, 50, 0.8);
            box-shadow: 0px 0px 0px 0px rgba(34, 45, 50, 0.8);
        }

        .table a.link_deleted {
            color: #da3727 !important;
        }

        .tooltip-hiden {
            width: auto;
        }

        .toast-item-wrapper p {
            margin: 0px 5px 0px 42px;
            font-size: 14px;
            text-align: justify;
        }

        .toast-type-success {
            background-color: #00A65A;
            border-color: #00A65A;
        }

        .toast-type-error {
            background-color: #DD4B39;
            border-color: #DD4B39;
        }

        .toast-type-notice {
            background-color: #00C0EF;
            border-color: #00C0EF;
        }

        .toast-type-warning {
            background-color: #F39C12;
            border-color: #F39C12;
        }

        @media(min-width: 1400px) {
            #addremark .all_remarks {
                max-height: 300px;
            }
        }
    </style>


    <script>
        $('div#addr').click(function () {
            $('.remarkadd').hide();
            $('.addnow').show();
        });

        $('.addnow').click(function () {
            $(this).hide();
            $('.closermrk').show();
            $('.remarkadd').show();
        });
        $('.closermrk').click(function () {
            $(this).hide();
            $('.addnow').show();
            $('.remarkadd').hide();
            $('#remark').val('');
        });

        function closeaction() {
            $('#remark').val('');
            $('#addremark').hide("slide", {
                direction: "right"
            }, 200);
        }

        function addremark(pur_order, event) {
            var id = pur_order;
            $(".popover").removeClass("in");
            $('.remarkadd').show();
            $('.addnow').hide();
            $('#addremark').show("slide", {
                direction: "right"
            }, 500);
            $("#txtPurchaseId").val(id);
            event.preventDefault();
            $.ajax({
                type: "GET",
                data: {
                    purchaseid: id
                },
                dataType: 'json',
                url: '<?php echo Yii::app()->createUrl('purchase/viewremarks'); ?>',
                success: function (response) {
                    $("#remarkList").html(response.result);
                    $(".po_no").html(response.po_no)
                    $("#txtPurchaseId").val(id);
                }
            });

        }
        var url = '<?php echo Yii::app()->getBaseUrl(true); ?>';

        $(document).ready(function () {
            $('.update_status').click(function (e) {
                e.preventDefault();
                var element = $(this);
                var answer = confirm("Are you sure you want to change the status?");
                if (answer) {
                    var purchase_id = $(this).attr('id');
                    $.ajax({
                        url: '<?php echo Yii::app()->createAbsoluteUrl('purchase/updatepurchasestatus'); ?>',
                        type: 'GET',
                        dataType: 'json',
                        data: {
                            purchase_id: purchase_id
                        },
                        success: function (response) {
                            console.log(response);
                            if (response.response == 'success') {
                                $(".purchase_status_" + purchase_id).html('Saved');
                                $(".edit_option_" + purchase_id).hide();
                                element.closest('tr').find('.bill_label').text('Pending to be Billed');
                                element.closest('tr').find('.bill_status').addClass('bill_class');
                                $("#msg_box").html('<div class="alert alert-success alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><strong> Success!</strong>  &nbsp;' + response.msg + '</div>');
                            } else {

                                $("#msg_box").html('<div class="alert alert-warning alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><strong> Sorry!</strong>  &nbsp;' + response.msg + '</div>');
                            }
                        }
                    });
                } else {
                    return false;
                }
            });


        });

        $(document).on('click', '.edit_purchase', function () {
            var val = $(this).attr('id');
            $.ajax({
                method: "GET",
                data: {
                    despatch_id: val
                },
                dataType: "json",
                url: '<?php echo Yii::app()->createUrl('wh/warehousedespatch/CheckEditDespatch'); ?>',
                success: function (result) {
                    if (result.response == 'success') {
                        location.href = url + '/index.php?r=wh/warehousedespatch/update&despatchid=' + val;
                    } else {
                        $(this).attr("href", "#");
                        //  $("#msg_box").html('<div class="alert alert-danger alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><strong> Error!</strong>  &nbsp;'+result.msg+' </div>').fadeOut(5000);

                        $().toastmessage('showErrorToast', "" + result.msg + "");
                    }

                }
            });
        });


        $(document).on('click', '.view_purchase', function () {
            var val = $(this).attr('id');
            location.href = url + '/index.php?r=warehousedespatch/view&despatchid=' + val;
        });

        function myfunction(elem) {
            var id = "";
            id = $(elem).attr('id');
            var element = elem;
            if (confirm('Are you sure you want to delete this?')) {
                $.ajax({
                    url: '<?php echo Yii::app()->createAbsoluteUrl('purchase/deletepurchase'); ?>',
                    type: 'GET',
                    dataType: 'json',
                    data: {
                        purchase_id: id
                    },
                    success: function (response) {
                        console.log(response);
                        if (response == 1) {
                            $("#msg_box").html('<div class="alert alert-success alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><strong> Success!</strong>  &nbsp;Deleted successfully </div>').fadeOut(5000);
                            element.closest('tr').remove();
                            setTimeout(
                                function () {
                                    //                         location.reload();

                                }, 500);
                        } else {
                            $("#msg_box").html('<div class="alert alert-warning alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><strong> Sorry!</strong>  &nbsp; Some Problem occured</div>');
                        }
                    }

                });

            }
        }

        $(document).on('click', '.removebtn', function (e) {
            e.preventDefault();
            element = $(this);
            var item_id = $(this).attr('id');
            var answer = confirm("Are you sure you want to delete?");
            if (answer) {

                var despatch_id = $("#warehousedespatch_id").val();
                var data = {
                    'despatch_id': despatch_id,
                    'item_id': item_id
                };
                $.ajax({
                    method: "GET",
                    async: false,
                    data: {
                        data: data
                    },
                    dataType: "json",
                    url: '<?php echo Yii::app()->createUrl('wh/warehousedespatch/removeitem'); ?>',
                    success: function (result) {
                        if (result.response == 'success') {
                            $('.addrow').html(result.html);
                            $('#final_amount').val(result.grand_total);
                            $('#grand_total').text(result.grand_total);
                            $().toastmessage('showSuccessToast', "" + result.msg + "");
                        } else {
                            $().toastmessage('showErrorToast', "" + result.msg + "");
                        }
                        $('#description').val('').trigger('change');
                        $('#remarks').val('');
                        var quantity = $('#quantity').val('');
                        var unit = $('#item_unit').text('');
                        var rate = $('#rate').val('');
                        var amount = $('#item_amount').html('');
                        $(".item_save").attr('value', 'Save');
                        $(".item_save").attr('id', 0);
                        $('#description').select2('focus');
                    }
                });

            } else {

                return false;
            }
        });

        $(document).ready(function () {
            $().toastmessage({
                sticky: false,
                position: 'top-right',
                inEffectDuration: 1000,
                stayTime: 3000,
                closeText: '<i class="icon ion-close-round"></i>',
            });


            //$("#fixtable").tableHeadFixer({'left' : false, 'foot' : true, 'head' : true});
            $(".popover-test").popover({
                html: true,
                content: function () {
                    //return $('#popover-content').html();
                    return $(this).next('.popover-content').html();
                }
            });
            $('[data-toggle=popover]').on('click', function (e) {
                $('[data-toggle=popover]').not(this).popover('hide');
            });
            $('body').on('hidden.bs.popover', function (e) {
                $(e.target).data("bs.popover").inState.click = false;
            });
            $('body').on('click', function (e) {
                $('[data-toggle=popover]').each(function () {
                    // hide any open popovers when the anywhere else in the body is clicked
                    if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                        $(this).popover('hide');
                    }
                });
            });
            $(document).ajaxComplete(function () {
                //$("#fixtable").tableHeadFixer({'left' : false, 'foot' : true, 'head' : true});
                $(".popover-test").popover({
                    html: true,
                    content: function () {
                        return $(this).next('.popover-content').html();
                    }
                });

            });

        });
    </script>