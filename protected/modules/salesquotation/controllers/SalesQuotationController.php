<?php

class SalesQuotationController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        $accessArr = array();
        $accessauthArr = array();
        $controller = 'salesquotation/' . Yii::app()->controller->id;

        if (isset(Yii::app()->user->menuall)) {
            if (array_key_exists($controller, Yii::app()->user->menuall)) {
                $accessArr = Yii::app()->user->menuall[$controller];
            }
        }

        if (isset(Yii::app()->user->menuauth)) {
            if (array_key_exists($controller, Yii::app()->user->menuauth)) {
                $accessauthArr = Yii::app()->user->menuauth[$controller];
            }
        }
        $access_privlg = count($accessauthArr);
        return array(
            array(
				'allow', // allow all users to access 'SaveQuotationDuplicate' action
				'actions' => array('SaveQuotationDuplicate'),
				'users' => array('*'), // allow access to all users
			),
            array(
                'allow', // allow all users to perform 'index' and 'view' actions
                'actions' => $accessArr,
                'users' => array('@'),
                'expression' => "$access_privlg > 0",
            ),
            array(
                'allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => $accessauthArr,
                'users' => array('@'),
                'expression' => "$access_privlg > 0",
            ),
            array(
                'allow', // allow admin user to perform 'admin' and 'delete' actions
                //'actions' => array(''),
                'users' => array('@'),
            ),
            array(
                'deny', // deny all users
                'users' => array('*'),
            ),
            
            
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate($qid = false,$rev =false,$status =false,$selected_revision=false) {        
               
        $main_sec_model = new QuotationSection;
        $main_cat_model = new QuotationGenCategory;
        $main_wtype_model = new QuotationGenWorktype;
        $modelsales = new SalesQuotation;

        if(isset($_REQUEST['sec_id']) && $_REQUEST['sec_id']){
            $main_sec_model = QuotationSection::model()->findByPk($_REQUEST['sec_id']);
        }

        if(isset($_REQUEST['sub_section_id']) && $_REQUEST['sub_section_id']){
            $main_cat_model = QuotationGenCategory::model()->findByPk($_REQUEST['sub_section_id']);
        }

        if(isset($_REQUEST['sub_item_id']) && $_REQUEST['sub_item_id']){
            $main_wtype_model = QuotationGenWorktype::model()->findByPk($_REQUEST['sub_item_id']);
            $qitems = SalesQuotation::model()->findAll(array('condition'=>'parent_id='.$_REQUEST['sub_item_id']));
            $modelsales = !empty($qitems)?$qitems:$modelsales;
        }
        
        $main_item_model = new QuotationCategoryMaster();        
        $model = new SalesQuotationMaster;
                
        $model->unsetAttributes();  // clear any default values
        $location_data = Location::model()->findAll();
        if (isset($_GET['SalesQuotation']))
            $model->attributes = $_GET['SalesQuotation'];
        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        $newQuery = "";
        foreach ($arrVal as $arr) {
            if ($newQuery)
                $newQuery .= ' OR';
            $newQuery .= " FIND_IN_SET('" . $arr . "', company_id)";
        }
        if(!empty($rev)){
          $revision_no = 'REV-'.$rev;  
        }else{
            $rev = 1;
           $revision_no = 'REV-'.$rev;   
        }
        if (!empty($qid)) {
            $model = SalesQuotationMaster::model()->findByPk($qid);
        } else {
            $model->invoice_no = $this->generateQuotationNumber();
        }
         
        if(empty($status)){
         $status = 0;   
        }
        $this->performAjaxValidation($model);
        $user = Users::model()->findByPk(Yii::app()->user->id);
        $invoice_number = $this->generateQuotationNumber();
        $main_item_master = QuotationItemMaster::model()->findAll();

        $template_type = '1';  //agac              
        $activeProjectTemplate = $this->getActiveTemplate();
        if ($activeProjectTemplate == 'TYPE-2') {
            $template_type = '2'; //d2r
        }else if ($activeProjectTemplate == 'TYPE-5') {
            $template_type = '5'; //concord
        } else if ($activeProjectTemplate == 'TYPE-7') {
            $template_type = '2'; //d2r
        } else if ($activeProjectTemplate == 'TYPE-8') {
            $template_type = '1';  //agac 
        }else if($this->getActiveTemplateID()>8){
            $template_type = '1';
        } 

        if (isset($_POST['SalesQuotationMaster'])) {
            $model->attributes = $_POST['SalesQuotationMaster'];
            $model->sales_executive_id = isset($_POST['SalesQuotationMaster']['sales_executive_id'])?$_POST['SalesQuotationMaster']['sales_executive_id']:"";
            
            if (empty($qid)) {
                $tblpx = Yii::app()->db->tablePrefix;
                $res = SalesQuotationMaster::model()->findAll(array('condition' => 'invoice_no = "' . $_POST['SalesQuotationMaster']['invoice_no'] . '"'));
                if (empty($res)) {
                    $model = new SalesQuotationMaster;
                    $model->invoice_no = $_POST['SalesQuotationMaster']['invoice_no'];
                    $model->client_name = $_POST['SalesQuotationMaster']['client_name'];
                    $model->address = $_POST['SalesQuotationMaster']['address'];
                    $model->phone_no = $_POST['SalesQuotationMaster']['phone_no'];
                    $model->email = $_POST['SalesQuotationMaster']['email'];
                    $model->location_id = $_POST['SalesQuotationMaster']['location_id'];
                    $model->company_id = $_POST['SalesQuotationMaster']['company_id'];
                    $model->site_address = isset($_POST['SalesQuotationMaster']['site_address'])? $_POST['SalesQuotationMaster']['site_address']:"";
                    $model->house_name = isset($_POST['SalesQuotationMaster']['house_name'])?$_POST['SalesQuotationMaster']['house_name']:"";
                    $model->project_type = isset($_POST['SalesQuotationMaster']['project_type'])?$_POST['SalesQuotationMaster']['project_type']:"0";
                    $model->revision_no = $revision_no;
                    $model->template_type = $template_type;
                    $model->date_quotation = date('Y-m-d', strtotime($_POST['date']));
                    $model->created_date = date('Y-m-d H:i:s');
                    $model->created_by = Yii::app()->user->id;
                    $model->sales_executive_id =  isset($_POST['SalesQuotationMaster']['sales_executive_id'])?$_POST['SalesQuotationMaster']['sales_executive_id']:"";;
                    if ($model->save(false)) {
                        $last_id = $model->id;
                        if ($model->invoice_no != '' && $model->client_name != '' && $model->address != '' && $model->phone_no != '' && $model->email != '' && $model->location_id != '') {
                            Yii::app()->user->setFlash('success', "Successfully Created");
                        }

                        $this->redirect(array('salesQuotation/create', 'qid' => $last_id,'rev'=>$rev,'status'=> $status));
                    } else {
                        $error = json_encode($model->getErrors());
                        Yii::app()->user->setFlash('error', $error);
                    }
                } else {
                    Yii::app()->user->setFlash('error', "Quotation number already exist");
                }
            } else {
                $model = SalesQuotationMaster::model()->findByPk($qid);
                $model->invoice_no = $_POST['SalesQuotationMaster']['invoice_no'];
                $model->client_name = $_POST['SalesQuotationMaster']['client_name'];
                $model->address = $_POST['SalesQuotationMaster']['address'];
                $model->phone_no = $_POST['SalesQuotationMaster']['phone_no'];
                $model->email = $_POST['SalesQuotationMaster']['email'];
                $model->location_id = $_POST['SalesQuotationMaster']['location_id'];
                $model->company_id = $_POST['SalesQuotationMaster']['company_id'];
                $model->sales_executive_id = $_POST['SalesQuotationMaster']['sales_executive_id'];
                $model->site_address = isset($_POST['SalesQuotationMaster']['site_address'])? $_POST['SalesQuotationMaster']['site_address']:"";
                    $model->house_name = isset($_POST['SalesQuotationMaster']['house_name'])?$_POST['SalesQuotationMaster']['house_name']:"";
                    $model->project_type = isset($_POST['SalesQuotationMaster']['project_type'])?$_POST['SalesQuotationMaster']['project_type']:"0";
                $model->revision_no = $revision_no;
                $model->created_date = date('Y-m-d H:i:s');
                $model->created_by = Yii::app()->user->id;
                $model->date_quotation = date('Y-m-d', strtotime($_POST['date']));

                if ($model->save(false)) {
                    if ($model->invoice_no != '' && $model->client_name != '' && $model->address != '' && $model->phone_no != '' && $model->email != '' && $model->location_id != '') {
                        Yii::app()->user->setFlash('success', "Successfully Updated");
                    }
                    $this->redirect(array('salesQuotation/create', 'qid' => $qid,'rev'=>$rev,'status'=> $status));
                } else {
                    $error = json_encode($model->getErrors());
                    Yii::app()->user->setFlash('error', $error);
                }
            }
        }
        
        $salesquotation_edit = $model->getQuotationdetailsedit($qid, 1,$selected_revision);
        $salesquotation_extra_edit = $model->getQuotationdetailsedit($qid, 2,$selected_revision);
        $salesquotation = $model->getQuotationdetails($qid,1);
        $salesquotation_extra = $model->getQuotationdetails($qid,2);
        $file = 'create';
        $activeProjectTemplate = $this->getActiveTemplate();
        if ($this->getActiveTemplate() == 'TYPE-7' || $this->getActiveTemplate() == 'TYPE-8'){
            $file = 'create1';
		}else if($this->getActiveTemplateID()>8){
            $file = 'create1';
        } 
        
        $this->render($file, array(
            'model' => $model,
            'modelsales' => $modelsales,
            'user_companies' => $user->company_id,
            'main_item_model' => $main_item_model,
            'main_item_master' => $main_item_master,
            'location_data' => $location_data,
            'salesquotation_edit' => $salesquotation_edit,
            'salesquotation_extra_edit' => $salesquotation_extra_edit,
            'salesquotation' => $salesquotation,
            'salesquotation_extra' => $salesquotation_extra,
            'main_sec_model'=>$main_sec_model,
            'main_cat_model'=>$main_cat_model,
            'main_wtype_model'=>$main_wtype_model
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['SalesQuotation'])) {
            $model->attributes = $_POST['SalesQuotation'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    public function actionRemovequotation() {
        $tblpx = Yii::app()->db->tablePrefix;
        $data = $_REQUEST['data'];
        $id = $data['quotation_id'];
        $quotation = $this->loadModel($id);
        $logmodel = new JpLog('search');
        $logmodel->log_data = json_encode($quotation->attributes);
        $logmodel->log_action = 2;
        $logmodel->log_table = $tblpx . "sales_quotation_master";
        $logmodel->log_datetime = date('Y-m-d H:i:s');
        $logmodel->log_action_by = Yii::app()->user->id;
        $logmodel->company_id = yii::app()->user->company_id;
        $logmodel->save();
        
        Yii::app()->db->createCommand()->delete($tblpx . 'sales_quotation', 'master_id=:id', array(':id' => $id));
        Yii::app()->db->createCommand()->delete($tblpx . 'quotation_gen_worktype', 'qid=:id', array(':id' => $id));            
        Yii::app()->db->createCommand()->delete($tblpx . 'quotation_gen_category', 'qid=:id', array(':id' => $id));
        Yii::app()->db->createCommand()->delete($tblpx . 'quotation_section', 'qtn_id=:id', array(':id' => $id));
        Yii::app()->db->createCommand()->delete($tblpx . 'quotation_revision', 'qid=:id', array(':id' => $id));
        $del = $quotation->delete();
        if ($del) {
            echo json_encode(array('response' => 'success', 'msg' => 'Quotation  deleted successfully',));
        } else {
            echo json_encode(array('response' => 'error', 'msg' => 'Some problem occured'));
        }
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $main_item_model = new QuotationCategoryMaster();
        $model = new SalesQuotation;

        // Define pagination parameters
        $pageSize = 10; // Number of items per page
        $currentPage = isset($_GET['page']) ? max(1, intval($_GET['page'])) : 1;

        // Define the criteria for fetching data
        $criteria = new CDbCriteria;
        if(Yii::app()->user->role == 1 || Yii::app()->user->role == 8){
            $criteria->order = 'id DESC';
        } else { 
            $criteria->condition = 'created_by = ' . Yii::app()->user->id;
            $criteria->order = 'id DESC';
        }

        // Count total number of records
        $totalItemCount = SalesQuotationMaster::model()->count($criteria);

        // Set pagination parameters
        $pagination = new CPagination($totalItemCount);
        $pagination->pageSize = $pageSize;
        $pagination->currentPage = $currentPage - 1;

        // Apply pagination to the criteria
        $pagination->applyLimit($criteria);
        $criteria->select = 'id, invoice_no, company_id, client_name, date_quotation, address, location_id, revision_no, template_type,total_amount_after_discount';

        // Fetch data based on the criteria
        $modelnew = SalesQuotationMaster::model()->findAll($criteria);

        $salesexecutive = Salesexecutive::model()->findAll(array(
            'select' => array('id', 'name'),
            'order' => 'id',
        ));

        
        $this->render('index', array(
            'model' => $model,
            'modelnew' => $modelnew,
            'salesexecutive' => $salesexecutive,
            'pagination' => $pagination,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $dataProvider = new CActiveDataProvider('SalesQuotation');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return SalesQuotation the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = SalesQuotationMaster::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param SalesQuotation $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'sales-quotation-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actiondynamicclient() {
        $company_id = $_POST['company_id'];
        $tblpx = Yii::app()->db->tablePrefix;
        $html = '';
        $html['html'] = '';
        $html['status'] = '';
        $expense_type = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}clients WHERE FIND_IN_SET(" . $company_id . ",company_id)")->queryAll();
        if (!empty($expense_type)) {
            $html['html'] .= '<option value="">Select Client</option>';
            foreach ($expense_type as $key => $value) {
                $html['html'] .= '<option value="' . $value['cid'] . '">' . $value['name'] . '</option>';
                $html['status'] = 'success';
            }
        } else {
            $html['status'] = 'no_success';
            $html['html'] .= '<option value="">Select Client</option>';
        }
        $html = array('clients_list' => $html);
        echo json_encode($html);
    }

    public function actiongetquotationitem() {
        
        $qid = isset($_POST['qid'])?$_POST['qid']:'';
        $rev = isset($_POST['rev'])? $_POST['rev']:'';
        $status = isset($_POST['status'])?$_POST['status']:'';
        $parent_type = isset($_POST['parent_type'])?$_POST['parent_type']:'';
        $parent_id = isset($_POST['parent_id'])?$_POST['parent_id']:'';
        $category_id = isset($_POST['category_id']) ? $_POST['category_id']:'';
        $category_label_id = isset($_POST['category_label_id']) ? $_POST['category_label_id']:'';
        $tblpx = Yii::app()->db->tablePrefix;
        $model = new SalesQuotation;
        $criteria = new CDbCriteria();
        $criteria->select = 't.*,p.template_id';
        $criteria->join = 'LEFT JOIN '.$this->tableNameAcc('quotation_worktype_master',0).' p ON t.work_type_id = p.id';
        $criteria->condition = 'quotation_category_id = "' . $category_id . '"';
        $criteria->group= 'work_type_id';
        $criteria->order= 'template_id ASC';
        $criteria->distinct= true; 
        $all_quotation_items = QuotationItemMaster::model()->findAll($criteria);
        if (!empty($qid)) {
            $salesquotation = SalesQuotation::model()->findAll(array(
                'condition' => 'master_id = ' . $qid,
            ));
        }
        $rev_no = "REV-" . $rev;
        $quotation_latest = SalesQuotation::model()->find(
                array("condition" => "master_id = '$qid' AND revision_no ='$rev_no' "
                    . "AND deleted_status IN(1,2) AND revision_status =0",
                    'order' => 'id DESC '));
        $model->tax_slab = isset($quotation_latest->tax_slab)?$quotation_latest->tax_slab:'';
        $model->sgst_percent = isset($quotation_latest->sgst_percent)?$quotation_latest->sgst_percent:'';
        $model->sgst_amount = isset($quotation_latest->sgst_amount)?$quotation_latest->sgst_amount:'0.00';
        $model->cgst_percent =isset($quotation_latest->cgst_percent)?$quotation_latest->cgst_percent:'';
        $model->cgst_amount =isset($quotation_latest->cgst_amount)?$quotation_latest->cgst_amount:'0.00';
        $model->igst_percent =isset($quotation_latest->igst_percent)?$quotation_latest->igst_percent:'';
        $model->igst_amount =isset($quotation_latest->igst_amount)?$quotation_latest->igst_amount:'0.00';
        $model->mainitem_total =isset($quotation_latest->mainitem_total)?$quotation_latest->mainitem_total:'0.00';
        $model->mainitem_tax =isset($quotation_latest->mainitem_tax)?$quotation_latest->mainitem_tax:'0.00';
        $model->maintotal_withtax =isset($quotation_latest->maintotal_withtax)?$quotation_latest->maintotal_withtax:'0.00';
        $category_data = QuotationCategoryMaster::model()->findByPk($category_id);
                       
        $salesQuotationMaster = SalesQuotationMaster::model()->findByPk($qid);          
        $file = 'quotation_item';
        if ($salesQuotationMaster->template_type == '2') {
            $file = 'quotation_itemnew';
        }else if($salesQuotationMaster->template_type == '5'){
            $file = 'concord_quotation_itemnew';
        }
        if ($this->getActiveTemplate() == 'TYPE-7' || $this->getActiveTemplate() == 'TYPE-8'){
            $file = 'quotation_itemnew1';
		}else if($this->getActiveTemplateID() >8){
            $file = 'quotation_itemnew1';
        } 
        echo $this->renderPartial(
            $file, array(
            'all_quotation_items' => $all_quotation_items,
            'model' => $model,
            'quotation_count' => count($all_quotation_items),
            'category_id' => $category_id,
            'qid' => $qid,
            'rev' => $rev,
            'status' => $status,
            'parent_type' => $parent_type,
            'parent_id' => $parent_id,
            'category_label_id' => $category_label_id,
            'salesquotation' => $salesquotation,
            'count_quotation' => count($salesquotation),
            'category_label' => isset($category_data->name)?$category_data->name:""
            )
        );
    }

    public function actionAddQuotationItemsDetails() {
        $result = '';
        $error = '';
        $result_response = array();
        $error_records = array(); // Array to track errors for specific records
    
        if (isset($_POST)) {
            $count = 0;
            if (isset($_POST['worktype_label'])) {
                $count = count($_POST['worktype_label']);
            }
    
            if ($count > 0) {
                $cat_id = isset($_POST['cat_id']) ? $_POST['cat_id'] : '';
                $transaction = Yii::app()->db->beginTransaction();
                $master_id = isset($_POST['master_id']) ? $_POST['master_id'] : '';
                $parent_id = isset($_POST['parent_id']) ? $_POST['parent_id'] : '';
                $rev_no = '';
    
                try {
                    $sql = "SELECT revision_no FROM `jp_quotation_gen_worktype` WHERE id = " . $parent_id;
                    $rev_no = Yii::app()->db->createCommand($sql)->queryScalar();
    
                    $activeProjectTemplate = $this->getActiveTemplate();
                    if ($activeProjectTemplate == 'TYPE-5') {
                        $sql = "SELECT revision_no FROM `jp_quotation_section` WHERE id = " . $parent_id;
                        $rev_no = Yii::app()->db->createCommand($sql)->queryScalar();
                    }
    
                    $parent_type = isset($_POST['parent_type']) ? $_POST['parent_type'] : '';
                    $item_id = isset($_POST['item_id']) ? $_POST['item_id'] : '';
                    $category_label = isset($_POST['cat_name']) ? $_POST['cat_name'] : '';
                    $status = isset($_POST['status']) ? $_POST['status'] : '';
                    $tax_slab = isset($_POST['tax_slab']) ? $_POST['tax_slab'] : '';
                    $sgst_percent = isset($_POST['sgst_percent']) ? $_POST['sgst_percent'] : '';
                    $sgst_amount = isset($_POST['sgst_amount']) ? $_POST['sgst_amount'] : '';
                    $cgst_percent = isset($_POST['cgst_percent']) ? $_POST['cgst_percent'] : '';
                    $cgst_amount = isset($_POST['cgst_amount']) ? $_POST['cgst_amount'] : '';
                    $igst_percent = isset($_POST['igst_percent']) ? $_POST['igst_percent'] : '';
                    $igst_amount = isset($_POST['igst_amount']) ? $_POST['igst_amount'] : '';
                    $mainitem_total = isset($_POST['mainitem_total']) ? $_POST['mainitem_total'] : '';
                    $mainitem_tax = isset($_POST['mainitem_tax']) ? $_POST['mainitem_tax'] : '';
                    $maintotal_withtax = isset($_POST['maintotal_withtax']) ? $_POST['maintotal_withtax'] : '';
    
                    // Process worktype_label items
                    for ($i = 0; $i < $count; $i++) {
                        try {
                            $ifcondition = !empty($_POST['worktype_label'][$i]);
                            if (in_array($this->getActiveTemplate(), ['TYPE-7', 'TYPE-8']) || $this->getActiveTemplateID() > 8) {
                                $ifcondition = !empty($_POST['worktype_label'][$i]) && ($_POST['entry_id'][$i] == "");
                            }
    
                            if ($ifcondition) {
                                $model = new SalesQuotation();
                                if ($status == 1) {
                                    $model->revision_remarks = 'New Item Added';
                                    $model->revision_update_status = 1;
                                }
    
                                $model->master_id = $master_id;
                                $model->revision_no = $rev_no;
                                $model->category_id = $cat_id;
                                $model->parent_id = $parent_id;
                                $model->parent_type = $parent_type;
                                $model->item_id = $item_id;
                                $model->category_label = trim($category_label);
                                $model->tax_slab = $tax_slab;
                                $model->sgst_percent = $sgst_percent;
                                $model->sgst_amount = $sgst_amount;
                                $model->cgst_percent = $cgst_percent;
                                $model->cgst_amount = $cgst_amount;
                                $model->igst_percent = $igst_percent;
                                $model->igst_amount = $igst_amount;
                                $model->mainitem_total = $mainitem_total;
                                $model->mainitem_tax = $mainitem_tax;
                                $model->maintotal_withtax = $maintotal_withtax;
                                $model->work_type = isset($_POST['work_type_id'][$i]) ? $_POST['work_type_id'][$i] : '';
                                $model->shutterwork_description = isset($_POST['shutterwork_description'][$i]) ? $_POST['shutterwork_description'][$i] : null;
                                $model->carcass_description = isset($_POST['caracoss_description'][$i]) ? $_POST['caracoss_description'][$i] : '';
                                $model->worktype_label = isset($_POST['worktype_label'][$i]) ? $_POST['worktype_label'][$i] : '';
                                $model->shutterwork_material = isset($_POST['shutterwork_material'][$i]) ? $_POST['shutterwork_material'][$i] : '';
                                $model->shutterwork_finish = isset($_POST['shutterwork_finish'][$i]) ? $_POST['shutterwork_finish'][$i] : '';
                                $model->carcass_material = isset($_POST['carcass_material'][$i]) ? $_POST['carcass_material'][$i] : '';
                                $model->carcass_finish = isset($_POST['carcass_finish'][$i]) ? $_POST['carcass_finish'][$i] : '';
                                $model->material = isset($_POST['material'][$i]) ? $_POST['material'][$i] : '';
                                $model->finish = isset($_POST['finish'][$i]) ? $_POST['finish'][$i] : '';
                                $model->description = isset($_POST['description'][$i]) ? $_POST['description'][$i] : '';
                                $model->item_id = isset($_POST['item_id'][$i]) ? $_POST['item_id'][$i] : '';
                                $model->hsn_code = isset($_POST['hsn_code'][$i]) ? $_POST['hsn_code'][$i] : '';
                                $model->unit = isset($_POST['unit'][$i]) ? $_POST['unit'][$i] : '';
                                $model->quantity = isset($_POST['qty'][$i]) ? $_POST['qty'][$i] : '';
                                $model->item_name = isset($_POST['item_name'][$i]) ? $_POST['item_name'][$i] : '';
                                $model->quantity_nos = isset($_POST['qtynos'][$i]) ? $_POST['qtynos'][$i] : '';
                                $model->mrp = isset($_POST['mrp'][$i]) ? $_POST['mrp'][$i] : '';
                                $model->amount_after_discount = isset($_POST['amount_after_discount'][$i]) ? $_POST['amount_after_discount'][$i] : '';
                                $model->created_by = Yii::app()->user->id;
                                $model->created_date = date('Y-m-d H:i:s');
                                $model->length = isset($_POST['length'][$i]) ? $_POST['length'][$i] : '';
                                $model->width = isset($_POST['width'][$i]) ? $_POST['width'][$i] : '';
    
                                if (!$model->save()) {
                                    throw new Exception('An error occurred while adding item ' . $i);
                                }
                            }
                        } catch (Exception $e) {
                            $error_records[] = $e->getMessage();
                        }
                    }
    
                    // Process SalesQuotation items
                    if (isset($_POST['SalesQuotation'])) {
                        $count = 0;
                        if (isset($_POST['SalesQuotation']['description'])) {
                            $count = count($_POST['SalesQuotation']['description']);
                        }
    
                        for ($i = 0; $i < $count; $i++) {
                            try {
                                $ifcondition = !empty($_POST['SalesQuotation']['description'][$i]);
                                if (in_array($this->getActiveTemplate(), ['TYPE-7', 'TYPE-8']) || $this->getActiveTemplateID() > 8) {
                                    $ifcondition = !empty($_POST['SalesQuotation']['description'][$i]) && ($_POST['entry_id'][$i] == "");
                                }
    
                                if ($ifcondition) {
                                    $modelnew = new SalesQuotation();
                                    if ($status == 1) {
                                        $modelnew->revision_remarks = 'New Item Added';
                                        $modelnew->revision_update_status = 1;
                                    }
    
                                    $modelnew->category_id = $cat_id;
                                    $modelnew->category_label = $category_label;
                                    $modelnew->parent_id = $parent_id;
                                    $modelnew->parent_type = $parent_type;
                                    $modelnew->master_id = $master_id;
                                    $modelnew->revision_no = $rev_no;
                                    $modelnew->work_type = "";
                                    $modelnew->tax_slab = $tax_slab;
                                    $modelnew->sgst_percent = $sgst_percent;
                                    $modelnew->sgst_amount = $sgst_amount;
                                    $modelnew->cgst_percent = $cgst_percent;
                                    $modelnew->cgst_amount = $cgst_amount;
                                    $modelnew->igst_percent = $igst_percent;
                                    $modelnew->igst_amount = $igst_amount;
                                    $modelnew->mainitem_total = $mainitem_total;
                                    $modelnew->mainitem_tax = $mainitem_tax;
                                    $modelnew->maintotal_withtax = $maintotal_withtax;
                                    $modelnew->description = $_POST['SalesQuotation']['description'][$i];
                                    $modelnew->hsn_code = $_POST['SalesQuotation']['hsn_code'][$i];
                                    $modelnew->unit = $_POST['SalesQuotation']['unit'][$i];
                                    $modelnew->length = isset($_POST['SalesQuotation']['length'][$i]) ? $_POST['SalesQuotation']['length'][$i] : '';
                                    $modelnew->width = isset($_POST['SalesQuotation']['width'][$i]) ? $_POST['SalesQuotation']['width'][$i] : '';
                                    $modelnew->quantity = $_POST['SalesQuotation']['quantity'][$i];
                                    $modelnew->quantity_nos = $_POST['SalesQuotation']['quantity_nos'][$i];
                                    $modelnew->mrp = $_POST['SalesQuotation']['mrp'][$i];
                                    $modelnew->amount_after_discount = $_POST['SalesQuotation']['amount_after_discount'][$i];
                                    $modelnew->created_by = Yii::app()->user->id;
                                    $modelnew->created_date = date('Y-m-d H:i:s');
    
                                    $image_name = isset($_FILES['SalesQuotation']['name']['image'][$i]) ? $_FILES['SalesQuotation']['name']['image'][$i] : '';
                                    $uploadedFile = CUploadedFile::getInstance($modelnew, 'image[' . $i . ']');
    
                                    if ($uploadedFile) {
                                        $image_name = strtotime("now") . 'p' . $i . '.' . $uploadedFile->getExtensionName();
                                    }
    
                                    if ($modelnew->save()) {
                                        $images_path = realpath(Yii::app()->basePath . '/../uploads');
                                        $file_upload = $uploadedFile->saveAs($images_path . '/' . $image_name);
                                        if (!$file_upload) {
                                            throw new Exception('Error while file upload for item ' . $i);
                                        }
                                    } else {
                                        throw new Exception('An error occurred while adding extra item ' . $i);
                                    }
                                }
                            } catch (Exception $e) {
                                $error_records[] = $e->getMessage();
                            }
                        }
                    }
    
                    if ($result == '') {
                        Yii::app()->db->createCommand()->update(
                            $this->tableNameAcc('sales_quotation_master', 0),
                            ['revision_no' => $rev_no],
                            'id=:master_id',
                            [':master_id' => $master_id]
                        );
                    }
    
                    $transaction->commit();
                } catch (Exception $e) {
                    $error = $e->getMessage();
                    $result = "err";
                    $transaction->rollBack();
                } finally {
                    if ($result == "") {
                        $total_amount = Controller::getQuotationSum($master_id, $rev_no, NULL);
                        $response = [
                            'response' => 'Success',
                            'total_amount' => Controller::money_format_inr($total_amount, 2),
                            'data_val' => $total_amount
                        ];
                        if (!empty($error_records)) {
                            $response['partial_errors'] = $error_records;
                        }
                        echo json_encode($response);
                    } else if ($result == "err") {
                        echo json_encode(['response' => 'Error', 'message' => 'Error Occurred']);
                    }
                }
            }
        }
    }
    

    
    

    public function actionaddQuotationItems() {
        $result = '';
        $error = '';
        $result_response = array();
        if (isset($_POST)) {
            $count = 0;
            if (isset($_POST['worktype_label'])) {
                $count = count($_POST['worktype_label']);
            }
            $cat_id = $_POST['cat_id'];
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $master_id = isset($_POST['master_id']) ? $_POST['master_id'] : '';
                $revision_no = isset($_POST['revision_no']) ? $_POST['revision_no'] : '';
                $rev_no = "REV-" . $revision_no;
                $cat_id = isset($_POST['cat_id']) ? $_POST['cat_id'] : '';
                $item_id = isset($_POST['item_id']) ? $_POST['item_id'] : '';
                $category_label = isset($_POST['cat_name']) ? $_POST['cat_name'] : '';
                $status = isset($_POST['status']) ? $_POST['status'] : '';
                $tax_slab = isset($_POST['tax_slab']) ? $_POST['tax_slab'] : '';
                $sgst_percent = isset($_POST['sgst_percent']) ? $_POST['sgst_percent'] : '';
                $sgst_amount = isset($_POST['sgst_amount']) ? $_POST['sgst_amount'] : '';
                $cgst_percent = isset($_POST['cgst_percent']) ? $_POST['cgst_percent'] : '';
                $cgst_amount = isset($_POST['cgst_amount']) ? $_POST['cgst_amount'] : '';
                $igst_percent = isset($_POST['igst_percent']) ? $_POST['igst_percent'] : '';
                $igst_amount = isset($_POST['igst_amount']) ? $_POST['igst_amount'] : '';
                $mainitem_total = isset($_POST['mainitem_total']) ? $_POST['mainitem_total'] : '';
                $mainitem_tax = isset($_POST['mainitem_tax']) ? $_POST['mainitem_tax'] : '';
                $maintotal_withtax = isset($_POST['maintotal_withtax']) ? $_POST['maintotal_withtax'] : '';
                if ($status == 1) {
                    $old_revision_data = $this->getOldData($master_id, $revision_no);
                    $old_revision_count = count($old_revision_data);
                    if ($old_revision_count >= 1) {
                        $arr_revision = array();
                        foreach ($old_revision_data as $key => $revision_data) {
                            array_push($arr_revision, $revision_data->attributes);
                            $arr_revision[$key]['revision_no'] = $rev_no;
                            $arr_revision[$key]['id'] = NULL;
                        }
                        $revision_insert = Yii::app()->db->getCommandBuilder()->
                                        createMultipleInsertCommand('jp_sales_quotation', $arr_revision)->execute();
                        if(!$revision_insert){
                            $result = "err";
                            throw new Exception('An error occured while item add');
                        }
                    }
                }
                for ($i = 0; $i < $count; $i++) {
                    if (!empty($_POST['worktype_label'][$i])) {
                        $model = new SalesQuotation;
                        if ($status == 1) {
                        $model->revision_remarks = 'New Item Added';
                        }
                        $model->master_id = $master_id;
                        $model->revision_no = $rev_no;
                        $model->category_id = $cat_id;
                        $model->item_id = $item_id;
                        $model->category_label = trim($category_label);
                        $model->tax_slab = $tax_slab;
                        $model->sgst_percent = $sgst_percent;
                        $model->sgst_amount = $sgst_amount;
                        $model->cgst_percent = $cgst_percent;
                        $model->cgst_amount = $cgst_amount;
                        $model->igst_percent = $igst_percent;
                        $model->igst_amount = $igst_amount;
                        $model->mainitem_total = $mainitem_total;
                        $model->mainitem_tax = $mainitem_tax;
                        $model->maintotal_withtax = $maintotal_withtax;
                        if (isset($_POST['worktype_label'][$i])) {
                            $model->work_type = isset($_POST['work_type_id'][$i]) ? $_POST['work_type_id'][$i] : '';
                            $model->shutterwork_description = isset($_POST['shutterwork_description'][$i]) ? $_POST['shutterwork_description'][$i] : null;
                            $model->carcass_description = isset($_POST['caracoss_description'][$i]) ? $_POST['caracoss_description'][$i] : '';
                            $model->worktype_label = isset($_POST['worktype_label'][$i]) ? $_POST['worktype_label'][$i] : '';
                            $model->shutterwork_material = isset($_POST['shutterwork_material'][$i]) ? $_POST['shutterwork_material'][$i] : '';
                            $model->shutterwork_finish = isset($_POST['shutterwork_finish'][$i]) ? $_POST['shutterwork_finish'][$i] : '';
                            $model->carcass_material = isset($_POST['carcass_material'][$i]) ? $_POST['carcass_material'][$i] : '';
                            $model->carcass_finish = isset($_POST['carcass_finish'][$i]) ? $_POST['carcass_finish'][$i] : '';
                            $model->material = isset($_POST['material'][$i]) ? $_POST['material'][$i] : '';
                            $model->finish = isset($_POST['finish'][$i]) ? $_POST['finish'][$i] : '';
                            $model->description = isset($_POST['description'][$i]) ? $_POST['description'][$i] : '';
                            $model->item_id = isset($_POST['item_id'][$i]) ? $_POST['item_id'][$i] : '';
                            $model->unit = isset($_POST['unit'][$i]) ? $_POST['unit'][$i] : '';
                            $model->quantity = isset($_POST['qty'][$i]) ? $_POST['qty'][$i] : '';
                            $model->quantity_nos = isset($_POST['qtynos'][$i]) ? $_POST['qtynos'][$i] : '';
                            $model->mrp = isset($_POST['mrp'][$i]) ? $_POST['mrp'][$i] : '';
                            $model->amount_after_discount = isset($_POST['amount_after_discount'][$i]) ? $_POST['amount_after_discount'][$i] : '';
                            $model->created_by = Yii::app()->user->id;
                            $model->created_date = date('Y-m-d H:i:s');
                            $model->length = isset($_POST['length'][$i]) ? $_POST['length'][$i] : '';
                            $model->width = isset($_POST['width'][$i]) ? $_POST['width'][$i] : '';
                        }
                        if (!$model->save()) {
                            $result = "err";
                            throw new Exception('An error occured while item add');
                        }
                    }
                }
                
                if (isset($_POST['subitem'])) {
                    $sub_count = 0;
                    if (isset($_POST['subitem']['worktype_type'])) {
                        $sub_count = count($_POST['subitem']['worktype_type']);
                    }
                    for ($i = 0; $i < $sub_count; $i++) {
                        if (!empty($_POST['subitem']['worktype_type'][$i])) {
                            $modelsub = new SalesQuotation;
                            if ($status == 1) {
                                $modelsub->revision_remarks = 'New Item Added';
                            }
                            $modelsub->master_id = $master_id;
                            $modelsub->revision_no = $rev_no;
                            $modelsub->category_id = $cat_id;
                            $modelsub->item_id = $item_id;
                            $modelsub->category_label = $category_label;
                            $modelsub->tax_slab = $tax_slab;
                            $modelsub->sgst_percent = $sgst_percent;
                            $modelsub->sgst_amount = $sgst_amount;
                            $modelsub->cgst_percent = $cgst_percent;
                            $modelsub->cgst_amount = $cgst_amount;
                            $modelsub->igst_percent = $igst_percent;
                            $modelsub->igst_amount = $igst_amount;
                            $modelsub->mainitem_total = $mainitem_total;
                            $modelsub->mainitem_tax = $mainitem_tax;
                            $modelsub->maintotal_withtax = $maintotal_withtax;
                            $modelsub->sub_status = 1;
                            $modelsub->subitem_label = isset($_POST['subitem']['subitem_label']) ? $_POST['subitem']['subitem_label'] : '';
                            if (isset($_POST['subitem']['worktype_type'][$i])) {
                                $modelsub->work_type = isset($_POST['subitem']['worktype_type'][$i]) ? $_POST['subitem']['worktype_type'][$i] : '';
                                $modelsub->shutterwork_description = isset($_POST['subitem']['shutterwork_description'][$i]) ? $_POST['subitem']['shutterwork_description'][$i] : null;
                                $modelsub->carcass_description = isset($_POST['subitem']['caracoss_description'][$i]) ? $_POST['subitem']['caracoss_description'][$i] : '';
                                $modelsub->worktype_label = isset($_POST['subitem']['worktype_label'][$i]) ? $_POST['subitem']['worktype_label'][$i] : '';
                                $modelsub->shutterwork_material = isset($_POST['subitem']['shutterwork_material'][$i]) ? $_POST['subitem']['shutterwork_material'][$i] : '';
                                $modelsub->shutterwork_finish = isset($_POST['subitem']['shutterwork_finish'][$i]) ? $_POST['subitem']['shutterwork_finish'][$i] : '';
                                $modelsub->carcass_material = isset($_POST['subitem']['carcass_material'][$i]) ? $_POST['subitem']['carcass_material'][$i] : '';
                                $modelsub->carcass_finish = isset($_POST['subitem']['carcass_finish'][$i]) ? $_POST['subitem']['carcass_finish'][$i] : '';
                                $modelsub->material = isset($_POST['subitem']['material'][$i]) ? $_POST['subitem']['material'][$i] : '';
                                $modelsub->finish = isset($_POST['subitem']['finish'][$i]) ? $_POST['subitem']['finish'][$i] : '';
                                $modelsub->description = isset($_POST['subitem']['description'][$i]) ? $_POST['subitem']['description'][$i] : '';
                                $modelsub->item_id = isset($_POST['subitem']['item_id'][$i]) ? $_POST['subitem']['item_id'][$i] : '';
                                $modelsub->unit = isset($_POST['subitem']['unit'][$i]) ? $_POST['subitem']['unit'][$i] : '';
                                $modelsub->length = isset($_POST['subitem']['length'][$i]) ? $_POST['subitem']['length'][$i] : '';
                                $modelsub->width = isset($_POST['subitem']['width'][$i]) ? $_POST['subitem']['width'][$i] : '';
                                $modelsub->quantity = isset($_POST['subitem']['quantity'][$i]) ? $_POST['subitem']['quantity'][$i] : '';
                                $modelsub->quantity_nos = isset($_POST['subitem']['quantity_nos'][$i]) ? $_POST['subitem']['quantity_nos'][$i] : '';
                                $modelsub->mrp = isset($_POST['subitem']['mrp'][$i]) ? $_POST['subitem']['mrp'][$i] : '';
                                $modelsub->amount_after_discount = isset($_POST['subitem']['amount_after_discount'][$i]) ? $_POST['subitem']['amount_after_discount'][$i] : '';
                                $modelsub->created_by = Yii::app()->user->id;
                                $modelsub->created_date = date('Y-m-d H:i:s');
                            }
                            if (!$modelsub->save()) {
                                $result = "err";
                                throw new Exception('An error occured while item add');
                            }
                        }
                    }
                }

                if (isset($_POST['SalesQuotation'])) {
                    $count = 0;
                    if (isset($_POST['SalesQuotation']['description'])) {
                        $count = count($_POST['SalesQuotation']['description']);
                    }
                    for ($i = 0; $i < $count; $i++) {
                        if (!empty($_POST['SalesQuotation']['description'][$i])) {
                            $modelnew = new SalesQuotation;
                            if ($status == 1) {
                            $modelnew->revision_remarks = 'New Item Added';
                            }
                            $modelnew->category_id = $cat_id;
                            $modelnew->category_label = $category_label;
                            $modelnew->master_id = $master_id;
                            $modelnew->revision_no = $rev_no;
                            $modelnew->work_type = "";
                            $modelnew->tax_slab = $tax_slab;
                            $modelnew->sgst_percent = $sgst_percent;
                            $modelnew->sgst_amount = $sgst_amount;
                            $modelnew->cgst_percent = $cgst_percent;
                            $modelnew->cgst_amount = $cgst_amount;
                            $modelnew->igst_percent = $igst_percent;
                            $modelnew->igst_amount = $igst_amount;
                            $modelnew->mainitem_total = $mainitem_total;
                            $modelnew->mainitem_tax = $mainitem_tax;
                            $modelnew->maintotal_withtax = $maintotal_withtax;
                            $modelnew->description = $_POST['SalesQuotation']['description'][$i];
                            $modelnew->unit = $_POST['SalesQuotation']['unit'][$i];
                            $modelnew->quantity = $_POST['SalesQuotation']['quantity'][$i];
                            $modelnew->length = isset($_POST['SalesQuotation']['length'][$i]) ? $_POST['SalesQuotation']['length'][$i] : '';
                            $modelnew->width = isset($_POST['SalesQuotation']['width'][$i]) ? $_POST['SalesQuotation']['width'][$i] : '';
                            $modelnew->quantity_nos = $_POST['SalesQuotation']['quantity_nos'][$i];
                            $modelnew->mrp = $_POST['SalesQuotation']['mrp'][$i];
                            $modelnew->amount_after_discount = $_POST['SalesQuotation']['amount_after_discount'][$i];
                            $modelnew->created_by = Yii::app()->user->id;
                            $modelnew->created_date = date('Y-m-d H:i:s');
                            $image_name = isset($_FILES['SalesQuotation']['name']['image'][$i])?$_FILES['SalesQuotation']['name']['image'][$i]:'';
                            $uploadedFile = CUploadedFile::getInstance($modelnew, 'image['.$i.']');
                        
                            if ($image_name) {
                                $filename                   = strtotime("now").'p'.$i.'.'.$uploadedFile->getExtensionName();					
                                $newfilename                = rand(1000, 9999) . time();
                                $newfilename                = md5($newfilename); //optional
                                $extension                  = $uploadedFile->getExtensionName();
                                $modelnew->image = $newfilename . "." . $extension;
                            }
                            
                            if($uploadedFile){
                                $image_name = strtotime("now").'p'.$i.'.'.$uploadedFile->getExtensionName();
                            }
                            if ($modelnew->save()) {
                                if($uploadedFile){
                                    $images_path = realpath(Yii::app()->basePath . '/../uploads');
                                    $file_upload =  $uploadedFile->saveAs($images_path . '/' .$image_name);
                                    if(!$file_upload){
                                        $result = "err";
                                        throw new Exception('Error while file Upload');
                                    }
                                }                                
                            }else{
                                $result = "err";
                                throw new Exception('An error occured while extra item add');
                            }
                            
                        }
                    }
                }
                if ($result == '') {
                    $update = Yii::app()->db->createCommand()->update($this->tableNameAcc('sales_quotation_master',0), array('revision_no' => $rev_no), 'id=:master_id ', array(':master_id' => $master_id));
                }
                $transaction->commit();
            } catch (Exception $error) {
                $error = $error->getMessage();
                $transaction->rollBack();
            } finally {
                if ($result == "") {
                    Yii::app()->user->setFlash('success', "Item added successfully");
                   // $this->redirect(array('salesQuotation/create','qid' => $master_id,'rev' => $revision_no,'status' =>$status,'selected_revision'=>'REV-'.$revision_no));
                   $this->redirect(array('salesQuotation/index'));
                } else if($result == "err") {
                    Yii::app()->user->setFlash('error', $error);
                    $this->redirect(array('salesQuotation/create','qid' => $master_id,'rev' => $revision_no,'status' =>$status,'selected_revision'=>'REV-'.$revision_no));
                }
            }
        }
    }

    public function getOldData($master_id, $revision_no) {
        $tblpx = Yii::app()->db->tablePrefix;
        $revision_no_old = $revision_no - 1;
        $rev_no_old = "REV-" . $revision_no_old;
        $data = SalesQuotation::model()->findAll(array("condition" => "master_id = '$master_id' AND revision_no ='$rev_no_old' AND revision_status='0' ", 'order' => 'category_id '));
        if($rev_no_old!=''){
                        $sql = "UPDATE " . $tblpx . "sales_quotation"
                                . " SET revision_status =1"
                                . " WHERE master_id='" . $master_id . "' "
                            . " AND revision_no='".$rev_no_old."'";
                        Yii::app()->db->createCommand($sql)->execute();
        }
        return $data;
    }

    public function actiongetamountdetails() {
        $item_id = $_POST['item_id'];
        if(isset($_POST['status'])){
            $item_id = Yii::app()->db->createCommand("SELECT id FROM `jp_quotation_item_master` WHERE `worktype_label` LIKE '".$_POST['item_id'] ."'")->queryScalar();
        }
        
        if (isset($item_id)) {
            $id = $item_id;
            $qty = $_POST['qty'];
            $amount_data = QuotationItemMaster::model()->findByPk($id);
            $amount = 0;
            $discount = 0;
            if (!empty($amount_data)) {
                $amount = $amount_data->rate + (($amount_data->rate * $amount_data->profit) / 100);
                $discount = $amount_data->discount;
            }
            $profit_amount = 0;
            if ($amount != 0) {
                $profit_amount = $qty * $amount;
            }
            if(isset($_POST['length'])&& $_POST['length']>0  && isset($_POST['width'])&& $_POST['width']>0){
                $profit_amount *= $_POST['length']*$_POST['width'];
            }
            $result['discount'] = $discount;
            $result['profit_amount'] = $profit_amount;
            echo json_encode($result);
            exit;
        }
    }

    public function actionViewquotation($qtid, $rev_no) {
        $company_name = '';
        $model = SalesQuotationMaster::model()->find(array("condition" => "id = '$qtid'"));
        if (!empty($model->company_id)) {
            $company_name = Controller::getCompanyName($model->company_id);
            
        }
        if (empty($model)) {
            $this->redirect(array('salesquotation/SalesQuotation/index'));
        }
        $settings = GeneralSettings::model()->findByPk(1);
        $item_model = $this->getCategoryItem($qtid, $rev_no, $sub_status = 0);
        $sub_item = $this->getCategoryItem($qtid, $rev_no, $sub_status = 1);
        $quotation_categories = $this->getQuotationCategory($qtid, $rev_no);
        $view_datas = $this->setQuotationsList($item_model, $sub_item, $model, $quotation_categories);
        $extra_work = $this->getExtraWork($qtid, $rev_no);
        $item_total = SalesQuotation::model()->find(
                array("condition" => "master_id = '$qtid' AND revision_no ='$rev_no' "
                    . "AND deleted_status IN(1,2)",
                    'order' => 'id DESC '));

                    $file = 'quotationview';

                    if ($model->template_type == '2') {
                        $file = 'quotationviewnew';
                    } 
                    if($model->template_type == '5'){
                        $file = 'concord_quotationview';
                    } 
            
                    if ($this->getActiveTemplate() == 'TYPE-7'){
                        $file = 'quotationviewnew';
                    }
                    if($this->getActiveTemplate() == 'TYPE-8'){
                        $file = 'agac_quotationviewnew';
                    }
                    if($this->getActiveTemplateID() >8){
                        $file = 'agac_quotationviewnew';
                    } 
          
        $this->render($file, array(
            'model' => $model,
            'company_name' => $company_name,
            'view_datas' => $view_datas,
            'extra_work' => $extra_work,
            'qtid' => $qtid,
            'rev_no' => $rev_no,
            'settings' => $settings,
            'total_taxable_amount' => isset($item_total->mainitem_total)?$item_total->mainitem_total:'',
            'total_tax' => isset($item_total->mainitem_tax)?$item_total->mainitem_tax:'',
            'total_amoun_withtax' => isset($item_total->maintotal_withtax)?$item_total->maintotal_withtax:'',
        ));
    }

    public function getCategoryItem($qtid, $rev_no, $sub_status) {
        $item_model = SalesQuotation::model()->findAll(
                array("condition" => "master_id = '$qtid' AND revision_no ='$rev_no' "
                    . "AND work_type IS NOT NULL"
                    . " AND deleted_status IN(1,2) "
                    . "AND sub_status='$sub_status'", 'order' => 'category_id '));
        return $item_model;
    }

    public function getQuotationCategory($qtid, $rev_no) {
        $quotation_categories = SalesQuotation::model()->findAll(array("select" => "DISTINCT category_label,category_id",
            "condition" => "master_id = '$qtid' AND revision_no ='$rev_no' "
            . "AND work_type IS NOT NULL AND deleted_status IN(1,2)",
            'order' => 'category_id '));
        return $quotation_categories;
    }

    public function getExtraWork($qtid, $rev_no) {
        $extrawork = SalesQuotation::model()->findAll(
                array("condition" => "master_id = '$qtid' AND  revision_no ='$rev_no' "
                    . "AND work_type IS NULL AND deleted_status IN(1,2)", 'order' => 'category_id '));
        return $extrawork;
    }

    public function actionSendAttachment()
    {
        $qid = isset($_POST['qid'])?$_POST['qid']:'';
        $revision_no = isset($_POST['revision_no'])?$_POST['revision_no']:'';
        $mail_to = isset($_POST['mail_to'])?$_POST['mail_to']:'';
        $mail_content = isset($_POST['mail_content'])?$_POST['mail_content']:'';
        $path = isset($_POST['pdf_path'])?$_POST['pdf_path']:'';
        $name = isset($_POST['pdf_name'])?$_POST['pdf_name']:'';
        $mail = new JPhpMailer();
        $subject = "" . Yii::app()->name . ": Sales Quotation Email";
        $headers = "" . Yii::app()->name . "";
        $bodyContent = "<p>Hi</p><p>" . $mail_content . "</p><p>Please find the attachment</p>";
        $server = (($_SERVER['HTTP_HOST'] == 'localhost' || $_SERVER['HTTP_HOST'] == 'bhi.localhost.com') ? '0' : '1');
        $newdate = date('Y-m-d');
        $mail->setFrom(EMAILFROM, Yii::app()->name);
        $allfiles = glob(Yii::getPathOfAlias('webroot') . '/documents/quotation/*');
        $mailSendData = array();
        $mailSendData['email_from_name'] = Yii::app()->name;
        $mailSendData['mail_to'] = $mail_to;
        $mailSendData['subject'] = Yii::app()->name . " : Sales Quotation Email";
        $mailSendData['send_copy_status'] = $_POST['send_copy'];
        if (file_exists($path)) {
            $mailSendData['attachment'][] = array($path, $name);
        }
        $mailSendData['message'] = $bodyContent;
        $mail_send_status = $this->sendSMTPMail($mailSendData);
        if ($mail_send_status['status'] == 1) {
            foreach ($allfiles as $file) {
                if (is_file($file))
                    unlink($file);
            }
        }
        echo json_encode($mail_send_status);
    }

    public function setQuotationsList($items_model, $subitem_model, $model, $quotation_categories) {
        $items = array();
        foreach ($items_model as $item) {
            $items[] = $item->attributes;
        }
        $subarray = array();
        foreach ($subitem_model as $item) {
            $subarray[] = $item->attributes;
        }
        $lists = array();
        $data_items = array();
        $data = array();
        $sub_items = array();        
        if (!empty($items)) {
            if (!empty($quotation_categories)) {
                foreach ($quotation_categories as $quotation_category) {
                    $keys = array_keys(array_column($items, 'category_id'), $quotation_category->category_id);
                    $data_items = array();
                    $data_items_label = array();
                    foreach ($keys as $key) {
                        array_push($data_items, $items[$key]);
                    }
                    
                    $keys1 = array_keys(array_column($data_items, 'category_label'), $quotation_category->category_label);
                    foreach ($keys1 as $key) {
                        array_push($data_items_label, $data_items[$key]);
                    }
                    $keys_sub = array_keys(array_column($subarray, 'category_label'), $quotation_category->category_label);
                    $sub_items = array();
                    foreach ($keys_sub as $keys) {
                        array_push($sub_items, $subarray[$keys]);
                    }
                    $data = array('category_details' => $quotation_category->attributes, 'items_list' => $data_items_label, 'sub_items' => $sub_items);
                    array_push($lists, $data);
                }                
            } else {
                $data_items = array();
                foreach ($items as $item) {
                    array_push($data_items, $item);
                }
                $sub_items = array();
                foreach ($subitem_model as $items) {
                    array_push($sub_items, $item);
                }
                $data = array('category_details' => '', 'items_list' => $data_items, 'sub_items' => $sub_items);
                if (!empty($data)) {
                    array_push($lists, $data);
                }
            }
        }        
        return $lists;
    }

    public function actionSaveQuotation($qid, $rev_no) {
        $model = SalesQuotationMaster::model()->find(array("condition" => "id='$qid'"));
        $quotation_no = $model->invoice_no;
        $item_data = $this->getCategoryItem($qid, $rev_no, $sub_status = 0);
        $sub_item = $this->getCategoryItem($qid, $rev_no, $sub_status = 1);
        $quotation_categories = $this->getQuotationCategory($qid, $rev_no);
        $item_model = $this->setQuotationsList($item_data, $sub_item, $model, $quotation_categories);
        $item_extra = $this->getExtraWork($qid, $rev_no);
        $materials = $this->getMaterials($qid);
        $compmodel = Company::model()->findByPk($model->company_id);
        $address = $compmodel->address;
        $mPDF1 = Yii::app()->ePdf->mPDF('', 'A4');
        $mPDF1->debug = true;
        $mPDF1->showImageErrors = true;
        $mPDF1->SetImportUse();
        $is_top_image = 0;

        // top files
        $sql  = "SELECT `id`,`top_bottom_image` FROM `jp_quotation_generator_image` WHERE type='0'";
        $image = Yii::app()->db->createCommand($sql)->queryAll(); 
        foreach ($image as $key => $value) {
            $pagecount = $mPDF1->SetSourceFile(realpath(Yii::app()->basePath . '/../uploads/quotation_generator/top-images/'.$value["top_bottom_image"]));            
            for ($i=1; $i<=$pagecount; $i++) {
                $import_page = $mPDF1->ImportPage($i);
                $mPDF1->UseTemplate($import_page);
        
                if ($i <= $pagecount)
                    $mPDF1->AddPage();
                $is_top_image = 1;
            }
        }   
        $app_root = YiiBase::getPathOfAlias('webroot');
        $site_hosturl = str_replace('/index.php','', Yii::app()->createAbsoluteUrl('/'));
        $image_base_url=Yii::app()->theme->baseUrl .'/assets/default/logo.png'; 
        $theme_asset_url = $site_hosturl."/themes/assets/" ; 
       
        if(file_exists(Yii::app()->theme->basePath .'/images/quotation-file/logo.png')){
            $image_url=Yii::app()->theme->baseUrl .'/images/quotation-file/logo.png';
        }else if( file_exists($app_root . "/themes/assets/logo.png") ){
            $image_url=$theme_asset_url .'logo.png';
        }else{
            $image_url=$theme_asset_url .'default-logo.png';
        }
               // middle content
        if ($model->template_type == '2') {
           // $pdf = 'quotationpdf';
           $pdf = "quotation_pdf_latest";
        }else if ($model->template_type == '5') {
            $pdf = 'concord_quotationpdf';
        }
        else{
            $pdf = 'agac_quotation_pdf';
            $mPDF1->SetWatermarkImage($image_url,0.2,array(59.266666667,26.458333333),P);
            $mPDF1->showWatermarkImage = true;
        }

        $activeProjectTemplate = $this->getActiveTemplate();
        if ($activeProjectTemplate == 'TYPE-8'||$activeProjectTemplate == 'TYPE-7') {
            //$pdf = 'agac_quotation_pdf_new';
            $pdf = "quotation_pdf_latest";
        }else if($this->getActiveTemplateID() >8){
            $pdf = "quotation_pdf_latest";
            $mPDF1->showWatermarkImage = false;
        }
        $pdf_data = ProjectTemplate::model()->find(array("condition" => "status='1'"));
        
        $item_total = SalesQuotation::model()->find(
                array("condition" => "master_id = '$qid' AND revision_no ='$rev_no' "
                    . "AND deleted_status IN(1,2)",
                    'order' => 'id DESC '));
        
        $sql = 'SELECT template_name '
                    . 'FROM jp_quotation_template '
                    . 'WHERE status="1"';
        $selectedtemplate =Yii::app()->db->createCommand($sql)->queryScalar();
        $template = $this->getTemplate($selectedtemplate);
        $usermodel = Users::model()->findByPk(Yii::app()->user->id);
        $estimator_name =  $usermodel->first_name.' '.$usermodel->last_name;
        $estimator_phone =  $usermodel->phonenumber;
        
        
        $data = $this->renderPartial($pdf, array(
            'qtid'=>$qid,
            'rev_no'=>$rev_no,
            'model' => $model,
            'address' => $address,
            'itemmodel' => $item_model,
            'item_extra' => $item_extra,
            'materials' => $materials,
            'client' => $model->client_name,
            'total_taxable_amount' => $item_total->mainitem_total,
            'total_tax' => $item_total->mainitem_tax,
            'total_amoun_withtax' => $item_total->maintotal_withtax,
            'pdf_data' => $pdf_data->template_format,           
            'is_top_image' => $is_top_image ,
            'compmodel' => $compmodel,
            'estimator_name' => $estimator_name,
            'estimator_phone' => $estimator_phone
                ), true);  
            if ($model->template_type == '5') {   
                $mPDF1->SetHTMLHeader('<table class="details">
                <tbody>
                    <tr>
                        <td class="border-bottom border-right-0" colspan="3">'. CHtml::image($this->logo, Yii::app()->name,array("style" => "max-height:45px;"))
                                .'
                            </td>

                            <td class="border-bottom text-right"  colspan="3">
                                <h2>CONCORD <span class="orange_text">DESIGN STUDIO</span></h2>      
                                
                                <small>Emir Palaza, Near St.Francis Xaviers Church,</small>
                                <small>Kaloor Kadavanthra Road, Kathrikadavu, Kochi - 682 017</small>
                                <small>ph: 0484-234622, 4028208, m:interiors@concordkerala.com</small>
                                <small>www.concordkerala.com</small>
                            </td>                                
                    </tr>
                    
                    <tr>
                        <td width="28%"  class="border-bottom-0 border-right-0">Name Of Client</td>
                        <td class="border-bottom-0 border-right-0">:</td>
                        <td width="21%" class="border-bottom-0"><b> '.$model->client_name.'</b></td>
                        <td width="28%" class="border-bottom-0 border-right-0">Quotation/Invoice No</td>
                        <td class="border-bottom-0 border-right-0">:</td>
                        <td width="21%" class="border-bottom-0"><b> '.(isset($model->invoice_no) ? $model->invoice_no : "").'</b></td>
                    </tr>
                    <tr>
                        <td width="28%"  class="border-bottom-0 border-right-0">Flat No./House no./House Name</td>
                        <td class="border-bottom-0 border-right-0">:</td>
                        <td width="21%" class="border-bottom-0"><b> '.$model->house_name.'</b></td>
                        <td width="28%" class="border-bottom-0 border-right-0">Date</td>
                        <td class="border-bottom-0 border-right-0">:</td>
                        <td width="21%" class="border-bottom-0"><b> '.(isset($model->date_quotation) ? date('d-m-Y', strtotime($model->date_quotation)) : "").'</b></td>
                    </tr>
                    <tr>
                        <td width="28%"  class="border-bottom-0 border-right-0">Project Address</td>
                        <td class="border-bottom-0 border-right-0">:</td>
                        <td width="21%" class="border-bottom-0"><b style="display:inline"> '.$model->address.'</b></td>
                        <td width="28%" class="border-bottom-0 border-right-0">Project Type</td>
                        <td class="border-bottom-0 border-right-0">:</td>
                        <td width="21%" class="border-bottom-0"><b> '.(($model->project_type=='0')?"Residential":"Commercial").'</b></td>
                    </tr> 

                    <tr>
                        <td width="28%"  class="border-bottom-0 border-right-0">Site Address</td>
                        <td class="border-bottom-0 border-right-0">:</td>
                        <td width="21%" class="border-bottom-0"><b> '.$model->site_address.'</b></td>
                        <td width="28%" class="border-bottom-0 border-right-0">&nbsp;</td>
                        <td class="border-bottom-0 border-right-0"></td>
                        <td width="21%" class="border-bottom-0"></td>
                    </tr>
                    <tr>
                        <td width="28%"  class="border-bottom-0 border-right-0">Ph No</td>
                        <td class="border-bottom-0 border-right-0">:</td>
                        <td width="21%" class="border-bottom-0"><b> '.$model->phone_no.'</b></td>
                        <td width="28%" class="border-bottom-0 border-right-0">&nbsp;</td>
                        <td class="border-bottom-0 border-right-0"></td>
                        <td width="21%" class="border-bottom-0"></td>
                    </tr> 
                    <tr>
                        <td width="28%"  class="border-right-0">Email id </td>
                        <td class="border-bottom-0 border-right-0">:</td>
                        <td width="21%" class=""><b> '.$model->email.'</b></td>
                        <td width="28%" class="border-right-0">&nbsp;</td>
                        <td class="border-bottom-0 border-right-0"></td>
                        <td width="21%" class=""></td>
                    </tr> 
                                                                        
                </tbody>
            </table><br>'); 
            $mPDF1->AddPage('','', '', '', '', 10,10,70, 30, 10,0); 
        } 

        $this->actiondownloadQuotationPdf($data)  ;       
         
        $pagecount1 = $mPDF1->SetSourceFile(realpath(Yii::app()->basePath . '/../documents/quotation/qgenerator.pdf'));            
        for ($i=1; $i<=$pagecount1; $i++) {
            $import_page1 = $mPDF1->ImportPage($i);
            $mPDF1->UseTemplate($import_page1);
    
            if ($i <= $pagecount1)
            $mPDF1->AddPage();
        }
    
        
        $blankpage = $mPDF1->page + 1; 
        $mPDF1->DeletePages($blankpage);
        $mPDF1->WriteHTML('');
        $mPDF1->AddPage();
        // bottom files
 
        // bottom files
        $sql  = "SELECT `id`,`top_bottom_image` FROM `jp_quotation_generator_image` WHERE type='1'";
        $image1 = Yii::app()->db->createCommand($sql)->queryAll(); 
        foreach ($image1 as $key => $value) {
            $pagecount2 = $mPDF1->SetSourceFile(realpath(Yii::app()->basePath . '/../uploads/quotation_generator/bottom-images/'.$value["top_bottom_image"]));            
            for ($i=1; $i<=$pagecount2; $i++) {
                $import_page2 = $mPDF1->ImportPage($i);
                $mPDF1->UseTemplate($import_page2);

                if ($i < $pagecount2)
                    $mPDF1->AddPage();
            }
        }

        //$mPDF1->AddPage(); 
        if ($template['footer'] != '') {
           // $mPDF1->SetHTMLFooter('<div class="text-center" style="margin:-50px -60px !important; margin-bottom:-180px !important;">' . $template['footer']  . '</div>', '', true);
        }
        $mPDF1->Output('"' . $quotation_no . '".pdf', 'D');        

    }

    public function actiondownloadQuotationPdf($data){        
        
        $mPDF1 = Yii::app()->ePdf->mPDF();
        $mPDF1 = Yii::app()->ePdf->mPDF('', 'A4');
        $mPDF1->debug = true;
        $mPDF1->showImageErrors = true;                        
        $mPDF1->WriteHTML($data);            
        $path = Yii::getPathOfAlias('webroot') . '/documents/quotation/qgenerator.pdf';
        $mPDF1->Output($path, 'F');              
    }

    public function actionPdfGeneration() {
        $qid = $_POST['q_id'];
        $rev_no = $_POST['revision_no'];
        $model = SalesQuotationMaster::model()->find(array("condition" => "id='$qid'"));
        $quotation_no = $model->invoice_no;
        $item_data = $this->getCategoryItem($qid, $rev_no, $sub_status = 0);
        $sub_item = $this->getCategoryItem($qid, $rev_no, $sub_status = 1);
        $quotation_categories = $this->getQuotationCategory($qid, $rev_no);
        $item_model = $this->setQuotationsList($item_data, $sub_item, $model, $quotation_categories);
        $item_extra = $this->getExtraWork($qid, $rev_no);
        $materials = $this->getMaterials($qid);
        $compmodel = Company::model()->findByPk($model->company_id);
        $address = $compmodel->address;
        $mPDF1 = Yii::app()->ePdf->mPDF('', 'A4');
        $mPDF1->debug = true;
        $mPDF1->showImageErrors = true;
        $mPDF1->SetImportUse();

        // top files
        $sql  = "SELECT `id`,`top_bottom_image` FROM `jp_quotation_generator_image` WHERE type='0'";
        $image = Yii::app()->db->createCommand($sql)->queryAll(); 
        foreach ($image as $key => $value) {
            $pagecount = $mPDF1->SetSourceFile(realpath(Yii::app()->basePath . '/../uploads/quotation_generator/top-images/'.$value["top_bottom_image"]));            
            for ($i=1; $i<=$pagecount; $i++) {
                $import_page = $mPDF1->ImportPage($i);
                $mPDF1->UseTemplate($import_page);
        
                if ($i <= $pagecount)
                    $mPDF1->AddPage();
            }
        }        
        
        // middle content
        if ($model->template_type == '2') {
            $pdf = 'quotationpdf';
        }else if ($model->template_type == '5') {
            $pdf = 'concord_quotationpdf';
        }
        else{
            $real_theme_asset_url = realpath(Yii::app()->basePath . "/../themes/assets/"); 
            $image_url=$real_theme_asset_url . '/default-logo.png';    
           if(file_exists(Yii::app()->theme->basePath .'/images/quotation-file/logo.png')){
            $image_url=Yii::app()->theme->baseUrl .'/images/quotation-file/logo.png';
           }
            $pdf = 'agac_quotation_pdf';
            $mPDF1->SetWatermarkImage($image_url,0.2,array(59.266666667,26.458333333),P);
            $mPDF1->showWatermarkImage = true;
        }

        $activeProjectTemplate = $this->getActiveTemplate();
        if ($activeProjectTemplate == 'TYPE-8') {
            $pdf = 'agac_quotation_pdf_new';
        }
        
        $item_total = SalesQuotation::model()->find(
                array("condition" => "master_id = '$qid' AND revision_no ='$rev_no' "
                    . "AND deleted_status IN(1,2)",
                    'order' => 'id DESC '));
        $data = $this->renderPartial($pdf, array(
            'qtid'=>$qid,
            'rev_no'=>$rev_no,
            'model' => $model,
            'address' => $address,
            'itemmodel' => $item_model,
            'item_extra' => $item_extra,
            'materials' => $materials,
            'client' => $model->client_name,
            'total_taxable_amount' => $item_total->mainitem_total,
            'total_tax' => $item_total->mainitem_tax,
            'total_amoun_withtax' => $item_total->maintotal_withtax
                ), true);                 
            if ($model->template_type == '5') {   
                $mPDF1->SetHTMLHeader('<table class="details">
                <tbody>
                    <tr>
                        <td class="border-bottom border-right-0" colspan="3">'. CHtml::image($this->logo, Yii::app()->name,array("style" => "max-height:45px;"))
                                .'
                            </td>

                            <td class="border-bottom text-right"  colspan="3">
                                <h2>CONCORD <span class="orange_text">DESIGN STUDIO</span></h2>      
                                
                                <small>Emir Palaza, Near St.Francis Xaviers Church,</small>
                                <small>Kaloor Kadavanthra Road, Kathrikadavu, Kochi - 682 017</small>
                                <small>ph: 0484-234622, 4028208, m:interiors@concordkerala.com</small>
                                <small>www.concordkerala.com</small>
                            </td>                                
                    </tr>
                    
                    <tr>
                        <td width="28%"  class="border-bottom-0 border-right-0">Name Of Client</td>
                        <td class="border-bottom-0 border-right-0">:</td>
                        <td width="21%" class="border-bottom-0"><b> '.$model->client_name.'</b></td>
                        <td width="28%" class="border-bottom-0 border-right-0">Quotation/Invoice No</td>
                        <td class="border-bottom-0 border-right-0">:</td>
                        <td width="21%" class="border-bottom-0"><b> '.(isset($model->invoice_no) ? $model->invoice_no : "").'</b></td>
                    </tr>
                    <tr>
                        <td width="28%"  class="border-bottom-0 border-right-0">Flat No./House no./House Name</td>
                        <td class="border-bottom-0 border-right-0">:</td>
                        <td width="21%" class="border-bottom-0"><b> '.$model->house_name.'</b></td>
                        <td width="28%" class="border-bottom-0 border-right-0">Date</td>
                        <td class="border-bottom-0 border-right-0">:</td>
                        <td width="21%" class="border-bottom-0"><b> '.(isset($model->date_quotation) ? date('d-m-Y', strtotime($model->date_quotation)) : "").'</b></td>
                    </tr>
                    <tr>
                        <td width="28%"  class="border-bottom-0 border-right-0">Project Address</td>
                        <td class="border-bottom-0 border-right-0">:</td>
                        <td width="21%" class="border-bottom-0"><b style="display:inline"> '.$model->address.'</b></td>
                        <td width="28%" class="border-bottom-0 border-right-0">Project Type</td>
                        <td class="border-bottom-0 border-right-0">:</td>
                        <td width="21%" class="border-bottom-0"><b> '.(($model->project_type=='0')?"Residential":"Commercial").'</b></td>
                    </tr> 

                    <tr>
                        <td width="28%"  class="border-bottom-0 border-right-0">Site Address</td>
                        <td class="border-bottom-0 border-right-0">:</td>
                        <td width="21%" class="border-bottom-0"><b> '.$model->site_address.'</b></td>
                        <td width="28%" class="border-bottom-0 border-right-0">&nbsp;</td>
                        <td class="border-bottom-0 border-right-0"></td>
                        <td width="21%" class="border-bottom-0"></td>
                    </tr>
                    <tr>
                        <td width="28%"  class="border-bottom-0 border-right-0">Ph No</td>
                        <td class="border-bottom-0 border-right-0">:</td>
                        <td width="21%" class="border-bottom-0"><b> '.$model->phone_no.'</b></td>
                        <td width="28%" class="border-bottom-0 border-right-0">&nbsp;</td>
                        <td class="border-bottom-0 border-right-0"></td>
                        <td width="21%" class="border-bottom-0"></td>
                    </tr> 
                    <tr>
                        <td width="28%"  class="border-right-0">Email id </td>
                        <td class="border-bottom-0 border-right-0">:</td>
                        <td width="21%" class=""><b> '.$model->email.'</b></td>
                        <td width="28%" class="border-right-0">&nbsp;</td>
                        <td class="border-bottom-0 border-right-0"></td>
                        <td width="21%" class=""></td>
                    </tr> 
                                                                        
                </tbody>
            </table><br>'); 
            $mPDF1->AddPage('','', '', '', '', 10,10,70, 30, 10,0); 
        } 

        $this->actiondownloadQuotationPdf($data)  ;       
         
        $pagecount1 = $mPDF1->SetSourceFile(realpath(Yii::app()->basePath . '/../documents/quotation/qgenerator.pdf'));            
        for ($i=1; $i<=$pagecount1; $i++) {
            $import_page1 = $mPDF1->ImportPage($i);
            $mPDF1->UseTemplate($import_page1);
    
            if ($i <= $pagecount1)
            $mPDF1->AddPage();
        }
    
        // bottom files
        $sql  = "SELECT `id`,`top_bottom_image` FROM `jp_quotation_generator_image` WHERE type='1'";
        $image1 = Yii::app()->db->createCommand($sql)->queryAll(); 
        foreach ($image1 as $key => $value) {
            $pagecount2 = $mPDF1->SetSourceFile(realpath(Yii::app()->basePath . '/../uploads/quotation_generator/bottom-images/'.$value["top_bottom_image"]));            
            for ($i=1; $i<=$pagecount2; $i++) {
                $import_page2 = $mPDF1->ImportPage($i);
                $mPDF1->UseTemplate($import_page2);

                if ($i < $pagecount2)
                    $mPDF1->AddPage();
            }
        }
        
        $mPDF1->WriteHTML('');

        $newdate = date('Y-m-d');
        $path = Yii::getPathOfAlias('webroot') . '/documents/quotation/' . $quotation_no . '_' . $newdate . '.pdf';
        $mPDF1->Output($path, 'F');
        echo json_encode(array('path' => $path, 'name' => 'Quotation_' . $newdate . '.pdf'));
    }

    public function getMaterials($qid) {
        $criteria = new CDbCriteria();
        $criteria->select = "shutterwork_material,carcass_material,material,item_id";
        $criteria->condition = "master_id='" . $qid . "'";
        $material_data = SalesQuotation::model()->findAll($criteria);
        $data_array = array();
        foreach ($material_data as $key => $material) {
            $item_master = QuotationItemMaster::model()->findByPk($material['item_id']);            
            
            if ($material->shutterwork_material) {
                $data_array = array_merge($data_array, explode(',', $item_master['shutter_material_id']));
            }

            if ($material->carcass_material) {
                $data_array = array_merge($data_array, explode(',', $item_master['carcass_material_id']));
            }
            if ($material->material) {
                $data_array = array_merge($data_array, explode(',', $item_master['material_ids']));
            }
        }
        $material_ids = array_unique($data_array);
        $mainarray = array_chunk($material_ids, 2);

        return $material_ids;
    }

    public function actionGetdatasofworktype() {

        $html = '';
        $html = array('html' => '', 'status' => '', 'shutterdesc' => '',
            'carcassdesc' => '','shuttermaterial' => '','carcasmaterial' => '',
            'shutterfinish' => '','carcassfinish' => '','material' => '','finish' => '',
            'description' => '', 'item_id' => '');
        $cat_id = isset($_REQUEST['cat_id']) ? $_REQUEST['cat_id'] : '';        
        $worktypelabel = isset($_REQUEST['worktypelabel']) ? $_REQUEST['worktypelabel'] : '';        
        $worktypeid = isset($_REQUEST['worktypeid']) ? $_REQUEST['worktypeid'] : '';        
        $template_id = isset($_REQUEST['template_id']) ? $_REQUEST['template_id'] : '';        
        $criteria = new CDbCriteria();
        $criteria->select = "id,shutter_material_id, shutter_finish_id,carcass_material_id,"
                . "carcass_finish_id ,material_ids,finish_id,shutterwork_description,caracoss_description,description ";
        $criteria->condition = "quotation_category_id='" . $cat_id . "'"
                . "AND work_type_id ='" . $worktypeid . "'"
                . "AND worktype_label='" . $worktypelabel . "' ";
        $data = QuotationItemMaster::model()->findAll($criteria);        
        $sh_materialdata = '';
        $cc_materialdata = '';
        $material = '';
        $finish_data = '';
        $sh_finish = '';
        $cc_finish = '';
        $shidden = 'display:none';
        $deschidden = '';
        $additional_hidden = 'display:none';
        $colspan = 2;
        if(!empty($data)){
            if (1 == $template_id) {
                $sh_materialdata = Controller::getMaterialNames($data[0]['shutter_material_id']);
                $cc_materialdata = Controller::getMaterialNames($data[0]['carcass_material_id']);
                $finishdata = Controller::getFinishData();
                $sh_finish = isset($data[0]['shutter_finish_id']) ? $finishdata[$data[0]['shutter_finish_id']] : '';
                $cc_finish = isset($data[0]['carcass_finish_id']) ? $finishdata[$data[0]['carcass_finish_id']] : '';
                $shidden = '';
                $deschidden = 'display:none';
                $additional_hidden = 'display:none';
                $colspan = 7;
            } else if (3 == $template_id) {
                $material = Controller::getMaterialNames($data[0]['material_ids']);
                $finishdata = Controller::getFinishData();
                $finish_data = isset($data[0]['finish_id']) ? $finishdata[$data[0]['finish_id']] : '';
                $additional_hidden = '';
                $deschidden = '';
                $shidden = 'display:none';
                $colspan = 5;
            }
        }
        echo $this->renderPartial('worktypedata', array('data' => $data,
            'cat_id' => $cat_id, 'worktypelabel' => $worktypelabel,
            'worktypeid' => $worktypeid, 'sh_materialdata' => $sh_materialdata,
            'cc_materialdata' => $cc_materialdata, 'sh_finish' => $sh_finish,
            'cc_finish' => $cc_finish, 'material' => $material, 'finish_data' => $finish_data,
            'shidden' => $shidden, 'deschidden' => $deschidden, 'additional_hidden' => $additional_hidden,
            'colspan' => $colspan, 'html' => $html), true);
    }

    public function actionassignExecutive() {
        $tblpx = Yii::app()->db->tablePrefix;
        $status = 1;
        if (isset($_POST)) {
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $quotation_array = $_POST['id'];
                $req = $_POST['req'];
                $assignee = $_POST['assignee'];
                foreach ($quotation_array as $quotation_id) {
                    $sql = "UPDATE " . $tblpx . "sales_quotation_master "
                            . "SET sales_executive_id ='" . $assignee . "'"
                            . " WHERE id='" . $quotation_id . "'";
                    Yii::app()->db->createCommand($sql)->execute();
                }
                $transaction->commit();
            } catch (Exception $error) {
                $status = 0;
                $transaction->rollBack();
            } finally {
                if ($status == 0) {
                    $ret['status'] = 'error';
                    $ret['msg'] = 'Some Problem Occured in Sales Executive Assigning';
                } else {
                    $ret['status'] = 'success';
                    $ret['msg'] = 'Sales Executive Assigned Successfully';
                }

                echo json_encode($ret);
            }
        }
    }

    public function actionReport() {

        $model = new SalesQuotation;
        $tblpx = Yii::app()->db->tablePrefix;
        $new_query = "";
        $sales_executive = Salesexecutive::model()->findAll();
        $location = Location::model()->findAll();

        if (isset($_GET['executive']) || isset($_GET['location'])) {
            $executive = $_GET['executive'];
            $location_id = $_GET['location'];
            $datefrom = isset($_GET['date_from']) ? $_GET['date_from'] : "";
            $dateto = isset($_GET['date_to']) ? $_GET['date_to'] : "";

            if ($executive != '') {
                $new_query .= ' AND e.sales_executive_id =' . $executive . '';
            }
            if ($location_id != '') {
                $new_query .= ' AND e.location_id =' . $location_id . '';
            }
            if (!empty($datefrom) && empty($dateto)) {
                $new_query .= " AND e.date_quotation >= '" . date('Y-m-d', strtotime($datefrom)) . "'";
            } elseif (!empty($dateto) && empty($datefrom)) {
                $new_query .= " AND e.date_quotation <= '" . date('Y-m-d', strtotime($dateto)) . "'";
            } elseif (!empty($dateto) && !empty($datefrom)) {
                $new_query .= " AND e.date_quotation between '" . date('Y-m-d', strtotime($datefrom)) . "' and  '" . date('Y-m-d', strtotime($dateto)) . "'";
            }
        }

        $sql = "SELECT e.invoice_no,e.client_name,e.date_quotation,"
                . "l.name as location,s.name as executive,sum(q.amount_after_discount) as amount "
                . "FROM " . $tblpx . "sales_quotation_master e"
                . " LEFT JOIN " . $tblpx . "location l ON e.location_id = l.id"
                . " LEFT JOIN " . $tblpx . "salesexecutive s ON e.sales_executive_id = s.id"
                . " LEFT JOIN " . $tblpx . "sales_quotation q on e.id= q.master_id"
                . " WHERE 1=1 " . $new_query . " group by e.id order by e.id DESC";
        $result = Yii::app()->db->createCommand($sql)->queryAll();
        $final_array = array();
        foreach ($result as $key => $data) {
            $final_array[$key]['invoice_no'] = $data['invoice_no'];
            $final_array[$key]['client_name'] = $data['client_name'];
            $final_array[$key]['date_quotation'] = $data['date_quotation'];
            $final_array[$key]['location'] = $data['location'];
            $final_array[$key]['executive'] = $data['executive'];
            $final_array[$key]['amount'] = $data['amount'];
        }

        if (isset($_GET['date_from']) && isset($_GET['date_to']) && $_GET['date_to'] != '' && $_GET['date_from'] != '') {
            $datefrom = $_GET['date_from'];
            $dateto = $_GET['date_to'];
        } else {
            $datefrom = date("Y-") . "01-" . "01";
            $dateto = date("Y-m-d");
        }
        $this->render('report', array(
            'model' => $model,
            'sales_executive' => $sales_executive,
            'location' => $location,
            'datefrom' => $datefrom,
            'dateto' => $dateto,
            'final_array' => $final_array,
        ));
    }

    public function actionRemoveitem() {
        $data = $_REQUEST['data'];
        $id = $data['item_id'];
        $del_status = $data['del_status'];
        $model = SalesQuotation::model()->findByPk($id);
        $qid = $model->master_id;
        $rev_no = $model->revision_no;
        $deleted_status = 1;
        $msg = 'Item restored Successfully';
        if ($del_status == 1) {
            $deleted_status = 2;
            $msg = 'Item Deleted Successfully';
        }
        $transaction = Yii::app()->db->beginTransaction();
        try {
            $model->deleted_status = $deleted_status;
            if ($model->save()) {
                $success_status = 1;
            } else {
                $success_status = 0;
                throw new Exception(json_encode($model->getErrors()));
            }
            $transaction->commit();
        } catch (Exception $error) {
            $transaction->rollBack();
            $success_status = 0;
        } finally {

            if ($success_status == 1) {
                echo json_encode(array('response' => 'success', 'msg' => $msg));
            } else {
                echo json_encode(array('response' => 'error', 'msg' => 'Some Error Occured'));
            }
        }
    }

    public function getQuotationData($qid,$rev_no){
            
        $quotation_data = Yii::app()->db->createCommand()
          ->select('SUM(amount_after_discount) AS amount_after_discount,cgst_percent,sgst_percent,igst_percent')
          ->from($this->tableNameAcc('sales_quotation',0))
          ->where('master_id = "' . $qid . '" AND revision_no = "' .$rev_no. '" AND deleted_status IN(1,2)')
          ->queryAll();
        return $quotation_data;
    }

    public function actionEdititem() {
        $data = $_REQUEST['data'];
        $id = $data['item_id'];
        $type = $data['type'];
        $model = SalesQuotation::model()->findByPk($id);
        echo $this->renderPartial('_quotation_update', array(
            'model' => $model,
            'type' => $type
        ));
    }

    public function actionUpdateQuotationItems(){
        $result = '';
        $result_response = array();
        $error_response = '';
        $tblpx = Yii::app()->db->tablePrefix;
        if (isset($_POST)) {
            //echo '<pre>';print_r($_POST);exit;
            $quotation_id = isset($_POST['quotation_id']) ? $_POST['quotation_id'] : '';
            $revision_no = isset($_POST['rev_no']) ? $_POST['rev_no'] : '';
            $rev_no = 'REV-' . $revision_no;
            $revision_old = $revision_no - 1;
            $revision_no_old = 'REV-' . $revision_old;
            $tax_slab = isset($_POST['tax_slab']) ? $_POST['tax_slab'] : '';
            $sgst_percent = isset($_POST['sgst_percent']) ? $_POST['sgst_percent'] : '';
            $sgst_amount = isset($_POST['sgst_amount']) ? $_POST['sgst_amount'] : '';
            $cgst_percent = isset($_POST['cgst_percent']) ? $_POST['cgst_percent'] : '';
            $cgst_amount = isset($_POST['cgst_amount']) ? $_POST['cgst_amount'] : '';
            $igst_percent = isset($_POST['igst_percent']) ? $_POST['igst_percent'] : '';
            $igst_amount = isset($_POST['igst_amount']) ? $_POST['igst_amount'] : '';
            $mainitem_total = isset($_POST['mainitem_total']) ? $_POST['mainitem_total'] : '';
            $mainitem_tax = isset($_POST['mainitem_tax']) ? $_POST['mainitem_tax'] : '';
            $maintotal_withtax = isset($_POST['maintotal_withtax']) ? $_POST['maintotal_withtax'] : '';
            $data = SalesQuotation::model()->findAll(array('condition' => 'master_id = "' . $quotation_id . '" AND revision_no = "' . $rev_no . '" '));
            $data_count = count($data);
            $transaction = Yii::app()->db->beginTransaction();
            try{
                if ($data_count >= 1) {
                    SalesQuotation::model()->deleteAll(array('condition' => 'master_id = "' . $quotation_id . '" AND revision_no ="' . $rev_no . '" AND deleted_status IN(1,2)'));
                }

                $count = 0;
                if (isset($_POST['worktype_label'])) {
                    $count = count($_POST['worktype_label']);
                }

                for ($i = 0; $i < $count; $i++) {
                    if (!empty($_POST['worktype_label'][$i])) {
                        $model = new SalesQuotation;
                        $model->master_id = isset($_POST['master_id']) ? $_POST['master_id'] : '';
                        $model->revision_no = $rev_no;
                        if (isset($_POST['worktype_label'][$i])) {
                            $deleted_status = isset($_POST['deleted_status'][$i]) ? $_POST['deleted_status'][$i] : '';
                            $model->category_id = isset($_POST['cat_id'][$i]) ? $_POST['cat_id'][$i] : '';
                            $model->item_id = isset($_POST['item_id'][$i]) ? $_POST['item_id'][$i] : '';
                            $model->category_label =isset($_POST['category_label'][$i]) ?  trim($_POST['category_label'][$i]) : '';
                            $model->work_type = isset($_POST['work_type_id'][$i]) ? $_POST['work_type_id'][$i] : '';
                            $model->shutterwork_description = isset($_POST['shutterwork_description'][$i]) ? $_POST['shutterwork_description'][$i] : null;
                            $model->carcass_description = isset($_POST['carcass_description'][$i]) ? $_POST['carcass_description'][$i] : '';
                            $model->worktype_label = isset($_POST['worktype_label'][$i]) ? $_POST['worktype_label'][$i] : '';
                            $model->shutterwork_material = isset($_POST['shutterwork_material'][$i]) ? $_POST['shutterwork_material'][$i] : '';
                            $model->shutterwork_finish = isset($_POST['shutterwork_finish'][$i]) ? $_POST['shutterwork_finish'][$i] : '';
                            $model->carcass_material = isset($_POST['carcass_material'][$i]) ? $_POST['carcass_material'][$i] : '';
                            $model->carcass_finish = isset($_POST['carcass_finish'][$i]) ? $_POST['carcass_finish'][$i] : '';
                            $model->material = isset($_POST['material'][$i]) ? $_POST['material'][$i] : '';
                            $model->finish = isset($_POST['finish'][$i]) ? $_POST['finish'][$i] : '';
                            $model->description = isset($_POST['description'][$i]) ? $_POST['description'][$i] : '';
                            $model->item_id = isset($_POST['item_id'][$i]) ? $_POST['item_id'][$i] : '';
                            $model->unit = isset($_POST['unit'][$i]) ? $_POST['unit'][$i] : '';
                            $model->length = isset($_POST['length'][$i]) ? $_POST['length'][$i] : '';
                            $model->width = isset($_POST['width'][$i]) ? $_POST['width'][$i] : '';
                            $model->quantity = isset($_POST['quantity'][$i]) ? $_POST['quantity'][$i] : '';
                            $model->quantity_nos = isset($_POST['quantity_nos'][$i]) ? $_POST['quantity_nos'][$i] : '';
                            $model->mrp = isset($_POST['mrp'][$i]) ? $_POST['mrp'][$i] : '';
                            $model->sub_status = isset($_POST['sub_status'][$i]) ? $_POST['sub_status'][$i] : '';
                            $model->subitem_label = isset($_POST['subitem_label'][$i]) ? $_POST['subitem_label'][$i] : '';
                            if ($deleted_status == 2) {
                                $model->deleted_status = 0;
                            }
                            $model->amount_after_discount = isset($_POST['amount_after_discount'][$i]) ? $_POST['amount_after_discount'][$i] : '';
                            $model->tax_slab = $tax_slab;
                            $model->sgst_percent = $sgst_percent;
                            $model->sgst_amount = $sgst_amount;
                            $model->cgst_percent = $cgst_percent;
                            $model->cgst_amount = $cgst_amount;
                            $model->igst_percent = $igst_percent;
                            $model->igst_amount = $igst_amount;
                            $model->mainitem_total = $mainitem_total;
                            $model->mainitem_tax = $mainitem_tax;
                            $model->maintotal_withtax = $maintotal_withtax;
                            $model->revision_remarks = isset($_POST['SalesQuotation']['revision_remarks']) ? $_POST['SalesQuotation']['revision_remarks'] : '';
                            $model->created_by = Yii::app()->user->id;
                            $model->created_date = date('Y-m-d H:i:s');
                        }

                        if (!$model->save()) {
                            $result = "err";
                            throw new Exception('Error In Sales Quotation Update');
                        }
                    }
                }
                if (isset($_POST['SalesQuotation'])) {
                    $count = 0;
                    if (isset($_POST['SalesQuotation'])) {
                        $count = count($_POST['SalesQuotation']);
                    }

                    for ($i = 0; $i < $count; $i++) {
                        if (!empty($_POST['SalesQuotation'][$i]['description'])) {
                            $modelnew = new SalesQuotation;
                            $deleted_status = isset($_POST['SalesQuotation'][$i]['deleted_status']) ? $_POST['SalesQuotation'][$i]['deleted_status'] : '';
                            $modelnew->category_id = isset($_POST['SalesQuotation'][$i]['category_id']) ? $_POST['SalesQuotation'][$i]['category_id'] : '';
                            $modelnew->category_label = isset($_POST['SalesQuotation'][$i]['category_label']) ? $_POST['SalesQuotation'][$i]['category_label'] : '';
                            $modelnew->master_id = $_POST['master_id'];
                            $modelnew->revision_no = $rev_no;
                            $modelnew->work_type = "";
                            $modelnew->description = $_POST['SalesQuotation'][$i]['description'];
                            $modelnew->unit = $_POST['SalesQuotation'][$i]['unit'];
                            $modelnew->length = isset($_POST['SalesQuotation'][$i]['length'])?$_POST['SalesQuotation'][$i]['length']:'';
                            $modelnew->width = isset($_POST['SalesQuotation'][$i]['width'])?$_POST['SalesQuotation'][$i]['width']:'';
                            $modelnew->quantity = $_POST['SalesQuotation'][$i]['quantity'];
                            $modelnew->quantity_nos = $_POST['SalesQuotation'][$i]['quantity_nos'];
                            $modelnew->mrp = $_POST['SalesQuotation'][$i]['mrp'];
                            $modelnew->amount_after_discount = $_POST['SalesQuotation'][$i]['amount_after_discount'];
                            $modelnew->tax_slab = $tax_slab;
                            $modelnew->sgst_percent = $sgst_percent;
                            $modelnew->sgst_amount = $sgst_amount;
                            $modelnew->cgst_percent = $cgst_percent;
                            $modelnew->cgst_amount = $cgst_amount;
                            $modelnew->igst_percent = $igst_percent;
                            $modelnew->igst_amount = $igst_amount;
                            $modelnew->mainitem_total = $mainitem_total;
                            $modelnew->mainitem_tax = $mainitem_tax;
                            $modelnew->maintotal_withtax = $maintotal_withtax;
                            if ($deleted_status == 2) {
                                $modelnew->deleted_status = 0;
                            }
                            $modelnew->revision_remarks = isset($_POST['SalesQuotation']['revision_remarks']) ? $_POST['SalesQuotation']['revision_remarks'] : '';
                            $modelnew->created_by = Yii::app()->user->id;
                            $modelnew->created_date = date('Y-m-d H:i:s');
                            if (!$modelnew->save()) {
                                $result = "err";
                                throw new Exception('Error In Sales Quotation Extra Work Update');
                            }
                        }
                    }
                }
                if ($result == '') { 

                    if ($revision_old != '') {
                        $update = SalesQuotation::model()->updateAll(array('revision_status' => 1), 'master_id=:qid AND revision_no=:rev_no', array(':qid' => $quotation_id, ':rev_no' => $revision_no_old));
                    }
                    $update = SalesQuotationMaster::model()->updateAll(array('revision_no' => $rev_no), 'id=:master_id', array(':master_id' => $quotation_id));
                }
                $transaction->commit();
            } catch (Exception $error) {
                $error_response = $error->getMessage();
                $transaction->rollBack();
            } finally {

                if ($result == "") {
                    $result_response['success'] = 1;
                } else if ($result == "err") {
                    $result_response['success'] = 0;
                    $result_response['error'] = $error_response;
                }
                echo json_encode($result_response);
            }
        }
    }

    public function actionRevisions($qid) {
        $condition ='';
        if(!isset($_REQUEST['checked'])){
            $condition = ' AND p.revision_delete_status="1"';
        }
       $location_data = Controller::getLocationData();
       $quotation_data = SalesQuotationMaster::model()->findByPk($qid);
       $revision_no = $this->getRevisionno($qid);
       $criteria = new CDbCriteria();
       $criteria->select = 't.*,p.revision_no,p.revision_approved';
       $criteria->distinct=true;
       $criteria->join = 'LEFT JOIN ' . $this->tableNameAcc('sales_quotation',0) . ' p ON t.id = p.master_id';
       $criteria->condition = 't.id = "' .$qid. '" '.$condition;
       $sales_quotation = SalesQuotationMaster::model()->findAll($criteria);
       $this->render('revision_listing', array(
            'model' => $sales_quotation,
           'location_data' => $location_data,
           'quotation_data' => $quotation_data,
           'revision_no' =>$revision_no
       ));
    }
    
    public function actionApproveRevision() {
        $result = array();
        $error_response = '';
        $qid = isset($_POST['quotation_id']) ? $_POST['quotation_id'] : '';
        $rev_no = isset($_POST['revision_no']) ? $_POST['revision_no'] : '';
        $transaction = Yii::app()->db->beginTransaction();
        try {
            $sales_update_old = SalesQuotation::model()->updateAll(array('revision_approved' => 0), 'master_id=:qid', array(':qid' => $qid));
            $sales_update = SalesQuotation::model()->updateAll(array('revision_approved' => 1), 'master_id=:qid AND revision_no=:rev_no', array(':qid' => $qid, ':rev_no' => $rev_no));
            $sales_master = SalesQuotationMaster::model()->updateAll(array('revision_no' => $rev_no), 'id=:master_id', array(':master_id' => $qid));
            $transaction->commit();
        } catch (Exception $ex) {
            $error = $ex->getMessage();
            $error_response = "err";
            $transaction->rollBack();
        } finally {
            if ($error_response == "") {
                $result['success'] = 1;
                $result['msg'] = 'Approved Successfully';
            } else {
                $result['success'] = 0;
                $result['msg'] = 'Something Went wrong in approval';
            }
            echo json_encode($result);
        }
    }
    
    public function actiongetWorktypeLabel(){
        $worktype = $_POST["worktype"];
        $cat_id = $_POST["cat_id"];
        $tblpx = Yii::app()->db->tablePrefix;
        $html = array();
        $html['labellist'] = '';
        $html['status'] = '';
        if($worktype !=""){
            $work_type_label = QuotationWorktypeMaster::model()->findByPk($worktype);
            $query = 'work_type_id =' . $worktype . ' AND quotation_category_id =' . $cat_id . '';
            $item_label = QuotationItemMaster::model()->findAll(array('condition' => $query));
        }        
        if (!empty($item_label)) {
            $html['labellist'] .= '<option value="">Select Label</option>';
            foreach ($item_label as $key => $value) {
                $label = $value['worktype_label'];
                $html['labellist'] .= "<option value='".$label."' data-template = '".$work_type_label['template_id']."'>" . $value['worktype_label'] . "</option>";
                $html['status'] = 'success';
            }
        } else {
            $html['status'] = 'no_success';
            $html['labellist'] .= '<option value="">No Label</option>';
        }
        echo json_encode($html);
    }

    public function actioncreatesection(){                 
        $sec_id = $_POST['sec_id'];
        
        $model =  QuotationSection::model()->findByPk($sec_id);           

        if($sec_id == ""){
            $model = new QuotationSection;
            $model->created_date = date('Y-m-d H:i:s');
            $model->created_by = Yii::app()->user->id;
        }
        $model->revision_no = 'REV-'.$_POST['revision_no'];
        
        if($_POST['status']== 1 && $sec_id != ""){ //edit page and edit details sec
            $model->revision_no = $_POST['revision_no'];
        }
        
        $model->attributes = $_POST; 
                   
        if($model->save()){
            Yii::app()->user->setFlash('success', "Successfully Created");
            $total_amount = Controller::getQuotationSum($_POST['qtn_id'], $model->revision_no,NULL); 
            if($_POST['status'] == 0){
                $sql ='SELECT count(*) as count ,id '
                        . 'FROM jp_quotation_revision '
                        . ' WHERE qid='.$model->qtn_id
                        . ' AND revision_no="'.$model->revision_no.'"'; 
                $revision_model_count = Yii::app()->db->createCommand($sql)->queryRow();
                
                $revision_model = new QuotationRevision();
                if($revision_model_count['count'] > 0) {
                    $revision_model = QuotationRevision::model()->findByPk($revision_model_count['id']);
                }
    
                $revision_model->qid = $model->qtn_id;
                $revision_model->revision_no = $model->revision_no;
                $revision_model->save();
            }
            
            echo json_encode(array(
                'id'=>$model->id,
                'total_amount' => Controller::money_format_inr($total_amount, 2),
                'data_val'=>$total_amount
            ));                                    
        }

    }

    public function actioncreatecategorylabel(){         
        $cat_label_id = isset($_POST['cat_label_id'])?$_POST['cat_label_id']:"";
        if ($this->getActiveTemplate() == 'TYPE-7' || $this->getActiveTemplate() == 'TYPE-8'){
            $cat_label_id = isset($_POST['sub_section_id'])?$_POST['sub_section_id']:""; 
		}else if($this->getActiveTemplateID() >8){
            $cat_label_id = isset($_POST['sub_section_id'])?$_POST['sub_section_id']:""; 
		}        
        $model =  QuotationGenCategory::model()->findByPk($cat_label_id);           

        if($cat_label_id == ""){
            $model = new QuotationGenCategory;
            $model->created_date = date('Y-m-d H:i:s');
            $model->created_by = Yii::app()->user->id;
        }

        $sql ="SELECT revision_no FROM `jp_quotation_section` WHERE id =". $_POST['section_id'];
        if ($this->getActiveTemplate() == 'TYPE-7' || $this->getActiveTemplate() == 'TYPE-8'){
            $sql ="SELECT revision_no FROM `jp_quotation_section` WHERE id =". $_POST['sec_id'];
        }else if($this->getActiveTemplateID() >8){
            $sql ="SELECT revision_no FROM `jp_quotation_section` WHERE id =". $_POST['sec_id'];
        }
        $revision_no = Yii::app()->db->createCommand($sql)->queryScalar();
        $model->revision_no = $revision_no;
        
        
        $model->attributes = $_POST;
        $model->quantity_nos = isset($_POST['quantity_nos'])?$_POST['quantity_nos']:"";
        if($model->save()){
            $total_amount = Controller::getQuotationSum($_POST['qid'], $model->revision_no,NULL);
            Yii::app()->user->setFlash('success', "Successfully Created");
            echo json_encode(array(
                'id'=>$model->id,
                'total_amount' => Controller::money_format_inr($total_amount, 2),
                'data_val'=>$total_amount,
                'section'=>$model->section_id
            ));
        }

    }

    public function actioncreateworktypelabel(){
                
        $worktype_label_id = $_POST['worktype_label_id'];
        $model =  QuotationGenWorktype::model()->findByPk($worktype_label_id);           

        if($worktype_label_id == ""){
            $model = new QuotationGenWorktype;
            $model->created_date = date('Y-m-d H:i:s');
            $model->created_by = Yii::app()->user->id;
        }

        $sql ="SELECT revision_no FROM `jp_quotation_gen_category` WHERE id =". $_POST['category_label_id'];
        $revision_no = Yii::app()->db->createCommand($sql)->queryScalar();
        $model->revision_no = $revision_no;
        
        $model->attributes = $_POST;
        $model->quantity_nos  = isset($_POST['quantity_nos'])?$_POST['quantity_nos']:0;
        
        if($model->save()){
            $total_amount = Controller::getQuotationSum($_POST['qid'], $model->revision_no,NULL);
            Yii::app()->user->setFlash('success', "Successfully Created");
            echo json_encode(array(
                'id'=>$model->id,
                'section'=>$model->section_id,
                'sub'=>$model->category_label_id,
                'total_amount' => Controller::money_format_inr($total_amount, 2),
                'data_val'=>$total_amount
            ));
            
        }else{
            Yii::app()->user->setFlash('error', "Some errorn Occured");
        }

    }

    public function actionaddsalesQuotationItems(){

        $data = $_REQUEST['data'];        
        $rev_no = 'REV-'.$_POST['revision_no'];
        $quotation_id = $_POST['master_id'] ;

        $revdata = SalesQuotation::model()->findAll(array('condition' => 'master_id = "' . $quotation_id . '" AND revision_no = "' . $rev_no . '" AND `revision_update_status` = "0"'));
        $data_count = count($data);
                        
        if ($data_count >= 1) {
            SalesQuotation::model()->deleteAll(array('condition' => 'master_id = "' . $quotation_id . '" AND revision_no ="' . $rev_no . '" AND `revision_update_status` = "0" AND deleted_status IN(1,2)'));
        }
        foreach ($data as $key => $value) {
            $model = new SalesQuotation();
            $model->attributes = $value;  
            $model->sub_status =0; 
            $model->master_id =$value['master_id'];              
            $model->created_by = Yii::app()->user->id;
            $model->created_date = date('Y-m-d');
            $model->revision_no ='REV-'.$value['revision_no']; 
            $model->parent_type =$value['parent_type']; 
            $model->parent_id =$value['parent_id']; 
            
            if (!$model->save()) {                    
                echo json_encode(array('response' => 'error', 'msg' => 'Some Error occured'));
            } else { 
                if($_POST['status'] == 1){
                    $sql = 'SELECT count(*) as count ,id '
                            . ' FROM jp_quotation_revision '
                            . ' WHERE qid='.$model->master_id
                            . ' AND revision_no="'.$model->revision_no.'"';
                    $revision_model_count = Yii::app()->db->createCommand($sql)->queryRow();
                    
                    $revision_model = new QuotationRevision();
                    if($revision_model_count['count'] > 0) {
                        $revision_model = QuotationRevision::model()->findByPk($revision_model_count['id']);
                    }
        
                    $revision_model->qid = $model->master_id;
                    $revision_model->revision_no = $model->revision_no;
                    $revision_model->save();
                } 
                $total_amount = Controller::getQuotationSum($model->master_id, $model->revision_no,NULL);

                echo json_encode(array(
                    'response' => 'success', 
                    'msg' => 'Quotation Items saved to new revision',
                    'total_amount' => Controller::money_format_inr($total_amount, 2),
                    'data_val'=>$total_amount
                ));
            }
        } 
        
        $update = SalesQuotationMaster::model()->updateAll(array('revision_no' => $rev_no), 'id=:master_id', array(':master_id' => $quotation_id));                           
    }

    
    public function actionDiscardRevision() {
        $result = array();
        $error_response = '';
        $qid = isset($_POST['quotation_id']) ? $_POST['quotation_id'] : '';
        $rev_no = isset($_POST['revision_no']) ? $_POST['revision_no'] : '';
        $transaction = Yii::app()->db->beginTransaction();
        try {            
            $sales_update = SalesQuotation::model()->updateAll(array('revision_delete_status' => '0'), 'master_id=:qid AND revision_no=:rev_no', array(':qid' => $qid, ':rev_no' => $rev_no));            
            $transaction->commit();
        } catch (Exception $ex) {
            $error = $ex->getMessage();
            $error_response = "err";
            $transaction->rollBack();
        } finally {
            if ($error_response == "") {
                $result['success'] = 1;
                $result['msg'] = 'Deleted Successfully';
            } else {
                $result['success'] = 0;
                $result['msg'] = 'Something Went wrong in deletion';
            }
            echo json_encode($result);
        }
    }

    public function actionsaveRevisionData(){        
        $revision = 'REV-'.$_POST['rev'];
        $sql = 'SELECT count(*) as count ,id '
            . ' FROM jp_quotation_revision '
            . ' WHERE qid='.$_POST['qid']
            .' AND revision_no="'.$revision.'"';
        $revision_model_count = Yii::app()->db->createCommand($sql)->queryRow();
            
        $revision_model = new QuotationRevision();
        if($revision_model_count['count'] > 0) {
            $revision_model = QuotationRevision::model()->findByPk($revision_model_count['id']);
        }else{
            $revision_model->qid =isset($_POST['qid'])?$_POST['qid']:null;
            $revision_model->revision_no =$revision; 
        
        }

        $revision_model->tax_slab =isset($_POST['tax_slab'])?$_POST['tax_slab']:0 ; 
        $revision_model->sgst_percent =isset($_POST['sgst_percent'])?$_POST['sgst_percent']:0 ;        
        $revision_model->sgst_amount =isset($_POST['sgst_amount'])?$_POST['sgst_amount']:0 ; 
        $revision_model->cgst_percent =isset($_POST['cgst_percent'])?$_POST['cgst_percent']:0 ; 
        $revision_model->cgst_amount =isset($_POST['cgst_amount'])?$_POST['cgst_amount']:0 ; 
        $revision_model->igst_percent =isset($_POST['igst_percent'])?$_POST['igst_percent']:0 ; 
        $revision_model->igst_amount =isset($_POST['igst_amount'])?$_POST['igst_amount']:0 ; 
        $revision_model->discount_precent =isset($_POST['discount_percent'])?$_POST['discount_percent']:0 ; 
        $revision_model->discount_amount =isset($_POST['discount_amount'])?$_POST['discount_amount']:0 ; 
        $revision_model->total_after_additionl_discount =(isset($_POST['total_after_additionl_discount']) && (float)$_POST['total_after_additionl_discount'] > 0)?$_POST['total_after_additionl_discount']:NULL;

        if($revision_model->save()){
            $total_amount = Controller::getQuotationSum($_POST['qid'], $revision,NULL); 
 
            echo json_encode(array(
                'tax_slab' => $revision_model->tax_slab,
                'sgst_percent' => $revision_model->sgst_percent,
                'sgst_amount'  => $revision_model->sgst_amount,
                'cgst_percent' => $revision_model->cgst_percent,
                'cgst_amount'  => $revision_model->cgst_amount,
                'igst_percent' => $revision_model->igst_percent,
                'igst_amount'  => $revision_model->igst_amount,
                'discount_precent' => $revision_model->discount_precent,
                'discount_amount'  => $revision_model->discount_amount,
                'total_amount'=>Controller::money_format_inr($total_amount, 2),
                'post_discount' =>(isset($_POST['total_after_additionl_discount']) && (float)$_POST['total_after_additionl_discount'] > 0)?$_POST['total_after_additionl_discount']:NULL

                ));            
        }else{
           //echo "<pre>";print_R($revision_model->getErrors());die; 
        }
    }

    public function actiongetWorkType(){
        
        $sql = 'SELECT i.work_type_id,w.name  '
        . ' FROM `jp_quotation_item_master` i '
        . ' LEFT JOIN jp_quotation_worktype_master w '
        . ' ON  i.work_type_id = w.id  '
        . ' WHERE `quotation_category_id` = '.$_POST['cat_id']
        . ' GROUP BY i.work_type_id';
        $worktype = Yii::app()->db->createCommand($sql)->queryAll();

        $selection="";
        $empty = array('empty'=>'-Select Work Type-'); 
        $options = CHtml::listData($worktype, 'work_type_id', 'name');
        $workTypeData =  CHtml::listOptions($selection,$options , $empty);  
        
        echo json_encode(array('workTypeData' => $workTypeData));
        
    }

    public function actiongetWorkTypeLabelArray(){           
        $workTypeMaster = QuotationWorktypeMaster::model()->findByPk($_POST["workType_id"]) ;        
        $newQuery = 'work_type_id =' . $_POST["workType_id"] . ' AND quotation_category_id =' . $_POST["category_id"];                                
        $selection="";
        $empty = array('empty'=>'-Select Worktype Label-'); 
        $options = CHtml::listData(QuotationItemMaster::model()->findAll(array('condition' => $newQuery)), 'worktype_label', 'worktype_label');
        
        $workTypeLabelData =  CHtml::listOptions($selection,$options , $empty);  
        
        echo json_encode(array('workTypeLabelData' => $workTypeLabelData,'template'=>$workTypeMaster['template_id']));
        
    }

    public function actionquotationPreview(){
        $rev = $_POST['rev'];
        $qtid = $_POST['qtid'];
        $model = SalesQuotationMaster::model()->findByPk($qtid);
        if ($model->template_type == '2') {
            $file = 'quotationpreview';
        } else if($model->template_type == '5'){
            $file = 'quotationpreview_concord';
        }

        if ($this->getActiveTemplate() == 'TYPE-7' || $this->getActiveTemplate() == 'TYPE-8'){
            $file = 'quotationpreview1';
		}else if($this->getActiveTemplateID() >8){
            $file = 'quotationpreview1';
		}
        //echo "<pre>";print_R($_POST);print_r($_REQUEST);die;
        $data = $this->renderPartial($file,array('model'=>$model,'qtid'=>$qtid,'rev_no'=>$rev,'status'=>$_REQUEST['status'],'interRev'=>$_REQUEST['interRev']));
        return $data;
        
    }
    
    public function actionremoveOldQuotationData(){
        $revision_no = isset($_POST['revision_no']) ? $_POST['revision_no'] : '';
        $rev_no = "REV-" . $revision_no;

        $where="";
        $where1 = "";
        $activeProjectTemplate = $this->getActiveTemplate();
        if ($activeProjectTemplate != 'TYPE-5') {
        $subids = array();   
            $subArray = Yii::app()->db->createCommand('SELECT id FROM `jp_quotation_gen_worktype` 
                        WHERE `category_label_id` = '.$_POST['subsec_id'])->queryAll();
            
            
            foreach ($subArray as $key => $value) {
                $subids[] = $value['id'];
            }

            $sub_ids = implode(",", $subids);
            
            if ($sub_ids != '') {
                $where = ' AND parent_id IN(' . $sub_ids . ')';
            }
        }else{
            $where = ' AND parent_id ='.$_POST['subsec_id'];
        }
        if ($_POST['type'] == 1) {
			$where1 = ' AND category_id = '.$_POST['cat_id'];
		}

        $sql = "SELECT count(*) as count FROM `jp_sales_quotation` "
            . " WHERE `master_id` = ".$_POST['master_id']            
            . " AND `parent_type` = 1 ". $where.$where1 
            . " AND revision_no ='" . $rev_no."'";          
        
        $olddata = Yii::app()->db->createCommand($sql)->queryScalar();
        
        if($olddata > 0) { 
                      
            $sql = "DELETE FROM  `jp_sales_quotation` "
                . " WHERE `master_id` = ".$_POST['master_id']                
                . " AND `parent_type` = 1 ". $where.$where1  
                . " AND revision_no ='" . $rev_no."'";
                
            $data =  Yii::app()->db->createCommand($sql);
            if($data->execute()){
                return 2;
            }else{
                return 1;
            }
        }else{
            
            return 2;
        }
        
    }

    public function actionupdateRevisionStatus(){
       
        $rev_no = 'REV-'.$_POST['rev'];
        $quotation_id = $_POST['qtid'] ;

        $revdata = SalesQuotation::model()->findAll(array('condition' => 'master_id = "' . $quotation_id . '" AND revision_no = "' . $rev_no . '" AND `revision_update_status` = "1"'));
        $data_count = count($revdata);
                        
        if ($data_count >= 1) {
            $sales_update = SalesQuotation::model()->updateAll(array('revision_update_status' => "0"), 'master_id=:qid AND revision_no=:rev_no', array(':qid' => $quotation_id, ':rev_no' => $rev_no));
            if($sales_update){
                return "successfully updated revision_update_status";
            }
        }
        
    }

    public function actiontermsandconditions(){
        $sql = "SELECT template_id FROM `jp_project_template` WHERE status = '1'";
        $templateId = Yii::app()->db->createCommand($sql)->queryScalar();

        $termsql = "SELECT * FROM `jp_terms_conditions`";
        $terms = Yii::app()->db->createCommand($termsql)->queryRow();
        
        echo $this->render('termsandconditions',array('terms'=>$terms));
    }

    public function actionsaveterms(){
        
        $model = new  TermsConditions;
        if(isset($_POST['term_id']) && $_POST['term_id'] !=""){
            $model = TermsConditions::model()->findByPk($_POST['term_id']);
        }
        
        $model->terms_and_conditions = $_POST['terms'];
        $model->template_id = 1;
        if($model->save()){
            echo "Terms and conditions Updated Successfully";
        }else{
            echo "<pre>";
            print_r($model->getErrors());exit;
        }
    }

    public function actionsaveInterRevision(){
        
        $sql = 'SELECT * FROM `jp_quotation_section` '
                . 'WHERE `qtn_id` = ' . $_REQUEST['qtn_id']
                . ' AND revision_no = "' . $_REQUEST['selected_revision'] . '"';
        $sectionArray = Yii::app()->db->createCommand($sql)->queryAll();
//section
        foreach($sectionArray as $section){
            $old_sec_Id = $section['id'];
            $secmodel = new QuotationSection;
            $secmodel->created_date = date('Y-m-d H:i:s');
            $secmodel->created_by = Yii::app()->user->id;
            $secmodel->revision_no = $_REQUEST['inter_rev'];
            $secmodel->attributes = $section;

            if($secmodel->save()){
                $LastInsertedSectionId = $secmodel->id; 
                // Items
                $sql = 'SELECT * FROM `jp_sales_quotation` '
                        . 'WHERE `parent_type` = 0 AND `parent_id` = ' . $old_sec_Id
                        . ' AND revision_no="' . $_REQUEST['selected_revision'] . '"';
                $wrktypeitemArray = Yii::app()->db->createCommand($sql)->queryAll();

                foreach($wrktypeitemArray as $worktype){
                    $wrktypemodel = new SalesQuotation;
                    $wrktypemodel->attributes = $worktype;  
                    $wrktypemodel->sub_status =0;  
                    $wrktypemodel->master_id  = $_REQUEST['qtn_id'];                                               
                    $wrktypemodel->created_by = Yii::app()->user->id;
                    $wrktypemodel->created_date = date('Y-m-d');
                    $wrktypemodel->revision_no =$_REQUEST['inter_rev']; 
                    $wrktypemodel->parent_type = 0 ;                                     
                    $wrktypemodel->parent_id =$LastInsertedSectionId; 
                    $wrktypemodel->image_gallery_id = $worktype['image_gallery_id'];
                    if($wrktypemodel->save()){
                                                                                
                    }

                }

                $sql = 'SELECT * FROM `jp_quotation_gen_category` '
                    . ' WHERE `qid` = ' . $_REQUEST['qtn_id'] 
                    . ' AND `section_id` = ' . $old_sec_Id
                    . ' AND revision_no = "' . $_REQUEST['selected_revision'] . '"';
                $subSectionArray = Yii::app()->db->createCommand($sql)->queryAll();
//subsection
                foreach($subSectionArray as $subsection){
                    
                    $old_sub_sec_Id = $subsection['id'];
                    $subSecmodel = new QuotationGenCategory;
                    $subSecmodel->created_date = date('Y-m-d H:i:s');
                    $subSecmodel->created_by = Yii::app()->user->id;        
                    $subSecmodel->revision_no = $_REQUEST['inter_rev'];
                    $subSecmodel->attributes = $subsection;
                    $subSecmodel->section_id = $LastInsertedSectionId;
                                        
                    if($subSecmodel->save()){
                        $LastInsertedSubSectionId = $subSecmodel->id; 
                        $sql = 'SELECT * FROM `jp_quotation_gen_worktype` '
                                . ' WHERE `qid` = ' . $_REQUEST['qtn_id'] 
                                . ' AND `section_id` = ' . $old_sec_Id
                                . ' AND `category_label_id` = ' . $old_sub_sec_Id
                                . ' AND revision_no = "' . $_REQUEST['selected_revision'] . '"';
                        $subitemArray = Yii::app()->db->createCommand($sql)->queryAll();
//subitem        
                        foreach($subitemArray as $subitem){
                            $old_sub_item_Id = $subitem['id'];
                            $subItemmodel = new QuotationGenWorktype;
                            $subItemmodel->created_date = date('Y-m-d H:i:s');
                            $subItemmodel->created_by = Yii::app()->user->id;                        
                            $subItemmodel->revision_no = $_REQUEST['inter_rev'];                            
                            $subItemmodel->attributes = $subitem; 
                            $subItemmodel->section_id = $LastInsertedSectionId;
                            $subItemmodel->category_label_id = $LastInsertedSubSectionId;

                            if($subItemmodel->save()){
                                $LastInsertedSubItemId = $subItemmodel->id;
                                $sql = 'SELECT * FROM `jp_sales_quotation` '
                                        . 'WHERE `parent_type` = 1 AND `parent_id` = ' . $old_sub_item_Id
                                        . ' AND revision_no="' . $_REQUEST['selected_revision'] . '"';
                                $wrktypeitemArray = Yii::app()->db->createCommand($sql)->queryAll();
//worktype                                
                                foreach($wrktypeitemArray as $worktype){
                                    $wrktypemodel = new SalesQuotation;
                                    $wrktypemodel->attributes = $worktype;  
                                    $wrktypemodel->sub_status =0;  
                                    $wrktypemodel->master_id  = $_REQUEST['qtn_id'];
                                    $wrktypemodel->item_id = $worktype ['item_id'];
                                    $wrktypemodel->item_name = $worktype['item_name'];                                     
                                    $wrktypemodel->created_by = Yii::app()->user->id;
                                    $wrktypemodel->created_date = date('Y-m-d');
                                    $wrktypemodel->revision_no =$_REQUEST['inter_rev']; 
                                    $wrktypemodel->parent_type = 1 ;                                     
                                    $wrktypemodel->parent_id =$LastInsertedSubItemId; 
                                    $wrktypemodel->image_gallery_id = $worktype['image_gallery_id'];   
                                    if($wrktypemodel->save()){
// quotation revision                                                                                
                                    }

                                }


                            }
        
                        }                                
                    }

                }                
            }            
        }       
    }

    public function actionupdateInterRevision(){

        $revision_no = 'REV-'.$_REQUEST['new_rev']; 
        if(!empty($_REQUEST['new_rev'])){
            $transaction = Yii::app()->db->beginTransaction();
            try {

                $sql1 = 'UPDATE `jp_quotation_section` '
                . ' SET `revision_no`="'.$revision_no.'" '
                . ' WHERE  `qtn_id` ='. $_REQUEST['qtid']
                . ' AND `revision_no` =  "'.$_REQUEST['rev'].'"';
                $data1 =  Yii::app()->db->createCommand($sql1);
                if($data1->execute()){
                    $response = 1;
                    $sql4 = 'UPDATE `jp_sales_quotation` '
                            . ' SET `revision_no`="'.$revision_no.'" '
                            . ' WHERE`master_id`= '. $_REQUEST['qtid']
                            . ' AND revision_no = "'.$_REQUEST['rev'].'"';
                    $data4 =  Yii::app()->db->createCommand($sql4);
                    
                    if($data4->execute()){
                        $response = 1;
                        $sql ='SELECT count(*) as count ,id '
                                . 'FROM jp_quotation_revision '
                                . ' WHERE qid='.$_REQUEST['qtid']
                                . ' AND revision_no="'.$revision_no.'"'; 
                        $revision_model_count = Yii::app()->db->createCommand($sql)->queryRow();
                                
                        $revision_model = new QuotationRevision();
                        if($revision_model_count['count'] > 0) {
                            $revision_model = QuotationRevision::model()->findByPk($revision_model_count['id']);
                        }
                    
                        $revision_model->qid = $_REQUEST['qtid'];
                        $revision_model->revision_no = $revision_no;
                        if($revision_model->save()){
                            $response = 1;
                            $update = SalesQuotationMaster::model()->updateAll(array('revision_no' => $revision_no), 'id=:master_id', array(':master_id' => $_REQUEST['qtid'])); 
                            if(!$update){
                                $response = 0;
                            }                          
                        };                            
                    }
                    $sql2 = 'UPDATE `jp_quotation_gen_category` '
                        . ' SET `revision_no`="'.$revision_no.'" '
                        . ' WHERE `qid` ='. $_REQUEST['qtid']
                        . '  AND `revision_no`=  "'.$_REQUEST['rev'].'"';
                    $data2 =  Yii::app()->db->createCommand($sql2);
                    if($data2->execute()){
                        $response = 1;
                        $sql3 = 'UPDATE `jp_quotation_gen_worktype` '
                            . ' SET `revision_no`="'.$revision_no.'" '
                            . ' WHERE `qid` ='. $_REQUEST['qtid']
                            . ' AND `revision_no` = "'.$_REQUEST['rev'].'"';
                        $data3 =  Yii::app()->db->createCommand($sql3);
                        if($data3->execute()){
                            $response = 1;
                            $sql5 = 'UPDATE `jp_sales_quotation` '
                                . ' SET `revision_no`="'.$revision_no.'" '
                                . ' WHERE`master_id`= '. $_REQUEST['qtid']
                                . ' AND revision_no = "'.$_REQUEST['rev'].'"';
                            $data5 =  Yii::app()->db->createCommand($sql4);
                            if($data5->execute()){
                                $response = 1;
                                $sql ='SELECT count(*) as count ,id '
                                . 'FROM jp_quotation_revision '
                                . ' WHERE qid='.$_REQUEST['qtid']
                                . ' AND revision_no="'.$revision_no.'"'; 
                                $revision_model_count = Yii::app()->db->createCommand($sql)->queryRow();
                                
                                $revision_model = new QuotationRevision();
                                if($revision_model_count['count'] > 0) {
                                    $revision_model = QuotationRevision::model()->findByPk($revision_model_count['id']);
                                }
                    
                                $revision_model->qid = $_REQUEST['qtid'];
                                $revision_model->revision_no = $revision_no;
                                if($revision_model->save()){
                                    $response = 1;
                                    $update = SalesQuotationMaster::model()->updateAll(array('revision_no' => $revision_no), 'id=:master_id', array(':master_id' => $_REQUEST['qtid'])); 
                                    if(!$update){
                                        $response = 0;
                                    }                          
                                };
                                

                            }
                        }
                    }

                }
                $transaction->commit();
            } catch (CDbException $e) {
                $transaction->rollBack();
                $response = 0;
                $error =  $e->getMessage();
            } finally {
                if ($response == 1) {
                    echo "Revision updated";
                } else {
                    echo $error;
                }
            }

        }
        
    }

    public function actionremoveQItem(){
        $sql = 'UPDATE `jp_sales_quotation` '
                . ' SET `deleted_status`=0 '
                . ' WHERE id ='. $_POST['id'];
        $data =  Yii::app()->db->createCommand($sql);
        if($data->execute()){
            echo "Delete status updated"  ;
        }
    }

    public function actionrestoreQItem(){
        $sql = 'UPDATE `jp_sales_quotation` '
                . ' SET `deleted_status`=1 '
                . ' WHERE id ='. $_POST['id'];
        $data =  Yii::app()->db->createCommand($sql);
        if($data->execute()){
            echo "Delete status updated"  ;
        }
    }

    public function actionsaveItemUpdate(){

        $quantity_nos = isset($_POST['quantity_nos'])?$_POST['quantity_nos']:0;
        $sql = 'UPDATE `jp_sales_quotation` SET `quantity`='.$_POST['qty'].',`quantity_nos`='.$quantity_nos.',`mrp`='.$_POST['mrp_after_discount'].',`amount_after_discount`='.$_POST['profit_amount'].' WHERE id ='.$_POST['enbtry_id'];
        $data =  Yii::app()->db->createCommand($sql);
        if($data->execute()){
            $rev_no = 'REV-'.$_POST['rev'];
            $total_amount = Controller::getQuotationSum($_POST['qid'] , $rev_no,NULL); 
            echo json_encode(array(
                'message'=>'Updated successfully',
                'total_amount' => Controller::money_format_inr($total_amount, 2),
                'data_val'=>$total_amount
            ));
        }
        
    }    

    public function actiondeleteSubItem(){
         
        $transaction = Yii::app()->db->beginTransaction();
        try {
            $sql = "SELECT count(*) FROM  `jp_sales_quotation` "
            . " WHERE `master_id` = ".$_POST['qid']                
            . " AND `parent_type` = 1 AND `parent_id`= ".$_POST['subitem_id']
            . " AND revision_no ='REV-" . $_POST['rev']."'";
            $count = Yii::app()->db->createCommand($sql)->queryScalar();
            if($count > 0){
                $sql = "DELETE FROM  `jp_sales_quotation` "
                    . " WHERE `master_id` = ".$_POST['qid']                
                    . " AND `parent_type` = 1 AND `parent_id`= ".$_POST['subitem_id']
                    . " AND revision_no ='REV-" . $_POST['rev']."'";                
                
                $data =  Yii::app()->db->createCommand($sql);
                if($data->execute()){
                    $status = 1;
                    $sql = "DELETE FROM  `jp_quotation_gen_worktype` "
                        . " WHERE `id` = ".$_POST['subitem_id'];                
                    $data =  Yii::app()->db->createCommand($sql);
                    if($data->execute()){
                        $status = 1;
                    }else{
                        $status = 0;
                    }
                }else{
                    $status = 0;
                }
            }else{
                $sql = "DELETE FROM  `jp_quotation_gen_worktype` "
                    . " WHERE `id` = ".$_POST['subitem_id'];                
                $data =  Yii::app()->db->createCommand($sql);
                if($data->execute()){
                    $status = 1;
                }else{
                    $status = 0;
                } 
            }
            $transaction->commit();
        } catch (Exception $error) {
            $status = 0;
            $transaction->rollBack();
        } finally {            
            if($status == 1){
                $rev_no = 'REV-'.$_POST['rev'];
                $total_amount = Controller::getQuotationSum($_POST['qid'] , $rev_no,NULL); 
                echo json_encode(array(
                    'response'=>1,
                    'total_amount' => Controller::money_format_inr($total_amount, 2),
                    'data_val'=>$total_amount
                ));
            } else {
                echo json_encode(array('response'=>0));
                
            }
            
        }

    }

    public function actionuploadBanner(){
        
        
        $banner_image = realpath(Yii::app()->basePath . '/../uploads/quotation_generator/banner');
        $thumb_banner_image = realpath(Yii::app()->basePath . '/../uploads/quotation_generator/banner/thumbnail');

        $footer_image = realpath(Yii::app()->basePath . '/../uploads/quotation_generator/footer');
        $thumb_footer_image = realpath(Yii::app()->basePath . '/../uploads/quotation_generator/footer/thumbnail');

        $model = new QuotationGeneratorImage;        
        $bannermodel =  QuotationGeneratorImage::model()->find();
        if ($_FILES['QuotationGeneratorImage']['name']['banner_image']) {
            
            
            if(!is_null($bannermodel)){
                $oldbannerfile = $bannermodel['banner_image'];
                // var_dump($oldbannerfile);exit;
                if(file_exists($banner_image.'/'. $oldbannerfile)){
                    unlink($banner_image.'/'. $oldbannerfile);  
                    unlink($thumb_banner_image.'/'. $oldbannerfile); 
                }
                             
            }
                        
            $filename = CUploadedFile::getInstance($model, 'banner_image');
            $newfilename = rand(1000, 9999) . time();
            $newfilename = md5($newfilename);
            $extension = $filename->getExtensionName();
            $model->banner_image = $newfilename . "." . $extension;
        }else{
            // echo "here";exit;
            if(!is_null($bannermodel)){
                echo "aa";
                $model->banner_image = $bannermodel['banner_image'];
            }else{
                echo "bb";
            }
            exit;
        }

        if ($_FILES['QuotationGeneratorImage']['name']['footer_image']) {
            $bannermodel =  QuotationGeneratorImage::model()->find();
            if(!is_null($bannermodel)){
                $oldfooterfile = $bannermodel['footer_image'];
                if(file_exists($footer_image.'/'. $oldfooterfile)){
                unlink($footer_image.'/'. $oldfooterfile); 
                unlink($thumb_footer_image.'/'. $oldfooterfile); 
                }              
            }
            $filename_footer = CUploadedFile::getInstance($model, 'footer_image');
            $newfilename_footer = rand(1000, 9999) . time();
            $newfilename_footer = md5($newfilename_footer);
            $extension_footer = $filename_footer->getExtensionName();
            $model->footer_image = $newfilename_footer . "." . $extension_footer;
        }else{
            if(!is_null($bannermodel)){
                $model->footer_image = $bannermodel['footer_image'];
            }
        }

        $model->created_by = Yii::app()->user->id;
        $model->created_at = date('Y-m-d H:i:s');
        if(!empty($bannermodel)){ 
            $bannermodel->delete();
        }   
        echo "<pre>";
        print_r($model);    
        if ($model->save()) {
            if ($_FILES['QuotationGeneratorImage']['name']['banner_image']) {
                $filename->saveAs($banner_image . '/' . $newfilename . "." . $extension);
                $image = new EasyImage($banner_image . '/' . $newfilename . "." . $extension);
                $image->resize(60, 60);
                $image->save($thumb_banner_image . '/' . $newfilename . "." . $extension);
            }

            if ($_FILES['QuotationGeneratorImage']['name']['footer_image']) {
                $filename_footer->saveAs($footer_image . '/' . $newfilename_footer . "." . $extension_footer);
                $image_footer = new EasyImage($footer_image . '/' . $newfilename_footer . "." . $extension_footer);
                $image_footer->resize(60, 60);
                $image_footer->save($thumb_footer_image . '/' . $newfilename_footer . "." . $extension_footer);
            }
        }else{
            echo "<pre>";
        print_r($model->getErrors());exit; 
        }
        Yii::app()->user->setFlash('success', "Banner Uploaded Successfully");
        $this->redirect(array('salesQuotation/index'));
    }

    public function actiongetlatestDescription(){

        $sql="SELECT * FROM `jp_quotation_item_master` WHERE `id`=".$_POST['item_id'];
        $quotation_item_master = Yii::app()->db->createCommand($sql)->queryRow();

        $model = SalesQuotation::model()->findByPk($_POST['id']);
        $model->shutterwork_description = $quotation_item_master['shutterwork_description'];
        $model->carcass_description = $quotation_item_master['caracoss_description'];
        $model->description = $quotation_item_master['description'];
        if($model->save()){
            echo json_encode(array('response'=>'success'));
        }else{
            echo json_encode(array('response'=>'error'));
        }        
    }

    public function actiondeleteSubSection(){

        $subsecId = $_REQUEST['subsec_id'];
        $subsecArray = QuotationGenCategory::model()->findByPk($subsecId);
        $tblpx = Yii::app()->db->tablePrefix;

        $transaction = Yii::app()->db->beginTransaction();
        try{

            $sql1 = "DELETE FROM {$tblpx}quotation_gen_category "
            . " WHERE id = $subsecId AND revision_no = '".$_REQUEST['rev_no']."' "
            . " AND qid = '".$_REQUEST['qtid']."'";

            
            $subsecdel = Yii::app()->db->createCommand($sql1);
            if ($subsecdel->execute()) { 
                $success_status = 1;
                $subitemdData = QuotationGenWorktype::model()->findAll(
                    array("condition" => "qid = '".$_REQUEST['qtid']."' 
                    AND revision_no = '".$_REQUEST['rev_no']."' 
                    AND section_id = '".$subsecArray['section_id']."' 
                    AND category_label_id = '".$subsecId."'"
                ));

                if (!empty($subitemdData)) {
                    foreach ($subitemdData as $entry) {
                        if ($entry['id'] != "") {
                            $wtype_list[] = $entry['id'];
                        }
                    }
                    $wtypes = implode(',', $wtype_list);
    
                    $sql2 = "DELETE FROM {$tblpx}quotation_gen_worktype 
                        WHERE qid = '".$_REQUEST['qtid']."' 
                        AND revision_no = '".$_REQUEST['rev_no']."' 
                        AND section_id = '".$subsecArray['section_id']."' 
                        AND category_label_id = '".$subsecId."'";
                    $subitemdel = Yii::app()->db->createCommand($sql2);
    
                    if($subitemdel->execute()){
                        $success_status = 1;

                        $countsql = "SELECT count(*) FROM  {$tblpx}sales_quotation "
                        . " WHERE  master_id='". $_REQUEST['qtid']."' "
                        . " AND revision_no='".$_REQUEST['rev_no']."' "
                        . " AND `parent_type` = '1' "
                        . " AND  `parent_id` IN ($wtypes)";
                        $itemcount = Yii::app()->db->createCommand($countsql)->queryScalar();
                        if($itemcount  > 0){
                            $sql3 = "DELETE FROM  {$tblpx}sales_quotation "
                                . " WHERE  master_id='". $_REQUEST['qtid']."' "
                                . " AND revision_no='".$_REQUEST['rev_no']."' "
                                . " AND `parent_type` = '1' "
                                . " AND  `parent_id` IN ($wtypes)" ;
                            $quotdel = Yii::app()->db->createCommand($sql3);
                            
                            if($quotdel->execute()){
                                $success_status = 1;
                                echo "deleted success";
                            }else{
                                $success_status = 0;
                                throw new Exception('Error in items delete');
                            }
                        }
                    }else{
                        $success_status = 0;
                        throw new Exception('Error in sub item delete');
                    }
                }
    

            }else{
                $success_status = 0;
                throw new Exception('Error in sub section delete');
            }
            $transaction->commit();
        } catch (Exception $error) {
            $transaction->rollBack();
            $success_status = 0;
        } finally {

            if ($success_status == 1) {
                echo json_encode(array('response' => 'success', 'msg' => 'Deleted Successfully'));
            } else {
                echo json_encode(array('response' => 'error', 'msg' => $error->getMessage()));
            }
        }
       
    }


    public function actiondeleteSection1($id){

        $secId = $id;
        $secArray = QuotationSection::model()->findByPk($secId);
        $tblpx = Yii::app()->db->tablePrefix;

        $transaction = Yii::app()->db->beginTransaction();
        try{

            $sql = "DELETE FROM {$tblpx}quotation_section "
            . " WHERE id =". $secId;            
            $secdel = Yii::app()->db->createCommand($sql);
            
            if ($secdel->execute()) { 
                $success_status = 1;
                
                $subsecData = QuotationGenCategory::model()->findAll(
                    array("condition" => "section_id = '".$secId."'"
                ));

                if (!empty($subsecData)) {                    
    
                    $sql2 = "DELETE FROM {$tblpx}quotation_gen_category 
                        WHERE section_id = '".$secId."'";
                    $subsecdel = Yii::app()->db->createCommand($sql2);
    
                    if($subsecdel->execute()){
                        $success_status = 1;

                        $subitemdData = QuotationGenWorktype::model()->findAll(
                            array("condition" => "section_id = '".$secId."'"
                        ));
                        foreach ($subitemdData as $entry) {
                            if ($entry['id'] != "") {
                                $wtype_list[] = $entry['id'];
                            }
                        }
                        $wtypes = implode(',', $wtype_list);

                        $countsql = "SELECT count(*) FROM  {$tblpx}sales_quotation "
                        . " WHERE  `parent_type` = '1' "
                        . " AND  `parent_id` IN ($wtypes)";
                        $itemcount = Yii::app()->db->createCommand($countsql)->queryScalar();
                        if($itemcount  > 0){
                            $sql3 = "DELETE FROM  {$tblpx}sales_quotation "
                                . " WHERE `parent_type` = '1' "
                                . " AND  `parent_id` IN ($wtypes)" ;
                            $quotdel = Yii::app()->db->createCommand($sql3);
                            
                            if($quotdel->execute()){
                                $success_status = 1;
                                echo "deleted success";
                            }else{
                                $success_status = 0;
                                throw new Exception('Error in items delete');
                            }
                        }
                    }else{
                        $success_status = 0;
                        throw new Exception('Error in sub item delete');
                    }
                }
    

            }else{
                $success_status = 0;
                throw new Exception('Error in sub section delete');
            }
            $transaction->commit();
        } catch (Exception $error) {
            $transaction->rollBack();
            $success_status = 0;
        } finally {

            if ($success_status == 1) {
                echo json_encode(array('response' => 'success', 'msg' => 'Section Deleted Successfully'));
            } else {
                echo json_encode(array('response' => 'error', 'msg' => $error->getMessage()));
            }
        }
       
    }


    public function actiondeleteSubSection1($id){

        $subsecId = $id;
        $subsecArray = QuotationGenCategory::model()->findByPk($subsecId);
        $tblpx = Yii::app()->db->tablePrefix;

        $transaction = Yii::app()->db->beginTransaction();
        try{

            $sql1 = "DELETE FROM {$tblpx}quotation_gen_category "
            . " WHERE id =". $subsecId;

            
            $subsecdel = Yii::app()->db->createCommand($sql1);
            if ($subsecdel->execute()) { 
                $success_status = 1;
                $subitemdData = QuotationGenWorktype::model()->findAll(
                    array("condition" => "section_id = '".$subsecArray['section_id']."' 
                    AND category_label_id = '".$subsecId."'"
                ));

                if (!empty($subitemdData)) {
                    foreach ($subitemdData as $entry) {
                        if ($entry['id'] != "") {
                            $wtype_list[] = $entry['id'];
                        }
                    }
                    $wtypes = implode(',', $wtype_list);
    
                    $sql2 = "DELETE FROM {$tblpx}quotation_gen_worktype 
                        WHERE section_id = '".$subsecArray['section_id']."' 
                        AND category_label_id = '".$subsecId."'";
                    $subitemdel = Yii::app()->db->createCommand($sql2);
    
                    if($subitemdel->execute()){
                        $success_status = 1;

                        $countsql = "SELECT count(*) FROM  {$tblpx}sales_quotation "
                        . " WHERE  `parent_type` = '1' "
                        . " AND  `parent_id` IN ($wtypes)";
                        $itemcount = Yii::app()->db->createCommand($countsql)->queryScalar();
                        if($itemcount  > 0){
                            $sql3 = "DELETE FROM  {$tblpx}sales_quotation "
                                . " WHERE `parent_type` = '1' "
                                . " AND  `parent_id` IN ($wtypes)" ;
                            $quotdel = Yii::app()->db->createCommand($sql3);
                            
                            if($quotdel->execute()){
                                $success_status = 1;
                                echo "deleted success";
                            }else{
                                $success_status = 0;
                                throw new Exception('Error in items delete');
                            }
                        }
                    }else{
                        $success_status = 0;
                        throw new Exception('Error in sub item delete');
                    }
                }
    

            }else{
                $success_status = 0;
                throw new Exception('Error in sub section delete');
            }
            $transaction->commit();
        } catch (Exception $error) {
            $transaction->rollBack();
            $success_status = 0;
        } finally {

            if ($success_status == 1) {
                echo json_encode(array('response' => 'success', 'msg' => 'Sub Section Deleted Successfully'));
            } else {
                echo json_encode(array('response' => 'error', 'msg' => $error->getMessage()));
            }
        }
       
    }

    public function actiondeleteSubItem1($id){

        $subsecArray = QuotationGenWorktype::model()->findByPk($id);
        $tblpx = Yii::app()->db->tablePrefix;

        $transaction = Yii::app()->db->beginTransaction();
        try{

            $sql1 = "DELETE FROM {$tblpx}quotation_gen_worktype "
            . " WHERE id =". $id;            
            $subitemdel = Yii::app()->db->createCommand($sql1);
            if ($subitemdel->execute()) { 
                $success_status = 1;
                $countsql = "SELECT count(*) FROM  {$tblpx}sales_quotation "
                    . " WHERE  `parent_type` = '1' "
                    . " AND  `parent_id`=".$id;
                $itemcount = Yii::app()->db->createCommand($countsql)->queryScalar();
                if($itemcount  > 0){
                    $sql3 = "DELETE FROM  {$tblpx}sales_quotation "
                        . " WHERE `parent_type` = '1' "
                        . " AND  `parent_id` =".$id ;
                     $quotdel = Yii::app()->db->createCommand($sql3);
                            
                    if($quotdel->execute()){
                        $success_status = 1;
                        echo "deleted success";
                    }else{
                        $success_status = 0;
                        throw new Exception('Error in items delete');
                    }
                }                    
            }else{
                $success_status = 0;
                throw new Exception('Error in sub section delete');
            }
            $transaction->commit();
        } catch (Exception $error) {
            $transaction->rollBack();
            $success_status = 0;
        } finally {

            if ($success_status == 1) {
                echo json_encode(array('response' => 'success', 'msg' => 'Sub Item Deleted Successfully'));
            } else {
                echo json_encode(array('response' => 'error', 'msg' => $error->getMessage()));
            }
        }
       
    }

    public function actiongetquotationitemforedit() {
        
        $qid = isset($_POST['qid'])?$_POST['qid']:'';
        $rev = isset($_POST['rev'])? $_POST['rev']:'';
        $status = isset($_POST['status'])?$_POST['status']:'';
        $parent_type = isset($_POST['parent_type'])?$_POST['parent_type']:'';
        $parent_id = isset($_POST['parent_id'])?$_POST['parent_id']:'';
        $category_id = isset($_POST['category_id']) ? $_POST['category_id']:'';
        $category_label_id = isset($_POST['category_label_id']) ? $_POST['category_label_id']:'';
        $tblpx = Yii::app()->db->tablePrefix;
        $rev_no = "REV-" . $rev;
        
        if (!empty($qid)) {
            
            $salesquotation = SalesQuotation::model()->findAll(array(
                'condition' => "master_id = ' $qid ' AND parent_id='$parent_id'"
            ));
        }
                
        echo $this->renderPartial(
            'quotation_edit', array(                        
            'category_id' => $category_id,
            'qid' => $qid,
            'rev' => $rev,
            'status' => $status,
            'parent_type' => $parent_type,
            'parent_id' => $parent_id,
            'category_label_id' => $category_label_id,
            'salesquotation' => $salesquotation,
            'count_quotation' => count($salesquotation),
            'category_label' => isset($category_data->name)?$category_data->name:""
            )
        );
    }

    public function actionsaveRowQuotationItem(){

      
        $model = SalesQuotation::model()->findByPk($_POST['id']);       
        $model->attributes = $_POST;  
        $model->item_name = $_POST['item_name'];
        $model->item_id =$_POST['item_id'];
        if(!empty($_POST['item_id'])){
            $sql="SELECT * FROM `jp_quotation_item_master` WHERE `id`=".$_POST['item_id'];
        $quotation_item_master = Yii::app()->db->createCommand($sql)->queryRow();

        $model->shutterwork_description = $quotation_item_master['shutterwork_description'];
        $model->carcass_description = $quotation_item_master['caracoss_description'];
        $model->description = $quotation_item_master['description'];
        }
        if(isset($_POST['shutterwork_description'])){
            $model->shutterwork_description =$_POST['shutterwork_description'];
        }
        if(isset($_POST['caracoss_description'])){
            $model->carcass_description =$_POST['caracoss_description'];
        }
        if(isset($_POST['description'])){
            $model->description =$_POST['description'];
        }
         
        $length =isset($_POST['length'])?$_POST['length']:'';
        $width = isset($_POST['width'])?$_POST['width']:'';
        $error=0;
        if ($length == "" && $width != "") {
            $error=1;
            echo json_encode(array('response'=>'danger','msg'=>'Length is required when width is entered.'));exit;
        } elseif ($length != "" && $width == "") {
            $error=1;
            echo json_encode(array('response'=>'danger','msg'=>'Width is required when length is entered.'));exit;
        }
        if($model->save() && !$error){
            echo json_encode(array('response'=>'success','msg'=>'Item Updated Succesfully'));
        }else{ 
            echo json_encode(array('response'=>'danger','msg'=>'test error getsss'));//'Some Error Occured'));
        }
    }

    public function actiondeleteQuotationData(){
        $type = $_POST['type'];
        $id = $_POST['id'];

        if($type=="section"){
            $this->actiondeleteSection1($id);
        }

        if($type=="subsection"){            
            $this->actiondeleteSubSection1($id);
        }

        if($type=="subitem"){
            $this->actiondeleteSubItem1($id);
        }
    }

    public function actionuploadImages(){
        $qtid= $_GET['qtid'];
        $rev_no= $_GET['rev_no'];
        $model = new SalesQuotationGallery;
        $images_path = realpath(Yii::app()->basePath . '/../uploads/quotation_gallery');
        
        if(isset($_POST['SalesQuotationGallery']))
		{
			$model->attributes=$_POST['SalesQuotationGallery'];
            $model->category_id = isset($_POST['SalesQuotationGallery']['category_id'])?$_POST['SalesQuotationGallery']['category_id']:'';
            $model->worktype_id = isset($_POST['SalesQuotationGallery']['worktype_id'])?$_POST['SalesQuotationGallery']['worktype_id']:'';
            $model->sales_quotation_id = isset($_POST['SalesQuotationGallery']['sales_quotation_id'])?$_POST['SalesQuotationGallery']['sales_quotation_id']:'';
            if ($_FILES['SalesQuotationGallery']['name']['image']) {
                $filename = CUploadedFile::getInstance($model, 'image');
                $newfilename = rand(1000, 9999) . time();
                $newfilename = md5($newfilename);
                $extension = $filename->getExtensionName();
                $model->image = $newfilename . "." . $extension;
            }
			if ($model->save()) {
                if ($_FILES['SalesQuotationGallery']['name']['image']) {
                    $filename->saveAs($images_path . '/' . $newfilename . "." . $extension);
                    $LastInsertedGalleryId = $model->id; 
                    $sales_quotation_id = $model->sales_quotation_id;
                    $update = Yii::app()->db->createCommand()->update($this->tableNameAcc('sales_quotation',0), array('image_gallery_id' => $LastInsertedGalleryId), 'id=:sales_quotation_id ', array(':sales_quotation_id' => $sales_quotation_id));
                
                    $this->redirect(array('uploadImages','qtid'=>$qtid,"rev_no"=>$rev_no));
                }

            }
		}
        $this->render('quotation_image_gallery',
        array("qtid"=>$qtid,"rev_no"=>$rev_no,"model"=>$model)
        );       
    }

    public function actionremoveQuotationImage(){
        $id = $_POST['id'];
        $model =  SalesQuotation::model()->findByPk($id);
        if ($model !== null) {
            $model->image_gallery_id = null;
            if ($model->save(false)) {
                echo "Deleted Successfully";
            } else {
                echo "Error occurred while deleting.";
            }
        }
        
        
    }

    protected function buildTreeData($categories, $parentId = null) {
        $treeData = [];
        foreach ($categories as $category) {
            if ($category->parent_id == $parentId && ($category->parent_type == 2 ||$category->parent_type == 0)) {
                $node = ['text' => $category->item_name];
                $children = $this->buildTreeData($categories, $category->item_id);
                if (!empty($children)) {
                    $node['children'] = $children;
                }
                $treeData[] = $node;
            }
        }
        return $treeData;
    }
    public function actionGetWorkTypeBySubsection()
    {
        $qtn_id = $_REQUEST["qtn_id"];
        $rev_no = $_REQUEST["rev_no"];
        $cat_label_id = $_REQUEST["cat_label_id"];
        $sql = 'SELECT * FROM `jp_quotation_gen_worktype` '
                                . ' WHERE `qid` = ' . $qtn_id
                                . ' AND `category_label_id` = ' . $cat_label_id
                                . ' AND revision_no = "' . $rev_no . '"';
        $subitemArray = Yii::app()->db->createCommand($sql)->queryAll();
        $result     = "<option value=''>-Select Worktype-</option>";
        foreach ($subitemArray as $subitem) {
            $result .= "<option value='" . $subitem["id"] . "'>" . $subitem["worktype_label"] . "</option>";
        }
        echo $result;
    }
    public function actionGetItemByWorktype()
    {
        $qtn_id = $_REQUEST["qtn_id"];
        $rev_no = $_REQUEST["rev_no"];
        $worktype_id = $_REQUEST["worktype_id"];
        $sql = 'SELECT s.id,s.item_name,m.worktype_label FROM `jp_sales_quotation` s LEFT JOIN `jp_quotation_item_master` m ON m.id=s.item_id '
                . 'WHERE s.parent_type = 1 AND s.parent_id = ' . $worktype_id
                . ' AND s.revision_no="' . $rev_no . '" AND s.deleted_status = 1';
               
        $wrktypeitemArray = Yii::app()->db->createCommand($sql)->queryAll();
        //echo "<pre>";print_r($wrktypeitemArray);exit;
        $result     = "<option value=''>-Select Item-</option>";
        foreach ($wrktypeitemArray as $wrktypeitem) {
            
            
            if(!empty($wrktypeitem["item_name"])){
                $item = $wrktypeitem["item_name"];
            }else{
                 $item = $wrktypeitem["worktype_label"];
            }
            $result .= "<option value='" . $wrktypeitem["id"] . "'>" . $item . "</option>";
        }
        echo $result;
    }
   
    public function replaceItemlistWithCondition($extractedContent, $replacements) {
        $grouppattern = '/\[\((.*?)\)\]/';
    
        preg_match_all($grouppattern, $extractedContent, $groupmatches);
    
        foreach ($groupmatches[0] as $match) {
          //  $replaceString = $match;
          $input = str_replace(array("[(", ")]"), '', $match); 
    
            // Check if "length," "width," and "size" are all present
            $variables = preg_match_all('/\{([^}]+)\}/', $match, $matches);
            $requiredVariables = ["length", "width"];
           
            // Check if any required variable is missing
           // if (count(array_intersect($requiredVariables, $matches[0])) == count($requiredVariables)) {
            $i=0;
                foreach ($matches[0] as $variable) { 
                    if (!empty($replacements[$variable])) {$i++;
                        $replaceString = str_replace($variable, $replacements[$variable], $input);
                       
                    } else {
                        // If any variable is missing a value, replace it with an empty string
                        $replaceString = str_replace($variable, '', $match);
                    }
                }
           if(count($matches[0]) != $i) {
                // Any required variable is missing, replace with an empty string
                $replaceString = '';
            }
  
            $extractedContent = str_replace($match, $replaceString, $extractedContent);
           
            //$extractedContent = preg_replace('/\[|\]|\(|\)|\s+/', '', $extractedContent);


           
        }//echo $extractedContent;die;
    
        return $extractedContent;
    }
    
    public function replaceItemlistWithCondition_old($extractedContent, $replacements) {

        $grouppattern = '/\[\((.*?)\)\]/';
        $pattern = '/\{([^}]+)\}/';
        $seperatorpattern = '/\{\w+}([X-])\{\w+}/';
        
        preg_match_all($grouppattern, $extractedContent, $groupmatches);//print_r($groupmatches);die();
        $html_string = '';
        for($i=0; $i<count($groupmatches); $i++){
            preg_match_all($pattern, $groupmatches[1][$i], $matches);
            preg_match_all($seperatorpattern, $groupmatches[1][$i], $seperator);
  
            if (empty($replacements[''.$matches[0][0].'']) ||  empty($replacements[''.$matches[0][1].''])) {
                $extractedContent = str_replace('[('.$groupmatches[1][$i].')]', '', $extractedContent);
            }else{
                                                   
                if (!empty($seperator[1])) {
                    $XCharacters = $seperator[1];
                        foreach ($XCharacters as $X) {
                            $currentseperator = $X;
                        }
                }
                $extractedContent = str_replace('[('.$groupmatches[1][$i].')]', $replacements[''.$matches[0][0].''] . $currentseperator . $replacements[''.$matches[0][1].''], $extractedContent);
                                                  
            }
                                                
        }
        return $extractedContent;
                                            
        
        }

        /**mdf issue ticket */
        public function getCategoryItemDuplicate($qtid, $rev_no, $sub_status) {
            $item_model = SalesQuotation::model()->findAll(
                    array("condition" => "master_id = '$qtid' AND revision_no ='$rev_no' "
                        . "AND work_type IS NOT NULL"
                        . " AND deleted_status IN(1,2) "
                        . "AND sub_status='$sub_status'", 'order' => 'category_id '));
            return $item_model;
        }
    
        public function getQuotationCategoryDuplicate($qtid, $rev_no) {
            $quotation_categories = SalesQuotation::model()->findAll(array("select" => "DISTINCT category_label,category_id",
                "condition" => "master_id = '$qtid' AND revision_no ='$rev_no' "
                . "AND work_type IS NOT NULL AND deleted_status IN(1,2)",
                'order' => 'category_id '));
            return $quotation_categories;
        }
    
        public function getExtraWorkDuplicate($qtid, $rev_no) {
            $extrawork = SalesQuotation::model()->findAll(
                    array("condition" => "master_id = '$qtid' AND  revision_no ='$rev_no' "
                        . "AND work_type IS NULL AND deleted_status IN(1,2)", 'order' => 'category_id '));
            return $extrawork;
        }
        public function actionaddsalesQuotationItemsDuplicate(){

            $data = $_REQUEST['data'];        
            $rev_no = 'REV-'.$_POST['revision_no'];
            $quotation_id = $_POST['master_id'] ;
    
            $revdata = SalesQuotation::model()->findAll(array('condition' => 'master_id = "' . $quotation_id . '" AND revision_no = "' . $rev_no . '" AND `revision_update_status` = "0"'));
            $data_count = count($data);
                            
            if ($data_count >= 1) {
                SalesQuotation::model()->deleteAll(array('condition' => 'master_id = "' . $quotation_id . '" AND revision_no ="' . $rev_no . '" AND `revision_update_status` = "0" AND deleted_status IN(1,2)'));
            }
            foreach ($data as $key => $value) {
                $model = new SalesQuotation();
                $model->attributes = $value;  
                $model->sub_status =0; 
                $model->master_id =$value['master_id'];              
                $model->created_by = 54;
                $model->created_date = date('Y-m-d');
                $model->revision_no ='REV-'.$value['revision_no']; 
                $model->parent_type =$value['parent_type']; 
                $model->parent_id =$value['parent_id']; 
                
                if (!$model->save()) {                    
                    echo json_encode(array('response' => 'error', 'msg' => 'Some Error occured'));
                } else { 
                    if($_POST['status'] == 1){
                        $sql = 'SELECT count(*) as count ,id '
                                . ' FROM jp_quotation_revision '
                                . ' WHERE qid='.$model->master_id
                                . ' AND revision_no="'.$model->revision_no.'"';
                        $revision_model_count = Yii::app()->db->createCommand($sql)->queryRow();
                        
                        $revision_model = new QuotationRevision();
                        if($revision_model_count['count'] > 0) {
                            $revision_model = QuotationRevision::model()->findByPk($revision_model_count['id']);
                        }
            
                        $revision_model->qid = $model->master_id;
                        $revision_model->revision_no = $model->revision_no;
                        $revision_model->save();
                    } 
                    $total_amount = Controller::getQuotationSum($model->master_id, $model->revision_no,NULL);
    
                    echo json_encode(array(
                        'response' => 'success', 
                        'msg' => 'Quotation Items saved to new revision',
                        'total_amount' => Controller::money_format_inr($total_amount, 2),
                        'data_val'=>$total_amount
                    ));
                }
            } 
            
            $update = SalesQuotationMaster::model()->updateAll(array('revision_no' => $rev_no), 'id=:master_id', array(':master_id' => $quotation_id));                           
        }
        public function setQuotationsListDuplicate($items_model, $subitem_model, $model, $quotation_categories) {
            $items = array();
            foreach ($items_model as $item) {
                $items[] = $item->attributes;
            }
            $subarray = array();
            foreach ($subitem_model as $item) {
                $subarray[] = $item->attributes;
            }
            $lists = array();
            $data_items = array();
            $data = array();
            $sub_items = array();        
            if (!empty($items)) {
                if (!empty($quotation_categories)) {
                    foreach ($quotation_categories as $quotation_category) {
                        $keys = array_keys(array_column($items, 'category_id'), $quotation_category->category_id);
                        $data_items = array();
                        $data_items_label = array();
                        foreach ($keys as $key) {
                            array_push($data_items, $items[$key]);
                        }
                        
                        $keys1 = array_keys(array_column($data_items, 'category_label'), $quotation_category->category_label);
                        foreach ($keys1 as $key) {
                            array_push($data_items_label, $data_items[$key]);
                        }
                        $keys_sub = array_keys(array_column($subarray, 'category_label'), $quotation_category->category_label);
                        $sub_items = array();
                        foreach ($keys_sub as $keys) {
                            array_push($sub_items, $subarray[$keys]);
                        }
                        $data = array('category_details' => $quotation_category->attributes, 'items_list' => $data_items_label, 'sub_items' => $sub_items);
                        array_push($lists, $data);
                    }                
                } else {
                    $data_items = array();
                    foreach ($items as $item) {
                        array_push($data_items, $item);
                    }
                    $sub_items = array();
                    foreach ($subitem_model as $items) {
                        array_push($sub_items, $item);
                    }
                    $data = array('category_details' => '', 'items_list' => $data_items, 'sub_items' => $sub_items);
                    if (!empty($data)) {
                        array_push($lists, $data);
                    }
                }
            }        
            return $lists;
        }
        public function getMaterialsDuplicate($qid) {
            $criteria = new CDbCriteria();
            $criteria->select = "shutterwork_material,carcass_material,material,item_id";
            $criteria->condition = "master_id='" . $qid . "'";
            $material_data = SalesQuotation::model()->findAll($criteria);
            $data_array = array();
            foreach ($material_data as $key => $material) {
                $item_master = QuotationItemMaster::model()->findByPk($material['item_id']);            
                
                if ($material->shutterwork_material) {
                    $data_array = array_merge($data_array, explode(',', $item_master['shutter_material_id']));
                }
    
                if ($material->carcass_material) {
                    $data_array = array_merge($data_array, explode(',', $item_master['carcass_material_id']));
                }
                if ($material->material) {
                    $data_array = array_merge($data_array, explode(',', $item_master['material_ids']));
                }
            }
            $material_ids = array_unique($data_array);
            $mainarray = array_chunk($material_ids, 2);
    
            return $material_ids;
        }
        public function getTestTemplate($selectedtemplate) {
            $header = "";
            $footer = "";
            
            $company = Company::model()->findBypk(54);
            $company_name=(isset($company['name']) && !empty($company['name'])) ? ($company['name']) : "";
            $address = (isset($company['address']) && !empty($company['address'])) ? ($company['address']) : "";
            $pin = (isset($company['pincode']) && !empty($company['pincode'])) ? ($company['pincode']) : "";
            $phone = (isset($company['phone']) && !empty($company['phone'])) ? ( $company['phone']) : "";
            $email = (isset($company['email_id']) && !empty($company['email_id'])) ? ( $company['email_id']) : "";
            $gst = "GST NO: " . isset($company["company_gstnum"]) ?
                    $company["company_gstnum"] : '';
    
            $logo = CHtml::image($this->logo, Yii::app()->name,
                            array('style' => 'max-height:45px;'));
            $activeProjectTemplate = $this->getActiveTemplate();
            if ($selectedtemplate == 'template1' && $activeProjectTemplate != 'TYPE-6') {
                $header = '<div style="padding:20px;padding-bottom:200px">
                            <table border=0 style="margin-bottom:200px">
                                    <tbody>
                                        <tr>
                                            <td class="img-hold"> ' . $logo . '</td>
                                            <td class="text-center">
                                            <h1></h1>
                                            </td>
                                        </tr>                                
                                        <tr>
                                            <td></td>
                                            <td class="text-center">
                                                <div>' . $address . '<div>
                                                <div> PIN : ' . $pin . ',&nbsp;
                                                    PHONE : ' . $phone . '<div>
                                                <div>  EMAIL : ' . $email . ' <div>                       
                                                <div>  GST NO: ' . $gst . '</div> 
                                                <div>  <br><br><br><br><br><br><br><br><br><br><br><br><br></div> 
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                
                                </div>';
                $footer = '<div style="padding: 5px 20px 20px;text-align:center;font-size:10px;">
                            Powered by bluehorizoninfotech.com<br>
                            Copyright © 2024 by Blue Horizon Infotech, Cochin. All Rights Reserved.
                            </div>';
                            
            } else if ($selectedtemplate == 'template1' && $activeProjectTemplate == 'TYPE-6') {
                $header = '<div style="padding:20px;">
                            <table border=0 >
                                <tbody>
                                    <tr>
                                        <td class="img-hold"> ' . $logo . '</td>
                                        <td class="text-center">
                                            
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>';
                $footer = '<div style="padding: 5px 20px 20px;text-align:center;font-size:10px;">
                            Powered by bluehorizoninfotech.com<br>
                            Copyright © 2024 by Blue Horizon Infotech, Cochin. All Rights Reserved.
                            </div>';
            } else if ($selectedtemplate == 'template2') {
                $header = '<div style="border-bottom: 2px solid #000000;padding:0px 20px;">
                        <table width="100%">
                            <tr>
                                <td width="9%" rowspan="2">' . $logo . '</td>
                                <td width="44%"><h2>R-A-LAB</h2></td>
                                <td width="44%">&nbsp;</td>
                            </tr>
                            <tr>            
                                <td width="44%"><div style="font-size:15px">Ramees Ali + teamLab</div></td>
                                <td width="44%" align="right"><div style="font-size:15px">' . $gst . '</div></td>
                            </tr>
                            <br>
                        </table>
                    </div>';
    
                $footer = '<div style="padding:20px;background-color:#000;">
                            <table width="100%">
                                <tr>
                                    <td width="33%" style="color:#fff;">
                                        <div>office:</div>        
                                        <div>Opp. Indian Oil
                                        Vayapparapadi , Manjeri
                                        Malappuram 676121
                                        mail@ralab.in</div> 
                                    </td>
                                    <td width="44%" align="center"></td>
                                    <td width="22%">' . $logo . '</td>
                                </tr>
                                <tr>
                                <td width="33%" style="color:#fff;">GSTIN: 32AUZPP4647B1ZC</td>
                                <td width="44%" align="center"></td>
                                <td width="22%" style="color:#fff;">www.ralab.in</td>
                                </tr>
                            </table>
                        </div><div style="padding: 5px 20px 20px;text-align:center;background-color:#000;color:#fff;font-size:10px;">
                        Powered by bluehorizoninfotech.com<br>
                        Copyright © 2024 by Blue Horizon Infotech, Cochin. All Rights Reserved.
                        </div>';
            } else if ($selectedtemplate == 'template3' && $activeProjectTemplate == 'TYPE-6') {
                $header = '<div style="border-bottom: 2px solid #000000;padding:0px 20px;">
                            <table width="100%">
                            <tr>
                                <td width="9%" rowspan="2">' . $logo . '</td>
                                <td width="44%"></td>
                                <td width="44%"></td>
                            </tr>
                            <tr>            
                                <td width="44%"><div style="font-size:15px"></div></td>
                                <td width="44%" align="right">
                                    <div style="font-size:15px">' . $gst . '</div>
                                </td>
                            </tr>
                            <br>
                            </table>
                        </div>';
    
                $footer = '<div style="padding:20px;background-color:#fff;">
                <table width="100%">
                    <tr>
                        <td width="44%" style="color:#000;">  
                            <div><b>  BILLING ADDRESS  </b><div>  
                            <div><b>' . $company_name . ',</b><div>                                  
                            <div>' . $address . '<div>
                            <div> PIN : ' . $pin . '</div>
                            <div>   PHONE : ' . $phone . '<div>
                            <div>  EMAIL : ' . $email . ' <div>                       
                            <div>  GST NO: ' . $gst . '</div> 
                        </td>
                        <td width="33%" align="center"></td>
                        <td width="22%">' . $logo . '</td>
                    </tr>
                </table>
            </div>
            <div style="padding: 5px 20px 20px;text-align:center;background-color:#fff;color:#000;font-size:10px;">
                Powered by bluehorizoninfotech.com<br>
                Copyright © 2024 by Blue Horizon Infotech, Cochin. All Rights Reserved.
            </div>';
            } else {
                $header = '<div style="border-bottom: 2px solid #000000;padding:0px 20px;">
                                <table width="100%">
                                    <tr>
                                        <td width="9%" rowspan="2">' . $logo . '</td>
                                        <td width="44%"></td>
                                        <td width="44%"></td>
                                    </tr>
                                    <tr>            
                                        <td width="44%"><div style="font-size:15px"></div></td>
                                        <td width="44%" align="right">
                                            <div style="font-size:15px">' . $gst . '</div>
                                        </td>
                                    </tr>
                                    <br>
                                </table>
                            </div>';
    
                $footer = '<div style="padding:20px;background-color:#000;">
                                <table width="100%">
                                    <tr>
                                        <td width="44%" style="color:#fff;">                                    
                                            <div>' . $address . '<div>
                                            <div> PIN : ' . $pin . '</div>
                                            <div>   PHONE : ' . $phone . '<div>
                                            <div>  EMAIL : ' . $email . ' <div>                       
                                            <div>  GST NO: ' . $gst . '</div> 
                                        </td>
                                        <td width="33%" align="center"></td>
                                        <td width="22%">' . $logo . '</td>
                                    </tr>
                                </table>
                            </div>
                            <div style="padding: 5px 20px 20px;text-align:center;background-color:#000;color:#fff;font-size:10px;">
                                Powered by bluehorizoninfotech.com<br>
                                Copyright © 2024 by Blue Horizon Infotech, Cochin. All Rights Reserved.
                            </div>';
            }
            return (array('header' => $header, 'footer' => $footer));
        }
    
    
        public function actionSaveQuotationDuplicate($qid, $rev_no) {
            //die('hi');
            $model = SalesQuotationMaster::model()->find(array("condition" => "id='$qid'"));
            $quotation_no = $model->invoice_no;
            $item_data = $this->getCategoryItemDuplicate($qid, $rev_no, $sub_status = 0);
            $sub_item = $this->getCategoryItemDuplicate($qid, $rev_no, $sub_status = 1);
            $quotation_categories = $this->getQuotationCategoryDuplicate($qid, $rev_no);
            $item_model = $this->setQuotationsListDuplicate($item_data, $sub_item, $model, $quotation_categories);
            $item_extra = $this->getExtraWorkDuplicate($qid, $rev_no);
            $materials = $this->getMaterialsDuplicate($qid);
            $compmodel = Company::model()->findByPk($model->company_id);
            $address = $compmodel->address;
            $mPDF1 = Yii::app()->ePdf->mPDF('', 'A4');
            $mPDF1->debug = true;
            $mPDF1->showImageErrors = true;
            $mPDF1->SetImportUse();
            $is_top_image = 0;
    
            // top files
            $sql  = "SELECT `id`,`top_bottom_image` FROM `jp_quotation_generator_image` WHERE type='0'";
            $image = Yii::app()->db->createCommand($sql)->queryAll(); 
            foreach ($image as $key => $value) {
                $pagecount = $mPDF1->SetSourceFile(realpath(Yii::app()->basePath . '/../uploads/quotation_generator/top-images/'.$value["top_bottom_image"]));            
                for ($i=1; $i<=$pagecount; $i++) {
                    $import_page = $mPDF1->ImportPage($i);
                    $mPDF1->UseTemplate($import_page);
            
                    if ($i <= $pagecount)
                        $mPDF1->AddPage();
                    $is_top_image = 1;
                }
            }   
            $app_root = YiiBase::getPathOfAlias('webroot');
            $site_hosturl = str_replace('/index.php','', Yii::app()->createAbsoluteUrl('/'));
            $image_base_url=Yii::app()->theme->baseUrl .'/assets/default/logo.png'; 
            $theme_asset_url = $site_hosturl."/themes/assets/" ; 
           
            if(file_exists(Yii::app()->theme->basePath .'/images/quotation-file/logo.png')){
                $image_url=Yii::app()->theme->baseUrl .'/images/quotation-file/logo.png';
            }else if( file_exists($app_root . "/themes/assets/logo.png") ){
                $image_url=$theme_asset_url .'logo.png';
            }else{
                $image_url=$theme_asset_url .'default-logo.png';
            }
                   // middle content
            if ($model->template_type == '2') {
               // $pdf = 'quotationpdf';
               $pdf = "quotation_pdf_latest";
            }else if ($model->template_type == '5') {
                $pdf = 'concord_quotationpdf';
            }
            else{
                $pdf = 'agac_quotation_pdf';
                $mPDF1->SetWatermarkImage($image_url,0.2,array(59.266666667,26.458333333),P);
                $mPDF1->showWatermarkImage = true;
            }
    
            $activeProjectTemplate = $this->getActiveTemplate();
            if ($activeProjectTemplate == 'TYPE-8'||$activeProjectTemplate == 'TYPE-7') {
                //$pdf = 'agac_quotation_pdf_new';
                $pdf = "quotation_pdf_latest";
            }else if($this->getActiveTemplateID() >8){
                $pdf = "quotation_pdf_latest";
                $mPDF1->showWatermarkImage = false;
            }
            $pdf_data = ProjectTemplate::model()->find(array("condition" => "status='1'"));
            
            $item_total = SalesQuotation::model()->find(
                    array("condition" => "master_id = '$qid' AND revision_no ='$rev_no' "
                        . "AND deleted_status IN(1,2)",
                        'order' => 'id DESC '));
            
            $sql = 'SELECT template_name '
                        . 'FROM jp_quotation_template '
                        . 'WHERE status="1"';
            $selectedtemplate =Yii::app()->db->createCommand($sql)->queryScalar();
            $template = $this->getTestTemplate(9);
            $usermodel = Users::model()->findByPk(54);
            $estimator_name =  $usermodel->first_name.' '.$usermodel->last_name;
            $estimator_phone =  $usermodel->phonenumber;
            
            
            $data = $this->renderPartial($pdf, array(
                'qtid'=>$qid,
                'rev_no'=>$rev_no,
                'model' => $model,
                'address' => $address,
                'itemmodel' => $item_model,
                'item_extra' => $item_extra,
                'materials' => $materials,
                'client' => $model->client_name,
                'total_taxable_amount' => $item_total->mainitem_total,
                'total_tax' => $item_total->mainitem_tax,
                'total_amoun_withtax' => $item_total->maintotal_withtax,
                'pdf_data' => $pdf_data->template_format,           
                'is_top_image' => $is_top_image ,
                'compmodel' => $compmodel,
                'estimator_name' => $estimator_name,
                'estimator_phone' => $estimator_phone
                    ), true);  
                if ($model->template_type == '5') {   
                    $mPDF1->SetHTMLHeader('<table class="details">
                    <tbody>
                        <tr>
                            <td class="border-bottom border-right-0" colspan="3">'. CHtml::image($this->logo, Yii::app()->name,array("style" => "max-height:45px;"))
                                    .'
                                </td>
    
                                <td class="border-bottom text-right"  colspan="3">
                                    <h2>CONCORD <span class="orange_text">DESIGN STUDIO</span></h2>      
                                    
                                    <small>Emir Palaza, Near St.Francis Xaviers Church,</small>
                                    <small>Kaloor Kadavanthra Road, Kathrikadavu, Kochi - 682 017</small>
                                    <small>ph: 0484-234622, 4028208, m:interiors@concordkerala.com</small>
                                    <small>www.concordkerala.com</small>
                                </td>                                
                        </tr>
                        
                        <tr>
                            <td width="28%"  class="border-bottom-0 border-right-0">Name Of Client</td>
                            <td class="border-bottom-0 border-right-0">:</td>
                            <td width="21%" class="border-bottom-0"><b> '.$model->client_name.'</b></td>
                            <td width="28%" class="border-bottom-0 border-right-0">Quotation/Invoice No</td>
                            <td class="border-bottom-0 border-right-0">:</td>
                            <td width="21%" class="border-bottom-0"><b> '.(isset($model->invoice_no) ? $model->invoice_no : "").'</b></td>
                        </tr>
                        <tr>
                            <td width="28%"  class="border-bottom-0 border-right-0">Flat No./House no./House Name</td>
                            <td class="border-bottom-0 border-right-0">:</td>
                            <td width="21%" class="border-bottom-0"><b> '.$model->house_name.'</b></td>
                            <td width="28%" class="border-bottom-0 border-right-0">Date</td>
                            <td class="border-bottom-0 border-right-0">:</td>
                            <td width="21%" class="border-bottom-0"><b> '.(isset($model->date_quotation) ? date('d-m-Y', strtotime($model->date_quotation)) : "").'</b></td>
                        </tr>
                        <tr>
                            <td width="28%"  class="border-bottom-0 border-right-0">Project Address</td>
                            <td class="border-bottom-0 border-right-0">:</td>
                            <td width="21%" class="border-bottom-0"><b style="display:inline"> '.$model->address.'</b></td>
                            <td width="28%" class="border-bottom-0 border-right-0">Project Type</td>
                            <td class="border-bottom-0 border-right-0">:</td>
                            <td width="21%" class="border-bottom-0"><b> '.(($model->project_type=='0')?"Residential":"Commercial").'</b></td>
                        </tr> 
    
                        <tr>
                            <td width="28%"  class="border-bottom-0 border-right-0">Site Address</td>
                            <td class="border-bottom-0 border-right-0">:</td>
                            <td width="21%" class="border-bottom-0"><b> '.$model->site_address.'</b></td>
                            <td width="28%" class="border-bottom-0 border-right-0">&nbsp;</td>
                            <td class="border-bottom-0 border-right-0"></td>
                            <td width="21%" class="border-bottom-0"></td>
                        </tr>
                        <tr>
                            <td width="28%"  class="border-bottom-0 border-right-0">Ph No</td>
                            <td class="border-bottom-0 border-right-0">:</td>
                            <td width="21%" class="border-bottom-0"><b> '.$model->phone_no.'</b></td>
                            <td width="28%" class="border-bottom-0 border-right-0">&nbsp;</td>
                            <td class="border-bottom-0 border-right-0"></td>
                            <td width="21%" class="border-bottom-0"></td>
                        </tr> 
                        <tr>
                            <td width="28%"  class="border-right-0">Email id </td>
                            <td class="border-bottom-0 border-right-0">:</td>
                            <td width="21%" class=""><b> '.$model->email.'</b></td>
                            <td width="28%" class="border-right-0">&nbsp;</td>
                            <td class="border-bottom-0 border-right-0"></td>
                            <td width="21%" class=""></td>
                        </tr> 
                                                                            
                    </tbody>
                </table><br>'); 
                $mPDF1->AddPage('','', '', '', '', 10,10,70, 30, 10,0); 
            } 
    
            $this->actiondownloadQuotationPdf($data)  ;       
             
            $pagecount1 = $mPDF1->SetSourceFile(realpath(Yii::app()->basePath . '/../documents/quotation/qgenerator.pdf'));            
            for ($i=1; $i<=$pagecount1; $i++) {
                $import_page1 = $mPDF1->ImportPage($i);
                $mPDF1->UseTemplate($import_page1);
        
                if ($i <= $pagecount1)
                $mPDF1->AddPage();
            }
        
            
            $blankpage = $mPDF1->page + 1; 
            $mPDF1->DeletePages($blankpage);
            $mPDF1->WriteHTML('');
            $mPDF1->AddPage();
            // bottom files
     
            // bottom files
            $sql  = "SELECT `id`,`top_bottom_image` FROM `jp_quotation_generator_image` WHERE type='1'";
            $image1 = Yii::app()->db->createCommand($sql)->queryAll(); 
            foreach ($image1 as $key => $value) {
                $pagecount2 = $mPDF1->SetSourceFile(realpath(Yii::app()->basePath . '/../uploads/quotation_generator/bottom-images/'.$value["top_bottom_image"]));            
                for ($i=1; $i<=$pagecount2; $i++) {
                    $import_page2 = $mPDF1->ImportPage($i);
                    $mPDF1->UseTemplate($import_page2);
    
                    if ($i < $pagecount2)
                        $mPDF1->AddPage();
                }
            }
    
            //$mPDF1->AddPage(); 
            if ($template['footer'] != '') {
               // $mPDF1->SetHTMLFooter('<div class="text-center" style="margin:-50px -60px !important; margin-bottom:-180px !important;">' . $template['footer']  . '</div>', '', true);
            }
            $mPDF1->Output('"' . $quotation_no . '".pdf', 'D');        
    
        }
         /**mdf issue ticket */

}

