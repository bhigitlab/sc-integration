<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.min.js"></script>
<?php
/* @var $this SubcontractorController */
/* @var $model Subcontractor */
/* @var $form CActiveForm */
?>
<div class="page_filter">
    <?php $form = $this->beginWidget('CActiveForm', array(
        'action' => Yii::app()->createUrl($this->route), // Ensure the form action is properly set
        'method' => 'get', // Using GET method to append parameters to URL
    )); ?>
    <div class="row">
        <!-- Date From Field -->
        <div class="col-lg-2 col-md-3 col-sm-4">
            <label class="text-nowrap">Date From :</label>
            <?php
            // Check if 'date_from' is passed in the URL, otherwise set a default value
            if (!isset($_GET['date_from']) || $_GET['date_from'] == '') {
                $datefrom = ""; // Set the default value for date_from
            } else {
                $datefrom = $_GET['date_from']; // Get the value from the URL
            }
            // Render the date_from input field with name 'date_from' and value
            echo CHtml::textField('date_from', $datefrom, array('readonly' => true, 'class' => 'form-control'));
            ?>
        </div>

        <!-- Date To Field -->
        <div class="col-lg-2 col-md-3 col-sm-4">
            <label class="text-nowrap ">Date To :</label>
            <?php
            // Check if 'date_to' is passed in the URL, otherwise set a default value
            if (!isset($_GET['date_to']) || $_GET['date_to'] == '') {
                $date_to = ""; // Set the default value for date_to
            } else {
                $date_to = $_GET['date_to']; // Get the value from the URL
            }
            // Render the date_to input field with name 'date_to' and value
            echo CHtml::textField('date_to', $date_to, array('readonly' => true, 'class' => 'form-control'));
            ?>
        </div>

        <!-- Filter Buttons -->
        <div class="col-sm-4  text-right text-sm-left ">
            <label>&nbsp;</label>
            <div>
                <input type="submit" id="submitButton" value="Go" class="btn btn-primary btn-sm" />
                <input type="reset" id="reset" value="Clear" class="btn btn-default btn-sm" />
            </div>
        </div>
    </div>

    <?php $this->endWidget(); ?>
</div>

<script>
    $(document).ready(function () {
        // Initialize datepickers for date_from and date_to
        $("#date_from").datepicker({
            dateFormat: 'dd-mm-yy'
        });
        $("#date_to").datepicker({
            dateFormat: 'dd-mm-yy'
        });

        // Set min/max date limits
        $("#date_from").change(function () {
            $("#date_to").datepicker('option', 'minDate', $(this).val());
        });
        $("#date_to").change(function () {
            $("#date_from").datepicker('option', 'maxDate', $(this).val());
        });
    });

    // Reset button redirects to the 'admin' page (adjust as per your routing)
    $('#reset').click(function () {
        location.href = '<?php echo $this->createUrl('allentries'); ?>';
        //document.getElementById("dailyexpense-form").reset();
    });
</script>