<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="de" lang="de">

<head>
    <!-- CSS Links -->
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/bootstrap-theme.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/message/css/jquery.message.css">
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl; ?>/css/purchase.css">
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.4.1/css/simple-line-icons.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/style.css?<?php echo time(); ?>" />

    <!-- jQuery -->
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.min.js"></script>
  
    <!-- jQuery UI -->
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery-ui.js"></script>
  
    <!-- Other Scripts -->
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/moment.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/tableHeadFixer.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->baseUrl . '/ckeditor/ckeditor.js'; ?>"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jspdf.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/html2canvas.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.plugin.html2canvas.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/base64.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/canvas2image.js"></script>

    <!-- Bootstrap JS -->
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/bootstrap.min.js"></script>
    
    <!-- Select2 JS -->
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.js"></script>
    
    <!-- Message JS -->
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/message/js/jquery.message.js"></script>
</head>
<body>
    <div id="page">
            <?php echo $content; ?>
            <script type="text/javascript">
                var shim = (function() {
		        document.createElement('datalist');
	            })();
                $(function() {
                $("#datepicker").datepicker({
                    dateFormat: 'dd-mm-yy'
                }).datepicker("setDate", new Date());

				$("#tax_slab").change(function(){                    
                    var tax_slab = $(this).val();
					
                    var gst = parseFloat(tax_slab)/2;
                    $(this).parents(".row").find(".sgstp").val(gst).trigger("change");
                    $(this).parents(".row").find(".cgstp").val(gst).trigger("change");
                })

                $(document).on("change",".tax_slab",function(){                   
                    var tax_slab = $(this).val();
                    var gst = tax_slab/2;
					// if(tax_slab==18){
					// $("#sgstp").val('9');
					// $("#cgstp").val('9');
					
					// }
                    $(this).parents("tr").find(".sgstp").val(gst).trigger("change");
                    $(this).parents("tr").find(".cgstp").val(gst).trigger("change");
                })

                });
                $(".popover-test").popover({
                html: true,
                content: function() {
                    return $(this).next('.popover-content').html();
                }
               });
               $('[data-toggle=popover]').on('click', function(e) {
			    $('[data-toggle=popover]').not(this).popover('hide');
		        });
                $('body').on('hidden.bs.popover', function(e) {
                    $(e.target).data("bs.popover").inState.click = false;
                });
        var $ = jQuery.noConflict();
        $(document).ready(function() {
		$().toastmessage({
			sticky: false,
			position: 'top-right',
			inEffectDuration: 1000,
			stayTime: 3000,
			closeText: '<i class="icon ion-close-round"></i>',
			addClass: 'custom-toast'
		});

		//$(".bill_items").addClass('checkek_edit');
	    });
        $(document).ajaxComplete(function() {
		$('.loading-overlay').removeClass('is-active');
		$('#loading').hide();
	    });
		
		$(document).ready(function(){
			$(".js-example-basic-single").select2();
		$(".project").select2();
		$(".vendor").select2();
		//$("#Warehousestock_warehousestock_warehouseid").select2();
		$(".expense_head").select2();
		$(".company_id").select2();
		$(".tax_slab").select2();
    
			
		})
            </script>

                
            <style>
	.lefttdiv {
		float: left;
	}

	.extracharge {
		float: left;
	}

	.extracharge h4 {
		position: relative;
	}
	

	.addcharge {
		position: absolute;
		top: 2px;
		right: -25px;
		cursor: pointer;
	}

	.extracharge div {
		margin-bottom: 10px;
	}

	.addRow input.textbox {
		width: 200px;
		display: inline-block;
		margin-right: 10px;
	}

	.delbil,
	.deleteitem {

		color: #000;
		background-color: #eee;
		padding: 3px;
		cursor: pointer;
	}

	.addRow label {
		display: inline-block;
	}

	.base {
		display: none;
	}
</style>
<style>
	.dropdown {
		position: relative;
		display: inline-block;
	}

	.dropdown::before {
		position: absolute;
		content: " \2193";
		top: 0px;
		right: -8px;
		height: 20px;
		width: 20px;
	}

	button#caret {
		border: none;
		background: none;
		position: absolute;
		top: 0px;
		right: 0px;
	}

	.invoicemaindiv th,
	.invoicemaindiv td {
		padding: 10px;
		text-align: left;
	}

	.addcolumn {
		cursor: pointer;
		background-color: black;
		border: none;
		color: white;
		text-align: center;
		text-decoration: none;
		margin: 2px;
		font-size: 30px;
		width: 40px;
	}

	.invoicemaindiv .pull-right,
	.invoicemaindiv .pull-left {
		float: none !important;
	}

	.add_selection {
		background: #000;
	}

	.toast-container {
		width: 350px;
	}

	.toast-position-top-right {
		top: 57px;
		right: 6px;
	}

	.toast-item-close {
		background-image: none;
		cursor: pointer;
		width: 12px;
		height: 12px;
		text-align: center;
		border-radius: 2px;
	}

	.toast-item-image {
		font-size: 24px;
	}

	.toast-item-close:hover {
		color: red;
	}

	.toast-item {
		border: transparent;
		border-radius: 3px;
		font-size: 10px;
		opacity: 1;
		background-color: rgba(34, 45, 50, 0.8);
	}

	.toast-item-wrapper p {
		margin: 0px 5px 0px 42px;
		font-size: 14px;
		text-align: justify;
	}


	.toast-type-success {
		background-color: #00A65A;
		border-color: #00A65A;
	}

	.toast-type-error {
		background-color: #DD4B39;
		border-color: #DD4B39;
	}

	.toast-type-notice {
		background-color: #00C0EF;
		border-color: #00C0EF;
	}

	.toast-type-warning {
		background-color: #F39C12;
		border-color: #F39C12;
	}

	.span_class {
		min-width: 70px;
		display: inline-block;
	}

	.form-fields .form-control {
		height: 28px;
		display: inline-block;
		padding: 6px 3px;
		width: 60%;
	}

	.remark {
		display: none;
	}

	th {
		height: auto;
	}

	.quantity,
	.rate,
	.small_class {
		max-width: 100%;
	}

	.amt_sec {
		float: right;
	}

	.amt_sec:after,
	.padding-box:after {
		display: block;
		content: '';
		clear: both;
	}

	.client_data {
		margin-top: 6px;
	}

	*:focus {
		border: 1px solid #333;
		box-shadow: 0 0 6px 1px rgba(0, 0, 0, 0.25);
	}

	#parent,
	#parent2 {
		max-height: 150px;
	}

	.tooltip-hiden {
		width: auto;
	}

	.mb-1 {
		margin-bottom: 5px;
	}

	@media(max-width: 767px) {

		.quantity,
		.rate,
		.small_class {
			width: auto !important;
		}

		.padding-box {
			float: right;
		}

		#tax_amount,
		#item_amount {
			display: inline-block;
			float: none;
			text-align: right;
		}

		span.select2.select2-container.select2-container--default.select2-container--focus {
			width: auto !important;
		}

		span.select2.select2-container.select2-container--default.select2-container--below {
			width: auto !important;
		}

		.form-fields .form-control {
			width: 100%;
		}
	}
	@media(min-width: 1200px) {
		
		span.select2.select2-container.select2-container--default.select2-container--below,.select2-dropdown.select2-dropdown--below {
			width: 280px !important;
		}
		
	}
	

	@media(min-width: 768px) {
		.invoicemaindiv .pull-right {
			float: right !important;
		}

		.invoicemaindiv .pull-left {
			float: left !important;
		}

		
	}

	.pre_content {
		max-height: 100px;
		overflow-y: auto;
	}

	.pre_content p {
		font-size: 14px;
	}

	.select2-dropdown {
		z-index: 1;
	}
    
</style>

     </div>
</body>
</html>           
