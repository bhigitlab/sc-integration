<?php
/* @var $this EquipmentsController */
/* @var $model Equipments */

$this->breadcrumbs=array(
	'Equipments'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Equipments', 'url'=>array('index')),
	array('label'=>'Create Equipments', 'url'=>array('create')),
	array('label'=>'Update Equipments', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Equipments', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Equipments', 'url'=>array('admin')),
);
?>

<h1>View Equipments #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'equipment_name',
		'equipment_unit',
	),
)); ?>
