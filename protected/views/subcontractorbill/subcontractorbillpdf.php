<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/pdfstyle.css">
<style>
.text-center{text-align:center;}
.img-hold{width: 10%;}
.details td{
    background-color:#fff;
}
.sc-bill-content{border:3 solid}
</style>


<div class="container" id="vendors" style="margin:0px 30px;">
    <br>
    <table class="table sc-bill-content" style="margin-top:40px;">
        <tr><th colspan="4" style="height: 50px;"><h4><center><b>INTER OFFICE PAYMENT REQUEST</b></center></h4></th></tr>
        <tr><td colspan="2" style="width: 50%;height: 50px;vertical-align: middle;">REF NO: <?= $model->bill_number; ?></td>
            <td colspan="2" style="width: 50%;height: 50px;vertical-align: middle;">DATE: <?= date("d-m-Y", strtotime($model->date)); ?></td>
        </tr>
        <tr><td colspan="1" style="width: 25%;height: 50px;vertical-align: middle;">FROM DEPT</td>
            <td colspan="3" style="width: 75%;height: 50px;vertical-align: middle;">PROJECT</td>
        </tr>
        <tr><td colspan="1" style="width: 25%;height: 50px;vertical-align: middle;">TO DEPT</td>
            <td colspan="3" style="width: 75%;height: 50px;vertical-align: middle;">FINANCE</td>
        </tr>
        <tr><td colspan="1" style="width: 25%;height: 50px;vertical-align: middle;">JOB NUMBER / PROJECT DETAILS</td>
            <td colspan="3" style="width: 75%;height: 50px;vertical-align: middle;"><?php echo Projects::model()->findByPk($model->quotation->project_id)->name;?></td>
        </tr>
        <tr><td colspan="1" style="width: 25%;height: 50px;vertical-align: middle;">PAYEE DETAILS</td>
            <td colspan="3" style="width: 75%;height: 50px;vertical-align: middle;"><?php
                        echo Subcontractor::model()->findByPk($model->quotation->subcontractor_id)->subcontractor_name;
                    ?></td>
        </tr>
        <tr><td colspan="1" style="width: 25%;height: 100px;vertical-align: middle;">PAYMENT DETAILS</td>
            <td colspan="3" style="width: 75%;height: 100px;vertical-align: middle;"><?php 
                        $pan_no = Subcontractor::model()->findByPk($model->quotation->subcontractor_id)->pan_no;
                        $pan_no = ($pan_no)?" PAN CARD: $pan_no":'';
                        $payment_details = (Subcontractor::model()->findByPk($model->quotation->subcontractor_id)->account_details)?:'';
                        echo "$payment_details <br>$pan_no";
                        
                    ?></td>
        </tr>
        <tr><td colspan="1" style="width: 25%;height: 100px;vertical-align: middle;">PAYMENT REQUESTED AMOUNT</td>
            <td colspan="3" style="width: 75%;height: 100px;vertical-align: middle;"><?php echo Controller::money_format_inr($model->total_amount,2);  ?>/-</td>
            </tr>
        <tr><td colspan="1" style="width: 25%;height: 100px;vertical-align: middle;">REMARKS / DETAILS</td>
            <td colspan="3" style="width: 75%;height: 100px;vertical-align: middle;"><?php echo $model->remarks; ?></td>
        </tr>
        <tr><th style="width: 25%;height: 45px;vertical-align: middle;" ><center >REQUESTED BY</center></th>
            <th style="width: 25%;height: 45px;vertical-align: middle;" ><center >CHECKED BY</center></th>
            <th style="width: 25%;height: 45px;vertical-align: middle;" ><center >APPROVED BY</center></th>
            <th style="width: 25%;height: 45px;vertical-align: middle;" ><center >ACCOUNTED BY</center></th>
        </tr>
        <tr><td style="width: 25%;height: 130px;vertical-align: middle;text-align: center;"><?php echo $model->requested_by; ?></td>
            <td style="width: 25%;height: 130px;vertical-align: middle;"></td>
            <td style="width: 25%;height: 130px;vertical-align: middle;"></td>
            <td style="width: 25%;height: 130px;vertical-align: middle;"></td>
        </tr>
    </table>
</div>