<?php
/* @var $this PurchaseCategoryController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Purchase Categories',
);

$this->menu=array(
	array('label'=>'Create PurchaseCategory', 'url'=>array('create')),
	array('label'=>'Manage PurchaseCategory', 'url'=>array('admin')),
);
?>

<h1>Purchase Categories</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
