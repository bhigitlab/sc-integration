<?php

class GlobalApiCall
{
    public static function callApi($slug, $method, $request = array())
    {
        $log_id = ApiLogs::apiLog($slug, $method, $request);
        $ch = curl_init();
        $headers = array();
        $result = array();
        $data = $request;
        $data['coms_api_log_id'] = $log_id;
        $token = APP_AUTH_KEY;
        $headers[] = "accept: application/json";
        $headers[] = 'APP_KEY:' . $token;
        $headers[] = 'Content-Type: application/json'; // Specify that we're sending JSON data

        curl_setopt($ch, CURLOPT_URL, GLOBAL_API_DOMAIN . $slug);
        curl_setopt($ch, CURLOPT_NOBODY, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_FAILONERROR, false);

        // Set timeout options
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10); // Timeout for connection (in seconds)
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);        // Timeout for response (in seconds)

        $response = curl_exec($ch);
        $response = json_decode($response, true);
        if (!isset($response['success'])) {
            $response['success'] = false;
        }

        return $response;
        exit;
    }
    public static function croneCallApi($slug, $method, $request = array(),$log_id)
    {
        $ch = curl_init();
        $headers = array();
        $result = array();
        $data = $request;
        $data['pms_api_log_id'] = $log_id;
        $token = APP_AUTH_KEY;
        $headers[] = "accept: application/json";
        $headers[] = 'APP_KEY:' . $token;
        $headers[] = 'Content-Type: application/json'; // Specify that we're sending JSON data

        curl_setopt($ch, CURLOPT_URL, GLOBAL_API_DOMAIN . $slug);
        curl_setopt($ch, CURLOPT_NOBODY, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_FAILONERROR, false);

        $response = curl_exec($ch);
        $response = json_decode($response, true);
        if (!isset($response['success'])) {
            $response['success'] = false;
        }

        return $response;
        exit;
    }
}