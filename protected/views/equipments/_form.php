<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.min.js"></script>
<?php 
$pms_api_integration=ApiSettings::model()->pmsIntegrationStatus();
?>
<div class="form">
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'equipments-form',
        'enableAjaxValidation' => false,
        
    ));
    ?>

    <div class="panel-body">
        <div class="row">
        <input type="hidden" id="pms_integration" value="<?php echo $pms_api_integration ;?>">
            <div class="col-md-4">
                <div class="form-group">
                    <?php echo $form->labelEx($model, 'equipment_name'); ?>
                    <?php echo $form->textField($model, 'equipment_name', array(
                        'size' => 30,
                        'maxlength' => 255,
                        'class' => 'form-control',
                        'pattern' => '[A-Za-z\s]+',
                        'title' => 'Only alphabetic characters and spaces are allowed',
                        'required'=>'required',
                        'onchange' => 'checkDuplicateEquipmentName(this.value)'
                    )); ?>
                    <?php echo $form->error($model, 'equipment_name'); ?>
                </div>
                <div id="equipment-name-error" class="text-danger"></div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                <?php echo $form->labelEx($model, 'equipment_unit'); ?>
                    <?php
                        $pms_api_integration=ApiSettings::model()->pmsIntegrationStatus();
                        if($pms_api_integration==1){
                         $unitData = CHtml::listData(Unit::model()->findAll(array(
                             'condition' => 'pms_unit_id > 0',
                         )), 'id', 'unit_name');
                        }
                        else{
                         $unitData = CHtml::listData(Unit::model()->findAll(), 'id', 'unit_name');
                        }
                        $options = [];
                        if (is_array($model->equipment_unit)) {
                            foreach ($model->equipment_unit as $unit) {
                                $options[$unit] = ['selected' => true];
                            }
                        }
                        
                        echo $form->dropDownList($model, 'equipment_unit[]', $unitData, array(
                            'class' => 'form-control js-example-basic-multiple',
                            'multiple' => 'multiple',
                            'required'=>'required',
                            'options' => $options,
                        ));
                    ?>
                    <?php echo $form->error($model, 'equipment_unit'); ?>
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-md-4 save-btnHold">
                <label style="display:block;">&nbsp;</label>
                <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-info btn-sm submit')); ?>
                <?php if (Yii::app()->user->role == 1) {
                    if (!$model->isNewRecord) { 
                    }
                } ?>
                
                <?php
                if (!$model->isNewRecord) {
                    echo CHtml::ResetButton('Close', array('onclick' => 'closeaction(this,event);', 'class' => 'btn btn-sm'));
                } else {
                    echo CHtml::ResetButton('Reset', array('style' => 'margin-right:3px;', 'class' => 'btn btn-default btn-sm'));
                    echo CHtml::ResetButton('Close', array('onclick' => 'closeaction(this,event);', 'class' => 'btn btn-sm'));
                }
                echo CHtml::hiddenField('execute_api', 0);
                ?>
            </div>
        </div>
    </div>
    <?php echo $form->hiddenField($model, 'pms_equipment_id'); ?>
    <?php $this->endWidget(); ?>
</div>
<script>
    $(document).ready(function() {
        $('.js-example-basic-multiple').select2({
            width: '100%',
            placeholder: "Select Units",
        });
    });
    $(document).ready(function() {
        $('#<?php echo CHtml::activeId($model, 'equipment_name'); ?>').on('input', function() {
            var input = $(this).val();
            var valid = /^[A-Za-z\s]*$/.test(input);
            if (!valid) {
                $(this).val(input.replace(/[^A-Za-z\s]/g, ''));
            }
        });
    });

    function checkDuplicateEquipmentName(equipmentName) {
    console.log('Checking duplicate for equipment name:', equipmentName); // Debugging

    $.ajax({
        url: '<?php echo Yii::app()->createUrl("equipments/checkDuplicateEquipmentName"); ?>', // Update controllerName to your actual controller
        type: 'POST',
        data: {
            equipment_name: equipmentName,
            YII_CSRF_TOKEN: '<?php echo Yii::app()->request->csrfToken; ?>' // Include CSRF token if CSRF validation is enabled
        },
        success: function(response) {
            var data = $.parseJSON(response);
            console.log('Response from server:', data); // Debugging

            if (data.isDuplicate) {
                $('#equipment-name-error').text('This equipment name is already taken.');
                $('#submitButton').prop('disabled', true);
            } else {
                $('#equipment-name-error').text('');
                $('#submitButton').prop('disabled', false);
            }
        },
        error: function(xhr, status, error) {
            console.log('AJAX error:', error); // Debugging
            $('#equipment-name-error').text('Error checking equipment name.');
            $('#submitButton').prop('disabled', false);
        }
    });
}
$(document).ready(function() {
        document.getElementById('equipments-form').onsubmit = function() {
        var pms_integration = $("#pms_integration").val();
        var pmsEquipmentId = $('#Equipments_pms_equipment_id').val();
        if(pms_integration =='1'){
            if(pmsEquipmentId=='0'){
                var confirmApiCall = confirm("Do you want to save the equipment in Pms?");
                if (confirmApiCall) {
                    document.getElementById('execute_api').value = 1;
                }
            }
            else if (pmsEquipmentId!='0') {
                document.getElementById('execute_api').value = 1;
            }
       }
    };
    });
</script>