<?php
/* @var $this ReceiptController */
/* @var $data Receipt */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('rec_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->rec_id), array('view', 'id'=>$data->rec_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('project_id')); ?>:</b>
	<?php echo CHtml::encode($data->project_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('receipt_amount')); ?>:</b>
	<?php echo CHtml::encode($data->receipt_amount); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('date')); ?>:</b>
	<?php echo CHtml::encode($data->date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('description')); ?>:</b>
	<?php echo CHtml::encode($data->description); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('payment_type')); ?>:</b>
	<?php echo CHtml::encode($data->payment_type); ?>
	<br />


</div>