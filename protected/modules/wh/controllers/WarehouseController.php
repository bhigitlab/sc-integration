<?php

class WarehouseController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout = '//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array(
				'allow',  // allow all users to perform 'index' and 'view' actions
				'actions' => array('index', 'view','stockcorrection','defectreturn','createdefectReturn','GetItemsByWarehouse','addItemsForDefectReturn','validateReturnNumber','DefectView','DefectUpdate',
                'changeStockCorrectionStatus','approveDefectReturn','consumptionrequest','createconsumption','updateConsumption','approveConsumedQuantity','RefreshButton','getNewInputRow','GetfieldsForEdit','RejectConsumedQuantity','getStockEstimatedQuantity'),
				'users' => array('@'),
			),
			array(
				'allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions' => array('create', 'update','delete', 'AutoReceiptToWarehouse', 'ViewStocksInOtherWarehouse','getReceiptItems','getStockItems','addStockCorrection','unassignedwarehouse','GetSpecifications','UpdateSpecificationAndWarehouse','GetRate','GetWarehouses','ViewConsumption'),
				'users' => array('@'),
			),
			array(
				'allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions' => array('admin', 'delete'),
				'users' => array('admin'),
			),
			array(
				'deny',  // deny all users
				'users' => array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view', array(
			'model' => $this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model = new Warehouse;
		$model2 = new Projects;
		$tblpx = Yii::app()->db->tablePrefix;
		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);


		if (isset($_POST['Warehouse'])) {

            $whassigned_to = $_POST['Warehouse']['assigned_to'];
            $model->attributes = $_POST['Warehouse'];
            $model->assigned_to = NULL;

            if (!empty($whassigned_to)) {
                $assigned_to = implode(",", $whassigned_to);
                $model->assigned_to = $assigned_to;
            }


			$project = isset($_POST['Warehouse']['project_id']) ? $_POST['Warehouse']['project_id'] : Null;
			$model->status = 1;
			$model->created_by = yii::app()->user->id;
			$model->project_id = $project;
			$model->created_date = date('Y-m-d');
			$model->company_id = Yii::app()->user->company_id;
			$model->updated_by = yii::app()->user->id;
			$model->updated_date = date('Y-m-d');



			if ($model->save() && $model->validate()) {
				$this->redirect(array('index'));
			}
		}

		if (isset($_GET['layout'])) {
			$this->layout = false;
		}

        $receiptStockData = $this->getReceiptStockData();
        $auto_receipt_to_warehouse_status = $receiptStockData['auto_receipt_to_warehouse_status'];
        $view_stocks_in_other_warehouse_status = $receiptStockData['view_stocks_in_other_warehouse_status'];

        $this->render('create', array(
            'model' => $model,
            'model2' => $model2,
            'auto_receipt_to_warehouse_status' => $auto_receipt_to_warehouse_status,
            'view_stocks_in_other_warehouse_status' => $view_stocks_in_other_warehouse_status
        ));
    }

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{


		$tblpx = Yii::app()->db->tablePrefix;
		$this->layout = false;
		$model = $this->loadModel($id);
		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);
		$userids = array();
        if (isset($_POST['Warehouse'])) {
            $model->attributes = $_POST['Warehouse'];
            $model->assigned_to = NULL;
            $hidden_relation_to_project = $_POST['Warehouse']['hidden_relation_to_project'];
            $hidden_project_id = $_POST['Warehouse']['hidden_project_id'];
            $whassigned_to = $_POST['Warehouse']['assigned_to'];

            if (!empty($whassigned_to)) {
                $assigned_to = implode(",", $whassigned_to);
                $model->assigned_to = $assigned_to;
            }

            $project = isset($hidden_project_id) ? $hidden_project_id : NULL;
            if (isset($hidden_relation_to_project) && isset($hidden_project_id)) {
                $model->status = 0;
                if ($hidden_relation_to_project != 0 && $hidden_project_id != "") {
                    $model->status = 1;
                }
            }

            $model->project_id = $project;
            $model->updated_by = yii::app()->user->id;
            $model->updated_date = date('Y-m-d');
            $model->company_id = Yii::app()->user->company_id;

            if ($model->save()) {
                if (Yii::app()->user->returnUrl != 0) {
                    $this->redirect(array('index', 'Clients_page' => Yii::app()->user->returnUrl));
                } else {
                    $this->redirect(array('index'));
                }
            }
        }

        $receiptStockData = $this->getReceiptStockData();
        $auto_receipt_to_warehouse_status = $receiptStockData['auto_receipt_to_warehouse_status'];
        $view_stocks_in_other_warehouse_status = $receiptStockData['view_stocks_in_other_warehouse_status'];

        $this->render('update', array(
            'model' => $model,
            'auto_receipt_to_warehouse_status' => $auto_receipt_to_warehouse_status,
            'view_stocks_in_other_warehouse_status' => $view_stocks_in_other_warehouse_status
        ));
    }

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{		
        $model = $this->loadModel($id);
        $transaction = Yii::app()->db->beginTransaction();
        try {
            if (!$model->delete()) {
                $success_status = 0;
                throw new Exception(json_encode($model->getErrors()));
            } else {
                $success_status = 1;
            }
            $transaction->commit();
        } catch (Exception $error) {
            $transaction->rollBack();
            $success_status = 0;
        } finally {
            if ($success_status == 1) {
                echo json_encode(array('response' => 'success', 'msg' => 'Data Deleted Successfully'));
            } else {
                if ($error->errorInfo[1] == 1451) {
                    echo json_encode(array('response' => 'warning', 'msg' => 'Cannot Delete ! This record alredy in use.'));
                } else {
                    echo json_encode(array('response' => 'error', 'msg' => 'Some Error Occured'));
                }
            }
        }
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model = new Warehouse('search');
        $receiptStockData = $this->getReceiptStockData();
        $auto_receipt_to_warehouse_status = $receiptStockData['auto_receipt_to_warehouse_status'];
        $view_stocks_in_other_warehouse_status = $receiptStockData['view_stocks_in_other_warehouse_status'];

        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Warehouse']))
            $model->attributes = $_GET['Warehouse'];

        if (Yii::app()->user->role == 1) {
            $this->render('index', array(
                'model' => $model,
                'auto_receipt_to_warehouse_status' => $auto_receipt_to_warehouse_status,
                'view_stocks_in_other_warehouse_status' => $view_stocks_in_other_warehouse_status,
                'dataProvider' => $model->search(),
            ));
        } else {

            $this->render('index', array(
                'model' => $model,
                'dataProvider' => $model->search(Yii::app()->user->id),
                'auto_receipt_to_warehouse_status' => '',
                'view_stocks_in_other_warehouse_status' => ''
            ));
        }
    }
    public function actionUnassignedwarehouse(){
        $model = new Warehouse('search');
        $receiptStockData = $this->getReceiptStockData();
        $auto_receipt_to_warehouse_status = $receiptStockData['auto_receipt_to_warehouse_status'];
        $view_stocks_in_other_warehouse_status = $receiptStockData['view_stocks_in_other_warehouse_status'];

        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Warehouse']))
            $model->attributes = $_GET['Warehouse'];

        if (Yii::app()->user->role == 1) {
            $this->render('unassigned_warehouse', array(
                'model' => $model,
                'auto_receipt_to_warehouse_status' => $auto_receipt_to_warehouse_status,
                'view_stocks_in_other_warehouse_status' => $view_stocks_in_other_warehouse_status,
                'dataProvider' => $model->searchUnassigned(),
            ));
        } else {

            $this->render('unassigned_warehouse', array(
                'model' => $model,
                'dataProvider' => $model->searchUnassigned(Yii::app()->user->id),
                'auto_receipt_to_warehouse_status' => '',
                'view_stocks_in_other_warehouse_status' => ''
            ));
        }
    }
	public function actionAutoReceiptToWarehouse()
	{


		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if (isset($_POST['id'])) {
			$model = GeneralSettings::model()->findByPk($_POST['id']);
			if ($_POST['id'] == 1) {
				$model->status = $_POST['status'];
				if ($model->save())
					echo "Success";
				else
					echo "Some problem occured";
			} else {
				echo "Some problem occured";
			}
		} else {
			echo "Some problem occured";
		}
	}
	public function actionViewStocksInOtherWarehouse()
	{


		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if (isset($_POST['id'])) {
			$model = GeneralSettings::model()->findByPk($_POST['id']);
			if ($_POST['id'] == 2) {
				$model->status = $_POST['status'];
				if ($model->save())
					echo "Success";
				else
					echo "Some problem occured";
			} else {
				echo "Some problem occured";
			}
		} else {
			echo "Some problem occured";
		}
	}
	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model = new Warehouse('search');
		$model->unsetAttributes();  // clear any default values
		if (isset($_GET['Warehouse']))
			$model->attributes = $_GET['Warehouse'];

		$this->render('admin', array(
			'model' => $model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Warehouse the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model = Warehouse::model()->findByPk($id);
		if ($model === null)
			throw new CHttpException(404, 'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Warehouse $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'warehouse-form') {
			if ($_POST['Warehouse']['relation_to_project'] == '1') {
				$model->setScenario('project_required');
			}
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
    public function getReceiptStockData() {

        $auto_receipt_to_warehouse_id = 1;
        $auto_receipt_to_warehouse_data = GeneralSettings::model()->findByPk($auto_receipt_to_warehouse_id);
        $auto_receipt_to_warehouse_status = $auto_receipt_to_warehouse_data->status; //here
        $view_stocks_in_other_warehouse_id = 2;
        $view_stocks_in_other_warehouse_data = GeneralSettings::model()->findByPk($view_stocks_in_other_warehouse_id);
        $view_stocks_in_other_warehouse_status = $view_stocks_in_other_warehouse_data->status; // here

        return ( array(
            'auto_receipt_to_warehouse_status' => $auto_receipt_to_warehouse_status,
            'view_stocks_in_other_warehouse_status' => $view_stocks_in_other_warehouse_status
        ));
    }

    public function actionStockCorrection() {
        $criteria = new CDbCriteria();
        $criteria->order = "id DESC"; // Modify as needed
        $dataProvider = new CActiveDataProvider('WarehouseStockCorrection', array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => 10, // Adjust items per page
            ),
        ));

        $this->render('stockcorrection', array('dataProvider' => $dataProvider));   
    }


    public function actiongetReceiptItems(){
        $whId = $_REQUEST['warehouseId'];

        $sql = 'SELECT *,warehousereceipt_itemid as item_id '
                . 'FROM '.$this->tableNameAcc('warehousereceipt_items',0)
                . ' WHERE warehousereceipt_warehouseid='.$whId;
        $receiptitems = Yii::app()->db->createCommand($sql)->queryAll();

        $sql = 'SELECT *,warehousedespatch_itemid as item_id '
                . 'FROM '.$this->tableNameAcc('warehousedespatch_items',0)
                . ' WHERE warehousedespatch_warehouseid='.$whId;
        $despatchitems = Yii::app()->db->createCommand($sql)->queryAll();
        $items = array_merge($receiptitems,$despatchitems);
        $options = array();
			
        foreach ($items as $item) :
            $specificsql = "SELECT id, cat_id,brand_id, specification,unit"
                            . " FROM ".$this->tableNameAcc('specification',0)
                            . " WHERE id=" . $item['item_id'] . "";
            $specification = Yii::app()->db->createCommand($specificsql)->queryRow();
            $parent_categorysql = "SELECT * FROM ".$this->tableNameAcc('purchase_category',0) 
                                    . " WHERE id='" . $specification['cat_id'] . "'";
            $parent_category = Yii::app()->db->createCommand($parent_categorysql)->queryRow();
            $brand = '';
            if ($specification['brand_id'] != NULL) {
                $brandsql = "SELECT brand_name FROM ".$this->tableNameAcc('brand',0) ." 
                            WHERE id=" . $specification['brand_id'] . "";
                $brand_details = Yii::app()->db->createCommand($brandsql)->queryRow();
                $brand = '-' . $brand_details['brand_name'];
            } 

            $spc_details = $parent_category['category_name'] . $brand . ' - ' . $specification['specification'];
            $options[$item['item_id']] = $spc_details;
        endforeach;

        echo CHtml::dropDownList('', '', $options,array(
            'empty'=>'Choose Item','class'=>'form-control select_box receiptItems'
            )
        ); 
			
    }


    public function actiongetStockItems(){
		
        $warehouse_id = $_POST['warehouse_id'];
        $item_id=$_POST['item_id'];

        $sql = 	'SELECT *  FROM `jp_warehousestock` WHERE `warehousestock_warehouseid` = '.$warehouse_id
                . ' AND `warehousestock_itemid` ='.$item_id
                . ' AND rate !=0';
        $stockData=Yii::app()->db->createCommand($sql)->queryAll();
				
        $items = $this->renderPartial('_stock_data', array(
            'stockData' => $stockData
        ));
   
        echo $items;  exit;      
    }

    public function actionaddStockCorrection(){
        $status='';
	$data = json_decode($_REQUEST['data'],true);
		
        foreach ($data as $key => $value) {
            $model = new WarehouseStockCorrection();
            $model->attributes = $value;
            $model->warehouse_id = $value['warehouse_id'];
            $model->Item_Id = $value['Item_Id'];
            $model->stock_quantity = $value['stock_quantity'];
            $model->wh_receipt_quantity = $value['wh_receipt_quantity'];
            $model->rate =floatval($value['wh_rate']);
            $model->remarks = $value['remarks'];
            $model->created_by = Yii::app()->user->id;
            $model->created_date = date('Y-m-d H:i:s');

            if (!$model->save()) {
                $status=0;
               
            } else {
                $status=1;
                
            }
        }
		if($status==0){
            echo json_encode(array('response' => 'error', 'msg' => 'Some Error occured'));
        }else{
            echo json_encode(array('response' => 'success', 'msg' => 'Stock Correction added successfully'));
        }

    }

    public function actiondefectreturn() {
        $model = new DefectReturn('search');
        $model->unsetAttributes();
        if (isset($_GET['DefectReturn']))
            $model->attributes = $_GET['DefectReturn'];

        $this->render('defectreturn', array(
            'model' => $model,
            'dataProvider' => $model->search(),
        ));
    }

    public function actioncreatedefectReturn(){
	$model=new DefectReturn;
        $tblpx = Yii::app()->db->tablePrefix;
		
        if (isset($_POST['PurchaseReturn'])) {
            $model->attributes = $_POST['PurchaseReturn'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->return_id));
        }

        if(isset($_POST['PurchaseReturn']))
        {
            $id = $_POST["billid"];
            $newmodel = $this->loadModel($id);
            $newmodel->return_status = 2;
            $billNumber = $_POST['PurchaseReturn']['return_number'];
            $model->return_date = date('Y-m-d', strtotime($_POST['PurchaseReturn']['return_date']));
            if ($newmodel->save()) {
                $deleteBills = Yii::app()->db->createCommand("DELETE FROM {$tblpx}defect_return WHERE return_number = '" . $billNumber . "' and return_status = 1");
                $deleteBills->execute();
                Yii::app()->user->setFlash('success', "Purchase Return details added successfully!");
                $this->redirect(array('admin'));
            }
        }

        $this->render('createdefectreturn',array(
			'model'=>$model,
		));
    }

    public function actionGetItemsByWarehouse() {
        $id = $_GET['id'];
        $client = "";
        $pData = "";
        $pitems = "";

        $tblpx = Yii::app()->db->tablePrefix;
        $sql = "SELECT * FROM " . $tblpx . "warehousereceipt_items "
                . "WHERE `warehousereceipt_id` =" . $id
                . " AND `warehousereceipt_rejected_effective_quantity` > 0 "
                . "ORDER BY item_id";

        $pData = Yii::app()->db->createCommand($sql)->queryAll();


        $pitems = $this->renderPartial('purchaseitems', array(
            'purchase' => $pData,
        ));
        echo $pitems;
    }

    public function actionValidateReturnNumber() {
        $billNo = isset($_POST["billno"])?$_POST["billno"]:'';
        $tblpx = Yii::app()->db->tablePrefix;
        $sql = "SELECT * FROM " . $tblpx . "defect_return "
                . "WHERE return_number = '" . $billNo . "'";
        $billData = Yii::app()->db->createCommand($sql)->queryAll();
        if (!empty($billData)) {
           echo 1;
        } else {
           echo 2;
        }
    }

    public function actionaddItemsForDefectReturn() {
        $tblpx = Yii::app()->db->tablePrefix;
        $bill_Id = $_POST["bill_id"];
        $returnNumber = $_POST["returnnumber"];
        $returnDate = $_POST["returndate"];
        $billNTotal = $_POST["billamount"];
        $billNGTotal = $_POST["billtotal"];
        $itemId = $_POST["itemid"];
        $quantity = $_POST["quantity"];
        $unit = $_POST["unit"];
        $rate = $_POST["rate"];
        $newAmt = $_POST["amount"];

        $newTotal = $_POST["totalamount"];
        $billId = $_POST["billid"];
        $aStat = $_POST["astat"];
        $categoryId = $_POST["categoryid"];
        $categoryName = $_POST["categoryname"];
        $availQty = $_POST["availqty"];
        $billItemId = $_POST["billitem"];
        $purchaseStatus = $_POST["purchasestatus"];
        $bills_details = Bills::model()->findByPk($bill_Id);
        $returnNDate = date('Y-m-d', strtotime($returnDate));
        $createdBy = Yii::app()->user->id;
        $company_id = $bills_details['company_id'];
        $createdDate = date('Y-m-d');
        if ($billId == "") {
            $addBills = Yii::app()->db->createCommand("INSERT INTO {$tblpx}defect_return(receipt_id, "
            . "return_number, return_date, return_amount,  created_by, created_date, "
            . "return_status, company_id) "
            . "VALUES (" . $bill_Id . ",'" . $returnNumber . "','" . $returnNDate . "','" . $billNTotal . "'," . $createdBy . ",'" . $createdDate . "',1, " . $company_id . ")");
            $addBills->execute();
            $billId = Yii::app()->db->getLastInsertID();
        } else {
            $updateBills = Yii::app()->db->createCommand("update {$tblpx}defect_return "
            . "SET receipt_id = " . $bill_Id . ", return_number = '" . $returnNumber . "', "
            . "return_date = '" . $returnNDate . "', return_amount = '" . $billNTotal . "',  "
            . "updated_date = '" . date('Y-m-d') . "', return_status = 1, "
            . "company_id= " . $company_id . " where return_id = " . $billId);
            $updateBills->execute();
        }
        if ($aStat == 1) {
            if ($categoryId != '')
                $updateBillItems = Yii::app()->db->createCommand("INSERT INTO {$tblpx}defect_returnitem(return_id, "
                . "receiptitem_id, returnitem_quantity, returnitem_unit, returnitem_rate, "
                . "returnitem_amount,  category_id, created_date,created_by) "
                . "VALUES (" . $billId . "," . $itemId . ",'" . $quantity . "','" . $unit . "','" . $rate . "','" . $newAmt . "'," . $categoryId . ",'" . $createdDate . "','".$createdBy."')");
            else
                $updateBillItems = Yii::app()->db->createCommand("INSERT INTO {$tblpx}defect_returnitem(return_id, "
                . "receiptitem_id, returnitem_quantity, returnitem_unit, returnitem_rate, "
                . "returnitem_amount,  remark, created_date,created_by) "
                . "VALUES (" . $billId . "," . $itemId . ",'" . $quantity . "','" . $unit . "','" . $rate . "','" . $newAmt . "','" . $categoryName . "','" . $createdDate . "','".$createdBy."')");
            $updateBillItems->execute();
            $billItemId = Yii::app()->db->getLastInsertID();
            $quantityRem = $availQty - $quantity;
        } else if ($aStat == 2) {
            $updateBillItems = Yii::app()->db->createCommand("DELETE FROM {$tblpx}defect_returnitem "
            . "WHERE return_id = " . $billId . " and receiptitem_id = " . $itemId);
            $updateBillItems->execute();
            $quantityRem = $availQty;
        } else if ($aStat == 3) {
            if ($categoryId != '')
                $updateBillItems = Yii::app()->db->createCommand("UPDATE {$tblpx}defect_returnitem "
                . "SET returnitem_quantity = '" . $quantity . "', returnitem_unit = '" . $unit . "', returnitem_rate = '" . $rate . "', returnitem_amount = '" . $newAmt . "',  category_id = " . $categoryId . " WHERE return_id = " . $billId . " and receiptitem_id = " . $itemId);
            else
                $updateBillItems = Yii::app()->db->createCommand("UPDATE {$tblpx}defect_returnitem "
                . "SET returnitem_quantity = '" . $quantity . "', returnitem_unit = '" . $unit . "', returnitem_rate = '" . $rate . "', returnitem_amount = '" . $newAmt . "', remark = '" . $categoryName . "' WHERE return_id = " . $billId . " and receiptitem_id = " . $itemId);
            $updateBillItems->execute();
            $quantityRem = $availQty - $quantity;
        } else {
            $updateBillItems = Yii::app()->db->createCommand("DELETE FROM {$tblpx}defect_returnitem "
            . "WHERE return_id = " . $billId . " and receiptitem_id = " . $itemId);
            $updateBillItems->execute();
            $quantityRem = $availQty;
        }
        if ($quantityRem > 0)
            $itemStatus = 90;
        else
            $itemStatus = 91;

        $result[0] = $billId;
        $result[1] = $billItemId;
        echo json_encode($result);
    }

    public function actionDefectView($id) {
        $model = DefectReturn::model()->findByPk($id);
        $newmodel = DefectReturnitem::model()->findAll(array("condition" => "return_id = " . $id));
        $render_datas = array(
            'model' => $model,
            'newmodel' => $newmodel,
        );

        if (filter_input(INPUT_GET, 'export') == 'pdf') {
            $this->logo = $this->realpath_logo;
            $mPDF1 = Yii::app()->ePdf->mPDF();
            $mPDF1 = Yii::app()->ePdf->mPDF('', 'A4');
            $sql = 'SELECT template_name '
                . 'FROM jp_quotation_template '
                . 'WHERE status="1"';
            $selectedtemplate =Yii::app()->db->createCommand($sql)->queryScalar();
            
            $template = $this->getTemplate($selectedtemplate);
            $mPDF1->SetHTMLHeader($template['header']);
            $mPDF1->SetHTMLFooter($template['footer']);
            $mPDF1->AddPage('','', '', '', '', 0,0,32, 30, 10,0);
            $mPDF1->WriteHTML($this->renderPartial('viewreturnitems', $render_datas, true));            
            $mPDF1->Output('DEFECT_RETURN.pdf', 'D');            
        }elseif (filter_input(INPUT_GET, 'export') == 'csv') {
            $exported_data = $this->renderPartial('viewreturnitems', $render_datas, true);
            $export_filename = 'DEFECT_RETURN' . date('Ymd_His');
            ExportHelper::exportGridAsCSV($exported_data, $export_filename);
        } else {
            $this->render('viewreturnitems', $render_datas);
        }
    }

    public function actionDefectUpdate($id)
	{
		$model    = DefectReturn::model()->findByPk($id);
        $client   = '';
        $tblpx    = Yii::app()->db->tablePrefix;
        $purchase = Yii::app()->db->createCommand("SELECT * FROM ".$tblpx."defect_return WHERE return_id = ".$id)->queryRow();
        $bill_Id      = $purchase["receipt_id"];
        $client = $this->GetItemsReceiptId($id,$bill_Id);
                
		if(isset($_POST['PurchaseReturn']))
		{
			$model->attributes=$_POST['PurchaseReturn'];
            $model->return_date = date('Y-m-d', strtotime($_POST['PurchaseReturn']['return_date']));
            $model->updated_date = date('Y-m-d');
            $model->return_status = 2;
			if($model->save())
				$this->redirect(array('defectreturn','id'=>$model->return_id));
		}
		$this->render('updatereturn',array(
			'model'=>$model,'client' => $client,
		));
	}

    public function GetItemsReceiptId($itemid,$bill_Id){
        
        $client = "";
        $tblpx = Yii::app()->db->tablePrefix;        
        $pData = Yii::app()->db->createCommand("SELECT a.*,b.*,a.warehousereceipt_itemid as pcategory,b.category_id as bcategory,b.remark as bremark, a.item_id as receiptitem_id FROM {$tblpx}warehousereceipt_items a LEFT JOIN {$tblpx}defect_returnitem b ON a.item_id = b.receiptitem_id  WHERE a.warehousereceipt_id = ".$bill_Id." and b.return_id = ".$itemid." GROUP by a.item_id")->queryAll();
        $client = $this->renderPartial('updatepurchaseitems',array(
        'purchase'=> $pData, 'billId' =>$itemid,
        ),true);
        return $client;
    }

    public function actionchangeStockCorrectionStatus($id){
        $model = WarehouseStockCorrection::model()->findByPk($id);
        $status = $_POST['status'];
        $model->approval_status = '1';
        $msg = 'Approved  successfully';
        if($status ==2){
            $model->approval_status = '2';
            $msg = 'Rejected  successfully';
        }
        
        if (!$model->save()) {
            echo json_encode(array('response' => 'error', 'msg' => 'Some Error occured'));
        } else {
            echo json_encode(array('response' => 'success', 'msg' => $msg));
        }

    }
    public function actionapproveDefectReturn(){
        $model    = DefectReturn::model()->findByPk($_REQUEST['id']);
        $model->approval_status = 1;
                    
        if (!$model->save()) {
            echo json_encode(array('response' => 'error', 'msg' => 'Some Error occured'));
        } else {
            echo json_encode(array('response' => 'success', 'msg' => 'Approved successfully'));
        }
    }

    public function actionconsumptionrequest(){
        $sql = "SELECT * FROM `pms_acc_wpr_item_consumed` ";
        $consume_data_item = Yii::app()->db->createCommand($sql)->queryAll();

        $sql = "SELECT * FROM `jp_consumption_request` ORDER BY id DESC";
        $consume_data = Yii::app()->db->createCommand($sql)->queryAll();
        //project
        $sql = "SELECT * FROM `pms_acc_wpr_item_consumed` WHERE `coms_project_id` IS NULL";
        $wpr_null_data = Yii::app()->db->createCommand($sql)->queryAll();
        //material
        $sql = "SELECT * FROM `pms_acc_wpr_item_consumed` WHERE `coms_material_id` IS NULL";
        $wpr_material_datas = Yii::app()->db->createCommand($sql)->queryAll();
        //api integration
        $pms_api_integration_model=ApiSettings::model()->findByPk(1);
        $pms_api_integration =$pms_api_integration_model->api_integration_settings;

        $this->render('consumptionrequest', array(
			'consume_data' => $consume_data,
            'wpr_null_data'=> $wpr_null_data,
            'wpr_material_datas'=> $wpr_material_datas,
            'pms_api_integration'=>$pms_api_integration,
		));
    }

    public function actiongetNewInputRow(){
        $index = $_POST['index'];
        if($index==''){
            $index==0;
        }
        $materails='';
        $project = $_POST['project'];
        $material_arr=array();
        $wprData = PmsAccWprItemConsumed::model()->findAll();
        if(!empty($project)){
            $model_exist= Itemestimation::model()->find(array("condition" => "project_id = '$project'"));
            if(!empty($model_exist)){
                 $estimation_id =$model_exist->itemestimation_id;
            
                 $sql="SELECT m.material FROM `jp_material_estimation` m LEFT JOIN `jp_itemestimation` e ON e.itemestimation_id=m.estimation_id WHERE e.itemestimation_id=".$estimation_id ." AND e.itemestimation_status=2 AND  m.estimation_id=".$estimation_id ."  AND m.project_id=".$project;
                $materails = Yii::app()->db->createCommand($sql)->queryAll();
              
                 $material_arr=array();
                 if(!empty($materails)){
                    foreach($materails as $materail_item){
                        $items_det= Materials::Model()->findByPk($materail_item['material']);
                        
                        if(!empty($items_det)){
                            $material_arr[$items_det->id]=$items_det["material_name"];
                        }

                    }
                 }
                // echo "<pre>";print_r($material_arr);exit;
            }
           
        }

        $this->renderPartial('_consumption_item_add', array('modelitem' => $wprData, 'index' => $index,'materials'=>$material_arr));

    }
    public function actiongetStockEstimatedQuantity(){
        $rate='';
        $rate=$_POST["rate"];
        $specification_id='';
        $specification_id=$_POST["specification_id"];
        $material_id='';
        $material=$_POST["material_id"];
        $project =$_POST["project"];
        $warehouseId='';
        $estimated_bal_quantity=0;
        $stock_qty=0;
        $warehouseId=$_POST["warehouseId"];
        $balance='';
        if(!empty([$_POST["specification_id"]])){
            $sqlStock = "SELECT COALESCE(SUM(warehousestock_stock_quantity), 0) as stock_quantity FROM `jp_warehousestock` 
                         WHERE warehousestock_warehouseid = ".$warehouseId."
                         AND warehousestock_itemid =". $specification_id."
                         AND rate =" .$rate;
                  
            $stock_qty = Yii::app()->db->createCommand($sqlStock)->queryRow();
            $sql="SELECT m.material,m.quantity,m.amount,m.used_quantity,m.used_amount FROM `jp_material_estimation` m LEFT JOIN `jp_itemestimation` e ON e.itemestimation_id=m.estimation_id WHERE e.project_id=".$project." AND e.itemestimation_status=2  AND m.project_id=".$project ." AND m.material=".$material;
           
            //die($sql);
            $estimated_det = Yii::app()->db->createCommand($sql)->queryRow();
            
           
            $estimated_bal_quantity=$estimated_det["quantity"]-$estimated_det["used_quantity"];
        }
        $balance = "[Estimated Bal:".$estimated_bal_quantity.",Stock bal:".$stock_qty['stock_quantity']."]";
        echo json_encode(array(
                'response' => 'success',
                'msg' => $balance
            ));
            return;
    }

    public function actionCreateConsumption()
    {
        $model = new ConsumptionRequest;
        $modelitem = new PmsAccWprItemConsumed;

        if (isset($_POST['ConsumptionRequest'])) {
            // Save ConsumptionRequest data
            $model->attributes = $_POST['ConsumptionRequest'];
            $model->status = 0;
            $model->date = date('Y-m-d', strtotime($_POST['ConsumptionRequest']['date']));
            $model->item_added_by = Yii::app()->user->id; 
            $model->wpr_id = 0; 
            $model->type_from = 1;
            $model->created_at = new CDbExpression('NOW()'); 
            $model->updated_at = new CDbExpression('NOW()');

            if ($model->save()) {
                $consumptionId = $model->id;

                // items save
                if (isset($_POST['PmsAccWprItemConsumed']) && !empty($_POST['PmsAccWprItemConsumed']['materials'])) {
                    foreach ($_POST['PmsAccWprItemConsumed']['materials'] as $index => $item) {
                        $modelItem = new PmsAccWprItemConsumed;

                        $modelItem->consumption_id = $consumptionId;
                        $modelItem->coms_material_id = $item['material_id'];
                        $modelItem->item_id = $item['specification_id'];
                        $modelItem->item_rate = $item['item_rate'];
                        $modelItem->item_qty = $item['item_qty'];
                        $modelItem->item_amount = $item['item_amount'];
                        $modelItem->status=0;
                        $modelItem->wpr_id=0;
                        $modelItem->item_added_by = Yii::app()->user->id;
                        $modelItem->type_from=1;
                        $modelItem->created_date = new CDbExpression('NOW()');
                        if (!$modelItem->save()) {
                            Yii::app()->user->setFlash('error', 'An error occurred while saving the item: ' . print_r($modelItem->getErrors(), true));
                            break;
                        }
                        $pms_api_integration=ApiSettings::model()->pmsIntegrationStatus();
                        if($pms_api_integration==0){
                            $materialID=$modelItem->coms_material_id;
                        }
                        else{
                            $material = Materials::model()->find(
                                'id = :id',
                                [
                                    ':id' => $modelItem->coms_material_id,
                                ]
                            ); 
                            $materialID=$material->pms_material_id;
                        }
                        $estimation = MaterialEstimation::model()->find(
                            'project_id = :project_id AND material = :material AND specification = :specification',
                            [
                                ':project_id' => $model->coms_project_id,
                                ':material' => $materialID,
                                ':specification' => $modelItem->item_id,
                            ]
                        );
                       
                        if ($estimation !== null) {
                            $estimationAmount = $estimation->amount;
                            $modelItemAmount = $modelItem->item_rate * $modelItem->item_qty;
                            if ($estimationAmount < $modelItemAmount) {
                                Yii::app()->user->setFlash('error', 'The WPR amount exceeds the estimation.');
                                $this->redirect(array('warehouse/consumptionrequest'));
                                return;
                            }
                        }

                       
                    }
                }
               
               Yii::app()->user->setFlash('success', 'Consumption Request Added Successfully.');
                $this->redirect(array('warehouse/consumptionrequest'));
            }
        }

        // Render the create form
        $this->renderPartial('createconsumption', array(
            'model' => $model,
            'modelitem' => $modelitem,
        ));
    }
    public function actionUpdateConsumption($id)
    {
        $model = ConsumptionRequest::model()->findByPk($id);
        $modelitem = PmsAccWprItemConsumed::model()->findAllByAttributes(array('consumption_id' => $id));

        if (isset($_POST['ConsumptionRequest'])) {
            $model->attributes = $_POST['ConsumptionRequest'];
            $model->date = date('Y-m-d', strtotime($_POST['ConsumptionRequest']['date']));
            $model->item_added_by = Yii::app()->user->id; 
            $model->updated_at = new CDbExpression('NOW()');

            if ($model->save()) {
                if (isset($_POST['PmsAccWprItemConsumed']) && !empty($_POST['PmsAccWprItemConsumed']['materials'])) {
                    PmsAccWprItemConsumed::model()->deleteAllByAttributes(array('consumption_id' => $id));

                    foreach ($_POST['PmsAccWprItemConsumed']['materials'] as $index => $item) {
                        $modelItem = new PmsAccWprItemConsumed;
                        //pms rate save draft
                        if($model->type_from==2){
                            $type=2;
                            if(empty($item['item_rate'])){
                                $item['item_rate']=0;
                            }
                        }
                        else{
                            $type=1;
                        }
                        // new data
                        $modelItem->consumption_id = $id;
                        $modelItem->coms_material_id = $item['material_id'];
                        $modelItem->item_id = $item['specification_id'];
                        $modelItem->item_rate = $item['item_rate'];
                        $modelItem->item_qty = $item['item_qty'];
                        $modelItem->item_amount = $item['item_amount'];
                        $modelItem->status = 0;
                        $modelItem->wpr_id = 0;
                        $modelItem->item_added_by = Yii::app()->user->id;
                        $modelItem->type_from = $type;
                        $modelItem->created_date = new CDbExpression('NOW()');
                        if (!$modelItem->save()) {
                            Yii::app()->user->setFlash('error', 'An error occurred while saving the item: ' . print_r($modelItem->getErrors(), true));
                            break;
                        }
                    }
                }

                $this->redirect(array('warehouse/consumptionrequest'));
            }
        }
        $this->renderPartial('updateconsumption', array(
            'model' => $model,
            'modelitem' => $modelitem,
            'id' => $id,
        ));
    }

    public function actionViewConsumption($id) {
        $model = ConsumptionRequest::model()->findByPk($id);
        $modelItems = PmsAccWprItemConsumed::model()->findAllByAttributes(array('consumption_id' => $id));
        if ($model === null) {
            throw new CHttpException(404, 'The requested consumption request does not exist.');
        }
    
        // Pass data to view
        $this->render('consumption_view', array(
            'model' => $model,
            'modelItems' => $modelItems,
        ));
    }
    
    public function actionGetfieldsForEdit(){
        $consumptionId=$_GET['id'];
        $index = $_GET['index'];
        if($index==''){
            $index==0;
        }
        $consumptionRequest=ConsumptionRequest::model()->findByPk($consumptionId);
        $materails='';
        $project = $consumptionRequest['coms_project_id'];
        $material_arr=array();
        if(!empty($project)){
            $model_exist= Itemestimation::model()->find(array("condition" => "project_id = '$project'"));
            if(!empty($model_exist)){
                 $estimation_id =$model_exist->itemestimation_id;
            
                 $sql="SELECT m.material FROM `jp_material_estimation` m LEFT JOIN `jp_itemestimation` e ON e.itemestimation_id=m.estimation_id WHERE e.itemestimation_id=".$estimation_id ." AND e.itemestimation_status=2 AND  m.estimation_id=".$estimation_id ."  AND m.project_id=".$project;
                $materails = Yii::app()->db->createCommand($sql)->queryAll();
              
                 $material_arr=array();
                 if(!empty($materails)){
                    foreach($materails as $materail_item){
                        $items_det= Materials::Model()->findByPk($materail_item['material']);
                        //echo "<pre>";print_r($items_det);exit;
                        if(!empty($items_det)){
                            $material_arr[$items_det->id]=$items_det["material_name"];
                        }

                    }
                 }
                // echo "<pre>";print_r($material_arr);exit;
            }
           
        }
        $consumptionItems = PmsAccWprItemConsumed::model()->findAllByAttributes(array('consumption_id' => $consumptionId));
        echo $this->renderPartial('_consumption_item_edit', array(
            'consumptionItems' => $consumptionItems,
            'index'=>$index,
            'consumptionRequest'=>$consumptionRequest,
            'warehouse_id'=>$consumptionRequest->warehouse_id,
            'materials'=>$material_arr
        ));
    }

    public function actionRefreshButton(){
        $sql = "SELECT * FROM `pms_acc_wpr_item_consumed` WHERE `coms_project_id` IS NULL";
        $wpr_null_datas = Yii::app()->db->createCommand($sql)->queryAll();

            if(!empty($wpr_null_datas)){
                foreach($wpr_null_datas as $wpr_null_data){
                    
                    $pms_project_id_from_wpr = $wpr_null_data['pms_project_id'];

                    $acc_project_model = Projects::model()->findByAttributes(array('pms_project_id' => $pms_project_id_from_wpr));
                    
                    if ($acc_project_model !== null) {
                        $acc_project_id = $acc_project_model->pid;

                        $warehouses=Warehouse::model()->findByAttributes(array('project_id' =>$acc_project_id));
                        
                        if (count($warehouses) == 1) {
                           
                            $warehouseId=$warehouses->warehouse_id;
                            $update_sql = "UPDATE `pms_acc_wpr_item_consumed` SET  `warehouse_id` = :warehouse_id WHERE `id` = :id";
                            Yii::app()->db->createCommand($update_sql)
                                ->bindParam(':warehouse_id', $warehouseId)
                                ->bindParam(':id', $wpr_null_data['id'])
                                ->execute();
                        }
                        
                        $update_sql = "UPDATE `pms_acc_wpr_item_consumed` SET `coms_project_id` = :coms_project_id WHERE `id` = :id";
                        Yii::app()->db->createCommand($update_sql)
                            ->bindParam(':coms_project_id', $acc_project_id)
                            ->bindParam(':id', $wpr_null_data['id'])
                            ->execute();
                            }
                }
            }
        $sql = "SELECT * FROM `pms_acc_wpr_item_consumed` WHERE `coms_material_id` IS NULL";
        $wpr_material_datas = Yii::app()->db->createCommand($sql)->queryAll();
            
        if(!empty($wpr_material_datas)){
            foreach($wpr_material_datas as $wpr_material_data){
                
                $pms_material_id_from_wpr = $wpr_material_data['pms_material_id'];
                
                $acc_material_model = Materials::model()->findByAttributes(array('pms_material_id' => $pms_material_id_from_wpr));
                if ($acc_material_model !== null) {
                    $acc_material_id = $acc_material_model->id;
                    $acc_specification_id =$acc_material_model->specification;
                    
                    //specification update only 1 value
                    if (count($acc_specification_id) == 1) {
                        $spec=$acc_specification_id[0];

                        // $stockRecord=Warehousestock::model()->findAllByAttributes(array('warehousestock_warehouseid' => $warehouseId,'warehousestock_itemid'=>$spec));
                        // $rate=$stockRecord->rate;
                        $update_specification_sql = "UPDATE `pms_acc_wpr_item_consumed` SET `item_id` = :item_id WHERE `id` = :id";
                        Yii::app()->db->createCommand($update_specification_sql)
                            ->bindParam(':item_id',$spec)
                            // ->bindParam(':item_rate',$rate)
                            ->bindParam(':id', $wpr_material_data['id'])
                            ->execute();
                    }
                    
                    $update_sql = "UPDATE `pms_acc_wpr_item_consumed` SET `coms_material_id` = :coms_material_id WHERE `id` = :id";
                    Yii::app()->db->createCommand($update_sql)
                        ->bindParam(':coms_material_id', $acc_material_id)
                        ->bindParam(':id', $wpr_material_data['id'])
                        ->execute();
                        }
            }
        }
        
        $this->redirect(array('consumptionrequest'));
    }

    public function actionGetSpecifications() {
        $materialId = Yii::app()->request->getParam('material_id');
        $project_id = Yii::app()->request->getParam('project_id');
        //$materials = Materials::model()->findByPk($materialId);
        $sql="SELECT m.material,m.specification FROM `jp_material_estimation` m LEFT JOIN `jp_itemestimation` e ON e.itemestimation_id=m.estimation_id WHERE m.project_id=".$project_id ." AND e.itemestimation_status=2 AND  e.project_id=".$project_id ."  AND m.project_id=".$project_id." AND m.material=" .$materialId;
        //die($sql);
        $materials = Yii::app()->db->createCommand($sql)->queryAll();
        //echo "<pre>";print_r($materails);exit;
        $specData = [];
        $newQuery='';
        $catname='';
        $brandname='';
        if (!empty($materials)) {
            foreach($materials as $material){
                 
                $specificationIds = explode(',', $material["specification"]);
                foreach($specificationIds as $specification){
                    if(!empty($newQuery)){
                         $newQuery.=" OR ";
                    }
                   
                    $newQuery.=" FIND_IN_SET(id, '{$material["specification"]}')";
                }
               
                $specSql = "SELECT * FROM jp_specification WHERE (".$newQuery.")";
                
                //die($specSql);
                
                 $specificationss = Yii::app()->db->createCommand($specSql)->queryAll();
                 //echo "<pre>";print_r($specifications);exit;
                 if(!empty($specificationss)){
                     foreach ($specificationss as $spec) {
                        $cat = PurchaseCategory::model()->findByPk($spec['cat_id']);
                        if(!empty($cat)){
                            $catname=$cat->category_name;
                        }
                        $brand=Brand::Model()->findByPk($spec['brand_id']);
                        $brandname=$brand["brand_name"];
                        $specData[] = [
                            'id' => $spec['id'],
                            'specification' => $catname.'-'.$brandname.'-'.$spec["specification"],
                        ];
                    }
                 }
               //echo "<pre>";print_r($specData);exit;

            }
            
        }
        echo CJSON::encode([
            'status' => 'success',
            'specifications' => $specData,
        ]);
    }
    
    public function actionGetWarehouses() {
        $projectId = Yii::app()->request->getParam('projectId');
        $warehouses = Warehouse::model()->findAllByAttributes(array('project_id' => $projectId));
        $warehouseData = [];
        if ($warehouses) {
            foreach ($warehouses as $warehouse) {
                $warehouseData[] = [
                    'id' => $warehouse->warehouse_id,
                    'name' => $warehouse->warehouse_name,
                ];
            }
        }
        echo CJSON::encode([
            'status' => 'success',
            'warehouses' => $warehouseData,
        ]);
    }
    
    public function actionUpdateSpecificationAndWarehouse() {
        if (isset($_POST['specification_id']) && isset($_POST['warehouse_id'])) {
            $specificationId = $_POST['specification_id'];
            $warehouseId = $_POST['warehouse_id'];
            $wprId = $_POST['wpr_id'];
            $rate=$_POST['rate'];

            $updateSql = "UPDATE `pms_acc_wpr_item_consumed` SET `item_id` = :specification_id, `warehouse_id` = :warehouse_id, `item_rate` = :item_rate WHERE `id` = :id";
            Yii::app()->db->createCommand($updateSql)
                          ->bindParam(':specification_id', $specificationId)
                          ->bindParam(':warehouse_id', $warehouseId)
                          ->bindParam(':item_rate', $rate)
                          ->bindParam(':id', $wprId)
                          ->execute();
    
            echo CJSON::encode(['status' => 'success', 'message' => 'Specification and Warehouse updated successfully']);
        } else {
            echo CJSON::encode(['status' => 'error', 'message' => 'Invalid request']);
        }
    }
     
    public function actionGetRate() {
        $specificationId = Yii::app()->request->getPost('specification_id');
        $warehouseId = Yii::app()->request->getPost('warehouse_id');
        $wprId = Yii::app()->request->getPost('wpr_id');
        $stockRecords=Warehousestock::model()->findAllByAttributes(array('warehousestock_warehouseid' => $warehouseId,'warehousestock_itemid'=>$specificationId));
    
        $rateData = [];
        if (!empty($stockRecords)) {
            foreach ($stockRecords as $stock) {
                $rateData[] = [
                    'id' => $stock->warehousestock_id,
                    'rate' => $stock->rate,
                ];
            }
            echo CJSON::encode([
                'status' => 'success',
                'rates' => $rateData,
            ]);
        } else {
            echo CJSON::encode([
                'status' => 'error',
                'message' => 'Item not  linked with the material or Item Rate Not Found ',
            ]);
        }
    }
    
    
    public function actionapproveConsumedQuantity() {
        // Get consumption request data
        $sql = "SELECT * FROM `jp_consumption_request` WHERE id = :id";
        $data = Yii::app()->db->createCommand($sql)->queryRow(true, array(':id' => $_POST['id']));

        if (!$data) {
            echo json_encode(array(
                'response' => 'error',
                'msg' => 'Consumption request not found'
            ));
            return;
        }

        // Get child items for the consumption request
        $sqlChild = "SELECT * FROM `pms_acc_wpr_item_consumed` WHERE consumption_id = :consumption_id";
        $childItems = Yii::app()->db->createCommand($sqlChild)->queryAll(true, array(':consumption_id' => $_POST['id']));

        if (empty($childItems)) {
            echo json_encode(array(
                'response' => 'error',
                'msg' => 'No items found for this consumption request'
            ));
            return;
        }

        $project = $data["coms_project_id"];
        $warehouse_id = $data['warehouse_id'];
        $errors = []; 

        // First pass: Check conditions on all child items without updating anything
        foreach ($childItems as $child) {
            $material = $child["coms_material_id"];
            $material_det = Materials::model()->findByPk($material);
            if (!$material_det) {
                $errors[] = "Material with ID $material not found.";
                continue;
            }
            
            // Get current stock quantity for the material, item, and rate
            $sqlStock = "SELECT COALESCE(SUM(warehousestock_stock_quantity), 0) as stock_quantity FROM `jp_warehousestock` 
                        WHERE warehousestock_warehouseid = {$warehouse_id}
                        AND warehousestock_itemid = {$child['item_id']}
                        AND rate = {$child['item_rate']}";
            $stock_qty = Yii::app()->db->createCommand($sqlStock)->queryRow();

            // Get estimated details for the material from estimation tables
            $sql = "SELECT m.material, m.quantity, m.amount, m.used_quantity, m.used_amount 
                    FROM `jp_material_estimation` m 
                    LEFT JOIN `jp_itemestimation` e ON e.itemestimation_id = m.estimation_id 
                    WHERE e.project_id = {$project} 
                    AND e.itemestimation_status = 2  
                    AND m.project_id = {$project} 
                    AND m.material = {$material}";
            $estimated_det = Yii::app()->db->createCommand($sql)->queryRow();

            if (empty($estimated_det)) {
                $errors[] = "Estimation details not found or Estimation may  not be approved for Material " . $material_det['material_name'];
                continue;
            }
            
            // Check if the requested quantity exceeds the available estimated quantity
            $available_quantity = $estimated_det["quantity"] - $estimated_det["used_quantity"];
            if (intval($available_quantity) < intval($child['item_qty'])) {
                $errors[] = "Requested quantity ({$child['item_qty']}) exceeds available estimated quantity ({$available_quantity}) for Material " . $material_det['material_name'];
            }

            // Check if the requested amount exceeds the available estimated amount
            $available_amount = $estimated_det["amount"] - $estimated_det["used_amount"];
            if ($available_amount < $child['item_amount']) {
                $errors[] = "Requested amount ({$child['item_amount']}) exceeds available estimated amount ({$available_amount}) for Material " . $material_det['material_name'];
            }

            // Check if there is enough stock quantity
            if ($stock_qty['stock_quantity'] < $child['item_qty']) {
                $errors[] = "Requested quantity ({$child['item_qty']}) exceeds available stock ({$stock_qty['stock_quantity']}) for Material " . $material_det['material_name'];
            }
        }

        // If any errors were found, return them as a warning response
        if (!empty($errors)) {
            // Combine errors into one message string (or you can return an array)
            $msg = implode("<br/>", $errors);
            echo json_encode(array(
                'response' => 'warning',
                'msg' => $msg,
            ));
            return;
        }

        
        // Proceed with the approval process.
        $status = 1; // Approved status

        // Loop through each child item to update records
        foreach ($childItems as $child) {
            $material = $child["coms_material_id"];
            $project = $data["coms_project_id"];
            $material_det = Materials::model()->findByPk($material);
            
            // Update consumption request record
            $model = ConsumptionRequest::model()->findByPk($_POST['id']);
            if ($model) {
                $model->status = $status;
                $model->pms_wpr_status = $status;
                $model->item_added_by = Yii::app()->user->id;

                if (!$model->save()) {
                    echo json_encode([
                        'response' => 'error', 
                        'msg' => 'Failed to update consumption request for item ID: ' . $child['id']
                    ]);
                    return;
                }
            }

            // Update warehouse stock record for each child item
            $sqlStocks = "SELECT * FROM `jp_warehousestock` 
                        WHERE warehousestock_warehouseid = {$data['warehouse_id']}
                            AND warehousestock_itemid = {$child['item_id']}
                            AND rate = {$child['item_rate']}";
            $stock_qty_det = Yii::app()->db->createCommand($sqlStocks)->queryRow();

            if ($stock_qty_det) {
                $stockModel = Warehousestock::model()->findByPk($stock_qty_det["warehousestock_id"]);
                if ($stockModel) {
                    $initialQuantity = $stockModel->warehousestock_stock_quantity;
                    $finalQuantity = $initialQuantity - $child["item_qty"];
                    $stockModel->warehousestock_stock_quantity = $finalQuantity;

                    // Update consumed quantity
                    $consumed_quantity = $stockModel->consumed_quantity;
                    if (empty($consumed_quantity)) {
                        $consumed_quantity = 0;
                    }
                    $consumed_quantity += $child["item_qty"];
                    $stockModel->consumed_quantity = $consumed_quantity;

                    if (!$stockModel->save()) {
                        echo json_encode([
                            'response' => 'error', 
                            'msg' => 'Failed to update stock for Material ' . $material_det['material_name']
                        ]);
                        return;
                    }
                }
            }

            // Update the estimation record for the material
            $sql = "SELECT m.id, m.material, m.quantity, m.amount, m.used_quantity, m.used_amount 
                    FROM `jp_material_estimation` m 
                    LEFT JOIN `jp_itemestimation` e ON e.itemestimation_id = m.estimation_id 
                    WHERE e.project_id = {$project} 
                    AND e.itemestimation_status = 2  
                    AND m.project_id = {$project} 
                    AND m.material = {$material}";
            $estimated_det = Yii::app()->db->createCommand($sql)->queryRow();

            if ($estimated_det) {
                $mt_id = $estimated_det["id"];
                $mtmodel = MaterialEstimation::model()->findByPk($mt_id);
                if ($mtmodel) {
                    // Update the used quantity and amount
                    $mtmodel->used_quantity += $child["item_qty"];
                    $mtmodel->used_amount += $child['item_amount'];
                    if (!$mtmodel->save()) {
                        echo json_encode([
                            'response' => 'error', 
                            'msg' => 'Failed to update estimation for Material ' . $material_det['material_name']
                        ]);
                        return;
                    }
                }
            }

            // Optionally, send the approved status to PMS via API if enabled
            $pms_api_integration = 0;
            $pms_api_integration_model = ApiSettings::model()->findByPk(1);
            if ($pms_api_integration_model) {
                $pms_api_integration = $pms_api_integration_model->api_integration_settings;
            }
            if ($pms_api_integration == 1) {
                $request = [
                    'type' => 'material',
                ];
                $wpr_id = $data['wpr_id']; 
                $slug = "api/wpr-approve/" . $wpr_id;
                $method = "POST";
                GlobalApiCall::callApi($slug, $method, $request);
            }
        }

        // If everything has been updated successfully, return success response.
        echo json_encode(array(
            'response' => 'success',
            'msg' => 'All items approved successfully'
        ));
    }


    public function actionRejectConsumedQuantity() {
        if (isset($_POST['id']) && isset($_POST['remarks'])) {
            $id = $_POST['id'];
            $remarks = $_POST['remarks'];
            $consumptionRequest = ConsumptionRequest::model()->findByPk($id);
            if ($consumptionRequest !== null) {
                $consumptionRequest->remarks = $remarks;
                $consumptionRequest->status = 2;
                $consumptionRequest->pms_wpr_status = 1;
                if ($consumptionRequest->save()) {
                    //send Response to Api
                    $pms_api_integration=0;
                    $pms_api_integration_model=ApiSettings::model()->findByPk(1);
                    $pms_api_integration =$pms_api_integration_model->api_integration_settings;
                    if($pms_api_integration==1){
                        $request = [
                            'type' =>'material',
                        ];
                        $wpr_id = $consumptionRequest->wpr_id; 
                        $slug = "api/wpr-reject/" . $wpr_id;
                        $method = "POST";
                        $globalapi_response = GlobalApiCall::callApi($slug, $method, $request);
                
                        if (!$globalapi_response) {
                            echo json_encode(array(
                                'response' => 'error',
                                'msg' => 'Error in API call for item ID: ' . $child['id']
                            ));
                            return;
                        }
                    }
                    echo json_encode(['success' => true]);
                } else {
                    echo json_encode(['success' => false, 'message' => 'Failed to update record.']);
                }
            } else {
                echo json_encode(['success' => false, 'message' => 'Record not found.']);
            }
        } else {
            echo json_encode(['success' => false, 'message' => 'Invalid request.']);
        }
    
        Yii::app()->end();
    }
    
    
}
