<?php

/**
 * This is the model class for table "{{image_gallery}}".
 *
 * The followings are the available columns in table '{{image_gallery}}':
 * @property integer $id
 * @property integer $projectid
 * @property string $image
 *
 * The followings are the available model relations:
 * @property Projects $project
 */
class ImageGallery extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return '{{image_gallery}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('image, projectid', 'required'),
            array('projectid', 'numerical', 'integerOnly' => true),
            array('image', 'file',
                'types' => 'jpg,jpeg,gif,png',
                'maxSize' => 1024 * 1024 * 10, // 10MB
                'tooLarge' => 'The file was larger than 10MB. Please upload a smaller file.',
                'allowEmpty' => false
            ),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, projectid, image,created_by,created_date,updated_by,updated_date,albumid', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'createdBy' => array(self::BELONGS_TO, 'Users', 'created_by'),
            'project' => array(self::BELONGS_TO, 'Projects', 'projectid'),
            'album' => array(self::BELONGS_TO, 'Albums', 'albumid'),
            'updatedBy' => array(self::BELONGS_TO, 'Users', 'updated_by'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'projectid' => 'Project',
            'albumid' => 'Album',
            'image' => 'Image',
            'created_by' => 'Created By',
            'created_date' => 'Created Date',
            'updated_by' => 'Updated By',
            'updated_date' => 'Updated Date',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.
        $tblpx = Yii::app()->db->tablePrefix;

        $criteria = new CDbCriteria;
        $criteria->compare('id', $this->id);
        if (Yii::app()->user->role == 4) {
            $id = Yii::app()->user->id;
            $exist = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}users WHERE userid=" . $id)
                    ->queryRow();
            $clientid = $exist['client_id'];
            $projectid = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}projects WHERE client_id=" . $clientid)
                    ->queryAll();
            $arr = array();
            if (!empty($projectid)) {
                foreach ($projectid as $proj) {
                    array_push($arr, $proj['pid']);
                }
                $criteria->addInCondition('projectid', $arr, 'OR');
            }
        } else {
            $criteria->compare('projectid', $this->projectid);
            //$criteria->addCondition('projectid','projectid! = NULL');
        }
        
        $criteria->group = 'albumid';
        $criteria->compare('image', $this->image, true);
        $criteria->compare('albumid', $this->albumid, true);
        $criteria->compare('created_by', $this->created_by);
        $criteria->compare('created_date', $this->created_date, true);
        $criteria->compare('updated_by', $this->updated_by);
        $criteria->compare('updated_date', $this->updated_date, true);
        //print_r($criteria);die;
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return ImageGallery the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
