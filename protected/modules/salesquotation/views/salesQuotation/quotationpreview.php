<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/validation/jquery.validate.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/validation/additional-methods.js"></script>

<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/message/css/jquery.message.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/message/js/jquery.message.js"></script>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/qgenerator.css">
<div class="row">
                <div class="col-md-2">
                    <label>COMPANY : </label>
                    <div>
                        <?php
                        $company = Company::model()->findByPk($model->company_id);
                        echo $company['name'];
                        ?>
                    </div>
                </div>
                <div class="col-md-2">
                    <label>CLIENT : </label>
                    <div>
                        <?php echo $model->client_name; ?>
                    </div>
                </div>
                <div class="col-md-2">
                    <label>DATE : </label>
                    <div>
                        <?php echo isset($model->date_quotation) ? date('d-m-Y', strtotime($model->date_quotation)) : ''; ?>
                    </div>
                </div>
                <div class="col-md-2">
                    <label>Quotation No : </label>
                    <div>
                        <?php echo isset($model->invoice_no) ? $model->invoice_no : ''; ?>
                    </div>
                </div>
                <div class="col-md-2">
                    <label>Revision No : </label>
                    <div>
                        <?php echo "REV-".$rev_no ?>
                    </div>
                </div>
            </div>
            <br>
    <div id="table-scroll" class="table-scroll">                        
        <div id="table-wrap" class="table-wrap">
            <div class="table-responsive">
                <table cellpadding="10" class="table item_table w-100">
                    <thead>
                        <tr>
                            <th>S.NO</th>
                            <th>DESCRIPTION OF ITEM</th>
                            <?php
                            if ($model->template_type == '5') { ?>
                            <th>HSN CODE</th>
                            <?php } ?>
                            <th>UNIT</th>
                            <th>QTY</th>                                    
                            <th>MRP</th>
                            <th>AFTER DISCOUNT</th>
                            <th>TOTAL </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $new_rev = $rev_no;
                        $rev_no = "REV-".$rev_no."";
                        $sql = 'SELECT * FROM `jp_quotation_section` '
                                . 'WHERE `qtn_id` = ' . $qtid
                                . ' AND revision_no = "' . $rev_no . '"';
                        $sectionArray = Yii::app()->db->createCommand($sql)->queryAll();
                        $i = 1;
                        foreach ($sectionArray as $key => $value1) {
                            $sql = 'SELECT * FROM `jp_quotation_gen_category` '
                                    . 'WHERE `qid` = ' . $qtid . ' AND `section_id` = ' . $value1["id"]
                                    . ' AND revision_no = "' . $rev_no . '"';
                            $subsectionArray = Yii::app()->db->createCommand($sql)->queryAll();
                            $unit = Unit::model()->findByPk($value1['unit']);
                            ?>
                            <tr>
                                <?php if ($model->template_type == '5') { ?>
                                    <th colspan="<?php echo empty($subsectionArray) ? '2' : '7' ?>" style="background:#c00000;color:#fff;"><?php echo $value1['section_name'] ?></th>
                                <?php }else{ ?>
                                    <th colspan="<?php echo empty($subsectionArray) ? '2' : '7' ?>" style="background:#c00000;color:#fff;"><?php echo $value1['section_name'] ?></th>
                                <?php } ?>
                                <?php if (empty($subsectionArray)) { 
                                    if ($model->template_type == '5') { ?>
                                        <td style="background:#c00000;color:#fff;"><?php echo $value1['hsn_code'] ?></td>
                                    <?php } ?>
                                    <td style="background:#c00000;color:#fff;"><?php echo isset($unit) ? $unit->unit_name : "" ?></td>
                                    <td style="background:#c00000;color:#fff;"><?php echo ($model->template_type == '5') ? $value1['quantity']:$value1['quantity_nos']; ?></td>
                                    <td style="background:#c00000;color:#fff;"><?php echo $value1['mrp'] ?></td>
                                    <td style="background:#c00000;color:#fff;"><?php echo $value1['amount_after_discount'] ?></td>
                                    <td style="background:#c00000;color:#fff;"></td>
                                <?php } ?>
                            </tr>
                            <?php
                            foreach ($subsectionArray as $key => $value2) {
                                $sql = 'SELECT * FROM `jp_sales_quotation` WHERE `parent_type` = 0 '
                                        . ' AND `parent_id` = ' . $value2['id']
                                        . ' AND revision_no="' . $rev_no . '" AND `deleted_status` = 1';
                                $itemArray = Yii::app()->db->createCommand($sql)->queryAll();

                                $sql = 'SELECT * FROM `jp_quotation_gen_worktype` '
                                        . ' WHERE `qid` = ' . $value2["qid"]
                                        . ' AND `section_id` = ' . $value2["section_id"]
                                        . ' AND `category_label_id` = ' . $value2["id"]
                                        . ' AND revision_no = "' . $rev_no . '"';
                                $subitemArray = Yii::app()->db->createCommand($sql)->queryAll();
                                $unit = Unit::model()->findByPk($value2['unit']);
                                ?>
                                <tr>
                                    <th style="background:#c5d9f1;"><?php echo $i; ?></th>
                                    <th  colspan="<?php echo empty($subitemArray) ? '1' : '6'?>" style="background:#c5d9f1;"><?php echo $value2['category_label'] ?></th>
                                    <?php if (empty($subitemArray)) { 
                                        if ($model->template_type == '5')  { ?>
                                            <td style="background:#c5d9f1;"><?php echo $value2['hsn_code'] ?></td>
                                        <?php } ?>
                                        <td  style="background:#c5d9f1;"><?php echo isset($unit) ? $unit->unit_name : "" ?></td>
                                        <td  style="background:#c5d9f1;"><?php echo ($model->template_type == '5') ? $value2['quantity']:$value2['quantity_nos']; ?></td>
                                        <td  style="background:#c5d9f1;"><?php echo $value2['mrp'] ?></td>
                                        <td  style="background:#c5d9f1;"><?php echo $value2['amount_after_discount'] ?></td>
                                        <td  style="background:#c5d9f1;"></td> 
                                    <?php } ?>
                                </tr>
                                <?php
                                foreach ($itemArray as $key => $value) { ?>
                                    <tr>
                                        <td><?php echo '*' ?></td>
                                        <td colspan="7"><?php echo $value['worktype_label'] ?></td>
                                    </tr>
                                <?php } ?>
                                <?php
                                $letters = range('a', 'z');
                                $k = 0;
                                foreach ($subitemArray as $key => $value) {
                                    $sql = 'SELECT * FROM `jp_sales_quotation` '
                                            . ' WHERE `parent_type` = 1 AND `parent_id` = ' . $value['id']
                                            . ' AND revision_no="' . $rev_no . '" AND `deleted_status` = 1';
                                    $wrktypeitemArray = Yii::app()->db->createCommand($sql)->queryAll();
                                    $unit = Unit::model()->findByPk($value['unit']);
                                    ?>
                                    <tr class="td_group">
                                        <th><?php echo $letters[$k]; ?></th>
                                        <td colspan="<?php echo empty($wrktypeitemArray) ? '1' : '6'?>">
                                            <b><?php echo $value['worktype_label'] ?></b>
                                        </td>
                                        <?php if (empty($wrktypeitemArray)) {                                                             
                                            if ($model->template_type == '5') { ?>
                                                <td><?php echo $value['hsn_code'] ?></td>
                                            <?php } ?>
                                            <td><?php echo isset($unit) ? $unit->unit_name : "" ?></td>
                                            <td><?php echo ($model->template_type == '5')  ? $value['quantity']:$value['quantity_nos']; ?></td>
                                            <td><?php echo $value['mrp'] ?></td>
                                            <td><?php echo $value['amount_after_discount'] ?></td>
                                            <td></td>                                                   
                                        <?php } ?>
                                    </tr>
                                    <?php if ($value['description'] != "") { ?>
                                        <tr class="td_group">
                                            <th>*</th>
                                            <td colspan="5"><?php echo $value['description'] ?></td>
                                            <td></td>
                                        </tr>
                                    <?php }
                                    $sum = 0;
                                    foreach ($wrktypeitemArray as $key => $value) {
                                        $sum += $value['amount_after_discount'];
                                        $unit = Unit::model()->findByPk($value['unit']);
                                        $desc = ($value['description'] !="")?" - ".$value['description']:"";

                                        if($value['description']=="" && ($value['shutterwork_description'] !="" || $value['carcass_description']!="")){
                                            $desc = " - <b>Shutter:</b> ".$value['shutterwork_description']." , <b>Carcass:</b> ".$value['carcass_description'];
                                        }
                                        ?>
                                        <tr class="td_group">
                                            <th><?php echo '*' ?></th>
                                            <td><?php echo $value['worktype_label'].$desc ?></td>
                                            <?php
                                            $activeProjectTemplate = Controller::getActiveTemplate();
                                            if ($model->template_type == '5')  { ?>
                                                <td><?php echo $value['hsn_code'] ?></td>
                                            <?php } ?>
                                            <td><?php echo isset($unit) ? $unit->unit_name : "" ?></td>
                                            <td><?php echo ($model->template_type == '5')  ? $value['quantity']:$value['quantity_nos']; ?></td>
                                            <td><?php echo $value['mrp'] ?></td>
                                            <td><?php echo $value['amount_after_discount'] ?></td>
                                            <td></td>
                                        </tr>
                                    <?php }
                                    if (!empty($wrktypeitemArray)) {  } 
                                    $k++;
                                }
                                ?>
                                <?php
                                    $i++;
                                ?>
                                <tr>
                                    <td class="text-right" colspan="6">Total Cost for <b><?php echo $value2['category_label'] ?></b></td>
                                    <th class="text-right">
                                        <?php echo Controller::money_format_inr(Controller::getSubSectionSum($value2['id'], $qtid, $value2["section_id"], $rev_no), 2); ?>
                                    </th>
                                </tr>
                            <?php } ?>
                            <tr>
                                <td class="text-right"  colspan="<?php echo ($model->template_type == '5')? '7':'6' ?>"  style="background: #d6000a14;">Total Cost for <b><?php echo $value1['section_name'] ?></b></td>
                                <th class="text-right" style="background: #d6000a14;">
                                    <?php echo Controller::money_format_inr(Controller::getSectionSum($value1['id'], $qtid, $rev_no), 2); ?>
                                </th>
                            </tr>
                        <?php } ?>
                    </tbody>  
                    <tfoot>
                        <tr>
                            <th colspan="<?php echo ($model->template_type == '5')? '7':'6' ?>" class="text-right">MRP</th>
                            <th class="text-right">
                                <h5><?php
                                $rev_status = NULL;                                
                                if($status ==1){
                                    $intrRev = $interRev.$new_rev;
                                    $newRev = $rev_no;
                                    $revision_no = ("'$intrRev'" .','. "'$newRev'");
                                    $rev_status = 1;
                                    $total_amount = Controller::getMrpSum($qtid, $revision_no,$rev_status);
                                    
                                }else{
                                    $total_amount = Controller::getMrpSum($qtid, $rev_no,$rev_status);
                                }
                                
                                echo Controller::money_format_inr($total_amount, 2);
                                ?></h5>
                            </th>
                        </tr>
                        <tr>
                            <th colspan="<?php echo ($model->template_type == '5')? '7':'6' ?>" class="text-right">Amount After Discount</th>
                            <th class="text-right">
                                <h5><?php
                                $rev_status = NULL;
                                
                                if($status ==1){
                                    $intrRev = $interRev.$new_rev;
                                    $newRev = $rev_no;
                                    $revision_no = ("'$intrRev'" .','. "'$newRev'");
                                    $rev_status = 1;
                                    $total_amount = Controller::getQuotationSum($qtid, $revision_no,$rev_status);
                                    
                                }else{
                                    $total_amount = Controller::getQuotationSum($qtid, $rev_no,$rev_status);
                                }
                                
                                echo Controller::money_format_inr($total_amount, 2);
                                ?></h5>
                            </th>
                        </tr>
                        <tr>
                            <th colspan="<?php echo ($model->template_type == '5')? '7':'6' ?>" class="text-right">Amount After GST</th>
                            <th  class="text-right">
                                <h5><?php
                                $sql = 'SELECT * FROM jp_quotation_revision '
                                        . ' WHERE qid='.$qtid
                                        . ' AND revision_no="'.$rev_no.'"';
                                $revision_model = Yii::app()->db->createCommand($sql)->queryRow();
                                $total_tax = $revision_model['sgst_amount']+
                                $revision_model['cgst_amount']+
                                $revision_model['igst_amount'];
                                $total_amoun_withtax = $total_amount + $total_tax;
                                echo Controller::money_format_inr($total_amoun_withtax, 2);
                                ?></h5>
                            </th>
                        </tr>
                        <tr>
                            <th colspan="<?php echo ($model->template_type == '5')? '7':'6' ?>" class="text-right">Grand Total</th>
                            <th class="text-right">
                                <h5><?php
                                $total_after_discount = $total_amoun_withtax - $revision_model['discount_amount'];
                                echo Controller::money_format_inr($total_after_discount , 2);                                        
                                ?></h5>
                            </th>
                        </tr>
                        <tr>
                            <th colspan="<?php echo ($model->template_type == '5')? '7':'6' ?>" class="text-right">Amount after additional discount</th>
                            <th class="text-right">
                                <h5><?php
                                $addition_discount_amount = is_null($revision_model['total_after_additionl_discount']) ? $total_after_discount  :$revision_model['total_after_additionl_discount'];
                                echo Controller::money_format_inr($addition_discount_amount,2); 
                                ?></h5>
                            </th>
                        </tr>                                        
                    </tfoot>
                </table>
            </div>
        </div>
    </div>

