<?php
/* @var $this ExpenseTypeController */
/* @var $model ExpenseType */

$this->breadcrumbs=array(
	'Labour Types'=>array('index'),
	'Create',
);

?>

<div class="panel panel-gray">
<div class="panel-heading form-head">
        <h3 class="panel-title">Add Labour WorkType</h3>
    </div>
        <?php echo $this->renderPartial('_form', array('model' => $model)); ?>
</div>
