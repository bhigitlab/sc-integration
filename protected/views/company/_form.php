<?php
/* @var $this ProjectsController */
/* @var $model Projects */
/* @var $form CActiveForm */

?>

<div class="">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'company-form',
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
        'enableClientValidation' => true,
        'enableAjaxValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true,
            'validateOnType' => false,
        ),
    ));
    ?>
    <!-- Popup content-->
    <div class="custom-form-style">
        <div class="row">
            <div class="col-md-3 col-sm-4 form-group">
                <?php echo $form->labelEx($model, 'name'); ?>
                <?php echo $form->textField($model, 'name', array('size' => 60, 'maxlength' => 500, 'class' => 'form-control')); ?>
                <?php echo $form->error($model, 'name'); ?>
            </div>
            <div class="col-md-3 col-sm-4 form-group">
                <?php echo $form->labelEx($model, 'email_id'); ?>
                <?php echo $form->textField($model, 'email_id', array('size' => 60, 'maxlength' => 100, 'class' => 'form-control')); ?>
                <?php echo $form->error($model, 'email_id'); ?>
            </div>
            <div class="col-md-2 col-sm-4 form-group">
                <?php echo $form->labelEx($model, 'pincode'); ?>
                <?php echo $form->textField($model, 'pincode', array('size' => 30, 'maxlength' => 30, 'class' => 'form-control')); ?>
                <?php echo $form->error($model, 'pincode'); ?>
            </div>

            <div class="col-md-2 col-sm-4 form-group">
                <?php echo $form->labelEx($model, 'phone'); ?>
                <?php echo $form->textField($model, 'phone', array('size' => 30, 'maxlength' => 30, 'class' => 'form-control')); ?>
                <?php echo $form->error($model, 'phone'); ?>
            </div>

            <div class="col-md-2 col-sm-4 form-group">
                <?php echo $form->labelEx($model, 'company_gstnum'); ?>
                <?php echo $form->textField($model, 'company_gstnum', array('size' => 30, 'maxlength' => 50, 'class' => 'form-control')); ?>
                <?php echo $form->error($model, 'company_gstnum'); ?>
            </div>
            <div class="col-md-3 col-sm-4 form-group">
                <?php echo $form->labelEx($model, 'company_tolerance'); ?>
                <?php echo $form->textField($model, 'company_tolerance', array('size' => 60, 'class' => 'form-control')); ?>
                <?php echo $form->error($model, 'company_tolerance'); ?>
            </div>

            <div class="col-md-3 col-sm-4 form-group">
                <?php echo $form->labelEx($model, 'subcontractor_limit'); ?>
                <?php echo $form->textField($model, 'subcontractor_limit', array('size' => 60, 'class' => 'form-control')); ?>
                <?php echo $form->error($model, 'subcontractor_limit'); ?>
            </div>
            <div class="col-lg-2 col-md-3 col-sm-4 form-group ">
                <?php echo $form->labelEx($model, 'company_popermission'); ?><br>
                <?php echo $form->checkBox($model, 'company_popermission', array('class' => 'popermission', 'onchange' => 'valueChanged()')); ?>
                <?php echo $form->error($model, 'company_popermission'); ?>
            </div>
            <div class="col-lg-2 col-md-3 col-sm-4 form-group margin-bottom-26">
                <?php echo $form->labelEx($model, 'auto_purchaseno'); ?><br>
                <?php echo $form->checkBox($model, 'auto_purchaseno'); ?>
                <?php echo $form->error($model, 'auto_purchaseno'); ?>
            </div>
            <div class="col-md-2 col-sm-4 form-group">
                <?php
                if (!$model->isNewRecord) {
                    $class = " ";
                    if ($model->company_popermission == 1) {
                        $class = 'readonly';
                    } else {
                        $class = "";
                    }
                } else {
                    $class = "";
                }
                ?>
                <?php echo $form->labelEx($model, 'purchase_amount'); ?>
                <?php echo $form->textField($model, 'purchase_amount', array('size' => 60, 'class' => 'form-control purchase_amount', 'readonly' => $class)); ?>
                <?php echo $form->error($model, 'purchase_amount'); ?>
            </div>
            <div class="col-md-2 col-sm-4 form-group">
                <?php echo $form->labelEx($model, 'expense_calculation'); ?>
                <?php echo $form->dropDownList(
                    $model,
                    'expense_calculation',
                    array(
                        0 => 'Purchase Bill Only',
                        1 => 'Consumption Only',
                    ),
                    array(
                        'empty' => '-- Select --',  // Placeholder value
                        'class' => 'form-control', // Bootstrap styling
                    )
                ); ?>
                <?php echo $form->error($model, 'expense_calculation'); ?>
            </div>

            <div class="col-lg-3 col-md-4 col-sm-6 form-group">
                <?php // echo $form->labelEx($model,'expenses_email'); ?>
                <label>Expenses Email <small class="font-weight-normal">(Separate values with a comma)</small> </label>
                <?php echo $form->textField($model, 'expenses_email', array('size' => 60, 'class' => 'form-control')); ?>
                <?php echo $form->error($model, 'expenses_email'); ?>
            </div>

            <div class="col-lg-3 col-md-4 col-sm-6 form-group">
                <?php // echo $form->labelEx($model,'expenses_percentage'); ?>
                <label>Expenses % <small class="font-weight-normal">(Separate values with a comma)</small></label>
                <?php echo $form->textField($model, 'expenses_percentage', array('size' => 60, 'class' => 'form-control')); ?>
                <?php echo $form->error($model, 'expenses_percentage'); ?>
            </div>

        </div>
        <div class="row">
            <div class="col-lg-3 col-md-4 col-sm-6">
                <?php echo $form->labelEx($model, 'subco_email_userid'); ?>
                <ul class="checkboxList">
                    <?php
                    $type_list = CHtml::listData(Users::model()->findAll(), 'userid', 'first_name');
                    $typelist = Users::model()->findAll(array());
                    $assigned_company_array = array();
                    if (!$model->isNewRecord) {
                        $assigned_types = Company::model()->find(array('condition' => 'id=' . $model->id));
                        $assigned_types_array = explode(",", $assigned_types->subco_email_userid);
                    } else {
                        $assigned_types_array = '';
                    }
                    echo CHtml::checkBoxList('Company[subco_email_userid]', $assigned_types_array, CHtml::listData($typelist, 'userid', 'first_name'), array('checkAll' => 'Check all', 'template' => '<li class="checkboxtype">{input}{label}</li>', 'separator' => ''));
                    ?>
                </ul>
                <?php echo $form->error($model, 'subco_email_userid'); ?>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6">
                <?php echo $form->labelEx($model, 'po_email_userid'); ?>
                <ul class="checkboxList">
                    <?php
                    $type_list = CHtml::listData(Users::model()->findAll(), 'userid', 'first_name');
                    $typelist = Users::model()->findAll(array());
                    $assigned_company_array = array();
                    if (!$model->isNewRecord) {
                        $assigned_types = Company::model()->find(array('condition' => 'id=' . $model->id));
                        $assigned_types_array = explode(",", $assigned_types->po_email_userid);
                    } else {
                        $assigned_types_array = '';
                    }
                    echo CHtml::checkBoxList('Company[po_email_userid]', $assigned_types_array, CHtml::listData($typelist, 'userid', 'first_name'), array('checkAll' => 'Check all', 'template' => '<li class="checkboxtype">{input}{label}</li>', 'separator' => ''));
                    ?>
                </ul>
                <?php echo $form->error($model, 'po_email_userid'); ?>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6">
                <?php echo $form->labelEx($model, 'invoice_email_userid'); ?>
                <ul class="checkboxList">
                    <?php
                    $type_list = CHtml::listData(Users::model()->findAll(), 'userid', 'first_name');
                    $typelist = Users::model()->findAll(array());
                    $assigned_company_array = array();
                    if (!$model->isNewRecord) {
                        $assigned_types = Company::model()->find(array('condition' => 'id=' . $model->id));
                        $assigned_types_array = explode(",", $assigned_types->invoice_email_userid);
                    } else {
                        $assigned_types_array = '';
                    }
                    echo CHtml::checkBoxList('Company[invoice_email_userid]', $assigned_types_array, CHtml::listData($typelist, 'userid', 'first_name'), array('checkAll' => 'Check all', 'template' => '<li class="checkboxtype">{input}{label}</li>', 'separator' => ''));
                    ?>
                </ul>
                <?php echo $form->error($model, 'invoice_email_userid'); ?>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6 textArea-box form-group">
                <?php echo $form->labelEx($model, 'description'); ?>
                <?php echo $form->textArea($model, 'description', array('rows' => 4, 'cols' => 50, 'class' => 'form-control')); ?>
                <?php echo $form->error($model, 'description'); ?>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6 textArea-box form-group">
                <?php echo $form->labelEx($model, 'address'); ?>
                <?php echo $form->textArea($model, 'address', array('rows' => 4, 'cols' => 50, 'class' => 'form-control')); ?>
                <?php echo $form->error($model, 'address'); ?>
            </div>

            <div class="col-lg-3 col-md-4 col-sm-6 textArea-box form-group">
                <?php echo $form->labelEx($model, 'bank_details'); ?>
                <?php echo $form->textArea($model, 'bank_details', array('rows' => 4, 'cols' => 50, 'class' => 'form-control')); ?>
                <?php echo $form->error($model, 'bank_details'); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3  col-sm-4 form-group">
                <?php echo $form->labelEx($model, 'company_updateduration'); ?>
                <?php echo $form->textField($model, 'company_updateduration', array('size' => 60, 'class' => 'form-control')); ?>
                <?php echo $form->error($model, 'company_updateduration'); ?>
            </div>
            <div class="col-md-3  col-sm-4 form-group">
                <?php echo $form->labelEx($model, 'purchaseorder_limit'); ?>
                <?php echo $form->textField($model, 'purchaseorder_limit', array('class' => 'form-control')); ?>
                <?php echo $form->error($model, 'purchaseorder_limit'); ?>
            </div>
            <input type="hidden" id="bank_dummy_data" value="">
            <div class="col-md-3  col-sm-4 form-group">
                <?php
                //echo CHtml::hiddenField('Company[id]' , $qtid, array('id' => 'qtid'));            
                echo $form->labelEx($model, 'logo');
                echo $form->fileField($model, 'logo', array('id' => 'file'));
                if ($model->hasErrors('logo') && isset($_POST['Company'])) {
                    echo $form->error($model, 'logo');
                }
                ?>
            </div>
            <div class="col-md-3  col-sm-4 form-group" id="exist-image-view"
                display="<?php ($model->logo != '') ? 'none' : 'block'; ?>">
                <label>Existing image</label>
                <img id="existing-image"
                    src="<?php echo Yii::app()->request->baseUrl . "/uploads/image/" . $model->logo; ?>"
                    style="max-height: 45px;" alt="Existing Image">
                <button id="remove-image-button" type="button">Remove Image</button>
                <input type="hidden" id="image_remove" value="">
                <input type="hidden" id="company_logo" name="Company[logo]" value="<?php echo $model->logo; ?>">
            </div>
        </div>
        <div class="row">

            <?php
            $invoice_readonly = 'readonly';
            if ((isset(Yii::app()->user->role) && (in_array('/invoice/format', Yii::app()->user->menuauthlist)))) {
                $invoice_readonly = '';
            }
            $purchase_readonly = 'readonly';
            if ((isset(Yii::app()->user->role) && (in_array('/purchase/format', Yii::app()->user->menuauthlist)))) {
                $purchase_readonly = '';
            }
            $sc_readonly = 'readonly';
            $sc_billreadonly = 'disabled';
            if ((isset(Yii::app()->user->role) && (in_array('/subcontractor/format', Yii::app()->user->menuauthlist)))) {
                $sc_readonly = '';
                $sc_billreadonly = '';
            }
            $warehouse_readonly = 'readonly';
            if ((isset(Yii::app()->user->role) && (in_array('/wh/warehouse/format', Yii::app()->user->menuauthlist)))) {
                $warehouse_readonly = '';
            }
            $mr_purchase_readonly = 'readonly';
            if ((isset(Yii::app()->user->role) && (in_array('/purchase/mrformat', Yii::app()->user->menuauthlist)))) {
                $mr_purchase_readonly = '';
            }
            ?>
            <div class="col-md-6 textArea-box form-group">
                <label>Invoice Format (Separate the values with slash )</label>
                <?php echo $form->textField($model, 'invoice_format', array('size' => 60, 'placeholder' => 'shortkeyword/{invoice_sequence}/{financial_year}', 'class' => 'form-control', 'readonly' => $invoice_readonly)); ?>
                <?php echo CHtml::tag('div', array('class' => 'hint'), 'NOTE: [ Eg:shortkeyword/{invoice_sequence}/{financial_year} ]. Use {invoice_sequence} for sequence no and {financial_year} for financial year and {year} for current year along with slash(/)'); ?>
                <?php echo $form->error($model, 'invoice_format'); ?>
            </div>

            <div class="col-md-6 textArea-box form-group">
                <label>Subcontractor Quotation Format (Separate the values with slash )</label>
                <?php echo $form->textField($model, 'sc_quotation_format', array('size' => 60, 'placeholder' => 'shortkeyword/{year}/{scq_sequence}', 'class' => 'form-control', 'readonly' => $sc_readonly)); ?>
                <?php echo CHtml::tag('div', array('class' => 'hint'), 'NOTE: [ Eg:shortkeyword/{year}/{scq_sequence} ]. Use {scq_sequence} for sequence no and {year} for  year along with slash(/)'); ?>
                <?php echo $form->error($model, 'sc_quotation_format'); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 textArea-box form-group">
                <label>Purchase Order Format (Separate the values with slash )</label>
                <?php echo $form->textField($model, 'purchase_order_format', array('size' => 60, 'placeholder' => 'shortkeyword/{year}/{po_sequence}', 'class' => 'form-control', 'readonly' => $purchase_readonly)); ?>
                <?php echo CHtml::tag('div', array('class' => 'hint'), 'NOTE: [ Eg:shortkeyword/{year}/{po_sequence} ]. Use {po_sequence} for sequence no and {year} for year along with slash(/)'); ?>
                <?php echo $form->error($model, 'purchase_order_format'); ?>
            </div>
            <div class="col-md-6 textArea-box form-group">
                <?php echo $form->labelEx($model, 'sc_bill_format'); ?>
                <?php echo $form->checkBox($model, 'sc_bill_format', array('disabled' => $sc_billreadonly)) . " Tick to use same above format for subcontractor bill -[shortkeyword/{year}/{scq_sequence}/scq_bill_sequence]"; ?>
                <?php echo $form->error($model, 'sc_bill_format'); ?>
            </div>

        </div>
        <div class="row">
            <div class="col-md-6 textArea-box form-group">
                <label>Material Requisition Format (Separate the values with slash )</label>
                <?php echo $form->textField($model, 'material_requisition_fomat', array('size' => 60, 'placeholder' => 'shortkeyword/{year}/{mr_sequence}', 'class' => 'form-control', 'readonly' => $mr_purchase_readonly)); ?>
                <?php echo CHtml::tag('div', array('class' => 'hint'), 'NOTE: [ Eg:shortkeyword/{year}/{mr_sequence} ]. Use {mr_sequence} for sequence no and {year} for  year along with slash(/)'); ?>
                <?php echo $form->error($model, 'material_requisition_fomat'); ?>

            </div>
            <div class="col-md-6 textArea-box form-group">
                <label>Warehouse Receipt Format (Separate the values with slash )</label>
                <?php echo $form->textField($model, 'warehouse_receipt_format', array('size' => 60, 'placeholder' => 'shortkeyword/{year}/{grn_sequence}', 'class' => 'form-control', 'readonly' => $warehouse_readonly)); ?>
                <?php echo CHtml::tag('div', array('class' => 'hint'), 'NOTE: [ Eg:shortkeyword/{year}/{grn_sequence} ]. Use {grn_sequence} for sequence no and {year} for  year along with slash(/)'); ?>
                <?php echo $form->error($model, 'warehouse_receipt_format'); ?>

            </div>

        </div>
        <div class="row">
            <div class="form-group col-xs-12 text-right">
                <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-info createcomp_btn')); ?>
                <?php if (!$model->isNewRecord) {
                    echo CHtml::ResetButton('Close', array('onclick' => 'closeaction(this,event);', 'class' => 'btn btn-other'));
                } else {
                    echo CHtml::ResetButton('Reset', array('style' => 'margin-right:3px;', 'class' => 'btn btn-default btn-sm'));
                    echo CHtml::ResetButton('Close', array('onclick' => 'closeaction(this,event);', 'class' => 'btn btn-other'));
                }
                ?>
            </div>
        </div>
    </div>
    <?php $this->endWidget(); ?>
</div><!-- form -->

<script>
    document.getElementById('remove-image-button').addEventListener('click', function () {
        // Clear the file input value and hide the existing image and the remove button
        document.getElementById('file').value = '';
        document.getElementById('existing-image').style.display = 'none';
        this.style.display = 'none';
        document.getElementById('image_remove').value = 1;
        document.getElementById('company_logo').value = '';
    });
    document.getElementById('file').addEventListener('change', function () {
        var input = this;
        var output = document.getElementById('existing-image');
        var removeButton = document.getElementById('remove-image-button');

        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                output.setAttribute('src', e.target.result);
                output.style.display = 'inline-block';
                removeButton.style.display = 'inline-block';
            };

            reader.readAsDataURL(input.files[0]);
        } else {
            output.style.display = 'none';
            removeButton.style.display = 'none';
        }
    });
    $(document).ready(function () {
        $("#Company_company_updateduration").keydown(function (e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13]) !== -1 ||
                // Allow: Ctrl+A, Command+A
                (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                // Allow: home, end, left, right, down, up
                (e.keyCode >= 35 && e.keyCode <= 40)) {
                // let it happen, don't do anything
                return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });
        //select all checkboxes
        $("#select_all").change(function () {  //"select all" change
            var status = this.checked; // "select all" checked status
            $('.checkbox').each(function () { //iterate all listed checkbox items
                this.checked = status; //change ".checkbox" checked status
            });
        });

        $('.checkbox').change(function () { //".checkbox" change
            //uncheck "select all", if one of the listed checkbox item is unchecked
            if (this.checked == false) { //if this item is unchecked
                $("#select_all")[0].checked = false; //change "select all" checked status to false
            }

            //check "select all" if all checkbox items are checked
            if ($('.checkbox:checked').length == $('.checkbox').length) {
                $("#select_all")[0].checked = true; //change "select all" checked status to true
            }
        });


        $("#select_alltype").change(function () {  //"select all" change
            var status = this.checked; // "select all" checked status
            $('.checkboxtype').each(function () { //iterate all listed checkbox items
                this.checked = status; //change ".checkbox" checked status
            });
        });


        $('.checkboxtype').change(function () { //".checkbox" change
            //uncheck "select all", if one of the listed checkbox item is unchecked
            if (this.checked == false) { //if this item is unchecked
                $("#select_alltype")[0].checked = false; //change "select all" checked status to false
            }

            //check "select all" if all checkbox items are checked
            if ($('.checkboxtype:checked').length == $('.checkboxtype').length) {
                $("#select_alltype")[0].checked = true; //change "select all" checked status to true
            }
        });


    });

    function valueChanged() {
        if ($('.popermission').is(":checked")) {
            $('.purchase_amount').attr('readonly', true);
        } else {
            $('.purchase_amount').attr('readonly', false);
        }
    }

    $('.createcomp_btn').click(function () {
        $("html, body").animate({ scrollTop: 0 }, "slow");
    });

    function countOfLetters(str) {
        var letter = 0;
        for (i = 0; i < str.length; i++) {
            if ((str[i] >= 'A' && str[i] <= 'Z')
                || (str[i] >= 'a' && str[i] <= 'z'))
                letter++;
        }

        return letter;
    }

    $("#Company_name").keyup(function () {
        var alphabetCount = countOfLetters(this.value);
        var message = "";
        var disabled = false;

        if (alphabetCount < 1) {
            var message = "Invalid Company Name";
            var disabled = true;
        }

        $(this).siblings(".errorMessage").show().html(message).addClass('d-block');
        $(".btn-info").attr('disabled', disabled);

    });
    $(document).ready(function () {
        $("#Company_bank_details").keypress(function () {
            var key = window.event.keyCode;
            var bankval = '';
            var bank_dummy_data = '';
            if (key == 13) {
                bankval = $(this).val();
                document.getElementById("Company_bank_details").value = document.getElementById("Company_bank_details").value + ".";
                // alert(document.getElementById("Company_bank_details").value);
                return true;
            } else {
                return true;
            }
        });
    });



</script>