<?php
$user = Users::model()->findByPk(Yii::app()->user->id);
$arrVal = explode(',', $user->company_id);
$newQuery = "";
$newQuery1 = "";
foreach ($arrVal as $arr) {
    if ($newQuery)
        $newQuery .= ' OR';
    if ($newQuery1)
        $newQuery1 .= ' OR';
    $newQuery .= " FIND_IN_SET('" . $arr . "', id)";
    $newQuery1 .= " FIND_IN_SET('" . $arr . "', company_id)";
}
?>
<script>
    $(function () {
        $("#date_from").datepicker({
            dateFormat: 'yy-mm-dd',
            maxDate: $("#date_to").val()
        });
        $("#date_to").datepicker({
            dateFormat: 'yy-mm-dd',
        });
    });

</script>
<div class="page_filter  custom-form-style">
    <?php $form = $this->beginWidget('CActiveForm', array(
        'action' => Yii::app()->createUrl($this->route),
        'method' => 'POST',
    )); ?>
    <div class="row">
        <?php
        $tblpx = Yii::app()->db->tablePrefix;
        $projects = $model = Yii::app()->db->createCommand("SELECT pid,name FROM {$tblpx}projects WHERE (" . $newQuery1 . ") order by name")->queryAll();
        ?>
        <div class="form-group  col-xs-12 col-sm-3 col-md-2">
            <label for="project">Project</label>
            <select name="Bills[project]" class="form-control">
                <option value="">Select Project</option>
                <?php foreach ($projects as $pro) { ?>
                    <option value="<?= $pro['pid'] ?>" <?php if (isset($pro_id) && $pro['pid'] == $pro_id) { ?> Selected<?php } ?>>
                        <?= $pro['name'] ?>
                    </option>
                <?php } ?>
            </select>
        </div>
        <?php
        $vendors = $model = Yii::app()->db->createCommand("SELECT vendor_id,name FROM {$tblpx}vendors WHERE (" . $newQuery1 . ") order by name")->queryAll();
        ?>
        <div class="form-group  col-xs-12 col-sm-3 col-md-2">
            <label for="vendor">Vendor</label>
            <select name="Bills[vendor]" class="form-control">
                <option value="">Select Vendor</option>
                <?php foreach ($vendors as $ven) { ?>
                    <option value="<?= $ven['vendor_id'] ?>" <?php if (isset($vendor_id) && $ven['vendor_id'] == $vendor_id) { ?> Selected<?php } ?>>
                        <?= $ven['name'] ?>
                    </option>
                <?php } ?>
            </select>
        </div>
        <div class="form-group  col-xs-12 col-sm-3 col-md-2">
            <label for="company">Company</label>
            <?php
            echo CHtml::dropDownList('company_id', 'company_id', CHtml::listData(Company::model()->findAll(array(
                'select' => array('id, name'),
                'order' => 'id DESC',
                'condition' => '(' . $newQuery . ')',
                'distinct' => true
            )), 'id', 'name'), array('empty' => 'Select Company', 'id' => 'company_id', 'class' => 'form-control', 'style' => 'padding: 2.5px 0px;', 'options' => array($company_id => array('selected' => true))));
            ?>
        </div>
        <div class="form-group  col-xs-12 col-sm-3 col-md-2">
            <label for="pur">Purchase Number</label>
            <?php echo CHtml::textField('Bills[pu_num]', (($pu_num != '') ? $pu_num : ''), array('placeholder' => 'Purchase Number', 'class' => 'form-control')); ?>
        </div>

        <div class="form-group  col-xs-12 col-sm-3 col-md-2">
            <label for="">Bill Number</label>
            <?php echo CHtml::textField('Bills[bill_num]', (($bill_num != '') ? $bill_num : ''), array('placeholder' => 'Bill Number', 'class' => 'form-control')); ?>
        </div>
        <div class="form-group  col-xs-12 col-sm-3 col-md-2">
            <label for="spe">Specification</label>
            <select name="category_id[]" id="category_id" class="form-control js-example-basic-multiple category_id"
                placeholder="select Item spec">
                <option value="">Select Item specification</option>
                <?php
                foreach ($specification as $key => $value) {
                    ?>
                    <option value="<?php echo $value['id']; ?>">
                        <?php echo $value['data']; ?>
                    </option>
                    <?php
                }
                ?>
            </select>
        </div>

        <div class="form-group  col-xs-12 col-sm-3 col-md-2">
            <label for="exp">Expense Head</label>
            <select name="expense_head[]" id="expense_head" class="form-control js-example-basic-multiple expense_head">
                <option value="">Select Expense Head</option>
                <?php
                foreach ($expense_type as $key => $value) {
                    ?>
                    <option value="<?php echo $value['type_id']; ?>">
                        <?php echo $value['type_name']; ?>
                    </option>
                    <?php
                }
                ?>
            </select>
        </div>
        <div class="form-group  col-xs-12 col-sm-3 col-md-2 margin-bottom-16">
            <label for="">Days Interval</label>
            <?php
            echo CHtml::textField('Bills[day_interval]', (($day_interval != '') ? $day_interval : ''), array("id" => "day_interval",  "class" => "form-control", "placeholder" => 'Days Interval', 'autocomplete' => 'off'));
            ?>
        </div>
        <div class="form-group col-xs-12 col-sm-3 col-md-2">
            <label for="">Select Range</label>
            <?php
            echo CHtml::dropDownList(
                'Bills[day_range]',
                (($day_range != '') ? $day_range : 'Select Range'),
                array(
                    '1' => '1 to 15',
                    '2' => '15 to 30',
                    '3' => '30 to 45',
                    '4' => 'above 45',
                ),
                array('empty' => 'Select Range', "class" => "form-control")
            );
            ?>
        </div>
        <div class="form-group col-xs-12 col-sm-3 col-md-2">
            <label>From</label>
            <?php
            echo CHtml::textField('Bills[date_from]', (($date_from != '') ? date('Y-m-d', strtotime($date_from)) : ''), array("id" => "date_from", "class" => "form-control", "readonly" => true, "placeholder" => 'Date From', 'autocomplete' => 'off'));
            ?>
        </div>
        <div class="form-group  col-xs-12 col-sm-3 col-md-2">
            <label>To</label>
            <?php echo CHtml::textField('Bills[date_to]', (($date_to != '') ? date('Y-m-d', strtotime($date_to)) : ''), array("id" => "date_to", "class" => "form-control", "readonly" => true, "placeholder" => 'Date To', 'autocomplete' => 'off')); ?>
        </div>

        <div class="form-group  col-xs-12 col-sm-3 col-md-2 text-right">
            <label>&nbsp;</label>
            <div>
                <?php
                echo CHtml::submitButton('Go', array(
                    'class' => 'btn btn-sm btn-primary'
                ));
                ?>
                <?php
                echo CHtml::resetButton('Clear', array(
                    'onclick' => 'javascript:location.href="' . $this->createUrl('ageingreport') . '"',
                    'class' => 'btn btn-sm btn-default'
                ));
                ?>
            </div>

        </div>
    </div>
    <?php $this->endWidget(); ?>
</div>
<style>
    select,
    input {
        margin-right: 5px;
    }

    .topgap {
        margin-top: 10px;
    }

    .select2-container--default .select2-selection--multiple {
        border: 1px solid #ccc;
    }

    .filter_elem {
        margin-right: 0px;
    }

    input::placeholder,
    select::placeholder {
        color: #555 !important;
    }
</style>
<script>
    $(function () {
        $('#category_id').select2({
            width: '100%',
            multiple: true,
            placeholder: 'Select Item Specification',
        });
        $('#expense_head').select2({
            width: '100%',
            multiple: true,
            placeholder: 'Select Expense Head',
        });
    });
</script>