<?php
/* @var $this ExpenseTypeController */
/* @var $model ExpenseType */

$this->breadcrumbs=array(
	'Labour Types'=>array('index'),
	$model->type_id=>array('view','id'=>$model->type_id),
	'Update',
);
?>
<div class="panel panel-gray">
    <div class="panel-heading form-head">
        <h3 class="panel-title">Update Labour Types</h3>
    </div>
    <?php echo $this->renderPartial('_form', array('model' => $model)); ?>
</div>
