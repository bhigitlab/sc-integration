<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.min.js"></script>

<?php
/* @var $this PmsAccWprItemConsumedController */
/* @var $model PmsAccWprItemConsumed */

$this->breadcrumbs=array(
	'Consumption Request'=>array('index'),
	'Create',
);

?>

<div>
    <?php echo $this->renderPartial('_consumptionform', array('modelitem'=>$modelitem,'model'=>$model)); ?>
</div>
