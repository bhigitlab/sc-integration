<?php
/* @var $this ExpenseTypeController */
/* @var $model ExpenseType */
/* @var $form CActiveForm */
?>
<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'expense-type-form',
    'enableAjaxValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
        'validateOnChange' => true,
        'validateOnType' => false,
    ),
));
?>
<div class="row ">
    <div class="form-group col-xs-12 col-md-3">
        <?php echo $form->labelEx($model, 'type_name'); ?>
        <?php echo $form->textField($model, 'type_name', array('size' => 30, 'maxlength' => 30, 'class' => 'form-control')); ?>
        <?php echo $form->error($model, 'type_name'); ?>
    </div>
    <div class="form-group col-xs-12 col-md-3">
        <?php echo $form->labelEx($model, 'expense_type'); ?>
        <?php echo $form->dropDownList($model, 'expense_type', CHtml::listData(Status::model()->findAll(array('order' => 'sid desc', 'condition' => 'status_type= "expense_head_type"')), 'sid', 'caption'), array('empty' => '--', 'class' => 'form-control', )); ?>
        <?php echo $form->error($model, 'expense_type'); ?>
    </div>
    <div class="form-group col-xs-12 col-md-5">
        <?php echo $form->labelEx($model, 'expense_category'); ?>
        <div class="form-group">
            <?php echo $form->radioButton($model, 'expense_category', array('value' => 1, 'uncheckValue' => null)); ?>
            Direct Expense
            <?php echo $form->radioButton($model, 'expense_category', array('value' => 2, 'uncheckValue' => null, 'class' => 'ml-2')); ?>
            Indirect Expense
            <?php echo $form->error($model, 'expense_category'); ?>
        </div>
    </div>

</div>


<div class="row">
    <div class="form-group col-xs-12 text-right">
        <label style="display:block;">&nbsp;</label>
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-primary btn-sm submit')); ?>
        <?php
        if (!$model->isNewRecord) {
            echo CHtml::ResetButton('Close', array('onclick' => 'closeaction(this,event);', 'class' => 'btn btn-default btn btn-sm'));
        } else {
            echo CHtml::ResetButton('Reset', array('style' => 'margin-right:3px;', 'class' => 'btn btn-other btn-sm'));
            echo CHtml::ResetButton('Close', array('onclick' => 'closeaction(this,event);', 'class' => 'btn btn-default btn btn-sm'));
        }
        ?>
    </div>
</div>

<!-- form -->

<?php $this->endWidget(); ?>




<script>
    $(document).ready(function () {
        var type_val = $('#ExpenseType_template_type').val();
        templateValues(type_val);
        $("#select_all").change(function () {
            var status = this.checked;
            $('.checkbox').each(function () {
                this.checked = status;
            });
        });

        $('.checkbox').change(function () {

            if (this.checked == false) {
                $("#select_all")[0].checked = false;
            }

            if ($('.checkbox:checked').length == $('.checkbox').length) {
                $("#select_all")[0].checked = true;
            }
        });

        $('#ExpenseType_template_type').change(function () {
            var type_val = $(this).val();
            templateValues(type_val);
        });

        function templateValues(type_val) {
            $('.section_class').addClass('hide');
            if (type_val === '1') {
                $('.labour_label,.wage_label,.wage_rate_label').removeClass('hide');
            } else if (type_val === '2') {
                $('.labour_label,.wage_rate_label,.helper_label,.helper_labour_label,.labour_wage_label,.helper_wage_label').removeClass('hide');
            } else if (type_val === '3') {
                $('.labour_label,.wage_rate_label,.helper_label,.wage_label').removeClass('hide');
            } else if (type_val === '4') {
                $('.lump_sum_label').removeClass('hide');
            } else {
                $('.section_class').addClass('hide');
            }
        }
    });
</script>

<script>
    $('.text_field[type="text"]').change(function () {
        if ($(this).val() != '') {
            $(this).closest('.section_class').find('input[type=checkbox]').prop('checked', true);
        }
    }).change()

    $('input[type="checkbox"]').change(function () {
        if (!this.checked) {
            $(this).closest('.section_class').find('input[type=text]').val('');
        }
    }).change()

    $("#ExpenseType_expense_type").change(function () {
        var type_val = $(this).val();
        if (type_val == 96) {
            $('#ExpenseType_template_type').attr('disabled', true);
            $('#ExpenseType_template_type').siblings('.errorMessage').hide();
            $('#ExpenseType_template_type').siblings('label').removeClass('required');
            $('#ExpenseType_template_type').parent().removeClass('error');
        } else {
            $('#ExpenseType_template_type').attr('disabled', false);
        }
    })

    function countOfLetters(str) {
        var letter = 0;
        for (i = 0; i < str.length; i++) {
            if ((str[i] >= 'A' && str[i] <= 'Z')
                || (str[i] >= 'a' && str[i] <= 'z'))
                letter++;
        }

        return letter;
    }


    $("#ExpenseType_type_name").keyup(function () {
        var alphabetCount = countOfLetters(this.value);
        var message = "";
        var disabled = false;

        if (alphabetCount < 1) {
            var message = "Invalid Type Name";
            var disabled = true;
        }

        $(this).siblings(".errorMessage").show().html(message).addClass('d-block');
        $(".submit").attr('disabled', disabled);
    });

</script>


<style>
    .popermission {
        margin-top: 30px !important;
    }
</style>