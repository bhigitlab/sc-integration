<?php
/* @var $this ExpensesController */
/* @var $model Expenses */

$this->breadcrumbs = array(
    'Expenses' => array('index'),
    'Create',
);

/* $this->menu=array(
    array('label'=>'List Expenses', 'url'=>array('index')),
    array('label'=>'Manage Expenses', 'url'=>array('admin')),
); */
?>
<div class="container" id="expense">
    <div class="expenses-heading">
        <div class="clearfix">
            <h2>DUPLICATE DAILY EXPENSE ENTRIES</h2>
        </div>
    </div>
    <div class="popwindow" style="display:none;">
        <div id="details">

        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="table-responsive">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>Sl No.</th>
                            <th>Company</th>
                            <th>Bill Number</th>
                            <th>Transaction Head</th>
                            <th>Transaction Type</th>
                            <th>Bank</th>
                            <th>Cheque Number</th>
                            <th>Description</th>
                            <th>Amount</th>
                            <th>Tax</th>
                            <th>Total</th>
                            <th>Receipt</th>
                            <th>Paid</th>
                            <th>Payment Type</th>
                            <th>Paid To</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i = 1;
                        foreach ($duplicateentry as $entry) {
                            ?>
                            <tr class="duplicaterow" id="duplicaterow<?php echo $i; ?>" data-id="<?php echo $i; ?>"
                                title="Click here to view <?php echo $entry["rowcount"]; ?> similar entries.">
                                <td>
                                    <?php echo $i; ?>
                                </td>
                                <td>
                                    <?php
                                    $company = Company::model()->findByPk($entry["company_id"]);
                                    if ($company)
                                        echo $company->name;
                                    ?>
                                </td>
                                <td>
                                    <?php echo $entry["bill_id"]; ?>
                                </td>
                                <td>
                                    <?php
                                    if ($entry["dailyexpense_type"] == 'expense') {
                                        $expense = ExpenseType::model()->findByPk($entry["expensehead_id"]);
                                        if ($expense)
                                            echo "Expense - " . $expense->type_name;
                                    } else if ($entry["dailyexpense_type"] == 'receipt') {
                                        $receipt = Companyexpensetype::model()->findByPk($entry["dailyexpense_receipt_head"]);
                                        if ($receipt)
                                            echo "Receipt - " . $receipt->name;
                                    } else if ($entry["dailyexpense_type"] == 'deposit') {
                                        $deposit = Companyexpensetype::model()->findByPk($entry["exp_type_id"]);
                                        if ($deposit)
                                            echo "Deposit - " . $deposit->deposit_name;
                                    }
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    if ($entry["dailyexpense_type"] == 'receipt')
                                        $exptype = Status::model()->findByPk($entry["dailyexpense_receipt_type"]);
                                    else
                                        $exptype = Status::model()->findByPk($entry["expense_type"]);
                                    if ($exptype)
                                        echo $exptype->caption;
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    $bank = Bank::model()->findByPk($entry["bank_id"]);
                                    if ($bank)
                                        echo $bank->bank_name;
                                    ?>
                                </td>
                                <td>
                                    <?php echo $entry["dailyexpense_chequeno"]; ?>
                                </td>
                                <td>
                                    <?php echo $entry["description"]; ?>
                                </td>
                                <td class="text-right">
                                    <?php echo Controller::money_format_inr($entry["dailyexpense_amount"], 2); ?>
                                </td>
                                <td class="text-right">
                                    <?php echo Controller::money_format_inr($entry["dailyexpense_sgst"] + $entry["dailyexpense_cgst"] + $entry["dailyexpense_igst"], 2); ?>
                                </td>
                                <td class="text-right">
                                    <?php echo Controller::money_format_inr($entry["amount"], 2); ?>
                                </td>
                                <td class="text-right">
                                    <?php echo Controller::money_format_inr($entry["dailyexpense_receipt"], 2); ?>
                                </td>
                                <td class="text-right">
                                    <?php echo Controller::money_format_inr($entry["dailyexpense_paidamount"], 2); ?>
                                </td>
                                <td>
                                    <?php
                                    if ($entry["dailyexpense_purchase_type"] == 1)
                                        echo "Credit";
                                    else if ($entry["dailyexpense_purchase_type"] == 2)
                                        echo "Full Paid";
                                    else if ($entry["dailyexpense_purchase_type"] == 3)
                                        echo "Partially Paid";
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    $employee = Users::model()->findByPk($entry["employee_id"]);
                                    if ($employee)
                                        echo $employee->first_name . " " . $employee->last_name;
                                    ?>
                                </td>
                                <input type="hidden" name="amount[]" id="amount<?php echo $i; ?>"
                                    value="<?php echo $entry["amount"]; ?>" />
                                <input type="hidden" name="paid[]" id="paid<?php echo $i; ?>"
                                    value="<?php echo $entry["dailyexpense_paidamount"]; ?>" />
                                <input type="hidden" name="date[]" id="date<?php echo $i; ?>"
                                    value="<?php echo $entry["date"]; ?>" />
                            </tr>
                            <?php
                            $i++;
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<style>
    .popwindow {
        position: fixed;
        top: 94px;
        left: 70px;
        right: 70px;
        z-index: 2;
        max-width: 1150px;
        margin: 0 auto;
    }

    #details {
        background-color: #fff;
        border-color: #fff;
        box-shadow: 0px 0px 8px 6px rgba(0, 0, 0, 0.25);
        color: #555;
        padding: 10px;
        border-radius: 4px;
    }
</style>
<script>
    $(document).ready(function () {
        $(".duplicaterow").click(function (e) {
            $(".popwindow").show();
            var row = $(this).attr("data-id");
            var amount = $("#amount" + row).val();
            var paid = $("#paid" + row).val();
            var date = $("#date" + row).val();
            $.ajax({
                type: "GET",
                data: { amount: amount, paid: paid, date: date },
                url: "<?php echo $this->createUrl('dailyexpense/getduplicateentries') ?>",
                success: function (data) {
                    $("#details").html(data);
                }
            });
        });
        $(document).on('click', '#details', function (e) {
            $(".popwindow").hide();
            $("#details").html("");
        });
    });
</script>