<!-- <div class="panel panel-default"> -->
        <div class="row">
            <div class="col-xs-12">
                <div class="heading-title">Payment Stages</div>
                <div class="dotted-line"></div>
            </div>
        </div>
        <div class="panel-body append_div payment-stage-new">            
            <div class="row"> 
                <form class="stage-form" id="payment-stage" method="POST">               
                    <div class="form-group col-xs-12 col-md-2">
                        <label>Payment Stage:</label>
                        <input type="hidden" class="stage_id"/>
                        <input type="hidden" name="Scquotation[scquotation_id]" id="Scquotation_scquotation_id" value="<?php echo $scquotation_model->scquotation_id; ?>">
                        <input type="text" class="payment_stage form-control"/>
                    </div>
                    <div class="form-group col-xs-12 col-md-2">
                        <label>Percentage:</label>
                        <input type="hidden" class="selected_percent" value="0">
                        <input type="number" class="percentage form-control"/>
                        <input type ="hidden" class="floated_percentage" value="0" />
                        <div class="error"></div>
                    </div>
                    <div class="form-group col-xs-12 col-md-2">
                        <label>Amount:</label>
                        <input type="number" class="stage_amount form-control" />
                        <div class="error"></div>
                    </div>
                    <div class="form-group col-xs-12 col-md-2">
						<label>Sl Number:</label>
						<input type="text" class="serial_number form-control" value="<?php echo $serial_number; ?>" />
						<div class="error"></div>
					</div>
                    <div class="col-md-2 more_sec">
                        <label class="d-block">&nbsp;</label>
                        <button type="button" class="btn btn-primary btn-sm add_more_stage"><i class="fa fa-plus"></i></button>						
                        <button type="button" class="btn btn-danger btn-sm remove_stage"><i class="fa fa-minus"></i></button>                    
                    </div>
                </form>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-xs-12 text-right">
                <button class="btn btn-primary btn-sm addstage" type="button">Save</button>
                <button class="btn btn-default btn-sm" type="reset" id="resetBtn">Reset</button>
            </div>
        </div>
    <!-- </div> -->
    <div class="row">
    <div class="form-group col-xs-12 new-listing-table">
        <table class="table margin-top-15">
            <thead class="entry-table">
                <tr>
                    <th>Sl Number</th>
                    <th>Stage</th>
                    <th>Stage Percentage</th>
                    <th>Amount</th>
                    <th>Bill Status</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $payment_stage = Yii::app()->db->createCommand("SELECT * FROM `jp_sc_payment_stage` WHERE `quotation_id` = ".$scquotation_model->scquotation_id." order by `serial_number` ASC")->queryAll();
                $payment_stage_amount = 0;
                $quotation_amount =$scquotation_model->getTotalQuotationAmount($scquotation_model->scquotation_id) ;
                foreach ($payment_stage as $key => $value) {
                                    
                ?>
                <?php
                $bgcolor = "";
                
                if($value['delete_approve_status']==1){ 
                    $bgcolor = "style=background:#ff000036";
                }
                ?>
                <tr <?php echo $bgcolor ?>>
                    <td><?php echo $value['serial_number'];?></td>
                    <td><?php echo $value['payment_stage'];?></td>
                    <td><?php echo number_format($value['stage_percent'],2,'.','');?></td>
                    <td>
                        <?php 
                        $quotation_amount =$scquotation_model->getTotalQuotationAmount($scquotation_model->scquotation_id) ;
                        if($value['stage_amount'] == '0.00'){
                            $stage_amount = Controller::money_format_inr($quotation_amount * ($value['stage_percent'] / 100),2);
                    
                        }else{
                            $stage_amount = Controller::money_format_inr($value['stage_amount'], 2) ;
                        }
                        echo  $stage_amount;
                        ?></td>
                    <td><?php echo ($value['bill_status']==0)?'Pending':'Bill Created';?></td>
                    <td>                		
                        <span class="icon icon-options-vertical popover-test" data-toggle="popover" data-placement="left" type="button" data-html="true" style="cursor: pointer;"></span>
                        <div class="popover-content hide">
                            <ul class="tooltip-hiden">   
                                <li>
                                <?php if($value['bill_status'] == '0'){ ?>
                                <a href="#" class="btn btn-xs btn-default editStage" data-id="<?php echo $value['id'] ?>" data-qid="<?php echo $value['quotation_id'] ?>">Edit</a>
                                <?php if($value['delete_approve_status']==0){ ?>
                                </li>                 
                                <li>
                                <a href="javascript:void(0);" onclick="redirectToBill(<?php echo $value['quotation_id']; ?>, <?php echo $value['id']; ?>, '<?php echo $scquotation_model['approve_status']; ?>')" class="btn btn-xs btn-default convert_bill">Convert to Bill</a>
                                </li>
                                <li>
                                <a href="#" class="btn btn-xs btn-default deleteStage" data-id="<?php echo $value['id'] ?>" data-qid="<?php echo $value['quotation_id'] ?>">Delete</a>
                                
                                	
                                </li>
                                <?php } ?> 

                               <?php } ?>                         
                            </ul>
                        </div>	
                                
                    </td>
                </tr>
                
                <?php } ?>
            
            </tbody>
        </table> 
    </div>
    </div>