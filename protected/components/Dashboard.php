<?php

class Dashboard extends CController {
    public function __construct($id, $module = null) {
        // Call the parent constructor with necessary arguments
        parent::__construct($id, $module);
    }
    public function getDashboardData() {
        // Fetch accounts payable data
        $payableData = $this->getPayableDashboard();
        // Fetch accounts receivable data
        $receivableData = $this->getPaymentReceivable();
    
        // Merge the arrays
        $dashboardData = array_merge($payableData, $receivableData);
        
        return $dashboardData;
    }

    public function getPayableDashboard() {
        $bill_list = array();
        $bills = "";
        $condition = "";
        $command = Yii::app()->db->createCommand()->select('b.bill_id as billId');
        $command->from($this->tableNameAcc('expenses', 0) . ' as e');
        $command->leftJoin($this->tableNameAcc('bills', 0) . ' as b', 'e.bill_id = b.bill_id');
        $command->where("e.type = 73 AND b.bill_totalamount IS NOT NULL AND b.purchase_id IS NOT NULL");
        $billsArray = $command->queryAll();
    
        if (!empty($billsArray)) {
            foreach ($billsArray as $entry) {
                if ($entry['billId'] != "") {
                    $bill_list[] = $entry['billId'];
                }
            }
            $bills = implode(',', $bill_list);
            $condition = "AND b.bill_id NOT IN ($bills)";
        }
    
        $sql = "(SELECT b.bill_id as bill_id, b.bill_number as bill_number, b.bill_date as bill_date, 
                    v.name as name, v.payment_date_range as payment_date_range, 'vendor' as type 
                 FROM jp_bills b 
                 LEFT JOIN jp_purchase p ON b.purchase_id = p.p_id 
                 LEFT JOIN jp_vendors v ON v.vendor_id = p.vendor_id 
                 WHERE 1=1 $condition) 
                 UNION ALL 
                 (SELECT sb.id AS bill_id, sb.bill_number AS bill_number, sb.date AS bill_date, 
                         s.subcontractor_name AS name, NULL AS payment_date_range, 'subcontractor' AS type 
                  FROM jp_subcontractorbill sb 
                  LEFT JOIN jp_subcontractor_payment sp ON sp.sc_bill_id = sb.id 
                  LEFT JOIN jp_scquotation sq ON sq.scquotation_id = sb.scquotation_id 
                  LEFT JOIN jp_subcontractor s ON s.subcontractor_id = sq.subcontractor_id 
                  GROUP BY sb.id, s.subcontractor_name 
                  HAVING COALESCE(SUM(sp.amount), 0) = 0 OR COALESCE(SUM(sp.amount), 0) < MAX(sb.total_amount))";
    
        $aging_array = Yii::app()->db->createCommand($sql)->queryAll();
    
        // Initialize totals
        $currentMonthTotal = 0;
        $previousMonthTotal = 0;
    
        // Get current and previous month range
        $currentMonthStart = date('Y-m-01');
        $currentMonthEnd = date('Y-m-t');
        $previousMonthStart = date('Y-m-01', strtotime('first day of last month'));
        $previousMonthEnd = date('Y-m-t', strtotime('last day of last month'));
    
        foreach ($aging_array as $aging) {
            $bill = Bills::model()->find('bill_id=:bill_id', array(':bill_id' => $aging['bill_id']));
    
            if ($bill !== null) {
                $billDate = $bill->bill_date; // Assuming `bill_date` is stored in YYYY-MM-DD format
                $billAmount = $bill->bill_amount;
    
                // Check if the bill belongs to the current month
                if ($billDate >= $currentMonthStart && $billDate <= $currentMonthEnd) {
                    $currentMonthTotal += $billAmount;
                }
                // Check if the bill belongs to the previous month
                elseif ($billDate >= $previousMonthStart && $billDate <= $previousMonthEnd) {
                    $previousMonthTotal += $billAmount;
                }
            }
        }
    
        // Prepare the data to be returned
        $dataValues = [
            [
                'name' => 'Accounts Payable',
                'current' => $currentMonthTotal,
                'previous' => $previousMonthTotal
            ]
        ];

        return $dataValues;
    }

    public function getPaymentReceivable(){
        $inv_list = array();
        $invoice = "";
        $condition = "";
        $command = Yii::app()->db->createCommand()->select('i.invoice_id as invoiceId');
        $command->from($this->tableNameAcc('expenses', 0) . ' as e');
        $command->leftJoin($this->tableNameAcc('invoice', 0) . ' as i', 'e.invoice_id = i.invoice_id ');
        $command->where("e.type = 72 AND i.subtotal IS NOT NULL");
        $invArray = $command->queryAll();

        if (!empty($invArray)) {
            foreach ($invArray as $entry) {
                if ($entry['invoiceId'] != "") {
                    $inv_list[] = $entry['invoiceId'];
                }
            }
            $invoice = implode(',', $inv_list);
            $condition = "AND i.invoice_id NOT IN ($invoice)";
        }
        $sql = "SELECT i.inv_no,i.date,"
            . " p.name "
            . " FROM jp_invoice i LEFT JOIN jp_projects p "
            . " ON i.project_id = p.pid "        
            . "WHERE 1=1 $condition";
        
            
        $aging_array = Yii::app()->db->createCommand($sql)->queryAll();
        
         // Initialize totals
         $currentMonthTotal = 0;
         $previousMonthTotal = 0;
    
         // Get current and previous month range
         $currentMonthStart = date('Y-m-01');
         $currentMonthEnd = date('Y-m-t');
         $previousMonthStart = date('Y-m-01', strtotime('first day of last month'));
         $previousMonthEnd = date('Y-m-t', strtotime('last day of last month'));
         
        foreach ($aging_array as $aging) {
            $invoice = Invoice::model()->find('inv_no=:inv_no', array(':inv_no' => $aging['inv_no']));
            
            if ($invoice !== null) {
                $invoiceDate = $invoice->date; // Assuming `bill_date` is stored in YYYY-MM-DD format
                $invoiceAmount = $invoice->amount;
                // Check if the bill belongs to the current month
                if ($invoiceDate >= $currentMonthStart && $invoiceDate <= $currentMonthEnd) {
                    $currentMonthTotal += $invoiceAmount;
                }
                // Check if the bill belongs to the previous month
                elseif ($invoiceDate >= $previousMonthStart && $invoiceDate <= $previousMonthEnd) {
                    $previousMonthTotal += $invoiceAmount;
                }
            }
        }
    
        // Prepare the data to be returned
        $dataValues = [
            [
                'name' => 'Accounts Receivable',
                'current' => $currentMonthTotal,
                'previous' => $previousMonthTotal
            ]
        ];
        return $dataValues;
    }

}