<?php
Yii::app()->clientScript->registerScript(
    'myHideEffect',
    '$(".info").animate({opacity: 1.0}, 2000).fadeOut("slow");',
    CClientScript::POS_READY
);
?>
<style>
 .image_btn{
    padding-left:12px;
 }
 img.img-fluid {
    max-width: 100px;
 }
 .txt-center{
    text-align:center;
 }
.image-text {
   
    text-align: center; /* Center text horizontally */
   
}
</style>


<div class="container" id="project">
    <div class="clearfix">
        <h2>Media Settings</h2>
    </div>
    <?php if (Yii::app()->user->hasFlash('success')) : ?>
        <div class="info alert alert-success alert-dismissible" role="alert" id="success-alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <?php echo Yii::app()->user->getFlash('success'); ?>
        </div>
    <?php endif; ?>
    <div class="container">
    <div class="left-side">
    <?php echo $this->renderPartial('_mediauploadfile');?>  
    </div>
 
    <div class="col-md-12" style="margin-bottom:10px; ">
            <button  id="showGalleryButton" style="float:left;" class="btn btn-primary">Show Media Gallery</button>
    </div>
    <!-- Modal for the image gallery -->
    <div id="imageGalleryModal" class="modal">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" style="margin-top:5px;">Image Gallery </h5><span>[Use these image keywords inside pdf layouts]
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                         </button>
                    </div>
                    <div class="modal-body">
                    <div id="imageRow">
                            <!-- Images will be dynamically loaded here -->
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

   
    
</div>
<script>
    function displayGallery() {
    $.ajax({
        type: "GET",
        url: "<?php echo $this->createUrl('ProjectTemplate/getGalleryMedia') ?>", // Replace this with the URL of your PHP script
        success: function(data) {
            // Assuming your PHP script returns JSON data containing image file names
            var images = JSON.parse(data);

            // Initialize HTML variable for image gallery
            var html = '<div class="row" style="margin-bottom:10px;">'; // Start the first row
            var count = 0; // Initialize image count

            // Loop through each image file in the data
            for (var i = 0; i < images.length; i++) {
                var imageName = "{image:: " + images[i] + "||width:'250px;'}";
                var imageSrc = "<?php echo Yii::app()->request->baseUrl; ?>/uploads/company_assets/media/" + images[i];
                html += '<div class="col-md-4 txt-center">';
                html += '<img src="' + imageSrc + '" alt="' + imageName + '" class="img-fluid" data-file="'+ imageName +' " title="click image to copy keyword">';
                html += '<p><b>'+imageName+'</b></p>';
                html += '</div>';

                // Increment image count
                count++;

                // Start a new row after every third image
                if (count % 3 == 0) {
                    html += '</div>'; // Close the previous row
                    html += '<div class="row">'; // Start a new row
                }
            }

            // Close the last row
            html += '</div>';

            // Append the generated HTML to the #imageRow element
            $("#imageRow").html(html);
        }
    });
}

$(document).ready(function() {
    $("#showGalleryButton").click(function() {
    // Call the PHP function to display images
    displayGallery(); 

    // Show the modal
        $("#imageGalleryModal").modal("show");
    });
});

    $(document).ready(function(){
       $("#cancel_btn").click(function(){
            $("#showGalleryButton").show();
            $("#media_gallery_box").hide();
           

       }) ;
    });
   
</script>
