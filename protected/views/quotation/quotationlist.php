<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$this->breadcrumbs = array(
    'performa invoice',
)
    ?>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<!--<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/tableHeadFixer.js"></script>-->
<div class="container" id="project">
    <div class="expenses-heading">
        <div class="header-container">
            <h3>Quotation</h3>
            <?php if ((isset(Yii::app()->user->role) && (in_array('/quotation/addquotation', Yii::app()->user->menuauthlist)))) { ?>
                <button href="index.php?r=quotation/addquotation" type="button"
                    class="btn btn-info" id="button1">Add Quotation</button>
            <?php } ?>
        </div>
    </div>
    <div id="msg_box"></div>
    <?php $this->renderPartial('_search', array('model' => $model)) ?>
    <?php if (Yii::app()->user->hasFlash('success')): ?>
        <div class="info"
            style="width: 500px;margin-left: 400px;padding: 8px;color: green;font-size: 20px;font-weight: bold;">
            <?php echo Yii::app()->user->getFlash('success'); ?>
        </div>
    <?php endif; ?>


    <?php if (Yii::app()->user->hasFlash('error')): ?>
        <div class="info"
            style="width: 500px;margin-left: 400px;padding: 8px;color: red;font-size: 20px;font-weight: bold;">
            <?php echo Yii::app()->user->getFlash('error'); ?>
        </div>
    <?php endif; ?>

    <div class="row">
        <div class='col-md-12'>
            <?php
            $dataProvider->sort->defaultOrder = 'quotation_id DESC';
            $this->widget('zii.widgets.CListView', array(
                'dataProvider' => $dataProvider,
                'itemView' => '_view',
                /*'enableSorting'=>1,
                'sortableAttributes'=>array(
                    'bill_id' => 'Bill Id',
                    'bill_number'=>'Bill Number',
                    'bill_date' => 'Bill Date',
                    'created_date' => 'Created Date'
                ), */
                'template' => '<div class="table-wrapper margin-top-20"><div class="dataTables_wrapper no-footer"><div class="new-listing-table1 table-responsive"><table cellpadding="10" class="table" id="qtntable">{items}</table></div></div></div>',
                'emptyText' => '<br><table cellpadding="10" class="table"><thead>
                    <tr>
					<th>SI No</th>
					<th>Project</th>
                    <th>Quotation No</th>
                    <th>Client</th>
                    <th>Date</th>
                    <th>Sub Total</th>
                    <th>Tax Total</th>
                    <th>Total Amount </th>
                    <th></th>
                </tr></thead><tr><td colspan="9" class="text-center">No results found</td></tr></table>',
            )); ?>
        </div>
    </div>
</div>

<style>
    a.button {
        background-color: #6a8ec7;
        display: block;
        padding: 5px;
        color: #fff;
        cursor: pointer;
        float: right;
        border: 1px solid #6a8ec8;
    }

    .tooltip-hiden {
        width: auto;
    }

    /*#parent{max-height:300px;}*/
    table thead th {
        background-color: #eee;
    }
</style>


<script>
    $(document).ready(function () {
        //$("#qtntable").tableHeadFixer({'left' : false, 'foot' : false, 'head' : true});
        $(".popover-test").popover({
            html: true,
            content: function () {
                return $(this).next('.popover-content').html();
            }
        });
        $('[data-toggle=popover]').on('click', function (e) {
            $('[data-toggle=popover]').not(this).popover('hide');
        });
        $('body').on('hidden.bs.popover', function (e) {
            $(e.target).data("bs.popover").inState.click = false;
        });
        $('body').on('click', function (e) {
            $('[data-toggle=popover]').each(function () {
                if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                    $(this).popover('hide');
                }
            });
        });
        $(document).ajaxComplete(function () {
            $(".popover-test").popover({
                html: true,
                content: function () {
                    return $(this).next('.popover-content').html();
                }
            });
            $("#qtntable").tableHeadFixer({ 'left': false, 'foot': false, 'head': true });

        });
    });


</script>
<?php
Yii::app()->clientScript->registerScript('myjavascript', '
            $(document).ready(function(){
                $("#qtntable").dataTable( {
                   
                    "scrollCollapse": true,
                   // "paging": false,
                     "columnDefs"        : [
                        {
                            "searchable"    : false,
                            "targets"       : [0] 
                        },
                        { "bSortable": false, "aTargets": [-1]  }
                    ],

                } );

            });

            ');
?>



<script>
    $(document).on('click', '.deletequotation', function (e) {

        var element = $(this);
        var quotation_id = $(this).attr('data-id');
        var userConfirmation = confirm("Are you sure you want to delete this entry?");
        if (userConfirmation) {
            $.ajax({
                url: '<?php echo Yii::app()->createAbsoluteUrl('quotation/deletequotation'); ?>',
                type: 'POST',
                dataType: 'json',
                data: { id: quotation_id },
                success: function (response) {
                    $("#msg_box").html('<div class="alert alert-' + response.response + ' alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>' + response.msg + '</div>');
                    setTimeout(function () {
                        window.location.reload(1);
                    }, 1000);

                }
            });

        }

    });
    $(document).ready(function () {
        $("#button1").click(function () {
            let hrefloc = $(this).attr("href");
            window.location.href = hrefloc;

        });

    });
</script>