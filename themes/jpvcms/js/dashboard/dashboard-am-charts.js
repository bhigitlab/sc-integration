// START OF TOP ARE GRAPH

am5.ready(function () {
  function createChart(chartId, chartData) {
    // Create root element
    var root = am5.Root.new(chartId);

    // Set themes
    root.setThemes([am5themes_Animated.new(root)]);

    // Create chart
    var chart = root.container.children.push(
      am5xy.XYChart.new(root, {
        panX: true,
        panY: true,
        wheelX: "panX",
        wheelY: "zoomX",
        pinchZoomX: true,
        paddingLeft: 0,
      })
    );

    // Add cursor
    var cursor = chart.set(
      "cursor",
      am5xy.XYCursor.new(root, {
        behavior: "none",
      })
    );
    cursor.lineX.setAll({
      stroke: am5.color("#ffffff"), // White color
      strokeWidth: 5, // Adjust thickness if needed
      strokeDasharray: [], // Ensures solid line
    });
    cursor.lineY.set("visible", false);

    // Create axes
    var xAxis = chart.xAxes.push(
      am5xy.DateAxis.new(root, {
        baseInterval: {
          timeUnit: "day",
          count: 1,
        },
        renderer: am5xy.AxisRendererX.new(root, {}),
      })
    );
    xAxis.get("renderer").grid.template.setAll({ visible: false });
    xAxis.get("renderer").labels.template.setAll({ visible: false });

    var yAxis = chart.yAxes.push(
      am5xy.ValueAxis.new(root, {
        renderer: am5xy.AxisRendererY.new(root, {}),
      })
    );
    yAxis.get("renderer").grid.template.setAll({ visible: false });
    yAxis.get("renderer").labels.template.setAll({ visible: false });

    // Format numbers in Indian number system
    function formatIndianNumber(value) {
      const absValue = Math.abs(value);
      let formattedValue;
      
      if (absValue >= 10000000) {
        formattedValue = (absValue / 10000000).toFixed(1) + "Cr";
      } else if (absValue >= 100000) {
        formattedValue = (absValue / 100000).toFixed(1) + "L";
      } else if (absValue >= 1000) {
        formattedValue = (absValue / 1000).toFixed(1) + "K";
      } else {
        formattedValue = absValue.toString();
      }
    
      return value < 0 ? "-" + formattedValue : formattedValue;
    }
    

    // To convert timestamp to date
    function convertSecondstoTime(secValue) {

        let current_date = new Date(secValue);
        return current_date.getUTCDate() + "-" + (current_date.getUTCMonth() + 1) + "-" + current_date.getUTCFullYear();
    }


    // Add series
    var series = chart.series.push(
      am5xy.LineSeries.new(root, {
        name: "Net Profit",
        xAxis: xAxis,
        yAxis: yAxis,
        valueYField: "profit",
        valueXField: "date",
        stroke: am5.color("#845ec2"),
        tooltip: am5.Tooltip.new(root, {
          labelText: "{valueY}",
          dy: -5, // Moves the tooltip slightly lower - NEW
          getFillFromSprite: false, // Ensures custom styling applies - NEW
        }),
      })
    );
    // Add bullets to each data point - NEW
    series.bullets.push(function () {
      return am5.Bullet.new(root, {
        sprite: am5.Circle.new(root, {
          radius: 2, // Bullet size
          fill: am5.color("#845ec2"), // Bullet color matching line
          stroke: am5.color("#ffffff"), // White outline
          strokeWidth: 1, // Outline thickness
        }),
      });
    });
    // Apply styling to tooltip text - NEW
    series.get("tooltip").label.setAll({
      fontWeight: "bold", // Bold text
      fontSize: 16, //  Adjust size
    });

    // Ensure the fill color applies correctly - NEW
    series.get("tooltip").label.adapters.add("fill", function () {
      return am5.color("#000"); // Custom color
    });
    // Make tooltip background transparent - NEW
    series
    .get("tooltip")
    .get("background")
    .setAll({
      fill: am5.color("#f9c462"),
      fillOpacity: 0.25, // Makes background transparent
      strokeOpacity: 0, // Removes border
    });

  // Explicitly set stroke width on the stroke template
  series.strokes.template.setAll({
    strokeWidth: 2, // Adjust thickness as needed
  });
    // Format tooltip values
    series.get("tooltip").label.adapters.add("text", function (text, target) {
      
      if (target.dataItem && target.dataItem.get("valueY") !== undefined) {
        if(target.dataItem.dataContext.range==30){
          return (
            formatIndianNumber(target.dataItem.get("valueY")) + "|" + convertSecondstoTime(target.dataItem.get("valueX"))
          );
        }else if(target.dataItem.dataContext.range==6){
          return (
          formatIndianNumber(target.dataItem.get("valueY")) + "|" +
            target.dataItem.dataContext.month
          );
        }else{
          return (
            formatIndianNumber(target.dataItem.get("valueY")) + "|" +
              target.dataItem.dataContext.year
            );
        }
         // Add date
      }
      return text;
    });

    series.data.setAll(chartData);

    // Make stuff animate on load
    series.appear(1000);
    chart.appear(1000, 100);
  }
  // Manually defined data for each chart
  let data1 = lineGraphData.map(entry => ({
      date: new Date(entry.start).getTime(),
      profit: entry.data,
      month: entry.date,
      range: entry.range,
      year: entry.year
  }));

  let data2 = lineGraphReceivableJson.map(entry => ({
    date: new Date(entry.start).getTime(),
      profit: entry.data,
      month: entry.date,
      range: entry.range,
      year: entry.year
   }));
   let data3 = lineGraphCashflowJson.map(entry => ({
    date: new Date(entry.start).getTime(),
    profit: entry.data,
    month: entry.date,
      range: entry.range,
      year: entry.year
   }));
   let data4 = lineGraphNetprofitJson.map(entry => ({
    date: new Date(entry.start).getTime(),
    profit: entry.data,
    month: entry.date,
      range: entry.range,
      year: entry.year
   }));
   let data5 = lineGraphGrossprofitJson.map(entry => ({
    date: new Date(entry.start).getTime(),
    profit: entry.data,
    month: entry.date,
      range: entry.range,
      year: entry.year
   }));
  // let data2 = [
  //   { date: new Date(2024, 1, 1).getTime(), profit: 500000 },
  //   { date: new Date(2024, 1, 2).getTime(), profit: 520000 },
  //   { date: new Date(2024, 1, 3).getTime(), profit: 490000 },
  //   { date: new Date(2024, 1, 4).getTime(), profit: 510000 },
  //   { date: new Date(2024, 1, 5).getTime(), profit: 530000 },
  //   { date: new Date(2024, 1, 6).getTime(), profit: 500000 },
  //   { date: new Date(2024, 1, 7).getTime(), profit: 540000 },
  //   { date: new Date(2024, 1, 8).getTime(), profit: 550000 },
  //   { date: new Date(2024, 1, 9).getTime(), profit: 510000 },
  //   { date: new Date(2024, 1, 10).getTime(), profit: 520000 },
  // ];

  // let data3 = [
  //   { date: new Date(2024, 1, 1).getTime(), profit: 600000 },
  //   { date: new Date(2024, 1, 2).getTime(), profit: 620000 },
  //   { date: new Date(2024, 1, 3).getTime(), profit: 690000 },
  //   { date: new Date(2024, 1, 4).getTime(), profit: 610000 },
  //   { date: new Date(2024, 1, 5).getTime(), profit: 630000 },
  //   { date: new Date(2024, 1, 6).getTime(), profit: 500000 },
  //   { date: new Date(2024, 1, 7).getTime(), profit: 740000 },
  //   { date: new Date(2024, 1, 8).getTime(), profit: 650000 },
  //   { date: new Date(2024, 1, 9).getTime(), profit: 610000 },
  //   { date: new Date(2024, 1, 10).getTime(), profit: 620000 },
  // ];

  // let data4 = [
  //   { date: new Date(2024, 1, 1).getTime(), profit: 100000 },
  //   { date: new Date(2024, 1, 2).getTime(), profit: 120000 },
  //   { date: new Date(2024, 1, 3).getTime(), profit: 190000 },
  //   { date: new Date(2024, 1, 4).getTime(), profit: 10000 },
  //   { date: new Date(2024, 1, 5).getTime(), profit: 230000 },
  //   { date: new Date(2024, 1, 6).getTime(), profit: 100000 },
  //   { date: new Date(2024, 1, 7).getTime(), profit: 140000 },
  //   { date: new Date(2024, 1, 8).getTime(), profit: 150000 },
  //   { date: new Date(2024, 1, 9).getTime(), profit: 110000 },
  //   { date: new Date(2024, 1, 10).getTime(), profit: 120000 },
  // ];

  // let data5 = [
  //   { date: new Date(2024, 1, 1).getTime(), profit: 900000 },
  //   { date: new Date(2024, 1, 2).getTime(), profit: 920000 },
  //   { date: new Date(2024, 1, 3).getTime(), profit: 990000 },
  //   { date: new Date(2024, 1, 4).getTime(), profit: 810000 },
  //   { date: new Date(2024, 1, 5).getTime(), profit: 830000 },
  //   { date: new Date(2024, 1, 6).getTime(), profit: 900000 },
  //   { date: new Date(2024, 1, 7).getTime(), profit: 840000 },
  //   { date: new Date(2024, 1, 8).getTime(), profit: 950000 },
  //   { date: new Date(2024, 1, 9).getTime(), profit: 910000 },
  //   { date: new Date(2024, 1, 10).getTime(), profit: 920000 },
  // ];

  // Initialize multiple charts with manually defined data
  createChart("chartdiv1", data1);
  createChart("chartdiv2", data2);
  createChart("chartdiv3", data3);
  createChart("chartdiv4", data4);
  createChart("chartdiv5", data5);
}); // end am5.ready()


  
  // START OF ACCOUNTS BALANCE CHART
  
  var rootx = am5.Root.new("chartdiv6");
  
  // Set themes
  rootx.setThemes([am5themes_Animated.new(root)]);
  
  // Create chart
  var chartx = rootx.container.children.push(
    am5xy.XYChart.new(rootx, {
      panX: false,
      panY: false,
      wheelX: "panX",
      wheelY: "zoomX",
      layout: rootx.verticalLayout,
    })
  );
  
  // Add legend
  var legendx = chartx.children.push(
    am5.Legend.new(rootx, {
      centerX: am5.p50,
      x: am5.p50,
    })
  );
  
  // Data
  var datax = [
    {
      month: accountBalanceJson[2].month,
      Bank: parseFloat(accountBalanceJson[2].Bank),
      Cash: parseFloat(accountBalanceJson[2].Cash),
      Petty: parseFloat(accountBalanceJson[2].Petty),
    },
    {
      month: accountBalanceJson[1].month,
      Bank: parseFloat(accountBalanceJson[1].Bank),
      Cash: parseFloat(accountBalanceJson[1].Cash),
      Petty: parseFloat(accountBalanceJson[1].Petty),
    },
    {
      month: accountBalanceJson[0].month,
      Bank: parseFloat(accountBalanceJson[0].Bank),
      Cash: parseFloat(accountBalanceJson[0].Cash),
      Petty: parseFloat(accountBalanceJson[0].Petty),
    },
  ];
  console.log(accountBalanceJson);
  // Calculate total values dynamically
  datax.forEach((item) => {
    item.Total = item.Bank + item.Cash + item.Petty;
  });
  
  // Set custom number formatting
  rootx.numberFormatter.setAll({
    numberFormat: "#a",
    bigNumberPrefixes: [
      { number: 1e3, suffix: "K" }, // 1,000 -> 1K
      { number: 1e5, suffix: "L" }, // 1,00,000 -> 1L
      { number: 1e7, suffix: "Cr" }, // 1,00,00,000 -> 1Cr
    ],
    smallNumberPrefixes: [],
  });

  // Create axes
  var xAxisx = chartx.xAxes.push(
    am5xy.CategoryAxis.new(rootx, {
      categoryField: "month",
      renderer: am5xy.AxisRendererX.new(rootx, {
        cellStartLocation: 0.1,
        cellEndLocation: 0.9,
      }),
      tooltip: am5.Tooltip.new(rootx, {}),
    })
  );
  
  xAxisx.data.setAll(datax);
  
  xAxisx.get("renderer").labels.template.setAll({
    fontSize: 12,
    fontWeight: "bold",
  });
  
  var yAxisx = chartx.yAxes.push(
    am5xy.ValueAxis.new(rootx, {
      calculateTotals: true,
      min: 0,
      extraMax: 0.1,
      renderer: am5xy.AxisRendererY.new(rootx, {}),
      numberFormatter: rootx.numberFormatter, // Apply number formatting to Y-axis
    })
  );
  
  // Add series with specific colors
  function makeSeriesx(name, fieldName, color) {
    var seriesx = chartx.series.push(
      am5xy.ColumnSeries.new(rootx, {
        name: name,
        xAxis: xAxisx,
        yAxis: yAxisx,
        valueYField: fieldName,
        categoryXField: "month",
        stacked: true,
        maskBullets: false,
      })
    );
  
    seriesx.columns.template.setAll({
      tooltipText: "{name}, {categoryX}: {valueY.formatNumber('#a')}", // Use updated format
      width: am5.percent(90),
      tooltipY: 0,
      fill: am5.color(color),
      stroke: am5.color(color),
    });
  
    seriesx.data.setAll(datax);
    seriesx.appear(1000, 100);
    legendx.data.push(seriesx);
  }
  
  legendx.labels.template.setAll({
    fontSize: 14,
  });
  
  // Define custom colors for each series
  makeSeriesx("Bank", "Bank", "#845ec2"); // Purple
  makeSeriesx("Cash", "Cash", "#eb6787"); // Pink
  makeSeriesx("Petty", "Petty", "#67b7dc"); // Yellow
  
  // Add total series (non-legend, only labels)
  var totalSeriesx = chartx.series.push(
    am5xy.ColumnSeries.new(rootx, {
      name: "Total",
      xAxis: xAxisx,
      yAxis: yAxisx,
      valueYField: "Total",
      categoryXField: "month",
      stacked: true,
      maskBullets: false,
    })
  );
  
  totalSeriesx.columns.template.setAll({
    fillOpacity: 0,
    strokeOpacity: 0,
  });
  
  totalSeriesx.bullets.push(function () {
    return am5.Bullet.new(rootx, {
      locationY: 0.25,
      sprite: am5.Label.new(rootx, {
        text: "{valueY.formatNumber('#a')}",
        fontSize: 12,
        fill: am5.color(0x000000),
        centerY: am5.p50,
        centerX: am5.p50,
        populateText: true,
      }),
    });
  });
  
  totalSeriesx.data.setAll(datax);
  totalSeriesx.appear(1000, 100);
  
  // Animate chart on load
  chartx.appear(1000, 100);
  
  // START OF SALES GRAPH
  
  am5.ready(function () {
    // Create root element
    var root = am5.Root.new("chartdiv7");
  
    // Set themes
    root.setThemes([am5themes_Animated.new(root)]);
    
    root.numberFormatter.setAll({
      numberFormat: "#a",
      bigNumberPrefixes: [
        { number: 1e3, suffix: "K" }, // 1,000 -> 1K
        { number: 1e5, suffix: "L" }, // 1,00,000 -> 1L
        { number: 1e7, suffix: "Cr" }, // 1,00,00,000 -> 1Cr
      ],
      smallNumberPrefixes: [],
    });

    // Create chart
    var chart = root.container.children.push(
      am5xy.XYChart.new(root, {
        panX: true,
        panY: true,
        wheelX: "panX",
        wheelY: "zoomX",
        pinchZoomX: true,
      })
    );
  
    // Add cursor
    var cursor = chart.set("cursor", am5xy.XYCursor.new(root, {}));
    cursor.lineY.set("visible", false);
  
    // Create axes
    var xRenderer = am5xy.AxisRendererX.new(root, {
      minGridDistance: 30,
      minorGridEnabled: true,
    });
  
    xRenderer.labels.template.setAll({
      centerX: am5.p50,
      rotation: 0,
      paddingRight: 15,
      fontWeight: "bold",
      fontSize: 12,
    });
  
    xRenderer.grid.template.setAll({
      location: 1,
    });
  
    var xAxis = chart.xAxes.push(
      am5xy.CategoryAxis.new(root, {
        maxDeviation: 0.3,
        categoryField: "month",
        renderer: xRenderer,
        tooltip: am5.Tooltip.new(root, {}),
      })
    );
  
    var yRenderer = am5xy.AxisRendererY.new(root, {
      strokeOpacity: 0.1,
    });
  
    var yAxis = chart.yAxes.push(
      am5xy.ValueAxis.new(root, {
        maxDeviation: 0.3,
        renderer: yRenderer,
        numberFormat: "#a",
      })
    );
  
    // Create series
    var series = chart.series.push(
      am5xy.ColumnSeries.new(root, {
        name: "Sales",
        xAxis: xAxis,
        yAxis: yAxis,
        valueYField: "sales",
        sequencedInterpolation: true,
        categoryXField: "month",
        tooltip: am5.Tooltip.new(root, {
          labelText: "{valueY.formatNumber('#a')}",
        }),
      })
    );
  
    series.columns.template.setAll({ cornerRadiusTL: 5, cornerRadiusTR: 5, strokeOpacity: 0 });
    series.columns.template.adapters.add("fill", function (fill, target) {
      return chart.get("colors").getIndex(series.columns.indexOf(target));
    });
  
    series.columns.template.adapters.add("stroke", function (stroke, target) {
      return chart.get("colors").getIndex(series.columns.indexOf(target));
    });
  
    // Set custom colors
    chart.get("colors").set("colors", ["#67b7dc", "#6794dc", "#6771dc"]);
  
    // Set data
    var data = [ 
      {
          month: salesData.two_months_ago.month,
          sales: parseFloat(salesData.two_months_ago.sales),
      },
      {
        month: salesData.previous_month.month,
        sales: parseFloat(salesData.previous_month.sales),
      },
      {
        month: salesData.current_month.month,
        sales: parseFloat(salesData.current_month.sales),
      },
    ];
  
    xAxis.data.setAll(data);
    series.data.setAll(data);
  
    // Make stuff animate on load
    series.appear(1000);
    chart.appear(1000, 100);
  });
  
  // START OF PROFIT LOSS GRAPH
  
  var root = am5.Root.new("chartdiv8");
  
  // Set themes
  root.setThemes([am5themes_Animated.new(root)]);
  
  // Apply custom number formatting
  root.numberFormatter.setAll({
    numberFormat: "#.##a",
    bigNumberPrefixes: [
      { number: 1e3, suffix: "K" }, // 1,000 -> 1K
      { number: 1e5, suffix: "L" }, // 1,00,000 -> 1L
      { number: 1e7, suffix: "Cr" }, // 1,00,00,000 -> 1Cr
    ],
    smallNumberPrefixes: [],
  });

  // Create chart
  var chart = root.container.children.push(
    am5xy.XYChart.new(root, {
      panX: false,
      panY: false,
      wheelX: "panX",
      wheelY: "zoomX",
      layout: root.verticalLayout,
    })
  );
  
  // Add scrollbar
  
  chart
    .get("colors")
    .set("colors", [
      am5.color(0x845ec2),
      am5.color(0xd65db1),
      am5.color(0xff6f91),
      am5.color(0x86a873),
      am5.color(0xbb9f06),
    ]);
  var data = [
    {
      year: profitlossData[1].year,
      income: parseFloat(profitlossData[1].income),
      expenses: parseFloat(profitlossData[1].expenses),
    },
    {
      year: profitlossData[2].year,
      income: parseFloat(profitlossData[2].income),
      expenses: parseFloat(profitlossData[2].expenses),
    },
    {
      year: profitlossData[3].year,
      income: parseFloat(profitlossData[3].income),
      expenses: parseFloat(profitlossData[3].expenses),
    },
    {
      year: profitlossData[4].year,
      income: parseFloat(profitlossData[4].income),
      expenses: parseFloat(profitlossData[4].expenses),
      strokeSettings: {
        stroke: chart.get("colors").getIndex(1),
        strokeWidth: 3,
        strokeDasharray: [5, 5],
      },
    },
    {
      year: profitlossData[5].year,
        income: parseFloat(profitlossData[5].income),
        expenses: parseFloat(profitlossData[5].expenses),
      columnSettings: {
        strokeWidth: 1,
        strokeDasharray: [5],
        fillOpacity: 0.2,
      },
      info: "(projection)",
    },
  ];
  
  // Create axes
  var xRenderer = am5xy.AxisRendererX.new(root, {
    minorGridEnabled: true,
    minGridDistance: 60,
  });
  var xAxis = chart.xAxes.push(
    am5xy.CategoryAxis.new(root, {
      categoryField: "year",
      renderer: xRenderer,
      tooltip: am5.Tooltip.new(root, {}),
    })
  );
  xRenderer.grid.template.setAll({
    location: 1,
  });
  
  xAxis.data.setAll(data);
  
  xAxis.get("renderer").labels.template.setAll({
    fontSize: 12,
    fontWeight: "bold",
  });
  
  var yAxis = chart.yAxes.push(
    am5xy.ValueAxis.new(root, {
      min: 0,
      extraMax: 0.1,
      renderer: am5xy.AxisRendererY.new(root, {
        strokeOpacity: 0.1,
      }),
      numberFormat: "#.##a",
    })
  );
  
  // Add series
  
  var series1 = chart.series.push(
    am5xy.ColumnSeries.new(root, {
      name: "Income",
      xAxis: xAxis,
      yAxis: yAxis,
      valueYField: "income",
      categoryXField: "year",
      tooltip: am5.Tooltip.new(root, {
        pointerOrientation: "horizontal",
        labelText: "{name} in {categoryX}: {valueY.formatNumber('#.##a')} {info}", 
      }),
    })
  );
  
  series1.columns.template.setAll({
    tooltipY: am5.percent(10),
    templateField: "columnSettings",
  });
  
  series1.data.setAll(data);
  
  var series2 = chart.series.push(
    am5xy.LineSeries.new(root, {
      name: "Expenses",
      xAxis: xAxis,
      yAxis: yAxis,
      valueYField: "expenses",
      categoryXField: "year",
      tooltip: am5.Tooltip.new(root, {
        pointerOrientation: "horizontal",
        labelText: "{name} in {categoryX}: {valueY.formatNumber('#.##a')} {info}",
      }),
    })
  );
  
  series2.strokes.template.setAll({
    strokeWidth: 3,
    templateField: "strokeSettings",
  });
  
  series2.data.setAll(data);
  
  series2.bullets.push(function () {
    return am5.Bullet.new(root, {
      sprite: am5.Circle.new(root, {
        strokeWidth: 3,
        stroke: series2.get("stroke"),
        radius: 5,
        fill: root.interfaceColors.get("background"),
      }),
    });
  });
  
  chart.set("cursor", am5xy.XYCursor.new(root, {}));
  
  // Add legend
  var legend = chart.children.push(
    am5.Legend.new(root, {
      centerX: am5.p50,
      x: am5.p50,
    })
  );
  legend.data.setAll(chart.series.values);
  
  legend.labels.template.setAll({
    fontSize: 14,
  });
  
  // Make stuff animate on load
  chart.appear(1000, 100);
  series1.appear();
  
 // START OF PIE OF PIE GRAPH
 if (expenseData.length > 1 && 
  expenseData[0].value !== 0 && 
  expenseData[1].value !== 0) {  
  am5.ready(function () {
  var root = am5.Root.new("chartdiv9");
  root.setThemes([am5themes_Animated.new(root)]);

  var container = root.container.children.push(
    am5.Container.new(root, {
      width: am5.p100,
      height: am5.p100,
      layout: root.horizontalLayout,
    })
  );

  var chart = container.children.push(
    am5percent.PieChart.new(root, {
      tooltip: am5.Tooltip.new(root, {}),
    })
  );

  var series = chart.series.push(
    am5percent.PieSeries.new(root, {
      valueField: "value",
      categoryField: "category",
      alignLabels: false,
    })
  );

  series
    .get("colors")
    .set("colors", [
      am5.color(0x845ec2),
      am5.color(0xd65db1),
      am5.color(0xff6f91),
      am5.color(0x86a873),
      am5.color(0xbb9f06),
    ]);

  series.labels.template.setAll({
    textType: "circular",
    radius: 4,
  });

  series.ticks.template.set("visible", false);
  series.slices.template.set("toggleKey", "none");
  series.slices.template.events.on("click", function (e) {
    selectSlice(e.target);
  });
  var subChart = container.children.push(
    am5percent.PieChart.new(root, {
      radius: am5.percent(80),
      tooltip: am5.Tooltip.new(root, {}),
    })
  );
  var subSeries = subChart.series.push(
    am5percent.PieSeries.new(root, {
      valueField: "value",
      categoryField: "category",
    })
  );

  subSeries
    .get("colors")
    .set("colors", [
      am5.color(0x89e0142),
      am5.color(0x3288bd),
      am5.color(0x6c2a5),
      am5.color(0xd53e4f),
      am5.color(0xabdda4),
      am5.color(0xf46d43),
      am5.color(0xffffbf),
      am5.color(0x845ec2),
      am5.color(0xfdae61),
      am5.color(0xff6f91),
      am5.color(0x86a873),
      am5.color(0x5e4fa2),
      am5.color(0xbb9f06),
    ]);

  subSeries.data.setAll([
    { category: "A", value: 0 },
    { category: "B", value: 0 },
    { category: "C", value: 0 },
    { category: "D", value: 0 },
    { category: "E", value: 0 },
    { category: "F", value: 0 },
    { category: "G", value: 0 },
  ]);
  subSeries.slices.template.set("toggleKey", "none");
  var selectedSlice;
  series.on("startAngle", function () {
    updateLines();
  });
  container.events.on("boundschanged", function () {
    root.events.once("frameended", function () {
      updateLines();
    });
  });
  function updateLines() {
    if (selectedSlice) {
      var startAngle = selectedSlice.get("startAngle");
      var arc = selectedSlice.get("arc");
      var radius = selectedSlice.get("radius");
      var x00 = radius * am5.math.cos(startAngle);
      var y00 = radius * am5.math.sin(startAngle);
      var x10 = radius * am5.math.cos(startAngle + arc);
      var y10 = radius * am5.math.sin(startAngle + arc);
      var subRadius = subSeries.slices.getIndex(0).get("radius");
      var x01 = 0;
      var y01 = -subRadius;
      var x11 = 0;
      var y11 = subRadius;
      var point00 = series.toGlobal({ x: x00, y: y00 });
      var point10 = series.toGlobal({ x: x10, y: y10 });
      var point01 = subSeries.toGlobal({ x: x01, y: y01 });
      var point11 = subSeries.toGlobal({ x: x11, y: y11 });
      line0.set("points", [point00, point01]);
      line1.set("points", [point10, point11]);
    }
  }
  var line0 = container.children.push(
    am5.Line.new(root, {
      position: "absolute",
      stroke: root.interfaceColors.get("text"),
      strokeDasharray: [2, 2],
    })
  );
  var line1 = container.children.push(
    am5.Line.new(root, {
      position: "absolute",
      stroke: root.interfaceColors.get("text"),
      strokeDasharray: [2, 2],
    })
  );
  function formatIndianNumber(num) {
    if (num >= 10000000) {
      return (num / 10000000).toFixed(2).replace(/\.00$/, "") + "Cr";
    } else if (num >= 100000) {
      return (num / 100000).toFixed(2).replace(/\.00$/, "") + "L";
    } else if (num >= 1000) {
      return (num / 1000).toFixed(2).replace(/\.00$/, "") + "K";
    }
    return num.toString();
  }
  series.data.setAll([
    {
      category: expenseData[0].category,
      value: expenseData[0].value,
      subData: expenseData[0].subData.map(sub => ({
        category: sub.category,
          value: sub.value
      }))
    },
    {
      category: expenseData[1].category,
        value: expenseData[1].value,
        subData: expenseData[1].subData.map(sub => ({
          category: sub.category,
            value: sub.value
        }))
    },
  ]);

  function selectSlice(slice) {
    selectedSlice = slice;
    var dataItem = slice.dataItem;
    var dataContext = dataItem.dataContext;
    if (dataContext) {
      var i = 0;
      subSeries.data.each(function (dataObject) {
        var dataObj = dataContext.subData[i];
        if (dataObj) {
          if (!subSeries.dataItems[i].get("visible")) {
            subSeries.dataItems[i].show();
          }
          subSeries.data.setIndex(i, dataObj);
        } else {
          subSeries.dataItems[i].hide();
        }
        i++;
      });
    }
    chart.set("tooltip", am5.Tooltip.new(root, {}));
    chart.get("tooltip").label.adapters.add("text", function (text, target) {
      var dataItem = target.dataItem;
      if (dataItem) {
        var value = dataItem.get("value");
        return dataItem.get("category") + ": " + formatIndianNumber(value);
      }
      return text;
    });
    subChart.set("tooltip", am5.Tooltip.new(root, {}));
    subChart.get("tooltip").label.adapters.add("text", function (text, target) {
      var dataItem = target.dataItem;
      if (dataItem) {
        var value = dataItem.get("value");
        return dataItem.get("category") + ": " + formatIndianNumber(value);
      }
      return text;
    });
    var middleAngle = slice.get("startAngle") + slice.get("arc") / 2;
    var firstAngle = series.dataItems[0].get("slice").get("startAngle");
    series.animate({
      key: "startAngle",
      to: firstAngle - middleAngle,
      duration: 1000,
      easing: am5.ease.out(am5.ease.cubic),
    });
    series.animate({
      key: "endAngle",
      to: firstAngle - middleAngle + 360,
      duration: 1000,
      easing: am5.ease.out(am5.ease.cubic),
    });
  }
  container.appear(1000, 10);
  series.events.on("datavalidated", function () {
    selectSlice(series.slices.getIndex(0));
  });
  }); // end am5.ready()
}
else{
  document.getElementById("chartdiv9").innerHTML = "<p style='font-size: 18px; font-weight: bold; text-align: center; display: flex; align-items: center; justify-content: center; height: 100%;'>No Data found</p>";
}