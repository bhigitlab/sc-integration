<?php 
                   
                    foreach ($materials as $key => $material) {                                                
                        $specification = Specification::model()->findByPk($material);
                        $brand = Brand::model()->findByPk($specification ['brand_id']);
                        $category = PurchaseCategory::model()->findByPk($specification ['cat_id']);                                                
                    
                        $material_specification = '<tr id="material_specification">                              
                            <td class="text_title">
                            '.$brand["brand_name"].' - '.$specification["specification"].'
                            </td>                             
                        </tr>';
                    } ?>
     <?php
     /*-----------gallery section starts-----------*/
        $qtn_gallery =array();
        $sql_gallery = 'SELECT * FROM `jp_quotation_section` '
        . 'WHERE `qtn_id` = ' . $qtid
        . ' AND revision_no = "' . $rev_no . '"';
        $sectionArrayGallery = Yii::app()->db->createCommand($sql_gallery)->queryAll();
        $s = 1;
        foreach ($sectionArrayGallery as $key => $value) {
            $qtn_gallery[$s]['section_name']=$value['section_name'];
            $sql = 'SELECT * FROM `jp_quotation_gen_category` '
                    . 'WHERE `qid` = ' . $qtid . ' AND `section_id` = ' . $value["id"]
                    . ' AND revision_no = "' . $rev_no . '"';
           
            $subsectionArrayGallery = Yii::app()->db->createCommand($sql)->queryAll();
            $i_gallery = 1;
            foreach ($subsectionArrayGallery as $key => $value1) {
                $qtn_gallery[$s][$i_gallery]['id']=$value1['id'];
                $qtn_gallery[$s][$i_gallery]['category_label']=$value1['category_label'];
                $sql = 'SELECT * FROM `jp_quotation_gen_worktype` '
                . ' WHERE `qid` = ' . $value1["qid"]
                . ' AND `section_id` = ' . $value1["section_id"]
                . ' AND `category_label_id` = ' . $value1["id"]
                . ' AND revision_no = "' . $rev_no . '"';
                $subitemArrayGallery = Yii::app()->db->createCommand($sql)->queryAll();
                $letters = range('a', 'z');
                $k_gallery = 0;
                foreach ($subitemArrayGallery as $key => $value3) {
                    $qtn_gallery[$s][$i_gallery]['subitem'][$k_gallery]['id']=$value3['id'];
                    $qtn_gallery[$s][$i_gallery]['subitem'][$k_gallery]['worktype_label']=$value3['worktype_label'];
                    $sql = 'SELECT * FROM `jp_sales_quotation` '
                            . 'WHERE `parent_type` = 1 AND `parent_id` = ' . $value3['id']
                            . ' AND revision_no="' . $rev_no . '" AND `deleted_status` = 1';
                    $wrktypeitemArrayGallery = Yii::app()->db->createCommand($sql)->queryAll();
                    $l=0;
                    foreach ($wrktypeitemArrayGallery as $key => $value4) { 
                        $desc = ($value4['description'] !="")?" - ".$value4['description']:"";
                        if($value4['description']=="" && ($value4['shutterwork_description'] !="" || $value4['carcass_description']!="")){
                            $desc = " - <b>Shutter:</b> ".$value4['shutterwork_description']." , <b>Carcass:</b> ".$value4['carcass_description'];
                        }
                        $sql = 'SELECT g.image,g.caption FROM `jp_sales_quotation_gallery` g'
                        .' WHERE g.id="'.$value4['image_gallery_id'].'"';
                        $images = Yii::app()->db->createCommand($sql)->queryAll();
                        $master_images_sql = 'SELECT m.image,m.worktype_label as caption, 1 AS master_image FROM `jp_quotation_item_master` m'
                        .' WHERE m.id="'.$value4['item_id'].'" AND m.image IS NOT NULL AND LENGTH(m.image) > 0';
                        $master_images = Yii::app()->db->createCommand($master_images_sql)->queryAll();
                        if (empty($images)) {
                            $combined_array = $master_images;
                        } elseif (empty($master_images)) {
                            $combined_array = $images;
                        } else {
                            $combined_array = array_merge($images, $master_images);
                        }if(!empty($combined_array)) {
                            $j=0;
                            foreach ($combined_array as $key => $value) {
                                if ($value['image'] != '') {
                                    if(isset($value['master_image'])){
                                        $path = realpath(Yii::app()->basePath . '/../uploads/image/' . $value["image"]);
                                    }else{
                                        $path = realpath(Yii::app()->basePath . '/../uploads/quotation_gallery/' . $value["image"]);
                                    }
                                    if (file_exists($path)) {
                                        $qtn_gallery[$s][$i_gallery]['subitem'][$k_gallery]['is_image']=1;
                                        $qtn_gallery[$s][$i_gallery]['subitem'][$k_gallery]['item'][$l]['is_image']=1;
                                        $qtn_gallery[$s][$i_gallery]['subitem'][$k_gallery]['item'][$l]['images'][$j]=$value;
                                        $qtn_gallery[$s][$i_gallery]['is_image']=1;
                                        $qtn_gallery[$s]['is_image']=1;
                                    }
                                }
                                $j +=1;
                            }
                            if($value4['item_name']!=""){
                                $item_name= $value4['item_name'];
                            }else{
                                $item_name= $value4['worktype_label'].$desc;
                            }                                                                              
                            $qtn_gallery[$s][$i_gallery]['subitem'][$k_gallery]['item'][$l]['item_name']=$item_name;
                            
                        }
                        $l +=1;
                    }if (!empty($wrktypeitemArrayGallery)) {
                    }$k_gallery +=1;
                }$i_gallery +=1;
            }$s +=1;        
        }//gallery view
        $section_index=1;
        $total_section_count = 0;
        foreach ($qtn_gallery as $qtn_sections) {
            if (isset($qtn_sections['is_image']) && $qtn_sections['is_image'] === 1) {
                $total_section_count++;
            }
        }
        $image_index =1;
        if(is_array($qtn_gallery)){
        foreach($qtn_gallery as $qtn_sections){
            if(isset($qtn_sections['is_image']) && $qtn_sections['is_image'] )
            {   if($section_index==1){
                    $quotation_galley_images = "<table class='item_table'><tbody>";
                }$quotation_galley_images .= "<tr>
                    
                <th class='title' colspan='6'>".$qtn_sections['section_name']."</th></tr>";
                $indx=1;
                if(is_array($qtn_sections)){
                foreach($qtn_sections as $qtn_subsections){
                    if(isset($qtn_subsections['is_image']) && $qtn_subsections['is_image'] )
                    {
                        $quotation_galley_images .= "<tr>
                    
                        <th class='title' colspan='6'>".$indx.".".$qtn_subsections['category_label']."</th></tr>";
                        $k_indx=0;
                        $letters =range('a', 'z');
                        if(isset($qtn_subsections['subitem'])){
                        foreach($qtn_subsections['subitem'] as $qtn_subitems){
                            if(isset($qtn_subitems['is_image']) && $qtn_subitems['is_image'] )
                            {
                                $quotation_galley_images .= "<tr>
                                <th colspan='6'>".$letters[$k_indx].") ".$qtn_subitems['worktype_label']."</th></tr>";
                                if(isset($qtn_subitems['item'])){
                                foreach($qtn_subitems['item'] as $qtn_image){
                                    if($image_index==1){
                                        $quotation_galley_images .= "<tr class='td_group'>
                                                                <th class='title' colspan='3'>Image</th>
                                                                <th class='title' colspan='3'>Item Name</th></tr>";
                                        }
                                    if(isset($qtn_image['images'])){
                                    $quotation_galley_images .= "<tr class='td_group'>";
                                    $quotation_galley_images .="<td colspan='3'>";
                                
                                foreach($qtn_image['images'] as $qtn_images){
                                    if ($qtn_images['image'] != '') {
                                        if(isset($qtn_images['master_image'])){
                                            $path = realpath(Yii::app()->basePath . '/../uploads/image/' . $qtn_images["image"]);
                                        }else{
                                            $path = realpath(Yii::app()->basePath . '/../uploads/quotation_gallery/' . $qtn_images["image"]);
                                        }
                                        if (file_exists($path)) {
                                            if(isset($qtn_images['master_image'])){
                                                $quotation_galley_images .=  CHtml::tag('img',
                                                array(
                                                    'src' => Yii::app()->request->baseUrl . "/uploads/image/" . $qtn_images["image"],
                                                    'alt' => '',
                                                    'class' => 'pop',
                                                    'modal-src' => Yii::app()->request->baseUrl . "/uploads/image/" . $qtn_images["image"],
                                                    'width'=>'100px',
                                                    'style' => 'padding: 10px 25px 10px;'
                                                    )
                                                );
                                            }else{
                                                $quotation_galley_images .=  CHtml::tag('img',
                                                array(
                                                    'src' => Yii::app()->request->baseUrl . "/uploads/quotation_gallery/" . $qtn_images["image"],
                                                    'alt' => '',
                                                    'class' => 'pop',
                                                    'modal-src' => Yii::app()->request->baseUrl . "/uploads/quotation_gallery/" . $qtn_images["image"],
                                                    'width'=>'100px',
                                                    'style' => 'padding: 10px 25px 10px;'
                                                    )
                                                );  
                                            }
                                            }
                                    }$image_index +=1;
                                }$quotation_galley_images .="</td><td colspan='3'>".$qtn_image['item_name'].'</td>';
                                $quotation_galley_images .= '</tr>';
                                }
                                }
                            }
                                $k_indx +=1;    
                            }
                        }
                        }
                        $indx +=1;   
                    }
                }
                }
                if($total_section_count==$section_index){
                    $quotation_galley_images .= '</tbody></table>';
                }
                $section_index +=1; 
            }
        }
    }  
/*-----------gallery section ends-----------*/
        ?>
        <?php
            $item_list = "";
                $sql = 'SELECT * FROM `jp_quotation_section` '
                        . 'WHERE `qtn_id` = ' . $qtid
                        . ' AND revision_no = "' . $rev_no . '"';
                $sectionArray = Yii::app()->db->createCommand($sql)->queryAll();
                $i = 1;
                $secletters = range('A', 'Z');
                $se = 0;
                foreach ($sectionArray as $key => $value) {
                    $sql = 'SELECT * FROM `jp_quotation_gen_category` '
                            . 'WHERE `qid` = ' . $qtid . ' AND `section_id` = ' . $value["id"]
                            . ' AND revision_no = "' . $rev_no . '"';
                   
                    $subsectionArray = Yii::app()->db->createCommand($sql)->queryAll();
                    $unit = Unit::model()->findByPk($value['unit']);
                    $secsum = Controller::getSectionSum($value['id'], $qtid, $rev_no);
                    $subsectionArraySpan = empty($subsectionArray) ? '2' : '7';
                    $unit = isset($unit) ? $unit->unit_name : "";

            
                  
                    
                    $item_list .= '<tr>
                           ';
                     $item_list .= '<th colspan="'.$subsectionArraySpan.'" style="background:#c00000;color:#fff;">'.$value["section_name"].'</th>';
                        if (empty($subsectionArray)) {
                            $item_list .= '<td style="background:#c00000;color:#fff;"></td>';
                            $item_list .= '<td style="background:#c00000;color:#fff;">'.$unit.'</td>';
                            $item_list .= '<td style="background:#c00000;color:#fff;">'.$value['quantity_nos'] .'</td>';
                            $item_list .= '<td style="background:#c00000;color:#fff;">'.$value['mrp'].'</td>';
                            $item_list .= '<td style="background:#c00000;color:#fff;">'.$value['amount_after_discount'].'</td>';
                            
                         } 
                        
                     $item_list .= '</tr>';
                   
                    
                    foreach ($subsectionArray as $key => $value1) {
                       $sql = 'SELECT * FROM `jp_sales_quotation` WHERE `parent_type` = 0 '
                                . ' AND `parent_id` = ' . $value1['id']
                                . ' AND revision_no="' . $rev_no . '" AND `deleted_status` = 1';
                        $itemArray = Yii::app()->db->createCommand($sql)->queryAll();

                        $sql = 'SELECT * FROM `jp_quotation_gen_worktype` '
                                . ' WHERE `qid` = ' . $value1["qid"]
                                . ' AND `section_id` = ' . $value1["section_id"]
                                . ' AND `category_label_id` = ' . $value1["id"]
                                . ' AND revision_no = "' . $rev_no . '"';
                        $subitemArray = Yii::app()->db->createCommand($sql)->queryAll();
                        $unit = Unit::model()->findByPk($value1['unit']);
                        $subitemArraySpan = (empty($itemArray)) && (empty($subitemArray)) ? '1' : '6';
                        $unit = isset($unit) ? $unit->unit_name : "";
                        $item_list .= '<tr>
                            <th  style="background:#c5d9f1;">'.$i.'</th>';
                            $item_list .= '<th colspan="'.$subitemArraySpan.'" style="background:#c5d9f1;">'.$value1['category_label'].'</th>';
                             if ((empty($itemArray)) && (empty($subitemArray))) { 
                                $item_list .= '<td  style="background:#c5d9f1;"></td>';
                                $item_list .= '<td  style="background:#c5d9f1;">'.$unit.'</td>';
                                $item_list .= '<td  style="background:#c5d9f1;">'.$value1['quantity_nos'].'</td>';
                                $item_list .= '<td  style="background:#c5d9f1;">'.$value1['mrp'].'</td>';
                                $item_list .= '<td  style="background:#c5d9f1;">'.$value1['amount_after_discount'].'</td>';
                                
                             }                                                                                       
                             $item_list .= '</tr>';

                        
                        foreach ($itemArray as $key => $value2) {
                           
                            $item_list .= '<tr>
                                <td>'.'*'.'</td>
                                <td colspan="6">'.$value2['worktype_label'].'</td>
                            </tr>';

                         }
                        
                        
                        $letters = range('a', 'z');
                        $k = 0;
                        foreach ($subitemArray as $key => $value3) {
                            $sql = 'SELECT * FROM `jp_sales_quotation` '
                                    . 'WHERE `parent_type` = 1 AND `parent_id` = ' . $value3['id']
                                    . ' AND revision_no="' . $rev_no . '" AND `deleted_status` = 1';
                            $wrktypeitemArray = Yii::app()->db->createCommand($sql)->queryAll();
                            $unit = Unit::model()->findByPk($value3['unit']);
                            $wrktypeitemArray_span = empty($wrktypeitemArray) ? '1' : '6';
                            $unit = isset($unit) ? $unit->unit_name : "";
                            //$treeData = $this->buildTreeData($wrktypeitemArray);
                            
                            $item_list .= '<tr class="td_group">';
                            $item_list .= '<th>'.$letters[$k].'</th>';
                            $item_list .= '<td colspan="'.$wrktypeitemArray_span.'"><b>'.$value3['worktype_label'].'</b></td>';
                                if (empty($wrktypeitemArray)) {
                                    $item_list .='<td></td>';
                                    if($value3['quantity_nos']!=''){
                                        $item_list .='<td>'."nos".'</td>';
                                    }else{
                                        $item_list .='<td>'.$unit.'</td>';
                                    }
                                    if($value3['quantity_nos']!=''){
                                        $item_list .='<td>'.$value3['quantity_nos'].'</td>';
                                    }else{
                                        $item_list .='<td>'.$value3['quantity'].'</td>';
                                    }
                                    $item_list .= '<td>'.$value3['mrp'].'</td>';
                                    $item_list .= '<td>'.$value3['amount_after_discount'].'</td> ';                                                   
                                 } 
                                
                                 $item_list .= '</tr>';
                            if ($value3['description'] != "") { 
                                $item_list .= '<tr class="td_group">
                                    <th>*</th>
                                    <td colspan="6">'.$value3['description'].'</td>
                                    
                                </tr>';
                               
                            }
                            $sum = 0; 
                            foreach ($wrktypeitemArray as $key => $value4) {
                                $sum += $value4['amount_after_discount'];
                                $unit = Unit::model()->findByPk($value4['unit']);

                                $desc = ($value4['description'] !="")?" - ".$value4['description']:"";
                                if($value4['description']=="" && ($value4['shutterwork_description'] !="" || $value4['carcass_description']!="")){
                                    $desc = " - <b>Shutter:</b> ".$value4['shutterwork_description']." , <b>Carcass:</b> ".$value4['carcass_description'];
                                } 
                                 
                                
                                $item_list .= '<tr class="td_group">';
                                $item_list .= '<th>'.'*'.'</th>';
                                $item_list .='<td>';
                                        
                                        if($value4['item_name']!=""){
                                            $item_list .= $value4['item_name'];
                                        }else{
                                            $item_list .= $value4['worktype_label'].$desc;
                                        }                                                                              
                                     
                                    $item_list .='</td>
                                    <td>';
                                       
                                    $item_master = QuotationItemMaster::model()->findByPk($value4['item_id']);
                                    
                                    if ($value4['shutterwork_material']) {
                                        $shutter_material_id = $item_master['shutter_material_id'];                                        
                                        $specification = Specification::model()->findByPk($shutter_material_id);
                                        $brand = Brand::model()->findByPk($specification ['brand_id']);
                                        $category = PurchaseCategory::model()->findByPk($specification ['cat_id']);
                                        $smaterial_name = $category["category_name"].'-'.$brand["brand_name"].'-'.$specification["specification"];
                                    }
                        
                                    if ($value4['carcass_material']) {
                                        $carcass_material_id = $item_master['carcass_material_id'];
                                        $specification = Specification::model()->findByPk($carcass_material_id);
                                        $brand = Brand::model()->findByPk($specification ['brand_id']);
                                        $category = PurchaseCategory::model()->findByPk($specification ['cat_id']);
                                        $cmaterial_name = $category["category_name"].'-'.$brand["brand_name"].'-'.$specification["specification"];
                                    }
                                    if ($value4['material']) {
                                        $material_ids = $item_master['material_ids'];
                                        $specification = Specification::model()->findByPk($material_ids);
                                        $brand = Brand::model()->findByPk($specification ['brand_id']);
                                        $category = PurchaseCategory::model()->findByPk($specification ['cat_id']);
                                        $material_name = $category["category_name"].'-'.$brand["brand_name"].'-'.$specification["specification"];
                                    }

                                    $unit = isset($unit) ? $unit->unit_name : "";

                                    if ($value4['shutterwork_material']) {
                                        if($value4['shutterwork_material'] == $value4['carcass_material']){
                                            $item_list .= $smaterial_name;
                                        }else{
                                            $item_list .= $smaterial_name. " , " .$cmaterial_name;
                                        }
                                        
                                    } 

                                    if ($value4["material"]) {
                                        $item_list .= $material_name;
                                    }
                                    
                                    
                                    
                                    $item_list .= '</td>';
                                    if($value4['quantity_nos']!=''){
                                        $item_list .='<td>'."nos".'</td>';
                                    }else{
                                        $item_list .='<td>'.$unit.'</td>';
                                    }
                                    if($value4['quantity_nos']!=''){
                                        $item_list .='<td>'.$value4['quantity_nos'].'</td>';
                                    }else{
                                        $item_list .='<td>'.$value4['quantity'].'</td>';
                                    }
                                    $item_list .='<td class="text-right">'.$value4['mrp'].'</td>';
                                    $item_list .='<td class="text-right">'.$value4['amount_after_discount'].'</td>';
                                    
                                    $item_list .='</tr>';

                            }  
                            if (!empty($wrktypeitemArray)) {
                                
                            } $k++;
                        }
                        
                        $i++;
                       
                    }
                   $se++; 
                
                } ?>

<?php
            $item_list_type7 = "";
                $sql = 'SELECT * FROM `jp_sales_quotation` '
                        . 'WHERE `length` IS NOT NULL
                         AND `width` IS NOT NULL LIMIT 1';
                $length_or_width_exist = Yii::app()->db->createCommand($sql)->queryAll();
                $sql = 'SELECT * FROM `jp_quotation_section` '
                        . 'WHERE `qtn_id` = ' . $qtid
                        . ' AND revision_no = "' . $rev_no . '"';
                $sectionArray = Yii::app()->db->createCommand($sql)->queryAll();
                $i = 1;
                $secletters = range('A', 'Z');
                $se = 0;
                foreach ($sectionArray as $key => $value) {
                    $sql = 'SELECT * FROM `jp_quotation_gen_category` '
                            . 'WHERE `qid` = ' . $qtid . ' AND `section_id` = ' . $value["id"]
                            . ' AND revision_no = "' . $rev_no . '"';
                   
                    $subsectionArray = Yii::app()->db->createCommand($sql)->queryAll();
                    $unit = Unit::model()->findByPk($value['unit']);
                    $secsum = Controller::getSectionSum($value['id'], $qtid, $rev_no);
                    $subsectionArraySpan = empty($subsectionArray) ? '2' : empty($length_or_width_exist)? '7': '9';
                    $unit = isset($unit) ? $unit->unit_name : "";
                    $length = (floatval($value["length"])>0)?floatval($value["length"]):$value["length"];
                    $width = (floatval($value['width'])>0)?floatval($value['width']):$value['width'];
            

                    $item_list_type7 .= '<tr>
                           ';
                     $item_list_type7 .= '<th colspan="'.$subsectionArraySpan.'" style="background:#c00000;color:#fff;">'.$value["section_name"].'</th>';
                        if (empty($subsectionArray)) {
                            $item_list_type7 .= '<td style="background:#c00000;color:#fff;">'.$unit.'</td>';
                            if(!empty($length_or_width_exist)){
                                $item_list_type7 .= '<td style="background:#c00000;color:#fff;">'.$length.'</td>';
                                $item_list_type7 .= '<td style="background:#c00000;color:#fff;">'.$width.'</td>';
                            }
                            $item_list_type7 .= '<td style="background:#c00000;color:#fff;">'.$value['quantity_nos'] .'</td>';
                            $item_list_type7 .= '<td style="background:#c00000;color:#fff;">'.$value['mrp'].'</td>';
                            $item_list_type7 .= '<td style="background:#c00000;color:#fff;">'.$value['amount_after_discount'].'</td>';
                            
                         } 
                        
                     $item_list_type7 .= '</tr>';
                   
                    
                    foreach ($subsectionArray as $key => $value1) {
                       $sql = 'SELECT * FROM `jp_sales_quotation` WHERE `parent_type` = 0 '
                                . ' AND `parent_id` = ' . $value1['id']
                                . ' AND revision_no="' . $rev_no . '" AND `deleted_status` = 1';
                        $itemArray = Yii::app()->db->createCommand($sql)->queryAll();

                        $sql = 'SELECT * FROM `jp_quotation_gen_worktype` '
                                . ' WHERE `qid` = ' . $value1["qid"]
                                . ' AND `section_id` = ' . $value1["section_id"]
                                . ' AND `category_label_id` = ' . $value1["id"]
                                . ' AND revision_no = "' . $rev_no . '"';
                        $subitemArray = Yii::app()->db->createCommand($sql)->queryAll();
                        $unit = Unit::model()->findByPk($value1['unit']);
                        $subitemArraySpan = (empty($itemArray)) && (empty($subitemArray)) ? '1' : empty($length_or_width_exist)? '6': '8';
                        $unit = isset($unit) ? $unit->unit_name : "";
                        $length = (floatval($value1["length"])>0)?floatval($value1["length"]):$value1["length"];
                        $width = (floatval($value1['width'])>0)?floatval($value1['width']):$value1['width'];
                        $item_list_type7 .= '<tr>
                            <th  style="background:#c5d9f1;">'.$i.'</th>';
                            $item_list_type7 .= '<th colspan="'.$subitemArraySpan.'" style="background:#c5d9f1;">'.$value1['category_label'].'</th>';
                             if ((empty($itemArray)) && (empty($subitemArray))) { 
                                $item_list_type7 .= '<td  style="background:#c5d9f1;">'.$unit.'</td>';
                                if(!empty($length_or_width_exist)){
                                    $item_list_type7 .= '<td  style="background:#c5d9f1;">'.$length.'</td>';
                                    $item_list_type7 .= '<td  style="background:#c5d9f1;">'.$width.'</td>';
                                }
                                $item_list_type7 .= '<td  style="background:#c5d9f1;">'.$value1['quantity_nos'].'</td>';
                                $item_list_type7 .= '<td  style="background:#c5d9f1;">'.$value1['mrp'].'</td>';
                                $item_list_type7 .= '<td  style="background:#c5d9f1;">'.$value1['amount_after_discount'].'</td>';
                                
                             }                                                                                       
                             $item_list_type7 .= '</tr>';

                        
                        foreach ($itemArray as $key => $value2) {
                            $span = empty($length_or_width_exist)? '7': '9';
                            $item_list_type7 .= '<tr>
                                <td>'.'2*'.'</td>
                                <td colspan="'.$span.'">'.$value2['worktype_label'].'</td>
                            </tr>';

                         }
                        
                        
                        $letters = range('a', 'z');
                        $k = 0;
                        $subsection_total =0;
                        foreach ($subitemArray as $key => $value3) {
                            $sql = 'SELECT * FROM `jp_sales_quotation` '
                                    . 'WHERE `parent_type` = 1 AND `parent_id` = ' . $value3['id']
                                    . ' AND revision_no="' . $rev_no . '" AND `deleted_status` = 1';
                            $wrktypeitemArray = Yii::app()->db->createCommand($sql)->queryAll();
                            $unit = Unit::model()->findByPk($value3['unit']);
                            $wrktypeitemArray_span = empty($wrktypeitemArray) ? '1' : empty($wrktypeitemArray)? '1': '7';
                            $unit = isset($unit) ? $unit->unit_name : "";
                            $length = (floatval($value3["length"])>0)?floatval($value3["length"]):$value3["length"];
                            $width = (floatval($value3['width'])>0)?floatval($value3['width']):$value3['width'];
                            //$treeData = $this->buildTreeData($wrktypeitemArray);
                            
                            $item_list_type7 .= '<tr class="td_group">';
                            $item_list_type7 .= '<th>'.$letters[$k].'</th>';
                            $item_list_type7 .= '<td colspan="'.$wrktypeitemArray_span.'"><b>'.$value3['worktype_label'].'</b></td>';
                                if (empty($wrktypeitemArray)) { 
                                    if($value3['quantity_nos']!=''){
                                        $item_list_type7 .='<td>'."nos".'</td>';
                                    }else{
                                        $item_list_type7 .='<td>'.$unit.'</td>';
                                    }
                                    if(!empty($length_or_width_exist)){
                                        $item_list_type7 .= '<td>'.$length.'</td>';
                                        $item_list_type7 .= '<td>'.$width.'</td>';
                                    }
                                    if($value3['quantity_nos']!=''){
                                        $item_list_type7 .='<td>'.$value3['quantity_nos'].'</td>';
                                    }else{
                                        $item_list_type7 .='<td>'.$value3['quantity'].'</td>';
                                    }
                                    $item_list_type7 .= '<td>'.$value3['mrp'].'</td>';
                                    $item_list_type7 .= '<td>'.$value3['amount_after_discount'].'</td> ';                                                   
                                 }
                                  
                                 $item_list_type7 .= '<td style="border:none;"></td>';
                                 
                                 $item_list_type7 .= '</tr>';
                            if ($value3['description'] != "") { 
                                $span = empty($length_or_width_exist)? '6': '8';
                                $item_list_type7 .= '<tr class="td_group">
                                    <th>*</th>
                                    <td colspan="'.$span.'">'.$value3['description'].'</td>
                                    
                                </tr>';
                               
                            }
                            $sum = 0;
                            $item_count = 0;
                            foreach ($wrktypeitemArray as $key => $value4) {
                                $item_count += 1;
                                $sum += $value4['amount_after_discount'];
                                $subsection_total +=  $value4['amount_after_discount'];
                                $unit = Unit::model()->findByPk($value4['unit']);

                                $desc = ($value4['description'] !="")?" - ".$value4['description']:"";
                                if($value4['description']=="" && ($value4['shutterwork_description'] !="" || $value4['carcass_description']!="")){
                                    $desc = " - <b>Shutter:</b> ".$value4['shutterwork_description']." , <b>Carcass:</b> ".$value4['carcass_description'];
                                } 
                                 
                                
                                $item_list_type7 .= '<tr class="td_group">';
                                $item_list_type7 .= '<th>'.'*'.'</th>';
                                
                                       
                                    $item_master = QuotationItemMaster::model()->findByPk($value4['item_id']);
                                   
                                    $unit = isset($unit) ? $unit->unit_name : "";
                                    $length = (floatval($value4["length"])>0) ? floatval($value4["length"]):$value4["length"];
                                    $width = (floatval($value4['width'])>0)?floatval($value4['width']):$value4['width'];

                                   
                                    if($value4['item_name']!=""){
                                        $item_list_type7 .= '<td>'.$value4['item_name'].'</td>';
                                    }else{
                                        $item_list_type7 .= '<td>'.$value4['worktype_label'].$desc.'</td>';
                                    }  
                                    if($value4['quantity_nos']!=''){
                                        $item_list_type7 .='<td>'."nos".'</td>';
                                    }else{
                                        $item_list_type7 .='<td>'.$unit.'</td>';
                                    }
                                    if(!empty($length_or_width_exist)){
                                        $item_list_type7 .= '<td>'.$length.'</td>';
                                        $item_list_type7 .= '<td>'.$width.'</td>';
                                    }
                                    if($value4['quantity_nos']!=''){
                                        $item_list_type7 .='<td>'.$value4['quantity_nos'].'</td>';
                                    }else{
                                        $item_list_type7 .='<td>'.$value4['quantity'].'</td>';
                                    }
                                    
                                    $item_list_type7 .='<td class="text-right">'.$value4['mrp'].'</td>';
                                    $item_list_type7 .='<td class="text-right">'.$value4['amount_after_discount'].'</td>';
                                    if(($item_count==count($wrktypeitemArray)) && $k==count($subitemArray)-1){
                                        $item_list_type7 .='<td class="text-right" style="border-top:none;">'.Controller::money_format_inr($subsection_total , 2).'</td>';
                                
                                    }else{
                                        $item_list_type7 .='<td style="border:none;"></td>';
                                    }
                                    $item_list_type7 .='</tr>';

                            }       
                            if (!empty($wrktypeitemArray)) {
                                
                            } $k++;
                        }
                        
                        $i++;
                       
                    }
                   $se++; 
                
                } ?>


<?php
/* d2r*/
            $item_list_type7_d2r = "";
                $sql = 'SELECT * FROM `jp_sales_quotation` '
                        . 'WHERE `length` IS NOT NULL
                         AND `width` IS NOT NULL 
                         AND `master_id` = ' . $qtid
                        . ' AND revision_no = "' . $rev_no . '"
                         LIMIT 1';
                $length_or_width_exist = Yii::app()->db->createCommand($sql)->queryAll();
                $sql = 'SELECT * FROM `jp_quotation_section` '
                        . 'WHERE `qtn_id` = ' . $qtid
                        . ' AND revision_no = "' . $rev_no . '"';
                $sectionArray = Yii::app()->db->createCommand($sql)->queryAll();
                $i = 1;
                $secletters = range('A', 'Z');
                $se = 0;
                foreach ($sectionArray as $key => $value) {
                    $sql = 'SELECT * FROM `jp_quotation_gen_category` '
                            . 'WHERE `qid` = ' . $qtid . ' AND `section_id` = ' . $value["id"]
                            . ' AND revision_no = "' . $rev_no . '"';
                   
                    $subsectionArray = Yii::app()->db->createCommand($sql)->queryAll();
                    $unit = Unit::model()->findByPk($value['unit']);
                    $secsum = Controller::getSectionSum($value['id'], $qtid, $rev_no);
                    $subsectionArraySpan = empty($subsectionArray) ? '2' : empty($length_or_width_exist)? '7': '9';
                    $unit = isset($unit) ? $unit->unit_name : "";
                    $length = (floatval($value["length"])>0)?floatval($value["length"]):$value["length"];
                    $width = (floatval($value['width'])>0)?floatval($value['width']):$value['width'];
            

                    $item_list_type7_d2r .= '<tr>
                           ';
                     $item_list_type7_d2r .= '<th colspan="'.$subsectionArraySpan.'" style="background:#c00000;color:#fff;">'.$value["section_name"].'</th>';
                        if (empty($subsectionArray)) {
                            $item_list_type7_d2r .= '<td style="background:#c00000;color:#fff;">'.$unit.'</td>';
                            if(!empty($length_or_width_exist)){
                                $item_list_type7_d2r .= '<td style="background:#c00000;color:#fff;">'.$length.'</td>';
                                $item_list_type7_d2r .= '<td style="background:#c00000;color:#fff;">'.$width.'</td>';
                            }
                            $item_list_type7_d2r .= '<td style="background:#c00000;color:#fff;">'.$value['quantity_nos'] .'</td>';
                            $item_list_type7_d2r .= '<td style="background:#c00000;color:#fff;">'.$value['mrp'].'</td>';
                            $item_list_type7_d2r .= '<td style="background:#c00000;color:#fff;">'.$value['amount_after_discount'].'</td>';
                            
                         } 
                        
                     $item_list_type7_d2r .= '</tr>';
                   
                    
                    foreach ($subsectionArray as $key => $value1) {
                       $sql = 'SELECT * FROM `jp_sales_quotation` WHERE `parent_type` = 0 '
                                . ' AND `parent_id` = ' . $value1['id']
                                . ' AND revision_no="' . $rev_no . '" AND `deleted_status` = 1';
                        $itemArray = Yii::app()->db->createCommand($sql)->queryAll();

                        $sql = 'SELECT * FROM `jp_quotation_gen_worktype` '
                                . ' WHERE `qid` = ' . $value1["qid"]
                                . ' AND `section_id` = ' . $value1["section_id"]
                                . ' AND `category_label_id` = ' . $value1["id"]
                                . ' AND revision_no = "' . $rev_no . '"';
                        $subitemArray = Yii::app()->db->createCommand($sql)->queryAll();
                        $unit = Unit::model()->findByPk($value1['unit']);
                        $subitemArraySpan = (empty($itemArray)) && (empty($subitemArray)) ? '1' : empty($length_or_width_exist)? '6': '8';
                        $unit = isset($unit) ? $unit->unit_name : "";
                        $length = (floatval($value1["length"])>0)?floatval($value1["length"]):$value1["length"];
                        $width = (floatval($value1['width'])>0)?floatval($value1['width']):$value1['width'];
                        $item_list_type7_d2r .= '<tr>
                            <th  style="background:#c5d9f1;">'.$i.'</th>';
                            $item_list_type7_d2r .= '<th colspan="'.$subitemArraySpan.'" style="background:#c5d9f1;">'.$value1['category_label'].'</th>';
                             if ((empty($itemArray)) && (empty($subitemArray))) { 
                                $item_list_type7_d2r .= '<td  style="background:#c5d9f1;">'.$unit.'</td>';
                                if(!empty($length_or_width_exist)){
                                    $item_list_type7_d2r .= '<td  style="background:#c5d9f1;">'.$length.'</td>';
                                    $item_list_type7_d2r .= '<td  style="background:#c5d9f1;">'.$width.'</td>';
                                }
                                $item_list_type7_d2r .= '<td  style="background:#c5d9f1;">'.$value1['quantity_nos'].'</td>';
                                $item_list_type7_d2r .= '<td  style="background:#c5d9f1;">'.$value1['mrp'].'</td>';
                                $item_list_type7_d2r .= '<td  style="background:#c5d9f1;">'.$value1['amount_after_discount'].'</td>';
                                
                             }                                                                                       
                             $item_list_type7_d2r .= '</tr>';

                        
                        foreach ($itemArray as $key => $value2) {
                            $span = empty($length_or_width_exist)? '7': '9';
                            $item_list_type7_d2r .= '<tr>
                                <td>'.'3*'.'</td>
                                <td colspan="'.$span.'">'.$value2['worktype_label'].'</td>
                            </tr>';

                         }
                        
                        
                        $letters = range('a', 'z');
                        $k = 0;
                        $subsection_total =0;
                        foreach ($subitemArray as $key => $value3) {
                            $sql = 'SELECT * FROM `jp_sales_quotation` '
                                    . 'WHERE `parent_type` = 1 AND `parent_id` = ' . $value3['id']
                                    . ' AND revision_no="' . $rev_no . '" AND `deleted_status` = 1';
                            $wrktypeitemArray = Yii::app()->db->createCommand($sql)->queryAll();
                            $unit = Unit::model()->findByPk($value3['unit']);
                            $wrktypeitemArray_span = empty($wrktypeitemArray) ? '1' : empty($length_or_width_exist)? '5': '7';
                            $unit = isset($unit) ? $unit->unit_name : "";
                            $length = (floatval($value3["length"])>0)?floatval($value3["length"]):$value3["length"];
                            $width = (floatval($value3['width'])>0)?floatval($value3['width']):$value3['width'];
                            //$treeData = $this->buildTreeData($wrktypeitemArray);
                            
                            $item_list_type7_d2r .= '<tr class="td_group">';
                            $item_list_type7_d2r .= '<th>'.$letters[$k].'</th>';
                            $item_list_type7_d2r .= '<td colspan="'.$wrktypeitemArray_span.'"><b>'.$value3['worktype_label'].'</b></td>';
                                if (empty($wrktypeitemArray)) { 
                                    if($value3['quantity_nos']!=''){
                                        $item_list_type7_d2r .='<td>'."nos".'</td>';
                                    }else{
                                        $item_list_type7_d2r .='<td>'.$unit.'</td>';
                                    }
                                    if(!empty($length_or_width_exist)){
                                        $item_list_type7_d2r .= '<td>'.$length.'</td>';
                                        $item_list_type7_d2r .= '<td>'.$width.'</td>';
                                    }
                                    if($value3['quantity_nos']!=''){
                                        $item_list_type7_d2r .='<td>'.$value3['quantity_nos'].'</td>';
                                    }else{
                                        $item_list_type7_d2r .='<td>'.$value3['quantity'].'</td>';
                                    }
                                    $item_list_type7_d2r .= '<td>'.$value3['mrp'].'</td>';
                                    $item_list_type7_d2r .= '<td>'.$value3['amount_after_discount'].'</td> ';                                                   
                                 }
                                  
                                 $item_list_type7_d2r .= '<td style="border:none;"></td>';
                                 
                                 $item_list_type7_d2r .= '</tr>';
                            if ($value3['description'] != "") { 
                                $span = empty($length_or_width_exist)? '6': '8';
                                $item_list_type7_d2r .= '<tr class="td_group">
                                    <th>*</th>
                                    <td colspan="'.$span.'">'.$value3['description'].'</td>
                                    
                                </tr>';
                               
                            }
                            $sum = 0;
                            $item_count = 0;
                            foreach ($wrktypeitemArray as $key => $value4) {
                                $item_count += 1;
                                $sum += $value4['amount_after_discount'];
                                $subsection_total +=  $value4['amount_after_discount'];
                                $unit = Unit::model()->findByPk($value4['unit']);

                                $desc = ($value4['description'] !="")?" - ".$value4['description']:"";
                                if($value4['description']=="" && ($value4['shutterwork_description'] !="" || $value4['carcass_description']!="")){
                                    $desc = " - <b>Shutter:</b> ".$value4['shutterwork_description']." , <b>Carcass:</b> ".$value4['carcass_description'];
                                } 
                                 
                                
                                $item_list_type7_d2r .= '<tr class="td_group">';
                                $item_list_type7_d2r .= '<th>'.'*'.'</th>';
                                
                                       
                                    $item_master = QuotationItemMaster::model()->findByPk($value4['item_id']);
                                   
                                    $unit = isset($unit) ? $unit->unit_name : "";
                                    $length = (floatval($value4["length"])>0) ? floatval($value4["length"]):$value4["length"];
                                    $width = (floatval($value4['width'])>0)?floatval($value4['width']):$value4['width'];
                                    
                                    $item_list_type7_d2r .='<td>'.$value4['worktype_label'].$desc.'</td>';
                                      
                                    if($value4['quantity_nos']!=''){
                                        $item_list_type7_d2r .='<td>'."nos".'</td>';
                                    }else{
                                        $item_list_type7_d2r .='<td>'.$unit.'</td>';
                                    }
                                    if(!empty($length_or_width_exist)){
                                        $item_list_type7_d2r .= '<td>'.$length.'</td>';
                                        $item_list_type7_d2r .= '<td>'.$width.'</td>';
                                    }
                                    if($value4['quantity_nos']!=''){
                                        $item_list_type7_d2r .='<td>'.$value4['quantity_nos'].'</td>';
                                    }else{
                                        $item_list_type7_d2r .='<td>'.$value4['quantity'].'</td>';
                                    }
                                    $item_list_type7_d2r .='<td class="text-right">'.$value4['mrp'].'</td>';
                                    $item_list_type7_d2r .='<td class="text-right">'.$value4['amount_after_discount'].'</td>';
                                    if(($item_count==count($wrktypeitemArray)) && $k==count($subitemArray)-1){
                                        $item_list_type7_d2r .='<td class="text-right" style="border-top:none;">'.Controller::money_format_inr($subsection_total , 2).'</td>';
                                
                                    }else{
                                        $item_list_type7_d2r .='<td style="border:none;"></td>';
                                    }
                                    $item_list_type7_d2r .='</tr>';

                            }       
                            if (!empty($wrktypeitemArray)) {
                                
                            } $k++;
                        }
                        
                        $i++;
                       
                    }
                   $se++; 
                
                } 
                ?>
        <?php
         $sql = 'SELECT * FROM jp_quotation_revision '
             . ' WHERE qid='.$qtid
             . ' AND revision_no="'.$rev_no.'"';
         $revision_model = Yii::app()->db->createCommand($sql)->queryRow();
         $total_tax = $revision_model['sgst_amount']+
         $revision_model['cgst_amount']+
         $revision_model['igst_amount'];

         $total_tax_percent = $revision_model['sgst_percent']+
         $revision_model['cgst_percent']+
         $revision_model['igst_percent'];
         
        $total_tax_inr = Controller::money_format_inr($total_tax, 2);
       
        
        //$total_amount = Controller::getMrpSum($qtid, $rev_no,NULL);
        $total_amount = Controller::getQuotationSum($qtid, $rev_no,NULL,1);
        $total_price_inr = Controller::money_format_inr($total_amount, 2);
        $total_offer_price = Controller::getQuotationSum($qtid, $rev_no,NULL);
        $offer_price_inr = Controller::money_format_inr($total_offer_price, 2);
        $material_discount =  $total_amount - $total_offer_price;
        $material_discount_inr = Controller::money_format_inr($material_discount, 2);

        $total_amoun_withtax = $total_offer_price + $total_tax;

        if($total_tax_percent >0) {
            $total_tax_percent_value = "(".$total_tax_percent."%)";;
        }else{
            $total_tax_percent_value = "";
        } 
        $total_amoun_withtax_inr = Controller::money_format_inr($total_amoun_withtax , 2); 
        
        $d2r_special_discount =$total_amoun_withtax*.91;        
        $d2r_special_discount_inr = Controller::money_format_inr($d2r_special_discount , 2); 
        $discount_value = $revision_model['discount_precent'];
        if($discount_value >0) {
            $discount_percent = "(".$discount_value."%)";;
        }else{
            $discount_percent = "";
        } 
        $discount_amount = ($total_amoun_withtax * $revision_model['discount_precent']) / 100;
        $discount_amount_inr = Controller::money_format_inr($discount_amount , 2); 

        $price_after_discount = $total_amount - $discount_amount;
        $price_after_discount_inr = Controller::money_format_inr($price_after_discount , 2);
        
        $total_after_discount = $total_amoun_withtax - $discount_amount;
        /*if($revision_model['total_after_additionl_discount']!=NULL){
            $total_after_discount = $revision_model['total_after_additionl_discount'];
        }*/
        $grand_total_after_discount = Controller::money_format_inr($total_after_discount , 2); 
        
$base_url = Yii::app()->theme->baseUrl;
$terms = Yii::app()->db->createCommand("SELECT * FROM `jp_terms_conditions` WHERE `template_id` = 1")->queryRow();
$terms_and_conditions = $terms['terms_and_conditions'];
$location = Location::model()->findByPk($model->location_id); 
$location_name = $location["name"];

$sales_ex ="";
if($model->sales_executive_id !=""){
    $salesExecutive = Salesexecutive::model()->findByPk($model->sales_executive_id);
    $sales_ex = $salesExecutive['name'];
}

$sales_ex_phone = "";
if($model->sales_executive_id !=""){
    $salesExecutive = Salesexecutive::model()->findByPk($model->sales_executive_id);
    $sales_ex_phone = $salesExecutive['phone'];
}

$sales_ex_designation ="";
if($model->sales_executive_id !=""){
    $salesExecutive = Salesexecutive::model()->findByPk($model->sales_executive_id);
    $sales_ex_designation = $salesExecutive['designation'];
}

$pdf_data = str_replace(
    array('{base_url}','{terms_and_conditions}','{location_name}', '{client}', 
    '{address}', '{phone_no}', '{invoice_no}', '{date_quotation}', '{material_specification}', '{quotation_galley_images}', 
    '{total_tax_percent_value}', '{total_tax_inr}', '{total_amoun_withtax_inr}', 
    '{offer_price_inr}', '{total_price_inr}', '{item_list}', 
    '{sales_executive_name}', '{sales_executive_phone}', '{email}'
    , '{sales_executive_designation}', '{discount_percent}', '{discount_amount}', '{item_list_type7}'
    , '{price_after_discount_inr}', '{grand_total_after_discount}', '{material_discount_inr}',
    '{company_name}', '{company_address}' ,'{company_phone}','{company_email}',
    '{estimator_name}','{estimator_phone}','{d2r_special_discount_inr}', '{item_list_type7_d2r}'
   ),
    array($base_url, $terms_and_conditions, $location_name, $client, 
    $model->address, $model->phone_no, $model->invoice_no, date('d-m-Y', strtotime($model->date_quotation)), $material_specification, $quotation_galley_images, 
    $total_tax_percent_value, $total_tax_inr, $total_amoun_withtax_inr, 
    $offer_price_inr, $total_price_inr, $item_list, 
    $sales_ex, $sales_ex_phone, $model->email
    , $sales_ex_designation, $discount_percent, $discount_amount, $item_list_type7,
    $price_after_discount_inr, $grand_total_after_discount, $material_discount_inr,
    $compmodel->name, $compmodel->address, $compmodel->phone, $compmodel->email_id,
    $estimator_name, $estimator_phone,$d2r_special_discount_inr, $item_list_type7_d2r
), $pdf_data);

$str = html_entity_decode($pdf_data, ENT_QUOTES, 'UTF-8');
eval('?>'.$str.'');
?>