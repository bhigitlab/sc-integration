<link rel="stylesheet" type="text/css" href="uploads/new_template/css/newstyle.css" />

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  
  <script>
  $( function() {
       $( "#datepicker" ).datepicker({dateFormat: 'dd-mm-yy'}).datepicker("setDate", new Date());
    
  } );
  </script>

<br/><br/>


<div class="invoicemaindiv" >


                 <a style="cursor:pointer; float:right;" id="download" class="save_btn1 pdf_excel" >
                 <div class="save_pdf"></div>SAVE AS PDF
                 </a>
                 
                 <a style="cursor:pointer; float:right;margin-right:2px;" id="download" class="excelbtn1 pdf_excel" >
                 <div class="save_pdf"></div>SAVE AS EXCEL
                 </a><br/><br/><br/>
                
 
 
 
 <form id="pdfvals1" method="post" action="<?php echo $this->createAbsoluteUrl('invoice/addperforma'); ?>">

	<div class="invoiceheader">
	  <img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/logo.png" style="height:100px;padding:5px;">
	  <textarea class="txtBox pastweek" name="details" placeholder="Please click to edit" style="float:right;width:50%;height:110px"/>
	  3rd Floor,Dr.Rajkrishnan's Building &#13;
	  Nort Fort Gate,Tripunithura,Kochi &#13;&#10;
	  Call:0484 4030077, 9995107007 &#13;
	  Mail:info@jpventures.in
	  </textarea>
	</div>
	<h2>PERFORMA INVOICE</h2>
	
	<div class="inv_client_det">
	<label>BILL TO : </label>
	<input type="text" value="<?php  echo ((isset($billto) && $billto != '') ? $billto : ''); ?>" class="txtBox pastweek" name="billto" placeholder="Please click to edit"/>
	<span class="error_message"><?php  echo ((isset($error['client_name'][0]) && $error['client_name'][0] != '') ? $error['client_name'][0] : ''); ?> </span>
	</div>
	
	<div class="rightdiv">
	<table border="1" style="width:100%;">
	<tr>
		<td>
		<label>DATE : </label>
		<input type="text" value="<?php   echo ((isset($date) && $date != '') ? date("d-m-Y", strtotime($date)) : '');  ?>" id="datepicker" class="txtBox pastweek" name="date"  placeholder="Please click to edit" >
		<span class="error_message"><?php  echo ((isset($error['date'][0]) && $error['date'][0] != '') ? $error['date'][0] : ''); ?> </span>
		</td>
		</tr>
	<tr>
		<td>
		<label>INVOICE NO : </label>
		<input type="text"  value="<?php  echo ((isset($invoiceno) && $invoiceno != '') ? $invoiceno : ''); ?>" class="txtBox pastweek" name="invoiceno" placeholder="Please click to edit"/>
		<span class="error_message"><?php  echo ((isset($error['inv_no'][0]) && $error['inv_no'][0] != '') ? $error['inv_no'][0] : ''); ?> </span>
		</td>
	</tr>
	</table>
	</div>
	<br><br><br><br>
	
	<div class="inv_client_det">
	<label>PROJECT NAME : </label>
	<input type="text" value="<?php  echo ((isset($project) && $project != '') ? $project : ''); ?>"  class="txtBox pastweek" name="project" placeholder="Please click to edit"/><br>
	<span class="error_message"><?php  echo ((isset($error['project_name'][0]) && $error['project_name'][0] != '') ? $error['project_name'][0] : ''); ?> </span>
	</div>
	<br>
	<br>
	
	<table border="1" class="table">
			<thead>
				<tr>
					<th>Sl.No</th>
					<th>Description</th>
					<th>Quantity</th>
					<th>Unit</th>
					<th>Rate</th>
					<th>Amount</th>
					<th></th>
				</tr>
			</thead>
			<tbody class="addrow">
				<?php
				if(!empty($sl_No))
				{
					
					foreach($sl_No as $k => $sl) 
					{
				?>
			
				<tr>
					<td><input type="text" value ="<?php echo $sl_No[$k];?>" class="txtBox pastweek" name="sl_No[]" placeholder="Please click to edit" readonly></td>
					<td><textarea class="txtBox pastweek" name="description[]"/><?php echo $description[$k];?></textarea></td>
					<td class="quantity"><input type="text" value ="<?php echo $quantity[$k];?>" class="txtBox pastweek quantity" name="quantity[]" placeholder="Please click to edit"/></td>
					<td><input type="text" value ="<?php echo $unit[$k];?>" class="txtBox pastweek" name="unit[]" placeholder="Please click to edit"/></td>
					<td><input type="text" value ="<?php echo $rate[$k];?>" class="txtBox pastweek rate" name="rate[]" placeholder="Please click to edit"/></td>
					<td><input type="text" value ="<?php echo $amount[$k];?>" class="txtBox pastweek amt" name="amount[]" placeholder="Please click to edit"/></td>
					<td><button class="removebtn">Remove</button></td>
				</tr>
				<?php
				}
				}
				else
				{
				?>
				<tr>
					<td><input type="text"  class="txtBox pastweek" name="sl_No[]" value ="1" placeholder="Please click to edit" readonly></td>
					<td><textarea class="txtBox pastweek" name="description[]"/></textarea></td>
					<td class="quantity"><input type="text" class="txtBox pastweek quantity" name="quantity[]" placeholder="Please click to edit"/></td>
					<td><input type="text" class="txtBox pastweek" name="unit[]" placeholder="Please click to edit"/></td>
					<td><input type="text" class="txtBox pastweek rate" name="rate[]" placeholder="Please click to edit"/></td>
					<td><input type="text" class="txtBox pastweek amt" name="amount[]" placeholder="Please click to edit"/></td>
					<td><button class="removebtn">Remove</button></td>
				</tr>
				
				<?php	
				}
				?>
				
				
			</tbody>
	</table>
	
	<div class="addcolumn">+</div>
	
	<table border="1" style="width:100%;">
		<tr>
			<td>SUB TOTAL :<input type="text" value="<?php  echo ((isset($subtot) && $subtot != '') ? $subtot : ''); ?>" class="txtBox pastweek" name="subtot" readonly=ture/></td>
			<td>FEES :<input type="text" value="<?php  echo ((isset($fees) && $fees != '') ? $fees : ''); ?>" class="txtBox pastweek" name="fees" placeholder="Please click to edit"/></td>
			<td>GRAND TOTAL :<input type="text" value="<?php  echo ((isset($grand) && $grand != '') ? $grand : ''); ?>" class="txtBox pastweek grand" name="grand" readonly=true />
			<span class="error_message"><?php  echo ((isset($error['amount'][0]) && $error['amount'][0] != '') ? $error['amount'][0] : ''); ?> </span></td>
		
		</tr>
	</table>
	<br><br>
  <center><input type="submit" name="performa" value="Submit" class="form_submit1"></center>
        
              
</form>
</div>



<style>
	.error_message{
		color:red;
	}
 
	a.pdf_excel {
    background-color: #6a8ec7;
    display:inline-block;
	padding:8px;
    color: #fff;
    border: 1px solid #6a8ec8;
}
</style>


<script>

	
	$(document).ready(function() {
		
	$(document).on('change', 'input[name="fees"]', function(e){
		
	    var val = Number($(this).val()) + Number($('input[name="subtot"]').val());
		$('input[name="grand"]').val(val);
	});	
	
	$(document).on('change', '.table input[name="amount[]"],.table input[name="quantity[]"],.table input[name="rate[]"]', function(e){
	var v1 = '';
	var v2 = '';
	var v = '';
	
				$(this).closest('tr').find("input[name='rate[]']").each(function() {
				var va1 = this.value;
				v1 +=  va1;
				//alert(v1);
				});
				
				$(this).closest('tr').find("input[name='quantity[]']").each(function() {
				var va2 = this.value;
				v2 +=  va2;
				//alert(v2);
				
				});
			
			if (!(v1 == '' && v2 == '')) {
			//alert('hi');
			
			var v = v1*v2;
		    $(this).closest('tr').find("input[name='amount[]']").val(v); 
			}
			else
			{
			//alert('hi');
			$(this).closest('tr').find("input[name='amount[]']").val(''); 
			}
                        var val = 0;
		$('.table input[name="amount[]"]').each(function() {
		val += Number($(this).val());
		});
		$('input[name="subtot"]').val(val);
		val = val+Number($('input[name="fees"]').val());
		$('input[name="grand"]').val(val);
			
	});
	
	
	$(document).on('focus', '.table input[name="amount[]"]', function(e){
		//alert('hi');
		var val = 0;
		$('.table input[name="amount[]"]').each(function() {
		val += Number($(this).val());
		});
		$('input[name="subtot"]').val(val);
		val = val+Number($('input[name="fees"]').val());
		$('input[name="grand"]').val(val);
		});
		
	});
	
	
	
	$(document).on('change', 'input[name="invoiceno"]', function(){
		
		var inv_no = $(this).val();
		
			$.ajax({
				
				type: "POST",
				url: "<?php echo $this->createUrl('invoice/checkPerformano') ?>",
				data: {"inv_no": inv_no}, 
				dataType: "json",
				success:function(response){
					
					if(response.response == "fail"){
						
					alert("Invoice Number is already in use!");
					
					
					}
			
				
				}
        
	});
		
		
	});
	
	
	
	
	
	
	


    $(function () {

        $('.save_btn1').click(function () {
			
			$("form#pdfvals1").attr('action', "<?php echo $this->createAbsoluteUrl('invoice/saveperformainvoice'); ?>"); 
            $("form#pdfvals1").submit();

        });
        
         $('.excelbtn1').click(function () {
			
			$("form#pdfvals1").attr('action', "<?php echo $this->createAbsoluteUrl('invoice/exportPerforma'); ?>"); 
            $("form#pdfvals1").submit();

        });
        
         $('.form_submit1').click(function () {
			
			$("form#pdfvals1").attr('action', "<?php echo $this->createAbsoluteUrl('invoice/addperforma'); ?>"); 
            $("form#pdfvals1").submit();

        });
        
        
       
          
        
       
        
       
        $('.addcolumn').on('click', function () {
			
			/*var val = 1;
			$("table.table tr:last input[name='sl_No[]']").each(function() {
			val += Number($(this).val());
			}); */
			
			//alert(val);
			$('.addrow').append('<tr><input type="hidden" class="txtBox pastweek" name="ids[]" /><td><input type="text" class="txtBox pastweek" name="sl_No[]" placeholder="Please click to edit" readonly></td><td><textarea class="txtBox pastweek" name="description[]" value=""/></textarea></td><td><input type="text" class="txtBox pastweek" name="quantity[]" placeholder="Please click to edit" value=""/></td><td><input type="text" class="txtBox pastweek" name="unit[]" placeholder="Please click to edit" value=""/></td><td><input type="text" class="txtBox pastweek" name="rate[]" placeholder="Please click to edit" value=""/></td><td><input type="text" class="txtBox pastweek amt" name="amount[]" placeholder="Please click to edit" value=""/></td><td><button class="removebtn">Remove</button></td></tr>');		
		//	$("table.table tr:last input[name='sl_No[]']").val(val);
			
		
		 });
		 
		   $('table').on('click', '.removebtn', function(e){
			$(this).closest('tr').find('input[name="amount[]"]').each(function() {
                            $(this).closest('tr').remove();
                            var re = this.value;
                            var v1 = $('input[name="subtot"]').val();
                            $('input[name="subtot"]').val(eval(v1)-eval(re));
                        });
		});
		
		$(document).on("click", ".addcolumn, .removebtn", function(){
			  //alert('hi');
			
			$("table.table  input[name='sl_No[]']").each(function(index,element) {               
            $(element).val(index + 1); 
			});
		});
		 
		 
		
        
       


        $(".inputSwitch span").on("click", function() {

            var $this = $(this);

            $this.hide().siblings("input").val($this.text()).show();

        });

        $(".inputSwitch input").bind('blur', function() {

                   var $this = $(this);

                   $(this).attr('value', $(this).val());

                   $this.hide().siblings("span").text($this.val()).show();

        }).hide();

 

    });
</script>
