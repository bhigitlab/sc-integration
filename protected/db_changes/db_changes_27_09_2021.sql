ALTER TABLE `jp_sales_quotation` ADD `sub_status` TINYINT NOT NULL DEFAULT '0' COMMENT '1=\'subitem\',0=\'item\'' AFTER `maintotal_withtax`;

ALTER TABLE `jp_sales_quotation` ADD `subitem_label` VARCHAR(250) NULL AFTER `sub_status`;