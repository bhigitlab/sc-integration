<?php
/* @var $this ProjectsController */
/* @var $model Projects */
/* @var $form CActiveForm */
?>



<div class="">
	
	<div class="">
		<?php $form=$this->beginWidget('CActiveForm', array(
			'action'=>Yii::app()->createUrl($this->route),
			'method'=>'get',
		)); ?>
		<?php echo $form->label($model,'Filter By'); ?>
		<?php echo $form->textField($model,'name',array('size'=>30,'maxlength'=>100,'placeholder' => 'Name')); ?>

			<?php echo CHtml::submitButton('Go'); ?>
			<?php echo CHtml::resetButton('Clear', array('onclick' => 'javascript:location.href="'. $this->createUrl('newlist').'"')); ?> 
		<?php $this->endWidget(); ?>
	</div>
</div>
				
