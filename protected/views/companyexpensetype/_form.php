<?php
/* @var $this CompanyExpenseTypeController */
/* @var $model CompanyExpenseType */
/* @var $form CActiveForm */
?>

<div class="form">

<?php  $form = $this->beginWidget('CActiveForm', array(
        'id'=>'companyexpensetype-form',
        'enableAjaxValidation' => true,
        'enableClientValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true,
            'validateOnType' => false,),
    )); ?>

    <div class="panel-body">
         <div class="row">
            <div class="col-md-4">
                <?php echo $form->labelEx($model,'name'); ?>
                <?php echo $form->textField($model,'name',array('size'=>50,'maxlength'=>50,'class'=>'form-control')); ?>
                <?php echo $form->error($model,'name'); ?>
            </div>
            <?php if($dailyexp==0){?>
       
            <div class="col-md-4">
                <?php echo $form->labelEx($model, 'type'); ?>
                <div class="radio_btn">
                    <?php echo $form->radioButtonList($model, 'type', array(0 => 'Receipt',1 => 'Payment','2'=>'Both'), array('separator' => '')); ?>                   
                </div>
                <?php echo $form->error($model, 'type'); ?>
            </div>
        
         <?php }?>
            <div class="col-md-4 save-btnHold">
                <label style="display:block;">&nbsp;</label>
                <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'btn btn-info btn-sm submit')); ?>
                <?php if(!$model->isNewRecord){
                    echo CHtml::ResetButton('Close', array('onclick'=>'closeaction(this,event);','class'=>'btn btn-sm'));  
                    } 
                    else{
                       echo CHtml::ResetButton('Reset', array('style'=>'margin-right:3px;','class'=>'btn btn-default btn-sm')); 
                       echo CHtml::ResetButton('Close', array('onclick'=>'closeaction(this,event);','class'=>'btn btn-sm'));  
                    }
                 ?>
            </div>
        </div>
            
    </div>

<?php $this->endWidget(); ?>

</div>
<script>
    function countOfLetters(str) {
            var letter = 0;
            for (i = 0; i < str.length; i++) {
                if ((str[i] >= 'A' && str[i] <= 'Z')
                    || (str[i] >= 'a' && str[i] <= 'z'))
                    letter++;
            }
            
            return letter;
        }
      

    $("#Companyexpensetype_name").keyup(function () { 
               
        var alphabetCount =  countOfLetters(this.value);  
        var message ="";      
        var disabled=false;

        if(alphabetCount < 1){
            var message = "Invalid Daily Expense Type";
            var disabled=true;           
        } 

        $(this).siblings(".errorMessage").show().html(message).addClass('d-block');                
        $(".submit").attr('disabled',disabled);
    });
    </script>
