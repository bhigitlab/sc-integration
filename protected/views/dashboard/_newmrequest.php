<div class="parent_row">
    <div class="row">
        <div class="col-md-1 logo_sec">
            <button class="btn_logo">MR</button>
        </div>
        <div class="col-md-9 content_sec">
            <div class="row  clearfix">
                <div class="col-md-4">
                    <div class="list-item">
                    Requisition No : <b><?php echo $data['requisition_no']; ?></b>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="list-item pull-right">
                        <label class="w-auto">PO Number:</label>
                        <span><?php 
                        $purchase_id = $data['purchase_id'];
                        echo  (isset($purchase_id) ?$purchase_name = MaterialRequisition::model()->getPurchaseName($purchase_id) :'Nil');
                        ?></span>
                    </div>
                </div> 
                <div class="col-md-3">
                    <div class="list-item pull-right">
                        <label class="w-auto">Billing Status: </label>
                        <span><?php 
                        $purchase_id = $data['purchase_id'];
                        echo  (isset($purchase_id) ?$purchase_name = MaterialRequisition::model()->getBillingStatus($purchase_id) :'Nil');
                        ?></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-2">
            <div class="list-item text-right act_btn">
            <a href="<?php echo Yii::app()->createUrl("materialRequisition/view", array('id' => $data['requisition_id'])) ?>" class="btn btn-xs btn_view btn_sec" target="_blank">View</a>
            </div>
        </div>
    </div>
</div>