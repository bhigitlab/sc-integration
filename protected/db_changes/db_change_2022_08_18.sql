ALTER TABLE `jp_purchase` ADD `expected_delivery_date` DATE NULL AFTER `purchase_type`;
ALTER TABLE `jp_log` CHANGE `log_action` `log_action` ENUM('update','delete','add') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;

ALTER TABLE `jp_warehousereceipt_items` CHANGE `warehousereceipt_itemid` `warehousereceipt_itemid` INT(11) NULL DEFAULT NULL;

RENAME TABLE `defect_return` TO `jp_defect_return`;
RENAME TABLE `project_assign` TO `jp_project_assign`;
RENAME TABLE `withdrawals` TO `jp_withdrawals`;