<?php
$user = Users::model()->findByPk(Yii::app()->user->id);
$tblpx  = Yii::app()->db->tablePrefix;
$arrVal = explode(',', $user->company_id);
$newQuery = "";
$newQuery1 = "";
foreach($arrVal as $arr) {
    if ($newQuery) $newQuery .= ' OR';
    if ($newQuery1) $newQuery1 .= ' OR';
    $newQuery .= " FIND_IN_SET('".$arr."', id)";
    $newQuery1 .= " FIND_IN_SET('".$arr."', company_id)";
}
?>
<script>
    $( function() {
        $( "#date_from" ).datepicker({
            dateFormat: 'yy-mm-dd',
            maxDate: $("#date_to").val()
        });
        $( "#date_to" ).datepicker({
            dateFormat: 'yy-mm-dd',
        });
    });

</script>
<div class="page_filter clearfix custom-form-style">
    <?php $form=$this->beginWidget('CActiveForm', array(
        'action'=>Yii::app()->createUrl($this->route),
        'method'=>'GET',
    )); ?>
    <div class="row">
    <?php
        $vendorsorsubcontractors = $model = Yii::app()->db->createCommand("SELECT vendor_id,name FROM {$tblpx}vendors WHERE (".$newQuery1.") order by name")->queryAll();
        ?>
        <div class="col-md-3 col-sm-6 col-xs-12 filter_elem padding-y-5">
            <select name="Bills[client_type]" style="width:200px;padding:2.5px 0px;" class="form-control js-example-basic-single2 client_type">
                <option value="">Select Vendor /Subcontractor</option>
               
                <option value="1" <?php if( isset($vendor_id) && $vendor_id != ''){ ?> Selected<?php }  ?> >Vendor</option>
                <option value="2" <?php if( isset($subcontractor_id) && $subcontractor_id !=''){ ?> Selected<?php }  ?> >Subcontractor</option>
            </select>
        </div>
        
        <?php
        $vendors = $model = Yii::app()->db->createCommand("SELECT vendor_id,name FROM {$tblpx}vendors WHERE (".$newQuery1.") order by name")->queryAll();
        ?>
        <div class="col-md-3 col-sm-6 col-xs-12 filter_elem padding-y-5" id="vendor_div"  style="display:none;">
            <select name="Bills[vendor]" style="width:200px;padding:2.5px 0px;" class="form-control js-example-basic-single">
                <option value="">Select Vendor</option>
                <?php foreach($vendors as $ven) { ?>
                <option value="<?= $ven['vendor_id']?>" <?php if( isset($vendor_id) && $ven['vendor_id']==$vendor_id){ ?> Selected<?php }  ?> ><?= $ven['name']?></option>
                <?php } ?>
            </select>
        </div>
        <?php
        $subcontractors = $model = Yii::app()->db->createCommand("SELECT subcontractor_id,subcontractor_name FROM {$tblpx}subcontractor WHERE (".$newQuery1.") order by subcontractor_name")->queryAll();
        ?>
        <div class="col-md-3 col-sm-6 col-xs-12 filter_elem padding-y-5" id="subcontractor_div" style="display:none;">
            <select name="Bills[subcontractor_id]" style="width:200px;padding:2.5px 0px;" class="form-control js-example-basic-single1" >
                <option value="">Select Subcontractor</option>
                <?php foreach($subcontractors as $sub) { ?>
                <option value="<?= $sub['subcontractor_id']?>" <?php if( isset($subcontractor_id) && $sub['subcontractor_id']==$subcontractor_id){ ?> Selected<?php }  ?> ><?= $sub['subcontractor_name']?></option>
                <?php } ?>
            </select>
        </div>
       
    
        <!-- <div class="col-md-3 col-sm-6 col-xs-12 filter_elem padding-y-5 d-flex">
            
            <?php            
            echo CHtml::textField('Bills[day_interval]', (($day_interval!='') ?  $day_interval :''), array("id" => "day_interval",'style'=>'display:inline-block', "class" => "form-control","placeholder" =>'Days Interval','autocomplete'=>'off'));
            ?>
        </div> -->
        <div class="col-md-3 col-sm-6 col-xs-12 filter_elem padding-y-5 d-flex">
            <?php            
            echo CHtml::dropDownList('Bills[day_range]',
                (($day_range!='') ?  $day_range :'Select Range'),
                array(
                    '1'=>'1 to 15',
                    '2'=>'16 to 30',
                    '3'=>'31 to 45',
                    '4'=>'above 45',
                ),
                array('empty'=>'Select Range',"class" => "form-control date-range")
            );                            
            ?>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12 filter_elem padding-y-5 d-flex">
            <label class="filter_label">From:</label>
            <?php
            echo CHtml::textField('Bills[date_from]', (($date_from!='') ?  date('Y-m-d', strtotime($date_from)) :''), array("id" => "date_from",'style'=>'display:inline-block', "class" => "form-control", "readonly" => true,"placeholder" =>'Date From','autocomplete'=>'off'));
            ?>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12 filter_elem padding-y-5 d-flex">
            <label class="filter_label">To:</label>
            <?php echo CHtml::textField('Bills[date_to]', (($date_to!='') ? date('Y-m-d', strtotime($date_to)) :''), array("id" => "date_to","class" => "form-control",'style'=>'display:inline-block',  "readonly" => true,"placeholder" =>'Date To','autocomplete'=>'off')); ?>
        </div>

        <div class="col-md-3 col-sm-6 col-xs-12 filter_elem padding-y-5 filter_btns ">
            <div class="">
                <?php echo CHtml::submitButton('Go'); ?>
                <?php echo CHtml::resetButton('Clear', array('onclick' => 'javascript:location.href="'. $this->createUrl('ageingvendorReport').'"')); ?>
            </div>
        </div>
    </div>
    <?php $this->endWidget(); ?>
</div>
<style>
    select, input{margin-right:5px;}
    .topgap{margin-top: 10px;}
    .select2-container--default .select2-selection--multiple {border: 1px solid #ccc;}
    .filter_elem{margin-right:0px;}
    input::placeholder ,select::placeholder{
    color: #555 !important;
    }

</style>
<script>
    $(function(){
        $('#category_id').select2({
            width: '100%',
            multiple:true,
            placeholder:'Select Item Specification',
        });
        $('#expense_head').select2({
            width: '100%',
            multiple:true,
            placeholder:'Select Expense Head',
        });
        $('.js-example-basic-single').select2({
            width: '100%',
            
            placeholder:'Select Vendor',
        });
        $('.js-example-basic-single1').select2({
            width: '100%',
            
            placeholder:'Select Subcontractor',
        });
        $('.js-example-basic-single2').select2({
            width: '100%',
            
            placeholder:'Select Type',
        });
        $('.date-range').select2({
            width: '100%',
            
            placeholder:'Select Range',
        });
        let type = $('.client_type').val();
        if(type==1){
            $("#vendor_div").show();
            $("#subcontractor_div").hide();
        }
        if(type==2){
            $("#vendor_div").hide();
            $("#subcontractor_div").show();
        }
    });
    $(document).ready(function(){
        $('.client_type').change(function(){
            let type = $('.client_type').val();
            alert(type);
            if(type==1){
            $("#vendor_div").show();
            $("#subcontractor_div").hide();
            }
            if(type==2){
                $("#vendor_div").hide();
                $("#subcontractor_div").show();
            }

        });
    })
    
</script>
