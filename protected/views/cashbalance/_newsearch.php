<?php
/* @var $this ProjectsController */
/* @var $model Projects */
/* @var $form CActiveForm */
?>


<div class="">
	
	<div class="">
		<?php $form=$this->beginWidget('CActiveForm', array(
			'action'=>Yii::app()->createUrl($this->route),
			'method'=>'get',
		)); ?>
		<?php echo $form->label($model,'Cashbalance Type'); ?>
		<?php // echo $form->textField($model,'cashbalance_type',array('size'=>25,'maxlength'=>100,'placeholder' => 'Name')); ?>
		<?php echo $form->dropDownList($model, 'cashbalance_type', CHtml::listData(Status::model()->findAll(array('order' => 'caption ASC','condition' => 'status_type= "cashbalance_type"')), 'sid', 'caption'), array('empty' => 'Select Type', 'class' => 'type','style'=>'padding:2.5px')); ?>


			<?php echo CHtml::submitButton('Go'); ?>

		<?php $this->endWidget(); ?>
	</div>
</div>
				
