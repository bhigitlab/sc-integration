<?php

class ReportsController extends Controller
{

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array(
                'allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('transferreport', 'warehouseandpurchaseReportData', 'getWarehouseTo', 'getWarehouseFrom', 'stockvaluereport', 'stockStatusReport', 'GetItemCategory', 'GetParent', 'detail','pendingbills','stockCorrectionReport','stockValueReportdetail','getProjectlist','PendingDeliveries'),
                'users' => array('@'),
            ),
            array(
                'deny', // deny all users
                'users' => array('*'),
            ),
        );
    }
    public function actiongetWarehouseTo()
    {
        $warehouse_to = $_POST['warehouse_to'];
        $warehouse_from = $_POST['warehouse_from'];
        $warehouse_to_data_arr = array();
        $warehouse_to_result_array = array();
        $data_arr = array();
        $tblpx = Yii::app()->db->tablePrefix;
        if ($warehouse_from != "") {
            $warehouse_to_data_sql = "SELECT DISTINCT warehousedespatch_warehouseid_to FROM {$tblpx}warehousedespatch WHERE warehousedespatch_warehouseid = '" . $warehouse_from . "'";
            $warehouse_to_data = Yii::app()->db->createCommand($warehouse_to_data_sql)->queryAll();
            foreach ($warehouse_to_data as $warehouse_to_data_value) {
                if ($warehouse_to_data_value['warehousedespatch_warehouseid_to'] != '') {
                    $warehouse_to_data_arr[] = $warehouse_to_data_value['warehousedespatch_warehouseid_to'];
                }
            }
            $commom_warehouse = Warehouse::model()->findAll(
                array(
                    'select' => array('warehouse_id', 'warehouse_name'), 'condition' => 'project_id IS NULL',
                    'order' => 'warehouse_id DESC'
                )
            );
            $project_warehouse = Warehouse::model()->findAll(
                array(
                    'select' => array('warehouse_id', 'warehouse_name', 'project_id'), 'condition' => 'project_id IS NOT NULL',
                    'order' => 'warehouse_id DESC'
                )
            );
            foreach ($commom_warehouse as $common) {
                if (!empty($warehouse_to_data_arr)) {
                    if (in_array($common['warehouse_id'], $warehouse_to_data_arr)) {
                        $data_arr = array('id' => $common['warehouse_id'], 'text' => $common['warehouse_name'], 'group' => 'General');
                        array_push($warehouse_to_result_array, $data_arr);
                    }
                }
            }
            foreach ($project_warehouse as $project_ware) {
                if (!empty($warehouse_to_data_arr)) {
                    if (in_array($project_ware['warehouse_id'], $warehouse_to_data_arr)) {
                        if (!empty($project_ware['project_id'])) {
                            $project_model = Projects::model()->findByPk($project_ware['project_id']);
                            if (!empty($project_model)) {
                                $project_name = $project_model['name'];
                            }
                        }

                        $data_arr = array('id' => $project_ware['warehouse_id'], 'text' => $project_ware['warehouse_name'], 'group' => $project_name);
                        array_push($warehouse_to_result_array, $data_arr);
                    }
                }
            }
            $data = CHtml::listData($warehouse_to_result_array, 'id', 'text', 'group');
            echo CHtml::dropDownList('warehouse_to', 'warehouse_to', $data, array('class' => 'select_box', 'empty' => 'Select Warehouse    ', 'style' => 'width:200px;', 'id' => 'warehouse_to', 'options' => array($warehouse_to => array('selected' => true))));
        } else {
            if (yii::app()->user->role == 5) {
                $data = CHtml::listData(Controller::getCategoryOptions($type = 1), 'id', 'text', 'group');
            } else {
                $data = CHtml::listData(Controller::getCategoryOptions(), 'id', 'text', 'group');
            }
            echo CHtml::dropDownList('warehouse_to', 'warehouse_to', $data, array('class' => 'select_box', 'empty' => 'Select Warehouse    ', 'style' => 'width:200px;', 'id' => 'warehouse_to', 'options' => array($warehouse_to => array('selected' => true))));
        }
    }

    public function actiongetWarehouseFrom()
    {
        $warehouse_to = $_POST['warehouse_to'];
        $warehouse_from = $_POST['warehouse_from'];
        $warehouse_from_data_arr = array();
        $warehouse_from_result_array = array();
        $data_arr = array();
        $tblpx = Yii::app()->db->tablePrefix;
        if ($warehouse_to != "") {
            $warehouse_from_data_sql = "SELECT DISTINCT warehousedespatch_warehouseid FROM {$tblpx}warehousedespatch WHERE warehousedespatch_warehouseid_to = '" . $warehouse_to . "'";
            $warehouse_from_data = Yii::app()->db->createCommand($warehouse_from_data_sql)->queryAll();
            foreach ($warehouse_from_data as $warehouse_from_data_value) {
                if ($warehouse_from_data_value['warehousedespatch_warehouseid'] != '') {
                    $warehouse_from_data_arr[] = $warehouse_from_data_value['warehousedespatch_warehouseid'];
                }
            }
            $commom_warehouse = Warehouse::model()->findAll(
                array(
                    'select' => array('warehouse_id', 'warehouse_name'), 'condition' => 'project_id IS NULL',
                    'order' => 'warehouse_id DESC'
                )
            );
            $project_warehouse = Warehouse::model()->findAll(
                array(
                    'select' => array('warehouse_id', 'warehouse_name', 'project_id'), 'condition' => 'project_id IS NOT NULL',
                    'order' => 'warehouse_id DESC'
                )
            );
            foreach ($commom_warehouse as $common) {
                if (!empty($warehouse_from_data_arr)) {
                    if (in_array($common['warehouse_id'], $warehouse_from_data_arr)) {
                        $data_arr = array('id' => $common['warehouse_id'], 'text' => $common['warehouse_name'], 'group' => 'General');
                        array_push($warehouse_from_result_array, $data_arr);
                    }
                }
            }
            foreach ($project_warehouse as $project_ware) {
                if (!empty($warehouse_from_data_arr)) {
                    if (in_array($project_ware['warehouse_id'], $warehouse_from_data_arr)) {
                        if (!empty($project_ware['project_id'])) {
                            $project_model = Projects::model()->findByPk($project_ware['project_id']);
                            if (!empty($project_model)) {
                                $project_name = $project_model['name'];
                            }
                        }

                        $data_arr = array('id' => $project_ware['warehouse_id'], 'text' => $project_ware['warehouse_name'], 'group' => $project_name);
                        array_push($warehouse_from_result_array, $data_arr);
                    }
                }
            }
            $data = CHtml::listData($warehouse_from_result_array, 'id', 'text', 'group');
            echo CHtml::dropDownList('warehouse_from', 'warehouse_from', $data, array('class' => 'select_box', 'empty' => 'Select Warehouse    ', 'style' => 'width:200px;', 'id' => 'warehouse_from', 'options' => array($warehouse_from => array('selected' => true))));
        } else {
            if (yii::app()->user->role == 5) {
                $data = CHtml::listData(Controller::getCategoryOptions($type = 1), 'id', 'text', 'group');
            } else {
                $data = CHtml::listData(Controller::getCategoryOptions(), 'id', 'text', 'group');
            }
            echo CHtml::dropDownList('warehouse_from', 'warehouse_from', $data, array('class' => 'select_box', 'empty' => 'Select Warehouse    ', 'style' => 'width:200px;', 'id' => 'warehouse_from', 'options' => array($warehouse_from => array('selected' => true))));
        }
    }
    public function actionTransferreport()
    {
        $warehouse_from = '';
        $warehouse_to = '';
        $date_to = '';
        $date_from = '';
        $criteria = new CDbCriteria();
        $model = new Warehousedespatch;
        $model->unsetAttributes();  // clear any default values
        $criteria->condition = "";
        if (isset($_GET['warehouse_from']) && !empty($_GET['warehouse_from'])) {
            $warehouse_from = $_GET['warehouse_from'];
            $criteria->addCondition("warehousedespatch_warehouseid = " . $warehouse_from);
        }
        if (isset($_GET['warehouse_to']) && !empty($_GET['warehouse_to'])) {
            $warehouse_to = $_GET['warehouse_to'];
            $criteria->addCondition("warehousedespatch_warehouseid_to = " . $warehouse_to);
        }
        if (isset($_GET['date_from']) && !empty($_GET['date_from'])) {
            $date_from = date('Y-m-d', strtotime($_GET['date_from']));
            $criteria->addCondition("warehousedespatch_date >= '" . $date_from . "'");
        } else {
            $date_from = '';
            $criteria->compare('warehousedespatch_date', $date_from, true);
        }
        if (isset($_GET['date_to']) && !empty($_GET['date_to'])) {
            $date_to = date('Y-m-d', strtotime($_GET['date_to']));
            $criteria->addCondition("warehousedespatch_date <= '" . $date_to . "'");
        } else {
            $date_to = '';
            $criteria->compare('warehousestock_date', $date_to, true);
        }
        if (isset($_GET['status']) && !empty($_GET['status'])) {
            $filter_status_id = $_GET['status'];
        } else {
            $filter_status_id = '';
        }
        $criteria->order = 'warehousedespatch_id DESC';
        if ($filter_status_id == 1) {
            $filter_status = "Dispatched";
        } elseif ($filter_status_id == 2) {
            $filter_status = "In Transit";
        } elseif ($filter_status_id == 3) {
            $filter_status = "Received";
        } elseif ($filter_status_id == 4) {
            $filter_status = "Deleted";
        } else {
            $filter_status = "";
        }
        $dispatch_datas = Warehousedespatch::model()->findAll($criteria);
        $warehouse_assigned_arr = array();
        if (Yii::app()->user->role != 1) {
            $warehouse_assigned_data = Warehouse::model()->findAll(array('condition' => 'FIND_IN_SET(' . Yii::app()->user->id . ', assigned_to)'));
        } else {
            $warehouse_assigned_data = Warehouse::model()->findAll();
        }
        foreach ($warehouse_assigned_data as $assigned_data) {
            $warehouse_assigned_arr[] = $assigned_data['warehouse_id'];
        }
        $result_data = $this->transferReportData($dispatch_datas, $filter_status, $warehouse_assigned_arr);
        // echo '<pre>';print_r($warehouse_assigned_arr);exit;
        $dataprovider = new CArrayDataProvider($result_data, array(
            'id' => 'transfer_report',
            'keyField' => 'dispatch_no',
            'pagination' => array('pageSize' => 20),
        ));
        $result_data_count = count($result_data);
        $dataProvider1 = $this->transferReportDataFetch($warehouse_from, $warehouse_to, $date_from, $date_to, $filter_status_id);
        return $this->render('_transfer_report_1', array(
            'warehouse_from' => $warehouse_from,
            'warehouse_to' => $warehouse_to,
            'filter_status' => $filter_status_id,
            'date_from' => $date_from,
            'date_to' => $date_to,
            'result_data_count' => $result_data_count,
            'dataprovider' => $dataProvider1
        ));
    }
    public function transferReportDataFetch($warehouse_from, $warehouse_to, $date_from, $date_to, $filter_status_id)
    {
        $sql = 'SELECT * FROM (
                    SELECT *,(
                        CASE 
                            WHEN (edit_status =1 AND  delete_status = 0) THEN 
                            (CASE 
                            WHEN (warehousereceipt_date IS NOT NULL ) THEN "Received"
                            WHEN (warehousedespatch_date = CURDATE()) THEN "Dispatched"
                            ELSE "In Transit"
                            END)
                            WHEN (edit_status =0 AND  delete_status = 1) THEN "Deleted"
                            WHEN (edit_status =1 AND  delete_status = 1) THEN 
                            (CASE 
                            WHEN (warehousereceipt_date IS NOT NULL ) THEN "Received"
                            WHEN (warehousedespatch_date = CURDATE()) THEN "Dispatched"
                            ELSE "In Transit"
                            END)
                            ELSE (CASE 
                            WHEN (warehousereceipt_date IS NOT NULL AND edit_status !=0 AND  delete_status != 0) THEN "Received"
                            WHEN (edit_status =0 AND  delete_status = 0) THEN "Deleted"
                            WHEN (warehouse_despatch_date = CURDATE()) THEN "Dispatched"
                            ELSE "In Transit"
                            END)
                        END) AS despatch_status,
                        (
                            CASE 
                                WHEN delete_status = 1 THEN (CASE 
                                    WHEN edit_status =1 THEN "Dispatch"
                                    ELSE "Delete"
                                END)
                                WHEN delete_status = 0 THEN (CASE 
                                WHEN (warehousereceipt_id IS NOT NULL AND edit_status =0) THEN "Delete"
                                WHEN (warehousereceipt_id IS NOT NULL) THEN "Receipt"
                                ELSE "Dispatch"
                                END) 
                                ELSE (CASE 
                                WHEN (warehousereceipt_id IS NOT NULL) THEN "Receipt"
                                ELSE "Dispatch"
                                END)
                            END) AS type,
                        (CASE 
                            WHEN (edit_status =1 AND  delete_status = 0) THEN 
                                IF(warehousereceipt_date IS NOT NULL, warehousereceipt_date, warehouse_eta_date)
                            WHEN (edit_status =0 AND  delete_status = 1) THEN 
                                IF(warehouse_transfer_date IS NOT NULL,warehouse_transfer_date, warehouse_despatch_eta_date)
                            WHEN (edit_status =1 AND  delete_status = 1) THEN 
                                IF(warehousereceipt_date IS NOT NULL, warehousereceipt_date, warehouse_eta_date)
                            ELSE 
                                IF(warehousereceipt_date IS NOT NULL, warehousereceipt_date, warehouse_eta_date)
                        END) AS received_date,
                        (CASE 
                            WHEN (edit_status =1 AND  delete_status = 0) THEN 
                                IF(warehousereceipt_date != warehouse_eta_date || warehouse_eta_date IS NULL, "", "(ETA)")
                            WHEN (edit_status =0 AND  delete_status = 1) THEN 
                                IF(warehouse_transfer_date != warehouse_despatch_eta_date || warehouse_eta_date IS NULL, "", "(ETA)")
                            WHEN (edit_status =1 AND  delete_status = 1) THEN 
                                IF(warehousereceipt_date != warehouse_eta_date || warehouse_eta_date IS NULL, "", "(ETA)")
                            ELSE 
                                IF(warehousereceipt_date != warehouse_eta_date || warehouse_eta_date IS NULL, "", "(ETA)")
                        END) AS eta
                        
                        FROM `warehouse_transfer_details_all`) as a WHERE 1';
        if (!empty($warehouse_from)) {
            $sql .= ' AND a.warehousedespatch_warehouseid ="' . $warehouse_from . '"';
        }
        if (!empty($warehouse_to)) {
            $sql .= ' AND ((a.warehousedespatch_warehouseid_to =' . $warehouse_to . ' AND ((a.delete_status =0) OR (a.delete_status =1 AND a.edit_status =1))) OR (a.warehouseid_to =' . $warehouse_to . ' AND a.delete_status =1))';
        }
        if (!empty($date_from) || !empty($date_to)) {
            if ((isset($date_from) && !empty($date_from)) && ((isset($date_to) && empty($date_to)) || !isset($date_to))) {
                $sql .= ' AND ((a.warehousedespatch_date >="' . $date_from . '" AND ((a.delete_status =0) OR (a.delete_status =1 AND a.edit_status =1))) 
                                    OR 
                                (a.warehouse_despatch_date >="' . $date_from . '" AND a.delete_status =1))';
            } else if ((isset($date_to) && !empty($date_to)) && ((isset($date_from) && empty($date_from)) || !isset($date_from))) {
                $sql .= ' AND ((a.warehousedespatch_date <="' . $date_to . '" AND ((a.delete_status =0) OR (a.delete_status =1 AND a.edit_status =1))) 
                                OR 
                            (a.warehouse_despatch_date <="' . $date_to . '" AND a.delete_status =1))';
            } else if ((isset($date_to) && !empty($date_to)) && (isset($date_from) && !empty($date_from))) {
                $sql .= ' AND ((a.warehousedespatch_date >="' . $date_from . '" AND a.warehousedespatch_date <="' . $date_to . '" AND ((a.delete_status =0) OR (a.delete_status =1 AND a.edit_status =1))) 
                                OR 
                            (a.warehouse_despatch_date >="' . $date_from . '" AND a.warehouse_despatch_date <="' . $date_to . '" AND a.delete_status =1))';
            } else {
                $sql .= ' AND 1=1 ';
            }
        } else {
            $sql .= ' AND 1=1 ';
        }
        if (isset($_GET['status']) && !empty($_GET['status'])) {
            $filter_status_id = $_GET['status'];
            if ($filter_status_id == 1) {
                $filter_status = "Dispatched";
                $sql .= " AND a.despatch_status = 'Dispatched'";
            } elseif ($filter_status_id == 2) {
                $filter_status = "In Transit";
                $sql .= " AND a.despatch_status = 'In Transit'";
            } elseif ($filter_status_id == 3) {
                $filter_status = "Received";
                $sql .= " AND a.despatch_status = 'Received'";
            } elseif ($filter_status_id == 4) {
                $filter_status = "Deleted";
                $sql .= " AND a.despatch_status = 'Deleted'";
            } else {
                $filter_status = "";
                $sql .= " AND 1=1";
            }
        }

        $sql .= " ORDER BY warehousedespatch_id DESC,all_warehousereceipt_id DESC";
        //echo $sql;exit;
        $warehousedespatch_details = Yii::app()->db->createCommand($sql)->queryAll();
        $warehousedespatch_count = count($warehousedespatch_details);
        $dataProvider = new CSqlDataProvider($sql, array(
            'keyField' => 'warehousedespatch_id',
            'totalItemCount' => $warehousedespatch_count,
            'pagination' => array('pageSize' => 10,),
        ));
        return $dataProvider;
    }
    public function transferReportData($dispatch_datas, $filter_status, $warehouse_assigned_arr)
    {

        $result_array = array();
        $j = 0;
        $i = 0;
        $final_result_array = array();
        if (!empty($dispatch_datas)) {
            foreach ($dispatch_datas as $dispatch_data) {
                $data = array();

                if (!empty($dispatch_data['warehousedespatch_id'])) {
                    $warehouse_receipt_data = Warehousereceipt::model()->find(['condition' => 'warehousereceipt_despatch_id = ' . $dispatch_data['warehousedespatch_id'] . ' AND warehousereceipt_transfer_type = 2']);

                    if (!empty($warehouse_receipt_data)) {
                        $data = array('dispatch_no' => $dispatch_data['warehousedespatch_no'], 'receipt_no' => $warehouse_receipt_data['warehousereceipt_no'], 'dispatch_id' => $dispatch_data['warehousedespatch_id'], 'receipt_id' => $warehouse_receipt_data['warehousereceipt_id'], 'type' => 'Receipt', 'from_warehouse' => $dispatch_data->warehouse['warehouse_name'], 'from_warehouse_id' => $dispatch_data->warehousedespatch_warehouseid, 'to_warehouse' => $warehouse_receipt_data->warehouse['warehouse_name'], 'to_warehouse_id' => $dispatch_data->warehousedespatch_warehouseid_to, 'status' => 'Received', 'disaptch_date' => $dispatch_data['warehousedespatch_date'], 'received_date' => !empty($warehouse_receipt_data['warehousereceipt_date']) ? date('d-M-Y', strtotime($warehouse_receipt_data['warehousereceipt_date'])) : '');
                        array_push($result_array, $data);
                    } else {

                        $dispatch_date = strtotime($dispatch_data['warehousedespatch_date']);
                        $current_date = strtotime(date('Y-m-d'));
                        if ($dispatch_date == $current_date) {
                            $status = 'Dispatched';
                        } else {
                            $status = 'In Transit';
                        }
                        $data = array('dispatch_no' => $dispatch_data['warehousedespatch_no'], 'receipt_no' => '-', 'dispatch_id' => $dispatch_data['warehousedespatch_id'], 'receipt_id' => '', 'type' => 'Dispatch', 'from_warehouse' => $dispatch_data->warehouse['warehouse_name'], 'from_warehouse_id' => $dispatch_data->warehousedespatch_warehouseid, 'to_warehouse' => $dispatch_data->warehousedespatchWarehouseidTo['warehouse_name'], 'to_warehouse_id' => $dispatch_data->warehousedespatch_warehouseid_to, 'status' => $status, 'disaptch_date' => $dispatch_data['warehousedespatch_date'], 'received_date' => !empty($dispatch_data['warehouse_eta_date']) ? date('d-M-Y', strtotime($dispatch_data['warehouse_eta_date'])) . ' (ETA)' : '');
                        array_push($result_array, $data);
                    }
                    $warehouse_receipt_deleted_data_cond_arr = array('condition' => 'warehousereceipt_despatch_id = "' . $dispatch_data['warehousedespatch_id'] . '" AND warehouse_transfer_type_delete = 1 AND warehousereceipt_transfer_type = 2');
                    $warehouse_receipt_deleted_data = WarehouseTransferDeletedLog::model()->findAll($warehouse_receipt_deleted_data_cond_arr);
                    if (!empty($warehouse_receipt_deleted_data)) {
                        foreach ($warehouse_receipt_deleted_data as $deleted_data) {
                            $data = array('dispatch_no' => $dispatch_data['warehousedespatch_no'], 'receipt_no' => $deleted_data['warehouse_transfer_type_deleted_no'], 'dispatch_id' => $dispatch_data['warehousedespatch_id'], 'receipt_id' => $deleted_data['warehouse_transfer_type_deleted_id'], 'type' => 'Deleted', 'from_warehouse' => $deleted_data->warehouseidFrom['warehouse_name'], 'from_warehouse_id' => $deleted_data['warehouseid_from'], 'to_warehouse' => $deleted_data->warehouseidTo['warehouse_name'], 'to_warehouse_id' => $deleted_data['warehouseid_to'], 'status' => 'Deleted', 'disaptch_date' => $deleted_data['warehouse_despatch_date'], 'received_date' => !empty($deleted_data['warehouse_transfer_date']) ? date('d-M-Y', strtotime($deleted_data['warehouse_despatch_eta_date'])) : '');
                            array_push($result_array, $data);
                        }
                    }
                }
            }
        }
        //   echo "<pre>";print_r($warehouse_assigned_arr); echo "<br>";

        foreach ($result_array as $key => $result_array_value) {
            if ($filter_status != "") {
                if ($filter_status == $result_array_value['status']) {
                    if (!empty($warehouse_assigned_arr)) {
                        if (in_array($result_array_value['from_warehouse_id'], $warehouse_assigned_arr) || in_array($result_array_value['to_warehouse_id'], $warehouse_assigned_arr)) {
                            $final_result_array[$key]['sl_no'] = $j++;
                            $final_result_array[$key]['dispatch_no'] = $result_array_value['dispatch_no'];
                            $final_result_array[$key]['receipt_no'] = $result_array_value['receipt_no'];
                            $final_result_array[$key]['dispatch_id'] = $result_array_value['dispatch_id'];
                            $final_result_array[$key]['receipt_id'] = $result_array_value['receipt_id'];
                            $final_result_array[$key]['type'] = $result_array_value['type'];
                            $final_result_array[$key]['from_warehouse'] = $result_array_value['from_warehouse'];
                            $final_result_array[$key]['to_warehouse'] = $result_array_value['to_warehouse'];
                            $final_result_array[$key]['from_warehouse_id'] = $result_array_value['from_warehouse_id'];
                            $final_result_array[$key]['to_warehouse_id'] = $result_array_value['to_warehouse_id'];
                            $final_result_array[$key]['status'] = $result_array_value['status'];
                            $final_result_array[$key]['disaptch_date'] = $result_array_value['disaptch_date'];
                            $final_result_array[$key]['received_date'] = $result_array_value['received_date'];
                        }
                    }
                }
            } else {
                if (!empty($warehouse_assigned_arr)) {
                    if (in_array($result_array_value['from_warehouse_id'], $warehouse_assigned_arr) || in_array($result_array_value['to_warehouse_id'], $warehouse_assigned_arr)) {
                        $final_result_array[$key]['sl_no'] = $i++;
                        $final_result_array[$key]['dispatch_no'] = $result_array_value['dispatch_no'];
                        $final_result_array[$key]['receipt_no'] = $result_array_value['receipt_no'];
                        $final_result_array[$key]['dispatch_id'] = $result_array_value['dispatch_id'];
                        $final_result_array[$key]['receipt_id'] = $result_array_value['receipt_id'];
                        $final_result_array[$key]['type'] = $result_array_value['type'];
                        $final_result_array[$key]['from_warehouse'] = $result_array_value['from_warehouse'];
                        $final_result_array[$key]['from_warehouse_id'] = $result_array_value['from_warehouse_id'];
                        $final_result_array[$key]['to_warehouse_id'] = $result_array_value['to_warehouse_id'];
                        $final_result_array[$key]['to_warehouse'] = $result_array_value['to_warehouse'];
                        $final_result_array[$key]['status'] = $result_array_value['status'];
                        $final_result_array[$key]['disaptch_date'] = $result_array_value['disaptch_date'];
                        $final_result_array[$key]['received_date'] = $result_array_value['received_date'];
                    }
                }

                //  $final_result_array = $result_array;
            }
        }
        // echo $filter_status;
        //  echo "<pre>";print_r($final_result_array);exit();
        return $final_result_array;
    }
    public function actionwarehouseandpurchaseReportData()
    {
        $WarehouseCategory_data = WarehouseCategory::model()->findAll();
        $PurchaseCategory_data = PurchaseCategory::model()->findAll();
        // echo "<pre>"; print_r($WarehouseCategory_data);   
        if (!empty($WarehouseCategory_data)) {
            foreach ($WarehouseCategory_data as $WarehouseCategory) {
                foreach ($PurchaseCategory_data as $PurchaseCategory) {
                    if ($WarehouseCategory['brand_id'] == $PurchaseCategory['brand_id'] && $WarehouseCategory['category_name'] == $PurchaseCategory['category_name']) {
                        echo  $WarehouseCategory['id'] . "-" . $WarehouseCategory['brand_id'] . "-" . $WarehouseCategory['category_name'] . "-" . $WarehouseCategory['specification'] . "<br>";
                        echo  $PurchaseCategory['id'] . "-" . $PurchaseCategory['brand_id'] . "-" . $PurchaseCategory['category_name'] . "-" . $PurchaseCategory['specification'];

                        // $data = array('PurchaseCategory_id' => $PurchaseCategory['id'],
                        //  'receipt_no' => $PurchaseCategory['warehouse_transfer_type_deleted_no'],
                        //  'dispatch_id'=>$PurchaseCategory['warehousedespatch_id'],
                        //  'receipt_id'=>$PurchaseCategory['warehouse_transfer_type_deleted_id'],
                        //  'type' => 'Deleted',
                        //   'from_warehouse' => $deleted_data->warehouseidFrom['warehouse_name'],
                        //   'from_warehouse_id' => $deleted_data['warehouseid_from'],
                        //    'to_warehouse' => $deleted_data->warehouseidTo['warehouse_name'],
                        //    'to_warehouse_id' => $deleted_data['warehouseid_to'],
                        //     'status' => 'Deleted', 
                        //     'disaptch_date' => $deleted_data['warehouse_despatch_date'], 
                        //     'received_date' => !empty($deleted_data['warehouse_transfer_date']) ? date('d-M-Y', strtotime($deleted_data['warehouse_despatch_eta_date'])) : '');
                        // array_push($result_array, $data);
                    }
                }
            }
        }
    }
    public function actionstockValueReport()
    {
        $model = new Warehousestock;
        $final_stock_value_data = array();
        $specification = $this->actionGetItemCategory($parent = 0, $spacing = '', $user_tree_array = '');
        $model->unsetAttributes();  // clear any default values
        $criteria = new CDbCriteria();
        $criteria->condition = "";



        if (isset($_GET['warehouse_id']) && !empty($_GET['warehouse_id'])) {
            $model->warehousestock_warehouseid = $_GET['warehouse_id'];
            $criteria->addCondition("warehousestock_warehouseid = " . $_GET['warehouse_id']);
        }
        if (isset($_GET['date_from']) && !empty($_GET['date_from'])) {
            $model->date_from = $_GET['date_from'];
            $date_from = date('Y-m-d', strtotime($_GET['date_from']));
            $criteria->addCondition("warehousestock_date >= '" . $date_from . "'");
        } else {

            $date_from = '';
            $criteria->compare('warehousestock_date', $date_from, true);
        }
        if (isset($_GET['date_to']) && !empty($_GET['date_to'])) {
            $model->date_to = $_GET['date_to'];
            $date_to = date('Y-m-d', strtotime($_GET['date_to']));
            $criteria->addCondition("warehousestock_date <= '" . $date_to . "'");
        } else {
            $date_to = '';
            $criteria->compare('warehousestock_date', $date_to, true);
        }
        if (isset($_GET['item_id']) && !empty($_GET['item_id'])) {
            $model->warehousestock_itemid = $_GET['item_id'];
            $criteria->addCondition("warehousestock_itemid = " . $_GET['item_id']);
        }
        $criteria->addCondition("warehousestock_stock_quantity != 0");
        $criteria->order = 'warehousestock_id DESC';
        $stock_datas = Warehousestock::model()->findAll($criteria);

        $final_stock_value_data = $this->stockValueData($stock_datas);
        return $this->render('_stock_value_report', array(
            'model' => $model,
            'specification' => $specification,
            'result_data' => $final_stock_value_data
        ));
    }


    public function stockValueData($stockvaluedata)
    {

        $stock_value_array = array();
        if (!empty($stockvaluedata)) {
            foreach ($stockvaluedata as $stockdata) {


                if ($stockdata['dimension'] != null) {
                    $multiplying_quantity_arr = explode(" x ", $stockdata['dimension']);
                    $multiplying_quantity = $multiplying_quantity_arr['0'] * $multiplying_quantity_arr['1'];
                } else {
                    $multiplying_quantity = 1;
                }
                $amount = $stockdata['rate'] * $stockdata['warehousestock_stock_quantity'] * $multiplying_quantity;
                $tblpx = Yii::app()->db->tablePrefix;
                $desctiptions = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}specification WHERE id=" . $stockdata['warehousestock_itemid'] . "")->queryRow();

                $parent_category = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}purchase_category WHERE id='" . $desctiptions['cat_id'] . "'")->queryRow();
                if ($desctiptions['brand_id'] != NULL) {
                    $brand_details = Yii::app()->db->createCommand("SELECT brand_name FROM {$tblpx}brand WHERE id=" . $desctiptions['brand_id'] . "")->queryRow();
                    $brand = '-' . $brand_details['brand_name'];
                } else {
                    $brand = '';
                }
                $spc_details = $parent_category['category_name'] . $brand . ' - ' . $desctiptions['specification'];
                $item_img = $desctiptions['filename'];
                $batch = $stockdata['batch'];
                if (array_key_exists($stockdata['warehousestock_itemid'], $stock_value_array)) {
                    $data = array('warehouseId' => $stockdata->warehouse['warehouse_id'],'warehouse' => $stockdata->warehouse['warehouse_name'], 'item' => $spc_details, 'item_img' => $item_img, 'item_id' => $stockdata['warehousestock_itemid'], 'dimension' => $stockdata['dimension'], 'batch' => $stockdata['batch'], 'unit' => $stockdata['warehousestock_unit'], 'qty' => $stockdata['warehousestock_stock_quantity'], 'rate' => $stockdata['rate'], 'amount' => $amount, 'date' => $stockdata['warehousestock_date']);
                    array_push($stock_value_array[$stockdata['warehousestock_itemid']], $data);
                } else {
                    $data = array('warehouseId' => $stockdata->warehouse['warehouse_id'],'warehouse' => $stockdata->warehouse['warehouse_name'], 'item' => $spc_details, 'item_img' => $item_img, 'item_id' => $stockdata['warehousestock_itemid'], 'dimension' => $stockdata['dimension'], 'batch' => $stockdata['batch'], 'unit' => $stockdata['warehousestock_unit'], 'qty' => $stockdata['warehousestock_stock_quantity'], 'rate' => $stockdata['rate'], 'amount' => $amount, 'date' => $stockdata['warehousestock_date']);

                    $stock_value_array[$stockdata['warehousestock_itemid']][] = $data;
                }
            }
            
        }
        return $stock_value_array;
    }

    public function actionstockValueReportdetail()
    {
        $model = new WarehousereceiptItems;
        $model2 = new WarehousedespatchItems;
        $final_stock_value_datareceipt = array();
        $final_stock_value_datadespatch = array();
        $specification = $this->actionGetItemCategory($parent = 0, $spacing = '', $user_tree_array = '');
        $model->unsetAttributes();  // clear any default values
        $model2->unsetAttributes();

        $criteria = new CDbCriteria();
        $criteria->condition = "";

        $criteria1 = new CDbCriteria();
        $criteria1->condition = "";



        if (isset($_GET['warehouse_id']) && !empty($_GET['warehouse_id'])) {
            $model->warehousereceipt_warehouseid = $_GET['warehouse_id'];
            $criteria->addCondition("warehousereceipt_warehouseid = " . $_GET['warehouse_id']);
        }
        if (isset($_GET['date_from']) && !empty($_GET['date_from'])) {
            $date_from = date('Y-m-d', strtotime($_GET['date_from']));
            $model->created_date = $_GET['date_from'];
            $criteria->addCondition("created_date >= '" . $date_from . "'");
        } else {

            $date_from = '';
            $criteria->compare('created_date', $date_from, true);
        }
        if (isset($_GET['date_to']) && !empty($_GET['date_to'])) {
            $date_to = date('Y-m-d', strtotime($_GET['date_to']));
            $model->created_date = $_GET['date_to'];
            $criteria->addCondition("DATE(created_date) <= '" . $date_to . "'");
        } else {
            $date_to = '';
            $criteria->compare('created_date', $date_to, true);
        }
        if (isset($_GET['item_id']) && !empty($_GET['item_id'])) {
            $model->warehousereceipt_itemid = $_GET['item_id'];
            $criteria->addCondition("warehousereceipt_itemid = " . $_GET['item_id']);
        }

        if (isset($_GET['rate']) && !empty($_GET['rate'])) {
            $rate = $_GET['rate'];
            $model->warehousereceipt_rate = $rate;
            $criteria->addCondition("warehousereceipt_rate = " . $rate);
        }
        $criteria->addCondition("warehousereceipt_quantity != 0");
        $criteria->order = 'item_id DESC';


        $stock_datareceipt = WarehousereceiptItems::model()->findAll($criteria);


        if (isset($_GET['warehouse_id']) && !empty($_GET['warehouse_id'])) {
            $model2->warehousedespatch_warehouseid = $_GET['warehouse_id'];
            $criteria1->addCondition("warehousedespatch_warehouseid = " . $_GET['warehouse_id']);
        }
        if (isset($_GET['date_from']) && !empty($_GET['date_from'])) {
            $date_from = date('Y-m-d', strtotime($_GET['date_from']));
            $model2->created_date = $_GET['date_from'];
            $criteria1->addCondition("created_date >= '" . $date_from . "'");
        } else {

            $date_from = '';
            $criteria1->compare('created_date', $date_from, true);
        }
        if (isset($_GET['date_to']) && !empty($_GET['date_to'])) {
            $date_to = date('Y-m-d', strtotime($_GET['date_to']));
            $model2->created_date = $date_to;
            $criteria1->addCondition("DATE(created_date) <= '" . $date_to . "'");
        } else {
            $date_to = '';
            $criteria1->compare('created_date', $date_to, true);
        }
        if (isset($_GET['item_id']) && !empty($_GET['item_id'])) {
            $model2->warehousedespatch_itemid = $_GET['item_id'];
            $criteria1->addCondition("warehousedespatch_itemid = " . $_GET['item_id']);
        }

        if (isset($_GET['rate']) && !empty($_GET['rate'])) {
            $rate = $_GET['rate'];
            $model2->warehousedespatch_rate = $rate;
            $criteria1->addCondition("warehousedespatch_rate = " . $rate);
        }
        $criteria1->addCondition("warehousedespatch_quantity != 0");
        $criteria1->order = 'item_id DESC';
        $stock_datadespatch = WarehousedespatchItems::model()->findAll($criteria1);


        $final_stock_value_datareceipt = $this->stockValueDatadetail($stock_datareceipt);
        $final_stock_value_datadespatch = $this->stockValuedetaildespatch($stock_datadespatch);

        if (!empty($final_stock_value_datareceipt) && !empty($final_stock_value_datadespatch)) {
            $mainarray = array_merge($final_stock_value_datareceipt, $final_stock_value_datadespatch);
        } else {
            $mainarray = $final_stock_value_datareceipt;
        }

        return $this->render('_stock_value_report_new', array(
            'model' => $model,
            'specification' => $specification,
            'result_data' => $mainarray,
        ));
    }



    public function stockValueDatadetail($stockvaluedata)
    {

        $stock_value_array = array();
        if (!empty($stockvaluedata)) {
            foreach ($stockvaluedata as $stockdata) {
                if ($stockdata['dimension'] != null) {
                    $multiplying_quantity_arr = explode(" x ", $stockdata['dimension']);
                    $multiplying_quantity = $multiplying_quantity_arr['0'] * $multiplying_quantity_arr['1'];
                } else {
                    $multiplying_quantity = 1;
                }
                $amount = $stockdata['warehousereceipt_rate'] * $stockdata['warehousereceipt_quantity'] * $multiplying_quantity;
                $tblpx = Yii::app()->db->tablePrefix;
                $sql = "SELECT * FROM {$tblpx}specification "
                . " WHERE id=" . $stockdata['warehousereceipt_itemid'] ;
                $desctiptions = Yii::app()->db->createCommand($sql)->queryRow();
                
                $sql ="SELECT * FROM {$tblpx}purchase_category "
                . " WHERE id='" . $desctiptions['cat_id'] . "'" ;
                $parent_category = Yii::app()->db->createCommand($sql)->queryRow();

                $brand = '';
                if ($desctiptions['brand_id'] != NULL) {
                    $brand_details = Yii::app()->db->createCommand("SELECT brand_name FROM {$tblpx}brand WHERE id=" . $desctiptions['brand_id'] . "")->queryRow();
                    $brand = '-' . $brand_details['brand_name'];
                }

                $spc_details = $parent_category['category_name'] . $brand . ' - ' . $desctiptions['specification'];
                $item_img = $desctiptions['filename'];
                $batch = $stockdata['warehousereceipt_batch'];

                $sql="SELECT * FROM {$tblpx}specification "
                . " WHERE id=" . $stockdata['warehousereceipt_itemid'];
                $desctiptions = Yii::app()->db->createCommand($sql)->queryRow();

                $warehousedata = Yii::app()->db->createCommand("SELECT warehouse_name FROM {$tblpx}warehouse WHERE 	warehouse_id=" . $stockdata['warehousereceipt_warehouseid'] . "")->queryRow();



                if (array_key_exists($stockdata['warehousereceipt_itemid'], $stock_value_array)) {

                    $data = array(
                        'warehouse' => $warehousedata['warehouse_name'], 'item' => $spc_details, 'item_img' => $item_img, 'item_id' => $stockdata['warehousereceipt_itemid'], 'dimension' => $stockdata['dimension'], 'batch' => $stockdata['warehousereceipt_batch'], 'unit' => $stockdata['warehousereceipt_unit'], 'qty' => $stockdata['warehousereceipt_quantity'], 'rate' => $stockdata['warehousereceipt_rate'], 'amount' => $amount,
                        'date' => date('Y-m-d', strtotime($stockdata['created_date'])),
                        'type' => 'receipt'
                    );
                    array_push($stock_value_array[$stockdata['warehousereceipt_itemid']], $data);
                } else {
                    $data = array(
                        'warehouse' => $warehousedata['warehouse_name'], 'item' => $spc_details, 'item_img' => $item_img, 'item_id' => $stockdata['warehousereceipt_itemid'], 'dimension' => $stockdata['dimension'], 'batch' => $stockdata['warehousereceipt_batch'], 'unit' => $stockdata['warehousereceipt_unit'], 'qty' => $stockdata['warehousereceipt_quantity'], 'rate' => $stockdata['warehousereceipt_rate'], 'amount' => $amount, 'date' => date('Y-m-d', strtotime($stockdata['created_date'])),
                        'type' => 'receipt'
                    );

                    $stock_value_array[$stockdata['warehousereceipt_itemid']][] = $data;
                }
            }
        }


        return $stock_value_array;
    }



    public function stockValuedetaildespatch($stockvaluedata)
    {

        $stock_value_array = array();
        if (!empty($stockvaluedata)) {
            foreach ($stockvaluedata as $stockdata) {
                if ($stockdata['dimension'] != null) {
                    $multiplying_quantity_arr = explode(" x ", $stockdata['dimension']);
                    $multiplying_quantity = $multiplying_quantity_arr['0'] * $multiplying_quantity_arr['1'];
                } else {
                    $multiplying_quantity = 1;
                }


                $purchaseqty = $stockdata["warehousedespatch_baseqty"];
                $purchaserate = $stockdata["warehousedespatch_baserate"];
                $purchaseunit = $stockdata["warehousedespatch_baseunit"];


                if ($purchaseqty != '' && $purchaserate != '' && $purchaseunit != "") {
                    if ($stockdata['warehousedespatch_unit'] != $purchaseunit) {
                        $billedquantity = $purchaseqty;
                        $rate = $purchaserate;
                        $bill_unit_id = $purchaseunit;
                    } else {
                        $billedquantity = $purchaseqty;
                        $rate = $purchaserate;
                        $bill_unit_id = $purchaseunit;
                    }
                } else {
                    $bill_unit_id = $stockdata['warehousedespatch_unit'];
                    $billedquantity = $stockdata["warehousedespatch_quantity"];
                    $rate = $stockdata["warehousedespatch_rate"];
                    $bill_unit_id_data = $stockdata['warehousedespatch_unit'];
                    $bill_unit_id_data_dis = $stockdata['warehousedespatch_unit'];
                }
                $amount = $rate * $billedquantity * $multiplying_quantity;
                $tblpx = Yii::app()->db->tablePrefix;
                $desctiptions = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}specification WHERE id=" . $stockdata['warehousedespatch_itemid'] . "")->queryRow();

                $parent_category = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}purchase_category WHERE id='" . $desctiptions['cat_id'] . "'")->queryRow();
                if ($desctiptions['brand_id'] != NULL) {
                    $brand_details = Yii::app()->db->createCommand("SELECT brand_name FROM {$tblpx}brand WHERE id=" . $desctiptions['brand_id'] . "")->queryRow();
                    $brand = '-' . $brand_details['brand_name'];
                } else {
                    $brand = '';
                }
                $spc_details = $parent_category['category_name'] . $brand . ' - ' . $desctiptions['specification'];
                $item_img = $desctiptions['filename'];
                $batch = $stockdata['warehousedespatch_batch'];

                $desctiptions = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}specification WHERE id=" . $stockdata['warehousedespatch_itemid'] . "")->queryRow();

                $warehousedata = Yii::app()->db->createCommand("SELECT warehouse_name FROM {$tblpx}warehouse WHERE 	
                warehouse_id=" . $stockdata['warehousedespatch_warehouseid'] . "")->queryRow();

                if (array_key_exists($stockdata['warehousedespatch_itemid'], $stock_value_array)) {

                    $data = array(
                        'warehouse' => $warehousedata['warehouse_name'], 'item' => $spc_details, 'item_img' => $item_img, 'item_id' => $stockdata['warehousedespatch_itemid'], 'dimension' => $stockdata['dimension'], 'batch' => $stockdata['warehousedespatch_batch'], 'unit' => $bill_unit_id, 'qty' => $billedquantity, 'rate' => $rate, 'amount' => $amount, 'date' => date('Y-m-d', strtotime($stockdata['created_date'])),
                        'type' => 'despatch'
                    );
                    array_push($stock_value_array[$stockdata['warehousedespatch_itemid']], $data);
                } else {
                    $data = array(
                        'warehouse' => $warehousedata['warehouse_name'], 'item' => $spc_details, 'item_img' => $item_img, 'item_id' => $stockdata['warehousedespatch_itemid'], 'dimension' => $stockdata['dimension'], 'batch' => $stockdata['warehousedespatch_batch'], 'unit' => $bill_unit_id, 'qty' => $billedquantity, 'rate' => $rate, 'amount' => $amount, 'date' => date('Y-m-d', strtotime($stockdata['created_date'])),
                        'type' => 'despatch'
                    );

                    $stock_value_array[$stockdata['warehousedespatch_itemid']][] = $data;
                }
            }
        }
        return $stock_value_array;
    }


    public function actionstockStatusReport()
    {
        $model = new Warehousestock;
        $final_stock_status_data = array();
        $specification = $this->actionGetItemCategory($parent = 0, $spacing = '', $user_tree_array = '');
        $units = Controller::StockItemunitlist();
        $model->unsetAttributes();  // clear any default values
        $criteria = new CDbCriteria();
        $criteria->condition = "";



        if (isset($_GET['warehouse_id']) && !empty($_GET['warehouse_id'])) {
            $model->warehousestock_warehouseid = $_GET['warehouse_id'];
            $criteria->addCondition("warehousestock_warehouseid = " . $_GET['warehouse_id']);
        }
        if (isset($_GET['date_from']) && !empty($_GET['date_from'])) {
            $model->date_from = $_GET['date_from'];
            $date_from = date('Y-m-d', strtotime($_GET['date_from']));
            $criteria->addCondition("warehousestock_date >= '" . $date_from . "'");
        } else {
            $date_from = '';
            $criteria->compare('warehousestock_date', $date_from, true);
        }
        if (isset($_GET['date_to']) && !empty($_GET['date_to'])) {
            $model->date_to = $_GET['date_to'];
            $date_to = date('Y-m-d', strtotime($model->date_to));
            $criteria->addCondition("warehousestock_date <= '" . $date_to . "'");
        } else {
            $date_to = '';
            $criteria->compare('warehousestock_date', $date_to, true);
        }
        if (isset($_GET['item_id']) && !empty($_GET['item_id'])) {
            $model->warehousestock_itemid = $_GET['item_id'];
            $criteria->addCondition("warehousestock_itemid = " . $_GET['item_id']);
        }
        if (isset($_GET['unit_id']) && !empty($_GET['unit_id'])) {
            $unitName = Controller::ItemunitName($_GET['unit_id']);
            $model->warehousestock_unit = $unitName;
            $criteria->addCondition("warehousestock_unit = '" . $unitName . "'");
        }
        $criteria->addCondition("warehousestock_stock_quantity != 0");
        $criteria->order = 'warehousestock_id DESC';

        $stock_datas = Warehousestock::model()->findAll($criteria);
        $final_stock_status_data = $this->stockStatusData($stock_datas);


        return $this->render('_stock_status_report', array(
            'model' => $model,
            'specification' => $specification,
            'units' => $units,
            'result_data' => $final_stock_status_data
        ));
    }
    public function actionGetItemCategory($parent = 0, $spacing = '', $user_tree_array = '')
    {
        if (!is_array($user_tree_array))
            $user_tree_array = array();
        $data = array();
        $final = array();
        $tblpx = Yii::app()->db->tablePrefix;
        
        $specification = Yii::app()->db->createCommand("SELECT id, specification, brand_id, cat_id FROM {$tblpx}specification ")->queryAll();


        foreach ($specification as $key => $value) {
            if ($value['brand_id'] != NULL) {
                $brand_details = Yii::app()->db->createCommand("SELECT brand_name FROM {$tblpx}brand WHERE id=" . $value['brand_id'] . "")->queryRow();
                $brand = '-' . ucfirst($brand_details['brand_name']);
            } else {
                $brand = '';
            }

            if ($value['cat_id'] != '') {
                $result = $this->actionGetParent($value['cat_id']);
                $final[$key]['data'] = ucfirst($result['category_name']) . $brand . '-' . ucfirst($value['specification']);
            } else {
                $final[$key]['data'] = $brand . '-' . $value['specification'];
            }



            $final[$key]['id'] = $value['id'];
        }
        return $final;
    }

    public function actionGetParent($id)
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $category = Yii::app()->db->createCommand("SELECT parent_id, id , category_name FROM {$tblpx}purchase_category WHERE id=" . $id)->queryRow();

        return $category;
    }


    public function stockStatusData($stockstatusdata)
    {



        $warehouse_assigned_arr = array();
        $view_stocks_in_other_warehouse_status = Controller::generalSettingsForFunctionsActiveOrInactive($settings_id = 2);
        if ($view_stocks_in_other_warehouse_status == 1) {
            if (Yii::app()->user->role == 1 || Yii::app()->user->role == 5) {
                $warehouse_assigned_data = Warehouse::model()->findAll();
            } else {
                $warehouse_assigned_data = Warehouse::model()->findAll(array('condition' => 'FIND_IN_SET(' . Yii::app()->user->id . ', assigned_to)'));
            }
        } else {
            if (Yii::app()->user->role == 1) {
                $warehouse_assigned_data = Warehouse::model()->findAll();
            } else {
                $warehouse_assigned_data = Warehouse::model()->findAll(array('condition' => 'FIND_IN_SET(' . Yii::app()->user->id . ', assigned_to)'));
            }
        }

        foreach ($warehouse_assigned_data as $assigned_data) {
            $warehouse_assigned_arr[] = $assigned_data['warehouse_id'];
        }
        $tblpx = Yii::app()->db->tablePrefix;
        $stock_status_array = array();
        if (!empty($stockstatusdata)) {
            //echo "<pre> first";print_r($stockstatusdata);exit;
            foreach ($stockstatusdata as $stockdata) {
                $stock_correction_det  = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}warehouse_stock_correction WHERE warehouse_id=".$stockdata['warehousestock_warehouseid']." AND Item_Id=".$stockdata['warehousestock_itemid']. " AND ROUND(wh_receipt_quantity, 2)=".$stockdata['warehousestock_stock_quantity']." AND ROUND(rate, 2)=".$stockdata['rate']." AND approval_status='1'  ORDER BY `id` DESC ")->queryRow();
                                               
                $stock_quantity='';
                if(!empty($stock_correction_det)){
                    $stock_quantity=$stock_correction_det['stock_quantity'] ;
                }else{
                    $stock_quantity=$stockdata['warehousestock_stock_quantity'] ;
                 }
                if (!empty($warehouse_assigned_arr)) {
                    if (in_array($stockdata['warehousestock_warehouseid'], $warehouse_assigned_arr)) {

                        $amount = $stockdata['rate'] * $stockdata['warehousestock_stock_quantity'];
                        
                        $desctiptions = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}specification WHERE id=" . $stockdata['warehousestock_itemid'] . "")->queryRow();

                        $parent_category = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}purchase_category WHERE id='" . $desctiptions['cat_id'] . "'")->queryRow();
                        if ($desctiptions['brand_id'] != NULL) {
                            $brand_details = Yii::app()->db->createCommand("SELECT brand_name FROM {$tblpx}brand WHERE id=" . $desctiptions['brand_id'] . "")->queryRow();
                            $brand = '-' . $brand_details['brand_name'];
                        } else {
                            $brand = '';
                        }
                        $spc_details = $parent_category['category_name'] . $brand . ' - ' . $desctiptions['specification'];
                        $item_img = $desctiptions['filename'];

                        $batch = $stockdata['batch'];
                        if (array_key_exists($stockdata['warehousestock_itemid'], $stock_status_array)) {
                            
                            $data = array('warehouseId' => $stockdata->warehouse['warehouse_id'],'used_quantity' => $stockdata['consumed_quantity'],'warehouse' => $stockdata->warehouse['warehouse_name'], 'item' => $spc_details, 'item_img' => $item_img, 'item_id' => $stockdata['warehousestock_itemid'], 'dimension' => $stockdata['dimension'], 'batch' => $stockdata['batch'], 'unit' => $stockdata['warehousestock_unit'], 'qty' => $stock_quantity, 'rate' => $stockdata['rate'], 'amount' => $amount, 'date' => $stockdata['warehousestock_date']);
                            array_push($stock_status_array[$stockdata['warehousestock_itemid']], $data);
                            
                        } else {
                            
                            $data = array('warehouseId' => $stockdata->warehouse['warehouse_id'],'used_quantity' => $stockdata['consumed_quantity'],'warehouse' => $stockdata->warehouse['warehouse_name'], 'item' => $spc_details, 'item_img' => $item_img, 'item_id' => $stockdata['warehousestock_itemid'], 'dimension' => $stockdata['dimension'], 'batch' => $stockdata['batch'], 'unit' => $stockdata['warehousestock_unit'], 'qty' => $stock_quantity, 'rate' => $stockdata['rate'], 'amount' => $amount, 'date' => $stockdata['warehousestock_date']);

                            $stock_status_array[$stockdata['warehousestock_itemid']][] = $data;
                            //echo "<pre> final2";print_r($stock_status_array);exit;
                        }
                    }
                }
            }
           
        }
        return $stock_status_array;
    }

    public function actionPendingBills(){
        $pendingBills = $this->getPendingBills();
        if (filter_input(INPUT_GET, 'export') == 'pdf') {
            $this->logo = $this->realpath_logo;
            $mPDF1 = Yii::app()->ePdf->mPDF();
            $mPDF1 = Yii::app()->ePdf->mPDF('', 'A4');
            $sql = 'SELECT template_name '
                . 'FROM jp_quotation_template '
                . 'WHERE status="1"';
            $selectedtemplate =Yii::app()->db->createCommand($sql)->queryScalar();
            
            $template = $this->getTemplate($selectedtemplate);
            $mPDF1->SetHTMLHeader($template['header']);
            $mPDF1->SetHTMLFooter($template['footer']);
            $mPDF1->AddPage('','', '', '', '', 0,0,40, 45, 10,0);
            $mPDF1->WriteHTML($this->renderPartial('pendingbills', array('data'=>$pendingBills), true));
            $mPDF1->Output('PENDING BILLS.pdf', 'D');
            
        } elseif (filter_input(INPUT_GET, 'export') == 'csv') {
            $exported_data = $this->renderPartial('pendingbills', array('data'=>$pendingBills), true);
            $export_filename = 'PENDING BILLS' . date('Ymd_His');
            ExportHelper::exportGridAsCSV($exported_data, $export_filename);
        } else {
            return $this->render('pendingbills',
                array('data'=>$pendingBills)
            );
        }
    }
    public function actionstockCorrectionReport()
    {
  
        $model = new Warehousestock;     
        $final_stock_value_data =array();
        $specification = $this->actionGetItemCategory($parent = 0, $spacing = '', $user_tree_array = '');
        $model->unsetAttributes(); 
        $condition =$condition1 =$condition2= $condition3=$condition4="";
        $final_array=array();
        $array1=array();
        $array2=array();
        $array3=array();


        if (isset($_GET['warehouse_id']) && !empty($_GET['warehouse_id'])) {            
            $whId = $_GET['warehouse_id'];
            $condition1 .= 'AND `warehousereceipt_warehouseid` = '.$whId;
            $condition2 .= 'AND `warehousedespatch_warehouseid` = '.$whId;
            $condition3 .= 'AND `warehouse_id` = '.$whId;
            $condition4 .= 'AND `warehousestock_warehouseid` = '.$whId;
        }
        if (isset($_GET['item_id']) && !empty($_GET['item_id'])) {            
            $condition1 .= " AND warehousereceipt_itemid = " . $_GET['item_id'];
            $condition2 .= " AND warehousedespatch_itemid = " . $_GET['item_id'];
            $condition3 .= " AND Item_id = " . $_GET['item_id'];
            $condition4 .= " AND warehousestock_itemid = " . $_GET['item_id'];
        }
        if (isset($_GET['date_from']) && !empty($_GET['date_from'])) {            
            $date_from = date('Y-m-d', strtotime($_GET['date_from']));
            $condition = " AND created_date >= '" . $date_from . " 00:00:00'";
        } 

        if (isset($_GET['date_to']) && !empty($_GET['date_to'])) {            
            $date_to = date('Y-m-d', strtotime($_GET['date_to']));
            $condition .=" AND created_date <= '" . $date_to . " 23:59:59'";
        } 
        
        if ( (isset($_GET['warehouse_id']) && !empty($_GET['warehouse_id']))){
            $sql = 'SELECT warehousereceipt_id,dimension,
                    warehousereceipt_warehouseid as warehouse,
                    warehousereceipt_itemid as item,
                    warehousereceipt_batch as batch,
                    warehousereceipt_rate as rate,
                    warehousereceipt_accepted_effective_quantity as qty ,created_date
                    FROM `jp_warehousereceipt_items` WHERE `warehousereceipt_rate` != 0
                    ' . $condition1 . $condition . ' GROUP BY created_date';

            $array1 = Yii::app()->db->createCommand($sql)->queryAll();



            $sql = 'SELECT  warehousedespatch_id,dimension,
                    warehousedespatch_warehouseid as warehouse,
                    warehousedespatch_itemid as item,
                    warehousedespatch_batch as batch,
                    warehousedespatch_rate as rate ,
                    warehousedespatch_baseunit_quantity as qty ,created_date
                    FROM `jp_warehousedespatch_items` WHERE  warehousedespatch_rate !=0 
                    ' . $condition2 . $condition . ' GROUP BY created_date';
            $array2 = Yii::app()->db->createCommand($sql)->queryAll();

            $sql = 'SELECT  id as correction_id ,wh_receipt_id,warehouse_id as warehouse,
                    Item_id as item,
                    (stock_quantity-wh_receipt_quantity) as qty ,created_date,rate
                    FROM `jp_warehouse_stock_correction` WHERE approval_status="1" AND 1=1
                    '.$condition3.$condition.' ORDER BY id ASC';
            $array3 = Yii::app()->db->createCommand($sql)->queryAll();
                            
            foreach ($array3 as $key => $value) {
                $array3[$key]['dimension'] = '';
                $array3[$key]['batch'] = '';

            }
        }else{
            $array1=array();
            $array2=array();
            $array3=array();
        }
        

                    
        $final_array = array_merge($array1,$array2, $array3);                                
        $date = array_column($final_array, 'created_date');
        array_multisort($date, SORT_ASC, $final_array);
        $final_stock_value_data =$final_array;              
        
        $warehouseArray=array();
        foreach ($final_array as $key => $value) {
            if (!in_array($value['warehouse'],$warehouseArray)){
            array_push($warehouseArray,$value['warehouse']);
            }
        }

       $data = $this->stockCorrectionData($final_stock_value_data,$warehouseArray);    
        
        return $this->render('_stockcorrection_report', array(
           'model' => $model,
            'specification' => $specification,
            'result_data' => $data
        ));
    }
   
    public function stockCorrectionData($items_model,$warehouseArray){
        
        
        $lists = array();
        $result= array();    
        $subitems = array();    
        foreach ($warehouseArray as $key => $warehouse) {
            
            foreach ($items_model as $quotation_category) {                                 
                $result[$quotation_category['item']][$quotation_category['rate']][]= $quotation_category; 
                $keys = array_keys(array_column($items_model, 'warehouse'), $quotation_category['warehouse']); 
                                                                               
                foreach ($result as $key=>$value) { 
                    $subitems[$key]['count']= count($value);                     
                    $subitems[$key]['details']= $value;                    
                }
                
            }
            
            
            $data = array('warehouse' => $warehouse,
            'count'=>count($keys),
            'item' => $subitems
            );
            array_push($lists, $data);
       }
                             
        return $lists;
    }

    public function getQuantity($warehouse_id, $itemId,$date,$type){
        if($type=='Receipt'){
            $sql = "SELECT SUM(warehousereceipt_accepted_effective_quantity) as quantity 
                FROM `jp_warehousereceipt_items` 
                WHERE `warehousereceipt_warehouseid` = ".$warehouse_id
                . " AND `warehousereceipt_itemid` = ".$itemId." 
                AND `created_date` = '$date'";            
        }
        else{
            $sql = "SELECT SUM(warehousedespatch_baseunit_quantity) as quantity 
                    FROM `jp_warehousedespatch_items` 
                    WHERE `warehousedespatch_warehouseid` = ".$warehouse_id
                    . " AND `warehousedespatch_itemid` =".$itemId." 
                    AND `created_date` = '$date'";            
        }
        $quantity = Yii::app()->db->createCommand($sql)->queryScalar();
        return $quantity;
    }

    public function getOpeningStockQuantity($warehouse_id, $itemId,$date,$rate){
        $sql = "SELECT SUM(warehousereceipt_accepted_effective_quantity) as quantity 
                FROM `jp_warehousereceipt_items` 
                WHERE `warehousereceipt_warehouseid` = " . $warehouse_id 
                . " AND `warehousereceipt_itemid` = " . $itemId . " 
                AND warehousereceipt_rate = ".$rate." AND `created_date` < '$date'";
                // if($warehouse_id ==3 && $itemId==11){
                //     echo $sql;
                // }
               
        $receiptqty = Yii::app()->db->createCommand($sql)->queryScalar();        

        $sql = "SELECT SUM(warehousedespatch_baseunit_quantity) as quantity 
                FROM `jp_warehousedespatch_items` 
                WHERE `warehousedespatch_warehouseid` = " . $warehouse_id 
                . " AND `warehousedespatch_itemid` =" . $itemId . " 
                AND warehousedespatch_rate=".$rate." AND `created_date` < '$date'";
        $despatchqty = Yii::app()->db->createCommand($sql)->queryScalar();
                    
        $quantity =  $receiptqty - $despatchqty;
        return $quantity;
    }

    public function getReceiptQuantity($warehouse_id, $itemId,$date_from,$date_to,$rate){
        $condition = "";
        if ($date_from!="") {            
            $date_from = date('Y-m-d', strtotime($date_from));
            $condition = " AND created_date >= '" . $date_from . " 00:00:00'";
        }
        if ($date_to!="") {            
            $date_to = date('Y-m-d', strtotime($date_to));
            $condition .=" AND created_date <= '" . $date_to . " 23:59:59'";
        }
        $sql = "SELECT SUM(warehousereceipt_accepted_effective_quantity) as quantity 
                FROM `jp_warehousereceipt_items` 
                WHERE `warehousereceipt_warehouseid` = " . $warehouse_id 
                . " AND `warehousereceipt_itemid` = " . $itemId . " 
                  AND warehousereceipt_rate = ".$rate.$condition;
    
        $receiptqty = Yii::app()->db->createCommand($sql)->queryScalar();

        return $receiptqty;
    }

    public function getDespatchQuantity($warehouse_id, $itemId,$date_from,$date_to,$rate){
        $condition = "";
        if ($date_from!="") {            
            $date_from = date('Y-m-d', strtotime($date_from));
            $condition = " AND created_date >= '" . $date_from . " 00:00:00'";
        }
        if ($date_to!="") {            
            $date_to = date('Y-m-d', strtotime($date_to));
            $condition .=" AND created_date <= '" . $date_to . " 23:59:59'";
        }

        $sql = "SELECT SUM(warehousedespatch_baseunit_quantity) as quantity 
                FROM `jp_warehousedespatch_items` 
                WHERE `warehousedespatch_warehouseid` = " . $warehouse_id 
                . " AND `warehousedespatch_itemid` =" . $itemId . " 
                  AND warehousedespatch_rate=".$rate.$condition;
        $despatchqty = Yii::app()->db->createCommand($sql)->queryScalar();
        return $despatchqty;
    }

    public function getStockCorrectionQuantity($warehouse_id, $itemId,$date_from,$date_to,$rate){
        
        $condition = "";
        if ($date_from!="") {            
            $date_from = date('Y-m-d', strtotime($date_from));
            $condition = " AND created_date >= '" . $date_from . " 00:00:00'";
        }
        if ($date_to!="") {            
            $date_to = date('Y-m-d', strtotime($date_to));
            $condition .=" AND created_date <= '" . $date_to . " 23:59:59'";
        } 
        $sql = "SELECT SUM(stock_quantity-wh_receipt_quantity) as qty 
                FROM `jp_warehouse_stock_correction` 
                WHERE `warehouse_id` = " . $warehouse_id 
                . " AND `Item_Id` =" . $itemId . " 
                 AND rate=".$rate.$condition;

        $stockqty = Yii::app()->db->createCommand($sql)->queryScalar();
        return $stockqty;
    }

    public function actiongetProjectlist(){
        $view_type = $_REQUEST['view_type'];
        $wh_type = $_REQUEST['wh_type'];
        $selection="";
        if($view_type==2){
            if($wh_type==1){
                $tblpx = Yii::app()->db->tablePrefix;
                $sql = "SELECT {$tblpx}vendors.name as vendor_name,"
                        . " {$tblpx}vendors.vendor_id as vendor_id,"
                        . " jp_warehousereceipt.warehousereceipt_bill_id"
                        . " FROM {$tblpx}purchase LEFT JOIN {$tblpx}vendors "
                        . " ON {$tblpx}purchase.vendor_id={$tblpx}vendors.vendor_id  "
                        . " LEFT JOIN {$tblpx}bills ON {$tblpx}purchase.p_id = {$tblpx}bills.purchase_id   "
                        . " LEFT JOIN {$tblpx}warehousereceipt "
                        . " ON {$tblpx}warehousereceipt.warehousereceipt_bill_id = {$tblpx}bills.bill_id "
                        . " WHERE jp_warehousereceipt.warehousereceipt_bill_id IS NOT NULL GROUP BY vendor_name";

                $vmodel = Yii::app()->db->createCommand($sql)->queryAll();
                $data = CHtml::listData($vmodel,'vendor_id','vendor_name');                
                $empty = array('empty'=>'-Select Vendor-'); 
                
            }else{
                
                $sql = "SELECT p.name,r.`warehousedespatch_project` "
                        . " FROM `jp_warehousedespatch` r,jp_projects p "
                        . " WHERE p.pid=r.warehousedespatch_project";

                $pmodel = Yii::app()->db->createCommand($sql)->queryAll();
                if(empty($pmodel)){
                    $sql = "SELECT p.name ,w.project_id "
                    . " FROM `jp_warehousedespatch` wd "
                    . " LEFT JOIN jp_warehouse w "
                    . " ON wd.warehousedespatch_warehouseid_to = w.warehouse_id "
                    . " LEFT JOIN jp_projects p ON p.pid = w.project_id "
                    . " GROUP BY w.project_id";

                    $pmodel = Yii::app()->db->createCommand($sql)->queryAll();
                }
                $data = CHtml::listData($pmodel,'project_id','name');
                $empty = array('empty'=>'-Select Project-'); 
            }
        }

        $details =  CHtml::listOptions($selection,$data , $empty); 
        echo json_encode(array('data' => $details));
    }

    public function actionPendingDeliveries(){
        $pendingDeliveries = $this->getPendingDeliveries();

        return $this->render('pendingdeliveries',
            array('data'=>$pendingDeliveries['pending_deliveries_array'])
        );
    }
    
}
