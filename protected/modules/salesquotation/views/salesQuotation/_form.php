<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/message/css/jquery.message.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/message/js/jquery.message.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/moment.js"></script>
<script  type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/tableHeadFixer.js"></script>
<script src="<?php echo Yii::app()->baseUrl . '/ckeditor/ckeditor.js'; ?>"></script>
<style>
    .cke_contents {
        height: 80px !important;
    }

    .panel-body>.panel {
        margin-top: 10px;
    }

    .pb-2 {
        padding-bottom: 10px;
    }

    .h-head {
        height: 45px;
    }

    .add_new_sec {
        margin-bottom: 10px;
        padding: 6px 10px;
    }

    .add_new_sec button {
        border: none;
        box-shadow: none;
    }

    .add_new_sec h4 {
        margin: 0px !important;
        padding-top: 10px;
    }


    .panel-heading h4 {
        margin: 0px !important;
    }

    h4.categ_item {
        color: #337ab7;
        line-height: 3px;
        font-size: 1.5rem;
        font-weight: bold;
    }

    i.toggle-caret {
        font-size: 2rem;
        font-weight: bold;
        color: #3383ca;
    }

    input.total_sum {
        width: 50px;
        margin-right: 3rem;
    }

    .sum_sec {
        max-width: 400px;
        text-align: right;
        margin-left: auto;
        background: #fafafa;
        margin-bottom: 10px;
        font-weight: bold;
        white-space: nowrap;
        padding: 10px;
    }

    .toast-container {
        width: 350px;
    }

    .toast-position-top-right {
        top: 57px;
        right: 6px;
    }

    .toast-item-close {
        background-image: none;
        cursor: pointer;
        width: 12px;
        height: 12px;
        text-align: center;
        border-radius: 2px;
    }

    .toast-item-image {
        font-size: 24px;
    }

    .toast-item-close:hover {
        color: red;
    }

    .toast-item {
        border: transparent;
        border-radius: 3px;
        font-size: 10px;
        opacity: 1;
        background-color: rgba(34, 45, 50, 0.8);
    }

    .toast-item-wrapper p {
        margin: 0px 5px 0px 42px;
        font-size: 14px;
        text-align: justify;
    }


    .toast-type-success {
        background-color: #00A65A;
        border-color: #00A65A;
    }

    .toast-type-error {
        background-color: #DD4B39;
        border-color: #DD4B39;
    }

    .toast-type-notice {
        background-color: #00C0EF;
        border-color: #00C0EF;
    }

    .toast-type-warning {
        background-color: #F39C12;
        border-color: #F39C12;
    }

    .permission_style {
        background: #f8cbcb !important;
    }

    .addRow label {
        display: inline-block;
    }

    input[type="radio"] {
        margin: 4px 4px 0;
    }

    input[type="checkbox"][readonly] {
        pointer-events: none;
    }

    .row-margin {
        padding-bottom: 50px;
    }

    .row-margin-append {
        margin-top: 10px;
        margin-left: 0px;
        margin-right: 0px;
    }

    .button-panel {
        margin-top: 30px;
    }

    .panel-margin {
        padding-left: 30px;
        padding-right: 30px;
    }

    span.select2.select2-container.select2-container--default {
        width: 100% !important;
    }

    .qitem_table tbody tr td {
        vertical-align: bottom;
    }
    table.table tbody tr.catehead td {
        background-color: #c5c5dd;
        font-weight: 600;
        color: #000;
        padding: 15px 10px;}
</style>
<?php if (Yii::app()->user->hasFlash('success')) : ?>
    <div class="flash-holder flash-success">
        <?php echo Yii::app()->user->getFlash('success'); ?>
    </div>
<?php endif; ?>

<?php if (Yii::app()->user->hasFlash('error')) : ?>
    <div class="alert alert-danger  alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <?php echo Yii::app()->user->getFlash('error'); ?>
    </div>
<?php endif; ?>
<div class="alert alert-success" role="alert" style="display:none;">
</div>
<div class="alert alert-danger" role="alert" style="display:none;">
</div>
<div class="alert alert-warning" role="alert" style="display:none;"> </div>

<div id="previous_details"></div>
<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'quotation-form',
    'enableClientValidation' => true,
    'clientOptions' => array('validateOnChange' => true),
    'enableAjaxValidation' => false,
        ));
?>
<div class="row">
    <div class="col-md-3">
        <div class="form-group">
            <input type="hidden" name="Quotation[quotation_id]" id="Quotation_quotation_id" value="">
            <input type="hidden" name="Quotation[total_amount]" id="Quotation_total_amount" value="">
            <?php echo $form->labelEx($model, 'company_id'); ?>
            <?php
            $user = Users::model()->findByPk(Yii::app()->user->id);
            $arrVal = explode(',', $user->company_id);
            $newQuery = "";
            foreach ($arrVal as $arr) {
                if ($newQuery)
                    $newQuery .= ' OR';
                $newQuery .= " FIND_IN_SET('" . $arr . "', id)";
            }
            $companyInfo = Company::model()->findAll(array('condition' => $newQuery));
            ?>
            <?php
            echo $form->dropDownList($model, 'company_id', CHtml::listData(Company::model()->findAll(array('condition' => $newQuery)), 'id', 'name'), array('class' => 'form-control js-example-basic-single field_change invoice_add require', 'empty' => '-Select Company-', 'style' => 'width:100%'));
            ?>
            <?php echo $form->error($model, 'company_id'); ?>
        </div>
    </div>

    <div class="col-md-3">
        <div class="form-group">
            <?php echo $form->labelEx($model, 'client_name'); ?>
            <?php echo $form->textField($model, 'client_name', array('class' => 'form-control ')); ?>
            <?php echo $form->error($model, 'client_name'); ?>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <?php echo $form->labelEx($model, 'address'); ?>
            <?php echo $form->textField($model, 'address', array('class' => 'form-control ')); ?>
            <?php echo $form->error($model, 'address'); ?>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <?php echo $form->labelEx($model, 'phone_no'); ?>
            <?php echo $form->textField($model, 'phone_no', array('class' => 'form-control ', 'value' => $model->phone_no)); ?>
            <?php echo $form->error($model, 'phone_no'); ?>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <?php echo $form->labelEx($model, 'email'); ?>
            <?php echo $form->textField($model, 'email', array('class' => 'form-control ')); ?>
            <?php echo $form->error($model, 'email'); ?>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <label>DATE : </label>
            <input type="text" value="<?php echo ((isset($date) && $date != '') ? date("d-m-Y", strtotime($date)) : ''); ?>" id="datepicker" class="txtBox date inputs target form-control invoice_add require" name="date" placeholder="Please click to edit" readonly="true">
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">

            <?php echo $form->labelEx($model, 'location'); ?>
            <?php
            echo $form->dropDownList($model, 'location_id', CHtml::listData($location_data, 'id', 'name'), array('class' => 'form-control js-example-basic-single field_change invoice_add require', 'empty' => '-Select Location-', 'style' => 'width:100%'));
            ?>
            <?php echo $form->error($model, 'location_id'); ?>

        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <?php
            echo $form->labelEx($model, 'invoice_no');
            echo $form->textField($model, 'invoice_no', array('class' => 'form-control require', 'readonly' => true));
            ?>
            <?php echo $form->error($model, 'invoice_no'); ?>
        </div>
    </div>
</div>
<div class="row">
</div>
<?php $this->endWidget(); ?>

<div id="items_holder">
    <div class="quotationmaindiv" id="items_list">
        <div class="add_new_sec ">
            <div class="clearfix mb-2">
                <div class="pull-left">
                    <h4>Category</h4>
                </div>
            </div>
            <?php
            $main_items = $model->getCategories($model->id);
            $qid = '';
            if (isset($_GET['qid'])) {
                $qid = $_GET['qid'];
            }
            $rev = '';
            if (isset($_GET['rev'])) {
                $rev = $_GET['rev'];
            }
            $status = '';
            if (isset($_GET['status'])) {
                $status = $_GET['status'];
            }
            ?>

            <div class="panel-heading h-head">
                <div class="row">

                    <div class="col-md-2">
                        <?php
                        echo $form->dropDownList($modelsales, 'id', CHtml::listData(QuotationCategoryMaster::model()->findAll(), 'id', 'name'), array('ajax' => array(
                                'type' => 'POST',
                                'url' => Yii::app()->createUrl('salesquotation/SalesQuotation/getquotationitem'),
                                'success' => 'js:function(data)'
                                . '{'
                                . '    $("#quotation_item").html(data);'
                                . '}',
                                'data' => array('category_id' => 'js:this.value', 'form' => $form, 'qid' => $qid, 'rev' => $rev, 'status' => $status),
                            ), 'class' => 'form-control js-example-basic-single field_change require', 'empty' => '-Select Category-', 'style' => 'width:100%'));
                        ?>
                        <?php echo CHtml::hiddenField('category_id_data', '', array('id' => 'category_id_data')); ?>
                    </div>
                    <div class="col-md-2">
                        <?php
                        echo $form->textField($modelsales, 'category_label', array('class' => 'form-control require', 'id' => 'category_label', 'placeholder' => 'category'));
                        ?>
                        <div class="errorMessage"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="" id="quotation_item">
</div>
<div class="" id="quotation_item_edit">
</div><br><br>
<?php
if ($status == 1) {
    if ((!empty($salesquotation_edit) or ! empty($salesquotation_extra_edit))) {
        $this->renderPartial('_quotation_revisionedit', array(
            'modelsales' => $salesquotation_edit,
            'sales_extra' => $salesquotation_extra_edit,
            'rev_no' => $rev,
            'qid' => $qid
        ));
    }
}
?>
<br><br><br>
<div>
    <table cellpadding="10" class="table">
        <thead>
            <tr>
                <th>Sl No</th>
                <th>Work Type</th>
                <th>Work Type Label</th>
                <th>Material</th>
                <th>Finish</th>
                <th>Description</th>
                <th>Rate</th>
                <th>Amount</th>
            </tr>
        </thead>
        <tbody>

            <?php            
            if (!empty($salesquotation)) {
                $i = 1;
                $previous_cat = '';
                foreach ($salesquotation as $value) {
                    $datas = $value['items_list'];
                    $sub_items = $value['sub_items'];
                    $category_id = $value['category_details']['category_id'];
                    $category_name = QuotationCategoryMaster::model()->findByPk($category_id);
                    $category_label = $value['category_details']['category_label'] != '' ? $value['category_details']['category_label'] : $category_name['name'];
                    ?>
                    <tr class="catehead">
                        <td colspan="8">
                            <b><?php echo $category_label; ?></b>
                    </tr>
                    <?php
                    $i = 1;
                    $count = count($datas);
                    foreach ($datas as $key => $item) {
                        $work_type_label = QuotationWorktypeMaster::model()->findByPk($item['work_type']);
                        ?>
                        <tr>
                            <td><?php echo $i; ?></td>
                            <td> <?php echo ($item['worktype_label'] != '') ? $item['worktype_label'] : $work_type_label['name']; ?></td>
                            <td><?php echo $item['worktype_label']; ?></td>
                            <?php
                            $finish_data = Controller::getFinishData();
                            if ($work_type_label['template_id'] == 1) {
                                $sh_description = $item['shutterwork_description'];
                                $css_description = $item['carcass_description'];
                                ?>
                                <td>
                                    <span>
                                        <?php echo $item['shutterwork_material']; ?>
                                    </span><br>
                                    <span>
                                        <?php echo $item['carcass_material']; ?>
                                    </span>
                                </td>
                                <td>
                                    <span>
                                        <?php echo isset($item['shutterwork_finish']) ? $item['shutterwork_finish'] : ''; ?>
                                    </span><br>
                                    <span>
                                        <?php echo isset($item['carcass_finish']) ? $item['carcass_finish'] : ''; ?>
                                    </span>
                                </td>
                            <?php } else if (3 == $work_type_label['template_id']) { ?> 
                                <td><span>
                                        <?php echo $item['material']; ?>
                                    </span></td>
                                <td><span>
                                        <?php echo isset($item['finish']) ? $item['finish'] : ''; ?>
                                    </span></td>
                            <?php } else { ?>
                                <td></td>
                                <td></td>
                            <?php } ?>

                            <?php if ($work_type_label['template_id'] == 1) { ?>
                                <td>
                                    <span> <?php echo $sh_description ?></span><br>
                                    <span><?php echo $css_description ?></span>
                                </td>
                            <?php } else { ?>
                                <td><?php echo $item['description'] ?></td>
                            <?php } ?>
                            <td><?php echo $item['mrp'] ?></td>
                            <td><?php echo $item['amount_after_discount']; ?></td>

                        </tr>
                        <?php
                        $i++;
                    }
                    if(!empty($sub_items)){
                        foreach ($sub_items as $key1 => $item) {
                            $work_type_label = QuotationWorktypeMaster::model()->findByPk($item['work_type']);
                            ?>
                            <tr>
                                <td colspan="8">
                                    <b><?php echo $item['subitem_label']; ?></b>
                                </td>
                            </tr>
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td> <?php echo ($item['worktype_label'] != '') ? $item['worktype_label'] : $work_type_label['name']; ?></td>
                                <td><?php echo $item['worktype_label']; ?></td>
                                <?php
                                $finish_data = Controller::getFinishData();
                                if ($work_type_label['template_id'] == 1) {
                                    $sh_description = $item['shutterwork_description'];
                                    $css_description = $item['carcass_description'];
                                    ?>
                                    <td>
                                        <span>
                                            <?php echo $item['shutterwork_material']; ?>
                                        </span><br>
                                        <span>
                                            <?php echo $item['carcass_material']; ?>
                                        </span>
                                    </td>
                                    <td>
                                        <span>
                                            <?php echo isset($item['shutterwork_finish']) ? $item['shutterwork_finish'] : ''; ?>
                                        </span><br>
                                        <span>
                                            <?php echo isset($item['carcass_finish']) ? $item['carcass_finish'] : ''; ?>
                                        </span>
                                    </td>
                                <?php } else if (3 == $work_type_label['template_id']) { ?> 
                                    <td><span>
                                            <?php echo $item['material']; ?>
                                        </span></td>
                                    <td><span>
                                            <?php echo isset($item['finish']) ? $item['finish'] : ''; ?>
                                        </span></td>
                                <?php } else { ?>
                                    <td></td>
                                    <td></td>
                                <?php } ?>

                                <?php if ($work_type_label['template_id'] == 1) { ?>
                                    <td>
                                        <span> <?php echo $sh_description ?></span><br>
                                        <span><?php echo $css_description ?></span>
                                    </td>
                                <?php } else { ?>
                                    <td><?php echo $item['description'] ?></td>
                                <?php } ?>
                                <td><?php echo $item['mrp'] ?></td>
                                <td><?php echo $item['amount_after_discount']; ?></td>

                            </tr>
                            <?php
                            $i++;
                        }
                    }
                    $previous_cat = $category_id;
                }
            } else {
                ?>
                <tr>
                    <td colspan="9" class="text-center">NO results found</td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
    <?php
    if (!empty($salesquotation_extra)) {
        ?>
        <h3>Extra Work</h3>
        <table cellpadding="10" class="table" >
            <thead>
                <tr>
                    <th>Sl No</th>
                    <th>Description</th>
                    <th>Unit</th>
                    <th>Quantity</th>
                    <th>Quantity No</th>
                    <th>MRP</th>
                    <th>Amount After Discount</th>

                </tr>
            </thead>
            <tbody>
                <?php
                $i = 1;
                foreach ($salesquotation_extra as $sales_extra) {
                    $unit = Unit::model()->findByPk($sales_extra['unit']);
                    ?>
                    <tr>
                        <td><?php echo $i; ?></td>
                        <td><?php echo $sales_extra['description']; ?></td>
                        <td><?php echo $unit['unit_name']; ?></td>
                        <td><?php echo $sales_extra['quantity']; ?></td>
                        <td><?php echo $sales_extra['quantity_nos']; ?></td>
                        <td><?php echo $sales_extra['mrp']; ?></td>
                        <td><?php echo $sales_extra['amount_after_discount']; ?></td>
                    </tr>
                    <?php
                    $i++;
                }
                ?>
            </tbody>
        <?php } ?>
    </table>
</div>
<script>
    var count = 1;
    $(function () {
        $('p#add_field').click(function () {
            count += 1;
            $('#container').append(
                    '<div class="row addRow row-margin-append">' +
                    '<div class="col-md-1">' +
                    '<label for="Clients_Shutter_Work">Extra Work</label>                </div>' +
                    '<div class="col-md-2">' +
                    '<input class="form-control" type="file" name="img" id="img_' + count + '" ><option value=""></input>      ' +
                    '           </div>' +
                    '<div class="col-md-2">' +
                    '<input class="form-control" id="descrition_' + count + '" name="description[]' + '" type="text" placeholder="description"></input>      ' +
                    '           </div>' +
                    '<div class="col-md-1">' +
                    '<select class="form-control" id="sq_feet_' + count + '" name="sq_feet[]' + '" type="text"><option value="">Sq Ft.</option></select>      ' +
                    '          </div>' +
                    '<div class="col-md-1">' +
                    '<input class="form-control" id="qty_' + count + '" name="qty[]' + '" type="text" placeholder="qty"></input>      ' +
                    '           </div>' +
                    '<div class="col-md-1">' +
                    '<input class="form-control" id="qtynos_' + count + '" name="qtynos[]' + '" type="text" placeholder="qty nos"></input>      ' +
                    '           </div>' +
                    '<div class="col-md-2">' +
                    '<input class="form-control" id="mrp_' + count + '" name="mrp[]' + '" type="text" placeholder="MRP"></input>      ' +
                    '           </div>' +
                    '<div class="col-md-2">' +
                    '<input class="form-control" id="amt_dis_' + count + '" name="amt_dis[]' + '" type="text" placeholder="amt after disc"></input>      ' +
                    '           </div>' +
                    '           </div>' +
                    '<br/>');
        });
    });

    $("#quotation-form :input").change(function () {
        var allRequired = true;
        var $inputs = $('#quotation-form :input').filter('.require');
        var values = {};
        $inputs.each(function () {
            if ($(this).val() == '') {
                allRequired = false;
            }
            values[this.name] = $(this).val();
        });
        if (allRequired) {
            $("#quotation-form").submit();
        }
    });
    $("#category_label").change(function () {
        $category_label = $("#category_label").val();
        $("#cat_name").val($category_label);
    });

    $("#SalesQuotation_id").change(function () {
        $category_label = $("#category_label").val();
        $("#cat_name").val($category_label);
    });

    $(".js-example-basic-single").select2();

    $(".company_id").select2();
    $(function () {
        $("#datepicker").datepicker({
            dateFormat: 'dd-mm-yy',
            maxDate: '0'
        }).datepicker("setDate", new Date());
    });

    function countOfLetters(str) {
        var letter = 0;
        for (i = 0; i < str.length; i++) {
            if ((str[i] >= 'A' && str[i] <= 'Z')
                || (str[i] >= 'a' && str[i] <= 'z'))
            letter++;
        }
        return letter;
    }
      

    $("#category_label").keyup(function () {  
        var alphabetCount =  countOfLetters(this.value);  
        var message ="";      
        var disabled=false;
        if(alphabetCount < 1){
            var message = "Invalid Label Name";
            var disabled=true;   
        }     
        $(this).siblings(".errorMessage").html(message);
        var err_count1 = $("#items_holder").find(".errorMessage").not(":empty").length;
        var err_count2 = $("#subitem").find(".errorMessage").not(":empty").length;
        
        if(err_count1 !=0 || err_count2 !=0){            
            $("#additem").attr('disabled',true);
        }else{
            $("#additem").attr('disabled',false)
        }
    });
    $(document).on('keyup','#subitem_label1',function(){        
        var alphabetCount =  countOfLetters(this.value);  
        var message ="";      
        var disabled=false;
        if(alphabetCount < 1){
            var message = "Invalid Label Name";
            var disabled=true;           
        }    
        $(this).siblings(".errorMessage").html(message);
        var err_count1 = $("#items_holder").find(".errorMessage").not(":empty").length;
        var err_count2 = $("#subitem").find(".errorMessage").not(":empty").length;
        
        if(err_count1 !=0 || err_count2 !=0){            
            $("#additem").attr('disabled',true);
        }else{
            $("#additem").attr('disabled',false)
        }  
    });
</script> 



