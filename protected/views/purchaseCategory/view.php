<?php
/* @var $this PurchaseCategoryController */
/* @var $model PurchaseCategory */

$this->breadcrumbs=array(
	'Purchase Categories'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List PurchaseCategory', 'url'=>array('index')),
	array('label'=>'Create PurchaseCategory', 'url'=>array('create')),
	array('label'=>'Update PurchaseCategory', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete PurchaseCategory', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage PurchaseCategory', 'url'=>array('admin')),
);
?>

<h1>View PurchaseCategory #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'parent_id',
		'category_name',
		'specification',
		'spec_flag',
		'created_by',
		'created_date',
	),
)); ?>
