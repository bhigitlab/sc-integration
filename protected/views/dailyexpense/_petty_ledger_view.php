<?php if (filter_input(INPUT_GET, 'export')) { ?>
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/pdfstyle.css">
<?php } ?>
<?php
$users = Users::model()->findByPk($employee_id);
$ledger_style = '';
$table_style = '';
?>
<style>
    .ledger_table {
        border-collapse: collapse;
        <?php if (isset($_GET['ledger']) && $_GET['ledger'] == 1 && isset($_GET['export']) && $_GET['export'] == "PDF") { ?>
            margin: 15px;

        <?php } ?>
    }

    .ledger_table th,
    .ledger_table td {
        border: 1px solid black;
    }

    <?php if (isset($_GET['ledger']) && $_GET['ledger'] == 1 && isset($_GET['export']) && $_GET['export'] == "PDF") {
        $ledger_style = "text-align:center";
        $table_style = "margin-left : 35px !important;";
    } ?>
</style>

<h2 style="<?php echo $ledger_style ?>">Petty Cash Report of <b>
        <?php echo $users->first_name; ?> - Ledger View
    </b></h2>

<?php
$tblpx = Yii::app()->db->tablePrefix;
$final_array = array();

//petty issued
foreach ($dailyexpense as $dailyexp) {
    $dailyexp['key'] = 1;
    $dailyexp['ref'] = isset($dailyexp['bill_id']) ? $dailyexp['bill_id'] : "-";
    $final_array[] = $dailyexp;
}

//petty expense
foreach ($dailyexpense_exp as $dailyexp_exp) {
    $dailyexp_exp['key'] = 2;
    $dailyexp_exp['ref'] = isset($dailyexp_exp['bill_id']) ? $dailyexp_exp['bill_id'] : "-";
    $final_array[] = $dailyexp_exp;
}

foreach ($scpayment as $sc) {
    $sc['key'] = 2;
    $sc['ref'] = "-";
    $final_array[] = $sc;
}
foreach ($daybook as $value) {

    $sql = "SELECT * FROM " . $tblpx . "expenses "
        . " LEFT JOIN " . $tblpx . "users "
        . " ON " . $tblpx . "expenses.employee_id= " . $tblpx . "users.userid "
        . " WHERE " . $tblpx . "expenses.type = 73  "
        . " AND " . $tblpx . "expenses.expense_type=103 " . $where1
        . " AND " . $tblpx . "expenses.projectid = " . $value['pid']
        . " order by " . $tblpx . "expenses.date asc";
    $daybook_details = Yii::app()->db->createCommand($sql)->queryAll();

    foreach ($daybook_details as $dayboopayment) {
        $dayboopayment['key'] = 2;
        $dayboopayment['ref'] = "-";
        $final_array[] = $dayboopayment;
    }

}

//petty refund
foreach ($dailyexpense_refund as $refund) {
    $refund['key'] = 3;
    $refund['ref'] = isset($refund['bill_id']) ? $refund['bill_id'] : "-";
    $final_array[] = $refund;
}

$date = array_column($final_array, 'date');
array_multisort($date, SORT_ASC, $final_array);
// echo "<pre>";print_r($final_array);exit;
$company_address = Company::model()->find(array('order' => ' id ASC'));

?>
<div class="table-responsive">
    <table class="table ledger_table " style="<?php echo $table_style; ?>">
        <thead>
            <tr>
                <th colspan="7">
                    <div>
                        <?php echo isset($company_address->name) ? $company_address->name : ''; ?>
                    </div>
                    <div>
                        <?php echo isset($company_address->address) ? $company_address->address : ''; ?>
                    </div>
                </th>
            </tr>
            <tr>
                <th>Date</th>
                <th>Reff</th>
                <th>Narration</th>
                <th>Petty Cash Issued</th>
                <th>Petty Cash Expense</th>
                <th>Petty Cash Refund</th>
                <th>Balance</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $balanceledger = 0;
            foreach ($final_array as $key => $value) {
                if ($value['key'] == 1) { //issued
                    $balanceledger += $value['amount'];
                } else if ($value['key'] == 2) { //expense
                    $balanceledger += 0 - $value['amount'];
                } else { //refund
                    $balanceledger -= $value['amount'] - 0;
                }
                ?>
                <tr>
                    <td>
                        <?php echo Yii::app()->controller->changeDateForamt($value['date']); ?>
                    </td>
                    <td>
                        <?php echo $value['ref']; ?>
                    </td>
                    <td>
                        <?php echo $value['description']; ?>
                    </td>
                    <td class="text-right">
                        <?php
                        if ($value['key'] == 1) {
                            echo Yii::app()->Controller->money_format_inr($value['amount'], 2);
                        } else {
                            echo "";
                        }
                        ?>
                    </td>
                    <td class="text-right">
                        <?php
                        if ($value['key'] == 2) {
                            if (isset($value["exp_id"])) {
                                //echo "<pre>";print_r($value);exit;
                                ?>
                                <a target="_blank"
                                    href='<?php echo Yii::app()->createUrl("expenses/generateVoucher&id=") . $value["exp_id"] ?>'>
                                    <?php echo Yii::app()->Controller->money_format_inr($value['amount'], 2); ?>
                                </a>
                            <?php
                            } elseif (isset($value["dailyexp_id"])) { ?>
                                <a target="_blank"
                                    href='<?php echo Yii::app()->createUrl("dailyexpense/view&id=") . $value["dailyexp_id"] ?>'>
                                    <?php echo Yii::app()->Controller->money_format_inr($value['amount'], 2); ?>
                                </a>
                            <?php


                            }

                        } else {
                            echo "";
                        }
                        ?>
                    </td>
                    <td class="text-right">
                        <?php
                        if ($value['key'] == 3) {
                            echo Yii::app()->Controller->money_format_inr($value['amount'], 2);
                        } else {
                            echo "";
                        }
                        ?>
                    </td>
                    <td class="text-right">
                        <?php echo Yii::app()->Controller->money_format_inr($balanceledger, 2); ?>
                    </td>
                </tr>

            <?php } ?>
            <tr>
                <?php
                $issued_sum = 0;
                $expense_sum = 0;
                $refund_sum = 0;

                foreach ($final_array as $key => $value) {
                    if ($value['key'] == 1) {
                        $issued_sum += $value['amount'];
                    }

                    if ($value['key'] == 2) {
                        $expense_sum += $value['amount'];
                    }

                    if ($value['key'] == 3) {
                        $refund_sum += $value['amount'];
                    }
                }
                ?>
                <th colspan="3"></th>
                <th class="text-right">
                    <?php echo Yii::app()->Controller->money_format_inr($issued_sum, 2) ?>
                </th>
                <th class="text-right">
                    <?php echo Yii::app()->Controller->money_format_inr($expense_sum, 2) ?>
                </th>
                <th class="text-right">
                    <?php echo Yii::app()->Controller->money_format_inr($refund_sum, 2) ?>
                </th>
                <th></th>
            </tr>
            <tr>
                <th colspan="6" class="text-right">Cash In Hand:</th>
                <th class="text-right">
                    <?php echo Yii::app()->Controller->money_format_inr($balanceledger, 2) ?>
                </th>
            </tr>
        </tbody>
    </table>
</div>