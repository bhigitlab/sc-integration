<?php if(isset($data->project->name)){ ?>
<div class="parent_row">
    <div class="row box-collapse">
        <div class="col-md-1 logo_sec">
        
        <input type="checkbox" name="clearnotificationcheck[]" class="clearnotificationcheck clearcheck_ea" id="clearnotification<?php $index; ?>" value="<?php echo $data['id']; ?>" data-type="expense_notification" style="vertical-align: text-bottom;"/>
       
            <button class="btn_logo">EA</button>
        </div>
        <div class="col-md-9 content_sec">
            <div class="row  clearfix">
                <div class="col-md-8">
                    <div class="list-item <?php echo $data->view_status == 1 ? 'blinking' : '' ?>">
                        <?php
                        if ($data->notification_type == 1) {
                            if (!empty($data->expense_perc)) {
                        ?>
                                Expense reached <b><?php echo isset($data['expense_perc'])?$data['expense_perc']. '%':"" ?></b> of received amount in <b><?php echo isset($data->project->name)?$data->project->name:""; ?></b>.
                            <?php
                            } else {
                            ?>
                                Total expense exceeded received amount in <b><?php echo isset($data->project->name)?$data->project->name:""; ?></b>.
                            <?php
                            }
                        } elseif ($data->notification_type == 2) {
                            ?>
                            Payment limit <b><?php echo isset($data['expense_perc'])?$data['expense_perc']. '%':""  ?></b> exceed in <b><?php echo isset($data->project->name)?$data->project->name:""; ?></b>.
                        <?php
                        }

                        ?>

                    </div>
                </div>
                <div class="col-md-4">
                    <div class="list-item pull-right">
                        <label>Date:</label>
                        <span><?php echo date('d-m-Y', strtotime($data['generated_date'])); ?></span>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-2">
            <div class="list-item text-right act_btn">
                <?php
                if ($data->view_status == 1) {
                ?>
                    <button class="btn btn-xs btn_approve btn_sec view_expense" id="<?php echo $data['id']; ?>">Mark as read</button>
                <?php
                }
                ?>

                <button class="compare_icon popup btn btn-xs btn-default" title="Compare">Details</i></button>
                <button class="box_close btn btn-xs btn-default" style="display:none;">Close</i></button>
            </div>
        </div>
    </div>
    <div class="compare_box popuptext" style="display:none;">
        <div class="box_content">
            <table class="table w-100">
                <thead>
                    <tr>
                        <th class="text-center">Project</th>
                        <?php
                        if ($data->notification_type == 1) {
                            $balance = $data->advance_amount - $data->expense_amount;
                        ?>
                            <th class="text-center">Advance</th>
                            <th class="text-center">Expense</th>
                            <th class="text-center">Balance</th>
                        <?php
                        } elseif ($data->notification_type == 2) {
                        ?>
                            <th class="text-center">Amount</th>
                            <th class="text-center">Profit Percentage</th>
                            <th class="text-center">Project Quote</th>
                        <?php } ?>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="text-center"><?php echo isset($data->project->name)?$data->project->name:""; ?></td>
                        <?php
                        if ($data->notification_type == 1) {
                        ?>
                            <td class="text-center"><?php echo $data->advance_amount; ?></td>
                            <td class="text-center"><?php echo $data->expense_amount; ?></td>
                            <td class="text-center"><?php echo $balance; ?></td>
                        <?php
                        } elseif ($data->notification_type == 2) {
                        ?>
                            <td class="text-center"><?php echo $data->expense_amount; ?></td>
                            <td class="text-center"><?php echo $data->project->profit_margin; ?></td>
                            <td class="text-center"><?php echo $data->project->project_quote; ?></td>
                        <?php
                        }
                        ?>


                    </tr>
                </tbody>
            </table>
        </div>

    </div>
</div>
<?php } ?>
<script>
    
</script>
