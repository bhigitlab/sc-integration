<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.min.js"></script>
<?php
/* @var $this SubcontractorbillController */
/* @var $model Subcontractorbill */

$this->breadcrumbs = array(
    'Subcontractorbills' => array('index'),
    'Create',
);

?>
<div class="container" id="vendors">
    <div class="loading-overlay">
        <span class="fa fa-spinner fa-3x fa-spin"></span>
    </div>
    <div class="expenses-heading">
        <div class="clearfix">
            <!-- remove addentries class -->
            <button type="button" id="billView" href="<?php echo $this->createUrl('subcontractorbill/admin') ?>"
                class="btn btn-info pull-right mt-0 mb-10">View Bills</button>
            <h3>Subcontractor Bill</h3>
            <div id="loading"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/ajax-loader.gif"></div>
        </div>
    </div>

    <div id="errormessage"></div>
    <div class="entries-wrapper">
        <div class="row">
            <div class="col-xs-12">
                <div class="heading-title">Add Subcontractor Bill</div>
                <div class="dotted-line"></div>
            </div>
        </div>
        <!-- <div class="panel-body"> -->
        <?php
        if (!empty($quotation_model)) {

            $this->renderPartial('_quotation_bill_form', array('model' => $model, 'quotation_model' => $quotation_model));
        } else {

            $this->renderPartial('_form', array('model' => $model));
        }
        ?>
        <!-- </div> -->
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#loading').hide();
    });

    $("#Subcontractorbill_date").change(function () {

        var entry_date = $(this).val();
        var id = $("#Subcontractorbill_scquotation_id").val();

        $.ajax({
            type: 'POST',
            dataType: 'json',
            data: {
                scquotation_id: id,
                entry_date: entry_date
            },
            url: "<?php echo Yii::app()->createUrl("subcontractorpayment/checkQuotationDate") ?>",
            success: function (response) {

                if (response.response == "error") {
                    $("#submitButton").attr('disabled', true);
                    $("#errormessage").show()
                        .html('<div class="alert alert-danger">' + response.msg + '</div>');

                } else {
                    $("#errormessage").hide()
                    $("#submitButton").attr('disabled', false);

                }
            }
        })
    })
    $("#billView").click(function () {
        let redirecturl = $(this).attr("href");
        window.location.href = redirecturl;
    });
</script>