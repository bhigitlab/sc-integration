<?php
/* @var $this TasksController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'Tasks',
);

if (yii::app()->user->role <= 2) {
    $this->menu = array(
        array('label' => 'Manage Tasks', 'url' => array('admin')),
       // array('label' => 'Create Tasks', 'url' => array('create')),
    );
}

if (yii::app()->user->role == 6 or yii::app()->user->role == 4) {
    $this->menu = array(
        array('label' => 'Create Tasks', 'url' => array('create')),
    );
}
?>
<div>
<div style="float:left;">
<h2>Tasks (<?php echo ($type=='wop'?'Without project':($type=='odt'?'Overdues':'All')) ;?>)</h2></div>

	<div class="addlink" style="float:left;width: 25%;">
    <?php
    $createUrl = $this->createUrl('create', array("asDialog" => 1, "gridId" => 'address-grid'));
    echo CHtml::link('New Task', '', array('class' => 'edittime', 'onclick' => "$('#cru-frame').attr('src','$createUrl '); $('#cru-dialog').dialog('open');"));
    ?>
</div>


<div class="dash-summary" style="float:right;">

    <ul class="menu">

	        <li>
            <?php
            //For tasks without projects
            echo CHtml::link("Show all", array('tasks/index'));
            ?>
        </li>
        <li>
            <?php
			if(Yii::app()->user->role==1)
			{
				$empty_tasks = Tasks::Model()->count("project_id IS NULL");
			}
			else
			{
            //For tasks without projects
            $empty_tasks = Tasks::Model()->count("coordinator=:co_or AND project_id IS NULL", array("co_or" => yii::app()->user->id));
			}
            echo CHtml::link("Tasks without project" . '<span class="bubble ' . ($empty_tasks == 0 ? 'greenbub' : '') . '">' . $empty_tasks . '</span>', array('tasks/index','type'=>'wop'));
            ?>
        </li>

        <li>           
            <?php
            //For overdue tasks
            $overdues = Tasks::Model()->count("(coordinator=:co_or OR report_to=:co_or) AND due_date<now() AND status!=7", array("co_or" => yii::app()->user->id));
            echo CHtml::link("My overdue tasks" . '<span class="bubble ' . ($overdues == 0 ? 'greenbub' : '') . '">' . $overdues . '</span>', array('tasks/index','type'=>'odt'));
            ?>
        </li>
    </ul>
</div>
<br clear="all" />
</div>

<?php if(Yii::app()->user->role == 1)
{
?>
<div style="float:left;margin-left:30px;font-size: 100% !important;margin-bottom:20px; padding:10px 20px;background-color:#E5F1F4;">
<?php
echo CHtml::dropDownList('projects', '', CHtml::listData(Projects::model()->findAll(
                        array(
                            'select' => array('pid,name'),
                            'order' => 'name',
                            'distinct' => true
                )), "pid", "name"), array('empty' => 'Select a project', 'id' => 'projectid'));
echo "&nbsp; &nbsp; ";
echo CHtml::ajaxSubmitButton("Assign to selected tasks", CHtml::normalizeUrl(array('tasks/assignproject')), array(
    'error' => 'js:function(){
                    alert(\'Error while submit\');
                }',
    'beforeSend' => 'function(){
    var selected_tasks = 0;
    
        if($(\'#projectid\').val()==\'\')
            {
            alert("Please select project to assign"); 
                    return false;
            }

    $("input[name=\'selected_tasks[]\']:checked").each(function(i) {       
        selected_tasks = 1;
    });
    if(selected_tasks==0)
        {
            alert("Please select task(s)"); 
            return false;
        }
            if(!confirm("Are you sure to update selected project to selected below tasks?")){
               return false;}
               
            $(\'#ajax_project_assign_result\').animate({opacity: 1}, 1000).fadeIn("slow");
                $(\'#ajax_project_assign_result\').html("Loading...");
        }',
    'data' => 'js:getSelectedTasks()+"&pid="+$(\'#projectid\').val()',
    'success' => 'js:function(data){
                
                $("input[name=\'selected_tasks[]\']:checked").each(function(i) {       
                    $(this).closest(\'tr\').find("td").eq(3).html("<b>"+$("#projectid :selected").text()+"</b>");
                    $(this).removeAttr(\'checked\');
                });
               // $(\'#ajax_project_assign_result\').animate({opacity: 1}, 1000).fadeIn("slow");
                $(\'#ajax_project_assign_result\').html(data);
                $(\'#ajax_project_assign_result\').animate({opacity: 1.0}, 1500).fadeOut("slow");
                $(\'#ajax_project_assign_result\').hide();
            }',), array());
?>

<?php
Yii::app()->clientScript->registerScript('checkBoxSelect', "
function getSelectedTasks(){
var taskids = [];
 $(\"input[name='selected_tasks[]']:checked\").each(function(i) {
            taskids[i]= $(this).val();
          });
       return \"&checkd_tsk=\"+taskids;
   }
");
?>
    </div>
	<?php
	}
	?>
<div style="color: green; opacity: 1; display: none;font-weight:bold;float:left;clear:right;" id="ajax_project_assign_result">
&nbsp;
</div>

<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'tasks-grid',
    'dataProvider' => $model->search($type),
    'filter' => $model,
    'columns' => array(
        array(
            'id' => 'selected_tasks',
            'class' => 'CCheckBoxColumn',
            'selectableRows' => '50',
        ),
        array('class' => 'IndexColumn', 'header' => 'Sl.No.',),
        array('name' => 'tskid', 'htmlOptions' => array('width' => '40px', 'style' => 'font-weight: bold;text-align:center')),
        array(
            'name' => 'project_id',
            'value' => '(isset($data->project->name)?$data->project->name:"")',
            'type' => 'raw',
            'filter' => CHtml::listData(Projects::model()->findAll(
                            array(
                                'select' => array('pid,name'),
                                'order' => 'name',
                                'distinct' => true
                    )), "pid", "name")
        ),
        /*   array(
          'name' => 'work_type',
          'value' => '$data->worktype0->work_type',
          'type' => 'raw',
          'filter' => CHtml::listData(WorkType::model()->findAll(
          array(
          'select' => array('wtid, work_type'),
          'order' => 'work_type',
          'distinct' => true
          )), "wtid", "work_type")
          ), */
        'title',
        array(
            'name' => 'assigned_to',
            'value' => '$data->assignedTo->first_name',
            'type' => 'raw',
            'filter' => CHtml::listData(Users::model()->findAll(
                            array(
                                'select' => array('userid,first_name'),
				'condition'=>'status=0',
                                'order' => 'first_name',
                                'distinct' => true
                    )), "userid", "first_name")
        ),
    /*    array(
            'name' => 'report_to',
            'value' => '$data->reportTo->first_name',
            'type' => 'raw',
            'filter' => CHtml::listData(Users::model()->findAll(
                            array(
                                'select' => array('userid,first_name'),
                                'order' => 'first_name',
                                'distinct' => true
                    )), "userid", "first_name")
        ),*/
        array(
            'name' => 'coordinator',
            'value' => '$data->coordinator0->first_name',
            'type' => 'raw',
            'filter' => CHtml::listData(Users::model()->findAll(
                            array(
                                'select' => array('userid,first_name'),
				'condition'=>'status=0',
                                'order' => 'first_name',
                                'distinct' => true
                    )), "userid", "first_name"),
            'htmlOptions' => array('style' => 'border-right:1px solid #c2c2c2;')
        ),
        array(
            'header' => 'Billable hrs',
            'value' => 'TimeEntry::model()->find(
                            array(
                                "select" => array("sum(hours) as hours"),
                                "condition" => "billable=\"3\" AND tskid=$data->tskid"
                    ))->hours',
            'type' => 'raw',
            'htmlOptions' => array('width' => '40px', 'style' => 'border:1px solid #c2c2c2;font-weight: bold;text-align:center')
        ),
        array(
            'header' => 'Non-billable hrs',
            'value' => 'TimeEntry::model()->find(
                            array(
                                "select" => array("sum(hours) as hours"),
                                "condition" => "billable=\"4\" AND tskid=$data->tskid"
                    ))->hours',
            'type' => 'raw',
            'htmlOptions' => array('width' => '40px', 'style' => 'border:1px solid #c2c2c2;font-weight: bold;text-align:center')
        ),
        array(
            'header' => 'Total Hours',
            'value' => 'TimeEntry::model()->find(
                            array(
                                "select" => array("sum(hours) as hours"),
                                "condition" => "tskid=$data->tskid"
                    ))->hours',
            'type' => 'raw',
            'htmlOptions' => array('width' => '40px', 'style' => 'border:1px solid #c2c2c2;font-weight: bold;text-align:center')
        ),
        array('name' => 'total_hrs',
            'header' => 'Estimated hrs',
            'htmlOptions' => array('width' => '40px', 'style' => 'border:1px solid #c2c2c2;font-weight: bold;text-align:center')
        ),
        array(
            'name' => 'status',
            'value' => '$data->status0->caption',
            'type' => 'raw',
            'filter' => CHtml::listData(Status::model()->findAll(
                            array(
                                'select' => array('sid,caption'),
                                'condition' => 'status_type="task_status"',
                                'order' => 'status_type',
                                'distinct' => true
                    )), "sid", "caption")
        ),
		'due_date',
        array(
            'class' => 'CButtonColumn',
            'template' => '{view}{update}',
        ),
    ),
));

//--------------------- begin new code --------------------------
// add the (closed) dialog for the iframe
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id' => 'cru-dialog',
    'options' => array(
        'title' => 'New Task',
        'autoOpen' => false,
        'modal' => false,
        'width' => 700,
        'height' => 500,
    ),
));
?>
<iframe id="cru-frame" width="680" style="min-height:430px;"></iframe>

<?php
$this->endWidget();
?>
<div id="id_view"></div>
