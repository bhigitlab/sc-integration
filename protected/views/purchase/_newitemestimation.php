<?php
/* @var $this MaterialEstimationController */
/* @var $data MaterialEstimation */

if ($index == 0) { ?>
    <thead>
        <tr>
            <th>SI No</th>
            
            <th>Material</th>
            <th>Specification</th>
            <th>Remarks</th>
            <th>Quantity</th>
            <th>Unit</th>
            <th>Rate</th>
            <th>Amount</th>
            
        </tr>
    </thead>
<?php } ?>

<tr>
    <td style="width: 50px;"><?php echo $index + 1; ?></td>
   
    <td>
        <?php
        $pms_api_integration_model=ApiSettings::model()->findByPk(1);
        $pms_api_integration =$pms_api_integration_model->api_integration_settings;

        if($pms_api_integration==0){
            $materialModel = Materials::model()->findByPk($data->material);
            echo !empty($materialModel) ? CHtml::encode($materialModel->material_name) : "";
        }
        else{
            $materialModel = ApiMaterials::model()->findByAttributes(array('material_id' => $data->material));
            echo !empty($materialModel) ? CHtml::encode($materialModel->material_name) : "";
        }
        
        ?>
    </td>
    <td>
        <?php
        $specificationModel = Specification::model()->findByPk($data->specification);
        echo !empty($specificationModel) ? CHtml::encode($specificationModel->specification) : "";
        ?>
    </td>
    <td>
        <span><?php echo CHtml::encode($data->remarks); ?></span>
    </td>
    <td>
        <span><?php echo CHtml::encode($data->quantity); ?></span>
    </td>
    <td>
    <span>
        <?php 
        $unitModel = Unit::model()->findByPk($data->unit);
        echo !empty($unitModel) ? CHtml::encode($unitModel->unit_name) : "Unit not found"; 
        ?>
    </span>
    </td>
    <td>
        <span><?php echo CHtml::encode($data->rate); ?></span>
    </td>
    <td>
        <span><?php echo CHtml::encode($data->amount); ?></span>
    </td>
    
</tr>

