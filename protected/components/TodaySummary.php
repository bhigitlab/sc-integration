<?php
/*
 * For top menu module 
 * 
 */
Yii::import('zii.widgets.CPortlet');

class TodaySummary extends CPortlet {
    public function getSummary() {
        $tbl_px = Yii::app()->db->tablePrefix;

        $sql = "select date_format(`entry_date`, \"%d/%m/%Y\") as tentry_date,teid,t.tskid,  
            title as tsk_name ,hours, t.billable, work_type, t.description,  usr.first_name";
        $sql .= " from ".$tbl_px."time_entry as t 
            INNER JOIN ". $tbl_px.'tasks tsk ON tsk.tskid = t.tskid
            INNER JOIN '. $tbl_px.'users as usr on usr.userid=tsk.assigned_to   
                WHERE entry_date BETWEEN (CURDATE() - INTERVAL 7 DAY) AND CURDATE() and userid!=43
                ORDER BY  t.entry_date DESC';
                
                $command = Yii::app()->db->createCommand($sql);
                $command->execute();        
                $work_details = $command->queryAll(); 
        
        return $work_details;
    }

    protected function renderContent() {
        $this->render("todaySummary");
    }
}

?>
