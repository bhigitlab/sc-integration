
SET @rank=38;
UPDATE `jp_scquotation` SET `scquotation_no` = CONCAT('23/ARCH/SCQ-', LPAD(@rank:=@rank+1, 3, '0')) where `scquotation_id` > 39;

update jp_subcontractorbill b
inner join jp_scquotation s on
   b.scquotation_id = s.scquotation_id
set b.bill_number = CONCAT(s.scquotation_no,'-0');

update jp_subcontractorbill t
inner join (
    select id, 
        (select count(*) from jp_subcontractorbill t1 where t1.`scquotation_id` = t.`scquotation_id` and t1.id <= t.id) rn
    from jp_subcontractorbill t
) t1 on t1.id = t.id
set t.`bill_number` = CONCAT(t.bill_number, t1.rn);