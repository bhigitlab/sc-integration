<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.min.js"></script>
<?php
/* @var $this BillsController */
/* @var $model Bills */
/* @var $form CActiveForm */
?>
<style type="text/css">
    table.table .form-control {
        padding: 1px 1px;
        font-size: inherit;
        min-width: 70px;
    }

    table.table tr.pitems td div {
        padding-top: 8px;
    }

    table.table tr.pitems td div .fa {
        color: #060;
    }

    *:focus {
        border: 1px solid #333;
        box-shadow: 0 0 6px 1px rgba(0, 0, 0, 0.25);
    }

    .pitems td .hideinput {
        box-shadow: none;
        border: none;
    }
</style>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/jquery.validationEngine.js"></script>
<script
    src="https://cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/languages/jquery.validationEngine-en.min.js"></script>
<link rel="stylesheet"
    href="https://cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/validationEngine.jquery.min.css" />
<?php if (Yii::app()->user->hasFlash('success')): ?><br>
    <div class="example1consolesucces">
        <?php echo Yii::app()->user->getFlash('success'); ?>
    </div>.select();
<?php endif; ?>

<?php $form = $this->beginWidget('CActiveForm', array(
    'id' => 'bills-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation' => false,
)); ?>
<div class="entries-wrapper">
    <div class="row ">
        <div class="col-xs-12">
            <div class="heading-title">Add Details</div>
            <div class="dotted-line"></div>
        </div>
        <div class="" id="alert">

        </div>
    </div>
    <div class="row">
        <div class="form-group col-xs-12 col-sm-4 col-md-3 col-lg-2">
            <?php echo $form->labelEx($model, 'bill_id'); ?>
            <?php //echo $form->textField($model,'purchase_id'); 
            ?>
            <?php
            $user = Users::model()->findByPk(Yii::app()->user->id);
            $arrVal = explode(',', $user->company_id);
            $newQuery = "";
            foreach ($arrVal as $arr) {
                if ($newQuery)
                    $newQuery .= ' OR';
                $newQuery .= " FIND_IN_SET('" . $arr . "', company_id)";
            }
            if (Yii::app()->user->role == 1 || Yii::app()->user->role == 2) {
                echo $form->dropDownList($model, 'bill_id', CHtml::listData(Bills::model()->findAll(array(
                    'select' => array('bill_id, bill_number'),
                    "condition" => 'purchase_id IS NOT NULL  AND (' . $newQuery . ')',
                    'order' => 'bill_id DESC',
                    'distinct' => true
                )), 'bill_id', 'bill_number'), array('class' => 'form-control js-example-basic-single validate[required]', 'empty' => '-Select Bill-'));
            } else {
                echo $form->dropDownList($model, 'bill_id', CHtml::listData(Bills::model()->findAll(array(
                    'select' => array('bill_id, bill_number'),
                    "condition" => 'purchase_id IS NOT NULL  AND (' . $newQuery . ')',
                    'order' => 'bill_id DESC',
                    'distinct' => true
                )), 'bill_id', 'bill_number'), array('class' => 'form-control js-example-basic-single validate[required]', 'empty' => '-Select Bill-'));
            }
            ?>
            <?php echo $form->error($model, 'bill_id'); ?>
        </div>

        <div class="form-group col-xs-12 col-sm-4 col-md-3 col-lg-2">
            <?php echo $form->labelEx($model, 'return_number'); ?>
            <?php echo $form->textField($model, 'return_number', array('class' => 'form-control validate[required]', 'readonly' => !$model->isNewRecord)); ?>
            <?php echo $form->error($model, 'return_number'); ?>
        </div>
        <div class="form-group col-xs-12 col-sm-4 col-md-3 col-lg-2">
            <?php echo $form->labelEx($model, 'return_date'); ?>
            <?php //echo $form->textField($model,'bill_date',array('class'=>'form-control')); 
            ?>
            <?php echo CHtml::activeTextField($model, 'return_date', array('readonly' => 'true', "value" => (($model->isNewRecord) ? date('d-M-Y') : date('d-M-Y', strtotime($model->bill_date))), 'size' => 10, 'class' => 'form-control')); ?>
            <?php
            $this->widget('application.extensions.calendar.SCalendar', array(
                'inputField' => 'PurchaseReturn_return_date',
                'ifFormat' => '%d-%b-%Y',
            ));
            ?>
            <?php echo $form->error($model, 'return_date'); ?>
        </div>

        <div class="form-group col-xs-12 col-sm-4 col-md-3">
            <?php echo $form->labelEx($model, 'vendor_id'); ?>
            <?php echo $form->hiddenField($model, 'vendor_id', array('id' => 'PurchaseReturn_vendor_id')); ?>
            <?php echo $form->textField($model, 'vendor_name', array('class' => 'form-control validate[required]', 'readonly' => true, 'id' => 'PurchaseReturn_vendor_name')); ?>
            <?php echo $form->error($model, 'vendor_id'); ?>
        </div>
        <div class="form-group col-xs-12 col-sm-8 col-md-3">
            <?php echo $form->labelEx($model, 'remarks'); ?>
            <?php echo $form->textArea($model, 'remarks', array('class' => 'form-control validate[required]', 'rows' => 2, 'autocomplete' => 'off')); ?>
            <?php echo $form->error($model, 'remarks'); ?>
        </div>

        <div id="client">
            <?php echo ((!$model->isNewRecord) ? '' : '') ?>
        </div>
        <div class="col-xs-12 margin-top-10">
            <div class="data-amnts">
                <div>
                    <label class="inline">Amount:</label>
                    <span class="total-value-label w-100p" id="PurchaseReturn_return_amount">
                        <?php echo $model->return_amount ? Controller::money_format_inr($model->return_amount, 2) : 0; ?>
                    </span>
                </div>
                <div>
                    <label class="inline">Discount : </label>
                    <span class="total-value-label w-100p" id="PurchaseReturn_return_discountamount">
                        <?php echo $model->return_discountamount ? Controller::money_format_inr($model->return_discountamount, 2) : 0; ?>
                    </span>
                </div>
                <div>
                    <label class="inline">Tax Amount:</label>
                    <span class="total-value-label w-100p" id="PurchaseReturn_return_taxamount">
                        <?php echo $model->return_taxamount ? Controller::money_format_inr($model->return_taxamount, 2) : 0; ?>
                    </span>
                </div>
                <div>
                    <label class="inline">Total Amount:</label>
                    <span class="total-value-label w-100p" id="PurchaseReturn_return_totalamount">
                        <?php echo $model->return_totalamount ? Controller::money_format_inr($model->return_totalamount, 2) : 0; ?>
                    </span>
                </div>
                <input type="hidden" name="billid" id="billid" value="" />
            </div>
        </div>
    </div>
    <div class="row">
        <div class="form-group col-xs-12 text-right">
            <div class="col-sm-12 text-center">
                <?php echo CHtml::Button($model->isNewRecord ? 'Create' : 'Save', array('class' => 'buttonsubmit btn btn-primary', 'id' => 'buttonsubmit')); ?>
            </div>
        </div>
    </div>
    <?php $this->endWidget(); ?>
</div><!-- form -->
<script>
    <?php $addUrl = Yii::app()->createAbsoluteUrl("PurchaseReturn/addItemsForPurchaseReturn"); ?>
    <?php $vendorUrl = Yii::app()->createAbsoluteUrl("PurchaseReturn/getVendor"); ?>
    <?php $valUrl = Yii::app()->createAbsoluteUrl("PurchaseReturn/validateReturnNumber"); ?>
    <?php $reloadUrl = Yii::app()->createAbsoluteUrl("bills/updateBillsOnReload"); ?>

    $(".js-example-basic-single").select2();

    $(document).ready(function () {
        $('select').first().focus();
    });


    $(document).ready(function () {
        $("#bills-form").validationEngine({
            'custom_error_messages': {
                'custom[number]': {
                    'message': 'Invalid number'
                }
            }
        });
        $("#PurchaseReturn_bill_id").focus();
        //$("#Bills_purchase_id").click();
        //var myDropDown=$("#Bills_purchase_id");
        //var length = $('#Bills_purchase_id>option').length;
        //myDropDown.attr('size',length);
        var billCookie = getCookie("billid");
        var billNoCookie = getCookie("billnumber");
        if (billCookie) {
            $.ajax({
                url: "<?php echo $reloadUrl; ?>",
                data: {
                    "billid": billCookie,
                    "billnumber": billNoCookie
                },
                type: "POST",
                success: function (data) {
                    if (data == 1) {
                        setCookie("billid", "", -1);
                        setCookie("billnumber", "", -1);
                    }
                }
            });
        }
        /*$("body").on("change", ".chkitem", function (e){
             //$nillNumber  = $("#")
             e.preventDefault();
             var lId      = $(this).attr("value");
             var amount   = parseFloat($("#amount"+lId).val());
             var billAmt  = parseFloat($("#Bills_bill_amount").val());
             var billTAmt = parseFloat($("#Bills_bill_totalamount").val());
             if(this.checked) {
                 billAmt  = billAmt + amount;
                 billTAmt = billTAmt + amount;
             } else {
                 billAmt  = billAmt - amount;
                 billTAmt = billTAmt - amount;
             }
             $("#Bills_bill_amount").val(billAmt);
             $("#Bills_bill_totalamount").val(billTAmt);
             $("#Bills_purchase_id").prop("readonly", true);
             //$( "#bills-form" ).submit();
         });
         $("body").on('change', '.table input[name="quantity[]"],.table input[name="rate[]"]', function(e){
             //alert($(this).attr("id"));
             //alert($(this).last().attr("id"));
             var rowId = $(this).parent().attr("id");
             if ($("input[name='chkitem["+rowId+"]']").is(':checked')) {
                 var amount     = parseFloat($("#amount"+rowId).val());
                 var billAmt    = parseFloat($("#Bills_bill_amount").val());
                 var billTAmt   = parseFloat($("#Bills_bill_totalamount").val());
                 var mbAmount   = billAmt - amount;
                 var mbTAmount  = billTAmt - amount;

                 var quantity   = parseFloat($("#quantity"+rowId).val());
                 var rate       = parseFloat($("#rate"+rowId).val());

                 var nAmount    = quantity * rate;
                 var nMBAmount  = mbAmount + nAmount;
                 var nBMTAmount = mbTAmount + nAmount;

                 $("#amount"+rowId).val(nAmount);
                 $("#Bills_bill_amount").val(nMBAmount);
                 $("#Bills_bill_totalamount").val(nBMTAmount);
             }
         });*/
        /*$("body").on("change", ".chkitem", function (e){
            e.preventDefault();
            var lId    = $(this).attr("value");
            var billNo = $("#Bills_bill_number").val();
            var quantity = parseFloat($("#biquantity"+lId).val());
            var availQty = parseFloat($("#availablequantity"+lId).val());
            if(billNo == "") {
                $("#alert").addClass("alert alert-danger").html("<strong>Please enter bill number<strong>");
                //this.checked = false;
                return false;
            } else if(availQty < quantity) {
                $("#alert").addClass("alert alert-danger").html("<strong>Total available quantity is "+availQty+"<strong>");
                //this.checked = false;
                return false;
            } else {
                $("#alert").removeClass("alert alert-danger").html("");
                validateBillNumber(billNo,lId,1);
            }
        });*/
        $("body").on('change', '.table tr.pitems input', function (e) {
            var rowId = $(this).parent().attr("id");
            var aStat;
            var billNo = $("#PurchaseReturn_return_number").val();
            var quantity = parseFloat($("#biquantity" + rowId).val());
            var amount = parseFloat($("#biamount" + rowId).text());
            var damount = parseFloat($("#damount" + rowId).val());
            var dpercent = parseFloat($("#dpercent" + rowId).text());
            var newDAmt = amount * (dpercent / 100);
            if (newDAmt != 0)
                newDAmt = newDAmt.toFixed(2);
            var availQty = parseFloat($("#availablequantity" + rowId).val());
            var crId = $(this).attr("id");
            if (amount < damount && (amount != 0 || quantity == 0)) {
                $("#alert").addClass("alert alert-danger").html("<strong>Discount amount must be less than the amount<strong>");
                $("#damount" + rowId).val(newDAmt);
            } else if (availQty < quantity) {
                $("#alert").addClass("alert alert-danger").html("<strong>Total available quantity is " + availQty + "<strong>");
                $("#biquantity" + rowId).val($("#savedquantity" + rowId).val());
                //$("input[name='chkitem["+rowId+"]']").checked = false;
                //$("input[name='chkitem["+rowId+"]']").attr("disabled", true);
                return false;
            } else if (quantity < 0) {
                $("#alert").addClass("alert alert-danger").html("<strong>Quantity must be greater than or equal to zero<strong>");
                $("#biquantity" + rowId).val(0);
                return false;
            } else if (billNo == "") {
                $("#alert").addClass("alert alert-danger").html("<strong>Please enter return number<strong>");
                this.checked = false;
                return false;
            } else {
                $("#alert").removeClass("alert alert-danger").html("");
                //$("input[name='chkitem["+rowId+"]']").removeAttr("disabled");
                validateBillNumber(billNo, rowId, 2, crId);
            }
        });
        /*$('#bills-form').on('keyup keypress', function(e) {
            var keyCode = e.keyCode || e.which;
            if (keyCode === 13) {
                e.preventDefault();
                return false;
            }
        });*/
        $('#bills-form').on('keypress', ':input', function (event) {
            if (event.keyCode == 13) {
                /* FOCUS ELEMENT */
                var thisId = $(this).attr("id");
                var thisVal = $(this).val();
                //alert(thisId);
                if (thisId == "PurchaseReturn_bill_id" && thisVal == "") {
                    $("#PurchaseReturn_bill_id").click();
                } else {
                    var inputs = $(this).parents("form").eq(0).find(":input");
                    var idx = inputs.index(this);

                    if (idx == inputs.length - 1) {
                        //inputs[0].select()
                    } else {
                        inputs[idx + 1].focus(); //  handles submit buttons
                        inputs[idx + 1].select();
                    }
                    return false;
                }
            }
        });
        $("#PurchaseReturn_return_date").keypress(function (e) {
            if (e.keyCode == 13) {
                $("#biquantity0").focus();
                $("#biquantity0").select();
            }
        });
        $('#bills-form').on('keypress', '.lnext', function (e) {
            //$( "#sgst2" ).keypress(function(e) {
            if (e.keyCode == 13) {
                var rowId = parseInt($(this).parent().attr("id"));
                var totrows = $("#totrows").val();
                var nextId = rowId + 1;
                if (nextId == totrows) {
                    var billNo = $("#PurchaseReturn_return_number").val();
                    if (billNo == "")
                        $("#PurchaseReturn_return_number").focus()
                    else
                        $("#buttonsubmit").focus();
                } else {
                    $("#biquantity" + nextId).focus();
                    $("#biquantity" + nextId).select();
                }
            }
        });
        /*$( "#Bills_bill_totalamount" ).keypress(function(e) {
            if(e.keyCode == 13) {
                $("#buttonsubmit").focus();
            }
        });*/

        /* $( "#buttonsubmit" ).keypress(function(e) {
             if(e.keyCode == 13) {
                 //$("#bills").submit();
                 //alert(1);
                 $(this).closest("form").submit();
             }
         }); */

        $("#buttonsubmit").keypress(function (e) {
            if (e.keyCode == 13) {
                $('.buttonsubmit').click();
            }
        });

        $(".buttonsubmit").click(function () {
            $("#bills-form").submit();
        })


        $("#PurchaseReturn_return_number").change(function () {
            var billNo = $("#PurchaseReturn_return_number").val();
            var crId = "";
            $('#loading').show();
            $.ajax({
                url: "<?php echo $valUrl; ?>",
                data: {
                    "billno": billNo
                },
                type: "GET",
                success: function (data) {
                    if (data == 1) {
                        $("#alert").addClass("alert alert-danger").html("<strong>Return number already exist. Please enter a new bill number.<strong>");
                        //$("#chkitem"+rowId).checked = false;
                        $('#buttonsubmit').prop('disabled', true);
                        return false;
                    } else {
                        $("#alert").removeClass("alert alert-danger").html("");
                        $('#buttonsubmit').prop('disabled', true);
                        var totrows = parseInt($("#totrows").val());
                        for (var i = 0; i < totrows; i++) {
                            var biQuantity = parseFloat($("#biquantity" + i).val());
                            if (biQuantity > 0) {
                                validateBillNumber(billNo, i, 2, crId);
                            }
                        }
                    }
                }
            });
        });
        /*$("body").keydown(function (e){
            if (e.which == 116 || e.which == 17) {
              inFormOrLink = true;
              alert(1);
            }
        });*/
        /*window.onbeforeunload = function() {
            return "Are you sure?";
        }*/
        //$("body form#bills-form input").on("keypress", function(e) {
        /* ENTER PRESSED*/
        //if (e.keyCode == 13) {
        /* FOCUS ELEMENT */
        //var inputs = $(this).parents("form").eq(0).find(":input");
        //var idx = inputs.index(this);

        //if (idx == inputs.length - 1) {
        //inputs[0].select()
        //} else {
        //inputs[idx + 1].focus(); //  handles submit buttons
        //inputs[idx + 1].select();
        //}
        //return false;
        //}
        //});
    });

    function convertEnterToTab() {

        if (event.keyCode == 13) {
            //console.log($(this).next());
            //$(this).next().focus();
            //event.stopPropagation();
            //event.preventDefault();
            //$(this).nextAll("input").eq(0).focus();
        }

    }
    $(document).ready(function () {
        $('#PurchaseReturn_bill_id').on('change', function () {
            var selectedValue = $(this).val();
            $.ajax({
                type: 'POST',
                url: "<?php echo $vendorUrl; ?>",
                data: { id: selectedValue },
                success: function (response) {
                    console.log(response);
                    $('#PurchaseReturn_vendor_id').val('');
                    $('#PurchaseReturn_vendor_name').val('');
                    var vendor = JSON.parse(response);
                    $('#PurchaseReturn_vendor_id').val(vendor.id);
                    $('#PurchaseReturn_vendor_name').val(vendor.name);
                },
                error: function (xhr, status, error) {
                    // Handle any errors
                    console.error(error);
                }
            });
        });
    });
    function setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }

    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }
    //document.onkeydown = convertEnterToTab;
    function addItemToBills(rowId, aStat, crId) {
        $(".formError").hide();
        var bill_Id = $("#PurchaseReturn_bill_id").val();
        var returnNumber = $("#PurchaseReturn_return_number").val();
        var returnDate = $("#PurchaseReturn_return_date").val();
        var totrows = parseInt($("#totrows").val());
        var billId = $("#billid").val();
        var itemId = parseInt($("#ids" + rowId).val());
        var quantity = parseFloat($("#biquantity" + rowId).val());
        quantity = isNaN(quantity) ? 0 : quantity;
        $("#biquantity" + rowId).val(quantity);
        var unit = $("#biunit" + rowId).text();
        var rate = parseFloat($("#birate" + rowId).val());
        var orRate = parseFloat($("#orrate" + rowId).val());
        rate = isNaN(rate) ? orRate : rate;
        if (rate < 0)
            rate = orRate;
        if (quantity % 1 == 0)
            quantity = quantity;
        else
            quantity = quantity.toFixed(2);
        $("#biquantity" + rowId).val(quantity);
        if (rate != 0)
            rate = parseFloat(rate.toFixed(2));
        $("#birate" + rowId).val(rate);
        if (crId == "biquantity" + rowId) {
            $("#birate" + rowId).select();
        }
        var amount = parseFloat($("#biamount" + rowId).text());
        var damount = parseFloat($("#damount" + rowId).val());
        damount = isNaN(damount) ? 0 : damount;
        if (damount < 0)
            damount = 0;
        if (damount != 0)
            damount = parseFloat(damount.toFixed(2));
        $("#damount" + rowId).val(damount);
        if (crId == "birate" + rowId) {
            $("#damount" + rowId).select();
        }
        var dpercent = parseFloat($("#dpercent" + rowId).text());
        var cgst = parseFloat($("#cgst" + rowId).val());
        cgst = isNaN(cgst) ? 0 : cgst;
        if (cgst < 0)
            cgst = 0;
        if (cgst != 0)
            cgst = parseFloat(cgst.toFixed(2));
        $("#cgst" + rowId).val(cgst);
        if (crId == "damount" + rowId) {
            $("#cgst" + rowId).select();
        }
        var cgstpercent = parseFloat($("#cgstpercent" + rowId).text());
        var sgst = parseFloat($("#sgst" + rowId).val());
        sgst = isNaN(sgst) ? 0 : sgst;
        if (sgst < 0)
            sgst = 0;
        if (sgst != 0)
            sgst = parseFloat(sgst.toFixed(2));
        $("#sgst" + rowId).val(sgst);
        if (crId == "cgst" + rowId) {
            $("#sgst" + rowId).select();
        }

        var igst = parseFloat($("#igst" + rowId).val());
        igst = isNaN(igst) ? 0 : igst;
        if (igst < 0)
            igst = 0;
        if (igst != 0)
            igst = parseFloat(igst.toFixed(2));
        $("#igst" + rowId).val(igst);
        if (crId == "igst" + rowId) {
            $("#igst" + rowId).select();
        }




        var sgstpercent = parseFloat($("#sgstpercent" + rowId).text());
        var igstpercent = parseFloat($("#igstpercent" + rowId).text());
        var taxtotal = parseFloat($("#taxamount" + rowId).text());
        var taxpercent = parseFloat($("#taxpercent" + rowId).text());
        var totalamount = parseFloat($("#totalamount" + rowId).text());
        var categoryId = $("#category" + rowId).val();
        var billTotal = parseFloat($("#PurchaseReturn_return_amount").text());
        var billDiscount = parseFloat($("#PurchaseReturn_return_discountamount").text());
        var billTax = parseFloat($("#PurchaseReturn_return_taxamount").text());
        var billGTotal = parseFloat($("#PurchaseReturn_return_totalamount").text());
        var categoryName = $("#bicategoryname" + rowId).text();
        var availQty = parseFloat($("#availablequantity" + rowId).val());
        var billItem = $("#billitem" + rowId).val();
        if (quantity > 0) {
            var newAmt = (quantity * rate).toFixed(2);
            //var newDp         = ((damount / newAmt) * 100);
            var newDp = ((dpercent / 100) * newAmt);
            if (newDp % 1 !== 0)
                newDp = newDp.toFixed(2);
            var newAmtD = newAmt - newDp;
            //var newCgstP      = ((cgst / newAmtD) * 100);
            var newCgstP = ((cgstpercent / 100) * newAmtD);
            if (newCgstP % 1 !== 0)
                newCgstP = newCgstP.toFixed(2);
            //var newSgstP      = ((sgst / newAmtD) * 100);
            var newSgstP = ((sgstpercent / 100) * newAmtD);
            if (newSgstP % 1 !== 0)
                newSgstP = newSgstP.toFixed(2);

            //var newIgstP      = ((igst / newAmtD) * 100);
            var newIgstP = ((igstpercent / 100) * newAmtD);
            if (newIgstP % 1 !== 0)
                newIgstP = newIgstP.toFixed(2);

            var newTotalTax = parseFloat(newCgstP) + parseFloat(newSgstP) + parseFloat(newIgstP);
            newTotalTax = newTotalTax.toFixed(2);
            //var newTaxP       = ((newTotalTax / newAmtD) * 100);
            //var newTaxP       = ((newTotalTax / 100) * newAmtD);
            var newTaxP = taxpercent;
            if (newTaxP % 1 !== 0)
                newTaxP = newTaxP.toFixed(2);
            if (rate == 0) {
                // quantity      = 0;
                //$("#biquantity"+rowId).val(quantity);
            }
            newAmt = isNaN(newAmt) ? 0 : newAmt;
            newDp = isNaN(newDp) ? 0 : newDp;
            newCgstP = isNaN(newCgstP) ? 0 : newCgstP;
            newSgstP = isNaN(newSgstP) ? 0 : newSgstP;
            newIgstP = isNaN(newIgstP) ? 0 : newIgstP;
            newTaxP = isNaN(newTaxP) ? 0 : newTaxP;

            var newTotal = parseFloat(newAmtD) + parseFloat(newTotalTax);
            newTotal = isNaN(newTotal) ? 0 : newTotal;
            newTotal = newTotal.toFixed(2);
        } else {
            var newAmt = 0;
            var newDp = 0;
            var newAmtD = 0;
            var newCgstP = 0;
            var newSgstP = 0;
            var newIgstP = 0;
            var newTotalTax = 0;
            var newTaxP = 0;
            var newTotal = 0;
        }
        $("#biamount" + rowId).text(newAmt ? newAmt : 0);
        $("#damount" + rowId).text(newDp ? newDp : 0);
        $("#cgst" + rowId).text(newCgstP ? newCgstP : 0);
        $("#sgst" + rowId).text(newSgstP ? newSgstP : 0);
        $("#igst" + rowId).text(newIgstP ? newIgstP : 0);
        $("#taxamount" + rowId).text(newTotalTax ? newTotalTax : 0);
        //$("#taxpercent"+rowId).text(newTaxP?newTaxP:0);
        $("#totalamount" + rowId).text(newTotal ? newTotal : 0);
        var billNTotal = 0;
        var billNDiscount = 0;
        var billNTax = 0;
        var billNGTotal = 0;
        for (var i = 0; i < totrows; i++) {
            if ($("#biquantity" + i).val() > 0) {
                billNTotal = billNTotal + parseFloat($("#biamount" + i).text());
                billNDiscount = billNDiscount + parseFloat($("#damount" + i).text());
                billNTax = billNTax + parseFloat($("#taxamount" + i).text());
                billNGTotal = billNGTotal + parseFloat($("#totalamount" + i).text());
            }
        }
        $("#PurchaseReturn_return_amount").text(billNTotal ? billNTotal.toFixed(2) : 0);
        $("#PurchaseReturn_return_discountamount").text(billNDiscount ? billNDiscount.toFixed(2) : 0);
        $("#PurchaseReturn_return_taxamount").text(billNTax ? billNTax.toFixed(2) : 0);
        $("#PurchaseReturn_return_totalamount").text(billNGTotal ? billNGTotal.toFixed(2) : 0);
        if (quantity == 0 && billItem == "") {
            var opStatus = 0;
        } else if (quantity == 0 && billItem != "") {
            var opStatus = 2;
        } else if (quantity > 0 && billItem == "") {
            var opStatus = 1;
        } else if (quantity > 0 && billItem != "") {
            var opStatus = 3;
        } else {
            var opStatus = 2;
        }
        var tqty = parseFloat($("#tqty").val());
        var btqty = parseFloat($("#btqty").val());
        var newQT = 0;
        for (var i = 0; i < totrows; i++) {
            newQT = newQT + parseFloat($("#biquantity" + i).val());
        }
        var newTotalQty = btqty + newQT;
        if (tqty > newTotalQty)
            var purchaseStatus = 94;
        else
            var purchaseStatus = 93;
        //alert(opStatus);
        //alert(crId);
        //if(crId == "sgst"+rowId) {
        $('.loading-overlay').addClass('is-active');
        $.ajax({
            url: "<?php echo $addUrl; ?>",
            data: {
                "bill_id": bill_Id,
                "returnnumber": returnNumber,
                "returndate": returnDate,
                "billamount": billNTotal,
                "billdiscount": billNDiscount,
                "billtax": billNTax,
                "billtotal": billNGTotal,
                "itemid": itemId,
                "quantity": quantity,
                "unit": unit,
                "rate": rate,
                "amount": newAmt,
                "damount": newDp,
                "dpercent": dpercent,
                "cgst": newCgstP,
                "cgstpercent": cgstpercent,
                "sgst": newSgstP,
                "sgstpercent": sgstpercent,
                "igst": newIgstP,
                "igstpercent": igstpercent,
                "totaltax": newTotalTax,
                "totaltaxp": newTaxP,
                "totalamount": newTotal,
                "billid": billId,
                "astat": opStatus,
                "categoryid": categoryId,
                "categoryname": categoryName,
                "availqty": availQty,
                "billitem": billItem,
                "purchasestatus": purchaseStatus
            },
            type: "POST",
            success: function (data) {
                //$("#billid").val(data);
                var result = JSON.parse(data);
                //alert(result[0]+" "+result[1]);
                $("#billid").val(result[0]);
                $("#billitem" + rowId).val(result[1]);
                $("#savedquantity" + rowId).val(quantity);
                setCookie("billid", result[0], 1);
                setCookie("billnumber", returnNumber, 1);
                if (quantity > 0) {
                    $("#tickmark" + rowId).html('<i class="fa fa-check" aria-hidden="true"></i>');
                    $('#buttonsubmit').prop('disabled', false);
                } else {
                    $("#tickmark" + rowId).html("");
                    $('#buttonsubmit').prop('disabled', true);
                }
                //$("#alert").addClass("alert alert-success").html("<strong>Raw has been modified!<strong>");
            }
        });
        //}
    }

    function validateBillNumber(billNo, rowId, stat, crId) {
        $('#loading').show();
        $.ajax({
            url: "<?php echo $valUrl; ?>",
            data: {
                "billno": billNo
            },
            type: "GET",
            success: function (data) {
                if (data == 1) {
                    $("#alert").addClass("alert alert-danger").html("<strong>Return number already exist. Please enter a new bill number.<strong>");
                    $(".chkitem").prop('checked', false);
                    //$("#chkitem"+rowId).checked = false;
                    return false;
                } else if (data == 2) {
                    $("#alert").removeClass("alert alert-danger").html("");
                    $("#PurchaseReturn_return_number").attr("readonly", true)
                    var aStat;
                    if (stat == 1) {
                        if ($("input[name='chkitem[" + rowId + "]']").is(':checked')) {
                            aStat = 1;
                        } else {
                            aStat = 2;
                        }
                        addItemToBills(rowId, aStat, crId);
                    } else if (stat == 2) {
                        if ($("input[name='chkitem[" + rowId + "]']").is(':checked')) {
                            aStat = 3
                        } else {
                            aStat = 4;
                        }
                        addItemToBills(rowId, aStat, crId);
                    }
                }
            }
        });
    }
    $(document).ajaxComplete(function () {
        $('.loading-overlay').removeClass('is-active');
        $('#loading').hide();
    });
</script>
<?php $url = Yii::app()->createAbsoluteUrl("PurchaseReturn/GetItemsByPurchase"); ?>
<?php Yii::app()->clientScript->registerScript('myscript', '
    $(document).ready(function(){
    var bill_id = $("#PurchaseReturn_bill_id").val();
    if(bill_id != "") {
        getAllItems(bill_id);
    }
    $("#PurchaseReturn_bill_id").change(function(){
        var bill_id= $("#PurchaseReturn_bill_id").val();
        if(bill_id == "")
            bill_id = 0;
        getAllItems(bill_id);
    });
    });
    function getAllItems(bill_id) {
        $.ajax({
           url: "' . $url . '",
            data: {"id": bill_id},
            //dataType: "json",
            type: "GET",
            success:function(data){
                //alert(data);
                $("#client").html(data);
                $("#PurchaseReturn_return_number").focus();
                //var amount = $("#aj-amount").val();
                //alert(amount);
                //$("#PurchaseReturn_return_amount").val(amount);
                //$("#PurchaseReturn_return_totalamount").val(amount);
            }
        });
    }
        '); ?>