ALTER TABLE `jp_equipments_items` ADD `rate` FLOAT NULL DEFAULT NULL AFTER `quantity`;
ALTER TABLE `jp_equipments_items` ADD `amount` FLOAT NULL AFTER `rate`;

ALTER TABLE `jp_equipments_estimation` ADD `used_quantity` INT NULL DEFAULT '0' COMMENT 'defines used quantity in equipment entry' AFTER `remarks`;

ALTER TABLE `jp_equipments_estimation` ADD `used_amount` INT(11) NULL DEFAULT '0' COMMENT 'used amount in equipment entry' AFTER `used_quantity`;