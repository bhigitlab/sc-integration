<?php

/**
 * This is the model class for table "{{pre_dailyreport}}".
 *
 * The followings are the available columns in table '{{pre_dailyreport}}':
 * @property integer $dr_id
 * @property integer $ref_id
 * @property integer $followup_id
 * @property string $record_grop_id
 * @property integer $projectid
 * @property integer $company_id
 * @property integer $subcontractor_id
 * @property integer $expensehead_id
 * @property string $date
 * @property string $works_done
 * @property string $materials_unlade
 * @property string $wrktype_and_numbers
 * @property string $extra_work_done
 * @property double $amount
 * @property double $labour
 * @property double $wage
 * @property double $wage_rate
 * @property double $helper
 * @property double $helper_labour
 * @property double $lump_sum
 * @property string $approve_status
 * @property string $description
 * @property integer $created_by
 * @property string $created_date
 * @property integer $updated_by
 * @property string $updated_date
 * @property double $labour_wage
 * @property double $helper_wage
 * @property string $approval_status
 * @property integer $approved_by
 * @property string $record_action
 * @property string $approve_notify_status
 * @property integer $cancelled_by
 * @property string $cancelled_at
 *
 * The followings are the available model relations:
 * @property Company $company
 * @property Users $createdBy
 * @property Projects $project
 * @property Dailyreport $ref
 * @property Subcontractor $subcontractor
 */
class PreDailyreport extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{pre_dailyreport}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('ref_id, amount', 'required'),
            array('ref_id, followup_id, projectid, company_id, subcontractor_id, expensehead_id, created_by, updated_by, approved_by, cancelled_by', 'numerical', 'integerOnly'=>true),
            array('amount, labour, wage, wage_rate, helper, helper_labour, lump_sum, labour_wage, helper_wage', 'numerical'),
            array('approve_status, approval_status, approve_notify_status', 'length', 'max'=>1),
            array('record_action', 'length', 'max'=>300),
            array('record_grop_id, date, works_done, materials_unlade, wrktype_and_numbers, extra_work_done, description, created_date, updated_date, cancelled_at', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('dr_id, ref_id, followup_id, record_grop_id, projectid, company_id, subcontractor_id, expensehead_id, date, works_done, materials_unlade, wrktype_and_numbers, extra_work_done, amount, labour, wage, wage_rate, helper, helper_labour, lump_sum, approve_status, description, created_by, created_date, updated_by, updated_date, labour_wage, helper_wage, approval_status, approved_by, record_action, approve_notify_status, cancelled_by, cancelled_at', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'company' => array(self::BELONGS_TO, 'Company', 'company_id'),
            'createdBy' => array(self::BELONGS_TO, 'Users', 'created_by'),
            'project' => array(self::BELONGS_TO, 'Projects', 'projectid'),
            'ref' => array(self::BELONGS_TO, 'Dailyreport', 'ref_id'),
            'subcontractor' => array(self::BELONGS_TO, 'Subcontractor', 'subcontractor_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'dr_id' => 'Dr',
            'ref_id' => 'Ref',
            'followup_id' => 'Followup',
            'record_grop_id' => 'Record Grop',
            'projectid' => 'Projectid',
            'company_id' => 'Company',
            'subcontractor_id' => 'Subcontractor',
            'expensehead_id' => 'Expensehead',
            'date' => 'Date',
            'works_done' => 'Works Done',
            'materials_unlade' => 'Materials Unlade',
            'wrktype_and_numbers' => 'Wrktype And Numbers',
            'extra_work_done' => 'Extra Work Done',
            'amount' => 'Amount',
            'labour' => 'Labour',
            'wage' => 'Wage',
            'wage_rate' => 'Wage Rate',
            'helper' => 'Helper',
            'helper_labour' => 'Helper Labour',
            'lump_sum' => 'Lump Sum',
            'approve_status' => '1=>approve, 2=>not approve',
            'description' => 'Description',
            'created_by' => 'Created By',
            'created_date' => 'Created Date',
            'updated_by' => 'Updated By',
            'updated_date' => 'Updated Date',
            'labour_wage' => 'Labour Wage',
            'helper_wage' => 'Helper Wage',
            'approval_status' => '0=>Pending,1=>Approved,2=>Rejected',
            'approved_by' => 'Approved By',
            'record_action' => 'Record Action',
            'approve_notify_status' => '0=>no.not visible to admin,1=>yes visible to admin',
            'cancelled_by' => 'Cancelled By',
            'cancelled_at' => 'Cancelled At',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('dr_id',$this->dr_id);
        $criteria->compare('ref_id',$this->ref_id);
        $criteria->compare('followup_id',$this->followup_id);
        $criteria->compare('record_grop_id',$this->record_grop_id,true);
        $criteria->compare('projectid',$this->projectid);
        $criteria->compare('company_id',$this->company_id);
        $criteria->compare('subcontractor_id',$this->subcontractor_id);
        $criteria->compare('expensehead_id',$this->expensehead_id);
        $criteria->compare('date',$this->date,true);
        $criteria->compare('works_done',$this->works_done,true);
        $criteria->compare('materials_unlade',$this->materials_unlade,true);
        $criteria->compare('wrktype_and_numbers',$this->wrktype_and_numbers,true);
        $criteria->compare('extra_work_done',$this->extra_work_done,true);
        $criteria->compare('amount',$this->amount);
        $criteria->compare('labour',$this->labour);
        $criteria->compare('wage',$this->wage);
        $criteria->compare('wage_rate',$this->wage_rate);
        $criteria->compare('helper',$this->helper);
        $criteria->compare('helper_labour',$this->helper_labour);
        $criteria->compare('lump_sum',$this->lump_sum);
        $criteria->compare('approve_status',$this->approve_status,true);
        $criteria->compare('description',$this->description,true);
        $criteria->compare('created_by',$this->created_by);
        $criteria->compare('created_date',$this->created_date,true);
        $criteria->compare('updated_by',$this->updated_by);
        $criteria->compare('updated_date',$this->updated_date,true);
        $criteria->compare('labour_wage',$this->labour_wage);
        $criteria->compare('helper_wage',$this->helper_wage);
        $criteria->compare('approval_status',$this->approval_status,true);
        $criteria->compare('approved_by',$this->approved_by);
        $criteria->compare('record_action',$this->record_action,true);
        $criteria->compare('approve_notify_status',$this->approve_notify_status,true);
        $criteria->compare('cancelled_by',$this->cancelled_by);
        $criteria->compare('cancelled_at',$this->cancelled_at,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return PreDailyreport the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}