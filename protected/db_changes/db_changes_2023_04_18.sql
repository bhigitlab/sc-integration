ALTER TABLE `pms_material_requisition` ADD `type_from` TINYINT NOT NULL DEFAULT '1' COMMENT '1=pms,2=accounts' AFTER `requisition_status`, ADD `project_id` INT  NULL AFTER `type_from`, ADD `task_description` VARCHAR(255)  NULL AFTER `project_id`;

ALTER TABLE `pms_material_requisition` CHANGE `requisition_project_id` `requisition_project_id` INT(11) NULL, CHANGE `requisition_task_id` `requisition_task_id` INT(11) NULL;

ALTER TABLE `pms_material_requisition` ADD FOREIGN KEY (`project_id`) REFERENCES `jp_projects`(`pid`) ON DELETE RESTRICT ON UPDATE CASCADE;
