<?php
/* @var $this TasksController */
/* @var $model Tasks */

$this->breadcrumbs = array(
    'Tasks' => array('index'),
    'Create',
);

$this->menu = array(
    array('label' => 'List Tasks', 'url' => array('index')),
    //array('label' => 'Manage Tasks', 'url' => array('admin')),
);

if (yii::app()->user->role <= 2) {
    $this->menu = array(
        array('label' => 'Manage Tasks', 'url' => array('admin')),
    );
}

?>
<style type="text/css">
    .toggle-field{display:none;}
</style>

<?php if (empty($_GET['asDialog'])) { ?>

<h1>Create Tasks</h1>
<?php }?>
<?php
Yii::app()->clientScript->registerScript('Show all fields', "
$('.toggle-button').click(function(){
	$('.toggle-field').toggle();
	return false;
});
");
?>
<div style="margin-left:400px;"><?php echo CHtml::link('Show all fields', '#', array('class' => 'toggle-button')); ?></div>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>