<?php
class ProjectTypeController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        /* return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view','newlist','create', 'update','update2'),
                   'users' => array('@'),
                     'expression' => 'Yii::app()->user->role==1 || Yii::app()->user->role==2 ',
            ),
            
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update','admin', 'delete','update2'),
                   'users' => array('@'),
                     'expression' => 'yii::app()->user->role==1',     
           ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        ); */
        
           $accessArr = array();
           $accessauthArr = array();
           $controller = Yii::app()->controller->id;
           if(isset(Yii::app()->user->menuall)){
               if (array_key_exists($controller, Yii::app()->user->menuall)) {
                   $accessArr = Yii::app()->user->menuall[$controller];
               } 
           }

           if(isset(Yii::app()->user->menuauth)){
               if (array_key_exists($controller, Yii::app()->user->menuauth)) {
                   $accessauthArr = Yii::app()->user->menuauth[$controller];
               }
           }
           $access_privlg = count($accessauthArr);
           return array(
               array('allow', // allow all users to perform 'index' and 'view' actions
                   'actions' => $accessArr,
                   'users' => array('@'),
                   'expression' => "$access_privlg > 0",
               ),

               array('allow', // allow admin user to perform 'admin' and 'delete' actions
                   'actions' => $accessauthArr,
                   'users' => array('@'),
                   'expression' => "$access_privlg > 0",
               ),
               array('deny',  // deny all users
                   'users'=>array('*'),
               ),
           );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
		
			$model=new ProjectType;

            // Uncomment the following line if AJAX validation is needed
             $this->performAjaxValidation($model);

            if(isset($_POST['ProjectType']))
            {
                    $model->attributes=$_POST['ProjectType'];
                    $model->company_id = Yii::app()->user->company_id;
                    if($model->save())
					$this->redirect(array('newlist'));
            }
             if(isset($_GET['layout']))
                $this->layout = false;

            $this->render('create',array(
                    'model'=>$model,
            ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
         $this->performAjaxValidation($model);

        if (isset($_POST['ProjectType'])) {
            $model->attributes = $_POST['ProjectType'];
            $model->company_id = Yii::app()->user->company_id;
            if ($model->save())
			$this->redirect(array('newlist'));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }
    public function actionUpdate2()
	{
           
            $id = $_POST['id'];
            $value = $_POST['value'];
            $model=$this->loadModel($id);
            $model->project_type = $value;

            // Uncomment the following line if AJAX validation is needed
            // $this->performAjaxValidation($model);
            if($model->save())
            {
                    $data = null;
                    print_r(json_encode($data));
            }else
            {
                    $error = $model->getErrors(); 
                    print_r(json_encode($error['project_type']));
            }
        }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
//    public function actionDelete($id) {
//        $this->loadModel($id)->delete();
//
//        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
//        if (!isset($_GET['ajax']))
//            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
//    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $model = new ProjectType('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['ProjectType']))
            $model->attributes = $_GET['ProjectType'];
        $this->render('index', array(
            'model' => $model,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new ProjectType('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['ProjectType']))
            $model->attributes = $_GET['ProjectType'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model = ProjectType::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'project-type-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
    
    public function actionNewlist(){
            
            $model = new ProjectType('search');
            $model->unsetAttributes();  // clear any default values
            if (isset($_GET['ProjectType']))
                $model->attributes = $_GET['ProjectType'];
                
            $this->render('newlist', array(
                    'model' => $model, 
                    'dataProvider' =>  $model->search(),
                ));
     
    }

    public function actionremoveclientype() {

        $id = $_POST['clientId'];
        $model = $this->loadModel($id);
        $transaction = Yii::app()->db->beginTransaction();
        try {
            if (!$model->delete()) {
                $success_status = 0;
                throw new Exception(json_encode($model->getErrors()));
            } else {
                $success_status = 1;
            }
            $transaction->commit();
        } catch (Exception $error) {
            $transaction->rollBack();
            $success_status = 0;
        } finally {
            if ($success_status == 1) {
                echo json_encode(array('response' => 'success', 'msg' => 'Data Deleted Successfully'));
            } else {
                if ($error->errorInfo[1] == 1451) {
                    echo json_encode(array('response' => 'warning', 'msg' => 'Cannot Delete ! This record alredy in use.'));
                } else {
                    echo json_encode(array('response' => 'error', 'msg' => 'Some Error Occured'));
                }
            }
        }
    }

}
