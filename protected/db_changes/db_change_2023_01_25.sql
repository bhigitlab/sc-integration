UPDATE `jp_bills` SET `company_id` = NULL WHERE `jp_bills`.`company_id` = 0;
ALTER TABLE `jp_purchase` ADD CONSTRAINT `fk_companyid` FOREIGN KEY (`company_id`) REFERENCES `jp_company`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `jp_bills` ADD CONSTRAINT `fk_bill_companyid` FOREIGN KEY (`company_id`) REFERENCES `jp_company`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;
