<?php
/* @var $this MaterialRequisitionController */
/* @var $details MaterialRequisition */


?>

<div class="container">
    <div class="loading-overlay">
        <span class="fa fa-spinner fa-3x fa-spin"></span>
    </div>
    
	<div class="expenses-heading">
                <div class="clearfix">
                  <!-- remove addentries class -->
                  <button
                    type="button"
                    class="btn btn-info pull-right mt-0 mb-10 dailyexpense_btn"
                    href="<?php echo Yii::app()->createAbsoluteUrl('purchaseCategory/purchasereport', array(
                            
                            'project_id' => $_REQUEST['project_id'],
                        
                        )); ?>"
                    id="dailyformbutton"
                  >Purchase Category </button>
                  <h3>Purchase Category </h3>
                  <div id="loading" style="display: none"><img src="/themes/jpvcms/images/ajax-loader.gif" /></div>
                </div>
    </div>
   
 
 <!--PO Items details-->
    <div class="entries-wrapper">
		<div class="row">
        <div class="col-xs-12">
        <div class="heading-title">Purchase Category Details </div>
        <div class="dotted-line"></div>
        <div class="margin-horizontal-10 margin-top-20">
        <div class="horizontal-scroll">
            <table class="table" id="main_table">
                <thead class="entry-table">
                    <tr>
                        <th >Sl.No</th>
                        
                        <th class="text-center">Category Name</th>
                      
                        
                        <th class="text-center">Bill Quantity</th>
                        <th class="text-center">Warehouse</th>
                        <th class="text-center">Unit</th>
                        <th class="text-center">Bill Date</th>
                        <th class="text-center">HSN Code</th>
                        <th class="text-center">Description</th>
                        <th class="text-center">Purchase No</th>
                        <th class="text-center">Bill Number</th>
                        <th class="text-center">Item Total Amount</th>
                        <th class="text-center">Bill Total Amount</th>
                       
                    </tr>
                </thead>
                <tbody class="addrow">
                    <?php if (!empty($details)):
                    $i=1; 
                        ?>
                        
                <?php foreach ($details as $detail): 
                     ?>
                    <tr>
                        <td class="text-center"><?php echo $i; ?></td>
                        <td class="text-center"><?php echo htmlspecialchars($detail['category_name']); ?></td>
                       
                      
                        <td class="text-center"><?php echo htmlspecialchars($detail['billitem_quantity']); ?></td>
                        <?php 
                         $warehouse_name='';
                        $warehouse = Warehouse::model()->findByPk($detail['warehouse_id']);
                        if(!empty( $warehouse)){
                             $warehouse_name= $warehouse->warehouse_name;
                        }
                        ?>
                        <td class="text-center"><?php echo htmlspecialchars($warehouse_name); ?></td>
                         <?php 
                         $unit_name='';
                        $unit = Unit::model()->findByPk($detail['billitem_unit']);
                        if(!empty( $unit)){
                             $unit_name= $unit->unit_name;
                        }
                        ?>
                        <td class="text-center"><?php echo htmlspecialchars($unit_name); ?></td>
                        <td class="text-center"><?php echo htmlspecialchars(DATE('d-m-Y',strtotime($detail['bill_date']))); ?></td>
                        <td class="text-center"><?php echo htmlspecialchars($detail['billitem_hsn_code']); ?></td>
                        <td class="text-center"><?php echo htmlspecialchars($detail['billitem_description']); ?></td>
                        <td class="text-center"><?php echo htmlspecialchars($detail['purchase_no']); ?></td>
                        <td class="text-center"><?php echo htmlspecialchars($detail['bill_number']); ?></td>
                       
                       
                        <td class="text-center"><?php echo Yii::app()->Controller->money_format_inr($detail['itemtotalamount'], 2, 1); ?></td>
                         <td class="text-center"><?php echo Yii::app()->Controller->money_format_inr($detail['bill_totalamount'], 2, 1); ?></td>
                          <?php  $i++ ?>
                    </tr>
                  
                <?php endforeach; ?>
            <?php else: ?>
                <tr>
                    <td colspan="15" class="text-center">No records found.</td>
                </tr>
            <?php endif; ?>
                </tbody>
               
            </table>
        </div>
    </div>
        
       

    </div>    

   
   

</div>
</div>
 
 


<style>
   
</style>
<script>
    
     function reloadParent() {
            location.reload();
    }
    $(document).ready(function() {
        $('#poModal .clsbtn').on('click', function () {
           
            console.log('Modal is now hidden');
            window.parent.location.reload();
        });
    $('#addPoButton,#editPoButton').click(function() {
        var mrId = $("#mrId").val();
        var baseUrl = '<?php echo Yii::app()->baseUrl; ?>';
        var url = baseUrl + '/index.php?r=materialRequisition/loadPoModal&mrId=' + mrId;
        $('.loading-overlay').addClass('is-active');
        // Perform AJAX request
        $.get(url, function(data) {
            // Load response into modal body
            $('#poModal .modal-body').html(data);
            // Show modal
            $('#poModal').modal('show');
            // Initialize select2 after the modal is shown and the content is loaded
            $('#poModal .select2').select2({
                dropdownParent: $("#poModal")
            });
            $('.loading-overlay').removeClass('is-active');
        }).fail(function() {
            $('#poModal .modal-body').html('<p>Error loading information</p>');
        });
    });

    $('#billModal .clsbtn').on('click', function () {
           
           console.log('Modal is now hidden');
           window.parent.location.reload();
       });
   $('#addBillButton,#editBillButton').click(function() {
       var mrId = $("#mrId").val();
       var baseUrl = '<?php echo Yii::app()->baseUrl; ?>';
       var url = baseUrl + '/index.php?r=bills/loadbillModal&mrId=' + mrId;
       $('.loading-overlay').addClass('is-active');
        $.get(url, function(data) {
          
          $('#billModal').modal('show');
           $('#billModal .modal-body').html(data);
           
           
            $('#billModal .select2').select2({
               dropdownParent: $("#billModal")
           });
           $('.loading-overlay').removeClass('is-active');

       }).fail(function() {
           $('#billModal .modal-body').html('<p>Error loading information</p>');
       });
   });


          

    });
	$(".dailyexpense_btn").click(function(){
        var redirecturl = $(this).attr("href");
        window.location.href=redirecturl;
    });
   
        
</script>