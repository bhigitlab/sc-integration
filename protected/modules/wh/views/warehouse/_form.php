<?php
/* @var $this ClientsController */
/* @var $model Clients */
/* @var $form CActiveForm */
?>

<div>

    <?php
    $whchecked = "";
    $view_stocks_in_other_warehouse_checked = "";

    if ($auto_receipt_to_warehouse_status == 1) {
        $whchecked = "checked";
    }

    if ($view_stocks_in_other_warehouse_status == 1) {
        $view_stocks_in_other_warehouse_checked = "checked";
    }

    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'warehouse-form',
        'enableAjaxValidation' => true,
        'enableClientValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true,
            'validateOnType' => false,
        ),
    ));
    $relation_to_project = 0;
    $project_id = null;
    $style = "style='display:none;'";
    $checked = false;

    if ($model->project_id != "") {
        $style = "";
        $checked = true;
        $relation_to_project = 1;
        $project_id = $model->project_id;
    }
    ?>



        <div class="row">
            <div class="form-group col-xs-12 col-md-4">
                <input type="hidden" id="hidden_relation_to_project" name="Warehouse[hidden_relation_to_project]" value="<?php echo $relation_to_project; ?>">
                <input type="hidden" id="hidden_project_id" name="Warehouse[hidden_project_id]" value="<?php echo $project_id; ?>">

                <?php echo $form->labelEx($model, 'warehouse_name'); ?>
                <?php echo $form->textField($model, 'warehouse_name', array('size' => 60, 'maxlength' => 200, 'class' => 'form-control')); ?>
                <?php echo $form->error($model, 'warehouse_name'); ?>
            </div>
            <div class="form-group col-xs-12 col-md-4">
                <?php echo $form->labelEx($model, 'warehouse_place'); ?>
                <?php echo $form->textField($model, 'warehouse_place', array('size' => 60, 'maxlength' => 200, 'class' => 'form-control')); ?>
                <?php echo $form->error($model, 'warehouse_place'); ?>
            </div>
            <div class="form-group col-xs-12 col-md-4">
                <?php echo $form->labelEx($model, 'warehouse_incharge'); ?>
                <?php echo $form->dropDownList($model, 'warehouse_incharge', CHtml::listData(Users::model()->findAll(array('select' => array('userid', 'concat(first_name," ",last_name) as first_name'), 'order' => 'userid DESC')), 'userid', 'first_name'), array('empty' => '-Choose a User-', 'class' => 'form-control')); ?>
                <?php echo $form->error($model, 'warehouse_incharge'); ?>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-xs-12 col-md-4">
                <?php echo $form->labelEx($model, 'assigned_to'); ?>                
                <ul class="checkboxList">
                    <?php
                    $typelist = Users::model()->findAll(array('select' => array('userid', 'concat(first_name," ",last_name) as first_name')));
                    $assigned_company_array = array();
                    $assigned_types_array = '';
                    if (!$model->isNewRecord) {
                        $assigned_types = Warehouse::model()->find(array('condition' => 'warehouse_id=' . $model->warehouse_id));
                        $assigned_types_array = explode(",", $assigned_types->assigned_to);
                    }
                    echo CHtml::checkBoxList('Warehouse[assigned_to]', $assigned_types_array, CHtml::listData($typelist, 'userid', 'first_name'), array('checkAll' => 'Check all', 'template' => '<li class="checkboxtype">{input}{label}</li>', 'separator' => ''));
                    ?>
                </ul>
                <?php echo $form->error($model, 'assigned_to'); ?>
            </div>
            <div class="form-group col-xs-12 col-md-4">
                <?php echo $form->labelEx($model, 'warehouse_address'); ?>
                <?php echo $form->textArea($model, 'warehouse_address', array('rows' => 6, 'cols' => 50, 'class' => 'form-control')); ?>
                <?php echo $form->error($model, 'warehouse_address'); ?>
            </div>
            <?php
            $edit_project_condition = $model->warehouseStockCheck($model->warehouse_id);

            if (!$model->isNewRecord && !empty($model['project_id'])) {
                $edit_project_condition = $model->warehouseStockCheck($model->warehouse_id);
            }
            ?>
            <div class="form-group col-xs-12 col-md-4 margin-top-15">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="form-check">
                            <?php echo $form->checkBox($model, 'relation_to_project',
                                    array('id' => 'relation_to_project',
                                        'class' => 'relation_to_project form-check-input',
                                        'checked' => $checked, 'disabled' =>
                                        $edit_project_condition == 2 ? true : ''));?>
                            <?php echo $form->labelEx($model, 'relation_to_project',array('class' => 'form-check-label')); ?>
                            <?php echo $form->error($model, 'relation_to_project'); ?>
                        </div>
                    </div>
                    <div class="col-xs-12 project" <?php echo $style; ?>>
                        <?php echo $form->dropDownList(
                                 $model,
                                'project_id',
                                CHtml::listData(Projects::model()->findAll(
                                                array(
                                                    'select' => array('pid', 'name'),
                                                    'order' => 'pid DESC'
                                                )
                                        ), 'pid', 'name'),
                                array(
                                    'empty' => '-Choose a Project-',
                                    'class' => 'form-control mandatory',
                                    'disabled' =>
                                    $edit_project_condition == 2 ? true : ''
                                )
                        );?>
                        <?php echo $form->labelEx($model, 'project_id',array('class' => 'form-check-label')); ?>                
                        <?php echo $form->error($model, 'project_id');?>
                    </div>
                    <div class="col-xs-12">
                        <div class="form-check">
                            <input type="checkbox" id="auto_receive_to_project_warehouse" value ="1" <?php echo $whchecked; ?> class="form-check-input"> 
                            <label class="form-check-label font-weight-normal"> Auto receive item for project</label>
                        </div>
                    </div> 
                    <div class="col-xs-12">
                        <div class="form-check">
                            <input type="checkbox" id="view_stocks_in_other_warehouse" 
                                   value ="2" <?php echo $view_stocks_in_other_warehouse_checked; ?> class="form-check-input">
                            <label class="form-check-label font-weight-normal"> View stocks in other warehouse(<i>for factory in-charge only</i>) </label> 
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="form-check">
                            <?php echo $form->checkBox($model,'auto_debit',array('class'=> 'popermission form-check-input','onchange' =>'valueChanged()')); ?>
                            <?php echo $form->labelEx($model,'auto_debit',array('class' => 'form-check-label')); ?>
                        </div>
                        <?php echo $form->error($model,'auto_debit'); ?>
                    </div>
                </div>                
            </div>    
        </div>
        <div class="row">
            <div class="form-group col-xs-12 btn-container">
                <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-primary')); ?>
                <?php
                if (!$model->isNewRecord) {

                    echo CHtml::Button('close', array('class' => 'btn btn-othe', 'onclick' => 'closeaction(this,event);'));
                } else {
                    echo CHtml::ResetButton('Reset', array('class' => 'btn btn-default btnreset'));
                    echo CHtml::Button('close', array('class' => 'btn btn-othe', 'onclick' => 'closeaction(this,event);'));
                }
                ?>
            </div>
        </div>
    <?php $this->endWidget(); ?>
</div>
<script>
    $(document).ready(function () {
        $('.relation_to_project').val(0);
    });
    $(document).on("change", "#relation_to_project", function (e) {
        e.preventDefault();
        if (this.checked) {
            $('.relation_to_project').val(1);
            $('#hidden_relation_to_project').val(1);
            $('.project').show();
        } else {
            $('.relation_to_project').val(0);
            $('#Warehouse_project_id').val('');
            $('#hidden_relation_to_project').val(0);
            $('#hidden_project_id').val('');
            $('.project').hide();
        }
    });
    $(document).on("change", "#Warehouse_project_id", function (e) {
        e.preventDefault();
        var project = $('#Warehouse_project_id').val();
        $('#hidden_project_id').val(project);
    });
    $('#auto_receive_to_project_warehouse').click(function () {
        var general_settings_id = $('#auto_receive_to_project_warehouse').val();
        var auto_receive_to_project_warehouse_status = 0;

        if ($('#auto_receive_to_project_warehouse').is(":checked")) {
            var auto_receive_to_project_warehouse_status = 1;
        }

        $.ajax({
            type: "POST",
            data: {
                'id': general_settings_id,
                'status': auto_receive_to_project_warehouse_status
            },
            url: '<?php echo Yii::app()->createAbsoluteUrl('wh/warehouse/AutoReceiptToWarehouse'); ?>',
            success: function (response) {
                alert(response);
            }
        });
    });

    $('#view_stocks_in_other_warehouse').click(function () {
        var general_settings_id = $('#view_stocks_in_other_warehouse').val();
        if ($('#view_stocks_in_other_warehouse').is(":checked")) {
            var view_stocks_in_other_warehouse_status = 1;
        } else {
            var view_stocks_in_other_warehouse_status = 0;
        }
        $.ajax({
            type: "POST",
            data: {
                'id': general_settings_id,
                'status': view_stocks_in_other_warehouse_status
            },
            url: '<?php echo Yii::app()->createAbsoluteUrl('wh/warehouse/ViewStocksInOtherWarehouse'); ?>',
            success: function (response) {
                alert(response);
            }
        });
    });

</script>