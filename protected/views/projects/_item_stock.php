<table class="table table-bordered">
    <tr bgcolor="#eee">
        <td colspan="8"><b>Dispatch Details</b></td>
    </tr>
    <tr>
        <th>Despatch No</th>
        <th>Warehouse From </th>
        <th>Warehouse To</th>
        <th>Qty</th>  
        <th>Amount</th>                                                                                                        
    </tr>
    <?php
    foreach ($dispatch_details as $data) {
    ?>
        <tr>
            <td><?= isset($data['warehousedespatch_no']) ? $data['warehousedespatch_no'] : '-' ?></td>
            <td>
            <?php 
            $wh_from = Warehouse::model()->findByPk($data['warehousedespatch_warehouseid']);
            echo $wh_from['warehouse_name'];            
            ?></td>
            <td>
            <?php 
            $wh_to = Warehouse::model()->findByPk($data['warehousedespatch_warehouseid_to']);
            echo $wh_to['warehouse_name'];
             ?></td>
            <td><?php echo $data['warehousedespatch_quantity'] ?></td>
            <td class="text-right"><?php 
            $sql = "SELECT SUM(`warehousedespatch_baserate`) "
                . " FROM `jp_warehousedespatch_items` "
                . " WHERE `warehousedespatch_id`=".$data['warehousedespatch_id'];
                
            $rate = Yii::app()->db->createCommand($sql)->queryScalar();
            echo Controller::money_format_inr($rate,2); 
            ?></td>
        </tr>
    <?php } ?>
</table>