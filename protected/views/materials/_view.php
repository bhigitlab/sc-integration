<?php
/* @var $this MaterialsController */
/* @var $data Materials */
?>
<style>
    .legend {
        list-style: none;
        padding-left: 0px;

    }

    .legend li {
        float: left;
        margin-right: 10px;
    }

    .legend span {
        border: 1px solid #ccc;
        float: left;
        width: 12px;
        height: 12px;
        margin: 2px;
    }

    .legend .project_not_mapped {
        background-color: #A9E9EC;
        border: 2px solid #f8cbcb;
        margin-right: 4px;
    }
</style>
<?php
if ($index == 0) {
    ?>
    <?php 
$pms_api_integration=ApiSettings::model()->pmsIntegrationStatus();
if($pms_api_integration==1){
?>
    <ul class="legend">
            <li><span class="project_not_mapped"></span> Material Not Mapped In Integration</li>    
    </ul>
    <?php } ?>
    <thead>
    <tr> 
        <th>SI No</th>
        <th>Name</th>
        <th>Material Unit</th>
        <th>Specification</th>
        <?php 
        if(isset(Yii::app()->user->role) && (in_array('/materials/update', Yii::app()->user->menuauthlist))){
        ?>
        <th>Action</th>
        <?php } ?>
    </tr>   
    </thead>
<?php } ?>
<?php
 if($data->pms_material_id !=='0'){
        $styleval= '';
    }else{
        $styleval='background-color:#A9E9EC;';
    }             
?>
    <tr style="<?php echo $styleval; ?>">
        
        <td style="width: 50px;"><?php echo $index+1; ?></td>
        <td>
            <span><?php echo $data->material_name ?></span>            
        </td>        
        <td>
			<span>
            <?php
                    // Assuming data->material_unit is a comma-separated string of IDs
                    $unitIds = explode(',', $data->material_unit);
                    $unitNames = [];

                    foreach ($unitIds as $unitId) {
                        $unit = Unit::model()->findByPk($unitId);
                        if (isset($unit)) {
                            $unitNames[] = $unit->unit_name;
                        }
                    }

                    // Join the unit names with commas
                    echo implode(', ', $unitNames);
                ?>
			</span>
        </td>
        <td>
            <span>
                <?php
                    $specIds = explode(',', $data->specification);
                    $specNames = [];

                    foreach ($specIds as $specId) {
                        $spec = Specification::model()->findByPk($specId);
                        if (isset($spec)) {
                            $specNames[] = $spec->specification;
                        }
                    }
                    echo implode(', ', $specNames);
                ?>
            </span>
        </td>

        <td style="width: 50px;">
        <?php 
            if(isset(Yii::app()->user->role) && (in_array('/materials/update', Yii::app()->user->menuauthlist))){
            ?>        
           <a class="fa fa-edit editProject" data-toggle="modal" data-target=".edit" data-id="<?php echo $data->id; ?>"></a>
         <?php } ?>
         <?php
            $pms_api_integration=ApiSettings::model()->pmsIntegrationStatus();

            if ($pms_api_integration != 1) {
                if (isset(Yii::app()->user->role) && in_array('/materials/delete', Yii::app()->user->menuauthlist)) {
                    ?>
                    <a class="fa fa-trash deleteProject" data-toggle="modal" data-target=".delete" data-id="<?php echo $data->id; ?>"></a>
                    <?php
                }
            }
            ?>

        </td>        
    </tr>  
