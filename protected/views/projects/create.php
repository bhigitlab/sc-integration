<?php
/* @var $this ProjectsController */
/* @var $model Projects */

$this->breadcrumbs = array(
    'Projects' => array('index'),
    'Create',
);
/*
  $this->menu=array(
  array('label'=>'List Projects', 'url'=>array('index')),
  array('label'=>'Manage Projects', 'url'=>array('admin')),
  ); */
?>

<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" onclick="javascript:window.location.reload()">&times;</button>
            <h4 class="modal-title">Add Projects</h4>
        </div>
        <?php echo $this->renderPartial('_form', array('model' => $model)); ?>
    </div>

</div>
