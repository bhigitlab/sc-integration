<?php
/* @var $this MenuPermissionsController */
/* @var $model MenuPermissions */

$this->breadcrumbs=array(
	'Menu Permissions'=>array('index'),
	$model->mp_id,
);

$this->menu=array(
	array('label'=>'List MenuPermissions', 'url'=>array('index')),
	array('label'=>'Create MenuPermissions', 'url'=>array('create')),
	array('label'=>'Update MenuPermissions', 'url'=>array('update', 'id'=>$model->mp_id)),
	array('label'=>'Delete MenuPermissions', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->mp_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage MenuPermissions', 'url'=>array('admin')),
);
?>

<h1>View MenuPermissions #<?php echo $model->mp_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'mp_id',
		'user_id',
		'menu_id',
	),
)); ?>
