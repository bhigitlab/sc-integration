<?php
/* @var $this WarehousestockController */
/* @var $model Warehousestock */

$this->breadcrumbs=array(
	'Warehousestocks'=>array('index'),
	$model->warehousestock_id=>array('view','id'=>$model->warehousestock_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Warehousestock', 'url'=>array('index')),
	array('label'=>'Create Warehousestock', 'url'=>array('create')),
	array('label'=>'View Warehousestock', 'url'=>array('view', 'id'=>$model->warehousestock_id)),
	array('label'=>'Manage Warehousestock', 'url'=>array('admin')),
);
?>

<h1>Update Warehousestock <?php echo $model->warehousestock_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>