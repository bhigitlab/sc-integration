<?php

/**
 * This is the model class for table "{{scquotation_items}}".
 *
 * The followings are the available columns in table '{{scquotation_items}}':
 * @property integer $item_id
 * @property integer $scquotation_id
 * @property string $item_description
 * @property string $item_date
 * @property double $item_amount
 * @property integer $created_by
 * @property string $created_date
 * @property string $approve_status
 */
class ScquotationItems extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{scquotation_items}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('scquotation_id, item_description, item_date, item_amount, created_by, created_date, approve_status', 'required'),
			array('scquotation_id, created_by', 'numerical', 'integerOnly'=>true),
			array('item_amount', 'numerical'),
			array('approve_status', 'length', 'max'=>3),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('item_id, scquotation_id, item_description, item_date, item_amount, created_by, created_date, approve_status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'item_id' => 'Item',
			'scquotation_id' => 'Scquotation',
			'item_description' => 'Item Description',
			'item_date' => 'Item Date',
			'item_amount' => 'Item Amount',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
			'approve_status' => 'Approve Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('item_id',$this->item_id);
		$criteria->compare('scquotation_id',$this->scquotation_id);
		$criteria->compare('item_description',$this->item_description,true);
		$criteria->compare('item_date',$this->item_date,true);
		$criteria->compare('item_amount',$this->item_amount);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('approve_status',$this->approve_status,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ScquotationItems the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
