<?php
if($data['deletepending_data'] != 'null'){
    $editrequest  = Deletepending::model()->findByPK($data['deletepending_id']);
        $deletedata   = json_decode($editrequest->deletepending_data);    
        if(!empty($deletedata)){
        $tblpx      = Yii::app()->db->tablePrefix;
        $parent_scquotation=array();
        $class="";
        if($data['deletepending_table'] == "{$tblpx}expenses") {
            $section = "Daybook";
            if($deletedata->type == 72)
                $amount = $deletedata->receipt;
            else
                $amount = $deletedata->paid;

        } else if($data['deletepending_table'] == "{$tblpx}dailyexpense") {
            $section = "Dailyexpense";
            if($deletedata->exp_type == 72)
                $amount = $deletedata->dailyexpense_receipt;
            else if($deletedata->exp_type == 73)
                $amount = $deletedata->dailyexpense_paidamount;
            else
                $amount = 0;
                
        } else if($data['deletepending_table'] == "{$tblpx}dailyvendors") {
            $section    = "Vendor Payment";
            $amount     = $deletedata->amount + $deletedata->tax_amount;
        } else if($data['deletepending_table'] == "{$tblpx}subcontractor_payment") {
            $section    = "Subcontractor Payment";
            $amount     = $deletedata->amount + $deletedata->tax_amount;
        } else if($data['deletepending_table'] == "{$tblpx}dailyreport") {
            $section    = "Daily Report";
            $amount     = $deletedata->amount;
        } else if($data['deletepending_table'] == "{$tblpx}bills") {
            $section    = "Bills";
            $amount     = "";
        } else if($data['deletepending_table'] == "{$tblpx}quotation") {
            $section    = "Quotation";
            $amount     = $deletedata->amount;
        } else if($data['deletepending_table'] == "{$tblpx}invoice") {
            $section    = "Invoice";
            $amount     = $deletedata->amount;
        } else if($data['deletepending_table'] == "{$tblpx}sc_payment_stage") {
            $parent_scquotation =Scquotation::model()->findByPk($deletedata->quotation_id) ;
            if(empty($parent_scquotation)){
                $class="hide";
            }

            $section    = "Subcontractor Payment Stage";            
            $quotation_amount =Scquotation::model()->getTotalQuotationAmount($deletedata->quotation_id) ;
            $amount  = $quotation_amount * ($deletedata->stage_percent / 100);                        
        } else if($data['deletepending_table'] == "{$tblpx}subcontractor") {
            $section    = "Subcontractor";
            $amount     = "";
        }else {
            $section    = "";
            $amount     = "";
        }
        if($section != "") {
        ?>
        <div class="parent_row">
            <div class="row box-collapse">
                <div class="col-md-1 logo_sec nowrap">
                    <input type="checkbox" name="deleterequest" class="deleterequestcheck" id="deleterequest<?php $index; ?>" value="<?php echo $data->deletepending_id; ?>" style="vertical-align: text-bottom;margin-right: 3px;"/>
                    <button class="btn_logo">DR</button>
                </div>
                <div class="col-md-8 content_sec">
                    <div class="row  clearfix">
                        <div class="col-md-8">
                        <?php
                            $users = Users::model()->findByPk($data['user_id']);
                            ?>
                            <div class="list-item"><b><?php echo $users->first_name." ".$users->last_name; ?></b> Requested to delete <b><?php echo $section; ?></b> section.</div>
                            <div class="list-item"> Description : <b><?php echo isset($deletedata->description)?$deletedata->description:""; ?></b> 
                            <?php if($amount !=""){ ?> &nbsp; &nbsp; Amount : <b><?php echo $amount; ?></b><?php } ?></div>               
                        </div>            
                        <div class="col-md-4">
                            <div class="list-item pull-right">
                                <label>Date:</label>
                                <span><?php echo date('d-m-Y',strtotime($data['requested_date'])); ?></span>
                        
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="list-item text-right act_btn">
                        <button class="btn btn-xs btn_approve btn_sec approve_delete" id="<?php echo $data['deletepending_id']; ?>">Approve</button>
                        
                        &nbsp;
                        <button class="btn btn-xs btn_decline btn_sec cancel_delete <?php echo $class?>" id="<?php echo $data['deletepending_id']; ?>">Decline </button>&nbsp;
                        <!-- <button class="compare_icon popup btn btn-xs btn-default  <?php echo $class?>" title="Compare">Details</i></button>
                        <button class="box_close btn btn-xs btn-default" style="display:none;">Close</i></button> -->
                    </div>
                </div>
            </div>
            <div class="compare_box popuptext" style="display:none;">
                <div class="box_content">
                    
                </div>
            </div>
        </div>
    <?php } } 
}
?>