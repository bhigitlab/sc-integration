-- for release note
CREATE TABLE `jp_release_note` (
  `id` int(11) NOT NULL,
  `showed_users` text DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;
INSERT INTO `jp_release_note` (`id`, `showed_users`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES ('1', NULL, current_timestamp(), NULL, NULL, NULL);
ALTER TABLE `jp_release_note`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jp_release_note_created_by_fk` (`created_by`),
  ADD KEY `jp_release_note_updated_by_fk` (`updated_by`);
ALTER TABLE `jp_release_note`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
ALTER TABLE `jp_release_note`
  ADD CONSTRAINT `jp_release_note_created_by_fk` FOREIGN KEY (`created_by`) REFERENCES `jp_users` (`userid`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `jp_release_note_updated_by_fk` FOREIGN KEY (`updated_by`) REFERENCES `jp_users` (`userid`) ON DELETE SET NULL ON UPDATE CASCADE;