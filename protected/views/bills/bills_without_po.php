<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$this->breadcrumbs=array(
	'performa invoice',)
?>
<div class="container" id="project">

    <div class="clearfix">
        <div class="add-btn pull-right">
        <a href="<?php echo Yii::app()->createUrl('bills/Addbill') ?>" class="button">Add Bill</a>
        </div>
        <h2>Bills</h2>
    </div>
   
<?php 

 $this->renderPartial('_newsearch1', array('model' => $model)) ?>

 


<?php if(Yii::app()->user->hasFlash('success')):?>
    <div class="info" style="width: 500px;margin-left: 400px;padding: 8px;color: green;font-size: 20px;font-weight: bold;">
        <?php echo Yii::app()->user->getFlash('success'); ?>
    </div>
<?php endif; ?>


<?php if(Yii::app()->user->hasFlash('error')):?>
    <div class="info" style="width: 500px;margin-left: 400px;padding: 8px;color: red;font-size: 20px;font-weight: bold;">
        <?php echo Yii::app()->user->getFlash('error'); ?>
    </div>
<?php endif; ?>



<br>
<div class="">
<?php
    $dataProvider->sort->defaultOrder='bill_id DESC';
    $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_newview_without_po',
        'enableSorting'=>1,
        'sortableAttributes'=>array(
            'bill_id' => 'Bill Id',
            'bill_number'=>'Bill Number',
            'bill_date' => 'Bill Date',
            'created_date' => 'Created Date'
        ),
        'template' => '<div class="clearfix"><div class="pull-left">{sorter}</div><div class="pull-right">{summary}</div></div><div class="tblwrap scrollTable"><div class="container1"><table cellpadding="10" class="table">{items}</table></div></div>{pager}'
	//'itemView' => '_newview', 'template' => '<div>{summary}{sorter}</div><div class="tblwrap scrollTable"><div class="container1"><table cellpadding="10" class="table  list-view sorter">{items}</table></div></div>{pager}',
)); ?>
</div>
</div>

<style>
a.button {
    background-color: #6a8ec7;
    display: block;
    padding: 5px;
    color: #fff;
    cursor: pointer;
    float: right;
    border: 1px solid #6a8ec8;
}
</style>

<style>
 .container1 {
        width: 100%;
        height: 500px;
        overflow: auto;
        position: relative;
    }

th,
.top-left {
	background: #eee;
}
.top-left {
	box-sizing: border-box;
	position: absolute;
	left: 0;
	border-right: 1px solid #ccc;
	border-bottom: 1px solid #ccc;
}

table {
   
	border-spacing: 0;
	position: absolute;
   
}
th,
td {
	border-right: 1px solid #ccc !important;

}

</style>
