<?php

class ApiLogs
{
    public static function apiLog($slug, $method, $request = array())
    {
        $model = new ApiLog;
        $model->slug = $slug;
        $model->method = $method;
        $model->request = json_encode($request);
        $model->status = 0;
        $model->created_by = yii::app()->user->id;
        $model->created_date = new CDbExpression('NOW()');
        $model->updated_date = new CDbExpression('NOW()');
        if($model->save()){
            return $model->id;
        }else{
            return false;
        }

    }
    
    public static function apiLogSuccess($id,$response)
    {
        $model = ApiLog::model()->findByPk($id);
        $model->status = 1;
        $model->response = json_encode($response);
        return $model->save();
    }
    public static function apiPendingRequestRun()
    {
        $logs = ApiLog::model()->findAll('status=:status', array(':status' =>0));        
        foreach($logs as $log){
            $globalapi_response = GlobalApiCall::croneCallApi($log->slug, $log->method, json_decode($log->request,true),$log->id);
        }
        return true;
        
    }
}