<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.js"></script>
<div class="container">
    <div class="loading-overlay">
        <span class="fa fa-spinner fa-3x fa-spin"></span>
    </div>
    <div class="clearfix">
        <div class="pull-left">
            <h2>Voucher Details</h2>
        </div>
        <?php echo CHtml::link('Vouchers', array('vouchers/index'), array('class' => 'btn btn-primary pull-right mt')); ?>

    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="mt-0">Add Entry</h4>
        </div>
        <div class="panel-body">
            <?php if (Yii::app()->user->hasFlash('success')) : ?>
                <div class="alert alert-success alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <?php echo Yii::app()->user->getFlash('success'); ?>
                </div>
            <?php endif; ?>


            <?php if (Yii::app()->user->hasFlash('error')) : ?>
                <div class="alert alert-danger  alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <?php echo Yii::app()->user->getFlash('error'); ?>
                </div>
            <?php endif; ?>
            <?php $form = $this->beginWidget('CActiveForm', array(
                'id' => 'vouchers-form',
                'enableClientValidation' => true,
                'clientOptions' => array('validateOnSubmit' => true),
            )); ?>
            <div class="row">
                <div class="col-md-2">
                    <input type="hidden" name="Vouchers[id]" id="Vouchers_id" value="<?php echo $model->id; ?>">
                    <div class="form-group">
                        <?php echo $form->labelEx($model, 'voucher_for'); ?>
                        <div>
                            <?php
                            if ($model->isNewRecord)
                                $model->voucher_for = 1;
                            echo $form->radioButtonList(
                                $model,
                                'voucher_for',
                                array(
                                    '1' => 'Subcontractor', '2' => 'Vendor'
                                ),
                                array(
                                    'onChange' => CHtml::ajax(array(
                                        'type' => 'POST',
                                        'url' => Yii::app()->createUrl('vouchers/dynamicproject'),
                                        'dataType' => 'JSON',
                                        'success' => 'js:function(data)'
                                            . '{'
                                            . '    $("#Vouchers_project").html(data.projects_list);'
                                            . '    $("#Vouchers_voucher_for_user_id").html(data.voucher_for_list);'
                                            . '}',
                                        'data' => array('company_id' => 'js:$(\'#Vouchers_company\').val()', 'voucher_type' => 'js:this.value'),
                                    )),
                                    'labelOptions' => array('style' => 'display:inline'), // add this code
                                    'separator' => '  '
                                )
                            ); ?>
                            <?php echo $form->error($model, 'voucher_for'); ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <?php echo $form->labelEx($model, 'company'); ?>
                        <div>
                            <?php
                            echo $form->dropDownList($model, 'company', CHtml::listData(Company::model()->findAll(array('condition' => 'id IN (' . $user_companies . ')')), 'id', 'name'), array('ajax' => array(
                                'type' => 'POST',
                                'url' => Yii::app()->createUrl('vouchers/dynamicproject'),
                                'dataType' => 'JSON',
                                'success' => 'js:function(data)'
                                    . '{'
                                    . '    $("#Vouchers_project").html(data.projects_list);'
                                    . '    $("#Vouchers_voucher_for_user_id").html(data.voucher_for_list);'
                                    . '}',
                                'data' => array('company_id' => 'js:this.value', 'voucher_type' => 'js:$(\'input[name="Vouchers[voucher_for]"]:checked\').val()'),
                            ), 'class' => 'form-control js-example-basic-single field_change', 'empty' => '-Select Company-', 'style' => 'width:100%'));
                            ?>
                            <?php echo $form->error($model, 'company'); ?>
                        </div>
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="form-group">
                        <?php echo $form->labelEx($model, 'project'); ?>
                        <div>
                            <?php
                            echo $form->dropDownList($model, 'project', CHtml::listData(Projects::model()->findAll(array('condition' => 'company_id IN (' . $user_companies . ')')), 'pid', 'name'), array('ajax' => array(
                                'type' => 'POST',
                                'url' => Yii::app()->createAbsoluteURL('expenses/dynamicDropdown'),
                                'dataType' => 'JSON',
                                'success' => 'js:function(data)'
                                    . '{'
                                    . '    $("#Vouchers_expense_head").html(data.expenses);'
                                    . '}',
                                'data' => array('project' => 'js:this.value', 'bill' => 0, 'expId' => 0)
                            ), 'class' => 'form-control js-example-basic-single', 'empty' => '-Select Project-', 'style' => 'width:100%'));
                            ?>
                            <?php echo $form->error($model, 'project'); ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <?php echo $form->labelEx($model, 'voucher_for_user_id'); ?>
                        <div>
                            <?php
                            $lists = array();
                            if (!$model->isNewRecord) {
                                if ($model->voucher_for == 1) {
                                    $lists = CHtml::listData(Subcontractor::model()->findAll(array('condition' => 'company_id IN (' . $user_companies . ')')), 'subcontractor_id', 'subcontractor_name');
                                } elseif ($model->voucher_for == 2) {
                                    $lists = CHtml::listData(Vendors::model()->findAll(array('condition' => 'company_id IN (' . $user_companies . ')')), 'vendor_id', 'name');
                                }
                            }

                            echo $form->dropDownList($model, 'voucher_for_user_id', $lists, array('class' => 'form-control js-example-basic-single', 'empty' => '-Select-', 'style' => 'width:100%'));
                            ?>
                            <?php echo $form->error($model, 'voucher_for_user_id'); ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <?php echo $form->labelEx($model, 'voucher_date'); ?>
                        <div>
                            <?php
                            if ($model->isNewRecord)
                                $model->voucher_date = date('Y-m-d');
                            echo $form->textField($model, 'voucher_date', array('class' => 'form-control')); ?>
                            <?php echo $form->error($model, 'voucher_date'); ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <?php echo $form->labelEx($model, 'expense_head'); ?>
                        <div>
                            <?php
                            echo $form->dropDownList($model, 'expense_head', CHtml::listData(ExpenseType::model()->findAll(array('condition' => 'company_id IN (' . $user_companies . ')')), 'type_id', 'type_name'), array('class' => 'form-control js-example-basic-single', 'empty' => '-Select Expense-', 'style' => 'width:100%'));
                            ?>
                            <?php echo $form->error($model, 'expense_head'); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2">
                    <div>
                        <?php echo $form->labelEx($model, 'vehicle_number'); ?>
                        <div>
                            <?php echo $form->textField($model, 'vehicle_number', array('size' => 60, 'maxlength' => 100, 'class' => 'form-control')); ?>
                            <?php echo $form->error($model, 'vehicle_number'); ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div>
                        <?php echo $form->labelEx($model, 'voucher_number'); ?>
                        <div>
                            <?php
                            if ($model->isNewRecord)
                                $model->voucher_number = $model->getMaxId();
                            echo $form->textField($model, 'voucher_number', array('size' => 60, 'maxlength' => 100, 'class' => 'form-control', 'readonly' => true)); ?>
                            <?php echo $form->error($model, 'voucher_number'); ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div>
                        <?php echo $form->labelEx($model, 'staff_id'); ?>
                        <div>
                            <?php
                            echo $form->dropDownList($model, 'staff_id', CHtml::listData(Users::model()->findAll(array('condition' => 'user_type = 6')), 'userid', 'first_name'), array('class' => 'form-control js-example-basic-single', 'empty' => '-Select User-', 'style' => 'width:100%'));
                            ?>
                            <?php echo $form->error($model, 'staff_id'); ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div>
                        <?php echo $form->labelEx($model, 'received_by'); ?>
                        <div>
                            <?php echo $form->textField($model, 'received_by', array('size' => 60, 'maxlength' => 100, 'class' => 'form-control')); ?>
                            <?php echo $form->error($model, 'received_by'); ?>
                        </div>
                    </div>
                </div>
            </div>
            <?php $this->endWidget(); ?>
            <?php $this->renderPartial('_items_form', array('model' => $model, 'items_model' => $items_model, 'voucher_items' => $voucher_items));
            ?>
        </div>
    </div>
    <div class="daybook">
        <div id="daybook-entry">
            <?php
            if (!$model->isNewRecord) {
                $this->renderPartial('item_lists', array('voucher_items' => $voucher_items));
            }
            ?>
        </div>
    </div>
</div>
<script>
    $(function() {
        $("#Vouchers_voucher_date").datepicker({
            dateFormat: 'dd-mm-yy'
        });

    });
    $(document).ready(function() {
        $(".js-example-basic-single").select2();
        $("#vouchers-form :input").change(function() {
            var allRequired = true;
            var required = $('input,textarea,select').filter('[required]:visible');
            required.each(function() {
                if ($(this).val() == '') {
                    allRequired = false;
                }
            });
            if (allRequired) {
                $("#vouchers-form").submit();
            }

        });
        setTimeout(function() {
            $('.alert').fadeOut('fast');
        }, 1000);
        var voucher_for = $('input[name="Vouchers[voucher_for]"]:checked').val();
        if (voucher_for == 1) {
            $("label[for='Vouchers_voucher_for_user_id").html("Subcontractor ");
            $('.for_sc').addClass('show');
        } else if (voucher_for == 2) {
            $("label[for='Vouchers_voucher_for_user_id").html("Vendor ");
            $('.for_sc').addClass('hide');
        }
        $("label[for='Vouchers_voucher_for_user_id").append("<span class=\"required\">*</span>");
        $('input[type=radio][name="Vouchers[voucher_for]"]').change(function() {
            if (this.value == '1') {
                $("label[for='Vouchers_voucher_for_user_id").html("Subcontractor ");
            } else if (this.value == '2') {
                $("label[for='Vouchers_voucher_for_user_id").html("Vendor ");
            }
            $("label[for='Vouchers_voucher_for_user_id").append("<span class=\"required\">*</span>");
        });

    });
    $(document).on('click', '.delete_item', function(e) {
        var answer = confirm("Are you sure you want to delete?");
        if (answer) {
            $('.loading-overlay').addClass('is-active');
            var rowId = $(this).attr("id");
            $.ajax({
                type: "POST",
                data: {
                    id: rowId,
                },
                url: "<?php echo Yii::app()->createUrl("vouchers/deleteitem") ?>",
                success: function(data) {
                    location.reload();
                }
            })
        }

    })
    $(document).ajaxComplete(function() {
        $('.loading-overlay').removeClass('is-active');
        $('#loading').hide();
    });
</script>