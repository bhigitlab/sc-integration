<?php if (filter_input(INPUT_GET, 'export')) { ?>
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/pdfstyle.css">
<?php } ?>
<?php
$this->breadcrumbs = array(
    'Projects' => array('index'),
    'Manage',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});

$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('projects-grid', {
		data: $(this).serialize()
	});
	return false;
});

   /* $('#download').click(function () {
        html2canvas($('#financialreport1'), {
            onrendered: function (canvas) {
                var imgData = canvas.toDataURL('image/jpeg');
                var imgWidth = 210;
                var pageHeight = 295;
                var imgHeight = canvas.height * imgWidth / canvas.width;
                var heightLeft = imgHeight;
                var doc = new jsPDF('p', 'mm');
                var position = 0;
                doc.page = 1; // use this as a counter.
                doc.addImage(imgData, 'jpeg', 0, position, imgWidth, imgHeight);
                heightLeft -= pageHeight;
                while (heightLeft >= 0) {
                    position = heightLeft - imgHeight;
                    doc.addPage();
                    doc.addImage(imgData, 'jpeg', 0, position, imgWidth, imgHeight);
                    heightLeft -= pageHeight;
                }
                doc.save('financialreport.pdf');
            }
        });
    });*/

    /*
        var doc = new jsPDF();
        var specialElementHandlers = {
        '#editor': function (element, renderer) {
        return true;
        }
        };
        $(document).ready(function() {
        $('#download').click(function () {
        doc.fromHTML($('#financialreport1').html(), 15, 15, {
        'width': 170,
        'elementHandlers': specialElementHandlers
        });
        doc.save('sample-content.pdf');
        });
        });*/

");

$tblpx = Yii::app()->db->tablePrefix;
?>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/tableHeadFixer.js"></script>
<style type="text/css">
    .grandtotal td {
        border: none !important;
    }

    #financialreport1 h3 {
        color: inherit;
        text-align: left;
        margin: 15px 0px;
    }

    #parent1,
    #parent2 {
        max-height: 300px;
        overflow: auto;
    }

    .img-hold {
        width: 10%;
    }

    .companyhead {
        font-size: 18px;
        font-weight: bold;
        margin-top: 0px;
        color: #789AD1;
    }


    .tab1 th,
    .tab1 td {
        border: 1px solid #ddd;
        padding: 5px 10px;
    }

    .tab2 th,
    .tab2 td {
        border: 1px solid #ddd;
        padding: 5px 10px;

    }
</style>

<div class="container" id="project">
    <div id="financialreport1">
        <div class="expenses-heading header-container">
            <h3>Financial Report:
                <?php echo date('d-M-Y'); ?>
            </h3>
            <div class="btn-container">
                <?php if (!filter_input(INPUT_GET, 'export')) { ?>
                    <button type="button" class="btn btn-info">
                        <a href="<?php echo Yii::app()->request->requestUri . "&export=csv"; ?>"
                            style="text-decoration: none; color: white;" title="SAVE AS EXCEL">
                            <i class="fa fa-file-excel-o button-icon" aria-hidden="true"></i>
                        </a>
                    </button>
                    <button type="button" class="btn btn-info">
                        <a href="<?php echo Yii::app()->request->requestUri . "&export=pdf"; ?>"
                            style="text-decoration: none; color: white;" title="SAVE AS PDF">
                            <i class="fa fa-file-pdf-o button-icon" aria-hidden="true"></i>
                        </a>
                    </button>
                <?php } ?>
            </div>
        </div>

        <?php
        $tblpx = Yii::app()->db->tablePrefix;
        $company_address = Company::model()->findBypk(Yii::app()->user->company_id);
        ?>
        <?php
        $spacing = "";
        if (filter_input(INPUT_GET, 'export') == 'pdf') {
            $spacing = "style='margin:0px 30px'";

            ?>
            <br><br>
            <h4 class="text-center">Financial Report :
                <?php echo date('d-M-Y'); ?>
            </h4>
        <?php } ?>


        <?php
        if ($buyer_module) {
            $buyer_datas = BuyerTransactions::model()->getBuyerTransactions();

            ?>
            <?php if (!filter_input(INPUT_GET, 'export') == 'csv') { ?>
                <h3>Buyer Transactions</h3>
            <?php } ?>
            <div style="position:relative;" <?php echo $spacing; ?>>
                <div class="text-right">Total
                    <?php echo count($buyer_datas); ?> results
                </div>
                <div id="parent1">
                    <div class="report-table-wrapper">
                        <table class="table total-table tab1" id="fixTable1">
                            <thead class="entry-table sticky-thead">
                                <?php if (filter_input(INPUT_GET, 'export') == 'csv') { ?>
                                    <tr>
                                        <th>Buyer Transactions</th>
                                    </tr>
                                <?php } ?>
                                <tr>
                                    <th>Sl No.</th>
                                    <th>Buyer Name</th>
                                    <th>Project Name</th>
                                    <th>Company</th>
                                    <th>SQ. FT.</th>
                                    <th>Receipts</th>
                                    <th>Expenses</th>
                                    <th>Invoice</th>
                                    <th>Balance</th>
                                    <th>Remarks</th>
                                </tr>

                            </thead>
                            <tbody>
                                <?php
                                if (!empty($buyer_datas)) {
                                    $sl = 1;
                                    $buyer_receipt_sum = 0;
                                    $buyer_balance_sum = 0;
                                    ob_start();
                                    foreach ($buyer_datas as $buyer_data) {
                                        $balance = $buyer_data['receipt'] - $buyer_data['expense'];
                                        $balance_style = '';
                                        if ($balance < 0) {
                                            $balance_style = 'color:#d06464';
                                        }
                                        $buyer_receipt_sum += $buyer_data['advance'];
                                        $buyer_balance_sum += $balance;
                                        ?>
                                        <tr>
                                            <td>
                                                <?php echo $sl; ?>
                                            </td>
                                            <td>
                                                <?php echo $buyer_data['buyer_name']; ?>
                                            </td>
                                            <td>
                                                <?php echo $buyer_data['project']; ?>
                                            </td>
                                            <td>
                                                <?php echo $buyer_data['company']; ?>
                                            </td>
                                            <td></td>
                                            <td class="buyer_receipt text-right">
                                                <?php echo Controller::money_format_inr($buyer_data['advance'], 2); ?>
                                            </td>
                                            <td class=" text-right">
                                                <?php echo Controller::money_format_inr($buyer_data['expense'], 2); ?>
                                            </td>
                                            <td class=" text-right">
                                                <?php echo Controller::money_format_inr($buyer_data['receipt'], 2); ?>
                                            </td>
                                            <td style="<?php echo $balance_style; ?>" class="buyer_balanace text-right">
                                                <?php echo Controller::money_format_inr($balance, 2); ?>
                                            </td>
                                            <td></td>

                                        </tr>
                                        <?php
                                        $sl++;
                                    }
                                    $buyer_details = ob_get_contents();
                                    ob_end_clean(); ?>
                                    <tr class="grandtotal">
                                        <?php
                                        if (filter_input(INPUT_GET, 'export') == 'csv') { ?>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>Total</td>
                                        <?php } else { ?>
                                            <td colspan="5" align="right"><b>Total<b></td>
                                        <?php } ?>
                                        <td align="right">
                                            <b>
                                                <?php echo Controller::money_format_inr($buyer_receipt_sum, 2) ?>
                                            </b>
                                        </td>
                                        <td></td>
                                        <td></td>
                                        <td align="right">
                                            <b>
                                                <?php echo Controller::money_format_inr($buyer_balance_sum, 2) ?>
                                            </b>

                                        </td>
                                        <td>

                                    </tr>
                                    <?php
                                    echo $buyer_details;
                                } else {
                                    ?>
                                    <tr>

                                        <td colspan="12">No results found.</td>

                                    </tr>

                                <?php }
                                ?>
                            </tbody>


                        </table>
                    </div>
                </div>

            </div>
        <?php } ?>
        <?php if (!filter_input(INPUT_GET, 'export') == 'csv') { ?>
            <h4 class="margin-bottom-0">Fixed Rate Contracts</h4>
        <?php } ?>
        <?php $reportdata = $financialdata1->getData(); ?>
        <div style="position:relative;">
            <div class="text-right">
                Total
                <?php echo count($reportdata); ?> results
            </div>
            <div class="table-responsive" id="parent1">
                <table class="table  tab1 frc_table" id="fixTable1" <?php echo $spacing; ?>>
                    <thead class="entry-table sticky-thead">
                        <?php if (filter_input(INPUT_GET, 'export') == 'csv') { ?>
                            <tr>
                                <th>Fixed Rate Contracts</th>
                            </tr>
                        <?php } ?>
                        <tr>
                            <th>Sl No.</th>
                            <th>Project Name</th>
                            <th>Company</th>
                            <th>SQ. FT.</th>
                            <th>SQ.FT.Rate or Lumpsum</th>
                            <th>Receipts</th>
                            <th>Expenses</th>
                            <th>Invoice</th>
                            <th>Balance</th>
                            <th>Remarks</th>
                        </tr>

                    </thead>

                    <tbody>
                        <?php

                        if (!empty($reportdata)) {
                            $i = 0;
                            $def_sum = 0;
                            $inv_sum = 0;
                            $receipt_sum = 0;
                            $exp_sum = 0;
                            ob_start();
                            $receipt_grandtotal = 0;
                            $deficit_grandtotal = 0;
                            $surplus_grandtotal = 0;
                            $paid_total = 0;//echo "<pre>";print_r($reportdata);die;
                            foreach ($reportdata as $data) {
                                $i++;
                                $projectid = $data['pid'];
                                $receipt_data = Yii::app()->db->createCommand("SELECT SUM(paidamount) as receipt_amount FROM {$tblpx}expenses WHERE projectid = " . $projectid . " AND type = 72")->queryRow();
                                if (isset($receipt_data['receipt_amount'])) {
                                    $data['tot_receipt'] = $receipt_data['receipt_amount'];
                                } else {
                                    $data['tot_receipt'] = 0;
                                }
                                $inv_dtls = Yii::app()->db->createCommand("SELECT SUM(amount) as total_amount,SUM(tax_amount) as tax_amount FROM {$tblpx}invoice WHERE project_id = " . $projectid . " AND invoice_status ='saved'")->queryRow();
                                if (($inv_dtls)) {
                                    if (($inv_dtls['total_amount'] + $inv_dtls['tax_amount']) == 0) {
                                        $invoice = 0;
                                    } else {
                                        $invoice = $inv_dtls['total_amount'] + $inv_dtls['tax_amount'];
                                    }
                                } else {
                                    $invoice = 0;
                                }

                                $percentage = Yii::app()->db->createCommand("SELECT percentage FROM {$tblpx}projects WHERE pid = {$projectid}")->queryRow();
                                $servise_charge = ($data->getProjectTotalExpense($data) * $percentage['percentage']) / 100;
                                $receipt_grandtotal += $data['tot_receipt'];
                                setlocale(LC_MONETARY, 'en_IN');

                                $expense = $data->getProjectTotalExpense($data) + $servise_charge;
                                if ($invoice > $expense) {
                                    $diff = $data['tot_receipt'] - $invoice;
                                } else {
                                    $diff = $data['tot_receipt'] - $expense;
                                }

                                $balance_style = '';
                                $deficit = $surplus = 0;
                                if ($diff < 0) {
                                    $balance_style = 'color:#d06464';
                                    $surplus = $diff;
                                } else {

                                    $deficit = $diff;
                                }
                                $deficit_grandtotal += $deficit;
                                $surplus_grandtotal += $surplus;
                                $def_sum += $diff;
                                $inv_sum += $invoice;
                                $receipt_sum += $data['tot_receipt'];
                                $exp_sum += $expense;

                                ?>
                                <tr>
                                    <td>
                                        <?= $i; ?>
                                    </td>
                                    <td>
                                        <?= $data['name']; ?>
                                    </td>
                                    <td>
                                        <?php
                                        $companyname = array();
                                        $user = Users::model()->findByPk(Yii::app()->user->id);
                                        $comVal = explode(',', $user->company_id);
                                        $arrVal = explode(',', $data['company_id']);
                                        foreach ($arrVal as $arr) {
                                            if (in_array($arr, $comVal)) {
                                                $value = Company::model()->findByPk($arr);
                                                if ($value) {
                                                    array_push($companyname, $value->name);
                                                }

                                            }
                                        }
                                        echo implode(', ', $companyname);
                                        ?>
                                    </td>
                                    <td align="right">
                                        <?= $data['sqft']; ?>
                                    </td>
                                    <td align="right">
                                        <?= $data['sqft_rate']; ?>
                                    </td>
                                    <td align="right" class="receipt_tot_1">
                                        <?php
                                        $amount = Yii::app()->Controller->money_format_inr($data['tot_receipt'], 2);
                                        echo $amount;
                                        ?>
                                    </td>
                                    <td align="right">
                                        <?php echo Yii::app()->Controller->money_format_inr($expense, 2); ?>
                                    </td>
                                    <td align="right" class="inv_tot_1">
                                        <?php echo Yii::app()->Controller->money_format_inr($invoice, 2); ?>
                                    </td>
                                    <td align="right" class="deificit_tot_1" style="<?php echo $balance_style; ?>">
                                        <?php echo Yii::app()->Controller->money_format_inr($diff, 2); ?>
                                    </td>
                                    <td>
                                        <?= $data['remarks']; ?>
                                    </td>
                                </tr>
                                <?php
                                $paid_total += $data['tot_paid_to_vendor'];
                            }
                            $fixed_items = ob_get_contents();
                            ob_end_clean();
                            ?>
                            <tr class="grandtotal">
                                <?php
                                if (filter_input(INPUT_GET, 'export') == 'csv') { ?>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td>Total</td>
                                <?php } else { ?>
                                    <td colspan="5" align="right"><b>Total<b></td>
                                <?php } ?>
                                <td align="right">
                                    <b>
                                        <?php echo Yii::app()->Controller->money_format_inr($receipt_sum, 2); ?>
                                    </b>
                                </td>
                                <td align="right">
                                    <b>
                                        <?php echo Yii::app()->Controller->money_format_inr($exp_sum, 2); ?>
                                    </b>
                                </td>
                                <td>
                                    <b>
                                        <?php echo Yii::app()->Controller->money_format_inr($inv_sum, 2); ?>
                                    </b>
                                </td>
                                <td align="right">
                                    <b class="text-wrap-center">
                                        <?php echo Yii::app()->Controller->money_format_inr($def_sum, 2); ?>

                                        <?php if ($inv_sum != 0) { ?>

                                            <sup class="text-danger sup_text">*</sup>
                                        <?php } ?>
                                    </b>
                                </td>
                                <td></td>
                            </tr>
                        </tbody>
                        <?php
                        echo $fixed_items;
                        } else {
                            ?>

                        <tr>
                            <td colspan="12">No results found.</td>
                        </tr>
                    <?php } ?>
                </table>
            </div>
        </div>

        <pagebreak></pagebreak>
        <?php if (!filter_input(INPUT_GET, 'export') == 'csv') { ?>
            <h4 class="margin-bottom-0">Fixed Percentage Contracts</h4>
        <?php } ?>
        <div style="position:relative;">
            <?php $reportdata = $fixedprojects->getData(); ?>
            <div class="text-right">
                Total
                <?php echo count($reportdata); ?> results
            </div>
            <div class="table-responsive" id="parent2">
                <table class="table  tab2 fpc_table " id="fixTable2" <?php echo $spacing; ?>>
                    <thead>
                        <?php if (filter_input(INPUT_GET, 'export') == 'csv') { ?>
                            <tr>
                                <th>Fixed Percentage Contracts</th>
                            </tr>
                        <?php } ?>
                        <tr>
                            <th>Sl No.</th>
                            <th>Project Name</th>
                            <th>Company</th>
                            <th>SQ. FT.</th>
                            <th>Fixed Percentage</th>
                            <th>Receipts</th>
                            <th>Expenses</th>
                            <th>Service Charge</th>
                            <th>Invoice</th>
                            <th>Deficit</th>
                            <th>Remarks</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (!empty($reportdata)) {
                            $i = 0;
                            $receipt_grandtotal = 0;
                            $deficit_grandtotal = 0;
                            $surplus_grandtotal = 0;
                            $paid_total = 0;
                            $invoice_total = 0;
                            $deficit_total = 0;
                            ob_start();
                            foreach ($reportdata as $data) {
                                $i++;
                                $projectid = $data['pid'];
                                $inv_dtls = Yii::app()->db->createCommand("SELECT SUM(amount) as total_amount,SUM(tax_amount) as tax_amount FROM {$tblpx}invoice WHERE project_id = " . $projectid . " AND invoice_status ='saved'")->queryRow();
                                $receipt_data = Yii::app()->db->createCommand("SELECT SUM(paidamount) as receipt_amount FROM {$tblpx}expenses WHERE projectid = " . $projectid . " AND type = 72")->queryRow();
                                if (isset($receipt_data['receipt_amount'])) {
                                    $data['tot_receipt'] = $receipt_data['receipt_amount'];
                                } else {
                                    $data['tot_receipt'] = 0;
                                }
                                if (($inv_dtls)) {
                                    if (($inv_dtls['total_amount'] + $inv_dtls['tax_amount']) == 0) {
                                        $invoice = 0;
                                    } else {
                                        $invoice = $inv_dtls['total_amount'] + $inv_dtls['tax_amount'];
                                    }
                                } else {
                                    $invoice = 0;
                                }
                                $percentage = Yii::app()->db->createCommand("SELECT percentage FROM {$tblpx}projects WHERE pid = {$projectid}")->queryRow();
                                $servise_charge = ($data->getProjectTotalExpense($data) * $percentage['percentage']) / 100;
                                $receipt_grandtotal += $data['tot_receipt'];

                                $paid_total += $data['tot_paid_to_vendor'];
                                $diff = $data['tot_receipt'] - ($data->getProjectTotalExpense($data) + $servise_charge); //Receipt - (Expense + Service Charge)                    
                                $deficit = 0;
                                $surplus = 0;
                                $balance_style = '';
                                if ($diff < 0) {
                                    $balance_style = 'color:#d06464';
                                    $surplus = $diff;
                                } else {
                                    $deficit = $diff;
                                }
                                $deficit_grandtotal += $deficit;
                                $surplus_grandtotal += $surplus;
                                $invoice_total += $invoice;
                                $deficit_total += $diff;
                                ?>
                                <tr>
                                    <td>
                                        <?= $i; ?>
                                    </td>
                                    <td>
                                        <?= $data['name']; ?>
                                    </td>
                                    <td>
                                        <?php
                                        $companyname = array();
                                        $user = Users::model()->findByPk(Yii::app()->user->id);
                                        $comVal = explode(',', $user->company_id);
                                        $arrVal = explode(',', $data['company_id']);
                                        foreach ($arrVal as $arr) {
                                            if (in_array($arr, $comVal)) {
                                                $value = Company::model()->findByPk($arr);
                                                if ($value) {
                                                    array_push($companyname, $value->name);
                                                }
                                            }
                                        }
                                        echo implode(', ', $companyname);
                                        ?>
                                    </td>
                                    <td align="right">
                                        <?= $data['sqft']; ?>
                                    </td>
                                    <td align="right">
                                        <?php echo (($data['percentage']) ? $data['percentage'] . '%' : ''); ?>
                                    </td>
                                    <td align="right" class="receipt_amount">
                                        <?php echo Controller::money_format_inr($data['tot_receipt'], 2); ?>
                                    </td>
                                    <td align="right">
                                        <?php echo Controller::money_format_inr($data->getProjectTotalExpense($data), 2); ?>
                                    </td>
                                    <td align="right">
                                        <?php echo Controller::money_format_inr($servise_charge, 2); ?>
                                    </td>
                                    <td align="right" class="inv_tot_2">
                                        <?php echo Controller::money_format_inr($invoice, 2); ?>
                                    </td>
                                    <td align="right" class="deficit_amount" style="<?php echo $balance_style; ?>">
                                        <?php echo Controller::money_format_inr($diff, 2); ?>
                                    </td>
                                    <td>
                                        <?= $data['remarks']; ?>
                                    </td>
                                </tr>
                                <?php
                            }
                            $fixed_pitems = ob_get_contents();
                            ob_end_clean();
                            ?>
                            <tr class="grandtotal">
                                <?php
                                if (filter_input(INPUT_GET, 'export') == 'csv') { ?>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td>Total</td>
                                <?php } else { ?>
                                    <td colspan="5" align="right"><b>Total<b></td>
                                <?php } ?>
                                <td align="right"><b>
                                        <?php echo Yii::app()->Controller->money_format_inr($receipt_grandtotal, 2); ?>
                                    </b></td>
                                <td></td>
                                <td></td>
                                <td><b>
                                        <?php echo Yii::app()->Controller->money_format_inr($invoice_total, 2); ?>
                                    </b></td>
                                <td align="right">
                                    <b class="text-wrap-center">
                                        <?php echo Yii::app()->Controller->money_format_inr($deficit_total, 2); ?>
                                        <?php if ($invoice_total != 0) { ?>
                                            <sup class="text-danger sup_text">*</sup>
                                        <?php } ?>
                                    </b>

                                </td>
                                <td></td>
                            </tr>
                        </tbody>
                        <?php
                        echo $fixed_pitems;
                        } else {
                            ?>
                        <tr>
                            <td colspan="12">No results found.</td>
                        </tr>
                    <?php } ?>

                </table>
            </div>

        </div>



    </div>
    <br>
    <p class="sup_info"><i class="text-wrap-center">Note :<sup class="text-danger sup_text">*</sup> Invoice Amount Not considered</p></i>

    <script>
        $(document).ready(function () {
            $("#fixTable1").tableHeadFixer({
                'left': false,
                'foot': true,
                'head': true
            });
            $("#fixTable2").tableHeadFixer({
                'foot': true,
                'head': true
            });
            var sum = 0;
            var surplu_sum_1 = 0;
            var deficit_sum = 0;
            var deficit_sum_1 = 0;
            var receipt_sum = 0;
            var receipt_sum_1 = 0;
            var inv_sum_1 = 0;
            var inv_sum_2 = 0;
            // iterate through each td based on class and add the values
            $(".surplus_amount").each(function () {
                var value = $(this).text();
                var val = value.replace(/,/ig, '');
                sum += parseFloat(val, 10);
                var surplus_sum = ReplaceNumberWithCommas(sum);
                $('#surplus_total').text(surplus_sum);
            });
            $(".deficit_amount").each(function () {
                var value = $(this).text();
                var val = value.replace(/,/ig, '');
                deficit_sum += parseFloat(val, 10);
                if (deficit_sum < 0) {
                    $('#deficit_total').css('color', '#d06464');
                }
                var deficit_sum_format = ReplaceNumberWithCommas(deficit_sum);
                $('#deficit_total').text(deficit_sum_format);
            });
            $(".receipt_amount").each(function () {
                var value = $(this).text();
                var val = value.replace(/,/ig, '');
                receipt_sum += parseFloat(val, 10);
                var receipt_sum_format = ReplaceNumberWithCommas(receipt_sum);
                $('#receipt_total').text(receipt_sum_format);
            });
            $(".receipt_tot_1").each(function () {
                var value = $(this).text();
                var val = value.replace(/,/ig, '');
                receipt_sum_1 += parseFloat(val, 10);
                var receipt_sum_format_1 = ReplaceNumberWithCommas(receipt_sum_1);
                $('#receipt_total_1').text(receipt_sum_format_1);
            });
            $(".inv_tot_1").each(function () {
                var value = $(this).text();
                var val = value.replace(/,/ig, '');
                inv_sum_1 += parseFloat(val, 10);
                var inv_sum_format_1 = ReplaceNumberWithCommas(inv_sum_1);
                $('#inv_total_1').text(inv_sum_format_1);
            });
            $(".inv_tot_2").each(function () {
                var value = $(this).text();
                var val = value.replace(/,/ig, '');
                inv_sum_2 += parseFloat(val, 10);
                var inv_sum_format_2 = ReplaceNumberWithCommas(inv_sum_2);
                $('#inv_total_2').text(inv_sum_format_2);
            });
            $(".deificit_tot_1").each(function () {
                var value = $(this).text();
                var val = value.replace(/,/ig, '');
                deficit_sum_1 += parseFloat(val, 10);
                // deficit_sum_1 = parseFloat(deficit_sum_1).toFixed(2);
                if (deficit_sum_1 < 0) {
                    $('#deficit_total_1').css('color', '#d06464');
                }
                var deficit_sum_format_1 = ReplaceNumberWithCommas(deficit_sum_1);
                $('#deficit_total_1').text(deficit_sum_format_1);
            });
            $(".surplus_tot_1").each(function () {
                var value = $(this).text();
                var val = value.replace(/,/ig, '');
                surplu_sum_1 += parseFloat(val, 10);
                var deficit_sum_format_1 = ReplaceNumberWithCommas(surplu_sum_1);
                $('#surplus_total_1').text(deficit_sum_format_1);
            });

            $(".buyer_receipt").each(function () {
                var value = $(this).text();
                var val = value.replace(/,/ig, '');
                surplu_sum_1 += parseFloat(val, 10);
                var deficit_sum_format_1 = ReplaceNumberWithCommas(surplu_sum_1);
                $('#buyer_total_receipt').text(deficit_sum_format_1);
            });
            $(".buyer_balanace").each(function () {
                var value = $(this).text();
                var val = value.replace(/,/ig, '');
                surplu_sum_1 += parseFloat(val, 10);
                var deficit_sum_format_1 = ReplaceNumberWithCommas(surplu_sum_1);
                $('#buyer_total_balance').text(deficit_sum_format_1);
            });


        });

        function ReplaceNumberWithCommas(yourNumber) {
            yourNumber = parseFloat(yourNumber).toFixed(2);
            var n = yourNumber.toString().split(".");
            n[0] = n[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            return n.join(".");
        }
    </script>