<?php
/* @var $this WorkTypeController */
/* @var $model WorkType */

$this->breadcrumbs=array(
	'Work Types'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List WorkType', 'url'=>array('index')),
	array('label'=>'Manage WorkType', 'url'=>array('admin')),
);
?>

<div class="panel panel-gray">
<div class="panel-heading form-head">
        <h3 class="panel-title">Add New Daily WorkType</h3>
    </div>
        <?php echo $this->renderPartial('_form', array('model' => $model)); ?>
</div>
