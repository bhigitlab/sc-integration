<?php
/* @var $this ExpensesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'Equipments',
);
$page = Yii::app()->request->getParam('Equipments_page');
if ($page != '') {
    Yii::app()->user->setReturnUrl($page);
} else {
    Yii::app()->user->setReturnUrl(0);
}
?>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<div class="container" id="equipments">
    <div class="loading-overlay">
        <span class="fa fa-spinner fa-3x fa-spin"></span>
    </div>
    <div class="clearfix">
        <div class="add-btn pull-right">
                <a class="button addequipments">Add Equipments</a>
        </div>
        <h2>Equipments</h2>
    </div>
    <div id="addequipments" style="display:none;"></div>
    <div id="errMsg"></div><br><br>
    <div class="row">
        <div class="col-md-12">
		<?php $this->widget('zii.widgets.CListView', array(
			'dataProvider'=>$dataProvider,
			'itemView'=>'_view',
			'template' => '<div class="container1"><table cellpadding="10" id="exptypetbl" class="table">{items}</table></div>',
                'ajaxUpdate' => false,
		)); ?>
        </div>

        <?php
        Yii::app()->clientScript->registerScript('myjavascript', '
             $(document).ready(function(){
               $("#exptypetbl").dataTable( {
                    "scrollY": "300px",
                    "scrollCollapse": true,
                    
                     "columnDefs"        : [       
                        { 
                            "searchable"    : false, 
                            "targets"       : [0,2] 
                        },
                        { "bSortable": false, "aTargets": [-1]  }
                    ],
                } );
                });

            ');
        ?>

        <script>
            function closeaction() {
                $('#addequipments').slideUp(500);
                location.reload();
            }

            $(document).ready(function() {
                $('.addequipments').click(function() {
                    $('.loading-overlay').addClass('is-active');
                    var id = $(this).attr('data-id');
                    $.ajax({
                        type: "POST",
                        url: "<?php echo $this->createUrl('equipments/create&layout=1') ?>",
                        success: function(response) {
                            $('#addequipments').html(response).slideDown();
                        },
                    });
                });
                $('.editProject').click(function() {
                    $('.loading-overlay').addClass('is-active');
                    var id = $(this).attr('data-id');
                    $.ajax({
                        type: "POST",
                        url: "<?php echo $this->createUrl('equipments/update&layout=1&id=') ?>" + id,
                        success: function(response) {
                            $('.loading-overlay').removeClass('is-active');
                            $('#addequipments').html(response).slideDown();
                        },
                    });
                });
                $('.deleteProject').click(function() {
                    // Show a confirmation dialog
                    if (confirm("Are you sure you want to delete this project?")) {
                        // If the user confirms, proceed with the deletion
                        $('.loading-overlay').addClass('is-active');
                        var id = $(this).attr('data-id');
                        $.ajax({
                            type: "POST",
                            url: "<?php echo $this->createUrl('equipments/delete&layout=1&id=') ?>" + id,
                            dataType: 'json',
                            success: function(response) {
                                console.log();
                                $('html, body').animate({ scrollTop: 0 }, 'slow');
                                $("#errMsg").show()
                                    .html('<div class="alert alert-'+response.success+'">'+response.message+'</div>')
                                    .fadeOut(10000);
                                setTimeout(function () {
                                    location.reload(true);
                                }, 1000);
                            },
                            error: function() {
                                $('.loading-overlay').removeClass('is-active');
                                alert('There was an error deleting the project. Please try again.');
                            }
                        });
                    }
                });



                jQuery(function($) {
                    $('#addequipmentstype').on('keydown', function(event) {
                        if (event.keyCode == 13) {
                            $("#equipmentssearch").submit();
                        }
                    });
                });

            });
            $(document).ajaxComplete(function() {
                $('.loading-overlay').removeClass('is-active');
                $('#loading').hide();
            });
        </script>
    </div>
</div>
<style>

    .page-body h3 {
        margin: 4px 0px;
        color: inherit;
        text-align: left;
    }

    .panel {
        border: 1px solid #ddd;
    }

    .panel-heading {
        background-color: #eee;
        height: 40px;
    }
</style>