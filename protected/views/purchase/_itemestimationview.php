<?php
$class = '';
$newclass = '';

if (isset($data["itemestimation_status"]) && $data["itemestimation_status"] != '2') {
    $newclass = 'row_class';
}
if (isset($data["itemestimation_status"]) && $data["itemestimation_status"] == 4) {
    $newclass = 'reject_class';
}

// Ensure approve key exists
$approve = isset($data["itemestimation_status"]) ? $data["itemestimation_status"] : 0;
?>

<tr class="<?php echo $class . '' . $newclass; ?>">
    <td>
        <span class="icon icon-options-vertical popover-test" data-toggle="popover" data-placement="right" type="button" data-html="true" style="cursor: pointer;"></span>
        <div class="popover-content hide">
            <ul class="tooltip-hiden">
                <li>
                    <a class="click btn btn-xs btn-default" href="<?php echo Yii::app()->createAbsoluteUrl('purchase/viewestimation', array("project_id" => $data["project_id"])); ?>" title="View">View</a>
                </li>
                <?php if (!empty($data['project_id'])): ?>
                <li>
                    <a class="click btn btn-xs btn-default" href="<?php echo Yii::app()->createAbsoluteUrl('materialEstimation/editEstimation', array('project_id' => $data['project_id'])); ?>" title="Edit">Edit</a>
                </li>
                <?php endif; ?>
                <?php if (empty($data['pms_project_id'])): ?>
                <li>
                    <a class="click btn btn-xs btn-default deleteProject" data-toggle="modal" data-target=".delete" data-id="<?php echo htmlspecialchars($data['itemestimation_id'], ENT_QUOTES, 'UTF-8'); ?>" data-project="<?php echo htmlspecialchars($data['project_id'], ENT_QUOTES, 'UTF-8'); ?>" title="Delete">Delete</a>
                </li>
                <?php endif; ?>
                <?php if ($approve == 1 || $approve ==3): ?>
                <li>
                    <a class="click btn btn-xs btn-default approve" id="<?php echo $data['itemestimation_id']; ?>" href="#" onClick="approve('<?php echo $data['itemestimation_id']; ?>', this)" title="Approve">Approve</a>
                </li>
                <?php endif; ?>
                <li><a class="click btn btn-xs btn-default" id="<?php echo $data['itemestimation_id']; ?>" href="#" onClick="openRejectModal('<?php echo $data['itemestimation_id']; ?>')" title="Reject">Reject</a></li>
            </ul>
        </div>
    </td>
    <td><?php echo $index + 1; ?></td>
    <td>
        <?php
        $projectModel = Projects::model()->findByPk($data['project_id']);
        echo !empty($projectModel) ? CHtml::encode($projectModel->name) : "";
        ?>
    </td>
    <td>
        <?php echo empty($data["pms_project_id"]) ? 'Accounts' : 'PMS'; ?>
    </td>
</tr>
