<style>
 .grandtotal th{
        /* border-bottom:1px solid black !important; */
        background-color:rgb(247, 240, 240) !important;
    } 
	</style>
<?php
$tblpx = Yii::app()->db->tablePrefix;
if ($index == 0) {
    ?>

    <thead class="entry-table">
        <tr width="100%">
            <th style="display:none"></th>
            <th width="2%">Sl No.</th>
            <th width="13%">Purchase NO</th>
            <th width="10%">Bill no</th>
            <th width="10%">Company</th>
            <th width="10%">Vendor</th>
            <th width="7%">Project</th>
            <th width="10%">Bill Date</th>
            <th width="11%">Total Amount</th>
<!--            <th width="12%">Amount Paid</th>
            <th width="12%">Balance Amount</th>-->
        </tr>
        <tr class="grandtotal">

        <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th style="text-align: right;">Total</th>
      <th><?= Controller::money_format_inr($tot_billamount,2,1); ?></th>

        </tr>
    </thead>


    <!-- <tfoot>


      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th style="text-align: right;">Total</th>
      <th><?= Controller::money_format_inr($tot_billamount,2,1); ?></th>
   <th><?= Controller::money_format_inr($tot_paidamount,2,1); ?></th>
      <th><?= Controller::money_format_inr($tot_bal,2,1); ?></th>-->



    <!-- </tfoot>  -->

     <?php } ?>



    <?php

        $balance =0;
        $balance = $data['bill_totalamount'] - $data['paid'];
    ?>

        <tbody>
            <tr>
                 <td><?= $index+1;?></td>
                 <td><a href="" class="punum" data-id="<?= $data['p_id']?>" style="color:#337ab7;"><?= $data['purchase_no'] ?></a></td>
                 <td><a href="" class="billnum" data-id="<?= $data['bill_id']?>" style="color:#337ab7;"><?= $data['bill_number'] ?></a></td>
                 <td>
                         <?php
                          $company = Company::model()->findByPk($data['company_id']);
                          echo (isset($company )?$company->name:"NIL");
                         ?>
                 </td>
                 <td><?= $data['vendorname'] ?></td>
                 <td><?= $data['projectname'] ?></td>
                 <td><?= date('d-m-Y', strtotime($data['bill_date'])) ?></td>
                 <?php
             $tot_amount =0;

             $addcharges  =  Yii::app()->db->createCommand("SELECT ROUND(SUM(amount), 2) as amount FROM ".$tblpx."additional_bill WHERE `bill_id`=".$data['bill_id'])->queryRow(); 
 
              if(!empty($addcharges)){ 
                 $tot_amount = $addcharges['amount']+$data['bill_totalamount'];
              }
            ?>
                 <td><?= Controller::money_format_inr($tot_amount,2,1); ?></td>

            </tr>
	</tbody>

    <script type="text/javascript">

    $(".billnum").click(function(e){

        var billid = $(this).attr('data-id');



        location.href='<?= $this->createAbsoluteUrl('/Bills/view&id=') ?>'+billid;
        e.preventDefault();
   });


    $(".punum").click(function(e){

        var purchaseid = $(this).attr('data-id');
        location.href='<?= $this->createAbsoluteUrl('/purchase/viewpurchase&pid=') ?>'+purchaseid;
        e.preventDefault();
   });




    </script>
