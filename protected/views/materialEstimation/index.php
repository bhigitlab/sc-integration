<?php
/* @var $this MaterialEstimationController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'Material Estimations',
);
$page = Yii::app()->request->getParam('MaterialEstimation_page');
if ($page != '') {
    Yii::app()->user->setReturnUrl($page);
} else {
    Yii::app()->user->setReturnUrl(0);
}
?>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.min.js"></script>

<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<div class="container" id="materialEstimations">
    <div class="loading-overlay">
        <span class="fa fa-spinner fa-3x fa-spin"></span>
    </div>
    <div class="clearfix">
    <?php
       $pms_api_integration=ApiSettings::model()->pmsIntegrationStatus();
        $selectedProjectId = isset($_GET['project_id']) ? $_GET['project_id'] : '';
        ?>
    <div>
        <h2>Estimation</h2>
    </div>
    <?php
    $flag = isset($flag) ? $flag : false;
    ?>
         <div class="row">
                <div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label>PROJECT : <span class="required">*</span></label>
                        <select name="project" class="inputs target project js-example-basic-single-project" id="project">
                            <option value="">Choose Project</option>
                            <?php
                            // Fetch all project IDs that are already saved in ItemEstimation model
                            $savedProjectIds = Itemestimation::model()->findAll(array(
                                'select' => 'project_id',
                                'distinct' => true,
                            ));
                            $savedProjectIdsArray = array_map(function($item) {
                                return $item->project_id;
                            }, $savedProjectIds);

                            // Fetch all projects
                            $projects = Projects::model()->findAll();

                            // Render all projects, disable those that are in the savedProjectIdsArray
                            foreach ($projects as $project) {
                                $selected = ($project->pid == $selectedProjectId) ? 'selected' : '';
                                $disabled = in_array($project->pid, $savedProjectIdsArray) && !$selected ? 'disabled' : '';
                                echo "<option value='{$project->pid}' {$selected} {$disabled}>{$project->name}</option>";
                            }
                            ?>
                        </select>
                    </div>
                </div>

            </div>
            <?php if ($pms_api_integration==0): ?>
             <div class="add-btn pull-left">
            <a class="btn btn-info addMaterialEstimation" style="margin-right:25px;margin-top:19px;">Add Material Estimation</a>
        </div>
		<div class="add-btn pull-left">
            <a class="btn btn-info addMaterialEstimation" style="margin-right:25px; margin-top:19px;">Add Labour Estimation</a>
        </div>
		<div class="add-btn pull-left">
            <a class="btn btn-info addMaterialEstimation" style="margin-right:25px;  margin-top:19px;">Add Equipment Estimation</a>
        </div>
        <?php endif; ?>
        <?php
       $pms_api_integration_model=ApiSettings::model()->findByPk(1);
       $pms_api_integration =$pms_api_integration_model->api_integration_settings;
       
        if($pms_api_integration==1){
            $currentUrl = Yii::app()->request->getUrl();
            echo CHtml::link('Refresh', array('MaterialEstimation/refreshButton', 'type' => 1,'returnUrl' => $currentUrl), array('class' => 'btn btn-info ml-10 pull-right','style'=>'margin-right:40px;  margin-top:19px;' ,'id' => 'materialrequest_refresh')); 
        }
        ?>
        <div class="add-btn pull-right">
          <a href="index.php?r=purchase/itemestimation" class="btn btn-info" style="margin-right:20px; margin-top:19px;">Estimations</a>
        </div>
        </div>
        
        <!-- <h2> Estimations</h2> -->
    </div>
    <br><br>
    <div id="addMaterialEstimation" style="display:none;"></div>
    <div id="errMsg"></div>
    <div class="row">
    <div class="col-md-12">
    <?php
// Calculate the total amounts
$totalAmount = 0;
foreach ($dataProvider->getData() as $data) {
    $totalAmount += $data->amount;
}

$totalAmount_labor = 0;
foreach ($dataProvider1->getData() as $data) {
    $totalAmount_labor += $data->amount;
}

$totalAmount_equipment = 0;
foreach ($dataProvider2->getData() as $data) {
    $totalAmount_equipment += $data->amount;
}
?>

<div class="col-md-12">
   

   <div id="material-table-container" class="container1"></div>
   <div id="labour-table-container" class="container1"></div>
   <div id="equipment-table-container" class="container1"></div>
</div>

</div>
        <?php
        Yii::app()->clientScript->registerScript('myjavascript', '
            $(document).ready(function(){
                $("#materialEstimationTbl").dataTable({
                    "scrollY": "300px",
                    "scrollCollapse": true,
                    "paging": false,
                    "columnDefs": [       
                        { 
                            "searchable": false, 
                            "targets": [0,2] 
                        },
                        { "bSortable": false, "aTargets": [-1] }
                    ],
                });
            });
        ');
        ?>

        <script>
            $(document).ready(function() {
                var flag = <?php echo json_encode($flag); ?>;
                if (flag) {
                    $('#project').attr('disabled', 'disabled').data('disabled', true);
                } else {
                    $('#project').removeAttr('disabled');
                }
            });
            $(document).ready(function() {
                $('.js-example-basic-single-project').select2({
                    width: '100%',
                });
                var selectedProject = $('#project').val();
                if (selectedProject) {
                    fetchProjectData(selectedProject);
                }
            });
            
            $("#project").on("change", function() {
                var selectedProject = $(this).val();
                if (selectedProject) {
                    $(this).prop("disabled", true);
                    fetchProjectData(selectedProject);
                }
            });

            function fetchProjectData(projectId) {
                $.ajax({
                    type: "GET",
                    url: "<?php echo $this->createUrl('materialEstimation/fetchProjectData'); ?>",
                    data: { project_id: projectId },
                    success: function(response) {
                        var data = $.parseJSON(response);
                        console.log(data.materialTable);
                        $("#material-table-container").html(data.materialTable);
                        $("#labour-table-container").html(data.labourTable);
                        $("#equipment-table-container").html(data.equipmentTable);
                    },
                    error: function() {
                        alert("There was an error fetching the project data. Please try again.");
                    }
                });
            }

            $('.createEstimation').click(function(event) {
                var project_id = $('#project').val();
                if (project_id) {
                    createEstimation(project_id);
                } else {
                    alert('Please select a project before creating an estimation.');
                }
            });

            function createEstimation(projectId) {
                var url = "<?php echo $this->createUrl('purchase/addEstimation'); ?>";
                $.ajax({
                    type: "POST",
                    url: url,
                    data: { project_id: projectId },
                    success: function(response) {
                        alert('Estimation created successfully!');
                        window.location.href = "<?php echo $this->createUrl('purchase/itemestimation'); ?>";
                    },
                    error: function() {
                        alert('There was an error creating the estimation. Please try again.');
                    }
                });
            }

            function closeaction() {
                $('#addMaterialEstimation').slideUp(500);
            }

            $(document).ready(function() {
                $('.addMaterialEstimation').click(function(event) {
                    var buttonClicked = $(this).text().trim(); 
                    var project_id=$('#project').val();
                    //$('.loading-overlay').addClass('is-active');
                    if (!project_id) {
                        alert('Please select a project ID.');
                        return;
                    }
                    var url = "<?php echo $this->createUrl('materialEstimation/create&layout=1') ?>";
                    var estimation='';
                    // Adjust the URL or other actions based on the button clicked
                    if (buttonClicked === 'Add Material Estimation') {
                         estimation='material_estimation';
                    } else if (buttonClicked === 'Add Labour Estimation') {
                         estimation='labour_estimation';
                    } else if (buttonClicked === 'Add Equipment Estimation') {
                         estimation='equipment_estimation';
                    }
                   
                    $.ajax({
                        type: "POST",
                        url: url,
                        data: { estimation: estimation ,project_id: project_id},
                        success: function(response) {
                            $('#addMaterialEstimation').html(response).slideDown();
                        },
                    });
                   
                   
                });


                $('.editProject').click(function() {
                    $('.loading-overlay').addClass('is-active');
                    var id = $(this).attr('data-id');
                    var estimation = $(this).attr('data-estimation');
                    var project_id = $(this).attr('data-project_id');
                    $("#project").val(project_id);
                    $("#project").trigger('change');
                    
                    $.ajax({
                        type: "POST",
                        url: "<?php echo $this->createUrl('materialEstimation/update&layout=1&id=') ?>" + id,
                        data: {
                            estimation: estimation
                        },
                        success: function(response) {
                            $('.loading-overlay').removeClass('is-active');
                            $('#addMaterialEstimation').html(response).slideDown();
                             
                        },
                    });
                });

                $('.deleteProject').click(function() {
                    if (confirm("Are you sure you want to delete this material estimation?")) {
                        $('.loading-overlay').addClass('is-active');
                        var id = $(this).attr('data-id');
                        var estimation = $(this).attr('data-estimation');
                        $.ajax({
                            type: "POST",
                            url: "<?php echo $this->createUrl('materialEstimation/delete&layout=1&id=') ?>" + id,
                            data: {
                            estimation: estimation
                            },
                            dataType: 'json',
                            success: function(response) {
                                console.log();
                                $('html, body').animate({ scrollTop: 0 }, 'slow');
                                $("#errMsg").show()
                                    .html('<div class="alert alert-' + response.success + '">' + response.message + '</div>')
                                    .fadeOut(10000);
                                setTimeout(function() {
                                    location.reload(true);
                                }, 1000);
                            },
                            error: function() {
                                $('.loading-overlay').removeClass('is-active');
                                alert('There was an error deleting the material estimation. Please try again.');
                            }
                        });
                    }
                });

                jQuery(function($) {
                    $('#addMaterialEstimationType').on('keydown', function(event) {
                        if (event.keyCode == 13) {
                            $("#materialEstimationSearch").submit();
                        }
                    });
                });
            });

            $(document).ajaxComplete(function() {
                $('.loading-overlay').removeClass('is-active');
                $('#loading').hide();
            });
        </script>
    </div>
</div>
<style>
    .page-body h3 {
        margin: 4px 0px;
        color: inherit;
        text-align: left;
    }

    .panel {
        border: 1px solid #ddd;
    }

    .panel-heading {
        background-color: #eee;
        height: 40px;
    }
</style>
