<?php
/* @var $this SalesQuotationController */
/* @var $model SalesQuotation */

$this->breadcrumbs=array(
	'Sales Quotations'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List SalesQuotation', 'url'=>array('index')),
	array('label'=>'Create SalesQuotation', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#sales-quotation-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Sales Quotations</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'sales-quotation-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'company_id',
		'client_id',
		'project_id',
		'date_quotation',
		'invoice_no',
		/*
		'category_id',
		'caregory_label',
		'work_type',
		'shutterwork_description',
		'carcass_description',
		'description',
		'worktype_label',
		'unit',
		'quantity',
		'quantity_nos',
		'mrp',
		'amount_after_discount',
		'status',
		'created_by',
		'created_on',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
