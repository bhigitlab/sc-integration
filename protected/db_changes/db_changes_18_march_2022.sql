ALTER TABLE `jp_subcontractorbillitem` ADD `item_type` VARCHAR(100) NULL DEFAULT NULL AFTER `description`;

ALTER TABLE `jp_subcontractorbillitem` ADD `item_quantity` FLOAT NULL DEFAULT NULL AFTER `item_type`, ADD `item_unit` VARCHAR(500) NULL DEFAULT NULL AFTER `item_quantity`, ADD `item_rate` FLOAT NULL DEFAULT NULL AFTER `item_unit`;

INSERT INTO `jp_menu` (`menu_id`, `menu_name`, `parent_id`, `status`, `controller`, `action`, `params`, `showmenu`, `show_list`, `related`) VALUES ('771', 'getexpensehead', '288', '0', 'projects', 'getexpensehead', '', '0', '0', NULL);

INSERT INTO `jp_menu` (`menu_id`, `menu_name`, `parent_id`, `status`, `controller`, `action`, `params`, `showmenu`, `show_list`, `related`) VALUES ('772', 'Remove ClientType', '349', '0', 'projectType', 'removeclientype', '', '2', '1', NULL);

ALTER TABLE `jp_clients` ADD CONSTRAINT `clients_projtype_fk` FOREIGN KEY (`project_type`) REFERENCES `jp_project_type`(`ptid`) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `jp_purchase` ADD CONSTRAINT `projectfk` FOREIGN KEY (`project_id`) REFERENCES `jp_projects`(`pid`) ON DELETE RESTRICT ON UPDATE CASCADE; 

ALTER TABLE `jp_users` ADD CONSTRAINT `jp_users_client_id_fk` FOREIGN KEY (`client_id`) REFERENCES `jp_clients`(`cid`) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE `jp_projects` ADD CONSTRAINT `clientid_fk` FOREIGN KEY (`client_id`) REFERENCES `jp_clients`(`cid`) ON DELETE RESTRICT ON UPDATE CASCADE;

INSERT INTO `jp_menu` (`menu_id`, `menu_name`, `parent_id`, `status`, `controller`, `action`, `params`, `showmenu`, `show_list`, `related`) VALUES ('773', 'Remove Client', '324', '0', 'clients', 'removeclient', '', '2', '1', NULL);

INSERT INTO `jp_menu` (`menu_id`, `menu_name`, `parent_id`, `status`, `controller`, `action`, `params`, `showmenu`, `show_list`, `related`) VALUES ('774', 'Remove Company', '416', '0', 'company', 'removecompany', '', '2', '1', NULL);

ALTER TABLE `jp_users` ADD CONSTRAINT `users_type_fk` FOREIGN KEY (`user_type`) REFERENCES `jp_user_roles`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

INSERT INTO `jp_menu` (`menu_id`, `menu_name`, `parent_id`, `status`, `controller`, `action`, `params`, `showmenu`, `show_list`, `related`) VALUES ('775', 'Remove UserRole', '359', '0', 'userRoles', 'removeuserole', '', '2', '1', NULL);

ALTER TABLE `jp_projects` ADD CONSTRAINT `createdby_fk` FOREIGN KEY (`created_by`) REFERENCES `jp_users`(`userid`) ON DELETE RESTRICT ON UPDATE CASCADE;

