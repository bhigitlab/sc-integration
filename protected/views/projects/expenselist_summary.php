
<?php
$tblpx      = Yii::app()->db->tablePrefix;
$newQuery   = "";
$newQuery1  = "";
$newQuery2  = "";
$newQuery3  = "";
$user       = Users::model()->findByPk(Yii::app()->user->id);
$arrVal     = explode(',', $user->company_id);
foreach ($arrVal as $arr) {
    if ($newQuery) $newQuery .= ' OR';
    $newQuery .= " FIND_IN_SET('" . $arr . "', id)";
}
if ($company_id != '') {
    $newQuery2 .= " FIND_IN_SET('" . $company_id . "', company_id)";
} else {
    foreach ($arrVal as $arr) {
        if ($newQuery2) $newQuery2 .= ' OR';
        $newQuery2 .= " FIND_IN_SET('" . $arr . "', company_id)";
    }
}
?>

    <?php
    $exp_cal=0;
                    //die($company);
    $project_det=Projects::Model()->findByPk($project_id);
    if(!empty($project_det)){
        $pro_company_str=$project_det->company_id;
        if(!empty($pro_company_str)){
            $company_arr=explode(',',$pro_company_str);
            $company = $company_arr['0'];
        }
     }
                   
    if(!empty($company)){
         $company_det=Company::Model()->findByPk($company);
        $exp_cal =$company_det->expense_calculation;
                       
    }
    
    $fromdate       = (!empty($date_from) ?  date('d-M-Y', strtotime($date_from)) : '');
    $todate         = (!empty($date_to) ? date('d-M-Y', strtotime($date_to)) : '');
    $companyArray   = explode(",", Yii::app()->user->company_ids);
    $condition      = "";
    foreach ($companyArray as $company) {
        $condition .= "FIND_IN_SET('" . $company . "',company_id) OR ";
    }
    $condition      = substr($condition, 0, -3);
    
    ?>
    <?php
    if (!filter_input(INPUT_GET, 'export')) { ?>
       

        <?php

        $url_data = '';
        if (isset($_GET['project_id'])) {
            $url_data .= '&project_id=' . $_GET['project_id'];
        }
        if (isset($_GET['company_id'])) {
            $url_data .= '&company_id=' . $_GET['company_id'];
        }
        if (isset($_GET['date_from'])) {
            $url_data .= '&date_from=' . $_GET['date_from'];
        }
        if (isset($_GET['date_to'])) {
            $url_data .= '&date_to=' . $_GET['date_to'];
        }
        if (isset($_GET['reconcil_type'])) {
            $url_data .= '&reconcil_type=' . $_GET['reconcil_type'];
        }
        ?>
    <?php }    ?>


                <table class="table">
                    <tr>
                        <td colspan="8"><b>Client : </b><?php echo $clientname; ?></td>
                    </tr>
                    <tr>
                        <td colspan="4"><b>Project Name: </b><?php echo $name; ?></td>
                        <td colspan="4"><b>Square Feet: </b><?php echo $sqft; ?></td>
                    </tr>
                    <tr>
                        <td colspan="4"><b>Work Contract: </b><?php echo $wc; ?></td>
                        <?php if ($wc_type == 87) { ?>
                            <td colspan="4"><b>Square Feet Rate :</b><?php echo $sqft_rate; ?></td>
                        <?php } else if ($wc_type == 86) { ?>
                            <td colspan="4"><b>Percentage : </b><?php echo ($percentage) ? $percentage . '%' : ''; ?></td>
                        <?php } ?>
                    </tr>
                    <tr>
                        <td colspan="8"><b>Site: </b><?php echo $site; ?></td>
                    </tr>
                </table>
                
                            <?php
                            $invoice = 0;
                            if($_GET['reconcil_type'] =='unreconcil'){
                                foreach ($uinvoice_dtls as $invoice_data) {
                                    $invoice += $invoice_data['amount'] + $invoice_data['tax_amount'];
                                }
                            }else{
                                foreach ($invoice_dtls as $invoice_data) {
                                    $invoice += $invoice_data['amount'] + $invoice_data['tax_amount'];
                                }
                            }
                            
                        $clienttotal = 0;
                        
                        //unreconciled
                        if($_GET['reconcil_type'] =='unreconcil'){
                            foreach ($client1 as $unreconcilClient) {
                                $clienttotal += $unreconcilClient['receipt'];
                            }
                        }else{
                            foreach ($client as $newclient) {
                                $clienttotal += $newclient['receipt'];
                            }
                        }
                        $total_expense_amount = Projects::model()->getProjectTotalExpense($model);
                        $project_balance_amount = $clienttotal - $total_expense_amount;
                        ?>
                        
                        <input type="hidden" value = "<?php echo $clienttotal ?>" class="receipt_amount_col">
                        
                <br>
                <?php
                if ($_GET['reconcil_type'] == 'reconcil') {                    
                        $sales_quotations = Quotation::model()->getProjectQuotations($project_id);
                        $total_qtn = 0;
                        foreach ($sales_quotations as $sales_quotation) {                        
                            $total_qtn += $sales_quotation['total_amount'];
                        }
                        
                        $invoice = 0;

                        foreach ($invoice_dtls as $invoice_data) {
                             $invoice += $invoice_data['amount'] + $invoice_data['tax_amount']; 
                             }
                        
                        ?>
                    
                        <?php
                        $clienttotal = 0;

                        foreach ($client as $newclient) {
                            $clienttotal += $newclient['receipt'];  
                        }

                        $bill_amount = 0;
                        $purchase_list = Projects::model()->groupPurchaseBillItems($purchase);
                        $expense_types_listing = Projects::model()->setExpenseTypesListingofPurchaseBill($purchase_list);

                        foreach ($purchase_list as $purchase) {
                            $count = count($purchase);
                            $count = ($count - 2);
                            $index = 1;
                            foreach ($purchase as $key => $value) {
                                if (is_array($value)) {
                                    $amount =  ($value['bill_totalamount'] + $value['bill_additionalcharge']);
                                    $vendor = Vendors::model()->findByPk($value['vendor_id']);
                                    $bill_amount += $amount; 
                                }
                                $index++;
                            }
                        }
                       
                    
                }else{ 
                        $invoice = 0;

                        foreach ($uinvoice_dtls as $invoice_data) {
                            $invoice += $invoice_data['amount'] + $invoice_data['tax_amount']; 
                        }
                        
                        $clienttotal = 0;

                        foreach ($client1 as $newclient) {
                            $clienttotal += $newclient['receipt']; 
                        }
                        
                        $bill_amount = 0;
                        $purchase_list = Projects::model()->groupPurchaseBillItems($purchase1);
                        $expense_types_listing = Projects::model()->setExpenseTypesListingofPurchaseBill($purchase_list);

                        foreach ($purchase_list as $purchase) {

                            $count = count($purchase);
                            $count = ($count - 2);
                            $index = 1;
                            foreach ($purchase as $key => $value) {
                                if (is_array($value)) {
                                    $amount =  $value['bill_totalamount'] + $value['bill_additionalcharge'];
                                    $vendor = Vendors::model()->findByPk($value['vendor_id']);
                                    $bill_amount += $amount;
                                }
                                $index++;
                            }
                        }
                    }

                $return_total_amount = 0;
                if ($_GET['reconcil_type'] == 'reconcil') {
                    $purchase_return = Projects::model()->setPurchaseReturn($expreceipt_return);
                        $return_total_amount = 0;
                        $return_data_array=array();
                        foreach ($purchase_return as $key=>$return_datas) {                   
                            foreach ($return_datas as $key=>$return_data) {
                                if (is_array($return_data)) {
                                    $return_total_amount += $return_data['amount'];
                                    $return_data_array[$return_data['exptype']][]  = $return_data;
                                }            
                            }                                    
                        }
    
                        foreach ($return_data_array as $key => $return_data) {         
                            $expensehead = Yii::app()->db->createCommand("SELECT type_name FROM `jp_expense_type` where type_id ='" . $key . "' ")->queryRow();                                                        
                        ?>
                        
                            <?php
                            $row_sum = 0;
                            foreach ($return_data as $key => $value) { 
                                $row_sum += $value['paidamount'];
                            }
                            $i=0;
                            foreach ($return_data as $key => $value) {                 
                                $vendorname = Yii::app()->db->createCommand("SELECT name FROM `jp_vendors` WHERE vendor_id='" . $value['vendor_id'] . "'")->queryRow();
                                ?>
                                
                                    <?php
                                    $bill_id = Yii::app()->db->createCommand("SELECT bill_id  from {$tblpx}purchase_return where return_id = '" . $value['return_id'] . "' ")->queryAll();
                                    foreach ($bill_id as $key => $bill) {
                                        $bills = Bills::model()->findByPk($bill['bill_id']);
                                        if ($bills['purchase_id'] != NULL) {
                                            $purchase = Purchase::model()->findByPk($bills->purchase_id);
                                            
                                        }
                                    }
                                    ?>                        
                    <?php $i++; 
                    } ?>
        
                    <?php } ?>
                        
                        <?php
                        $grand = 0;
                        $total = 0;
                        $daybook_exp_total = 0;

                        foreach ($sql as $new) {
                            $daybook_expense_index = 1;
                            $where = '';
                            if (!empty($date_from) && !empty($date_to)) {
                                $where .= " AND {$tblpx}expenses.date between '" . $date_from . "' and'" . $date_to . "'";
                            } else {
                                if (!empty($date_from)) {
                                    $where .= " AND {$tblpx}expenses.date >= '" . $date_from . "'";
                                }
                                if (!empty($date_to)) {
                                    $date_to = date('Y-m-d', strtotime($date_to));
                                    $where .= " AND {$tblpx}expenses.date <= '" . $date_to . "'";
                                }
                            }
                            $cheque_array = array();

                            foreach ($unreconciled_data as  $data) {
                                $recon_checque = "'{$data["reconciliation_chequeno"]}'";
                                array_push($cheque_array, $recon_checque);
                            }

                            $cheque_array1 = implode(',', $cheque_array);

                            $sql = "select * from " . $tblpx . "expenses "
                                . " WHERE amount!=0 AND exptype=" . $new['type_id'] 
                                . "  AND projectid=" . $new['project_id'] . " " . $where 
                                . " AND exptype IS NOT NULL AND vendor_id IS NOT NULL "
                                . " AND bill_id IS NULL AND (" . $newQuery2 . ")  "
                                . " AND (reconciliation_status IS NULL OR reconciliation_status=1) "
                                . " ORDER BY date ";
                            $sqlexp = Yii::app()->db->createCommand($sql)->queryAll();

                            if (!empty($sqlexp)) {
                                $day_book_expense_count = count($sqlexp);

                        ?>

                                <?php
                                $total = $daybook_expense_tot_amount = 0;
                                foreach ($sqlexp as $newsql) {
                                    $expense_types_listing = Projects::model()->setExpenseList($expense_types_listing, $newsql);
                                    $daybook_expense_tot_amount += $newsql['paidamount'];
                                }
                                foreach ($sqlexp as $newsql) {
                                    if ($newsql['paidamount'] != 0) {
                                        $total += $newsql['paidamount']; 
                                        $daybook_expense_index++;
                                    }
                                   
                                }
                                
                                ?>


                            <?php
                                $grand += $total;
                                $daybook_exp_total += $total;
                            }
                            ?>
                        <?php

                        }
                        ?>
                        
                <?php } else { 
                        $grand = 0;
                        $total = 0;
                        $daybook_exp_total = 0;

                        foreach ($sql as $new) {
                            $daybook_expense_index = 1;
                            $where = '';
                            if (!empty($date_from) && !empty($date_to)) {
                                $where .= " AND {$tblpx}expenses.date between '" . $date_from . "' and'" . $date_to . "'";
                            } else {
                                if (!empty($date_from)) {
                                    $where .= " AND {$tblpx}expenses.date >= '" . $date_from . "'";
                                }
                                if (!empty($date_to)) {
                                    $date_to = date('Y-m-d', strtotime($date_to));
                                    $where .= " AND {$tblpx}expenses.date <= '" . $date_to . "'";
                                }
                            }

                            $cheque_array = array();
                            foreach ($unreconciled_data as  $data) {
                                $recon_checque = "'{$data["reconciliation_chequeno"]}'";
                                array_push($cheque_array, $recon_checque);
                            }
                            $cheque_array1 = implode(',', $cheque_array);

                            $sql = "select * from " . $tblpx . "expenses "
                                . " WHERE amount!=0 AND exptype=" . $new['type_id'] 
                                . " AND projectid=" . $new['project_id'] . " " . $where 
                                . " AND exptype IS NOT NULL AND vendor_id IS NOT NULL "
                                . " AND bill_id IS NULL AND (" . $newQuery2 . ") "
                                . " AND reconciliation_status= 0   "
                                . " ORDER BY date ";
                            $sqlexp = Yii::app()->db->createCommand($sql)->queryAll();
                            

                            if (!empty($sqlexp)) {
                                $day_book_expense_count = count($sqlexp);

                        ?>

                                <?php
                                $total = $daybook_expense_tot_amount = 0;
                                foreach ($sqlexp as $newsql) {
                                    $expense_types_listing = Projects::model()->setExpenseList($expense_types_listing, $newsql);
                                    $daybook_expense_tot_amount += $newsql['paidamount'];
                                }
                                foreach ($sqlexp as $newsql) {
                                    if ($newsql['paidamount'] != 0) {
                                ?>
                                        
                                            <?php $total += $newsql['paidamount']; ?>
                                       
                                <?php
                                    }
                                    $daybook_expense_index++;
                                }

                                ?>


                            <?php
                                $grand += $total;
                                $daybook_exp_total += $total;
                            }
                            ?>
                        <?php

                        }
                        ?>
                        
                <?php } ?>
                <!--Despatch and Consumption Request STARTS-->
                <?php
                $despatch_amount = 0; 
                //if ($_GET['reconcil_type'] == 'reconcil') {
                    foreach ($dispatch_details as $data) {
                        $sql_despatch= "SELECT SUM(
                            CASE 
                                WHEN warehousedespatch_amount IS NOT NULL THEN warehousedespatch_amount
                                ELSE warehousedespatch_baseqty * warehousedespatch_baserate
                            END
                        ) AS total_amount
                        FROM jp_warehousedespatch_items
                        WHERE warehousedespatch_id =".$data['warehousedespatch_id'];
                        $rate = Yii::app()->db->createCommand($sql_despatch)->queryScalar();
                        $despatch_amount += $rate;
                    }
                    
                    ?>


                <?php //}  ?>
                <?php
                $consumption_amount=0;
               
                if ($_GET['reconcil_type'] == 'reconcil') {
                    if(!empty($consumptionreq_details)){
                        foreach ($consumptionreq_details as $datas) {
                            $consumptionreq = PmsAccWprItemConsumed::model()->findAll('consumption_id = :consumption_id', array(':consumption_id' => $datas->id));
                            foreach($consumptionreq as $req){
                                $rate=$req['item_qty'] * $req['item_rate'];
                                $consumption_amount  +=  $rate;
                            }
                            
                        }
                    }
                    
                 }else{
                    if(!empty($unconsumptionreq_details)){
                        foreach ($unconsumptionreq_details as $datas) {
                           $consumptionreq = PmsAccWprItemConsumed::model()->findAll('consumption_id = :consumption_id', array(':consumption_id' => $datas->id));
                            foreach($consumptionreq as $req){
                                $rate=$req['item_qty'] * $req['item_rate'];
                                $consumption_amount  +=  $rate;
                            }
                        }
                    }
                } 

                ?>
                <!--Despatch and Consumption Request ENDS-->
                <?php
                if ($_GET['reconcil_type'] == 'reconcil') {
                
                        $check_array = array();
                        $daybook_expenseamount = 0;
                        $subcontractor_payment_lists = Projects::model()->setSubcontractorPayment($daybook_expense);
                        foreach ($subcontractor_payment_lists as $daybook_expense) {
                            $scpayment_group_count = count($daybook_expense) - 1;
                            $sc_index = 1;
                            foreach ($daybook_expense as $newdata) {
                                if (is_array($newdata)) {
                                    $daybook_expenseamount += $newdata['paid'];
                                    $tblpx = Yii::app()->db->tablePrefix;
                                    $payment = SubcontractorPayment::model()->findByPk($newdata['subcontractor_id']);
                                    $query = "";
                                    $subcontractor_id = '';
                                    if (isset($payment->subcontractor_id)) {
                                        $query = ' AND b.subcontractor_id=' . $payment->subcontractor_id . '';
                                        $subcontractor_id = $payment->subcontractor_id;
                                    }
                                    $sub_total1 = Yii::app()->db->createCommand("select SUM(a.paidamount) as paid_amount,count(a.exp_id) as total_count,b.* from jp_expenses a LEFT JOIN jp_subcontractor_payment b ON a.subcontractor_id=b.payment_id WHERE a.amount !=0 AND a.projectid=" . $project_id . " AND a.type = '73' AND a.exptype IS NULL AND a.vendor_id IS NULL AND a.bill_id IS NULL AND a.date ='" . $newdata['date'] . "' " . $query . " GROUP BY a.date,b.subcontractor_id")->queryRow();
                                    $check_data = $newdata['date'] . '-' . $subcontractor_id;
                                    if (in_array($check_data, $check_array)) {
                                        $flag = 0;
                                        $row_count = "";
                                    } else {
                                        $flag = 1;
                                        $row_count = $sub_total1['total_count'];
                                    }
                                    array_push($check_array, $check_data);
                        
                                            $payment = SubcontractorPayment::model()->findByPk($newdata['subcontractor_id']);
                                            if (isset($payment->subcontractor_id)) {
                                                $subcontractor_data = Subcontractor::model()->findByPk($payment->subcontractor_id);
                                            }
                                            ?>

                        <?php
                                    $sc_index++;
                                }
                            }
                        }
                        $grand += $daybook_expenseamount;
                        
                        ?>

                <?php } else { ?>
                    
                        <?php
                        $check_array = array();
                        $daybook_expenseamount = 0;
                        $subcontractor_payment_lists = Projects::model()->setSubcontractorPayment($daybook_expense1);
                        foreach ($subcontractor_payment_lists as $daybook_expense1) {
                            $scpayment_group_count = count($daybook_expense1) - 1;
                            $sc_index = 1;
                            foreach ($daybook_expense1 as $newdata) {
                                if (is_array($newdata)) {
                                    $daybook_expenseamount += $newdata['paid'];
                                    $tblpx = Yii::app()->db->tablePrefix;
                                    $payment = SubcontractorPayment::model()->findByPk($newdata['subcontractor_id']);
                                    $query = "";
                                    $subcontractor_id = '';
                                    if (isset($payment->subcontractor_id)) {
                                        $query = ' AND b.subcontractor_id=' . $payment->subcontractor_id . '';
                                        $subcontractor_id = $payment->subcontractor_id;
                                    }
                                    $sub_total1 = Yii::app()->db->createCommand("select SUM(a.paidamount) as paid_amount,count(a.exp_id) as total_count,b.* from jp_expenses a LEFT JOIN jp_subcontractor_payment b ON a.subcontractor_id=b.payment_id WHERE a.amount !=0 AND a.projectid=" . $project_id . " AND a.type = '73' AND a.exptype IS NULL AND a.vendor_id IS NULL AND a.bill_id IS NULL AND a.date ='" . $newdata['date'] . "' " . $query . " GROUP BY a.date,b.subcontractor_id")->queryRow();
                                    $check_data = $newdata['date'] . '-' . $subcontractor_id;
                                    if (in_array($check_data, $check_array)) {
                                        $flag = 0;
                                        $row_count = "";
                                    } else {
                                        $flag = 1;
                                        $row_count = $sub_total1['total_count'];
                                    }
                                    array_push($check_array, $check_data);
                        ?>


                                       
                                            <?php
                                            $payment = SubcontractorPayment::model()->findByPk($newdata['subcontractor_id']);
                                            if (isset($payment->subcontractor_id)) {
                                                $subcontractor_data = Subcontractor::model()->findByPk($payment->subcontractor_id);
                                            }
                                            ?>
                                        
                                        

                        <?php
                                    $sc_index++;
                                }
                            }
                        }
                        $grand += $daybook_expenseamount;
                        
                        ?>

                <?php } ?>
                
                    <?php
                    $recon_status = (isset($_GET['reconcil_type'])&& $_GET['reconcil_type'] =="reconcil")?'1':'0';
                    $labour_expenses = Dailyreport::model()->getLabourExpenses($project_id,$recon_status);
                    $labour_total_amount = 0;
                    foreach ($labour_expenses as $labour_expense) {
                        $labour_exp_count = count($labour_expense) - 2;
                        $labour_exp_index = 1;

                        foreach ($labour_expense as $labour_data) {
                            if (is_array($labour_data)) {
                    ?>
                                

                                    
                                    <?php
                                    if ($labour_exp_index == 1) {
                                    ?>
                                        
                                    <?php
                                        $labour_total_amount += $labour_expense['total_amount'];
                                    }
                                    ?>

                                
                    <?php
                                $labour_exp_index++;
                            }
                        }
                    }
                    
                    ?>
            
                    

                    <?php
                    $exp_total_val = 0;
                    foreach ($expense_types_listing as $expense_type) {
                        $exp_total_val += $expense_type['amount'];
                    
                    }
                    ?>

                   
                        <?php
                        $final_total = $bill_amount + $return_total_amount + $daybook_exp_total + $daybook_expenseamount;
                        ?>
                        
                    <?php
                    $where = '';
                    if (!empty($date_from) && !empty($date_to)) {
                        $where .= " AND {$tblpx}expenses.date between '" . $date_from . "' and'" . $date_to . "'";
                    } else {
                        if (!empty($date_from)) {
                            $where .= " AND {$tblpx}expenses.date >= '" . $date_from . "'";
                        }
                        if (!empty($date_to)) {
                            $date_to = date('Y-m-d', strtotime($date_to));
                            $where .= " AND {$tblpx}expenses.date <= '" . $date_to . "'";
                        }
                    }
                    $percentage = Yii::app()->db->createCommand("SELECT percentage
                                     FROM {$tblpx}projects
                                     inner join " . $tblpx . "expenses on " . $tblpx . "expenses.projectid = " . $tblpx . "projects.pid
                                     WHERE pid = {$project_id} " . $where . "")->queryRow();

                    $servise_charge = (($bill_amount + $grand) * $percentage['percentage']) / 100;
                    if ($invoice >  ($bill_amount + $grand + $servise_charge)) {
                        $balance = $clienttotal - $invoice;                        
                    } else {
                        $balance = $clienttotal - ($bill_amount + $grand + $servise_charge);
                    }
                    ?>

               
                <br>
                <table class="table">
                    <!--html added here-->
                    <tr>
                        <td ><b>SALES QUOTATION :</b></td>
                        <td class="text-right project-report-width"><?php echo Quotation::model()->getTotalQuotationProjectCostAmount($project_id); ?></td>
                    </tr>

                    <tr>
                        <td ><b>INVOICE AMOUNT (A) :</b></td>
                        <input type="hidden" val="<?php echo $invoice ?>" class="invoice_amount_col">
                        <td align="right" class="invoice_amount_col project-report-width">
                            <?php echo Controller::money_format_inr($invoice, 2, 1); ?></td>
                    </tr>
                    
                    <tr>
                        <td ><b>EXPENSE HEAD SUMMARY : </b></td>
                        <td align="right" class=""></td>
                    </tr>
                     <?php if(!empty($daybook_purchase)){?>
                    <tr>
                        <td  ><b>DAYBOOK EXPENSE HEAD : </b>
                        </td> 
                        <td align="right" class="project-report-width"></td>
                    </tr>
                    <?php } ?>
                    <?php
                   
                        
                    foreach ($daybook_purchase as $new) {
                        $where = '';

                        // Add date filtering
                        if (!empty($date_from)) {
                            $where .= " AND {$tblpx}expenses.date >= '" . $date_from . "'";
                        }
                        if (!empty($date_to)) {
                            $date_to = date('Y-m-d', strtotime($date_to));
                            $where .= " AND {$tblpx}expenses.date <= '" . $date_to . "'";
                        }
                      if($exp_cal==0){
                         // Build SQL query
                                                    $sql = "SELECT 
                                COALESCE(bill_id, exp_id) AS unique_id,
                                date,
                                vendor_id,
                                exptype,
                                projectid,
                                type,
                                reconciliation_status,
                                bank_id,
                                CASE 
                                    WHEN bill_id IS NOT NULL THEN amount 
                                    ELSE paidamount 
                                END AS amount
                            FROM 
                                " . $tblpx . "expenses 
                            WHERE 
                                amount != 0 
                                AND exptype = " . $new['type_id'] . "
                                AND projectid = " . $new['project_id'] . " " . $where . "
                                AND exptype IS NOT NULL 
                                AND type = '73' 
                                AND vendor_id IS NOT NULL 
                                AND (" . $newQuery2 . ")
                                AND (
                                    (reconciliation_status IS NULL AND bank_id IS NULL) 
                                    OR (reconciliation_status = 1 AND bank_id IS NOT NULL)
                                )
                            GROUP BY 
                                COALESCE(bill_id, exp_id)
                            ORDER BY 
                                date;
                            ";
                        

                      }else{
                         // Build SQL query
                        $sql = "SELECT * 
                    FROM " . $tblpx . "expenses 
                    WHERE 
                        amount != 0 
                        AND exptype = " . $new['type_id'] . "
                        AND projectid = " . $new['project_id'] . " " . $where . "
                        AND exptype IS NOT NULL 
                        AND type = '73' 
                        AND vendor_id IS NOT NULL AND bill_id IS NULL 
                        AND (" . $newQuery2 . ")
                        AND (
                            (reconciliation_status IS NULL AND bank_id IS NULL) 
                            OR (reconciliation_status = 1 AND bank_id IS NOT NULL)
                        )
                    GROUP BY COALESCE(bill_id, exp_id)
                    ORDER BY date";
                        
                      }
                       
                        $sqlexp = Yii::app()->db->createCommand($sql)->queryAll();
                       // echo "<pre>";print_r($sqlexp);exit;

                        if (!empty($sqlexp)) {
                            // Use array_sum to calculate the total amount
                            $daybook_expense_tot_amount = array_sum(array_column($sqlexp, 'amount'));

                           
                            ?>
                            <tr>
                                <td >
                                    <b> &nbsp;&nbsp;&nbsp;<?php echo $new['type_name'] ? strtoupper($new['type_name']) : "N/A"; ?></b>
                                </td>
                                <td class="text-right project-report-width">
                                    <?php echo Controller::money_format_inr($daybook_expense_tot_amount, 2); ?>
                                </td>
                            </tr>
                            <?php
                        }
                    }



                   
                    $recon_status = (isset($_GET['reconcil_type']) && $_GET['reconcil_type'] == "reconcil") ? '1' : '0';
                    $labour_expenses = Dailyreport::model()->getNoSubLabourExpenses($project_id, $recon_status);
                    $labour_total_no_sub =0;
                    if (!empty($labour_expenses)) {
                        ?>
                        <tr>
                            <td ><b>LABOUR ENTRY (No subcontractor):</b></td>
                            <td class="text-right  project-report-width"></td>
                        </tr>
                        <?php

                        foreach ($labour_expenses as $labour_expense) {
                            $labour_exp_count = count($labour_expense) - 2; // Total rows to rowspan
                            $labour_exp_index = 1; // Track rows for current expense head

                            foreach ($labour_expense as $labour_data) {
                                if (is_array($labour_data)) {
                                    $labour_total_no_sub += $labour_data['amount'];

                                    // Show expense head and total amount
                                    if ($labour_exp_index == 1) {
                                        ?>
                                        <tr>
                                            <td ><b>&nbsp;&nbsp;&nbsp;<?php echo strtoupper($labour_expense['expense_head']); ?></b></td>
                                            <td class="text-right  project-report-width">
                                                <?php echo Controller::money_format_inr($labour_expense['total_amount'], 2); ?>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    $labour_exp_index++; // Increment the row index
                                }
                            }
                        }
                    }
                    $grand +=$labour_total_no_sub;

                    ?>
                    <?php 
                    
                    
                    if($exp_cal==1){
                        $grand +=$consumption_amount;
                        ?>

                        <tr>
                                <td  ><b>MATERIAL CONSUMED EXPENSE:</b></td>
                                <td class="text-right  project-report-width"></td>
                        </tr>
                        <tr>
                            <td ><b>&nbsp;&nbsp;&nbsp;MATERIAL EXP:</b></td>
                            <td class="text-right  project-report-width">
                                <?php echo Controller::money_format_inr($consumption_amount, 2); ?>
                            </td>
                        </tr>
                        <?php 
                    } ?>
                    <tr>
                        <td ><b>TOTAL EXPENSE (G) :</b>  <span style="text-decoration: underline; cursor: pointer;" title="Daybook expense ( Vendor payment) + Subcontractor payment + Labour Entry (No subcontractor) + Material Consumed or Daybook Purchase Bill(Based on selection of company expense calculation)">
                        More Info..
                        </span>
                        </td>
                        <?php 
                        if($exp_cal==0){
                        ?>
                        <td align="right" class="expense_amount_col text-right  project-report-width">
                            <?php echo Controller::money_format_inr(($grand + $bill_amount), 2, 1); ?></td>
                        <?php }else{ ?>
                            <td align="right" class="expense_amount_col text-right  project-report-width">
                            <?php echo Controller::money_format_inr(($grand), 2, 1); ?></td>

                        <?php } ?>
                    </tr>
                    <tr>
                        <td ><b>AMOUNT RECEIVED (B) : </b></td>
                        <td align="right" class="text-right  project-report-width"><?php echo Controller::money_format_inr($clienttotal, 2, 1); ?></td>
                    </tr>
                    <tr>
                        <td ><b>EXPENSE (including Dispatch) (G+Despatch):</b></td>
                         <?php 
                        if($exp_cal==0){
                        ?>
                        <td align="right" class="expense_amount_col text-right  project-report-width">
                            <?php echo Controller::money_format_inr(($grand + $bill_amount+$despatch_amount), 2, 1); ?></td>
                        <?php }else{ ?>
                             <td align="right" class="expense_amount_col text-right  project-report-width">
                            <?php echo Controller::money_format_inr(($grand +$despatch_amount), 2, 1); ?></td>
                        <?php }?>
                    </tr>
                    <?php 
                    if($exp_cal==0){
                    ?>
                    <tr>
                        <td ><b>EXPENSE (including Consumption) ( [G-Purchase]+Consumption):</b></td>

                        <td align="right" class="expense_amount_col text-right  project-report-width">
                            <?php echo Controller::money_format_inr(($grand + $bill_amount)-$bill_amount +$consumption_amount, 2, 1); ?></td>

                    </tr>
                    <?php } ?>

                    <tr>

                        <td ><b>SERVICE CHARGE :</b></td>
                        <td align="right" class="text-right  project-report-width"><?php echo Controller::money_format_inr($servise_charge, 2, 1); ?>
                        </td>

                    </tr>
                    <tr>
                        <td ><b>PROFIT AND LOSS (P/L) = (B)-(G) : </b></td>
                         <?php 
                        if($exp_cal==0){
                        ?>
                        <td align="right" class="balance_bottom text-right  project-report-width"><?php echo Controller::money_format_inr(($clienttotal-($grand + $bill_amount)), 2); ?>
                        </td>
                        <?php }else{ ?>
                            <td align="right" class="balance_bottom text-right  project-report-width"><?php echo Controller::money_format_inr(($clienttotal-($grand )), 2); ?>
                        </td>
                        <?php }?>

                    </tr>
                    <tr>
                        <td ><b>GST LESS : </b></td>
                       
                        <td align="right" class="balance_bottom text-right  project-report-width"><?php echo isset($clientgstresult)?Controller::money_format_inr(($clientgstresult['gst']), 2):'0.00'; ?>
                        </td>

                    </tr>
                    <tr>
                        <?php 
                        $gst_amount=0;
                        $net_profit=0;
                        if(!empty($clientgstresult['gst'])){
                            $gst_amount=$clientgstresult['gst'];
                         }
                          if($exp_cal==0){
                            $net_profit=($clienttotal-($grand + $bill_amount))-$gst_amount;
                          }else{
                             $net_profit=($clienttotal-($grand ))-$gst_amount;
                          }
                        
                        
                        ?>
                        <td ><b>NET PROFIT (P/L)-(GST LESS) : </b><br>
                       </td>
                        <td align="right" class="balance_bottom text-right  project-report-width"><?php echo Controller::money_format_inr(($net_profit), 2); ?>
                        </td>

                    </tr>
                    <?php 
                    $percentage = Yii::app()->db->createCommand("SELECT percentage
                                     FROM {$tblpx}projects
                                     inner join " . $tblpx . "expenses on " . $tblpx . "expenses.projectid = " . $tblpx . "projects.pid
                                     WHERE pid = {$project_id} " . $where . "")->queryRow();
                    if($exp_cal==0){
                        $servise_charge = (($bill_amount + $grand) * $percentage['percentage']) / 100;
                        if ($invoice >  ($bill_amount + $grand + $servise_charge)) {
                            $balance = $clienttotal - $invoice;                        
                        } else {
                            $balance = $clienttotal - ($bill_amount + $grand + $servise_charge);
                        }
                    }else{
                        $servise_charge = (( $grand) * $percentage['percentage']) / 100;
                        if ($invoice >  ( $grand + $servise_charge)) {
                            $balance = $clienttotal - $invoice;                        
                        } else {
                            $balance = $clienttotal - ( $grand + $servise_charge);
                        }
                    }
                    ?>
                    <tr>
                        <td ><b>BALANCE AMOUNT (B)-(G*) : </b><br>
                        &nbsp;&nbsp;* Greater of Invoice or Expense is used as G in the calculation</td>
                        <td align="right" class="balance_bottom text-right  project-report-width"><?php echo Controller::money_format_inr(($balance), 2); ?>
                        </td>

                    </tr>
                    
                   
                </table>
