<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/tablestyles.css" />
<link rel="stylesheet" type="text/css" href="uploads/new_template/css/newstyle.css" />
<style>
    table {
        border-collapse: collapse;
        width: 100%;
    }
    /*.container{max-width: 1050px;margin:0 auto;padding: 15px;border: 1px solid #ddd;}*/
    .text-right{text-align:right}
    .details,.info-table{border: 1px solid #212121;margin-top: 20px;}
    .details td, .info-table td,.info-table th{padding: 6px 8px; }
    .info-table{margin-bottom: 10px;}
    .text-center{text-align:center;}
    .img-hold{width: 10%;}
    .companyhead{font-size: 18px;font-weight:bold;margin-top:0px;color: #789AD1;}
</style>
<?php
    $company_address = Company::model()->findBypk(!empty($company_id)?$company_id:Yii::app()->user->company_id);	
?>
<div id="contenthtml">
    <div style="margin-left:30px;">
        <table border=0>
            <tbody>
                    <tr>
                        <td class=text-right>GST NO: <?php echo isset($company_address["company_gstnum"])?$company_address["company_gstnum"]:''; ?></td>
                    </tr>
            </tbody>
        </table>
        <table border=0>
            <tbody>
                <tr>
                    <td class="img-hold"><img  src="<?php echo Yii::app()->theme->baseUrl; ?>/images/logo.png" alt="img" style='margin-top:-45px;'/></td>
                    <td class="text-center"><p class='companyhead'><?php echo isset($company_address['name'])?$company_address['name']:''; ?></p><br>
                        <p><?php echo ( (isset($company_address['address']) && !empty($company_address['address'])) ? ($company_address['address']) : ""); ?><br>
                        PIN : <?php echo ( (isset($company_address['pincode']) && !empty($company_address['pincode'])) ?($company_address['pincode']) : ""); ?>,&nbsp;
                        PHONE : <?php echo ( (isset($company_address['phone']) && !empty($company_address['phone'])) ?( $company_address['phone']) : ""); ?><br>
                        EMAIL : <?php echo ( (isset($company_address['email_id']) && !empty($company_address['email_id'])) ?( $company_address['email_id']) : ""); ?></p><br>                        
                        <h4>Salary Report</h4>   
                    </td>
                </tr>
            </tbody>
        </table>
        <table class="table table-bordered table-striped" border="1">
        
            <thead>
               <tr>
                    <td colspan="2">Employee Name :<?= $name ?>
                    &nbsp; &nbsp;
                    <?php
                    if (!empty($company_id)){
                                $company = Company::model()->findByPk($company_id);
                                echo "Company: ". $company->name;
                            }
                    ?>
                    </td>
               </tr>
               <tr>
               <td colspan="2" class="whiteborder">&nbsp;</td>
               </tr>
               <tr>
                   <td></td>
                    <td align='right'><b>Amount</b></td>
                </tr> 
            </thead>
            <tbody>
                
                <?= $petty_entries ?>
            </tbody>
        </table>
        
    </div>
</div>