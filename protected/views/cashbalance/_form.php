<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.js"></script>
<?php
$user = Users::model()->findByPk(Yii::app()->user->id);
$arrVal = explode(',', $user->company_id);
$newQuery = "";
foreach ($arrVal as $arr) {
    if ($newQuery)
        $newQuery .= ' OR';
    $newQuery .= " FIND_IN_SET('" . $arr . "', id)";
}
$company = Company::model()->findAll(array('condition' => $newQuery));
?>

<div class="">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'cashbalance-form',
        'enableAjaxValidation' => false,
        'enableClientValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true,
            'validateOnType' => false,
        ),
    ));
    ?>

    <?php
    $tblpx = Yii::app()->db->tablePrefix;
    $data = Yii::app()->db->createCommand("select * FROM {$tblpx}status WHERE status_type='cashbalance_type'")->queryAll();
    ?>

    <div class="panel-body clearfix custom-form-style">

        <div class="row addRow">

            <div class="col-md-3 col-sm-6 elem_block">
                <?php echo $form->labelEx($model, 'company_id'); ?>
                <select name="Cashbalance[company_id]" id="Cashbalance_company_id" class="form-control js-example-basic-multiple">
                    <option value="">Choose Company</option>
                    <?php
                    foreach ($company as $key => $value) {
                        ?>
                        <option value="<?php echo $value['id']; ?>" <?php echo ($value['id'] == $model->company_id) ? 'selected' : ''; ?>><?php echo $value['name']; ?></option>
                        <?php
                    }
                    ?>

                </select>
                <?php echo $form->error($model, 'company_id'); ?>
            </div>

            <?php
            $tblpx = Yii::app()->db->tablePrefix;
            $paymentData = Yii::app()->db->createCommand("SELECT * FROM " . $tblpx . "cashbalance WHERE cashbalance_type = 100 AND company_id=" . Yii::app()->user->company_id . "")->queryAll();
            ?>

            <div class="col-md-3 col-sm-6 elem_block">
                <?php echo $form->labelEx($model, 'cashbalance_type'); ?>				
                <?php
                if ($model->isNewRecord) {
                    ?>
                    <?php
                    if (empty($paymentData)) {
                        echo $form->dropDownList($model, 'cashbalance_type', CHtml::listData(Status::model()->findAll(array('order' => 'caption ASC', 'condition' => 'status_type= "cashbalance_type"')), 'sid', 'caption'), array('empty' => '--', 'class' => 'form-control type'));
                    } else {
                        echo $form->dropDownList($model, 'cashbalance_type', CHtml::listData(Status::model()->findAll(array('order' => 'caption ASC', 'condition' => 'sid= 99')), 'sid', 'caption'), array('empty' => '--', 'class' => 'form-control type'));
                    }
                    ?>
                <?php } else { ?>
                    <?php
                    if ($model->cashbalance_type == 100) {
                        echo $form->dropDownList($model, 'cashbalance_type', CHtml::listData(Status::model()->findAll(array('order' => 'caption ASC', 'condition' => 'sid= 100')), 'sid', 'caption'), array('empty' => '--', 'class' => 'form-control type'));
                    } else {
                        echo $form->dropDownList($model, 'cashbalance_type', CHtml::listData(Status::model()->findAll(array('order' => 'caption ASC', 'condition' => 'sid= 99')), 'sid', 'caption'), array('empty' => '--', 'class' => 'form-control type'));
                    }
                    ?>
                <?php } ?>

                <?php echo $form->error($model, 'cashbalance_type'); ?>
            </div>

            <div class="col-md-3 col-sm-6 elem_block bank">
                <?php echo $form->labelEx($model, 'bank_id'); ?>				
                <?php 
                $user = Users::model()->findByPk(Yii::app()->user->id);
                $arrVal = explode(',', $user->company_id);
                $newQuery = "";
                foreach($arrVal as $arr) {
                    if ($newQuery) $newQuery .= ' OR';
                    $newQuery .= " FIND_IN_SET('".$arr."', company_id)";
                }
                echo $form->dropDownList($model, 'bank_id', CHtml::listData(Bank::model()->findAll(array('order' => 'bank_id DESC', 'condition' => $newQuery)), 'bank_id', 'bank_name'), array('empty' => '--', 'class' => 'form-control')); ?>

                <?php echo $form->error($model, 'bank_id'); ?>
            </div>

            <div class="col-md-3 col-sm-6 elem_block">
                <label>Date</label>	
                <div class="display-flex">		
                    <?php
                    if ($model->isNewRecord) {
                        $date_to = date("Y-m-d");
                    } else {
                        $date_to = $model->cashbalance_date;
                    }
                    ?>
                    <?php
                    echo CHtml::textField('Cashbalance[cashbalance_date]',  $date_to, array("id" => "date_from", 'class' => 'form-control', 'style' => 'width:85%;display:inline-block;'));
                    ?>

                    <?php echo CHtml::image("images/calendar-month.png", "calendar", array("id" => "c_button3", "class" => "pointer", "style" => "cursor:pointer;width:25px;display:inline-block;"));
                    ?>
                    <?php
                    $this->widget('application.extensions.calendar.SCalendar', array(
                        'inputField' => 'date_from',
                        'button' => 'c_button3',
                        'ifFormat' => '%Y-%m-%d',
                    ));
                    ?>
                </div>	
            </div>

            <div class="col-md-3 col-sm-6 elem_block">
                <?php echo $form->labelEx($model, 'cashbalance_opening_balance'); ?>
                <?php echo $form->textField($model, 'cashbalance_opening_balance', array('class' => 'form-control')); ?>
                <?php echo $form->error($model, 'cashbalance_opening_balance'); ?>
            </div>


        </div>
    </div>

    <div class="modal-footer save-btnHold">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
        <?php
        if (!$model->isNewRecord) {

            echo CHtml::Button('Close', array('onclick' => 'closeaction(this,event)'));
        } else {
            echo CHtml::ResetButton('Reset', array('style' => 'margin-right:3px;'));
            echo CHtml::ResetButton('Close', array('onclick' => 'closeaction(this,event)'));
        }
        ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->
<script>
    $(document).ready(function () {
        $('.js-example-basic-multiple').select2({
            width: '200px'
        });
    });
    var type = '<?php echo $model->cashbalance_type; ?>';
    if (type == '' || type == 100) {
        $('.bank').hide();
    } else {
        $('.bank').show();
    }

    $('.type').change(function () {
        var val = $(this).val();
        if (val == 99) {
            $('.bank').show();
        } else {
            $('.bank').hide();
        }
    })


    $("#Cashbalance_company_id").change(function () {
        var val = $(this).val();
        $("#Cashbalance_bank_id").html('<option value="">Select Bank</option>');
        $('#loading').show();
        $.ajax({
            url: '<?php echo Yii::app()->createUrl('cashbalance/dynamicbank'); ?>',
            method: 'POST',
            data: {
                company_id: val
            },
            dataType: "json",
            success: function (response) {
                $("#Cashbalance_cashbalance_type").html(response.type);
                if (response.status == 'success') {
                    $("#Cashbalance_bank_id").html(response.html);
                } else {
                    $("#Cashbalance_bank_id").html(response.html);
                }
            }
        })
    })
    $(document).ajaxComplete(function () {
        $('.loading-overlay').removeClass('is-active');
        $('#loading').hide();
    });

</script>