<?php
/* @var $this SubcontractorbillController */
/* @var $model Subcontractorbill */
/* @var $form CActiveForm */
?>
<div class="alert alert-success hide"></div>
<div class="alert alert-danger hide"></div>
<div id="loading" style="display:none;"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/ajax-loader.gif">
</div>
<div class="form">
    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'subcontractorbill-form',
        'enableAjaxValidation' => false,
        'clientOptions' => array(
            'validateOnSubmit' => true,
        ),
    )); ?>
    <?php
    $user = Users::model()->findByPk(Yii::app()->user->id);
    $arrVal = explode(',', $user->company_id);
    $newQuery = "";
    foreach ($arrVal as $arr) {
        if ($newQuery)
            $newQuery .= ' OR';
        $newQuery .= " FIND_IN_SET('" . $arr . "', id)";
    }
    $scquotation = Scquotation::model()->findByPk($model->scquotation_id);
    $project_id = $scquotation["project_id"];
    $subcontractor_id = $scquotation["subcontractor_id"];
    $tblpx = Yii::app()->db->tablePrefix;
    ?>
    <div class="row">
        <div class="form-group col-xs-12 col-md-3">
            <?php echo $form->labelEx($model, 'company_id'); ?>
            <?php echo $form->dropDownList($model, 'company_id', CHtml::listData(Company::model()->findAll(array(
                'select' => array('id, name'),
                "condition" => '(' . $newQuery . ')',
                'order' => 'id',
                'distinct' => true
            )), 'id', 'name'), array('class' => 'form-control company_id', 'empty' => '-Select Company-')); ?>
            <?php echo $form->error($model, 'company_id'); ?>

        </div>
        <div class="form-group col-xs-12 col-md-3">
            <label for="Subcontractorbill_project" class="required projectlabel">Project <span
                    class="required">*</span></label>
            <select class="form-control" id="project_id">
                <option value="">Select Project</option>
                <?php
                $company_id = $model->company_id;
                $projectdata = Projects::model()->findAll(array('select' => 'pid,name', 'condition' => "(FIND_IN_SET('" . $company_id . "', company_id))"));
                foreach ($projectdata as $data) {
                    if ($data->pid == $project_id)
                        $selected = " selected";
                    else
                        $selected = "";
                    echo "<option value='" . $data->pid . "'" . $selected . ">" . $data->name . "</option>";
                }
                ?>
            </select>
            <div class="errorMessage" id="projectErrorMsg"></div>

        </div>
        <div class="form-group col-xs-12 col-md-3">

            <label for="Subcontractorbill_subcontractor" class="required subcontractorlabel">Subcontractor <span
                    class="required">*</span></label>
            <select class="form-control" id="subcontractor_id">
                <option value="">Select Subcontractor</option>
                <?php
                $subcontractor = Yii::app()->db->createCommand("SELECT sc.subcontractor_id, sc.subcontractor_name FROM {$tblpx}subcontractor sc
                                        LEFT JOIN {$tblpx}scquotation sq ON sc.subcontractor_id = sq.subcontractor_id
                                        WHERE sq.project_id = '{$project_id}' AND sq.company_id = '{$company_id}' GROUP BY sc.subcontractor_id")->queryAll();

                foreach ($subcontractor as $data) {
                    if ($data["subcontractor_id"] == $subcontractor_id)
                        $selected = " selected";
                    else
                        $selected = "";
                    echo "<option value='" . $data["subcontractor_id"] . "'" . $selected . ">" . $data["subcontractor_name"] . "</option>";
                }
                ?>
            </select>
            <div class="errorMessage" id="subcontractorErrorMsg"></div>
        </div>



        <div class="form-group col-xs-12 col-md-3">

            <?php echo $form->labelEx($model, 'scquotation_id'); ?>
            <select class="form-control" name="Subcontractorbill[scquotation_id]" id="Subcontractorbill_scquotation_id">
                <option value="">Select Quotation</option>
                <?php
                $quotationdata = Scquotation::model()->findAll(array('select' => 'scquotation_id,scquotation_no,scquotation_decription', 'condition' => "project_id = '" . $project_id . "' AND subcontractor_id = '" . $subcontractor_id . "' AND company_id = '" . $company_id . "'"));

                foreach ($quotationdata as $data) {
                    if ($data->scquotation_id == $model->scquotation_id)
                        $selected = " selected";
                    else
                        $selected = "";
                    echo "<option value='" . $data->scquotation_id . "'" . $selected . ">" . $data->scquotation_no . "</option>";
                }
                ?>
            </select>
            <?php echo $form->error($model, 'scquotation_id'); ?>

        </div>
        <div class="form-group col-xs-12 col-md-3">

            <?php echo $form->labelEx($model, 'stage_id'); ?>
            <select class="form-control" name="Subcontractorbill[stage_id]" id="Subcontractorbill_stage_id">
                <option value="">Select Stage</option>
                <?php
                $stagedata = ScPaymentStage::model()->findAll(array('select' => 'id,payment_stage', 'condition' => "quotation_id = '" . $model->scquotation_id . "'"));

                foreach ($stagedata as $data) {
                    if ($data->id == $model->stage_id)
                        $selected = " selected";
                    else
                        $selected = "";
                    echo "<option value='" . $data->id . "'" . $selected . ">" . $data->payment_stage . "</option>";
                }
                ?>
            </select>
            <?php echo $form->error($model, 'stage_id'); ?>

        </div>
        <div class="form-group col-xs-12 col-md-3">
            <?php echo $form->labelEx($model, 'bill_number'); ?>
            <?php echo $form->textField($model, 'bill_number', array(
                'size' => 60,
                'maxlength' => 100,
                'class' => 'form-control',
                'rows' => 3,
                'cols' => 50
            )); ?>

            <?php echo $form->error($model, 'bill_number'); ?>

        </div>
        <div class="form-group col-xs-12 col-md-3">

            <?php echo $form->labelEx($model, 'date'); ?>
            <?php echo $form->textField($model, 'date', array('class' => 'form-control')); ?>
            <?php echo $form->error($model, 'date'); ?>

        </div>
        <div class="form-group col-xs-12 col-md-3">

            <?php
            echo $form->labelEx($model, 'total_amount'); ?>
            <?php echo $form->textField($model, 'total_amount', array('class' => 'form-control total_amount', 'autocomplete' => 'off', 'readonly' => 'readonly')); ?>


        </div>
        <div class="form-group col-xs-12 col-md-3">

            <?php echo $form->labelEx($model, 'remarks'); ?>
            <?php echo $form->textArea($model, 'remarks', array('size' => 60, 'maxlength' => 200, 'class' => 'form-control')); ?>
            <?php echo $form->error($model, 'remarks'); ?>

        </div>
        <div class="form-group col-xs-12 col-md-3">

            <?php echo $form->labelEx($model, 'requested_by'); ?>
            <?php echo $form->textField($model, 'requested_by', array('size' => 60, 'maxlength' => 100, 'class' => 'form-control')); ?>
            <?php echo $form->error($model, 'requested_by'); ?>

        </div>

    </div>
    <div class="row">
        <div class="form-group col-xs-12 text-right">

            <label for="Subcontractorbill_button">&nbsp;</label>
            <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-primary', 'id' => 'submitButton')); ?>


        </div>
    </div>
    <?php $this->endWidget(); ?>
</div><!-- form -->
</div>


<script type="text/javascript">

    $(document).on('keypress', 'input,select', function (e) {
        if (e.which == 13) {
            e.preventDefault();
            // Get all focusable elements on the page
            var $canfocus = $(':focusable');
            var index = $canfocus.index(document.activeElement) + 1;
            if (index >= $canfocus.length) index = 0;
            $canfocus.eq(index).focus();
        }
    });

    $('#Subcontractorbillitem_quotationitem_id').on("select2:select", function (e) {
        $('#description').focus();
        e.preventDefault();
    });
    $('#tax_slab').on("select2:select", function (e) {
        $('#sgstp').focus();
        e.preventDefault();
    });


    $(document).ready(function () {
        $('#loading').hide();
        $("#Subcontractorbill_company_id").select2();
        $("#project_id").select2();
        $("#subcontractor_id").select2();
        $("#Subcontractorbill_scquotation_id").select2();
        $("#Subcontractorbillitem_quotationitem_id").select2();
        $("#tax_slab").select2();
        $(function () {
            $("#Subcontractorbill_date").datepicker({
                dateFormat: 'dd-mm-yy'
            });
        });
        $("#Subcontractorbill_company_id").change(function () {
            $('#loading').show();
            var company_id = $(this).val();
            $.ajax({
                url: "<?php echo $this->createUrl('subcontractorbill/getprojectbycompany') ?>",
                data: {
                    "company_id": company_id
                },
                type: "POST",
                success: function (data) {
                    $("#project_id").html(data);
                }
            });
        });
        $("#project_id").change(function () {
            var project_id = $(this).val();
            $('#loading').show();
            var company_id = $("#Subcontractorbill_company_id").val();
            $.ajax({
                url: "<?php echo $this->createUrl('subcontractorbill/getsubcontractorbyproject') ?>",
                data: {
                    "company_id": company_id,
                    "project_id": project_id
                },
                type: "POST",
                success: function (data) {
                    $("#subcontractor_id").html(data);
                }
            });
        });
        $("#subcontractor_id").change(function () {
            $('#loading').show();
            var subcontractor_id = $(this).val();
            var project_id = $("#project_id").val();
            var company_id = $("#Subcontractorbill_company_id").val();
            $.ajax({
                url: "<?php echo $this->createUrl('subcontractorbill/getquotationsbyproject') ?>",
                data: {
                    "company_id": company_id,
                    "project_id": project_id,
                    "subcontractor_id": subcontractor_id
                },
                type: "POST",
                success: function (data) {
                    $("#Subcontractorbill_scquotation_id").html(data);
                }
            });
        });
        $("#submitButton").click(function () {
            var project_id = $("#project_id").val();
            var subcontractor_id = $("#subcontractor_id").val();
            if (project_id == "") {
                $("#projectErrorMsg").text("Project cannot be blank.");
                $(".projectlabel").css("color", "red");
            }
            if (subcontractor_id == "") {
                $("#subcontractorErrorMsg").text("Subcontractor cannot be blank.");
                $(".subcontractorlabel").css("color", "red");
            }
        });
        $("#amount, #sgstp, #cgstp, #igstp").keyup(function () {
            var amount = parseFloat($("#amount").val());
            if (amount == "")
                amount = 0;
            var sgstp = parseFloat($("#sgstp").val());
            if (sgstp == "")
                sgstp = 0;
            var cgstp = parseFloat($("#cgstp").val());
            if (cgstp == "")
                cgstp = 0;
            var igstp = parseFloat($("#igstp").val());
            if (igstp == "")
                igstp = 0;
            var sgst = (sgstp / 100) * amount;
            if (isNaN(sgst)) sgst = 0;
            var cgst = (cgstp / 100) * amount;
            if (isNaN(cgst)) cgst = 0;
            var igst = (igstp / 100) * amount;
            if (isNaN(igst)) igst = 0;
            var total_tax = sgst + cgst + igst;
            var total_amount = amount + total_tax;

            $("#sgstamount").text(sgst.toFixed(2));
            $("#sgst").val(sgst.toFixed(2));
            $("#cgstamount").text(cgst.toFixed(2));
            $("#cgst").val(cgst.toFixed(2));
            $("#igstamount").text(igst.toFixed(2));
            $("#igst").val(igst.toFixed(2));
            $("#total_taxamount").text(total_tax.toFixed(2));
            $("#total_tax").val(total_tax.toFixed(2));
            $("#total_amounttext").text(total_amount.toFixed(2));
            $("#total_amount").val(total_amount.toFixed(2));
        });
        $("#additem").click(function () {
            var bill_id = "<?php echo $model->id; ?>";
            var quotation_id = "<?php echo $model->scquotation_id; ?>";
            var billitem_id = $("#billitem_id").val();
            var item_id = $("#Subcontractorbillitem_quotationitem_id").val();
            var description = $("#description").val();
            var item_type = $("#item_type").val();
            var quantity = $("#quantity").val();
            var unit = $("#unit").val();
            var rate = $("#rate").val();
            var amount = $("#amount").val();
            var tax_slab = $("#tax_slab").val();
            var sgstp = $("#sgstp").val();
            var sgst = $("#sgst").val();
            var cgstp = $("#cgstp").val();
            var cgst = $("#cgst").val();
            var igstp = $("#igstp").val();
            var igst = $("#igst").val();
            var total_tax = $("#total_tax").val();
            var total_amount = $("#total_amount").val();
            if (item_id == "" || description == "" || amount == "") {
                $("#errorMessage").show()
                    .html('<div class="alert alert-danger">Enter all mandatory fields.</div>')
                    .fadeOut(10000);
                return false;
            } else {
                $('.loading-overlay').addClass('is-active');
                $.ajax({
                    url: "<?php echo $this->createUrl('subcontractorbill/addbillitem') ?>",
                    data: {
                        "bill_id": bill_id,
                        "quotation_id": quotation_id,
                        "billitem_id": billitem_id,
                        "item_id": item_id,
                        "description": description,
                        "item_type": item_type,
                        "quantity": quantity,
                        "unit": unit,
                        "rate": rate,
                        "amount": amount,
                        "tax_slab": tax_slab,
                        "sgstp": sgstp,
                        "sgst": sgst,
                        "cgstp": cgstp,
                        "cgst": cgst,
                        "igstp": igstp,
                        "igst": igst,
                        "total_tax": total_tax,
                        "total_amount": total_amount
                    },
                    type: "GET",
                    success: function (data) {
                        var result = JSON.parse(data);
                        if (result["status"] == 1) {
                            $("#errorMessage").show()
                                .html('<div class="alert alert-danger">Amount exceed. The unbilled amount for this item is ' + result["unbilled"] + '</div>')
                                .fadeOut(10000);
                            return false;
                        } else if (result["status"] == 2) {
                            $("#errorMessage").show()
                                .html('<div class="alert alert-success">Item was added succefully.</div>')
                                .fadeOut(10000);
                            $("#billitem_id").val("");
                            $("#Subcontractorbillitem_quotationitem_id").val("").trigger('change.select2');
                            $("#description").val("");
                            $("#amount").val("");
                            $("#tax_slab").val("").trigger('change.select2');
                            $("#sgstp").val("");
                            $("#sgst").val("");
                            $("#sgstamount").text("");
                            $("#cgstp").val("");
                            $("#cgst").val("");
                            $("#cgstamount").text("");
                            $("#igstp").val("");
                            $("#igst").val("");
                            $("#igstamount").text("");
                            $("#total_tax").val("");
                            $("#total_taxamount").text("");
                            $("#total_amount").val("");
                            $("#total_amounttext").text("");
                            $("#billitem_id").text("");
                            $.ajax({
                                url: "<?php echo $this->createUrl('subcontractorbill/getNewItemView') ?>",
                                data: {
                                    "bill_id": bill_id
                                },
                                type: "POST",
                                success: function (data) {
                                    $("#item_list").html(data);
                                }
                            });
                        } else {
                            $("#errorMessage").show()
                                .html('<div class="alert alert-danger">Failed! Please try again.</div>')
                                .fadeOut(10000);
                        }
                    }
                });
            }
        });
        $("body").on("click", "#editItem", function () {
            var item_id = $(this).attr("data-id");
            $('.loading-overlay').addClass('is-active');
            $.ajax({
                url: "<?php echo $this->createUrl('subcontractorbill/getItemsById') ?>",
                data: {
                    "item_id": item_id
                },
                type: "POST",
                success: function (data) {
                    var result = JSON.parse(data);
                    $("#item_type").val(result["item_type"]);
                    $("#quantity").val(result["item_quantity"]);
                    $("#unit").val(result["item_unit"]);
                    $("#rate").val(result["item_rate"]);
                    $("#billitem_id").val(result["id"]);
                    $("#Subcontractorbillitem_quotationitem_id").val(result["quotationitem_id"]).trigger('change.select2');;
                    $("#description").val(result["description"]);
                    $("#amount").val(result["amount"]);
                    $("#tax_slab").val(result["tax_slab"]);
                    $("#sgstp").val(result["cgstp"]);
                    $("#sgst").val(result["cgst"]);
                    $("#sgstamount").text(result["cgst"]);
                    $("#cgstp").val(result["sgstp"]);
                    $("#cgst").val(result["sgst"]);
                    $("#cgstamount").text(result["sgst"]);
                    $("#igstp").val(result["igstp"]);
                    $("#igst").val(result["igst"]);
                    $("#igstamount").text(result["igst"]);
                    $("#total_tax").val(result["tax_amount"]);
                    $("#total_taxamount").text(result["tax_amount"]);
                    $("#total_amount").val(result["total_amount"]);
                    $("#total_amounttext").text(result["total_amount"]);
                    $("#additem").val('Update');
                }
            });
        });
        $("body").on("click", "#removeItem", function () {
            var item_id = $(this).attr("data-id");
            var bill_id = "<?php echo $model->id; ?>";
            if (confirm("Are you sure you really want to delete this item?")) {
                $('.loading-overlay').addClass('is-active');
                $.ajax({
                    url: "<?php echo $this->createUrl('subcontractorbill/removeBillItem') ?>",
                    data: {
                        "item_id": item_id
                    },
                    type: "POST",
                    success: function (data) {
                        if (data == 1) {
                            $("#errorMessage").show()
                                .html('<div class="alert alert-success">Item was deleted succefully.</div>')
                                .fadeOut(10000);
                            $('.loading-overlay').addClass('is-active');
                            $.ajax({
                                url: "<?php echo $this->createUrl('subcontractorbill/getNewItemView') ?>",
                                data: {
                                    "bill_id": bill_id
                                },
                                type: "POST",
                                success: function (data) {
                                    $("#item_list").html(data);
                                }
                            });
                        } else {
                            $("#errorMessage").show()
                                .html('<div class="alert alert-danger">Failed! Please try again.</div>')
                                .fadeOut(10000);
                        }
                    }
                });
            } else {
                return false;
            }
        });
    });
    $(document).ajaxComplete(function () {
        $('.loading-overlay').removeClass('is-active');
        $('#loading').hide();
    });
    $(document).on("change", "#Subcontractorbillitem_quotationitem_id", function () {
        item_change();
    })

    function item_change() {
        var val = $('#Subcontractorbillitem_quotationitem_id').val();
        $.ajax({
            type: "POST",
            url: '<?php echo Yii::app()->createUrl('subcontractorbill/getItemData'); ?>',
            data: 'item_id=' + val,
            dataType: 'json',
            success: function (data) {
                console.log(data);
                if (data.status == 'success') {
                    if (data.item_type == 1) {
                        $('#item_type').val("Quantity * Rate");
                    } else if (data.item_type == 2) {
                        $('#item_type').val("Lumpsum");
                    } else {
                        $('#item_type').val("");
                    }
                    $('#quantity').val(data.quantity);
                    $('#unit').val(data.unit);
                    $('#rate').val(data.rate);
                    $('#amount').val(data.amount);
                    $("#sgstp").keyup();
                    $("#cgstp").keyup();
                    $("#igstp").keyup();
                } else {
                    $('#quantity').val('');
                    $('#unit').val('');
                    $('#rate').val('');
                    $('#amount').val('');
                }

            },
        })
    }

    $("#quantity").change(function () {
        var qty = $(this).val();
        var rate = $("#rate").val();
        var amount = qty * rate;
        $("#amount").val(amount);
        $("#sgstp").keyup();
        $("#cgstp").keyup();
        $("#igstp").keyup();
    });

    $("#rate").change(function () {
        var rate = $(this).val();
        var qty = $("#quantity").val();
        var amount = qty * rate;
        $("#amount").val(amount);
        $("#sgstp").keyup();
        $("#cgstp").keyup();
        $("#igstp").keyup();
    })

    $("#Subcontractorbill_stage_id").change(function () {
        $('#loading').show();
        var quotation_id = $("#Subcontractorbill_scquotation_id").val();
        var stage_id = $(this).val();

        $.ajax({
            url: "<?php echo $this->createUrl('subcontractorbill/GetpaymentstageAmount') ?>",
            data: {
                "stage_id": stage_id,
                "quotation_id": quotation_id
            },
            type: "POST",
            success: function (data) {
                $(".total_amount").val(data);
            }
        });
    });

    $("#Subcontractorbill_bill_number").change(function () {
        $('#loading').show();
        var bill_number = $("#Subcontractorbill_bill_number").val();
        $.ajax({
            url: "<?php echo $this->createUrl('subcontractorbill/billnumberexistance') ?>",
            data: {
                "bill_number": bill_number,
            },
            type: "POST",
            dataType: "json",
            success: function (data) {
                if (data > 0) {
                    if (confirm("Bill number already exist.Do you want to continue?")) {
                        $("#Subcontractorbill_bill_number").val(bill_number);
                    } else {
                        $("#Subcontractorbill_bill_number").val('');
                    }
                }
            }
        });
    });

</script>