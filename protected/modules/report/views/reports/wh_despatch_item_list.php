<div class="table-responsive">
    <table cellpadding="10" class="table  list-view " id="fixtable">
        <thead class="entry-table">
            <tr>
                <th>SL No</th>
                <th>Projects</th>
                <th>Despacth No</th>
                <th>Date</th>
                <th>Item Description</th>
                <th>Qty in Nos</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $daterange = '';
            if (!empty($date_from) || !empty($date_to)) {
                if ((isset($date_from) && !empty($date_from)) && ((isset($date_to) && empty($date_to)) || !isset($date_to))) {
                    $daterange .= ' WHERE `created_date` >="' . $date_from . '"';
                } else if ((isset($date_to) && !empty($date_to)) && ((isset($date_from) && empty($date_from)) || !isset($date_from))) {
                    $daterange .= ' WHERE `created_date` <="' . $date_to . '"';
                } else if ((isset($date_to) && !empty($date_to)) && (isset($date_from) && !empty($date_from))) {
                    $daterange .= 'WHERE created_date >="' . $date_from . '" AND created_date <="' . $date_to . '"';
                } else {
                    $daterange .= '';
                }
            }

            $tblpx = Yii::app()->db->tablePrefix;
            $sql = "SELECT * FROM jp_warehousedespatch_items " . $daterange;
            $despatch_items = Yii::app()->db->createCommand($sql)->queryAll();
            $i = 1;
            foreach ($despatch_items as $key => $value) {
                ?>
                <tr>
                    <td>
                        <?php echo $i; ?>
                    </td>
                    <td>
                        <?php
                        $sql = "SELECT p.name FROM jp_projects p, "
                            . " jp_warehousedespatch_items i, jp_warehousedespatch d "
                            . " WHERE i.`warehousedespatch_warehouseid` = d.warehousedespatch_id "
                            . " AND d.warehousedespatch_project = p.pid "
                            . " AND i.item_id=" . $value['item_id'];
                        $project = Yii::app()->db->createCommand($sql)->queryRow();
                        $sql = "SELECT p.name "
                            . " FROM `jp_warehousedespatch` wd "
                            . " LEFT JOIN jp_warehouse w "
                            . " ON wd.warehousedespatch_warehouseid_to = w.warehouse_id "
                            . " LEFT JOIN jp_projects p ON p.pid = w.project_id "
                            . " WHERE wd.warehousedespatch_id =" . $value['warehousedespatch_id'];

                        $wh_project = Yii::app()->db->createCommand($sql)->queryScalar();


                        echo !is_null($project['name']) ? $project['name'] : (!is_null($wh_project) ? $wh_project : "N/A");
                        ?>
                    </td>
                    <td>
                        <?php
                        $warehouse_despatch = warehousedespatch::model()->findByPk($value['warehousedespatch_id']);
                        echo CHtml::link($warehouse_despatch->warehousedespatch_no, 'index.php?r=wh/warehousedespatch/view&despatchid=' . $warehouse_despatch['warehousedespatch_id'], array('class' => 'link', 'target' => '_blank'));
                        ?>
                    </td>
                    <td>
                        <?php echo date("d-m-Y", strtotime($value['created_date'])); ?>
                    </td>

                    <td>
                        <?php
                        $spec_sql = "SELECT id, specification, brand_id, cat_id "
                            . " FROM {$tblpx}specification "
                            . "WHERE id=" . $value['warehousedespatch_itemid'];
                        $specification = Yii::app()->db->createCommand($spec_sql)->queryRow();

                        $brand = '';
                        if ($specification['brand_id'] != NULL) {
                            $sql = "SELECT brand_name FROM {$tblpx}brand "
                                . "WHERE id=" . $specification['brand_id'];
                            $brand_details = Yii::app()->db->createCommand($sql)->queryRow();
                            $brand = '-' . ucwords($brand_details['brand_name']);
                        }
                        $category_name = "";
                        if ($specification['cat_id'] != "") {
                            $category = PurchaseCategory::model()->findByPk($specification['cat_id']);
                            $category_name = $category->category_name;
                        }
                        $item = ucwords($category_name) . $brand . '-' . ucwords($specification['specification']);
                        echo $item;
                        ?>
                    </td>
                    <td>
                        <?php echo $value['warehousedespatch_quantity']; ?>
                    </td>
                </tr>
                <?php
                $i++;
            }
            ?>
        </tbody>
    </table>
</div>