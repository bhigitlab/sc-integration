<?php

/**
 * This is the model class for table "{{inv_list}}".
 *
 * The followings are the available columns in table '{{inv_list}}':
 * @property integer $id
 * @property integer $inv_id
 * @property integer $quantity
 * @property string $unit
 * @property integer $rate
 * @property integer $amount
 * @property string $description
 *
 * The followings are the available model relations:
 * @property Invoice $inv
 */
class InvList extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{inv_list}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('amount, description', 'required'),
			array('inv_id, quantity, rate', 'numerical', 'integerOnly'=>true),
			array('quantity, rate', 'numerical'),
			array('unit', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, inv_id, quantity, sgst, sgst_amount, cgst, cgst_amount, igst, igst_amount, tax_amount, unit, rate, amount, description, created_date,hsn_code', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'inv' => array(self::BELONGS_TO, 'Invoice', 'inv_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'inv_id' => 'Inv',
			'quantity' => 'Quantity',
			'unit' => 'Unit',
			'rate' => 'Rate',
			'amount' => 'Amount',
            'description' => 'Description',
            'hsn_code' =>'HSN Code'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('inv_id',$this->inv_id);
		$criteria->compare('quantity',$this->quantity);
		$criteria->compare('unit',$this->unit,true);
		$criteria->compare('rate',$this->rate);
		$criteria->compare('amount',$this->amount);
		$criteria->compare('sgst',$this->sgst);
		$criteria->compare('sgst_amount',$this->sgst_amount);
		$criteria->compare('cgst',$this->cgst);
		$criteria->compare('cgst_amount',$this->cgst_amount);
		$criteria->compare('igst',$this->igst);
		$criteria->compare('igst_amount',$this->igst_amount);
		$criteria->compare('tax_amount',$this->tax_amount);
		$criteria->compare('description',$this->description,true);
        $criteria->compare('hsn_code',$this->hsn_code,true);
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return InvList the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
