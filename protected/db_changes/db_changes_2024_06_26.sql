CREATE TABLE `jp_crone_delete_log_settings` (`id` INT NOT NULL AUTO_INCREMENT , `no_of_days` INT NULL DEFAULT NULL , `created_at` DATETIME NULL DEFAULT CURRENT_TIMESTAMP , `updated_at` DATETIME NULL DEFAULT CURRENT_TIMESTAMP , PRIMARY KEY (`id`)) ENGINE = InnoDB;
ALTER TABLE `jp_crone_delete_log_settings` ADD `updated_by` INT(11) NULL DEFAULT NULL AFTER `updated_at`;
INSERT INTO `jp_crone_delete_log_settings` (`id`, `no_of_days`, `created_at`, `updated_at`,`updated_by`) VALUES (NULL, '30', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP,1);
