<?php

class CashBalanceHelper {

    public static function bankDeposit($reconcil, $unreconcil, $bank_id, $company_id) {

        $tblpx = Yii::app()->db->tablePrefix;
        $sql = "SELECT SUM(receipt) as total_amount "
                . " FROM {$tblpx}expenses "
                . " WHERE " . $reconcil . " AND payment_type=88 "
                . " AND bank_id=" . $bank_id . " AND type=72 "
                . " AND company_id=" . $company_id;
        $daybook = Yii::app()->db->createCommand($sql)->queryScalar();

        $sql = "SELECT SUM(dailyexpense_receipt) as total_amount "
                . " FROM {$tblpx}dailyexpense "
                . " WHERE (" . $reconcil . " AND dailyexpense_receipt_type=88 AND bank_id=" . $bank_id.")"
                . " OR (" . $unreconcil . "  AND dailyexpense_receipt_type=89 AND bank_id=" . $bank_id.""
                . " AND expensehead_type=5) AND  parent_status='1' "                
                . " AND exp_type=72"
                . " AND company_id=" .  $company_id;                
        $dailyexpense = Yii::app()->db->createCommand($sql)->queryScalar();

        return (array('daybook' => $daybook, 'dailyexpense' => $dailyexpense));
    }

    public static function bankWithdrawal($reconcil, $unreconcil, $bank_id, $company_id) {

        $tblpx = Yii::app()->db->tablePrefix;
        $sql = "SELECT SUM((IFNULL(paid, 0))  - IFNULL(expense_tds, 0)) as total_amount "
                . " FROM {$tblpx}expenses "
                . " WHERE " . $reconcil . " AND expense_type=88 "
                . " AND bank_id=" . $bank_id . " AND type=73  "
                . " AND company_id=" . $company_id;
        $daybook_with = Yii::app()->db->createCommand($sql)->queryScalar();

        $sql = "SELECT SUM(dailyexpense_paidamount) as total_amount "
                . " FROM {$tblpx}dailyexpense "
                . " WHERE ((" . $reconcil . " AND (expense_type=88 )) "
                . " AND dailyexpense_chequeno IS NOT NULL 
                )"
                . " AND parent_status='1' AND bank_id=" . $bank_id
                . " AND exp_type=73 AND company_id=" . $company_id;
               // die($sql);

        $dailyexpense_with = Yii::app()->db->createCommand($sql)->queryScalar();
      //  echo "Amount ".$dailyexpense_with;exit;
        $sql = "SELECT SUM( (IFNULL(amount, 0) + IFNULL(tax_amount, 0))- IFNULL(tds_amount,0) ) "
                . " as total_amount FROM {$tblpx}dailyvendors "
                . " WHERE " . $reconcil . " AND payment_type=88  AND "
                . " bank=" . $bank_id ." AND company_id=" . $company_id;
        $dailyvendors = Yii::app()->db->createCommand($sql)->queryScalar();

        $sql = "SELECT SUM( (IFNULL(amount, 0) + IFNULL(tax_amount, 0))- IFNULL(tds_amount,0) ) "
                . " as total_amount FROM {$tblpx}subcontractor_payment "
                . " WHERE " . $reconcil . " AND payment_type=88  "
                . " AND bank=" . $bank_id." AND company_id=" . $company_id;           
        $subcontractor = Yii::app()->db->createCommand($sql)->queryScalar();

        return (array(
            'daybook_with' => $daybook_with,
            'dailyexpense_with' => $dailyexpense_with,
            'dailyvendors' => $dailyvendors,
            'subcontractor' => $subcontractor
        ));
    }

    public static function cashDeposit($cashbalance_date, $current_date, $company_id) {

        $tblpx = Yii::app()->db->tablePrefix;
        $sql = "SELECT SUM(receipt) as total_amount "
                . " FROM {$tblpx}expenses WHERE date "
                . " BETWEEN '" . $cashbalance_date . "' "
                . " AND '" . $current_date . "'"
                . " AND payment_type=89 AND type=72 "
                . " AND company_id=" . $company_id;

        $daybook_cash = Yii::app()->db->createCommand($sql)->queryScalar();

        $sql = "SELECT SUM(dailyexpense_receipt) as total_amount "
                . " FROM {$tblpx}dailyexpense "
                . " WHERE date BETWEEN '" . $cashbalance_date . "' "
                . " AND '" . $current_date . "'"
                . " AND (dailyexpense_receipt_type=89 "
                . " AND dailyexpense_chequeno IS NULL AND "
                . " reconciliation_status IS NULL AND company_id=" . $company_id.") "
                . " OR (dailyexpense_receipt_type=88 AND "
                . " expensehead_type=4 AND company_id=" . $company_id.") AND exp_type=72 "
                . " AND parent_status='1'";

        $dailyexpense_cash = Yii::app()->db->createCommand($sql)->queryScalar();

        return (array(
            'daybook_cash' => $daybook_cash,
            'dailyexpense_cash' => $dailyexpense_cash
        ));
    }

    public static function cashWithdrawal($cashbalance_date, $current_date, $company_id) {

        $tblpx = Yii::app()->db->tablePrefix;
        $sql = "SELECT SUM((IFNULL(paid, 0))  - IFNULL(expense_tds, 0)) as total_amount "
                . " FROM {$tblpx}expenses "
                . " WHERE date BETWEEN '" . $cashbalance_date . "' "
                . " AND '" . $current_date . "'"
                . " AND (expense_type=89) AND type=73"
                . " AND company_id=" . $company_id;
        $daybook_with_amount = Yii::app()->db->createCommand($sql)->queryScalar();

        $sql = "SELECT SUM(dailyexpense_paidamount) as total_amount "
                . " FROM {$tblpx}dailyexpense "
                . " WHERE date BETWEEN '" . $cashbalance_date . "' "
                . " AND '" . $current_date . "'"
                . " AND (expense_type=89) AND exp_type=73 "
                . " AND parent_status='1' "
                . " AND dailyexpense_chequeno IS NULL "
                . " AND reconciliation_status IS NULL "
                . " AND company_id=" . $company_id;
        $dailyexpense_with_amount = Yii::app()->db->createCommand($sql)->queryScalar();

        $sql = "SELECT SUM( (IFNULL(amount, 0) + IFNULL(tax_amount, 0))- IFNULL(tds_amount,0) ) "
                . " as total_amount FROM {$tblpx}dailyvendors"
                . " WHERE date BETWEEN '" . $cashbalance_date . "'"
                . " AND '" . $current_date . "'"
                . " AND payment_type=89"
                . " AND company_id=" . $company_id;
        $dailyvendors_cash = Yii::app()->db->createCommand($sql)->queryScalar();

        $sql = "SELECT SUM( (IFNULL(amount, 0) + IFNULL(tax_amount, 0))- IFNULL(tds_amount,0) ) "
                . " as total_amount "
                . " FROM {$tblpx}subcontractor_payment "
                . " WHERE date BETWEEN '" . $cashbalance_date . "' "
                . " AND '" . $current_date . "'"
                . " AND payment_type=89 "
                . " AND company_id=" . $company_id;
        $subcontractor_cash = Yii::app()->db->createCommand($sql)->queryScalar();

        return (array(
            'daybook_with_amount' => $daybook_with_amount,
            'dailyexpense_with_amount' => $dailyexpense_with_amount,
            'dailyvendors_cash' => $dailyvendors_cash,
            'subcontractor_cash' => $subcontractor_cash
        ));
    }

    public static function depositOpening($opening_bank, $bank_id, $company_id) {

        $tblpx = Yii::app()->db->tablePrefix;
        $sql = "SELECT SUM(receipt) as total_amount "
                . " FROM {$tblpx}expenses "
                . " WHERE reconciliation_status = 1 " . $opening_bank
                . " AND payment_type=88 AND bank_id=" . $bank_id
                . " AND type=72";
        $daybook_check_opening = Yii::app()->db->createCommand($sql)->queryScalar();

        $sql = "SELECT SUM(dailyexpense_receipt) as total_amount "
                . " FROM {$tblpx}dailyexpense "
                . " WHERE reconciliation_status = 1 " . $opening_bank
                . " AND dailyexpense_receipt_type=88 AND bank_id=" . $bank_id
                . " AND exp_type=72";
        $dailyexpense_check_opening = Yii::app()->db->createCommand($sql)->queryScalar();

        return (array(
            'daybook_check_opening' => $daybook_check_opening,
            'dailyexpense_check_opening' => $dailyexpense_check_opening
        ));
    }

    public static function withdrawOpening($opening_bank, $bank_id, $company_id) {

        $tblpx = Yii::app()->db->tablePrefix;
        $sql = "SELECT SUM(paid) as total_amount "
                . " FROM {$tblpx}expenses "
                . " WHERE reconciliation_status = 1 " . $opening_bank
                . " AND expense_type=88 AND bank_id=" . $bank_id
                . " AND type=73";
        $daybook_with_amount_opening = Yii::app()->db->createCommand($sql)->queryScalar();

        $sql = "SELECT SUM(dailyexpense_paidamount) as total_amount "
                . " FROM {$tblpx}dailyexpense "
                . " WHERE reconciliation_status = 1 " . $opening_bank
                . " AND expense_type=88 AND bank_id=" . $bank_id
                . " AND exp_type=73";
        $dailyexpense_with_amount_opening = Yii::app()->db->createCommand($sql)->queryScalar();

        $sql = "SELECT SUM( IFNULL(amount, 0) + IFNULL(tax_amount, 0) )"
                . " as total_amount "
                . " FROM {$tblpx}dailyvendors "
                . " WHERE reconciliation_status = 1 " . $opening_bank
                . " AND payment_type=88  AND bank=" . $bank_id;
        $dailyvendors_check_opening = Yii::app()->db->createCommand($sql)->queryScalar();


        return (array(
            'daybook_with_amount_opening' => $daybook_with_amount_opening,
            'dailyexpense_with_amount_opening' => $dailyexpense_with_amount_opening,
            'dailyvendors_check_opening' => $dailyvendors_check_opening
        ));
    }

    public static function cdepositOpening($opening_hand, $company_id) {

        $tblpx = Yii::app()->db->tablePrefix;
        $sql = "SELECT SUM(receipt) as total_amount"
                . " FROM {$tblpx}expenses "
                . " WHERE payment_type=89 AND type=72 " . $opening_hand;
        $daybook_cash_opening = Yii::app()->db->createCommand($sql)->queryScalar();

        $sql = "SELECT SUM(dailyexpense_receipt) as total_amount"
                . " FROM {$tblpx}dailyexpense "
                . " WHERE (dailyexpense_receipt_type=89)"
                . " AND exp_type=72 " . $opening_hand;
        $dailyexpense_cash_opening = Yii::app()->db->createCommand($sql)->queryScalar();

        return (array(
            'daybook_cash_opening' => $daybook_cash_opening,
            'dailyexpense_cash_opening' => $dailyexpense_cash_opening,
        ));
    }

    public static function cwithdrawOpening($opening_hand, $company_id) {

        $tblpx = Yii::app()->db->tablePrefix;
        $sql = "SELECT SUM(paid) as total_amount"
                . " FROM {$tblpx}expenses "
                . " WHERE (expense_type=89) AND type=73 " . $opening_hand;
        $daybook_with_amount_opening = Yii::app()->db->createCommand($sql)->queryScalar();

        $sql = "SELECT SUM(dailyexpense_paidamount) as total_amount "
                . " FROM {$tblpx}dailyexpense"
                . " WHERE (expense_type=89) AND "
                . " exp_type=73 " . $opening_hand;
        $dailyexpense_with_amount_opening = Yii::app()->db->createCommand($sql)->queryScalar();

        $sql = "SELECT SUM( IFNULL(amount, 0) + IFNULL(tax_amount, 0) )"
                . " as total_amount "
                . " FROM {$tblpx}dailyvendors"
                . " WHERE (payment_type=89)" . $opening_hand;
        $dailyvendors_cash_opening = doubleval(Yii::app()->db->createCommand($sql)->queryScalar());

        return (array(
            'daybook_with_amount_opening' => $daybook_with_amount_opening,
            'dailyexpense_with_amount_opening' => $dailyexpense_with_amount_opening,
            'dailyvendors_cash_opening' => $dailyvendors_cash_opening
        ));
    }

}
