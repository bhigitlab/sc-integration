<?php
$this->breadcrumbs=array(
	'Password recovery',
);
?>

<div class="form">

    <?php
    // Verification code is wrong

    $flash_message = array();
    $flash_message = Yii::app()->user->getFlashes();

    $fexist = 0;
    if (array_key_exists('flash_msg', $flash_message)) {
        $fexist = 1;
    }

    foreach ($flash_message as $key => $message) {
        echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
    }

    if ($fexist == 0) {
        ?>    

        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'users-pwdrecovery-form',
            'enableAjaxValidation' => true,
            'clientOptions' => array(
                'validateOnSubmit' => true,
                'validateOnChange' => true,
                'validateOnType' => false,),
                ));
        ?>

        <p class="note">Please enter your email below and we will send you a new password.</p>

        <?php echo $form->errorSummary($model); ?>

        <div class="row">
            <?php echo $form->labelEx($model, 'myemail_or_username'); ?>
            <?php echo $form->textField($model, 'myemail_or_username', array('size' => 50, 'maxlength' => 100)); ?>
        </div>
        <div class="row buttons">
            <?php echo CHtml::submitButton('Submit'); ?>
        </div>

        <?php
        $this->endWidget();
    }
    ?>

</div><!-- form -->