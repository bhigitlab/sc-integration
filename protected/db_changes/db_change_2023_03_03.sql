ALTER TABLE `jp_pre_dailyreport` CHANGE `works_done` `works_done` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL; 
ALTER TABLE `jp_pre_dailyreport` CHANGE `materials_unlade` `materials_unlade` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL; 
ALTER TABLE `jp_pre_dailyreport` CHANGE `extra_work_done` `extra_work_done` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL; 
ALTER TABLE `jp_pre_dailyreport` CHANGE `labour_wage` `labour_wage` FLOAT(20,2) NULL DEFAULT NULL; 
ALTER TABLE `jp_pre_dailyreport` CHANGE `helper_wage` `helper_wage` FLOAT(20,2) NULL DEFAULT NULL; 
ALTER TABLE `jp_pre_dailyreport` CHANGE `approved_by` `approved_by` INT(1) NULL DEFAULT NULL; 
ALTER TABLE `jp_pre_dailyreport` CHANGE `cancelled_at` `cancelled_at` DATETIME NULL DEFAULT NULL; 

UPDATE jp_deletepending INNER JOIN jp_invoice ON invoice_id=deletepending_parentid SET deletepending_status = '0' WHERE delete_approve_status = '1' AND deletepending_table="jp_invoice";

ALTER TABLE `jp_purchase_category` CHANGE `dieno` `dieno` VARCHAR(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;
ALTER TABLE `jp_purchase_category` CHANGE `spec_flag` `spec_flag` ENUM('N','Y') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;
ALTER TABLE `jp_purchase_category` CHANGE `type` `type` ENUM('C','S') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;
ALTER TABLE `jp_purchase_category` CHANGE `length` `length` VARCHAR(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;
ALTER TABLE `jp_purchase_category` CHANGE `filename` `filename` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;
ALTER TABLE `jp_purchase_category` CHANGE `purchase_mastercategory` `purchase_mastercategory` INT(11) NULL DEFAULT NULL;
ALTER TABLE `jp_purchase_category` CHANGE `warehouse_cat_primary_key` `warehouse_cat_primary_key` INT(11) NULL DEFAULT NULL;
 
ALTER TABLE `jp_subcontractor` ADD `adhar_no` VARCHAR(300) NULL DEFAULT NULL AFTER `pan_no`;
ALTER TABLE `jp_subcontractor` ADD `address` TEXT NULL DEFAULT NULL AFTER `adhar_no`;
ALTER TABLE `jp_subcontractor` ADD `account_details` TEXT NULL DEFAULT NULL AFTER `address`;
ALTER TABLE `jp_projects` ADD `project_incharge` INT(11) NULL DEFAULT NULL AFTER `project_totalestimate`;