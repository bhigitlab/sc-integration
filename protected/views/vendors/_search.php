<?php
/* @var $this VendorsController */
/* @var $model Vendors */
/* @var $form CActiveForm */
?>

<div class="page_filter clearfix">
	<?php $form = $this->beginWidget('CActiveForm', array(
		'action' => Yii::app()->createUrl($this->route),
		'method' => 'get',
	)); ?>
	<div class="filter_elem">
		<?php echo $form->textField($model, 'name', array('class' => 'form-control', 'placeholder' => 'Name')); ?>
	</div>
	<div class="filter_elem filter_btns">
		<input class="btn btn-default btn-sm" type="reset" id="clear" value="Clear">
	</div>
	<?php $this->endWidget(); ?>
</div>