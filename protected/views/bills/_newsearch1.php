<?php
/* @var $this BillsController */
/* @var $model Users */
/* @var $form CActiveForm */
?>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.min.js"></script>

<?php $form=$this->beginWidget('CActiveForm', array(
    'action'=>Yii::app()->createUrl($this->route),
    'method'=>'get',
)); ?>
        <div class="row">
            <div class="col-md-3 col-sm-4 col-xs-6">	
                <?php
                $tblpx = Yii::app()->db->tablePrefix;
                $bills_data = Yii::app()->db->createCommand("SELECT * FROM ".$tblpx."bills WHERE company_id=".Yii::app()->user->company_id." AND purchase_id IS NULL ORDER BY bill_id desc")->queryAll();
                ?>
                <select style="width:100%; padding:6px 12px; border-radius:4px; border:1px solid #ccc;" id="Bills_bill_number" class="select_box"  name="Bills[bill_number]">
					<option value="">Select Bill no</option>
					<?php
					if(!empty($bills_data)) {
							foreach($bills_data as $key => $value){	
					?>
					<option value="<?php echo $value['bill_number'] ?>" <?php echo ($value['bill_number'] == $model->bill_number)?'selected':''; ?>><?php echo $value['bill_number']; ?></option>
					<?php } } ?>
				</select>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6">	
<!--                <label>&nbsp;</label>-->
                <div class= "text-left">
                    <?php echo CHtml::submitButton('Go', array("style"=>"padding:6px 12px; border-radius:4px; border:1px solid #ccc;")); ?>
                    <?php echo CHtml::resetButton('Clear', array('style'=>'padding:6px 12px; border-radius:4px; border:1px solid #ccc;', 'onclick' => 'javascript:location.href="'. $this->createUrl('admin').'"')); ?>
                </div>
            </div>
        </div>

<?php $this->endWidget(); ?>			
				
	<script>
	$(document).ready(function() {
		 $(".select_box").select2();
		 
		});
	</script>			
				
