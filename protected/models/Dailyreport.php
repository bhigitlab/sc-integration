<?php

/**
 * This is the model class for table "{{dailyreport}}".
 *
 * The followings are the available columns in table '{{dailyreport}}':
 * @property integer $dr_id
 * @property integer $projectid
 * @property string $date
 * @property string $works_done
 * @property string $materials_unlade
 * @property string $description
 *
 * The followings are the available model relations:
 * @property Projects $project
 */
class Dailyreport extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public $datefrom;
    public $dateto;
    public function tableName()
    {
        return '{{dailyreport}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('projectid, company_id', 'required'),
            array('projectid', 'numerical', 'integerOnly' => true),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('projectid,company_id,description', 'safe'),
            array('dr_id, projectid,company_id,subcontractor_id,expensehead_id,date,amount,labour,wage,wage_rate,helper,helper_labour,lump_sum,created_by,created_date,update_by,updated_date,approve_status,datefrom,dateto,overtime', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'project' => array(self::BELONGS_TO, 'Projects', 'projectid'),
            'company' => array(self::BELONGS_TO, 'Company', 'company_id'),
            'subcontractor' => array(self::BELONGS_TO, 'Subcontractor', 'subcontractor_id'),
            'expensehead' => array(self::BELONGS_TO, 'ExpenseType', 'expensehead_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'dr_id' => 'Dr',
            'projectid' => 'Project',
            'company_id' => 'Company',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search($status = '')
    {
        // @todo Please modify the following code to remove attributes that should not be searched.
        $tblpx = Yii::app()->db->tablePrefix;
        $pms_api_integration_model = ApiSettings::model()->findByPk(1);
        $pms_api_integration = $pms_api_integration_model->api_integration_settings;
        $criteria = new CDbCriteria;
        $user = Users::model()->findByPk(Yii::app()->user->id);
        $criteria->compare('dr_id', $this->dr_id);
        $criteria->compare('projectid', $this->projectid);
        if($pms_api_integration!='1'){
            $criteria->addCondition('company_id IN (' . $user->company_id . ')');
        }
        $criteria->compare('subcontractor_id', $this->subcontractor_id);
        $criteria->compare('expensehead_id', $this->expensehead_id);
        $criteria->compare('date', $this->date);
        $criteria->compare('amount', $this->amount);
        $criteria->compare('labour', $this->labour);
        $criteria->compare('wage', $this->wage);
        $criteria->compare('wage_rate', $this->wage_rate);
        $criteria->compare('helper', $this->helper);
        $criteria->compare('helper_labour', $this->helper_labour);
        $criteria->compare('lump_sum', $this->lump_sum);
        $criteria->compare('created_by', $this->created_by, true);
        $criteria->compare('updated_by', $this->updated_by);
        $criteria->compare('overtime', $this->overtime);
        if ($status == '1') {
            $criteria->addCondition('approve_status = "1"');
        } elseif ($status == '2') {
            $criteria->addCondition('approve_status != "1"');
        }else{
             $criteria->compare('approve_status', $this->approve_status);
        }
        
        // $criteria->compare('datefrom',$this->datefrom);
        //$criteria->compare('dateto',$this->dateto);
        //$criteria->addCondition("company_id = ".Yii::app()->user->company_id);


        if (!empty($this->datefrom) && empty($this->dateto)) {
            $criteria->addCondition("date >= '" . date('Y-m-d', strtotime($this->datefrom)) . "'");  // date is database date column field
        } elseif (!empty($this->dateto) && empty($this->datefrom)) {
            $criteria->addCondition("date <= '" . date('Y-m-d', strtotime($this->dateto)) . "'");
        } elseif (!empty($this->dateto) && !empty($this->datefrom)) {
            $criteria->addCondition("date between '" . date('Y-m-d', strtotime($this->datefrom)) . "' and  '" . date('Y-m-d', strtotime($this->dateto)) . "'");
        }
       
        if($pms_api_integration!=1){
            $criteria->addCondition('company_id IN (' . $user->company_id . ')');
        }
       
        return new CActiveDataProvider($this, array(
            'pagination' => array(
            'pageSize' => 10, // Default page size
        ),
            'criteria' => $criteria, 'sort' => array(

                'defaultOrder' => 'date DESC',
            ),
        ));
    }

    public static function getpayment($subcontractor_id, $company_id, $project_id,$new_query3)
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $query = "";
        if($subcontractor_id !=''){
            $query .=  " AND e.subcontractor_id = " . $subcontractor_id . "";
        }
        if ($project_id != '') {
            $query .= " AND e.projectid =" . $project_id . "";
        }
        
        $sql="SELECT e.projectid FROM " . $tblpx . "dailyreport e
        LEFT JOIN " . $tblpx . "projects p ON e.projectid = p.pid
        LEFT JOIN " . $tblpx . "subcontractor s ON e.subcontractor_id = s.subcontractor_id LEFT JOIN " . $tblpx . "expense_type exp on
        exp.type_id= e.expensehead_id
        WHERE 1=1 " . $query . $new_query3. " group by e.projectid";
        
        $result = Yii::app()->db->createCommand($sql)->queryAll();

        return $result;
    }

    public static function getpayment2($subcontractor_id, $company_id, $project_id,$new_query3)
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $query = "";
        if($subcontractor_id !=''){
            $query .=  " AND subcontractor_id = " . $subcontractor_id . "";
        }
        if ($project_id != '') {
            $query .= " AND project_id =" . $project_id . "";
        }
        $sql="SELECT project_id as projectid FROM " . $tblpx . "subcontractor_payment WHERE 1=1 " . $query .$new_query3. " group by project_id";
        $result = Yii::app()->db->createCommand($sql)->queryAll();
        return $result;
    }

    public static function getadvpayment($subcontractor_id, $company_id, $project_id,$new_query3)
    {   
        $query = "";
        if (empty($subcontractor_id)) {
        return ['total_adv' => 0];
        }
        
        if ($subcontractor_id != '') {
            $query .= " subcontractor_id = " . (int)$subcontractor_id . " AND ";
        }
        $tblpx = Yii::app()->db->tablePrefix;
        $sql = "SELECT SUM(paidamount) as total_adv 
                FROM " . $tblpx . "subcontractor_payment
                WHERE " . $query. " company_id = " . (int)$company_id . " 
                AND project_id = " . (int)$project_id . " 
                AND " . $tblpx . "subcontractor_payment.payment_quotation_status = 2  ".$new_query3 ."
                GROUP BY project_id, subcontractor_id, company_id";

        $result = Yii::app()->db->createCommand($sql)->queryRow();
      //die($sql);
        return $result;
    }

    public static function getadvpaymentindetailfun($subcontractor_id, $company_id, $project_id,$new_query3)
    {   
        $query = "";
        if ($subcontractor_id != '') {
            $query .= " subcontractor_id = " . (int)$subcontractor_id . " AND ";
        }
        $tblpx = Yii::app()->db->tablePrefix;
        $sql="SELECT * FROM " . $tblpx . "subcontractor_payment
        WHERE ".$query ."company_id = " . (int)$company_id . " AND project_id = " . (int)$project_id . " AND " . $tblpx . "subcontractor_payment.payment_quotation_status =2 ".$new_query3 ." order by date desc";
       //die($sql);
        $result = Yii::app()->db->createCommand($sql)->queryAll();
       
        return $result;
        
    }

    public static function getunapproveexpense($subcontractor_id, $company_id, $project_id,$new_query3)
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $query = "";
        
        if($subcontractor_id !=''){
            $query .=  " AND e.subcontractor_id = " . $subcontractor_id . "";
        }
        if ($project_id != '') {
            $query .= " AND e.projectid =" . $project_id . "";
        }
        $sql="SELECT e.*, p.name as pname,s.subcontractor_name as subname,exp.type_name,sum(e.amount) as total FROM " . $tblpx . "dailyreport e
        LEFT JOIN " . $tblpx . "projects p ON e.projectid = p.pid
        LEFT JOIN " . $tblpx . "subcontractor s ON e.subcontractor_id = s.subcontractor_id LEFT JOIN " . $tblpx . "expense_type exp on
        exp.type_id= e.expensehead_id
        WHERE 1=1 " . $query . " AND e.approve_status NOT IN (1, 3) ".$new_query3." group by e.projectid";
        
        $result = Yii::app()->db->createCommand($sql)->queryRow();
        return $result;
    }


    public static function getapproveexpense($subcontractor_id, $company_id, $project_id,$new_query3)
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $query = "";
        if($subcontractor_id !=''){
            $query .=  " AND e.subcontractor_id = " . $subcontractor_id . "";
        }
        if ($project_id != '') {
            $query .= " AND e.projectid =" . $project_id . "";
        }
        if(!empty($subcontractor_id)){
            $sql="SELECT e.*, p.name as pname,s.subcontractor_name as subname,exp.type_name,sum(e.amount) as total FROM " . $tblpx . "dailyreport e
        LEFT JOIN " . $tblpx . "projects p ON e.projectid = p.pid
        LEFT JOIN " . $tblpx . "subcontractor s ON e.subcontractor_id = s.subcontractor_id LEFT JOIN " . $tblpx . "expense_type exp on
        exp.type_id= e.expensehead_id
        WHERE 1=1 " . $query . " AND e.approve_status IN (1,3) ".$new_query3." group by e.projectid";
        $result = Yii::app()->db->createCommand($sql)->queryRow();

        }else{
            $sql="SELECT e.*, p.name as pname,s.subcontractor_name as subname,exp.type_name,sum(e.amount) as total FROM " . $tblpx . "dailyreport e
        LEFT JOIN " . $tblpx . "projects p ON e.projectid = p.pid
        LEFT JOIN " . $tblpx . "subcontractor s ON e.subcontractor_id = s.subcontractor_id LEFT JOIN " . $tblpx . "expense_type exp on
        exp.type_id= e.expensehead_id
        WHERE 1=1 " . $query . " AND (e.subcontractor_id = '-1' OR e.subcontractor_id IS NULL) AND e.approve_status IN (1,3) ".$new_query3." group by e.projectid";
        $result = Yii::app()->db->createCommand($sql)->queryRow();
            
        }
        
        return $result;
    }
    public static function getRegapproveexpense($subcontractor_id, $company_id, $project_id,$new_query3)
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $query = "";
        if($subcontractor_id !=''){
            $query .=  " AND e.subcontractor_id = " . $subcontractor_id . "";
        }
        if ($project_id != '') {
            $query .= " AND e.projectid =" . $project_id . "";
        }
        if(!empty($subcontractor_id)){
                $sql="SELECT e.*, p.name as pname,s.subcontractor_name as subname,exp.type_name,sum(e.amount) as total FROM " . $tblpx . "dailyreport e
            LEFT JOIN " . $tblpx . "projects p ON e.projectid = p.pid
            LEFT JOIN " . $tblpx . "subcontractor s ON e.subcontractor_id = s.subcontractor_id LEFT JOIN " . $tblpx . "expense_type exp on
            exp.type_id= e.expensehead_id
            WHERE 1=1 " . $query . " AND e.overtime =0 AND e.approve_status IN (1,3) ".$new_query3." group by e.projectid";
            $result = Yii::app()->db->createCommand($sql)->queryRow();
        }else{
            $sql="SELECT e.*, p.name as pname,s.subcontractor_name as subname,exp.type_name,sum(e.amount) as total FROM " . $tblpx . "dailyreport e
            LEFT JOIN " . $tblpx . "projects p ON e.projectid = p.pid
            LEFT JOIN " . $tblpx . "subcontractor s ON e.subcontractor_id = s.subcontractor_id LEFT JOIN " . $tblpx . "expense_type exp on
            exp.type_id= e.expensehead_id
            WHERE 1=1 " . $query . " AND e.overtime =0  AND (e.subcontractor_id = '-1' OR e.subcontractor_id IS NULL) AND e.approve_status IN (1,3) ".$new_query3." group by e.projectid";
            $result = Yii::app()->db->createCommand($sql)->queryRow();

        }
        
        return $result;
    }
    public static function getOTapproveexpense($subcontractor_id, $company_id, $project_id,$new_query3)
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $query = "";
        if($subcontractor_id !=''){
            $query .=  " AND e.subcontractor_id = " . $subcontractor_id . "";
        }
        if ($project_id != '') {
            $query .= " AND e.projectid =" . $project_id . "";
        }
         if(!empty($subcontractor_id)){
            $sql="SELECT e.*, p.name as pname,s.subcontractor_name as subname,exp.type_name,sum(e.amount) as total FROM " . $tblpx . "dailyreport e
            LEFT JOIN " . $tblpx . "projects p ON e.projectid = p.pid
            LEFT JOIN " . $tblpx . "subcontractor s ON e.subcontractor_id = s.subcontractor_id LEFT JOIN " . $tblpx . "expense_type exp on
            exp.type_id= e.expensehead_id
            WHERE 1=1 " . $query . " AND e.overtime =1 AND e.approve_status IN (1,3) ".$new_query3." group by e.projectid";
            $result = Yii::app()->db->createCommand($sql)->queryRow();
         }else{
                $sql="SELECT e.*, p.name as pname,s.subcontractor_name as subname,exp.type_name,sum(e.amount) as total FROM " . $tblpx . "dailyreport e
            LEFT JOIN " . $tblpx . "projects p ON e.projectid = p.pid
            LEFT JOIN " . $tblpx . "subcontractor s ON e.subcontractor_id = s.subcontractor_id LEFT JOIN " . $tblpx . "expense_type exp on
            exp.type_id= e.expensehead_id
            WHERE 1=1 " . $query . " AND e.overtime =1 AND (e.subcontractor_id = '-1' OR e.subcontractor_id IS NULL) AND e.approve_status IN (1,3) ".$new_query3." group by e.projectid";
            $result = Yii::app()->db->createCommand($sql)->queryRow();
         }
        
       // die($sql);
       // echo "<pre>";print_r($result);exit;
        return $result;
    }
    
    public static function getRegunapproveexpense($subcontractor_id, $company_id, $project_id,$new_query3)
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $query = "";
        
        if($subcontractor_id !=''){
            $query .=  " AND e.subcontractor_id = " . $subcontractor_id . "";
        }
        if ($project_id != '') {
            $query .= " AND e.projectid =" . $project_id . "";
        }
         if(!empty($subcontractor_id)){
            $sql="SELECT e.*, p.name as pname,s.subcontractor_name as subname,exp.type_name,sum(e.amount) as total FROM " . $tblpx . "dailyreport e
            LEFT JOIN " . $tblpx . "projects p ON e.projectid = p.pid
            LEFT JOIN " . $tblpx . "subcontractor s ON e.subcontractor_id = s.subcontractor_id LEFT JOIN " . $tblpx . "expense_type exp on
            exp.type_id= e.expensehead_id
            WHERE 1=1 " . $query . " AND e.overtime =0 AND e.approve_status NOT IN (1, 3) ".$new_query3." group by e.projectid";
        
            $result = Yii::app()->db->createCommand($sql)->queryRow();
         }else{
            $sql="SELECT e.*, p.name as pname,s.subcontractor_name as subname,exp.type_name,sum(e.amount) as total FROM " . $tblpx . "dailyreport e
            LEFT JOIN " . $tblpx . "projects p ON e.projectid = p.pid
            LEFT JOIN " . $tblpx . "subcontractor s ON e.subcontractor_id = s.subcontractor_id LEFT JOIN " . $tblpx . "expense_type exp on
            exp.type_id= e.expensehead_id
            WHERE 1=1 " . $query . " AND e.overtime =0 AND (e.subcontractor_id = '-1' OR e.subcontractor_id IS NULL) AND  e.approve_status NOT IN (1, 3) ".$new_query3." group by e.projectid";
            
            $result = Yii::app()->db->createCommand($sql)->queryRow();

         }
        
        return $result;
    }
    public static function getOTunapproveexpense($subcontractor_id, $company_id, $project_id,$new_query3)
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $query = "";
        
        if($subcontractor_id !=''){
            $query .=  " AND e.subcontractor_id = " . $subcontractor_id . "";
        }
        if ($project_id != '') {
            $query .= " AND e.projectid =" . $project_id . "";
        }
         if(!empty($subcontractor_id)){
            $sql="SELECT e.*, p.name as pname,s.subcontractor_name as subname,exp.type_name,sum(e.amount) as total FROM " . $tblpx . "dailyreport e
            LEFT JOIN " . $tblpx . "projects p ON e.projectid = p.pid
            LEFT JOIN " . $tblpx . "subcontractor s ON e.subcontractor_id = s.subcontractor_id LEFT JOIN " . $tblpx . "expense_type exp on
            exp.type_id= e.expensehead_id
            WHERE 1=1 " . $query . " AND e.overtime =1 AND e.approve_status NOT IN (1, 3) ".$new_query3." group by e.projectid";
            
            $result = Yii::app()->db->createCommand($sql)->queryRow();
         }else{
            $sql="SELECT e.*, p.name as pname,s.subcontractor_name as subname,exp.type_name,sum(e.amount) as total FROM " . $tblpx . "dailyreport e
            LEFT JOIN " . $tblpx . "projects p ON e.projectid = p.pid
            LEFT JOIN " . $tblpx . "subcontractor s ON e.subcontractor_id = s.subcontractor_id LEFT JOIN " . $tblpx . "expense_type exp on
            exp.type_id= e.expensehead_id
            WHERE 1=1 " . $query . " AND e.overtime =1 AND (e.subcontractor_id = '-1' OR e.subcontractor_id IS NULL) AND  e.approve_status NOT IN (1, 3) ".$new_query3." group by e.projectid";
            
            $result = Yii::app()->db->createCommand($sql)->queryRow();

         }
        
        return $result;
    }



    public static function getapproveexpense_labourcount($subcontractor_id, $company_id, $project_id,$new_query3){
         $dr_id_arr=array();
        $tblpx = Yii::app()->db->tablePrefix;
        $query = "";
        if($subcontractor_id !=''){
            $query .=  " AND e.subcontractor_id = " . $subcontractor_id . "";
        }
        if ($project_id != '') {
            $query .= " AND e.projectid =" . $project_id . "";
        }
         if(!empty($subcontractor_id)){
            $sql="SELECT e.*, p.name as pname,s.subcontractor_name as subname,exp.type_name FROM " . $tblpx . "dailyreport e
            LEFT JOIN " . $tblpx . "projects p ON e.projectid = p.pid
            LEFT JOIN " . $tblpx . "subcontractor s ON e.subcontractor_id = s.subcontractor_id LEFT JOIN " . $tblpx . "expense_type exp on
            exp.type_id= e.expensehead_id
            WHERE 1=1 " . $query . " AND e.overtime=0 AND e.approve_status IN (1 OR 3)".$new_query3;
            $result = Yii::app()->db->createCommand($sql)->queryAll();
         }else{
            $sql="SELECT e.*, p.name as pname,s.subcontractor_name as subname,exp.type_name FROM " . $tblpx . "dailyreport e
            LEFT JOIN " . $tblpx . "projects p ON e.projectid = p.pid
            LEFT JOIN " . $tblpx . "subcontractor s ON e.subcontractor_id = s.subcontractor_id LEFT JOIN " . $tblpx . "expense_type exp on
            exp.type_id= e.expensehead_id
            WHERE 1=1 " . $query . " AND e.overtime=0   AND (e.subcontractor_id = '-1' OR e.subcontractor_id IS NULL) AND e.approve_status IN (1 OR 3)".$new_query3;
            $result = Yii::app()->db->createCommand($sql)->queryAll();

         }
        
       // die($sql);
        // echo "<pre>";print_r($result);exit;
        if(!empty($result)){
            $dr_id_arr = array_column($result,'dr_id');
           // echo "<pre>";print_r($dr_id_arr);exit;
        }
        $dr_ids = implode(',',$dr_id_arr);
        $labour_count_sql='';
        $labours='';
        if(!empty( $dr_ids)){
            $labour_count_sql="SELECT 
            CASE 
                WHEN l.short_keyword IS NULL OR l.short_keyword = '' THEN l.worktype 
                ELSE l.short_keyword 
            END AS labour, 
            dr.labour_id,
            ROUND(SUM(dr.labour_count), 1) AS total_labour_count,
            CONCAT(
            CASE 
                WHEN l.short_keyword IS NULL OR l.short_keyword = '' THEN l.worktype 
                ELSE l.short_keyword 
            END,'-',ROUND(SUM(dr.labour_count), 1)
            ) AS labour_with_count
        FROM 
            jp_daily_report_labour_details dr
        LEFT JOIN 
            jp_labour_worktype l 
        ON 
            l.type_id = dr.labour_id
        WHERE 
            dr.daily_report_id IN (". $dr_ids.")
        GROUP BY 
            dr.labour_id
        HAVING 
        total_labour_count > 0.0;";
        
        }
            if(!empty($labour_count_sql)){
                 $labours = Yii::app()->db->createCommand($labour_count_sql)->queryAll(); 
                 
            }
            
      return $labours;
        
    }
     public static function getOTapproveexpense_labourcount($subcontractor_id, $company_id, $project_id,$new_query3){
         $dr_id_arr=array();
        $tblpx = Yii::app()->db->tablePrefix;
        $query = "";
        if($subcontractor_id !=''){
            $query .=  " AND e.subcontractor_id = " . $subcontractor_id . "";
        }
        if ($project_id != '') {
            $query .= " AND e.projectid =" . $project_id . "";
        }
         if(!empty($subcontractor_id)){
            $sql="SELECT e.*, p.name as pname,s.subcontractor_name as subname,exp.type_name FROM " . $tblpx . "dailyreport e
            LEFT JOIN " . $tblpx . "projects p ON e.projectid = p.pid
            LEFT JOIN " . $tblpx . "subcontractor s ON e.subcontractor_id = s.subcontractor_id LEFT JOIN " . $tblpx . "expense_type exp on
            exp.type_id= e.expensehead_id
            WHERE 1=1 " . $query . " AND e.overtime=1 AND e.approve_status IN (1 OR 3) " .$new_query3;
            $result = Yii::app()->db->createCommand($sql)->queryAll();
           
         }else{
            $sql="SELECT e.*, p.name as pname,s.subcontractor_name as subname,exp.type_name FROM " . $tblpx . "dailyreport e
            LEFT JOIN " . $tblpx . "projects p ON e.projectid = p.pid
            LEFT JOIN " . $tblpx . "subcontractor s ON e.subcontractor_id = s.subcontractor_id LEFT JOIN " . $tblpx . "expense_type exp on
            exp.type_id= e.expensehead_id
            WHERE 1=1 " . $query . " AND e.overtime=1 AND (e.subcontractor_id = '-1' OR e.subcontractor_id IS NULL) AND  e.approve_status IN (1 OR 3) " .$new_query3;
            $result = Yii::app()->db->createCommand($sql)->queryAll();
         }
       // die($sql);
        // echo "<pre>";print_r($result);exit;
        if(!empty($result)){
            $dr_id_arr = array_column($result,'dr_id');
           // echo "<pre>";print_r($dr_id_arr);exit;
        }
        $dr_ids = implode(',',$dr_id_arr);
        $labour_count_sql='';
        $labours='';
        if(!empty( $dr_ids)){
            $labour_count_sql="SELECT 
            CASE 
                WHEN l.short_keyword IS NULL OR l.short_keyword = '' THEN l.worktype 
                ELSE l.short_keyword 
            END AS labour, 
            dr.labour_id,
            ROUND(SUM(dr.labour_count), 1) AS total_labour_count,
            CONCAT(
            CASE 
                WHEN l.short_keyword IS NULL OR l.short_keyword = '' THEN l.worktype 
                ELSE l.short_keyword 
            END,'-',ROUND(SUM(dr.labour_count), 1)
            ) AS labour_with_count
        FROM 
            jp_daily_report_labour_details dr
        LEFT JOIN 
            jp_labour_worktype l 
        ON 
            l.type_id = dr.labour_id
        WHERE 
            dr.daily_report_id IN (". $dr_ids.")
        GROUP BY 
            dr.labour_id
        HAVING 
        total_labour_count > 0.0  ;";
       
        }
            if(!empty($labour_count_sql)){
                 $labours = Yii::app()->db->createCommand($labour_count_sql)->queryAll(); 
                 
            }
            
      return $labours;
        
    }
    public static function getunapproveexpense_labourcount($subcontractor_id, $company_id, $project_id,$new_query3)
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $query = "";
        $dr_ids=array();
        $labours='';
        if($subcontractor_id !=''){
            $query .=  " AND e.subcontractor_id = " . $subcontractor_id . "";
        }
        if ($project_id != '') {
            $query .= " AND e.projectid =" . $project_id . "";
        }
        if(!empty($subcontractor_id)){
        $sql="SELECT e.*, p.name as pname,s.subcontractor_name as subname,exp.type_name FROM " . $tblpx . "dailyreport e
        LEFT JOIN " . $tblpx . "projects p ON e.projectid = p.pid
        LEFT JOIN " . $tblpx . "subcontractor s ON e.subcontractor_id = s.subcontractor_id LEFT JOIN " . $tblpx . "expense_type exp on
        exp.type_id= e.expensehead_id
        WHERE 1=1 " . $query . " AND   e.overtime=0 AND  e.approve_status NOT IN (1, 3) " .$new_query3;
        }else{
            $sql="SELECT e.*, p.name as pname,s.subcontractor_name as subname,exp.type_name FROM " . $tblpx . "dailyreport e
        LEFT JOIN " . $tblpx . "projects p ON e.projectid = p.pid
        LEFT JOIN " . $tblpx . "subcontractor s ON e.subcontractor_id = s.subcontractor_id LEFT JOIN " . $tblpx . "expense_type exp on
        exp.type_id= e.expensehead_id
        WHERE 1=1 " . $query . " AND   e.overtime=0  AND (e.subcontractor_id = '-1' OR e.subcontractor_id IS NULL) AND  e.approve_status NOT IN (1, 3) " .$new_query3;
        }
        
        $result = Yii::app()->db->createCommand($sql)->queryAll();
        $dr_id_arr=array();
        if(!empty($result)){
            $dr_id_arr = array_column($result,'dr_id');
           // echo "<pre>";print_r($dr_id_arr);exit;
        }
        $dr_ids = implode(',',$dr_id_arr);
        //die($dr_ids);
         $labour_count_sql='';
         $labours ='';
        if(!empty($dr_ids)){
            $labour_count_sql="SELECT 
            CASE 
                WHEN l.short_keyword IS NULL OR l.short_keyword = '' THEN l.worktype 
                ELSE l.short_keyword 
            END AS labour, 
            dr.labour_id,
            ROUND(SUM(dr.labour_count), 1) AS total_labour_count,
            CONCAT(
            CASE 
                WHEN l.short_keyword IS NULL OR l.short_keyword = '' THEN l.worktype 
                ELSE l.short_keyword 
            END,'-',ROUND(SUM(dr.labour_count), 1)
            ) AS labour_with_count
        FROM 
            jp_daily_report_labour_details dr
        LEFT JOIN 
            jp_labour_worktype l 
        ON 
            l.type_id = dr.labour_id
        WHERE 
            dr.daily_report_id IN (". $dr_ids.")
        GROUP BY 
            dr.labour_id
        HAVING 
        total_labour_count > 0.0;
            ;";
           // die($labour_count_sql);
            $labours = Yii::app()->db->createCommand($labour_count_sql)->queryAll(); 
      
        }
        
        return $labours;
    }
    public static function getOTunapproveexpense_labourcount($subcontractor_id, $company_id, $project_id,$new_query3)
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $query = "";
        $dr_ids=array();
        $labours='';
        if($subcontractor_id !=''){
            $query .=  " AND e.subcontractor_id = " . $subcontractor_id . "";
        }
        if ($project_id != '') {
            $query .= " AND e.projectid =" . $project_id . "";
        }
        if(!empty($subcontractor_id)){
        $sql="SELECT e.*, p.name as pname,s.subcontractor_name as subname,exp.type_name FROM " . $tblpx . "dailyreport e
        LEFT JOIN " . $tblpx . "projects p ON e.projectid = p.pid
        LEFT JOIN " . $tblpx . "subcontractor s ON e.subcontractor_id = s.subcontractor_id LEFT JOIN " . $tblpx . "expense_type exp on
        exp.type_id= e.expensehead_id
        WHERE 1=1 " . $query . " AND   e.overtime=1 AND  e.approve_status NOT IN (1, 3) ".$new_query3;
        }else{
            $sql="SELECT e.*, p.name as pname,s.subcontractor_name as subname,exp.type_name FROM " . $tblpx . "dailyreport e
        LEFT JOIN " . $tblpx . "projects p ON e.projectid = p.pid
        LEFT JOIN " . $tblpx . "subcontractor s ON e.subcontractor_id = s.subcontractor_id LEFT JOIN " . $tblpx . "expense_type exp on
        exp.type_id= e.expensehead_id
        WHERE 1=1 " . $query . " AND   e.overtime=1  AND (e.subcontractor_id = '-1' OR e.subcontractor_id IS NULL) AND  e.approve_status NOT IN (1, 3) ".$new_query3;
        }
        
        $result = Yii::app()->db->createCommand($sql)->queryAll();
        $dr_id_arr=array();
        if(!empty($result)){
            $dr_id_arr = array_column($result,'dr_id');
           // echo "<pre>";print_r($dr_id_arr);exit;
        }
        $dr_ids = implode(',',$dr_id_arr);
        //die($dr_ids);
         $labour_count_sql='';
         $labours ='';
        if(!empty($dr_ids)){
            $labour_count_sql="SELECT 
            CASE 
                WHEN l.short_keyword IS NULL OR l.short_keyword = '' THEN l.worktype 
                ELSE l.short_keyword 
            END AS labour, 
            dr.labour_id,
            ROUND(SUM(dr.labour_count), 1) AS total_labour_count,
            CONCAT(
            CASE 
                WHEN l.short_keyword IS NULL OR l.short_keyword = '' THEN l.worktype 
                ELSE l.short_keyword 
            END,'-',ROUND(SUM(dr.labour_count), 1)
            ) AS labour_with_count
        FROM 
            jp_daily_report_labour_details dr
        LEFT JOIN 
            jp_labour_worktype l 
        ON 
            l.type_id = dr.labour_id
        WHERE 
            dr.daily_report_id IN (". $dr_ids.")
        GROUP BY 
            dr.labour_id;";
           // die($labour_count_sql);
            $labours = Yii::app()->db->createCommand($labour_count_sql)->queryAll(); 
      
        }
        
        return $labours;
    }



    public static function getdailyreportDetails($subcontractor_id, $expensehead_id, $projectid, $datefrom, $dateto)
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $new_query = '';
        if (!empty($datefrom) && empty($dateto)) {
            $new_query .= " AND e.date >= '" . date('Y-m-d', strtotime($datefrom)) . "'";
        } elseif (!empty($dateto) && empty($datefrom)) {
            $new_query .= " AND e.date <= '" . date('Y-m-d', strtotime($dateto)) . "'";
        } elseif (!empty($dateto) && !empty($datefrom)) {
            $new_query .= " AND e.date between '" . date('Y-m-d', strtotime($datefrom)) . "' and  '" . date('Y-m-d', strtotime($dateto)) . "'";
        }
        $result = Yii::app()->db->createCommand("SELECT e.*, p.name as pname,s.subcontractor_name as subname,exp.type_name FROM " . $tblpx . "dailyreport e
            LEFT JOIN " . $tblpx . "projects p ON e.projectid = p.pid
            LEFT JOIN " . $tblpx . "subcontractor s ON e.subcontractor_id = s.subcontractor_id LEFT JOIN " . $tblpx . "expense_type exp on
            exp.type_id= e.expensehead_id
            WHERE 1=1 AND e.expensehead_id = " . $expensehead_id . " AND e.subcontractor_id =" . $subcontractor_id . " AND e.projectid = " . $projectid . " " . $new_query . " AND e.approve_status =1 ")->queryAll();
        return $result;
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Dailyreport the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function getLabourExpenses($project_id,$recon_status)
    {
        $data = array();
        $lists = Dailyreport::model()->findAllByAttributes(array('projectid' => $project_id,'approval_status'=>$recon_status,'approve_status'=>1));
        foreach ($lists as $list) {
            $amount = 0;
            $expense_model = ExpenseType::model()->findByPk($list['expensehead_id']);
            $subcontractor = Subcontractor::model()->findByPk($list['subcontractor_id']);
            $values = $list->attributes;
            $values['sc_name'] = $subcontractor['subcontractor_name'];
            if (!array_key_exists($list['expensehead_id'], $data)) {
                $data[$list['expensehead_id']][] = $values;
                $data[$list['expensehead_id']]['expense_head'] = $expense_model['type_name'];
                $data[$list['expensehead_id']]['total_amount'] = $amount;
            } else {
                array_push($data[$list['expensehead_id']], $values);
            }
            $data[$list['expensehead_id']]['total_amount'] += $list['amount'];
        }
        // echo '<pre>';
        // print_r($data);
        // exit;
        return $data;
    }
    public function getNoSubLabourExpenses($project_id,$recon_status)
    {
        $data = array();
        $lists = Dailyreport::model()->findAll(array(
            'condition' => 'projectid = :project_id 
                            AND approval_status = :recon_status 
                            AND approve_status = 1 
                            AND (subcontractor_id IS NULL OR subcontractor_id = -1)',
            'params' => array(
                ':project_id' => $project_id,
                ':recon_status' => $recon_status,
            ),
        ));

        foreach ($lists as $list) {
            $amount = 0;
            $expense_model = ExpenseType::model()->findByPk($list['expensehead_id']);
            $subcontractor = Subcontractor::model()->findByPk($list['subcontractor_id']);
            $values = $list->attributes;
            $values['sc_name'] = isset($subcontractor['subcontractor_name'])? $subcontractor['subcontractor_name']:'No Subcontractor';
            if (!array_key_exists($list['expensehead_id'], $data)) {
                $data[$list['expensehead_id']][] = $values;
                $data[$list['expensehead_id']]['expense_head'] = $expense_model['type_name'];
                $data[$list['expensehead_id']]['total_amount'] = $amount;
            } else {
                array_push($data[$list['expensehead_id']], $values);
            }
            $data[$list['expensehead_id']]['total_amount'] += $list['amount'];
        }
        // echo '<pre>';
        // print_r($data);
        // exit;
        return $data;
    }
}
