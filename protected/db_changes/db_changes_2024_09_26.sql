CREATE TABLE jp_consumption_request ( id INT AUTO_INCREMENT PRIMARY KEY, coms_project_id INT NULL, warehouse_id INT NULL, wpr_id INT NULL, task_name VARCHAR(255) NULL, pms_project_id INT NULL, created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP, updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP );

ALTER TABLE `pms_acc_wpr_item_consumed` ADD `consumption_id` INT NULL DEFAULT NULL AFTER `id`;

ALTER TABLE `jp_consumption_request` ADD `status` INT NOT NULL AFTER `updated_at`;
ALTER TABLE `jp_consumption_request` ADD `item_added_by` INT NULL AFTER `status`, ADD `type_from` INT NULL AFTER `item_added_by`;

ALTER TABLE `jp_consumption_request` CHANGE `status` `status` INT(11) NOT NULL DEFAULT '0';

ALTER TABLE `pms_acc_wpr_item_consumed` ADD CONSTRAINT `fk_consumption_id` FOREIGN KEY (`consumption_id`) REFERENCES `jp_consumption_request`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `pms_acc_wpr_item_consumed` CHANGE `item_added_by` `item_added_by` INT(11) NULL;

ALTER TABLE `pms_acc_wpr_item_consumed` CHANGE `wpr_id` `wpr_id` INT(11) NULL;