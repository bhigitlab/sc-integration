<?php
if (!isset($_GET['exportpdf'])) {
    ?>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.min.js"></script>

    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/message/css/jquery.message.css">
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/message/js/jquery.message.js"></script>
    <style>
        .lefttdiv {
            float: left;
        }

        .purchase-title {
            border-bottom: 1px solid #ddd;
        }

        .purchase_items h3 {
            font-size: 18px;
            color: #333;
            margin: 0px;
            padding: 0px;
            text-align: left;
            padding-bottom: 10px;
        }

        .purchase_items {
            padding: 15px;
            box-shadow: 0 0 13px 1px rgba(0, 0, 0, 0.25);
        }

        .purchaseitem {
            display: inline-block;
            margin-right: 20px;
        }

        .purchaseitem last-child {
            margin-right: 0px;
        }

        .purchaseitem label {
            font-size: 12px;
        }

        .remark {
            display: none;
        }

        .padding-box {
            padding: 3px 0px;
            min-height: 17px;
            display: inline-block;
        }

        th {
            height: auto;
        }

        .quantity,
        .rate {
            max-width: 80px;
        }

        .text_align {

            text-align: center;
        }


        *:focus {
            border: 1px solid #333;
            box-shadow: 0 0 6px 1px rgba(0, 0, 0, 0.25);
        }

        .text-right {
            text-align: right;
        }

        .table-scroll table {
            border-collapse: collapse !important;
            border: transparent !important;
        }
       
    </style>
    <script>
        $(function () {
            $("#datepicker").datepicker({
                dateFormat: 'dd-mm-yy'
            }).datepicker("setDate", new Date());

        });
    </script>

    <br /><br />

    <div class="container">
        <div class="invoicemaindiv">
            <div class="clearfix purchase-title ">
                <div class="add-btn pull-right">
                    <?php if ((isset(Yii::app()->user->role) && (in_array('/subcontractorbill/create', Yii::app()->user->menuauthlist)))) { ?>
                        <a href="<?php echo $this->createUrl('subcontractorbill/createquotationbill&quotation_id=' . $model->scquotation_id) ?>" class="button btn btn-primary hide">Generate SC Bill</a>
                        <?php
                    }
                    if ((isset(Yii::app()->user->role) && (in_array('/subcontractor/pdfdownload', Yii::app()->user->menuauthlist)))) {
                        echo CHtml::link('Download PDF', Yii::app()->request->requestUri . "&exportpdf=1", array('class' => 'button btn btn-primary pdfbtn'));
                    }
                    ?>

                    <!-- <a href="#" class="button btn btn-primary pdfbtn">Download PDF</a> -->
                </div>
                <h3 class="pull-left">Quotation #<?php echo $model->scquotation_id; ?></h3>
            </div>

            <br>
            <form id="pdfvals1" method="post" action="">
                <input type="hidden" name="purchaseview">
                <div class="row">
                    <div class="col-md-3">

                        <label>COMPANY : </label>
                        <?php echo $company_name; ?>
                    </div>

                    <div class="col-md-3">
                        <label>PROJECT : </label>
                        <?php echo isset($model->project_id) ? $model->projects->name : ''; ?>
                    </div>
                    <div class="col-md-3">
                        <label>SUBCONTRACTOR : </label>
                        <?php echo isset($model->subcontractor_id) ? $model->subcontractor->subcontractor_name : ''; ?>
                    </div>
                    <div class="col-md-3">
                        <label>DATE : </label>
                        <?php echo Yii::app()->Controller->changeDateForamt($model->scquotation_date); ?>
                    </div>
                    <div class="col-md-3">
                        <label>WORK ORDER DATE : </label>
                        <?php echo Yii::app()->Controller->changeDateForamt($model->work_order_date); ?>
                    </div>
                    <div class="col-md-3">
                        <label>SC Quotation No : </label>
                        <?php echo $model->scquotation_no; ?>
                    </div>
                    <div class="col-md-3">
                        <label>Expense Head : </label>
                        <?php echo $model->getExpensehead($model); ?>
                    </div>
                    <div class="col-md-3">
                        <label>Completion Date: </label>
                        <?php echo Yii::app()->Controller->changeDateForamt($model->completion_date); ?>
                    </div>
                    <div class="col-md-3">
                        <label>Description : </label>
                        <?php echo $model->scquotation_decription; ?>
                    </div>
                </div>

                <?php
            } else {
                $company_address = Company::model()->findBypk($model->company_id);
                $template = $this->getActiveTemplate();
                ?>
                <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/pdfstyle.css">
                
                <?php if($template != 'TYPE-6'){ ?>
                <br>
                <h4 class="page_heading mt-3">WORK ORDER</h4>
                <?php } ?>
                <div class="container" style="margin:0px 30px">
                <?php if($template == 'TYPE-6'){ //Kapcher
                    $this->renderPartial('_view_quotation_head',array('model'=>$model));
                } else { ?>
                    <div>Quotation No: <?php echo $model->scquotation_no ?> </div>
                    <table class="details" style="margin-top:5px;">
                        <tr>
                            <td><label>Subcontractor : </label><b><?php echo isset($model->subcontractor_id) ? $model->subcontractor->subcontractor_name : ''; ?></b></td>
                            <td><label>Sign Date : </label><b><?php echo Yii::app()->Controller->changeDateForamt($model->scquotation_date); ?></b></td>
                        </tr>
                        <tr>
                            <td><label>PROJECT : </label><b><?php echo isset($model->project_id) ? $model->projects->name : ''; ?></b></td>
                            <td><label>Completion Date: : </label><b><?php echo Yii::app()->Controller->changeDateForamt($model->completion_date); ?></b></td>
                        </tr>
                        <tr>
                            <td><label>Tel : </label><b><?php echo isset($model->subcontractor_id) ? $model->subcontractor->subcontractor_phone : ''; ?></b></td>
                        </tr>
                    </table>
                    <?php } ?>
                    <h4 class="text-center">Work Details</h4>
                    <br>
                </div>
            <?php }
            ?>

            <?php
            $spacing="margin:0px 30px";
            if (!isset($_GET['exportpdf'])) {
                $spacing="";
                ?>

                <div class="clearfix"></div>
                <br><br>

                <div id="table-scroll" class="table-scroll">
                    <div id="faux-table" class="faux-table" aria="hidden"></div>
                    <div id="table-wrap" class="table-wrap">
                        <div class="table-responsive">
                        <?php } ?>
                        <table border="1" class="table table-first" style="<?php echo $spacing ?>">
                            <thead>
                                <tr bgcolor="#ddd">
                                    <th>Sl.No</th>
                                    <th>Quotation Description</th>
                                    <th>Quantity</th>
                                    <th>Unit</th>
                                    <th>Rate</th>
                                    <th>Amount</th>
                                </tr>
                            </thead>
                            <tbody class="addrow">
                                <?php
                                $i = 1;
                                if (!empty($view_datas)) {
                                    foreach ($view_datas as $view_data) {
                                        $category_details = $view_data['category_details'];
                                        if (!empty($category_details)) {
                                            $category_id = $category_details['id'];
                                            $category_total = ScquotationItems::model()->getCategoryTotal($category_details['sc_quotaion_id'], $category_id);
                                            $datas = $view_data['items_list'];
                                            $sql = 'SELECT count(*) as count FROM `jp_scquotation_items` '
                                                    . ' WHERE `item_category_id` = '.$category_id;
                                            $count = Yii::app()->db->createCommand($sql)->queryScalar();
                                            if(isset($_GET['exportpdf'])){
                                                if($count >0 ){
                                            ?>
                                            <tr class="blue_bg">
                                                <td colspan="4" class="light_bg_grey">
                                                    <?php echo sprintf("%02d", $i) . '. ' . $category_details['main_title']; ?>
                                                </td>
                                                <td colspan="2" class="light_bg_grey text-right">
                                                ₹ <?php echo $category_total ?>
                                                </td>
                                            </tr>
                                                <?php } }else{?>
                                                    <tr class="blue_bg">
                                                        <td colspan="4" class="light_bg_grey">
                                                            <?php echo sprintf("%02d", $i) . '. ' . $category_details['main_title']; ?>
                                                        </td>
                                                        <td colspan="2" class="light_bg_grey text-right">
                                                        ₹ <?php echo $category_total ?>
                                                        </td>
                                                    </tr>
                                            <?php } ?>
                                            <?php if ($category_details['main_description']) { ?>
                                                <tr>
                                                    <td colspan="6">
                                                        <?php echo $category_details['main_description']; ?>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                            <?php
                                            $k = 1;
                                            foreach ($datas as $item) {
                                                ?>
                                                <tr class="">
                                                    <?php if ($item['item_amount'] != 0) { ?>
                                                        <td><?php echo $k; ?></td>
                                                    <?php } ?>

                                                    <?php if ($item['item_amount'] == 0) { ?>
                                                        <td colspan="6"><?php echo $item['item_description']; ?></td>
                                                    <?php } else { ?>
                                                        <td><?php echo $item['item_description']; ?></td>
                                                    <?php } ?>

                                                    <?php
                                                    if ((empty($item['item_quantity'])) && (empty($item['item_rate']))) {
                                                        ?>
                                                        <?php if ($item['item_amount'] != 0) { ?>
                                                            <td colspan="3"><?php echo 'Lumpsum'; ?></td>
                                                        <?php } ?>
                                                    <?php } else { ?>
                                                        <?php if ($item['item_amount'] != 0) { ?>
                                                            <td><?php echo $item['item_quantity']; ?></td>
                                                            <td><?php echo $item['item_unit']; ?></td>
                                                            <td class="text-right">₹ <?php echo $item['item_rate']; ?></td>
                                                        <?php } ?>
                                                    <?php } ?>
                                                    <?php if ($item['item_amount'] != 0) { ?>
                                                        <td class="text-right">₹ <?php echo isset($item['item_amount']) ? Yii::app()->Controller->money_format_inr($item['item_amount'], 2) : '0.00'; ?>
                                                        <?php } ?>
                                                    </td>
                                                </tr>
                                                <?php
                                                if ($item['item_amount'] != 0) {
                                                    $k++;
                                                }
                                            }
                                            
                                            if(isset($_GET['exportpdf'])){
                                                if($count >0){
                                                    $i++;
                                                }
                                            }else{
                                                $i++;
                                            }                                            
                                        }
                                    }
                                    ?>
                                </tbody>
                            </table>
                            <br><br>
                            <table class="table" border="1" style="<?php echo $spacing ?>">
                                <tbody>
                                    <tr class="blue_bg">
                                        <td colspan="6" class="light_bg_grey">
                                            TOTAL SUMMARY
                                        </td>
                                    </tr>
                                    <?php
                                    $i = 1;
                                    foreach ($view_datas as $view_data) {
                                        if (!empty($category_details)) {
                                            $category_details = $view_data['category_details'];
                                            $category_total = ScquotationItems::model()->getCategoryTotal($category_details['sc_quotaion_id'], $category_details['id']);
                                            
                                            $summary = '<tr>
                                                            <td>'. sprintf("%02d", $i) .'</td>
                                                            <td colspan="4">' .
                                                                $category_details['main_title'].'
                                                            </td>
                                                            <td class="text-right">₹ ' . $category_total .'</td>
                                                        </tr>';
                                            
                                            if(isset($_GET['exportpdf'])){
                                                if($category_total >0){
                                                   echo $summary;
                                                   $i++;
                                                }
                                            }else{
                                                echo $summary;
                                                $i++;
                                            }                                                                                         
                                        }
                                    }
                                    ?>

                                    <?php
                                }
                                ?>


                            </tbody>
                            <tfoot>
                                <tr>
                                    <th style="text-align: right;" colspan="5" class="text-right">TOTAL: </th>

                                    <th class="text-right">
                                        <div id="grand_total">₹ <?php echo Yii::app()->Controller->money_format_inr($model->getTotalQuotationAmount($model->scquotation_id), 2); ?></div>
                                    </th>
                                </tr>
                                <tr>
                                    <?php
                                    $gst = $model->sgst_percent + $model->cgst_percent + $model->igst_percent;
                                    $gst_percent_total="";
                                    if($gst >0){
                                        $gst_percent_total = '@'.$gst.'%';
                                    }
                                    ?>
                                    <th style="text-align: right;" colspan="5" class="text-right">GST<?php echo $gst_percent_total ?></th>

                                    <th class="text-right">
                                    ₹ <?php 
                                        $sgst_amount = ($model->sgst_percent / 100) * $model->getTotalQuotationAmount($model->scquotation_id);
                                        $cgst_amount = ($model->cgst_percent / 100) * $model->getTotalQuotationAmount($model->scquotation_id);
                                        $igst_amount = ($model->igst_percent / 100) * $model->getTotalQuotationAmount($model->scquotation_id);
                                    
                                        echo Controller::money_format_inr(($sgst_amount+$cgst_amount+$igst_amount), 2, 1);  
                                    ?>
                                    </th>
                                </tr>
                                <tr>
                                    <th style="text-align: right;" colspan="5" class="text-right">TOTAL: </th>

                                    <th class="text-right">
                                        <div id="grand_total">₹ <?php echo Yii::app()->Controller->money_format_inr(($model->getTotalQuotationAmount($model->scquotation_id) +$sgst_amount+$cgst_amount+$igst_amount), 2); ?></div>
                                    </th>
                                </tr>
                                <?php
                                if (isset($_GET['exportpdf'])) {
                                    if($template != 'TYPE-6'){
                                    ?>
                                    <tr>
                                        <th class="text-center" colspan="6">In words: <?php echo Yii::app()->Controller->displaywords($model->getTotalQuotationAmount($model->scquotation_id)) ?></th>
                                    </tr>
                                <?php } } ?>

                            </tfoot>
                        </table>
                        <br>
                        <?php
                        if (!empty($payment_entries)) {
                            ?>
                            <table class="detail-table" style="<?php echo $spacing ?>">
                                <tbody>
                                    <tr bgcolor="#add8e6">
                                        <th colspan="3" class="text-left border-0"> Payment Terms</th>
                                    </tr>
                                    <?php
                                    $p_key = 1;

                                    $payment_entry_total = 0;
                                    foreach ($payment_entries as $payment_entry) {
                                        $payment_entry_total += $payment_entry['amount'];
                                        ?>
                                        <tr>
                                            <td><?php echo $p_key; ?></td>
                                            <td><?php echo $payment_entry['payment_title']; ?></td>
                                            <td class="text-right"><?php echo controller::money_format_inr($payment_entry['amount'], 2); ?></td>
                                        </tr>
                                        <?php
                                        $p_key++;
                                    }
                                    ?>
                                    <tr>
                                        <td colspan="2" class="text-right"><b>Total</b></td>
                                        <td class="text-right"><b><?php echo controller::money_format_inr($payment_entry_total, 2); ?></b></td>
                                    </tr>
                                </tbody>
                            </table>
                        <?php }
                        
                        $payment_stage = Yii::app()->db->createCommand("SELECT * FROM `jp_sc_payment_stage` WHERE `quotation_id` = ".$_GET['scquotation_id'])->queryAll();
                        if (!empty($payment_stage)) {
                            ?>
                        <table border="1" class="table"  style="<?php echo $spacing ?>">
                            <thead>
                                <tr>
                                    <th>Sl Number</th>
                                    <th>Stage</th>
                                    <th>Stage Percentage</th>
                                    <th>Amount</th>
                                    <th>Bill Status</th>    
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $payment_stage = Yii::app()->db->createCommand("SELECT * FROM `jp_sc_payment_stage` WHERE `quotation_id` = ".$_GET['scquotation_id']." ORDER BY `serial_number` ASC ")->queryAll();
                                foreach ($payment_stage as $key => $value) {
                    
                                ?>
                                <tr>
                                     <td><?php echo $value['serial_number'];?></td>
                                    <td><?php echo $value['payment_stage'];?></td>
                                    <td><?php echo number_format($value['stage_percent'],2,'.','');?></td>
                                    <td>
                                        <?php 
                                        $quotation_amount =$model->getTotalQuotationAmount($model->scquotation_id) ;
                                        $stage_amount = Controller::money_format_inr($quotation_amount * ($value['stage_percent'] / 100),2);
                                        echo  $stage_amount;
                                        ?></td>
                                    <td><?php echo ($value['bill_status']==0)?'Pending':'Bill Created';?></td>                
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                        <?php } ?>
                    
                        <?php if (!isset($_GET['exportpdf'])) { ?>
                        </div>

                    </div>
                </div>

            <?php } else { 
                $terms = Yii::app()->db->createCommand("SELECT * FROM `jp_terms_conditions` WHERE `template_id` = 1 AND `type` = '1'")->queryRow();
                if(!empty($terms)){
                ?>
                    <table class=" no-border terms_table"  style="<?php echo $spacing ?>">
                        <tbody>
                            <tr>
                                <th class="title">TERMS AND CONDITIONS</th>
                            </tr>
                            <tr>
                                <td>
                                    <?php                            
                                    echo $terms['terms_and_conditions'];
                                    ?>
                                </td>
                            </tr>
                        </tbody>
                    </table>
             <?php } ?>
            <br><br>
                
                <?php if($template == 'TYPE-6'){ ?>
                    <br>
                    <br><br>
                <table style="<?php echo $spacing ?>">
                    <tr>
                        <td><h5>CHECKED & APPROVED BY</h5> <br><br></td>
                        <td><h5>DATE:</h5> <br><br></td>
                        <td><h5>CONTRACTOR</h5> <br><br></td>
                    </tr>
                </table>
                <?php }else{ ?>
                    <table style="<?php echo $spacing ?>">
                    <tr>
                        <td>Approved BY <br><br>
                            <b>For <?php echo Yii::app()->name ?></b></td>
                        <td>Agreed BY <br><br>
                            <?php echo isset($model->subcontractor_id) ? $model->subcontractor->subcontractor_name : ''; ?>
                        </td>
                    </tr>
                </table>
                <?php } ?>
            <?php } ?>
            <br><br>

            </form>
            
        </div>

        <style>
            .error_message {
                color: red;
            }

            a.pdf_excel {
                background-color: #6a8ec7;
                display: inline-block;
                padding: 8px;
                color: #fff;
                border: 1px solid #6a8ec8;
            }

            .invoicemaindiv th,
            .invoicemaindiv td {
                padding: 10px;
            }

            .light_bg_grey {
                background-color: #ddd !important;
            }

            .table tbody tr td,.table thead tr th,.table tfoot tr th{
                border:1px solid #ddd;
            }
        </style>

    </div>

    <script>
        (function () {
            var mainTable = document.getElementById("main-table");
            if (mainTable !== null) {
                var tableHeight = mainTable.offsetHeight;
                if (tableHeight > 380) {
                    var fauxTable = document.getElementById("faux-table");
                    document.getElementById("table-wrap").className += ' ' + 'fixedON';
                    var clonedElement = mainTable.cloneNode(true);
                    clonedElement.id = "";
                    fauxTable.appendChild(clonedElement);
                }
            }
        })();


        $('.pdfbtn').click(function () {
            $("form#pdfvals1").attr('action', "<?php echo $this->createAbsoluteUrl('subcontractor/SaveInvoice', array('scqid' => $model->scquotation_id)); ?>");
            $("form#pdfvals1").submit();
        });
    </script>
