<?php
/* @var $this SubcontractorController */
/* @var $model Subcontractor */
/* @var $form CActiveForm */
?>

<div class="">

<?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'subcontractor-form',
        'action' =>Yii::app()->createUrl('subcontractor/permissionsave',array('id'=>$id)),
        'enableAjaxValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true,
            'validateOnType' => false,),
    ));
    ?>



    <!-- Popup content-->



    <div class="panel-body">
            <div class="row addRow">
                <div class="col-md-12">	
                    <label>Project :</label>
                    <ul class="checkboxList">
                        <li><input type="checkbox"id='select_alltype' value='0'>Type Name</li>
                        <?php 
                        $typelist = ExpenseType::model()->findAll(array('condition' => 'expense_type IN (98,97) AND company_id='.Yii::app()->user->company_id.''));
                        $assigned_types_array = array();
                        //if (!$model->isNewRecord) {
                             //                      $assigned_types = SubcontractorExptype::model()->findAll();

                            $assigned_types = SubcontractorPermission::model()->findAll(array('condition' => 'subcontractor_id=' . $id));
                            foreach ($assigned_types as $atype) {
                                $assigned_types_array[] = $atype['project_id'];
                            }
                       // }

                        foreach ($sql as $type) {
                           // print_r($type);
                            //exit();
                            ?>
                            <li><input type="checkbox" class="checkboxtype" <?php echo (in_array($type['pid'], $assigned_types_array) ? 'checked="checked"' : ''); ?> name="type[]" value='<?php echo $type['pid'] ?>' /> <?php echo $type['name']; ?></li>
                            <?php
                        }
                        ?>
                    </ul> 
                </div>
            </div>
            <div class="modal-footer save-btnHold text-center">
                <?php  echo CHtml::submitButton('Save'); ?>                            
                <button data-dismiss="modal" onclick="javascript:window.location.reload()">Close</button>
            </div>
        </div>
        <style>
            .tableborder td{
                border:1px solid #c2c2c2;
            }
            .checkboxList input{margin-right: 5px;}
        </style>



        <?php $this->endWidget(); ?>

</div><!-- form -->
<script>
        $(document).ready(function () {

            //select all checkboxes
            $("#select_all").change(function () {  //"select all" change
                var status = this.checked; // "select all" checked status
                $('.checkbox').each(function () { //iterate all listed checkbox items
                    this.checked = status; //change ".checkbox" checked status
                });
            });

            $('.checkbox').change(function () { //".checkbox" change
                //uncheck "select all", if one of the listed checkbox item is unchecked
                if (this.checked == false) { //if this item is unchecked
                    $("#select_all")[0].checked = false; //change "select all" checked status to false
                }

                //check "select all" if all checkbox items are checked
                if ($('.checkbox:checked').length == $('.checkbox').length) {
                    $("#select_all")[0].checked = true; //change "select all" checked status to true
                }
            });
            
            
            $("#select_alltype").change(function () {  //"select all" change
                var status = this.checked; // "select all" checked status
                $('.checkboxtype').each(function () { //iterate all listed checkbox items
                    this.checked = status; //change ".checkbox" checked status
                });
            });
            
            
            $('.checkboxtype').change(function () { //".checkbox" change
                //uncheck "select all", if one of the listed checkbox item is unchecked
                if (this.checked == false) { //if this item is unchecked
                    $("#select_alltype")[0].checked = false; //change "select all" checked status to false
                }

                //check "select all" if all checkbox items are checked
                if ($('.checkboxtype:checked').length == $('.checkboxtype').length) {
                    $("#select_alltype")[0].checked = true; //change "select all" checked status to true
                }
            });


        });


    </script>
