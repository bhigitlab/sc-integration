<?php
$tblpx = Yii::app()->db->tablePrefix;
?>
<div class="modal-dialog modal-md">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" onclick="javascript:window.location.reload()">&times;</button>
            <h4 class="modal-title">Edit Daily Expense</h4>
        </div>
        <?php //echo $this->renderPartial('_form', array('model' => $model)); ?>
        
        
        
<div class="modal-body">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'dailyexpense-form',
        'enableAjaxValidation' => true,
        'enableClientValidation'=>true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true,
            'validateOnType' => false,),
    ));
    ?>
<?php //echo $form->hiddenField($model, 'type'); ?>

<div class="clearfix">
    <div class="row addRow">
        
        <div class="col-md-6">
            <?php echo $form->labelEx($model, 'date'); ?>
            <?php echo CHtml::activeTextField($model, 'date', array("value" =>(($model->isNewRecord) ? date('d-M-Y'): date('d-M-Y',strtotime($model->date))))); ?>
            <?php $this->widget('application.extensions.calendar.SCalendar', array(
            'inputField' => 'Dailyexpense_date',
            'ifFormat' => '%d-%b-%Y'
			)); ?>
            <?php echo $form->error($model, 'date'); ?>
        </div>
        <div class="col-md-6">
			<?php //echo $model->exp_type; ?>
            <?php echo $form->labelEx($model, 'Type'); ?>
            <?php echo $form->dropDownList($model,'exp_type',array("0"=>"Receipt","1"=>"Expense"), array('empty' => '--', 'style' => 'width:250px;',
				
				'ajax' => array(
						'type' => 'POST',
					    'dataType' => 'json',
						'url' => CController::createUrl('dailyexpense/getexpensetypes'),
					    'data' => array('exp_type' => 'js:this.value'),
						 'success' => 'function(data) {
							//alert(data.exptypeslist);
								  $("#Dailyexpense_exp_type_id").html(data.exptypeslist);
							  }'
						)
            
            )); ?>
            <?php echo $form->error($model, 'exp_type'); ?>
        </div>
     </div>
     
     <div class="row addRow">
       <div class="col-md-6">
		    <?php //echo $model->exp_type_id; ?>
            <?php echo $form->labelEx($model, 'Expense Type'); ?>
            <?php //echo $form->textField($model, 'exp_type_id'); ?>
            <?php  echo $form->dropDownList($model, 'exp_type_id', CHtml::listData(Companyexpensetype::model()->findAll(array(
									'condition' => 'type in (select type from '.$tblpx.'company_expense_type where type='.$model->exp_type.' OR type = 2)',
									)), 'company_exp_id', 'name'), array('empty' => '--', 'style' => 'width:250px;',
					
         
            ));  ?>
            <?php echo $form->error($model, 'exp_type_id'); ?>
        </div>
        <div class="col-md-6">
            <?php echo $form->labelEx($model, 'amount'); ?>
            <?php echo $form->textField($model, 'amount'); ?>
            <?php echo $form->error($model, 'amount'); ?>
        </div>
     </div>
	
	<div class="row addRow">
		<div class="col-md-12">
            <?php echo $form->labelEx($model, 'description'); ?>
            <?php echo $form->textField($model, 'description'); ?>
            <?php echo $form->error($model, 'description'); ?>
        </div>
    </div>
     
    
        
    <br>
    <div class="modal-footer save-btnHold">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?> 
        <button data-dismiss="modal" onclick="javascript:window.location.reload()">Close</button>
    </div>

    <?php $this->endWidget(); ?>
</div>
</div><!-- form -->


 </div>
</div>


