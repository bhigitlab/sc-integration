ALTER TABLE `jp_inv_list` ADD `type` ENUM('0','1') NOT NULL DEFAULT '0' COMMENT '0=>lumpsum,1=>qty*rate' AFTER `hsn_code`;
ALTER TABLE `jp_inv_list` ADD `created_by` INT(11)  NULL  DEFAULT NULL AFTER `created_date`;

ALTER TABLE `jp_quotation` CHANGE `inv_no` `inv_no` VARCHAR(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;