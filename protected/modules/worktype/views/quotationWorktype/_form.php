<?php
/* @var $this QuotationCategoryMasterController */
/* @var $model QuotationCategoryMaster */
/* @var $form CActiveForm */
?>
<div class="form">
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'quotation-finish-master-form',
        'enableClientValidation' => true,
        'clientOptions' => array(
            'validateOnChange' => true,
            'validateOnType' => false,
        )
    ));
    ?>
    
    <div class="row">
        <div class="col-md-12">
            <?php echo $form->labelEx($model, 'name'); ?>
            <?php echo $form->textField($model, 'name', array('size' => 50, 'maxlength' => 50, 'class' => 'form-control')); ?>
            <?php echo $form->error($model, 'name'); ?>
        </div>
        <div class="col-md-12">
            <?php
            $id = isset($_GET['id'])?$_GET['id']:''; 
            $style = 'display:block;';
             ?>
            <div style="<?php echo $style;?>">
            <?php echo $form->labelEx($model, 'template');
                echo $form->dropDownList($model, 'template_id', CHtml::listData(WorktypeTemplate::model()->findAll(), 'id', 'name'), array('class' => 'form-control js-example-basic-single field_change invoice_add require', 'style' => 'width:100%'));
            ?>
            </div>
        </div>
        <div class="col-md-12">
            <?php echo $form->labelEx($model, 'status'); ?>
            <?php
            echo CHtml::activeDropDownList($model, 'status', array('1' => 'Active', '0' => 'Inactive'), array('class' => 'form-control')
            );
            ?>
        </div>
        <div class="col-md-6 save-btnHold">
            <label style="display:block;">&nbsp;</label>
            <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-sm btn-info submit')); ?>
            <?php
            if (!$model->isNewRecord) {
                echo CHtml::Button('Close', array('class' => 'btn', 'onclick' => 'javascript:location.href="' . $this->createUrl('index') . '"'));
            } else {
                echo CHtml::ResetButton('Reset', array('style' => 'margin-right:3px;', 'class' => 'btn btn-sm btn-default'));
            }
            ?>
        </div>
    </div>
    <?php $this->endWidget(); ?>
</div>