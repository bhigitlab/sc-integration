
<?php
/* @var $this UserRolesController */
/* @var $model UserRoles */

$this->breadcrumbs=array(
	'User Roles'=>array('index'),
	'Create',
);

?>


<div class="panel panel-gray">
    <div class="panel-heading form-head">
        <h3 class="panel-title">Add User Role</h3>
    </div>
    <?php echo $this->renderPartial('_form', array('model' => $model)); ?>
</div>
