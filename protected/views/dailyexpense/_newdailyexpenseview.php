<?php
/* @var $this ExpensesController */
/* @var $data Expenses */
if ($index == 0) {
	
    ?>

    <thead class="entry-table">
        <tr>
            <th>Sl No.</th>
            <th>Company</th>
            <th>Bill Number</th>
            <th>Transaction Head</th>
            <th>Transaction Type</th>            
            <th>Bank</th>
            <th>Cheque Number</th>
            <th>Description</th>
            <th>Amount</th>
            <th>Tax</th>
            <th>Total</th>
            <th>Receipt</th>
            <th>Paid</th>
            <th>Payment Type</th>
            <th>Paid To</th>
        </tr>   
    </thead>
    <?php
}
?>
    <tr>
        <td><?php echo $index+1; ?></td>
        <td>
        <?php
            $company = Company::model()->findByPk($data->company_id);
            if($company)
                echo $company->name;
        ?>
        </td>
        <td><?php echo $data->bill_id; ?></td>
        <td>
        <?php
        if($data->dailyexpense_type == 'expense') {
            $expense = Companyexpensetype::model()->findByPk($data->expensehead_id);
            if($expense)
                echo "Expense - ".$expense->name;
        } else if($data->dailyexpense_type == 'receipt') {
            $receipt = Companyexpensetype::model()->findByPk($data->dailyexpense_receipt_head);
            if($receipt)
                echo "Receipt - ".$receipt->name;
        } else if($data->dailyexpense_type == 'deposit') {
            $deposit = Deposit::model()->findByPk($data->exp_type_id);
            if($deposit)
                echo "Deposit - ".$deposit->deposit_name;
        }
        ?>
        </td>
        <td>
        <?php
        if($data->dailyexpense_type == 'receipt')
            $exptype = Status::model()->findByPk($data->dailyexpense_receipt_type);
        else
            $exptype = Status::model()->findByPk($data->expense_type);
        if($exptype)
            echo $exptype->caption;
        ?>   
        </td>
        <td>
        <?php
            $bank = Bank::model()->findByPk($data->bank_id);
            if($bank)
                echo $bank->bank_name;
        ?>
        </td>
        <td><?php echo $data->dailyexpense_chequeno; ?></td>
        <td><?php echo $data->description; ?></td>
        <td class="text-right"><?php echo Controller::money_format_inr($data->dailyexpense_amount, 2); ?></td>
        <td class="text-right"><?php echo Controller::money_format_inr($data->dailyexpense_sgst + $data->dailyexpense_cgst + $data->dailyexpense_igst, 2); ?></td>
        <td class="text-right"><?php echo Controller::money_format_inr($data->amount, 2); ?></td>
        <td class="text-right"><?php echo Controller::money_format_inr($data->dailyexpense_receipt, 2); ?></td>
        <td class="text-right"><?php echo Controller::money_format_inr($data->dailyexpense_paidamount, 2); ?></td>
        <td>
        <?php
            if($data->dailyexpense_purchase_type == 1)
                echo "Credit";
            else if($data->dailyexpense_purchase_type == 2)
                echo "Full Paid";
            else if($data->dailyexpense_purchase_type == 3)
                echo "Partially Paid";
        ?>
        </td>
        <td>
        <?php
            $employee = Users::model()->findByPk($data->employee_id);
            if($employee)
                echo $employee->first_name." ".$employee->last_name;
        ?>
        </td>
    </tr>




