<?php

/**
 * This is the model class for table "{{unit_conversion}}".
 *
 * The followings are the available columns in table '{{unit_conversion}}':
 * @property integer $id
 * @property integer $item_id
 * @property integer $base_unit
 * @property double $conversion_factor
 * @property integer $conversion_unit
 * @property string $created_date
 * @property string $priority
 */
class UnitConversion extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{unit_conversion}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('item_id, base_unit, conversion_factor, conversion_unit', 'required'),
			array('item_id, base_unit, conversion_unit', 'length', 'max' => 150),
			array('conversion_factor', 'numerical'),
			array('priority', 'length', 'max' => 1),
			array('created_date', 'safe'),
			//array('item_id','purchaseItems'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, item_id, base_unit, conversion_factor, conversion_unit, created_date, priority', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'ItemId' => array(self::BELONGS_TO, 'PurchaseCategory', 'id'),
			'Unit' => array(self::BELONGS_TO, 'Unit', 'id'),


		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'item_id' => 'Item',
			'base_unit' => 'Base Unit',
			'conversion_factor' => 'Conversion Factor',
			'conversion_unit' => 'Conversion Unit',
			'created_date' => 'Created Date',
			'priority' => 'Priority',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// echo $this->conversion_factor;
		// exit;
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('item_id', $this->item_id);
		$criteria->compare('base_unit', $this->base_unit);
		$criteria->compare('conversion_factor', $this->conversion_factor);
		$criteria->compare('conversion_unit', $this->conversion_unit);
		$criteria->compare('created_date', $this->created_date, true);
		$criteria->compare('priority', $this->priority, true);
		$criteria->order = 'item_id DESC';



		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'pagination' => false,
		));
	}
	public function GetItemName($item_id)
	{
		$tblpx = Yii::app()->db->tablePrefix;
		$sql = "SELECT id, specification, unit, brand_id, cat_id "
			. " FROM {$tblpx}specification "
			. " WHERE id='" . $item_id . "'";
		$item = Yii::app()->db->createCommand($sql)->queryRow();

		if ($item['brand_id'] != NULL) {
			$brand_sql = "SELECT brand_name FROM {$tblpx}brand "
				. " WHERE id=" . $item['brand_id'] . "";
			$brand_details = Yii::app()->db->createCommand($brand_sql)->queryRow();
			$brand = '-' . ucfirst($brand_details['brand_name']);
		} else {
			$brand = '';
		}
		if ($item['unit'] != NULL) {
			$unit = '-' . $item['unit'];
		} else {
			$unit = '';
		}

		if ($item['cat_id'] != '') {
			$result = $this->GetParent($item['cat_id']);

			$item_name = ucfirst($result['category_name']) . $brand . '-' . ucfirst($item['specification']);
		} else {

			$item_name = $brand . '-' . $item['specification'] . ' - ' . $unit . "(unit)";
		}
		return $item_name;
	}
	public function GetItemcount($item_id, $base_unit)
	{
		$tblpx = Yii::app()->db->tablePrefix;
		$conArray  = array('condition' => 'item_id = ' . $item_id . ' AND conversion_unit !="' . $base_unit . '"');
		$item = UnitConversion::model()->findAll($conArray);
		$itemcount = count($item);
		return $itemcount;
	}
	public function GetParent($id)
	{
		$tblpx = Yii::app()->db->tablePrefix;
		$sql = "SELECT parent_id, id , category_name FROM {$tblpx}purchase_category 
		WHERE id=" . $id . " ORDER BY created_date DESC";
		$category = Yii::app()->db->createCommand($sql)->queryRow();

		return $category;
	}
	public function unitname($id)
	{

		$tblpx = Yii::app()->db->tablePrefix;
		$sql = "SELECT * FROM {$tblpx}unit WHERE id='.$id.'";
		$unit = Yii::app()->db->createCommand($sql)->queryRow();
		return $unit['unit_name'];
	}

	/*public function purchaseItems() {        
		if (empty($this->item_id)){
			   $this->addError("item_id", 'Item is Required.');
		 }
		 
	 }*/
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return UnitConversion the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}
}
