<?php
/* @var $this ProjectsController */
/* @var $data Projects */
?>

<?php
$tblpx = Yii::app()->db->tablePrefix;
$id = $data->vendor_id;
$expense_head_content = Vendors::model()->getExpensetypes($data->vendor_id);
$max_length = 150;
$expense_head_hover_content = "";
if (strlen($expense_head_content) > $max_length) {
    $expense_head_hover_content = 'title="' . CHtml::encode($expense_head_content) . '"';
    $expense_head_short_content = substr($expense_head_content, 0, $max_length - 3) . "..."; // Subtract 3 to make space for "..."
} else {
    $expense_head_short_content = $expense_head_content;
}
if ($index == 0) {
    ?>
    <thead class="entry-table">
        <tr>
            <th>Sl No.</th>
            <th>Vendor Name</th>
            <th>Company</th>
            <th>Phone</th>
            <th>Email ID</th>
            <th>Payment Date Range</th>
            <?php if ($sixth_column_width) { ?>
                <th>Expense Head</th>
            <?php } ?>
            <th>Miscellaneous</th>
            <th>Address</th>
            <th>Action</th>
        </tr>
    </thead>
    <?php
}
?>
<tr>
    <td>
        <?php echo $index + 1; ?>
    </td>
    <td>
        <?php echo CHtml::encode($data->name); ?>
    </td>
    <td>
        <?php
        $companyname = array();
        $arrVal = explode(',', $data->company_id);
        foreach ($arrVal as $arr) {
            $value = Company::model()->findByPk($arr);
            array_push($companyname, $value['name']);
        }
        echo implode(', ', $companyname);
        ?>
    </td>
    <td>
        <?php echo CHtml::encode($data->phone); ?>
    </td>
    <td>
        <?php echo CHtml::encode($data->email_id); ?>
    </td>
    <td>
        <?php echo CHtml::encode($data->payment_date_range); ?>
    </td>
    <?php if ($sixth_column_width) { ?>
        <td <?= $expense_head_hover_content; ?>>
            <?= $expense_head_short_content; ?>
        </td>
    <?php } ?>
    <td>
        <?= $data->vendor_type == '2' ? 'Yes' : 'No' ?>
    </td>
    <td>
        <?php echo CHtml::encode($data->address); ?>
    </td>
    <td>
        <?php if ((isset(Yii::app()->user->role) && (in_array('/vendors/update', Yii::app()->user->menuauthlist)))) { ?>
            <a class="fa fa-edit" onclick="editvendor(<?php echo $id; ?>)" data-id="<?php echo $id; ?>"
                style="cursor:pointer;"></a>
        <?php } ?>
        <?php if ((isset(Yii::app()->user->role) && (in_array('/vendors/deletevendor', Yii::app()->user->menuauthlist)))) { ?>
            <a class="fa fa-trash" onclick="deletevendor(<?php echo $id; ?>)" data-id="<?php echo $id; ?>"
                style="cursor:pointer;"></a>
        <?php } ?>
    </td>
</tr>