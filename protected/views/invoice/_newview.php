<?php
if($index == 0) {
?>
    <thead>
        <tr>
            <?php
                if((isset(Yii::app()->user->role) && (in_array('/invoice/viewinvoice', Yii::app()->user->menuauthlist)) || (in_array('/invoice/updateinvoice', Yii::app()->user->menuauthlist)))){
            ?>
            <th></th>
            <?php } ?>
            <th>Sl No</th>
            <th>Company</th>
            <th>Invoice No</th>
            <th>Client</th>
            <th>Project</th>
            <th>Date</th>
            <th>Status</th>
            <th>Stage</th>
            <th>Total Amount </th>
            <th>Write off</th>
            <th>Balance</th>
             
        </tr>
    </thead>
        <?php } 
        $style="";
        $title="";
        if($data->invoice_status == 'draft') {
            $style="style='background-color:#1c27b724 !important'";
            $title="Change status to saved" ; 
        }
        
        if($data['delete_approve_status']==1){
            $style="style='background-color:#f8cbcb !important'";
            $title="Delete Request Sent!" ; 
        }
        if($data['status_id']==108){
            $style="style='background-color:#fdf3c1 !important'";
            $title="Pending for Invoice" ; 
        }
        ?>

        <tr <?php echo $style ?> title="<?php echo $title ?>">
            <?php
                if((isset(Yii::app()->user->role) && (in_array('/invoice/viewinvoice', Yii::app()->user->menuauthlist)) || (in_array('/invoice/updateinvoice', Yii::app()->user->menuauthlist)))){
            ?>
            <td style="width:30px;text-align: center;">
                <span class="icon icon-options-vertical popover-test" data-toggle="popover"   data-placement="right" type="button" data-html="true" style="cursor: pointer;" ></span>
                <div class="popover-content hide">
                    <ul class="tooltip-hiden">
                        <?php
                        if((in_array('/invoice/viewinvoice', Yii::app()->user->menuauthlist))){
                        ?>
                        <li><a href="<?php echo Yii::app()->createAbsoluteUrl('invoice/viewinvoice',array('invid' => $data->invoice_id)); ?>" class="btn btn-xs btn-default">View</a></li>
                        <?php }?>
                        <?php
                        if((in_array('/invoice/updateinvoicelumpsum', Yii::app()->user->menuauthlist))){
                                if($data->invoice_status == 'draft') {
                                if($data->type =='quantity_rate'){
                        ?>
                        <li><a href="<?php echo Yii::app()->createAbsoluteUrl('invoice/updateinvoice',array('invid' => $data->invoice_id)); ?>"  class="btn btn-xs btn-default">Edit</a></li>
                        <?php } else{ ?>
                        <li><a href="<?php echo Yii::app()->createAbsoluteUrl('invoice/updateinvoicelumpsum',array('invid' => $data->invoice_id)); ?>"  class="btn btn-xs btn-default">Edit</a></li>        
                        <?php    } } } ?>
                        <?php                            
                        if($data['delete_approve_status']==0){ ?>
                        <li><button class="btn btn-xs btn-default remove_invoice" data-id ="<?php echo $data->invoice_id ?>">Delete</button></li> 
                        <?php } else{ ?>
                        <li><i class="fa fa-exclamation text-danger" title ="<?php echo $title ?>"></i></li> 
                        <?php } ?>
                    </ul>
                </div>
            </td>
            <?php } ?>
            <td width="50"><?php echo  $index+1; ?></td>
            <td width="50">
                <?php
                $company = Company::model()->findByPk($data['company_id']);
                echo $company['name'];
                ?>
            </td>
            <td><?php echo $data->inv_no; ?></td>
            <?php
            $id= $data->invoice_id;
			$tblpx = Yii::app()->db->tablePrefix;
			$query=Yii::app()->db->createCommand("select * from {$tblpx}inv_list where inv_id=$id")->queryAll();
			$id= $data['client_id'];
			if($id != ''){
			$sql=Yii::app()->db->createCommand("select {$tblpx}clients.name from {$tblpx}clients
			 left join {$tblpx}projects ON {$tblpx}projects.client_id={$tblpx}clients.cid 
			 where {$tblpx}clients.cid=$id")->queryRow();
			 
			 $clientname = $sql['name'];
			}else{
			 $pid = $data['project_id'];
			 $pmodel  = Projects::model()->findBypk($pid);	
			 $clientname = $pmodel->client->name;
			}
            ?>
            <td><?php echo $clientname; ?></td>
            <td>
                <?php 
                $pmodel  = Projects::model()->findBypk($data->project_id);
                echo isset($pmodel->name)?$pmodel->name:'';
                ?>
            </td>
            <td><?php echo  date("d-m-Y", strtotime($data->date)); ?></td>
            <td>
                <?php
                if($data->invoice_status == 'saved') {
                echo ucfirst($data->invoice_status); 
                } else{ 
                    if(!ANCAPMS_LINKED){
                    ?>
                <div style="cursor:pointer" class="update_status invoice_status_<?php echo $data->invoice_id; ?>" id="<?php echo $data->invoice_id; ?>" data-toggle="tooltip" data-placement="bottom" title="Change status to saved">Draft &nbsp;<span class="fa fa-save" style="color: #2e6da4;"></span></div>
                <div id="loader" class="loader_<?php echo $data->invoice_id ?>"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/ajax-loader.gif"></div>
               <?php }else{ 
                        if($data->status_id == '108'){
                            echo ucfirst('pending for invoice');
                        }else{ ?>
                        <div style="cursor:pointer" class="update_status invoice_status_<?php echo $data->invoice_id; ?>" id="<?php echo $data->invoice_id; ?>" data-toggle="tooltip" data-placement="bottom" title="Change status to saved">Draft &nbsp;<span class="fa fa-save" style="color: #2e6da4;"></span></div>
                        <div id="loader" class="loader_<?php echo $data->invoice_id ?>"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/ajax-loader.gif"></div>
                        <?php } 
                    }
                }
                ?>
            </td>
            <td>
                <?php 
                $stgage_name = "";
                $quotation_no = "";
                if($data->stage_id != 0 || $data->stage_id !=""){
                    $paymentstage = $payment_stage = PaymentStage::model()->findByPk($data->stage_id);
                    $stgage_name = $paymentstage['payment_stage'];
                    $quotation_data = Quotation::model()->findByPk($paymentstage['quotation_id']);
                    $quotation_no = $quotation_data['inv_no']." / ";
                }            
                echo $quotation_no.$stgage_name;  
                ?>
            </td>
            <td class="text-right"><?php echo Controller::money_format_inr(($data->amount + $data->tax_amount+($data->round_off)),2,1); ?></td>
            <?php
            $addcharges  =  Yii::app()->db->createCommand("SELECT ROUND(SUM(amount), 2) as amount FROM " . $tblpx . "invoice_additional_charges WHERE `invoice_id`=" . $data->invoice_id)->queryRow();
            
                $writeoff = Writeoff::model()->find(array(
                'condition' => 'invoice_id = :invoice_id',
                'params' => array(':invoice_id' => $data->invoice_id),
                'order' => 'id DESC' // Replace `created_at` with the appropriate timestamp field in your table
            ));
                if(!empty($writeoff->amount)) {
                    $writeoffAmount = $writeoff->amount;
                } else {
                    $writeoffAmount = 0;
                }
                $balanceAmount = ($data->amount + $data->tax_amount+($data->round_off)) - $writeoffAmount;
                $balanceAmount = ($balanceAmount!== 0)?($balanceAmount + $addcharges['amount']):$addcharges['amount'];
            ?>
            <td class="text-right"><?php echo Controller::money_format_inr($writeoffAmount,2,1); ?></td>
            <td class="text-right"><?php echo Controller::money_format_inr($balanceAmount,2,1); ?></td>
        </tr>



<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
});
</script>
