<?php

$company = Company::model()->findBypk($model->company_id);
$purchase_data = Purchase::model()->findByPK($model->purchase_no);

if (!isset($_GET['exportpdf'])) {
    ?>
    <?php
    $tblpx = Yii::app()->db->tablePrefix;
    if (Yii::app()->user->role == 1) {
        $bill_status = Yii::app()->db->createCommand("SELECT *  FROM {$tblpx}purchase LEFT JOIN {$tblpx}status ON
        {$tblpx}purchase.purchase_billing_status = {$tblpx}status.sid WHERE p_id = " . $model->p_id . " AND
        ({$tblpx}status.caption='Pending to be Billed' OR {$tblpx}status.caption='Fully Billed' OR {$tblpx}status.caption='Partially Billed') AND
        {$tblpx}status.status_type='purchase_bill_status'")->queryRow();
    } else {
        $bill_status = Yii::app()->db->createCommand("SELECT *  FROM {$tblpx}purchase LEFT JOIN {$tblpx}status ON
        {$tblpx}purchase.purchase_billing_status = {$tblpx}status.sid WHERE p_id = " . $model->p_id . " AND
        {$tblpx}status.caption='Pending to be Billed'  AND
        {$tblpx}status.status_type='purchase_bill_status'")->queryRow();
    }
    ?>
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.min.js"></script>
    <script type="text/javascript"
        src="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/validation/jquery.validate.js"></script>
    <script type="text/javascript"
        src="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/validation/additional-methods.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/tableHeadFixer.js"></script>

    <script>
        $(function () {
            $("#datepicker").datepicker({
                dateFormat: 'dd-mm-yy'
            }).datepicker("setDate", new Date());

        });
    </script>

    <div class="container">
        <div class="loading-overlay">
            <span class="fa fa-spinner fa-3x fa-spin"></span>
        </div>

        <div class="invoicemaindiv">
            <div class="header-container">
                <h3>Purchase Order</h3>
                <div class="btn-container">
                    <?php
                    if ($model->purchase_status == 'saved') {
                        echo CHtml::link('<i class="fa fa-file-pdf-o button-icon" aria-hidden="true"></i>', Yii::app()->request->requestUri . "&exportpdf=1", array('class' => 'btn btn-info', 'title' => 'SAVE AS PDF'));
                        ?>
                        <a id="download" class="excelbtn1 btn btn-info" title="SAVE AS EXCEL"><i
                                class="fa fa-file-excel-o button-icon" aria-hidden="true"></i></a>
                        <?php
                    }
                    ?>
                    <?php
                    if (isset(Yii::app()->user->role) && (in_array('/purchase/sendattachment', Yii::app()->user->menuauthlist))) {
                        if ($model->purchase_status == 'saved') {
                            ?>
                            <a data-id="<?php echo $model->p_id; ?>" id="sendmail" class="btn btn-info sendmail" title="SEND MAIL">
                                <i class="fa fa-envelope-o button-icon" aria-hidden="true"></i>
                            </a>
                            <?php
                        }
                    }
                    ?>
                </div>
            </div>
            <div id="loading"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/ajax-loader.gif"></div>

            <div class="row">
                <div class="col-sm-12">
                    <?php
                    foreach (Yii::app()->user->getFlashes() as $key => $message) {
                        $alertType = ($key == 'error') ? 'danger' : 'success';

                        echo '<div class="alert alert-' . $alertType . '">' . $message . "</div>\n";
                    }
                    ?>
                </div>
            </div>
            <div id="errormessage"></div>
            <div id="send_form" style="display:none;">
                <div class="entries-wrapper padding-bottom-10">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="heading-title">Send Mail</div>
                            <div class="dotted-line"></div>
                        </div>
                    </div>
                    <form id="form_send">

                        <div class="row">
                            <div class="col-sm-6 col-md-4 form-group">
                                <input type="hidden" name="p_id" id="p_id" value="<?php echo $model->p_id; ?>">
                                <input type="hidden" name="pdf_path" id="pdf_path" value="">
                                <input type="hidden" name="pdf_name" id="pdf_name" value="">
                                <div class="form-group">
                                    <label for="ProjectType_project_type" class="required">To</label>
                                    <input id="mail_to" name="mail_to" class="form-control">
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-4 form-group">
                                <input type="hidden" name="p_id" id="p_id" value="<?php echo $model->p_id; ?>">
                                <div class="form-group">
                                    <label for="ProjectType_project_type" class="required">Message</label>
                                    <textarea id="mail_content" name="mail_content" class="form-control"></textarea>
                                </div>
                            </div>
                            <?php
                            if (!empty($settings['po_email_from'])) {
                                ?>
                                <div class="col-sm-6 col-md-4 form-group">
                                    <label> &nbsp; </label>
                                    <div>
                                        <input type="checkbox" class="form-check-input change_val " checked='true'
                                            id="send_copy" name="send_copy">
                                        <label class="form-check-label" for="inclusive">Send copy to (
                                            <?php echo $settings['po_email_from']; ?>)
                                        </label>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>

                            <div class="col-xs-12 text-right">
                                <?php echo CHtml::ResetButton('Close', array('onclick' => 'closeaction(this,event);', 'class' => 'btn btn-sm')); ?>
                                <button class="btn btn-info btn-sm send_btn">Send</button>
                                <div id="loader"><img
                                        src="<?php echo Yii::app()->theme->baseUrl; ?>/images/ajax-loader.gif"></div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <form id="pdfvals1" method="post" action="<?php echo $this->createAbsoluteUrl('purchase/addpurchase'); ?>">
                <input type="hidden" name="purchaseview" value="<?php echo $model->p_id ?>">
                <div class="page_filter margin-bottom-10">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>COMPANY :</label>
                                <?php
                                if (isset(Yii::app()->user->role) && (in_array('/purchase/companyedit', Yii::app()->user->menuauthlist))) {
                                    if (!empty($bill_status)) {
                                        ?>
                                        <a href="#" id="menu-trigger">
                                            <icon class="fa fa-edit">
                                        </a>
                                        <?php
                                    }
                                }
                                ?>
                                <?php if (Yii::app()->user->role == 1 && !empty($admin_approval_data)) { ?>
                                    <a href="#" id="edit_company" class="btn btn-xs btn-default"
                                        attr="<?= $model->p_id ?>">Approve</a>
                                <?php } ?>
                                <div class="po_company <?= !empty($admin_approval_data) ? 'user_company_edit_class' : '' ?>"
                                    id="<?= $model->p_id ?>">
                                    <?php echo ucfirst($company['name']); ?>
                                </div>
                                <input type="hidden" id="txtCompany" name="txtCompany"
                                    value="<?php echo ucfirst($company['name']); ?>" />
                                <input type="hidden" id="txtPurchaseCompany" name="txtPurchaseCompany"
                                    value="<?php echo $model->company_id; ?>" />
                                <div id="menu" style="display:none">
                                    <select name="company" class="company select2-hidden-accessible" id="company_id"
                                        style="width:190px" tabindex="-1" aria-hidden="true">
                                        <option value="">Choose Company</option>
                                        <?php foreach ($typelist as $key => $value) { ?>
                                            <option value="<?php echo $value['id']; ?>">
                                                <?php echo $value['name']; ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                    <button id="company_change">Save</button>
                                </div>
                                <div id="previous_details"
                                    style="position: fixed;top: 94px; left: 70px; right: 70px; z-index: 2; max-width:1150px; margin:0 auto;">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>GST NO</label>
                                <div class="gstnum">
                                    <?php echo ucfirst($company['company_gstnum']); ?>
                                </div>
                                <input type="hidden" class="gstnum" name="txtCompanyGst"
                                    value="<?php echo ucfirst($company['company_gstnum']); ?>" />
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>PROJECT : </label>
                                <div>
                                    <?php echo isset($project->name) ? $project->name : ''; ?>
                                </div>
                                <input type="hidden" name="project" id="project"
                                    value="<?php echo isset($project->pid) ? $project->pid : ''; ?>">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>EXPENSE HEAD : </label>
                                <div>
                                    <?php echo (isset($expense_head->type_name)) ? $expense_head->type_name : ''; ?>
                                </div>
                                <input type="hidden" name="expensehead" id="expensehead"
                                    value="<?php echo (isset($expense_head->type_id)) ? $expense_head->type_id : ''; ?>">
                            </div>
                        </div>
                    </div>
                    <div class='row'>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>VENDOR : </label>
                                <div>
                                    <?php echo (isset($vendor->name)) ? $vendor->name : ''; ?>
                                </div>
                                <input type="hidden" name="vendor" id="vendor"
                                    value="<?php echo (isset($vendor->vendor_id)) ? $vendor->vendor_id : ''; ?>">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>DATE : </label>
                                <div>
                                    <?php echo date('d-m-Y', strtotime($model->purchase_date)); ?>
                                    <input type="hidden" name="date"
                                        value="<?php echo date('d-m-Y', strtotime($model->purchase_date)); ?>" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>PURCHASE NO : </label>
                                <div>
                                    <?php echo isset($model->purchase_no) ? $model->purchase_no : ''; ?>
                                    <input type="hidden" name="purchaseno"
                                        value="<?php echo isset($model->purchase_no) ? $model->purchase_no : ''; ?>"
                                        readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>CONTACT NO : </label>

                                <div>
                                    <?php echo isset($model->contact_no) ? $model->contact_no : ''; ?>
                                    <input type="hidden" name="contact_no"
                                        value="<?php echo isset($model->contact_no) ? $model->contact_no : '-'; ?>"
                                        readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>SHIPPING ADDRESS : </label>
                                <div>
                                    <?php echo isset($model->shipping_address) ? nl2br($model->shipping_address) : '-'; ?>
                                    <input type="hidden" name="shipping_address"
                                        value="<?php echo isset($model->shipping_address) ? $model->shipping_address : '-'; ?>"
                                        readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="form-group">
                                <label>DESCRIPTION: </label>
                                <div class="desc_po">
                                    <?php echo isset($model->purchase_description) ? nl2br($model->purchase_description) : '-'; ?>
                                    <input type="hidden" name="shipping_address"
                                        value="<?php echo isset($model->purchase_description) ? $model->purchase_description : '-'; ?>"
                                        readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
} else {
    ?>
                    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/pdfstyle.css">
                    <style>
                        .item_table>tbody>tr>td {
                            border: 1px solid red !important;
                            border-collapse: collapse;
                        }

                        table {
                            border-collapse: collapse;
                            width: 100%;
                            font-family: Arial, Helvetica, sans-serif;

                        }
                    </style>

                    <div class="container" style="margin:0px 30px">
                        <!-- <br> -->
                        <h3 class="text-center">PURCHASE ORDER</h3>
                        <?php
                        $activeProjectTemplate = $this->getActiveTemplate();
                        ?>

                        <table class="details">
                            <tbody>
                                <tr>
                                    <td class="border-bottom" colspan="2">
                                        <b>PURCHASE NO :
                                            <?php echo isset($model->purchase_no) ? $model->purchase_no : ''; ?>
                                        </b>
                                    </td>
                                    <td class="text-right border-bottom">
                                        <p>Date:
                                            <?php echo date("d-m-Y", strtotime($model->purchase_date)); ?>
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="border-right border-bottom">
                                        <b>
                                            <?php echo isset($vendor->name) ? $vendor->name : '' ?>
                                        </b>
                                        <p>
                                            <?php echo (isset($vendor->address)) ? $vendor->address : ''; ?>
                                        </p>
                                        <p>Phone:
                                            <?php echo isset($vendor->phone) ? $vendor->phone : ''; ?>
                                        </p>
                                        <p><b>GST NO :
                                                <?php echo (isset($vendor->gst_no)) ? $vendor->gst_no : ''; ?><b></p>
                                    </td>
                                    <td class="border-right border-bottom">
                                        <b class="header-border">Delivery Address</b><br><br>
                                        <p>
                                            <?php echo isset($model->shipping_address) ? nl2br($model->shipping_address) : '-'; ?>
                                        </p>
                                        <p>PH:
                                            <?php echo isset($model->contact_no) ? $model->contact_no : '-'; ?>
                                        </p>
                                    </td>
                                    <td class=" border-bottom">

                                        <p>PROJECT:
                                            <?php echo isset($project->name) ? $project->name : '' ?>
                                        </p>
                                        <p>EXPENSE HEAD :
                                            <?php echo (isset($expense_head->type_name)) ? $expense_head->type_name : ''; ?>
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <b class="header-border">Description:</b><br><br>
                                        <div class="text-sm note-list">
                                            <?php echo $model->purchase_description; ?>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                    </div>
                    <?php
}
?>
            </div>
            <?php
            if (!isset($_GET['exportpdf'])) {
                $discount_total = $tax_total_amount = $item_total = 0;
                ?>
                <div class="row">
                    <div class="col-xs-12">
                        <div id="table-scroll" class="table-scroll">
                            <div id="faux-table" class="faux-table" aria="hidden"></div>
                            <div id="table-wrap" class="table-wrap">
                                <div class="table-responsive">
                                    <table border="1" class="item_table table">
                                        <thead class="entry-table sticky-thead">
                                            <tr class="table-header">
                                                <th class="text-center">Sl.No</th>
                                                <th class="text-center">Specification</th>
                                                <th class="text-center">Quantity</th>
                                                <th class="text-center">Unit</th>
                                                <th class="text-center">HSN Code</th>
                                                <th class="text-center">Rate</th>
                                                <th class="text-center">Amount</th>
                                                <th class="text-center">Tax Slab (%)</th>
                                                <th class="text-center">SGST Amount</th>
                                                <th class="text-center">SGST (%)</th>
                                                <th class="text-center">CGST Amount</th>
                                                <th class="text-center">CGST (%)</th>
                                                <th class="text-center">IGST Amount</th>
                                                <th class="text-center">IGST (%)</th>
                                                <th class="text-center">Discount (%)</th>
                                                <th class="text-center">Discount Amount</th>
                                                <th class="text-center">Tax Amount</th>
                                                <th class="text-center">Total</th>
                                            </tr>
                                        </thead>
                                        <tbody class="addrow">
                                            <?php
                                            $total_amount = 0;
                                            if (!empty($itemmodel)) {
                                                $i = 0;

                                                foreach ($itemmodel as $value) {
                                                    ?>
                                                    <tr>
                                                        <td colspan="18" class="categ_td">
                                                            <?php echo !empty($value['category_title']) ? $value['category_title'] : '' ?>
                                                        </td>
                                                    </tr>

                                                    <?php

                                                    foreach ($value as $item) {



                                                        if (isset($item['specification'])) {
                                                            $discount_total += $item['disc'];
                                                            $tax_total_amount += $item['tot_tax'];
                                                            $amount_ = $item['quantity'] * $item['rate'];
                                                            if ($item['amount'] != $amount_) {
                                                                $item['amount'] = $amount_;
                                                            }
                                                            $total_amt = ($item['amount'] + $item['tot_tax']) - $item['disc'];
                                                            $item_total += $total_amt;
                                                            ?>
                                                            <tr class="tr_class">
                                                                <td><input type="hidden" name="sl_No[]" value="<?php echo $i + 1; ?>">
                                                                    <?php echo $i + 1; ?>
                                                                </td>
                                                                <td><input type="hidden" name="description[]"
                                                                        value="
                                                            <?php echo filter_var($item['specification'], FILTER_SANITIZE_STRING); ?>">
                                                                    <?php echo $item['specification']; ?>
                                                                </td>
                                                                <td class="text-right"><input type="hidden" name="quantity[]"
                                                                        value="<?php echo $item['quantity']; ?>">
                                                                    <?php echo $item['quantity']; ?>
                                                                </td>
                                                                <td><input type="hidden" name="unit[]" value="<?php echo $item['unit']; ?>"
                                                                        readonly><?php echo $item['unit']; ?>
                                                                </td>
                                                                <td><input type="hidden" name="hsn_code[]"
                                                                        value="<?php echo $item['hsn_code']; ?>" readonly>
                                                                    <?php echo $item['hsn_code']; ?>
                                                                </td>
                                                                <td class="text-right"><input type="hidden" name="rate[]"
                                                                        value="<?php echo $item['rate']; ?>" readonly>
                                                                    <?php echo Yii::app()->Controller->money_format_inr($item['rate'], 2, 1); ?>
                                                                </td>

                                                                <td class="text-right"><input type="hidden" name="amount[]"
                                                                        value="<?php echo $item['rate']; ?>" readonly>
                                                                    <?php echo Yii::app()->Controller->money_format_inr($item['amount'], 2, 1); ?>
                                                                </td>

                                                                <td class="text-right"><input type="hidden" name="tax_slab[]"
                                                                        value="<?php echo $item['tax_slab']; ?>" readonly>
                                                                    <?php echo $item['tax_slab']; ?>
                                                                </td>
                                                                <td class="text-right"><input type="hidden" name="sgst[]"
                                                                        value="<?php echo $item['sgst']; ?>" readonly>
                                                                    <?php echo Yii::app()->Controller->money_format_inr($item['sgst'], 2, 1); ?>
                                                                </td>
                                                                <td class="text-right"><input type="hidden" name="sgstp[]"
                                                                        value="<?php echo $item['sgstp']; ?>" readonly>
                                                                    <?php echo Yii::app()->Controller->money_format_inr($item['sgstp'], 2, 1); ?>
                                                                </td>
                                                                <td class="text-right"><input type="hidden" name="cgst[]"
                                                                        value="<?php echo $item['cgst']; ?>" readonly>
                                                                    <?php echo Yii::app()->Controller->money_format_inr($item['cgst'], 2, 1); ?>
                                                                </td>
                                                                <td class="text-right"><input type="hidden" name="cgstp[]"
                                                                        value="<?php echo $item['cgstp']; ?>" readonly>
                                                                    <?php echo Yii::app()->Controller->money_format_inr($item['cgstp'], 2, 1); ?>
                                                                </td>
                                                                <td class="text-right"><input type="hidden" name="igst[]"
                                                                        value="<?php echo $item['igst']; ?>" readonly>
                                                                    <?php echo Yii::app()->Controller->money_format_inr($item['igst'], 2, 1); ?>
                                                                </td>
                                                                <td class="text-right"><input type="hidden" name="igstp[]"
                                                                        value="<?php echo $item['igstp']; ?>" readonly>
                                                                    <?php echo Yii::app()->Controller->money_format_inr($item['igstp'], 2, 1); ?>
                                                                </td>
                                                                <td class="text-right"><input type="hidden" name="disc[]"
                                                                        value="<?php echo $item['discp']; ?>" readonly>
                                                                    <?php echo Yii::app()->Controller->money_format_inr($item['discp'], 2, 1); ?>
                                                                </td>
                                                                <td class="text-right"><input type="hidden" name="discp[]"
                                                                        value="<?php echo $item['disc']; ?>" readonly>
                                                                    <?php echo Yii::app()->Controller->money_format_inr($item['disc'], 2, 1); ?>
                                                                </td>

                                                                <td class="text-right"><input type="hidden" name="tot_tax[]"
                                                                        value="<?php echo $item['tot_tax']; ?>" readonly>
                                                                    <?php echo Yii::app()->Controller->money_format_inr($item['tot_tax'], 2, 1); ?>
                                                                </td>
                                                                <td class="text-right"><input type="hidden" name="total_amt[]"
                                                                        value="<?php echo $total_amt; ?>" readonly>
                                                                    <?php echo Yii::app()->Controller->money_format_inr($total_amt, 2, 1); ?>
                                                                </td>

                                                            </tr>
                                                            <?php
                                                            $i++;
                                                        }
                                                    }

                                                    ?>


                                                    <?php

                                                }
                                            }
                                            ?>

                                        </tbody>
                                        <tfoot class="entry-table">
                                            <tr>
                                                <th style="text-align: right;" colspan="15" class="text-right">TOTAL </th>
                                                <th class="text-right">
                                                    <div id="disc_total">
                                                        <?php echo isset($discount_total) ? Yii::app()->Controller->money_format_inr($discount_total, 2, 1) : '' ?>

                                                </th>
                                                <th class="text-right">
                                                    <div id="tax_total">
                                                        <?php echo isset($tax_total_amount) ? Yii::app()->Controller->money_format_inr($tax_total_amount, 2, 1) : ''; ?>
                                                    </div>
                                                </th>
                                                <th class="text-right">
                                                    <div id="grand_total">
                                                        <?php echo Yii::app()->Controller->money_format_inr($item_total, 2, 1); ?>
                                                    </div>
                                                </th>
                                            </tr>
                                            <tr>
                                                <th colspan="15" class="text-right">GRAND TOTAL </th>
                                                <th class="text-right" colspan="3">
                                                    <div id="disc_total" class="text-right">
                                                        <?php echo isset($model->total_amount) ? Yii::app()->Controller->money_format_inr($model->getPurchaseTotalAmount($model['p_id']), 2, 1) : '' ?>

                                                </th>
                                            </tr>
                                            <?php
                                            if ($model->inclusive_gst == 1) {
                                                ?>
                                                <tr>
                                                    <td colspan="18">*Inclusive of GST</td>
                                                </tr>
                                                <?php
                                            }
                                            ?>

                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } else {
                ?>
                <div class="content" style="margin:0px 30px">
                    <div class="wrapper">
                        <div class="holder">
                            <table class="table pdf-table">
                                <thead>
                                    <tr>
                                        <th>SI</th>
                                        <th>Description of Goods</th>
                                        <th width="20px">HSN Code</th>
                                        <th>Quantity</th>
                                        <th>Unit Price</th>
                                        <th>Taxable Amount</th>
                                        <th>GST (%)</th>
                                        <th width="20px">GST Amount</th>
                                        <th>Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $total_amount = 0;
                                    $cgst_sum = $sgst_sum = $igst_sum = 0;
                                    $discount_total = $tax_total_amount = $quantity_total = 0;
                                    if (!empty($itemmodel)) {
                                        $i = 1;

                                        foreach ($itemmodel as $value) {
                                            ?>
                                            <tr>
                                                <td colspan="9" class="categ_td border-bottom">
                                                    <?php echo !empty($value['category_title']) ? $value['category_title'] : '' ?>
                                                </td>
                                            </tr>
                                            <?php

                                            foreach ($value as $item) {
                                                if (isset($item['specification'])) {
                                                    $discount_total += $item['disc'];
                                                    $tax_total_amount += $item['tot_tax'];
                                                    $amount_ = ($item['quantity'] * $item['rate']) - $item['disc'];
                                                    if ($item['amount'] != $amount_) {
                                                        $item['amount'] = $amount_;
                                                    }
                                                    $total_tax_perc = $item['sgstp'] + $item['cgstp'] + $item['igstp'];
                                                    $amount_total = $item['amount'] + $item['tot_tax'];
                                                    $quantity_total += $item['quantity'];
                                                    if (!empty($item['cgst']))
                                                        $cgst_sum += $item['cgst'];
                                                    if (!empty($item['sgst']))
                                                        $sgst_sum += $item['sgst'];
                                                    if (!empty($item['igst']))
                                                        $igst_sum += $item['igst'];
                                                    ?>
                                                    <tr>
                                                        <td>
                                                            <?php echo $i; ?>
                                                        </td>
                                                        <td>
                                                            <?php echo $item['specification']; ?>
                                                        </td>
                                                        <td>
                                                            <?php echo $item['hsn_code']; ?>
                                                        </td>
                                                        <td>
                                                            <?php echo $item['quantity'] . ' ' . $item['unit']; ?>
                                                        </td>
                                                        <td class="text-right">
                                                            <?php echo Yii::app()->Controller->money_format_inr($item['rate'], 2, 1); ?>
                                                        </td>
                                                        <td class="text-right">
                                                            <?php echo Yii::app()->Controller->money_format_inr($item['amount'], 2, 1); ?>
                                                        </td>
                                                        <td>
                                                            <?php echo $total_tax_perc; ?>
                                                        </td>
                                                        <td class="text-right">
                                                            <?php echo Yii::app()->Controller->money_format_inr($item['tot_tax'], 2, 1); ?>
                                                        </td>
                                                        <td class="text-right">
                                                            <?php echo Yii::app()->Controller->money_format_inr($amount_total, 2, 1); ?>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                    $i++;
                                                }
                                            }
                                            ?>
                                            <?php
                                        }
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                        <table class="table foot-table w-100">
                            <tbody>
                                <tr>
                                    <?php
                                    if ($model->inclusive_gst == 1) {
                                        ?>
                                        <td width="82%">*Inclusive of GST</td>

                                        <?php
                                    }
                                    ?>

                                    <td class="text-right"><b>Total:</b></td>
                                    <td class="text-right"><b>
                                            <?php echo isset($model->total_amount) ? Yii::app()->Controller->money_format_inr(($model->getPurchaseTotalAmount($model['p_id'])), 2, 1) : '' ?>
                                        </b>&nbsp;&nbsp;</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <table class="table details border-top-0" style="margin:0px 30px">
                    <tbody>
                        <tr>
                            <td colspan="9" class="text-right">
                                <?php
                                $amountforletters = '';
                                $amountforletters = $model->getPurchaseTotalAmount($model['p_id']);
                                ?>
                                <p class="text-capitalize">
                                    <?php echo Yii::app()->Controller->displaywords($amountforletters); ?>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <th colspan="8" class="text-right">
                                <label>Taxable Amount:</label>
                            </th>
                            <th class="text-right">
                                <?php echo isset($model->total_amount) ? Yii::app()->Controller->money_format_inr($model->total_amount, 2, 1) : '' ?>
                            </th>
                        </tr>
                        <tr>
                            <th colspan="8" class="text-right">
                                <label>CGST:</label>
                            </th>
                            <th class="text-right">
                                <?php echo Yii::app()->Controller->money_format_inr($cgst_sum, 2, 1); ?>
                            </th>
                        </tr>
                        <tr>
                            <th colspan="8" class="text-right">
                                <label>SGST:</label>
                            </th>
                            <th class="text-right">
                                <?php echo Yii::app()->Controller->money_format_inr($sgst_sum, 2, 1); ?>
                            </th>
                        </tr>
                        <tr>
                            <th colspan="8" class="text-right">
                                <label>IGST:</label>
                            </th>
                            <th class="text-right">
                                <?php echo Yii::app()->Controller->money_format_inr($igst_sum, 2, 1); ?>
                            </th>
                        </tr>
                        <tr>
                            <th colspan="8" class="text-right">
                                <label>Total Amount : </label>
                            </th>
                            <th class="text-right">
                                <?php echo isset($model->total_amount) ? Yii::app()->Controller->money_format_inr(($model->getPurchaseTotalAmount($model['p_id'])), 2, 1) : '' ?>
                            </th>
                        </tr>
                    </tbody>
                </table>
                <table class="mt-2 w-100" style="margin:20px 30px">
                    <tbody>
                        <tr>
                            <td class="text-right valign-top">
                                <div class="text-md text-right"><b>Authorized Signatory,</b></div> <br> <br><br>
                            </td>
                        </tr>
                    </tbody>
                </table>

            <?php }
            ?>
            <?php
            if (!isset($_GET['exportpdf'])) {
                ?>
                <div style="padding-right: 0px;"
                    class="col-md-offset-9 col-md-3 col-sm-offset-8 col-sm-4 col-xs-offset-6 col-xs-6">

                    <input type="hidden" name="subtot" value="<?php echo $model->total_amount; ?>" readonly>
                    <input type="hidden" name="grand" value="<?php echo $model->total_amount; ?>" readonly>
                    <input type="hidden" name="disc_toatal"
                        value="<?php isset($discount_total) ? $discount_total : '0'; ?>">
                    <input type="hidden" name="tax_total_amount"
                        value="<?php isset($tax_total_amount) ? $tax_total_amount : ''; ?>">

                </div>
            </form>




            <script>
                $(function () {

                    $('.pdfbtn1').click(function () {
                        $("form#pdfvals1").attr('action', "<?php echo $this->createAbsoluteUrl('purchase/exportpdf'); ?>");
                        $("form#pdfvals1").submit();

                    });

                    $('.excelbtn1').click(function () {
                        $("form#pdfvals1").attr('action', "<?php echo $this->createAbsoluteUrl('purchase/exportexcel'); ?>");
                        $("form#pdfvals1").submit();

                    });


                });
            </script>

            <script>
                function closeaction() {
                    $('#send_form').slideUp(500);
                }

                $('#sendmail').click(function () {
                    $('#send_form').slideDown();
                });

                $("#form_send").validate({
                    rules: {
                        mail_content: {
                            required: true,
                        },
                        mail_to: {
                            required: true,
                            email: true
                        },
                    },
                    messages: {
                        mail_content: {
                            required: "Please enter mesage",
                        },
                        mail_to: {
                            required: "Please enter email",
                            email: "Please enter valid email"
                        }
                    },
                });

                $('.send_btn').click(function (e) {
                    e.preventDefault();
                    var data = $('#form_send').serialize();
                    if ($("#form_send").valid()) {
                        $('#loader').css('display', 'inline-block');
                        if ($("#send_copy").prop('checked') == true) {
                            var send_copy = 1;
                        } else {
                            var send_copy = 0;
                        }
                        var p_id = $('#p_id').val();
                        var mail_to = $('#mail_to').val();
                        var mail_content = $('#mail_content').val();
                        var pdf_path = $('#pdf_path').val();
                        var pdf_name = $('#pdf_name').val();
                        $('.send_btn').prop('disabled', true)
                        $('.loading-overlay').addClass('is-active');
                        $.ajax({
                            url: '<?php echo Yii::app()->createAbsoluteUrl('purchase/sendattachment'); ?>',
                            type: 'POST',
                            dataType: 'json',
                            data: {
                                p_id: p_id,
                                mail_to: mail_to,
                                mail_content: mail_content,
                                pdf_path: pdf_path,
                                pdf_name: pdf_name,
                                send_copy: send_copy
                            },
                            success: function (response) {
                                if (response.status == '1') {
                                    $('#loader').hide();
                                    $('.send_btn').prop('disabled', false)
                                    $('#send_form').slideUp(500);
                                    $("#form_send").trigger('reset');
                                    $("#errormessage").show()
                                        .html('<div class="alert alert-success">' + response.message + '</div>')
                                        .fadeOut(10000);
                                } else {
                                    $('#loader').hide();
                                    $('.send_btn').prop('disabled', false)
                                    $("#errormessage").show()
                                        .html('<div class="alert alert-danger">' + response.message + '</div>')
                                        .fadeOut(10000);
                                }
                            }
                        })
                    }
                })

                $(".sendmail").click(function () {
                    var p_id = $(this).attr("data-id");
                    $('#loading').show();
                    $.ajax({
                        type: 'POST',
                        dataType: 'json',
                        url: '<?php echo Yii::app()->createAbsoluteUrl('purchase/pdfgeneration'); ?>',
                        data: {
                            p_id: p_id
                        },
                        success: function (response) {
                            $('#pdf_path').val(response.path);
                            $('#pdf_name').val(response.name);
                        }
                    })
                })

                $(document).ready(function () {
                    $('#loading').hide();
                    $('#previous_details').hide();
                    $('#menu-trigger').click(function (event) {
                        $('#menu').show();
                    });
                })

                $(document).ready(function () {
                    $(".company").select2();
                    $(document).on('mouseover', '.user_company_edit_class', function (e) {
                        var purchase_id = this.id;
                        $('#loading').show();
                        $.ajax({
                            url: '<?php echo Yii::app()->createAbsoluteUrl('dashboard/pocompanyeditlog'); ?>',
                            type: 'GET',
                            dataType: 'json',
                            data: {
                                id: purchase_id
                            },
                            success: function (result) {
                                if (result.status == 1) {
                                    $('#previous_details').html(result.html);
                                    $("#previous_details").show();
                                    $("#pre_fixtable2").tableHeadFixer();
                                } else {
                                    $('#previous_details').html(result.html);
                                    $("#previous_details").hide();
                                    $("#pre_fixtable2").tableHeadFixer();
                                }
                            }
                        });
                        $('#previous_details').show();
                    });
                    $(document).on('mouseout', '.user_company_edit_class', function (e) {
                        $("#previous_details").hide();
                    });
                });
                $(document).on('click', '#edit_company', function (e) {
                    if (confirm("Do you want to approve company change?")) {
                        var purchase_id = $(this).attr('attr');
                        $('.loading-overlay').addClass('is-active');
                        $.ajax({
                            type: 'POST',
                            url: '<?php echo Yii::app()->createAbsoluteUrl('dashboard/companyapprove'); ?>',

                            data: {
                                'purchase_id': purchase_id
                            },
                            success: function (result) {

                                if (result === "success") {
                                    $('#edit_company').hide();
                                    $("#errormessage").show()
                                        .html('<div class="alert alert-success">Success! Request approved successfully.</div>')
                                        .fadeOut(10000);
                                    $('.po_company').removeClass('user_company_edit_class');
                                } else {
                                    $("#errormessage").show()
                                        .html('<div class="alert alert-warning">Failed! Please try again.</div>')
                                        .fadeOut(10000);
                                    return false;
                                }
                            }
                        });
                    } else {
                        return false;
                    }
                });
                $(document).on('click', '#company_change', function (e) {
                    e.preventDefault();
                    var prev_company_id = $('#txtPurchaseCompany').val();
                    var prev_company_name = $('#txtCompany').val();
                    var company_id = $('#company_id').val();
                    if (company_id != "") {
                        $('#loading').show();
                        var purchase_id = '<?php echo $model->p_id; ?>';
                        $.ajax({
                            type: 'GET',
                            url: '<?php echo Yii::app()->createAbsoluteUrl('purchase/companyedit'); ?>',
                            dataType: 'json',
                            data: {
                                'company_id': company_id,
                                'purchase_id': purchase_id,
                                'prev_company_id': prev_company_id,
                                'prev_comp_name': prev_company_name
                            },
                            success: function (result) {
                                console.log(result.company);
                                if (result.status == 1) {
                                    $('#txtCompany').val(result.company);
                                    $('#txtPurchaseCompany').val(result.id);
                                    $('.po_company').html(result.company);
                                    $('.gstnum').html(result.gstnum);
                                    $('#menu').hide();
                                } else if (result.status == 2) {
                                    $('#txtCompany').val(result.company);
                                    $('#txtPurchaseCompany').val(result.id);
                                    $('.po_company').html(result.company);
                                    $('.gstnum').html(result.gstnum);
                                    $('#menu').hide();
                                    $('.po_company').addClass('user_company_edit_class');
                                }
                            }
                        })
                    }
                })
                $(document).ajaxComplete(function () {
                    $('.loading-overlay').removeClass('is-active');
                    $('#loading').hide();
                });
            </script>
            <?php
            }
            ?>