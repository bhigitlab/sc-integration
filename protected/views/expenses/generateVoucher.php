<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/voucherstyle.css">

<?php
$daybook = Expenses::model()->findByPk($id);
?>
<div class="container" style="<?php echo (isset($_GET['export']))?'margin:0px 30px':'' ?>">
    <div class="clearfix">
        <div class="pull-left">
            <h2>DAYBOOK VOUCHER</h2>
        </div>
        <?php if (!isset($_GET['export']) && (!isset($export))) { ?>
            <div class="add-btn pull-right">           
                <a style="cursor:pointer;margin-top:15px;" id="download" class="save_btn" href="<?php echo $this->createAbsoluteUrl('expenses/generateVoucher&export=pdf', array('id' => $id)) ?>">SAVE AS PDF</a> 
                <a data-id="<?php echo $id ?>" id="sendmail" class="pdf_excel sendmail">
                    SEND MAIL
                </a>                                                       
            </div>
        <?php } ?>
    </div>

    <div id="send_form" style="display:none;">
        <div class="panel panel-gray">
            <div class="panel-heading form-head">
                <h3 class="panel-title">Send Mail</h3>
            </div>
            <form id="form_send">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6">
                            <input type="hidden" name="p_id" id="p_id" value="<?php echo $id; ?>">
                            <input type="hidden" name="pdf_path" id="pdf_path" value="">
                            <input type="hidden" name="pdf_name" id="pdf_name" value="">
                            <div class="form-group">
                                <label for="ProjectType_project_type" class="required">To</label>
                                <input id="mail_to" name="mail_to" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <input type="hidden" name="p_id" id="p_id" value="<?php echo $id; ?>">
                            <div class="form-group">
                                <label for="ProjectType_project_type" class="required">Message</label>
                                <textarea id="mail_content" name="mail_content" class="form-control"></textarea>
                            </div>
                        </div>
                        <?php
                        if (!empty($settings['po_email_from'])) {
                        ?>
                            <div class="col-md-6">
                                <input type="checkbox" class="form-check-input change_val " checked='true' id="send_copy" name="send_copy">
                                <label class="form-check-label" for="inclusive">Send copy to (<?php echo $settings['po_email_from']; ?>)
                                </label>
                            </div>
                        <?php
                        }
                        ?>

                        <div class="col-md-6">
                            <label style="display:block;">&nbsp;</label>
                            <button class="btn btn-info btn-sm send_btn">Send</button>
                            <?php echo CHtml::ResetButton('Close', array('onclick' => 'closeaction(this,event);', 'class' => 'btn btn-sm')); ?>
                            <div id="loader"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/ajax-loader.gif"></div>
                        </div>
                    </div>
            </form>
        </div>
    </div>
</div>

<table class="table head_tbl">
    <tbody>
        <tr class="logo_sec">
            <th style="vertical-align:middle">
                <?php echo CHtml::image($this->logo, Yii::app()->name,
                        array('style' => 'max-height:45px;')); ?>
            </th>
            <th></th>
            <th class="text-left">
                <div class="address_block">
                    <?php
                    $company = Company::model()->findBypk(Yii::app()->user->company_id);
                    $address = (isset($company['address']) && !empty($company['address'])) ? ($company['address']) : "";
                    $pin = (isset($company['pincode']) && !empty($company['pincode'])) ? ($company['pincode']) : "";
                    $phone = (isset($company['phone']) && !empty($company['phone'])) ? ( $company['phone']) : "";
                    $email = (isset($company['email_id']) && !empty($company['email_id'])) ? ( $company['email_id']) : "";
                    $gst = "GST NO: " . isset($company["company_gstnum"]) ?
                    $company["company_gstnum"] : '';
                            ?>
                    <div><?php echo $company['name'] ?> , <?php echo $address ?> , <?php echo $pin ?></div>
                    <div>Ph: <?php echo $phone ?></div>
                    <div><?php echo $email ?></div>
                    <div><?php echo $gst ?></div>
                </div>  
            </th>

        </tr>
        <tr>
            <td colspan="3" class="border-bottom-0 text-left">

                <div class="mt-1">RECEIPT VOUCHER</div>

                <br>
                <table style="width: 100%;border: none !important;">
            <tr style="border: none !important;">
                <td style="width: 50%; vertical-align: top;border: none !important;">
                    <div >TO </div>
                    <?php
                    $project = Projects::model()->findByPk($daybook['projectid']);
                    ?>
                    <div class="mt-1 mb-2">&nbsp;&nbsp;<?php echo $project->name; ?><br> </div>
                </td>
                <td style="width: 50%; vertical-align: top;border: none !important;">
                    <div>VENDOR</div>
                    <?php
                    $vendor = Vendors::model()->findByPk($daybook['vendor_id']);
                    $vendor_name='';
                    if(!empty($vendor)){
                        $vendor_name = ucfirst($vendor->name);
                    }
                    ?>
                    <div class="mt-1 mb-2">&nbsp;&nbsp;<?php echo $vendor_name; ?><br> </div>
                </td>
            </tr>
        </table>
            </td>
        </tr>
        <tr class="no_border">
            <th>State : Kerala</th>
            <th>State code : 32</th>
            <th>Date : <?php echo date('Y-m-d'); ?></th>
        </tr>
        <tr>
            <th colspan="2" class="text-left">Particulars</th>
            <th class="text-left">Amount</th>
        </tr><?php //echo "<pre>";print_r($daybook);die; ?>
        <tr style="height: 100px;" class="h_100 border-right-0">
            <th colspan="2" class="border-r0 text-left">Received Amount Details</th>
            <th class="text-left"><?php echo $daybook['date']; ?> - <?php echo Yii::app()->Controller->money_format_inr($daybook['paidamount'], 2, 1); ?> /-</th>
        </tr>
        <tr>
            <td colspan="3" class="border-bottom-0">Received Rupees : <?php echo Yii::app()->Controller->displaywords($daybook['paidamount']); ?>.</td>
        </tr>
        <tr class="">
            <td colspan="2" class="border-r0 border-t0">Receivers Signature</td>
            <td></td>
        </tr>
    </tbody>
</table>
</div>
<script>
    function closeaction() {
        $('#send_form').slideUp(500);
    }

    $('#sendmail').click(function () {
        $('#send_form').slideDown();
    });    

    $('.send_btn').click(function (e) {
        e.preventDefault();
        var data = $('#form_send').serialize();        
        $('#loader').css('display', 'inline-block');
        if ($("#send_copy").prop('checked') == true) {
            var send_copy = 1;
        } else {
            var send_copy = 0;
        }
        var p_id = $('#p_id').val();
        var mail_to = $('#mail_to').val();
        var mail_content = $('#mail_content').val();
        var pdf_path = $('#pdf_path').val();
        var pdf_name = $('#pdf_name').val();
        $('.send_btn').prop('disabled', true)
        $('.loading-overlay').addClass('is-active');
        $.ajax({
            url: '<?php echo Yii::app()->createAbsoluteUrl('expenses/sendattachment&export=mail'); ?>',
            type: 'POST',
            dataType: 'json',
            data: {
                p_id: p_id,
                mail_to: mail_to,
                mail_content: mail_content,
                pdf_path: pdf_path,
                pdf_name: pdf_name,
                send_copy: send_copy
            },
            success: function (response) {
                if (response.status == '1') {
                    $('#loader').hide();
                    $('.send_btn').prop('disabled', false)
                    $('#send_form').slideUp(500);
                    $("#form_send").trigger('reset');
                    $("#errormessage").show()
                            .html('<div class="alert alert-success">' + response.message + '</div>')
                            .fadeOut(10000);
                } else {
                    $('#loader').hide();
                    $('.send_btn').prop('disabled', false)
                    $("#errormessage").show()
                            .html('<div class="alert alert-danger">' + response.message + '</div>')
                            .fadeOut(10000);
                }
            }
        })        
    })

    $(".sendmail").click(function () {
        var p_id = $(this).attr("data-id");
        $('#loading').show();
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: '<?php echo Yii::app()->createAbsoluteUrl('expenses/pdfgeneration'); ?>',
            data: {
                p_id: p_id
            },
            success: function (response) {
                $('#pdf_path').val(response.path);
                $('#pdf_name').val(response.name);
            }
        })
    })
</script>