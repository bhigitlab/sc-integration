<?php
/* @var $this MaterialRequisitionController */
/* @var $data MaterialRequisition */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('requisition_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->requisition_id), array('view', 'id'=>$data->requisition_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('requisition_project_id')); ?>:</b>
	<?php echo CHtml::encode($data->requisition_project_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('requisition_task_id')); ?>:</b>
	<?php echo CHtml::encode($data->requisition_task_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('requisition_no')); ?>:</b>
	<?php echo CHtml::encode($data->requisition_no); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('requisition_item')); ?>:</b>
	<?php echo CHtml::encode($data->requisition_item); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('requisition_unit')); ?>:</b>
	<?php echo CHtml::encode($data->requisition_unit); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('requisition_quantity')); ?>:</b>
	<?php echo CHtml::encode($data->requisition_quantity); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('requisition_date')); ?>:</b>
	<?php echo CHtml::encode($data->requisition_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('requisition_status')); ?>:</b>
	<?php echo CHtml::encode($data->requisition_status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_by')); ?>:</b>
	<?php echo CHtml::encode($data->created_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_date')); ?>:</b>
	<?php echo CHtml::encode($data->created_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_by')); ?>:</b>
	<?php echo CHtml::encode($data->updated_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_date')); ?>:</b>
	<?php echo CHtml::encode($data->updated_date); ?>
	<br />

	*/ ?>

</div>