DROP TABLE `jp_quotation_generator_image`;

CREATE TABLE `jp_quotation_generator_image` (
  `id` int(11) NOT NULL,
  `top_bottom_image` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `type` enum('0','1') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '0' COMMENT '0=>top image ,1=>bottom image',
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

ALTER TABLE `jp_quotation_generator_image`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `jp_quotation_generator_image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

