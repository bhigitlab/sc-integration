
<?php

/**
 * This is the model class for table "{{pre_subcontractor_payment}}".
 *
 * The followings are the available columns in table '{{pre_subcontractor_payment}}':
 * @property integer $payment_id
 * @property integer $ref_id
 * @property integer $followup_id
 * @property integer $record_grop_id
 * @property integer $subcontractor_id
 * @property integer $project_id
 * @property double $amount
 * @property string $description
 * @property string $date
 * @property integer $payment_type
 * @property integer $bank
 * @property string $cheque_no
 * @property double $sgst
 * @property double $sgst_amount
 * @property double $cgst
 * @property double $cgst_amount
 * @property double $igst
 * @property double $igst_amount
 * @property integer $tax_amount
 * @property double $tds
 * @property double $tds_amount
 * @property double $paidamount
 * @property integer $company_id
 * @property integer $reconciliation_status
 * @property string $reconciliation_date
 * @property string $approve_status
 * @property string $uniqueid
 * @property integer $rowcount_slno
 * @property integer $rowcount_total
 * @property double $budget_percentage
 * @property integer $update_status
 * @property string $payment_quotation_status
 * @property integer $quotation_number
 * @property integer $created_by
 * @property string $created_date
 * @property integer $updated_by
 * @property string $updated_date
 * @property integer $delete_status
 * @property string $approval_status
 * @property integer $approved_by
 * @property string $record_action
 * @property string $approve_notify_status
 * @property integer $cancelled_by
 * @property string $cancelled_at
 *
 * The followings are the available model relations:
 * @property Users $approvedBy
 * @property Bank $bank0
 * @property Company $company
 * @property Users $createdBy
 * @property Projects $project
 * @property SubcontractorPayment $ref
 * @property Subcontractor $subcontractor
 */
class PreSubcontractorPayment extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{pre_subcontractor_payment}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('ref_id, record_grop_id, project_id, amount, description, date, payment_type, quotation_number, created_by, created_date, record_action, approve_notify_status', 'required'),
            array('ref_id, followup_id, subcontractor_id, project_id, payment_type, bank, tax_amount, company_id, reconciliation_status, rowcount_slno, rowcount_total, update_status, quotation_number, created_by, updated_by, delete_status, approved_by, cancelled_by', 'numerical', 'integerOnly'=>true),
            array('amount, sgst, sgst_amount, cgst, cgst_amount, igst, igst_amount, tds, tds_amount, paidamount, budget_percentage', 'numerical'),
            array('cheque_no', 'length', 'max'=>100),
            array('approve_status', 'length', 'max'=>3),
            array('uniqueid', 'length', 'max'=>200),
            array('payment_quotation_status, approval_status, approve_notify_status', 'length', 'max'=>1),
            array('record_action', 'length', 'max'=>300),
            array('reconciliation_date', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('payment_id, ref_id, followup_id, record_grop_id, subcontractor_id, project_id, amount, description, date, payment_type, bank, cheque_no, sgst, sgst_amount, cgst, cgst_amount, igst, igst_amount, tax_amount, tds, tds_amount, paidamount, company_id, reconciliation_status, reconciliation_date, approve_status, uniqueid, rowcount_slno, rowcount_total, budget_percentage, update_status, payment_quotation_status, quotation_number, created_by, created_date, updated_by, updated_date, delete_status, approval_status, approved_by, record_action, approve_notify_status, cancelled_by, cancelled_at', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'approvedBy' => array(self::BELONGS_TO, 'Users', 'approved_by'),
            'bank0' => array(self::BELONGS_TO, 'Bank', 'bank'),
            'company' => array(self::BELONGS_TO, 'Company', 'company_id'),
            'createdBy' => array(self::BELONGS_TO, 'Users', 'created_by'),
            'project' => array(self::BELONGS_TO, 'Projects', 'project_id'),
            'ref' => array(self::BELONGS_TO, 'SubcontractorPayment', 'ref_id'),
            'subcontractor' => array(self::BELONGS_TO, 'Subcontractor', 'subcontractor_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'payment_id' => 'Payment',
            'ref_id' => 'Ref',
            'followup_id' => 'Followup',
            'record_grop_id' => 'Record Grop',
            'subcontractor_id' => 'Subcontractor',
            'project_id' => 'Project',
            'amount' => 'Amount',
            'description' => 'Description',
            'date' => 'Date',
            'payment_type' => 'Payment Type',
            'bank' => 'Bank',
            'cheque_no' => 'Cheque No',
            'sgst' => 'Sgst',
            'sgst_amount' => 'Sgst Amount',
            'cgst' => 'Cgst',
            'cgst_amount' => 'Cgst Amount',
            'igst' => 'Igst',
            'igst_amount' => 'Igst Amount',
            'tax_amount' => 'Tax Amount',
            'tds' => 'Tds',
            'tds_amount' => 'Tds Amount',
            'paidamount' => 'Paidamount',
            'company_id' => 'Company',
            'reconciliation_status' => 'Reconciliation Status',
            'reconciliation_date' => 'Reconciliation Date',
            'approve_status' => 'Approve Status',
            'uniqueid' => 'Uniqueid',
            'rowcount_slno' => 'Rowcount Slno',
            'rowcount_total' => 'Rowcount Total',
            'budget_percentage' => 'Budget Percentage',
            'update_status' => 'Update Status',
            'payment_quotation_status' => 'Payment Quotation Status',
            'quotation_number' => 'Quotation Number',
            'created_by' => 'Created By',
            'created_date' => 'Created Date',
            'updated_by' => 'Updated By',
            'updated_date' => 'Updated Date',
            'delete_status' => 'Delete Status',
            'approval_status' => '    0=>Pending,1=>Approved,2=>Rejected',
            'approved_by' => 'Approved By',
            'record_action' => 'Record Action',
            'approve_notify_status' => '0=>no.not visible to admin,1=>yes visible to admin',
            'cancelled_by' => 'Cancelled By',
            'cancelled_at' => 'Cancelled At',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('payment_id',$this->payment_id);
        $criteria->compare('ref_id',$this->ref_id);
        $criteria->compare('followup_id',$this->followup_id);
        $criteria->compare('record_grop_id',$this->record_grop_id);
        $criteria->compare('subcontractor_id',$this->subcontractor_id);
        $criteria->compare('project_id',$this->project_id);
        $criteria->compare('amount',$this->amount);
        $criteria->compare('description',$this->description,true);
        $criteria->compare('date',$this->date,true);
        $criteria->compare('payment_type',$this->payment_type);
        $criteria->compare('bank',$this->bank);
        $criteria->compare('cheque_no',$this->cheque_no,true);
        $criteria->compare('sgst',$this->sgst);
        $criteria->compare('sgst_amount',$this->sgst_amount);
        $criteria->compare('cgst',$this->cgst);
        $criteria->compare('cgst_amount',$this->cgst_amount);
        $criteria->compare('igst',$this->igst);
        $criteria->compare('igst_amount',$this->igst_amount);
        $criteria->compare('tax_amount',$this->tax_amount);
        $criteria->compare('tds',$this->tds);
        $criteria->compare('tds_amount',$this->tds_amount);
        $criteria->compare('paidamount',$this->paidamount);
        $criteria->compare('company_id',$this->company_id);
        $criteria->compare('reconciliation_status',$this->reconciliation_status);
        $criteria->compare('reconciliation_date',$this->reconciliation_date,true);
        $criteria->compare('approve_status',$this->approve_status,true);
        $criteria->compare('uniqueid',$this->uniqueid,true);
        $criteria->compare('rowcount_slno',$this->rowcount_slno);
        $criteria->compare('rowcount_total',$this->rowcount_total);
        $criteria->compare('budget_percentage',$this->budget_percentage);
        $criteria->compare('update_status',$this->update_status);
        $criteria->compare('payment_quotation_status',$this->payment_quotation_status,true);
        $criteria->compare('quotation_number',$this->quotation_number);
        $criteria->compare('created_by',$this->created_by);
        $criteria->compare('created_date',$this->created_date,true);
        $criteria->compare('updated_by',$this->updated_by);
        $criteria->compare('updated_date',$this->updated_date,true);
        $criteria->compare('delete_status',$this->delete_status);
        $criteria->compare('approval_status',$this->approval_status,true);
        $criteria->compare('approved_by',$this->approved_by);
        $criteria->compare('record_action',$this->record_action,true);
        $criteria->compare('approve_notify_status',$this->approve_notify_status,true);
        $criteria->compare('cancelled_by',$this->cancelled_by);
        $criteria->compare('cancelled_at',$this->cancelled_at,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return PreSubcontractorPayment the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}