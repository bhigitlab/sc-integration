<?php
/* @var $this UsersController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'Users',
);

$this->menu = array(
    array('label' => 'Create User', 'url' => array('create')),
);
?>

<h1>Users List</h1>

<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'users-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        array('class' => 'IndexColumn', 'header' => 'Sl.No.',),
        array('name'=>'userid', 'htmlOptions' => array('width' => '30px','style'=>'font-weight: bold')),
        'first_name',
        'last_name',
        'username',
        array(
            'name' => 'user_type',
            'value' => '$data->userType->role',
            'type' => 'raw',
            'filter' => CHtml::listData(UserRoles::model()->findAll(
                            array(
                                'select' => array('id,role'),
                                'order' => 'role',
                                'distinct' => true
                    )), "id", "role")
        ),
        'email',
		'phonenumber',
         array(
            'name' => 'reporting_person',
            'type' => 'raw',
            'value' => '($data->reporting_person===null?"--":$data->reportingPerson->first_name." ".$data->reportingPerson->last_name)',
         ),
        array(
            'name' => 'status',
            'value' => '($data->status==0?"<b>Active</b>":"Inactive")',
            'type' => 'raw',
            'filter' => array('Active', 'Inactive')

        ),
        array(
            'class' => 'CButtonColumn',
            'template'=>'{update}{delete}',
        ),
    ),
));
?>
