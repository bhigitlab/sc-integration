
ALTER TABLE `jp_sales_quotation_gallery` ADD `category_id` INT(11) NULL DEFAULT NULL AFTER `caption`, ADD `worktype_id` INT(11) NULL DEFAULT NULL AFTER `category_id`, ADD `rev_no` VARCHAR(120) NULL DEFAULT NULL AFTER `worktype_id`, ADD `created_date` DATETIME NULL DEFAULT NULL AFTER `rev_no`, ADD `created_by` INT(11) NULL DEFAULT NULL AFTER `created_date`;
ALTER TABLE `jp_sales_quotation_gallery` ADD `sales_quotation_id` INT(11) NULL DEFAULT NULL AFTER `worktype_id`;
ALTER TABLE `jp_sales_quotation` ADD `image_gallery_id` INT(11) NULL DEFAULT NULL AFTER `item_name`;
