
<?php

/**
 * This is the model class for table "{{pre_dailyexpenses}}".
 *
 * The followings are the available columns in table '{{pre_dailyexpenses}}':
 * @property integer $dailyexp_id
 * @property integer $ref_id
 * @property integer $followup_id
 * @property string $record_grop_id
 * @property string $date
 * @property integer $exp_type_id
 * @property string $bill_id
 * @property integer $expensehead_id
 * @property integer $expense_type
 * @property integer $vendor_id
 * @property double $dailyexpense_amount
 * @property double $dailyexpense_sgstp
 * @property double $dailyexpense_sgst
 * @property double $dailyexpense_cgstp
 * @property double $dailyexpense_cgst
 * @property double $dailyexpense_igstp
 * @property double $dailyexpense_igst
 * @property double $amount
 * @property string $description
 * @property integer $dailyexpense_receipt_type
 * @property integer $dailyexpense_receipt_head
 * @property double $dailyexpense_receipt
 * @property integer $dailyexpense_purchase_type
 * @property double $dailyexpense_paidamount
 * @property integer $bank_id
 * @property string $dailyexpense_chequeno
 * @property integer $user_id
 * @property integer $created_by
 * @property string $created_date
 * @property integer $updated_by
 * @property string $updated_date
 * @property string $data_entry
 * @property integer $exp_type
 * @property string $dailyexpense_type
 * @property integer $company_id
 * @property integer $reconciliation_status
 * @property string $reconciliation_date
 * @property integer $employee_id
 * @property string $display_flg
 * @property integer $transaction_parent
 * @property string $parent_status
 * @property integer $expensehead_type
 * @property integer $transfer_parentid
 * @property integer $update_status
 * @property integer $delete_status
 * @property string $approval_status
 * @property integer $approved_by
 * @property string $record_action
 * @property string $approve_notify_status
 * @property integer $cancelled_by
 * @property string $cancelled_at
 */
class PreDailyexpenses extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{pre_dailyexpenses}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('ref_id, record_grop_id, date, amount, description, user_id, created_by, created_date, exp_type, record_action', 'required'),
            array('ref_id, followup_id, exp_type_id, expensehead_id, expense_type, vendor_id, dailyexpense_receipt_type, dailyexpense_receipt_head, dailyexpense_purchase_type, bank_id, user_id, created_by, updated_by, exp_type, company_id, reconciliation_status, employee_id, transaction_parent, expensehead_type, transfer_parentid, update_status, delete_status, approved_by, cancelled_by', 'numerical', 'integerOnly'=>true),
            array('dailyexpense_amount, dailyexpense_sgstp, dailyexpense_sgst, dailyexpense_cgstp, dailyexpense_cgst, dailyexpense_igstp, dailyexpense_igst, amount, dailyexpense_receipt, dailyexpense_paidamount', 'numerical'),
            array('bill_id, dailyexpense_chequeno', 'length', 'max'=>100),
            array('description, record_action', 'length', 'max'=>300),
            array('data_entry', 'length', 'max'=>30),
            array('dailyexpense_type', 'length', 'max'=>7),
            array('display_flg', 'length', 'max'=>3),
            array('parent_status, approval_status, approve_notify_status', 'length', 'max'=>1),
            array('updated_date, reconciliation_date', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('dailyexp_id, ref_id, followup_id, record_grop_id, date, exp_type_id, bill_id, expensehead_id, expense_type, vendor_id, dailyexpense_amount, dailyexpense_sgstp, dailyexpense_sgst, dailyexpense_cgstp, dailyexpense_cgst, dailyexpense_igstp, dailyexpense_igst, amount, description, dailyexpense_receipt_type, dailyexpense_receipt_head, dailyexpense_receipt, dailyexpense_purchase_type, dailyexpense_paidamount, bank_id, dailyexpense_chequeno, user_id, created_by, created_date, updated_by, updated_date, data_entry, exp_type, dailyexpense_type, company_id, reconciliation_status, reconciliation_date, employee_id, display_flg, transaction_parent, parent_status, expensehead_type, transfer_parentid, update_status, delete_status, approval_status, approved_by, record_action, approve_notify_status, cancelled_by, cancelled_at', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'dailyexp_id' => 'Dailyexp',
            'ref_id' => 'Ref',
            'followup_id' => 'Followup',
            'record_grop_id' => 'Record Grop',
            'date' => 'Date',
            'exp_type_id' => 'company expense type',
            'bill_id' => 'Bill',
            'expensehead_id' => 'Expensehead',
            'expense_type' => 'Expense Type',
            'vendor_id' => 'Vendor',
            'dailyexpense_amount' => 'Dailyexpense Amount',
            'dailyexpense_sgstp' => 'Dailyexpense Sgstp',
            'dailyexpense_sgst' => 'Dailyexpense Sgst',
            'dailyexpense_cgstp' => 'Dailyexpense Cgstp',
            'dailyexpense_cgst' => 'Dailyexpense Cgst',
            'dailyexpense_igstp' => 'Dailyexpense Igstp',
            'dailyexpense_igst' => 'Dailyexpense Igst',
            'amount' => 'Amount',
            'description' => 'Description',
            'dailyexpense_receipt_type' => 'Dailyexpense Receipt Type',
            'dailyexpense_receipt_head' => 'Dailyexpense Receipt Head',
            'dailyexpense_receipt' => 'Dailyexpense Receipt',
            'dailyexpense_purchase_type' => 'Dailyexpense Purchase Type',
            'dailyexpense_paidamount' => 'Dailyexpense Paidamount',
            'bank_id' => 'Bank',
            'dailyexpense_chequeno' => 'Dailyexpense Chequeno',
            'user_id' => 'User',
            'created_by' => 'Created By',
            'created_date' => 'Created Date',
            'updated_by' => 'Updated By',
            'updated_date' => 'Updated Date',
            'data_entry' => 'Data Entry',
            'exp_type' => 'Exp Type',
            'dailyexpense_type' => 'Dailyexpense Type',
            'company_id' => 'Company',
            'reconciliation_status' => 'Reconciliation Status',
            'reconciliation_date' => 'Reconciliation Date',
            'employee_id' => 'Employee',
            'display_flg' => 'Display Flg',
            'transaction_parent' => 'Transaction Parent',
            'parent_status' => 'Parent Status',
            'expensehead_type' => 'Expensehead Type',
            'transfer_parentid' => 'Transfer Parentid',
            'update_status' => 'Update Status',
            'delete_status' => 'Delete Status',
            'approval_status' => '0=>Pending,1=>Approved,2=>Rejected    ',
            'approved_by' => 'Approved By',
            'record_action' => 'Record Action',
            'approve_notify_status' => '0=>no.not visible to admin,1=>yes visible to admin',
            'cancelled_by' => 'Cancelled By',
            'cancelled_at' => 'Cancelled At',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('dailyexp_id',$this->dailyexp_id);
        $criteria->compare('ref_id',$this->ref_id);
        $criteria->compare('followup_id',$this->followup_id);
        $criteria->compare('record_grop_id',$this->record_grop_id,true);
        $criteria->compare('date',$this->date,true);
        $criteria->compare('exp_type_id',$this->exp_type_id);
        $criteria->compare('bill_id',$this->bill_id,true);
        $criteria->compare('expensehead_id',$this->expensehead_id);
        $criteria->compare('expense_type',$this->expense_type);
        $criteria->compare('vendor_id',$this->vendor_id);
        $criteria->compare('dailyexpense_amount',$this->dailyexpense_amount);
        $criteria->compare('dailyexpense_sgstp',$this->dailyexpense_sgstp);
        $criteria->compare('dailyexpense_sgst',$this->dailyexpense_sgst);
        $criteria->compare('dailyexpense_cgstp',$this->dailyexpense_cgstp);
        $criteria->compare('dailyexpense_cgst',$this->dailyexpense_cgst);
        $criteria->compare('dailyexpense_igstp',$this->dailyexpense_igstp);
        $criteria->compare('dailyexpense_igst',$this->dailyexpense_igst);
        $criteria->compare('amount',$this->amount);
        $criteria->compare('description',$this->description,true);
        $criteria->compare('dailyexpense_receipt_type',$this->dailyexpense_receipt_type);
        $criteria->compare('dailyexpense_receipt_head',$this->dailyexpense_receipt_head);
        $criteria->compare('dailyexpense_receipt',$this->dailyexpense_receipt);
        $criteria->compare('dailyexpense_purchase_type',$this->dailyexpense_purchase_type);
        $criteria->compare('dailyexpense_paidamount',$this->dailyexpense_paidamount);
        $criteria->compare('bank_id',$this->bank_id);
        $criteria->compare('dailyexpense_chequeno',$this->dailyexpense_chequeno,true);
        $criteria->compare('user_id',$this->user_id);
        $criteria->compare('created_by',$this->created_by);
        $criteria->compare('created_date',$this->created_date,true);
        $criteria->compare('updated_by',$this->updated_by);
        $criteria->compare('updated_date',$this->updated_date,true);
        $criteria->compare('data_entry',$this->data_entry,true);
        $criteria->compare('exp_type',$this->exp_type);
        $criteria->compare('dailyexpense_type',$this->dailyexpense_type,true);
        $criteria->compare('company_id',$this->company_id);
        $criteria->compare('reconciliation_status',$this->reconciliation_status);
        $criteria->compare('reconciliation_date',$this->reconciliation_date,true);
        $criteria->compare('employee_id',$this->employee_id);
        $criteria->compare('display_flg',$this->display_flg,true);
        $criteria->compare('transaction_parent',$this->transaction_parent);
        $criteria->compare('parent_status',$this->parent_status,true);
        $criteria->compare('expensehead_type',$this->expensehead_type);
        $criteria->compare('transfer_parentid',$this->transfer_parentid);
        $criteria->compare('update_status',$this->update_status);
        $criteria->compare('delete_status',$this->delete_status);
        $criteria->compare('approval_status',$this->approval_status,true);
        $criteria->compare('approved_by',$this->approved_by);
        $criteria->compare('record_action',$this->record_action,true);
        $criteria->compare('approve_notify_status',$this->approve_notify_status,true);
        $criteria->compare('cancelled_by',$this->cancelled_by);
        $criteria->compare('cancelled_at',$this->cancelled_at,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return PreDailyexpenses the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}