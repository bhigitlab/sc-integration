<?php

/**
 * This is the model class for table "{{warehousedespatch}}".
 *
 * The followings are the available columns in table '{{warehousedespatch}}':
 * @property integer $warehousedespatch_id
 * @property string $warehousedespatch_no
 * @property string $warehousedespatch_date
 * @property string $warehousedespatch_vendor
 * @property integer $warehousedespatch_warehouseid
 * @property integer $warehousedespatch_clerk
 * @property double $warehousedespatch_quantity
 * @property string $warehousedespatch_through
 * @property integer $company_id
 * @property integer $created_by
 * @property string $created_date
 * @property integer $updated_by
 * @property string $updated_date
 */
class Warehousedespatch extends CActiveRecord
{

        public $date_from;
	public $date_to;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{warehousedespatch}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('warehousedespatch_no, warehousedespatch_date, warehousedespatch_vendor, warehousedespatch_warehouseid, warehousedespatch_clerk, warehousedespatch_through, company_id, created_by, created_date, updated_by, updated_date,warehouse_eta_date', 'required'),
			array('warehousedespatch_warehouseid, warehousedespatch_clerk, company_id, created_by, updated_by', 'numerical', 'integerOnly'=>true),
			array('warehousedespatch_quantity', 'numerical'),
			array('warehousedespatch_no, warehousedespatch_vendor', 'length', 'max'=>255),
			array('warehousedespatch_through', 'length', 'max'=>200),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('warehousedespatch_id, warehousedespatch_no, warehousedespatch_date, warehousedespatch_vendor, warehousedespatch_warehouseid, warehousedespatch_clerk, warehousedespatch_quantity, warehousedespatch_through, company_id, created_by, created_date, updated_by, updated_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'createdBy' => array(self::BELONGS_TO, 'Users', 'created_by'),
					'warehouse' => array(self::BELONGS_TO, 'Warehouse', 'warehousedespatch_warehouseid'),
					'warehousedespatchWarehouseidTo' => array(self::BELONGS_TO, 'Warehouse', 'warehousedespatch_warehouseid_to'),
                    'vendor' => array(self::BELONGS_TO, 'Vendors', 'warehousedespatch_vendor'),
					'clerk' => array(self::BELONGS_TO, 'Users', 'warehousedespatch_clerk'),
					'project' => array(self::BELONGS_TO, 'Projects', 'warehousedespatch_clerk'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'warehousedespatch_id' => 'Warehousedespatch',
			'warehousedespatch_no' => 'Warehousedespatch No',
			'warehousedespatch_date' => 'Warehousedespatch Date',
			'warehousedespatch_vendor' => 'Warehousedespatch Vendor',
			'warehousedespatch_project' => 'Warehousedespatch Project',
			'warehousedespatch_warehouseid' => 'Warehousedespatch Warehouseid',
			'warehousedespatch_clerk' => 'Warehousedespatch Clerk',
			'warehousedespatch_quantity' => 'Warehousedespatch Quantity',
			'warehousedespatch_through' => 'Warehousedespatch Through',
			'company_id' => 'Company',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
			'updated_by' => 'Updated By',
			'updated_date' => 'Updated Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;
		$criteria->join = 'LEFT JOIN jp_warehousereceipt wr ON wr.warehousereceipt_despatch_id=t.warehousedespatch_id';

		$criteria->compare('warehousedespatch_id', $this->warehousedespatch_id);
		$criteria->compare('warehousedespatch_no', $this->warehousedespatch_no, true);
		$criteria->compare('warehousedespatch_date', $this->warehousedespatch_date, true);
		$criteria->compare('warehousedespatch_warehouseid_to', $this->warehousedespatch_warehouseid_to, true);
		$criteria->compare('warehousedespatch_vendor', $this->warehousedespatch_vendor, true);
		$criteria->compare('warehousedespatch_project', $this->warehousedespatch_project, true);
		$criteria->compare('warehousedespatch_warehouseid', $this->warehousedespatch_warehouseid);
		$criteria->compare('warehousedespatch_clerk', $this->warehousedespatch_clerk);
		$criteria->compare('warehousedespatch_quantity', $this->warehousedespatch_quantity);
		$criteria->compare('warehousedespatch_through', $this->warehousedespatch_through, true);
		$criteria->compare('company_id', $this->company_id);
		$criteria->compare('created_by', $this->created_by);
		$criteria->compare('created_date', $this->created_date, true);
		$criteria->compare('updated_by', $this->updated_by);
		$criteria->compare('updated_date', $this->updated_date, true);
		$criteria->addCondition("t.company_id =" . Yii::app()->user->company_id . "");

		if (!empty($this->date_from) && empty($this->date_to)) {
			$criteria->addCondition("warehousedespatch_date >= '" . date('Y-m-d', strtotime($this->date_from)) . "'");  // date is database date column field
		} else if (!empty($this->date_to) && empty($this->date_from)) {
			$criteria->addCondition("warehousedespatch_date <= '" . date('Y-m-d', strtotime($this->date_to)) . "'");
		} else if (!empty($this->date_to) && !empty($this->date_from)) {
			$criteria->addCondition("warehousedespatch_date between '" . date('Y-m-d', strtotime($this->date_from)) . "' and  '" . date('Y-m-d', strtotime($this->date_to)) . "'");
		}

		if (Yii::app()->user->role != 1) {
			$assigned = Warehouse::model()->findAll(array('condition' => 'FIND_IN_SET(' . Yii::app()->user->id . ', assigned_to)'));
			if (!empty($assigned)) {
				$id_values = '';
				foreach ($assigned as $key => $value) {
					$arrVal = explode(',', $value['warehouse_id']);
					$newQuery = "";
					foreach ($arrVal as $arr) {
						$id_values .= $arr . ',';
						//   if ($newQuery) $newQuery .= ' OR';
						//   $newQuery .= " FIND_IN_SET('".$arr."', warehousedespatch_warehouseid)";
					}
					//   $criteria->addCondition($newQuery);
				}
				$id_values = rtrim($id_values, ',');
				$newQuery = "warehousedespatch_warehouseid IN (" . $id_values . ")";
				$criteria->addCondition($newQuery);
			} else {
				$criteria->addCondition("warehousedespatch_warehouseid IS NULL");
			}
		}
		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'sort' => array('defaultOrder' => 'warehousedespatch_id DESC',),
			'pagination' => array('pageSize' => 10,),
		));
	}
	public function projectname($project_id)
	{
		$tblpx = Yii::app()->db->tablePrefix;
		$sql = "SELECT * FROM {$tblpx}projects WHERE pid=" . $project_id;
		$projects = Yii::app()->db->createCommand($sql)->queryRow();
		return $projects['name'];
	}

	public function transferreport()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('warehousedespatch_id', $this->warehousedespatch_id);
		$criteria->compare('warehousedespatch_no', $this->warehousedespatch_no, true);
		$criteria->compare('warehousedespatch_date', $this->warehousedespatch_date, true);
		$criteria->compare('warehousedespatch_warehouseid_to', $this->warehousedespatch_warehouseid_to, true);
		$criteria->compare('warehousedespatch_vendor', $this->warehousedespatch_vendor, true);
		$criteria->compare('warehousedespatch_project', $this->warehousedespatch_project, true);
		$criteria->compare('warehousedespatch_warehouseid', $this->warehousedespatch_warehouseid);
		$criteria->compare('warehousedespatch_clerk', $this->warehousedespatch_clerk);
		$criteria->compare('warehousedespatch_quantity', $this->warehousedespatch_quantity);
		$criteria->compare('warehousedespatch_through', $this->warehousedespatch_through, true);
		$criteria->compare('company_id', $this->company_id);
		$criteria->compare('created_by', $this->created_by);
		$criteria->compare('created_date', $this->created_date, true);
		$criteria->compare('updated_by', $this->updated_by);
		$criteria->compare('updated_date', $this->updated_date, true);
		$criteria->addCondition("company_id =" . Yii::app()->user->company_id . "");

		if (!empty($this->date_from) && empty($this->date_to)) {
			$criteria->addCondition("warehousedespatch_date >= '" . date('Y-m-d', strtotime($this->date_from)) . "'");  // date is database date column field
		} else if (!empty($this->date_to) && empty($this->date_from)) {
			$criteria->addCondition("warehousedespatch_date <= '" . date('Y-m-d', strtotime($this->date_to)) . "'");
		} else if (!empty($this->date_to) && !empty($this->date_from)) {
			$criteria->addCondition("warehousedespatch_date between '" . date('Y-m-d', strtotime($this->date_from)) . "' and  '" . date('Y-m-d', strtotime($this->date_to)) . "'");
		}



		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'sort' => array('defaultOrder' => 'warehousedespatch_id DESC',),
			'pagination' => array('pageSize' => 10,),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Warehousedespatch the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}
}
