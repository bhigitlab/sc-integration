<?php
/* @var $this BankController */
/* @var $model Bank */
/* @var $form CActiveForm */
?>

<div class="pull-left">
	
	<div class="">
		<?php $form=$this->beginWidget('CActiveForm', array(
			'action'=>Yii::app()->createUrl($this->route),
			'method'=>'get',
		)); ?>
		<?php echo $form->label($model,'Filter By'); ?>
		<?php echo $form->textField($model,'bank_name',array('size'=>30,'maxlength'=>100,'placeholder' => 'Name')); ?>

			<?php echo CHtml::submitButton('Go'); ?>

		<?php $this->endWidget(); ?>
	</div>
</div>