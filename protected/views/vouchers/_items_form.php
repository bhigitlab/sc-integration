<?php $form = $this->beginWidget('CActiveForm', array(
    'id' => 'vouchers-items-form',
    'action' => Yii::app()->createUrl('vouchers/additems'),
    // 'enableAjaxValidation' => true,
    'enableClientValidation' => true,
    'clientOptions' => array('validateOnSubmit' => true),
)); ?>
<input type="hidden" name="VoucherItems[id]" id="VoucherItems_id" value="<?php echo $items_model->id; ?>">
<input type="hidden" name="VoucherItems[voucher_id]" id="VoucherItems_voucher_id" value="<?php echo $model->id; ?>">
<h4 class="section_title">Work Details</h4>
<div class="row daybook-inner">
    <div class="col-md-2 for_sc">
        <div class="form-group">
            <?php echo $form->labelEx($items_model, 'worker_label'); ?>
            <?php echo $form->textField($items_model, 'worker_label', array('size' => 60, 'maxlength' => 100, 'class' => 'form-control')); ?>
            <?php echo $form->error($items_model, 'worker_label'); ?>
        </div>
    </div>

    <div class="col-md-2  for_sc">
        <div class="form-group">
            <?php echo $form->labelEx($items_model, 'no_workers'); ?>
            <?php echo $form->textField($items_model, 'no_workers', array('class' => 'form-control')); ?>
            <?php echo $form->error($items_model, 'no_workers'); ?>
        </div>
    </div>

    <div class="col-md-2">
        <div class="form-group">
            <?php echo $form->labelEx($items_model, 'working_time_size'); ?>
            <?php echo $form->textField($items_model, 'working_time_size', array('size' => 60, 'maxlength' => 100, 'class' => 'form-control')); ?>
            <?php echo $form->error($items_model, 'working_time_size'); ?>
        </div>
    </div>

    <div class="col-md-2">
        <div class="form-group">
            <?php echo $form->labelEx($items_model, 'net_quantity'); ?>
            <?php echo $form->textField($items_model, 'net_quantity', array('size' => 60, 'maxlength' => 100, 'class' => 'form-control')); ?>
            <?php echo $form->error($items_model, 'net_quantity'); ?>
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group">
            <?php echo $form->labelEx($items_model, 'unit'); ?>
            <?php
            echo $form->dropDownList($items_model, 'unit', CHtml::listData(Unit::model()->findAll(), 'id', 'unit_name'), array('class' => 'form-control js-example-basic-single', 'empty' => '-Select Unit-','style'=>'width:100%'));
            ?>
            <?php echo $form->error($items_model, 'unit'); ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <?php echo $form->labelEx($items_model, 'description'); ?>
            <?php echo $form->textArea($items_model, 'description', array('rows' => 4, 'cols' => 50, 'class' => 'form-control')); ?>
            <?php echo $form->error($items_model, 'description'); ?>
        </div>
    </div>
</div>

<div class="submit-button text-right">    
    <?php echo CHtml::submitButton($items_model->isNewRecord ? 'Add' : 'Update', array('class' => 'btn btn-info', 'id' => 'submit_item')); ?>
</div>

<?php $this->endWidget(); ?>
