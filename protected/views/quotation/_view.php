<?php
if ($index == 0) {
?>
    <thead class="entry-table">
        <tr>
            <?php
            if ((isset(Yii::app()->user->role) && ((in_array('/quotation/updatequotation', Yii::app()->user->menuauthlist)) || (in_array('/quotation/viewquotation', Yii::app()->user->menuauthlist))))) {
            ?>
                <th></th>
            <?php } ?>
            <th>SI No</th>
            <th>Project</th>
            <th>Quotation No</th>
            <th>Company</th>
            <th>Client</th>
            <th>Date</th>            
            <th>Total Project Cost </th>
            
        </tr>
    </thead>
    <?php } 
    $style="";
    $title="";
    if($data['delete_approve_status']==1){
        $style="style='background-color:#f8cbcb !important'";
        $title="Delete Request Sent!" ;
    }
    ?>

<tr <?php echo $style ?> title="<?php echo $title ?>">

<?php
    if ((isset(Yii::app()->user->role) && ((in_array('/quotation/updatequotation', Yii::app()->user->menuauthlist)) || (in_array('/quotation/viewquotation', Yii::app()->user->menuauthlist))))) {
    ?>
        <td>
            <span class="icon icon-options-vertical popover-test" data-toggle="popover" data-placement="right" type="button" data-html="true" style="cursor: pointer;"></span>
            <div class="popover-content hide">
                <ul class="tooltip-hiden">
                    <?php
                    if ((in_array('/quotation/viewquotation', Yii::app()->user->menuauthlist))) {
                    ?>
                        <li><a href="<?php echo Yii::app()->createAbsoluteUrl('quotation/viewquotation', array('qtid' => $data->quotation_id)); ?>" class="btn btn-xs btn-default viewExpense">View</a></li>
                    <?php } ?>
                    <?php
                    if ((in_array('/quotation/updatequotation', Yii::app()->user->menuauthlist))) {
                    ?>
                        <li><a href="<?php echo Yii::app()->createAbsoluteUrl('quotation/updatequotation', array('qtid' => $data->quotation_id)); ?>" class="btn btn-xs btn-default editExpense">Edit</a></li>
                    <?php } ?>
                    <?php if($data['delete_approve_status']==0){ ?>
                    <li>
                        <button class="btn btn-xs btn-default deletequotation" data-id="<?php echo $data->quotation_id ?>">Delete</button>                        
                    </li>
                    <?php } else{ ?>
                        <li><i class="fa fa-exclamation text-danger" title ="<?php echo $title ?>"></i></li> 
                    <?php } ?>
                </ul>
            </div>
        </td>
    <?php } ?>
    <td><?php echo $index + 1; ?></td>
    <td>
        <?php
        $project = Projects::model()->findBypk($data->project_id);
        echo $project['name'];
        ?>
    </td>
    <td><?php echo $data->inv_no; ?></td>
    <td>
        <?php
        $company = Company::model()->findBypk($data->company_id);
        echo $company['name'];
        ?>
    </td>
    <?php
    $id = $data['quotation_id'];
    $tblpx = Yii::app()->db->tablePrefix;
    $query = Yii::app()->db->createCommand("select * from {$tblpx}quotation_list where qt_id=$id")->queryAll();
    $id = $data['client_id'];
    if ($id != '') {
        $sql = Yii::app()->db->createCommand("select {$tblpx}clients.name from {$tblpx}clients
			 left join {$tblpx}projects ON {$tblpx}projects.client_id={$tblpx}clients.cid
			 where {$tblpx}clients.cid=$id")->queryRow();

        $clientname = $sql['name'];
    } else {
        $pid = $data['project_id'];
        $pmodel = Projects::model()->findBypk($pid);
        $clientname = $pmodel->client->name;
    }
    ?>
    <td><?php echo $clientname; ?></td>
    <td><?php echo date("d-m-Y", strtotime($data->date)); ?></td>    
    <td class="text-right"><?php echo Yii::app()->Controller->money_format_inr(($data->total_project_cost), 2, 1); ?></td>

</tr>
