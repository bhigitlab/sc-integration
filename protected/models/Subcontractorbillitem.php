<?php

/**
 * This is the model class for table "{{subcontractorbillitem}}".
 *
 * The followings are the available columns in table '{{subcontractorbillitem}}':
 * @property integer $id
 * @property integer $bill_id
 * @property integer $quotationitem_id
 * @property string $description
 * @property integer $amount
 * @property double $tax_slab
 * @property double $cgstp
 * @property double $cgst
 * @property double $sgstp
 * @property double $sgst
 * @property double $igstp
 * @property double $igst
 * @property double $tax_amount
 * @property double $total_amount
 * @property string $created_date
 * @property string $updated_date
 */
class Subcontractorbillitem extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{subcontractorbillitem}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('bill_id, quotationitem_id, description, amount, total_amount, created_date', 'required'),
			array('bill_id, quotationitem_id, amount', 'numerical', 'integerOnly'=>true),
			array('tax_slab, cgstp, cgst, sgstp, sgst, igstp, igst, tax_amount, total_amount', 'numerical'),
			array('description', 'length', 'max'=>300),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, bill_id, quotationitem_id, description, amount, tax_slab, cgstp, cgst, sgstp, sgst, igstp, igst, tax_amount, total_amount, created_date, updated_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'bill_id' => 'Bill',
			'quotationitem_id' => 'Quotationitem',
			'description' => 'Description',
			'amount' => 'Amount',
			'tax_slab' => 'Tax Slab',
			'cgstp' => 'Cgstp',
			'cgst' => 'Cgst',
			'sgstp' => 'Sgstp',
			'sgst' => 'Sgst',
			'igstp' => 'Igstp',
			'igst' => 'Igst',
			'tax_amount' => 'Tax Amount',
			'total_amount' => 'Total Amount',
			'created_date' => 'Created Date',
                        'updated_date' => 'Created Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('bill_id',$this->bill_id);
		$criteria->compare('quotationitem_id',$this->quotationitem_id);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('amount',$this->amount);
		$criteria->compare('tax_slab',$this->tax_slab);
		$criteria->compare('cgstp',$this->cgstp);
		$criteria->compare('cgst',$this->cgst);
		$criteria->compare('sgstp',$this->sgstp);
		$criteria->compare('sgst',$this->sgst);
		$criteria->compare('igstp',$this->igstp);
		$criteria->compare('igst',$this->igst);
		$criteria->compare('tax_amount',$this->tax_amount);
		$criteria->compare('total_amount',$this->total_amount);
		$criteria->compare('created_date',$this->created_date,true);
                $criteria->compare('updated_date',$this->updated_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                        'pagination'=>false,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Subcontractorbillitem the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
