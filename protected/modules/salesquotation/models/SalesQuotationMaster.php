<?php

/**
 * This is the model class for table "{{sales_quotation_master}}".
 *
 * The followings are the available columns in table '{{sales_quotation_master}}':
 * @property integer $id
 * @property integer $company_id
 * @property integer $client_name
 * @property integer $address
 * @property integer $phone_no
 * @property integer $email
 * @property string $date_quotation
 * @property integer $invoice_no
 * @property integer $house_name
 * @property integer $project_type
 * @property integer site_address
 * @property integer $created_by
 * @property string $created_date
 */
class SalesQuotationMaster extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return '{{sales_quotation_master}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('company_id,date_quotation, invoice_no, created_by, created_date,location_id,project_type', 'required'),
            array('company_id, created_by', 'numerical', 'integerOnly' => true),
            array('site_address','length','max'=>300),
            array('house_name','length','max'=>200),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('email','email'),
            array('phone_no','match', 'pattern' => '{^\+?[0-9-]+$}',
            'message' => 'Invalid phone number'),
            array('id, company_id,client_name,address,phone_no,email,date_quotation, invoice_no, created_by, created_date', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'company_id' => 'Company',
            'client_name' => 'Client Name',
            'address' => 'Project Address',
            'phone_no' => 'Phone No',
            'email' => 'Email',
            'date_quotation' => 'Date Quotation',
            'invoice_no' => 'Quotation No',
            'created_by' => 'Created By',
            'created_date' => 'Created Date',
            'sales_executive_id' => 'Sales Executive',
            'location_id'=>'Location',
            'house_name'=>'Flat No./House no./House Name',
            'site_address'=>'Site Address',
            'project_type'=>'Project Type'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('company_id', $this->company_id);
        $criteria->compare('date_quotation', $this->date_quotation, true);
        $criteria->compare('invoice_no', $this->invoice_no);
        $criteria->compare('client_name', $this->client_name, true);
        $criteria->compare('phone_no', $this->phone_no, true);
        $criteria->compare('address', $this->address, true);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('project_type', $this->project_type, true);
        $criteria->compare('site_address', $this->site_address, true);
        $criteria->compare('house_name', $this->house_name, true);
        $criteria->compare('created_date', $this->created_date, true);
        $criteria->compare('created_by', $this->created_by);
        $criteria->compare('created_date', $this->created_date, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return SalesQuotationMaster the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function getCategories($id) {
        $items = QuotationCategoryMaster::model()->findAllByAttributes(array('id' => $id));
        return $items;
    }

    public function getQuotationdetails($qid, $type) {
        if ($qid != '') {
            if (1 == $type) {
                $salesquotation = SalesQuotation::model()->findAll(array(
                    'condition' => 'master_id = ' . $qid . ' AND work_type IS NOT NULL'
                    . ' AND deleted_status IN(1,2) AND revision_status = 0 and sub_status=0', "order" => "revision_no  DESC"
                ));
                $sub_items = SalesQuotation::model()->findAll(array(
                    'condition' => 'master_id = ' . $qid . ' AND work_type IS NOT NULL'
                    . ' AND deleted_status IN(1,2) AND revision_status = 0 and sub_status=1', "order" => "revision_no  DESC"
                ));
                $items = array();
                foreach ($salesquotation as $quotation) {
                    $items[] = $quotation->attributes;
                }

                $subitems = array();
                foreach ($sub_items as $subitem) {
                    $subitems[] = $subitem->attributes;
                }

                $quotation_categories = SalesQuotation::model()->findAll(array("select" => "DISTINCT category_label,category_id", 'condition' => 'master_id = ' . $qid . ' AND work_type IS NOT NULL', "order" => "category_id  ASC"));
                $lists = array();
                $data = array();
                if (!empty($items)) {
                    if (!empty($quotation_categories)) {
                        foreach ($quotation_categories as $quotation_category) {
                            $data_items = array();
                            $data_item_label = array();
                            $keys = array_keys(array_column($items, 'category_id'), $quotation_category->category_id);
                            foreach ($keys as $key) {
                                array_push($data_items, $items[$key]);
                            }

                            $keys1 = array_keys(array_column($data_items, 'category_label'), $quotation_category->category_label);
                            foreach ($keys1 as $key) {
                                array_push($data_item_label, $data_items[$key]);
                            }

                            $keys_sub = array_keys(array_column($subitems, 'category_label'), $quotation_category->category_label);
                            $data_subitems = array();
                            foreach ($keys_sub as $keys) {
                                array_push($data_subitems, $subitems[$keys]);
                            }
                            $data = array('category_details' => $quotation_category->attributes, 'items_list' => $data_item_label, 'sub_items' => $data_subitems);
                            array_push($lists, $data);
                        }
                    } else {
                        $data_items = array();
                        foreach ($items as $item) {
                            array_push($data_items, $item);
                        }
                        $data_subitems = array();
                        foreach ($subitems as $items) {
                            array_push($data_subitems, $items);
                        }
                        $data = array('category_details' => '', 'items_list' => $data_items, 'sub_items' => $data_subitems);
                        if (!empty($data)) {
                            array_push($lists, $data);
                        }
                    }
                }
                return $lists;
            } else {
                $salesquotation = SalesQuotation::model()->findAll(array(
                    'condition' => 'master_id = ' . $qid . ' AND work_type IS NULL AND deleted_status IN(1,2) AND revision_status = 0', "order" => "revision_no  DESC"
                ));
                return $salesquotation;
            }
        }
    }

    public function getQuotationdetailsedit($qid, $type,$selected_revision) {
        
        if ($qid != '') {
            $condition = "";
            if (1 == $type) {
                $condition = 'AND work_type IS NOT NULL';                
            } else {
                $condition = 'AND work_type IS NULL';                                
            }
            $sql = "SELECT * FROM jp_sales_quotation WHERE master_id =$qid "
                    . " AND deleted_status IN (1,2) "
                    . "  AND revision_no ='".$selected_revision."' $condition "
                    . " ORDER BY `category_id` ASC" ;                   

            $salesquotation = Yii::app()->db->createCommand($sql)->queryAll();
            return $salesquotation;
        }
    }

}
