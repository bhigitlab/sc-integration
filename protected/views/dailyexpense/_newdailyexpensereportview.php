<?php
/* @var $this ExpensesController */
/* @var $data Expenses */
if ($index == 0) {
?>
    <style>
        .grandtotal th {
            background-color: rgb(247, 240, 240) !important;
        }
    </style>
    <thead>
        <tr>
            <th>Sl No.</th>
            <th>#ID</th>
            <th>Company</th>
            <th>Bill No</th>
            <th>Expense Head</th>
            <th>Date</th>
            <th>Expense Type</th>
            <th>Payment</th>
            <th>Receipt Type</th>
            <th>Receipt</th>
            <th>Paid To</th>
            <th>Description</th>
        </tr>
        <tr class="grandtotal">
            <th colspan="7" style="text-align: right;"><b>Total</b></th>
            <th><?= Controller::money_format_inr($paidsum, 2, 1); ?></th>
            <th></th>
            <th><?= Controller::money_format_inr($receiptsum, 2, 1); ?></th>
            <th></th>
            <th></th>
        </tr>
    </thead>
<?php
}

?>
<tr>
    <td><?php echo $index + 1; ?></td>
    <td><b>#<?php echo $data['dailyexp_id'] ?></b></td>
    <td><?php echo ($data->company_id) ? $data->Company->name : ""; ?></td>
    <td><?php echo $data->bill_id; ?></td>
    <td>
        <?php
        if ($data->dailyexpense_type == "deposit") {
            if ($data->expensehead_id && $data->Deposit) {
                echo "Deposit - " . $data->Deposit->deposit_name;
            } else {
                echo ""; // Or provide a default value or handle the case accordingly
            }
            
        } else if ($data->dailyexpense_type == "expense") {
            if ($data->expensehead_id && $data->CompExpType) {
                echo "Expense - " . $data->CompExpType->name;
            } else {
                echo ""; // Or provide a default value or handle the case accordingly
            }
           
        } else {
            if ($data->exp_type_id && $data->expType) {
                echo "Receipt - " . $data->expType->name;
            } else {
                echo ""; // Or provide a default value or handle the case accordingly
            }
        }
        ?>
    </td>
    <td><?php echo date('d-m-Y', strtotime($data->date)); ?></td>
    <td><?php echo ($data->expense_type) ? $data->ExpenseType->caption : ""; ?></td>
    <td align="right"><?php echo ($data->dailyexpense_paidamount != '') ? Controller::money_format_inr($data->dailyexpense_paidamount, 2, 1) : '0.00'; ?></td>
    <td><?php echo ($data->dailyexpense_receipt_type) ? $data->ReceiptType->caption : ""; ?></td>
    <td align="right"><?php echo ($data->dailyexpense_receipt != '') ? Controller::money_format_inr($data->dailyexpense_receipt, 2, 1) : '0.00'; ?></td>
    <td><?php echo  !empty($data->employee_id) ? $data->employee->first_name : '-'; ?></td>
    <td><?php echo $data->description; ?></td>

</tr>