
<div class="table-responsive report-table-wrapper">
    <table class="table total-table" id="fixtable">
        <thead class="entry-table sticky-thead">
            <tr>

                <th>Receipt/Despatch No</th>
                <th>Transfer Type</th>
                <th>Expense Head</th>
                <th>Date</th>
                <th>From</th>
                <th>To</th>
                <th>Remark</th>
                <th>Item</th>
                <th>Dimension</th>
                <th>Unit</th>
                <th>Qty</th>
                <th>Rate (Rs.)</th>
                <th>Receipt (Rs.)</th>
                <th>Dispatch (Rs.)</th>
                <th>Net Total (Rs.)</th>

            </tr>

            <tr class="grandtotal">
                <td colspan="12" align="right"><b>Total</b></td>
                <td><b>
                        <?php echo Controller::money_format_inr($receipt_total, 2, 1); ?>
                    </b></td>
                <td><b>
                        <?php echo Controller::money_format_inr($dispatch_total, 2, 1); ?>
                    </b></td>
                <td><b>
                        <?= Controller::money_format_inr($net_total, 2, 1) ?>
                    </b></td>
            </tr>
        </thead>


        <?php // } 
        ?>
        <?php

        $total_qty = 0;
        $total_amount = 0;

        if (!empty($result_data)) {
            foreach ($result_data as $datas) {
                $row = 1;
                $qty_sum = 0;
                $amount_sum = 0;
                $amount_dispatch_sum = 0;
                if (!empty($datas)) {
                    $count = count($datas);
                    if ($count == 1) {
                        $rowspan = 1;
                    } else {
                        $rowspan = $count;
                    }
                    foreach ($datas as $dat) {
                        $total_qty += $dat['qty'];
                        $total_amount += $dat['amount'];


                        if ($dat['type'] == 1) {
                            $type = 'Opening';
                            $amount_sum += $dat['amount'];
                            $qty_sum += $dat['qty'];
                        } elseif ($dat['type'] == 2) {
                            $type = 'Purchase';
                            $amount_sum += $dat['amount'];
                            $qty_sum += $dat['qty'];
                        } elseif ($dat['type'] == 3) {
                            $type = "Dispatch";
                            $amount_dispatch_sum += $dat['amount'];
                            $qty_sum -= $dat['qty'];
                        } elseif ($dat['type'] == 4) {
                            $type = "Dispatch";
                            $amount_sum += $dat['amount'];
                            $qty_sum += $dat['qty'];
                        } else {
                            $type = '';
                            $qty_sum = 0;
                            $amount_sum = 0;
                        }



                        ?>
                        <tr>
                            <td>
                                <?php
                                if ($dat['type'] == 2) {
                                    echo CHtml::link($dat['receipt_deispatch_no'], 'index.php?r=wh/warehousereceipt/view&receiptid=' . $dat['id_val'], array('class' => 'link', 'target' => '_blank'));
                                } elseif ($dat['type'] == 3) {
                                    echo CHtml::link($dat['receipt_deispatch_no'], 'index.php?r=wh/warehousedespatch/view&despatchid=' . $dat['id_val'], array('class' => 'link', 'target' => '_blank'));
                                } elseif ($dat['type'] == 4) {
                                    echo CHtml::link($dat['receipt_deispatch_no'], 'index.php?r=wh/warehousedespatch/view&despatchid=' . $dat['id_val'], array('class' => 'link', 'target' => '_blank'));
                                } else {
                                    echo $dat['receipt_deispatch_no'];
                                }

                                ?>
                            </td>
                            <td>
                                <?= $type ?>
                            </td>
                            <td>
                                <?php echo $dat['expensehead']; ?>
                            </td>
                            <td>
                                <?= !empty($dat['date']) ? date('d-M-Y', strtotime($dat['date'])) : '' ?>
                            </td>
                            <td>
                                <?= $dat['from'] ?>
                            </td>
                            <td>
                                <?= $dat['to'] ?>
                            </td>
                            <td>
                                <?= $dat['remark'] ?>
                            </td>
                            <td>
                                <?= $dat['item'] ?>
                            </td>
                            <td>
                                <?= ($dat['dimension'] != '') ? "(" . $dat['dimension'] . ")" : "N/A" ?>
                            </td>
                            <td>
                                <?= $dat['unit'] ?>
                            </td>
                            <td>
                                <?= round($dat['qty'], 2) ?>
                            </td>
                            <td>
                                <?= Controller::money_format_inr($dat['rate'], 2, 1) ?>
                            </td>
                            <td>
                                <?= $dat['type'] == 2 || $dat['type'] == 4 || $dat['type'] == 1 ? Controller::money_format_inr($dat['amount'], 2, 1) : '' ?>
                            </td>
                            <td>
                                <?= $dat['type'] == 3 ? Controller::money_format_inr($dat['amount'], 2, 1) : '' ?>
                            </td>
                            <td></td>

                        </tr>
                        <?php
                        $row++;
                    }
                    ?>
                    <tr>
                        <td colspan="9" align="right"><b>Total</b></td>
                        <td><b>
                                <?= $dat['unit'] ?>
                            </b></td>
                        <td><b>
                                <?= round($qty_sum, 2) ?>
                            </b></td>
                        <td></td>
                        <td><b>
                                <?= Controller::money_format_inr($amount_sum, 2, 1) ?>
                            </b></td>
                        <td><b>
                                <?= Controller::money_format_inr($amount_dispatch_sum, 2, 1) ?>
                            </b></td>
                        <td><b>
                                <?= Controller::money_format_inr(($amount_sum - $amount_dispatch_sum), 2, 1) ?>
                            </b></td>
                    </tr>
                    <?php
                }
            }
        }
        ?>

        <tfoot class="entry-table">
            <tr>
                <th colspan="12" align="right"><b>Total</b></th>
                <th><b>
                        <?php echo Controller::money_format_inr($receipt_total, 2, 1); ?>
                    </b></th>
                <th><b>
                        <?php echo Controller::money_format_inr($dispatch_total, 2, 1); ?>
                    </b></th>
                <th><b>
                        <?= Controller::money_format_inr($net_total, 2, 1) ?>
                    </b></th>
            </tr>
        </tfoot>
        <table>
</div>