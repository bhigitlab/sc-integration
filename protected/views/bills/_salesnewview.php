
<?php
$tblpx = Yii::app()->db->tablePrefix;
if ($index == 0) {
?>
<thead class="entry-table">
    <tr>
        <th>Sl No.</th>
		<th>Date</th>
		<th>Client</th>
		<th>Invoice No</th>
                <th>Return No</th>
		<th>GST No</th>
		<th>Amount</th>
		<?php
		foreach($sales_head as $key_h => $value_h){
			echo '<th>Tax.val</th>';
			echo '<th>CGST '.$value_h['cgst'].' %</th>';
			echo '<th>SGST '.$value_h['sgst'].' %</th>';
			//echo '<th>IGST '.$value_h['igst'].' %</th>';
		}
		?>


		<?php
		foreach($igst_head_sales as $key_h => $value_h){
			echo '<th>IGST '.$value_h['igst'].' %</th>';
		}
		?>

		<th>Net Amount</th>
    </tr>
	<tr class="grandtotal">
							<th colspan="6" class="text-right"><b>Total</b></th>
							<th><?php  echo Controller::money_format_inr($sales_totalamount,2); ?></th>
							<?php
							$net_taxamount = 0;
							$net_sgst = 0;
							$net_cgst = 0;
							$net_finalamount = 0;
							foreach($sales_head as $key_h => $value1){


							$query = '';
							if($value1['sgst'] == null) {
								$sgst = 0;
								$query .=" AND {$tblpx}inv_list.sgst IS NULL";
							} else{
								$sgst = $value1['sgst'];
								$query .= "AND FORMAT({$tblpx}inv_list.sgst,2) = FORMAT(".$sgst.",2)";
							}

							if($value1['cgst'] == null) {
								$cgst = 0;
								$query .=" AND {$tblpx}inv_list.cgst IS NULL";
							} else{
								$cgst = $value1['cgst'];
								$query .= " AND FORMAT({$tblpx}inv_list.cgst,2) = FORMAT(".$cgst.",2)";
							}

							//if($data['type'] =='Invoice') {
                                                            $row_sgst1 = Yii::app()->db->createCommand("SELECT SUM({$tblpx}inv_list.sgst_amount) as sgst FROM {$tblpx}inv_list LEFT JOIN {$tblpx}invoice ON {$tblpx}invoice.invoice_id ={$tblpx}inv_list.inv_id WHERE {$tblpx}invoice.date BETWEEN '".$date_from."' AND '".$date_to."' ".$query." ")->queryRow();
                                                            $row_cgst1 = Yii::app()->db->createCommand("SELECT SUM({$tblpx}inv_list.cgst_amount) as cgst FROM {$tblpx}inv_list LEFT JOIN {$tblpx}invoice ON {$tblpx}invoice.invoice_id ={$tblpx}inv_list.inv_id WHERE {$tblpx}invoice.date BETWEEN '".$date_from."' AND '".$date_to."' ".$query."")->queryRow();
                                                            $row_tax1 = Yii::app()->db->createCommand("SELECT SUM({$tblpx}inv_list.amount) as tax_amount FROM {$tblpx}inv_list LEFT JOIN {$tblpx}invoice ON {$tblpx}invoice.invoice_id ={$tblpx}inv_list.inv_id WHERE {$tblpx}invoice.date BETWEEN '".$date_from."' AND '".$date_to."' ".$query."")->queryRow();
                                                            //$net_finalamount += $row_tax['tax_amount']+$row_cgst['cgst']+$row_sgst['sgst'];
                                                            $sales_taxamount = $row_tax1['tax_amount'];
                                                            $sales_sgst = $row_sgst1['sgst'];
                                                            $sales_cgst = $row_cgst1['cgst'];
                                                        //}else if($data['type'] =='Purchase Return') {
                                                            //echo "jjj";
                                                            $row_cgst2 = Yii::app()->db->createCommand("SELECT SUM(returnitem_cgst) as cgst FROM {$tblpx}purchase_returnitem LEFT JOIN {$tblpx}purchase_return ON {$tblpx}purchase_returnitem.return_id = {$tblpx}purchase_return.return_id  WHERE {$tblpx}purchase_return.return_date BETWEEN '".$date_from."' AND '".$date_to."' AND FORMAT(returnitem_cgstpercent,2) = FORMAT(".$value1['cgst'].",2) AND FORMAT(returnitem_sgstpercent,2) = FORMAT(".$value1['sgst'].",2)")->queryRow();
                                                            $row_sgst2 = Yii::app()->db->createCommand("SELECT SUM(returnitem_sgst) as sgst FROM {$tblpx}purchase_returnitem LEFT JOIN {$tblpx}purchase_return ON {$tblpx}purchase_returnitem.return_id = {$tblpx}purchase_return.return_id  WHERE {$tblpx}purchase_return.return_date BETWEEN '".$date_from."' AND '".$date_to."' AND FORMAT(returnitem_cgstpercent,2) = FORMAT(".$value1['cgst'].",2) AND FORMAT(returnitem_sgstpercent,2) = FORMAT(".$value1['sgst'].",2)")->queryRow();
                                                            $row_tax2 = Yii::app()->db->createCommand("SELECT SUM(returnitem_amount) as tax_amount FROM {$tblpx}purchase_returnitem LEFT JOIN {$tblpx}purchase_return ON {$tblpx}purchase_returnitem.return_id = {$tblpx}purchase_return.return_id  WHERE {$tblpx}purchase_return.return_date BETWEEN '".$date_from."' AND '".$date_to."' AND FORMAT(returnitem_cgstpercent,2) = FORMAT(".$value1['cgst'].",2) AND FORMAT(returnitem_sgstpercent,2) = FORMAT(".$value1['sgst'].",2)")->queryRow();
                                                            $row_discount = Yii::app()->db->createCommand("SELECT SUM(returnitem_discountamount) as discount_amount FROM {$tblpx}purchase_returnitem LEFT JOIN {$tblpx}purchase_return ON {$tblpx}purchase_returnitem.return_id = {$tblpx}purchase_return.return_id  WHERE {$tblpx}purchase_return.return_date BETWEEN '".$date_from."' AND '".$date_to."' AND FORMAT(returnitem_cgstpercent,2) = FORMAT(".$value1['cgst'].",2) AND FORMAT(returnitem_sgstpercent,2) = FORMAT(".$value1['sgst'].",2)")->queryRow();
                                                            $return_taxamount = ($row_tax2['tax_amount']-$row_discount['discount_amount']);
                                                            $return_cgst =   $row_cgst2['cgst'];
                                                            $return_sgst = $row_sgst2['sgst'];


                                                            $net_sgst = $sales_sgst+$return_sgst;
                                                            $net_cgst = $sales_cgst+$return_cgst;
                                                            $net_taxamount = $sales_taxamount+$return_taxamount;
                                                            $net_finalamount += $net_taxamount+$net_cgst+$net_sgst;
                                                        //}

								echo '<th>'.Controller::money_format_inr($net_taxamount,2,1).'</th>';
								echo '<th>'.Controller::money_format_inr($net_sgst,2,1).'</th>';
								echo '<th>'.Controller::money_format_inr($net_cgst,2,1).'</th>';
							}
							?>


							<?php
							$net_igst= 0;
							$net_amountigst= 0;
							foreach($igst_head_sales as $key_h => $value1){
								$query = '';
								if($value1['igst'] == null) {
									$igst = 0;
									$query .=" AND igst IS NULL";
								} else{
									$igst = $value1['igst'];
									$query .= " AND FORMAT(igst,2) = FORMAT(".$igst.",2)";
								}
								
								
								if($data['type'] =='Invoice') {
									$row_igst1 = Yii::app()->db->createCommand("SELECT SUM(igst_amount) as igst FROM {$tblpx}inv_list WHERE inv_id=".$data['id']." ".$query." ")->queryRow();
							
									$sales_igst = $row_igst1['igst'];
								 } else if($data['type'] =='Purchase Return') {
								
									$sales_igst = $row_igst2['igst'];
								
								 }
								 $row_igst1 = Yii::app()->db->createCommand("SELECT SUM(igst_amount) as igst FROM {$tblpx}inv_list LEFT JOIN {$tblpx}invoice ON {$tblpx}invoice.invoice_id ={$tblpx}inv_list.inv_id WHERE {$tblpx}invoice.date BETWEEN '".$date_from."' AND '".$date_to."' ".$query." ")->queryRow();
								 $sales_igst += $row_igst1['igst'];
 
																 $row_igst2 = Yii::app()->db->createCommand("SELECT SUM(returnitem_igst) as igst FROM {$tblpx}purchase_returnitem LEFT JOIN {$tblpx}purchase_return ON {$tblpx}purchase_returnitem.return_id = {$tblpx}purchase_return.return_id  WHERE {$tblpx}purchase_return.return_date BETWEEN '".$date_from."' AND '".$date_to."' AND FORMAT(returnitem_igstpercent,2) = FORMAT(".$value1['igst'].",2)")->queryRow();
																 $return_igst = $row_igst2['igst'];
 
																 $net_igst = $row_igst2['igst']+$row_igst1['igst'];
								 
								echo '<th>'.Controller::money_format_inr($row_igst1['igst']+$row_igst2['igst'],2,1).'</th>';
							}
							?>

							<th><?php echo Controller::money_format_inr($net_finalamount+$net_igst,2,1); ?></th>
						</tr>
  </thead>
<?php
    }
?>

<tbody>
    <tr>
				 <td><?php echo $index+1; ?></td>
				 <td><?php echo date('Y-m-d', strtotime($data['date'])); ?></td>
				<!-- <td><?php // echo $data['project_name']; ?></td> -->
				 <td><?php echo $data['client']; ?></td>
				 <td><?php echo $data['inv_no'];?></td>
                                 <td><?php echo $data['return_no'];?></td>
				 <td><?php echo $data['gst_no'];?></td>
				 <td><?php echo Controller::money_format_inr($data['totalamount'],2,1);?></td>
				 <?php
						$net_amount= 0;
						foreach($sales_head as $key_h => $value1){
     							$query = '';
							if($value1['sgst'] == null) {
								$sgst = 0;
								$query .=" AND sgst IS NULL";
							} else{
								$sgst = $value1['sgst'];
								$query .= "AND FORMAT(sgst,2) = FORMAT(".$sgst.",2)";
							}

							if($value1['cgst'] == null) {
								$cgst = 0;
								$query .=" AND cgst IS NULL";
							} else{
								$cgst = $value1['cgst'];
								$query .= " AND FORMAT(cgst,2) = FORMAT(".$cgst.",2)";
							}

							/* if($value1['igst'] == null) {
								$igst = 0;
								$query .=" AND igst IS NULL";
							} else{
								$igst = $value1['igst'];
								$query .= " AND FORMAT(igst,2) = FORMAT(".$igst.",2)";
							} */
							if($data['type'] =='Invoice') {
                                                            $row_sgst1 = Yii::app()->db->createCommand("SELECT SUM(sgst_amount) as sgst FROM {$tblpx}inv_list WHERE inv_id=".$data['id']." ".$query." ")->queryRow();
                                                            //$row_igst = Yii::app()->db->createCommand("SELECT SUM(igst_amount) as igst FROM {$tblpx}inv_list WHERE inv_id=".$data['invoice_id']." ".$query." ")->queryRow();
                                                            $row_cgst1 = Yii::app()->db->createCommand("SELECT SUM(cgst_amount) as cgst FROM {$tblpx}inv_list WHERE inv_id=".$data['id']." ".$query."")->queryRow();
                                                            $row_tax = Yii::app()->db->createCommand("SELECT SUM(amount) as tax_amount FROM {$tblpx}inv_list WHERE inv_id=".$data['id']." ".$query."")->queryRow();
                                                            $tax_amount = $row_tax['tax_amount'];
                                                            $sales_cgst = $row_cgst1['cgst'];
							    $sales_sgst = $row_sgst1['sgst'];
                                                            $net_amount += $tax_amount+$sales_cgst+$sales_sgst;
                                                        } else if($data['type'] =='Purchase Return') {
                                                            $row_cgst2 = Yii::app()->db->createCommand("SELECT SUM(returnitem_cgst) as cgst FROM {$tblpx}purchase_returnitem WHERE return_id=".$data['id']." AND FORMAT(returnitem_cgstpercent,2) = FORMAT(".$value1['cgst'].",2) AND FORMAT(returnitem_sgstpercent,2) = FORMAT(".$value1['sgst'].",2)")->queryRow();
                                                            $row_sgst2 = Yii::app()->db->createCommand("SELECT SUM(returnitem_sgst) as sgst FROM {$tblpx}purchase_returnitem WHERE return_id=".$data['id']." AND FORMAT(returnitem_cgstpercent,2) = FORMAT(".$value1['cgst'].",2) AND FORMAT(returnitem_sgstpercent,2) = FORMAT(".$value1['sgst'].",2)")->queryRow();
                                                            $row_tax2 = Yii::app()->db->createCommand("SELECT SUM(returnitem_amount) as tax_amount FROM {$tblpx}purchase_returnitem WHERE return_id=".$data['id']." AND FORMAT(returnitem_cgstpercent,2) = FORMAT(".$value1['cgst'].",2) AND FORMAT(returnitem_sgstpercent,2) = FORMAT(".$value1['sgst'].",2)")->queryRow();
                                                            $row_discount = Yii::app()->db->createCommand("SELECT SUM(returnitem_discountamount) as discount_amount FROM {$tblpx}purchase_returnitem WHERE return_id=".$data['id']." AND FORMAT(returnitem_cgstpercent,2) = FORMAT(".$value1['cgst'].",2) AND FORMAT(returnitem_sgstpercent,2) = FORMAT(".$value1['sgst'].",2)")->queryRow();
                                                            $tax_amount = ($row_tax2['tax_amount']-$row_discount['discount_amount']);
                                                            $sales_cgst =   $row_cgst2['cgst'];
                                                            $sales_sgst = $row_sgst2['sgst'];
                                                            $net_amount += $tax_amount+$sales_cgst+$sales_sgst;
                                                        }
				?>
				<td><?php echo (($tax_amount == 0)?'':Controller::money_format_inr($tax_amount,2,1)); ?> </td>
				<td><?php echo ($sales_cgst == 0)?'':Controller::money_format_inr($sales_cgst,2,1); ?></td>
				<td><?php echo ($sales_sgst == 0)?'':Controller::money_format_inr($sales_sgst,2,1); ?></td>
				<!-- <td><?php // echo ($row_igst['igst'] == 0)?'':number_format($row_igst['igst'],2); ?></td> -->
				<?php } ?>

				<?php
						$net_amountigst= 0;
						foreach($igst_head_sales as $key_h => $value1){

							$query = '';
							if($value1['igst'] == null) {
								$igst = 0;
								$query .=" AND igst IS NULL";
							} else{
								$igst = $value1['igst'];
								$query .= " AND FORMAT(igst,2) = FORMAT(".$igst.",2)";
							}
						if($data['type'] =='Invoice') {
                                                    $row_igst1 = Yii::app()->db->createCommand("SELECT SUM(igst_amount) as igst FROM {$tblpx}inv_list WHERE inv_id=".$data['id']." ".$query." ")->queryRow();
                                                    $row_tax1 = Yii::app()->db->createCommand("SELECT SUM(amount) as tax_amount FROM {$tblpx}inv_list WHERE inv_id=".$data['id']." ".$query."")->queryRow();
                                                    $net_amountigst += $row_igst1['igst'];
                                                    $sales_igst = $row_igst1['igst'];
                                                 } else if($data['type'] =='Purchase Return') {
                                                    $row_igst2 = Yii::app()->db->createCommand("SELECT SUM(returnitem_igst) as igst FROM {$tblpx}purchase_returnitem WHERE return_id=".$data['id']." AND FORMAT(returnitem_igstpercent,2) = FORMAT(".$value1['igst'].",2)")->queryRow();
                                                    $sales_igst = $row_igst2['igst'];
                                                    $row_tax2 = Yii::app()->db->createCommand("SELECT SUM(returnitem_amount) as tax_amount FROM {$tblpx}purchase_returnitem WHERE return_id=".$data['id']." AND FORMAT(returnitem_igstpercent,2) = FORMAT(".$value1['igst'].",2)")->queryRow();
                                                    $row_discount = Yii::app()->db->createCommand("SELECT SUM(returnitem_discountamount) as discount_amount FROM {$tblpx}purchase_returnitem WHERE return_id=".$data['id']." AND FORMAT(returnitem_igstpercent,2) = FORMAT(".$value1['igst'].",2)")->queryRow();
                                                    $tax_amount = ($row_tax2['tax_amount']-$row_discount['discount_amount']);
                                                    $net_amountigst += $row_igst2['igst'];
												 }
												 

				?>

				<td><?php echo ($sales_igst == 0)?'':Controller::money_format_inr($sales_igst,2,1); ?></td>


				<?php } ?>

				<td><?php echo Controller::money_format_inr($net_amount+$net_amountigst,2); ?></td>
				 </tr>

				 </tbody>

				 <?php
				 if ($index == 0) {
					?>
					<!-- <tfoot>
						<tr>
							<th colspan="6" class="text-right"><b>Total</b></th>
							<th><?php  echo Controller::money_format_inr($sales_totalamount,2); ?></th>
							<?php
							$net_taxamount = 0;
							$net_sgst = 0;
							$net_cgst = 0;
							$net_finalamount = 0;
							foreach($sales_head as $key_h => $value1){


							$query = '';
							if($value1['sgst'] == null) {
								$sgst = 0;
								$query .=" AND {$tblpx}inv_list.sgst IS NULL";
							} else{
								$sgst = $value1['sgst'];
								$query .= "AND FORMAT({$tblpx}inv_list.sgst,2) = FORMAT(".$sgst.",2)";
							}

							if($value1['cgst'] == null) {
								$cgst = 0;
								$query .=" AND {$tblpx}inv_list.cgst IS NULL";
							} else{
								$cgst = $value1['cgst'];
								$query .= " AND FORMAT({$tblpx}inv_list.cgst,2) = FORMAT(".$cgst.",2)";
							}

							//if($data['type'] =='Invoice') {
                                                            $row_sgst1 = Yii::app()->db->createCommand("SELECT SUM({$tblpx}inv_list.sgst_amount) as sgst FROM {$tblpx}inv_list LEFT JOIN {$tblpx}invoice ON {$tblpx}invoice.invoice_id ={$tblpx}inv_list.inv_id WHERE {$tblpx}invoice.date BETWEEN '".$date_from."' AND '".$date_to."' ".$query." ")->queryRow();
                                                            $row_cgst1 = Yii::app()->db->createCommand("SELECT SUM({$tblpx}inv_list.cgst_amount) as cgst FROM {$tblpx}inv_list LEFT JOIN {$tblpx}invoice ON {$tblpx}invoice.invoice_id ={$tblpx}inv_list.inv_id WHERE {$tblpx}invoice.date BETWEEN '".$date_from."' AND '".$date_to."' ".$query."")->queryRow();
                                                            $row_tax1 = Yii::app()->db->createCommand("SELECT SUM({$tblpx}inv_list.amount) as tax_amount FROM {$tblpx}inv_list LEFT JOIN {$tblpx}invoice ON {$tblpx}invoice.invoice_id ={$tblpx}inv_list.inv_id WHERE {$tblpx}invoice.date BETWEEN '".$date_from."' AND '".$date_to."' ".$query."")->queryRow();
                                                            //$net_finalamount += $row_tax['tax_amount']+$row_cgst['cgst']+$row_sgst['sgst'];
                                                            $sales_taxamount = $row_tax1['tax_amount'];
                                                            $sales_sgst = $row_sgst1['sgst'];
                                                            $sales_cgst = $row_cgst1['cgst'];
                                                        //}else if($data['type'] =='Purchase Return') {
                                                            //echo "jjj";
                                                            $row_cgst2 = Yii::app()->db->createCommand("SELECT SUM(returnitem_cgst) as cgst FROM {$tblpx}purchase_returnitem LEFT JOIN {$tblpx}purchase_return ON {$tblpx}purchase_returnitem.return_id = {$tblpx}purchase_return.return_id  WHERE {$tblpx}purchase_return.return_date BETWEEN '".$date_from."' AND '".$date_to."' AND FORMAT(returnitem_cgstpercent,2) = FORMAT(".$value1['cgst'].",2) AND FORMAT(returnitem_sgstpercent,2) = FORMAT(".$value1['sgst'].",2)")->queryRow();
                                                            $row_sgst2 = Yii::app()->db->createCommand("SELECT SUM(returnitem_sgst) as sgst FROM {$tblpx}purchase_returnitem LEFT JOIN {$tblpx}purchase_return ON {$tblpx}purchase_returnitem.return_id = {$tblpx}purchase_return.return_id  WHERE {$tblpx}purchase_return.return_date BETWEEN '".$date_from."' AND '".$date_to."' AND FORMAT(returnitem_cgstpercent,2) = FORMAT(".$value1['cgst'].",2) AND FORMAT(returnitem_sgstpercent,2) = FORMAT(".$value1['sgst'].",2)")->queryRow();
                                                            $row_tax2 = Yii::app()->db->createCommand("SELECT SUM(returnitem_amount) as tax_amount FROM {$tblpx}purchase_returnitem LEFT JOIN {$tblpx}purchase_return ON {$tblpx}purchase_returnitem.return_id = {$tblpx}purchase_return.return_id  WHERE {$tblpx}purchase_return.return_date BETWEEN '".$date_from."' AND '".$date_to."' AND FORMAT(returnitem_cgstpercent,2) = FORMAT(".$value1['cgst'].",2) AND FORMAT(returnitem_sgstpercent,2) = FORMAT(".$value1['sgst'].",2)")->queryRow();
                                                            $row_discount = Yii::app()->db->createCommand("SELECT SUM(returnitem_discountamount) as discount_amount FROM {$tblpx}purchase_returnitem LEFT JOIN {$tblpx}purchase_return ON {$tblpx}purchase_returnitem.return_id = {$tblpx}purchase_return.return_id  WHERE {$tblpx}purchase_return.return_date BETWEEN '".$date_from."' AND '".$date_to."' AND FORMAT(returnitem_cgstpercent,2) = FORMAT(".$value1['cgst'].",2) AND FORMAT(returnitem_sgstpercent,2) = FORMAT(".$value1['sgst'].",2)")->queryRow();
                                                            $return_taxamount = ($row_tax2['tax_amount']-$row_discount['discount_amount']);
                                                            $return_cgst =   $row_cgst2['cgst'];
                                                            $return_sgst = $row_sgst2['sgst'];


                                                            $net_sgst = $sales_sgst+$return_sgst;
                                                            $net_cgst = $sales_cgst+$return_cgst;
                                                            $net_taxamount = $sales_taxamount+$return_taxamount;
                                                            $net_finalamount += $net_taxamount+$net_cgst+$net_sgst;
                                                        //}

								echo '<th>'.Controller::money_format_inr($net_taxamount,2,1).'</th>';
								echo '<th>'.Controller::money_format_inr($net_sgst,2,1).'</th>';
								echo '<th>'.Controller::money_format_inr($net_cgst,2,1).'</th>';
							}
							?>


							<?php
							$net_igst= 0;
							foreach($igst_head_sales as $key_h => $value1){
								$query = '';
								if($value1['igst'] == null) {
									$igst = 0;
									$query .=" AND igst IS NULL";
								} else{
									$igst = $value1['igst'];
									$query .= " AND FORMAT(igst,2) = FORMAT(".$igst.",2)";
								}

								$row_igst1 = Yii::app()->db->createCommand("SELECT SUM(igst_amount) as igst FROM {$tblpx}inv_list LEFT JOIN {$tblpx}invoice ON {$tblpx}invoice.invoice_id ={$tblpx}inv_list.inv_id WHERE {$tblpx}invoice.date BETWEEN '".$date_from."' AND '".$date_to."' ".$query." ")->queryRow();
								$sales_igst += $row_igst1['igst'];

                                                                $row_igst2 = Yii::app()->db->createCommand("SELECT SUM(returnitem_igst) as igst FROM {$tblpx}purchase_returnitem LEFT JOIN {$tblpx}purchase_return ON {$tblpx}purchase_returnitem.return_id = {$tblpx}purchase_return.return_id  WHERE {$tblpx}purchase_return.return_date BETWEEN '".$date_from."' AND '".$date_to."' AND FORMAT(returnitem_igstpercent,2) = FORMAT(".$value1['igst'].",2)")->queryRow();
                                                                $return_igst = $row_igst2['igst'];

                                                                $net_igst = $row_igst2['igst']+$row_igst1['igst'];


								echo '<th>'.Controller::money_format_inr($row_igst1['igst']+$row_igst2['igst'],2,1).'</th>';
							}
							?>

							<th><?php echo Controller::money_format_inr($net_finalamount+$net_igst,2,1); ?></th>
						</tr>
					  </tfoot> -->
					<?php
						}
					?>
