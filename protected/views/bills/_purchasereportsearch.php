<?php
$user = Users::model()->findByPk(Yii::app()->user->id);
$arrVal = explode(',', $user->company_id);
$newQuery = "";
$newQuery1 = "";
foreach ($arrVal as $arr) {
    if ($newQuery)
        $newQuery .= ' OR';
    if ($newQuery1)
        $newQuery1 .= ' OR';
    $newQuery .= " FIND_IN_SET('" . $arr . "', id)";
    $newQuery1 .= " FIND_IN_SET('" . $arr . "', company_id)";
}
?>
<script>
    $(function () {
        $("#date_from").datepicker({
            dateFormat: 'yy-mm-dd',
            maxDate: $("#date_to").val()
        });
        $("#date_to").datepicker({
            dateFormat: 'yy-mm-dd',
        });
    });

</script>
<div class="page_filter clearfix">
    <?php $form = $this->beginWidget('CActiveForm', array(
        'action' => Yii::app()->createUrl($this->route),
        'method' => 'GET',
    )); ?>
    <div class="row">
        <?php
        $tblpx = Yii::app()->db->tablePrefix;
        $projects = $model = Yii::app()->db->createCommand("SELECT pid,name FROM {$tblpx}projects WHERE (" . $newQuery1 . ") order by name")->queryAll();
        ?>
        <div class="form-group col-xs-12 col-sm-3 col-md-2 ">
            <label for="project">Project</label>
            <select name="Bills[project]" class="form-control">
                <option value="">Select Project</option>
                <?php foreach ($projects as $pro) { ?>
                    <option value="<?= $pro['pid'] ?>" <?php if (isset($pro_id) && $pro['pid'] == $pro_id) { ?> Selected<?php } ?>>
                        <?= $pro['name'] ?>
                    </option>
                <?php } ?>
            </select>
        </div>
        <?php
        $vendors = $model = Yii::app()->db->createCommand("SELECT vendor_id,name FROM {$tblpx}vendors WHERE (" . $newQuery1 . ") order by name")->queryAll();
        ?>
        <div class="form-group col-xs-12 col-sm-3 col-md-2 ">
            <label for="vendor">Vendor</label>
            <select name="Bills[vendor]" class="form-control">
                <option value="">Select Vendor</option>
                <?php foreach ($vendors as $ven) { ?>
                    <option value="<?= $ven['vendor_id'] ?>" <?php if (isset($vendor_id) && $ven['vendor_id'] == $vendor_id) { ?> Selected<?php } ?>>
                        <?= $ven['name'] ?>
                    </option>
                <?php } ?>
            </select>
        </div>
        <div class="form-group col-xs-12 col-sm-3 col-md-2 ">
            <label for="company">Company</label>
            <?php
            echo CHtml::dropDownList('company_id', 'company_id', CHtml::listData(Company::model()->findAll(array(
                'select' => array('id, name'),
                'order' => 'id DESC',
                'condition' => '(' . $newQuery . ')',
                'distinct' => true
            )), 'id', 'name'), array('empty' => 'Select Company', 'id' => 'company_id', 'class' => 'form-control', 'options' => array($company_id => array('selected' => true))));
            ?>
        </div>
        <div class="form-group col-xs-12 col-sm-3 col-md-2 ">
            <label for="purchase">Purchase Number</label>
            <?php echo CHtml::textField('Bills[pu_num]', (($pu_num != '') ? $pu_num : ''), array('placeholder' => 'Purchase Number', 'class' => 'form-control')); ?>
        </div>
        <div class="form-group col-xs-12 col-sm-3 col-md-2 ">
            <label for="bill">Bill Number</label>
            <?php echo CHtml::textField('Bills[bill_num]', (($bill_num != '') ? $bill_num : ''), array('placeholder' => 'Bill Number', 'class' => 'form-control')); ?>
        </div>
        <div class="form-group col-xs-12 col-sm-3 col-md-2 ">
            <label for="spec">Specification</label>
            <select name="category_id[]" id="category_id" class="form-control js-example-basic-multiple category_id"
                placeholder="Please select Item spec">
                <?php
                foreach ($specification as $key => $value) {
                    ?>
                    <option value="<?php echo $value['id']; ?>">
                        <?php echo $value['data']; ?>
                    </option>
                    <?php
                }
                ?>
            </select>
        </div>

        <div class="form-group col-xs-12 col-sm-3 col-md-2 ">
            <label for="expense_head">Expense Head</label>
            <select name="expense_head[]" id="expense_head" class="form-control js-example-basic-multiple expense_head">
                <?php
                foreach ($expense_type as $key => $value) {
                    ?>
                    <option value="<?php echo $value['type_id']; ?>">
                        <?php echo $value['type_name']; ?>
                    </option>
                    <?php
                }
                ?>
            </select>
        </div>
        <!-- </div>
    <div class ="row"> -->
        <div class="form-group col-xs-12 col-sm-3 col-md-2 ">
            <label>From</label>
            <?php
            echo CHtml::textField('Bills[date_from]', (($date_from != '') ? date('Y-m-d', strtotime($date_from)) : ''), array("id" => "date_from", "class" => "form-control", "readonly" => true, "placeholder" => 'Date From', 'autocomplete' => 'off'));
            ?>
        </div>
        <div class="form-group col-xs-12 col-sm-3 col-md-2  ">
            <label>To</label>
            <?php echo CHtml::textField('Bills[date_to]', (($date_to != '') ? date('Y-m-d', strtotime($date_to)) : ''), array("id" => "date_to", "class" => "form-control", "readonly" => true, "placeholder" => 'Date To', 'autocomplete' => 'off')); ?>
        </div>
        <div class="form-group col-xs-12 col-sm-3 col-md-2 text-sm-left text-right">
            <label class="d-sm-block d-none">&nbsp;</label>
            <div>
                <?php echo CHtml::submitButton('GO', array('class' => 'btn btn-sm btn-primary')); ?>
                <?php echo CHtml::resetButton('Clear', array('onclick' => 'javascript:location.href="' . $this->createUrl('purchasebillreport') . '"', 'class' => 'btn btn-sm btn-default')); ?>
            </div>
        </div>
    </div>
    <?php $this->endWidget(); ?>
</div>

<script>
    $(function () {
        $('#category_id').select2({
            width: '100%',
            multiple: true,
            placeholder: 'Select Item Specification',
        });
        $('#expense_head').select2({
            width: '100%',
            multiple: true,
            placeholder: 'Select Expense Head',
        });
    });
</script>