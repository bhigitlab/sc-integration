<div class="table-responsive">
    <table cellpadding="10" class="table  list-view " id="fixtable">
        <thead class="entry-table">
            <tr>
                <th>SL No</th>
                <th>Despatch No</th>
                <th>Warehouse Name</th>
                <th>Goods Despatched To</th>
                <th>Clerk</th>
                <th>Date</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $daterange = '';
            if (!empty($date_from) || !empty($date_to)) {
                if ((isset($date_from) && !empty($date_from)) && ((isset($date_to) && empty($date_to)) || !isset($date_to))) {
                    $daterange .= ' WHERE `created_date` >="' . $date_from . '"';
                } else if ((isset($date_to) && !empty($date_to)) && ((isset($date_from) && empty($date_from)) || !isset($date_from))) {
                    $daterange .= ' WHERE `created_date` <="' . $date_to . '"';
                } else if ((isset($date_to) && !empty($date_to)) && (isset($date_from) && !empty($date_from))) {
                    $daterange .= 'WHERE created_date >="' . $date_from . '" AND created_date <="' . $date_to . '"';
                } else {
                    $daterange .= '';
                }
            }
            $sql = "SELECT * FROM jp_warehousedespatch " . $daterange;
            $despatch = Yii::app()->db->createCommand($sql)->queryAll();
            $i = 1;
            foreach ($despatch as $key => $value) {
                ?>
                <tr>
                    <td>
                        <?php echo $i; ?>
                    </td>
                    <td>
                        <?php
                        echo CHtml::link($value['warehousedespatch_no'], 'index.php?r=wh/warehousedespatch/view&despatchid=' . $value['warehousedespatch_id'], array('class' => 'link', 'target' => '_blank'));
                        ?>
                    </td>
                    <td>
                        <?php
                        $warehouse = Warehouse::model()->findByPk($value['warehousedespatch_warehouseid']);
                        echo $warehouse['warehouse_name'];
                        ?>
                    </td>
                    <td>
                        <?php
                        $warehouse_despatch_to = Warehouse::model()->findByPk($value['warehousedespatch_warehouseid_to']);
                        echo $warehouse_despatch_to['warehouse_name'];
                        ?>
                    </td>
                    <td>
                        <?php
                        $clerk = Users::model()->findByPk($value['warehousedespatch_clerk']);
                        echo $clerk['first_name'] . ' ' . $clerk['last_name'];
                        ?>
                    </td>
                    <td>
                        <?php echo date("d-m-Y", strtotime($value['warehousedespatch_date'])); ?>
                    </td>
                </tr>
                <?php
                $i++;
            }
            ?>
        </tbody>
    </table>
</div>