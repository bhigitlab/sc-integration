<?php
/* @var $this LogController */
/* @var $model JpLog */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'jp-log-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'log_data'); ?>
		<?php echo $form->textArea($model,'log_data',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'log_data'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'log_table'); ?>
		<?php echo $form->textField($model,'log_table',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'log_table'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'log_primary_key'); ?>
		<?php echo $form->textField($model,'log_primary_key'); ?>
		<?php echo $form->error($model,'log_primary_key'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'log_datetime'); ?>
		<?php echo $form->textField($model,'log_datetime'); ?>
		<?php echo $form->error($model,'log_datetime'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'log_action'); ?>
		<?php echo $form->textField($model,'log_action',array('size'=>6,'maxlength'=>6)); ?>
		<?php echo $form->error($model,'log_action'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'log_action_by'); ?>
		<?php echo $form->textField($model,'log_action_by'); ?>
		<?php echo $form->error($model,'log_action_by'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->