
<?php
$tblpx = Yii::app()->db->tablePrefix;
$user = Users::model()->findByPk(Yii::app()->user->id);
$arrVal = explode(',', $user->company_id);
$newQuery = "";
$newQuery1 = "";
$newQuery2 = "";
$newQuery3 = "";
$newQuery4 = "";
$newQuery5 = "";

if ($company_id == NULL) {
	foreach ($arrVal as $arr) {
		if ($newQuery) $newQuery .= ' OR';
		if ($newQuery1) $newQuery1 .= ' OR';
		if ($newQuery2) $newQuery2 .= ' OR';
		if ($newQuery3) $newQuery3 .= ' OR';
		if ($newQuery4) $newQuery4 .= ' OR';
		if ($newQuery5) $newQuery5 .= ' OR';
		$newQuery .= " FIND_IN_SET('" . $arr . "', {$tblpx}bills.company_id)";
		$newQuery1 .= " FIND_IN_SET('" . $arr . "', {$tblpx}expenses.company_id)";
		$newQuery2 .= " FIND_IN_SET('" . $arr . "', {$tblpx}dailyexpense.company_id)";
		$newQuery3 .= " FIND_IN_SET('" . $arr . "', {$tblpx}dailyvendors.company_id)";
		$newQuery4 .= " FIND_IN_SET('" . $arr . "', {$tblpx}invoice.company_id)";
		$newQuery5 .= " FIND_IN_SET('" . $arr . "', {$tblpx}purchase_return.company_id)";
	}
} else {
	$newQuery .= " FIND_IN_SET('" . $company_id . "', {$tblpx}bills.company_id)";
	$newQuery1 .= " FIND_IN_SET('" . $company_id . "', {$tblpx}expenses.company_id)";
	$newQuery2 .= " FIND_IN_SET('" . $company_id . "', {$tblpx}dailyexpense.company_id)";
	$newQuery3 .= " FIND_IN_SET('" . $company_id . "', {$tblpx}dailyvendors.company_id)";
	$newQuery4 .= " FIND_IN_SET('" . $company_id . "', {$tblpx}invoice.company_id)";
	$newQuery5 .= " FIND_IN_SET('" . $company_id . "', {$tblpx}purchase_return.company_id)";
}


if ($index == 0) {
?>
	<thead class="entry-table">
		<tr>
			<th>Sl No.</th>
			<th>Payment mode</th>
			<th>Bill date</th>
			<th>Vendor</th>
			<th>GST No</th>
			<th>Commodity</th>
			<th>Commodity Code</th>
			<th>Bill no</th>
			<th>Bill amount</th>
			<?php
			foreach ($purchase_head as $key_h => $value_h) {
				echo '<th>Tax.val</th>';
				echo '<th>CGST ' . $value_h['cgstpercent'] . ' %</th>';
				echo '<th>SGST ' . $value_h['sgstpercent'] . ' %</th>';
				//echo '<th>IGST '.$value_h['billitem_igstpercent'].' %</th>';
			}
			?>
			<?php
			foreach ($purchase_head_igst as $key_h => $value_h) {
				echo '<th>Tax.val</th>';
				echo '<th>IGST ' . $value_h['igstpercent'] . ' %</th>';
			}
			?>
			<th>Other changers</th>
			<th>Net amount</th>
		</tr>
		<tr class="grandtotal">
			<th colspan="8" class="text-right"><b>Total</b></th>
			<th><?php echo ($purchase_totalamount != '') ? Controller::money_format_inr($purchase_totalamount, 2, 1) : '0.00'; ?></th>
			<?php
			$net_amount = 0;
			$net_taxamount = 0;
			$net_sgst = 0;
			$net_cgst = 0;
			foreach ($purchase_head as $key1 => $value1) {
				//if($data['type'] =='bills') {
				$row_cgst1 = Yii::app()->db->createCommand("SELECT SUM(billitem_cgst) as cgst FROM {$tblpx}billitem LEFT JOIN {$tblpx}bills ON {$tblpx}bills.bill_id ={$tblpx}billitem.bill_id WHERE {$tblpx}bills.bill_date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND FORMAT(billitem_cgstpercent,2) = FORMAT(" . $value1['cgstpercent'] . ",2) AND FORMAT(billitem_sgstpercent,2) = FORMAT(" . $value1['sgstpercent'] . ",2) AND billitem_cgstpercent > 0 AND billitem_sgstpercent > 0 AND (" . $newQuery . ")")->queryRow();
				$row_sgst1 = Yii::app()->db->createCommand("SELECT SUM(billitem_sgst) as sgst FROM {$tblpx}billitem  LEFT JOIN {$tblpx}bills ON {$tblpx}bills.bill_id ={$tblpx}billitem.bill_id WHERE {$tblpx}bills.bill_date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND FORMAT(billitem_cgstpercent,2) = FORMAT(" . $value1['cgstpercent'] . ",2) AND FORMAT(billitem_sgstpercent,2) = FORMAT(" . $value1['sgstpercent'] . ",2) AND billitem_cgstpercent > 0 AND billitem_sgstpercent > 0 AND (" . $newQuery . ")")->queryRow();
				$row_tax1 = Yii::app()->db->createCommand("SELECT SUM(billitem_amount) as tax_amount FROM {$tblpx}billitem LEFT JOIN {$tblpx}bills ON {$tblpx}bills.bill_id ={$tblpx}billitem.bill_id WHERE {$tblpx}bills.bill_date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND FORMAT(billitem_cgstpercent,2) = FORMAT(" . $value1['cgstpercent'] . ",2) AND FORMAT(billitem_sgstpercent,2) = FORMAT(" . $value1['sgstpercent'] . ",2) AND billitem_cgstpercent > 0 AND billitem_sgstpercent > 0 AND (" . $newQuery . ")")->queryRow();
				$row_discount = Yii::app()->db->createCommand("SELECT SUM(billitem_discountamount) as discount_amount FROM {$tblpx}billitem  LEFT JOIN {$tblpx}bills ON {$tblpx}bills.bill_id ={$tblpx}billitem.bill_id WHERE {$tblpx}bills.bill_date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND FORMAT(billitem_cgstpercent,2) = FORMAT(" . $value1['cgstpercent'] . ",2) AND FORMAT(billitem_sgstpercent,2) = FORMAT(" . $value1['sgstpercent'] . ",2) AND billitem_cgstpercent > 0 AND billitem_sgstpercent > 0 AND (" . $newQuery . ")")->queryRow();
				$tax_amount1 = ($row_tax1['tax_amount'] - $row_discount['discount_amount']);
				$bill_cgst = $row_cgst1['cgst'];
				$bill_sgst = $row_sgst1['sgst'];

				$row_cgst2 = Yii::app()->db->createCommand("SELECT SUM(expense_cgst) as cgst FROM {$tblpx}expenses WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND FORMAT(expense_cgstp,2) = FORMAT(" . $value1['cgstpercent'] . ",2) AND FORMAT(expense_sgstp,2) = FORMAT(" . $value1['sgstpercent'] . ",2) AND (" . $newQuery1 . ") AND {$tblpx}expenses.bill_id IS NULL AND {$tblpx}expenses.invoice_id IS NULL AND {$tblpx}expenses.type=73 AND (expense_sgstp > 0 AND expense_cgstp > 0)")->queryRow();
				$row_sgst2 = Yii::app()->db->createCommand("SELECT SUM(expense_sgst) as sgst FROM {$tblpx}expenses WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "'  AND FORMAT(expense_cgstp,2) = FORMAT(" . $value1['cgstpercent'] . ",2) AND FORMAT(expense_sgstp,2) = FORMAT(" . $value1['sgstpercent'] . ",2) AND (" . $newQuery1 . ") AND {$tblpx}expenses.bill_id IS NULL AND {$tblpx}expenses.invoice_id IS NULL AND {$tblpx}expenses.type=73 AND (expense_sgstp > 0 AND expense_cgstp > 0)")->queryRow();
				$row_tax2 = Yii::app()->db->createCommand("SELECT SUM(expense_amount) as tax_amount FROM {$tblpx}expenses WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "'  AND FORMAT(expense_cgstp,2) = FORMAT(" . $value1['cgstpercent'] . ",2) AND FORMAT(expense_sgstp,2) = FORMAT(" . $value1['sgstpercent'] . ",2) AND (" . $newQuery1 . ") AND {$tblpx}expenses.bill_id IS NULL AND {$tblpx}expenses.invoice_id IS NULL AND {$tblpx}expenses.type=73")->queryRow();
				$tax_amount2 = $row_tax2['tax_amount'];
				$exp_cgst = $row_cgst2['cgst'];
				$exp_sgst = $row_sgst2['sgst'];

				$row_cgst3 = Yii::app()->db->createCommand("SELECT SUM(dailyexpense_cgst) as cgst FROM {$tblpx}dailyexpense WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND FORMAT(dailyexpense_cgstp,2) = FORMAT(" . $value1['cgstpercent'] . ",2) AND FORMAT(dailyexpense_sgstp,2) = FORMAT(" . $value1['sgstpercent'] . ",2) AND (" . $newQuery2 . ") AND {$tblpx}dailyexpense.exp_type=73 AND (dailyexpense_sgstp > 0 AND dailyexpense_cgstp > 0)")->queryRow();
				$row_sgst3 = Yii::app()->db->createCommand("SELECT SUM(dailyexpense_sgst) as sgst FROM {$tblpx}dailyexpense WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "'  AND FORMAT(dailyexpense_cgstp,2) = FORMAT(" . $value1['cgstpercent'] . ",2) AND FORMAT(dailyexpense_sgstp,2) = FORMAT(" . $value1['sgstpercent'] . ",2) AND (" . $newQuery2 . ") AND {$tblpx}dailyexpense.exp_type=73 AND (dailyexpense_sgstp > 0 AND dailyexpense_cgstp > 0)")->queryRow();
				$row_tax3 = Yii::app()->db->createCommand("SELECT SUM(dailyexpense_amount) as tax_amount FROM {$tblpx}dailyexpense WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "'  AND FORMAT(dailyexpense_cgstp,2) = FORMAT(" . $value1['cgstpercent'] . ",2) AND FORMAT(dailyexpense_sgstp,2) = FORMAT(" . $value1['sgstpercent'] . ",2) AND (" . $newQuery2 . ") AND {$tblpx}dailyexpense.exp_type=73")->queryRow();
				$tax_amount3 = $row_tax3['tax_amount'];
				$daexp_cgst = $row_cgst3['cgst'];
				$daexp_sgst = $row_sgst3['sgst'];

				$row_cgst4 = Yii::app()->db->createCommand("SELECT SUM(cgst_amount) as cgst FROM {$tblpx}dailyvendors WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND FORMAT(cgst,2) = FORMAT(" . $value1['cgstpercent'] . ",2) AND FORMAT(sgst,2) = FORMAT(" . $value1['sgstpercent'] . ",2) AND (" . $newQuery3 . ") AND {$tblpx}dailyvendors.tax_amount >0 AND sgst >0 AND cgst >0")->queryRow();
				$row_sgst4 = Yii::app()->db->createCommand("SELECT SUM(sgst_amount) as sgst FROM {$tblpx}dailyvendors WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "'  AND FORMAT(cgst,2) = FORMAT(" . $value1['cgstpercent'] . ",2) AND FORMAT(sgst,2) = FORMAT(" . $value1['sgstpercent'] . ",2) AND (" . $newQuery3 . ") AND {$tblpx}dailyvendors.tax_amount >0 AND sgst >0 AND cgst >0")->queryRow();
				$row_tax4 = Yii::app()->db->createCommand("SELECT SUM(amount) as tax_amount FROM {$tblpx}dailyvendors WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "'  AND FORMAT(cgst,2) = FORMAT(" . $value1['cgstpercent'] . ",2) AND FORMAT(sgst,2) = FORMAT(" . $value1['sgstpercent'] . ",2) AND (" . $newQuery3 . ") AND sgst >0 AND cgst >0")->queryRow();
				$tax_amount4 = $row_tax4['tax_amount'];
				$vndr_cgst = $row_cgst4['cgst'];
				$vndr_sgst = $row_sgst4['sgst'];

				$net_taxamount += $tax_amount1 + $tax_amount2 + $tax_amount3 + $tax_amount4;
				$net_cgst += $bill_cgst + $exp_cgst + $daexp_cgst + $vndr_cgst;
				$net_sgst += $bill_sgst + $exp_sgst + $daexp_sgst + $vndr_sgst;

				//print_r($row_cgst);
				///}
				echo '<th>' . Controller::money_format_inr($tax_amount1 + $tax_amount2 + $tax_amount3 + $tax_amount4, 2, 1) . '</th>';
				echo '<th>' . Controller::money_format_inr($bill_cgst + $exp_cgst + $daexp_cgst + $vndr_cgst, 2, 1) . '</th>';
				echo '<th>' . Controller::money_format_inr($bill_sgst + $exp_sgst + $daexp_sgst + $vndr_sgst, 2, 1) . '</th>';
			}
			?>
			<?php
			$net_igst = 0;
			$net_tax  = 0;
			foreach ($purchase_head_igst as $key1 => $value1) {
				$row_igst1 = Yii::app()->db->createCommand("SELECT SUM(billitem_igst) as igst FROM {$tblpx}billitem LEFT JOIN {$tblpx}bills ON {$tblpx}bills.bill_id ={$tblpx}billitem.bill_id WHERE {$tblpx}bills.bill_date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND FORMAT(billitem_igstpercent,2) = FORMAT(" . $value1['igstpercent'] . ",2) AND (" . $newQuery . ") AND billitem_igstpercent > 0")->queryRow();
				$row_tax1 = Yii::app()->db->createCommand("SELECT SUM(billitem_amount) as tax_amount FROM {$tblpx}billitem LEFT JOIN {$tblpx}bills ON {$tblpx}bills.bill_id ={$tblpx}billitem.bill_id WHERE {$tblpx}bills.bill_date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND FORMAT(billitem_igstpercent,2) = FORMAT(" . $value1['igstpercent'] . ",2) AND billitem_igstpercent > 0 AND (" . $newQuery . ")")->queryRow();
				$row_discount = Yii::app()->db->createCommand("SELECT SUM(billitem_discountamount) as discount_amount FROM {$tblpx}billitem  LEFT JOIN {$tblpx}bills ON {$tblpx}bills.bill_id ={$tblpx}billitem.bill_id WHERE {$tblpx}bills.bill_date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND FORMAT(billitem_igstpercent,2) = FORMAT(" . $value1['igstpercent'] . ",2) AND billitem_igstpercent > 0 AND (" . $newQuery . ")")->queryRow();
				$tax_amount1 = ($row_tax1['tax_amount'] - $row_discount['discount_amount']);

				$row_igst2 = Yii::app()->db->createCommand("SELECT SUM(expense_igst) as igst FROM {$tblpx}expenses WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND FORMAT(expense_igstp,2) = FORMAT(" . $value1['igstpercent'] . ",2) AND (expense_igstp >0) AND {$tblpx}expenses.bill_id IS NULL AND {$tblpx}expenses.invoice_id IS NULL AND {$tblpx}expenses.type=73 AND (" . $newQuery1 . ")")->queryRow();
				$row_tax2 = Yii::app()->db->createCommand("SELECT SUM(expense_amount) as tax_amount FROM {$tblpx}expenses WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "'  AND FORMAT(expense_igstp,2) = FORMAT(" . $value1['igstpercent'] . ",2) AND (" . $newQuery1 . ") AND {$tblpx}expenses.bill_id IS NULL AND {$tblpx}expenses.invoice_id IS NULL AND {$tblpx}expenses.type=73 AND (expense_igstp >0)")->queryRow();
				$tax_amount2 = $row_tax2['tax_amount'];

				$row_igst3 = Yii::app()->db->createCommand("SELECT SUM(dailyexpense_igst) as igst FROM {$tblpx}dailyexpense WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND FORMAT(dailyexpense_igstp,2) = FORMAT(" . $value1['igstpercent'] . ",2) AND (dailyexpense_igstp >0) AND {$tblpx}dailyexpense.exp_type=73 AND (" . $newQuery2 . ")")->queryRow();
				$row_tax3 = Yii::app()->db->createCommand("SELECT SUM(dailyexpense_amount) as tax_amount FROM {$tblpx}dailyexpense WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "'  AND FORMAT(dailyexpense_igstp,2) = FORMAT(" . $value1['igstpercent'] . ",2) AND (" . $newQuery2 . ") AND {$tblpx}dailyexpense.exp_type=73 AND (dailyexpense_igstp >0)")->queryRow();
				$tax_amount3 = $row_tax3['tax_amount'];


				$row_igst4 = Yii::app()->db->createCommand("SELECT SUM(igst_amount) as igst FROM {$tblpx}dailyvendors WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND FORMAT(igst,2) = FORMAT(" . $value1['igstpercent'] . ",2) AND (" . $newQuery3 . ") AND igst >0")->queryRow();
				$row_tax4 = Yii::app()->db->createCommand("SELECT SUM(amount) as tax_amount FROM {$tblpx}dailyvendors WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "'  AND FORMAT(igst,2) = FORMAT(" . $value1['igstpercent'] . ",2) AND (" . $newQuery3 . ") AND igst >0")->queryRow();
				$tax_amount4 = $row_tax4['tax_amount'];

				$net_tax  += $tax_amount1 + $tax_amount2 + $tax_amount3 + $tax_amount4;
				$net_igst += $row_igst2['igst'] + $row_igst1['igst'] + $row_igst3['igst'] + $row_igst4['igst'];

				echo '<th>' . Controller::money_format_inr($tax_amount1 + $tax_amount2 + $tax_amount3 + $tax_amount4, 2, 1) . '</th>';
				echo '<th>' . Controller::money_format_inr($row_igst2['igst'] + $row_igst1['igst'] + $row_igst3['igst'] + $row_igst4['igst'], 2, 1) . '</th>';
			}
			?>
			<th></th>
			<th><?php echo Controller::money_format_inr(($net_taxamount + $net_cgst + $net_sgst + $net_igst + $net_tax), 2, 1); ?></th>
		</tr>
	</thead>
<?php
}
?>

<tbody>
	<tr>
		<td><?php echo ($index + 1); ?></td>
		<td>
			<?php
			if ($data['type'] == 'bills') {
				echo "Bills";
			} else if ($data['type'] == 'daybook') {
				echo "Daybook";
			} else if ($data['type'] == 'dailyexpense') {
				echo "Daily Expenses";
			} else if ($data['type'] == 'vendorpayment') {
				echo "Vendor Payment";
			}
			?>
		</td>
		<td><?php echo date('Y-m-d', strtotime($data['date'])); ?> </td>
		<td><?php echo $data['vendor_name']; ?></td>
		<td><?php echo $data['gst_no']; ?></td>
		<td></td>
		<td></td>
		<td>
			<?php
			if ($data['type'] == 'bills') {
				echo 	'<span class="bill_view" id="' . $data['id'] . '" style="color: #337ab7;
				cursor: pointer;">' . $data['bill_number'] . '</span>';
			}
			?></td>
		<?php
		$total_amount = 0;
		if ($data['type'] == 'bills') {
			$item_amount = Yii::app()->db->createCommand("SELECT sum(billitem_amount) as billitem_amount FROM {$tblpx}billitem WHERE bill_id=" . $data['id'] . " AND ((billitem_cgstpercent > 0 AND billitem_sgstpercent > 0) OR (billitem_igstpercent > 0))")->queryRow();
			$item_discount = Yii::app()->db->createCommand("SELECT sum(billitem_discountamount) as billitem_discountamount FROM {$tblpx}billitem WHERE bill_id=" . $data['id'] . " AND ((billitem_cgstpercent > 0 AND billitem_sgstpercent > 0) OR (billitem_igstpercent > 0))")->queryRow();
			$item_taxamount = Yii::app()->db->createCommand("SELECT sum(billitem_taxamount) as billitem_taxamount FROM {$tblpx}billitem WHERE bill_id=" . $data['id'] . " AND ((billitem_cgstpercent > 0 AND billitem_sgstpercent > 0) OR (billitem_igstpercent > 0))")->queryRow();
			//Controller::money_format_inr($data['bill_totalamount'],2,1)
			$total_amount = ($item_amount['billitem_amount'] - $item_discount['billitem_discountamount']) + $item_taxamount['billitem_taxamount'];
		} else if ($data['type'] == 'daybook') {
			$total_amount = $data['totalamount'];
		} else if ($data['type'] == 'dailyexpense') {
			$total_amount = $data['totalamount'];
		} else if ($data['type'] == 'vendorpayment') {
			$item_taxamount = Yii::app()->db->createCommand("SELECT tax_amount FROM {$tblpx}dailyvendors WHERE 	daily_v_id=" . $data['id'] . "")->queryRow();
			$total_amount = $data['totalamount'] + $item_taxamount['tax_amount'];
		}
		?>
		<td><?php echo Controller::money_format_inr($total_amount, 2, 1); ?></td>
		<?php
		$net_amount = 0;
		$net_taxamount = 0;
		$net_sgst = 0;
		$net_cgst = 0;
		$tax_amount = 0;
		$purchase_sgst = 0;
		$purchase_cgst = 0;
		foreach ($purchase_head as $key1 => $value1) {
			if ($data['type'] == 'bills') {
				$row_cgst1 = Yii::app()->db->createCommand("SELECT SUM(billitem_cgst) as cgst FROM {$tblpx}billitem WHERE bill_id=" . $data['id'] . " AND FORMAT(billitem_cgstpercent,2) = FORMAT(" . $value1['cgstpercent'] . ",2) AND FORMAT(billitem_sgstpercent,2) = FORMAT(" . $value1['sgstpercent'] . ",2)")->queryRow();
				$row_sgst1 = Yii::app()->db->createCommand("SELECT SUM(billitem_sgst) as sgst FROM {$tblpx}billitem WHERE bill_id=" . $data['id'] . " AND FORMAT(billitem_cgstpercent,2) = FORMAT(" . $value1['cgstpercent'] . ",2) AND FORMAT(billitem_sgstpercent,2) = FORMAT(" . $value1['sgstpercent'] . ",2)")->queryRow();
				$row_tax1 = Yii::app()->db->createCommand("SELECT SUM(billitem_amount) as tax_amount FROM {$tblpx}billitem WHERE bill_id=" . $data['id'] . " AND FORMAT(billitem_cgstpercent,2) = FORMAT(" . $value1['cgstpercent'] . ",2) AND FORMAT(billitem_sgstpercent,2) = FORMAT(" . $value1['sgstpercent'] . ",2)")->queryRow();
				$row_discount = Yii::app()->db->createCommand("SELECT SUM(billitem_discountamount) as discount_amount FROM {$tblpx}billitem WHERE bill_id=" . $data['id'] . " AND FORMAT(billitem_cgstpercent,2) = FORMAT(" . $value1['cgstpercent'] . ",2) AND FORMAT(billitem_sgstpercent,2) = FORMAT(" . $value1['sgstpercent'] . ",2)")->queryRow();
				$tax_amount = ($row_tax1['tax_amount'] - $row_discount['discount_amount']);
				$purchase_cgst =   $row_cgst1['cgst'];
				$purchase_sgst = $row_sgst1['sgst'];
			} else if ($data['type'] == 'daybook') {

				$row_cgst2 = Yii::app()->db->createCommand("SELECT SUM(expense_cgst) as cgst FROM {$tblpx}expenses WHERE exp_id=" . $data['id'] . " AND FORMAT(expense_cgstp,2) = FORMAT(" . $value1['cgstpercent'] . ",2) AND FORMAT(expense_sgstp,2) = FORMAT(" . $value1['sgstpercent'] . ",2) AND (" . $newQuery1 . ") AND {$tblpx}expenses.bill_id IS NULL AND {$tblpx}expenses.invoice_id IS NULL AND {$tblpx}expenses.type=73")->queryRow();
				$row_sgst2 = Yii::app()->db->createCommand("SELECT SUM(expense_sgst) as sgst FROM {$tblpx}expenses WHERE exp_id=" . $data['id'] . " AND FORMAT(expense_cgstp,2) = FORMAT(" . $value1['cgstpercent'] . ",2) AND FORMAT(expense_sgstp,2) = FORMAT(" . $value1['sgstpercent'] . ",2) AND (" . $newQuery1 . ") AND {$tblpx}expenses.bill_id IS NULL AND {$tblpx}expenses.invoice_id IS NULL AND {$tblpx}expenses.type=73")->queryRow();
				$row_taxk = Yii::app()->db->createCommand("SELECT SUM(expense_amount) as tax_amount FROM {$tblpx}expenses WHERE exp_id=" . $data['id'] . " AND FORMAT(expense_cgstp,2) = FORMAT(" . $value1['cgstpercent'] . ",2) AND FORMAT(expense_sgstp,2) = FORMAT(" . $value1['sgstpercent'] . ",2) AND (" . $newQuery1 . ") AND {$tblpx}expenses.bill_id IS NULL AND {$tblpx}expenses.invoice_id IS NULL AND {$tblpx}expenses.type=73")->queryRow();
				$tax_amount = $row_taxk['tax_amount'];
				$purchase_cgst = $row_cgst2['cgst'];
				$purchase_sgst = $row_sgst2['sgst'];
			} else if ($data['type'] == 'dailyexpense') {

				$row_cgst2 = Yii::app()->db->createCommand("SELECT SUM(dailyexpense_cgst) as cgst FROM {$tblpx}dailyexpense WHERE 	dailyexp_id=" . $data['id'] . " AND FORMAT(dailyexpense_cgstp,2) = FORMAT(" . $value1['cgstpercent'] . ",2) AND FORMAT(dailyexpense_sgstp,2) = FORMAT(" . $value1['sgstpercent'] . ",2) AND (" . $newQuery2 . ") AND {$tblpx}dailyexpense.exp_type=73")->queryRow();
				$row_sgst2 = Yii::app()->db->createCommand("SELECT SUM(dailyexpense_sgst) as sgst FROM {$tblpx}dailyexpense WHERE 	dailyexp_id=" . $data['id'] . " AND FORMAT(dailyexpense_cgstp,2) = FORMAT(" . $value1['cgstpercent'] . ",2) AND FORMAT(dailyexpense_sgstp,2) = FORMAT(" . $value1['sgstpercent'] . ",2) AND (" . $newQuery2 . ") AND {$tblpx}dailyexpense.exp_type=73")->queryRow();
				$row_taxk = Yii::app()->db->createCommand("SELECT SUM(dailyexpense_amount) as tax_amount FROM {$tblpx}dailyexpense WHERE 	dailyexp_id=" . $data['id'] . " AND FORMAT(dailyexpense_cgstp,2) = FORMAT(" . $value1['cgstpercent'] . ",2) AND FORMAT(dailyexpense_sgstp,2) = FORMAT(" . $value1['sgstpercent'] . ",2) AND (" . $newQuery2 . ") AND {$tblpx}dailyexpense.exp_type=73")->queryRow();
				$tax_amount = $row_taxk['tax_amount'];
				$purchase_cgst = $row_cgst2['cgst'];
				$purchase_sgst = $row_sgst2['sgst'];
			} else if ($data['type'] == 'vendorpayment') {
				$row_cgst2 = Yii::app()->db->createCommand("SELECT SUM(cgst_amount) as cgst FROM {$tblpx}dailyvendors WHERE daily_v_id=" . $data['id'] . " AND FORMAT(cgst,2) = FORMAT(" . $value1['cgstpercent'] . ",2) AND FORMAT(sgst,2) = FORMAT(" . $value1['sgstpercent'] . ",2) AND (" . $newQuery3 . ")")->queryRow();
				$row_sgst2 = Yii::app()->db->createCommand("SELECT SUM(sgst_amount) as sgst FROM {$tblpx}dailyvendors WHERE daily_v_id=" . $data['id'] . " AND FORMAT(cgst,2) = FORMAT(" . $value1['cgstpercent'] . ",2) AND FORMAT(sgst,2) = FORMAT(" . $value1['sgstpercent'] . ",2) AND (" . $newQuery3 . ")")->queryRow();
				$row_taxk = Yii::app()->db->createCommand("SELECT SUM(amount) as tax_amount FROM {$tblpx}dailyvendors WHERE daily_v_id=" . $data['id'] . " AND FORMAT(cgst,2) = FORMAT(" . $value1['cgstpercent'] . ",2) AND FORMAT(sgst,2) = FORMAT(" . $value1['sgstpercent'] . ",2) AND (" . $newQuery3 . ")")->queryRow();
				$tax_amount = $row_taxk['tax_amount'];
				$purchase_cgst = $row_cgst2['cgst'];
				$purchase_sgst = $row_sgst2['sgst'];
			}

			$net_taxamount = $tax_amount;

			$net_sgst = $purchase_sgst;
			$net_cgst = $purchase_cgst;
			$net_amount += $net_taxamount + $purchase_sgst + $purchase_cgst;
		?>
			<td><?php echo (($tax_amount == 0) ? '' : Controller::money_format_inr($tax_amount, 2, 1)); ?> </td>
			<td><?php echo ($purchase_cgst == 0) ? '' : Controller::money_format_inr($purchase_cgst, 2, 1); ?></td>
			<td><?php echo ($purchase_sgst == 0) ? '' : Controller::money_format_inr($purchase_sgst, 2, 1); ?></td>
			<!-- <td><?php // echo ($row_igst['igst'] == 0)?'':number_format($row_igst['igst'],2);
						?></td> -->
		<?php }
		?>

		<?php
		$net_amount_igst = 0;
		$purchase_igst = 0;
		foreach ($purchase_head_igst as $key1 => $value1) {
			if ($data['type'] == 'bills') {
				$row_igst = Yii::app()->db->createCommand("SELECT SUM(billitem_igst) as igst FROM {$tblpx}billitem WHERE bill_id=" . $data['id'] . " AND FORMAT(billitem_igstpercent,2) = FORMAT(" . $value1['igstpercent'] . ",2)")->queryRow();
				$purchase_igst = $row_igst['igst'];
				$row_tax1 = Yii::app()->db->createCommand("SELECT SUM(billitem_amount) as tax_amount FROM {$tblpx}billitem WHERE bill_id=" . $data['id'] . " AND FORMAT(billitem_igstpercent,2) = FORMAT(" . $value1['igstpercent'] . ",2)")->queryRow();
				$row_discount = Yii::app()->db->createCommand("SELECT SUM(billitem_discountamount) as discount_amount FROM {$tblpx}billitem WHERE bill_id=" . $data['id'] . " AND FORMAT(billitem_igstpercent,2) = FORMAT(" . $value1['igstpercent'] . ",2)")->queryRow();
				$tax_amount = ($row_tax1['tax_amount'] - $row_discount['discount_amount']);
			} else if ($data['type'] == 'daybook') {
				$row_igst = Yii::app()->db->createCommand("SELECT SUM(expense_igst) as igst FROM {$tblpx}expenses WHERE exp_id=" . $data['id'] . " AND FORMAT(expense_igstp,2) = FORMAT(" . $value1['igstpercent'] . ",2) AND {$tblpx}expenses.bill_id IS NULL AND {$tblpx}expenses.invoice_id IS NULL AND {$tblpx}expenses.type=73 AND (" . $newQuery1 . ") AND expense_igst > 0")->queryRow();
				$purchase_igst = $row_igst['igst'];
				$row_taxk = Yii::app()->db->createCommand("SELECT SUM(expense_amount) as tax_amount FROM {$tblpx}expenses WHERE exp_id=" . $data['id'] . " AND FORMAT(expense_igstp,2) = FORMAT(" . $value1['igstpercent'] . ",2) AND (" . $newQuery1 . ") AND {$tblpx}expenses.bill_id IS NULL AND {$tblpx}expenses.invoice_id IS NULL AND {$tblpx}expenses.type=73 AND expense_igst > 0")->queryRow();
				$tax_amount = $row_taxk['tax_amount'];
			} else if ($data['type'] == 'dailyexpense') {
				$row_igst = Yii::app()->db->createCommand("SELECT SUM(dailyexpense_igst) as igst FROM {$tblpx}dailyexpense WHERE dailyexp_id=" . $data['id'] . " AND FORMAT(dailyexpense_igstp,2) = FORMAT(" . $value1['igstpercent'] . ",2) AND {$tblpx}dailyexpense.exp_type=73 AND (" . $newQuery2 . ")")->queryRow();
				$purchase_igst = $row_igst['igst'];
				$row_taxk = Yii::app()->db->createCommand("SELECT SUM(dailyexpense_amount) as tax_amount FROM {$tblpx}dailyexpense WHERE 	dailyexp_id=" . $data['id'] . " AND FORMAT(dailyexpense_igstp,2) = FORMAT(" . $value1['igstpercent'] . ",2) AND (" . $newQuery2 . ") AND {$tblpx}dailyexpense.exp_type=73")->queryRow();
				$tax_amount = $row_taxk['tax_amount'];
			} else if ($data['type'] == 'vendorpayment') {
				$row_igst = Yii::app()->db->createCommand("SELECT SUM(igst_amount) as igst FROM {$tblpx}dailyvendors WHERE daily_v_id=" . $data['id'] . " AND FORMAT(igst,2) = FORMAT(" . $value1['igstpercent'] . ",2) AND (" . $newQuery3 . ")")->queryRow();
				$purchase_igst = $row_igst['igst'];
				$row_taxk = Yii::app()->db->createCommand("SELECT SUM(amount) as tax_amount FROM {$tblpx}dailyvendors WHERE daily_v_id=" . $data['id'] . " AND FORMAT(igst,2) = FORMAT(" . $value1['igstpercent'] . ",2) AND (" . $newQuery3 . ")")->queryRow();
				$tax_amount = $row_taxk['tax_amount'];
			}

			$net_amount_igst += $purchase_igst + $tax_amount;
		?>
			<td><?php echo ($purchase_igst == 0) ? '' : Controller::money_format_inr($tax_amount, 2, 1); ?></td>
			<td><?php echo ($purchase_igst == 0) ? '' : Controller::money_format_inr($purchase_igst, 2, 1); ?></td>

		<?php
		} ?>
		<td></td>
		<td><?php echo Controller::money_format_inr($net_amount + $net_amount_igst, 2); ?></td>
	</tr>

</tbody>
<?php
if ($index == 0) {
?>
	<!-- <tfoot>
					   <th colspan="8" class="text-right"><b>Total</b></th>
                        <th><?php echo ($purchase_totalamount != '') ? Controller::money_format_inr($purchase_totalamount, 2, 1) : '0.00'; ?></th>
                        <?php
						$net_amount = 0;
						$net_taxamount = 0;
						$net_sgst = 0;
						$net_cgst = 0;
						foreach ($purchase_head as $key1 => $value1) {
							//if($data['type'] =='bills') {
							$row_cgst1 = Yii::app()->db->createCommand("SELECT SUM(billitem_cgst) as cgst FROM {$tblpx}billitem LEFT JOIN {$tblpx}bills ON {$tblpx}bills.bill_id ={$tblpx}billitem.bill_id WHERE {$tblpx}bills.bill_date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND FORMAT(billitem_cgstpercent,2) = FORMAT(" . $value1['cgstpercent'] . ",2) AND FORMAT(billitem_sgstpercent,2) = FORMAT(" . $value1['sgstpercent'] . ",2) AND billitem_cgstpercent > 0 AND billitem_sgstpercent > 0 AND (" . $newQuery . ")")->queryRow();
							$row_sgst1 = Yii::app()->db->createCommand("SELECT SUM(billitem_sgst) as sgst FROM {$tblpx}billitem  LEFT JOIN {$tblpx}bills ON {$tblpx}bills.bill_id ={$tblpx}billitem.bill_id WHERE {$tblpx}bills.bill_date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND FORMAT(billitem_cgstpercent,2) = FORMAT(" . $value1['cgstpercent'] . ",2) AND FORMAT(billitem_sgstpercent,2) = FORMAT(" . $value1['sgstpercent'] . ",2) AND billitem_cgstpercent > 0 AND billitem_sgstpercent > 0 AND (" . $newQuery . ")")->queryRow();
							$row_tax1 = Yii::app()->db->createCommand("SELECT SUM(billitem_amount) as tax_amount FROM {$tblpx}billitem LEFT JOIN {$tblpx}bills ON {$tblpx}bills.bill_id ={$tblpx}billitem.bill_id WHERE {$tblpx}bills.bill_date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND FORMAT(billitem_cgstpercent,2) = FORMAT(" . $value1['cgstpercent'] . ",2) AND FORMAT(billitem_sgstpercent,2) = FORMAT(" . $value1['sgstpercent'] . ",2) AND billitem_cgstpercent > 0 AND billitem_sgstpercent > 0 AND (" . $newQuery . ")")->queryRow();
							$row_discount = Yii::app()->db->createCommand("SELECT SUM(billitem_discountamount) as discount_amount FROM {$tblpx}billitem  LEFT JOIN {$tblpx}bills ON {$tblpx}bills.bill_id ={$tblpx}billitem.bill_id WHERE {$tblpx}bills.bill_date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND FORMAT(billitem_cgstpercent,2) = FORMAT(" . $value1['cgstpercent'] . ",2) AND FORMAT(billitem_sgstpercent,2) = FORMAT(" . $value1['sgstpercent'] . ",2) AND billitem_cgstpercent > 0 AND billitem_sgstpercent > 0 AND (" . $newQuery . ")")->queryRow();
							$tax_amount1 = ($row_tax1['tax_amount'] - $row_discount['discount_amount']);
							$bill_cgst = $row_cgst1['cgst'];
							$bill_sgst = $row_sgst1['sgst'];

							$row_cgst2 = Yii::app()->db->createCommand("SELECT SUM(expense_cgst) as cgst FROM {$tblpx}expenses WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND FORMAT(expense_cgstp,2) = FORMAT(" . $value1['cgstpercent'] . ",2) AND FORMAT(expense_sgstp,2) = FORMAT(" . $value1['sgstpercent'] . ",2) AND (" . $newQuery1 . ") AND {$tblpx}expenses.bill_id IS NULL AND {$tblpx}expenses.invoice_id IS NULL AND {$tblpx}expenses.type=73 AND (expense_sgstp > 0 AND expense_cgstp > 0)")->queryRow();
							$row_sgst2 = Yii::app()->db->createCommand("SELECT SUM(expense_sgst) as sgst FROM {$tblpx}expenses WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "'  AND FORMAT(expense_cgstp,2) = FORMAT(" . $value1['cgstpercent'] . ",2) AND FORMAT(expense_sgstp,2) = FORMAT(" . $value1['sgstpercent'] . ",2) AND (" . $newQuery1 . ") AND {$tblpx}expenses.bill_id IS NULL AND {$tblpx}expenses.invoice_id IS NULL AND {$tblpx}expenses.type=73 AND (expense_sgstp > 0 AND expense_cgstp > 0)")->queryRow();
							$row_tax2 = Yii::app()->db->createCommand("SELECT SUM(expense_amount) as tax_amount FROM {$tblpx}expenses WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "'  AND FORMAT(expense_cgstp,2) = FORMAT(" . $value1['cgstpercent'] . ",2) AND FORMAT(expense_sgstp,2) = FORMAT(" . $value1['sgstpercent'] . ",2) AND (" . $newQuery1 . ") AND {$tblpx}expenses.bill_id IS NULL AND {$tblpx}expenses.invoice_id IS NULL AND {$tblpx}expenses.type=73")->queryRow();
							$tax_amount2 = $row_tax2['tax_amount'];
							$exp_cgst = $row_cgst2['cgst'];
							$exp_sgst = $row_sgst2['sgst'];

							$row_cgst3 = Yii::app()->db->createCommand("SELECT SUM(dailyexpense_cgst) as cgst FROM {$tblpx}dailyexpense WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND FORMAT(dailyexpense_cgstp,2) = FORMAT(" . $value1['cgstpercent'] . ",2) AND FORMAT(dailyexpense_sgstp,2) = FORMAT(" . $value1['sgstpercent'] . ",2) AND (" . $newQuery2 . ") AND {$tblpx}dailyexpense.exp_type=73 AND (dailyexpense_sgstp > 0 AND dailyexpense_cgstp > 0)")->queryRow();
							$row_sgst3 = Yii::app()->db->createCommand("SELECT SUM(dailyexpense_sgst) as sgst FROM {$tblpx}dailyexpense WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "'  AND FORMAT(dailyexpense_cgstp,2) = FORMAT(" . $value1['cgstpercent'] . ",2) AND FORMAT(dailyexpense_sgstp,2) = FORMAT(" . $value1['sgstpercent'] . ",2) AND (" . $newQuery2 . ") AND {$tblpx}dailyexpense.exp_type=73 AND (dailyexpense_sgstp > 0 AND dailyexpense_cgstp > 0)")->queryRow();
							$row_tax3 = Yii::app()->db->createCommand("SELECT SUM(dailyexpense_amount) as tax_amount FROM {$tblpx}dailyexpense WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "'  AND FORMAT(dailyexpense_cgstp,2) = FORMAT(" . $value1['cgstpercent'] . ",2) AND FORMAT(dailyexpense_sgstp,2) = FORMAT(" . $value1['sgstpercent'] . ",2) AND (" . $newQuery2 . ") AND {$tblpx}dailyexpense.exp_type=73")->queryRow();
							$tax_amount3 = $row_tax3['tax_amount'];
							$daexp_cgst = $row_cgst3['cgst'];
							$daexp_sgst = $row_sgst3['sgst'];

							$row_cgst4 = Yii::app()->db->createCommand("SELECT SUM(cgst_amount) as cgst FROM {$tblpx}dailyvendors WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND FORMAT(cgst,2) = FORMAT(" . $value1['cgstpercent'] . ",2) AND FORMAT(sgst,2) = FORMAT(" . $value1['sgstpercent'] . ",2) AND (" . $newQuery3 . ") AND {$tblpx}dailyvendors.tax_amount >0 AND sgst >0 AND cgst >0")->queryRow();
							$row_sgst4 = Yii::app()->db->createCommand("SELECT SUM(sgst_amount) as sgst FROM {$tblpx}dailyvendors WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "'  AND FORMAT(cgst,2) = FORMAT(" . $value1['cgstpercent'] . ",2) AND FORMAT(sgst,2) = FORMAT(" . $value1['sgstpercent'] . ",2) AND (" . $newQuery3 . ") AND {$tblpx}dailyvendors.tax_amount >0 AND sgst >0 AND cgst >0")->queryRow();
							$row_tax4 = Yii::app()->db->createCommand("SELECT SUM(amount) as tax_amount FROM {$tblpx}dailyvendors WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "'  AND FORMAT(cgst,2) = FORMAT(" . $value1['cgstpercent'] . ",2) AND FORMAT(sgst,2) = FORMAT(" . $value1['sgstpercent'] . ",2) AND (" . $newQuery3 . ") AND sgst >0 AND cgst >0")->queryRow();
							$tax_amount4 = $row_tax4['tax_amount'];
							$vndr_cgst = $row_cgst4['cgst'];
							$vndr_sgst = $row_sgst4['sgst'];

							$net_taxamount += $tax_amount1 + $tax_amount2 + $tax_amount3 + $tax_amount4;
							$net_cgst += $bill_cgst + $exp_cgst + $daexp_cgst + $vndr_cgst;
							$net_sgst += $bill_sgst + $exp_sgst + $daexp_sgst + $vndr_sgst;

							//print_r($row_cgst);
							///}
							echo '<th>' . Controller::money_format_inr($tax_amount1 + $tax_amount2 + $tax_amount3 + $tax_amount4, 2, 1) . '</th>';
							echo '<th>' . Controller::money_format_inr($bill_cgst + $exp_cgst + $daexp_cgst + $vndr_cgst, 2, 1) . '</th>';
							echo '<th>' . Controller::money_format_inr($bill_sgst + $exp_sgst + $daexp_sgst + $vndr_sgst, 2, 1) . '</th>';
						}
						?>
                        <?php
						$net_igst = 0;
						$net_tax  = 0;
						foreach ($purchase_head_igst as $key1 => $value1) {
							$row_igst1 = Yii::app()->db->createCommand("SELECT SUM(billitem_igst) as igst FROM {$tblpx}billitem LEFT JOIN {$tblpx}bills ON {$tblpx}bills.bill_id ={$tblpx}billitem.bill_id WHERE {$tblpx}bills.bill_date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND FORMAT(billitem_igstpercent,2) = FORMAT(" . $value1['igstpercent'] . ",2) AND (" . $newQuery . ") AND billitem_igstpercent > 0")->queryRow();
							$row_tax1 = Yii::app()->db->createCommand("SELECT SUM(billitem_amount) as tax_amount FROM {$tblpx}billitem LEFT JOIN {$tblpx}bills ON {$tblpx}bills.bill_id ={$tblpx}billitem.bill_id WHERE {$tblpx}bills.bill_date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND FORMAT(billitem_igstpercent,2) = FORMAT(" . $value1['igstpercent'] . ",2) AND billitem_igstpercent > 0 AND (" . $newQuery . ")")->queryRow();
							$row_discount = Yii::app()->db->createCommand("SELECT SUM(billitem_discountamount) as discount_amount FROM {$tblpx}billitem  LEFT JOIN {$tblpx}bills ON {$tblpx}bills.bill_id ={$tblpx}billitem.bill_id WHERE {$tblpx}bills.bill_date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND FORMAT(billitem_igstpercent,2) = FORMAT(" . $value1['igstpercent'] . ",2) AND billitem_igstpercent > 0 AND (" . $newQuery . ")")->queryRow();
							$tax_amount1 = ($row_tax1['tax_amount'] - $row_discount['discount_amount']);

							$row_igst2 = Yii::app()->db->createCommand("SELECT SUM(expense_igst) as igst FROM {$tblpx}expenses WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND FORMAT(expense_igstp,2) = FORMAT(" . $value1['igstpercent'] . ",2) AND (expense_igstp >0) AND {$tblpx}expenses.bill_id IS NULL AND {$tblpx}expenses.invoice_id IS NULL AND {$tblpx}expenses.type=73 AND (" . $newQuery1 . ")")->queryRow();
							$row_tax2 = Yii::app()->db->createCommand("SELECT SUM(expense_amount) as tax_amount FROM {$tblpx}expenses WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "'  AND FORMAT(expense_igstp,2) = FORMAT(" . $value1['igstpercent'] . ",2) AND (" . $newQuery1 . ") AND {$tblpx}expenses.bill_id IS NULL AND {$tblpx}expenses.invoice_id IS NULL AND {$tblpx}expenses.type=73 AND (expense_igstp >0)")->queryRow();
							$tax_amount2 = $row_tax2['tax_amount'];

							$row_igst3 = Yii::app()->db->createCommand("SELECT SUM(dailyexpense_igst) as igst FROM {$tblpx}dailyexpense WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND FORMAT(dailyexpense_igstp,2) = FORMAT(" . $value1['igstpercent'] . ",2) AND (dailyexpense_igstp >0) AND {$tblpx}dailyexpense.exp_type=73 AND (" . $newQuery2 . ")")->queryRow();
							$row_tax3 = Yii::app()->db->createCommand("SELECT SUM(dailyexpense_amount) as tax_amount FROM {$tblpx}dailyexpense WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "'  AND FORMAT(dailyexpense_igstp,2) = FORMAT(" . $value1['igstpercent'] . ",2) AND (" . $newQuery2 . ") AND {$tblpx}dailyexpense.exp_type=73 AND (dailyexpense_igstp >0)")->queryRow();
							$tax_amount3 = $row_tax3['tax_amount'];


							$row_igst4 = Yii::app()->db->createCommand("SELECT SUM(igst_amount) as igst FROM {$tblpx}dailyvendors WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND FORMAT(igst,2) = FORMAT(" . $value1['igstpercent'] . ",2) AND (" . $newQuery3 . ") AND igst >0")->queryRow();
							$row_tax4 = Yii::app()->db->createCommand("SELECT SUM(amount) as tax_amount FROM {$tblpx}dailyvendors WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "'  AND FORMAT(igst,2) = FORMAT(" . $value1['igstpercent'] . ",2) AND (" . $newQuery3 . ") AND igst >0")->queryRow();
							$tax_amount4 = $row_tax4['tax_amount'];

							$net_tax  += $tax_amount1 + $tax_amount2 + $tax_amount3 + $tax_amount4;
							$net_igst += $row_igst2['igst'] + $row_igst1['igst'] + $row_igst3['igst'] + $row_igst4['igst'];

							echo '<th>' . Controller::money_format_inr($tax_amount1 + $tax_amount2 + $tax_amount3 + $tax_amount4, 2, 1) . '</th>';
							echo '<th>' . Controller::money_format_inr($row_igst2['igst'] + $row_igst1['igst'] + $row_igst3['igst'] + $row_igst4['igst'], 2, 1) . '</th>';
						}
						?>
                        <th></th>
                        <th><?php echo Controller::money_format_inr(($net_taxamount + $net_cgst + $net_sgst + $net_igst + $net_tax), 2, 1); ?></th>
				 </tr>
				 </tfoot> -->
<?php } ?>