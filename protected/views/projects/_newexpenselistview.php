<?php
/* @var $this ProjectsController */
/* @var $data Projects */
?>
<?php if ($index == 0) { ?>
    <thead class="entry-table sticky-thead">
        <tr>
            <th>Sl No.</th>
            <th>Project Name</th>
            <th>Start Date</th>
            <th>Created Date</th>
        </tr>
    </thead>
<?php } ?>
<tr>
    <td><?php echo $index + 1; ?></td>
    <td><?php echo $data->name;?></td>
    <td><?php echo date('d-m-Y', strtotime($data->start_date)); ?></td>
    <td><?php echo date('d-m-Y', strtotime($data->created_date)); ?></td>
</tr>