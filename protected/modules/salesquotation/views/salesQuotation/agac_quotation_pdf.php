<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/agacpdfstyle.css">
<?php 
    
    $alternative_image_path = Yii::app()->theme->basePath . '/images/quotation-file/logo.png';
    $app_root = YiiBase::getPathOfAlias('webroot');
    $site_hosturl = str_replace('/index.php','', Yii::app()->createAbsoluteUrl('/'));
    $image_base_url=Yii::app()->theme->baseUrl .'/assets/default/logo.png'; 
    $theme_asset_url = $site_hosturl."/themes/assets/" ;
    if(file_exists(Yii::app()->theme->basePath .'/images/quotation-file/logo.png')){
        $image_url=Yii::app()->theme->baseUrl .'/images/quotation-file/logo.png';
    }else if( file_exists($app_root . "/themes/assets/logo.png") ){
        $image_url=$theme_asset_url .'logo.png';
    }else{
        $image_url=$theme_asset_url .'default-logo.png';
    }
   
?>
<div class="bg_doted">
    <!--page 1 -->
    <div class="banner_holder">
        <div id="content">
            <img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/quotation-file/main.png" alt="img" height="1300" />
        </div>
        <div id="background"></div>
    </div>
    <!-- page 2 -->
    <div class="p-5">
        <div class="red_border p_2 detail_sec">
            <table class="detail_table bg-white">
                <tr>
                    <!-- client -->
                    <td width="150" class="bg_light_grey text_title border-bottom pb_1">CLIENT</td>
                    <!-- details -->
                    <td class="bg_light_grey border-bottom">
                        <div class="text_value"><?php echo $client; ?></div>
                        <?php $location_name = Location::model()->findByPk($model->location_id); ?>
                        <div class="text_value">
                            <?php
                            echo (isset($model->address) && $model->address !== '')? $model->address:$location_name['name'];
                            ?>
                        </div>
                        <div><span class="text_title">Ph: </span><span class="text_value"><?php echo $model->phone_no; ?></span></div>
                    </td>
                    <!-- AGAC LOGO AND CONTACT DETAILS -->
                    <td width="50%" align="right" class="text-right" rowspan="5">
                        <img src="<?php echo $image_url?>" alt="Agac" height="75" />
                        <div class="text_value mt_2">
                            <div>Agac Interiors</div>
                            <div>Near Suzuki Bike showroom</div>
                            <div>Kuttoor, Thiruvalla</div>
                        </div>
                        <div class="mt_2">
                            <span class="text_title">Ph: </span>
                            <span class="text_value">+91 9400485915</span>
                        </div>
                        <div class="mt_2">
                            <span class="text_title">Email- </span>
                            <a class="text_value" href="mailto:office1agac@gmail.com">office1agac@gmail.com</a>
                        </div>
                        <div class="mt_2">
                            <a class="text_value" href="www.agacinteriors.com" target="_blak">www.agacinteriors.com</a>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="bg_light_grey text_title border-bottom">REF. NO</td>
                    <td class="text_value bg_light_grey border-bottom"><?php echo isset($model->invoice_no) ? $model->invoice_no : ''; ?></td>
                </tr>
                <tr>
                    <td class="text_title bg_light_grey border-bottom">DATE</td>
                    <td class="text_value bg_light_grey border-bottom"><?php echo isset($model->date_quotation) ? date('d-m-Y', strtotime($model->date_quotation)) : ''; ?></td>
                </tr>
                <tr>
                    <td class="text_title bg_light_grey">SUBJECT</td>
                    <td class="text_value bg_light_grey pb_1">ESTIMATION FOR RESIDENTIAL INTERIOR WORK ,<?php echo $location_name['name']; ?></td>
                </tr>
                <tr>
                    <td class="text_title bg_light_grey"></td>
                    <td class="text_value bg_light_grey pb_1"></td>
                </tr>
            </table>
        </div>
    </div>

    <!-- page 3 -->
    <div class="dot_bg">
        <div class="table_h">
            <table class="item_table">
                <tbody>                    
                    <tr class="header">
                        <th width="8%">SNO</th>
                        <th width="12%">WORK TYPE</th>                        
                        <th width="43%">DESCRIPTION OF ITEM AND MATERIAL</th>
                        <th width="8%">UNIT</th>
                        <th width="7%">QTY</th>
                        <th width="13%">MRP</th>
                        <th width="13%">DISCOUNT PRICE</th>
                    </tr>
                    <?php
                    $amount_afetr_discount = 0;
                    $amount_afetr_discount_extra = 0;
                    if (!empty($itemmodel)) {
                        $i = 1;
                        $mrp = 0;
                        $amount_afetr_discount = 0;
                        $modeldata = $itemmodel;
                        $previous_cat = '';
                        foreach ($itemmodel as $key => $item) {
                            $datas = $item['items_list'];
                            $sub_items = $item['sub_items'];
                            $category_id = $item['category_details']['category_id'];
                            $category_name = QuotationCategoryMaster::model()->findByPk($category_id);
                            $i = 1;
                            ?>
                            <tr class="catehead">
                                <td colspan="7">
                                    <?php echo ($item['category_details']['category_label'] != '') ? $item['category_details']['category_label'] : $category_name['name']; ?>
                            </tr>
                            <?php
                            foreach ($datas as $key => $value) {
                                $unit = Unit::model()->findByPk($value['unit']);
                                $work_type_label = QuotationWorktypeMaster::model()->findByPk($value['work_type']);
                                ?>
                                <tr class="item_body">
                                    <td><?php echo $i; ?></td>
                                    <td class="text_title"><?php echo ($value['worktype_label'] != '') ? $value['worktype_label'] : $work_type_label['name']; ?></td>
                                    <?php if ($work_type_label['template_id'] == 1) {
                                        $item_master = QuotationItemMaster::model()->findByPk($value['item_id']);

                                        $specification = Specification::model()->findByPk($item_master['shutter_material_id']);
                                        $brand = Brand::model()->findByPk($specification ['brand_id']);
                                        $shutter_material = $brand['brand_name'] . ' - ' . $specification ['specification'];

                                        $specification = Specification::model()->findByPk($item_master['carcass_material_id']);
                                        $brand = Brand::model()->findByPk($specification ['brand_id']);
                                        $carcass_material = $brand['brand_name'] . ' - ' . $specification ['specification'];
                                        ?>
                                        
                                    <?php }  ?>
                                    <td><?php echo $value['description']; ?></td>
                                    <td><?php echo $unit['unit_name']; ?></td>
                                    <td><?php echo $value['quantity_nos']; ?></td>
                                    <td class="text-right nowrap"><?php echo $value['mrp']; ?></td>
                                    <td class="text-right nowrap"><?php echo $value['amount_after_discount']; ?></td>
                                </tr>

                                <!--END case for multiple items -- rowspan = total items + 1 -->
                                <?php
                                $i++;
                                $mrp = $mrp + $value['mrp'];
                                $amount_afetr_discount = $amount_afetr_discount + $value['amount_after_discount'];
                            }if(!empty($sub_items)){
                            ?>
                            <tr>
                                <td colspan="7">
                                    <b><?php echo $sub_items[0]['subitem_label']; ?></b>
                                </td>
                            </tr>
                            <?php
                            }
                            foreach ($sub_items as $key => $value) {
                                $unit = Unit::model()->findByPk($value['unit']);
                                ?>
                                <tr class="item_body">
                                    <td><?php echo $i; ?></td>
                                    <td class="text_title"><?php echo ($value['worktype_label'] != '') ? $value['worktype_label'] : $work_type_label['name']; ?></td>                                    
                                    <td><?php echo $value['description']; ?></td>
                                    <td class="uppercase"><?php echo $unit['unit_name']; ?></td>
                                    <td><?php echo $value['quantity_nos']; ?></td>
                                    <td class="text-right nowrap"><?php echo $value['mrp']; ?></td>
                                    <td class="text-right nowrap"><?php echo $value['amount_after_discount']; ?></td>
                                </tr>

                                <!--END case for multiple items -- rowspan = total items + 1 -->
                                <?php
                                $i++;
                                $mrp = $mrp + $value['mrp'];
                                $amount_afetr_discount = $amount_afetr_discount + $value['amount_after_discount'];
                            }
                            $previous_cat = $category_id;
                        }
                        ?> 
                        <tr class="catetotal item_body">
                            <th colspan="5" class="text-right">Total Amount</th>
                            <td colspan="2" class="text-right nowrap" bgcolor="#fff"><?php echo Yii::app()->Controller->money_format_inr($mrp, 2) ?></td>
                        </tr>
                        <tr  class="item_body">
                            <th colspan="5"  class="text-right">Discount Amount</th>
                            <td colspan="2" class="text-right nowrap"><?php echo Yii::app()->Controller->money_format_inr(($mrp -$amount_afetr_discount) , 2) ?></td>
                        </tr>
                        
                    <?php } ?>
                    <?php if (!empty($item_extra)) { ?>
                        <tr>
                            <td colspan="7">Extra Work</td>
                        </tr>

                        <?php
                        $j = 1;
                        $mrp_extra = 0;
                        $amount_afetr_discount_extra = 0;
                        foreach ($item_extra as $key => $value) {
                            $unit = Unit::model()->findByPk($value['unit']);
                            ?>
                            <tr class="item_body">
                                <td> <?php echo $j; ?></td>
                                <td> <?php echo $value['description']; ?></td>                                
                                <td class="uppercase"><?php echo $unit['unit_name']; ?></td>
                                <td> <?php echo $value['quantity_nos']; ?></td>
                                <td class="text-right nowrap"> <?php echo $value['mrp']; ?></td>
                                <td class="text-right nowrap"> <?php echo $value['amount_after_discount']; ?></td>
                            </tr>
                            <?php
                            $mrp_extra = $mrp_extra + $value['mrp'];
                            $amount_afetr_discount_extra = $amount_afetr_discount_extra + $value['amount_after_discount'];
                            $j++;
                        }
                        ?>

                        <tr class="item_body">
                            <th colspan="5" class="text-right">Total Amount</th>
                            <td colspan="2" class="text-right nowrap"><?php echo Yii::app()->Controller->money_format_inr($mrp_extra, 2) ?></td>
                        </tr>
                        <tr class="item_body">
                            <th colspan="5" class="text-right">Discount Amount</th>
                            <td colspan="2"class="text-right nowrap"><?php echo Yii::app()->Controller->money_format_inr(($mrp_extra -$amount_afetr_discount_extra) , 2) ?></td>
                        </tr>
                   
                    <?php } ?> 
                    <tr class="item_body">
                        <th colspan="5" class="text-right">Grand Total (Limited Offer) :</th>
                        <td colspan="2" class="text-right nowrap"><?php echo Yii::app()->Controller->money_format_inr(($amount_afetr_discount + $amount_afetr_discount_extra +$total_tax) , 2) ?></td>
                    </tr>
                    <tr class="item_body">
                        <th style="color:red" class=" text-right" colspan="9">Final Discount will be Provided only after the finalization of quotation</th>
                    </tr>
                    <tr class="item_body">
                        <th colspan="5" class="text-right">Final Sale Amount :</th>
                        <td colspan="2" class="text-right">
                            <input type="text" style="width:20%" readonly>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>



      <!-- <pagebreak /> -->
    <!-- page 4 -->
    <!-- MATERIAL DESCRIPTION -->
    <div class="dot_bg ">
        <div class="table_h">
            <table class="item_table">
                <tbody>
                    <tr>
                        <th colspan="3" class="title">MATERIALS DESCRIPTION</th>
                    </tr>
                    <?php foreach ($materials as $materialid) { ?>
                        <tr>
                            <?php if (is_array($materialid)) {
                                foreach ($materialid as $child) { ?>
                                <td class="text_title">/ <?php echo $child; ?></td>
                            <?php } ?>
                        </tr>
                    <?php }
                    } ?>

                </tbody>
            </table>
            <br /><br />
            <!-- ITEM NOT INCLUDED IN THE ESTIMATE -->
            <table class="item_table no-border">
                <tbody>
                    <tr>
                        <th class="title">ITEM NOT INCLUDED IN THE ESTIMATE</th>
                    </tr>
                    <tr>
                        <td>* Electrical Appliances</td>
                    </tr>
                    <tr>
                        <td>* Plumping and Electrical work</td>
                    </tr>
                    <tr>
                        <td>* Civil work if any</td>
                    </tr>
                    <tr>
                        <td>* Demolishing work</td>
                    </tr>
                    <tr>
                        <td>* Putty, Painting and Tile work</td>
                    </tr>
                </tbody>
            </table>
            <table class="item_table no-border">
                <tbody>
                    <tr>
                        <th colspan="3" class="title">SERVICES</th>
                    </tr>
                    <tr>
                        <td>* We will attend your services within 2 days from the date of service request</td>
                    </tr>
                    <tr>
                        <td>
                            * Replacement and repair of products/ product parts which are not covered under warranty will be
                            charged extra.
                        </td>
                    </tr>
                    <tr>
                        <td>* Service charges will be chargeable as per the estimate</td>
                    </tr>
                </tbody>
            </table>
            <br /><br />
            <!-- ABOUT OUR PRODUCT WARRANTY -->
            <table class="item_table no-border">
                <tbody>
                    <tr>
                        <th colspan="3" class="title">ABOUT OUR PRODUCT WARRANTY</th>
                    </tr>
                    <tr>
                        <th>AGAC INTERIORS Provides Warranty for the product as follows:</th>
                    </tr>
                    <tr>
                        <td>* 5 Year service warranty for all laminated marine ply under normal usage.</td>
                    </tr>
                    <tr>
                        <td>* 5 Year replacement warranty for all laminated marine ply wood under normal usage.</td>
                    </tr>
                    <tr>
                        <td>
                            * 5 Year service warranty for hard wares (Hinges and sliders) against mechanical and manufacturing defects Provided by the manufacturer.
                        </td>
                    </tr>
                    <tr>
                        <td>
                            * 15-year replacement warranty for kitchen basket against mechanical and manufacturing defects provide by the manufacturer
                        </td>
                    </tr>
                    <tr>
                        <td>* Products which are not included in the above doesnt come under the scope of warranty claim.</td>
                    </tr>
                    <tr>
                        <td>* The client has to submit the original bill (estimation and cash receipt copy) during the time of warranty claim</td>
                    </tr>
                    <tr>
                        <td>* Above mentioned warranty and services will be given only after full settlement of payment</td>
                    </tr>
                </tbody>
            </table>
            <br>
            <table class="item_table no-border">
                <thead>
                    <tr>
                        <th class="subtitle">AGAC INTERIORS Warranty will not be applicable on:</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>* Alteration or modification of the product.</td>
                    </tr>
                    <tr>
                        <td>* Third party fabric, upholstery, foam, accessory or third-party material applied to products</td>
                    </tr>
                    <tr>
                        <td>* Damage caused by a carrier other than AGAC interiors</td>
                    </tr>
                    <tr>
                        <td><b><u>Damage caused by water, includes: -</u></b></td>
                    </tr>
                    
                        <tr>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;* Due to flood or any other natural calamity,</td>
                        </tr>
                        <tr>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;* Water leakage (without proper maintenance) in sink or wash area</td>
                        </tr>
                    <tr>
                        <td>*  Damages due to rough use are not include.</td>
                    </tr>
                    <tr>
                        <td>* Product not installed by AGAC INTERIORS</td>
                    </tr>
                </tbody>
            </table>
            <br /><br />
            <!-- TERMS AND CONDITIONS -->
            <table class=" no-border terms_table">
                <tbody>
                    <tr>
                        <th class="title">TERMS AND CONDITIONS</th>
                    </tr>
                    <tr>
                        <td>
                            <?php
                            $terms = $terms = Yii::app()->db->createCommand("SELECT * FROM `jp_terms_conditions` WHERE `template_id` = 1")->queryRow();
                            echo $terms['terms_and_conditions'];
                            ?>
                        </td>
                    </tr>
                </tbody>
            </table>
            <br /><br />
            <table class="item_table">
                <tbody>
                    <tr>
                        <th class="title">WHY CHOOSE AGAC INTERIORS?</th>
                    </tr>
                    <tr>
                        <td>TRUSTED BY CLIENTS SINCE 2011</td>
                    </tr>
                    <tr>
                        <td>HIGH QUALITY PRODUCT</td>
                    </tr>
                    <tr>
                        <td>ON TIME DELIVERY</td>
                    </tr>
                    <tr>
                        <td>100% CUSTOMER SATISFACTION</td>
                    </tr>
                    <tr>
                        <td>LATEST TECHNOLOGY</td>
                    </tr>
                    <tr>
                        <td>LIFETIME WARRANTY AND SERVICE</td>
                    </tr>
                </tbody>
            </table>
            <br /><br />
            <table class="item_table no-border">
                <tbody>
                    <tr>
                        <th colspan="2" class="title">BANK DETAILS</th>
                    </tr>
                    <tr>
                        <td width="100" class="text_title">Name:</td>
                        <td>AGAC Interiors</td>
                    </tr>
                    <tr>
                        <td class="text_title">Bank:</td>
                        <td>Canara Bank</td>
                    </tr>
                    <tr>
                        <td class="text_title">Branch:</td>
                        <td>Mallapally</td>
                    </tr>
                    <tr>
                        <td class="text_title">A/c no:</td>
                        <td>2358201000195</td>
                    </tr>
                    <tr>
                        <td class="text_title">IFSC Code:</td>
                        <td>CNRB0002358</td>
                    </tr>
                    <tr>
                        <td class="text_title">GST NO:</td>
                        <td>32AZAPA7570JIZL</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

