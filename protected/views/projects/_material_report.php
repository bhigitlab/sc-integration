<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.min.js"></script>
<?php
/* @var $this ExpensesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'Clients',
);
$page = Yii::app()->request->getParam('Clients_page');
if ($page != '') {
    Yii::app()->user->setReturnUrl($page);
} else {
    Yii::app()->user->setReturnUrl(0);
}
?>


<div class="container" id="project">
    <div class="expenses-heading mb-10">
        <h3>Material Report</h3>
    </div>
    <div class="page_filter clearfix ">
        <form id="projectform" action="<?php $url = Yii::app()->createAbsoluteUrl("projects/expenselist"); ?>"
            method="POST">

            <div class="search-form">
                <div class="row">
                    <div class="form-group col-xs-12 col-sm-3 col-md-2">
                        <label for="project">Projects</label>
                        <?php
                        $fromdate = (isset($date_from) && !empty($date_from)) ? date('d-M-Y', strtotime($date_from)) : "";
                        $todate = (isset($date_to) && !empty($date_to)) ? date('d-M-Y', strtotime($date_to)) : "";
                        $search_item_id = isset($search_item_id) ? $search_item_id : "";
                        ?>
                        <?php
                        echo CHtml::dropDownList('project_id', 'project_id', CHtml::listData(Projects::model()->findAll(array(
                            'select' => array('pid, name'),
                            'order' => 'name',
                            'distinct' => true
                        )), 'pid', 'name'), array(
                            'class' => 'select_box',
                            'empty' => 'Select Project    ',
                            'style' => 'width:200px;',
                            'id' => 'project_id',
                            'options' => array($project_id => array('selected' => true)),
                            'ajax' => array(
                                'url' => array('projects/getitem'),
                                'type' => 'POST',
                                'data' => array('project_id' => 'js:this.value'),
                                'update' => '#item_id',
                            )
                        ));
                        ?>
                    </div>
                    <div class="form-group col-xs-12 col-sm-3 col-md-2">
                        <label for="type">Expense Head</label>
                        <?php
                        $type_id = isset($_REQUEST['type_id']) ? $_REQUEST['type_id'] : "";
                        echo CHtml::dropDownList('type_id', $type_id, CHtml::listData(ExpenseType::model()->findAll(array(
                            'select' => array('type_id, type_name'),
                            'order' => 'type_name',
                            'distinct' => true
                        )), 'type_id', 'type_name'), array(
                            'class' => 'select_box',
                            'empty' => 'Select Expensehead    ',
                            'style' => 'width:200px;',
                            'id' => 'project_id',
                            'options' => array($expensehead => array('selected' => true)),
                            'ajax' => array(
                                'url' => array('projects/getexpensehead'),
                                'type' => 'POST',
                                'data' => array('expensehead' => 'js:this.value'),
                                'update' => '#item_id',
                            )
                        ));
                        ?>
                    </div>
                    <div class="form-group col-xs-12 col-sm-3 col-md-2">
                        <!-- <div class="display-flex nowrap-white-space"> -->
                        <!-- <div class="margin-top-5"> -->
                        <?php echo CHtml::label('Date From', ''); ?>
                        <!-- </div> -->
                        <?php echo CHtml::textField('date_from', $fromdate, array("id" => "date_from", "placeholder" => "From", "readonly" => true, "class" => "form-control")); ?>
                        <!-- </div> -->
                    </div>
                    <div class="form-group col-xs-12 col-sm-3 col-md-2">
                        <!-- <div class="display-flex nowrap-white-space"> -->
                        <!-- <div class="margin-top-5"> -->
                        <?php echo CHtml::label('Date To ', ''); ?>
                        <!-- </div> -->
                        <?php echo CHtml::textField('date_to', $todate, array("id" => "date_to", "placeholder" => "To", "readonly" => true, "class" => "form-control")); ?>
                        <!-- </div> -->
                    </div>
                    <div class="form-group col-xs-12 col-sm-3 col-md-2">
                        <label for="item">Item</label>
                        <select id="item_id" class="select_box" name="item_id" class="sel">
                            <option value="">Select Item</option>
                            <?php
                            if (!empty($specification)) {
                                foreach ($specification as $key => $value) {
                                    ?>
                                    <option value="<?php echo $value['id'] ?>" <?php echo ($value['id'] == $search_item_id) ? 'selected' : ''; ?>>
                                        <?php echo $value['data']; ?>
                                    </option>
                                <?php }
                            } ?>
                        </select>
                    </div>
                    <div class="form-group col-xs-12 col-sm-3 col-md-2 text-sm-left text-right">
                        <label class="d-sm-block d-none">&nbsp;</label>
                        <div>
                            <?php echo CHtml::submitButton('GO', array('id' => 'btSubmit', 'class' => 'btn btn-sm btn-primary')); ?>
                            <?php echo CHtml::resetButton('Clear', array('onclick' => 'javascript:location.href="' . $this->createUrl('materialconsumptionreport') . '"', 'class' => 'btn btn-sm btn-default')); ?>
                        </div>
                    </div>
                </div>
            </div>

        </form>
    </div>

    <div class="warehousestock">
        <div class="" id="newlist">
            <?php
            $this->renderPartial(
                '_material_list',
                array(
                    'receipt_total' => $receipt_total,
                    'dispatch_total' => $dispatch_total,
                    'net_total' => $net_total,
                    'result_data' => $result_data
                )
            );
            ?>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $(".select_box").select2();
    });
    $("#date_from").datepicker({ dateFormat: 'dd-mm-yy' });
    $("#date_to").datepicker({ dateFormat: 'dd-mm-yy' });
</script>