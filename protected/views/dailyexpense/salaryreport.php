<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/tableHeadFixer.js"></script>
<style>
 .grandtotal td{
        /* border-bottom:1px solid black !important; */
        background-color:rgb(247, 240, 240) !important;
    } 
	</style>
<?php
Yii::app()->clientScript->registerScript('search', "

    $('#btSubmit').click(function(){

            $('#pettyform').submit();

    });
");
$tblpx = Yii::app()->db->tablePrefix;

$user = Users::model()->findByPk(Yii::app()->user->id);
$arrVal = explode(',', $user->company_id);
$newQuery = "";
$newQuery1 = "";
foreach($arrVal as $arr) {
    if ($newQuery) $newQuery .= ' OR';
    if ($newQuery1) $newQuery1 .= ' OR';
    $newQuery .= " FIND_IN_SET('".$arr."', id)";
    $newQuery1 .= " FIND_IN_SET('".$arr."', company_id)";
}
?>

<div class="container" id="project">
<div class="expenses-heading">
    <div class="clearfix">
        <div class="pull-right">
            <button type="button" class="btn btn-info mt-0 mb-10">
                <a href="<?php echo Yii::app()->createUrl('dailyexpense/savetopdfsalary') ?>" style="text-decoration: none; color: white;">
                    <i class="fa fa-file-pdf-o button-icon" aria-hidden="true"></i> 
                </a>
            </button>
            <button type="button" class="btn btn-info mt-0 mb-10 margin-right-5">
                <a href="<?php echo Yii::app()->createUrl('dailyexpense/savetoexcelsalary') ?>" style="text-decoration: none; color: white;">
                    <i class="fa fa-file-excel-o button-icon" aria-hidden="true"></i> 
                </a>
            </button>
            
        </div>
        <h3>Salary Report</h3>
    </div>
   
</div>
<div class="page_filter clearfix custom-form-style">
<form id="pettyform" action="<?php $url = Yii::app()->createAbsoluteUrl("dailyexpense/pettycash"); ?>" method="POST">
    <div class="row">
        <div class="form-group col-xs-12 col-md-2">
            <label for="employee">Employee</label>
            <?php //echo CHtml::label('Employee : ', ''); ?>
            <?php
            echo CHtml::dropDownList('employee_id', 'employee_id', CHtml::listData(Users::model()->findAll(array(
            'select' => array('userid, concat_ws(" ",first_name,last_name) as first_name'),
            'order' => 'userid DESC',
            //'condition' => 'company_id ='.Yii::app()->user->company_id.' AND user_type NOT IN (1)',
            'condition' => '('.$newQuery1.') AND user_type NOT IN (1)',
            'distinct' => true
            )), 'userid', 'first_name'), array('empty'=>'Select Employee','id' => 'employee_id','class'=>'form-control','style'=>'padding: 2.5px 0px;', 'options' => array($employee_id => array('selected' => true))));
            ?>
        </div>
            <div class="form-group col-xs-12 col-md-2">
            <label for="company">Company</label>
            <?php
            echo CHtml::dropDownList('company_id', 'company_id', CHtml::listData(Company::model()->findAll(array(
            'select' => array('id, name'),
            'order' => 'id DESC',
            'condition' => '('.$newQuery.')',
            'distinct' => true
            )), 'id', 'name'), array('empty'=>'Select Company','id' => 'company_id','class'=>'form-control','style'=>'padding: 2.5px 0px;', 'options' => array($company_id => array('selected' => true))));
            ?>
        </div>
        <div class="form-group col-xs-12 col-md-2">
            <!-- <div class="display-flex nowrap-white-space">
                <div class="margin-top-5"> -->
                    <?php echo CHtml::label('From :', ''); ?>
                <!-- </div> -->
                <?php echo CHtml::textField('date_from', (!empty($date_from) ?  date('d-M-Y', strtotime($date_from)) : ''), array('placeholder'=>'Date From',"id" => "date_from", "readonly" => true,'class'=>'form-control','autocomplete'=>'off','style'=>'display:inline-block;')); ?>
            <!-- </div> -->
        </div>
        <div class="form-group col-xs-12 col-md-2">
            <!-- <div class="display-flex nowrap-white-space">
                <div class="margin-top-5"> -->
                    <?php echo CHtml::label('To :', ''); ?>
                <!-- </div> -->
                <?php echo CHtml::textField('date_to', (!empty($date_to) ? date('d-M-Y', strtotime($date_to)) : ''), array('placeholder'=>'Date To',"id" => "date_to", "readonly" => true,'class'=>'form-control','autocomplete'=>'off','style'=>'display:inline-block;')); ?>
            <!-- </div> -->
        </div>
        <div class="form-group col-xs-12 col-md-2 margin-top-20 filter_btns pull-right">
            <div class="pull-right">
                <?php echo CHtml::submitButton('GO', array('id' => 'btSubmit','class' => 'btn btn-sm btn-primary')); ?>
                <?php echo CHtml::resetButton('Clear', array('onclick' => 'javascript:location.href="'. $this->createUrl('dailyexpense/salaryreport').'"','class' => 'btn btn-sm btn-default')); ?>
            </div>
        </div>
    </div>
</form>
</div>
    <?php if ((!empty($dailyexpense_salary) || !empty($dailyexpense_advance)) && ($employee_id != 0)) { ?>
        <?php
        $users =  Users::model()->findByPk($employee_id);
        ?>

         <div id="contenthtml" class="contentdiv">

        <div class="pull-right">

                <a style="cursor:pointer;" id="download" class="save_btn" href="<?php echo $this->createAbsoluteUrl('dailyexpense/salarypdfdetails', array("employee_id"=>$employee_id, 'date_from' => $date_from, 'date_to' => $date_to, 'company_id' => $company_id)) ?>">SAVE AS PDF</a>

                <a style="cursor:pointer;" id="download" class="save_btn" href="<?php echo $this->createAbsoluteUrl('dailyexpense/salaryexceldetails', array("employee_id"=>$employee_id, 'date_from' => $date_from, 'date_to' => $date_to, 'company_id' => $company_id)) ?>">SAVE AS EXCEL</a>

            <br/>

            </div>

        <h2><center>Salary Report of <b><?php echo $users->first_name.' '.$users->last_name;?></b></center></h2>
        <br><br>
        <div class="container-fluid">



                <table class="table table-bordered ">

                    <tr>
                         <td colspan="2"><b>Employee Name: </b><?php echo $users->first_name.' '.$users->last_name; ?></td>
<!--                     <td><b>Vendor Name: </b><?php //echo $name;?></td>
                     <td></td>-->
                     </tr>
                     <tr>
                     <td colspan="2" class="whiteborder">&nbsp;</td>
                     </tr>

                     <?php
                     $advance_amount = 0;
                     if(!empty($dailyexpense_advance)) {
                     ?>
                     <tr>
                         <td><b>Salary Advance</b></td>
                        <td align="right"><b>Amount</b></td>

                     </tr>


                        <?php
                    foreach ($dailyexpense_advance as $value) {

                        ?>

                        <tr>

                            <td><?php echo date('d-M-Y', strtotime($value['date'])); ?></td>

                            <td align="right" border="1"><?php echo Controller::money_format_inr($value['dailyexpense_paidamount'],2); ?></td>

                        <?php $advance_amount += $value['dailyexpense_paidamount']; ?>

                        </tr>

                        <?php }

                    ?>


                    <tr>
                        <td><b>Net Amount</b></td>
                        <td align="right"><?php  echo '<b>' .Controller::money_format_inr($advance_amount,2). '</b>'; ?></td>

                        <!--<td ></td>-->
                    </tr>
                     <?php } ?>

                    <tr>
                        <td colspan="2" class="whiteborder">&nbsp;</td>
                    </tr>

                    <?php
                    $salary_amount = 0;
                    if (!empty($dailyexpense_salary)){
                    ?>
                    <tr>
                         <td><b>Salary Paid</b></td>
                        <td align="right"><b>Amount</b></td>

                     </tr>
                        <?php
                    foreach ($dailyexpense_salary as $value) {
                        ?>

                        <tr>

                            <td><?php echo date('d-M-Y', strtotime($value['date'])); ?></td>

                            <td align="right" border="1"><?php echo Controller::money_format_inr($value['dailyexpense_paidamount'],2); ?></td>

                        <?php $salary_amount += $value['dailyexpense_paidamount']; ?>

                        </tr>

                        <?php }

                    ?>


                    <tr>
                        <td><b>Net Amount</b></td>
                        <td align="right"><?php  echo '<b>' .Controller::money_format_inr($salary_amount,2). '</b>'; ?></td>
                    </tr>
                    <?php } ?>

                        <tr>
                            <td colspan="2" class="whiteborder">&nbsp;</td>
                        </tr>
                    <tr>


                         <tr>
                        <td><b>Total</b></td>
                        <td align="right"><?php  echo '<b>' .Controller::money_format_inr($salary_amount+$advance_amount,2). '</b>'; ?></td>
                    </tr>






                </table>

            </div>

        </div>

    <?php } ?>

<?php if ((empty($dailyexpense_salary) && empty($dailyexpense_advance)) && ($employee_id != 0)) {

    echo "<br><table class='table'><thead><tr><th>Sl No</th><th>Employee Name</th><th>Salary Advance</th><th>Salary Paid</th><th>Total</th></tr></thead><tr><td colspan='5' class='text-center'>No Result Found</td></tr></table>";
}
 ?>

     <?php if ((empty($dailyexpense_salary) && empty($dailyexpense_advance)) && ($employee_id == 0)) { ?>

        <div id="contenthtml" class="contentdiv">
            <!-- <div class="clearfix">
                <div class="pull-right">
                    <a style="cursor:pointer; " class="save_btn"  href="<?php echo Yii::app()->createUrl('dailyexpense/savetopdfsalary') ?>">SAVE AS PDF</a>
                    <a style="cursor:pointer; " class="save_btn"  href="<?php echo Yii::app()->createUrl('dailyexpense/savetoexcelsalary') ?>">SAVE AS EXCEL</a>
                     <br><br>
                </div>
            </div> -->
            <div style="position:relative;">
            <div id="parent" style="margin-top:10px;">
            <table class="table" id="fixTable">

                <thead>
                    <tr>
                        <th>Sl No.</th>
                        <th>Employee Name</th>
                        <th>Company</th>
                        <th align="right" style="text-align:right">Salary Advance</th>
                        <th align="right" style="text-align:right">Salary Paid</th>
                        <th align="right" style="text-align:right">Total</th>
                    </tr>
                    <?php
                    /******************** */
                    $t_advance = 0;
                    $t_salary =0;
                    $t_total = 0;
                    $user = Users::model()->findByPk(Yii::app()->user->id);
                    $arrVal = explode(',', $user->company_id);
                    $newQuery1 = "";
                    foreach($arrVal as $arr) {
                        if ($newQuery1) $newQuery1 .= ' OR';
                        $newQuery1 .= " FIND_IN_SET('".$arr."', ".$tblpx."dailyexpense.company_id)";
                    }
                    $employee = Yii::app()->db->createCommand("SELECT *,".$tblpx."users.company_id as companyid FROM ".$tblpx."dailyexpense LEFT JOIN ".$tblpx."users ON ".$tblpx."dailyexpense.employee_id = ".$tblpx."users.userid LEFT JOIN ".$tblpx."company_expense_type ON ".$tblpx."dailyexpense.exp_type_id = ".$tblpx."company_expense_type.company_exp_id  WHERE (".$newQuery1.") AND ".$tblpx."dailyexpense.exp_type=73 AND (".$tblpx."company_expense_type.name='SALARY ADVANCE' OR ".$tblpx."company_expense_type.name='SALARY') AND ".$tblpx."dailyexpense.dailyexpense_paidamount > 0 AND ".$tblpx."users.user_type NOT IN (1) GROUP BY ".$tblpx."users.userid")->queryAll();
                    foreach($employee as $key=> $value) {
                        $advance_salary = Yii::app()->db->createCommand("SELECT SUM(".$tblpx."dailyexpense.dailyexpense_paidamount) as amount FROM ".$tblpx."dailyexpense LEFT JOIN ".$tblpx."users ON ".$tblpx."dailyexpense.employee_id= ".$tblpx."users.userid LEFT JOIN ".$tblpx."company_expense_type ON ".$tblpx."dailyexpense.exp_type_id = ".$tblpx."company_expense_type.company_exp_id   WHERE ".$tblpx."dailyexpense.exp_type =73 AND ".$tblpx."dailyexpense.employee_id = ".$value['userid']." AND (".$newQuery1.") AND ".$tblpx."company_expense_type.name='SALARY ADVANCE'")->queryRow();
    
                        $salary = Yii::app()->db->createCommand("SELECT SUM(".$tblpx."dailyexpense.dailyexpense_paidamount) as amount FROM ".$tblpx."dailyexpense LEFT JOIN ".$tblpx."users ON ".$tblpx."dailyexpense.employee_id= ".$tblpx."users.userid LEFT JOIN ".$tblpx."company_expense_type ON ".$tblpx."dailyexpense.exp_type_id = ".$tblpx."company_expense_type.company_exp_id   WHERE ".$tblpx."dailyexpense.exp_type =73 AND ".$tblpx."dailyexpense.employee_id = ".$value['userid']." AND (".$newQuery1.") AND ".$tblpx."company_expense_type.name='SALARY'")->queryRow();
    
    
                        $t_advance += $advance_salary['amount'];
                        $t_salary += $salary['amount'];
                        $t_total += $t_advance+$t_salary;
                    }

                  //$employee = Yii::app()->db->createCommand("SELECT * FROM ".$tblpx."users WHERE  company_id =".Yii::app()->user->company_id." AND user_type NOT IN (1) ORDER BY userid DESC")->queryAll();
                 


                    /******************* */


                    ?>
                    <tr class="grandtotal">
                         <td colspan="3" align="right"><b>Total</b></td>
                         <td align="right"><b><?php  echo Controller::money_format_inr($t_advance,2); //echo Controller::money_format_inr($totalcredit,0,".",""); ?></b></td>
                         <td align="right"><b><?php  echo Controller::money_format_inr($t_salary,2); //echo Controller::money_format_inr($totaldebit,0,".",""); ?></b></td>
                         <td align="right"><b><?php  echo Controller::money_format_inr($t_advance+$t_salary,2);//echo Controller::money_format_inr($totalremaining,0,".",""); ?></b></td>
                     </tr>
                </thead>
                 <tbody>
                <?php
                  $t_advance = 0;
                  $t_salary =0;
                  $t_total = 0;

                   
                  foreach($employee as $key=> $value) {
                    $advance_salary = Yii::app()->db->createCommand("SELECT SUM(".$tblpx."dailyexpense.dailyexpense_paidamount) as amount FROM ".$tblpx."dailyexpense LEFT JOIN ".$tblpx."users ON ".$tblpx."dailyexpense.employee_id= ".$tblpx."users.userid LEFT JOIN ".$tblpx."company_expense_type ON ".$tblpx."dailyexpense.exp_type_id = ".$tblpx."company_expense_type.company_exp_id   WHERE ".$tblpx."dailyexpense.exp_type =73 AND ".$tblpx."dailyexpense.employee_id = ".$value['userid']." AND (".$newQuery1.") AND ".$tblpx."company_expense_type.name='SALARY ADVANCE'")->queryRow();

                    $salary = Yii::app()->db->createCommand("SELECT SUM(".$tblpx."dailyexpense.dailyexpense_paidamount) as amount FROM ".$tblpx."dailyexpense LEFT JOIN ".$tblpx."users ON ".$tblpx."dailyexpense.employee_id= ".$tblpx."users.userid LEFT JOIN ".$tblpx."company_expense_type ON ".$tblpx."dailyexpense.exp_type_id = ".$tblpx."company_expense_type.company_exp_id   WHERE ".$tblpx."dailyexpense.exp_type =73 AND ".$tblpx."dailyexpense.employee_id = ".$value['userid']." AND (".$newQuery1.") AND ".$tblpx."company_expense_type.name='SALARY'")->queryRow();


                    $t_advance += $advance_salary['amount'];
                    $t_salary += $salary['amount'];
                    $t_total += $t_advance+$t_salary;
                ?>
                <tr>
                    <td><?php echo $key+1; ?></td>
                    <td><?php echo $value['first_name'].' '.$value['last_name']; ?></td>
                    <td>
                        <?php
                        $companyname = array();
                        $user = Users::model()->findByPk(Yii::app()->user->id);
                        $comVal = explode(',', $user->company_id);
                        $arrVal = explode(',', $value['companyid']);
                        foreach($arrVal as $arr) {
                           if(in_array($arr, $comVal)){
                            $value = Company::model()->findByPk($arr);
                            if($value){
                                array_push($companyname, $value->name);
                            }
                           }
                        }
                        echo implode(', ', $companyname);
                        ?>
                    </td>
                    <td align="right"><?php echo Controller::money_format_inr($advance_salary['amount'],2) ; ?></td>
                    <td align="right"><?php echo Controller::money_format_inr($salary['amount'],2) ; ?></td>
                    <td align="right"><?php echo Controller::money_format_inr($advance_salary['amount']+$salary['amount'],2) ; ?></td>
                </tr>

                  <?php }  ?>
                 </tbody>

                 <!-- <tfoot> -->
<!--
-->                    
 <!-- <tr>
                         <td colspan="3" align="right"><b>Total</b></td>
                         <td align="right"><b><?php  echo Controller::money_format_inr($t_advance,2); //echo Controller::money_format_inr($totalcredit,0,".",""); ?></b></td>
                         <td align="right"><b><?php  echo Controller::money_format_inr($t_salary,2); //echo Controller::money_format_inr($totaldebit,0,".",""); ?></b></td>
                         <td align="right"><b><?php  echo Controller::money_format_inr($t_advance+$t_salary,2);//echo Controller::money_format_inr($totalremaining,0,".",""); ?></b></td>
                     </tr> -->
                 <!-- </tfoot> -->


            </table>
            </div>
<div style="position: absolute;right: 3px;top: -19px;">Total <?php echo count($employee);?> results</div>
</div>
        </div>


        <?php
    }

?>

</div>
<style>
     #parent{max-height: 500px;}
    thead th, tfoot td{background: #eee;}
    .table{margin-bottom: 0px;}
    .table>thead>tr>th, .table>tbody>tr>th, .table>tfoot>tr>th, .table>thead>tr>td, .table>tbody>tr>td, .table>tfoot>tr>td{
        border-bottom:0px;
    }
</style>
<script>
    $(document).ready(function () {
        $("#parent").css('max-height',$(window).height() - $("#myNavbar").height() - $(".page_filter").height() - (2*$('footer').height()));
        $("#fixTable").tableHeadFixer({'head' : true,'foot':true});
        $( "#date_from" ).datepicker({
            dateFormat: 'dd-M-yy',
            changeYear: true,
            changeMonth: true,
            maxDate: $("#date_to").val()
        });
        $( "#date_to" ).datepicker({
            dateFormat: 'dd-M-yy',
             changeYear: true,
             changeMonth: true
        });
        $("#date_from").change(function() {
            $("#date_to").datepicker('option', 'minDate', $(this).val());
        });
        $("#date_to").change(function() {
            $("#date_from").datepicker('option', 'maxDate', $(this).val());
        });
    });
</script>
