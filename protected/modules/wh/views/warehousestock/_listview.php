<?php
$tblpx = Yii::app()->db->tablePrefix;
$second_unit = '';
$second_quantity = '';
$coversion_factor = 0;
$priority = [];
$base_unit = $data->GetItemunit($data->warehousestock_itemid);

if ($base_unit != null) {
  $basewarehouse_unit = $base_unit;
} else {
  $basewarehouse_unit = $data->GetwarehouseItemunitid($data['warehousestock_unit']);
}
$priority = $data->checkpriority($data->warehousestock_itemid, $basewarehouse_unit);
if (count($priority) == 0) {
  $unit = $data['warehousestock_unit'];
  $quantity = $data['warehousestock_initial_quantity'];
} else {
  if ($base_unit != $priority['conversion_unit']) {
    $second_unit = $priority['conversion_unit'];
    $coversion_factor = $priority['conversion_factor'];
    $second_quantity = $data['warehousestock_initial_quantity'] * $coversion_factor;
    $unit = $data['warehousestock_unit'] . "(" . $second_unit . ")";
    $quantity = $data['warehousestock_initial_quantity'] . "(" . $second_quantity . ")";
  } else {
    $unit = $data['warehousestock_unit'];
    $quantity = $data['warehousestock_initial_quantity'];
  }
}
if ($index == 0) {
?>
  <thead>
    <tr>

      <th>Warehouse</th>
      <th>Date</th>
      <th>Item</th>
      <th>Unit</th>
      <th>Batch</th>
      <!-- <th>Expense Head</th> -->


      <th>Quantity</th>
    </tr>
  </thead>
<?php } ?>
<tr id="<?php echo $index; ?>" class="">
  <!-- <td><?php echo $index + 1; ?></td> -->
  <td><?php echo $data->warehouse['warehouse_name']; ?></td>
  <td><?php echo date('d-m-Y',  strtotime($data['warehousestock_date'])); ?></td>
  <td><?php
      $tblpx = Yii::app()->db->tablePrefix;
      $specsql = "SELECT id, cat_id,brand_id, specification, unit "
							. " FROM {$tblpx}specification "
							. " WHERE id=" . $data->warehousestock_itemid . "";
			$specification  = Yii::app()->db->createCommand($specsql)->queryRow();
			$cat_sql = "SELECT * FROM {$tblpx}purchase_category "
							. " WHERE id='" . $specification['cat_id'] . "'";
			$parent_category = Yii::app()->db->createCommand($cat_sql)->queryRow();

			if ($specification['brand_id'] != NULL) {
							$brand_sql = "SELECT brand_name "
								. " FROM {$tblpx}brand "
								. " WHERE id=" . $specification['brand_id'] . "";
							$brand_details = Yii::app()->db->createCommand($brand_sql)->queryRow();
							$brand = '-' . $brand_details['brand_name'];
			} else {
							$brand = '';
			}
      $spc_details = $parent_category['category_name'] . $brand . '-' . $specification['specification'];
      echo $spc_details;
			?></td>
  <td><?php echo $unit; ?></td>
  <td><?php echo $data['batch']; ?></td>
  <td><?php echo $quantity; ?>
    <div class="warehouseid" id="<?php echo $data['warehousestock_warehouseid']; ?>"></div>
    <div class="itemid" id="<?php echo $data['warehousestock_itemid']; ?>"></div>
  </td>
</tr>