<div class="container" id="expense">
<div>
<h3>Material Entries</h3>
        <?php $this->widget('zii.widgets.CListView', array(
            'dataProvider' => $model->search(),
            'viewData' => array(),
            'itemView' => '_view', 'template' => '<div class="clearfix"><div class="pull-right">{summary}</div><div class="pull-left">{sorter}</div></div><div id="parent" class="subq_parent"><table cellpadding="10" class="table tab1" id="subqtntbl">{items}</table></div>',
            'enableSorting' => true,
            
        )); ?>
</div>
</div>
