<?php

/**
 * This is the model class for table "{{materials}}".
 *
 * The followings are the available columns in table '{{materials}}':
 * @property integer $id
 * @property string $material_name
 * @property string $material_unit
 */
class Materials extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{materials}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            array('material_name, material_unit, specification', 'required'),
            array('material_name', 'unique'),
            array('material_unit, specification', 'safe'),
            array('material_name, material_unit, specification', 'length', 'max' => 255),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, material_name, material_unit, specification', 'safe', 'on' => 'search'),
        );
	}
	
	public function getMaterialUnitArray()
    {
        if (is_string($this->material_unit)) {
            return explode(',', $this->material_unit);
        }
        return [];
    }
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'material_name' => 'Material Name',
			'material_unit' => 'Material Unit',
			'specification' => 'Specification',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('material_name',$this->material_name,true);
		$criteria->compare('material_unit',$this->material_unit,true);
		$criteria->compare('specification', $this->specification, true);
		return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination' => false
        ));
		
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Materials the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
