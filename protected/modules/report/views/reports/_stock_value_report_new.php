<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/tableHeadFixer.js"></script>
<style>
    .notfound {
        display: none;
    }

    .panel-gray {
        box-shadow: 0px 3px 8px 0px rgba(0, 0, 0, 0.25);
    }

    .panel {
        border: 1px solid #ddd;
    }

    .panel-heading {
        background-color: #eee;
        height: 40px;
    }

    .addRow label {
        display: inline-block;
    }

    input[type="radio"] {
        margin: 4px 4px 0;
    }

    .page-body h3 {
        margin: 4px 0px;
        color: inherit;
        text-align: left;
    }

    a.button {
        background-color: #6a8ec7;
        display: block;
        padding: 5px;
        color: #fff;
        cursor: pointer;
        float: right;
        border: 1px solid #6a8ec8;
    }

    .dataTables_wrapper .dataTables_paginate .paginate_button.current,
    .dataTables_wrapper .dataTables_paginate .paginate_button.current:hover {
        color: #fff !important;
    }

    .dataTables_wrapper .dataTables_paginate .paginate_button:hover,
    .dataTables_wrapper .dataTables_paginate .paginate_button.disabled:hover,
    .dataTables_wrapper .dataTables_paginate .paginate_button.disabled:active {
        color: #fff !important;
    }

    .list-view .sorter {
        margin: 0px;
    }

    h4 {
        background-color: #fafafa;
        padding: 8px;

    }

    .dataTables_wrapper.no-footer .dataTables_scrollBody {
        border-bottom: none;
    }

    s table.dataTable {
        border-collapse: collapse;
    }

    #warehousestock-entry {
        max-height: 350px;
        overflow: auto;
    }

    .table {
        margin-bottom: 0;
    }

    #fixtable thead tr td {
        background-color: #eee;
        z-index: 1;
    }

    #fixtable tfoot tr td {
        background-color: #eee;
        z-index: 1;
    }

    .imd_div2 {
        width: 77px;
        height: 77px;
        position: relative
    }

    .popover {
        max-width: 400px;
    }

    img {
        max-height: 200px;
    }

    .pop {
        cursor: pointer;
    }

    /* .imd_div2 img:hover{    
    transform: scale(3.5);    
    position: absolute;
    z-index: 2;
} 
table tbody tr:first-child td:nth-child(2).imd_div2 img:hover{
    top: 83px;
}
.imd_div2 img.img_border:hover{
    box-shadow: 0 2px 4px rgba(0,0,0,0.2);
}*/
</style>
<?php
/* @var $this ExpensesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'Clients',
);
$page = Yii::app()->request->getParam('Clients_page');
if ($page != '') {
    Yii::app()->user->setReturnUrl($page);
} else {
    Yii::app()->user->setReturnUrl(0);
}
?>


<div class="container" id="project">
    <div class="clearfix">
        <h2>Warehouse Stock Value Detail Report</h2>
    </div>
    <div id="addclient" style="display:none;">

    </div>
    <div class="clearfix">
        <?php $this->renderPartial('_searchStockValueReportnew', array('model' => $model, 'specification' => $specification)); ?>
        <div id="search" class="pull-right">
            <label>Search</label>
            <input type="text" id="myInput" placeholder="Search" title="Type in a name" class="form-control" />
        </div>
    </div>
    <br>
    <div class="warehousestock">
        <div id="warehousestock-entry">
            <?php
            $this->renderPartial('_stock_value_report_data_new', array('result_data' => $result_data));

            ?>
        </div>
    </div>

</div>




<script>
    function closeaction() {
        $('#addclient').slideUp('slow');
    }
    $(document).ready(function() {


        $('.addclient').click(function() {
            // alert('hi');
            $.ajax({
                type: "POST",
                url: "<?php echo $this->createUrl('warehousestock/create&layout=1') ?>",
                success: function(response) {
                    $("#addclient").html(response).slideDown();
                }
            });
        });
        $(document).delegate('.editstock', 'click', function() {
            var id = $(this).attr('data-id');
            $.ajax({
                type: "POST",
                url: "<?php echo $this->createUrl('warehousestock/update&id=') ?>" + id,
                success: function(response) {
                    $("#addclient").html(response).slideDown();
                    $("#warehouseStockId").val(id);
                }
            });
        });

        $(document).on('click', '.deletestock', function() {
            var id = $(this).attr('data-id');
            $.ajax({
                type: "POST",
                url: "<?php echo $this->createUrl('warehousestock/delete&id=') ?>" + id,
                success: function(result) {
                    $("#addclient").slideUp();
                    if (result == 1) {

                    } else {
                        $("#newlist").html(result);
                        $("#myTable").dataTable({
                            "scrollY": "300px",
                            "scrollCollapse": true,
                            "paging": false,
                            "columnDefs": [{
                                "targets": [0],
                                "searchable": false
                            }, ]
                        });
                    }
                }
            });
        });

        jQuery(function($) {
            $('#client').on('keydown', function(event) {
                if (event.keyCode == 13) {
                    $("#clientsearch").submit();
                }
            });
        });
    });



    $(document).ready(function() {

        $('#myInput').keyup(function() {
            var myinput_val = $('#myInput').val();
            if (myinput_val == "") {
                $("#grand_total_value").show();
            } else {
                $("#grand_total_value").hide();
            }
            // Search Text
            var search = $(this).val();

            // Hide all table tbody rows
            $('table tbody tr').hide();

            // Count total search result
            var len = $('table tbody tr:not(.notfound) td:nth-child(1):contains("' + search + '")').length;
            var len1 = $('table tbody tr:not(.notfound) td:nth-child(3):contains("' + search + '")').length;
            var len2 = $('table tbody tr:not(.notfound) td:nth-child(5):contains("' + search + '")').length;
            var len3 = $('table tbody tr:not(.notfound) td:nth-child(10):contains("' + search + '")').length;

            if (len > 0 || len1 > 0 || len2 > 0 || len3 > 0) {
                // Searching text in columns and show match row
                $('table tbody tr:not(.notfound) td:contains("' + search + '")').each(function() {
                    $(this).closest('tr').show();
                });
            } else {
                $('.notfound').show();
            }

        });
        $.expr[":"].contains = $.expr.createPseudo(function(arg) {
            return function(elem) {
                return $(elem).text().toUpperCase().indexOf(arg.toUpperCase()) >= 0;
            };
        });


    });
</script>


<?php
Yii::app()->clientScript->registerScript('myjavascript', '
     $(document).ready(function(){
     ajaxdatatable();
    function ajaxdatatable(){
    
       $("#myTable").dataTable( {
       
            "scrollY": "300px",
            "scrollCollapse": true,
            "paging": false,
            
             "columnDefs": [ {
      "targets": [0],
      "searchable": false
    },
    //{ "bSortable": false, "aTargets": [-1]  }
    ]
                  
	} );
	
	
     }
        
	});
      
        
    ');
?>