<?php
if (!isset($_GET['exportpdf'])) {
    ?>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.min.js"></script>

    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/message/css/jquery.message.css">
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/message/js/jquery.message.js"></script>
    <style>
        .lefttdiv {
            float: left;
        }

        .purchase-title {
            border-bottom: 1px solid #ddd;
        }

        .purchase_items h3 {
            font-size: 18px;
            color: #333;
            margin: 0px;
            padding: 0px;
            text-align: left;
            padding-bottom: 10px;
        }

        .purchase_items {
            padding: 15px;
            box-shadow: 0 0 13px 1px rgba(0, 0, 0, 0.25);
        }

        .purchaseitem {
            display: inline-block;
            margin-right: 20px;
        }

        .purchaseitem last-child {
            margin-right: 0px;
        }

        .purchaseitem label {
            font-size: 12px;
        }

        .remark {
            display: none;
        }

        .padding-box {
            padding: 3px 0px;
            min-height: 17px;
            display: inline-block;
        }

        th {
            height: auto;
        }

        .quantity,
        .rate {
            max-width: 80px;
        }

        .text_align {

            text-align: center;
        }


        *:focus {
            border: 1px solid #333;
            box-shadow: 0 0 6px 1px rgba(0, 0, 0, 0.25);
        }

        .text-right {
            text-align: right;
        }
    </style>
    <script>
        $(function () {
            $("#datepicker").datepicker({
                dateFormat: 'dd-mm-yy'
            }).datepicker("setDate", new Date());

        });
    </script>

    <br /><br />
    <div class="container">
        <div class="invoicemaindiv">
            <div class="add-btn pull-right">
                <?php if ((isset(Yii::app()->user->role) && (in_array('/subcontractorbill/create', Yii::app()->user->menuauthlist)))) { ?>
                    <a href="<?php echo $this->createUrl('subcontractorbill/createquotationbill&quotation_id=' . $model->scquotation_id) ?>" class="button btn btn-primary">Generate SC Bill</a>
                    <?php
                }
                if ((isset(Yii::app()->user->role) && (in_array('/subcontractor/pdfdownload', Yii::app()->user->menuauthlist)))) {
                    echo CHtml::link('Download PDF', Yii::app()->request->requestUri . "&exportpdf=1", array('class' => 'button btn btn-primary pdfbtn'));
                }
                ?>
            </div>

            <h2 class="purchase-title">Quotation By Quantity * Rate</h2>
            <br>
            <form id="pdfvals1" method="post" action="">
                <input type="hidden" name="purchaseview">
                <div class="row">
                    <div class="col-md-3">

                        <label>COMPANY : </label>
                        <?php echo $company_name; ?>
                    </div>

                    <div class="col-md-3">
                        <label>PROJECT : </label>
                        <?php echo isset($model->project_id) ? $model->projects->name : ''; ?>
                    </div>
                    <div class="col-md-3">
                        <label>SUBCONTRACTOR : </label>
                        <?php echo isset($model->subcontractor_id) ? $model->subcontractor->subcontractor_name : ''; ?>
                    </div>
                    <div class="col-md-3">
                        <label>DATE : </label>
                        <?php echo isset($model->scquotation_date) ? date('d-m-Y', strtotime($model->scquotation_date)) : ''; ?>
                    </div>
                    <div class="col-md-3">
                        <label>SC Quotation No : </label>
                        <?php echo $model->scquotation_no; ?>
                    </div>
                    <div class="col-md-3">
                        <label>Description : </label>
                        <?php echo $model->scquotation_decription; ?>
                    </div>
                </div>

                <?php
            } else {
                $company_address = Company::model()->findBypk($model->company_id);
                ?>
                <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/pdfstyle.css">
                <div class="container">
                    <table border=0>
                        <tbody>
                            <tr>
                                <td class="img-hold">
                                    <img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/logo.png" alt="img" style='margin-top:-45px;' />
                                </td>
                                <td class="text-center">
                                    <h1><?php echo Yii::app()->name ?></h1>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table border=0>
                        <tbody>
                            <tr>
                                <td class="text-center">
                                    <p><?php echo ((isset($company_address['address']) && !empty($company_address['address'])) ? ($company_address['address']) : ""); ?><br>
                                        PIN : <?php echo ((isset($company_address['pincode']) && !empty($company_address['pincode'])) ? ($company_address['pincode']) : ""); ?>,&nbsp;
                                        PHONE : <?php echo ((isset($company_address['phone']) && !empty($company_address['phone'])) ? ($company_address['phone']) : ""); ?><br>
                                        EMAIL : <?php echo ((isset($company_address['email_id']) && !empty($company_address['email_id'])) ? ($company_address['email_id']) : ""); ?> <br>
                                        GST NO: <?php echo isset($company_address["company_gstnum"]) ? $company_address["company_gstnum"] : ''; ?></p><br>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <h4 class="page_heading">WORK ORDER</h4>
                    <table class="details">
                        <tr>
                            <td><label>VENDOR : </label><b><?php echo isset($model->subcontractor_id) ? $model->subcontractor->subcontractor_name : ''; ?></b></td>
                            <td><label>Sign Date : </label><b><?php echo isset($model->scquotation_date) ? date('d-m-Y', strtotime($model->scquotation_date)) : ''; ?></b></td>
                        </tr>
                        <tr>
                            <td><label>SITE : </label><b></b></td>
                            <td><label>Completion Date: : </label><b></b></td>
                        </tr>
                        <tr>
                            <td><label>Tel : </label><b><?php echo isset($model->subcontractor_id) ? $model->subcontractor->subcontractor_phone : ''; ?></b></td>
                        </tr>
                    </table>
                    <br>
                    <h4 class="text-center">Work Details</h4>
                </div>
            <?php }
            ?>
            <?php
            if (!isset($_GET['exportpdf'])) {
                ?>
                <div class="clearfix"></div>
                <br><br>

                <div id="table-scroll" class="table-scroll">
                    <div id="faux-table" class="faux-table" aria="hidden"></div>
                    <div id="table-wrap" class="table-wrap">
                        <div class="table-responsive">
                        <?php } ?>
                        <table border="1" class="table">
                            <thead>
                                <tr>
                                    <th>Sl.No</th>
                                    <th>Quotation Description</th>
                                    <th>Date</th>
                                    <th>Quantity</th>
                                    <th>Unit</th>
                                    <th>Rate</th>
                                    <th>Amount</th>
                                </tr>
                            </thead>
                            <tbody class="addrow">
                                <?php
                                $k = 1;
                                if (!empty($item_model)) {
                                    foreach ($item_model as $items) {
                                        $datas = $items['datas'];
                                        ?>
                                        <tr class="blue_bg">
                                            <td colspan="7">
                                                <?php echo $items['expense_head_title']; ?>
                                            </td>
                                        </tr>
                                        <?php
                                        foreach ($datas as $item) {
                                            ?>
                                            <tr class="tr_class">
                                                <td><?php echo $k;
                                            ?></td>
                                                <td><?php echo $item['item_description'];
                                            ?></td>
                                                <td><?php echo $item['item_date'];
                                            ?></td>
                                                <td>
                                                    <?php echo $item['item_quantity'];
                                                    ?>
                                                </td>
                                                <td class="text-right">
                                                    <?php echo $item['item_unit'];
                                                    ?>
                                                </td>
                                                <td class="text-right">
                                                    <?php echo isset($item['item_rate']) ? Yii::app()->Controller->money_format_inr($item['item_rate'], 2) : '';
                                                    ?>
                                                </td>
                                                <td class="text-right">
                                                    <?php echo isset($item['item_amount']) ? Yii::app()->Controller->money_format_inr($item['item_amount'], 2) : '';
                                                    ?>
                                                </td>
                                            </tr>
                                            <?php
                                            $k++;
                                        }
                                        ?>
                                        <?php
                                    }
                                }
                                ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th style="text-align: right;" colspan="6" class="text-right">TOTAL: </th>

                                    <th class="text-right">
                                        <div id="grand_total"><?php echo Yii::app()->Controller->money_format_inr($model->scquotation_amount, 2); ?></div>
                                    </th>
                                </tr>
                                <?php
                                if (isset($_GET['exportpdf'])) {
                                    ?>
                                    <tr>
                                        <th class="text-center" colspan="7">In words: <?php echo Yii::app()->Controller->displaywords($model->scquotation_amount) ?></th>
                                    </tr>
                                <?php } ?>


                            </tfoot>
                        </table>
                        <?php if (!isset($_GET['exportpdf'])) { ?>
                        </div>

                    </div>
                </div>
            <?php } else {
                ?>
                <table>
                    <tr>
                        <td>Approved BY <br><br>
                            <b>For <?php echo Yii::app()->name ?></b></td>
                        <td>Agreed BY <br><br>
                            <?php echo isset($model->subcontractor_id) ? $model->subcontractor->subcontractor_name : ''; ?>
                        </td>
                    </tr>
                </table>
            <?php }
            ?>
            <?php
            if (!isset($_GET['exportpdf'])) {
                ?>

            </form>
        </div>
        <style>
            .error_message {
                color: red;
            }

            a.pdf_excel {
                background-color: #6a8ec7;
                display: inline-block;
                padding: 8px;
                color: #fff;
                border: 1px solid #6a8ec8;
            }

            .invoicemaindiv th,
            .invoicemaindiv td {
                padding: 10px;
            }
        </style>

    </div>

    <script>
        (function () {
            var mainTable = document.getElementById("main-table");
            if (mainTable !== null) {
                var tableHeight = mainTable.offsetHeight;
                if (tableHeight > 380) {
                    var fauxTable = document.getElementById("faux-table");
                    document.getElementById("table-wrap").className += ' ' + 'fixedON';
                    var clonedElement = mainTable.cloneNode(true);
                    clonedElement.id = "";
                    fauxTable.appendChild(clonedElement);
                }
            }
        })();
    </script>
<?php } ?>