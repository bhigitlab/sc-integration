<?php

/**
 * This is the model class for table "{{tax_slabs}}".
 *
 * The followings are the available columns in table '{{tax_slabs}}':
 * @property integer $id
 * @property double $tax_slab_value
 * @property double $cgst
 * @property double $sgst
 * @property double $igst
 * @property integer $status
 * @property integer $created_by
 * @property string $created_date
 * @property integer $updated_by
 * @property string $updated_date
 *
 * The followings are the available model relations:
 * @property Users $createdBy
 * @property Users $updatedBy
 */
class TaxSlabs extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{tax_slabs}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('tax_slab_value', 'required'),
			array('id, status, created_by, updated_by', 'numerical', 'integerOnly' => true),
			array('tax_slab_value, cgst, sgst, igst', 'numerical'),
			array('created_date, updated_date', 'safe'),
			// array('tax_slab_value', 'validateValue'),
			array('set_default', 'checkDefault'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, tax_slab_value, cgst, sgst, igst, status, created_by, created_date, updated_by, updated_date', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'createdBy' => array(self::BELONGS_TO, 'Users', 'created_by'),
			'updatedBy' => array(self::BELONGS_TO, 'Users', 'updated_by'),
		);
	}

	public function validateValue($attribute, $params)
	{
		if (!empty($this->tax_slab_value) && ($this->tax_slab_value > 0)) {
			$tax_sum = 0;
			if (!empty($this->cgst)) {
				$tax_sum += $this->cgst;
				if ($tax_sum > $this->tax_slab_value)
					$this->addError('cgst', 'Number has to be greater than 0');
			}
			if (!empty($this->sgst)) {
				$tax_sum += $this->sgst;
				if ($tax_sum > $this->tax_slab_value)
					$this->addError('sgst', 'Number has to be greater than 0');
			}
			if (!empty($this->igst)) {
				$tax_sum += $this->igst;
				if ($tax_sum > $this->tax_slab_value)
					$this->addError('igst', 'Number has to be greater than 0');
				if (!empty($this->sgst))
					$this->addError('sgst', 'sgst not required');
				if (!empty($this->cgst))
					$this->addError('cgst', 'cgst not required');
			}
			if (!empty($this->igst) && !empty($this->sgst) && !empty($this->cgst)) {
				$this->addError('sgst', 'Error');
			}
		}
	}

	public function checkDefault($attribute, $params)
	{
		if (!empty($this->set_default) && !empty($this->id)) {
			$checkexist = TaxSlabs::model()->find(array('condition' => 'set_default = 1 AND id !=' . $this->id));
			if (!empty($checkexist)) {
				$this->addError('set_default', 'Already a default exist.');
			}
		}
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'tax_slab_value' => 'Tax Slab Value',
			'cgst' => 'Cgst',
			'sgst' => 'Sgst',
			'igst' => 'Igst',
			'status' => 'Status',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
			'updated_by' => 'Updated By',
			'updated_date' => 'Updated Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('tax_slab_value', $this->tax_slab_value);
		$criteria->compare('cgst', $this->cgst);
		$criteria->compare('sgst', $this->sgst);
		$criteria->compare('igst', $this->igst);
		$criteria->compare('status', $this->status);
		$criteria->compare('created_by', $this->created_by);
		$criteria->compare('created_date', $this->created_date, true);
		$criteria->compare('updated_by', $this->updated_by);
		$criteria->compare('updated_date', $this->updated_date, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TaxSlabs the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function getAllDatas()
	{
		$datas = TaxSlabs::model()->findAllByAttributes(array('status' => 1));
		return $datas;
	}
}
