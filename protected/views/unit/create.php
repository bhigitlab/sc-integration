<?php
/* @var $this CompanyExpenseTypeController */
/* @var $model CompanyExpenseType */

$this->breadcrumbs=array(
	'Company Expense Types'=>array('index'),
	'Create',
);
?>
<div class="modal-dialog modal-sm">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" onclick="javascript:window.location.reload()">&times;</button>
            <h4 class="modal-title">Add Daily Expense Type</h4>
        </div>
        <?php echo $this->renderPartial('_form', array('model' => $model)); ?>
    </div>

</div>
