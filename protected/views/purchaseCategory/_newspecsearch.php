<?php
/* @var $this ProjectsController */
/* @var $model Projects */
/* @var $form CActiveForm */
?>




<div class="page_filter clearfix">
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'action' => Yii::app()->createUrl($this->route),
        'method' => 'get',
    ));
    ?>
    <div class="filter_elem">
        <?php
        $spec_catId = Yii::app()->db->createCommand('SELECT DISTINCT  cat_id FROM `jp_specification`')->queryAll();
        $catIds = implode(',', array_column($spec_catId, 'cat_id'));
        echo CHtml::dropDownList('cat_id', 'cat_id', CHtml::listData(PurchaseCategory::model()->findAll(array(
                            'select' => array('category_name, category_name'),
                            'condition'=>'id IN ('.$catIds.')',
                            'order' => 'category_name',
                            'distinct' => true
                        )), 'category_name', 'category_name'), array('empty' => 'Category', 'class' => 'form-control select_js'));
        ?>				
    </div>
    <div class="filter_elem">
        <?php
        echo CHtml::dropDownList('brand_id', 'brand_id', CHtml::listData(Brand::model()->findAll(array(
                            'select' => array('brand_name, brand_name'),
                            'order' => 'brand_name',
                            'distinct' => true
                        )), 'brand_name', 'brand_name'), array('empty' => 'Brand', 'class' => 'form-control select_js', 'id' => 'spec_brand_id'));
        ?>				
    </div>
    <div class="filter_elem">
        <?php
        echo CHtml::dropDownList('specification', 'specification', CHtml::listData(Specification::model()->findAll(array(
                            'select' => array('specification, specification'),
                            'order' => 'specification',
                            'distinct' => true
                        )), 'specification', 'specification'), array('empty' => 'Specification', 'class' => 'form-control select_js', 'id' => 'spec_id'));
        ?>

    </div>
    <div class="filter_elem">
        <select name="PurchaseSpecification[specification_type]" id="Specification_type" class="Specification_type form-control select_js">
            <option value="By Quantity">By Quantity</option>
            <option value="By Length">By Length</option>
            <option value="By Width * Height">By Width * Height</option>
        </select>
    </div>
    <div class="filter_elem">
        <?php echo $form->textField($model, 'hsn_code', array('placeholder' => 'HSN Code', 'class' => 'form-control')); ?>
    </div>			
    <div class="filter_elem filter_btns">
        <input type="reset" id="clear" value="Clear"/>
    </div>
    <?php $this->endWidget(); ?>
</div>
