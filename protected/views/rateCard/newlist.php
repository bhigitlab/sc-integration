<?php
$this->breadcrumbs = array(
    'work Type',
);
?>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<div class="container" id="expensetype">
    <div class="loading-overlay">
        <span class="fa fa-spinner fa-3x fa-spin"></span>
    </div>
    <div class="clearfix">
        <div class="add-btn pull-right">
            
        </div>
        <h2>Work Types</h2>
    </div>
    <div id="updteratecard" style="display:none;"></div>

    <?php //$this->renderPartial('_newsearch', array('model' => $model)) 
    ?>
    <div class="row">
        <div class="col-md-12">
            <?php
            $this->widget('zii.widgets.CListView', array(
                'dataProvider' => $dataProvider,
                'itemView' => '_newview',
                'template' => '<div>{sorter}</div><div class=""><table cellpadding="10" id="worktypetab" class="table">{items}</table></div>',
            ));
            ?>
        </div>
        <!-- Add Expense type Popup -->
        <div id="addworkType" class="modal fade" role="dialog">
            <div class="modal-dialog modal-lg">

            </div>
        </div>


        <!-- Edit Expense type Popup -->

        <div class="modal fade edit" role="dialog">
            <div class="modal-dialog modal-lg">


            </div>
        </div>
        <?php
        Yii::app()->clientScript->registerScript('myjavascript', '
     $(document).ready(function(){
     
       $("#worktypetab").dataTable( {
            "scrollY": "300px",
            "scrollCollapse": true,
            "paging": false,
             "columnDefs"        : [       
                { 
                    "searchable"    : false, 
                    "targets"       : [0,2] 
                },
            ],
              
            
	} );
	
	
      
        
	});
        
    ');
        ?>

    

        <style>
            span.text_black {
                color: #333
            }

            .editable {
                float: right;
            }

            .page-body h3 {
                margin: 4px 0px;
                color: inherit;
                text-align: left;
            }

            .panel {
                border: 1px solid #ddd;
            }

            .panel-heading {
                background-color: #eee;
                height: 40px;
            }

            table.dataTable>thead>tr th:last-child {
                width: 40px;
                background-image: none;
            }

            .errorMessage1 {
                color: red;
            }
        </style>

    </div>
</div>