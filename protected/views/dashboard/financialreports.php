<div id="dashboard-content">
    <!-- <div class="dashboard-title-area">
        <h4 class="fw-bold margin-zero">Cash Balance</h4>
    </div> -->

    <div id="content-wrapper" class="p-relative responsive-content-base">
        <div>
            <div class="dashboard-buttons pull-left">
                <!-- Add buttons here if needed -->
            </div>
            <div class="dashboard-icon-buttons pull-right">
                <!-- Add icon buttons here if needed -->
            </div>
        </div>
        <!-- <div class="tab-content" style="width: 100%;"> -->

        <div id="financialReport">
            <?php echo $this->renderPartial('_financial_report', array(
                'model' => $model,
                'project_id' => $project_id,
                'financialdata1' => $financialdata1,
                'company_id' => $company_id,
                'buyer_module' => $buyer_module,
                'fixedprojects' => $fixedprojects,
                'completed' => $completed,
            )); ?>
        </div>

        <!-- </div> -->
    </div>
</div>