<?php
$user = Users::model()->findByPk(Yii::app()->user->id);
$arrVal = explode(',', $user->company_id);
$newQuery = "";
$newQuery1 = "";
foreach ($arrVal as $arr) {
  if ($newQuery)
    $newQuery .= ' OR';
  if ($newQuery1)
    $newQuery1 .= ' OR';
  $newQuery .= " FIND_IN_SET('" . $arr . "', id)";
  $newQuery1 .= " FIND_IN_SET('" . $arr . "', company_id)";
}
?>
<!--<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">-->

<script>
  $(function () {
    $("#Reconciliation_fromdate").datepicker({
      dateFormat: 'yy-mm-dd'
    });
    $("#Reconciliation_todate").datepicker({
      dateFormat: 'yy-mm-dd'
    });
    $("#Reconciliation_fromdate").change(function () {
      $("#Reconciliation_todate").datepicker('option', 'minDate', $(this).val());
    });
    $("#Reconciliation_todate").change(function () {
      $("#Reconciliation_fromdate").datepicker('option', 'maxDate', $(this).val());
    });
  });
</script>

<div class="page_filter clearfix custom-form-style">
  <div class="row">
    <?php $form = $this->beginWidget('CActiveForm', array(
      'action' => Yii::app()->createUrl($this->route),
      'method' => 'get',
    )); ?>
    <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-2">

      <label>From Date</label>
      <?php echo $form->textField($model, 'fromdate', array('placeholder' => 'Date From', 'class' => 'date form-control', 'autocomplete' => 'off', 'style' => 'display:inline-block;')); ?>

    </div>
    <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-2">

      <label>To Date</label>
      <?php echo $form->textField($model, 'todate', array('placeholder' => 'Date To', 'class' => 'date form-control', 'autocomplete' => 'off', 'style' => 'display:inline-block;')); ?>

    </div>
    <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-2">
      <label>Reconciliation Payment </label>
      <?php echo $form->textField($model, 'reconciliation_payment', array('class' => 'form-control', 'placeholder' => 'Payment Name', )); ?>
    </div>
    <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-2">
      <label>Reconciliation Bank </label>
      <?php echo $form->dropDownList($model, 'reconciliation_bank', CHtml::listData(Bank::model()->findAll(array('order' => 'bank_id ASC', 'condition' => '(' . $newQuery1 . ')')), 'bank_id', 'bank_name'), array('empty' => '-Choose a bank-', 'style' => 'padding: 2.5px 0px;', 'class' => 'form-control')); ?>
    </div>
    <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-2">
      <label>Company </label>
      <?php echo $form->dropDownList($model, 'company_id', CHtml::listData(Company::model()->findAll(array('order' => 'id DESC', 'condition' => '(' . $newQuery . ')')), 'id', 'name'), array('empty' => '-Choose a company-', 'style' => 'padding: 2.5px 0px;', 'class' => 'form-control')); ?>
    </div>
    <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-2">
      <label>Reconciliation Status</label>
      <?php echo $form->dropDownList($model, 'reconciliation_status', array('0' => 'Pending', '1' => 'completed'), array('empty' => '-Choose reconcilation status-', 'style' => 'padding: 2.5px 0px;', 'class' => 'form-control')); ?>
    </div>
    <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-2">
      <label>Reconciliation Cheque no</label>
      <?php echo $form->textField($model, 'reconciliation_chequeno', array('class' => 'form-control', 'placeholder' => 'Cheque Number', )); ?>
    </div>

    <div class="form-group col-xs-12 col-sm-3 text-right text-sm-left">
      <label>&nbsp;</label>
      <div>
        <?php echo CHtml::submitButton('Go', array('class' => 'btn btn-sm btn-primary')); ?>
        <?php echo CHtml::resetButton('Clear', array('class' => 'btn btn-sm btn-default', 'onclick' => 'javascript:location.href="' . $this->createUrl('reconciliation') . '"')); ?>
      </div>
    </div>
    <?php $this->endWidget(); ?>
  </div>
</div><!-- search-form -->