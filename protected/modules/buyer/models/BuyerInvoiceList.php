<?php

/**
 * This is the model class for table "{{buyer_invoice_list}}".
 *
 * The followings are the available columns in table '{{buyer_invoice_list}}':
 * @property integer $id
 * @property integer $buyer_inv_id
 * @property string $description
 * @property integer $transaction_type
 * @property double $amount
 * @property double $sgst_p
 * @property double $sgst_val
 * @property double $cgst_p
 * @property double $cgst_val
 * @property double $igst_p
 * @property double $igst_val
 * @property double $tax_total
 * @property double $subtotal
 * @property integer $created_by
 * @property string $created_date
 * @property integer $updated_by
 * @property string $updated_date
 *
 * The followings are the available model relations:
 * @property BuyerInvoice $buyerInv
 * @property Users $createdBy
 * @property Users $updatedBy
 */
class BuyerInvoiceList extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{buyer_invoice_list}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('buyer_inv_id, transaction_type, created_by, updated_by', 'numerical', 'integerOnly' => true),
			array('amount, sgst_p, sgst_val, cgst_p, cgst_val, igst_p, igst_val, tax_total, subtotal', 'numerical'),
			array('description, created_date, updated_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, buyer_inv_id, description, transaction_type, amount, sgst_p, sgst_val, cgst_p, cgst_val, igst_p, igst_val, tax_total, subtotal, created_by, created_date, updated_by, updated_date', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'buyerInv' => array(self::BELONGS_TO, 'BuyerInvoice', 'buyer_inv_id'),
			'createdBy' => array(self::BELONGS_TO, 'Users', 'created_by'),
			'updatedBy' => array(self::BELONGS_TO, 'Users', 'updated_by'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'buyer_inv_id' => 'Buyer Inv',
			'description' => 'Description',
			'transaction_type' => 'Transaction Type',
			'amount' => 'Amount',
			'sgst_p' => 'Sgst P',
			'sgst_val' => 'Sgst Val',
			'cgst_p' => 'Cgst P',
			'cgst_val' => 'Cgst Val',
			'igst_p' => 'Igst P',
			'igst_val' => 'Igst Val',
			'tax_total' => 'Tax Total',
			'subtotal' => 'Subtotal',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
			'updated_by' => 'Updated By',
			'updated_date' => 'Updated Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('buyer_inv_id', $this->buyer_inv_id);
		$criteria->compare('description', $this->description, true);
		$criteria->compare('transaction_type', $this->transaction_type);
		$criteria->compare('amount', $this->amount);
		$criteria->compare('sgst_p', $this->sgst_p);
		$criteria->compare('sgst_val', $this->sgst_val);
		$criteria->compare('cgst_p', $this->cgst_p);
		$criteria->compare('cgst_val', $this->cgst_val);
		$criteria->compare('igst_p', $this->igst_p);
		$criteria->compare('igst_val', $this->igst_val);
		$criteria->compare('tax_total', $this->tax_total);
		$criteria->compare('subtotal', $this->subtotal);
		$criteria->compare('created_by', $this->created_by);
		$criteria->compare('created_date', $this->created_date, true);
		$criteria->compare('updated_by', $this->updated_by);
		$criteria->compare('updated_date', $this->updated_date, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return BuyerInvoiceList the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}
}
