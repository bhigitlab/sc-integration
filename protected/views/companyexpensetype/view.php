<?php
/* @var $this CompanyexpensetypeController */
/* @var $model Companyexpensetype */

$this->breadcrumbs=array(
	'Companyexpensetypes'=>array('index'),
	$model->company_exp_id,
);

$this->menu=array(
	array('label'=>'List Companyexpensetype', 'url'=>array('index')),
	array('label'=>'Create Companyexpensetype', 'url'=>array('create')),
	array('label'=>'Update Companyexpensetype', 'url'=>array('update', 'id'=>$model->company_exp_id)),
	array('label'=>'Delete Companyexpensetype', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->company_exp_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Companyexpensetype', 'url'=>array('admin')),
);
?>

<h1>View Companyexpensetype #<?php echo $model->company_exp_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'company_exp_id',
		'expense_name',
	),
)); ?>
