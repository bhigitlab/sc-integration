<?php
/* @var $this ExpensesController */
/* @var $model Expenses */

$this->breadcrumbs=array(
	'Expenses'=>array('index'),
	$model->expense_id,
);

$this->menu=array(
	array('label'=>'List Expenses', 'url'=>array('index')),
	array('label'=>'Create Expenses', 'url'=>array('create')),
	array('label'=>'Update Expenses', 'url'=>array('update', 'id'=>$model->expense_id)),
	array('label'=>'Delete Expenses', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->expense_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Expenses', 'url'=>array('admin')),
);
?>

<h1>View Expenses #<?php echo $model->expense_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'expense_id',
		'expense_date',
		'project_id',
		'bill_id',
		'expensetype_id',
		'vendor_id',
		'expense_amount',
		'expense_sgstp',
		'expense_sgst',
		'expense_cgstp',
		'expense_cgst',
		'expense_totalamount',
		'expense_description',
		'receipt_type',
		'receipt',
		'purchase_type',
		'paidamount',
		'user_id',
		'created_by',
		'created_date',
		'updated_by',
		'updated_date',
	),
)); ?>
