<?php
/* @var $this ImageGalleryController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Image Galleries',
);

$this->menu=array(
	array('label'=>'Create ImageGallery', 'url'=>array('create')),
	array('label'=>'Manage ImageGallery', 'url'=>array('admin')),
);
?>

<h1>Image Galleries</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
