<div class="parent_row">
    <div class="row">
        <div class="col-md-1 logo_sec">
        <input type="checkbox" name="clearnotificationcheck[]" class="clearnotificationcheck clearcheck_sa" id="clearnotification<?php $index; ?>" data-type="scquotationstab" value="<?php echo $data['scquotation_id']; ?>" style="vertical-align: text-bottom;"/>
            <button class="btn_logo">SC</button>
        </div>
        <div class="col-md-9 content_sec">
            <div class="row  clearfix">
                <div class="col-md-6">
                    <div class="list-item">
                        Quotation by <b><?php echo isset($data->subcontractor->subcontractor_name)?$data->subcontractor->subcontractor_name:""; ?></b> for project <b><?php echo isset($data->projects->name)?$data->projects->name:""; ?></b>  Added by <b><?php echo $data->addedBy->first_name . " " . $data->addedBy->last_name; ?></b>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="list-item pull-right">
                        <label>Date:</label>
                        <span><?php echo date('d-m-Y', strtotime($data['scquotation_date'])); ?></span>

                    </div>
                </div>
                <div class="col-md-3">
                    <div class="list-item pull-right">
                        <label class="w-auto">Amount: </label>
                        <span><?php echo Yii::app()->controller->money_format_inr(Scquotation::model()->getTotalQuotationAmount($data->scquotation_id), 2); ?></span>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-2">
            <div class="list-item text-right act_btn">
                <a href="<?php echo Yii::app()->createUrl("subcontractor/addquotation", array('scquotation_id' => $data['scquotation_id'])) ?>" class="btn btn-xs btn_view btn_sec" target="_blank">View</a>
            </div>
        </div>
    </div>
</div>