<?php
/* @var $this WarehousedespatchController */
/* @var $data Warehousedespatch */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('warehousedespatch_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->warehousedespatch_id), array('view', 'id'=>$data->warehousedespatch_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('warehousedespatch_no')); ?>:</b>
	<?php echo CHtml::encode($data->warehousedespatch_no); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('warehousedespatch_date')); ?>:</b>
	<?php echo CHtml::encode($data->warehousedespatch_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('warehousedespatch_vendor')); ?>:</b>
	<?php echo CHtml::encode($data->warehousedespatch_vendor); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('warehousedespatch_warehouseid')); ?>:</b>
	<?php echo CHtml::encode($data->warehousedespatch_warehouseid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('warehousedespatch_clerk')); ?>:</b>
	<?php echo CHtml::encode($data->warehousedespatch_clerk); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('warehousedespatch_quantity')); ?>:</b>
	<?php echo CHtml::encode($data->warehousedespatch_quantity); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('warehousedespatch_through')); ?>:</b>
	<?php echo CHtml::encode($data->warehousedespatch_through); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('company_id')); ?>:</b>
	<?php echo CHtml::encode($data->company_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_by')); ?>:</b>
	<?php echo CHtml::encode($data->created_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_date')); ?>:</b>
	<?php echo CHtml::encode($data->created_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_by')); ?>:</b>
	<?php echo CHtml::encode($data->updated_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_date')); ?>:</b>
	<?php echo CHtml::encode($data->updated_date); ?>
	<br />

	*/ ?>

</div>