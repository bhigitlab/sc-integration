<?php

class GlobalusersController extends Controller
{
    
    public function accessRules() {
        /*  $this->access_rules = array(
             array('allow', // allow all users to perform 'index' and 'view' actions
                 'actions' => array('register', 'pwdrecovery', 'pwdreset', 'message_page', 'activation', 'googleauth'),
                 'users' => array('*'),
             ),
             array('allow', // allow authenticated user to perform 'create' and 'update' actions
                 'actions' => array('myprofile', 'changepass', 'passchange', 'editprofile','userlist'),
                 'users' => array('@'),
             ),
             array('allow', // allow admin user to perform 'admin' and 'delete' actions
                 'actions' => array('create', 'update','delete', 'index', 'view','deleteuser'),
                 'users' => array('@'),
                 'expression' => 'yii::app()->user->role<=2',
             ),
             array('allow', // allow super admin user to perform this shift user option
                 'actions' => array('usershift',),
                 'users' => array('@'),
                 'expression' => 'yii::app()->user->mainuser_role==1',
             ),            
             
             
         ); */
         
             $accessArr = array();
            $accessauthArr = array();
            $controller = Yii::app()->controller->id;
            if(isset(Yii::app()->session['menuall'])){
                if (array_key_exists($controller, Yii::app()->session['menuall'])) {
                    $accessArr = Yii::app()->session['menuall'][$controller];
                } 
            }
 
            if(isset(Yii::app()->session['menuauth'])){
                if (array_key_exists($controller, Yii::app()->session['menuauth'])) {
                    $accessauthArr = Yii::app()->session['menuauth'][$controller];
                }
            }
            $access_privlg = count($accessauthArr);
            
            return array(
                 array('allow', // allow all users to perform 'index' and 'view' actions
                     'actions' => $accessArr,
                     'users' => array('@'),
                     'expression' => "$access_privlg > 0",
 
                 ),
 
                 array('allow', // allow admin user to perform 'admin' and 'delete' actions
                     'actions' => $accessauthArr,
                     'users' => array('@'),
                     'expression' => "$access_privlg > 0",
 
                 ),
                 array('allow', // allow authenticated user to perform 'create' and 'update' actions
                 'actions' => array('myprofile', 'changepass', 'passchange', 'editprofile'),
                 'users' => array('@'),
                 ),
                  array('allow', // allow super admin user to perform this shift user option
                 'actions' => array('usershift'),
                 'users' => array('@'),
                 'expression' => 'yii::app()->user->mainuser_role==1',
                  ),   
                 array('deny', // deny all users
                     'users' => array('*'),
                 ),
 
             );
 
         //this will include in the register user. because captcha is not working if we use this.
         //if remove, other pages wont work.
         if (trim(strtolower(Yii::app()->controller->action->id) != 'captcha')) {
             // deny all users
             $this->access_rules[] = array('deny', 'users' => array('*'),);
         }
 
         return $this->access_rules;
     }

     public function actionIndex() {
        $model = new GlobalUsers('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Users']))
            $model->attributes = $_GET['Users'];

        $this->render('list', array(
            'model' => $model,
             'dataProvider' =>  $model->search(),
        ));
    }
}
