<?php

class BillsController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout = '//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		$controller = Yii::app()->controller->id;
		$hidden_actions = array('additionalbill1', 'deletebill2', 'additionalbill', 'deletebill1', 'deletebill', 'additionalitems', 'getadditionalitem', 'listadditionalitems', 'updatebillamount', 'autoReceiptBillWithoutPo', 'getUnitconversionFactor', 'adminlistexcel', 'Getduplicateentries', 'ageingreport', 'totalreport', 'AddDailyexpensebill', 'checkdailybillnumber', 'getItemsBasedonBill', 'loadbillModal', 'loadbillItemsModal','approveRateQuantity');
		$rules = AccessRulesHelper::getRules($controller, $hidden_actions);
		//Additional action rules
		return $rules;
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$model = $this->loadModel($id);
		$purchase_id = $model->purchase_id;
		$purchase_items = $this->setBillItems($id);
		if (isset($_GET['exportpdf'])) {
			$this->logo = $this->realpath_logo;
			$mPDF1 = Yii::app()->ePdf->mPDF();
			$mPDF1 = Yii::app()->ePdf->mPDF('', 'A4');
			$sql = 'SELECT template_name '
				. 'FROM jp_quotation_template '
				. 'WHERE status="1"';
			$selectedtemplate = Yii::app()->db->createCommand($sql)->queryScalar();

			$template = $this->getTemplate($selectedtemplate);
			$mPDF1->SetHTMLHeader($template['header']);
			$mPDF1->SetHTMLFooter($template['footer']);
			$mPDF1->AddPage('', '', '', '', '', 0, 0, 40, 30, 10, 0);
			$mPDF1->WriteHTML($this->renderPartial('viewbills', array('newmodel' => $purchase_items, 'model' => $model), true));
			$mPDF1->Output('Bills.pdf', 'D');
		} else {
			$this->render('viewbills', array(
				'model' => $model,
				'newmodel' => $purchase_items,
			));
		}
	}

	public function setBillItems($id)
	{
		$items = Billitem::model()->findAll(array("condition" => "bill_id = " . $id . ' AND (purchaseitem_id IS NOT NULL AND purchaseitem_id != 0)', 'group' => 'purchaseitem_id'));
		$data = array();
		$item = Billitem::model()->findAll(array("condition" => "bill_id = " . $id));
		//echo "<pre>";print_r($item);exit;
		foreach ($items as $item) {

			if (!empty($item['category_id']) && $item['category_id'] != 0) {
				$category_model = Specification::model()->findByPk($item['category_id']);
				$purchase_item_model = PurchaseItems::model()->findByPk($item['purchaseitem_id']);
				$purchase_rate = isset($purchase_item_model) ? $purchase_item_model['rate'] : '';
				$purchase_qty = isset($purchase_item_model) ? $purchase_item_model['quantity'] : '';
				if ($category_model['brand_id'] != NULL) {
					$brand_details = Brand::model()->findByPk($category_model['brand_id']);
					$brand = $brand_details['brand_name'];
				} else {
					$brand = '';
				}
				$tot_taxamount = $item['billitem_cgst'] + $item['billitem_igst'] + $item['billitem_sgst'];
				$parent_category = PurchaseCategory::model()->findByPk($category_model['cat_id']);
				$specification = $brand . ' - ' . $category_model['specification'];
				$dat = array('specification' => $specification, 'quantity' => $item['billitem_quantity'], 'unit' => $item['billitem_unit'], 'hsn_code' => $item['billitem_hsn_code'], 'rate' => $item['billitem_rate'], 'amount' => $item['billitem_amount'], 'tax_slab' => $item['billitem_taxslab'], 'sgstp' => $item['billitem_sgstpercent'], 'sgst' => $item['billitem_sgst'], 'cgstp' => $item['billitem_cgstpercent'], 'cgst' => $item['billitem_cgst'], 'igstp' => $item['billitem_igstpercent'], 'igst' => $item['billitem_igst'], 'discp' => $item['billitem_discountpercent'], 'disc' => $item['billitem_discountamount'], 'tot_tax' => $tot_taxamount, 'billitem_id' => $item['billitem_id'], 'taxpercent' => $item['billitem_taxpercent'], 'purchase_rate' => $purchase_rate, 'purchase_qty' => $purchase_qty, 'rate_approve' => $item['rate_approve'], 'approve_status' => $item['approve_status']);
				if (!empty($category_model['cat_id'])) {
					if (!array_key_exists($category_model['cat_id'], $data)) {
						$data[$category_model['cat_id']]['category_title'] = $parent_category['category_name'];
						$data[$category_model['cat_id']][] = $dat;
					} else {
						array_push($data[$category_model['cat_id']], $dat);
					}
				} else {
					if (!array_key_exists($category_model['id'], $data)) {
						$data[$category_model['id']]['category_title'] = $category_model['category_name'];
						$data[$category_model['id']][] = $dat;
					} else {
						array_push($data[$category_model['id']], $dat);
					}
				}
			}
		}
		$items_add = Billitem::model()->findAll(array("condition" => "bill_id = " . $id . ' AND (purchaseitem_id IS  NULL OR purchaseitem_id = 0)'));
		$data_add = array();
		foreach ($items_add as $item) {

			if (!empty($item['category_id']) && $item['category_id'] != 0) {
				$category_model = Specification::model()->findByPk($item['category_id']);
				if ($category_model['brand_id'] != NULL) {
					$brand_details = Brand::model()->findByPk($category_model['brand_id']);
					$brand = $brand_details['brand_name'];
				} else {
					$brand = '';
				}
				$tot_taxamount = $item['billitem_cgst'] + $item['billitem_igst'] + $item['billitem_sgst'];
				$parent_category = PurchaseCategory::model()->findByPk($category_model['cat_id']);
				$specification = $brand . ' - ' . $category_model['specification'];
				$dat = array('specification' => $specification, 'quantity' => $item['billitem_quantity'], 'unit' => $item['billitem_unit'], 'hsn_code' => $item['billitem_hsn_code'], 'rate' => $item['billitem_rate'], 'amount' => $item['billitem_amount'], 'tax_slab' => $item['billitem_taxslab'], 'sgstp' => $item['billitem_sgstpercent'], 'sgst' => $item['billitem_sgst'], 'cgstp' => $item['billitem_cgstpercent'], 'cgst' => $item['billitem_cgst'], 'igstp' => $item['billitem_igstpercent'], 'igst' => $item['billitem_igst'], 'discp' => $item['billitem_discountpercent'], 'disc' => $item['billitem_discountamount'], 'tot_tax' => $tot_taxamount, 'billitem_id' => $item['billitem_id'], 'taxpercent' => $item['billitem_taxpercent']);
				if (!empty($category_model['cat_id'])) {
					if (!array_key_exists($category_model['cat_id'], $data_add)) {
						$data_add[$category_model['cat_id']]['category_title'] = $parent_category['category_name'];
						$data_add[$category_model['cat_id']][] = $dat;
					} else {
						array_push($data_add[$category_model['cat_id']], $dat);
					}
				} else {
					if (!array_key_exists($category_model['id'], $data_add)) {
						$data_add[$category_model['id']]['category_title'] = $category_model['category_name'];
						$data_add[$category_model['id']][] = $dat;
					} else {
						array_push($data_add[$category_model['id']], $dat);
					}
				}
			}
		}
		$items_others = Billitem::model()->findAll(array(
			"condition" => "bill_id = " . $id . ' AND (purchaseitem_id IS NULL OR purchaseitem_id = 0)'
		));
		$data_add = array();

		foreach ($items_others as $item) {
			$category_model = Specification::model()->findByPk($item['category_id']);
			$brand = ($category_model && $category_model['brand_id'] != NULL)
				? Brand::model()->findByPk($category_model['brand_id'])['brand_name']
				: '';

			$tot_taxamount = $item['billitem_cgst'] + $item['billitem_igst'] + $item['billitem_sgst'];
			$specification = $brand . ' - ' . ($category_model ? $category_model['specification'] : 'Others');
			$dat = array(
				'specification' => $specification,
				'quantity' => $item['billitem_quantity'],
				'unit' => $item['billitem_unit'],
				'hsn_code' => $item['billitem_hsn_code'],
				'rate' => $item['billitem_rate'],
				'amount' => $item['billitem_amount'],
				'tax_slab' => $item['billitem_taxslab'],
				'sgstp' => $item['billitem_sgstpercent'],
				'sgst' => $item['billitem_sgst'],
				'cgstp' => $item['billitem_cgstpercent'],
				'cgst' => $item['billitem_cgst'],
				'igstp' => $item['billitem_igstpercent'],
				'igst' => $item['billitem_igst'],
				'discp' => $item['billitem_discountpercent'],
				'disc' => $item['billitem_discountamount'],
				'tot_tax' => $tot_taxamount,
				'billitem_id' => $item['billitem_id'],
				'taxpercent' => $item['billitem_taxpercent']
			);

			if ($category_model && !empty($category_model['cat_id'])) {
				if (!array_key_exists($category_model['cat_id'], $data_add)) {
					$data_add[$category_model['cat_id']]['category_title'] = PurchaseCategory::model()->findByPk($category_model['cat_id'])['category_name'];
					$data_add[$category_model['cat_id']][] = $dat;
				} else {
					array_push($data_add[$category_model['cat_id']], $dat);
				}
			} else {
				// Add to "Others" category
				if (!array_key_exists('others', $data_add)) {
					$data_add['others']['category_title'] = 'Others';
					$data_add['others'][] = $dat;
				} else {
					array_push($data_add['others'], $dat);
				}
			}
		}
		//echo "<pre>";print_r($data_add);exit;
		$result['items'] = $data;
		$result['add_items'] = $data_add;
		return $result;
		// echo "<pre>";print_r($result);exit;

	}

	public function actionautoReceiptBillWithoutPo()
	{
		$id = $_POST["bill_id"];
		$tblpx = Yii::app()->db->tablePrefix;
		$general_settings_for_auto_receipt = Controller::generalSettingsForAutoReceiptToProjectWarehouse($settings = 1);
		if ($general_settings_for_auto_receipt == 1) {
			$main_msg_status = 'success';
			$msg = "Billed details added successfully! ";
			$auto_receipt_bill_details = Bills::model()->findByPk($id);
			$receipt_default_date = $auto_receipt_bill_details->bill_date;
			$receipt_vendor = 'System generated Receipt Note';
			$receipt_warehouse_to = $auto_receipt_bill_details->warehouse_id;
			$receipt_warehouse_from = NULL;
			$receipt_clerk = yii::app()->user->id;
			$receipt_no = 'PR-' . $auto_receipt_bill_details->bill_number;
			$receipt_transfer_type = '1';
			$bill_or_despatch_id = $id;
			$bill_item_approoved_status_arr = array();
			$bill_item_approoved_status_details = Billitem::model()->findAll(
				array(
					'select' => array('*'),
					'condition' => 'bill_id ="' . $bill_or_despatch_id . '"',
					'order' => 'billitem_id DESC'
				)
			);
			if (!empty($bill_item_approoved_status_details)) {
				foreach ($bill_item_approoved_status_details as $value) {
					if ($value['approve_status'] == '' || $value['approve_status'] == 1) {
						$approve_status = 1;
						array_push($bill_item_approoved_status_arr, $approve_status);
					} else {
						$approve_status = 0;
						array_push($bill_item_approoved_status_arr, $approve_status);
					}
				}
			}
			if (!empty($bill_item_approoved_status_arr) && !in_array(0, $bill_item_approoved_status_arr)) {
				$auto_receipt_warehouse_check = Controller::autoReceiptWarehouseCheck($receipt_warehouse_to, $receipt_warehouse_from, $receipt_clerk);
				if ($auto_receipt_warehouse_check == "true") {
					$auto_receipt_to_warehouse = Controller::autoReceiptToWarehouse($receipt_warehouse_to, $receipt_warehouse_from, $receipt_default_date, $receipt_vendor, $receipt_clerk, $receipt_no, $receipt_transfer_type, $bill_or_despatch_id);
					if ($auto_receipt_to_warehouse == "true") {
						$msg_status = 'success';
						$msg = "Billed details added successfully! ";
					} else {
						$msg_status = 'error';
						$msg .= "<br>Some problem occured in Auto receipt!";
					}
				} else {
					$msg_status = 'error';
					$msg .= "<br><span class='errorContent'>Auto receipt is not possible.</span>";
				}

			} else {
				$msg_status = 'error';
				$msg .= "<br>Bill require approval for auto receipt.";
			}
		} else {
			$main_msg_status = 'success';
			$msg = "Billed details added successfully! ";
		}
		echo json_encode(array('main_msg_status' => $main_msg_status, 'msg' => $msg));
	}


	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{

		$tblpx = Yii::app()->db->tablePrefix;
		$model = new Bills;
		$model2 = new Warehousestock;
		$client = '';
		$warehouse_billed_stock = '1';
		$msg = '';
		if (isset($_POST['Bills'])) {
			$id = $_POST["billid"];
			$newmodel = $this->loadModel($id);
			$newmodel->bill_status = 2;
			//$newmodel->updated_date = date("Y-m-d");
			$newmodel->round_off = isset($_POST['Bills']["round_off"]) ? $_POST['Bills']["round_off"] : '';
			$billNumber = $_POST['Bills']['bill_number'];
			$model->bill_date = date('Y-m-d', strtotime($_POST['Bills']['bill_date']));
			$newmodel->warehouse_id = $_POST['Warehousestock']['warehousestock_warehouseid'];
			$billamount = $newmodel->bill_totalamount;
			if ($newmodel->round_off) {
				$billamount = $newmodel->bill_totalamount + $newmodel->round_off;
			}
			$newmodel->bill_totalamount = $billamount;
			$billed_items = Billitem::model()->findAll(array('condition' => 'bill_id = ' . $id));
			//echo "<pre>";print_r($_POST);exit;
			$billItems_id = '';
			if (!empty($billed_items)) {
				$billItems_id = array();
				$billItems_id = array_map(function ($item) {
					return $item->purchaseitem_id;
				}, $billed_items);
			}
			$purchaseItems = PurchaseItems::model()->findAll(array('condition' => 'purchase_id = ' . $_POST['Bills']['purchase_id']));
			// print_r($purchaseItems);
			// die();
			if (!empty($purchaseItems)) {
				$purchase_items_id = array();
				$purchase_items_id = array_map(function ($item) {
					return $item->item_id;
				}, $purchaseItems);
			}
			$item_id_diff = array_diff($purchase_items_id, $billItems_id);

			if (!empty($item_id_diff)) {
				foreach ($item_id_diff as $items) {
					$item_details = PurchaseItems::model()->findByPk($items);
					//echo "<pre>";print_r($item_details);exit;
					if (!empty($item_details)) {
						$bill_item_model = new Billitem();
						$bill_item_model->bill_id = $_POST["billid"];
						$bill_item_model->purchaseitem_id = $item_details->item_id;
						$bill_item_model->billitem_quantity = 0;
						$bill_item_model->billitem_unit = $item_details->unit;
						$bill_item_model->billitem_rate = $item_details->rate;
						;
						$bill_item_model->billitem_taxslab = $item_details->tax_slab;
						$bill_item_model->billitem_amount = 0;
						$bill_item_model->billitem_discountamount = $item_details->discount_amount;
						$bill_item_model->billitem_discountpercent = $item_details->discount_percentage;
						$bill_item_model->billitem_cgst = $item_details->cgst_amount;
						$bill_item_model->billitem_cgstpercent = $item_details->cgst_percentage;
						$bill_item_model->billitem_sgst = $item_details->sgst_amount;
						$bill_item_model->billitem_sgstpercent = $item_details->sgst_percentage;
						$bill_item_model->billitem_igst = $item_details->igst_amount;
						$bill_item_model->billitem_igstpercent = $item_details->igst_percentage;
						$bill_item_model->billitem_taxamount = $item_details->tax_amount;
						$bill_item_model->billitem_taxpercent = $item_details->tax_perc;
						$bill_item_model->category_id = $item_details->category_id;
						$bill_item_model->created_date = date('Y-m-d');
						$bill_item_model->billitem_hsn_code = $item_details->hsn_code;
						if ($bill_item_model->save())
							$billItemId = $bill_item_model->billitem_id;




					}

				}
			}
			if ($newmodel->save()) {
				$deleteBills = Yii::app()->db->createCommand("DELETE FROM {$tblpx}bills WHERE bill_number = '" . $billNumber . "' and bill_status = 1");
				$deleteBills->execute();
				$billed_items = Billitem::model()->findAll(array('condition' => 'bill_id = ' . $id));
				$bill_det = '';
				if (!empty($billed_items)) {
					$sql = '';
					$sql = "SELECT SUM(billitem_amount) as billitem_amount , SUM(billitem_taxamount) as billitem_taxamount,SUM(billitem_discountamount) as billitem_discountamount FROM `jp_billitem` WHERE billitem_quantity !=0 AND `bill_id` =" . $id;
					$bill_det = Yii::app()->db->createCommand($sql)->queryRow();

				}
				if (!empty($bill_det)) {
					$bills = Bills::Model()->findByPk($id);
					if (!empty($bills)) {
						$bill_amount = isset($bill_det["billitem_amount"]) ? round($bill_det["billitem_amount"], 2) : 0;
						$bill_taxamount = isset($bill_det["billitem_taxamount"]) ? round($bill_det["billitem_taxamount"], 2) : 0;
						$bill_discountamount = isset($bill_det["billitem_discountamount"]) ? round($bill_det["billitem_discountamount"], 2) : 0;

						$bills->bill_amount = $bill_amount;
						$bills->bill_taxamount = $bill_taxamount;
						$bills->bill_discountamount = $bill_discountamount;
						$bills->bill_totalamount =
							($bill_amount +
								$bill_taxamount) -
							$bill_discountamount;
						$bills->updated_date = DATE('Y-m-d');
						$bills->save(false);
					}
				}

				//Auto receipt section

				$general_settings_for_auto_receipt = Controller::generalSettingsForAutoReceiptToProjectWarehouse($settings = 1);


				if ($general_settings_for_auto_receipt == 1) {
					if ($warehouse_billed_stock == '1') {
						$main_msg_status = 'success';
						$msg = "Billed details added successfully! ";
					} else {
						$main_msg_status = 'error';
						$msg = "Some problem occured for Details added in warehouse Stock.";
					}
					$auto_receipt_bill_details = Bills::model()->findByPk($id);
					$receipt_default_date = $auto_receipt_bill_details->bill_date;
					$receipt_vendor = 'System generated Receipt Note';
					$receipt_warehouse_to = $auto_receipt_bill_details->warehouse_id;
					$receipt_warehouse_from = NULL;
					$receipt_clerk = yii::app()->user->id;
					$receipt_no = 'PR-' . $auto_receipt_bill_details->bill_number;

					/*rev*/
					$tblpx = Yii::app()->db->tablePrefix;
					$bills = Bills::model()->findByPk($id);
					$company_id = $bills->company_id;
					$company = Company::model()->findByPk($company_id);
					$wr_no = "";
					if (!empty($company->warehouse_receipt_format)) {
						$warehouse_receipt_format = trim($company["warehouse_receipt_format"]);
						$general_wr_format_arr = explode('/', $warehouse_receipt_format);
						$status = 1;
						$org_prefix = $general_wr_format_arr[0];
						$org_prefix_slash=$general_wr_format_arr[0].'/';
						$prefix_key = 0;
						$wr_key = '';
						$current_key = '';
						foreach ($general_wr_format_arr as $key => $value) {

							if ($value == '{grn_sequence}') {
								$wr_key = $key;
							} else if ($value == '{year}') {
								$current_key = $key;
							}
						}

						$current_year = date('Y');
						$previous_year = date('Y') - 1;
						$next_year = date('Y') + 1;

						$where = '';
						if ($current_key == 2) {
							$where .= " AND `warehousereceipt_no` LIKE '%$current_year'";
						} else if ($current_key == 1) {
							$where .= " AND `warehousereceipt_no` LIKE '%$current_year%'";
						}
						$sql = "SELECT * from " . $tblpx . "warehousereceipt  WHERE `warehousereceipt_no` LIKE '$org_prefix_slash%' $where ORDER BY `warehousereceipt_id` DESC LIMIT 1;";
						//echo "<pre>";print_r($sql);exit;
						$previous_wr_no = '';
						$wr_seq_no = 1;
						$wr_seq_prefix = 'GRN-';
						$previous_data = Yii::app()->db->createCommand($sql)->queryRow();
						if (!empty($previous_data)) {
							$previous_wr_no = $previous_data['warehousereceipt_no'];
							$prefix_arr = explode('/', $previous_wr_no);
							$prefix = $prefix_arr[0];
							$previous_prefix_seq_no = $prefix_arr[$wr_key];
							$wr_split = explode('-', $previous_prefix_seq_no);
							$previous_prefix_seq_no_val = $wr_split[1];
							$wr_seq_no = $previous_prefix_seq_no_val + 1;

						}
						$digit = strlen((string) $wr_seq_no);

						if ($digit == 1) {
							$wr_no = $wr_seq_prefix . '00' . $wr_seq_no;
						} else if ($digit == 2) {
							$wr_no = $wr_seq_prefix . '0' . $wr_seq_no;
						} else {
							$wr_no = $wr_seq_prefix . $wr_seq_no;
						}
						$status = 1;


						$new_invoice = str_replace('{year}', $current_year, $warehouse_receipt_format);
						$new_wr_no = str_replace('{grn_sequence}', $wr_no, $new_invoice);
						//die($new_wr_no);
						$wrno = $new_wr_no;
						if (!empty($wrno)) {
							$receipt_no = $new_wr_no;
						}
					}
					/*rev*/
					$receipt_transfer_type = '1';
					$bill_or_despatch_id = $id;
					$bill_item_approoved_status_arr = array();
					$bill_item_approoved_status_details = Billitem::model()->findAll(
						array(
							'select' => array('*'),
							'condition' => 'bill_id ="' . $bill_or_despatch_id . '"',
							'order' => 'billitem_id DESC'
						)
					);
					if (!empty($bill_item_approoved_status_details)) {
						foreach ($bill_item_approoved_status_details as $value) {
							if ($value['approve_status'] == '' || $value['approve_status'] == 1) {
								$approve_status = 1;
								array_push($bill_item_approoved_status_arr, $approve_status);
							} else {
								$approve_status = 0;
								array_push($bill_item_approoved_status_arr, $approve_status);
							}
						}
					}
					if (!empty($bill_item_approoved_status_arr) && !in_array(0, $bill_item_approoved_status_arr)) {
						if (Yii::app()->user->role == 1) {
							$auto_receipt_to_warehouse = Controller::autoReceiptToWarehouse($receipt_warehouse_to, $receipt_warehouse_from, $receipt_default_date, $receipt_vendor, $receipt_clerk, $receipt_no, $receipt_transfer_type, $bill_or_despatch_id);
							if ($auto_receipt_to_warehouse == "true") {
								$msg_status = 'success';
								$msg = "Billed details added successfully! ";
							} else {
								$msg_status = 'error';
								$msg .= "<br>Some problem occured in Auto receipt!";
							}
						} else {
							if (Yii::app()->user->role == 2 || Yii::app()->user->role == 5 || Yii::app()->user->role == 3) {
								$auto_receipt_warehouse_check = Controller::autoReceiptWarehouseCheck($receipt_warehouse_to, $receipt_warehouse_from, $receipt_clerk);
								if ($auto_receipt_warehouse_check == "true") {
									$auto_receipt_to_warehouse = Controller::autoReceiptToWarehouse($receipt_warehouse_to, $receipt_warehouse_from, $receipt_default_date, $receipt_vendor, $receipt_clerk, $receipt_no, $receipt_transfer_type, $bill_or_despatch_id);
									if ($auto_receipt_to_warehouse == "true") {
										$msg_status = 'success';
										$msg = "Billed details added successfully! ";
									} else {
										$msg_status = 'error';
										$msg .= "<br>Some problem occured in Auto receipt!";
									}
								} else {
									$msg_status = 'error';
									$msg .= "<br><span class='errorContent'>Auto receipt is not possible.</span>";
								}
							}
						}
					} else {
						$msg_status = 'error';
						$msg .= "<br>Bill require approval for auto receipt.";
					}
				} else {
					if ($warehouse_billed_stock == '1') {
						$main_msg_status = 'success';
						$msg = "Billed details added successfully! ";
					} else {
						$main_msg_status = 'error';
						$msg = "Some problem occured for Details added in warehouse Stock.";
					}
				}
				Yii::app()->user->setFlash($main_msg_status, $msg);
				$this->redirect(array('admin'));
			}
		}

		$this->render('create', array(
			'model' => $model,
			'model2' => $model2,
			'client' => $client
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$tblpx = Yii::app()->db->tablePrefix;
		$model = $this->loadModel($id);
		$daybook_entry_count = Bills::model()->DaybookStatus($id);
		if ($daybook_entry_count > 0) {
			Yii::app()->user->setFlash('error', "Daybook entry added for the bill.Cant edit..");
			$this->redirect(array('bills/admin'));
		}
		$model2 = new Warehousestock;
		$client = '';
		$bill_id = $id;
		$new_billitem = [];
		$bllitemno = '';
		$tblpx = Yii::app()->db->tablePrefix;
		$purchase = Yii::app()->db->createCommand("SELECT * FROM " . $tblpx . "bills WHERE bill_id = " . $id)->queryRow();
		$pId = $purchase["purchase_id"];
		$client = $this->GetItemsByBillId($id, $pId);
		$warehouse_id = '';

		if (isset($_POST['Bills'])) {
			$warehouse_id = $model->warehouse_id;
			$model->attributes = $_POST['Bills'];
			$bill_amount = isset($_POST['Bills']['bill_amount']) ? $_POST['Bills']['bill_amount'] : $model->bill_amount;
			$bill_taxamount = isset($_POST['Bills']['bill_taxamount']) ? $_POST['Bills']['bill_taxamount'] : $model->bill_taxamount;
			$roundoff = isset($_POST['Bills']['round_off']) ? $_POST['Bills']['round_off'] : $model->round_off;
			$bill_discountamount = isset($_POST['Bills']['bill_discountamount']) ? $_POST['Bills']['bill_discountamount'] : $model->bill_discountamount;
			$total = $bill_amount + $bill_taxamount + $roundoff - $bill_discountamount;
			$model->bill_totalamount = $total;
			$model->bill_date = date("Y-m-d", strtotime($_POST['Bills']['bill_date']));
			$bill_stockdate = date("Y-m-d", strtotime($_POST['Bills']['bill_date']));
			$bill_warehouse_id = isset($_POST['Warehousestock']['warehousestock_warehouseid']) ? $_POST['Warehousestock']['warehousestock_warehouseid'] : null;
			if (!empty($bill_warehouse_id)) {
				$model->warehouse_id = $bill_warehouse_id;
			}

			$model->bill_status = 2;

			if ($model->save()) {
				$bill_warehouse_id = ($bill_warehouse_id != '') ? $bill_warehouse_id : null;
				$updateBillsitemArray = array(
					'warehouse_id' => $bill_warehouse_id
				);
				$updateBillsitem = Yii::app()->db->createCommand()
					->update(
						$tblpx . 'billitem',
						$updateBillsitemArray,
						'bill_id = :bill_id',
						array(':bill_id' => $bill_id)
					);


				//Auto receipt section

				$general_settings_for_auto_receipt = Controller::generalSettingsForAutoReceiptToProjectWarehouse($settings = 1);
				if ($general_settings_for_auto_receipt == 1) {
					$main_msg_status = 'success';
					$msg = "Billed details added successfully! ";
					$auto_receipt_bill_details = Bills::model()->findByPk($id);
					$receipt_default_date = $auto_receipt_bill_details->bill_date;
					$receipt_vendor = 'System generated Receipt Note';
					$receipt_warehouse_to = $auto_receipt_bill_details->warehouse_id;
					$receipt_warehouse_from = NULL;
					$receipt_clerk = yii::app()->user->id;
					$receipt_no = 'PR-' . $auto_receipt_bill_details->bill_number;
					$receipt_transfer_type = '1';
					$bill_or_despatch_id = $id;
					$bill_item_approoved_status_arr = array();
					$bill_item_approoved_status_details = Billitem::model()->findAll(
						array(
							'select' => array('*'),
							'condition' => 'bill_id ="' . $bill_or_despatch_id . '"',
							'order' => 'billitem_id DESC'
						)
					);
					if (!empty($bill_item_approoved_status_details)) {
						foreach ($bill_item_approoved_status_details as $value) {
							if ($value['approve_status'] == '' || $value['approve_status'] == 1) {
								$approve_status = 1;
								array_push($bill_item_approoved_status_arr, $approve_status);
							} else {
								$approve_status = 0;
								array_push($bill_item_approoved_status_arr, $approve_status);
							}
						}
					}
					if (!empty($bill_item_approoved_status_arr) && !in_array(0, $bill_item_approoved_status_arr)) {
						if (Yii::app()->user->role == 1) {
							$auto_receipt_to_warehouse = Controller::autoReceiptToWarehouse($receipt_warehouse_to, $receipt_warehouse_from, $receipt_default_date, $receipt_vendor, $receipt_clerk, $receipt_no, $receipt_transfer_type, $bill_or_despatch_id);
							if ($auto_receipt_to_warehouse == "true") {
								$msg_status = 'success';
								$msg = "Warehouse receipt added successfully! ";
							} else {
								$msg_status = 'error';
								$msg .= "<br>Some problem occured in Auto receipt!";
							}
						} else {
							if (Yii::app()->user->role == 2 || Yii::app()->user->role == 5 || Yii::app()->user->role == 3) {
								$auto_receipt_warehouse_check = Controller::autoReceiptWarehouseCheck($receipt_warehouse_to, $receipt_warehouse_from, $receipt_clerk);
								if ($auto_receipt_warehouse_check == "true") {
									$auto_receipt_to_warehouse = Controller::autoReceiptToWarehouse($receipt_warehouse_to, $receipt_warehouse_from, $receipt_default_date, $receipt_vendor, $receipt_clerk, $receipt_no, $receipt_transfer_type, $bill_or_despatch_id);
									if ($auto_receipt_to_warehouse == "true") {
										$msg_status = 'success';
										$msg = "Warehouse receipt added successfully! ";
									} else {
										$msg_status = 'error';
										$msg .= "<br>Some problem occured in Auto receipt!";
									}
								} else {
									$msg_status = 'error';
									$msg .= "<br><span class='errorContent'>Auto receipt is not possible.</span>";
								}
							}
						}
					} else {
						$msg_status = 'error';
						$msg .= "<br>Bill require approval for auto receipt.";
					}
				} else {
					$main_msg_status = 'success';
					$msg = "Billed details added successfully! ";
				}

				Yii::app()->user->setFlash($main_msg_status, $msg);
				$this->redirect(array('admin'));
				$this->redirect(array('admin', 'id' => $model->bill_id));
			} else {
				$main_msg_status = 'error';
				$msg = "Some problem occured.. ";
				Yii::app()->user->setFlash($main_msg_status, $msg);
				$this->redirect(array('admin'));
				// $this->redirect(array('admin', 'id' => $model->bill_id));
			}
		}
		$additional_items = Billitem::model()->findAll(array('condition' => 'bill_id = ' . $id . ' AND (purchaseitem_id IS NULL OR purchaseitem_id = 0)'));

		$this->render('update', array(
			'model' => $model,
			'client' => $client,
			'additional_items' => $additional_items,
			'model2' => $model2
		));
	}

	public function saveBillItem($post_data, $bill_id)
	{

		$saved_status_datas = $post_data['save_status'];
		foreach ($saved_status_datas as $key => $status) {

			if ($post_data['biquantity'][$key] > 0) {
				$amount = $post_data['biquantity'][$key] * $post_data['birate'][$key];
				if (!empty($post_data['damount'][$key])) {
					$amount_after_dis = $amount - $post_data['damount'][$key];
					$newDp = (($post_data['damount'][$key] / $amount) * 100);
				} else {
					$amount_after_dis = $amount;
					$newDp = 0;
				}
				$amount = is_numeric($amount) ? $amount : 0;
				$amount_after_dis = is_numeric($amount_after_dis) ? $amount_after_dis : 0;
				$cgst_amount = $amount_after_dis * ($post_data['cgstpercent'][$key] / 100);
				$cgst_amount = is_numeric($cgst_amount) ? $cgst_amount : 0;
				$sgst_amount = $amount_after_dis * ($post_data['sgstpercent'][$key] / 100);
				$sgst_amount = is_numeric($sgst_amount) ? $sgst_amount : 0;
				$igst_amount = $amount_after_dis * ($post_data['igstpercent'][$key] / 100);
				$igst_amount = is_numeric($igst_amount) ? $igst_amount : 0;
				$total_tax = $cgst_amount + $sgst_amount + $igst_amount;
				$total_tax_perc = $post_data['cgstpercent'][$key] + $post_data['sgstpercent'][$key] + $post_data['igstpercent'][$key];
				if (empty($post_data['billitem'][$key])) {
					$bill_item_model = new Billitem();
					$billItemId = Yii::app()->db->getLastInsertID();
				} else {
					$bill_item_model = Billitem::model()->findByPk($post_data['billitem'][$key]);
				}
				$bill_item_model->bill_id = $bill_id;
				$bill_item_model->purchaseitem_id = $post_data['ids'][$key];
				$bill_item_model->billitem_quantity = $post_data['biquantity'][$key];
				$bill_item_model->billitem_unit = $post_data['purchase_unitvendor_'][$key];
				$bill_item_model->billitem_rate = $post_data['birate'][$key];
				$bill_item_model->billitem_taxslab = $post_data['tax_slab'][$key];
				$bill_item_model->billitem_amount = $amount;
				$bill_item_model->billitem_discountamount = $post_data['damount'][$key];
				$bill_item_model->billitem_discountpercent = $newDp;
				$bill_item_model->billitem_cgst = $cgst_amount;
				$bill_item_model->billitem_cgstpercent = $post_data['cgstpercent'][$key];
				$bill_item_model->billitem_sgst = $sgst_amount;
				$bill_item_model->billitem_sgstpercent = $post_data['sgstpercent'][$key];
				$bill_item_model->billitem_igst = $igst_amount;
				$bill_item_model->billitem_igstpercent = $post_data['igstpercent'][$key];
				$bill_item_model->billitem_taxamount = $total_tax;
				$bill_item_model->billitem_taxpercent = $total_tax_perc;
				$bill_item_model->category_id = $post_data['category'][$key];
				$bill_item_model->created_date = date('Y-m-d');
				$bill_item_model->billitem_hsn_code = $post_data['hsn_code'][$key];
				if ($bill_item_model->save())
					$billItemId = $bill_item_model->billitem_id;


				$quantityRem = $post_data['biremquantity'][$key] - $post_data['biquantity'][$key];
				if ($quantityRem > 0) {
					$item_status = 90;
				} else {
					$item_status = 91;
				}
				$purchase_items_model = PurchaseItems::model()->findByPk($post_data['ids'][$key]);
				$purchase_items_model->item_status = $item_status;
				$purchase_items_model->save();
				$saved_qty = $post_data['savedquantity'][$key];
				$saved_qty = is_numeric($saved_qty) ? $saved_qty : 0;
				$newTotalQty = ($post_data['btqty'][0] + $post_data['biquantity'][$key]) - $saved_qty;
				if ($post_data['tqty'][0] > $newTotalQty)
					$purchaseStatus = 94;
				else
					$purchaseStatus = 93;
				$purchase_model = Purchase::model()->findByPk($post_data['Bills']['purchase_id']);
				$purchase_model->purchase_billing_status = $purchaseStatus;
				$purchase_model->save(false);

				$item_bills = Billitem::model()->findAll(array(
					'condition' => 'purchaseitem_id= ' . $post_data['ids'][$key]
				));
				$item_bill_qty = 0;
				foreach ($item_bills as $item_bill) {
					$bill_model_check = Bills::model()->findByPk($item_bill['bill_id']);
					if (!empty($bill_model_check)) {
						$item_bill_qty += $item_bill['billitem_quantity'];
					}
				}
				$item_quantity = $item_bill_qty;
				$purchase_item_model = PurchaseItems::model()->findByPk($post_data['ids'][$key]);
				$bill_item_model = Billitem::model()->findByPk($billItemId);
				if ($purchase_items_model['quantity'] < $item_quantity) {
					$bill_item_model->approve_status = 1;
				} else {
					$bill_item_model->approve_status = NULL;
				}
				if ($purchase_item_model['rate'] < $post_data['birate'][$key]) {
					$bill_item_model->rate_approve = 1;
				}

				$bill_item_model->save();

				$unbilled_amount = $this->getUnbilledAmount($post_data['Bills']['purchase_id']);
				$newpurchase = Purchase::model()->findByPk($post_data['Bills']['purchase_id']);
				$newpurchase->unbilled_amount = round($unbilled_amount, 2);
				$newpurchase->save(false);
			}
			// }
		}
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if (!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider = new CActiveDataProvider('Bills');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model = new Bills('search');
		$model->unsetAttributes();  // clear any default values
		if (isset($_GET['Bills']) || isset($_GET['purchase_no']) || isset($_GET['project_id']) || isset($_GET['vendor_id']) || isset($_GET['date_from']) || isset($_GET['date_to']) || isset($_GET['Bills']['company_id']) || isset($_GET['expensehead_id'])) {
			$model->attributes = $_GET['Bills'];
			$model->date_from = isset($_GET['date_from']) ? $_GET['date_from'] : "";
			$model->date_to = isset($_GET['date_to']) ? $_GET['date_to'] : "";
			$model->purchase_no = isset($_GET['purchase_no']) ? $_GET['purchase_no'] : "";
			$model->project_id = isset($_GET['project_id']) ? $_GET['project_id'] : "";
			$model->expensehead_id = isset($_GET['expensehead_id']) ? $_GET['expensehead_id'] : "";
			$model->vendor_id = isset($_GET['vendor_id']) ? $_GET['vendor_id'] : "";
			$model->company_id = isset($_GET['Bills']['company_id']) ? $_GET['Bills']['company_id'] : "";
			$model->created_date_from = isset($_GET['created_date_from']) ? $_GET['created_date_from'] : "";

			$model->created_date_to = isset($_GET['created_date_to']) ? $_GET['created_date_to'] : "";
		}
		$userId = Yii::app()->user->id;
		$companyList = Yii::app()->user->company_ids;
		$companyListArr = explode(',', Yii::app()->user->company_ids);
		$tblpx = Yii::app()->db->tablePrefix;
		$user = Users::model()->findByPk(Yii::app()->user->id);
		$arrVal = explode(',', $user->company_id);
		$newQuery = "";
		foreach ($arrVal as $arr) {
			if ($newQuery)
				$newQuery .= ' OR';
			$newQuery .= " FIND_IN_SET('" . $arr . "', company_id)";
		}
		//  $expensehead = Yii::app()->db->createCommand("SELECT DISTINCT type_id,type_name FROM {$tblpx}expense_type WHERE (" . $newQuery . ") ORDER BY type_name")->queryAll();
		$expensehead = ExpenseType::model()->findAll();
		$compCondition = "";
		if (count($companyListArr) > 0) {
			$compCondition .= " AND (";
			foreach ($companyListArr as $company) {
				$compCondition .= " bl.company_id = " . $company . " OR";
			}
		}
		$compCondition = substr($compCondition, 0, -3);
		$compCondition .= ")";

		$sql = "SELECT bl.* FROM {$tblpx}bills bl
                LEFT JOIN {$tblpx}purchase pu ON bl.purchase_id = pu.p_id
                LEFT JOIN {$tblpx}projects pr ON pr.pid = pu.project_id
                LEFT JOIN {$tblpx}project_assign pa ON pr.pid = pa.projectid
                WHERE 1 = 1{$compCondition} AND pa.userid = {$userId}";

		$count = Yii::app()->db->createCommand($sql)->queryScalar();
		$dataProvider = new CSqlDataProvider($sql, array(
			'totalItemCount' => $count,
			'keyField' => 'bill_id',
			'id' => 'bill_id',
			'sort' => array(
				'attributes' => array(
					'bill_id',
					'purchase_id',
					'bill_number',
					'bill_date',
					'bill_totalamount',
				),
			),
			'pagination' => false,
		));
		$render_datas = array(
			'model' => $model,
			'dataProvider' => $model->search(),
			'expensehead' => $expensehead
		);
		if (isset($_GET['export'])) {
			$pdfdata = $this->renderPartial('newlist', $render_datas, true);
			$filename = 'Invoice.pdf';
			$mPDF1 = Yii::app()->ePdf->mPDF();
			$mPDF1 = Yii::app()->ePdf->mPDF('', 'A4');
			$mPDF1->WriteHTML($pdfdata);
			$mPDF1->Output($filename, 'D');
		} else {
			$this->render('newlist', $render_datas);
		}

	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Bills the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model = Bills::model()->findByPk($id);
		if ($model === null)
			throw new CHttpException(404, 'The requested page does not exist.');
		return $model;
	}

	public function loadModelBillItem($id)
	{
		$model = Billitem::model()->findByPk($id);
		if ($model === null)
			throw new CHttpException(404, 'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Bills $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'bills-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	public function actionSaveBills()
	{

		$details = "";
		$vendor = $_POST['vendor'];
		$date = date('Y-m-d', strtotime($_POST['date']));
		$billno = $_POST['billno'];
		$purchaseno = $_POST['purchaseno'];
		$project = $_POST['project'];
		$company = $_POST['company'];
		$company_id = $_POST['company_id'];
		if (isset($_POST['slNo'])) {
			$sl_No = $_POST['slNo'];
		} else {
			$sl_No = '';
		}
		if (isset($_POST['category'])) {
			$category = $_POST['category'];
		} else {
			$category = '';
		}
		if (isset($_POST['quantity'])) {
			$quantity = $_POST['quantity'];
		} else {
			$quantity = '';
		}
		if (isset($_POST['unit'])) {
			$unit = $_POST['unit'];
		} else {
			$unit = '';
		}
		if (isset($_POST['rate'])) {
			$rate = $_POST['rate'];
		} else {
			$rate = '';
		}
		if (isset($_POST['amount'])) {
			$amount = $_POST['amount'];
		} else {
			$amount = '';
		}
		if (isset($_POST['discount'])) {
			$discount = $_POST['discount'];
		} else {
			$discount = '';
		}
		if (isset($_POST['hsn_code'])) {
			$hsn_code = $_POST['hsn_code'];
		} else {
			$hsn_code = '';
		}
		if (isset($_POST['discountp'])) {
			$discountp = $_POST['discountp'];
		} else {
			$discountp = '';
		}
		if (isset($_POST['cgst'])) {
			$cgst = $_POST['cgst'];
		} else {
			$cgst = '';
		}
		if (isset($_POST['cgstp'])) {
			$cgstp = $_POST['cgstp'];
		} else {
			$cgstp = '';
		}
		if (isset($_POST['sgst'])) {
			$sgst = $_POST['sgst'];
		} else {
			$sgst = '';
		}
		if (isset($_POST['sgstp'])) {
			$sgstp = $_POST['sgstp'];
		} else {
			$sgstp = '';
		}
		if (isset($_POST['igst'])) {
			$igst = $_POST['igst'];
		} else {
			$igst = '';
		}
		if (isset($_POST['igstp'])) {
			$igstp = $_POST['igstp'];
		} else {
			$igstp = '';
		}
		if (isset($_POST['ttax'])) {
			$ttax = $_POST['ttax'];
		} else {
			$ttax = '';
		}
		if (isset($_POST['ttaxp'])) {
			$ttaxp = $_POST['ttaxp'];
		} else {
			$ttaxp = '';
		}
		if (isset($_POST['totalamount'])) {
			$totalamount = $_POST['totalamount'];
		} else {
			$totalamount = '';
		}
		if (isset($_POST['tax_slab'])) {
			$tax_slab = $_POST['tax_slab'];
		} else {
			$tax_slab = '';
		}


		if (isset($_POST['purchase_id'])) {
			$purchase_id = $_POST['purchase_id'];
		} else {
			$purchase_id = '';
		}

		if (isset($_POST['bill_id'])) {
			$bill_id = $_POST['bill_id'];
		} else {
			$bill_id = '';
		}

		$billamount = $_POST['billamount'];
		$billdiscount = $_POST['billdiscount'];
		$billtax = $_POST['billtaxamount'];
		$billgrandtotal = $_POST['billtotal'];

		$mPDF1 = Yii::app()->ePdf->mPDF();

		$mPDF1 = Yii::app()->ePdf->mPDF('', 'A4');

		$mPDF1->WriteHTML($stylesheet, 1);

		$mPDF1->WriteHTML($this->renderPartial('billsview', array(
			'details' => $details,
			'vendor' => $vendor,
			'date' => $date,
			'company_id' => $company_id,
			'billno' => $billno,
			'purchaseno' => $purchaseno,
			'project' => $project,
			'billamount' => $billamount,
			'billdiscount' => $billdiscount,
			'billtax' => $billtax,
			'billgrandtotal' => $billgrandtotal,
			'sl_No' => $sl_No,
			'category' => $category,
			'quantity' => $quantity,
			'unit' => $unit,
			'rate' => $rate,
			'amount' => $amount,
			'discount' => $discount,
			'discountp' => $discountp,
			'cgst' => $cgst,
			'cgstp' => $cgstp,
			'igst' => $igst,
			'igstp' => $igstp,
			'sgst' => $sgst,
			'sgstp' => $sgstp,
			'ttax' => $ttax,
			'ttaxp' => $ttaxp,
			'totalamount' => $totalamount,
			'company' => $company,
			'tax_slab' => $tax_slab,
			'bill_id' => $bill_id
		), true));

		$mPDF1->Output('Bills.pdf', 'D');
	}
	public function actionExportBills()
	{

		$details = "";
		$vendor = $_POST['vendor'];
		$date = date('d-m-Y', strtotime($_POST['date']));
		$billno = $_POST['billno'];
		$purchaseno = $_POST['purchaseno'];
		$project = $_POST['project'];
		$company = $_POST['company'];
		if (isset($_POST['slNo'])) {
			$sl_No = $_POST['slNo'];
		} else {
			$sl_No = '';
		}
		if (isset($_POST['category'])) {
			$category = $_POST['category'];
		} else {
			$category = '';
		}
		if (isset($_POST['quantity'])) {
			$quantity = $_POST['quantity'];
		} else {
			$quantity = '';
		}
		if (isset($_POST['unit'])) {
			$unit = $_POST['unit'];
		} else {
			$unit = '';
		}
		if (isset($_POST['rate'])) {
			$rate = $_POST['rate'];
		} else {
			$rate = '';
		}
		if (isset($_POST['amount'])) {
			$amount = $_POST['amount'];
		} else {
			$amount = '';
		}
		if (isset($_POST['discount'])) {
			$discount = $_POST['discount'];
		} else {
			$discount = '';
		}
		if (isset($_POST['hsn_code'])) {
			$hsn_code = $_POST['hsn_code'];
		} else {
			$hsn_code = '';
		}
		if (isset($_POST['discountp'])) {
			$discountp = $_POST['discountp'];
		} else {
			$discountp = '';
		}
		if (isset($_POST['cgst'])) {
			$cgst = $_POST['cgst'];
		} else {
			$cgst = '';
		}
		if (isset($_POST['cgstp'])) {
			$cgstp = $_POST['cgstp'];
		} else {
			$cgstp = '';
		}
		if (isset($_POST['sgst'])) {
			$sgst = $_POST['sgst'];
		} else {
			$sgst = '';
		}
		if (isset($_POST['sgstp'])) {
			$sgstp = $_POST['sgstp'];
		} else {
			$sgstp = '';
		}
		if (isset($_POST['igst'])) {
			$igst = $_POST['igst'];
		} else {
			$igst = '';
		}
		if (isset($_POST['igstp'])) {
			$igstp = $_POST['igstp'];
		} else {
			$igstp = '';
		}
		if (isset($_POST['ttax'])) {
			$ttax = $_POST['ttax'];
		} else {
			$ttax = '';
		}
		if (isset($_POST['ttaxp'])) {
			$ttaxp = $_POST['ttaxp'];
		} else {
			$ttaxp = '';
		}
		if (isset($_POST['totalamount'])) {
			$totalamount = $_POST['totalamount'];
		} else {
			$totalamount = '';
		}
		if (isset($_POST['tax_slab'])) {
			$tax_slab = $_POST['tax_slab'];
		} else {
			$tax_slab = '';
		}
		if (isset($_POST['purchase_id'])) {
			$purchase_id = $_POST['purchase_id'];
		} else {
			$purchase_id = '';
		}
		if (isset($_POST['bill_id'])) {
			$bill_id = $_POST['bill_id'];
		} else {
			$bill_id = '';
		}
		$additional_amount = 0;
		$tblpx = Yii::app()->db->tablePrefix;
		$purchase = Yii::app()->db->createCommand("SELECT  purchase_no FROM `{$tblpx}purchase` 
			where p_id =" . $purchase_id)->queryRow();
		if (!empty($bill_id)) {
			$item_model = PurchaseItems::model()->findAll(array("condition" => "purchase_id = '$purchase_id'"));
			$bill_items = $this->setBillItems($bill_id);
		}
		$billamount = $_POST['billamount'];
		$billdiscount = $_POST['billdiscount'];
		$billtax = $_POST['billtaxamount'];
		$billgrandtotal = $_POST['billtotal'];
		if ($bill_id != '') {
			$addcharges = Yii::app()->db->createCommand("SELECT ROUND(SUM(amount), 2) as amount FROM " . $tblpx . "additional_bill WHERE `bill_id`=" . $bill_id)->queryRow();

			$additional_amount = $addcharges['amount'];
			if (!empty($additional_amount)) {
				$billamount += $additional_amount;
			}
		}
		$arraylabel = array('Company', 'Project Name', 'Vendor', 'Date', 'Bill Number', 'Purchase Number', '');
		$finaldata = array();
		$finaldata[0][] = $company;
		$finaldata[0][] = $project;
		$finaldata[0][] = $vendor;
		$finaldata[0][] = $date;
		$finaldata[0][] = $billno;
		$finaldata[0][] = $purchaseno;

		$finaldata[1][] = '';
		$finaldata[1][] = '';
		$finaldata[1][] = '';
		$finaldata[1][] = '';
		$finaldata[1][] = '';
		$finaldata[1][] = '';


		$finaldata[2][] = 'Sl.No';
		$finaldata[2][] = 'Specification/Remark';
		$finaldata[2][] = 'HSN Code';
		$finaldata[2][] = 'Quantity';
		$finaldata[2][] = 'Unit';
		$finaldata[2][] = 'Rate';
		$finaldata[2][] = 'Amount';
		$finaldata[2][] = 'Tax Slab (%)';
		$finaldata[2][] = 'Discount';
		$finaldata[2][] = 'Discount (%)';
		$finaldata[2][] = 'CGST';
		$finaldata[2][] = 'CGST (%)';
		$finaldata[2][] = 'SGST';
		$finaldata[2][] = 'SGST (%)';
		$finaldata[2][] = 'IGST';
		$finaldata[2][] = 'IGST (%)';
		$finaldata[2][] = 'Total Tax';
		$finaldata[2][] = 'Total Tax (%)';
		$finaldata[2][] = 'Total Amount';
		if (!empty($bill_items)) {

			$items = $bill_items['items'];
			$additonal_items = $bill_items['add_items'];
			$k = 1;

			foreach ($items as $purchase_item) {
				if (isset($purchase_item['category_title'])) {
					$finaldata[$k + 2][] = $purchase_item['category_title'];
					$finaldata[$k + 2][] = '';
					$finaldata[$k + 2][] = '';
					$finaldata[$k + 2][] = '';
					$finaldata[$k + 2][] = '';
					$finaldata[$k + 2][] = '';
					$finaldata[$k + 2][] = '';
					$finaldata[$k + 2][] = '';
					$finaldata[$k + 2][] = '';
					$finaldata[$k + 2][] = '';
					$finaldata[$k + 2][] = '';
					$finaldata[$k + 2][] = '';
					$finaldata[$k + 2][] = '';
					$finaldata[$k + 2][] = '';
					$finaldata[$k + 2][] = '';
					$finaldata[$k + 2][] = '';
					$finaldata[$k + 2][] = '';
					$finaldata[$k + 2][] = '';
					$finaldata[$k + 2][] = '';
				}

				$i = $k;
				foreach ($purchase_item as $data) {

					if (isset($data['specification'])) {
						$finaldata[$i + 3][] = '1';
						$finaldata[$i + 3][] = $data['specification'];
						$finaldata[$i + 3][] = $data['hsn_code'];
						$finaldata[$i + 3][] = $data['quantity'];
						$finaldata[$i + 3][] = $data['unit'];
						$finaldata[$i + 3][] = Yii::app()->Controller->money_format_inr($data['rate'], 2, 1);
						$finaldata[$i + 3][] = Yii::app()->Controller->money_format_inr($data['amount'], 2, 1);
						$finaldata[$i + 3][] = Yii::app()->Controller->money_format_inr($data['tax_slab'], 2, 1);
						$finaldata[$i + 3][] = Yii::app()->Controller->money_format_inr($data['disc'], 2, 1);
						$finaldata[$i + 3][] = $data['discp'];
						$finaldata[$i + 3][] = Yii::app()->Controller->money_format_inr($data['cgst'], 2, 1);
						$finaldata[$i + 3][] = $data['cgstp'];
						$finaldata[$i + 3][] = Yii::app()->Controller->money_format_inr($data['sgst'], 2, 1);
						$finaldata[$i + 3][] = $data['sgstp'];
						$finaldata[$i + 3][] = Yii::app()->Controller->money_format_inr($data['igst'], 2, 1);
						$finaldata[$i + 3][] = $data['igstp'];
						$finaldata[$i + 3][] = Yii::app()->Controller->money_format_inr($data['tot_tax'], 2, 1);
						$finaldata[$i + 3][] = Yii::app()->Controller->money_format_inr($data['taxpercent'], 2, 1);
						$amount_tot = $data['tot_tax'] + $data['amount'];
						$finaldata[$i + 3][] = Yii::app()->Controller->money_format_inr($amount_tot, 2, 1);
						$i++;
						$x = $i + 3;
					}
				}

				$k += count($purchase_item);
			}

			foreach ($additonal_items as $purchase_item) {
				if (isset($purchase_item['category_title'])) {
					$finaldata[$k + 2][] = $purchase_item['category_title'];
					$finaldata[$k + 2][] = '';
					$finaldata[$k + 2][] = '';
					$finaldata[$k + 2][] = '';
					$finaldata[$k + 2][] = '';
					$finaldata[$k + 2][] = '';
					$finaldata[$k + 2][] = '';
					$finaldata[$k + 2][] = '';
					$finaldata[$k + 2][] = '';
					$finaldata[$k + 2][] = '';
					$finaldata[$k + 2][] = '';
					$finaldata[$k + 2][] = '';
					$finaldata[$k + 2][] = '';
					$finaldata[$k + 2][] = '';
					$finaldata[$k + 2][] = '';
					$finaldata[$k + 2][] = '';
					$finaldata[$k + 2][] = '';
					$finaldata[$k + 2][] = '';
					$finaldata[$k + 2][] = '';
				}

				$i = $k;
				foreach ($purchase_item as $data) {

					if (isset($data['specification'])) {
						$finaldata[$i + 3][] = '1';
						$finaldata[$i + 3][] = $data['specification'];
						$finaldata[$i + 3][] = $data['hsn_code'];
						$finaldata[$i + 3][] = $data['quantity'];
						$finaldata[$i + 3][] = $data['unit'];
						$finaldata[$i + 3][] = Yii::app()->Controller->money_format_inr($data['rate'], 2, 1);
						$finaldata[$i + 3][] = Yii::app()->Controller->money_format_inr($data['amount'], 2, 1);
						$finaldata[$i + 3][] = Yii::app()->Controller->money_format_inr($data['tax_slab'], 2, 1);
						$finaldata[$i + 3][] = Yii::app()->Controller->money_format_inr($data['disc'], 2, 1);
						$finaldata[$i + 3][] = $data['discp'];
						$finaldata[$i + 3][] = Yii::app()->Controller->money_format_inr($data['cgst'], 2, 1);
						$finaldata[$i + 3][] = $data['cgstp'];
						$finaldata[$i + 3][] = Yii::app()->Controller->money_format_inr($data['sgst'], 2, 1);
						$finaldata[$i + 3][] = $data['sgstp'];
						$finaldata[$i + 3][] = Yii::app()->Controller->money_format_inr($data['igst'], 2, 1);
						$finaldata[$i + 3][] = $data['igstp'];
						$finaldata[$i + 3][] = Yii::app()->Controller->money_format_inr($data['tot_tax'], 2, 1);
						$finaldata[$i + 3][] = Yii::app()->Controller->money_format_inr($data['taxpercent'], 2, 1);
						$amount_tot = $data['tot_tax'] + $data['amount'];
						$finaldata[$i + 3][] = Yii::app()->Controller->money_format_inr($amount_tot, 2, 1);
						$i++;
						$x = $i + 3;
					}
				}

				$k += count($purchase_item);
			}

			// $x = $i + 4;
			$finaldata[$x][] = '';
			$finaldata[$x][] = '';
			$finaldata[$x][] = '';
			$finaldata[$x + 1][] = 'Grand Total:';
			$finaldata[$x + 1][] = Controller::money_format_inr($billamount, 2, 1);
			$finaldata[$x + 1][] = '*Exclusive of GST';
		} else {
			$finaldata[3][] = '';
			$finaldata[3][] = '';
			$finaldata[3][] = '';
			$finaldata[3][] = '';
			$finaldata[3][] = '';
			$finaldata[3][] = '';


			$addchargesall = Yii::app()->db->createCommand("SELECT * FROM " . $tblpx . "additional_bill WHERE `bill_id`=" . $bill_id)->queryAll();
			if (!empty($addchargesall)) {
				$p = 0;
				$finaldata[$x + 4][] = 'Additional Charges';
				foreach ($addchargesall as $data) {
					$finaldata[$x + 5 + $p][] = $data['category'] . ' - ' . $data['amount'];
					$p++;
					$k = $p;
				}
			} else {
				$k = $x + 3;
			}




			$finaldata[$k + 1][] = '';
			$finaldata[$k + 1][] = '';
			$finaldata[$k + 1][] = '';
			$finaldata[$k + 1][] = '';

			$finaldata[$k + 2][] = 'Amount';
			$finaldata[$k + 2][] = $billamount;

			$finaldata[$k + 3][] = 'Discount';
			$finaldata[$k + 3][] = $billdiscount;

			$finaldata[$k + 4][] = 'Tax';
			$finaldata[$k + 4][] = $billtax;

			$finaldata[$k + 5][] = 'Additional Change';
			$finaldata[$k + 5][] = ($additional_amount != '') ? $additional_amount : 0;

			$finaldata[$k + 6][] = 'Grand Total';
			$finaldata[$k + 6][] = Controller::money_format_inr($billamount + $additional_amount, 2, 1);
		}
		Yii::import('ext.ECSVExport');

		$csv = new ECSVExport($finaldata);
		$csv->setHeaders($arraylabel);
		$csv->setToAppend();
		$contentdata = $csv->toCSV();
		$csvfilename = 'Bills' . date('d_M_Y') . '.csv';
		Yii::app()->getRequest()->sendFile($csvfilename, $contentdata, "text/csv", false);
		exit();
	}
	public function actionGetItemsByPurchase()
	{
		$id = $_GET['id'];
		$client = "";
		$pData = "";
		$pitems = "";
		$purchase = Purchase::model()->findByPk($id);
		$pData = Yii::app()->db->createCommand("SELECT * FROM " . $this->tableNameAcc('purchase_items', 0) . " WHERE purchase_id = " . $id . " ORDER BY item_id")->queryAll();

		$bill = Yii::app()->db->createCommand("SELECT  sum(billitem_quantity) as quantity ,purchaseitem_id FROM " . $this->tableNameAcc('billitem', 0) . " GROUP BY purchaseitem_id")->queryAll();

		$billdetails = CHtml::listData($bill, 'purchaseitem_id', 'qunatity');

		$bill_data = Yii::app()->db->createCommand("SELECT  sum(purchaseitem_quantity) as quantity,purchaseitem_id  FROM " . $this->tableNameAcc('billitem', 0) . " GROUP BY purchaseitem_id")->queryAll();

		$bill_datadetails = CHtml::listData($bill_data, 'purchaseitem_id', 'quantity');

		$pitems = $this->renderPartial('purchaseitems', array(
			'purchase' => $pData,
			'purchase_type' => $purchase->purchase_type,
			'bills' => $billdetails,
			'bill_datas' => $bill_datadetails
		));
		echo $pitems;
	}
	public function GetItemsByBillId($billId, $pId)
	{
		$client = "";
		//$tblpx = Yii::app()->db->tablePrefix;
		$pdatasql = "SELECT a.*, a.category_id as categoryid, b.*,
		a.category_id as pcategory,b.category_id as bcategory,
		a.remark as premark,b.remark as bremark FROM " . $this->tableNameAcc('purchase_items', 0) . " a LEFT JOIN " . $this->tableNameAcc('billitem', 0) . " b ON a.item_id = b.purchaseitem_id and b.bill_id = " . $billId . " WHERE a.purchase_id = " . $pId . " GROUP by a.item_id";
		$pData = Yii::app()->db->createCommand($pdatasql)->queryAll();
		$purchase = Purchase::model()->findByPk($pId);

		$bill = Yii::app()->db->createCommand("SELECT  sum(billitem_quantity) as quantity ,purchaseitem_id FROM " . $this->tableNameAcc('billitem', 0) . " GROUP BY purchaseitem_id")->queryAll();

		$billdetails = CHtml::listData($bill, 'purchaseitem_id', 'qunatity');

		$bill_data = Yii::app()->db->createCommand("SELECT  sum(purchaseitem_quantity) as quantity,purchaseitem_id  FROM " . $this->tableNameAcc('billitem', 0) . " GROUP BY purchaseitem_id")->queryAll();

		$bill_datadetails = CHtml::listData($bill_data, 'purchaseitem_id', 'quantity');

		$client = $this->renderPartial('updatepurchaseitems', array(
			'purchase' => $pData,
			'billId' => $billId,
			'purchase_type' => $purchase->purchase_type,
			'bills' => $billdetails,
			'bill_datas' => $bill_datadetails
		), true);
		return $client;
	}
	public function actionUpdateItemsToList()
	{

		$tblpx = Yii::app()->db->tablePrefix;

		$itemId = $_REQUEST["itemid"];
		$billId = $_REQUEST["billid"];
		$desc = $_REQUEST["desc"];
		$quantity = $_REQUEST["quantity"];
		$unit = $_REQUEST["unit"];
		$rate = $_REQUEST["rate"];
		$amount = $_REQUEST["amount"];
		$billAmt = $_REQUEST["billamount"];
		$billTAmt = $_REQUEST["billtotal"];
		$ustatus = $_REQUEST["ustatus"];
		$updateBills = Yii::app()->db->createCommand("update {$tblpx}bills SET bill_amount = " . $billAmt . ", bill_totalamount = " . $billTAmt . " where bill_id = " . $billId);
		$updateBills->execute();
		if ($ustatus == 1) {
			$updateItems = Yii::app()->db->createCommand("update {$tblpx}purchase_items SET bill_id = " . $billId . ", or_description = '" . $desc . "', or_quantity = " . $quantity . ", or_unit = '" . $unit . "', or_rate = " . $rate . ", or_amount = " . $amount . " where item_id = " . $itemId);
			$updateItems->execute();
		} else {
			$updateItems = Yii::app()->db->createCommand("update {$tblpx}purchase_items SET bill_id = NULL, or_description = NULL, or_quantity = NULL, or_unit = NULL, or_rate = NULL, or_amount = NULL where item_id = " . $itemId);
			$updateItems->execute();
		}
		//echo 1;
	}
	public function actionValidateBillNumber()
	{
		$billNo = $_GET["billno"];
		$tblpx = Yii::app()->db->tablePrefix;
		$billData = Yii::app()->db->createCommand("SELECT * FROM " . $tblpx . "bills WHERE bill_number = '" . $billNo . "' and bill_status = 2 and company_id=" . Yii::app()->user->company_id . "")->queryAll();
		if ($billData) {
			echo 1;
		} else {
			echo 2;
		}
	}



	public function actionAddItemsForBills()
	{

		$billdata = $_POST;
		$billId = $billdata["billid"];
		$billItemId = $_POST["billitem"];
		$billNTotal = $billdata['billamount'];
		$billNTax = $billdata["billtax"];
		$billNDiscount = $billdata["billdiscount"];
		$billNGTotal = $billdata["billtotal"];
		$billitembatch = isset($_POST["billitembatch"]) ? $_POST["billitembatch"] : "";
		$Warehouse_id = isset($_POST["Warehouse_id"]) ? $_POST["Warehouse_id"] : 'NULL';
		$Warehouse_id = ($Warehouse_id != '') ? $Warehouse_id : 'NULL';
		$length = ($_POST["length"] != '') ? $_POST["length"] : NULL;
		$width = ($_POST["width"] != '') ? $_POST["width"] : NULL;
		$height = ($_POST["height"] != '') ? $_POST["height"] : NULL;
		$item_details = PurchaseItems::model()->findByPk($billdata['itemid']);
		$remark = ($item_details->remark != '') ? $item_details->remark : 'NULL';
		$billNDate = date('Y-m-d', strtotime($billdata['billdate']));
		$createdBy = Yii::app()->user->id;
		$createdDate = date('Y-m-d H:i:s');


		$billattribute_data = array(
			'purchase_id' => $billdata['pid'],
			'bill_number' => $billdata["billnumber"],
			'bill_date' => $billNDate,
			'bill_amount' => $billNTotal,
			'bill_taxamount' => $billNTax,
			'bill_discountamount' => $billNDiscount,
			'bill_totalamount' => $billNGTotal,
			'bill_status' => 1,
			'company_id' => $billdata['company_id'],
			'warehouse_id' => $Warehouse_id
		);

		if ($billId == "") {
			$billattribute_data['created_by'] = $createdBy;
			$billattribute_data['created_date'] = $createdDate;
			$addBills = Yii::app()->db->createCommand()->insert($this->tableNameAcc('bills', 0), $billattribute_data);
			$billId = Yii::app()->db->getLastInsertID();
		} else {
			$additional_item_data = Billitem::model()->findAll(array('condition' => 'bill_id = ' . $billId . ' AND (purchaseitem_id IS NULL OR purchaseitem_id = 0)'));

			$add_amount = $add_tax_amount = $add_disc_amount = $add_total_amount = 0;
			foreach ($additional_item_data as $addition_item) {
				$add_amount += $addition_item['billitem_amount'];
				$add_tax_amount += $addition_item['billitem_taxamount'];
				$add_disc_amount += $addition_item['billitem_discountamount'];
			}
			$add_total_amount = (($add_amount + $add_tax_amount) - $add_disc_amount);
			$billNTotal += $add_amount;
			$billNTax += $add_tax_amount;
			$billNDiscount += $add_disc_amount;
			$billNGTotal += $add_total_amount;
			$billattribute_data['updated_date'] = date('Y-m-d H:i:s');
			$updateBills = Yii::app()->db->createCommand()->update($this->tableNameAcc('bills', 0), $billattribute_data, 'bill_id = "' . $billId . '"');
		}


		$attribute_data = array(
			'bill_id' => $billId,
			'purchaseitem_id' => $billdata["itemid"],
			'batch' => $billitembatch,
			'billitem_quantity' => $billdata["quantity"],
			'billitem_unit' => $billdata["unit"],
			'billitem_rate' => $billdata["rate"],
			'purchaseitem_quantity' => $billdata["purchasequantity"],
			'purchaseitem_unit' => $billdata["purchaseunit"],
			'purchaseitem_rate' => $billdata["purchaserate"],
			'billitem_taxslab' => $billdata["tax_slab"],
			'billitem_amount' => $billdata["amount"],
			'billitem_discountamount' => $billdata["damount"],
			'billitem_discountpercent' => $billdata["dpercent"],
			'billitem_cgst' => $billdata["cgst"],
			'billitem_cgstpercent' => $billdata["cgstpercent"],
			'billitem_sgst' => $billdata["sgst"],
			'billitem_sgstpercent' => $billdata["sgstpercent"],
			'billitem_taxamount' => $billdata["totaltax"],
			'billitem_taxpercent' => $billdata["totaltaxp"],
			'billitem_igst' => $billdata["igst"],
			'billitem_igstpercent' => $billdata["igstpercent"],
			'billitem_hsn_code' => $billdata["hsn_code"],
			'warehouse_id' => $Warehouse_id,
			'billitem_length' => $length,
			'billitem_width' => $width,
			'billitem_height' => $height
		);
		$exist_check = Billitem::model()->findByAttributes(array('bill_id' => $billId, 'purchaseitem_id' => $billdata["itemid"]));

		if ($billdata["astat"] == 1) {

			$attribute_data['category_id'] = $billdata["categoryid"];
			$attribute_data['remark'] = $billdata["categoryname"];
			$attribute_data['created_date'] = $createdDate;

			if (empty($exist_check)) {
				$updateBillItems = Yii::app()->db->createCommand()->insert($this->tableNameAcc('billitem', 0), $attribute_data);
			}
			$billItemId = Yii::app()->db->getLastInsertID();
			$quantityRem = $billdata["availqty"] - $billdata["quantity"];
			$result[1] = $billItemId;
		} else if ($billdata["astat"] == 2) {

			$updateBillItems = Yii::app()->db->createCommand("DELETE FROM " . $this->tableNameAcc('billitem', 0) . " WHERE bill_id = " . $billId . " and purchaseitem_id = " . $billdata["itemid"]);
			$updateBillItems->execute();
			$quantityRem = $billdata["availqty"];
			$result[1] = "";
		} else if ($billdata["astat"] == 3) {

			if ($billdata["categoryid"] != '') {
				$attribute_data['category_id'] = $billdata["categoryid"];
			} else {
				$attribute_data['remark'] = $billdata["categoryname"];
			}

			$updateBillItems = Yii::app()->db->createCommand()->update(
				$this->tableNameAcc('billitem', 0),
				$attribute_data,
				'bill_id=:billid  AND
					purchaseitem_id=:itemid',
				array(':billid' => $billId, ':itemid' => $billdata["itemid"])
			);
			$quantityRem = $billdata["availqty"] - $billdata["quantity"];
			$result[1] = $billItemId;
		} else {
			$updateBillItems = Yii::app()->db->createCommand("DELETE FROM " . $this->tableNameAcc('billitem', 0) . " WHERE bill_id = " . $billId . " and purchaseitem_id = " . $billdata["itemid"]);
			$updateBillItems->execute();
			$quantityRem = $billdata["availqty"];
			$result[1] = "";
		}

		if ($quantityRem > 0)
			$itemStatus = 90;
		else
			$itemStatus = 91;

		$purchase_items_model = PurchaseItems::model()->findByPk($billdata["itemid"]);
		$purchase_items_model->item_status = $itemStatus;
		$purchase_items_model->save();
		$purchase_model = Purchase::model()->findByPk($billdata["pid"]);
		$purchase_model->purchase_billing_status = $billdata["purchasestatus"];
		$purchase_model->save(false);

		$item_quantity_row = Billitem::model()->find(array(
			'select' => 'SUM(billitem_quantity) as billitem_quantity',
			'condition' => 'purchaseitem_id= ' . $billdata["itemid"]
		));

		$item_quantity = $item_quantity_row->billitem_quantity;
		$purchase_item_model = PurchaseItems::model()->findByPk($billdata["itemid"]);
		$bill_item_model = Billitem::model()->findByPk($billItemId);
		if ($billdata["actualquantity"] < $item_quantity) {
			$bill_item_model->approve_status = 1;
			$bill_item_model->approve_check = 0;
			
		} else {
			$bill_item_model->approve_status = NULL;
		}
		if ($purchase_item_model['rate'] < $billdata["rate"]) {
			$bill_item_model->rate_approve = 1;
			$bill_item_model->approve_check = 0;
		} else {
			$bill_item_model->rate_approve = NULL;
		}
		if (($billdata["actualquantity"] < $item_quantity) && ($purchase_item_model['rate'] < $billdata["rate"])) {
			$bill_item_model->approve_check = 0;
		}

		$bill_item_model->save();

		$result[0] = $billId;
		$unbilled_amount = $this->getUnbilledAmount($billdata["pid"]);
		$newpurchase = Purchase::model()->findByPk($billdata["pid"]);
		$newpurchase->unbilled_amount = round($unbilled_amount, 2);
		$newpurchase->save(false);
		$result['amount'] = $billNTotal;
		$result['tax_amount'] = $billNTax;
		$result['disc_amount'] = $billNDiscount;
		$result['tot_amount'] = $billNGTotal;
		echo json_encode($result);
	}
	public function actionUpdateBillsOnReload()
	{
		$billId = isset($_REQUEST["billid"]) ? $_REQUEST["billid"] : "";
		$billNumber = isset($_REQUEST["billnumber"]) ? $_REQUEST["billnumber"] : "";
		if ($billId != "") {
			$updateBills = Yii::app()->db->createCommand("UPDATE " . $this->tableNameAcc('bills', 0) . " SET bill_status = 2 WHERE bill_id =" . $billId);
			$updateBills->execute();
		}
		if ($billNumber != "") {
			$deleteBills = Yii::app()->db->createCommand("DELETE FROM " . $this->tableNameAcc('bills', 0) . " WHERE bill_number = '" . $billNumber . "' and bill_status = 1");
			$deleteBills->execute();
		}
		echo 1;
	}

	public function actionTaxreport()
	{
		$tblpx = Yii::app()->db->tablePrefix;
		$newmodel = new Bills();
		if (isset($_POST['date_from']) && isset($_POST['date_to'])) {
			$date_from = date('Y-m-d', strtotime($_POST['date_from']));
			$date_to = date('Y-m-d', strtotime($_POST['date_to']));

			if (!empty($_POST['company_id'])) {
				$company_id = $_POST['company_id'];
			} else {
				$company_id = "";
			}

			$user = Users::model()->findByPk(Yii::app()->user->id);
			$arrVal = explode(',', $user->company_id);
			$newQuery = "";
			$newQuery1 = "";
			$newQuery2 = "";
			$newQuery3 = "";
			$newQuery4 = "";
			$newQuery5 = "";

			if ($company_id == NULL) {
				foreach ($arrVal as $arr) {
					if ($newQuery)
						$newQuery .= ' OR';
					if ($newQuery1)
						$newQuery1 .= ' OR';
					if ($newQuery2)
						$newQuery2 .= ' OR';
					if ($newQuery3)
						$newQuery3 .= ' OR';
					if ($newQuery4)
						$newQuery4 .= ' OR';
					if ($newQuery5)
						$newQuery5 .= ' OR';
					$newQuery .= " FIND_IN_SET('" . $arr . "', {$tblpx}bills.company_id)";
					$newQuery1 .= " FIND_IN_SET('" . $arr . "', {$tblpx}expenses.company_id)";
					$newQuery2 .= " FIND_IN_SET('" . $arr . "', {$tblpx}dailyexpense.company_id)";
					$newQuery3 .= " FIND_IN_SET('" . $arr . "', {$tblpx}dailyvendors.company_id)";
					$newQuery4 .= " FIND_IN_SET('" . $arr . "', {$tblpx}invoice.company_id)";
					$newQuery5 .= " FIND_IN_SET('" . $arr . "', {$tblpx}purchase_return.company_id)";
				}
			} else {
				$newQuery .= " FIND_IN_SET('" . $company_id . "', {$tblpx}bills.company_id)";
				$newQuery1 .= " FIND_IN_SET('" . $company_id . "', {$tblpx}expenses.company_id)";
				$newQuery2 .= " FIND_IN_SET('" . $company_id . "', {$tblpx}dailyexpense.company_id)";
				$newQuery3 .= " FIND_IN_SET('" . $company_id . "', {$tblpx}dailyvendors.company_id)";
				$newQuery4 .= " FIND_IN_SET('" . $company_id . "', {$tblpx}invoice.company_id)";
				$newQuery5 .= " FIND_IN_SET('" . $company_id . "', {$tblpx}purchase_return.company_id)";
			}



			// purchase  section
			$purchase = array();
			$purchase_head = array();
			$purchase_head_igst = array();
			$a = 0;
			$b = 0;
			$c = 0;
			$d = 0;
			$e = 0;
			$f = 0;
			$g = 0;
			$h = 0;
			$i = 0;
			$j = 0;
			// bills
			$data = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}bills LEFT JOIN {$tblpx}purchase ON {$tblpx}bills.purchase_id= {$tblpx}purchase.p_id LEFT JOIN {$tblpx}vendors ON {$tblpx}purchase.vendor_id={$tblpx}vendors.vendor_id WHERE (" . $newQuery . ") AND {$tblpx}bills.bill_date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND {$tblpx}bills.bill_taxamount > 0 ORDER BY {$tblpx}bills.bill_date desc")->queryAll();
			foreach ($data as $key => $value) {
				$purchase[$key]['bill_number'] = $value['bill_number'];
				$purchase[$key]['date'] = $value['bill_date'];
				$purchase[$key]['vendor_name'] = $value['name'];
				$purchase[$key]['gst_no'] = $value['gst_no'];
				$purchase[$key]['totalamount'] = $value['bill_totalamount'];
				$purchase[$key]['id'] = $value['bill_id'];
				$purchase[$key]['type'] = 'bills';
			}

			$data1 = Yii::app()->db->createCommand("SELECT DISTINCT billitem_cgstpercent,billitem_igstpercent,billitem_sgstpercent FROM {$tblpx}billitem  LEFT JOIN {$tblpx}bills ON {$tblpx}bills.bill_id ={$tblpx}billitem.bill_id WHERE {$tblpx}bills.bill_date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND billitem_cgstpercent > 0 AND billitem_sgstpercent > 0 AND (" . $newQuery . ") GROUP BY billitem_cgstpercent, billitem_sgstpercent")->queryAll();
			foreach ($data1 as $key => $value) {
				$purchase_head[$key]['cgstpercent'] = $value['billitem_cgstpercent'];
				$purchase_head[$key]['sgstpercent'] = $value['billitem_sgstpercent'];
				//$purchase_head[$key]['igstpercent'] = $value['billitem_igstpercent'];
			}

			$data5 = Yii::app()->db->createCommand("SELECT DISTINCT billitem_igstpercent FROM {$tblpx}billitem  LEFT JOIN {$tblpx}bills ON {$tblpx}bills.bill_id ={$tblpx}billitem.bill_id WHERE {$tblpx}bills.bill_date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND billitem_igstpercent > 0 AND (" . $newQuery . ") GROUP BY billitem_igstpercent")->queryAll();
			foreach ($data5 as $key => $value) {
				$purchase_head_igst[$key]['igstpercent'] = $value['billitem_igstpercent'];
			}

			// day book

			$a = count($data);
			$data6 = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}expenses LEFT JOIN {$tblpx}projects ON {$tblpx}expenses.projectid={$tblpx}projects.pid LEFT JOIN {$tblpx}expense_type ON {$tblpx}expenses.exptype = {$tblpx}expense_type.type_id LEFT JOIN {$tblpx}vendors ON {$tblpx}expenses.vendor_id= {$tblpx}vendors.vendor_id WHERE {$tblpx}expenses.date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND (" . $newQuery1 . ") AND {$tblpx}expenses.bill_id IS NULL AND {$tblpx}expenses.invoice_id IS NULL AND {$tblpx}expenses.type=73 AND ((expense_sgstp > 0 AND expense_cgstp > 0) OR (expense_igstp >0)) ORDER BY {$tblpx}expenses.date desc")->queryAll();
			foreach ($data6 as $key => $value) {
				$key = count(($data1)) + 1;
				$purchase[$a]['bill_number'] = '';
				$purchase[$a]['date'] = $value['date'];
				$purchase[$a]['vendor_name'] = $value['name'];
				$purchase[$a]['gst_no'] = $value['gst_no'];
				$purchase[$a]['totalamount'] = $value['amount'];
				$purchase[$a]['id'] = $value['exp_id'];
				$purchase[$a]['type'] = 'daybook';
				$a++;
			}

			$b = count($data1) + 1;
			$data7 = Yii::app()->db->createCommand("SELECT DISTINCT expense_sgstp, expense_cgstp, expense_igstp FROM {$tblpx}expenses WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND (expense_sgstp > 0 AND expense_cgstp > 0) AND {$tblpx}expenses.bill_id IS NULL AND {$tblpx}expenses.invoice_id IS NULL AND {$tblpx}expenses.type=73 AND (" . $newQuery1 . ") GROUP BY expense_sgstp,expense_cgstp")->queryAll();
			foreach ($data7 as $key => $value) {
				$purchase_head[$b]['cgstpercent'] = $value['expense_cgstp'];
				$purchase_head[$b]['sgstpercent'] = $value['expense_sgstp'];
				//$purchase_head[$b]['igstpercent'] = $value['expense_igstp'];
				$b++;
			}

			$c = count($data5) + 1;
			;
			$data8 = Yii::app()->db->createCommand("SELECT DISTINCT expense_igstp FROM {$tblpx}expenses WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND (expense_igstp >0) AND {$tblpx}expenses.bill_id IS NULL AND {$tblpx}expenses.invoice_id IS NULL AND {$tblpx}expenses.type=73 AND (" . $newQuery1 . ") GROUP BY expense_igstp")->queryAll();
			foreach ($data8 as $key => $value) {
				$purchase_head_igst[$c]['igstpercent'] = $value['expense_igstp'];
				$c++;
			}

			// daily expense


			$d = count($data) + count($data6);
			$data9 = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}dailyexpense WHERE {$tblpx}dailyexpense.date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND (" . $newQuery2 . ") AND {$tblpx}dailyexpense.exp_type=73 AND ((dailyexpense_sgstp > 0 AND dailyexpense_cgstp > 0) OR (dailyexpense_igstp > 0)) ORDER BY {$tblpx}dailyexpense.date desc")->queryAll();
			foreach ($data9 as $key => $value) {
				$key = count(($data1)) + 1;
				$purchase[$d]['bill_number'] = '';
				$purchase[$d]['date'] = $value['date'];
				$purchase[$d]['vendor_name'] = '';
				$purchase[$d]['gst_no'] = '';
				$purchase[$d]['totalamount'] = $value['amount'];
				$purchase[$d]['id'] = $value['dailyexp_id'];
				$purchase[$d]['type'] = 'dailyexpense';
				$d++;
			}

			$e = count($data1) + count($data7) + 1;
			$data10 = Yii::app()->db->createCommand("SELECT DISTINCT dailyexpense_sgstp, dailyexpense_cgstp, dailyexpense_igstp FROM {$tblpx}dailyexpense WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND (dailyexpense_sgstp > 0 AND dailyexpense_cgstp > 0) AND {$tblpx}dailyexpense.exp_type=73 AND (" . $newQuery2 . ") GROUP BY dailyexpense_sgstp, dailyexpense_cgstp")->queryAll();
			foreach ($data10 as $key => $value) {
				$purchase_head[$e]['cgstpercent'] = $value['dailyexpense_cgstp'];
				$purchase_head[$e]['sgstpercent'] = $value['dailyexpense_sgstp'];
				$e++;
			}


			$f = count($data5) + count($data8) + 1;
			;
			$data11 = Yii::app()->db->createCommand("SELECT DISTINCT dailyexpense_igstp FROM {$tblpx}dailyexpense WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND (dailyexpense_igstp >0) AND {$tblpx}dailyexpense.exp_type=73 AND (" . $newQuery2 . ") GROUP BY dailyexpense_igstp")->queryAll();
			foreach ($data11 as $key => $value) {
				$purchase_head_igst[$c]['igstpercent'] = $value['dailyexpense_igstp'];
				$c++;
			}


			// vendor payment

			$g = count($data) + count($data6) + count($data9);
			$data12 = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}dailyvendors LEFT JOIN {$tblpx}vendors ON {$tblpx}dailyvendors.vendor_id = {$tblpx}vendors.vendor_id  WHERE {$tblpx}dailyvendors.date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND (" . $newQuery3 . ") AND {$tblpx}dailyvendors.tax_amount > 0 ORDER BY {$tblpx}dailyvendors.date desc")->queryAll();
			foreach ($data12 as $key => $value) {
				$key = count(($data1)) + 1;
				$purchase[$g]['bill_number'] = '';
				$purchase[$g]['date'] = $value['date'];
				$purchase[$g]['vendor_name'] = $value['name'];
				$purchase[$g]['gst_no'] = $value['gst_no'];
				$purchase[$g]['totalamount'] = $value['amount'];
				$purchase[$g]['id'] = $value['daily_v_id'];
				$purchase[$g]['type'] = 'vendorpayment';
				$g++;
			}

			$h = count($data1) + count($data7) + count($data10) + 1;
			$data13 = Yii::app()->db->createCommand("SELECT DISTINCT sgst, cgst, igst FROM {$tblpx}dailyvendors WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND {$tblpx}dailyvendors.tax_amount >0 AND (" . $newQuery3 . ") AND sgst > 0 AND cgst > 0 GROUP BY sgst, cgst")->queryAll();
			foreach ($data13 as $key => $value) {
				$purchase_head[$h]['cgstpercent'] = $value['cgst'];
				$purchase_head[$h]['sgstpercent'] = $value['sgst'];
				$h++;
			}

			$j = count($data5) + count($data8) + count($data11) + 1;
			;
			$data14 = Yii::app()->db->createCommand("SELECT DISTINCT igst FROM {$tblpx}dailyvendors WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND {$tblpx}dailyvendors.tax_amount > 0 AND (" . $newQuery3 . ") AND igst > 0 GROUP BY igst")->queryAll();
			foreach ($data14 as $key => $value) {
				$purchase_head_igst[$j]['igstpercent'] = $value['igst'];
				$j++;
			}

			// invoice

			$invoice_head = array();
			$invoice_head_igst = array();
			$sales = array();

			$p = 0;
			$q = 0;
			$r = 0;
			$s = 0;
			$t = 0;


			$data2 = Yii::app()->db->createCommand("SELECT {$tblpx}invoice.*, {$tblpx}projects.name as project_name,{$tblpx}clients.name as client,{$tblpx}clients.gst_no  FROM {$tblpx}invoice LEFT JOIN {$tblpx}projects ON {$tblpx}invoice.project_id = {$tblpx}projects.pid LEFT JOIN {$tblpx}clients ON {$tblpx}invoice.client_id = {$tblpx}clients.cid WHERE (" . $newQuery4 . ") AND {$tblpx}invoice.date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND {$tblpx}invoice.invoice_status ='saved' ORDER BY {$tblpx}invoice.date desc");
			$sales_data = $data2->queryAll();

			$data15 = Yii::app()->db->createCommand("SELECT {$tblpx}purchase_return.*,{$tblpx}clients.* FROM {$tblpx}purchase_return LEFT JOIN {$tblpx}bills ON {$tblpx}purchase_return.bill_id = {$tblpx}bills.bill_id LEFT JOIN {$tblpx}purchase ON {$tblpx}bills.purchase_id = {$tblpx}purchase.p_id LEFT JOIN {$tblpx}projects ON {$tblpx}purchase.project_id = {$tblpx}projects.pid LEFT JOIN {$tblpx}clients ON {$tblpx}projects.client_id = {$tblpx}clients.cid WHERE (" . $newQuery5 . ") AND {$tblpx}purchase_return.return_date BETWEEN '" . $date_from . "' AND '" . $date_to . "' ORDER BY {$tblpx}purchase_return.return_date desc ");
			$purchase_returndata = $data15->queryAll();

			$data3 = Yii::app()->db->createCommand("SELECT DISTINCT sgst,cgst,igst FROM {$tblpx}inv_list LEFT JOIN {$tblpx}invoice ON {$tblpx}invoice.invoice_id ={$tblpx}inv_list.inv_id WHERE {$tblpx}invoice.date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND sgst >0 AND cgst > 0 AND {$tblpx}invoice.invoice_status ='saved' AND (" . $newQuery4 . ") GROUP BY sgst,cgst");
			$sales_head = $data3->queryAll();

			$data4 = Yii::app()->db->createCommand("SELECT DISTINCT igst FROM {$tblpx}inv_list LEFT JOIN {$tblpx}invoice ON {$tblpx}invoice.invoice_id ={$tblpx}inv_list.inv_id WHERE {$tblpx}invoice.date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND igst >0 AND {$tblpx}invoice.invoice_status ='saved' AND (" . $newQuery4 . ")  GROUP BY igst");
			$igst_head_sales = $data4->queryAll();

			$data16 = Yii::app()->db->createCommand("SELECT DISTINCT returnitem_cgstpercent,returnitem_sgstpercent,returnitem_igstpercent FROM {$tblpx}purchase_returnitem LEFT JOIN {$tblpx}purchase_return ON {$tblpx}purchase_return.return_id ={$tblpx}purchase_returnitem.return_id WHERE {$tblpx}purchase_return.return_date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND returnitem_cgstpercent > 0 AND returnitem_sgstpercent > 0 AND (" . $newQuery5 . ")  GROUP BY returnitem_cgstpercent,returnitem_sgstpercent");
			$return_head = $data16->queryAll();

			$data17 = Yii::app()->db->createCommand("SELECT DISTINCT returnitem_igstpercent FROM {$tblpx}purchase_returnitem LEFT JOIN {$tblpx}purchase_return ON {$tblpx}purchase_return.return_id ={$tblpx}purchase_returnitem.return_id WHERE {$tblpx}purchase_return.return_date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND returnitem_igstpercent >0 AND (" . $newQuery5 . ")  GROUP BY returnitem_igstpercent");
			$return_headigst = $data17->queryAll();

			foreach ($sales_head as $key => $value) {
				$invoice_head[$key]['cgst'] = $value['cgst'];
				$invoice_head[$key]['sgst'] = $value['sgst'];
			}

			$q = count($sales_head) + 1;
			foreach ($return_head as $key => $value) {
				$invoice_head[$q]['cgst'] = $value['returnitem_cgstpercent'];
				$invoice_head[$q]['sgst'] = $value['returnitem_cgstpercent'];
				$q++;
			}

			foreach ($igst_head_sales as $key => $value) {
				$invoice_head_igst[$key]['igst'] = $value['igst'];
			}

			$r = count($igst_head_sales) + 1;
			foreach ($return_headigst as $key => $value) {
				$invoice_head_igst[$r]['igst'] = $value['returnitem_igstpercent'];
				$r++;
			}

			foreach ($sales_data as $key => $value) {
				$sales[$key]['client'] = $value['client'];
				$sales[$key]['date'] = $value['date'];
				$sales[$key]['inv_no'] = $value['inv_no'];
				$sales[$key]['return_no'] = '';
				$sales[$key]['gst_no'] = $value['gst_no'];
				$sales[$key]['totalamount'] = $value['amount'] + $value['tax_amount'];
				$sales[$key]['type'] = 'Invoice';
				$sales[$key]['id'] = $value['invoice_id'];
			}

			$p = count($sales_data);
			foreach ($purchase_returndata as $key => $value) {
				$sales[$p]['client'] = $value['name'];
				$sales[$p]['date'] = $value['return_date'];
				$sales[$p]['inv_no'] = '';
				$sales[$p]['return_no'] = $value['return_number'];
				$sales[$p]['gst_no'] = $value['gst_no'];
				$sales[$p]['totalamount'] = $value['return_totalamount'];
				$sales[$p]['type'] = 'Purchase Return';
				$sales[$p]['id'] = $value['return_id'];
				$p++;
			}

			//                                print_r($sales_head);
			//                                exit();

			$dataprovider = new CArrayDataProvider($sales, array(
				'id' => 'user',
				'keyField' => 'id',
				//'pagination' => array('pageSize' => 20)
				'pagination' => false
			));

			$dataprovider1 = new CArrayDataProvider($purchase, array(
				'id' => 'user',
				'keyField' => 'id',
				//'pagination' => array('pageSize' => 20)
				'pagination' => false
			));
		} else {

			$user = Users::model()->findByPk(Yii::app()->user->id);
			$arrVal = explode(',', $user->company_id);
			$newQuery = "";
			$newQuery1 = "";
			$newQuery2 = "";
			$newQuery3 = "";
			$newQuery4 = "";
			$newQuery5 = "";
			foreach ($arrVal as $arr) {
				if ($newQuery)
					$newQuery .= ' OR';
				if ($newQuery1)
					$newQuery1 .= ' OR';
				if ($newQuery2)
					$newQuery2 .= ' OR';
				if ($newQuery3)
					$newQuery3 .= ' OR';
				if ($newQuery4)
					$newQuery4 .= ' OR';
				if ($newQuery5)
					$newQuery5 .= ' OR';
				$newQuery .= " FIND_IN_SET('" . $arr . "', {$tblpx}bills.company_id)";
				$newQuery1 .= " FIND_IN_SET('" . $arr . "', {$tblpx}expenses.company_id)";
				$newQuery2 .= " FIND_IN_SET('" . $arr . "', {$tblpx}dailyexpense.company_id)";
				$newQuery3 .= " FIND_IN_SET('" . $arr . "', {$tblpx}dailyvendors.company_id)";
				$newQuery4 .= " FIND_IN_SET('" . $arr . "', {$tblpx}invoice.company_id)";
				$newQuery5 .= " FIND_IN_SET('" . $arr . "', {$tblpx}purchase_return.company_id)";
			}


			$company_id = "";
			$date_from = date("Y-m-") . "01";
			$date_to = date('Y-m-d');

			// purchase  section
			$purchase = array();
			$purchase_head = array();
			$purchase_head_igst = array();
			$a = 0;
			$b = 0;
			$c = 0;
			$d = 0;
			$e = 0;
			$f = 0;
			$g = 0;
			$h = 0;
			$i = 0;
			$j = 0;
			// bills
			$data = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}bills LEFT JOIN {$tblpx}purchase ON {$tblpx}bills.purchase_id= {$tblpx}purchase.p_id LEFT JOIN {$tblpx}vendors ON {$tblpx}purchase.vendor_id={$tblpx}vendors.vendor_id WHERE (" . $newQuery . ") AND {$tblpx}bills.bill_date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND {$tblpx}bills.bill_taxamount > 0 ORDER BY {$tblpx}bills.bill_date desc")->queryAll();
			foreach ($data as $key => $value) {
				$purchase[$key]['bill_number'] = $value['bill_number'];
				$purchase[$key]['date'] = $value['bill_date'];
				$purchase[$key]['vendor_name'] = $value['name'];
				$purchase[$key]['gst_no'] = $value['gst_no'];
				$purchase[$key]['totalamount'] = $value['bill_totalamount'];
				$purchase[$key]['id'] = $value['bill_id'];
				$purchase[$key]['type'] = 'bills';
			}

			$data1 = Yii::app()->db->createCommand("SELECT DISTINCT billitem_cgstpercent,billitem_igstpercent,billitem_sgstpercent FROM {$tblpx}billitem  LEFT JOIN {$tblpx}bills ON {$tblpx}bills.bill_id ={$tblpx}billitem.bill_id WHERE {$tblpx}bills.bill_date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND billitem_cgstpercent > 0 AND billitem_sgstpercent > 0 AND (" . $newQuery . ") GROUP BY billitem_cgstpercent, billitem_sgstpercent")->queryAll();
			foreach ($data1 as $key => $value) {
				$purchase_head[$key]['cgstpercent'] = $value['billitem_cgstpercent'];
				$purchase_head[$key]['sgstpercent'] = $value['billitem_sgstpercent'];
				//$purchase_head[$key]['igstpercent'] = $value['billitem_igstpercent'];
			}

			$data5 = Yii::app()->db->createCommand("SELECT DISTINCT billitem_igstpercent FROM {$tblpx}billitem  LEFT JOIN {$tblpx}bills ON {$tblpx}bills.bill_id ={$tblpx}billitem.bill_id WHERE {$tblpx}bills.bill_date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND billitem_igstpercent > 0 AND (" . $newQuery . ") GROUP BY billitem_igstpercent")->queryAll();
			foreach ($data5 as $key => $value) {
				$purchase_head_igst[$key]['igstpercent'] = $value['billitem_igstpercent'];
			}

			// day book

			$a = count($data);
			$data6 = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}expenses LEFT JOIN {$tblpx}projects ON {$tblpx}expenses.projectid={$tblpx}projects.pid LEFT JOIN {$tblpx}expense_type ON {$tblpx}expenses.exptype = {$tblpx}expense_type.type_id LEFT JOIN {$tblpx}vendors ON {$tblpx}expenses.vendor_id= {$tblpx}vendors.vendor_id WHERE {$tblpx}expenses.date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND (" . $newQuery1 . ") AND {$tblpx}expenses.bill_id IS NULL AND {$tblpx}expenses.invoice_id IS NULL AND {$tblpx}expenses.type=73 AND ((expense_sgstp > 0 AND expense_cgstp > 0) OR (expense_igstp >0)) ORDER BY {$tblpx}expenses.date desc")->queryAll();
			foreach ($data6 as $key => $value) {
				$key = count(($data1)) + 1;
				$purchase[$a]['bill_number'] = '';
				$purchase[$a]['date'] = $value['date'];
				$purchase[$a]['vendor_name'] = $value['name'];
				$purchase[$a]['gst_no'] = $value['gst_no'];
				$purchase[$a]['totalamount'] = $value['amount'];
				$purchase[$a]['id'] = $value['exp_id'];
				$purchase[$a]['type'] = 'daybook';
				$a++;
			}

			$b = count($data1) + 1;
			$data7 = Yii::app()->db->createCommand("SELECT DISTINCT expense_sgstp, expense_cgstp, expense_igstp FROM {$tblpx}expenses WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND (expense_sgstp > 0 AND expense_cgstp > 0) AND {$tblpx}expenses.bill_id IS NULL AND {$tblpx}expenses.invoice_id IS NULL AND {$tblpx}expenses.type=73 AND (" . $newQuery1 . ") GROUP BY expense_sgstp,expense_cgstp")->queryAll();
			foreach ($data7 as $key => $value) {
				$purchase_head[$b]['cgstpercent'] = $value['expense_cgstp'];
				$purchase_head[$b]['sgstpercent'] = $value['expense_sgstp'];
				//$purchase_head[$b]['igstpercent'] = $value['expense_igstp'];
				$b++;
			}

			$c = count($data5) + 1;
			;
			$data8 = Yii::app()->db->createCommand("SELECT DISTINCT expense_igstp FROM {$tblpx}expenses WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND (expense_igstp >0) AND {$tblpx}expenses.bill_id IS NULL AND {$tblpx}expenses.invoice_id IS NULL AND {$tblpx}expenses.type=73 AND (" . $newQuery1 . ") GROUP BY expense_igstp")->queryAll();
			foreach ($data8 as $key => $value) {
				$purchase_head_igst[$c]['igstpercent'] = $value['expense_igstp'];
				$c++;
			}

			// daily expense


			$d = count($data) + count($data6);
			$data9 = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}dailyexpense WHERE {$tblpx}dailyexpense.date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND (" . $newQuery2 . ") AND {$tblpx}dailyexpense.exp_type=73 AND ((dailyexpense_sgstp > 0 AND dailyexpense_cgstp > 0) OR (dailyexpense_igstp > 0)) ORDER BY {$tblpx}dailyexpense.date desc")->queryAll();
			foreach ($data9 as $key => $value) {
				$key = count(($data1)) + 1;
				$purchase[$d]['bill_number'] = '';
				$purchase[$d]['date'] = $value['date'];
				$purchase[$d]['vendor_name'] = '';
				$purchase[$d]['gst_no'] = '';
				$purchase[$d]['totalamount'] = $value['amount'];
				$purchase[$d]['id'] = $value['dailyexp_id'];
				$purchase[$d]['type'] = 'dailyexpense';
				$d++;
			}

			$e = count($data1) + count($data7) + 1;
			$data10 = Yii::app()->db->createCommand("SELECT DISTINCT dailyexpense_sgstp, dailyexpense_cgstp, dailyexpense_igstp FROM {$tblpx}dailyexpense WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND (dailyexpense_sgstp > 0 AND dailyexpense_cgstp > 0) AND {$tblpx}dailyexpense.exp_type=73 AND (" . $newQuery2 . ") GROUP BY dailyexpense_sgstp, dailyexpense_cgstp")->queryAll();
			foreach ($data10 as $key => $value) {
				$purchase_head[$e]['cgstpercent'] = $value['dailyexpense_cgstp'];
				$purchase_head[$e]['sgstpercent'] = $value['dailyexpense_sgstp'];
				$e++;
			}


			$f = count($data5) + count($data8) + 1;
			;
			$data11 = Yii::app()->db->createCommand("SELECT DISTINCT dailyexpense_igstp FROM {$tblpx}dailyexpense WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND (dailyexpense_igstp >0) AND {$tblpx}dailyexpense.exp_type=73 AND (" . $newQuery2 . ") GROUP BY dailyexpense_igstp")->queryAll();
			foreach ($data11 as $key => $value) {
				$purchase_head_igst[$c]['igstpercent'] = $value['dailyexpense_igstp'];
				$c++;
			}


			// vendor payment

			$g = count($data) + count($data6) + count($data9);
			$data12 = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}dailyvendors LEFT JOIN {$tblpx}vendors ON {$tblpx}dailyvendors.vendor_id = {$tblpx}vendors.vendor_id  WHERE {$tblpx}dailyvendors.date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND ($newQuery3) AND {$tblpx}dailyvendors.tax_amount > 0 ORDER BY {$tblpx}dailyvendors.date desc")->queryAll();
			foreach ($data12 as $key => $value) {
				$key = count(($data1)) + 1;
				$purchase[$g]['bill_number'] = '';
				$purchase[$g]['date'] = $value['date'];
				$purchase[$g]['vendor_name'] = $value['name'];
				$purchase[$g]['gst_no'] = $value['gst_no'];
				$purchase[$g]['totalamount'] = $value['amount'];
				$purchase[$g]['id'] = $value['daily_v_id'];
				$purchase[$g]['type'] = 'vendorpayment';
				$g++;
			}

			$h = count($data1) + count($data7) + count($data10) + 1;
			$data13 = Yii::app()->db->createCommand("SELECT DISTINCT sgst, cgst, igst FROM {$tblpx}dailyvendors WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND {$tblpx}dailyvendors.tax_amount >0 AND (" . $newQuery3 . ") AND sgst > 0 AND cgst > 0 GROUP BY sgst, cgst")->queryAll();
			foreach ($data13 as $key => $value) {
				$purchase_head[$h]['cgstpercent'] = $value['cgst'];
				$purchase_head[$h]['sgstpercent'] = $value['sgst'];
				$h++;
			}

			$j = count($data5) + count($data8) + count($data11) + 1;
			;
			$data14 = Yii::app()->db->createCommand("SELECT DISTINCT igst FROM {$tblpx}dailyvendors WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND {$tblpx}dailyvendors.tax_amount > 0 AND (" . $newQuery3 . ") AND igst > 0 GROUP BY igst")->queryAll();
			foreach ($data14 as $key => $value) {
				$purchase_head_igst[$j]['igstpercent'] = $value['igst'];
				$j++;
			}

			// invoice

			$invoice_head = array();
			$invoice_head_igst = array();
			$sales = array();

			$p = 0;
			$q = 0;
			$r = 0;
			$s = 0;
			$t = 0;


			$data2 = Yii::app()->db->createCommand("SELECT {$tblpx}invoice.*, {$tblpx}projects.name as project_name,{$tblpx}clients.name as client,{$tblpx}clients.gst_no  FROM {$tblpx}invoice LEFT JOIN {$tblpx}projects ON {$tblpx}invoice.project_id = {$tblpx}projects.pid LEFT JOIN {$tblpx}clients ON {$tblpx}invoice.client_id = {$tblpx}clients.cid WHERE (" . $newQuery4 . ") AND {$tblpx}invoice.date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND {$tblpx}invoice.invoice_status ='saved' ORDER BY {$tblpx}invoice.date desc");
			$sales_data = $data2->queryAll();

			$data15 = Yii::app()->db->createCommand("SELECT {$tblpx}purchase_return.*,{$tblpx}clients.* FROM {$tblpx}purchase_return LEFT JOIN {$tblpx}bills ON {$tblpx}purchase_return.bill_id = {$tblpx}bills.bill_id LEFT JOIN {$tblpx}purchase ON {$tblpx}bills.purchase_id = {$tblpx}purchase.p_id LEFT JOIN {$tblpx}projects ON {$tblpx}purchase.project_id = {$tblpx}projects.pid LEFT JOIN {$tblpx}clients ON {$tblpx}projects.client_id = {$tblpx}clients.cid WHERE (" . $newQuery5 . ") AND {$tblpx}purchase_return.return_date BETWEEN '" . $date_from . "' AND '" . $date_to . "' ORDER BY {$tblpx}purchase_return.return_date desc ");
			$purchase_returndata = $data15->queryAll();

			$data3 = Yii::app()->db->createCommand("SELECT DISTINCT sgst,cgst,igst FROM {$tblpx}inv_list LEFT JOIN {$tblpx}invoice ON {$tblpx}invoice.invoice_id ={$tblpx}inv_list.inv_id WHERE {$tblpx}invoice.date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND sgst >0 AND cgst > 0 AND {$tblpx}invoice.invoice_status ='saved' AND (" . $newQuery4 . ") GROUP BY sgst,cgst");
			$sales_head = $data3->queryAll();

			$data4 = Yii::app()->db->createCommand("SELECT DISTINCT igst FROM {$tblpx}inv_list LEFT JOIN {$tblpx}invoice ON {$tblpx}invoice.invoice_id ={$tblpx}inv_list.inv_id WHERE {$tblpx}invoice.date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND igst >0 AND {$tblpx}invoice.invoice_status ='saved' AND (" . $newQuery4 . ")  GROUP BY igst");
			$igst_head_sales = $data4->queryAll();

			$data16 = Yii::app()->db->createCommand("SELECT DISTINCT returnitem_cgstpercent,returnitem_sgstpercent,returnitem_igstpercent FROM {$tblpx}purchase_returnitem LEFT JOIN {$tblpx}purchase_return ON {$tblpx}purchase_return.return_id ={$tblpx}purchase_returnitem.return_id WHERE {$tblpx}purchase_return.return_date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND returnitem_cgstpercent > 0 AND returnitem_sgstpercent > 0 AND (" . $newQuery5 . ")  GROUP BY returnitem_cgstpercent,returnitem_sgstpercent");
			$return_head = $data16->queryAll();

			$data17 = Yii::app()->db->createCommand("SELECT DISTINCT returnitem_igstpercent FROM {$tblpx}purchase_returnitem LEFT JOIN {$tblpx}purchase_return ON {$tblpx}purchase_return.return_id ={$tblpx}purchase_returnitem.return_id WHERE {$tblpx}purchase_return.return_date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND returnitem_igstpercent >0 AND (" . $newQuery5 . ")  GROUP BY returnitem_igstpercent");
			$return_headigst = $data17->queryAll();

			foreach ($sales_head as $key => $value) {
				$invoice_head[$key]['cgst'] = $value['cgst'];
				$invoice_head[$key]['sgst'] = $value['sgst'];
			}

			$q = count($sales_head) + 1;
			foreach ($return_head as $key => $value) {
				$invoice_head[$q]['cgst'] = $value['returnitem_cgstpercent'];
				$invoice_head[$q]['sgst'] = $value['returnitem_cgstpercent'];
				$q++;
			}

			foreach ($igst_head_sales as $key => $value) {
				$invoice_head_igst[$key]['igst'] = $value['igst'];
			}

			$r = count($igst_head_sales) + 1;
			foreach ($return_headigst as $key => $value) {
				$invoice_head_igst[$r]['igst'] = $value['returnitem_igstpercent'];
				$r++;
			}

			foreach ($sales_data as $key => $value) {
				$sales[$key]['client'] = $value['client'];
				$sales[$key]['date'] = $value['date'];
				$sales[$key]['inv_no'] = $value['inv_no'];
				$sales[$key]['return_no'] = '';
				$sales[$key]['gst_no'] = $value['gst_no'];
				$sales[$key]['totalamount'] = $value['amount'] + $value['tax_amount'];
				$sales[$key]['type'] = 'Invoice';
				$sales[$key]['id'] = $value['invoice_id'];
			}

			$p = count($sales_data);
			foreach ($purchase_returndata as $key => $value) {
				$sales[$p]['client'] = $value['name'];
				$sales[$p]['date'] = $value['return_date'];
				$sales[$p]['inv_no'] = '';
				$sales[$p]['return_no'] = $value['return_number'];
				$sales[$p]['gst_no'] = $value['gst_no'];
				$sales[$p]['totalamount'] = $value['return_totalamount'];
				$sales[$p]['type'] = 'Purchase Return';
				$sales[$p]['id'] = $value['return_id'];
				$p++;
			}

			//                                print_r($sales_head);
			//                                exit();

			$dataprovider = new CArrayDataProvider($sales, array(
				'id' => 'user',
				'keyField' => 'id',
				//'pagination' => array('pageSize' => 20)
				'pagination' => false
			));

			$dataprovider1 = new CArrayDataProvider($purchase, array(
				'id' => 'user',
				'keyField' => 'id',
				//'pagination' => array('pageSize' => 20)
				'pagination' => false
			));
		}

		$purchase_head_igst = array_map("unserialize", array_unique(array_map("serialize", $purchase_head_igst)));
		$purchase_head = array_map("unserialize", array_unique(array_map("serialize", $purchase_head)));
		$invoice_head = array_map("unserialize", array_unique(array_map("serialize", $invoice_head)));
		$invoice_head_igst = array_map("unserialize", array_unique(array_map("serialize", $invoice_head_igst)));
		$this->render('tax_report', array(
			'purchase_head' => $purchase_head,
			'date_from' => $date_from,
			'date_to' => $date_to,
			'sales' => $sales,
			'sales_head' => $invoice_head,
			'dataProvider' => $dataprovider,
			'dataProvider1' => $dataprovider1,
			'igst_head_sales' => $invoice_head_igst,
			'purchase_head_igst' => $purchase_head_igst,
			'company_id' => $company_id,
		));
	}

	/*   bills with out po no   */

	public function actionAddbill()
	{
		$model = new Bills;
		$newmodel = new Purchase();
		$model2 = new Warehousestock;
		$tblpx = Yii::app()->db->tablePrefix;
		$user = Users::model()->findByPk(Yii::app()->user->id);
		$arrVal = explode(',', $user->company_id);
		$newQuery = "";
		$newQuery1 = "";
		foreach ($arrVal as $arr) {
			if ($newQuery)
				$newQuery .= ' OR';
			if ($newQuery)
				$newQuery1 .= ' OR';
			$newQuery .= " FIND_IN_SET('" . $arr . "', company_id)";
			$newQuery1 .= " FIND_IN_SET('" . $arr . "', p.company_id)";
		}
		$specification = $this->actionGetItemCategory($parent = 0, $spacing = '', $user_tree_array = '');
		if (Yii::app()->user->role == 1 || Yii::app()->user->role == 2) {
			$project = Yii::app()->db->createCommand("SELECT pid, name  FROM {$tblpx}projects WHERE " . $newQuery . " ORDER BY name")->queryAll();
		} else {
			$project = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}projects p JOIN {$tblpx}project_assign pa ON p.pid = pa.projectid WHERE " . $newQuery1 . " AND pa.userid = '" . Yii::app()->user->id . "'")->queryAll();
		}
		$vendor = Yii::app()->db->createCommand("SELECT DISTINCT vendor_id, name FROM {$tblpx}vendors WHERE (" . $newQuery . ") ORDER BY name")->queryAll();

		$this->render('addbills', array(
			'model' => $model,
			'model2' => $model2,
			'project' => $project,
			'vendor' => $vendor,
			'newmodel' => $newmodel,
			'specification' => $specification,
		));
	}
	public function actionAddDailyexpensebill()
	{

		$model = new DailyexpenseBill;
		$billId = isset($_GET['bill_id']) ? $_GET['bill_id'] : null;
		if ($billId !== null) {
			$model = DailyexpenseBill::model()->findByPk($billId);
			if ($model === null) {
				throw new CHttpException(404, 'The requested page does not exist.');
			}
		}
		if (Yii::app()->request->isAjaxRequest) {
			$bill_no = isset($_REQUEST['bill_no']) ? $_REQUEST['bill_no'] : '';
			$company_id = isset($_REQUEST['company_id']) ? $_REQUEST['company_id'] : '';
			$transaction_head = isset($_REQUEST['transaction_head']) ? $_REQUEST['transaction_head'] : '';
			$transaction_head_type = isset($_REQUEST['transaction_head_type']) ? $_REQUEST['transaction_head_type'] : '';
			$amount = isset($_REQUEST['amount']) ? $_REQUEST['amount'] : '';
			$sgst = isset($_REQUEST['sgst']) ? $_REQUEST['sgst'] : '';
			$cgst = isset($_REQUEST['cgst']) ? $_REQUEST['cgst'] : '';
			$igst = isset($_REQUEST['igst']) ? $_REQUEST['igst'] : '';
			$total_amount = isset($_REQUEST['total_amount']) ? $_REQUEST['total_amount'] : '';
			$due_date = isset($_REQUEST['due_date']) ? $_REQUEST['due_date'] : '';
			$description = isset($_REQUEST['description']) ? $_REQUEST['description'] : '';

			if (isset($_REQUEST['id']) && $_REQUEST['id'] != '') {
				$model = DailyexpenseBill::model()->findByPk($_REQUEST['id']);
			} else {
				$model = new DailyexpenseBill;
			}
			$model->bill_no = $bill_no;
			$model->company_id = $company_id;
			$model->transaction_head = $transaction_head;
			$model->transaction_head_type = $transaction_head_type;
			$model->amount = $amount;
			$model->sgst = $sgst;
			$model->cgst = $cgst;
			$model->igst = $igst;
			$model->total_amount = $total_amount;
			$model->due_date = date('Y-m-d', strtotime($due_date));
			$model->created_by = Yii::app()->user->id;
			$model->created_date = date("Y-m-d H:i:s");
			$model->description = $description;
			$dailyExpenseBill = new DailyexpenseBill;
			$count = $dailyExpenseBill->count('bill_no = :bill_no AND id != :bill_id', array(':bill_no' => $model->bill_no, ':bill_id' => $model->id));

			if ($count > 0) {
				$result = '{"status":"false","message":"Bill number already exist"}';
			} else {
				if ($model->validate() && $model->save()) {

					$result = '{"status":"true","message":"success", "redirectUrl": "' . Yii::app()->createUrl('dailyexpense/expenses') . '"}';
					Yii::app()->user->setFlash('success', "Bill data saved successfully.");
				} else {
					$result = '{"status":"false","message":"Fill mandatory fields"}';
				}

			}
			echo $result;
		} else {
			$this->render('adddailyexpensebills', array('model' => $model)); // Render the form view

		}
	}

	public function actionCreatenewbills()
	{
		//die('hi');
		$bill_id = $_REQUEST['bill_id'];
		$bill_number = $_REQUEST['bill_number'];
		$default_date = $_REQUEST['default_date'];
		$warehouse_id = $_REQUEST['warehouse_id'];
		$purchase_id = $_REQUEST['purchase_id'];
		$mrId = '';
		if (isset($_REQUEST['mrId'])) {
			$mrId = $_REQUEST['mrId'];
		}
		if (isset($_REQUEST['selected_items']) && !empty($_REQUEST['selected_items'])) {
			$material_selected_items = $_REQUEST['selected_items'];
		}

		$company_id = '';
		if (isset($_REQUEST['company_id'])) {
			$company_id = $_REQUEST['company_id'];
		}

		$expensehead_id = $_REQUEST['expensehead_id'];
		$result = '';
		$transaction = Yii::app()->db->beginTransaction();
		try {

			if (isset($bill_number) && isset($default_date)) {
				if ($bill_id == 0) {
					$newmodel = new Purchase;
					$newmodel->attributes = $_REQUEST;
					$newmodel->purchase_no = NULL;
					$newmodel->purchase_date = date('Y-m-d', strtotime($default_date));
					$newmodel->purchase_status = 'saved';
					$newmodel->purchase_billing_status = 93;
					$newmodel->type = 'bill';
					$newmodel->created_date = date('Y-m-d');
					$newmodel->created_by = Yii::app()->user->id;
					$newmodel->company_id = $company_id;
					$newmodel->expensehead_id = $expensehead_id;
					if (!empty($mrId)) {
						$newmodel->mr_id = $mrId;
					}
					if (!empty($material_selected_items)) {
						$newmodel->mr_material_ids = $material_selected_items;
					}

					$newmodel->save(false);
					$last_pid = $newmodel->p_id;

					$model = new Bills;
					$model->attributes = $_REQUEST;
					$model->bill_number = $bill_number;
					$model->purchase_id = $last_pid;
					$model->bill_date = date('Y-m-d', strtotime($default_date));
					$model->created_date = date('Y-m-d');
					$model->created_by = Yii::app()->user->id;
					$model->company_id = $company_id;
					$model->warehouse_id = $warehouse_id;
					$model->updated_date = Date('Y-m-d');
					$model->save(false);
					$last_id = $model->bill_id;
					if (isset($mrId) && !empty($mrId)) {
						$mrModel = MaterialRequisition::model()->findByPk($mrId);
						if (!empty($mrModel)) {
							$new_pos = "";
							$pos = $mrModel->purchase_id;
							if (!empty($pos)) {
								$new_pos = $pos . ',' . $last_pid;
							}
							if (empty($new_pos)) {
								$new_pos = $last_pid;
							}
							$new_bills = "";
							$bills = $mrModel->bill_id;
							if (!empty($bills)) {
								$new_bills = $bills . ',' . $last_id;
							}
							if (empty($new_bills)) {
								$new_bills = $last_id;
							}

							$mrModel->purchase_id = $new_pos;
							$mrModel->save();
							$mr_no = Controller::updateDynamicMaterialReqNo($mrId, $last_pid);
						}
						$mrModel->purchase_id = $new_pos;
						$mrModel->bill_id = $new_bills;
						$mrModel->save();
					}
					if (!empty($material_selected_items)) {
						$selected_materials_arr = explode(',', $material_selected_items);
						foreach ($selected_materials_arr as $selected_material) {
							$criteria = new CDbCriteria();
							$criteria->condition = 'mr_id = :mr_id AND material_item_id = :material_item_id';
							$criteria->params = array(':mr_id' => $mrId, ':material_item_id' => $selected_material);

							$mr_materials_model = MaterialRequisitionItems::Model()->findAll($criteria);

							if (!empty($mr_materials_model)) {
								foreach ($mr_materials_model as $material) {
									$material->po_id = $last_pid;
									$material->bill_id = $last_id;
									$material->save(); // Update the po_id value

								}
							}
						}
					}
					$pms_api_integration_model = ApiSettings::model()->findByPk(1);
					$pms_api_integration = $pms_api_integration_model->api_integration_settings;
					$mr_status = '';
					if ($pms_api_integration == 1) {
						$p_id = $last_pid;
						$purchase_model = Purchase::model()->findByPk($p_id);
						$mrId = $purchase_model->mr_id;
						//die($mrId);
						if (!empty($mrId)) {
							$materialRequisition = MaterialRequisition::model()->findByPk($mrId);
							$pms_mr_id = "";
							$mr_status = "";
							if ($materialRequisition) {
								$request = array();
								$pms_mr_id = $materialRequisition->pms_mr_id;
								$sql = "SELECT po_id FROM pms_material_requisition_items WHERE mr_id = :mr_id";
								$command = Yii::app()->db->createCommand($sql);
								$command->bindParam(':mr_id', $mrId, PDO::PARAM_INT);
								$items = $command->queryAll();
								//echo "<pre>";print_r($items);exit;
								$allSaved = '1';

								foreach ($items as $item) {
									if (empty($item['po_id'])) {
										$allSaved = '0';
										break;
									}
									if ($allSaved) {
										// Check the po_status of the current item
										$sql = "SELECT purchase_status FROM jp_purchase WHERE p_id = :po_id";
										$command = Yii::app()->db->createCommand($sql);
										$command->bindParam(':po_id', $item['po_id'], PDO::PARAM_INT);
										$poStatus = $command->queryScalar();

										if ($poStatus !== 'saved') {
											$allSaved = '0';
											break;
										}
									}
								}
								if ($allSaved == '1') {
									$mr_status = "4";
								} else {
									$mr_status = "3";
								}
								// Update the mr_status based on the check
								$attributes = array('po_id' => $p_id, 'mr_id' => $mrId);
								$materialRequisition_items = MaterialRequisitionItems::model()->findAllByAttributes($attributes);
								$template_materials = array();

								foreach ($materialRequisition_items as $materialRequisition_item) {
									$pms_template_id = $materialRequisition_item->pms_template_material_id;
									array_push($template_materials, $pms_template_id);
								}

								$request = [
									'template_materials' => $template_materials,
									'mr_status' => $mr_status,
								];



							}

							$slug = "api/mr-approve/" . $pms_mr_id;
							$method = "POST";

							$globalapi_response = GlobalApiCall::callApi($slug, $method, $request);
						}

					}

					$result = array(
						'response' => 'success',
						'msg' => 'Bill added successfully',
						'mrId' => $mrId,
						'bill_id' => $last_id,
						'p_id' => $last_pid
					);

				} else {
					$bill_warehouse_id = isset($warehouse_id) ? $warehouse_id : null;

					$newmodel = Purchase::model()->findByPk($purchase_id);
					$newmodel->attributes = $_REQUEST;
					$newmodel->purchase_no = NULL;
					$newmodel->purchase_date = date('Y-m-d', strtotime($default_date));
					$newmodel->created_date = date('Y-m-d');
					$newmodel->type = 'bill';
					// $newmodel->company_id = $company_id;
					$newmodel->expensehead_id = $expensehead_id;
					$newmodel->save(false);

					$model = Bills::model()->findByPk($bill_id);
					$model->attributes = $_REQUEST;
					$model->bill_number = $bill_number;
					$model->warehouse_id = $bill_warehouse_id;
					$model->bill_date = date('Y-m-d', strtotime($default_date));
					$model->updated_date = date('Y-m-d');
					//$model->company_id = $company_id;

					if ($model->save(false)) {
						if (isset($mrId) && !empty($mrId)) {
							$mrModel = MaterialRequisition::model()->findByPk($mrId);
							//echo "<pre>";print_r($mrModel);exit;
							$mrModel->purchase_id = $last_pid;
							$mrModel->bill_id = $last_id;
							$mrModel->save();
						}
						$result = array(
							'response' => 'success',
							'msg' => 'Bill updated successfully',

							'mrId' => $mrId,
							'bill_id' => $bill_id,
							'p_id' => $purchase_id
						);
					}
				}
			}

			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollBack();
		} finally {
			echo json_encode($result);
		}
	}

	public function actionBillsitem()
	{
		$data = $_REQUEST['data'];


		if (!empty($data['item_amount'])) {
			$billitem_amount = $data['item_amount'];
		} else {
			$billitem_amount = 0;
		}

		if (!empty($data['dis_amount'])) {
			$billitem_discountamount = $data['dis_amount'];
		} else {
			$billitem_discountamount = 0;
		}

		if (!empty($data['tax_amount'])) {
			$billitem_taxamount = $data['tax_amount'];
		} else {
			$billitem_taxamount = 0;
		}

		if (!empty($data['disp'])) {
			$billitem_discountpercent = $data['disp'];
		} else {
			$billitem_discountpercent = 0;
		}

		if (!empty($data['sgstp'])) {
			$billitem_sgstpercent = $data['sgstp'];
		} else {
			$billitem_sgstpercent = 0;
		}

		if (!empty($data['sgst_amount'])) {
			$billitem_sgst = $data['sgst_amount'];
		} else {
			$billitem_sgst = 0;
		}

		if (!empty($data['cgstp'])) {
			$billitem_cgstpercent = $data['cgstp'];
		} else {
			$billitem_cgstpercent = 0;
		}

		if (!empty($data['cgst_amount'])) {
			$billitem_cgst = $data['cgst_amount'];
		} else {
			$billitem_cgst = 0;
		}

		if (!empty($data['igstp'])) {
			$billitem_igstpercent = $data['igstp'];
		} else {
			$billitem_igstpercent = 0;
		}

		if (!empty($data['igst_amount'])) {
			$billitem_igst = $data['igst_amount'];
		} else {
			$billitem_igst = 0;
		}

		if (!empty($data['bibatch'])) {
			$bibatch = $data['bibatch'];
		} else {
			$bibatch = '';
		}

		$item_model = new Billitem();
		$item_model->bill_id = $data['bill_id'];
		$item_model->batch = $data['bibatch'];
		$item_model->billitem_quantity = $data['quantity'];
		$item_model->billitem_unit = $data['unit'];
		$item_model->billitem_hsn_code = $data['hsn_code'];
		$item_model->billitem_rate = $data['rate'];
		$item_model->billitem_quantity = $data['purchase_quantity'];
		$item_model->billitem_unit = $data['purchaseitem_unit'];
		$item_model->billitem_rate = $data['purchase_rate'];
		$item_model->billitem_amount = $billitem_amount;
		$item_model->category_id = $data['description'];
		$item_model->remark = $data['remark'];
		$item_model->billitem_taxpercent = $billitem_igstpercent + $billitem_cgstpercent + $billitem_sgstpercent;
		$item_model->billitem_taxamount = $billitem_taxamount;
		$item_model->billitem_discountpercent = $billitem_discountpercent;
		$item_model->billitem_discountamount = $billitem_discountamount;
		$item_model->billitem_cgst = $billitem_cgst;
		$item_model->billitem_cgstpercent = $billitem_cgstpercent;
		$item_model->billitem_sgst = $billitem_sgst;
		$item_model->billitem_sgstpercent = $billitem_sgstpercent;
		$item_model->billitem_igst = $billitem_igst;
		$item_model->billitem_igstpercent = $billitem_igstpercent;
		$item_model->created_date = date('Y-m-d');
		$item_model->billitem_taxslab = $data['tax_slab'];
		$item_model->purchaseitem_unit = $data['unit'];
		$item_model->purchaseitem_quantity = $data['quantity'];
		$item_model->purchaseitem_rate = $data['rate'];
		if ($item_model->save(false)) {

			$last_id = $item_model->billitem_id;
			$tblpx = Yii::app()->db->tablePrefix;
			$addcharges = Yii::app()->db->createCommand("SELECT SUM(amount) as amount FROM " . $tblpx . "additional_bill WHERE `bill_id`=" . $data['bill_id'])->queryRow();
			$data1 = Yii::app()->db->createCommand("SELECT SUM(billitem_amount) as bill_amount FROM {$tblpx}billitem WHERE bill_id=" . $data['bill_id'] . "")->queryRow();
			$data2 = Yii::app()->db->createCommand("SELECT SUM(billitem_taxamount) as bill_taxamount FROM {$tblpx}billitem WHERE bill_id=" . $data['bill_id'] . "")->queryRow();
			$data3 = Yii::app()->db->createCommand("SELECT SUM(billitem_discountamount) as bill_discountamount FROM {$tblpx}billitem WHERE bill_id=" . $data['bill_id'] . "")->queryRow();

			$model = Bills::model()->findByPk($data['bill_id']);
			if (!empty($model)) {

			}
			$model->bill_amount = $data1['bill_amount'];
			$model->bill_taxamount = $data2['bill_taxamount'];
			$model->bill_discountamount = $data3['bill_discountamount'];
			$model->bill_totalamount = ($data1['bill_amount'] + $data2['bill_taxamount'] + ($model->round_off)) - $data3['bill_discountamount'];
			//echo Yii::app()->user->company_id;;exit;
			//$model->company_id = Yii::app()->user->company_id;
			$model->updated_date = date('Y-m-d');
			$grand_total = ($data1['bill_amount'] + $data2['bill_taxamount'] + $addcharges['amount'] + ($model->round_off)) - $data3['bill_discountamount'];
			if ($model->save(false)) {
				$result = '';
				$bill_details = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}billitem  WHERE bill_id=" . $data['bill_id'] . " ORDER BY billitem_id desc")->queryAll();
				foreach ($bill_details as $key => $values) {
					if ($values['category_id'] == 0) {

						if ($values['remark'] != '') {
							$spc_details = $values['remark'];
						} else {
							$spc_details = $values['billitem_description'];
						}
						$spc_id = 'other';
					} else {
						$specsql = "SELECT id, cat_id,brand_id, specification, unit "
							. " FROM {$tblpx}specification "
							. " WHERE id=" . $values['category_id'] . "";
						$specification = Yii::app()->db->createCommand($specsql)->queryRow();
						$cat_sql = "SELECT * FROM {$tblpx}purchase_category "
							. " WHERE id='" . $specification['cat_id'] . "'";
						$parent_category = Yii::app()->db->createCommand($cat_sql)->queryRow();

						if ($specification['brand_id'] != NULL) {
							$brand_sql = "SELECT brand_name "
								. " FROM {$tblpx}brand "
								. " WHERE id=" . $specification['brand_id'] . "";
							$brand_details = Yii::app()->db->createCommand($brand_sql)->queryRow();
							$brand = '-' . $brand_details['brand_name'];
						} else {
							$brand = '';
						}

						$spc_details = $parent_category['category_name'] . $brand . '-' . $specification['specification'];
						$spc_id = $specification['id'];
					}
					$result .= '<tr>';
					$result .= '<td><div id="item_sl_no">' . ($key + 1) . '</div></td>';
					$result .= '<td><div class="item_description" id="' . $spc_id . '">' . $spc_details . '</div> </td>';
					$result .= '<td class="text-right"> <div class="" id="qty"> ' . $values['billitem_quantity'] . '</div> </td>';
					$result .= '<td> <div class="unit" id="unit"> ' . $values['billitem_unit'] . '</div> </td>';
					$result .= '<td class="text-right"><div class="" id="rateval">' . number_format($values['billitem_rate'], 2, '.', '') . '</div></td>';
					$result .= '<td> <div class="batch" id="batch"> ' . $values['batch'] . '</div> </td>';
					$result .= '<td> <div class="hsncode" id="hsncode"> ' . $values['billitem_hsn_code'] . '</div> </td>';
					$result .= '<td> <div class="po_quantity" id="po_quantity"> ' . $values['purchaseitem_quantity'] . '</div> </td>';
					$result .= '<td> <div class="po_unit" id="po_unit"> ' . $values['purchaseitem_unit'] . '</div> </td>';
					$result .= '<td> <div class="po_rate" id="po_rate"> ' . $values['purchaseitem_rate'] . '</div> </td>';

					$result .= '<td class="text-right">' . number_format($values['billitem_taxslab'], 2) . '</td>';
					$result .= '<td class="text-right">' . number_format($values['billitem_sgst'], 2) . '</td>';
					$result .= '<td class="text-right">' . number_format($values['billitem_sgstpercent'], 2, '.', '') . '</td>';
					$result .= '<td class="text-right">' . number_format($values['billitem_cgst'], 2) . '</td>';
					$result .= '<td class="text-right">' . number_format($values['billitem_cgstpercent'], 2, '.', '') . '</td>';
					$result .= '<td class="text-right">' . number_format($values['billitem_igst'], 2) . '</td>';
					$result .= '<td class="text-right">' . number_format($values['billitem_igstpercent'], 2, '.', '') . '</td>';
					$result .= '<td class="text-right"><div class="" id="rate">' . number_format($values['billitem_discountpercent'], 2, '.', '') . '</div></td>';
					$result .= '<td class="text-right"><div class="" id="rate">' . number_format($values['billitem_discountamount'], 2, '.', '') . '</div></td>';
					$result .= '<td class="text-right">' . number_format($values['billitem_amount'], 2, '.', '') . '</td>';
					$result .= '<td class="text-right">' . number_format($values['billitem_taxamount'], 2, '.', '') . '</td>';


					//$result .= '<td style="width: 60px;"><a href="#" id='.$values['billitem_id'].' class="removebtn"><span class="fa fa-trash"></span> </a> <a href="" id='.$values['billitem_id'].' class="edit_item"><span class="fa fa-edit"></span> </a></td>';
					$result .= '<td width="60"><span class="icon icon-options-vertical popover-test" data-toggle="popover"   data-placement="left" type="button" data-html="true" style="cursor: pointer;" ></span>
                                                            <div class="popover-content hide">
                                                                <ul class="tooltip-hiden">

                                                                        <li><a href="" id=' . $values['billitem_id'] . '  class="btn btn-xs btn-default edit_item">Edit</a></li> <li><a href="#" id=' . $values['billitem_id'] . ' class="btn btn-xs btn-default removebtn">Delete</a></li></ul></div></td>';

					$result .= '</tr>';
				}
				echo json_encode(array('response' => 'success', 'msg' => 'Bill item created successfully', 'html' => $result, 'discount_total' => number_format($data3['bill_discountamount'], 2), 'amount_total' => number_format($data1['bill_amount'], 2), 'tax_total' => number_format($data2['bill_taxamount'], 2), 'grand_total' => number_format($grand_total, 2)));
			}
		} else {
			echo json_encode(array('response' => 'error', 'msg' => 'Some problem occured'));
		}
	}

	public function actionRemovebillitem()
	{
		$data = $_REQUEST['data'];
		$bill_id = $data['bill_id'];
		$item_id = $data['item_id'];
		$tblpx = Yii::app()->db->tablePrefix;
		$del = Yii::app()->db->createCommand()->delete($tblpx . 'billitem', 'billitem_id=:billitem_id', array(':billitem_id' => $item_id));
		if ($del) {
			$addcharges = Yii::app()->db->createCommand("SELECT SUM(amount) as amount FROM " . $tblpx . "additional_bill WHERE `bill_id`=" . $bill_id)->queryRow();
			$data1 = Yii::app()->db->createCommand("SELECT SUM(billitem_amount) as bill_amount FROM {$tblpx}billitem WHERE bill_id=" . $bill_id . "")->queryRow();
			$data2 = Yii::app()->db->createCommand("SELECT SUM(billitem_taxamount) as bill_taxamount FROM {$tblpx}billitem WHERE bill_id=" . $bill_id . "")->queryRow();
			$data3 = Yii::app()->db->createCommand("SELECT SUM(billitem_discountamount) as bill_discountamount FROM {$tblpx}billitem WHERE bill_id=" . $bill_id . "")->queryRow();
			$model = Bills::model()->findByPk($bill_id);
			$model->bill_amount = !empty($data1['bill_amount']) ? $data1['bill_amount'] : 0;
			$model->bill_taxamount = $data2['bill_taxamount'];
			$model->bill_discountamount = $data3['bill_discountamount'];
			$model->bill_totalamount = ($data1['bill_amount'] + $data2['bill_taxamount'] + ($model->round_off)) - $data3['bill_discountamount'];
			$model->company_id = Yii::app()->user->company_id;
			$model->updated_date = date('Y-m-d');
			$grand_total = ($data1['bill_amount'] + $data2['bill_taxamount'] + $addcharges['amount'] + ($model->round_off)) - $data3['bill_discountamount'];
			$model->save();
			$result = '';
			$bill_details = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}billitem  WHERE bill_id=" . $data['bill_id'] . " ORDER BY billitem_id desc")->queryAll();
			foreach ($bill_details as $key => $values) {
				if ($values['category_id'] == 0) {

					if ($values['remark'] != '') {
						$spc_details = $values['remark'];
					} else {
						$spc_details = $values['billitem_description'];
					}
					$spc_id = 'other';
				} else {
					$spec_sql = "SELECT id, cat_id,brand_id, specification, unit "
						. " FROM {$tblpx}specification "
						. " WHERE id=" . $values['category_id'] . "";
					$specification = Yii::app()->db->createCommand($spec_sql)->queryRow();
					$cat_sql = "SELECT * FROM {$tblpx}purchase_category WHERE id='" . $specification['cat_id'] . "'";
					$parent_category = Yii::app()->db->createCommand($cat_sql)->queryRow();

					if ($specification['brand_id'] != NULL) {
						$brand_sql = "SELECT brand_name FROM {$tblpx}brand WHERE id=" . $specification['brand_id'] . "";
						$brand_details = Yii::app()->db->createCommand($brand_sql)->queryRow();
						$brand = '-' . $brand_details['brand_name'];
					} else {
						$brand = '';
					}

					$spc_details = $parent_category['category_name'] . $brand . '-' . $specification['specification'];
					$spc_id = $specification['id'];
				}


				$result .= '<tr>';
				$result .= '<td><div id="item_sl_no">' . ($key + 1) . '</div></td>';
				$result .= '<td><div class="item_description" id="' . $spc_id . '">' . $spc_details . '</div> </td>';
				$result .= '<td class="text-right"> <div class="" id="qty"> ' . $values['billitem_quantity'] . '</div> </td>';
				$result .= '<td> <div class="unit" id="unit"> ' . $values['billitem_unit'] . '</div> </td>';
				$result .= '<td class="text-right"><div class="" id="rateval">' . number_format($values['billitem_rate'], 2, '.', '') . '</div></td>';
				$result .= '<td> <div class="batch" id="batch"> ' . $values['batch'] . '</div> </td>';
				$result .= '<td> <div class="hsncode" id="hsncode"> ' . $values['billitem_hsn_code'] . '</div> </td>';
				$result .= '<td> <div class="po_quantity" id="po_quantity"> ' . $values['purchaseitem_quantity'] . '</div> </td>';
				$result .= '<td> <div class="po_unit" id="po_unit"> ' . $values['purchaseitem_unit'] . '</div> </td>';
				$result .= '<td> <div class="po_rate" id="po_rate"> ' . $values['purchaseitem_rate'] . '</div> </td>';

				$result .= '<td class="text-right">' . number_format($values['billitem_taxslab'], 2) . '</td>';
				$result .= '<td class="text-right">' . number_format($values['billitem_sgst'], 2) . '</td>';
				$result .= '<td class="text-right">' . number_format($values['billitem_sgstpercent'], 2, '.', '') . '</td>';
				$result .= '<td class="text-right">' . number_format($values['billitem_cgst'], 2) . '</td>';
				$result .= '<td class="text-right">' . number_format($values['billitem_cgstpercent'], 2, '.', '') . '</td>';
				$result .= '<td class="text-right">' . number_format($values['billitem_igst'], 2) . '</td>';
				$result .= '<td class="text-right">' . number_format($values['billitem_igstpercent'], 2, '.', '') . '</td>';
				$result .= '<td class="text-right"><div class="" id="rate">' . number_format($values['billitem_discountpercent'], 2, '.', '') . '</div></td>';
				$result .= '<td class="text-right"><div class="" id="rate">' . number_format($values['billitem_discountamount'], 2, '.', '') . '</div></td>';
				$result .= '<td class="text-right">' . number_format($values['billitem_amount'], 2, '.', '') . '</td>';
				$result .= '<td class="text-right">' . number_format($values['billitem_taxamount'], 2, '.', '') . '</td>';
				$result .= '<td width="60"><span class="icon icon-options-vertical popover-test" data-toggle="popover"   data-placement="left" type="button" data-html="true" style="cursor: pointer;" ></span>
                                                            <div class="popover-content hide">
                                                                <ul class="tooltip-hiden">

                                                                        <li><a href="" id=' . $values['billitem_id'] . '  class="btn btn-xs btn-default edit_item">Edit</a></li> <li><a href="#" id=' . $values['billitem_id'] . ' class="btn btn-xs btn-default removebtn">Delete</a></li></ul></div></td>';

				$result .= '</tr>';
			}
			echo json_encode(array('response' => 'success', 'msg' => 'Bill item deleted successfully', 'html' => $result, 'discount_total' => number_format($data3['bill_discountamount'], 2), 'amount_total' => number_format($data1['bill_amount'], 2), 'tax_total' => number_format($data2['bill_taxamount'], 2), 'grand_total' => number_format($grand_total, 2)));
		} else {
			echo json_encode(array('response' => 'error', 'msg' => 'Some problem occured'));
		}
	}

	public function actionUpdatesbillitem()
	{

		$data = $_REQUEST['data'];

		if (!empty($data['item_amount'])) {
			$billitem_amount = $data['item_amount'];
		} else {
			$billitem_amount = 0;
		}

		if (!empty($data['dis_amount'])) {
			$billitem_discountamount = $data['dis_amount'];
		} else {
			$billitem_discountamount = 0;
		}

		if (!empty($data['tax_amount'])) {
			$billitem_taxamount = $data['tax_amount'];
		} else {
			$billitem_taxamount = 0;
		}

		if (!empty($data['disp'])) {
			$billitem_discountpercent = $data['disp'];
		} else {
			$billitem_discountpercent = 0;
		}

		if (!empty($data['sgstp'])) {
			$billitem_sgstpercent = $data['sgstp'];
		} else {
			$billitem_sgstpercent = 0;
		}

		if (!empty($data['sgst_amount'])) {
			$billitem_sgst = $data['sgst_amount'];
		} else {
			$billitem_sgst = 0;
		}

		if (!empty($data['cgstp'])) {
			$billitem_cgstpercent = $data['cgstp'];
		} else {
			$billitem_cgstpercent = 0;
		}

		if (!empty($data['cgst_amount'])) {
			$billitem_cgst = $data['cgst_amount'];
		} else {
			$billitem_cgst = 0;
		}

		if (!empty($data['igstp'])) {
			$billitem_igstpercent = $data['igstp'];
		} else {
			$billitem_igstpercent = 0;
		}

		if (!empty($data['igst_amount'])) {
			$billitem_igst = $data['igst_amount'];
		} else {
			$billitem_igst = 0;
		}

		if (!empty($data['purchaseitem_quantity'])) {

			$purchaseitem_quantity = $data['purchaseitem_quantity'];
		} else {
			$purchaseitem_quantity = 0;
		}
		if (!empty($data['purchase_rate'])) {

			$purchase_rate = $data['purchase_rate'];
		} else {
			$purchase_rate = 0;
		}
		if (!empty($data['purchase_quantity'])) {

			$purchase_quantity = $data['purchase_quantity'];
		} else {
			$purchase_quantity = 0;
		}

		if (!empty($data['bibatch'])) {
			$bibatch = $data['bibatch'];
		} else {
			$bibatch = '';
		}

		$item_model = Billitem::model()->findByPk($data['item_id']);
		$item_model->billitem_quantity = $purchase_quantity;
		$item_model->billitem_unit = $data['purchaseitem_unit'];
		$item_model->batch = $data['bibatch'];
		$item_model->billitem_hsn_code = $data['hsn_code'];
		$item_model->billitem_rate = $purchase_rate;
		$item_model->billitem_amount = $billitem_amount;
		$item_model->category_id = $data['description'];
		$item_model->remark = $data['remark'];
		$item_model->billitem_taxpercent = $billitem_igstpercent + $billitem_cgstpercent + $billitem_sgstpercent;
		$item_model->billitem_taxamount = $billitem_taxamount;
		$item_model->billitem_discountpercent = $billitem_discountpercent;
		$item_model->billitem_discountamount = $billitem_discountamount;
		$item_model->billitem_cgst = $billitem_cgst;
		$item_model->billitem_cgstpercent = $billitem_cgstpercent;
		$item_model->billitem_sgst = $billitem_sgst;
		$item_model->billitem_sgstpercent = $billitem_sgstpercent;
		$item_model->billitem_igst = $billitem_igst;
		$item_model->billitem_igstpercent = $billitem_igstpercent;
		$item_model->billitem_taxslab = $data['tax_slab'];
		$item_model->purchaseitem_quantity = isset($data['quantity']) ? $data['quantity'] : "";
		$item_model->purchaseitem_unit = $data['unit'];
		$item_model->purchaseitem_rate = isset($data['rate']) ? $data['rate'] : "";
		if ($item_model->save(false)) {
			$tblpx = Yii::app()->db->tablePrefix;
			$data1 = Yii::app()->db->createCommand("SELECT SUM(billitem_amount) as bill_amount FROM {$tblpx}billitem WHERE bill_id=" . $data['bill_id'] . "")->queryRow();
			$data2 = Yii::app()->db->createCommand("SELECT SUM(billitem_taxamount) as bill_taxamount FROM {$tblpx}billitem WHERE bill_id=" . $data['bill_id'] . "")->queryRow();
			$data3 = Yii::app()->db->createCommand("SELECT SUM(billitem_discountamount) as bill_discountamount FROM {$tblpx}billitem WHERE bill_id=" . $data['bill_id'] . "")->queryRow();
			$addcharges = AdditionalBill::model()->find(array('select' => 'sum(amount) as amount', 'condition' => 'bill_id =' . $data['bill_id']));
			$model = Bills::model()->findByPk($data['bill_id']);
			$model->bill_amount = $data1['bill_amount'];
			$model->bill_taxamount = $data2['bill_taxamount'];
			$model->bill_discountamount = $data3['bill_discountamount'];
			$model->bill_totalamount = ($data1['bill_amount'] + $data2['bill_taxamount'] + ($model['round_off'])) - $data3['bill_discountamount'];
			$model->company_id = Yii::app()->user->company_id;
			$model->updated_date = date('Y-m-d');
			$grand_total = ($data1['bill_amount'] + $data2['bill_taxamount'] + $addcharges['amount'] + ($model['round_off'])) - $data3['bill_discountamount'];
			if ($model->save(false)) {
				$result = '';
				$bill_details = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}billitem  WHERE bill_id=" . $data['bill_id'] . " ORDER BY billitem_id desc")->queryAll();
				foreach ($bill_details as $key => $values) {
					if ($values['category_id'] == 0) {

						if ($values['remark'] != '') {
							$spc_details = $values['remark'];
						} else {
							$spc_details = $values['billitem_description'];
						}
						$spc_id = 'other';
					} else {
						$spec_sql = "SELECT id, cat_id,brand_id, specification, unit "
							. " FROM {$tblpx}specification "
							. " WHERE id=" . $values['category_id'] . "";
						$specification = Yii::app()->db->createCommand($spec_sql)->queryRow();
						$cat_sql = "SELECT * FROM {$tblpx}purchase_category "
							. " WHERE id='" . $specification['cat_id'] . "'";
						$parent_category = Yii::app()->db->createCommand($cat_sql)->queryRow();

						if ($specification['brand_id'] != NULL) {
							$brand_sql = "SELECT brand_name FROM {$tblpx}brand "
								. " WHERE id=" . $specification['brand_id'] . "";
							$brand_details = Yii::app()->db->createCommand($brand_sql)->queryRow();
							$brand = '-' . $brand_details['brand_name'];
						} else {
							$brand = '';
						}

						$spc_details = $parent_category['category_name'] . $brand . '-' . $specification['specification'];
						$spc_id = $specification['id'];
					}
					$result .= '<tr>';
					$result .= '<td><div id="item_sl_no">' . ($key + 1) . '</div></td>';
					$result .= '<td><div class="item_description" id="' . $spc_id . '">' . $spc_details . '</div> </td>';
					$result .= '<td class="text-right"> <div class="" id="qty"> ' . $values['billitem_quantity'] . '</div> </td>';
					$result .= '<td> <div class="unit" id="unit"> ' . $values['billitem_unit'] . '</div> </td>';
					$result .= '<td class="text-right"><div class="" id="rateval">' . number_format($values['billitem_rate'], 2, '.', '') . '</div></td>';
					$result .= '<td> <div class="batch" id="batch"> ' . $values['batch'] . '</div> </td>';
					$result .= '<td> <div class="hsncode" id="hsncode"> ' . $values['billitem_hsn_code'] . '</div> </td>';
					$result .= '<td> <div class="po_quantity" id="po_quantity"> ' . $values['purchaseitem_quantity'] . '</div> </td>';
					$result .= '<td> <div class="po_unit" id="po_unit"> ' . $values['purchaseitem_unit'] . '</div> </td>';
					$result .= '<td> <div class="po_rate" id="po_rate"> ' . $values['purchaseitem_rate'] . '</div> </td>';

					$result .= '<td class="text-right">' . number_format($values['billitem_taxslab'], 2) . '</td>';
					$result .= '<td class="text-right">' . number_format($values['billitem_sgst'], 2) . '</td>';
					$result .= '<td class="text-right">' . number_format($values['billitem_sgstpercent'], 2, '.', '') . '</td>';
					$result .= '<td class="text-right">' . number_format($values['billitem_cgst'], 2) . '</td>';
					$result .= '<td class="text-right">' . number_format($values['billitem_cgstpercent'], 2, '.', '') . '</td>';
					$result .= '<td class="text-right">' . number_format($values['billitem_igst'], 2) . '</td>';
					$result .= '<td class="text-right">' . number_format($values['billitem_igstpercent'], 2, '.', '') . '</td>';
					$result .= '<td class="text-right"><div class="" id="rate">' . number_format($values['billitem_discountpercent'], 2, '.', '') . '</div></td>';
					$result .= '<td class="text-right"><div class="" id="rate">' . number_format($values['billitem_discountamount'], 2, '.', '') . '</div></td>';
					$result .= '<td class="text-right">' . number_format($values['billitem_amount'], 2, '.', '') . '</td>';
					$result .= '<td class="text-right">' . number_format($values['billitem_taxamount'], 2, '.', '') . '</td>';



					$result .= '<td width="60"><span class="icon icon-options-vertical popover-test" data-toggle="popover"   data-placement="left" type="button" data-html="true" style="cursor: pointer;" ></span>
                                                            <div class="popover-content hide">
                                                                <ul class="tooltip-hiden">

                                                                        <li><a href="" id=' . $values['billitem_id'] . '  class="btn btn-xs btn-default edit_item">Edit</a></li> <li><a href="#" id=' . $values['billitem_id'] . ' class="btn btn-xs btn-default removebtn">Delete</a></li></ul></div></td>';

					$result .= '</tr>';
				}
				echo json_encode(array('response' => 'success', 'msg' => 'Bill item update successfully', 'html' => $result, 'discount_total' => number_format($data3['bill_discountamount'], 2), 'amount_total' => number_format($data1['bill_amount'], 2), 'tax_total' => number_format($data2['bill_taxamount'], 2), 'grand_total' => number_format($grand_total, 2)));
			}
		} else {
			echo json_encode(array('response' => 'error', 'msg' => 'Some problem occured'));
		}
	}

	public function actionEditbill($id)
	{

		$model = Bills::model()->find(array("condition" => "bill_id = " . $id . ""));
		$model2 = new Warehousestock;
		$newmodel = Purchase::model()->find(array("condition" => "p_id = " . $model->purchase_id . ""));
		$specification = $this->actionGetItemCategory($parent = 0, $spacing = '', $user_tree_array = '');

		if (empty($model)) {
			$this->redirect(array('bills/newlist'));
		}
		$daybook_entry_count = Bills::model()->DaybookStatus($id);
		if ($daybook_entry_count > 0) {
			Yii::app()->user->setFlash('error', "Daybook entry added for the bill.Cant edit..");
			$this->redirect(array('bills/admin'));
		}
		$item_model = Billitem::model()->findAll(array("condition" => "bill_id = " . $id . ""));

		$tblpx = Yii::app()->db->tablePrefix;
		$user = Users::model()->findByPk(Yii::app()->user->id);
		$arrVal = explode(',', $user->company_id);
		$newQuery = "";
		$newQuery1 = "";
		$newQuery2 = "";
		$newQuery3 = "";
		foreach ($arrVal as $arr) {
			if ($newQuery)
				$newQuery .= ' OR';
			if ($newQuery1)
				$newQuery1 .= ' OR';
			if ($newQuery2)
				$newQuery2 .= ' OR';
			if ($newQuery3)
				$newQuery3 .= ' OR';
			$newQuery .= " FIND_IN_SET('" . $arr . "', company_id)";
			$newQuery1 .= " FIND_IN_SET('" . $arr . "', p.company_id)";
			$newQuery2 .= " FIND_IN_SET('" . $arr . "', {$tblpx}expense_type.company_id)";
			$newQuery3 .= " FIND_IN_SET('" . $arr . "', v.company_id)";
		}
		if (Yii::app()->user->role == 1 || Yii::app()->user->role == 2) {
			$project = Yii::app()->db->createCommand("SELECT pid, name  FROM {$tblpx}projects WHERE  FIND_IN_SET(" . $model->company_id . ",company_id) ORDER BY name")->queryAll();
		} else {
			$project = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}projects p JOIN {$tblpx}project_assign pa ON p.pid = pa.projectid WHERE  FIND_IN_SET(" . $model->company_id . ",company_id) AND pa.userid = '" . Yii::app()->user->id . "'")->queryAll();
		}
		$vendor = array();
		$expense_head = array();
		if (isset($newmodel->project_id)) {
			$projects = Projects::model()->findByPK($newmodel->project_id);
			if ($projects->auto_update == 0) {
				$expense_head = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}project_exptype LEFT JOIN {$tblpx}expense_type ON {$tblpx}project_exptype.type_id={$tblpx}expense_type.type_id WHERE {$tblpx}project_exptype.project_id=" . $newmodel->project_id . "")->queryAll();
				//AND (" . $newQuery2 . ")
			} else {
				$expense_head = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}expense_type WHERE 1=1 ")->queryAll();
				//AND (" . $newQuery2 . ")
			}
		}

		if (isset($newmodel->expensehead_id)) {
			$vendor = Yii::app()->db->createCommand("SELECT v.vendor_id as vendorid, v.name as vendorname FROM " . $tblpx . "vendors v LEFT JOIN " . $tblpx . "vendor_exptype vet ON v.vendor_id = vet.vendor_id WHERE vet.type_id = " . $newmodel->expensehead_id . " AND (" . $newQuery3 . ")")->queryAll();
		}
		$this->render('bill_update', array(
			'model' => $model,
			'model2' => $model2,
			'item_model' => $item_model,
			'newmodel' => $newmodel,
			'project' => $project,
			'vendor' => $vendor,
			'expense_head' => $expense_head,
			'specification' => $specification,
		));
	}

	public function actionNewlist()
	{
		$model = new Bills('billwithoutpo');
		$model->unsetAttributes();  // clear any default values
		if (isset($_GET['Bills']))
			$model->attributes = $_GET['Bills'];

		/*$this->render('admin',array(
				  'model'=>$model,
			  ));*/

		$this->render('bills_without_po', array(
			'model' => $model,
			'dataProvider' => $model->billwithoutpo(),
		));
	}

	public function actionBillview($id)
	{
		$model = Bills::model()->find(array("condition" => "bill_id = " . $id . ""));
		$newmodel = Purchase::model()->find(array("condition" => "p_id = " . $model->purchase_id . ""));
		if (empty($model)) {
			$this->redirect(array('bills/newlist'));
		}
		$item_model = $this->setBillItems($id);

		$project = Projects::model()->findByPK($newmodel->project_id);
		$vendor = Vendors::model()->findByPK($newmodel->vendor_id);
		$expense_head = ExpenseType::model()->findByPK($newmodel->expensehead_id);
		$tblpx = Yii::app()->db->tablePrefix;
		if (isset($_GET['exportpdf'])) {
			$this->logo = $this->realpath_logo;
			$mPDF1 = Yii::app()->ePdf->mPDF();
			$mPDF1 = Yii::app()->ePdf->mPDF('', 'A4');
			$sql = 'SELECT template_name '
				. 'FROM jp_quotation_template '
				. 'WHERE status="1"';
			$selectedtemplate = Yii::app()->db->createCommand($sql)->queryScalar();

			$template = $this->getTemplate($selectedtemplate);
			$mPDF1->SetHTMLHeader($template['header']);
			$mPDF1->SetHTMLFooter($template['footer']);
			$mPDF1->AddPage('', '', '', '', '', 0, 0, 20, 30, 10, 0);
			$mPDF1->WriteHTML($this->renderPartial('bill_view', array(
				'model' => $model,
				'item_model' => $item_model,
				'project' => $project,
				'vendor' => $vendor,
				'expense_head' => $expense_head,
			), true));
			$mPDF1->Output('Bills.pdf', 'D');
		} else {
			$this->render('bill_view', array(
				'model' => $model,
				'item_model' => $item_model,
				'project' => $project,
				'vendor' => $vendor,
				'expense_head' => $expense_head,
			));
		}
	}

	public function actionPdfbills($bill_id)
	{
		$model = Bills::model()->find(array("condition" => "bill_id='$bill_id'"));
		$newmodel = Purchase::model()->find(array("condition" => "p_id = " . $model->purchase_id . ""));
		if (empty($model)) {
			$this->redirect(array('bills/newlist'));
		}
		$project = Projects::model()->findByPK($newmodel->project_id);
		$vendor = Vendors::model()->findByPK($newmodel->vendor_id);
		$expense_head = ExpenseType::model()->findByPK($newmodel->expensehead_id);
		$item_model = Billitem::model()->findAll(array("condition" => "bill_id = '$bill_id'"));
		$mPDF1 = Yii::app()->ePdf->mPDF();

		$mPDF1 = Yii::app()->ePdf->mPDF('', 'A4');

		$mPDF1->WriteHTML($this->renderPartial('pdfbills', array(
			'model' => $model,
			'item_model' => $item_model,
			'newmodel' => $newmodel,
			'project' => $project,
			'vendor' => $vendor,
			'expense_head' => $expense_head,
		), true));

		$mPDF1->Output('purchase_bills.pdf', 'D');
	}

	public function actionExportbils($bill_id)
	{
		$model = Bills::model()->find(array("condition" => "bill_id='$bill_id'"));
		$newmodel = Purchase::model()->find(array("condition" => "p_id = " . $model->purchase_id . ""));
		$item_model = Billitem::model()->findAll(array("condition" => "bill_id = '$bill_id'"));
		$project = Projects::model()->findByPK($newmodel->project_id);
		$vendor = Vendors::model()->findByPK($newmodel->vendor_id);
		$expense_head = ExpenseType::model()->findByPK($newmodel->expensehead_id);
		$company = Company::model()->findByPk($model->company_id);
		$arraylabel = array('Company', 'Project', 'Expense Head', 'Vendor', 'Date', 'Bill No');
		$finaldata = array();
		if (isset($_POST['purchase_id'])) {
			$purchase_id = $_POST['purchase_id'];
		} else {
			$purchase_id = '';
		}

		if (isset($_POST['bill_id'])) {
			$bill_id = $_POST['bill_id'];
		} else {
			$bill_id = '';
		}

		$additional_amount = 0;
		$tblpx = Yii::app()->db->tablePrefix;
		$purchase = Yii::app()->db->createCommand("SELECT  purchase_no FROM `{$tblpx}purchase` 
			where p_id =" . $purchase_id)->queryRow();
		if ($bill_id != '') {
			$addcharges = Yii::app()->db->createCommand("SELECT ROUND(SUM(amount), 2) as amount FROM " . $tblpx . "additional_bill WHERE `bill_id`=" . $bill_id)->queryRow();
			$bill_items = $this->setBillItems($bill_id);

			$additional_amount = $addcharges['amount'];
		}
		$addcharges = Yii::app()->db->createCommand("SELECT SUM(amount) as amount FROM " . $tblpx . "additional_bill WHERE `bill_id`=" . $model->bill_id)->queryRow();
		if (!empty($addcharges)) {
			$additional_amt = $addcharges['amount'];
		}
		$finaldata[0][] = $company['name'];
		$finaldata[0][] = $project->name;
		$finaldata[0][] = (isset($expense_head->type_name)) ? $expense_head->type_name : '';
		$finaldata[0][] = (isset($vendor->name)) ? $vendor->name : '';
		$finaldata[0][] = date('d-m-Y', strtotime($model->bill_date));
		$finaldata[0][] = $model->bill_number;

		$finaldata[1][] = ' ';
		$finaldata[1][] = ' ';
		$finaldata[1][] = ' ';
		$finaldata[1][] = ' ';
		$finaldata[1][] = ' ';
		$finaldata[1][] = ' ';


		$finaldata[2][] = ' ';
		$finaldata[2][] = ' ';
		$finaldata[2][] = ' ';
		$finaldata[2][] = ' ';
		$finaldata[2][] = ' ';
		$finaldata[2][] = ' ';


		$finaldata[3][] = ' ';
		$finaldata[3][] = ' ';
		$finaldata[3][] = ' ';
		$finaldata[3][] = ' ';
		$finaldata[3][] = ' ';
		$finaldata[3][] = ' ';


		$finaldata[4][] = 'Sl.No';
		$finaldata[4][] = 'Description';
		$finaldata[4][] = 'HSN Code';
		$finaldata[4][] = 'Quantity';
		$finaldata[4][] = 'Unit';
		$finaldata[4][] = 'Rate';
		$finaldata[4][] = 'Tax Slab (%)';
		$finaldata[4][] = 'SGST Amount';
		$finaldata[4][] = 'SGST (%)';
		$finaldata[4][] = 'CGST Amount';
		$finaldata[4][] = 'CGST (%)';
		$finaldata[4][] = 'IGST Amount';
		$finaldata[4][] = 'IGST (%)';
		$finaldata[4][] = 'Discount (%)';
		$finaldata[4][] = 'Discount Amount';
		$finaldata[4][] = 'Amount';
		$finaldata[4][] = 'Tax Amount';

		$tblpx = Yii::app()->db->tablePrefix;
		$data1 = Yii::app()->db->createCommand("SELECT SUM(billitem_amount) as bill_amount FROM {$tblpx}billitem WHERE bill_id=" . $model->bill_id . "")->queryRow();
		$data2 = Yii::app()->db->createCommand("SELECT SUM(billitem_taxamount) as bill_taxamount FROM {$tblpx}billitem WHERE bill_id=" . $model->bill_id . "")->queryRow();
		$data3 = Yii::app()->db->createCommand("SELECT SUM(billitem_discountamount) as bill_discountamount FROM {$tblpx}billitem WHERE bill_id=" . $model->bill_id . "")->queryRow();
		$grand_total = ($data1['bill_amount'] + $data2['bill_taxamount']) - $data3['bill_discountamount'];
		if (!empty($bill_items)) {

			$items = $bill_items['add_items'];
			// $additonal_items = $bill_items['add_items'];
			$k = 4;

			foreach ($items as $bill_item) {

				if (isset($bill_item['category_title'])) {
					$finaldata[$k + 1][] = $bill_item['category_title'];
					$finaldata[$k + 1][] = '';
					$finaldata[$k + 1][] = '';
					$finaldata[$k + 1][] = '';
					$finaldata[$k + 1][] = '';
					$finaldata[$k + 1][] = '';
					$finaldata[$k + 1][] = '';
					$finaldata[$k + 1][] = '';
					$finaldata[$k + 1][] = '';
					$finaldata[$k + 1][] = '';
					$finaldata[$k + 1][] = '';
					$finaldata[$k + 1][] = '';
					$finaldata[$k + 1][] = '';
					$finaldata[$k + 1][] = '';
					$finaldata[$k + 1][] = '';
					$finaldata[$k + 1][] = '';
					$finaldata[$k + 1][] = '';
				}
				$i = $k - 1;
				foreach ($bill_item as $data) {

					if (isset($data['specification'])) {
						$finaldata[$i + 3][] = '1';
						$finaldata[$i + 3][] = $data['specification'];
						$finaldata[$i + 3][] = $data['hsn_code'];
						$finaldata[$i + 3][] = $data['quantity'];
						$finaldata[$i + 3][] = $data['unit'];
						$finaldata[$i + 3][] = Yii::app()->Controller->money_format_inr($data['rate'], 2, 1);
						$finaldata[$i + 3][] = Yii::app()->Controller->money_format_inr($data['tax_slab'], 2, 1);
						$finaldata[$i + 3][] = Yii::app()->Controller->money_format_inr($data['sgst'], 2, 1);
						$finaldata[$i + 3][] = $data['sgstp'];
						$finaldata[$i + 3][] = Yii::app()->Controller->money_format_inr($data['cgst'], 2, 1);
						$finaldata[$i + 3][] = $data['cgstp'];
						$finaldata[$i + 3][] = Yii::app()->Controller->money_format_inr($data['igst'], 2, 1);
						$finaldata[$i + 3][] = $data['igstp'];
						$finaldata[$i + 3][] = $data['discp'];
						$finaldata[$i + 3][] = Yii::app()->Controller->money_format_inr($data['disc'], 2, 1);
						$finaldata[$i + 3][] = Yii::app()->Controller->money_format_inr($data['amount'], 2, 1);
						$finaldata[$i + 3][] = Yii::app()->Controller->money_format_inr($data['tot_tax'], 2, 1);
						$i++;
						$x = $i + 3;
					}
				}

				$k += count($bill_item);
			}
		} else {
			$x = 4;
		}

		$finaldata[$x][] = ' ';
		$finaldata[$x][] = ' ';
		$finaldata[$x][] = ' ';
		$finaldata[$x][] = ' ';
		$finaldata[$x][] = ' ';
		$finaldata[$x][] = ' ';
		$finaldata[$x][] = ' ';
		$finaldata[$x][] = ' ';
		$finaldata[$x][] = ' ';
		$finaldata[$x][] = ' ';
		$finaldata[$x][] = ' ';
		$finaldata[$x][] = ' ';
		$finaldata[$x][] = ' ';
		$finaldata[$x][] = ' ';
		$finaldata[$x][] = ' ';
		$finaldata[$x][] = ' ';
		$finaldata[$x][] = ' ';

		$finaldata[$x + 1][] = ' ';
		$finaldata[$x + 1][] = ' ';
		$finaldata[$x + 1][] = ' ';
		$finaldata[$x + 1][] = ' ';
		$finaldata[$x + 1][] = ' ';
		$finaldata[$x + 1][] = ' ';
		$finaldata[$x + 1][] = ' ';
		$finaldata[$x + 1][] = ' ';
		$finaldata[$x + 1][] = ' ';
		$finaldata[$x + 1][] = ' ';
		$finaldata[$x + 1][] = ' ';
		$finaldata[$x + 1][] = ' ';
		$finaldata[$x + 1][] = ' ';
		$finaldata[$x + 1][] = 'Total';
		$finaldata[$x + 1][] = ($data3['bill_discountamount'] != '') ? number_format($data3['bill_discountamount'], 2) : '0.00';
		$finaldata[$x + 1][] = ($data1['bill_amount'] != '') ? number_format($data1['bill_amount'], 2) : '0.00';
		$finaldata[$x + 1][] = ($data2['bill_taxamount'] != '') ? number_format($data2['bill_taxamount'], 2) : '0.00';

		$finaldata[$x + 2][] = '';
		$finaldata[$x + 2][] = '';
		$finaldata[$x + 2][] = '';
		$finaldata[$x + 2][] = '';
		$finaldata[$x + 2][] = '';
		$finaldata[$x + 2][] = '';
		$finaldata[$x + 2][] = '';
		$finaldata[$x + 2][] = '';
		$finaldata[$x + 2][] = '';
		$finaldata[$x + 2][] = '';
		$finaldata[$x + 2][] = ' ';
		$finaldata[$x + 2][] = '';
		$finaldata[$x + 2][] = 'Grand Total';
		$finaldata[$x + 2][] = ($grand_total != '') ? $grand_total : '0.00';
		$finaldata[$x + 2][] = '';
		$finaldata[$x + 2][] = '';


		$addchargesall = Yii::app()->db->createCommand("SELECT * FROM " . $tblpx . "additional_bill WHERE `bill_id`=" . $bill_id)->queryAll();
		if (!empty($addchargesall)) {

			$finaldata[$x + 4][] = 'Additional Charges';
			$p = $x + 5;
			foreach ($addchargesall as $data) {
				$finaldata[$p][] = $data['category'] . ' - ' . $data['amount'];
				$k = $p;
				$p++;
			}
		} else {
			$k = $x + 1;
		}

		$finaldata[$k + 0][] = 'Discount';
		$finaldata[$k + 0][] = $data3['bill_discountamount'];
		$finaldata[$k + 1][] = 'Tax';
		$finaldata[$k + 1][] = round($data2['bill_taxamount']);

		$finaldata[$k + 2][] = 'Additional Charge';
		$finaldata[$k + 2][] = $additional_amt;

		$finaldata[$k + 3][] = 'Amount';
		$finaldata[$k + 3][] = $data1['bill_amount'];
		// $finaldata[$k + 3][] = 'Grand Total';
// 		$finaldata[$k + 3][] = (($data1['bill_amount']+round($data2['bill_taxamount']))+$additional_amt);

		$finaldata[$k + 4][] = 'Grand Total1';
		$finaldata[$k + 4][] = ($data1['bill_amount'] + $additional_amt) - $data3['bill_discountamount'];



		Yii::import('ext.ECSVExport');

		$csv = new ECSVExport($finaldata);
		$csv->setHeaders($arraylabel);
		$csv->setToAppend();
		$contentdata = $csv->toCSV();
		$csvfilename = 'Bills' . date('d_M_Y') . '.csv';
		Yii::app()->getRequest()->sendFile($csvfilename, $contentdata, "text/csv", false);
		exit();
	}

	public function actionTaxreportcsv($date_from, $date_to, $company_id)
	{
		$tblpx = Yii::app()->db->tablePrefix;
		$date_from = date('Y-m-d', strtotime($date_from));
		$date_to = date('Y-m-d', strtotime($date_to));
		$user = Users::model()->findByPk(Yii::app()->user->id);
		$arrVal = explode(',', $user->company_id);
		$newQuery = "";
		$newQuery1 = "";
		$newQuery2 = "";
		$newQuery3 = "";
		$newQuery4 = "";
		$newQuery5 = "";

		if ($company_id == NULL) {
			foreach ($arrVal as $arr) {
				if ($newQuery)
					$newQuery .= ' OR';
				if ($newQuery1)
					$newQuery1 .= ' OR';
				if ($newQuery2)
					$newQuery2 .= ' OR';
				if ($newQuery3)
					$newQuery3 .= ' OR';
				if ($newQuery4)
					$newQuery4 .= ' OR';
				if ($newQuery5)
					$newQuery5 .= ' OR';
				$newQuery .= " FIND_IN_SET('" . $arr . "', {$tblpx}bills.company_id)";
				$newQuery1 .= " FIND_IN_SET('" . $arr . "', {$tblpx}expenses.company_id)";
				$newQuery2 .= " FIND_IN_SET('" . $arr . "', {$tblpx}dailyexpense.company_id)";
				$newQuery3 .= " FIND_IN_SET('" . $arr . "', {$tblpx}dailyvendors.company_id)";
				$newQuery4 .= " FIND_IN_SET('" . $arr . "', {$tblpx}invoice.company_id)";
				$newQuery5 .= " FIND_IN_SET('" . $arr . "', {$tblpx}purchase_return.company_id)";
			}
		} else {
			$newQuery .= " FIND_IN_SET('" . $company_id . "', {$tblpx}bills.company_id)";
			$newQuery1 .= " FIND_IN_SET('" . $company_id . "', {$tblpx}expenses.company_id)";
			$newQuery2 .= " FIND_IN_SET('" . $company_id . "', {$tblpx}dailyexpense.company_id)";
			$newQuery3 .= " FIND_IN_SET('" . $company_id . "', {$tblpx}dailyvendors.company_id)";
			$newQuery4 .= " FIND_IN_SET('" . $company_id . "', {$tblpx}invoice.company_id)";
			$newQuery5 .= " FIND_IN_SET('" . $company_id . "', {$tblpx}purchase_return.company_id)";
		}

		if ($company_id != NULL) {
			$company = Company::model()->findbyPk($company_id);
			$company_name = $company->name;
		} else {
			$company_name = "";
		}

		// purchase  section
		$purchase = array();
		$purchase_head = array();
		$purchase_head_igst = array();
		$a = 0;
		$b = 0;
		$c = 0;
		$d = 0;
		$e = 0;
		$f = 0;
		$g = 0;
		$h = 0;
		$i = 0;
		$j = 0;
		// bills
		$data = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}bills LEFT JOIN {$tblpx}purchase ON {$tblpx}bills.purchase_id= {$tblpx}purchase.p_id LEFT JOIN {$tblpx}vendors ON {$tblpx}purchase.vendor_id={$tblpx}vendors.vendor_id WHERE (" . $newQuery . ") AND {$tblpx}bills.bill_date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND {$tblpx}bills.bill_taxamount > 0 ORDER BY {$tblpx}bills.bill_date desc")->queryAll();
		foreach ($data as $key => $value) {
			$purchase[$key]['bill_number'] = $value['bill_number'];
			$purchase[$key]['date'] = $value['bill_date'];
			$purchase[$key]['vendor_name'] = $value['name'];
			$purchase[$key]['gst_no'] = $value['gst_no'];
			$purchase[$key]['totalamount'] = $value['bill_totalamount'];
			$purchase[$key]['id'] = $value['bill_id'];
			$purchase[$key]['type'] = 'bills';
		}

		$data1 = Yii::app()->db->createCommand("SELECT DISTINCT billitem_cgstpercent,billitem_igstpercent,billitem_sgstpercent FROM {$tblpx}billitem  LEFT JOIN {$tblpx}bills ON {$tblpx}bills.bill_id ={$tblpx}billitem.bill_id WHERE {$tblpx}bills.bill_date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND billitem_cgstpercent > 0 AND billitem_sgstpercent > 0 AND (" . $newQuery . ") GROUP BY billitem_cgstpercent, billitem_sgstpercent")->queryAll();
		foreach ($data1 as $key => $value) {
			$purchase_head[$key]['cgstpercent'] = $value['billitem_cgstpercent'];
			$purchase_head[$key]['sgstpercent'] = $value['billitem_sgstpercent'];
			//$purchase_head[$key]['igstpercent'] = $value['billitem_igstpercent'];
		}

		$data5 = Yii::app()->db->createCommand("SELECT DISTINCT billitem_igstpercent FROM {$tblpx}billitem  LEFT JOIN {$tblpx}bills ON {$tblpx}bills.bill_id ={$tblpx}billitem.bill_id WHERE {$tblpx}bills.bill_date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND billitem_igstpercent > 0 AND (" . $newQuery . ") GROUP BY billitem_igstpercent")->queryAll();
		foreach ($data5 as $key => $value) {
			$purchase_head_igst[$key]['igstpercent'] = $value['billitem_igstpercent'];
		}

		// day book

		$a = count($data);
		$data6 = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}expenses LEFT JOIN {$tblpx}projects ON {$tblpx}expenses.projectid={$tblpx}projects.pid LEFT JOIN {$tblpx}expense_type ON {$tblpx}expenses.exptype = {$tblpx}expense_type.type_id LEFT JOIN {$tblpx}vendors ON {$tblpx}expenses.vendor_id= {$tblpx}vendors.vendor_id WHERE {$tblpx}expenses.date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND (" . $newQuery1 . ") AND {$tblpx}expenses.bill_id IS NULL AND {$tblpx}expenses.invoice_id IS NULL AND {$tblpx}expenses.type=73 AND ((expense_sgstp > 0 AND expense_cgstp > 0) OR (expense_igstp >0)) ORDER BY {$tblpx}expenses.date desc")->queryAll();
		foreach ($data6 as $key => $value) {
			$key = count(($data1)) + 1;
			$purchase[$a]['bill_number'] = '';
			$purchase[$a]['date'] = $value['date'];
			$purchase[$a]['vendor_name'] = $value['name'];
			$purchase[$a]['gst_no'] = $value['gst_no'];
			$purchase[$a]['totalamount'] = $value['amount'];
			$purchase[$a]['id'] = $value['exp_id'];
			$purchase[$a]['type'] = 'daybook';
			$a++;
		}

		$b = count($data1);
		$data7 = Yii::app()->db->createCommand("SELECT DISTINCT expense_sgstp, expense_cgstp, expense_igstp FROM {$tblpx}expenses WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND (expense_sgstp > 0 AND expense_cgstp > 0) AND {$tblpx}expenses.bill_id IS NULL AND {$tblpx}expenses.invoice_id IS NULL AND {$tblpx}expenses.type=73 AND (" . $newQuery1 . ") GROUP BY expense_sgstp,expense_cgstp")->queryAll();
		foreach ($data7 as $key => $value) {
			$purchase_head[$b]['cgstpercent'] = $value['expense_cgstp'];
			$purchase_head[$b]['sgstpercent'] = $value['expense_sgstp'];
			//$purchase_head[$b]['igstpercent'] = $value['expense_igstp'];
			$b++;
		}

		$c = count($data5);
		;
		$data8 = Yii::app()->db->createCommand("SELECT DISTINCT expense_igstp FROM {$tblpx}expenses WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND (expense_igstp >0) AND {$tblpx}expenses.bill_id IS NULL AND {$tblpx}expenses.invoice_id IS NULL AND {$tblpx}expenses.type=73 AND (" . $newQuery1 . ") GROUP BY expense_igstp")->queryAll();
		foreach ($data8 as $key => $value) {
			$purchase_head_igst[$c]['igstpercent'] = $value['expense_igstp'];
			$c++;
		}

		// daily expense


		$d = count($data) + count($data6);
		$data9 = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}dailyexpense WHERE {$tblpx}dailyexpense.date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND (" . $newQuery2 . ") AND {$tblpx}dailyexpense.exp_type=73 AND ((dailyexpense_sgstp > 0 AND dailyexpense_cgstp > 0) OR (dailyexpense_igstp > 0)) ORDER BY {$tblpx}dailyexpense.date desc")->queryAll();
		foreach ($data9 as $key => $value) {
			$key = count(($data1)) + 1;
			$purchase[$d]['bill_number'] = '';
			$purchase[$d]['date'] = $value['date'];
			$purchase[$d]['vendor_name'] = '';
			$purchase[$d]['gst_no'] = '';
			$purchase[$d]['totalamount'] = $value['amount'];
			$purchase[$d]['id'] = $value['dailyexp_id'];
			$purchase[$d]['type'] = 'dailyexpense';
			$d++;
		}

		$e = count($data1) + count($data7) + 1;
		$data10 = Yii::app()->db->createCommand("SELECT DISTINCT dailyexpense_sgstp, dailyexpense_cgstp, dailyexpense_igstp FROM {$tblpx}dailyexpense WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND (dailyexpense_sgstp > 0 AND dailyexpense_cgstp > 0) AND {$tblpx}dailyexpense.exp_type=73 AND (" . $newQuery2 . ") GROUP BY dailyexpense_sgstp, dailyexpense_cgstp")->queryAll();
		foreach ($data10 as $key => $value) {
			$purchase_head[$e]['cgstpercent'] = $value['dailyexpense_cgstp'];
			$purchase_head[$e]['sgstpercent'] = $value['dailyexpense_sgstp'];
			$e++;
		}


		$f = count($data5) + count($data8) + 1;
		;
		$data11 = Yii::app()->db->createCommand("SELECT DISTINCT dailyexpense_igstp FROM {$tblpx}dailyexpense WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND (dailyexpense_igstp >0) AND {$tblpx}dailyexpense.exp_type=73 AND (" . $newQuery2 . ") GROUP BY dailyexpense_igstp")->queryAll();
		foreach ($data11 as $key => $value) {
			$purchase_head_igst[$c]['igstpercent'] = $value['dailyexpense_igstp'];
			$c++;
		}


		// vendor payment

		$g = count($data) + count($data6) + count($data9) + 1;
		$data12 = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}dailyvendors LEFT JOIN {$tblpx}vendors ON {$tblpx}dailyvendors.vendor_id = {$tblpx}vendors.vendor_id  WHERE {$tblpx}dailyvendors.date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND (" . $newQuery3 . ") AND {$tblpx}dailyvendors.tax_amount > 0 ORDER BY {$tblpx}dailyvendors.date desc")->queryAll();
		foreach ($data12 as $key => $value) {
			$key = count(($data1)) + 1;
			$purchase[$g]['bill_number'] = '';
			$purchase[$g]['date'] = $value['date'];
			$purchase[$g]['vendor_name'] = $value['name'];
			$purchase[$g]['gst_no'] = $value['gst_no'];
			$purchase[$g]['totalamount'] = $value['amount'];
			$purchase[$g]['id'] = $value['daily_v_id'];
			$purchase[$g]['type'] = 'vendorpayment';
			$g++;
		}

		$h = count($data1) + count($data7) + count($data10) + 1;
		$data13 = Yii::app()->db->createCommand("SELECT DISTINCT sgst, cgst, igst FROM {$tblpx}dailyvendors WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND {$tblpx}dailyvendors.tax_amount >0 AND (" . $newQuery3 . ") AND sgst > 0 AND cgst > 0 GROUP BY sgst, cgst")->queryAll();
		foreach ($data13 as $key => $value) {
			$purchase_head[$h]['cgstpercent'] = $value['cgst'];
			$purchase_head[$h]['sgstpercent'] = $value['sgst'];
			$h++;
		}

		$j = count($data5) + count($data8) + count($data11) + 1;
		;
		$data14 = Yii::app()->db->createCommand("SELECT DISTINCT igst FROM {$tblpx}dailyvendors WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND {$tblpx}dailyvendors.tax_amount > 0 AND (" . $newQuery3 . ") AND igst > 0 GROUP BY igst")->queryAll();
		foreach ($data14 as $key => $value) {
			$purchase_head_igst[$j]['igstpercent'] = $value['igst'];
			$j++;
		}
		// invoice

		//				$data2     = Yii::app()->db->createCommand("SELECT {$tblpx}invoice.*, {$tblpx}projects.name as project_name,{$tblpx}clients.name as client,{$tblpx}clients.gst_no  FROM {$tblpx}invoice LEFT JOIN {$tblpx}projects ON {$tblpx}invoice.project_id = {$tblpx}projects.pid LEFT JOIN {$tblpx}clients ON {$tblpx}invoice.client_id = {$tblpx}clients.cid WHERE {$tblpx}invoice.company_id=".Yii::app()->user->company_id." AND {$tblpx}invoice.date BETWEEN '".$date_from."' AND '".$date_to."' ORDER BY {$tblpx}invoice.date desc");
		//				$sales     = $data2->queryAll();
		//
		//				$data3     = Yii::app()->db->createCommand("SELECT DISTINCT sgst,cgst,igst FROM {$tblpx}inv_list LEFT JOIN {$tblpx}invoice ON {$tblpx}invoice.invoice_id ={$tblpx}inv_list.inv_id WHERE {$tblpx}invoice.date BETWEEN '".$date_from."' AND '".$date_to."' GROUP BY sgst,cgst");
		//				$sales_head = $data3->queryAll();
		//
		//				$data4 = Yii::app()->db->createCommand("SELECT DISTINCT igst FROM {$tblpx}inv_list LEFT JOIN {$tblpx}invoice ON {$tblpx}invoice.invoice_id ={$tblpx}inv_list.inv_id WHERE {$tblpx}invoice.date BETWEEN '".$date_from."' AND '".$date_to."'  GROUP BY igst");
		//				$igst_head_sales = $data4->queryAll();


		$invoice_head = array();
		$invoice_head_igst = array();
		$sales = array();

		$p = 0;
		$q = 0;
		$r = 0;
		$s = 0;
		$t = 0;


		$data2 = Yii::app()->db->createCommand("SELECT {$tblpx}invoice.*, {$tblpx}projects.name as project_name,{$tblpx}clients.name as client,{$tblpx}clients.gst_no  FROM {$tblpx}invoice LEFT JOIN {$tblpx}projects ON {$tblpx}invoice.project_id = {$tblpx}projects.pid LEFT JOIN {$tblpx}clients ON {$tblpx}invoice.client_id = {$tblpx}clients.cid WHERE (" . $newQuery4 . ") AND {$tblpx}invoice.date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND {$tblpx}invoice.invoice_status ='saved' ORDER BY {$tblpx}invoice.date desc");
		$sales_data = $data2->queryAll();

		$data15 = Yii::app()->db->createCommand("SELECT {$tblpx}purchase_return.*,{$tblpx}clients.* FROM {$tblpx}purchase_return LEFT JOIN {$tblpx}bills ON {$tblpx}purchase_return.bill_id = {$tblpx}bills.bill_id LEFT JOIN {$tblpx}purchase ON {$tblpx}bills.purchase_id = {$tblpx}purchase.p_id LEFT JOIN {$tblpx}projects ON {$tblpx}purchase.project_id = {$tblpx}projects.pid LEFT JOIN {$tblpx}clients ON {$tblpx}projects.client_id = {$tblpx}clients.cid WHERE (" . $newQuery5 . ") AND {$tblpx}purchase_return.return_date BETWEEN '" . $date_from . "' AND '" . $date_to . "' ORDER BY {$tblpx}purchase_return.return_date desc ");
		$purchase_returndata = $data15->queryAll();

		$data3 = Yii::app()->db->createCommand("SELECT DISTINCT sgst,cgst,igst FROM {$tblpx}inv_list LEFT JOIN {$tblpx}invoice ON {$tblpx}invoice.invoice_id ={$tblpx}inv_list.inv_id WHERE {$tblpx}invoice.date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND sgst >0 AND cgst > 0 AND {$tblpx}invoice.invoice_status ='saved' AND (" . $newQuery4 . ") GROUP BY sgst,cgst");
		$sales_head = $data3->queryAll();

		$data4 = Yii::app()->db->createCommand("SELECT DISTINCT igst FROM {$tblpx}inv_list LEFT JOIN {$tblpx}invoice ON {$tblpx}invoice.invoice_id ={$tblpx}inv_list.inv_id WHERE {$tblpx}invoice.date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND igst >0 AND {$tblpx}invoice.invoice_status ='saved' AND (" . $newQuery4 . ")  GROUP BY igst");
		$igst_head_sales = $data4->queryAll();

		$data16 = Yii::app()->db->createCommand("SELECT DISTINCT returnitem_cgstpercent,returnitem_sgstpercent,returnitem_igstpercent FROM {$tblpx}purchase_returnitem LEFT JOIN {$tblpx}purchase_return ON {$tblpx}purchase_return.return_id ={$tblpx}purchase_returnitem.return_id WHERE {$tblpx}purchase_return.return_date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND returnitem_cgstpercent > 0 AND returnitem_sgstpercent > 0 AND (" . $newQuery5 . ")  GROUP BY returnitem_cgstpercent,returnitem_sgstpercent");
		$return_head = $data16->queryAll();

		$data17 = Yii::app()->db->createCommand("SELECT DISTINCT returnitem_igstpercent FROM {$tblpx}purchase_returnitem LEFT JOIN {$tblpx}purchase_return ON {$tblpx}purchase_return.return_id ={$tblpx}purchase_returnitem.return_id WHERE {$tblpx}purchase_return.return_date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND returnitem_igstpercent >0 AND (" . $newQuery5 . ")  GROUP BY returnitem_igstpercent");
		$return_headigst = $data17->queryAll();

		foreach ($sales_head as $key => $value) {
			$invoice_head[$key]['cgst'] = $value['cgst'];
			$invoice_head[$key]['sgst'] = $value['sgst'];
		}

		$q = count($sales_head) + 1;
		foreach ($return_head as $key => $value) {
			$invoice_head[$q]['cgst'] = $value['returnitem_cgstpercent'];
			$invoice_head[$q]['sgst'] = $value['returnitem_cgstpercent'];
			$q++;
		}

		foreach ($igst_head_sales as $key => $value) {
			$invoice_head_igst[$key]['igst'] = $value['igst'];
		}

		$r = count($igst_head_sales) + 1;
		foreach ($return_headigst as $key => $value) {
			$invoice_head_igst[$r]['igst'] = $value['returnitem_igstpercent'];
			$r++;
		}

		foreach ($sales_data as $key => $value) {
			$sales[$key]['client'] = $value['client'];
			$sales[$key]['date'] = $value['date'];
			$sales[$key]['inv_no'] = $value['inv_no'];
			$sales[$key]['return_no'] = '';
			$sales[$key]['gst_no'] = $value['gst_no'];
			$sales[$key]['totalamount'] = $value['amount'] + $value['tax_amount'];
			$sales[$key]['type'] = 'Invoice';
			$sales[$key]['id'] = $value['invoice_id'];
		}

		$p = count($sales_data) + 1;
		foreach ($purchase_returndata as $key => $value) {
			$sales[$p]['client'] = $value['name'];
			$sales[$p]['date'] = $value['return_date'];
			$sales[$p]['inv_no'] = '';
			$sales[$p]['return_no'] = $value['return_number'];
			$sales[$p]['gst_no'] = $value['gst_no'];
			$sales[$p]['totalamount'] = $value['return_totalamount'];
			$sales[$p]['type'] = 'Purchase Return';
			$sales[$p]['id'] = $value['return_id'];
			$p++;
		}

		// total amount calculation purchase

		$purchase_totalamount = 0;
		foreach ($purchase as $key => $data) {
			if ($data['type'] == 'bills') {
				$item_amount = Yii::app()->db->createCommand("SELECT sum(billitem_amount) as billitem_amount FROM {$tblpx}billitem WHERE bill_id=" . $data['id'] . " AND ((billitem_cgstpercent > 0 AND billitem_sgstpercent > 0) OR (billitem_igstpercent > 0))")->queryRow();
				$item_discount = Yii::app()->db->createCommand("SELECT sum(billitem_discountamount) as billitem_discountamount FROM {$tblpx}billitem WHERE bill_id=" . $data['id'] . " AND ((billitem_cgstpercent > 0 AND billitem_sgstpercent > 0) OR (billitem_igstpercent > 0))")->queryRow();
				$item_taxamount = Yii::app()->db->createCommand("SELECT sum(billitem_taxamount) as billitem_taxamount FROM {$tblpx}billitem WHERE bill_id=" . $data['id'] . " AND ((billitem_cgstpercent > 0 AND billitem_sgstpercent > 0) OR (billitem_igstpercent > 0))")->queryRow();
				//Controller::money_format_inr($data['bill_totalamount'],2,1)
				$total_amount = ($item_amount['billitem_amount'] - $item_discount['billitem_discountamount']) + $item_taxamount['billitem_taxamount'];
			} else if ($data['type'] == 'daybook') {
				$total_amount = $data['totalamount'];
			} else if ($data['type'] == 'dailyexpense') {
				$total_amount = $data['totalamount'];
			} else if ($data['type'] == 'vendorpayment') {
				$item_taxamount = Yii::app()->db->createCommand("SELECT tax_amount FROM {$tblpx}dailyvendors WHERE 	daily_v_id=" . $data['id'] . "")->queryRow();
				$total_amount = $data['totalamount'] + $item_taxamount['tax_amount'];
			}
			$purchase_totalamount += $total_amount;
		}

		// total amount calculation sales

		$sales_totalamount = 0;
		foreach ($sales as $key => $value) {
			$sales_totalamount += $value['totalamount'];
		}


		$purchase_head_igst = array_map("unserialize", array_unique(array_map("serialize", $purchase_head_igst)));
		$purchase_head = array_map("unserialize", array_unique(array_map("serialize", $purchase_head)));
		$sales_head = array_map("unserialize", array_unique(array_map("serialize", $invoice_head)));
		$igst_head_sales = array_map("unserialize", array_unique(array_map("serialize", $invoice_head_igst)));

		$arraylabel = array('');
		$finaldata = array();

		$pl = 0;
		$tfoot = 0;
		$sale_loop = 0;
		$sales_foot = 0;

		if (!empty($purchase)) {
			$finaldata[0][] = 'Purchase  ' . $company_name . ' - ' . $date_from . ' to ' . $date_to . '';

			$finaldata[1][] = 'Sl No.';
			$finaldata[1][] = 'Payment mode';
			$finaldata[1][] = 'Purchase date';
			$finaldata[1][] = 'Vendor';
			$finaldata[1][] = 'GST No';
			$finaldata[1][] = 'Commodity';
			$finaldata[1][] = 'Commodity Code';
			$finaldata[1][] = 'Bill no';
			$finaldata[1][] = 'Bill amount';
			foreach ($purchase_head as $key_h => $value_h) {
				$finaldata[1][] = 'Tax.val';
				$finaldata[1][] = 'CGST ' . $value_h['cgstpercent'] . ' %';
				$finaldata[1][] = 'SGST ' . $value_h['sgstpercent'] . ' %';
			}
			foreach ($purchase_head_igst as $key_h => $value_h) {
				$finaldata[1][] = 'Tax.val';
				$finaldata[1][] = 'IGST ' . $value_h['igstpercent'] . ' %';
			}
			$finaldata[1][] = 'Other changers';
			$finaldata[1][] = 'Net amount';
			/****************************** */
			$finaldata[2][] = '';
			$finaldata[2][] = '';
			$finaldata[2][] = '';
			$finaldata[2][] = '';
			$finaldata[2][] = '';
			$finaldata[2][] = '';
			$finaldata[2][] = '';
			$finaldata[2][] = 'Total';
			$finaldata[2][] = ($purchase_totalamount != '') ? Controller::money_format_inr($purchase_totalamount, 2, 1) : '0.00';
			$net_amount = 0;
			$net_taxamount = 0;
			$net_sgst = 0;
			$net_cgst = 0;
			foreach ($purchase_head as $key1 => $value1) {
				//if($data['type'] =='bills') {
				$row_cgst1 = Yii::app()->db->createCommand("SELECT SUM(billitem_cgst) as cgst FROM {$tblpx}billitem LEFT JOIN {$tblpx}bills ON {$tblpx}bills.bill_id ={$tblpx}billitem.bill_id WHERE {$tblpx}bills.bill_date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND FORMAT(billitem_cgstpercent,2) = FORMAT(" . $value1['cgstpercent'] . ",2) AND FORMAT(billitem_sgstpercent,2) = FORMAT(" . $value1['sgstpercent'] . ",2) AND billitem_cgstpercent > 0 AND billitem_sgstpercent > 0 AND (" . $newQuery . ")")->queryRow();
				$row_sgst1 = Yii::app()->db->createCommand("SELECT SUM(billitem_sgst) as sgst FROM {$tblpx}billitem  LEFT JOIN {$tblpx}bills ON {$tblpx}bills.bill_id ={$tblpx}billitem.bill_id WHERE {$tblpx}bills.bill_date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND FORMAT(billitem_cgstpercent,2) = FORMAT(" . $value1['cgstpercent'] . ",2) AND FORMAT(billitem_sgstpercent,2) = FORMAT(" . $value1['sgstpercent'] . ",2) AND billitem_cgstpercent > 0 AND billitem_sgstpercent > 0 AND (" . $newQuery . ")")->queryRow();
				$row_tax1 = Yii::app()->db->createCommand("SELECT SUM(billitem_amount) as tax_amount FROM {$tblpx}billitem LEFT JOIN {$tblpx}bills ON {$tblpx}bills.bill_id ={$tblpx}billitem.bill_id WHERE {$tblpx}bills.bill_date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND FORMAT(billitem_cgstpercent,2) = FORMAT(" . $value1['cgstpercent'] . ",2) AND FORMAT(billitem_sgstpercent,2) = FORMAT(" . $value1['sgstpercent'] . ",2) AND billitem_cgstpercent > 0 AND billitem_sgstpercent > 0 AND (" . $newQuery . ")")->queryRow();
				$row_discount = Yii::app()->db->createCommand("SELECT SUM(billitem_discountamount) as discount_amount FROM {$tblpx}billitem  LEFT JOIN {$tblpx}bills ON {$tblpx}bills.bill_id ={$tblpx}billitem.bill_id WHERE {$tblpx}bills.bill_date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND FORMAT(billitem_cgstpercent,2) = FORMAT(" . $value1['cgstpercent'] . ",2) AND FORMAT(billitem_sgstpercent,2) = FORMAT(" . $value1['sgstpercent'] . ",2) AND billitem_cgstpercent > 0 AND billitem_sgstpercent > 0 AND (" . $newQuery . ")")->queryRow();
				$tax_amount1 = ($row_tax1['tax_amount'] - $row_discount['discount_amount']);
				$bill_cgst = $row_cgst1['cgst'];
				$bill_sgst = $row_sgst1['sgst'];

				$row_cgst2 = Yii::app()->db->createCommand("SELECT SUM(expense_cgst) as cgst FROM {$tblpx}expenses WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND FORMAT(expense_cgstp,2) = FORMAT(" . $value1['cgstpercent'] . ",2) AND FORMAT(expense_sgstp,2) = FORMAT(" . $value1['sgstpercent'] . ",2) AND (" . $newQuery1 . ") AND {$tblpx}expenses.bill_id IS NULL AND {$tblpx}expenses.invoice_id IS NULL AND {$tblpx}expenses.type=73 AND (expense_sgstp > 0 AND expense_cgstp > 0)")->queryRow();
				$row_sgst2 = Yii::app()->db->createCommand("SELECT SUM(expense_sgst) as sgst FROM {$tblpx}expenses WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "'  AND FORMAT(expense_cgstp,2) = FORMAT(" . $value1['cgstpercent'] . ",2) AND FORMAT(expense_sgstp,2) = FORMAT(" . $value1['sgstpercent'] . ",2) AND (" . $newQuery1 . ") AND {$tblpx}expenses.bill_id IS NULL AND {$tblpx}expenses.invoice_id IS NULL AND {$tblpx}expenses.type=73 AND (expense_sgstp > 0 AND expense_cgstp > 0)")->queryRow();
				$row_tax2 = Yii::app()->db->createCommand("SELECT SUM(expense_amount) as tax_amount FROM {$tblpx}expenses WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "'  AND FORMAT(expense_cgstp,2) = FORMAT(" . $value1['cgstpercent'] . ",2) AND FORMAT(expense_sgstp,2) = FORMAT(" . $value1['sgstpercent'] . ",2) AND (" . $newQuery1 . ") AND {$tblpx}expenses.bill_id IS NULL AND {$tblpx}expenses.invoice_id IS NULL AND {$tblpx}expenses.type=73")->queryRow();
				$tax_amount2 = $row_tax2['tax_amount'];
				$exp_cgst = $row_cgst2['cgst'];
				$exp_sgst = $row_sgst2['sgst'];

				$row_cgst3 = Yii::app()->db->createCommand("SELECT SUM(dailyexpense_cgst) as cgst FROM {$tblpx}dailyexpense WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND FORMAT(dailyexpense_cgstp,2) = FORMAT(" . $value1['cgstpercent'] . ",2) AND FORMAT(dailyexpense_sgstp,2) = FORMAT(" . $value1['sgstpercent'] . ",2) AND (" . $newQuery2 . ") AND {$tblpx}dailyexpense.exp_type=73 AND (dailyexpense_sgstp > 0 AND dailyexpense_cgstp > 0)")->queryRow();
				$row_sgst3 = Yii::app()->db->createCommand("SELECT SUM(dailyexpense_sgst) as sgst FROM {$tblpx}dailyexpense WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "'  AND FORMAT(dailyexpense_cgstp,2) = FORMAT(" . $value1['cgstpercent'] . ",2) AND FORMAT(dailyexpense_sgstp,2) = FORMAT(" . $value1['sgstpercent'] . ",2) AND (" . $newQuery2 . ") AND {$tblpx}dailyexpense.exp_type=73 AND (dailyexpense_sgstp > 0 AND dailyexpense_cgstp > 0)")->queryRow();
				$row_tax3 = Yii::app()->db->createCommand("SELECT SUM(dailyexpense_amount) as tax_amount FROM {$tblpx}dailyexpense WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "'  AND FORMAT(dailyexpense_cgstp,2) = FORMAT(" . $value1['cgstpercent'] . ",2) AND FORMAT(dailyexpense_sgstp,2) = FORMAT(" . $value1['sgstpercent'] . ",2) AND (" . $newQuery2 . ") AND {$tblpx}dailyexpense.exp_type=73")->queryRow();
				$tax_amount3 = $row_tax3['tax_amount'];
				$daexp_cgst = $row_cgst3['cgst'];
				$daexp_sgst = $row_sgst3['sgst'];

				$row_cgst4 = Yii::app()->db->createCommand("SELECT SUM(cgst_amount) as cgst FROM {$tblpx}dailyvendors WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND FORMAT(cgst,2) = FORMAT(" . $value1['cgstpercent'] . ",2) AND FORMAT(sgst,2) = FORMAT(" . $value1['sgstpercent'] . ",2) AND (" . $newQuery3 . ") AND {$tblpx}dailyvendors.tax_amount >0 AND sgst >0 AND cgst >0")->queryRow();
				$row_sgst4 = Yii::app()->db->createCommand("SELECT SUM(sgst_amount) as sgst FROM {$tblpx}dailyvendors WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "'  AND FORMAT(cgst,2) = FORMAT(" . $value1['cgstpercent'] . ",2) AND FORMAT(sgst,2) = FORMAT(" . $value1['sgstpercent'] . ",2) AND (" . $newQuery3 . ") AND {$tblpx}dailyvendors.tax_amount >0 AND sgst >0 AND cgst >0")->queryRow();
				$row_tax4 = Yii::app()->db->createCommand("SELECT SUM(amount) as tax_amount FROM {$tblpx}dailyvendors WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "'  AND FORMAT(cgst,2) = FORMAT(" . $value1['cgstpercent'] . ",2) AND FORMAT(sgst,2) = FORMAT(" . $value1['sgstpercent'] . ",2) AND (" . $newQuery3 . ") AND sgst >0 AND cgst >0")->queryRow();
				$tax_amount4 = $row_tax4['tax_amount'];
				$vndr_cgst = $row_cgst4['cgst'];
				$vndr_sgst = $row_sgst4['sgst'];

				$net_taxamount += $tax_amount1 + $tax_amount2 + $tax_amount3 + $tax_amount4;
				$net_cgst += $bill_cgst + $exp_cgst + $daexp_cgst + $vndr_cgst;
				$net_sgst += $bill_sgst + $exp_sgst + $daexp_sgst + $vndr_sgst;


				$finaldata[2][] = Controller::money_format_inr($tax_amount1 + $tax_amount2 + $tax_amount3 + $tax_amount4, 2, 1);
				$finaldata[2][] = Controller::money_format_inr($bill_cgst + $exp_cgst + $daexp_cgst + $vndr_cgst, 2, 1);
				$finaldata[2][] = Controller::money_format_inr($bill_sgst + $exp_sgst + $daexp_sgst + $vndr_sgst, 2, 1);
			}

			$net_igst = 0;
			$net_tax = 0;
			foreach ($purchase_head_igst as $key1 => $value1) {
				$row_igst1 = Yii::app()->db->createCommand("SELECT SUM(billitem_igst) as igst FROM {$tblpx}billitem LEFT JOIN {$tblpx}bills ON {$tblpx}bills.bill_id ={$tblpx}billitem.bill_id WHERE {$tblpx}bills.bill_date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND FORMAT(billitem_igstpercent,2) = FORMAT(" . $value1['igstpercent'] . ",2) AND (" . $newQuery . ") AND billitem_igstpercent > 0")->queryRow();
				$row_tax1 = Yii::app()->db->createCommand("SELECT SUM(billitem_amount) as tax_amount FROM {$tblpx}billitem LEFT JOIN {$tblpx}bills ON {$tblpx}bills.bill_id ={$tblpx}billitem.bill_id WHERE {$tblpx}bills.bill_date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND FORMAT(billitem_igstpercent,2) = FORMAT(" . $value1['igstpercent'] . ",2) AND billitem_igstpercent > 0 AND (" . $newQuery . ")")->queryRow();
				$row_discount = Yii::app()->db->createCommand("SELECT SUM(billitem_discountamount) as discount_amount FROM {$tblpx}billitem  LEFT JOIN {$tblpx}bills ON {$tblpx}bills.bill_id ={$tblpx}billitem.bill_id WHERE {$tblpx}bills.bill_date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND FORMAT(billitem_igstpercent,2) = FORMAT(" . $value1['igstpercent'] . ",2) AND billitem_igstpercent > 0 AND (" . $newQuery . ")")->queryRow();
				$tax_amount1 = ($row_tax1['tax_amount'] - $row_discount['discount_amount']);

				$row_igst2 = Yii::app()->db->createCommand("SELECT SUM(expense_igst) as igst FROM {$tblpx}expenses WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND FORMAT(expense_igstp,2) = FORMAT(" . $value1['igstpercent'] . ",2) AND (expense_igstp >0) AND {$tblpx}expenses.bill_id IS NULL AND {$tblpx}expenses.invoice_id IS NULL AND {$tblpx}expenses.type=73 AND (" . $newQuery1 . ")")->queryRow();
				$row_tax2 = Yii::app()->db->createCommand("SELECT SUM(expense_amount) as tax_amount FROM {$tblpx}expenses WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "'  AND FORMAT(expense_igstp,2) = FORMAT(" . $value1['igstpercent'] . ",2) AND (" . $newQuery1 . ") AND {$tblpx}expenses.bill_id IS NULL AND {$tblpx}expenses.invoice_id IS NULL AND {$tblpx}expenses.type=73 AND (expense_igstp >0)")->queryRow();
				$tax_amount2 = $row_tax2['tax_amount'];

				$row_igst3 = Yii::app()->db->createCommand("SELECT SUM(dailyexpense_igst) as igst FROM {$tblpx}dailyexpense WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND FORMAT(dailyexpense_igstp,2) = FORMAT(" . $value1['igstpercent'] . ",2) AND (dailyexpense_igstp >0) AND {$tblpx}dailyexpense.exp_type=73 AND (" . $newQuery2 . ")")->queryRow();
				$row_tax3 = Yii::app()->db->createCommand("SELECT SUM(dailyexpense_amount) as tax_amount FROM {$tblpx}dailyexpense WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "'  AND FORMAT(dailyexpense_igstp,2) = FORMAT(" . $value1['igstpercent'] . ",2) AND (" . $newQuery2 . ") AND {$tblpx}dailyexpense.exp_type=73 AND (dailyexpense_igstp >0)")->queryRow();
				$tax_amount3 = $row_tax3['tax_amount'];


				$row_igst4 = Yii::app()->db->createCommand("SELECT SUM(igst_amount) as igst FROM {$tblpx}dailyvendors WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND FORMAT(igst,2) = FORMAT(" . $value1['igstpercent'] . ",2) AND (" . $newQuery3 . ") AND igst >0")->queryRow();
				$row_tax4 = Yii::app()->db->createCommand("SELECT SUM(amount) as tax_amount FROM {$tblpx}dailyvendors WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "'  AND FORMAT(igst,2) = FORMAT(" . $value1['igstpercent'] . ",2) AND (" . $newQuery3 . ") AND igst >0")->queryRow();
				$tax_amount4 = $row_tax4['tax_amount'];

				$net_tax += $tax_amount1 + $tax_amount2 + $tax_amount3 + $tax_amount4;
				$net_igst += $row_igst2['igst'] + $row_igst1['igst'] + $row_igst3['igst'] + $row_igst4['igst'];

				$finaldata[2][] = Controller::money_format_inr($tax_amount1 + $tax_amount2 + $tax_amount3 + $tax_amount4, 2, 1);
				$finaldata[2][] = Controller::money_format_inr($row_igst2['igst'] + $row_igst1['igst'] + $row_igst3['igst'] + $row_igst4['igst'], 2, 1);
			}
			$finaldata[2][] = '';
			$finaldata[2][] = Controller::money_format_inr(($net_taxamount + $net_cgst + $net_sgst + $net_igst + $net_tax), 2, 1);
			/********************* */
			$pl = 3;
			$tfoot = 3;
			$no = 0;
			foreach ($purchase as $key => $data) {
				$no++;
				$finaldata[$pl][] = $no;
				if ($data['type'] == 'bills') {
					$payment_mode = "Bills";
				} else if ($data['type'] == 'daybook') {
					$payment_mode = "Daybook";
				} else if ($data['type'] == 'dailyexpense') {
					$payment_mode = "Daily Expenses";
				} else if ($data['type'] == 'vendorpayment') {
					$payment_mode = "Vendor Payment";
				}
				$finaldata[$pl][] = $payment_mode;
				$finaldata[$pl][] = date('Y-m-d', strtotime($data['date']));
				$finaldata[$pl][] = $data['vendor_name'];
				$finaldata[$pl][] = $data['gst_no'];
				$finaldata[$pl][] = '';
				$finaldata[$pl][] = '';
				$finaldata[$pl][] = $data['bill_number'];
				$total_amount = 0;
				if ($data['type'] == 'bills') {
					$item_amount = Yii::app()->db->createCommand("SELECT sum(billitem_amount) as billitem_amount FROM {$tblpx}billitem WHERE bill_id=" . $data['id'] . " AND ((billitem_cgstpercent > 0 AND billitem_sgstpercent > 0) OR (billitem_igstpercent > 0))")->queryRow();
					$item_discount = Yii::app()->db->createCommand("SELECT sum(billitem_discountamount) as billitem_discountamount FROM {$tblpx}billitem WHERE bill_id=" . $data['id'] . " AND ((billitem_cgstpercent > 0 AND billitem_sgstpercent > 0) OR (billitem_igstpercent > 0))")->queryRow();
					$item_taxamount = Yii::app()->db->createCommand("SELECT sum(billitem_taxamount) as billitem_taxamount FROM {$tblpx}billitem WHERE bill_id=" . $data['id'] . " AND ((billitem_cgstpercent > 0 AND billitem_sgstpercent > 0) OR (billitem_igstpercent > 0))")->queryRow();
					$total_amount = ($item_amount['billitem_amount'] - $item_discount['billitem_discountamount']) + $item_taxamount['billitem_taxamount'];
				} else if ($data['type'] == 'daybook') {
					$total_amount = $data['totalamount'];
				} else if ($data['type'] == 'dailyexpense') {
					$total_amount = $data['totalamount'];
				} else if ($data['type'] == 'vendorpayment') {
					$item_taxamount = Yii::app()->db->createCommand("SELECT tax_amount FROM {$tblpx}dailyvendors WHERE 	daily_v_id=" . $data['id'] . "")->queryRow();
					$total_amount = $data['totalamount'] + $item_taxamount['tax_amount'];
				}
				$finaldata[$pl][] = Controller::money_format_inr($total_amount, 2, 1);
				$net_amount = 0;
				$net_taxamount = 0;
				$net_sgst = 0;
				$net_cgst = 0;
				$tax_amount = 0;
				$purchase_sgst = 0;
				$purchase_cgst = 0;
				foreach ($purchase_head as $key1 => $value1) {
					if ($data['type'] == 'bills') {
						$row_cgst1 = Yii::app()->db->createCommand("SELECT SUM(billitem_cgst) as cgst FROM {$tblpx}billitem WHERE bill_id=" . $data['id'] . " AND FORMAT(billitem_cgstpercent,2) = FORMAT(" . $value1['cgstpercent'] . ",2) AND FORMAT(billitem_sgstpercent,2) = FORMAT(" . $value1['sgstpercent'] . ",2)")->queryRow();
						$row_sgst1 = Yii::app()->db->createCommand("SELECT SUM(billitem_sgst) as sgst FROM {$tblpx}billitem WHERE bill_id=" . $data['id'] . " AND FORMAT(billitem_cgstpercent,2) = FORMAT(" . $value1['cgstpercent'] . ",2) AND FORMAT(billitem_sgstpercent,2) = FORMAT(" . $value1['sgstpercent'] . ",2)")->queryRow();
						$row_tax1 = Yii::app()->db->createCommand("SELECT SUM(billitem_amount) as tax_amount FROM {$tblpx}billitem WHERE bill_id=" . $data['id'] . " AND FORMAT(billitem_cgstpercent,2) = FORMAT(" . $value1['cgstpercent'] . ",2) AND FORMAT(billitem_sgstpercent,2) = FORMAT(" . $value1['sgstpercent'] . ",2)")->queryRow();
						$row_discount = Yii::app()->db->createCommand("SELECT SUM(billitem_discountamount) as discount_amount FROM {$tblpx}billitem WHERE bill_id=" . $data['id'] . " AND FORMAT(billitem_cgstpercent,2) = FORMAT(" . $value1['cgstpercent'] . ",2) AND FORMAT(billitem_sgstpercent,2) = FORMAT(" . $value1['sgstpercent'] . ",2)")->queryRow();
						$tax_amount = ($row_tax1['tax_amount'] - $row_discount['discount_amount']);
						$purchase_cgst = $row_cgst1['cgst'];
						$purchase_sgst = $row_sgst1['sgst'];
					} else if ($data['type'] == 'daybook') {

						$row_cgst2 = Yii::app()->db->createCommand("SELECT SUM(expense_cgst) as cgst FROM {$tblpx}expenses WHERE exp_id=" . $data['id'] . " AND FORMAT(expense_cgstp,2) = FORMAT(" . $value1['cgstpercent'] . ",2) AND FORMAT(expense_sgstp,2) = FORMAT(" . $value1['sgstpercent'] . ",2) AND (" . $newQuery1 . ") AND {$tblpx}expenses.bill_id IS NULL AND {$tblpx}expenses.invoice_id IS NULL AND {$tblpx}expenses.type=73")->queryRow();
						$row_sgst2 = Yii::app()->db->createCommand("SELECT SUM(expense_sgst) as sgst FROM {$tblpx}expenses WHERE exp_id=" . $data['id'] . " AND FORMAT(expense_cgstp,2) = FORMAT(" . $value1['cgstpercent'] . ",2) AND FORMAT(expense_sgstp,2) = FORMAT(" . $value1['sgstpercent'] . ",2) AND (" . $newQuery1 . ") AND {$tblpx}expenses.bill_id IS NULL AND {$tblpx}expenses.invoice_id IS NULL AND {$tblpx}expenses.type=73")->queryRow();
						$row_taxk = Yii::app()->db->createCommand("SELECT SUM(expense_amount) as tax_amount FROM {$tblpx}expenses WHERE exp_id=" . $data['id'] . " AND FORMAT(expense_cgstp,2) = FORMAT(" . $value1['cgstpercent'] . ",2) AND FORMAT(expense_sgstp,2) = FORMAT(" . $value1['sgstpercent'] . ",2) AND (" . $newQuery1 . ") AND {$tblpx}expenses.bill_id IS NULL AND {$tblpx}expenses.invoice_id IS NULL AND {$tblpx}expenses.type=73")->queryRow();
						$tax_amount = $row_taxk['tax_amount'];
						$purchase_cgst = $row_cgst2['cgst'];
						$purchase_sgst = $row_sgst2['sgst'];
					} else if ($data['type'] == 'dailyexpense') {

						$row_cgst2 = Yii::app()->db->createCommand("SELECT SUM(dailyexpense_cgst) as cgst FROM {$tblpx}dailyexpense WHERE 	dailyexp_id=" . $data['id'] . " AND FORMAT(dailyexpense_cgstp,2) = FORMAT(" . $value1['cgstpercent'] . ",2) AND FORMAT(dailyexpense_sgstp,2) = FORMAT(" . $value1['sgstpercent'] . ",2) AND (" . $newQuery2 . ") AND {$tblpx}dailyexpense.exp_type=73")->queryRow();
						$row_sgst2 = Yii::app()->db->createCommand("SELECT SUM(dailyexpense_sgst) as sgst FROM {$tblpx}dailyexpense WHERE 	dailyexp_id=" . $data['id'] . " AND FORMAT(dailyexpense_cgstp,2) = FORMAT(" . $value1['cgstpercent'] . ",2) AND FORMAT(dailyexpense_sgstp,2) = FORMAT(" . $value1['sgstpercent'] . ",2) AND (" . $newQuery2 . ") AND {$tblpx}dailyexpense.exp_type=73")->queryRow();
						$row_taxk = Yii::app()->db->createCommand("SELECT SUM(dailyexpense_amount) as tax_amount FROM {$tblpx}dailyexpense WHERE 	dailyexp_id=" . $data['id'] . " AND FORMAT(dailyexpense_cgstp,2) = FORMAT(" . $value1['cgstpercent'] . ",2) AND FORMAT(dailyexpense_sgstp,2) = FORMAT(" . $value1['sgstpercent'] . ",2) AND (" . $newQuery2 . ") AND {$tblpx}dailyexpense.exp_type=73")->queryRow();
						$tax_amount = $row_taxk['tax_amount'];
						$purchase_cgst = $row_cgst2['cgst'];
						$purchase_sgst = $row_sgst2['sgst'];
					} else if ($data['type'] == 'vendorpayment') {
						$row_cgst2 = Yii::app()->db->createCommand("SELECT SUM(cgst_amount) as cgst FROM {$tblpx}dailyvendors WHERE daily_v_id=" . $data['id'] . " AND FORMAT(cgst,2) = FORMAT(" . $value1['cgstpercent'] . ",2) AND FORMAT(sgst,2) = FORMAT(" . $value1['sgstpercent'] . ",2) AND (" . $newQuery3 . ")")->queryRow();
						$row_sgst2 = Yii::app()->db->createCommand("SELECT SUM(sgst_amount) as sgst FROM {$tblpx}dailyvendors WHERE daily_v_id=" . $data['id'] . " AND FORMAT(cgst,2) = FORMAT(" . $value1['cgstpercent'] . ",2) AND FORMAT(sgst,2) = FORMAT(" . $value1['sgstpercent'] . ",2) AND (" . $newQuery3 . ")")->queryRow();
						$row_taxk = Yii::app()->db->createCommand("SELECT SUM(amount) as tax_amount FROM {$tblpx}dailyvendors WHERE daily_v_id=" . $data['id'] . " AND FORMAT(cgst,2) = FORMAT(" . $value1['cgstpercent'] . ",2) AND FORMAT(sgst,2) = FORMAT(" . $value1['sgstpercent'] . ",2) AND (" . $newQuery3 . ")")->queryRow();
						$tax_amount = $row_taxk['tax_amount'];
						$purchase_cgst = $row_cgst2['cgst'];
						$purchase_sgst = $row_sgst2['sgst'];
					}

					$net_taxamount = $tax_amount;
					$net_sgst = $purchase_sgst;
					$net_cgst = $purchase_cgst;
					$net_amount += $net_taxamount + $purchase_sgst + $purchase_cgst;

					$finaldata[$pl][] = (($tax_amount == 0) ? '' : Controller::money_format_inr($tax_amount, 2, 1));
					$finaldata[$pl][] = ($purchase_cgst == 0) ? '' : Controller::money_format_inr($purchase_cgst, 2, 1);
					$finaldata[$pl][] = ($purchase_sgst == 0) ? '' : Controller::money_format_inr($purchase_sgst, 2, 1);
				}

				$net_amount_igst = 0;
				$purchase_igst = 0;
				foreach ($purchase_head_igst as $key1 => $value1) {
					if ($data['type'] == 'bills') {
						$row_igst = Yii::app()->db->createCommand("SELECT SUM(billitem_igst) as igst FROM {$tblpx}billitem WHERE bill_id=" . $data['id'] . " AND FORMAT(billitem_igstpercent,2) = FORMAT(" . $value1['igstpercent'] . ",2)")->queryRow();
						$purchase_igst = $row_igst['igst'];
						$row_tax1 = Yii::app()->db->createCommand("SELECT SUM(billitem_amount) as tax_amount FROM {$tblpx}billitem WHERE bill_id=" . $data['id'] . " AND FORMAT(billitem_igstpercent,2) = FORMAT(" . $value1['igstpercent'] . ",2)")->queryRow();
						$row_discount = Yii::app()->db->createCommand("SELECT SUM(billitem_discountamount) as discount_amount FROM {$tblpx}billitem WHERE bill_id=" . $data['id'] . " AND FORMAT(billitem_igstpercent,2) = FORMAT(" . $value1['igstpercent'] . ",2)")->queryRow();
						$tax_amount = ($row_tax1['tax_amount'] - $row_discount['discount_amount']);
					} else if ($data['type'] == 'daybook') {
						$row_igst = Yii::app()->db->createCommand("SELECT SUM(expense_igst) as igst FROM {$tblpx}expenses WHERE exp_id=" . $data['id'] . " AND FORMAT(expense_igstp,2) = FORMAT(" . $value1['igstpercent'] . ",2) AND {$tblpx}expenses.bill_id IS NULL AND {$tblpx}expenses.invoice_id IS NULL AND {$tblpx}expenses.type=73 AND (" . $newQuery1 . ") AND expense_igst > 0")->queryRow();
						$purchase_igst = $row_igst['igst'];
						$row_taxk = Yii::app()->db->createCommand("SELECT SUM(expense_amount) as tax_amount FROM {$tblpx}expenses WHERE exp_id=" . $data['id'] . " AND FORMAT(expense_igstp,2) = FORMAT(" . $value1['igstpercent'] . ",2) AND (" . $newQuery1 . ") AND {$tblpx}expenses.bill_id IS NULL AND {$tblpx}expenses.invoice_id IS NULL AND {$tblpx}expenses.type=73 AND expense_igst > 0")->queryRow();
						$tax_amount = $row_taxk['tax_amount'];
					} else if ($data['type'] == 'dailyexpense') {
						$row_igst = Yii::app()->db->createCommand("SELECT SUM(dailyexpense_igst) as igst FROM {$tblpx}dailyexpense WHERE dailyexp_id=" . $data['id'] . " AND FORMAT(dailyexpense_igstp,2) = FORMAT(" . $value1['igstpercent'] . ",2) AND {$tblpx}dailyexpense.exp_type=73 AND (" . $newQuery2 . ")")->queryRow();
						$purchase_igst = $row_igst['igst'];
						$row_taxk = Yii::app()->db->createCommand("SELECT SUM(dailyexpense_amount) as tax_amount FROM {$tblpx}dailyexpense WHERE 	dailyexp_id=" . $data['id'] . " AND FORMAT(dailyexpense_igstp,2) = FORMAT(" . $value1['igstpercent'] . ",2) AND (" . $newQuery2 . ") AND {$tblpx}dailyexpense.exp_type=73")->queryRow();
						$tax_amount = $row_taxk['tax_amount'];
					} else if ($data['type'] == 'vendorpayment') {
						$row_igst = Yii::app()->db->createCommand("SELECT SUM(igst_amount) as igst FROM {$tblpx}dailyvendors WHERE daily_v_id=" . $data['id'] . " AND FORMAT(igst,2) = FORMAT(" . $value1['igstpercent'] . ",2) AND (" . $newQuery3 . ")")->queryRow();
						$purchase_igst = $row_igst['igst'];
						$row_taxk = Yii::app()->db->createCommand("SELECT SUM(amount) as tax_amount FROM {$tblpx}dailyvendors WHERE daily_v_id=" . $data['id'] . " AND FORMAT(igst,2) = FORMAT(" . $value1['igstpercent'] . ",2) AND (" . $newQuery3 . ")")->queryRow();
						$tax_amount = $row_taxk['tax_amount'];
					}

					$net_amount_igst += $purchase_igst + $tax_amount;

					$finaldata[$pl][] = ($tax_amount == 0) ? '' : Controller::money_format_inr($tax_amount, 2, 1);
					$finaldata[$pl][] = ($purchase_igst == 0) ? '' : Controller::money_format_inr($purchase_igst, 2, 1);
				}

				$finaldata[$pl][] = '';
				$finaldata[$pl][] = Controller::money_format_inr($net_amount + $net_amount_igst, 2);
				$pl++;
				$tfoot = $pl;
			}

			$finaldata[$tfoot][] = '';
			$finaldata[$tfoot][] = '';
			$finaldata[$tfoot][] = '';
			$finaldata[$tfoot][] = '';
			$finaldata[$tfoot][] = '';
			$finaldata[$tfoot][] = '';
			$finaldata[$tfoot][] = '';
			$finaldata[$tfoot][] = '';
			$finaldata[$tfoot][] = ($purchase_totalamount != '') ? Controller::money_format_inr($purchase_totalamount, 2, 1) : '0.00';
			$net_amount = 0;
			$net_taxamount = 0;
			$net_sgst = 0;
			$net_cgst = 0;
			foreach ($purchase_head as $key1 => $value1) {
				//if($data['type'] =='bills') {
				$row_cgst1 = Yii::app()->db->createCommand("SELECT SUM(billitem_cgst) as cgst FROM {$tblpx}billitem LEFT JOIN {$tblpx}bills ON {$tblpx}bills.bill_id ={$tblpx}billitem.bill_id WHERE {$tblpx}bills.bill_date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND FORMAT(billitem_cgstpercent,2) = FORMAT(" . $value1['cgstpercent'] . ",2) AND FORMAT(billitem_sgstpercent,2) = FORMAT(" . $value1['sgstpercent'] . ",2) AND billitem_cgstpercent > 0 AND billitem_sgstpercent > 0 AND (" . $newQuery . ")")->queryRow();
				$row_sgst1 = Yii::app()->db->createCommand("SELECT SUM(billitem_sgst) as sgst FROM {$tblpx}billitem  LEFT JOIN {$tblpx}bills ON {$tblpx}bills.bill_id ={$tblpx}billitem.bill_id WHERE {$tblpx}bills.bill_date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND FORMAT(billitem_cgstpercent,2) = FORMAT(" . $value1['cgstpercent'] . ",2) AND FORMAT(billitem_sgstpercent,2) = FORMAT(" . $value1['sgstpercent'] . ",2) AND billitem_cgstpercent > 0 AND billitem_sgstpercent > 0 AND (" . $newQuery . ")")->queryRow();
				$row_tax1 = Yii::app()->db->createCommand("SELECT SUM(billitem_amount) as tax_amount FROM {$tblpx}billitem LEFT JOIN {$tblpx}bills ON {$tblpx}bills.bill_id ={$tblpx}billitem.bill_id WHERE {$tblpx}bills.bill_date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND FORMAT(billitem_cgstpercent,2) = FORMAT(" . $value1['cgstpercent'] . ",2) AND FORMAT(billitem_sgstpercent,2) = FORMAT(" . $value1['sgstpercent'] . ",2) AND billitem_cgstpercent > 0 AND billitem_sgstpercent > 0 AND (" . $newQuery . ")")->queryRow();
				$row_discount = Yii::app()->db->createCommand("SELECT SUM(billitem_discountamount) as discount_amount FROM {$tblpx}billitem  LEFT JOIN {$tblpx}bills ON {$tblpx}bills.bill_id ={$tblpx}billitem.bill_id WHERE {$tblpx}bills.bill_date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND FORMAT(billitem_cgstpercent,2) = FORMAT(" . $value1['cgstpercent'] . ",2) AND FORMAT(billitem_sgstpercent,2) = FORMAT(" . $value1['sgstpercent'] . ",2) AND billitem_cgstpercent > 0 AND billitem_sgstpercent > 0 AND (" . $newQuery . ")")->queryRow();
				$tax_amount1 = ($row_tax1['tax_amount'] - $row_discount['discount_amount']);
				$bill_cgst = $row_cgst1['cgst'];
				$bill_sgst = $row_sgst1['sgst'];

				$row_cgst2 = Yii::app()->db->createCommand("SELECT SUM(expense_cgst) as cgst FROM {$tblpx}expenses WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND FORMAT(expense_cgstp,2) = FORMAT(" . $value1['cgstpercent'] . ",2) AND FORMAT(expense_sgstp,2) = FORMAT(" . $value1['sgstpercent'] . ",2) AND (" . $newQuery1 . ") AND {$tblpx}expenses.bill_id IS NULL AND {$tblpx}expenses.invoice_id IS NULL AND {$tblpx}expenses.type=73 AND (expense_sgstp > 0 AND expense_cgstp > 0)")->queryRow();
				$row_sgst2 = Yii::app()->db->createCommand("SELECT SUM(expense_sgst) as sgst FROM {$tblpx}expenses WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "'  AND FORMAT(expense_cgstp,2) = FORMAT(" . $value1['cgstpercent'] . ",2) AND FORMAT(expense_sgstp,2) = FORMAT(" . $value1['sgstpercent'] . ",2) AND (" . $newQuery1 . ") AND {$tblpx}expenses.bill_id IS NULL AND {$tblpx}expenses.invoice_id IS NULL AND {$tblpx}expenses.type=73 AND (expense_sgstp > 0 AND expense_cgstp > 0)")->queryRow();
				$row_tax2 = Yii::app()->db->createCommand("SELECT SUM(expense_amount) as tax_amount FROM {$tblpx}expenses WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "'  AND FORMAT(expense_cgstp,2) = FORMAT(" . $value1['cgstpercent'] . ",2) AND FORMAT(expense_sgstp,2) = FORMAT(" . $value1['sgstpercent'] . ",2) AND (" . $newQuery1 . ") AND {$tblpx}expenses.bill_id IS NULL AND {$tblpx}expenses.invoice_id IS NULL AND {$tblpx}expenses.type=73")->queryRow();
				$tax_amount2 = $row_tax2['tax_amount'];
				$exp_cgst = $row_cgst2['cgst'];
				$exp_sgst = $row_sgst2['sgst'];

				$row_cgst3 = Yii::app()->db->createCommand("SELECT SUM(dailyexpense_cgst) as cgst FROM {$tblpx}dailyexpense WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND FORMAT(dailyexpense_cgstp,2) = FORMAT(" . $value1['cgstpercent'] . ",2) AND FORMAT(dailyexpense_sgstp,2) = FORMAT(" . $value1['sgstpercent'] . ",2) AND (" . $newQuery2 . ") AND {$tblpx}dailyexpense.exp_type=73 AND (dailyexpense_sgstp > 0 AND dailyexpense_cgstp > 0)")->queryRow();
				$row_sgst3 = Yii::app()->db->createCommand("SELECT SUM(dailyexpense_sgst) as sgst FROM {$tblpx}dailyexpense WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "'  AND FORMAT(dailyexpense_cgstp,2) = FORMAT(" . $value1['cgstpercent'] . ",2) AND FORMAT(dailyexpense_sgstp,2) = FORMAT(" . $value1['sgstpercent'] . ",2) AND (" . $newQuery2 . ") AND {$tblpx}dailyexpense.exp_type=73 AND (dailyexpense_sgstp > 0 AND dailyexpense_cgstp > 0)")->queryRow();
				$row_tax3 = Yii::app()->db->createCommand("SELECT SUM(dailyexpense_amount) as tax_amount FROM {$tblpx}dailyexpense WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "'  AND FORMAT(dailyexpense_cgstp,2) = FORMAT(" . $value1['cgstpercent'] . ",2) AND FORMAT(dailyexpense_sgstp,2) = FORMAT(" . $value1['sgstpercent'] . ",2) AND (" . $newQuery2 . ") AND {$tblpx}dailyexpense.exp_type=73")->queryRow();
				$tax_amount3 = $row_tax3['tax_amount'];
				$daexp_cgst = $row_cgst3['cgst'];
				$daexp_sgst = $row_sgst3['sgst'];

				$row_cgst4 = Yii::app()->db->createCommand("SELECT SUM(cgst_amount) as cgst FROM {$tblpx}dailyvendors WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND FORMAT(cgst,2) = FORMAT(" . $value1['cgstpercent'] . ",2) AND FORMAT(sgst,2) = FORMAT(" . $value1['sgstpercent'] . ",2) AND (" . $newQuery3 . ") AND {$tblpx}dailyvendors.tax_amount >0 AND sgst >0 AND cgst >0")->queryRow();
				$row_sgst4 = Yii::app()->db->createCommand("SELECT SUM(sgst_amount) as sgst FROM {$tblpx}dailyvendors WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "'  AND FORMAT(cgst,2) = FORMAT(" . $value1['cgstpercent'] . ",2) AND FORMAT(sgst,2) = FORMAT(" . $value1['sgstpercent'] . ",2) AND (" . $newQuery3 . ") AND {$tblpx}dailyvendors.tax_amount >0 AND sgst >0 AND cgst >0")->queryRow();
				$row_tax4 = Yii::app()->db->createCommand("SELECT SUM(amount) as tax_amount FROM {$tblpx}dailyvendors WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "'  AND FORMAT(cgst,2) = FORMAT(" . $value1['cgstpercent'] . ",2) AND FORMAT(sgst,2) = FORMAT(" . $value1['sgstpercent'] . ",2) AND (" . $newQuery3 . ") AND sgst >0 AND cgst >0")->queryRow();
				$tax_amount4 = $row_tax4['tax_amount'];
				$vndr_cgst = $row_cgst4['cgst'];
				$vndr_sgst = $row_sgst4['sgst'];

				$net_taxamount += $tax_amount1 + $tax_amount2 + $tax_amount3 + $tax_amount4;
				$net_cgst += $bill_cgst + $exp_cgst + $daexp_cgst + $vndr_cgst;
				$net_sgst += $bill_sgst + $exp_sgst + $daexp_sgst + $vndr_sgst;


				$finaldata[$tfoot][] = Controller::money_format_inr($tax_amount1 + $tax_amount2 + $tax_amount3 + $tax_amount4, 2, 1);
				$finaldata[$tfoot][] = Controller::money_format_inr($bill_cgst + $exp_cgst + $daexp_cgst + $vndr_cgst, 2, 1);
				$finaldata[$tfoot][] = Controller::money_format_inr($bill_sgst + $exp_sgst + $daexp_sgst + $vndr_sgst, 2, 1);
			}

			$net_igst = 0;
			$net_tax = 0;
			foreach ($purchase_head_igst as $key1 => $value1) {
				$row_igst1 = Yii::app()->db->createCommand("SELECT SUM(billitem_igst) as igst FROM {$tblpx}billitem LEFT JOIN {$tblpx}bills ON {$tblpx}bills.bill_id ={$tblpx}billitem.bill_id WHERE {$tblpx}bills.bill_date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND FORMAT(billitem_igstpercent,2) = FORMAT(" . $value1['igstpercent'] . ",2) AND (" . $newQuery . ") AND billitem_igstpercent > 0")->queryRow();
				$row_tax1 = Yii::app()->db->createCommand("SELECT SUM(billitem_amount) as tax_amount FROM {$tblpx}billitem LEFT JOIN {$tblpx}bills ON {$tblpx}bills.bill_id ={$tblpx}billitem.bill_id WHERE {$tblpx}bills.bill_date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND FORMAT(billitem_igstpercent,2) = FORMAT(" . $value1['igstpercent'] . ",2) AND billitem_igstpercent > 0 AND (" . $newQuery . ")")->queryRow();
				$row_discount = Yii::app()->db->createCommand("SELECT SUM(billitem_discountamount) as discount_amount FROM {$tblpx}billitem  LEFT JOIN {$tblpx}bills ON {$tblpx}bills.bill_id ={$tblpx}billitem.bill_id WHERE {$tblpx}bills.bill_date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND FORMAT(billitem_igstpercent,2) = FORMAT(" . $value1['igstpercent'] . ",2) AND billitem_igstpercent > 0 AND (" . $newQuery . ")")->queryRow();
				$tax_amount1 = ($row_tax1['tax_amount'] - $row_discount['discount_amount']);

				$row_igst2 = Yii::app()->db->createCommand("SELECT SUM(expense_igst) as igst FROM {$tblpx}expenses WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND FORMAT(expense_igstp,2) = FORMAT(" . $value1['igstpercent'] . ",2) AND (expense_igstp >0) AND {$tblpx}expenses.bill_id IS NULL AND {$tblpx}expenses.invoice_id IS NULL AND {$tblpx}expenses.type=73 AND (" . $newQuery1 . ")")->queryRow();
				$row_tax2 = Yii::app()->db->createCommand("SELECT SUM(expense_amount) as tax_amount FROM {$tblpx}expenses WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "'  AND FORMAT(expense_igstp,2) = FORMAT(" . $value1['igstpercent'] . ",2) AND (" . $newQuery1 . ") AND {$tblpx}expenses.bill_id IS NULL AND {$tblpx}expenses.invoice_id IS NULL AND {$tblpx}expenses.type=73 AND (expense_igstp >0)")->queryRow();
				$tax_amount2 = $row_tax2['tax_amount'];

				$row_igst3 = Yii::app()->db->createCommand("SELECT SUM(dailyexpense_igst) as igst FROM {$tblpx}dailyexpense WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND FORMAT(dailyexpense_igstp,2) = FORMAT(" . $value1['igstpercent'] . ",2) AND (dailyexpense_igstp >0) AND {$tblpx}dailyexpense.exp_type=73 AND (" . $newQuery2 . ")")->queryRow();
				$row_tax3 = Yii::app()->db->createCommand("SELECT SUM(dailyexpense_amount) as tax_amount FROM {$tblpx}dailyexpense WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "'  AND FORMAT(dailyexpense_igstp,2) = FORMAT(" . $value1['igstpercent'] . ",2) AND (" . $newQuery2 . ") AND {$tblpx}dailyexpense.exp_type=73 AND (dailyexpense_igstp >0)")->queryRow();
				$tax_amount3 = $row_tax3['tax_amount'];


				$row_igst4 = Yii::app()->db->createCommand("SELECT SUM(igst_amount) as igst FROM {$tblpx}dailyvendors WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND FORMAT(igst,2) = FORMAT(" . $value1['igstpercent'] . ",2) AND (" . $newQuery3 . ") AND igst >0")->queryRow();
				$row_tax4 = Yii::app()->db->createCommand("SELECT SUM(amount) as tax_amount FROM {$tblpx}dailyvendors WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "'  AND FORMAT(igst,2) = FORMAT(" . $value1['igstpercent'] . ",2) AND (" . $newQuery3 . ") AND igst >0")->queryRow();
				$tax_amount4 = $row_tax4['tax_amount'];

				$net_tax += $tax_amount1 + $tax_amount2 + $tax_amount3 + $tax_amount4;
				$net_igst += $row_igst2['igst'] + $row_igst1['igst'] + $row_igst3['igst'] + $row_igst4['igst'];

				$finaldata[$tfoot][] = Controller::money_format_inr($tax_amount1 + $tax_amount2 + $tax_amount3 + $tax_amount4, 2, 1);
				$finaldata[$tfoot][] = Controller::money_format_inr($row_igst2['igst'] + $row_igst1['igst'] + $row_igst3['igst'] + $row_igst4['igst'], 2, 1);
			}
			$finaldata[$tfoot][] = '';
			$finaldata[$tfoot][] = Controller::money_format_inr(($net_taxamount + $net_cgst + $net_sgst + $net_igst + $net_tax), 2, 1);
		}

		if (!empty($sales)) {
			if (empty($purchase)) {
				$finaldata[0][] = '';
			}

			$finaldata[$tfoot + 1][] = 'Sales ' . $company_name . ' - ' . $date_from . ' to ' . $date_to . '';

			$finaldata[$tfoot + 2][] = 'Sl No';
			$finaldata[$tfoot + 2][] = 'Date';
			$finaldata[$tfoot + 2][] = 'Client';
			$finaldata[$tfoot + 2][] = 'Invoice No';
			$finaldata[$tfoot + 2][] = 'Return No';
			$finaldata[$tfoot + 2][] = 'GST No';
			$finaldata[$tfoot + 2][] = 'Amount';
			foreach ($sales_head as $key_h => $value_h) {
				$finaldata[$tfoot + 2][] = 'Tax.val';
				$finaldata[$tfoot + 2][] = 'CGST ' . $value_h['cgst'] . ' %';
				$finaldata[$tfoot + 2][] = 'SGST ' . $value_h['sgst'] . ' %';
			}

			foreach ($igst_head_sales as $key_h => $value_h) {
				$finaldata[$tfoot + 2][] = 'IGST ' . $value_h['igst'] . ' %';
			}
			$finaldata[$tfoot + 2][] = 'Net Amount';
			if (!empty($sales)) {
				$finaldata[$tfoot + 3][] = '';
				$finaldata[$tfoot + 3][] = '';
				$finaldata[$tfoot + 3][] = '';
				$finaldata[$tfoot + 3][] = '';
				$finaldata[$tfoot + 3][] = '';
				$finaldata[$tfoot + 3][] = 'Total';
				$finaldata[$tfoot + 3][] = Controller::money_format_inr($sales_totalamount, 2);
				$net_taxamount = 0;
				$net_sgst = 0;
				$net_cgst = 0;
				$net_finalamount = 0;
				foreach ($sales_head as $key_h => $value1) {

					$query = '';
					if ($value1['sgst'] == null) {
						$sgst = 0;
						$query .= " AND {$tblpx}inv_list.sgst IS NULL";
					} else {
						$sgst = $value1['sgst'];
						$query .= "AND FORMAT({$tblpx}inv_list.sgst,2) = FORMAT(" . $sgst . ",2)";
					}

					if ($value1['cgst'] == null) {
						$cgst = 0;
						$query .= " AND {$tblpx}inv_list.cgst IS NULL";
					} else {
						$cgst = $value1['cgst'];
						$query .= " AND FORMAT({$tblpx}inv_list.cgst,2) = FORMAT(" . $cgst . ",2)";
					}


					$row_sgst1 = Yii::app()->db->createCommand("SELECT SUM({$tblpx}inv_list.sgst_amount) as sgst FROM {$tblpx}inv_list LEFT JOIN {$tblpx}invoice ON {$tblpx}invoice.invoice_id ={$tblpx}inv_list.inv_id WHERE {$tblpx}invoice.date BETWEEN '" . $date_from . "' AND '" . $date_to . "' " . $query . " ")->queryRow();
					$row_cgst1 = Yii::app()->db->createCommand("SELECT SUM({$tblpx}inv_list.cgst_amount) as cgst FROM {$tblpx}inv_list LEFT JOIN {$tblpx}invoice ON {$tblpx}invoice.invoice_id ={$tblpx}inv_list.inv_id WHERE {$tblpx}invoice.date BETWEEN '" . $date_from . "' AND '" . $date_to . "' " . $query . "")->queryRow();
					$row_tax1 = Yii::app()->db->createCommand("SELECT SUM({$tblpx}inv_list.amount) as tax_amount FROM {$tblpx}inv_list LEFT JOIN {$tblpx}invoice ON {$tblpx}invoice.invoice_id ={$tblpx}inv_list.inv_id WHERE {$tblpx}invoice.date BETWEEN '" . $date_from . "' AND '" . $date_to . "' " . $query . "")->queryRow();
					//$net_finalamount += $row_tax['tax_amount']+$row_cgst['cgst']+$row_sgst['sgst'];
					$sales_taxamount = $row_tax1['tax_amount'];
					$sales_sgst = $row_sgst1['sgst'];
					$sales_cgst = $row_cgst1['cgst'];
					//}else if($data['type'] =='Purchase Return') {
					//echo "jjj";
					$row_cgst2 = Yii::app()->db->createCommand("SELECT SUM(returnitem_cgst) as cgst FROM {$tblpx}purchase_returnitem LEFT JOIN {$tblpx}purchase_return ON {$tblpx}purchase_returnitem.return_id = {$tblpx}purchase_return.return_id  WHERE {$tblpx}purchase_return.return_date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND FORMAT(returnitem_cgstpercent,2) = FORMAT(" . $value1['cgst'] . ",2) AND FORMAT(returnitem_sgstpercent,2) = FORMAT(" . $value1['sgst'] . ",2)")->queryRow();
					$row_sgst2 = Yii::app()->db->createCommand("SELECT SUM(returnitem_sgst) as sgst FROM {$tblpx}purchase_returnitem LEFT JOIN {$tblpx}purchase_return ON {$tblpx}purchase_returnitem.return_id = {$tblpx}purchase_return.return_id  WHERE {$tblpx}purchase_return.return_date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND FORMAT(returnitem_cgstpercent,2) = FORMAT(" . $value1['cgst'] . ",2) AND FORMAT(returnitem_sgstpercent,2) = FORMAT(" . $value1['sgst'] . ",2)")->queryRow();
					$row_tax2 = Yii::app()->db->createCommand("SELECT SUM(returnitem_amount) as tax_amount FROM {$tblpx}purchase_returnitem LEFT JOIN {$tblpx}purchase_return ON {$tblpx}purchase_returnitem.return_id = {$tblpx}purchase_return.return_id  WHERE {$tblpx}purchase_return.return_date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND FORMAT(returnitem_cgstpercent,2) = FORMAT(" . $value1['cgst'] . ",2) AND FORMAT(returnitem_sgstpercent,2) = FORMAT(" . $value1['sgst'] . ",2)")->queryRow();
					$row_discount = Yii::app()->db->createCommand("SELECT SUM(returnitem_discountamount) as discount_amount FROM {$tblpx}purchase_returnitem LEFT JOIN {$tblpx}purchase_return ON {$tblpx}purchase_returnitem.return_id = {$tblpx}purchase_return.return_id  WHERE {$tblpx}purchase_return.return_date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND FORMAT(returnitem_cgstpercent,2) = FORMAT(" . $value1['cgst'] . ",2) AND FORMAT(returnitem_sgstpercent,2) = FORMAT(" . $value1['sgst'] . ",2)")->queryRow();
					$return_taxamount = ($row_tax2['tax_amount'] - $row_discount['discount_amount']);
					$return_cgst = $row_cgst2['cgst'];
					$return_sgst = $row_sgst2['sgst'];


					$net_sgst = $sales_sgst + $return_sgst;
					$net_cgst = $sales_cgst + $return_cgst;
					$net_taxamount = $sales_taxamount + $return_taxamount;
					$net_finalamount += $net_taxamount + $net_cgst + $net_sgst;

					$finaldata[$tfoot + 3][] = Controller::money_format_inr($net_taxamount, 2, 1);
					$finaldata[$tfoot + 3][] = Controller::money_format_inr($net_sgst, 2, 1);
					$finaldata[$tfoot + 3][] = Controller::money_format_inr($net_cgst, 2, 1);
				}

				$net_igst = 0;
				foreach ($igst_head_sales as $key_h => $value1) {
					$query = '';
					if ($value1['igst'] == null) {
						$igst = 0;
						$query .= " AND igst IS NULL";
					} else {
						$igst = $value1['igst'];
						$query .= " AND FORMAT(igst,2) = FORMAT(" . $igst . ",2)";
					}
					//   if($data['type'] =='Invoice') {
					// 	$row_igst1 = Yii::app()->db->createCommand("SELECT SUM(igst_amount) as igst FROM {$tblpx}inv_list WHERE inv_id=".$data['id']." ".$query." ")->queryRow();

					// 	$sales_igst = $row_igst1['igst'];
					//  } else if($data['type'] =='Purchase Return') {

					// 	$sales_igst = $row_igst2['igst'];

					//  }
					$row_igst1 = Yii::app()->db->createCommand("SELECT SUM(igst_amount) as igst FROM {$tblpx}inv_list LEFT JOIN {$tblpx}invoice ON {$tblpx}invoice.invoice_id ={$tblpx}inv_list.inv_id WHERE {$tblpx}invoice.date BETWEEN '" . $date_from . "' AND '" . $date_to . "' " . $query . " ")->queryRow();
					$sales_igst = $row_igst1['igst'];
					$row_igst2 = Yii::app()->db->createCommand("SELECT SUM(returnitem_igst) as igst FROM {$tblpx}purchase_returnitem LEFT JOIN {$tblpx}purchase_return ON {$tblpx}purchase_returnitem.return_id = {$tblpx}purchase_return.return_id  WHERE {$tblpx}purchase_return.return_date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND FORMAT(returnitem_igstpercent,2) = FORMAT(" . $value1['igst'] . ",2)")->queryRow();
					$return_igst = $row_igst2['igst'];
					$net_igst = $row_igst2['igst'] + $row_igst1['igst'];

					$finaldata[$tfoot + 3][] = Controller::money_format_inr($row_igst1['igst'] + $row_igst2['igst'], 2, 1);
				}

				$finaldata[$tfoot + 3][] = Controller::money_format_inr($net_finalamount + $net_igst, 2, 1);
			}
			$sale_loop = $tfoot + 4;
			$sales_foot = 1;
			$no = 0;
			foreach ($sales as $key => $data) {
				$no++;
				$finaldata[$sale_loop][] = $no;
				$finaldata[$sale_loop][] = date('Y-m-d', strtotime($data['date']));
				$finaldata[$sale_loop][] = $data['client'];
				$finaldata[$sale_loop][] = $data['inv_no'];
				$finaldata[$sale_loop][] = $data['return_no'];
				$finaldata[$sale_loop][] = $data['gst_no'];
				$finaldata[$sale_loop][] = Controller::money_format_inr($data['totalamount'], 2, 1);
				$net_amount = 0;
				foreach ($sales_head as $key_h => $value1) {

					$query = '';
					if ($value1['sgst'] == null) {
						$sgst = 0;
						$query .= " AND sgst IS NULL";
					} else {
						$sgst = $value1['sgst'];
						$query .= "AND FORMAT(sgst,2) = FORMAT(" . $sgst . ",2)";
					}

					if ($value1['cgst'] == null) {
						$cgst = 0;
						$query .= " AND cgst IS NULL";
					} else {
						$cgst = $value1['cgst'];
						$query .= " AND FORMAT(cgst,2) = FORMAT(" . $cgst . ",2)";
					}

					if ($data['type'] == 'Invoice') {
						$row_sgst1 = Yii::app()->db->createCommand("SELECT SUM(sgst_amount) as sgst FROM {$tblpx}inv_list WHERE inv_id=" . $data['id'] . " " . $query . " ")->queryRow();
						//$row_igst = Yii::app()->db->createCommand("SELECT SUM(igst_amount) as igst FROM {$tblpx}inv_list WHERE inv_id=".$data['invoice_id']." ".$query." ")->queryRow();
						$row_cgst1 = Yii::app()->db->createCommand("SELECT SUM(cgst_amount) as cgst FROM {$tblpx}inv_list WHERE inv_id=" . $data['id'] . " " . $query . "")->queryRow();
						$row_tax = Yii::app()->db->createCommand("SELECT SUM(amount) as tax_amount FROM {$tblpx}inv_list WHERE inv_id=" . $data['id'] . " " . $query . "")->queryRow();
						$tax_amount = $row_tax['tax_amount'];
						$sales_cgst = $row_cgst1['cgst'];
						$sales_sgst = $row_sgst1['sgst'];
						$net_amount += $tax_amount + $sales_cgst + $sales_sgst;
					} else if ($data['type'] == 'Purchase Return') {
						$row_cgst2 = Yii::app()->db->createCommand("SELECT SUM(returnitem_cgst) as cgst FROM {$tblpx}purchase_returnitem WHERE return_id=" . $data['id'] . " AND FORMAT(returnitem_cgstpercent,2) = FORMAT(" . $value1['cgst'] . ",2) AND FORMAT(returnitem_sgstpercent,2) = FORMAT(" . $value1['sgst'] . ",2)")->queryRow();
						$row_sgst2 = Yii::app()->db->createCommand("SELECT SUM(returnitem_sgst) as sgst FROM {$tblpx}purchase_returnitem WHERE return_id=" . $data['id'] . " AND FORMAT(returnitem_cgstpercent,2) = FORMAT(" . $value1['cgst'] . ",2) AND FORMAT(returnitem_sgstpercent,2) = FORMAT(" . $value1['sgst'] . ",2)")->queryRow();
						$row_tax2 = Yii::app()->db->createCommand("SELECT SUM(returnitem_amount) as tax_amount FROM {$tblpx}purchase_returnitem WHERE return_id=" . $data['id'] . " AND FORMAT(returnitem_cgstpercent,2) = FORMAT(" . $value1['cgst'] . ",2) AND FORMAT(returnitem_sgstpercent,2) = FORMAT(" . $value1['sgst'] . ",2)")->queryRow();
						$row_discount = Yii::app()->db->createCommand("SELECT SUM(returnitem_discountamount) as discount_amount FROM {$tblpx}purchase_returnitem WHERE return_id=" . $data['id'] . " AND FORMAT(returnitem_cgstpercent,2) = FORMAT(" . $value1['cgst'] . ",2) AND FORMAT(returnitem_sgstpercent,2) = FORMAT(" . $value1['sgst'] . ",2)")->queryRow();
						$tax_amount = ($row_tax2['tax_amount'] - $row_discount['discount_amount']);
						$sales_cgst = $row_cgst2['cgst'];
						$sales_sgst = $row_sgst2['sgst'];
						$net_amount += $tax_amount + $sales_cgst + $sales_sgst;
					}

					$finaldata[$sale_loop][] = (($tax_amount == 0) ? '' : Controller::money_format_inr($tax_amount, 2, 1));
					$finaldata[$sale_loop][] = ($sales_cgst == 0) ? '' : Controller::money_format_inr($sales_cgst, 2, 1);
					$finaldata[$sale_loop][] = ($sales_sgst == 0) ? '' : Controller::money_format_inr($sales_sgst, 2, 1);
				}

				$net_amountigst = 0;
				foreach ($igst_head_sales as $key_h => $value1) {
					$query = '';
					if ($value1['igst'] == null) {
						$igst = 0;
						$query .= " AND igst IS NULL";
					} else {
						$igst = $value1['igst'];
						$query .= " AND FORMAT(igst,2) = FORMAT(" . $igst . ",2)";
					}

					if ($data['type'] == 'Invoice') {
						$row_igst1 = Yii::app()->db->createCommand("SELECT SUM(igst_amount) as igst FROM {$tblpx}inv_list WHERE inv_id=" . $data['id'] . " " . $query . " ")->queryRow();
						$row_tax1 = Yii::app()->db->createCommand("SELECT SUM(amount) as tax_amount FROM {$tblpx}inv_list WHERE inv_id=" . $data['id'] . " " . $query . "")->queryRow();
						$net_amountigst += $row_igst1['igst'];
						$sales_igst = $row_igst1['igst'];
					} else if ($data['type'] == 'Purchase Return') {
						$row_igst2 = Yii::app()->db->createCommand("SELECT SUM(returnitem_igst) as igst FROM {$tblpx}purchase_returnitem WHERE return_id=" . $data['id'] . " AND FORMAT(returnitem_igstpercent,2) = FORMAT(" . $value1['igst'] . ",2)")->queryRow();
						$sales_igst = $row_igst2['igst'];
						$row_tax2 = Yii::app()->db->createCommand("SELECT SUM(returnitem_amount) as tax_amount FROM {$tblpx}purchase_returnitem WHERE return_id=" . $data['id'] . " AND FORMAT(returnitem_igstpercent,2) = FORMAT(" . $value1['igst'] . ",2)")->queryRow();
						$row_discount = Yii::app()->db->createCommand("SELECT SUM(returnitem_discountamount) as discount_amount FROM {$tblpx}purchase_returnitem WHERE return_id=" . $data['id'] . " AND FORMAT(returnitem_igstpercent,2) = FORMAT(" . $value1['igst'] . ",2)")->queryRow();
						$tax_amount = ($row_tax2['tax_amount'] - $row_discount['discount_amount']);
						$net_amountigst += $row_igst2['igst'];
					}
					$finaldata[$sale_loop][] = ($sales_igst == 0) ? '' : Controller::money_format_inr($sales_igst, 2, 1);
				}

				$finaldata[$sale_loop][] = Controller::money_format_inr($net_amount + $net_amountigst, 2);
				$sale_loop++;
				$sales_foot = $sale_loop;
			}

			//   if(!empty($sales)) {
			// 	  $finaldata[$sales_foot][] ='';
			// 	  $finaldata[$sales_foot][] ='';
			// 	  $finaldata[$sales_foot][] ='';
			// 	  $finaldata[$sales_foot][] ='';
			// 	  $finaldata[$sales_foot][] ='';
			//                           $finaldata[$sales_foot][] ='';
			// 	  $finaldata[$sales_foot][] = Controller::money_format_inr($sales_totalamount,2);
			// 		$net_taxamount = 0;
			// 		$net_sgst = 0;
			// 		$net_cgst = 0;
			// 		$net_finalamount = 0;
			// 		foreach($sales_head as $key_h => $value1){

			// 		$query = '';
			// 		if($value1['sgst'] == null) {
			// 			$sgst = 0;
			// 			$query .=" AND {$tblpx}inv_list.sgst IS NULL";
			// 		} else{
			// 			$sgst = $value1['sgst'];
			// 			$query .= "AND FORMAT({$tblpx}inv_list.sgst,2) = FORMAT(".$sgst.",2)";
			// 		}

			// 		if($value1['cgst'] == null) {
			// 			$cgst = 0;
			// 			$query .=" AND {$tblpx}inv_list.cgst IS NULL";
			// 		} else{
			// 			$cgst = $value1['cgst'];
			// 			$query .= " AND FORMAT({$tblpx}inv_list.cgst,2) = FORMAT(".$cgst.",2)";
			// 		}


			// 		$row_sgst1 = Yii::app()->db->createCommand("SELECT SUM({$tblpx}inv_list.sgst_amount) as sgst FROM {$tblpx}inv_list LEFT JOIN {$tblpx}invoice ON {$tblpx}invoice.invoice_id ={$tblpx}inv_list.inv_id WHERE {$tblpx}invoice.date BETWEEN '".$date_from."' AND '".$date_to."' ".$query." ")->queryRow();
			//                                 $row_cgst1 = Yii::app()->db->createCommand("SELECT SUM({$tblpx}inv_list.cgst_amount) as cgst FROM {$tblpx}inv_list LEFT JOIN {$tblpx}invoice ON {$tblpx}invoice.invoice_id ={$tblpx}inv_list.inv_id WHERE {$tblpx}invoice.date BETWEEN '".$date_from."' AND '".$date_to."' ".$query."")->queryRow();
			//                                 $row_tax1 = Yii::app()->db->createCommand("SELECT SUM({$tblpx}inv_list.amount) as tax_amount FROM {$tblpx}inv_list LEFT JOIN {$tblpx}invoice ON {$tblpx}invoice.invoice_id ={$tblpx}inv_list.inv_id WHERE {$tblpx}invoice.date BETWEEN '".$date_from."' AND '".$date_to."' ".$query."")->queryRow();
			//                                 //$net_finalamount += $row_tax['tax_amount']+$row_cgst['cgst']+$row_sgst['sgst'];
			//                                 $sales_taxamount = $row_tax1['tax_amount'];
			//                                 $sales_sgst = $row_sgst1['sgst'];
			//                                 $sales_cgst = $row_cgst1['cgst'];
			//                             //}else if($data['type'] =='Purchase Return') {
			//                                 //echo "jjj";
			//                                 $row_cgst2 = Yii::app()->db->createCommand("SELECT SUM(returnitem_cgst) as cgst FROM {$tblpx}purchase_returnitem LEFT JOIN {$tblpx}purchase_return ON {$tblpx}purchase_returnitem.return_id = {$tblpx}purchase_return.return_id  WHERE {$tblpx}purchase_return.return_date BETWEEN '".$date_from."' AND '".$date_to."' AND FORMAT(returnitem_cgstpercent,2) = FORMAT(".$value1['cgst'].",2) AND FORMAT(returnitem_sgstpercent,2) = FORMAT(".$value1['sgst'].",2)")->queryRow();
			//                                 $row_sgst2 = Yii::app()->db->createCommand("SELECT SUM(returnitem_sgst) as sgst FROM {$tblpx}purchase_returnitem LEFT JOIN {$tblpx}purchase_return ON {$tblpx}purchase_returnitem.return_id = {$tblpx}purchase_return.return_id  WHERE {$tblpx}purchase_return.return_date BETWEEN '".$date_from."' AND '".$date_to."' AND FORMAT(returnitem_cgstpercent,2) = FORMAT(".$value1['cgst'].",2) AND FORMAT(returnitem_sgstpercent,2) = FORMAT(".$value1['sgst'].",2)")->queryRow();
			//                                 $row_tax2 = Yii::app()->db->createCommand("SELECT SUM(returnitem_amount) as tax_amount FROM {$tblpx}purchase_returnitem LEFT JOIN {$tblpx}purchase_return ON {$tblpx}purchase_returnitem.return_id = {$tblpx}purchase_return.return_id  WHERE {$tblpx}purchase_return.return_date BETWEEN '".$date_from."' AND '".$date_to."' AND FORMAT(returnitem_cgstpercent,2) = FORMAT(".$value1['cgst'].",2) AND FORMAT(returnitem_sgstpercent,2) = FORMAT(".$value1['sgst'].",2)")->queryRow();
			//                                 $row_discount = Yii::app()->db->createCommand("SELECT SUM(returnitem_discountamount) as discount_amount FROM {$tblpx}purchase_returnitem LEFT JOIN {$tblpx}purchase_return ON {$tblpx}purchase_returnitem.return_id = {$tblpx}purchase_return.return_id  WHERE {$tblpx}purchase_return.return_date BETWEEN '".$date_from."' AND '".$date_to."' AND FORMAT(returnitem_cgstpercent,2) = FORMAT(".$value1['cgst'].",2) AND FORMAT(returnitem_sgstpercent,2) = FORMAT(".$value1['sgst'].",2)")->queryRow();
			//                                 $return_taxamount = ($row_tax2['tax_amount']-$row_discount['discount_amount']);
			//                                 $return_cgst =   $row_cgst2['cgst'];
			//                                 $return_sgst = $row_sgst2['sgst'];


			//                                 $net_sgst = $sales_sgst+$return_sgst;
			//                                 $net_cgst = $sales_cgst+$return_cgst;
			//                                 $net_taxamount = $sales_taxamount+$return_taxamount;
			//                                 $net_finalamount += $net_taxamount+$net_cgst+$net_sgst;

			// 		 $finaldata[$sales_foot][] =Controller::money_format_inr($net_taxamount,2,1);
			// 		 $finaldata[$sales_foot][] =Controller::money_format_inr($net_sgst,2,1);
			// 		 $finaldata[$sales_foot][] =Controller::money_format_inr($net_cgst,2,1);
			// 	   }

			// 	   $net_igst= 0;
			// 		foreach($igst_head_sales as $key_h => $value1){
			// 			$query = '';
			// 			if($value1['igst'] == null) {
			// 				$igst = 0;
			// 				$query .=" AND igst IS NULL";
			// 			} else{
			// 				$igst = $value1['igst'];
			// 				$query .= " AND FORMAT(igst,2) = FORMAT(".$igst.",2)";
			// 			}

			// 			$row_igst1 = Yii::app()->db->createCommand("SELECT SUM(igst_amount) as igst FROM {$tblpx}inv_list LEFT JOIN {$tblpx}invoice ON {$tblpx}invoice.invoice_id ={$tblpx}inv_list.inv_id WHERE {$tblpx}invoice.date BETWEEN '".$date_from."' AND '".$date_to."' ".$query." ")->queryRow();
			//                                         $sales_igst += $row_igst1['igst'];
			//                                         $row_igst2 = Yii::app()->db->createCommand("SELECT SUM(returnitem_igst) as igst FROM {$tblpx}purchase_returnitem LEFT JOIN {$tblpx}purchase_return ON {$tblpx}purchase_returnitem.return_id = {$tblpx}purchase_return.return_id  WHERE {$tblpx}purchase_return.return_date BETWEEN '".$date_from."' AND '".$date_to."' AND FORMAT(returnitem_igstpercent,2) = FORMAT(".$value1['igst'].",2)")->queryRow();
			//                                         $return_igst = $row_igst2['igst'];
			//                                         $net_igst = $row_igst2['igst']+$row_igst1['igst'];

			// 			$finaldata[$sales_foot][] =Controller::money_format_inr($row_igst1['igst']+$row_igst2['igst'],2,1);
			// 		}

			// 		$finaldata[$sales_foot][] =Controller::money_format_inr($net_finalamount+$net_igst,2,1);
			//   }
		}


		Yii::import('ext.ECSVExport');
		$csv = new ECSVExport($finaldata);
		$csv->setHeaders($arraylabel);
		$csv->setToAppend();
		$contentdata = $csv->toCSV();
		$csvfilename = 'Taxreport' . date('d_M_Y') . '.csv';
		Yii::app()->getRequest()->sendFile($csvfilename, $contentdata, "text/csv", false);
		exit();
	}

	public function actionTaxreportpdf($date_from, $date_to, $company_id)
	{

		$this->logo = $this->realpath_logo;
		$tblpx = Yii::app()->db->tablePrefix;
		$date_from = date('Y-m-d', strtotime($date_from));
		$date_to = date('Y-m-d', strtotime($date_to));
		$user = Users::model()->findByPk(Yii::app()->user->id);
		$arrVal = explode(',', $user->company_id);
		$newQuery = "";
		$newQuery1 = "";
		$newQuery2 = "";
		$newQuery3 = "";
		$newQuery4 = "";
		$newQuery5 = "";

		if ($company_id == NULL) {
			foreach ($arrVal as $arr) {
				if ($newQuery)
					$newQuery .= ' OR';
				if ($newQuery1)
					$newQuery1 .= ' OR';
				if ($newQuery2)
					$newQuery2 .= ' OR';
				if ($newQuery3)
					$newQuery3 .= ' OR';
				if ($newQuery4)
					$newQuery4 .= ' OR';
				if ($newQuery5)
					$newQuery5 .= ' OR';
				$newQuery .= " FIND_IN_SET('" . $arr . "', {$tblpx}bills.company_id)";
				$newQuery1 .= " FIND_IN_SET('" . $arr . "', {$tblpx}expenses.company_id)";
				$newQuery2 .= " FIND_IN_SET('" . $arr . "', {$tblpx}dailyexpense.company_id)";
				$newQuery3 .= " FIND_IN_SET('" . $arr . "', {$tblpx}dailyvendors.company_id)";
				$newQuery4 .= " FIND_IN_SET('" . $arr . "', {$tblpx}invoice.company_id)";
				$newQuery5 .= " FIND_IN_SET('" . $arr . "', {$tblpx}purchase_return.company_id)";
			}
		} else {
			$newQuery .= " FIND_IN_SET('" . $company_id . "', {$tblpx}bills.company_id)";
			$newQuery1 .= " FIND_IN_SET('" . $company_id . "', {$tblpx}expenses.company_id)";
			$newQuery2 .= " FIND_IN_SET('" . $company_id . "', {$tblpx}dailyexpense.company_id)";
			$newQuery3 .= " FIND_IN_SET('" . $company_id . "', {$tblpx}dailyvendors.company_id)";
			$newQuery4 .= " FIND_IN_SET('" . $company_id . "', {$tblpx}invoice.company_id)";
			$newQuery5 .= " FIND_IN_SET('" . $company_id . "', {$tblpx}purchase_return.company_id)";
		}



		// purchase  section
		$purchase = array();
		$purchase_head = array();
		$purchase_head_igst = array();
		$a = 0;
		$b = 0;
		$c = 0;
		$d = 0;
		$e = 0;
		$f = 0;
		$g = 0;
		$h = 0;
		$i = 0;
		$j = 0;
		// bills
		$data = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}bills LEFT JOIN {$tblpx}purchase ON {$tblpx}bills.purchase_id= {$tblpx}purchase.p_id LEFT JOIN {$tblpx}vendors ON {$tblpx}purchase.vendor_id={$tblpx}vendors.vendor_id WHERE (" . $newQuery . ") AND {$tblpx}bills.bill_date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND {$tblpx}bills.bill_taxamount > 0 ORDER BY {$tblpx}bills.bill_date desc")->queryAll();
		foreach ($data as $key => $value) {
			$purchase[$key]['bill_number'] = $value['bill_number'];
			$purchase[$key]['date'] = $value['bill_date'];
			$purchase[$key]['vendor_name'] = $value['name'];
			$purchase[$key]['gst_no'] = $value['gst_no'];
			$purchase[$key]['totalamount'] = $value['bill_totalamount'];
			$purchase[$key]['id'] = $value['bill_id'];
			$purchase[$key]['type'] = 'bills';
		}

		$data1 = Yii::app()->db->createCommand("SELECT DISTINCT billitem_cgstpercent,billitem_igstpercent,billitem_sgstpercent FROM {$tblpx}billitem  LEFT JOIN {$tblpx}bills ON {$tblpx}bills.bill_id ={$tblpx}billitem.bill_id WHERE {$tblpx}bills.bill_date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND billitem_cgstpercent > 0 AND billitem_sgstpercent > 0 AND (" . $newQuery . ") GROUP BY billitem_cgstpercent, billitem_sgstpercent")->queryAll();
		foreach ($data1 as $key => $value) {
			$purchase_head[$key]['cgstpercent'] = $value['billitem_cgstpercent'];
			$purchase_head[$key]['sgstpercent'] = $value['billitem_sgstpercent'];
			//$purchase_head[$key]['igstpercent'] = $value['billitem_igstpercent'];
		}

		$data5 = Yii::app()->db->createCommand("SELECT DISTINCT billitem_igstpercent FROM {$tblpx}billitem  LEFT JOIN {$tblpx}bills ON {$tblpx}bills.bill_id ={$tblpx}billitem.bill_id WHERE {$tblpx}bills.bill_date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND billitem_igstpercent > 0 AND (" . $newQuery . ") GROUP BY billitem_igstpercent")->queryAll();
		foreach ($data5 as $key => $value) {
			$purchase_head_igst[$key]['igstpercent'] = $value['billitem_igstpercent'];
		}

		// day book

		$a = count($data) + 1;
		$data6 = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}expenses LEFT JOIN {$tblpx}projects ON {$tblpx}expenses.projectid={$tblpx}projects.pid LEFT JOIN {$tblpx}expense_type ON {$tblpx}expenses.exptype = {$tblpx}expense_type.type_id LEFT JOIN {$tblpx}vendors ON {$tblpx}expenses.vendor_id= {$tblpx}vendors.vendor_id WHERE {$tblpx}expenses.date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND (" . $newQuery1 . ") AND {$tblpx}expenses.bill_id IS NULL AND {$tblpx}expenses.invoice_id IS NULL AND {$tblpx}expenses.type=73 AND ((expense_sgstp > 0 AND expense_cgstp > 0) OR (expense_igstp >0)) ORDER BY {$tblpx}expenses.date desc")->queryAll();
		foreach ($data6 as $key => $value) {
			$key = count(($data1)) + 1;
			$purchase[$a]['bill_number'] = '';
			$purchase[$a]['date'] = $value['date'];
			$purchase[$a]['vendor_name'] = $value['name'];
			$purchase[$a]['gst_no'] = $value['gst_no'];
			$purchase[$a]['totalamount'] = $value['amount'];
			$purchase[$a]['id'] = $value['exp_id'];
			$purchase[$a]['type'] = 'daybook';
			$a++;
		}

		$b = count($data1) + 1;
		$data7 = Yii::app()->db->createCommand("SELECT DISTINCT expense_sgstp, expense_cgstp, expense_igstp FROM {$tblpx}expenses WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND (expense_sgstp > 0 AND expense_cgstp > 0) AND {$tblpx}expenses.bill_id IS NULL AND {$tblpx}expenses.invoice_id IS NULL AND {$tblpx}expenses.type=73 AND (" . $newQuery1 . ") GROUP BY expense_sgstp,expense_cgstp")->queryAll();
		foreach ($data7 as $key => $value) {
			$purchase_head[$b]['cgstpercent'] = $value['expense_cgstp'];
			$purchase_head[$b]['sgstpercent'] = $value['expense_sgstp'];
			//$purchase_head[$b]['igstpercent'] = $value['expense_igstp'];
			$b++;
		}

		$c = count($data5) + 1;
		;
		$data8 = Yii::app()->db->createCommand("SELECT DISTINCT expense_igstp FROM {$tblpx}expenses WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND (expense_igstp >0) AND {$tblpx}expenses.bill_id IS NULL AND {$tblpx}expenses.invoice_id IS NULL AND {$tblpx}expenses.type=73 AND (" . $newQuery1 . ") GROUP BY expense_igstp")->queryAll();
		foreach ($data8 as $key => $value) {
			$purchase_head_igst[$c]['igstpercent'] = $value['expense_igstp'];
			$c++;
		}

		// daily expense


		$d = count($data) + count($data6) + 1;
		$data9 = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}dailyexpense WHERE {$tblpx}dailyexpense.date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND (" . $newQuery2 . ") AND {$tblpx}dailyexpense.exp_type=73 AND ((dailyexpense_sgstp > 0 AND dailyexpense_cgstp > 0) OR (dailyexpense_igstp > 0)) ORDER BY {$tblpx}dailyexpense.date desc")->queryAll();
		foreach ($data9 as $key => $value) {
			$key = count(($data1)) + 1;
			$purchase[$d]['bill_number'] = '';
			$purchase[$d]['date'] = $value['date'];
			$purchase[$d]['vendor_name'] = '';
			$purchase[$d]['gst_no'] = '';
			$purchase[$d]['totalamount'] = $value['amount'];
			$purchase[$d]['id'] = $value['dailyexp_id'];
			$purchase[$d]['type'] = 'dailyexpense';
			$d++;
		}

		$e = count($data1) + count($data7) + 1;
		$data10 = Yii::app()->db->createCommand("SELECT DISTINCT dailyexpense_sgstp, dailyexpense_cgstp, dailyexpense_igstp FROM {$tblpx}dailyexpense WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND (dailyexpense_sgstp > 0 AND dailyexpense_cgstp > 0) AND {$tblpx}dailyexpense.exp_type=73 AND (" . $newQuery2 . ") GROUP BY dailyexpense_sgstp, dailyexpense_cgstp")->queryAll();
		foreach ($data10 as $key => $value) {
			$purchase_head[$e]['cgstpercent'] = $value['dailyexpense_cgstp'];
			$purchase_head[$e]['sgstpercent'] = $value['dailyexpense_sgstp'];
			$e++;
		}


		$f = count($data5) + count($data8) + 1;
		;
		$data11 = Yii::app()->db->createCommand("SELECT DISTINCT dailyexpense_igstp FROM {$tblpx}dailyexpense WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND (dailyexpense_igstp >0) AND {$tblpx}dailyexpense.exp_type=73 AND (" . $newQuery2 . ") GROUP BY dailyexpense_igstp")->queryAll();
		foreach ($data11 as $key => $value) {
			$purchase_head_igst[$c]['igstpercent'] = $value['dailyexpense_igstp'];
			$c++;
		}


		// vendor payment

		$g = count($data) + count($data6) + count($data9) + 1;
		$data12 = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}dailyvendors LEFT JOIN {$tblpx}vendors ON {$tblpx}dailyvendors.vendor_id = {$tblpx}vendors.vendor_id  WHERE {$tblpx}dailyvendors.date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND (" . $newQuery3 . ") AND {$tblpx}dailyvendors.tax_amount > 0 ORDER BY {$tblpx}dailyvendors.date desc")->queryAll();
		foreach ($data12 as $key => $value) {
			$key = count(($data1)) + 1;
			$purchase[$g]['bill_number'] = '';
			$purchase[$g]['date'] = $value['date'];
			$purchase[$g]['vendor_name'] = $value['name'];
			$purchase[$g]['gst_no'] = $value['gst_no'];
			$purchase[$g]['totalamount'] = $value['amount'];
			$purchase[$g]['id'] = $value['daily_v_id'];
			$purchase[$g]['type'] = 'vendorpayment';
			$g++;
		}

		$h = count($data1) + count($data7) + count($data10) + 1;
		$data13 = Yii::app()->db->createCommand("SELECT DISTINCT sgst, cgst, igst FROM {$tblpx}dailyvendors WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND {$tblpx}dailyvendors.tax_amount >0 AND (" . $newQuery3 . ") AND sgst > 0 AND cgst > 0 GROUP BY sgst, cgst")->queryAll();
		foreach ($data13 as $key => $value) {
			$purchase_head[$h]['cgstpercent'] = $value['cgst'];
			$purchase_head[$h]['sgstpercent'] = $value['sgst'];
			$h++;
		}

		$j = count($data5) + count($data8) + count($data11) + 1;
		;
		$data14 = Yii::app()->db->createCommand("SELECT DISTINCT igst FROM {$tblpx}dailyvendors WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND {$tblpx}dailyvendors.tax_amount > 0 AND (" . $newQuery3 . ") AND igst > 0 GROUP BY igst")->queryAll();
		foreach ($data14 as $key => $value) {
			$purchase_head_igst[$j]['igstpercent'] = $value['igst'];
			$j++;
		}

		// invoice

		//				$data2     = Yii::app()->db->createCommand("SELECT {$tblpx}invoice.*, {$tblpx}projects.name as project_name,{$tblpx}clients.name as client,{$tblpx}clients.gst_no  FROM {$tblpx}invoice LEFT JOIN {$tblpx}projects ON {$tblpx}invoice.project_id = {$tblpx}projects.pid LEFT JOIN {$tblpx}clients ON {$tblpx}invoice.client_id = {$tblpx}clients.cid WHERE {$tblpx}invoice.company_id=".Yii::app()->user->company_id." AND {$tblpx}invoice.date BETWEEN '".$date_from."' AND '".$date_to."' ORDER BY {$tblpx}invoice.date desc");
		//				$sales     = $data2->queryAll();
		//
		//				$data3     = Yii::app()->db->createCommand("SELECT DISTINCT sgst,cgst,igst FROM {$tblpx}inv_list LEFT JOIN {$tblpx}invoice ON {$tblpx}invoice.invoice_id ={$tblpx}inv_list.inv_id WHERE {$tblpx}invoice.date BETWEEN '".$date_from."' AND '".$date_to."' GROUP BY sgst,cgst");
		//				$sales_head = $data3->queryAll();
		//
		//				$data4 = Yii::app()->db->createCommand("SELECT DISTINCT igst FROM {$tblpx}inv_list LEFT JOIN {$tblpx}invoice ON {$tblpx}invoice.invoice_id ={$tblpx}inv_list.inv_id WHERE {$tblpx}invoice.date BETWEEN '".$date_from."' AND '".$date_to."'  GROUP BY igst");
		//				$igst_head_sales = $data4->queryAll();

		$invoice_head = array();
		$invoice_head_igst = array();
		$sales = array();

		$p = 0;
		$q = 0;
		$r = 0;
		$s = 0;
		$t = 0;

		$data2 = Yii::app()->db->createCommand("SELECT {$tblpx}invoice.*, {$tblpx}projects.name as project_name,{$tblpx}clients.name as client,{$tblpx}clients.gst_no  FROM {$tblpx}invoice LEFT JOIN {$tblpx}projects ON {$tblpx}invoice.project_id = {$tblpx}projects.pid LEFT JOIN {$tblpx}clients ON {$tblpx}invoice.client_id = {$tblpx}clients.cid WHERE (" . $newQuery4 . ") AND {$tblpx}invoice.date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND {$tblpx}invoice.invoice_status ='saved' ORDER BY {$tblpx}invoice.date desc");
		$sales_data = $data2->queryAll();

		$data15 = Yii::app()->db->createCommand("SELECT {$tblpx}purchase_return.*,{$tblpx}clients.* FROM {$tblpx}purchase_return LEFT JOIN {$tblpx}bills ON {$tblpx}purchase_return.bill_id = {$tblpx}bills.bill_id LEFT JOIN {$tblpx}purchase ON {$tblpx}bills.purchase_id = {$tblpx}purchase.p_id LEFT JOIN {$tblpx}projects ON {$tblpx}purchase.project_id = {$tblpx}projects.pid LEFT JOIN {$tblpx}clients ON {$tblpx}projects.client_id = {$tblpx}clients.cid WHERE (" . $newQuery5 . ") AND {$tblpx}purchase_return.return_date BETWEEN '" . $date_from . "' AND '" . $date_to . "' ORDER BY {$tblpx}purchase_return.return_date desc ");
		$purchase_returndata = $data15->queryAll();

		$data3 = Yii::app()->db->createCommand("SELECT DISTINCT sgst,cgst,igst FROM {$tblpx}inv_list LEFT JOIN {$tblpx}invoice ON {$tblpx}invoice.invoice_id ={$tblpx}inv_list.inv_id WHERE {$tblpx}invoice.date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND sgst >0 AND cgst > 0 AND {$tblpx}invoice.invoice_status ='saved' AND (" . $newQuery4 . ") GROUP BY sgst,cgst");
		$sales_head = $data3->queryAll();

		$data4 = Yii::app()->db->createCommand("SELECT DISTINCT igst FROM {$tblpx}inv_list LEFT JOIN {$tblpx}invoice ON {$tblpx}invoice.invoice_id ={$tblpx}inv_list.inv_id WHERE {$tblpx}invoice.date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND igst >0 AND {$tblpx}invoice.invoice_status ='saved' AND (" . $newQuery4 . ")  GROUP BY igst");
		$igst_head_sales = $data4->queryAll();

		$data16 = Yii::app()->db->createCommand("SELECT DISTINCT returnitem_cgstpercent,returnitem_sgstpercent,returnitem_igstpercent FROM {$tblpx}purchase_returnitem LEFT JOIN {$tblpx}purchase_return ON {$tblpx}purchase_return.return_id ={$tblpx}purchase_returnitem.return_id WHERE {$tblpx}purchase_return.return_date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND returnitem_cgstpercent > 0 AND returnitem_sgstpercent > 0 AND (" . $newQuery5 . ")  GROUP BY returnitem_cgstpercent,returnitem_sgstpercent");
		$return_head = $data16->queryAll();

		$data17 = Yii::app()->db->createCommand("SELECT DISTINCT returnitem_igstpercent FROM {$tblpx}purchase_returnitem LEFT JOIN {$tblpx}purchase_return ON {$tblpx}purchase_return.return_id ={$tblpx}purchase_returnitem.return_id WHERE {$tblpx}purchase_return.return_date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND returnitem_igstpercent >0 AND (" . $newQuery5 . ") GROUP BY returnitem_igstpercent");
		$return_headigst = $data17->queryAll();

		foreach ($sales_head as $key => $value) {
			$invoice_head[$key]['cgst'] = $value['cgst'];
			$invoice_head[$key]['sgst'] = $value['sgst'];
		}

		$q = count($sales_head) + 1;
		foreach ($return_head as $key => $value) {
			$invoice_head[$q]['cgst'] = $value['returnitem_cgstpercent'];
			$invoice_head[$q]['sgst'] = $value['returnitem_cgstpercent'];
			$q++;
		}

		foreach ($igst_head_sales as $key => $value) {
			$invoice_head_igst[$key]['igst'] = $value['igst'];
		}

		$r = count($igst_head_sales) + 1;
		foreach ($return_headigst as $key => $value) {
			$invoice_head_igst[$r]['igst'] = $value['returnitem_igstpercent'];
			$r++;
		}

		foreach ($sales_data as $key => $value) {
			$sales[$key]['client'] = $value['client'];
			$sales[$key]['date'] = $value['date'];
			$sales[$key]['inv_no'] = $value['inv_no'];
			$sales[$key]['return_no'] = '';
			$sales[$key]['gst_no'] = $value['gst_no'];
			$sales[$key]['totalamount'] = $value['amount'] + $value['tax_amount'];
			$sales[$key]['type'] = 'Invoice';
			$sales[$key]['id'] = $value['invoice_id'];
		}

		$p = count($sales_data) + 1;
		foreach ($purchase_returndata as $key => $value) {
			$sales[$p]['client'] = $value['name'];
			$sales[$p]['date'] = $value['return_date'];
			$sales[$p]['inv_no'] = '';
			$sales[$p]['return_no'] = $value['return_number'];
			$sales[$p]['gst_no'] = $value['gst_no'];
			$sales[$p]['totalamount'] = $value['return_totalamount'];
			$sales[$p]['type'] = 'Purchase Return';
			$sales[$p]['id'] = $value['return_id'];
			$p++;
		}


		$purchase_head_igst = array_map("unserialize", array_unique(array_map("serialize", $purchase_head_igst)));
		$purchase_head = array_map("unserialize", array_unique(array_map("serialize", $purchase_head)));
		$invoice_head = array_map("unserialize", array_unique(array_map("serialize", $invoice_head)));
		$invoice_head_igst = array_map("unserialize", array_unique(array_map("serialize", $invoice_head_igst)));

		$mPDF1 = Yii::app()->ePdf->mPDF();

		$mPDF1 = Yii::app()->ePdf->mPDF('', 'A4');
		$sql = 'SELECT template_name '
			. 'FROM jp_quotation_template '
			. 'WHERE status="1"';
		$selectedtemplate = Yii::app()->db->createCommand($sql)->queryScalar();

		$template = $this->getTemplate($selectedtemplate);
		$mPDF1->SetHTMLHeader($template['header']);
		$mPDF1->SetHTMLFooter($template['footer']);
		$mPDF1->AddPage('', '', '', '', '', 0, 0, 52, 30, 10, 0);
		$mPDF1->WriteHTML($this->renderPartial('taxreport_pdf', array(
			'purchase_head' => $purchase_head,
			'date_from' => $date_from,
			'date_to' => $date_to,
			'sales' => $sales,
			'sales_head' => $invoice_head,
			'purchase' => $purchase,
			'sales' => $sales,
			'igst_head_sales' => $invoice_head_igst,
			'purchase_head_igst' => $purchase_head_igst,
			'company_id' => $company_id,
		), true));

		$mPDF1->Output('Taxreport.pdf', 'D');
	}

	public function actionAjax()
	{
		echo json_encode('success');
	}

	public function actionGetItemCategory($parent = 0, $spacing = '', $user_tree_array = '')
	{

		if (!is_array($user_tree_array))
			$user_tree_array = array();
		$data = array();
		$final = array();
		$tblpx = Yii::app()->db->tablePrefix;
		$user = Users::model()->findByPk(Yii::app()->user->id);
		$arrVal = explode(',', $user->company_id);
		$newQuery = "";
		foreach ($arrVal as $arr) {
			if ($newQuery)
				$newQuery .= ' OR';
			$newQuery .= " FIND_IN_SET('" . $arr . "', company_id)";
		}
		$spec_sql = "SELECT id, specification, brand_id, cat_id "
			. " FROM {$tblpx}specification ";
		$specification = Yii::app()->db->createCommand($spec_sql)->queryAll();

		foreach ($specification as $key => $value) {
			$brand = '';
			if ($value['brand_id'] != NULL) {
				$brand_sql = "SELECT brand_name "
					. " FROM {$tblpx}brand "
					. " WHERE id=" . $value['brand_id'] . "";
				$brand_details = Yii::app()->db->createCommand($brand_sql)->queryRow();
				$brand = '-' . ucwords($brand_details['brand_name']);
			}

			if ($value['cat_id'] != "") {
				$result = $this->actionGetParent($value['cat_id']);
			}
			$final[$key]['data'] = ucwords($result['category_name']) . $brand . '-' . ucwords($value['specification']);
			$final[$key]['id'] = $value['id'];
		}
		//echo "<pre>";print_r($final);exit;
		return $final;
	}

	public function actionGetParent($id)
	{
		$tblpx = Yii::app()->db->tablePrefix;
		$category = Yii::app()->db->createCommand("SELECT parent_id, id , category_name FROM {$tblpx}purchase_category WHERE id=" . $id . "")->queryRow();
		return $category;
	}

	public function actiontestransaction()
	{
		$tblpx = Yii::app()->db->tablePrefix;
		$other = '';
		if (!isset($_POST['searchTerm'])) {
			$html = '';
			$specification = $this->actionGetItemCategory($parent = 0, $spacing = '', $user_tree_array = '');
			$other = array("id" => 'other', "text" => 'Other');
		} else {
			$html['html'] = '';
			$html['status'] = '';
			$html['unit'] = '';
			$html['hsn_code'] = '';
			$search = $_POST['searchTerm'];
			$length = strlen($search);
			$specification = $this->actionGetItemCategorySearch($parent = 0, $spacing = '', $user_tree_array = '', $search);
			if ($search != '' && $length >= 3) {
				if ($search == 'other') {
					$other = array("id" => 'other', "text" => 'Other');
				} else {
					$other = '';
				}

				if (!empty($specification)) {
					$i = 1;
					foreach ($specification as $key => $value) {
						$spec_sql = "SELECT id, cat_id, brand_id, specification, unit "
							. " FROM {$tblpx}specification "
							. " WHERE id=" . $value['id'] . "";
						$specificationd = Yii::app()->db->createCommand($spec_sql)->queryRow();
						if ($specificationd['brand_id'] != NULL) {
							$brand_sql = "SELECT brand_name "
								. " FROM {$tblpx}brand "
								. " WHERE id=" . $specificationd['brand_id'] . "";
							$brand_details = Yii::app()->db->createCommand($brand_sql)->queryRow();
							$brand = '-' . ucwords($brand_details['brand_name']);
						} else {
							$brand = '';
						}
						$cat_sql = "SELECT category_name "
							. " FROM {$tblpx}purchase_category "
							. " WHERE id=" . $specificationd['cat_id'] . "";
						$category = Yii::app()->db->createCommand($cat_sql)->queryRow();
						$hsn_sql = "SELECT hsn_code "
							. " FROM {$tblpx}specification "
							. " WHERE id=" . $specificationd['id'] . "";
						$hsn_code = Yii::app()->db->createCommand($hsn_sql)->queryRow();
						$select = "{$tblpx}billitem.billitem_taxamount, {$tblpx}billitem.billitem_discountpercent, {$tblpx}billitem.billitem_discountamount, {$tblpx}billitem.billitem_cgst, {$tblpx}billitem.billitem_cgstpercent, {$tblpx}billitem.billitem_sgst, {$tblpx}billitem.billitem_sgstpercent, {$tblpx}billitem.billitem_igst, {$tblpx}billitem.billitem_igstpercent";

						$datasql = "SELECT {$tblpx}bills.bill_number, "
							. " {$tblpx}bills.bill_date, "
							. " {$tblpx}billitem.billitem_quantity, "
							. " {$tblpx}billitem.billitem_unit,"
							. " {$tblpx}billitem.billitem_hsn_code, "
							. " {$tblpx}billitem.billitem_rate, "
							. " {$tblpx}billitem.billitem_amount, "
							. " {$tblpx}purchase.purchase_no, "
							. " {$tblpx}purchase.vendor_id, "
							. " {$tblpx}purchase.project_id, " . $select . " "
							. " FROM  {$tblpx}billitem "
							. " LEFT JOIN {$tblpx}bills  "
							. " ON {$tblpx}bills.bill_id={$tblpx}billitem.bill_id "
							. " LEFT JOIN {$tblpx}purchase_items "
							. " ON {$tblpx}billitem.purchaseitem_id={$tblpx}purchase_items.item_id "
							. " LEFT JOIN {$tblpx}purchase "
							. " ON {$tblpx}purchase_items.purchase_id= {$tblpx}purchase.p_id "
							. " WHERE {$tblpx}billitem.category_id=" . $value['id'] . " "
							. " ORDER BY {$tblpx}billitem.billitem_id "
							. " desc LIMIT 0,4";

						$data = Yii::app()->db->createCommand($datasql)->queryAll();
						if (!empty($data)) {
							if ($i == 1) {
								$html['html'] .= '<div class="alert alert-success get_prevdata alert-dismissible fade in" role="alert"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>';
								$html['html'] .= '<div id="parent2">
                                        <table style="width:100%;" id="pre_fixtable2">
                                        	<thead>
                                                <tr>
                                                    <th style="text-align:center;">Sl.No</th>
                                                    <th style="text-align:center;">Project</th>
                                                    <th style="text-align:center;">Bill No</th>
                                                    <th style="text-align:center;">Purchase No</th>
                                                    <th style="text-align:center;">Bill Date</th>
                                                    <th style="text-align:center;">Vendor</th>
                                                    <th style="text-align:center;">Quantity</th>
                                                    <th style="text-align:center;">Unit</th>
													<th style="text-align:center;">HSN Code </th>
                                                    <th style="text-align:center;">Rate</th>
                                                    <th style="text-align:center;">Amount</th>
                                                </tr>
                                            <thead>';
							}
							$html['html'] .= '<tbody><tr><td colspan="9"><p>' . ucwords($category['category_name']) . $brand . '-' . ucwords($specificationd['specification']) . ' Previous "' . count($data) . '" ​Purchase Rate</p></td></tr>
								';
							foreach ($data as $key => $value) {
								$vendor = Vendors::model()->findByPk($value['vendor_id']);
								$project = Projects::model()->findByPk($value['project_id']);

								$html['html'] .= '<tr class="getprevious" style="cursor:pointer;"  data-id="' . $specificationd['id'] . ',' . $value['billitem_quantity'] . ',' . $value['billitem_unit'] . ',' . $value['billitem_hsn_code'] . ',' . $value['billitem_rate'] . ',' . $value['billitem_amount'] . ',' . $value['billitem_taxamount'] . ', ' . $value['billitem_discountpercent'] . ', ' . $value['billitem_discountamount'] . ', ' . $value['billitem_cgst'] . ', ' . $value['billitem_cgstpercent'] . ', ' . $value['billitem_sgst'] . ', ' . $value['billitem_sgstpercent'] . ', ' . $value['billitem_igst'] . ', ' . $value['billitem_igstpercent'] . '"><td style="text-align:center;">' . ($key + 1) . ' </td><td style="text-align:center">' . (isset($project['name']) ? $project['name'] : '') . '</td><td style="text-align:center;">' . $value['bill_number'] . '</td><td style="text-align:center;">' . $value['purchase_no'] . '</td><td style="text-align:center;">' . date("Y-m-d", strtotime($value['bill_date'])) . '</td><td style="text-align:center;">' . (isset($vendor['name']) ? $vendor['name'] : '') . '</td><td style="text-align:center">' . $value['billitem_quantity'] . '</td><td style="text-align:center;">' . $value['billitem_unit'] . '</td><td  style="text-align:center">' . $value['billitem_hsn_code'] . '</td><td  style="text-align:center">' . $value['billitem_rate'] . '</td><td style="text-align:center;">' . $value['billitem_amount'] . '</td></tr>';
							}

							$html['status'] .= true;
							$i++;
						} else {
							$html['html'] .= '';
							$html['status'] .= false;
						}
						if (!empty($specificationd['unit'])) {
							$html['unit'] .= $specificationd['unit'];
						} else {
							$html['unit'] .= '';
						}
						if (!empty($hsn_code)) {
							$html['hsn_code'] .= $hsn_code['hsn_code'];
						} else {
							$html['hsn_code'] .= '';
						}
					}
					$html['html'] .= '</tbody></table></div>';
					$html['html'] .= '</div>';
				}
				$html = $html['html'];
			} else {
				$html = '';
				$other = array("id" => 'other', "text" => 'Other');
			}
		}
		$data = array();
		foreach ($specification as $key => $value) {
			$data[] = array("id" => $value['id'], "text" => $value['data']);
		}
		$data[] = $other;
		echo json_encode(array('data' => $data, 'html' => $html));
	}

	 public function actionGetItemCategorySearch($parent = 0, $spacing = '', $user_tree_array = '', $search)
    {
        if (!is_array($user_tree_array))
            $user_tree_array = array();
        $data = array();
        $final = array();
        $tblpx = Yii::app()->db->tablePrefix;
        $where = '';
        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        $newQuery = "";
        foreach ($arrVal as $arr) {
            if ($newQuery)
                $newQuery .= ' OR';
            $newQuery .= " FIND_IN_SET('" . $arr . "', company_id)";
        }


        $spec_sql = "SELECT id, specification, brand_id, cat_id "
            . " FROM {$tblpx}specification "
            . " WHERE  (" . $newQuery . ") "
            . " AND specification LIKE '%" . $search . "%' " . $where . " "
            . "  AND spec_status='1'";
        $specification = Yii::app()->db->createCommand($spec_sql)->queryAll();

        foreach ($specification as $key => $value) {
            if ($value['brand_id'] != NULL) {
                $brand_details = Yii::app()->db->createCommand("SELECT brand_name FROM {$tblpx}brand WHERE id=" . $value['brand_id'] . "")->queryRow();
                $brand = '-' . ucwords($brand_details['brand_name']);
            } else {
                $brand = '';
            }
            $result = $this->actionGetParent($value['cat_id']);
            $final[$key]['data'] = ucwords($result['category_name']) . $brand . '-' . ucwords($value['specification']);
            $final[$key]['id'] = $value['id'];
        }
        return $final;
    }

	public function actionGetunits()
	{
		$id = $_POST['id'];
		$tblpx = Yii::app()->db->tablePrefix;
		$spec_sql = "SELECT id, cat_id, brand_id, specification, unit FROM {$tblpx}specification WHERE id=" . $id . "";
		$specificationd = Yii::app()->db->createCommand($spec_sql)->queryRow();

		if (!empty($specificationd['unit'])) {
			$unit = $specificationd['unit'];
		} else {
			$unit = '';
		}
		echo json_encode($unit);
	}

	public function actionPrevioustransaction()
	{
		$tblpx = Yii::app()->db->tablePrefix;
		$html['html'] = '';
		$html['status'] = '';
		$html['unit'] = '';
		$html['hsn_code'] = '';
		$html['billitem_unit'] = '';
		$html['purchaseitem_unit'] = '';
		if ($_REQUEST['data'] != '' && $_REQUEST['data'] != 'other') {
			$spec_sql = "SELECT id, cat_id, brand_id, specification, unit,hsn_code "
				. " FROM {$tblpx}specification "
				. " WHERE id=" . $_REQUEST['data'] . "";
			$specification = Yii::app()->db->createCommand($spec_sql)->queryRow();

			if ($specification['brand_id'] != NULL) {
				$brand_sql = "SELECT brand_name FROM {$tblpx}brand WHERE id=" . $specification['brand_id'] . "";
				$brand_details = Yii::app()->db->createCommand($brand_sql)->queryRow();
				$brand = '-' . ucwords($brand_details['brand_name']);
			} else {
				$brand = '';
			}

			$cat_sql = "SELECT category_name "
				. " FROM {$tblpx}purchase_category "
				. " WHERE id=" . $specification['cat_id'] . "";
			$category = Yii::app()->db->createCommand($cat_sql)->queryRow();
			$unit_sql = "SELECT id, unit_name "
				. " FROM {$tblpx}unit "
				. " WHERE unit_name='" . $specification['unit'] . "'";
			$unit = Yii::app()->db->createCommand($unit_sql)->queryRow();

			$update_unit = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}billitem WHERE bill_id=" . $_REQUEST['bill_id'] . "")->queryRow();


			if (!empty($specification['hsn_code'])) {
				$hsn_code = $specification['hsn_code'];
			} else {
				$hsn_code = NULL;
			}

			$select = "{$tblpx}billitem.billitem_taxamount, {$tblpx}billitem.billitem_discountpercent, {$tblpx}billitem.billitem_discountamount, {$tblpx}billitem.billitem_cgst, {$tblpx}billitem.billitem_cgstpercent, {$tblpx}billitem.billitem_sgst, {$tblpx}billitem.billitem_sgstpercent, {$tblpx}billitem.billitem_igst, {$tblpx}billitem.billitem_igstpercent";

			$data = Yii::app()->db->createCommand("SELECT {$tblpx}bills.bill_number, {$tblpx}bills.bill_date, {$tblpx}billitem.billitem_quantity, {$tblpx}billitem.billitem_unit,{$tblpx}billitem.billitem_hsn_code, {$tblpx}billitem.billitem_rate, {$tblpx}billitem.billitem_amount, {$tblpx}purchase.purchase_no, {$tblpx}purchase.vendor_id, {$tblpx}purchase.project_id, " . $select . " FROM  {$tblpx}billitem LEFT JOIN {$tblpx}bills  ON {$tblpx}bills.bill_id={$tblpx}billitem.bill_id LEFT JOIN {$tblpx}purchase_items ON {$tblpx}billitem.purchaseitem_id={$tblpx}purchase_items.item_id LEFT JOIN {$tblpx}purchase ON {$tblpx}purchase_items.purchase_id= {$tblpx}purchase.p_id WHERE {$tblpx}billitem.category_id=" . $_REQUEST['data'] . " AND (jp_billitem.purchaseitem_id IS NOT NULL AND jp_billitem.purchaseitem_id <> 0) AND {$tblpx}purchase.purchase_type = 'O' ORDER BY {$tblpx}billitem.billitem_id desc LIMIT 0,4")->queryAll();

			if (!empty($data)) {
				$html['html'] .= '<div class="alert alert-success get_prevdata alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
			<div id="parent">
                            <table style="width:100%;" id="pre_fixtable">
                                <thead>
                                <tr>
                                    <th style="text-align:center;">Sl.No</th>
                                    <th style="text-align:center;">Project</th>
                                    <th style="text-align:center">Bill No</th>
                                    <th style="text-align:center;">Purchase No</th>
                                    <th style="text-align:center;">Bill Date</th>
                                    <th style="text-align:center;">Vendor</th>
                                    <th style="text-align:center;">Quantity</th>
                                    <th style="text-align:center;">Unit</th>
                                    <th style="text-align:center">HSN Code</th>
                                    <th style="text-align:center;">Rate</th>
                                    <th style="text-align:center;">Amount</th>
                                </tr>
                                <thead><tbody><tr><td colspan="9"><p>' . ucwords($category['category_name']) . $brand . '-' . ucwords($specification['specification']) . ' Previous "' . count($data) . '" ​Purchase Rate</p></td></tr>';
				foreach ($data as $key => $value) {
					$project = Projects::model()->findByPk($value['project_id']);
					$vendor = Vendors::model()->findByPk($value['vendor_id']);

					$html['html'] .= '<tr class="getprevious" style="cursor:pointer;" data-id="' . $_REQUEST['data'] . ',' . $value['billitem_quantity'] . ',' . $value['billitem_unit'] . ',' . $value['billitem_hsn_code'] . ',' . $value['billitem_rate'] . ',' . $value['billitem_amount'] . ',' . $value['billitem_taxamount'] . ', ' . $value['billitem_discountpercent'] . ', ' . $value['billitem_discountamount'] . ', ' . $value['billitem_cgst'] . ', ' . $value['billitem_cgstpercent'] . ', ' . $value['billitem_sgst'] . ', ' . $value['billitem_sgstpercent'] . ', ' . $value['billitem_igst'] . ', ' . $value['billitem_igstpercent'] . '"><td style="text-align:center;">' . ($key + 1) . ' </td><td style="text-align:center;">' . (isset($project['name']) ? $project['name'] : '') . '</td><td style="text-align:center;">' . $value['bill_number'] . '</td><td style="text-align:center;">' . $value['purchase_no'] . '</td><td style="text-align:center;">' . date("Y-m-d", strtotime($value['bill_date'])) . '</td><td style="text-align:center;">' . (isset($vendor['name']) ? $vendor['name'] : '') . '</td><td style="text-align:center">' . $value['billitem_quantity'] . '</td><td style="text-align:center;">' . $value['billitem_unit'] . '</td><td style="text-align:center;">' . $value['billitem_hsn_code'] . '</td><td  style="text-align:center">' . $value['billitem_rate'] . '</td><td style="text-align:center;">' . $value['billitem_amount'] . '</td></tr>';
				}
				$html['html'] .= '</tbody></table></div>';
				$html['status'] = true;
			} else {
				$html['html'] .= '';
				$html['status'] .= false;
			}
			if (!empty($unit)) {
				$html['unit'] .= $unit['unit_name'];
				$html['billitem_unit'] .= $update_unit['billitem_unit'];
				$html['purchaseitem_unit'] .= $update_unit['purchaseitem_unit'];
			} else {
				$html['unit'] .= '';
			}
			if (!empty($hsn_code)) {
				$html['hsn_code'] .= $hsn_code;
			} else {
				$html['hsn_code'] .= '';
			}
		} else {

			$html['html'] .= '';
			$html['status'] .= false;
		}
		echo json_encode($html);
	}


	public function actionpurchasebillreport()
	{



		$model = new Bills;
		$tblpx = Yii::app()->db->tablePrefix;
		$user = Users::model()->findByPk(Yii::app()->user->id);
		$specification = $this->actionGetItemCategory($parent = 0, $spacing = '', $user_tree_array = '');
		$expense_type = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}expense_type ORDER BY type_name")->queryAll();
		$arrVal = explode(',', $user->company_id);
		$newQuery = "";
		foreach ($arrVal as $arr) {
			if ($newQuery)
				$newQuery .= ' OR';
			$newQuery .= " FIND_IN_SET('" . $arr . "', bill.company_id)";
		}
		$where = ' where 1=1 ';
		$date_from = '';
		$date_to = '';
		$pro_id = '';
		$vendor_id = '';
		$pu_num = '';
		$bill_num = '';
		$company_id = '';
		$item_spec = '';
		$exp_head = '';


		if (isset($_REQUEST['Bills'])) {

			// print_r($_REQUEST['Bills']);exit;

			$date_from = $_REQUEST['Bills']['date_from'];
			$date_to = $_REQUEST['Bills']['date_to'];
			$pro_id = $_REQUEST['Bills']['project'];
			$vendor_id = $_REQUEST['Bills']['vendor'];
			$pu_num = $_REQUEST['Bills']['pu_num'];
			$bill_num = $_REQUEST['Bills']['bill_num'];
			$company_id = $_REQUEST['company_id'];
			$item_spec = '';
			if (!empty($_REQUEST['category_id'])) {
				$category_id = $_REQUEST['category_id'];
				$item_spec = implode(",", $category_id);
			}
			$exp_head = '';
			if (!empty($_REQUEST['expense_head'])) {
				$expense_head = $_REQUEST['expense_head'];
				$exp_head = implode(",", $expense_head);
			}
			if ($date_from != '' && $date_to != '') {
				$where .= " and date(bill_date) BETWEEN '" . $date_from . "' and '" . $date_to . "' ";
			}
			if ($pro_id != '') {
				$where .= ' and  pro.pid = ' . $pro_id;
			}
			if ($vendor_id != '') {
				$where .= ' and ven.vendor_id = ' . $vendor_id;
			}
			if ($pu_num != '') {
				$where .= " and purchase_no = '" . $pu_num . "' ";
			}
			if ($bill_num != '') {
				$where .= "and  bill_number = '" . $bill_num . "' ";
			}
			if (!empty($date_from)) {
				$where .= " AND date(bill_date) >= '" . $date_from . "'";
			}
			if (!empty($date_to)) {
				$where .= " AND date(bill_date) <= '" . $date_to . "'";
			}

			if (!empty($company_id)) {
				$where .= " AND bill.company_id=" . $company_id . "";
			} else {
				$where .= " AND (" . $newQuery . ")";
			}
			if (!empty($category_id)) {
				$where .= " AND item.category_id IN (" . $item_spec . ")";
			}
			if (!empty($expense_head)) {
				$where .= " AND exp_head.type_id IN (" . $exp_head . ")";
			}
		} else {
			$date_from = date("Y-m-") . "01";
			$date_to = date("Y-m-d");
			if (!empty($date_from)) {
				$where .= " AND date(bill_date) >= '" . $date_from . "'";
			}
			if (!empty($date_to)) {
				$where .= " AND date(bill_date) <= '" . $date_to . "'";
			}
		}
		Yii::app()->db->createCommand('SET SQL_BIG_SELECTS=1')->execute();
		$sql = "SELECT pro.pid,bill.bill_id,bill_number,bill_date,bill_totalamount, p_id,purchase_no,ven.name as vendorname,pro.name as projectname,paid,bill.company_id FROM {$tblpx}bills as bill left join {$tblpx}purchase as pu on pu.p_id = bill.purchase_id LEFT join {$tblpx}vendors as ven on ven.vendor_id = pu.vendor_id LEFT join {$tblpx}projects as pro on pro.pid = pu.project_id LEFT join  {$tblpx}expenses as exp on exp.bill_id = bill.`bill_id` LEFT JOIN {$tblpx}billitem as item ON bill.bill_id=item.bill_id LEFT JOIN {$tblpx}expense_type as exp_head ON pu.expensehead_id=exp_head.type_id $where group by bill_number order by bill.bill_id DESC";



		$model = Yii::app()->db->createCommand($sql)->queryAll();
		$dataProvider1 = new CSqlDataProvider($sql, array(
			'keyField' => 'bill_id',
			'id' => 'bill_id',
			/*'sort'=>array(
								 'attributes'=>array(
									 'exp_id', 'name',
								 ),
							 ),*/
			'totalItemCount' => count($model),
			/* 'pagination'=>array(

							 'pageSize'=>10,
							 'itemCount'=>count($model),
						  ),*/
			'pagination' => false,
		));

		//	echo $item_spec;

		$this->render('purchaseitemsreport', array(
			'model' => $model,
			'dataProvider' => $dataProvider1,
			'date_from' => $date_from,
			'date_to' => $date_to,
			'pro_id' => $pro_id,
			'vendor_id' => $vendor_id,
			'pu_num' => $pu_num,
			'bill_num' => $bill_num,
			'company_id' => $company_id,
			'specification' => $specification,
			'item_spec' => $item_spec,
			'expense_type' => $expense_type,
			'exp_head' => $exp_head,
		));
	}


	public function actionpurchasetopdf($date_from, $date_to, $pro_id, $vendor_id, $pu_num, $bill_num, $company_id, $item_spec, $exp_head)
	{
		$where = "WHERE 1=1";
		$user = Users::model()->findByPk(Yii::app()->user->id);
		$arrVal = explode(',', $user->company_id);
		$newQuery = "";
		foreach ($arrVal as $arr) {
			if ($newQuery)
				$newQuery .= ' OR';
			$newQuery .= " FIND_IN_SET('" . $arr . "', bill.company_id)";
		}
		if ($date_from != '' && $date_to != '') {
			$where .= " and date(bill_date) BETWEEN '" . $date_from . "' and '" . $date_to . "' ";
		}
		if (!empty($date_from)) {
			$where .= " AND date(bill_date) >= '" . $date_from . "'";
		}
		if (!empty($date_to)) {
			$where .= " AND date(bill_date) <= '" . $date_to . "'";
		}
		if ($pro_id != '') {
			$where .= ' and  pro.pid = ' . $pro_id;
		}
		if ($vendor_id != '') {
			$where .= ' and ven.vendor_id = ' . $vendor_id;
		}
		if ($pu_num != '') {
			$where .= ' and purchase_no = ' . $pu_num;
		}
		if ($bill_num != '') {
			$where .= "and  bill_number = '" . $bill_num . "' ";
		}
		if (!empty($company_id)) {
			$where .= " AND bill.company_id=" . $company_id . "";
		} else {
			$where .= " AND (" . $newQuery . ")";
		}
		if (isset($item_spec) && $item_spec != '') {
			$where .= " AND item.category_id IN (" . $item_spec . ")";
		}
		if (isset($exp_head) && $exp_head != '') {
			$where .= " AND exp_head.type_id IN (" . $exp_head . ")";
		}

		$tblpx = Yii::app()->db->tablePrefix;
		// $sql = "SELECT pro.pid,bill.bill_id,bill_number,bill_date,bill_totalamount,purchase_no,ven.name as vendorname,pro.name as projectname,paid,bill.company_id FROM {$tblpx}bills as bill left join {$tblpx}purchase as pu on pu.p_id = bill.purchase_id LEFT join {$tblpx}vendors as ven on ven.vendor_id = pu.vendor_id LEFT join {$tblpx}projects as pro on pro.pid = pu.project_id LEFT join  {$tblpx}expenses as exp on exp.bill_id = bill.`bill_id` ".$where." group by bill_number order by bill.bill_id DESC";
		Yii::app()->db->createCommand('SET SQL_BIG_SELECTS=1')->execute();
		$sql = "SELECT pro.pid,bill.bill_id,bill_number,bill_date,bill_totalamount, p_id,purchase_no,ven.name as vendorname,pro.name as projectname,paid,bill.company_id FROM {$tblpx}bills as bill left join {$tblpx}purchase as pu on pu.p_id = bill.purchase_id LEFT join {$tblpx}vendors as ven on ven.vendor_id = pu.vendor_id LEFT join {$tblpx}projects as pro on pro.pid = pu.project_id LEFT join  {$tblpx}expenses as exp on exp.bill_id = bill.`bill_id` LEFT JOIN {$tblpx}billitem as item ON bill.bill_id=item.bill_id LEFT JOIN {$tblpx}expense_type as exp_head ON pu.expensehead_id=exp_head.type_id $where group by bill_number order by bill.bill_id DESC";


		$model = Yii::app()->db->createCommand($sql)->queryAll();

		$this->logo = $this->realpath_logo;
		$mPDF1 = Yii::app()->ePdf->mPDF();
		$mPDF1 = Yii::app()->ePdf->mPDF('', 'A4');
		$sql = 'SELECT template_name '
			. 'FROM jp_quotation_template '
			. 'WHERE status="1"';
		$selectedtemplate = Yii::app()->db->createCommand($sql)->queryScalar();

		$template = $this->getTemplate($selectedtemplate);
		$mPDF1->SetHTMLHeader($template['header']);
		$mPDF1->SetHTMLFooter($template['footer']);
		$mPDF1->AddPage('', '', '', '', '', 10, 10, 52, 30, 10, 0);
		$mPDF1->SetAutoPageBreak(true, 40);
		$file = $mPDF1->WriteHTML($this->renderPartial('purchasetopdf', array('model' => $model, 'company_id' => $company_id, ), true));
		$filename = "purchase_bill_report";
		$mPDF1->Output($filename . '.pdf', 'D');
	}

	public function actionpurchasetoexcel($date_from, $date_to, $pro_id, $vendor_id, $pu_num, $bill_num, $company_id, $item_spec, $exp_head)
	{

		$where = "WHERE 1=1";
		$user = Users::model()->findByPk(Yii::app()->user->id);
		$arrVal = explode(',', $user->company_id);
		$newQuery = "";
		foreach ($arrVal as $arr) {
			if ($newQuery)
				$newQuery .= ' OR';
			$newQuery .= " FIND_IN_SET('" . $arr . "', bill.company_id)";
		}
		if ($date_from != '' && $date_to != '') {
			$where .= " and date(bill_date) BETWEEN '" . $date_from . "' and '" . $date_to . "' ";
		}
		if (!empty($date_from)) {
			$where .= " AND date(bill_date) >= '" . $date_from . "'";
		}
		if (!empty($date_to)) {
			$where .= " AND date(bill_date) <= '" . $date_to . "'";
		}
		if ($pro_id != '') {
			$where .= ' and  pro.pid = ' . $pro_id;
		}
		if ($vendor_id != '') {
			$where .= ' and ven.vendor_id = ' . $vendor_id;
		}
		if ($pu_num != '') {
			$where .= ' and purchase_no = ' . $pu_num;
		}
		if ($bill_num != '') {
			$where .= "and  bill_number = '" . $bill_num . "' ";
		}
		if (!empty($company_id)) {
			$where .= " AND bill.company_id=" . $company_id . "";
		} else {
			$where .= " AND (" . $newQuery . ")";
		}

		if (isset($item_spec) && $item_spec != '') {
			$where .= " AND item.category_id IN (" . $item_spec . ")";
		}
		if (isset($exp_head) && $exp_head != '') {
			$where .= " AND exp_head.type_id IN (" . $exp_head . ")";
		}
		$tblpx = Yii::app()->db->tablePrefix;
		$arraylabel = array('sl no', 'Purchase NO', 'Bill no', 'Company', 'Vendor', 'Project', 'Bill Date', 'Total Amount');



		// $sql1= "SELECT pro.pid,bill.bill_id,bill_number,bill_date,bill_totalamount,purchase_no,ven.name as vendorname,pro.name as projectname,paid,bill.company_id FROM {$tblpx}bills as bill left join {$tblpx}purchase as pu on pu.p_id = bill.purchase_id LEFT join {$tblpx}vendors as ven on ven.vendor_id = pu.vendor_id LEFT join {$tblpx}projects as pro on pro.pid = pu.project_id LEFT join  {$tblpx}expenses as exp on exp.bill_id = bill.`bill_id` ".$where." group by bill_number order by  bill.bill_id DESC ";
		Yii::app()->db->createCommand('SET SQL_BIG_SELECTS=1')->execute();
		$sql1 = "SELECT pro.pid,bill.bill_id,bill_number,bill_date,bill_totalamount, p_id,purchase_no,ven.name as vendorname,pro.name as projectname,paid,bill.company_id FROM {$tblpx}bills as bill left join {$tblpx}purchase as pu on pu.p_id = bill.purchase_id LEFT join {$tblpx}vendors as ven on ven.vendor_id = pu.vendor_id LEFT join {$tblpx}projects as pro on pro.pid = pu.project_id LEFT join  {$tblpx}expenses as exp on exp.bill_id = bill.`bill_id` LEFT JOIN {$tblpx}billitem as item ON bill.bill_id=item.bill_id LEFT JOIN {$tblpx}expense_type as exp_head ON pu.expensehead_id=exp_head.type_id $where group by bill_number order by bill.bill_id DESC";

		$model = Yii::app()->db->createCommand($sql1)->queryAll();
		/************************* */
		$tot_billamount = 0;
		$tot_paidamount = 0;
		$balance = 0;
		$tot_bal = 0;
		foreach ($model as $key => $data) {

			$balance = $data['bill_totalamount'] - $data['paid'];
			$tot_billamount += $data['bill_totalamount'];
			$tot_paidamount += $data['paid'];
			$tot_bal += $balance;
		}
		$finaldata = array();
		$finaldata[0][] = '';
		$finaldata[0][] = '';
		$finaldata[0][] = '';
		$finaldata[0][] = '';
		$finaldata[0][] = '';
		$finaldata[0][] = '';
		$finaldata[0][] = 'Total';
		$finaldata[0][] = ($tot_billamount != 0) ? Controller::money_format_inr($tot_billamount, 2, 1) : 0;
		/********************************************* */
		$tot_billamount = 0;
		$tot_paidamount = 0;
		$balance = 0;
		$tot_bal = 0;


		foreach ($model as $key => $data) {

			$balance = $data['bill_totalamount'] - $data['paid'];
			$tot_billamount += $data['bill_totalamount'];
			$tot_paidamount += $data['paid'];
			$tot_bal += $balance;
			$company = Company::model()->findByPk($data['company_id']);
			$finaldata[$key + 1][] = $key + 1;
			$finaldata[$key + 1][] = $data['purchase_no'];
			$finaldata[$key + 1][] = $data['bill_number'];
			$finaldata[$key + 1][] = $company->name;
			$finaldata[$key + 1][] = $data['vendorname'];
			$finaldata[$key + 1][] = $data['projectname'];
			$finaldata[$key + 1][] = date('d-m-Y', strtotime($data['bill_date']));
			$finaldata[$key + 1][] = Controller::money_format_inr($data['bill_totalamount'], 2, 1);
			//            $finaldata[$key][] =  Controller::money_format_inr($data['paid'],2,1);
			//            $finaldata[$key][] =   Controller::money_format_inr($balance,2,1);

		}


		//    $finaldata[$key+1][] = '';
		//    $finaldata[$key+1][] = '';
		//    $finaldata[$key+1][] = '';
		//    $finaldata[$key+1][] = '';
		//    $finaldata[$key+1][] = '';
		//    $finaldata[$key+1][] = '';
		//    $finaldata[$key+1][] = 'Total';
		//    $finaldata[$key+1][] = ($tot_billamount !=0)?Controller::money_format_inr($tot_billamount,2,1):0;
		//       $finaldata[$key+1][] = ($tot_paidamount !=0)?Controller::money_format_inr($tot_paidamount,2,1):0;
		//       $finaldata[$key+1][] = ($tot_bal !=0)?Controller::money_format_inr($tot_bal,2,1):0;



		Yii::import('ext.ECSVExport');
		$csv = new ECSVExport($finaldata);
		$csv->setHeaders($arraylabel);
		$csv->setToAppend();
		$contentdata = $csv->toCSV();
		$csvfilename = 'credit_debit_report' . date('d_M_Y') . '.csv';
		Yii::app()->getRequest()->sendFile($csvfilename, $contentdata, "text/csv", false);
		exit();
	}

	public function actiondynamicpurchase()
	{
		$company_id = $_POST['company_id'];
		$html = '';
		$html = array('html' => '', 'status' => 'no_success');

		$command = Yii::app()->db->createCommand();
		$command->select('p.p_id,p.purchase_no, '
			. '(CASE e.purchase_type WHEN 3 THEN "PARTIALLY BILLED" '
			. 'WHEN 2 THEN "FULLY BILLED"  '
			. 'ELSE "NOT BILLED" END) As purchase_type');
		$command->from($this->tableNameAcc('purchase', 0) . ' as p');
		$command->leftJoin($this->tableNameAcc('bills', 0) . ' as b', 'p.p_id=b.purchase_id ');
		$command->leftJoin($this->tableNameAcc('expenses', 0) . ' as e', 'e.bill_id=b.bill_id ');
		$command->where(' p.purchase_status = "saved" '
			. 'AND p.purchase_no IS NOT NULL '
			. 'AND p.company_id =' . $company_id);
		$command->order('p.p_id');

		$purchase = $command->queryAll();
		$data = CHtml::listData($purchase, 'p_id', 'purchase_no', 'purchase_type');

		if (!empty($data)) {
			$html['html'] = CHtml::dropDownList(
				'',
				'purchase_id',
				$data,
				array('empty' => '-Select Purchase-')
			);
			$html['status'] = 'success';
		}

		echo json_encode($html);
	}

	public function actiondynamicproject()
	{
		$company_id = $_POST['company_id'];
		$tblpx = Yii::app()->db->tablePrefix;
		$html = '';
		// $html['html'] = '';
		// $html['status'] = '';
		$html = array('html' => '', 'status' => '');
		$expense_type = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}projects WHERE FIND_IN_SET(" . $company_id . ",company_id)")->queryAll();
		if (!empty($expense_type)) {
			$html['html'] = '<option value="">Select Project</option>';
			foreach ($expense_type as $key => $value) {
				$html['html'] .= '<option value="' . $value['pid'] . '">' . $value['name'] . '</option>';
				$html['status'] = 'success';
			}
		} else {
			$html['status'] = 'no_success';
			$html['html'] .= '<option value="">Select Project</option>';
		}
		echo json_encode($html);
	}
	public function actionTdsreport()
	{
		$tblpx = Yii::app()->db->tablePrefix;
		$model = new Bills();
		if (isset($_POST['date_from']) && isset($_POST['date_to']) && !empty($_POST['date_from']) && !empty($_POST['date_to'])) {
			$date_from = date('Y-m-d', strtotime($_POST['date_from']));
			$date_to = date('Y-m-d', strtotime($_POST['date_to']));
		} else if (isset($_POST['date_from']) && isset($_POST['date_to']) && !empty($_POST['date_from']) && empty($_POST['date_to'])) {
			$date_from = date('Y-m-d', strtotime($_POST['date_from']));
			$date_to = "";
		} else if (isset($_POST['date_from']) && isset($_POST['date_to']) && empty($_POST['date_from']) && !empty($_POST['date_to'])) {
			$date_from = "";
			$date_to = date('Y-m-d', strtotime($_POST['date_to']));
		} else {
			$date_from = "";
			$date_to = "";
		}
		$pid = array();
		$paymentids = '';
		$subcon = '';

		if (isset($_POST["company_id"]) && !empty($_POST["company_id"]))
			$company_id = $_POST["company_id"];
		else
			$company_id = "";
		if (isset($_POST["vendor_id"]) && !empty($_POST["vendor_id"]))
			$vendor_id = $_POST["vendor_id"];
		else
			$vendor_id = "";
		if (isset($_POST['subcontractor_id']) && !empty($_POST['subcontractor_id'])) {
			$paymentid = Yii::app()->db->createCommand("SELECT payment_id FROM {$tblpx}subcontractor_payment WHERE subcontractor_id = " . $_POST['subcontractor_id'] . "")->queryAll();
			$subcon = $_POST['subcontractor_id'];
			if (!empty($paymentid)) {
				foreach ($paymentid as $id) {
					$pid[] = $id["payment_id"];
				}
			}
			if (!empty($pid)) {
				$paymentids = implode(", ", $pid);
			}

		} else {
			$paymentids = "";
			$subcon = "";
		}

		$tdsdata = array();
		$daybook = new Expenses;
		$daybook->unsetAttributes();
		$daybook->fromdate = $date_from;
		$daybook->todate = $date_to;
		$daybook->company_id = $company_id;
		$daybook->vendor_id = $vendor_id;
		$daybook->paymentids = $paymentids;

		$vendorpayment = new Dailyvendors;
		$vendorpayment->unsetAttributes();
		$vendorpayment->fromdate = $date_from;
		$vendorpayment->todate = $date_to;
		$vendorpayment->vendor_id = $vendor_id;
		$vendorpayment->company_id = $company_id;

		$daybookdata = $daybook->tdssearch();
		$vendordata = $vendorpayment->tdssearch();
		$i = 0;
		foreach ($daybookdata->getData() as $data) {
			$tdsdata[$i]["project"] = ($data->projectid) ? $data->project->name : "";
			$tdsdata[$i]["date"] = ($data->date) ? date("d-m-Y", strtotime($data->date)) : "";
			$tdsdata[$i]["vendor"] = ($data->vendor_id) ? $data->vendor->name : "";
			if (isset($data->subcontractor->subcontractor_id)) {
				$subcontractor_id = $data->subcontractor->subcontractor_id;
				if ($subcontractor_id) {
					$subcontractor = Subcontractor::model()->findByPk($subcontractor_id);
					$tdsdata[$i]["subcontractor"] = $subcontractor->subcontractor_name;
				} else {
					$tdsdata[$i]["subcontractor"] = "";
				}
			} else {
				$tdsdata[$i]["subcontractor"] = "";
			}
			$tdsdata[$i]["amount"] = $data->amount;
			$tdsdata[$i]["paid"] = $data->paid;
			$tdsdata[$i]["tdsp"] = $data->expense_tdsp;
			$tdsdata[$i]["tdsamount"] = $data->expense_tds;
			$tdsdata[$i]["balance"] = $data->paidamount;
			$i++;
		}
		if (empty($paymentids)) {
			foreach ($vendordata->getData() as $data) {
				$tdsdata[$i]["project"] = ($data->project_id) ? $data->project->name : "";
				$tdsdata[$i]["date"] = ($data->date) ? date("d-m-Y", strtotime($data->date)) : "";
				$tdsdata[$i]["vendor"] = ($data->vendor_id) ? $data->vendor->name : "";
				$tdsdata[$i]["subcontractor"] = "";
				$tdsdata[$i]["amount"] = $data->amount + $data->tax_amount;
				$tdsdata[$i]["paid"] = $data->amount + $data->tax_amount;
				$tdsdata[$i]["tdsp"] = $data->tds;
				$tdsdata[$i]["tdsamount"] = $data->tds_amount;
				$tdsdata[$i]["balance"] = $data->paidamount;
				$i++;
			}
		}
		usort($tdsdata, array($this, 'sortByDate'));

		$this->render('tdsreport', array(
			'model' => $model,
			'date_from' => $date_from,
			'date_to' => $date_to,
			'company_id' => $company_id,
			'vendor_id' => $vendor_id,
			'subcontractor_id' => $subcon,
			'tdsdata' => $tdsdata,
		));
	}
	public function actionTdstopdf()
	{
		$tblpx = Yii::app()->db->tablePrefix;
		$model = new Bills();
		if (!empty($_REQUEST['fromdate']) && !empty($_REQUEST['todate'])) {
			$date_from = date('Y-m-d', strtotime($_REQUEST['fromdate']));
			$date_to = date('Y-m-d', strtotime($_REQUEST['todate']));
		} else if (!empty($_REQUEST['fromdate']) && empty($_REQUEST['todate'])) {
			$date_from = date('Y-m-d', strtotime($_REQUEST['fromdate']));
			$date_to = "";
		} else if (empty($_REQUEST['fromdate']) && !empty($_REQUEST['todate'])) {
			$date_from = "";
			$date_to = date('Y-m-d', strtotime($_REQUEST['todate']));
		} else {
			$date_from = "";
			$date_to = "";
		}
		if (!empty($_REQUEST["company_id"]))
			$company_id = $_REQUEST["company_id"];
		else
			$company_id = "";
		if (!empty($_REQUEST["vendor_id"]))
			$vendor_id = $_REQUEST["vendor_id"];
		else
			$vendor_id = "";
		if (!empty($_REQUEST['subcontractor_id'])) {
			$paymentid = Yii::app()->db->createCommand("SELECT payment_id FROM {$tblpx}subcontractor_payment WHERE subcontractor_id = " . $_REQUEST['subcontractor_id'] . "")->queryAll();
			$subcon = $_REQUEST['subcontractor_id'];
			foreach ($paymentid as $id) {
				$pid[] = $id["payment_id"];
			}
			$paymentids = implode(", ", $pid);
		} else {
			$paymentids = "";
			$subcon = "";
		}
		$tdsdata = array();
		$daybook = new Expenses;
		$daybook->unsetAttributes();
		$daybook->fromdate = $date_from;
		$daybook->todate = $date_to;
		$daybook->company_id = $company_id;
		$daybook->vendor_id = $vendor_id;
		$daybook->paymentids = $paymentids;

		$vendorpayment = new Dailyvendors;
		$vendorpayment->unsetAttributes();
		$vendorpayment->fromdate = $date_from;
		$vendorpayment->todate = $date_to;
		$vendorpayment->vendor_id = $vendor_id;
		$vendorpayment->company_id = $company_id;

		$daybookdata = $daybook->tdssearch();
		$vendordata = $vendorpayment->tdssearch();
		$i = 0;
		foreach ($daybookdata->getData() as $data) {
			$tdsdata[$i]["project"] = ($data->projectid) ? $data->project->name : "";
			$tdsdata[$i]["date"] = ($data->date) ? date("d-m-Y", strtotime($data->date)) : "";
			$tdsdata[$i]["vendor"] = ($data->vendor_id) ? $data->vendor->name : "";
			if (isset($data->subcontractor->subcontractor_id)) {
				$subcontractor_id = $data->subcontractor->subcontractor_id;
				if ($subcontractor_id) {
					$subcontractor = Subcontractor::model()->findByPk($subcontractor_id);
					$tdsdata[$i]["subcontractor"] = $subcontractor->subcontractor_name;
				} else {
					$tdsdata[$i]["subcontractor"] = "";
				}
			} else {
				$tdsdata[$i]["subcontractor"] = "";
			}
			$tdsdata[$i]["amount"] = $data->amount;
			$tdsdata[$i]["paid"] = $data->paid;
			$tdsdata[$i]["tdsp"] = $data->expense_tdsp;
			$tdsdata[$i]["tdsamount"] = $data->expense_tds;
			$tdsdata[$i]["balance"] = $data->paidamount;
			$i++;
		}
		if (empty($paymentids)) {
			foreach ($vendordata->getData() as $data) {
				$tdsdata[$i]["project"] = ($data->project_id) ? $data->project->name : "";
				$tdsdata[$i]["date"] = ($data->date) ? date("d-m-Y", strtotime($data->date)) : "";
				$tdsdata[$i]["vendor"] = ($data->vendor_id) ? $data->vendor->name : "";
				$tdsdata[$i]["subcontractor"] = "";
				$tdsdata[$i]["amount"] = $data->amount + $data->tax_amount;
				$tdsdata[$i]["paid"] = $data->amount + $data->tax_amount;
				$tdsdata[$i]["tdsp"] = $data->tds;
				$tdsdata[$i]["tdsamount"] = $data->tds_amount;
				$tdsdata[$i]["balance"] = $data->paidamount;
				$i++;
			}
		}
		usort($tdsdata, array($this, 'sortByDate'));

		$mPDF1 = Yii::app()->ePdf->mPDF();
		$mPDF1 = Yii::app()->ePdf->mPDF('', 'A4');
		$mPDF1->WriteHTML($this->renderPartial('tdsreportpdf', array(
			'model' => $model,
			'date_from' => $date_from,
			'date_to' => $date_to,
			'company_id' => $company_id,
			'vendor_id' => $vendor_id,
			'subcontractor_id' => $subcon,
			'tdsdata' => $tdsdata,
		), true));

		$mPDF1->Output('TDS_Report_' . strtotime(date("Y-m-d H:i:s")) . '.pdf', 'D');
	}
	public function actionTdstoexcel()
	{
		$tblpx = Yii::app()->db->tablePrefix;
		$model = new Bills();
		if (!empty($_REQUEST['fromdate']) && !empty($_REQUEST['todate'])) {
			$date_from = date('Y-m-d', strtotime($_REQUEST['fromdate']));
			$date_to = date('Y-m-d', strtotime($_REQUEST['todate']));
		} else if (!empty($_REQUEST['fromdate']) && empty($_REQUEST['todate'])) {
			$date_from = date('Y-m-d', strtotime($_REQUEST['fromdate']));
			$date_to = "";
		} else if (empty($_REQUEST['fromdate']) && !empty($_REQUEST['todate'])) {
			$date_from = "";
			$date_to = date('Y-m-d', strtotime($_REQUEST['todate']));
		} else {
			$date_from = "";
			$date_to = "";
		}
		if (!empty($_REQUEST["company_id"]))
			$company_id = $_REQUEST["company_id"];
		else
			$company_id = "";
		if (!empty($_REQUEST["vendor_id"]))
			$vendor_id = $_REQUEST["vendor_id"];
		else
			$vendor_id = "";
		if (!empty($_REQUEST['subcontractor_id'])) {
			$paymentid = Yii::app()->db->createCommand("SELECT payment_id FROM {$tblpx}subcontractor_payment WHERE subcontractor_id = " . $_REQUEST['subcontractor_id'] . "")->queryAll();
			$subcon = $_REQUEST['subcontractor_id'];
			foreach ($paymentid as $id) {
				$pid[] = $id["payment_id"];
			}
			$paymentids = implode(", ", $pid);
		} else {
			$paymentids = "";
			$subcon = "";
		}
		$tdsdata = array();
		$daybook = new Expenses;
		$daybook->unsetAttributes();
		$daybook->fromdate = $date_from;
		$daybook->todate = $date_to;
		$daybook->company_id = $company_id;
		$daybook->vendor_id = $vendor_id;
		$daybook->paymentids = $paymentids;

		$vendorpayment = new Dailyvendors;
		$vendorpayment->unsetAttributes();
		$vendorpayment->fromdate = $date_from;
		$vendorpayment->todate = $date_to;
		$vendorpayment->vendor_id = $vendor_id;
		$vendorpayment->company_id = $company_id;

		$daybookdata = $daybook->tdssearch();
		$vendordata = $vendorpayment->tdssearch();
		$i = 0;
		foreach ($daybookdata->getData() as $data) {
			$tdsdata[$i]["project"] = ($data->projectid) ? $data->project->name : "";
			$tdsdata[$i]["date"] = ($data->date) ? date("d-m-Y", strtotime($data->date)) : "";
			$tdsdata[$i]["vendor"] = ($data->vendor_id) ? $data->vendor->name : "";
			if (isset($data->subcontractor->subcontractor_id)) {
				$subcontractor_id = $data->subcontractor->subcontractor_id;
				if ($subcontractor_id) {
					$subcontractor = Subcontractor::model()->findByPk($subcontractor_id);
					$tdsdata[$i]["subcontractor"] = $subcontractor->subcontractor_name;
				} else {
					$tdsdata[$i]["subcontractor"] = "";
				}
			} else {
				$tdsdata[$i]["subcontractor"] = "";
			}
			$tdsdata[$i]["amount"] = $data->amount;
			$tdsdata[$i]["paid"] = $data->paid;
			$tdsdata[$i]["tdsp"] = $data->expense_tdsp;
			$tdsdata[$i]["tdsamount"] = $data->expense_tds;
			$tdsdata[$i]["balance"] = $data->paidamount;
			$i++;
		}
		if (empty($paymentids)) {
			foreach ($vendordata->getData() as $data) {
				$tdsdata[$i]["project"] = ($data->project_id) ? $data->project->name : "";
				$tdsdata[$i]["date"] = ($data->date) ? date("d-m-Y", strtotime($data->date)) : "";
				$tdsdata[$i]["vendor"] = ($data->vendor_id) ? $data->vendor->name : "";
				$tdsdata[$i]["subcontractor"] = "";
				$tdsdata[$i]["amount"] = $data->amount + $data->tax_amount;
				$tdsdata[$i]["paid"] = $data->amount + $data->tax_amount;
				$tdsdata[$i]["tdsp"] = $data->tds;
				$tdsdata[$i]["tdsamount"] = $data->tds_amount;
				$tdsdata[$i]["balance"] = $data->paidamount;
				$i++;
			}
		}
		usort($tdsdata, array($this, 'sortByDate'));
		$arraylabel = array('Sl No', 'Project', 'Date', 'Vendor', 'Subcontractor', 'Amount Paid', 'TDS (%)', 'TDS Amount', 'Balance');
		$finaldata = array();
		$i = 0;
		$totalpaid = 0;
		$totaltdsp = 0;
		$totaltdsamount = 0;
		$totalpaidamount = 0;

		if (count($tdsdata) > 0) {
			foreach ($tdsdata as $data) {
				$totalpaid = $totalpaid + $data["paid"];
				$totaltdsp = $totaltdsp + $data["tdsp"];
				$totaltdsamount = $totaltdsamount + $data["tdsamount"];
				$totalpaidamount = $totalpaidamount + $data["balance"];
			}
		}
		$averagetdsp = $totaltdsp / count($tdsdata);
		$finaldata[$i][] = " ";
		$finaldata[$i][] = " ";
		$finaldata[$i][] = " ";
		$finaldata[$i][] = " ";
		$finaldata[$i][] = "Total";
		$finaldata[$i][] = Controller::money_format_inr($totalpaid, 2);
		$finaldata[$i][] = Controller::money_format_inr($averagetdsp, 2);
		$finaldata[$i][] = Controller::money_format_inr($totaltdsamount, 2);
		$finaldata[$i][] = Controller::money_format_inr($totalpaidamount, 2);
		$i++;

		$totalpaid = 0;
		$totaltdsp = 0;
		$totaltdsamount = 0;
		$totalpaidamount = 0;
		if (count($tdsdata) > 0) {
			foreach ($tdsdata as $data) {
				$totalpaid = $totalpaid + $data["paid"];
				$totaltdsp = $totaltdsp + $data["tdsp"];
				$totaltdsamount = $totaltdsamount + $data["tdsamount"];
				$totalpaidamount = $totalpaidamount + $data["balance"];

				$finaldata[$i][] = $i + 1;
				$finaldata[$i][] = ($data["project"]) ? $data["project"] : " ";
				$finaldata[$i][] = ($data["date"]) ? date("d/m/Y", strtotime($data["date"])) : " ";
				$finaldata[$i][] = ($data["vendor"]) ? $data["vendor"] : " ";
				$finaldata[$i][] = ($data["subcontractor"]) ? $data["subcontractor"] : " ";
				$finaldata[$i][] = ($data["paid"]) ? $data["paid"] : " ";
				$finaldata[$i][] = ($data["tdsp"]) ? $data["tdsp"] : " ";
				$finaldata[$i][] = ($data["tdsamount"]) ? $data["tdsamount"] : " ";
				$finaldata[$i][] = ($data["balance"]) ? $data["balance"] : " ";
				$i++;
			}
			$averagetdsp = $totaltdsp / count($tdsdata);
			$finaldata[$i][] = " ";
			$finaldata[$i][] = " ";
			$finaldata[$i][] = " ";
			$finaldata[$i][] = " ";
			$finaldata[$i][] = "";
			$finaldata[$i][] = '';
			$finaldata[$i][] = '';
			$finaldata[$i][] = '';
			$finaldata[$i][] = '';
		} else {
			$finaldata[$i][] = " ";
			$finaldata[$i][] = " ";
			$finaldata[$i][] = " ";
			$finaldata[$i][] = " ";
			$finaldata[$i][] = " ";
			$finaldata[$i][] = " ";
			$finaldata[$i][] = " ";
			$finaldata[$i][] = " ";
			$finaldata[$i][] = " ";
		}
		$filterdata = "";
		if (!empty($date_from)) {
			$filterdata .= "From: " . date("d/m/Y", strtotime($date_from)) . "\n";
		}
		if (!empty($date_to)) {
			$filterdata .= "To: " . date("d/m/Y", strtotime($date_to)) . "\n";
		}
		if (!empty($vendor_id)) {
			$vendor = Vendor::model()->findByPk($vendor_id);
			$filterdata .= "Vendor: " . $vendor->name . "\n";
		}
		if (!empty($subcontractor_id)) {
			$subcontractor = Subcontractor::model()->findByPk($subcontractor_id);
			$filterdata .= "Subcontractor: " . $subcontractor->subcontractor_name . "\n";
		}
		if (!empty($company_id)) {
			$company = Company::model()->findByPk($company_id);
			$filterdata .= "Company: " . $company->company_id . "<br/>";
		}
		$finaldata[$i + 1][] = " ";
		$finaldata[$i + 1][] = $filterdata;
		$finaldata[$i + 1][] = " ";
		$finaldata[$i + 1][] = " ";
		$finaldata[$i + 1][] = " ";
		$finaldata[$i + 1][] = " ";
		$finaldata[$i + 1][] = " ";
		$finaldata[$i + 1][] = " ";
		$finaldata[$i + 1][] = " ";

		Yii::import('ext.ECSVExport');
		$csv = new ECSVExport($finaldata);
		$csv->setHeaders($arraylabel);
		$csv->setToAppend();
		$contentdata = $csv->toCSV();
		$csvfilename = 'TDS_Report' . strtotime(date('Y-m-d H:i:s')) . '.csv';
		Yii::app()->getRequest()->sendFile($csvfilename, $contentdata, "text/csv", false);
		exit();
	}
	public function GetItemCategoryById($parent = 0, $spacing = '', $user_tree_array = '', $id = '')
	{
		if (!is_array($user_tree_array))
			$user_tree_array = array();
		$data = array();
		$final = array();

		$user = Users::model()->findByPk(Yii::app()->user->id);
		$arrVal = explode(',', $user->company_id);
		$newQuery = "";
		foreach ($arrVal as $arr) {
			if ($newQuery)
				$newQuery .= ' OR';
			$newQuery .= " FIND_IN_SET('" . $arr . "', company_id)";
		}
		$specsql = "SELECT id, specification, brand_id, cat_id,unit "
			. " FROM " . $this->tableNameAcc('specification', 0) . " "
			. " WHERE id = {$id}";
		$specification = Yii::app()->db->createCommand($specsql)->queryRow();
		if ($specification['brand_id'] != NULL) {
			$brandsql = "SELECT brand_name FROM " . $this->tableNameAcc('brand', 0) . " WHERE id=" . $specification['brand_id'] . "";
			$brand_details = Yii::app()->db->createCommand($brandsql)->queryRow();
			$brand = '-' . ucwords($brand_details['brand_name']);
		} else {
			$brand = '';
		}

		$result = $this->actionGetParent($specification['cat_id']);
		$final['data'] = ucwords($result['category_name']) . $brand . '-' . ucwords($specification['specification']);
		$final['id'] = $specification['id'];
		$final['base_unit'] = $specification['unit'];
		return $final;
	}
	protected function getUnbilledAmount($purchase_id)
	{
		$tblpx = Yii::app()->db->tablePrefix;
		$unbilled = Yii::app()->db->createCommand("SELECT SUM(((pi.quantity - IFNULL((SELECT SUM(billitem_quantity) FROM {$tblpx}billitem WHERE purchaseitem_id = pi.item_id), 0)) * pi.rate)) as unbilled_amount FROM {$tblpx}purchase_items pi WHERE pi.purchase_id = {$purchase_id}")->queryRow();
		$unbilled_amount = $unbilled["unbilled_amount"];
		if ($unbilled_amount > 0)
			return $unbilled_amount;
		else
			return 0;
	}
	private function sortByDate($a, $b)
	{
		$a = $a['date'];
		$b = $b['date'];
		if ($a == $b)
			return 0;
		return ($a > $b) ? -1 : 1;
	}
	public function actionadditionalbill1()
	{

		if (isset($_POST)) {
			$bill_id = $_POST['billid'];
			$category = $_POST['category'];
			$amount = $_POST['amount'];
			$totalamount = $_POST['totalamount'];

			$tblpx = Yii::app()->db->tablePrefix;
			$addBills = Yii::app()->db->createCommand(" INSERT INTO " . $tblpx . "additional_bill(`id`, `bill_id`, `category`, `amount`, `created_by`, `created_date`)VALUES (null, " . $bill_id . ",'" . $category . "'," . $amount . "," . yii::app()->user->id . ",'" . date('Y-m-d') . "')");
			$addBills->execute();
			$billItemId = Yii::app()->db->getLastInsertID();
			if ($billItemId) {

				$addcharges = Yii::app()->db->createCommand("SELECT SUM(amount) as amount FROM " . $tblpx . "additional_bill WHERE `bill_id`=" . $bill_id)->queryRow();

				$data1 = Yii::app()->db->createCommand("SELECT SUM(billitem_amount) as bill_amount FROM {$tblpx}billitem WHERE bill_id=" . $bill_id . "")->queryRow();

				$data2 = Yii::app()->db->createCommand("SELECT SUM(billitem_taxamount) as bill_taxamount FROM {$tblpx}billitem WHERE bill_id=" . $bill_id . "")->queryRow();

				$data3 = Yii::app()->db->createCommand("SELECT SUM(billitem_discountamount) as bill_discountamount FROM {$tblpx}billitem WHERE bill_id=" . $bill_id . "")->queryRow();
				$model = Bills::model()->findByPk($bill_id);

				$total = ($data1['bill_amount'] + $data2['bill_taxamount'] + $addcharges['amount'] + ($model->round_off)) - $data3['bill_discountamount'];
				// $total = $totalamount + $amount;
				$addBillsh = Yii::app()->db->createCommand(" UPDATE " . $tblpx . "bills SET bill_additionalcharge=" . $addcharges['amount'] . " WHERE bill_id=" . $bill_id . "");
				$addBillsh->execute();

				$data = array('itemid' => $billItemId, 'amount' => $total);

				echo json_encode($data);
				exit;
			} else {
				echo 1;
				exit;
			}
		}
	}
	public function actiondeletebill2()
	{
		$tblpx = Yii::app()->db->tablePrefix;
		if (isset($_POST)) {

			$id = $_POST['id'];
			$billid = $_POST['billid'];

			if ($id != '') {
				$additional_details = Yii::app()->db->createCommand("SELECT amount,bill_id FROM {$tblpx}additional_bill WHERE id=" . $id . "")->queryRow();
				$bill_details = Yii::app()->db->createCommand("SELECT bill_totalamount FROM {$tblpx}bills WHERE bill_id=" . $billid . "")->queryRow();

				$delbill = Yii::app()->db->createCommand("DELETE FROM " . $tblpx . "additional_bill WHERE `id`=" . $id);
				$delbill->execute();

				$additional = Yii::app()->db->createCommand("SELECT SUM(amount)as amount FROM {$tblpx}additional_bill WHERE bill_id=" . $billid . "")->queryRow();

				$bill = Yii::app()->db->createCommand("SELECT bill_totalamount FROM {$tblpx}bills WHERE bill_id=" . $billid . "")->queryRow();

				$total_amount = $bill['bill_totalamount'] + $additional['amount'];

				$addBillsh = Yii::app()->db->createCommand(" UPDATE " . $tblpx . "bills SET bill_additionalcharge=" . ($additional['amount'] != '' ? $additional['amount'] : 0) . " WHERE bill_id=" . $billid . "");
				$addBillsh->execute();

				echo json_encode(array('bill_amount' => number_format($total_amount, 2, '.', ''), 'additional' => ($additional['amount'] != '' ? $additional['amount'] : 0)));
				exit;
			}
		}
	}
	public function actionadditionalbill()
	{

		if (isset($_POST)) {


			$bill_id = $_POST['billid'];
			$category = $_POST['category'];
			$amount = $_POST['amount'];

			$tblpx = Yii::app()->db->tablePrefix;
			$addBills = Yii::app()->db->createCommand(" INSERT INTO " . $tblpx . "additional_bill(`id`, `bill_id`, `category`, `amount`, `created_by`, `created_date`)VALUES (null, " . $bill_id . ",'" . $category . "'," . $amount . "," . yii::app()->user->id . ",'" . date('Y-m-d') . "')");
			$addBills->execute();
			$billItemId = Yii::app()->db->getLastInsertID();
			if ($billItemId) {

				$addcharges = Yii::app()->db->createCommand("SELECT ROUND(SUM(amount), 2) as amount FROM " . $tblpx . "additional_bill WHERE `bill_id`=" . $bill_id)->queryRow();

				$addBillsh = Yii::app()->db->createCommand(" UPDATE " . $tblpx . "bills SET bill_additionalcharge=" . $addcharges['amount'] . " WHERE bill_id=" . $bill_id . "");
				$addBillsh->execute();

				$data = array('itemid' => $billItemId, 'amount' => $addcharges['amount']);

				echo json_encode($data);
				exit;
			} else {
				echo 1;
				exit;
			}
		}
	}
	public function actiondeletebill1()
	{
		$tblpx = Yii::app()->db->tablePrefix;
		if (isset($_POST)) {

			$id = $_POST['id'];
			$bill_id = $_POST['billid'];
			if ($id != '') {
				$delbill_amount_query = Yii::app()->db->createCommand("SELECT amount FROM " . $tblpx . "additional_bill WHERE `id`=" . $id)->queryRow();
				$delbill_amount = $delbill_amount_query['amount'];

				$delbill = Yii::app()->db->createCommand("DELETE FROM " . $tblpx . "additional_bill WHERE `id`=" . $id);
				if ($delbill->execute()) {
					$additional_amount = Yii::app()->db->createCommand("SELECT SUM(amount)as additionalamount FROM " . $tblpx . "additional_bill WHERE `bill_id`=" . $bill_id)->queryRow();

					$addBillsh = Yii::app()->db->createCommand(" UPDATE " . $tblpx . "bills SET bill_additionalcharge=" . ($additional_amount['additionalamount'] != '' ? $additional_amount['additionalamount'] : 0) . " WHERE bill_id=" . $bill_id . "");
					$addBillsh->execute();
				}

				echo 1;
				exit;
			}
		}
	}
	public function actiondeletebill()
	{
		$tblpx = Yii::app()->db->tablePrefix;
		if (isset($_POST)) {

			$id = $_POST['id'];
			$bill_id = $_POST['billid'];
			if ($id != '') {
				$bill_amount = Yii::app()->db->createCommand("SELECT bill_totalamount FROM " . $tblpx . "bills WHERE `bill_id`=" . $bill_id)->queryRow();

				$delbill = Yii::app()->db->createCommand("DELETE FROM " . $tblpx . "additional_bill WHERE `id`=" . $id);

				if ($delbill->execute()) {
					$additional_amount = Yii::app()->db->createCommand("SELECT SUM(amount)as additionalamount FROM " . $tblpx . "additional_bill WHERE `bill_id`=" . $bill_id)->queryRow();

					$addBillsh = Yii::app()->db->createCommand(" UPDATE " . $tblpx . "bills SET bill_additionalcharge=" . ($additional_amount['additionalamount'] != '' ? $additional_amount['additionalamount'] : 0) . "  WHERE bill_id=" . $bill_id . "");
					$addBillsh->execute();

					$total_amount = $bill_amount['bill_totalamount'] + $additional_amount['additionalamount'];

					echo json_encode(array('amount' => $total_amount, 'additional' => ($additional_amount['additionalamount'] != '' ? $additional_amount['additionalamount'] : 0)));
					exit;
				} else {
					$additional_amount = Yii::app()->db->createCommand("SELECT SUM(amount)as additionalamount FROM " . $tblpx . "additional_bill WHERE `bill_id`=" . $bill_id)->queryRow();
					$total_amount = $bill_amount['bill_totalamount'] + $additional_amount['additionalamount'];
					echo json_encode(array('amount' => number_format($total_amount, 2, '.', ''), 'additional' => ($additional_amount['additionalamount'] != '' ? $additional_amount['additionalamount'] : 0)));
					exit;
				}
			}
		}
	}

	public function actionapprovebills($id = 0)
	{
		$id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_SPECIAL_CHARS);
		$transaction = Yii::app()->db->beginTransaction();
		$result = array('status' => '1', 'msg' => 'Successfully updated');
		try {
			$model = $this->loadModelBillItem($id);
			if (($model->approve_status == '1') && ($model->approve_status != NULL)) {
				$model->approve_status = 2;
			}
			if ($model->rate_approve == 1) {
				$model->rate_approve = 2;
			}
			if (!$model->update())
				throw new Exception('An error occured in updating');
			$transaction->commit();
		} catch (Exception $error) {
			$transaction->rollBack();
			$result = array('status' => '0', 'msg' => $error->getMessage());
		} finally {
			echo json_encode($result);
			exit;
		}
	}

	public function actionadditionalItems()
	{


		$result = array();
		$billId = $_POST["billid"];
		$billItemId = $_POST['billitemid'];
		$billitembatch = isset($_POST["billitembatch"]) ? $_POST["billitembatch"] : "";
		$createdBy = Yii::app()->user->id;
		$createdDate = date('Y-m-d');
		$bill_prev_amt = $bill_prev_tax_amt = $bill_prev_disc_amt = $bill_prev_total_amt = 0;
		if ($billId == "") {
			$bill_model = new Bills();
			$bill_model->created_by = $createdBy;
			$bill_model->created_date = $createdDate;
		} else {
			$bill_model = Bills::model()->findByPk($billId);
			$bill_model->updated_date = date('Y-m-d');
		}
		$bill_model->purchase_id = $_POST["pid"];
		$bill_model->bill_number = $_POST["billnumber"];
		$bill_model->bill_date = date('Y-m-d', strtotime($_POST["billdate"]));
		$bill_model->bill_status = 1;
		$bill_model->company_id = $_POST["company_id"];
		$transaction = Yii::app()->db->beginTransaction();
		try {
			if ($bill_model->save()) {
				$billId = $bill_model->bill_id;
				if ($billItemId != '') {
					$bill_item_model = Billitem::model()->findByPk($billItemId);
				} else {

					$bill_item_model = new Billitem();
					$bill_item_model->created_date = date('Y-m-d');
				}
				if ($_POST["categoryid"] != '') {


					$bill_item_model->bill_id = $billId;
					$bill_item_model->batch = $billitembatch;
					$bill_item_model->warehouse_id = $_POST["warehouse"];
					$bill_item_model->billitem_quantity = $_POST["quantity"];
					$bill_item_model->billitem_unit = $_POST["unit"];
					$bill_item_model->billitem_rate = $_POST["rate"];
					$bill_item_model->purchaseitem_quantity = $_POST["purchasequantity"];
					$bill_item_model->purchaseitem_unit = $_POST["purchaseunit"];
					$bill_item_model->purchaseitem_rate = $_POST["purchaserate"];
					$bill_item_model->billitem_taxslab = $_POST["tax_slab"];
					$bill_item_model->billitem_amount = $_POST["amount"];
					$bill_item_model->billitem_discountamount = $_POST["damount"];
					$bill_item_model->billitem_discountpercent = $_POST["dpercent"];
					$bill_item_model->billitem_cgst = $_POST["cgst"];
					$bill_item_model->billitem_cgstpercent = $_POST["cgstpercent"];
					$bill_item_model->billitem_sgst = $_POST["sgst"];
					$bill_item_model->billitem_sgstpercent = $_POST["sgstpercent"];
					$bill_item_model->billitem_taxamount = $_POST["totaltax"];
					$bill_item_model->billitem_taxpercent = $_POST["totaltaxp"];
					$bill_item_model->category_id = $_POST["categoryid"];
					$bill_item_model->billitem_igst = $_POST["igst"];
					$bill_item_model->billitem_igstpercent = $_POST["igstpercent"];
					$bill_item_model->billitem_hsn_code = $_POST["hsn_code"];

					if ($bill_item_model->save()) {

						$bill_item_sum = Yii::app()->db->createCommand()
							->select('b.bill_id, SUM(billitem_amount) as amount,SUM(billitem_taxamount) as tax_amount,SUM(billitem_discountamount) as disc_amount')
							->from($this->tableName('Billitem') . ' b')
							->join($this->tableName('PurchaseItems') . ' p', 'b.purchaseitem_id=p.item_id')
							->where('b.bill_id = ' . $billId . ' AND p.purchase_id = ' . $_POST["pid"])
							->queryRow();
						$bill_additional_item_sum = Yii::app()->db->createCommand()
							->select('b.bill_id, SUM(billitem_amount) as amount,SUM(billitem_taxamount) as tax_amount,SUM(billitem_discountamount) as disc_amount')
							->from($this->tableName('Billitem') . ' b')
							->where('b.bill_id = ' . $billId . ' AND (b.purchaseitem_id IS NULL OR b.purchaseitem_id = 0)')
							->queryRow();

						$bill_model->bill_amount = $bill_item_sum['amount'] + $bill_additional_item_sum['amount'];
						$bill_model->bill_taxamount = $bill_item_sum['tax_amount'] + $bill_additional_item_sum['tax_amount'];
						$bill_model->bill_discountamount = $bill_item_sum['disc_amount'] + $bill_additional_item_sum['disc_amount'];
						$bill_model->bill_totalamount = ($bill_model['bill_amount'] + $bill_model['bill_taxamount'] + $bill_model['bill_additionalcharge'] + $bill_model['round_off']) - $bill_model['bill_discountamount'];
						if (!$bill_model->update()) {
							throw new Exception('An error occured in saving bill');
						}
						$billItemId = Yii::app()->db->getLastInsertID();
					} else {
						throw new Exception('An error occured in saving bill item');
					}
				}
				$result[1] = $billItemId;
				$result[0] = $billId;
				$result['amount'] = $bill_model->bill_amount;
				$result['tax_amount'] = $bill_model->bill_taxamount;
				$result['disc_amount'] = $bill_model->bill_discountamount;
				$result['tot_amount'] = $bill_model->bill_totalamount;
				$result['message'] = 'Success';
				$result['status'] = '1';
			} else {
				throw new Exception('An error occured in saving bill');
			}
			$transaction->commit();
		} catch (Exception $error) {
			$transaction->rollBack();
			$result[1] = $billItemId;
			$result[0] = $billId;
			$result['tax_amount'] = '';
			$result['disc_amount'] = '';
			$result['tot_amount'] = '';
			$result['message'] = $error->getMessage();
			$result['status'] = '0';
		} finally {
			echo json_encode($result);
			exit;
		}
	}

	public function actiongetAdditionalItem()
	{
		$result = array();
		$unit_list = array();
		$baseunit = '';
		if (isset($_POST['id'])) {
			$item_id = $_POST['id'];
			$bill_item = Billitem::model()->findByPk($item_id);
			$result = $bill_item->attributes;
		}
		if (isset($_POST['cat_id'])) {

			$cat_id = $_POST['cat_id'];
			$item_details = PurchaseCategory::model()->findByPk($cat_id);
			$baseunit = $item_details['unit'];
			$unit_list = Yii::app()->db->createCommand("SELECT * FROM " . $this->tableNameAcc('unit_conversion', 0) . " LEFT JOIN " . $this->tableNameAcc('unit', 0) . " ON " . $this->tableNameAcc('unit_conversion', 0) . ".conversion_unit=" . $this->tableNameAcc('unit', 0) . ".unit_name WHERE " . $this->tableNameAcc('unit_conversion', 0) . ".item_id=" . $cat_id)->queryAll();
		}
		$result_data = array('resultdata' => $result, 'unit_title' => $unit_list, 'baseunit' => $baseunit);
		echo json_encode($result_data);
	}

	public function actionlistadditionalitems()
	{
		$billId = $_POST['billid'];
		if (!empty($billId)) {
			$additional_items = Billitem::model()->findAll(array('condition' => 'bill_id = ' . $billId . ' AND (purchaseitem_id IS NULL OR purchaseitem_id = 0)'));

			$itemlist = $this->renderPartial('_additional_item_list', array('items' => $additional_items));
		}

		echo $itemlist;
	}

	public function actionupdateBillAmount()
	{
		$result = array('status' => 0, 'message' => 'Nothing to submit');
		if (isset($_POST['Bills'])) {
			$model = $this->loadModel($_POST['bill_id']);
			$model->attributes = $_POST['Bills'];
			$model->bill_totalamount = ($model->bill_amount + $model->bill_taxamount + ($model->round_off) - $model->bill_discountamount);
			if ($model->save()) {
				$result = array('status' => 1, 'message' => 'success', 'values' => $model->attributes);
			} else {
				$result = array('status' => 0, 'message' => $model->getErrors());
			}
		}
		echo json_encode($result);
	}

	public function actiongetRelatedUnit()
	{
		$cat_id = $_POST['cat_id'];
		$tblpx = Yii::app()->db->tablePrefix;
		$html['html'] = '';
		$html['unit'] = '';
		if ($cat_id != '') {
			$unit_purch_list = Yii::app()->db->createCommand("SELECT unit,id FROM {$tblpx}specification  WHERE id=" . $cat_id)->queryRow();
			$unit_sql = "SELECT * FROM {$tblpx}unit_conversion "
				. " LEFT JOIN {$tblpx}unit "
				. " ON {$tblpx}unit_conversion.conversion_unit={$tblpx}unit.unit_name "
				. " WHERE {$tblpx}unit_conversion.item_id=" . $cat_id;
			$unit_list = Yii::app()->db->createCommand($unit_sql)->queryAll();

			if (!empty($unit_list)) {

				$html['html'] .= '<option value="" >Select Unit</option>';
				foreach ($unit_list as $key => $value) {
					$html['html'] .= '<option value="' . $value['conversion_unit'] . '" >' . $value['unit_name'] . '</option>';
				}
				$html['unit'] .= $unit_purch_list['unit'];
			} else {
				$html['html'] .= '<option value="" >Select Unit</option>';
				$html['html'] .= '<option value="' . $unit_purch_list['unit'] . '" >' . $unit_purch_list['unit'] . '</option>';
				$html['unit'] .= $unit_purch_list['unit'];
			}
		}
		echo json_encode($html);
	}

	public function actiongetUnitconversionFactor()
	{
		if (isset($_POST)) {

			extract($_POST);
			$conversion_value = 0;
			$model = UnitConversion::model()->findByAttributes(array('base_unit' => $base_unit, 'conversion_unit' => $purchase_unit, 'item_id' => $item_id));
			$conversion_value = $model['conversion_factor'];

			echo $conversion_value;
		}
	}

	public function GetItemunitID($unit_name)
	{
		$unit_name = trim($unit_name);
		$ItemUnit = Unit::model()->findAll(
			array("condition" => "unit_name ='" . $unit_name . "' OR unit_code ='" . $unit_name . "'", "order" => "id")
		);
		$ItemUnit_id = 0;
		if (!empty($ItemUnit)) {
			foreach ($ItemUnit as $key => $value) {
				$ItemUnit_id = $value["id"];
			}
		}

		return $ItemUnit_id;
	}

	public function GetItemunitname($unit_id)
	{
		$unit_id = $unit_id;
		$unitdetails = Unit::model()->findByPk($unit_id);
		$Unit_name = $unitdetails["unit_name"];
		return $Unit_name;
	}

	public function actionadminlistexcel($date_to, $date_from, $bill_number, $purchase_no, $project_id, $vendor_id)
	{
		$tblpx = Yii::app()->db->tablePrefix;
		$where = 'AND 1=1';
		$filter = "Filter By : ";
		if (isset($purchase_no) && $purchase_no != '') {
			$where .= " AND " . $tblpx . "purchase.purchase_no ='" . $purchase_no . "'";
			$filter .= " Purchase No : " . $purchase_no;
		}
		if (isset($bill_number) && $bill_number != '') {
			$where .= " AND " . $tblpx . "bills.bill_number ='" . $bill_number . "'";
			$filter .= " Bill No : " . $bill_number;
		}
		if (isset($date_from) && isset($date_to) && $date_to != '' && $date_from != '') {
			$where .= " AND " . $tblpx . "bills.bill_date >= '" . date('Y-m-d', strtotime($date_from)) . "'";
			$where .= " AND " . $tblpx . "bills.bill_date <= '" . date('Y-m-d', strtotime($date_to)) . "'";
			$filter .= " Date Between : " . $date_from . " AND " . $date_to . "";
		}
		if (isset($project_id) && $project_id != '') {
			$where .= " AND " . $tblpx . "purchase.project_id =" . $project_id . "";

			$sql = "SELECT {$tblpx}projects.name as project_name,{$tblpx}vendors.name as vendor_name "
				. " FROM {$tblpx}purchase LEFT JOIN {$tblpx}projects "
				. " ON {$tblpx}purchase.project_id = {$tblpx}projects.pid "
				. " LEFT JOIN {$tblpx}vendors ON {$tblpx}purchase.vendor_id={$tblpx}vendors.vendor_id  "
				. " WHERE {$tblpx}purchase.p_id = " . $project_id;
			$purchase_data = Yii::app()->db->createCommand($sql)->queryRow();
			$filter .= " Project : " . $purchase_data['project_name'];
		}
		if (isset($vendor_id) && $vendor_id != '') {
			$where .= " AND " . $tblpx . "purchase.vendor_id =" . $vendor_id . "";
			$purchase_data = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}vendors "
				. " WHERE vendor_id=" . $vendor_id . "")->queryRow();
			$filter .= " Vendor : " . $purchase_data['name'];
		}

		$sql = "SELECT * FROM {$tblpx}bills LEFT JOIN {$tblpx}purchase "
			. " ON {$tblpx}bills.purchase_id ={$tblpx}purchase.p_id  "
			. " WHERE {$tblpx}bills.company_id=" . Yii::app()->user->company_id . " " . $where
			. " GROUP BY {$tblpx}bills.bill_id ORDER BY {$tblpx}bills.bill_id DESC";
		$data = Yii::app()->db->createCommand($sql)->queryAll();
		$arraylabel = array($filter, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
		$finaldata = array();

		$finaldata[0][] = 'Sl No';
		$finaldata[0][] = 'Bill No';
		$finaldata[0][] = 'Purchase No';
		$finaldata[0][] = 'Project';
		$finaldata[0][] = 'Vendor';
		$finaldata[0][] = 'Date';
		$finaldata[0][] = 'Amount';
		$finaldata[0][] = 'Total Amount';
		$i = 1;
		foreach ($data as $key => $value) {
			$finaldata[$i][] = $key + 1;
			$finaldata[$i][] = $value['bill_number'];
			$finaldata[$i][] = $value['purchase_no'];
			if ($value['purchase_id'] != NULL) {
				$tblpx = Yii::app()->db->tablePrefix;

				$sql = "SELECT {$tblpx}projects.name as project_name,{$tblpx}vendors.name "
					. " as vendor_name FROM {$tblpx}purchase LEFT JOIN {$tblpx}projects "
					. " ON {$tblpx}purchase.project_id = {$tblpx}projects.pid "
					. " LEFT JOIN {$tblpx}vendors ON {$tblpx}purchase.vendor_id={$tblpx}vendors.vendor_id  "
					. " WHERE {$tblpx}purchase.p_id = " . $value['purchase_id'];
				$purchase_data = Yii::app()->db->createCommand($sql)->queryRow();
				$project_name = $purchase_data['project_name'];
			}
			if ($value['vendor_id'] != NULL || $value['purchase_id'] != NULL) {
				$tblpx = Yii::app()->db->tablePrefix;
				$sql = "SELECT {$tblpx}projects.name as project_name,{$tblpx}vendors.name "
					. " as vendor_name FROM {$tblpx}purchase "
					. " LEFT JOIN {$tblpx}projects ON {$tblpx}purchase.project_id = {$tblpx}projects.pid "
					. " LEFT JOIN {$tblpx}vendors ON {$tblpx}purchase.vendor_id={$tblpx}vendors.vendor_id "
					. " WHERE {$tblpx}purchase.p_id = " . $value['purchase_id'];
				$purchase_data = Yii::app()->db->createCommand($sql)->queryRow();
				$vendor_name = $purchase_data['vendor_name'];
			}
			$finaldata[$i][] = isset($project_name) ? $project_name : '';
			$finaldata[$i][] = isset($vendor_name) ? $vendor_name : '';
			$finaldata[$i][] = date('d-m-Y', strtotime($value['bill_date']));
			$finaldata[$i][] = ($value['bill_amount'] != '') ? Controller::money_format_inr($value['bill_amount'], 2, 1) : '0.00';
			$finaldata[$i][] = ($value['bill_totalamount'] != '') ? Controller::money_format_inr($value['bill_totalamount'] + $value['bill_additionalcharge'], 2, 1) : '0.00';

			$i++;
		}

		Yii::import('ext.ECSVExport');

		$csv = new ECSVExport($finaldata);
		$csv->setHeaders($arraylabel);
		$csv->setToAppend();
		$contentdata = $csv->toCSV();
		$csvfilename = 'Bills' . date('d_M_Y') . '.csv';
		Yii::app()->getRequest()->sendFile($csvfilename, $contentdata, "text/csv", false);
		exit();
	}

	public function actionGetduplicateentries()
	{
		$vendor = $_REQUEST["vendor"];
		$billno = $_REQUEST["billno"];
		$project = $_REQUEST["project"];
		$tblpx = Yii::app()->db->tablePrefix;
		$sql = "SELECT b.*,p.* FROM {$tblpx}bills b LEFT JOIN jp_purchase p "
			. " ON b.purchase_id=p.p_id WHERE b.bill_number = '{$billno}' "
			. " AND p.vendor_id = {$vendor}";

		$duplicate = Yii::app()->db->createCommand($sql)->queryAll();
		$result = '<button type="button" class="close"  aria-hidden="true">×</button><h4>Purchase Bills (' . count($duplicate) . ')</h4><div class="table-responsive"><table class="table table-bordered table-hover">
                            <thead class="entry-table">
                                <tr>
                                    <th>Sl No.</th>
                                    <th>Company</th>
                                    <th>Project</th>
                                    <th>Bill Number</th>
                                    <th>Invoice Number</th>            
                                    <th>Expense Head</th>
                                    <th>Vendor</th>
                                    <th>Expense Type</th>
                                    <th>Bank</th>
                                    <th>Cheque No</th>
                                    <th>Description</th>
                                    <th>Total</th>
                                    <th>Paid</th>
                                    <th>Receipt Type</th>
                                    <th>Receipt</th>
                                    <th>Purchase Type</th>
                                </tr>   
                            </thead>
                            <tbody>';
		$i = 1;
		foreach ($duplicate as $entry) {
			$result .= '<tr>
                        <td>' . $i . '</td>
                        <td>';
			$company = Company::model()->findByPk($entry["company_id"]);
			if ($company)
				$result .= '' . $company->name;
			$result .= '</td><td>';
			$project = Projects::model()->findByPk($entry["project_id"]);
			if ($project)
				$result .= '' . $project->name;
			$result .= '</td><td>';
			$bill = Bills::model()->findByPk($entry["bill_id"]);
			if ($bill)
				$result .= '' . $bill->bill_number;
			$result .= '</td><td>';
			$result .= '</td><td>';
			$result .= '</td><td>';
			$vendor = Vendors::model()->findByPk($entry["vendor_id"]);
			if ($vendor)
				$result .= '' . $vendor->name;
			$result .= '</td><td>';
			$result .= '</td><td>';
			$result .= '</td>';
			$result .= '<td></td>
                        <td></td>
                        <td class="text-right">' . Controller::money_format_inr($entry["bill_amount"], 2) . '</td>
                        <td class="text-right"></td>
                        <td>';
			$result .= '</td>
                        <td class="text-right"></td>
                        <td></td>
                        </tr>';
			$i++;
		}
		$result .= '</tbody>
                       </table>
					   </div>';
		echo $result;
	}

	public function actionageingreport()
	{
		$model = new Bills;
		$tblpx = Yii::app()->db->tablePrefix;
		$user = Users::model()->findByPk(Yii::app()->user->id);
		$specification = $this->actionGetItemCategory($parent = 0, $spacing = '', $user_tree_array = '');
		$expense_type = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}expense_type ORDER BY type_name")->queryAll();
		$arrVal = explode(',', $user->company_id);
		$newQuery = "";
		foreach ($arrVal as $arr) {
			if ($newQuery)
				$newQuery .= ' OR';
			$newQuery .= " FIND_IN_SET('" . $arr . "', bill.company_id)";
		}
		$where = ' where 1=1 ';
		$date_from = '';
		$date_to = '';
		$pro_id = '';
		$vendor_id = '';
		$pu_num = '';
		$bill_num = '';
		$company_id = '';
		$item_spec = '';
		$exp_head = '';
		$day_interval = '';
		$day_range = '';

		if (isset($_POST['Bills'])) {

			$date_from = $_POST['Bills']['date_from'];
			$date_to = $_POST['Bills']['date_to'];
			$pro_id = $_POST['Bills']['project'];
			$vendor_id = $_POST['Bills']['vendor'];
			$pu_num = $_POST['Bills']['pu_num'];
			$bill_num = $_POST['Bills']['bill_num'];
			$company_id = $_POST['company_id'];
			$day_interval = $_POST['Bills']['day_interval'];
			$day_range = isset($_POST['Bills']['day_range']) ? $_POST['Bills']['day_range'] : "";
			$item_spec = '';
			if (!empty($_POST['category_id'])) {
				$category_id = $_POST['category_id'];
				$item_spec = implode(",", $category_id);
			}
			$exp_head = '';
			if (!empty($_POST['expense_head'])) {
				$expense_head = $_POST['expense_head'];
				$exp_head = implode(",", $expense_head);
			}
			if (!empty($day_interval)) {
				$day_range = "";
				$date_from = "";
				$date_to = "";
				$where .= " AND bill_date < DATE_SUB(NOW(), INTERVAL $day_interval DAY)";
			}

			if (!empty($day_range)) {
				$day_interval = "";
				$date_from = "";
				$date_to = "";
				if ($day_range == 1) {
					$where .= " AND bill_date < DATE_SUB(NOW(), INTERVAL 1 DAY) AND bill_date > DATE_SUB(NOW(), INTERVAL 15 DAY) ";
				} else if ($day_range == 2) {
					$where .= " AND bill_date < DATE_SUB(NOW(), INTERVAL 15 DAY) AND bill_date > DATE_SUB(NOW(), INTERVAL 30 DAY) ";
				} else if ($day_range == 3) {
					$where .= " AND bill_date < DATE_SUB(NOW(), INTERVAL 30 DAY) AND bill_date > DATE_SUB(NOW(), INTERVAL 45 DAY) ";
				} else {
					$where .= " AND bill_date < DATE_SUB(NOW(), INTERVAL 45 DAY)";
				}
			}
			if ($date_from != '' && $date_to != '') {
				$day_interval = '';
				$day_range = '';
				$where .= " and date(bill_date) BETWEEN '" . $date_from . "' and '" . $date_to . "' ";
			}
			if ($pro_id != '') {
				$where .= ' and  pro.pid = ' . $pro_id;
			}
			if ($vendor_id != '') {
				$where .= ' and ven.vendor_id = ' . $vendor_id;
			}
			if ($pu_num != '') {
				$where .= " and purchase_no = '" . $pu_num . "' ";
			}
			if ($bill_num != '') {
				$where .= " and  bill_number = '" . $bill_num . "' ";
			}
			if (!empty($date_from) && empty($date_to)) {
				$day_range = '';
				$day_interval = '';
				$where .= " AND date(bill_date) >= '" . $date_from . "'";
			}
			if (!empty($date_to) && empty($date_from)) {
				$day_range = '';
				$day_interval = '';
				$where .= " AND date(bill_date) <= '" . $date_to . "'";
			}

			if (!empty($company_id)) {
				$where .= " AND bill.company_id=" . $company_id . " ";
			} else {
				$where .= " AND (" . $newQuery . ")";
			}
			if (!empty($category_id)) {
				if (!empty($item_spec)) {
					$item_spec = ltrim($item_spec, ',');
					$where .= " AND item.category_id IN (" . $item_spec . ")";

				}
			}
			if (!empty($expense_head)) {
				if (!empty($exp_head)) {
					$exp_head = ltrim($exp_head, ',');
					$where .= " AND exp_head.type_id IN (" . $exp_head . ")";
				}
			}
		} else {
			$date_from = date("Y-m-") . "01";
			$date_to = date("Y-m-d");
			if (!empty($date_from)) {
				$where .= " AND date(bill_date) >= '" . $date_from . "'";
			}
			if (!empty($date_to)) {
				$where .= " AND date(bill_date) <= '" . $date_to . "'";
			}
		}

		$bill_list = array();
		$bills = "";

		$command = Yii::app()->db->createCommand()->select('b.bill_id as billId');
		$command->from($this->tableNameAcc('expenses', 0) . ' as e');
		$command->leftJoin($this->tableNameAcc('bills', 0) . ' as b', 'e.bill_id = b.bill_id ');

		$tblpx = Yii::app()->db->tablePrefix;
		$user = Users::model()->findByPk(Yii::app()->user->id);
		$arrVal = explode(',', $user->company_id);
		$newQuery2 = "";
		foreach ($arrVal as $arr) {
			if ($newQuery2)
				$newQuery2 .= ' OR';
			$newQuery2 .= " FIND_IN_SET('" . $arr . "', b.company_id)";
		}

		$command->where("e.type = 73 AND b.bill_totalamount IS NOT NULL AND b.purchase_id IS NOT NULL AND $newQuery2");
		$billsArray = $command->queryAll();

		if (!empty($billsArray)) {
			foreach ($billsArray as $entry) {
				if ($entry['billId'] != "") {
					$bill_list[] = $entry['billId'];
				}
			}
			$bills = implode(',', $bill_list);
		}
		if (!empty($bills)) {
			$where .= " AND bill.bill_id NOT IN (" . $bills . ")";
		}


		Yii::app()->db->createCommand('SET SQL_BIG_SELECTS=1')->execute();
		$sql = "SELECT pro.pid,bill.bill_id,bill_number,bill_date,bill_totalamount, p_id,purchase_no,ven.name as vendorname,pro.name as projectname,paid,bill.company_id FROM {$tblpx}bills as bill left join {$tblpx}purchase as pu on pu.p_id = bill.purchase_id LEFT join {$tblpx}vendors as ven on ven.vendor_id = pu.vendor_id LEFT join {$tblpx}projects as pro on pro.pid = pu.project_id LEFT join  {$tblpx}expenses as exp on exp.bill_id = bill.`bill_id` LEFT JOIN {$tblpx}billitem as item ON bill.bill_id=item.bill_id LEFT JOIN {$tblpx}expense_type as exp_head ON pu.expensehead_id=exp_head.type_id $where group by bill_number order by bill.bill_date ASC";
		//echo $sql;exit;
		$model = Yii::app()->db->createCommand($sql)->queryAll();
		$dataProvider1 = new CSqlDataProvider($sql, array(
			'keyField' => 'bill_id',
			'id' => 'bill_id',
			'totalItemCount' => count($model),
			'pagination' => false,
		));


		$this->render('ageingreport', array(
			'model' => $model,
			'dataProvider' => $dataProvider1,
			'date_from' => $date_from,
			'date_to' => $date_to,
			'pro_id' => $pro_id,
			'vendor_id' => $vendor_id,
			'pu_num' => $pu_num,
			'bill_num' => $bill_num,
			'company_id' => $company_id,
			'specification' => $specification,
			'item_spec' => $item_spec,
			'expense_type' => $expense_type,
			'exp_head' => $exp_head,
			'day_interval' => $day_interval,
			'day_range' => $day_range
		));
	}

	public function actiontotalreport()
	{
		$tblpx = Yii::app()->db->tablePrefix;
		$sql = "SELECT bill_id,bill_number,round_off,bill_additionalcharge,created_date,bill_amount,bill_taxamount,bill_discountamount,ROUND(bill_amount + bill_taxamount + round_off - bill_discountamount ,2) AS total , ROUND(bill_amount + bill_taxamount  + bill_additionalcharge + round_off - bill_discountamount ,2) AS additionaltotal1, bill_totalamount,ROUND(bill_additionalcharge,2) AS additional_billcharge FROM `jp_bills` WHERE bill_additionalcharge IS NOT NULL and created_date >'2023-03-29'";
		$total = Yii::app()->db->createCommand($sql)->queryAll();
		$main_array = array();
		if ($total) {
			foreach ($total as $key => $value) {
				$total = (float) $value['bill_amount'] + $value['bill_taxamount'] + $value['round_off'] - $value['bill_discountamount'];
				$main_array[] = array(
					'bill_number' => $value['bill_number'],
					'round_off' => $value['round_off'],
					'bill_additionalcharge' => $value['bill_additionalcharge'],
					'created_date' => $value['created_date'],
					'bill_amount' => $value['bill_amount'],
					'bill_taxamount' => $value['bill_taxamount'],
					'bill_discountamount' => $value['bill_discountamount'],
					'total' => $total,
					'bill_totalamount' => $value['bill_totalamount'],
					'additional_billcharge' => $value['additional_billcharge'],
				);

				$addBillsh = Yii::app()->db->createCommand(" UPDATE " . $tblpx . "bills SET bill_totalamount=" . $total . "  WHERE bill_id=" . $value['bill_id'] . "");
				$addBillsh->execute();
			}
		}

	}
	public function actioncheckdailybillnumber()
	{
		$billNumber = Yii::app()->request->getParam('bill_no'); // Assuming 'bill_no' is the parameter name
		$billId = Yii::app()->request->getParam('bill_id'); // Assuming 'bill_no' is the parameter name
		$response = array();
		$dailyExpenseBill = new DailyexpenseBill;
		$count = $dailyExpenseBill->count('bill_no = :bill_no AND id != :bill_id', array(':bill_no' => $billNumber, ':bill_id' => $billId));

		if ($count > 0) {
			$response = array('exists' => $count);
		}
		echo json_encode($response);
	}
	public function actiongetItemsBasedonBill()
	{

		$id = $_GET["bill_id"];
		$model = $this->loadModel($id);
		if (empty($model)) {
			$model = Bills::model()->find(array("condition" => "bill_id = " . $id . ""));

		}
		$items = Billitem::model()->findAll(array("condition" => "bill_id = " . $id . ' AND (purchaseitem_id IS NOT NULL AND purchaseitem_id != 0)', 'group' => 'purchaseitem_id'));
		//echo "<pre>";print_r($items);exit;
		$data = array();
		foreach ($items as $item) {
			if (!empty($item['category_id']) && $item['category_id'] != 0) {
				$category_model = Specification::model()->findByPk($item['category_id']);
				if ($category_model['brand_id'] != NULL) {
					$brand_details = Brand::model()->findByPk($category_model['brand_id']);
					$brand = $brand_details['brand_name'];
				} else {
					$brand = '';
				}
				if ($category_model['cat_id'] != NULL) {
					$parent_category = PurchaseCategory::model()->findByPk($category_model['cat_id']);
					$category_name = $parent_category["category_name"] . ' - ';

				} else {
					$category_name = '';
				}
				$specification = $category_name . $brand . ' - ' . $category_model['specification'];
				$data[] = $specification;

			}
		}
		$items_add = Billitem::model()->findAll(array("condition" => "bill_id = " . $id . ' AND (purchaseitem_id IS  NULL OR purchaseitem_id = 0)'));
		foreach ($items_add as $item) {
			if (!empty($item['category_id']) && $item['category_id'] != 0) {
				$category_model = Specification::model()->findByPk($item['category_id']);
				if ($category_model['brand_id'] != NULL) {
					$brand_details = Brand::model()->findByPk($category_model['brand_id']);
					$brand = $brand_details['brand_name'];
				} else {
					$brand = '';
				}
				if ($category_model['cat_id'] != NULL) {
					$parent_category = PurchaseCategory::model()->findByPk($category_model['cat_id']);
					$category_name = $parent_category["category_name"] . ' - ';

				} else {
					$category_name = '';
				}
				$specification = $category_name . $brand . ' - ' . $category_model['specification'];
				$data[] = $specification;

			}
		}
		echo CJSON::encode($data);
	}

	//bill
	public function actionloadbillModal($mrId = '', $selectedItems = '')
	{

		$this->layout = '//layouts/bill_blank';
		$model = '';
		$pid = '';
		$vendor = '';
		$project = '';
		$expense_head = '';
		$selectedItems = '';
		$mrId = $_GET['mrId'];
		if (isset($_GET['mrId'])) {
			$mrId = $_GET['mrId'];
			if (isset($_GET['selectedItems'])) {
				$selectedItems = $_GET['selectedItems'];
			}

			$materialreq = MaterialRequisition::model()->findByPk($mrId);

			$model = new Bills;
			$newmodel = new Purchase();
			$model2 = new Warehousestock;
			$tblpx = Yii::app()->db->tablePrefix;
			$user = Users::model()->findByPk(Yii::app()->user->id);
			$arrVal = explode(',', $user->company_id);
			$newQuery = "";
			$newQuery1 = "";
			foreach ($arrVal as $arr) {
				if ($newQuery)
					$newQuery .= ' OR';
				if ($newQuery)
					$newQuery1 .= ' OR';
				$newQuery .= " FIND_IN_SET('" . $arr . "', company_id)";
				$newQuery1 .= " FIND_IN_SET('" . $arr . "', p.company_id)";
			}
			$specification = $this->actionGetItemCategory($parent = 0, $spacing = '', $user_tree_array = '');
			if (Yii::app()->user->role == 1 || Yii::app()->user->role == 2) {
				$project = Yii::app()->db->createCommand("SELECT pid, name  FROM {$tblpx}projects WHERE " . $newQuery . " ORDER BY name")->queryAll();
			} else {
				$project = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}projects p JOIN {$tblpx}project_assign pa ON p.pid = pa.projectid WHERE " . $newQuery1 . " AND pa.userid = '" . Yii::app()->user->id . "'")->queryAll();
			}
			$vendor = Yii::app()->db->createCommand("SELECT DISTINCT vendor_id, name FROM {$tblpx}vendors WHERE (" . $newQuery . ") ORDER BY name")->queryAll();
			$this->render('//bills/_bill_details_form', array(
				'model' => $model,
				'model2' => $model2,
				'project' => $project,
				'vendor' => $vendor,
				'newmodel' => $newmodel,
				'specification' => $specification,
				'mrId' => $mrId,
				'selectedItems' => $selectedItems,
			));

		}


	}

	public function actionloadbillItemsModal($mrId = '', $itemId = '')
	{
		$bill_id = '';
		$billModel = '';
		$purchase = '';
		if (isset($_GET['mrId'])) {
			$mrId = $_GET['mrId'];
			$mrModel = MaterialRequisition::model()->findByPk($mrId);
			if (!empty($mrModel)) {
				if (!empty($mrModel->bill_id)) {
					$bill_id = $mrModel->bill_id;
					if (!empty($itemId)) {
						$table_id = MaterialRequisitionItems::Model()->findByPk($itemId);
						if (!empty($table_id)) {
							$billModel = Bills::Model()->findByPk($table_id->bill_id);
						}
					}

					$purchase = Purchase::Model()->findByPk($mrModel->purchase_id);
				}
			}
		}
		if (isset($_GET['itemId'])) {
			$itemId = $_GET['itemId'];
		}
		$this->layout = '//layouts/bill_blank';

		$newmodel = new Purchase();
		$model2 = new Warehousestock;
		$tblpx = Yii::app()->db->tablePrefix;
		$user = Users::model()->findByPk(Yii::app()->user->id);
		$arrVal = explode(',', $user->company_id);
		$newQuery = "";
		$newQuery1 = "";
		foreach ($arrVal as $arr) {
			if ($newQuery)
				$newQuery .= ' OR';
			if ($newQuery)
				$newQuery1 .= ' OR';
			$newQuery .= " FIND_IN_SET('" . $arr . "', company_id)";
			$newQuery1 .= " FIND_IN_SET('" . $arr . "', p.company_id)";
		}
		$specification = $this->actionGetItemCategory($parent = 0, $spacing = '', $user_tree_array = '');
		if (Yii::app()->user->role == 1 || Yii::app()->user->role == 2) {
			$project = Yii::app()->db->createCommand("SELECT pid, name  FROM {$tblpx}projects WHERE " . $newQuery . " ORDER BY name")->queryAll();
		} else {
			$project = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}projects p JOIN {$tblpx}project_assign pa ON p.pid = pa.projectid WHERE " . $newQuery1 . " AND pa.userid = '" . Yii::app()->user->id . "'")->queryAll();
		}
		$vendor = Yii::app()->db->createCommand("SELECT DISTINCT vendor_id, name FROM {$tblpx}vendors WHERE (" . $newQuery . ") ORDER BY name")->queryAll();
		$this->render('//bills/_bill_items_form', array(
			'model2' => $model2,
			'project' => $project,
			'vendor' => $vendor,
			'newmodel' => $newmodel,
			'specification' => $specification,
			'mrId' => $mrId,
			'itemId' => $itemId,
			'bill_id' => $bill_id,
			'billModel' => $billModel,
			'purchase' => $purchase
		));

	}
	public function actionapproveRateQuantity()
	{
		if(isset($_GET['bill_id']) && !empty($_GET['bill_id'])){
			$bill_id = $_GET['bill_id'];

			// Fetch all items matching the bill_id
			$items = Billitem::model()->findAll(array("condition" => "bill_id = :bill_id", "params" => array(":bill_id" => $bill_id)));

			if (!$items) {
				echo json_encode(["status" => "error", "message" => "No items found for bill_id: $bill_id"]);
				exit;
			}

			foreach($items as $item) {
				$item->approve_check = 1;
				if (!$item->save()) {
					echo json_encode(["status" => "error", "message" => "Failed to update item ID: " . $item->id]);
					exit;
				}
			}

			echo json_encode(["status" => "success", "message" => "Bill items approved successfully"]);
			exit;
		}

		echo json_encode(["status" => "error", "message" => "Invalid request"]);
		exit;
	}



}


