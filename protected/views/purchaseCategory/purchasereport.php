<?php
/* @var $this PurchaseCategoryController */
/* @var $model PurchaseCategory */

$this->breadcrumbs = array(
    'Purchase Categories' => array('index'),
    'Manage',
);

/*$this->menu=array(
    array('label'=>'List PurchaseCategory', 'url'=>array('index')),
    array('label'=>'Create PurchaseCategory', 'url'=>array('create')),
);*/

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#purchase-category-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
$tblpx = Yii::app()->db->tablePrefix;
?>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.min.js"></script>

<div class="container" id="project">
    <?php
    $fromdate = (isset($date_from) ? date('d-M-Y', strtotime($date_from)) : '');
    $todate = (isset($date_to) ? date('d-M-Y', strtotime($date_to)) : '');
    ?>
    <?php
    if (!filter_input(INPUT_GET, 'export')) {
        $url_data = ''; ?>
        <div class="expenses-heading header-container">
            <h3>Purchase Report</h3>
            <div class="btn-container">
                <?php if (!empty($_REQUEST['project_id'])) {
                    $url_data = '';
                    if (isset($_REQUEST['project_id'])) {
                        $url_data .= '&project_id=' . $_REQUEST['project_id'];
                    }
                    if (isset($_REQUEST['fromdate'])) {
                        $url_data .= '&fromdate=' . $_REQUEST['fromdate'];
                    }
                    if (isset($_REQUEST['todate'])) {
                        $url_data .= '&todate=' . $_REQUEST['todate'];
                    }
                }
                if (empty($_REQUEST['project_id'])) { ?>
                    <button type="button" id="excel-btn" class="btn btn-info" title="SAVE AS EXCEL"
                        data-href="<?php echo Yii::app()->request->requestUri . '&export=csv' . $url_data ?>">
                        <i class="fa fa-file-excel-o button-icon" aria-hidden="true"></i></button>
                    <button type="button" id="pdf-btn" class="btn btn-info" title="SAVE AS PDF"
                        data-href="<?php echo Yii::app()->request->requestUri . '&export=pdf' . $url_data ?>">
                        <i class="fa fa-file-pdf-o button-icon" aria-hidden="true"></i>
                    </button>
                <?php } ?>
            </div>
        </div>
    <?php } else { ?>
        <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/pdfstyle.css">
    <?php } ?>
    <div class="page_filter clearfix custom-form-style">
        <form id="projectform"
            action="<?php $url = Yii::app()->createAbsoluteUrl("projects/projectsquantityreport"); ?>" method="POST">
            <?php if (!filter_input(INPUT_GET, 'export')) { ?>
                <div class="search-form">
                    <div class="row">
                        <div class="form-group col-xs-12 col-sm-3 col-md-2 ">
                            <?php echo CHtml::label('Projects  ', ''); ?>

                            <?php
                            echo CHtml::dropDownList('project_id', 'project_id', CHtml::listData(Projects::model()->findAll(array(
                                'select' => array('pid, name'),
                                'order' => 'name',

                                'distinct' => true
                            )), 'pid', 'name'), array('empty' => 'Select Project', 'id' => 'project_id', 'options' => array($project_id => array('selected' => true))));
                            ?>

                        </div>
                        <div class="form-group col-xs-12 col-sm-3 col-md-2 ">
                            <?php echo CHtml::label('Date From ', ''); ?>
                            <?php echo CHtml::textField('date_from', (!empty($date_from) ? date('d-M-Y', strtotime($date_from)) : ''), array("id" => "date_from", "readonly" => true, "class" => "form-control")); ?>
                        </div>
                        <div class="form-group col-xs-12 col-sm-3 col-md-2 ">

                            <?php echo CHtml::label('Date To ', ''); ?>
                            <?php echo CHtml::textField('date_to', (!empty($date_to) ? date('d-M-Y', strtotime($date_to)) : ''), array("id" => "date_to", "readonly" => true, "class" => "form-control")); ?>

                        </div>
                        <div class="form-group col-xs-12 col-sm-3 col-md-2 text-sm-left text-right">
                            <label class="d-sm-block d-none">&nbsp;</label>
                            <div>
                                <?php echo CHtml::submitButton('GO', array('id' => 'btSubmit', 'class' => 'btn btn-sm btn-primary')); ?>
                                <?php echo CHtml::resetButton('Clear', array('class' => 'btn btn-sm btn-default', 'onclick' => 'javascript:location.href="' . $this->createUrl('purchasereport') . '"')); ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </form>
    </div>
    <?php if (!empty($_REQUEST['project_id'])) { ?>
        <div id="contenthtml" class="contentdiv">
            <div id="" class="pull-right">



            </div>

            <h2 class="text-center"></h2>
            <div class="row">
                <div class="col-xs-12">
                    <div class="heading-title">Purchase Report of
                        <?php echo $model["name"]; ?>
                    </div>
                    <div class="dotted-line"></div>
                </div>
            </div>
            <div class="table-wrapper  margin-top-20">
                <table class="table table-bordered ">
                    <tr>
                        <td><b>Client : </b><?php echo $model["clientname"]; ?>
                        </td>
                    </tr>
                    <tr>
                        <td><b>Project Name : </b><?php echo $model["name"]; ?>
                        </td>
                    </tr>
                    <tr>
                        <td><b>Site : </b><?php echo $model["site"]; ?>
                        </td>
                    </tr>
                </table>
                <table class="table table-bordered ">
                    <thead class="entry-table sticky-thead">
                        <tr>
                            <th>Sl No</th>
                            <th>Category</th>
                            <th>Quantity</th>
                            <th>Amount</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i = 1;
                        $quantitySum = 0;
                        $amountSum = 0;
                        // echo "<pre>";print_r($itemmodelcat);exit;
                        foreach ($itemmodelcat as $modelcat) {
                            $quantitySum = $quantitySum + $modelcat["billitemsum"];
                            $amountSum = $amountSum + $modelcat["itemtotalamount"];
                            ?>
                            <tr>
                                <td>
                                    <?php echo $i; ?>
                                </td>
                                <td>
                                    <?php echo $modelcat["category_name"]; ?>
                                </td>
                                <td class="text-right">
                                    <?php echo Controller::money_format_inr($modelcat["billitemsum"], 2); ?>
                                </td>
                                <td class="text-right"><a target="_blank" href="<?php echo Yii::app()->createAbsoluteUrl('purchaseCategory/purchasereportdetails', array(
                                    'category_id' => $modelcat["id"],
                                    'project_id' => $_REQUEST['project_id'],

                                )); ?>">
                                        <?php echo Controller::money_format_inr($modelcat["itemtotalamount"], 2); ?>
                                    </a></td>
                            </tr>
                            <?php
                            $i = $i + 1;
                        }
                        ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th></th>
                            <th></th>
                            <th class="text-right">
                                <?php echo Controller::money_format_inr($quantitySum, 2); ?>
                            </th>
                            <th class="text-right">
                                <?php echo Controller::money_format_inr($amountSum, 2); ?>
                            </th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    <?php } else {
        $this->widget('zii.widgets.CListView', array(
            'dataProvider' => $dataProvider,
            'itemView' => '_newpurchasereport',
            'template' => '<div>{summary}{sorter}</div><div class="table-wrapper margin-top-10"><div class="table-responsive" id="table-wrapper"><table class="table total-table ">{items}</table></div></div>{pager}',
        ));
    } ?>
</div>
<script>
    $(document).ready(function () {
        $(".select_box").select2();
        $("#project_id").select2();
        $("#date_from").datepicker({
            dateFormat: 'dd-M-yy',
            maxDate: $("#date_to").val()
        });
        $("#date_to").datepicker({
            dateFormat: 'dd-M-yy',
        });
        $("#date_from").change(function () {
            $("#date_to").datepicker('option', 'minDate', $(this).val());
        });
        $("#date_to").change(function () {
            $("#date_from").datepicker('option', 'maxDate', $(this).val());
        });
    });
    (function () {
        'use strict';
        var container = document.querySelector('.container');
        var table = document.querySelector('table');
        var leftHeaders = [].concat.apply([], document.querySelectorAll('tbody th'));
        var topHeaders = [].concat.apply([], document.querySelectorAll('thead th'));
        var topLeft = document.createElement('div');
        var computed = window.getComputedStyle(topHeaders[0]);
        container.appendChild(topLeft);
        topLeft.classList.add('top-left');
        topLeft.style.width = computed.width;
        topLeft.style.height = computed.height;

        container.addEventListener('scroll', function (e) {
            var x = container.scrollLeft;
            var y = container.scrollTop;

            leftHeaders.forEach(function (leftHeader) {
                leftHeader.style.transform = translate(x, 0);
            });
            topHeaders.forEach(function (topHeader, i) {
                if (i === 0) {
                    topHeader.style.transform = translate(0, y);
                } else {
                    topHeader.style.transform = translate(0, y);
                }
            });
            topLeft.style.transform = translate(x, y);
        });

        function translate(x, y) {
            return 'translate(' + x + 'px, ' + y + 'px)';
        }
    })();
    $("#excel-btn").click(function () {
        var redirectUrl = $(this).data("href");
        window.location.href = redirectUrl;
    });
    $("#pdf-btn").click(function () {
        var redirectUrl = $(this).data("href");
        window.location.href = redirectUrl;
    });
</script>