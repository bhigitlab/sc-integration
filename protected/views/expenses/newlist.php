<?php
$this->breadcrumbs=array(
	'performa invoice',)
?>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/tableHeadFixer.js"></script>
<?php
$receipt    = 0;
$paid       = 0;
$grandgst   = 0;
$grandtotal = 0;
$tdsamount  = 0;
$expamount  = 0;
$beneficiary_amt =0;
foreach($newmodel as $expenses) {
    $receipt = $receipt + $expenses["receipt"];
    $paid    = $paid + $expenses["paid"];
    $grandtotal = $grandtotal + $expenses["amount"];
    $grandgst = $grandgst + $expenses["expense_sgst"]+$expenses["expense_cgst"]+ $expenses["expense_igst"];
    $tdsamount = $tdsamount + $expenses["expense_tds"];
    $expamount = $expamount + $expenses["expense_amount"];
    $beneficiary_amt = $beneficiary_amt + $expenses["paidamount"];
}
?>
<div class="" id="newlist">
<?php
    $sort = new CSort;
    $sort->defaultOrder = 'exp_id DESC';
    $sort->attributes = array('exp_id' => 'Expense Id',
        'bill_id'=> 'Bill Id',
        'amount' => 'Total Amount',
        'paid' => 'Paid Amount');
    $dataProvider=new CArrayDataProvider($newmodel, array(
        'id'=>'exp_id', 
        'sort'=>$sort,
        'keyField'=>'exp_id', 
        'pagination'=>false
    ));
    $this->widget('zii.widgets.CListView', array(
        'dataProvider'=>$dataProvider,
        'viewData' => array( 'receipt' => $receipt, 
            'paid' => $paid, 'grandtotal' => $grandtotal, 
            'grandgst'=>$grandgst, 
            'tdsamount'=>$tdsamount, 
            'expamount' => $expamount,
            'beneficiary_amt' => $beneficiary_amt 
            ), 
        'itemView'=>'_newview',
        'id'=>'daybooklist',
        'enableSorting'=>1,
        'enablePagination' => true,
        'template' => ' <div class="table-wrapper margin-top-25"><ul class="legend margin-bottom-0">
         <li><span class="declined"></span> Petty cash Payment Approval</li>
                  <li><span class="unreconciled"></span> UnReconciled</li>          
    </ul><div class="summary text-right">{summary}{sorter}</div><div id="table-wrapper"><table class="table total-table" id="fixTable">{items}</table></div></div>{pager}',
        'enablePagination' => false
    )); ?>
</div>
<script>
    $(document).ready(function() {
        $("#fixTable").tableHeadFixer({'left' : 1, 'foot' : true, 'head' : true}); 
	});
    $(document).ready(function() {
       $(".popover-test").popover({
            html: true,
                content: function() {
                  return $(this).next('.popover-content').html();
                }
        });
    $('[data-toggle=popover]').on('click', function (e) {
       $('[data-toggle=popover]').not(this).popover('hide');
    });
    $('body').on('hidden.bs.popover', function (e) {
        $(e.target).data("bs.popover").inState.click = false;
    });
    $('body').on('click', function (e) {
        $('[data-toggle=popover]').each(function () {
            if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                $(this).popover('hide');
            }
        });
    });

});
</script>
<style>
    #parent {max-height: 400px;}
    th{background: #eee;}
    /* .pro_back{background: #fafafa;} */
    .total-table{margin-bottom: 0px;}
    th,td {border-right: 1px solid #ccc !important;}
    .petty_approval{
        background-color: #2e6da438;
        border: 1px solid #2e6da438;
        width: 12px;
        height: 12px; 
        margin-right:10px;       
    }
    .pending_reconil{
        background-color: #5bc0de3b;
        border: 1px solid #5bc0de3b;
        width: 12px;
        height: 12px; 
        margin-right:10px;
    }

    #daybooklist{
        margin-top: -17px;
    }
</style>
