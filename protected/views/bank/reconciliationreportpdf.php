<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/pdfstyle.css">

<div class="container pdf_spacing" id="user">
    <?php
    $company_address = Company::model()->findBypk(Yii::app()->user->company_id);	
    ?>
        
    <h3 class='text-center'>Bank Reconciliation</h3>
    <?php $this->widget('zii.widgets.CListView', array(
            'dataProvider' => $dataProvider,
            'viewData' => array( 'paymentamount' => $paymentamount, 'receiptamount' => $receiptamount ),
            'itemView' => '_reconreportnewviewpdf', 'template' => '<table border="1" cellpadding="10" class="table  list-view sorter" id="fixTable">{items}</table>',
    )); ?>
</div>