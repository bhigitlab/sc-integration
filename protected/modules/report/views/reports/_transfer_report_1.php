<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/tableHeadFixer.js"></script>

<style>
    .table-holder {
        max-height: 350px;
        overflow: auto;
    }
</style>
<div class="container" id="project">
    <h3>Transfer Report</h3>
    <?php
    $warehouse_from_arr = array();
    $warehouse_to_arr = array();
    $warehouse = Warehouse::model()->findAll();
    $fromdate = (isset($date_from) && !empty($date_from)) ? date('d-M-Y', strtotime($date_from)) : "";
    $todate = (isset($date_to) && !empty($date_to)) ? date('d-M-Y', strtotime($date_to)) : "";
    $filter_status = isset($filter_status) ? $filter_status : '';
    ?>


    <div class="page_filter custom-form-style mb-10">
        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'action' => Yii::app()->createUrl($this->route),
            'method' => 'get',
        ));
        ?>

        <div class="row">
            <div class="form-group col-xs-12 col-sm-4 col-md-2">
                <label>Type</label>
                <?php
                if (yii::app()->user->role == 5) {
                    $data_arr = Controller::getCategoryOptions($type = 1);
                    $data = CHtml::listData(Controller::getCategoryOptions($type = 1), 'id', 'text', 'group');
                } else {
                    $data_arr = Controller::getCategoryOptions();
                    $data = CHtml::listData(Controller::getCategoryOptions(), 'id', 'text', 'group');
                }

                if ($warehouse_from == '' && $warehouse_to == '') {
                    $warehouse_from_arr = $data;
                    $warehouse_to_arr = $data;
                } else {
                    if ($warehouse_from != '') {
                        $warehouse_from_arr = CHtml::listData(Controller::addWarehouseCategoryOptions($data_arr, $warehouse_from), 'id', 'text', 'group');
                    } else {
                        $warehouse_from_arr = $data;
                    }
                    if ($warehouse_to != '') {
                        $warehouse_to_arr = CHtml::listData(Controller::addWarehouseCategoryOptions($data_arr, $warehouse_to), 'id', 'text', 'group');
                    } else {
                        $warehouse_to_arr = $data;
                    }
                }

                $type = isset($_REQUEST['Type']) ? $_REQUEST['Type'] : "";
                $options = array('1' => 'Receipt', '2' => 'Despatch');
                echo CHtml::dropDownList('Type', $type, $options, array('empty' => 'Choose Type', 'class' => 'form-control wh_type'));
                ?>
                <span class="error"></span>
            </div>
            <div class="form-group col-xs-12 col-sm-4 col-md-2">
                <label>View</label>
                <?php
                $viewtype = isset($_REQUEST['viewType']) ? $_REQUEST['viewType'] : "";
                $viewoptions = array('1' => 'All View', '2' => 'Item View');
                echo CHtml::dropDownList('viewType', $viewtype, $viewoptions, array('class' => 'form-control viewType'));

                ?>
            </div>
            <div class="form-group col-xs-12 col-sm-4 col-md-2">
                <label>Project / Vendor</label>
                <?php
                $project_val = isset($_REQUEST['wh_project']) ? $_REQUEST['wh_project'] : "";
                echo CHtml::dropDownList('wh_project', $project_val, array('' => 'Select Project/Vendor'), array('class' => 'form-control wh_project'));
                ?>
            </div>
            <div class="form-group col-xs-12 col-sm-4 col-md-2">
                <label>Status</label>
                <?php
                $status_arr['1'] = 'Dispatched';
                $status_arr['2'] = 'In Transit';
                $status_arr['3'] = 'Received';
                $status_arr['4'] = 'Deleted';
                echo CHtml::dropDownList('status', 'status', $status_arr, array('class' => 'select_box', 'empty' => 'Select Status    ', 'id' => 'status', 'options' => array($filter_status => array('selected' => true))));
                ?>
            </div>
            <div class="form-group col-xs-12 col-sm-4 col-md-2">
                <label>From</label>
                <?php echo CHtml::textField('date_from', $fromdate, array("id" => "date_from", "placeholder" => "From", "readonly" => true, 'class' => 'form-control')); ?>
            </div>
            <div class="form-group col-xs-12 col-sm-4 col-md-2">
                <label>To</label>
                <?php echo CHtml::textField('date_to', $todate, array("id" => "date_to", "placeholder" => "To", "readonly" => true, 'class' => 'form-control')); ?>
            </div>

            <div class="col-xs-12 text-right">
                <?php echo CHtml::submitButton('GO', array('id' => 'btSubmit', 'class' => 'btn btn-info btn-sm')); ?>
                <?php echo CHtml::resetButton('Clear', array('class' => 'btn btn-info btn-sm', 'onclick' => 'javascript:location.href="' . $this->createUrl('transferreport') . '"')); ?>
            </div>
        </div>
        <?php $this->endWidget(); ?><br>
    </div>
    <?php
    if (isset($_REQUEST['viewType']) && $_REQUEST['Type'] != "") {
        //RECEIPT
        if ($_REQUEST['Type'] == 1 && $_REQUEST['viewType'] == 1) {

            $fromdate = (isset($_REQUEST['date_from']) && !empty($_REQUEST['date_from'])) ? date('Y-m-d', strtotime($_REQUEST['date_from'])) : "";
            $todate = (isset($_REQUEST['date_to']) && !empty($_REQUEST['date_to'])) ? date('Y-m-d', strtotime($_REQUEST['date_to'])) : "";

            $this->renderPartial('wh_receipt', array('date_from' => $fromdate, 'date_to' => $todate));
        }

        if ($_REQUEST['Type'] == 1 && $_REQUEST['viewType'] == 2 && $_REQUEST['wh_project'] == "") {

            $fromdate = (isset($_REQUEST['date_from']) && !empty($_REQUEST['date_from'])) ? date('Y-m-d', strtotime($_REQUEST['date_from'])) : "";
            $todate = (isset($_REQUEST['date_to']) && !empty($_REQUEST['date_to'])) ? date('Y-m-d', strtotime($_REQUEST['date_to'])) : "";

            $this->renderPartial('wh_receipt_item_list', array('date_from' => $fromdate, 'date_to' => $todate));
        }

        if ($_REQUEST['Type'] == 1 && $_REQUEST['viewType'] == 2 && $_REQUEST['wh_project'] != "") {
            $this->renderPartial('wh_receipt_projectview', array('vendor' => $_REQUEST['wh_project']));
        }

        //DESPATCH
        if ($_REQUEST['Type'] == 2 && $_REQUEST['viewType'] == 1) {

            $fromdate = (isset($_REQUEST['date_from']) && !empty($_REQUEST['date_from'])) ? date('Y-m-d', strtotime($_REQUEST['date_from'])) : "";
            $todate = (isset($_REQUEST['date_to']) && !empty($_REQUEST['date_to'])) ? date('Y-m-d', strtotime($_REQUEST['date_to'])) : "";

            $this->renderPartial('wh_despatch', array('date_from' => $fromdate, 'date_to' => $todate));
        }

        if ($_REQUEST['Type'] == 2 && $_REQUEST['viewType'] == 2 && $_REQUEST['wh_project'] == "") {

            $fromdate = (isset($_REQUEST['date_from']) && !empty($_REQUEST['date_from'])) ? date('Y-m-d', strtotime($_REQUEST['date_from'])) : "";
            $todate = (isset($_REQUEST['date_to']) && !empty($_REQUEST['date_to'])) ? date('Y-m-d', strtotime($_REQUEST['date_to'])) : "";

            $this->renderPartial('wh_despatch_item_list', array('date_from' => $fromdate, 'date_to' => $todate));
        }

        if ($_REQUEST['Type'] == 2 && $_REQUEST['viewType'] == 2 && $_REQUEST['wh_project'] != "") {

            $this->renderPartial('wh_despatch_projectview', array('project' => $_REQUEST['wh_project']));
        }
    } else {
        ?>
        <div class="row">
            <div class="col-xs-12">
                <input type="hidden" id="trasfer_report_item_count" value=<?= $result_data_count ?> />
                <?php
                $this->widget('zii.widgets.CListView', array(
                    'dataProvider' => $dataprovider,
                    'itemView' => '_report_data',
                    'template' => '<div class="sub-heading"><div>{sorter}</div>{summary}</div><div class="table-responsive" id="parent"><table cellpadding="10" class="table  list-view " id="fixtable">{items}</table></div>{pager}',
                    'emptyText' => '<table cellpadding="10" class="table"><thead class="entry-table">
                            <tr>
                            <th>SI.No</th>
                            <th>Dispatch No</th>
                            <th>Receipt No</th>
                            <th>Type</th>
                            <th>From Warehouse</th>
                            <th>To Warehouse</th>
                            <th>Status</th>
                            <th>Dispatched Date</th>
                            <th>Received Date</th>
                            <th></th>
                            </tr></thead><tr><td colspan="13" class="text-center">No results found</td></tr></table>'
                ));
                ?>
            </div>
        </div>
    <?php } ?>

</div>

<script>
    $(document).ready(function () {
        var trasfer_report_item_count = $("#trasfer_report_item_count").val();
        $(".wh_type,.viewType").select2();
        $("#warehouse_from").select2();
        $("#warehouse_to").select2();
        $("#status").select2();
        $('#warehouse_from').change(function () {
            var warehouse_from = $(this).val();
            $("#warehouse_to option[value='" + warehouse_from + "']").remove();
        });
        $("#date_from").datepicker({
            dateFormat: 'dd-mm-yy'
        });
        $("#date_to").datepicker({
            dateFormat: 'dd-mm-yy'
        });
    });

    $("#myTable").tableHeadFixer({
        'left': false,
        'foot': true,
        'head': true
    });

    $(".viewType").change(function () {
        var view_type = $(this).val();
        var wh_type = $(".wh_type").val();
        if ((view_type == '2') || (view_type == '3')) {
            if (wh_type == "") {
                $(".wh_type").siblings('.error').text('Choose Type');
                $("#btSubmit").attr('disabled', true);
            } else {
                getProjectlist(view_type, wh_type);
                $(".wh_type").siblings('.error').text('');
                $("#btSubmit").attr('disabled', false);
            }
        } else {
            $(".wh_type").siblings('.error').text('');
            $("#btSubmit").attr('disabled', false);
        }
    })

    $("#Type").change(function () {
        $(".viewType").change();
    })

    $(function () {
        $(".viewType").change();
    })

    function getProjectlist(view_type, wh_type) {
        $.ajax({
            url: "<?php echo Yii::app()->createUrl("report/reports/getProjectlist") ?>",
            data: {
                'view_type': view_type,
                'wh_type': wh_type
            },
            type: "POST",
            dataType: 'json',
            success: function (data) {
                $("#wh_project").html(data.data);
                optionValue = "<?php echo isset($_REQUEST['wh_project']) ? $_REQUEST['wh_project'] : "" ?>";
                $("#wh_project").find("option[value=" + optionValue + "]").attr('selected', 'selected');
            }
        });
    }

</script>