<?php
/* @var $this ClientsController */
/* @var $model Clients */
/* @var $form CActiveForm */
//Yii::app()->clientScript->registerScriptFile('/path/to/parsedown.js');
?>

<div class="">
<div class="col-md-12" style="margin-bottom:10px; ">
        <button  id="showGalleryButton" style="float:right;margin-top: 12px;" class="btn btn-primary">Show Media Gallery</button>
</div>
<!-- Modal for the image gallery -->
<div id="imageGalleryModal" class="modal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" style="margin-top:5px;">Image Gallery </h5><span>[Use these image keywords inside pdf layouts, Click the image and paste the image keyword in textarea]</span>
                    <button type="button" class="close_btn" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                <div id="imageRow">
                        <!-- Images will be dynamically loaded here -->
                </div>
                </div>
            </div>
        </div>
    </div>
</div>             
    
<form id="template-form"> <!-- Add the plain HTML form here -->
        <input type="hidden" id="ProjectTemplate[template_id]" name="ProjectTemplate[template_id]" value="<?php echo $model->template_id; ?>">
<div class="panel-body">
<div id="errormessage"></div>  
        
    <div class="row addRow">
    <div class="col-md-6">
        <?php echo CHtml::label('PDF Type', 'pdf_type'); ?>
                    <?php
                    $data = array(
                        'quotation' => 'Quotation PDF',
                        'invoice' => 'Invoice PDF',
                        'purchase' => 'Purchase PDF'
                    );
                    echo CHtml::dropDownList('redirectDropdown', 'invoice', $data, array(
                        'onchange' => 'redirectToSelectedOption()',
                        'class' => 'form-control',
                    ));
                    ?>
                    </br>  
    </div>
    <div class="col-md-12">
        <p>* Use following code in template :
        {base_url},{terms_and_conditions},{logo},{item_list},{dynamic_item_list},{company},{project},{client},{client_address},{client_gst},{date},{invoice_number},{payment_stage},{amount},{total_amount},{total_tax},{round_off},{grand_total},{write_off},{balance},{balance_with_additional},{app_name},{company_email},{company_address},{company_gst},{company_bank},{balance_in_words},{balance_additional_in_words},{tax_in_words},{total_sgst},{total_cgst},{total_igst},{additional_charge}
        *</p>
    </div>

    <div class="col-md-6">
        <div class="col-md-6">
            <label for="ProjectTemplate_template_name">Template Name</label>
            <input type="text" name="ProjectTemplate[template_name]" id="ProjectTemplate[template_name]" class="form-control" value="<?php echo $model->template_name; ?>">
        </div>
        <div class="col-md-6">        
            <label for="ProjectTemplate_company_id">Company</label>
            <select name="ProjectTemplate[company_id]"    class="inputs target company_id form-control"  id="ProjectTemplate[company_id]" style="width:270px">
                    <option value="">Choose Company</option>
                    <?php
                    $user = Users::model()->findByPk(Yii::app()->user->id);
                    $arrVal = explode(',', $user->company_id);
                    $newQuery = "";
                    foreach($arrVal as $arr) {
                    	if ($newQuery) $newQuery .= ' OR';
                            $newQuery .= " FIND_IN_SET('".$arr."', id)";
                    }
                    $companyInfo = Company::model()->findAll(array('condition' => $newQuery));
                    foreach($companyInfo as $key => $value){
                    ?>  
                    <option value="<?php echo $value['id']; ?>" <?= $value['id'] == $model->company_id?'selected':''?>><?php echo $value['name']; ?></option>
                    <?php       
                    }
                    ?>
                </select>
        </div>      
    </div>
    <div class="col-md-6">
            <label for="ProjectTemplate_description">Description</label>
            <textarea name="ProjectTemplate[description]" id="ProjectTemplate[description]" rows="6" cols="50" class="form-control"><?php echo $model->description; ?></textarea>
    </div>  
    <div class="col-md-6">
        <?php
                    echo CHtml::label('Template', 'template_format_invoice');
                    if ($model->template_id) {
                        if ($count_revisions > 1) {
                            echo '<a id="1" class="version_change">Previous Version </a><--->';
                        }
                        if ($count_revisions > 0) {
                            echo '<a id="2" class="version_change">Current  Version</a>';
                        }
                    }
                    ?>
                    <textarea name="ProjectTemplate[template_format_invoice]" class="form-control" id="editor" style="height: 800px; overflow: auto;"><?php echo $model['template_format_invoice']; ?></textarea>
    </div>
    <div class="col-md-6">
                
            <label for="sample_template_data">Sample Data</label>
                <?php
                $company_details = Company::model()->findByPk($model->company_id);
                $logo = '<img src="' . Yii::app()->request->baseUrl . '/uploads/image/' . $company_details['logo'] . '" alt="" class="pop" modal-src="' . Yii::app()->request->baseUrl . '/uploads/image/' . $company_details['logo'] . '" style="max-height:45px;">';

                $sample_data= 
                [
                    'base_url' => Yii::app()->theme->baseUrl, 
                    'terms_and_conditions' => '5 Year service warranty for all laminated marine ply under normal usage',
                    'location_name' => 'Ernakulam',
                    'client' => 'MR. RATHEESH',
                    'phone_no' => '+91 11111111',
                    'email' => 'test@gmail.com',
                    'address' => '123 Main Road, Ravipuram, Ernakulam',
                    'logo' => $logo,
                    'company' => 'Company1',
                    'company_address' => "ORIENT SQUARE, 2ND FLOOR, SAHODARAN
                                        AYYAPPAN RD, KADAVANTHRA JUNCTION,
                                        KOCHI, KERALA 682020",
                    'company_phone' => '+91 44444444',
                    'company_email' => 'company2@gmail.com',
                    'company_bank' => 'SBI Kochi,KADAVANTHRA JUNCTION,
                                        KOCHI, KERALA 682020',
                    'estimator_name' => 'Estimator1',
                    'estimator_phone' => '+91 33333333',
                    'invoice_no' => 'QUT-2023-020',
                    'date_quotation' => '28-04-2023',
                    'sales_executive_name' => 'Executive1',
                    'sales_executive_phone' => '+91 22222222',
                    'sales_executive_designation' => 'executive1@gmail.com',
                    'material_specification' => array(
                        array('brand' => 'Ply marc', 'specification' => '12mm Grade marine ply'),
                        array('brand' => 'Glass house', 'specification' => '12 mm glass')
                    ),
                    'item_list' => array(
                        array(
                            "description" => "Description1",
                            "hsn_code" => "hsn01",
                            "sgst" => "9",
                            "sgst_amount" => "1800",        
                            "cgst" => "9",  
                            "cgst_amount" => "1800",          
                            "igst" => "0",                         
                            "igst_amount" => "0",                         
                            "amount" => "20000.00",                         
                            "tax_amount" => "3600",                         
                            "igst" => "0"  
                        ),
                    ),
                    'discount_percent'=> '(18%)',
                    "gst_percentage" => "5",
                    "total" => "23600",
                    "amount" => "23600",
                    "project" => "Amster",
                    "balance" => "23600",
                    "tax_amount" => "3600",
                    "date" => "9-11-2023",
                    "invoice_number" => "INV01",
                    "payment_stage" => "1",
                    "total_amount" => "23600",
                    "total_tax" =>"3600",
                    "round_off" => "0",
                    "grand_total" => "23600",
                    "write_off" => "0",
                    "app_name" => "Accounts",
                    "company_gst" => "9",
                    "total_cgst" => "9",
                    "total_sgst" => "9",
                    "total_igst" => "0",
                    "gst_amount" => "3600"
                ];
                $jsonString = json_encode($sample_data, JSON_PRETTY_PRINT);
                ?>
                    <textarea name="ProjectTemplate[template_format_data]" class="form-control" id="sample_template_data" style="height: 800px; overflow: auto;">
                    <?php echo $jsonString; ?>
                </textarea>
           
    </div>

        </div>
    </div>

    <div class="panel-footer save-btnHold text-center">
        <button type="button" class="btn" id="previewBtn">Preview</button>
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'btn btn-info')); ?> 
        <?php if (Yii::app()->user->role == 1) { 
        	if(!$model->isNewRecord){  /*
        	?> 
            <a class="btn del-btn" href="<?php echo $this->createUrl('clients/deleteclients', array('id' => $model->cid)) ?>" class="deletebtn" onclick="return confirm('Are you sure that you want to delete?');">Delete </a>                           
        <?php */ } } ?>
       <button class="btn" data-dismiss="modal" onclick="javascript:window.location.reload()">Close</button>
            
    </div>
</form>
</div><!-- form -->

<?php $template_preview = $this->createUrl('/projectTemplate/preview'); ?>
<?php $switch_template = $this->createUrl('/projectTemplate/switchTemplate'); ?>
<?php $quotation_pdf_url = $this->createUrl("/projectTemplate/index&type=quotation&tid=$model->template_id"); ?>
<?php $invoice_pdf_url = $this->createUrl("/projectTemplate/index&type=invoice&tid=$model->template_id"); ?>
<?php $form_submission_url = $this->createUrl("/projectTemplate/update&id=$model->template_id&type=invoice"); ?>

<style>
    .addRow label {display: inline-block;}
    input[type="radio"]{
        margin: 4px 4px 0;
    }

    input[type="checkbox"][readonly] {
        pointer-events: none;
      }
    img.img-fluid {
    max-width: 100px;
    }
    .txt-center{
        text-align:center;
    }
    .close_btn {
    float: right;
    font-size: 14px;
    font-weight: bold;
    line-height: 1;
    color: #000;
    text-shadow: 0 1px 0 #fff;
    opacity: 0.2;
    margin-bottom:10px;
    filter: alpha(opacity=20);
  }
      
</style>
<script>
$( document ).ready(function() {
   
    $('#previewBtn').on('click', function() {
        var ptype = document.getElementById('redirectDropdown').value;        
        var params = {};
            params['template_data'] = $('#editor').val();
            params['sample_template_data'] = $('#sample_template_data').val(),
               
                    $.ajax({
                        type: "POST",
                        url: "<?php echo $this->createUrl('/projectTemplate/preview&type=') ?>" + ptype,
                        data:  params,
                        success: function(output){console.log(output);
                            //popup = window.open('', 'PDF Popup', params);
                            popup = window.open('', '', 'width=800,height=600');
                            // set the document size to A4 size
                            popup.document.body.style.width = '210mm';
                            popup.document.body.style.height = '297mm';
                            popup.document.body.style.margin = '0 auto';
                            popup.document.write(output);
                            popup.document.write("<div class='text-center' style='padding-bottom:20px'><button onclick='window.close()'>Close</button></div>");
                            popup.document.close();
                        }
                    });

    });

    $('.version_change').on('click', function(){
        
        var params = {};
        params['id'] = '<?php echo $model->template_id?>';
        params['type'] = $(this).attr('id');
        if(params['type'] == 1)
            var result = confirm("Are you sure you want to switch to previous version?");
        else
            var result = confirm("Are you sure you want to switch to latest version?");
        if (result) {
            $.ajax({
                type: "POST",
                url: '<?php echo $switch_template; ?>',
                data:  params,
                success: function(output){
                    $('#editor').val(output);
                }
             });
        }    
    });
    
    $('#template-form').on('submit', function(e) {
        e.preventDefault();
        var formData = $(this).serialize();
        var ptype = document.getElementById('redirectDropdown').value;
        var error=0;
        if(ptype=='invoice'){
            var company_id =document.getElementById('ProjectTemplate[company_id]').value;
            if(company_id ==''){
                error=1;
                $("#errormessage").show()
                    .html('<div class="alert alert-danger">Please choose company</div>')
                    .fadeOut(10000);
					$("html, body").animate({
                                scrollTop: 0
                            }, 600);
            }
        }
        var id =document.getElementById('ProjectTemplate[template_id]').value;
        if(!error){
            $.ajax({
                type: "POST",
                url: "<?php echo $this->createUrl('ProjectTemplate/SaveTemplateData&id=') ?>" + id + "&type=" + ptype,
                data: formData,
                success: function(response) {
                    window.location.href = "<?php echo $this->createUrl('ProjectTemplate/index') ?>";
                }
            });
        }
    });  
});
function redirectToSelectedOption() {
    var type = document.getElementById('redirectDropdown').value;
    var tid =document.getElementById('ProjectTemplate[template_id]').value;
    
    editTemplate(tid,type);
}
function displayGallery() {
    $.ajax({
        type: "GET",
        url: "<?php echo $this->createUrl('ProjectTemplate/getGalleryMedia') ?>", // Replace this with the URL of your PHP script
        success: function(data) {
            // Assuming your PHP script returns JSON data containing image file names
            var images = JSON.parse(data);

            // Initialize HTML variable for image gallery
            var html = '<div class="row" style="margin-bottom:10px;">'; // Start the first row
            var count = 0; // Initialize image count

            // Loop through each image file in the data
            for (var i = 0; i < images.length; i++) {
                var imageName = "{image:: " + images[i] + "||width:'250px;'}";
                var imageSrc = "<?php echo Yii::app()->request->baseUrl; ?>/uploads/company_assets/media/" + images[i];
                html += '<div class="col-md-4 txt-center">';
                html += '<img src="' + imageSrc + '" alt="' + imageName + '" class="img-fluid" data-file="'+ imageName +' " title="click image to copy keyword">';
                html += '<p><b>'+imageName+'</b></p>';
                html += '</div>';

                // Increment image count
                count++;

                // Start a new row after every third image
                if (count % 3 == 0) {
                    html += '</div>'; // Close the previous row
                    html += '<div class="row">'; // Start a new row
                }
            }

            // Close the last row
            html += '</div>';

            // Append the generated HTML to the #imageRow element
            $("#imageRow").html(html);
        }
    });
}

$(document).ready(function() {
    $("#showGalleryButton").click(function() {
    // Call the PHP function to display images
    displayGallery(); 

    // Show the modal
        $("#imageGalleryModal").modal("show");
    });
});
// Add an event listener to the document for click events on any element with the 'img-fluid' class
$(document).on('click', '.img-fluid', function() {
    // Retrieve the value passed for this image in the 'data-file' attribute
    var fileName = $(this).data('file');
    
    // Create a hidden textarea element
    var textarea = document.createElement('textarea');
    textarea.value = fileName;
    document.body.appendChild(textarea);

    // Select the text in the textarea
    textarea.select();

    try {
        // Execute the copy command
        var successful = document.execCommand('copy');
        if(successful){
            $("#imageGalleryModal").modal("hide");
        }
        var message = successful ? 'Image Keyword' + fileName +' Copied to clipboard: Paste it  where ever you want the image in Template layout': 'Failed to copy to clipboard';
       
        console.log(message);
        
        alert(message);
        $("#editor").focus();
        // Optionally, you can provide some visual feedback to the user
        
    } catch (error) {
        console.error('Error copying to clipboard: ', error);
        // Handle the error gracefully, e.g., by displaying an error message to the user
        alert('Error copying to clipboard: ' + error.message);
    }

    // Remove the textarea from the DOM
    document.body.removeChild(textarea);
});
</script>
