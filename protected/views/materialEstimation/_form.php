<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.min.js"></script>
<?php 
if($estimation == 'labour_estimation'){
    $model=$model_labour;
}
elseif($estimation == 'equipment_estimation'){
    $model=$model_equipment;
}
else{
    $model=$model;
}
?>
<div class="form">
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'material-estimation-form',
        'enableAjaxValidation' => false,
    ));
    ?>
    <?php echo $form->errorSummary($model); ?>

    <div class="panel-body" >
       
		<div class="row">
            <div class="col-md-4">
            <h4>Add/Edit Purchase Item</h4>
            </div>
		</div>
        <div class="row">
        <?php echo $form->hiddenField($model, 'project_id', array('id' => 'project_id', 'value' => $project_id)); ?>

        <?php echo CHtml::hiddenField('entry_id', $model->id); ?>

        <div class="row" style="margin-left:20px;">
            <div class="col-md-2">
                <div class="form-group">
                    <?php 
                    $pms_api_integration=ApiSettings::model()->pmsIntegrationStatus();
                    
                    if ($estimation == 'material_estimation') {
                        echo $form->labelEx($model, 'Materials');
                        
                        if($pms_api_integration==0){
                            $materials = CHtml::listData(Materials::model()->findAll(), 'id', 'material_name');
                            echo $form->dropDownList($model, 'material', $materials, array('class' => 'form-control js-example-basic-single', 'prompt' => 'Select Material')); 
                            echo $form->error($model, 'material'); 
                        }
                        else{
                            $materials = CHtml::listData(
                                Materials::model()->findAll(array(
                                    'condition' => 'pms_material_id IS NOT NULL AND pms_material_id != ""'
                                )),
                                'id',
                                'material_name'
                            );
                            echo $form->dropDownList(
                                $model,
                                'material',
                                $materials,
                                array(
                                    'class' => 'form-control js-example-basic-single',
                                    'prompt' => 'Select Material'
                                )
                            );
                            
                            // Display any validation errors for the 'material' attribute
                            echo $form->error($model, 'material'); 
                        }
                    }
                    elseif($estimation == 'labour_estimation') {
                        if($pms_api_integration==0){
                            echo $form->labelEx($model_labour, 'labour'); 
                            $labours = CHtml::listData(LabourWorktype::model()->findAll(), 'type_id', 'worktype');
                            echo $form->dropDownList($model_labour, 'labour', $labours, array('class' => 'form-control js-example-basic-single', 'prompt' => 'Select Labour')); 
                            echo $form->error($model_labour, 'labour');
                        }
                        else{
                            
                            // Fetch labor work types with non-empty pms_labour_id
                            $labours = CHtml::listData(
                                LabourWorktype::model()->findAll(array(
                                    'condition' => 'pms_labour_id IS NOT NULL AND pms_labour_id != ""'
                                )),
                                'type_id',
                                'worktype'
                            );

                            // Render the label for the dropdown
                            echo $form->labelEx($model_labour, 'labour');

                            // Render the dropdown list
                            echo $form->dropDownList(
                                $model_labour,
                                'labour',
                                $labours,
                                array(
                                    'class' => 'form-control js-example-basic-single',
                                    'prompt' => 'Select Labour'
                                )
                            );

                            // Display any validation errors for the 'labour' attribute
                            echo $form->error($model_labour, 'labour');


                        }
                    }
                    else{
                        if($pms_api_integration==0){
                        echo $form->labelEx($model_equipment, 'equipment'); 
                            $equipments = CHtml::listData(Equipments::model()->findAll(), 'id', 'equipment_name');
                            echo $form->dropDownList($model_equipment, 'equipment', $equipments, array('class' => 'form-control js-example-basic-single', 'prompt' => 'Select Equipment')); 
                            echo $form->error($model_equipment, 'equipment');
                        }
                        else{
                            $equipments = CHtml::listData(
                                Equipments::model()->findAll(array(
                                    'condition' => 'pms_equipment_id IS NOT NULL AND pms_equipment_id != ""'
                                )),
                                'id',
                                'equipment_name'
                            );
                            
                            // Render the label for the equipment dropdown
                            echo $form->labelEx($model_equipment, 'equipment');
                            
                            // Render the dropdown list with the filtered equipment options
                            echo $form->dropDownList(
                                $model_equipment,
                                'equipment',
                                $equipments,
                                array(
                                    'class' => 'form-control js-example-basic-single',
                                    'prompt' => 'Select Equipment'
                                )
                            );
                            
                            echo $form->error($model_equipment, 'equipment');
                        } 
                    }
                    ?>
                </div>
                <div id="errorMsg"></div>
            </div>
            <?php if ($estimation == 'material_estimation') { ?>
                <div class="col-md-2">
                <div class="form-group">
                    <?php echo $form->labelEx($model, 'specification'); ?>
                    <?php
                        $specifications = CHtml::listData(Specification::model()->findAll(), 'id', 'specification');
                        
                        // Ensure that $model->specification is an array for multiple selection
                        $selectedSpecifications = !empty($model->specification) ? explode(',', $model->specification) : [];

                        echo $form->dropDownList($model, 'specification', $specifications, array(
                            'class' => 'form-control js-example-basic-single',
                            'multiple' => 'multiple',
                            'prompt' => 'Select one',
                            'options' => array_combine($selectedSpecifications, array_fill(0, count($selectedSpecifications), array('selected' => true)))
                        ));
                    ?>
                    <?php echo $form->error($model, 'specification'); ?>
                    </div>
                </div>
            <?php } ?>
            <div class="col-md-2">
                <div class="form-group">
                    <?php 
                    echo $form->labelEx($model, 'unit'); 
                    
                    // Fetch unit data
                    $unitData = CHtml::listData(Unit::model()->findAll(), 'id', 'unit_name');
                    
                    // Create dropdown list
                    echo $form->dropDownList($model, 'unit', $unitData, array(
                        'class' => 'form-control js-example-basic-single',
                        'prompt' => 'Select Unit'
                    ));
                    
                    echo $form->error($model, 'unit'); 
                    ?>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <?php echo $form->labelEx($model, 'quantity'); ?>
                    <?php echo $form->textField($model, 'quantity', array('class' => 'form-control quantity','id'=>'quantity')); ?>
                    <?php echo $form->error($model, 'quantity'); ?>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <?php echo $form->labelEx($model, 'used_quantity'); ?>
                    <?php echo $form->textField($model, 'used_quantity', array('class' => 'form-control used_quantity','id'=>'used_quantity', 'readonly' => true)); ?>
                    <?php echo $form->error($model, 'quantity'); ?>
                </div>
            </div>
            <?php
            if ($model) {
                $balance_quantity = $model->quantity - $model->used_quantity;
                $balance_amount = $model->amount - $model->used_amount;
            } else {
                $balance_quantity = 0;
                $balance_amount =0;
            }
            ?>
             
            <div class="col-md-2">
                <div class="form-group">
                    <label for="balance_quantity">Bal Quantity</label>
                    <input type="text" class="form-control" id="balance_quantity" name="balance_quantity" value  = "<?php echo $balance_quantity; ?>" readonly>
                </div>
            </div>

        </div>
        <div class="row "  style="margin-left:20px;">
            
            <div class="col-md-2">
                <div class="form-group">
                    <?php echo $form->labelEx($model, 'rate'); ?>
                    <?php echo $form->textField($model, 'rate', array('class' => 'form-control','id' => 'rate')); ?>
                    <?php echo $form->error($model, 'rate'); ?>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <?php echo $form->labelEx($model, 'amount'); ?>
                    <?php echo $form->textField($model, 'amount', array('class' => 'form-control','id' => 'amount' )); ?>
                    <?php echo $form->error($model, 'amount'); ?>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                                    <?php echo $form->labelEx($model, 'used_amount'); ?>
                                    <?php echo $form->textField($model, 'used_amount', array('class' => 'form-control used_amount','id'=>'used_amount', 'readonly' => true)); ?>
                                    <?php echo $form->error($model, 'used_amount'); ?>
                </div>
            </div>
             <div class="col-md-2">
                    <div class="form-group">
                        <label for="balance_amount">Bal Amount</label>
                        <input type="text" class="form-control" id="balance_amount" name="balance_amount" value  = "<?php echo $balance_amount; ?>" readonly>
                    </div>
            </div>

            <div class="col-md-2">
                <div class="form-group">
                    <?php echo $form->labelEx($model, 'Remarks'); ?>
                    <?php echo $form->textField($model, 'remarks', array('class' => 'form-control')); ?>
                    <?php echo $form->error($model, 'remarks'); ?>
                </div>
            </div>
        </div>
        <div class="row" style="margin-left:20px;">
            <div class="col-md-2">
                <div class="form-group">
                    <label>&nbsp;</label>
                    <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-info btn-sm submit','id'=>'button-submit','style'=>'margin-top: 22px;')); ?>
                    <?php
                    if (!$model->isNewRecord) {
                        echo CHtml::ResetButton('Close', array('onclick' => 'closeaction(this,event);', 'class' => 'btn btn-sm','style'=>'margin-top: 22px;margin-left:5px;'));
                    } else {
                        echo CHtml::ResetButton('Reset', array('style' => 'margin-right:3px;', 'class' => 'btn btn-default btn-sm','style'=>'margin-top: 22px;'));
                        echo CHtml::ResetButton('Close', array('onclick' => 'closeaction(this,event);', 'class' => 'btn btn-sm','style'=>'margin-top: 22px;margin-left:5px;'));
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>

    <?php $this->endWidget(); ?>
</div>
<!-- <div class="row">
        <div class="col-sm-12">
            <div id="errorMsg"></div>
        </div>
</div> -->
<script>
    $(document).ready(function() {
        $('.js-example-basic-single').select2({
            width: '100%',
           
        });
    });

    $(document).ready(function() {
    $('.material-change').change(function() {
        var material_id = $(this).val();
        var project_id=$('#project_id').val();
       
        if (material_id) {
            $.ajax({
                url: '<?php echo Yii::app()->createUrl("MaterialEstimation/GetMaterialData"); ?>',
                type: 'POST',
                data: { material_id: material_id,project_id:project_id },
                success: function(response) {
                    console.log(response);
                    var obj = JSON.parse(response);
                    $("#button-submit").attr('disabled', false);
                    if (obj.status === 'success') {
                        var specifications = obj.data2; // Assuming data2 contains the specifications
                        console.log('Specifications:', specifications);
                        $('#MaterialItems_unit').val('');
                        if (specifications.length == 1) {
                            $('#MaterialEstimation_specification').val(specifications[0].id).trigger('change');
                        } else {
                            $('#MaterialEstimation_specification').empty();
                            $('#MaterialEstimation_specification').append($('<option>', {
                                value: '',
                                text: 'Select specification',
                                selected: true,
                                disabled: true
                            }));
                            specifications.forEach(function(spec) {
                                $('#MaterialEstimation_specification').append($('<option>', {
                                    value: spec.id,
                                    text: spec.specification
                                }));
                            });
                            
                            // Optionally, you can trigger a change event here if needed
                            $('#MaterialEstimation_specification').trigger('change');
                        }
                        
                        $('#quantity').val(obj.data.count);
                        $('#MaterialEstimation_unit').val(obj.data.coms_unit_id).trigger('change');
                    }else if(obj.status=='exists'){
                        $("#button-submit").attr('disabled', true);
                        $("#errorMsg").show()
                        .html('<div class="text-danger"><b>Material Alredy Exist.</b></div>')
                        .fadeOut(5000);
                    } else {
                        $("#button-submit").attr('disabled', false);
                        console.error(obj.message);
                    }
                },
                error: function(xhr, status, error) {
                    console.error(error);
                }
            });
        }
    });
    $('.labour-change').change(function() {
            var labour_id = $(this).val();
            if (labour_id) {
                $.ajax({
                    url: '<?php echo Yii::app()->createUrl("MaterialEstimation/GetLabourData"); ?>', 
                    type: 'POST',
                    data: { labour_id: labour_id },
                    success: function(response) {
                        console.log(response);
                        var obj = JSON.parse(response);
                        if (obj.status === 'success') {
                        $('#quantity').val(obj.data.count);
                        $('#LabourEstimation_unit').val(obj.data.coms_unit_id).trigger('change');
                        } else {
                            console.error(obj.message);
                        }
                    },
                    error: function(xhr, status, error) {
                    
                        console.error(error);
                    }
                });
            }
        });
        $('.equipment-change').change(function() {
            var equipment_id = $(this).val();
            if (equipment_id) {
                $.ajax({
                    url: '<?php echo Yii::app()->createUrl("MaterialEstimation/GetEquipmentData"); ?>', 
                    type: 'POST',
                    data: { equipment_id: equipment_id },
                    success: function(response) {
                        console.log(response);
                        var obj = JSON.parse(response);
                        if (obj.status === 'success') {
                        $('#quantity').val(obj.data.count);
                        $('#EquipmentsEstimation_unit').val(obj.data.coms_unit_id).trigger('change');
                        } else {
                            console.error(obj.message);
                        }
                    },
                    error: function(xhr, status, error) {
                    
                        console.error(error);
                    }
                });
            }
        });
    });

    $('#quantity, #rate').change(function () {
    var quantity = parseFloat($('#quantity').val()); 
    var rate = parseFloat($('#rate').val());

        console.log(quantity,rate);
         if (!isNaN(quantity) && !isNaN(rate)) { 
                var amount = quantity * rate; 
                $('#amount').val(amount.toFixed(2)); 
            } else {
                $('#amount').val(''); 
                console.log('Nan error');
            }
        });

    // AJAX call when quantity changes
    $('.quantity').change(function() {
        var estimation = '<?php echo $estimation; ?>';
        var entryId = $('#entry_id').val();
        var quantity = $(this).val();
        
        $.ajax({
            type: 'POST',
            url: '<?php echo Yii::app()->createUrl("MaterialEstimation/quantityApprove"); ?>',
            data: {
                estimation: estimation,
                entryId: entryId,
                quantity: quantity
            },
            success: function(response) {
                // Handle the response from the controller
                console.log(response);
            },
            error: function() {
                console.log('Error occurred while processing your request.');
            }
        });
    });


</script>
