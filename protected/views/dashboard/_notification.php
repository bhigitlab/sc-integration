<style>
.exp-list {
	padding-left: 0px !important;
}

.error {
    color: #ff0000;
  }
</style>

<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/validation/jquery.validate.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/validation/additional-methods.js"></script>
<?php

$this->breadcrumbs=array(
	'Purchase List',)
?>
<div class="container" id="project">
    <div class="clearfix">
        
        <h2>All Notifications</h2>
    </DIV>


<div id="msg_box"></div>
<div class="row explist">
    <div class="col-md-12">
<?php  $this->widget('zii.widgets.CListView', array(
    'dataProvider'=>$dataProvider,
    'viewData' => array( 'model' => $model),
	'itemView' => '_notificationlists', 'template' => '<div>{summary}{sorter}</div><div class=""><div class=""><table cellpadding="10" class="table  list-view sorter">{items}</table></div>{pager}',
)); ?>

    </div>

        
        
</div>
</div>



<div id='FlickrGallery'></div>



<style>
a.button {
    background-color: #6a8ec7;
    display: block;
    padding: 5px;
    color: #fff;
    cursor: pointer;
    float: right;
    border: 1px solid #6a8ec8;
}
.page-body h3{
    margin:4px 0px;
    color:inherit;
    text-align: left;
}
.panel{border:1px solid #ddd;}
.panel-heading{background-color:#eee;height:40px; }
</style>


