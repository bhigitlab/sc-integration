<?php
if ($type == 2) {
    $template = Yii::app()->db->createCommand("SELECT * FROM `jp_labour_template` WHERE `id` = ".$template_id)->queryRow();

    if ($template) {
        $arrVal = explode(',', $template['labour_type']);
        ?>
        <table class="table" style="margin-bottom:0px;">
            <thead>
                <tr>
                    <!-- Template Heading -->
                    <th colspan="2"><h5>Template: <?php echo $template['template_label']; ?></h5></th>
                </tr>
                <tr>
                    <th>Labour Label</th>
                    <th>Labour Rate</th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($arrVal as $val) {
                    $labour_data = Yii::app()->db->createCommand("SELECT * FROM `jp_labour_worktype` WHERE `type_id` = ".$val)->queryRow();
                    ?>
                    <tr>
                        <td><?php echo $labour_data['worktype']; ?></td>
                        <td class="text-right">
                            <input type="text" name="Projects[lab_templates][<?php echo $template_id; ?>][<?php echo $val; ?>][rate]" value="<?php echo Controller::money_format_inr($labour_data['rate'], 2); ?>" class="form-control" />
                            <input type="hidden" name="Projects[lab_templates][<?php echo $template_id; ?>][<?php echo $val; ?>][type_id]" value="<?php echo $labour_data["type_id"]; ?>" />
                            <input type="hidden" name="Projects[lab_templates][<?php echo $template_id; ?>][<?php echo $val; ?>][template_id]" value="<?php echo $template_id; ?>" /> <!-- Hidden field for template ID -->
                        </td>
                    </tr>
                    <?php
                } ?>
            </tbody>
        </table>
        <?php
    }
} elseif ($type == 1) {
    $arrVal = Yii::app()->db->createCommand("SELECT * FROM `jp_project_labour` WHERE `template_id` = " . $template_id . " AND `project_id` =" . $project)->queryAll();
    $flag =1; //for already created templates of project
    $template = Yii::app()->db->createCommand("SELECT * FROM `jp_labour_template` WHERE `id` = ".$template_id)->queryRow();
    if(empty($arrVal)){
       

        if ($template) {
            $flag =2;
            $arrVal = explode(',', $template['labour_type']);

        }
       
    }else{
        $existingLabourIds = array_column($arrVal,'labour_id');
        $allLaboursInTemplate = explode(',', $template['labour_type']);
        foreach ($allLaboursInTemplate as $labourId) {
            // Check if the labourId is not in the existingLabourIds array
            if (!in_array($labourId, $existingLabourIds)) {
                // If labour is not already in project_labour, add it to $arrVal with a default rate
                $labour_dt = Yii::app()->db->createCommand("SELECT * FROM `jp_labour_worktype` WHERE `type_id` = " . $labourId)->queryRow();
                $arrVal[] = ['labour_id' => $labourId, 'labour_rate' =>  $labour_dt["rate"]]; // Default rate is 0 or fetch from template
            }
        }
    }
   


    if ($arrVal) {
        if ($flag==1) {
            ?>
            <table class="table" style="margin-bottom:0px;">
                <thead>
                    <tr>
                        <!-- Template Heading -->
                        <th colspan="2"><h5>Template: <?php echo $template['template_label']; ?></h5></th>
                    </tr>
                    <tr>
                        <th>Labour Label</th>
                        <th>Labour Rate</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($arrVal as $val) {
                        $labour_data = Yii::app()->db->createCommand("SELECT * FROM `jp_labour_worktype` WHERE `type_id` = " . $val['labour_id'])->queryRow();
                        ?>
                        <tr>
                            <td><?php echo $labour_data['worktype']; ?></td>
                            <td class="text-right">
                                <input type="text" name="Projects[lab_templates][<?php echo $template_id; ?>][<?php echo $val['labour_id']; ?>][rate]" value="<?php echo Controller::money_format_inr($val['labour_rate'], 2); ?>" class="form-control" />
                                <input type="hidden" name="Projects[lab_templates][<?php echo $template_id; ?>][<?php echo $val['labour_id']; ?>][type_id]" value="<?php echo $val['labour_id']; ?>" />
                                <input type="hidden" name="Projects[lab_templates][<?php echo $template_id; ?>][<?php echo $val['labour_id']; ?>][template_id]" value="<?php echo $template_id; ?>" /> <!-- Hidden field for template ID -->
                                
                            </td>
                        </tr>
                        <?php
                    } ?>
                </tbody>
            </table>
            <?php
        }else{ ?>
        <table class="table" style="margin-bottom:0px;">
            <thead>
                <tr>
                    <!-- Template Heading -->
                    <th colspan="2"><h5>Template: <?php echo $template['template_label']; ?></h5></th>
                </tr>
                <tr>
                    <th>Labour Label</th>
                    <th>Labour Rate</th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($arrVal as $val) {
                    $labour_data = Yii::app()->db->createCommand("SELECT * FROM `jp_labour_worktype` WHERE `type_id` = ".$val)->queryRow();
                    ?>
                    <tr>
                        <td><?php echo $labour_data['worktype']; ?></td>
                        <td class="text-right">
                            <input type="text" name="Projects[lab_templates][<?php echo $template_id; ?>][<?php echo $val; ?>][rate]" value="<?php echo Controller::money_format_inr($labour_data['rate'], 2); ?>" class="form-control" />
                            <input type="hidden" name="Projects[lab_templates][<?php echo $template_id; ?>][<?php echo $val; ?>][type_id]" value="<?php echo $labour_data["type_id"]; ?>" />
                            <input type="hidden" name="Projects[lab_templates][<?php echo $template_id; ?>][<?php echo $val; ?>][template_id]" value="<?php echo $template_id; ?>" /> <!-- Hidden field for template ID -->
                        </td>
                    </tr>
                    <?php
                } ?>
            </tbody>
        </table>
            

<?php  }
    }
}
?>
