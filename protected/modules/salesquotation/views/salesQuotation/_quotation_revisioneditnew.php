
<div class="updatedetails">
    <h4 class="ml-2" style="font-weight:bold">Edit Details</h4>
    <div class="invoicemaindiv">
        <div id="errormessage"></div>
        <form id="pdfvals1" method="post" action="">            
            <?php
            $sql = 'SELECT revision_no FROM `jp_quotation_section` '
                    . ' WHERE `qtn_id` = ' . $_GET['qid']
                    . ' ORDER BY `id` DESC LIMIT 1';
            $latestrev = Yii::app()->db->createCommand($sql)->queryScalar();
            ?>
            <input class="latest_rev" type="hidden" value="<?php echo $latestrev ?>"  name="latest_rev"> 
            <?php
            $revNo = $_GET['selected_revision'];
            if ($latestrev == "REV-" . $_GET['rev']) {
                $revNo = $latestrev;
            }

            $sql = 'SELECT * FROM `jp_quotation_section` '
                    . 'WHERE `qtn_id` = ' . $_GET['qid']
                    . ' AND revision_no="' . $revNo . '"';
            $sectionArray = Yii::app()->db->createCommand($sql)->queryAll();
            $i = 1;
            foreach ($sectionArray as $key => $value) {
                $subsectionArray = Yii::app()->db->createCommand('SELECT * '
                                . ' FROM `jp_quotation_gen_category` '
                                . ' WHERE `qid` = ' . $_GET['qid']
                                . ' AND `section_id` = ' . $value["id"]
                                . ' AND revision_no="' . $revNo . '"')->queryAll();
                ?>
                <div class="add_sec">
                <!-- section starts -->
                <div class="sections_block">
                    <div class="quotation_sec">
                        <!-- start main sec -->
                        <div class="panel" style="position:relative">
                            <div class="panel-heading">
                                <h5 class="bold">Main Section</h4>
                            </div>
                            <div class="panel-body collapse_sec">
                                <div class="row">        
                                    <div class="col-md-3">
                                        <input class="parent_section_id" type="hidden"   name="parent_section_id">                                  
                                        <input id="section_id" type="hidden" value="<?php echo $value['id'] ?>"  name="section_id">        
                                        <input class="form-control bold section_name" type="text" value="<?php echo $value['section_name'] ?>" >        
                                    </div>                                
                                    <div class="col-md-2">                                    
                                        <input type="text" class="form-control sec_unit" value="<?php echo $value['unit'] ?>" placeholder="unit">
                                    </div>
                                    <div class="col-md-1">
                                        <input type="text" class="form-control sec_quantity" value="<?php echo $value['quantity'] ?>"  placeholder="quantity">                   
                                    </div>
                                    <div class="col-md-1">
                                        <input type="text" class="form-control sec_quantity_nos" value="<?php echo $value['quantity_nos'] ?>"   placeholder="quantity_nos">                   
                                    </div>
                                    <div class="col-md-1">
                                        <input type="text" class="form-control sec_mrp" value="<?php echo $value['mrp'] ?>"   placeholder="mrp">                   
                                    </div>
                                    <div class="col-md-2">
                                        <input type="text" class="form-control sec_amount_after_discount" value="<?php echo $value['amount_after_discount'] ?>" placeholder ="amount_after_discount">                   
                                    </div>
                                </div>            
                                <div class="add_sub_section toggle_btn" title="Add Sub Section">        
                                    <i class="toggle-caret fa fa-plus"></i>
                                </div>
                                <?php
                                foreach ($subsectionArray as $key => $value) {
                                    $sql = 'SELECT * FROM `jp_sales_quotation` '
                                            . ' WHERE `parent_type` = 0 AND '
                                            . '`parent_id` = ' . $value['id']
                                            . '  AND revision_no="' . $revNo . '"';
                                            
                                    $itemArray = Yii::app()->db->createCommand($sql)->queryAll();

                                    $sql = 'SELECT * FROM `jp_quotation_gen_worktype` '
                                            . ' WHERE `qid` = ' . $value["qid"]
                                            . ' AND `section_id` = ' . $value["section_id"]
                                            . ' AND `category_label_id` = ' . $value["id"]
                                            . ' AND revision_no="' . $revNo . '"';
                                    $subitemArray = Yii::app()->db->createCommand($sql)->queryAll();
                                ?>
                                
                                <div class="sub_sec_block"  style="display:none;"> 
                                    <div class="sub_section">                           
                                        <br>
                                        <div id="items_holder" class="position-relative items_holder panel">
                                            <div class="panel-heading">
                                                <h5 class="bold">Sub Section</h5>
                                            </div>
                                            <div class="quotationmaindiv panel-body" id="items_list">
                                                <div class="add_new_sec ">                        
                                                    <div class="h-head">
                                                        <div class="row">
                                                            <div class="col-md-2">
                                                                <label class="bold">Sub Section</label>   
                                                                <input type="hidden" id="category_label_id"  value="<?php echo $value['id'] ?>">                     
                                                                <input class="form-control" id="category_label" 
                                                                       type="text" value="<?php echo $value['category_label'] ?>" >
                                                            </div>

                                                            <div class="col-md-2">    
                                                                <label>&nbsp;</label>                                
                                                                <input type="text" class="form-control sub_unit" value="<?php echo $value['unit'] ?>" placeholder="unit" >
                                                            </div>
                                                            <div class="col-md-1">
                                                                <label>&nbsp;</label>
                                                                <input type="text" class="form-control sub_quantity" value="<?php echo $value['quantity'] ?>"  placeholder="quantity" >                   
                                                            </div>
                                                            <div class="col-md-1">
                                                                <label>&nbsp;</label>
                                                                <input type="text" class="form-control sub_quantity_nos" value="<?php echo $value['quantity_nos'] ?>"  placeholder="quantity_nos" >                   
                                                            </div>
                                                            <div class="col-md-1">
                                                                <label>&nbsp;</label>
                                                                <input type="text" class="form-control sub_mrp" value="<?php echo $value['mrp'] ?>"   placeholder="mrp" >                   
                                                            </div>
                                                            <div class="col-md-2">
                                                                <label>&nbsp;</label>
                                                                <input type="text" class="form-control sub_amount_after_discount" value="<?php echo $value['amount_after_discount'] ?>" placeholder ="amount_after_discount">                   
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="" id="quotation_item">
                                            </div>
                                            <div class="add_wrktype toggle_btn1" title="Add Sub Section">        
                                                <i class="toggle-caret fa fa-plus"></i>
                                            </div>
                                            <div class="worktype_label_sec px-3 mt-2">
                                                <?php if(!empty($subitemArray)){ ?>
                                                <div class=" panel panel-default">
                                                    <div class="panel-heading position-relative">
                                                        <div class="clearfix">
                                                            <div class="pull-left">
                                                                <h5>Add Sub Items</h5>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="panel-body" style="background: #f8fbff;">
                                                        <?php
                                                            foreach ($subitemArray as $key => $value) {
                                                                $sql = 'SELECT * FROM `jp_sales_quotation` WHERE `parent_type` = 1 '
                                                                        . ' AND `parent_id` = ' . $value['id'] 
                                                                        . ' AND revision_no="' . $qmodel->revision_no . '"';
                                                                $wrktypeitemArray = Yii::app()->db->createCommand($sql)->queryAll();
                                                                $readonly = '';
                                                                if (!empty($wrktypeitemArray)) {
                                                                    $readonly = 'readonly';
                                                                }
                                                                ?>
                                                                <div class="worktype_label_sec px-3 mt-2">
                                                                    <div class=" panel panel-default">
                                                                        <div class="panel-heading">Sub Items</div>
                                                                        <div class="panel-body">
                                                                            <div class=" worktype_label_row">
                                                                                <div class="row mb-2  subitem_block" style="background: #eee;padding: 10px 0px;">
                                                                                    <div class="col-md-6">
                                                                                        <div class="row">
                                                                                            <div class="col-md-6">
                                                                                                <label>Work Type </label>                                             
                                                                                                <input type="hidden" id="worktype_label_id"  value="<?php echo $value['id'] ?>">                              
                                                                                                <input class="form-control sub_item_label" type="text" value="<?php echo $value['worktype_label'] ?>" <?php echo $readonly ?>>
                                                                                            </div>
                                                                                            <div class="col-md-6">
                                                                                                <label>&nbsp;</label>
                                                                                                <input type="text" class="form-control QuotationGenWorktype_master_cat_id" value="<?php echo $value['master_cat_id'] ?>" placeholder="Category" <?php echo $readonly ?>>
                                                                                            </div>
                                                                                            
                                                                                            <div class="col-md-6">
                                                                                                <label>&nbsp;</label>
                                                                                                <input type="text" class="form-control sub_item_unit" value="<?php echo $value['unit'] ?>" placeholder="unit" <?php echo $readonly ?>>
                                                                                            </div>
                                                                                            <div class="col-md-6">
                                                                                                <label>&nbsp;</label>
                                                                                                <input type="text" class="form-control sub_item_quantity" value="<?php echo $value['quantity'] ?>" placeholder="quantity" <?php echo $readonly ?>>
                                                                                            </div>
                                                                                            <div class="col-md-6">
                                                                                                <label>&nbsp;</label>
                                                                                                <input type="text" class="form-control sub_item_quantity_nos" value="<?php echo $value['quantity_nos'] ?>" placeholder="quantity_nos" <?php echo $readonly ?>>
                                                                                            </div>
                                                                                            <div class="col-md-3">
                                                                                                <label>&nbsp;</label>
                                                                                                <input type="text" class="form-control sub_item_mrp" value="<?php echo $value['mrp'] ?>" placeholder="mrp" <?php echo $readonly ?>>                        
                                                                                            </div>
                                                                                            <div class="col-md-3">
                                                                                                <label>&nbsp;</label>
                                                                                                <input type="text" class="form-control sub_item_amount_after_discount" value="<?php echo $value['amount_after_discount'] ?>" placeholder="amount_after_discount" <?php echo $readonly ?>> 
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-6">                                                        
                                                                                        <label>&nbsp;</label>                                                 
                                                                                        <textarea class="form-control sub_item_description" rows="3" <?php echo $readonly ?>><?php echo $value['description'] ?></textarea>                                                        
                                                                                    </div>
                                                                                </div>
                                                                                <?php
                                                                                foreach ($wrktypeitemArray as $key => $value) {
                                                                                    ?>
                                                                                    <br>
                                                                                    <div class="worktype_data_sub row  mb-2">


                                                                                        <div  class="col-md-2">
                                                                                            <input type="hidden" id="subItem_category_label_id"  value="<?php echo $value['id'] ?>"> 
                                                                                            <input type="text" class="form-control category_label" value="<?php echo $value['category_label'] ?>" readonly>
                                                                                        </div>
                                                                                        <input type="hidden" class="form-control category_id" value="<?php echo $value['category_id'] ?>">
                                                                                        <input type="hidden" class="form-control master_id" value="<?php echo $value['master_id'] ?>">
                                                                                        <input type="hidden" class="form-control item_id" value="<?php echo $value['item_id'] ?>">
                                                                                        <input type="hidden" class="form-control work_type" value="<?php echo $value['work_type'] ?>">

                                                                                        <input type="hidden" class="form-control revision_no" value="<?php echo $_GET['rev'] ?>">
                                                                                        <input type="hidden" class="form-control parent_type" value="<?php echo $value['parent_type'] ?>">
                                                                                        <input type="hidden" class="form-control parent_id" value="<?php echo $value['parent_id'] ?>">
                                                                                        <div  class="col-md-2">
                                                                                            <?php
                                                                                            $newQuery = 'work_type_id =' . $value['work_type'] . ' AND quotation_category_id =' . $value['category_id'] . '';
                                                                                            echo CHtml::dropDownList(
                                                                                                    '', 'worktype_label', CHtml::listData(QuotationItemMaster::model()->findAll(array('condition' => $newQuery)), 'worktype_label', 'worktype_label'), array('class' => 'form-control js-example-basic-single field_change invoice_add require  common-val worktype_label_select worktype_label', 'empty' => $value['worktype_label'], 'id' => 'worktype_label' . $key, 'style' => 'width:100%', 'name' => 'worktype_label[]', 'data-cat' => $value['category_id'], 'data-id' => $value['work_type']))
                                                                                            ?>
                                                                                            <input type="hidden" class="form-control worktype_label" value="<?php echo $value['worktype_label'] ?>">
                                                                                        </div>
                                                                                        <input class="form-control require" value="<?php echo $value['shutterwork_description'] ?>" id="shutterwork_description" type="hidden">                                
                                                                                        <input class="form-control require" value="<?php echo $value['carcass_description'] ?>" id="caracoss_description" type="hidden" value="">                                
                                                                                        <input class="form-control require" value="<?php echo $value['shutterwork_material'] ?>" id="shutterwork_material" type="hidden" value="">                                
                                                                                        <input class="form-control require" value="<?php echo $value['shutterwork_finish'] ?>" id="shutterwork_finish" type="hidden" value="">                                
                                                                                        <input class="form-control require" value="<?php echo $value['carcass_material'] ?>" id="carcass_material" type="hidden" value="">                                
                                                                                        <input class="form-control require" value="<?php echo $value['carcass_finish'] ?>" id="carcass_finish" type="hidden" value=""> 
                                                                                        <input class="form-control require" value="<?php echo $value['deleted_status'] ?>" id="deleted_status" type="hidden" value=""> 

                                                                                        <div  class="col-md-2">

                                                                                            <input class="form-control require common-val description" name="description" readonly="readonly" type="text" maxlength="255" value="<?php echo $value['description'] ?>">
                                                                                        </div>
                                                                                        <div  class="col-md-1">
                                                                                            <?php
                                                                                            $unit = Unit::model()->findByPk($value['unit']);
                                                                                            echo CHtml::dropDownList(
                                                                                                    '', 'unit', CHtml::listData(Unit::model()->findAll(), 'id', 'unit_name'), array('class' => 'form-control js-example-basic-single field_change invoice_add require common-val unit_select', 'empty' => $unit->unit_name, 'id' => 'unit', 'name' => 'unit[]', 'style' => 'width:100%')
                                                                                            );
                                                                                            ?>
                                                                                            <input type="hidden" class="form-control unit" value="<?php echo $value['unit'] ?>">
                                                                                            <input type="hidden" class="form-control quantity_nos" value="<?php echo $value['quantity_nos'] ?>">
                                                                                        </div>
                                                                                        <div  class="col-md-1">
                                                                                            <input type="text" class="form-control quantity" value="<?php echo $value['quantity'] ?>">
                                                                                        </div>
                                                                                        <div  class="col-md-2 text-right">
                                                                                            <input type="text" class="form-control mrp" value="<?php echo $value['mrp'] ?>">
                                                                                        </div>
                                                                                        <div  class="col-md-2 text-right">
                                                                                            <input type="text" class="form-control amount_after_discount" value="<?php echo $value['amount_after_discount'] ?>">
                                                                                        </div>                                                    
                                                                                    </div>
                                                                                    <?php
                                                                                }
                                                                                ?>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                            <div class="" id="quotation_item_edit">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>

                            <!-- end main sec -->
                        </div>

                    </div>


                </div>
            </div>
                <!-- section end -->
            <?php } ?>

            <br>

        </form>

        <div class="text-center">
            <button class="btn btn-primary btn_update1">Save</button>
        </div>
    </div>   
</div>
<style>
    .input_style{
        margin:15px;
    }

</style>
<script>
    $(".collapse_block").hide();
    $(".collapse_sec ").click(function () {
        $(this).toggleClass('input_style');
        $(this).siblings(".collapse_block").slideToggle();
    })

    $(".worktype_label_select").change(function () {
        var worktype_label_select = $(this).val();
        $(this).siblings('.worktype_label').val(worktype_label_select);
    });

    $(".unit_select").change(function () {
        var unit_select = $(this).val();
        $(this).siblings('.unit').val(unit_select);
    });

    $(".btn_update1").click(function () {
        $('.loading-overlay').show();
        var sec = [];
        var revno = $(".revision_no").val();
        var masterid = $(".master_id").val();
        $(this).parent().siblings('form').find(".sections_block").each(function () {               
            createsection($(this), sec);
            $(document).find(".alert-msg-box").show(); 
            $('html, body').animate({
                scrollTop: $(document).find(".alert-msg-box").offset().top - 80
            }, 'slow');                                     
            $(document).find(".alert-msg").html('Quotation Item Added Successfully to new revision');                                    
        });

    })

    function getamount(elem, qty, item_id) {
        var total_amount = $("#total-amount").val();
        $.ajax({
            url: "<?php echo Yii::app()->createAbsoluteUrl('salesquotation/SalesQuotation/getamountdetails'); ?>",
            data: {
                item_id: item_id,
                qty: qty
            },
            type: "POST",
            dataType: 'json',
            success: function (result) {
                if (result != '') {
                    $(elem).parents(".worktype_data_sub").find(".amount_after_discount").val(result.profit_amount);
                    var mrp_after_discount = parseInt(result.profit_amount) + parseInt((result.profit_amount * result.discount) / 100);
                    $(elem).parents(".worktype_data_sub").find(".mrp").val(mrp_after_discount);

                }
            }
        });
    }

    $(document).on('change', '.worktype_label', function () {
        var element = $(this);
        var worktypelabel = $('option:selected', this).text();
        var cat_id = element.parent().siblings(".category_id").val();
        var worktypeid = element.parent().siblings('.work_type').val();
        var template_id = element.parent().siblings('.worktypedata').attr('data-template');

        if (worktypelabel != '') {
            element.parent().siblings().find('input[type=text],select').addClass('commonadd-val');
        } else {
            element.parent().siblings().find('input[type=text],select').removeClass('commonadd-val');
        }
        var id = element.parent().siblings('.worktypedata').attr('data-incremnt');
        var qty = element.parents(".worktype_data_sub ").find(".quantity").val();
        $.ajax({
            url: '<?php echo Yii::app()->createAbsoluteUrl('salesquotation/SalesQuotation/getdatasofworktype'); ?>',
            type: 'GET',
            dataType: 'json',
            data: {
                cat_id: cat_id,
                worktypelabel: worktypelabel,
                worktypeid: worktypeid,
                template_id: template_id
            },
            success: function (result) {
                if (result.status == 1) {
                    var itemid = result.item_id;
                    $('#hiddenitem' + id).val(result.item_id);
                    $('#previous_details').html(result.html);
                    $("#previous_details").show();
                    // $("#pre_fixtable").tableHeadFixer();
                    getamount(element, qty, itemid);
                } else {
                    $('#previous_details').html(result.html);
                    // $("#pre_fixtable").tableHeadFixer();
                }

                if (template_id == 1) {
                    $('#shutterwork_description' + id).val(result.shutterdesc);
                    $('#caracoss_description' + id).val(result.carcassdesc);
                    $('#shutterwork_material' + id).val(result.shuttermaterial);
                    $('#shutterwork_finish' + id).val(result.shutterfinish);
                    $('#carcass_material' + id).val(result.carcasmaterial);
                    $('#carcass_finish' + id).val(result.carcassfinish);
                } else {
                    if (template_id == 3) {
                        $('#material' + id).val(result.material);
                        $('#finish' + id).val(result.finish);
                    }
                    element.parents('.worktype_data_sub').find('.description').val(result.description);

                }
            }
        })
    })

    $(document).on('change', '.quantity', function () {
        $(this).parents(".worktype_data_sub").find("select.worktype_label").change();
    });

    function createsection(elem, sec) {
        // $(elem).addClass("added_class");
        var $t = $(elem);
        var sec_id = "";
        var latestrev = $('.latest_rev').val();

        var section = $(elem).find('.collapse_sec ').find('.section_name').val();
        // alert(section);return;
        var revision_no = "<?php echo $_GET['rev']; ?>";

        if (latestrev == 'REV-' + revision_no) {
            var sec_id = $(elem).find('.collapse_sec ').find('#section_id').val();
        }
        var qid = "<?php echo $_GET['qid']; ?>";
        var unit = $(elem).find('.collapse_sec ').find(".sec_unit").val();
        var quantity = $(elem).find('.collapse_sec ').find(".sec_quantity").val();
        var quantity_nos = $(elem).find('.collapse_sec ').find(".sec_quantity_nos").val();
        var mrp = $(elem).find('.collapse_sec ').find(".sec_mrp").val();
        var hsn_code = $(elem).find('.collapse_sec ').find(".sec_hsn_code").val();
        var amount_after_discount = $(elem).find('.collapse_sec ').find(".sec_amount_after_discount").val();
        var status = "<?php echo $_GET['status'] ?>";
        var url = "<?php echo Yii::app()->createAbsoluteUrl('salesquotation/SalesQuotation/createsection'); ?>";

        $.ajax({
            url: url,
            data: {
                section_name: section,
                qtn_id: qid,
                unit: unit,
                quantity: quantity,
                quantity_nos: quantity_nos,
                mrp: mrp,
                amount_after_discount: amount_after_discount,
                sec_id: sec_id,
                revision_no: revision_no,
                status:status,
                hsn_code:hsn_code,

            },
            type: "POST",
            dataType: 'json',
            success: function (result) {
                $t.find('.collapse_sec ').find(".parent_section_id").val(result.id);
                $t.find(".sub_sec_block").each(function () {
                    createsubsection($(this), result.id, sec);
                })
            }
        })
    }

    function createsubsection(elem, sectionId, sec) {
        var $t = $(elem);
        var latestrev = $('.latest_rev').val();
        var category_label = $(elem).find(".quotationmaindiv").find('#category_label').val();
        var revision_no = "<?php echo $_GET['rev'] ?>";
        var cat_label_id = "";
        if (latestrev == 'REV-' + revision_no) {
            var cat_label_id = $(elem).find(".quotationmaindiv").find('#category_label_id').val();
        }
        var qid = "<?php echo $_GET['qid'] ?>";
        var sec_id = sectionId;
        var master_cat_id = $(elem).parents(".row").find(".QuotationGenCategory_master_cat_id").val();
        var unit = $(elem).find(".quotationmaindiv").find(".sub_unit").val();
        var quantity = $(elem).find(".quotationmaindiv").find(".sub_quantity").val();
        var quantity_nos = $(elem).find(".quotationmaindiv").find(".sub_quantity_nos").val();
        var mrp = $(elem).find(".quotationmaindiv").find(".sub_mrp").val();
        var amount_after_discount = $(elem).find(".quotationmaindiv").find(".sub_amount_after_discount").val();

        var url = "<?php echo Yii::app()->createAbsoluteUrl('salesquotation/SalesQuotation/createcategorylabel'); ?>";



        $.ajax({
            url: url,
            data: {
                category_label: category_label,
                qid: qid,
                section_id: sec_id,
                unit: unit,
                quantity: quantity,
                quantity_nos: quantity_nos,
                mrp: mrp,
                amount_after_discount: amount_after_discount,
                sec_id: sec_id,
                master_cat_id: master_cat_id,
                cat_label_id: cat_label_id,
                revision_no: revision_no

            },
            type: "POST",
            dataType: 'json',
            success: function (result) {
                $t.find(".quotationmaindiv").find("#category_label_id").val(result.id);
                var id = 'QuotationGenCategory_master_cat_id' + result.id;
                $t.find(".quotationmaindiv").find(".QuotationGenCategory_master_cat_id").attr("id", id);
                $t.find(".worktype_label_sec ").find(".subitem_block").each(function () {
                    createsubItem($(this), result.id, result.section, sec);
                })
            }
        })
    }


    function createsubItem(elem, subId, secId, sec) {
        var $t = $(elem);
        var latestrev = $('.latest_rev').val();
        var revision_no = "<?php echo $_GET['rev'] ?>";
        var worktype_label_id = "";
        if (latestrev == 'REV-' + revision_no) {
            var worktype_label_id = $t.find('#worktype_label_id').val();
        }
        var qid = "<?php echo $_GET['qid'] ?>";
        var sec_id = secId;
        var cat_label_id = subId;
        var worktype_label = $t.find('.sub_item_label').val();
        var master_cat_id = $t.find(".QuotationGenWorktype_master_cat_id").val();
        var unit = $t.find(".sub_item_unit").val();
        var quantity = $t.find(".sub_item_quantity").val();
        var quantity_nos = $t.find(".sub_item_quantity_nos").val();
        var mrp = $t.find(".sub_item_mrp").val();
        var amount_after_discount = $t.find(".sub_item_amount_after_discount").val();

        var description = $t.find('.sub_item_description').val();
        var url = "<?php echo Yii::app()->createAbsoluteUrl('salesquotation/SalesQuotation/createworktypelabel'); ?>";


        $.ajax({
            url: url,
            data: {
                qid: qid,
                section_id: sec_id,
                category_label_id: cat_label_id,
                worktype_label: worktype_label,
                unit: unit,
                quantity: quantity,
                quantity_nos: quantity_nos,
                mrp: mrp,
                amount_after_discount: amount_after_discount,
                master_cat_id: master_cat_id,
                worktype_label_id: worktype_label_id,
                description: description,
                revision_no: revision_no

            },
            type: "POST",
            dataType: 'json',
            success: function (result) {
                $t.find("#worktype_label_id").val(result.id);
                var id = 'QuotationGenWorktype_master_cat_id' + result.id;
                $t.find(".QuotationGenWorktype_master_cat_id").attr("id", id);
                $t.siblings(".worktype_data_sub").each(function () {                    
                    createcatItem($(this), result.id, result.section, result.sub, sec);                    
                })
            }
        })

    }
    function createcatItem(elem, subItemId, sectionId, subSectionId, sec) {
        var latestrev = $('.latest_rev').val();
        var master_id = "<?php echo $_GET['qid'] ?>";
        var category_id = $(elem).find(".category_id").val();
        var item_id = $(elem).find(".item_id").val();
        var category_label = $(elem).find(".category_label").val();
        var work_type = $(elem).find(".work_type").val();
        var shutterwork_material = $(elem).find(".shutterwork_material").val();
        var shutterwork_finish = $(elem).find(".shutterwork_finish").val();
        var carcass_material = $(elem).find(".carcass_material").val();
        var carcass_finish = $(elem).find(".carcass_finish").val();
        var material = $(elem).find(".material").val();
        var finish = $(elem).find(".finish").val();
        var shutterwork_description = $(elem).find(".shutterwork_description").val();
        var carcass_description = $(elem).find(".carcass_description").val();
        var description = $(elem).find(".description").val();
        var worktype_label = $(elem).find("input.worktype_label").val();
        var unit = $(elem).find(".unit").val();
        var quantity = $(elem).find(".quantity").val();
        var quantity_nos = $(elem).find(".quantity_nos").val();
        var mrp = $(elem).find(".mrp").val();
        var amount_after_discount = $(elem).find(".amount_after_discount").val();
        var revision_no = "<?php echo $_GET['rev'] ?>";
        var deleted_status = $(elem).find(".deleted_status").val();
        var revision_status = $(elem).find(".revision_status").val();
        var revision_approved = $(elem).find(".revision_approved").val();
        var parent_type = $(elem).find(".parent_type").val();
        var parent_id = subItemId;
        var status = "<?php echo $_GET['status'] ?>";
        var subItem_category_label_id = "";
        if (latestrev == 'REV-' + revision_no) {
            var subItem_category_label_id = $(elem).find("#subItem_category_label_id").val();
        }
        sec.push({
            master_id: master_id,
            category_id: category_id,
            item_id: item_id,
            category_label: category_label,
            work_type: work_type,
            shutterwork_material: shutterwork_material,
            shutterwork_finish: shutterwork_finish,
            carcass_material: carcass_material,
            carcass_finish: carcass_finish,
            material: material,
            finish: finish,
            shutterwork_description: shutterwork_description,
            carcass_description: carcass_description,
            description: description,
            worktype_label: worktype_label,
            unit: unit,
            quantity: quantity,
            quantity_nos: quantity_nos,
            mrp: mrp,
            amount_after_discount: amount_after_discount,
            revision_no: revision_no,
            deleted_status: deleted_status,
            revision_status: revision_status,
            revision_approved: revision_approved,
            parent_type: parent_type,
            parent_id: parent_id,
            subItem_category_label_id: subItem_category_label_id

        });
        var data = JSON.stringify(sec);
        console.log(data);
        var url = "<?php echo Yii::app()->createAbsoluteUrl('salesquotation/SalesQuotation/addsalesQuotationItems'); ?>";
        $.ajax({
            type: "POST",
            data: {
                "data": sec,
                "revision_no": revision_no,
                "master_id": master_id,
                "status": status
            },
            url: url,
            success: function (response) {
                
                $(document).find("#total_after_discount").val(response.total_amount)
                $(document).find("#total_after_discount").attr('data-val', response.data_val);
                $("#sgst_percent").change();
                $("#cgst_percent").change();
                $("#igst_percent").change();
                location.reload();
            }
        })
    }

    $(".updatedetails").find(".sections_block").find(".bg-grey.collapse_sec").find("input").attr('readonly', true);
    $(".updatedetails").find(".sub_sec_block").find(".quotationmaindiv").find("input").attr('readonly', true);


</script>
