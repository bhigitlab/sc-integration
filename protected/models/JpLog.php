<?php

/**
 * This is the model class for table "{{log}}".
 *
 * The followings are the available columns in table '{{log}}':
 * @property integer $log_id
 * @property string $log_data
 * @property string $log_table
 * @property integer $log_primary_key
 * @property string $log_datetime
 * @property string $log_action
 * @property integer $log_action_by
 * @property integer $company_id
 */
class JpLog extends CActiveRecord
{
        public $fromdate;
        public $todate;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{log}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('log_data, log_datetime, log_action, log_action_by', 'required'),
			array('log_primary_key, log_action_by, company_id', 'numerical', 'integerOnly'=>true),
			array('log_table', 'length', 'max'=>100),
			array('log_action', 'length', 'max'=>6),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('log_id, fromdate, todate, log_data, log_table, log_primary_key, log_datetime, log_action, log_action_by, company_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'log_id' => 'Log',
			'log_data' => 'Log Data',
			'log_table' => 'Log Table',
			'log_primary_key' => 'Log Primary Key',
			'log_datetime' => 'Log Datetime',
			'log_action' => 'Log Action',
			'log_action_by' => 'Log Action By',
			'company_id' => 'Company',
                        'fromdate' => 'Date From',
			'todate' => 'Date To',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('log_id',$this->log_id);
		$criteria->compare('log_data',$this->log_data,true);
		$criteria->compare('log_table',$this->log_table,true);
		$criteria->compare('log_primary_key',$this->log_primary_key);
		//$criteria->compare('log_datetime',$this->log_datetime,true);
		$criteria->compare('log_action',$this->log_action,true);
		$criteria->compare('log_action_by',$this->log_action_by);
		//$criteria->compare('company_id',Yii::app()->user->company_id);
                $user = Users::model()->findByPk(Yii::app()->user->id);
                $arrVal = explode(',', $user->company_id);
                $newQuery = "";
                foreach($arrVal as $arr) {
                    if ($newQuery) $newQuery .= ' OR';
                    $newQuery .= " FIND_IN_SET('".$arr."', company_id)";
                }
                if(!empty($this->fromdate) && empty($this->todate)) {
                    $criteria->addCondition("log_datetime >= '".date('Y-m-d',strtotime($this->fromdate))."'");  // date is database date column field
                } else if(!empty($this->todate) && empty($this->fromdate)) {
                    $criteria->addCondition("log_datetime <= '".date('Y-m-d',strtotime($this->todate))."'");
                } elseif(!empty($this->todate) && !empty($this->fromdate)) {
                    $criteria->addCondition("log_datetime between '".date('Y-m-d',strtotime($this->fromdate))."' and  '".date('Y-m-d',strtotime($this->todate))."'");
                }
            
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return JpLog the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
