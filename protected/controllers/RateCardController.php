<?php

class RateCardController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		
           $accessArr = array();
           $accessauthArr = array();
           $controller = Yii::app()->controller->id;
           if(isset(Yii::app()->user->menuall)){
               if (array_key_exists($controller, Yii::app()->user->menuall)) {
                   $accessArr = Yii::app()->user->menuall[$controller];
               } 
           }

           if(isset(Yii::app()->user->menuauth)){
               if (array_key_exists($controller, Yii::app()->user->menuauth)) {
                   $accessauthArr = Yii::app()->user->menuauth[$controller];
               }
           }
           $access_privlg = count($accessauthArr);
           return array(
               array('allow', // allow all users to perform 'index' and 'view' actions
                   'actions' => $accessArr,
                   'users' => array('@'),
                   'expression' => "$access_privlg > 0",
               ),

               array('allow', // allow admin user to perform 'admin' and 'delete' actions
                   'actions' => $accessauthArr,
                   'users' => array('@'),
                   'expression' => "$access_privlg > 0",
               ),
               array('deny',  // deny all users
                   'users'=>array('*'),
               ),
           );
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	public function actionIndex()
    {
		$model = new RateCard('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['RateCard']))
            $model->attributes = $_GET['RateCard'];

        if (Yii::app()->user->role == 1) {

            $this->render('index', array(
                'model' => $model,
                'dataProvider' => $model->search(),
            ));
        } else {
            $this->render('index', array(
                'model' => $model,
                'dataProvider' => $model->search(Yii::app()->user->id),
            ));
        }
    }


	public function actionCreate()
    {
        
        $model = new RateCard;
        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['RateCard'])) {
            $model->attributes = $_POST['RateCard'];
            $model->worktype = isset($_POST['RateCard']['worktype'])?$_POST['RateCard']['worktype']:'';
            $model->rate = isset($_POST['RateCard']['rate'])?$_POST['RateCard']['rate']:'';
            $model->created_by = Yii::app()->user->id;
            $model->created_date = date('y-m-d H:i:s');
			$model->updated_date = date('y-m-d H:i:s');
            if($model->save())
			 $this->redirect(array('index'));
        }
        if (isset($_GET['layout']))
            $this->layout = false;
        $this->render('create', array(
            'model' => $model,
        ));
    }

	public function actionUpdate($id)
    {
        $model = $this->loadModel($id);
		$this->performAjaxValidation($model);
		if (isset($_POST['RateCard'])) {
            $model->attributes = $_POST['RateCard'];
            $model->worktype = isset($_POST['RateCard']['worktype'])?$_POST['RateCard']['worktype']:'';
            $model->rate = isset($_POST['RateCard']['rate'])?$_POST['RateCard']['rate']:'';
            if($model->save())
			$this->redirect(array('index'));
        }
        if (isset($_GET['layout']))
            $this->layout = false;

        $this->render('update', array(
            'model' => $model,
        ));
    }

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new RateCard('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['RateCard']))
			$model->attributes=$_GET['RateCard'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=RateCard::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='work-type-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
	public function actionNewlist(){
            
		$model = new RateCard('search');
		$model->unsetAttributes();  // clear any default values
		if (isset($_GET['RateCard']))
			$model->attributes = $_GET['RateCard'];
			
		$this->render('newlist', array(
				'model' => $model, 
				'dataProvider' =>  $model->search(),
		));
	}
}
