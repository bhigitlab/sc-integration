<?php
/* @var $this PmsAccWprItemConsumedController */
/* @var $data PmsAccWprItemConsumed */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('warehouse_id')); ?>:</b>
	<?php echo CHtml::encode($data->warehouse_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('item_id')); ?>:</b>
	<?php echo CHtml::encode($data->item_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('item_rate')); ?>:</b>
	<?php echo CHtml::encode($data->item_rate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('item_qty')); ?>:</b>
	<?php echo CHtml::encode($data->item_qty); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('item_added_by')); ?>:</b>
	<?php echo CHtml::encode($data->item_added_by); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('item_approved_by')); ?>:</b>
	<?php echo CHtml::encode($data->item_approved_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('wpr_id')); ?>:</b>
	<?php echo CHtml::encode($data->wpr_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pms_project_id')); ?>:</b>
	<?php echo CHtml::encode($data->pms_project_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('coms_project_id')); ?>:</b>
	<?php echo CHtml::encode($data->coms_project_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('coms_material_id')); ?>:</b>
	<?php echo CHtml::encode($data->coms_material_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pms_material_id')); ?>:</b>
	<?php echo CHtml::encode($data->pms_material_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pms_template_material_id')); ?>:</b>
	<?php echo CHtml::encode($data->pms_template_material_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('type_from')); ?>:</b>
	<?php echo CHtml::encode($data->type_from); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_date')); ?>:</b>
	<?php echo CHtml::encode($data->created_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_date')); ?>:</b>
	<?php echo CHtml::encode($data->updated_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pms_task_name')); ?>:</b>
	<?php echo CHtml::encode($data->pms_task_name); ?>
	<br />

	*/ ?>

</div>