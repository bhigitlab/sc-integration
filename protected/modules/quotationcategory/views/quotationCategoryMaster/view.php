<?php
/* @var $this QuotationCategoryMasterController */
/* @var $model QuotationCategoryMaster */

$this->breadcrumbs=array(
	'Quotation Category Masters'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List QuotationCategoryMaster', 'url'=>array('index')),
	array('label'=>'Create QuotationCategoryMaster', 'url'=>array('create')),
	array('label'=>'Update QuotationCategoryMaster', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete QuotationCategoryMaster', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage QuotationCategoryMaster', 'url'=>array('admin')),
);
?>

<h1>View QuotationCategoryMaster #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'parent_id',
		'name',
		'status',
		'created_by',
		'created_on',
	),
)); ?>
