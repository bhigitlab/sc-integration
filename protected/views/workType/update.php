<?php
/* @var $this WorkTypeController */
/* @var $model WorkType */

$this->breadcrumbs=array(
	'Work Types'=>array('index'),
	$model->wtid=>array('view','id'=>$model->wtid),
	'Update',
);

/*$this->menu=array(
	array('label'=>'List WorkType', 'url'=>array('index')),
	array('label'=>'Create WorkType', 'url'=>array('create')),
	array('label'=>'View WorkType', 'url'=>array('view', 'id'=>$model->wtid)),
	array('label'=>'Manage WorkType', 'url'=>array('admin')),
); */
?>



<div class="modal-dialog modal-sm">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" onclick="javascript:window.location.reload()">&times;</button>
			<h4 class="modal-title">Edit WorkType</h4>
		</div>
		<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
	</div>
	
	
</div>
