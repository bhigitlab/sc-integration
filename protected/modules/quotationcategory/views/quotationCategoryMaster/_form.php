<?php
/* @var $this QuotationCategoryMasterController */
/* @var $model QuotationCategoryMaster */
/* @var $form CActiveForm */
?>
<?php if (Yii::app()->user->hasFlash('success')) : ?>
    <div class="flash-holder flash-success">
        <?php echo Yii::app()->user->getFlash('success'); ?>
    </div>
<?php
endif;
if (Yii::app()->user->hasFlash('error')) :
    ?>
    <div class="alert alert-danger  alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <?php echo Yii::app()->user->getFlash('error'); ?>
    </div>
    <?php endif; ?>
<div class="form">
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'quotation-category-master-form',
        'enableClientValidation' => true,
        'clientOptions' => array(
            'validateOnChange' => true,
            'validateOnType' => false,
        )
    ));
    ?>
    
    <div class="row">
        <div class="col-md-12">
            <?php echo $form->labelEx($model, 'name'); ?>
            <?php echo $form->textField($model, 'name', array('size' => 50, 'maxlength' => 50, 'class' => 'form-control')); ?>
            <?php echo $form->error($model, 'name'); ?>
        </div>
        <div class="col-md-6 save-btnHold">
            <label style="display:block;">&nbsp;</label>
            <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-sm btn-info submit')); ?>
            <?php
            if (!$model->isNewRecord) {
                echo CHtml::Button('Close', array('class' => 'btn', 'onclick' => 'javascript:location.href="' . $this->createUrl('index') . '"'));
            } else {
                echo CHtml::ResetButton('Reset', array('style' => 'margin-right:3px;', 'class' => 'btn btn-sm btn-default'));
            }
            ?>
        </div>
    </div>
<?php $this->endWidget(); ?>
</div>