ALTER TABLE `jp_quotation_worktype_master` ADD `template_id` INT  NULL AFTER `name`;

CREATE TABLE `jp_worktype_template` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `page` varchar(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '1 = Active, 0 = Inactive',
  `created_by` int(11) DEFAULT NULL COMMENT 'From user table',
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `jp_worktype_template` (`id`, `name`, `page`, `status`, `created_by`, `created_date`) VALUES
(1, 'Box Template', '_template1', 1, NULL, '2021-08-26 17:11:14'),
(2, 'Additional ', '_template2', 1, NULL, '2021-08-26 17:11:54'),
(3, 'Other ', '_template3', 1, NULL, '2021-08-26 17:13:15');

ALTER TABLE `jp_worktype_template`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`);

  ALTER TABLE `jp_worktype_template`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

  ALTER TABLE `jp_worktype_template`
  ADD CONSTRAINT `jp_worktype_template_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `jp_users` (`userid`) ON UPDATE CASCADE;

ALTER TABLE `jp_quotation_worktype_master` ADD FOREIGN KEY (`template_id`) REFERENCES `jp_worktype_template`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

UPDATE `jp_quotation_worktype_master`
   SET template_id = CASE id 
                      WHEN 1 THEN 1 
                      WHEN 2 THEN 3 
                      WHEN 3 THEN 3 
                      WHEN 4 THEN 2 
                      END
 WHERE id IN('1', '2','3','4');

ALTER TABLE `jp_sales_quotation` ADD `tax_slab` FLOAT NULL AFTER `revision_approved`,
 ADD `cgst_amount` FLOAT(11,2) NULL AFTER `tax_slab`,
  ADD `cgst_percent` FLOAT(11,2) NULL AFTER `cgst_amount`, 
  ADD `sgst_amount` FLOAT(11,2) NULL AFTER `cgst_percent`, 
  ADD `sgst_percent` FLOAT(11,2) NULL AFTER `sgst_amount`, 
  ADD `igst_amount` FLOAT(11,2)  NULL AFTER `sgst_percent`,
   ADD `igst_percent` FLOAT(11,2)  NULL AFTER `igst_amount`,
   ADD `mainitem_total` FLOAT(11,2) NULL AFTER `igst_percent`,
   ADD `mainitem_tax` FLOAT(11,2) NULL AFTER `mainitem_total`,
   ADD `maintotal_withtax` FLOAT(11,2) NULL AFTER `mainitem_tax`;



ALTER TABLE `jp_sales_quotation` ADD `sub_status` TINYINT NOT NULL DEFAULT '0' COMMENT '1=\'subitem\',0=\'item\'' AFTER `maintotal_withtax`;

ALTER TABLE `jp_sales_quotation` ADD `subitem_label` VARCHAR(250) NULL AFTER `sub_status`;

UPDATE `jp_worktype_template` SET `name` = 'Other ', `page` = '_template3' WHERE `jp_worktype_template`.`id` = 2;
UPDATE `jp_worktype_template` SET `name` = 'Additional ', `page` = '_template2' WHERE `jp_worktype_template`.`id` = 3;

UPDATE `jp_quotation_worktype_master` SET `template_id` = '2' WHERE `jp_quotation_worktype_master`.`id` = 2;
UPDATE `jp_quotation_worktype_master` SET `template_id` = '2' WHERE `jp_quotation_worktype_master`.`id` = 3;
UPDATE `jp_quotation_worktype_master` SET `template_id` = '3' WHERE `jp_quotation_worktype_master`.`id` = 4;


INSERT INTO `jp_menu` (`menu_id`, `menu_name`, `parent_id`, `status`, `controller`, `action`, `params`, `showmenu`, `show_list`, `related`) VALUES (766, 'Quantity estimation report', '561', '0', 'projects', 'projectsquantityreport', '', '2', '1', NULL);
ALTER TABLE `jp_quotation_item_master` ADD `additional_image` VARCHAR(250) NULL AFTER `image`;

ALTER TABLE `jp_sales_quotation` ADD `image` VARCHAR(500) NULL AFTER `mrp`;



SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";



CREATE TABLE `jp_quotation_template` (
  `template_id` int(11) NOT NULL,
  `template_name` varchar(100) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0->inactive,1->active',
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


INSERT INTO `jp_quotation_template` (`template_id`, `template_name`, `status`, `description`) VALUES
(1, 'template1', '0', 'Default template'),
(2, 'template2', '1', 'template2');

ALTER TABLE `jp_quotation_template`
  ADD PRIMARY KEY (`template_id`);

ALTER TABLE `jp_quotation_template`
  MODIFY `template_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;


INSERT INTO `jp_quotation_template` (`template_id`, `template_name`, `status`, `description`) VALUES ('3', 'template3', '0', 'template3');

INSERT INTO `jp_menu` (`menu_id`, `menu_name`, `parent_id`, `status`, `controller`, `action`, `params`, `showmenu`, `show_list`, `related`) VALUES (767, 'Warehouse', '704', '0', 'wh/warehouse', 'stockcorrection', '', '2', '1', NULL);
-- change query

CREATE TABLE `jp_warehouse_stock_correction` ( `id` INT NOT NULL AUTO_INCREMENT , `warehouse_id` INT NOT NULL , `wh_receipt_id` INT NOT NULL , `wh_receipt_item_id` INT NOT NULL, `Item_Id` INT NOT NULL , `wh_receipt_quantity` FLOAT NOT NULL , `stock_quantity` FLOAT NOT NULL , `created_by` INT NOT NULL , `created_date` DATETIME NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;

INSERT INTO `jp_menu` (`menu_id`, `menu_name`, `parent_id`, `status`, `controller`, `action`, `params`, `showmenu`, `show_list`, `related`) VALUES ('768', 'Stock Correction Report', '561', '0', 'report/reports', 'stockCorrectionReport', '', '2', '1', NULL);


INSERT INTO `jp_menu` (`menu_id`, `menu_name`, `parent_id`, `status`, `controller`, `action`, `params`, `showmenu`, `show_list`, `related`) VALUES (769, 'Defect Return', '704', '0', 'wh/warehouse', 'defectreturn', '', '2', '1', NULL);


CREATE TABLE `defect_return` ( `return_id` INT NOT NULL AUTO_INCREMENT , `receipt_id` INT(11) NULL DEFAULT NULL , `return_number` TEXT NOT NULL , `return_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP , `return_amount` FLOAT NOT NULL  , `company_id` INT(11) NOT NULL , `created_by` INT(11) NOT NULL , `created_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP , `updated_by` INT(11) NULL DEFAULT NULL , `updated_date` TIMESTAMP NULL DEFAULT NULL , `return_status` INT(11) NULL DEFAULT NULL , PRIMARY KEY (`return_id`)) ENGINE = InnoDB;

CREATE TABLE `jp_defect_returnitem` ( `returnitem_id` INT(11) NOT NULL AUTO_INCREMENT , `return_id` INT(11) NOT NULL , `receiptitem_id` INT(11) NULL DEFAULT NULL , `returnitem_description` MEDIUMTEXT NULL DEFAULT NULL , `returnitem_quantity` FLOAT NOT NULL , `returnitem_unit` VARCHAR(20) NOT NULL , `returnitem_rate` FLOAT NOT NULL , `returnitem_amount` FLOAT NOT NULL ,  `category_id` INT NULL DEFAULT NULL , `remark` VARCHAR(100) NULL DEFAULT NULL ,  `created_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP , `created_by` INT(11) NOT NULL , PRIMARY KEY (`returnitem_id`)) ENGINE = InnoDB;

ALTER TABLE `jp_warehouse_stock_correction` ADD `rate` FLOAT(11,2) NOT NULL AFTER `stock_quantity`;


UPDATE `jp_menu` SET `menu_name` = 'Stock Correction' WHERE `jp_menu`.`menu_id` = 767;

ALTER TABLE `jp_sales_quotation` ADD `revision_delete_status` ENUM('0','1') NOT NULL DEFAULT '1' COMMENT '0=>Deleted, 1=>Not Deleted' AFTER `subitem_label`;

ALTER TABLE `jp_warehouse_stock_correction` CHANGE `warehouse_id` `warehouse_id` INT(11) NULL DEFAULT NULL, CHANGE `wh_receipt_id` `wh_receipt_id` INT(11) NULL DEFAULT NULL;

ALTER TABLE `jp_warehouse_stock_correction` ADD `approval_status` ENUM('0', '1', '2') NOT NULL DEFAULT '0' COMMENT '0=>Pending,1=>approved,2=>rejected' AFTER `rate`;


CREATE TABLE  `jp_quotation_section` ( `id` INT NOT NULL AUTO_INCREMENT , `qtn_id` INT NOT NULL , `section_name` VARCHAR(200) NOT NULL , `unit` INT NULL DEFAULT NULL , `quantity` INT NULL DEFAULT NULL , `quantity_nos` INT NULL DEFAULT NULL , `mrp` FLOAT(10,2) NULL DEFAULT NULL , `amount_after_discount` FLOAT(10,2) NULL DEFAULT NULL , `description` VARCHAR(300)  NULL DEFAULT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;

CREATE TABLE  `jp_quotation_gen_category` ( `id` INT NOT NULL AUTO_INCREMENT , `qid` INT NOT NULL , `section_id` INT NOT NULL , `category_label` VARCHAR(200) NOT NULL , `master_cat_id` INT NULL DEFAULT NULL , `unit` INT NULL DEFAULT NULL , `quantity` INT NULL DEFAULT NULL , `quantity_nos` INT NULL DEFAULT NULL , `mrp` FLOAT(10,2) NULL DEFAULT NULL , `amount_after_discount` FLOAT(10,2) NULL DEFAULT NULL , `description` VARCHAR(300) NULL DEFAULT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;

ALTER TABLE `jp_sales_quotation` ADD `parent_type` ENUM('0','1') NULL DEFAULT NULL COMMENT '0=>category,1=>work type' AFTER `subitem_label`, ADD `parent_id` INT NULL DEFAULT NULL AFTER `parent_type`;

CREATE TABLE  `jp_quotation_gen_worktype` ( `id` INT NOT NULL AUTO_INCREMENT , `qid` INT NOT NULL , `section_id` INT NOT NULL , `category_gen_id` INT NOT NULL , `unit` INT NULL DEFAULT NULL , `quantity` INT NULL DEFAULT NULL , `quantity_nos` INT NULL DEFAULT NULL , `mrp` FLOAT(10,2) NULL DEFAULT NULL , `amount_after_discount` FLOAT(10,2) NULL DEFAULT NULL , `description` VARCHAR(300) NULL DEFAULT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;

ALTER TABLE `jp_quotation_section` ADD `created_date` DATETIME NOT NULL AFTER `description`, ADD `created_by` INT NOT NULL AFTER `created_date`;
ALTER TABLE `jp_quotation_gen_category` ADD `created_date` DATETIME NOT NULL AFTER `description`, ADD `created_by` INT NOT NULL AFTER `created_date`;
ALTER TABLE `jp_quotation_gen_worktype` ADD `created_date` DATETIME NOT NULL AFTER `description`, ADD `created_by` INT NOT NULL AFTER `created_date`;
ALTER TABLE `jp_quotation_gen_worktype` ADD `worktype_label` VARCHAR(100) NOT NULL AFTER `category_gen_id`;
ALTER TABLE `jp_quotation_gen_worktype` CHANGE `category_gen_id` `category_label_id` INT(11) NOT NULL;
ALTER TABLE `jp_quotation_gen_worktype` ADD `master_cat_id` INT(11) NULL DEFAULT NULL AFTER `worktype_label`;
ALTER TABLE `jp_sales_quotation` CHANGE `parent_type` `parent_type` INT NULL DEFAULT NULL COMMENT '0=>category,1=>work type';
ALTER TABLE `jp_quotation_gen_worktype` CHANGE `description` `description` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;
ALTER TABLE `jp_quotation_section` ADD `revision_no` VARCHAR(120) NOT NULL AFTER `description`;
ALTER TABLE `jp_quotation_gen_category` ADD `revision_no` VARCHAR(120) NOT NULL AFTER `description`;
ALTER TABLE `jp_quotation_gen_worktype` ADD `revision_no` VARCHAR(120) NOT NULL AFTER `description`;
ALTER TABLE `jp_quotation_section` ADD `ref_id` INT NULL DEFAULT NULL AFTER `id`;
ALTER TABLE `jp_quotation_gen_category` ADD `ref_id` INT NULL DEFAULT NULL AFTER `id`;
ALTER TABLE `jp_quotation_gen_worktype` ADD `ref_id` INT NULL DEFAULT NULL AFTER `id`;
ALTER TABLE  `jp_sales_quotation` DROP FOREIGN KEY jp_sales_quotation_ibfk_1;

UPDATE `jp_expenses` SET `reconciliation_status` = NULL WHERE `expense_type` = 0;

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


CREATE TABLE `jp_project_template` (
  `template_id` int(11) NOT NULL,
  `template_name` varchar(100) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '0',
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



INSERT INTO `jp_project_template` (`template_id`, `template_name`, `status`, `description`) VALUES
(1, 'AGAC', '0', 'AGAC'),
(2, 'D2R', '0', 'D2R'),
(3, 'AMZER', '1', 'AMZER');


ALTER TABLE `jp_project_template`
  ADD PRIMARY KEY (`template_id`);

ALTER TABLE `jp_project_template`
  MODIFY `template_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

INSERT INTO `jp_menu` (`menu_id`, `menu_name`, `parent_id`, `status`, `controller`, `action`, `params`, `showmenu`, `show_list`, `related`) VALUES ('770', 'Remove Category', '424', '0', 'PurchaseCategory', 'removecategory', '', '2', '1', NULL);

ALTER TABLE `jp_subcontractorbillitem` ADD `item_type` VARCHAR(100) NULL DEFAULT NULL AFTER `description`;

ALTER TABLE `jp_subcontractorbillitem` ADD `item_quantity` FLOAT NULL DEFAULT NULL AFTER `item_type`, ADD `item_unit` VARCHAR(500) NULL DEFAULT NULL AFTER `item_quantity`, ADD `item_rate` FLOAT NULL DEFAULT NULL AFTER `item_unit`;

INSERT INTO `jp_menu` (`menu_id`, `menu_name`, `parent_id`, `status`, `controller`, `action`, `params`, `showmenu`, `show_list`, `related`) VALUES ('771', 'getexpensehead', '288', '0', 'projects', 'getexpensehead', '', '0', '0', NULL);

INSERT INTO `jp_menu` (`menu_id`, `menu_name`, `parent_id`, `status`, `controller`, `action`, `params`, `showmenu`, `show_list`, `related`) VALUES ('772', 'Remove ClientType', '349', '0', 'projectType', 'removeclientype', '', '2', '1', NULL);

ALTER TABLE `jp_clients` ADD CONSTRAINT `clients_projtype_fk` FOREIGN KEY (`project_type`) REFERENCES `jp_project_type`(`ptid`) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `jp_purchase` ADD CONSTRAINT `projectfk` FOREIGN KEY (`project_id`) REFERENCES `jp_projects`(`pid`) ON DELETE RESTRICT ON UPDATE CASCADE; 

ALTER TABLE `jp_users` ADD CONSTRAINT `jp_users_client_id_fk` FOREIGN KEY (`client_id`) REFERENCES `jp_clients`(`cid`) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE `jp_projects` ADD CONSTRAINT `clientid_fk` FOREIGN KEY (`client_id`) REFERENCES `jp_clients`(`cid`) ON DELETE RESTRICT ON UPDATE CASCADE;

INSERT INTO `jp_menu` (`menu_id`, `menu_name`, `parent_id`, `status`, `controller`, `action`, `params`, `showmenu`, `show_list`, `related`) VALUES ('773', 'Remove Client', '324', '0', 'clients', 'removeclient', '', '2', '1', NULL);

INSERT INTO `jp_menu` (`menu_id`, `menu_name`, `parent_id`, `status`, `controller`, `action`, `params`, `showmenu`, `show_list`, `related`) VALUES ('774', 'Remove Company', '416', '0', 'company', 'removecompany', '', '2', '1', NULL);

ALTER TABLE `jp_users` ADD CONSTRAINT `users_type_fk` FOREIGN KEY (`user_type`) REFERENCES `jp_user_roles`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

INSERT INTO `jp_menu` (`menu_id`, `menu_name`, `parent_id`, `status`, `controller`, `action`, `params`, `showmenu`, `show_list`, `related`) VALUES ('775', 'Remove UserRole', '359', '0', 'userRoles', 'removeuserole', '', '2', '1', NULL);

ALTER TABLE `jp_projects` ADD CONSTRAINT `createdby_fk` FOREIGN KEY (`created_by`) REFERENCES `jp_users`(`userid`) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `jp_expenses` ADD `duplicate_ignore_status` ENUM('0','1') NOT NULL DEFAULT '0' COMMENT '0=>Not Ignored, 1=>Ignored' AFTER `approval_status`, ADD `duplicate_delete_status` ENUM('0','1') NOT NULL DEFAULT '0' COMMENT '0=>Not Deleted, 1=>Deleted' AFTER `duplicate_ignore_status`;

ALTER TABLE `jp_bills` ADD `duplicate_ignore_status` ENUM('0','1') NOT NULL DEFAULT '0' COMMENT '0=>Not Ignored, 1=>Ignored' AFTER `warehouse_id`, ADD `duplicate_delete_status` ENUM('0','1') NOT NULL DEFAULT '0' COMMENT '0=>Not Deleted, 1=>Deleted' AFTER `duplicate_ignore_status`;

ALTER TABLE `jp_dailyvendors` ADD `duplicate_ignore_status` ENUM('0','1') NOT NULL DEFAULT '0' COMMENT '0=>Not Ignored, 1=>Ignored' AFTER `approval_status`, ADD `duplicate_delete_status` ENUM('0','1') NOT NULL DEFAULT '0' COMMENT '0=>Not Deleted, 1=>Deleted' AFTER `duplicate_ignore_status`;


ALTER TABLE `jp_dailyexpense` ADD `duplicate_ignore_status` ENUM('0','1') NOT NULL DEFAULT '0' COMMENT '0=>Not Ignored, 1=>Ignored' AFTER `approval_status`, ADD `duplicate_delete_status` ENUM('0','1') NOT NULL DEFAULT '0' COMMENT '0=>Not Deleted, 1=>Deleted' AFTER `duplicate_ignore_status`;


ALTER TABLE `jp_subcontractor_payment` ADD `duplicate_ignore_status` ENUM('0','1') NOT NULL DEFAULT '0' COMMENT '0=>Not Ignored, 1=>Ignored' AFTER `approval_status`, ADD `duplicate_delete_status` ENUM('0','1') NOT NULL DEFAULT '0' COMMENT '0=>Not Deleted, 1=>Deleted' AFTER `duplicate_ignore_status`;

CREATE TABLE `jp_quotation_revision` ( `id` INT(11) NOT NULL AUTO_INCREMENT , `qid` INT(11) NOT NULL , `revision_no` VARCHAR(120) NOT NULL , `tax_slab` FLOAT NULL DEFAULT NULL , `sgst_percent` FLOAT(11,2) NULL DEFAULT NULL , `sgst_amount` FLOAT(11,2) NULL DEFAULT NULL , `cgst_percent` FLOAT(11,2) NULL DEFAULT NULL , `cgst_amount` FLOAT(11,2) NULL DEFAULT NULL , `igst_percent` FLOAT(11,2) NULL DEFAULT NULL , `igst_amount` FLOAT(11,2) NULL DEFAULT NULL , `total_after_discount` FLOAT(11,2) NULL DEFAULT NULL , `round_off` FLOAT(11,2) NULL DEFAULT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;

UPDATE `jp_project_template` SET `template_name` = 'TYPE-1' WHERE `jp_project_template`.`template_id` = 1;
UPDATE `jp_project_template` SET `template_name` = 'TYPE-2' WHERE `jp_project_template`.`template_id` = 2;
UPDATE `jp_project_template` SET `template_name` = 'TYPE-3' WHERE `jp_project_template`.`template_id` = 3;

ALTER TABLE `jp_sales_quotation_master` ADD `template_type` ENUM('1','2') NOT NULL DEFAULT '1' COMMENT '1=>agac, 2=>d2r' AFTER `revision_no`;


DELETE FROM `jp_specification` WHERE `spec_status` ="0";



ALTER TABLE `jp_purchase_items` ADD CONSTRAINT `jp_purchase_itemse_category_id` FOREIGN KEY (`category_id`) REFERENCES `jp_specification`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

UPDATE `jp_expenses` SET `reconciliation_status`=0  WHERE `payment_type` = 88 AND `reconciliation_status` IS NULL;


UPDATE `jp_expense_notification` SET `view_status`= 2 ,`expense_perc` = NULL WHERE project_id = 9;

UPDATE `jp_expense_notification` SET `expense_perc`= 50,`view_status`= 1  WHERE project_id = 9
 ORDER BY id DESC LIMIT 1;