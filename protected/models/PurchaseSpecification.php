<?php

/**
 * This is the model class for table "{{purchase_category}}".
 *
 * The followings are the available columns in table '{{purchase_category}}':
 * @property integer $id
 * @property integer $parent_id
 * @property string $category_name
 * @property string $specification
 * @property string $spec_flag
 * @property integer $created_by
 * @property string $created_date
 * @property integer $spec_status
 */
class PurchaseSpecification extends CActiveRecord
{

    /**
     * @return string the associated database table name
     */
    public $rules_array = array();

    public function tableName()
    {
        return '{{purchase_category}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        $action =  Yii::app()->controller->action->id;
        if ($action == 'create') {
            $this->rules_array = array(
                array('category_name, spec_flag, created_by, created_date', 'required'),
                array('parent_id, created_by', 'numerical', 'integerOnly' => true),
                array('category_name,unit', 'length', 'max' => 100),
                array('specification', 'length', 'max' => 255),
                array('spec_flag', 'length', 'max' => 1),
                array('category_name', 'unique'),
                // The following rule is used by search().
                // @todo Please remove those attributes that should not be searched.
                array('id, parent_id, category_name, specification, spec_flag, created_by, created_date , dieno ', 'safe', 'on' => 'search'),
            );
        } elseif ($action == 'update') {
            $this->rules_array = array(
                array('category_name, spec_flag, created_by, created_date', 'required'),
                array('parent_id, created_by', 'numerical', 'integerOnly' => true),
                array('category_name,unit', 'length', 'max' => 100),
                array('specification', 'length', 'max' => 255),
                array('spec_flag', 'length', 'max' => 1),
                array('category_name', 'unique'),
                array('category_name', 'categorycheck'),
                // The following rule is used by search().
                // @todo Please remove those attributes that should not be searched.
                array('id, parent_id, category_name, specification, spec_flag, created_by, created_date, dieno', 'safe', 'on' => 'search'),
            );
        } elseif ($action == 'createspecification') {
            $this->rules_array = array(
                array('parent_id, unit', 'required'),
                array('parent_id, created_by, brand_id', 'numerical', 'integerOnly' => true),
                //array('category_name', 'length', 'max'=>100),
                array('specification,unit', 'length', 'max' => 255),
                array('unit', 'length', 'max' => 255),
                array('spec_flag', 'length', 'max' => 1),
                array('parent_id', 'checkexists'),
                array('dieno,filename', 'safe'),
                array('dieno', 'safe'),
                array('specification_type', 'checkspc'),


                // The following rule is used by search().
                // @todo Please remove those attributes that should not be searched.
                array('id, parent_id, brand_id, category_name, specification, unit, spec_flag, type, dieno, length, specification_type,filename, company_id, created_by, created_date', 'safe', 'on' => 'search'),
            );
        }
        return $this->rules_array;
    }

    public function checkexists($attribute, $params)
    {

        $tblpx = Yii::app()->db->tablePrefix;
        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        $newQuery = "";
        foreach ($arrVal as $arr) {
            if ($newQuery)
                $newQuery .= ' OR';
            $newQuery .= " FIND_IN_SET('" . $arr . "', company_id)";
        }
        if ($this->id == '') {

            if ($this->parent_id != '' && $this->specification != '' && $this->brand_id != '') {
                $data = PurchaseCategory::model()->findByAttributes(array('parent_id' => $this->parent_id, 'specification' => $this->specification, 'brand_id' => $this->brand_id), array('condition' => $newQuery));
            }

            if ($this->parent_id != '' && $this->specification != '' && $this->brand_id == '') {
                $data = Yii::app()->db->createCommand()
                        ->select('*')
                        ->from('' . $tblpx . 'purchase_category')
                        ->where('parent_id=:parent_id and specification=:specification and brand_id IS NULL and (' . $newQuery . ')', array(':parent_id' => $this->parent_id, ':specification' => $this->specification))
                        ->queryRow();
                //$data = PurchaseCategory::model()->find(array('condition' => 'parent_id ='. $this->parent_id.' AND specification ="'. $this->specification.'" AND brand_id IS NULL' ));
            }
        } else {
            if ($this->parent_id != '' && $this->specification != '' && $this->brand_id != '') {
                $data = PurchaseCategory::model()->findByAttributes(array('parent_id' => $this->parent_id, 'specification' => $this->specification, 'brand_id' => $this->brand_id), array('condition' => 'id !=' . $this->id . ' AND (' . $newQuery . ')'));
            }

            if ($this->parent_id != '' && $this->specification != '' && $this->brand_id == '') {
                $data = Yii::app()->db->createCommand()
                        ->select('*')
                        ->from('' . $tblpx . 'purchase_category')
                        ->where('parent_id=:parent_id and specification=:specification and brand_id IS NULL and (' . $newQuery . ') AND id != ' . $this->id . '', array(':parent_id' => $this->parent_id, ':specification' => $this->specification))
                        ->queryRow();
                //$data = PurchaseCategory::model()->find(array('condition' => 'parent_id ='. $this->parent_id.' AND specification ="'. $this->specification.'" AND brand_id IS NULL' ));
            }
        }
        if (!empty($data)) {
            $this->addError("specification", ("specification is already taken"));
        }
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'parent_id' => 'Items ',
            'category_name' => 'Category Name',
            'specification' => 'Specification',
            'spec_flag' => 'Spec Flag',
            'created_by' => 'Created By',
            'unit' => 'Unit',
            'created_date' => 'Created Date',
            'company_id' => 'Company',
            'spec_status' => 'Status',
            'brand_id' => 'Brand',
            'type' => 'Type',
            'dieno' => 'Dieno',
            'length' => 'Length',
            'specification_type' => 'Specification Type',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function specificationsearch() {
        $tblpx = Yii::app()->db->tablePrefix;
        $criteria = new CDbCriteria;
        $criteria->select = 'child.id,t.category_name, child.specification, child.unit, child.created_date, child.brand_id,child.company_id,child.hsn_code,child.specification_type,child.dieno';
        $criteria->join = ' LEFT JOIN `' . $tblpx . 'purchase_category` AS `child` ON t.id = child.parent_id';
        //$criteria->join = ' LEFT JOIN `'.$tblpx.'purchase_category` AS `child` ON t.id = child.parent_id';
        $criteria->addCondition('child.parent_id IS NOT NULL');
        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        $newQuery = "";
        foreach ($arrVal as $arr) {
            if ($newQuery)
                $newQuery .= ' OR';
            $newQuery .= " FIND_IN_SET('" . $arr . "', child.company_id)";
        }
        $criteria->addCondition('child.specification IS NOT NULL AND child.spec_status = 1 AND (' . $newQuery . ')');
        $criteria->order = 'child.id DESC';
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => false,
        ));
    }

    public function checkspc($attribute, $params)
    {
        if ($this->specification_type == 'A') {
            if ($this->dieno == '') {
                $this->addError("dieno", (" Profile/Dieno Required"));
            }
        } else {
            if ($this->specification == '') {
                $this->addError("specification", (" Specification Required"));
            }
        }
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return PurchaseCategory the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
