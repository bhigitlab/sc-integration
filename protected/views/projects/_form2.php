<?php
$user = Users::model()->findByPk(Yii::app()->user->id);
$arrVal = explode(',', $user->company_id);
$newQuery = "";
foreach ($arrVal as $arr) {
    if ($newQuery)
        $newQuery .= ' OR';
    $newQuery .= " FIND_IN_SET('" . $arr . "', company_id)";
}
$pms_api_integration = ApiSettings::model()->pmsIntegrationStatus();
?>

<div class="">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'projects-form',
        'enableAjaxValidation' => true,

        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true,
            'validateOnType' => false,
            'afterValidate' => 'js:function(form, data, hasError) {
            if(hasError) {
            if(typeof $(".error").first().offset() !== "undefined") {
                       $("html, body").animate({scrollTop: ($(".error").first().position().top- 100) + "px"}, 1000);
                       $(".error").first().find("input, select").focus();
                   }
                return false;
            }
            else
            {
             return true;
               }
           }',
        ),
    ));
    ?>
    <!-- Popup content-->
    <div class="custom-form-style">
        <div class="row">
            <?php if (Yii::app()->user->role != 10) { ?>
                <div class="col-lg-2 col-md-3 col-sm-4 form-group">
                    <?php echo $form->labelEx($model, 'name'); ?>
                    <?php echo $form->textField($model, 'name', array('size' => 60, 'maxlength' => 100, 'class' => 'form-control')); ?>
                    <?php echo $form->error($model, 'name'); ?>
                </div>
            <?php } ?>
            <div class="col-lg-2 col-md-3 col-sm-4 form-group">
                <?php
                echo $form->labelEx($model, 'Client  <span class="required">*</span>');
                $pms_api_integration = ApiSettings::model()->pmsIntegrationStatus();

                if ($pms_api_integration == 1) {
                    $clients = Clients::model()->findAll(array(
                        'order' => 'name ASC',
                        'condition' => 'client_mapping_id > 0',
                    ));
                } else {
                    $clients = Clients::model()->findAll(array('order' => 'name ASC', 'condition' => 'status= 1 AND (' . $newQuery . ')'));
                }
                echo $form->dropDownList(
                    $model,
                    'client_id',
                    CHtml::listData($clients, 'cid', 'name'),
                    array('empty' => '--', 'class' => 'form-control')
                );
                echo $form->error($model, 'client_id');
                ?>
            </div>
            <div class="col-md-2 col-sm-4 form-group">
                <?php echo $form->label($model, 'start_date'); ?>
                <?php echo CHtml::activeTextField($model, 'start_date', array('size' => 10, 'readonly' => 'true', 'class' => 'form-control')); ?>
                <?php
                $this->widget('application.extensions.calendar.SCalendar', array(
                    'inputField' => 'Projects_start_date',
                    'ifFormat' => '%d-%b-%Y',
                ));
                ?>
                <?php echo $form->error($model, 'start_date'); ?>
            </div>
            <div class="col-md-2 col-sm-4 form-group">
                <?php echo $form->label($model, 'completion_date'); ?>
                <?php echo CHtml::activeTextField($model, 'completion_date', array('size' => 10, 'readonly' => 'true', 'class' => 'form-control')); ?>
                <?php
                $this->widget('application.extensions.calendar.SCalendar', array(
                    'inputField' => 'Projects_completion_date',
                    'ifFormat' => '%d-%b-%Y',
                ));
                ?>
                <?php echo $form->error($model, 'completion_date'); ?>
            </div>
            <div class="col-md-2 col-sm-4 form-group">
                <?php echo $form->labelEx($model, 'project_duration'); ?>
                <?php echo $form->textField($model, 'project_duration', array('size' => 60, 'maxlength' => 200, 'class' => 'form-control')); ?>
                <?php echo $form->error($model, 'project_duration'); ?>
            </div>
            <?php if (Yii::app()->user->role != 10) { ?>
                <div class="col-lg-2 col-md-3 col-sm-4 form-group">
                    <?php echo $form->labelEx($model, 'site'); ?>
                    <?php echo $form->textField($model, 'site', array('size' => 60, 'maxlength' => 100, 'class' => 'form-control')); ?>
                    <?php echo $form->error($model, 'site'); ?>
                </div>
            <?php } ?>
            <div class="col-lg-2 col-md-3 col-sm-4 form-group">
                <?php echo $form->labelEx($model, 'project_category'); ?>
                <div>
                    <?php echo $form->radioButton($model, 'project_category', array('value' => 104, 'uncheckValue' => null)); ?>
                    Residential
                    <?php echo $form->radioButton($model, 'project_category', array('value' => 105, 'uncheckValue' => null, 'class' => 'ml-1')); ?>
                    Commercial
                </div>
                <?php echo $form->error($model, 'project_category'); ?>
            </div>
            <?php if (Yii::app()->user->role != 10) { ?>
                <div class="col-lg-2 col-md-3 col-sm-4 form-group">
                    <?php echo $form->labelEx($model, 'project_status'); ?>
                    <?php echo $form->dropDownList($model, 'project_status', CHtml::listData(Status::model()->findAll(
                        array(
                            'select' => array('sid,caption'),
                            'condition' => 'status_type="project_status"',
                            'order' => 'caption',
                            'distinct' => true
                        )
                    ), 'sid', 'caption'), array('empty' => '--', 'class' => 'form-control')); ?>
                    <?php echo $form->error($model, 'project_status'); ?>
                </div>
                <div class="col-lg-2 col-md-3 col-sm-4 form-group">
                    <?php echo $form->labelEx($model, 'project_type'); ?>
                    <?php echo $form->dropDownList($model, 'project_type', CHtml::listData(Status::model()->findAll(
                        array(
                            'select' => array('sid,caption'),
                            'condition' => 'status_type="project_type"',
                            'order' => 'caption',
                            'distinct' => true
                        )
                    ), 'sid', 'caption'), array('empty' => '--', 'class' => 'form-control')); ?>
                    <?php echo $form->error($model, 'project_type'); ?>
                </div>
                <div class="col-lg-2 col-md-3 col-sm-4 form-group">
                    <?php echo $form->labelEx($model, 'sqft'); ?>
                    <?php echo $form->textField($model, 'sqft', array('class' => 'form-control')); ?>
                    <?php echo $form->error($model, 'sqft'); ?>
                </div>
                <div class="col-lg-2 col-md-3 col-sm-4 form-group hidbox" style="display:none">
                    <?php echo $form->labelEx($model, 'sqft_rate'); ?>
                    <?php echo $form->textField($model, 'sqft_rate', array('onChange' => 'sum1()', 'onBlur' => 'sum1()', 'class' => 'form-control')); ?>
                    <?php echo $form->error($model, 'sqft_rate'); ?>
                </div>

                <div class="col-lg-2 col-md-3 col-sm-4 form-group hidbox1" style="display:none">
                    <?php echo $form->labelEx($model, 'percentage'); ?>
                    <?php echo $form->textField($model, 'percentage', array('class' => 'form-control')); ?>
                    <?php echo $form->error($model, 'percentage'); ?>
                </div>
            <?php } ?>
            <?php if (Yii::app()->user->role != 10) { ?>
                <div class="col-md-2 col-sm-4 form-group">
                    <?php echo $form->labelEx($model, 'project_quote'); ?>
                    <?php echo $form->textField($model, 'project_quote', array('class' => 'form-control')); ?>
                    <?php echo $form->error($model, 'project_quote'); ?>
                </div>
                <div class="col-md-2 col-sm-4 form-group">
                    <label>Profit Margin (%)</label>
                    <?php echo $form->textField($model, 'profit_margin', array('class' => 'form-control')); ?>
                    <?php echo $form->error($model, 'profit_margin'); ?>
                </div>
            <?php } ?>

            <?php
            $buyermodule = GeneralSettings::model()->checkBuyerModule();
            if ($buyermodule) { ?>
                <div class="col-lg-2 col-md-3 col-sm-4 form-group">
                    <?php echo $form->labelEx($model, 'number_of_flats'); ?>
                    <?php echo $form->textField($model, 'number_of_flats', array('class' => 'form-control')); ?>
                    <?php echo $form->error($model, 'number_of_flats'); ?>
                </div>
                <div class="col-lg-2 col-md-3 col-sm-4 form-group">
                    <?php echo $form->labelEx($model, 'flat_names'); ?>
                    <?php echo $form->textArea($model, 'flat_names', array('rows' => 5, 'cols' => 10, 'class' => 'form-control h_area')); ?>
                    <span class="comment">(comma separted values)</span>
                    <?php echo $form->error($model, 'flat_names'); ?>
                </div>
            <?php } ?>
            <div class="col-md-2 col-sm-4 form-group">
                <?php echo $form->labelEx($model, 'incharge'); ?>
                <?php echo $form->textField($model, 'incharge', array('class' => 'form-control')); ?>
                <?php echo $form->error($model, 'incharge'); ?>
            </div>
        </div>
        <div class="row">
            <?php if (Yii::app()->user->role != 10) { ?>
                <div class="col-lg-4 col-md-6 textArea-box">
                    <?php echo $form->labelEx($model, 'description'); ?>
                    <?php echo $form->textArea($model, 'description', array('rows' => 4, 'cols' => 50, 'class' => 'form-control')); ?>
                    <?php echo $form->error($model, 'description'); ?>
                </div>
            <?php } ?>
            <div class="col-lg-4 col-md-6 form-group">
                <label>Company :</label>
                <ul class="checkboxList">
                    <?php
                    $newQuery = "";
                    $user = Users::model()->findByPk(Yii::app()->user->id);
                    $arrVal = explode(',', $user->company_id);
                    $comp_array = implode("', '", $arrVal);
                    if ($newQuery)
                        $newQuery .= ' OR';
                    if ($comp_array) {
                        $newQuery .= " id IN ('" . $comp_array . "')";
                    }
                    $typelist = Company::model()->findAll(array('condition' => $newQuery));
                    $assigned_company_array = array();
                    echo CHtml::checkBoxList('Projects[company_id]', $assigned_company_array, CHtml::listData($typelist, 'id', 'name'), array('checkAll' => 'Select All', 'template' => '<li class="checkboxtype">{input}{label}</li>', 'separator' => ''));
                    ?>
                </ul>
                <?php echo $form->error($model, 'company_id'); ?>
            </div>
            <div class="col-lg-4 col-md-6 form-group">
                <label>Assign to :</label>
                <ul class="checkboxList">
                    <li><input type="checkbox" id='select_all' value='0'> Select All</li>
                    <?php
                    $userslist = Users::model()->findAll(array('condition' => ''));
                    $assigned_users_array = array();
                    if (!$model->isNewRecord) {
                        $assigned_users = ProjectAssign::model()->findAll(array('condition' => 'projectid=' . $model->pid));
                        foreach ($assigned_users as $auser) {
                            $assigned_users_array[] = $auser['userid'];
                        }
                    }

                    foreach ($userslist as $user) {
                        ?>
                        <li><input type="checkbox" class="checkbox" <?php echo (in_array($user->userid, $assigned_users_array) ? 'checked="checked"' : ''); ?> name="user[]"
                                value='<?php echo $user->userid ?>' />
                            <?php echo $user->first_name . " " . $user->last_name ?>
                        </li>

                        <?php
                    }
                    ?>
                </ul>
            </div>
            <div class="col-lg-4 col-md-6 form-group">
                <label style="display:inline-block;margin-right: 5px;">Expense Head :</label>
                <div style="display:inline-block">
                    <?php echo $form->checkBox($model, 'auto_update'); ?>
                    <label style="display:inline-block;margin-right: 5px;">Auto Update</label>
                </div>
                <ul class="checkboxList">
                    <li><input type="checkbox" id='select_alltype' value='0'>Select All</li>
                    <?php
                    $typelist = ExpenseType::model()->findAll();
                    $assigned_types_array = array();
                    if (!$model->isNewRecord) {
                        $assigned_types = ProjectExpType::model()->findAll(array('condition' => 'project_id=' . $model->pid));
                        foreach ($assigned_types as $atype) {
                            $assigned_types_array[] = $atype['type_id'];
                        }
                    }

                    foreach ($typelist as $type) {
                        ?>
                        <li><input type="checkbox" class="checkboxtype1" name="type[]"
                                value='<?php echo $type->type_id ?>' /> <?php echo $type->type_name ?>
                        </li>
                        <?php
                    }
                    ?>
                </ul>
            </div>
            <div class="col-lg-4 col-md-6 form-group">
                <label>Labour Template :</label>
                <ul class="checkboxList">
                    <?php
                    echo CHtml::hiddenField('Projects[labour_template]', '');
                    $templatelist = LabourTemplate::model()->findAll();
                    if ($pms_api_integration == 1) {
                        $templatelist = LabourTemplate::model()->findAll(array(
                            'condition' => 'pms_labour_template_id != :pms_id',
                            'params' => array(':pms_id' => 0),
                        ));
                    }

                    $assigned_templates = [];
                    if (!$model->isNewRecord) {
                        $assigned_templates = Yii::app()->db->createCommand("SELECT DISTINCT template_id FROM `jp_projects` WHERE project_id=" . $model->pid)->queryColumn();
                    }

                    foreach ($templatelist as $template) {
                        $isChecked = in_array($template->id, $assigned_templates) ? 'checked' : '';
                        echo '<li class="checkboxtype">';
                        echo '<input type="checkbox" class="po_checkbox" name="Projects[labour_template][]" value="' . $template->id . '" ' . $isChecked . '>';
                        echo CHtml::label($template->template_label, 'Projects_labour_template_' . $template->id);
                        echo '</li>';
                    }
                    ?>
                </ul>
                <?php echo $form->error($model, 'labour_template'); ?>
            </div>
            <div class="col-lg-4 col-md-6 form-group d-none" id="selected_labour_templates">
                <label>&nbsp;</label>
                <div id="dialog-modal" class="" style="max-height: 150px;overflow: auto;"></div>
            </div>
            <div class="col-lg-4 col-md-6 form-group">
                <?php echo $form->labelEx($model, 'remarks'); ?>
                <?php echo $form->textArea($model, 'remarks', array('rows' => 3, 'cols' => 50, 'class' => 'form-control')); ?>
                <?php echo $form->error($model, 'remarks'); ?>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-xs-12 text-right">
                <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-info')); ?>
                <?php echo CHtml::ResetButton('Reset', array('class' => 'btn btn-default')); ?>
                <?php echo CHtml::ResetButton('Close', array('onclick' => 'closeaction(this,event);', 'class' => 'btn btn-other')); ?>
            </div>
        </div>
    </div>
    <?php $this->endWidget(); ?>
</div>

<script>
    function sum1() {
        var txtFirstNumberValue = $('#Projects_sqft').val();
        var txtSecondNumberValue = $('#Projects_sqft_rate').val();
        if (txtFirstNumberValue == "")
            txtFirstNumberValue = 0;
        if (txtSecondNumberValue == "")
            txtSecondNumberValue = 0;
        var result = parseInt(txtFirstNumberValue) * parseInt(txtSecondNumberValue);
        if (!isNaN(result)) {
            $('#Projects_bill_amount').val(result);
        }
    }
    $(document).ready(function () {
        $("#select_all").change(function () {
            var status = this.checked;
            $('.checkbox').each(function () {
                this.checked = status;
            });
        });

        $('.checkbox').change(function () {
            if (this.checked == false) {
                $("#select_all")[0].checked = false;
            }

            if ($('.checkbox:checked').length == $('.checkbox').length) {
                $("#select_all")[0].checked = true;
            }
        });


        $("#select_alltype").change(function () {
            var status = this.checked;
            $('.checkboxtype1').each(function () {
                this.checked = status;
            });
        });
        $("#select_allwrktype").change(function () {
            var status = this.checked;
            $('.checkboxwrktype').each(function () {
                this.checked = status;
            });
        });
        $("#select_allwrktypepms").change(function () {
            var status = this.checked;
            $('.checkboxwrktypepms').each(function () {
                this.checked = status;
            });
        });
        function initializeAutoUpdate() {
            if ($("#Projects_auto_update").is(':checked')) {
                $("#select_alltype").addClass('readonly-checkbox');
                // Add the class to make checkboxes appear readonly
                $('.checkboxtype1').each(function () {
                    if ($(this).is(':checked')) {
                        $(this).addClass('readonly-checkbox');
                    }

                });
            }
        }
        initializeAutoUpdate();


        $("#Projects_auto_update").change(function () {
            var isAutoUpdate = this.checked;

            if (isAutoUpdate) {
                // Check #select_alltype and apply behavior
                $("#select_alltype").prop('checked', true);
                $("#select_alltype").addClass('readonly-checkbox');
                $('.checkboxtype1').each(function () {
                    this.checked = true;
                    // Add a class to simulate read-only (but not disabled)
                    $(this).addClass('readonly-checkbox');
                });
            } else {
                $("#select_alltype").removeClass('readonly-checkbox');
                $('.checkboxtype1').each(function () {
                    $(this).removeClass('readonly-checkbox');

                });
            }
        });

        $('.checkboxtype1').change(function () {
            if (this.checked == false) {
                $("#select_alltype")[0].checked = false;
            }
            if ($('.checkboxtype1:checked').length == $('.checkboxtype1').length) {
                $("#select_alltype")[0].checked = true;
            }
        });
        $('.checkboxwrktype').change(function () {
            if (this.checked == false) {
                $("#select_allwrktype")[0].checked = false;
            }

            if ($('.checkboxwrktype:checked').length == $('.checkboxwrktype').length) {
                $("#select_allwrktype")[0].checked = true;
            }
        });
        $('.checkboxwrktypepms').change(function () {
            if (this.checked == false) {
                $("#select_allwrktypepms")[0].checked = false;
            }

            if ($('.checkboxwrktypepms:checked').length == $('.checkboxwrktypepms').length) {
                $("#select_allwrktypepms")[0].checked = true;
            }
        });
        $("#select_alltemplate").change(function () {
            var status = this.checked;
            $('.po_checkbox').each(function () {
                this.checked = status;
            });
            $('.po_checkbox').trigger('change');
        });

    });

    $(document).on("change", "#Projects_project_type", function () {
        if ($("#Projects_project_type").val() == 87) {
            $('.hidbox').show();
        } else {
            $('.hidbox').hide();
        }

    });
    $(document).on("change", "#Projects_project_type", function () {
        if ($("#Projects_project_type").val() == 86) {
            $('.hidbox1').show();
            $('.billam').hide();
        } else {
            $('.hidbox1').hide();
            $('.billam').show();
        }

    });

    $('input.po_checkbox').change(function () {
        var selectedTemplates = [];
        var project = '<?php echo isset($model->pid) ? $model->pid : ''; ?>';
        var type = 2;

        // Gather all checked checkboxes
        $('input.po_checkbox:checked').each(function () {
            selectedTemplates.push($(this).val());
        });

        if (selectedTemplates.length > 0) {
            // Make an AJAX request with the array of selected template IDs
            $.ajax({
                url: '<?php echo Yii::app()->createUrl('projects/getTemplateDetails'); ?>',
                method: 'POST',
                data: {
                    template_ids: selectedTemplates, // Pass the array of template IDs
                    type: type,
                    project: project,
                },
                success: function (response) {
                    // Append the labour details for each template
                    $('#dialog-modal').html(response);
                    $('#selected_labour_templates').show();
                }
            });
        } else {
            // If no checkbox is selected, clear the modal
            $('#dialog-modal').html("");
            $('#selected_labour_templates').hide();
        }
    });

</script>