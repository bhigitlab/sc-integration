<?php

/**
 * This is the model class for table "{{sales_quotation}}".
 *
 * The followings are the available columns in table '{{sales_quotation}}':
 * @property integer $id
 * @property integer $company_id
 * @property integer $client_id
 * @property integer $project_id
 * @property string $date_quotation
 * @property integer $invoice_no
 * @property integer $category_id
 * @property string $category_label
 * @property integer $work_type
 * @property string $shutterwork_description
 * @property string $carcass_description
 * @property string $description
 * @property string $worktype_label
 * @property integer $unit
 * @property double $quantity
 * @property double $quantity_nos
 * @property double $mrp
 * @property double $amount_after_discount
 * @property string $revision_remarks
 * @property integer $status
 * @property integer $created_by
 * @property string $created_date
 * @property string revision_delete_status
 * @property string image 
 * @property string item_id
 */
class SalesQuotation extends CActiveRecord {

    public $image;
    public $length;
    public $width;
    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return '{{sales_quotation}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('category_id,unit, quantity, mrp, amount_after_discount,created_by, created_date', 'required'),
            array('category_id, work_type, unit, status, created_by', 'numerical', 'integerOnly' => true),
            array('quantity, length, width, mrp, amount_after_discount,quantity_nos,item_id', 'numerical'),
            array('category_label, shutterwork_description, carcass_description, description, worktype_label,revision_remarks,hsn_code', 'length', 'max' => 255),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('image', 'length', 'max' => 255),
            
            array('id,category_id, category_label, work_type, shutterwork_description, carcass_description, description, worktype_label, unit,hsn_code, quantity, quantity_nos, mrp, amount_after_discount, status,image, created_by, created_date,revision_remarks,deleted_status,revision_delete_status', 'safe', 'on' => 'search'),
            // Custom validation for length and width
            array('length,width', 'validateDimensions'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'master' => array(self::BELONGS_TO, 'SalesQuotationMaster', 'master_id'),
            'category' => array(self::BELONGS_TO, 'QuotationCategoryMaster', 'category_id'),
            'workType' => array(self::BELONGS_TO, 'QuotationWorktypeMaster', 'work_type'),
            'unit0' => array(self::BELONGS_TO, 'Unit', 'unit'),
            'createdBy' => array(self::BELONGS_TO, 'Users', 'created_by'),
            'item' => array(self::BELONGS_TO, 'QuotationItemMaster', 'item_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'master_id' => 'Master',
            'category_id' => 'Category',
            'item_id' => 'Item',
            'category_label' => 'Category Label',
            'work_type' => 'Work Type',
            'shutterwork_material' => 'Shutterwork Material',
            'shutterwork_finish' => 'Shutterwork Finish',
            'carcass_material' => 'Carcass Material',
            'carcass_finish' => 'Carcass Finish',
            'material' => 'Material',
            'finish' => 'Finish',
            'shutterwork_description' => 'Shutterwork Description',
            'carcass_description' => 'Carcass Description',
            'description' => 'Description',
            'worktype_label' => 'Worktype Label',
            'unit' => 'Unit',
            'length' => 'Length',
            'width' => 'Width',
            'quantity' => 'Quantity',
            'quantity_nos' => 'Quantity Nos',
            'mrp' => 'Mrp',
            'image' => 'Image',
            'amount_after_discount' => 'Amount After Discount',
            'revision_no' => 'Revision No',
            'status' => 'Status',
            'deleted_status' => 'Deleted Status',
            'revision_remarks' => 'Revision Remarks',
            'revision_status' => 'Revision Status',
            'revision_approved' => 'Revision Approved',
            'tax_slab' => 'Tax Slab',
            'cgst_amount' => 'Cgst Amount',
            'cgst_percent' => 'Cgst Percent',
            'sgst_amount' => 'Sgst Amount',
            'sgst_percent' => 'Sgst Percent',
            'igst_amount' => 'Igst Amount',
            'igst_percent' => 'Igst Percent',
            'mainitem_total' => 'Mainitem Total',
            'mainitem_tax' => 'Mainitem Tax',
            'maintotal_withtax' => 'Maintotal Withtax',
            'sub_status' => 'Sub Status',
            'subitem_label' => 'Subitem Label',
            'created_by' => 'Created By',
            'created_date' => 'Created Date',
            'revision_delete_status'=>'Revision delete status',
            'hsn_code' => 'HSN Code',
            'image_gallery_id'=>'Image Gallery',
        );
    }


    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('category_id', $this->category_id);
        $criteria->compare('category_label', $this->category_label, true);
        $criteria->compare('work_type', $this->work_type);
        $criteria->compare('shutterwork_description', $this->shutterwork_description, true);
        $criteria->compare('carcass_description', $this->carcass_description, true);
        $criteria->compare('description', $this->description, true);
        $criteria->compare('worktype_label', $this->worktype_label, true);
        $criteria->compare('unit', $this->unit);
        $criteria->compare('length', $this->length);
        $criteria->compare('width', $this->width);
        $criteria->compare('quantity', $this->quantity);
        $criteria->compare('quantity_nos', $this->quantity_nos);
        $criteria->compare('mrp', $this->mrp);
        $criteria->compare('image', $this->image,true);
        $criteria->compare('amount_after_discount', $this->amount_after_discount);
        $criteria->compare('status', $this->status);
        $criteria->compare('created_by', $this->created_by);
        $criteria->compare('created_date', $this->created_date, true);
        $criteria->compare('revision_delete_status', $this->revision_delete_status, true);
        $criteria->compare('hsn_code',$this->hsn_code,true);
        $criteria->compare('image_gallery_id', $this->image_gallery_id);
        
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return SalesQuotation the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function getMaterialData($sh_description, $css_description, $cat_id, $worktype_id, $worktypelabel) {

        $criteria = new CDbCriteria();
        $criteria->select = "shutter_material_id,shutter_finish_id,carcass_material_id,carcass_finish_id ";
        $criteria->condition = "shutterwork_description='" . $sh_description . "' AND caracoss_description ='" . $css_description . "'"
                . " AND work_type_id='" . $worktype_id . "' AND  worktype_label='" . $worktypelabel . "' AND quotation_category_id='" . $cat_id . "'";
        $material_data = QuotationItemMaster::model()->findAll($criteria);
        return $material_data;
    }

    public function getAmount($quotation_id,$rev_no) {
        $criteria = new CDbCriteria();
        $criteria->select = "maintotal_withtax AS maintotal_withtax ,mainitem_total,revision_remarks,created_date";
        $criteria->condition = "t.master_id='" . $quotation_id . "' AND t.revision_no ='".$rev_no."'";
        $criteria->order = "t.id DESC";
        $criteria->limit = 1; 
        $amount_data = SalesQuotation::model()->findAll($criteria);
        return $amount_data;
    }
    public function validateDimensions($attribute, $params)
{
    $length = $this->length;
    $width = $this->width;

    if ($length == "" && $width != "") {
        $this->addError('length', 'Length is required when width is entered.');
    } elseif ($length != "" && $width == "") {
        $this->addError('width', 'Width is required when length is entered.');
    }
}   
  
}
