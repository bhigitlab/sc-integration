<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<link rel="stylesheet" type="text/css" href="uploads/new_template/css/newstyle.css" />

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  
  <script>
  $( function() {
       $( "#datepicker" ).datepicker({dateFormat: 'dd-mm-yy'});
    
    
  } );
  </script>

<br/> 

<div class="invoicemaindiv" >


                <a style="cursor:pointer; float:right;" id="download" class="save_btn2 pdf_excel" >
                 <div class="save_pdf"></div>SAVE AS PDF
                 </a>
                 
                 <a style="cursor:pointer; float:right;margin-right:2px;" id="download" class="excelbtn2 pdf_excel" >
                 <div class="save_pdf"></div>SAVE AS EXCEL
                 </a><br/><br/><br>
                
 
 
 <?php
 $id = $model['id'];
 ?>
 <form id="pdfvals2" method="post" action="<?php echo $this->createAbsoluteUrl('invoice/updateperforma&pid='.$id)?>">
	
	<input type="hidden" id="form_perf_id" value ="<?php echo $model['id'];?>" />
	<div class="invoiceheader">
	  <img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/logo.png" style="height:100px;padding:5px;">
	  <textarea class="txtBox pastweek" name="details" readOnly="true" placeholder="Please click to edit" style="float:right;width:50%;height:110px"/>
	  3rd Floor,Dr.Rajkrishnan's Building &#13;
	  Nort Fort Gate,Tripunithura,Kochi &#13;&#10;
	  Call:0484 4030077, 9995107007 &#13;
	  Mail:info@jpventures.in
	  </textarea>
	</div>
	<h2>PERFORMA INVOICE</h2>
	
	<div class="inv_client_det">
	<label>BILL TO : </label>
	<input type="text" class="txtBox pastweek" readOnly="true" name="billto" value ="<?php echo $model['client_name'];?>" />
	</div>
	
	<div class="rightdiv">
	<table border="1" style="width:100%;">
	<tr>
		<td>
		<label>DATE : </label>
		<input type="text" id="datepicker" class="txtBox pastweek" readOnly="true" name="date"  value ="<?php echo date("d-m-Y", strtotime($model->date)); ?>" >
		</td>
		</tr>
	<tr>
		<td>
		<label>INVOICE NO : </label>
		<input type="text" class="txtBox pastweek" readOnly="true" name="invoiceno" value ="<?php echo $model['inv_no'];?>"/>
		</td>
	</tr>
	</table>
	</div>
	<br><br><br><br>
	
	<div class="inv_client_det">
	<label>PROJECT NAME : </label>
	<input type="text" class="txtBox pastweek" readOnly="true" name="project" value ="<?php echo $model['project_name'];?>" />
	</div>
	<br>
	<br>
	
	<table border="1" class="table">
			<thead>
				<tr>
					
					<th>Sl.No</th>
					<th>Description</th>
					<th>Quantity</th>
					<th>Unit</th>
					<th>Rate</th>
					<th>Amount</th>
				</tr>
			</thead>
			<tbody class="addrow">
				<?php
				$i=1;
				foreach($newmodel as $new) 
				{
				?>
				<tr>
					<input type="hidden" class="txtBox pastweek" readOnly="true" name="ids[]" value="<?php echo $new['id'];?>" />
					<td><input type="text" class="txtBox pastweek" readOnly="true" name="sl_No[]" value="<?php echo $i;?>" /></td>
					<td><textarea class="txtBox pastweek" readOnly="true" name="description[]" /><?php echo $new['description'];?></textarea></td>
					<td><input type="text" class="txtBox pastweek" readOnly="true" name="quantity[]"  value="<?php echo $new['quantity'];?>" /></td>
					<td><input type="text" class="txtBox pastweek" readOnly="true" name="unit[]"  value="<?php echo $new['unit'];?>"/></td>
					<td><input type="text" class="txtBox pastweek" readOnly="true" name="rate[]"  value="<?php echo $new['rate'];?>"/></td>
					<td><input type="text" class="txtBox pastweek amt" readOnly="true" name="amount[]"  value="<?php echo $new['amount'];?>"/></td>
				</tr>
				
				<?php
				$i++;
				}
				?>
				
				
			</tbody>
	</table>
	
<!--	<div class="addcolumn">+</div>-->
	
	<table border="1" style="width:100%;">
		<tr>
			
			<td>SUB TOTAL :<input type="text" class="txtBox pastweek" readOnly="true" name="subtot" value="<?php echo $model['subtotal'];?>" /></td>
			<td>FEES :<input type="text" class="txtBox pastweek" readOnly="true" name="fees" value="<?php echo $model['fees'];?>"/></td>
			<td>GRAND TOTAL :<input type="text" class="txtBox pastweek grand" readOnly="true" name="grand" value="<?php echo $model['amount'];?>"/></td>
		</tr>
	</table>
	<br><br>
<!--<center><input type="submit" name="performa" value="Submit" class="form_submit2"></center>-->
        
              
</form>
</div>
<script>

	
	$(document).ready(function() {
	
	$(document).on('focus', '.table input[name="amount[]"]', function(e){
	var v1 = '';
	var v2 = '';
	var v = '';
	
				$(this).closest('tr').find("input[name='rate[]']").each(function() {
				var va1 = this.value;
				v1 +=  va1;
				//alert(v1);
				});
				
				$(this).closest('tr').find("input[name='quantity[]']").each(function() {
				var va2 = this.value;
				v2 +=  va2;
				//alert(v2);
				
				});
			
			if (!(v1 == '' && v2 == '')) {
			//alert('hi');
			
			var v = v1*v2;
		    $(this).closest('tr').find("input[name='amount[]']").val(v); 
			}
			else
			{
			//alert('hi');
			$(this).closest('tr').find("input[name='amount[]']").val(''); 
			}
			
	});
	
	
	$(document).on('focus', '.table input[name="amount[]"]', function(e){
		//alert('hi');
		var val = 0;
		$('.table input[name="amount[]"]').each(function() {
		val += Number($(this).val());
		});
		$('input[name="grand"]').val(val);
		});
		
	});
	
	
	
	$(document).on('change', 'input[name="invoiceno"]', function(){
		
		var inv_no = $(this).val();
		
			$.ajax({
				
				type: "POST",
				url: "<?php echo $this->createUrl('invoice/checkPerformano') ?>",
				data: {"inv_no": inv_no}, 
				dataType: "json",
				success:function(response){
					
					if(response.response == "fail"){
						
					alert("Invoice Number is already in use!");
					
					
					}
			
				
				}
        
	});
		
		
	});
	
	
	
	
	
	
	


    $(function () {

        $('.save_btn1').click(function () {
			
			$("form#pdfvals1").attr('action', "<?php echo $this->createAbsoluteUrl('invoice/saveperformainvoice'); ?>"); 
            $("form#pdfvals1").submit();

        });
        
         $('.excelbtn1').click(function () {
			
			$("form#pdfvals1").attr('action', "<?php echo $this->createAbsoluteUrl('invoice/exportPerforma'); ?>"); 
            $("form#pdfvals1").submit();

        });
        
         $('.form_submit1').click(function () {
			
			$("form#pdfvals1").attr('action', "<?php echo $this->createAbsoluteUrl('invoice/addperforma'); ?>"); 
            $("form#pdfvals1").submit();

        });
        
        
         $('.save_btn2').click(function () {
			
			$("form#pdfvals2").attr('action', "<?php echo $this->createAbsoluteUrl('invoice/saveperformainvoice'); ?>"); 
            $("form#pdfvals2").submit();

        });
        
         $('.excelbtn2').click(function () {
			
			$("form#pdfvals2").attr('action', "<?php echo $this->createAbsoluteUrl('invoice/exportPerforma'); ?>"); 
            $("form#pdfvals2").submit();

        });
        
         $('.form_submit2').click(function () {
			$("form#pdfvals2").attr('action', "<?php echo $this->createAbsoluteUrl('invoice/updateperforma&pid='.$id); ?>"); 
            $("form#pdfvals2").submit();

        });
        
          
        
       
        
       
        
        $('.addcolumn').on('click', function () {
	
			$('.addrow').append('<tr><input type="hidden" class="txtBox pastweek" name="ids[]" /><td><input type="text" class="txtBox pastweek" name="sl_No[]" placeholder="Please click to edit"/></td><td><textarea class="txtBox pastweek" name="description[]"/></textarea></td><td><input type="text" class="txtBox pastweek" name="quantity[]" placeholder="Please click to edit"/></td><td><input type="text" class="txtBox pastweek" name="unit[]" placeholder="Please click to edit"/></td><td><input type="text" class="txtBox pastweek" name="rate[]" placeholder="Please click to edit"/></td><td><input type="text" class="txtBox pastweek amt" name="amount[]" placeholder="Please click to edit"/></td></tr>');
			
		
		 });
		 
		
        
       


        $(".inputSwitch span").on("click", function() {

            var $this = $(this);

            $this.hide().siblings("input").val($this.text()).show();

        });

        $(".inputSwitch input").bind('blur', function() {

                   var $this = $(this);

                   $(this).attr('value', $(this).val());

                   $this.hide().siblings("span").text($this.val()).show();

        }).hide();

 

    });
</script>

<style>
	a.pdf_excel {
    background-color: #6a8ec7;
    display:inline-block;
	padding:8px;
    color: #fff;
    border: 1px solid #6a8ec8;
}
</style>
