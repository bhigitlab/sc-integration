<?php
/* @var $this VendorsController */
/* @var $model Vendors */
/* @var $form CActiveForm */
?>

<div class="page_filter clearfix custom-form-style">

	<?php $form = $this->beginWidget('CActiveForm', array(
		'action' => Yii::app()->createUrl($this->route),
		'method' => 'get',
	)); ?>
	<div class="row">
		<div class=" form-group col-xs-12 col-sm-3 ">
			<label>From</label>
			<input type="text" name="Dailyvendors[fromdate]" id="Dailyvendors_fromdate" class="form-control"
				autocomplete="off" readonly=true placeholder="From Date"
				value="<?php echo ($model->fromdate) ? date("d-m-Y", strtotime($model->fromdate)) : ""; ?>" />
		</div>
		<div class=" form-group col-xs-12 col-sm-3">
			<label>To</label>
			<input type="text" name="Dailyvendors[todate]" id="Dailyvendors_todate" class="form-control"
				autocomplete="off" readonly=true placeholder="To Date"
				value="<?php echo ($model->todate) ? date("Y-m-d", strtotime($model->todate)) : ""; ?>" />
		</div>
		<div class=" form-group col-xs-12 col-sm-3 text-sm-left text-right">
			<label>&nbsp;</label>
			<div >
				<button class="btn btn-info btn-sm" type="submit" name="submitForm" id="submitForm">Go</button>
				<button class="btn btn-default btn-sm" type="button" name="clearForm" id="clearForm">Clear</button>
			</div>
		</div>
	</div>
	<?php $this->endWidget(); ?>

</div><!-- search-form -->