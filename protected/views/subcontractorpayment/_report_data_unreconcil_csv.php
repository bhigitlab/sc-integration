<tbody>
    <?php
    foreach ($final_array  as $key => $quotation_list) {
        foreach ($quotation_list['details'] as $key2 => $details) {
            foreach ($details as $key3 => $detail) {
                if (isset($detail['scquotation_decription'])) {
    ?>
                    <tr>
                        <td><?php echo  $quotation_list['project']; ?></td>
                        <td><?php echo Yii::app()->Controller->changeDateForamt($key2) ?></td>
                        <td><?php echo $detail['scquotation_decription'] ?></td>
                        <td><?php echo $detail['scquotation_no'] ?></td>
                        <td><?php echo Yii::app()->Controller->money_format_inr($detail['amount'], 2, 1) ?></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td> </td>
                    </tr>
                    <?php
                    $payment_details = SubcontractorPayment::model()->findAllByAttributes(array('subcontractor_id' => $detail['subcontractor_id'], 'quotation_number' => $detail['scquotation_id'], 'project_id' => $detail['project_id']));
                    foreach ($payment_details as $payment) {
                        $bill = "Nil";
                        if(isset($payment['sc_bill_id'])){
                            $stagebillmodel = Subcontractorbill::model()->findByPk($payment['sc_bill_id']);
                            $bill =$stagebillmodel['bill_number'];

                        }
                                                               $stage = "Nil";
                        if(isset($payment['stage_id'])){
                            $stagemodel = ScPaymentStage::model()->findByPk($payment['stage_id']);
                            $stage = $stagemodel['payment_stage'];

                        }
                    ?>
                        <tr>
                            <td><?php echo  $quotation_list['project']; ?></td>
                            <td><?php echo Yii::app()->Controller->changeDateForamt($key2) ?></td>
                            <td><?php echo $detail['scquotation_decription'] ?></td>
                            <td><?php echo $detail['scquotation_no'] ?></td>
                            <td><?php echo Yii::app()->Controller->money_format_inr($detail['amount'], 2, 1) ?></td>
                            <td> <?php echo Yii::app()->Controller->changeDateForamt($payment['date']) ?></td>
                            <td><?php echo $bill ?></td>
                            <td><?php echo $stage ?></td>
                           
                            <td><?php echo $payment['description'] ?></td>
                            <td><?php echo Yii::app()->Controller->money_format_inr($payment['paidamount'], 2, 1) ?></td>
                            <td> </td>
                        </tr>
    <?php
                    }
                }
            }
        }
        // echo '<pre>';
        // print_r($quotation_list);
    }

    // exit;
    ?>
    <tr>
        <?php
        for ($i = 1; $i <= 9; $i++) {
            echo "<td>&nbsp;</td>";
        }
        ?>
    </tr>
    <tr>
        <td>Payment Without Quotation</td>
    </tr>
    <tr>
        <?php
        for ($i = 1; $i <= 9; $i++) {
            echo "<td>&nbsp;</td>";
        }
        ?>
    </tr>
    <?php
    $advance_total = 0;
    $payment_total = 0;
    $balance_total = 0;

    foreach ($final_array2 as $key => $value) {
        $j = 0;
        $a = 0;
        $blance = 0;
        $expense_sum = 0;
        $adv_sum = 0;
        if (!empty($value['details'])) {
            foreach ($value['details'] as $key2 => $value2) {
                if (isset($value2['advance_amount'])) {
                    $advance_amount = $value2['advance_amount'];
                    $adv_sum += $advance_amount;
                } else {
                    $advance_amount = 0;
                }
                if (isset($value2['payment_amount'])) {
                    $payment_amount = $value2['payment_amount'];
                    $expense_sum += $payment_amount;
                } else {
                    $payment_amount = 0;
                }

                $blance += $advance_amount - $payment_amount;
                if ($a >= 5) {
                    $blance = $blance + $advance_amount;
                } else {
                    $blance = $blance;
                }
                $advance_total += $advance_amount;
                $payment_total += $payment_amount;
                $balance_total += $blance;
                $final_array2[$key]['expense_amount'] = $expense_sum;
                $final_array2[$key]['adv_amount'] = $adv_sum;
            }
        }
    }
    $balance_sum = $advance_total - $payment_total;
    ?>
    <tr>
        <td><b>Project Name</b></td>
        <td><b>Date</b></td>
        <td><b>Description</b></td>
        <td><b>Amount</b></td>
        <td><b>Date</b></td>
        <td><b>Description</b></td>
        <td><b>Amount</b></td>
        <td><b>Balance</b></td>
    </tr>
    <tr class="grandtotal">
        <td>Total</td>
        <td></td>
        <td></td>
        <td>
            <b><?php echo ($payment_total != '') ? Controller::money_format_inr($payment_total, 2, 1) : '0.00'; ?></b>
        </td>
        <td></td>
        <td></td>
        <td>
            <b><?php echo ($advance_total != '') ? Controller::money_format_inr($advance_total, 2, 1) : '0.00'; ?></b>
        </td>
        <td>
            <?php echo ($balance_sum != '') ?  ($balance_sum > 0 ? '<b>' . $balance_sum . '</b>' : '<b class="red_bg">' . $balance_sum . '</b>') : '0.00'; ?>
        </td>
    </tr>
    <?php
    $advance_total = 0;
    $payment_total = 0;
    $balance_total = 0;
    foreach ($final_array2 as $key => $value) {
        $balance_amount = $value['adv_amount'] - $value['expense_amount'];
        if (!empty($value['details'])) {
            foreach ($value['details'] as $key2 => $value2) {
                if (isset($value2['advance_amount'])) {
                    $advance_amount = $value2['advance_amount'];
                } else {
                    $advance_amount = 0;
                }
                if (isset($value2['payment_amount'])) {
                    $payment_amount = $value2['payment_amount'];
                } else {
                    $payment_amount = 0;
                }

                $blance += $advance_amount - $payment_amount;
                if ($a >= 5) {
                    $blance = $blance + $advance_amount;
                } else {
                    $blance = $blance;
                }
                $advance_total += $advance_amount;
                $payment_total += $payment_amount;
                $balance_total += $blance;
    ?>
                <tr>
                    <td><?php echo $value['project']; ?></td>
                    <td><?php echo isset($value2['payment_amount']) ?  Yii::app()->Controller->changeDateForamt($key2)  : ""; ?></td>
                    <td><?php echo isset($value2['payment_description']) ? $value2['payment_description'] : ""; ?></td>
                    <td><?php echo isset($value2['payment_amount']) ? $value2['payment_amount'] : ""; ?></td>
                    <td><?php echo isset($value2['advance_amount']) ?  Yii::app()->Controller->changeDateForamt($key2) : ""; ?></td>
                    <td><?php echo isset($value2['description']) ? $value2['description'] : ""; ?></td>
                    <td><?php echo isset($value2['advance_amount']) ? $value2['advance_amount'] : ""; ?></td>
                </tr>
    <?php
            }
        }
    }
    ?>
    <tr>
        <?php
        for ($i = 1; $i <= 9; $i++) {
            echo "<td>&nbsp;</td>";
        }
        ?>
    </tr>
    <tr>
        <td>Payment With Quotation(Labour Entry)</td>
    </tr>
    <tr>
        <?php
        for ($i = 1; $i <= 9; $i++) {
            echo "<td>&nbsp;</td>";
        }
        ?>
    </tr>
    <?php
    $advance_total = 0;
    $payment_total = 0;
    $balance_total = 0;

    foreach ($final_array3 as $key => $value) {
        $j = 0;
        $a = 0;
        $blance = 0;
        $expense_sum = 0;
        $adv_sum = 0;
        if (!empty($value['details'])) {
            foreach ($value['details'] as $key2 => $value2) {
                if (isset($value2['advance_amount'])) {
                    $advance_amount = $value2['advance_amount'];
                    $adv_sum += $advance_amount;
                } else {
                    $advance_amount = 0;
                }
                if (isset($value2['payment_amount'])) {
                    $payment_amount = $value2['payment_amount'];
                    $expense_sum += $payment_amount;
                } else {
                    $payment_amount = 0;
                }

                $blance += $advance_amount - $payment_amount;
                if ($a >= 5) {
                    $blance = $blance + $advance_amount;
                } else {
                    $blance = $blance;
                }
                $advance_total += $advance_amount;
                $payment_total += $payment_amount;
                $balance_total += $blance;
                $final_array3[$key]['expense_amount'] = $expense_sum;
                $final_array3[$key]['adv_amount'] = $adv_sum;
            }
        }
    }
    $balance_sum = $advance_total - $payment_total;
    ?>
    <tr>
        <td><b>Project Name</b></td>
        <td><b>Date</b></td>
        <td><b>Quotation No</b></td>
        <td><b>Description</b></td>
        <td><b>Amount</b></td>
        <td><b>Date</b></td>
        <td><b>Description</b></td>
        <td><b>Amount</b></td>
        <td><b>Balance</b></td>
    </tr>
    <tr class="grandtotal">
        <td>Total</td>
        <td></td>
        <td></td>
        <td></td>
        <td>
            <b><?php echo ($payment_total != '') ? Controller::money_format_inr($payment_total, 2, 1) : '0.00'; ?></b>
        </td>
        <td></td>
        <td></td>
        <td>
            <b><?php echo ($advance_total != '') ? Controller::money_format_inr($advance_total, 2, 1) : '0.00'; ?></b>
        </td>
        <td>
            <?php echo ($balance_sum != '') ?  ($balance_sum > 0 ? '<b>' . $balance_sum . '</b>' : '<b class="red_bg">' . $balance_sum . '</b>') : '0.00'; ?>
        </td>
    </tr>
    <?php
    $advance_total = 0;
    $payment_total = 0;
    $balance_total = 0;
    foreach ($final_array3 as $key => $value) {
        if (isset($value['adv_amount']) && isset($value['expense_amount'])) {
            $balance_amount = $value['adv_amount'] - $value['expense_amount'];
        } else {
            // Handle the case where 'adv_amount' or 'expense_amount' is not set
            $balance_amount = 0; // or another default value
        }
        
        if (!empty($value['details'])) {
            foreach ($value['details'] as $key2 => $value2) {
                if (isset($value2['advance_amount'])) {
                    $advance_amount = $value2['advance_amount'];
                } else {
                    $advance_amount = 0;
                }
                if (isset($value2['payment_amount'])) {
                    $payment_amount = $value2['payment_amount'];
                } else {
                    $payment_amount = 0;
                }

                $blance += $advance_amount - $payment_amount;
                if ($a >= 5) {
                    $blance = $blance + $advance_amount;
                } else {
                    $blance = $blance;
                }
                $advance_total += $advance_amount;
                $payment_total += $payment_amount;
                $balance_total += $blance;
    ?>
                <tr>
                    <td><?php echo $value['project']; ?></td>
                    <td><?php echo isset($value2['payment_amount']) ?  Yii::app()->Controller->changeDateForamt($key2)  : ""; ?></td>
                     <td><?php echo isset($value2['sc_quot_id']) ?  $value2['sc_quot_id']  : ""; ?></td>
                    <td><?php echo isset($value2['payment_description']) ? $value2['payment_description'] : ""; ?></td>
                    <td><?php echo isset($value2['payment_amount']) ? $value2['payment_amount'] : ""; ?></td>
                    <td><?php echo isset($value2['advance_amount']) ?  Yii::app()->Controller->changeDateForamt($key2) : ""; ?></td>
                    <td><?php echo isset($value2['description']) ? $value2['description'] : ""; ?></td>
                    <td><?php echo isset($value2['advance_amount']) ? $value2['advance_amount'] : ""; ?></td>
                </tr>
    <?php
            }
        }
    }
    ?>


</tbody>