<?php

/**
 * This is the model class for table "{{editrequest}}".
 *
 * The followings are the available columns in table '{{editrequest}}':
 * @property integer $editrequest_id
 * @property string $editrequest_data
 * @property string $editrequest_table
 * @property integer $editrequest_headstatus
 * @property integer $editrequest_headtype
 * @property string $editrequest_payment
 * @property integer $parent_id
 * @property integer $user_id
 * @property string $editrequest_date
 * @property integer $editrequest_status
 */
class Editrequest extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{editrequest}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('editrequest_data, editrequest_table, editrequest_payment, parent_id, user_id, editrequest_date, editrequest_status', 'required'),
			array('editrequest_headstatus, editrequest_headtype, parent_id, user_id, editrequest_status', 'numerical', 'integerOnly'=>true),
			array('editrequest_data', 'length', 'max'=>10000),
			array('editrequest_table, editrequest_payment', 'length', 'max'=>100),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('editrequest_id, editrequest_data, editrequest_table, editrequest_headstatus, editrequest_headtype, editrequest_payment, parent_id, user_id, editrequest_date, editrequest_status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'editrequest_id' => 'Editrequest',
			'editrequest_data' => 'Editrequest Data',
			'editrequest_table' => 'Editrequest Table',
			'editrequest_headstatus' => 'Editrequest Headstatus',
			'editrequest_headtype' => 'Editrequest Headtype',
			'editrequest_payment' => 'Editrequest Payment',
			'parent_id' => 'Parent',
			'user_id' => 'User',
			'editrequest_date' => 'Editrequest Date',
			'editrequest_status' => 'Editrequest Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('editrequest_id',$this->editrequest_id);
		$criteria->compare('editrequest_data',$this->editrequest_data,true);
		$criteria->compare('editrequest_table',$this->editrequest_table,true);
		$criteria->compare('editrequest_headstatus',$this->editrequest_headstatus);
		$criteria->compare('editrequest_headtype',$this->editrequest_headtype);
		$criteria->compare('editrequest_payment',$this->editrequest_payment,true);
		$criteria->compare('parent_id',$this->parent_id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('editrequest_date',$this->editrequest_date,true);
		$criteria->compare('editrequest_status',$this->editrequest_status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>false,    
		));
	}
        public function pendingsearch()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('editrequest_id',$this->editrequest_id);
		$criteria->compare('editrequest_data',$this->editrequest_data,true);
		$criteria->compare('editrequest_table',$this->editrequest_table,true);
		$criteria->compare('editrequest_headstatus',$this->editrequest_headstatus);
		$criteria->compare('editrequest_headtype',$this->editrequest_headtype);
		$criteria->compare('editrequest_payment',$this->editrequest_payment,true);
		$criteria->compare('parent_id',$this->parent_id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('editrequest_date',$this->editrequest_date,true);
		$criteria->compare('editrequest_status',0);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array('pageSize'=>20),
		));
	}
        public function approvedsearch()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('editrequest_id',$this->editrequest_id);
		$criteria->compare('editrequest_data',$this->editrequest_data,true);
		$criteria->compare('editrequest_table',$this->editrequest_table,true);
		$criteria->compare('editrequest_headstatus',$this->editrequest_headstatus);
		$criteria->compare('editrequest_headtype',$this->editrequest_headtype);
		$criteria->compare('editrequest_payment',$this->editrequest_payment,true);
		$criteria->compare('parent_id',$this->parent_id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('editrequest_date',$this->editrequest_date,true);
		$criteria->compare('editrequest_status',1);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>false,
		));
	}
        public function cancelledsearch()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('editrequest_id',$this->editrequest_id);
		$criteria->compare('editrequest_data',$this->editrequest_data,true);
		$criteria->compare('editrequest_table',$this->editrequest_table,true);
		$criteria->compare('editrequest_headstatus',$this->editrequest_headstatus);
		$criteria->compare('editrequest_headtype',$this->editrequest_headtype);
		$criteria->compare('editrequest_payment',$this->editrequest_payment,true);
		$criteria->compare('parent_id',$this->parent_id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('editrequest_date',$this->editrequest_date,true);
		$criteria->compare('editrequest_status',2);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>false,
		));
	}
        public function actionsearch()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('editrequest_id',$this->editrequest_id);
		$criteria->compare('editrequest_data',$this->editrequest_data,true);
		$criteria->compare('editrequest_table',$this->editrequest_table,true);
		$criteria->compare('editrequest_headstatus',$this->editrequest_headstatus);
		$criteria->compare('editrequest_headtype',$this->editrequest_headtype);
		$criteria->compare('editrequest_payment',$this->editrequest_payment,true);
		$criteria->compare('parent_id',$this->parent_id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('editrequest_date',$this->editrequest_date,true);
		//$criteria->compare('editrequest_status',2);
                $criteria->condition = "editrequest_status != 0";

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>false,
		));
	}


	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Editrequest the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
