<?php
/* @var $this PaymentRemindersController */
/* @var $model PaymentReminders */

$this->breadcrumbs=array(
	'Payment Reminders'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List PaymentReminders', 'url'=>array('index')),
	array('label'=>'Create PaymentReminders', 'url'=>array('create')),
	array('label'=>'View PaymentReminders', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage PaymentReminders', 'url'=>array('admin')),
);
?>

<div class="panel panel-gray">
    <div class="panel-heading form-head">
        <h3 class="panel-title">Edit Payment Reminders</h3>
    </div>
    <?php echo $this->renderPartial('_form', array('model' => $model)); ?>
</div>