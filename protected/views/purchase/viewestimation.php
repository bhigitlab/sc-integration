<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$this->breadcrumbs=array(
	'Estimation List',)
?>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/tableHeadFixer.js"></script>
<div class="container" id="project">
<div class="clearfix">
    <h2 class="pull-left"><?php echo $projects->name; ?></h2>
    <div class="add-btn pull-right">
        <a href="index.php?r=purchase/itemestimation" class="button btn btn-primary">Back</a>
        
        <!-- <a href="index.php?r=purchase/glassestimation" class="button" style="margin-right: 10px;">Add Estimation by Width x Height</a>
        <a href="index.php?r=purchase/estimationbylength" class="button" style="margin-right: 10px;">Add  Estimation by Length</a> -->
    </div>
</div>

<?php 
//$this->renderPartial('_newsearch', array('model' => $model, 'project' => $project, 'vendor' => $vendor, 'purchase_no' => $purchase_no,'expense' => $expense)) ?>

<?php if(Yii::app()->user->hasFlash('success')):?>
<div class="info" style="width: 500px;margin-left: 400px;padding: 8px;color: green;font-size: 20px;font-weight: bold; padding-top: 46px;">
    <?php echo Yii::app()->user->getFlash('success'); ?>
</div>
<?php endif; ?>


<?php if(Yii::app()->user->hasFlash('error')):?>
<div class="info" style="width: 500px;margin-left: 400px;padding: 8px;color: red;font-size: 20px;font-weight: bold; padding-top: 46px;">
<?php echo Yii::app()->user->getFlash('error'); ?>
</div>
<?php endif; ?>
   

<div  style="">
    <div id="addremark" class="panel panel-gray" style="display:none;position: fixed;top: 15%; bottom: 25%;right: 0px; z-index: 6;width:500px; margin:0 auto;background-color: #fff;">
        <div class="panel-heading form-head">
            <h3 class="panel-title">Remark For Purchase No: <span class="po_no"></span></h3>
            <button type="button" onclick="closeaction(this)" class="close" data-dismiss="alert" aria-label="Close" ><span aria-hidden="true">×</span></button>
        </div>
        <div class="panel-body">
            <div class="clearfix">
                <a class="addnow" style="display:none;">Add Remark</a>
                <div class="remarkadd">
                    
                    <div class="form-group">
                        <label>Add Remark</label>
                        <textarea type="text" name="remark" id="remark" class="form-control"></textarea>
                    </div> 
                    <div class="text-right">
                        <input type="submit" value="Save" id="remarkSubmit" class="btn btn-sm btn-info"/>
                        <input type="button" value="Close" class="closermrk btn  btn-sm btn-default" style="display:none;"/>
                    </div>
                </div>
                <input type="hidden" name="txtPurchaseId" id="txtPurchaseId" value=""/>
                <h4 class="head_remarks">Remarks</h4>
                <div class="all_remarks">
                    <div id="remarkList">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="msg_box"></div>
    <div id="estimationlist">
    <?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	//'itemView'=>'_view',
	'itemView' => '_newitemestimation', 'template' => '<div>{summary}{sorter}</div><div class="tblwrap scrollTable"><div class="container1"><table cellpadding="10" class="table  list-view sorter">{items}</table></div></div>{pager}',
	'ajaxUpdate' => false,
    )); ?><br><br>

<div class="container1">
    <h4><b>Labour Estimation</b></h4>
    <table cellpadding="10" id="laborTypetbl" class="table">
        <?php if ($dataProviderLabour->getItemCount() > 0) : ?>
            <thead>
                <tr>
                    <th>SI No</th>
                    
                    <th>Labour Type</th>
                    <th>Remarks</th>
                    <th>Quantity</th>
                    <th>Unit</th>
                    <th>Rate</th>
                    <th>Amount</th>
                   
                </tr>
            </thead>
            <tbody>
                <?php foreach ($dataProviderLabour->getData() as $index => $data): ?>
                    <tr>
                        <td style="width: 50px;"><?php echo $index + 1; ?></td>
                       
                        <td>
                            <?php
                            $pms_api_integration_model=ApiSettings::model()->findByPk(1);
                            $pms_api_integration =$pms_api_integration_model->api_integration_settings;
                            if($pms_api_integration==0){
                                $labourModel = LabourWorktype::model()->findByPk($data->labour);
                                echo !empty($labourModel) ? CHtml::encode($labourModel->worktype) : "";
                            }
                            else{
                                $labourModel = ApiLabours::model()->findByAttributes(array('labour_id' => $data->labour));
                                
                                echo !empty($labourModel) ? CHtml::encode($labourModel->labour_name) : "";
                            }

                            
                            ?>
                        </td>
                        <td>
                            <?php echo CHtml::encode($data->remarks); ?>
                        </td>
                        <td>
                            <?php echo CHtml::encode($data->quantity); ?>
                        </td>
                        <td>
                            <?php 
                            $unitModel = Unit::model()->findByPk($data->unit);
                            echo !empty($unitModel) ? CHtml::encode($unitModel->unit_name) : "Unit not found"; 
                            ?>
                        </td>
                        <td>
                            <?php echo CHtml::encode($data->rate); ?>
                        </td>
                        <td>
                            <?php echo CHtml::encode($data->amount); ?>
                        </td>
                    
                    </tr>
                <?php endforeach; ?>
            </tbody>
            <tfoot>
                <!-- <tr style="background-color: #f2f2f2;">
                    <td colspan="7" style="text-align: right;"><strong>Total:</strong></td>
                    <td colspan="1"><strong></strong></td>
                    <td></td>
                </tr> -->
            </tfoot>
        <?php else : ?>
            <tr>
                <td colspan="9" style="text-align: center;">No labour data found.</td>
            </tr>
        <?php endif; ?>
    </table>
</div><br><br>

<div class="container1">
    <h4><b>Equipment Estimation</b></h4>
    <table cellpadding="10" id="equipmentTypetbl" class="table">
        <?php if ($dataProviderEquipment->getItemCount() > 0) : ?>
            <thead>
                <tr>
                    <th>SI No</th>
                    
                    <th>Equipment Type</th>
                    <th>Remarks</th>
                    <th>Quantity</th>
                    <th>Unit</th>
                    <th>Rate</th>
                    <th>Amount</th>
                    
                </tr>
            </thead>
            <tbody>
                <?php foreach ($dataProviderEquipment->getData() as $index => $data): ?>
                    <tr>
                        <td style="width: 50px;"><?php echo $index + 1; ?></td>
                       
                        <td>
                            <?php
                             $pms_api_integration_model=ApiSettings::model()->findByPk(1);
                             $pms_api_integration =$pms_api_integration_model->api_integration_settings;
                             if($pms_api_integration==0){
                                $equipmentModel = Equipments::model()->findByPk($data->equipment);
                                echo !empty($equipmentModel) ? CHtml::encode($equipmentModel->equipment_name) : "";
                             }
                             else{
                                 $equipmentModel = ApiEquipments::model()->findByAttributes(array('equipment_id' => $data->equipment));
                                     echo !empty($equipmentModel) ? CHtml::encode($equipmentModel->equipment_name) : "";
                             }

                            
                            ?>
                        </td>
                        <td>
                            <?php echo CHtml::encode($data->remarks); ?>
                        </td>
                        <td>
                            <?php echo CHtml::encode($data->quantity); ?>
                        </td>
                        <td>
                            <?php 
                            $unitModel = Unit::model()->findByPk($data->unit);
                            echo !empty($unitModel) ? CHtml::encode($unitModel->unit_name) : "Unit not found"; 
                            ?>
                        </td>
                        <td>
                            <?php echo CHtml::encode($data->rate); ?>
                        </td>
                        <td>
                            <?php echo CHtml::encode($data->amount); ?>
                        </td>
                        
                    </tr>
                <?php endforeach; ?>
            </tbody>
            <tfoot>
                <!-- <tr style="background-color: #f2f2f2;">
                    <td colspan="7" style="text-align: right;"><strong>Total:</strong></td>
                    <td colspan="1"><strong></strong></td>
                    <td></td>
                </tr> -->
            </tfoot>
        <?php else : ?>
            <tr>
                <td colspan="9" style="text-align: center;">No equipment data found.</td>
            </tr>
        <?php endif; ?>
    </table>
</div>

<style>
    #fixtable thead th ,#fixtable tfoot th{background-color: #eee; z-index: 1;}
    
a.button {
    background-color: #6a8ec7;
    display: block;
    padding: 5px;
    color: #fff;
    cursor: pointer;
    float: right;
    border: 1px solid #6a8ec8;
}
.bill_class {
	background: #FF6347 !important;
}
.exp-list{padding: 0px;}
.page-body h3{
        margin:4px 0px;
        color:inherit;
        text-align: left;
    }
    tfoot th{background-color: #eee;}
.panel{border:1px solid #ddd;box-shadow: 0px 0px 12px 1px rgba(0,0,0,0.25);}
.panel-heading{background-color: #6A8EC7;height: 40px;color: #fff;}    
.panel label{display:block;}

.head_remarks{
    padding-left: 5px;
}  
    
.ind_remarks{
    padding: 5px;    
    font-size: 13px;
}
.ind_remarks p:first-child{font-size: 11px;}
.ind_remarks p:last-child {
background: #f6f6f6;
padding: 5px;
margin-top: -5px;
}
.pur_remark{
    font-weight:600;
    vertical-align:middle !important;
    white-space: nowrap;
    position: relative; 
}
.cursor-pointer{cursor: pointer;}
.pur_remark .icon{}
.form-head {
    position:relative;
}
.close{
    position: absolute;
    top: 7px;
    right: 6px;
}
#addremark .all_remarks{
    overflow: auto;
    margin-top: 5px;
    max-height: 150px;
    border-top: 1px solid #ddd;
}
.badge1{/*color: #555;background-color: #ccc;*/
    position:absolute;
   right: 0px;
    top: 0px;
    color:#0093dd;
    font-weight:900;
    font-size:11px;
}

.addnow {cursor:pointer;font-size:14px;float:right;}
.highlight {
    background-color:#DD1035;
    color:#fff;	
}
#parent{max-height: 350px;}
#parent .table{margin-bottom: 0px;}
@media(min-width: 1400px){
   #addremark .all_remarks{
        max-height: 300px;
   }
}

</style>


<script>
    
$('div#addr').click(function(){
    $('.remarkadd').hide();
    $('.addnow').show();       
});

$('.addnow').click(function(){
    $(this).hide();
    $('.closermrk').show();
    $('.remarkadd').show();
});
$('.closermrk').click(function(){
    $(this).hide();
    $('.addnow').show();
    $('.remarkadd').hide();
    $('#remark').val('');
});

function closeaction(){
    $('#remark').val('');
    $('#addremark').hide("slide", { direction: "right" }, 200);
}
function addremark(pur_order,event){
    var id  = pur_order;
    $(".popover").removeClass( "in" );
    $('.remarkadd').show();
    $('.addnow').hide();
    $('#addremark').show("slide", { direction: "right" }, 500);
    $("#txtPurchaseId").val(id);
    event.preventDefault();
    $.ajax({
        type: "GET",
        data: {purchaseid: id},
        dataType:'json',
        url: '<?php echo Yii::app()->createUrl('purchase/viewremarks'); ?>',
        success: function (response) {
            $("#remarkList").html(response.result);
            $(".po_no").html(response.po_no)
            $("#txtPurchaseId").val(id);
       }
    });
}
function deleteEstimation(estimation_id, project_id) {
    if(confirm("Do you want to delete this item?")) {
        $.ajax({
            url: '<?php echo Yii::app()->createUrl('purchase/deleteestimation'); ?>',
            data: {"estimation_id": estimation_id, "project_id": project_id},
            type: "POST",
            success: function (response) {
                $("#estimationlist").html(response);
                $("#errorMessages").show()
                    .html('<div class="alert alert-success">Item estimation deleted successfully.</div>')
                    .fadeOut(10000);
            }
        });
    } else {
        return false;
    }
}
$(document).ready(function() {

   
});
</script>