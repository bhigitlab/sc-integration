<?php
if(Yii::app()->user->role !=1){
  $warehouse = Warehouse::model()->findAll(array('condition' => 'FIND_IN_SET('.Yii::app()->user->id.', assigned_to)'));
}else{
  $warehouse = Warehouse::model()->findAll();
}
$vendor = Vendors::model()->findAll();
$user = Users::model()->findAll();
$tblpx= Yii::app()->db->tablePrefix;
$receipt = Yii::app()->db->createCommand("SELECT warehousereceipt_id FROM {$tblpx}warehousereceipt ORDER BY warehousereceipt_id DESC")->queryRow();
if(empty($receipt)){
    $receipt_no = 1;
}else{
    $receipt_no = $receipt['warehousereceipt_id']+1;
}
?>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.js"></script>

<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/message/css/jquery.message.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/message/js/jquery.message.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/tableHeadFixer.js"></script>

<!--<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/fonts/glyphicons-halflings-regular.eot">
<link rel="stylesheet" href="<?php //echo Yii::app()->theme->baseUrl; ?>/plugins/smoke/css/smoke.min.css">
<script type="text/javascript" src="<?php // echo Yii::app()->theme->baseUrl; ?>/plugins/smoke/js/smoke.min.js"></script>-->
<script>
    var shim = (function(){document.createElement('datalist');})();
</script>
<style>

    .lefttdiv{
            float: left;
    }
    .select2 {
	width : 100%;
	}
</style>
<script>
    $( function() {
            $( "#datepicker" ).datepicker({dateFormat: 'dd-mm-yy'}).datepicker("setDate", new Date());

    } );

</script>
<style>
    .dropdown{
            position: relative;
            display: inline-block;
    }
    .dropdown::before{
            position: absolute;
            content: " \2193";
            top: 0px;
            right: -8px;
            height: 20px;
            width: 20px;
    }

    button#caret {
            border : none;
            background : none;
            position: absolute;
            top: 0px;
            right: 0px;
    }
    .invoicemaindiv th, .invoicemaindiv td{
        padding: 10px;
        text-align: left;
    }
    .addcolumn {
        cursor: pointer;
        background-color: black;
        border: none;
        color: white;
        text-align: center;
        text-decoration: none;
        margin: 2px;
        font-size: 30px;
        width: 40px;
      }
      .invoicemaindiv .pull-right, .invoicemaindiv .pull-left{
          float: none !important;
      }
      #parent , #parent2, #parent3{max-height: 150px;}
      .select2.select2-container.select2-container--default.select2-container--above,.select2.select2-container.select2-container--default.select2-container--below,.select2.select2-container.select2-container--default.select2-container--focus{
          width:  200px  !important;
      }
      @media(min-width: 767px){
          .invoicemaindiv .pull-right{
          float: right !important;
      }
      .invoicemaindiv .pull-left{
          float: left !important;
      }
      }

      .checkek_edit{
		pointer-events: none;
	 }



	 .add_selection {
		 background: #000;
	  }




	.toast-container {
	  width:350px;
	}
	.toast-position-top-right {
	  top: 57px;
	  right: 6px;
	}
	.toast-item-close {
	  background-image: none;
	  cursor: pointer;
	  width: 12px;
	  height: 12px;
	  text-align: center;
	  border-radius: 2px;
	}
	.toast-item-image {
	  font-size: 24px;
	}
	.toast-item-close:hover {
	  color: red;
	}
	.toast-item {
	  border: transparent;
	  border-radius: 3px;
	  font-size: 10px;
	  opacity: 1;
	  background-color: rgba(34,45,50,0.8);
	}
	.toast-item-wrapper p {
	  margin: 0px 5px 0px 42px;
	  font-size: 14px;
	  text-align: justify;
	}


	.toast-type-success {
	  background-color: #00A65A;
	  border-color: #00A65A;
	}
	.toast-type-error {
	  background-color: #DD4B39;
	  border-color: #DD4B39;
	}
	.toast-type-notice {
	  background-color: #00C0EF;
	  border-color: #00C0EF;
	}
	.toast-type-warning {
	  background-color: #F39C12;
	  border-color: #F39C12;
	}

	.span_class {
	   min-width: 70px;
       display: inline-block;
	}
	.purchase-title{
		border-bottom:1px solid #ddd;
	}

	.purchase_items h3{
		font-size:18px;
		color:#333;
		margin:0px;
		padding:0px;
		text-align:left;
		padding-bottom:10px;
	}
	.purchase_items{
		padding:15px;
		box-shadow:0 0 13px 1px rgba(0,0,0,0.25);
	}
	.purchaseitem {
		display: inline-block;
		margin-right: 20px;
	}
	.purchaseitem last-child{margin-right:0px;}
	.purchaseitem label{
		font-size:12px;
	}
	.remark{display:none;}
	.padding-box{padding:3px 0px; min-height:17px; display:inline-block;}
	th{height:auto;}
	.accepted_quantity,.rejected_quantity,.quantity, .rate{max-width:80px;}

	*:focus{
		border:1px solid #333;
		box-shadow:0 0 6px 1px rgba(0,0,0,0.25);
	}

	.rate_highlight {
		background:#DD1035 !important;
		color: #fff;
	}
	.block_purchase{

		padding-left: 15px;

	}
</style>

<div class="container">

<div class="invoicemaindiv" >
    <!--<a style="float:right;" id="download" class="pdfbtn1 pdf_excel" >
            <div class="save_pdf"></div>SAVE AS PDF
    </a>
    <a style="float:right; margin-right:2px;" id="download" class="excelbtn1 pdf_excel" >
            <div class="save_pdf"></div>SAVE AS EXCEL
    </a> -->
    <div class='clearfix purchase-title items_separator'>
    <div class="pull-left">
        <h2 class="">Add Warehouse Receipt</h2>
    </div>
    <div class="add-btn pull-right">
        <?php echo CHtml::Button('Back', array('class'=>'button back_btn btn_top','style'=>'margin-top:15px;','onclick' => 'javascript:location.href="'. $this->createUrl('index').'"'));?>
    </div>
</div>
    <div id="msg_box"></div>


	<form id="pdfvals1" method="post" action="<?php echo $this->createAbsoluteUrl('warehousereceipt/receiptitem'); ?>"  >
	<input type="hidden" name="remove" id="remove" value="">
	<input type="hidden" name="warehousereceipt_id" id="warehousereceipt_id" value="0">
	<input type="hidden" name="item_unit_id_hidden" id="item_unit_id_hidden" value="0">
	<div class="block_purchase">
            <div class="row_block" style="margin-left: -30px;">
                <div class="elem_block col-sm-3">
                        <div class="form-group">
                            <label>WAREHOUSE To: <span class="required">*</span></label>
                            <?php $data = CHtml::listData(Controller::getCategoryOptions(),'id','text','group'); ?>

							<?php echo CHtml::activeDropDownList($model,'warehousereceipt_warehouseid',$data,array('empty' => '-Choose a Warehouse-',
							'class' => 'form-control mandatory','options'=>array($model['warehousereceipt_warehouseid']=>array('selected'=>true))
							)); ?>
                        </div></div>
						<div class="elem_block col-sm-3">
                        <div class="form-group">
                            <label>DATE : <span class="required">*</span></label>
                            <input type="text" value="<?php echo ((isset($date) && $date != '') ? date("d-m-Y", strtotime($date)) : ''); ?>" id="datepicker" class="txtBox date inputs target form-control" name="warehouse_date"  placeholder="Please click to edit" readonly="true">
                        </div> </div>
						<div class="elem_block col-sm-3">
                        <div class="form-group">
                            <label>RECEIPT CLERK : <span class="required">*</span></label>
                            <select name="clerk" class="inputs target clerk"  id="clerk" style="width:190px">
                                <option value="">Choose Receipt Clerk</option>
                                <?php
                                foreach ($user as $key => $value) {
                                    ?>
                                    <option value="<?php echo $value['userid']; ?>"><?php echo $value['first_name'].' '.$value['last_name']; ?></option>
                                    <?php
                                }
                                ?>

                            </select>
                        </div></div>
						<div class="elem_block col-sm-3">
                        <div class="form-group">

							<label>RECEIPT NO : <span class="required">*</span></label>
							<input type="text" value="" id="receipt_no" class="receipt_no form-control" name="receipt_no" >   
						</div> </div>
						
						<div class="elem_block col-sm-3">
                        <div class="form-group">
                            <label>Purchase Bill No : </label>
                            <select name="purchase_bill_no" class="inputs target purchase_bill_no"  id="purchase_bill_no" style="width:190px">
                                <option value="">Choose Purchase Bill No</option>
                            </select>
                        </div></div>
						<div class="elem_block col-sm-3">
                        <div class="form-group">
                            <label>WAREHOUSE FROM: </label>
                            <?php $data = CHtml::listData(Controller::getCategoryOptions(),'id','text','group'); ?>

							<?php echo CHtml::activeDropDownList($model,'warehousereceipt_warehouseid_from',$data,array('empty' => '-Choose a Warehouse-',
							'class' => 'form-control mandatory','options'=>array($model['warehousereceipt_warehouseid_from']=>array('selected'=>true))
							)); ?>
                        </div></div>
						<div class="elem_block col-sm-3">
                        <div class="form-group">
                            <label>Dispatch No : </label>
                            <select name="dispatch_no" class="inputs target dispatch_no"  id="dispatch_no" style="width:190px">
                                <option value="">Choose Dispatch No</option>
							</select>
                        </div></div>
						
                   
						
						<div class="elem_block col-sm-3">
                        <div class="form-group">
                            <label>Remarks :</label>
                            <input type="text" name="vendor" id="vendor" class="form-control">
                        </div></div>

            </div>
            </div>

            <div class="clearfix"></div>
		  <div id="msg"></div>

		  <div id="previous_details" style="position: fixed;top: 94px; left: 70px; right: 70px; z-index: 2; max-width:1150px; margin:0 auto;"></div>
		  <div class="">
            <div class="">
                <div id="warehousereceipt_items"><?php //echo ((!$model->isNewRecord) ? $client : '') ?></div>
            </div>
        </div>













<?php /*

	<div class="purchase_items">
		<h3>Add/Edit Receipt Item</h3>
		<div class="clearfix" style="display:flex;">
			<div class="purchaseitem">
				<div id="select_div">
					<label>Item:</label>
					<select style="width:200px;" class="txtBox stock_item" id="stock_item" name="stock_item">
                                        <option value="">Select one</option>
                                        <?php
                                        foreach ($specification as $key => $value) {
                                            ?>
                                            <option value="<?php echo $value['id']; ?>"><?php echo $value['data']; ?></option>
                                        <?php } ?>
                                        <!--					<option value="other">Other</option>-->
                                    </select>
				</div>
			</div>

			<div class="purchaseitem">
				<div id="select_div">
					<label>Batch:</label>
					<select style="width:100px;" class="txtBox batch" id="batch" name="batch">
											<option value="">Select batch</option>
											
						</select>
				</div>
			</div>
			<div class="purchaseitem">
				<label>Units/Size:</label>
				<select class="txtBox item_unit" id="item_unit" name="unit">
                                        <option value="">Unit</option>
                                        
                </select>
			</div>
            <div class="purchaseitem quantity_div">
				<label>Quantity:</label>
				<input type="text" class="inputs target txtBox quantity allownumericdecimal"  id="quantity" name="quantity[]" value ="0" placeholder=""/></td>
			</div>
			<div class="purchaseitem accepted_quantity_div">
				<label>Accepted Quantity:</label>
				<input type="text" class="inputs target txtBox accepted_quantity allownumericdecimal"  id="accepted_quantity" name="accepted_quantity[]" value ="0" placeholder=""/></td>
			</div>
			<div class="purchaseitem rejected_quantity_div">
				<label>Rejected Quantity:</label>
				<input type="text" class="inputs target txtBox rejected_quantity allownumericdecimal"  id="rejected_quantity" name="rejected_quantity[]" value ="0" placeholder=""/></td>
			</div>
                        <div class="purchaseitem quantity_div">
				<label>J.O No:</label>
				<input type="text" class="inputs target txtBox jono"  id="jono" name="jono[]" placeholder=""/></td>
			</div>
			<div class="purchaseitem pull-right">
			<label>&nbsp;</label>
				<input type="button" class="item_save" id="0" value="Save">
			</div>
		</div>
	</div>
	</div>
*/ ?>
	<br>
	<!-- Additional Items-->
	<?php  echo $this->renderPartial('_additional_items',array('specification'=>$specification))?>
   


<!--	<div id="table-scroll" class="table-scroll">-->
<!--        <div id="faux-table" class="faux-table" aria="hidden"></div>-->
<!--            <div id="table-wrap" class="table-wrap">-->
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Sl.No</th>
                            <th>Item</th>
							<th>Batch</th>
							<th>Unit/Size</th>
                            <th>Quantity</th>
							<th>Accepted Quantity</th>
							<th>Rejected Quantity</th>
                            <th>J.O No</th>
                            <th></th>
                        </tr>
                    </thead>
                        <tbody class="addrow">

                                 </tr>
                        </tbody>
                    </table>
<!--                </div>


                </div>-->
                </div>
                   <!-- <div class="addcolumn">+</div> -->

                    <!--<table border="1" style="width:100%;">
                        <tr>
                            <td>SUB TOTAL :<input type="text" value="<?php // echo ((isset($subtot) && $subtot != '') ? $subtot : ''); ?>"  class="txtBox pastweek" readonly=ture name="subtot"  /></td>
                            <td>GRAND TOTAL :<input type="text" value="<?php // echo ((isset($grand) && $grand != '') ? $grand : ''); ?>"  class="txtBox pastweek grand" name="grand"  readonly=true/>
                            </td>
                        </tr>
                    </table>-->

                    <!--<div class="row">
					  <div class="col-xs-12 col-sm-6 col-md-8">.col-xs-12 .col-sm-6 .col-md-8</div>
					  <div class="col-xs-7 col-md-3">.col-xs-6 .col-md-4</div>
					</div>-->

					<div style="padding-right: 0px;" class="col-md-offset-9 col-md-3 col-sm-offset-8 col-sm-4 col-xs-offset-6 col-xs-6">

		<input type="hidden" value="<?php  echo ((isset($subtot) && $subtot != '') ? $subtot : ''); ?>"  class="txtBox pastweek" readonly=ture name="subtot"  /></td>
         <input type="hidden" value="<?php  echo ((isset($grand) && $grand != '') ? $grand : ''); ?>"  class="txtBox pastweek grand" name="grand"  readonly=true/>



            </div>

                    <br><br>
                    <!--<center><input type="submit" name="invoice" value="Submit" class="form_submit1"></center>-->
                     <center> <!--<input type="button" name="purchase" id="purchase_btn" value="Submit" class="form_submit1">--></center>
					 <div class="row addRow">
            			<div class="col-sm-12 text-center"><?php echo CHtml::Button($model->isNewRecord ? 'Create' : 'Save',array('class' => 'buttonsubmit btn btn-default', 'id' => 'buttonsubmit')); ?></div>
        			</div>
		</form>
	</div>



                <input type="hidden" name="final_amount" id="final_amount" value="0">

             <input type="hidden" name="item_amount_temp" id="item_amount_temp" value="0">


<style>
    .error_message{
            color:red;
    }
    a.pdf_excel {
            background-color: #6a8ec7;
            display:inline-block;
            padding:8px;
            color: #fff;
            border: 1px solid #6a8ec8;
    }
</style>


<?php $url = Yii::app()->createAbsoluteUrl("Warehousereceipt/GetItemsByPurchaseOrDespatch");?>
<script>

	 jQuery.extend(jQuery.expr[':'], {
			focusable: function (el, index, selector) {
				return $(el).is('button, :input, [tabindex]');
			}
		});

		$(document).on('keypress', 'input,select', function (e) {
			if (e.which == 13) {
				e.preventDefault();
				// Get all focusable elements on the page
				var $canfocus = $(':focusable');
				var index = $canfocus.index(document.activeElement) + 1;
				if (index >= $canfocus.length) index = 0;
				$canfocus.eq(index).focus();
			}
		});

	 $(document).ready(function() {
             $(".js-example-basic-single").select2();
             $("#Warehousereceipt_warehousereceipt_warehouseid,.dispatch_no,.purchase_bill_no,#Warehousereceipt_warehousereceipt_warehouseid_from,.vendor,.clerk").select2();
             $(".stock_item").select2();        
    		 $('select').first().focus();
});
	$( "#buttonsubmit" ).keypress(function(e) {
		if (e.keyCode == 13) {
          $('.buttonsubmit').click();
      }
    });
    
    $(".buttonsubmit").click(function(){
       $("#bills-form").submit();
	});


		</script>



<script>
	$(document).ready(function() {
		$().toastmessage({
			sticky: false,
			position: 'top-right',
			inEffectDuration: 1000,
			stayTime: 3000,
			closeText: '<i class="icon ion-close-round"></i>',
		});

		//$('.check_class').hide();
		$(".purchase_items").addClass('checkek_edit');


	});


$(".inputSwitch span").on("click", function() {

	var $this = $(this);

	$this.hide().siblings("input").val($this.text()).show();

});

$(".inputSwitch input").bind('blur', function() {

	var $this = $(this);

	$(this).attr('value', $(this).val());

	$this.hide().siblings("span").text($this.val()).show();

}).hide();


		$(".allownumericdecimal").keydown(function (event) {



			if (event.shiftKey == true) {
				event.preventDefault();
			}

			if ((event.keyCode >= 48 && event.keyCode <= 57) ||
				(event.keyCode >= 96 && event.keyCode <= 105) ||
				event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 ||
				event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190 || event.keyCode == 110 ||  event.keyCode == 13) {
				var splitfield = $(this).val().split(".");
				if((splitfield[1] !== undefined && splitfield[1].length >= 2) && event.keyCode != 8 && event.keyCode != 0 && event.keyCode != 13){
					event.preventDefault();
				}
			} else {
				event.preventDefault();
			}

			if($(this).val().indexOf('.') !== -1 && (event.keyCode == 190 || event.keyCode == 110 ))
				event.preventDefault();
			//if a decimal has been added, disable the "."-button





		});

        $('.stock_item').change(function(){
			var element = $(this);
			var category_id= $(this).val();
			var warehouse_id = $('#Warehousereceipt_warehousereceipt_warehouseid').val();
			var dispatch_no = $('#dispatch_no').val();
			var purchase_bill_no = $('#purchase_bill_no').val();
			var unit_id_hidden = $('#item_unit_id_hidden').val();
			if(category_id != ''){
                            $.ajax({
                                    url:'<?php echo  Yii::app()->createAbsoluteUrl('warehousereceipt/getUnits'); ?>',
                                    type:'GET',
                                    dataType:'json',
                                    data:{data:category_id},
                                    success: function(result) {
										if(result.status == 1) {
										$('select[id="item_unit"]').empty();
										if(result.unit == result.base_unit){
											$('select[id="item_unit"]').append('<option value="'+ result.unit +'" selected>'+ result.units +'</option>');
										}else{
											$.each(result["unit"], function(key, value) {
												if(result.base_unit == value.value){
													var selected ='selected';
												}else{
													var selected ='';
												}
										 		$('select[id="item_unit"]').append('<option value="'+ value.id +'" '+ selected +'>'+ value.value +'</option>');
											});
										}
                    					
											
                                       } 
                                            $('#quantity').focus();
                                    }
                            });
                        }
						if(category_id != '' && warehouse_id != ''){
                            $.ajax({
                                    url:'<?php echo  Yii::app()->createAbsoluteUrl('warehousereceipt/getBatch'); ?>',
                                    type:'GET',
                                    async:true,
                					data:{data:category_id,warehouse_id:warehouse_id,dispatch_no:dispatch_no,purchase_bill_no:purchase_bill_no},
                                    success: function(response) {
										var result = JSON.parse(response);
										//alert(result);
										$('select[id="batch"]').empty();
                    					$.each(result["Batchlist"], function(key, value) {
											 if(value.value ==''){
												$('select[id="batch"]').append('<option value="'+ value.id +'">Please choose Batch</option>');
											 }
										});
										 $.each(result["Batchlist"], function(key, value) {
											 if(value.value ==''){
												value.value ='Default Batch';
											 }
										 	$('select[id="batch"]').append('<option value="'+ value.id +'">'+ value.value +'</option>');
										 });
										
										
                                    }
                            });
                        }
		});



</script>


<script>
function getAllItems(purchase_bill_no,dispatch_no,receipt_id) {
        $.ajax({
           url: "<?php echo $url; ?>",
            data: {"purchase_bill_no": purchase_bill_no,"dispatch_no": dispatch_no,"receipt_id":receipt_id}, 
            //dataType: "json",
            type: "GET",
            success:function(data){
                //alert(data);
                $("#warehousereceipt_items").html(data);
               // $("#Bills_bill_number").focus(); 
                //var amount = $("#aj-amount").val();
                //alert(amount);
                //$("#Bills_bill_amount").val(amount);
                //$("#Bills_bill_totalamount").val(amount);
            }
        });
}
/* Neethu  */

	$(document).on("change", "#Warehousereceipt_warehousereceipt_warehouseid", function () {
	  var element = $(this);
	  var receipt_id = $("#warehousereceipt_id").val();
	  var default_date = $(".date").val();
	  var warehouse = $(this).val();
	  var warehouse_from = $('#Warehousereceipt_warehousereceipt_warehouseid_from').val();
	  var vendor = $('#vendor').val();
	  var dispatch_no = $("#dispatch_no").val();
	  var purchase_bill_no = $("#purchase_bill_no").val();
	  var receipt_no = $('#receipt_no').val();
	  var clerk = $('#clerk').val();
	  if(warehouse != '' || (warehouse != '' && warehouse_from !='')){
		$.ajax({
				method: "GET",
				async: true,
				data: {receipt_id:receipt_id,receipt_no: receipt_no,warehouse_from:warehouse_from,dispatch_no: dispatch_no,purchase_bill_no: purchase_bill_no, default_date: default_date, warehouse: warehouse, vendor: vendor,clerk:clerk},
				url: '<?php echo Yii::app()->createUrl('warehousereceipt/DespatchPurchaseBilllist'); ?>',
				success: function(response) {
					var result = JSON.parse(response);
					$('select[id="dispatch_no"]').empty();
					$('select[id="dispatch_no"]').append('<option value="">Choose Dispatch No</option>');
					$.each(result["despatches"], function(key, value) {
						$('select[id="dispatch_no"]').append('<option value="'+ value.id +'">'+ value.value +'</option>');
					});
					$('select[id="purchase_bill_no"]').empty();
					$('select[id="purchase_bill_no"]').append('<option value="">Choose Purchase Bill No</option>');
					$.each(result["bills"], function(key, value) {
						$('select[id="purchase_bill_no"]').append('<option value="'+ value.id +'">'+ value.value +'</option>');
					});
					$("#Warehousereceipt_warehousereceipt_warehouseid_from option[value='"+warehouse+"']").remove();

					
				}
			});
	  }
	  if((purchase_bill_no == '' || purchase_bill_no == '') && warehouse !=''){
		$.ajax({
				method: "GET",
				async: true,
				data: {receipt_id:receipt_id,receipt_no: receipt_no,warehouse_from:warehouse_from,dispatch_no: dispatch_no,purchase_bill_no: purchase_bill_no, default_date: default_date, warehouse: warehouse, vendor: vendor,clerk:clerk},
				url: '<?php echo Yii::app()->createUrl('warehousereceipt/StockList'); ?>',
				success: function(response) {
					var result = JSON.parse(response);
					$('select[id="stock_item"]').empty();
					$('select[id="stock_item"]').append('<option value="">Select One</option>');
					$.each(result["StockItems"], function(key, value) {
						$('select[id="stock_item"]').append('<option value="'+ value.id +'">'+ value.data +'</option>');
					});
					
				}
		});
	  }
	  var checkrequired = '';
	  if(purchase_bill_no !=""){
          checkrequired = purchase_bill_no;
	  }else{
         if(warehouse_from !=""){
			checkrequired = dispatch_no;
		 }else{
			checkrequired = '';
		 }
	  }
	  if((moment(default_date, 'DD-MM-YYYY',true).isValid())){
		if(warehouse == '' || default_date == '' || receipt_no == '' || clerk =='' || checkrequired == ''){
			$.ajax({
				method: "GET",
				dataType:"json",
				url: '<?php echo Yii::app()->createUrl('warehousereceipt/ajax'); ?>',
				success: function(result) {
					 $(".date").select2("focus");
				}
			});
		} else{
		$.ajax({
				method: "GET",
				async: true,
				data: {receipt_id:receipt_id,receipt_no: receipt_no,warehouse_from:warehouse_from,dispatch_no: dispatch_no,purchase_bill_no: purchase_bill_no, default_date: default_date, warehouse: warehouse, vendor: vendor,clerk:clerk},
				dataType:"json",
				url: '<?php echo Yii::app()->createUrl('warehousereceipt/createnewreceipt'); ?>',
				success: function(result) {
					if(result.response == 'success')
					 {
						$(".purchase_items").removeClass('checkek_edit');
						$('.js-example-basic-single').select2('focus');
						 $("#warehousereceipt_id").val(result.receipt_id);
						 $().toastmessage('showSuccessToast', ""+result.msg+"");
					 } else {
						 $().toastmessage('showErrorToast', ""+result.msg+"");
						 element.val('');
					 }

					  $(".stock_item").select2("focus");
				}
			});
		}
		} else {
		$(this).focus();
		  $().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
		  $(this).focus();
		}
	});

	$(document).on("change", "#vendor", function () {
	  var element = $(this);
	  var receipt_id = $("#warehousereceipt_id").val();
	  var default_date = $(".date").val();
	  var vendor = $(this).val();
	  var warehouse_from = $("#Warehousereceipt_warehousereceipt_warehouseid_from").val();
	  var dispatch_no = $("#dispatch_no").val();
	  var purchase_bill_no = $("#purchase_bill_no").val();
	  var warehouse = $('#Warehousereceipt_warehousereceipt_warehouseid').val();
	  var receipt_no = $('#receipt_no').val();
	  var clerk = $('#clerk').val();
	  var checkrequired = '';
	  if(purchase_bill_no !=""){
          checkrequired = purchase_bill_no;
	  }else{
         if(warehouse_from !=""){
			checkrequired = dispatch_no;
		 }else{
			checkrequired = '';
		 }
	  }
	  if((moment(default_date, 'DD-MM-YYYY',true).isValid()))
		{

		if(warehouse == '' || default_date == '' || receipt_no == '' || clerk =='' || checkrequired == ''){
			$.ajax({
				method: "GET",
				dataType:"json",
				url: '<?php echo Yii::app()->createUrl('warehousereceipt/ajax'); ?>',
				success: function(result) {
					 $(".clerk").focus();
				}
			});

		} else{
		$.ajax({
				method: "GET",
				data: {receipt_id:receipt_id,receipt_no: receipt_no,warehouse_from:warehouse_from,dispatch_no: dispatch_no,purchase_bill_no: purchase_bill_no, default_date: default_date, warehouse: warehouse, vendor: vendor,clerk:clerk},
				dataType:"json",
				url: '<?php echo Yii::app()->createUrl('warehousereceipt/createnewreceipt'); ?>',
				success: function(result) {
					if(result.response == 'success')
					 {
						 $(".purchase_items").removeClass('checkek_edit');
						$('.js-example-basic-single').select2('focus');
						 $("#warehousereceipt_id").val(result.receipt_id);
						 $().toastmessage('showSuccessToast', ""+result.msg+"");
					 } else {
						 $().toastmessage('showErrorToast', ""+result.msg+"");
					 }

					 $(".stock_item").select2("focus");
				}
			});

		}

		} else {
		$(this).focus();
		  $().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
		 $(this).focus();
		}

	});


	$(document).on("change", "#clerk", function () {
	  var element = $(this);
	  var receipt_id = $("#warehousereceipt_id").val();
	  var default_date = $(".date").val();
	  var clerk = $(this).val();
	  var warehouse_from = $('#Warehousereceipt_warehousereceipt_warehouseid_from').val();
	  var dispatch_no = $("#dispatch_no").val();
	  var purchase_bill_no = $("#purchase_bill_no").val();
	  var warehouse = $('#Warehousereceipt_warehousereceipt_warehouseid').val();
	  var receipt_no = $('#receipt_no').val();
	  var vendor = $('#vendor').val();
	  var checkrequired = '';
	  if(purchase_bill_no !=""){
          checkrequired = purchase_bill_no;
	  }else{
         if(warehouse_from !=""){
			checkrequired = dispatch_no;
		 }else{
			checkrequired = '';
		 }
	  }
	  if((moment(default_date, 'DD-MM-YYYY',true).isValid()))
		{
		if(warehouse == '' || default_date == '' || receipt_no == '' || clerk =='' || checkrequired == ''){
			$.ajax({
				method: "GET",
				data: {purchase_id:'test'},
				dataType:"json",
				url: '<?php echo Yii::app()->createUrl('warehousereceipt/ajax'); ?>',
				success: function(result) {
					 $(".purchase_bill_no").select2("focus");
				}
			});

		} else{
		$.ajax({
				method: "GET",
				data: {receipt_id:receipt_id,receipt_no: receipt_no,warehouse_from:warehouse_from,dispatch_no: dispatch_no,purchase_bill_no: purchase_bill_no, default_date: default_date, warehouse: warehouse, vendor: vendor,clerk:clerk},
				dataType:"json",
				url: '<?php echo Yii::app()->createUrl('warehousereceipt/createnewreceipt'); ?>',
				success: function(result) {
					if(result.response == 'success')
					 {
						 $(".purchase_items").removeClass('checkek_edit');
						$('.js-example-basic-single').select2('focus');
						 $("#warehousereceipt_id").val(result.receipt_id);
						 $().toastmessage('showSuccessToast', ""+result.msg+"");
					 } else {
						 $().toastmessage('showErrorToast', ""+result.msg+"");
					 }

					$(".stock_item").select2("focus");
				}
			});

		}

		} else {
		$(this).focus();
		  $().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
		 $(this).focus();
		}

	});



	$(document).on("change", ".date", function () {
	  var element = $(this);
	  var receipt_id = $("#warehousereceipt_id").val();
	  var default_date = $(this).val();
	  var warehouse = $('#Warehousereceipt_warehousereceipt_warehouseid').val();
	  var warehouse_from = $('#Warehousereceipt_warehousereceipt_warehouseid_from').val();
	  var dispatch_no = $("#dispatch_no").val();
	  var purchase_bill_no = $("#purchase_bill_no").val();
	  var vendor = $('#vendor').val();
	  var receipt_no = $('#receipt_no').val();
	  var clerk = $('#clerk').val();
	  if(purchase_bill_no !=""){
          checkrequired = purchase_bill_no;
	  }else{
         if(warehouse_from !=""){
			checkrequired = dispatch_no;
		 }else{
			checkrequired = '';
		 }
	  }
	if((moment(default_date, 'DD-MM-YYYY',true).isValid()))
		{
		  if(warehouse == '' || default_date == '' || receipt_no == '' || clerk =='' || checkrequired == ''){

		} else{
		$.ajax({
				method: "GET",
				async: false,
				data: {receipt_id:receipt_id,receipt_no: receipt_no,warehouse_from:warehouse_from,dispatch_no: dispatch_no,purchase_bill_no: purchase_bill_no, default_date: default_date, warehouse: warehouse, vendor: vendor, clerk:clerk},
				dataType:"json",
				url: '<?php echo Yii::app()->createUrl('warehousereceipt/createnewreceipt'); ?>',
				success: function(result) {
					if(result.response == 'success')
					 {
						 $(".purchase_items").removeClass('checkek_edit');
						 $('.js-example-basic-single').select2('focus');
						 $("#warehousereceipt_id").val(result.receipt_id);
						 $().toastmessage('showSuccessToast', ""+result.msg+"");
					 } else {
						 $().toastmessage('showErrorToast', ""+result.msg+"");
					 }
                                         $(".stock_item").select2("focus");
				}

			});

		}
		} else {
		$(this).focus();
		  $().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
		  $(this).focus();
		}

	$('#clerk').focus();

});


	$(document).on("blur", "#receipt_no", function () {
	  var element = $(this);
	  var receipt_id = $("#warehousereceipt_id").val();
	  var default_date = $(".date").val();
	  var receipt_no = $(this).val();
	  var through = $('#through').val();
	  var warehouse_from = $('#Warehousereceipt_warehousereceipt_warehouseid_from').val();
	  var dispatch_no = $("#dispatch_no").val();
	  var purchase_bill_no = $("#purchase_bill_no").val();
	  var warehouse = $('#Warehousereceipt_warehousereceipt_warehouseid').val();
	  var clerk = $('#clerk').val();
	  var vendor = $('#vendor').val();
	  if(purchase_bill_no !=""){
          checkrequired = purchase_bill_no;
	  }else{
         if(warehouse_from !=""){
			checkrequired = dispatch_no;
		 }else{
			checkrequired = '';
		 }
	  }
	  if((moment(default_date, 'DD-MM-YYYY',true).isValid()))
		{
		if(warehouse == '' || default_date == '' || receipt_no == '' || clerk =='' || checkrequired == ''){

		} else{
		$.ajax({
				method: "GET",
				data: {receipt_id:receipt_id,receipt_no: receipt_no,warehouse_from:warehouse_from,dispatch_no: dispatch_no,purchase_bill_no: purchase_bill_no, default_date: default_date, warehouse: warehouse, vendor: vendor,clerk:clerk},
				dataType:"json",
				url: '<?php echo Yii::app()->createUrl('warehousereceipt/createnewreceipt'); ?>',
				success: function(result) {
					if(result.response == 'success')
					 {
						 $(".purchase_items").removeClass('checkek_edit');
						 $('.js-example-basic-single').select2('focus');
						 $("#warehousereceipt_id").val(result.receipt_id);
						 $().toastmessage('showSuccessToast', ""+result.msg+"");
					 } else {
						 $().toastmessage('showErrorToast', ""+result.msg+"");
					 }

					 $(".stock_item").select2("focus");
				}
			});

		}

		} else {
		$(this).focus();
		  $().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
		 $(this).focus();
		}

	});
	$(document).on("change", "#Warehousereceipt_warehousereceipt_warehouseid_from", function () {
	  var element = $(this);
	  var receipt_id = $("#warehousereceipt_id").val();
	  var default_date = $(".date").val();
	  var warehouse_from = $(this).val();
	  var through = $('#through').val();
	  var receipt_no = $("#receipt_no").val();
	  var dispatch_no = $("#dispatch_no").val();
	  $('#purchase_bill_no').prop('selectedIndex',0);
	  var purchase_bill_no = $("#purchase_bill_no").val();
	  var warehouse = $('#Warehousereceipt_warehousereceipt_warehouseid').val();
	  var clerk = $('#clerk').val();
	  var vendor = $('#vendor').val();
	  if(purchase_bill_no !=""){
          checkrequired = purchase_bill_no;
	  }else{
         if(warehouse_from !=""){
			checkrequired = dispatch_no;
		 }else{
			checkrequired = '';
		 }
	  }
	  if(warehouse != '' || (warehouse != '' && warehouse_from !='')){
		$.ajax({
				method: "GET",
				async: true,
				data: {receipt_id:receipt_id,receipt_no: receipt_no,warehouse_from:warehouse_from,dispatch_no: dispatch_no,purchase_bill_no: purchase_bill_no, default_date: default_date, warehouse: warehouse, vendor: vendor,clerk:clerk},
				url: '<?php echo Yii::app()->createUrl('warehousereceipt/DespatchPurchaseBilllist'); ?>',
				success: function(response) {
					var result = JSON.parse(response);
					$('select[id="dispatch_no"]').empty();
					$('select[id="dispatch_no"]').append('<option value="">Choose Dispatch No</option>');
					$.each(result["despatches"], function(key, value) {
						$('select[id="dispatch_no"]').append('<option value="'+ value.id +'">'+ value.value +'</option>');
					});
					$('select[id="purchase_bill_no"]').empty();
					$('select[id="purchase_bill_no"]').append('<option value="">Choose Purchase Bill No</option>');
					$.each(result["bills"], function(key, value) {
						$('select[id="purchase_bill_no"]').append('<option value="'+ value.id +'">'+ value.value +'</option>');
					});
					
				}
			});
	  }
	  if((purchase_bill_no == '' || purchase_bill_no == '') && warehouse_from !=''){
		$.ajax({
				method: "GET",
				async: true,
				data: {receipt_id:receipt_id,receipt_no: receipt_no,warehouse_from:warehouse_from,dispatch_no: dispatch_no,purchase_bill_no: purchase_bill_no, default_date: default_date, warehouse: warehouse, vendor: vendor,clerk:clerk},
				url: '<?php echo Yii::app()->createUrl('warehousereceipt/StockList'); ?>',
				success: function(response) {
					var result = JSON.parse(response);
					$('select[id="stock_item"]').empty();
					$('select[id="stock_item"]').append('<option value="">Select One</option>');
					$.each(result["StockItems"], function(key, value) {
						$('select[id="stock_item"]').append('<option value="'+ value.id +'">'+ value.data +'</option>');
					});
					
				}
		});
	  }
	  if((moment(default_date, 'DD-MM-YYYY',true).isValid()))
		{
		if(warehouse == '' || default_date == '' || receipt_no == '' || clerk =='' || checkrequired == ''){

		} else{
		$.ajax({
				method: "GET",
				data: {receipt_id:receipt_id,receipt_no: receipt_no,warehouse_from:warehouse_from,dispatch_no: dispatch_no,purchase_bill_no: purchase_bill_no, default_date: default_date, warehouse: warehouse, vendor: vendor,clerk:clerk},
				dataType:"json",
				url: '<?php echo Yii::app()->createUrl('warehousereceipt/createnewreceipt'); ?>',
				success: function(result) {
					if(result.response == 'success')
					 {
						 $(".purchase_items").removeClass('checkek_edit');
						 $('.js-example-basic-single').select2('focus');
						 $("#warehousereceipt_id").val(result.receipt_id);
						 $().toastmessage('showSuccessToast', ""+result.msg+"");
					 } else {
						 $().toastmessage('showErrorToast', ""+result.msg+"");
					 }

					 $(".stock_item").select2("focus");
				}
			});

		}

		} else {
		$(this).focus();
		  $().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
		 $(this).focus();
		}

	});
	$(document).on("change", "#dispatch_no", function () {
	  var element = $(this);
	  var receipt_id = $("#warehousereceipt_id").val();
	  var default_date = $(".date").val();
	  var receipt_no = $("#receipt_no").val();
	  var through = $('#through').val();
	  var warehouse_from = $("#Warehousereceipt_warehousereceipt_warehouseid_from").val();
	  var dispatch_no = $(this).val();
	  $('#purchase_bill_no').prop('selectedIndex',0);
	  var purchase_bill_no = $("#purchase_bill_no").val();
	  var warehouse = $('#Warehousereceipt_warehousereceipt_warehouseid').val();
	  var clerk = $('#clerk').val();
	  var vendor = $('#vendor').val();
	  if(purchase_bill_no !=""){
          checkrequired = purchase_bill_no;
	  }else{
         if(warehouse_from !=""){
			checkrequired = dispatch_no;
		 }else{
			checkrequired = '';
		 }
	  }
	  if(warehouse != '' || (warehouse != '' && warehouse_from !='')){
		$.ajax({
				method: "GET",
				async: true,
				data: {receipt_id:receipt_id,receipt_no: receipt_no,warehouse_from:warehouse_from,dispatch_no: dispatch_no,purchase_bill_no: purchase_bill_no, default_date: default_date, warehouse: warehouse, vendor: vendor,clerk:clerk},
				url: '<?php echo Yii::app()->createUrl('warehousereceipt/DespatchPurchaseBilllist'); ?>',
				success: function(response) {
					var result = JSON.parse(response);
					$('select[id="purchase_bill_no"]').empty();
					$('select[id="purchase_bill_no"]').append('<option value="">Choose Purchase Bill No</option>');
					$.each(result["bills"], function(key, value) {
						$('select[id="purchase_bill_no"]').append('<option value="'+ value.id +'">'+ value.value +'</option>');
					});
					
				}
			});
	  }
	  if(dispatch_no != '' || dispatch_no == ''){
		$.ajax({
				method: "GET",
				async: true,
				data: {receipt_id:receipt_id,receipt_no: receipt_no,warehouse_from:warehouse_from,dispatch_no: dispatch_no,purchase_bill_no: purchase_bill_no, default_date: default_date, warehouse: warehouse, vendor: vendor,clerk:clerk},
				url: '<?php echo Yii::app()->createUrl('warehousereceipt/StockList'); ?>',
				success: function(response) {
					var result = JSON.parse(response);
					$('select[id="stock_item"]').empty();
					$('select[id="stock_item"]').append('<option value="">Select One</option>');
					$.each(result["StockItems"], function(key, value) {
						$('select[id="stock_item"]').append('<option value="'+ value.id +'">'+ value.data +'</option>');
					});
					
				}
		});
	  }
	  if((moment(default_date, 'DD-MM-YYYY',true).isValid()))
		{
		if(warehouse == '' || default_date == '' || receipt_no == '' || clerk =='' || checkrequired == ''){

		} else{
		$.ajax({
				method: "GET",
				data: {receipt_id:receipt_id,receipt_no: receipt_no,warehouse_from:warehouse_from,dispatch_no: dispatch_no,purchase_bill_no: purchase_bill_no, default_date: default_date, warehouse: warehouse, vendor: vendor,clerk:clerk},
				dataType:"json",
				url: '<?php echo Yii::app()->createUrl('warehousereceipt/createnewreceipt'); ?>',
				success: function(result) {
					if(result.response == 'success')
					 {
						 $(".purchase_items").removeClass('checkek_edit');
						 $('.js-example-basic-single').select2('focus');
						 $("#warehousereceipt_id").val(result.receipt_id);
						 $().toastmessage('showSuccessToast', ""+result.msg+"");
						if(dispatch_no == ""){
							dispatch_no = 0;
						}else{
							getAllItems(purchase_bill_no,dispatch_no,receipt_id);
						}
					 } else {
						 $().toastmessage('showErrorToast', ""+result.msg+"");
					 }

					 $(".stock_item").select2("focus");
					 
				}
			});

		}

		} else {
		$(this).focus();
		  $().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
		 $(this).focus();
		}

	});
	$(document).on("change", "#purchase_bill_no", function () {
	  var element = $(this);
	  var receipt_id = $("#warehousereceipt_id").val();
	  var default_date = $(".date").val();
	  var receipt_no = $("#receipt_no").val();
	  var through = $('#through').val();
	  $('#dispatch_no').prop('selectedIndex',0);
	  $('#Warehousereceipt_warehousereceipt_warehouseid_from').prop('selectedIndex',0);
	  var warehouse_from = $("#Warehousereceipt_warehousereceipt_warehouseid_from").val();
	  var dispatch_no = $("#dispatch_no").val();
	  var purchase_bill_no = $(this).val();
	  var warehouse = $('#Warehousereceipt_warehousereceipt_warehouseid').val();
	  var clerk = $('#clerk').val();
	  var vendor = $('#vendor').val();
	  $('#Warehousereceipt_warehousereceipt_warehouseid_from').val("");
	  if(purchase_bill_no !=""){
          checkrequired = purchase_bill_no;
	  }else{
         if(warehouse_from !=""){
			checkrequired = dispatch_no;
		 }else{
			checkrequired = '';
		 }
	  }
	  if(warehouse != '' || (warehouse != '' && warehouse_from !='')){
		$.ajax({
				method: "GET",
				async: true,
				data: {receipt_id:receipt_id,receipt_no: receipt_no,warehouse_from:warehouse_from,dispatch_no: dispatch_no,purchase_bill_no: purchase_bill_no, default_date: default_date, warehouse: warehouse, vendor: vendor,clerk:clerk},
				url: '<?php echo Yii::app()->createUrl('warehousereceipt/DespatchPurchaseBilllist'); ?>',
				success: function(response) {
					var result = JSON.parse(response);
					$('select[id="dispatch_no"]').empty();
					$('select[id="dispatch_no"]').append('<option value="">Choose Dispatch No</option>');
					$.each(result["despatches"], function(key, value) {
						$('select[id="dispatch_no"]').append('<option value="'+ value.id +'">'+ value.value +'</option>');
					});
					
				}
			});
	  }
	  if(purchase_bill_no != '' || purchase_bill_no == ''){
		$.ajax({
				method: "GET",
				async: true,
				data: {receipt_id:receipt_id,receipt_no: receipt_no,warehouse_from:warehouse_from,dispatch_no: dispatch_no,purchase_bill_no: purchase_bill_no, default_date: default_date, warehouse: warehouse, vendor: vendor,clerk:clerk},
				url: '<?php echo Yii::app()->createUrl('warehousereceipt/StockList'); ?>',
				success: function(response) {
					var result = JSON.parse(response);
					$('select[id="stock_item"]').empty();
					$('select[id="stock_item"]').append('<option value="">Select One</option>');
					$.each(result["StockItems"], function(key, value) {
						$('select[id="stock_item"]').append('<option value="'+ value.id +'">'+ value.data +'</option>');
					});
					
				}
		});
	  }
	  if((moment(default_date, 'DD-MM-YYYY',true).isValid()))
		{
		if(warehouse == '' || default_date == '' || receipt_no == '' || clerk =='' || checkrequired == ''){

		} else{
		$.ajax({
				method: "GET",
				data: {receipt_id:receipt_id,receipt_no: receipt_no,warehouse_from:warehouse_from,dispatch_no: dispatch_no,purchase_bill_no: purchase_bill_no, default_date: default_date, warehouse: warehouse, vendor: vendor,clerk:clerk},
				dataType:"json",
				url: '<?php echo Yii::app()->createUrl('warehousereceipt/createnewreceipt'); ?>',
				success: function(result) {
					if(result.response == 'success')
					 {
						 $(".purchase_items").removeClass('checkek_edit');
						 $('.js-example-basic-single').select2('focus');
						 $("#warehousereceipt_id").val(result.receipt_id);
						 $().toastmessage('showSuccessToast', ""+result.msg+"");
						if(purchase_bill_no == ""){
							purchase_bill_no = 0;
						}else{
							getAllItems(purchase_bill_no,dispatch_no,receipt_id);
						}
					 } else {
						 $().toastmessage('showErrorToast', ""+result.msg+"");
					 }

					 $(".stock_item").select2("focus");
				}
			});

		}

		} else {
		$(this).focus();
		  $().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
		 $(this).focus();
		}
		
		
        

	});



	$("#Warehousereceipt_warehousereceipt_warehouseid").change(function(){
            $.ajax({
                    method: "GET",
                    dataType:"json",
                    url: '<?php echo Yii::app()->createUrl('warehousereceipt/ajax'); ?>',
                    success: function(result) {
                             $("#datepicker").focus();
                    }
            });
        });

        $("#clerk").change(function(){
            $.ajax({
                    method: "GET",
                    dataType:"json",
                    url: '<?php echo Yii::app()->createUrl('warehousereceipt/ajax'); ?>',
                    success: function(result) {
                             $("#receipt_no").focus();
                    }
            });
        });
		$("#Warehousereceipt_warehousereceipt_warehouseid_from").change(function(){
            $.ajax({
                    method: "GET",
                    dataType:"json",
                    url: '<?php echo Yii::app()->createUrl('warehousereceipt/ajax'); ?>',
                    success: function(result) {
                             $("#dispatch_no").focus();
                    }
            });
        });
		$("#dispatch_no").change(function(){
			$('#purchase_bill_no').prop('selectedIndex',0);
            $.ajax({
                    method: "GET",
                    dataType:"json",
                    url: '<?php echo Yii::app()->createUrl('warehousereceipt/ajax'); ?>',
                    success: function(result) {
                             $("#dispatch_no").focus();
                    }
            });
        });
		$("#purchase_bill_no").change(function(){
			$('#dispatch_no').prop('selectedIndex',0);
			$('#Warehousereceipt_warehousereceipt_warehouseid_from').prop('selectedIndex',0);
            $.ajax({
                    method: "GET",
                    dataType:"json",
                    url: '<?php echo Yii::app()->createUrl('warehousereceipt/ajax'); ?>',
                    success: function(result) {
                             $(".purchase_bill_no").focus();
                    }
            });
        });

	$("#date").keypress(function(event){
        if(event.keyCode == 13){
            if($(this).val()){
				$("#purchaseno").focus();
			}
        }
    });

	$(".date").keyup(function(event){
		if(event.keyCode == 13){
			$(".date").click();
		}
	});

	$("#vendor").keyup(function(event){
		if(event.keyCode == 13){
			$("#vendor").click();
		}
	});

		var sl_no =1;
		var howMany = 0;
		$('.item_save').click(function()
		{
		 $("#previous_details").hide();
		 var element = $(this);
		  var item_id = $(this).attr('id');

		  if(item_id == 0) {

		 // add
		  var stock_item = $('.stock_item').val();
		  var quantity = $('#quantity').val();
		  var accepted_quantity = $('#accepted_quantity').val();
		  var rejected_quantity = $('#rejected_quantity').val();
		  var batch = $('#batch').val();
		  var unit = $('#item_unit').val();
          var jono = $("#jono").val();
		  var warehouse_from = $('#Warehousereceipt_warehousereceipt_warehouseid_from').val();
		  var warehouse = $('#Warehousereceipt_warehousereceipt_warehouseid').val();
		  var dispatch_no = $("#dispatch_no").val();
	  	  var purchase_bill_no = $("#purchase_bill_no").val();
		  var vendor = $('#vendor').val();
		  var date = $(".date").val();
		  var receipt_no = $('#receipt_no').val();
		  var clerk = $('#clerk').val();
		  var rowCount = $('.table .addrow tr').length;
          	if(purchase_bill_no !=""){
          		checkrequired = purchase_bill_no;
			}else{
				if(warehouse_from !=""){
					checkrequired = dispatch_no;
				}else{
					checkrequired = '';
				}
			}

		  if(warehouse == '' || date == '' || receipt_no == '' || clerk =='' || checkrequired == ''){
				 $().toastmessage('showErrorToast', "Please enter Warehouse receipt details");
			} else{
				if((moment(date, 'DD-MM-YYYY',true).isValid()))
				{

				if(stock_item == '' || quantity == 0 || accepted_quantity == 0)
				{
					 $().toastmessage('showErrorToast', "Please fill item details");
				} else {
						howMany += 1;
						if(howMany ==1)
						{
						var receipt_id = $('input[name="warehousereceipt_id"]').val();
						var data = {'sl_no':rowCount,'quantity':quantity,'accepted_quantity':accepted_quantity,'rejected_quantity':rejected_quantity,'batch':batch,'unit':unit,'stock_item':stock_item,'unit':unit, 'receipt_id' : receipt_id,'jono':jono};
						$.ajax({
							url: '<?php echo  Yii::app()->createAbsoluteUrl('warehousereceipt/receiptitem'); ?>',
							type:'GET',
							dataType:'json',
							data:{data:data},
							success: function(response) {
								if(response.response == 'success')
								{
									 $().toastmessage('showSuccessToast', ""+response.msg+"");
									$('.addrow').html(response.html);
								} else {
									 $().toastmessage('showErrorToast', ""+response.msg+"");
								}
								howMany =0;
								$('#stock_item').val('').trigger('change');
								var quantity = $('#quantity').val('0');
								var accepted_quantity = $('#accepted_quantity').val('');
								var rejected_quantity = $('#rejected_quantity').val('');
                                    $("#jono").val('');
								var unit = $('#item_unit').val('');
								$('#batch').val('');
								$('#stock_item').select2('focus');
							}
						});

						$('.js-example-basic-single').select2('focus');
					}
					}

				} else {

				  $(this).focus();
				  $().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
				  $(this).focus();
				}




			}



		} else {
			// update


		  var stock_item = $('.stock_item').val();
		  var quantity = $('#quantity').val();
		  var accepted_quantity = $('#accepted_quantity').val();
		  var rejected_quantity = $('#rejected_quantity').val();
		  var batch = $('#batch').val();
		  var unit = $('#item_unit').val();
		  var warehouse = $('#Warehousereceipt_warehousereceipt_warehouseid').val();
		  var warehouse_from = $('#Warehousereceipt_warehousereceipt_warehouseid_from').val();
		  var vendor = $('#vendor').val();
		  var date = $(".date").val();
		  var receipt_no = $('#receipt_no').val();
		  var clerk = $('#clerk').val();
		  var jono = $("#jono").val();
		  	if(purchase_bill_no !=""){
          		checkrequired = purchase_bill_no;
			}else{
				if(warehouse_from !=""){
					checkrequired = dispatch_no;
				}else{
					checkrequired = '';
				}
			}
		  if(warehouse == '' || date == '' || receipt_no == '' || clerk =='' || checkrequired == ''){
			  $().toastmessage('showErrorToast', "Please enter Warehouse receipt details");
		  } else {

			  if((moment(date, 'DD-MM-YYYY',true).isValid()))
				{
		  if(stock_item == '' || quantity == 0 || accepted_quantity == 0)
				{
                                    $().toastmessage('showErrorToast', "Please fill item details");
				} else {
					    howMany += 1;
						if(howMany ==1)
						{
						var receipt_id = $('input[name="warehousereceipt_id"]').val();
						var data = {'item_id': item_id, 'sl_no':sl_no,'quantity':quantity,'accepted_quantity':accepted_quantity,'rejected_quantity':rejected_quantity,'batch':batch,'unit':unit,'stock_item':stock_item,'unit':unit,'receipt_id' : receipt_id, 'jono' : jono};
						$.ajax({
							url: '<?php echo  Yii::app()->createAbsoluteUrl('warehousereceipt/updatereceiptitem'); ?>',
							type:'GET',
							dataType:'json',
							data:{data:data},
							success: function(response) {
								if(response.response == 'success')
								{
									$('#final_amount').val(response.final_amount);
									$().toastmessage('showSuccessToast', ""+response.msg+"");
									$('#grand_total').text(response.final_amount);
									$('.addrow').html(response.html);
								} else {
									 $().toastmessage('showErrorToast', ""+response.msg+"");
								}
								howMany =0;
								$('#stock_item').val('').trigger('change');
								var quantity = $('#quantity').val('0');
								var accepted_quantity = $('#accepted_quantity').val('');
								var rejected_quantity = $('#rejected_quantity').val('');
								var unit = $('#item_unit').val('');
								$('#batch').val('');
								$(".item_save").attr('value', 'Save');
								$(".item_save").attr('id', 0);
								$('#stock_item').select2('focus');
                                                                $("#jono").val('');
							}
						});

						$('.js-example-basic-single').select2('focus');
					}
					}

				}  else {
					$(this).focus();
					  $().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
					  $(this).focus();
					}



				}


		 }




		});



$(document).on("blur", ".accepted_quantity", function () {
	var billedquantity = $(".billedquantity").val();
	var accepted_quantity = $(".accepted_quantity").val();	
	var rejectedquantity = $(".rejected_quantity").val();	
	var quantity = $(".receivedquantity").val();	  
    if(quantity <= billedquantity){
		if(accepted_quantity <= quantity){
			rejected_quantity = quantity-accepted_quantity;	
			$(".rejected_quantity").val(rejected_quantity);
		}else{
			alert(quantity);
			$(".accepted_quantity").val(0);
			$(".rejected_quantity").val(0);
		}
	}else{
		$(".accepted_quantity").val(0);
		$(".receivedquantity").val(0);
		$(".rejected_quantity").val(0);
	}
	

});
$(document).on("change", ".receivedquantity", function () {
	var billedquantity = $(".billedquantity").val();
	var accepted_quantity = $(".accepted_quantity").val();	
	var rejectedquantity = $(".rejected_quantity").val();	
	var quantity = $(".receivedquantity").val();
	alert("billedquantity"+billedquantity+"accepted_quantity"+accepted_quantity+"- rejectedquantity"+rejectedquantity+"- quantity"+quantity);	  
	if(quantity <= billedquantity){
		$(".accepted_quantity").val(quantity);
		accepted_quantity = $(".accepted_quantity").val();
		if(accepted_quantity <= quantity){
			var rejected_quantity = quantity-accepted_quantity;	
			$(".rejected_quantity").val(rejected_quantity);
		}else{
			$(".accepted_quantity").val(0);
			$(".rejected_quantity").val(0);
		}
	}else{
		$(".accepted_quantity").val(0);
		$(".receivedquantity").val(0);
		$(".rejected_quantity").val(0);
	}

});
// $(document).on("blur", ".rejectedquantity", function () {
// 	var billedquantity = $(".billedquantity").val();
// 	var accepted_quantity = $(".accepted_quantity").val();	
// 	var rejectedquantity = $(".rejectedquantity").val();	
// 	var quantity = $(".receivedquantity").val();
// 	if(quantity <= billedquantity){
// 		if(rejectedquantity <= quantity){
// 			var accepted_quantity = quantity-rejectedquantity;
// 			$(".accepted_quantity").val(accepted_quantity);
// 		}else{
// 			$(".accepted_quantity").val(0);
// 			$(".rejectedquantity").val(0);
// 		}
// 	}else{
// 		$(".accepted_quantity").val(0);
// 		$(".receivedquantity").val(0);
// 		$(".rejectedquantity").val(0);
// 	}

// });
$(document).on("change", "#quantity", function () {
	var accepted_quantity = $("#accepted_quantity").val();	
	var quantity = $("#quantity").val();
	var rejected_quantity = quantity-accepted_quantity;	  
	$("#rejected_quantity").val(rejected_quantity);

});

$(document).on('click', '.edit_item', function(e){
	e.preventDefault();
	var item_id = $(this).attr('id');
	var $tds = $(this).closest('tr').find('td');
        var description = $tds.eq(1).text();
		var batch = $tds.eq(2).text();
        var quantity = $tds.eq(3).text();
		var accepted_quantity = $tds.eq(4).text();
		var rejected_quantity = $tds.eq(5).text();
        var unit = $tds.eq(6).text();
        var jono = $tds.eq(7).text();
        $abc =  $(this).closest('tr').find('.item_description').attr('id');
        var des_id =  $(this).closest('tr').find('.item_description').attr('id');
		var unit_name = $.trim(unit);
		var unit_id = $('#item_unit_id'+unit_name).val();
        <?php $abc; ?>
		$('#stock_item').val(des_id).trigger('change');
		$('.js-example-basic-single').select2('focus');
		$('#item_unit').val(unit_id);
		$('#item_unit_id_hidden').val(unit_id);
	$('#quantity').val(parseFloat(quantity));
	$('#accepted_quantity').val(parseFloat(accepted_quantity));
	$('#rejected_quantity').val(parseFloat(rejected_quantity));
	$(".item_save").attr('value', 'Update');
	$(".item_save").attr('id', item_id);
        $("#jono").val(jono);
 });


 $('.item_save').keypress(function(e) {
      if (e.keyCode == 13) {
          $('.item_save').click();
      }
 });


function filterDigits(eventInstance) {
                eventInstance = eventInstance || window.event;
                    key = eventInstance.keyCode || eventInstance.which;
                if ((47 < key) && (key < 58) || key == 8) {
                    return true;
                } else {
                        if (eventInstance.preventDefault)
                             eventInstance.preventDefault();
                        eventInstance.returnValue = false;
                        return false;
                    } //if
}


// approve items

	$(document).on('click','.approve_item', function(e){
	e.preventDefault();
	var element = $(this);
	var item_id = $(this).attr('id');
    $.ajax({
		url: '<?php echo  Yii::app()->createAbsoluteUrl('purchase/permissionapprove'); ?>',
		type:'POST',
		dataType:'json',
		data:{item_id:item_id},
		success: function(response) {
			if(response.response == 'success')
			{
				 $(".approveoption_"+item_id).hide();
				 element.closest('tr').find('.rate_highlight').removeClass('rate_highlight');
				 $().toastmessage('showSuccessToast', ""+response.msg+"");
			} else if(response.response == 'warning'){
				 $(".approveoption_"+item_id).hide();
				 element.closest('tr').find('.rate_highlight').removeClass('rate_highlight');
				 $().toastmessage('showWarningToast', ""+response.msg+"");
			} else {
				$().toastmessage('showErrorToast', ""+response.msg+"");
			}
		}
	});
});

$(document).on('mouseover','.rate_highlight', function(e){
	var item_id = $(this).attr('id');
	$.ajax({
		url:'<?php echo  Yii::app()->createAbsoluteUrl('purchase/previousratedetails'); ?>',
		type:'GET',
		dataType:'json',
		data:{item_id:item_id},
		success: function(result) {
			if(result.status == 1) {
			$('#previous_details').html(result.html);
			$("#previous_details").show();
                        $("#pre_fixtable2").tableHeadFixer();
		   } else {
			$('#previous_details').html(result.html);
			$("#previous_details").hide();
		 }
		}
	})
})
$(document).on('mouseout','.rate_highlight', function(e){
	$("#previous_details").hide();
});

$( "#purchaseno").keyup(function() {
       if (this.value.match(/[^a-zA-Z0-9.:]/g)) {
           this.value = this.value.replace(/[^a-zA-Z0-9.:\-/]/g, '');
       }
   });

   $(document).on('click','.getprevious',function(){
	var id = $(this).attr('data-id');
	var res = id.split(",");
	var amount = parseFloat(res[4]);
		$('#description').val(res[0]).trigger('change.select2');
		$('#quantity').val(res[1]);
		$('#item_unit').html(res[2]);
		$('#rate').val(res[3]);
		$('#item_amount').text(amount.toFixed(2));
		var total = (res[1]*res[3]);
		if (isNaN(total)) total = 0;
		$('#previousvalue').text(total.toFixed(2));
	})

        	$(document).on('click', '.removebtn', function(e){
	e.preventDefault();
	element = $(this);
	var item_id = $(this).attr('id');
            var answer = confirm("Are you sure you want to delete?");
            if (answer)
            {

            var receipt_id = $("#warehousereceipt_id").val();
            var data = {'receipt_id':receipt_id,'item_id':item_id};
             $.ajax({
                            method: "GET",
                            async: false,
                            data: {data: data},
                            dataType:"json",
                            url: '<?php echo Yii::app()->createUrl('warehousereceipt/removeitem'); ?>',
                            success: function(result) {
                                    if(result.response == 'success')
                                     {
                                             $('.addrow').html(result.html);
                                             $().toastmessage('showSuccessToast', ""+result.msg+"");
                                     } else {
                                             $().toastmessage('showErrorToast', ""+result.msg+"");
                                     }
                                     $('#stock_item').val('').trigger('change');
                                    var quantity = $('#quantity').val('0');
									var accepted_quantity = $('#accepted_quantity').val('');
									var rejected_quantity = $('#rejected_quantity').val('');
                                    var unit = $('#item_unit').val('');
									$('select[id="item_unit"]').empty();
									$('select[id="item_unit"]').append('<option value="" selected>Unit</option>');
									$('select[id="batch"]').empty();
									$('select[id="batch"]').append('<option value="" selected>Please choose Batch</option>');
                                    $(".item_save").attr('value', 'Save');
                                    $(".item_save").attr('id', 0);
                                    $('#stock_item').select2('focus');
                                    $('#jono').val('');
                            }
                    });

            }
       else
       {

               return false;
       }
});

</script>

<style>
.table-scroll {
	position: relative;
	max-width: 1280px;
	width:100%;
	margin: auto;
	display:table;
}
.table-wrap {
	width: 100%;
	display:block;
	overflow: auto;
	position:relative;
	z-index:1;
        border: 1px solid #ddd;
}
.table-wrap.fixedON,
.table-wrap.fixedON table,
.faux-table table {
	height: 380px;/* match heights*/
}
.table-scroll table {
	width: 100%;
	margin: auto;
	border-collapse: separate;
	border-spacing: 0;
        border: 1px solid #ddd;
}
.table-scroll th, .table-scroll td {
	padding: 5px 10px;
	border: 1px solid #ddd;
	background: #fff;
	vertical-align: top;
}
.faux-table table {
	position: absolute;
	top: 0;
	left: 0;
	width: 100%;
	pointer-events: none;
}
.faux-table table tbody {
	visibility: hidden;
}
/* shrink cells in cloned table so that the table height is exactly 300px so that the header and footer appear fixed */
.faux-table table tbody th, .faux-table table tbody td {
	padding-top:0;
	padding-bottom:0;
	border-top:none;
	border-bottom:none;
	line-height:0.1;
}
.faux-table table tbody tr + tr th, .faux-table tbody tr + tr td {
	line-height:0;
}
.faux-table thead th, .faux-table tfoot th, .faux-table tfoot td,
.table-wrap thead th, .table-wrap tfoot th, .table-wrap tfoot td{
	background: #eee;
}
.faux-table {
	position:absolute;
	top:0;
	right:0;
	left:0;
	bottom:0;
	overflow-y:scroll;
}
.faux-table thead, .faux-table tfoot, .faux-table thead th, .faux-table tfoot th, .faux-table tfoot td {
	position:relative;
	z-index:2;
}
/* ie bug */
.table-scroll table thead tr,
.table-scroll table thead tr th,
.table-scroll table tfoot  tr,
.table-scroll table tfoot  tr th,
.table-scroll table tfoot  tr td{height:1px;}
</style>
<script>
(function() {
   var mainTable = document.getElementById("main-table");
   var tableHeight = mainTable.offsetHeight;
   if (tableHeight > 380) {
  		var fauxTable = document.getElementById("faux-table");
		document.getElementById("table-wrap").className += ' ' + 'fixedON';
  		var clonedElement = mainTable.cloneNode(true);
  		clonedElement.id = "";
  		fauxTable.appendChild(clonedElement);
   }
})();
</script>

<script>
$(".popover-test").popover({
            html: true,
                content: function() {
                  //return $('#popover-content').html();
                  return $(this).next('.popover-content').html();
                }
        });
    $('[data-toggle=popover]').on('click', function (e) {
       $('[data-toggle=popover]').not(this).popover('hide');
    });
    $('body').on('hidden.bs.popover', function (e) {
        $(e.target).data("bs.popover").inState.click = false;
    });
    $('body').on('click', function (e) {
        $('[data-toggle=popover]').each(function () {
            // hide any open popovers when the anywhere else in the body is clicked
            if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                $(this).popover('hide');
            }
        });
    });

     $(document).ajaxComplete(function() {
                   $(".popover-test").popover({
                   html: true,
                   content: function() {
                     return $(this).next('.popover-content').html();
                   }
               });


           });
    </script>

    <style>

        .tooltip-hiden {
    width: auto}
    </style>
