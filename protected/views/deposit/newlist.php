<?php
/* @var $this BankController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'Banks',
);
$page = Yii::app()->request->getParam('Deposit_page');
if ($page != '') {
    Yii::app()->user->setReturnUrl($page);
} else {
    Yii::app()->user->setReturnUrl(0);
}
/*$this->menu=array(
	array('label'=>'Create Bank', 'url'=>array('create')),
	array('label'=>'Manage Bank', 'url'=>array('admin')),
);*/
?>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">

<div class="container" id="expensetype">
    <div class="loading-overlay">
        <span class="fa fa-spinner fa-3x fa-spin"></span>
    </div>
    <div class="clearfix">
        <div class="add-btn pull-right">
            <a style="cursor:pointer;margin-top: 15px;" class="save_btn" href="<?php echo Yii::app()->createUrl('deposit/savetopdf') ?>">SAVE AS PDF</a></span>
            <a style="cursor:pointer;margin-top: 15px;margin-right:3px;" class="save_btn" href="<?php echo Yii::app()->createUrl('deposit/savetoexcel') ?>">SAVE AS EXCEL</a></span>
            <?php
            if (isset(Yii::app()->user->role) && (in_array('/deposit/create', Yii::app()->user->menuauthlist))) {
            ?>

                <!--                <button data-toggle="modal" data-target="#addBank"  class="createBank">Add Deposit</button>-->

                <a class="button adddeposit">Add Deposit</a>
            <?php } ?>
        </div>
        <h2>Deposit</h2>
    </div>
    <div id="adddeposit" style="display:none;"></div>

    <!--        <div class="pull-right">
            <a style="cursor:pointer; " class="savepdf"  href="<?php echo Yii::app()->createUrl('deposit/savetopdf') ?>">SAVE AS PDF</a></span>
            <a style="cursor:pointer; " class="savepdf"  href="<?php echo Yii::app()->createUrl('deposit/savetoexcel') ?>">SAVE AS EXCEL</a></span>
        </div>-->

    <?php // $this->renderPartial('_newsearch', array('model' => $model)) 
    ?>



    <div class="row">
        <div class="col-md-12">

            <?php $this->widget('zii.widgets.CListView', array(
                'dataProvider' => $dataProvider,
                'itemView' => '_newview',
                'template' => '<div>{sorter}</div><div class=""><table cellpadding="10" id="depositbl" class="table">{items}</table></div>',
                'ajaxUpdate' => false,
            )); ?>
        </div>
        <!-- Add Expense type Popup -->
        <!--        <div id="addBank" class="modal fade" role="dialog">
            <div class="modal-dialog modal-lg">

            </div>
        </div>-->


        <!-- Edit Expense type Popup -->

        <!--        <div class="modal fade edit" role="dialog">
            <div class="modal-dialog modal-lg">


            </div>
        </div>-->
        <?php
        Yii::app()->clientScript->registerScript('myjavascript', '
     $(document).ready(function(){
     
       $("#depositbl").dataTable( {
            "scrollY": "300px",
            "scrollCollapse": true,
            "paging": false,
             "columnDefs"        : [       
                { 
                    "searchable"    : false, 
                    "targets"       : [0,2] 
                },
            ],
               
            
	} );
	
	
      
        
	});
        
    ');
        ?>

        <script>
            function editaction(elem, event) {

                $(elem).parent().hide();

                $(elem).parents().find('td:eq(2)').show();
                var rolename = $(elem).parent().find('.list_item').text();

                var id = $(elem).attr('data-id');

                $('.deposit_name').val(rolename);
                $(elem).parents().siblings().find('.test').hide();
                $(elem).parents().siblings().find('td:eq(1)').show();


                return false;
            }

            function cancelaction(elem, event) {

                $(elem).parents().find('.text_black').show();
                $(elem).parents().find('.test').hide();

            }

            function saveaction(elem, event) {
                var value = $(elem).parent().find('.deposit_name').val();
                $('.loading-overlay').addClass('is-active');
                var id = $(elem).attr('data-id');
                $.ajax({
                    type: "POST",
                    dataType: "json",
                    url: "<?php echo $this->createUrl('Deposit/update2') ?>",
                    data: {
                        id: id,
                        value: value
                    },
                    success: function(response) {
                        if (response == null) {
                            $(elem).parents().find('td:eq(2)').hide();
                            $(elem).parents().find('td:eq(1)').show();
                            $(elem).parent().parent().find('.list_item').text(value);

                            // location.reload();
                        } else {
                            var obj = eval(response);
                            $(".errorMessage1").text(obj);
                        }
                    },


                });
                return false;
            }

            function closeaction() {
                $('#adddeposit').slideUp(500);
            }

            $(document).ready(function() {

                $('.adddeposit').click(function() {
                    $('.loading-overlay').addClass('is-active');
                    $.ajax({
                        type: "POST",
                        //dataType: "JSON",
                        url: "<?php echo $this->createUrl('Deposit/create&layout=1') ?>",
                        success: function(response) {
                            $('#adddeposit').html(response).slideDown();
                        },
                    });
                });

            });
            $(document).ajaxComplete(function() {
                $('.loading-overlay').removeClass('is-active');
                $('#loading').hide();
            });
        </script>

    </div>
</div>
<style>
    .savepdf {
        background-color: #6a8ec7;
        border: 1px solid #6a8ec8;
        color: #fff;
        padding: 5px;
    }

    span.text_black {
        color: #333
    }

    .editable {
        float: right;
        cursor: pointer
    }

    .page-body h3 {
        margin: 4px 0px;
        color: inherit;
        text-align: left;
    }

    .panel {
        border: 1px solid #ddd;
    }

    .panel-heading {
        background-color: #ddd;
        height: 40px;
    }

    .errorMessage1 {
        color: red;
    }
</style>