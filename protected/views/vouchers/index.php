<div class="container" id="expense">
    <div class="loading-overlay">
        <span class="fa fa-spinner fa-3x fa-spin"></span>
    </div>
    <div class="expenses-heading">
        <div class="clearfix">
            <?php if ((isset(Yii::app()->user->role) && (in_array('/vendors/paymentcreate', Yii::app()->user->menuauthlist)))) { ?>
                <a href="index.php?r=vouchers/create" class="btn btn-primary  pull-right mt">Add Entry</a>
            <?php } ?>
            <h2>Vouchers</h2>
        </div>
    </div>

    <div class="daybook form">
        <?php if (Yii::app()->user->hasFlash('success')) : ?>
            <div class="alert alert-success alert-dismissible">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <?php echo Yii::app()->user->getFlash('success'); ?>
            </div>
        <?php endif; ?>


        <?php if (Yii::app()->user->hasFlash('error')) : ?>
            <div class="alert alert-danger  alert-dismissible">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <?php echo Yii::app()->user->getFlash('error'); ?>
            </div>
        <?php endif; ?>
    </div>
    <div>
        <div id="errormessage"></div>
    </div>
    <div class="daybook">
        <div class="">
            <?php $this->renderPartial('_search', array(
                'model' => $model, 'user_companies' => $user_companies
            )); ?>
            <div id="daybook-entry">
                <?php $this->widget('zii.widgets.CListView', array(
                    'id' => 'vouchers-grid',
                    'dataProvider' => $model->search(),
                    'itemView' => '_list',
                    'id' => 'id',
                    'enableSorting' => 1,
                    'enablePagination' => true,
                    'template' => '<div>{summary}{sorter}</div><div id="parent"><table width="100" id="fixTable" class="table total-table">{items}</table></div>{pager}',
                    'enablePagination' => false
                )); ?>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).on('click', '.delete_voucher', function(e) {
        e.preventDefault();
        var answer = confirm("Are you sure you want to delete?");
        if (answer) {
            $('.loading-overlay').addClass('is-active');
            var rowId = $(this).attr("id");
            var date = $("#Vouchers_voucher_date").val();
            $.ajax({
                type: "POST",
                data: {
                    id: rowId,
                    date: date
                },
                url: "<?php echo Yii::app()->createUrl("vouchers/deletevoucher") ?>",
                success: function(data) {
                    location.reload();
                }
            })
        }

    })
    $(document).on('click', '.edit_voucher', function() {
        var id = $(this).attr('id');
        var url = "<?php echo Yii::app()->createAbsoluteUrl("vouchers/update") ?>";
        location.href = url + '&id=' + id;
    })
    $(document).on('click', '.view_voucher', function() {
        var id = $(this).attr('id');
        var url = "<?php echo Yii::app()->createAbsoluteUrl("vouchers/view") ?>";
        location.href = url + '&id=' + id;
    })
    $(document).ajaxComplete(function() {
        $('.loading-overlay').removeClass('is-active');
        $('#loading').hide();
    });
</script>