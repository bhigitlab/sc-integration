<?php
$tblpx 	 = Yii::app()->db->tablePrefix;
?>



<div class="">

	<div class="page_filter clearfix">
		<?php $form=$this->beginWidget('CActiveForm', array(
			'action'=>Yii::app()->createUrl($this->route),
			'method'=>'get',
		)); ?>
		<?php
        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        $newQuery = "";
        foreach($arrVal as $arr) {
            if ($newQuery) $newQuery .= ' OR';
            $newQuery .= " FIND_IN_SET('".$arr."', id)";
        }
        $companyInfo = Company::model()->findAll(array('condition' => $newQuery));
        ?>
				<div class="filter_elem">
        <select  id="company_id" class="select_box form-control"  name="company_id">
            <option value="">Select company</option>
            <?php
            if(!empty($companyInfo)) {
                foreach($companyInfo as $key => $value){
            ?>
                    <option value="<?php echo $value['id'] ?>" <?php echo ($value['id'] == $model->company_id)?'selected':''; ?>><?php echo $value['name']; ?></option>

            <?php  } } ?>
        </select>
			</div>
            <?php
        				$user = Users::model()->findByPk(Yii::app()->user->id);
                $arrVal = explode(',', $user->company_id);
                $newQuery = "";
                foreach($arrVal as $arr) {
                    if ($newQuery) $newQuery .= ' OR';
                    $newQuery .= " FIND_IN_SET('".$arr."', company_id)";
                }
								$client = Yii::app()->db->createCommand("SELECT DISTINCT cid,name FROM {$tblpx}clients WHERE (".$newQuery.") ORDER BY name")->queryAll();
        	?>
				<div class="filter_elem">

        <select  id="client_id" class="select_box form-control" name="client_id">
            <option value="">Select client</option>
            <?php
            if(!empty($client)) {
                    foreach($client as $key => $value){
            ?>
            <option value="<?php echo $value['cid'] ?>" <?php echo ($value['cid'] == $model->client_id)?'selected':''; ?>><?php echo $value['name']; ?></option>
            <?php } } ?>
        </select>
			</div>

        <?php
        				$user = Users::model()->findByPk(Yii::app()->user->id);
                $arrVal = explode(',', $user->company_id);
                $newQuery = "";
                foreach($arrVal as $arr) {
                    if ($newQuery) $newQuery .= ' OR';
                    $newQuery .= " FIND_IN_SET('".$arr."', company_id)";
                }
								$project = Yii::app()->db->createCommand("SELECT DISTINCT pid,name FROM {$tblpx}projects WHERE (".$newQuery.") ORDER BY name")->queryAll();
        ?>
				<div class="filter_elem">
	        <select id="projectid" class="select_box form-control" name="project_id">
	            <option value="">Select project</option>
	            <?php
	            if(!empty($project)) {
	                    foreach($project as $key => $value){
	            ?>
	            <option value="<?php echo $value['pid'] ?>" <?php echo ($value['pid'] == $model->project_id)?'selected':''; ?>><?php echo $value['name']; ?></option>
	            <?php } } ?>
	        </select>
				</div>
						<div class="filter_elem">
							<input type="checkbox" <?php echo ($model->status == 'complected')?'checked':''; ?> id="myCheck" onclick="myFunction()">
	            <select  id="status" class="select_box form-control"  name="status">
		            <option value="">Select status</option>
		             <option value="open" <?php echo ('open' == $model->status)?'selected':''; ?>>Open</option>
		             <option value="Extended" <?php echo ('Extended' == $model->status)?'selected':''; ?>>Extended</option>
		             <?php
		             if($model->status == 'Completed'){
		             ?>
		             <option value="Completed" <?php echo ('Completed' == $model->status)?'selected':''; ?>>Completed</option>
		             <?php } ?>
	        		</select>
					</div>
			<?php
					if (!isset($_GET['date_from']) || $_GET['date_from'] == '') {
							$datefrom = '';
					} else {
							$datefrom = $_GET['date_from'];
					}
					?>

			<!--Date From :-->
			<div class="filter_elem">
				<?php echo CHtml::textField('date_from', $datefrom, array('value' => $datefrom, 'readonly' => true,'placeholder'=>'Date From','autocomplete'=>'off','class'=>'form-control')); ?>
			</div>
					<?php
					if (!isset($_GET['date_to']) || $_GET['date_to'] == '') {
							$date_to = '';
					} else {
							$date_to = $_GET['date_to'];
					}
					?>

					<!--Date To :-->
					<div class="filter_elem">
							<?php echo CHtml::textField('date_to', $date_to, array( 'value' => $date_to, 'readonly' => true,'placeholder'=>'Date To','autocomplete'=>'off','class'=>'form-control')); ?>
					</div>

			<div class="filter_elem filter_btns">
				<?php echo CHtml::submitButton('Go'); ?>
				<?php echo CHtml::resetButton('Clear', array('onclick' => 'javascript:location.href="'. $this->createUrl('newlist').'"')); ?>
		</div>
		<?php $this->endWidget(); ?>
	</div>
</div>

<script>
$(document).ready(function() {
                 $(function () {
                    $( "#date_from" ).datepicker({dateFormat: 'dd-mm-yy'});

                     $( "#date_to" ).datepicker({dateFormat: 'dd-mm-yy'});

                });
                 $("#date_from").change(function() {
                    $("#date_to").datepicker('option', 'minDate', $(this).val());
                });
                $("#date_to").change(function() {
                    $("#date_from").datepicker('option', 'maxDate', $(this).val());
                });

		});
</script>
