<?php
/* @var $this ExpensesController */
/* @var $model Expenses */

$this->breadcrumbs = array(
    'Expenses' => array('index'),
    'Create',
);

/* $this->menu=array(
    array('label'=>'List Expenses', 'url'=>array('index')),
    array('label'=>'Manage Expenses', 'url'=>array('admin')),
); */
?>
<div class="container" id="expense">
    <div class="expenses-heading">
        <h3>DUPLICATE VENDOR PAYMENT ENTRIES</h3>
    </div>
    <div class="popwindow" style="display:none;">
        <div id="details">

        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="table-responsive">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>Sl No.</th>
                            <th>Company</th>
                            <th>Project</th>
                            <th>Vendor</th>
                            <th>Description</th>
                            <th>Transaction Type</th>
                            <th>Bank</th>
                            <th>Cheque Number</th>
                            <th>Amount</th>
                            <th>Tax</th>
                            <th>TDS Amount</th>
                            <th>Paid Amount</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i = 1;
                        foreach ($duplicateentry as $entry) {
                            ?>
                            <tr class="duplicaterow" id="duplicaterow<?php echo $i; ?>" data-id="<?php echo $i; ?>"
                                title="Click here to view <?php echo $entry["rowcount"]; ?> similar entries.">
                                <td>
                                    <?php echo $i; ?>
                                </td>
                                <td>
                                    <?php
                                    $company = Company::model()->findByPk($entry["company_id"]);
                                    if ($company)
                                        echo $company->name;
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    $project = Projects::model()->findByPk($entry["project_id"]);
                                    if ($project)
                                        echo $project->name;
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    $vendor = Vendors::model()->findByPk($entry["vendor_id"]);
                                    if ($vendor)
                                        echo $vendor->name;
                                    ?>
                                </td>
                                <td>
                                    <?php echo $entry["description"]; ?>
                                </td>
                                <td>
                                    <?php
                                    $exptype = Status::model()->findByPk($entry["payment_type"]);
                                    if ($exptype)
                                        echo $exptype->caption;
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    $bank = Bank::model()->findByPk($entry["bank"]);
                                    if ($bank)
                                        echo $bank->bank_name;
                                    ?>
                                </td>
                                <td>
                                    <?php echo $entry["cheque_no"]; ?>
                                </td>
                                <td class="text-right">
                                    <?php echo Controller::money_format_inr($entry["amount"], 2); ?>
                                </td>
                                <td class="text-right">
                                    <?php echo Controller::money_format_inr($entry["tax_amount"], 2); ?>
                                </td>
                                <td class="text-right">
                                    <?php echo Controller::money_format_inr($entry["tds_amount"], 2); ?>
                                </td>
                                <td class="text-right">
                                    <?php echo Controller::money_format_inr($entry["paidamount"], 2); ?>
                                </td>
                                <input type="hidden" name="vendor[]" id="vendor<?php echo $i; ?>"
                                    value="<?php echo $entry["vendor_id"]; ?>" />
                                <input type="hidden" name="date[]" id="date<?php echo $i; ?>"
                                    value="<?php echo $entry["date"]; ?>" />
                                <input type="hidden" name="amount[]" id="amount<?php echo $i; ?>"
                                    value="<?php echo $entry["amount"]; ?>" />
                            </tr>
                            <?php
                            $i++;
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<style>
    .popwindow {
        position: fixed;
        top: 94px;
        left: 70px;
        right: 70px;
        z-index: 2;
        max-width: 1150px;
        margin: 0 auto;
    }

    #details {
        background-color: #fff;
        border-color: #fff;
        box-shadow: 0px 0px 8px 6px rgba(0, 0, 0, 0.25);
        color: #555;
        padding: 10px;
        border-radius: 4px;
    }
</style>
<script>
    $(document).ready(function () {
        $(".duplicaterow").click(function (e) {
            $(".popwindow").show();
            var row = $(this).attr("data-id");
            var vendor = $("#vendor" + row).val();
            var date = $("#date" + row).val();
            var amount = $("#amount" + row).val();
            $.ajax({
                type: "GET",
                data: { vendor: vendor, date: date, amount: amount },
                url: "<?php echo $this->createUrl('vendors/getduplicateentries') ?>",
                success: function (data) {
                    $("#details").html(data);
                }
            });
        });
        $(document).on('click', '#details', function (e) {
            $(".popwindow").hide();
            $("#details").html("");
        });
    });
</script>