

<?php
/* @var $this WorkTypeController */
/* @var $model WorkType */
/* @var $form CActiveForm */
?>

<div class="">
    <?php
       $form = $this->beginWidget('CActiveForm', array(
        'id'=>'work-type-form',
        'enableAjaxValidation' => true,
        'enableClientValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true,
            'validateOnType' => false,),
    ));
    ?>

    <!-- Popup content-->
    <div class="panel-body">

            <div class="row">

                <div class="col-md-6">
                    <div class="form-group">
                            <?php echo $form->labelEx($model,'worktype'); ?>
                            <?php echo $form->dropDownList($model, 'worktype', CHtml::listData(WorkType::model()->findAll(array('order' => 'wtid desc')), 'wtid', 'work_type'), array('empty' => '--', 'class' => 'form-control',)); ?>
                            <?php echo $form->error($model,'worktype'); ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                            <?php echo $form->labelEx($model,'rate'); ?>
                            <?php echo $form->textField($model,'rate',array('class'=>'form-control','size'=>30,'maxlength'=>30)); ?>
                            <?php echo $form->error($model,'rate'); ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="save-btnHold">
                        <label style="display:block;">&nbsp;</label>
                        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'btn btn-info btn-sm')); ?>
                        <?php echo CHtml::ResetButton('Reset', array('class'=>'btn btn-default btn-sm')); ?>
                        <?php echo CHtml::ResetButton('Close', array('onclick'=>'closeaction(this,event);','class'=>'btn btn-sm'));  ?>

                    </div>
                </div>
            </div>
        </div>
<?php $this->endWidget(); ?>
</div><!-- form -->