<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.js"></script>

<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/message/css/jquery.message.css">
<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/purchase.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/message/js/jquery.message.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/moment.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/tableHeadFixer.js"></script>
<script>
	var shim = (function() {
		document.createElement('datalist');
	})();
</script>
<style>
	.lefttdiv {
		float: left;
	}

	.extracharge {
		float: left;
	}

	.extracharge h4 {
		position: relative;
	}

	.addcharge {
		position: absolute;
		top: 2px;
		right: -25px;
		cursor: pointer;
	}

	.extracharge div {
		margin-bottom: 10px;
	}

	.addRow input.textbox {
		width: 200px;
		display: inline-block;
		margin-right: 10px;
	}

	.delbil,
	.deleteitem {

		color: #000;
		background-color: #eee;
		padding: 3px;
		cursor: pointer;
	}

	.addRow label {
		display: inline-block;
	}

	.base {
		display: none;
	}
</style>
<style>
	.dropdown {
		position: relative;
		display: inline-block;
	}

	.dropdown::before {
		position: absolute;
		content: " \2193";
		top: 0px;
		right: -8px;
		height: 20px;
		width: 20px;
	}

	button#caret {
		border: none;
		background: none;
		position: absolute;
		top: 0px;
		right: 0px;
	}

	.invoicemaindiv th,
	.invoicemaindiv td {
		padding: 10px;
		text-align: left;
	}

	.addcolumn {
		cursor: pointer;
		background-color: black;
		border: none;
		color: white;
		text-align: center;
		text-decoration: none;
		margin: 2px;
		font-size: 30px;
		width: 40px;
	}

	.invoicemaindiv .pull-right,
	.invoicemaindiv .pull-left {
		float: none !important;
	}

	.add_selection {
		background: #000;
	}

	.toast-container {
		width: 350px;
	}

	.toast-position-top-right {
		top: 57px;
		right: 6px;
	}

	.toast-item-close {
		background-image: none;
		cursor: pointer;
		width: 12px;
		height: 12px;
		text-align: center;
		border-radius: 2px;
	}

	.toast-item-image {
		font-size: 24px;
	}

	.toast-item-close:hover {
		color: red;
	}

	.toast-item {
		border: transparent;
		border-radius: 3px;
		font-size: 10px;
		opacity: 1;
		background-color: rgba(34, 45, 50, 0.8);
	}

	.toast-item-wrapper p {
		margin: 0px 5px 0px 42px;
		font-size: 14px;
		text-align: justify;
	}


	.toast-type-success {
		background-color: #00A65A;
		border-color: #00A65A;
	}

	.toast-type-error {
		background-color: #DD4B39;
		border-color: #DD4B39;
	}

	.toast-type-notice {
		background-color: #00C0EF;
		border-color: #00C0EF;
	}

	.toast-type-warning {
		background-color: #F39C12;
		border-color: #F39C12;
	}

	.span_class {
		min-width: 70px;
		display: inline-block;
	}

	.form-fields .form-control {
		height: 28px;
		display: inline-block;
		padding: 6px 3px;
		width: 60%;
	}

	.remark {
		display: none;
	}

	th {
		height: auto;
	}

	.quantity,
	.rate,
	.small_class {
		max-width: 100%;
	}

	.amt_sec {
		float: right;
	}

	.amt_sec:after,
	.padding-box:after {
		display: block;
		content: '';
		clear: both;
	}

	.client_data {
		margin-top: 6px;
	}

	*:focus {
		border: 1px solid #333;
		box-shadow: 0 0 6px 1px rgba(0, 0, 0, 0.25);
	}

	#parent,
	#parent2 {
		max-height: 150px;
	}

	.tooltip-hiden {
		width: auto;
	}

	.mb-1 {
		margin-bottom: 5px;
	}

	@media(max-width: 767px) {

		.quantity,
		.rate,
		.small_class {
			width: auto !important;
		}

		.padding-box {
			float: right;
		}

		#tax_amount,
		#item_amount {
			display: inline-block;
			float: none;
			text-align: right;
		}

		/* span.select2.select2-container.select2-container--default.select2-container--focus {
			width: auto !important;
		}

		span.select2.select2-container.select2-container--default.select2-container--below {
			width: auto !important;
		} */

		.form-fields .form-control {
			width: 100%;
		}
	}

	@media(min-width: 768px) {
		.invoicemaindiv .pull-right {
			float: right !important;
		}

		.invoicemaindiv .pull-left {
			float: left !important;
		}

		.main-form span.select2.select2-container,.main-form span.select2.select2-container.select2-container--default.select2-container--focus,.main-form .form-control {
			width: 190px !important;
		}

		.main-form span.select2.select2-container.select2-container--default.select2-container--below {
			width: 190px !important;
		}
	}

	.pre_content {
		max-height: 100px;
		overflow-y: auto;
	}

	.pre_content p {
		font-size: 14px;
	}

	.select2-dropdown {
		z-index: 1;
	}
</style>
<?php
$general_settings_for_auto_receipt = Controller::generalSettingsForAutoReceiptToProjectWarehouse($settings = 1);
?>
<?php
        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        $newQuery = "";
        foreach ($arrVal as $arr) {
            if ($newQuery) $newQuery .= ' OR';
            $newQuery .= " FIND_IN_SET('" . $arr . "', id)";
        }
        $companyInfo = Company::model()->findAll(array('condition' => $newQuery));
        ?>
<div class="container">
	<div class="loading-overlay">
		<span class="fa fa-spinner fa-3x fa-spin"></span>
	</div>
	
	<div class="invoicemaindiv">
		<h2 class="purchase-title">Dailyexpense Bills</h2>
		<div id="loading"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/ajax-loader.gif"></div>
		<div id="msg_box"></div>
		<form id="pdfvals1" method="post" action="<?php echo $this->createAbsoluteUrl('bills/addDailyexpensebill'); ?>">
			<input type="hidden" name="remove" id="remove" value="">
			<input type="hidden" name="bill_id" id="bill_id" value="0">
			<input type="hidden" name="purchase_id" id="purchase_id" value="0">
			
			<div id="errormessage"></div>  
        <div class="block_hold shdow_box clearfix  custom-form-style" id="dailyform">
            <h4 class="section_title">Transaction Details</h4>
            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <div class="form-group">
                        <label for="expenseType">Company<span class="required">*</span></label>
                        <select class="form-control js-example-basic-single" name="DailyexpenseBill[company_id]" id="Dailyexpense_company_id" style="width:100%">
                            <option value="">-Select Company-</option>
                            <?php
                            foreach ($companyInfo as $key => $value) {
                            ?>
                                <option value="<?php echo $value['id']; ?>" <?php echo ($model->company_id == $value['id']) ? 'selected' : ''; ?>><?php echo $value['name']; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>

                <div class="col-md-3 col-sm-6">
                    <div class="form-group">
                        <label for="billNumber">Bill No<span class="required">*</span></label>
                        <input type="text" value="<?= $model->bill_no; ?>" class="form-control" name="DailyexpenseBill[bill_no]" id="Dailyexpense_bill_no" />
                        <div class="errorMessage nowrap"></div>
                    </div>
                </div>
                <?php
                $tblpx           = Yii::app()->db->tablePrefix;
                $cExpType        = Yii::app()->db->createCommand("SELECT * FROM " . $tblpx . "company_expense_type WHERE type IN(2,1) ORDER BY name ASC")->queryAll();
                $cReceiptType    = Yii::app()->db->createCommand("SELECT * FROM " . $tblpx . "company_expense_type WHERE type IN(0,2) ORDER BY name ASC")->queryAll();
                $deposit         = Yii::app()->db->createCommand("SELECT * FROM " . $tblpx . "deposit ORDER BY deposit_name ASC")->queryAll();
                ?>
                <div class="col-md-3 col-sm-6">
                    <div class="form-group">
                        <label for="expenseHead">Transaction Head<span class="required">*</span></label>
                        <select class="form-control js-example-basic-single" name="DailyexpenseBill[transaction_head]" id="Dailyexpense_transaction_head" style="width:100%">
                            <option value="">-Select Transaction Head-</option>
                            <optgroup label="Expense Head">
                                <?php
                                foreach ($cExpType as $company) { ?>
                                    <option data-id="1" value="1,<?php echo $company["company_exp_id"]; ?>" <?php echo ($model->transaction_head == $company["company_exp_id"] && $model->transaction_head_type == 1) ? 'selected' : ''; ?>><?php echo $company["name"]; ?></option>
                                <?php } ?>
                            </optgroup>
                            <optgroup label="Receipt Head">
                                <?php
                                foreach ($cReceiptType as $receipt) { ?>
                                    <option data-id="2" value="2,<?php echo $receipt["company_exp_id"]; ?>"  <?php echo ($model->transaction_head == $receipt["company_exp_id"] && $model->transaction_head_type == 2) ? 'selected' : ''; ?>><?php echo $receipt["name"]; ?></option>
                                <?php } ?>
                            </optgroup>
                            <optgroup label="Deposit">
                                <?php
                                foreach ($deposit as $deposits) { ?>
                                    <option data-id="3" value="3,<?php echo $deposits["deposit_id"]; ?>"  <?php echo ($model->transaction_head == $deposits["deposit_id"] && $model->transaction_head_type == 3) ? 'selected' : ''; ?>><?php echo $deposits["deposit_name"]; ?></option>
                                <?php } ?>
                            </optgroup>
                            <optgroup label="Withdrawal">
                                <option data-id="4" value="4,Withdrawal From Bank">Withdrawal From Bank</option>
                            </optgroup>
                            <optgroup label="Deposit to bank">
                                <option data-id="5" value="5,Cash Deposit to Bank">Cash Deposit To Bank</option>
                            </optgroup>
                        </select>
                    </div>
                </div>

                
            </div>
            

            <h4 class="section_title">Amount Details</h4>
            <div class="row daybook-inner">
                <div class="col-md-3 col-sm-3">
                    <div class="form-group">
                        <label for="amount">Amount<span class="required">*</span></label>
                        <input type="text"  value="<?= $model->amount; ?>" class="form-control check" id="txtAmount" name="DailyexpenseBill[amount]" />
                    </div>
                </div>
                <div class="col-md-2 col-sm-3 gsts">
                    <div class="form-group">
                        <label for="sgstp">SGST(%)<span class="required">*</span></label>
                        <input type="text"  value="<?= $model->sgst; ?>" class="form-control check percentage" id="txtSgstp" name="DailyexpenseBill[sgst]" />
                        <span class="gstvalue" id="txtSgst1" name="DailyexpenseBill[sgst]"></span>
                    </div>
                </div>
                <div class="col-md-2 col-sm-3 amntblock">
                    <div class="form-group">
                        <label for="sgst"></label>
                        <input type="hidden" class="form-control" id="txtSgst" name="DailyexpenseBill[sgst]" readonly="true" />

                    </div>
                </div>
                <div class="col-md-2 col-sm-3 gsts">
                    <div class="form-group">
                        <label for="cgstp">CGST(%)<span class="required">*</span></label>
                        <input type="text"  value="<?= $model->cgst; ?>" class="form-control check percentage" id="txtCgstp" name="DailyexpenseBill[cgstp]" />
                        <span class="gstvalue" id="txtCgst1" name="DailyexpenseBill[cgst]"></span>

                    </div>
                </div>
                <div class="col-md-2 col-sm-3 amntblock">
                    <div class="form-group">
                        <label for="cgst"></label>
                        <input type="hidden" class="form-control" id="txtCgst" name="DailyexpenseBill[cgst]" readonly="true" />

                    </div>
                </div>
                <div class="col-md-2 col-sm-3 gsts">
                    <div class="form-group">
                        <label for="igstp">IGST(%)<span class="required">*</span></label>
                        <input type="text"  value="<?= $model->igst; ?>" class="form-control check percentage" id="txtIgstp" name="DailyexpenseBill[igstp]" />
                        <span class="gstvalue" id="txtIgst1" name="DailyexpenseBill[dailyexpense_igst]"></span>

                    </div>
                </div>
                <div class="col-md-2 col-sm-3 amntblock">
                    <div class="form-group">
                        <label for="igst"></label>
                        <input type="hidden" class="form-control" id="txtIgst" name="DailyexpenseBill[igst]" readonly="true" />

                    </div>
                </div>

                <div class="col-md-3 col-sm-3 totalamnt">
                    <div class="form-group">
                        <label for="total"></label>
                        <input type="hidden" class="form-control" id="txtTotal" name="DailyexpenseBill[amount]" readonly="true" />
                        <span class="gstvalue" id="txtgstTotal"></span>
                        <span class="gstvalue" id="txtTotal1" name="DailyexpenseBill[amount]"></span>
                    </div>
                </div>

            </div>


            <div class="row">
                
                <div class="col-md-3 col-sm-6">
                    <div class="form-group"><?php 
					date_default_timezone_set('Asia/Kolkata'); // Set your desired time zone, in this case, Asia/Kolkata
					echo ((isset($model->due_date) && $model->due_date != '') ? date("d-m-Y", strtotime($model->due_date)) : ''); ?>
                        <label for="paid">Due Date<span class="required">*</span></label>
                        <input type="text" value="<?php echo ((isset($model->due_date) && $model->due_date != '') ? date("d-m-Y", strtotime($model->due_date)) : date("d-m-Y")); ?>" id="datepicker1" class="txtBox date inputs target form-control"  name="due_date" placeholder="Please click to edit">
						</div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="description">Description</label>
                        <textarea class="form-control height-60" id="txtDescription" name="DailyexpenseBill[description]" rows="1" /> <?= $model->description; ?> </textarea>
                    </div>
                </div>
				<div class="data-amnts box_holder">
					<div>
						<label class="inline">Total Amount:</label>
						<span id="Bills_bill_amount"> <?= $model->total_amount; ?></span>
					</div>
				</div>
            </div>
			
            <input type="hidden" name="DailyexpenseBill[txtExpensesId]" value="" id="txtExpensesId" />
            <div class="form-group submit-button text-right">
                <button type="button" class="btn btn-info" id="buttonsubmit">SAVE</button>
                <button type="button" class="btn" id="btnReset">RESET</button>
            </div>
        </div>

			<div class="clearfix"></div>
			<div id="msg"></div>
			
	</div>

	</form>
	
</div>



<input type="hidden" name="final_amount" id="final_amount" value="0">

<input type="hidden" name="item_amount_temp" id="item_amount_temp" value="0">

<input type="hidden" name="final_tax" id="final_tax" value="0">

<input type="hidden" name="item_tax_temp" id="item_tax_temp" value="0">


<style>
	.error_message {
		color: red;
	}

	a.pdf_excel {
		background-color: #6a8ec7;
		display: inline-block;
		padding: 8px;
		color: #fff;
		border: 1px solid #6a8ec8;
	}
</style>
<?php $addbillurl = Yii::app()->createAbsoluteUrl("bills/adddailyexpensebill"); ?>
<?php $delbillurl = Yii::app()->createAbsoluteUrl("bills/deletebill2"); ?>

<script>
$(document).ready(function() {
    // Handle the form submission
    $("#buttonsubmit").click(function() { 
        // Collect form data
        var bill_no = $("#Dailyexpense_bill_no").val();
        var company_id = $("#Dailyexpense_company_id").val();
        var transaction_heads = $("#Dailyexpense_transaction_head").val();
        var transaction_head_values = transaction_heads.split(",");
		var transaction_head_type = transaction_head_values[0];
		var transaction_head = transaction_head_values[1];
		var amount = $("#txtAmount").val();
        var sgst = $("#txtSgstp").val();
        var cgst = $("#txtCgstp").val();
        var igst = $("#txtIgstp").val();
        var total_amount = parseFloat(amount)+(((parseFloat(sgst)+parseFloat(cgst)+parseFloat(igst))/100)*parseFloat(amount));
        var due_date = $("#datepicker1").val();
        var description = $("#txtDescription").val();
		var id = "<?= $model->id; ?>";
		console.log("bill_no: " + bill_no);
        $.ajax({
            method: "GET",
			async: true,
            url: "<?php echo $addbillurl; ?>",
            data: {
				id:id,
                bill_no: bill_no,
                company_id: company_id,
                transaction_head: transaction_head,
                transaction_head_type: transaction_head_type,
                amount: amount,
                sgst: sgst,
                cgst: cgst,
                igst: igst,
                total_amount: total_amount,
                due_date:due_date,
                description:description,
            },
            dataType: "json",    
            success: function(response) {console.log(response);
                if (response.status=='true') {
					if (response.redirectUrl) {
						// Redirect to another page using JavaScript
						window.location.href = response.redirectUrl;
                	}
				} else {
					if (response.message && response.message.toLowerCase().includes("mandatory fields")) {
						$("#errormessage").show()
                    .html('<div class="alert alert-danger">Enter all mandatory fields.</div>')
                    .fadeOut(10000);
					$("html, body").animate({
                                scrollTop: 0
                            }, 600);
				} else {
					$("#errormessage").show()
                    .html('<div class="alert alert-danger">Failed to save bill data.</div>')
                    .fadeOut(10000);
					$("html, body").animate({
                                scrollTop: 0
                            }, 600);
					}
				}
            },
            error: function(xhr, status, error) {                
				console.error("AJAX Error: ", error);
				console.log("Response Text: ", xhr.responseText);
            }
        });
    });

    $("#btnReset").click(function() {
		$("#Dailyexpense_bill_no").val('');
					$("#Dailyexpense_company_id").val('');
					$("#Dailyexpense_transaction_head").val('');
					$("#txtAmount").val('');
					$("#txtSgstp").val('');
					$("#txtCgstp").val('');
					$("#txtIgstp").val('');
					$("#datepicker1").val('');
					$("#txtDescription").val('');
        //$("#pdfvals1")[0].reset();
    });

	$("#txtAmount,#txtSgstp,#txtCgstp,#txtIgstp").on("input", function() {
        var amount = parseFloat($("#txtAmount").val()) || 0;
        var sgst = parseFloat($("#txtSgstp").val()) || 0;
        var cgst = parseFloat($("#txtCgstp").val()) || 0;
        var igst = parseFloat($("#txtIgstp").val()) || 0;
		var totalAmount = amount + ((sgst + cgst + igst) / 100 * amount);
		$("#Bills_bill_amount").text(totalAmount.toFixed(2));
    });

	$("#Dailyexpense_bill_no").on("blur", function() {
        var billNumber = $(this).val();
		var bill_id="<?= $model->id; ?>";
		$.ajax({
            method: "GET",
            url: "<?php echo Yii::app()->createUrl("bills/checkdailybillnumber") ?>",  // Replace with your server-side script to check bill number existence
            data: { bill_no: billNumber,bill_id:bill_id },
            dataType: "json",
            success: function(response) {
                if (response.exists) {
                    $("#errormessage").show()
                    .html('<div class="alert alert-danger">Bill number already exists. Please choose a different one.</div>')
                    .fadeOut(10000);
					$("#buttonsubmit").prop('disabled', true);

                } else {
                    $("#buttonsubmit").prop('disabled', false);
				}
            },
            error: function(xhr, status, error) {
                console.error("AJAX Error: " + error);
            }
        });
    });
	$(function() {
    $("#datepicker1").datepicker({
        dateFormat: 'dd-mm-yy', // Set the desired date format
        defaultDate: new Date("<?php echo date('Y-m-d', strtotime($model->due_date)); ?>"), // Set the default date
        // Add any other datepicker options as needed
    });
});

});
</script>



