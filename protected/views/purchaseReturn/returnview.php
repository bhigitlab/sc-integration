<style>
    table {
        border-collapse: collapse;
        width: 100%;
    }
    /*.container{max-width: 1050px;margin:0 auto;padding: 15px;border: 1px solid #ddd;}*/
    .text-right{text-align:right}
    .details,.info-table{border: 1px solid #212121;margin-top: 20px;}
    .details td, .info-table td,.info-table th{padding: 6px 8px; }
    .info-table{margin-bottom: 10px;}
    .text-center{text-align:center;}
    .img-hold{width: 10%;}
    .companyhead{font-size: 18px;font-weight:bold;margin-top:0px;color: #789AD1;}
</style>	 
<div class="container" style="margin:0px 30px">
<?php
    $company_address = Company::model()->findBypk($model->company_id);	
?>
    
    <h4 class="text-center">PURCHASE RETURN DETAILS</h4>
    <?php
    $tblpx = Yii::app()->db->tablePrefix;
    $billId     = $model['bill_id'];
    $bill_details=Yii::app()->db->createCommand("select * FROM ".$tblpx."bills WHERE bill_id=".$billId)->queryRow();
    ?>
	<table class="details">
            <tr>
                <td><label>BILL No : </label><span><?php echo $bill_details['bill_number']; ?></span></td>
            </tr>
            <tr>
                <td><label>RETURN No : </label><span><?php echo $model['return_number']; ?></span></td>
            </tr>
            <tr>            
                <td><label>DATE : </label><span><?php echo date("d-m-Y", strtotime($model['return_date'])); ?></span></td>
            </tr>
            <tr>
                <td><label>VENDOR NAME : </label><span><?php echo $vendor->name; ?></span></td>
            </tr>

        </table>
        
	<br/><br/>
	
	<table border="1" style="width:100%;">
			<thead>
				<tr>
					<th>Sl.No</th>
                                        <th>Specifications/Remark</th>
                                        <th>Quantity</th>
                                        <th>Unit</th>
                                        <th>Rate</th>
                                        <th>Amount</th>
                                        <th>Discount</th>
                                        <th>Discount (%)</th>
                                        <th>CGST</th>
                                        <th>CGST (%)</th>
                                        <th>SGST</th>
                                        <th>SGST (%)</th>
                                        <th>IGST</th>
                                        <th>IGST (%)</th>
                                        <th>Total Tax</th>
                                        <th>Total Tax (%)</th>
                                        <th>Total</th>
				</tr>
			</thead>
			<tbody class="addrow">
				<?php
                $i=1;
                foreach($newmodel as $new) 
                {
                    //extract($new);
                    if($new['category_id']) {
                        $categorymodel = Specification::model()->findByPk($new['category_id']);
                        if($categorymodel)
                            $categoryName = $categorymodel->specification;
                        else
                            $categoryName = "Not available";
                    } else {
                       $categoryName  = $new['remark'];
                    }
                ?>
                <tr>
                    <td><?php echo $i;?></td>
                    <td><?php echo $categoryName; ?></td>
                    <td class="quantity"><?php echo $new['returnitem_quantity']; ?></td>
                    <td><?php echo $new['returnitem_unit'];; ?></td>
                    <td class="rate"><?php echo number_format((float)$new['returnitem_rate'], 2, '.', ''); ?></td>
                    <td><?php echo Controller::money_format_inr($new['returnitem_amount'],2,1); ?></td>
                    <td><?php echo Controller::money_format_inr($new['returnitem_discountamount'],2,1); ?></td>
                    <td><?php echo $new['returnitem_discountpercent']; ?></td>
                    <td><?php echo Controller::money_format_inr($new['returnitem_cgst'],2,1); ?></td>
                    <td><?php echo $new['returnitem_cgstpercent']; ?></td>
                    <td><?php echo Controller::money_format_inr($new['returnitem_sgst'],2,1); ?></td>
                    <td><?php echo $new['returnitem_sgstpercent']; ?></td>
                    <td><?php echo Controller::money_format_inr($new['returnitem_igst'],2,1); ?></td>
                    <td><?php echo $new['returnitem_igstpercent']; ?></td>
                    <td><?php echo Controller::money_format_inr($new['returnitem_taxamount'],2,1); ?></td>
                    <td><?php echo $new['returnitem_taxpercent']; ?></td>
                    <td><?php echo Controller::money_format_inr(($new['returnitem_amount'] - $new['returnitem_discountamount']) + $new['returnitem_taxamount'],2,1); ?></td>
                </tr>

                <?php
                $i++;
                }
                ?>
			</tbody>
	</table>
	<br/><br/>
    <h3>Remarks :</h3>
    <p><?php echo $model['remarks'];?></p>
        <table border="1" align="right" style="width:50%; float: right; font-size:14px;">
		<tr>
                    <td>Amount</td><td style="text-align:right;"><?php echo Controller::money_format_inr($model['return_amount'],2,1); ?></td>
                </tr>
                <tr>
                        <td>Discount Amount</td><td style="text-align:right;"><?php echo Controller::money_format_inr($model['return_discountamount'],2,1); ?></td>
                </tr>
                <tr>
                        <td>Tax Amount</td><td style="text-align:right;"><?php echo Controller::money_format_inr($model['return_taxamount'],2,1); ?></td>
                </tr>
                <tr>
			<td>Grand Total</td><td style="text-align:right;"><?php echo Controller::money_format_inr($model['return_totalamount'],2,1);?></td>
		</tr>
	</table>
	
	<h4>Authorized Signatory,</h4>
	<h4>For <?php echo Yii::app()->name; ?></h4>
	<p>Please make cash payment.</p>
</div>


