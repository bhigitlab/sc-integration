CREATE TABLE `jp_quotation_generator_image` (`id` INT NOT NULL AUTO_INCREMENT , `banner_image` VARCHAR(300) NOT NULL , `created_by` INT(11) NOT NULL , `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP , PRIMARY KEY (`id`)) ENGINE = InnoDB;
ALTER TABLE `jp_quotation_generator_image` ADD `footer_image` VARCHAR(300) NOT NULL AFTER `banner_image`;

ALTER TABLE `jp_purchase_items_import` CHANGE `hsn_code` `hsn_code` VARCHAR(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL;