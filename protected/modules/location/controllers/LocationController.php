<?php

class LocationController extends Controller {

    public function accessRules() {
        $accessArr = array();
        $accessauthArr = array();
        $controller = 'location/' . Yii::app()->controller->id;

        if (isset(Yii::app()->user->menuall)) {
            if (array_key_exists($controller, Yii::app()->user->menuall)) {
                $accessArr = Yii::app()->user->menuall[$controller];
            }
        }

        if (isset(Yii::app()->user->menuauth)) {
            if (array_key_exists($controller, Yii::app()->user->menuauth)) {
                $accessauthArr = Yii::app()->user->menuauth[$controller];
            }
        }
        $access_privlg = count($accessauthArr);
        return array(
            array(
                'allow', // allow all users to perform 'index' and 'view' actions
                'actions' => $accessArr,
                'users' => array('@'),
                'expression' => "$access_privlg > 0",
            ),
            array(
                'allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => $accessauthArr,
                'users' => array('@'),
                'expression' => "$access_privlg > 0",
            ),
            array(
                'allow', // allow admin user to perform 'admin' and 'delete' actions
                //'actions' => array(''),
                'users' => array('@'),
            ),
            array(
                'deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionAdmin() {
        $this->redirect(array('index'));
    }

    public function actionIndex($id = 0) {
        if ($id !== 0 and $id > 0) {
            $formmodel = $this->loadModel($id);
            $actiontype = ' Updated ';
        }

        if ($id == 0) {
            $formmodel = new Location;
            $actiontype = ' Created ';
        }

        if (isset($_POST['Location'])) {
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $formmodel->attributes = $_POST['Location'];
                $formmodel->created_by = Yii::app()->user->id;
                ;
                $formmodel->created_date = date('Y-m-d');
                $success_status = 1;
                if (!$formmodel->save()) {
                    $success_status = 0;
                    throw new Exception("An error occured in Location {$actiontype}");
                }
                $transaction->commit();
            } catch (Exception $error) {
                $transaction->rollBack();
                $success_status = 0;
                $error = $error->getMessage();
            } finally {
                if ($success_status == 1) {
                    Yii::app()->user->setFlash('success', "Location {$actiontype} successfully");
                    return $this->redirect(array('location/index'));
                } else {
                    Yii::app()->user->setFlash('error', $error);
                }
            }
        }
        $model = new Location('search');
        $model->unsetAttributes();  // clear any default values
        $this->render('index', array(
            'model' => $model, 'formmodel' => $formmodel
        ));
    }

    public function actionDelete($id) {

        $model = $this->loadModel($id);
        $transaction = Yii::app()->db->beginTransaction();
        try {
            if (!$model->delete()) {
                $success_status = 0;
                throw new Exception(json_encode($model->getErrors()));
            } else {
                $success_status = 1;
            }
            $transaction->commit();
        } catch (Exception $error) {
            $transaction->rollBack();
            $success_status = 0;
        } finally {
            if ($success_status == 1) {
                echo json_encode(array('response' => 'success', 'msg' => 'Data Deleted Successfully'));
            } else {
                if ($error->errorInfo[1] == 1451) {
                    echo json_encode(array('response' => 'warning', 'msg' => 'Cannot Delete ! This record alredy in use.'));
                } else {
                    echo json_encode(array('response' => 'error', 'msg' => 'Some Error Occured'));
                }
            }
        }
    }

    public function loadModel($id) {
        $model = Location::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    public function actionSearch() {
        $this->render('search');
    }

    public function actionUpdate() {
        $this->render('update');
    }

    public function actionView() {
        $this->render('view');
    }

    // Uncomment the following methods and override them if needed
    /*
      public function filters()
      {
      // return the filter configuration for this controller, e.g.:
      return array(
      'inlineFilterName',
      array(
      'class'=>'path.to.FilterClass',
      'propertyName'=>'propertyValue',
      ),
      );
      }

      public function actions()
      {
      // return external action classes, e.g.:
      return array(
      'action1'=>'path.to.ActionClass',
      'action2'=>array(
      'class'=>'path.to.AnotherActionClass',
      'propertyName'=>'propertyValue',
      ),
      );
      }
     */
}
