<?php
/* @var $this TaxSlabsController */
/* @var $model TaxSlabs */

$this->breadcrumbs = array(
	'Tax Slabs' => array('index'),
	'Manage',
);

$page = Yii::app()->request->getParam('ExpenseType_page');
if ($page != '') {
	Yii::app()->user->setReturnUrl($page);
} else {
	Yii::app()->user->setReturnUrl(0);
}
?>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">

<div class="container" id="expensetype">
	<div class="clearfix">
		<div class="add-btn pull-right">

			<?php
			if (isset(Yii::app()->user->role) && (in_array('/taxSlabs/create', Yii::app()->user->menuauthlist))) {
			?>
				<a class="button addexpense">Add Tax Slab</a>
			<?php } ?>
		</div>
		<h2>Tax Slabs</h2>
	</div>
	<div id="addexpense" style="display:none;"></div>
	<div class="row">
		<div class="col-md-12">

			<?php
			$this->widget('zii.widgets.CListView', array(
				'dataProvider' => $dataProvider,
				'itemView' => '_list',
				'template' => '<div class="container1"><table cellpadding="10" id="exptypetbl" class="table">{items}</table></div>',
				'ajaxUpdate' => false,
			));
			?>
		</div>

		<?php
		Yii::app()->clientScript->registerScript('myjavascript', '
             $(document).ready(function(){
               $("#exptypetbl").dataTable( {
                    "scrollY": "300px",
                    "scrollCollapse": true,
                    "paging": false,
                     "columnDefs"        : [       
                        { 
                            "searchable"    : false, 
                            "targets"       : [0,2] 
                        },
                        { "bSortable": false, "aTargets": [-1]  }
                    ],
                } );
                });

            ');
		?>

		<script>
			function closeaction() {
				$('#addexpense').slideUp(500);
			}


			$(document).ready(function() {


				$('.addexpense').click(function() {
					var id = $(this).attr('data-id');
					$.ajax({
						type: "POST",
						//dataType: "JSON",
						url: "<?php echo $this->createUrl('/TaxSlabs/create&layout=1') ?>",
						success: function(response) {
							$('#addexpense').html(response).slideDown();
						},
					});
				});
				$('.editProject').click(function() {
					//                $('.addexpense').show();
					var id = $(this).attr('data-id');
					$.ajax({
						type: "POST",
						//dataType: "JSON",
						url: "<?php echo $this->createUrl('/TaxSlabs/update&layout=1&id=') ?>" + id,
						success: function(response) {
							$('#addexpense').html(response).slideDown();
						},
					});
				});


				jQuery(function($) {
					$('#addExpensetype').on('keydown', function(event) {
						if (event.keyCode == 13) {
							$("#expensetypesearch").submit();
						}


					});
				});

			});
		</script>



	</div>
</div>
<style>
	.savepdf {
		background-color: #6a8ec7;
		border: 1px solid #6a8ec8;
		color: #fff;
		padding: 5px;
	}

	.page-body h3 {
		margin: 4px 0px;
		color: inherit;
		text-align: left;
	}

	.panel {
		border: 1px solid #ddd;
	}

	.panel-heading {
		background-color: #eee;
		height: 40px;
	}
</style>