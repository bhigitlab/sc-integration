<?php 
if ($index == 0) {
    ?>

    <thead>
        <tr>
            
            <th style="width:40px;">Sl No.</th>
            <th>User Name</th>
            <th>Name</th>
            <th>Role</th>
            <th>Email ID</th>            
            <th>Phone</th>
            <th>Reporting Person</th>
            <th style="display:none;"></th>
            <th style="display:none;"></th>
            <?php
            if(isset(Yii::app()->user->role) && (in_array('/users/updateusers', Yii::app()->user->menuauthlist))){
            ?>
            <th  style="width:40px;">Action</th>
            <?php } ?>
        </tr>   
    </thead>
            <?php
        }
        ?>
            <tr <?php echo ($data->status == 0 ? '' : 'style="background:#C7C5C5"'); ?>>
                <td style="width:40px;"><?php echo $index + 1; ?></td>
                <td><?php echo $data->username; ?></td>
                <td><?php echo $data->first_name.''.$data->last_name; ?></td>
                <td><?php echo $data->userType->role ?></td>
                <td><?php echo $data->email; ?></td>
                <td><?php echo $data->phonenumber; ?></td>
                <td><?php echo $data->reportingPerson['first_name']. " " .$data->reportingPerson['last_name']; ?></td>
                <td style="display:none;"><?php echo $data->first_name; ?></td>
                <td style="display:none;"><?php echo $data->status; ?></td>
                <td> 
                    <?php
                    if(isset(Yii::app()->user->role) && (in_array('/users/updateusers', Yii::app()->user->menuauthlist))){
                    ?>
                    <a class="fa fa-edit editUser" style="cursor:pointer;" data-id="<?php echo $data->userid; ?>" onclick="edituser(<?php echo $data->userid; ?>)"></a>
                    <?php } ?>
                    <?php 
                    if(isset(Yii::app()->user->role) && (in_array('/users/delete', Yii::app()->user->menuauthlist))){
                        if (defined('DELETE_USER') && DELETE_USER) { ?>
                    <span class="delete_user" title="Delete" onclick="deletUser(this,<?php echo $data->userid; ?>)">
                    <span  class="fa fa-trash deletUser" ></span>
                    </span>
                    <?php }
                    } ?>
                </td>                        
            </tr>
                       





