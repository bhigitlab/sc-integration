<div id='previous_details'></div>

<?php
$form = $this->beginWidget('CActiveForm', array(
    'enableAjaxValidation' => true,
    'enableClientValidation' => true,
    'action' => Yii::app()->createAbsoluteUrl("salesquotation/salesQuotation/addquotationitemsDetails"),
    'htmlOptions' => array('enctype' => 'multipart/form-data', 'class' => 'clients-form'),
    'clientOptions' => array(
        'validateOnSubmit' => true,
        'validateOnChange' => true,
        'validateOnType' => false,
    ),
        ));
?>
<?php echo CHtml::hiddenField('cat_id', $category_id, array('id' => 'cat_id')); ?>
<?php echo CHtml::hiddenField('cat_name', $category_label, array('id' => 'cat_name')); ?>
<?php echo CHtml::hiddenField('master_id', $qid, array('id' => 'master_id')); ?>
<?php echo CHtml::hiddenField('revision_no', $rev, array('id' => 'revision_no')); ?>
<?php echo CHtml::hiddenField('status', $status, array('id' => 'status')); ?>
<?php echo CHtml::hiddenField('parent_type', $parent_type, array('id' => 'parent_type')); ?>
<?php echo CHtml::hiddenField('parent_id', $parent_id, array('id' => 'parent_id')); ?>
<?php echo CHtml::hiddenField('category_label_id', $category_label_id, array('id' => 'category_label_id')); ?>

<span id="error-add"></span>
<div class="panel-body">
    <?php if ($quotation_count > 0) { ?>
        <div class="mainitem" style="margin-top: 2rem;"> 
            <br>
            <table class="table qitem_table mt-2 shdow_box" id="qitem_table">
                <thead>
                    <tr>
                        <th colspan="12">Add Items</th>
                    </tr>
                    
                </thead>
                <tbody>
                    <?php
                    $worktypearray = array();
                    $i=0;
                    foreach ($all_quotation_items as $key => $value) {

                        $worktypeid = $value->work_type_id;
                        array_push($worktypearray, $worktypeid);
                        $work_type_label = QuotationWorktypeMaster::model()->findByPk($value->work_type_id);
                        ?>
                        <tr>
                            <td>
                                <?php if($i==0){ ?>
                                <label>Item Name</label>
                                <?php } ?>
                                <?php echo $form->textField($model, 'item_name', array('class' => 'form-control  item_name ', 'autocomplete' => 'off', 'name' => 'item_name[]', 'data-incremntid' => $key, 'id' => 'item_name' . $key)); ?>
                            </td>
                            <td class="worktypedata" data-id ='<?php echo $value->work_type_id ?>'    data-incremnt ='<?php echo $key ?>' data-template ='<?php echo $work_type_label['template_id'] ?>' >
                                <?php if($i==0){ ?>
                                <label>Work Type</label>
                                <?php } ?>
                                <span><input type="text" class="form-control" value="<?php echo $work_type_label['name'] ?>" readonly></span>
                                <?php echo CHtml::hiddenField('work_type_id[]', $value->work_type_id, array('id' => 'hiddenworktype' . $key,'class'=>'hiddenworktype')); ?>
                                <?php echo CHtml::hiddenField('item_id[]', '', array('id' => 'hiddenitem' . $key,'class'=>'hiddenitem')); ?>
                                <?php echo CHtml::hiddenField('template_id[]', $work_type_label['template_id'], array('id' => 'hiddentemplate' . $key,'class'=>'hiddentemplate')); ?>
                                <?php echo $form->hiddenField($model, 'id', array('class' => 'entry_id',  'name' => 'entry_id[]')); ?>                                
                            </td>
                            <td width="30px">
                                <?php if($i==0){ ?>
                                <label>WorkType Label</label>
                                <?php } ?>
                                <?php
                                $newQuery = 'work_type_id =' . $value->work_type_id . ' AND quotation_category_id =' . $category_id . '';
                                echo $form->dropDownList(
                                        $model, 'worktype_label', CHtml::listData(QuotationItemMaster::model()->findAll(array('condition' => $newQuery)), 'worktype_label', 'worktype_label'), array('class' => 'form-control js-example-basic-single field_change invoice_add require worktype_label ', 'name' => 'worktype_label[]', 'empty' => '-Select Worktype Label-', 'id' => 'worktype_label' . $key, 'style' => 'width:100%')
                                );
                                ?>

                            </td>
                            <?php 
                            $style="display:none";
                            $style1="display:block";
                            if ($work_type_label['template_id'] == 1) { 
                                $style="display:block";
                                $style1="display:none";
                            }  ?>
                                <td>
                                    <?php if($i==0){ ?>
                                    <label style="<?php echo $style ?>">Shutter Work</label>
                                    <?php } ?>
                                    <?php echo $form->textField($model, 'shutterwork_description', array('class' => 'form-control require', 'name' => 'shutterwork_description[]', 'value' => '', 'id' => 'shutterwork_description' . $key, 'readonly' => true,'style'=>$style)); ?>
                                
                                <!-- OR -->
                                    <?php echo $form->hiddenField($model, 'material', array('class' => 'form-control require', 'name' => 'material[]', 'value' => '', 'id' => 'material' . $key)); ?>
                                    <?php echo $form->hiddenField($model, 'finish', array('class' => 'form-control require', 'name' => 'finish[]', 'value' => '', 'id' => 'finish' . $key)); ?>
                                
                                </td>
                                <td>
                                    <?php if($i==0){ ?>
                                    <label style="<?php echo $style ?>">Carcass Box Work</label>
                                    <?php } ?>
                                    <?php echo $form->textField($model, 'carcass_description', array('class' => 'form-control require ', 'name' => 'caracoss_description[]', 'value' => '', 'id' => 'caracoss_description' . $key, 'readonly' => true,'style'=>$style)); ?>
                                    <?php echo $form->hiddenField($model, 'shutterwork_material', array('class' => 'form-control require', 'name' => 'shutterwork_material[]', 'value' => '', 'id' => 'shutterwork_material' . $key)); ?>
                                    <?php echo $form->hiddenField($model, 'shutterwork_finish', array('class' => 'form-control require', 'name' => 'shutterwork_finish[]', 'value' => '', 'id' => 'shutterwork_finish' . $key)); ?>
                                    <?php echo $form->hiddenField($model, 'carcass_material', array('class' => 'form-control require', 'name' => 'carcass_material[]', 'value' => '', 'id' => 'carcass_material' . $key)); ?>
                                    <?php echo $form->hiddenField($model, 'carcass_finish', array('class' => 'form-control require', 'name' => 'carcass_finish[]', 'value' => '', 'id' => 'carcass_finish' . $key)); ?>
                                    
                                <!-- OR --> 
                                <?php if($i==0){ ?> 
                                <label style="<?php echo $style1 ?>">Description</label> 
                                <?php } ?>
                                <?php echo $form->textField($model, 'description', array('class' => 'form-control require description', 'autocomplete' => 'off', 'name' => 'description[]', 'value' => '', 'id' => 'description' . $key, 'readonly' => true,'style'=>$style1)); ?>

                                </td>
                                

                            <td>
                                <?php if($i==0){ ?>
                                <label>Unit</label>
                                <?php } ?>
                                <?php
                                echo $form->dropDownList(
                                        $model, 'unit', CHtml::listData(Unit::model()->findAll(), 'id', 'unit_name'), array('class' => 'form-control js-example-basic-single field_change invoice_add require ', 'name' => 'unit[]', 'empty' => '-Select Unit-', 'id' => 'unit' . $key, 'style' => 'width:100%')
                                );
                                ?>
                            </td>
                            <?php if($this->getActiveTemplate() == 'TYPE-7'){ ?>
                            <td>
                                <?php if($i==0){ ?>
                                <label>Length</label>
                                <?php } ?>
                                <?php echo $form->textField($model, 'length', array('class' => 'form-control allownumericdecimal length ', 'autocomplete' => 'off', 'name' => 'length[]', 'data-incremntid' => $key, 'id' => 'length' . $key)); ?>
                                <?php echo $form->error($model, 'length'); ?>
                            </td>
                            <td>
                                <?php if($i==0){ ?>
                                <label>Width</label>
                                <?php } ?>
                                <?php echo $form->textField($model, 'width', array('class' => 'form-control allownumericdecimal width ', 'autocomplete' => 'off', 'name' => 'width[]', 'data-incremntid' => $key, 'id' => 'width' . $key)); ?>
                                <?php echo $form->error($model, 'width'); ?>
                            </td>
                            <?php } ?>
                            <td>
                                <?php if($i==0){ ?>
                                <label>Quantity</label>
                                <?php } ?>
                                <?php echo $form->textField($model, 'quantity', array('class' => 'form-control allownumericdecimal  require quantity ', 'autocomplete' => 'off', 'name' => 'qty[]', 'data-incremntid' => $key, 'id' => 'qty' . $key)); ?>
                                <?php echo $form->error($model, 'quantity'); ?>
                            </td>
                            <td>
                            <?php if($i==0){ ?>
                                <label>Quantity (in NOS)</label>  
                                <?php } ?>                          
                                <?php echo $form->textField($model, 'quantity_nos', array('class' => 'form-control   require ', 'autocomplete' => 'off', 'name' => 'qtynos[]')); ?>
                                <?php echo $form->error($model, 'quantity_nos'); ?>
                            </td>
                            <td>
                                <?php if($i==0){ ?>
                                <label>MRP</label>
                                <?php } ?>                                
                                <?php echo $form->textField($model, 'mrp', array('class' => 'form-control allownumericdecimal require mrp', 'autocomplete' => 'off', 'name' => 'mrp[]')); ?>
                                <?php echo $form->error($model, 'mrp'); ?>
                            </td>
                            <td>
                                <?php if($i==0){ ?>
                                <label>Amount after discount</label>
                                <?php } ?>
                                <?php echo $form->textField($model, 'amount_after_discount', array('class' => 'form-control allownumericdecimal  require amountafter-discount', 'autocomplete' => 'off', 'name' => 'amount_after_discount[]', 'id' => 'amount_after_discount' . $key)); ?>
                                <?php echo $form->error($model, 'amount_after_discount'); ?>
                            </td>
                        </tr>

                    <?php $i++; } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <th colspan="12" class="text-right">
                            <button type="button" class = 'btn btn-info additem btn-sm'>Save</button>
                        </th>
                    </tr>
                </tfoot>
            </table>
        </div>
    <?php } ?>
    <?php $this->endWidget(); ?>

</div>
<script>
<?php
$addItem = Yii::app()->createAbsoluteUrl("salesquotation/salesQuotation/addquotationitemsDetails");
$url = Yii::app()->createAbsoluteUrl("salesquotation/salesQuotation/index");
$amountget = Yii::app()->createAbsoluteUrl("salesquotation/salesQuotation/getamountdetails");
?>
    function  displaysub() {
        $("#subitem").show();
    }
    function displayextra() {
        $("#extrawork").show();
    }

    $(document).on('change', '.sub_worktype_label', function () {
        var element = $(this);
        var worktypelabel = $('option:selected', this).text();
        var cat_id = '<?php echo $category_id ?>';
        var id = $(this).attr('data-subincrement');
        var worktypeid = $("#sub_worktype" + id).val();
        var template_id = $('option:selected', this).attr('data-template');
        $.ajax({
            url: '<?php echo Yii::app()->createAbsoluteUrl('salesquotation/SalesQuotation/getdatasofworktype'); ?>',
            type: 'GET',
            dataType: 'json',
            data: {
                cat_id: cat_id,
                worktypelabel: worktypelabel,
                worktypeid: worktypeid,
                template_id: template_id
            },
            success: function (result) {

                if (result.status == 1) {
                    $('#sub_item' + id).val(result.item_id);
                    $('#previous_details').html(result.html);
                    $("#previous_details").show();//
                   // $("#pre_fixtable").tableHeadFixer();
                    getamountsub(id);
                } else {
                    $('#previous_details').html(result.html);
                  //  $("#pre_fixtable").tableHeadFixer();
                }
                if (template_id == 1) {
                    $('#sub_shutterwork_description' + id).val(result.shutterdesc);
                    $('#sub_caracoss_description' + id).val(result.carcassdesc);
                    $('#sub_shutterwork_material' + id).val(result.shuttermaterial);
                    $('#sub_shutterwork_finish' + id).val(result.shutterfinish);
                    $('#sub_carcass_material' + id).val(result.carcasmaterial);
                    $('#sub_carcass_finish' + id).val(result.carcassfinish);
                } else {
                    if (template_id == 3) {
                        $('#sub_material' + id).val(result.material);
                        $('#sub_finish' + id).val(result.finish);
                    }
                    $('#sub_description' + id).val(result.description);
                }
            }
        })
    })
    function getamountsub(id) {
        var item_id = $("#sub_item" + id).val();
        var qty = $("#sub_quantity" + id).val();
        var total_amount = $("#total-amount").val();
        var total_amount_val = '<?php echo $model->mainitem_total ?>';
        var status = '<?php echo $status; ?>';
        $.ajax({
            url: "<?php echo $amountget; ?>",
            data: {
                item_id: item_id,
                qty: qty
            },
            type: "POST",
            dataType: 'json',
            success: function (result) {
                if (result != '') {
                    var amount = result.profit_amount;
                    $("#sub_amount_after_discount" + id).val(result.profit_amount);
                    var mrp_after_discount = parseInt(result.profit_amount) + parseInt((result.profit_amount * result.discount) / 100);
                    $("#sub_mrp" + id).val(mrp_after_discount);
                    var sum_value = 0;
                    $('.amountafter-discount').each(function () {
                        sum_value += +$(this).val();
                    })
                    $('.amount-after-discount-extra').each(function () {
                        sum_value += +$(this).val();
                    })
                    var total = sum_value;
                    if (status == 0) {
                        total = parseFloat(sum_value) + parseFloat(total_amount_val);
                    }
                    $('#total-amount-val').val(total);
                    if (total_amount != '') {
                        $("#sgstp").change();
                        $("#cgstp").change();
                        $("#igstp").change();
                    }

                }
            }
        });
    }

    $(document).on('change', '.quantity_sub', function () {
        var element = $(this);
        var id = $(this).attr('data-subincrement');
        var worktypeid = $("#sub_worktype" + id).val();
        var template_id = $('option:selected', $("#sub_worktypelabel" + id)).attr('data-template');
        getamountsub(id);
    });

    $(".allownumericdecimal").keydown(function (event) {
        if (event.shiftKey == true) {
            event.preventDefault();
        }
        if ((event.keyCode >= 48 && event.keyCode <= 57) ||
                (event.keyCode >= 96 && event.keyCode <= 105) ||
                event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 ||
                event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190 || event.keyCode == 110 || event.keyCode == 13) {
            var splitfield = $(this).val().split(".");
            if ((splitfield[1] !== undefined && splitfield[1].length >= 2) && event.keyCode != 8 && event.keyCode != 0 && event.keyCode != 13) {
                event.preventDefault();
            }
        } else {
            event.preventDefault();
        }

        if ($(this).val().indexOf('.') !== -1 && (event.keyCode == 190 || event.keyCode == 110))
            event.preventDefault();

    });



    function closeaction(elem, event) {
        $(elem).parents(".clients-form").hide();

    }


    function getamount(element, id) {
        var item_id = $(element).parents('tr').find("#hiddenitem" + id).val();
        console.log(id) ;
        var qty = $(element).parents('tr').find(".quantity").val();
        var length = $(element).parents('tr').find(".length").val();
        var width = $(element).parents('tr').find(".width").val();
        var total_amount = $("#total-amount").val();
        var total_amount_val = '<?php echo $model->mainitem_total ?>';
        var status = '<?php echo $status; ?>';
        $.ajax({
            url: "<?php echo $amountget; ?>",
            data: {
                item_id: item_id,
                length: length,
                width: width,
                qty: qty
            },
            type: "POST",
            dataType: 'json',
            success: function (result) {
                if (result != '') {
                    var amount = result.profit_amount;
                    $(element).parents('tr').find(".amountafter-discount").val(result.profit_amount);
                    var mrp_after_discount = parseInt(result.profit_amount) + parseInt((result.profit_amount * result.discount) / 100);
                    $(element).parents('tr').find(".mrp").val(mrp_after_discount);
                    var sum_value = 0;
                    $('.amountafter-discount').each(function () {
                        sum_value += +$(this).val();
                    })
                    $('.amount-after-discount-extra').each(function () {
                        sum_value += +$(this).val();
                    })
                    var total = sum_value;
                    if (status == 0) {
                        total = parseFloat(sum_value) + parseFloat(total_amount_val);
                    }
                    $('#total-amount-val').val(total);
                    if (total_amount != '') {
                        $("#sgstp").change();
                        $("#cgstp").change();
                        $("#igstp").change();
                    }

                }
            }
        });
    }

    
    $(function () {
        $('#add_field_demo').click(function () {
            var clone = $('.extra-work:last-child').clone();
            clone.appendTo("#container_extrawork");
            clone.find('input:text').val('');
        });
    });
    $(function () {
        $('#add_field_demo_remove').click(function () {
            var rows = $(this).parent(".btn-holder").siblings('#container_extrawork');
            var row_len = $(rows).children().length - 1;
            if (row_len >= 1) {
                $(rows).find('.extra-work:last-child').remove();
                return false;
            }
        });
    });

    $(function () {
        var regex = /^(.+?)(\d+)$/i;
        var cloneIndex = $(".sub_item").length;
        $('#add_field_sub').click(function () {
            var clone = $('.sub_item:last-child').clone();
            clone.find('input:text').val('');
            clone.appendTo("#container_subitem")
                    .attr("id", "subiteminput" + (cloneIndex + 1))
                    .find("*")
                    .each(function () {
                        var id = this.id || "";
                        var match = id.match(regex) || [];
                        if (match.length == 3) {
                            this.id = match[1] + (cloneIndex + 1);
                            $(this).attr('data-subincrement', (cloneIndex + 1));
                        }
                    })
            cloneIndex++;
        });
    });

    $(function () {
        $('#add_field_remove_sub').click(function () {
            var rows = $(this).parent(".sub_btn").siblings('#container_subitem');
            var row_len = $(rows).children().length - 1;
            if (row_len >= 1) {
                $(rows).find('.sub_item:last-child').remove();
                return false;
            }
        });
    });

    function getWorkType(category_id, callback){        
        $.ajax({
            url: '<?php echo Yii::app()->createAbsoluteUrl('salesquotation/SalesQuotation/getWorkType'); ?>',
            type: 'POST',
            dataType: 'json',
            data: {
                cat_id: category_id
            },
            success: callback
        })        
    }
    $(document).on('change', '.length', function () {
        var element = $(this);
        var val = element.attr('data-incremntid');
        getamount(element, val);
     
    }); 
    $(document).on('change', '.width', function () {
        var element = $(this);
        var val = element.attr('data-incremntid');
        getamount(element, val);
     
    });
    $(document).on('change', '.quantity', function () {
        var totalRow = $(this).parents('tbody').find('tr').length - 1;
        var rowIndex = $(this).parents('tr').index();        
        var element = $(this);
        var category_id = $(this).parents(".clients-form").find("#cat_id").val();         
        var val = element.attr('data-incremntid');
        var worktype = $('#hiddenworktype' + val).val();
        var template = $('#hiddentemplate' + val).val();
        getamount(element, val);
        var increment_id = $('#qitem_table tr:last').children('td:first').attr('data-incremnt');
        var count = $('.worktypedata').length;
        var worktype_label = element.parents("tr").find('#worktype_label' + val).val();        
        if (worktype_label != '') {                        
            var $tr = $(this).parents('tbody').find('tr:last-child');
            var allTrs = $tr.closest('table').find('tr');
            var lastTr = allTrs[allTrs.length - 1];
            var $clone = $tr.clone();
            var key = parseInt(val, 10) + parseInt(1, 10);
            $clone.find('td').each(function () {
                var el = $(this).children('input,select');
                $(this).children('input,select').removeClass('commonadd-val');
                el.each(function () {
                    var id = $(this).attr('id') || null;
                    if (id) {
                        var i = id.substr(id.length - 1);
                        var name = $(this).attr('name');
                        var classname = $(this).attr('class');
                        var prefix = id.substr(0, (id.length - 1));
                        $(this).attr('id', prefix + (+count));
                        $(this).parents('tr').find('td:first-child').attr('data-incremnt', count);
                        $(this).attr('name', name);
                        $(this).attr('class', classname);
                        $(this).parents('tr').attr('class', 'clone');
                    }
                });
            });
            getWorkType(category_id, function(response) {
                $clone.find('.worktypedata').find('span').hide();
                $clone.find('.worktypedata').find('select').hide();
                // $clone.find('.worktypedata').find("input:first-child").hide();
                $clone.find('.worktypedata').append('<select class="form-control worktypeClone">'+response.workTypeData+'</select>')           
            });
            
            $clone.find('.worktypedata').attr('data-incremnt',count);            
            $clone.find('.quantity').attr('data-incremntid', count);
            $clone.find('input:text').val('');
            if(($("tr.clone").length !=0)){                  
               var  $lastclone  = $(this).parents('tbody').find('tr.clone:last-child');
                var worktypeClone = $lastclone.find(".worktypeClone").val();
                var worktype_label = $lastclone.find(".worktype_label").val();
                var quantity = $lastclone.find(".quantity").val();
                var description =  $lastclone.find(".description").val();
                if(worktypeClone != "" && worktype_label!="" && quantity!="" && description!=""){                    
                    $tr.append($clone);
                    $($clone).insertAfter($tr);
                }else if(totalRow == rowIndex){
                    $tr.append($clone);
                    $($clone).insertAfter($tr);
                }
            }else{
                $tr.append($clone);
                $($clone).insertAfter($tr); 
            }
            
        }
    });

    function getWorkTypeLabel(workType_id,category_id, callback){        
        $.ajax({
            url: '<?php echo Yii::app()->createAbsoluteUrl('salesquotation/SalesQuotation/getWorkTypeLabelArray'); ?>',
            type: 'POST',
            dataType: 'json',
            data: {
                category_id:category_id,
                workType_id: workType_id
            },
            success: callback
        })        
    }
    
    $(document).on('change','.worktypeClone',function(){
        var el = $(this);    
        var category_id = $(this).parents(".clients-form").find("#cat_id").val(); 
        var workType_id = $(this).val();
        getWorkTypeLabel(workType_id,category_id, function(response) {  
            var incId =  el.parents('td').attr('data-incremnt');
            el.parents('td').find(".hiddenworktype").attr('id','hiddenworktype'+incId);
            el.parents('td').find(".hiddenitem").attr('id','hiddenitem'+incId);
            el.parents('td').find(".hiddentemplate").attr('id','hiddentemplate'+incId);
            el.parents('td').attr('data-id',workType_id);
            el.parents('td').attr('data-template',response.template);
            el.parents('td').find('#hiddenworktype'+incId).val(workType_id);
            el.parents('td').find('#hiddentemplate'+incId).val(response.template);
            el.parents('tr').find('td:nth-child(3)').find('select').html(response.workTypeLabelData);
            el.parents('tr').find('td:nth-child(4)').html('<input class="form-control require commonadd-val" name="shutterwork_description[]" value="" id="shutterwork_description'+incId+'" readonly="readonly" style="visibility:hidden;height:0px;" type="text" maxlength="255"><input class="form-control require" name="material[]" value="" id="material'+incId+'" type="hidden"><input class="form-control require" name="finish[]" value="" id="finish'+incId+'" type="hidden">');
            el.parents('tr').find('td:nth-child(5)').html('<input class="form-control require  commonadd-val" name="caracoss_description[]" value="" id="caracoss_description'+incId+'"  readonly="readonly" style="visibility:hidden;height:0px;" type="text" maxlength="255"><input class="form-control require" name="shutterwork_material[]" value="" id="shutterwork_material'+incId+'"  type="hidden"><input class="form-control require" name="shutterwork_finish[]" value="" id="shutterwork_finish'+incId+'"  type="hidden"><input class="form-control require" name="carcass_material[]" value="" id="carcass_material'+incId+'"  type="hidden"><input class="form-control require" name="carcass_finish[]" value="" id="carcass_finish'+incId+'"  type="hidden"> <input class="form-control require description commonadd-val" autocomplete="off" name="description[]" value="" id="description'+incId+'"  readonly="readonly" style="visibility:visible;" type="text" maxlength="255">');

            if(response.template == 1){
                el.parents('tr').find('td:nth-child(4)').html('<input class="form-control require commonadd-val" name="shutterwork_description[]" value="" id="shutterwork_description'+incId+'" readonly="readonly" style="visibility:visible" type="text" maxlength="255"><input class="form-control require" name="material[]" value="" id="material'+incId+'" type="hidden"><input class="form-control require" name="finish[]" value="" id="finish'+incId+'" type="hidden">');
                el.parents('tr').find('td:nth-child(5)').html('<input class="form-control require  commonadd-val" name="caracoss_description[]" value="" id="caracoss_description'+incId+'"  readonly="readonly" style="visibility:visible" type="text" maxlength="255"><input class="form-control require" name="shutterwork_material[]" value="" id="shutterwork_material'+incId+'"  type="hidden"><input class="form-control require" name="shutterwork_finish[]" value="" id="shutterwork_finish'+incId+'"  type="hidden"><input class="form-control require" name="carcass_material[]" value="" id="carcass_material'+incId+'"  type="hidden"><input class="form-control require" name="carcass_finish[]" value="" id="carcass_finish'+incId+'"  type="hidden"> <input class="form-control require description commonadd-val" autocomplete="off" name="description[]" value="" id="description'+incId+'"  readonly="readonly" style="visibility:hidden;height:0px;" type="text" maxlength="255">');
            }
        });
    })
    $(document).on('change', '.amountafter-discount', function () {
        var total_amount = $("#total-amount").val();
        var total_amount_val = '<?php echo $model->mainitem_total ?>'
        var status = '<?php echo $status; ?>';
        var sum_value = 0;
        $('.amountafter-discount').each(function () {
            sum_value += +$(this).val();
        })
        $('.amount-after-discount-extra').each(function () {
            sum_value += +$(this).val();
        })
        var total = sum_value;
        if (status == 0) {
            total = parseFloat(sum_value) + parseFloat(total_amount_val);
        }
        $('#total-amount-val').val(total);
        if (total_amount != '') {
            $("#sgstp").change();
            $("#cgstp").change();
            $("#igstp").change();
        }
    });
    $(document).on('change', '.amount-after-discount-extra', function () {
        var total_amount = $("#total-amount").val();
        var total_amount_val = '<?php echo $model->mainitem_total ?>'
        var status = '<?php echo $status; ?>';
        var sum_value = 0;
        $('.amountafter-discount').each(function () {
            sum_value += +$(this).val();
        })
        $('.amount-after-discount-extra').each(function () {
            sum_value += +$(this).val();
        })
        var total = sum_value;
        if (status == 0) {
            total = parseFloat(sum_value) + parseFloat(total_amount_val);
        }
        $('#total-amount-val').val(total);
        if (total_amount != '') {
            $("#sgstp").change();
            $("#cgstp").change();
            $("#igstp").change();
        }
    });
    $(document).on('change', '.sub_worktype', function () {
        var val = $(this).val();
        var cat_id = '<?php echo $category_id ?>';
        var id = $(this).attr('data-subincrement');
        if (val != '') {
            $(this).parent().siblings().find('input[type=text],select').addClass('commonadd-val');
        } else {
            $(this).parent().siblings().find('input[type=text],select').removeClass('commonadd-val');
        }
        $("#sub_worktypelabel" + id).html('<option value="">Select Label</option>');
        $.ajax({
            url: '<?php echo Yii::app()->createUrl('salesquotation/SalesQuotation/getworktypelabel'); ?>',
            method: 'POST',
            data: {
                worktype: val,
                cat_id: cat_id
            },
            dataType: "json",
            success: function (response) {
                if (response.status == 'success') {
                    $("#sub_worktypelabel" + id).html(response.labellist);
                } else {
                    $("#sub_worktypelabel" + id).html(response.labellist);
                }
            }

        })
    })

    $(function(){
        $("#sgstp").change();
        $("#cgstp").change();
        $("#igstp").change();
    })
</script>