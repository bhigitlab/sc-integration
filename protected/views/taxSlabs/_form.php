<?php
/* @var $this TaxSlabsController */
/* @var $model TaxSlabs */
/* @var $form CActiveForm */
?>
<div class="">

	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'tax-slabs-form',
		'enableAjaxValidation' => true,
		'clientOptions' => array(
			'validateOnSubmit' => true,
			'validateOnChange' => true,
			'validateOnType' => false,
		),
	)); ?>



	<div class="panel-body">
		<div class="row ">

			<div class="col-md-4">
				<?php echo $form->labelEx($model, 'tax_slab_value'); ?>
				<?php echo $form->textField($model, 'tax_slab_value', array('class' => 'form-control')); ?>
				<?php echo $form->error($model, 'tax_slab_value'); ?>
			</div>
			<div class="col-md-2">
				<?php echo $form->labelEx($model, 'status'); ?>
				<?php
				echo $form->dropDownList($model, 'status', $status_values, array('class' => 'form-control'));
				?>

				<?php echo $form->error($model, 'status'); ?>

			</div>
			<div class="col-md-2">
				<label class="d-block">&nbsp;</label>
				<?php echo $form->checkBox($model, 'set_default', array('value' => '1', 'uncheckValue' => '0')); ?>
				<span>set as default</span>
				<?php echo $form->error($model, 'set_default'); ?>
			</div>
			<!-- <div class="col-md-4">
				<?php echo $form->labelEx($model, 'cgst'); ?>
				<?php echo $form->textField($model, 'cgst', array('class' => 'form-control')); ?>
				<?php echo $form->error($model, 'cgst'); ?>
			</div>
			<div class="col-md-4">
				<?php echo $form->labelEx($model, 'sgst'); ?>
				<?php echo $form->textField($model, 'sgst', array('class' => 'form-control')); ?>
				<?php echo $form->error($model, 'sgst'); ?>
			</div> -->
		</div>
		<div class="row ">

			<!-- <div class="col-md-4">
				<?php echo $form->labelEx($model, 'igst'); ?>
				<?php echo $form->textField($model, 'igst', array('class' => 'form-control')); ?>
				<?php echo $form->error($model, 'igst'); ?>
			</div> -->


			<div class="col-md-4 save-btnHold">
				<label style="display:block;">&nbsp;</label>
				<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-info btn-sm')); ?>
				<?php if (Yii::app()->user->role == 1) {


					if (!$model->isNewRecord) {
						echo CHtml::ResetButton('Close', array('onclick' => 'closeaction(this,event);', 'class' => 'btn btn-sm'));
					}
				} else {
					echo CHtml::ResetButton('Reset', array('style' => 'margin-right:3px;', 'class' => 'btn btn-default btn-sm'));
					echo CHtml::ResetButton('Close', array('onclick' => 'closeaction(this,event);', 'class' => 'btn btn-sm'));
				}
				?>
			</div>

		</div>

	</div><!-- form -->

	<?php $this->endWidget(); ?>
</div>