
<?php
/* @var $this ProjectsController */
/* @var $model Projects */

$this->breadcrumbs=array(
	'Po Companies'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);
/*
$this->menu=array(
	array('label'=>'List Projects', 'url'=>array('index')),
	array('label'=>'Create Projects', 'url'=>array('create')),
	array('label'=>'View Projects', 'url'=>array('view', 'id'=>$model->pid)),
	array('label'=>'Manage Projects', 'url'=>array('admin')),
); */
?>
<!--<div class="modal-dialog modal-md" id="projectform">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" onclick="javascript:window.location.reload()">&times;</button>
			<h4 class="modal-title">Edit Company</h4>
		</div>
		<?php //echo $this->renderPartial('_form', array('model'=>$model)); ?>
	</div>
	
</div>-->
<div class="panel panel-gray">
    <div class="panel-heading form-head">
        <h3 class="panel-title">Edit Company</h3>
    </div>
    <?php echo $this->renderPartial('_form', array('model' => $model)); ?>
</div>
