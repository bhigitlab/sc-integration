<?php
/* @var $this SubcontractorbillController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Subcontractorbills',
);

$this->menu=array(
	array('label'=>'Create Subcontractorbill', 'url'=>array('create')),
	array('label'=>'Manage Subcontractorbill', 'url'=>array('admin')),
);
?>

<h1>Subcontractorbills</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
