<?php
/* @var $this WarehousedespatchController */
/* @var $model Warehousedespatch */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'warehousedespatch_id'); ?>
		<?php echo $form->textField($model,'warehousedespatch_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'warehousedespatch_no'); ?>
		<?php echo $form->textField($model,'warehousedespatch_no',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'warehousedespatch_date'); ?>
		<?php echo $form->textField($model,'warehousedespatch_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'warehousedespatch_vendor'); ?>
		<?php echo $form->textField($model,'warehousedespatch_vendor',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'warehousedespatch_warehouseid'); ?>
		<?php echo $form->textField($model,'warehousedespatch_warehouseid'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'warehousedespatch_clerk'); ?>
		<?php echo $form->textField($model,'warehousedespatch_clerk'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'warehousedespatch_quantity'); ?>
		<?php echo $form->textField($model,'warehousedespatch_quantity'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'warehousedespatch_through'); ?>
		<?php echo $form->textField($model,'warehousedespatch_through',array('size'=>60,'maxlength'=>200)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'company_id'); ?>
		<?php echo $form->textField($model,'company_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created_by'); ?>
		<?php echo $form->textField($model,'created_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created_date'); ?>
		<?php echo $form->textField($model,'created_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'updated_by'); ?>
		<?php echo $form->textField($model,'updated_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'updated_date'); ?>
		<?php echo $form->textField($model,'updated_date'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->