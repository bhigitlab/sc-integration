
<div class="panel panel-gray" id="generalform">
  <div class="panel-heading">
    <div class="panel-titl">Media Upload Files</div>
  </div>
  <?php 
$form = $this->beginWidget('CActiveForm', array(
    'method'=>'post',
    'action' => Yii::app()->createAbsoluteUrl("dashboard/mediaUploadFiles"),
    'htmlOptions' => array('class' => 'form-inline', 'enctype' => 'multipart/form-data'),      
    ));
    $model = new Company();
    $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        $newQuery = "";
        foreach($arrVal as $arr) {
            if ($newQuery) $newQuery .= ' OR';
            $newQuery .= " FIND_IN_SET('".$arr."', id)";
        }
        $companyInfo = Company::model()->findAll(array('condition' => $newQuery));
 ?>
  <div class="panel-body">


<div class="row">
    
        <div class="col-md-6">
        <label>Media File:</label>
            <?php              
              $this->widget('CMultiFileUpload', array(
                'name'=>'media_image',
                'attribute'=>'image',
                'accept'=>'jpg,png',
                'max'=>10,              
              'duplicate'=>'Already Selected',
              ));
              
             ?>
                
          </div>
          <div class="col-md-6">
    
          </div>
          
              </div>
      </div>
      <div class="col-md-6">
        
        </div>
      <div class="panel-footer text-right">
      <?php echo CHtml::submitButton('Upload',array('class'=>'btn btn-primary btn-sm')); ?>
      </div>
      <?php $this->endWidget(); ?>
  </div>
  <div>

  </div>
</div>

<script>
  $(document).on("click",".remove_file",function(){
    var answer = confirm("Are you sure you want to delete this file?");
    if (answer) {
      var id= $(this).attr("id");
      $.ajax({
        type: "POST",
        data: {
          "id": id,
        },
        url: "<?php echo $this->createUrl('dashboard/removeFile') ?>",
        success: function (response)
        {
          setTimeout(function() {
              location.reload();
          }, 2000);
        }
      });
    }
  })

  </script>
