<?php
/* @var $this ExpenseTypeController */
/* @var $model ExpenseType */
/* @var $form CActiveForm */
$pms_api_integration=ApiSettings::model()->pmsIntegrationStatus();
?>

<div class="">
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'expense-type-form',
        'enableAjaxValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true,
            'validateOnType' => false,
        ),
    ));
    ?>

    <div class="panel-body">
        <div class="row ">
        <input type="hidden" id="pms_integration" value="<?php echo $pms_api_integration ;?>">
            <div class="col-md-4">
                <div class="form-group">
                    <?php echo $form->labelEx($model, 'worktype'); ?>
                    <?php echo $form->textField($model, 'worktype', array('size' => 30, 'maxlength' => 30, 'class' => 'form-control')); ?>
                    <?php echo $form->error($model, 'worktype'); ?>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <?php echo $form->labelEx($model, 'rate'); ?>
                    <?php echo $form->textField($model, 'rate', array('class' => 'form-control text_field', 'size' => 30, 'maxlength' => 30)); ?>
                    <?php echo $form->error($model, 'rate'); ?>
                </div>
            </div>
             <div class="col-md-4">
                <div class="form-group">
                    <?php echo $form->labelEx($model, 'short_keyword'); ?>
                    <?php echo $form->textField($model, 'short_keyword', array('class' => 'form-control text_field', 'size' => 30, 'maxlength' => 30)); ?>
                    <?php echo $form->error($model, 'short_keyword'); ?>
                </div>
            </div>
            <div class="col-md-4 save-btnHold">
                <label style="display:block;">&nbsp;</label>
                <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-info btn-sm submit')); ?>
                <?php if (Yii::app()->user->role == 1) {
                    if (!$model->isNewRecord) { 
                    }
                } ?>
                
                <?php
                if (!$model->isNewRecord) {
                    echo CHtml::ResetButton('Close', array('onclick' => 'closeaction(this,event);', 'class' => 'btn btn-sm'));
                } else {
                    echo CHtml::ResetButton('Reset', array('style' => 'margin-right:3px;', 'class' => 'btn btn-default btn-sm'));
                    echo CHtml::ResetButton('Close', array('onclick' => 'closeaction(this,event);', 'class' => 'btn btn-sm'));
                }
                echo CHtml::hiddenField('execute_api', 0);
                ?>
                 <?php echo $form->hiddenField($model, 'pms_labour_id'); ?>
            </div>
            
        </div>
         <div class="row">
            
        </div>

    </div><!-- form -->
    <?php echo $form->hiddenField($model, 'pms_labour_id'); ?>
    <?php $this->endWidget(); ?>
</div>



<script>
    $(document).ready(function() {
        var type_val = $('#ExpenseType_template_type').val();
        templateValues(type_val);
        $("#select_all").change(function() { 
            var status = this.checked; 
            $('.checkbox').each(function() { 
                this.checked = status;
            });
        });

        $('.checkbox').change(function() { 
            
            if (this.checked == false) { 
                $("#select_all")[0].checked = false;
            }

            if ($('.checkbox:checked').length == $('.checkbox').length) {
                $("#select_all")[0].checked = true;
            }
        });

        $('#ExpenseType_template_type').change(function() {
            var type_val = $(this).val();
            templateValues(type_val);
        });

        function templateValues(type_val) {
            $('.section_class').addClass('hide');
            if (type_val === '1') {
                $('.labour_label,.wage_label,.wage_rate_label').removeClass('hide');
            } else if (type_val === '2') {
                $('.labour_label,.wage_rate_label,.helper_label,.helper_labour_label,.labour_wage_label,.helper_wage_label').removeClass('hide');
            } else if (type_val === '3') {
                $('.labour_label,.wage_rate_label,.helper_label,.wage_label').removeClass('hide');
            } else if (type_val === '4') {
                $('.lump_sum_label').removeClass('hide');
            } else {
                $('.section_class').addClass('hide');
            }
        }
    });
</script>

<script>
    $('.text_field[type="text"]').change(function() {
        if ($(this).val() != '') {
            $(this).closest('.section_class').find('input[type=checkbox]').prop('checked', true);
        }
    }).change()

    $('input[type="checkbox"]').change(function() {
        if (!this.checked) {
            $(this).closest('.section_class').find('input[type=text]').val('');
        }
    }).change()

    $("#ExpenseType_expense_type").change(function(){
        var type_val =$(this).val();
        if(type_val == 96){
            $('#ExpenseType_template_type').attr('disabled',true);
            $('#ExpenseType_template_type').siblings('.errorMessage').hide();
            $('#ExpenseType_template_type').siblings('label').removeClass('required');
            $('#ExpenseType_template_type').parent().removeClass('error');
        }else{
            $('#ExpenseType_template_type').attr('disabled',false);
        }
    })
    
    function countOfLetters(str) {
            var letter = 0;
            for (i = 0; i < str.length; i++) {
                if ((str[i] >= 'A' && str[i] <= 'Z')
                    || (str[i] >= 'a' && str[i] <= 'z'))
                    letter++;
            }
            
            return letter;
        }
      

    $("#ExpenseType_type_name").keyup(function () {         
        var alphabetCount =  countOfLetters(this.value);  
        var message ="";      
        var disabled=false;

        if(alphabetCount < 1){
            var message = "Invalid Type Name";
            var disabled=true;           
        } 

        $(this).siblings(".errorMessage").show().html(message).addClass('d-block');                
        $(".submit").attr('disabled',disabled);
    });
    $(document).ready(function() {
        document.getElementById('expense-type-form').onsubmit = function() {
        var pms_integration = $("#pms_integration").val();
        var pmsLabourId = $('#LabourWorktype_pms_labour_id').val();
        if(pms_integration =='1'){
            if(pmsLabourId=='0'){
                var confirmApiCall = confirm("Do you want to save the labour in Pms?");
                if (confirmApiCall) {
                    document.getElementById('execute_api').value = 1;
                }
            }
            else if (pmsLabourId!='0') {
                    document.getElementById('execute_api').value = 1;
                }
       }
    };
    });
</script>


<style>
    .popermission {
        margin-top: 30px !important;
    }
</style>