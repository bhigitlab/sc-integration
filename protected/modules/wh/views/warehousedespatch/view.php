<?php
$tblpx = Yii::app()->db->tablePrefix;
$warehouse = Warehouse::model()->findByPK($model->warehousedespatch_warehouseid); //Yii::app()->user->company_id;

$project =$warehouse['project_id'];
$project_det =Projects::model()->findByPk($project);
$company='';
if(!empty($project_det)){
	$company_id_arr =$project_det['company_id'];
	if(!empty($company_id_arr)){
		$company_id_arr=explode(',',$company_id_arr);
		$company=$company_id_arr[0];
	}
}

if(!empty($company)){
	$companyInfo = Company::model()->findByPK($company);
}else{
	$company = Yii::app()->user->company_id;
	$companyInfo = Company::model()->findByPK($company);
	
}



$companyName = $companyInfo["name"];
$companyGst = $companyInfo["company_gstnum"];
$item_model = WarehousedespatchItems::model()->findAll(array("condition" => "warehousedespatch_id = '$model->warehousedespatch_id'"));
$totalAmount = 0;
?>
<!DOCTYPE html>
<html>

<head>
	<title>Despatch Pdf</title>
	<link href="https://fonts.googleapis.com/css?family=Diplomata+SC|Limelight" rel="stylesheet">
	<style>
		table {
			width: 100%;
			border-collapse: collapse;
		}

		table td,
		table th {
			padding: 6px 0px;
		}

		.item_table td,
		.item_table th {
			border: 1px solid #ccc;
			padding: 8px;
			text-align: center;
		}

		table tbody tr:last-child {
			border-bottom: 0px;
		}

		table tbody tr td:nth-child(2) {
			border-left: 0px;
		}

		h2 {
			margin: 0px;
			/* font-family: 'Limelight', cursive; */
			padding: 0px;
		}

		td.text-right {
			text-align: right;
		}

		h2,
		h4 {
			text-align: center;
			text-transform: uppercase;
		}

		td.text-uppercase {
			text-transform: uppercase;
		}

		b.receipt_text {
			color: red;
			font-size: 18px;
		}

		.text-sm {
			font-size: 12px;
		}
	</style>
</head>

<body>

	<div class="container">
		<div class="clearfix">
			<div class="add-btn pull-right">
				<?php if (!filter_input(INPUT_GET, 'export')) { ?>
					<?php echo CHtml::link('<i class="fa fa-file-pdf-o button-icon" aria-hidden="true"></i>', Yii::app()->request->requestUri . "&export=pdf", array('class' => 'save_btn btn btn-info','title'=>'SAVE AS PDF')); ?>
					<?php echo CHtml::link('<i class="fa fa-file-excel-o button-icon" aria-hidden="true"></i>', Yii::app()->request->requestUri . "&export=csv", array('class' => 'save_btn btn btn-info','title'=>'SAVE AS EXCEL')); ?>
				<?php } ?>
			</div>
		</div>
		<table>
			<tr>
				<td>GSTIN:<b>
						<?php echo $companyGst; ?>
					</b></td>
				<td class="text-right">Phone:<b>
						<?php echo $companyInfo['phone']; ?>
					</b></td>
			</tr>
		</table>
		<div style="text-transform:uppercase;text-align:center;">
			<h2>
				<?php echo $companyName; ?>
			</h2>
			<h4 style="margin:0px;">
				<?php echo $warehouse_address; ?>
			</h4>
		</div>
		<table>
			<tr>
				<td>NO.<b style="color: red;ont-size: 18px;">
						<?php echo $model->warehousedespatch_no ?>
					</b>
					<br /><b> Despatch From: </b> <?php echo $model->warehouse['warehouse_name']; ?>
					<br /><b> Clerk: </b> <?php echo $model->clerk['first_name'] . ' ' . $model->clerk['last_name']; ?>
				</td>
				<td class="text-right">
					Date:<b>
						<?php echo date("d-m-Y", strtotime($model->warehousedespatch_date)); ?>
					</b>
					<?php if ($model->warehousedespatch_through != "") { ?><br />Through:<b>
							<?php echo $model->warehousedespatch_through ?>
						</b><?php } ?>
					<br />ETA:<b>
						<?php echo isset($model->warehouse_eta_date) ? date("d-m-Y", strtotime($model->warehouse_eta_date)) : '-'; ?>
					</b>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<h4 style="text-transform:uppercase;margin-bottom:5px;margin-top:0px;font-weight:bold">Despatch
						Note:</h4>Stock of Goods despatched to: <b>
						<?php echo $model->warehousedespatchWarehouseidTo['warehouse_name']; ?>
					</b>
				</td>
			</tr>
		</table>
		<table class="item_table">
			<thead class="entry-table">
				<tr>
					<th style="width: 40px;">Sl No</th>
					<th>Description</th>
					<th style="width: 100px;">Dimension</th>
					<th style="width: 100px;">Batch</th>
					<th style="width: 100px;">Unit/Size</th>
					<th style="width: 100px;">No of pieces</th>
					<th style="width: 100px;">Base Unit</th>
					<th style="width: 100px;">Base Quantity</th>
					<th style="width: 100px;">Base Rate</th>
					<th style="width: 100px;">Base Amount</th>
					<th style="width: 150px;">Remarks</th>
				</tr>
			</thead>
			<?php
			if (!empty($item_model)) {
				foreach ($item_model as $key => $values) {
					$spec_sql = "SELECT id, cat_id,brand_id, specification, unit "
						. " FROM {$tblpx}specification "
						. " WHERE id=" . $values['warehousedespatch_itemid'] . "";
					$specification = Yii::app()->db->createCommand($spec_sql)->queryRow();
					$parent_sql = "SELECT * FROM {$tblpx}purchase_category "
						. " WHERE id='" . $specification['cat_id'] . "'";
					$parent_category = Yii::app()->db->createCommand($parent_sql)->queryRow();

					if ($specification['brand_id'] != NULL) {
						$brand_details = Yii::app()->db->createCommand("SELECT brand_name FROM {$tblpx}brand WHERE id=" . $specification['brand_id'] . "")->queryRow();
						$brand = '-' . $brand_details['brand_name'];
					} else {
						$brand = '';
					}
					$spc_details = $parent_category['category_name'] . $brand . ' - ' . $specification['specification'];

					$base_unit = Controller::GetStockItemunit($values['warehousedespatch_itemid']);
					if ($base_unit != null) {
						$basewarehouse_unit = $base_unit;
					} else {
						$basewarehouse_unit = $values['warehousedespatch_unit'];
					}
					$warehousereceiptitem_unit_id = $values['warehousedespatch_unit'];
					$warehouse_qty = $values['warehousedespatch_quantity'];
					$warehouse_unit = $values['warehousedespatch_unit'];
					if ($basewarehouse_unit != $warehousereceiptitem_unit_id) {
						if ($values['warehousedespatch_baseunit'] != '') {
							$baseunit = $values['warehousedespatch_baseunit'];
						} else {
							$baseunit = $values['warehousedespatch_unit'];
						}

						if ($values['warehousedespatch_baseqty'] > 0) {
							$baseqty = $values['warehousedespatch_baseqty'];
						} else {
							$baseqty = $values['warehousedespatch_quantity'];
						}
						if ($values['warehousedespatch_baserate'] > 0) {
							$baserate = $values['warehousedespatch_baserate'];
						} else {
							$baserate = $values['warehousedespatch_rate'];
						}
					} else {
						$baseunit = $values['warehousedespatch_unit'];
						$baseqty = $values['warehousedespatch_quantity'];
						$baserate = $values['warehousedespatch_rate'];
					}
					$totalAmount += $values['warehousedespatch_amount']
						?>
					<tr>
						<td>
							<?php echo $key + 1; ?>
						</td>
						<td style="text-align:justify;">
							<?php echo $spc_details; ?>
						</td>
						<td>
							<?php echo ($values['dimension'] != '') ? "(" . $values['dimension'] . ")" : "N/A"; ?>
							<input type="hidden" id="warehousestock_itemid_dimension_category"
								name="warehousestock_itemid_dimension_category"
								value="<?php $values['warehousestock_itemid_dimension_category']; ?>">
						</td>
						<td>
							<?php echo $values['warehousedespatch_batch']; ?>
						</td>
						<td>
							<?php echo $warehouse_unit; ?>
						</td>
						<td>
							<?php echo $warehouse_qty; ?>
						</td>
						<td>
							<?php echo $baseunit; ?>
						</td>
						<td>
							<?php echo $baseqty; ?>
						</td>
						<td>
							<?php echo $baserate; ?>
						</td>
						<td>
							<?php echo $values['warehousedespatch_amount']; ?>
						</td>
						<td>
							<?php echo $values['warehousedespatch_remarks']; ?>
						</td>
					</tr>
				<?php } ?>
			<?php } else { ?>
				<tr>
					<td colspan="5">No Item found</td>
				</tr>
			<?php } ?>
			<tfoot class="entry-table">
				<tr>
					<th colspan="9" style="text-align: right;">Total Amount:</th>
					<th>
						<?php echo $totalAmount . '.' . '00'; ?>
					</th>
					<th></th>
				</tr>
			</tfoot>
		</table><br><br>
		<table>
			<tr>

				<td class="text-sm" colspan="2">Dispatch Clerk :<b class="text-uppercase text-sm">
						<?php echo $model->clerk->first_name . ' ' . $model->clerk->last_name ?>
					</b></td>
				<td colspan="3" class="text-right text-sm">For <b class="text-uppercase text-sm">
						<?php echo $companyName; ?>
					</b></td>
			</tr>
		</table>

	</div>

</body>

</html>