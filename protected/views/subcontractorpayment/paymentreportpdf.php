<?php
$tblpx = Yii::app()->db->tablePrefix;
$newQuery1 = "";
$newQuery2 = "";
$user = Users::model()->findByPk(Yii::app()->user->id);
$arrVal = explode(',', $user->company_id);
foreach($arrVal as $arr) {
    if ($newQuery1) $newQuery1 .= ' OR';
    if ($newQuery2) $newQuery2 .= ' OR';
    $newQuery1 .= " FIND_IN_SET('".$arr."', {$tblpx}scquotation.company_id)";
    $newQuery2 .= " FIND_IN_SET('".$arr."', {$tblpx}subcontractor_payment.company_id)";
}
?>
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/tablestyles.css" />
<link rel="stylesheet" type="text/css" href="uploads/new_template/css/newstyle.css" />
<style>
    table {
        border-collapse: collapse;
        width: 100%;
    }
    /*.container{max-width: 1050px;margin:0 auto;padding: 15px;border: 1px solid #ddd;}*/
    .text-right{text-align:right}
    .details,.info-table{border: 1px solid #212121;margin-top: 20px;}
    .details td, .info-table td,.info-table th{padding: 6px 8px; }
    .info-table{margin-bottom: 10px;}
    .text-center{text-align:center;}
    .img-hold{width: 10%;}
    .companyhead{font-size: 18px;font-weight:bold;margin-top:0px;color: #789AD1;}
</style>
<?php
    $company_address = Company::model()->findBypk(($company_id)?$company_id:Yii::app()->user->company_id);	
?>
    <table border=0>
        <tbody>
                <tr>
                    <td class="img-hold"><img  src="<?php echo Yii::app()->theme->baseUrl; ?>/images/logo.png" alt="img" style='margin-top:-45px;'/></td>
                    <td class=text-right>GST NO: <?php echo isset($company_address["company_gstnum"])?$company_address["company_gstnum"]:''; ?></td>
                </tr>
        </tbody>
    </table>
    <table border=0>
        <tbody>
            <tr>
                <td class="text-center"><p class='companyhead'><?php echo isset($company_address['name'])?$company_address['name']:''; ?></p><br>
                    <p><?php echo ( (isset($company_address['address']) && !empty($company_address['address'])) ? ($company_address['address']) : ""); ?><br>
                    PIN : <?php echo ( (isset($company_address['pincode']) && !empty($company_address['pincode'])) ?($company_address['pincode']) : ""); ?>,&nbsp;
                    PHONE : <?php echo ( (isset($company_address['phone']) && !empty($company_address['phone'])) ?( $company_address['phone']) : ""); ?><br>
                    EMAIL : <?php echo ( (isset($company_address['email_id']) && !empty($company_address['email_id'])) ?( $company_address['email_id']) : ""); ?></p><br>                        
                    <h3>Subcontractor Payment</h3>   
                </td>
            </tr>
        </tbody>
    </table>
<div class="container-fluid">
            
            <table class="table table-bordered table-striped" border="1">

                <thead>
                    <tr>
                        <th>Sl No.</th>
                        <th>Subcontractor</th>
                        <th>Project</th>
                        <th>Quotation Amount</th>
                        <th>Payment Done</th>
                        <th>Balance</th>
                    </tr>
                </thead>
                 <tbody>
                 <?php
                  $tblpx = Yii::app()->db->tablePrefix;

                 /*************************** */
                $final_amount = 0;
                if(!empty($scquotation)) {
                    foreach ($scquotation as $data){
                      $i++;
                     
                     $scquotation_amount    = Yii::app()->db->createCommand("SELECT SUM( IFNULL(".$tblpx.
                     "scquotation_items.item_amount,0)) as total_amount FROM ".$tblpx."scquotation LEFT JOIN ".
                     $tblpx."scquotation_items ON ".$tblpx."scquotation.scquotation_id = ".$tblpx."scquotation_items.
                     scquotation_id WHERE ".$tblpx."scquotation.project_id= ".$data['project_id']." AND 
                     ".$tblpx."scquotation.subcontractor_id=".$data['subcontractor_id']." AND "
                     .$tblpx."scquotation_items.approve_status = 'Yes' AND (".$newQuery1.")")->queryRow();


                   // $payment_amount = Yii::app()->db->createCommand("SELECT SUM(amount) as total_amount 
                   // FROM {$tblpx}subcontractor_payment INNER JOIN {$tblpx}subcontractor ON {$tblpx}subcontractor_payment.
                   // subcontractor_id= {$tblpx}subcontractor.subcontractor_id LEFT JOIN {$tblpx}projects ON 
                   // {$tblpx}subcontractor_payment.project_id= {$tblpx}projects.pid WHERE {$tblpx}subcontractor_payment.
                   // subcontractor_id = ".$data['subcontractor_id']." AND
                   //  {$tblpx}subcontractor_payment.project_id=".$data['project_id']." AND (".$newQuery2.") ")->queryRow();

                   $payment_amount = Yii::app()->db->createCommand("SELECT SUM(IFNULL(amount, 0) +
                   IFNULL(tax_amount, 0)) as total_amount FROM {$tblpx}subcontractor_payment INNER 
                   JOIN {$tblpx}subcontractor ON {$tblpx}subcontractor_payment.subcontractor_id= 
                   {$tblpx}subcontractor.subcontractor_id LEFT JOIN {$tblpx}projects 
                   ON {$tblpx}subcontractor_payment.project_id= {$tblpx}projects.pid 
                   WHERE {$tblpx}subcontractor_payment.subcontractor_id = " . $data['subcontractor_id'] . "
                   AND {$tblpx}subcontractor_payment.project_id=" . $data['project_id'] . " 
                   AND {$tblpx}subcontractor_payment.approve_status ='Yes' AND (".$newQuery2.") ")->queryRow();

                   $balance = $scquotation_amount['total_amount']-$payment_amount['total_amount'];
                   $final_amount += 	$balance;
                    }
                }
                ?>
                 <tr>
                         <td colspan="5" align="right"><b>Total</b></td>
                         <td align="right"><?php echo ($final_amount !='')?Controller::money_format_inr($final_amount,2,1):'0.00'; ?></td>
                     </tr>
                <?php 
                /**************************************** */
               
                 $i=0;$totalcredit=0;$totaldebit=0;
                 $final_amount = 0;
                   if(!empty($scquotation)) {
				     foreach ($scquotation as $data){
					   $i++;
					  
                      $scquotation_amount    = Yii::app()->db->createCommand("SELECT SUM( IFNULL(".$tblpx.
                      "scquotation_items.item_amount,0)) as total_amount FROM ".$tblpx."scquotation LEFT JOIN ".
                      $tblpx."scquotation_items ON ".$tblpx."scquotation.scquotation_id = ".$tblpx."scquotation_items.
                      scquotation_id WHERE ".$tblpx."scquotation.project_id= ".$data['project_id']." AND 
                      ".$tblpx."scquotation.subcontractor_id=".$data['subcontractor_id']." AND "
                      .$tblpx."scquotation_items.approve_status = 'Yes' AND (".$newQuery1.")")->queryRow();


                    // $payment_amount = Yii::app()->db->createCommand("SELECT SUM(amount) as total_amount 
                    // FROM {$tblpx}subcontractor_payment INNER JOIN {$tblpx}subcontractor ON {$tblpx}subcontractor_payment.
                    // subcontractor_id= {$tblpx}subcontractor.subcontractor_id LEFT JOIN {$tblpx}projects ON 
                    // {$tblpx}subcontractor_payment.project_id= {$tblpx}projects.pid WHERE {$tblpx}subcontractor_payment.
                    // subcontractor_id = ".$data['subcontractor_id']." AND
                    //  {$tblpx}subcontractor_payment.project_id=".$data['project_id']." AND (".$newQuery2.") ")->queryRow();

                    $payment_amount = Yii::app()->db->createCommand("SELECT SUM(IFNULL(amount, 0) +
                    IFNULL(tax_amount, 0)) as total_amount FROM {$tblpx}subcontractor_payment INNER 
                    JOIN {$tblpx}subcontractor ON {$tblpx}subcontractor_payment.subcontractor_id= 
                    {$tblpx}subcontractor.subcontractor_id LEFT JOIN {$tblpx}projects 
                    ON {$tblpx}subcontractor_payment.project_id= {$tblpx}projects.pid 
                    WHERE {$tblpx}subcontractor_payment.subcontractor_id = " . $data['subcontractor_id'] . "
                    AND {$tblpx}subcontractor_payment.project_id=" . $data['project_id'] . " 
                    AND {$tblpx}subcontractor_payment.approve_status ='Yes' AND (".$newQuery2.") ")->queryRow();

                    $balance = $scquotation_amount['total_amount']-$payment_amount['total_amount'];
					$final_amount += 	$balance;
                   ?>
                     <tr>
                        <td><?php echo $i; ?></td>
                         <td><?php echo $data['subcontractor_name']; ?></td>
                         <td><?php echo $data['name']; ?></td>
                          <td  align="right"><?php echo ($scquotation_amount['total_amount'] != '')?Controller::money_format_inr($scquotation_amount['total_amount'],2,1):'0.00'; ?></td>
                         <td  align="right"><?php echo ($payment_amount['total_amount'] !='')?Controller::money_format_inr($payment_amount['total_amount'],2,1):'0.00'; ?></td>
                         <td  align="right"><?php echo ($balance !='')?Controller::money_format_inr($balance,2,1):'0.00'; ?></td>
                     </tr>
                    
                
               <?php } }?>
                     
                 </tbody>

            </table>

        </div>
