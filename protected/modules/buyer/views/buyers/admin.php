<?php
/* @var $this ExpensesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
	'Buyers',
);

?>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<div class="container" id="project">
	<div class="clearfix">
		<div class="add-btn pull-right">
			<?php
			if (isset(Yii::app()->user->role) && (in_array('/buyer/buyers/create', Yii::app()->user->menuauthlist))) {
			?>
				<button class="createbuyer btn btn-primary">Add Buyer</a>
				<?php } ?>
		</div>
		<h2 class="pull-left">Buyers</h2>
	</div>
	<div id="buyerform" style="display:none;"></div>
	<div class="">
		<?php
		$this->widget('zii.widgets.CListView', array(
			'dataProvider' => $dataProvider,
			'itemView' => '_view', 'template' => '<div class=""><table cellpadding="10" class="table" id="buyertab">{items}</table></div>',
			'ajaxUpdate' => false,
		));
		?>

		<?php
		Yii::app()->clientScript->registerScript('myjavascript', '
             $(document).ready(function(){
               $("#buyertab").dataTable( {
                    "scrollY": "300px",
                    "scrollCollapse": true,
                    "paging": false,
                     "columnDefs"        : [       
                        { 
                            "searchable"    : false, 
                            "targets"       : [0,3] 
                        },
                        { "bSortable": false, "aTargets": [-1]  }
                    ],
                } );
                });

            ');
		?>

		<script>
			jQuery(function($) {
				$('#client').on('keydown', function(event) {
					if (event.keyCode == 13) {
						$("#clientsearch").submit();
					}
				});
			});

			function closeaction() {
				$('#buyerform').slideUp();
			}

			function editclient(client_id) {
				var id = client_id;
				$.ajax({
					type: "GET",
					url: "<?php echo $this->createUrl('/buyer/buyers/update&layout=1&id=') ?>" + id,
					success: function(response) {
						$("#buyerform").html(response).slideDown();

					}
				});

			};
			$(document).ready(function() {
				$('.createbuyer').click(function() {
					$.ajax({
						type: "GET",
						url: "<?php echo $this->createUrl('/buyer/buyers/create&layout=1') ?>",
						success: function(response) {
							$("#buyerform").html(response).slideDown();
							var role = "<?php echo Yii::app()->user->role; ?>";
							if (role == 3)
								$("#Clients_company_id_all").click();
						}
					});
				});
			});
		</script>



	</div>
</div>



<style>
	.page-body h3 {
		margin: 4px 0px;
		color: inherit;
		text-align: left;
	}

	.panel {
		border: 1px solid #ddd;
	}

	.panel-heading {
		background-color: #eee;
		height: 40px;
	}

	input[type="checkbox"] {
		margin: 11px 3px 0;
	}
</style>