<?php
/* @var $this EquipmentsController */
/* @var $model Equipments */

$this->breadcrumbs=array(
	'Equipments'=>array('index'),
	'Create',
);

?>

<div class="panel panel-gray">
<div class="panel-heading form-head">
        <h3 class="panel-title">Add Equipment</h3>
    </div>
        <?php echo $this->renderPartial('_form', array('model' => $model)); ?>
</div>
