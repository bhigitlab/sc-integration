ALTER TABLE jp_projects ADD mapped_status INT(11) NOT NULL DEFAULT '0' AFTER incharge;
ALTER TABLE jp_clients ADD mapped_status INT(11) NOT NULL DEFAULT '0' AFTER company_id;
ALTER TABLE jp_unit ADD mapped_status INT(11) NOT NULL DEFAULT '0' AFTER company_id;
ALTER TABLE jp_equipments ADD mapped_status INT(11) NOT NULL DEFAULT '0' AFTER equipment_unit;
ALTER TABLE jp_labours ADD mapped_status INT(11) NOT NULL DEFAULT '0' AFTER labour_rate;
ALTER TABLE jp_materials ADD mapped_status INT(11) NOT NULL DEFAULT '0' AFTER material_unit;


CREATE TABLE `jp_api_materials` (`id` INT NOT NULL AUTO_INCREMENT , `material_name` VARCHAR(255) NULL , `template_material_id` INT NULL , `material_id` INT NULL , `project_id` INT NULL , `unit` VARCHAR(255) NULL , `count` INT NULL , `created_at` DATETIME NULL DEFAULT CURRENT_TIMESTAMP , `updated_at` DATETIME NULL DEFAULT CURRENT_TIMESTAMP , PRIMARY KEY (`id`)) ENGINE = InnoDB;

CREATE TABLE `jp_api_labours` (`id` INT NOT NULL AUTO_INCREMENT , `labour_name` VARCHAR(255) NULL , `template_labour_id` INT NULL , `labour_id` INT NULL , `project_id` INT NULL , `unit` VARCHAR(255) NULL , `count` INT NULL , `created_at` DATETIME NULL DEFAULT CURRENT_TIMESTAMP , `updated_at` DATETIME NULL DEFAULT CURRENT_TIMESTAMP , PRIMARY KEY (`id`)) ENGINE = InnoDB;

CREATE TABLE `jp_api_equipments` (`id` INT NOT NULL AUTO_INCREMENT , `equipment_name` VARCHAR(255) NULL , `template_equipment_id` INT NULL , `equipment_id` INT NULL , `project_id` INT NULL , `unit` VARCHAR(255) NULL , `count` INT NULL , `created_at` DATETIME NULL DEFAULT CURRENT_TIMESTAMP , `updated_at` DATETIME NULL DEFAULT CURRENT_TIMESTAMP , PRIMARY KEY (`id`)) ENGINE = InnoDB;

CREATE TABLE `jp_api_settings` (`id` INT NOT NULL AUTO_INCREMENT , `api_integration_settings` ENUM('0','1') NOT NULL DEFAULT '0' , `created_at` DATETIME NULL DEFAULT CURRENT_TIMESTAMP , `updated_at` DATETIME NULL DEFAULT CURRENT_TIMESTAMP , PRIMARY KEY (`id`)) ENGINE = InnoDB;
INSERT INTO `jp_api_settings` (`id`, `api_integration_settings`, `created_at`, `updated_at`) VALUES (1, '0', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
ALTER TABLE `pms_material_requisition`
DROP FOREIGN KEY `pms_material_requisition_ibfk_1`;
ALTER TABLE `pms_material_requisition`
DROP FOREIGN KEY `pms_material_requisition_ibfk_2`;
ALTER TABLE `pms_material_requisition` DROP INDEX `requisition_project_id`;
ALTER TABLE `pms_material_requisition` DROP INDEX `requisition_task_id`;

ALTER TABLE jp_projects ADD pms_project_id INT(11) NULL DEFAULT NULL COMMENT 'this field is used for api integration' AFTER labour_template;
ALTER TABLE pms_material_requisition ADD pms_mr_id INT(11) NULL DEFAULT NULL COMMENT 'Api integration field' AFTER bill_id;

CREATE TABLE jp_api_log (
  id int(11) NOT NULL,
  request text NOT NULL,
  slug varchar(200) NOT NULL,
  method varchar(11) DEFAULT NULL,
  response text DEFAULT NULL,
  status int(11) NOT NULL,
  created_by int(11) NOT NULL,
  updated_date timestamp NOT NULL DEFAULT current_timestamp(),
  created_date timestamp NOT NULL DEFAULT current_timestamp()
);

ALTER TABLE jp_api_log
  ADD PRIMARY KEY (id);
  
ALTER TABLE jp_api_log
  MODIFY id int(11) NOT NULL AUTO_INCREMENT;
  ALTER TABLE pms_material_requisition_items ADD  material_item_id INT(11) NULL DEFAULT NULL;
  ALTER TABLE pms_material_requisition_items ADD  material_item_name VARCHAR(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;
  ALTER TABLE jp_purchase ADD mr_id INT(11) NULL DEFAULT NULL AFTER created_date, ADD mr_material_ids VARCHAR(250) NULL DEFAULT NULL AFTER mr_id;
  ALTER TABLE pms_material_requisition CHANGE purchase_id purchase_id VARCHAR(250) NULL DEFAULT NULL;
  ALTER TABLE pms_material_requisition_items ADD po_id INT(11) NULL DEFAULT NULL AFTER updated_date, ADD bill_id INT(11) NULL DEFAULT NULL AFTER po_id, ADD pms_material_id INT(11) NULL DEFAULT NULL AFTER bill_id, ADD pms_material_name VARCHAR(255) NULL DEFAULT NULL AFTER pms_material_id, ADD pms_unit INT(11) NULL DEFAULT NULL AFTER pms_material_name, ADD pms_template_material_id INT(11) NULL DEFAULT NULL AFTER pms_unit;
  ALTER TABLE pms_material_requisition CHANGE bill_id bill_id VARCHAR(255) NULL DEFAULT NULL;

ALTER TABLE `jp_api_materials` ADD `coms_material_id` INT NULL AFTER `updated_at`, ADD `coms_unit_id` INT NULL AFTER `coms_material_id`;

ALTER TABLE `jp_api_labours` ADD `coms_labour_id` INT NULL AFTER `updated_at`, ADD `coms_unit_id` INT NULL AFTER `coms_labour_id`;

ALTER TABLE `jp_api_equipments` ADD `coms_equipment_id` INT NULL AFTER `updated_at`, ADD `coms_unit_id` INT NULL AFTER `coms_equipment_id`;

ALTER TABLE `jp_api_materials` ADD `coms_project_id` INT NULL AFTER `coms_unit_id`;

ALTER TABLE `jp_api_labours` ADD `coms_project_id` INT NULL AFTER `coms_unit_id`;

ALTER TABLE `jp_api_equipments` ADD `coms_project_id` INT NULL AFTER `coms_unit_id`;

ALTER TABLE jp_labour_worktype ADD pms_labour_id INT(11) NOT NULL DEFAULT '0' AFTER is_active;
