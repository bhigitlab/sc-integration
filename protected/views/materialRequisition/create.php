<?php
/* @var $this MaterialRequisitionController */
/* @var $model MaterialRequisition */

$this->breadcrumbs = array(
    'Material Requisitions' => array('index'),
    'Create',
);
?>
<?php echo $this->renderPartial('_form', array(
    'model' => $model, 
    'project' => $project,
    'requisition_no' => $requisition_no, 
    'project_id' => '',
    'mr_for_task' => $mr_for_task,
    'account_items' => $account_items,
    'unit_list' => $unit_list,
    'type'=>$type,
    'company'=>$company
)); ?>
