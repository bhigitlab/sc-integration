<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.min.js"></script>
<?php
/* @var $this SubcontractorbillController */
/* @var $model Subcontractorbill */

$this->breadcrumbs = array(
    'Subcontractorbills' => array('index'),
    $model->id => array('view', 'id' => $model->id),
    'Update',
);

/* $this->menu=array(
    array('label'=>'List Subcontractorbill', 'url'=>array('index')),
    array('label'=>'Create Subcontractorbill', 'url'=>array('create')),
    array('label'=>'View Subcontractorbill', 'url'=>array('view', 'id'=>$model->id)),
    array('label'=>'Manage Subcontractorbill', 'url'=>array('admin')),
); */
?>

<div class="container" id="vendors">
    <div class="loading-overlay">
        <span class="fa fa-spinner fa-3x fa-spin"></span>
    </div>
    <div class="expenses-heading">
        <div class="clearfix">
            <!-- remove addentries class -->
            <a type="button" id="billView" href="<?php echo $this->createUrl('subcontractorbill/admin') ?>"
                class="btn btn-info pull-right mt-0 mb-10">View Bills</a>
            <?php
            if ($model->bill_type == 1) { ?>
                <h3>Subcontractor Bill </h3>
            <?php } else { ?>
                <h3>Subcontractor Bill By Item</h3>
            <?php } ?>
            <div id="loading"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/ajax-loader.gif"></div>
        </div>
    </div>
    <?php if (Yii::app()->user->hasFlash('success')): ?>
        <div class="info d-block mb-2 text-center" style="color: green;font-size: 15px;font-weight: bold;">
            <?php echo Yii::app()->user->getFlash('success'); ?>
        </div>
    <?php endif; ?>
    <div class="entries-wrapper">
        <div class="row">
            <div class="col-xs-12">
                <div class="heading-title">Edit Subcontractor Bill</div>
                <div class="dotted-line"></div>
            </div>
        </div>
        <?php if ($model->bill_type == 1) { ?>
            <?php $this->renderPartial('_updateform', array('model' => $model, 'itemmodel' => $itemmodel));
        } else {
            $this->renderPartial('_form_with_item', array('model' => $model, 'itemmodel' => $itemmodel, 'is_update' => 1, ));
        } ?>
    </div>
</div>
