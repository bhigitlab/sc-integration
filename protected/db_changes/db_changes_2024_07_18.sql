ALTER TABLE jp_clients ADD pms_client_id INT(11) NOT NULL DEFAULT '0' AFTER company_id;
ALTER TABLE jp_unit ADD pms_unit_id INT(11) NOT NULL DEFAULT '0' AFTER company_id;
ALTER TABLE jp_equipments ADD pms_equipment_id INT(11) NOT NULL DEFAULT '0' AFTER equipment_unit;
ALTER TABLE jp_labours ADD pms_labour_id INT(11) NOT NULL DEFAULT '0' AFTER labour_rate;
ALTER TABLE jp_materials ADD pms_material_id INT(11) NOT NULL DEFAULT '0' AFTER material_unit;

ALTER TABLE jp_projects DROP COLUMN mapped_status;
ALTER TABLE jp_clients DROP COLUMN mapped_status;
ALTER TABLE jp_unit DROP COLUMN mapped_status;
ALTER TABLE jp_equipments DROP COLUMN mapped_status;
ALTER TABLE jp_labours DROP COLUMN mapped_status;
ALTER TABLE jp_materials DROP COLUMN mapped_status;

