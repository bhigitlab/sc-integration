<div id='previous_details'></div>

<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'clients-form',
    'enableAjaxValidation' => false,
    'enableClientValidation' => true,
    'action' => Yii::app()->createAbsoluteUrl("salesquotation/salesQuotation/addquotationitems"),
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
    'clientOptions' => array(
        'validateOnSubmit' => true,
        'validateOnChange' => true,
        'validateOnType' => false,
    ),
        ));
?>
<?php echo CHtml::hiddenField('cat_id', $category_id, array('id' => 'cat_id')); ?>
<?php echo CHtml::hiddenField('cat_name', $category_label, array('id' => 'cat_name')); ?>
<?php echo CHtml::hiddenField('master_id', $qid, array('id' => 'master_id')); ?>
<?php echo CHtml::hiddenField('revision_no', $rev, array('id' => 'revision_no')); ?>
<?php echo CHtml::hiddenField('status', $status, array('id' => 'status')); ?>
<span id="error-add"></span>
<div class="panel-body">
    <?php if ($quotation_count > 0) { ?>
        <div class="mainitem"> 
            <table class="table qitem_table" id="qitem_table">
                <thead>
                    <tr>
                        <th>Work Type</th>
                        <th>WorkType Label</th>
                        <th></th>
                        <th>Description</th>
                        <th>Unit</th>
                        <th>Quantity</th>
                        <th>Quantity (in NOS)</th>
                        <th>MRP</th>
                        <th>Amount after discount</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $worktypearray = array();
                    foreach ($all_quotation_items as $key => $value) {
                        $worktypeid = $value->work_type_id;
                        array_push($worktypearray, $worktypeid);
                        $work_type_label = QuotationWorktypeMaster::model()->findByPk($value->work_type_id);
                        ?>
                        <tr>
                            <td class="worktypedata" data-id ='<?php echo $value->work_type_id ?>'    data-incremnt ='<?php echo $key ?>' data-template ='<?php echo $work_type_label['template_id'] ?>' >
                                <?php echo $work_type_label['name'] ?>
                                <?php echo CHtml::hiddenField('work_type_id[]', $value->work_type_id, array('id' => 'hiddenworktype' . $key,'class'=>'hiddenworktype')); ?>
                                <?php echo CHtml::hiddenField('item_id[]', '', array('id' => 'hiddenitem' . $key,'class'=>'hiddenitem')); ?>
                                <?php echo CHtml::hiddenField('template_id[]', $work_type_label['template_id'], array('id' => 'hiddentemplate' . $key,'class'=>'hiddentemplate')); ?>

                            </td>
                            <td>
                                <?php
                                $newQuery = 'work_type_id =' . $value->work_type_id . ' AND quotation_category_id =' . $category_id . '';
                                echo $form->dropDownList(
                                        $model, 'worktype_label', CHtml::listData(QuotationItemMaster::model()->findAll(array('condition' => $newQuery)), 'worktype_label', 'worktype_label'), array('class' => 'form-control js-example-basic-single field_change invoice_add require worktype_label', 'name' => 'worktype_label[]', 'empty' => '-Select Worktype Label-', 'id' => 'worktype_label' . $key, 'style' => 'width:100%')
                                );
                                ?>

                            </td>
                            <?php if ($work_type_label['template_id'] == 1) { ?>
                                <td>
                                    <label>Shutter Work</label>
                                    <?php echo $form->textField($model, 'shutterwork_description', array('class' => 'form-control require  shutterwork_description', 'name' => 'shutterwork_description[]', 'value' => '', 'id' => 'shutterwork_description' . $key, 'readonly' => true)); ?>
                                </td>
                                <td>
                                    <label>Carcass Box Work</label>
                                    <?php echo $form->textField($model, 'carcass_description', array('class' => 'form-control require caracoss_description', 'name' => 'caracoss_description[]', 'value' => '', 'id' => 'caracoss_description' . $key, 'readonly' => true)); ?>
                                    <?php echo $form->hiddenField($model, 'shutterwork_material', array('class' => 'form-control require shutterwork_material', 'name' => 'shutterwork_material[]', 'value' => '', 'id' => 'shutterwork_material' . $key)); ?>
                                    <?php echo $form->hiddenField($model, 'shutterwork_finish', array('class' => 'form-control require shutterwork_finish', 'name' => 'shutterwork_finish[]', 'value' => '', 'id' => 'shutterwork_finish' . $key)); ?>
                                    <?php echo $form->hiddenField($model, 'carcass_material', array('class' => 'form-control require carcass_material', 'name' => 'carcass_material[]', 'value' => '', 'id' => 'carcass_material' . $key)); ?>
                                    <?php echo $form->hiddenField($model, 'carcass_finish', array('class' => 'form-control require carcass_finish', 'name' => 'carcass_finish[]', 'value' => '', 'id' => 'carcass_finish' . $key)); ?>
                                    <?php echo $form->hiddenField($model, 'material', array('class' => 'form-control require material', 'name' => 'material[]', 'value' => '', 'id' => 'material' . $key)); ?>
                                    <?php echo $form->hiddenField($model, 'finish', array('class' => 'form-control require finish', 'name' => 'finish[]', 'value' => '', 'id' => 'finish' . $key)); ?>
                                    <?php echo $form->hiddenField($model, 'description', array('class' => 'form-control require description', 'name' => 'description[]', 'value' => '', 'id' => 'description' . $key)); ?>
                                </td>
                                <?php
                            } else {
                                ?>
                                <td><?php echo $form->hiddenField($model, 'material', array('class' => 'form-control require material', 'name' => 'material[]', 'value' => '', 'id' => 'material' . $key)); ?>
                                    <?php echo $form->hiddenField($model, 'finish', array('class' => 'form-control require finish', 'name' => 'finish[]', 'value' => '', 'id' => 'finish' . $key)); ?></td>
                                <td>
                                    <?php echo $form->textField($model, 'description', array('class' => 'form-control require description', 'autocomplete' => 'off', 'name' => 'description[]', 'value' => '', 'id' => 'description' . $key, 'readonly' => true)); ?>
                                </td>
                            <?php } ?>

                            <td>
                                <?php
                                echo $form->dropDownList(
                                        $model, 'unit', CHtml::listData(Unit::model()->findAll(), 'id', 'unit_name'), array('class' => 'form-control js-example-basic-single field_change invoice_add require unit', 'name' => 'unit[]', 'empty' => '-Select Unit-', 'id' => 'unit' . $key, 'style' => 'width:100%')
                                );
                                ?>
                            </td>
                            <td>
                                <?php echo $form->textField($model, 'quantity', array('class' => 'form-control allownumericdecimal  require quantity ', 'autocomplete' => 'off', 'name' => 'qty[]', 'data-incremntid' => $key, 'id' => 'qty' . $key)); ?>
                                <?php echo $form->error($model, 'quantity'); ?>
                            </td>
                            <td>
                                <?php echo $form->textField($model, 'quantity_nos', array('class' => 'form-control  require qtynos', 'autocomplete' => 'off', 'name' => 'qtynos[]', 'id' => 'qtynos' . $key)); ?>
                                <?php echo $form->error($model, 'quantity_nos'); ?>
                            </td>
                            <td>
                                <?php echo $form->textField($model, 'mrp', array('class' => 'form-control allownumericdecimal require mrp' ,'readonly'=>'readonly', 'autocomplete' => 'off', 'name' => 'mrp[]', 'id' => 'mrp' . $key)); ?>
                                <?php echo $form->error($model, 'mrp'); ?>
                            </td>
                            <td>
                                <?php echo $form->textField($model, 'amount_after_discount', array('class' => 'form-control allownumericdecimal  require amountafter-discount amount_after_discount','readonly'=>'readonly', 'autocomplete' => 'off', 'name' => 'amount_after_discount[]', 'id' => 'amount_after_discount' . $key)); ?>
                                <?php echo $form->error($model, 'amount_after_discount'); ?>
                            </td>
                        </tr>

                    <?php } ?>
                </tbody>
            </table>
        </div>
        <?php echo CHtml::button('Add Sub Item', array('class' => 'btn btn-info', 'onclick' => 'displaysub();')); ?>
        <div id="subitem" style="display:none;" >
            <br>
            <div style ='width:250px;'>
                <?php echo $form->textField($model, 'subitem_label', array('class' => 'form-control', 'autocomplete' => 'off', 'placeholder' => 'subitem label', 'name' => 'subitem[subitem_label]', 'id' => 'subitem_label1', 'data-subincrement' => '1')); ?>
                <?php echo $form->error($model, 'subitem_label'); ?>
            </div><br>
            <div class="row" id="container_subitem">

                <div class="row-margin sub_item" id="subiteminput1">
                    <div class="col-md-2">
                        <?php
                        $worktype = implode(',', $worktypearray);
                        $newQuery = 'id IN(' . $worktype . ')';
                        echo $form->dropDownList(
                                $model, 'work_type', CHtml::listData(QuotationWorktypeMaster::model()->findAll(array('condition' => $newQuery)), 'id', 'name'), array('class' => 'form-control js-example-basic-single field_change invoice_add require sub_worktype', 'data-subincrement' => '1', 'empty' => '-Select Worktype-', 'name' => 'subitem[worktype_type][]', 'style' => 'width:100%', 'id' => 'sub_worktype1')
                        );
                        ?>
                        <?php echo $form->error($model, 'work_type'); ?>
                    </div>

                    <div class="col-md-2">

                        <select name="subitem[worktype_label][]" class="form-control js-example-basic-single field_change invoice_add require sub_worktype_label" id="sub_worktypelabel1" data-subincrement="1">
                            <option value="">No label</option>
                        </select>

                        <?php echo $form->error($model, 'worktype_label'); ?>
                    </div>

                    <div class="col-md-1">
                        <?php
                        echo $form->dropDownList($model, 'unit', CHtml::listData(Unit::model()->findAll(), 'id', 'unit_name'), array('class' => 'form-control js-example-basic-single field_change invoice_add require ', 'empty' => '-Select Unit-', 'name' => 'subitem[unit][]', 'style' => 'width:100%', 'id' => 'sub_unit1', 'data-subincrement' => '1')
                        );
                        ?>
                    </div>
                    <div class="col-md-1">
                        <?php echo $form->textField($model, 'quantity', array('class' => 'form-control allownumericdecimal quantity_sub', 'autocomplete' => 'off', 'placeholder' => 'qty', 'name' => 'subitem[quantity][]', 'id' => 'sub_quantity1', 'data-subincrement' => '1')); ?>
                        <?php echo $form->error($model, 'quantity'); ?>
                    </div>
                    <div class="col-md-1">
                        <?php echo $form->textField($model, 'quantity_nos', array('class' => 'form-control allownumericdecimal ', 'autocomplete' => 'off', 'placeholder' => 'qty nos', 'name' => 'subitem[quantity_nos][]', 'id' => 'sub_quantity_nos1', 'data-subincrement' => '1')); ?>
                        <?php echo $form->error($model, 'quantity_nos'); ?>
                    </div>
                    <div class="col-md-2">
                        <?php echo $form->textField($model, 'mrp', array('class' => 'form-control allownumericdecimal ', 'autocomplete' => 'off', 'placeholder' => 'MRP', 'name' => 'subitem[mrp][]', 'id' => 'sub_mrp1', 'data-subincrement' => '1')); ?>
                        <?php echo $form->error($model, 'mrp'); ?>
                    </div>
                    <div class="col-md-2">
                        <?php echo $form->textField($model, 'amount_after_discount', array('class' => 'form-control allownumericdecimal amountafter-discount', 'autocomplete' => 'off', 'name' => 'subitem[amount_after_discount][]', 'placeholder' => 'amt after disc', 'id' => 'sub_amount_after_discount1', 'data-subincrement' => '1')); ?>
                        <?php echo $form->error($model, 'amount_after_discount'); ?>
                    </div> 
                    <?php echo $form->hiddenField($model, 'item_id', array('class' => 'form-control require', 'name' => 'subitem[item_id][]', 'value' => '', 'id' => 'sub_item1')); ?>
                    <?php echo $form->hiddenField($model, 'shutterwork_description', array('class' => 'form-control require', 'name' => 'subitem[shutterwork_description][]', 'value' => '', 'id' => 'sub_shutterwork_description1')); ?>
                    <?php echo $form->hiddenField($model, 'carcass_description', array('class' => 'form-control require ', 'name' => 'subitem[caracoss_description][]', 'value' => '', 'id' => 'sub_caracoss_description1')); ?>
                    <?php echo $form->hiddenField($model, 'shutterwork_material', array('class' => 'form-control require', 'name' => 'subitem[shutterwork_material][]', 'value' => '', 'id' => 'sub_shutterwork_material1')); ?>
                    <?php echo $form->hiddenField($model, 'shutterwork_finish', array('class' => 'form-control require', 'name' => 'subitem[shutterwork_finish][]', 'value' => '', 'id' => 'sub_shutterwork_finish1')); ?>
                    <?php echo $form->hiddenField($model, 'carcass_material', array('class' => 'form-control require', 'name' => 'subitem[carcass_material][]', 'value' => '', 'id' => 'sub_carcass_material1')); ?>
                    <?php echo $form->hiddenField($model, 'carcass_finish', array('class' => 'form-control require', 'name' => 'subitem[carcass_finish][]', 'value' => '', 'id' => 'sub_carcass_finish1')); ?>
                    <?php echo $form->hiddenField($model, 'material', array('class' => 'form-control require', 'name' => 'subitem[material][]', 'value' => '', 'id' => 'sub_material1')); ?>
                    <?php echo $form->hiddenField($model, 'finish', array('class' => 'form-control require', 'name' => 'subitem[finish][]', 'value' => '', 'id' => 'sub_finish1')); ?>
                    <?php echo $form->hiddenField($model, 'description', array('class' => 'form-control require', 'name' => 'subitem[description][]', 'value' => '', 'id' => 'sub_description1')); ?>
                </div>
            </div>
            <br>
            <div class="sub_btn">
                <a id="add_field_sub"><span>&raquo; Add More</span></a>&nbsp;&nbsp;
                <a id="add_field_remove_sub" class="hide text-danger"><span>&raquo; Remove</span></a>
            </div>
        </div>
    <?php } ?>
    <br><br>
    <?php echo CHtml::button('Add Extra Work', array('class' => 'btn btn-info', 'onclick' => 'displayextra();')); ?>
    <div id="extrawork" style="display:none;" ><br>
        <div class="row" id="container_extrawork">
            <div class="row-margin extra-work">
                <div class="col-md-1">
                    <?php
                    echo $form->labelEx($model, 'Extra Work');
                    ?>
                    <?php echo CHtml::hiddenField('work_type_extra_id', '', array('id' => 'hiddenInputworktype')); ?>
                </div>
                <div class="col-md-2">
                    <?php echo CHtml::activeFileField($model, 'image[]'); ?>
                    <?php echo $form->error($model, 'image'); ?>
                </div>
                <div class="col-md-2">
                    <?php echo $form->textField($model, 'description[]', array('class' => 'form-control ', 'autocomplete' => 'off', 'placeholder' => 'description')); ?>
                    <?php echo $form->error($model, 'description'); ?>
                </div>
                <div class="col-md-1">
                    <?php
                    echo $form->dropDownList($model, 'unit[]', CHtml::listData(Unit::model()->findAll(), 'id', 'unit_name'), array('class' => 'form-control js-example-basic-single field_change invoice_add require ', 'empty' => '-Select Unit-', 'style' => 'width:100%')
                    );
                    ?>
                </div>
                <div class="col-md-1">
                    <?php echo $form->textField($model, 'quantity[]', array('class' => 'form-control allownumericdecimal ', 'autocomplete' => 'off', 'placeholder' => 'qty')); ?>
                    <?php echo $form->error($model, 'quantity'); ?>
                </div>
                <div class="col-md-1">
                    <?php echo $form->textField($model, 'quantity_nos[]', array('class' => 'form-control allownumericdecimal ', 'autocomplete' => 'off', 'placeholder' => 'qty nos')); ?>
                    <?php echo $form->error($model, 'quantity_nos'); ?>
                </div>
                <div class="col-md-2">
                    <?php echo $form->textField($model, 'mrp[]', array('class' => 'form-control allownumericdecimal ', 'autocomplete' => 'off', 'placeholder' => 'MRP')); ?>
                    <?php echo $form->error($model, 'mrp'); ?>
                </div>
                <div class="col-md-2">
                    <?php echo $form->textField($model, 'amount_after_discount[]', array('class' => 'form-control allownumericdecimal amount-after-discount-extra ', 'autocomplete' => 'off', 'placeholder' => 'amt after disc')); ?>
                    <?php echo $form->error($model, 'amount_after_discount'); ?>
                </div> 
                <?php echo CHtml::hiddenField('extra_amount_discount', '0.00', array('id' => 'extra_amount_discount')); ?>
            </div>
        </div>
        <br>
        <div class="btn-holder">
            <a id="add_field_demo"><span >&raquo; Add More</span></a> &nbsp;&nbsp;
            <a id="add_field_demo_remove"><span >&raquo; Remove</span></a>
        </div>
    </div><br><br>
    <div class='row'>
        <div class="col-md-1" >
            <div class="form-group">
                <label>Tax Slab:</label>
                <?php
                $datas = TaxSlabs::model()->getAllDatas();
                ?>
                <select class="form-control" name="tax_slab" id="add_tax_slab">
                    <option value="">Select one</option>
                    <?php
                    foreach ($datas as $value) {
                        if ($value['set_default'] == 1) {
                            echo ' <option data-id="' . $value['id'] . '" value="' . $value['tax_slab_value'] . '" selected>' . $value['tax_slab_value'] . '%</option>';
                        } else {
                            echo ' <option  data-id="' . $value['id'] . '" value="' . $value['tax_slab_value'] . '">' . $value['tax_slab_value'] . '%</option>';
                        }
                    }
                    ?>
                </select>
            </div>
        </div>

        <div class="col-md-1" >                       
            <div class="form-group">
                <label>SGST% :</label>
                <?php echo $form->textField($model, 'sgst_percent', array('class' => 'inputs target small_class txtBox  tax_percentage sgstp allownumericdecimal form-control calculation', 'autocomplete' => 'off', 'name' => 'sgst_percent', 'id' => 'sgstp')); ?>
                <?php echo $form->error($model, 'sgst_percent'); ?>
                <div id="sgst_amount" class="padding-box"><?php echo $model->sgst_amount ?></div>
                <?php echo CHtml::hiddenField('sgst_amount', $model->sgst_amount, array('id' => 'sgst_amount_hidden')); ?>
            </div>
        </div>		
        <div class="col-md-1" >
            <div class="form-group">
                <label>CGST% :</label>
                <?php echo $form->textField($model, 'cgst_percent', array('class' => 'inputs target small_class txtBox tax_percentage cgstp allownumericdecimal form-control calculation ', 'autocomplete' => 'off', 'name' => 'cgst_percent', 'id' => 'cgstp')); ?>
                <?php echo $form->error($model, 'cgst_percent'); ?>
                <div id="cgst_amount" class="padding-box"><?php echo $model->cgst_amount ?></div>
                <?php echo CHtml::hiddenField('cgst_amount', $model->cgst_amount, array('id' => 'cgst_amount_hidden')); ?>
            </div>
        </div>
        <div class="col-md-1" >
            <div class="form-group">
                <label>IGST% :</label>
                <?php echo $form->textField($model, 'igst_percent', array('class' => 'inputs target small_class txtBox tax_percentage igstp allownumericdecimal form-control calculation', 'autocomplete' => 'off', 'name' => 'igst_percent', 'id' => 'igstp')); ?>
                <?php echo $form->error($model, 'igst_percent'); ?>
                <div id="igst_amount" class="padding-box"><?php echo $model->igst_amount ?></div>
                <?php echo CHtml::hiddenField('igst_amount', $model->igst_amount, array('id' => 'igst_amount_hidden')); ?>
            </div>
        </div>
        <div iv class="col-md-2" >
            <label>Total After Discount</label>
            <div class="form-group">
                <?php echo $form->textField($model, 'mainitem_total', array('class' => 'inputs target small_class txtBox tax_percentage igstp allownumericdecimal form-control calculation text-right', 'autocomplete commonadd-val' => 'off', 'name' => 'mainitem_total', 'id' => 'total-amount-val', 'readonly' => 'readonly')); ?>
                <?php echo $form->error($model, 'mainitem_total'); ?>
            </div>
        </div>
        <div class="col-md-2" >
            <label>Total Tax Amount</label>
            <div class="form-group">
                <?php echo $form->textField($model, 'mainitem_tax', array('class' => 'inputs target small_class txtBox tax_percentage igstp allownumericdecimal form-control calculation text-right', 'autocomplete' => 'off', 'name' => 'mainitem_tax', 'id' => 'total-tax', 'readonly' => 'readonly')); ?>
                <?php echo $form->error($model, 'mainitem_tax'); ?>

            </div>
        </div>

        <div class="col-md-2" >
            <label>Total Amount With Tax</label>
            <div class="form-group">
                <?php echo $form->textField($model, 'maintotal_withtax', array('class' => 'inputs target small_class txtBox tax_percentage igstp allownumericdecimal form-control calculation text-right', 'autocomplete' => 'off', 'name' => 'maintotal_withtax', 'id' => 'total-amount', 'readonly' => 'readonly')); ?>
                <?php echo $form->error($model, 'maintotal_withtax'); ?>
                ,
            </div>
        </div> 
        <div class="col-md-1">
            <label class="inline">Round Off:</label>
            <input type="text" name="Quotation[round_off]" id="Quotation_round_off" value="0.00"  class="text-right form-control">
        </div>           
    </div> 
    <div class="row clearfix">
        <div class="col-md-2 pull-right">
            <label class="inline">Total Amount:</label>
            <?php echo $form->textField($model, 'maintotal_withtax', array('class' => 'inputs target small_class txtBox tax_percentage igstp allownumericdecimal form-control calculation text-right', 'autocomplete' => 'off', 'name' => 'maintotal_withtax', 'id' => 'final-total-amount', 'readonly' => 'readonly')); ?>
        </div>
    </div> 
    <div class="panel panel-margin">
        <div class="panel-footer save-btnHold text-center button-panel">
            <?php echo CHtml::button($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-info', 'id' => 'additem')); ?>
            <?php
            if (!$model->isNewRecord) {
                echo CHtml::ResetButton('Close', array('onclick' => 'closeaction(this,event);', 'class' => 'btn'));
            } else {
                echo CHtml::ResetButton('Reset', array('style' => 'margin-right:3px;', 'class' => 'btn btn-default'));
                echo CHtml::ResetButton('Close', array('onclick' => 'closeaction(this,event);', 'class' => 'btn'));
            }
            ?>
        </div>

        <?php $this->endWidget(); ?>

    </div>
</div>
<script>
<?php
$addItem = Yii::app()->createAbsoluteUrl("salesquotation/salesQuotation/addquotationitems");
$url = Yii::app()->createAbsoluteUrl("salesquotation/salesQuotation/index");
$amountget = Yii::app()->createAbsoluteUrl("salesquotation/salesQuotation/getamountdetails");
?>
    function  displaysub() {
        $("#subitem").show();
    }
    function displayextra() {
        $("#extrawork").show();
    }

    $(document).on('change', '.sub_worktype_label', function () {
        var element = $(this);
        var worktypelabel = $('option:selected', this).text();
        var cat_id = '<?php echo $category_id ?>';
        var id = $(this).attr('data-subincrement');
        var worktypeid = $("#sub_worktype" + id).val();
        var template_id = $('option:selected', this).attr('data-template');
        $.ajax({
            url: '<?php echo Yii::app()->createAbsoluteUrl('salesquotation/SalesQuotation/getdatasofworktype'); ?>',
            type: 'GET',
            dataType: 'json',
            data: {
                cat_id: cat_id,
                worktypelabel: worktypelabel,
                worktypeid: worktypeid,
                template_id: template_id
            },
            success: function (result) {

                if (result.status == 1) {
                    $('#sub_item' + id).val(result.item_id);
                    $('#previous_details').html(result.html);
                    $("#previous_details").show();//
                    $("#pre_fixtable").tableHeadFixer();
                    getamountsub(id);
                } else {
                    $('#previous_details').html(result.html);
                    $("#pre_fixtable").tableHeadFixer();
                }
                if (template_id == 1) {
                    $('#sub_shutterwork_description' + id).val(result.shutterdesc);
                    $('#sub_caracoss_description' + id).val(result.carcassdesc);
                    $('#sub_shutterwork_material' + id).val(result.shuttermaterial);
                    $('#sub_shutterwork_finish' + id).val(result.shutterfinish);
                    $('#sub_carcass_material' + id).val(result.carcasmaterial);
                    $('#sub_carcass_finish' + id).val(result.carcassfinish);
                } else {
                    if (template_id == 3) {
                        $('#sub_material' + id).val(result.material);
                        $('#sub_finish' + id).val(result.finish);
                    }
                    $('#sub_description' + id).val(result.description);
                }

                if(result.description ==""){
                    $inputs = $('#sub_description' + id).siblings();                   
                    $inputs.find("#sub_unit" + id).val("");
                    $inputs.find("#sub_item" + id).val("");
                    $inputs.find("#sub_material" + id).val("");
                    $inputs.find("#sub_finish" + id).val("");
                    $inputs.find("#sub_description" + id).val("");
                    $inputs.find("#sub_carcass_finish" + id).val("");
                    $inputs.find("#sub_carcass_material" + id).val("");
                    $inputs.find("#sub_shutterwork_finish" + id).val("");
                    $inputs.find("#sub_shutterwork_material" + id).val("");
                    $inputs.find("#sub_caracoss_description" + id).val("");
                    $inputs.find("#sub_shutterwork_description" + id).val("");                    
                    $inputs.find("#sub_quantity" + id).val("");
                    $inputs.find("#sub_mrp" + id ).val("");
                    $inputs.find("#sub_amount_after_discount" + id ).val("");
                    $inputs.find("#sub_quantity_nos" + id).val("");
                }
                getTotalAmount();
            }
        })
    })
    function getamountsub(id) {
        var item_id = $("#sub_item" + id).val();
        var qty = $("#sub_quantity" + id).val();
        var total_amount = $("#total-amount").val();
        var total_amount_val = '<?php echo $model->mainitem_total ?>';
        var status = '<?php echo $status; ?>';
        $.ajax({
            url: "<?php echo $amountget; ?>",
            data: {
                item_id: item_id,
                qty: qty
            },
            type: "POST",
            dataType: 'json',
            success: function (result) {
                if (result != '') {
                    var amount = result.profit_amount;
                    $("#sub_amount_after_discount" + id).val(result.profit_amount);
                    var mrp_after_discount = parseInt(result.profit_amount) + parseInt((result.profit_amount * result.discount) / 100);
                    $("#sub_mrp" + id).val(mrp_after_discount);
                    var sum_value = 0;
                    $('.amountafter-discount').each(function () {
                        sum_value += +$(this).val();
                    })
                    $('.amount-after-discount-extra').each(function () {
                        sum_value += +$(this).val();
                    })
                    var total = sum_value;
                    if (status == 0) {
                        total = parseFloat(sum_value) + parseFloat(total_amount_val);
                    }
                    $('#total-amount-val').val(total);
                    if (total_amount != '') {
                        $("#sgstp").keyup();
                        $("#cgstp").keyup();
                        $("#igstp").keyup();
                    }

                }
            }
        });
    }

    $(document).on('keyup', '.quantity_sub', function () {
        var element = $(this);
        var id = $(this).attr('data-subincrement');
        var worktypeid = $("#sub_worktype" + id).val();
        var template_id = $('option:selected', $("#sub_worktypelabel" + id)).attr('data-template');
        getamountsub(id);
    });

    $(".allownumericdecimal").keydown(function (event) {
        if (event.shiftKey == true) {
            event.preventDefault();
        }
        if ((event.keyCode >= 48 && event.keyCode <= 57) ||
                (event.keyCode >= 96 && event.keyCode <= 105) ||
                event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 ||
                event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190 || event.keyCode == 110 || event.keyCode == 13) {
            var splitfield = $(this).val().split(".");
            if ((splitfield[1] !== undefined && splitfield[1].length >= 2) && event.keyCode != 8 && event.keyCode != 0 && event.keyCode != 13) {
                event.preventDefault();
            }
        } else {
            event.preventDefault();
        }

        if ($(this).val().indexOf('.') !== -1 && (event.keyCode == 190 || event.keyCode == 110))
            event.preventDefault();

    });
    $('#sgstp').keyup(function () {
        var sgst_percent = $('#sgstp').val();
        var total_amount = $('#total-amount-val').val();
        var totaltax = $('#total-tax').val();
        var cgst_amount = $('#cgst_amount').text();
        var igst_amount = $('#igst_amount').text();
        var roundoff = $("#Quotation_round_off").val();
        var sgst_amount = (total_amount * sgst_percent) / 100;
        $('#sgst_amount').text(sgst_amount.toFixed(2));
        $('#sgst_amount_hidden').val(sgst_amount.toFixed(2));
        var total_tax = parseFloat(cgst_amount) + parseFloat(sgst_amount) + parseFloat(igst_amount);
        $('#total-tax').val(total_tax.toFixed(2));
        var total_with_tax = parseFloat(total_amount) + parseFloat(total_tax);
        $('#total-amount').val(total_with_tax.toFixed(2));
        var final_total =  total_with_tax + parseFloat(roundoff);
        $('#final-total-amount').val(final_total.toFixed(2));
        if (total_amount == 0)  {
            $('#sgst_amount').text('0.00');
        }
    });

    $('#cgstp').keyup(function () {
        var cgst_percent = $('#cgstp').val();
        var total_amount = $('#total-amount-val').val();
        var totaltax = $('#total-tax').val();
        var sgst_amount = $('#sgst_amount').text();
        var igst_amount = $('#igst_amount').text();
        var roundoff = $("#Quotation_round_off").val();
        

            var cgst_amount = (total_amount * cgst_percent) / 100;
            $('#cgst_amount_hidden').val(cgst_amount.toFixed(2));
            $('#cgst_amount').text(cgst_amount.toFixed(2));
            var total_tax = parseFloat(cgst_amount) + parseFloat(sgst_amount) + parseFloat(igst_amount);
            $('#total-tax').val(total_tax.toFixed(2));
            var total_with_tax = parseFloat(total_amount) + parseFloat(total_tax);
            $('#total-amount').val(total_with_tax.toFixed(2));
            var final_total =  total_with_tax + parseFloat(roundoff);
            $('#final-total-amount').val(final_total.toFixed(2));
            if (total_amount == 0)  {
            $('#cgst_amount').text('0.00');
        }
    });

    $('#igstp').keyup(function () {
        var igst_percent = $('#igstp').val();
        var total_amount = $('#total-amount-val').val();
        var totaltax = $('#total-tax').val();
        var sgst_amount = $('#sgst_amount').text();
        var cgst_amount = $('#cgst_amount').text();
        var roundoff = $("#Quotation_round_off").val();
        
            var igst_amount = (total_amount * igst_percent) / 100;
            $('#igst_amount').text(igst_amount.toFixed(2));
            $('#igst_amount_hidden').val(igst_amount.toFixed(2));
            var total_tax = parseFloat(cgst_amount) + parseFloat(sgst_amount) + parseFloat(igst_amount);
            $('#total-tax').val(total_tax.toFixed(2));
            var total_with_tax = parseFloat(total_amount) + parseFloat(total_tax);
            $('#total-amount').val(total_with_tax.toFixed(2));
            var final_total =  total_with_tax + parseFloat(roundoff);
            $('#final-total-amount').val(final_total.toFixed(2));
            if (total_amount == 0)  {
            $('#igst_amount').text('0.00');
        }
    });
    function closeaction() {
        $("#clients-form").hide();

    }



    $('#additem').click(function () {
        $('.quantity_sub,.quantity').trigger('change');
        var data = $("#clients-form").serialize();
        var error = '';
        $(".commonadd-val").each(function () {
            var id = this.id;
            var item_value = $('#' + id).val();
            if (item_value != '') {
                $('#' + id).removeClass("commonadd-val");
                $('#' + id).css({'border': ''});
            }
        });

        var count = $('.commonadd-val').not(function () {
            return this.value;
        }).length;

        if (count > 0)
        {
            error += "<p>Please Enter Mandatory Fields</p>";
            $('.commonadd-val').css('border', "1px solid red");
            $('#error-add').html('<div class="alert alert-danger">' + error + '</div>');
            return false;
        }
        if (error == '') {
            $('#error-add').html('');
            $('#clients-form').submit();
        }
        return false;
    });

    function getamount(id) {
        var item_id = $("#hiddenitem" + id).val();
        var qty = $("#qty" + id).val();
        var total_amount = $("#total-amount").val();
        var total_amount_val = '<?php echo $model->mainitem_total ?>';
        var status = '<?php echo $status; ?>';
        $.ajax({
            url: "<?php echo $amountget; ?>",
            data: {
                item_id: item_id,
                qty: qty
            },
            type: "POST",
            dataType: 'json',
            success: function (result) {
                if (result != '') {
                    var amount = result.profit_amount;
                    $("#amount_after_discount" + id).val(result.profit_amount);
                    var mrp_after_discount = parseInt(result.profit_amount) + parseInt((result.profit_amount * result.discount) / 100);
                    $("#mrp" + id).val(mrp_after_discount);
                    var sum_value = 0;
                    $('.amountafter-discount').each(function () {
                        sum_value += +$(this).val();
                    })
                    $('.amount-after-discount-extra').each(function () {
                        sum_value += +$(this).val();
                    })
                    var total = sum_value;
                    if (status == 0) {
                        total = parseFloat(sum_value) + parseFloat(total_amount_val);
                    }
                    $('#total-amount-val').val(total);
                    if(status==1){
                        $("#clients-form").find("#sgstp").val($("#item_update_form").find("#sgstp").val());
                        $("#clients-form").find("#cgstp").val($("#item_update_form").find("#cgstp").val());
                        $("#clients-form").find("#igstp").val($("#item_update_form").find("#igstp").val());
                    }
                    if (total_amount != '') {
                        $("#sgstp").keyup();
                        $("#cgstp").keyup();
                        $("#igstp").keyup();
                    }

                }
            }
        });
    }
    $(document).on('change', '.worktype_label', function () {
        var element = $(this);
        var worktypelabel = $('option:selected', this).text();
        var cat_id = $("#SalesQuotation_id").val();
        var worktypeid = element.parent().siblings('.worktypedata').attr('data-id');
        var template_id = element.parent().siblings('.worktypedata').attr('data-template');
        if (worktypelabel != '') {
            element.parent().siblings().find('input[type=text],select').addClass('commonadd-val');
        } else {
            element.parent().siblings().find('input[type=text],select').removeClass('commonadd-val');
        }
        var id = element.parent().siblings('.worktypedata').attr('data-incremnt');
        $.ajax({
            url: '<?php echo Yii::app()->createAbsoluteUrl('salesquotation/SalesQuotation/getdatasofworktype'); ?>',
            type: 'GET',
            dataType: 'json',
            data: {
                cat_id: cat_id,
                worktypelabel: worktypelabel,
                worktypeid: worktypeid,
                template_id: template_id
            },
            success: function (result) {

                if (result.status == 1) {
                    $('#hiddenitem' + id).val(result.item_id);
                    $('#previous_details').html(result.html);
                    $("#previous_details").show();
                    $("#pre_fixtable").tableHeadFixer();
                    getamount(id);
                } else {
                    $('#previous_details').html(result.html);
                    $("#pre_fixtable").tableHeadFixer();
                }

                if (template_id == 1) {
                    $('#shutterwork_description' + id).val(result.shutterdesc);
                    $('#caracoss_description' + id).val(result.carcassdesc);
                    $('#shutterwork_material' + id).val(result.shuttermaterial);
                    $('#shutterwork_finish' + id).val(result.shutterfinish);
                    $('#carcass_material' + id).val(result.carcasmaterial);
                    $('#carcass_finish' + id).val(result.carcassfinish);
                     
                    if(result.shutterdesc ==""){
                        $inputs = $('#shutterwork_description' + id).parent().siblings();
                        $inputs.find(".unit").val("");
                        $inputs.find(".quantity,.mrp ,.amount_after_discount ,.qtynos ,.hiddenitem").val("0");
                    }
                } else {
                    if (template_id == 3) {
                        $('#material' + id).val(result.material);
                        $('#finish' + id).val(result.finish);
                    }
                    $('#description' + id).val(result.description);
                    if(result.description ==""){
                        $inputs = $('#description' + id).parent().siblings();
                        $inputs.find(".unit").val("");
                        $inputs.find(".quantity,.mrp ,.amount_after_discount ,.qtynos ,.hiddenitem").val("0");
                    }

                }
                $('.quantity_sub,.quantity').trigger('keyup');                
                $("#sgstp").keyup();
                $("#cgstp").keyup();
                $("#igstp").keyup();

            }
        })
    })
    $(function () {
        $('#add_field_demo').click(function () {
            var clone = $('.extra-work:last-child').clone();
            clone.appendTo("#container_extrawork");
            clone.find('input:text').val('');
        });
    });
    $(function () {
        $('#add_field_demo_remove').click(function () {
            var rows = $(this).parent(".btn-holder").siblings('#container_extrawork');
            var row_len = $(rows).children().length - 1;
            if (row_len >= 1) {
                $(rows).find('.extra-work:last-child').remove();
                return false;
            }
        });
    });

    $(function () {
        var regex = /^(.+?)(\d+)$/i;
        var cloneIndex = $(".sub_item").length;
        $('#add_field_sub').click(function () {
            var rowLength = $(".row-margin.sub_item").length;            
            if(rowLength>=1){
                $("#add_field_remove_sub").removeClass('hide');
            }else{
                $("#add_field_remove_sub").addClass('hide');
            }
            var clone = $('.sub_item:last-child').clone();
            clone.find('input:text').val('');
            clone.appendTo("#container_subitem")
                    .attr("id", "subiteminput" + (cloneIndex + 1))
                    .find("*")
                    .each(function () {
                        var id = this.id || "";
                        var match = id.match(regex) || [];
                        if (match.length == 3) {
                            this.id = match[1] + (cloneIndex + 1);
                            $(this).attr('data-subincrement', (cloneIndex + 1));
                        }
                    })
            cloneIndex++;
        });
    });

    $(function () {
        $('#add_field_remove_sub').click(function () {
            var rowLength = $(".row-margin.sub_item").length;    
            
            if(rowLength<=2){
                $(this).addClass('hide');                
            }else{
                $(this).removeClass('hide');
            }
            var rows = $(this).parent(".sub_btn").siblings('#container_subitem');
            var row_len = $(rows).children().length - 1;
            if (row_len >= 1) {
                $(rows).find('.sub_item:last-child').remove();
                getTotalAmount();
                return false;
            }
        });
    });
    $(document).on('keyup', '.quantity', function () {
        var element = $(this);
        var val = element.attr('data-incremntid');
        var worktype = $('#hiddenworktype' + val).val();
        var template = $('#hiddentemplate' + val).val();
        getamount(val);
        var increment_id = $('#qitem_table tr:last').children('td:first').attr('data-incremnt');
        var count = $('.worktypedata').length;
        var worktype_label = $('#worktype_label' + val).val();
        if (template == 3 && worktype_label != '') {
            var $tr = $(this).closest('tr');
            var allTrs = $tr.closest('table').find('tr');
            var lastTr = allTrs[allTrs.length - 1];
            var $clonecount =$tr.nextAll('tr').length;            
            var $clone = $tr.clone();
            var key = parseInt(val, 10) + parseInt(1, 10);
            $clone.find('td').each(function () {
                var el = $(this).children('input,select');
                $(this).children('input,select').removeClass('commonadd-val');
                el.each(function () {
                    var id = $(this).attr('id') || null;
                    if (id) {
                        var i = id.substr(id.length - 1);
                        var name = $(this).attr('name');
                        var classname = $(this).attr('class');
                        var prefix = id.substr(0, (id.length - 1));
                        $(this).attr('id', prefix + (+count));
                        $(this).parents('tr').find('td:first-child').attr('data-incremnt', count);
                        $(this).attr('name', name);
                        $(this).attr('class', classname);
                    }
                });
            });
            $clone.find('.quantity').attr('data-incremntid', count);

            $clone.find('.hiddenworktype').attr('id', 'hiddenworktype'+count);
            $clone.find('.hiddenitem').attr('id', 'hiddenitem'+count);
            $clone.find('.hiddentemplate').attr('id', 'hiddentemplate'+count);
            $clone.find('.worktype_label').attr('id', 'worktype_label'+count);
            $clone.find('.carcass_finish').attr('id', 'carcass_finish'+count);
            $clone.find('.carcass_material').attr('id', 'carcass_material'+count);
            $clone.find('.shutterwork_finish').attr('id', 'shutterwork_finish'+count); 
            $clone.find('.shutterwork_material').attr('id', 'shutterwork_material'+count);
            $clone.find('.caracoss_description').attr('id', 'caracoss_description'+count); 
            $clone.find('.shutterwork_description').attr('id', 'shutterwork_description'+count);
            $clone.find('.material').attr('id', 'material'+count);
            $clone.find('.finish').attr('id', 'finish'+count);
            $clone.find('.description').attr('id', 'description'+count);
            $clone.find('.unit').attr('id', 'unit'+count);
            $clone.find('.quantity').attr('id', 'qty'+count);
            $clone.find('.qtynos').attr('id', 'qtynos'+count);
            $clone.find('.mrp').attr('id', 'mrp'+count);
            $clone.find('.amount_after_discount').attr('id', 'amount_after_discount'+count);
            $clone.find('.hiddenitem').val('');
            $clone.find('.material').val('');
            $clone.find('.finish').val('');
            $clone.find('input:text').val('');
            if($clonecount ==0){
                $tr.append($clone);
                $($clone).insertAfter($tr);
            }
        }
    });
    $(document).on('change', '.amountafter-discount', function () {
        var total_amount = $("#total-amount").val();
        var total_amount_val = '<?php echo $model->mainitem_total ?>'
        var status = '<?php echo $status; ?>';
        var sum_value = 0;
        $('.amountafter-discount').each(function () {
            sum_value += +$(this).val();
        })
        $('.amount-after-discount-extra').each(function () {
            sum_value += +$(this).val();
        })
        var total = sum_value;
        if (status == 0) {
            total = parseFloat(sum_value) + parseFloat(total_amount_val);
        }
        $('#total-amount-val').val(total);
        if (total_amount != '') {
            $("#sgstp").keyup();
            $("#cgstp").keyup();
            $("#igstp").keyup();
        }
    });
    $(document).on('change', '.amount-after-discount-extra', function () {
        var total_amount = $("#total-amount").val();
        var total_amount_val = '<?php echo $model->mainitem_total ?>'
        var status = '<?php echo $status; ?>';
        var sum_value = 0;
        $('.amountafter-discount').each(function () {
            sum_value += +$(this).val();
        })
        $('.amount-after-discount-extra').each(function () {
            sum_value += +$(this).val();
        })
        var total = sum_value;
        if (status == 0) {
            total = parseFloat(sum_value) + parseFloat(total_amount_val);
        }
        $('#total-amount-val').val(total);
        if (total_amount != '') {
            $("#sgstp").keyup();
            $("#cgstp").keyup();
            $("#igstp").keyup();
        }
    });
    $(document).on('change', '.sub_worktype', function () {
        var val = $(this).val();
        var cat_id = '<?php echo $category_id ?>';
        var id = $(this).attr('data-subincrement');
        if (val != '') {
            $(this).parent().siblings().find('input[type=text],select').addClass('commonadd-val');
        } else {
            $(this).parent().siblings().find('input[type=text],select').removeClass('commonadd-val');
        }
        $("#sub_worktypelabel" + id).html('<option value="">Select Label</option>');
        $.ajax({
            url: '<?php echo Yii::app()->createUrl('salesquotation/SalesQuotation/getworktypelabel'); ?>',
            method: 'POST',
            data: {
                worktype: val,
                cat_id: cat_id
            },
            dataType: "json",
            success: function (response) {
                if (response.status == 'success') {
                    $("#sub_worktypelabel" + id).html(response.labellist);
                } else {
                    $("#sub_worktypelabel" + id).html(response.labellist);
                    if($("#sub_worktypelabel" + id).val() ==""){
                        $("#sub_unit" + id).val("");
                        $("#sub_quantity" + id).val("");
                        $("#sub_quantity_nos" + id).val("");
                        $("#sub_mrp" + id).val("");
                        $("#sub_amount_after_discount" + id).val("");
                    }                    
                }
                getTotalAmount();
            }

        })
    })

    $('#Quotation_round_off').change(function() {
        var roundOff = parseFloat(this.value);       
        var amount = $("#total-amount").val().replace(/,/g, '');
        amount = parseFloat(amount);                           
        var total_amount = amount + roundOff;
        $("#final-total-amount").val(parseFloat(total_amount).toFixed(2));
    });

    function getTotalAmount(){
        var sum_value = 0;
        var total_amount = $("#total-amount").val();
        
        $('.amountafter-discount').each(function () {            
            sum_value += +$(this).val();
        })
        $('.amount-after-discount-extra').each(function () {
            sum_value += +$(this).val();
        })
        
        $('#total-amount-val').val(sum_value);
        if (total_amount != '') {
            $("#sgstp").keyup();
            $("#cgstp").keyup();
            $("#igstp").keyup();
        }
    }
    $('#SalesQuotation_image').change(function(){
        var ext = $('#SalesQuotation_image').val().split('.').pop().toLowerCase();
        var message = "";
        var disabled=false;
        if($.inArray(ext, ['png','jpg','jpeg']) == -1) {
            var message = "Accept only jpg ,jpeg and png format!";
            var disabled=true;
            $('#SalesQuotation_image').val(''); 
        }
        $(this).siblings(".errorMessage").text(message).addClass("d-block");        
    });
</script>