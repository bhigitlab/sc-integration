<?php
/* @var $this ProjectTypeController */
/* @var $model ProjectType */

$this->breadcrumbs=array(
	'Project Types'=>array('index'),
	'Create',
);

/*$this->menu=array(
	array('label'=>'List ProjectType', 'url'=>array('index')),
	array('label'=>'Manage ProjectType', 'url'=>array('admin')),
); */
?>


<div class="panel panel-gray">
    <div class="panel-heading form-head">
        <h3 class="panel-title">Add Client Type</h3>
    </div>
        <?php echo $this->renderPartial('_form', array('model' => $model)); ?>
</div>
