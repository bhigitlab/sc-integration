INSERT INTO `jp_menu` (`menu_id`, `menu_name`, `parent_id`, `status`, `controller`, `action`, `params`, `showmenu`, `show_list`, `related`) VALUES (801, 'Ratecard', '0', '0', 'RateCard', '', '', '0', '1', NULL);

INSERT INTO `jp_menu` (`menu_id`, `menu_name`, `parent_id`, `status`, `controller`, `action`, `params`, `showmenu`, `show_list`, `related`) VALUES (802, 'Listing', '801', '0', 'RateCard', 'index', '', '2', '1', NULL);

INSERT INTO `jp_menu` (`menu_id`, `menu_name`, `parent_id`, `status`, `controller`, `action`, `params`, `showmenu`, `show_list`, `related`) VALUES (803, 'Create', '801', '0', 'RateCard', 'create', '', '2', '1', NULL);

INSERT INTO `jp_menu` (`menu_id`, `menu_name`, `parent_id`, `status`, `controller`, `action`, `params`, `showmenu`, `show_list`, `related`) VALUES (804, 'Update', '801', '0', 'RateCard', 'update', '', '2', '1', NULL);

CREATE TABLE `jp_rate_card` (`id` INT NOT NULL AUTO_INCREMENT , `worktype` SMALLINT NULL , `rate` FLOAT(15,2) NULL , `comment` VARCHAR(250) NULL , `created_by` INT NULL , `created_date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP , `updated_by` INT NULL , `updated_date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP , PRIMARY KEY (`id`)) ENGINE = InnoDB;

ALTER TABLE `jp_rate_card` ADD CONSTRAINT `jp_ratecard_worktype_ibfk_1` FOREIGN KEY (`worktype`) REFERENCES `jp_work_type`(`wtid`) ON DELETE RESTRICT ON UPDATE CASCADE; ALTER TABLE `jp_rate_card` ADD CONSTRAINT `jp_ratecard_createdby_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `jp_users`(`userid`) ON DELETE RESTRICT ON UPDATE CASCADE; ALTER TABLE `jp_rate_card` ADD CONSTRAINT `jp_ratecard_updatedby_ibfk_3` FOREIGN KEY (`updated_by`) REFERENCES `jp_users`(`userid`) ON DELETE RESTRICT ON UPDATE CASCADE;



ALTER TABLE `jp_warehouse` CHANGE `warehouse_address` `warehouse_address` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL;

ALTER TABLE `jp_warehouse` CHANGE `warehouse_place` `warehouse_place` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;

ALTER TABLE `jp_projects` CHANGE `updated_date` `updated_date` DATE NULL DEFAULT NULL;

ALTER TABLE `jp_clients` CHANGE `local_address` `local_address` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,CHANGE `contact_person` `contact_person` VARCHAR(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;

INSERT INTO `jp_status` (`sid`, `caption`, `status_type`) VALUES ('108', 'Pending for invoice', 'invoice_status');
ALTER TABLE `jp_invoice` ADD `status_id` INT NULL DEFAULT NULL AFTER `round_off`;
ALTER TABLE `jp_inv_list` ADD `hours` FLOAT(11,2) NULL DEFAULT NULL AFTER `type`;
ALTER TABLE `jp_invoice` ADD `pms_invoice_id` INT NULL DEFAULT NULL AFTER `status_id`;
DROP TABLE IF EXISTS `jp_api_request_log`;
CREATE TABLE `jp_api_request_log` (`request_id` INT NOT NULL , `project_id` INT NOT NULL , `type` VARCHAR(50) NOT NULL , `request` TEXT NOT NULL , `response` TEXT NULL , `created_by` INT NOT NULL , `created_date` TIMESTAMP NOT NULL , `updated_by` INT NULL , `updated_date` TIMESTAMP NULL DEFAULT NULL ) ENGINE = InnoDB;
ALTER TABLE `jp_api_request_log` ADD PRIMARY KEY(`request_id`);
ALTER TABLE `jp_api_request_log` CHANGE `request_id` `request_id` INT(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `jp_inv_list` ADD `worktype_id` INT NULL DEFAULT NULL AFTER `hours`, ADD `ratecard_id` INT NULL DEFAULT NULL AFTER `worktype_id`;