<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.min.js"></script>

<?php
/* @var $this MaterialRequisitionController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'Material Requisitions',
);
?>

<div class="container">
    <div class="header-container margin-bottom-0">
        <h3>Material Requisitions</h3>
        <div class="btn-container">
            <?php
            echo CHtml::link('Add Material Requisition', array('MaterialRequisition/create', 'type' => 1), array('class' => 'btn btn-info', 'id' => 'materialrequest'));
            $acc_proj_id_empty_style = '';
            $project_map_legend = '<div></div>';
            if ($pms_api_integration == 1) {
                $attributes = array('project_id' => null);
                $materialRequisitions = MaterialRequisition::model()->findAllByAttributes($attributes);
                if (!empty($materialRequisitions)) {
                    $empty_project_count = MaterialRequisition::model()->countByAttributes($attributes);
                    if ($empty_project_count >= 1) {
                        echo CHtml::link('Refresh', array('MaterialRequisition/refreshButton', 'type' => 1), array('class' => 'btn btn-info', 'id' => 'materialrequest_refresh'));
                    }
                    $acc_proj_id_empty_style = "style='background-color:green'";
                }
                $project_map_legend = "<div class='legend status-legend'><span class='project_not_mapped'></span>Project Not Mapped In Integration</div>";
            } ?>
        </div>
    </div>
    <div id="alert"></div>
    <div class="alert <?= Yii::app()->user->hasFlash('error') ? "" : "hide" ?>">
        <?php
        if (Yii::app()->user->hasFlash('error')) {
            echo Yii::app()->user->getFlash('error');
        }
        ?>
        <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
    </div>
    <div id="reject_form" data-toggle="modal" data-target="#reviewLoginModal" class="modal">
    </div>
    <?php
    $this->widget('zii.widgets.grid.CGridView', array(
        'id' => 'material-requisition-grid',
        'dataProvider' => $model->search(),
        'template' => '<div class="sub-heading">' . $project_map_legend . '{summary}</div>
        <div class="tableholder text-nowrap enquiry-table report-table-wrapper">{items}</div>
        <div class="text-right margin-top-10">{pager}</div>',
        'itemsCssClass' => 'table table-bordered custom-entry-table',
        'filter' => $model,
        'pager' => array(
            'id' => 'dataTables-example_paginate',
            'header' => '',
            'prevPageLabel' => 'Previous ',
            'nextPageLabel' => 'Next '
        ),
        'pagerCssClass' => 'dataTables_paginate paging_simple_numbers',
        'rowCssClassExpression' => '$data->project_id === null ? "empty-project-row" : ""',
        'columns' => array(
            array('class' => 'IndexColumn', 'header' => 'S.No.', 'htmlOptions' => array('style' => 'width:20px;'), ),
            array(
                'name' => 'requisition_no',
                'value' => '$data->requisition_no',
                'type' => 'raw',
            ),
            array(
                'name' => 'project_id',
                'type' => 'raw',
                'value' => 'isset($data->project->name) ? $data->project->name : "Project not mapped"',
                'filter' => CHtml::listData(Projects::model()->findAll(
                    array(
                        'select' => array('pid', 'name'),
                        'order' => 'name',
                        'distinct' => true
                    )
                ), "pid", "name"),
            ),
            array(
                'name' => 'task_description',
                'value' => 'isset($data->task_description) ? $data->task_description : "Nil"',
                'type' => 'raw',
            ),
            array(
                'name' => 'purchase_no',
                'type' => 'raw',
                'value' => function ($data) {
                    return $data->getPurchaseNumbersOrBillNumbers();
                },
                'header' => 'Purchase /Bill No'
            ),
            array(
                'name' => 'caption',
                'type' => 'raw',
                'value' => function ($data) {
                    return $data->getBillingStatuses();
                },
                'header' => 'Billing Status'
            ),
            array(
                'name' => 'type_from',
                'type' => 'raw',
                'value' => function ($data) {
                    return $data->type_from == 1 ? 'PMS' : 'ACCOUNTS';
                },
                'filter' => CHtml::dropDownList(
                    'MaterialRequisition[type_from]',
                    $model->type_from,
                    array('1' => 'PMS', '2' => 'ACCOUNTS'),
                    array(
                        'empty' => '',
                    )
                ),
                'header' => 'Type From'
            ),
            array(
                'htmlOptions' => array('class' => 'nowrap'),
                'class' => 'CustomButtonColumn', // Use the custom column here
                'template' => '{view}{add_po}{edit}{deleting}',
                'buttons' => array(
                    'view' => array(
                        'label' => function ($data) {
                            $disabled = $data->project_id === null ? 'disabled' : '';
                            return '<button class="btn btn-default btn-xs" ' . $disabled . '>View</button>';
                        },
                        'imageUrl' => false,
                        'options' => array(
                            'id' => '$data->requisition_id',
                            'title' => 'View',
                        ),
                    ),
                    'add_po' => array(
                        'label' => function ($data) {
                            $disabled = $data->project_id === null ? 'disabled' : '';
                            return '<button class="btn btn-default btn-xs ml-1" ' . $disabled . '>Add PO</button>';
                        },

                        'url' => 'Yii::app()->createUrl("materialRequisition/addPo", array("id"=>$data->requisition_id))',
                        'imageUrl' => false,
                        'options' => array(
                            'id' => '$data->requisition_id',
                            'title' => 'Add PO',
                        ),
                    ),
                    'edit' => array(
                        'label' => '<button class="btn btn-primary btn-xs ml-1">Edit</button>',
                        'url' => 'Yii::app()->createUrl("materialRequisition/update", array("id"=>$data->requisition_id))',
                        'imageUrl' => false,
                        'options' => array(
                            'id' => '$data->requisition_id',
                            'title' => 'Edit',
                        ),
                    ),

                    'deleting' => array(
                        'label' => function ($data) {
                            return '<button class="btn btn-danger btn-xs ml-1" onclick="deleteItem(' . $data->requisition_id . ')">Delete</button>';
                        },

                        'imageUrl' => false,
                        'options' => array(
                            'title' => 'Delete',
                        ),
                        'htmlOptions' => array('class' => 'nowrap'),
                    ),

                ),
            ),
        ),
    ));


    ?>
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header clearfix">
                    <h5 class="modal-title pull-left mt-2" id="exampleModalLabel">ADD PO</h5>
                    <button type="button" class="close  pull-right" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <input type="hidden" id="req_id" value="">
                            <input type="text" id="req_no" value="" class="form-control" readonly>
                        </div>
                        <div class="col-md-6">
                            <?php
                            $command = Yii::app()->db->createCommand();
                            $command->select('p.p_id,p.purchase_no');
                            $command->from($this->tableNameAcc('purchase', 0) . ' as p');

                            $command->where(' p.purchase_status = "saved" '
                                . 'AND p.purchase_no IS NOT NULL ');
                            $command->order('p.p_id');
                            $purchase = $command->queryAll();
                            $data = CHtml::listData($purchase, 'p_id', 'purchase_no');

                            echo CHtml::dropDownList(
                                '',
                                'purchase_id',
                                $data,
                                array('empty' => '-Select Purchase-', 'class' => 'select_box', 'id' => 'po_number')
                            );
                            ?>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">

                    <button type="button" class="btn btn-primary add_po">Save</button>
                </div>
            </div>
        </div>
    </div>
</div>




<script>
    $(".select_box").select2();
    $(document).on('click', ".approve", function () {
        if (!confirm("Do you want to Approve ?")) { } else {

            var id = this.id;
            $.ajax({
                url: '<?php echo Yii::app()->createUrl('materialRequisition/approveentry'); ?>',
                data: {
                    id: id
                },
                method: "POST",
                dataType: 'json',
                success: function (result) {

                    if (result.status == 1) {
                        $('.alert').removeClass('hide');
                        $('.alert').css('display', 'block');
                        $('.alert').append(result.message);
                        setTimeout(location.reload.bind(location), 1000);
                    } else {
                        $('.alert').removeClass('hide');
                        $('.alert').css('display', 'block');
                        $('.alert').append(result.message);
                    }
                }
            })
        }
    });


    $(document).on('click', ".reject", function () {
        var id = this.id;
        $.ajax({
            "type": "POST",

            "url": "<?php echo Yii::app()->createUrl('materialRequisition/reject_mr'); ?>",
            "dataType": "json",
            "data": {
                id: id
            },
            "success": function (data) {
                $("#reject_form").html(data).dialog({
                    title: "Reject MR"
                });
            }
        });
    });
    $(document).on('click', ".ui-dialog-titlebar-close", function () {

        window.location.reload();
    });
    $(document).on("click", ".btn_po", function () {
        var req_no = $(this).parent().attr('id');
        var req_id = $(this).parent().siblings().attr('id');
        $('#req_id').val(req_id);
        $('#req_no').val(req_no);

    });
    function deleteItem(mr_id) {

        if (confirm('Are you sure you want to delete this MR?')) {
            $.ajax({
                type: "GET",
                url: "<?php echo Yii::app()->createUrl('MaterialRequisition/deleteMRData'); ?>",
                dataType: "json",
                data: { mr_id: mr_id },
                success: function (data) {
                    if (data.status == 1) {
                        $('.alert').removeClass('hide');
                        $('.alert').css('display', 'block');
                        $('.alert').append(
                            'Material Requisition Deleted Successfully!'
                        );
                        setTimeout(location.reload.bind(location), 3000);

                    } else if (data.status == 0) {
                        $('.alert').removeClass('hide');
                        $('.alert').css('display', 'block');
                        $('.alert').append('Cannot Delete! Record is Already In Use');
                        setTimeout(location.reload.bind(location), 3000);
                    } else {
                        $('.alert').removeClass('hide');
                        $('.alert').css('display', 'block');
                        $('.alert').append('Some Error Occurred');
                        setTimeout(location.reload.bind(location), 3000);
                    }
                },
                error: function () {
                    alert('An error occurred while deleting the MR.');
                }
            });
        }
    }

    // $(document).on("click", ".add_po", function () {
    //     var po_number = $('#po_number').val();
    //     var req_id =$('#req_id').val();

    //     $.ajax({
    //         "type": "POST",
    //         "url": "<?php echo Yii::app()->createUrl('materialRequisition/addPurchaseData'); ?>",
    //         "dataType": "json",
    //         "data": {
    //             req_id: req_id,
    //             po_number:po_number
    //         },
    //         "success": function(data) {

    //             setTimeout(function(){
    //             window.location.reload(1);
    //             }, 1000);
    //         }
    //     });
    // });
    $(document).on("click", ".addpo", function () {
        // var po_number = $('#po_number').val();
        var req_id = $('#req_id').val();

        $.ajax({
            "type": "GET",
            "url": "<?php echo Yii::app()->createUrl('materialRequisition/view'); ?>",
            "dataType": "json",
            "data": {
                id: req_id,
                //po_number:po_number
            },
            "success": function (data) {

                setTimeout(function () {
                    window.location.reload(1);
                }, 1000);
            }
        });
    });

    $(document).on("click", "#materialrequest_refresh", function () {

        $.ajax({
            "type": "GET",
            "url": "<?php echo Yii::app()->createUrl('materialRequisition/refreshButton'); ?>",
            "dataType": "json",
            "success": function (data) {
                alert(data);
            }
        });
    });
</script>