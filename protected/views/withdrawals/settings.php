<?php
/* @var $this WithdrawalsController */
/* @var $model Withdrawals */
/* @var $form CActiveForm */
$this->breadcrumbs=array(
	'Withdrawals'=>array('index'),
	'Create',
);



Yii::app()->clientScript->registerScript('myscript', "

    $('.addtype').click(function(){
        var type = $('#type').val();
        if(type != ''){
        
            $.ajax({
                method: 'post',
                url: 'index.php?r=withdrawals/ajaxcreatetype',
                data: 'type='+type,
                //dataType: 'json',
                success: function (data) {

                    data = $.parseJSON(data);
                    if(data.status == 'success'){
                        location.reload();


    //                    $('.successcls').html('Type Created successfully').show();
    //                    $('.successcls').addClass('success');
    //                    $('.successcls').delay(2000).fadeOut('slow');
                    }else{
                        $('.successcls').html('Data not inserted').show();
                        $('.successcls').addClass('error');
                        $('.successcls').delay(2000).fadeOut('slow');
                    }


                }
            });
        }
        else{
            $('.typeErr').html('Please enter Type name').show();
            $('.typeErr').css('color','red');
            $('.typeErr').delay(2000).fadeOut('slow');

        }

    });
    
    $('.adddetail').click(function(){
        var detail = $('#detail').val();
        if(detail != ''){
            $.ajax({
                method: 'post',
                url: 'index.php?r=withdrawals/ajaxcreatedetail',
                data: 'detail='+detail,
                //dataType: 'json',
                success: function (data) {

                    data = $.parseJSON(data);
                    if(data.status == 'success'){
                        location.reload();


    //                    $('.successcls').html('Type Created successfully').show();
    //                    $('.successcls').addClass('success');
    //                    $('.successcls').delay(2000).fadeOut('slow');
                    }else{
                        $('.successcls').html('Data not inserted').show();
                        $('.successcls').addClass('error');
                        $('.successcls').delay(2000).fadeOut('slow');
                    }


                }
            });
        }
        else{
            $('.detailErr').html('Please enter Detail name').show();
            $('.detailErr').css('color','red');
            $('.detailErr').delay(2000).fadeOut('slow');

        }

    });
    
     
    var editmode = false;
    $('.editable').on('click',function(){
        if (editmode) {
            //$(this).text('Edit');
            $(this).closest('.typeItem').find('input.typeInput').hide();
            $(this).closest('.typeItem').find('span.typeCaption').text(
                $(this).find('input.typeInput').attr('value')
            ).show();
            editmode = false;
        }
        else{

            var prev = $(this).closest('.typeItem').find('input').val();
            //$(this).text('Save');
            $(this).closest('tr.typeItem').find('span.typeCaption').hide();
            $(this).closest('.typeItem').find('input').show();
            var sid = $(this).attr('data-id');
            
            $(this).closest('.typeItem').find('input.typeInput').bind('change', function() {
               
               var caption = $(this).val();
               if(caption != ''){
                    $.ajax({
                        method: 'post',
                        url: 'index.php?r=withdrawals/ajaxEditstatus',
                        data: 'caption='+caption+'&sid='+sid,
                        //dataType: 'json',
                        success: function (data) {

                            data = $.parseJSON(data);
                            if(data.status == 'success'){
                                editmode = false;

                            }else{
                                $(this).parent().find('.editErr').html('Data not saved').show();
                                $(this).parent().find('.editErr').css('color','red');
                            }


                        }
                    });
                    $(this).attr('value', $(this).val());
                    $(this).parent().find('span.typeCaption').text($(this).val());
                    
                }
                else{
                    $(this).closest('.typeItem').find('input').val(prev).show();

                }
            });
            editmode = true;
        }
     });
     
     


");


?>
<div class="container" id="settings">
<div>
<div style="float:right;margin-top: 25px;"><?php echo CHtml::link('Withdrawals',array('withdrawals/admin'),array('class'=>'btn del-btn')); ?></div>
<h2>Settings</h2>
</div>
<div class="successcls"></div> 

	<div class="row">
		<div class="col-md-6">
			<h4>Add New Item for Type</h4>
				<div class="form">
					<form id="form1">
						<div class="">
							<input type="text" name="type" id="type" placeholder="Type"/>
							<span class="typeErr"></span>
							<input type="hidden" name="status_type" value="withdrawal"/>
							<input type="button" class="addtype" value="Add"/>
						</div>
					</form>
				</div><!--form-->
   
    
				<div class="table">
					<table class="table">
						<thead>
							<tr>
								<th>Sl No.</th>
								<th>Type</th>
								<th>Actions</th> 
							</tr>
                
						</thead>
						<tbody>
								<?php
									$i = 1;
									foreach ($types as $type){
								?>
							<tr class="typeItem">
								<td><?php echo $i; ?></td>
								<td>
									<span class="typeCaption"><?php echo $type['caption']; ?></span>
									<input type="text" class="typeInput" value="<?php echo $type['caption']; ?>" style="display: none; width: 100px;"/>
									<span class="editErr"></span>
								</td>
								<td><button class="editable" data-id="<?php echo $type['sid']; ?>">Edit</button></td>
							</tr>
								<?php
									$i++;
									}
									
								?>
						</tbody>
					</table>
			    </div><!--table-->
			</div>

			<div class="col-md-6">
				<h4>Add New Item for Details</h4>
					<div class="form">
						<form id="form2">
							<div class="">
								<input type="text" name="detail" id="detail" placeholder="Detail"/>
								<span class="detailErr"></span>
								<input type="hidden" name="status_type2" value="withdrawal_details"/>
								<input type="button" class="adddetail" value="Add"/>
							</div>
						</form>
					</div>
    <?php //echo $this->renderPartial('_detailsettings', array('detailsmodel'=>$detailsmodel)); ?>
				 <div class="table">
					<table class="table">
						<thead>
							<tr>
								<th>Sl No.</th>
								<th>Detail Item</th>
								<th>Actions</th> 
							</tr>
							
						</thead>
						<tbody>
								<?php
									$j = 1;
									foreach ($details as $detail){
								?>
							<tr class="typeItem">
								<td><?php echo $j; ?></td>
								<td>
									<span class="typeCaption"><?php echo $detail['caption']; ?></span>
									<input type="text" class="typeInput" value="<?php echo $detail['caption']; ?>" style="display: none; width: 100px;"/>
								</td>
								<td><button class="editable" data-id="<?php echo $detail['sid']; ?>">Edit</button></td>
							</tr>
								<?php
									$j++;
									}
									
								?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
</div>