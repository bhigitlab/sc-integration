<?php
/* @var $this TasksController */
/* @var $model Tasks */
/* @var $form CActiveForm */
?>

<div class="form" style="margin-top:-20px">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'tasks-form',
        'enableAjaxValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true,
            'validateOnType' => false,),
            ));
    ?>

    <?php //echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->labelEx($model, 'title'); ?>
        <?php echo $form->textField($model, 'title', array('size' => 92, 'maxlength' => 100)); ?>
        <?php echo $form->error($model, 'title'); ?>
    </div>

    <div class="row toggle-field">
	<?php 
	if(Yii::app()->user->role==1)
		$display='block';
	else
		$display="none";
	?>
		
	
        <div class="subrow" style="display:<?php echo $display;?>">
	
		
            <?php echo $form->labelEx($model, 'project_id'); ?>
            <?php echo $form->dropDownList($model, 'project_id', CHtml::listData(Projects::model()->findAll(array('order' => 'name ASC')), 'pid', 'name'), array('empty' => 'Choose a project', 'style' => 'width:120px;')); ?>
            <?php echo $form->error($model, 'project_id'); ?>
        </div>

        <div class="subrow">
            <?php echo $form->labelEx($model, 'priority'); ?>
            <?php
            echo $form->dropDownList($model, 'priority', CHtml::listData(Status::model()->findAll(array(
                                'select' => array('sid,caption'),
                                'condition' => 'status_type="priority"',
                                'order' => 'caption',
                                'distinct' => true
                            )), 'sid', 'caption'), array('empty' => 'Choose a priority', 'style' => 'width:120px;'));
            ?>
            <?php echo $form->error($model, 'priority'); ?>
        </div>
		
		<div class="subrow">
		    <?php
//Default value for start and due date

    if (!isset($model->start_date))
        $model->start_date = date('Y-m-d');

    if (!isset($model->due_date)) {
        $cd = strtotime(date('Y-m-d'));
        $model->due_date = date('Y-m-d', mktime(0, 0, 0, date('m', $cd), date('d', $cd) + 7, date('Y', $cd)));
    }
    ?>
		
		
            <?php echo $form->labelEx($model, 'start_date'); ?>
            <?php echo CHtml::activeTextField($model, 'start_date', array("id" => "start_date",'size'=>10)); ?>
            <?php
            $this->widget('application.extensions.calendar.SCalendar', array(
                'inputField' => 'start_date',
                'ifFormat' => '%Y-%m-%d',
            ));
            ?>
            <?php echo $form->error($model, 'start_date'); ?>
        </div>
		
        <div class="subrow toggle-field">
            <?php echo $form->labelEx($model, 'due_date'); ?>
            <?php echo CHtml::activeTextField($model, 'due_date', array("id" => "due_date",'size'=>10)); ?>
            <?php
            $this->widget('application.extensions.calendar.SCalendar', array(
                'inputField' => 'due_date',
                'ifFormat' => '%Y-%m-%d',
            ));
            ?>
            <?php echo $form->error($model, 'due_date'); ?>
        </div>		
		
    </div>

    <div class="row">

		<div class="subrow">
            <?php
            $co_toggle = '';
            $rep_toggle = '';
            if (Yii::app()->user->role == 6) {
                if (!isset($model->coordinator)) {
                    $model->coordinator = Yii::app()->user->id;
                    $co_toggle = 'toggle-field';
                }
            } else {
                if (!isset($model->report_to)) {
                    $model->report_to = Yii::app()->user->id;
                    $rep_toggle = 'toggle-field';
                }
            }
            ?>
            <?php echo $form->labelEx($model, 'assigned_to'); ?>
			
			
            <?php if ($model->isNewRecord) 
					$condition = array('condition' => 'status=0','order' => 'first_name ASC');
					else
					$condition = array('order' => 'first_name ASC');
					
			echo $form->dropDownList($model, 'assigned_to', CHtml::listData(Users::model()->findAll($condition), 'userid', 'first_name'), array('empty' => '-Choose a resource-', 'style' => 'width:120px')); ?>
            <?php echo $form->error($model, 'assigned_to'); ?>
        </div>
		
		        <div class="subrow <?php echo $co_toggle; ?>">
            <?php echo $form->labelEx($model, 'coordinator'); ?>
            <?php echo $form->dropDownList($model, 'coordinator', CHtml::listData(Users::model()->findAll(array('condition' => 'status=0','order' => 'first_name ASC')), 'userid', 'first_name'), array('empty' => '-Choose co-ordinator-', 'style' => 'width:120px')); ?>
            <?php echo $form->error($model, 'coordinator'); ?>
        </div>  

		 <div class="subrow <?php echo $rep_toggle; ?>">
            <?php echo $form->labelEx($model, 'report_to'); ?>
            <?php echo $form->dropDownList($model, 'report_to', CHtml::listData(Users::model()->findAll(array('condition' => 'status=0','order' => 'first_name ASC')), 'userid', 'first_name'), array('empty' => '-Choose a reporting person-', 'style' => 'width:120px')); ?>
            <?php echo $form->error($model, 'report_to'); ?>
        </div>
		
		<div class="subrow toggle-field">
            <?php echo $form->labelEx($model, 'hourly_rate'); ?>
            <?php echo $form->textField($model, 'hourly_rate', array('size'=>10)); ?>
            <?php echo $form->error($model, 'hourly_rate'); ?>
        </div>
    </div>

    <div class="row toggle-field">
        <div class="subrow">
            <?php echo $form->labelEx($model, 'status'); ?>
            <?php
            echo $form->dropDownList($model, 'status', CHtml::listData(Status::model()->findAll(array(
                                'select' => array('sid,caption'),
                                'condition' => 'status_type="task_status"',
                                'order' => 'caption',
                                'distinct' => true
                            )), 'sid', 'caption'), array('empty' => 'Choose a status', 'style' => 'width:120px'));
            ?>
            <?php echo $form->error($model, 'status'); ?>
        </div>
		
        <div class="subrow">
            <?php echo $form->labelEx($model, 'billable'); ?>
            <div class="radio_btn">
                <?php
                echo $form->radioButtonList($model, 'billable', CHtml::listData(Status::model()->findAll(
                                        array(
                                            'select' => array('sid,caption'),
                                            'condition' => 'status_type="yesno"',
                                            'order' => 'caption',
                                            'distinct' => true
                                )), 'sid', 'caption'), array('separator' => ''));
                ?>
            </div>
            <?php echo $form->error($model, 'billable'); ?>
        </div>

        <div class="subrow">
            <?php echo $form->labelEx($model, 'total_hrs'); ?>
            <?php echo $form->textField($model, 'total_hrs', array('size'=>10)); ?>
            <?php echo $form->error($model, 'total_hrs'); ?>
        </div>

        <div class="subrow">
            <?php echo $form->labelEx($model, 'min_rate'); ?>
            <?php echo $form->textField($model, 'min_rate', array('size'=>10)); ?>
            <?php echo $form->error($model, 'min_rate'); ?>
        </div>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'description'); ?>
        <?php echo $form->textArea($model, 'description', array('rows' => 6, 'cols' => 70)); ?>
        <?php echo $form->error($model, 'description'); ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->

<style type="text/css">
    .row{
        padding:4px 0px;
    }

    .subrow{
        width: 125px;
    }
</style>
