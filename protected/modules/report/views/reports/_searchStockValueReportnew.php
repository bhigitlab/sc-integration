<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.min.js"></script>
<script>
    $('#reset').click(function() {
        location.href = '<?php echo $this->createUrl('stockValueReportnew'); ?>';
    })
    $(function() {
        $("#date_from").datepicker({
            dateFormat: 'dd-mm-yy'
        });

        $("#date_till").datepicker({
            dateFormat: 'dd-mm-yy'
        });

        $("#date_from").change(function() {
            $("#date_till").datepicker('option', 'minDate', $(this).val());
        });

        $("#date_till").change(function() {
            $("#date_from").datepicker('option', 'maxDate', $(this).val());
        });
    });

    $(document).ready(function() {
        $(".select_box").select2();

    });
</script>
<style>
    .search-form {
        background-color: #fafafa;
        padding: 10px;
        box-shadow: 0px 0px 2px 0px rgba(0, 0, 0, 0.1);
        margin-bottom: 10px;
    }

    .select2-container,
    .search-form select.form-control {
        width: 200px !important;
        display: inline-block;
    }

    .date-input {
        width: 120px;
        display: inline-block;
    }

    .select2-container--default .select2-selection--single {
        margin-top: -3px;
    }

    .select2-container--default .select2-selection--single .select2-selection__arrow {
        top: -2px;
    }

    .btn-search {
        margin-top: -5px;
    }
</style>

<div class="search-form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'action' => Yii::app()->createUrl($this->route),
        'method' => 'get',
    ));
    ?>
    <div class="search-content">
        <?php
        if (yii::app()->user->role == 5) {
            $warehouse_data = Controller::getCategoryOptions($type = 1);
            $data = CHtml::listData(Controller::getCategoryOptions($type = 1), 'id', 'text', 'group');
        } else {
            $warehouse_data = Controller::getCategoryOptions();
            $data = CHtml::listData(Controller::getCategoryOptions(), 'id', 'text', 'group');
        }

        if (!empty($model->warehousereceipt_warehouseid)) {
            $warehouse = $model->warehousereceipt_warehouseid;
        } else {
            if (count($warehouse_data) == 1) {
                $warehouse = $warehouse_data[0]['id'];
            } else {
                $warehouse = '';
            }
        }
        echo CHtml::dropDownList(
            'warehouse_id',
            $warehouse,
            $data,
            array('empty' => 'Choose Warehouse ', 'id' => 'warehouse', 'class' => ' select_box inputs target warehouse warehouse-from form-control')
        );
        ?>
        
        <select id="item_id" class="select_box form-control" name="item_id">
            <option value="">Select Item</option>
            <?php
            if (!empty($specification)) {
                foreach ($specification as $key => $value) {
            ?>
                    <option value="<?php echo $value['id'] ?>" <?php echo ($value['id'] == $model->warehousereceipt_itemid) ? 'selected' : ''; ?>><?php echo $value['data']; ?></option>
            <?php }
            } ?>
        </select>

        <?php
        if (!isset($_GET['date_from']) || $_GET['date_from'] == '') {
            $datefrom =  "";
        } else {
            $datefrom = $_GET['date_from'];
        }
        ?>
        <?php echo CHtml::textField('date_from', $datefrom, array("id" => "date_from", "placeholder" => "From", "readonly" => true, "class" => "form-control date-input")); ?>

        <?php
        if (!isset($_GET['date_to']) || $_GET['date_to'] == '') {

            $date_to =  '';
        } else {
            $date_to = $_GET['date_to'];
        }



        ?>
        <?php echo CHtml::textField('date_to', $date_to, array("id" => "date_till", "placeholder" => "To", "readonly" => true, "class" => "form-control date-input")); ?>


        <?php
        if (!isset($_GET['rate']) || $_GET['rate'] == '') {

            $rate = '';
        } else {
            $rate = $_GET['rate'];
        }

        ?>
        <?php
        echo CHtml::hiddenField('rate', $rate, array('id' => 'hiddenInput'));

        echo CHtml::submitButton('Filter', array('id' => 'usersearchbtn', 'class' => 'btn-search btn btn-sm btn-primary', 'style' => 'margin-left: 25px;'));

        echo CHtml::ResetButton('reset', array('style' => 'margin-left: 10px;', 'class' => 'btn-search btn btn-sm btn-default', 'id' => 'reset'));

        ?>

    </div>
    <?php $this->endWidget(); ?>
</div>