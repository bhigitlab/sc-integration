<style>
    .exp-list {
        padding-left: 0px !important;
    }

    .error {
        color: #ff0000;
    }
</style>
<?php $pms_api_integration=0;
$pms_api_integration=ApiSettings::model()->pmsIntegrationStatus();
 ?>
<!--<link rel="stylesheet" href="<?php // echo Yii::app()->theme->baseUrl; 
                                    ?>/plugins/message/css/jquery.message.css">-->
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">

<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/validation/jquery.validate.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/validation/additional-methods.js"></script>
<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$this->breadcrumbs = array(
    'Purchase List',
)
?>
<div class="container" id="project">
    <div class="loading-overlay">
        <span class="fa fa-spinner fa-3x fa-spin"></span>
    </div>
    <div class="clearfix">
        <div class="add-btn">
            <a class="button addunit">Add Unit</a>
        </div>
        <h2>Unit</h2>
    </div>
    <?php
        foreach (Yii::app()->user->getFlashes() as $key => $message) {
            echo '<div class="flash-' . $key . ' alert alert-success flash_msg" style=" width:335px;"> <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>' . $message . "</div>\n";
        }
        ?>
    <div class="panel panel-gray" id="unitform" style="display:none;">
    <input type="hidden" id="pms_integration" value="<?php echo $pms_api_integration ;?>">
    <?php echo CHtml::hiddenField('execute_api', 0, ['id' => 'execute_api']); ?>

        <div class="panel-heading form-head">
            <h3 class="panel-title">Add Unit</h3>
        </div>
        <span class="error_message"></span>
        <form class="" id="form_unit">
        
        <input type="hidden" id="pms_unit_id" value="">
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-4 form-group">
                        <?php echo $model->id;
                        // echo "<pre>";
                        // print_r($model->id); 
                        ?>
                        <input type="hidden" name="id" id="id" value="">
                        <input type="text" name="unit" class="form-control" id="unit" placeholder="Unit">
                        <div class="errorMessage"></div>
                    </div>
                    <div class="col-md-4">
                        <button type="button" id="submit" class="btn btn-info btn-sm">Add</button>
                        <button type="reset" class="btn btn-default btn-sm" onclick="closeaction(this,event)">Close</button>
                    </div>
                </div>
            </div>
        </form>
    </div>




    <?php if (Yii::app()->user->hasFlash('success')) : ?>
        <div class="info" style="width: 500px;margin-left: 400px;padding: 8px;color: green;font-size: 20px;font-weight: bold; padding-top: 46px;">
            <?php echo Yii::app()->user->getFlash('success'); ?>
        </div>
    <?php endif; ?>


    <?php if (Yii::app()->user->hasFlash('error')) : ?>
        <div class="info" style="width: 500px;margin-left: 400px;padding: 8px;color: red;font-size: 20px;font-weight: bold; padding-top: 46px;">
            <?php echo Yii::app()->user->getFlash('error'); ?>
        </div>
    <?php endif; ?>

    <div id="msg_box"></div>
    <div class="row explist">
        <div class="col-md-12">
            <?php $this->widget('zii.widgets.CListView', array(
                'dataProvider' => $dataProvider,
                'itemView' => '_newview',
                 'template' => '
                 <div class="container1">
                 <table cellpadding="10" id="exptypetbl" class="table">{items}
                 </table>
                 </div>{pager}',
                'ajaxUpdate' => false,
            )); ?>

        </div>
        <?php
        Yii::app()->clientScript->registerScript('myjavascript', '
             $(document).ready(function(){
               $("#exptypetbl").dataTable( {
                    "scrollY": "300px",
                    "scrollCollapse": true,
                    "paging": false,
                     "columnDefs"        : [       
                        { 
                            "searchable"    : false, 
                            "targets"       : [0,2] 
                        },
                        { "bSortable": false, "aTargets": [-1]  }
                    ],
                } );
                });

            ');
        ?>
    </div>
</div>



<div id='FlickrGallery'></div>



<style>
    a.button {
        background-color: #6a8ec7;
        display: block;
        padding: 5px;
        color: #fff;
        cursor: pointer;
        float: right;
        border: 1px solid #6a8ec8;
    }

    .page-body h3 {
        margin: 4px 0px;
        color: inherit;
        text-align: left;
    }

    .panel {
        border: 1px solid #ddd;
    }

    .panel-heading {
        background-color: #eee;
        height: 40px;
    }
</style>
<script>
    function closeaction() {
        $("#unitform").slideUp();
    };
    $(document).ready(function() {
        $(".addunit").click(function() {
            $("#unitform").slideDown();
        });

        $("#form_unit").validate({
            rules: {
                unit: {
                    required: true,
                    remote: {
                        url: "<?php echo  Yii::app()->createAbsoluteUrl('Unit/checkunit'); ?>",
                        type: "get",
                        data: {
                            unit: function() {
                                return $("#unit").val();
                            }
                        }
                    }
                },
            },
            messages: {
                unit: {
                    required: "Please enter unit",
                    remote: jQuery.validator.format("{0} is already taken.")
                }
            },
        });

        $('#submit').click(function() {
            var pms_integration = $("#pms_integration").val();
            var pms_unit_id = $("#pms_unit_id").val();

            // Handle null or undefined value for pms_unit_id
            if (pms_unit_id === null || pms_unit_id === undefined || pms_unit_id === '') {
                pms_unit_id = '0';
            }

            if (pms_integration == '1') {
                if (pms_unit_id == '0') {
                    var confirmApiCall = confirm("Do you want to save the unit in Pms?");
                    if (confirmApiCall) {
                        document.getElementById('execute_api').value = 1;
                    }
                } else if (pms_unit_id != '0') {
                    document.getElementById('execute_api').value = 1;
                }
            }


            if ($("#form_unit").valid()) {
                var urls = '<?php echo  Yii::app()->createAbsoluteUrl('unit/newlist'); ?>'
                var unit = $('#unit').val();
                var executeApiValue = $("#execute_api").val();
                var id = $('#id').val();
                $('.loading-overlay').addClass('is-active');
                $.ajax({
                    url: '<?php echo  Yii::app()->createAbsoluteUrl('unit/create'); ?>',
                    type: 'GET',
                    data: {
                        data: unit,
                        id:id,
                        executeApiValue:executeApiValue
                    },
                    async: true,
                    success: function(response) {
                        $('.explist').html(response);
                        $('#unit').val('');
                        $("#unitform").slideUp();
                        location.reload();
                    }
                });
            }
        });


        $('#unit').keypress(function(event) {
            var val = $(this).val();
            var keycode = (event.keyCode ? event.keyCode : event.which);
            if (keycode == '13') {
                var response =  onlySpecialchars(val);  
                if(response.message==""){
                    $('#submit').click();
                    event.preventDefault();
                }
            }
        });


    })
    $(document).ajaxComplete(function() {
        $('.loading-overlay').removeClass('is-active');
        $('#loading').hide();
    });


    function onlySpecialchars(str)
    {        
        var regex = /^[^a-zA-Z0-9]+$/;
        var message =""; 
        var disabled=false;    
        var matchedAuthors = regex.test(str);
         
        if (matchedAuthors) {
            var message ="Special characters not allowed"; 
            var disabled=true;           
        } 
        return {"message":message,"disabled":disabled};        
    }
      

    $("#unit").keyup(function () {         
        var response =  onlySpecialchars(this.value);      
        $(this).siblings(".errorMessage").show().html(response.message);    
        $("#submit ").attr('disabled',response.disabled);
    });
    $(document).on('click','.editUnit',function(e) {
        var unit = $(this).parents("tr").find("td:nth-child(3)").text();
        var id = $(this).attr("data-id"); 
        var pms_unit_id= $(this).attr("data-pms-unit-id");      
        $("#unit").val(unit); 
        $("#pms_unit_id").val(pms_unit_id);  
        $("#id").val(id); 
        $("#submit").text("Update");
        $("#unitform").slideDown();
    });

</script>
