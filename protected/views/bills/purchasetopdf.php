<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/pdfstyle.css">

<?php
    $company_address = Company::model()->findBypk(($company_id)?$company_id:Yii::app()->user->company_id);	
?>
    
    <h3 class="text-center">Purchase Bill</h3> 
<table class="table table-bordered table-striped" border="1">

    <thead>
        <tr width="100%">
         
            <th width="2%">Sl No.</th>
            <th width="13%">Purchase NO</th>
            <th width="10%">Bill no</th>
            <th width="10%">Company</th>
            <th width="10%">Vendor</th>
            <th width="7%">Project</th>            
            <th width="10%">Bill Date</th>
            <th width="11%">Total Amount</th>
        </tr> 
        <?php
if (!empty($model)) {
        
    $tot_billamount=0;
    $tot_paidamount=0;
    $balance=0;
    $tot_bal =0;

    foreach ($model as $key => $data) {
       
       $balance = $data['bill_totalamount'] - $data['paid'];
       $tot_billamount += $data['bill_totalamount'];
       $tot_paidamount += $data['paid'];
       $tot_bal += $balance;
    }
}

?>
        <tr>

<th colspan="7"  class="text-right">Total</th>
<td><?= Controller::money_format_inr($tot_billamount,2,1) ?></td>

  
</tr>  
    </thead>
    <tbody>


    <?php   
    if (!empty($model)) {
        
        $tot_billamount=0;
        $tot_paidamount=0;
        $balance=0;
        $tot_bal =0;

        foreach ($model as $key => $data) {
           
           $balance = $data['bill_totalamount'] - $data['paid'];
           $tot_billamount += $data['bill_totalamount'];
           $tot_paidamount += $data['paid'];
           $tot_bal += $balance;
       
    ?>


            <tr>
               <td><?= $key+1;?></td>
               <td><?= $data['purchase_no'] ?></td>
               <td><?= $data['bill_number'] ?></td>
               <td>
                <?php
                 $company = Company::model()->findByPk($data['company_id']);
                 echo $company->name;
                ?>
                </td>
               <td><?= $data['vendorname'] ?></td>
               <td><?= $data['projectname'] ?></td>
               <td><?= date('d-m-Y', strtotime($data['bill_date'])) ?></td>
               <td><?= Controller::money_format_inr($data['bill_totalamount'],2,1); ?></td>
<!--               <td><?= Controller::money_format_inr($data['paid'],2,1); ?></td>
               <td><?= Controller::money_format_inr($balance,2,1); ?></td>-->
		
            </tr>
             

        <?php }  } ?>

          <!-- <tr>

               <th colspan="7" style="text-align: right;">Total</th>
               <td><?= Controller::money_format_inr($tot_billamount,2,1) ?></td>
              <td><?= Controller::money_format_inr($tot_paidamount,2,1) ?></td>
               <?= Controller::money_format_inr($tot_bal,2,1) ?></td>
                 
             </tr> -->
	    </tbody>
 </table>
