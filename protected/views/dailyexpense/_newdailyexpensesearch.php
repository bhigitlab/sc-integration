<?php
/* @var $this ExpensesController */
/* @var $model Expenses */
/* @var $form CActiveForm */
?>

<div class="page_filter">
	<?php $form = $this->beginWidget('CActiveForm', array(
		'action' => Yii::app()->createUrl('dailyexpense/dailyexpensebyuser'),
		'method' => 'get',
	)); ?>
	<div class="row">
		<div class=" form-group col-xs-12 col-sm-3 col-md-2">
			<label>From</label>
			<input type="text" name="fromdate" id="Dailyexpense_fromdate" class="form-control" autocomplete="off"
				readonly=true placeholder="From Date"
				value="<?php echo ($model->fromdate) ? date("d-m-Y", strtotime($model->fromdate)) : ""; ?>" />
		</div>
		<div class=" form-group col-xs-12 col-sm-3 col-md-2">
			<label>To</label>
			<input type="text" name="todate" id="Dailyexpense_todate" class="form-control" autocomplete="off"
				readonly=true placeholder="To Date"
				value="<?php echo ($model->todate) ? date("Y-m-d", strtotime($model->todate)) : ""; ?>" />
		</div>
		<div class="form-group col-xs-12 col-sm-3 col-md-2 text-right text-sm-left">
			<label>&nbsp;</label>
			<div>
				<button type="submit" class="btn btn-sm btn-primary" name="submitForm" id="submitForm">Go</button>
				<button type="button" class="btn btn-sm btn-default" name="clearForm" id="clearForm">Clear</button>
			</div>
		</div>
	</div>
	<?php $this->endWidget(); ?>

</div><!-- search-form -->