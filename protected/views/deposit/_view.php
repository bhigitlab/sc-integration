<?php
/* @var $this DepositController */
/* @var $data Deposit */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('deposit_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->deposit_id), array('view', 'id'=>$data->deposit_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('deposit_name')); ?>:</b>
	<?php echo CHtml::encode($data->deposit_name); ?>
	<br />


</div>