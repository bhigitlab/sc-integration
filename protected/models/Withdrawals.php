<?php

/**
 * This is the model class for table "withdrawals".
 *
 * The followings are the available columns in table 'withdrawals':
 * @property integer $exp_id
 * @property integer $projectid
 * @property integer $userid
 * @property string $date
 * @property double $amount
 * @property string $description
 * @property integer $type
 * @property integer $created_by
 * @property string $created_date
 * @property integer $updated_by
 * @property string $updated_date
 */
class Withdrawals extends CActiveRecord
{
	
	
	public $fromdate;
	public $todate;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Withdrawals the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{withdrawals}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('projectid, userid,type,details', 'required','message'=>'Please choose {attribute} '),
			array('date, amount, description','required','message'=>'Please enter {attribute}'),
			array('projectid, userid, type, created_by, updated_by', 'numerical', 'integerOnly'=>true),
			array('amount', 'numerical'),
			array('description', 'length', 'max'=>300),
			array('created_by, created_date, updated_by, updated_date', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('exp_id, projectid, userid, date, amount, description, type, created_by, created_date, updated_by, updated_date, fromdate, todate', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
	                    'createdBy' => array(self::BELONGS_TO, 'Users', 'created_by'),
	                    'updatedBy' => array(self::BELONGS_TO, 'Users', 'updated_by'),
	                    'project' => array(self::BELONGS_TO, 'Projects', 'projectid'),
	                    'user' => array(self::BELONGS_TO, 'Users', 'userid'),
	                    'status0' => array(self::BELONGS_TO, 'Status', 'type'),
	                    'status1' => array(self::BELONGS_TO, 'Status', 'details'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'exp_id' => 'Exp',
			'projectid' => 'Project',
			'userid' => 'User',
			'date' => 'Date',
			'amount' => 'Amount',
			'description' => 'Description',
			'type' => 'Type',
            			'details' => 'Details',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
			'updated_by' => 'Updated By',
			'updated_date' => 'Updated Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search($userid=0,$pid=0)
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		if(Yii::app()->user->role ==2){
		$criteria->addCondition("created_by ='".Yii::app()->user->id."'");
		}

		if(!empty($this->fromdate) && empty($this->todate))
		{
			$criteria->addCondition("date >= '".date('Y-m-d',strtotime($this->fromdate))."'");  // date is database date column field
		}elseif(!empty($this->todate) && empty($this->fromdate))
		{
			$criteria->addCondition("date <= '".date('Y-m-d',strtotime($this->todate))."'");
		}elseif(!empty($this->todate) && !empty($this->fromdate))
		{
			$criteria->addCondition("date between '".date('Y-m-d',strtotime($this->fromdate))."' and  '".date('Y-m-d',strtotime($this->todate))."'");
		}


		//$criteria->compare('exp_id',$this->exp_id);
		if(!empty($this->projectid)){
		$criteria->compare('projectid',($pid==0?$this->projectid:$pid));
		}else{
		$criteria->compare('projectid', $this->projectid);
		}
		if(!empty($this->userid)){
		$criteria->compare('userid',($userid==0?$this->userid:$userid));
		}else{
		$criteria->compare('userid', $this->userid);
		}

		 if(Yii::app()->user->role ==2){
		 //   $criteria->compare('created_by',Yii::app()->user->id);
	              }else{
		 //   $criteria->compare('created_by',$this->created_by);
	              } 

		// $criteria->compare('date',$this->date,true);
		//$criteria->compare('amount',$this->amount);
		//$criteria->compare('description',$this->description,true);
		//$criteria->compare('type',$this->type);
		//$criteria->compare('details',$this->details);
	 
		//$criteria->compare('created_date',$this->created_date,true);
		//$criteria->compare('updated_by',$this->updated_by);
		//$criteria->compare('updated_date',$this->updated_date,true);
		

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>array("defaultOrder"=>"exp_id DESC")
		));
	}
	/*
	public function beforeSave()
	{
	    $this->date = date('Y-m-d', strtotime($this->date));
	    return parent::beforeSave();
	}
	*/
}