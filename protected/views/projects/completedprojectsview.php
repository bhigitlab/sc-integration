<?php
/* @var $this ProjectsController */

/* @var $model Projects */



$this->breadcrumbs = array(
    'Projects' => array('index'),
    'Manage',
);







Yii::app()->clientScript->registerScript('search', "

$('.search-button').click(function(){

	$('.search-form').toggle();

	return false;

});

$('.search-form form').submit(function(){

	$.fn.yiiGridView.update('projects-grid', {

		data: $(this).serialize()

	});

	return false;

});



   /* $('#download').click(function () {





        html2canvas($('#financialreport1'), {

            onrendered: function (canvas) {

                var imgData = canvas.toDataURL('image/jpeg');



                var imgWidth = 210;

                var pageHeight = 295;

                var imgHeight = canvas.height * imgWidth / canvas.width;

                var heightLeft = imgHeight;

                var doc = new jsPDF('p', 'mm');

                var position = 0;

                doc.page = 1; // use this as a counter.



                doc.addImage(imgData, 'jpeg', 0, position, imgWidth, imgHeight);

                heightLeft -= pageHeight;



                while (heightLeft >= 0) {

                    position = heightLeft - imgHeight;

                    doc.addPage();

                    doc.addImage(imgData, 'jpeg', 0, position, imgWidth, imgHeight);

                    heightLeft -= pageHeight;

                }

                doc.save('financialreport.pdf');



            }

        });

    });*/



    /*

        var doc = new jsPDF();

        var specialElementHandlers = {

        '#editor': function (element, renderer) {

        return true;

        }

        };



        $(document).ready(function() {

        $('#download').click(function () {

        doc.fromHTML($('#financialreport1').html(), 15, 15, {

        'width': 170,

        'elementHandlers': specialElementHandlers

        });

        doc.save('sample-content.pdf');

        });

        });*/

");



$tblpx = Yii::app()->db->tablePrefix;
?>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/tableHeadFixer.js"></script>


<style type="text/css">

    .grandtotal td{

        border:none !important;

    }
    #financialreport1 h3{color:inherit;text-align:left;margin: 15px 0px;}
    #parent1,#parent2{max-height: 300px;}
     #parent1,#parent2{max-height: 300px;}
    table thead th,table tfoot tr{background-color: #eee;}
    .table{margin-bottom: 0px;}
</style>

<div class="container" id="project">

<!--    <br/><br/>-->
    <div id="financialreport1" >
<!--        <br/>-->
    <div class="clearfix">
        <h2 class="pull-left">Completed Project Report  : <?php echo date('d-M-Y');?></h2>
        <div class="pull-right">
            <a style="cursor:pointer;margin-top:15px; " id="download" class="save_btn" href="<?php echo $this->createAbsoluteUrl('projects/savetocompletedpdf') ?>">SAVE AS PDF</a></span>
            <a style="cursor:pointer;padding-left:5px;margin-top:15px; " id="download" class="save_btn" href="<?php echo $this->createAbsoluteUrl('projects/savetoexcelcompleted') ?>">SAVE AS EXCEL</a>
        </div>
    </div>
<!--        <br/><br/>-->
        <div class="">

             <h3>Fixed Rate Contracts</h3>
            <div id="parent1">
                <table class="table  tab1" id="fixTable1">
                    <thead>
                        <tr>
                            <th>Sl No.</th>
                            <th>Project Name</th>
                            <th>SQ. FT.</th>
                            <th>SQ.FT.Rate or Lumpsum</th>
                            <th>Receipts</th>
                            <th>Expenses</th>
                            <th>Invoice</th>
                            <th>Deficit</th>
                            <th>Surplus</th>
                            <th>Remarks</th>
                        </tr>
                    </thead>

                    <tbody>

                        <?php
                        $reportdata = $financialdata1->getData();
						//echo "<pre>"; print_r($reportdata); die;
                        if (!empty($reportdata)) {

                            $i = 0;

                            $receipt_grandtotal = 0;

                            $deficit_grandtotal = 0;

                            $surplus_grandtotal = 0;
                            $paid_total = 0;
                            foreach ($reportdata as $data) {

                                $i++;

                                $projectid = $data['pid'];

                                $inv_dtls = Yii::app()->db->createCommand("SELECT SUM(amount) as total_amount,SUM(tax_amount) as tax_amount FROM {$tblpx}invoice WHERE project_id = ".$projectid."")->queryRow();
                                if(($inv_dtls)){
									if(($inv_dtls['total_amount'] + $inv_dtls['tax_amount']) == 0) {
										$invoice = 0;
									} else {
										$invoice = $inv_dtls['total_amount'] + $inv_dtls['tax_amount'];
									}
							    } else {
										$invoice = 0;
								}

                                $percentage = Yii::app()->db->createCommand("SELECT percentage FROM {$tblpx}projects WHERE pid = {$projectid}")->queryRow();
                                $servise_charge = ($data['tot_expense'] * $percentage['percentage']) / 100;


                                $receipt_grandtotal += $data['tot_receipt'];

								setlocale(LC_MONETARY, 'en_IN');


									if($invoice == 0){

									$diff =($data['tot_expense']+$servise_charge) - $data['tot_receipt'];

									}else{

									$diff = $invoice - $data['tot_receipt'];	// Invoice - Receipt

									}

									$deficit = 0;

									$surplus = 0;

									if ($diff < 0) {

										$surplus = $diff;
									} else {

										$deficit = $diff;

									}


								/*   modified by sikha ends */
								$deficit_grandtotal += $deficit;
								$surplus_grandtotal += $surplus;

                                ?>

                                <tr>
                                    <td><?= $i; ?></td>

                                    <td><?= $data['name']; ?></td>
                                    <td align="right"><?= $data['sqft']; ?></td>
                                    <td align="right"><?= $data['sqft_rate']; ?></td>
                                    <td align="right">
									<?php
									$amount = Controller::money_format_inr($data['tot_receipt'],2); //money_format('%!.0n', $data['tot_receipt']);
									echo $amount;
									?>
									</td>
                                    <td align="right"><?php echo Controller::money_format_inr($data['tot_expense'],2);  ?></td>
                                    <td><?php echo Controller::money_format_inr($invoice,2);  //echo $invoice; ?></td>
                                    <td align="right"><?php echo Controller::money_format_inr($deficit,2,1);  ?></td>

                                    <td align="right"><?php echo Controller::money_format_inr($surplus,2,1);  ?></td>
                                    <td><?= $data['remarks']; ?></td>

                                </tr>

                                <?php
                                $paid_total += $data['tot_paid_to_vendor'];
                            }
                            ?>
                        <tfoot>
                            <tr class="grandtotal">
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th style="text-align:right;"><?php echo Controller::money_format_inr($receipt_grandtotal,2);  ?></th>
                                <th></th>
                                <th></th>
                                <th style="text-align:right;"><?php echo Controller::money_format_inr($deficit_grandtotal,2,1);  ?></th>
                                <th style="text-align:right;"><?php echo Controller::money_format_inr($surplus_grandtotal,2,1);  ?></th>
                                <th></td>
                            </tr>
                        </tfoot>

                            <?php
                        } else {
                            ?>

                            <tr>

                                <td colspan="12" class="text-center">No results found.</td>

                            </tr>

                        <?php } ?>

                    </tbody>

                </table>
            </div>
        </div>

          <h3>Fixed Percentage Contracts</h3>
        <div class="">
            <div id="parent2">
                <table class="table  tab2" id="fixTable2">

                    <thead>

                        <tr>
                            <th>Sl No.</th>
                            <th>Project Name</th>
                            <th>SQ. FT.</th>
                            <th>Fixed Percentage</th>
                            <th>Receipts</th>
                            <th>Expenses</th>
                            <th>Service Charge</th>
                            <th>Invoice</th>
                            <th>Deficit</th>
                            <th>Surplus</th>
                            <th>Remarks</th>

                        </tr>
                    </thead>

                    <tbody>

                        <?php
                        $reportdata = $fixedprojects->getData();
					//	echo "<pre>"; print_r($reportdata); die;
                        if (!empty($reportdata)) {

                            $i = 0;
                            $receipt_grandtotal = 0;

                            $deficit_grandtotal = 0;

                            $surplus_grandtotal = 0;
                            $paid_total = 0;
                            foreach ($reportdata as $data) {

                                $i++;

                                $projectid = $data['pid'];

                                $inv_dtls = Yii::app()->db->createCommand("SELECT SUM(amount) as total_amount,SUM(tax_amount) as tax_amount FROM {$tblpx}invoice WHERE project_id = ".$projectid."")->queryRow();
                                if(($inv_dtls)){
									if(($inv_dtls['total_amount'] + $inv_dtls['tax_amount']) == 0) {
										$invoice = 0;
									} else {
										$invoice = $inv_dtls['total_amount'] + $inv_dtls['tax_amount'];
									}
							    } else {
										$invoice = 0;
								}

                                $percentage = Yii::app()->db->createCommand("SELECT percentage FROM {$tblpx}projects WHERE pid = {$projectid}")->queryRow();
                                $servise_charge = ($data['tot_expense'] * $percentage['percentage']) / 100;



                                $receipt_grandtotal += $data['tot_receipt'];

                                $paid_total += $data['tot_paid_to_vendor'];


									if($invoice == 0){

									$diff =($data['tot_expense']+$servise_charge) - $data['tot_receipt']; //Receipt - Expense - Service Charge

									}else{

									$diff = $invoice - $data['tot_receipt'];

									}

									$deficit = 0;

									$surplus = 0;

									if ($diff < 0) {
											$surplus = $diff;

									} else {
										$deficit = $diff;

									}



								$deficit_grandtotal += $deficit;
								$surplus_grandtotal += $surplus;
                                ?>

                                <tr>
                                    <td><?= $i; ?></td>
                                    <td><?= $data['name']; ?></td>
                                    <td align="right"><?= $data['sqft']; ?></td>
                                    <td align="right"><?php echo (($data['percentage']) ? $data['percentage'] . '%' : ''); ?></td>
                                    <td align="right"><?php echo Controller::money_format_inr($data['tot_receipt'],2);  ?></td>
                                    <td align="right"><?php echo Controller::money_format_inr($data['tot_expense'],2);  ?></td>
                                    <td align="right"><?php echo Controller::money_format_inr($servise_charge,2);  ?></td>
                                    <td><?php echo Controller::money_format_inr($invoice,2);  //echo $invoice; ?></td>
                                    <td align="right"><?php echo Controller::money_format_inr($deficit,2,1);  ?></td>
                                    <td align="right"><?php echo Controller::money_format_inr($surplus,2,1);  ?></td>
                                    <td><?= $data['remarks']; ?></td>
                                </tr>

                                <?php
                            }
                            ?>
                        <tfoot>
                            <tr class="grandtotal">
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th style="text-align:right;"><?php echo Controller::money_format_inr($receipt_grandtotal,2);  ?></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th align="right"><?php echo Controller::money_format_inr($deficit_grandtotal,2,1);  ?></th>
                                <th align="right"><?php echo Controller::money_format_inr($surplus_grandtotal,2,1);  ?></th>
                                <th></th>
                            </tr>
                        </tfoot>
                            <?php
                        } else {
                            ?>
                            <tr>
                                <td colspan="12" class="text-center">No results found.</td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>



</div>


<script>
    $(document).ready(function() {
        $("#fixTable1").tableHeadFixer({'left' : false, 'foot' : true, 'head' : true});
        $("#fixTable2").tableHeadFixer({'foot' : true, 'head' : true});
	});


</script>
