<?php
$tblpx = Yii::app()->db->tablePrefix;
if (Yii::app()->user->role == 1) {
    $bill_status = Yii::app()->db->createCommand("SELECT *  FROM {$tblpx}purchase LEFT JOIN {$tblpx}status ON 
    {$tblpx}purchase.purchase_billing_status = {$tblpx}status.sid WHERE p_id = " . $model->p_id . " AND 
    ({$tblpx}status.caption='Pending to be Billed' OR {$tblpx}status.caption='Fully Billed' OR {$tblpx}status.caption='Partially Billed') AND 
    {$tblpx}status.status_type='purchase_bill_status'")->queryRow();
} else {
    $bill_status = Yii::app()->db->createCommand("SELECT *  FROM {$tblpx}purchase LEFT JOIN {$tblpx}status ON 
{$tblpx}purchase.purchase_billing_status = {$tblpx}status.sid WHERE p_id = " . $model->p_id . " AND 
{$tblpx}status.caption='Pending to be Billed'  AND 
{$tblpx}status.status_type='purchase_bill_status'")->queryRow();
}
?>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/validation/jquery.validate.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/validation/additional-methods.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/tableHeadFixer.js"></script>
<style>
    .lefttdiv {
        float: left;
    }

    .purchase-title {
        border-bottom: 1px solid #ddd;
    }

    .purchase_items h3 {
        font-size: 18px;
        color: #333;
        margin: 0px;
        padding: 0px;
        text-align: left;
        padding-bottom: 10px;
    }

    .purchase_items {
        padding: 15px;
        box-shadow: 0 0 13px 1px rgba(0, 0, 0, 0.25);
    }

    .purchaseitem {
        display: inline-block;
        margin-right: 20px;
    }

    .purchaseitem last-child {
        margin-right: 0px;
    }

    .purchaseitem label {
        font-size: 12px;
    }

    .remark {
        display: none;
    }

    .padding-box {
        padding: 3px 0px;
        min-height: 17px;
        display: inline-block;
    }

    th {
        height: auto;
    }

    .quantity,
    .rate {
        max-width: 80px;
    }

    .text_align {

        text-align: center;
    }


    *:focus {
        border: 1px solid #333;
        box-shadow: 0 0 6px 1px rgba(0, 0, 0, 0.25);
    }

    .text-right {
        text-align: right;
    }

    .block_purchase {
        padding-left: 0px;
    }

    .row_block .elem_block {
        padding: 0px 6px;
    }

    #loader {
        display: none;
    }

    .error {
        color: #ff0000;
    }

    label.error {
        font-weight: normal;
    }

    .pdf_excel {
        cursor: pointer;
    }

    .user_company_edit_class {
        background-color: red;
        color: white;
        font-weight: bold;
        padding: 5px;
    }
</style>
<script>
    $(function() {
        $("#datepicker").datepicker({
            dateFormat: 'dd-mm-yy'
        }).datepicker("setDate", new Date());

    });
</script>
<?php
$company = Company::model()->findBypk($model->company_id);
$purchase_data = Purchase::model()->findByPK($model->purchase_no);
?>
<div class="container">
    <div class="invoicemaindiv">
        <?php
        if ($model->purchase_status == 'saved') {
        ?>
            <a style="float:right;margin-top: 15px;" id="download" class="pdfbtn1 pdf_excel">
                <div class="save_pdf"></div>SAVE AS PDF
            </a>
        <?php
        }
        if ($model->purchase_status == 'saved') {
        ?>
            <a style="float:right;;margin-right:2px;margin-top: 15px;" id="download" class="excelbtn1 pdf_excel">
                <div class="save_pdf"></div>SAVE AS EXCEL
            </a>
        <?php } ?>
        <?php
        if (isset(Yii::app()->user->role) && (in_array('/purchase/sendattachment', Yii::app()->user->menuauthlist))) {
            if ($model->purchase_status == 'saved') {
        ?>
                <a data-id="<?php echo $model->p_id; ?>" style="float:right;;margin-right:2px;margin-top: 15px;" id="sendmail" class="pdf_excel sendmail">
                    SEND MAIL
                </a>
        <?php
            }
        }
        ?>
        <h2 class="purchase-title">Purchase Order</h2>
        <br>
        <div id="send_form" style="display:none;">
            <div class="panel panel-gray">
                <div class="panel-heading form-head">
                    <h3 class="panel-title">Send Mail</h3>
                </div>
                <form id="form_send">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-6">
                                <input type="hidden" name="p_id" id="p_id" value="<?php echo $model->p_id; ?>">
                                <input type="hidden" name="pdf_path" id="pdf_path" value="">
                                <input type="hidden" name="pdf_name" id="pdf_name" value="">
                                <div class="form-group">
                                    <label for="ProjectType_project_type" class="required">To</label>
                                    <input id="mail_to" name="mail_to" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <input type="hidden" name="p_id" id="p_id" value="<?php echo $model->p_id; ?>">
                                <div class="form-group">
                                    <label for="ProjectType_project_type" class="required">Message</label>
                                    <textarea id="mail_content" name="mail_content" class="form-control"></textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label style="display:block;">&nbsp;</label>
                                <button class="btn btn-info btn-sm send_btn">Send</button>
                                <?php echo CHtml::ResetButton('Close', array('onclick' => 'closeaction(this,event);', 'class' => 'btn btn-sm')); ?>
                                <div align="center" id="loader"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/ajax-loader.gif"></div>
                            </div>
                        </div>
                </form>
            </div>
        </div>
    </div>

    <form id="pdfvals1" method="post" action="<?php echo $this->createAbsoluteUrl('purchase/addpurchase'); ?>">
        <input type="hidden" name="purchaseview">

        <div class="block_purchase">
            <div id="errormessage"></div>
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label>COMPANY :</label>

                        <?php
                        if (isset(Yii::app()->user->role) && (in_array('/purchase/companyedit', Yii::app()->user->menuauthlist))) {
                            if (!empty($bill_status)) {
                        ?>
                                <a href="#" id="menu-trigger">
                                    <icon class="fa fa-edit">
                                </a>
                        <?php
                            }
                        }
                        ?>
                        <?php if (Yii::app()->user->role == 1 && !empty($admin_approval_data)) { ?>
                            <a href="#" id="edit_company" class="btn btn-xs btn-default" attr="<?= $model->p_id ?>">Approve</a>
                        <?php } ?>
                        <div class="po_company <?= !empty($admin_approval_data) ? 'user_company_edit_class' : '' ?>" id="<?= $model->p_id ?>"><?php echo ucfirst($company['name']); ?></div>
                        <input type="hidden" id="txtCompany" name="txtCompany" value="<?php echo ucfirst($company['name']); ?>" />
                        <input type="hidden" id="txtPurchaseCompany" name="txtPurchaseCompany" value="<?php echo $model->company_id; ?>" />
                        <div id="menu" style="display:none">
                            <select name="company" class="company select2-hidden-accessible" id="company_id" style="width:190px" tabindex="-1" aria-hidden="true">
                                <option value="">Choose Company</option>
                                <?php
                                foreach ($typelist as $key => $value) {
                                ?>
                                    <option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                                <?php
                                }
                                ?>
                            </select>
                            <button id="company_change">Save</button>
                        </div>
                        <div id="previous_details" style="position: fixed;top: 94px; left: 70px; right: 70px; z-index: 2; max-width:1150px; margin:0 auto;"></div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>GST NO</label>
                        <div class="gstnum"><?php echo ucfirst($company['company_gstnum']); ?></div>
                        <input type="hidden" class="gstnum" name="txtCompanyGst" value="<?php echo ucfirst($company['company_gstnum']); ?>" />
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>PROJECT : </label>
                        <div><?php echo isset($project->name) ? $project->name : ''; ?></div>
                        <input type="hidden" name="project" id="project" value="<?php echo isset($project->pid) ? $project->pid : ''; ?>">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>EXPENSE HEAD : </label>
                        <div><?php echo (isset($expense_head->type_name)) ? $expense_head->type_name : ''; ?></div>
                        <input type="hidden" name="expensehead" id="expensehead" value="<?php echo (isset($expense_head->type_id)) ? $expense_head->type_id : ''; ?>">
                    </div>
                </div>
            </div>
            <div class='row'>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>VENDOR : </label>
                        <div><?php echo (isset($vendor->name)) ? $vendor->name : ''; ?></div>
                        <input type="hidden" name="vendor" id="vendor" value="<?php echo (isset($vendor->vendor_id)) ? $vendor->vendor_id : ''; ?>">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>DATE : </label>
                        <div><?php echo date('d-m-Y', strtotime($model->purchase_date)); ?>
                            <input type="hidden" name="date" value="<?php echo date('d-m-Y', strtotime($model->purchase_date)); ?>" readonly> </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>PURCHASE NO : </label>
                        <div> <?php echo isset($model->purchase_no) ? $model->purchase_no : ''; ?>
                            <input type="hidden" name="purchaseno" value="<?php echo isset($model->purchase_no) ? $model->purchase_no : ''; ?>" readonly>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>CONTACT NO : </label>

                        <div> <?php echo isset($purchase_data->contact_no) ? $purchase_data->contact_no : ''; ?>
                            <input type="hidden" name="contact_no" value="<?php echo isset($purchase_data->contact_no) ? $purchase_data->contact_no : '-'; ?>" readonly>
                        </div>
                    </div>
                </div>
                <div>


                </div>
            </div>
            <div class="row">
                <div class="col-md-6 pull-left">
                    <div class="form-group">
                        <label>SHIPPING ADDRESS : </label>

                        <div> <?php echo isset($purchase_data->shipping_address) ? nl2br($purchase_data->shipping_address) : '-'; ?>
                            <input type="hidden" name="shipping_address" value="<?php echo isset($purchase_data->shipping_address) ? $purchase_data->shipping_address : '-'; ?>" readonly>
                        </div>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>

            <div id="table-scroll" class="table-scroll">
                <div id="faux-table" class="faux-table" aria="hidden"></div>
                <div id="table-wrap" class="table-wrap">
                    <div class="table-responsive">
                        <table border="1" class="table">
                            <thead>
                                <tr>
                                    <th class="text_align">Sl.No</th>
                                    <th class="text_align">Specification</th>
                                    <th class="text_align">Quantity</th>
                                    <th class="text_align">Unit</th>
                                    <th class="text_align">HSN Code</th>
                                    <th class="text_align">Rate</th>
                                    <th class="text_align">Amount</th>
                                </tr>
                            </thead>
                            <tbody class="addrow">
                                <?php
                                $total_amount = 0;
                                if (!empty($itemmodel)) {
                                    $i = 0;
                                    foreach ($itemmodel as $value) {

                                        $tblpx = Yii::app()->db->tablePrefix;
                                        if ($value['category_id'] == '0') {
                                            $specification = $value['remark'];
                                        } else {

                                            $data = Yii::app()->db->createCommand("SELECT specification, brand_id, parent_id FROM {$tblpx}purchase_category WHERE id=" . $value['category_id'] . "")->queryRow();

                                            if ($data['brand_id'] != NULL) {
                                                $brand_details = Yii::app()->db->createCommand("SELECT brand_name FROM {$tblpx}brand WHERE id=" . $data['brand_id'] . "")->queryRow();
                                                $brand = '-' . $brand_details['brand_name'];
                                            } else {
                                                $brand = '';
                                            }

                                            $parent_category = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}purchase_category WHERE id='" . $data['parent_id'] . "'")->queryRow();

                                            $specification = $parent_category['category_name'] . $brand . ' - ' . $data['specification'];
                                        }
                                        $total_amount += $value->amount;
                                ?>

                                        <tr class="tr_class">
                                            <td><input type="hidden" name="sl_No[]" value="<?php echo $i + 1; ?>"> <?php echo $i + 1; ?></td>
                                            <td><input type="hidden" name="description[]" value="<?php echo $specification; ?>"><?php echo $specification; ?> </td>
                                            <td class="text-right"><input type="hidden" name="quantity[]" value="<?php echo $value->quantity; ?>"> <?php echo $value->quantity; ?> </td>
                                            <td><input type="hidden" name="unit[]" value="<?php echo $value->unit; ?>" readonly><?php echo $value->unit; ?></td>
                                            <td><input type="hidden" name="hsn_code[]" value="<?php echo $value->hsn_code; ?>" readonly><?php echo $value->hsn_code; ?></td>
                                            <td class="text-right"><input type="hidden" name="rate[]" value="<?php echo $value->rate; ?>" readonly><?php echo Controller::money_format_inr($value->rate, 2, 1); ?></td>
                                            <td class="text-right"><input type="hidden" name="amount[]" value="<?php echo $value->amount; ?>" readonly><?php echo Controller::money_format_inr($value->amount, 2, 1); ?></td>
                                        </tr>
                                <?php
                                        $i++;
                                    }
                                }
                                ?>


                            </tbody>
                            <tfoot>
                                <tr>
                                    <th style="text-align: right;" colspan="6" class="text-right">GRAND TOTAL: </th>
                                    <th class="text-right">
                                        <div id="grand_total"><?php echo Controller::money_format_inr($model->total_amount, 2, 1); ?></div>
                                    </th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>


                </div>

            </div>
            <div class="row">
                <div class="pull-right col-md-2">*Exclusive of GST</div>
            </div>

            <div style="padding-right: 0px;" class="col-md-offset-9 col-md-3 col-sm-offset-8 col-sm-4 col-xs-offset-6 col-xs-6">

                <input type="hidden" name="subtot" value="<?php echo $model->total_amount; ?>" readonly>
                <input type="hidden" name="grand" value="<?php echo $model->total_amount; ?>" readonly>


            </div>


            <br><br>
    </form>
</div>
<style>
    .error_message {
        color: red;
    }

    a.pdf_excel {
        background-color: #6a8ec7;
        display: inline-block;
        padding: 8px;
        color: #fff;
        border: 1px solid #6a8ec8;
    }

    .invoicemaindiv th,
    .invoicemaindiv td {
        padding: 10px;
    }

    .page-body h3 {
        margin: 4px 0px;
        color: inherit;
        text-align: left;
    }

    .panel {
        border: 1px solid #ddd;
        box-shadow: 0px 0px 0px 0px;
    }

    .panel-heading {
        background-color: #eee;
        height: 40px;
    }
</style>


<script>
    $(function() {

        $('.pdfbtn1').click(function() {
            $("form#pdfvals1").attr('action', "<?php echo $this->createAbsoluteUrl('purchase/exportpdf'); ?>");
            $("form#pdfvals1").submit();

        });

        $('.excelbtn1').click(function() {
            $("form#pdfvals1").attr('action', "<?php echo $this->createAbsoluteUrl('purchase/exportexcel'); ?>");
            $("form#pdfvals1").submit();

        });


    });
</script>

</div>

<script>
    function closeaction() {
        $('#send_form').slideUp(500);
    }

    $('#sendmail').click(function() {
        $('#send_form').slideDown();
    });

    $("#form_send").validate({
        rules: {
            mail_content: {
                required: true,
            },
            mail_to: {
                required: true,
                email: true
            },
        },
        messages: {
            mail_content: {
                required: "Please enter mesage",
            },
            mail_to: {
                required: "Please enter email",
                email: "Please enter valid email"
            }
        },
    });

    $('.send_btn').click(function(e) {
        e.preventDefault();
        var data = $('#form_send').serialize();
        if ($("#form_send").valid()) {
            $('#loader').css('display', 'inline-block');
            var p_id = $('#p_id').val();
            var mail_to = $('#mail_to').val();
            var mail_content = $('#mail_content').val();
            var pdf_path = $('#pdf_path').val();
            var pdf_name = $('#pdf_name').val();
            $('.send_btn').prop('disabled', true)
            $.ajax({
                url: '<?php echo Yii::app()->createAbsoluteUrl('purchase/sendattachment'); ?>',
                type: 'POST',
                dataType: 'json',
                data: {
                    p_id: p_id,
                    mail_to: mail_to,
                    mail_content: mail_content,
                    pdf_path: pdf_path,
                    pdf_name: pdf_name
                },
                success: function(response) {
                    if (response == 'success') {
                        $('#loader').hide();
                        $('.send_btn').prop('disabled', false)
                        $('#send_form').slideUp(500);
                        $("#form_send").trigger('reset');
                    } else {
                        $('#loader').hide();
                        $('.send_btn').prop('disabled', false)
                    }
                }
            })
        }
    })

    $(".sendmail").click(function() {
        var p_id = $(this).attr("data-id");
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: '<?php echo Yii::app()->createAbsoluteUrl('purchase/pdfgeneration'); ?>',
            data: {
                p_id: p_id
            },
            success: function(response) {
                $('#pdf_path').val(response.path);
                $('#pdf_name').val(response.name);
            }
        })
    })

    $(document).ready(function() {
        $('#previous_details').hide();
        $('#menu-trigger').click(function(event) {
            $('#menu').show();
        });
    })

    $(document).ready(function() {
        $(".company").select2();
        $(document).on('mouseover', '.user_company_edit_class', function(e) {
            var purchase_id = this.id;
            $.ajax({
                url: '<?php echo Yii::app()->createAbsoluteUrl('dashboard/pocompanyeditlog'); ?>',
                type: 'GET',
                dataType: 'json',
                data: {
                    id: purchase_id
                },
                success: function(result) {
                    if (result.status == 1) {
                        $('#previous_details').html(result.html);
                        $("#previous_details").show();
                        $("#pre_fixtable2").tableHeadFixer();
                    } else {
                        $('#previous_details').html(result.html);
                        $("#previous_details").hide();
                        $("#pre_fixtable2").tableHeadFixer();
                    }
                }
            });
            $('#previous_details').show();
        });
        $(document).on('mouseout', '.user_company_edit_class', function(e) {
            $("#previous_details").hide();
        });
    });
    $(document).on('click', '#edit_company', function(e) {
        if (confirm("Do you want to approve company change?")) {
            var purchase_id = $(this).attr('attr');

            $.ajax({
                type: 'POST',
                url: '<?php echo Yii::app()->createAbsoluteUrl('dashboard/companyapprove'); ?>',

                data: {
                    'purchase_id': purchase_id
                },
                success: function(result) {

                    if (result === "success") {
                        $('#edit_company').hide();
                        $("#errormessage").show()
                            .html('<div class="alert alert-success">Success! Request approved successfully.</div>')
                            .fadeOut(10000);
                        $('.po_company').removeClass('user_company_edit_class');
                    } else {
                        $("#errormessage").show()
                            .html('<div class="alert alert-warning">Failed! Please try again.</div>')
                            .fadeOut(10000);
                        return false;
                    }
                }
            });
        } else {
            return false;
        }
    });
    $(document).on('click', '#company_change', function(e) {
        e.preventDefault();
        var prev_company_id = $('#txtPurchaseCompany').val();
        var prev_company_name = $('#txtCompany').val();
        var company_id = $('#company_id').val();
        if (company_id != "") {
            var purchase_id = '<?php echo $model->p_id; ?>';
            $.ajax({
                type: 'GET',
                url: '<?php echo Yii::app()->createAbsoluteUrl('purchase/companyedit'); ?>',
                dataType: 'json',
                data: {
                    'company_id': company_id,
                    'purchase_id': purchase_id,
                    'prev_company_id': prev_company_id,
                    'prev_comp_name': prev_company_name
                },
                success: function(result) {
                    console.log(result.company);
                    if (result.status == 1) {
                        $('#txtCompany').val(result.company);
                        $('#txtPurchaseCompany').val(result.id);
                        $('.po_company').html(result.company);
                        $('.gstnum').html(result.gstnum);
                        $('#menu').hide();
                    } else if (result.status == 2) {
                        $('#txtCompany').val(result.company);
                        $('#txtPurchaseCompany').val(result.id);
                        $('.po_company').html(result.company);
                        $('.gstnum').html(result.gstnum);
                        $('#menu').hide();
                        $('.po_company').addClass('user_company_edit_class');
                    }
                }
            })
        }
    })
    $(document).ajaxComplete(function() {
        $('.loading-overlay').removeClass('is-active');
        $('#loading').hide();
    });
</script>