<?php
/* @var $this DailyReportController */
/* @var $model Dailyreport */
/* @var $form CActiveForm */

        
//print_r($completion_date);
 ?>
     <?php if(Yii::app()->user->hasFlash('success')):?><br>
    <div class="example1consolesucces">
        <?php echo Yii::app()->user->getFlash('success'); ?>
    </div>
<?php endif; ?>

<div class="form ">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'dailyreport-form',
	'enableAjaxValidation'=>false,
)); ?>
 

    <div class="clearfix">
            <div class="row addRow">
				
				<?php if($model->isNewRecord)
				{ ?>
				 <div class="col-md-4">
                    <?php echo $form->labelEx($model,'inv_no'); ?>
                    <?php echo $form->textField($model,'inv_no',array('class'=>'form-control')); ?>
                    <?php echo $form->error($model,'inv_no'); ?>
                </div>
                <?php
				}
				else
				{
				?>
				<div class="col-md-4">
                    <?php echo $form->labelEx($model,'inv_no'); ?>
                    <?php echo $form->textField($model,'inv_no',array('class'=>'form-control','readonly'=>true)); ?>
                    <?php echo $form->error($model,'inv_no'); ?>
                </div>
                 <?php
				}
                ?>		
                			
                <div class="col-md-4">
		<?php echo $form->labelEx($model,'project_id'); ?>
                <?php
                
                if(Yii::app()->user->role == 1 || Yii::app()->user->role == 2){
                    echo $form->dropDownList($model, 'project_id', CHtml::listData(Projects::model()->findAll(array(
                                                'select' => array('pid, name'),
                                                //"condition" => 'pid in (select projectid from project_assign where userid=' . Yii::app()->user->id . ')',
                                                'order' => 'name',
                                                'distinct' => true
                                            )), 'pid', 'name'), array('class'=>'form-control','style'=>'width:200px;','empty' => '-----------'));
                                                
                }else{
                    echo $form->dropDownList($model, 'project_id', CHtml::listData(Projects::model()->findAll(array(
                                                'select' => array('pid, name'),
                                                "condition" => 'pid in (select projectid from jp_project_assign where userid=' . Yii::app()->user->id . ')',
                                                'order' => 'name',
                                                'distinct' => true
                                            )), 'pid', 'name'), array('class'=>'form-control','style'=>'width:200px;','empty' => '-----------'));
                                                
                }
                ?>
		
		<?php echo $form->error($model,'project_id'); ?>
                </div>
						
                <div class="col-md-4">
                    <label>Client:</label><input type="text" id="client" class="form-control" style="width:200px;" readonly="true" value = "<?php echo ((!$model->isNewRecord) ? $client : '') ?>" />
                </div>
           
               
            </div>
            <div class="row addRow">
				 <div class="col-md-4">
				<?php echo $form->labelEx($model, 'date'); ?>
                    <?php echo CHtml::activeTextField($model, 'date', array('readonly' => 'true',"value" => (($model->isNewRecord) ? date('d-M-Y') : date('d-M-Y', strtotime($model->date))), 'size' => 10,'class'=>'form-control','style'=>'width:200px;')); ?>
                    <?php
                    $this->widget('application.extensions.calendar.SCalendar', array(
                        'inputField' => 'Invoice_date',
                        'ifFormat' => '%d-%b-%Y',
                    ));
                    ?>
                <?php echo $form->error($model,'date'); ?>
                </div>
                <div class="col-md-4">
                    <?php echo $form->labelEx($model,'amount'); ?>
                    <?php echo $form->textField($model,'amount',array('class'=>'form-control')); ?>
                    <?php echo $form->error($model,'amount'); ?>
                </div>
            </div>

            <div class="row addRow">
                <div class="col-md-3">
                    <?php echo CHtml::submitButton($model->isNewRecord ? 'Submit' : 'Submit',array('class' => 'btn pull-left')); ?>
                </div>
                <div class="col-md-8"></div>
            </div>
        </div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<?php Yii::app()->clientScript->registerCss('myscript','
    .row{
        padding-left:25px;
    }
    .form-control{
        padding:5px;
        width:150px;
   }
   row .buttons{
       paddding-left:100px;
   }
   .addRow {
  padding: 0px;
   }
   .dailyreport_form{
       width:300px;
       margin :0px auto;
   }
   .flash-success{
   color:#fff;
   padding:5px;
   padding: 5px;
  height: 35px;
  width: 330px;
  border: solid #32CD32;
  background-color: #32CD32;
   }
 @media (max-width:767px){
    .dailyreport_form{
       width:300px;
       margin :0px auto;
   }
}  

@media (min-width:768px){
    .dailyreport_form{
       width:500px;
       margin :0px auto;
   }
}
.example1consolesucces {
color: white;
background: #247B17;
padding: 5px;
width:200px;
}
  
   ');
?>
<?php $url = Yii::app()->createAbsoluteUrl("invoice/getclientByProject");?>
<?php Yii::app()->clientScript->registerScript('myscript','
    $(document).ready(function(){
    $("#Invoice_project_id").change(function(){
    var pro_id= $("#Invoice_project_id").val();
    $.ajax({
       url: "'.$url.'",
        data: {"id": pro_id}, 
        //dataType: "json",
        type: "POST",
        success:function(data){
        //alert(data);
            document.getElementById("client").value=data; 
            
        }
        
});

});
$(".flash-success").animate({opacity: 1.0}, 5000).fadeOut("slow");
  
    });
        ');?>
