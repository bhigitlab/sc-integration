<?php
/* @var $this ExpensesImportController */
/* @var $model ExpensesImport */

$this->breadcrumbs=array(
	'Expenses Imports'=>array('index'),
	$model->exp_id=>array('view','id'=>$model->exp_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List ExpensesImport', 'url'=>array('index')),
	array('label'=>'Create ExpensesImport', 'url'=>array('create')),
	array('label'=>'View ExpensesImport', 'url'=>array('view', 'id'=>$model->exp_id)),
	array('label'=>'Manage ExpensesImport', 'url'=>array('admin')),
);
?>

<h1>Update ExpensesImport <?php echo $model->exp_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>