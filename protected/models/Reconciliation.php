<?php

/**
 * This is the model class for table "{{reconciliation}}".
 *
 * The followings are the available columns in table '{{reconciliation}}':
 * @property integer $reconciliation_id
 * @property string $reconciliation_table
 * @property integer $reconciliation_parentid
 * @property string $reconciliation_payment
 * @property string $reconciliation_paymentdate
 * @property double $reconciliation_amount
 * @property string $reconciliation_chequeno
 * @property integer $reconciliation_bank
 * @property string $reconciliation_date
 * @property string $created_date
 * @property integer $reconciliation_status
 * @property string $reconciliation_parentids
 * @property integer $company_id
 */
class Reconciliation extends CActiveRecord
{
	public $fromdate;
	public $todate;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{reconciliation}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('reconciliation_table, reconciliation_parentid, reconciliation_payment, reconciliation_amount, reconciliation_chequeno, reconciliation_bank, created_date, reconciliation_status', 'required'),
			array('reconciliation_parentid, reconciliation_bank, reconciliation_status, company_id', 'numerical', 'integerOnly' => true),
			array('reconciliation_amount', 'numerical'),
			array('reconciliation_table, reconciliation_payment, reconciliation_chequeno', 'length', 'max' => 50),
			array('reconciliation_paymentdate, reconciliation_date', 'safe'),
			array('reconciliation_status', 'checkNegativeBalance'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('reconciliation_id, fromdate, todate, reconciliation_table, reconciliation_parentid, reconciliation_payment, reconciliation_paymentdate, reconciliation_amount, reconciliation_chequeno, reconciliation_bank, reconciliation_date, created_date, reconciliation_status, reconciliation_parentids, company_id', 'safe', 'on' => 'search'),
		);
	}

	public function checkNegativeBalance() {
    
		if($this->reconciliation_id !="" && $this->reconciliation_table=='jp_dailyexpense' && $this->reconciliation_status==1 ){
			$invoice_amount = 0;
			$expense_amount = 0;
			$payment_type = "";
			$available_amount = "";
			$company_id = $this->company_id;
			if($this->reconciliation_table=='jp_dailyexpense'){
				$entry = Dailyexpense::model()->findByPk($this->reconciliation_parentid);
				if(!empty($entry)){
					$payment_type=$entry->expense_type;
					if($entry->expensehead_type==4){ //cash withdrawal
					$payment_type=88;					
					}
					if($entry->expensehead_type==5){ //cash deposit
					$payment_type=89;					
					}
				}
				
			}
							
	   
			if (!empty($company_id)) {
				$invoice_amount = BalanceHelper::cashInvoice(
								$payment_type,
								$company_id,
								NULL,
								$this->reconciliation_bank,
								$this->reconciliation_date
				);
				$expense_amount = BalanceHelper::cashExpense(
								$payment_type,
								$company_id,
								NULL,
								$this->reconciliation_parentid,
								$this->reconciliation_bank,
								$this->reconciliation_date
				);

				if (is_numeric($invoice_amount) && is_numeric($expense_amount)) {
					$available_amount = $invoice_amount - $expense_amount;
				}
				

				if (is_numeric($available_amount) && $available_amount >= $this->reconciliation_amount) {				
					$closing_balance = $available_amount - $this->reconciliation_amount;  //initial closing balace          					
					$date = date('Y-m-d', strtotime('+1 day', strtotime($this->reconciliation_date)));
					$paymentArray = DailyExpense::model()->getPaymentArray($date, $this->reconciliation_parentid, $status = 0, $payment_type, $company_id, $this->reconciliation_bank);
					if(!empty($paymentArray)){
						
						if (count($paymentArray) < 20000) {

							foreach ($paymentArray as $data) {
								if ($data['type'] == 73) { //expense
									$closing_balance -= $data['paidamount'];
								} else {
									$closing_balance += $data['receipt'];
								}

								if ($closing_balance < 0) {
									$this->addError("reconciliation_amount", "Insufficient balance on " . Controller::formattedcommonDate($data['date']) . " in " . $data['tablename'] . " - ID: #" . $data['entry_id']);
									return;
								}
							}
						} else {
							$this->addError("reconciliation_amount", "Limit Of 20000 Entries Exceeded");
							return;
						}
					}
				} else if (($this->reconciliation_amount > $available_amount) && $payment_type != "103" && $payment_type != "") {				
					$this->addError("reconciliation_amount", "insufficient balance on " . Controller::formattedcommonDate($this->reconciliation_date));
					$date = date('Y-m-d', strtotime($this->reconciliation_date));
					$this->addError("reconciliation_amount", "insufficient balance on " . Controller::formattedcommonDate($this->reconciliation_date));
				}

				if (!is_numeric($available_amount)) {
					$this->addError("", "Something went wrong! Check again or contact support team");
				}
			}
		}
	}


		

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array();
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'reconciliation_id' => 'Reconciliation',
			'reconciliation_table' => 'Reconciliation Table',
			'reconciliation_parentid' => 'Reconciliation Parentid',
			'reconciliation_payment' => 'Payment Name',
			'reconciliation_paymentdate' => 'Reconciliation Paymentdate',
			'reconciliation_amount' => 'Reconciliation Amount',
			'reconciliation_chequeno' => 'Reconciliation Chequeno',
			'reconciliation_bank' => 'Bank Name',
			'reconciliation_date' => 'Reconciliation Date',
			'created_date' => 'Created Date',
			'reconciliation_status' => 'Reconciliation Status',
			'reconciliation_parentids' => 'Reconciliation Parent Ids',
			'company_id' => 'Company',
			'fromdate' => 'Date From',
			'todate' => 'Date To',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('reconciliation_id', $this->reconciliation_id);
		$criteria->compare('reconciliation_table', $this->reconciliation_table, true);
		$criteria->compare('reconciliation_parentid', $this->reconciliation_parentid);
		$criteria->compare('reconciliation_payment', $this->reconciliation_payment, true);
		$criteria->compare('reconciliation_paymentdate', $this->reconciliation_paymentdate, true);
		$criteria->compare('reconciliation_amount', $this->reconciliation_amount);
		$criteria->compare('reconciliation_chequeno', $this->reconciliation_chequeno, true);
		$criteria->compare('reconciliation_bank', $this->reconciliation_bank);
		$criteria->compare('reconciliation_date', $this->reconciliation_date, true);
		$criteria->compare('created_date', $this->created_date, true);
		$criteria->compare('reconciliation_status', $this->reconciliation_status);
		$criteria->compare('reconciliation_parentids', $this->reconciliation_parentids);
		//$criteria->compare('company_id',Yii::app()->user->company_id);
		$user = Users::model()->findByPk(Yii::app()->user->id);
		$arrVal = explode(',', $user->company_id);
		$newQuery = "";
		foreach ($arrVal as $arr) {
			if ($newQuery) $newQuery .= ' OR';
			$newQuery .= " FIND_IN_SET('" . $arr . "', company_id)";
		}
		if (!empty($this->fromdate) && empty($this->todate)) {
			$criteria->addCondition("reconciliation_paymentdate >= '" . date('Y-m-d', strtotime($this->fromdate)) . "'");  // date is database date column field
		} else if (!empty($this->todate) && empty($this->fromdate)) {
			$criteria->addCondition("reconciliation_paymentdate <= '" . date('Y-m-d', strtotime($this->todate)) . "'");
		} elseif (!empty($this->todate) && !empty($this->fromdate)) {
			$criteria->addCondition("reconciliation_paymentdate between '" . date('Y-m-d', strtotime($this->fromdate)) . "' and  '" . date('Y-m-d', strtotime($this->todate)) . "'");
		}

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
	public function pendingreconciliation()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;
		$criteria->compare('reconciliation_id', $this->reconciliation_id);
		$criteria->compare('reconciliation_table', $this->reconciliation_table, true);
		$criteria->compare('reconciliation_parentid', $this->reconciliation_parentid);
		$criteria->compare('reconciliation_payment', $this->reconciliation_payment, true);
		$criteria->compare('reconciliation_paymentdate', $this->reconciliation_paymentdate, true);
		$criteria->compare('reconciliation_amount', $this->reconciliation_amount);
		$criteria->compare('reconciliation_chequeno', $this->reconciliation_chequeno, true);
		$criteria->compare('reconciliation_bank', $this->reconciliation_bank);
		$criteria->compare('reconciliation_date', $this->reconciliation_date, true);
		$criteria->compare('created_date', $this->created_date, true);
		if (empty($this->reconciliation_status))
			$criteria->compare('reconciliation_status', 0);
		else {
			$criteria->compare('reconciliation_status', $this->reconciliation_status);
		}
		//$criteria->compare('company_id',Yii::app()->user->company_id);
		if (!empty($this->company_id)) {
			$criteria->compare('company_id', $this->company_id);
		} else {
			$user = Users::model()->findByPk(Yii::app()->user->id);
			$arrVal = explode(',', $user->company_id);
			$newQuery = "";
			foreach ($arrVal as $arr) {
				if ($newQuery) $newQuery .= ' OR';
				$newQuery .= " FIND_IN_SET('" . $arr . "', company_id)";
			}
		}
		//$criteria->order = 'reconciliation_id DESC';
		if (!empty($this->fromdate) && empty($this->todate)) {
			$criteria->addCondition("reconciliation_paymentdate >= '" . date('Y-m-d', strtotime($this->fromdate)) . "'");  // date is database date column field
		} else if (!empty($this->todate) && empty($this->fromdate)) {
			$criteria->addCondition("reconciliation_paymentdate <= '" . date('Y-m-d', strtotime($this->todate)) . "'");
		} elseif (!empty($this->todate) && !empty($this->fromdate)) {
			$criteria->addCondition("reconciliation_paymentdate between '" . date('Y-m-d', strtotime($this->fromdate)) . "' and  '" . date('Y-m-d', strtotime($this->todate)) . "'");
		}
		//$criteria->group = 'reconciliation_chequeno';
		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'pagination' => false,
			'sort' => array(
				'defaultOrder' => 'reconciliation_id DESC',
			),
		));
	}
	public function completedreconciliation()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;
		$criteria->compare('reconciliation_id', $this->reconciliation_id);
		$criteria->compare('reconciliation_table', $this->reconciliation_table, true);
		$criteria->compare('reconciliation_parentid', $this->reconciliation_parentid);
		$criteria->compare('reconciliation_payment', $this->reconciliation_payment, true);
		$criteria->compare('reconciliation_paymentdate', $this->reconciliation_paymentdate, true);
		$criteria->compare('reconciliation_amount', $this->reconciliation_amount);
		$criteria->compare('reconciliation_chequeno', $this->reconciliation_chequeno, true);
		$criteria->compare('reconciliation_bank', $this->reconciliation_bank);
		$criteria->compare('reconciliation_date', $this->reconciliation_date, true);
		$criteria->compare('created_date', $this->created_date, true);
		$criteria->compare('reconciliation_status', 1);
		//$criteria->compare('company_id',Yii::app()->user->company_id);
		if (!empty($this->company_id)) {
			$criteria->compare('company_id', $this->company_id);
		} else {
			$user = Users::model()->findByPk(Yii::app()->user->id);
			$arrVal = explode(',', $user->company_id);
			$newQuery = "";
			foreach ($arrVal as $arr) {
				if ($newQuery) $newQuery .= ' OR';
				$newQuery .= " FIND_IN_SET('" . $arr . "', company_id)";
			}
		}
		//$criteria->order = 'reconciliation_id DESC';
		if (!empty($this->fromdate) || !empty($this->todate)) {
			if (!empty($this->fromdate) && empty($this->todate)) {
				$criteria->addCondition("reconciliation_date >= '" . date('Y-m-d', strtotime($this->fromdate)) . "'");  // date is database date column field
			} else if (!empty($this->todate) && empty($this->fromdate)) {
				$criteria->addCondition("reconciliation_date <= '" . date('Y-m-d', strtotime($this->todate)) . "'");
			} elseif (!empty($this->todate) && !empty($this->fromdate)) {
				$criteria->addCondition("reconciliation_date between '" . date('Y-m-d', strtotime($this->fromdate)) . "' and  '" . date('Y-m-d', strtotime($this->todate)) . "'");
			}
		} else {
			$datefrom = date("Y-m-") . "01";
			$date_to = date("Y-m-d");
			$criteria->addCondition("reconciliation_date between '" . date('Y-m-d', strtotime($datefrom)) . "' and  '" . date('Y-m-d', strtotime($date_to)) . "'");
		}

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'pagination' => false,
			'sort' => array(
				'defaultOrder' => 'reconciliation_date DESC',
			),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Reconciliation the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function getChequeAmount($data)
	{
		$datas = Reconciliation::model()->findAllByAttributes(array('reconciliation_chequeno' => $data->reconciliation_chequeno, 'reconciliation_status' => 0));
		$amount = 0;
		if ($data->reconciliation_table == 'jp_expenses') {
			foreach ($datas as $dat) {
				if ($dat->reconciliation_amount == 0) {
					$parent_model = Expenses::model()->findByPk($dat->reconciliation_parentid);
					$amount += $parent_model->amount;
				} else {
					$amount += $dat->reconciliation_amount;
				}
			}
		} elseif ($data->reconciliation_table == 'jp_dailyexpense') {
			foreach ($datas as $dat) {
				if ($dat->reconciliation_amount == 0) {
					$parent_model = Dailyexpense::model()->findByPk($dat->reconciliation_parentid);
					$amount += $parent_model->dailyexpense_amount;
				} else {
					$amount += $dat->reconciliation_amount;
				}
			}
		} elseif ($data->reconciliation_table == 'jp_dailyvendors') {
			foreach ($datas as $dat) {
				if ($dat->reconciliation_amount == 0) {
					$parent_model = Dailyvendors::model()->findByPk($dat->reconciliation_parentid);
					$amount += $parent_model->amount;
				} else {
					$amount += $dat->reconciliation_amount;
				}
			}
		} elseif ($data->reconciliation_table == 'jp_buyer_transactions') {
			foreach ($datas as $dat) {
				if ($dat->reconciliation_amount == 0) {
					$parent_model = BuyerTransactions::model()->findByPk($dat->reconciliation_parentid);
					$amount += $parent_model->total_amount;
				} else {
					$amount += $dat->reconciliation_amount;
				}
			}
		}
		return $amount;
	}
}
