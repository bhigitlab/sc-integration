<?php
/* @var $this PmsAccWprItemConsumedController */
/* @var $model PmsAccWprItemConsumed */

$this->breadcrumbs=array(
	'Pms Acc Wpr Item Consumeds'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List PmsAccWprItemConsumed', 'url'=>array('index')),
	array('label'=>'Create PmsAccWprItemConsumed', 'url'=>array('create')),
	array('label'=>'Update PmsAccWprItemConsumed', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete PmsAccWprItemConsumed', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage PmsAccWprItemConsumed', 'url'=>array('admin')),
);
?>

<h1>View PmsAccWprItemConsumed #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'warehouse_id',
		'item_id',
		'item_rate',
		'item_qty',
		'status',
		'item_added_by',
		'item_approved_by',
		'wpr_id',
		'pms_project_id',
		'coms_project_id',
		'coms_material_id',
		'pms_material_id',
		'pms_template_material_id',
		'type_from',
		'created_date',
		'updated_date',
		'pms_task_name',
	),
)); ?>
