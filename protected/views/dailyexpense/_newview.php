<?php
$gsttotal = $data["dailyexpense_sgst"] + $data["dailyexpense_cgst"] + $data["dailyexpense_igst"];
$tblpx = Yii::app()->db->tablePrefix;
?>
<?php
if ($index == 0) {
?>
    <thead class="entry-table">
        <tr>
            <?php
            if ((isset(Yii::app()->user->role) && ((in_array('/dailyexpense/dailyexpensedelete', Yii::app()->user->menuauthlist)) || (in_array('/dailyexpense/dailyexpenseedit', Yii::app()->user->menuauthlist))))) {
            ?>
                <th></th>
            <?php } ?>
            <th>#ID</th>
            <th>Company</th>
            <th colspan=2>Bill No / Transaction Head</th>
            <th>Transaction Type</th>
            <th>Bank</th>
            <th>Cheque No</th>
            <th>Description</th>
            <th>Amount</th>
            <th>Tax</th>
            <th>Total</th>
            <th>Receipt</th>
            <th>Paid</th>
            <th>Payment Type</th>
            <th>Paid To</th>
            
        </tr>
    </thead>
<?php } ?>
<?php
$unreconcilbgcolor ="";
if(($data["expense_type"]==88 || $data["dailyexpense_receipt_type"]==88 )&& ($data["reconciliation_status"] == 0)) {  
    $unreconcilbgcolor =" background:#5bc0de3b;";
}

$bgcolor = '';
$approval_status = $data["approval_status"];
$id = 'dailyexp_id';
$tbl =$this->tableNameAcc('pre_dailyexpenses', 0);
$modelName= 'PreDailyexpenses';

$final = Yii::app()->controller->actiongetDailyExpFinalData($data, $approval_status,$id,$tbl,$modelName);
$data = $final['data'];
$approval_status = $final['approval_status'];
$count =$final['count'];

if ($data["dailyexpense_receipt_type"])
    if ($data["dailyexpense_receipt_type"] == 88) {
        $receiptType = "Cheque";
    } else if ($data["dailyexpense_receipt_type"] == 89) {
        $receiptType = "Cash";
    } else if ($data["dailyexpense_receipt_type"] == 103) {
        $receiptType = "Petty Cash";
    } else {
        $receiptType = "Credit";
    }
else
    $receiptType = NULL;
if ($data["expense_type"])
    if ($data["expense_type"] == 88) {
        $expenseType = "Cheque";
    } else if ($data["expense_type"] == 89) {
        $expenseType = "Cash";
    } else if ($data["expense_type"] == 103) {
        $expenseType = "Petty Cash";
    } else {
        $expenseType = "Credit";
    }
else
    $expenseType = NULL;
    $purchaseType = "";
if ($data["dailyexpense_purchase_type"]) {
    if ($data["dailyexpense_purchase_type"] == 1) $purchaseType = "Credit";
    else if ($data["dailyexpense_purchase_type"] == 2) $purchaseType = "Full Paid";
    else if ($data["dailyexpense_purchase_type"] == 3) $purchaseType = "Partially Paid";
} else {
    $internalTrans  = Dailyexpense::model()->find(array('condition' => 'transaction_parent="' . $data["dailyexp_id"] . '"'));
    if (!empty($internalTrans)) {
        $type_transaction = $internalTrans->dailyexpense_purchase_type;
        if ($type_transaction == 1) $purchaseType = "Credit";
        else if ($type_transaction == 2) $purchaseType = "Full Paid";
        else if ($type_transaction == 3) $purchaseType = "Partially Paid";
    }
}
$class = '';
$date = strtotime("+2 days", strtotime($data["date"]));
$check_date =  date("Y-m-d", $date);
if (date('Y-m-d') <= $check_date) {
    $class = 'row-daybook';
} else {
    $class = '';
}

$employee = '';
if (isset($data["employee_id"]) && ($data["employee_id"] != '')) {
    $users = Users::model()->findByPk($data["employee_id"]);    
    $employee = !empty($users) ? $users->first_name . ' ' . $users->last_name:"";
}
$expenseId      = $data["dailyexp_id"];
if($expenseId){
    $deleteconfirm  = Deletepending::model()->find(array("condition" => "deletepending_parentid = " . $expenseId . " AND deletepending_status = 0 AND deletepending_table = '{$tblpx}dailyexpense'"));
}
$confirmcount   = isset($deleteconfirm) ? count($deleteconfirm) : 0;
if ($confirmcount > 0) {
    $deleteclass = "deleteclass";
    $deletetitle = "Waiting for delete confirmation from admin.";
} else {
    $deleteclass = "";
    $deletetitle = "";
}
if ($data["update_status"] == 1 || $data["delete_status"] == 1) {
    $pro_back       = "";
    $request_back   = " request_back";
} else {
    $pro_back       = "pro_back";
    $request_back   = "";
}

?>
<?php if(isset($data['dailyexp_id']) && $data['dailyexp_id']>0 ){?>
<tr id="<?php echo $data['dailyexp_id']; ?>" 
class="rowedit <?php echo $deleteclass." ". (($approval_status == 0 && (isset($data["petty_payment_approval"]) && $data["petty_payment_approval"] == 0)) ? 'bg_pending' : '');  ?><?php echo $request_back; ?>" title="<?php echo $deletetitle; ?>" data-status="<?php echo $data["approval_status"] ?>" data-id="<?php echo ($data["approval_status"] == 0) ? $data['ref_id'] : $data['dailyexp_id']; ?>"  style="<?php echo $unreconcilbgcolor ?>">

    <?php
    if ((isset(Yii::app()->user->role) && ((in_array('/dailyexpense/dailyexpensedelete', Yii::app()->user->menuauthlist)) || (in_array('/dailyexpense/dailyexpenseedit', Yii::app()->user->menuauthlist))))) {
    ?>
        <td data-status = "<?php echo $data['reconciliation_status']; ?>">
            <?php
            if (yii::app()->user->role != 1 && ($data["update_status"] == 1 || $data["delete_status"] == 1)) {
                if ($data["update_status"] == 1) {
                    $requestmessage = "Already an update request is pending";
                } else if ($data["delete_status"] == 1) {
                    $requestmessage = "Already a delete request is pending";
                }
            ?>
                <span class="icon icon-options-vertical popover-test" data-toggle="popover" data-placement="right" type="button" data-html="true" style="cursor: pointer;"></span>
                <div class="popover-content hide">
                    <ul class="tooltip-hiden">
                        <li><span id="<?php echo $index; ?>" class="pending-icon fa fa-question" title="<?php echo $requestmessage; ?>"></span></li>
                    </ul>
                </div>
                <?php
            } else {
                $payDate    = date("Y-m-d", strtotime($data["date"]));
                $company    = Company::model()->findByPk($data["company_id"]);
                $updateDays = ($company["company_updateduration"]) - 1;
                $editDate   = date('Y-m-d ', strtotime($payDate . ' + ' . $updateDays . ' days'));
                $addedDate   = date("Y-m-d 23:59:59", strtotime($data["date"]));
                $currentDate = date("Y-m-d");
                if (Yii::app()->user->role == 1) {
                ?>
                    <span class="icon icon-options-vertical popover-test" data-toggle="popover" data-placement="right" type="button" data-html="true" style="cursor: pointer;"></span>
                    <div class="popover-content hide">
                        <ul class="tooltip-hiden">
                            <?php
                            if ((in_array('/dailyexpense/dailyexpenseedit', Yii::app()->user->menuauthlist))) {
                            ?>
                                <li><button id="<?php echo $index; ?>" class="btn btn-xs btn-default row-daybook margin-right-5">Edit</button></li>
                            <?php }
                            if ((in_array('/dailyexpense/dailyexpensedelete', Yii::app()->user->menuauthlist))) {
                            ?>
                                <li><button id="<?php echo $index; ?>" class="btn btn-xs btn-default delete_row margin-right-5">Delete</button></li>
                            <?php } ?>
                            <?php if(isset($data["petty_payment_approval"])&&$data["petty_payment_approval"] == 0) { ?>
                                <li><button data-id="<?php echo ($data["approval_status"] == 0) ? $data['ref_id'] : $data['dailyexp_id']; ?>" class="btn btn-xs btn-default delete_row margin-right-5">Payment Approve</button></li>
                            <?php } ?>
                        </ul>
                    </div>
                    <?php
                } else {
                    if (isset($company->company_updateduration) && !empty($company->company_updateduration)) {
                        if ($editDate >= $currentDate) {
                    ?>
                            <span class="icon icon-options-vertical popover-test" data-toggle="popover" data-placement="right" type="button" data-html="true" style="cursor: pointer;"></span>
                            <div class="popover-content hide">
                                <ul class="tooltip-hiden">
                                    <li><button id="<?php echo $index; ?>" class="btn btn-xs btn-default row-daybook margin-right-5">Edit</button></li>
                                    <?php if ($confirmcount == 0) { ?>
                                        <li><button id="<?php echo $index; ?>" class="btn btn-xs btn-default delete_confirmation margin-right-5">Delete</button></li>
                                    <?php } else { ?>
                                        <li><button id="<?php echo $index; ?>" data-id="<?php echo $deleteconfirm["deletepending_id"]; ?>" class="btn btn-xs btn-default delete_restore margin-right-5">Restore</button></li>
                                    <?php } ?>
                                </ul>
                            </div>
                        <?php
                        }
                    } else {
                        if ($currentDate <= $addedDate) {
                        ?>
                            <span class="icon icon-options-vertical popover-test" data-toggle="popover" data-placement="right" type="button" data-html="true" style="cursor: pointer;"></span>
                            <div class="popover-content hide">
                                <ul class="tooltip-hiden">
                                    <?php
                                    if ((in_array('/dailyexpense/dailyexpenseedit', Yii::app()->user->menuauthlist))) {
                                    ?>
                                        <li><button id="<?php echo $index; ?>" class="btn btn-xs btn-default row-daybook margin-right-5">Edit</button></li>
                                    <?php }
                                    if ((in_array('/dailyexpense/dailyexpensedelete', Yii::app()->user->menuauthlist))) {
                                    ?>
                                        <li><button id="<?php echo $index; ?>" class="btn btn-xs btn-default delete_row margin-right-5">Delete</button></li>
                                    <?php } ?>
                                </ul>
                            </div>
                    <?php
                        }
                    }
                    ?>
    </td>
     <?php
        } ?>
    <?php }
    } ?>
    <td><b>#<?php echo ($data["approval_status"] == 0) ? $data['ref_id'] : $data['dailyexp_id']; ?></b></td>
    <td class='<?php //echo $pro_back; ?>' id="<?php echo "typename" . $index; ?>">
        <?php
        $company = Company::model()->findByPk($data['company_id']);
        echo $company['name'];
        ?>
    </td>
    <td class='<?php //echo $pro_back; ?>' id="<?php echo "billno" . $index; ?>"><?php echo $data["billno"] ? $data["billno"] : ""; ?></td>
    <?php if (($data["dailyexpense_type"] == "deposit") || ($data["dailyexpense_type"] == "expense")) { ?>
        <td class='<?php //echo $pro_back; ?>' id="<?php echo "typename" . $index; ?>">
            <?php  ?>
            <?php
            if ($data["dailyexpense_type"] == "deposit") {
                $depositId = $data["expensehead_id"];
                $deposit   = Deposit::model()->findByPk($depositId);
                echo "Deposit - " . $deposit["deposit_name"];
            } else if ($data["dailyexpense_type"] == "expense") {
                if($data["expensehead_type"]==4){
                    echo "Withdrawal From Bank";
                }else{
                    echo $data["typename"] ? "Expense - " . $data["typename"] : "";
                }
                
            } else {
                $receipt   = Companyexpensetype::model()->findByPk($data['exp_type_id']);
                echo "Receipt - " . $receipt['name'];
            }
            ?>
        </td>
        <td id="<?php echo "exptype" . $index; ?>"><?php echo $expenseType; ?></td>
    <?php } else { ?>
        <td class='<?php //echo $pro_back; ?>' id="<?php echo "vname" . $index; ?>">
            <?php
            $name  = $data["vname"] ? "Receipt - " . $data["vname"] : "";
            if($data["expensehead_type"]==5){
                $name = "Cash Deposit to Bank";
            }else if($data["expensehead_type"]==4){
                $name = "Withdrawal From Bank";
            } 
            echo $name;
            ?>
        </td>
        <td id="<?php echo "receipttype" . $index; ?>"><?php echo $receiptType ? $receiptType : ""; ?></td>

    <?php } ?>

    <td id="<?php echo "bank" . $index; ?>"><?php echo $data["bankname"] ? $data["bankname"] : ""; ?></td>
    <td id="<?php echo "chequeno" . $index; ?>"><?php echo $data["dailyexpense_chequeno"] ? $data["dailyexpense_chequeno"] : ""; ?></td>
    <td id="<?php echo "desc" . $index; ?>"><?php echo $data["description"] ? $data["description"] : ""; ?></td>
    <td class="text-right" id="<?php echo "amount" . $index; ?>"><?php echo $data["dailyexpense_amount"] ? Controller::money_format_inr($data["dailyexpense_amount"], 2) : ""; ?></td>
    <td class="text-right"><span class="popover-test tax_amnt" data-toggle="popover" data-trigger="hover" data-placement="left" data-trigger="hover" type="button" data-html="true"><?php echo Controller::money_format_inr($gsttotal, 2) ?></span>
        <div class="popover-content hide">
            <div><span>SGST: (<?php echo $data["dailyexpense_sgstp"] ? $data["dailyexpense_sgstp"] : ""; ?>&#37;) <?php echo $data["dailyexpense_sgst"] ? Controller::money_format_inr($data["dailyexpense_sgst"], 2) : ""; ?></span></div>
            <div><span>CGST: (<?php echo $data["dailyexpense_cgstp"] ? $data["dailyexpense_cgstp"] : ""; ?>&#37;) <?php echo $data["dailyexpense_cgst"] ? Controller::money_format_inr($data["dailyexpense_cgst"], 2) : ""; ?></span></div>
            <div><span>IGST: (<?php echo $data["dailyexpense_igstp"] ? $data["dailyexpense_igstp"] : ""; ?>&#37;) <?php echo $data["dailyexpense_igst"] ? Controller::money_format_inr($data["dailyexpense_igst"], 2) : ""; ?></span></div>
        </div>
    </td>

    <td class="text-right" id="<?php echo "total" . $index; ?>"><?php echo $data["amount"] ? Controller::money_format_inr($data["amount"], 2) : ""; ?></td>
    <td class="text-right" id="<?php echo "receipt" . $index; ?>"><?php echo $data["dailyexpense_receipt"] ? Controller::money_format_inr($data["dailyexpense_receipt"], 2) : Controller::money_format_inr(0, 2); ?></td>
    <td class="text-right" id="<?php echo "paidamount" . $index; ?>"><?php echo ($data["dailyexpense_paidamount"] != NULL) ? Controller::money_format_inr($data["dailyexpense_paidamount"], 2) : Controller::money_format_inr(0, 2); ?></td>
    <td id="<?php echo "purchasetype" . $index; ?>"><?php echo $purchaseType ? $purchaseType : ""; ?></td>
    <td id="<?php echo "purchasetype" . $index; ?>"><?php echo $employee; ?></td>
    <input type="hidden" name="expenseid[]" id="expenseid<?php echo $index; ?>" value="<?php echo $data["dailyexp_id"]; ?>" />
    <input type="hidden" name="transferid[]" id="transferid<?php echo $index; ?>" value="<?php echo isset($data["transfer_parentid"]) ? $data["transfer_parentid"] : 0; ?>" />
    <input type="hidden" name="rowid[]" id="rowid<?php echo $index; ?>" value="<?php echo $index; ?>" />

    

</tr>
<?php } ?>
<?php
if ($index == 0) {
?>
    <tfoot>
        <tr>
            <?php
            if ((isset(Yii::app()->user->role) && ((in_array('/dailyexpense/dailyexpensedelete', Yii::app()->user->menuauthlist)) || (in_array('/dailyexpense/dailyexpenseedit', Yii::app()->user->menuauthlist))))) {
            ?>
                <th></th>
            <?php } ?>
            <th class="total_amnt"></th>
            <th class="total_amnt"></th>
            <th colspan="7" class="text-right">Total Amount:</th>
            <th class="text-right"><?php echo Controller::money_format_inr($grandgst, 2); ?></th>
            <th class="text-right"><?php echo Controller::money_format_inr($grandtotal, 2); ?></th>
            <th class="text-right"><?php echo Controller::money_format_inr($receipt, 2); ?></th>
            <th class="text-right"><?php echo Controller::money_format_inr($paid, 2); ?></th>
            <th></th>
            <th></th>
            

        </tr>
    </tfoot>
<?php } ?>

<style>
    .popover {
        white-space:nowrap;
    }
</style>