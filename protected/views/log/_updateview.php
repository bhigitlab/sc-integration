<?php
/* @var $this LogController */
/* @var $data JpLog */
?>
<?php
if ($index == 0) {
?>

    <thead>
        <tr>
            <th width="5%">Sl No.</th>
            <th width="45%">Data</th>
            <th width="10%">Action</th>
            <th width="10%">Table</th>
            <th width="15">Deleted By</th>
            <th width="15%">Date & Time</th>
            <th></th>
        </tr>
    </thead>
<?php
}
?>
<tbody>
    <tr>
        <td><?php echo $index + 1; ?></td>
        <td class="log">
            <?php
            $logData      = json_decode($data->log_data);
            $tableName    = $data->log_table;
            $tblpx        = Yii::app()->db->tablePrefix;
            $logId = $data->log_id;


            if ($tableName == $tblpx . "expenses") {
                $id             = $logData->exp_id;
                $projectModel   = Projects::model()->findByPk($logData->projectid);

            ?>
            <div>
                <label class="label label-primary">ID: </label>
                <span><?php echo $logData->exp_id; ?></span>
            </div> |
            <div>
                <label class="label label-primary">Project:</label>
                <span> <?php echo $projectModel->name; ?></span>
            </div> |
                <?php
                if ($logData->bill_id) {
                    $billModel      = Bills::model()->findByPk($logData->bill_id);
                ?>
                <div>
                    <label class="label label-primary">Bill Number:</label>
                    <span><?php echo ($billModel) ? $billModel->bill_number : ""; ?></span>
                </div> |
                <?php
                } else if ($logData->invoice_id) {
                ?>
                <div>
                    <label class="label label-primary">Invoice ID: </label>
                    <span><?php echo $logData->invoice_id; ?></span>
                </div> |
                <?php
                }
                ?>
                <div>
                <label class="label label-primary">Date:</label>
                <span> <?php echo $logData->date; ?></span>
                </div> |
                <div>
                <label class="label label-primary">Description:</label>
                 
                <span> <?php echo $logData->description; ?></span>
                </div>|
                <?php
                if ($logData->vendor_id) {
                    $vendorModel    = Vendors::model()->findByPk($logData->vendor_id);
                ?>
                <div>
                    <label class="label label-primary">Vendor:</label>
                    <span> <?php echo ($vendorModel) ? $vendorModel->name : ""; ?></span>
                </div> |
                <?php
                }
                if ($logData->exptype) {
                    $etypeModel    = ExpenseType::model()->findByPk($logData->exptype);
                ?>
                <div>
                    <label class="label label-primary">Expense Head:</label>
                    <span> <?php echo ($etypeModel) ? $etypeModel->type_name : ""; ?></span>
                </div> |
                <?php
                }
                ?>
                <div>
                <label class="label label-primary">Total: </label>
                <span><?php echo $logData->amount; ?></span>
                </div> |
                <?php
                if ($logData->expense_type) {
                    $expenseType = $logData->expense_type;
                ?>
                <div>
                    <label class="label label-primary">Expense Type:</label>
                    <span>
                        <?php if ($expenseType == 88) {
                            echo "Cheque";
                        } else if ($expenseType == 89) {
                            echo "Cash";
                        } else if ($expenseType == 0) {
                            echo "Credit";
                        } else if ($expenseType == 103) {
                            echo "Petty Cash";
                        } ?>
                    </span>
                </div> |
                <?php
                }
                if ($logData->payment_type) {
                    $paymentType = $logData->payment_type;
                ?>
                <div>
                    <label class="label label-primary">Payment Type:</label>
                    <span>
                        <?php if ($paymentType == 88) {
                            echo "Cheque";
                        } else if ($paymentType == 89) {
                            echo "Cash";
                        } else if ($paymentType == 0) {
                            echo "Credit";
                        } else if ($paymentType == 103) {
                            echo "Petty Cash";
                        } ?>
                    </span>
                </div> |
                <?php
                }
                if ($logData->bank_id) {
                    $bankModel    = Bank::model()->findByPk($logData->bank_id);
                ?>
                <div>
                    <span class="label label-primary">Bank Name:</span>
                    <span><?php echo ($bankModel) ? $bankModel->bank_name : ""; ?></span>
                </div> |
                <?php
                }
                if ($logData->cheque_no) {
                ?>
                <div>
                    <span class="label label-primary">Cheque Number:</span>
                    <span><?php echo $logData->cheque_no; ?></span>
                </div> |
                <?php
                }
                if ($logData->receipt) {
                ?>
                <div>
                    <span class="label label-primary">Receipt Amount: </span>
                    <span><?php echo $logData->receipt; ?></span>
                </div> |
                <?php
                }
                if ($logData->purchase_type) {
                ?>
                <div>
                    <span class="label label-primary">Purchase Type: </span>
                    <span><?php if ($logData->purchase_type == 1) {
                                echo "Credit";
                            } else if ($logData->purchase_type == 2) {
                                echo "Full Paid";
                            } else if ($logData->purchase_type == 3) {
                                echo "Partially Paid";
                            } ?>
                    </span>
                </div> |
                <?php
                }
                if ($logData->paid) {
                ?>
                <div>
                    <span class="label label-primary">Paid Amount: </span>
                    <span><?php echo $logData->paid; ?></span>
                </div> |
                <?php
                }
                $userModel = Users::model()->findByPk($logData->created_by);
                ?>
                <div>
                <span class="label label-primary">Created By: </span>
                <span><?php echo (isset($userModel) ? $userModel->first_name . " " . $userModel->last_name : ""); ?></span>
                </div> |
                <div>
                <span class="label label-primary">Created Date: </span>
                <span><?php echo $logData->created_date; ?></span>
                </div> 
            <?php
            } else if ($tableName == $tblpx . "dailyexpense") {
            ?>  
                <div>
                <span class="label label-primary">ID: </span>
                <span><?php echo $logData->exp_type_id; ?></span>
                </div> |
                <div>
                <span class="label label-primary">Date: </span>
                <span><?php echo $logData->date; ?></span>
                </div>|
                <div>
                <span class="label label-primary">Bill Number: </span>
                <span><?php echo $logData->bill_id; ?></span>
                </div> |
                <?php
                if ($logData->expensehead_id) {
                    $eheadModel    = Companyexpensetype::model()->findByPk($logData->exp_type_id);
                ?>
                    <div>
                    <span class="label label-primary">Expense Head: </span>
                    <span><?php echo ($eheadModel) ? $eheadModel->name : ""; ?></span>
                     </div> |
                <?php
                }
                if ($logData->expense_type) {
                    $expenseType = $logData->expense_type;
                ?>
                     <div> 
                    <span class="label label-primary">Expense Type: </span>
                    <span><?php if ($expenseType == 88) {
                                echo "Cheque";
                            } else if ($expenseType == 89) {
                                echo "Cash";
                            } else if ($expenseType == 0) {
                                echo "Credit";
                            } else if ($expenseType == 103) {
                                echo "Petty Cash";
                            } ?></span>
                    </div> |
                <?php
                }
                if ($logData->dailyexpense_receipt_head) {
                    $eheadModel    = Companyexpensetype::model()->findByPk($logData->dailyexpense_receipt_head);
                ?>
                 <div> 
                    <span class="label label-primary">Receipt Head: </span>
                    <span><?php echo (isset($etypeModel) ? $etypeModel->type_name : ""); ?></span>
                </div> |
                <?php
                }
                if ($logData->dailyexpense_receipt_type) {
                    $receiptType = $logData->dailyexpense_receipt_type;
                ?>
                 <div> 
                    <span class="label label-primary">Receipt Type: </span>
                    <span><?php if ($receiptType == 88) {
                                echo "Cheque";
                            } else if ($receiptType == 89) {
                                echo "Cash";
                            } else if ($receiptType == 0) {
                                echo "Credit";
                            } else if ($receiptType == 103) {
                                echo "Petty Cash";
                            } ?></span>
                 </div> |
                <?php
                }
                ?>
                
                  <div> 
                <span class="label label-primary">Total: </span>
                <span><?php echo $logData->amount; ?></span>
                 </div> |
                 <div>
                <span class="label label-primary">Description: </span>
                <span><?php echo $logData->description; ?></span>
                </div> |
                <?php
                if ($logData->dailyexpense_purchase_type) {
                ?>
                <div>
                    <span class="label label-primary">Purchase Type: </span>
                    <span><?php if ($logData->dailyexpense_purchase_type == 1) {
                                echo "Credit";
                            } else if ($logData->dailyexpense_purchase_type == 2) {
                                echo "Full Paid";
                            } else if ($logData->dailyexpense_purchase_type == 3) {
                                echo "Partially Paid";
                            } ?></span>
                </div> |
                <?php
                }

                if ($logData->dailyexpense_paidamount) {
                ?>
                <div>
                    <span class="label label-primary">Paid Amount: </span>
                    <span><?php echo $logData->dailyexpense_paidamount; ?></span>
                </div> |
                <?php
                }
                if ($logData->dailyexpense_receipt) {
                ?>
                 <div>
                    <span class="label label-primary">Receipt Amount: </span>
                    <span><?php echo $logData->dailyexpense_receipt; ?></span>
                </div> |
                <?php
                }
                if ($logData->bank_id) {
                    $bankModel    = Bank::model()->findByPk($logData->bank_id);
                ?>
                 <div>
                    <span class="label label-primary">Bank Name: </span>
                    <span><?php echo ($bankModel) ? $bankModel->bank_name : ""; ?></span>
                </div> |
                <?php
                }
                if ($logData->dailyexpense_chequeno) {
                ?>
                <div>
                    <span class="label label-primary">Cheque Number: </span>
                    <span><?php echo $logData->dailyexpense_chequeno; ?></span>
                 </div> |
                <?php
                }
                $userModel = Users::model()->findByPk($logData->created_by);
                ?>
                <div>
                <span class="label label-primary">Created By: </span>
                <span><?php echo ($userModel) ? $userModel->first_name . " " . $userModel->last_name : ""; ?></span>
                </div> |
                 <div>
                <span class="label label-primary">Created Date: </span>
                <span><?php echo $logData->created_date; ?></span>
                </div> 
            <?php
            } else if ($tableName == $tblpx . "dailyvendors") {
            ?>
                <div>
                <span class="label label-primary">ID: </span>
                <span><?php echo $logData->daily_v_id; ?></span>
                </div> |
                <?php
                if ($logData->vendor_id) {
                    $vendorModel    = Vendors::model()->findByPk($logData->vendor_id);
                ?>
                 <div>
                    <span class="label label-primary">Vendor: </span>
                    <span><?php echo ($vendorModel) ? $vendorModel->name : ""; ?></span>
                </div> |
                <?php
                }
                ?>
                <div> 
                <span class="label label-primary">Total: </span>
                <span><?php echo $logData->amount; ?></span>
                </div> |
                 <div> 
                <span class="label label-primary">Description: </span>
                <span><?php echo $logData->description; ?></span>
                </div> |
                <div> 
                <span class="label label-primary">Date: </span>
                <span><?php echo $logData->date; ?></span>
                 </div> |
                <?php
                if ($logData->payment_type) {
                    $paymentType = $logData->payment_type;
                ?>
                <div> 
                    <span class="label label-primary">Payment Type: </span>
                    <span><?php if ($paymentType == 88) {
                                echo "Cheque";
                            } else if ($paymentType == 89) {
                                echo "Cash";
                            } else if ($paymentType == 0) {
                                echo "Credit";
                            } else if ($paymentType == 103) {
                                echo "Petty Cash";
                            } ?></span>
                </div> |
                <?php
                }
                if ($logData->bank) {
                    $bankModel    = Bank::model()->findByPk($logData->bank);
                ?>
                 <div> 
                    <span class="label label-primary">Bank Name: </span>
                    <span><?php echo ($bankModel) ? $bankModel->bank_name : ""; ?></span>
                    </div> |
                <?php
                }
                if ($logData->cheque_no) {
                ?>
                 <div> 
                    <span class="label label-primary">Cheque Number: </span>
                    <span><?php echo $logData->cheque_no; ?></span>
                </div> |
                <?php
                }
                $userModel = Users::model()->findByPk($logData->created_by);
                ?>
                <div> 
                <span class="label label-primary">Created By: </span>
                <span><?php echo ($userModel) ? $userModel->first_name . " " . $userModel->last_name : ""; ?></span>
                 </div> |
                  <div> 
                <span class="label label-primary">Created Date: </span>
                <span><?php echo $logData->created_date; ?></span>
                 </div> 
            <?php
            } else if ($tableName == $tblpx . "subcontractor_payment") {
            ?>
                <div> 
                <span class="label label-primary">ID: </span>
                <span><?php echo $logData->payment_id; ?></span>
                </div> |
                <?php
                if ($logData->subcontractor_id) {
                    $subcontractorModel    = Subcontractor::model()->findByPk($logData->subcontractor_id);
                ?>
                <div> 
                    <span class="label label-primary">Subcontractor: </span>
                    <span><?php echo ($subcontractorModel) ? $subcontractorModel->subcontractor_name : ""; ?></span>
                </div> |
                <?php
                }
                $projectModel   = Projects::model()->findByPk($logData->project_id);
                ?>
                 <div> 
                <span class="label label-primary">Project: </span>
                <span><?php echo ($projectModel) ? $projectModel->name : ""; ?></span>
                 </div> |
                  <div> 
                <span class="label label-primary">Date: </span>
                <span><?php echo $logData->date; ?></span>
                 </div> |
                  <div> 
                <span class="label label-primary">Total: </span>
                <span><?php echo $logData->amount; ?></span>
                </div> |
                <div> 
                <span class="label label-primary">Description: <?php echo $logData->description; ?></span>
                </div> |
                <?php
                if ($logData->payment_type) {
                    $paymentType = $logData->payment_type;
                ?>
                <div> 
                    <span class="label label-primary">Payment Type: </span>
                    <span><?php if ($paymentType == 88) {
                                echo "Cheque";
                            } else if ($paymentType == 89) {
                                echo "Cash";
                            } else if ($paymentType == 0) {
                                echo "Credit";
                            } else if ($paymentType == 103) {
                                echo "Petty Cash";
                            } ?></span>
                </div> |
                <?php
                }
                if ($logData->bank) {
                    $bankModel    = Bank::model()->findByPk($logData->bank);
                ?>
                <div> 
                    <span class="label label-primary">Bank Name: </span>
                    <span><?php echo ($bankModel) ? $bankModel->bank_name : ""; ?></span>
                </div> |
                <?php
                }
                if ($logData->cheque_no) {
                ?>
                <div> 
                    <span class="label label-primary">Cheque Number: </span>
                    <span><?php echo $logData->cheque_no; ?></span>
                 </div> |
                <?php
                }
                $userModel = Users::model()->findByPk($logData->created_by);
                ?>
                <div> 
                <span class="label label-primary">Created By: </span>
                <span><?php echo ($userModel) ? $userModel->first_name . " " . $userModel->last_name : ""; ?></span>
                </div> |
                <div> 
                <span class="label label-primary">Created Date: </span>
                <span><?php echo $logData->created_date; ?></span>
                </div> 
            <?php
            } else if ($tableName == $tblpx . "scquotation") {
            ?>
                <div> 
                <span class="label label-primary">ID: </span>
                <span><?php echo $logData->scquotation_id; ?></span>
                </div> |
                <?php
                $projectModel   = Projects::model()->findByPk($logData->project_id);
                ?>
                <div>
                <span class="label label-primary">Project: </span>
                <span><?php echo ($projectModel) ? $projectModel->name : ""; ?></span>
                 </div> |
                <?php
                if ($logData->subcontractor_id) {
                    $subcontractorModel    = Subcontractor::model()->findByPk($logData->subcontractor_id);
                ?>
                <div>
                    <span class="label label-primary">Subcontractor: </span>
                    <span><?php echo ($subcontractorModel) ? $subcontractorModel->subcontractor_name : ""; ?></span>
                </div> |
                <?php
                }
                ?>
                <div>
                <span class="label label-primary">Amount: </span>
                <span><?php echo $logData->scquotation_amount; ?></span>
                </div> |
                <div>
                <span class="label label-primary">Description: </span>
                <span><?php echo $logData->scquotation_decription; ?></span>
                </div> |
                <div>
                <span class="label label-primary">Date: </span>
                <span><?php echo $logData->scquotation_date; ?></span>
                </div> |
                <?php
                $userModel = Users::model()->findByPk($logData->created_by);
                ?>
                 <div>
                <span class="label label-primary">Created By: </span>
                <span><?php echo ($userModel) ? $userModel->first_name . " " . $userModel->last_name : ""; ?></span>
                 </div> |
                  <div>
                <span class="label label-primary">Created Date: </span>
                <span><?php echo $logData->created_date; ?></span>
                 </div> 

            <?php
            }else if($tableName == $tblpx . "purchase"){ ?>
             <div>
                <span class="label label-primary">ID: </span>
                <span><?php echo $logData->purchase_no; ?></span>
            </div> |
                <?php
                $projectModel   = Projects::model()->findByPk($logData->project_id);
                ?>
                 <div>
                <span class="label label-primary">Project: </span>
                <span><?php echo ($projectModel) ? $projectModel->name : ""; ?></span>
                </div> |
                 <div>
                <span class="label label-primary">Amount: </span>
                <span><?php echo $logData->total_amount; ?></span>
                </div> |
                 <div>
                <span class="label label-primary">Description: </span>
                <span><?php echo $logData->purchase_description; ?></span>
                </div> |
                <div>
                <span class="label label-primary">Date: </span>
                <span><?php echo $logData->purchase_date; ?></span>
                 </div> |
                <?php
                $userModel = Users::model()->findByPk($logData->created_by);
                ?>
                <div>
                <span class="label label-primary">Created By: </span>
                <span><?php echo ($userModel) ? $userModel->first_name . " " . $userModel->last_name : ""; ?></span>
                  </div> |
                  <div>
                <span class="label label-primary">Created Date: </span>
                <span><?php echo $logData->created_date; ?></span>
                 </div> 
           <?php }else if($tableName == $tblpx . "invoice"){  ?>
                <div>
                <span class="label label-primary">Invoice ID: </span>
                <span><?php echo $logData->invoice_id; ?></span>
                </div> |
                <?php
                $projectModel   = Projects::model()->findByPk($logData->project_id);
                $client_data =  Clients::model()->findByPK($logData->client_id);
                ?>
                <div>
                <span class="label label-primary">Project: </span>
                <span><?php echo ($projectModel) ? $projectModel->name : ""; ?></span>
                  </div> |
                  <div>
                <span class="label label-primary">Client: </span>
                <span><?php echo ($client_data) ? $client_data->name : ""; ?></span>
                 </div> |
                 <div>
                <span class="label label-primary">Amount: </span>
                <span><?php echo $logData->amount; ?></span>
                 </div> |
                 <div>
                <span class="label label-primary">Invoice Number: </span>
                <span><?php echo $logData->inv_no; ?></span>
                 </div> |
                 <div>

                <span class="label label-primary">Created Date: </span>
                <span><?php echo $logData->created_date; ?></span>
                </div> |

                <?php $userModel = Users::model()->findByPk($logData->created_by);?>
                <div>
                <span class="label label-primary">Created By: </span>
                <span><?php echo ($userModel) ? $userModel->first_name . " " . $userModel->last_name : ""; ?></span>
                </div> 

                <?php
            }else if($tableName == $tblpx . "bills"){ ?>
                <div>
                <span class="label label-primary">ID: </span>
                <span><?php echo $logData->bill_id ; ?></span>
                 </div> |
                 <div>
                <span class="label label-primary">Bill No: </span>
                <span><?php echo $logData->bill_number ; ?></span>
                 </div> |
                <?php
                $purchaseModel   = Purchase::model()->findByPk($logData->purchase_id);
                if(!empty($purchaseModel)){
                    $projectModel   = Projects::model()->findByPk($purchaseModel->project_id);
                }
                
                ?>
                 <div>
                <span class="label label-primary">Project: </span>
                <span><?php echo ($projectModel) ? $projectModel->name : ""; ?></span>
                </div> |
                 <div> 
                <span class="label label-primary">Amount: </span>
                <span><?php echo $logData->bill_amount; ?></span>
                </div> |
                <div> 
                <span class="label label-primary">Date: </span>
                <span><?php echo $logData->bill_date; ?></span>
                </div> |
                <?php
                $userModel = Users::model()->findByPk($logData->created_by);
                ?>
                <div> 
                <span class="label label-primary">Created By: </span>
                <span><?php echo ($userModel) ? $userModel->first_name . " " . $userModel->last_name : ""; ?></span>
                 </div> |
                 <div> 
                <span class="label label-primary">Created Date: </span>
                <span><?php echo $logData->created_date; ?></span>
                 </div> 
           <?php }else if($tableName == $tblpx . "warehousedespatch_items"){ ?>
               <div> 
                <span class="label label-primary">Item ID: </span>
                <span><?php echo $logData->item_id ; ?></span>
                 </div> |
                 <div>
                <span class="label label-primary">Despatch No: </span>
                <span><?php $despatch = Warehousedespatch::Model()->findByPk($logData->warehousedespatch_id);
                echo !empty($despatch)? $despatch->warehousedespatch_no: "" ; ?></span>
                </div> |
                <?php
                // $scquotModel   = Scquotation::model()->findByPk($logData->scquotation_id);
                if(!empty($despatch)){
                    $projectModel   = Projects::model()->findByPk( $despatch->warehousedespatch_project);
                    $warehouse = Warehouse::model()->findByPk($despatch->warehousedespatch_warehouseid);
                }
                
                ?>
                <div> 
                <span class="label label-primary">Project: </span>
                <span><?php echo ($projectModel)? $projectModel->name: ' '; ?></span>
                </div> |
                <div> 
                <span class="label label-primary">Warehouse: </span>
                <span><?php  echo ($warehouse)?$warehouse->warehouse_name:' ';  ?></span>
                 </div> |
                 <div> 
                <span class="label label-primary">Date: </span>
                <span><?php echo $logData->created_date; ?></span>
                </div> |
                <?php
                $userModel = Users::model()->findByPk($logData->created_by);
                ?>
                 <div> 

                <span class="label label-primary">Created By: </span>
                <span><?php echo ($userModel) ? $userModel->first_name . " " . $userModel->last_name : ""; ?></span>
                </div> |
                 <div> 
                <span class="label label-primary">Created Date: </span>
                <span><?php echo $logData->created_date; ?></span>
                </div> 
                <?php }else if($tableName == $tblpx . "subcontractorbill"){ ?>
                <div> 
                <span class="label label-primary">ID: </span>
                <span><?php echo $logData->id ; ?></span>
                </div> |
                <div> 
                <span class="label label-primary">Bill No: </span>
                <span><?php echo $logData->bill_number ; ?></span>
                 </div> |
                <?php
                $scquotModel   = Scquotation::model()->findByPk($logData->scquotation_id);
                if(!empty($scquotModel)){
                    $projectModel   = Projects::model()->findByPk( $scquotModel->project_id);
                }
                
                ?>
                <div> 
                <span class="label label-primary">Project: </span>
                <span><?php echo ($projectModel) ? $projectModel->name : ""; ?></span>
                 </div> |
                 <div> 
                <span class="label label-primary">Amount: </span>
                <span><?php echo $logData->total_amount; ?></span>
                </div> |
                 <div> 
                <span class="label label-primary">Date: </span>
                <span><?php echo $logData->date; ?></span>
                </div> 
                <?php
                $userModel = Users::model()->findByPk($logData->created_by);
                ?>
                 <div> 
                <span class="label label-primary">Created By: </span>
                <span><?php echo ($userModel) ? $userModel->first_name . " " . $userModel->last_name : ""; ?></span>
                </div> |
                <div>
                <span class="label label-primary">Created Date: </span>
                <span><?php echo $logData->created_date; ?></span>
                </div> 
                <?php }else if($tableName == $tblpx . "warehousereceipt"){ ?>
               <div>
                <span class="label label-primary"> ID: </span>
                <span><?php echo $logData->warehousereceipt_id ; ?></span>
                </div> |
                <div>
                <span class="label label-primary">Warehouse No: </span>
                <span><?php 
                echo $logData->warehousereceipt_no ; ?></span>
                </div> |
                <?php
                // $scquotModel   = Scquotation::model()->findByPk($logData->scquotation_id);
                $projectModel='';
                $warehouse='';
                
                     $projectModel   = Projects::model()->findByPk( $logData->warehousereceipt_purchasebill_project);
                    $warehouse = Warehouse::model()->findByPk($logData->warehousereceipt_warehouseid);
               
                
                ?>
                <div>
                 <span class="label label-primary">Project: </span>
                 <span><?php echo ($projectModel) ? $projectModel->name : ""; ?></span>
                </div> |
                <div>
                <span class="label label-primary">Warehouse To: </span> 
                <span><?php  echo ($warehouse)?$warehouse->warehouse_name:' ';  ?></span>
                </div> |
                <div>
                <span class="label label-primary">Date: </span>
                <span><?php echo $logData->warehousereceipt_date; ?></span>
                 </div> |
                <?php
                $userModel = Users::model()->findByPk($logData->warehousereceipt_clerk);
                ?>
                <div>
                <span class="label label-primary">Clerk: </span>
                <span><?php echo ($userModel) ? $userModel->first_name . " " . $userModel->last_name : ""; ?></span>
                 </div> |
                 <div>
                <span class="label label-primary">Created Date: </span>
                <span><?php echo $logData->created_date; ?></span>
                 </div> 
                
                <?php }else if($tableName == $tblpx . "subcontractorbill"){ ?>
                 <div>
                    <span class="label label-primary">ID: </span>

                <span><?php echo $logData->id ; ?></span>
                 </div> |
                  <div>
                <span class="label label-primary">Bill No: </span>
                <span><?php echo $logData->bill_number ; ?></span>
                 </div> |
                <?php
                $scquotModel   = Scquotation::model()->findByPk($logData->scquotation_id);
                if(!empty($scquotModel)){
                    $projectModel   = Projects::model()->findByPk( $scquotModel->project_id);
                }
                
                ?>
                 <div>
                <span class="label label-primary">Project: </span>
                <span><?php echo ($projectModel) ? $projectModel->name : ""; ?></span>
                 </div> |
                  <div>
                <span class="label label-primary">Amount: </span>
                <span><?php echo $logData->total_amount; ?></span>
                 </div> |
                 <div>
                <span class="label label-primary">Date: </span>
                <span><?php echo $logData->date; ?></span>
                </div> |
                <?php
                $userModel = Users::model()->findByPk($logData->created_by);
                ?>
                 <div>
                <span class="label label-primary">Created By: </span>
                <span><?php echo ($userModel) ? $userModel->first_name . " " . $userModel->last_name : ""; ?></span>
                </div> |
                 <div>
                <span class="label label-primary">Created Date: </span>
                <span><?php echo $logData->created_date; ?></span>
                 </div> 
           <?php } else if ($tableName == $tblpx . "dailyreport") {
            ?>  
                <div>
                <span class="label label-primary">ID: </span>
                <span><?php echo $logData->dr_id; ?></span>
                </div> |

                <?php
                $projectModel   = Projects::model()->findByPk($logData->projectid);
                ?>
                <div>
                <span class="label label-primary">Project: </span>
                <span><?php echo ($projectModel) ? $projectModel->name : ""; ?></span>
                 </div> |
                <?php
                if ($logData->subcontractor_id) {
                    $subcontractorModel    = Subcontractor::model()->findByPk($logData->subcontractor_id);
                ?>
                <div>
                    <span class="label label-primary">Subcontractor: </span>
                    <span><?php echo ($subcontractorModel) ? $subcontractorModel->subcontractor_name : ""; ?></span>
                </div> |
                <?php
                }
                
                if ($logData->expensehead_id) {
                    $etypeModel    = ExpenseType::model()->findByPk($logData->expensehead_id);
                ?>
                <div>
                    <label class="label label-primary">Expense Head:</label>
                    <span> <?php echo ($etypeModel) ? $etypeModel->type_name : ""; ?></span>
                </div> |
                <?php
                }
                ?>
                <div>
                <span class="label label-primary">Amount: </span>
                <span><?php echo $logData->amount; ?></span>
                 </div> |
                 <div>
                <span class="label label-primary">Description: </span>
                <span><?php echo $logData->description; ?></span>
                </div> |
                <div>
                <span class="label label-primary">Date: </span>
                <span><?php echo $logData->date; ?></span>
                </div> |
                <?php
                $userModel = Users::model()->findByPk($logData->created_by);
                ?>
                 <div>
                <span class="label label-primary">Created By: </span>
                <span><?php echo ($userModel) ? $userModel->first_name . " " . $userModel->last_name : ""; ?></span>
                </div> |
                <div>
                <span class="label label-primary">Created Date: </span>
                <span><?php echo $logData->created_date; ?></span>
                 </div> 
            <?php }else if($tableName == $tblpx . "quotation"){  ?>
                <div>
                <span class="label label-primary">Qutation ID: </span>
                <span><?php echo $logData->quotation_id; ?></span>
                </div> |
                <?php
                $projectModel   = Projects::model()->findByPk($logData->project_id);
                $client_data =  Clients::model()->findByPK($logData->client_id);
                ?>
                <div>
                <span class="label label-primary">Project: </span>
                <span><?php echo ($projectModel) ? $projectModel->name : ""; ?></span>
                </div> |
                <div>
                <span class="label label-primary">Client: </span>
                <span><?php echo ($client_data) ? $client_data->name : ""; ?></span>
                 </div> |
                 <div>
                <span class="label label-primary">Amount: </span>
                <span><?php echo $logData->amount; ?></span>
                 </div> |
                 <div>
                <span class="label label-primary">Quotation Number: </span>
                <span><?php echo $logData->inv_no; ?></span>
                </div> |
                <div>
                <span class="label label-primary">Created Date: </span>
                <span><?php echo $logData->created_date; ?></span>
                 </div> |
                 <div>
                <?php $userModel = Users::model()->findByPk($logData->created_by);?>
                <span class="label label-primary">Created By: </span>
                <span><?php echo ($userModel) ? $userModel->first_name . " " . $userModel->last_name : ""; ?></span>
                </div> 
                <?php
                 }else if($tableName == $tblpx . "sales_quotation_master"){  ?>
                <div>
                <span class="label label-primary"> ID: </span>
                <span><?php echo $logData->id; ?></span>
                </div> |
                
                <div>
                <span class="label label-primary">Address: </span>
                <span><?php  $logData->address  ?></span>
                  </div> |
                    <div>
                <span class="label label-primary">Quotation Number: </span>
                <span><?php echo $logData->invoice_no; ?></span>
                 </div> |
                  <div>
                <span class="label label-primary">Revision no: </span>
                <span><?php echo $logData->revision_no; ?></span>
                 </div> |
                 <div>

                <span class="label label-primary">Created Date: </span>
                <span><?php echo $logData->created_date; ?></span>
                </div> |
                <div>

                <span class="label label-primary">Quotation Date: </span>
                <span><?php echo $logData->date_quotation; ?></span>
                </div> |

                <?php $userModel = Users::model()->findByPk($logData->created_by);?>
                <div>
                <span class="label label-primary">Created By: </span>
                <span><?php echo ($userModel) ? $userModel->first_name . " " . $userModel->last_name : ""; ?></span>
                </div> 

                <?php
            } ?>


        </td>
        <td><?php echo CHtml::encode($data->log_action); ?></td>
        <td><?php echo CHtml::encode($data->log_table); ?></td>
        <td>
            <?php
            $subModel = Users::model()->findByPk($data->log_action_by);
            echo CHtml::encode(($subModel) ? $subModel->first_name . " " . $subModel->last_name : "");
            ?>
        </td>
        <td><?php echo CHtml::encode($data->log_datetime); ?></td>
        <td class="icon-restore" data-id="<?php echo $logId ?>" >
            <span <?php if($tableName == $tblpx . "purchase"){ ?> style="display:none;" <?php } ?>>
            <?php
            if (ENV == 'stage' || ENV == 'development') {
                if ($data->log_action == 'delete') { ?>
                    <i class="fa fa-undo"></i>
            <?php }
            } ?>
            </span>
        </td>

    </tr>
</tbody>