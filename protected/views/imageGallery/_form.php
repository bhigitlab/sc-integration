<?php
/* @var $this ImageGalleryController */
/* @var $model ImageGallery */
/* @var $form CActiveForm */
?>

<div class="container">
    <h4>Upload Image</h4>
<?php
            $form = $this->beginWidget('CActiveForm', array(
                'id' => 'image-gallery-form',
                'enableAjaxValidation' => true,
                'method' => 'POST',
                'htmlOptions' => array(
                    'enctype' => 'multipart/form-data',
                    'enableClientValidation' => true,
                ),
            ));
            ?>

            <div class="row">
                <div class="col-md-3 col-sm-12 col-xs-12">
                    <?php echo $form->labelEx($model, 'projectid'); ?>
                    <?php
                    echo $form->dropDownList($model, 'projectid', CHtml::listData(Projects::model()->findAll(), 'pid', 'name'), array('empty' => '-----------'))
                    ?>
                    <?php echo $form->error($model, 'projectid'); ?>
                </div>  

                <div class="col-md-3 col-sm-12 col-xs-12">
                    <?php echo $form->labelEx($model, 'image'); ?>
                    <?php echo $form->fileField($model, 'image'); ?>
                    <?php echo $form->error($model, 'image'); ?>
                </div>
                <?php if(!$model->isNewRecord){ ?>
                <div class="col-md-3 col-sm-12 col-xs-12">
                   <div style="width: 80px; height: 80px;" class="fileupload-new thumbnail"><img src="<?php echo Yii::app()->request->baseUrl . '/uploads/image/' . $model->image; ?>" alt="" /></div>
                </div>
                <?php } ?>
            </div><br>
            <div class="row">
                <div class="col-md-3 col-sm-12 col-xs-12">
                    <?php echo CHtml::submitButton($model->isNewRecord ? 'Upload' : 'Save'); ?>
                </div>
            </div>

            <?php $this->endWidget(); ?>

</div>