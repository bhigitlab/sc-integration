<?php
/* @var $this BillsController */
/* @var $model Bills */
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$warehousereceipt_item_Data = [];
$dimension = '';
$purchase_type = '';
$purchase_type_arr = array();
$despatch_dimension_arr = array();
$dimension_arr = array();
if ($received_type == "bill") {
    $transfer_quantity_type = "Billed Quantity";
} else {
    $transfer_quantity_type = "Despatched Quantity";
}

if ($received_type == "bill") {
    foreach ($item_Data as $items) {
        array_push($purchase_type_arr, $items["purchase_type"]);
    }
    if (!empty($purchase_type_arr)) {
        if (!in_array("O", $purchase_type_arr)) {
            if (!in_array("G", $purchase_type_arr)) {
                if (!in_array("A", $purchase_type_arr)) {
                    $dimension = '';
                    $purchase_type = '';
                } else {
                    $purchase_type = 'A';
                    $dimension = 'Length';
                }
            } elseif (!in_array("A", $purchase_type_arr)) {
                if (!in_array("G", $purchase_type_arr)) {
                    $purchase_type = '';
                    $dimension = '';
                } else {
                    $purchase_type = 'G';
                    $dimension = 'Dimension (W x H)';
                }
            }
        } else {
            $purchase_type = 'O';
            $dimension = '';
        }
    }
} else {
    foreach ($item_Data as $items) {
        if ($items["dimension"] != null && $items["dimension"] != '') {
            array_push($despatch_dimension_arr, $items["dimension"]);
        }
    }
    if (!empty($despatch_dimension_arr)) {
        $purchase_type = 'G';
        $dimension = 'Dimension (W x H)';
    } else {
        $purchase_type = '';
        $dimension = '';
    }
}
// echo $dimension;die("gg");
?>
<div class="row">
    <div class="col-xs-12">
        <div class="subcontractor-bill-table table-responsive">
            <table class="table">
                <thead class="entry-table">
                    <tr>
                        <th>Sl.No</th>
                        <th>Specifications/Remark</th>
                        <th>Batch</th>
                        <?php
                        if ($purchase_type != '') {
                            if ($purchase_type == 'A') {
                                echo "<th>" . $dimension . "</th>";
                            } elseif ($purchase_type == 'G') {
                                echo "<th>" . $dimension . "</th>";
                            } else {
                                echo $dimension;
                            }
                        }
                        ?>
                        <th>Unit</th>
                        <th>
                            <?php echo $transfer_quantity_type; ?>
                        </th>
                        <th>Rate</th>
                        <th>Received Quantity</th>
                        <th>Accepted Quantity</th>
                        <th>Rejected Quantity</th>
                        <th>Remark</th>

                    </tr>
                </thead>
                <tbody>
                    <?php
                    $tblpx = Yii::app()->db->tablePrefix;
                    $warehousereceiptEntry_count = WarehousereceiptItems::model()->findAllByAttributes(array(
                        'warehousereceipt_id' => $receipt_id,
                    ));

                    if ($received_type == "bill") {
                        $i = 1;
                        $sl_no = 0;
                        $number = 1;

                        foreach ($item_Data as $item) {
                            $warehousereceipt_item_Data = Yii::app()->db->createCommand("SELECT * FROM " . $tblpx . "warehousereceipt_items WHERE warehousereceipt_id = " . $receipt_id . " AND warehousereceipt_transfer_type_item_id = '" . $item["billitem_id"] . "' ORDER BY item_id")->queryRow();
                            $receipt_type = $received_type . "no_" . $item["billitem_id"];
                            if ($item['category_id']) {
                                $categorymodel = Specification::model()->findByPk($item['category_id']);
                                $categoryName = "Others";
                                if ($categorymodel) {
                                    $brand = '';
                                    $unit = '';
                                    $item_category_name = '';

                                    if ($categorymodel->brand_id != NULL) {
                                        $brand_details = Yii::app()->db->createCommand("SELECT brand_name FROM {$tblpx}brand "
                                            . " WHERE id=" . $categorymodel->brand_id)->queryRow();
                                        $brand = ucfirst($brand_details['brand_name']);
                                    }

                                    if ($categorymodel->unit != NULL) {
                                        $unit = '-' . $categorymodel->unit;
                                    }

                                    if ($categorymodel->cat_id != NULL) {
                                        $item_category_name = Yii::app()->db->createCommand("SELECT category_name "
                                            . " FROM {$tblpx}purchase_category WHERE 
                                                    id = " . $categorymodel->cat_id)->queryScalar();
                                    }
                                    $categoryName = $item_category_name . '-' . $brand . '-' . $categorymodel->specification;
                                }
                            } else {
                                $categoryName = "Others";
                            }
                            $batch_bill = $item["batch"];
                            $item_units_details = Controller::getUnits($item['category_id']);
                            $item_unit = $item_units_details['unit'];
                            $item_units = $item_units_details['units'];
                            $item_unit_status = $item_units_details['status'];
                            $item_unit_base_unit = $item_units_details['base_unit'];
                            $received_type = $received_type . "no_" . $item["billitem_id"];

                            $purchaseqty = $item["purchaseitem_quantity"];
                            $purchaserate = $item["purchaseitem_rate"];
                            $purchaseunit = $item["purchaseitem_unit"];
                            $billedquantity_data = $item["billitem_quantity"];
                            $rate_data = $item["billitem_rate"];


                            if ($purchaseqty != '' && $purchaserate != '' && $purchaseunit != "") {

                                if ($item['billitem_unit'] != $purchaseunit) {

                                    $billedquantity = $purchaseqty;
                                    $rate = $purchaserate;
                                    $bill_unit_id = $purchaseunit;
                                    $bill_unit_id_data = $item['billitem_unit'];
                                    $bill_unit_id_data_dis = $purchaseunit;
                                } else {
                                    $billedquantity = $item["billitem_quantity"];
                                    $rate = $item["billitem_rate"];
                                    $bill_unit_id = $item['billitem_unit'];
                                    $bill_unit_id_data = $item['billitem_unit'];
                                    $bill_unit_id_data_dis = $item['billitem_unit'];
                                }
                            } else {
                                $billedquantity = $item["billitem_quantity"];
                                $rate = $item["billitem_rate"];
                                $bill_unit_id = $item['billitem_unit'];
                                $bill_unit_id_data = $item['billitem_unit'];
                                $bill_unit_id_data_dis = $item['billitem_unit'];
                            }





                            if (count($warehousereceiptEntry_count) == 0) { ?>

                                <tr class="pitems">
                                    <td>
                                        <?php echo $i++; ?>
                                    </td>
                                    <input type="hidden" id="sl_no_<?php echo $item["billitem_id"]; ?>" value="<?php echo $sl_no;
                                       $sl_no++; ?>" class="txtBox pastweek sl_no" name="sl_no[]" />
                                    <input type="hidden" id="item_type_id_<?php echo $item["billitem_id"]; ?>"
                                        value="<?php echo $item["billitem_id"]; ?>" class="txtBox pastweek item_id"
                                        name="item_id[]" />
                                    <input type="hidden" id="rate_<?php echo $item["billitem_id"]; ?>" class="form-control rate"
                                        value="<?php echo $rate; ?>" name="rates[]" />
                                    <input type="hidden" id="billedquantity_<?php echo $item["billitem_id"]; ?>"
                                        value="<?php echo $billedquantity; ?>" class="txtBox pastweek billedquantity"
                                        name="transferablequantity[]" />
                                    <input type="hidden" id="bill_unit_id_<?php echo $item["billitem_id"]; ?>"
                                        value="<?php echo $bill_unit_id; ?>" class="txtBox pastweek bill_unit_id"
                                        name="item_unit_id[]" />
                                    <input type="hidden" id="rate_data<?php echo $item["billitem_id"]; ?>" class="form-control rate"
                                        value="<?php echo $rate_data; ?>" name="rates_data[]" />
                                    <input type="hidden" id="billedquantity_data<?php echo $item["billitem_id"]; ?>"
                                        value="<?php echo $billedquantity_data; ?>" class="txtBox pastweek billedquantity"
                                        name="transferablequantity_data[]" />
                                    <input type="hidden" id="bill_unit_id_data<?php echo $item["billitem_id"]; ?>"
                                        value="<?php echo $bill_unit_id_data; ?>" class="txtBox pastweek bill_unit_id"
                                        name="item_unit_id_data[]" />
                                    <input type="hidden" id="item_unit_base_unit_<?php echo $item["billitem_id"]; ?>"
                                        value="<?php echo $item_unit_base_unit; ?>" class="txtBox pastweek unit"
                                        name="item_unit_base_unit[]" />
                                    <input type="hidden" id="stock_item_<?php echo $item["billitem_id"]; ?>"
                                        value="<?php echo $item['category_id']; ?>" class="txtBox pastweek stock_item"
                                        name="stock_item[]" />
                                    <input type="hidden" id="itembatch_<?php echo $item["billitem_id"]; ?>"
                                        class="form-control batch" value="<?php echo $batch_bill; ?>" name="batch[]" />
                                    <input type="hidden" id="warehouse_receipt_item_id_<?php echo $item["billitem_id"]; ?>"
                                        class="form-control warehouse_receipt_item_id" value=""
                                        name="warehouse_receipt_item_id[]" />
                                    <input type="hidden" id="billitem_purchase_type_<?php echo $item["billitem_id"]; ?>"
                                        class="form-control billitem_purchase_type" value="<?php echo $purchase_type; ?>"
                                        name="billitem_purchase_type[]" />
                                    <input type="hidden" id="item_length_<?php echo $item["billitem_id"]; ?>"
                                        class="form-control item_length" value="<?php echo $item["item_length"]; ?>"
                                        name="warehouse_receipt_item_length[]" />
                                    <input type="hidden" id="item_width_<?php echo $item["billitem_id"]; ?>"
                                        class="form-control item_width" value="<?php echo $item["item_width"]; ?>"
                                        name="warehouse_receipt_item_width[]" />
                                    <input type="hidden" id="item_height_<?php echo $item["billitem_id"]; ?>"
                                        class="form-control item_height" value="<?php echo $item["item_height"]; ?>"
                                        name="warehouse_receipt_item_height[]" />
                                    <input type="hidden" id="purchase_type_<?php echo $item["billitem_id"]; ?>"
                                        class="form-control purchase_type" value="<?php echo $purchase_type; ?>"
                                        name="purchase_type[]" />
                                    <input type="hidden" id="item_dimension_<?php echo $item["billitem_id"]; ?>"
                                        class="form-control item_dimension" value="" name="warehouse_receipt_item_dimension[]" />
                                    <input type="hidden" id="item_dimension_category_<?php echo $item["billitem_id"]; ?>"
                                        class="form-control item_dimension_category" value=""
                                        name="warehouse_receipt_item_dimension_category[]" />


                                    <td id="<?php echo $number - 1; ?>">
                                        <div id="categoryname<?php echo $number - 1; ?>">
                                            <?php echo $categoryName; ?>
                                        </div>
                                    </td>
                                    <td id="<?php echo $number - 1; ?>">
                                        <div id="batch<?php echo $number - 1; ?>">
                                            <?php echo $item["batch"]; ?>
                                        </div>
                                    </td>
                                    <?php if ($purchase_type != '') {
                                        if ($purchase_type == 'A') { ?>
                                            <td id="<?php echo $number - 1; ?>">
                                                <?php echo $item["item_length"]; ?>
                                            </td>
                                        <?php } elseif ($purchase_type == 'G') { ?>
                                            <td id="<?php echo $number - 1; ?>">
                                                <?php echo "(" . $item["item_width"] . "x" . $item["item_height"] . ")"; ?>
                                            </td>
                                        <?php } else {
                                            echo "";
                                        }
                                        ?>
                                    <?php } ?>
                                    <td id="<?php echo $number - 1; ?>">
                                        <?php echo $bill_unit_id_data_dis; ?>
                                    </td>
                                    <td id="<?php echo $number - 1; ?>">
                                        <div id="billedquantity_<?php echo $number - 1; ?>">
                                            <?php echo $billedquantity; ?>
                                        </div>
                                    </td>
                                    <td id="<?php echo $number - 1; ?>">
                                        <div id="itemrate_<?php echo $number - 1; ?>">
                                            <?php echo $rate; ?>
                                        </div>
                                    </td>
                                    <td class="vertical-middle" id="<?php echo $number - 1; ?>" style="width: 55px;">
                                        <div id="receivedquantity_<?php echo $number - 1; ?>"><input type="text"
                                                id="receivedquantity_<?php echo $item["billitem_id"]; ?>"
                                                class="form-control  receivedquantity validate[required,custom[number],min[0]]"
                                                value="<?php echo isset($warehousereceipt_item_Data['warehousereceipt_quantity']) ? $warehousereceipt_item_Data['warehousereceipt_quantity'] : "0"; ?>"
                                                name="receivedquantity[]" /></div>
                                    </td>
                                    <td class="vertical-middle" id="<?php echo $number - 1; ?>" style="width: 55px;">
                                        <div id="acceptedquantity_<?php echo $number - 1; ?>"><input type="text"
                                                id="accepted_quantity_<?php echo $item["billitem_id"]; ?>"
                                                class="form-control  accepted_quantity validate[required,custom[number],min[0]]"
                                                value="<?php echo isset($warehousereceipt_item_Data['warehousereceipt_accepted_quantity']) ? $warehousereceipt_item_Data['warehousereceipt_accepted_quantity'] : "0"; ?>"
                                                name="accepted_quantity[]" /></div>
                                    </td>
                                    <td class="vertical-middle" id="<?php echo $number - 1; ?>" style="width: 55px;">
                                        <div id="rejectedquantity_<?php echo $number - 1; ?>"><input type="text"
                                                id="rejected_quantity_<?php echo $item["billitem_id"]; ?>"
                                                class="form-control  rejected_quantity validate[required,custom[number],min[0]]"
                                                value="<?php echo isset($warehousereceipt_item_Data['warehousereceipt_rejected_quantity']) ? $warehousereceipt_item_Data['warehousereceipt_rejected_quantity'] : "0"; ?>"
                                                name="rejected_quantity[]" /></div>
                                    </td>

                                    <td id="<?php echo $number - 1; ?>">
                                        <div id="remark_data<?php echo $number - 1; ?>">
                                            <textarea id="remark_data_<?php echo $item["billitem_id"]; ?>"
                                                class="form-control  remark_data"
                                                name="remark_data[]"><?php echo isset($warehousereceipt_item_Data['remark_data']) ? $warehousereceipt_item_Data['remark_data'] : ""; ?></textarea>
                                        </div>
                                    </td>



                                </tr>
                                <?php
                            } else {


                                if ($item["billitem_id"] == $warehousereceipt_item_Data['warehousereceipt_transfer_type_item_id']) {
                                    $warehousereceipt_itemid = $warehousereceipt_item_Data['item_id'];
                                    ?>
                                    <tr class="pitems">
                                        <td>
                                            <?php echo $i++; ?>
                                        </td>
                                        <input type="hidden" id="sl_no_<?php echo $item["billitem_id"]; ?>" value="<?php echo $sl_no;
                                           $sl_no++; ?>" class="txtBox pastweek sl_no" name="sl_no[]" />
                                        <input type="hidden" id="item_type_id_<?php echo $item["billitem_id"]; ?>"
                                            value="<?php echo $item["billitem_id"]; ?>" class="txtBox pastweek item_id"
                                            name="item_id[]" />
                                        <input type="hidden" id="rate_<?php echo $item["billitem_id"]; ?>" class="form-control rate"
                                            value="<?php echo $rate; ?>" name="rates[]" />
                                        <input type="hidden" id="billedquantity_<?php echo $item["billitem_id"]; ?>"
                                            value="<?php echo $billedquantity; ?>" class="txtBox pastweek billedquantity"
                                            name="transferablequantity[]" />
                                        <input type="hidden" id="bill_unit_id_<?php echo $item["billitem_id"]; ?>"
                                            value="<?php echo $bill_unit_id; ?>" class="txtBox pastweek bill_unit_id"
                                            name="item_unit_id[]" />
                                        <input type="hidden" id="rate_data<?php echo $item["billitem_id"]; ?>" class="form-control rate"
                                            value="<?php echo $rate_data; ?>" name="rates_data[]" />
                                        <input type="hidden" id="billedquantity_data<?php echo $item["billitem_id"]; ?>"
                                            value="<?php echo $billedquantity_data; ?>" class="txtBox pastweek billedquantity"
                                            name="transferablequantity_data[]" />
                                        <input type="hidden" id="bill_unit_id_data<?php echo $item["billitem_id"]; ?>"
                                            value="<?php echo $bill_unit_id_data; ?>" class="txtBox pastweek bill_unit_id"
                                            name="item_unit_id_data[]" />
                                        <input type="hidden" id="item_unit_base_unit_<?php echo $item["billitem_id"]; ?>"
                                            value="<?php echo $item_unit_base_unit; ?>" class="txtBox pastweek unit"
                                            name="item_unit_base_unit[]" />
                                        <input type="hidden" id="stock_item_<?php echo $item["billitem_id"]; ?>"
                                            value="<?php echo $item['category_id']; ?>" class="txtBox pastweek stock_item"
                                            name="stock_item[]" />
                                        <input type="hidden" id="itembatch_<?php echo $item["billitem_id"]; ?>"
                                            class="form-control batch" value="<?php echo $batch_bill; ?>" name="batch[]" />
                                        <input type="hidden" id="warehouse_receipt_item_id_<?php echo $item["billitem_id"]; ?>"
                                            class="form-control warehouse_receipt_item_id"
                                            value="<?php echo $warehousereceipt_itemid; ?>" name="warehouse_receipt_item_id[]" />
                                        <input type="hidden" id="billitem_purchase_type_<?php echo $item["billitem_id"]; ?>"
                                            class="form-control billitem_purchase_type" value="<?php echo $purchase_type; ?>"
                                            name="billitem_purchase_type[]" />
                                        <input type="hidden" id="item_length_<?php echo $item["billitem_id"]; ?>"
                                            class="form-control item_length" value="<?php echo $item["item_length"]; ?>"
                                            name="warehouse_receipt_item_length[]" />
                                        <input type="hidden" id="item_width_<?php echo $item["billitem_id"]; ?>"
                                            class="form-control item_width" value="<?php echo $item["item_width"]; ?>"
                                            name="warehouse_receipt_item_width[]" />
                                        <input type="hidden" id="item_height_<?php echo $item["billitem_id"]; ?>"
                                            class="form-control item_height" value="<?php echo $item["item_height"]; ?>"
                                            name="warehouse_receipt_item_height[]" />
                                        <input type="hidden" id="purchase_type_<?php echo $item["billitem_id"]; ?>"
                                            class="form-control purchase_type" value="<?php echo $purchase_type; ?>"
                                            name="purchase_type[]" />
                                        <input type="hidden" id="item_dimension_<?php echo $item["billitem_id"]; ?>"
                                            class="form-control item_dimension" value="" name="warehouse_receipt_item_dimension[]" />
                                        <input type="hidden" id="item_dimension_category_<?php echo $item["billitem_id"]; ?>"
                                            class="form-control item_dimension_category" value=""
                                            name="warehouse_receipt_item_dimension_category[]" />


                                        <td id="<?php echo $number - 1; ?>">
                                            <div id="categoryname<?php echo $number - 1; ?>">
                                                <?php echo $categoryName; ?>
                                            </div>
                                        </td>
                                        <td id="<?php echo $number - 1; ?>">
                                            <div id="batch<?php echo $number - 1; ?>">
                                                <?php echo $item["batch"]; ?>
                                            </div>
                                        </td>
                                        <?php if ($purchase_type != '') {
                                            if ($purchase_type == 'A') { ?>
                                                <td id="<?php echo $number - 1; ?>">
                                                    <?php echo $item["item_length"]; ?>
                                                </td>
                                            <?php } elseif ($purchase_type == 'G') { ?>
                                                <td id="<?php echo $number - 1; ?>">
                                                    <?php echo "(" . $item["item_width"] . "x" . $item["item_height"] . ")"; ?>
                                                </td>
                                            <?php } else {
                                                echo "";
                                            }
                                            ?>
                                        <?php } ?>
                                        <td id="<?php echo $number - 1; ?>">
                                            <?php echo $bill_unit_id_data_dis; ?>
                                        </td>
                                        <td id="<?php echo $number - 1; ?>">
                                            <div id="billedquantity_<?php echo $number - 1; ?>">
                                                <?php echo $billedquantity; ?>
                                            </div>
                                        </td>
                                        <td id="<?php echo $number - 1; ?>">
                                            <div id="itemrate_<?php echo $number - 1; ?>">
                                                <?php echo $rate; ?>
                                            </div>
                                        </td>
                                        <td id="<?php echo $number - 1; ?>" style="width: 55px;">
                                            <div id="receivedquantity_<?php echo $number - 1; ?>"><input type="text"
                                                    id="receivedquantity_<?php echo $item["billitem_id"]; ?>"
                                                    class="form-control  receivedquantity validate[required,custom[number],min[0]]"
                                                    value="<?php echo isset($warehousereceipt_item_Data['warehousereceipt_quantity']) ? $warehousereceipt_item_Data['warehousereceipt_quantity'] : "0"; ?>"
                                                    name="receivedquantity[]" /></div>
                                        </td>
                                        <td id="<?php echo $number - 1; ?>" style="width: 55px;">
                                            <div id="acceptedquantity_<?php echo $number - 1; ?>"><input type="text"
                                                    id="accepted_quantity_<?php echo $item["billitem_id"]; ?>"
                                                    class="form-control  accepted_quantity validate[required,custom[number],min[0]]"
                                                    value="<?php echo isset($warehousereceipt_item_Data['warehousereceipt_accepted_quantity']) ? $warehousereceipt_item_Data['warehousereceipt_accepted_quantity'] : "0"; ?>"
                                                    name="accepted_quantity[]" /></div>
                                        </td>
                                        <td id="<?php echo $number - 1; ?>" style="width: 55px;">
                                            <div id="rejectedquantity_<?php echo $number - 1; ?>"><input type="text"
                                                    id="rejected_quantity_<?php echo $item["billitem_id"]; ?>"
                                                    class="form-control  rejected_quantity validate[required,custom[number],min[0]]"
                                                    value="<?php echo isset($warehousereceipt_item_Data['warehousereceipt_rejected_quantity']) ? $warehousereceipt_item_Data['warehousereceipt_rejected_quantity'] : "0"; ?>"
                                                    name="rejected_quantity[]" /></div>
                                        </td>

                                        <td id="<?php echo $number - 1; ?>">
                                            <div id="remark_data<?php echo $number - 1; ?>">
                                                <textarea id="remark_data_<?php echo $item["billitem_id"]; ?>"
                                                    class="form-control  remark_data"
                                                    name="remark_data[]"><?php echo isset($warehousereceipt_item_Data['remark_data']) ? $warehousereceipt_item_Data['remark_data'] : ""; ?></textarea>
                                            </div>
                                        </td>

                                    </tr>
                                    <?php
                                }
                            }
                        }
                    } else {
                        if ($received_type == "dispatch") {

                            $i = 1;
                            $sl_no = 0;
                            $number = 1;

                            foreach ($item_Data as $item) {
                                $warehousereceipt_item_Data = Yii::app()->db->createCommand("SELECT * FROM " . $tblpx . "warehousereceipt_items WHERE warehousereceipt_id = " . $receipt_id . " AND warehousereceipt_transfer_type_item_id = '" . $item["item_id"] . "' ORDER BY item_id")->queryRow();
                                $receipt_type = $received_type . "no_" . $item["item_id"];
                                if ($item['warehousedespatch_itemid']) {
                                    $categorymodel = Specification::model()->findByPk($item['warehousedespatch_itemid']);
                                    $categoryName = "Others";
                                    if ($categorymodel) {
                                        $unit = '';
                                        $brand = '';
                                        $item_category_name = '';
                                        if ($categorymodel->brand_id != NULL) {
                                            $brand_details = Yii::app()->db->createCommand("SELECT brand_name FROM {$tblpx}brand WHERE id=" . $categorymodel->brand_id . "")->queryRow();
                                            $brand = ucfirst($brand_details['brand_name']);
                                        }
                                        if ($categorymodel->unit != NULL) {
                                            $unit = '-' . $categorymodel->unit;
                                        }
                                        if ($categorymodel->cat_id != NULL) {
                                            $category = PurchaseCategory::model()->findByPk($categorymodel->cat_id);
                                            $item_category_name = $category->category_name . " -";
                                        }
                                        $categoryName = $item_category_name . $brand . '-' . $categorymodel->specification;
                                    }
                                } else {
                                    $categoryName = "Not available";
                                }
                                $batch_despatch = $item["warehousedespatch_batch"];
                                $item_units_details = Controller::getUnits($item['warehousedespatch_itemid']);
                                $item_unit = $item_units_details['unit'];
                                $item_units = $item_units_details['units'];
                                $item_unit_status = $item_units_details['status'];
                                $item_unit_base_unit = $item_units_details['base_unit'];
                                $received_type = $received_type . "no_" . $item["item_id"];

                                $purchaseqty = $item["warehousedespatch_baseqty"];
                                $purchaserate = $item["warehousedespatch_baserate"];
                                $purchaseunit = $item["warehousedespatch_baseunit"];
                                $billedquantity_data = $item["warehousedespatch_quantity"];
                                $rate_data = $item["warehousedespatch_rate"];

                                if ($purchaseqty != '' && $purchaserate != '' && $purchaseunit != "") {
                                    if ($item['warehousedespatch_unit'] != $purchaseunit) {
                                        $billedquantity = $purchaseqty;
                                        $rate = $purchaserate;
                                        $bill_unit_id = $purchaseunit;
                                        $bill_unit_id_data = $item['warehousedespatch_unit'];
                                        $bill_unit_id_data_dis = $purchaseunit;
                                    } else {

                                        $billedquantity = $purchaseqty;
                                        $rate = $purchaserate;
                                        $bill_unit_id = $purchaseunit;
                                        $bill_unit_id_data = $item['warehousedespatch_unit'];
                                        $bill_unit_id_data_dis = $purchaseunit;
                                    }
                                } else {
                                    $bill_unit_id = $item['warehousedespatch_unit'];
                                    $billedquantity = $item["warehousedespatch_quantity"];
                                    $rate = $item["warehousedespatch_rate"];
                                    $bill_unit_id_data = $item['warehousedespatch_unit'];
                                    $bill_unit_id_data_dis = $item['warehousedespatch_unit'];
                                }

                                if (count($warehousereceiptEntry_count) == 0) {
                                    ?>

                                    <tr class="pitems">
                                        <td>
                                            <?php echo $i++; ?>
                                        </td>
                                        <input type="hidden" id="sl_no_<?php echo $item["item_id"]; ?>" value="<?php echo $sl_no;
                                           $sl_no++; ?>" class="txtBox pastweek sl_no" name="sl_no[]" />
                                        <input type="hidden" id="item_type_id_<?php echo $item["item_id"]; ?>"
                                            value="<?php echo $item["item_id"]; ?>" class="txtBox pastweek item_id" name="item_id[]" />
                                        <input type="hidden" id="rate_<?php echo $item["item_id"]; ?>" class="form-control rate"
                                            value="<?php echo $rate; ?>" name="rates[]" />
                                        <input type="hidden" id="billedquantity_<?php echo $item["item_id"]; ?>"
                                            value="<?php echo $billedquantity; ?>" class="txtBox pastweek billedquantity"
                                            name="transferablequantity[]" />
                                        <input type="hidden" id="bill_unit_id_<?php echo $item["item_id"]; ?>"
                                            value="<?php echo $bill_unit_id; ?>" class="txtBox pastweek bill_unit_id"
                                            name="item_unit_id[]" />
                                        <input type="hidden" id="rate_data<?php echo $item["item_id"]; ?>" class="form-control rate"
                                            value="<?php echo $rate_data; ?>" name="rates_data[]" />
                                        <input type="hidden" id="billedquantity_data<?php echo $item["item_id"]; ?>"
                                            value="<?php echo $billedquantity_data; ?>" class="txtBox pastweek billedquantity"
                                            name="transferablequantity_data[]" />
                                        <input type="hidden" id="bill_unit_id_data<?php echo $item["item_id"]; ?>"
                                            value="<?php echo $bill_unit_id_data; ?>" class="txtBox pastweek bill_unit_id"
                                            name="item_unit_id_data[]" />
                                        <input type="hidden" id="item_unit_base_unit_<?php echo $item["item_id"]; ?>"
                                            value="<?php echo $item_unit_base_unit; ?>" class="txtBox pastweek unit"
                                            name="item_unit_base_unit[]" />
                                        <input type="hidden" id="stock_item_<?php echo $item["item_id"]; ?>"
                                            value="<?php echo $item['warehousedespatch_itemid']; ?>" class="txtBox pastweek stock_item"
                                            name="stock_item[]" />
                                        <input type="hidden" id="itembatch_<?php echo $item["item_id"]; ?>" class="form-control batch"
                                            value="<?php echo $batch_despatch; ?>" name="batch[]" />
                                        <input type="hidden" id="warehouse_receipt_item_id_<?php echo $item["item_id"]; ?>"
                                            class="form-control warehouse_receipt_item_id" value=""
                                            name="warehouse_receipt_item_id[]" />
                                        <input type="hidden" id="purchase_type_<?php echo $item["item_id"]; ?>"
                                            class="form-control purchase_type" value="<?php echo $purchase_type; ?>"
                                            name="purchase_type[]" />
                                        <input type="hidden" id="item_dimension_<?php echo $item["item_id"]; ?>"
                                            class="form-control item_dimension" value="<?php echo $item["dimension"]; ?>"
                                            name="warehouse_receipt_item_dimension[]" />
                                        <input type="hidden" id="item_dimension_category_<?php echo $item["item_id"]; ?>"
                                            class="form-control item_dimension_category"
                                            value="<?php echo $item["warehousestock_itemid_dimension_category"]; ?>"
                                            name="warehouse_receipt_item_dimension_category[]" />

                                        <td id="<?php echo $number - 1; ?>">
                                            <div id="categoryname<?php echo $number - 1; ?>">
                                                <?php echo $categoryName; ?>
                                            </div>
                                        </td>
                                        <td id="<?php echo $number - 1; ?>">
                                            <div id="batch<?php echo $number - 1; ?>">
                                                <?php echo $item["warehousedespatch_batch"]; ?>
                                            </div>
                                        </td>
                                        <?php if ($purchase_type != '') {
                                            if ($purchase_type == 'G') { ?>
                                                <td id="<?php echo $number - 1; ?>">
                                                    <?php echo "(" . ($item["dimension"] != '' && $item["dimension"] != null) ? $item["dimension"] : "N/A" . ")"; ?>
                                                </td>
                                            <?php } else {
                                                echo "";
                                            }
                                            ?>
                                        <?php } ?>
                                        <td id="<?php echo $number - 1; ?>">
                                            <?php echo $bill_unit_id_data_dis; ?>
                                        </td>
                                        <td id="<?php echo $number - 1; ?>">
                                            <div id="billedquantity_<?php echo $number - 1; ?>">
                                                <?php echo $billedquantity; ?>
                                            </div>
                                        </td>
                                        <td id="<?php echo $number - 1; ?>">
                                            <div id="itemrate_<?php echo $number - 1; ?>">
                                                <?php echo $rate; ?>
                                            </div>
                                        </td>
                                        <td id="<?php echo $number - 1; ?>" style="width: 55px;">
                                            <div id="receivedquantity_<?php echo $number - 1; ?>"><input type="text"
                                                    id="receivedquantity_<?php echo $item["item_id"]; ?>"
                                                    class="form-control  receivedquantity validate[required,custom[number],min[0]]"
                                                    value="<?php echo isset($warehousereceipt_item_Data['warehousereceipt_quantity']) ? $warehousereceipt_item_Data['warehousereceipt_quantity'] : "0"; ?>"
                                                    name="receivedquantity[]" /></div>
                                        </td>
                                        <td id="<?php echo $number - 1; ?>" style="width: 55px;">
                                            <div id="acceptedquantity_<?php echo $number - 1; ?>"><input type="text"
                                                    id="accepted_quantity_<?php echo $item["item_id"]; ?>"
                                                    class="form-control  accepted_quantity validate[required,custom[number],min[0]]"
                                                    value="<?php echo isset($warehousereceipt_item_Data['warehousereceipt_accepted_quantity']) ? $warehousereceipt_item_Data['warehousereceipt_accepted_quantity'] : "0"; ?>"
                                                    name="accepted_quantity[]" /></div>
                                        </td>
                                        <td id="<?php echo $number - 1; ?>" style="width: 55px;">
                                            <div id="rejectedquantity_<?php echo $number - 1; ?>"><input type="text"
                                                    id="rejected_quantity_<?php echo $item["item_id"]; ?>"
                                                    class="form-control  rejected_quantity validate[required,custom[number],min[0]]"
                                                    value="<?php echo isset($warehousereceipt_item_Data['warehousereceipt_rejected_quantity']) ? $warehousereceipt_item_Data['warehousereceipt_rejected_quantity'] : "0"; ?>"
                                                    name="rejected_quantity[]" /></div>
                                        </td>

                                        <td id="<?php echo $number - 1; ?>">
                                            <div id="remark_data<?php echo $number - 1; ?>">
                                                <textarea id="remark_data_<?php echo $item["item_id"]; ?>"
                                                    class="form-control  remark_data"
                                                    name="remark_data[]"><?php echo isset($warehousereceipt_item_Data['remark_data']) ? $warehousereceipt_item_Data['remark_data'] : ""; ?></textarea>
                                            </div>
                                        </td>


                                    </tr>
                                    <?php
                                } else {
                                    if ($item["item_id"] == $warehousereceipt_item_Data['warehousereceipt_transfer_type_item_id']) {
                                        $warehousereceipt_itemid = $warehousereceipt_item_Data['item_id'];
                                        ?>
                                        <tr class="pitems">
                                            <td>
                                                <?php echo $i++; ?>
                                            </td>
                                            <input type="hidden" id="sl_no_<?php echo $item["item_id"]; ?>" value="<?php echo $sl_no;
                                               $sl_no++; ?>" class="txtBox pastweek sl_no" name="sl_no[]" />
                                            <input type="hidden" id="item_type_id_<?php echo $item["item_id"]; ?>"
                                                value="<?php echo $item["item_id"]; ?>" class="txtBox pastweek item_id" name="item_id[]" />
                                            <input type="hidden" id="rate_<?php echo $item["item_id"]; ?>" class="form-control rate"
                                                value="<?php echo $rate; ?>" name="rates[]" />
                                            <input type="hidden" id="billedquantity_<?php echo $item["item_id"]; ?>"
                                                value="<?php echo $billedquantity; ?>" class="txtBox pastweek billedquantity"
                                                name="transferablequantity[]" />
                                            <input type="hidden" id="bill_unit_id_<?php echo $item["item_id"]; ?>"
                                                value="<?php echo $bill_unit_id; ?>" class="txtBox pastweek bill_unit_id"
                                                name="item_unit_id[]" />
                                            <input type="hidden" id="item_unit_base_unit_<?php echo $item["item_id"]; ?>"
                                                value="<?php echo $item_unit_base_unit; ?>" class="txtBox pastweek unit"
                                                name="item_unit_base_unit[]" />
                                            <input type="hidden" id="stock_item_<?php echo $item["item_id"]; ?>"
                                                value="<?php echo $item['warehousedespatch_itemid']; ?>" class="txtBox pastweek stock_item"
                                                name="stock_item[]" />
                                            <input type="hidden" id="itembatch_<?php echo $item["item_id"]; ?>" class="form-control batch"
                                                value="<?php echo $batch_despatch; ?>" name="batch[]" />
                                            <input type="hidden" id="warehouse_receipt_item_id_<?php echo $item["item_id"]; ?>"
                                                class="form-control warehouse_receipt_item_id"
                                                value="<?php echo $warehousereceipt_itemid; ?>" name="warehouse_receipt_item_id[]" />
                                            <input type="hidden" id="purchase_type_<?php echo $item["item_id"]; ?>"
                                                class="form-control purchase_type" value="<?php echo $purchase_type; ?>"
                                                name="purchase_type[]" />
                                            <input type="hidden" id="item_dimension_<?php echo $item["item_id"]; ?>"
                                                class="form-control item_dimension" value="<?php echo $item["dimension"]; ?>"
                                                name="warehouse_receipt_item_dimension[]" />
                                            <input type="hidden" id="item_dimension_category_<?php echo $item["item_id"]; ?>"
                                                class="form-control item_dimension_category"
                                                value="<?php echo $item["warehousestock_itemid_dimension_category"]; ?>"
                                                name="warehouse_receipt_item_dimension_category[]" />

                                            <td id="<?php echo $number - 1; ?>">
                                                <div id="categoryname<?php echo $number - 1; ?>">
                                                    <?php echo $categoryName; ?>
                                                </div>
                                            </td>
                                            <td id="<?php echo $number - 1; ?>">
                                                <div id="batch<?php echo $number - 1; ?>">
                                                    <?php echo $item["warehousedespatch_batch"]; ?>
                                                </div>
                                            </td>
                                            <?php if ($purchase_type != '') {
                                                if ($purchase_type == 'G') { ?>
                                                    <td id="<?php echo $number - 1; ?>">
                                                        <?php echo "(" . ($item["dimension"] != '' && $item["dimension"] != null) ? $item["dimension"] : "N/A" . ")"; ?>
                                                    </td>
                                                <?php } else {
                                                    echo "";
                                                }
                                                ?>
                                            <?php } ?>
                                            <td id="<?php echo $number - 1; ?>">
                                                <?php echo $bill_unit_id_data_dis; ?>
                                            </td>
                                            <td id="<?php echo $number - 1; ?>">
                                                <div id="billedquantity_<?php echo $number - 1; ?>">
                                                    <?php echo $billedquantity; ?>
                                                </div>
                                            </td>
                                            <td id="<?php echo $number - 1; ?>">
                                                <div id="itemrate_<?php echo $number - 1; ?>">
                                                    <?php echo $rate; ?>
                                                </div>
                                            </td>
                                            <td id="<?php echo $number - 1; ?>" style="width: 55px;">
                                                <div id="receivedquantity_<?php echo $number - 1; ?>"><input type="text"
                                                        id="receivedquantity_<?php echo $item["item_id"]; ?>"
                                                        class="form-control  receivedquantity validate[required,custom[number],min[0]]"
                                                        value="<?php echo isset($warehousereceipt_item_Data['warehousereceipt_quantity']) ? $warehousereceipt_item_Data['warehousereceipt_quantity'] : "0"; ?>"
                                                        name="receivedquantity[]" /></div>
                                            </td>
                                            <td id="<?php echo $number - 1; ?>" style="width: 55px;">
                                                <div id="acceptedquantity_<?php echo $number - 1; ?>"><input type="text"
                                                        id="accepted_quantity_<?php echo $item["item_id"]; ?>"
                                                        class="form-control  accepted_quantity validate[required,custom[number],min[0]]"
                                                        value="<?php echo isset($warehousereceipt_item_Data['warehousereceipt_accepted_quantity']) ? $warehousereceipt_item_Data['warehousereceipt_accepted_quantity'] : "0"; ?>"
                                                        name="accepted_quantity[]" /></div>
                                            </td>
                                            <td id="<?php echo $number - 1; ?>" style="width: 55px;">
                                                <div id="rejectedquantity_<?php echo $number - 1; ?>"><input type="text"
                                                        id="rejected_quantity_<?php echo $item["item_id"]; ?>"
                                                        class="form-control  rejected_quantity validate[required,custom[number],min[0]]"
                                                        value="<?php echo isset($warehousereceipt_item_Data['warehousereceipt_rejected_quantity']) ? $warehousereceipt_item_Data['warehousereceipt_rejected_quantity'] : "0"; ?>"
                                                        name="rejected_quantity[]" /></div>
                                            </td>

                                            <td id="<?php echo $number - 1; ?>">
                                                <div id="remark_data<?php echo $number - 1; ?>">
                                                    <textarea id="remark_data_<?php echo $item["item_id"]; ?>"
                                                        class="form-control  remark_data"
                                                        name="remark_data[]"><?php echo isset($warehousereceipt_item_Data['remark_data']) ? $warehousereceipt_item_Data['remark_data'] : ""; ?></textarea>


                                                </div>
                                            </td>


                                        </tr>
                                        <?php
                                    }
                                }
                            }
                        }
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>