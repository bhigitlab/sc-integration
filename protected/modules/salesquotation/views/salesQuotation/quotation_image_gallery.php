<div class="container">
    <div class="clearfix">
        <h2 class="purchase-title">UPLOAD IMAGES</h2>        
    </div>
    <div class="panel panel-gray" style="box-shadow: -5px 0px 2px 0px rgba(0,0,0,0.25);">
        <div class="panel-body">
            <?php
            $form = $this->beginWidget('CActiveForm', array(
                    'id' => 'upload-form',
                    'enableAjaxValidation' => false,
                    'htmlOptions' => array('enctype' => 'multipart/form-data'),
                    'enableAjaxValidation' => false,
                    'enableClientValidation' => true,
                    'clientOptions' => array(
                        'validateOnSubmit' => true,
                        'validateOnChange' => true,
                        'validateOnType' => false,
                    ),
                )
            ); ?>
                <div class="row">
                <div class="col-md-3">
                <?php  $sql1 = 'SELECT id FROM `jp_quotation_section` '
                        . 'WHERE `qtn_id` = ' . $qtid
                        . ' AND revision_no = "' . $rev_no . '"';
                $sectionArray = Yii::app()->db->createCommand($sql1)->queryColumn();
                $sectionIds = implode(',', $sectionArray);
                if(!empty($sectionIds)){
                    $categories = QuotationGenCategory::model()->findAll(array(
                        'select' => array('id, category_label'),
                        'order' => 'category_label ASC',
                        'condition' => 'section_id IN (' . $sectionIds . ')', // Use the section IDs in the condition
                        'distinct' => true,
                    ));
                }else{
                     $categories=[];
                }
                
                $categoryList = CHtml::listData($categories, 'id', 'category_label');
                ?>
                <div class="form-group">
                    <label for="bank">Subsection</label>
                    <?php
                       echo $form->dropDownList($model, 'category_id', $categoryList, array(
                        'class' => 'form-control js-example-basic-single',
                        'empty' => '-Select Subsection-',
                        'style' => 'width:100%',
                        ));
                        echo $form->error($model, 'category_id');
                        
                    ?>
                    </div>
                    </div>
                    <div class="col-md-3">
                        <?php
                        $sql = 'SELECT DISTINCT id as worktype_id, worktype_label FROM `jp_quotation_gen_worktype` '
                        . ' WHERE `qid` = ' . $qtid
                        . ' AND revision_no = "' . $rev_no . '"';
                $subitemArray = Yii::app()->db->createCommand($sql)->queryAll();
                $worktypedropdownList = CHtml::listData($subitemArray, 'worktype_id', 'worktype_label');
                ?>
                
                <div class="form-group">
                    <label for="bank">Work Type</label>
                    <?php
                    echo $form->dropDownList($model, 'worktype_id', $worktypedropdownList, array(
                        'class' => 'form-control js-example-basic-single',
                        'empty' => '-Select Worktype-',
                        'style' => 'width:100%',
                    ));
                    echo $form->error($model, 'worktype_id');
                        ?>
                    </div>
                </div>
                <div class="col-md-3">
                <?php
                    $sql = 'SELECT id,item_name FROM `jp_sales_quotation` '
                    . 'WHERE `parent_type` = 1 AND `master_id` = ' . $qtid
                    . ' AND revision_no="' . $rev_no . '" AND `deleted_status` = 1';
            $wrktypeitemArray = Yii::app()->db->createCommand($sql)->queryAll();
            $itemdropdownList = CHtml::listData($wrktypeitemArray, 'id', 'item_name');
                ?>
                
                <div class="form-group">
                    <label for="bank">Item</label>
                    <?php
                    echo $form->dropDownList($model, 'sales_quotation_id', $itemdropdownList, array(
                        'class' => 'form-control js-example-basic-single',
                        'empty' => '-Select Item-',
                        'style' => 'width:100%',
                    ));
                    echo $form->error($model, 'sales_quotation_id');
                    ?>
                    </div>
                </div>
                    <div class="col-md-3">
                        <?php
                        echo CHtml::hiddenField('SalesQuotationGallery[qtid]' , $qtid, array('id' => 'qtid'));            
                        echo $form->labelEx($model, 'image');
                        echo $form->fileField($model, 'image');
                        if ($model->hasErrors('image') && isset($_POST['SalesQuotationGallery'])) {
                            echo $form->error($model, 'image');
                        }
                        ?>
                    </div>
                </div>
                        <div class="row">
                    <div class="col-md-3">
                        <?php
                        echo $form->labelEx($model, 'caption');
                        echo $form->textField($model, 'caption',array('class'=>'form-control'));
                        echo $form->error($model, 'caption');
                        ?>
                    </div>
                    <div class="col-md-2">
                        <label class="d-block">&nbsp;</label>
                        <?php
                        echo CHtml::submitButton('Submit');
                        ?>
                    </div>
                </div>
            <?php
            $this->endWidget();
            ?>
        </div>
    </div>
    <div class="show_gallery_block">
        <!-- <h1 class="heading">Image Gallery with CSS Grid <span>& Flexbox Fallback</span></h1> -->

        <table class="table">
            <thead>
                <th>SI No.</th>
                <th>Section </th>
                <th>Worktype</th>
                <th>Subitem</th>
                <th>Item</th>
                <th>Image</th>
                <th>Description</th>
                <th>Action</th>
            </thead>
            <tbody>                       
                <?php
                $sql = 'SELECT s.*,g.image,g.caption,w.category_label_id,w.worktype_label as work_title,w.section_id,c.section_id,c.category_label as subsection_title,qs.section_name as section_title FROM `jp_sales_quotation` s'
                        . ' LEFT JOIN `jp_sales_quotation_gallery` g ON g.id = s.image_gallery_id'
                        . ' LEFT JOIN `jp_quotation_gen_worktype` w ON w.id = s.parent_id'
                        . ' LEFT JOIN `jp_quotation_gen_category` c ON c.id = w.category_label_id'
                        . ' LEFT JOIN `jp_quotation_section` qs ON qs.id = c.section_id'
                        .' WHERE s.master_id="'.$qtid.'" AND s.revision_no="'.$rev_no.'"';
                $images = Yii::app()->db->createCommand($sql)->queryAll();
                $i=1;
                foreach ($images as $key => $value) { 
                    if($value['image_gallery_id']>0){ ?>
                <tr>
                    <td><?php echo $i; ?></td> 
                    <td><?php echo $value['section_title']." - ". $value['subsection_title']; ?></td> 
                    <td><?php echo $value['work_title']." - ".$value['category_label']?></td>
                    <td><?php echo $value['worktype_label']?></td>
                    <td><?php echo $value['item_name']?></td>
                    <td>
                        <?php 
                        if ($value["image"] != '') {
                            $path = realpath(Yii::app()->basePath . '/../uploads/quotation_gallery/' . $value["image"]);
                            if (file_exists($path)) {
                                echo CHtml::tag('img',
                                    array(
                                        'src' => Yii::app()->request->baseUrl . "/uploads/quotation_gallery/" . $value["image"],
                                        'alt' => '',
                                        'class' => 'pop',
                                        'modal-src' => Yii::app()->request->baseUrl . "/uploads/quotation_gallery/" . $value["image"],
                                        'width'=>'100px'
                                    )
                                );
                            }
                        } 
                        ?>
                    </td>
                    <td><?php echo $value['caption']?></td>
                    <td width="100px"><button class="btn btn-sm btn-remove" id="<?php echo $value["id"] ?>">Remove</button></td>
                </tr>
                <?php $i++;
            } } ?> 
            </tbody>               
        </table>
    </div>
</div>
<?php $getWorkTypeBySubsection     = Yii::app()->createAbsoluteUrl("salesquotation/salesQuotation/getWorkTypeBySubsection"); ?>
<?php $getItemByWorktype     = Yii::app()->createAbsoluteUrl("salesquotation/salesQuotation/getItemByWorktype"); ?>

<script>
    $(document).on("click",".btn-remove",function(){
        var id = $(this).attr("id");
        var answer = confirm("Are you sure you want to remove?");
        if (answer) {
            $.ajax({
                method: "POST",                
                data: {
                    id: id
                },                
                url: '<?php echo Yii::app()->createAbsoluteUrl("/salesquotation/salesQuotation/removeQuotationImage"); ?>',
                success: function(response) {
                    setTimeout(function () {
                    location.reload(true);
                    },1500);
                }
            })
        }
    })
    $(document).ready(function() {
    $("#SalesQuotationGallery_category_id").change(function() {
            var cat_label_id = $(this).val();
            var qtn_id = "<?php echo $qtid; ?>";
            var rev_no = "<?php echo $rev_no; ?>";
            getWorkTypeBySubsection(qtn_id,rev_no,cat_label_id);

        });
        function getWorkTypeBySubsection(qtn_id,rev_no,cat_label_id) {
        $('#loading').show();
        $.ajax({
            url: "<?php echo $getWorkTypeBySubsection; ?>",
            data: {
                "cat_label_id": cat_label_id,
                "qtn_id":qtn_id,
                "rev_no":rev_no
            },
            type: "POST",
            success: function(data) {
                $("#SalesQuotationGallery_worktype_id").html(data);
            }
        });
    }
    $("#SalesQuotationGallery_worktype_id").change(function() {
            var worktype_id = $(this).val();
            var qtn_id = "<?php echo $qtid; ?>";
            var rev_no = "<?php echo $rev_no; ?>";
            getItemByWorktype(qtn_id,rev_no,worktype_id);

        });
        function getItemByWorktype(qtn_id,rev_no,worktype_id) {
        $('#loading').show();
        $.ajax({
            url: "<?php echo $getItemByWorktype; ?>",
            data: {
                "worktype_id": worktype_id,
                "qtn_id":qtn_id,
                "rev_no":rev_no
            },
            type: "POST",
            success: function(data) {
                $("#SalesQuotationGallery_sales_quotation_id").html(data);
            }
        });
    }
    });
    </script>

