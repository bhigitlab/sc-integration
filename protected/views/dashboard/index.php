<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/message/js/jquery.message.js"></script>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/message/css/jquery.message.css">
<script src="https://cdn.amcharts.com/lib/5/index.js"></script>
<script src="https://cdn.amcharts.com/lib/5/xy.js"></script>
<script src="https://cdn.amcharts.com/lib/5/themes/Micro.js"></script>
<script src="https://cdn.amcharts.com/lib/4/core.js"></script>
<script src="https://cdn.amcharts.com/lib/4/charts.js"></script>
<script src="https://cdn.amcharts.com/lib/4/themes/animated.js"></script>
<script src="https://cdn.amcharts.com/lib/5/percent.js"></script>
<script src="https://cdn.amcharts.com/lib/5/themes/Animated.js"></script>
<?php
$display_style = "";
$display_style1 = "display:none;";
?>


<!-- <div class="container pt-5" id="dashboard_data_holder"> -->
<div class="loading-overlay">
    <span class="fa fa-spinner fa-3x fa-spin"></span>
</div>
<div id="dashboard-content" class="page-wrapper-index">
    <div class="dashboard-title-area">
          <h4 class="fw-bold margin-zero">Dashboard</h4>
          <div class="dashboard-title-selects">
            <div class="dashboard-dropdown-container" id="dropdown1">
              <i class="fa fa-calendar"></i>
              <?php
              $range = isset($range) ? $range : 30; 
              ?>
              <select class="calendar" id="dateRangeSelect">
                <option value="30" <?php echo $range == 30 ? 'selected' : ''; ?>>Current Month</option>
                <option value="6" <?php echo $range == 6 ? 'selected' : ''; ?>>Last 6 Months</option>
                <option value="1" <?php echo $range == 1 ? 'selected' : ''; ?>>1 Year</option>
                <!-- <option value="custom">Custom range</option> -->
              </select>
            </div>
            <?php 
            $companies = Company::model()->findAll();
            ?>
            <div class="dashboard-dropdown-container" id="dropdown2">
              <select id="company_id">
                <option value="">Select a Company</option>
                <?php foreach ($companies as $company): ?>
                  <option value="<?php echo $company->id; ?>" <?php echo ($company->id == $companyId) ? 'selected' : ''; ?>>
                    <?php echo CHtml::encode($company->name); ?>
                  </option>
                <?php endforeach; ?>
              </select>
            </div>
          </div>
    </div>
    <?php 
    $paymentAgingData = json_decode($paymentAgingJson, true);
    ?>
    <div class="dashboard-body">
          <!-- Start of top area - microcharts -->
          <div class="dashboard-top-area">
            <div class="dashboard-accounts-payable dashboard-top-area-block">
              <div class="dashboard-block-header">Accounts Payable</div>
              <p class="dashboard-top-first">&#8377; <?php echo $currentPayable; ?></p>
              <div id="chartdiv1"></div>
              <p class="dashboard-top-second <?php echo $metrics['Accounts Payable']['percentage'] < 0 ? 'dashboard-negative' : 'dashboard-positive'; ?>">
                  <?php echo $metrics['Accounts Payable']['percentage']; ?>
              </p>
              <p class="dashboard-top-third">
              <?php if($range==30){
                echo 'vs previous month';
              }elseif($range==6){
                echo 'vs previous six month';
              }else{
                echo 'vs previous one year';
              } ?>
              </p>
            </div>
            <div class="dashboard-accounts-receivable dashboard-top-area-block">
              <div class="dashboard-block-header">Accounts Receivable</div>
              <p class="dashboard-top-first">&#8377;<?php echo $accountReceivable; ?></p>
              <div id="chartdiv2"></div>
              <p class="dashboard-top-second <?php echo $metrics['Accounts Receivable']['percentage'] < 0 ? 'dashboard-negative' : 'dashboard-positive'; ?>">
                <?php echo $metrics['Accounts Receivable']['percentage']; ?>
            </p>
              <p class="dashboard-top-third"> <?php if($range==30){
                echo 'vs previous month';
              }elseif($range==6){
                echo 'vs previous six month';
              }else{
                echo 'vs previous one year';
              } ?></p>
            </div>
            <div class="dashboard-cash-flow dashboard-top-area-block">
              <div class="dashboard-block-header">Cash Flow</div>
              <p class="dashboard-top-first">&#8377;<?php echo $accountCashflow; ?></p>
              <div id="chartdiv3"></div>
              <p class="dashboard-top-second <?php echo $metrics['Cash Flow']['percentage'] < 0 ? 'dashboard-negative' : 'dashboard-positive'; ?>">
                <?php echo $metrics['Cash Flow']['percentage']; ?>
            </p>
              <p class="dashboard-top-third"> <?php if($range==30){
                echo 'vs previous month';
              }elseif($range==6){
                echo 'vs previous six month';
              }else{
                echo 'vs previous one year';
              } ?></p>
            </div>
            <div class="dashboard-net-profit dashboard-top-area-block">
              <div class="dashboard-block-header">Net Profit</div>
              <p class="dashboard-top-first">&#8377;<?php echo $accountNetprofit; ?></p>
              <div id="chartdiv4"></div>
              <p class="dashboard-top-second <?php echo $metrics['Net Profit']['percentage'] < 0 ? 'dashboard-negative' : 'dashboard-positive'; ?>">
                <?php echo $metrics['Net Profit']['percentage']; ?>
            </p>
              <p class="dashboard-top-third">
                 <?php if($range==30){
                echo 'vs previous month';
              }elseif($range==6){
                echo 'vs previous six month';
              }else{
                echo 'vs previous one year';
              } ?>
              </p>
            </div>
            <div class="dashboard-gross-profit dashboard-top-area-block">
              <div class="dashboard-block-header">Gross Profit</div>
              <p class="dashboard-top-first">&#8377;<?php echo $accountGrossprofit; ?></p>
              <div id="chartdiv5"></div>
              <p class="dashboard-top-second <?php echo $metrics['Gross Profit']['percentage'] < 0 ? 'dashboard-negative' : 'dashboard-positive'; ?>">
                <?php echo $metrics['Gross Profit']['percentage']; ?>
            </p>
              <p class="dashboard-top-third"> 
                <?php if($range==30){
                echo 'vs previous month';
              }elseif($range==6){
                echo 'vs previous six month';
              }else{
                echo 'vs previous one year';
              } ?></p>
            </div>
          </div>

          <!-- Start of second area different graphs -->
          <div class="dashboard-second-area">
            <!-- Accounts Balance -->
            <div class="dashboard-accounts-balance dashboard-area-block">
              <!-- Filter area with dropdowns -->
              <div class="dashboard-filter-header">
                <div class="dashboard-block-header">Accounts Balance</div>
                <!-- <button class="dashboard-filter-button"><i class="fa fa-sliders"></i></button> -->
                <!-- <div class="dashboard-filter-dropdown hidden">
                  <div class="dashboard-filter-selects">
                    <div class="dashboard-dropdown-container" id="dropdown1">
                      <i class="fa fa-calendar"></i>
                      <select class="calendar">
                        <option value="30">Monthly</option>
                        <option value="7">Weekly</option>
                        <option value="1">Yearly</option>
                      </select>
                    </div>
                    <div class="dashboard-dropdown-container" id="dropdown2">
                      <select>
                        <option value="weekly">All Company</option>
                        <option value="weekly">Administrator</option>
                        <option value="monthly">Tony Stark</option>
                        <option value="yearly">Steve Rogers</option>
                      </select>
                    </div>
                  </div>
                </div> -->
              </div>
              <div class="dashboard-area-content">
                <div id="chartdiv6">
                  <div class="dashboard-blocker"></div>
                </div>
              </div>
            </div>

            <!-- Sales graph -->
            <div class="dashboard-sales-graph dashboard-area-block">
              <div class="dashboard-filter-header">
                <div class="dashboard-block-header">Sales</div>
                <!-- <button class="dashboard-filter-button"><i class="fa fa-sliders"></i></button>
                <div class="dashboard-filter-dropdown hidden">
                  <div class="dashboard-filter-selects">
                    <div class="dashboard-dropdown-container" id="dropdown1">
                      <i class="fa fa-calendar"></i>
                      <select class="calendar">
                        <option value="30">Monthly</option>
                        <option value="7">Weekly</option>
                        <option value="1">Yearly</option>
                      </select>
                    </div>
                    <div class="dashboard-dropdown-container" id="dropdown2">
                      <select>
                        <option value="weekly">All Company</option>
                        <option value="weekly">Administrator</option>
                        <option value="monthly">Tony Stark</option>
                        <option value="yearly">Steve Rogers</option>
                      </select>
                    </div>
                  </div>
                </div> -->
              </div>
              <div class="dashboard-area-content">
                <div id="chartdiv7">
                  <div class="dashboard-blocker"></div>
                </div>
              </div>
            </div>

            <!-- Profit and Loss -->
            <div class="dashboard-profit-loss dashboard-area-block">
              <div class="dashboard-filter-header">
                <div class="dashboard-block-header">Profit & Loss</div>
                <!-- <button class="dashboard-filter-button"><i class="fa fa-sliders"></i></button>
                <div class="dashboard-filter-dropdown hidden">
                  <div class="dashboard-filter-selects">
                    <div class="dashboard-dropdown-container" id="dropdown1">
                      <i class="fa fa-calendar"></i>
                      <select class="calendar">
                        <option value="30">Monthly</option>
                        <option value="7">Weekly</option>
                        <option value="1">Yearly</option>
                      </select>
                    </div>
                    <div class="dashboard-dropdown-container" id="dropdown2">
                      <select>
                        <option value="weekly">All Company</option>
                        <option value="weekly">Administrator</option>
                        <option value="monthly">Tony Stark</option>
                        <option value="yearly">Steve Rogers</option>
                      </select>
                    </div>
                  </div>
                </div> -->
              </div>
              <div class="dashboard-area-content">
                <div id="chartdiv8">
                  <div class="dashboard-blocker"></div>
                </div>
              </div>
            </div>
          </div>

          <!-- Start of third area pie of pie chart -->
          <div class="dashboard-third-area">
            <div class="dashboard-expense-graph dashboard-area-block">
              <div class="dashboard-block-header">Expense</div>
              <div class="dashboard-area-content dashboard-expense-graph-desktop">
                <div id="chartdiv9">
                  <div class="dashboard-blocker"></div>
                </div>
              </div>
              <!-- <div class="dashboard-expense-graph-mobile">
                <div class="dashboard-expense-graph-content">
                  <table class="table">
                    <thead>
                      <tr>
                        <th class="text-left">Total Expense</th>
                        <th class="text-right">1850000</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <th class="dashboard-indirect-expense">Indirect Expense</th>
                        <th class="text-right dashboard-indirect-expense">1150000</th>
                      </tr>
                      <tr>
                        <td>Salaries</td>
                        <td class="text-right">450000</td>
                      </tr>
                      <tr>
                        <td>Office Rent</td>
                        <td class="text-right">25000</td>
                      </tr>
                      <tr>
                        <td>Utilities</td>
                        <td class="text-right">55000</td>
                      </tr>
                      <tr>
                        <td>Depreciation</td>
                        <td class="text-right">50000</td>
                      </tr>
                      <tr>
                        <td>Office Supplies</td>
                        <td class="text-right">150000</td>
                      </tr>
                      <tr>
                        <td>Legal and Accounting Fees</td>
                        <td class="text-right">50000</td>
                      </tr>
                      <tr>
                        <td>Marketing and Advertising</td>
                        <td class="text-right">25000</td>
                      </tr>
                      <tr>
                        <td>Bank Charges</td>
                        <td class="text-right">35000</td>
                      </tr>
                      <tr>
                        <td>Communication Costs</td>
                        <td class="text-right">25000</td>
                      </tr>
                      <tr>
                        <td>Maintenance and Repairs</td>
                        <td class="text-right">5000</td>
                      </tr>
                      <tr>
                        <td>Loan</td>
                        <td class="text-right">325000</td>
                      </tr>
                      <tr>
                        <th class="dashboard-direct-expense">Direct Expense</th>
                        <th class="text-right dashboard-direct-expense">700000</th>
                      </tr>
                      <tr>
                        <td>Materials</td>
                        <td class="text-right">250000</td>
                      </tr>
                      <tr>
                        <td>Labour</td>
                        <td class="text-right">150000</td>
                      </tr>
                      <tr>
                        <td>Subcontractor</td>
                        <td class="text-right">100000</td>
                      </tr>
                      <tr>
                        <td>Rental</td>
                        <td class="text-right">100000</td>
                      </tr>
                      <tr>
                        <td>Transportation</td>
                        <td class="text-right">100000</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div> -->
            </div>
          </div>

          <!-- Start of fourth area - table -->
          <div class="dashboard-fourth-area">
            <div class="dashboard-profit-loss dashboard-area-block">
              <div class="dashboard-filter-header">
                <div class="dashboard-block-header dashboard-empty-header"></div>
                <!-- <button class="dashboard-filter-button"><i class="fa fa-sliders"></i></button>
                <div class="dashboard-filter-dropdown hidden">
                  <div class="dashboard-filter-selects">
                    <div class="dashboard-dropdown-container" id="dropdown1">
                      <i class="fa fa-calendar"></i>
                      <select class="calendar">
                        <option value="30">Monthly</option>
                        <option value="7">Weekly</option>
                        <option value="1">Yearly</option>
                      </select>
                    </div>
                    <div class="dashboard-dropdown-container" id="dropdown2">
                      <select>
                        <option value="weekly">All Company</option>
                        <option value="weekly">Administrator</option>
                        <option value="monthly">Tony Stark</option>
                        <option value="yearly">Steve Rogers</option>
                      </select>
                    </div>
                  </div>
                </div> -->
              </div>
              <?php 
              $profit_loss_array = Controller::getProfitLossTable();
              // echo'<pre>';print_r($profit_loss_array);exit;
              ?>
              <div class="dashboard-table-content">
                <div class="dashboard-block-header dashboard-border-none">Profit & Loss</div>
                <div class="dashboard-profit-loss-table">
                  <table class="table">
                    <thead>
                      <tr>
                        <th>Sl No.</th>
                        <th>Company</th>
                        <th>Total Receipt</th>
                        <th>Total invoice</th>
                        <th>Total Expense</th>
                        <th>Total Daily Expense</th>
                        <th>Total Deposit</th>
                        <th>Purchase Return</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php 
                        $sl_no = 1;
                       
                        foreach ($profit_loss_array as $data) {
                          $company_name = Company::model()->findByPk($data['Company']);
                          $company_name = $company_name ? $company_name->name : 'N/A';

                          echo '<tr>';
                          echo '<td>' . $sl_no . '</td>';
                          echo '<td>' .$company_name. '</td>';
                          echo '<td class="text-right">' . (isset($data['Total Receipt']) ? number_format($data['Total Receipt'], 2) : '0.00') . '</td>';
                          echo '<td class="text-right">' . (isset($data['Total Invoice']) ? number_format($data['Total Invoice'], 2) : '0.00') . '</td>';
                          echo '<td class="text-right">' . (isset($data['Total Expense']) ? number_format($data['Total Expense'], 2) : '0.00') . '</td>';
                          echo '<td class="text-right">' . (isset($data['Total Daily Expense']) ? number_format($data['Total Daily Expense'], 2) : '0.00') . '</td>';
                          echo '<td class="text-right">' . (isset($data['Total Deposit']) ? number_format($data['Total Deposit'], 2) : '0.00') . '</td>';
                          echo '<td class="text-right">' . (isset($data['Purchase Return']) ? number_format($data['Purchase Return'], 2) : '0.00') . '</td>';
                          echo '</tr>';
                          $sl_no++;
                        }
                      ?>
                    </tbody>
                  </table>
                </div>
                <p class="dashboard-profit-loss-disclaimer"><span>***</span>Note: Includes expenses of all projects</p>
              </div>
            </div>
          </div>
    </div>
   
</div>
<!-- </div> -->

<script type="text/javascript">
  

    $(document).ready(function() {})
    //graphes 
     var lineGraphData = <?php echo $lineGraphJson; ?>;
     var lineGraphReceivableJson = <?php echo $lineGraphReceivableJson; ?>;
     var lineGraphCashflowJson = <?php echo $lineGraphCashflowJson; ?>;
     var lineGraphGrossprofitJson = <?php echo $lineGraphGrossprofitJson; ?>;
     var lineGraphNetprofitJson = <?php echo $lineGraphNetprofitJson; ?>;
     var paymentAgingData = <?php echo $paymentAgingJson; ?>;
     var profitlossData = <?php echo $profitlossJson; ?>;
     var salesData = <?php echo $salesJson; ?>;
     var expenseData = <?php echo $expenseJson; ?>;
     var accountBalanceJson = <?php echo $accountBalanceJson; ?>;

      $('#dateRangeSelect').change(function () {
       
        var selectedValue = $(this).val();
        var selectedCompany = $('#company_id').val();
        var url = 'index.php?r=dashboard/index&range=' + selectedValue +'&company='+selectedCompany;        
        window.location.href = url;
      });

      $('#company_id').change(function () {
        // Get the selected value
        var selectedCompany = $(this).val();
        var selectedDateRange = $('#dateRangeSelect').val();
        var url = 'index.php?r=dashboard/index&company=' + selectedCompany + '&range=' + selectedDateRange;
        window.location.href = url;
      });
    

    function updateRequestAction(id, status, tableName, modelName, parentName) {
        $('.loading-overlay').addClass('is-active');
        $.ajax({
            type: "GET",
            data: {
                id: id,
                status: status,
                tableName: tableName,
                modelName: modelName,
                parentName: parentName

            },
            url: "<?php echo Yii::app()->createUrl("dashboard/updaterequestaction") ?>",
            success: function(data) {
                $('.compare_box').hide();
                if (data.response == 'success') {
                    $("#errormessage").show()
                        .html('<div class="alert alert-success">' + data.msg + '</div>')
                        .fadeOut(10000);
                        setTimeout(function(){
                        window.location.reload(1);
                        }, 3000);

                }
                if (data == 2) {
                    $("#errormessage").show()
                        .html('<div class="alert alert-warning">Failed! Please try again.</div>')
                        .fadeOut(10000);
                        setTimeout(function(){
                        window.location.reload(1);
                        }, 3000);
                    return false;
                } else {
                    setTimeout(location.reload, 200);
                    // $("#newlist").html(data);
                    if (status == 1) {
                        $("#errormessage").show()
                            .html('<div class="alert alert-success">Success! Request approved successfully.</div>')
                            .fadeOut(10000);
                    } else if (status == 2) {
                        $("#errormessage").show()
                                .html('<div class="alert alert-success">Success! Request cancelled successfully.</div>')
                                .fadeOut(10000);
                    }
                    setTimeout(function(){
                    window.location.reload(1);
                    }, 3000);
                }
              location.reload();
            }
            
        });
        location.reload();
        
    }

    function deleteRequestAction(delId, status) {
        $('.loading-overlay').addClass('is-active');
        $.ajax({
            type: "GET",
            dataType:"json",
            data: {
                delId: delId,
                status: status
            },
            url: "<?php echo Yii::app()->createUrl("dashboard/deleterequestaction") ?>",
            success: function (data) {                
                $("#errormessage").show()
                    .html('<div class="alert alert-'+data.response+'">'+data.msg+'</div>')
                    .fadeOut(5000); 
                setTimeout(function(){
                window.location.reload(1);
                }, 3000);

            }
        });
    }

    function updateAllRequestAction(editArray, status) {
        $('.loading-overlay').addClass('is-active');
        $.ajax({
            type: "GET",
            data: {
                records: editArray,
                status: status
            },
            url: "<?php echo Yii::app()->createUrl("dashboard/allupdaterequestaction") ?>",
            success: function (data) {
                $("#newlist").html(data);
                if (status == 1) {
                    $("#errormessage").show()
                            .html('<div class="alert alert-success">Success! Selected requests are approved successfully.</div>')
                            .fadeOut(10000,function(){
                                location.reload();
                            });
                            
                } else if (status == 2) {
                    $("#errormessage").show()
                            .html('<div class="alert alert-success">Success! Selected requests are cancelled successfully.</div>')
                            .fadeOut(10000);
                }
            }
        });
    }

    function deleteAllRequestAction(deleteArray, status) {
        $('.loading-overlay').addClass('is-active');
        $.ajax({
            type: "GET",
            dataType:"json",
            data: {
                deleteArray: deleteArray,
                status: status
            },
            
            url: "<?php echo Yii::app()->createUrl("dashboard/alldeleterequestaction") ?>",
            success: function (data) {
                $("#errormessage").show()
                    .html('<div class="alert alert-'+data.response+'">'+data.msg+'</div>')
                    .fadeOut(5000);                 
            }
            
        });
        setTimeout(function(){
                window.location.reload(1);
                }, 3000);
    }

    $(document).on('click', '.cancel_edit', function () {
        var id = $(this).attr("id");
        var tableName = $(this).attr('data-table');
        var modelName = $(this).attr('data-model');
        var parentName = $(this).attr('data-parent');


        if (confirm("Do you really want to cancel this request?")) {
            updateRequestAction(id, 2, tableName, modelName, parentName);
        } else {
            return false;
        }
    });
    $(document).on('click', '.approve_edit', function () {
        var id = $(this).attr("id");
        var tableName = $(this).attr('data-table');
        var modelName = $(this).attr('data-model');
        var parentName = $(this).attr('data-parent');

        if (confirm("Are you sure you want to accept this request?")) {
            updateRequestAction(id, 1, tableName, modelName, parentName);
        } else {
            return false;
        }
    });

    $(document).on('click', '.cancel_delete', function () {
        var id = $(this).attr("id");
        if (confirm("Do you really want to cancel this request?")) {
            deleteRequestAction(id, 2);
        } else {
            return false;
        }
    });
    $(document).on('click', '.approve_delete', function () {
        var id = $(this).attr("id");
        if (confirm("Are you sure you want to accept this request?")) {
            deleteRequestAction(id, 1);
        } else {
            return false;
        }
    });
    $(document).on('click', '#approveAllEdit', function () {
        var editArray = [];
        $("input:checkbox[name=editrequest]:checked").each(function () {
            editArray.push({id: $(this).data('id'),ref_id:$(this).data('ref_id'),
            tableName: $(this).data('table'),
            modelName: $(this).data('model'),
            parentName: $(this).data('parent')});
           
        });
         //alert(editArray);
        if (editArray.length > 0)
            updateAllRequestAction(editArray, 1);
        else
            alert("Please select atleast one item.");
    });
    $(document).on('click', '#cancelAllEdit', function () {
        var editArray = [];
        $("input:checkbox[name=editrequest]:checked").each(function () {
            editArray.push($(this).val());
        });
        if (editArray.length > 0)
            updateAllRequestAction(editArray, 2);
        else
            alert("Please select atleast one item.");
    });
    $(document).on('click', '#approveAllDelete', function () {
        var deleteArray = [];
        $("input:checkbox[name=deleterequest]:checked").each(function () {
            deleteArray.push($(this).val());
        });
        if (deleteArray.length > 0)
            deleteAllRequestAction(deleteArray, 1);
        else
            alert("Please select atleast one item.");
    });
    $(document).on('change', '.clearnotificationcheck', function() {
        if ($('.clearnotificationcheck:checked').length > 0) {
            $('#clearNotification').show();
        } else {
            $('#clearNotification').hide();
        }
    });
       

    $(document).on('click', '#cancelAllDelete', function () {
        var deleteArray = [];
        $("input:checkbox[name=deleterequest]:checked").each(function () {
            deleteArray.push($(this).val());
        });
        if (deleteArray.length > 0)
            deleteAllRequestAction(deleteArray, 2);
        else
            alert("Please select atleast one item.");
    });
    $(document).on('click', '#checkalledit', function () {
        if ($(this).prop('checked')) {
            $('.editrequestcheck').prop('checked', true);
        } else {
            $('.editrequestcheck').prop('checked', false);
        }
    });
    $(document).on('click', '#checkallnotifi_al', function () {
        if ($(this).prop('checked')) {
            $(this).prop('checked', true);
            $('.clearcheck_all').prop('checked', true);
            $('#clearNotification').show();
        } else {
            $('.clearcheck_all').prop('checked', false);
            $('#clearNotification').hide();
        }
    });
    $(document).on('click', '#checkallnotifi_ea', function () {
        if ($(this).prop('checked')) {
            $(this).prop('checked', true);
            $('.clearcheck_ea').prop('checked', true);
            $('#clearNotification').show();
        } else {
            $('.clearcheck_ea').prop('checked', false);
            $('#clearNotification').hide();
        }
    });
    $(document).on('click', '#checkallnotifi_sc', function () {
        if ($(this).prop('checked')) {
            $(this).prop('checked', true);
            $('.clearcheck_sa').prop('checked', true);
            $('#clearNotification').show();
        } else {
            $('.clearcheck_sa').prop('checked', false);
            $('#clearNotification').hide();
        }
    });
    $(document).on('click', '#checkallnotifi_no', function () {
        if ($(this).prop('checked')) {
            $(this).prop('checked', true);
            $('.clearcheck_no').prop('checked', true);
            $('#clearNotification').show();
        } else {
            $('.clearcheck_no').prop('checked', false);
            $('#clearNotification').hide();
        }
    });
    $(document).on('click', '#checkalldelete', function () {
        if ($(this).prop('checked')) {
            $('.deleterequestcheck').prop('checked', true);
        } else {
            $('.deleterequestcheck').prop('checked', false);
        }
    });
    $(document).on('click', '.editrequestcheck', function () {
        if (!$(this).prop('checked')) {
            $('#checkalledit').prop('checked', false);
        }
    });
    $(document).on('click', '.deleterequestcheck', function () {
        if (!$(this).prop('checked')) {
            $('#checkalldelete').prop('checked', false);
        }
    });

    $('#download').click(function () {
        html2canvas($("#contenthtml"), {
            onrendered: function (canvas) {
                var imgData = canvas.toDataURL('image/jpeg');

                var imgWidth = 210;
                var pageHeight = 295;
                var imgHeight = canvas.height * imgWidth / canvas.width;
                var heightLeft = imgHeight;
                var doc = new jsPDF('p', 'mm');
                var position = 0;
                doc.page = 1;

                doc.addImage(imgData, 'jpeg', 0, position, imgWidth, imgHeight);
                heightLeft -= pageHeight;

                while (heightLeft >= 0) {
                    position = heightLeft - imgHeight;
                    doc.addPage();
                    doc.addImage(imgData, 'jpeg', 0, position, imgWidth, imgHeight);
                    heightLeft -= pageHeight;
                }
                doc.save('expenses.pdf');

            }
        });
    });

    $(document).ready(function () {
        var lengthText = 30;
        var shortText;
        var id;
        $('.desc').hover(function () {
            $(this).text(text);
            id = $(this).attr('id');
            var text = $('#' + id).text();
            shortText = $.trim(text).substring(0, lengthText).split(" ").slice(0, -1).join(" ") + "...";

        }, function () {
            $(id).text(shortText);
        });
    });
    $(document).delegate(".edit_area input[type=checkbox]", "click", function () {
        if ($(".edit_area").find("input:checked").length > 0) {
            $("#cancelAllEdit,#approveAllEdit").show();
        } else {
            $("#cancelAllEdit,#approveAllEdit").hide();
        }
    });
    $(document).delegate(".delete_area input[type=checkbox]", "click", function () {
        if ($(".delete_area").find("input:checked").length > 0) {
            $("#cancelAllDelete,#approveAllDelete").show();
        } else {
            $("#cancelAllDelete,#approveAllDelete").hide();
        }
    });
    $(document).delegate("#checkalledit", "click", function () {
        if (this.checked) {
            $("#cancelAllEdit,#approveAllEdit").show();
        } else {
            $("#cancelAllEdit,#approveAllEdit").hide();
        }
        return true;
    });
    $(document).delegate("#checkalldelete", "click", function () {
        if (this.checked) {
            $("#cancelAllDelete,#approveAllDelete").show();
        } else {
            $("#cancelAllDelete,#approveAllDelete").hide();
        }
        return true;
    });
    $(document).delegate("#newlist input[type=checkbox]", "change", function () {
        var total_box = $("#newlist").find("input[type=checkbox]").length;
        var checked_box = $("#newlist").find("input:checked").length;
        if (total_box == checked_box) {
            $("#checkalledit").prop('checked', true);
        } else {
            $("#checkalledit").prop('checked', false);
        }

    });
    $(document).delegate("#newlist1 input[type=checkbox]", "change", function () {
        var total_box = $("#newlist1").find("input[type=checkbox]").length;
        var checked_box = $("#newlist1").find("input:checked").length;
        if (total_box == checked_box) {
            $("#checkalldelete").prop('checked', true);
        } else {
            $("#checkalldelete").prop('checked', false);
        }

    });

    $(document).on("click", ".view_not", function (e) {
        $('.loading-overlay').addClass('is-active');
        var notification_id = this.id;
        e.stopImmediatePropagation();
        e.preventDefault();
        $.ajax({
            type: 'POST',
            url: "<?php echo Yii::app()->createUrl("dashboard/notificationview") ?>",
            data: {
                id: notification_id
            },
            success: function (data) {
                if (data == "success") {
                    location.reload();
                }
            },
            error: function (data) {
                alert('Error occured.please try again');
            },

        });
    })

    $(document).on('click', '.approve_po', function (e) {
        $('.loading-overlay').addClass('is-active');
        e.preventDefault();
        var element = $(this);
        var item_id = $(this).attr('id');
        $.ajax({
            url: '<?php echo Yii::app()->createAbsoluteUrl('purchase/permission'); ?>',
            type: 'POST',
            dataType: 'json',
            data: {
                item_id: item_id
            },
            success: function (response) {
                if (response.response == 'success') {
                    location.reload();
                } else if (response.response == 'warning') {

                } else {
                }
            }
        });
    });
    $(document).on('click', '.view_expense', function (e) {
        e.preventDefault();
        var element = $(this);
        var id = $(this).attr('id');
        $.ajax({
            url: '<?php echo Yii::app()->createAbsoluteUrl('dashboard/changeexpenseview'); ?>',
            type: 'POST',
            dataType: 'json',
            data: {
                id: id
            },
            success: function (response) {
                if (response.response == 'success') {
                    location.reload();
                } else if (response.response == 'warning') {

                } else {
                }
            }
        });
    });
      $(document).on('change', '.expenseclearnotificationcheck', function() {
        if ($('.expenseclearnotificationcheck:checked').length > 0) {
             $('#clearNotification').hide();
           $('#expenseclearNotification').show();
            
         } else {
            $('#expenseclearNotification').hide();
        }
        });


    $(document).on('click', '.po_rate_decline', function (e) {
        if (confirm("Do you really want to decline this request ?")) {
            $(this).parents(".parent_row").find(".box").animate({
                width: "400px"
            });                        
        }else {
            return false;
        }

    });

    $(document).on('click', '.panel-close', function (e) {
        $(this).parents(".parent_row").find(".box").animate({
            width: "0px"
        });
    });

    $(document).on('click', '.decline_comment_submit', function (e) {
        $('.loading-overlay').addClass('is-active');
        e.preventDefault();
        var element = $(this);
        var item_id = $(this).attr('id');
        var comments = $(this).parents(".row").find(".decline_remarks").val();        
        
        $.ajax({
            url: '<?php echo Yii::app()->createAbsoluteUrl('purchase/ratedecline'); ?>',
            type: 'POST',
            dataType: 'json',
            data: {
                id: item_id,
                comments:comments
            },
            success: function (response) {
                if (response.response == 'success') {
                    $("#dashboard_data_holder").load(location.href + " #dasboard_data");
                    $().toastmessage('showSuccessToast', response.msg);
                } else if (response.response == 'error') {
                    $().toastmessage('showErrorToast', "Error occured");
                } else {
                }
                setTimeout(function() {
                    location.reload();
                }, 3000);
            }
        });
    
    });
    $(function () {
        $('.tab_btn').click(function () {
            $(this).addClass("active");
            $(this).siblings().removeClass("active");
        });
        $(".parent_row").css("background-color", "#fafafa");
        $(".parent_row:even").css("background-color", "#fff");
        $('.compare_icon').click(function (e) {
            $(this).hide();
            $(this).siblings('.box_close').show();
            $(this).parents(".box-collapse").siblings('.compare_box').slideDown();
        });
        $('.box_close').click(function () {
            $(this).hide();
            $(this).siblings('.compare_icon').show();
            $(this).parents(".box-collapse").siblings('.compare_box').slideUp();
        });
    })
    $(document).ajaxComplete(function () {
        $('.loading-overlay').removeClass('is-active');
        $('#loading').hide();
    });
    
</script>

<style>
    .parent_row{
        position:relative;
    }
    .box{
        position: absolute;
        right: 0;
        overflow: hidden;
        z-index: 1;
        width: 0px;
    }
    
    .box-inner{        
        padding: 10px;        
    }
</style>