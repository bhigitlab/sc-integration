<?php
if (Yii::app()->user->role != 1) {
	$warehouse = Warehouse::model()->findAll(array('condition' => 'FIND_IN_SET(' . Yii::app()->user->id . ', assigned_to)'));
} else {
	$warehouse = Warehouse::model()->findAll();
}
$vendor = Vendors::model()->findAll();
$user = Users::model()->findAll();
$tblpx = Yii::app()->db->tablePrefix;
$receipt = Yii::app()->db->createCommand("SELECT warehousereceipt_id FROM {$tblpx}warehousereceipt ORDER BY warehousereceipt_id DESC")->queryRow();
if (empty($receipt)) {
	$receipt_no = 1;
} else {
	$receipt_no = $receipt['warehousereceipt_id'] + 1;
}
?>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.js"></script>

<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/message/css/jquery.message.css">
<script type="text/javascript"
	src="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/message/js/jquery.message.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/tableHeadFixer.js"></script>

<!--<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/fonts/glyphicons-halflings-regular.eot">
<link rel="stylesheet" href="<?php //echo Yii::app()->theme->baseUrl; ?>/plugins/smoke/css/smoke.min.css">
<script type="text/javascript" src="<?php // echo Yii::app()->theme->baseUrl; ?>/plugins/smoke/js/smoke.min.js"></script>-->
<script>
	var shim = (function () { document.createElement('datalist'); })();
</script>
<script>
	$(function () {
		$("#datepicker").datepicker({ dateFormat: 'dd-mm-yy' }).datepicker("setDate", new Date());

	});

</script>
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/responsivestyle.css" />
<div class="container">
	<div class="expenses-heading">
		<div class="clearfix">
			<!-- remove addentries class -->
			<?php echo CHtml::Button('Back', array('class' => 'btn btn-primary pull-right mt-0 mb-10', 'onclick' => 'javascript:location.href="' . $this->createUrl('index') . '"')); ?>
			<h3>Add Warehouse Receipt</h3>
		</div>
	</div>
	<div>
		<div id="msg_box"></div>
		<?php
		$activeProjectTemplate = $this->getActiveTemplate();
		$wrNo = "";
		$readonly = "";

		?>

		<form id="warehousereceipt_form" method="post"
			action="<?php echo $this->createAbsoluteUrl('warehousereceipt/savereceiptitem'); ?>">
			<?php echo CHtml::hiddenField('remove', '', array('id' => "remove")); ?>
			<?php echo CHtml::hiddenField('warehousereceipt_id', 0, array('id' => "warehousereceipt_id")); ?>
			<?php echo CHtml::hiddenField('item_unit_id_hidden', 0, array('id' => "item_unit_id_hidden")); ?>
			<div class="entries-wrapper block_purchase">
				<div class="row">
					<div class="col-xs-12">
						<div class="heading-title">Add Details</div>
						<div class="dotted-line"></div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-md-3">
						<div class="form-group">
							<label>WAREHOUSE To: <span class="required">*</span></label>
							<?php

							if (yii::app()->user->role == 5) {
								$warehouse_data = Controller::getCategoryOptions($type = 1);
								$data = CHtml::listData(Controller::getCategoryOptions($type = 1), 'id', 'text', 'group');
							} else {
								$warehouse_data = Controller::getCategoryOptions();
								$data = CHtml::listData(Controller::getCategoryOptions(), 'id', 'text', 'group');
							}

							?>

							<?php
							$bill = array();
							if (isset($_GET['bill_id'])) {
								$bill = Bills::model()->findByPk($_GET['bill_id']);
							}

							$warehouse_selected = $model['warehousereceipt_warehouseid'];
							if (!empty($bill)) {
								$warehouse_selected = $bill['warehouse_id'];
							}

							echo CHtml::activeDropDownList($model, 'warehousereceipt_warehouseid', $data, array(
								'empty' => '-Choose a Warehouse-',
								'class' => 'form-control mandatory',
								'options' => array($warehouse_selected => array('selected' => true))
							)); ?>
						</div>
					</div>
					<div class="col-xs-12 col-md-3">
						<div class="form-group">
							<label>DATE : <span class="required">*</span></label>
							<input type="text"
								value="<?php echo ((isset($date) && $date != '') ? date("d-m-Y", strtotime($date)) : ''); ?>"
								id="datepicker" class="txtBox date inputs target form-control" name="warehouse_date"
								placeholder="Please click to edit" readonly="true">
						</div>
					</div>
					<div class="col-xs-12 col-md-3">
						<div class="form-group">
							<label>RECEIPT CLERK : <span class="required">*</span></label>
							<select name="clerk" class="form-control inputs target clerk" id="clerk">
								<?php
								if (yii::app()->user->role == 1) { ?>
									<option value="">Choose Receipt Clerk</option>
									<?php
									foreach ($user as $key => $value) {
										?>
										<option value="<?php echo $value['userid']; ?>">
											<?php echo $value['first_name'] . ' ' . $value['last_name']; ?>
										</option>
										<?php
									}
								} else {
									foreach ($user as $key => $value) {
										if ($value['userid'] == Yii::app()->user->id) {
											?>

											<option value="<?php echo $value['userid']; ?>">
												<?php echo $value['first_name'] . ' ' . $value['last_name']; ?>
											</option>
										<?php }
									}
								} ?>
							</select>
						</div>
					</div>

					<div class="col-xs-12 col-md-3">
						<div class="form-group">
							<label>Transfer Type : <span class="required">*</span></label>
							<select name="transfer_type" class="form-control inputs target transfer_type"
								id="transfer_type">
								<?php
								$selected = "";
								if (!empty($bill)) {
									$selected = "selected";
								}
								?>
								<option value="">Choose Transfer Type</option>
								<option value="1" <?php echo $selected ?>>Purchase Bill</option>
								<option value="2">Despatch </option>
							</select>
						</div>
					</div>
					<div class="col-xs-12 col-md-3 elem_block_purchase_bill_no" style="display:none">
						<div class="form-group">
							<label>Purchase Bill No : <span class="required">*</span></label>
							<select name="purchase_bill_no" class="form-control inputs target purchase_bill_no"
								id="purchase_bill_no">
								<option value="">Choose Purchase Bill No</option>
							</select>
						</div>
					</div>
					<div class="col-xs-12 col-md-3 elem_block_warehouseid_from " style="display:none">
						<div class="form-group">
							<label>WAREHOUSE FROM: <span class="required">*</span></label>
							<?php $data = CHtml::listData(Controller::getCategoryOptions(), 'id', 'text', 'group'); ?>

							<?php echo CHtml::activeDropDownList($model, 'warehousereceipt_warehouseid_from', $data, array(
								'empty' => '-Choose a Warehouse-',
								'class' => 'form-control mandatory',
								'options' => array($model['warehousereceipt_warehouseid_from'] => array('selected' => true))
							)); ?>
						</div>
					</div>
					<div class="col-xs-12 col-md-3 elem_block_dispatch_no" style="display:none">
						<div class="form-group">
							<label>Dispatch No : <span class="required">*</span></label>
							<select name="dispatch_no" class="inputs target dispatch_no" id="dispatch_no">
								<option value="">Choose Dispatch No</option>
							</select>
						</div>
					</div>
					<div class="col-xs-12 col-md-3">
						<div class="form-group">
							<label>RECEIPT NO : <span class="required">*</span></label>
							<input type="text" value="<?php echo $wrNo ?>" id="receipt_no"
								class="receipt_no form-control" name="receipt_no" <?php echo $readonly ?>>
						</div>
					</div>


					<div class="col-xs-12 col-md-3">
						<div class="form-group">
							<label>Remarks :</label>
							<?php /* <input type="text" name="vendor" id="vendor" class="form-control"> */ ?>
							<textarea rows="2" cols="30" name="vendor" id="vendor" class="form-control"></textarea>
						</div>
					</div>

				</div>

				<div class="clearfix"></div>
				<div id="msg"></div>

				<div id="previous_details"
					style="position: fixed;top: 94px; left: 70px; right: 70px; z-index: 2; max-width:1150px; margin:0 auto;">
				</div>

				<div id="warehousereceipt_items"></div>

				<!-- Additional Items-->
				<?php echo $this->renderPartial('_additional_items', array('specification' => $specification)) ?>
				<div class="row">
					<div class="col-xs-12">
						<div class="subcontractor-bill-table table-responsive">
							<table class="table" id="add_item_table">
								<thead class="entry-table">
									<tr>
										<th>Sl.No</th>
										<th>Item</th>
										<th>Batch</th>
										<th>Unit/Size</th>
										<th>Quantity</th>
										<th>Rate</th>
										<th>Base Quantity</th>
										<th>Base Unit</th>
										<th>Base Rate</th>
										<th>Amount</th>
										<th></th>
									</tr>
								</thead>
								<tbody class="addrow">

								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div style="padding-right: 0px;"
					class="col-md-offset-9 col-md-3 col-sm-offset-8 col-sm-4 col-xs-offset-6 col-xs-6">

					<input type="hidden" value="<?php echo ((isset($subtot) && $subtot != '') ? $subtot : ''); ?>"
						class="txtBox pastweek" readonly=ture name="subtot" /></td>
					<input type="hidden" value="<?php echo ((isset($grand) && $grand != '') ? $grand : ''); ?>"
						class="txtBox pastweek grand" name="grand" readonly=true />
				</div>

				<div class="row">
					<div class="form-group col-xs-12 text-right ">
						<?php echo CHtml::Button($model->isNewRecord ? 'Create' : 'Save', array('class' => 'buttonsubmit btn btn-primary', 'id' => 'buttonsubmit', 'disabled' => true)); ?>
					</div>
				</div>
		</form>
	</div>
</div>



<input type="hidden" name="final_amount" id="final_amount" value="0">

<input type="hidden" name="item_amount_temp" id="item_amount_temp" value="0">


<style>
	a.pdf_excel {
		background-color: #6a8ec7;
		display: inline-block;
		padding: 8px;
		color: #fff;
		border: 1px solid #6a8ec8;
	}
</style>


<?php $url = Yii::app()->createAbsoluteUrl("wh/Warehousereceipt/GetItemsByPurchaseOrDespatch"); ?>
<script>

	jQuery.extend(jQuery.expr[':'], {
		focusable: function (el, index, selector) {
			return $(el).is('button, :input, [tabindex]');
		}
	});

	$(document).on('keypress', 'input,select,textarea', function (e) {
		if (e.which == 13) {
			e.preventDefault();
			// Get all focusable elements on the page
			var $canfocus = $(':focusable');
			var index = $canfocus.index(document.activeElement) + 1;
			if (index >= $canfocus.length) index = 0;
			$canfocus.eq(index).focus();
		}
	});

	$(document).ready(function () {
		$(".js-example-basic-single").select2();
		$("#Warehousereceipt_warehousereceipt_warehouseid,.dispatch_no,.purchase_bill_no,#Warehousereceipt_warehousereceipt_warehouseid_from,.vendor,.clerk,#transfer_type").select2();
		//$(".stock_item").select2();        
		$('select').first().focus();

		var bill_id = "<?php echo isset($_GET["bill_id"]) ? $_GET["bill_id"] : "" ?>";
		if (bill_id != "") {
			$("#Warehousereceipt_warehousereceipt_warehouseid").trigger("change");
			$("#transfer_type").trigger("change");
			$("#purchase_bill_no").val(bill_id);

		}

	});
	$("#buttonsubmit").keypress(function (e) {
		if (e.keyCode == 13) {
			$('.buttonsubmit').click();
		}
	});

	$(".buttonsubmit").click(function () {
		$("#warehousereceipt_form").submit();
	});


</script>



<script>
	$(document).ready(function () {
		$().toastmessage({
			sticky: false,
			position: 'top-right',
			inEffectDuration: 1000,
			stayTime: 3000,
			closeText: '<i class="icon ion-close-round"></i>',
		});

		//$('.check_class').hide();
		$(".purchase_items").addClass('checkek_edit');


	});


	$(".inputSwitch span").on("click", function () {

		var $this = $(this);

		$this.hide().siblings("input").val($this.text()).show();

	});

	$(".inputSwitch input").bind('blur', function () {

		var $this = $(this);

		$(this).attr('value', $(this).val());

		$this.hide().siblings("span").text($this.val()).show();

	}).hide();




	$('#additional_item').change(function () {
		var element = $(this);
		var category_id = $(this).val();
		var warehouse_id = $('#Warehousereceipt_warehousereceipt_warehouseid').val();
		var dispatch_no = $('#dispatch_no').val();
		var purchase_bill_no = $('#purchase_bill_no').val();
		var unit_id_hidden = $('#item_unit_id_hidden').val();

	});



</script>


<script>
	function getAllItems(purchase_bill_no, dispatch_no, receipt_id, transfer_type) {
		$.ajax({
			url: "<?php echo $url; ?>",
			data: { "purchase_bill_no": purchase_bill_no, "transfer_type": transfer_type, "dispatch_no": dispatch_no, "receipt_id": receipt_id },
			type: "GET",
			success: function (data) {

				$("#warehousereceipt_items").html(data);

			}
		});
	}
	function saveitems(attr) {

		$("#previous_details").hide();
		var attr = attr;
		var attr_id_arr = attr.split("_");
		var attr_id = attr_id_arr.pop();
		var receipt_id = $('input[name="warehousereceipt_id"]').val();
		var warehouse_from = $('#Warehousereceipt_warehousereceipt_warehouseid_from').val();
		var warehouse = $('#Warehousereceipt_warehousereceipt_warehouseid').val();
		var dispatch_no = $("#dispatch_no").val();
		var purchase_bill_no = $("#purchase_bill_no").val();
		var transfer_type = $("#transfer_type").val();
		var vendor = $('#vendor').val();
		var date = $(".date").val();
		var receipt_no = $('#receipt_no').val();
		var clerk = $('#clerk').val();

		var sl_no = $('#sl_no_' + attr_id).val();
		var stock_item = $('#stock_item_' + attr_id).val();
		var item_id = $('#item_type_id_' + attr_id).val();
		var rates = $('#rate_' + attr_id).val();
		var transferablequantity = $('#billedquantity_' + attr_id).val();
		var item_unit_id = $('#bill_unit_id_' + attr_id).val();
		var item_unit_base_unit = $('#item_unit_base_unit_' + attr_id).val();
		var batch = $('#itembatch_' + attr_id).val();
		var receivedquantity = $('#receivedquantity_' + attr_id).val();
		var accepted_quantity = $('#accepted_quantity_' + attr_id).val();
		var rejected_quantity = $('#rejected_quantity_' + attr_id).val();
		var remark_data = $('#remark_data_' + attr_id).val();



		var jono = $('#jono_' + attr_id).val();
		var warehouse_receipt_item_id = $('#warehouse_receipt_item_id_' + attr_id).val();
		var billitem_purchase_type = $('#billitem_purchase_type_' + attr_id).val();
		var warehouse_receipt_item_length = $('#item_length_' + attr_id).val();
		var warehouse_receipt_item_width = $('#item_width_' + attr_id).val();
		var warehouse_receipt_item_height = $('#item_height_' + attr_id).val();
		var purchase_type = $('#purchase_type_' + attr_id).val();
		var item_dimension = $('#item_dimension_' + attr_id).val();
		var item_dimension_category = $('#item_dimension_category_' + attr_id).val();
		var itemunit = $('#bill_unit_id_data' + attr_id).val();
		var itemqty = $('#billedquantity_data' + attr_id).val();
		var itemrate = $('#rate_data' + attr_id).val();


		if (purchase_bill_no != "") {
			checkrequired = purchase_bill_no;
		} else {
			if (warehouse_from != "") {
				checkrequired = dispatch_no;
			} else {
				checkrequired = '';
			}
		}
		if (warehouse == '' || date == '' || receipt_no == '' || transfer_type == '' || clerk == '' || checkrequired == '') {
			$().toastmessage('showErrorToast', "Please enter Warehouse receipt details");
		} else {
			if ((moment(date, 'DD-MM-YYYY', true).isValid())) {

				if (stock_item == '' || receivedquantity == 0 || accepted_quantity == 0) {
					$().toastmessage('showErrorToast', "Please fill item details");
				} else {
					if (warehouse_receipt_item_id == '') {
						var receipt_id = $('input[name="warehousereceipt_id"]').val();
						$.ajax({
							url: '<?php echo Yii::app()->createAbsoluteUrl('wh/warehousereceipt/receiptitem'); ?>',
							type: 'POST',
							dataType: 'json',
							data: {
								'warehousereceipt_id': receipt_id,
								'transfer_type': transfer_type,
								'Warehousereceipt[warehousereceipt_warehouseid]': warehouse,
								'Warehousereceipt[warehousereceipt_warehouseid_from]': warehouse_from,
								'sl_no': sl_no,
								'stock_item': stock_item,
								'item_id': item_id,
								'rates': rates,
								'transferablequantity': transferablequantity,
								'item_unit_id': item_unit_id,
								'item_unit_base_unit': item_unit_base_unit,
								'batch': batch,
								'receivedquantity': receivedquantity,
								'accepted_quantity': accepted_quantity,
								'rejected_quantity': rejected_quantity,
								'remark_data': remark_data,
								'jono': jono,
								'warehouse_receipt_item_id': warehouse_receipt_item_id,
								'billitem_purchase_type': billitem_purchase_type,
								'warehouse_receipt_item_length': warehouse_receipt_item_length,
								'warehouse_receipt_item_width': warehouse_receipt_item_width,
								'warehouse_receipt_item_height': warehouse_receipt_item_height,
								'purchase_type': purchase_type,
								'item_dimension': item_dimension,
								'item_dimension_category': item_dimension_category,
								'itemunit': itemunit,
								'itemqty': itemqty,
								'itemrate': itemrate,
								'dispatch_no': dispatch_no,
								'purchase_bill_no': purchase_bill_no
							},
							success: function (response) {
								if (response.response == 'success') {
									$().toastmessage('showSuccessToast', "" + response.msg + "");
									$('#warehouse_receipt_item_id_' + attr_id).val(response.warehouse_receipt_item_id);
									$('#transfer_type').attr("disabled", true);
									$('#purchase_bill_no').attr("disabled", true);
									$('#Warehousereceipt_warehousereceipt_warehouseid_from').attr("disabled", true);
									$('#Warehousereceipt_warehousereceipt_warehouseid').attr("disabled", true);
									$('#dispatch_no').attr("disabled", true);
								} else {
									$().toastmessage('showErrorToast', "" + response.msg + "");
									$("#receivedquantity_" + attr_id).val(0);
									$("#accepted_quantity_" + attr_id).val(0);
									$("#rejected_quantity_" + attr_id).val(0);
								}

							}
						});

					} else {
						var receipt_id = $('input[name="warehousereceipt_id"]').val();
						$.ajax({
							url: '<?php echo Yii::app()->createAbsoluteUrl('wh/warehousereceipt/updatereceiptitem'); ?>',
							type: 'POST',
							dataType: 'json',
							data: {
								'warehousereceipt_id': receipt_id,
								'transfer_type': transfer_type,
								'Warehousereceipt[warehousereceipt_warehouseid]': warehouse,
								'Warehousereceipt[warehousereceipt_warehouseid_from]': warehouse_from,
								'sl_no': sl_no,
								'stock_item': stock_item,
								'item_id': item_id,
								'rates': rates,
								'transferablequantity': transferablequantity,
								'item_unit_id': item_unit_id,
								'item_unit_base_unit': item_unit_base_unit,
								'batch': batch,
								'receivedquantity': receivedquantity,
								'accepted_quantity': accepted_quantity,
								'rejected_quantity': rejected_quantity,
								'remark_data': remark_data,
								'jono': jono,
								'warehouse_receipt_item_id': warehouse_receipt_item_id,
								'billitem_purchase_type': billitem_purchase_type,
								'warehouse_receipt_item_length': warehouse_receipt_item_length,
								'warehouse_receipt_item_width': warehouse_receipt_item_width,
								'warehouse_receipt_item_height': warehouse_receipt_item_height,
								'purchase_type': purchase_type,
								'item_dimension': item_dimension,
								'item_dimension_category': item_dimension_category,
								'itemunit': itemunit,
								'itemqty': itemqty,
								'itemrate': itemrate,
								'dispatch_no': dispatch_no,
								'purchase_bill_no': purchase_bill_no
							},
							success: function (response) {
								if (response.response == 'success') {
									$().toastmessage('showSuccessToast', "" + response.msg + "");
									$('#warehouse_receipt_item_id_' + attr_id).val(response.warehouse_receipt_item_id);
									$('#transfer_type').attr("disabled", true);
									$('#purchase_bill_no').attr("disabled", true);
									$('#Warehousereceipt_warehousereceipt_warehouseid_from').attr("disabled", true);
									$('#dispatch_no').attr("disabled", true);
									$('#Warehousereceipt_warehousereceipt_warehouseid').attr("disabled", true);
								} else {

									$().toastmessage('showErrorToast', "" + response.msg + "");
									$("#receivedquantity_" + attr_id).val(0);
									$("#accepted_quantity_" + attr_id).val(0);
									$("#rejected_quantity_" + attr_id).val(0);
								}

							}
						});
					}
				}

			} else {

				$(this).focus();
				$().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
				$(this).focus();
			}




		}
	}
	/* Neethu  */

	$(document).on("change", "#Warehousereceipt_warehousereceipt_warehouseid", function () {
		var element = $(this);
		var receipt_id = $("#warehousereceipt_id").val();
		var default_date = $(".date").val();
		var warehouse = $(this).val();
		var warehouse_from = $('#Warehousereceipt_warehousereceipt_warehouseid_from').val();
		var vendor = $('#vendor').val();
		var dispatch_no = $("#dispatch_no").val();
		var purchase_bill_no = $("#purchase_bill_no").val();
		var transfer_type = $("#transfer_type").val();
		var receipt_no = $('#receipt_no').val();
		var clerk = $('#clerk').val();
		if (warehouse != '' || (warehouse != '' && warehouse_from != '')) {
			$.ajax({
				method: "GET",
				async: true,
				data: { receipt_id: receipt_id, receipt_no: receipt_no, transfer_type: transfer_type, warehouse_from: warehouse_from, dispatch_no: dispatch_no, purchase_bill_no: purchase_bill_no, default_date: default_date, warehouse: warehouse, vendor: vendor, clerk: clerk },
				url: '<?php echo Yii::app()->createUrl('wh/warehousereceipt/DespatchPurchaseBilllist'); ?>',
				success: function (response) {
					var result = JSON.parse(response);
					$('select[id="dispatch_no"]').empty();
					$('select[id="dispatch_no"]').append('<option value="">Choose Dispatch No</option>');
					$.each(result["despatches"], function (key, value) {
						$('select[id="dispatch_no"]').append('<option value="' + value.id + '">' + value.value + '</option>');
					});
					$('select[id="purchase_bill_no"]').empty();
					$('select[id="purchase_bill_no"]').append('<option value="">Choose Purchase Bill No</option>');

					var bill_id = "<?php echo isset($_GET["bill_id"]) ? $_GET["bill_id"] : "" ?>";

					$.each(result["bills"], function (key, value) {
						var selected = "";
						if (bill_id != "") {
							var selected = (bill_id == value.id) ? "selected" : "";
						}
						$('select[id="purchase_bill_no"]').
							append('<option value="' + value.id + '" ' + selected + '>' + value.value + '</option>');
					});
					$("#Warehousereceipt_warehousereceipt_warehouseid_from option[value='" + warehouse + "']").remove();


				}
			});
		}

		var checkrequired = '';
		if (purchase_bill_no != "") {
			checkrequired = purchase_bill_no;
		} else {
			if (warehouse_from != "") {
				checkrequired = dispatch_no;
			} else {
				checkrequired = '';
			}
		}
		if ((moment(default_date, 'DD-MM-YYYY', true).isValid())) {
			if (warehouse == '' || default_date == '' || receipt_no == '' || transfer_type == '' || clerk == '' || checkrequired == '') {
				$.ajax({
					method: "GET",
					dataType: "json",
					url: '<?php echo Yii::app()->createUrl('wh/warehousereceipt/ajax'); ?>',
					success: function (result) {
						//  $(".date").select2("focus");
					}
				});
			} else {
				$.ajax({
					method: "GET",
					async: true,
					data: { receipt_id: receipt_id, transfer_type: transfer_type, receipt_no: receipt_no, warehouse_from: warehouse_from, dispatch_no: dispatch_no, purchase_bill_no: purchase_bill_no, default_date: default_date, warehouse: warehouse, vendor: vendor, clerk: clerk },
					dataType: "json",
					url: '<?php echo Yii::app()->createUrl('wh/warehousereceipt/createnewreceipt'); ?>',
					success: function (result) {
						if (result.response == 'success') {
							$(".purchase_items").removeClass('checkek_edit');
							$('.js-example-basic-single').select2('focus');
							$("#warehousereceipt_id").val(result.receipt_id);
							$().toastmessage('showSuccessToast', "" + result.msg + "");
							window.location.href = "index.php?r=wh/warehousereceipt/update&receiptid=" + result.receipt_id;
						} else {
							$().toastmessage('showErrorToast', "" + result.msg + "");
							element.val('');
						}

						// $(".stock_item").select2("focus");
					}
				});
			}
		} else {
			$(this).focus();
			$().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
			$(this).focus();
		}
	});

	$(document).on("change", "#vendor", function () {
		var element = $(this);
		var receipt_id = $("#warehousereceipt_id").val();
		var default_date = $(".date").val();
		var vendor = $(this).val();
		var warehouse_from = $("#Warehousereceipt_warehousereceipt_warehouseid_from").val();
		var dispatch_no = $("#dispatch_no").val();
		var purchase_bill_no = $("#purchase_bill_no").val();
		var transfer_type = $("#transfer_type").val();
		var warehouse = $('#Warehousereceipt_warehousereceipt_warehouseid').val();
		var receipt_no = $('#receipt_no').val();
		var clerk = $('#clerk').val();
		var checkrequired = '';
		if (purchase_bill_no != "") {
			checkrequired = purchase_bill_no;
		} else {
			if (warehouse_from != "") {
				checkrequired = dispatch_no;
			} else {
				checkrequired = '';
			}
		}
		if ((moment(default_date, 'DD-MM-YYYY', true).isValid())) {

			if (warehouse == '' || default_date == '' || receipt_no == '' || transfer_type == '' || clerk == '' || checkrequired == '') {
				$.ajax({
					method: "GET",
					dataType: "json",
					url: '<?php echo Yii::app()->createUrl('wh/warehousereceipt/ajax'); ?>',
					success: function (result) {
						$("#vendor").focus();
					}
				});

			} else {
				$.ajax({
					method: "GET",
					data: { receipt_id: receipt_id, transfer_type: transfer_type, receipt_no: receipt_no, warehouse_from: warehouse_from, dispatch_no: dispatch_no, purchase_bill_no: purchase_bill_no, default_date: default_date, warehouse: warehouse, vendor: vendor, clerk: clerk },
					dataType: "json",
					url: '<?php echo Yii::app()->createUrl('wh/warehousereceipt/createnewreceipt'); ?>',
					success: function (result) {
						if (result.response == 'success') {
							$(".purchase_items").removeClass('checkek_edit');
							$('.js-example-basic-single').select2('focus');
							$("#warehousereceipt_id").val(result.receipt_id);
							$().toastmessage('showSuccessToast', "" + result.msg + "");
							window.location.href = "index.php?r=wh/warehousereceipt/update&receiptid=" + result.receipt_id;
						} else {
							$().toastmessage('showErrorToast', "" + result.msg + "");
						}

						//$(".stock_item").select2("focus");
					}
				});

			}

		} else {
			$(this).focus();
			$().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
			$(this).focus();
		}

	});
	$(document).on("change", "#transfer_type", function () {
		var transfer_type = $(this).val();
		var transfer_type_val = $.trim($("#transfer_type option:selected").text());
		if (transfer_type_val == 'Despatch') {
			$('#Add_purchase_form').hide();
			$('#add_item_table').hide();
		} else {
			$('#Add_purchase_form').show();
			$('#add_item_table').show();
		}
		var element = $(this);
		var receipt_id = $("#warehousereceipt_id").val();
		var default_date = $(".date").val();
		var warehouse_from = $("#Warehousereceipt_warehousereceipt_warehouseid_from").val();
		var dispatch_no = $("#dispatch_no").val();
		var purchase_bill_no = $("#purchase_bill_no").val();
		var vendor = $("#vendor").val();
		var warehouse = $('#Warehousereceipt_warehousereceipt_warehouseid').val();
		var receipt_no = $('#receipt_no').val();
		var clerk = $('#clerk').val();
		var checkrequired = '';
		if (transfer_type == "") {
			$(".elem_block_purchase_bill_no").hide();
			$(".elem_block_dispatch_no").hide();
			$(".elem_block_warehouseid_from").hide();
			$("#warehousereceipt_items").hide();
		} else {
			if (transfer_type == 1) {
				$(".elem_block_purchase_bill_no").show();
				$(".elem_block_warehouseid_from").hide();
				$(".elem_block_dispatch_no").hide();
				$("#warehousereceipt_items").hide();
			} else {
				$(".elem_block_warehouseid_from").show();
				$(".elem_block_dispatch_no").show();
				$(".elem_block_purchase_bill_no").hide();
				$("#warehousereceipt_items").hide();
			}
		}
		if (purchase_bill_no != "") {
			checkrequired = purchase_bill_no;
		} else {
			if (warehouse_from != "") {
				checkrequired = dispatch_no;
			} else {
				checkrequired = '';
			}
		}
		if ((moment(default_date, 'DD-MM-YYYY', true).isValid())) {

			if (warehouse == '' || default_date == '' || receipt_no == '' || transfer_type == '' || clerk == '' || checkrequired == '') {
				$.ajax({
					method: "GET",
					dataType: "json",
					url: '<?php echo Yii::app()->createUrl('wh/warehousereceipt/ajax'); ?>',
					success: function (result) {
						if (transfer_type == 1) {
							$("#purchase_bill_no").focus()
						} else {
							$("#Warehousereceipt_warehousereceipt_warehouseid_from").focus();
						}
					}
				});

			} else {
				$.ajax({
					method: "GET",
					data: { receipt_id: receipt_id, transfer_type: transfer_type, receipt_no: receipt_no, warehouse_from: warehouse_from, dispatch_no: dispatch_no, purchase_bill_no: purchase_bill_no, default_date: default_date, warehouse: warehouse, vendor: vendor, clerk: clerk },
					dataType: "json",
					url: '<?php echo Yii::app()->createUrl('wh/warehousereceipt/createnewreceipt'); ?>',
					success: function (result) {
						if (result.response == 'success') {
							$(".purchase_items").removeClass('checkek_edit');
							$('.js-example-basic-single').select2('focus');
							$("#warehousereceipt_id").val(result.receipt_id);
							$().toastmessage('showSuccessToast', "" + result.msg + "");
							window.location.href = "index.php?r=wh/warehousereceipt/update&receiptid=" + result.receipt_id;
						} else {
							$().toastmessage('showErrorToast', "" + result.msg + "");
						}

						//$(".stock_item").select2("focus");
					}
				});

			}

		} else {
			$(this).focus();
			$().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
			$(this).focus();
		}

	});

	$(document).on("change", "#clerk", function () {
		var element = $(this);
		var receipt_id = $("#warehousereceipt_id").val();
		var default_date = $(".date").val();
		var clerk = $(this).val();
		var warehouse_from = $('#Warehousereceipt_warehousereceipt_warehouseid_from').val();
		var dispatch_no = $("#dispatch_no").val();
		var purchase_bill_no = $("#purchase_bill_no").val();
		var transfer_type = $("#transfer_type").val();
		var warehouse = $('#Warehousereceipt_warehousereceipt_warehouseid').val();
		var receipt_no = $('#receipt_no').val();
		var vendor = $('#vendor').val();
		var checkrequired = '';
		if (purchase_bill_no != "") {
			checkrequired = purchase_bill_no;
		} else {
			if (warehouse_from != "") {
				checkrequired = dispatch_no;
			} else {
				checkrequired = '';
			}
		}
		if ((moment(default_date, 'DD-MM-YYYY', true).isValid())) {
			if (warehouse == '' || default_date == '' || receipt_no == '' || transfer_type == '' || clerk == '' || checkrequired == '') {
				$.ajax({
					method: "GET",
					data: { purchase_id: 'test' },
					dataType: "json",
					url: '<?php echo Yii::app()->createUrl('wh/warehousereceipt/ajax'); ?>',
					success: function (result) {
						$("#receipt_no").focus();
					}
				});

			} else {
				$.ajax({
					method: "GET",
					data: { receipt_id: receipt_id, transfer_type: transfer_type, receipt_no: receipt_no, warehouse_from: warehouse_from, dispatch_no: dispatch_no, purchase_bill_no: purchase_bill_no, default_date: default_date, warehouse: warehouse, vendor: vendor, clerk: clerk },
					dataType: "json",
					url: '<?php echo Yii::app()->createUrl('wh/warehousereceipt/createnewreceipt'); ?>',
					success: function (result) {
						if (result.response == 'success') {
							$(".purchase_items").removeClass('checkek_edit');
							$('.js-example-basic-single').select2('focus');
							$("#warehousereceipt_id").val(result.receipt_id);
							$().toastmessage('showSuccessToast', "" + result.msg + "");
						} else {
							$().toastmessage('showErrorToast', "" + result.msg + "");
						}

						//$(".stock_item").select2("focus");
					}
				});

			}

		} else {
			$(this).focus();
			$().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
			$(this).focus();
		}

	});



	$(document).on("change", ".date", function () {
		var element = $(this);
		var receipt_id = $("#warehousereceipt_id").val();
		var default_date = $(this).val();
		var warehouse = $('#Warehousereceipt_warehousereceipt_warehouseid').val();
		var warehouse_from = $('#Warehousereceipt_warehousereceipt_warehouseid_from').val();
		var dispatch_no = $("#dispatch_no").val();
		var purchase_bill_no = $("#purchase_bill_no").val();
		var transfer_type = $("#transfer_type").val();
		var vendor = $('#vendor').val();
		var receipt_no = $('#receipt_no').val();
		var clerk = $('#clerk').val();
		if (purchase_bill_no != "") {
			checkrequired = purchase_bill_no;
		} else {
			if (warehouse_from != "") {
				checkrequired = dispatch_no;
			} else {
				checkrequired = '';
			}
		}
		if ((moment(default_date, 'DD-MM-YYYY', true).isValid())) {
			if (warehouse == '' || default_date == '' || receipt_no == '' || transfer_type == '' || clerk == '' || checkrequired == '') {

			} else {
				$.ajax({
					method: "GET",
					async: false,
					data: { receipt_id: receipt_id, transfer_type: transfer_type, receipt_no: receipt_no, warehouse_from: warehouse_from, dispatch_no: dispatch_no, purchase_bill_no: purchase_bill_no, default_date: default_date, warehouse: warehouse, vendor: vendor, clerk: clerk },
					dataType: "json",
					url: '<?php echo Yii::app()->createUrl('wh/warehousereceipt/createnewreceipt'); ?>',
					success: function (result) {
						if (result.response == 'success') {
							$(".purchase_items").removeClass('checkek_edit');
							$('.js-example-basic-single').select2('focus');
							$("#warehousereceipt_id").val(result.receipt_id);
							$().toastmessage('showSuccessToast', "" + result.msg + "");
							window.location.href = "index.php?r=wh/warehousereceipt/update&receiptid=" + result.receipt_id;
						} else {
							$().toastmessage('showErrorToast', "" + result.msg + "");
						}
						//$(".stock_item").select2("focus");
					}

				});

			}
		} else {
			$(this).focus();
			$().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
			$(this).focus();
		}

		$('#clerk').focus();

	});


	$(document).on("blur", "#receipt_no", function () {

		var element = $(this);
		var receipt_id = $("#warehousereceipt_id").val();
		var default_date = $(".date").val();
		var receipt_no = $(this).val();
		var through = $('#through').val();
		var warehouse_from = $('#Warehousereceipt_warehousereceipt_warehouseid_from').val();
		var dispatch_no = $("#dispatch_no").val();
		var purchase_bill_no = $("#purchase_bill_no").val();
		var transfer_type = $("#transfer_type").val();
		var warehouse = $('#Warehousereceipt_warehousereceipt_warehouseid').val();
		var clerk = $('#clerk').val();
		var vendor = $('#vendor').val();
		if (purchase_bill_no != "") {
			checkrequired = purchase_bill_no;
		} else {
			if (warehouse_from != "") {
				checkrequired = dispatch_no;
			} else {
				checkrequired = '';
			}
		}

		if ((moment(default_date, 'DD-MM-YYYY', true).isValid())) {

			if (warehouse == '' || default_date == '' || receipt_no == '' || transfer_type == '' || clerk == '' || checkrequired == '') {

			} else {

				$.ajax({
					method: "GET",
					data: { receipt_id: receipt_id, transfer_type: transfer_type, receipt_no: receipt_no, warehouse_from: warehouse_from, dispatch_no: dispatch_no, purchase_bill_no: purchase_bill_no, default_date: default_date, warehouse: warehouse, vendor: vendor, clerk: clerk },
					dataType: "json",
					url: '<?php echo Yii::app()->createUrl('wh/warehousereceipt/createnewreceipt'); ?>',
					success: function (result) {
						console.log(result);
						if (result.response == 'success') {
							$(".purchase_items").removeClass('checkek_edit');
							$('.js-example-basic-single').select2('focus');
							$("#warehousereceipt_id").val(result.receipt_id);
							$().toastmessage('showSuccessToast', "" + result.msg + "");
							if (dispatch_no == "") {
								dispatch_no = 0;
							}
							if (purchase_bill_no == "") {
								purchase_bill_no = 0;
							}

							window.location.href = "index.php?r=wh/warehousereceipt/update&receiptid=" + result.receipt_id;

						} else {
							$().toastmessage('showErrorToast', "" + result.msg + "");
						}
					}
				});

			}

		} else {
			$(this).focus();
			$().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
			$(this).focus();
		}

	});
	$("#purchase_bill_no").change(function () {

		var receiptno = $("#receipt_no").val();
		var purchase_bill_no = $("#purchase_bill_no").val();
		//alert(purchase_bill_no);
		$.ajax({
			url: "<?php echo Yii::app()->createUrl('wh/warehousereceipt/getDynamicReceipt'); ?>",
			data: {
				"purchase_bill_no": purchase_bill_no,
			},
			type: "POST",
			dataType: "json",
			success: function (data) {
				if (data.status == 1) {
					$("#receipt_no").val(data.receipt_no);
					$("#receipt_no").attr('readonly', 'readonly');
				} else {
					$("#receipt_no").val(receiptno);
				}

			}
		});
	});
	$("#dispatch_no").change(function () {

		var receiptno = $("#receipt_no").val();
		var dispatch_no = $("#dispatch_no").val();
		var warehouse_from = $('#Warehousereceipt_warehousereceipt_warehouseid_from').val();
		//alert(warehouse_from);
		$.ajax({
			url: "<?php echo Yii::app()->createUrl('wh/warehousereceipt/getDynamicReceiptBasedOnDispatch'); ?>",
			data: {
				"dispatch_no": dispatch_no,
				"warehouse_from": warehouse_from,
			},
			type: "POST",
			dataType: "json",
			success: function (data) {
				if (data.status == 1) {
					$("#receipt_no").val(data.receipt_no);
					$("#receipt_no").attr('readonly', 'readonly');
				} else {
					$("#receipt_no").val(receiptno);
				}

			}
		});
	});


	$(document).on("change", "#Warehousereceipt_warehousereceipt_warehouseid_from", function () {
		var element = $(this);
		var receipt_id = $("#warehousereceipt_id").val();
		var default_date = $(".date").val();
		var warehouse_from = $(this).val();
		var through = $('#through').val();
		var receipt_no = $("#receipt_no").val();
		var dispatch_no = $("#dispatch_no").val();
		$('#purchase_bill_no').prop('selectedIndex', 0);
		var transfer_type = $("#transfer_type").val();
		var purchase_bill_no = $("#purchase_bill_no").val();
		var warehouse = $('#Warehousereceipt_warehousereceipt_warehouseid').val();
		var clerk = $('#clerk').val();
		var vendor = $('#vendor').val();
		if (purchase_bill_no != "") {
			checkrequired = purchase_bill_no;
		} else {
			if (warehouse_from != "") {
				checkrequired = dispatch_no;
			} else {
				checkrequired = '';
			}
		}

		if (warehouse != '' || (warehouse != '' && warehouse_from != '')) {
			$.ajax({
				method: "GET",
				async: true,
				data: { receipt_id: receipt_id, transfer_type: transfer_type, receipt_no: receipt_no, warehouse_from: warehouse_from, dispatch_no: dispatch_no, purchase_bill_no: purchase_bill_no, default_date: default_date, warehouse: warehouse, vendor: vendor, clerk: clerk },
				url: '<?php echo Yii::app()->createUrl('wh/warehousereceipt/DespatchPurchaseBilllist'); ?>',
				success: function (response) {
					var result = JSON.parse(response);
					$('select[id="dispatch_no"]').empty();
					$('select[id="dispatch_no"]').append('<option value="">Choose Dispatch No</option>');
					$.each(result["despatches"], function (key, value) {
						$('select[id="dispatch_no"]').append('<option value="' + value.id + '">' + value.value + '</option>');
					});
					$('select[id="purchase_bill_no"]').empty();
					$('select[id="purchase_bill_no"]').append('<option value="">Choose Purchase Bill No</option>');
					$.each(result["bills"], function (key, value) {
						$('select[id="purchase_bill_no"]').append('<option value="' + value.id + '">' + value.value + '</option>');
					});

				}
			});
		}

		if ((moment(default_date, 'DD-MM-YYYY', true).isValid())) {
			if (warehouse == '' || default_date == '' || receipt_no == '' || transfer_type == '' || clerk == '' || checkrequired == '') {

			} else {
				$.ajax({
					method: "GET",
					data: { receipt_id: receipt_id, transfer_type: transfer_type, receipt_no: receipt_no, warehouse_from: warehouse_from, dispatch_no: dispatch_no, purchase_bill_no: purchase_bill_no, default_date: default_date, warehouse: warehouse, vendor: vendor, clerk: clerk },
					dataType: "json",
					url: '<?php echo Yii::app()->createUrl('wh/warehousereceipt/createnewreceipt'); ?>',
					success: function (result) {
						if (result.response == 'success') {
							$(".purchase_items").removeClass('checkek_edit');
							$('.js-example-basic-single').select2('focus');
							$("#warehousereceipt_id").val(result.receipt_id);
							$().toastmessage('showSuccessToast', "" + result.msg + "");
							getAllItems(purchase_bill_no, dispatch_no, receipt_id, transfer_type);
						} else {
							$().toastmessage('showErrorToast', "" + result.msg + "");
						}

						$("#dispatch_no").select2("focus");
					}
				});

			}

		} else {
			$(this).focus();
			$().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
			$(this).focus();
		}

	});

	$(document).on("change", "#dispatch_no", function () {
		$("#warehousereceipt_items").show();
		var element = $(this);
		var receipt_id = $("#warehousereceipt_id").val();
		var default_date = $(".date").val();
		var receipt_no = $("#receipt_no").val();
		var through = $('#through').val();
		var transfer_type = $("#transfer_type").val();
		var warehouse_from = $("#Warehousereceipt_warehousereceipt_warehouseid_from").val();
		var dispatch_no = $(this).val();
		$('#purchase_bill_no').prop('selectedIndex', 0);
		var purchase_bill_no = $("#purchase_bill_no").val();
		var warehouse = $('#Warehousereceipt_warehousereceipt_warehouseid').val();
		var clerk = $('#clerk').val();
		var vendor = $('#vendor').val();
		if (purchase_bill_no != "") {
			checkrequired = purchase_bill_no;
		} else {
			if (warehouse_from != "") {
				checkrequired = dispatch_no;
			} else {
				checkrequired = '';
			}
		}
		if (warehouse != '' || (warehouse != '' && warehouse_from != '')) {
			$.ajax({
				method: "GET",
				async: true,
				data: { receipt_id: receipt_id, transfer_type: transfer_type, receipt_no: receipt_no, warehouse_from: warehouse_from, dispatch_no: dispatch_no, purchase_bill_no: purchase_bill_no, default_date: default_date, warehouse: warehouse, vendor: vendor, clerk: clerk },
				url: '<?php echo Yii::app()->createUrl('wh/warehousereceipt/DespatchPurchaseBilllist'); ?>',
				success: function (response) {
					var result = JSON.parse(response);
					$('select[id="purchase_bill_no"]').empty();
					$('select[id="purchase_bill_no"]').append('<option value="">Choose Purchase Bill No</option>');
					$.each(result["bills"], function (key, value) {
						$('select[id="purchase_bill_no"]').append('<option value="' + value.id + '">' + value.value + '</option>');
					});

				}
			});
		}

		if ((moment(default_date, 'DD-MM-YYYY', true).isValid())) {
			if (warehouse == '' || default_date == '' || receipt_no == '' || transfer_type == '' || clerk == '' || checkrequired == '') {
			} else {
				$.ajax({
					method: "GET",
					data: { receipt_id: receipt_id, transfer_type: transfer_type, receipt_no: receipt_no, warehouse_from: warehouse_from, dispatch_no: dispatch_no, purchase_bill_no: purchase_bill_no, default_date: default_date, warehouse: warehouse, vendor: vendor, clerk: clerk },
					dataType: "json",
					url: '<?php echo Yii::app()->createUrl('wh/warehousereceipt/createnewreceipt'); ?>',
					success: function (result) {
						if (result.response == 'success') {
							$(".purchase_items").removeClass('checkek_edit');
							$('.js-example-basic-single').select2('focus');
							$("#warehousereceipt_id").val(result.receipt_id);
							$().toastmessage('showSuccessToast', "" + result.msg + "");
							if (dispatch_no == "") {
								dispatch_no = 0;
							} else {
								getAllItems(purchase_bill_no, dispatch_no, receipt_id, transfer_type);
							}
						} else {
							$().toastmessage('showErrorToast', "" + result.msg + "");
						}

						$("#vendor").focus();

					}
				});

			}

		} else {
			$(this).focus();
			$().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
			$(this).focus();
		}

	});


	$(document).on("change", "#purchase_bill_no", function () {
		$("#warehousereceipt_items").show();
		var element = $(this);
		var receipt_id = $("#warehousereceipt_id").val();
		var default_date = $(".date").val();
		var receipt_no = $("#receipt_no").val();
		var through = $('#through').val();
		$('#dispatch_no').prop('selectedIndex', 0);
		$('#Warehousereceipt_warehousereceipt_warehouseid_from').prop('selectedIndex', 0);
		var warehouse_from = $("#Warehousereceipt_warehousereceipt_warehouseid_from").val();
		var dispatch_no = $("#dispatch_no").val();
		var transfer_type = $("#transfer_type").val();
		var purchase_bill_no = $(this).val();
		var warehouse = $('#Warehousereceipt_warehousereceipt_warehouseid').val();
		var clerk = $('#clerk').val();
		var vendor = $('#vendor').val();
		$('#Warehousereceipt_warehousereceipt_warehouseid_from').val("");
		if (purchase_bill_no != "") {
			checkrequired = purchase_bill_no;
		} else {
			if (warehouse_from != "") {
				checkrequired = dispatch_no;
			} else {
				checkrequired = '';
			}
		}
		if (warehouse != '' || (warehouse != '' && warehouse_from != '')) {
			$.ajax({
				method: "GET",
				async: true,
				data: { receipt_id: receipt_id, transfer_type: transfer_type, receipt_no: receipt_no, warehouse_from: warehouse_from, dispatch_no: dispatch_no, purchase_bill_no: purchase_bill_no, default_date: default_date, warehouse: warehouse, vendor: vendor, clerk: clerk },
				url: '<?php echo Yii::app()->createUrl('wh/warehousereceipt/DespatchPurchaseBilllist'); ?>',
				success: function (response) {
					var result = JSON.parse(response);
					$('select[id="dispatch_no"]').empty();
					$('select[id="dispatch_no"]').append('<option value="">Choose Dispatch No</option>');
					$.each(result["despatches"], function (key, value) {
						$('select[id="dispatch_no"]').append('<option value="' + value.id + '">' + value.value + '</option>');
					});

				}
			});
			$('#vendor').focus();
		}

		if ((moment(default_date, 'DD-MM-YYYY', true).isValid())) {
			if (warehouse == '' || default_date == '' || receipt_no == '' || transfer_type == '' || clerk == '' || checkrequired == '') {

			} else {
				$.ajax({
					method: "GET",
					data: { receipt_id: receipt_id, transfer_type: transfer_type, receipt_no: receipt_no, warehouse_from: warehouse_from, dispatch_no: dispatch_no, purchase_bill_no: purchase_bill_no, default_date: default_date, warehouse: warehouse, vendor: vendor, clerk: clerk },
					dataType: "json",
					url: '<?php echo Yii::app()->createUrl('wh/warehousereceipt/createnewreceipt'); ?>',
					success: function (result) {
						if (result.response == 'success') {
							$(".purchase_items").removeClass('checkek_edit');
							$('.js-example-basic-single').select2('focus');
							$("#warehousereceipt_id").val(result.receipt_id);
							$().toastmessage('showSuccessToast', "" + result.msg + "");
							if (purchase_bill_no == "") {
								purchase_bill_no = 0;
							} else {
								getAllItems(purchase_bill_no, dispatch_no, receipt_id, transfer_type);
							}
						} else {
							$().toastmessage('showErrorToast', "" + result.msg + "");
						}

					}
				});

			}

		} else {
			$(this).focus();
			$().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
			$(this).focus();
		}




	});



	$("#Warehousereceipt_warehousereceipt_warehouseid").change(function () {
		$.ajax({
			method: "GET",
			dataType: "json",
			url: '<?php echo Yii::app()->createUrl('wh/warehousereceipt/ajax'); ?>',
			success: function (result) {
				$("#datepicker").focus();
			}
		});
	});

	$("#clerk").change(function () {
		$.ajax({
			method: "GET",
			dataType: "json",
			url: '<?php echo Yii::app()->createUrl('wh/warehousereceipt/ajax'); ?>',
			success: function (result) {
				$("#receipt_no").focus();
			}
		});
	});
	$("#Warehousereceipt_warehousereceipt_warehouseid_from").change(function () {
		$.ajax({
			method: "GET",
			dataType: "json",
			url: '<?php echo Yii::app()->createUrl('wh/warehousereceipt/ajax'); ?>',
			success: function (result) {
				$("#dispatch_no").focus();
			}
		});
	});
	$("#dispatch_no").change(function () {
		$('#purchase_bill_no').prop('selectedIndex', 0);
		$.ajax({
			method: "GET",
			dataType: "json",
			url: '<?php echo Yii::app()->createUrl('wh/warehousereceipt/ajax'); ?>',
			success: function (result) {
				$("#dispatch_no").focus();
			}
		});
	});
	$("#purchase_bill_no").change(function () {
		$('#dispatch_no').prop('selectedIndex', 0);
		$('#Warehousereceipt_warehousereceipt_warehouseid_from').prop('selectedIndex', 0);
		$.ajax({
			method: "GET",
			dataType: "json",
			url: '<?php echo Yii::app()->createUrl('wh/warehousereceipt/ajax'); ?>',
			success: function (result) {
				$('#vendor').focus();
			}
		});
	});



	$("#vendor").keyup(function (event) {
		if (event.keyCode == 13) {
			$("#vendor").click();
		}
	});

	var sl_no = 1;
	var howMany = 0;
	$(document).on('click', '.item_save', function () {
		$("#previous_details").hide();
		var element = $(this);
		var item_id = $(this).attr('id');
		if (item_id == 0) {

			// add
			var warehouse_from = $('#Warehousereceipt_warehousereceipt_warehouseid_from').val();
			var warehouse = $('#Warehousereceipt_warehousereceipt_warehouseid').val();
			var dispatch_no = $("#dispatch_no").val();
			var purchase_bill_no = $("#purchase_bill_no").val();
			var transfer_type = $("#transfer_type").val();
			var vendor = $('#vendor').val();
			var date = $(".date").val();
			var receipt_no = $('#receipt_no').val();
			var clerk = $('#clerk').val();
			var transfer_type_item_id = null;
			var item_unit_base_unit = $("#additional_item_base_unit").val();
			var stock_item = $('#additional_item').val();
			var batch = $('#bibatch').val();

			var baseqty = $('#basequantity').val();
			var baseunit = $('#item_unit_additional').val();
			var baserate = $('#baserate').val();

			var itemunit = $('#item_unit_additional_purchase').val();
			var itemqty = $('#quantity').val();
			var itemrate = $('#rate').val();

			if (baseqty != '' && baserate != '' && baseunit != '') {
				if (baseunit != itemunit) {
					var quantity = baseqty;
					var unit = baseunit;
					var rates = baserate;
				} else {
					var quantity = itemqty;
					var unit = itemunit;
					var rates = itemrate;
				}
			} else {
				var quantity = itemqty;
				var unit = itemunit;
				var rates = itemrate;
			}

			var amount = $('#amount').val();
			var jono = $("#jono").val();
			var rejected_quantity = 0;
			var rowCount = $('.table .addrow tr').length;
			if (purchase_bill_no != "") {
				checkrequired = purchase_bill_no;
			} else {
				if (warehouse_from != "") {
					checkrequired = dispatch_no;
				} else {
					checkrequired = '';
				}
			}

			if (warehouse == '' || date == '' || receipt_no == '' || transfer_type == '' || clerk == '' || checkrequired == '') {
				$().toastmessage('showErrorToast', "Please enter Warehouse receipt details");
			} else {
				if ((moment(date, 'DD-MM-YYYY', true).isValid())) {

					if (stock_item == '' || quantity == 0 || rate == '' || amount == '') {
						$().toastmessage('showErrorToast', "Please fill item details");
					} else {
						howMany += 1;
						if (howMany == 1) {
							var receipt_id = $('input[name="warehousereceipt_id"]').val();
							var data = { 'sl_no': rowCount, 'quantity': quantity, 'batch': batch, 'rate': rate, 'amount': amount, 'unit': unit, 'stock_item': stock_item, 'receipt_id': receipt_id, 'jono': jono, 'transfer_type': transfer_type, };
							$.ajax({
								url: '<?php echo Yii::app()->createAbsoluteUrl('wh/warehousereceipt/receiptitem'); ?>',
								type: 'POST',
								dataType: 'json',
								data: {
									'warehousereceipt_id': receipt_id,
									'transfer_type': transfer_type,
									'Warehousereceipt[warehousereceipt_warehouseid]': warehouse,
									'Warehousereceipt[warehousereceipt_warehouseid_from]': warehouse_from,
									'sl_no': sl_no,
									'stock_item': stock_item,
									'item_id': transfer_type_item_id,
									'rates': rates,
									'transferablequantity': quantity,
									'item_unit_id': unit,
									'item_unit_base_unit': item_unit_base_unit,
									'batch': batch,
									'receivedquantity': quantity,
									'accepted_quantity': quantity,
									'rejected_quantity': rejected_quantity,
									'jono': jono,
									'warehouse_receipt_item_id': item_id,
									'itemqty': itemqty,
									'itemunit': itemunit,
									'itemrate': itemrate,
									'dispatch_no': dispatch_no,
									'purchase_bill_no': purchase_bill_no
								},
								success: function (response) {
									if (response.response == 'success') {
										$().toastmessage('showSuccessToast', "" + response.msg + "");
										$('.addrow').html(response.html);
									} else {
										$().toastmessage('showErrorToast', "" + response.msg + "");
									}
									howMany = 0;

									$('#additional_item').val('').trigger('change');
									$('#rate').val('0');
									$('#amount').val('0');
									var quantity = $('#quantity').val('0');
									var accepted_quantity = $('#accepted_quantity').val('');
									var rejected_quantity = $('#rejected_quantity').val('');
									$("#jono").val('');
									var unit = $('#item_unit').val('');
									$('#batch').val('');
									$('#additional_item').select2('focus');
									$('#item_baseunit_data').hide();
									$('#item_conversion_data').hide();
									$('.base-data').hide();

								}
							});

							$('.js-example-basic-single').select2('focus');
						}
					}

				} else {

					$(this).focus();
					$().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
					$(this).focus();
				}
			}
		} else {
			// update


			var warehouse_from = $('#Warehousereceipt_warehousereceipt_warehouseid_from').val();
			var warehouse = $('#Warehousereceipt_warehousereceipt_warehouseid').val();
			var dispatch_no = $("#dispatch_no").val();
			var purchase_bill_no = $("#purchase_bill_no").val();
			var transfer_type = $("#transfer_type").val();
			var vendor = $('#vendor').val();
			var date = $(".date").val();
			var receipt_no = $('#receipt_no').val();
			var transfer_type_item_id = null;
			var clerk = $('#clerk').val();
			var item_unit_base_unit = $("#additional_item_base_unit").val();
			var stock_item = $('#additional_item').val();
			var batch = $.trim($('#bibatch').val());



			var baseqty = $('#basequantity').val();
			var baseunit = $('#item_unit_additional').val();
			var baserate = $('#baserate').val();
			var itemunit = $('#item_unit_additional_purchase').val();
			var itemqty = $('#quantity').val();
			var itemrate = $('#rate').val();
			if (baseqty != '' && baserate != '' && baseunit != '') {
				if (baseunit != itemunit) {
					var quantity = baseqty;
					var unit = baseunit;
					var rates = baserate;
				} else {
					var quantity = itemqty;
					var unit = itemunit;
					var rates = itemrate;

				}
			} else {
				var quantity = itemqty;
				var unit = itemunit;
				var rates = itemrate;
			}

			var amount = $('#amount').val();
			var jono = $("#jono").val();
			var rejected_quantity = 0;
			if (purchase_bill_no != "") {
				checkrequired = purchase_bill_no;
			} else {
				if (warehouse_from != "") {
					checkrequired = dispatch_no;
				} else {
					checkrequired = '';
				}
			}
			if (warehouse == '' || date == '' || receipt_no == '' || transfer_type == '' || clerk == '' || checkrequired == '') {
				$().toastmessage('showErrorToast', "Please enter Warehouse receipt details");
			} else {

				if ((moment(date, 'DD-MM-YYYY', true).isValid())) {
					if (stock_item == '' || quantity == 0 || rate == '' || amount == '') {
						$().toastmessage('showErrorToast', "Please fill item details");
					} else {
						howMany += 1;
						if (howMany == 1) {
							var receipt_id = $('input[name="warehousereceipt_id"]').val();
							//var data = {'sl_no':sl_no,'quantity':quantity,'batch':batch,'rate':rate,'amount':amount,'unit':unit,'stock_item':stock_item,'receipt_id' : receipt_id,'jono':jono,};
							$.ajax({
								url: '<?php echo Yii::app()->createAbsoluteUrl('wh/warehousereceipt/updatereceiptitem'); ?>',
								type: 'POST',
								dataType: 'json',
								data: {
									'warehousereceipt_id': receipt_id,
									'transfer_type': transfer_type,
									'Warehousereceipt[warehousereceipt_warehouseid]': warehouse,
									'Warehousereceipt[warehousereceipt_warehouseid_from]': warehouse_from,
									'sl_no': sl_no,
									'stock_item': stock_item,
									'item_id': transfer_type_item_id,
									'rates': rates,
									'transferablequantity': quantity,
									'item_unit_id': unit,
									'item_unit_base_unit': item_unit_base_unit,
									'batch': batch,
									'receivedquantity': quantity,
									'accepted_quantity': quantity,
									'rejected_quantity': rejected_quantity,
									'jono': jono,
									'warehouse_receipt_item_id': item_id,
									'itemqty': itemqty,
									'itemunit': itemunit,
									'itemrate': itemrate,
									'dispatch_no': dispatch_no,
									'purchase_bill_no': purchase_bill_no
								},
								success: function (response) {
									if (response.response == 'success') {
										$('#final_amount').val(response.final_amount);
										$().toastmessage('showSuccessToast', "" + response.msg + "");
										$('#grand_total').text(response.final_amount);
										$('.addrow').html(response.html);

									} else {
										$().toastmessage('showErrorToast', "" + response.msg + "");
									}
									howMany = 0;

									$('#additional_item').val('').trigger('change');
									$('#rate').val('0');
									$('#amount').val('0');
									var quantity = $('#quantity').val('0');
									var accepted_quantity = $('#accepted_quantity').val('');
									var rejected_quantity = $('#rejected_quantity').val('');
									var unit = $('#item_unit').val('');
									$('#batch').val('');
									$(".item_save").attr('value', 'Save');
									$(".item_save").attr('id', 0);
									$('#additional_item').select2('focus');
									$("#jono").val('');
									$('#item_baseunit_data').hide();
									$('#item_conversion_data').hide();
									$('.base-data').hide();
								}
							});

							$('.js-example-basic-single').select2('focus');
						}
					}

				} else {
					$(this).focus();
					$().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
					$(this).focus();
				}
			}
		}
	});



	$(document).on("blur", ".accepted_quantity", function () {
		var accepted_quantity = $(this).val();
		var attr = $(this).attr('id');
		var attr_id_arr = attr.split("_");
		var attr_id = attr_id_arr['2'];
		var billedquantity = $("#billedquantity_" + attr_id).val();
		var rejectedquantity = $("#rejected_quantity_" + attr_id).val();
		var quantity = $("#receivedquantity_" + attr_id).val();
		var check_Quantity = parseFloat(quantity) - parseFloat(accepted_quantity);
		if (accepted_quantity != "") {
			if (check_Quantity >= 0) {
				rejectedquantity = check_Quantity;
				$("#rejected_quantity_" + attr_id).val(rejectedquantity);
				saveitems(attr);
				// $("#warehousereceipt_form").submit();
			} else {
				$("#accepted_quantity_" + attr_id).val(quantity);
				$("#rejected_quantity_" + attr_id).val(0);
				$().toastmessage('showErrorToast', "Accepted Quantity must be less than the Received Quantity");
			}
		} else {
			$("#accepted_quantity_" + attr_id).val(quantity);
			$("#rejected_quantity_" + attr_id).val(0);
		}


	});
	$(document).on("change", ".receivedquantity", function () {
		var quantity = $(this).val();
		var attr = $(this).attr('id');
		var attr_id_arr = attr.split("_");
		var attr_id = attr_id_arr['1'];
		var billedquantity = $("#billedquantity_" + attr_id).val();
		var accepted_quantity = $("#accepted_quantity_" + attr_id).val();
		var rejectedquantity = $("#rejected_quantity_" + attr_id).val();
		var check_Quantity = parseFloat(billedquantity) - parseFloat(quantity);
		if (quantity != "") {
			if (check_Quantity >= 0) {
				$('#buttonsubmit').prop('disabled', false);
				$("#accepted_quantity_" + attr_id).val(quantity);
				accepted_quantity = $("#accepted_quantity_" + attr_id).val();
				rejected_quantity = quantity - accepted_quantity;
				$("#rejected_quantity_" + attr_id).val(rejected_quantity);
				saveitems(attr);
				// $("#warehousereceipt_form").submit();
			} else {
				$("#accepted_quantity_" + attr_id).val(0);
				$("#receivedquantity_" + attr_id).val(0);
				$("#rejected_quantity_" + attr_id).val(0);
				$().toastmessage('showErrorToast', "Received Quantity Entered is more than the available quantity");
			}
		} else {
			$("#accepted_quantity_" + attr_id).val(0);
			$("#receivedquantity_" + attr_id).val(0);
			$("#rejected_quantity_" + attr_id).val(0);
		}

	});

	$(document).on("blur", ".rejected_quantity", function () {
		var rejectedquantity = $(this).val();
		var attr = $(this).attr('id');
		var attr_id_arr = attr.split("_");
		var attr_id = attr_id_arr['2'];

		var billedquantity = $("#billedquantity_" + attr_id).val();
		var accepted_quantity = $("#accepted_quantity_" + attr_id).val();
		var rejectedquantity = $("#rejected_quantity_" + attr_id).val();
		var quantity = $("#receivedquantity_" + attr_id).val();
		var check_Quantity = parseFloat(quantity) - parseFloat(rejectedquantity);
		if (rejectedquantity != "") {
			if (check_Quantity >= 0) {
				accepted_quantity = check_Quantity;
				$("#accepted_quantity_" + attr_id).val(accepted_quantity);
				saveitems(attr);
				// $("#warehousereceipt_form").submit();
			} else {
				$("#accepted_quantity_" + attr_id).val(quantity);
				$("#rejected_quantity_" + attr_id).val(0);
				$().toastmessage('showErrorToast', "Rejected Quantity must be less than the Received Quantity");
			}
		} else {
			$("#accepted_quantity_" + attr_id).val(quantity);
			$("#rejected_quantity_" + attr_id).val(0);
		}

	});


	$(document).on("blur", ".remark_data", function () {
		var attr = $(this).attr('id');
		saveitems(attr);
	});



	$(document).on("blur", "#quantity", function () {
		var quantity = $("#quantity").val();
		var rate = $("#rate").val();
		var amount = quantity * rate;
		$("#amount").val(amount);

	});
	$(document).on("blur", "#rate", function () {
		var quantity = $("#quantity").val();
		var rate = $("#rate").val();
		var amount = quantity * rate;
		$("#amount").val(amount);

	});
	$(document).on("blur", "#amount", function () {
		var quantity = $("#quantity").val();
		var rate = $("#rate").val();
		var amount = quantity * rate;
		$("#amount").val(amount);

	});

	$(document).on('click', '.edit_item', function (e) {
		e.preventDefault();
		var item_id = $(this).attr('id');
		var $tds = $(this).closest('tr').find('td');
		var sl_no = $tds.eq(0).text();
		var description = $tds.eq(1).text();
		var batch = $tds.eq(2).text();
		var unit = $tds.eq(3).text();
		unit = $.trim(unit);
		var quantity = $tds.eq(4).text();
		var rate = $tds.eq(5).text();
		var amount = $tds.eq(9).text();
		var basequantity = $tds.eq(6).text();
		var baserate = $tds.eq(8).text();

		//var jono = $tds.eq(7).text();


		if (isNaN(parseFloat(quantity))) {
			quantity = 0;
		} else {
			quantity = parseFloat(quantity);
		}
		if (isNaN(parseFloat(rate))) {
			rate = 0;
		} else {
			rate = parseFloat(rate);
		}

		$abc = $(this).closest('tr').find('.item_description').attr('id');
		var des_id = $(this).closest('tr').find('.item_description').attr('id');
		var unit_name = $.trim(unit);
		var unit_id = $('#item_unit_id' + sl_no).val();
		<?php $abc; ?>
		$('#additional_item').val(des_id).trigger('change');
		if (des_id != '') {

			$.ajax({
				url: '<?php echo Yii::app()->createAbsoluteUrl('wh/warehousereceipt/getUnits'); ?>',
				type: 'GET',
				dataType: 'json',
				data: { data: des_id },
				success: function (result) {

					if (result.status == 1) {
						$("#additional_item_base_unit").val(unit);
						$("#item_unit_additional_span").html(unit);
						$("#item_unit_additional_purchase").val(unit);
						$("#item_unit_additional").val(result.base_unit);
						$('select[id="item_unit_additional_purchase"]').empty();


						if (result.unit == result.base_unit) {

							$('select[id="item_unit_additional_purchase"]').append('<option value="' + result.unit + '" selected>' + result.units + '</option>');
						} else {

							$.each(result["unit"], function (key, value) {

								if (unit == value.value) {
									var selected = 'selected';
								} else {
									var selected = '';
								}
								$('select[id="item_unit_additional_purchase"]').append('<option value="' + value.id + '" ' + selected + '>' + value.value + '</option>');
							});

						}
					}
					var purchase_unit = $("#item_unit_additional_purchase option:selected").text();
					if (result.base_unit == purchase_unit) {
						$('.base-data').hide();
					} else {
						$('.base-data').show();
					}
					$('#quantity').focus();
				}
			});
		}

		$('#bibatch').val(batch);
		$('#item_unit_additional_purchase').val(unit);
		$('.additional_rate').val(rate);
		$('#item_unit_additional_span').html(unit);
		$('#amount').val(amount);
		//$("#jono_additional").val(jono);
		$('#quantity').val(quantity);
		$('#basequantity').val(basequantity);
		$('#baserate').val(baserate);

		$('.js-example-basic-single').select2('focus');
		$('#item_unit').val(unit);
		$('#item_unit_id_hidden').val(unit);
		$('#bibatch').attr('readonly', true);
		$(".item_save").attr('value', 'Update');
		$(".item_save").attr('id', item_id);
	});

	$('.item_save').keypress(function (e) {
		if (e.keyCode == 13) {
			$('.item_save').click();
		}
	});


	function filterDigits(eventInstance) {
		eventInstance = eventInstance || window.event;
		key = eventInstance.keyCode || eventInstance.which;
		if ((47 < key) && (key < 58) || key == 8) {
			return true;
		} else {
			if (eventInstance.preventDefault)
				eventInstance.preventDefault();
			eventInstance.returnValue = false;
			return false;
		} //if
	}


	// approve items

	$(document).on('click', '.approve_item', function (e) {
		e.preventDefault();
		var element = $(this);
		var item_id = $(this).attr('id');
		$.ajax({
			url: '<?php echo Yii::app()->createAbsoluteUrl('purchase/permissionapprove'); ?>',
			type: 'POST',
			dataType: 'json',
			data: { item_id: item_id },
			success: function (response) {
				if (response.response == 'success') {
					$(".approveoption_" + item_id).hide();
					element.closest('tr').find('.rate_highlight').removeClass('rate_highlight');
					$().toastmessage('showSuccessToast', "" + response.msg + "");
				} else if (response.response == 'warning') {
					$(".approveoption_" + item_id).hide();
					element.closest('tr').find('.rate_highlight').removeClass('rate_highlight');
					$().toastmessage('showWarningToast', "" + response.msg + "");
				} else {
					$().toastmessage('showErrorToast', "" + response.msg + "");
				}
			}
		});
	});

	$(document).on('mouseover', '.rate_highlight', function (e) {
		var item_id = $(this).attr('id');
		$.ajax({
			url: '<?php echo Yii::app()->createAbsoluteUrl('purchase/previousratedetails'); ?>',
			type: 'GET',
			dataType: 'json',
			data: { item_id: item_id },
			success: function (result) {
				if (result.status == 1) {
					$('#previous_details').html(result.html);
					$("#previous_details").show();
					$("#pre_fixtable2").tableHeadFixer();
				} else {
					$('#previous_details').html(result.html);
					$("#previous_details").hide();
				}
			}
		})
	})
	$(document).on('mouseout', '.rate_highlight', function (e) {
		$("#previous_details").hide();
	});

	$("#purchaseno").keyup(function () {
		if (this.value.match(/[^a-zA-Z0-9.:]/g)) {
			this.value = this.value.replace(/[^a-zA-Z0-9.:\-/]/g, '');
		}
	});

	$(document).on('click', '.getprevious', function () {
		var id = $(this).attr('data-id');
		var res = id.split(",");
		var amount = parseFloat(res[4]);
		$('#description').val(res[0]).trigger('change.select2');
		$('#quantity').val(res[1]);
		$('#item_unit').html(res[2]);
		$('#rate').val(res[3]);
		$('#item_amount').text(amount.toFixed(2));
		var total = (res[1] * res[3]);
		if (isNaN(total)) total = 0;
		$('#previousvalue').text(total.toFixed(2));
	})

	$(document).on('click', '.removebtn', function (e) {
		e.preventDefault();
		element = $(this);
		var item_id = $(this).attr('id');
		var answer = confirm("Are you sure you want to delete?");
		if (answer) {

			var receipt_id = $("#warehousereceipt_id").val();
			var data = { 'receipt_id': receipt_id, 'item_id': item_id };
			$.ajax({
				method: "GET",
				async: false,
				data: { data: data },
				dataType: "json",
				url: '<?php echo Yii::app()->createUrl('wh/warehousereceipt/removeitem'); ?>',
				success: function (result) {
					if (result.response == 'success') {
						$('.addrow').html(result.html);
						$().toastmessage('showSuccessToast', "" + result.msg + "");
					} else {
						$().toastmessage('showErrorToast', "" + result.msg + "");
					}
					$('#additional_item').val('');
					var quantity = $('#quantity').val('0');
					var accepted_quantity = $('#accepted_quantity').val('');
					var rejected_quantity = $('#rejected_quantity').val('');
					var unit = $('#item_unit').val('');
					$('select[id="item_unit"]').empty();
					$('select[id="item_unit"]').append('<option value="" selected>Unit</option>');
					$('select[id="batch"]').empty();
					$('select[id="batch"]').append('<option value="" selected>Please choose Batch</option>');
					$(".item_save").attr('value', 'Save');
					$(".item_save").attr('id', 0);
					$('#additional_item').select2('focus');
					$('#jono').val('');
				}
			});

		}
		else {

			return false;
		}
	});

</script>
<script>
	/*
	(function() {
	   var mainTable = document.getElementById("main-table");
	   var tableHeight = mainTable.offsetHeight;
	   if (tableHeight > 380) {
				var fauxTable = document.getElementById("faux-table");
			document.getElementById("table-wrap").className += ' ' + 'fixedON';
				var clonedElement = mainTable.cloneNode(true);
				clonedElement.id = "";
				fauxTable.appendChild(clonedElement);
	   }
	})();
	*/
</script>

<script>
	$(".popover-test").popover({
		html: true,
		content: function () {
			//return $('#popover-content').html();
			return $(this).next('.popover-content').html();
		}
	});
	$('[data-toggle=popover]').on('click', function (e) {
		$('[data-toggle=popover]').not(this).popover('hide');
	});
	$('body').on('hidden.bs.popover', function (e) {
		$(e.target).data("bs.popover").inState.click = false;
	});
	$('body').on('click', function (e) {
		$('[data-toggle=popover]').each(function () {
			// hide any open popovers when the anywhere else in the body is clicked
			if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
				$(this).popover('hide');
			}
		});
	});

	$(document).ajaxComplete(function () {
		$(".popover-test").popover({
			html: true,
			content: function () {
				return $(this).next('.popover-content').html();
			}
		});
	});
</script>