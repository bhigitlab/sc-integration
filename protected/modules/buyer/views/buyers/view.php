<?php
/* @var $this BuyersController */
/* @var $model Buyers */

$this->breadcrumbs=array(
	'Buyers'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List Buyers', 'url'=>array('index')),
	array('label'=>'Create Buyers', 'url'=>array('create')),
	array('label'=>'Update Buyers', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Buyers', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Buyers', 'url'=>array('admin')),
);
?>

<h1>View Buyers #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'buyer_type',
		'phone',
		'address',
		'email_id',
		'gst_no',
		'status',
		'company',
		'project',
		'flat_numbers',
		'created_by',
		'updated_by',
		'created_date',
		'updated_date',
	),
)); ?>
