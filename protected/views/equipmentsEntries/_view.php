<?php
/* @var $this SubcontractorController */
/* @var $data Subcontractor */
?>

<?php
$tblpx = Yii::app()->db->tablePrefix;
$id = $data['id'];
?>
<style>
    .legend {
        list-style: none;
        padding-left: 0px;

    }

    .legend li {
        float: left;
        margin-right: 10px;
    }

    .legend span {
        border: 1px solid #ccc;
        float: left;
        width: 12px;
        height: 12px;
        margin: 2px;
    }

    .legend .project_not_mapped {
        background-color: #A9E9EC;
        border: 2px solid #f8cbcb;
        margin-right: 4px;
    }
</style>
<?php if ($index == 0) : ?>
    <thead>
        <tr>
            <th style="width:60px;">Action</th>
            <th style="width:20px;">Sl No.</th>
            <th>Company</th>
            <th>Project</th>
            <th>Sub Contractor</th>
            <th>Expense Head</th>
            <th>Description</th>
            <th>Date</th>
            <th>Material</th>
            <th>Requested From</th>
            <th>status</th>
        </tr>
    </thead>
<?php endif; ?>
<?php
$class = '';
$newclass = '';
$data = EquipmentsEntries::model()->findByPk($data['id']);
if(!$data['project_id'] || ($data->getUnmappedEquipmentsCount() >0 ) ){
    $newclass = 'empty-project-row';
}else if($data['status'] == '3'){
    $newclass='row_rejected';
}else if($data['status'] =='1'){
    $newclass = 'row_class';
}
?>
<tr class="<?php echo $class . '' . $newclass; ?>">
    <td>
        <?php if ($newclass != 'empty-project-row' ) { ?>
            <span class="icon icon-options-vertical popover-test" data-toggle="popover" data-placement="right" type="button"
                data-html="true" style="cursor: pointer;"></span>
            <div class="popover-content hide">
                <ul class="tooltip-hiden">
                    <?php if($data['status'] != '2'){?>
                    <li><a class="click btn btn-xs btn-default" id="<?php echo $data['id']; ?>" href="#"
                            onClick="edit('<?php echo $data['id']; ?>')" title="Update">Edit</a></li>
                   <?php } ?>
                   <?php if($data['status'] == '2'){?>
                    <li><a class="click btn btn-xs btn-default" id="<?php echo $data['id']; ?>" href="#"
                            onClick="view('<?php echo $data['id']; ?>')" title="Update">View</a></li>
                    <?php } ?> 
                    <?php if ($data['status'] == '1') { ?>
                        <!-- quotation view -->
                        <?php if ($data['request_from'] == '1') { ?>
                        <li><a class="click btn btn-xs btn-default" data-item-id="<?php echo $data['id']; ?>" href="#"
                                onClick="actdelete('<?php echo $data['id']; ?>',this)" title="delete">delete</a></li>
                        <?php } ?>
                        <li><a class="click btn btn-xs btn-default" id="<?php echo $data['id']; ?>" href="#"
                                onClick="approve('<?php echo $data['id']; ?>',this)" title="Approve">Approve</a></li>

                                <li><a class="click btn btn-xs btn-default" id="<?php echo $data['id']; ?>" href="#" onClick="openRejectModal('<?php echo $data['id']; ?>')" title="Reject">Reject</a></li>

                    <?php } ?>
                </ul>
            </div>
        <?php }
    ?>

    </td>
    <td style="width:20px;"><?php echo $index + 1; ?></td>
    <td>
        <?php
        $company = Company::model()->findByPk($data['company_id']);
        echo !empty($company) ? CHtml::encode($company->name) : "No company data";
        ?>
    </td>
    <td>
        <?php
        $projectModel = Projects::model()->findByPk($data['project_id']);
        echo !empty($projectModel) ? CHtml::encode($projectModel->name) : "No project data";
        ?>
    </td>
    <td>
        <?php
        $subModel = Subcontractor::model()->findByPk($data['subcontractor_id']);
        echo !empty($subModel) ? CHtml::encode($subModel->subcontractor_name) : "No subcontractor data";
        ?>
    </td>
    <td>
        <?php
        $expensehead = ExpenseType::model()->findByPk($data["expensehead_id"]);
        echo !empty($expensehead) ? CHtml::encode($expensehead->type_name) : "No expense head data";
        ?>
    </td>
    <td style="white-space:nowrap;"><?php echo CHtml::encode($data['description']); ?></td>
    <td style="white-space:nowrap;"><?php echo CHtml::encode($data['Date']); ?></td>
    <td>
        <?php 
        $equipmentItems = EquipmentsItems::model()->findAllByAttributes(['equipment_entry_id' => $id]);
        $equipmentData = [];

        if ($equipmentItems) {
            foreach ($equipmentItems as $equipment) {
                $equipmentDetail = Equipments::model()->findByPk($equipment->equipments);
                if ($equipmentDetail) {
                    $equipmentData[] = CHtml::encode($equipmentDetail->equipment_name);
                }
            }
            echo !empty($equipmentData) ? implode(", ", $equipmentData) : "No equipment data found";
        } else {
            echo "No equipment found";
        }
        ?>
    </td>
    <td style="white-space:nowrap;">
        <?php echo CHtml::encode($data['request_from'] == 1 ? 'Accounts' : 'PMS'); ?>
    </td>
    <td style="white-space:nowrap;">
        <?php 
        if($data['status'] == 1){
            echo CHtml::encode( 'Pending');
        }else if($data['status'] == 2){
            echo CHtml::encode( 'Approved');
        }
        else if($data['status'] == 3){
            echo CHtml::encode( 'Rejected');
        }
             ?>
    </td>
</tr>

<style>
    .permission_style {
        background: #f8cbcb !important;
    }

    .row_class {
        background-color: #f8cbcb !important;
    }
    .row_rejected{
        background-color: #f57275 !important;
    }
</style>    