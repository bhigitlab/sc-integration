CREATE TABLE `jp_sc_payment_stage` ( `id` INT(11) NOT NULL AUTO_INCREMENT , `quotation_id` INT(11) NOT NULL , `payment_stage` TEXT NOT NULL , `stage_percent` FLOAT NOT NULL , `stage_amount` FLOAT(11,2) NOT NULL , `bill_status` ENUM('0','1') NOT NULL DEFAULT '0' COMMENT '0=>pending,1=>generated' , `created_by` INT(11) NOT NULL , `created_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP , `updated_by` INT(11)  NULL , `updated_date` DATETIME  NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;

ALTER TABLE `jp_dailyreport` CHANGE `works_done` `works_done` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;

ALTER TABLE `jp_dailyreport` CHANGE `materials_unlade` `materials_unlade` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;

ALTER TABLE `jp_dailyreport` CHANGE `extra_work_done` `extra_work_done` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;

ALTER TABLE `jp_subcontractorbill` ADD `stage_id` INT(11) NULL DEFAULT NULL AFTER `scquotation_id`;

ALTER TABLE `jp_subcontractor_payment` ADD `stage_id` INT(11) NULL DEFAULT NULL AFTER `quotation_number`;

ALTER TABLE `jp_pre_subcontractor_payment` ADD `stage_id` INT(11) NULL DEFAULT NULL AFTER `quotation_number`;

UPDATE `jp_menu` SET `menu_name` = 'Add Payment Stage' WHERE `jp_menu`.`menu_id` = 776;
INSERT INTO `jp_menu` (`menu_id`, `menu_name`, `parent_id`, `status`, `controller`, `action`, `params`, `showmenu`, `show_list`, `related`) VALUES (778, 'Get Payment Stage By Quotation', '594', '0', 'Subcontractorbill', 'getpaymentstagebyquotation', '', '0', '0', NULL);