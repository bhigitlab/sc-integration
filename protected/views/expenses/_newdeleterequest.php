<?php
/* @var $this ExpensesController */
/* @var $data Expenses */

if ($index == 0) {
	
    ?>

    <thead>
        <tr>
            <th>Sl No.</th>
            <th width="55%">Data</th>
            <th>Table</th>
            <th>Requested By</th>
            <th>Role</th>
            <th>Date</th>
            <th></th>
        </tr>   
    </thead>
    <?php
}
?>
    <tr>
        <td class="text-right"  ><?php echo $index + 1; ?></td>
        <td><?php print_r(json_decode($data->deletepending_data)); ?></td>
        <td><?php echo $data->deletepending_table; ?></td>
        <?php
            $usermodel  = Users::model()->findBypk($data->user_id);
            $rolemodel  = UserRoles::model()->findBypk($usermodel->user_type);
        ?>
        <td><?php echo $usermodel->first_name." ".$usermodel->last_name; ?></td>
        <td><?php echo $rolemodel->role; ?></td>
        <td><?php echo date("d-m-Y", strtotime($data->requested_date)); ?></td>
        <td>
            <span class="icon icon-options-vertical popover-test" data-toggle="popover" data-placement="left" type="button" data-html="true" style="cursor: pointer;" ></span>
            <div class="popover-content hide">
                <ul class="tooltip-hiden">
                    <li><button id="<?php echo $data->deletepending_id; ?>" class="btn btn-xs btn-default approve_request">Approve</button></li>
                    <li><button id="<?php echo $data->deletepending_id; ?>" class="btn btn-xs btn-default cancel_request">Cancel</button></li>
                </ul>
            </div>
        </td>
    </tr>   




