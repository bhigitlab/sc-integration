<?php

/**
 * This is the model class for table "{{pms_material_requisition}}".
 *
 * The followings are the available columns in table '{{pms_material_requisition}}':
 * @property integer $requisition_id
 * @property integer $requisition_project_id
 * @property integer $requisition_task_id
 * @property string $requisition_no
 
 * @property string $requisition_unit
 * @property integer $requisition_quantity
 * @property string $requisition_date
 * @property integer $requisition_status
 * @property integer $created_by
 * @property string $created_date
 * @property integer $updated_by
 * @property string $updated_date
 *
 * The followings are the available model relations:
 * @property Projects $requisitionProject
 * @property Tasks $requisitionTask
 */
class MaterialRequisition extends CActiveRecord
{

    public $purchase_billing_status;
    public $purchase_no;
    public $caption;
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return MaterialRequisition the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    

    /**
     * @return string the associated database table name
     */
    
    public function tableName()
    {
        
        return 'pms_material_requisition';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('requisition_no,  created_by, updated_by', 'required'),
            // array('requisition_project_id, requisition_task_id, requisition_quantity, requisition_status, created_by, updated_by', 'numerical', 'integerOnly'=>true),
            array('requisition_no', 'length', 'max' => 200),
            array('created_date, updated_date,project_id,task_description,purchase_id', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('requisition_id, requisition_project_id, requisition_task_id, requisition_no,requisition_status, created_by, created_date, updated_by, updated_date,purchase_id,project_id,task_description,purchase_billing_status,purchase_no,caption,type_from,company_id', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'requisitionProject' => array(self::BELONGS_TO, 'Projects', 'requisition_project_id'),
            'requisitionTask' => array(self::BELONGS_TO, 'Tasks', 'requisition_task_id'),
            'requisitionCreated' => array(self::BELONGS_TO, 'Users', 'created_by'),
            'project' => array(self::BELONGS_TO, 'Projects', 'project_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'requisition_id' => 'Requisition',
            'requisition_project_id' => 'Project',
            'requisition_task_id' => 'Task',
            'requisition_no' => 'Requisition No',
            'requisition_status' => 'Status',
            'created_by' => 'Created By',
            'created_date' => 'Created Date',
            'updated_by' => 'Updated By',
            'updated_date' => 'Updated Date',
            'purchase_id'=>'Purchase Number',
            'company_id'=>'Company'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;
        $criteria->select = "t.*,`p`.purchase_no,`p`.purchase_billing_status,s.caption";
        $criteria->join = 'LEFT JOIN `jp_purchase` `p` ON (`p`.`p_id`=`t`.`purchase_id`)'; 
        $criteria->join .= 'LEFT JOIN `jp_status` `s` ON (`s`.`sid`=`p`.purchase_billing_status)'; 
        $criteria->compare('requisition_id', $this->requisition_id);
        $criteria->compare('requisition_project_id', $this->requisition_project_id);
        $criteria->compare('requisition_task_id', $this->requisition_task_id);
        $criteria->compare('requisition_no', $this->requisition_no, true);
        $criteria->compare('task_description', $this->task_description);
        $criteria->compare('requisition_status', $this->requisition_status);
        $criteria->compare('created_by', $this->created_by);
        $criteria->compare('created_date', $this->created_date, true);
        $criteria->compare('updated_by', $this->updated_by);
        $criteria->compare('updated_date', $this->updated_date, true);
        $criteria->compare('t.project_id', $this->project_id);
        $criteria->compare('purchase_id', $this->purchase_id,true);
        $criteria->compare('type_from', $this->type_from);
        $criteria->compare('caption', $this->caption,true);
        $criteria->compare('purchase_no', $this->purchase_no);
        $criteria->addCondition("requisition_status = 1");
        
        return new CActiveDataProvider($this, array(
            'pagination' => array('pageSize' => 10),
            'criteria' => $criteria, 'sort' => array(
                'defaultOrder' => 'requisition_id desc',
            ),
        ));
	 }

    public function searchmr()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;
        $criteria->compare('requisition_id', $this->requisition_id);
        $criteria->compare('requisition_project_id', $this->requisition_project_id);
        $criteria->compare('requisition_task_id', $this->requisition_task_id);
        $criteria->compare('requisition_no', $this->requisition_no, true);
        $criteria->compare('purchase_id', $this->purchase_id);
        $criteria->addCondition("requisition_status = 1");
        return new CActiveDataProvider($this, array(
            'pagination' => array('pageSize' => 25),
            'criteria' => $criteria, 'sort' => array(
                'defaultOrder' => 'requisition_id desc',
            ),
        ));
	 }

     public function searchaccounts()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;
        $criteria->compare('requisition_id', $this->requisition_id);
        $criteria->compare('project_id', $this->project_id);
        $criteria->compare('task_description', $this->task_description);
        $criteria->compare('requisition_no', $this->requisition_no, true);
        $criteria->compare('requisition_status', $this->requisition_status);
        $criteria->compare('created_by', $this->created_by);
        $criteria->compare('created_date', $this->created_date, true);
        $criteria->compare('updated_by', $this->updated_by);
        $criteria->compare('updated_date', $this->updated_date, true);
        $criteria->compare('purchase_id', $this->purchase_id);
        $criteria->addCondition("requisition_status = 1");
        $criteria->addCondition("type_from = 2");
		return new CActiveDataProvider($this, array(
            'pagination' => array('pageSize' => 25),
            'criteria' => $criteria, 'sort' => array(
                'defaultOrder' => 'requisition_id desc',
            ),
        ));
	 }

     public static function getPurchaseName($purchase_id)
     {
         if (empty($purchase_id)) {
             return null;
         }
 
         // Query to get purchase_no from jp_purchase table
         $sql = "SELECT purchase_no FROM jp_purchase WHERE p_id = :purchase_id";
         $command = Yii::app()->db->createCommand($sql);
         $command->bindParam(":purchase_id", $purchase_id, PDO::PARAM_INT);
         $purchase_no = $command->queryScalar();
 
         if ($purchase_no) {
             return $purchase_no;
         }
 
         // If purchase_no is empty, query to get bill_number from jp_bills table
         $sql = "SELECT bill_number FROM jp_bills WHERE purchase_id = :purchase_id";
         $command = Yii::app()->db->createCommand($sql);
         $command->bindParam(":purchase_id", $purchase_id, PDO::PARAM_INT);
         $bill_number = $command->queryScalar();
 
         return $bill_number ? $bill_number : "No Purchase No or Bill Number";
     }
 
     // Method to get purchase numbers or bill numbers for multiple purchase ids
     public function getPurchaseNumbersOrBillNumbers()
     {
        $items = MaterialRequisitionItems::model()->findAllByAttributes(array('mr_id' => $this->requisition_id));
        $names = [];

        foreach ($items as $item) {
            $item_det = MaterialRequisitionItems::model()->findByPK($item->id);
            $purchase_id=$item_det->po_id;
            $purchaseNumber = self::getPurchaseName($purchase_id);
            if ($purchaseNumber) {
                $names[] = $purchaseNumber;
            }
        }

        return implode(' <br>', $names);
        
     }
    public function getBillingStatuses()
    {
        $items = MaterialRequisitionItems::model()->findAllByAttributes(array('mr_id' => $this->requisition_id));
        $billingStatuses = [];

        foreach ($items as $item) {
            $item_det = MaterialRequisitionItems::model()->findByPK($item->id);
            $purchase_id=$item_det->po_id;
            
            $billingStatus = self::getBillingStatus(trim($purchase_id));
            if ($billingStatus) {
                $billingStatuses[] = $billingStatus;
            }
        }

        return implode(' <br>', $billingStatuses);
    }

    public static function getBillingStatus($purchase_id)
    {
        // Ensure the purchase_id is not empty
        if (empty($purchase_id)) {
            return null;
        }

        $sql = "SELECT purchase_billing_status FROM jp_purchase WHERE p_id = :purchase_id";
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(":purchase_id", $purchase_id, PDO::PARAM_INT);

        $po_bill_status_id = $command->queryScalar();
        if ($po_bill_status_id) {
            $status = Status::model()->findByPk($po_bill_status_id);
            return $status ? $status['caption'] : null;
        }
        return null;
    }
    
    public function updateMrStatus($mrId='')
    {
        // Get all items related to this MR
        $sql = "SELECT po_id FROM pms_material_requisition_items WHERE mr_id = :mr_id";
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(':mr_id', $mrId, PDO::PARAM_INT);
        $items = $command->queryAll();

        $allSaved = true;

        foreach ($items as $item) {
            if (empty($item['po_id'])) {
                $allSaved = false;
                break;
            }

            // Check the po_status of the current item
            $sql = "SELECT purchase_status FROM jp_purchase WHERE p_id = :po_id";
            $command = Yii::app()->db->createCommand($sql);
            $command->bindParam(':po_id', $item['po_id'], PDO::PARAM_INT);
            $poStatus = $command->queryScalar();

            if ($poStatus !== 'saved') {
                $allSaved = false;
                break;
            }
        }

        // Update the mr_status based on the check
        $mr_status = $allSaved ? 4 : 3;
        return $mr_status;
        
    }
    public function getPurchaseNumbers()
    {
        $purchaseIds = explode(',', $this->purchase_id);
        $criteria = new CDbCriteria();
        $criteria->addInCondition('p_id', $purchaseIds);

        $purchases = Purchase::model()->findAll($criteria);
        $purchaseNumbers = array_map(function($purchase) {
            return $purchase->purchase_no;
        }, $purchases);

        return implode(' <br>', $purchaseNumbers);
    }
    
}
