<div class="page_filter">
    <div class="row ">
        <div class="col-sm-6 col-md-3 form-group">
            <label>Warehouse</label>
            <?php
            $type_list = CHtml::listData(
                Warehouse::model()->findAll(),
                'warehouse_id',
                'warehouse_name'
            );
            echo CHtml::dropDownList(
                '',
                '',
                $type_list,
                array('empty' => 'Choose Warehouse', 'class' => 'form-control select_box warehouse_id')
            );
            ?>
        </div>
        <div class="col-sm-6 col-md-3 form-group">
            <label>Item</label>
            <?php
            $item_list = array();
            echo CHtml::dropDownList(
                '',
                '',
                $type_list,
                array('empty' => 'Choose Item', 'class' => 'form-control select_box receiptItems')
            );
            ?>
        </div>
        <div class="col-md-2 text-sm-left text-right">
            <label>&nbsp;</label>
            <div>
                <?php echo CHtml::button('Search', array('class' => 'btn btn-info btn-sm btn_search')); ?>
            </div>
        </div>
    </div>
</div>