<?php
/* @var $this ProjectsController */
/* @var $model Projects */
/* @var $form CActiveForm */
?>




	<div class="page_filter custom-form-style padding-top-10">
		<?php $form=$this->beginWidget('CActiveForm', array(
			'action'=>Yii::app()->createUrl($this->route),
			'method'=>'get',
		)); ?>
		<div class="row">
			<div class="col-md-3 col-sm-6 form-group">
				<?php echo $form->textField($model,'name',array('placeholder' => 'Project Name','class'=>'form-control')); ?>
			</div>
			<div class="col-md-3 col-sm-6 form-group">
				<?php echo $form->dropDownList($model, 'project_status', CHtml::listData(Status::model()->findAll(
						array(
							'select' => array('sid,caption'),
							'condition' => 'status_type="project_status"',
							'order' => 'caption',
							'distinct' => true,
				)), 'sid', 'caption'),array('class'=>'form-control','empty' => 'All Projects')); ?>
			</div>

			<?php
				$user = Users::model()->findByPk(Yii::app()->user->id);
				$arrVal = explode(',', $user->company_id);
				$newQuery = "";
				foreach($arrVal as $arr) {
					if ($newQuery) $newQuery .= ' OR';
					$newQuery .= " FIND_IN_SET('".$arr."', id)";
				}
			?>
			<div class="col-md-3 col-sm-6 form-group">
				<?php echo $form->dropDownList($model, 'company_id', CHtml::listData(Company::model()->findAll(
					array(
						'select' => array('id,name'),
						'condition' => $newQuery,
						'order' => 'name',
						'distinct' => true,
					)), 'id', 'name'),array('class'=>'form-control','empty' => 'Company')); ?>
				<?php echo $form->error($model,'company_id'); ?>
			</div>
			<div class="col-md-3 col-sm-6 form-group">
				<?php echo $form->dropDownList($model, 'project_category', CHtml::listData(Status::model()->findAll(
					array(
						'select' => array('sid,caption'),
						'condition' => 'status_type = "project_category"',
						'order' => 'caption',
						'distinct' => true,
					)), 'sid', 'caption'),array('class'=>'form-control','empty' => 'Category')); ?>
				<?php echo $form->error($model,'project_category'); ?>
			</div>
			<div class="col-sm-12 text-right">
				<input type="reset" class="btn btn-default btn-sm" id="clear" value="Clear"/>
			</div>
		</div>
		<?php $this->endWidget(); ?>
	</div>
