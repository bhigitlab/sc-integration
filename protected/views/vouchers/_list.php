<?php
$tblpx = Yii::app()->db->tablePrefix;
if ($index == 0) {
?>
    <thead>
        <tr>
            <th class="">Company</th>
            <th>Project</th>
            <th>Subcontractor/Vendor</th>
            <th>Expense Head</th>
            <th>Vehicle Number</th>
            <th>Voucher Number</th>
            <th>Voucher Date</th>
            <th>Staff</th>
            <th>Received By</th>
            <?php
            if ((isset(Yii::app()->user->role) && ((in_array('/vendors/paymentedit', Yii::app()->user->menuauthlist)) || (in_array('/vendors/paymentdelete', Yii::app()->user->menuauthlist))))) {
            ?>
                <th></th>
            <?php } ?>

        </tr>
    </thead>
<?php } ?>
<tr id="<?php echo $index; ?>" class="" title="">
    <td class="pro_back wrap"><?php echo $data->company0->name; ?></td>
    <td class="pro_back wrap"><?php echo $data->project0->name; ?></td>
    <td class="pro_back wrap"><?php echo $data->scVendorData($data->voucher_for, $data->voucher_for_user_id); ?></td>
    <td class="pro_back wrap"><?php echo $data->expenseHead->type_name; ?></td>
    <td class="pro_back wrap"><?php echo $data->vehicle_number; ?></td>
    <td class="pro_back wrap"><?php echo $data->voucher_number; ?></td>
    <td class="pro_back wrap"><?php echo $data->voucher_date; ?></td>
    <td class="pro_back wrap"><?php echo $data->staff->first_name; ?></td>

    <td class="pro_back wrap"><?php echo $data->received_by; ?></td>
    <td>
        <span class="icon icon-options-vertical popover-test" data-toggle="popover" data-placement="left" type="button" data-html="true" style="cursor: pointer;"></span>
        <div class="popover-content hide">
            <ul class="tooltip-hiden">
                <?php
                if ((in_array('/vendors/paymentedit', Yii::app()->user->menuauthlist))) {
                ?>
                    <li><button id="<?php echo $data->id; ?>" class="btn btn-xs btn-default edit_voucher">Edit</button></li>
                    <li><button id="<?php echo $data->id; ?>" class="btn btn-xs btn-default view_voucher">View</button></li>
                <?php }

                if ((in_array('/vendors/paymentdelete', Yii::app()->user->menuauthlist))) {
                ?>
                    <li><button id="<?php echo $data->id; ?>" class="btn btn-xs btn-default delete_voucher">Delete</button></li>
                <?php } ?>
            </ul>
        </div>
    </td>
</tr>
<script>
    $(document).ready(function() {
        $(".popover-test").popover({
            html: true,
            content: function() {
                return $(this).next('.popover-content').html();
            }
        });
        $('[data-toggle=popover]').on('click', function(e) {
            $('[data-toggle=popover]').not(this).popover('hide');
        });
        $('body').on('hidden.bs.popover', function(e) {
            $(e.target).data("bs.popover").inState.click = false;
        });
        $('body').on('click', function(e) {
            $('[data-toggle=popover]').each(function() {
                if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                    $(this).popover('hide');
                }
            });
        });


    });
</script>