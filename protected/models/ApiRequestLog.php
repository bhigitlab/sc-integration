<?php

/**
 * This is the model class for table "{{api_request_log}}".
 *
 * The followings are the available columns in table '{{api_request_log}}':
 * @property integer $request_id
 * @property integer $project_id
 * @property string $type
 * @property string $request
 * @property string $response
 * @property integer $created_by
 * @property string $created_date
 * @property integer $updated_by
 * @property string $updated_date
 */
class ApiRequestLog extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{api_request_log}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('project_id, type, request, created_by', 'required'),
            array('project_id, created_by, updated_by', 'numerical', 'integerOnly'=>true),
            array('type', 'length', 'max'=>50),
            array('response, created_date, updated_date', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('request_id, project_id, type, request, response, created_by, created_date, updated_by, updated_date', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'request_id' => 'Request',
            'project_id' => 'Project',
            'type' => 'Type',
            'request' => 'Request',
            'response' => 'Response',
            'created_by' => 'Created By',
            'created_date' => 'Created Date',
            'updated_by' => 'Updated By',
            'updated_date' => 'Updated Date',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('request_id',$this->request_id);
        $criteria->compare('project_id',$this->project_id);
        $criteria->compare('type',$this->type,true);
        $criteria->compare('request',$this->request,true);
        $criteria->compare('response',$this->response,true);
        $criteria->compare('created_by',$this->created_by);
        $criteria->compare('created_date',$this->created_date,true);
        $criteria->compare('updated_by',$this->updated_by);
        $criteria->compare('updated_date',$this->updated_date,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return ApiRequestLog the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}