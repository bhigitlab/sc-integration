<link rel="stylesheet" type="text/css" href="uploads/new_template/css/newstyle.css" />

<?php
$tblpx = Yii::app()->db->tablePrefix;
$proid= $model['project_id'];
$sql=Yii::app()->db->createCommand("select {$tblpx}clients.name from {$tblpx}clients
left join {$tblpx}projects ON {$tblpx}projects.client_id={$tblpx}clients.cid 
where {$tblpx}projects.pid=$proid")->queryRow();
?>

<?php  

$project = Yii::app()->db->createCommand("SELECT  pid, name FROM `{$tblpx}projects` 
where pid =".$proid)->queryAll();

?>




	 
<div class="container">

<header class="headerinv">
	<table class="invoiceheader">
		<tr>
		<td><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/logo.png" style="height:100px;padding:5px;"></td>
		<td class="details" style="width:300px;"><b>
			3rd Floor,Dr.Rajkrishnan's Building &#13;
			Nort Fort Gate,Tripunithura,Kochi &#13;&#10;
			Call:0484 4030077, 9995107007 &#13;
			Mail:info@jpventures.in</b>
		</td>
		</tr>
	</table>
	<br>
<header>	
	
	<h2>INVOICE</h2>
	<br/>
	
	
	<table style="float:left" border="1">
	<tr>
		<td><label>BILL TO : </label><?php echo $sql['name'];?></td>
		</tr>
	
	</table>
	
	<br/><br/>

	<table style="float:right" border="1">
	<tr>
		<td>
		<label>DATE : </label> <?php echo date("d-m-Y", strtotime($model['date'])); ?>
		</td>
		</tr>
	<tr>
		<td>
		<label>INVOICE NO : </label> <?php echo $model['inv_no']; ?>
		</td>
	</tr>
	</table>
	
	<br/><br/>
	
	
	<table border="1">
		<tr>
			<td><label>PROJECT NAME : </label> <?php echo $project[0]['name']; ?>
		</tr>
	</table>

	<br/><br/>
	
	<table border="1" style="width:100%;">
			<thead>
				<tr>
					<th>Sl.No</th>
					<th>Description</th>
					<th>Quantity</th>
					<th>Unit</th>
					<th>Rate</th>
					<th>Amount</th>
				</tr>
			</thead>
			<tbody class="addrow">
								<?php
				$i=1;
				foreach($newmodel as $new) 
				{
				?>
				<tr>
					<td><?php echo $i; ?></td>
					<td><?php echo $new['description']; ?></td>
					<td><?php echo $new['quantity']; ?></td>
					<td><?php echo $new['unit']; ?></td>
					<td><?php echo $new['rate']?></td>
					<td><?php echo $new['amount']; ?></td>
				</tr>
				<?php
				$i++;
				}
				?>
			</tbody>
	</table>
	<br/><br/>
	
	<table border="1" style="width:100%;">
		<tr>
			<td>SUB TOTAL :<?php echo $model['subtotal'];?></td>
			<td>FEES :  <?php echo $model['fees'];?></td>
			<td>GRAND TOTAL :<?php echo $model['amount'];?><td>
		</tr>
	</table>
	
	<h4>Autherised Signatory,</h4>
	<h4>For <?php echo  Yii::app()->name;?></h4>
	<p>Please make cash payment.</p>
	<br/><br/>
<footer class="inv_footer"><?php echo  Yii::app()->params['webiste_name'];?></footer>
</div>







