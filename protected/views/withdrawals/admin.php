<?php
/* @var $this WithdrawalsController */
/* @var $model Withdrawals */

$this->breadcrumbs=array(
	'Withdrawals'=>array('index'),
	'Manage',
);

$this->menu=array(
    array('label'=>'Settings', 'url'=>array('settings')),
	//array('label'=>'List Withdrawals', 'url'=>array('index')),
	//array('label'=>'Create Withdrawals', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('withdrawals-grid', {
		data: $(this).serialize()
	});
	return false;
});

function reinstallDatePicker(id, data) {
        //use the same parameters that you had set in your widget else the datepicker will be refreshed by default
    $('#datepicker_for_date').datepicker(jQuery.extend({showMonthAfterYear:false},jQuery.datepicker.regional['en-GB'],{'dateFormat':'yy-mm-dd'}));
}


calculate_totals();

$(document).ajaxStart(function(){
    //

    var user_select = $('.filters select:first').val();
    var dates_selct = $('.filters input:first').val();
    //
    var amounts_sel = $('.filters input:eq(1)').val();
    var typesin_sel = $('.filters select:eq(1)').val();
    var detailsin_sel = $('.filters select:eq(2)').val();
    var project_sel = $('.filters select:eq(3)').val();
    //

    if(user_select == ''){user_select = 0;}
    if(dates_selct == ''){dates_selct = 0;}
    if(amounts_sel == ''){amounts_sel = 0;}
    if(typesin_sel == ''){typesin_sel = 0;}
    if(detailsin_sel == ''){detailsin_sel = 0;}
    if(project_sel == ''){project_sel = 0;}
    //
    calculate_totals(user_select,dates_selct,amounts_sel,typesin_sel,detailsin_sel,project_sel);
    //        
});

function calculate_totals(user_select,dates_selct,amounts_sel,typesin_sel,detailsin_sel,project_sel){
    //
    $.ajax({
        url: '".$this->createUrl('withdrawals/returncalc')."&user_sel='+user_select+'&date='+dates_selct+'&amount='+amounts_sel+'&type='+typesin_sel+'&detail='+detailsin_sel+'&project='+project_sel,
        dataType: 'html'
    }).success(function(data){
        $('#dataprinthere').html('');
        $('#dataprinthere').append(data);
    });
    //
}



");
?>

<h1>Manage All Withdrawals</h1>
<div id="dataprinthere"></div>
<div class="addlink">
    <?php
    $createUrl = $this->createUrl('create', array("asDialog" => 1, "gridId" => 'address-grid'));
    echo CHtml::link('Add Withdrawal', '', array('class' => 'edittime', 'onclick' => "$('#cru-frame').attr('src','$createUrl '); $('#cru-dialog').dialog('open');"));
    ?>
</div>



<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'withdrawals-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
        'afterAjaxUpdate' => 'reinstallDatePicker', // (#1)
	'columns'=>array(
                array('class' => 'IndexColumn', 'header' => 'Sl.No.', 'htmlOptions' => array('style' => "width:50px;"),),
                array('name' => 'userid',
                    'value' => '$data->user->first_name',
                    'htmlOptions' => array('class'=>'user_in','style' => "width:100px;"),
                    'filter' => CHtml::listData(Users::model()->findAll(
                                    array(
                                        'select' => array('userid,first_name'),
                                        'order' => 'first_name',
                                        'distinct' => true
                            )), "userid", "first_name"),
                ),
                array('name' => 'date',
                    'value' => '$data->date',
                    'htmlOptions' => array('style' => "width:80px;"),
                                'filter' => $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model, 
                        'attribute'=>'date', 
                        'language' => 'en-GB',
                        // 'i18nScriptFile' => 'jquery.ui.datepicker-ja.js', (#2)
                                        'options' => array(
                                        'showAnim' => 'fold',
                                        'dateFormat'=>'yy-mm-dd',
                                        ),
                        'htmlOptions' => array(
                            'id' => 'datepicker_for_date',
                            'size' => '10',
                        ),
                        'defaultOptions' => array(  // (#3)
                            'showOn' => 'focus', 
                            'dateFormat' => 'yy-mm-dd',
                            'showOtherMonths' => true,
                            'selectOtherMonths' => true,
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showButtonPanel' => true,
                        )
                    ), 
                    true),
                ),
                array('name' => 'amount',
                    'value' => '"Rs ".$data->amount',
                    'htmlOptions' => array('style' => "width:100px;"),
                ),
                array(
                    'name' => 'type',
                    'value' => '$data->status0->caption',
                    'type' => 'raw',
                    'filter' => CHtml::listData(Status::model()->findAll(
                                    array(
                                        'select' => array('sid,caption'),
                                        'condition' => 'status_type="withdrawal"',
                                        'order' => 'status_type',
                                        'distinct' => true
                            )), "sid", "caption"),
                    'htmlOptions' => array('style' => "width:80px;"),
                ),
            
                array(
                        'name' => 'details',
                        'value' => '$data->status1->caption',
                        'type' => 'raw',
                        'filter' => CHtml::listData(Status::model()->findAll(
                                        array(
                                            'select' => array('sid,caption'),
                                            'condition' => 'status_type="withdrawal_details"',
                                            'order' => 'status_type',
                                            'distinct' => true
                                )), "sid", "caption"),
                        'htmlOptions' => array('style' => "width:80px;"),
                ),
		array(
                    'name' => 'projectid',
                    'value' => '$data->project->name',
                    'type' => 'raw',
                    'filter' => CHtml::listData(Projects::model()->findAll(
                                    array(
                                        'select' => array('pid,name'),
                                        'order' => 'name',
                                        'distinct' => true
                            )), "pid", "name"),
                    'htmlOptions' => array('style' => "width:300px;"),
                ),
		'description',
                array(
                    'name' => 'created_by',
                    'value' => '$data->createdBy->first_name',
                    'type' => 'raw',
                    'htmlOptions' => array('style' => "width:100px;"),
                ),
                array(
                    'name' => 'created_date',
                    'value' => '$data->created_date',
                    'type' => 'raw',
                    'htmlOptions' => array('style' => "width:100px;"),
                ),
		/*
		'updated_by',
		'updated_date',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>


<?php
//--------------------- begin new code --------------------------
// add the (closed) dialog for the iframe
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id' => 'cru-dialog',
    'options' => array(
        'title' => 'Add Withdrawal',
        'autoOpen' => false,
        'modal' => false,
        'width' => 420,
        'height' => 550,
    ),
));
?>
<iframe id="cru-frame" width="400" height="500" scrolling="auto"></iframe>

<?php
$this->endWidget();
?>

<style>
    .summary{
        text-align:left !important;
    }
    .bottom_count{
        font-weight: bolder;
    }
</style>