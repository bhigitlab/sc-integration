<?php

/**
 * This is the model class for table "jp_worktype".
 *
 * The followings are the available columns in table 'jp_worktype':
 * @property integer $wtid
 * @property string $work_type
 * @property double $rate
 * @property integer $department_id
 *
 * The followings are the available model relations:
 * @property Department $department
 */
class WorkTypepms extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'jp_worktype';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('work_type', 'required', 'message' => 'Enter  {attribute}'),
			array('department_id', 'safe', 'message' => 'Choose {attribute}'),
			
			//array('rate', 'numerical'),
			array('work_type', 'length', 'max'=>30),
			//array('work_type', 'department_id','rate','safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('wtid, work_type,department_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'tasks' => array(self::HAS_MANY, 'Tasks', 'work_type'),
			'department' => array(self::BELONGS_TO, 'Department', 'department_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'wtid' => 'Wtid',
			'work_type' => 'PMS Work Type',
			'rate' => 'Rate',
			'department_id' => 'Department',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('wtid',$this->wtid);
		$criteria->compare('work_type',$this->work_type,true);
		$criteria->compare('rate',$this->rate);
		$criteria->compare('department_id',$this->department_id);
		
		/*echo "<pre>";
		print_r($this->work_type);die;*/

		return new CActiveDataProvider($this, array('pagination' => array('pageSize' => 25,),
			'criteria'=>$criteria,
			 'sort'=>array(
				'defaultOrder'=>'wtid DESC',
					),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return WorkTypepms the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
