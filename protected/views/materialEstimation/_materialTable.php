
<div class="container1">
    <h4>Material Estimation</h4>
    <table cellpadding="10" id="materialEstimationTbl" class="table">
        <?php if ($dataProvider->getItemCount() > 0) : ?>
            <thead>
                <tr>
                    <th>SI No</th>
                   
                    <th>Material</th>
                    <th>Specification</th>
                    <th>Remarks</th>
                    <th>Total Quantity</th>
                    <th>Actual Quantity</th>

                    <th>Balance Quantity</th>
                    <th>Unit</th>
                    <th>Rate</th>
                    <th>Total Amount</th>
                     <th>Actual Amount</th>
                      <th>Balance Amount</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                $totalAmount=0;
                foreach ($dataProvider->getData() as $index => $data):
                    $totalAmount += $data->amount;
                 ?>
                    <tr>
                        <td style="width: 50px;"><?php echo $index + 1; ?></td>
                        
                        <td>
                            <?php
                            $pms_api_integration_model=ApiSettings::model()->findByPk(1);
                            $pms_api_integration =$pms_api_integration_model->api_integration_settings;

                            if($pms_api_integration==0){
                                $materialModel = Materials::model()->findByPk($data->material);
                                echo !empty($materialModel) ? CHtml::encode($materialModel->material_name) : "";
                            }
                            else{
                                $materialModel = Materials::model()->findByPk($data->material);
                                echo !empty($materialModel) ? CHtml::encode($materialModel->material_name) : "";
                            }
                            ?>
                        </td>
                        <td>
                            <?php
                            $specificationIds = explode(',', $data->specification); 
                            $specifications = [];
                            foreach ($specificationIds as $id) {
                                $specModel = Specification::model()->findByPk(trim($id)); 
                                if ($specModel) {
                                    $specifications[] = CHtml::encode($specModel->specification);
                                }
                            }
                            echo implode(', ', $specifications);
                            ?>
                        </td>
                        <td>
                            <span><?php echo CHtml::encode($data->remarks); ?></span>
                        </td>
                        <td>
                            <span><?php echo CHtml::encode($data->quantity); ?></span>
                        </td>
                        <td>
                            <span><?php echo CHtml::encode($data->used_quantity); ?></span>
                        </td>
                        <td>
                            <?php $bal_qauntity =0;
                                $bal_qauntity =$data->quantity-$data->used_quantity;
                            ?>
                            <span><?php echo CHtml::encode($bal_qauntity ) ;?></span>
                        </td>
                        <td>
                            <span>
                                <?php 
                                $unitModel = Unit::model()->findByPk($data->unit);
                                echo !empty($unitModel) ? CHtml::encode($unitModel->unit_name) : "Unit not found"; 
                                ?>
                            </span>
                        </td>
                        <td>
                            <span><?php echo CHtml::encode($data->rate); ?></span>
                        </td>
                        <td>
                            <span><?php echo CHtml::encode($data->amount); ?></span>
                        </td>
                        <td>
                            <span><?php echo CHtml::encode($data->used_amount); ?></span>
                        </td>
                        <td>
                            <?php $bal_amount =0;
                                $bal_amount =$data->amount-$data->used_amount;
                            ?>
                            <span><?php echo CHtml::encode($bal_amount ) ;?></span>
                        </td>

                        <td style="width: 50px;">
                            <a class="fa fa-edit editProject" data-toggle="modal" data-target=".edit" data-id="<?php echo $data->id; ?>" data-estimation="material_estimation" data-project_id="<?php echo $data['project_id']; ?>"></a>
                            <?php if (empty($data['pms_material_id'])): ?>
                            <a class="fa fa-trash deleteProject" data-toggle="modal" data-target=".delete" data-id="<?php echo $data->id; ?>" data-estimation="material_estimation"></a>
                            <?php endif; ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
            <tfoot>
                <tr style="background-color: #f2f2f2;">
                    <td colspan="9" style="text-align: right;"><strong>Total:</strong></td>
                    <td colspan="1"><strong><?php echo number_format($totalAmount, 2); ?></strong></td>
                    <td colspan="3"></td>
                </tr>
            </tfoot>
        <?php else: ?>
            <tr>
                <td colspan="9">No data found</td>
            </tr>
        <?php endif; ?>
    </table>
</div>

