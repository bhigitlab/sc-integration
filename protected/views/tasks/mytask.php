<?php
/* @var $this TasksController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'Tasks',
);

    ?>
    <h1>My tasks</h1>

    <?php
    $this->widget('zii.widgets.grid.CGridView', array(
        'id' => 'tasks-grid',
        'dataProvider' => $model->assigned_task(),
        'filter' => $model,
		 'afterAjaxUpdate' => 'function() { afterAjaxUpdate(); }',
        'columns' => array(
            array('class' => 'IndexColumn', 'header' => 'Sl.No.',),
            array(
                'name' => 'project_id',
                'value' => '(isset($data->project->name)?$data->project->name:"")',
                'type' => 'raw',
                'filter' => CHtml::listData(Projects::model()->findAll(
                                array(
                                    'select' => array('pid,name'),
                                    'order' => 'name',
                                    'distinct' => true
                        )), "pid", "name")
            ),

            'title',
            array(
                'name' => 'report_to',
                'value' => '$data->reportTo->first_name',
                'type' => 'raw',
                'filter' => CHtml::listData(Users::model()->findAll(
                                array(
                                    'select' => array('userid,first_name'),
                                    'order' => 'first_name',
                                    'distinct' => true
                        )), "userid", "first_name")
            ),
            array(
                'name' => 'coordinator',
                'value' => '$data->coordinator0->first_name',
                'type' => 'raw',
                'filter' => CHtml::listData(Users::model()->findAll(
                                array(
                                    'select' => array('userid,first_name'),
                                    'order' => 'first_name',
                                    'distinct' => true
                        )), "userid", "first_name"),
                'htmlOptions' => array( 'style' => 'border-right:1px solid #c2c2c2;')
            ),
//            'start_date',
//            'due_date',
 array(
            'header' => 'Billable hrs',
            'value' => 'TimeEntry::model()->find(
                            array(
                                "select" => array("sum(hours) as hours"),
                                "condition" => "billable=\"3\" AND tskid=$data->tskid"
                    ))->hours',
            'type' => 'raw',
            'htmlOptions' => array('width' => '40px', 'style' => 'border:1px solid #c2c2c2;font-weight: bold;text-align:center')
        ),
        array(
            'header' => 'Non-billable hrs',
            'value' => 'TimeEntry::model()->find(
                            array(
                                "select" => array("sum(hours) as hours"),
                                "condition" => "billable=\"4\" AND tskid=$data->tskid"
                    ))->hours',
            'type' => 'raw',
            'htmlOptions' => array('width' => '40px', 'style' => 'border:1px solid #c2c2c2;font-weight: bold;text-align:center')
        ),         
        array(
            'header' => 'Total Hours',
            'value' => 'TimeEntry::model()->find(
                            array(
                                "select" => array("sum(hours) as hours"),
                                "condition" => "tskid=$data->tskid"
                    ))->hours',
            'type' => 'raw',
            'htmlOptions' => array('width' => '40px', 'style' => 'border:1px solid #c2c2c2;font-weight: bold;text-align:center')
        ),
        array('name' => 'total_hrs',
               'header' => 'Estimated hrs',
            'htmlOptions' => array('width' => '40px', 'style' => 'border:1px solid #c2c2c2;font-weight: bold;text-align:center')
            ),
            array(
                'name' => 'status',
                'value' => '$data->status0->caption',
                'type' => 'raw',
                'filter' => CHtml::listData(Status::model()->findAll(
                                array(
                                    'select' => array('sid,caption'),
                                    'condition' => 'status_type="task_status"',
                                    'order' => 'status_type',
                                    'distinct' => true
                        )), "sid", "caption")
            ),
            array(
                'class' => 'CButtonColumn',
                'template' => '{view}',
            ),
        ),
    ));
    ?>
	
	<?php Yii::app()->clientScript->registerScript('script', <<<JS
    //$('table tr:nth-child(1) td:nth-child(2)').text();
	afterAjaxUpdate();
JS
, CClientScript::POS_READY);?>

<script type="text/javascript">
function afterAjaxUpdate() {
    var i=0;
	$('table.items > tbody  > tr').each(function() {
		var bcol = $(this).find('td:eq(5)');
		var ecolumn = $(this).find('td:eq(8)');
		var bhrs = parseInt(bcol.html());
		var estihrs = parseInt(ecolumn.html());

		if(isNaN(bhrs)==false && isNaN(estihrs)==false)
		{
			if(bhrs>estihrs)
				ecolumn.css("background-color", "red");
			else if(bhrs<estihrs)
				ecolumn.css("background-color", "#FF9900");
			else if(bhrs==estihrs)
				ecolumn.css("background-color", "#46B525");
		}
	});
}
</script>