<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/message/css/jquery.message.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/message/js/jquery.message.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/moment.js"></script>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.js"></script>

<script>
    var shim = (function () {
        document.createElement('datalist');
    })();
</script>
<style>
    .lefttdiv {
        float: left;
    }
</style>
<script>
    $(function () {
        $("#datepicker").datepicker({
            dateFormat: 'dd-mm-yy'
        }).datepicker("setDate", new Date());

    });
</script>

<div id='actionmsg' style="width: 440px;"></div>
<div class="container">

    <div class="quotationmaindiv">
        <div class="clearfix">
            <div class="pull-left">
                <h2>Revision Listing</h2>
            </div>

        </div><br><br>
        <?php
        $checked ='';
        if(isset($_GET['checked'])){
            $checked ='checked';
        }
        ?>
        <label class="label"> 
            <input type="checkbox" name="checkbox" value="text" class="show_discard" data-qid='<?php echo $_GET['qid'] ?>' <?php echo $checked?>> Show deleted revisions
        </label>
        <table cellpadding="10" class="table">
            <thead>
                <tr>
                    <th>Sl No</th>
                    <th>Quotation No</th>
                    <th>Company</th>
                    <th>Client</th>
                    <th>Date</th>
                    <th>Location</th>
                    <th>Revision No</th>
                    <th>Remarks</th>
                    <th>Grand Total</th>
                    <th>Action</th>
                </tr>
            </thead>

            <?php
            if ($model) {
                $i = 1;
                foreach ($model as $key => $data) {
                    $total_amount_data = SalesQuotation::model()->getAmount($data->id, $data->revision_no);
                    $bg_approved = '';
                    $display = '';
                    if ($quotation_data['revision_no'] == $data->revision_no) {
                        $bg_approved = "background:#90EE90;";
                        $display = "display:none";
                    }
                    $sql = 'SELECT * FROM jp_quotation_revision '
                    . ' WHERE qid='.$data->id
                    . ' AND revision_no="'.$data->revision_no.'"';
                    $revision_model = Yii::app()->db->createCommand($sql)->queryRow();
                    if(!empty($revision_model)){
                    ?>
                    <tr style="<?php echo $bg_approved; ?>">

                        <td><?php echo $i; ?></td>
                        <td><?php echo $data->invoice_no; ?></td>
                        <td> <?php
                            $company = Company::model()->findBypk($data->company_id);
                            echo $company['name'];
                            ?></td>
                        <td><?php echo $data->client_name; ?></td>                        
                        <td>
                            <?php echo date("d-m-Y", strtotime($total_amount_data[0]['created_date'])); ?>
                        </td>
                        <td><?php echo isset($data->location_id) ? $location_data[$data->location_id] : ''; ?> </td>
                        <td><?php echo $data->revision_no; ?></td>
                        <td><?php
                            echo isset($total_amount_data[0]['revision_remarks']) ? $total_amount_data[0]['revision_remarks'] : '';
                            ?></td>
                        <td>
                        <?php
                            $activeProjectTemplate = $this->getActiveTemplate();
                            $sql = 'SELECT * FROM jp_quotation_revision '
                            . ' WHERE qid='.$data->id
                            . ' AND revision_no="'.$data->revision_no.'"';
                            $revision_model = Yii::app()->db->createCommand($sql)->queryRow();
                            $int = (int)$revision_model['total_after_additionl_discount'];
                            if ($data->template_type == '2' || $data->template_type == '5' || ($activeProjectTemplate == 'TYPE-8' && $data->template_type == '1' )) {

                                if($revision_model['total_after_additionl_discount'] !="" && $int > 0){
                                    echo Controller::money_format_inr($revision_model['total_after_additionl_discount'],2);
                                }else{
                                    if($data->template_type == '5'){
                                        $sql = 'SELECT * FROM `jp_quotation_section` '
                                            . 'WHERE `qtn_id` = ' . $data->id
                                            . ' AND revision_no = "' . $data->revision_no . '"';
                                        $sectionArray = Yii::app()->db->createCommand($sql)->queryAll();
                                        $sum = 0;
                                        foreach ($sectionArray as $key => $value1) {
                                            $sql = 'SELECT * FROM `jp_sales_quotation` WHERE `parent_type` = 0 '
                                            . ' AND `parent_id` = ' . $value1['id']
                                            . ' AND revision_no="' . $data->revision_no . '" AND `deleted_status` = 1';
                                        
                                            $itemArray = Yii::app()->db->createCommand($sql)->queryAll();
                                            $sql = 'SELECT * FROM `jp_quotation_gen_category` '
                                            . 'WHERE `qid` = ' . $data->id . ' AND `section_id` = ' . $value1["id"]
                                            . ' AND revision_no = "' . $data->revision_no . '"';
                                            $subsectionArray = Yii::app()->db->createCommand($sql)->queryAll();
                                            foreach ($itemArray as $key => $value) {
                                                $sum += $value['amount_after_discount'];
                                            }
                                            foreach ($subsectionArray as $key => $value) {  
                                                $sum += $value['amount_after_discount'];
                                            }
                                        }
                                        $total_amount = $sum;
                                    }else{                                    
                                        $total_amount = Controller::getQuotationSum($data->id,$data->revision_no,NULL); 
                                    }                                   
                                    $total_tax = $revision_model['sgst_amount']+
                                                $revision_model['cgst_amount']+
                                                $revision_model['igst_amount'];

                                    echo Controller::money_format_inr(($total_amount+$total_tax -$revision_model['discount_amount']),2); 
                                    
                                }
                                // $total_amount = Controller::getQuotationSum($data->id,$data->revision_no,NULL);

                                // $sql = 'SELECT * FROM jp_quotation_revision '
                                //         . ' WHERE qid='.$data->id
                                //         . ' AND revision_no="'.$data->revision_no.'"';
                                // $revision_model = Yii::app()->db->createCommand($sql)->queryRow();
                                // $total_tax = $revision_model['sgst_amount']+
                                //         $revision_model['cgst_amount']+
                                //         $revision_model['igst_amount'];

                                // echo Controller::money_format_inr(($total_amount+$total_tax),2); 
                            } else {
                                $total_amount_data = SalesQuotation::model()->getAmount($data->id,$data->revision_no);
                                echo isset($total_amount_data[0]['maintotal_withtax']) ? $total_amount_data[0]['maintotal_withtax'] : '';

                            }
                                                                                                                      
                            ?>
                            
                        </td>
                        <td>
                            <span class="icon icon-options-vertical popover-test" data-toggle="popover" data-placement="left" type="button" data-html="true" style="cursor: pointer;"></span>
                            <div class="popover-content hide">
                                <ul class="tooltip-hiden">
                                    <li>
                                        <?php echo CHtml::link('View', array('salesQuotation/viewquotation', 'qtid' => $data->id, 'rev_no' => $data->revision_no), array('class' => 'button btn btn-xs btn-default action-btn'));?>
                                    </li>
                                    <li>
                                        <?php
                                         $sql = 'SELECT * FROM jp_quotation_revision '
                                         . ' WHERE qid='.$data->id
                                         . ' AND revision_no="'.$data->revision_no.'"';
                                         $revision_model = Yii::app()->db->createCommand($sql)->queryRow();
                                         if(!empty($revision_model)){
                                            echo CHtml::link('Edit', array('salesQuotation/create', 'qid' => $data->id, 'rev' => $revision_no, 'status' => 1,'selected_revision'=>$data->revision_no), array('class' => 'button btn btn-xs btn-default action-btn'));
                                         }
                                        
                                        ?>
                                    </li>
                                    <?php  if(!empty($revision_model)){ ?>
                                    <li style="<?php echo $display ?>">
                                        <button data-qid="<?php echo $data->id; ?>" data-rev="<?php echo $data->revision_no; ?>" class="add-btn btn btn-xs btn-default action-btn">Approve</button>
                                    </li>
                                    <?php }?>
                                    <li style="<?php echo $display ?>">
                                        <button data-qid="<?php echo $data->id; ?>" data-rev="<?php echo $data->revision_no; ?>" class="discard-btn btn btn-xs btn-default action-btn">Delete</button>
                                    </li>
                                </ul>
                            </div>
                        </td>

                    </tr>
                    <?php
                    }
                    $i++;
                }
            } else {
                ?>
                <tr>
                    <td colspan="11" class="text-center">NO results found</td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
        <div class="clearfix"></div>
        <br><br>
        <div id="msg"></div>
        <div id="previous_details"></div>
    </div>

</div>

<script>
    $(document).on('click', '.add-btn', function (e) {
        e.preventDefault();
        var quotation_id = $(this).attr('data-qid');
        var revision_no = $(this).attr('data-rev');
        $.ajax({
            method: "post",
            dataType: "json",
            data: {
                quotation_id: quotation_id,
                revision_no: revision_no,
            },
            url: '<?php echo Yii::app()->createUrl('salesquotation/salesQuotation/ApproveRevision'); ?>',
            success: function (data) {
                if (data.success == 1) {
                    location.reload();
                } else {
                    $().toastmessage('showSuccessToast', "" + data.msg + "");
                }
            }
        });
    });

    $(document).on("change", "#selectedIds_all", function () {
        if ($(this).is(':checked')) {
            $('input[name="selectedIds[]"]').prop('checked', true);
        } else {
            $('input[name="selectedIds[]"]').prop('checked', false);
        }
    });

    $(document).on("change", ".selectall", function () {
        var checkboxescount = $("table td input[type=checkbox]").length;
        var checkedcount = $("table td input[type=checkbox]:checked").length;
        if (checkboxescount == checkedcount) {
            $('input[name="selectedIds_all"]').prop('checked', true);
        } else {
            $('input[name="selectedIds_all"]').prop('checked', false);
        }
    });

    jQuery.extend(jQuery.expr[':'], {
        focusable: function (el, index, selector) {
            return $(el).is('button, :input, [tabindex]');
        }
    });

    $(document).on('keypress', 'input,select', function (e) {
        if (e.which == 13) {
            e.preventDefault();
            // Get all focusable elements on the page
            var $canfocus = $(':focusable');
            var index = $canfocus.index(document.activeElement) + 1;
            if (index >= $canfocus.length)
                index = 0;
            $canfocus.eq(index).focus();
        }
    });

    $(document).ready(function () {

        $(".popover-test").popover({
            html: true,
            content: function () {
                return $(this).next('.popover-content').html();
            }
        });
        $('[data-toggle=popover]').on('click', function (e) {
            $('[data-toggle=popover]').not(this).popover('hide');
        });
        $('body').on('hidden.bs.popover', function (e) {
            $(e.target).data("bs.popover").inState.click = false;
        });
        $('body').on('click', function (e) {
            $('[data-toggle=popover]').each(function () {
                // hide any open popovers when the anywhere else in the body is clicked
                if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                    $(this).popover('hide');
                }
            });
        });
        $(document).ajaxComplete(function () {
            $(".popover-test").popover({
                html: true,
                content: function () {
                    return $(this).next('.popover-content').html();
                }
            });

        });

    });


    $(document).ready(function () {
        $('select').first().focus();
    });

    $(document).ready(function () {
        $().toastmessage({
            sticky: false,
            position: 'top-right',
            inEffectDuration: 1000,
            stayTime: 3000,
            closeText: '<i class="icon ion-close-round"></i>',
        });
        $(".purchase_items").addClass('checkek_edit');
    });


    $(".allownumericdecimal").keydown(function (event) {
        if (event.shiftKey == true) {
            event.preventDefault();
        }
        if ((event.keyCode >= 48 && event.keyCode <= 57) ||
                (event.keyCode >= 96 && event.keyCode <= 105) ||
                event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 ||
                event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190 || event.keyCode == 110 || event.keyCode == 13) {
            var splitfield = $(this).val().split(".");
            if ((splitfield[1] !== undefined && splitfield[1].length >= 2) && event.keyCode != 8 && event.keyCode != 0 && event.keyCode != 13) {
                event.preventDefault();
            }
        } else {
            event.preventDefault();
        }

        if ($(this).val().indexOf('.') !== -1 && (event.keyCode == 190 || event.keyCode == 110))
            event.preventDefault();

    });

    $("#date").keypress(function (event) {
        if (event.keyCode == 13) {

            if ($(this).val()) {
                $("#inv_no").focus();
            }
        }

    });

    $(".date").keyup(function (event) {
        if (event.keyCode == 13) {
            $(".date").click();

        }
    });

</script>

<style>
    .table-scroll {
        position: relative;
        max-width: 1280px;
        width: 100%;
        margin: auto;
        display: table;
    }

    .table-wrap {
        width: 100%;
        display: block;
        overflow: auto;
        position: relative;
        z-index: 1;
        border: 1px solid #ddd;
    }

    .table-wrap.fixedON,
    .table-wrap.fixedON table,
    .faux-table table {
        height: 380px;
        /* match heights*/
    }

    .table-scroll table {
        width: 100%;
        margin: auto;
        border-collapse: separate;
        border-spacing: 0;
        border: 1px solid #ddd;
    }

    .table-scroll th,
    .table-scroll td {
        padding: 5px 10px;
        border: 1px solid #ddd;
        background: #fff;
        vertical-align: top;
    }

    .faux-table table {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        pointer-events: none;
    }

    .faux-table table tbody {
        visibility: hidden;
    }

    /* shrink cells in cloned table so that the table height is exactly 300px so that the header and footer appear fixed */
    .faux-table table tbody th,
    .faux-table table tbody td {
        padding-top: 0;
        padding-bottom: 0;
        border-top: none;
        border-bottom: none;
        line-height: 0.1;
    }

    .faux-table table tbody tr+tr th,
    .faux-table tbody tr+tr td {
        line-height: 0;
    }

    .faux-table thead th,
    .faux-table tfoot th,
    .faux-table tfoot td,
    .table-wrap thead th,
    .table-wrap tfoot th,
    .table-wrap tfoot td {
        background: #eee;
    }

    .faux-table {
        position: absolute;
        top: 0;
        right: 0;
        left: 0;
        bottom: 0;
        overflow-y: scroll;
    }

    .faux-table thead,
    .faux-table tfoot,
    .faux-table thead th,
    .faux-table tfoot th,
    .faux-table tfoot td {
        position: relative;
        z-index: 2;
    }

    /* ie bug */
    .table-scroll table thead tr,
    .table-scroll table thead tr th,
    .table-scroll table tfoot tr,
    .table-scroll table tfoot tr th,
    .table-scroll table tfoot tr td {
        height: 1px;
    }

    .add_sub_item {
        color: dodgerblue;
        font-weight: bold;
        cursor: pointer;
    }
</style>
<script>
    (function () {
        var mainTable = document.getElementById("main-table");
        if (mainTable !== null) {
            var tableHeight = mainTable.offsetHeight;
            if (tableHeight > 380) {
                var fauxTable = document.getElementById("faux-table");
                document.getElementById("table-wrap").className += ' ' + 'fixedON';
                var clonedElement = mainTable.cloneNode(true);
                clonedElement.id = "";
                fauxTable.appendChild(clonedElement);
            }
        }
    })();
    $('.sub_item_sec').hide();
    $('.add_sub_item').click(function () {
        $(this).siblings('.sub_item_sec').slideToggle();
    });
    
    $(document).on('click', '.discard-btn', function (e) {
        e.preventDefault();
        var quotation_id = $(this).attr('data-qid');
        var revision_no = $(this).attr('data-rev');
        $.ajax({
            method: "post",
            dataType: "json",
            data: {
                quotation_id: quotation_id,
                revision_no: revision_no,
            },
            url: '<?php echo Yii::app()->createUrl('salesquotation/salesQuotation/DiscardRevision'); ?>',
            success: function (data) {
                if (data.success == 1) {
                    location.reload();
                } else {
                    $().toastmessage('showSuccessToast', "" + data.msg + "");
                }
            }
        });
    });

    $('.show_discard').click(function(){    
        var quotation_id = $(this).attr('data-qid');    
        var checked = '';
        if($(this).is(':checked')) {
            var checked = '&checked=checked';          
        } 
        location.href = '<?php echo Yii::app()->createUrl('salesquotation/salesQuotation/revisions&qid='); ?>'+quotation_id+checked;
        
      }); 
</script>
<style>
    .label{
        color:#555;
        line-height:2;
        font-size:12px;
    }
</style>