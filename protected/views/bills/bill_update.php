<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.js"></script>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/message/css/jquery.message.css">
<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/purchase.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/message/js/jquery.message.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/moment.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/tableHeadFixer.js"></script>

<script>
	var shim = (function() {
		document.createElement('datalist');
	})();
</script>
<script>
	$(function() {
		$("#datepicker").datepicker({
			dateFormat: 'dd-mm-yy'
		}).datepicker("setDate", new Date());

	});
</script>
<?php
$tblpx = Yii::app()->db->tablePrefix;
$addchargesall = Yii::app()->db->createCommand("SELECT * FROM " . $tblpx . "additional_bill WHERE bill_id=" . $model->bill_id)->queryAll();
$addcharges = Yii::app()->db->createCommand("SELECT SUM(amount) as amount FROM " . $tblpx . "additional_bill WHERE `bill_id`=" . $model->bill_id)->queryRow();
?>
<style>
	.dropdown {
		position: relative;
		display: inline-block;
	}

	.dropdown::before {
		position: absolute;
		content: " \2193";
		top: 0px;
		right: -8px;
		height: 20px;
		width: 20px;
	}

	button#caret {
		border: none;
		background: none;
		position: absolute;
		top: 0px;
		right: 0px;
	}

	.addcolumn {
		cursor: pointer;
		background-color: black;
		border: none;
		color: white;
		text-align: center;
		text-decoration: none;
		margin: 2px;
		font-size: 30px;
		width: 40px;
	}

	.toast-container {
		width: 350px;
	}

	.toast-position-top-right {
		top: 57px;
		right: 6px;
	}

	.toast-item-close {
		background-image: none;
		cursor: pointer;
		width: 12px;
		height: 12px;
		text-align: center;
		border-radius: 2px;
	}

	.toast-item-image {
		font-size: 24px;
	}

	.toast-item-close:hover {
		color: red;
	}

	.toast-item {
		border: transparent;
		border-radius: 3px;
		font-size: 10px;
		opacity: 1;
		background-color: rgba(34, 45, 50, 0.8);
	}

	.toast-item-wrapper p {
		margin: 0px 5px 0px 42px;
		font-size: 14px;
		text-align: justify;
	}


	.toast-type-success {
		background-color: #00A65A;
		border-color: #00A65A;
	}

	.toast-type-error {
		background-color: #DD4B39;
		border-color: #DD4B39;
	}

	.toast-type-notice {
		background-color: #00C0EF;
		border-color: #00C0EF;
	}

	.toast-type-warning {
		background-color: #F39C12;
		border-color: #F39C12;
	}

	.remark {
		display: none;
	}

	.quantity,
	.rate,
	.small_class {
		max-width: 100%;
	}

	.amt_sec {
		float: right;
	}

	.amt_sec:after,
	.padding-box:after {
		display: block;
		content: '';
		clear: both;
	}

	.mb-1 {
		margin-bottom: 5px;
	}


	*:focus {
		border: 1px solid #333;
		box-shadow: 0 0 6px 1px rgba(0, 0, 0, 0.25);
	}

	#parent,
	#parent2 {
		max-height: 150px;
	}

	.tooltip-hiden {
		width: auto;
	}

	@media(max-width: 767px) {

		.quantity,
		.rate,
		.small_class {
			width: auto !important;
		}


		#tax_amount,
		#item_amount {
			display: inline-block;
			float: none;
			text-align: right;
		}

		/* span.select2.select2-container.select2-container--default.select2-container--focus {
			width: auto !important;
		}

		span.select2.select2-container.select2-container--default.select2-container--below {
			width: auto !important;
		} */
	}

	@media(min-width: 768px) {
		.invoicemaindiv .pull-right {
			float: right !important;
		}

		.invoicemaindiv .pull-left {
			float: left !important;
		}

		.main-form span.select2.select2-container,.main-form span.select2.select2-container.select2-container--default.select2-container--focus,.main-form .form-control {
			width: 190px !important;
		}

		.main-form span.select2.select2-container.select2-container--default.select2-container--below {
			width: 190px !important;
		}
	}

	.pre_content {
		max-height: 100px;
		overflow-y: auto;
	}

	.pre_content p {
		font-size: 14px;
	}

	.select2-dropdown {
		z-index: 1;
	}

	.extracharge {
		float: left;
	}

	.extracharge h4 {
		position: relative;
	}

	.addcharge {
		cursor: pointer;
	}

	.extracharge div {
		margin-bottom: 10px;
	}

	.addRow input.textbox {
		width: 200px;
		display: inline-block;
		margin-right: 10px;
	}

	.delbil,
	.deleteitem {

		color: #000;
		background-color: #eee;
		padding: 3px;
		cursor: pointer;
	}

	.addRow label {
		display: inline-block;
	}

	.base {
		display: none;
	}
</style>
<?php
$general_settings_for_auto_receipt = Controller::generalSettingsForAutoReceiptToProjectWarehouse($settings = 1);
?>
<div class="container">
	<div class="loading-overlay">
		<span class="fa fa-spinner fa-3x fa-spin"></span>
	</div>

	<div class="invoicemaindiv">
		<h2 class="purchase-title">Bill</h2>
		<div id="loading"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/ajax-loader.gif"></div>
		<div id="msg_box"></div>
		<form id="pdfvals1" method="post" action="<?php echo $this->createAbsoluteUrl('purchase/addpurchase'); ?>">
			<input type="hidden" name="remove" id="remove" value="">
			<input type="hidden" name="bill_id" id="bill_id" value="<?php echo $model->bill_id; ?>">
			<input type="hidden" name="purchase_id" id="purchase_id" value="<?php echo $model->purchase_id; ?>">
			<div class="block_purchase">
				<div class="row main-form ">
					<div class="col-sm-4 col-md-3 col-lg-2">
						<div class="form-group">
							<label>COMPANY : </label>
							<?php
							$user = Users::model()->findByPk(Yii::app()->user->id);
							$arrVal = explode(',', $user->company_id);
							$newQuery = "";
							foreach ($arrVal as $arr) {
								if ($newQuery) $newQuery .= ' OR';
								$newQuery .= " FIND_IN_SET('" . $arr . "', id)";
							}
							$companyInfo = Company::model()->findAll(array('condition' => $newQuery));
							?>
							<select name="company_id" class="inputs target company_id" id="company_id" >
								<option value="">Choose Company</option>
								<?php
								foreach ($companyInfo as $key => $value) {
								?>
									<option value="<?php echo $value['id']; ?>" <?php echo ($value['id'] == $model->company_id) ? 'selected' : ''; ?>><?php echo $value['name']; ?></option>
								<?php
								}
								?>

							</select>
						</div>
					</div>
					<div class="col-sm-4 col-md-3 col-lg-2">
						<div class="form-group">
							<label>PROJECT : </label>
							<select name="project" class="inputs target project" id="project">
								<!-- <option value="">Choose Project</option> -->
								<?php
								foreach ($project as $key => $value) {
								?>
									<option value="<?php echo $value['pid']; ?>" <?php echo ($value['pid'] == $newmodel->project_id) ? 'selected' : ''; ?>><?php echo $value['name']; ?></option>
								<?php
								}
								?>

							</select>
						</div>
					</div>
					<div class="col-sm-4 col-md-3 col-lg-2">
						<div class="form-group">

							<label>EXPENSE HEAD : </label>
							<select name="expense_head" class="inputs target expense_head" id="expense_head">
								<option value="">Choose Expense Head</option>
								<?php
								foreach ($expense_head as $value) {
								?>
									<option value="<?php echo $value['type_id']; ?>" <?php echo ($value['type_id'] == $newmodel->expensehead_id) ? 'selected' : ''; ?>><?php echo $value['type_name']; ?></option>
								<?php
								}
								?>
							</select>
						</div>
					</div>
					<div class="col-sm-4 col-md-3 col-lg-2">
						<div class="form-group">

							<label>VENDOR : </label>
							<select name="vendor" class="inputs target vendor" id="vendor" >
								<option value="">Choose Vendor</option>
								<?php
								foreach ($vendor as $key => $value) {
								?>
									<option value="<?php echo $value['vendorid']; ?>" <?php echo ($value['vendorid'] == $newmodel->vendor_id) ? 'selected' : ''; ?>><?php echo $value['vendorname']; ?></option>
								<?php
								}
								?>

							</select>
						</div>
					</div>
					<div class="col-sm-4 col-md-3 col-lg-2">
						<div class="form-group">
							<label>DATE : </label>
							<input type="text" value="<?php echo ((isset($model->bill_date) && $model->bill_date != '') ? date("d-m-Y", strtotime($model->bill_date)) : date("d-m-Y")); ?>" id="datepicker" class="txtBox date inputs target form-control" name="date" placeholder="Please click to edit" >
						</div>
					</div>
					<div class="col-sm-4 col-md-3 col-lg-2">
						<div class="form-group">
							<label for="warehousestock_warehouseid" class="required">Warehouse
								<!-- <span class="required">*</span> -->
							</label>
							<?php
							if (yii::app()->user->role == 5) {
								$warehouse_data = Controller::getCategoryOptions($type = 1);
								$data = CHtml::listData(Controller::getCategoryOptions($type = 1), 'id', 'text', 'group');
							} else {
								$warehouse_data = Controller::getCategoryOptions();
								$data = CHtml::listData(Controller::getCategoryOptions(), 'id', 'text', 'group');
							}
							if (count($warehouse_data) == 1) {
								$warehouse_selected = $warehouse_data[0]['id'];
							} else {
								$warehouse_selected = $model['warehouse_id'];
							}
							?>
							<?php echo CHtml::activeDropDownList($model2, 'warehousestock_warehouseid', $data, array(
								'empty' => '-Choose a Warehouse-',
								'class' => 'form-control mandatory',
								'options' => array($warehouse_selected => array('selected' => true))
							)); ?>

							<div class="errorMessage" id="Warehousestock_warehousestock_warehouseid_em_" style='display:none;'></div>

						</div>
					</div>
					<div class="col-sm-4 col-md-3 col-lg-2">
						<div class="form-group">
							<label>PURCHSE BILL NO : </label>
							<input type="text" required value="<?php echo ((isset($model->bill_number) && $model->bill_number != '') ? $model->bill_number : ''); ?>" class="txtBox inputs target check_type purchaseno form-control" name="bill_number" id="bill_number" placeholder="" >
						</div>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			<div id="msg"></div>

			<div id="previous_details" style="position: fixed;top: 94px; left: 70px; right: 70px; z-index: 2; max-width:1150px; margin:0 auto;"></div>

			<div class="bill_items shdow_box panel panel-default">
				<div class="panel-heading">
					<h3>Bill Items</h3>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-md-9">
							<div class="row">
								<div class="purchaseitem col-sm-4 col-lg-3">
									<div id="select_div" class="form-group">
										<label>Specification:</label>
										<select class="txtBox specification description " style='width:200px;' id="description" name="description[]">
											<option value="">Select one</option>
											<?php
											foreach ($specification as $key => $value) {
											?>
												<option value="<?php echo $value['id']; ?>"><?php echo $value['data']; ?></option>
											<?php } ?>
											<option value="other">Other</option>
										</select>
									</div>
								</div>
								<div class="purchaseitem remark col-sm-4 col-lg-3">
									<div class="form-group">
										<label>Remarks:</label>
										<input type="text" class="txtBox " id="remarks" name="remark[]" placeholder="Remark" />
									</div>
								</div>

								<input type="hidden" name="purchaseitem_unit_hidden" id="purchaseitem_unit_hidden" />
								<div class="purchaseitem quantity_div col-sm-4 col-lg-2">
									<div class="form-group">
										<label>Quantity:</label>
										<input type="text" class="inputs target txtBox quantity biquantity allownumericdecimal form-control" id="purchase_quantity" name="purchase_quantity" placeholder="" /></td>
									</div>
								</div>

								<div class="purchaseitem col-sm-4 col-lg-2">
									<div class="form-group">
										<label>Rate:</label>
										<input type="text" class="inputs target txtBox rate  birate allownumericdecimal form-control" id="purchase_rate" name="purchase_rate" placeholder="" />
									</div>
								</div>

								<div class="purchaseitem batch_div col-sm-4 col-lg-2">
									<label>Batch:</label>
									<input type="text" class="inputs target txtBox bibatch form-control" id="bibatch" name="bibatch" placeholder="" /></td>
								</div>
								<div class="purchaseitem col-sm-2 col-lg-1">
									<div class="form-group">
										<label>HSN Code:</label>
										<div id="hsn_code" class="hsn_code padding-box">&nbsp;&nbsp;</div>
									</div>
								</div>
								<div class="purchaseitem col-md-2 form-group">
									<label>Units:</label>
									<?php
									$purchase_unit = '';
									$options = CHtml::listData(Unit::model()->findAll(), 'unit_name', 'unit_name');
									echo CHtml::dropDownList('purchaseitem_unit', $purchase_unit, $options, array('empty' => 'Select Unit', 'class' => 'inputs target txtBox  allownumericdecimal form-control'));
									?>
								</div>
								<div class="purchaseitem quantity_div base  col-sm-4 col-lg-2   form-group">
									<label>Base Quantity:</label>
									<input type="text" class="inputs target txtBox quantity allownumericdecimal form-control" id="quantity" name="quantity" placeholder="" /></td>
								</div>
								<div class="purchaseitem base  col-sm-4 col-lg-2  form-group">
									<label>Base Units:</label>
									<input type="text" class="inputs target txtBox quantity allownumericdecimal form-control" id="item_unit" name="purchasebaseitem_unit" placeholder="" readonly="true" />
								</div>
								<div class="purchaseitem base  col-sm-4 col-lg-2  form-group">
									<label>Base Rate:</label>
									<input type="text" class="inputs target txtBox rate allownumericdecimal form-control" id="rate" name="rate" placeholder="" readonly=true />
								</div>
								<div class="purchaseitem gsts  col-sm-4 col-lg-2 mobile-margin-bottom-0 tab-margin-bottom-0  form-group">
									<label>Dis amount :</label>
									<input type="text" class="inputs target small_class txtBox sgst allownumericdecimal form-control calculation" id="dis_amount" name="dis_amount" placeholder="" />
									<div id="disp" class="padding-box">0.00</div>(%)
								</div>
									<div class="clearfix d-tab-block"></div>
								<div class="purchaseitem col-sm-2 col-lg-1 tab-margin-bottom-20 padding-left-5-15  form-group mobile-margin-bottom-0">
									<div id="select_div" class="select-div-full">
										<label>Tax Slab:</label>
										<?php
										$datas = TaxSlabs::model()->getAllDatas();
										?>
										<select class="txtBox tax_slab" style='width:100px;' id="tax_slab" name="tax_slab[]">
											<option value="">Select one</option>
											<?php
											foreach ($datas as  $value) {
												if ($value['set_default'] == 1) {
													echo ' <option data-id="' . $value['id'] . '" value="' . $value['tax_slab_value']  . '" selected>' . $value['tax_slab_value'] . '%</option>';
												} else {
													echo ' <option  data-id="' . $value['id'] . '" value="' . $value['tax_slab_value'] . '">' . $value['tax_slab_value'] . '%</option>';
												}
											}
											?>
										</select>
											<span class="d-mobile-block">&nbsp;</span>
									</div>
								</div>
								<div class="purchaseitem gsts col-sm-2 mobile-margin-bottom-0">
									<label>SGST :</label>
									<input type="text" class="inputs target small_class txtBox sgstp allownumericdecimal form-control calculation" id="sgstp" name="sgstp" placeholder="" />
									<div id="sgst_amount" class="padding-box">0.00</div>
								</div>

								<div class="purchaseitem gsts col-sm-2 mobile-margin-bottom-0">
									<label>CGST :</label>
									<input type="text" class="inputs target txtBox cgstp small_class allownumericdecimal form-control calculation" id="cgstp" name="cgstp" placeholder="" />
									<div id="cgst_amount" class="padding-box">0.00</div>
								</div>

								<div class="purchaseitem gsts col-sm-2 mobile-margin-bottom-0">
									<label>IGST :</label>
									<input type="text" class="inputs target txtBox igstp  small_class allownumericdecimal form-control calculation" id="igstp" name="igstp" placeholder="" />
									<div id="igst_amount" class="padding-box">0.00</div>
								</div>
							</div>
						</div>
						<div class="col-md-3 text-right">
							<div class="mb-1">
								<b>Amount: </b>
								<div id="item_amount" class="padding-box">0.00</div>
							</div>
							<div class="mb-1">
								<b>Discount Amount: </b>
								<div id="disc_amount" class="padding-box">0.00</div>
							</div>
							<div class="mb-1">
								<b>Tax Amount: </b>
								<div id="tax_amount" class="padding-box">0.00</div>
							</div>
							<div class="mb-1">
								<b>Total Amount: </b>
								<div id="total_amount" class="padding-box">0.00</div>
							</div>
						</div>
					</div>
					<div class="text-right">
						<input type="button" class="item_save btn btn-info" id="0" value="Save">
					</div>
				</div>
			</div>
	</div>


	<div id="table-scroll" class="table-scroll">
		<div id="faux-table" class="faux-table" aria="hidden"></div>
		<div id="table-wrap" class="table-wrap">
			<div class="table-responsive">
				<table border="1" class="table">
					<thead>
						<tr>
							<th>Sl.No</th>
							<th>Description</th>
							<th>Quantity</th>
							<th>Unit</th>
							<th>Rate</th>
							<th>Batch</th>
							<th>HSN Code</th>
							<th>Base Quantity</th>
							<th>Base Unit</th>
							<th>Base Rate</th>
							<th>Tax Slab (%)</th>
							<th>SGST Amount</th>
							<th>SGST (%)</th>
							<th>CGST Amount</th>
							<th>CGST (%)</th>
							<th>IGST Amount</th>
							<th>IGST (%)</th>
							<th>Discount (%)</th>
							<th>Discount Amount</th>
							<th>Amount</th>
							<th>Tax Amount</th>
							<th></th>
						</tr>
					</thead>
					<tbody class="addrow">
						<?php
						$tblpx = Yii::app()->db->tablePrefix;

						$data1 = Yii::app()->db->createCommand("SELECT SUM(billitem_amount) as bill_amount FROM {$tblpx}billitem WHERE bill_id=" . $model->bill_id . "")->queryRow();
						$data2 = Yii::app()->db->createCommand("SELECT SUM(billitem_taxamount) as bill_taxamount FROM {$tblpx}billitem WHERE bill_id=" . $model->bill_id . "")->queryRow();
						$data3 = Yii::app()->db->createCommand("SELECT SUM(billitem_discountamount) as bill_discountamount FROM {$tblpx}billitem WHERE bill_id=" . $model->bill_id . "")->queryRow();

						$addcharges = Yii::app()->db->createCommand("SELECT SUM(amount) as amount FROM " . $tblpx . "additional_bill WHERE `bill_id`=" . $model->bill_id)->queryRow();

						$roundoff = Yii::app()->db->createCommand("SELECT round_off as round_off FROM {$tblpx}bills WHERE bill_id=" . $model->bill_id . "")->queryRow();

						$sql = "SELECT SUM(billitem_sgst)+SUM(billitem_cgst)+SUM(billitem_igst) "
								. " AS total_tax FROM `jp_billitem` "
								. " WHERE bill_id=". $_GET['id'];
						$total_tax = Yii::app()->db->createCommand($sql)->queryScalar();
					
						$net_amount = ($data1['bill_amount'] + $total_tax ) - $data3['bill_discountamount'];

						$grand_total = ($net_amount + + $addcharges['amount']) + $roundoff['round_off'];


						foreach ($item_model as $key => $values) {
							$tblpx = Yii::app()->db->tablePrefix;
							$desc_sql = "SELECT * FROM {$tblpx}specification "
								. " WHERE id=" . $values['category_id'] . "";
							$desctiptions  = Yii::app()->db->createCommand($desc_sql)->queryRow();
							$parent_sql = "SELECT * FROM {$tblpx}purchase_category "
								. " WHERE id='" . $desctiptions['cat_id'] . "'";
							$parent_category = Yii::app()->db->createCommand($parent_sql)->queryRow();
							if ($desctiptions['brand_id'] != NULL) {
								$brand_details = Yii::app()->db->createCommand("SELECT brand_name FROM {$tblpx}brand WHERE id=" . $desctiptions['brand_id'] . "")->queryRow();
								$brand = '-' . ucwords($brand_details['brand_name']);
							} else {
								$brand = '';
							}
							if ($values['category_id'] == 0) {
								if ($values['remark'] != '') {
									$spc_details = $values['remark'];
								} else {
									$spc_details = $values['billitem_description'];
								}
								$spc_id = 'other';
							} else {
								$spc_details = ucwords($parent_category['category_name']) . $brand . ' - ' . ucwords($desctiptions['specification']);
								$spc_id = $values['category_id'];
							}
						?>
							<tr>
								<td>
									<div id="item_sl_no"><?php echo ($key + 1); ?></div>
								</td>
								<td>
									<div class="item_description" id="<?php echo $spc_id; ?>"> <?php echo $spc_details; ?></div>
								</td>
								<td class="text-right">
									<div class="" id="unit"> <?php echo $values['billitem_quantity']; ?></div>
								</td>
								<td>
									<div class="unit" id="unit"> <?php echo $values['billitem_unit']; ?></div>
								</td>
								<td class="text-right">
									<div class="" id="rateval"><?php echo number_format($values['billitem_rate'], 2, '.', ''); ?></div>
								</td>
								<td>
									<div class="batch" id="batch"> <?php echo $values['batch']; ?></div>
								</td>
								<td>
									<div class="hsncode" id="hsncode"> <?php echo $values['billitem_hsn_code']; ?></div>
								</td>
								<td>
									<div class="po_quantity" id="po_quantity"> <?php echo $values['purchaseitem_quantity'] ?></div>
								</td>
								<td>
									<div class="po_unit" id="po_unit"> <?php echo $values['purchaseitem_unit'] ?></div>
								</td>
								<td>
									<div class="po_rate" id="po_rate"><?php echo $values['purchaseitem_rate'] ?></div>
								</td>

								<td class="text-right"><?php echo $values['billitem_taxslab']; ?></td>
								<td class="text-right"><?php echo number_format($values['billitem_sgst'], 2); ?></td>
								<td class="text-right"><?php echo number_format($values['billitem_sgstpercent'], 2, '.', ''); ?></td>
								<td class="text-right"><?php echo number_format($values['billitem_cgst'], 2); ?></td>
								<td class="text-right"><?php echo number_format($values['billitem_cgstpercent'], 2, '.', ''); ?></td>
								<td class="text-right"><?php echo number_format($values['billitem_igst'], 2); ?></td>
								<td class="text-right"><?php echo number_format($values['billitem_igstpercent'], 2, '.', ''); ?></td>
								<td class="text-right">
									<div class="" id="rate"><?php echo number_format($values['billitem_discountpercent'], 2, '.', ''); ?></div>
								</td>
								<td class="text-right">
									<div class="" id="rate"><?php echo number_format($values['billitem_discountamount'], 2, '.', ''); ?></div>
								</td>
								<td class="text-right"><?php echo number_format($values['billitem_amount'], 2, '.', ''); ?></td>
								<td class="text-right"><?php echo number_format($values['billitem_taxamount'], 2, '.', ''); ?></td>
								<td width="70"><span class="icon icon-options-vertical popover-test" data-toggle="popover" data-placement="left" type="button" data-html="true" style="cursor: pointer;"></span>
									<div class="popover-content hide">
										<ul class="tooltip-hiden">
											<li><a href="#" id=" <?php echo $values['billitem_id']; ?>" class="btn btn-xs btn-default removebtn">Delete</a></li>
											<li><a href="" id="<?php echo $values['billitem_id']; ?>" class="btn btn-xs btn-default edit_item">Edit</a></li>
										</ul>
									</div>
								</td>
							</tr>
						<?php } ?>
					</tbody>
					<tfoot>
						<tr>
							<th colspan="18" class="text-right">TOTAL: </th>
							<th class="text-right">
								<div id="discount_total"><?php echo ($data3['bill_discountamount'] != '') ? number_format($data3['bill_discountamount'], 2) : '0.00'; ?></div>
							</th>
							<th class="text-right">
								<div id="amount_total"><?php echo ($data1['bill_amount'] != '') ? number_format($data1['bill_amount'], 2) : '0.00'; ?>
							</th>
							<th class="text-right">
								<div id="tax_total">
									<?php 
									echo ($total_tax != '') ? number_format($total_tax, 2) : '0.00'; 
									?>
							</th>
							<th></th>
						</tr>
						<tr>
							<th colspan="18" class="text-right">GRAND TOTAL:
							</th>
							<th colspan="4" class="text-right">
								<div id="grand_total"><?php echo ($net_amount != '') ? number_format($net_amount, 2) : '0.00'; ?></div>
							</th>

						</tr>
						<tr>
							<th colspan="18" class="text-right">ADDITIONAL CHARGE : </th>
							<th colspan="4" class="text-right">
								<div class="additional_total">

									<span id="totaladditional_charge"><?= (($addcharges['amount'] != '')) ? Controller::money_format_inr(round($addcharges['amount'], 2), 2) : 0; ?></span>
								</div>
							</th>
						</tr>
						<tr>
							<th colspan="18" class="text-right">ROUND OFF : </th>
							<th colspan="4" class="text-right">
								<input type="text" name="bill_round_off" class="text-right" id="bill_round_off" value="<?php echo $model['round_off'] != '' ? $model['round_off'] : 0; ?>">
							</th>
						</tr>
						<tr>
							<th colspan="18" class="text-right">NET AMOUNT: </th>
							<th colspan="4" class="text-right">
								<input type="text" name="grand_total" class="text-right grandtotal" id="grand_total" value="<?php echo ($grand_total != '') ? Yii::app()->controller->money_format_inr(round($grand_total, 2), 2) : 0; ?>" style="border: none !important;background-color: #eeeeee;" readonly=true>
							</th>
						</tr>


					</tfoot>
				</table>
			</div>
		</div>
	</div>
	<div style="padding-right: 0px;" class="col-md-offset-9 col-md-3 col-sm-offset-8 col-sm-4 col-xs-offset-6 col-xs-6">

		<input type="hidden" value="<?php echo ((isset($subtot) && $subtot != '') ? $subtot : ''); ?>" class="txtBox pastweek" readonly=ture name="subtot" /></td>
		<input type="hidden" value="<?php echo ((isset($grand) && $grand != '') ? $grand : ''); ?>" class="txtBox pastweek grand" name="grand" readonly=true />
	</div>
	</form>
	<div class="addRow clearfix">
		<div class="extracharge">
			<h4>Additional Charges <span class="icon icon-plus addcharge"></span></h4>
			<?php if (!empty($addchargesall)) {
				foreach ($addchargesall as $data) { ?>
					<div class="display_additional"> <span style="font-weight: bold;"><?= $data['category'] ?></span> : <span class='totalprice1'><?= Controller::money_format_inr($data['amount'], 2) ?></span>&nbsp;&nbsp;<span class='icon icon-trash deleteitem' title='Delete' id="<?= $data['id'] ?>"></span></div>
			<?php }
			} ?>

			<span class="errorcharge" style="color: red;"></span>
			<div class="chargeadd  box_holder" style="display:none;">
				<input type="text" placeholder="Charge Category" name="packagecharge" id="labelcat" class="form-control textbox" />
				<input type="number" placeholder="Enter Amount" name="packagecharge" id="currentval" class="form-control textbox currentval" />
				<input type="button" class=" btn btn-sm btn-primary savecharge" value="save" />
			</div>
			<div id="addtional_list"></div>
		</div>
	</div>
	<br><br>
    <div class="row addRow">
        <div class="col-sm-12 text-center"><?php echo CHtml::Button($model->isNewRecord ? 'Submit' : 'Save', array('class' => 'buttonsubmit btn btn-default', 'id' => 'buttonsubmit', 'disabled' => true)); ?></div>
    </div>
</div>
<input type="hidden" name="final_amount" id="final_amount" value="0">

<input type="hidden" name="item_amount_temp" id="item_amount_temp" value="0">

<input type="hidden" name="final_tax" id="final_tax" value="0">

<input type="hidden" name="item_tax_temp" id="item_tax_temp" value="0">


<style>
	.error_message {
		color: red;
	}

	a.pdf_excel {
		background-color: #6a8ec7;
		display: inline-block;
		padding: 8px;
		color: #fff;
		border: 1px solid #6a8ec8;
	}
</style>
<?php $addbillurl = Yii::app()->createAbsoluteUrl("bills/additionalbill1"); ?>
<?php $delbillurl = Yii::app()->createAbsoluteUrl("bills/deletebill2"); ?>


<script>
	$(document).ready(function() {
		$('#loading').hide();
		$('.addcharge').click(function() {
			$(".chargeadd").toggle();
			$(this).toggleClass("icon-minus hidecharge");

		});
		$(".savecharge").on('keyup keypress keydown click', function(e) {
			var label = $("#labelcat").val();
			var currentval = $("#currentval").val();
			var billid = $("#bill_id").val();
			var totalamount = parseFloat($(".grand_total").val()).toFixed(2);
			var addtot = parseFloat($("#totaladditional_charge").text());

			if (billid == '' || totalamount == '0.00') {

				$('.errorcharge').html('Please add item');

			} else if (label != '' && currentval != '') {
				$('.loading-overlay').addClass('is-active');
				$(".savecharge").attr('disabled', 'disabled');
				$.ajax({
					url: "<?php echo $addbillurl; ?>",
					data: {
						category: label,
						amount: currentval,
						billid: billid,
						totalamount: totalamount
					},
					type: "POST",
					dataType: 'json',
					success: function(data) {
						$(".savecharge").removeAttr('disabled');
						if (data != '') {
							$("#addtional_list").append("<div class='display_additional'>" + label + " : " + "<span class='totalprice'>" + parseFloat(currentval).toFixed(2) + "</span> &nbsp;&nbsp&nbsp;<span class='icon icon-trash deleteitem' title='Delete' id=" + data.itemid + "></span> " + "</div>");
							$(".additional_total").show();
							$('#labelcat').val('');
							$('#currentval').val('');
							if (isNaN(addtot)) {
								var totaladd = parseFloat(currentval);
							} else {
								var totaladd = parseFloat(addtot) + parseFloat(currentval);
							}

							$('.additional_charge').show();
							$("#totaladditional_charge").text(totaladd);
							var tot = parseFloat(currentval) + parseFloat(totalamount);
							$(".grandtotal").val(data.amount.toFixed(2));
						}
					}
				});
			} else {

				if (label == '' || currentval == '') {
					$('.errorcharge').html('Both fields are required');
				}

			}
		});
		$(document).on("click", ".deleteitem", function(event) {
			var billid = $("#bill_id").val();
			$(this).parent().remove();
			var id = $(this).attr('id');
			$('.loading-overlay').addClass('is-active');
			$.ajax({
				url: "<?php echo $delbillurl; ?>",
				data: {
					id: id,
					billid: billid
				},
				type: "POST",
				dataType: 'json',
				success: function(data) {
					$("#totaladditional_charge").text(data.additional);
					$(".grandtotal").val(data.bill_amount);
				}
			});
		});

		$(".popover-test").popover({
			html: true,
			content: function() {
				return $(this).next('.popover-content').html();
			}
		});
		$('[data-toggle=popover]').on('click', function(e) {
			$('[data-toggle=popover]').not(this).popover('hide');
		});
		$('body').on('hidden.bs.popover', function(e) {
			$(e.target).data("bs.popover").inState.click = false;
		});
		$('body').on('click', function(e) {
			$('[data-toggle=popover]').each(function() {
				if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
					$(this).popover('hide');
				}
			});
		});
		$(document).ajaxComplete(function() {
			$(".popover-test").popover({
				html: true,
				content: function() {
					return $(this).next('.popover-content').html();
				}
			});

		});
		$(".js-example-basic-single").select2();
		$(".project").select2();
		$(".vendor").select2();
		$(".expense_head").select2();
		$(".company_id").select2();
		$("#tax_slab").select2();

	});
	$(document).on("change", "#bill_round_off", function(event) {

		var bill_id = $('#bill_id').val();
		var roundOff = parseFloat(this.value);
		var additional_amount = parseFloat($('#totaladditional_charge').text());
		if (isNaN(additional_amount)) additional_amount = 0;
		if (isNaN(roundOff)) roundOff = 0;
		if (bill_id !== '') {
			$('.loading-overlay').addClass('is-active');
			$.ajax({
				url: "<?php echo Yii::app()->createAbsoluteUrl("bills/updatebillamount"); ?>",
				data: {
					'Bills[round_off]': roundOff,
					bill_id: bill_id
				},
				type: "POST",
				dataType: 'json',
				success: function(data) {
					if (data.status === 1) {
						var bill_totalamount = data.values['bill_totalamount'] + additional_amount;
						$('#grand_total').val(parseFloat(bill_totalamount).toFixed(2));
						$().toastmessage('showSuccessToast', "Total amount updated successfully");
						$(".table").load(window.location.href + " .table")
					} else {
						$().toastmessage('showErrorToast', "Error occured");
					}

				}
			});
		}

	});
	jQuery.extend(jQuery.expr[':'], {
		focusable: function(el, index, selector) {
			return $(el).is('button, :input, [tabindex]');
		}
	});

	$(document).on('keypress', 'input,select', function(e) {
		if (e.which == 13) {
			e.preventDefault();
			var $canfocus = $(':focusable');
			var index = $canfocus.index(document.activeElement) + 1;
			if (index >= $canfocus.length) index = 0;
			$canfocus.eq(index).focus();
		}
	});

	$("#expense_head").change(function() {
		var val = $(this).val();
		$("#vendor").html('<option value="">Select Vendor</option>');
		$('#loading').show();
		$.ajax({
			url: '<?php echo Yii::app()->createUrl('purchase/dynamicvendor'); ?>',
			method: 'POST',
			data: {
				exp_id: val
			},
			dataType: "json",
			success: function(response) {
				$("#vendor").html(response.html);
			}
		})
	})



	$("#project").change(function() {
		var val = $(this).val();
		$("#vendor").html('<option value="">Select Vendor</option>');
		$("#expense_head").html('<option value="">Select Expense Head</option>');
		$('#loading').show();
		$.ajax({
			url: '<?php echo Yii::app()->createUrl('purchase/dynamicexpensehead'); ?>',
			method: 'POST',
			data: {
				project_id: val
			},
			dataType: "json",
			success: function(response) {
				if (response.status == 'success') {
					$("#expense_head").html(response.html);
					$("#vendor").html('<option value="">Select Vendor</option>');
				} else {
					$("#expense_head").html(response.html);
					$("#vendor").html('<option value="">Select Vendor</option>');
				}
			}
		})
	})

	$(document).ready(function() {
		$('select').first().focus();
		$('#grand_total').change(function() {
			var amount = this.value;
			$("#grand_total").val(parseFloat(amount).toFixed(2));
		});
	});

	$(document).ready(function() {
		$("#inv_no").keyup(function() {
			if (this.value.match(/[^a-zA-Z0-9]/g)) {
				this.value = this.value.replace(/[^a-zA-Z0-9\-/]/g, '');
			}
		});
	});

	$(document).ready(function() {
		$().toastmessage({
			sticky: false,
			position: 'top-right',
			inEffectDuration: 1000,
			stayTime: 3000,
			closeText: '<i class="icon ion-close-round"></i>',
		});
	});

	$('#dis_amount').keyup(function(event) {
		var amount = parseFloat($('#item_amount').html());
		var val = $(this).val();
		if (amount < val) {
			$().toastmessage('showErrorToast', "This amount must be less than the amount");
			$(this).val('');
		}
	})


	$(document).on('click', '.removebtn', function(e) {
		e.preventDefault();
		element = $(this);
		var item_id = $(this).attr('id');
		if (item_id != 0) {
			var answer = confirm("Are you sure you want to delete?");
			if (answer) {
				$('.loading-overlay').addClass('is-active');
				var bill_id = $("#bill_id").val();
				var data = {
					'bill_id': bill_id,
					'item_id': item_id
				};
				$.ajax({
					method: "GET",
					async: false,
					data: {
						data: data
					},
					dataType: "json",
					url: '<?php echo Yii::app()->createUrl('bills/removebillitem'); ?>',
					success: function(response) {
						if (response.response == 'success') {
							element.closest('tr').remove();
							$('#discount_total').html(response.discount_total);
							$('#amount_total').html(response.amount_total);
							$('#tax_total').html(response.tax_total);
							$('#grand_total').attr('value', response.grand_total);
							$().toastmessage('showSuccessToast', "" + response.msg + "");
							$('.addrow').html(response.html);
							if (response.total_quantity == 0) {
                                $('#buttonsubmit').prop('disabled', true);
                            } else {
                                $('#buttonsubmit').prop('disabled', false);
                            }
						} else {
							$().toastmessage('showErrorToast', "" + response.msg + "");
						}

						$('#description').val('').trigger('change');
						$('#quantity').val('');
						$('#unit').val('');
						$('#rate').val('');
						$('#dis_amount').val('');
						$('#disp').html('0.00');
						$('#item_amount').html('0.00');
						$('#tax_amount').html('0.00');
						$('#sgstp').val('');
						$('#sgst_amount').html('0.00');
						$('#cgstp').val('');
						$('#cgst_amount').html('0.00');
						$('#igstp').val('');
						$('#igst_amount').html('0.00');
						$('#disc_amount').html('0.00');
						$('#total_amount').html('0.00');
						$('#description').focus();
						$('.item_save').attr("id", "0");
						$('#tax_slab').val('18').trigger('change');
					}
				});

			} else {
				return false;
			}
		}
	});



	$(document).on("click", ".addcolumn, .removebtn", function() {

		$("table.table  input[name='sl_No[]']").each(function(index, element) {
			$(element).val(index + 1);
			$('.sl_No').html(index + 1);
		});
	});

	$(".inputSwitch span").on("click", function() {
		var $this = $(this);
		$this.hide().siblings("input").val($this.text()).show();
	});

	$(".inputSwitch input").bind('blur', function() {
		var $this = $(this);
		$(this).attr('value', $(this).val());
		$this.hide().siblings("span").text($this.val()).show();
	}).hide();


	$(".allownumericdecimal").keydown(function(event) {
		if (event.shiftKey == true) {
			event.preventDefault();
		}
		if ((event.keyCode >= 48 && event.keyCode <= 57) ||
			(event.keyCode >= 96 && event.keyCode <= 105) ||
			event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 ||
			event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190 || event.keyCode == 110 || event.keyCode == 13) {
			var splitfield = $(this).val().split(".");
		} else {
			event.preventDefault();
		}

		if ($(this).val().indexOf('.') !== -1 && (event.keyCode == 190 || event.keyCode == 110))
			event.preventDefault();

	});



	$('.other').click(function() {
		if (this.checked) {
			$('#autoUpdate').fadeIn('slow');
			$('#select_div').hide();
		} else {
			$('#autoUpdate').fadeOut('slow');
			$('#select_div').show();
		}

	})


	$(document).on("change", ".other", function() {
		if (this.checked) {
			$(this).closest('tr').find('#autoUpdate').fadeIn('slow');
			$(this).closest('tr').find('#select_div').hide();
		} else {
			$(this).closest('tr').find('#autoUpdate').fadeOut('slow');
			$(this).closest('tr').find('#select_div').show();
		}
	});


	$(".specification").select2({
		ajax: {
			url: '<?php echo  Yii::app()->createAbsoluteUrl('bills/testransaction'); ?>',
			type: "POST",
			dataType: 'json',
			delay: 0,
			data: function(params) {
				return {
					searchTerm: params.term
				};
			},
			processResults: function(response) {
				$('#previous_details').html(response.html);
				$("#previous_details").show();
				$("#pre_fixtable2").tableHeadFixer();
				$('#item_unit').text(response.unit);
				$('#hsn_code').text(response.hsn_code);
				return {
					results: response.data
				};
			},
			cache: true
		}
	});


	$('.specification').change(function() {
		$('#loading').show();
		var element = $(this);
		var category_id = $(this).val();
		var bill_id = $("#bill_id").val();
		var val = $(".specification option:selected").val();
		$('#purchaseitem_unit').val('');
		$("#purchaseitem_unit_hidden").val('');

		$.ajax({
			url: '<?php echo Yii::app()->createUrl('bills/getRelatedUnit'); ?>',
			method: 'POST',
			data: {
				cat_id: val
			},
			dataType: "json",
			success: function(response) {
				$("#purchaseitem_unit").html(response.html);
			}
		})

		$.ajax({
			url: '<?php echo  Yii::app()->createAbsoluteUrl('bills/previoustransaction'); ?>',
			type: 'GET',
			dataType: 'json',
			data: {
				data: category_id,
				bill_id: bill_id
			},
			success: function(result) {
				if (result.status == 1) {
					$('#previous_details').html(result.html);
					$("#previous_details").show();
					$("#pre_fixtable").tableHeadFixer();
				} else {
					$('#previous_details').html(result.html);
					$("#pre_fixtable").tableHeadFixer();
				}

				if (result.unit != '') {
					$("#item_baseunit_data").show();
					$("#item_unit_additional_data").html(result.unit);
				}

				$('#hsn_code').text(result.hsn_code);
				var save = $('.item_save').attr("id");
				if (save == 0) {
					$("#purchaseitem_unit").val(result.unit);
					$('#item_unit').val(result.unit);
				} else {
					var purch_unit_hidden = $("#purchaseitem_unit_hidden").val();
					if (purch_unit_hidden != '') {
						$("#purchaseitem_unit").val(purch_unit_hidden);
						$('#item_unit').val(result.purchaseitem_unit);
					} else {
						$("#purchaseitem_unit").val(result.unit);
						$('#item_unit').val(result.unit);
					}
				}
				if (category_id == 'other') {
					$('.remark').css("display", "inline-block");
					$('#remarks').focus();
				} else if (category_id == '') {
					$('.js-example-basic-single').select2('focus');
					$('.remark').css("display", "none");
				} else {
					$('#quantity').focus();
					$('.remark').css("display", "none");
				}
			}

		})

	})




	/* bills add  */

	$(document).on("change", "#company_id", function() {
		var element = $(this);
		var bill_id = $("#bill_id").val();
		var purchase_id = $("#purchase_id").val();
		var default_date = $(".date").val();
		var company = $(this).val();
		var vendor = $('#vendor').val();
		var project = $('#project').val();
		var bill_number = $('#bill_number').val();
		var expense_head = $('#expense_head').val();
		var warehouse_id = $('#Warehousestock_warehousestock_warehouseid').val();
		if ((moment(default_date, 'DD-MM-YYYY', true).isValid())) {
			if (project == '' || vendor == '' || default_date == '' || bill_number == '' || expense_head == '' || company == '') {
				$.ajax({
					method: "GET",
					dataType: "json",
					url: '<?php echo Yii::app()->createUrl('bills/ajax'); ?>',
					success: function(result) {
						$("#project").select2("focus");
					}
				});
			} else {
				$('.loading-overlay').addClass('is-active');
				$.ajax({
					method: "GET",
					async: true,
					data: {
						bill_id: bill_id,
						bill_number: bill_number,
						default_date: default_date,
						project_id: project,
						vendor_id: vendor,
						expensehead_id: expense_head,
						purchase_id: purchase_id,
						company_id: company,
						warehouse_id: warehouse_id
					},
					dataType: "json",
					url: '<?php echo Yii::app()->createUrl('bills/createnewbills'); ?>',
					success: function(result) {
						if (result.response == 'success') {
							$("#purchase_id").val(result.p_id);
							$(".bill_items").removeClass('checkek_edit');
							$('.js-example-basic-single').select2('focus');
							$("#bill_id").val(result.bill_id);
							$().toastmessage('showSuccessToast', "" + result.msg + "");
						} else {
							$().toastmessage('showErrorToast', "" + result.msg + "");
							element.val('');
						}

						$("#project").select2("focus");
					}
				});
			}
		} else {
			$(this).focus();
			$().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
			$(this).focus();
		}
	});

	$(document).on("change", "#project", function() {
		var element = $(this);
		var bill_id = $("#bill_id").val();
		var purchase_id = $("#purchase_id").val();
		var default_date = $(".date").val();
		var project = $(this).val();
		var vendor = $('#vendor').val();
		var bill_number = $('#bill_number').val();
		var company = $('#company_id').val();
		var expense_head = $('#expense_head').val();
		var warehouse_id = $('#Warehousestock_warehousestock_warehouseid').val();
		if ((moment(default_date, 'DD-MM-YYYY', true).isValid())) {
			if (project == '' || vendor == '' || default_date == '' || bill_number == '' || expense_head == '' || company == '') {
				$.ajax({
					method: "GET",
					dataType: "json",
					url: '<?php echo Yii::app()->createUrl('bills/ajax'); ?>',
					success: function(result) {
						$("#expense_head").select2("focus");
					}
				});
			} else {
				$('.loading-overlay').addClass('is-active');
				$.ajax({
					method: "GET",
					async: true,
					data: {
						bill_id: bill_id,
						bill_number: bill_number,
						default_date: default_date,
						project_id: project,
						vendor_id: vendor,
						expensehead_id: expense_head,
						purchase_id: purchase_id,
						company_id: company,
						warehouse_id: warehouse_id
					},
					dataType: "json",
					url: '<?php echo Yii::app()->createUrl('bills/createnewbills'); ?>',
					success: function(result) {
						if (result.response == 'success') {
							$("#purchase_id").val(result.p_id);
							$(".bill_items").removeClass('checkek_edit');
							$('.js-example-basic-single').select2('focus');
							$("#bill_id").val(result.bill_id);
							$().toastmessage('showSuccessToast', "" + result.msg + "");
						} else {
							$().toastmessage('showErrorToast', "" + result.msg + "");
							element.val('');
						}

						$("#expense_head").select2("focus");
					}
				});
			}
		} else {
			$(this).focus();
			$().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
			$(this).focus();
		}
	});

	$(document).on("change", "#vendor", function() {
		var element = $(this);
		var bill_id = $("#bill_id").val();
		var default_date = $(".date").val();
		var vendor = $(this).val();
		var purchase_id = $("#purchase_id").val();
		var project = $('#project').val();
		var bill_number = $('#bill_number').val();
		var expense_head = $('#expense_head').val();
		var company = $('#company_id').val();
		var warehouse_id = $('#Warehousestock_warehousestock_warehouseid').val();
		if ((moment(default_date, 'DD-MM-YYYY', true).isValid())) {

			if (project == '' || vendor == '' || default_date == '' || bill_number == '' || expense_head == '' || company == '') {
				$.ajax({
					method: "GET",
					dataType: "json",
					url: '<?php echo Yii::app()->createUrl('bills/ajax'); ?>',
					success: function(result) {
						$(".date").focus();
					}
				});

			} else {
				$('.loading-overlay').addClass('is-active');
				$.ajax({
					method: "GET",
					data: {
						bill_id: bill_id,
						bill_number: bill_number,
						default_date: default_date,
						project_id: project,
						vendor_id: vendor,
						expensehead_id: expense_head,
						purchase_id: purchase_id,
						company_id: company,
						warehouse_id: warehouse_id
					},
					dataType: "json",
					url: '<?php echo Yii::app()->createUrl('bills/createnewbills'); ?>',
					success: function(result) {
						if (result.response == 'success') {
							$("#purchase_id").val(result.p_id);
							$(".bill_items").removeClass('checkek_edit');
							$('.js-example-basic-single').select2('focus');
							$("#bill_id").val(result.bill_id);
							$().toastmessage('showSuccessToast', "" + result.msg + "");
						} else {
							$().toastmessage('showErrorToast', "" + result.msg + "");
						}

						$(".date").focus();
					}
				});

			}

		} else {
			$(this).focus();
			$().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
			$(this).focus();
		}

	});


	$(document).on("change", "#expense_head", function() {
		var element = $(this);
		var bill_id = $("#bill_id").val();
		var default_date = $(".date").val();
		var expense_head = $(this).val();
		var purchase_id = $("#purchase_id").val();
		var project = $('#project').val();
		var bill_number = $('#bill_number').val();
		var vendor = $('#vendor').val();
		var company = $('#company_id').val();
		var warehouse_id = $('#Warehousestock_warehousestock_warehouseid').val();
		if ((moment(default_date, 'DD-MM-YYYY', true).isValid())) {
			if (project == '' || vendor == '' || default_date == '' || bill_number == '' || expense_head == '' || company == '') {
				$.ajax({
					method: "GET",
					dataType: "json",
					url: '<?php echo Yii::app()->createUrl('bills/ajax'); ?>',
					success: function(result) {
						$(".vendor").select2("focus");
					}
				});

			} else {
				$('.loading-overlay').addClass('is-active');
				$.ajax({
					method: "GET",
					data: {
						bill_id: bill_id,
						bill_number: bill_number,
						default_date: default_date,
						project_id: project,
						vendor_id: vendor,
						expensehead_id: expense_head,
						purchase_id: purchase_id,
						company_id: company,
						warehouse_id: warehouse_id
					},
					dataType: "json",
					url: '<?php echo Yii::app()->createUrl('bills/createnewbills'); ?>',
					success: function(result) {
						if (result.response == 'success') {
							$("#purchase_id").val(result.p_id);
							$(".bill_items").removeClass('checkek_edit');
							$('.js-example-basic-single').select2('focus');
							$("#bill_id").val(result.bill_id);
							$().toastmessage('showSuccessToast', "" + result.msg + "");
						} else {
							$().toastmessage('showErrorToast', "" + result.msg + "");
						}

						$(".vendor").select2("focus");
					}
				});

			}

		} else {
			$(this).focus();
			$().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
			$(this).focus();
		}

	});



	$(document).on("change", ".date", function() {
		var element = $(this);
		var bill_id = $("#bill_id").val();
		var default_date = $(this).val();
		var project = $('#project').val();
		var vendor = $('#vendor').val();
		var purchase_id = $("#purchase_id").val();
		var bill_number = $('#bill_number').val();
		var expense_head = $('#expense_head').val();
		var company = $('#company_id').val();
		var warehouse_id = $('#Warehousestock_warehousestock_warehouseid').val();
		if ((moment(default_date, 'DD-MM-YYYY', true).isValid())) {
			if (project == '' || vendor == '' || default_date == '' || bill_number == '' || expense_head == '' || company == '') {

			} else {
				$('.loading-overlay').addClass('is-active');
				$.ajax({
					method: "GET",
					async: false,
					data: {
						bill_id: bill_id,
						bill_number: bill_number,
						default_date: default_date,
						project_id: project,
						vendor_id: vendor,
						expensehead_id: expense_head,
						purchase_id: purchase_id,
						company_id: company,
						warehouse_id: warehouse_id
					},
					dataType: "json",
					url: '<?php echo Yii::app()->createUrl('bills/createnewbills'); ?>',
					success: function(result) {
						if (result.response == 'success') {
							$("#purchase_id").val(result.p_id);
							$(".bill_items").removeClass('checkek_edit');
							$('.js-example-basic-single').select2('focus');
							$("#bill_id").val(result.bill_id);
							$().toastmessage('showSuccessToast', "" + result.msg + "");
						} else {
							$().toastmessage('showErrorToast', "" + result.msg + "");
						}
					}

				});

			}
		} else {
			$(this).focus();
			$().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
			$(this).focus();
		}

		$('#purchaseno').focus();

	});


	$(document).on("blur", "#bill_number", function(event) {
		var element = $(this);
		var bill_id = $("#bill_id").val();
		var default_date = $(".date").val();
		var bill_number = $(this).val();
		var purchase_id = $("#purchase_id").val();
		var expense_head = $('#expense_head').val();
		var warehouse_id = $('#Warehousestock_warehousestock_warehouseid').val();
		if ((moment(default_date, 'DD-MM-YYYY', true).isValid())) {
			if (bill_number == '') {
				event.preventDefault();
			} else {
				var project = $('#project').val();
				var vendor = $('#vendor').val();
				var date = $('.date').val();
				var company = $('#company_id').val();
				if (project == '' || vendor == '' || default_date == '' || expense_head == '' || company == '') {

				} else {
					$('.loading-overlay').addClass('is-active');
					$.ajax({
						method: "GET",
						data: {
							bill_id: bill_id,
							bill_number: bill_number,
							default_date: default_date,
							project_id: project,
							vendor_id: vendor,
							expensehead_id: expense_head,
							purchase_id: purchase_id,
							company_id: company,
							warehouse_id: warehouse_id
						},
						dataType: "json",
						url: '<?php echo Yii::app()->createUrl('bills/createnewbills'); ?>',
						success: function(result) {
							if (result.response == 'success') {
								$("#purchase_id").val(result.p_id);
								$(".bill_items").removeClass('checkek_edit');
								$("#bill_id").val(result.bill_id);
								$().toastmessage('showSuccessToast', "" + result.msg + "");
							} else {
								$().toastmessage('showErrorToast', "" + result.msg + "");
							}
							$('#description').select2('focus');
							$('.js-example-basic-single').select2('focus');
						}
					});



				}
			}

		} else {
			$(this).focus();
			$().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
			$(this).focus();
		}

	});

	// bill item add 

	var sl_no = 1;
	var howMany = 0;
	$('.item_save').click(function() {
		var element = $(this);
		var item_id = $(this).attr('id');

		if (item_id == 0) {
			var purchaseitem_unit = $('#purchaseitem_unit').find("option:selected").text();
			var description = $('#description').val();
			var quantity = $('#quantity').val();
			var unit = $('#item_unit').val();
			var bibatch = $('#bibatch').val();
			var hsn_code = $('#hsn_code').text();
			var remark = $('#remarks').val();
			var rate = $('#rate').val();
			var dis_amount = $('#dis_amount').val();
			var disp = $('#disp').html();
			var cgstp = $('#cgstp').val();
			var cgst_amount = $('#cgst_amount').html();
			var sgstp = $('#sgstp').val();
			var sgst_amount = $('#sgst_amount').html();
			var igstp = $('#igstp').val();
			var igst_amount = $('#igst_amount').html();
			var tax_amount = $('#tax_amount').html();
			var item_amount = $('#item_amount').html()
			var discount_amount = $('#disc_amount').html();
			var total_amount = $('#total_amount').html();
			var bill_number = $('#bill_number').val();
			var bill_date = $('#datepicker').val();
			var date = $('input[name="date"]').val();
			var bill_id = $('#bill_id').val();
			var project = $('#project').val();
			var vendor = $('#vendor').val();
			var company = $('#company_id').val();
			var expense_head = $('#expense_head').val();
			var rowCount = $('.table .addrow tr').length;
			var tax_slab = $("#tax_slab").val();
			var Warehouse_id = $('#Warehousestock_warehousestock_warehouseid').val();
			var purchase_quantity = $('#purchase_quantity').val();
			var purchase_rate = $('#purchase_rate').val();


			if (bill_number == '' || bill_date == '' || project == '' || vendor == '' || expense_head == '' || company == '') {
				$().toastmessage('showErrorToast', "Please enter bills details");
			} else {
				if ((moment(date, 'DD-MM-YYYY', true).isValid())) {

					if (description != '' && purchase_rate != '' && purchase_rate > 0 && purchase_quantity != '' && tax_slab != '') {
						howMany += 1;
						if (howMany == 1) {
							var data = {
								'item_id': item_id,
								'quantity': quantity,
								'description': description,
								'Warehouse_id': Warehouse_id,
								'bibatch': bibatch,
								'unit': unit,
								'rate': rate,
								'item_amount': item_amount,
								'dis_amount': dis_amount,
								'disp': disp,
								'sgstp': sgstp,
								'sgst_amount': sgst_amount,
								'cgstp': cgstp,
								'cgst_amount': cgst_amount,
								'igstp': igstp,
								'igst_amount': igst_amount,
								'tax_amount': tax_amount,
								'bill_id': bill_id,
								'discount_amount': discount_amount,
								'total_amount': total_amount,
								'remark': remark,
								'tax_slab': tax_slab,
								'hsn_code': hsn_code,
								'purchase_quantity': purchase_quantity,
								'purchaseitem_unit': purchaseitem_unit,
								'purchase_rate': purchase_rate
							};
							$('.loading-overlay').addClass('is-active');
							$.ajax({
								url: '<?php echo  Yii::app()->createAbsoluteUrl('bills/billsitem'); ?>',
								type: 'GET',
								dataType: 'json',
								data: {
									data: data
								},
								success: function(response) {
									if (response.response == 'success') {
										$('#discount_total').html(response.discount_total);
										$('#amount_total').html(response.amount_total);
										$('#tax_total').html(response.tax_total);
										$('#grand_total').attr("value", response.grand_total);
										$().toastmessage('showSuccessToast', "" + response.msg + "");
										$('.addrow').html(response.html);
										$('#buttonsubmit').prop('disabled', false);
									} else {
										$().toastmessage('showErrorToast', "" + response.msg + "");
									}
									howMany = 0;
									$('#description').val('').trigger('change');
									$('#quantity').val('');
									$('#unit').val('');
									$('#bibatch').val('');
									$('#hsn_code').text('');
									$('#rate').val('');
									$('#dis_amount').val('');
									$('#disp').html('0.00');
									$('#item_amount').html('0.00');
									$('#tax_amount').html('0.00');
									$('#sgstp').val('');
									$('#sgst_amount').html('0.00');
									$('#cgstp').val('');
									$('#cgst_amount').html('0.00');
									$('#igstp').val('');
									$('#igst_amount').html('0.00');
									$('#disc_amount').html('0.00');
									$('#total_amount').html('0.00');
									$('#description').focus();
									$('.item_save').attr("id", "0");
									$('#tax_slab').val('18').trigger('change');
									$('#purchase_quantity').html('');
									$('#purchaseitem_unit').val('');
									$('#purchase_rate').html('');
									$('#purchaseitem_unit_hidden').val('');
									$(".base").hide();
									$('#item_baseunit_data').hide();
									$('#item_conversion_data').hide();
									
								}
							});

						}
					} else {
						$().toastmessage('showErrorToast', "Please fill the details");
					}

				} else {

					$(this).focus();
					$().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
					$(this).focus();
				}




			}

		} else {
			// update 

			var Warehouse_id = $('#Warehousestock_warehousestock_warehouseid').val();
			var description = $('#description').val();
			var quantity = $('#quantity').val();
			var unit = $('#item_unit').val();
			var bibatch = $('#bibatch').val();
			var hsn_code = $('#hsn_code').text();
			var remark = $('#remarks').val();
			var rate = $('#rate').val();
			var dis_amount = $('#dis_amount').val();
			var disp = $('#disp').html();
			var cgstp = $('#cgstp').val();
			var cgst_amount = $('#cgst_amount').html();
			var sgstp = $('#sgstp').val();
			var sgst_amount = $('#sgst_amount').html();
			var igstp = $('#igstp').val();
			var igst_amount = $('#igst_amount').html();
			var tax_amount = $('#tax_amount').html();
			var item_amount = $('#item_amount').html()
			var discount_amount = $('#disc_amount').html();
			var total_amount = $('#total_amount').html();
			var bill_number = $('#bill_number').val();
			var bill_date = $('#bill_date').val();
			var date = $('input[name="date"]').val();
			var bill_id = $('#bill_id').val();
			var project = $('#project').val();
			var vendor = $('#vendor').val();
			var company = $('#company_id').val();
			var expense_head = $('#expense_head').val();
			var rowCount = $('.table .addrow tr').length;
			var tax_slab = $("#tax_slab").val();
			var purchase_quantity = $('#purchase_quantity').val();
			var purchaseitem_unit = $('#purchaseitem_unit').val();
			var purchase_rate = $('#purchase_rate').val();

			if (bill_number == '' || bill_date == '' || project == '' || vendor == '' || expense_head == '' || company == '') {
				$().toastmessage('showErrorToast', "Please enter bill details");
			} else {

				if ((moment(date, 'DD-MM-YYYY', true).isValid())) {
					if (description != '' && purchase_rate != '' && purchase_rate > 0 && purchase_quantity != '' && tax_slab != '') {
						howMany += 1;
						if (howMany == 1) {
							var data = {
								'item_id': item_id,
								'quantity': quantity,
								'bibatch': bibatch,
								'description': description,
								'unit': unit,
								'rate': rate,
								'item_amount': item_amount,
								'dis_amount': dis_amount,
								'disp': disp,
								'sgstp': sgstp,
								'sgst_amount': sgst_amount,
								'cgstp': cgstp,
								'cgst_amount': cgst_amount,
								'igstp': igstp,
								'igst_amount': igst_amount,
								'tax_amount': tax_amount,
								'bill_id': bill_id,
								'discount_amount': discount_amount,
								'total_amount': total_amount,
								'remark': remark,
								'tax_slab': tax_slab,
								'hsn_code': hsn_code,
								'purchase_quantity': purchase_quantity,
								'purchaseitem_unit': purchaseitem_unit,
								'purchase_rate': purchase_rate
							};
							$('.loading-overlay').addClass('is-active');
							$.ajax({
								url: '<?php echo  Yii::app()->createAbsoluteUrl('bills/updatesbillitem'); ?>',
								type: 'GET',
								dataType: 'json',
								data: {
									data: data
								},
								success: function(response) {
									if (response.response == 'success') {
										$('#discount_total').html(response.discount_total);
										$('#amount_total').html(response.amount_total);
										$('#tax_total').html(response.tax_total);
										$('#grand_total').attr("value", response.grand_total);
										$().toastmessage('showSuccessToast', "" + response.msg + "");
										$('.addrow').html(response.html);
									} else {
										$().toastmessage('showErrorToast', "" + response.msg + "");
									}
									howMany = 0;
									$('#description').val('').trigger('change');
									$('#quantity').val('');
									$('#unit').val('');
									$('#rate').val('');
									$('#dis_amount').val('');
									$('#disp').html('0.00');
									$('#item_amount').html('0.00');
									$('#tax_amount').html('0.00');
									$('#sgstp').val('');
									$('#sgst_amount').html('0.00');
									$('#cgstp').val('');
									$('#cgst_amount').html('0.00');
									$('#igstp').val('');
									$('#igst_amount').html('0.00');
									$('#disc_amount').html('0.00');
									$('#total_amount').html('0.00');
									$('#description').focus();
									$('.item_save').attr("id", "0");
									$('#tax_slab').val('18').trigger('change');
									$('#purchase_quantity').val('');
									$('#purchaseitem_unit').val('');
									$('#purchase_rate').val('');
									$('.item_save').attr("id", "0");
									$('#purchaseitem_unit_hidden').val('');
									$(".item_save").attr('value', 'Save');
									$(".base").hide();
								}
							});
						}
					} else {

						$().toastmessage('showErrorToast', "Please fill the details");
					}
				} else {
					$(this).focus();
					$().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
					$(this).focus();
				}



			}

		}
	});


	// edit


	$(document).on('click', '.edit_item', function(e) {
		e.preventDefault();
		var item_id = $(this).attr('id');
		var $tds = $(this).closest('tr').find('td');
		var description = $tds.eq(1).text();
		var quantity = $tds.eq(2).text();
		var unit = $tds.eq(3).text();
		var rate = $tds.eq(4).text();
		var bibatch = $tds.eq(5).text();
		var hsn_code = $tds.eq(6).text();
		var purchaseitem_quantity = $tds.eq(7).text();
		var purchaseitem_unit = $tds.eq(8).text();
		var purchaseitem_rate = $tds.eq(9).text();
		var tax_slab = $tds.eq(10).text();
		tax_slab = Math.round(tax_slab);
		var sgst_amount = $tds.eq(11).text();
		var sgstp = $tds.eq(12).text();
		var cgst_amount = $tds.eq(13).text();
		var cgstp = $tds.eq(14).text();
		var igst_amount = $tds.eq(15).text();
		var igstp = $tds.eq(16).text();
		var disp = $tds.eq(17).text();
		var dis_amount = $tds.eq(18).text();
		var amount = $tds.eq(19).text();
		var tax_amount = $tds.eq(20).text();
		var bill_id = $("#bill_id").val();
		var des_id = $('.item_description').attr("id");

		var data = {
			bill_id: bill_id,
			description: des_id,
			bibatch: bibatch,
			billitem: item_id
		};




		$.ajax({
			method: "GET",
			data: {
				data: data
			},
			dataType: "json",
			url: '<?php echo Yii::app()->createUrl('wh/warehousereceipt/CheckEditBillDespatch'); ?>',
			success: function(result) {

				if (result.response == 'success') {

					if (des_id == '0' || des_id == 'other') {
						$('#remarks').val(description.trim());
						$('.remark').css("display", "inline-block");
						$('#remark').focus();
						$('#remarks').focus();
						$('#description').val('other').trigger('change');
					} else {

						$('#description').val(des_id).trigger('change');
						$('.remark').css("display", "none");
						$('.js-example-basic-single').select2('focus');
					}

					if (purchaseitem_unit == unit) {
						$('.base').hide();
					} else {
						$('.base').show();
					}



					$('#purchase_quantity').val(parseFloat(quantity));
					$('#purchase_unit').val(unit.trim());
					$('#bibatch').val(bibatch.trim());
					$('#hsn_code').val(hsn_code.trim());
					$('#item_amount').html(parseFloat(amount));
					$('#purchase_rate').val(parseFloat(rate));
					$('#tax_slab').val(tax_slab).trigger('change');
					$('#quantity').val(parseFloat(purchaseitem_quantity));
					$('#item_unit').val(parseFloat(purchaseitem_unit));
					$('#rate').val(parseFloat(purchaseitem_rate));
					$('#purchaseitem_unit_hidden').val(unit.trim());

					$('#tax_amount').html(parseFloat(tax_amount))
					$('#disc_amount').html(parseFloat(dis_amount))
					var totalamount = (parseFloat(amount) + parseFloat(tax_amount)) - parseFloat(dis_amount);
					$('#total_amount').html(parseFloat(totalamount).toFixed(2));

					$('#dis_amount').val(parseFloat(dis_amount));
					$('#disp').html(parseFloat(disp));
					$('#sgstp').val(parseFloat(sgstp));
					$('#sgst_amount').html(parseFloat(sgst_amount));
					$('#cgstp').val(parseFloat(cgstp));
					$('#cgst_amount').html(parseFloat(cgst_amount));
					$('#igstp').val(parseFloat(igstp));
					$('#igst_amount').html(parseFloat(igst_amount));
					$(".item_save").attr('value', 'Update');
					$(".item_save").attr('id', item_id);
					$(".item_save").attr('id', item_id);
					$('#description').focus();

				} else {
					$().toastmessage('showErrorToast', "" + result.msg + "");
				}
			}
		});
	})


	$('.item_save').keypress(function(e) {
		if (e.keyCode == 13) {
			$('.item_save').click();
		}
	});


	function filterDigits(eventInstance) {
		eventInstance = eventInstance || window.event;
		key = eventInstance.keyCode || eventInstance.which;
		if ((47 < key) && (key < 58) || key == 8) {
			return true;
		} else {
			if (eventInstance.preventDefault)
				eventInstance.preventDefault();
			eventInstance.returnValue = false;
			return false;
		} 
	}


	// calculation


	$("#purchase_quantity, #purchase_rate, #sgstp, #cgstp, #igstp, #dis_amount").blur(function() {
		var quantity = parseFloat($("#purchase_quantity").val());
		var rate = parseFloat($("#purchase_rate").val());
		var sgstp = parseFloat($("#sgstp").val());
		var cgstp = parseFloat($("#cgstp").val());
		var igstp = parseFloat($("#igstp").val());
		var dis_amount = parseFloat($("#dis_amount").val());
		if (($(this).hasClass("biquantity")) || ($(this).hasClass("birate"))) {
			getconversion_factor();
		}
		var amount = quantity * rate;
		var new_amount = 0;


		if (isNaN(sgstp)) sgstp = 0;
		if (isNaN(cgstp)) cgstp = 0;
		if (isNaN(igstp)) igstp = 0;
		if (isNaN(dis_amount)) dis_amount = 0;

		new_amount = amount - dis_amount;

		if (isNaN(new_amount)) new_amount = 0;

		var sgst_amount = (sgstp / 100) * new_amount;
		var cgst_amount = (cgstp / 100) * new_amount;
		var igst_amount = (igstp / 100) * new_amount;
		var disp = (dis_amount / amount) * 100;

		if (isNaN(sgst_amount)) sgst_amount = 0;
		if (isNaN(cgst_amount)) cgst_amount = 0;
		if (isNaN(igst_amount)) igst_amount = 0;
		if (isNaN(amount)) amount = 0;
		if (isNaN(disp)) disp = 0;

		var tax_amount = sgst_amount + cgst_amount + igst_amount;

		var total_amount = new_amount + tax_amount;

		$("#sgst_amount").html(sgst_amount.toFixed(2));
		$("#cgst_amount").html(cgst_amount.toFixed(2));
		$("#igst_amount").html(igst_amount.toFixed(2));
		$("#disp").html(disp.toFixed(2));
		$("#item_amount").html(amount.toFixed(2));
		$("#tax_amount").html(tax_amount.toFixed(2));
		$("#disc_amount").html(dis_amount.toFixed(2));
		$("#total_amount").html(total_amount.toFixed(2));
	});

	$('#purchaseitem_unit').change(function() {
		getconversion_factor();
	});


	function getconversion_factor() {

		var purchase_unit = $.trim($("#purchaseitem_unit option:selected").text());
		var base_unit = $.trim($("#item_unit").val());
		var item_id = $('#description').val();
		var quantity = $("#purchase_quantity").val();
		var rate = $("#purchase_rate").val();
		var sgst_p = parseFloat($("#sgstp").val());
		var cgst_p = parseFloat($("#cgstp").val());
		var igst_p = parseFloat($("#igstp").val());
		var dis_p = parseFloat($("#disp").val());
		if (purchase_unit !== '' && base_unit !== '' && item_id !== "" && quantity != '') {


			if (purchase_unit != base_unit) {

				$.ajax({
					method: "POST",
					data: {
						'purchase_unit': purchase_unit,
						'base_unit': base_unit,
						'item_id': item_id
					},
					dataType: "json",
					url: '<?php echo Yii::app()->createAbsoluteUrl('bills/getUnitconversionFactor'); ?>',
					success: function(result) {

						if (result != '') {
							$("#item_conversion_data").show();
							$("#item_unit_additional_conv").html(result);
						}

						var conversionvalue = result;
						$(".base").show();
						var amount = quantity * rate;
						var base_rate = base_quantity = 0;
						var base_quantity = conversionvalue * quantity;
						if (isNaN(base_quantity))
							base_quantity = 0;
						if (base_quantity != 0) {
							base_rate = amount / base_quantity;
						}
						base_rate = parseFloat(base_rate).toFixed(2);
						base_quantity = parseFloat(base_quantity).toFixed(2);
						if (isNaN(base_rate))
							base_rate = 0;
						var amount = quantity * rate;
						var dis_amount = ((dis_p / 100) * amount); // discount percent to amount 
						var new_amount = 0;
						if (isNaN(amount))
							amount = 0;
						if (isNaN(dis_amount))
							dis_amount = 0;
						new_amount = amount - dis_amount;

						if (isNaN(new_amount))
							new_amount = 0;

						var sgst_amount = (sgst_p / 100) * new_amount;
						var cgst_amount = (cgst_p / 100) * new_amount;
						var igst_amount = (igst_p / 100) * new_amount;

						if (isNaN(sgst_amount))
							sgst_amount = 0;
						if (isNaN(cgst_amount))
							cgst_amount = 0;
						if (isNaN(igst_amount))
							igst_amount = 0;
						if (isNaN(dis_amount))
							dis_amount = 0;

						var tax_amount = sgst_amount + cgst_amount + igst_amount;
						var total_amount = new_amount + tax_amount;
						$("#sgst_amount").html(sgst_amount.toFixed(2));
						$("#cgst_amount").html(cgst_amount.toFixed(2));
						$("#igst_amount").html(igst_amount.toFixed(2));
						$("#dis_amount").html(dis_amount.toFixed(2));
						$("#item_amount").html(amount.toFixed(2));
						$("#tax_amount").html(tax_amount.toFixed(2));
						$("#disc_amount").html(dis_amount.toFixed(2));
						$("#total_amount").html(total_amount.toFixed(2));
						$('#quantity').val(base_quantity);
						$('#rate').val(base_rate);
					}

				});

			} else {

				$('#quantity').val(quantity);
				$('#rate').val(rate);
				$(".base").hide();
				$("#item_conversion_data").hide();
				$('.base-data').hide();
			}
		}
	}

	$(document).on('click', '.getprevious', function(e) {
		var id = $(this).attr('data-id');
		var res = id.split(",");
		var amount = parseFloat(res[5]);
		var tax_amount = parseFloat(res[6]);
		var disp = parseFloat(res[7]);
		var dis_amount = parseFloat(res[8]);
		var cgst_amount = parseFloat(res[9]);
		var cgstp = parseFloat(res[10]);
		var sgst_amount = parseFloat(res[11]);
		var sgstp = parseFloat(res[12]);
		var igst_amount = parseFloat(res[13]);
		var igstp = parseFloat(res[14]);
		var totalamount = (amount + tax_amount) - dis_amount;

		$('#description').val(res[0]).trigger('change.select2');
		$('#purchase_quantity').val(res[1]);

		$.ajax({
			url: '<?php echo Yii::app()->createUrl('bills/getRelatedUnit'); ?>',
			method: 'POST',
			data: {
				cat_id: res[0],
				selected_unit: res[2]
			},
			dataType: "json",
			success: function(response) {

				$("#purchaseitem_unit").html(response.html);
				$("#purchaseitem_unit").val(res[2]);
				$("#item_unit").val($.trim(response.unit));
				if (response.unit != '') {
					$("#item_baseunit_data").show();
					$("#item_unit_additional_data").html(response.unit);
				}

			},
			complete: function(reponse) {
				$('#purchase_quantity').val(res[1]);
				$('#purchase_rate').val(res[4]);
				$("#purchaseitem_unit").val(res[2]);
				$('#hsn_code').html(res[3]);
				$('#item_amount').text(amount.toFixed(2));
				$('#tax_amount').text(tax_amount.toFixed(2));
				$('#total_amount').text(totalamount.toFixed(2));
				$('#disc_amount').text(dis_amount.toFixed(2));
				$('#dis_amount').val(dis_amount.toFixed(2));
				$('#disp').text(disp.toFixed(2));
				$('#sgst_amount').val(sgst_amount.toFixed(2));
				$('#sgstp').text(sgstp.toFixed(2));
				$('#cgst_amount').val(cgst_amount.toFixed(2));
				$('#cgstp').text(cgstp.toFixed(2));
				$('#igst_amount').val(igst_amount.toFixed(2));
				$('#igstp').text(igstp.toFixed(2));
				getconversion_factor();

			}
		})


		// $('#item_unit').html(res[2]);
		// $('#hsn_code').html(res[3]);
		// $('#rate').val(res[4]);
		// $('#item_amount').text(amount.toFixed(2));
		// $('#tax_amount').text(tax_amount.toFixed(2));
		// $('#total_amount').text(totalamount.toFixed(2));
		// $('#disc_amount').text(dis_amount.toFixed(2));
		// $('#dis_amount').val(dis_amount.toFixed(2));
		// $('#disp').text(disp.toFixed(2));
		// $('#sgst_amount').text(sgst_amount.toFixed(2));
		// $('#sgstp').val(sgstp.toFixed(2));
		// $('#cgst_amount').text(cgst_amount.toFixed(2));
		// $('#cgstp').val(cgstp.toFixed(2));
		// $('#igst_amount').text(igst_amount.toFixed(2));
		// $('#igstp').val(igstp.toFixed(2));
	})

	$("#company_id").change(function() {
		var val = $(this).val();
		$("#project").html('<option value="">Select Project</option>');
		$('#loading').show();
		$.ajax({
			url: '<?php echo Yii::app()->createUrl('bills/dynamicproject'); ?>',
			method: 'POST',
			data: {
				company_id: val
			},
			dataType: "json",
			success: function(response) {
				if (response.status == 'success') {
					$("#project").html(response.html);
				} else {
					$("#project").html(response.html);
				}
			}

		})
	})

	$(document).on('blur', '#quantity', function(e) {
		var newbase_quantity = $('#quantity').val();
		var amount = $('#item_amount').text();
		var base_rate = amount / newbase_quantity;
		base_rate = parseFloat(base_rate).toFixed(2);
		$('#rate').val(base_rate);
	});
</script>

<script>
	(function() {
		var mainTable = document.getElementById("main-table");
		if (mainTable !== null) {
			var tableHeight = mainTable.offsetHeight;
			if (tableHeight > 380) {
				var fauxTable = document.getElementById("faux-table");
				document.getElementById("table-wrap").className += ' ' + 'fixedON';
				var clonedElement = mainTable.cloneNode(true);
				clonedElement.id = "";
				fauxTable.appendChild(clonedElement);
			}
		}
	})();
	$(document).ajaxComplete(function() {
		$('.loading-overlay').removeClass('is-active');
		$('#loading').hide();
	});

	$("#buttonsubmit").keypress(function(e) {
        if (e.keyCode == 13) {
            $('.buttonsubmit').click();
        }
    });

    $(".buttonsubmit").click(function() {
        var general_settings_for_auto_receipt = <?php echo $general_settings_for_auto_receipt; ?>;
        if (general_settings_for_auto_receipt == 1) {
            var answer = confirm("Are you sure you want to auto receipt this item?");
            if (answer) {
				var data = $("#pdfvals1").serialize();					
				$.ajax({
					url: '<?php echo Yii::app()->createUrl('bills/autoReceiptBillWithoutPo'); ?>',
					method: 'POST',
					data: data,						
					success: function(response) {							
						$().toastmessage('showSuccessToast', 'Warehouse Receipt Added Successfully!!');
						setTimeout(function(){
							window.location.reload(1);
						}, 2000);
					}
				})                   
            } else {
                return false;
            }
        } else{
			var data = $("#pdfvals1").serialize();					
			$.ajax({
				url: '<?php echo Yii::app()->createAbsoluteUrl('bills/autoReceiptBillWithoutPo'); ?>',
				method: 'POST',
				data:  data,
				dataType: 'json',											
				success: function(response) {						
					$().toastmessage('showSuccessToast', 'Billed details added successfully!');	
					setTimeout(function(){
						window.location.reload(1);
					}, 2000);						
				}
			}) 
		}
    });
</script>