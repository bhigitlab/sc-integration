<?php
/* @var $this ProjectsController */
/* @var $data Projects */

if ($index == 0) { ?>
    <thead class="entry-table">
        <tr>
            <th style="width:40px;">Sl No.</th>
            <th>Client</th>
            <th>Company</th>
            <th>Phone</th>
            <th>Email ID</th>
            <th>Client Type</th>
            <th>Status</th>
            <th>Created From</th>
            <th>Address</th>
            <?php
            if (isset(Yii::app()->user->role) && (in_array('/clients/updateclients', Yii::app()->user->menuauthlist))) {
                ?>
                <th style="width:40px;">Action</th>
            <?php } ?>
        </tr>
    </thead>
    <?php
}
?>
<?php
if ($data->client_mapping_id !== '0') {
    $styleval = '';
} else {
    $styleval = 'background-color:#A9E9EC;';
}
?>
<tr style="<?php echo $styleval; ?>">
    <td style="width:40px;">
        <?php echo $index + 1; ?>
    </td>
    <td>
        <?php echo CHtml::encode($data->name); ?>
    </td>
    <td>
        <?php
        $companyname = array();
        $arrVal = explode(',', $data->company_id);
        foreach ($arrVal as $arr) {
            $value = Company::model()->findByPk($arr);
            array_push($companyname, $value['name']);
        }
        echo implode(', ', $companyname);
        ?>
    </td>
    <td>
        <?php echo CHtml::encode($data->phone); ?>
    </td>
    <td>
        <?php echo CHtml::encode($data->email_id); ?>
    </td>
    <td>
        <?php echo CHtml::encode($data->projectType['project_type']); ?>
    </td>
    <td>
        <?php echo CHtml::encode($data->status0['caption']); ?>
    </td>
    <td>
        <?php
        if ($data->created_from == 1) {
            echo 'COMS';
        } elseif ($data->created_from == 2) {
            echo 'PMS';
        } else {
            echo 'CRM';
        }
        ?>
    </td>
    <td>
        <?php echo CHtml::encode($data->address); ?>
    </td>
    <?php
    if (isset(Yii::app()->user->role) && (in_array('/clients/updateclients', Yii::app()->user->menuauthlist))) {
        ?>
        <td style="width:40px;">
            <a class="fa fa-edit editClient" data-id="<?php echo $data->cid; ?>"
                onclick="editclient(<?php echo $data->cid; ?>)"></a>
            <?php if (!ANCAPMS_LINKED) { ?>
                <?php
                if ($integration_status != 1) {

                    ?>
                    <span class="delete_client" title="Delete" onclick="deletClient(this,<?php echo $data->cid; ?>)">
                        <span class="fa fa-trash deleteClient"></span>
                    </span>
                    <?php

                }
                ?>
            <?php } ?>
        </td>
    <?php } ?>
</tr>