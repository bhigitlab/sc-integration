<?php
/* @var $this ExpensesController */
/* @var $model Expenses */
/* @var $form CActiveForm */
?>
<h5>Filter By :</h5>
<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>
		<div class="filter">
				<div class="row ">
 
					<div class="col-md-2 col-sm-5 col-xs-5">	
						<?php echo $form->label($model, 'userid'); ?>
                        <?php

							$tblpx = Yii::app()->db->tablePrefix;
							if(Yii::app()->user->role ==1){
								$sql="SELECT `t`.`userid`, concat_ws(' ',`t`.`first_name`, `t`.`last_name`) as full_name,"
										. "`{$tblpx}user_roles`.`role` "
										. "FROM `{$tblpx}users` `t` JOIN {$tblpx}user_roles ON {$tblpx}user_roles.id= t.user_type "
										. "WHERE status=0 ORDER BY user_type,full_name ASC";
							 }elseif(Yii::app()->user->role==2){
                                $sql="SELECT `t`.`userid`, concat_ws(' ',`t`.`first_name`, `t`.`last_name`) as full_name,"
                                        . "`{$tblpx}user_roles`.`role` "
                                        . "FROM `{$tblpx}users` `t` JOIN {$tblpx}user_roles ON {$tblpx}user_roles.id= t.user_type "
                                        . "WHERE status=0  AND userid IN (SELECT userid FROM {$tblpx}project_assign WHERE projectid in (SELECT projectid FROM {$tblpx}project_assign WHERE userid=".Yii::app()->user->id." ))  ORDER BY user_type,full_name ASC";
							 }else{
                                $sql="SELECT `t`.`userid`, concat_ws(' ',`t`.`first_name`, `t`.`last_name`) as full_name,"
                                    . "`{$tblpx}user_roles`.`role` "
                                    . "FROM `{$tblpx}users` `t` JOIN {$tblpx}user_roles ON {$tblpx}user_roles.id= t.user_type "
                                    . "WHERE status=0 AND  userid IN (SELECT userid FROM {$tblpx}project_assign WHERE projectid in (SELECT projectid FROM {$tblpx}project_assign WHERE userid=".Yii::app()->user->id." )) 
                                     ORDER BY user_type,full_name ASC";
							 }
                                    
                            $result = Yii::app()->db->createCommand($sql)->queryAll();
                            $listdata = CHtml::listData($result, 'userid', 'full_name','role');

					                    echo $form->dropDownList($model, 'userid', $listdata, array('empty' => 'Select a user', 'id' => 'userid',
					                    'ajax' => array(
					                        'type' => 'POST',
					                       'dataType' => 'json',
					                        'url' => CController::createUrl('projects/getprojects'),
					                      //  'update'=>'#Withdrawals_projectid_new', 
					                      'data' => array('userid' => 'js:this.value'),
					                         'success' => 'function(data) {
					                            //alert(data.projects);
					                                  $("#projectid").html(data.projects);
					                              }'
					                        )));
 
						?>
					</div>
					 
					
					<div class="col-md-2 col-sm-5 col-xs-5">					
					<?php echo $form->label($model,'projectid'); ?>
					<?php        
 					if(Yii::app()->user->role  ==1){
 					 
	 				      if(isset($model->userid) && trim($model->userid)!=""){
					                     echo $form->dropDownList($model, 'projectid', CHtml::listData(Projects::model()->findAll(array(
					                             'select' => array('pid, name'),
					                             "condition"=> 'pid in (select projectid from jp_project_assign where userid='.$model->userid.')',
					                             'order' => 'name',
					                             'distinct' => true
					                         )), 'pid', 'name'), array('empty' => '-----------', 'id'=>'projectid'));	
					               }else{
					   echo $form->dropDownList($model, 'projectid', array(), array('empty' => '-----------', 'id'=>'projectid'));
					   }					
 				     }else{

 				     	  echo $form->dropDownList($model, 'projectid', CHtml::listData(Projects::model()->findAll(array(
										'select' => array('pid, name'),							
										'order' => 'name',
										//'condition' => 'created_by='.Yii::app()->user->id,
										"condition"=> 'pid in (select projectid from jp_project_assign where userid='.Yii::app()->user->id.')',
										'distinct' => true
									)), 'pid', 'name'), array('empty' => 'select project', 'id'=>'projectid'));	
		
 				     	}
		
					?>
					</div>
					 
					 
					<div class="col-md-2 col-sm-5 col-xs-5">					
					<?php echo $form->label($model, 'type'); ?>
					<?php
					echo $form->dropDownList($model, 'type', CHtml::listData(Status::model()->findAll(array(
				                                'select' => array('sid,caption'),
				                                'condition' => 'status_type="expense"',
				                                'order' => 'caption',
				                                'distinct' => true
				                            )), 'sid', 'caption'), array('empty' => 'Choose a type'));
				            ?>
						
					</div>
					<div class="col-md-3 col-sm-5 col-xs-5 date">						
						<?php echo $form->label($model, 'date'); ?>
						<?php echo CHtml::activeTextField($model, 'fromdate', array('size'=>10,'value'=>isset($_REQUEST['Expenses']['fromdate']) ? $_REQUEST['Expenses']['fromdate']: '')); ?>
						<?php
						$this->widget('application.extensions.calendar.SCalendar', array(
							'inputField' => 'Expenses_fromdate',
							'ifFormat' => '%d-%b-%Y',
						));
						?> to
						<?php echo CHtml::activeTextField($model, 'todate', array('size'=>10,'value'=>isset($_REQUEST['Expenses']['todate']) ? $_REQUEST['Expenses']['todate']: '')); ?>
						<?php
						$this->widget('application.extensions.calendar.SCalendar', array(
							'inputField' => 'Expenses_todate',
							'ifFormat' => '%d-%b-%Y',
						));
						?>
						
					</div>
					<div class="col-md-2 col-sm-3 col-xs-3">	
					<label>&nbsp;</label>
					<div class= "text-left">
					<?php echo CHtml::submitButton('Go'); ?>
					<?php echo CHtml::resetButton('Clear', array('onclick' => 'javascript:location.href="'. $this->createUrl('admin').'"')); ?>
					</div>
					</div>
		 
				</div>
				
				</div>
<?php $this->endWidget(); ?>