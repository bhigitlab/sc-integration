<?php

/**
 * This is the model class for table "{{quotation}}".
 *
 * The followings are the available columns in table '{{quotation}}':
 * @property integer $quotation_id
 * @property integer $project_id
 * @property integer $client_id
 * @property string $date
 * @property double $amount
 * @property double $tax_amount
 * @property integer $created_by
 * @property string $created_date
 * @property string $inv_no
 * @property double $fees
 * @property double $subtotal
 * @property integer $company_id
 */
class Quotation extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{quotation}}';
	}
	public $fromdate;
	public $todate;
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('date,  created_date, inv_no' ,'required'),
			array('project_id, client_id, created_by, company_id', 'numerical', 'integerOnly' => true),
			array('amount, tax_amount, fees, subtotal', 'numerical'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('quotation_id,fromdate,todate, project_id, client_id, date, amount, tax_amount, created_by, created_date, inv_no, fees, subtotal, company_id', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array();
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'quotation_id' => 'Quotation',
			'project_id' => 'Project',
			'client_id' => 'Client',
			'date' => 'Date',
			'amount' => 'Amount',
			'tax_amount' => 'Tax Amount',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
			'inv_no' => 'Inv No',
			'fees' => 'Fees',
			'subtotal' => 'Subtotal',
			'company_id' => 'Company',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('quotation_id', $this->quotation_id);
		$criteria->compare('project_id', $this->project_id);
		$criteria->compare('client_id', $this->client_id);
		$criteria->compare('date', $this->date, true);
		$criteria->compare('amount', $this->amount);
		$criteria->compare('tax_amount', $this->tax_amount);
		$criteria->compare('created_by', $this->created_by);
		$criteria->compare('created_date', $this->created_date, true);
		$criteria->compare('inv_no', $this->inv_no, true);
		$criteria->compare('fees', $this->fees);
		$criteria->compare('subtotal', $this->subtotal);
		$criteria->compare('company_id', $this->company_id);
		$criteria->order = 'quotation_id DESC';
		
		$newQuery = array();

		if (!in_array('/quotation/showallquotation', Yii::app()->user->menuauthlist)) {
			$newQuery['condition'] = 'created_by=:created_by';
			$newQuery['params'] = array(':created_by' => Yii::app()->user->id);
		}
		if(!empty($newQuery)){
			$criteria->mergeWith($newQuery);
		}
		
		
		if (!empty($this->fromdate) && empty($this->todate)) {
			$criteria->addCondition("date >= '" . date('Y-m-d', strtotime($this->fromdate)) . "'");  // date is database date column field
		} elseif (!empty($this->todate) && empty($this->fromdate)) {
			$criteria->addCondition("date <= '" . date('Y-m-d', strtotime($this->todate)) . "'");
		} elseif (!empty($this->todate) && !empty($this->fromdate)) {
			$criteria->addCondition("date between '" . date('Y-m-d', strtotime($this->fromdate)) . "' and  '" . date('Y-m-d', strtotime($this->todate)) . "'");
		}
		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'pagination' => false,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Quotation the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}
	public function pdfsearch()
	{
		$where = '1=1';
		$tblpx = Yii::app()->db->tablePrefix;
		if (!empty($this->fromdate) && empty($this->todate)) {
			$where = "date >= '" . date('Y-m-d', strtotime($this->fromdate)) . "'";  // date is database date column field
		} elseif (!empty($this->todate) && empty($this->fromdate)) {
			$where = "date <= '" . date('Y-m-d', strtotime($this->todate)) . "'";
		} elseif (!empty($this->todate) && !empty($this->fromdate)) {
			$where = "date between '" . date('Y-m-d', strtotime($this->fromdate)) . "' and  '" . date('Y-m-d', strtotime($this->todate)) . "'";
		}
		if (!empty($this->project_id)) {
			$where .= " AND project_id = '" . $this->project_id . "'";
		}
		$data = Yii::app()->db->createCommand("SELECT * FROM " . $tblpx . "quotation WHERE $where ORDER BY quotation_id DESC")->queryAll();
		return $data;
	}

	public function getTotalQuotationAmount($project_id)
	{
		$quotations = Quotation::model()->findAllByAttributes(array('project_id' => $project_id));
		$total_quotation_amount = 0;
		foreach ($quotations as $quotation) {
			// $amount_sum = QuotationList::model()->find(array(
			// 	'select' => 'qt_id, SUM(amount) as amount,SUM(tax_amount) as tax_amount',
			// 	'condition' => 'qt_id=:qt_id',
			// 	'params' => array(':qt_id' => $quotation->quotation_id)
			// )); old
			$amount_sum = PaymentStage::model()->find(array(
				'select' => 'quotation_id, SUM(stage_amount) as stage_amount',
				'condition' => 'quotation_id=:quotation_id AND invoice_status=:invoice_status',
				'params' => array(':quotation_id' => $quotation->quotation_id, ':invoice_status' => '1')
			));
			        
			$total_quotation_amount += ($amount_sum['stage_amount']);
		}
		return isset($total_quotation_amount) ?  Yii::app()->controller->money_format_inr($total_quotation_amount, 2) : 0;
	}

	public function getProjectQuotations($project_id)
	{
		$quotations = Quotation::model()->findAllByAttributes(array('project_id' => $project_id));
		$quotation_list = array();
		foreach ($quotations as $quotation) {
			$amount_sum = PaymentStage::model()->find(array(
				'select' => 'quotation_id, SUM(stage_amount) as stage_amount',
				'condition' => 'quotation_id=:quotation_id AND invoice_status=:invoice_status',
				'params' => array(':quotation_id' => $quotation->quotation_id, ':invoice_status' => '1')
			));
			$quotation_amount = ($amount_sum['stage_amount']);
			$data  = array('attributes' => $quotation->attributes, 'total_amount' => $quotation_amount);
			array_push($quotation_list, $data);
		}
		return $quotation_list;
	}
	public function getProjectQuotationsWise($project_id, $date_from, $date_to){
		
		if (!empty($date_from) && !empty($date_to)) {
    		$criteria = new CDbCriteria();
    		$criteria->addCondition("project_id = " . (int) $project_id); // Casting to integer for safety
    		$criteria->addCondition("date >= '" . addslashes($date_from) . "' AND date <= '" . addslashes($date_to) . "'");
		}
		else if(!empty($date_from)){
			$criteria = new CDbCriteria();
    		$criteria->addCondition("project_id = " . (int) $project_id); // Casting to integer for safety
    		$criteria->addCondition("date >= '" . addslashes($date_from) ."'" );

		}else if(!empty($date_to)){
			$criteria = new CDbCriteria();
    		$criteria->addCondition("project_id = " . (int) $project_id); // Casting to integer for safety
    		$criteria->addCondition(" date <= '" . addslashes($date_to) . "'");

		}else{
			$criteria = new CDbCriteria();
    		$criteria->addCondition("project_id = " . (int) $project_id); // Casting to integer for safety
    		

		}
			

    	$quotations = Quotation::model()->findAll($criteria);
		$quotation_list = array();
		foreach ($quotations as $quotation) {
			$amount_sum = PaymentStage::model()->find(array(
				'select' => 'quotation_id, SUM(stage_amount) as stage_amount',
				'condition' => 'quotation_id=:quotation_id AND invoice_status=:invoice_status',
				'params' => array(':quotation_id' => $quotation->quotation_id, ':invoice_status' => '1')
			));
			$quotation_amount = ($amount_sum['stage_amount']);
			$data  = array('attributes' => $quotation->attributes, 'total_amount' => $quotation_amount);
			array_push($quotation_list, $data);
		}
		return $quotation_list;

	}
	public function getTotalQuotationProjectCostAmount($project_id)
	{
		$quotations = Quotation::model()->findAllByAttributes(array('project_id' => $project_id));
		$total_quotation_project_cost_amount = 0;
		foreach ($quotations as $quotation) {
		       
			$total_quotation_project_cost_amount += $quotation["total_project_cost"];
		}
		return isset($total_quotation_project_cost_amount) ?  Yii::app()->controller->money_format_inr($total_quotation_project_cost_amount, 2) : 0;
	}
}
