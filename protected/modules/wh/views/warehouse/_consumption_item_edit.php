<?php if (!empty($consumptionItems)): ?>
    <div id="dynamic-rows">
        <?php foreach ($consumptionItems as $index => $detail): ?>
            <?php 
                // Fetch rates based on item_id and warehouse_id
                $stockRecords = Warehousestock::model()->findAllByAttributes(array(
                    'warehousestock_warehouseid' => $warehouse_id, 
                    'warehousestock_itemid' => $detail->item_id
                ));
                $rates = [];
                if (!empty($stockRecords)) {
                    foreach ($stockRecords as $stock) {
                        $rates[] = [
                            'id' => $stock->warehousestock_id,
                            'rate' => $stock->rate,
                        ];
                    }
                }
            ?>

            <div class="row dynamic-row">

                <!-- Material Dropdown -->
                <div class="form-group col-xs-12 col-md-2">
                    <label>Material</label>
                    <select name="PmsAccWprItemConsumed[materials][<?php echo $index; ?>][material_id]" class="coms_material_id form-control select2">
                        <option value="" selected disabled>Select Material</option>
                        <?php foreach ($materials as $id => $materialName){ ?>
                            <option value="<?php echo CHtml::encode($id); ?>" <?php echo (isset($detail) && $detail->coms_material_id == $id) ? 'selected' : ''; ?>>
                                <?php echo CHtml::encode($materialName); ?>
                            </option>
                        <?php } ?>
                    </select>
                </div>

                <!-- Specification Dropdown -->
                <?php 
                    // Create a new variable to avoid conflict with the controller $materials variable
                    $materialSpecs = array();
                    $consumption_id = $detail->consumption_id;
                    $consumptionRequest = ConsumptionRequest::model()->findByPk($consumption_id);
                    $project_id = $consumptionRequest['coms_project_id'];
                    $materialId = $detail->coms_material_id; 
                    
                    $sql = "SELECT m.material, m.specification FROM `jp_material_estimation` m 
                            LEFT JOIN `jp_itemestimation` e ON e.itemestimation_id = m.estimation_id 
                            WHERE m.project_id = ".$project_id." 
                              AND e.itemestimation_status = 2 
                              AND e.project_id = ".$project_id."  
                              AND m.project_id = ".$project_id." 
                              AND m.material = ".$materialId;
                    $materialSpecs = Yii::app()->db->createCommand($sql)->queryAll();
                    
                    $specData = [];
                    $newQuery = '';
                    $catname = '';
                    $brandname = '';
                    
                    if (!empty($materialSpecs)) {
                        foreach($materialSpecs as $material){
                            $specificationIds = explode(',', $material["specification"]);
                            // Reset newQuery for each material row to avoid appending from previous iteration
                            $newQuery = '';
                            foreach($specificationIds as $specification){
                                if(!empty($newQuery)){
                                    $newQuery .= " OR ";
                                }
                                $newQuery .= " FIND_IN_SET(id, '". $material["specification"] ."')";
                            }
                        
                            if(!empty($newQuery)){
                                $specSql = "SELECT * FROM jp_specification WHERE (".$newQuery.")";
                                $specificationss = Yii::app()->db->createCommand($specSql)->queryAll();
                            
                                if(!empty($specificationss)){
                                    foreach ($specificationss as $spec) {
                                        $cat = PurchaseCategory::model()->findByPk($spec['cat_id']);
                                        if(!empty($cat)){
                                            $catname = $cat->category_name;
                                        }
                                        $brand = Brand::Model()->findByPk($spec['brand_id']);
                                        $brandname = $brand["brand_name"];
                                        $specData[] = [
                                            'id' => $spec['id'],
                                            'specification' => $catname.'-'.$brandname.'-'.$spec["specification"],
                                        ];
                                    }
                                }
                            }
                        }
                    }     
                ?>
                <div class="form-group col-xs-12 col-md-2">
                    <label>Specification</label>
                    <select name="PmsAccWprItemConsumed[materials][<?php echo $index; ?>][specification_id]" class="specification_id form-control select2" id="PmsAccWprItemConsumed_item_id_<?php echo $index; ?>">
                        <option value="" selected disabled>Select Specification</option>
                        <?php 
                        if(!empty($detail) && !empty($specData)){
                            foreach ($specData as $spec) { ?>
                                <option value="<?php echo CHtml::encode($spec['id']); ?>" <?php echo (isset($detail) && $detail->item_id == $spec['id']) ? 'selected="selected"' : ''; ?>>
                                    <?php echo CHtml::encode($spec['specification']); ?>
                                </option>
                        <?php }
                        } ?>
                    </select>
                </div>

                <!-- Item Rate Dropdown -->
                <div class="form-group col-xs-12 col-md-2">
                    <label>Item Rate</label>
                    <select name="PmsAccWprItemConsumed[materials][<?php echo $index; ?>][item_rate]" class="item_rate form-control select2" id="PmsAccWprItemConsumed_item_rate_<?php echo $index; ?>" onchange="handleRateChange(<?php echo $index; ?>)">
                        <option value="" selected disabled>Choose Rate</option>
                        <?php if (isset($rates) && is_array($rates)): ?>
                            <?php foreach ($rates as $rate): ?>
                                <option value="<?php echo CHtml::encode(number_format((float)$rate['rate'], 2, '.', '')); ?>" <?php echo ($detail->item_rate == $rate['rate']) ? 'selected' : ''; ?>>
                                    <?php echo CHtml::encode(number_format((float)$rate['rate'], 2, '.', '')); ?>
                                </option>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                    <span style="color:red;" id="rateError_<?php echo $index; ?>"></span>
                </div>

                <!-- Item Quantity Field -->
                <div class="form-group col-xs-12 col-md-2">
                    <label>Quantity</label>
                    <input type="number" class="item_qty form-control" name="PmsAccWprItemConsumed[materials][<?php echo $index; ?>][item_qty]" min="0" onchange="handleQuantityChange(this, <?php echo $index; ?>),handleRateChange(<?php echo $index; ?>)" value="<?php echo CHtml::encode($detail->item_qty); ?>">
                                <span style="color:red;" id="rateQuantityError_<?php echo $index; ?>"></span>
                </div>

                <!-- Item Amount Field -->
                <div class="form-group col-xs-12 col-md-2">
                    <label>Amount</label>
                    <input type="number" class="item_amount form-control" name="PmsAccWprItemConsumed[materials][<?php echo $index; ?>][item_amount]" min="0" value="<?php echo CHtml::encode($detail->item_amount); ?>">
                </div>

                <!-- Hidden Default Material Type -->
                <input type="hidden" name="PmsAccWprItemConsumed[materials][<?php echo $index; ?>][default_material_type]" value="1">

                <div class="col-xs-12 col-md-2">
                    <?php if ($detail->type_from != 2): ?>
                        <?php if ($consumptionRequest->status == 0): ?>
                            <button type="button" class="add-btn btn btn-info btn-sm">+</button>
                            <button type="button" class="remove-btn btn btn-danger btn-sm">-</button>
                        <?php endif; ?>
                    <?php endif; ?>
                </div>

            </div>
        <?php endforeach; ?>
    </div>
<?php endif; ?>

