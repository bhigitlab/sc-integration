<?php

class QuotationCategoryMasterController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout = '//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		$accessArr = array();
		$accessauthArr = array();
		$controller = 'quotationcategory/' . Yii::app()->controller->id;

		if (isset(Yii::app()->user->menuall)) {
			if (array_key_exists($controller, Yii::app()->user->menuall)) {
				$accessArr = Yii::app()->user->menuall[$controller];
			}
		}

		if (isset(Yii::app()->user->menuauth)) {
			if (array_key_exists($controller, Yii::app()->user->menuauth)) {
				$accessauthArr = Yii::app()->user->menuauth[$controller];
			}
		}
		$access_privlg = count($accessauthArr);
		return array(
			array(
				'allow', // allow all users to perform 'index' and 'view' actions
				'actions' => $accessArr,
				'users' => array('@'),
				'expression' => "$access_privlg > 0",
			),

			array(
				'allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions' => $accessauthArr,
				'users' => array('@'),
				'expression' => "$access_privlg > 0",
			),
			array(
				'allow', // allow admin user to perform 'admin' and 'delete' actions
				//'actions' => array(''),
				'users' => array('@'),
			),
			array(
				'deny',  // deny all users
				'users' => array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view', array(
			'model' => $this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model = new QuotationCategoryMaster;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if (isset($_POST['QuotationCategoryMaster'])) {
			$model->attributes = $_POST['QuotationCategoryMaster'];
			if ($model->save())
				$this->redirect(array('view', 'id' => $model->id));
		}

		$this->render('create', array(
			'model' => $model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model = $this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if (isset($_POST['QuotationCategoryMaster'])) {
			$model->attributes = $_POST['QuotationCategoryMaster'];
			if ($model->save())
				$this->redirect(array('view', 'id' => $model->id));
		}

		$this->render('update', array(
			'model' => $model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	
        
        public function actionDelete($id) {

        $model = $this->loadModel($id);
        $transaction = Yii::app()->db->beginTransaction();
        try {
            if (!$model->delete()) {
                $success_status = 0;
                throw new Exception(json_encode($model->getErrors()));
            } else {
                $success_status = 1;
            }
            $transaction->commit();
        } catch (Exception $error) {
            $transaction->rollBack();
            $success_status = 0;
        } finally {
            if ($success_status == 1) {
                echo json_encode(array('response' => 'success', 'msg' => 'Category Deleted Successfully'));
            } else {
                if ($error->errorInfo[1] == 1451) {
                    echo json_encode(array('response' => 'warning', 'msg' => 'Cannot Delete ! This record alredy in use.'));
                } else {
                    echo json_encode(array('response' => 'error', 'msg' => 'Some Error Occured'));
                }
            }
        }
    }

    /**
	 * Lists all models.
	 */
	public function actionIndex($id = 0)
	{

		if ($id !== 0 and $id > 0) {
			$formmodel = $this->loadModel($id);
			$actiontype = ' Updated ';
		}

		if ($id == 0) {
			$formmodel = new QuotationCategoryMaster;
			$actiontype = ' Created ';
		}

		if (isset($_POST['QuotationCategoryMaster'])) {
			$formmodel->attributes = $_POST['QuotationCategoryMaster'];
			if ($formmodel->save()) {

				Yii::app()->user->setFlash('success', "Quotation Category {$actiontype} successfully");
				$this->redirect(array('quotationCategoryMaster/index'));
			}
		}

		$model = new QuotationCategoryMaster('search');
		$model->unsetAttributes();  // clear any default values
		if (isset($_GET['QuotationCategoryMaster']))
			$model->attributes = $_GET['QuotationCategoryMaster'];


		$this->render('index', array(
			'model' => $model, 'formmodel' => $formmodel
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model = new QuotationCategoryMaster('search');
		$model->unsetAttributes();  // clear any default values
		if (isset($_GET['QuotationCategoryMaster']))
			$model->attributes = $_GET['QuotationCategoryMaster'];

		$this->render('admin', array(
			'model' => $model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return QuotationCategoryMaster the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model = QuotationCategoryMaster::model()->findByPk($id);
		if ($model === null)
			throw new CHttpException(404, 'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param QuotationCategoryMaster $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'quotation-category-master-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
