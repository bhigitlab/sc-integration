<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.min.js"></script>
<?php 
$pms_api_integration=ApiSettings::model()->pmsIntegrationStatus();
?>
<div class="form">
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'materials-form',
        'enableAjaxValidation' => false,
    ));
    ?>

    <div class="panel-body">
        <div class="row">
        <input type="hidden" id="pms_integration" value="<?php echo $pms_api_integration ;?>">
            <div class="col-md-4">
                <div class="form-group">
                    <?php echo $form->labelEx($model, 'material_name'); ?>
                    <?php echo $form->textField($model, 'material_name', array(
                        'size' => 30,
                        'maxlength' => 255,
                        'class' => 'form-control',
                        'pattern' => '[A-Za-z\s]+',
                        'title' => 'Only alphabetic characters and spaces are allowed','required'=>'required',
                        'onchange' => 'checkDuplicateMaterialName(this.value)'
                    )); ?>
                    <?php echo $form->error($model, 'material_name'); ?>
                </div>
                <div id="material-name-error" class="text-danger"></div> 
            </div>
            <div class="col-md-3">
            <div class="form-group">
                <?php echo $form->labelEx($model, 'material_unit'); ?>
                    <?php
                       $pms_api_integration=ApiSettings::model()->pmsIntegrationStatus();
                       if($pms_api_integration==1){
                        $unitData = CHtml::listData(Unit::model()->findAll(array(
                            'condition' => 'pms_unit_id > 0',
                        )), 'id', 'unit_name');
                       }
                       else{
                        $unitData = CHtml::listData(Unit::model()->findAll(), 'id', 'unit_name');
                       }
                        // Generate options array for selected units
                        $options = [];
                        if (is_array($model->material_unit)) {
                            foreach ($model->material_unit as $unit) {
                                $options[$unit] = ['selected' => true];
                            }
                        }
                        // Render the dropdown list with the selected options
                        echo $form->dropDownList($model, 'material_unit[]', $unitData, [
                            'class' => 'form-control js-example-basic-multiple',
                            'multiple' => 'multiple',
                            'options' => $options,
                            'required'=>'required',
                        ]);
                ?>
                <?php echo $form->error($model, 'material_unit'); ?>
            </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <?php echo $form->labelEx($model, 'specification'); ?>
                    <?php
                        $specifications = CHtml::listData(Specification::model()->findAll(), 'id', 'specification');
                    
                        $specOptions = [];
                        if (is_array($model->specification)) {
                            foreach ($model->specification as $spec) {
                                $specOptions[$spec] = ['selected' => true];
                            }
                        }
                        
                        echo $form->dropDownList($model, 'specification[]', $specifications, [
                            'class' => 'form-control js-example-basic-multiple',
                            'multiple' => 'multiple',
                            'options' => $specOptions,
                            'required' => 'required'
                        ]);
                    ?>
                    <?php echo $form->error($model, 'specification'); ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 save-btnHold">
                <label style="display:block;">&nbsp;</label>
                <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-info btn-sm submit','id' => 'submitButton')); ?>
                <?php if (Yii::app()->user->role == 1) {
                    if (!$model->isNewRecord) { 
                    }
                } ?>
                
                <?php
                if (!$model->isNewRecord) {
                    echo CHtml::ResetButton('Close', array('onclick' => 'closeaction(this,event);', 'class' => 'btn btn-sm'));
                } else {
                    echo CHtml::ResetButton('Reset', array('style' => 'margin-right:3px;', 'class' => 'btn btn-default btn-sm'));
                    echo CHtml::ResetButton('Close', array('onclick' => 'closeaction(this,event);', 'class' => 'btn btn-sm'));
                }
                echo CHtml::hiddenField('execute_api', 0);
                ?>
            </div>
        </div>
    </div>
    <?php echo $form->hiddenField($model, 'pms_material_id'); ?>
    <?php $this->endWidget(); ?>
</div>
<script>
    $(document).ready(function() {
        $('.js-example-basic-multiple').select2({
            width: '100%',
            
        });
    });
    $(document).ready(function() {
        $('#<?php echo CHtml::activeId($model, 'material_name'); ?>').on('input', function() {
            var input = $(this).val();
            var valid = /^[A-Za-z\s]*$/.test(input);
            if (!valid) {
                $(this).val(input.replace(/[^A-Za-z\s]/g, ''));
            }
        });
    });

    function checkDuplicateMaterialName(materialName) {
    console.log('Checking duplicate for material name:', materialName); // Debugging

    $.ajax({
        url: '<?php echo Yii::app()->createUrl("Materials/checkDuplicateMaterialName"); ?>', // Update controllerName to your actual controller
        type: 'POST',
        data: {
            material_name: materialName,
            YII_CSRF_TOKEN: '<?php echo Yii::app()->request->csrfToken; ?>' // Include CSRF token if CSRF validation is enabled
        },
        success: function(response) {
            var data = $.parseJSON(response);
            console.log('Response from server:', data); // Debugging

            if (data.isDuplicate) {
                $('#material-name-error').text('This material name is already taken.');
                $('#submitButton').prop('disabled', true);
            } else {
                $('#material-name-error').text('');
                $('#submitButton').prop('disabled', false);
            }
        },
        error: function(xhr, status, error) {
            console.log('AJAX error:', error); // Debugging
            $('#material-name-error').text('Error checking material name.');
            $('#submitButton').prop('disabled', false);
        }
    });
}
$(document).ready(function() {
        document.getElementById('materials-form').onsubmit = function() {
        var pms_integration = $("#pms_integration").val();
        var pmsMaterialId = $('#Materials_pms_material_id').val();
        if(pms_integration =='1'){
            if(pmsMaterialId=='0'){
                var confirmApiCall = confirm("Do you want to save the materials in Pms?");
                if (confirmApiCall) {
                  document.getElementById('execute_api').value = 1;
                }
            }
            if (pmsMaterialId!='0') {
                document.getElementById('execute_api').value = 1;
            }
       }
    };
    });
</script>
