
<div class="container">
    <h2>Gallery</h2>
    <?php 
   
    if(Yii::app()->user->role != 4){ ?>
    <div class="row">
        <div class="col-md-12 col-sm-6 col-xs-6 " >

            <?php echo CHtml::link('Upload Image', array('/ImageGallery/image'), array('class' => 'btn btn-sm btn-primary'));?>
        </div>
    </div>
    <br>
    <?php } ?>
 <?php $this->renderPartial('_search', array('model' => $model));  ?>
 <div class="exp-list">
 <div class="row">
 <?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$model->search(),
	'itemView'=>'_newview',
)); ?>
</div>

</div>
</div>
