<?php
/* @var $this UnitConversionController */
/* @var $model UnitConversion */

$this->breadcrumbs=array(
	'Unit Conversions'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List UnitConversion', 'url'=>array('index')),
	array('label'=>'Create UnitConversion', 'url'=>array('create')),
	array('label'=>'Update UnitConversion', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete UnitConversion', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage UnitConversion', 'url'=>array('admin')),
);
?>

<h1>View UnitConversion #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'item_id',
		'base_unit',
		'conversion_factor',
		'conversion_unit',
		'created_date',
		'priority',
	),
)); ?>
