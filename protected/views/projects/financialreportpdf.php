<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/tablestyles.css" />
<link rel="stylesheet" type="text/css" href="uploads/new_template/css/newstyle.css" />
<style>
    table {
        border-collapse: collapse;
        width: 100%;
    }
    /*.container{max-width: 1050px;margin:0 auto;padding: 15px;border: 1px solid #ddd;}*/
    .text-right{text-align:right}
    .details,.info-table{border: 1px solid #212121;margin-top: 20px;}
    .details td, .info-table td,.info-table th{padding: 6px 8px; }
    .info-table{margin-bottom: 10px;}
    .text-center{text-align:center;}
    .img-hold{width: 10%;}
    .companyhead{font-size: 18px;font-weight:bold;margin-top:0px;color: #789AD1;}
</style>
<?php
    $company_address = Company::model()->findBypk(Yii::app()->user->company_id);
?>
    <table border=0>
        <tbody>
                <tr>
                <td class="img-hold"><img  src="<?php echo Yii::app()->theme->baseUrl; ?>/images/logo.png" alt="img" style='margin-top:-45px;'/></td>
                    <td class=text-right>GST NO: <?php echo isset($company_address["company_gstnum"])?$company_address["company_gstnum"]:''; ?></td>
                </tr>
        </tbody>
    </table>
    <table border=0>
        <tbody>
            <tr>
               
                <td class="text-center"><p class='companyhead'><?php echo isset($company_address["name"])?$company_address["name"]:''; ?></p><br>
                    <p><?php echo ( (isset($company_address['address']) && !empty($company_address['address'])) ? ($company_address['address']) : ""); ?><br>
                    PIN : <?php echo ( (isset($company_address['pincode']) && !empty($company_address['pincode'])) ?($company_address['pincode']) : ""); ?>,&nbsp;
                    PHONE : <?php echo ( (isset($company_address['phone']) && !empty($company_address['phone'])) ?( $company_address['phone']) : ""); ?><br>
                    EMAIL : <?php echo ( (isset($company_address['email_id']) && !empty($company_address['email_id'])) ?( $company_address['email_id']) : ""); ?></p><br>
                    <h3>Financial Report  : <?php echo date('d-M-Y');?></h3>
                </td>
            </tr>
        </tbody>
    </table>
    
    <h3>Fixed Rate Contracts</h3>
        <table border="1">
            <thead>
                <tr>
                    <th>Sl No.</th>
                    <th>Project Name</th>
                    <th>Company</th>
                    <th>SQ. FT.</th>
                    <th>SQ.FT.Rate or Lumpsum</th>
                    <th>Receipts</th>
                    <th>Expenses</th>
                    <th>Invoice</th>
                    <th>Deficit</th>
                    <th>Surplus</th>
                    <th>Remarks</th>
                </tr>
            </thead>
            <tbody>
            <tr>
            <td colspan="5" align='right'><b>Total</b></td>
            <td><b><?= Controller::money_format_inr(abs($receipt_total),2)?></b></td>
            <td></td>
            <td></td>
            <td><b><?=  Controller::money_format_inr(abs($deficit_total),2)?></b></td>
            <td><b><?= Controller::money_format_inr(abs($surplus_total),2)?></b></td>
            <td></td>
            </tr>

                    <?php echo $entries; ?>
            </tbody>
        </table>

<h3>Fixed Percentage Contracts</h3>
<table border="1">
            <thead>
                <tr>
                    <th>Sl No.</th>
                    <th>Project Name</th>
                    <th>Company</th>
                    <th>SQ. FT.</th>
                    <th>Fixed Percentage</th>
                    <th>Receipts</th>
                    <th>Expenses</th>
                    <th>Service Charge</th>
                    <th>Invoice</th>
                    <th>Deficit</th>
                    <th>Surplus</th>
                    <th>Remarks</th>
                </tr>
            </thead>
            <tbody>
            <tr>
            <td colspan="5" align='right'><b>Total</b></td>
            <td><b><?= Controller::money_format_inr(abs($receipt_total_perc),2)?></b></td>
            <td></td>
            <td></td>
            <td></td>
            <td><b><?= Controller::money_format_inr(abs($deficit_total_perc),2)?></b></td>
            <td><b><?= Controller::money_format_inr(abs($surplus_total_perc),2)?></b></td>
            <td></td>
            </tr>
                <?php echo $entries2; ?>
            </tbody>
        </table>
