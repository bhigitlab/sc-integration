<?php
/* @var $this MaterialRequisitionController */
/* @var $model MaterialRequisition */

$this->breadcrumbs = array(
  'Material Requisitions' => array('index'),
  $model->requisition_id,
);

?>

<div class="container">
  <div class="loading-overlay">
    <span class="fa fa-spinner fa-3x fa-spin"></span>
  </div>
  <div class="header-container">
    <h3>
      <?php echo $model->requisition_no ? "#$model->requisition_no" : ''; ?>
    </h3>
    <div class="btn-container">
      <?php echo CHtml::link('List MaterialRequisition', array('index'), array('class' => 'btn btn-primary pdf_excel')); ?>
      <?php
      $acc_proj_id_empty_style = '';
      if ($pms_api_integration == 1) {
        $attributes = array('material_item_id' => null, 'mr_id' => $model->requisition_id);
        $materialRequisitions = MaterialRequisitionItems::model()->findAllByAttributes($attributes);
        if (!empty($materialRequisitions)) {
          $empty_item_count = MaterialRequisitionItems::model()->countByAttributes($attributes);
          if ($empty_item_count >= 1) {
            echo CHtml::link('Refresh', array('MaterialRequisition/refreshItemButton', 'mrId' => $model->requisition_id), array('class' => 'btn btn-primary pdf_excel'));

          }
          $acc_proj_id_empty_style = "style='background-color:green'";
        }
      }
      echo CHtml::button('Add PO', array('class' => 'btn btn-primary pdf_excel', 'style' => 'display:none;', 'id' => 'addPoButton'));
      echo CHtml::button('Add Bill Without PO', array('class' => ' btn btn-primary pdf_excel', 'style' => 'display:none;', 'id' => 'addBillButton'));
      ?>
    </div>
    <input type="hidden" id="mrId" value="<?php echo $model->requisition_id; ?>">
  </div>
  <!--PO Items model-->
  <div class="box_holder">
    <div class="row">
      <div class="col-sm-3 col-md-2">
        <label>Requisition No:</label>
        <div>
          <?php echo $model->requisition_no ?>
        </div>
      </div>
      <div class="col-sm-3 col-md-2">
        <label>Project:</label>
        <div>
          <?php echo isset($model->project) ? $model->project->name : ''; ?>
        </div>
      </div>
      <div class="col-sm-3 col-md-2">
        <label>Task:</label>
        <div>
          <?php
          echo isset($model->task_description) ? $model->task_description : '';
          ?>
        </div>
      </div>
      <div class="col-sm-3 col-md-2">
        <label>Created By:</label>
        <div>
          <?php
          if ($model->type_from == 1) {
            $table = "pms_users";
          } else {
            $table = "jp_users";
          }
          $sql = 'SELECT first_name,last_name '
            . ' FROM ' . $table . '  WHERE userid=' . $model->created_by;
          $users = Yii::app()->db->createCommand($sql)->queryRow();
          echo ($users['first_name'] . " " . $users['last_name']);
          ?>
        </div>
      </div>
      <div class="col-sm-3 col-md-2">
        <label> Date:</label>
        <div>
          <?php echo date('d-M-y', strtotime($model->created_date)); ?>
        </div>
      </div>
      <div class="col-sm-3 col-md-1">
        <label>Type From:</label>
        <div>
          <?php if ($model->type_from == 1) {
            echo 'PMS';
          } else {
            echo 'ACCOUNTS';
          } ?>
        </div>
      </div>
    </div>
  </div>
  <?php if ($pms_api_integration == 1) { ?>
    <div class="legend status-legend">
      <span class="project_not_mapped"></span> Materials Not Mapped In Integration
    </div>
  <?php } ?>
  <div class="table-responsive">
    <table cellpadding="3" cellspacing="0" class="table table-bordered">
      <thead>
        <tr class="tr_title">
          <th></th>
          <th>Sl no</th>
          <?php
          if ($pms_api_integration == 1) {
            if ($model->type_from == 1) { ?>
              <th>PMS TEMPLATE ITEM </th>
            <?php }
          } ?>
          <th>ITEM</th>
          <th>UNIT</th>
          <th>QUANTITY</th>
          <th>DATE</th>
          <th>PO/BILL NO</th>
          <th>PO/BILL STATUS</th>
          <th> REMARKS</th>
          <th> ACTIONS</th>
        </tr>
      </thead>
      <tbody>
        <?php
        //echo "<pre>";print_r($material_req_item_arr);exit;
        foreach ($material_req_item_arr as $key => $item) {
          if (!empty($item['mr_item_id'])) {
            $styleval = '';
          } else {

            $styleval = 'background-color:#A9E9EC;';
          }
          ?>
          <tr style="<?php echo $styleval; ?>">
            <td>
              <?php
              if (!empty($item['mr_item_id'])) {
                if (empty($item['item_po_id'])) {
                  ?>
                  <input type="checkbox" class="mrItemCheckbox" data-item-id="<?php echo $item['mr_item_id'] ?>">
                  <?php
                } else {
                  ?>
                  <input type="checkbox" class="mrItemCheckbox" data-item-id="<?php echo $item['mr_item_id'] ?>" disabled>
                  <?php
                }
              }
              ?>
            <td width="5%">
              <?= $key + 1 ?>
            </td>
            <?php
            if ($pms_api_integration == 1) {
              if ($model->type_from == 1) { ?>
                <td width="30%">
                  <?= !empty($item['item_pms_material_name']) ? $item['item_pms_material_name'] : '-' ?>
                </td>
              <?php }
            } ?>
            <td width="30%">
              <?= !empty($item['item_name']) ? $item['item_name'] : 'Item Not Mapped' ?>
            </td>
            <td width="10%">
              <?= $item['item_unit'] ?>
            </td>
            <td width="10%">
              <?= $item['item_quantity'] ?>
            </td>
            <td width="15%">
              <?= date('d-M-y', strtotime($item['item_req_date'])) ?>
            </td>
            <td width="10%">
              <?php
              if (!empty($item['items_bill_id'])) {
                $purchase_bill = Bills::Model()->findByPk($item['items_bill_id']);
                $purchase_no = $purchase_bill->bill_number;
                $viewUrl = Yii::app()->createUrl('bills/billview', array('id' => $item['items_bill_id']));
                echo '<a href="' . $viewUrl . '" target="_blank">' . $purchase_no . '</a>';
              } else {
                $viewUrl = Yii::app()->createUrl('purchase/viewpurchase', array('pid' => $item['item_po_id']));

                echo '<a href="' . $viewUrl . '" target="_blank">' . $item['item_po_no'] . '</a>'; ?>
              </td>
            <?php } ?>

            <td width="10%">
              <?php
              if (!empty($item['items_bill_id'])) {

                $billItemsExist = Billitem::model()->exists('bill_id=:bill_id', array(':bill_id' => $item['items_bill_id']));
                if ($billItemsExist) {

                  $sql = "SELECT purchase_billing_status FROM `jp_purchase` "
                    . " WHERE p_id=" . $item['item_po_id'];

                  $po_bill_status_id = Yii::app()->db->createCommand($sql)->queryScalar();

                  $data_bll_status = MaterialRequisition::model()->getBillingStatus($item['item_po_id']);

                  echo $data_bll_status;
                } else {
                  echo "Items Not Added";
                }
              } elseif (!empty($item['item_po_id'])) {
                echo isset($item['item_po_id']) ? MaterialRequisition::model()->getBillingStatus($item['item_po_id']) : "Nil";
              }
              ?>
            </td>
            <td width="25%">
              <?= $item['item_remarks'] ?>
            </td>

            <td width="45%">
              <?php
              if (!empty($item['item_po_id'])) {
                $sql = "SELECT purchase_billing_status FROM `jp_purchase` "
                  . " WHERE p_id=" . $item['item_po_id'];

                $po_bill_status_id = Yii::app()->db->createCommand($sql)->queryScalar();

                $data_bll_status = MaterialRequisition::model()->getBillingStatus($item['item_po_id']);

                if (!empty($po_bill_status_id)) {
                  if (($po_bill_status_id == 93) || ($po_bill_status_id == 94)) { ?>
                  <?php } else { ?>
                    <button class="btn btn-info btn-xs ml-1 addPoItembtn"
                      data-table-item-id="<?php echo isset($item['items_table_id']) ? $item['items_table_id'] : ''; ?>">Add PO
                      Items</button>

                  <?php }
                }
                ?>


              <?php } ?>
              <?php if (!empty($model->bill_id) && !empty($model->purchase_id)) {
                if ($item['items_bill_id']) {
                  ?>
                  <button class="btn btn-info btn-xs ml-1 addBillItembtn" style="margin:5px" ;
                    data-table-item-id="<?php echo isset($item['items_table_id']) ? $item['items_table_id'] : ''; ?>">Add Bill
                    Items</button>
                <?php }
              } ?>
            </td>
          </tr>
        <?php } ?>
      </tbody>
    </table>
  </div>
</div>


<!-- Modal -->
<div class="modal fade " id="poModal" role="dialog" aria-labelledby="poModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-long" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="poModalLabel"></h5>
        <button type="button" class="close clsbtn" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <!-- Content will be loaded here -->
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary clsbtn btn btn-primary pdf_excel"
          data-dismiss="modal">Save</button>
      </div>
    </div>
  </div>
</div>
<!-- Bill Modal -->
<div class="modal fade " id="billModal" role="dialog" aria-labelledby="billModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-long" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="billModalLabel"></h5>
        <button type="button" class="close clsbtn" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <!-- Content will be loaded here -->
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary clsbtn btn btn-primary pdf_excel"
          data-dismiss="modal">Save</button>
      </div>
    </div>
  </div>
</div>
<!-- PO ITEMS Modal -->
<div class="modal fade " id="poItemsModal" role="dialog" aria-labelledby="poItemsModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-long" role="document">
    <div class="modal-content">
      <div class="modal-header">

        <button type="button" class="close clsbtn" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <!-- Content will be loaded here -->
      </div>
      <!-- <div class="modal-footer">
        <button type="button" class="btn btn-secondary clsbtn btn btn-primary pdf_excel" data-dismiss="modal">Close</button>
      </div> -->
    </div>
  </div>
</div>
<!--Bill ITEMS-->
<div class="modal fade " id="billitemsModal" role="dialog" aria-labelledby="billitemsModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-long" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="billitemsModalLabel"></h5>
        <button type="button" class="close clsbtn" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <!-- Content will be loaded here -->
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary clsbtn btn btn-primary pdf_excel"
          data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<style>
  .modal-long-po {
    max-width: 1200px;
    width: 95%;
  }

  .select2-dropdown.select2-dropdown--below {
    z-index: 4111 !important;
  }

</style>
<script>

  function reloadParent() {
    location.reload();
  }
  $(document).ready(function () {
    $('#poModal .clsbtn').on('click', function () {

      console.log('Modal is now hidden');
      window.parent.location.reload();
    });
    function checkCheckboxes() {
      const checkboxes = document.querySelectorAll('.mrItemCheckbox');
      const addPoButton = document.getElementById('addPoButton');
      const addBillButton = document.getElementById('addBillButton');
      let isAnyChecked = false;

      checkboxes.forEach(checkbox => {
        if (checkbox.checked) {
          isAnyChecked = true;
        }
      });

      addPoButton.style.display = isAnyChecked ? 'block' : 'none';
      addBillButton.style.display = isAnyChecked ? 'block' : 'none';
    }
    // Add event listeners to checkboxes to call checkCheckboxes function on change
    document.querySelectorAll('.mrItemCheckbox').forEach(checkbox => {
      checkbox.addEventListener('change', checkCheckboxes);
    });
    $('#addPoButton').click(function () {
      var mrId = $("#mrId").val();
      var baseUrl = '<?php echo Yii::app()->baseUrl; ?>';
      var url = baseUrl + '/index.php?r=materialRequisition/loadPoModal&mrId=' + mrId;

      var selectedItems = [];

      // Collect selected item IDs
      $('.mrItemCheckbox:checked').each(function () {
        selectedItems.push($(this).data('item-id'));
      });

      // Convert the array of selected items to a comma-separated string
      var selectedItemsStr = selectedItems.join(',');

      $('.loading-overlay').addClass('is-active');
      // Append selected item IDs to the URL as a query parameter
      var url = baseUrl + '/index.php?r=materialRequisition/loadPoModal&mrId=' + mrId + '&selectedItems=' + selectedItemsStr;

      // Perform AJAX request
      $.get(url, function (data) {
        // Load response into modal body
        $('#poModal .modal-body').html(data);
        // Show modal
        $('#poModal').modal('show');
        // Initialize select2 after the modal is shown and the content is loaded
        $('#poModal .select2').select2({
          dropdownParent: $("#poModal")
        });
        $('.loading-overlay').removeClass('is-active');
      }).fail(function () {
        $('#poModal .modal-body').html('<p>Error loading information</p>');
      });
    });

    $('#billModal .clsbtn').on('click', function () {

      console.log('Modal is now hidden');
      window.parent.location.reload();
    });
    $('#addBillButton,#editBillButton').click(function () {
      var mrId = $("#mrId").val();
      var selectedItems = [];

      // Collect selected item IDs
      $('.mrItemCheckbox:checked').each(function () {
        selectedItems.push($(this).data('item-id'));
      });

      // Convert the array of selected items to a comma-separated string
      var selectedItemsStr = selectedItems.join(',');

      var baseUrl = '<?php echo Yii::app()->baseUrl; ?>';

      var url = baseUrl + '/index.php?r=bills/loadbillModal&mrId=' + mrId + '&selectedItems=' + selectedItemsStr;
      $('.loading-overlay').addClass('is-active');
      $.get(url, function (data) {

        $('#billModal').modal('show');
        $('#billModal .modal-body').html(data);


        $('#billModal .select2').select2({
          dropdownParent: $("#billModal")
        });
        $('.loading-overlay').removeClass('is-active');

      }).fail(function () {
        $('#billModal .modal-body').html('<p>Error loading information</p>');
      });
    });


    $(document).ready(function () {
      $('#poItemsModal .clsbtn').on('click', function () {

        console.log('Modal is now hidden');
        window.parent.location.reload();
      });
      $('.addPoItembtn').click(function () {
        $('.loading-overlay').addClass('is-active');

        var mrId = $("#mrId").val();
        itemId = $(this).data('table-item-id');
        var baseUrl = '<?php echo Yii::app()->baseUrl; ?>';

        var url = baseUrl + '/index.php?r=purchase/loadPoItemsModal&mrId=' + mrId + '&itemId=' + itemId;
        // Load content into modal body
        $('#poItemsModal .modal-body').load(url, function () {
          $('#poItemsModal').modal('show');
          $('.loading-overlay').removeClass('is-active');
        });

        $('#poItemsModal .select2').select2({
          dropdownParent: $("#poItemsModal")
        });


      });
      $('#billitemsModal .clsbtn').on('click', function () {

        console.log('Modal is now hidden');
        window.parent.location.reload();
      });
      $('.addBillItembtn').click(function () {
        $('.loading-overlay').addClass('is-active');
        var mrId = $("#mrId").val();
        itemId = $(this).data('table-item-id');

        var baseUrl = '<?php echo Yii::app()->baseUrl; ?>';
        var url = baseUrl + '/index.php?r=bills/loadbillItemsModal&mrId=' + mrId + '&itemId=' + itemId;
        // Load content into modal body
        $('#billitemsModal').modal('show');
        $('#billitemsModal .modal-body').load(url);
        $('.loading-overlay').removeClass('is-active');
        $('#billitemsModal .select2').select2({
          dropdownParent: $("#billitemsModal")
        });
      });



    });

    // $(".save-btn").click(function(){
    // 	$("#bill_number").trigger("blur");
    // }) 


  });
  $(document).on("click", "#materialrequest_refresh", function () {

    $.ajax({
      "type": "GET",
      "url": "<?php echo Yii::app()->createUrl('materialRequisition/refreshButton'); ?>",
      "dataType": "json",
      "success": function (data) {
        alert(data);
        // setTimeout(function(){
        // window.location.reload(1);
        // }, 1000);
      }
    });
  });

</script>