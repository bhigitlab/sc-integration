<?php
/* @var $this DailyReportController */
/* @var $model Dailyreport */

$this->breadcrumbs=array(
	'Invoice'=>array('index'),
	'Create',
);

$this->menu=array(
	//array('label'=>'List Dailyreport', 'url'=>array('index')),
	//array('label'=>'Manage Dailyreport', 'url'=>array('admin')),
);
?>

<div class="container" id="expense">
    <div class="row">
        <div class="col-md-6">
           <h2>Invoice</h2>
        </div>
        <div class="col-md-6">
            <a href="index.php?r=invoice/admin"><span class="btn btn-default" title="List Invoice" style="margin-top:10px;">List Invoice</span></a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
        <?php $this->renderPartial('_form', array('model'=>$model,'client' => $client)); ?>
    
        </div>
    </div>
</div>