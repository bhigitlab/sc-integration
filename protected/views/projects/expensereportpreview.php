<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/pdfstyle.css">
<?php
    $company_address = Company::model()->findBypk(Yii::app()->user->company_id);
?>

<div class="container pdf_spacing" id="project">
  <h3 class="text-center" style="margin-bottom:0px;">Project Report - Expense wise</h3>
  <?php
  $daybookVT      = !empty($daybookvendor)?$daybookvendor[0]["totalvendorpaid"]:0;
  $daybookSunT    = !empty($daybooksubcon)?$daybooksubcon[0]["subcontractortotalpaid"]:0;
  $vendorVT       = !empty($vendorpayment)?$vendorpayment[0]["vendortotalsum"]:0;
  $totalPayment   = $daybookVT + $daybookSunT + $vendorVT;
  $totalInvoice   = !empty($invoicedata)?$invoicedata[0]["invoicetotal"]:0;
  $invoiceReceipt = !empty($daybookreceipt)?$daybookreceipt[0]["receiptsum"]:0;
  $balanceInvoice = $totalInvoice - $invoiceReceipt;
  $totalBillAmt   = !empty($billdata)?$billdata[0]["billsum"]:0;
  $totalQuotSum   = !empty($quotationdata)?$quotationdata[0]["quotationsum"]:0;
  $totalExpense   = $totalBillAmt + $daybookSunT;
  $balanceExpense = $totalExpense - $totalPayment;
  $subbilltotal   = 0;
  $unbilledtotal  = 0;
  ?>
<div id="contenthtml" class="contentdiv">
<h4 class="text-center">Project Summary of <?php echo $projectdata["name"]; ?></h4>
           <div class="container-fluid">
                <table border="1" class="itemtable">
                                               <tr>
<td colspan="4"><b>Client : </b><?php echo $projectdata["client"]; ?></td>
                   </tr>
                                     <tr>
<td colspan="3"><b>Project Name: </b><?php echo $projectdata["name"]; ?></td>
<td><b>Square Feet: </b><?php echo $projectdata["sqft"]; ?></td>
</tr>
<tr>
<td colspan="3"><b>Work Contract: </b><?php echo $projectdata["caption"]; ?></td>
<?php if ($projectdata["project_type"] == 87) { ?>
<td><b>Square Feet Rate :</b><?php echo $projectdata["sqft_rate"]; ?></td>
<?php } else if ($projectdata["project_type"] == 86) { ?>
<td><b>Percentage : </b><?php echo ($projectdata["percentage"]) ? $projectdata["percentage"] . '%' : ''; ?></td>
<?php } else if ($projectdata["project_type"] == 104) { ?>
<td><b>Total Estimate : </b><?php echo $projectdata["project_totalestimate"]; ?></td>
<?php } ?>
</tr>
<tr>
<td colspan="4"><b>Site: </b><?php echo $projectdata["site"]; ?></td></tr>
<tr>
<td colspan="4" class="whiteborder">&nbsp;</td>
</tr>
</table>
<?php if(!empty($invoicedata)) { ?>
<h4>Invoice</h4>
<table border="1" class="itemtable">
<thead>
<tr>
<th width="5%">Sl No</th>
<th width="50%">Invoice Number</th>
<th width="25%">Date</th>
<th width="20%">Invoice Amount</th>
</tr>
</thead>
<tbody>
<?php
$i = 1;
                    foreach($invoicedata as $invoice) { ?>
<tr>
<td><?php echo $i; ?></td>
<td><?php echo $invoice["inv_no"]; ?></td>
<td><?php echo $invoice["date"]; ?></td>

<td class="text-right"><?php echo Controller::money_format_inr($invoice["subtotal"] + $invoice["tax_amount"],2); ?></td>


</tr>

<?php
 $i = $i + 1;
                    } ?>
</tbody>
<tfoot>
<tr>
<th colspan="3" class="text-right">Total</th>
<th class="text-right"><?php echo Controller::money_format_inr($invoicedata[0]["invoicetotal"],2); ?></th>
</tr>
</tfoot>
</table>
<table border="1" class="itemtable">
<thead>
<tr>
<th width="80%">Total Invoice Amount</th>
<th class="text-right"><?php echo Controller::money_format_inr(round($invoicedata[0]["invoicetotal"]),2); ?></th>
</tr>

</thead>
</table>
<?php } ?>
<?php if(!empty($daybookreceipt)) { ?>
<h4>Invoice Receipt</h4>
<table border="1" class="itemtable">
<thead>
<tr>
<th width="5%">Sl No</th>
<th width="50%">Description</th>
<th width="15%">Date</th>
<th width="15%">Receipt</th>
<th width="15%">Paid</th>
</tr>
</thead>
<tbody>
<?php
$i = 1;
foreach($daybookreceipt as $receipt) { ?>
<tr>
<td><?php echo $i; ?></td>
<td><?php echo $receipt["description"]; ?></td>
<td><?php echo $receipt["date"]; ?></td>
<td class="text-right"><?php echo Controller::money_format_inr($receipt["receipt"],2); ?></td>
<td></td>
</tr>

<?php
$i = $i + 1;
 } ?>
</tbody>
<tfoot>
<tr>
<th colspan="3" class="text-right">Total</th>
<th class="text-right"><?php echo Controller::money_format_inr($daybookreceipt[0]["receiptsum"],2); ?></th>
<th></th>
</tr>
</tfoot>
</table>
<table border="1" class="itemtable">

<thead>
<tr>
<th width="80%">Total Amount Received From Client</th>
<th class="text-right"><?php echo Controller::money_format_inr(round($daybookreceipt[0]["receiptsum"]),2); ?></th>
</tr>
</thead>
</table>
<?php } ?>
<?php if(!empty($billdata)) { ?>
<h4>Bill Details</h4>
<table border="1" class="itemtable">
<thead>
<tr>
<th width="15%">Sl No</th>
<th>Purchase Number</th>
<th>Bill Number</th>
<th>Vendor</th>
<th>Date</th>
<th>Bill Amount</th>
</tr>
</thead>
<tbody>
<?php
$i = 1;
foreach($billdata as $bill) { ?>
<tr>

<td width="15%"><?php echo $i; ?></td>
<td width="20%"><?php echo $bill["purchase_no"]; ?></td>
<td width="20%"><?php echo $bill["bill_number"]; ?></td>
<td width="20%"><?php echo $bill["vendorname"]; ?></td>
<td width="15%"><?php echo $bill["bill_date"]; ?></td>
<td width="20%" class="text-right"><?php echo Controller::money_format_inr($bill["bill_totalamount"],2); ?></td>
</tr>
<?php
  $i = $i + 1;
}
?>

</tbody>
<tfoot>
<tr>
<th colspan="5" class="text-right">Total</th>
<th class="text-right"><?php echo Controller::money_format_inr($billdata[0]["billsum"],2); ?></th>
</tr>
</tfoot>

</table>
<?php } ?>
<?php if(!empty($subconbill)) { ?>
<h4>Subcontractor Bill</h4>

<table border="1" class="itemtable">
<thead>
<tr>
<th width="15%">Sl No</th>
<th>Quotation</th>
<th>Bill Number</th>
<th>Sub Contractor</th>
<th>Date</th>

<th>Bill Amount</th>
</tr>
</thead>
<tbody>
<?php
 $i = 1;
  foreach($subconbill as $subbill) { ?>
<tr>
<td width="15%"><?php echo $i; ?></td>
<td width="20%"><?php echo $subbill["scquotation_decription"]; ?></td>
<td width="20%"><?php echo $subbill["bill_number"]; ?></td>
 <?php
  $subcontractor = Subcontractor::model()->findByPk($subbill["subcontractor_id"]);
?>
<td width="20%"><?php echo $subcontractor->subcontractor_name; ?></td>
<td width="15%"><?php echo $subbill["date"]; ?></td>
<td width="20%" class="text-right"><?php echo Controller::money_format_inr($subbill["total_amount"],2); ?></td>
</tr>
<?php
$subbilltotal = $subbilltotal + $subbill["total_amount"];
$i = $i + 1;
 } ?>
</tbody>
<tfoot>
<tr>
<th colspan="5" class="text-right">Total</th>
<th class="text-right"><?php echo Controller::money_format_inr($subbilltotal,2); ?></th>
</tr>
</tfoot>
</table>

<?php } ?>
<?php if(!empty($unbilledpo)) { ?>
<h4>Un billed Purchase Order</h4>
<table border="1" class="itemtable">

<thead>
<tr>
<th width="15%">Sl No</th>
<th>Purchase Number</th>
<th>Vendor</th>
<th>Expense Head</th>
<th>Date</th>
<th>Amount</th>
</tr>
</thead>
<tbody>
<?php
$i = 1;
foreach($unbilledpo as $unbill) {
?>
<tr>
<td width="15%"><?php echo $i; ?></td>
<td width="20%"><?php echo $unbill["purchase_no"]; ?></td>
<td width="20%"><?php echo $unbill["vname"]; ?></td>
<td width="20%"><?php echo $unbill["expensehead"]; ?></td>
<td width="15%"><?php echo $unbill["purchase_date"]; ?></td>
<td width="20%" class="text-right"><?php echo Controller::money_format_inr($unbill["unbilled_amount"],2); ?></td>
</tr>
<?php
$unbilledtotal = $unbilledtotal + $unbill["unbilled_amount"];
$i = $i + 1;
}
?>
</tbody>
<tfoot>
<tr>
<th colspan="5" class="text-right">Total</th>
<th class="text-right"><?php echo Controller::money_format_inr($unbilledtotal,2); ?></th>
</tr>
</tfoot>
</table>
<?php } ?>
<?php
$totalBillExpense   = $totalBillAmt + $subbilltotal + $unbilledtotal;
$balanceBillExpense = $totalBillExpense - $totalPayment;
?>
<table border="1" class="itemtable">
<thead>
<tr>
<th width="80%">Total Bill Amount</th>
<th class="text-right"><?php echo Controller::money_format_inr(round($totalBillExpense), 2); ?></th>
</tr>
</thead>
</table>
<?php if(!empty($quotationdata)) { ?>
<h4>Subcontractor Quotation</h4>
<table border="1" class="itemtable">
<thead>
<tr>
<th width="15%">Sl No</th>
<th width="20%">Subcontractor</th>
<th width="40%">Description</th>
<th width="15%">Date</th>
<th width="20%">Quotation Amount</th>
</tr>
</thead>
<tbody>
<?php
$i = 1;
foreach($quotationdata as $quotation) {
?>
<tr>
<td><?php echo $i; ?></td>
<td><?php echo $quotation["subcontractor_name"]; ?></td>
<td><?php echo $quotation["scquotation_decription"]; ?></td>
<td><?php echo $quotation["scquotation_date"]; ?></td>
<td class="text-right"><?php echo Controller::money_format_inr($quotation["scquotation_amount"],2); ?></td>
</tr>
<?php
$i = $i + 1;
}
?>
</tbody>
<tfoot>
<tr>
<th colspan="4" class="text-right">Total</th>
<th class="text-right"><?php echo Controller::money_format_inr($quotationdata[0]["quotationsum"],2); ?></th>
</tr>
</tfoot>
</table>
<table border="1" class="itemtable">
<thead>
<tr>
<th width="80%">Total Subcontractor Quotation</th>
<th class="text-right"><?php echo Controller::money_format_inr(round($quotationdata[0]["quotationsum"]),2); ?></th>
</tr>
</thead>
</table>
<?php } ?>
<?php if(!empty($daybookvendor)) { ?>
<h4>Daybook Vendor Payment</h4>
<table border="1" class="itemtable">
<tr>
<th width="15%">Sl No</th>
<th width="35%">Description</th>
<th width="15%">Expense Head</th>
<th width="15%">Date</th>
<th width="15%">Receipt</th>
<th width="15%">Paid</th>
</tr>
<?php
$c  = 1;
foreach($daybookvendor as $vendor) {
if($c != $vendor["vendorcount"]) {
$c = $c + 1;
} else {
$vendorCount[] = $vendor["vendorcount"];
$c = 1;
}
}
$vendorCount    = array_values($vendorCount);
$vendorNum      = count($vendorCount);
$totalV = 0;
for($i = 0; $i < $vendorNum; $i++) {
$vendorC    = $vendorCount[$i];
for($j = 0; $j < $vendorC; $j++) {
$vCount = $daybookvendor[$j]["vendorcount"];
if($j == 0) {
?>
<tr>
<td colspan="6" width="100%"><b><?php echo $daybookvendor[$totalV]["vendor"]; ?></b></td>
</tr>
<?php
}
?>
<tr>
<td width="5"><?php echo $j+1; ?></td>
<td width="50"><?php echo $daybookvendor[$totalV]["description"]; ?></td>
<td width="50"><?php echo $daybookvendor[$totalV]["expensetype"]; ?></td>
<td width="15"><?php echo $daybookvendor[$totalV]["date"]; ?></td>
<td width="15"></td>
<td width="15" class="text-right"><?php echo Controller::money_format_inr($daybookvendor[$totalV]["paid"],2); ?></td>
</tr>
<?php
if($vendorC == $j + 1) {
?>
<tr>
<th colspan="5" class="text-right"><b>Total</b></th>
<th class="text-right"><b><?php echo Controller::money_format_inr(round($daybookvendor[$totalV]["vendorpaid"]),2); ?></b></th>
</tr>
<tr>
<th colspan="5" class="text-right"><b>Balance To Be Paid</b></th>
<th class="text-right"><b><?php echo Controller::money_format_inr(round($daybookvendor[$totalV]["vendorsubsum"] - $daybookvendor[$totalV]["vendorpaid"]),2); ?></b></th>
</tr>
<?php
}
$totalV = $totalV + 1;
}
}
?>
</table>
<table border="1" class="itemtable">
<thead>
<tr>
<th width="80%">Total Payment to Vendor</th>
<th class="text-right"><?php echo Controller::money_format_inr(round($daybookVT),2); ?></th>
</tr>
<tr>
<th width="80%">Balance to be Paid</th>
<th class="text-right"><?php echo Controller::money_format_inr(round($totalBillAmt - $daybookVT),2); ?></th>
</tr>
</thead>
</table>
<?php } ?>
<?php if(!empty($daybooksubcon)) { ?>
<h4>Subcontractor Payment</h4>
<table border="1" class="itemtable">
<tr>
<th width="15%">Sl No</th>
<th width="50%">Description</th>
<th width="15%">Date</th>
<th width="15%">Receipt</th>
<th width="15%">Paid</th>
</tr>
<?php
$c  = 1;
foreach($daybooksubcon as $subcon) {
if($c != $subcon["subcontractorcount"]) {
$c = $c + 1;
} else {
$subconCount[] = $subcon["subcontractorcount"];
$c = 1;
}
}
$subconCount    = array_values($subconCount);
$subconNum      = count($subconCount);
$totalS = 0;
                for($i = 0; $i < $subconNum; $i++) {
$subconC    = $subconCount[$i];
for($j = 0; $j < $subconC; $j++) {
$sCount = $daybooksubcon[$j]["subcontractorcount"];
if($j == 0) {
?>
<tr>
<td colspan="5" width="100%"><b><?php echo $daybooksubcon[$totalS]["sname"]; ?></b></td>
</tr>
<?php
}
                    ?>
<tr>
<td width="5"><?php echo $j+1; ?></td>
<td width="50"><?php echo $daybooksubcon[$totalS]["description"]; ?></td>
<td width="15"><?php echo $daybooksubcon[$totalS]["date"]; ?></td>
<td width="15"></td>
<td width="15" class="text-right"><?php echo Controller::money_format_inr($daybooksubcon[$totalS]["paid"],2); ?></td>
</tr>
<?php
if($subconC == $j + 1) {
?>
<tr>
<th colspan="4" class="text-right"><b>Total</b></th>
<th class="text-right"><b><?php echo Controller::money_format_inr(round($daybooksubcon[$totalS]["subcontractorpaid"]),2); ?></b></th>
</tr>
<?php
}
$totalS = $totalS + 1;
}
}
?>

</table>
<table border="1" class="itemtable">
<thead>
<tr>
<th width="80%">Total Payment to Subcontractor</th>
<th class="text-right"><?php echo Controller::money_format_inr(round($daybooksubcon[0]["subcontractortotalpaid"]),2); ?></th>
</tr>
</thead>
</table>
<?php } ?>
<?php if(!empty($vendorpayment)) { ?>
<h4>Bulk Vendor Payment</h4>
<table border="1" class="itemtable">
<tr>
<th width="15%">Sl No</th>
<th width="50%">Description</th>
<th width="15%">Date</th>
<th width="15%">Receipt</th>
<th width="15%">Paid</th>
</tr>
<?php
$c  = 1;
foreach($vendorpayment as $vendor) {
 if($c != $vendor["vendorcount"]) {
   $c = $c + 1;
 } else {
   $vendorCount1[] = $vendor["vendorcount"];

    $c = 1;
   }
 }
 $vendorCount1    = array_values($vendorCount1);
 $vendorNum1      = count($vendorCount1);
 $totalV1 = 0;
 for($i = 0; $i < $vendorNum1; $i++) {
   $vendorC1    = $vendorCount1[$i];
   for($j = 0; $j < $vendorC1; $j++) {
     $vCount1 = $vendorpayment[$j]["vendorcount"];
     if($j == 0) {
       ?>
       <tr>
         <td colspan="5" width="100%"><b><?php echo $vendorpayment[$totalV1]["vendor"]; ?></b></td>
        </tr>
      <?php
    }
     ?>
     <tr>
        <td width="5"><?php echo $j+1; ?></td>
        <td width="50"><?php echo $vendorpayment[$totalV1]["description"]; ?></td>
        <td width="15"><?php echo $vendorpayment[$totalV1]["date"]; ?></td>
        <td width="15"></td>
        <td width="15" class="text-right"><?php echo Controller::money_format_inr($vendorpayment[$totalV1]["amount"] + $vendorpayment[$totalV1]["tax_amount"],2); ?></td>
       </tr>
        <?php
        if($vendorC1 == $j + 1) {
           ?>
            <tr>
              <th colspan="4" class="text-right"><b>Total</b></th>
              <th class="text-right"><b><?php echo Controller::money_format_inr(round($vendorpayment[$totalV1]["vendorsum"]),2); ?></b></th>
            </tr>
          <?php
        }
        $totalV1 = $totalV1 + 1;
      }
    }
    ?>
   </table>
   <table border="1" class="itemtable">
     <thead>
        <tr>
           <th width="80%">Total Payment to Vendor</th>
           <th class="text-right"><?php echo Controller::money_format_inr(round($vendorpayment[0]["vendortotalsum"]),2); ?></th>
         </tr>
       </thead>
     </table>
   <?php } ?>
   <table border="1" class="itemtable">
     <thead>
        <tr>
          <th width="80%">Total Payment</th>
          <th class="text-right"><?php echo Controller::money_format_inr(round($totalPayment),2); ?></th>
        </tr>
      </thead>
    </table>
    <table border="1" class="itemtable">
      <tr>
        <td width="80%">TOTAL INVOICE</td>
        <td class="text-right"><?php echo Controller::money_format_inr(round($totalInvoice),2); ?></td>
      </tr>
      <tr>
        <td width="80%">AMOUNT RECEIVED</td>
        <td class="text-right"><?php echo Controller::money_format_inr(round($invoiceReceipt),2); ?></td>
      </tr>
      <tr>
        <td width="80%">BALANCE TO BE RECEIVED</td>
        <td class="text-right"><?php echo Controller::money_format_inr(round($balanceInvoice),2); ?></td>
      </tr>
      <tr>
        <td width="80%">TOTAL EXPENSE</td>
        <td class="text-right"><?php echo Controller::money_format_inr(round($totalBillExpense),2); ?></td>
      </tr>
      <tr>
        <td width="80%">AMOUNT PAID</td>
        <td class="text-right"><?php echo Controller::money_format_inr(round($totalPayment),2); ?></td>
      </tr>
      <tr>
        <td width="80%">BALANCE EXPENSE TO BE PAID</td>
        <td class="text-right"><?php echo Controller::money_format_inr(round($balanceBillExpense),2); ?></td>
      </tr>
    </table>
  </div>
</div>
</div>

