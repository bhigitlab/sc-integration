<?php
/* @var $this ProjectsController */
/* @var $model Projects */
/* @var $form CActiveForm */
?>

<div class="custom-form-style">
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'vendors-form',
        // 'enableAjaxValidation' => true,
        'enableClientValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true,
            'validateOnType' => false,
        ),
    ));
    ?>
    <div>
        <div class="row">
            <div class="form-group col-xs-12 col-sm-6 col-md-3">
                <?php echo $form->labelEx($model, 'name'); ?>
                <?php echo $form->textField($model, 'name', array('size' => 60, 'maxlength' => 100, 'class' => 'form-control')); ?>
                <?php echo $form->error($model, 'name'); ?>
            </div>
            <div class="form-group col-xs-12 col-sm-6 col-md-3">
                <?php echo $form->labelEx($model, 'phone'); ?>
                <?php echo $form->textField($model, 'phone', array('class' => 'form-control')); ?>
                <?php echo $form->error($model, 'phone'); ?>
            </div>
            <div class="form-group col-xs-12 col-sm-6 col-md-3">
                <?php echo $form->labelEx($model, 'email_id'); ?>
                <?php echo $form->textField($model, 'email_id', array('class' => 'form-control')); ?>
                <?php echo $form->error($model, 'email_id'); ?>
            </div>
            <div class="form-group col-xs-12 col-sm-6 col-md-3">
                <?php echo $form->labelEx($model, 'status'); ?>
                <div class="radio_btn">
                    <?php
                    echo $form->radioButtonList($model, 'status', CHtml::listData(Status::model()->findAll(
                        array(
                            'select' => array('sid,caption'),
                            'condition' => 'status_type="active_status"',
                            'order' => 'caption',
                            'distinct' => true
                        )
                    ), 'sid', 'caption'), array('separator' => '', 'class' => 'statusall'));
                    ?>
                </div>
                <?php echo $form->error($model, 'status'); ?>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-xs-12 col-sm-6 col-md-3">
                <?php echo $form->labelEx($model, 'gst_no'); ?>
                <?php echo $form->textField($model, 'gst_no', array('size' => 60, 'maxlength' => 100, 'class' => 'form-control')); ?>
                <?php echo $form->error($model, 'gst_no'); ?>
            </div>
            <div class="form-group col-xs-12 col-sm-6 col-md-3">
                <?php echo $form->labelEx($model, 'pan_no'); ?>
                <?php echo $form->textField($model, 'pan_no', array('size' => 60, 'maxlength' => 100, 'class' => 'form-control')); ?>
                <?php echo $form->error($model, 'pan_no'); ?>
            </div>
            <div class="form-group col-xs-12 col-sm-6 col-md-3">
                <?php echo $form->labelEx($model, 'type'); ?>
                <?php echo $form->dropDownList($model, 'type', array(0 => 'registered', 1 => 'unregistered'), array('class' => 'form-control js-example-basic-single', 'empty' => '-Select Type-', 'style' => 'width:100%')); ?>
                <?php echo $form->error($model, 'type'); ?>
            </div>
            <div class="form-group col-xs-12 col-sm-6 col-md-3">
                <?php echo $form->labelEx($model, 'payment_date_range'); ?>
                <?php echo $form->textField($model, 'payment_date_range', array('size' => 60, 'maxlength' => 100, 'class' => 'form-control')); ?>
                <?php echo $form->error($model, 'payment_date_range'); ?>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-xs-12 col-sm-6 col-md-3">
                <label>Expense Head :</label>
                <ul class="checkboxList">
                    <li><input type="checkbox" id='select_alltype' value='0'>Select All</li>
                    <?php
                    $typelist = ExpenseType::model()->findAll(array('condition' => 'expense_type IN (96,98)'));
                    $assigned_types_array = array();
                    if (!$model->isNewRecord) {
                        $assigned_types = VendorExpType::model()->findAll(array('condition' => 'vendor_id=' . $model->vendor_id));
                        foreach ($assigned_types as $atype) {
                            $assigned_types_array[] = $atype['type_id'];
                        }
                    }

                    foreach ($typelist as $type) {
                        ?>
                        <li><input type="checkbox" class="checkboxtype" <?php echo (in_array($type->type_id, $assigned_types_array) ? 'checked="checked"' : ''); ?> name="type[]"
                                value='<?php echo $type->type_id ?>' /> <?php echo $type->type_name ?>
                        </li>
                        <?php
                    }
                    ?>
                </ul>
            </div>
            <div class="form-group col-xs-12 col-sm-6 col-md-3">
                <?php echo $form->labelEx($model, 'company_id'); ?>
                <ul class="checkboxList">
                    <?php
                    $user = Users::model()->findByPk(Yii::app()->user->id);
                    $arrVal = explode(',', $user->company_id);
                    $newQuery = "";
                    foreach ($arrVal as $arr) {
                        if ($newQuery)
                            $newQuery .= ' OR';
                        $newQuery .= " FIND_IN_SET('" . $arr . "', id)";
                    }
                    $typelist = Company::model()->findAll(array('condition' => $newQuery));
                    $assigned_company_array = array();
                    if (!$model->isNewRecord) {
                        $assigned_types = Vendors::model()->find(array('condition' => 'vendor_id=' . $model->vendor_id));
                        $assigned_company_array = explode(",", $assigned_types->company_id);
                    } else {
                        $assigned_company_array = "";
                    }
                    echo CHtml::checkBoxList('Vendors[company_id]', $assigned_company_array, CHtml::listData($typelist, 'id', 'name'), array('checkAll' => 'Check all', 'template' => '<li class="checkboxtype">{input}{label}</li>', 'separator' => ''));
                    ?>
                </ul>
                <?php echo $form->error($model, 'company_id'); ?>
            </div>
            <div class="form-group col-xs-12 col-sm-6 col-md-3 textArea-box">
                <?php echo $form->labelEx($model, 'description'); ?>
                <?php echo $form->textArea($model, 'description', array('rows' => 3, 'cols' => 50, 'class' => 'form-control')); ?>
                <?php echo $form->error($model, 'description'); ?>
            </div>
            <div class="form-group col-xs-12 col-sm-6 col-md-3 textArea-box">
                <?php echo $form->labelEx($model, 'address'); ?>
                <?php echo $form->textArea($model, 'address', array('rows' => 3, 'cols' => 50, 'class' => 'form-control')); ?>
                <?php echo $form->error($model, 'address'); ?>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-xs-12 col-sm-6 col-md-3">
                <?php echo $form->checkBox($model, 'vendor_type', array('value' => 2, 'uncheckValue' => 1, 'checked' => ($model->vendor_type == 1) ? false : true)); ?>
                <?php echo $form->labelEx($model, 'vendor_type'); ?>
                <?php echo $form->error($model, 'vendor_type'); ?>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-xs-12 text-right">
                <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-info')); ?>
                <?php if (!$model->isNewRecord) {
                    echo CHtml::Button('Close', array('onclick' => 'closeaction(this,event)', 'class' => 'btn btn-other'));
                } else {
                    echo CHtml::ResetButton('Reset', array('style' => 'margin-right:3px;', 'class' => 'btn btn-default'));
                    echo CHtml::ResetButton('Close', array('onclick' => 'closeaction(this,event)', 'class' => 'btn btn-other'));
                } ?>
            </div>
        </div>
    </div>
    <?php $this->endWidget(); ?>

</div><!-- form -->

<script>
    $(document).ready(function () {

        //select all checkboxes
        $("#select_all").change(function () { //"select all" change
            var status = this.checked; // "select all" checked status
            $('.checkbox').each(function () { //iterate all listed checkbox items
                this.checked = status; //change ".checkbox" checked status
            });
        });

        $('.checkbox').change(function () { //".checkbox" change
            //uncheck "select all", if one of the listed checkbox item is unchecked
            if (this.checked == false) { //if this item is unchecked
                $("#select_all")[0].checked = false; //change "select all" checked status to false
            }

            //check "select all" if all checkbox items are checked
            if ($('.checkbox:checked').length == $('.checkbox').length) {
                $("#select_all")[0].checked = true; //change "select all" checked status to true
            }
        });


        $("#select_alltype").change(function () { //"select all" change
            var status = this.checked; // "select all" checked status
            $('.checkboxtype').each(function () { //iterate all listed checkbox items
                this.checked = status; //change ".checkbox" checked status
            });
        });


        $('.checkboxtype').change(function () { //".checkbox" change
            //uncheck "select all", if one of the listed checkbox item is unchecked
            if (this.checked == false) { //if this item is unchecked
                $("#select_alltype")[0].checked = false; //change "select all" checked status to false
            }

            //check "select all" if all checkbox items are checked
            if ($('.checkboxtype:checked').length == $('.checkboxtype').length) {
                $("#select_alltype")[0].checked = true; //change "select all" checked status to true
            }
        });


    });
    function onlySpecialchars(str) {
        var regex = /^[^a-zA-Z0-9]+$/;
        var message = "";
        var disabled = false;
        var matchedAuthors = regex.test(str);

        if (matchedAuthors) {
            var message = "Special characters not allowed";
            var disabled = true;
        }

        return { "message": message, "disabled": disabled };
    }

    $("#Vendors_name").keyup(function () {
        var response = onlySpecialchars(this.value);
        $(this).siblings(".errorMessage").show().html(response.message).addClass('d-block');
        $(".btn-info").attr('disabled', response.disabled);

    });
</script>