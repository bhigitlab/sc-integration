<?php
/* @var $this PurchaseCategoryController */
/* @var $model PurchaseCategory */

$this->breadcrumbs=array(
	'Purchase Categories'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List PurchaseCategory', 'url'=>array('index')),
	array('label'=>'Create PurchaseCategory', 'url'=>array('create')),
	array('label'=>'View PurchaseCategory', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage PurchaseCategory', 'url'=>array('admin')),
);
?>

<h1>Update PurchaseCategory <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>