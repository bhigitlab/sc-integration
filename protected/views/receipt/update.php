<?php
/* @var $this ReceiptController */
/* @var $model Receipt */

$this->breadcrumbs=array(
	'Receipts'=>array('index'),
	$model->rec_id=>array('view','id'=>$model->rec_id),
	'Update',
);


?>

<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Edit Receipt</h4>
        </div>

        <?php echo $this->renderPartial('_form', array('model' => $model)); ?>
    </div>
</div>