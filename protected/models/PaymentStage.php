
<?php

/**
 * This is the model class for table "{{payment_stage}}".
 *
 * The followings are the available columns in table '{{payment_stage}}':
 * @property integer $id
 * @property integer $quotation_id
 * @property string $payment_stage
 * @property double $stage_percent
 * @property double $stage_amount
 * @property string $invoice_status
 * @property integer $created_by
 * @property string $created_date
 * @property integer $updated_by
 * @property string $updated_date
 */
class PaymentStage extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{payment_stage}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('quotation_id, payment_stage, stage_percent, stage_amount, created_by', 'required'),
            array('quotation_id, created_by, updated_by', 'numerical', 'integerOnly'=>true),
            array('stage_percent, stage_amount', 'numerical'),
            array('invoice_status', 'length', 'max'=>1),
            array('created_date', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, quotation_id, payment_stage, stage_percent, stage_amount, invoice_status, created_by, created_date, updated_by, updated_date', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'quotation_id' => 'Quotation',
            'payment_stage' => 'Payment Stage',
            'stage_percent' => 'Stage Percent',
            'stage_amount' => 'Stage Amount',
            'invoice_status' => 'Invoice Status',
            'created_by' => 'Created By',
            'created_date' => 'Created Date',
            'updated_by' => 'Updated By',
            'updated_date' => 'Updated Date',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('quotation_id',$this->quotation_id);
        $criteria->compare('payment_stage',$this->payment_stage,true);
        $criteria->compare('stage_percent',$this->stage_percent);
        $criteria->compare('stage_amount',$this->stage_amount);
        $criteria->compare('invoice_status',$this->invoice_status,true);
        $criteria->compare('created_by',$this->created_by);
        $criteria->compare('created_date',$this->created_date,true);
        $criteria->compare('updated_by',$this->updated_by);
        $criteria->compare('updated_date',$this->updated_date,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return PaymentStage the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}