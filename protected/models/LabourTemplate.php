<?php

/**
 * This is the model class for table "{{labour_template}}".
 *
 * The followings are the available columns in table '{{labour_template}}':
 * @property integer $id
 * @property string $template_label
 * @property string $labour_type
 *
 * The followings are the available model relations:
 * @property ProjectLabour[] $projectLabours
 */
class LabourTemplate extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{labour_template}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('template_label, labour_type,description', 'required'),
            array('template_label, labour_type', 'length', 'max'=>300),
            array('template_label', 'unique', 'message' => 'This template label has already been taken.'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, template_label, labour_type,description', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'projectLabours' => array(self::HAS_MANY, 'ProjectLabour', 'template_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'template_label' => 'Template Label',
            'labour_type' => 'Labour Type',
            'description' => 'Description'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('template_label',$this->template_label,true);
        $criteria->compare('labour_type',$this->labour_type,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination' => false
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return LabourTemplate the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}