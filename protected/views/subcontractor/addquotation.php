<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.min.js"></script>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/message/css/jquery.message.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/message/js/jquery.message.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/tableHeadFixer.js"></script>
<script src="<?php echo Yii::app()->baseUrl . '/ckeditor/ckeditor.js'; ?>"></script>

<script>
	var shim = (function() {
		document.createElement('datalist');
	})();
</script>
<style>
	.permission_style {
		background: #f8cbcb !important;
	}
</style>
<script>
	$(function() {
		$("#datepicker").datepicker({
			dateFormat: 'dd-mm-yy'
		}).datepicker("setDate", new Date());
		$("#item_date").datepicker({
			dateFormat: 'dd-mm-yy'
		}).datepicker("setDate", new Date());
	});
</script>
<style>
	.dropdown {
		position: relative;
		display: inline-block;
	}

	.dropdown::before {
		position: absolute;
		content: " \2193";
		top: 0px;
		right: -8px;
		height: 20px;
		width: 20px;
	}

	button#caret {
		border: none;
		background: none;
		position: absolute;
		top: 0px;
		right: 0px;
	}

	.invoicemaindiv th,
	.invoicemaindiv td {
		padding: 10px;
		text-align: left;
	}

	.addcolumn {
		cursor: pointer;
		background-color: black;
		border: none;
		color: white;
		text-align: center;
		text-decoration: none;
		margin: 2px;
		font-size: 30px;
		width: 40px;
	}

	.invoicemaindiv .pull-right,
	.invoicemaindiv .pull-left {
		float: none !important;
	}

	.add_selection {
		background: #000;
	}

	.toast-container {
		width: 350px;
	}

	.toast-position-top-right {
		top: 57px;
		right: 6px;
	}

	.toast-item-close {
		background-image: none;
		cursor: pointer;
		width: 12px;
		height: 12px;
		text-align: center;
		border-radius: 2px;
	}

	.toast-item-image {
		font-size: 24px;
	}

	.toast-item-close:hover {
		color: red;
	}

	.toast-item {
		border: transparent;
		border-radius: 3px;
		font-size: 10px;
		opacity: 1;
		background-color: rgba(34, 45, 50, 0.8);
	}

	.toast-item-wrapper p {
		margin: 0px 5px 0px 42px;
		font-size: 14px;
		text-align: justify;
	}


	.toast-type-success {
		background-color: #00A65A;
		border-color: #00A65A;
	}

	.toast-type-error {
		background-color: #DD4B39;
		border-color: #DD4B39;
	}

	.toast-type-notice {
		background-color: #00C0EF;
		border-color: #00C0EF;
	}

	.toast-type-warning {
		background-color: #F39C12;
		border-color: #F39C12;
	}

	.span_class {
		min-width: 70px;
		display: inline-block;
	}

	.remark {
		display: none;
	}

	.padding-box {
		padding: 3px 0px;
		min-height: 17px;
		display: inline-block;
	}

	th {
		height: auto;
	}

	.quantity,
	.rate {
		max-width: 80px;
	}

	.gsts input {
		width: 50px;
	}

	.amt_sec {
		float: right;
	}

	.amt_sec:after,
	.padding-box:after {
		display: block;
		content: '';
		clear: both;
	}

	.client_data {
		margin-top: 6px;
	}

	#previous_details {
		position: fixed;
		top: 94px;
		left: 70px;
		right: 70px;
		z-index: 2;
		max-width: 1150px;
		margin: 0 auto;
	}


	*:focus {
		border: 1px solid #333;
		box-shadow: 0 0 6px 1px rgba(0, 0, 0, 0.25);
	}

	table td a:focus {
		border: 1px solid transparent;
		box-shadow: none;
	}

	.addrow tr {
		background-color: #fafafa;
	}

	.tooltip-hiden {
		width: auto;
	}

	.payment_entries p {
		padding: 12px;
	}

	.payment_entries p span {
		padding-left: 12px;
	}

	.payment_entries p span a {
		cursor: pointer;
	}

	@media(max-width: 767px) {

		.quantity,
		.rate,
		.small_class {
			width: auto !important;
		}

		.padding-box {
			float: right;
		}

	}

	@media(min-width: 767px) {
		.invoicemaindiv .pull-right {
			float: right !important;
		}

		.invoicemaindiv .pull-left {
			float: left !important;
		}
	}
</style>

<div class="container">
	<div class="loading-overlay">
		<span class="fa fa-spinner fa-3x fa-spin"></span>
	</div>
	<div class="quotationmaindiv">
		<h2 class="border-bottom">Quotation7</h2>
		<div id="loading"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/ajax-loader.gif"></div>
		<div id="msg_box"></div>
		<form id="pdfvals1" method="post" action="<?php echo $this->createAbsoluteUrl('purchase/addpurchase'); ?>">
			<input type="hidden" name="remove" id="remove" value="">
			<input type="hidden" name="quotation_id" id="quotation_id" value="0">
			<div class="block_sec">
				<div class="row">
					<div class="col-md-2">
						<div class="form-group">
							<?php
							$user = Users::model()->findByPk(Yii::app()->user->id);
							$arrVal = explode(',', $user->company_id);
							$newQuery = "";
							foreach ($arrVal as $arr) {
								if ($newQuery) $newQuery .= ' OR';
								$newQuery .= " FIND_IN_SET('" . $arr . "', id)";
							}
							$companyInfo = Company::model()->findAll(array('condition' => $newQuery));
							?>
							<label class="d-block">COMPANY : <span id="client_label"></span></label>
							<select name="company_id" class="inputs target company_id" id="company_id" style="width:190px">
								<option value="">Choose Company</option>
								<?php
								foreach ($companyInfo as $key => $value) {
								?>
									<option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
								<?php
								}
								?>
							</select>
						</div>
					</div>
					<div class="col-md-2">
						<div class="form-group">
							<label class="d-block">PROJECT : <span id="client_label"></span></label>
							<select name="project" class="inputs target project" id="project" style="width:190px">
								<option value="">Choose Project</option>
								<?php
								foreach ($project as $key => $value) {
								?>
									<option value="<?php echo $value['pid']; ?>"><?php echo $value['name']; ?></option>
								<?php
								}
								?>
							</select>
						</div>
					</div>
					<div class="col-md-2">
						<div class="form-group">
							<label class="d-block">SUB : <span id="project_label"></span></label>
							<select name="subcontractor" class="inputs target subcontractor" id="subcontractor" style="width:190px">
								<option value="">Select subcontractor</option>
								<?php
								foreach ($subcontractor as $key => $value) {
								?>
									<option value="<?php echo $value['subcontractor_id']; ?>"><?php echo $value['subcontractor_name']; ?></option>
								<?php
								}
								?>
							</select>
						</div>
					</div>
					<div class="col-md-2">
						<div class="form-group">
							<label>DATE : </label>
							<input type="text" value="<?php echo ((isset($date) && $date != '') ? date("d-m-Y", strtotime($date)) : ''); ?>" id="datepicker" class="txtBox date inputs target form-control" name="date" placeholder="Please click to edit">
						</div>
					</div>
					<div class="col-md-2">
						<div class="form-group">
							<label>SC QUOTATION NO : </label>
							<input type="text" required value="" class="txtBox inputs target check_type inv_no form-control" name="quotation_no" id="quotation_no" placeholder="">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-2 ">
						<label>Expense Head:</label>
						<select class="inputs target txtBox expense_head form-control" id="expense_head" name="expense_head[]">
							<option value="">-Select Expense Head-</option>
							<?php foreach ($expensehead as $expense) { ?>
								<option value="<?php echo $expense["type_id"]; ?>"><?php echo $expense["type_name"]; ?></option>
							<?php } ?>
						</select>
					</div>
					<div class="col-md-4 pr-0">
						<div class="form-group">
							<label class="d-block">DESCRIPTION : </label>
							<input type="text" required value="" class="txtBox inputs target check_type inv_no form-control" name="description" id="description" placeholder="">
						</div>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			<div id="msg"></div>
			<div id="previous_details"></div>
			<div class="purchase_items panel panel-default">
				<div class="panel-heading">
					<h3>Quotation Entries</h3>
				</div>
				<div class="panel-body">
					<div class="row">
						<?php if (Yii::app()->user->role == 7) { ?>
							<div class="col-md-2 ">
								<label>Expense Head:</label>
								<select class="inputs target txtBox expense_head form-control" id="expense_head" name="expense_head[]">
									<option value="">-Select Expense Head-</option>
									<?php foreach ($expensehead as $expense) { ?>
										<option value="<?php echo $expense["type_id"]; ?>"><?php echo $expense["type_name"]; ?></option>
									<?php } ?>
								</select>
							</div>
						<?php } else { ?>
							<div class="col-md-2 quantity_div">
								<label>Expense Head:</label>
								<select class="inputs target txtBox expense_head form-control" id="expense_head" name="expense_head[]">
									<option value="">-Select Expense Head-</option>
									<?php foreach ($expensehead as $expense) { ?>
										<option value="<?php echo $expense["type_id"]; ?>"><?php echo $expense["type_name"]; ?></option>
									<?php } ?>
								</select>
							</div>
							<div class="col-md-2 quantity_div">
								<label>Quotation Description:</label>
								<input type="text" class="inputs target txtBox item_description form-control" id="item_description" name="item_description[]" placeholder="" />
							</div>
						<?php } ?>
						<div class="col-md-2 quantity_div">
							<label>Date:</label>
							<input type="text" value="<?php echo ((isset($date) && $date != '') ? date("d-m-Y", strtotime($date)) : ''); ?>" id="item_date" class="txtBox item_date inputs target form-control" name="item_date" placeholder="Please click to edit" readonly="true">
						</div>
						<div class="col-md-1">
							<label>Amount:</label>
							<input type="text" class="inputs target txtBox amount allownumericdecimal form-control" id="item_amount" name="item_amount[]" placeholder="" />
						</div>
						<div class="col-md-5 text-right mt">
							<label>&nbsp;</label>
							<input type="button" class="item_save btn btn-info" id="0" value="Save">
						</div>
					</div>
				</div>
			</div>
	</div>


	<div id="parent" class="subq_parent">
		<table border="1" class="table" id="fixtable">
			<thead>
				<tr>
					<th>Sl.No</th>
					<th>Expense Head</th>
					<th>Quotation Description</th>
					<th>Date</th>
					<th>Amount</th>
					<th></th>
				</tr>
			</thead>
			<tbody class="addrow">

				</tr>
			</tbody>
			<tfoot>
				<tr>
					<th></th>
					<th colspan="3" class="text-right">GRAND TOTAL: </th>
					<th class="text-right">
						<div id="grand_total">0.00</div>
					</th>
					<th></th>
				</tr>
			</tfoot>

		</table>
	</div>
	<div style="padding-right: 0px;" class="col-md-offset-9 col-md-3 col-sm-offset-8 col-sm-4 col-xs-offset-6 col-xs-6">
		<input type="hidden" value="<?php echo ((isset($subtot) && $subtot != '') ? $subtot : ''); ?>" class="txtBox pastweek" readonly=ture name="subtot" /></td>
		<input type="hidden" value="<?php echo ((isset($grand) && $grand != '') ? $grand : ''); ?>" class="txtBox pastweek grand" name="grand" readonly=true />
	</div>
	</form>
	<?php $this->renderPartial('_payment_entries', array('scquotation_model' => $model, 'model' => $payment_model, 'payment_entries' => array()))
	?>
	<div class="form-group">
		<label class="d-block">Notes :- </label>
		<input type="text" required value="" class="txtBox inputs target" name="note" id="note" placeholder="">
	</div>
</div>

<script>
	CKEDITOR.replace('note');
	var user_role = '<?php echo Yii::app()->user->role; ?>';
	$(document).ready(function() {
		$('#loading').hide();
		$("#fixtable").tableHeadFixer({
			'left': false,
			'foot': true,
			'head': true
		});
		$(".popover-test").popover({
			html: true,
			content: function() {
				return $(this).next('.popover-content').html();
			}
		});
		$('[data-toggle=popover]').on('click', function(e) {
			$('[data-toggle=popover]').not(this).popover('hide');
		});
		$('body').on('hidden.bs.popover', function(e) {
			$(e.target).data("bs.popover").inState.click = false;
		});
		$('body').on('click', function(e) {
			$('[data-toggle=popover]').each(function() {
				if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
					$(this).popover('hide');
				}
			});
		});
		$(document).ajaxComplete(function() {
			$(".popover-test").popover({
				html: true,
				content: function() {
					return $(this).next('.popover-content').html();
				}
			});
			$("#fixtable").tableHeadFixer({
				'left': false,
				'foot': true,
				'head': true
			});

		});

	});
	jQuery.extend(jQuery.expr[':'], {
		focusable: function(el, index, selector) {
			return $(el).is('button, :input, [tabindex]');
		}
	});

	$(document).on('keypress', 'input,select', function(e) {
		if (e.which == 13) {
			e.preventDefault();
			var $canfocus = $(':focusable');
			var index = $canfocus.index(document.activeElement) + 1;
			if (index >= $canfocus.length) index = 0;
			$canfocus.eq(index).focus();
		}
	});

	$(document).ready(function() {
		$(".project").select2();
		$(".subcontractor").select2();
		$(".company_id").select2();
	});

	$(document).ready(function() {
		$('select').first().focus();
	});
</script>



<script>
	$(document).ready(function() {
		$().toastmessage({
			sticky: false,
			position: 'top-right',
			inEffectDuration: 1000,
			stayTime: 3000,
			closeText: '<i class="icon ion-close-round"></i>',
		});
		$(".purchase_items").addClass('checkek_edit');


	});

	$(document).on('click', '.removebtn', function(e) {
		e.preventDefault();
		element = $(this);
		var item_id = $(this).attr('id');
		var answer = confirm("Are you sure you want to delete?");
		if (answer) {
			$('.loading-overlay').addClass('is-active');
			var item_id = $(this).attr('id');
			var quotation_id = $("#quotation_id").val();
			var data = {
				'quotation_id': quotation_id,
				'item_id': item_id
			};
			$.ajax({
				method: "GET",
				async: false,
				data: {
					data: data
				},
				dataType: "json",
				url: '<?php echo Yii::app()->createUrl('subcontractor/removequotationitem'); ?>',
				success: function(response) {
					if (response.response == 'success') {
						element.closest('tr').remove();
						$('#grand_total').html(response.grand_total);
						$().toastmessage('showSuccessToast', "" + response.msg + "");
						$('.addrow').html(response.html);
					} else {
						$().toastmessage('showErrorToast', "" + response.msg + "");
					}

					$('#item_description').val('').trigger('change');
					$('#item_date').val('<?php echo date('d-m-Y'); ?>');
					$('#item_amount').val('');
					$(".item_save").attr('value', 'Save');
					$(".item_save").attr('id', 0);
					$('#item_description').focus();
				}
			});

		} else {

			return false;
		}

	});

	$(document).on("click", ".addcolumn, .removebtn", function() {
		$("table.table  input[name='sl_No[]']").each(function(index, element) {
			$(element).val(index + 1);
			$('.sl_No').html(index + 1);
		});
	});

	$(".inputSwitch span").on("click", function() {
		var $this = $(this);
		$this.hide().siblings("input").val($this.text()).show();
	});

	$(".inputSwitch input").bind('blur', function() {
		var $this = $(this);
		$(this).attr('value', $(this).val());
		$this.hide().siblings("span").text($this.val()).show();
	}).hide();

	$(".allownumericdecimal").keydown(function(event) {
		if (event.shiftKey == true) {
			event.preventDefault();
		}

		if ((event.keyCode >= 48 && event.keyCode <= 57) ||
			(event.keyCode >= 96 && event.keyCode <= 105) ||
			event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 ||
			event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190 || event.keyCode == 110 || event.keyCode == 13) {
			var splitfield = $(this).val().split(".");
			if ((splitfield[1] !== undefined && splitfield[1].length >= 2) && event.keyCode != 8 && event.keyCode != 0 && event.keyCode != 13) {
				event.preventDefault();
			}
		} else {
			event.preventDefault();
		}

		if ($(this).val().indexOf('.') !== -1 && (event.keyCode == 190 || event.keyCode == 110))
			event.preventDefault();

	});
</script>


<script>
	/* Neethu  */

	$(document).on("change", "#company_id", function() {
		var element = $(this);
		var quotation_id = $("#quotation_id").val();
		var default_date = $(".date").val();
		var company = $(this).val();
		var subcontractor = $('#subcontractor').val();
		var project = $("#project").val();
		var description = $('#description').val();
		var quotation_no = $('#quotation_no').val();
		if ((moment(default_date, 'DD-MM-YYYY', true).isValid())) {
			if (subcontractor == '' || default_date == '' || description == '' || project == '' || company == '' || quotation_no == '') {
				$.ajax({
					method: "GET",
					data: {
						quotation_id: 'test'
					},
					dataType: "json",
					url: '<?php echo Yii::app()->createUrl('subcontractor/ajax'); ?>',
					success: function(result) {
						$('.project').select2('focus');
					}
				});
			} else {
				$('.loading-overlay').addClass('is-active');
				$.ajax({
					method: "GET",
					async: false,
					data: {
						quotation_id: quotation_id,
						subcontractor: subcontractor,
						default_date: default_date,
						project: project,
						description: description,
						company: company,
						quotation_no: quotation_no,
						type: 1
					},
					dataType: "json",
					url: '<?php echo Yii::app()->createUrl('subcontractor/createnewquotation'); ?>',
					success: function(result) {
						if (result.response == 'success') {
							$(".purchase_items").removeClass('checkek_edit');
							$('#item_description').focus();
							$("#quotation_id").val(result.quotation_id);
							$("#ScquotationPaymentEntries_scquotation_id").val(result.quotation_id);
							$().toastmessage('showSuccessToast', "" + result.msg + "");
						} else {
							$().toastmessage('showErrorToast', "" + result.msg + "");
							element.val('');
						}
					}
				});
			}
		} else {
			$(this).focus();
			$().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
			$(this).focus();
		}
	});


	$(document).on("change", "#project", function() {
		var element = $(this);
		var quotation_id = $("#quotation_id").val();
		var default_date = $(".date").val();
		var project = $(this).val();
		var subcontractor = $('#subcontractor').val();
		var company = $("#company_id").val();
		var description = $('#description').val();
		var quotation_no = $('#quotation_no').val();
		if ((moment(default_date, 'DD-MM-YYYY', true).isValid())) {
			if (subcontractor == '' || default_date == '' || description == '' || project == '' || company == '' || quotation_no == '') {
				$.ajax({
					method: "GET",
					data: {
						quotation_id: 'test'
					},
					dataType: "json",
					url: '<?php echo Yii::app()->createUrl('subcontractor/ajax'); ?>',
					success: function(result) {
						$('.subcontractor').select2('focus');
					}
				});
			} else {
				$('.loading-overlay').addClass('is-active');
				$.ajax({
					method: "GET",
					async: false,
					data: {
						quotation_id: quotation_id,
						subcontractor: subcontractor,
						default_date: default_date,
						project: project,
						description: description,
						company: company,
						quotation_no: quotation_no,
						type: 1
					},
					dataType: "json",
					url: '<?php echo Yii::app()->createUrl('subcontractor/createnewquotation'); ?>',
					success: function(result) {
						if (result.response == 'success') {
							$(".purchase_items").removeClass('checkek_edit');
							$('#item_description').focus();
							$("#quotation_id").val(result.quotation_id);
							$("#ScquotationPaymentEntries_scquotation_id").val(result.quotation_id);
							$().toastmessage('showSuccessToast', "" + result.msg + "");
						} else {
							$().toastmessage('showErrorToast', "" + result.msg + "");
							element.val('');
						}
					}
				});
			}
		} else {
			$(this).focus();
			$().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
			$(this).focus();
		}
	});

	$(document).on("change", "#subcontractor", function() {
		var element = $(this);
		var quotation_id = $("#quotation_id").val();
		var default_date = $(".date").val();
		var subcontractor = $(this).val();
		var project = $('#project').val();
		var description = $('#description').val();
		var company = $("#company_id").val();
		var quotation_no = $('#quotation_no').val();
		if ((moment(default_date, 'DD-MM-YYYY', true).isValid())) {

			if (project == '' || default_date == '' || description == '' || subcontractor == '' || company == '' || quotation_no == '') {
				$.ajax({
					method: "GET",
					data: {
						quotation_id: 'test'
					},
					dataType: "json",
					url: '<?php echo Yii::app()->createUrl('subcontractor/ajax'); ?>',
					success: function(result) {
						$('.date').focus();
					}
				});
			} else {
				$('.loading-overlay').addClass('is-active');
				$.ajax({
					method: "GET",
					data: {
						quotation_id: quotation_id,
						subcontractor: subcontractor,
						default_date: default_date,
						project: project,
						description: description,
						company: company,
						quotation_no: quotation_no,
						type: 1
					},
					dataType: "json",
					url: '<?php echo Yii::app()->createUrl('subcontractor/createnewquotation'); ?>',
					success: function(result) {
						if (result.response == 'success') {
							$(".purchase_items").removeClass('checkek_edit');
							$('#item_description').focus();
							$("#quotation_id").val(result.quotation_id);
							$("#ScquotationPaymentEntries_scquotation_id").val(result.quotation_id);
							$().toastmessage('showSuccessToast', "" + result.msg + "");
						} else {
							$().toastmessage('showErrorToast', "" + result.msg + "");
						}
					}
				});

			}

		} else {
			$(this).focus();
			$().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
			$(this).focus();
		}

	});



	$(document).on("change", ".date", function() {
		var element = $(this);
		var quotation_id = $("#quotation_id").val();
		var default_date = $(this).val();
		var project = $('#project').val();
		var subcontractor = $('#subcontractor').val();
		var description = $('#description').val();
		var company = $("#company_id").val();
		var quotation_no = $('#quotation_no').val();
		if ((moment(default_date, 'DD-MM-YYYY', true).isValid())) {
			if (subcontractor == '' || default_date == '' || project == '' || description == '' || company == '' || quotation_no == '') {
				$('#description').focus();
			} else {
				$('.loading-overlay').addClass('is-active');
				$.ajax({
					method: "GET",
					async: false,
					data: {
						quotation_id: quotation_id,
						subcontractor: subcontractor,
						default_date: default_date,
						project: project,
						description: description,
						company: company,
						quotation_no: quotation_no,
						type: 1
					},
					dataType: "json",
					url: '<?php echo Yii::app()->createUrl('subcontractor/createnewquotation'); ?>',
					success: function(result) {
						if (result.response == 'success') {
							$(".purchase_items").removeClass('checkek_edit');
							$('#item_description').focus();
							$("#quotation_id").val(result.quotation_id);
							$("#ScquotationPaymentEntries_scquotation_id").val(result.quotation_id);
							$().toastmessage('showSuccessToast', "" + result.msg + "");
						} else {
							$().toastmessage('showErrorToast', "" + result.msg + "");
						}
					}

				});
			}
		} else {
			$(this).focus();
			$().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
			$(this).focus();
		}

		$("#inv_no").focus();

	});


	$(document).on("blur", "#description", function(event) {
		var element = $(this);
		var quotation_id = $("#quotation_id").val();
		var default_date = $(".date").val();
		var description = $(this).val();
		var quotation_no = $('#quotation_no').val();

		if ((moment(default_date, 'DD-MM-YYYY', true).isValid())) {
			if (description == '') {
				event.preventDefault();
			} else {
				var project = $('#project').val();
				var subcontractor = $('#subcontractor').val();
				var company = $("#company_id").val();
				var date = $('.date').val();
				if (subcontractor == '' || default_date == '' || project == '' || company == '' || quotation_no == '') {} else {
					$('.loading-overlay').addClass('is-active');
					$.ajax({
						method: "GET",
						data: {
							quotation_id: quotation_id,
							subcontractor: subcontractor,
							default_date: default_date,
							project: project,
							description: description,
							company: company,
							quotation_no: quotation_no,
							type: 1
						},
						dataType: "json",
						url: '<?php echo Yii::app()->createUrl('subcontractor/createnewquotation'); ?>',
						success: function(result) {
							if (result.response == 'success') {
								$(".purchase_items").removeClass('checkek_edit');
								$("#quotation_id").val(result.quotation_id);
								$("#ScquotationPaymentEntries_scquotation_id").val(result.quotation_id);
								$().toastmessage('showSuccessToast', "" + result.msg + "");
								$('#item_description').focus();
							} else {
								$().toastmessage('showErrorToast', "" + result.msg + "");
							}

						}

					});



				}
			}

		} else {
			$(this).focus();
			$().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
			$(this).focus();
		}

	});
	$(document).on("change", "#quotation_no", function() {
		var element = $(this);
		var quotation_id = $("#quotation_id").val();
		var default_date = $(".date").val();
		var quotation_no = $(this).val();
		var project = $('#project').val();
		var description = $('#description').val();
		var company = $("#company_id").val();
		var subcontractor = $('#subcontractor').val();
		if ((moment(default_date, 'DD-MM-YYYY', true).isValid())) {

			if (project == '' || default_date == '' || description == '' || subcontractor == '' || company == '' || quotation_no == '') {
				$.ajax({
					method: "GET",
					data: {
						quotation_id: 'test'
					},
					dataType: "json",
					url: '<?php echo Yii::app()->createUrl('subcontractor/ajax'); ?>',
					success: function(result) {
						$('#description').focus();
					}
				});
			} else {
				$('.loading-overlay').addClass('is-active');
				$.ajax({
					method: "GET",
					data: {
						quotation_id: quotation_id,
						subcontractor: subcontractor,
						default_date: default_date,
						project: project,
						description: description,
						company: company,
						quotation_no: quotation_no,
						type: 1
					},
					dataType: "json",
					url: '<?php echo Yii::app()->createUrl('subcontractor/createnewquotation'); ?>',
					success: function(result) {
						if (result.response == 'success') {
							$(".purchase_items").removeClass('checkek_edit');
							$('#item_description').focus();
							$("#quotation_id").val(result.quotation_id);
							$("#ScquotationPaymentEntries_scquotation_id").val(result.quotation_id);
							$().toastmessage('showSuccessToast', "" + result.msg + "");
						} else {
							$().toastmessage('showErrorToast', "" + result.msg + "");
						}
					}
				});

			}

		} else {
			$(this).focus();
			$().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
			$(this).focus();
		}

	});




	$("#date").keypress(function(event) {
		if (event.keyCode == 13) {
			if ($(this).val()) {
				$("#description").focus();
			}
		}
	});

	$(".date").keyup(function(event) {
		if (event.keyCode == 13) {
			$(".date").click();
		}
	});

	$("#vendor").keyup(function(event) {
		if (event.keyCode == 13) {
			$("#vendor").click();
		}
	});

	$("#project").keyup(function(event) {
		if (event.keyCode == 13) {
			$("#project").click();
		}
	});


	var sl_no = 1;
	var howMany = 0;
	$('.item_save').click(function() {
		$(this).attr('disabled','disabled');
		$("#previous_details").hide();
		var element = $(this);
		var item_id = $(this).attr('id');

		if (item_id == 0) {


			var item_description = $('#item_description').val();
			var expense_head = $('#expense_head').val();
			var item_amount = $('#item_amount').val();
			var item_date = $('#item_date').val();
			var project = $('#project').val();
			var subcontractor = $('#subcontractor').val();
			var date = $('.date').val();
			var description = $('#description').val();
			var rowCount = $('.table .addrow tr').length;
			var company = $("#company_id").val();

			if (subcontractor == '' || date == '' || project == '' || description == '' || company == '') {
				$().toastmessage('showErrorToast', "Please enter quotation details");
			} else {
				if ((moment(date, 'DD-MM-YYYY', true).isValid())) {
					if (user_role == 1) {
						var condition = expense_head == '' || item_description == '' || item_amount == '' || item_date == '';
					} else {
						var condition = expense_head == '' || item_amount == '' || item_date == '';
					}
					if (condition) {
						if (item_amount == 0) {
							$().toastmessage('showErrorToast', "Please enter amount greater than 0");
						} else {
							$().toastmessage('showErrorToast', "Please fill the details");
						}
					} else {
						howMany += 1;
						if (howMany == 1) {
							$('.loading-overlay').addClass('is-active');
							var quotation_id = $('input[name="quotation_id"]').val();
							var data = {
								'sl_no': rowCount,
								'item_description': item_description,
								'item_date': item_date,
								'item_amount': item_amount,
								'quotation_id': quotation_id,
								'expense_head': expense_head
							};
							$.ajax({
								url: '<?php echo  Yii::app()->createAbsoluteUrl('subcontractor/quotationitem'); ?>',
								type: 'GET',
								dataType: 'json',
								data: {
									data: data
								},
								success: function(response) {
									if (response.response == 'success') {
										$('#grand_total').html(response.grand_total);
										$().toastmessage('showSuccessToast', "" + response.msg + "");
										$('.addrow').html(response.html);
									} else {
										$().toastmessage('showErrorToast', "" + response.msg + "");
									}
									howMany = 0;
									$('#item_description').val('');
									$('#item_amount').val('');
									$('#item_date').val('<?php echo date('d-m-Y'); ?>');
									$('#item_description').focus();
									$('#expense_head').val('').trigger('change');
								}
							});

						}
					}

				} else {
					$(this).focus();
					$().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
					$(this).focus();
				}
			}
		} else {
			// update


			var item_description = $('#item_description').val();
			var expense_head = $('#expense_head').val();
			var item_amount = $('#item_amount').val();
			var item_date = $('#item_date').val();
			var project = $('#project').val();
			var subcontractor = $('#subcontractor').val();
			var date = $('.date').val();
			var description = $('#description').val();
			var rowCount = $('.table .addrow tr').length;
			var company = $("#company_id").val();
			if (project == '' || date == '' || subcontractor == '' || description == '' || company == '') {
				$().toastmessage('showErrorToast', "Please enter quotation details");
			} else {

				if ((moment(date, 'DD-MM-YYYY', true).isValid())) {
					if (user_role == 1) {
						var condition = expense_head == '' || item_description == '' || item_amount == '' || item_date == '';
					} else {
						var condition = expense_head == '' || item_amount == '' || item_date == '';
					}
					if (condition) {
						if (item_amount == 0) {
							$().toastmessage('showErrorToast', "Please enter amount greater than 0");
						} else {
							$().toastmessage('showErrorToast', "Please fill the details");
						}
					} else {

						howMany += 1;
						if (howMany == 1) {
							$('.loading-overlay').addClass('is-active');
							var quotation_id = $('input[name="quotation_id"]').val();
							var data = {
								'item_id': item_id,
								'sl_no': rowCount,
								'item_description': item_description,
								'item_amount': item_amount,
								'item_date': item_date,
								'quotation_id': quotation_id,
								'expense_head': expense_head
							};
							$.ajax({
								url: '<?php echo  Yii::app()->createAbsoluteUrl('subcontractor/updatesquotationitem'); ?>',
								type: 'GET',
								dataType: 'json',
								data: {
									data: data
								},
								success: function(response) {
									if (response.response == 'success') {
										$('#grand_total').html(response.grand_total);
										$().toastmessage('showSuccessToast', "" + response.msg + "");
										$('.addrow').html(response.html);
									} else {
										$().toastmessage('showErrorToast', "" + response.msg + "");
									}
									$('#item_description').val('').trigger('change');
									$('#item_date').val('<?php echo date('d-m-Y'); ?>');
									$('#item_amount').val('');
									$(".item_save").attr('value', 'Save');
									$(".item_save").attr('id', 0);
									$('#item_description').focus();
									$('#expense_head').val('').trigger('change');
									howMany = 0;
								}
							});

						}

					}

				} else {
					$(this).focus();
					$().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
					$(this).focus();
				}

			}
		}
	});

	$(document).on('click', '.edit_item', function(e) {
		e.preventDefault();
		var item_id = $(this).attr('id');
		var expensehead = $("#expensehead" + item_id).val();
		var $tds = $(this).closest('tr').find('td');
		var description = $tds.eq(2).text();
		var date = $tds.eq(3).text();
		var amount = $tds.eq(4).text();
		if (user_role == 1) {
			$('#item_description').val(description.trim());
			$('#expense_head').val(expensehead);
		} else {
			$('#item_description').val(description.trim());
			$('#expense_head').val(expensehead);
		}
		$('#item_date').val(date);
		$('#item_amount').val(parseFloat(amount).toFixed(2));
		$(".item_save").attr('value', 'Update');
		$(".item_save").attr('id', item_id);
		$(".item_save").attr('id', item_id);
		$('#expense_head').focus();
	})

	$('.item_save').keypress(function(e) {
		if (e.keyCode == 13) {
			$('.item_save').click();
		}
	});

	$(document).on('click', '.permission_item', function(e) {
		$('#loading').show();
		e.preventDefault();
		var element = $(this);
		var item_id = $(this).attr('id');
		$.ajax({
			url: '<?php echo  Yii::app()->createAbsoluteUrl('subcontractor/permissionapprove'); ?>',
			type: 'POST',
			dataType: 'json',
			data: {
				item_id: item_id
			},
			success: function(response) {
				$(".popover").removeClass("in");
				if (response.response == 'success') {
					$(".approveoption_" + item_id).hide();
					element.closest('tr').removeClass('permission_style');
					$().toastmessage('showSuccessToast', "" + response.msg + "");
					if (response.mail_send == 'Y') {
						$('.mail_send').show();
					} else {
						$('.mail_send').hide();
					}
				} else if (response.response == 'warning') {
					$(".approveoption_" + item_id).hide();
					element.closest('tr').removeClass('permission_style');
					$().toastmessage('showWarningToast', "" + response.msg + "");
				} else {
					$().toastmessage('showErrorToast', "" + response.msg + "");
				}
			}
		});
	});

	$("#company_id").change(function() {
		var val = $(this).val();
		$("#project").html('<option value="">Select Project</option>');
		$('#loading').show();
		$.ajax({
			url: '<?php echo Yii::app()->createUrl('subcontractor/dynamicproject'); ?>',
			method: 'POST',
			data: {
				company_id: val
			},
			dataType: "json",
			success: function(response) {
				if (response.status == 'success') {
					$("#project").html(response.html);
					$("#subcontractor").html(response.subcontractor);
				} else {
					$("#project").html(response.html);
					$("#subcontractor").html(response.subcontractor);
				}
			}

		})
	})
	$(document).ajaxComplete(function() {
		$('.loading-overlay').removeClass('is-active');
		$('#loading').hide();
	});
</script>