<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="de" lang="de">
<head>
    <!-- CSS Links -->
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/bootstrap-theme.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/message/css/jquery.message.css">
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl; ?>/css/purchase.css">
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.4.1/css/simple-line-icons.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/style.css?<?php echo time(); ?>" />

    <!-- jQuery -->
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.min.js"></script>
  
    <!-- jQuery UI -->
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery-ui.js"></script>
  
    <!-- Other Scripts -->
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/moment.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/tableHeadFixer.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->baseUrl . '/ckeditor/ckeditor.js'; ?>"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jspdf.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/html2canvas.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.plugin.html2canvas.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/base64.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/canvas2image.js"></script>

    <!-- Bootstrap JS -->
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/bootstrap.min.js"></script>
    
    <!-- Select2 JS -->
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.js"></script>
    
    <!-- Message JS -->
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/message/js/jquery.message.js"></script>
</head>

<body>
    <div id="page">
            <?php echo $content; ?>
            <script type="text/javascript">
                var shim = (function() {
                document.createElement('datalist');
                })();
                $(document).ready(function () {
               //     $("#LeadActivity_followupdate").datepicker({dateFormat: 'yy-mm-dd'});
                });
                var $ = jQuery.noConflict();
                $(function() {
                    $("#datepicker").datepicker({
                        dateFormat: 'dd-mm-yy'
                    }).datepicker("setDate", new Date());
                    $("#delivery_date").datepicker({
                        dateFormat: 'dd-mm-yy'
                    });
                });
                
               
    $('.mail_send').hide();

    jQuery.extend(jQuery.expr[':'], {
        focusable: function(el, index, selector) {
            return $(el).is('button, :input, [tabindex]');
        }
    });
    $(document).on('keypress', 'input,select', function(e) {
        if (e.which == 13) {
            e.preventDefault();
            var $canfocus = $(':focusable');
            var index = $canfocus.index(document.activeElement) + 1;
            if (index >= $canfocus.length) index = 0;
            $canfocus.eq(index).focus();
        }
    });
 
    $(document).ready(function() {
        
        $('#loading').hide();
        $(".js-example-basic-single").select2();
        $(".project").select2();
        $(".vendor").select2();
        $(".company").select2();
        $(".expense_head").select2();
    });
    $(document).ajaxComplete(function() {
        $('.loading-overlay').removeClass('is-active');
        $('#loading').hide();
    });
    // $(document).ready(function() {
    //     // $().toastmessage({
    //     //     sticky: false,
    //     //     position: 'top-right',
    //     //     // inEffectDuration: 1000,
    //     //     stayTime: 7000,
    //     //     closeText: '<i class="icon ion-close-round"></i>',
    //     // });
    //     $(".purchase_items").addClass('checkek_edit');
    // });
    var sl_no = 1;
    var howMany = 0;
    $('.item_save').click(function() {
        $("#previous_details").hide();
        var element = $(this);
        var item_id = $(this).attr('id');

        if (item_id == 0) {
            var description = $('.specification').val();
            var remark = $('#remarks').val();
            var quantity = $('#quantity').val();
            var unit = $('#item_unit').text();
            var hsn_code = $('#hsn_code').text();
            var rate = $('#rate').val();
            var amount = $('#item_amount').html();
            var project = $('#project').val();
            var vendor = $('#vendor').val();
            var date = $(".date").val();
            var purchaseno = $('#purchaseno').val();
            var expense_head = $('#expense_head').val();
            var company = $('#company_id').val();
            var rowCount = $('.table .addrow tr').length;
            var dis_amount = $('#dis_amount').val();
            var disp = $('#disp').html();
            var cgstp = $('#cgstp').val();
            var cgst_amount = $('#cgst_amount').html();
            var sgstp = $('#sgstp').val();
            var sgst_amount = $('#sgst_amount').html();
            var igstp = $('#igstp').val();
            var igst_amount = $('#igst_amount').html();
            var tax_amount = $('#tax_amount').html();
            var item_amount = $('#item_amount').html()
            var discount_amount = $('#disc_amount').html();
            var total_amount = $('#total_amount').html();
            var tax_slab = $("#tax_slab").val();
            var base_qty = $('#base_qty').val();
            var base_unit = $('#base_unit').val();
            var base_rate = $('#base_rate').val();
            var remaining_estimation_qty = $("#remaining_estimation_qty").val();
                    
            if (project == '' || vendor == '' || date == '' || purchaseno == '' || expense_head == '' || company == '') {
                $(".toastmessage").addClass("alert alert-danger").html("Please enter purchase details").fadeIn().delay(4000).fadeOut();
                // $().toastmessage('showErrorToast', "Please enter purchase details");
            } else {
                if ((moment(date, 'DD-MM-YYYY', true).isValid())) {

                    if ((description == '' && remark == '') || quantity == '' || rate == '' || amount == 0) {
                        if (amount == 0) {
                            $(".toastmessage").addClass("alert alert-danger").html("Please fill valid quantity and rate").fadeIn().delay(4000).fadeOut();
                            // $().toastmessage('showErrorToast', "Please fill valid quantity and rate");
                        } else {
                            $(".toastmessage").addClass("alert alert-danger").html("Please fill the details").fadeIn().delay(4000).fadeOut();
                            // $().toastmessage('showErrorToast', "Please fill the details");
                        }
                    } else {
                        howMany += 1;
                        if (howMany == 1) {
                            $('#loading').show();
                            var purchase_id = $('input[name="purchase_id"]').val();
                            var subtot = $('#grand_total').text();
                            var grand = $('#grand_total').text();
                            var previousvalue = $('#previousvalue').text();
                            var data = {
                                'sl_no': rowCount,
                                'quantity': quantity,
                                'description': description,
                                'unit': unit,
                                'rate': rate,
                                'amount': amount,
                                'remark': remark,
                                'purchase_id': purchase_id,
                                'grand': grand,
                                'subtot': subtot,
                                'previousvalue': previousvalue,
                                'hsn_code': hsn_code,
                                'dis_amount': dis_amount,
                                'disp': disp,
                                'sgstp': sgstp,
                                'sgst_amount': sgst_amount,
                                'cgstp': cgstp,
                                'cgst_amount': cgst_amount,
                                'igstp': igstp,
                                'igst_amount': igst_amount,
                                'tax_amount': tax_amount,
                                'discount_amount': discount_amount,
                                'total_amount': total_amount,
                                'tax_slab': tax_slab,
                                'base_qty': base_qty,
                                'base_unit': base_unit,
                                'base_rate': base_rate,
                                'project':project,
                                'remaining_estimation_qty':remaining_estimation_qty
                            };
                            $.ajax({
                                url: '<?php echo  Yii::app()->createAbsoluteUrl('purchase/purchaseitem'); ?>',
                                type: 'GET',
                                dataType: 'json',
                                data: {
                                    data: data
                                },
                                success: function(response) {
                                    $('#loading').hide();
                                    if (response.response == 'success') {
                                        $('#final_amount').val(response.final_amount);
                                        $('#amount_total').text(response.grand_total);
                                        $('#discount_total').text(response.discount_total);
                                        $('#tax_total').text(response.tax_total);
                                        $('#grand_total').text(response.grand_total);
                                        $(".toastmessage").addClass("alert alert-success").html("" + response.msg + "").fadeIn().delay(4000).fadeOut();
                                        // $().toastmessage('showSuccessToast', "" + response.msg + "");
                                        $('.addrow').append(response.html);
                                        
                                        if (response.mail_send == 'Y') {
                                            $('.mail_send').show();
                                        } else {
                                            $('.mail_send').hide();
                                        }
                                    } else {
                                        $(".toastmessage").addClass("alert alert-danger").html("" + response.msg + "").fadeIn().delay(4000).fadeOut();
                                        // $().toastmessage('showErrorToast', "" + response.msg + "");
                                    }
                                    howMany = 0;
                                    $('#description').val('').trigger('change');
                                    $('#remarks').val('');
                                    var quantity = $('#quantity').val('');
                                    var unit = $('#item_unit').text('');
                                    var rate = $('#rate').val('');
                                    var amount = $('#item_amount').html('');
                                    $('#hsn_code').text('');
                                    $('#previousvalue').text('0');
                                    $('#description').select2('focus');
                                    $('#dis_amount').val('');
                                    $('#disp').html('0.00');
                                    $('#item_amount').html('0.00');
                                    $('#tax_amount').html('0.00');
                                    $('#sgstp').val('');
                                    $('#sgst_amount').html('0.00');
                                    $('#cgstp').val('');
                                    $('#cgst_amount').html('0.00');
                                    $('#igstp').val('');
                                    $('#igst_amount').html('0.00');
                                    $('#disc_amount').html('0.00');
                                    $('#total_amount').html('0.00');
                                    $('#tax_slab').val("18").trigger('change');
                                    $('.base-data').hide();
                                    $('#item_baseunit_data').hide();
                                    $('#item_conversion_data').hide();
                                    $("#unitval").val('');
                                }
                            });

                            // $('.js-example-basic-single').select2('focus');
                        }
                    }

                } else {

                    $(this).focus();
                    $(".toastmessage").addClass("alert alert-danger").html("Please enter valid date DD-MM-YYYY format").fadeIn().delay(4000).fadeOut();
                    // $().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
                    $(this).focus();
                }
            }
        } else {
            var description = $('.specification').val();
            var remark = $('#remarks').val();
            var quantity = $('#quantity').val();
            var unit = $('#item_unit').text();
            var hsn_code = $('#hsn_code').text();
            var rate = $('#rate').val();
            var amount = $('#item_amount').html();
            var project = $('#project').val();
            var vendor = $('#vendor').val();
            var date = $(".date").val();
            var purchaseno = $('#purchaseno').val();
            var expense_head = $('#expense_head').val();
            var company = $('#company_id').val();
            var dis_amount = $('#dis_amount').val();
            var disp = $('#disp').html();
            var cgstp = $('#cgstp').val();
            var cgst_amount = $('#cgst_amount').html();
            var sgstp = $('#sgstp').val();
            var sgst_amount = $('#sgst_amount').html();
            var igstp = $('#igstp').val();
            var igst_amount = $('#igst_amount').html();
            var tax_amount = $('#tax_amount').html();
            var item_amount = $('#item_amount').html()
            var discount_amount = $('#disc_amount').html();
            var total_amount = $('#total_amount').html();
            var tax_slab = $("#tax_slab").val();
            var base_qty = $('#base_qty').val();
            var base_unit = $('#base_unit').val();
            var base_rate = $('#base_rate').val();
            if (project == '' || vendor == '' || date == '' || purchaseno == '' || expense_head == '' || company == '') {
                $(".toastmessage").addClass("alert alert-danger").html("Please enter purchase details").fadeIn().delay(4000).fadeOut();
                // $().toastmessage('showErrorToast', "Please enter purchase details");
            } else {

                if ((moment(date, 'DD-MM-YYYY', true).isValid())) {
                    if ((description == '' && remark == '') || quantity == '' || rate == '' || amount == 0) {
                        if (amount == 0) {
                            $(".toastmessage").addClass("alert alert-danger").html("Please fill valid quantity and rate").fadeIn().delay(4000).fadeOut();
                            // $().toastmessage('showErrorToast', "Please fill valid quantity and rate");
                        } else {
                            $(".toastmessage").addClass("alert alert-danger").html("Please fill the details").fadeIn().delay(4000).fadeOut();
                            // $().toastmessage('showErrorToast', "Please fill the details");
                        }
                    } else {
                        howMany += 1;
                        if (howMany == 1) {
                            $('#loading').show();
                            var purchase_id = $('input[name="purchase_id"]').val();
                            var subtot = $('#grand_total').text();
                            var grand = $('#grand_total').text();
                            var previousvalue = $('#previousvalue').text();
                            var data = {
                                'item_id': item_id,
                                'sl_no': sl_no,
                                'quantity': quantity,
                                'description': description,
                                'unit': unit,
                                'rate': rate,
                                'amount': amount,
                                'remark': remark,
                                'purchase_id': purchase_id,
                                'grand': grand,
                                'subtot': subtot,
                                'previousvalue': previousvalue,
                                'hsn_code': hsn_code,
                                'dis_amount': dis_amount,
                                'disp': disp,
                                'sgstp': sgstp,
                                'sgst_amount': sgst_amount,
                                'cgstp': cgstp,
                                'cgst_amount': cgst_amount,
                                'igstp': igstp,
                                'igst_amount': igst_amount,
                                'tax_amount': tax_amount,
                                'discount_amount': discount_amount,
                                'total_amount': total_amount,
                                'tax_slab': tax_slab,
                                'base_qty': base_qty,
                                'base_unit': base_unit,
                                'base_rate': base_rate,
                                'project':project
                            };
                            $('.loading-overlay').addClass('is-active');
                            $.ajax({
                                url: '<?php echo  Yii::app()->createAbsoluteUrl('purchase/updatepurchaseitem'); ?>',
                                type: 'GET',
                                dataType: 'json',
                                data: {
                                    data: data
                                },
                                success: function(response) {
                                    $('#loading').hide();
                                    if (response.response == 'success') {
                                        $('#final_amount').val(response.final_amount);
                                        $('#amount_total').text(response.grand_total);
                                        $('#discount_total').text(response.discount_total);
                                        $('#tax_total').text(response.tax_total);
                                        $('#grand_total').text(response.grand_total);
                                        $(".toastmessage").addClass("alert alert-success").html("" + response.msg + "").fadeIn().delay(4000).fadeOut();
                                        // $().toastmessage('showSuccessToast', "" + response.msg + "");
                                        $('.addrow').html(response.html);
                                        
                                        if (response.mail_send == 'Y') {
                                            $('.mail_send').show();
                                        } else {
                                            $('.mail_send').hide();
                                        }
                                    } else {
                                        $(".toastmessage").addClass("alert alert-danger").html("" + response.msg + "").fadeIn().delay(4000).fadeOut();
                                        // $().toastmessage('showErrorToast', "" + response.msg + "");
                                    }
                                    howMany = 0;
                                    $('#description').val('').trigger('change');
                                    $('#remarks').val('');
                                    var quantity = $('#quantity').val('');
                                    var unit = $('#item_unit').text('');
                                    var rate = $('#rate').val('');
                                    var amount = $('#item_amount').html('');
                                    $(".item_save").attr('value', 'Save');
                                    $(".item_save").attr('id', 0);
                                    $('#previousvalue').text('0');
                                    $('#description').select2('focus');
                                    $('#hsn_code').text('');
                                    $('#dis_amount').val('');
                                    $('#disp').html('0.00');
                                    $('#item_amount').html('0.00');
                                    $('#tax_amount').html('0.00');
                                    $('#sgstp').val('');
                                    $('#sgst_amount').html('0.00');
                                    $('#cgstp').val('');
                                    $('#cgst_amount').html('0.00');
                                    $('#igstp').val('');
                                    $('#igst_amount').html('0.00');
                                    $('#disc_amount').html('0.00');
                                    $('#total_amount').html('0.00');
                                    $('#tax_slab').val("18").trigger('change');
                                    $('.base-data').hide();
                                    $('#item_baseunit_data').hide();
                                    $('#item_conversion_data').hide();
                                    $("#unitval").val('');
                                }
                            });

                            $('.js-example-basic-single').select2('focus');
                        }
                    }

                } else {
                    $(this).focus();
                    $(".toastmessage").addClass("alert alert-danger").html("Please enter valid date DD-MM-YYYY format").fadeIn().delay(4000).fadeOut();
                    // $().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
                    $(this).focus();
                }



            }


        }




    });
    

    function filterDigits(eventInstance) {
        eventInstance = eventInstance || window.event;
        key = eventInstance.keyCode || eventInstance.which;
        if ((47 < key) && (key < 58) || key == 8) {
            return true;
        } else {
            if (eventInstance.preventDefault)
                eventInstance.preventDefault();
            eventInstance.returnValue = false;
            return false;
        }
    }
       

    </script>
     <style>
      
	
        .selecboxwidth{
            width:200px !important;
        }
     </style>       
    </body>
</html>
