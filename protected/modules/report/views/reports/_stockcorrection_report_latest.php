<div class="table-responsive">
    <table class="table table-bordered" id="fixtable">
        <thead class="entry-table sticky-thead">
            <tr>
                <th>Warehouse</th>
                <th>Item</th>
                <th>Rate</th>
                <th>Dimension</th>
                <th>Batch</th>
                <th>Opening Stock</th>
                <th>Receipt</th>
                <th>Despatch</th>
                <th>Stock correction</th>
                <th>Closing Stock</th>

            </tr>
        </thead>
        <tbody class="addrow">
            <?php
            $i = 1;
            $rateArry = array();
            if (!empty($result_data)) {
                foreach ($result_data as $view_data) {
                    $warehouse_id = $view_data['warehouse'];
                    if (!empty($warehouse_id)) {
                        $warehouse = Warehouse::model()->findByPk($warehouse_id);
                        $item = $view_data['item'];
                        $i = 0;
                        $j = 0;
                        ?>

                        <?php
                        foreach ($item as $key => $value) {
                            $itemId = $key;
                            $details = $value['details'];
                            $ratecount = $value['count'];
                            $count1 = 0;
                            foreach ($details as $key => $detail) {
                                $count1 += count($detail);
                            }
                            ?>
                            <tr>
                                <?php if ($i == 0) { ?>
                                    <td rowspan="<?php echo $view_data['count'] ?>">
                                        <?php echo $warehouse['warehouse_name']; ?>
                                    </td>
                                <?php } ?>

                                <td rowspan="<?php echo $count1 ?>">
                                    <?php echo Controller::itemName($itemId); ?>
                                </td>
                                <?php
                                foreach ($details as $key => $detail) {
                                    $count1 += count($detail);
                                    $rate = $key;
                                    $date = array_column($detail, 'created_date');
                                    array_multisort($date, SORT_ASC, $detail);
                                    $detail_data[] = $detail[0];

                                    $i = 0;
                                    foreach ($detail_data as $key => $data) {
                                        if ($i == 0) {
                                            $used_sql = 'SELECT * FROM `pms_acc_wpr_item_consumed` '
                                                . ' WHERE `warehouse_id`=' . $data['warehouse']
                                                . ' AND `item_id`=' . $data['item']
                                                . '  AND`item_rate` =' . $data['rate'];
                                            $used_qty_in_pms = Yii::app()->db->createCommand($used_sql)->queryRow();
                                            $used_qty = !empty($used_qty_in_pms) ? $used_qty_in_pms['item_qty'] : 0;
                                            $x = $this->getOpeningStockQuantity($warehouse_id, $itemId, $data['created_date'], $rate);
                                        }
                                        $i++;
                                    }
                                    $cqty = $x - $used_qty;
                                    ?>
                                    <td rowspan="<?php echo count($detail_data); ?>">
                                        <?php echo $rate; ?>
                                    </td>
                                    <?php
                                    foreach ($detail_data as $key => $data) {

                                        if ($key == 0) {
                                            ?>
                                            <td>
                                                <?php echo $data['dimension']; ?>
                                            </td>
                                            <td>
                                                <?php echo isset($data['batch']) ? $data['batch'] : ""; ?>
                                            </td>

                                            <td>
                                                <?php echo $cqty ?>
                                            </td>
                                            <td>
                                                <?php
                                                $date_from = isset($_GET['date_from']) ? $_GET['date_from'] : "";
                                                $date_to = isset($_GET['date_to']) ? $_GET['date_to'] : "";
                                                $rcqty = $this->getReceiptQuantity($warehouse_id, $itemId, $date_from, $date_to, $rate);
                                                echo ($rcqty == NULL) ? 0 : $rcqty;

                                                ?>
                                            </td>
                                            <td>
                                                <?php $dspqty = $this->getDespatchQuantity($warehouse_id, $itemId, $date_from, $date_to, $rate);
                                                echo ($dspqty == NULL) ? 0 : $dspqty;
                                                ?>
                                            </td>
                                            <td>
                                                <?php

                                                $stkqty = $this->getStockCorrectionQuantity($warehouse_id, $itemId, $date_from, $date_to, $rate);
                                                echo ($stkqty == NULL) ? 0 : $stkqty;

                                                ?>
                                            </td>

                                            <td>

                                                <?php

                                                $closing_quantity = $cqty + $rcqty - $dspqty + $stkqty;


                                                $cqty = $closing_quantity;
                                                echo $closing_quantity;
                                                ?>
                                            </td>

                                        </tr>
                                    <?php }
                                    } ?>


                                <?php
                                }

                                $i++;
                        }
                        ?>


                        <?php
                    }
                }
            }
            ?>

        </tbody>
    </table>
</div>
<script>
    $(document).ready(function () {
        // $("#fixtable").tableHeadFixer({
        //     'left': false,
        //     'foot': true,
        //     'head': true
        // });
        $('[data-toggle="popover-hover"]').popover({
            html: true,
            container: "body",
            trigger: 'hover',
            placement: 'bottom',
            content: function () {

                return '<img src="' + $(this).data('img') + '" style="height:auto;"/>';
            }
        });
    });



</script>