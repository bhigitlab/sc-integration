<?php
/* @var $this SalesQuotationController */
/* @var $model SalesQuotation */

$this->breadcrumbs=array(
	'Sales Quotations'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List SalesQuotation', 'url'=>array('index')),
	array('label'=>'Create SalesQuotation', 'url'=>array('create')),
	array('label'=>'View SalesQuotation', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage SalesQuotation', 'url'=>array('admin')),
);
?>

<h1>Update SalesQuotation <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>