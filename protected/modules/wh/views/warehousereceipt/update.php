<?php
if (Yii::app()->user->role != 1) {
	$warehouse = Warehouse::model()->findAll(array('condition' => 'FIND_IN_SET(' . Yii::app()->user->id . ', assigned_to)'));
} else {
	$warehouse = Warehouse::model()->findAll();
}
$vendor = Vendors::model()->findAll();
$user = Users::model()->findAll();
$tblpx = Yii::app()->db->tablePrefix;
$receipt = Yii::app()->db->createCommand("SELECT warehousereceipt_id FROM {$tblpx}warehousereceipt ORDER BY warehousereceipt_id DESC")->queryRow();
if (empty($receipt)) {
	$receipt_no = 1;
} else {
	$receipt_no = $receipt['warehousereceipt_id'] + 1;
}
?>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.js"></script>

<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/message/css/jquery.message.css">
<script type="text/javascript"
	src="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/message/js/jquery.message.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/tableHeadFixer.js"></script>

<script>
	var shim = (function () { document.createElement('datalist'); })();
</script>
<script>
	$(function () {
		$("#datepicker").datepicker({ dateFormat: 'dd-mm-yy' }).datepicker("setDate", new Date());

	});

</script>
<style>
	/* .dropdown{
			position: relative;
			display: inline-block;
	}
	.dropdown::before{
			position: absolute;
			content: " \2193";
			top: 0px;
			right: -8px;
			height: 20px;
			width: 20px;
	}

	button#caret {
			border : none;
			background : none;
			position: absolute;
			top: 0px;
			right: 0px;
	}
	.invoicemaindiv th, .invoicemaindiv td{
		padding: 10px;
		text-align: left;
	}
	.addcolumn {
		cursor: pointer;
		background-color: black;
		border: none;
		color: white;
		text-align: center;
		text-decoration: none;
		margin: 2px;
		font-size: 30px;
		width: 40px;
	  }
	  .invoicemaindiv .pull-right, .invoicemaindiv .pull-left{
		  float: none !important;
	  }
	  #parent , #parent2, #parent3{max-height: 150px;}
	  .select2.select2-container.select2-container--default.select2-container--above,.select2.select2-container.select2-container--default.select2-container--below,.select2.select2-container.select2-container--default.select2-container--focus,
	  .select2.select2-container.select2-container--default{
		  width:  200px  !important;
	  }
	  label{display:block;}
	  @media(min-width: 767px){
		  .invoicemaindiv .pull-right{
		  float: right !important;
	  }
	  .invoicemaindiv .pull-left{
		  float: left !important;
	  }
	  }



	 .add_selection {
		 background: #000;
	  }




	.toast-container {
	  width:350px;
	}
	.toast-position-top-right {
	  top: 57px;
	  right: 6px;
	}
	.toast-item-close {
	  background-image: none;
	  cursor: pointer;
	  width: 12px;
	  height: 12px;
	  text-align: center;
	  border-radius: 2px;
	}
	.toast-item-image {
	  font-size: 24px;
	}
	.toast-item-close:hover {
	  color: red;
	}
	.toast-item {
	  border: transparent;
	  border-radius: 3px;
	  font-size: 10px;
	  opacity: 1;
	  background-color: rgba(34,45,50,0.8);
	}
	.toast-item-wrapper p {
	  margin: 0px 5px 0px 42px;
	  font-size: 14px;
	  text-align: justify;
	}


	.toast-type-success {
	  background-color: #00A65A;
	  border-color: #00A65A;
	}
	.toast-type-error {
	  background-color: #DD4B39;
	  border-color: #DD4B39;
	}
	.toast-type-notice {
	  background-color: #00C0EF;
	  border-color: #00C0EF;
	}
	.toast-type-warning {
	  background-color: #F39C12;
	  border-color: #F39C12;
	}

	.span_class {
	   min-width: 70px;
	   display: inline-block;
	}
	.purchase-title{
		border-bottom:1px solid #ddd;
	}

	.purchase_items h3{
		font-size:18px;
		color:#333;
		margin:0px;
		padding:0px;
		text-align:left;
		padding-bottom:10px;
	}
	.purchase_items{
		padding:15px;
		box-shadow:0 0 13px 1px rgba(0,0,0,0.25);
	}
	.purchaseitem {
		display: inline-block;
		margin-right: 20px;
	}
	.purchaseitem last-child{margin-right:0px;}
	.purchaseitem label{
		font-size:12px;
	}
	.remark{display:none;}
	.padding-box{padding:3px 0px; min-height:17px; display:inline-block;}
	th{height:auto;}
	.accepted_quantity,.rejected_quantity,.quantity, .rate{max-width:80px;}

	*:focus{
		border:1px solid #333;
		box-shadow:0 0 6px 1px rgba(0,0,0,0.25);
	}

	.rate_highlight {
		background:#DD1035 !important;
		color: #fff;
	}
	.block_purchase{

		padding-left: 15px;

	} */
</style>
<?php
if ($readonly == "true") {
	$disabled = "disabled";
	$whse_disabled = true;
} else {
	$disabled = "";
	$whse_disabled = false;
}
?>
<div class="container">

	<div class="invoicemaindiv">
		<div class="expenses-heading">
			<div class="clearfix">
				<!-- remove addentries class -->
				<?php echo CHtml::Button('Back', array('class' => 'btn btn-primary pull-right mt-0 mb-10', 'onclick' => 'javascript:location.href="' . $this->createUrl('index') . '"')); ?>

				<h3>Update Warehouse Receipt</h3>
			</div>
		</div>
		<div id="msg_box"></div>


		<form id="warehousereceipt_form" method="post"
			action="<?php echo $this->createAbsoluteUrl('warehousereceipt/updatereceiptitems'); ?>">
			<?php echo CHtml::hiddenField('remove', '', array('id' => "remove")); ?>
			<?php echo CHtml::hiddenField('warehousereceipt_id', $model->warehousereceipt_id, array('id' => "warehousereceipt_id")); ?>
			<div class="entries-wrapper block_purchase">
				<div class="block_purchases">
					<div class="row">
						<div class="col-xs-12">
							<div class="heading-title">Add Details</div>
							<div class="dotted-line"></div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-md-3">
							<div class="form-group">
								<label>WAREHOUSE To: <span class="required">*</span></label>
								<?php $data = CHtml::listData(Controller::getCategoryOptions(), 'id', 'text', 'group'); ?>
								<?php echo CHtml::activeDropDownList($model, 'warehousereceipt_warehouseid', $data, array(
									'empty' => '-Choose a Warehouse-',
									'class' => 'form-control mandatory',
									'disabled' => $whse_disabled,
									'options' => array($model['warehousereceipt_warehouseid'] => array('selected' => true))
								)); ?>
							</div>
						</div>
						<div class="col-xs-12 col-md-3">
							<div class="form-group">
								<label>DATE : </label>
								<input type="text"
									value="<?php echo ((isset($date) && $date != '') ? date("d-m-Y", strtotime($date)) : ''); ?>"
									id="datepicker" class="txtBox date inputs target form-control" name="warehouse_date"
									placeholder="Please click to edit" readonly="true">
							</div>
						</div>
						<div class="col-xs-12 col-md-3">
							<div class="form-group">
								<label>RECEIPT CLERK : </label>
								<select name="clerk" class="inputs target clerk form-control" id="clerk">
									<?php
									if (yii::app()->user->role == 1) { ?>
										<option value="">Choose Receipt Clerk</option>
										<?php foreach ($user as $key => $value) { ?>

											<option value="<?php echo $value['userid']; ?>" <?php echo ($model->warehousereceipt_clerk == $value['userid']) ? 'selected' : ""; ?>>
												<?php echo $value['first_name'] . ' ' . $value['last_name']; ?>
											</option>
										<?php }
									} else {
										foreach ($user as $key => $value) {
											if ($value['userid'] == Yii::app()->user->id) { ?>
												<option value="<?php echo $value['userid']; ?>" <?php echo ($model->warehousereceipt_clerk == $value['userid']) ? 'selected' : ""; ?>>
													<?php echo $value['first_name'] . ' ' . $value['last_name']; ?>
												</option>
												<?php
											}
										}
									}
									?>
								</select>
							</div>
						</div>
						<div class="col-xs-12 col-md-3">
							<div class="form-group">
								<label>RECEIPT NO : </label>
								<input type="text" value="<?php echo $model->warehousereceipt_no; ?>" id="receipt_no"
									name="receipt_no" placeholder="Please click to edit" class="form-control">
							</div>
						</div>
						<div class="col-xs-12 col-md-3">
							<div class="form-group">
								<label>Transfer Type : <span class="required">*</span></label>
								<select name="transfer_type" class="inputs target transfer_type form-control"
									id="transfer_type" <?php echo $disabled; ?>>
									<option value="">Choose Transfer Type</option>
									<option value="1" <?php echo ($model->warehousereceipt_transfer_type == 1) ? "selected" : ""; ?>>Purchase Bill</option>
									<option value="2" <?php echo ($model->warehousereceipt_transfer_type == 2) ? "selected" : ""; ?>>Despatch </option>
								</select>
							</div>
						</div>
						<div class="elem_block elem_block_purchase_bill_no  col-sm-4 col-md-3" style="display:none">
							<div class="form-group">
								<label>Purchase Bill No : </label>
								<select name="purchase_bill_no" class="inputs target purchase_bill_no form-control"
									id="purchase_bill_no" <?php echo $disabled; ?>>
									<option value="">Choose Purchase Bill No</option>
									<?php
									foreach ($DespatchPurchaseBilllist['bills'] as $key_bills => $value_bills) {
										?>
										<option value="<?php echo $value_bills['id']; ?>" <?php echo ($model->warehousereceipt_bill_id == $value_bills['id']) ? 'selected' : ""; ?>>
											<?php echo $value_bills['value']; ?>
										</option>
										<?php
									}
									?>
								</select>
							</div>
						</div>
						<div class="col-xs-12 col-md-3 elem_block_warehouseid_from " style="display:none">
							<div class="form-group">
								<label>WAREHOUSE FROM: <span class="required">*</span></label>
								<?php $data = CHtml::listData(Controller::getCategoryOptions(), 'id', 'text', 'group'); ?>
								<?php echo CHtml::activeDropDownList($model, 'warehousereceipt_warehouseid_from', $data, array(
									'empty' => '-Choose a Warehouse-',
									'class' => 'form-control mandatory',
									'disabled' => $whse_disabled,
									'options' => array($model['warehousereceipt_warehouseid_from'] => array('selected' => true))
								)); ?>
							</div>
						</div>
						<div class="col-xs-12 col-md-3 elem_block_dispatch_no" style="display:none">
							<div class="form-group">
								<label>Dispatch No : </label>
								<select name="dispatch_no" class="inputs target dispatch_no form-control"
									id="dispatch_no" <?php echo $disabled; ?>>
									<option value="">Choose Dispatch No</option>
									<?php
									foreach ($DespatchPurchaseBilllist['despatches'] as $key_despatches => $value_despatches) {
										?>
										<option value="<?php echo $value_despatches['id']; ?>" <?php echo ($model->warehousereceipt_despatch_id == $value_despatches['id']) ? 'selected' : ""; ?>>
											<?php echo $value_despatches['value']; ?>
										</option>
										<?php
									}
									?>
								</select>
							</div>
						</div>
						<div class="col-xs-12 col-md-3">
							<div class="form-group">
								<label>Remarks :</label>
								<?php /* <input type="text" name="vendor" value="<?php echo ($model->warehousereceiptwarehouseidfrom['warehouse_name'] !="")?$model->warehousereceiptwarehouseidfrom['warehouse_name']:$model->warehousereceiptBillId['bill_number']; ?>" id="vendor" class="form-control"> */ ?>
								<textarea rows="2" cols="30" name="vendor" id="vendor" class="form-control">
								 <?php echo $model->warehousereceipt_remark; ?>
							 </textarea>
							</div>
						</div>
					</div>
				</div>
				<div id="msg"></div>
				<div id="previous_details"
					style="position: fixed;top: 94px; left: 70px; right: 70px; z-index: 2; max-width:1150px; margin:0 auto;">
				</div>
				<div id="warehousereceipt_items">
					<?php //echo ((!$model->isNewRecord) ? $client : '') ?>
				</div>

				<!-- Additional Items-->
				<?php echo $this->renderPartial('_additional_items', array('specification' => $specification)) ?>

				<div class="row">
					<div class="col-xs-12">
						<div class="subcontractor-bill-table table-responsive">
							<table class="table">
								<thead class="entry-table">
									<tr>
										<th>Sl.No</th>
										<th>Item</th>
										<th>Batch</th>
										<th>Unit/Size</th>
										<th>Quantity</th>
										<th>Rate</th>
										<th>Base Quantity</th>
										<th>Base Unit</th>
										<th>Base Rate</th>
										<th>Amount</th>
										<!-- <th>J.O No</th> -->
										<th></th>
									</tr>
								</thead>
								<tbody class="addrow">
									<?php
									foreach ($item_model as $key => $values) {
										$sql = "SELECT id, cat_id,brand_id, specification, unit "
											. " FROM {$tblpx}specification "
											. " WHERE id=" . $values['warehousereceipt_itemid'];
										$specification = Yii::app()->db->createCommand($sql)->queryRow();
										$sql = "SELECT * FROM {$tblpx}purchase_category "
											. " WHERE id='" . $specification['cat_id'] . "'";
										$parent_category = Yii::app()->db->createCommand($sql)->queryRow();
										$brand = '';
										if ($specification['brand_id'] != NULL) {
											$brand_details = Yii::app()->db->createCommand("SELECT brand_name FROM {$tblpx}brand WHERE id=" . $specification['brand_id'] . "")->queryRow();
											$brand = '-' . $brand_details['brand_name'];
										}

										$spc_details = $parent_category['category_name'] . $brand . ' - ' . $specification['specification'];
										$spc_id = $specification['id'];

										$warehousereceipt_unit = $values['warehousereceipt_unit'];
										$warehousereceipt_itemunit = $values['warehousereceipt_itemunit'];

										if ($warehousereceipt_unit != $warehousereceipt_itemunit) {
											if ($values['warehousereceipt_itemqty'] > 0) {
												$quantity_data = $values['warehousereceipt_itemqty'];
											} else {
												$quantity_data = $values['warehousereceipt_quantity'];
											}

											if ($values['warehousereceipt_itemrate'] > 0) {
												$rate_data = $values['warehousereceipt_itemrate'];
											} else {
												$rate_data = $values['warehousereceipt_rate'];
											}

											if ($values['warehousereceipt_itemunit'] != '') {
												$unit_data = $values['warehousereceipt_itemunit'];
											} else {
												$unit_data = $values['warehousereceipt_unit'];
											}
											$quantity_base_data = $values['warehousereceipt_quantity'];
											$rate_base_data = $values['warehousereceipt_rate'];
											$unit_base_data = $values['warehousereceipt_unit'];
											$additional_amount = $quantity_data * $rate_data;
										} else {

											$quantity_data = $values['warehousereceipt_quantity'];
											$rate_data = $values['warehousereceipt_rate'];
											$unit_data = $values['warehousereceipt_unit'];
											$unit_base_data = $values['warehousereceipt_unit'];
											$quantity_base_data = $values['warehousereceipt_quantity'];
											$rate_base_data = $values['warehousereceipt_rate'];
											$additional_amount = $values['warehousereceipt_quantity'] * $values['warehousereceipt_rate'];

										}

										?>
										<tr class="tr_class">
											<td>
												<div id="item_sl_no">
													<?php echo $key + 1; ?>
												</div>
											</td>
											<td>
												<div class="item_description" id="<?php echo $spc_id; ?>">
													<?php echo $spc_details; ?>
												</div>
											</td>
											<td>
												<div class="text-right" id="batch<?php echo $values['item_id']; ?>">
													<?php echo $values['warehousereceipt_batch']; ?>
												</div>
											</td>
											<td>
												<div class="unit" id="unit<?php echo $values['item_id']; ?>">
													<?php echo $unit_data; ?>
												</div>
											</td>
											<td>
												<div id="quantity<?php echo $values['item_id']; ?>">
													<?php echo $quantity_data; ?>
												</div>
											</td>
											<td>
												<div id="item_rate<?php echo $values['item_id']; ?>">
													<?php echo $rate_data; ?>
												</div>
											</td>
											<td>
												<div id="item_qty_data<?php echo $values['item_id']; ?>">
													<?php echo $quantity_base_data; ?>
												</div>
											</td>
											<td>
												<div id="item_unit_data<?php echo $values['item_id']; ?>">
													<?php echo $unit_base_data; ?>
												</div>
											</td>
											<td>
												<div id="item_rate_data<?php echo $values['item_id']; ?>">
													<?php echo $rate_base_data; ?>
												</div>
											</td>
											<td>
												<div class="unit" id="amount<?php echo $values['item_id']; ?>">
													<?php echo $additional_amount; ?>
												</div>
											</td>

											<!-- <td><div class="unit" id="jono<?php // echo $values['item_id']; ?>"> <?php // echo $values['warehousereceipt_jono']; ?></div></td> -->
											<td width="70"><span class="icon icon-options-vertical popover-test"
													data-toggle="popover" data-placement="left" type="button"
													data-html="true" style="cursor: pointer;"></span>

												<div class="popover-content hide">
													<ul class="tooltip-hiden">
														<li><a href="" id='<?php echo $values['item_id']; ?>'
																class="removebtn btn btn-xs btn-default">Delete</a></li>
														<li><a href="" id='<?php echo $values['item_id']; ?>'
																class="btn btn-xs btn-default edit_item">Edit</a></li>
													</ul>
												</div>
											</td>
										</tr>
									<?php } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>

				<div style="padding-right: 0px;"
					class="col-md-offset-9 col-md-3 col-sm-offset-8 col-sm-4 col-xs-offset-6 col-xs-6">
					<input type="hidden" value="<?php echo ((isset($subtot) && $subtot != '') ? $subtot : ''); ?>"
						class="txtBox pastweek" readonly=ture name="subtot" /></td>
					<input type="hidden" value="<?php echo ((isset($grand) && $grand != '') ? $grand : ''); ?>"
						class="txtBox pastweek grand" name="grand" readonly=true />
				</div>
				<div class="row">
					<div class="form-group col-xs-12 text-right addRow">
						<?php echo CHtml::Button($model->isNewRecord ? 'Create' : 'Save', array('class' => 'buttonsubmit btn btn-primary', 'id' => 'buttonsubmit', 'disabled' => true)); ?>
					</div>
				</div>
			</div>
		</form>
	</div>
	<input type="hidden" name="final_amount" id="final_amount" value="0">
	<input type="hidden" name="item_amount_temp" id="item_amount_temp" value="0">
	<style>
		a.pdf_excel {
			background-color: #6a8ec7;
			display: inline-block;
			padding: 8px;
			color: #fff;
			border: 1px solid #6a8ec8;
		}
	</style>
	<script>
		jQuery.extend(jQuery.expr[':'], {
			focusable: function (el, index, selector) {
				return $(el).is('button, :input, [tabindex]');
			}
		});

		$(document).on('keypress', 'input,select,textarea', function (e) {
			if (e.which == 13) {
				e.preventDefault();
				// Get all focusable elements on the page
				var $canfocus = $(':focusable');
				var index = $canfocus.index(document.activeElement) + 1;
				if (index >= $canfocus.length) index = 0;
				$canfocus.eq(index).focus();
			}
		});

		$(document).ready(function () {
			$(".js-example-basic-single").select2();
			$("#Warehousereceipt_warehousereceipt_warehouseid,.dispatch_no,.purchase_bill_no,#Warehousereceipt_warehousereceipt_warehouseid_from,.vendor,.clerk,#transfer_type").select2();
			$("#additional_item").select2();
			$('select').first().focus();
			var transfer_type = $("#transfer_type").val();
			var dispatch_no = $("#dispatch_no").val();
			var purchase_bill_no = $("#purchase_bill_no").val();
			var warehouse_from = $('#Warehousereceipt_warehousereceipt_warehouseid_from').val();
			var receipt_id = $('input[name="warehousereceipt_id"]').val();
			if (transfer_type == "") {
				$(".elem_block_purchase_bill_no").hide();
				$(".elem_block_dispatch_no").hide();
				$(".elem_block_warehouseid_from").hide();
				$("#warehousereceipt_items").hide();
			} else {
				if (transfer_type == 1) {
					$(".elem_block_purchase_bill_no").show();
					$(".elem_block_warehouseid_from").hide();
					$(".elem_block_dispatch_no").hide();
					$("#warehousereceipt_items").show();
				} else {
					$(".elem_block_warehouseid_from").show();
					$(".elem_block_dispatch_no").show();
					$(".elem_block_purchase_bill_no").hide();
					$("#warehousereceipt_items").show();
				}
				getAllItems(purchase_bill_no, dispatch_no, receipt_id, transfer_type);
				$('#buttonsubmit').prop('disabled', false);
			}
		});
		$("#buttonsubmit").keypress(function (e) {
			if (e.keyCode == 13) {
				$('.buttonsubmit').click();
			}
		});

		$(".buttonsubmit").click(function () {
			$("#warehousereceipt_form").submit();
		});
	</script>
	<script>
		$(document).ready(function () {
			$().toastmessage({
				sticky: false,
				position: 'top-right',
				inEffectDuration: 1000,
				stayTime: 3000,
				closeText: '<i class="icon ion-close-round"></i>',
			});
			//$('.check_class').hide();
			$(".purchase_items").addClass('checkek_edit');
		});

		$(".inputSwitch span").on("click", function () {
			var $this = $(this);
			$this.hide().siblings("input").val($this.text()).show();
		});

		$(".inputSwitch input").bind('blur', function () {
			var $this = $(this);
			$(this).attr('value', $(this).val());
			$this.hide().siblings("span").text($this.val()).show();
		}).hide();

		$('#additional_item').change(function () {
			var element = $(this);
			var category_id = $(this).val();
			var warehouse_id = $('#warehouse').val();
			var dispatch_no = $('#dispatch_no').val();
			var purchase_bill_no = $('#purchase_bill_no').val();
			var unit_id_hidden = $('#item_unit_id_hidden').val();
		});

	</script>

	<?php $url = Yii::app()->createAbsoluteUrl("wh/Warehousereceipt/GetItemsByPurchaseOrDespatch"); ?>
	<script>
		function getAllItems(purchase_bill_no, dispatch_no, receipt_id, transfer_type) {
			$.ajax({
				url: "<?php echo $url; ?>",
				data: { "purchase_bill_no": purchase_bill_no, "transfer_type": transfer_type, "dispatch_no": dispatch_no, "receipt_id": receipt_id },
				//dataType: "json",
				type: "GET",
				success: function (data) {

					$("#warehousereceipt_items").html(data);

				}
			});
		}
		function saveitems(attr) {
			$("#previous_details").hide();
			var attr = attr;
			var attr_id_arr = attr.split("_");
			var attr_id = attr_id_arr.pop();
			var receipt_id = $('input[name="warehousereceipt_id"]').val();
			var warehouse_from = $('#Warehousereceipt_warehousereceipt_warehouseid_from').val();
			var warehouse = $('#Warehousereceipt_warehousereceipt_warehouseid').val();
			var dispatch_no = $("#dispatch_no").val();
			var purchase_bill_no = $("#purchase_bill_no").val();
			var transfer_type = $("#transfer_type").val();
			var vendor = $('#vendor').val();
			var date = $(".date").val();
			var receipt_no = $('#receipt_no').val();
			var clerk = $('#clerk').val();

			var sl_no = $('#sl_no_' + attr_id).val();
			var stock_item = $('#stock_item_' + attr_id).val();
			var item_id = $('#item_type_id_' + attr_id).val();
			var rates = $('#rate_' + attr_id).val();
			var transferablequantity = $('#billedquantity_' + attr_id).val();
			var item_unit_id = $('#bill_unit_id_' + attr_id).val();
			var item_unit_base_unit = $('#item_unit_base_unit_' + attr_id).val();
			var batch = $('#itembatch_' + attr_id).val();
			var receivedquantity = $('#receivedquantity_' + attr_id).val();
			var accepted_quantity = $('#accepted_quantity_' + attr_id).val();
			var rejected_quantity = $('#rejected_quantity_' + attr_id).val();
			var remark_data = $('#remark_data_' + attr_id).val();
			var jono = $('#jono_' + attr_id).val();
			var warehouse_receipt_item_id = $('#warehouse_receipt_item_id_' + attr_id).val();
			var billitem_purchase_type = $('#billitem_purchase_type_' + attr_id).val();
			var warehouse_receipt_item_length = $('#item_length_' + attr_id).val();
			var warehouse_receipt_item_width = $('#item_width_' + attr_id).val();
			var warehouse_receipt_item_height = $('#item_height_' + attr_id).val();
			var purchase_type = $('#purchase_type_' + attr_id).val();
			var item_dimension = $('#item_dimension_' + attr_id).val();
			var item_dimension_category = $('#item_dimension_category_' + attr_id).val();
			var itemunit = $('#bill_unit_id_data' + attr_id).val();
			var itemqty = $('#billedquantity_data' + attr_id).val();
			var itemrate = $('#rate_data' + attr_id).val();



			if (purchase_bill_no != "") {
				checkrequired = purchase_bill_no;
			} else {
				if (warehouse_from != "") {
					checkrequired = dispatch_no;
				} else {
					checkrequired = '';
				}
			}

			if (warehouse == '' || date == '' || receipt_no == '' || transfer_type == '' || clerk == '' || checkrequired == '') {
				$().toastmessage('showErrorToast', "Please enter Warehouse receipt details");
			} else {
				if ((moment(date, 'DD-MM-YYYY', true).isValid())) {

					if (stock_item == '' || receivedquantity == 0 || accepted_quantity == 0) {
						$().toastmessage('showErrorToast', "Please fill item details");
					} else {
						if (warehouse_receipt_item_id == '') {
							var receipt_id = $('input[name="warehousereceipt_id"]').val();
							$.ajax({
								url: '<?php echo Yii::app()->createAbsoluteUrl('wh/warehousereceipt/receiptitem'); ?>',
								type: 'POST',
								dataType: 'json',
								data: {
									'warehousereceipt_id': receipt_id,
									'transfer_type': transfer_type,
									'Warehousereceipt[warehousereceipt_warehouseid]': warehouse,
									'Warehousereceipt[warehousereceipt_warehouseid_from]': warehouse_from,
									'sl_no': sl_no,
									'stock_item': stock_item,
									'item_id': item_id,
									'rates': rates,
									'transferablequantity': transferablequantity,
									'item_unit_id': item_unit_id,
									'item_unit_base_unit': item_unit_base_unit,
									'batch': batch,
									'receivedquantity': receivedquantity,
									'accepted_quantity': accepted_quantity,
									'rejected_quantity': rejected_quantity,
									'remark_data': remark_data,
									'jono': jono,
									'warehouse_receipt_item_id': warehouse_receipt_item_id,
									'billitem_purchase_type': billitem_purchase_type,
									'warehouse_receipt_item_length': warehouse_receipt_item_length,
									'warehouse_receipt_item_width': warehouse_receipt_item_width,
									'warehouse_receipt_item_height': warehouse_receipt_item_height,
									'purchase_type': purchase_type,
									'item_dimension': item_dimension,
									'item_dimension_category': item_dimension_category,
									'itemunit': itemunit,
									'itemqty': itemqty,
									'itemrate': itemrate,
									'dispatch_no': dispatch_no,
									'purchase_bill_no': purchase_bill_no
								},
								success: function (response) {
									if (response.response == 'success') {
										$().toastmessage('showSuccessToast', "" + response.msg + "");
										$('#warehouse_receipt_item_id_' + attr_id).val(response.warehouse_receipt_item_id);
										$('#transfer_type').attr("disabled", true);
										$('#purchase_bill_no').attr("disabled", true);
										$('#Warehousereceipt_warehousereceipt_warehouseid_from').attr("disabled", true);
										$('#dispatch_no').attr("disabled", true);
										$('#Warehousereceipt_warehousereceipt_warehouseid').attr("disabled", true);
									} else {
										$().toastmessage('showErrorToast', "" + response.msg + "");
										$("#receivedquantity_" + attr_id).val(0);
										$("#accepted_quantity_" + attr_id).val(0);
										$("#rejected_quantity_" + attr_id).val(0);
									}

								}
							});

						} else {
							var receipt_id = $('input[name="warehousereceipt_id"]').val();
							$.ajax({
								url: '<?php echo Yii::app()->createAbsoluteUrl('wh/warehousereceipt/updatereceiptitem'); ?>',
								type: 'POST',
								dataType: 'json',
								data: {
									'warehousereceipt_id': receipt_id,
									'transfer_type': transfer_type,
									'Warehousereceipt[warehousereceipt_warehouseid]': warehouse,
									'Warehousereceipt[warehousereceipt_warehouseid_from]': warehouse_from,
									'sl_no': sl_no,
									'stock_item': stock_item,
									'item_id': item_id,
									'rates': rates,
									'transferablequantity': transferablequantity,
									'item_unit_id': item_unit_id,
									'item_unit_base_unit': item_unit_base_unit,
									'batch': batch,
									'receivedquantity': receivedquantity,
									'accepted_quantity': accepted_quantity,
									'rejected_quantity': rejected_quantity,
									'remark_data': remark_data,
									'jono': jono,
									'warehouse_receipt_item_id': warehouse_receipt_item_id,
									'billitem_purchase_type': billitem_purchase_type,
									'warehouse_receipt_item_length': warehouse_receipt_item_length,
									'warehouse_receipt_item_width': warehouse_receipt_item_width,
									'warehouse_receipt_item_height': warehouse_receipt_item_height,
									'purchase_type': purchase_type,
									'item_dimension': item_dimension,
									'item_dimension_category': item_dimension_category,
									'itemunit': itemunit,
									'itemqty': itemqty,
									'itemrate': itemrate,
									'dispatch_no': dispatch_no,
									'purchase_bill_no': purchase_bill_no
								},
								success: function (response) {

									if (response.response == 'success') {
										$().toastmessage('showSuccessToast', "" + response.msg + "");
										$('#warehouse_receipt_item_id_' + attr_id).val(response.warehouse_receipt_item_id);
										$('#transfer_type').attr("disabled", true);
										$('#purchase_bill_no').attr("disabled", true);
										$('#Warehousereceipt_warehousereceipt_warehouseid_from').attr("disabled", true);
										$('#dispatch_no').attr("disabled", true);
										$('#Warehousereceipt_warehousereceipt_warehouseid').attr("disabled", true);
									} else {

										$().toastmessage('showErrorToast', "" + response.msg + "");
										$("#receivedquantity_" + attr_id).val(0);
										$("#accepted_quantity_" + attr_id).val(0);
										$("#rejected_quantity_" + attr_id).val(0);
									}

								}
							});
						}
					}

				} else {

					$(this).focus();
					$().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
					$(this).focus();
				}




			}
		}
		/* Neethu  */
		$(document).on("change", "#Warehousereceipt_warehousereceipt_warehouseid", function () {
			var element = $(this);
			var receipt_id = $("#warehousereceipt_id").val();
			var default_date = $(".date").val();
			var warehouse = $(this).val();
			var warehouse_from = $('#Warehousereceipt_warehousereceipt_warehouseid_from').val();
			var vendor = $('#vendor').val();
			var dispatch_no = $("#dispatch_no").val();
			var purchase_bill_no = $("#purchase_bill_no").val();
			var transfer_type = $("#transfer_type").val();
			var receipt_no = $('#receipt_no').val();
			var clerk = $('#clerk').val();
			if (warehouse != '' || (warehouse != '' && warehouse_from != '')) {
				$.ajax({
					method: "GET",
					async: true,
					data: { receipt_id: receipt_id, receipt_no: receipt_no, transfer_type: transfer_type, warehouse_from: warehouse_from, dispatch_no: dispatch_no, purchase_bill_no: purchase_bill_no, default_date: default_date, warehouse: warehouse, vendor: vendor, clerk: clerk },
					url: '<?php echo Yii::app()->createUrl('wh/warehousereceipt/DespatchPurchaseBilllist'); ?>',
					success: function (response) {
						var result = JSON.parse(response);
						$('select[id="dispatch_no"]').empty();
						$('select[id="dispatch_no"]').append('<option value="">Choose Dispatch No</option>');
						$.each(result["despatches"], function (key, value) {
							$('select[id="dispatch_no"]').append('<option value="' + value.id + '">' + value.value + '</option>');
						});
						$('select[id="purchase_bill_no"]').empty();
						$('select[id="purchase_bill_no"]').append('<option value="">Choose Purchase Bill No</option>');
						$.each(result["bills"], function (key, value) {
							$('select[id="purchase_bill_no"]').append('<option value="' + value.id + '">' + value.value + '</option>');
						});
						$("#Warehousereceipt_warehousereceipt_warehouseid_from option[value='" + warehouse + "']").remove();


					}
				});
			}

			var checkrequired = '';
			if (purchase_bill_no != "") {
				checkrequired = purchase_bill_no;
			} else {
				if (warehouse_from != "") {
					checkrequired = dispatch_no;
				} else {
					checkrequired = '';
				}
			}
			if ((moment(default_date, 'DD-MM-YYYY', true).isValid())) {
				if (warehouse == '' || default_date == '' || receipt_no == '' || transfer_type == '' || clerk == '' || checkrequired == '') {
					$.ajax({
						method: "GET",
						dataType: "json",
						url: '<?php echo Yii::app()->createUrl('wh/warehousereceipt/ajax'); ?>',
						success: function (result) {
							$(".date").select2("focus");
						}
					});
				} else {
					$.ajax({
						method: "GET",
						async: true,
						data: { receipt_id: receipt_id, transfer_type: transfer_type, receipt_no: receipt_no, warehouse_from: warehouse_from, dispatch_no: dispatch_no, purchase_bill_no: purchase_bill_no, default_date: default_date, warehouse: warehouse, vendor: vendor, clerk: clerk },
						dataType: "json",
						url: '<?php echo Yii::app()->createUrl('wh/warehousereceipt/createnewreceipt'); ?>',
						success: function (result) {
							if (result.response == 'success') {
								$(".purchase_items").removeClass('checkek_edit');
								$('.js-example-basic-single').select2('focus');
								$("#warehousereceipt_id").val(result.receipt_id);
								$().toastmessage('showSuccessToast', "" + result.msg + "");
							} else {
								$().toastmessage('showErrorToast', "" + result.msg + "");
								element.val('');
							}

							$("#additional_item").select2("focus");
						}
					});
				}
			} else {
				$(this).focus();
				$().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
				$(this).focus();
			}
		});
		$(document).on("change", "#vendor", function () {
			var element = $(this);
			var receipt_id = $("#warehousereceipt_id").val();
			var default_date = $(".date").val();
			var vendor = $(this).val();
			var warehouse_from = $("#Warehousereceipt_warehousereceipt_warehouseid_from").val();
			var dispatch_no = $("#dispatch_no").val();
			var purchase_bill_no = $("#purchase_bill_no").val();
			var transfer_type = $("#transfer_type").val();
			var warehouse = $('#Warehousereceipt_warehousereceipt_warehouseid').val();
			var receipt_no = $('#receipt_no').val();
			var clerk = $('#clerk').val();
			var checkrequired = '';
			if (purchase_bill_no != "") {
				checkrequired = purchase_bill_no;
			} else {
				if (warehouse_from != "") {
					checkrequired = dispatch_no;
				} else {
					checkrequired = '';
				}
			}
			if ((moment(default_date, 'DD-MM-YYYY', true).isValid())) {

				if (warehouse == '' || default_date == '' || receipt_no == '' || transfer_type == '' || clerk == '' || checkrequired == '') {
					$.ajax({
						method: "GET",
						dataType: "json",
						url: '<?php echo Yii::app()->createUrl('wh/warehousereceipt/ajax'); ?>',
						success: function (result) {
							$("#vendor").focus();
						}
					});

				} else {
					$.ajax({
						method: "GET",
						data: { receipt_id: receipt_id, transfer_type: transfer_type, receipt_no: receipt_no, warehouse_from: warehouse_from, dispatch_no: dispatch_no, purchase_bill_no: purchase_bill_no, default_date: default_date, warehouse: warehouse, vendor: vendor, clerk: clerk },
						dataType: "json",
						url: '<?php echo Yii::app()->createUrl('wh/warehousereceipt/createnewreceipt'); ?>',
						success: function (result) {
							if (result.response == 'success') {
								$(".purchase_items").removeClass('checkek_edit');
								$('.js-example-basic-single').select2('focus');
								$("#warehousereceipt_id").val(result.receipt_id);
								$().toastmessage('showSuccessToast', "" + result.msg + "");
							} else {
								$().toastmessage('showErrorToast', "" + result.msg + "");
							}

							//$("#additional_item").select2("focus");
						}
					});

				}

			} else {
				$(this).focus();
				$().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
				$(this).focus();
			}

		});
		$(document).on("change", "#transfer_type", function () {
			var transfer_type = $(this).val();
			var transfer_type_val = $.trim($("#transfer_type option:selected").text());
			if (transfer_type_val == 'Despatch') {
				$('#Add_purchase_form').hide();
				$('#add_item_table').hide();
			} else {
				$('#Add_purchase_form').show();
				$('#add_item_table').show();
			}
			var element = $(this);
			var receipt_id = $("#warehousereceipt_id").val();
			var default_date = $(".date").val();
			var warehouse_from = $("#Warehousereceipt_warehousereceipt_warehouseid_from").val();
			var dispatch_no = $("#dispatch_no").val();
			var purchase_bill_no = $("#purchase_bill_no").val();
			var vendor = $("#vendor").val();
			var warehouse = $('#Warehousereceipt_warehousereceipt_warehouseid').val();
			var receipt_no = $('#receipt_no').val();
			var clerk = $('#clerk').val();
			var checkrequired = '';
			if (transfer_type == "") {
				$(".elem_block_purchase_bill_no").hide();
				$(".elem_block_dispatch_no").hide();
				$(".elem_block_warehouseid_from").hide();
				$("#warehousereceipt_items").hide();
			} else {
				if (transfer_type == 1) {
					$(".elem_block_purchase_bill_no").show();
					$(".elem_block_warehouseid_from").hide();
					$(".elem_block_dispatch_no").hide();
					$("#warehousereceipt_items").hide();
				} else {
					$(".elem_block_warehouseid_from").show();
					$(".elem_block_dispatch_no").show();
					$(".elem_block_purchase_bill_no").hide();
					$("#warehousereceipt_items").hide();
				}
			}
			if (purchase_bill_no != "") {
				checkrequired = purchase_bill_no;
			} else {
				if (warehouse_from != "") {
					checkrequired = dispatch_no;
				} else {
					checkrequired = '';
				}
			}
			if ((moment(default_date, 'DD-MM-YYYY', true).isValid())) {

				if (warehouse == '' || default_date == '' || receipt_no == '' || transfer_type == '' || clerk == '' || checkrequired == '') {
					$.ajax({
						method: "GET",
						dataType: "json",
						url: '<?php echo Yii::app()->createUrl('wh/warehousereceipt/ajax'); ?>',
						success: function (result) {
							if (transfer_type == 1) {
								$("#purchase_bill_no").focus()
							} else {
								$("#Warehousereceipt_warehousereceipt_warehouseid_from").focus();
							}
						}
					});

				} else {
					$.ajax({
						method: "GET",
						data: { receipt_id: receipt_id, transfer_type: transfer_type, receipt_no: receipt_no, warehouse_from: warehouse_from, dispatch_no: dispatch_no, purchase_bill_no: purchase_bill_no, default_date: default_date, warehouse: warehouse, vendor: vendor, clerk: clerk },
						dataType: "json",
						url: '<?php echo Yii::app()->createUrl('wh/warehousereceipt/createnewreceipt'); ?>',
						success: function (result) {
							if (result.response == 'success') {
								$(".purchase_items").removeClass('checkek_edit');
								$('.js-example-basic-single').select2('focus');
								$("#warehousereceipt_id").val(result.receipt_id);
								$().toastmessage('showSuccessToast', "" + result.msg + "");
							} else {
								$().toastmessage('showErrorToast', "" + result.msg + "");
							}

							$("#additional_item").select2("focus");
						}
					});

				}

			} else {
				$(this).focus();
				$().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
				$(this).focus();
			}

		});

		$(document).on("change", "#clerk", function () {
			var element = $(this);
			var receipt_id = $("#warehousereceipt_id").val();
			var default_date = $(".date").val();
			var clerk = $(this).val();
			var warehouse_from = $('#Warehousereceipt_warehousereceipt_warehouseid_from').val();
			var dispatch_no = $("#dispatch_no").val();
			var purchase_bill_no = $("#purchase_bill_no").val();
			var transfer_type = $("#transfer_type").val();
			var warehouse = $('#Warehousereceipt_warehousereceipt_warehouseid').val();
			var receipt_no = $('#receipt_no').val();
			var vendor = $('#vendor').val();
			var checkrequired = '';
			if (purchase_bill_no != "") {
				checkrequired = purchase_bill_no;
			} else {
				if (warehouse_from != "") {
					checkrequired = dispatch_no;
				} else {
					checkrequired = '';
				}
			}
			if ((moment(default_date, 'DD-MM-YYYY', true).isValid())) {
				if (warehouse == '' || default_date == '' || receipt_no == '' || transfer_type == '' || clerk == '' || checkrequired == '') {
					$.ajax({
						method: "GET",
						data: { purchase_id: 'test' },
						dataType: "json",
						url: '<?php echo Yii::app()->createUrl('wh/warehousereceipt/ajax'); ?>',
						success: function (result) {
							$("#receipt_no").focus();
						}
					});

				} else {
					$.ajax({
						method: "GET",
						data: { receipt_id: receipt_id, transfer_type: transfer_type, receipt_no: receipt_no, warehouse_from: warehouse_from, dispatch_no: dispatch_no, purchase_bill_no: purchase_bill_no, default_date: default_date, warehouse: warehouse, vendor: vendor, clerk: clerk },
						dataType: "json",
						url: '<?php echo Yii::app()->createUrl('warehousereceipt/createnewreceipt'); ?>',
						success: function (result) {
							if (result.response == 'success') {
								$(".purchase_items").removeClass('checkek_edit');
								$('.js-example-basic-single').select2('focus');
								$("#warehousereceipt_id").val(result.receipt_id);
								$().toastmessage('showSuccessToast', "" + result.msg + "");
							} else {
								$().toastmessage('showErrorToast', "" + result.msg + "");
							}

							$("#additional_item").select2("focus");
						}
					});

				}

			} else {
				$(this).focus();
				$().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
				$(this).focus();
			}

		});



		$(document).on("change", ".date", function () {
			var element = $(this);
			var receipt_id = $("#warehousereceipt_id").val();
			var default_date = $(this).val();
			var warehouse = $('#Warehousereceipt_warehousereceipt_warehouseid').val();
			var warehouse_from = $('#Warehousereceipt_warehousereceipt_warehouseid_from').val();
			var dispatch_no = $("#dispatch_no").val();
			var purchase_bill_no = $("#purchase_bill_no").val();
			var transfer_type = $("#transfer_type").val();
			var vendor = $('#vendor').val();
			var receipt_no = $('#receipt_no').val();
			var clerk = $('#clerk').val();
			if (purchase_bill_no != "") {
				checkrequired = purchase_bill_no;
			} else {
				if (warehouse_from != "") {
					checkrequired = dispatch_no;
				} else {
					checkrequired = '';
				}
			}
			if ((moment(default_date, 'DD-MM-YYYY', true).isValid())) {
				if (warehouse == '' || default_date == '' || receipt_no == '' || transfer_type == '' || clerk == '' || checkrequired == '') {

				} else {
					$.ajax({
						method: "GET",
						async: false,
						data: { receipt_id: receipt_id, transfer_type: transfer_type, receipt_no: receipt_no, warehouse_from: warehouse_from, dispatch_no: dispatch_no, purchase_bill_no: purchase_bill_no, default_date: default_date, warehouse: warehouse, vendor: vendor, clerk: clerk },
						dataType: "json",
						url: '<?php echo Yii::app()->createUrl('wh/warehousereceipt/createnewreceipt'); ?>',
						success: function (result) {
							if (result.response == 'success') {
								$(".purchase_items").removeClass('checkek_edit');
								$('.js-example-basic-single').select2('focus');
								$("#warehousereceipt_id").val(result.receipt_id);
								$().toastmessage('showSuccessToast', "" + result.msg + "");
							} else {
								$().toastmessage('showErrorToast', "" + result.msg + "");
							}
							$("#additional_item").select2("focus");
						}

					});

				}
			} else {
				$(this).focus();
				$().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
				$(this).focus();
			}

			$('#clerk').focus();

		});


		$(document).on("blur", "#receipt_no", function () {
			var element = $(this);
			var receipt_id = $("#warehousereceipt_id").val();
			var default_date = $(".date").val();
			var receipt_no = $(this).val();
			var through = $('#through').val();
			var warehouse_from = $('#Warehousereceipt_warehousereceipt_warehouseid_from').val();
			var dispatch_no = $("#dispatch_no").val();
			var purchase_bill_no = $("#purchase_bill_no").val();
			var transfer_type = $("#transfer_type").val();
			var warehouse = $('#Warehousereceipt_warehousereceipt_warehouseid').val();
			var clerk = $('#clerk').val();
			var vendor = $('#vendor').val();
			if (purchase_bill_no != "") {
				checkrequired = purchase_bill_no;
			} else {
				if (warehouse_from != "") {
					checkrequired = dispatch_no;
				} else {
					checkrequired = '';
				}
			}
			if ((moment(default_date, 'DD-MM-YYYY', true).isValid())) {
				if (warehouse == '' || default_date == '' || receipt_no == '' || transfer_type == '' || clerk == '' || checkrequired == '') {

				} else {
					$.ajax({
						method: "GET",
						data: { receipt_id: receipt_id, transfer_type: transfer_type, receipt_no: receipt_no, warehouse_from: warehouse_from, dispatch_no: dispatch_no, purchase_bill_no: purchase_bill_no, default_date: default_date, warehouse: warehouse, vendor: vendor, clerk: clerk },
						dataType: "json",
						url: '<?php echo Yii::app()->createUrl('wh/warehousereceipt/createnewreceipt'); ?>',
						success: function (result) {
							if (result.response == 'success') {
								$(".purchase_items").removeClass('checkek_edit');
								$('.js-example-basic-single').select2('focus');
								$("#warehousereceipt_id").val(result.receipt_id);
								$().toastmessage('showSuccessToast', "" + result.msg + "");
							} else {
								$().toastmessage('showErrorToast', "" + result.msg + "");
							}

							$("#additional_item").select2("focus");
						}
					});

				}

			} else {
				$(this).focus();
				$().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
				$(this).focus();
			}

		});
		$(document).on("change", "#Warehousereceipt_warehousereceipt_warehouseid_from", function () {
			var element = $(this);
			var receipt_id = $("#warehousereceipt_id").val();
			var default_date = $(".date").val();
			var warehouse_from = $(this).val();
			var through = $('#through').val();
			var receipt_no = $("#receipt_no").val();
			var dispatch_no = $("#dispatch_no").val();
			$('#purchase_bill_no').prop('selectedIndex', 0);
			var transfer_type = $("#transfer_type").val();
			var purchase_bill_no = $("#purchase_bill_no").val();
			var warehouse = $('#Warehousereceipt_warehousereceipt_warehouseid').val();
			var clerk = $('#clerk').val();
			var vendor = $('#vendor').val();
			if (purchase_bill_no != "") {
				checkrequired = purchase_bill_no;
			} else {
				if (warehouse_from != "") {
					checkrequired = dispatch_no;
				} else {
					checkrequired = '';
				}
			}
			if (warehouse != '' || (warehouse != '' && warehouse_from != '')) {
				$.ajax({
					method: "GET",
					async: true,
					data: { receipt_id: receipt_id, transfer_type: transfer_type, receipt_no: receipt_no, warehouse_from: warehouse_from, dispatch_no: dispatch_no, purchase_bill_no: purchase_bill_no, default_date: default_date, warehouse: warehouse, vendor: vendor, clerk: clerk },
					url: '<?php echo Yii::app()->createUrl('wh/warehousereceipt/DespatchPurchaseBilllist'); ?>',
					success: function (response) {
						var result = JSON.parse(response);
						$('select[id="dispatch_no"]').empty();
						$('select[id="dispatch_no"]').append('<option value="">Choose Dispatch No</option>');
						$.each(result["despatches"], function (key, value) {
							$('select[id="dispatch_no"]').append('<option value="' + value.id + '">' + value.value + '</option>');
						});
						$('select[id="purchase_bill_no"]').empty();
						$('select[id="purchase_bill_no"]').append('<option value="">Choose Purchase Bill No</option>');
						$.each(result["bills"], function (key, value) {
							$('select[id="purchase_bill_no"]').append('<option value="' + value.id + '">' + value.value + '</option>');
						});

					}
				});
			}

			if ((moment(default_date, 'DD-MM-YYYY', true).isValid())) {
				if (warehouse == '' || default_date == '' || receipt_no == '' || transfer_type == '' || clerk == '' || checkrequired == '') {

				} else {
					$.ajax({
						method: "GET",
						data: { receipt_id: receipt_id, transfer_type: transfer_type, receipt_no: receipt_no, warehouse_from: warehouse_from, dispatch_no: dispatch_no, purchase_bill_no: purchase_bill_no, default_date: default_date, warehouse: warehouse, vendor: vendor, clerk: clerk },
						dataType: "json",
						url: '<?php echo Yii::app()->createUrl('wh/warehousereceipt/createnewreceipt'); ?>',
						success: function (result) {
							if (result.response == 'success') {
								$(".purchase_items").removeClass('checkek_edit');
								$('.js-example-basic-single').select2('focus');
								$("#warehousereceipt_id").val(result.receipt_id);
								$().toastmessage('showSuccessToast', "" + result.msg + "");
							} else {
								$().toastmessage('showErrorToast', "" + result.msg + "");
							}

							$("#dispatch_no").select2("focus");
						}
					});

				}

			} else {
				$(this).focus();
				$().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
				$(this).focus();
			}

		});
		$(document).on("change", "#dispatch_no", function () {
			$("#warehousereceipt_items").show();
			var element = $(this);
			var receipt_id = $("#warehousereceipt_id").val();
			var default_date = $(".date").val();
			var receipt_no = $("#receipt_no").val();
			var through = $('#through').val();
			var transfer_type = $("#transfer_type").val();
			var warehouse_from = $("#Warehousereceipt_warehousereceipt_warehouseid_from").val();
			var dispatch_no = $(this).val();
			$('#purchase_bill_no').prop('selectedIndex', 0);
			var purchase_bill_no = $("#purchase_bill_no").val();
			var warehouse = $('#Warehousereceipt_warehousereceipt_warehouseid').val();
			var clerk = $('#clerk').val();
			var vendor = $('#vendor').val();
			if (purchase_bill_no != "") {
				checkrequired = purchase_bill_no;
			} else {
				if (warehouse_from != "") {
					checkrequired = dispatch_no;
				} else {
					checkrequired = '';
				}
			}
			if (warehouse != '' || (warehouse != '' && warehouse_from != '')) {
				$.ajax({
					method: "GET",
					async: true,
					data: { receipt_id: receipt_id, transfer_type: transfer_type, receipt_no: receipt_no, warehouse_from: warehouse_from, dispatch_no: dispatch_no, purchase_bill_no: purchase_bill_no, default_date: default_date, warehouse: warehouse, vendor: vendor, clerk: clerk },
					url: '<?php echo Yii::app()->createUrl('wh/warehousereceipt/DespatchPurchaseBilllist'); ?>',
					success: function (response) {
						var result = JSON.parse(response);
						$('select[id="purchase_bill_no"]').empty();
						$('select[id="purchase_bill_no"]').append('<option value="">Choose Purchase Bill No</option>');
						$.each(result["bills"], function (key, value) {
							$('select[id="purchase_bill_no"]').append('<option value="' + value.id + '">' + value.value + '</option>');
						});

					}
				});
			}

			if ((moment(default_date, 'DD-MM-YYYY', true).isValid())) {
				if (warehouse == '' || default_date == '' || receipt_no == '' || transfer_type == '' || clerk == '' || checkrequired == '') {

				} else {
					$.ajax({
						method: "GET",
						data: { receipt_id: receipt_id, transfer_type: transfer_type, receipt_no: receipt_no, warehouse_from: warehouse_from, dispatch_no: dispatch_no, purchase_bill_no: purchase_bill_no, default_date: default_date, warehouse: warehouse, vendor: vendor, clerk: clerk },
						dataType: "json",
						url: '<?php echo Yii::app()->createUrl('wh/warehousereceipt/createnewreceipt'); ?>',
						success: function (result) {
							if (result.response == 'success') {
								$(".purchase_items").removeClass('checkek_edit');
								$('.js-example-basic-single').select2('focus');
								$("#warehousereceipt_id").val(result.receipt_id);
								$().toastmessage('showSuccessToast', "" + result.msg + "");
								if (dispatch_no == "") {
									dispatch_no = 0;
								} else {
									getAllItems(purchase_bill_no, dispatch_no, receipt_id, transfer_type);
								}
							} else {
								$().toastmessage('showErrorToast', "" + result.msg + "");
							}

							$("#vendor").focus();

						}
					});

				}

			} else {
				$(this).focus();
				$().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
				$(this).focus();
			}

		});
		$(document).on("change", "#purchase_bill_no", function () {
			$("#warehousereceipt_items").show();
			var element = $(this);
			var receipt_id = $("#warehousereceipt_id").val();
			var default_date = $(".date").val();
			var receipt_no = $("#receipt_no").val();
			var through = $('#through').val();
			$('#dispatch_no').prop('selectedIndex', 0);
			$('#Warehousereceipt_warehousereceipt_warehouseid_from').prop('selectedIndex', 0);
			var warehouse_from = $("#Warehousereceipt_warehousereceipt_warehouseid_from").val();
			var dispatch_no = $("#dispatch_no").val();
			var transfer_type = $("#transfer_type").val();
			var purchase_bill_no = $(this).val();
			var warehouse = $('#Warehousereceipt_warehousereceipt_warehouseid').val();
			var clerk = $('#clerk').val();
			var vendor = $('#vendor').val();
			$('#Warehousereceipt_warehousereceipt_warehouseid_from').val("");
			if (purchase_bill_no != "") {
				checkrequired = purchase_bill_no;
			} else {
				if (warehouse_from != "") {
					checkrequired = dispatch_no;
				} else {
					checkrequired = '';
				}
			}
			if (warehouse != '' || (warehouse != '' && warehouse_from != '')) {
				$.ajax({
					method: "GET",
					async: true,
					data: { receipt_id: receipt_id, transfer_type: transfer_type, receipt_no: receipt_no, warehouse_from: warehouse_from, dispatch_no: dispatch_no, purchase_bill_no: purchase_bill_no, default_date: default_date, warehouse: warehouse, vendor: vendor, clerk: clerk },
					url: '<?php echo Yii::app()->createUrl('wh/warehousereceipt/DespatchPurchaseBilllist'); ?>',
					success: function (response) {
						var result = JSON.parse(response);
						$('select[id="dispatch_no"]').empty();
						$('select[id="dispatch_no"]').append('<option value="">Choose Dispatch No</option>');
						$.each(result["despatches"], function (key, value) {
							$('select[id="dispatch_no"]').append('<option value="' + value.id + '">' + value.value + '</option>');
						});

					}
				});
			}

			if ((moment(default_date, 'DD-MM-YYYY', true).isValid())) {
				if (warehouse == '' || default_date == '' || receipt_no == '' || transfer_type == '' || clerk == '' || checkrequired == '') {

				} else {
					$.ajax({
						method: "GET",
						data: { receipt_id: receipt_id, transfer_type: transfer_type, receipt_no: receipt_no, warehouse_from: warehouse_from, dispatch_no: dispatch_no, purchase_bill_no: purchase_bill_no, default_date: default_date, warehouse: warehouse, vendor: vendor, clerk: clerk },
						dataType: "json",
						url: '<?php echo Yii::app()->createUrl('wh/warehousereceipt/createnewreceipt'); ?>',
						success: function (result) {
							if (result.response == 'success') {
								$(".purchase_items").removeClass('checkek_edit');
								$('.js-example-basic-single').select2('focus');
								$("#warehousereceipt_id").val(result.receipt_id);
								$().toastmessage('showSuccessToast', "" + result.msg + "");
								if (purchase_bill_no == "") {
									purchase_bill_no = 0;
								} else {
									getAllItems(purchase_bill_no, dispatch_no, receipt_id, transfer_type);
								}
							} else {
								$().toastmessage('showErrorToast', "" + result.msg + "");
							}

							$("#additional_item").select2("focus");
						}
					});

				}

			} else {
				$(this).focus();
				$().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
				$(this).focus();
			}




		});



		$("#Warehousereceipt_warehousereceipt_warehouseid").change(function () {
			$.ajax({
				method: "GET",
				dataType: "json",
				url: '<?php echo Yii::app()->createUrl('wh/warehousereceipt/ajax'); ?>',
				success: function (result) {
					$("#datepicker").focus();
				}
			});
		});

		$("#clerk").change(function () {
			$.ajax({
				method: "GET",
				dataType: "json",
				url: '<?php echo Yii::app()->createUrl('wh/warehousereceipt/ajax'); ?>',
				success: function (result) {
					$("#receipt_no").focus();
				}
			});
		});
		$("#Warehousereceipt_warehousereceipt_warehouseid_from").change(function () {
			$.ajax({
				method: "GET",
				dataType: "json",
				url: '<?php echo Yii::app()->createUrl('wh/warehousereceipt/ajax'); ?>',
				success: function (result) {
					$("#dispatch_no").focus();
				}
			});
		});
		$("#dispatch_no").change(function () {
			$('#purchase_bill_no').prop('selectedIndex', 0);
			$.ajax({
				method: "GET",
				dataType: "json",
				url: '<?php echo Yii::app()->createUrl('wh/warehousereceipt/ajax'); ?>',
				success: function (result) {
					$("#dispatch_no").focus();
				}
			});
		});
		$("#purchase_bill_no").change(function () {
			$('#dispatch_no').prop('selectedIndex', 0);
			$('#Warehousereceipt_warehousereceipt_warehouseid_from').prop('selectedIndex', 0);
			$.ajax({
				method: "GET",
				dataType: "json",
				url: '<?php echo Yii::app()->createUrl('wh/warehousereceipt/ajax'); ?>',
				success: function (result) {
					$('#vendor').focus();
				}
			});
		});



		$("#vendor").keyup(function (event) {
			if (event.keyCode == 13) {
				$("#vendor").click();
			}
		});

		var sl_no = 1;
		var howMany = 0;
		$(document).on('click', '.item_save', function () {
			$("#previous_details").hide();
			var element = $(this);
			var item_id = $(this).attr('id');

			if (item_id == 0) {

				// add
				var warehouse_from = $('#Warehousereceipt_warehousereceipt_warehouseid_from').val();
				var warehouse = $('#Warehousereceipt_warehousereceipt_warehouseid').val();
				var dispatch_no = $("#dispatch_no").val();
				var purchase_bill_no = $("#purchase_bill_no").val();
				var transfer_type = $("#transfer_type").val();
				var vendor = $('#vendor').val();
				var date = $(".date").val();
				var receipt_no = $('#receipt_no').val();
				var clerk = $('#clerk').val();
				var transfer_type_item_id = null;
				var item_unit_base_unit = $("#additional_item_base_unit").val();
				var stock_item = $('#additional_item').val();
				var batch = $.trim($('#bibatch').val());
				var baseqty = $('#basequantity').val();
				var baseunit = $('#item_unit_additional').val();;
				var baserate = $('#baserate').val();
				var itemunit = $('#item_unit_additional_purchase').val();
				var itemqty = $('#quantity').val();
				var itemrate = $('#rate').val();

				if (baseqty != '' && baserate != '' && baseunit != '') {
					if (baseunit != itemunit) {
						var quantity = baseqty;
						var unit = baseunit;
						var rates = baserate;
					} else {
						var quantity = itemqty;
						var unit = itemunit;
						var rates = itemrate;
					}
				} else {
					var quantity = itemqty;
					var unit = itemunit;
					var rates = itemrate;
				}
				var amount = $('#amount').val();
				var jono = $("#jono").val();
				var rejected_quantity = 0;
				var rowCount = $('.table .addrow tr').length;
				if (purchase_bill_no != "") {
					checkrequired = purchase_bill_no;
				} else {
					if (warehouse_from != "") {
						checkrequired = dispatch_no;
					} else {
						checkrequired = '';
					}
				}

				if (warehouse == '' || date == '' || receipt_no == '' || transfer_type == '' || clerk == '' || checkrequired == '') {
					$().toastmessage('showErrorToast', "Please enter Warehouse receipt details");
				} else {
					if ((moment(date, 'DD-MM-YYYY', true).isValid())) {

						if (stock_item == '' || quantity == 0 || rate == '' || amount == '') {
							$().toastmessage('showErrorToast', "Please fill item details");
						} else {
							howMany += 1;
							if (howMany == 1) {
								var receipt_id = $('input[name="warehousereceipt_id"]').val();
								var data = { 'sl_no': rowCount, 'quantity': quantity, 'batch': batch, 'rate': rate, 'amount': amount, 'unit': unit, 'stock_item': stock_item, 'receipt_id': receipt_id, 'jono': jono, 'transfer_type': transfer_type, };
								$.ajax({
									url: '<?php echo Yii::app()->createAbsoluteUrl('wh/warehousereceipt/receiptitem'); ?>',
									type: 'POST',
									dataType: 'json',
									data: {
										'warehousereceipt_id': receipt_id,
										'transfer_type': transfer_type,
										'Warehousereceipt[warehousereceipt_warehouseid]': warehouse,
										'Warehousereceipt[warehousereceipt_warehouseid_from]': warehouse_from,
										'sl_no': sl_no,
										'stock_item': stock_item,
										'item_id': transfer_type_item_id,
										'rates': rates,
										'transferablequantity': quantity,
										'item_unit_id': unit,
										'item_unit_base_unit': item_unit_base_unit,
										'batch': batch,
										'receivedquantity': quantity,
										'accepted_quantity': quantity,
										'rejected_quantity': rejected_quantity,
										'jono': jono,
										'warehouse_receipt_item_id': item_id, 'baseqty': baseqty,
										'itemqty': itemqty,
										'itemunit': itemunit,
										'itemrate': itemrate,
										'dispatch_no': dispatch_no,
										'purchase_bill_no': purchase_bill_no
									},
									success: function (response) {
										if (response.response == 'success') {
											$().toastmessage('showSuccessToast', "" + response.msg + "");
											$('.addrow').html(response.html);
										} else {
											$().toastmessage('showErrorToast', "" + response.msg + "");
										}
										howMany = 0;
										$('#additional_item').val('').trigger('change');
										$('#bibatch').val('');
										$('.additional_rate').val('0');
										$('#amount').val('0');
										$("#jono_additional").val('');
										$('#quantity').val('0');
										var quantity = $('#quantity').val('0');
										var accepted_quantity = $('#accepted_quantity').val('');
										var rejected_quantity = $('#rejected_quantity').val('');
										$("#jono").val('');
										var unit = $('#item_unit').val('');
										$('#batch').val('');
										$('#additional_item').select2('focus');
										$('#item_baseunit_data').hide();
										$('#item_conversion_data').hide();
										$('.base-data').hide();
									}
								});

								$('.js-example-basic-single').select2('focus');
							}
						}

					} else {

						$(this).focus();
						$().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
						$(this).focus();
					}
				}
			} else {
				// update


				var warehouse_from = $('#Warehousereceipt_warehousereceipt_warehouseid_from').val();
				var warehouse = $('#Warehousereceipt_warehousereceipt_warehouseid').val();
				var dispatch_no = $("#dispatch_no").val();
				var purchase_bill_no = $("#purchase_bill_no").val();
				var transfer_type = $("#transfer_type").val();
				var vendor = $('#vendor').val();
				var date = $(".date").val();
				var receipt_no = $('#receipt_no').val();
				var transfer_type_item_id = null;
				var clerk = $('#clerk').val();
				var item_unit_base_unit = $("#additional_item_base_unit").val();
				var stock_item = $('#additional_item').val();
				var batch = $.trim($('#bibatch').val());

				var baseqty = $('#basequantity').val();
				var baseunit = $('#item_unit_additional').val();
				var baserate = $('#baserate').val();

				var itemunit = $('#item_unit_additional_purchase').val();
				var itemqty = $('#quantity').val();
				var itemrate = $('#rate').val();

				if (baseqty != '' && baserate != '' && baseunit != '') {
					if (baseunit != itemunit) {
						var quantity = baseqty;
						var unit = baseunit;
						var rates = baserate;
					} else {
						var quantity = itemqty;
						var unit = itemunit;
						var rates = itemrate;
					}
				} else {
					var quantity = itemqty;
					var unit = itemunit;
					var rates = itemrate;
				}
				var amount = $('#amount').val();
				var jono = $("#jono").val();
				var rejected_quantity = 0;
				if (purchase_bill_no != "") {
					checkrequired = purchase_bill_no;
				} else {
					if (warehouse_from != "") {
						checkrequired = dispatch_no;
					} else {
						checkrequired = '';
					}
				}
				if (warehouse == '' || date == '' || receipt_no == '' || transfer_type == '' || clerk == '' || checkrequired == '') {
					$().toastmessage('showErrorToast', "Please enter Warehouse receipt details");
				} else {

					if ((moment(date, 'DD-MM-YYYY', true).isValid())) {
						if (stock_item == '' || quantity == 0 || rate == '' || amount == '') {
							$().toastmessage('showErrorToast', "Please fill item details");
						} else {
							howMany += 1;
							if (howMany == 1) {
								var receipt_id = $('input[name="warehousereceipt_id"]').val();
								//var data = {'sl_no':sl_no,'quantity':quantity,'batch':batch,'rate':rate,'amount':amount,'unit':unit,'stock_item':stock_item,'receipt_id' : receipt_id,'jono':jono,};
								$.ajax({
									url: '<?php echo Yii::app()->createAbsoluteUrl('wh/warehousereceipt/updatereceiptitem'); ?>',

									type: 'POST',
									dataType: 'json',
									data: {
										'warehousereceipt_id': receipt_id,
										'transfer_type': transfer_type,
										'Warehousereceipt[warehousereceipt_warehouseid]': warehouse,
										'Warehousereceipt[warehousereceipt_warehouseid_from]': warehouse_from,
										'sl_no': sl_no,
										'stock_item': stock_item,
										'item_id': transfer_type_item_id,
										'rates': rates,
										'transferablequantity': quantity,
										'item_unit_id': unit,
										'item_unit_base_unit': item_unit_base_unit,
										'batch': batch,
										'receivedquantity': quantity,
										'accepted_quantity': quantity,
										'rejected_quantity': rejected_quantity,
										'jono': jono,
										'warehouse_receipt_item_id': item_id,
										'itemqty': itemqty,
										'itemunit': itemunit,
										'itemrate': itemrate,
										'dispatch_no': dispatch_no,
										'purchase_bill_no': purchase_bill_no
									},
									success: function (response) {

										if (response.response == 'success') {
											$('#final_amount').val(response.final_amount);
											$().toastmessage('showSuccessToast', "" + response.msg + "");
											$('#grand_total').text(response.final_amount);
											$('.addrow').html(response.html);
										} else {
											$().toastmessage('showErrorToast', "" + response.msg + "");
										}
										howMany = 0;
										$('#additional_item').val('').trigger('change');
										$('#bibatch').val('');
										$('#item_unit_additional_purchase').val('');
										$('.additional_rate').val('0');
										$('#amount').val('0');
										$("#jono_additional").val('');
										$('#quantity').val('0');
										var quantity = $('#quantity').val('0');
										var accepted_quantity = $('#accepted_quantity').val('');
										var rejected_quantity = $('#rejected_quantity').val('');
										var unit = $('#item_unit').val('');
										$('#batch').val('');
										$(".item_save").attr('value', 'Save');
										$(".item_save").attr('id', 0);
										$('#additional_item').select2('focus');
										$("#jono").val('');
										$('#item_baseunit_data').hide();
										$('#item_conversion_data').hide();
										$('.base-data').hide();
									}
								});

								$('.js-example-basic-single').select2('focus');
							}
						}

					} else {
						$(this).focus();
						$().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
						$(this).focus();
					}
				}
			}
		});


		$(document).on("blur", "#quantity", function () {
			var quantity = $("#quantity").val();
			var rate = $("#rate").val();
			var amount = quantity * rate;
			$("#amount").val(amount);

		});
		$(document).on("blur", "#rate", function () {
			var quantity = $("#quantity").val();
			var rate = $("#rate").val();
			var amount = quantity * rate;
			$("#amount").val(amount);

		});
		$(document).on("blur", "#amount", function () {
			var quantity = $("#quantity").val();
			var rate = $("#rate").val();
			var amount = quantity * rate;
			$("#amount").val(amount);

		});
		$(document).on('click', '.edit_item', function (e) {
			e.preventDefault();
			var item_id = $(this).attr('id');
			var $tds = $(this).closest('tr').find('td');
			var sl_no = $tds.eq(0).text();
			var description = $tds.eq(1).text();
			var batch = $tds.eq(2).text();
			var unit = $tds.eq(3).text();
			unit = $.trim(unit);
			var quantity = $tds.eq(4).text();
			var rate = $tds.eq(5).text();
			var basequantity = $tds.eq(6).text();
			var baserate = $tds.eq(8).text();
			var amount = $tds.eq(9).text();


			//var jono = $tds.eq(7).text();

			if (isNaN(parseFloat(quantity))) {
				quantity = 0;
			} else {
				quantity = parseFloat(quantity);
			}
			if (isNaN(parseFloat(rate))) {
				rate = 0;
			} else {
				rate = parseFloat(rate);
			}

			$abc = $(this).closest('tr').find('.item_description').attr('id');
			var des_id = $(this).closest('tr').find('.item_description').attr('id');
			var unit_name = $.trim(unit);
			var unit_id = $('#item_unit_id' + sl_no).val();
			<?php $abc; ?>
			$('#additional_item').val(des_id).trigger('change');
			if (des_id != '') {

				$.ajax({
					url: '<?php echo Yii::app()->createAbsoluteUrl('wh/warehousereceipt/getUnits'); ?>',
					type: 'GET',
					dataType: 'json',
					data: { data: des_id },
					success: function (result) {

						if (result.status == 1) {
							$("#additional_item_base_unit").val(unit);
							$("#item_unit_additional_span").html(unit);
							$("#item_unit_additional_purchase").val(unit);
							$("#item_unit_additional").val(result.base_unit);
							$('select[id="item_unit_additional_purchase"]').empty();

							if (result.unit == result.base_unit) {

								$('select[id="item_unit_additional_purchase"]').append('<option value="' + result.unit + '" selected>' + result.units + '</option>');
							} else {

								$.each(result["unit"], function (key, value) {

									if (unit == value.value) {
										var selected = 'selected';
									} else {
										var selected = '';
									}
									$('select[id="item_unit_additional_purchase"]').append('<option value="' + value.id + '" ' + selected + '>' + value.value + '</option>');
								});

							}
						}
						var purchase_unit = $("#item_unit_additional_purchase option:selected").text();

						if (result.base_unit == purchase_unit) {
							$('.base-data').hide();
						} else {
							$('.base-data').show();
						}
						//$('#quantity').focus();
					}
				});
			}
			$('#bibatch').val(batch);
			$('#item_unit_additional_purchase').val(unit);
			$('.additional_rate').val(rate);
			$('#item_unit_additional_span').html(unit);
			$('#amount').val(amount);
			//$("#jono_additional").val(jono);
			$('#quantity').val(quantity);
			$('#basequantity').val(basequantity);
			$('#baserate').val(baserate);
			$('.js-example-basic-single').select2('focus');
			$('#item_unit').val(unit_id);
			$('#item_unit_id_hidden').val(unit_id);
			$(".item_save").attr('value', 'Update');
			$(".item_save").attr('id', item_id);
			$('#bibatch').attr('readonly', true);

		});

		$('.item_save').keypress(function (e) {
			if (e.keyCode == 13) {
				$('.item_save').click();
			}
		});


		function filterDigits(eventInstance) {
			eventInstance = eventInstance || window.event;
			key = eventInstance.keyCode || eventInstance.which;
			if ((47 < key) && (key < 58) || key == 8) {
				return true;
			} else {
				if (eventInstance.preventDefault)
					eventInstance.preventDefault();
				eventInstance.returnValue = false;
				return false;
			} //if
		}


		// approve items

		$(document).on('click', '.approve_item', function (e) {
			e.preventDefault();
			var element = $(this);
			var item_id = $(this).attr('id');
			$.ajax({
				url: '<?php echo Yii::app()->createAbsoluteUrl('purchase/permissionapprove'); ?>',
				type: 'POST',
				dataType: 'json',
				data: { item_id: item_id },
				success: function (response) {
					if (response.response == 'success') {
						$(".approveoption_" + item_id).hide();
						element.closest('tr').find('.rate_highlight').removeClass('rate_highlight');
						$().toastmessage('showSuccessToast', "" + response.msg + "");
					} else if (response.response == 'warning') {
						$(".approveoption_" + item_id).hide();
						element.closest('tr').find('.rate_highlight').removeClass('rate_highlight');
						$().toastmessage('showWarningToast', "" + response.msg + "");
					} else {
						$().toastmessage('showErrorToast', "" + response.msg + "");
					}
				}
			});
		});

		$(document).on('mouseover', '.rate_highlight', function (e) {
			var item_id = $(this).attr('id');
			$.ajax({
				url: '<?php echo Yii::app()->createAbsoluteUrl('purchase/previousratedetails'); ?>',
				type: 'GET',
				dataType: 'json',
				data: { item_id: item_id },
				success: function (result) {
					if (result.status == 1) {
						$('#previous_details').html(result.html);
						$("#previous_details").show();
						$("#pre_fixtable2").tableHeadFixer();
					} else {
						$('#previous_details').html(result.html);
						$("#previous_details").hide();
					}
				}
			})
		})
		$(document).on('mouseout', '.rate_highlight', function (e) {
			$("#previous_details").hide();
		});

		$("#purchaseno").keyup(function () {
			if (this.value.match(/[^a-zA-Z0-9.:]/g)) {
				this.value = this.value.replace(/[^a-zA-Z0-9.:\-/]/g, '');
			}
		});

		$(document).on('click', '.getprevious', function () {
			var id = $(this).attr('data-id');
			var res = id.split(",");
			var amount = parseFloat(res[4]);
			$('#description').val(res[0]).trigger('change.select2');
			$('#quantity').val(res[1]);
			$('#item_unit').html(res[2]);
			$('#rate').val(res[3]);
			$('#item_amount').text(amount.toFixed(2));
			var total = (res[1] * res[3]);
			if (isNaN(total)) total = 0;
			$('#previousvalue').text(total.toFixed(2));
		})


		$(document).on('click', '.removebtn', function (e) {
			e.preventDefault();
			element = $(this);
			var item_id = $(this).attr('id');
			var answer = confirm("Are you sure you want to delete?");
			if (answer) {

				var receipt_id = $("#warehousereceipt_id").val();
				var data = { 'receipt_id': receipt_id, 'item_id': item_id };
				$.ajax({
					method: "GET",
					async: false,
					data: { data: data },
					dataType: "json",
					url: '<?php echo Yii::app()->createUrl('wh/warehousereceipt/removeitem'); ?>',
					success: function (result) {
						if (result.response == 'success') {
							$('.addrow').html(result.html);
							$().toastmessage('showSuccessToast', "" + result.msg + "");
						} else {
							$().toastmessage('showErrorToast', "" + result.msg + "");
						}
						$('#additional_item').val('');
						$('#bibatch').val('');
						$('#item_unit_additional').val('');
						$('.additional_rate').val('0');
						$('#amount').val('0');
						$("#jono_additional").val('');
						$('#quantity').val('0');
						var quantity = $('#quantity').val('0');
						var accepted_quantity = $('#accepted_quantity').val('');
						var rejected_quantity = $('#rejected_quantity').val('');
						var unit = $('#item_unit').val('');
						$('select[id="item_unit"]').empty();
						$('select[id="item_unit"]').append('<option value="" selected>Unit</option>');
						$('select[id="batch"]').empty();
						$('select[id="batch"]').append('<option value="" selected>Please choose Batch</option>');
						$(".item_save").attr('value', 'Save');
						$(".item_save").attr('id', 0);
						$('#additional_item').select2('focus');
						$('#jono').val('');
					}
				});

			}
			else {

				return false;
			}
		});
		$(document).on("blur", ".accepted_quantity", function () {
			var accepted_quantity = $(this).val();
			var attr = $(this).attr('id');
			var attr_id_arr = attr.split("_");
			var attr_id = attr_id_arr['2'];
			var billedquantity = $("#billedquantity_" + attr_id).val();
			var rejectedquantity = $("#rejected_quantity_" + attr_id).val();
			var quantity = $("#receivedquantity_" + attr_id).val();
			var check_Quantity = parseFloat(quantity) - parseFloat(accepted_quantity);
			if (accepted_quantity != "") {
				if (check_Quantity >= 0) {
					rejectedquantity = check_Quantity;
					$("#rejected_quantity_" + attr_id).val(rejectedquantity);
					saveitems(attr);
					// $("#warehousereceipt_form").submit();
				} else {
					$("#accepted_quantity_" + attr_id).val(quantity);
					$("#rejected_quantity_" + attr_id).val(0);
					$().toastmessage('showErrorToast', "Accepted Quantity must be less than the Received Quantity");
				}
			} else {
				$("#accepted_quantity_" + attr_id).val(quantity);
				$("#rejected_quantity_" + attr_id).val(0);
			}


		});
		$(document).on("change", ".receivedquantity", function () {
			var quantity = $(this).val();
			var attr = $(this).attr('id');
			var attr_id_arr = attr.split("_");
			var attr_id = attr_id_arr['1'];
			var billedquantity = $("#billedquantity_" + attr_id).val();
			var accepted_quantity = $("#accepted_quantity_" + attr_id).val();
			var rejectedquantity = $("#rejected_quantity_" + attr_id).val();
			var check_Quantity = parseFloat(billedquantity) - parseFloat(quantity);
			if (quantity != "") {
				if (check_Quantity >= 0) {
					$('#buttonsubmit').prop('disabled', false);
					$("#accepted_quantity_" + attr_id).val(quantity);
					accepted_quantity = $("#accepted_quantity_" + attr_id).val();
					rejected_quantity = quantity - accepted_quantity;
					$("#rejected_quantity_" + attr_id).val(rejected_quantity);
					saveitems(attr);
					// $("#warehousereceipt_form").submit();
				} else {
					$("#accepted_quantity_" + attr_id).val(0);
					$("#receivedquantity_" + attr_id).val(0);
					$("#rejected_quantity_" + attr_id).val(0);
					$().toastmessage('showErrorToast', "Received Quantity Entered is more than the available quantity");
				}
			} else {
				$("#accepted_quantity_" + attr_id).val(0);
				$("#receivedquantity_" + attr_id).val(0);
				$("#rejected_quantity_" + attr_id).val(0);
			}

		});
		$(document).on("blur", ".rejected_quantity", function () {
			var rejectedquantity = $(this).val();
			var attr = $(this).attr('id');
			var attr_id_arr = attr.split("_");
			var attr_id = attr_id_arr['2'];

			var billedquantity = $("#billedquantity_" + attr_id).val();
			var accepted_quantity = $("#accepted_quantity_" + attr_id).val();
			var rejectedquantity = $("#rejected_quantity_" + attr_id).val();
			var quantity = $("#receivedquantity_" + attr_id).val();
			var check_Quantity = parseFloat(quantity) - parseFloat(rejectedquantity);
			if (rejectedquantity != "") {
				if (check_Quantity >= 0) {
					accepted_quantity = check_Quantity;
					$("#accepted_quantity_" + attr_id).val(accepted_quantity);
					saveitems(attr);
					// $("#warehousereceipt_form").submit();
				} else {
					$("#accepted_quantity_" + attr_id).val(quantity);
					$("#rejected_quantity_" + attr_id).val(0);
					$().toastmessage('showErrorToast', "Rejected Quantity must be less than the Received Quantity");
				}
			} else {
				$("#accepted_quantity_" + attr_id).val(quantity);
				$("#rejected_quantity_" + attr_id).val(0);
			}

		});

		$(document).on("blur", ".remark_data", function () {
			var attr = $(this).attr('id');
			saveitems(attr);
		});



	</script>

	<style>
		/* .table-scroll {
			position: relative;
			max-width: 1280px;
			width: 100%;
			margin: auto;
			display: table;
		}

		.table-wrap {
			width: 100%;
			display: block;
			overflow: auto;
			position: relative;
			z-index: 1;
			border: 1px solid #ddd;
		}

		.table-wrap.fixedON,
		.table-wrap.fixedON table,
		.faux-table table {
			height: 380px;
		}

		.table-scroll table {
			width: 100%;
			margin: auto;
			border-collapse: separate;
			border-spacing: 0;
			border: 1px solid #ddd;
		}

		.table-scroll th,
		.table-scroll td {
			padding: 5px 10px;
			border: 1px solid #ddd;
			background: #fff;
			vertical-align: top;
		}

		.faux-table table {
			position: absolute;
			top: 0;
			left: 0;
			width: 100%;
			pointer-events: none;
		}

		.faux-table table tbody {
			visibility: hidden;
		}
		.faux-table table tbody th,
		.faux-table table tbody td {
			padding-top: 0;
			padding-bottom: 0;
			border-top: none;
			border-bottom: none;
			line-height: 0.1;
		}

		.faux-table table tbody tr+tr th,
		.faux-table tbody tr+tr td {
			line-height: 0;
		}

		.faux-table thead th,
		.faux-table tfoot th,
		.faux-table tfoot td,
		.table-wrap thead th,
		.table-wrap tfoot th,
		.table-wrap tfoot td {
			background: #eee;
		}

		.faux-table {
			position: absolute;
			top: 0;
			right: 0;
			left: 0;
			bottom: 0;
			overflow-y: scroll;
		}

		.faux-table thead,
		.faux-table tfoot,
		.faux-table thead th,
		.faux-table tfoot th,
		.faux-table tfoot td {
			position: relative;
			z-index: 2;
		}

		.table-scroll table thead tr,
		.table-scroll table thead tr th,
		.table-scroll table tfoot tr,
		.table-scroll table tfoot tr th,
		.table-scroll table tfoot tr td {
			height: 1px;
		} */
	</style>
	<script>
		$(".popover-test").popover({
			html: true,
			content: function () {
				//return $('#popover-content').html();
				return $(this).next('.popover-content').html();
			}
		});
		$('[data-toggle=popover]').on('click', function (e) {
			$('[data-toggle=popover]').not(this).popover('hide');
		});
		$('body').on('hidden.bs.popover', function (e) {
			$(e.target).data("bs.popover").inState.click = false;
		});
		$('body').on('click', function (e) {
			$('[data-toggle=popover]').each(function () {
				// hide any open popovers when the anywhere else in the body is clicked
				if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
					$(this).popover('hide');
				}
			});
		});

		$(document).ajaxComplete(function () {
			$(".popover-test").popover({
				html: true,
				content: function () {
					return $(this).next('.popover-content').html();
				}
			});
		});
	</script>