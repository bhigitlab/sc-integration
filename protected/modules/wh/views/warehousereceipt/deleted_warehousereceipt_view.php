<?php
$tblpx= Yii::app()->db->tablePrefix;
$company = Yii::app()->user->company_id;
$companyInfo = Company::model()->findByPK($company);
$companyName = $companyInfo["name"];
$companyGst = $companyInfo["company_gstnum"];
$item_model_json = $model['warehouse_transfer_items'];
$item_model = json_decode($item_model_json, true);
$bill_num = '';
$despatch_num = '';
if($model['warehousereceipt_transfer_type'] == 1){
	$transfer_type = "Purchase";
	$bill_id_details = $model->warehousereceiptBillIdDetails($model['warehousereceipt_bill_id']);
	$bill_check = Controller::billWithOrWithoutPO($bill_id_details['purchase_id']);
	if($bill_check == 'po'){
		$bill_num =!empty($bill_id_details['bill_number'])?CHtml::link($bill_id_details['bill_number'], 'index.php?r=bills/view&id='.$model['warehousereceipt_bill_id'], array('class' => 'link','target'=>'_blank')):'';
	}else{
		$bill_num = !empty($bill_id_details['bill_number'])?CHtml::link($bill_id_details['bill_number'], 'index.php?r=bills/billview&id='.$model['warehousereceipt_bill_id'], array('class' => 'link','target'=>'_blank')):'';
	}
}elseif($model['warehousereceipt_transfer_type'] == 2){
	$transfer_type = "Despatch";
	$despatch_id_details = $model->warehousereceiptDespatchDetails($model['warehousereceipt_despatch_id']);
	$despatch_num = !empty($despatch_id_details['warehousedespatch_no'])?CHtml::link($despatch_id_details['warehousedespatch_no'], 'index.php?r=wh/warehousedespatch/view&despatchid='.$model['warehousereceipt_despatch_id'], array('class' => 'link','target'=>'_blank')):'';
}else{
	$transfer_type = "-";
	$bill_or_despatch_num = "-";
}


?>
<!DOCTYPE html>
<html>
	<head>
		<title>Receipt Pdf</title>
		<link href="https://fonts.googleapis.com/css?family=Diplomata+SC|Limelight" rel="stylesheet">
		<style>
			table{width: 100%;border-collapse:collapse;
				/*font-size: 12px;*/
				}
			table td, table th{padding: 6px 0px;}
			.item_table td, .item_table th{border: 1px solid #ccc;padding: 8px;text-align:center}
			.item_table tr:last-child td{border: 1px solid transparent;}
                        table tbody tr:last-child {
                            border-bottom: 0px;
                         }
                         table tbody tr td:nth-child(2) {
                            border-left: 0px;
                         }
                         h2{
                             margin: 0px;font-family: 'Limelight', cursive;padding:0px;
                         }
		</style>
	</head>
	<body>
		<div class="container">
			<table>
				<tr>
					<td>GSTIN:<b> <?php echo isset($companyGst)?$companyGst:""; ?></b></td>
					<td style="text-align:right">Phone:<b> <?php echo isset($companyInfo['phone'])?$companyInfo['phone']:""; ?></b></td>
				</tr>
			</table>
			<div style="text-transform:uppercase;text-align:center;">
				<h2><?php echo $companyInfo['name'] ?></h2>
				<h4 style="margin:0px;"><?php //echo $companyInfo['address'] ?> <?php echo $warehouse_address;?></h4>
			</div>
			<table>
				<tr>
					<td>RECEIPT NOTE NO.<b style="color: red;font-size: 18px;"> <?php echo $model->warehouse_transfer_type_deleted_no; ?> (Deleted)</b></td>
					<td style="text-align:right">Date:<b> <?php echo date("d-m-Y", strtotime($model->warehouse_transfer_date)); ?></b></td>
				</tr>
		<tr><td colspan="2">Stock of Goods Received From: <b>
		<?php echo ($model->warehouseidFrom['warehouse_name'] !="")?$model->warehouseidFrom['warehouse_name'].'('.$despatch_num.')': $bill_num;
		 
		
		?></b></td>
		
		</tr>
			</table>
			<table class="item_table">
				<tr>
					<th style="width: 40px;">Sl No</th>
					<th>Item</th>
					<th style="width: 100px;">Batch</th>
					<th style="width: 100px;">Unit/Size</th>
					<th style="width: 100px;">Quantity</th>
					<th style="width: 100px;">Rate</th>
					<th style="width: 100px;">Amount</th>
					<th style="width: 100px;">Accepted Quantity</th>
					<th style="width: 100px;">Rejected Quantity</th>
				
					<!-- <th style="width: 150px;">J.O NO</th> -->
				</tr>
                                <?php
                                if(!empty($item_model)){
                                    foreach($item_model as $key=> $values){
                                        $specification = Yii::app()->db->createCommand("SELECT id, parent_id,brand_id, specification, unit FROM {$tblpx}purchase_category WHERE id=" . $values['warehousereceipt_itemid'] . "")->queryRow();
                                        $parent_category = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}purchase_category WHERE id='" . $specification['parent_id'] . "'")->queryRow();

                                        if ($specification['brand_id'] != NULL) {
                                            $brand_details = Yii::app()->db->createCommand("SELECT brand_name FROM {$tblpx}brand WHERE id=" . $specification['brand_id'] . "")->queryRow();
                                            $brand = '-' . $brand_details['brand_name'];
                                        } else {
                                            $brand = '';
                                        }

                                        $spc_details = $parent_category['category_name'] . $brand . ' - ' . $specification['specification'];
								  $amount = $values['warehousereceipt_quantity']*$values['warehousereceipt_rate'];
								?>
				<tr>
					<td><?php echo ($key+1); ?></td>
					<td style="text-align:justify;"><?php echo $spc_details; ?></td>
					<td><?php echo $values['warehousereceipt_batch']; ?></td>
                    <td><?php echo $values['warehousereceipt_unit']; ?></td>
					<td><?php echo $values['warehousereceipt_quantity']; ?></td>
					<td><?php echo Controller::money_format_inr($values['warehousereceipt_rate'], 2, 1); ?></td>
					<td><?php echo Controller::money_format_inr($amount, 2, 1); ?></td>
					<td><?php echo $values['warehousereceipt_accepted_quantity']; ?></td>
					<td><?php echo $values['warehousereceipt_rejected_quantity']; ?></td>
					
					
				</tr>
                                <?php
                                    }
                                ?>

                                <?php } else{
                                   ?>
                                <tr>
                                    <td colspan="10">No Item found</td>
                                </tr>
                                <?php
                                } ?>
                                <tr>
                                    <td colspan="2" style="padding-left:0px;text-align:left;" class="no-border">Receipt Clerk : <b><?php echo $model->clerk->first_name.' '.$model->clerk->last_name ?></b></td> 
									
                                    <td colspan="3" style="text-align:right;padding-right:0px;" class="no-border">For <b style="text-transform:uppercase;"><?php echo $companyName; ?></b></td>																
				</tr>
				
				
			</table>
<div>
<b>Remarks:</b><?php echo $model['warehouse_transfer_remark']; ?>
</div>
		</div>
	</body>
</html>
