<style>
    .legend {
        list-style: none;
        padding-left: 0px;

    }

    .legend li {
        float: left;
        margin-right: 10px;
    }

    .legend span {
        border: 1px solid #ccc;
        float: left;
        width: 12px;
        height: 12px;
        margin: 2px;
    }

    .legend .project_not_mapped {
        background-color: #A9E9EC;
        border: 2px solid #f8cbcb;
        margin-right: 4px;
    }
</style>
<div id="msg_box"></div>
<?php

?>

<?php
if ($index == 0) {
    ?>
    <?php 
    $pms_api_integration=ApiSettings::model()->pmsIntegrationStatus();
    if($pms_api_integration=='1'){
    ?>
    <ul class="legend">
            <li><span class="project_not_mapped"></span>Unit Not Mapped In Integration</li>    
    </ul>
    <?php } ?>
    <thead>
        <tr>
            <th>Action</th>
            <th>No</th>
            <th>Unit</th>
            <th>Created Date</th>
        </tr>   
    </thead>
<?php } ?>

<?php
 if($data->pms_unit_id !=='0'){
        $styleval= '';
    }else{
        $styleval='background-color:#A9E9EC;';
    }             
?>
    <tr style="<?php echo $styleval; ?>">
    <td>
            <div class="edit_purchase edit_option_<?php echo $data->id; ?>" id="<?php echo $data->id; ?>" data-toggle="tooltip" title="Edit"><span class="fa fa-edit editUnit" data-toggle="modal" data-target=".edit" data-id="<?php echo $data->id; ?>" data-pms-unit-id="<?php echo $data->pms_unit_id; ?>"></span></div>
        </td>
        <td><?php echo $index+1; ?></td>
        <td><?php echo $data->unit_name; ?></td>
        <td><?php echo date('d-m-Y', strtotime($data->created_date));  ?></td>
        



</tr>  




