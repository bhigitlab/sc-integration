INSERT INTO `jp_menu` (`menu_id`, `menu_name`, `parent_id`, `status`, `controller`, `action`, `params`, `showmenu`, `show_list`, `related`) VALUES ('791', 'Create Bill Itemwise', '594', '0', 'Subcontractorbill', 'createwithItem', '', '2', '1', NULL);

CREATE TABLE `jp_scbillitems` (
  `id` int(11) NOT NULL,
  `bill_id` int(11) NOT NULL,
  `scquotation_id` int(11) DEFAULT NULL,
  `item_category_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `item_type` int(11) NOT NULL COMMENT '1=>quantity*rate,2=>lumpsum',
  `item_description` text NOT NULL,
  `item_quantity` int(11) NOT NULL,
  `item_rate` float NOT NULL,
  `executed_quantity` int(11) NOT NULL,
  `item_amount` float NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` date NOT NULL,
  `completed_percentage` float NOT NULL,
  `bill_number` varchar(100) NOT NULL,
  `updated_date` date DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
);
ALTER TABLE `jp_scbillitems` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT, add PRIMARY KEY (`id`);
ALTER TABLE `jp_scbillitems` CHANGE `item_quantity` `item_quantity` INT(11) NULL;
ALTER TABLE `jp_scbillitems` CHANGE `item_rate` `item_rate` FLOAT NULL;
ALTER TABLE `jp_scbillitems` CHANGE `executed_quantity` `executed_quantity` INT(11) NULL;
ALTER TABLE `jp_subcontractorbill` ADD `bill_type` INT NOT NULL DEFAULT '1' COMMENT '1=>stage_wise,2=>item_wise ' AFTER `status`;
ALTER TABLE `jp_scbillitems` ADD CONSTRAINT `fk_sc_item_id` FOREIGN KEY (`item_id`) REFERENCES `jp_scquotation_items`(`item_id`) ON DELETE RESTRICT ON UPDATE CASCADE;