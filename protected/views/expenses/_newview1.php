<?php
/* @var $this ProjectsController */
/* @var $data Projects */
?>

<?php
$tblpx = Yii::app()->db->tablePrefix;
$id = $data['exp_id'];
?>
<?php
$id = $data['exp_id'];
$type = $data['type'];
$tr_type = "";

if ($type == 72) {
    $ptype = $data['payment_type'];
    $sql = Status::model()->find(array(
        'select' => array('caption'),
        "condition" => "sid='$ptype'",
    ));
    $tr_type = $sql['caption'];
} else {
    $ptype = $data['exp_type'];
    $sql = Status::model()->find(array(
        'select' => array('caption'),
        "condition" => "sid='$ptype'",
    ));
    $tr_type = $sql['caption'];
}
?>

<?php
/* @var $this ActivityController */
/* @var $data Activity */
if ($index == 0) {

?>

    <thead class="entry-table">
        <tr>
            <th style="display:none;"></th>
            <th>Sl No.</th>
            <th>#ID</th>
            <th>Date</th>
            <th>Project Name</th>
            <th>Company</th>
            <th>Vendor</th>
            <th>Bill No/ Return No</th>
            <th>Subcontractor</th>
            <th>Description</th>
            <th>Transaction Type</th>
            <th>Purchase Type</th>
            <th>Credit Amount</th>
            <th>Debit Amount</th>
            <!-- <th>Paid Amount</th> -->
            <?php if (isset($_GET['secure'])) {
                if ($_GET['secure'] == 1) {
            ?>
                    <th colspan="2">Action</th>
            <?php }
            } ?>


        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="text-right" colspan="11"><b>Grand Total : </b></td>
            <td class="text-right">
                <b><?php echo Controller::money_format_inr(Yii::app()->user->grandtotalcredit[0]['sumreceived'], 2, 1); ?></b>
            </td>
            <!-- <td class="text-right">
                <b><?php echo Controller::money_format_inr(Yii::app()->user->grandtotaldebit[0]['sumamt'], 2, 1); ?></b>
            </td> -->
            <td class="text-right">
                <b><?php echo Controller::money_format_inr(Yii::app()->user->grandtotaldebit[0]['sumpaid'], 2, 1); ?></b>
            </td>
            <?php if (Yii::app()->user->role == 1) { ?>
                <?php if (isset($_GET['secure'])) {
                    if ($_GET['secure'] == 1) {
                ?>
                        <td colspan="2"></td>
                <?php }
                } ?>
            <?php } ?>
        </tr>
    <?php
}
    ?>
    <tr>

        <td style="display:none;"></td>
        <td class="text-right"><?php echo $index + 1; ?></td>
        <td><b>#<?php echo $data['exp_id'] ?></b></td>
        <td style="width:80px;">
            <?php
            if ($data['date'] != "") {
                echo date("d-M-Y", strtotime($data['date']));
            }
            ?>
        </td>
        <td><?php echo $data['project_name']; ?></td>
        <td>
            <?php
            $company = Company::model()->findByPk($data['companyid']);
            echo $company->name;
            ?>
        </td>
        <td>
            <?php
            if ($data['vendor_name']!== '') {
                echo $data['vendor_name'];
            } else {
                echo "---";
            };
            ?>
        </td>
        <td class="">
            <?php
            $tblpx = Yii::app()->db->tablePrefix;
            if ($data['bill_id'] != "") {
                $billno = Yii::app()->db->createCommand("SELECT bill_number, bill_id from {$tblpx}bills where bill_id='" . $data['bill_id'] . "'")->queryAll();
                foreach ($billno as $key => $value) {
            ?>Bill No -
            <a href="#" id="<?php echo $value['bill_id']; ?>" class="link bill_view" title="View"><?php echo $value['bill_number'] ?></a>

        <?php
                }
            } else if ($data['return_id'] != "") {
                $returnno = Yii::app()->db->createCommand("SELECT return_number ,return_id from {$tblpx}purchase_return where return_id='" . $data['return_id'] . "'")->queryAll();
                foreach ($returnno as $key => $value) { ?>
            Return No -
            <a href="#" id="<?php echo $value['return_id']; ?>" class="link return_view" title="View"><?php echo $value['return_number'] ?></a>

    <?php
                }
            }
    ?>
        </td>
        <td>
            <?php
            if (!empty($data['subcontractor_id'])) {
                $scpayment = SubcontractorPayment::model()->findByPk($data['subcontractor_id']);
                if ($scpayment)
                    echo $scpayment->subcontractor->subcontractor_name;
            }
            ?>
        </td>
        <td><?php echo $data['des']; ?></td>
        <td><?php echo $tr_type; ?></td>
        <td class="text-right">
            <?php if ($data['type'] == 73) { ?>
            <?php
                if ($data['purchase_type'] == 2) {
                    echo 'Full Paid';
                } elseif ($data['purchase_type'] == 1) {
                    echo 'Credit';
                } elseif ($data['purchase_type'] == 3) {
                    echo 'Partially Paid';
                } else {
                    echo '';
                }
            } else {
                if ($data['purchase_type'] == 2) {
                    echo 'Full Paid';
                } elseif ($data['purchase_type'] == 1) {
                    echo 'Credit';
                } elseif ($data['purchase_type'] == 3) {
                    echo 'Partially Paid';
                } else {
                    echo '';
                }
            } ?>

        </td>
        <td class="text-right">
        <?php if ($data['exp_type_id'] == 72) { 
            if ($data['purchase_type'] == 1) {?>
                
                <?php echo isset($data['amount']) ? Controller::money_format_inr($data['amount'], 2, 1) : '0'; ?>
           

           <?php }else{?>
           
                <?php echo isset($data['receipt']) ? Controller::money_format_inr($data['receipt'], 2, 1) : '0'; ?>
           

          <?php   }
            ?>
            
        <?php
        } 
        ?>
        </td>
        <td class="text-right">
        <?php if ($data['exp_type_id'] == 73) {  
            if ($data['purchase_type'] == 1) {?>
               
                <?php echo isset($data['amount']) ? Controller::money_format_inr($data['amount'], 2, 1) : '0'; ?>
             

           <?php }else{?>
           
                <?php echo isset($data['paid']) ? Controller::money_format_inr($data['paid'], 2, 1) : '0'; ?>
           
        <?php
        } }
        ?>
        <!-- <td class="text-right">
            <?php
            if ($data['type'] == 73) {
                echo isset($data['paid']) ? Controller::money_format_inr($data['paid'], 2) : 0;
            }
            ?>
        </td> -->
        <?php if (isset($_GET['secure'])) {
            if ($_GET['secure'] == 1) {
        ?>
                <td>
                    <span class="fa fa-remove deletedaybook pull-right" title="Delete" data-id="<?php echo $data['exp_id']; ?>"></span>
                </td>
        <?php }
        } ?>
    </tr>