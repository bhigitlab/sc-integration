// COMS LAYOUT
document.addEventListener("DOMContentLoaded", () => {
    const toggleElement = document.querySelector(".coms-block");
  
    if (toggleElement) {
      toggleElement.addEventListener("click", function () {
        const content = this.querySelector(".coms-content");
        const arrow = this.querySelector(".coms-arrow");
  
        if (content.classList.contains("active")) {
          content.style.opacity = 0;
          content.style.transform = "translateY(-10px)";
          setTimeout(() => {
            content.classList.remove("active");
            content.style.display = "none";
          }, 300);
          arrow.style.transform = "rotate(0deg)";
        } else {
          content.style.display = "block";
  
          setTimeout(() => {
            content.classList.add("active");
            content.style.opacity = 1;
            content.style.transform = "translateY(0)";
          }, 10);
          arrow.style.transform = "rotate(180deg)";
        }
      });
    }
  });
  
  // SIDEBAR
  const sidebar = document.getElementById("mainSidebar");
  const main = document.getElementById("dashboard-content");
  const sidebarToggle = document.getElementById("sidebarToggle");
  
  sidebarToggle.addEventListener("click", () => {
    const isCollapsed = sidebar.classList.toggle("collapsed");
    main.classList.toggle("main-sidebar-collapsed", isCollapsed);
    sidebarToggle.classList.toggle("collapsed", isCollapsed);
  });
  function getRouteFromUrl() {
    const params = new URLSearchParams(window.location.search); 
    return params.get('r');
  }
  document.addEventListener("DOMContentLoaded", () => {
    const currentRoute = getRouteFromUrl(); 
  
    if (currentRoute !== 'dashboard/index' && currentRoute !== 'dashboard/notification' && currentRoute !== 'dashboard/cashbalance' && currentRoute !== 'dashboard/financialreport' ) {
      sidebarToggle.click();
  }

  });
  
  
  // DROPDOWN
  
  const dropdownContainers = document.querySelectorAll(".dashboard-dropdown-container");
  
  dropdownContainers.forEach((container) => {
    const selectElement = container.querySelector("select");
  
    selectElement.addEventListener("click", () => {
      container.classList.toggle("open");
    });
  
    selectElement.addEventListener("blur", () => {
      container.classList.remove("open");
    });
  });
  
  // Filter Window
  document.addEventListener("click", (event) => {
    document.querySelectorAll(".dashboard-filter-dropdown").forEach((dropdown) => {
      const button = dropdown.previousElementSibling;
  
      if (!dropdown.contains(event.target) && !button.contains(event.target)) {
        dropdown.classList.remove("visible");
        dropdown.classList.add("hidden");
      }
    });
  });
  
  document.querySelectorAll(".dashboard-filter-button").forEach((button) => {
    button.addEventListener("click", (event) => {
      event.stopPropagation();
      const buttonContainer = event.target.closest(".dashboard-filter-header");
      const dropdown = buttonContainer.querySelector(".dashboard-filter-dropdown");
  
      document.querySelectorAll(".dashboard-filter-dropdown").forEach((d) => {
        if (d !== dropdown) {
          d.classList.remove("visible");
          d.classList.add("hidden");
        }
      });
  
      if (dropdown.classList.contains("hidden")) {
        dropdown.classList.remove("hidden");
        dropdown.classList.add("visible");
      } else {
        dropdown.classList.remove("visible");
        dropdown.classList.add("hidden");
      }
    });
  });
  
  // Hide footer for mobile screen
  const pageWrapper = document.getElementById("page-wrapper");
  const footer = document.getElementById("footer");
  
  pageWrapper.addEventListener("scroll", () => {
    const scrolledToBottom = pageWrapper.scrollTop + pageWrapper.clientHeight >= pageWrapper.scrollHeight;
  
    if (scrolledToBottom) {
      footer.classList.add("visible");
    } else {
      footer.classList.remove("visible");
    }
  });
  