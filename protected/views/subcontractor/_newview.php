<?php
/* @var $this SubcontractorController */
/* @var $data Subcontractor */
?>

<?php
$tblpx = Yii::app()->db->tablePrefix;
$id    = $data->scquotation_id;
?>



<?php
if ($index == 0) {
    //echo "<pre>"; print_r($data);//exit;
?>

    <thead class="entry-table">
        <tr>
            <?php if ((isset(Yii::app()->user->role) && (in_array('/subcontractor/editquotation', 
        Yii::app()->user->menuauthlist) || in_array('/subcontractor/viewQuotation', Yii::app()->user->menuauthlist)))) { ?>
                <th style="width:60px;">Action</th>
            <?php } ?>
            <th style="width:20px;">Sl No.</th>
            <th>Company</th>
            <th>Sub Contractor</th>
            <th>Project</th>
            <th>Date</th>
            <th>Amount</th>
            <th>Tax Amount</th>
            <th>SC Quotation No</th>
            <th style="width:200px;">Description</th>
            <th>Write Off</th>
            <th>Balance</th>
            <th>Status</th>            
        </tr>
    </thead>
    <tfoot>
        <tr>
            <?php if ((isset(Yii::app()->user->role) && (in_array('/subcontractor/editquotation', Yii::app()->user->menuauthlist)))) { ?>
                <th></th>                
            <?php } ?>
            <th colspan="5" style="text-align:right;"><b>Total</b></th>
            <th class="text-right"><?php echo Controller::money_format_inr($amountsum, 2); ?></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
        </tr>
    </tfoot>
<?php
}
?>
<?php
$items = ScquotationItems::model()->findAll(array('condition' => 'scquotation_id=' . 755 . ''));
//echo "<pre>";print_r($items);exit;
$permission = array();
foreach ($items as $key => $value) {
    array_push($permission, $value['approve_status']);
}
$class = "";
if (!empty($items)) {
    if (in_array("No", $permission)) {
        $class = 'permission_style';
    } else {
        $class = "";
    }
} else {
    if(!empty($data->approve_status)){
        if($data->approve_status == "Yes"){
            $class = "";
        }else{
             $class = 'permission_style';
        }
    }
    
}
$newclass='';
if($data->labour_template_rate_change=='1'){
    $newclass='row_class';
}

?>
<tr class="<?php echo $class.' '.$newclass; ?>">
    <td>
        <span class="icon icon-options-vertical popover-test" data-toggle="popover" data-placement="right" type="button" data-html="true" style="cursor: pointer;"></span>
        <div class="popover-content hide">
            <ul class="tooltip-hiden">
                <?php if ((isset(Yii::app()->user->role) && (in_array('/subcontractor/editquotation', Yii::app()->user->menuauthlist)))) {
                ?>
                    <li><a class="click btn btn-xs btn-default" id="<?php echo $data->scquotation_id; ?>" href="#" onClick="edit('<?php echo $data->scquotation_id; ?>')" title="Update">Edit</a></li>
                <?php } ?>
                <!-- quotation view -->
                <?php if ((isset(Yii::app()->user->role) && (in_array('/subcontractor/viewquotation', Yii::app()->user->menuauthlist)))) {
                ?>
                    <li><a class="click btn btn-xs btn-default" id="<?php echo $data->scquotation_id; ?>" href="#" onClick="view('<?php echo $data->scquotation_id; ?>')" title="View">View</a></li>
                <?php
                }
                ?>
                 <?php if ((isset(Yii::app()->user->role) && (in_array('/subcontractor/approvelabourrate', Yii::app()->user->menuauthlist)))) {
                    if($data->labour_template_rate_change=='1'){
                ?>
                    <li><a class="click btn btn-xs btn-default permission_item" id="<?php echo $data->scquotation_id; ?>" href="#" onClick="approvelabourrate('<?php echo $data->scquotation_id; ?>')" title="Update">Labour Rate Approve</a></li>
                <?php } }?>

                <?php if ((isset(Yii::app()->user->role) && (in_array('/subcontractor/writeoff', Yii::app()->user->menuauthlist)))) { ?>
                    <?php if ($data->approve_status == "Yes") { ?>
                        <li><a class="click btn btn-xs btn-default" id="<?php echo $data->scquotation_id; ?>" onClick="writeoff('<?php echo $data->scquotation_id; ?>')" data-toggle="modal" data-target="#myModal" title="Write Off">Write Off</a></li>
                    <?php } ?>
                <?php } 
                if ((isset(Yii::app()->user->role) && (in_array('/subcontractor/clonequotation', Yii::app()->user->menuauthlist)))) {
                    ?>
                        <li><a class="click btn btn-xs btn-default" id="<?php echo $data->scquotation_id; ?>" href="#" onClick="clone('<?php echo $data->scquotation_id; ?>')" title="Clone">Clone</a></li>
                    <?php } 
                $sc_quotation_items = ScquotationItems::model()->findAll(array(
                    'condition' => 'scquotation_id ='. $data->scquotation_id .'  AND approve_status="No"'
                ));
                
                if(count($sc_quotation_items)) {
                ?>

                <li><a class="click btn btn-xs btn-default" id="<?php echo $data->scquotation_id; ?>" href="#" onClick="approve('<?php echo $data->scquotation_id; ?>',this)" title="Approve">Approve</a></li>
                <?php } 
                $sc_items = ScquotationItems::model()->findAll(array(
                    'condition' => 'scquotation_id ='. $data->scquotation_id 
                ));
                ?>
                <li><a class="click btn btn-xs btn-default" data-item-id ="<?php echo count($sc_items); ?>" id="<?php echo $data->scquotation_id; ?>" href="#" onClick="actdelete('<?php echo $data->scquotation_id; ?>',this)" title="delete">delete</a></li>
            </ul>
        </div>
    </td>
    <td style="width:20px;"><?php echo $index + 1; ?></td>
    <td>
        <?php
        $company = Company::model()->findByPk($data->company_id);
        echo $company['name'];
        ?>
    </td>
    <td>
        <?php
        $subModel = Subcontractor::model()->findByPk($data->subcontractor_id);
        echo CHtml::encode($subModel->subcontractor_name);
        ?>
    </td>
    <td>
        <?php
        $projectModel = Projects::model()->findByPk($data->project_id);
        echo !empty($projectModel)?CHtml::encode($projectModel->name):"";
        ?>
    </td>
    <td style="white-space:nowrap;"><?php echo CHtml::encode(date("d-m-Y", strtotime($data->scquotation_date))); ?></td>
    <td class="text-right"><?php echo Controller::money_format_inr($data->getTotalQuotationAmount($data->scquotation_id), 2, 1);  ?></td>
    <td class="text-right">
        <?php 
        $sgst_amount = ($data->sgst_percent / 100) * $data->getTotalQuotationAmount($data->scquotation_id);
        $cgst_amount = ($data->cgst_percent / 100) * $data->getTotalQuotationAmount($data->scquotation_id);
        $igst_amount = ($data->igst_percent / 100) * $data->getTotalQuotationAmount($data->scquotation_id);
        
        echo Controller::money_format_inr(($sgst_amount+$cgst_amount+$igst_amount), 2, 1);  
        ?>
    </td>
    <td style="width:20px;"><?php echo $data->scquotation_no; ?></td>
    <td style="width:200px;"><?php echo CHtml::encode($data->scquotation_decription); ?></td>
    <?php
    $writeoff       = Writeoff::model()->find(array('condition' => 'subcontractor_id = ' . $data->scquotation_id));
    $writeoffAmt    = ($writeoff) ? $writeoff->amount : 0;
    $balAmount      = ($data->scquotation_amount+$sgst_amount+$cgst_amount+$igst_amount) - $writeoffAmt;
    $reconcil_status = "({$tblpx}subcontractor_payment.reconciliation_status=1 OR {$tblpx}subcontractor_payment.reconciliation_status IS NULL)";
          $quotation_amount = 0;
          $quotation_amount  = Controller::money_format_inr($data->getTotalQuotationAmount($data->scquotation_id), 2, 1);         
          $sql = "SELECT COALESCE(SUM(jsp.paidamount), 0) AS paidamount 
          FROM `{$tblpx}subcontractor_payment` jsp 
          WHERE jsp.approve_status = 'Yes' 
          AND jsp.quotation_number = " . $data['scquotation_id'] . " 
          AND (jsp.reconciliation_status = 1 OR jsp.reconciliation_status IS NULL) 
          AND jsp.project_id = " . $data['project_id'] . " 
          GROUP BY jsp.quotation_number;";
$payment = Yii::app()->db->createCommand($sql)->queryRow();
$paid_amt =0;
$balance=0;
if(!empty($payment)){
    $paid_amt = $payment['paidamount'];
}
$quotation_amount  = $data->getTotalQuotationAmount($data->scquotation_id);         
         
$balAmount = ($quotation_amount+$sgst_amount+$cgst_amount+$igst_amount) - ($paid_amt + $writeoffAmt);
//echo "<pre>";echo($balAmount);//exit;
$tolerance = 1e-4;

// Check if the balance is within the tolerance range
if (abs($balAmount) < $tolerance) {
    $balAmount = 0;
}
//echo "id-". $data->scquotation_id.'quot amount= '. $quotation_amount."sgst= ".$sgst_amount."cgst= ".$cgst_amount."igst = ".$igst_amount."paid= ".$paid_amt."write= ".$writeoffAmt.'balAmount= '.$bal.'<br>';  
    ?>
    <td class="text-right"><?php echo Controller::money_format_inr($writeoffAmt, 2, 1);  ?></td>
    <td class="text-right"><?php echo Controller::money_format_inr($balAmount, 2);  ?></td>
    <td id="quotation_status<?php echo $data->scquotation_id; ?>">
        <?php
        if($data->approve_status=="No"){
            echo "Pending";
        }
        else{
            echo "Approved";
        }
        // if ($data->scquotation_status == 0) {
        //     if (Yii::app()->user->role == 1) {
        // >
        //         <span class="quotation_status" id="<?php echo $data->scquotation_id; >" title="Click here to change the status to saved">Draft</span>
        // <?php
        //     } else {
        //         echo "<span title='Need approval from admin'>Draft</span>";
        //     }
        // } else {
        //     echo "Saved";
        // }
        ?>
    </td>    
</tr>
<style>
    .permission_style {
        background: #f8cbcb !important;
    }
   
    .row_class{
        background-color: #f8cbcb !important;
    }

</style>
