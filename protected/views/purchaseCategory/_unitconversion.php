<?php
if ($index == 0) {
?>
    <div class="filter_sec pull-right col-sm-6 col-md-4 padding-0">
        <label>Search:</label>
        <input id="unit_search" type="text" class="form-control">
    </div>
    <thead class="entry-table sticky-thead">
        <tr>
            <th id="th_item">Item</th>
            <th>Base Unit</th>
            <th>Other unit</th>
            <th>Conversion Factor</th>
            <th>Priority</th>
            <th></th>
        </tr>
    </thead>
<?php }

$item_count = $data->GetItemcount($data->item_id, $data->base_unit);
if ($item_count > 0) {

?>
    <tr>
        <td class='item_name_<?php echo $data->id; ?>' id="item_name_<?php echo $data->item_id; ?>">
            <?php echo $data->GetItemName($data->item_id); ?>
            <input type='hidden' id='purchase_item_id_<?php echo $data->id; ?>' value='<?php echo $data->item_id; ?>'>
        </td>
        <td class='base_unit_<?php echo $data->id; ?>' id="base_unit_<?php echo $data->item_id; ?>">
            <?php echo $data->base_unit; ?>
            <input type='hidden' id='base_unit_hid_<?php echo $data->id; ?>' value='<?php echo $data->base_unit; ?>'>
        </td>
        <td>
            <?php echo $data->conversion_unit; ?>
            <input type='hidden' id='conversion_unit_<?php echo $data->id; ?>' value='<?php echo $data->conversion_unit; ?>'>
        </td>
        <td id='conversion_factor_<?php echo $data->id; ?>'>
            <?php echo $data->conversion_factor; ?>
        </td>
        <td>
            <input type="checkbox" value="<?php echo $data->priority; ?>" id='prioritycheck_item_<?php echo $data->item_id . "_data_" . $data->id; ?>' class='priority_check' <?php echo ($data->priority == 1) ? 'checked' : ''; ?>>
        </td>
        <td>
            <?php
            if ($data->base_unit != $data->conversion_unit) {
            ?>
                <div class="edit_purchase edit_unitoption edit_option_<?php echo $data->id; ?>" id="edit_unit_<?php echo $data->id; ?>" data-toggle="tooltip" title="Edit"><span class="fa fa-edit editunit" data-toggle="modal" data-target=".edit" data-id="<?php echo $data->id; ?>"></span></div>
            <?php } ?>
        </td>
    </tr>

<?php } ?>