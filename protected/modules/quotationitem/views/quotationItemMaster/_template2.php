<div  id="container_additionalwork" >
    <div class="row addRow panel" style="padding:10px">

        <div class="col-md-3">
            <?php echo $form->textField($model, 'worktype_label', array('name' => 'worktype_label_additional[]', 'class' => 'form-control worktypelabel_additional common-additional', 'id' => 'worktypelabel_additional_1', 'placeholder' => 'Work Type Label')); ?>
            <?php echo $form->error($model, 'worktype_label'); ?>
        </div>
        <div class="col-md-3">
            <?php echo $form->dropDownList($model, 'id', CHtml::listData($items, 'id', 'data'), array('class' => 'form-control js-example-basic-multiple require material getmaster_rate',  'id' => 'material_1', 'name' => 'material[0][]', 'empty' => '-Select material-', 'style' => 'width:100%')); ?>
            <?php echo $form->error($model, 'data'); ?>
        </div>
        <div class="col-md-3">
            <?php echo $form->dropDownList($model, 'id', CHtml::listData(QuotationFinishMaster::model()->findAll(), 'id', 'name'), array('class' => 'form-control js-example-basic-single  boxwork field_change require finish', 'empty' => '-Select Finish-', 'id' => 'finish_1', 'name' => 'finish[]', 'style' => 'width:100%')); ?>
            <?php echo $form->error($model, 'name'); ?>
        </div>

        <div class="col-md-3">
            <?php echo $form->textArea($model, 'description', array('class' => 'form-control description-additional', 'placeholder' => 'Description', 'name' => 'additional_description[]', 'id' => 'additional_description_1')); ?>
            <?php echo $form->error($model, 'description'); ?>
        </div>
        <div class="col-md-3">
            <?php echo $form->textField($model, 'rate', array('class' => 'form-control additional_rate common-additional', 'id' => 'additional_rate_1', 'placeholder' => 'Rate', 'name' => 'additional_rate[]')); ?>
            <?php echo $form->error($model, 'rate'); ?>
        </div>
        <div class="col-md-3">
            <?php echo $form->textField($model, 'profit', array('class' => 'form-control allownumericdecimal additional_profit common-additional', 'id' => 'additional_profit_1', 'placeholder' => 'Profit %', 'name' => 'additional_profit[]')); ?>
            <?php echo $form->error($model, 'profit'); ?>
        </div>
        <div class="col-md-3">
            <?php echo $form->textField($model, 'discount', array('class' => 'form-control allownumericdecimal additional_discount common-additional', 'id' => 'additional_discount_1', 'placeholder' => 'Discount %', 'name' => 'additional_discount[]')); ?>
            <?php echo $form->error($model, 'discount'); ?>
        </div>
        <div class="col-md-3" >
            <?php echo $form->fileField($model, 'additional_image[]', array('class' => 'form-control  additional_image', 'id' => 'additional_image_1')); ?>
            <?php echo $form->error($model, 'additional_image[]'); ?>
        </div>
    </div>
</div>


<div class="btn-holder-additional">
    <p id="add_field_additionalwork" ><a><span>&raquo; Add More</span></a></p>
    <p id="add_field_additionalwork_remove" ><a><span>&raquo; Remove</span></a></p>
</div>