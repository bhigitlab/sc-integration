
<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/tablestyles.css" />
<link rel="stylesheet" type="text/css" href="uploads/new_template/css/newstyle.css" />
<style>
    table {
        border-collapse: collapse;
        width: 100%;
    }
    /*.container{max-width: 1050px;margin:0 auto;padding: 15px;border: 1px solid #ddd;}*/
    .text-right{text-align:right}
    .details,.info-table{border: 1px solid #212121;margin-top: 20px;}
    .details td, .info-table td,.info-table th{padding: 6px 8px; }
    .info-table{margin-bottom: 10px;}
    .text-center{text-align:center;}
    .img-hold{width: 10%;}
    .companyhead{font-size: 18px;font-weight:bold;margin-top:0px;color: #789AD1;}
</style>

<?php
    $company_address = Company::model()->findBypk(Yii::app()->user->company_id);
?>
    <table border=0>
        <tbody>
                <tr>
                <td class="img-hold"><img  src="<?php echo Yii::app()->theme->baseUrl; ?>/images/logo.png" alt="img" style='margin-top:-45px;'/></td>
                    <td class=text-right>GST NO: <?php echo isset($company_address["company_gstnum"])?$company_address["company_gstnum"]:''; ?></td>
                </tr>
        </tbody>
    </table>
    <table border=0>
        <tbody>
            <tr>
               
                <td class="text-center"><p class='companyhead'><?php echo isset($company_address["name"])?$company_address["name"]:''; ?></p><br>
                    <p><?php echo ( (isset($company_address['address']) && !empty($company_address['address'])) ? ($company_address['address']) : ""); ?><br>
                    PIN : <?php echo ( (isset($company_address['pincode']) && !empty($company_address['pincode'])) ?($company_address['pincode']) : ""); ?>,&nbsp;
                    PHONE : <?php echo ( (isset($company_address['phone']) && !empty($company_address['phone'])) ?( $company_address['phone']) : ""); ?><br>
                    EMAIL : <?php echo ( (isset($company_address['email_id']) && !empty($company_address['email_id'])) ?( $company_address['email_id']) : ""); ?></p><br>
                    <h3>Work Status Report</h3>
                </td>
            </tr>
        </tbody>
    </table>
<?php
if(!empty($final_array)){
?>
<table border="1" style="width:100%;border:1px solid gray;">

<thead>
            <tr>
                <th>SI No</th>
                <th>Project</th>
                <th>Date</th>
                <?php
                if(!empty($head_array)){
                    foreach($head_array as $key2 => $head){
                        echo ' <th>'.$head.'</th>';
                ?>
                <?php  } } ?>
                <th>Total Amount</th>
            </tr>
        </thead>
        <tbody>
        <?php
        $grand_total =0;
        foreach($final_array as $key => $data){
            $grand_total += $data['total_amount'];
        ?>
        <tr>
        <th><?php echo $key+1; ?></th>
        <th><?php echo $data['project']; ?></th>
        <th></th>
        <?php
            if(!empty($head_array)){
                foreach($head_array as $key2 => $head){
                    echo ' <th></th>';
            ?>
            <?php  } } ?>
        <th><?php echo Controller::money_format_inr($data['total_amount'],2) ;?></th>
        </tr>
        <?php
       if(isset($data['payment']) && !empty($data['payment'])){
          foreach($data['payment'] as $key=> $values){
        ?>
        <tr>
        <td></td>
        <td></td>
        <td><?php  echo $values['date']; ?> </td>
        <?php
        if(!empty($values['items'])){
        foreach($values['items'] as $key3 => $value3){
        ?>
        <td><?php echo $value3; ?></td>
        <?php } } ?>
        <td><?php  echo Controller::money_format_inr($values['amount'],2); ?></td>
        </tr>
          <?php } } }  ?>
        </tbody>
        <tfoot>
        <tr>
        <th colspan="<?php echo count($head_array) +3; ?>" class="text-right">Total Expense</th>
        <th><?php echo Controller::money_format_inr($grand_total,2); ?></th>
        </tr>
        </tfoot>
</table>
<?php }else{
            echo "No data found";
        } ?>
