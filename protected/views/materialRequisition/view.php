<?php
/* @var $this MaterialRequisitionController */
/* @var $model MaterialRequisition */

$this->breadcrumbs = array(
  'Material Requisitions' => array('index'),
  $model->requisition_id,
);

?>

<div class="container">
  <div class="loading-overlay">
    <span class="fa fa-spinner fa-3x fa-spin"></span>
  </div>
  <div class="header-container">
    <h3>
      <?php echo $model->requisition_no ? "#$model->requisition_no" : ''; ?>
    </h3>
    <?php echo CHtml::link('List MaterialRequisition', array('index'), array('class' => 'pull-right btn btn-primary pdf_excel')); ?>
    <input type="hidden" id="mrId" value="<?php echo $model->requisition_id; ?>">
  </div>
  <!--PO Items model-->
  <div class="page_filter">
    <div class="row">
      <div class="col-sm-3 col-md-2">
        <label>Requisition No:</label>
        <div>
          <?php echo $model->requisition_no ?>
        </div>
      </div>

      <div class="col-sm-3 col-md-2">
        <label>Project:</label>
        <div>
          <?php
          echo isset($model->project) ? $model->project->name : '';

          ?>
        </div>
      </div>

      <div class="col-sm-3 col-md-2">
        <label>Task:</label>
        <div>
          <?php
          echo isset($model->task_description) ? $model->task_description : '';
          ?>
        </div>
      </div>
      <div class="col-sm-3 col-md-2 form-group">
        <div>
          <label>PO/Bill No:</label>
          <div id="po_bill">&nbsp;</div>
        </div>
      </div>
      <div class="col-sm-3 col-md-1">
        <label>Created By:</label>
        <div>
          <?php
          if ($model->type_from == 1) {
            $table = "pms_users";
          } else {
            $table = "jp_users";
          }
          $sql = 'SELECT first_name,last_name '
            . ' FROM ' . $table . '  WHERE userid=' . $model->created_by;
          $users = Yii::app()->db->createCommand($sql)->queryRow();
          echo ($users['first_name'] . " " . $users['last_name']);
          ?>
        </div>
      </div>
      <div class="col-sm-3 col-md-1">
        <label> Date:</label>
        <div>
          <?php echo date('d-M-y', strtotime($model->created_date)); ?>
        </div>
      </div>
      <div class="col-sm-3 col-md-1">
        <label>Type From:</label>
        <div>
          <?php if ($model->type_from == 1) {
            echo 'PMS';
          } else {
            echo 'ACCOUNTS';
          } ?>
        </div>
      </div>
    </div>
  </div>
  <div class="table-responsive">
    <table cellpadding="3" cellspacing="0" class="table table-bordered">
      <thead class="entry-table sticky-thead">
        <tr class="tr_title">
          <th>Sl no</th>
          <th>ITEM</th>
          <th>UNIT</th>
          <th>QUANTITY</th>
          <th>PO/Bill No</th>
          <th>PO/Bill Status</th>
          <th>DATE</th>
          <th> REMARKS</th>

        </tr>
      </thead>
      <tbody>
        <?php foreach ($material_req_item_arr as $key => $item) { ?>
          <tr>
            <td width="5%">
              <?= $key + 1 ?>
            </td>
            <td width="30%">
              <?= $item['item_name'] ?>
            </td>
            <td width="10%">
              <?= $item['item_unit'] ?>
            </td>
            <td width="10%">
              <?= $item['item_quantity'] ?>
            </td>
            <td width="10%">
              <?php
              $viewUrl = Yii::app()->createUrl('purchase/viewpurchase', array('pid' => $item['item_po_id']));

              echo '<a href="' . $viewUrl . '" target="_blank">' . $item['item_po_no'] . '</a>'; ?>
            </td>
            <td width="10%">
              <?= isset($item['item_po_id']) ? MaterialRequisition::model()->getBillingStatus($item['item_po_id']) : "Nil" ?>
            </td>
            <td width="15%">
              <?= date('d-M-y', strtotime($item['item_req_date'])) ?>
            </td>
            <td width="25%">
              <?= $item['item_remarks'] ?>
            </td>
          </tr>
        <?php } ?>
      </tbody>
    </table>
  </div>
</div>
</div>


<!-- Modal -->
<div class="modal fade " id="poModal" role="dialog" aria-labelledby="poModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-long" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="poModalLabel"></h5>
        <button type="button" class="close clsbtn" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <!-- Content will be loaded here -->
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary clsbtn btn btn-primary pdf_excel"
          data-dismiss="modal">Save</button>
      </div>
    </div>
  </div>
</div>
<!-- Bill Modal -->
<div class="modal fade " id="billModal" role="dialog" aria-labelledby="billModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-long" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="billModalLabel"></h5>
        <button type="button" class="close clsbtn" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <!-- Content will be loaded here -->
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary clsbtn btn btn-primary pdf_excel"
          data-dismiss="modal">Save</button>
      </div>
    </div>
  </div>
</div>
<!-- PO ITEMS Modal -->
<div class="modal fade " id="poItemsModal" role="dialog" aria-labelledby="poItemsModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-long" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="poItemsModalLabel"></h5>
        <button type="button" class="close clsbtn" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <!-- Content will be loaded here -->
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary clsbtn btn btn-primary pdf_excel"
          data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<!--Bill ITEMS-->
<div class="modal fade " id="billitemsModal" role="dialog" aria-labelledby="billitemsModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-long" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="billitemsModalLabel"></h5>
        <button type="button" class="close clsbtn" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <!-- Content will be loaded here -->
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary clsbtn btn btn-primary pdf_excel"
          data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<style>
  .modal-long {
    max-width: 1270px;
    width: 95%;
  }

  .modal-long-po {
    max-width: 1200px;
    width: 95%;
  }

  .select2-dropdown.select2-dropdown--below {
    z-index: 4111 !important;
  }
</style>
<script>

  function reloadParent() {
    location.reload();
  }
  $(document).ready(function () {
    $('#poModal .clsbtn').on('click', function () {

      console.log('Modal is now hidden');
      window.parent.location.reload();
    });
    $('#addPoButton,#editPoButton').click(function () {
      var mrId = $("#mrId").val();
      var baseUrl = '<?php echo Yii::app()->baseUrl; ?>';
      var url = baseUrl + '/index.php?r=materialRequisition/loadPoModal&mrId=' + mrId;
      $('.loading-overlay').addClass('is-active');
      // Perform AJAX request
      $.get(url, function (data) {
        // Load response into modal body
        $('#poModal .modal-body').html(data);
        // Show modal
        $('#poModal').modal('show');
        // Initialize select2 after the modal is shown and the content is loaded
        $('#poModal .select2').select2({
          dropdownParent: $("#poModal")
        });
        $('.loading-overlay').removeClass('is-active');
      }).fail(function () {
        $('#poModal .modal-body').html('<p>Error loading information</p>');
      });
    });

    $('#billModal .clsbtn').on('click', function () {

      console.log('Modal is now hidden');
      window.parent.location.reload();
    });
    $('#addBillButton,#editBillButton').click(function () {
      var mrId = $("#mrId").val();
      var baseUrl = '<?php echo Yii::app()->baseUrl; ?>';
      var url = baseUrl + '/index.php?r=bills/loadbillModal&mrId=' + mrId;
      $('.loading-overlay').addClass('is-active');
      $.get(url, function (data) {

        $('#billModal').modal('show');
        $('#billModal .modal-body').html(data);


        $('#billModal .select2').select2({
          dropdownParent: $("#billModal")
        });
        $('.loading-overlay').removeClass('is-active');

      }).fail(function () {
        $('#billModal .modal-body').html('<p>Error loading information</p>');
      });
    });


    $(document).ready(function () {
      $('#poItemsModal .clsbtn').on('click', function () {

        console.log('Modal is now hidden');
        window.parent.location.reload();
      });
      $('.addPoItembtn').click(function () {
        $('.loading-overlay').addClass('is-active');

        var mrId = $("#mrId").val();
        itemId = $(this).data('item-id');
        var baseUrl = '<?php echo Yii::app()->baseUrl; ?>';

        var url = baseUrl + '/index.php?r=purchase/loadPoItemsModal&mrId=' + mrId + '&itemId=' + itemId;
        // Load content into modal body
        $('#poItemsModal .modal-body').load(url, function () {
          $('#poItemsModal').modal('show');
          $('.loading-overlay').removeClass('is-active');
        });

        $('#poItemsModal .select2').select2({
          dropdownParent: $("#poItemsModal")
        });


      });
      $('#billitemsModal .clsbtn').on('click', function () {

        console.log('Modal is now hidden');
        window.parent.location.reload();
      });
      $('.addBillItembtn').click(function () {
        $('.loading-overlay').addClass('is-active');
        var mrId = $("#mrId").val();
        itemId = $(this).data('item-id');
        var baseUrl = '<?php echo Yii::app()->baseUrl; ?>';
        var url = baseUrl + '/index.php?r=bills/loadbillItemsModal&mrId=' + mrId + '&itemId=' + itemId;
        // Load content into modal body
        $('#billitemsModal').modal('show');
        $('#billitemsModal .modal-body').load(url);
        $('.loading-overlay').removeClass('is-active');
        $('#billitemsModal .select2').select2({
          dropdownParent: $("#billitemsModal")
        });
      });



    });

    // $(".save-btn").click(function(){
    // 	$("#bill_number").trigger("blur");
    // }) 


  });


</script>