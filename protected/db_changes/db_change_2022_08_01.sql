INSERT INTO `jp_menu` (`menu_id`, `menu_name`, `parent_id`, `status`, `controller`, `action`, `params`, `showmenu`, `show_list`, `related`) VALUES (779, 'Material Requisition', '3', '0', 'purchase', 'materialrequisition', '', '2', '1', NULL);
ALTER TABLE `jp_pre_expenses` CHANGE `approved_by` `approved_by` INT(11) NULL DEFAULT NULL;
ALTER TABLE `jp_pre_expenses` CHANGE `cancelled_at` `cancelled_at` DATETIME NULL DEFAULT NULL;
ALTER TABLE `jp_expenses` ADD CONSTRAINT `fk_invoice_id` FOREIGN KEY (`invoice_id`) REFERENCES `jp_invoice`(`invoice_id`) ON DELETE RESTRICT ON UPDATE CASCADE;
