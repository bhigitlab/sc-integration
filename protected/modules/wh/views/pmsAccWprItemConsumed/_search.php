<?php
/* @var $this PmsAccWprItemConsumedController */
/* @var $model PmsAccWprItemConsumed */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'warehouse_id'); ?>
		<?php echo $form->textField($model,'warehouse_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'item_id'); ?>
		<?php echo $form->textField($model,'item_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'item_rate'); ?>
		<?php echo $form->textField($model,'item_rate'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'item_qty'); ?>
		<?php echo $form->textField($model,'item_qty'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'status'); ?>
		<?php echo $form->textField($model,'status'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'item_added_by'); ?>
		<?php echo $form->textField($model,'item_added_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'item_approved_by'); ?>
		<?php echo $form->textField($model,'item_approved_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'wpr_id'); ?>
		<?php echo $form->textField($model,'wpr_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'pms_project_id'); ?>
		<?php echo $form->textField($model,'pms_project_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'coms_project_id'); ?>
		<?php echo $form->textField($model,'coms_project_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'coms_material_id'); ?>
		<?php echo $form->textField($model,'coms_material_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'pms_material_id'); ?>
		<?php echo $form->textField($model,'pms_material_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'pms_template_material_id'); ?>
		<?php echo $form->textField($model,'pms_template_material_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'type_from'); ?>
		<?php echo $form->textField($model,'type_from'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created_date'); ?>
		<?php echo $form->textField($model,'created_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'updated_date'); ?>
		<?php echo $form->textField($model,'updated_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'pms_task_name'); ?>
		<?php echo $form->textField($model,'pms_task_name',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->