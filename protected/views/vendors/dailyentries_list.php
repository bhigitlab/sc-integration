<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/tableHeadFixer.js"></script>
<?php
$total_amount    = 0;
$total_taxamount = 0;
$total_tds       = 0;
$total_paid      = 0;
foreach ($newmodel as $expenses) {
    $total_amount = $total_amount + $expenses["amount"];
    $total_taxamount = $total_taxamount + $expenses["tax_amount"];
    $total_tds = $total_tds + $expenses["tds_amount"];
    $total_paid = $total_paid + $expenses["paidamount"];
}
?>
<div class="" id="newlist">
    <?php
    $sort = new CSort;
    $sort->defaultOrder = 'daily_v_id DESC';
    $sort->attributes = array(
        'daily_v_id' => 'Daily Expense Id',
        'daily_v_id' => 'Payment ID',
        'amount' => 'Total Amount',
        'amount' => 'Paid Amount'
    );
    $dataProvider = new CArrayDataProvider($newmodel, array(
        'id' => 'daily_v_id', //this is an identifier for the array data provider
        'sort' => $sort,
        'keyField' => 'daily_v_id', //this is what will be considered your key field
        'pagination' => false
    ));
    $this->widget('zii.widgets.CListView', array(
        'dataProvider' => $dataProvider,
        //'data'=>$newmodel,
        'viewData' => array('total_amount' => $total_amount, 'tax_amount' => $total_taxamount, 'total_tds' => $total_tds, 'total_paid' => $total_paid),
        'itemView' => '_newviewlist',
        'id' => 'daily_v_id',
        'enableSorting' => 1,
        'enablePagination' => true,
        'template' => '<div>{summary}{sorter}</div><div id="parent" class="table-wrapper margin-top-10"><div id="table-wrapper"><table width="100" id="fixTable" class="table total-table ">{items}</table></div></div>{pager}',
        //'itemView' => '_newview', 'template' => '<div>{summary}{sorter}</div><div class="tblwrap scrollTable"><div class="container1"><table cellpadding="10" class="table  list-view sorter">{items}</table></div></div>{pager}',
        'enablePagination' => false
    )); ?>


</div>
<script>
    $(document).ready(function() {
        $("#fixTable").tableHeadFixer({
            'left': 1,
            'foot': true,
            'head': true
        });
        //$("#fixTable").tableHeadFixer({'foot' : true, 'head' : true}); 
    });
</script>
<style>
    #parent {
        max-height: 300px;
    }

    th {
        background: #eee;
    }

    .pro_back {
        /* background: #fafafa; */
    }

    .total-table {
        margin-bottom: 0px;
    }

    th,
    td {
        border-right: 1px solid #ccc !important;
    }
    .popover{
        white-space:nowrap;
    }
</style>