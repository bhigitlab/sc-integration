<?php
/* @var $this PaymentRemindersController */
/* @var $model PaymentReminders */

$this->breadcrumbs=array(
	'Payment Reminders'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List PaymentReminders', 'url'=>array('index')),
	array('label'=>'Create PaymentReminders', 'url'=>array('create')),
	array('label'=>'Update PaymentReminders', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete PaymentReminders', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage PaymentReminders', 'url'=>array('admin')),
);
?>

<h1>View PaymentReminders #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'company_id',
		'project_id',
		'amount',
		'date',
		'status',
		'description',
		'created_date',
	),
)); ?>
