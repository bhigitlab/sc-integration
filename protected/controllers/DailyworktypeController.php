<?php

class DailyworktypeController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','getfields'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new DailyworkType;

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['DailyworkType']))
		{
            $model->attributes=$_POST['DailyworkType'];
            $model->amount_label=$_POST['DailyworkType']['amount_label'];

            $model->labour_status=$_POST['DailyworkType']['labour_status'];
            $model->wage_status=$_POST['DailyworkType']['wage_status'];
            $model->wagerate_status=$_POST['DailyworkType']['wagerate_status'];
            $model->helper_status=$_POST['DailyworkType']['helper_status'];
            $model->helperlabour_status=$_POST['DailyworkType']['helperlabour_status'];
            $model->amount_status=$_POST['DailyworkType']['amount_status'];
			if($model->save(true)){
                $this->redirect(array('index'));
            }
		}
                if(isset($_GET['layout']))
                $this->layout = false;

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
        if(isset($_GET['layout']))
                $this->layout = false;
                
		$model=$this->loadModel($id);
        $this->performAjaxValidation($model);

		if(isset($_POST['DailyworkType']))
		{
            $model->attributes=$_POST['DailyworkType'];
            $model->amount_label=$_POST['DailyworkType']['amount_label'];

            $model->labour_status=$_POST['DailyworkType']['labour_status'];
            $model->wage_status=$_POST['DailyworkType']['wage_status'];
            $model->wagerate_status=$_POST['DailyworkType']['wagerate_status'];
            $model->helper_status=$_POST['DailyworkType']['helper_status'];
            $model->helperlabour_status=$_POST['DailyworkType']['helperlabour_status'];
            $model->amount_status=$_POST['DailyworkType']['amount_status'];
			if($model->save(true)){
                $this->redirect(array('index'));
            }
		}
             

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
        $model = new DailyworkType('search');
		$model->unsetAttributes();  // clear any default values
		if (isset($_GET['DailyworkType']))
			$model->attributes = $_GET['DailyworkType'];
			
		$this->render('index', array(
				'model' => $model, 
				'dataProvider' =>  $model->search(),
			));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new DailyworkType('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['DailyworkType']))
			$model->attributes=$_GET['DailyworkType'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return DailyworkType the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=DailyworkType::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
    }
    
    public function actionGetfields(){
        $id= $_REQUEST['id'];
        $model=$this->loadModel($id);
        $html= "";
        if($model->labour_label != '' && $model->labour_status ==1){
        $html .=' <div class="elem_block">
        <div class="form-group">   <label for="description">'.$model->labour_label.'</label>
            <input class="form-control" id="txtDescription" name="Expenses[expense_description]"> </div></div>';
        }
        if($model->wage_label != '' && $model->wage_status ==1){
        $html .='  <div class="elem_block">
        <div class="form-group">  <label for="description">'.$model->wage_label.'</label>
            <input class="form-control" id="txtDescription" name="Expenses[expense_description]"> </div></div>';
        }

        if($model->wage_rate_label != '' && $model->wagerate_status ==1){
            $html .=' <div class="elem_block">
            <div class="form-group">   <label for="description">'.$model->wage_rate_label.'</label>
                <input class="form-control" id="txtDescription" name="Expenses[expense_description]"> </div></div>';
        }
        if($model->helper_label != '' && $model->helper_status ==1){
            $html .='<div class="elem_block">
            <div class="form-group">    <label for="description">'.$model->helper_label.'</label>
                <input class="form-control" id="txtDescription" name="Expenses[expense_description]"> </div></div>';
        }

        if($model->helper_labour_label != '' && $model->helperlabour_status ==1){
            $html .='<div class="elem_block">
            <div class="form-group"><label for="description">'.$model->helper_labour_label.'</label>
                <input class="form-control" id="txtDescription" name="Expenses[expense_description]"> </div></div>';
        }

    echo $html;
    }

	/**
	 * Performs the AJAX validation.
	 * @param DailyworkType $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='daily-work-type-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
