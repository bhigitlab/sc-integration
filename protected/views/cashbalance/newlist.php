<?php
/* @var $this ExpensesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'Cash Balance',
);
$page = Yii::app()->request->getParam('Cashbalance_page');
if ($page != '') {
    Yii::app()->user->setReturnUrl($page);
} else {
    Yii::app()->user->setReturnUrl(0);
}
?>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<div class="container" id="expensetype">
    <div class="loading-overlay">
        <span class="fa fa-spinner fa-3x fa-spin"></span>
    </div>
    <div class="clearfix">
        <div class="add-btn pull-right">
            <?php
            if (isset(Yii::app()->user->role) && (in_array('/cashbalance/create', Yii::app()->user->menuauthlist))) {
            ?>
                <!--            <button data-toggle="modal" data-target="#addExpensetype"  class="createExpensetype">Add Cash Balance</button>-->

                <a class="button addcash">Add Cash Balance</a>

            <?php } ?>
        </div>
        <h2>Cash Balance</h2>
    </div>
    <div id="addcash" style="display:none;"></div>





    <div class="row">
        <div class="col-md-12">
            <?php
            /*$this->widget('zii.widgets.CListView', array(
    'dataProvider' => $dataProvider,
    'itemView' => '_newview',
)); */
            ?>

            <?php $this->widget('zii.widgets.CListView', array(
                'dataProvider' => $dataProvider,
                //'itemView'=>'_view',
                'itemView' => '_view', 'template' => '<div class=""><table cellpadding="10" class="table " id="cashblncetbl">{items}</table></div>',
                'ajaxUpdate' => false,
            )); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="clearfix">
                <a style="cursor:pointer;margin-left: 4px;margin-bottom: 10px;" class="save_btn pull-right" href="<?php echo $this->createAbsoluteUrl('Cashbalance/cashblancereportpdf', array('date_to' => '', 'date_from' => '')) ?>">SAVE AS PDF</a>
                <a style="cursor:pointer;" class="save_btn pull-right" href="<?php echo $this->createAbsoluteUrl('Cashbalance/cashblancereportexcel', array('date_to' => '', 'date_from' => '')) ?>">SAVE AS EXCEL</a>
            </div>
            <?php $this->renderPartial('_report', array('model' => $model)) ?>
        </div>
    </div>

    <!-- Add Expense type Popup -->
    <!--        <div id="addExpensetype" class="modal fade" role="dialog">
            <div class="modal-dialog modal-lg">

            </div>
        </div>-->


    <!-- Edit Expense type Popup -->

    <!--        <div class="modal fade edit" role="dialog">
            <div class="modal-dialog modal-lg">


            </div>
        </div>-->
    <?php
    Yii::app()->clientScript->registerScript('myjavascript', '
     $(document).ready(function(){
     
       $("#cashblncetbl").dataTable( {
            "scrollY": "300px",
            "scrollCollapse": true,
            
             "columnDefs"        : [       
                { 
                    "searchable"    : false, 
                    "targets"       : [0] 
                },{ "bSortable": false, "aTargets": [-1]  }
            ],
               
            
            
	} );
	
	
      
        
	});
        
    ');
    ?>
    <script>
        function closeaction() {
            $('#addcash').slideUp();
        }
        $(document).ready(function() {

            $('.addcash').click(function() {
                $('.loading-overlay').addClass('is-active');
                $.ajax({
                    type: "POST",
                    //dataType: "JSON",
                    url: "<?php echo $this->createUrl('cashbalance/create&layout=1') ?>",
                    success: function(response) {
                        $('#addcash').html(response).slideDown();
                    },
                });
            });



            $('.editProject').click(function() {

                $('.addcash').show();
                var id = $(this).attr('data-id');
                $('.loading-overlay').addClass('is-active');
                $.ajax({
                    type: "POST",
                    //dataType: "JSON",
                    url: "<?php echo $this->createUrl('cashbalance/update&layout=1&id=') ?>" + id,
                    success: function(response) {
                        $('#addcash').html(response).slideDown();
                    },
                });
            });
        });
        $(document).ajaxComplete(function() {
            $('.loading-overlay').removeClass('is-active');
            $('#loading').hide();
        });
    </script>



</div>
</div>
<style>
    .savepdf {
        background-color: #6a8ec7;
        border: 1px solid #6a8ec8;
        color: #fff;
        padding: 5px;
    }

    .page-body h3 {
        margin: 4px 0px;
        color: inherit;
        text-align: left;
    }

    .panel {
        border: 1px solid #ddd;
        box-shadow: 0px 0px 0px 0px;
    }

    .panel-heading {
        background-color: #eee;
        height: 40px;
    }
</style>