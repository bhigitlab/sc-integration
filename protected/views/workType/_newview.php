
<?php
if ($index == 0) {
    ?>
    <thead>
    <tr> 
        <th>No</th>
        <th>Work Type</th>
        
        <th style="display: none;"></th>
          
    </tr>   
    </thead>
<?php } ?>
    <tr> 
        <td style="width: 50px;"><?php echo $index+1; ?></td>
        
        <td  class="text_black" data-value="<?php echo CHtml::encode($data->work_type); ?>" data-id="<?php echo $data->wtid; ?>" >
            <span class="list_item"><?php echo CHtml::encode($data->work_type); ?></span>
            <?php
            if(isset(Yii::app()->user->role) && (in_array('/workType/update', Yii::app()->user->menuauthlist))){
            ?>
            <span class="fa fa-edit editable" onclick="editaction(this,event);" data-id="<?php echo $data->wtid; ?>" data-value="<?php echo CHtml::encode($data->work_type); ?>"></span>
            <?php } ?>
        </td>
        
        <td class='editelement test' style="display:none;">
        <input  class='work_type singletextbox' type = 'text'  name='work_type' data-value="<?php echo CHtml::encode($data->work_type); ?>">
        <a class='save btn btn-info btn-xs' onclick="saveaction(this,event);" type = 'submit' data-id="<?php echo $data->wtid; ?>" >Save</a>
        <a class='cancel btn btn-default btn-xs' onclick="cancelaction(this,event);">Cancel</a>
        <br>
        <span class='errorMessage1'></span>
       
        
        </td>		
    
    </tr>  
