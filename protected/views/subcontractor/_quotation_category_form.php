<div class="panel panel-default collapse" id="addCategory">
    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'sc-quotation-item-category-form',
        'enableAjaxValidation' => false,
        'htmlOptions' => array(
            'class' => 'category_form',
        )
    )); ?>
    <input type="hidden" name="ScQuotationItemCategory[sc_quotaion_id]" id="ScQuotationItemCategory_sc_quotaion_id" value="<?php echo $model->scquotation_id; ?>">
    <input type="hidden" name="ScQuotationItemCategory[id]" id="ScQuotationItemCategory_id" value="">
    <div class="panel-heading h-head">
        <div class="row">
            <div class="col-md-2">
                <?php
                echo $form->textField($main_item_model, 'main_title',  array('class' => 'form-control require', 'placeholder' => 'category'));
                ?>

            </div>
        </div>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-6">
                <label for="">Description</label>
                <?php
                echo $form->textArea($main_item_model, 'main_description',  array('class' => 'form-control  text_editor'));
                ?>

            </div>
        </div>
        <?php $this->endWidget(); ?>
        <?php
        $quotation_list = $model->getQuotationItems($model->scquotation_id);
        if (!empty($quotation_list)) {
            $this->renderPartial('_quotation_list', array('model' => $model, 'quotation_list' => $quotation_list));
        } ?>
    </div>
</div>