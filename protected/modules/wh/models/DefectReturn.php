<?php

/**
 * This is the model class for table "defect_return".
 *
 * The followings are the available columns in table 'defect_return':
 * @property integer $return_id
 * @property integer $receipt_id
 * @property string $return_number
 * @property string $return_date
 * @property double $return_amount
 * @property integer $company_id
 * @property integer $created_by
 * @property string $created_date
 * @property integer $updated_by
 * @property string $updated_date
 * @property integer $return_status
 */
class DefectReturn extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{defect_return}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('return_number, return_date, return_amount, company_id, created_by, created_date', 'required'),
            array('receipt_id, company_id, created_by, updated_by, return_status', 'numerical', 'integerOnly'=>true),
            array('return_amount', 'numerical'),
            array('updated_date', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('return_id, receipt_id, return_number, return_date, return_amount, company_id, created_by, created_date, updated_by, updated_date, return_status', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'return_id' => 'Return',
            'receipt_id' => 'Receipt',
            'return_number' => 'Defect Return Number',
            'return_date' => 'Return Date',
            'return_amount' => 'Return Amount',            
            'company_id' => 'Company',
            'created_by' => 'Created By',
            'created_date' => 'Created Date',
            'updated_by' => 'Updated By',
            'updated_date' => 'Updated Date',
            'return_status' => 'Return Status',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('return_id',$this->return_id);
        $criteria->compare('receipt_id',$this->receipt_id);
        $criteria->compare('return_number',$this->return_number,true);
        $criteria->compare('return_date',$this->return_date,true);
        $criteria->compare('return_amount',$this->return_amount);       
        $criteria->compare('company_id',$this->company_id);
        $criteria->compare('created_by',$this->created_by);
        $criteria->compare('created_date',$this->created_date,true);
        $criteria->compare('updated_by',$this->updated_by);
        $criteria->compare('updated_date',$this->updated_date,true);
        $criteria->compare('return_status',$this->return_status);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return DefectReturn the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}