<?php

class ApiEstimationController extends Controller
{
	public function actionIndex()
	{
		$this->render('index');
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
	public function actionSave()
    {
        $estimationData = json_decode(file_get_contents('php://input'), true);

        $estimation = new ApiEstimation();
        $estimation->attributes = $estimationData;

        if ($estimation->save()) {
            $response = array('success' => true, 'message' => 'Estimation saved successfully.');
        } else {
            $response = array('success' => false, 'errors' => $estimation->getErrors());
        }

        echo json_encode($response);
        Yii::app()->end();
    }
}