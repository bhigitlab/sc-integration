<?php
/* @var $this SubcontractorPaymentController */
/* @var $data SubcontractorPayment */
$tblpx = Yii::app()->db->tablePrefix;
if ($index == 0) {
?>
<thead class="entry-table">
    <tr>
        <th>SI No</th>
        <th>Company</th>
        <th>Subcontractor</th>
        <th>Project</th>
        <th>Description</th>
        <th>Transaction Type</th>
        <th>Bank</th>
        <th>Cheque No</th>
        <th>Amount</th>
        <th>Tax Amount</th>
        <th>TDS Amount</th>
        <th>Paid Amount</th>
    </tr>
</thead>
<?php } ?>
<?php
$class      = '';
$date       = strtotime("+2 days", strtotime($data->date));
$check_date = date("Y-m-d", $date);
if ($data->approve_status == 'No') {
    $class  = 'permission_style';
} else {
    $class  = '';
}
$expenseId      = $data->payment_id;
?>
<tr id="<?php echo $index; ?>">
  <td><?php echo $index + 1; ?></td>
    <td class="pro_back wrap vertical-td" id="<?php echo "subcontractor_id" . $index; ?>">
    <?php
        $company = Company::model()->findByPk($data->company_id);
        echo $company->name;
    ?>
    </td>
    <td id="<?php echo "subcontractor_id" . $index; ?>" class="vertical-td">
    <?php
        $subcon = Subcontractor::model()->findByPk($data->subcontractor_id);
        echo $subcon->subcontractor_name;
    ?>
    </td>
    <td id="<?php echo "project_id" . $index; ?>">
    <?php
        $project = Projects::model()->findByPk($data->project_id);

        if ($project !== null) {
        echo $project->name;
        } else {
            // Handle the case where project is not found
        echo "";
        }
    ?>
    </td>
    
    <td id="<?php echo "description" . $index; ?>" class="vertical-td"><?php echo $data->description ? $data->description : ""; ?></td>
    <td id="<?php echo "payment_type" . $index; ?>" class="vertical-td">
        <?php
        $ptype = Status::model()->findByPk($data->payment_type);
        echo $ptype->caption;
        ?>
    </td>
    <td id="<?php echo "bank" . $index; ?>" class="vertical-td">
        <?php
        $bank = Bank::model()->findByPk($data->bank);
        if ($bank)
            echo $bank->bank_name;
        ?>
    </td>
    <td id="<?php echo "cheque_no" . $index; ?>" class="vertical-td"><?php echo $data->cheque_no ? $data->cheque_no : ""; ?></td>
    

    <td class="text-right" id="<?php echo "amount" . $index; ?>"><?php echo $data->amount ? Controller::money_format_inr($data->amount, 2) : ""; ?></td>
    <td class="text-right"><span class="popover-test tax_amnt" data-toggle="popover" data-trigger="hover" data-placement="left" data-trigger="hover" type="button" data-html="true"><?php echo $data->tax_amount ? Controller::money_format_inr($data->tax_amount, 2) : ""; ?></span>
    <td class="text-right"><span class="popover-test tds_amnt" data-toggle="popover" data-trigger="hover" data-placement="left" data-trigger="hover" type="button" data-html="true"><?php echo $data->tds_amount ? Controller::money_format_inr($data->tds_amount, 2) : ""; ?></span>
    <td class="text-right"><span class="popover-test paid_amnt" data-toggle="popover" data-trigger="hover" data-placement="left" data-trigger="hover" type="button" data-html="true"><?php echo $data->paidamount ? Controller::money_format_inr($data->paidamount, 2) : ""; ?></span>
        <div class="popover-content hide">
            <div><span>SGST: (<?php echo $data->sgst ? $data->sgst : ""; ?>&#37;) <?php echo $data->sgst_amount ? Controller::money_format_inr($data->sgst_amount, 2) : ""; ?></span></div>
            <div><span>CGST: (<?php echo $data->cgst ? $data->cgst : ""; ?>&#37;) <?php echo $data->cgst_amount ? Controller::money_format_inr($data->cgst_amount, 2) : ""; ?></span></div>
            <div><span>IGST: (<?php echo $data->igst ? $data->igst : ""; ?>&#37;) <?php echo $data->igst_amount ? Controller::money_format_inr($data->igst_amount, 2) : ""; ?></span></div>
        </div>
    </td>
</tr>
