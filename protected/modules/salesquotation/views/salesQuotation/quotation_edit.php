<div id='previous_details'></div>

<?php
$form = $this->beginWidget('CActiveForm', array(
    'enableAjaxValidation' => true,
    'enableClientValidation' => true,
    'action' => Yii::app()->createAbsoluteUrl("salesquotation/salesQuotation/addquotationitemsDetails"),
    'htmlOptions' => array('enctype' => 'multipart/form-data', 'class' => 'clients-form'),
    'clientOptions' => array(
        'validateOnSubmit' => true,
        'validateOnChange' => true,
        'validateOnType' => false,
    ),
        ));
$amountget = Yii::app()->createAbsoluteUrl("salesquotation/salesQuotation/getamountdetails");

?>
<?php echo CHtml::hiddenField('cat_id', $category_id, array('id' => 'cat_id')); ?>
<?php echo CHtml::hiddenField('cat_name', $category_label, array('id' => 'cat_name')); ?>
<?php echo CHtml::hiddenField('master_id', $qid, array('id' => 'master_id')); ?>
<?php echo CHtml::hiddenField('revision_no', $rev, array('id' => 'revision_no')); ?>
<?php echo CHtml::hiddenField('status', $status, array('id' => 'status')); ?>
<?php echo CHtml::hiddenField('parent_type', $parent_type, array('id' => 'parent_type')); ?>
<?php echo CHtml::hiddenField('parent_id', $parent_id, array('id' => 'parent_id')); ?>
<?php echo CHtml::hiddenField('category_label_id', $category_label_id, array('id' => 'category_label_id')); ?>

<span id="error-add"></span>
<div class="panel-body">
    
        <div class="mainitem" style="margin-top: 2rem;"> 
            <br>
            <div class="msg"></div>
            <table class="table qitem_table mt-2 shdow_box" id="qitem_table">
                <thead>
                    <tr>
                        <th colspan="12">Add Items 
                            <div class="add_more_items" data-cat_id="<?php echo $category_id?>"></div>
                        </th>
                        
                    </tr>
                    
                </thead>
                <tbody>
                    <?php
                    $worktypearray = array();
                    $i=0;
                    foreach ($salesquotation as $key => $value) {
                        $work_type = QuotationWorktypeMaster::model()->findByPk($value['work_type']);
                        $bgcolor = "";
                        if(($value['id'] !="") && ($value['deleted_status'] !=1)){
                            $bgcolor = "style=background-color:#ff000017";
                        }
                        ?>
                        <tr <?php //echo  $bgcolor?>>
                            <td data-incremnt ='<?php  echo $key ?>'>
                                <?php if($i==0){ ?>
                                <label>Item Name</label>
                                <?php } ?>
                                <?php echo $form->textField($value, 'item_name', array('class' => 'form-control  item_name ', 'autocomplete' => 'off', 'name' => 'item_name[]', 'data-incremntid' => $key, 'id' => 'item_name' . $key)); ?>
                            </td>
                            <td class="worktypedata" data-id ='<?php echo $value['work_type'] ?>'    data-incremnt ='<?php  echo $key ?>' data-template ='<?php echo $work_type['template_id'] ?>' >
                                <?php if($i==0){ ?>
                                <label>Work Type</label>
                                <?php } 
                                
                                ?>
                                <span><input type="text" class="form-control" value="<?php echo $work_type['name'] ?>" readonly></span>   
                                <?php echo CHtml::hiddenField('work_type_id[]', $value['work_type'], array('id' => 'hiddenworktype' . $key,'class'=>'hiddenworktype')); ?>
                                <?php echo CHtml::hiddenField('item_id[]', $value['item_id'], array('id' => 'hiddenitem' . $key,'class'=>'hiddenitem')); ?>
                                <?php echo CHtml::hiddenField('template_id[]', $work_type['template_id'], array('id' => 'hiddentemplate' . $key,'class'=>'hiddentemplate')); ?> 
                                <input type="hidden" class="entry_id" value="<?php echo $value['id']?>" name = 'entry_id[]'>                            
                            </td>
                            <td>
                                <?php if($i==0){ ?>
                                <label>WorkType Label</label>
                                <?php } 
                                $newQuery = 'work_type_id =' . $value['work_type'] . ' AND quotation_category_id =' . $category_id . '';                                
                                echo CHtml::dropDownList('worktype_label', $value['worktype_label'], CHtml::listData(QuotationItemMaster::model()->findAll(array('condition' => $newQuery)), 'worktype_label', 'worktype_label') ,array('class' => 'form-control js-example-basic-single field_change invoice_add require worktype_label update_item_input', 'name' => 'worktype_label[]', 'empty' => '-Select Worktype Label-', 'id' => 'worktype_label' . $key, 'style' => 'width:100%'));
                                ?>
                              <input class="form-control require" name="shutterwork_material[]" value="<?php echo $value["shutterwork_material"]?>" id="shutterwork_material<?php echo $key ?>" type="hidden" >
            <input class="form-control require" name="shutterwork_finish[]" value="<?php echo $value["shutterwork_finish"] ?>" id="shutterwork_finish<?php echo $key ?>" type="hidden">
            <input class="form-control require" name="carcass_material[]"  id= "carcass_material<?php echo $key ?>" value="<?php echo $value["carcass_material"] ?>" type="hidden">
            <input class="form-control require" name="carcass_finish[]" id="carcass_finish<?php echo $key ?>" value="<?php echo $value["carcass_finish"] ?>"  type="hidden">  

                            </td>
                            <?php 

                            $style="display:block";
                            $style1="display:block";
                            if ($value['shutterwork_description'] !="") { 
                                // $style="display:block";
                                // $style1="display:none";
                            }  ?>
                             
                                <td>
                                    <?php if($i==0){ ?>
                                    <label style="<?php echo $style ?>">Shutter Work</label>
                                    <?php }  
                                    ?>
                                        <input type="text" class="form-control" name="shutterwork_description[]" id="shutterwork_description<?php echo $key ?>" class="shutterwork_description" value="<?php echo $value['shutterwork_description']  ?>" readonly>  
                                   
                                                                                           
                                </td>
                                <td>
                                    <?php if($i==0){ ?>
                                    <label style="<?php echo $style ?>">Carcass Box Work</label>
                                    <?php } ?>
                                    
                                        <input type="text" class="form-control" name="caracoss_description[]" id="caracoss_description<?php echo $key ?>"  class="caracoss_description" value="<?php echo $value['carcass_description'] ?>" readonly>                                     
                                                                    
                                    
                                     <!-- OR --> 
                                    <?php 
                                    $style="display:block";
                                    $style1="display:none";
                                    if($i==0){ ?> 
                                    <label style="<?php echo $style1 ?>">Description</label> 
                                    <?php } ?>
                                    <input type="text" class="form-control" name="description[]" id="description<?php echo $key ?>" class="description"
                                    value="<?php echo $value['description'] ?>" style= "<?php echo $style1?>" readonly>                              
                                </td>
                                

                            <td>
                                <?php if($i==0){ ?>
                                <label>Unit</label>
                                <?php } ?>
                                <?php
                                echo CHtml::dropDownList('unit', $value['unit'], CHtml::listData(Unit::model()->findAll(), 'id', 'unit_name') ,array('class' => 'form-control js-example-basic-single field_change invoice_add require unit update_item_input', 'empty' => '-Select Unit-', 'id' => 'unit' . $key, 'style' => 'width:100%'));
                               
                                ?>
                                
                            </td>
                            <?php if($this->getActiveTemplate() == 'TYPE-7'){?>
                            <td>
                                <?php if($i==0){ ?>
                                <label>Length</label>
                                <?php } ?>
                                <?php echo $form->textField($value, 'length', array('class' => 'form-control allownumericdecimal length ', 'autocomplete' => 'off', 'name' => 'length[]', 'data-incremntid' => $key, 'id' => 'length' . $key)); ?>                                
                            </td>
                            <td>
                                <?php if($i==0){ ?>
                                <label>Width</label>
                                <?php } ?>
                                <?php echo $form->textField($value, 'width', array('class' => 'form-control allownumericdecimal width ', 'autocomplete' => 'off', 'name' => 'width[]', 'data-incremntid' => $key, 'id' => 'width' . $key)); ?>                                
                            </td>
                            <?php }?>
                            <td>
                                <?php if($i==0){ ?>
                                <label>Quantity</label>
                                <?php } ?>
                                <?php echo $form->textField($value, 'quantity', array('class' => 'form-control allownumericdecimal  require quantity ', 'autocomplete' => 'off', 'name' => 'qty[]', 'data-incremntid' => $key, 'id' => 'qty' . $key)); ?>                                
                            </td>
                            <td>
                            <?php if($i==0){ ?>
                                <label>Quantity (in NOS)</label>  
                                <?php } ?>                          
                                <?php echo $form->textField($value, 'quantity_nos', array('class' => 'form-control   require update_item_input', 'autocomplete' => 'off', 'name' => 'qtynos[]')); ?>                                
                            </td>
                            <td>
                                <?php if($i==0){ ?>
                                <label>MRP</label>
                                <?php } ?>                                
                                <?php echo $form->textField($value, 'mrp', array('class' => 'form-control allownumericdecimal require mrp update_item_input', 'autocomplete' => 'off', 'name' => 'mrp[]')); ?>                               
                            </td>
                            <td>
                                <?php if($i==0){ ?>
                                <label>Amount after discount</label>
                                <?php } ?>
                                <?php echo $form->textField($value, 'amount_after_discount', array('class' => 'form-control allownumericdecimal  require amountafter-discount update_item_input', 'autocomplete' => 'off', 'name' => 'amount_after_discount[]', 'id' => 'amount_after_discount' . $key)); ?>
                                
                                <?php
                                if ($value['id'] !=""){
                                if ($value['deleted_status'] == 1) { ?>
                                    <span id="<?php echo $value['id'] ?>" data-del ="<?php echo $value['deleted_status'] ?>"   class="removebtn"><span class="fa fa-trash removeQItem"></span></span>
                                <?php } else { ?>
                                    <span id="<?php echo $value['id'] ?>" data-del ="<?php echo $value['deleted_status'] ?>"  class="removebtn"><span class="fa fa-refresh restoreQItem"></span></span>
                                <?php } } ?>
                            </td>
                        </tr>

                    <?php $i++; } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <th colspan="12" class="text-right">
                            <button type="button" class = 'btn btn-info additem btn-sm subitem-save'>Save</button>
                        </th>
                    </tr>
                </tfoot>
            </table>
        </div>
    
    <?php $this->endWidget(); ?>

</div>

<script>
// each input change save
// quantity change
// btn-preview click on save - list updation
$(document).on('change', '.length', function () {                
        var element = $(this);
        var val = $(element).parents('tr').find(".quantity").attr('data-incremntid');
        var qty = $(element).parents('tr').find(".quantity").val(); 
        if(qty>0){       
            getamount(element, val);
        }
    });
$(document).on('change', '.width', function () {                
        var element = $(this);
        var val = $(element).parents('tr').find(".quantity").attr('data-incremntid');
        var qty = $(element).parents('tr').find(".quantity").val(); 
        if(qty>0){       
            getamount(element, val);
        }
    });   
$(document).on('change', '.quantity', function () {                
        var element = $(this);
        var val = element.attr('data-incremntid');        
        getamount(element, val);  
        $(".add_more_items").trigger("click");
    });



    function getamount(element, id) {
       
        var item_id = $(element).parents('tr').find("#hiddenitem" + id).val();
        console.log(id) ;
        var qty = $(element).parents('tr').find(".quantity").val();
        var length = $(element).parents('tr').find(".length").val();  
        var width = $(element).parents('tr').find(".width").val();         
        var status = '<?php echo $status; ?>';
        $.ajax({
            url: "<?php echo $amountget; ?>",
            data: {
                item_id: item_id,
                length: length,
                width: width,
                qty: qty
            },
            type: "POST",
            dataType: 'json',
            success: function (result) {
                if (result != '') {
                    var amount = result.profit_amount;
                    $(element).parents('tr').find(".amountafter-discount").val(result.profit_amount);
                    var mrp_after_discount = parseInt(result.profit_amount) + parseInt((result.profit_amount * result.discount) / 100);
                    $(element).parents('tr').find(".mrp").val(mrp_after_discount);
                    var sum_value = 0;
                    $('.amountafter-discount').each(function () {
                        sum_value += +$(this).val();
                    })
                    $('.amount-after-discount-extra').each(function () {
                        sum_value += +$(this).val();
                    })                   

                }

                $(element).trigger("blur");
                // $(elem).parents('tr').find(".quantity").trigger("change"); 
                // $(".quantity").trigger("change");
            }
        });
    }

    $(document).on("click",".updateRevision",function(){
       
        var rev = $(this).attr('data-rev');
        var qtid = $(this).attr('data-qid');
        var new_rev = "<?php echo isset($_GET['rev']) ? $_GET['rev'] : "" ?>";
        $.ajax({
            type: 'POST',
            url: '<?php echo Yii::app()->createAbsoluteUrl('salesquotation/SalesQuotation/updateInterRevision'); ?>',
            data: {
                qtid:qtid,
                rev:rev,
                new_rev:new_rev
            },            
            success: function (result) {
                console.log(result);                
            },
            complete: function (result) {
                setTimeout(function () {
                    location.reload(true);
                },2000);               
            }
        })
    })
    

 
    
    $(".item_name,.worktype_label,.unit,.length,.width,.quantity,#qtynos,.mrp,.amountafter-discount,.caracoss_description,.shutterwork_description,.description").blur(function(){
        var id = $(this).parents("tr").find(".entry_id").val();
        var item_id = $(this).parents("tr").find(".hiddenitem").val(); 
        var description = $(this).parents("tr").find(".description").val();     
        var caracoss_description = $(this).parents("tr").find(".caracoss_description").val(); 
        var shutterwork_description = $(this).parents("tr").find(".shutterwork_description").val();
        var item_name = $(this).parents("tr").find(".item_name").val();
        var worktype_label = $(this).parents("tr").find(".worktype_label").val();
        var unit = $(this).parents("tr").find(".unit").val();
        var length =$(this).parents("tr").find(".length").val();
        var width =$(this).parents("tr").find(".width").val();
        var qty =$(this).parents("tr").find(".quantity").val();
        var qty_nos = $(this).parents("tr").find("#qtynos").val();
        var mrp = $(this).parents("tr").find(".mrp").val();
        var amount_after_discount = $(this).parents("tr").find(".amountafter-discount").val();
        var status = "<?php echo isset($_GET['status']) ? $_GET['status'] : "" ?>";
        var rev = "<?php echo isset($_GET['rev']) ? $_GET['rev'] : "" ?>";
        
            $.ajax({
                url: '<?php echo Yii::app()->createAbsoluteUrl('salesquotation/SalesQuotation/saveRowQuotationItem'); ?>',
                data: {
                    id:id,
                    item_id:item_id,
                    description:description,
                    caracoss_description:caracoss_description,
                    shutterwork_description:shutterwork_description,
                    item_name:item_name,
                    worktype_label: worktype_label,
                    unit:unit,
                    length: length,
                    width: width,
                    quantity: qty,
                    quantity_nos:qty_nos,
                    mrp:mrp,
                    amount_after_discount:amount_after_discount,
                    status:status,
                    rev:rev
                },
                type: "POST",
                dataType: 'json',
                success: function (response) {
                    $(document).find(".msg").addClass("text-"+response.response+"").html(response.msg).fadeIn( 300 ).delay( 500 ).fadeOut( 400 );;
                    $(".btn-preview").trigger("click");
                    $(".updateRevision").trigger("click");
                }
            });        
    })

    // **************************************************************

    function getWorkType(category_id, callback){        
        $.ajax({
            url: '<?php echo Yii::app()->createAbsoluteUrl('salesquotation/SalesQuotation/getWorkType'); ?>',
            type: 'POST',
            dataType: 'json',
            data: {
                cat_id: category_id
            },
            success: callback
        })        
    }

    $(document).on('click', '.add_more_items', function () {
        var totalRow = $(this).parents("#qitem_table").find('tbody').find('tr').length - 1;                
        var rowIndex = $(this).parents('#qitem_table').find('tbody').find('tr:last').index();        
        // var element = $(this);
        var category_id = $(this).attr("data-cat_id");         
        var val = $(this).parents('#qitem_table').find('tbody').find('tr:last').find(".quantity").attr('data-incremntid');
        var worktype = $('#hiddenworktype' + val).val();
        var template = $('#hiddentemplate' + val).val();
        // getamount(element, val);
        var increment_id = $(this).parents('#qitem_table').find('tbody').find('tr:last').children('td:first').attr('data-incremnt');
        var count = $('.worktypedata').length;
        var worktype_label =$(this).parents('#qitem_table').find('tbody').find('tr:last').find('#worktype_label' + val).val();        
        if (worktype_label != '') {  
            // console.log("dsdad");return;
            
            var $tr = $(this).parents('#qitem_table').find('tbody').find('tr:last');            
            var allTrs = $(this).parents('#qitem_table tbody').find('tr');
            var lastTr = allTrs[allTrs.length - 1];
            var $clone = $tr.clone();
            
            var key = parseInt(val, 10) + parseInt(1, 10);
            $clone.find('td').each(function () {
                var el = $(this).children('input,select');
                $(this).children('input,select').removeClass('commonadd-val');
                el.each(function () {
                    var id = $(this).attr('id') || null;
                    if (id) {
                        var i = id.substr(id.length - 1);
                        var name = $(this).attr('name');
                        var classname = $(this).attr('class');
                        var prefix = id.substr(0, (id.length - 1));
                        $(this).attr('id', prefix + (+count));
                        $(this).parents('tr').find('td:first-child').attr('data-incremnt', count);
                        $(this).attr('name', name);
                        $(this).attr('class', classname);
                        $(this).parents('tr').attr('class', 'clone');
                    }
                });
            });
            // $clone.find('select').val('');
            getWorkType(category_id, function(response) {
                
                $clone.find('.worktypedata').find('span').hide();
                $clone.find('.worktypedata').find('select').hide();                
                $clone.find('.worktypedata').append('<select class="form-control worktypeClone">'+response.workTypeData+'</select>')           
            });
            $clone.find('.removebtn').hide();
            $clone.find('.worktypedata').attr('data-incremnt',count);
            $clone.find('.quantity').attr('data-incremntid', count);
            $clone.find('input:text').val('');
            $clone.find('select.unit').val('');
            $clone.find('.worktype_label').val('');
            $clone.find('.entry_id').val('');
            if(($("tr.clone").length !=0)){                  
               var  $lastclone  = $(this).parents('tbody').find('tr.clone:last-child');
                var worktypeClone = $lastclone.find(".worktypeClone").val();
                var worktype_label = $lastclone.find(".worktype_label").val();
                var quantity = $lastclone.find(".quantity").val();
                var description =  $lastclone.find(".description").val();
                if(worktypeClone != "" && worktype_label!="" && quantity!="" && description!=""){                    
                    $tr.append($clone);
                    $($clone).insertAfter($tr);
                }else if(totalRow == rowIndex){
                    $tr.append($clone);
                    $($clone).insertAfter($tr);
                }
            }else{
                $tr.append($clone);
                $($clone).insertAfter($tr); 
            }
            
        }
    });


    function getWorkTypeLabel(workType_id,category_id, callback){        
        $.ajax({
            url: '<?php echo Yii::app()->createAbsoluteUrl('salesquotation/SalesQuotation/getWorkTypeLabelArray'); ?>',
            type: 'POST',
            dataType: 'json',
            data: {
                category_id:category_id,
                workType_id: workType_id
            },
            success: callback
        })        
    }


    $(document).on('change','.worktypeClone',function(){
        var el = $(this);    
        var category_id = $(this).parents(".clients-form").find("#cat_id").val(); 
        var workType_id = $(this).val();
        getWorkTypeLabel(workType_id,category_id, function(response) { 
           // alert('hi'); 
            var incId =  el.parents('td').attr('data-incremnt');
            el.parents('td').find(".hiddenworktype").attr('id','hiddenworktype'+incId);
            el.parents('td').find(".hiddenitem").attr('id','hiddenitem'+incId);
            el.parents('td').find(".hiddentemplate").attr('id','hiddentemplate'+incId);
            el.parents('td').attr('data-id',workType_id);
            el.parents('td').attr('data-template',response.template);
            el.parents('td').find('#hiddenworktype'+incId).val(workType_id);
            el.parents('td').find('#hiddentemplate'+incId).val(response.template);
            el.parents('tr').find('td:nth-child(3)').find('select').html(response.workTypeLabelData);
            el.parents('tr').find('td:nth-child(4)').html('<input class="form-control require commonadd-val" name="shutterwork_description[]" value="" id="shutterwork_description'+incId+'" readonly="readonly" style="visibility:hidden;height:0px;" type="text" maxlength="255"><input class="form-control require" name="material[]" value="" id="material'+incId+'" type="hidden"><input class="form-control require" name="finish[]" value="" id="finish'+incId+'" type="hidden">');
            el.parents('tr').find('td:nth-child(5)').html('<input class="form-control require  commonadd-val" name="caracoss_description[]" value="" id="caracoss_description'+incId+'"  readonly="readonly" style="visibility:hidden;height:0px;" type="text" maxlength="255"><input class="form-control require" name="shutterwork_material[]" value="" id="shutterwork_material'+incId+'"  type="hidden"><input class="form-control require" name="shutterwork_finish[]" value="" id="shutterwork_finish'+incId+'"  type="hidden"><input class="form-control require" name="carcass_material[]" value="" id="carcass_material'+incId+'"  type="hidden"><input class="form-control require" name="carcass_finish[]" value="" id="carcass_finish'+incId+'"  type="hidden"> <input class="form-control require description commonadd-val" autocomplete="off" name="description[]" value="" id="description'+incId+'"  readonly="readonly" style="visibility:visible;" type="text" maxlength="255">');

            if(response.template == 1){
                el.parents('tr').find('td:nth-child(4)').html('<input class="form-control require commonadd-val" name="shutterwork_description[]" value="" id="shutterwork_description'+incId+'" readonly="readonly" style="visibility:visible" type="text" maxlength="255"><input class="form-control require" name="material[]" value="" id="material'+incId+'" type="hidden"><input class="form-control require" name="finish[]" value="" id="finish'+incId+'" type="hidden">');
                el.parents('tr').find('td:nth-child(5)').html('<input class="form-control require  commonadd-val" name="caracoss_description[]" value="" id="caracoss_description'+incId+'"  readonly="readonly" style="visibility:visible" type="text" maxlength="255"><input class="form-control require" name="shutterwork_material[]" value="" id="shutterwork_material'+incId+'"  type="hidden"><input class="form-control require" name="shutterwork_finish[]" value="" id="shutterwork_finish'+incId+'"  type="hidden"><input class="form-control require" name="carcass_material[]" value="" id="carcass_material'+incId+'"  type="hidden"><input class="form-control require" name="carcass_finish[]" value="" id="carcass_finish'+incId+'"  type="hidden"> <input class="form-control require description commonadd-val" autocomplete="off" name="description[]" value="" id="description'+incId+'"  readonly="readonly" style="visibility:hidden;height:0px;" type="text" maxlength="255">');
            }
        });
    })

    $(function () {
        $("body").find(".sub_worktype_selector").find(".qitem_table").find(".unit").attr("name","unit[]");
        $("body").find(".sub_worktype_selector").find(".qitem_table").find(".worktype_label").attr("name","worktype_label[]");
        $(".add_more_items").trigger("click");
    });
    </script>
