<?php

class SubcontractorbillController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		/* return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','getprojectbycompany','getquotationsbyproject','addbillitem','getNewItemView','exporttopdf','exporttoexcel','getItemsById','removeBillItem','getsubcontractorbyproject'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		); */
                $accessArr = array();
                $accessauthArr = array();
                $controller = Yii::app()->controller->id;
                if(isset(Yii::app()->user->menuall)){
                    if (array_key_exists($controller, Yii::app()->user->menuall)) {
                        $accessArr = Yii::app()->user->menuall[$controller];
                    } 
                }

                if(isset(Yii::app()->user->menuauth)){
                    if (array_key_exists($controller, Yii::app()->user->menuauth)) {
                        $accessauthArr = Yii::app()->user->menuauth[$controller];
                    }
                }
                $access_privlg = count($accessauthArr);
                return array(
                    array('allow', // allow all users to perform 'index' and 'view' actions
                        'actions' => $accessArr,                       
                        'users' => array('@'),
                        'expression' => "$access_privlg > 0",
                    ),
                    array('allow', // allow all users to perform 'index' and 'view' actions
                   
                   'actions' => array('updatebill','createquotationbill','ValidateBillNumber','getItemData','GetpaymentstageAmount','Getscquotationitems','Getscquotationbillnumber','BillnumberExistance','getAllQuotations','getAllBills','getAllSubContractorName','getAllproject','getAllSubQuotation','getSubContractorName'),
                    'users' => array('@'),
                    'expression' => "$access_privlg > 0",
                ),

                    array('allow', // allow admin user to perform 'admin' and 'delete' actions
                        'actions' => $accessauthArr,
                        'users' => array('@'),
                        'expression' => "$access_privlg > 0",
                    ),
                    array('deny',  // deny all users
                        'users'=>array('*'),
                    ),
                );
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
            $model              = $this->loadModel($id);
            $itemmodel          = new Subcontractorbillitem;
            $itemmodel->bill_id = $id;
            $this->render('view',array(
                'model'=>$model,
                'itemmodel'=>$itemmodel,
            ));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
        $model=new Subcontractorbill;

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);
        
		if(isset($_POST['Subcontractorbill']))
		{
            //echo "<pre>";print_r($_POST['Subcontractorbill']);exit;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $model->attributes      = $_POST['Subcontractorbill'];
                $model->date            = date("Y-m-d", strtotime($_POST['Subcontractorbill']['date']));
                $model->created_by      = Yii::app()->user->id;
                $model->created_date    = date("Y-m-d");
                $model->remarks =  isset($_POST['Subcontractorbill']['remarks'])?$_POST['Subcontractorbill']['remarks']:'';
                $model->requested_by =  isset($_POST['Subcontractorbill']['requested_by'])?$_POST['Subcontractorbill']['requested_by']:'';
                if($model->save()) {
                    if($model->stage_id !=""){
                        $stage_model = ScPaymentStage::model()->findByPk($model->stage_id); 
                        $stage_model->bill_status = "1";
                        if(!$stage_model->save()) {
                            throw new Exception('Error Occured');
                        }
                    }                    
                }else{
                    throw new Exception('Error Occured');
                }
                $transaction->commit();
                Yii::app()->user->setFlash('success', "Bill  added successfully!");
                $this->redirect(array('admin'));
            } catch (Exception $e) {
                $transaction->rollBack();
                Yii::app()->user->setFlash('error', "An error occurred!");                    
            } 
		}
		$this->render('create',array(
                    'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id) {
            $model = $this->loadModel($id);
            $itemmodel = new Subcontractorbillitem;
            $itemmodel->unsetAttributes();
            $itemmodel->bill_id = $id;

            // Uncomment the following line if AJAX validation is needed
            $this->performAjaxValidation($model);

            if (isset($_POST['Subcontractorbill'])) {
                $model->attributes = $_POST['Subcontractorbill'];
                $model->date = date("Y-m-d", strtotime($_POST['Subcontractorbill']['date']));
                $model->updated_date = date("Y-m-d");
                $model->amount = $_POST['Subcontractorbill']['total_amount'];
                $model->total_amount = $_POST['Subcontractorbill']['total_amount'];
                $model->remarks =  isset($_POST['Subcontractorbill']['remarks'])?$_POST['Subcontractorbill']['remarks']:'';
                $model->requested_by =  isset($_POST['Subcontractorbill']['requested_by'])?$_POST['Subcontractorbill']['requested_by']:'';
            
                if ($model->save()) {
                    if($model->stage_id !=""){
                        $stage_model = ScPaymentStage::model()->findByPk($model->stage_id); 
                        $stage_model->bill_status = "1";
                        $stage_model->save();                        
                    }
                    if($model->bill_type==2){
                        $items = $_POST['Subcontractorbill']['item'];                        
                        $transaction = Yii::app()->db->beginTransaction();
                        try {
                            foreach ($items as $key => $value) {
                                $sc_bill_id = $value['bill_item_id'];
                                $scmodel= Scbillitems::model()->findByPk($sc_bill_id); 
                                $scmodel->executed_quantity = $value['executed_quantity'];
                                $scmodel->completed_percentage = $value['completed_percentage'];                    
                                $scmodel->item_amount = $value['item_amount'];                    
                                $scmodel->updated_by      = Yii::app()->user->id;
                                $scmodel->updated_date    = date("Y-m-d");
                                if(!$scmodel->save()) {                        
                                    throw new Exception(print_r($scmodel->getErrors()));
                                }
                            }
                            $transaction->commit();                                
                        } catch (Exception $e) {
                            $transaction->rollBack();
                            Yii::app()->user->setFlash('error', "An error occurred!");  
                            $this->redirect(array('admin'));                  
                        }
                        
                    }
                    Yii::app()->user->setFlash('success', 'SC Bill Updated Successfully');
                    $this->redirect(array('admin')); 
                }
            }
            $this->render('update', array(
                'model' => $model,
                'itemmodel' => $itemmodel,
            ));
        }

    /**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{        
		$model = $this->loadModel($id);
        $stage_id = $model->stage_id;
        $tblpx = Yii::app()->db->tablePrefix;                
        $transaction = Yii::app()->db->beginTransaction();
        try {
            if (!$model->delete()) {
                $success_status = 0;
                throw new Exception(json_encode($model->getErrors()));
            } else {
                if($model->bill_type==1){
                    $newmodel = new JpLog('search');
                            $newmodel->log_data = json_encode( $model->attributes);
                            $newmodel->log_action = 2;
                            $newmodel->log_table = $tblpx . "subcontractorbill";
                            $newmodel->log_datetime = date('Y-m-d H:i:s');
                            $newmodel->log_primary_key= $id;
                            $newmodel->log_action_by = Yii::app()->user->id;
                            $newmodel->company_id = $model->company_id;
                            if ($newmodel->save()) {
                               
                            }
                $stagemodel = ScPaymentStage::model()->findByPk($stage_id);
                if($stagemodel){
                    $stagemodel->bill_status = '0';
                    if (!$stagemodel->save()) {
                        $success_status = 0;
                        throw new Exception(json_encode($stagemodel->getErrors()));
                    }
                    
                }
            }else{
                $scmodel=Yii::app()->db->createCommand("DELETE FROM {$tblpx}scbillitems WHERE bill_id = '" . $model->id . "'");
                if($scmodel){
                    if (!$scmodel->execute()) {
                        $success_status = 0;
                        throw new Exception(json_encode($scmodel->getErrors()));
                    }
                    
                }                
            }
                $success_status = 1;
            }
            $transaction->commit();
        } catch (Exception $error) {
            $transaction->rollBack();
            $success_status = 0;
        } finally {
            if ($success_status == 1) {
                echo json_encode(array('response' => 'success', 'msg' => 'Data Deleted Successfully'));
            } else {
                if ($error->errorInfo[1] == 1451) {
                    echo json_encode(array('response' => 'warning', 'msg' => 'Cannot Delete ! This record alredy in use.'));
                } else {
                    echo json_encode(array('response' => 'error', 'msg' => 'Some Error Occured'));
                }
            }
        }
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Subcontractorbill');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Subcontractorbill('search');
		$model->unsetAttributes(); 
        $quotation_no='';
        $bill_no='';
        $project='';
        $subcontractor="";
        if (isset($_GET['Subcontractorbill']) || isset($_GET['scquotation_no']) || isset($_GET['project_id']) || isset($_GET['date_from']) || isset($_GET['date_to']) || isset($_GET['Subcontractorbill']['company_id'])) {
           $quotation_no=isset($_GET['scquotation_no']) ? $_GET['scquotation_no'] : "";
           if($quotation_no=='-Select Qoutation-'){
            $quotation_no='';
           } 
           $subcontractor_name=isset($_GET['subcontractor_name']) ? $_GET['subcontractor_name'] : "";
           if($subcontractor_name=='Select subcontractor name'){
            $subcontractor_name='';
           } 
           $project=isset($_GET['project_id']) ? $_GET['project_id'] : "";
           if($project=='-Select Project-'){
            $project='';
           } 
          
           $bill_no=isset($_GET['Subcontractorbill']['bill_number']) ? $_GET['Subcontractorbill']['bill_number'] : "";
           if($bill_no=='Select Bills'){
             $bill_no='';
           } 
			$model->attributes = $_GET['Subcontractorbill'];
			$model->date_from = isset($_GET['date_from']) ? $_GET['date_from'] : "";
			$model->date_to = isset($_GET['date_to']) ? $_GET['date_to'] : "";
			$model->scquotation_no =  $quotation_no;
            $model->subcontractor_name =  $subcontractor_name;
			$model->project_id= $project;
            $model->bill_number =  $bill_no;
            
			$model->company_id = isset($_GET['Subcontractorbill']['company_id']) ? $_GET['Subcontractorbill']['company_id'] : "";
			$model->created_date_from = isset($_GET['created_date_from']) ? $_GET['created_date_from'] : "";
			
			$model->created_date_to = isset($_GET['created_date_to']) ? $_GET['created_date_to'] : "";
		} // clear any default values
		if(isset($_GET['Subcontractorbill']))
			$model->attributes=$_GET['Subcontractorbill'];
		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Subcontractorbill the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Subcontractorbill::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Subcontractorbill $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='subcontractorbill-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        public function actionGetprojectbycompany() {
            $company_id     = $_REQUEST["company_id"];
            $projectdata    = Projects::model()->findAll(array('select' => 'pid,name','condition' => "(FIND_IN_SET('".$company_id."', company_id))"));
            $option         = "<option value=''>Select Project</option>";
            foreach($projectdata as $data) {
                $option .= "<option value='".$data->pid."'>".$data->name."</option>";
            }
            echo $option;
        }
        public function actionGetsubcontractorbyproject() {
            $company_id     = $_REQUEST["company_id"];
            $project_id     = $_REQUEST["project_id"];
            $tblpx          = Yii::app()->db->tablePrefix;
            $subcontractor  = Yii::app()->db->createCommand("SELECT sc.subcontractor_id, sc.subcontractor_name FROM {$tblpx}subcontractor sc
                                LEFT JOIN {$tblpx}scquotation sq ON sc.subcontractor_id = sq.subcontractor_id
                                WHERE sq.project_id = '{$project_id}' AND sq.company_id = '{$company_id}' GROUP BY sc.subcontractor_id")->queryAll();
            $option         = "<option value=''>Select Subcontractor</option>";
            foreach($subcontractor as $data) {
                $option .= "<option value='".$data["subcontractor_id"]."'>".$data["subcontractor_name"]."</option>";
            }
            echo $option;
        }
        public function actionGetquotationsbyproject() {
            $company_id         = $_REQUEST["company_id"];
            $project_id         = $_REQUEST["project_id"];
            $subcontractor_id   = $_REQUEST["subcontractor_id"];
            $quotationdata      = Scquotation::model()->findAll(array('select' => 'scquotation_id,scquotation_no','condition' => "project_id = '".$project_id."' AND subcontractor_id = '".$subcontractor_id."' AND company_id = '".$company_id."'"));         
            $option             = "<option value=''>Select Quotation</option>";
            foreach($quotationdata as $data) {
                if(!empty($data->scquotation_no)) {
                    $option .= "<option value='".$data->scquotation_id."'>".$data->scquotation_no."</option>";
                }
                
            }
            echo $option;
        }
        public function actionAddbillitem() {
            $bill_id        = $_REQUEST["bill_id"];
            $quotation_id   = $_REQUEST["quotation_id"];
            $item_id        = $_REQUEST["item_id"];
            $billitem_id    = $_REQUEST["billitem_id"];
            $description    = $_REQUEST["description"];
            $item_type    = $_REQUEST["item_type"];
            $quantity    = $_REQUEST["quantity"];
            $unit    = $_REQUEST["unit"];
            $rate    = $_REQUEST["rate"];
            $amount         = $_REQUEST["amount"];
            $tax_slab       = $_REQUEST["tax_slab"];
            $sgstp          = $_REQUEST["sgstp"];
            $sgst           = $_REQUEST["sgst"];
            $cgstp          = $_REQUEST["cgstp"];
            $cgst           = $_REQUEST["cgst"];
            $igstp          = $_REQUEST["igstp"];
            $igst           = $_REQUEST["igst"];
            $total_tax      = $_REQUEST["total_tax"];
            $total_amount   = $_REQUEST["total_amount"];
            $tblpx          = Yii::app()->db->tablePrefix;
            $totalQuotation = Yii::app()->db->createCommand("SELECT IFNULL(item_amount, 0) FROM {$tblpx}scquotation_items WHERE item_id = {$item_id}")->queryScalar();

            $totalBilled    = Yii::app()->db->createCommand("SELECT SUM(IFNULL(total_amount, 0)) FROM {$tblpx}subcontractorbillitem WHERE quotationitem_id = {$item_id}")->queryScalar();
            
            if($billitem_id != "") {
                $model          = Subcontractorbillitem::model()->findByPk($billitem_id);
                $currentBilled  = Yii::app()->db->createCommand("SELECT SUM(IFNULL(total_amount, 0)) FROM {$tblpx}subcontractorbillitem WHERE id = {$billitem_id}")->queryScalar();
                
            } else {
                $model          = new Subcontractorbillitem;
                $currentBilled  = 0;
            }
            $newBilled      = ($totalBilled - $currentBilled) + $total_amount;           
                $model->bill_id             = $bill_id;
                $model->quotationitem_id    = $item_id;
                $model->description         = $description;
                $model->item_type           = $item_type;
                $model->item_quantity            = $quantity;
                $model->item_unit                = $unit;
                $model->item_rate                = $rate;
                $model->amount              = $amount;
                $model->tax_slab            = $tax_slab;
                $model->cgstp               = $cgstp;
                $model->cgst                = $cgst;
                $model->sgstp               = $sgstp;
                $model->sgst                = $sgst;
                $model->igstp               = $igstp;
                $model->igst                = $igst;
                $model->tax_amount          = $total_tax;
                $model->total_amount        = $total_amount;
                if($billitem_id == "")
                    $model->created_date    = date("Y-m-d");
                else
                    $model->updated_date    = date("Y-m-d");
                if($model->save()) {
                    $billmodel                  = Subcontractorbill::model()->findByPk($bill_id);
                    $itemdata                  = Yii::app()->db->createCommand("SELECT SUM(IFNULL(amount, 0)) as amountsum, SUM(IFNULL(tax_amount, 0)) as taxsum, SUM(IFNULL(total_amount, 0)) as totalsum FROM {$tblpx}subcontractorbillitem WHERE bill_id = {$bill_id}")->queryRow();
                    $billmodel->amount          = $itemdata["amountsum"];
                    $billmodel->tax_amount      = $itemdata["taxsum"];
                    $billmodel->total_amount    = $itemdata["totalsum"];
                    $billmodel->save();
                    $result["status"]   = 2;
                    $result["unbilled"] = "";
                } else {
                    $result["status"]   = 3;
                    $result["unbilled"] = "";
                }
            // }
            echo json_encode($result);
        }
        public function actionGetNewItemView() {
            $bill_id            = $_REQUEST["bill_id"];
            $model              = $this->loadModel($bill_id);
            $itemmodel          = new Subcontractorbillitem;
            $itemmodel->unsetAttributes();
            $itemmodel->bill_id = $bill_id;
            $result             = $this->widget('zii.widgets.CListView', array(
                                    'viewData' => array("amount" => $model->amount, "tax_amount" => $model->tax_amount, "total_amount" => $model->total_amount),
                                    'dataProvider'=>$itemmodel->search(),
                                    'itemView' => '_newitemview', 'template' => '<div class=""><div class=""><table cellpadding="10" class="table  list-view sorter">{items}</table></div>',
                                  ));
            return $result;
        }
        public function actionExporttopdf() {
            $this->logo = $this->realpath_logo;
            $id                 = $_REQUEST["id"];
            $model              = $this->loadModel($id);
            $itemmodel          = new Subcontractorbillitem;
            $itemmodel->bill_id = $id;
            $mPDF1 = Yii::app()->ePdf->mPDF();
            $mPDF1 = Yii::app()->ePdf->mPDF('', 'A4');
            $sql = 'SELECT template_name '
                . 'FROM jp_quotation_template '
                . 'WHERE status="1"';
        	$selectedtemplate =Yii::app()->db->createCommand($sql)->queryScalar();
        
        	$template = $this->getTemplate($selectedtemplate);
        	$mPDF1->SetHTMLHeader($template['header']);
        	//$mPDF1->SetHTMLFooter($template['footer']);
        	$mPDF1->AddPage('','', '', '', '', 0,0,40, 30, 10,0);
            $mPDF1->WriteHTML($this->renderPartial('subcontractorbillpdf', array(
                        'model'=>$model,
                        'itemmodel'=>$itemmodel,
                    ), true));
            $filename = "Subcontractor_Bill" . $model->bill_number;
            $mPDF1->Output($filename . '.pdf', 'D');
            }
        public function actionExporttoexcel() {
            $id                 = $_REQUEST["id"];
            $model              = $this->loadModel($id);
            $itemmodel          = Subcontractorbillitem::model()->findAll(array('condition'=>'bill_id="'.$id.'"'));
            $arraylabel         = array('COMPANY', 'PROJECT', 'SUB CONTRACTOR','QUOTATION', 'BILL NUMBER', 'DATE');
            $finaldata          = array();
            $i                  = 0;
            $finaldata[$i][]    = $model->company->name;
            $finaldata[$i][]    = Projects::model()->findByPk($model->quotation->project_id)->name;
            $finaldata[$i][]    = Subcontractor::model()->findByPk($model->quotation->subcontractor_id)->subcontractor_name;
            $finaldata[$i][]    = $model->quotation->scquotation_decription;
            $finaldata[$i][]    = $model->bill_number;
            $finaldata[$i][]    = date("d-m-Y", strtotime($model->date));
            $i++;
            $finaldata[$i][]    = ' ';
            $finaldata[$i][]    = ' ';
            $finaldata[$i][]    = ' ';
            $finaldata[$i][]    = ' ';
            $finaldata[$i][]    = ' ';
            $finaldata[$i][]    = ' ';
            $i++;
            $finaldata[$i][]    = 'Sl No';
            $finaldata[$i][]    = 'Description';
            $finaldata[$i][]    = 'Amount';
            $finaldata[$i][]    = 'Tax Slab (%)';
            $finaldata[$i][]    = 'SGST (%)';
            $finaldata[$i][]    = 'SGST';
            $finaldata[$i][]    = 'CGST (%)';
            $finaldata[$i][]    = 'CGST';
            $finaldata[$i][]    = 'IGST (%)';
            $finaldata[$i][]    = 'IGST';
            $finaldata[$i][]    = 'Total Tax';
            $finaldata[$i][]    = 'Total Amount';
            $i++;
            $j = 1;
            foreach($itemmodel as $item) {
                $finaldata[$i][]    = $j;
                $finaldata[$i][]    = ($item->description)?$item->description:" ";
                $finaldata[$i][]    = ($item->amount)?Controller::money_format_inr($item->amount,2):" ";
                $finaldata[$i][]    = ($item->tax_slab)?$item->tax_slab:" ";
                $finaldata[$i][]    = ($item->sgstp)?$item->sgstp:" ";
                $finaldata[$i][]    = ($item->sgst)?Controller::money_format_inr($item->sgst,2):" ";
                $finaldata[$i][]    = ($item->cgstp)?$item->cgstp:" ";
                $finaldata[$i][]    = ($item->cgst)?Controller::money_format_inr($item->cgst,2):" ";
                $finaldata[$i][]    = ($item->igstp)?$item->igstp:" ";
                $finaldata[$i][]    = ($item->igst)?Controller::money_format_inr($item->igst,2):" ";
                $finaldata[$i][]    = ($item->tax_amount)?Controller::money_format_inr($item->tax_amount,2):" ";
                $finaldata[$i][]    = ($item->total_amount)?Controller::money_format_inr($item->total_amount,2):" ";
                $i++;
                $j++;
            }
            $finaldata[$i][]    = ' ';
            $finaldata[$i][]    = ' ';
            $finaldata[$i][]    = ($model->amount)?Controller::money_format_inr($model->amount,2):" ";
            $finaldata[$i][]    = ' ';
            $finaldata[$i][]    = ' ';
            $finaldata[$i][]    = ' ';
            $finaldata[$i][]    = ' ';
            $finaldata[$i][]    = ' ';
            $finaldata[$i][]    = ' ';
            $finaldata[$i][]    = ' ';
            $finaldata[$i][]    = ($model->tax_amount)?Controller::money_format_inr($model->tax_amount,2):" ";
            $finaldata[$i][]    = ($model->total_amount)?Controller::money_format_inr($model->total_amount,2):" ";
            
            Yii::import('ext.ECSVExport');
            $csv = new ECSVExport($finaldata);
            $csv->setHeaders($arraylabel);
            $csv->setToAppend();
            $contentdata = $csv->toCSV();
            $csvfilename = 'Subcontractor_Bill' . $model->bill_number . '.csv';
            Yii::app()->getRequest()->sendFile($csvfilename, $contentdata, "text/csv", false);
            exit();
        }
        public function actionGetItemsById() {
            $item_id    = $_REQUEST["item_id"];
            $itemdata   = Subcontractorbillitem::model()->findByPk($item_id);
            $result["id"]               = $itemdata->id;
            $result["bill_id"]          = $itemdata->bill_id;
            $result["quotationitem_id"] = $itemdata->quotationitem_id;
            $result["description"]      = $itemdata->description;  
            $result["item_type"]     = $itemdata->item_type;
            $result["item_quantity"]     = $itemdata->item_quantity;
            $result["item_unit"]     = $itemdata->item_unit;
            $result["item_rate"]     = $itemdata->item_rate;
            $result["amount"]           = $itemdata->amount;
            $result["tax_slab"]         = $itemdata->tax_slab;
            $result["cgstp"]            = $itemdata->cgstp;
            $result["cgst"]             = $itemdata->cgst;
            $result["sgstp"]            = $itemdata->sgstp;
            $result["sgst"]             = $itemdata->sgst;
            $result["igstp"]            = $itemdata->igstp;
            $result["igst"]             = $itemdata->igst;
            $result["tax_amount"]       = $itemdata->tax_amount;
            $result["total_amount"]     = $itemdata->total_amount;
            echo json_encode($result);
        }
        public function actionRemoveBillItem() {
            $item_id    = $_REQUEST["item_id"];
            $itemmodel  = Subcontractorbillitem::model()->findByPk($item_id);
            $bill_id    = $itemmodel->bill_id;
            $tblpx      = Yii::app()->db->tablePrefix;
            if($itemmodel->delete()) {
                $billmodel                  = Subcontractorbill::model()->findByPk($bill_id);
                $itemdata                  = Yii::app()->db->createCommand("SELECT SUM(IFNULL(amount, 0)) as amountsum, SUM(IFNULL(tax_amount, 0)) as taxsum, SUM(IFNULL(total_amount, 0)) as totalsum FROM {$tblpx}subcontractorbillitem WHERE bill_id = {$bill_id}")->queryRow();
                $billmodel->amount          = $itemdata["amountsum"];
                $billmodel->tax_amount      = $itemdata["taxsum"];
                $billmodel->total_amount    = $itemdata["totalsum"];
                $billmodel->save();
                echo 1;
            } else {
                echo 2;
            }
        }

        public function actioncreateQuotationBill($quotation_id,$q_stage_id){           
            $model=new Subcontractorbill;            
            $quotation_model= '';
            $quotation_items = '';
            $this->performAjaxValidation($model);
            $bill_number = "";
            if(isset($quotation_id) && isset($q_stage_id)){  
                $quotation_model = Scquotation::model()->findByPk($quotation_id);              
                $quotation_stages = ScPaymentStage::model()->find(array('condition'=>'quotation_id = '.$quotation_id.' AND id = '.$q_stage_id));
                $bill_number =$this->getScBillNumber($quotation_id);                
            }        
            if(isset($_POST['Subcontractorbill']))
		    {    
                $transaction = Yii::app()->db->beginTransaction();
                try {
                    $model->attributes      = $_POST['Subcontractorbill'];
                    $model->date            = date("Y-m-d", strtotime($_POST['Subcontractorbill']['date']));
                    $model->created_by      = Yii::app()->user->id;
                    $model->created_date    = date("Y-m-d");                           
                    $total_amount = ($_POST['Subcontractorbill']['stage_id'] !="") ?$_POST['total_amount']:$model->amount + $model->tax_amount;
                    $model->total_amount = $total_amount;
                    
                    $post_datas = $_POST;
                    if($model->save()) {  
                        if($model->stage_id !=""){
                            $stage_model = ScPaymentStage::model()->findByPk($model->stage_id); 
                            $stage_model->bill_status = "1";
                            if(!$stage_model->save()) {
                                throw new Exception('Error Occured');
                            }
                        }
                        $this->addbillitems($model,$post_datas);
                    }                     
                    $transaction->commit();
                    Yii::app()->user->setFlash('success', "Bill  added successfully!");
                  $this->redirect(array('admin'));
                } catch (Exception $e) {
                    $transaction->rollBack();
                    Yii::app()->user->setFlash('error', "An error occurred!");                    
                }                                
                   
            }
		   
            $this->render('_quotation_bill',array(
                'quotation_model'=> $quotation_model,
                'quotation_stages'=>$quotation_stages,
                'model'=>$model,
                'quotation_items'=>$quotation_items,
                'bill_number'=>$bill_number
            ));

        }

        public function addbillitems($model,$post_data){
         
            if(!empty($post_data['item_ids'])){
                foreach($post_data['item_ids'] as $key => $value){
                    $item_model = new Subcontractorbillitem;
                    $item_model->bill_id = $model['id'];
                    $item_model->quotationitem_id = $value;
                    $item_model->description = $post_data['description'][$key];
                    $item_model->amount = $post_data['amount'][$key];
                    $item_model->tax_slab = $post_data['tax_slab'][$key]; 
                    $item_model->cgstp = $post_data['cgstpercent'][$key];
                    $item_model->cgst = $post_data['cgst'][$key];
                    $item_model->sgstp = $post_data['sgstpercent'][$key];
                    $item_model->sgst = $post_data['sgst'][$key];
                    $item_model->igstp = $post_data['igstpercent'][$key];
                    $item_model->igst = $post_data['igst'][$key];
                    $item_model->tax_amount = $post_data['tax_total'][$key];
                    $item_model->total_amount = $post_data['amount_total'][$key];
                    $item_model->created_date = date('Y-m-d');
                    $item_model->save();                   
                }
                return 1;
            }else{
                return 2;
            }
            
        }
        public function actionValidateBillNumber() {
            $billNo   = $_POST["billno"];
            $company_id = $_POST['company_id'];
            $tblpx    = Yii::app()->db->tablePrefix;
            $billData = Yii::app()->db->createCommand("SELECT * FROM ".$tblpx."subcontractorbill WHERE bill_number = '".$billNo."'  and company_id=".$company_id."")->queryAll();
            if($billData) {
                echo 1;
            } else {
                echo 2;
            }
        }
        
        public function actiongetItemData() {
            
            if (!empty($_POST["item_id"])) {               
                $tblpx = Yii::app()->db->tablePrefix;
                $sql = Yii::app()->db->createCommand("SELECT * FROM ".$tblpx."scquotation_items WHERE item_id = '".$_POST["item_id"]."'")->queryAll();
                if (!empty($sql)) {
                    foreach($sql as $itemsql){                       
                        $result['item_type'] = $itemsql['item_type'];
                        $result['quantity'] = $itemsql['item_quantity'];
                        $result['unit'] = $itemsql['item_unit'];
                        $result['rate'] = $itemsql['item_rate'];
                        $result['amount'] = $itemsql['item_amount'];
                        $result['status'] = 'success';
                    }
                    
                } else {
                    $result['status'] = 'error';
                }
                echo json_encode($result);
            }
        }

        public function actionGetpaymentstagebyquotation() {
            $bill_number='';
            $company_id         = $_REQUEST["company_id"];
            $project_id         = $_REQUEST["project_id"];
            $subcontractor_id   = $_REQUEST["subcontractor_id"];
            $quotation_id       = $_REQUEST["quotation_id"];
            $tblpx = Yii::app()->db->tablePrefix;
            $user = Users::model()->findByPk(Yii::app()->user->id);
            $company = Company::model()->findByPk($company_id);
            $arrVal = explode(',', $user->company_id);
            $newQuery = "";
            $default_sqno= '';
            foreach ($arrVal as $arr) {
                if ($newQuery)
                    $newQuery .= ' OR';
                $newQuery .= " FIND_IN_SET('" . $arr . "', {$tblpx}expense_type.company_id)";
            }
            if( isset($company->sc_bill_format) &&   ($company->sc_bill_format ==1) ){
                if( isset($company->sc_quotation_format) && !empty($company->sc_quotation_format)){
                    $scquotation =Scquotation::model()->findByPk($quotation_id);
                    $scquotation_no = $scquotation->scquotation_no;
                    $seq_no = 1;
                    if(!empty($scquotation_no)){
                        $sql = "SELECT * from ".$tblpx."subcontractorbill  WHERE `bill_number` LIKE '$scquotation_no%'  ORDER BY `bill_number` DESC LIMIT 1;" ;
                        $previous_data = Yii::app()->db->createCommand($sql)->queryRow();
                        if(!empty($previous_data)){
                            $previous_scq_no = $previous_data['bill_number'];
				            $prefix_arr = explode('/', $previous_scq_no);
                            $count = count($prefix_arr);
                            $previous_prefix_seq_no = $prefix_arr[$count -1];
                            $seq_no=$previous_prefix_seq_no+1;
                           
                        }
                    }
                    $digit = strlen((string) $seq_no);
                    if ($digit == 1) {
                        $scq_bill_no ='00' .$seq_no;
                    } else if ($digit == 2) {
                        $scq_bill_no = '0' . $seq_no;
                    } else {
                        $scq_bill_no =$seq_no;
                    }
                    $bill_number=$scquotation_no.'/'.$scq_bill_no;
                }
            }else if($this->getActiveTemplate() == 'TYPE-4'){
                $bill_number =$this->getScBillNumber($quotation_id);
            }
            
            $stage_id_data = array();
            $stageArray = Yii::app()->db->createCommand("SELECT stage_id FROM `jp_subcontractorbill`")->queryAll();
            
            
            foreach($stageArray as $arr) {   
                if(!is_null($arr['stage_id'])){
                    array_push($stage_id_data, $arr['stage_id']);
                }
                
            }
            $condition = '';
            if(!empty($stage_id_data)){
                $stage_ids =  implode(',', $stage_id_data);
                $condition = ' AND id NOT IN('.$stage_ids.')';
            }
           
            
            $stagedata  = ScPaymentStage::model()->findAll(array('select' => 'id,payment_stage', 'condition' => "quotation_id = '" . $_REQUEST["quotation_id"] . "' $condition "));           
                     
            $option             = "<option value=''>Select Stage</option>";
            foreach($stagedata as $data) {
                if(!empty($data->payment_stage)) {
                    $option .= "<option value='".$data->id."'>".$data->payment_stage."</option>";
                }
                
            }
            echo json_encode(array('stageData'=>$option,'bill_number'=>$bill_number));
        }

        public function actionGetpaymentstageAmount() {
            $quotation_id       = $_REQUEST["quotation_id"];
            $stage_id           = $_REQUEST["stage_id"];
            $stage_amount       = 0;

            if($stage_id !=""){
                $stagedata  = ScPaymentStage::model()->findByPk($stage_id);
                $quotation_model = Scquotation::model()->findByPk($quotation_id);
                $quotation_amount =$quotation_model->getTotalQuotationAmount($quotation_id) ;
                $stage_amount = $quotation_amount * ($stagedata['stage_percent'] / 100);

            }
             
            echo $stage_amount;
        }

        public function getScBillNumber($quotation_id){
            $sc_bill_no = "";            
            $activeProjectTemplate = $this->getActiveTemplate();
            if ($activeProjectTemplate == 'TYPE-4') {
                $quotation_no = Scquotation::model()->findByPK($quotation_id);
                $sql = "SELECT count(*)  FROM `jp_subcontractorbill` "
                    . " WHERE `scquotation_id` =".$quotation_id;  
                $counter = Yii::app()->db->createCommand($sql)->queryScalar();

                if($counter == 0){
                    $counter_no = str_pad($counter+1,2,"0",STR_PAD_LEFT);            
                    $sc_bill_no = $quotation_no['scquotation_no']."-".$counter_no;
                }else{
                    $sql = "SELECT bill_number FROM `jp_subcontractorbill` "
                        . " WHERE `scquotation_id` =".$quotation_id
                        . " ORDER BY id DESC LIMIT 1";
                    $last_sc_bill_no = Yii::app()->db->createCommand($sql)->queryScalar();
                
                    $scArray = explode('-',$last_sc_bill_no);	                    
                    $next_qtn = str_pad($scArray[2]+1,2,"0",STR_PAD_LEFT);
                    $sc_bill_no = $quotation_no['scquotation_no']."-".$next_qtn;	
                }                                       
            }

            return $sc_bill_no;
        }
        /**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreatewithItem($id='')
	{
        $model=new Subcontractorbill;
        $update=0;
		$this->performAjaxValidation($model);

		if(isset($_POST['Subcontractorbill']))
		{
            $transaction = Yii::app()->db->beginTransaction();
            $items = $_POST['Subcontractorbill']['item'];
            $model->scquotation_id      = $_POST['Subcontractorbill']['scquotation_id'];
            $model->bill_number      = $_POST['Subcontractorbill']['bill_number'];
            $model->company_id      = $_POST['Subcontractorbill']['company_id'];
            $model->date            = date("Y-m-d", strtotime($_POST['Subcontractorbill']['date']));
            $model->created_by      = Yii::app()->user->id;
            $model->created_date    = date("Y-m-d"); 
            $model->total_amount =  $_POST['Subcontractorbill']['bill_sum'];
            $model->remarks =  isset($_POST['Subcontractorbill']['remarks'])?$_POST['Subcontractorbill']['remarks']:'';
            $model->requested_by =  isset($_POST['Subcontractorbill']['requested_by'])?$_POST['Subcontractorbill']['requested_by']:'';
            $model->bill_type = 2;
            try {
                if($model->save()) {                    
                
                        foreach ($items as $key => $value) {
                            //echo '<pre>';print_r($value);exit;
                            $scmodel=new Scbillitems;
                            $scmodel->attributes      = $value;                    
                            $scmodel->scquotation_id = $_POST['Subcontractorbill']['scquotation_id'];
                            $scmodel->bill_number = $_POST['Subcontractorbill']['bill_number']; 
                            $scmodel->item_quantity =   $value['item_quantity'];
                            $scmodel->executed_quantity =   $value['executed_quantity'];                 
                            $scmodel->created_by      = Yii::app()->user->id;
                            $scmodel->created_date    = date("Y-m-d");
                            $scmodel->bill_id = $model->id;
                            if(!$scmodel->save()) {                        
                                throw new Exception(print_r($scmodel->getErrors()));
                            }
                        }
                }else{
                    throw new Exception(print_r($model->getErrors()));
                }
                $transaction->commit(); 
                Yii::app()->user->setFlash('success', "Bill  added successfully!");
            } catch (Exception $e) {
                $transaction->rollBack();
                Yii::app()->user->setFlash('error', "An error occurred!");                    
            }
            $this->redirect(array('admin'));
        }
		$this->render('create_with_item',array(
            'is_update'=>$update,
            'model'=>$model,
		));
	}
    public function actionGetscquotationitems(){
        $scquotation_id='';
        $is_update ='';
        if( isset( $_REQUEST["scquotation_id"]) && !empty($_REQUEST["scquotation_id"]) ){
            $scquotation_id         = $_REQUEST["scquotation_id"];  
        }
        if( isset( $_REQUEST["scquotation_id"]) && !empty($_REQUEST["scquotation_id"]) ){
            $is_update         = $_REQUEST["is_update"]; 
        }
        
        if($scquotation_id!=""){
            $bill_number =$this->getScBillNumber($scquotation_id);
        }
        $model     = Scquotation::model()->find(array("condition" => "scquotation_id = '$scquotation_id'"));
        $item_model = ScquotationItems::model()->findAll('scquotation_id=' .
        $scquotation_id, array('order' => 'item_id DESC'));
        if($is_update){
            $bill_id         = $_REQUEST["bill_id"];
            $model     = Subcontractorbill::model()->find(array("condition" => "scquotation_id = '$scquotation_id'"));
            $item_model = Scbillitems::model()->findAll('bill_id=' .
            $bill_id, array('order' => 'item_id DESC'));  
        }
        $quotation_categories = ScQuotationItemCategory::model()->findAll('sc_quotaion_id=' .
        $scquotation_id, array('order' => 'id DESC'));
        $view_datas = Controller::setSCQuotationsList($item_model, $model, $quotation_categories);
        //include billed 
        $billed_qty=0;
        $billing_pending_quantity= 0;
        for($l=0;$l<count($view_datas);$l++){
            $view_data[$l]['category_details']=$view_datas[$l]['category_details'];
            for($i=0;$i<count($view_datas[$l]['items_list']);$i++){
               
                $item_id = $view_datas[$l]['items_list'][$i]['item_id'];                           
                $tblpx = Yii::app()->db->tablePrefix;
                if(!$is_update){
                    if($view_datas[$l]['items_list'][$i]['item_type']==1){
                        $billed_qty =Yii::app()->db->createCommand("SELECT SUM(IFNULL(executed_quantity, 0)) FROM {$tblpx}scbillitems WHERE item_id = {$item_id}")->queryScalar();
                        $billed_qty=($billed_qty >0)? $billed_qty:0;
                        $remaining_qty=$view_datas[$l]['items_list'][$i]['item_quantity']- $billed_qty;
                    
                        $view_datas[$l]['items_list'][$i]['item_amount']=$remaining_qty*$view_datas[$l]['items_list'][$i]['item_rate'];
                    
                    }else{
                        $remaining_qty=1- Yii::app()->db->createCommand("SELECT COUNT(id) FROM {$tblpx}scbillitems WHERE item_id = {$item_id}")->queryScalar();
                    }
                }else{
                    $orginal_quatation_quantity =Yii::app()->db->createCommand("SELECT item_quantity FROM {$tblpx}scquotation_items WHERE item_id = {$item_id}" )->queryScalar();
                    $view_datas[$l]['items_list'][$i]['item_quantity'] = $orginal_quatation_quantity;
                    $billed_qty =Yii::app()->db->createCommand("SELECT SUM(IFNULL(executed_quantity, 0)) FROM {$tblpx}scbillitems WHERE item_id = {$item_id} AND bill_id !={$bill_id}" )->queryScalar();
                    $billed_qty=($billed_qty >0)? $billed_qty:0;
                    $remaining_qty =$view_datas[$l]['items_list'][$i]['item_quantity']-$billed_qty;
                }
                $billing_pending_quantity =$billing_pending_quantity+$remaining_qty;
                $view_datas[$l]['items_list'][$i]['billed_quantity'] =$billed_qty;
                $view_datas[$l]['items_list'][$i]['remaining_quantity'] = $remaining_qty;
                $view_data[$l]['items_list']=$view_datas[$l]['items_list'];
           }
           
        }
        echo $this->renderPartial('_view_itemlist', array(
                'bill_number' => $bill_number,
                'view_datas' => $view_data,
                'model' => $model,
                'is_update' => $is_update,
                'billing_pending_quantity' => $billing_pending_quantity,
    
            ));
            
    }
    public function actionGetscquotationbillnumber(){
        $bill_number='';
        $company_id         = $_REQUEST["company_id"];
        $project_id         = $_REQUEST["project_id"];
        $subcontractor_id   = $_REQUEST["subcontractor_id"];
        $quotation_id         = $_REQUEST["scquotation_id"];
        $tblpx = Yii::app()->db->tablePrefix;
            $user = Users::model()->findByPk(Yii::app()->user->id);
            $company = Company::model()->findByPk($company_id);
            $arrVal = explode(',', $user->company_id);
            $newQuery = "";
            $default_sqno= '';
            foreach ($arrVal as $arr) {
                if ($newQuery)
                    $newQuery .= ' OR';
                $newQuery .= " FIND_IN_SET('" . $arr . "', {$tblpx}expense_type.company_id)";
            }
            if(isset($company->sc_bill_format ) && ($company->sc_bill_format ==1)){
                if( isset($company->sc_quotation_format) && !empty($company->sc_quotation_format)){
                    $scquotation =Scquotation::model()->findByPk($quotation_id);
                    $scquotation_no = $scquotation->scquotation_no;
                    $seq_no = 1;
                    if(!empty($scquotation_no)){
                        $sql = "SELECT * from ".$tblpx."subcontractorbill  WHERE `bill_number` LIKE '$scquotation_no%'  ORDER BY `bill_number` DESC LIMIT 1;" ;
                        $previous_data = Yii::app()->db->createCommand($sql)->queryRow();
                        if(!empty($previous_data)){
                            $previous_scq_no = $previous_data['bill_number'];
				            $prefix_arr = explode('/', $previous_scq_no);
                            $count = count($prefix_arr);
                            $previous_prefix_seq_no = $prefix_arr[$count -1];
                            $seq_no=$previous_prefix_seq_no+1;
                           
                        }
                    }
                    $digit = strlen((string) $seq_no);
                    if ($digit == 1) {
                        $scq_bill_no ='00' .$seq_no;
                    } else if ($digit == 2) {
                        $scq_bill_no = '0' . $seq_no;
                    } else {
                        $scq_bill_no =$seq_no;
                    }
                    $bill_number=$scquotation_no.'/'.$scq_bill_no;
                }
            }else if($this->getActiveTemplate() == 'TYPE-4'){
                $bill_number =$this->getScBillNumber($quotation_id);
            }
        echo $bill_number;
    }

    public function actionBillnumberExistance(){
        $bill_number= isset($_POST['bill_number'])?$_POST['bill_number']:'';
        $count = 0;
        if($bill_number!=""){
            $count = Subcontractorbill::model()->countByAttributes(array(
                'bill_number'=> $bill_number
            ));
        }
        echo $count;exit;
    }
    public function actiongetAllQuotations(){
        $company_id='';
        $where='';
        $tblpx = Yii::app()->db->tablePrefix;
        $quotationOpt="";
        if(isset($_GET['company_id'])){
            $company_id=$_GET['company_id'];
            $where .= "company_id ='". $company_id."'";
        }
        $quotationOpt.="<option val=''>-Select Qoutation-</option>";
        $qoutation_data = Yii::app()->db->createCommand("SELECT scquotation_id,scquotation_no FROM " . $tblpx . "scquotation WHERE (" . $where . ") AND approve_status = 'Yes'  ORDER BY scquotation_id desc")->queryAll();
        //echo "<pre>";print_r($qoutation_data);exit;
        if(!empty($qoutation_data)){
            foreach($qoutation_data as $qoutation){
                $quotationOpt.="<option val='".$qoutation['scquotation_no']."'>".$qoutation['scquotation_no']."</option>";
            }
        }else{
            $quotationOpt.="<option val=''>No data Available  </option>";
        }
        echo json_encode(['quotations'=>$quotationOpt]);

    }
    public function actiongetAllBills(){
        $quotation_no='';
        $where='';
        $tblpx = Yii::app()->db->tablePrefix;
        $billOpt="";
        if(isset($_GET['quotation_no'])){
            $quotation_no=$_GET['quotation_no'];
            $where .= "sc.scquotation_no ='". $quotation_no."'";
        }
        $billOpt.="<option val=''></option>";
        $qoutation_data = Yii::app()->db->createCommand("SELECT b.id,b.bill_number FROM " . $tblpx . "subcontractorbill b LEFT JOIN " . $tblpx . "scquotation sc ON b.scquotation_id= sc.scquotation_id WHERE (" . $where . ") ORDER BY b.id desc")->queryAll();
        //echo "<pre>";print_r($qoutation_data);exit;
        if(!empty($qoutation_data)){
            foreach($qoutation_data as $qoutation){
                $billOpt.="<option val='".$qoutation['bill_number']."'>".$qoutation['bill_number']."</option>";
            }
        }else{
            $billOpt.="<option val=''>No data Available  </option>";
        }
        echo json_encode(['bills'=>$billOpt]);

    }
    public function actiongetAllBillsDynamic(){
        $quotation_no='';
        $where='';
        $company_id="";
        $tblpx = Yii::app()->db->tablePrefix;
        $billOpt="";
        $company_id=$_GET['company_id'];
        if(isset($_GET['quotation_no']) && isset($_GET['company_id'])){
            $quotation_no=$_GET['quotation_no'];
            $company_id=$_GET['company_id'];
            $where .= " sc.scquotation_no ='". $quotation_no."' AND  sc.company_id ='". $company_id."'";
        }elseif(isset($_GET['quotation_no'])){
            $quotation_no=$_GET['quotation_no'];
            $where .= " sc.scquotation_no ='". $quotation_no."'";
        }elseif(isset($_GET['company_id'])){
            $company_id=$_GET['company_id'];
            $where .= " sc.company_id ='". $company_id."'";
        }

        $billOpt.="<option val=''></option>";
        $qoutation_data = Yii::app()->db->createCommand("SELECT b.id,b.bill_number FROM " . $tblpx . "subcontractorbill b LEFT JOIN " . $tblpx . "scquotation sc ON b.scquotation_id= sc.scquotation_id WHERE (" . $where . ") ORDER BY b.id desc")->queryAll();
        //echo "<pre>";print_r($qoutation_data);exit;
        if(!empty($qoutation_data)){
            foreach($qoutation_data as $qoutation){
                $billOpt.="<option val='".$qoutation['bill_number']."'>".$qoutation['bill_number']."</option>";
            }
        }else{
            $billOpt.="<option val=''>No data Available  </option>";
        }
        echo json_encode(['bills'=>$billOpt]);

    }
    public function actiongetAllproject() {
        $company_id = '';
        $where = '';
        $tblpx = Yii::app()->db->tablePrefix;
        $projectOpt = "";
        $subcontractorOpt = "";
        $subcontractor_where = '';
    
        if (isset($_GET['company_id'])) {
            $company_id = $_GET['company_id'];
            $where .= "FIND_IN_SET('" . $company_id . "', company_id)";
            $subcontractor_where .= "FIND_IN_SET('" . $company_id . "', company_id)";
        }
    
        $projectOpt .= "<option val=''>-Select Project-</option>";
        
        // Ensure project_id is numeric and properly passed
        $project_data = Yii::app()->db->createCommand("
            SELECT pid, name 
            FROM " . $tblpx . "projects 
            WHERE (" . $where . ") 
            ORDER BY pid DESC
        ")->queryAll();
        
        if (!empty($project_data)) {
            foreach ($project_data as $project) {
                $projectOpt .= "<option value=" . $project['pid'] . ">" . $project['name'] . "</option>";
            }
        } else {
            $projectOpt .= "<option value=''>No data Available</option>";
        }
    
        $subcontractorOpt .= "<option value=''>-Select Subcontractors-</option>";
        
        // Query subcontractors based on company_id
        $subcontractor_data = Yii::app()->db->createCommand("
            SELECT * 
            FROM " . $tblpx . "subcontractor 
            WHERE " . $subcontractor_where . " 
            ORDER BY subcontractor_id DESC
        ")->queryAll();
    
        if (!empty($subcontractor_data)) {
            foreach ($subcontractor_data as $subcontractor) {
                $subcontractorOpt .= "<option value='" . $subcontractor['subcontractor_name'] . "'>" . $subcontractor['subcontractor_name'] . "</option>";
            }
        } else {
            $subcontractorOpt .= "<option value=''>No data Available</option>";
        }
    
        // Return the project options and subcontractor options as JSON
        echo json_encode([
            'projects' => $projectOpt,
            'subcontractors' => $subcontractorOpt
        ]);
    }
    
    public function actiongetSubContractorName($project_id)
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $subcontractorOpt = "<option value=''>Select subcontractor name</option>";

        if (isset($_GET['project_id'])) {
            $project_id = $_GET['project_id'];
            $subcontractor_data = Yii::app()->db->createCommand("
                SELECT DISTINCT sc.subcontractor_name
                FROM " . $tblpx . "subcontractor sc
                JOIN " . $tblpx . "scquotation scq ON scq.subcontractor_id = sc.subcontractor_id
                WHERE scq.project_id = :project_id
                ORDER BY sc.subcontractor_name ASC
            ")->bindParam(':project_id', $project_id, PDO::PARAM_INT)->queryAll();

            if (!empty($subcontractor_data)) {
                foreach ($subcontractor_data as $subcontractor) {
                    $subcontractorOpt .= "<option value='" . $subcontractor['subcontractor_name'] . "'>" . $subcontractor['subcontractor_name'] . "</option>";
                }
            } else {
                $subcontractorOpt .= "<option value=''>No data Available</option>";
            }

            echo json_encode([
                'subcontractors' => $subcontractorOpt
            ]);
        } else {
            echo json_encode([
                'subcontractors' => "<option value=''>No project selected</option>"
            ]);
        }
    }
    public function actiongetAllSubContractorName($company_id){
        $where='';
        $subcontractor_where='';
        if (isset($_GET['company_id'])) {
            $company_id = $_GET['company_id'];
            $where .= "company_id ='" . $company_id . "'";
            $subcontractor_where .= "FIND_IN_SET('" . $company_id . "', company_id)";
        }
       
        $subcontractor_data = Yii::app()->db->createCommand("
        SELECT * 
        FROM " . $tblpx . "subcontractor 
        WHERE " . $subcontractor_where . " 
        ORDER BY subcontractor_id DESC
    ")->queryAll();

    if (!empty($subcontractor_data)) {
        foreach ($subcontractor_data as $subcontractor) {
            $subcontractorOpt .= "<option value='" . $subcontractor['subcontractor_name'] . "'>" . $subcontractor['subcontractor_name'] . "</option>";
        }
    } else {
        $subcontractorOpt .= "<option value=''>No data Available</option>";
    }

    // Return the project options and subcontractor options as JSON
    echo json_encode([
        
        'subcontractors' => $subcontractorOpt
    ]);
    }
    public function actiongetAllSubQuotation(){
        $subcontractor_name='';
        $where='';
        $tblpx = Yii::app()->db->tablePrefix;
        $quotationOpt="";
        if(isset($_GET['subcontractor_name'])){
            $subcontractor_name=$_GET['subcontractor_name'];
            $subcontractor = Subcontractor::model()->findByAttributes(array('subcontractor_name' => $subcontractor_name));
            $subcontractor_id = isset($subcontractor) ? $subcontractor->subcontractor_id : null;   
             if(!empty($subcontractor_id)) {
                $where .= "AND subcontractor_id ='". $subcontractor_id."'";
             }       
            
        }
    
        $quotationOpt.="<option val=''>-Select Qoutation-</option>";
        $qoutation_data = Yii::app()->db->createCommand("SELECT scquotation_id,scquotation_no FROM " . $tblpx . "scquotation WHERE  approve_status = 'Yes' " . $where . " ORDER BY scquotation_id desc")->queryAll();

        if(!empty($qoutation_data)){
            foreach($qoutation_data as $qoutation){
                $quotationOpt.="<option val='".$qoutation['scquotation_no']."'>".$qoutation['scquotation_no']."</option>";
            }
        }else{
            $quotationOpt.="<option val=''>No data Available  </option>";
        }
        echo json_encode(['quotations'=>$quotationOpt]);
    }


}
