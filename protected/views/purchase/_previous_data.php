<?php 
    $hsn_code = NULL;
    if($type == "O"){
        $hsn_code = $specification['hsn_code'] ;
    }
    ?>
<div class="alert alert-success get_prevdata alert-dismissible fade in" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
    <div id="parent" style="overflow: auto;">
        <table style="width:100%;" id="pre_fixtable">
            <thead>
                <tr>
                    <th class="text-center">Sl.No</th>
                    <th class="text-center">Vendor</th> 
                    <?php 
                    if($type == "O"){
                    ?>
                    <th class="text-center">Project</th>
                    <?php } ?>
                    <th class="text-center">Bill No</th>
                    <th class="text-center">Purchase No</th>
                    <th class="text-center">Bill Date</th>
                    <?php 
                    if($type == "A"){
                    ?>
                    <th class="text-center">Length</th> 
                    <?php } 
                    if($type == "G"){
                    ?>
                    <th class="text-center">Width</th> 
                    <th class="text-center">Height</th> 
                    <?php } ?>
                    <th class="text-center">Quantity</th>
                    <th class="text-center">Unit</th>
                    <?php 
                    if($type == "O"){                       
                        ?>
                        <th class="text-center">HSN code</th>
                        <?php } ?>
                    <th class="text-center">Rate</th>
                    <th class="text-center">Discount(%)</th>
                    <th class="text-center">Tax Slab</th>
                    <th class="text-center">Amount</th>
                </tr>
            </thead>           
            <tbody>
                <tr>
                    <td colspan="9">
                        <p style="margin: 0px;  font-size: 13px;"><?php echo ucwords($category['category_name']) . $brand . ' - ' . ucwords($specification['specification']) ." Previous ".count($data)?> Purchase Rate</p>
                    </td>
                </tr>	
                <?php 
                $total_po_quantity = 0;
                foreach ($data as $key => $value) {
                    
                    $vendorName = "";
                    $projectName = "";
                    if ($value['vendor_id'] != "") {
                        $vendor = Vendors::model()->findByPk($value['vendor_id']);
                        $vendorName  = $vendor->name;
                    } 
                    if ($value['project_id'] != "") {
                    $project = Projects::model()->findByPk($value['project_id']);
                    $projectName = $project->name;
                    }
                    
                ?>

                <tr class="getprevious" style="cursor:pointer;" 
                    data-id="<?php echo  $specification['id'] . ',' . $value['billitem_quantity'] . ','
                            . '' . $value['billitem_unit'] . ',' . $value['billitem_rate'] . ','
                            . '' . $value['billitem_amount'] . ','
                            . '' . $value['billitem_length'] . ','
                            . '' . $value['remark'] . ',' . $value['discount_amount'] . ''
                            . ',' . $value['discount_percentage'] . ','
                            . '' . $value['tax_slab'].','
                            . '' . $value['sgst_amount'] . ','
                            . '' . $value['sgst_percentage'] . ','
                            . '' . $value['cgst_amount'] . ','
                            . '' . $value['cgst_percentage'] . ','
                            . '' . $value['igst_amount'] . ','
                            . '' . $value['igst_percentage'] . ','
                            . '' .$value['hsn_code'].','
                            . '' .$value['billitem_width'].','
                            . '' .$value['billitem_height'].'' ?>">
                    <td class="text-center"><?php echo ($key + 1) ;?></td>
                    <td class="text-center"><?php echo $vendorName;?></td>
                    <?php 
                    if($type == "O"){
                    ?>
                    <td class="text-center"><?php echo $projectName;?></td>
                    <?php } ?>
                    <td class="text-center"><?php echo $value['bill_number']; ?></td>
                    <td class="text-center"><?php echo $value['purchase_no']; ?></td>
                    <td class="text-center"><?php echo date("Y-m-d", strtotime($value['bill_date'])); ?></td>
                    <?php 
                    if($type == "A"){
                    ?>
                    <td class="text-center"><?php echo $value['billitem_length']; ?></td>
                    <?php  } 
                    if($type == "G"){
                        ?>
                        <th class="text-center"><?php echo $value['billitem_width']; ?></th> 
                        <th class="text-center"><?php echo $value['billitem_height']; ?></th> 
                        <?php } ?>
                    <td class="text-center"><?php echo $value['billitem_quantity'];?></td>
                    <td class="text-center"><?php echo $value['billitem_unit']; ?></td>
                    <?php 
                    if($type == "O"){
                        ?>
                        <td class="text-center"><?php echo $value['hsn_code']; ?></td>
                        <?php } ?>
                    <td class="text-center"><?php echo $value['billitem_rate']; ?></td>
                    <td class="text-center"><?php echo $value['discount_percentage']?></td>
                    <td class="text-center"><?php echo $value['tax_slab']; ?></td>
                    <td class="text-center"><?php echo $value['billitem_amount']; ?></td>
                </tr>
                <?php $total_po_quantity += $value['billitem_quantity']; 
                } ?>
<input type="hidden" id="total_po_quantity" value="<?= $total_po_quantity;?>">
<input type="hidden" id="estimated_quantity" value="<?php echo (isset($estimated_quantity))?$estimated_quantity:'';?>">
<input type="hidden" id="estimated_rate" value="<?php echo (isset($estimated_rate))?$estimated_rate:'';?>">
            </tbody>
        </table>
    </div>
</div>

