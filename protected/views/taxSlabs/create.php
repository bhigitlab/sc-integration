<?php
/* @var $this TaxSlabsController */
/* @var $model TaxSlabs */

$this->breadcrumbs = array(
	'Tax Slabs' => array('index'),
	'Create',
);

$this->menu = array(
	array('label' => 'List TaxSlabs', 'url' => array('index')),
	array('label' => 'Manage TaxSlabs', 'url' => array('admin')),
);
?>
<div class="panel panel-gray">
	<div class="panel-heading form-head">
		<h3 class="panel-title">Add Tax Slab</h3>
	</div>
	<?php echo $this->renderPartial('_form', array('model' => $model, 'status_values' => $status_values)); ?>
</div>