<?php
/* @var $this CompanyExpenseTypeController */
/* @var $model CompanyExpenseType */

$this->breadcrumbs=array(
	'Company Expense Types'=>array('index'),
	$model->company_exp_id=>array('view','id'=>$model->company_exp_id),
	'Update',
);

?>
<!--<div class="modal-dialog modal-sm">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" onclick="javascript:window.location.reload()">&times;</button>
			<h4 class="modal-title">Edit Daily Expense Type</h4>
		</div>
		<?php //echo $this->renderPartial('_form', array('model'=>$model,'dailyexp'=>$dailyexp)); ?>
	</div>
	
</div>-->
<div class="panel panel-gray">
    <div class="panel-heading form-head">
        <h3 class="panel-title">Edit Daily Expense Type</h3>
    </div>
        <?php echo $this->renderPartial('_form', array('model'=>$model,'dailyexp'=>$dailyexp)); ?>
</div>