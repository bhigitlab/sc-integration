<?php
/* @var $this ExpensesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'Expense Type',
);
?>
<div class="container" id="expensetype">
    <div class="loading-overlay">
        <span class="fa fa-spinner fa-3x fa-spin"></span>
    </div>
    <h2>Purchase Category</h2>
    <!--<div style="float:right">
        <a style="cursor:pointer; " class="savepdf"  href="<?php echo Yii::app()->createUrl('expensetype/savetopdf') ?>">SAVE AS PDF</a></span>

        <a style="cursor:pointer; " class="savepdf"  href="<?php echo Yii::app()->createUrl('expensetype/savetoexcel') ?>">SAVE AS EXCEL</a></span>
    </div><br><br>-->
    <?php //$this->renderPartial('_newsearch', array('model' => $model)) 
    ?>

    <div class="add-btn">
        <?php if (Yii::app()->user->role == 1 || Yii::app()->user->role == 2) { ?>
            <button data-toggle="modal" data-target="#addCategory" class="createCategory">Add Unit</button>
        <?php } ?>
    </div>

    <div class="exp-list">

        <div>
            <h4>Category List</h4>
            <?php
            $this->widget('zii.widgets.CListView', array(
                'dataProvider' => $dataProvider,
                'itemView' => '_view',
            ));
            ?>
            <div>
                <br />
                <br />
                <br />


                <!-- Add Expense type Popup -->
                <div id="addCategory" class="modal fade" role="dialog">
                    <div class="modal-dialog modal-lg">

                    </div>
                </div>



                <!-- Edit Expense type Popup -->

                <div class="modal fade edit" role="dialog">
                    <div class="modal-dialog modal-lg">


                    </div>
                </div>

                <script>
                    $(document).ready(function() {
                        $('.createCategory').click(function() {
                            $('.loading-overlay').addClass('is-active');
                            $.ajax({
                                type: "GET",
                                url: "<?php echo $this->createUrl('Unit/create') ?>",
                                success: function(response) {
                                    $("#addCategory").html(response);

                                }
                            });
                        });



                        $(document).delegate('.editProject', 'click', function() {
                            // alert("hai");
                            var id = $(this).attr('data-id');
                            $('.loading-overlay').addClass('is-active');

                            $.ajax({
                                type: "GET",
                                url: "<?php echo $this->createUrl('expensetype/update&id=') ?>" + id,
                                success: function(response) {
                                    $(".edit").html(response);

                                }
                            });

                        });

                        jQuery(function($) {
                            $('#addExpensetype').on('keydown', function(event) {
                                if (event.keyCode == 13) {
                                    $("#expensetypesearch").submit();
                                }


                            });
                        });

                    });
                    $(document).ajaxComplete(function() {
                        $('.loading-overlay').removeClass('is-active');
                        $('#loading').hide();
                    });
                </script>



            </div>
        </div>
        <style>
            .savepdf {
                background-color: #6a8ec7;
                border: 1px solid #6a8ec8;
                color: #fff;
                padding: 5px;
            }
        </style>