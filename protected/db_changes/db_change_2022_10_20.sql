
CREATE TABLE `jp_purchase_items_import` (`import_id` INT(11) NOT NULL AUTO_INCREMENT , `main_category` VARCHAR(100) NOT NULL , `sub_category` VARCHAR(100) NULL DEFAULT NULL , `brand` VARCHAR(100) NOT NULL , `specification_type` ENUM('A','O','G') NULL DEFAULT NULL , `specification` VARCHAR(255) NOT NULL , `unit` VARCHAR(100) NOT NULL , `hsn_code` VARCHAR(200) NOT NULL , `company` VARCHAR(100) NOT NULL , `created_by` INT(11) NULL DEFAULT NULL , `created_on` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP , PRIMARY KEY (`import_id`)) ENGINE = InnoDB;
ALTER TABLE `jp_purchase_items_import` CHANGE `specification_type` `specification_type` VARCHAR(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL;

ALTER TABLE `jp_purchase_items_import` ADD `main_cat_exist` ENUM('0','1') NOT NULL DEFAULT '1' COMMENT '0=>Not Exist,1=>Exist' AFTER `main_category`;
ALTER TABLE `jp_purchase_items_import` ADD `sub_cat_exist` ENUM('0','1') NOT NULL DEFAULT '1' COMMENT '0=>Not Exist,1=>Exist' AFTER `sub_category`;
ALTER TABLE `jp_purchase_items_import` ADD `brand_exist` ENUM('0','1') NOT NULL DEFAULT '1' COMMENT '0=>Not Exist,1=>Exist' AFTER `brand`;
ALTER TABLE `jp_purchase_items_import` ADD `unit_exist` ENUM('0','1') NOT NULL DEFAULT '1' COMMENT '0=>Not Exist,1=>Exist' AFTER `unit`;
ALTER TABLE `jp_specification` CHANGE `dieno` `dieno` VARCHAR(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL;
ALTER TABLE `jp_specification` CHANGE `length` `length` VARCHAR(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL;
ALTER TABLE `jp_specification` CHANGE `filename` `filename` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL;
ALTER TABLE `jp_purchase_items_import` ADD `updated_by` INT(11) NULL DEFAULT NULL AFTER `created_on`, ADD `updated_date` DATE NULL DEFAULT NULL AFTER `updated_by`;
ALTER TABLE `jp_purchase_items_import` ADD `spec_exist` ENUM('0','1') NOT NULL DEFAULT '0' COMMENT '0=>Not Exist,1=>Exist' AFTER `specification`;