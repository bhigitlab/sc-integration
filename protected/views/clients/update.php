<?php
/* @var $this ClientsController */
/* @var $model Clients */

$this->breadcrumbs = array(
    'Clients' => array('index'),
    $model->name => array('view', 'id' => $model->cid),
    'Update',
);

/*$this->menu = array(
    array('label' => 'List Clients', 'url' => array('index')),
    array('label' => 'Create Clients', 'url' => array('create')),
    array('label' => 'View Clients', 'url' => array('view', 'id' => $model->cid)),
    array('label' => 'Manage Clients', 'url' => array('admin')),
);*/
?>

<div class="entries-wrapper">
    <div class="row">
        <div class="col-xs-12">
            <div class="heading-title">Edit Client</div>
            <div class="dotted-line"></div>
        </div>
    </div>
    <?php echo $this->renderPartial('_form', array('model' => $model,'users' => $users)); ?>
</div>
