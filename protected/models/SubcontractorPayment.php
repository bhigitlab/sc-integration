<?php

/**
 * This is the model class for table "{{subcontractor_payment}}".
 *
 * The followings are the available columns in table '{{subcontractor_payment}}':
 * @property integer $payment_id
 * @property integer $subcontractor_id
 * @property integer $project_id
 * @property double $amount
 * @property string $description
 * @property string $date
 * @property integer $payment_type
 * @property integer $bank
 * @property string $cheque_no
 * @property double $sgst
 * @property double $sgst_amount
 * @property double $cgst
 * @property double $cgst_amount
 * @property double $igst
 * @property double $igst_amount
 * @property double $tax_amount
 * @property integer $company_id
 * @property integer $created_by
 * @property string $created_date
 * @property integer $updated_by
 * @property string $updated_date
 * @property integer $reconciliation_status
 * @property string $reconciliation_date
 * @property integer $update_status
 * @property integere employee_id
 *
 * The followings are the available model relations:
 * @property Subcontractor $subcontractor
 */
class SubcontractorPayment extends CActiveRecord
{
	public $fromdate;
	public $todate;
	public $date_from;
	public $date_to;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{subcontractor_payment}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('project_id, amount, description, date, payment_type, created_by', 'required'),
			array('subcontractor_id, project_id, payment_type, bank, company_id, created_by, updated_by, reconciliation_status, rowcount_slno, rowcount_total, update_status, stage_id,employee_id', 'numerical', 'integerOnly' => true),
			array('amount, sgst, sgst_amount, cgst, cgst_amount, igst, igst_amount, tax_amount, tds, tds_amount, paidamount', 'numerical'),
			array('cheque_no', 'length', 'max' => 100),
			array('created_date, updated_date, reconciliation_date,quotation_number', 'safe'),
			array('amount', 'checkNegativeBalance'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('payment_id, subcontractor_id, project_id, amount, description, date, payment_type, bank, cheque_no, sgst, sgst_amount, cgst, cgst_amount, igst, igst_amount, tax_amount, tds, tds_amount, paidamount, company_id, created_by, created_date, updated_by, updated_date, reconciliation_status, reconciliation_date, approve_status, rowcount_slno, rowcount_total, budget_percentage, update_status,payment_quotation_status,employee_id', 'safe', 'on' => 'search'),
		);
	}

    public function checkNegativeBalance() {
		if($this->payment_id !=""){
			$sql = "SELECT paidamount FROM `jp_subcontractor_payment` "
					. " WHERE payment_id=".$this->payment_id;
					//die($sql);
			$oldpaidamount = Yii::app()->db->createCommand($sql)->queryScalar();
		}
		if ((($this->payment_id !="") && ($oldpaidamount != $this->paidamount)) || ($this->payment_id =="")) {
            $invoice_amount = 0;
            $expense_amount = 0;
            $payment_type = "";
            $available_amount = "";
            $company_id = $this->company_id;
            if ($this->payment_type == '89' || $this->payment_type == '88' || $this->payment_type == '103') {
                $payment_type = $this->payment_type;
            }
			if($this->payment_type != '107'){
				if (!empty($company_id)) {
					$invoice_amount = BalanceHelper::cashInvoice($payment_type, $company_id, NULL, $this->bank, $this->date);
					$expense_amount = BalanceHelper::cashExpense($payment_type, $company_id, NULL, $this->payment_id, $this->bank, $this->date);

					if (is_numeric($invoice_amount) && is_numeric($expense_amount)) {
						$available_amount = $invoice_amount - $expense_amount;
					}

					if (is_numeric($available_amount) && $available_amount >= $this->paidamount) {
						$closing_balance = $available_amount - $this->paidamount;  //initial closing balace	
							//die($this->paidamount)	;	
						$date = date('Y-m-d', strtotime('+1 day', strtotime($this->date)));
						$paymentArray = $this->getPaymentArray($date, NULL, $status = 0, $payment_type, $company_id, $this->bank);
						if(!empty($paymentArray)){
							//echo "<pre>";print_r($paymentArray);exit;
							if (count($paymentArray) < 20000) {
								foreach ($paymentArray as $data) {
									if ($data['type'] == 73) { //expense
										$closing_balance -= $data['paidamount'];
									} else {
										$closing_balance += $data['receipt'];
									}
									//echo "<pre>";echo $closing_balance.'<br>';
									if ($closing_balance < 0) {
										$this->addError("amount", "Insufficient balance on " . Controller::formattedcommonDate($data['date']) . " in " . $data['tablename'] . " - ID: #" . $data['entry_id']);
										return;
									}
								}
								
							} else {
								$this->addError("amount", " Limit Of 20000 Entries Exceeded");
								return;
							}
						}
					} else if (($this->paidamount > $available_amount) && $payment_type != "103" && $payment_type != "") {
						$this->addError("amount", "insufficient balance on " . Controller::formattedcommonDate($this->date));
						$date = date('Y-m-d', strtotime($this->date));
						$this->addError("amount", "insufficient balance on " . Controller::formattedcommonDate($this->date));
					}

					if (!is_numeric($available_amount)) {
						$this->addError("", "Something went wrong! Check again or contact support team");
					}
				}
			}
		}
    }

        /**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'subcontractor' => array(self::BELONGS_TO, 'Subcontractor', 'subcontractor_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'payment_id' => 'Payment',
			'subcontractor_id' => 'Subcontractor',
			'project_id' => 'Project',
			'amount' => 'Amount',
			'description' => 'Description',
			'date' => 'Date',
			'payment_type' => 'Payment Type',
			'bank' => 'Bank',
			'cheque_no' => 'Cheque No',
			'sgst' => 'Sgst',
			'sgst_amount' => 'Sgst Amount',
			'cgst' => 'Cgst',
			'cgst_amount' => 'Cgst Amount',
			'igst' => 'Igst',
			'igst_amount' => 'Igst Amount',
			'tax_amount' => 'Tax Amount',
			'tds' => "TDS (%)",
			'tds_amount' => "TDS Amount",
			'paidamount' => "Paid Amount",
			'company_id' => 'Company',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
			'updated_by' => 'Updated By',
			'updated_date' => 'Updated Date',
			'reconciliation_status' => 'Reconciliation Status',
			'reconciliation_date' => 'Reconciliation Date',
			'approve_status' => 'Approve Status',
			'rowcount_slno' => 'Row Slno',
			'rowcount_total' => 'Row Total',
			'budget_percentage' => 'Budget Percentage',
			'update_status' => 'Update Status',
			'payment_quotation_status' => 'Payment Status',
			'quotation_number' => 'Quotation Number',
			'employee_id'=>'Employee'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria = new CDbCriteria;

		// Attributes to be searched
		$criteria->compare('payment_id', $this->payment_id);
		$criteria->compare('subcontractor_id', $this->subcontractor_id);
		$criteria->compare('project_id', $this->project_id);
		$criteria->compare('amount', $this->amount);
		$criteria->compare('description', $this->description, true);
		$criteria->compare('date', $this->date, true); // Removing this if date range is used below
		$criteria->compare('payment_type', $this->payment_type);
		$criteria->compare('bank', $this->bank);
		$criteria->compare('cheque_no', $this->cheque_no, true);
		$criteria->compare('sgst', $this->sgst);
		$criteria->compare('sgst_amount', $this->sgst_amount);
		$criteria->compare('cgst', $this->cgst);
		$criteria->compare('cgst_amount', $this->cgst_amount);
		$criteria->compare('igst', $this->igst);
		$criteria->compare('igst_amount', $this->igst_amount);
		$criteria->compare('tax_amount', $this->tax_amount);
		$criteria->compare('company_id', $this->company_id);
		$criteria->compare('created_by', $this->created_by);
		$criteria->compare('created_date', $this->created_date, true);
		$criteria->compare('updated_by', $this->updated_by);
		$criteria->compare('updated_date', $this->updated_date, true);
		$criteria->compare('reconciliation_status', $this->reconciliation_status);
		$criteria->compare('reconciliation_date', $this->reconciliation_date);
		$criteria->compare('approve_status', $this->approve_status);
		$criteria->compare('rowcount_slno', $this->rowcount_slno);
		$criteria->compare('rowcount_total', $this->rowcount_total);
		$criteria->compare('budget_percentage', $this->budget_percentage);
		$criteria->compare('update_status', $this->update_status);
		$criteria->compare('payment_quotation_status', $this->payment_quotation_status);
		$criteria->compare('employee_id',$this->employee_id);

		// Date filtering
		if (!empty($this->date_from) && empty($this->date_to)) {
			$criteria->addCondition("date >= '" . date("Y-m-d", strtotime($this->date_from)) . "'");
		} elseif (!empty($this->date_to) && empty($this->date_from)) {
			$criteria->addCondition("date <= '" . date("Y-m-d", strtotime($this->date_to)) . "'");
		} elseif (!empty($this->date_from) && !empty($this->date_to)) {
			$criteria->addCondition("date BETWEEN '" . date('Y-m-d', strtotime($this->date_from)) . "' AND '" . date('Y-m-d', strtotime($this->date_to)) . "'");
		}

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	public function usersearch()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('payment_id', $this->payment_id);
		$criteria->compare('subcontractor_id', $this->subcontractor_id);
		$criteria->compare('project_id', $this->project_id);
		$criteria->compare('amount', $this->amount);
		$criteria->compare('description', $this->description, true);
		$criteria->compare('date', $this->date, true);
		$criteria->compare('payment_type', $this->payment_type);
		$criteria->compare('bank', $this->bank);
		$criteria->compare('cheque_no', $this->cheque_no, true);
		$criteria->compare('sgst', $this->sgst);
		$criteria->compare('sgst_amount', $this->sgst_amount);
		$criteria->compare('cgst', $this->cgst);
		$criteria->compare('cgst_amount', $this->cgst_amount);
		$criteria->compare('igst', $this->igst);
		$criteria->compare('igst_amount', $this->igst_amount);
		$criteria->compare('tax_amount', $this->tax_amount);
		$criteria->compare('company_id', $this->company_id);
		$criteria->compare('created_by', $this->created_by);
		$criteria->compare('created_date', $this->created_date, true);
		$criteria->compare('updated_by', $this->updated_by);
		$criteria->compare('updated_date', $this->updated_date, true);
		$criteria->compare('reconciliation_status', $this->reconciliation_status);
		$criteria->compare('reconciliation_date', $this->reconciliation_date);
		$criteria->compare('approve_status', $this->approve_status);
		$criteria->compare('rowcount_slno', $this->rowcount_slno);
		$criteria->compare('rowcount_total', $this->rowcount_total);
		$criteria->compare('budget_percentage', $this->budget_percentage);
		$criteria->compare('update_status', $this->update_status);
		$criteria->compare('payment_quotation_status', $this->payment_quotation_status);
		$criteria->compare('employee_id',$this->employee_id);
		if (!empty($this->fromdate) && !empty($this->todate)) {
			$criteria->addCondition("date >= '" . date('Y-m-d', strtotime($this->fromdate)) . "'");
			$criteria->addCondition("date <= '" . date('Y-m-d', strtotime($this->todate)) . "'");
		} else if (empty($this->fromdate) && !empty($this->todate)) {
			$criteria->addCondition("date <= '" . date('Y-m-d', strtotime($this->todate)) . "'");
		} else if (!empty($this->fromdate) && empty($this->todate)) {
			$criteria->addCondition("date >= '" . date('Y-m-d', strtotime($this->fromdate)) . "'");
		}
		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'pagination' => false,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SubcontractorPayment the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function getSubcontractorPaymentTotal($detail)
	{
		$sql = "SELECT SUM((IFNULL(amount, 0) + IFNULL(tax_amount, 0))- IFNULL(tds_amount,0)) as amount "
			. " from `jp_subcontractor_payment` "
			. " where `subcontractor_id` = '" . $detail['subcontractor_id'] . "' "
			. " AND `project_id` = '" . $detail['project_id'] . "' "
			. " AND (reconciliation_status = 1 OR reconciliation_status IS NULL) "
			. " AND approve_status = 'Yes' "
			. " AND quotation_number =" . $detail['scquotation_id'];
		$amount_sum = Yii::app()->db->createCommand($sql)->queryAll();

		ob_start();
		foreach ($amount_sum as $key => $value) {
			$reconcil_amount = $value['amount'];
		}
		ob_end_clean();


		$amount =  $reconcil_amount;

		return isset($amount) ? $amount : 0;
	}


	public function getSubcontractorPaymentTotal1($detail)
	{
		$sql = " SELECT SUM((IFNULL(amount, 0) + IFNULL(tax_amount, 0))- IFNULL(tds_amount,0)) as amount "
			. " FROM `jp_subcontractor_payment` "
			. " WHERE `subcontractor_id` = '" . $detail['subcontractor_id'] . "'"
			. "  AND `project_id` = '" . $detail['project_id'] . "' "
			. " AND ((reconciliation_status = 0  AND approve_status = 'No')"
			. " OR (reconciliation_status = 0  AND approve_status = 'Yes')"
			. " OR (reconciliation_status IS NULL  AND approve_status = 'No')) "
			. " AND quotation_number =" . $detail['scquotation_id'];
		$amount_sum = Yii::app()->db->createCommand($sql)->queryAll();
		ob_start();
		foreach ($amount_sum as $key => $value) {
			$reconcil_amount = $value['amount'];
		}
		ob_end_clean();

		$amount =  $reconcil_amount;

		return isset($amount) ? $amount : 0;
	}

	public function getUserDatas($data)
	{
		$created_by = $updated_by = '';
		if (!empty($data['created_by'])) {
			$created_by = Users::model()->findByPk($data['created_by']);
			$created_by = isset($created_by) ? $created_by['first_name'] . ' ' . $created_by['last_name'] : '';
		}
		if (!empty($data['updated_by'])) {
			$updated_by = Users::model()->findByPk($data['updated_by']);
			$updated_by = isset($updated_by) ? $updated_by['first_name'] . ' ' . $updated_by['last_name'] : '';
		}
		return array('created' => $created_by, 'updated' => $updated_by);
	}

    public function getPaymentArray($date, $id, $status, $payment_type, $company_id, $bank_id) {

        $current_date = date('Y-m-d');
        //73=>expense , 72=>receipt
        $condition = "";
        $condition1 = "`date` BETWEEN '" . $date . "' AND '" . $current_date . "'";
        $condition2 = "";
		$condition3 = "";
		$dailyexpense_cond="";
		$dailyreciept_cond="";
		$vendor_condition="";
		$subcontractor_condition="";
		
        $company_condition = " AND company_id=  $company_id";
        if ($id != "") {
            $condition = " AND  `payment_id` > " . $id;
        }
        if ($id != "" && $status == 1) {
            $condition = " AND  `payment_id` >= " . $id;
            $condition1 = "`date` ='" . $date . "'";
        }

        if ($payment_type == 88) {
            $condition2 = " AND reconciliation_status = 1 AND bank_id = " . $bank_id;
			$condition3 = "  AND (expense_type=88 OR payment_type=88)  AND subcontractor_id IS NULL";
			$dailyreciept_cond =" AND (dailyexpense_receipt_type=88 AND "
			. " dailyexpense_chequeno  IS NOT NULL  "
			. " AND reconciliation_status =1"
			. " AND company_id=" . $company_id.")  "
			. " OR (dailyexpense_receipt_type=88 "
			. " AND expensehead_type=4  AND "
			. " company_id=" . $company_id.") AND "
			. " parent_status='1' AND "
			. " exp_type=72 ";
			$dailyexpense_cond =" AND (expense_type=88) AND exp_type=73 "
			. " AND parent_status='1' "
			. " AND dailyexpense_chequeno IS NOT NULL "
			. " AND reconciliation_status =1 "
			. " AND company_id=" . $company_id;
			$vendor_condition = " AND payment_type=88 ";
			$subcontractor_condition = " AND payment_type=88 AND approve_status ='Yes'  AND bank IS NOT NULL AND cheque_no IS NOT NULL ";
        }
		
		if ($payment_type == 89) {
            $condition3 = "  AND (expense_type=89 OR payment_type=89)  AND subcontractor_id IS NULL";
			$dailyreciept_cond =" AND (dailyexpense_receipt_type=89 AND "
			. " dailyexpense_chequeno IS NULL "
			. " AND reconciliation_status IS NULL "
			. " AND company_id=" . $company_id.")  "
			. " OR (dailyexpense_receipt_type=88 "
			. " AND expensehead_type=4  AND "
			. " company_id=" . $company_id.") AND "
			. " parent_status='1' AND "
			. " exp_type=72 ";
			$dailyexpense_cond =" AND (expense_type=89) AND exp_type=73 "
			. " AND parent_status='1' "
			. " AND dailyexpense_chequeno IS NULL "
			. " AND reconciliation_status IS NULL "
			. " AND company_id=" . $company_id;
			$vendor_condition = " AND payment_type=89 ";
			$subcontractor_condition = " AND payment_type=89 AND approve_status ='Yes'";
        }
		if ($payment_type == 103) {
			$tblpx = Yii::app()->db->tablePrefix;
           $user = Users::model()->findByPk(Yii::app()->user->id);
           $arrVal = explode(',', $user->company_id);
			$dailyexpense = array();
			$employee_id = '';
			$date_from = '';
			$date_to = '';
			$company_id = '';
			$daybook = array();
			$dailyexpense_refund = array();
			$dailyexpense_exp = array();
			$scpayment = array();
			$chquery1 = "";
			$chquery2 = "";
			$chcondition1 = "";
			$chcondition2 = "";
			$where ='';
			$where1 ='';
			$where2 ='';
			$where .= " AND {$tblpx}dailyexpense.date between '" . $date . "' and '" . $current_date . "'";
                $where1 .= " AND {$tblpx}expenses.date between '" . $date . "' and '" . $current_date . "'";
                $where2 .= " AND {$tblpx}subcontractor_payment.date between '" . $date . "' and '" . $current_date . "'";
			$reconcilsql = "SELECT *  FROM `jp_reconciliation` "
                . " WHERE `reconciliation_status`='0' "
                . " and reconciliation_payment='Dailyexpense Payment' and "
                . "reconciliation_table='jp_dailyexpense'";
            $reconcil_dexpense_payment_data = Yii::app()->db->createCommand($reconcilsql)->queryAll();

            $payment_cheque_array = array();
            foreach ($reconcil_dexpense_payment_data as  $data) {

                $value = "'{$data['reconciliation_chequeno']}'";
                if ($value != '') {
                    $recon_data = $value;
                    array_push($payment_cheque_array, $recon_data);
                }
            }
            $payment_cheque_array1 = implode(',', $payment_cheque_array);
            if (($payment_cheque_array1)) {
                $chquery1 = "(dailyexpense_chequeno NOT IN ( $payment_cheque_array1) OR dailyexpense_chequeno IS NULL)";
                $chcondition1 = " AND " . $chquery1 . "";
            }

            $petty_issued_id = Yii::app()->db->createCommand("SELECT `company_exp_id` 
            FROM `jp_company_expense_type` WHERE  UPPER(`name`)='PETTY CASH ISSUED'")->queryScalar();
            $dailyexpense = array();
            if($petty_issued_id){
                $dailyexpensesql = "SELECT " . $tblpx . "dailyexpense.dailyexp_id as entry_id,". $tblpx . "dailyexpense.exp_type as type,"
                 . $tblpx . "dailyexpense.dailyexpense_receipt as receipt,"
                 . $tblpx . "dailyexpense.dailyexpense_paidamount as paidamount,". $tblpx . "dailyexpense.date  FROM " . $tblpx . "dailyexpense "
                . " LEFT JOIN " . $tblpx . "users "
                . " ON " . $tblpx . "dailyexpense.employee_id= " . $tblpx . "users.userid"
                . " WHERE " . $tblpx . "dailyexpense.exp_type =73 "
                . " AND " . $tblpx . "dailyexpense.expense_type  IN('89','88') "  . $chcondition1 . "  "
                . " AND jp_dailyexpense.exp_type_id= $petty_issued_id " . $where . ""
                . " order by " . $tblpx . "dailyexpense.date asc";

            $dailyexpense = Yii::app()->db->createCommand($dailyexpensesql)->queryAll();
            }
            //petty cash refund
            $reconcil_receipt_sql = "SELECT *  FROM `jp_reconciliation` "
                . " WHERE `reconciliation_status`='0' "
                . " and reconciliation_payment='Dailyexpense Receipt' and "
                . "reconciliation_table='jp_dailyexpense'";
            $reconcil_dexpense_receipt_data = Yii::app()->db->createCommand($reconcil_receipt_sql)->queryAll();
            $receipt_cheque_array = array();
            foreach ($reconcil_dexpense_receipt_data as  $receiptdata) {
                $receiptvalue = "'{$receiptdata['reconciliation_chequeno']}'";
                if ($receiptvalue != '') {
                    $recon_receipt_data = $receiptvalue;
                    array_push($receipt_cheque_array, $recon_receipt_data);
                }
            }
            $receipt_cheque_array1 = implode(',', $receipt_cheque_array);
            if (($receipt_cheque_array1)) {
                $chquery2 = "(dailyexpense_chequeno NOT IN ( $receipt_cheque_array1) OR dailyexpense_chequeno IS NULL)";
                $chcondition2 = " AND " . $chquery2 . "";
            }

            $petty_refund_id = Yii::app()->db->createCommand("SELECT `company_exp_id` 
            FROM `jp_company_expense_type` WHERE  UPPER(`name`)='PETTY CASH REFUND'")->queryScalar();
            $dailyexpense_refund=array();
            if($petty_refund_id){
                $dexp_refund_sql = "SELECT " . $tblpx . "dailyexpense.dailyexp_id as entry_id,". $tblpx . "dailyexpense.exp_type as type,"
				. $tblpx . "dailyexpense.dailyexpense_receipt as receipt,"
				. $tblpx . "dailyexpense.dailyexpense_paidamount as paidamount,". $tblpx . "dailyexpense.date  FROM " . $tblpx . "dailyexpense "
                    . " LEFT JOIN " . $tblpx . "users "
                    . " ON " . $tblpx . "dailyexpense.employee_id= " . $tblpx . "users.userid "
                    . " WHERE " . $tblpx . "dailyexpense.exp_type =72 "
                    . " AND " . $tblpx . "dailyexpense.dailyexpense_receipt_type IN('88','89') "  . $chcondition2 . "  "
                    . "AND jp_dailyexpense.exp_type_id= $petty_refund_id  ".$where
                    . " order by " . $tblpx . "dailyexpense.date asc";
                $dailyexpense_refund = Yii::app()->db->createCommand($dexp_refund_sql)->queryAll();
				//echo "hi <pre>";print_r($dailyexpense_refund);exit;
			}

            //petty cash expense
            $daybook_sql = "SELECT " . $tblpx . "expenses.exp_id as entry_id , " . $tblpx . "expenses.type,"
			 . $tblpx . "expenses.receipt," . $tblpx . "expenses.paid as paidamount," . $tblpx . "expenses.date "
                . " FROM " . $tblpx . "expenses LEFT JOIN " . $tblpx . "users "
                . " ON " . $tblpx . "expenses.employee_id= " . $tblpx . "users.userid "
                . " LEFT JOIN " . $tblpx . "projects "
                . " ON " . $tblpx . "projects.pid = " . $tblpx . "expenses.projectid "
                . " WHERE " . $tblpx . "expenses.type = 73  "
                . " AND " . $tblpx . "expenses.expense_type=103 ".$where1
                . " GROUP BY " . $tblpx . "expenses.projectid";
            $daybook = Yii::app()->db->createCommand($daybook_sql)->queryAll();
            $dailyexpense_exp_sql = "SELECT " . $tblpx . "dailyexpense.dailyexp_id as entry_id,". $tblpx . "dailyexpense.exp_type as type,"
			. $tblpx . "dailyexpense.dailyexpense_receipt as receipt,"
			. $tblpx . "dailyexpense.dailyexpense_paidamount as paidamount,". $tblpx . "dailyexpense.date  FROM " . $tblpx . "dailyexpense "
                . " LEFT JOIN " . $tblpx . "users "
                . " ON " . $tblpx . "dailyexpense.employee_id= " . $tblpx . "users.userid "
                . " WHERE (" . $tblpx . "dailyexpense.dailyexpense_receipt_type = 103 "
                . " OR  " . $tblpx . "dailyexpense.expense_type = 103  )".$where
                . " order by " . $tblpx . "dailyexpense.date asc";
            $dailyexpense_exp = Yii::app()->db->createCommand($dailyexpense_exp_sql)->queryAll();
            $scp_sql = "SELECT  " . $tblpx . "subcontractor_payment.payment_id as entry_id,"
			. " ( IFNULL(jp_subcontractor_payment.amount, 0) + IFNULL(jp_subcontractor_payment.tax_amount, 0) ) 
		 as paidamount ," . $tblpx . "subcontractor_payment.date   FROM " . $tblpx . "subcontractor_payment LEFT JOIN " . $tblpx . "users "
                . " ON " . $tblpx . "subcontractor_payment.employee_id= " . $tblpx . "users.userid "
                . " LEFT JOIN " . $tblpx . "projects "
                . " ON " . $tblpx . "projects.pid = " . $tblpx . "subcontractor_payment.project_id "
                . " WHERE " . $tblpx . "subcontractor_payment.payment_type=103 ".$where2;
				//die($scp_sql)     ;       
            $scpayment = Yii::app()->db->createCommand($scp_sql)->queryAll();
			$expense = !empty($daybook) ? $daybook : array();
		$dailyexpense_only= !empty($dailyexpense_exp) ? $dailyexpense_exp : array();
        $dailyexpense = !empty($dailyexpense) ? $dailyexpense : array();
        $dailyexpense_refund = !empty($dailyexpense_refund) ? $dailyexpense_refund : array();
        $scPayment = !empty($scPayment) ? $scPayment : array();

        $final_array = array();

        foreach ($expense as $expenseData) {
            $expenseData['tablename'] = 'Daybook';
            if ($expenseData['entry_id'] != NULL) {
                $final_array[] = $expenseData;
            }
        }

        foreach ($dailyexpense as $dailyexpenseData) {
            $dailyexpenseData['tablename'] = 'Daily Expense';
            if ($dailyexpenseData['entry_id'] != NULL) {
                $final_array[] = $dailyexpenseData;
            }
        }
		foreach ($dailyexpense_only as $dailyexpenseDatas) {
            $dailyexpenseDatas['tablename'] = 'Daily Expense';
            if ($dailyexpenseDatas['entry_id'] != NULL) {
                $final_array[] = $dailyexpenseDatas;
            }
        }
		foreach ($dailyexpense_refund as $dailyexpenseDatas) {
            $dailyexpenseDatas['tablename'] = 'Daily Expense Refund';
            if ($dailyexpenseDatas['entry_id'] != NULL) {
                $final_array[] = $dailyexpenseDatas;
            }
        }

        foreach ($scPayment as $scPaymentData) {
            $scPaymentData['tablename'] = 'Subcontractor Payment';
            $scPaymentData['type'] = 73;
            $scPaymentData['receipt'] = NULL;
            if ($scPaymentData['entry_id'] != NULL) {
                $final_array[] = $scPaymentData;
            }
        }


        $date = array_column($final_array, 'date');
        array_multisort($date, SORT_ASC, $final_array);
        return $final_array;
        }


        $sql = "SELECT exp_id as entry_id , type,"
		. " receipt,paid as paidamount,date  "
		. " FROM `jp_expenses` WHERE   " . $condition1 . $condition2 .$condition3. "  AND company_id = " . $company_id ;
	    $expense = Yii::app()->db->createCommand($sql)->queryAll();
       //echo "hi <pre>";print_r($expense);exit;	

        $sql = "SELECT dailyexp_id as entry_id,exp_type as type,"
                . " dailyexpense_receipt as receipt,"
                . " dailyexpense_paidamount as paidamount,date "
                . " FROM `jp_dailyexpense`  WHERE   " . $condition1 . $condition2 . $dailyreciept_cond. " AND company_id = " . $company_id;
        $dailyexpense = Yii::app()->db->createCommand($sql)->queryAll();
       
		$sql = "SELECT dailyexp_id as entry_id,exp_type as type,"
                . " dailyexpense_receipt as receipt,"
                . " dailyexpense_paidamount as paidamount,date "
                . " FROM `jp_dailyexpense`  WHERE   " . $condition1 . $condition2 . $dailyexpense_cond;
        $dailyexpense_only = Yii::app()->db->createCommand($sql)->queryAll();
		
        if ($payment_type == 88) {
            $condition2 = " AND reconciliation_status = 1 AND bank = " . $bank_id;
			
        }
        //  ( only expense) add receipt and type column
        $sql = "SELECT daily_v_id as entry_id,"
                . " ( IFNULL(amount, 0) + IFNULL(tax_amount, 0) ) "
                . " as paidamount,date FROM `jp_dailyvendors` "
                . " WHERE   " . $condition1 . $condition2 .$vendor_condition . " AND company_id = " . $company_id;
        $vendorPayment = Yii::app()->db->createCommand($sql)->queryAll();
        //  ( only expense) add receipt and type column
        $sql = "SELECT payment_id as entry_id,"
                . " ( IFNULL(amount, 0) + IFNULL(tax_amount, 0) ) "
                . " as paidamount ,date FROM `jp_subcontractor_payment`"
                . " WHERE " . $condition1 . $condition . $condition2 .$subcontractor_condition. " AND company_id = " . $company_id;
        $scPayment = Yii::app()->db->createCommand($sql)->queryAll();
		    
        $expense = !empty($expense) ? $expense : array();
		$dailyexpense_only= !empty($dailyexpense_only) ? $dailyexpense_only : array();
        $dailyexpense = !empty($dailyexpense) ? $dailyexpense : array();
        $vendorPayment = !empty($vendorPayment) ? $vendorPayment : array();
        $scPayment = !empty($scPayment) ? $scPayment : array();

        $final_array = array();

        foreach ($expense as $expenseData) {
            $expenseData['tablename'] = 'Daybook';
            if ($expenseData['entry_id'] != NULL) {
                $final_array[] = $expenseData;
            }
        }

        foreach ($dailyexpense as $dailyexpenseData) {
            $dailyexpenseData['tablename'] = 'Daily Expense';
            if ($dailyexpenseData['entry_id'] != NULL) {
                $final_array[] = $dailyexpenseData;
            }
        }
		foreach ($dailyexpense_only as $dailyexpenseDatas) {
            $dailyexpenseDatas['tablename'] = 'Daily Expense';
            if ($dailyexpenseDatas['entry_id'] != NULL) {
                $final_array[] = $dailyexpenseDatas;
            }
        }

        foreach ($vendorPayment as $vendorPaymentData) {
            $vendorPaymentData['tablename'] = 'Vendor Payment';
            $vendorPaymentData['type'] = 73;
            $vendorPaymentData['receipt'] = NULL;
            if ($vendorPaymentData['entry_id'] != NULL) {
                $final_array[] = $vendorPaymentData;
            }
        }
        foreach ($scPayment as $scPaymentData) {
            $scPaymentData['tablename'] = 'Subcontractor Payment';
            $scPaymentData['type'] = 73;
            $scPaymentData['receipt'] = NULL;
            if ($scPaymentData['entry_id'] != NULL) {
                $final_array[] = $scPaymentData;
            }
        }
        $date = array_column($final_array, 'date');
        array_multisort($date, SORT_ASC, $final_array);
        return $final_array;
    }

    public function checkDeleteBalance($data) {
        if ($data['exp_type'] == 72) {
            $exp_id = $data['payment_id'];
            $date = date('Y-m-d', strtotime($data['date']));
            $paymentArray = $this->getPaymentArray($date, $exp_id, $status = 0, $data['payment_type'], $data['company_id'], $data['bank']); //get the enetries after the selected entry
            //find the closing balance on the selected entry date
            $chekBalance = "";
            $invoice_amount = 0;
            $expense_amount = 0;
            $payment_type = 0;
            $available_amount = "";
            $company_id = $data['company_id'];
            if ($data['	payment_type'] == '89' || $data['	payment_type'] == '88' || $data['	payment_type'] == '103') {
                $payment_type = $data['	payment_type'];
            }

            if (!empty($company_id)) {
                $invoice_amount = BalanceHelper::cashInvoice($payment_type, $company_id, NULL, $this->bank, $this->date);
                $expense_amount = BalanceHelper::cashExpense($payment_type, $company_id, NULL, $this->payment_id, $this->bank, $this->date);


                if (is_numeric($invoice_amount) && is_numeric($expense_amount)) {
                    $available_amount = $invoice_amount - $expense_amount; //default closing balance
                }

                // get the  available amount just before the deleted entry
                $chekBalanceArray = $this->getPaymentArray($date, $exp_id, $status = 1, $payment_type, $company_id, $data['bank']);
                $chekBalance = $available_amount;

                foreach ($chekBalanceArray as $data) {
                    if ($data['type'] == 73) { //expense
                        $chekBalance += $data['paidamount'];
                    } else {
                        $chekBalance -= $data['receipt'];
                    }
                }

                $closingBalance = $chekBalance;
                $checkArray = array();
                foreach ($paymentArray as $data) {
                    if ($data['type'] == 73) { //expense
                        $closingBalance -= $data['paidamount'];
                    } else {
                        $closingBalance += $data['receipt'];
                    }
                    array_push($checkArray, $closingBalance);
                    if ($closingBalance < 0) {
                        return "Can't delete ! Insufficient balance on " . $data['date'] . " in " . $data['tablename'] . " - ID: #" . $data['entry_id'];
                    }
                }
            }

            return '1';
        } else {
            return '1';
        }
    }

}
