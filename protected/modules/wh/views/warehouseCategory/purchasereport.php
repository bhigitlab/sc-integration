<?php
/* @var $this PurchaseCategoryController */
/* @var $model PurchaseCategory */

$this->breadcrumbs=array(
	'Purchase Categories'=>array('index'),
	'Manage',
);

/*$this->menu=array(
	array('label'=>'List PurchaseCategory', 'url'=>array('index')),
	array('label'=>'Create PurchaseCategory', 'url'=>array('create')),
);*/

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#purchase-category-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
$tblpx = Yii::app()->db->tablePrefix;
?>
<div class="container" id="project">
    <h2>Purchase Report</h2>
    <?php
        $fromdate   = (isset($date_from) ?  date('d-M-Y', strtotime($date_from)) : '');
        $todate     = (isset($date_to) ? date('d-M-Y', strtotime($date_to)) : '');
    ?>
    <form id="projectform" action="<?php $url = Yii::app()->createAbsoluteUrl("projects/projectsquantityreport"); ?>" method="POST">
        <div class="search-form" >
        <?php
            echo CHtml::label('Projects : ', '');
            echo CHtml::dropDownList('project_id', 'project_id', CHtml::listData(Projects::model()->findAll(array(
                'select' => array('pid, name'),
                'order' => 'name',
                'condition' => 'company_id='.Yii::app()->user->company_id.'',
                'distinct' => true
            )), 'pid', 'name'), array('empty' => 'Select Project','style'=>'padding:2px 0px;', 'id' => 'project_id', 'options' => array($project_id => array('selected' => true))));
        ?>		
        <?php echo CHtml::label('Date From :', ''); ?>
        <?php echo CHtml::textField('date_from', (!empty($date_from) ?  date('d-M-Y', strtotime($date_from)) : ''), array("id" => "date_from", "readonly" => true)); ?>
        <?php echo CHtml::label('Date To :', ''); ?>
        <?php echo CHtml::textField('date_to', (!empty($date_to) ? date('d-M-Y', strtotime($date_to)) : ''), array("id" => "date_to", "readonly" => true)); ?>
        <?php echo CHtml::submitButton('GO', array('id' => 'btSubmit')); ?> 
        <?php echo CHtml::resetButton('Clear', array('onclick' => 'javascript:location.href="'. $this->createUrl('projectsquantityreport').'"')); ?>
        </div>
    </form>
    <?php if (!empty($project_id)) { ?>
    <div id="contenthtml" class="contentdiv">
        <div id="" class="pull-right" >
            <a style="cursor:pointer;" id="download" class="save_btn pdf_excel" href="<?php echo $this->createAbsoluteUrl('purchaseCategory/ExportPurchaseReportCSV', array("project_id" => $project_id,"fromdate" => $date_from,"todate" => $date_to)) ?>">SAVE AS EXCEL</a>
            <a style="cursor:pointer;" id="download" class="save_btn pdf_excel" href="<?php echo $this->createAbsoluteUrl('purchaseCategory/savepurchasereportpdf', array("project_id" => $project_id,"fromdate" => $date_from,"todate" => $date_to)) ?>">SAVE AS PDF</a>         
        </div>
        <h2 class="text-center">Purchase Report of <?php echo $model["name"]; ?></h2>
        <div class="container-fluid">
            <table class="table table-bordered ">
                <tr>
                    <td><b>Client : </b><?php echo $model["clientname"]; ?></td>
                </tr>
                <tr>
                    <td><b>Project Name : </b><?php echo $model["name"]; ?></td>
                </tr>
                <tr>
                    <td><b>Site : </b><?php echo $model["site"]; ?></td>
                </tr>
            </table>
            <table class="table table-bordered ">
                <thead>
                    <tr>
                        <th>Sl No</th>
                        <th>Category</th>
                        <th>Quantity</th>
                        <th>Amount</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                $i              = 1;
                $quantitySum    = 0;
                $amountSum      = 0;
                foreach($itemmodelcat as $modelcat) {
                    $quantitySum    = $quantitySum + $modelcat["billitemsum"];
                    $amountSum      = $amountSum + $modelcat["itemtotalamount"];
                ?>
                    <tr>
                        <td><?php echo $i; ?></td>
                        <td><?php echo $modelcat["category_name"]; ?></td>
                        <td class="text-right"><?php echo Controller::money_format_inr($modelcat["billitemsum"], 2); ?></td>
                        <td class="text-right"><?php echo Controller::money_format_inr($modelcat["itemtotalamount"], 2); ?></td>
                    </tr>
                <?php
                    $i = $i + 1;
                }
                ?>
                </tbody>
                <tfoot>
                    <tr>
                        <th></th>
                        <th></th>
                        <th class="text-right"><?php echo Controller::money_format_inr($quantitySum, 2); ?></th>
                        <th class="text-right"><?php echo Controller::money_format_inr($amountSum, 2); ?></th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
    <?php } else {
        $this->widget('zii.widgets.CListView', array(
            'dataProvider'=> $dataProvider,
            'itemView' => '_newpurchasereport', 'template' => '<div>{summary}{sorter}</div><div class="table-responsive"><table class="table table-responsive table-bordered table-hover">{items}</table></div>{pager}',
        ));
    } ?>
</div>
<script>
    $(document).ready(function () {
        $( "#date_from" ).datepicker({
            dateFormat: 'dd-M-yy',
            maxDate: $("#date_to").val()
        });
        $( "#date_to" ).datepicker({
            dateFormat: 'dd-M-yy',
        });
        $("#date_from").change(function() {
            $("#date_to").datepicker('option', 'minDate', $(this).val());
        });
        $("#date_to").change(function() {
            $("#date_from").datepicker('option', 'maxDate', $(this).val());
        });
    });
    (function () {
        'use strict';
        var container = document.querySelector('.container1');
        var table = document.querySelector('table');
        var leftHeaders = [].concat.apply([], document.querySelectorAll('tbody th'));
        var topHeaders = [].concat.apply([], document.querySelectorAll('thead th'));
        var topLeft = document.createElement('div');
        var computed = window.getComputedStyle(topHeaders[0]);
        container.appendChild(topLeft);
        topLeft.classList.add('top-left');
        topLeft.style.width = computed.width;
        topLeft.style.height = computed.height;

        container.addEventListener('scroll', function (e) {
            var x = container.scrollLeft;
            var y = container.scrollTop;

            leftHeaders.forEach(function (leftHeader) {
                leftHeader.style.transform = translate(x, 0);
            });
            topHeaders.forEach(function (topHeader, i) {
                if (i === 0) {
                    topHeader.style.transform = translate(0, y);
                } else {
                    topHeader.style.transform = translate(0, y);
                }
            });
            topLeft.style.transform = translate(x, y);
        });

        function translate(x, y) {
            return 'translate(' + x + 'px, ' + y + 'px)';
        }
    })();
</script>