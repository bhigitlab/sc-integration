<?php
/* @var $this VendorsController */
/* @var $data Vendors */
if ($index == 0) {
	
    ?>

    <thead class="entry-table">
        <tr>
            <th>Sl No.</th>
            <th>Company</th>
            <th>Project</th>
            <th>Vendor</th>
            <th>Description</th>            
            <th>Transaction Type</th>
            <th>Bank</th>
            <th>Cheque Number</th>
            <th>Amount</th>
            <th>Tax</th>
            <th>TDS Amount</th>
            <th>Paid Amount</th>
        </tr>   
    </thead>
    <?php
}
?>
    <tr>
        <td><?php echo $index+1; ?></td>
        <td>
        <?php
            $company = Company::model()->findByPk($data->company_id);
            if($company)
                echo $company->name;
        ?>
        </td>
        <td>
        <?php
            $project = Projects::model()->findByPk($data->project_id);
            if($project)
                echo $project->name;
        ?>
        </td>
        <td>
        <?php
            $vendor = Vendors::model()->findByPk($data->vendor_id);
            if($vendor)
                echo $vendor->name;
        ?>
        </td>
        <td><?php echo $data->description; ?></td>
        <td>
        <?php
        $exptype = Status::model()->findByPk($data->payment_type);
        if($exptype)
            echo $exptype->caption;
        ?>   
        </td>
        <td>
        <?php
            $bank = Bank::model()->findByPk($data->bank);
            if($bank)
                echo $bank->bank_name;
        ?>
        </td>
        <td><?php echo $data->cheque_no; ?></td>
        <td class="text-right"><?php echo Controller::money_format_inr($data->amount, 2); ?></td>
        <td class="text-right"><?php echo Controller::money_format_inr($data->tax_amount, 2); ?></td>
        <td class="text-right"><?php echo Controller::money_format_inr($data->tds_amount, 2); ?></td>
        <td class="text-right"><?php echo Controller::money_format_inr($data->paidamount, 2); ?></td>
    </tr>




