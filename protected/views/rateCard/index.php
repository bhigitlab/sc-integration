<?php
/* @var $this ExpensesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'Rate Card',
);

?>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<div class="container" id="expensetype">
    <div class="loading-overlay">
        <span class="fa fa-spinner fa-3x fa-spin"></span>
    </div>
    <div class="clearfix">
        <div class="add-btn pull-right">
            <?php
            if (isset(Yii::app()->user->role) && (in_array('/rateCard/create', Yii::app()->user->menuauthlist))) {
            ?>
            	<a class="button addratecard">Add Ratecard</a>
            <?php } ?>
        </div>
        <h2>Rate Card</h2>
    </div>
    <div id="addratecard" style="display:none;"></div>
    

    <div class="row">
        <div class="col-md-12">
            <?php
            $this->widget('zii.widgets.CListView', array(
                'dataProvider' => $dataProvider,
                'itemView' => '_newview',
                'template' => '<div class="container1"><table cellpadding="10" id="ratecardtbl" class="table">{items}</table></div>',
                'ajaxUpdate' => false,
            ));
            ?>
        </div>

        <?php
        Yii::app()->clientScript->registerScript('myjavascript', '
             $(document).ready(function(){
               $("#ratecardtbl").dataTable( {
                    "scrollY": "300px",
                    "scrollCollapse": true,
                    "paging": false,
                     "columnDefs"        : [       
                        { 
                            "searchable"    : false, 
                            "targets"       : [0,2] 
                        },
                        { "bSortable": false, "aTargets": [-1]  }
                    ],
                } );
                });

            ');
        ?>
        <script>
            function closeaction() {
                $('#addratecard').slideUp(500);
            }

            $(document).ready(function() {
                $('.addratecard').click(function() {
                    var id = $(this).attr('data-id');
                    $.ajax({
                        type: "POST",
                        url: "<?php echo $this->createUrl('rateCard/create&layout=1') ?>",
                        success: function(response) {
                            $('#addratecard').html(response).slideDown();
                        },
                    });
                });
                $(document).on('click','.editratecard',function(){
                    var id = $(this).attr('data-id');
                    $.ajax({
                        type: "POST",
                        url: "<?php echo $this->createUrl('rateCard/update&layout=1&id=') ?>" + id,
                        success: function(response) {
                            $('.loading-overlay').removeClass('is-active');
                            $('#addratecard').html(response).slideDown();
                        },
                    });
                });


                jQuery(function($) {
                    $('#addExpensetype').on('keydown', function(event) {
                        if (event.keyCode == 13) {
                            $("#expensetypesearch").submit();
                        }
                    });
                });

            });
            $(document).ajaxComplete(function() {
                $('.loading-overlay').removeClass('is-active');
                $('#loading').hide();
            });
        </script>
    </div>
</div>
<style>
    .savepdf {
        background-color: #6a8ec7;
        border: 1px solid #6a8ec8;
        color: #fff;
        padding: 5px;
    }

    .page-body h3 {
        margin: 4px 0px;
        color: inherit;
        text-align: left;
    }

    .panel {
        border: 1px solid #ddd;
    }

    .panel-heading {
        background-color: #eee;
        height: 40px;
    }
</style>