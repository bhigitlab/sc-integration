
<div class="panel panel-default" id="generalform">
  <div class="panel-heading">
    <div class="panel-titl">Upload Files</div>
  </div>
  <?php 
$form = $this->beginWidget('CActiveForm', array(
    'method'=>'post',
    'action' => Yii::app()->createAbsoluteUrl("dashboard/uploadFiles"),
    'htmlOptions' => array('class' => 'form-inline', 'enctype' => 'multipart/form-data'),      
    ));
    $model = new QuotationGeneratorImage();
 ?>
  <div class="panel-body">


<div class="row">
  <div class="col-md-6">
    <label>Top File:</label>
        <?php
          $this->widget('CMultiFileUpload', array(
             'model'=>$model,
             'name'=>'top_image',
             'attribute'=>'image',
             'accept'=>'pdf',
             'max'=>10,              
              'duplicate'=>'Already Selected',
              )); ?>
              <?php
              $sql  = "SELECT `id`,`top_bottom_image` FROM `jp_quotation_generator_image` WHERE type='0'";
              $image = Yii::app()->db->createCommand($sql)->queryAll(); ?>
              <ul style="padding-left: 2px;list-style: none;margin-top: 10px;max-height: 100px;overflow: auto;">
                <?php foreach ($image as $key => $value) { ?>
                  <li style="margin-bottom: 5px;color:blue;">
                  <a href="<?php echo Yii::app()->baseUrl."/uploads/quotation_generator/top-images/".$value['top_bottom_image'] ?>" target="_blank">
                    <i class="fa fa-file"></i>
                    <?php echo $value['top_bottom_image'] ?>
                    </a>
                    <b class="text-danger remove_file" id="<?php echo $value['id']?>" style="cursor:pointer;"> 
                    &nbsp;<span class="fa fa-trash"></span></b>
                  
                  </li>
                <?php } ?>
                
                </ul>
          </div>

        <div class="col-md-6">
        <label>Bottom File:</label>
            <?php              
              $this->widget('CMultiFileUpload', array(
                'model'=>$model,
                'name'=>'bottom_image',
                'attribute'=>'image',
                'accept'=>'pdf',
                'max'=>10,              
              'duplicate'=>'Already Selected',
              ));
              
              $sql  = "SELECT `id`,`top_bottom_image` FROM `jp_quotation_generator_image` WHERE type='1'";
              $image = Yii::app()->db->createCommand($sql)->queryAll(); ?>
              <ul style="padding-left: 2px;list-style: none;margin-top: 10px;max-height: 100px;overflow: auto;">
                <?php foreach ($image as $key => $value) { ?>
                  <li style="margin-bottom: 5px;color:blue;">
                  <a href="<?php echo Yii::app()->baseUrl."/uploads/quotation_generator/bottom-images/".$value['top_bottom_image'] ?>" target="_blank">
                    <i class="fa fa-file"></i><?php echo $value['top_bottom_image'] ?>
                    </a>
                    <b class="text-danger remove_file" id="<?php echo $value['id']?>" style="cursor:pointer;"> &nbsp;<span class="fa fa-trash"></span></b>
                
                  </li>
                <?php } ?>
                
                </ul>
                
          </div>
          
              </div>
      </div>
      <div class="panel-footer text-right">
      <?php echo CHtml::submitButton('Upload',array('class'=>'btn btn-primary btn-sm')); ?>
      </div>
      <?php $this->endWidget(); ?>
  </div>
</div>

<script>
  $(document).on("click",".remove_file",function(){
    var answer = confirm("Are you sure you want to delete this file?");
    if (answer) {
      var id= $(this).attr("id");
      $.ajax({
        type: "POST",
        data: {
          "id": id,
        },
        url: "<?php echo $this->createUrl('dashboard/removeFile') ?>",
        success: function (response)
        {
          setTimeout(function() {
              location.reload();
          }, 2000);
        }
      });
    }
  })

  </script>
