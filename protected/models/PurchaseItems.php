<?php

/**
 * This is the model class for table "{{purchase_items}}".
 *
 * The followings are the available columns in table '{{purchase_items}}':
 * @property integer $item_id
 * @property integer $purchase_id
 * @property string $description
 * @property double $quantity
 * @property double $unit
 * @property double $rate
 * @property double $amount
 * @property integer $bill_id
 * @property integer $created_by
 * @property string $created_date
 * @property string $or_description
 * @property double $or_quantity
 * @property double $or_unit
 * @property double $or_rate
 * @property double $or_amount
 */
class PurchaseItems extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{purchase_items}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('purchase_id, quantity, rate, amount, created_by, created_date', 'required'),
			array('purchase_id, bill_id, created_by', 'numerical', 'integerOnly'=>true),
			array('quantity, rate, amount, or_quantity, or_unit, or_rate, or_amount', 'numerical'),
			//array('or_description', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('item_id, purchase_id, description, quantity, unit, rate, amount, bill_id, created_by, created_date, or_description, or_quantity, or_unit, or_rate, or_amount, permission_status,hsn_code', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'item_id' => 'Item',
			'purchase_id' => 'Purchase',
			'description' => 'Description',
			'quantity' => 'Quantity',
			'unit' => 'Unit',
			'rate' => 'Rate',
			'amount' => 'Amount',
			'bill_id' => 'Bill',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
			'or_description' => 'Original Description',
			'or_quantity' => 'Original Quantity',
			'or_unit' => 'Original Unit',
			'or_rate' => 'Original Rate',
            'or_amount' => 'Original Amount',
            'hsn_code' =>'HSN Code',
			'estimation_approval_type' => 'Estimation Approval Type'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('item_id',$this->item_id);
		$criteria->compare('purchase_id',$this->purchase_id);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('quantity',$this->quantity);
		$criteria->compare('unit',$this->unit);
		$criteria->compare('rate',$this->rate);
		$criteria->compare('amount',$this->amount);
		$criteria->compare('bill_id',$this->bill_id);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('or_description',$this->or_description,true);
		$criteria->compare('or_quantity',$this->or_quantity);
		$criteria->compare('or_unit',$this->or_unit);
		$criteria->compare('or_rate',$this->or_rate);
		$criteria->compare('or_amount',$this->or_amount);
		$criteria->compare('permission_status',$this->permission_status);
		$criteria->compare('hsn_code',$this->hsn_code);
		$criteria->compare('estimation_approval_type',$this->estimation_approval_type);

		return new CActiveDataProvider($this, array('pagination' => array('pageSize' => 25),
            'sort' => array(
                'defaultOrder' => 'item_id DESC',
            ),'pagination' => array('pageSize' => 10),
            'criteria' => $criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PurchaseItems the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
