<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/pdfstyle.css">
                <style>
                    
                .title{
                 /* text-decoration:underline; */
                 border-bottom:1px solid;
                 
                }
                    table {
                        border-collapse: collapse;
                        width: 100%;
                        font-family: Arial, Helvetica, sans-serif;

                    }

                    .details,
                    .pdf-table {
                        border: 1px solid #ccc;
                        padding: 2rem;
                    }

                    .details td,
                    .details th,
                    .pdf-table td,
                    .pdf-table th {
                        padding: 5px 10px;
                        font-size: .85rem;
                        border: 1px solid #ccc;
                    }

                    .pdf-table {
                        margin-bottom: 10px;
                    }

                    .text-center {
                        text-align: center;
                    }

                    .img-hold {
                        width: 10%;
                    }

                    .companyhead {
                        font-size: 1rem;
                        font-weight: bold;
                        margin-top: 0px;
                        color: #789AD1;
                    }

                    .table-header th,
                    .details {

                        font-size: .75rem;
                        padding-left: 4px;
                        padding-right: 4px;
                    }

                    .medium-font {
                        font-size: .85rem;
                    }

                    .addrow th,
                    .addrow td {
                        font-size: .75rem;
                        border: 1px solid #333;
                        border-collapse: collapse;
                        padding-left: 4px;
                        padding-right: 4px;
                    }

                    .text-right {
                        text-align: right;
                    }

                    .w-50 {
                        width: 50%;
                    }

                    .text-sm {
                        font-size: 11px;
                    }

                    .dotted-border-bottom {
                        border-bottom: 2px dotted black;
                    }

                    .w-100 {
                        width: 100%;
                    }

                    .border-right {
                        border-right: 1px solid #ccc;
                    }

                    .border-0 {
                        border-right: 1px solid #fff !important;
                    }

                    .header-border {
                        border-bottom: 1px solid #000;
                    }
                    .border-bottom{
                        border-bottom:1px solid #ddd;
                    }
                    
                    p.terms{
                        display: block;
                        border-bottom: 1px solid black;
                        padding-bottom: 2px;                        
                        /* margin: auto;                         */
                        width: 100px;
                        text-align:center;
                        font-size:12px;
                    }
                    .border-bottom-0{
                        border-bottom:1px solid transparent;
                    }
                </style>

    <div class="table_h">
        
        <table cellpadding="10" class="table pdf-table item_table">
            
            <tbody>
                <tr class="thead">

                    <th width="6%">SI.NO</th>
                    <th width="40%">Description</th>
                    <th width="10%">HSN</th>
                    <th>Unit</th>
                    <th>Qty</th>                                    
                    <th>Rate</th>
                    <th>Amount</th>
                    

                </tr>
                <?php
                $sql = 'SELECT * FROM `jp_quotation_section` '
                        . 'WHERE `qtn_id` = ' . $qtid
                        . ' AND revision_no = "' . $rev_no . '"';
                $sectionArray = Yii::app()->db->createCommand($sql)->queryAll();
                $letters = range('A', 'Z');
                $k = 0;
                $sum = 0;
                foreach ($sectionArray as $key => $value) {
                    $sql = 'SELECT * FROM `jp_sales_quotation` WHERE `parent_type` = 0 '
                                . ' AND `parent_id` = ' . $value['id']
                                 . ' AND revision_no="' . $rev_no . '" AND `deleted_status` = 1';
                                           
                    $itemArray = Yii::app()->db->createCommand($sql)->queryAll();

                    $sql = 'SELECT * FROM `jp_quotation_gen_category` '
                            . 'WHERE `qid` = ' . $qtid . ' AND `section_id` = ' . $value["id"]
                            . ' AND revision_no = "' . $rev_no . '"';
                    $subsectionArray = Yii::app()->db->createCommand($sql)->queryAll();
                    $unit = Unit::model()->findByPk($value['unit']);
                    $secsum = Controller::getSectionSum($value['id'], $qtid, $rev_no);
                    
                    ?>
                    <tr>
                        <th  style="background:#c5d9f1;"><?php echo $letters[$k]; ?></th>
                        <th  style="background:#c5d9f1;"><?php echo $value['section_name'] ?></th>                                        
                        <td  style="background:#c5d9f1;"><?php echo $value['hsn_code'] ?></td>                                                                                                                    
                        <td  style="background:#c5d9f1;"><?php echo isset($unit) ? $unit->unit_name : "" ?></td>
                        <td  style="background:#c5d9f1;"><?php echo $value['quantity']; ?></td>
                        <td  style="background:#c5d9f1;" class="text-right"><?php //echo Controller::money_format_inr($value['mrp'] ,2) ?></td>
                        <td  style="background:#c5d9f1;" class="text-right"><?php //echo Controller::money_format_inr($value['amount_after_discount'],2) ?></td>
                    </tr>
                    <!-- items -->
                    <?php
                        
                        $j= 1;
                        foreach ($itemArray as $key => $item) {
                            $sum += $item['amount_after_discount'];
                            $unit = Unit::model()->findByPk($item['unit']);
                            $desc = ($item['description'] !="")?" - ".$item['description']:"";
                                if($item['description']=="" && ($item['shutterwork_description'] !="" || $item['carcass_description']!="")){
                                    $desc = " - <b>Shutter:</b> ".$item['shutterwork_description']." , <b>Carcass:</b> ".$item['carcass_description'];
                                }
                            ?>
                            <tr class="td_group">
                                <td><?php echo $j ?></td>
                                <td><?php echo $item['worktype_label'].$desc; ?></td>
                                <td><?php //echo $item['hsn_code'] ?></td>
                                <td><?php echo isset($unit) ? $unit->unit_name : "" ?></td>
                                <td><?php echo $item['quantity']; ?></td>
                                <td class="text-right"><?php echo Controller::money_format_inr($item['mrp'],2) ?></td>
                                <td class="text-right"><?php echo Controller::money_format_inr($item['amount_after_discount'],2) ?></td>
                            </tr>
                        <?php $j++; } ?>
                    <?php
                     $i = 1; 
                    foreach ($subsectionArray as $key => $value1) {
                        $sum += $value1['amount_after_discount'];
                        $unit = Unit::model()->findByPk($value1['unit']);
                        ?>
                        <tr>                                        
                            <td><?php echo $i; ?></th>
                            <td><?php echo $value1['category_label'] ?></th>                                            
                            <td><?php echo $value1['hsn_code'] ?></td>                                                
                            <td><?php echo isset($unit) ? $unit->unit_name : "" ?></td>
                            <td><?php echo ($model->template_type == '5') ? $value1['quantity']:$value1['quantity_nos']; ?></td>
                            <td class="text-right"><?php echo Controller::money_format_inr($value1['mrp'],2) ?></td>
                            <td class="text-right"><?php echo Controller::money_format_inr($value1['amount_after_discount'],2) ?></td>
                                
                            
                        </tr>                                                                                
                        <?php
                        $i++;   
                    }
                    ?>
                    <tr>
                        <td style="background:#c0c0c0;"></td>
                        <th style="background:#c0c0c0;" class="text-center">
                            Total Cost for <?php echo $value['section_name'];?>
                        </th>
                        <td style="background:#c0c0c0;"></td>
                        <td style="background:#c0c0c0;"></td>
                        <td style="background:#c0c0c0;"></td>
                        <td style="background:#c0c0c0;"></td>
                        <th class="text-right"  style="background:#c0c0c0;">
                        <!-- Controller::money_format_inr(Controller::getSubItemSum($qtid, $value2['id'],$rev_no),2) -->
                            <?php
                            $mainsum = Controller::getMainSectionSum($value['id'], $qtid, $_GET['rev_no']);
                            echo $mainsum; ?>
                        </th>
                    </tr>
                <?php $k++; } ?>
                <tr class="sum_sec">
                    <td colspan="6" class="text-right"><h5>MRP</h5></td>
                    <td class="text-right">
                        <h5><?php
                        $total_amount = Controller::getMrpSum($qtid, $rev_no,NULL);
                        echo Controller::money_format_inr($total_amount, 2);
                        ?></h5>
                    </td>
                </tr>
                <tr class="sum_sec">
                    <td colspan="6" class="text-right"><h5>Amount After Discount</h5></td>
                    <td class="text-right">
                        <h5><?php
                        $total_amount = $sum;
                        echo Controller::money_format_inr($total_amount , 2);
                        ?></h5>
                    </td>
                </tr>
                <tr class="sum_sec">
                    <td colspan="6" class="text-right"><h5>Amount After GST</h5></td>
                    <td  class="text-right border-bottom">
                        <h5><?php
                        $sql = 'SELECT * FROM jp_quotation_revision '
                                . ' WHERE qid='.$qtid
                                . ' AND revision_no="'.$rev_no.'"';
                        $revision_model = Yii::app()->db->createCommand($sql)->queryRow();
                        $total_tax = $revision_model['sgst_amount']+
                                    $revision_model['cgst_amount']+
                                    $revision_model['igst_amount'];
                        $total_amoun_withtax = $total_amount + $total_tax;
                        echo Controller::money_format_inr($total_amoun_withtax, 2); ?></h5>
                    </td>
                </tr>
                <tr class="sum_sec">
                    <td colspan="6" class="text-right"><h5>Total Amount After Discount</h5></td>
                    <td class="text-right">
                        <h5><?php
                        $total_after_discount = $total_amoun_withtax - $revision_model['discount_amount'];
                        echo Controller::money_format_inr($total_after_discount , 2);                                        
                        ?></h5>
                    </td>
                </tr>
                <?php if(!is_null($revision_model['total_after_additionl_discount']) && ($revision_model['total_after_additionl_discount'] !=0)){ ?>
                <tr class="sum_sec">
                    <td colspan="6" class="text-right"><h5>Amount after additional discount</h5></td>
                    <td class="text-right">
                        <h5><?php
                        $addition_discount_amount = $revision_model['total_after_additionl_discount'];
                        echo Controller::money_format_inr($addition_discount_amount,2); 
                        ?></h5>
                    </td>
                </tr>
                <?php } ?>
            </tbody>
        </table>

        <div style="border:1px solid #ccc;height:200px;">
         
            <table class=" no-border terms_table">
                <tbody>
                    <tr>
                        <th class="text-left" width="50%">
                            <h6>Amount in Words: INR <?php echo Yii::app()->Controller->displaywords($total_after_discount) ?></h6>
                            <?php if($total_tax >0) { ?>
                            <h6>Tax Amount in Words: <?php echo Yii::app()->Controller->displaywords($total_tax) ?>Only</h6>
                            <?php } ?>
                        </th>
                        <th class="text-right" width="50%"><h6>For CONCORD DESIGN STUDIO - <?php echo date('Y').'-'.date('y', strtotime('+1 year')) ?></h6></th>
                    </tr>
                    
                </tbody>
            </table>
                </div>
        <div style="border:1px solid #ccc;height:auto;">
         
            <table class=" no-border terms_table">
                <tbody>
                    <tr>
                        <th><div class="title">TERMS AND CONDITIONS<br></div></th>
                    </tr>
                    <tr>
                        <td>
                            <ol class="order_list">
                            <?php
                            $terms = $terms = Yii::app()->db->createCommand("SELECT * FROM `jp_terms_conditions` WHERE `template_id` = 1")->queryRow();
                            echo $terms['terms_and_conditions'];
                            ?>
                            </ol>
                        </td>
                    </tr>
                </tbody>
            </table>
                </div>
                <div style="border:1px solid #ccc;height:auto;">
            <table class=" no-border terms_table">
                <tbody>
                    <tr>
                        <th><div class="title">PAYMENT TERMS<br></div></th>
                    </tr>
                    <tr>
                        <td>
                           <ol class="order_list">
                                <li>Booking amount:
                                    <div class="ml-3"> a) Quotation value up to 10 lakhs - Rs . 40,000/-</div>
                                    <div class="ml-3"> b) Quotation value above 10 lakhs - Rs . 60,000/-</div>
                                </li>
                                <li>First slab: 15% of the total quote after finalizing design and before proceeding for 3D drawing / Electrical and plumbing marking etc. at site</li>
                                <li>Second slab: 35% advance of the total quote to start the production deducting initial Payments.</li>
                                <li>Third slab: 50% of total quote deducting Rs. 20,000/- after the production is over and before materials are dispatched to the site from the factory.</li>
                                <li>Final payment: Rs. 20,000/- within 3 days after completion of works.</li>
                           </ol>
                           <p>Note: In case of contract not getting materialized, booking amount will not be refunded</p>
                        </td>
                    </tr>
                </tbody>
            </table>
            </div>
                <div style="border:1px solid #ccc;height:auto;">
            <table class=" terms_table">
                <tbody>
                    <tr>
                        <th><div class="title">WARRANTY<br></div></th>
                    </tr>
                    <tr>
                        <td>
                           <ol class="order_list">
                                <li>Plywood: 303 grade plywood - 1 year. 710 grade marine plywood - 15 years. 710 grade full gurjan marine plywood-20 years</li>
                                <li>HDHMR: 5 year warranty for HDHMR(High Density High Moisture Resistant) boards and 1 year for MDF.</li>
                                <li>Particle board: No warranty for particle boards.</li>
                                <li>Service: 2 scheduled free services for the first 1 year. This will cover any issue related to workmanship. Paid service for any issues arising from improper handling from handing over till lifetime.</li>
                                <li>Hardware accessories: Lifetime warranty for all SS304 grade accessories. 1 year warranty for accessories of other materials. No warranty for rusting of MS materials</li>
                                <li>Epoxy work : 3 years warranty for any issues arising from epoxy works.</li>
                           </ol>
                           
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        
    </div>
</div>

