<?php
/* @var $this BillsController */
/* @var $model Bills */
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<style type="text/css">
    tr.pitems td input, tr.pitems td div.pricediv {
        text-align: right!important;
    }
    .table{margin-top: 10px;}
</style>
<div class="row">
    <div class="col-sm-12 table-responsive">
        <table class="table">
            <thead class="thead-inverse">
                <tr>
                    <th>Specifications/Remark</th>
                    <th>Rejected Quantity</th>
                    <th>Billed Quantity</th>
                    <th>Remaining Quantity</th>
                    <th>Defect Return Quantity</th>
                    <th>Unit</th>
                    <th>Rate</th>
                    <th>Amount</th>                    
                    <th></th>
                </tr>
            </thead>
            <tbody>
            <?php
            $number    = 1;
            $amount    = 0;
            $totalQty  = 0;
            $bTotalQty = 0;            
            foreach($purchase as $item) {
                if($item['pcategory']) {
                     $categorymodel = Specification::model()->findByPk($item['pcategory']);
                    if($categorymodel)
                        $categoryName = $categorymodel->specification;
                    else
                        $categoryName = "Not available";
                } else {
                   $categoryName  = $item['bremark']?$item['bremark']:$item['premark'];
                }
                $purchasemodel = WarehousereceiptItems::model()->findByPk($item['item_id']);
                $itemQuantity  = $purchasemodel->warehousereceipt_baseunit_rejected_effective_quantity;
                $tblpx         = Yii::app()->db->tablePrefix;
                $itemData      = Yii::app()->db->createCommand("SELECT SUM(returnitem_quantity) as quantitysum FROM ".$tblpx."defect_returnitem WHERE receiptitem_id = ".$item['item_id'])->queryRow();  
                $bItemQuantity = $itemData['quantitysum']?$itemData['quantitysum']:0;
                if(strpos($bItemQuantity,".") !== false){
                    $bItemQuantity = number_format((float)$bItemQuantity, 2, '.', '');
                }else{
                    $bItemQuantity = $bItemQuantity;
                }
                $quantityAvail =  $itemQuantity - $bItemQuantity;               
                $totalQty      = $totalQty + $itemQuantity;
                $bTotalQty     = $bTotalQty + $bItemQuantity;
            ?>
                
                <tr class="pitems">
                    <input type="hidden" id="ids<?php echo $number-1; ?>" value ="<?php echo $item["item_id"]; ?>" class="txtBox pastweek ids" name="ids[]"/>
                    <input type="hidden" id="category<?php echo $number-1; ?>" value ="<?php echo $item["pcategory"]?$item["pcategory"]:''; ?>" class="txtBox pastweek category" name="category[]"/>
                    
                    <?php $totalAmount = $item["returnitem_amount"]?$item["returnitem_amount"]:0; ?>
                    
                    <input type="hidden" id="biremquantity<?php echo $number-1; ?>" value="<?php echo $quantityAvail; ?>" name="biremquantity[]"/>
                    <td id="<?php echo $number-1; ?>"><div id="bicategoryname<?php echo $number-1; ?>"><?php echo $categoryName; ?></div></td>
                    
                    <td id="<?php echo $number-1; ?>"><div id="actualquantity<?php echo $number-1; ?>"><?php echo $itemQuantity; ?></div></td>
                    <td id="<?php echo $number-1; ?>"><div id="billedquantity<?php echo $number-1; ?>"><?php  echo  $bItemQuantity; ?></div></td>
                    <input type="hidden" id="orrate<?php echo $number-1; ?>" value ="<?php echo $item["returnitem_rate"]?number_format((float)$item["returnitem_rate"], 2, '.', ''):number_format((float)$item["warehousereceipt_rate"], 2, '.', ''); ?>" name="orrate[]"/>
                    <td id="<?php echo $number-1; ?>"><div id="availablequantity<?php echo $number-1; ?>"><?php echo $quantityAvail; ?></div></td>
                    <td id="<?php echo $number-1; ?>"><input type="text" id="biquantity<?php echo $number-1; ?>" value ="<?php echo $item["returnitem_quantity"]?$item["returnitem_quantity"]:0; ?>" class="form-control fnext validate[required,custom[number],min[0],max[<?php echo $quantityAvail; ?>]]" name="biquantity[]"/></td>
                    <td id="<?php echo $number-1; ?>"><div id="biunit<?php echo $number-1; ?>"><?php echo $item["returnitem_unit"]?$item["returnitem_unit"]:$item["warehousereceipt_itemunit"]; ?></div></td>
                    <td id="<?php echo $number-1; ?>"><input type="text" id="birate<?php echo $number-1; ?>" value ="<?php echo $item["returnitem_rate"]?number_format((float)$item["returnitem_rate"], 2, '.', ''):number_format((float)$item["warehousereceipt_rate"], 2, '.', ''); ?>" class="form-control lnext validate[required,custom[number],min[0]]" name="birate[]"/></td>
                    <td id="<?php echo $number-1; ?>"><div id="biamount<?php echo $number-1; ?>" class="pricediv"><?php echo $item["returnitem_amount"]?number_format((float)$item["returnitem_amount"], 2, '.', ''):0; ?></div></td>                    
                    <input type="hidden" id="billitem<?php echo $number-1; ?>" value ="<?php echo $item["returnitem_id"] ?>" name="billitem[]"/>
                    <input type="hidden" id="savedquantity<?php echo $number-1; ?>" value ="<?php echo $item["returnitem_quantity"]?$item["returnitem_quantity"]:0; ?>" name="savedquantity[]"/>
                    <td><div id="tickmark<?php echo $number-1; ?>"><?php if($item["returnitem_quantity"]!=0) { ?><i class="fa fa-check" aria-hidden="true"></i><?php } ?></div></td>
                </tr>
            <?php
            $amount = $amount ;
            $number = $number + 1;
            }
            ?>
                <input type="hidden" name="tqty[]" id="tqty" value="<?php echo $totalQty; ?>"/>
                <input type="hidden" name="btqty[]" id="btqty" value="<?php echo $bTotalQty; ?>"/>
                <input type="hidden" id="totrows" value ="<?php echo $number-1; ?>" class="txtBox pastweek totrows" name="totrows"/>
            </tbody>
        </table>
        
    </div>
</div>
