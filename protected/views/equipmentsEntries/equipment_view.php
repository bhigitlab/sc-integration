<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/validation/jquery.validate.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/validation/additional-methods.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/tableHeadFixer.js"></script>

<?php
/* @var $this Controller */
/* @var $model EquipmentsEntries */
/* @var $modelItems EquipmentsItems[] */

?>

<div class="container">
    <div class="expenses-heading header-container">
    <h3>EQUIPMENT ENTRY</h3>
    <?php if($model->status == 1) { ?>
        <div class="btn-container">
        <button class="btn btn-xs btn-info" id="<?php echo $model->id ?>" href="#"
        onClick="approve('<?php echo $model->id; ?>', this)" title="Approve">Approve</button>
        </div>
    
    <?php } ?>
    </div>
    <div id="errormessage"></div>
    <br><br>
    <div class="row">
        <div class="col-md-6">
            <p><strong>PROJECT :</strong> <?php 
                $pmodel  = Projects::model()->findByPk($model->project_id);
                echo isset($pmodel->name) ? CHtml::encode($pmodel->name) : '';
                ?>
            </p>
            <p><strong>COMPANY :</strong> <?php 
                $cmodel = Company::model()->findByPk($model->company_id);
                echo isset($cmodel->name) ? CHtml::encode($cmodel->name) : '';
            ?></p>
            <p><strong>DESCRIPTION :</strong> <?= CHtml::encode($model->description); ?></p>
        </div>
        <div class="col-md-6">
            <p><strong>DATE :</strong> <?= CHtml::encode(date('d-m-Y', strtotime($model->Date))); ?></p>
            <p><strong>STATUS :</strong> <?php 
                if($model->status == 1){
                    echo CHtml::encode( 'Pending');
                }else if($model->status == 2){
                    echo CHtml::encode( 'Approved');
                }
                else if($model->status == 3){
                    echo CHtml::encode( 'Rejected');
                }
                    ?></p>
            <p><strong>REMARKS :</strong> <?= CHtml::encode($model->remarks); ?></p>
        </div>
    </div>
    <ul class="legend">
        <li><span style="background-color:yellow;"></span> Equipment Not Estimated For This Project in Estimation</li>
    </ul>
    <table class="table table-bordered">
        <thead>
            <tr>
                <th>Sl.No</th>
                <th>Equipment Name</th>
                <th>Quantity</th>
                <th>Unit</th>
                
            </tr>
        </thead>
        <tbody>
            <?php 
            $bgcolor_status='';
            $totalAmount = 0; 
            foreach ($modelItems as $index => $item): 
                $totalAmount += $item['amount'];
                // Accumulate the total
                $equipment=$item["equipments"];
            $sql="SELECT m.id,m.equipment,m.quantity,m.amount,m.used_quantity,m.used_amount FROM `jp_equipments_estimation` m LEFT JOIN `jp_itemestimation` e ON e.itemestimation_id=m.estimation_id WHERE e.project_id=".$model->project_id." AND e.itemestimation_status=2  AND m.project_id=".$model->project_id ." AND m.equipment=".$item["equipments"];
            $estimated_det = Yii::app()->db->createCommand($sql)->queryRow();
            $equipment_dets = Equipments::model()->findByPk($equipment);
            $equipment_name=$equipment_dets["equipment_name"];
          // echo '<pre>';print
                // die($sql);
            if (!$estimated_det) {
                $bgcolor_status = 'style ="background-color:yellow"';
            }
                ?>
                <tr>
                    <td><?= $index + 1; ?></td>
                    <td <?php echo $bgcolor_status; ?>><?= CHtml::encode($equipment_name); ?></td>
                    <td><?= CHtml::encode($item->quantity); ?></td>
                    <td><?php 
                        $unitModel = Unit::model()->findByPk($item->unit);
                        echo isset($unitModel->unit_name) ? CHtml::encode($unitModel->unit_name) : 'N/A'; 
                    ?></td>
                   
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>

  
</div>

<style>
    .container {
        width: 80%;
        margin: auto;
    }
    .table {
        width: 100%;
        margin-top: 20px;
        border-collapse: collapse;
    }
    .table, .table th, .table td {
        border: 1px solid #000;
        text-align: center;
    }
    .total {
        margin-top: 20px;
        text-align: right;
    }
</style>
<script>
    function approve(id) {
    $('.loading-overlay').addClass('is-active');
    $.ajax({
        url: '<?php echo Yii::app()->createAbsoluteUrl('EquipmentsEntries/approve'); ?>',
        type: 'POST',
        data: { id: id },
        dataType: 'json', // Expect JSON response
        success: function (response) {
            if (response.status == 1) {
                $("#errorMsg").show()
                    .html('<div class="alert alert-success"><b>Success!</b> ' + response.message + '</div>')
                    .fadeOut(5000);
                setTimeout(function () {
                    location.reload();
                }, 3000);
            } else if (response.response == 'warning') {

            var warningAlert = '<div class="alert alert-warning alert-dismissible" role="alert">' +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                '</button>' +
                '<strong>Sorry!</strong> ' + response.msg +
            '</div>';
            $("#errorMsg").html(warningAlert).show();
            }
            
        },
        error: function () {
            $("#errorMsg").show()
                .html('<div class="alert alert-danger"><b>Error Occurred.</b></div>')
                .fadeOut(5000);
        }
    });
}
</script>