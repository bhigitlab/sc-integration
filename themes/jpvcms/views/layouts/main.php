<?php
Yii::app()->clientScript->registerCoreScript('jquery');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <meta name="language" content="en" />
            <link rel="icon" type="image/png" href="<?php echo $this->favicon; ?>" />
            <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/bootstrap.min.css" />
            <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/bootstrap-theme.min.css" />
            <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/style.css?<?php echo time(); ?>" />
            <link type="text/css" rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/font-awesome.min.css">
                <link type="text/css" rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/jquery-ui.css">
                    <link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.4.1/css/simple-line-icons.css" />
                    <title>
                        <?php echo CHtml::encode($this->pageTitle); ?> </title>
                    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery-ui.js"></script>
                    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/bootstrap.min.js"></script>
                    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jspdf.js"></script>
                    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/html2canvas.js"></script>
                    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.plugin.html2canvas.js"></script>
                    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/base64.js"></script>
                    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/canvas2image.js"></script>

                  
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
            
            <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/dashboard/new-styles.css" />
            <link rel="stylesheet" typw="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/dashboard/dashboard.css"/>
                    <style>
                        .app_switchmenu{
                            background-color:#ff9393;
                            color:black !important;
                        }

                    </style>
                    <?php
                    $buyer_module = GeneralSettings::model()->checkBuyerModule();
                    //echo '<pre>';print_r(Yii::app()->user->menuauthlist);exit;
                    
                    ?>
    </head>

                    <body>
                        <div class="page-wrapper"  id="page-wrapper">
                            <nav class="navbar navbar-default navbar-fixed-top">
                                <div class="">
                                    <div class="navbar-header  clearfix d-flex">
                                        <div class="logo-wrapper">
                                          <!-- <img src="dashboard-images/company-logo.png" alt="BHI COST Management System" /> -->
                                          <?php
                                            echo CHtml::image($this->logo, Yii::app()->name, array('style' => 'max-height:50px;max-width: 70px;'));
                                            ?>
                                        </div>
                                        <div class="coms-block">
                                            COMS <span class="coms-arrow">&#9660;</span>
                                            <div class="coms-content">
                                                <div class="coms-container">
                                                <a class="coms-item active" href="#" title="PMS">
                                                    <img src="<?php echo Yii::app()->baseUrl . '/themes/assets/coms-pms-icon.png'; ?>" alt="PMS" />
                                                    <span>PMS</span>
                                                </a>
                                                <a class="coms-item active current" href="#" title="CoMS">
                                                    <img src="<?php echo Yii::app()->baseUrl . '/themes/assets/coms-coms-icon.png'; ?>" alt="CoMS" />
                                                    <span>CoMS</span>
                                                </a>
                                                <a class="coms-item active" href="#" title="CRM">
                                                    <img src="<?php echo Yii::app()->baseUrl . '/themes/assets/coms-crm-icon.png'; ?>" alt="CRM" />
                                                    <span>CRM</span>
                                                </a>
                                                <a class="coms-item active" href="#" title="HRMS">
                                                    <img src="<?php echo Yii::app()->baseUrl . '/themes/assets/coms-hrms-icon.png'; ?>" alt="HRMS" />
                                                    <span>HRMS</span>
                                                </a>
                                                <a class="coms-item inactive" href="#" title="Q/BoQ">
                                                    <img src="<?php echo Yii::app()->baseUrl . '/themes/assets/coms-boq-icon.png'; ?>" alt="Q/BoQ" />
                                                    <span>Q/BoQ</span>
                                                </a>
                                                <a class="coms-item inactive" href="#" title="DMS">
                                                    <img src="<?php echo Yii::app()->baseUrl . '/themes/assets/coms-dms-icon.png'; ?>" alt="DMS" />
                                                    <span>DMS</span>
                                                </a>
                                                <a class="coms-item inactive" href="#" title="ProMS">
                                                    <img src="<?php echo Yii::app()->baseUrl . '/themes/assets/coms-proms-icon.png'; ?>" alt="ProMS" />
                                                    <span>ProMS</span>
                                                </a>
                                                <a class="coms-item inactive" href="#" title="IMS">
                                                    <img src="<?php echo Yii::app()->baseUrl . '/themes/assets/coms-ims-icon.png'; ?>" alt="IMS" />
                                                    <span>IMS</span>
                                                </a>
                                                <a class="coms-item inactive" href="#" title="TMS">
                                                    <img src="<?php echo Yii::app()->baseUrl . '/themes/assets/coms-tms-icon.png'; ?>" alt="TMS" />
                                                    <span>TMS</span>
                                                </a>
                                                <a class="coms-item inactive" href="#" title="SMS">
                                                    <img src="<?php echo Yii::app()->baseUrl . '/themes/assets/coms-sms-icon.png'; ?>" alt="SMS" />
                                                    <span>SMS</span>
                                                </a>
                                                <a class="coms-item inactive" href="#" title="RMS">
                                                    <img src="<?php echo Yii::app()->baseUrl . '/themes/assets/coms-rms-icon.png'; ?>" alt="RMS" />
                                                    <span>RMS</span>
                                                </a>
                                                </div>
                                            </div>
                                        </div>
                                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                        </button>
                                        
                                        <?php
                                        //Change user login from Direct admin user
                                        ?>
                                    </div>
                                    <div class=" collapse navbar-collapse" id="myNavbar">
                                        <div class="nav navbar-nav navbar-right">
                                            <ul class="nav navbar-nav">
                                                <li class="dropdown dropdown-user dropdown-dark">
                                                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-close-others="true" style="padding-top: 2px;padding-bottom: 2px;">
                                                        <img style="height:25px; vertical-align:middle;" alt="" class="img-circle" src="<?= Yii::app()->theme->baseUrl ?>/images/avatar.png" />
                                                    </a>
                                                    <ul class="dropdown-menu dropdown-menu-default">
                                                        <li>
                                                            <?php echo CHtml::link('Change Password', array('/users/passchange')); ?>
                                                        </li>
                                                        <li>
                                                            <?php echo CHtml::link('Logout (' . (isset(Yii::app()->user->name) ? Yii::app()->user->name : '') . ')', array('/site/logout')); ?>
                                                        </li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </div>

                                        <?php
                                        //echo "<pre>";print_r(Yii::app()->session['menuauthlist']);die;
                                        $user = Users::model()->findByPk(Yii::app()->user->id);
                                        $arrVal = explode(',', $user->company_id);
                                        $newQuery = "";
                                        foreach ($arrVal as $arr) {
                                            if ($newQuery)
                                                $newQuery .= ' OR';
                                            $newQuery .= " FIND_IN_SET('" . $arr . "', id)";
                                        }
                                        $remainder_newQuery = "";
                                        foreach ($arrVal as $arr) {
                                            if ($remainder_newQuery)
                                                $remainder_newQuery .= ' OR';
                                            $remainder_newQuery .= " FIND_IN_SET('" . $arr . "', company_id)";
                                        }
                                        $tblpx = Yii::app()->db->tablePrefix;
                                        $date = DATE('Y-m-d');
                                        $sql = "SELECT count(id) as count FROM {$tblpx}payment_reminders WHERE ($remainder_newQuery) AND (status = 'Open' OR status = 'Extended') AND viewed_status=0 AND date='" . $date . "' ";
                                        //die($sql);
                                        $remincount = Yii::app()->db->createCommand($sql)->queryRow();
                                        ?>
                                        <?php
                                        if (isset(Yii::app()->user->role)) {
                                            // switch apps
                                            $allowed_apps = Controller::getAllowedApps();
                                            $allowed_apps_menu = array();
                                            if (is_array($allowed_apps)) {
                                                foreach ($allowed_apps as $app) {
                                                    if ($app == 'accounts') {
                                                        continue;
                                                    }
                                                    $appName = strtoupper($app);
                                                    $appsImgIcon = CHtml::image('images/' . $app . '.png', $appName);
                                                    $allowed_apps_menu[$app] = array(
                                                        'label' => $appsImgIcon,
                                                        'url' => array('/site/redirect', 'type' => $appName),
                                                        'linkOptions' => array('target' => '_blank'),
                                                    );
                                                }
                                            }
                                            ?>
                                            <?php
                                            $this->widget('zii.widgets.CMenu', array(
                                                'activeCssClass' => 'active',
                                                'encodeLabel' => false,
                                                'activateParents' => true,
                                                'items' => array(
                                                    array('label' => 'Home', 'url' => (in_array('/dashboard/index', Yii::app()->user->menuauthlist) ? array('/dashboard/index') : array('/dashboard/home')), 'visible' => (isset(Yii::app()->user->role))),
                                                    array(
                                                        'label' => 'Purchase', 'url' => '#', 'visible' => (isset(Yii::app()->user->role) && ((in_array('/purchase/admin', Yii::app()->user->menuauthlist)) || (in_array('/bills/admin', Yii::app()->user->menuauthlist)) || (in_array('/purchase/previouspurchase', Yii::app()->user->menuauthlist)))), 'linkOptions' => array('class' => 'dropdown-toggle', 'data-toggle' => 'dropdown'),
                                                        'items' => array(
                                                            array('label' => 'Material Requisition', 'url' => array('/MaterialRequisition/index'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/purchase/materialrequisition', Yii::app()->user->menuauthlist)))),
                                                           // array('label' => 'Material Entries', 'url' => array('/materialEntries/create'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/materialEntries/create', Yii::app()->user->menuauthlist)))),
                                                           
                                                            array('label' => 'Purchase Orders', 'url' => array('/purchase/admin'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/purchase/admin', Yii::app()->user->menuauthlist)))),
                                                            array('label' => 'Purchase Bills', 'url' => array('/bills/admin'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/bills/admin', Yii::app()->user->menuauthlist)))),
                                                           
                                                            // array('label' => 'Estimation', 'url' => array('/materialEstimation/index'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/materialEstimation/index', Yii::app()->user->menuauthlist)))),
                                                            array('label' => 'Previous Purchase', 'url' => array('/purchase/previouspurchase'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/purchase/previouspurchase', Yii::app()->user->menuauthlist)))),
                                                            array('label' => 'Purchase Return', 'url' => array('/purchaseReturn/admin'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/purchaseReturn/admin', Yii::app()->user->menuauthlist)))),
                                                        )
                                                    ),
                                                    array(
                                                        'label' => 'WAREHOUSE', 'url' => '#', 'visible' => (isset(Yii::app()->user->role) && ((in_array('/wh/warehouse/index', Yii::app()->user->menuauthlist)) || (in_array('/wh/warehousereceipt/index', Yii::app()->user->menuauthlist)) || (in_array('/wh/warehousedespatch/index', Yii::app()->user->menuauthlist)))), 'linkOptions' => array('class' => 'dropdown-toggle', 'data-toggle' => 'dropdown'),
                                                        'items' => array(
                                                            array('label' => 'Warehouse ', 'url' => array('/wh/warehouse/index'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/wh/warehouse/index', Yii::app()->user->menuauthlist)))),
                                                            array('label' => 'Warehouse Receipt ', 'url' => array('/wh/warehousereceipt/index'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/wh/warehousereceipt/index', Yii::app()->user->menuauthlist)))),
                                                            array('label' => 'Warehouse Despatch', 'url' => array('/wh/warehousedespatch/index'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/wh/warehousedespatch/index', Yii::app()->user->menuauthlist)))),
                                                            array('label' => 'Stock Correction', 'url' => array('/wh/warehouse/stockcorrection'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/wh/warehouse/stockcorrection', Yii::app()->user->menuauthlist)))),
                                                            array('label' => 'Defect Return', 'url' => array('/wh/warehouse/defectreturn'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/wh/warehouse/defectreturn', Yii::app()->user->menuauthlist)))),
                                                           
                                                        )
                                                    ),
                                                    array(
                                                        'label' => 'Sales', 'url' => '#', 'visible' => (isset(Yii::app()->user->role) && ((in_array('/invoice/admin', Yii::app()->user->menuauthlist)) || (in_array('/quotation/admin', Yii::app()->user->menuauthlist)) || (in_array('/salesquotation/salesQuotation/index', Yii::app()->user->menuauthlist)))), 'linkOptions' => array('class' => 'dropdown-toggle', 'data-toggle' => 'dropdown'),
                                                        'items' => array(
                                                            array('label' => 'Quotation', 'url' => array('/quotation/admin'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/quotation/admin', Yii::app()->user->menuauthlist)))),
                                                            array('label' => 'Sales', 'url' => array('/invoice/admin'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/invoice/admin', Yii::app()->user->menuauthlist)))),
                                                            array('label' => 'Quotation Generator', 'url' => array('/salesquotation/salesQuotation/index'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/salesquotation/salesQuotation/index', Yii::app()->user->menuauthlist)))),
                                                            array('label' => 'Terms And Conditions', 'url' => array('/salesquotation/salesQuotation/termsandconditions'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/salesquotation/salesQuotation/termsandconditions', Yii::app()->user->menuauthlist)))),
                                                        )
                                                    ),
                                                    array('label' => 'Day Book', 'url' => array('/expenses/dailyEntries'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/expenses/dailyEntries', Yii::app()->user->menuauthlist)))),
                                                    array('label' => 'Daily Expenses', 'url' => array('/dailyexpense/expenses'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/dailyexpense/expenses', Yii::app()->user->menuauthlist)))),
                                                    array(
                                                        'label' => 'Buyer', 'url' => '#', 'visible' => (isset(Yii::app()->user->role) && ($buyer_module) && ((in_array('/buyer/buyers/admin', Yii::app()->user->menuauthlist)) || (in_array('/buyer/buyers/transactions', Yii::app()->user->menuauthlist)) || (in_array('/buyer/buyers/buyerinvoice', Yii::app()->user->menuauthlist)))), 'linkOptions' => array('class' => 'dropdown-toggle', 'data-toggle' => 'dropdown'),
                                                        'items' => array(
                                                            array('label' => 'Buyers ', 'url' => array('/buyer/buyers/admin'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/buyer/buyers/admin', Yii::app()->user->menuauthlist)))),
                                                            array('label' => 'Buyer Transactions ', 'url' => array('/buyer/buyers/transactions'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/buyer/buyers/transactions', Yii::app()->user->menuauthlist)))),
                                                            array('label' => 'Buyer Invoice ', 'url' => array('/buyer/buyers/buyerinvoice'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/buyer/buyers/buyerinvoice', Yii::app()->user->menuauthlist)))),
                                                        )
                                                    ),
                                                    array(
                                                        'label' => 'Labour', 'url' => '#', 'visible' => (isset(Yii::app()->user->role) && ((in_array('/dailyReport/create', Yii::app()->user->menuauthlist)) || (in_array('/labours/newlist', Yii::app()->user->menuauthlist)))), 'linkOptions' => array('class' => 'dropdown-toggle', 'data-toggle' => 'dropdown'),
                                                        'items' => array(
                                                            array('label' => 'Labour Master', 'url' => array('/labours/newlist'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/labours/newlist', Yii::app()->user->menuauthlist)))),
                                                            array('label' => 'Labour Template', 'url' => array('/labtemplate/newlist'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/labtemplate/newlist', Yii::app()->user->menuauthlist)))),
                                                            array('label' => 'Labour Entries', 'url' => array('/dailyReport/create'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/dailyReport/create', Yii::app()->user->menuauthlist)))),
                                                        )
                                                    ),
                                                    array(
                                                        'label' => 'Vendors', 'url' => '#', 'visible' => (isset(Yii::app()->user->role) && ((in_array('/vendors/newlist', Yii::app()->user->menuauthlist)) || (in_array('/vendors/dailyEntries', Yii::app()->user->menuauthlist)))), 'linkOptions' => array('class' => 'dropdown-toggle', 'data-toggle' => 'dropdown'),
                                                        'items' => array(
                                                            array('label' => 'Vendor list', 'url' => array('/vendors/newlist'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/vendors/newlist', Yii::app()->user->menuauthlist)))),
                                                            array('label' => 'Vendor Payments', 'url' => array('/vendors/dailyEntries'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/vendors/dailyEntries', Yii::app()->user->menuauthlist)))),
                                                            array('label' => 'Vouchers', 'url' => array('/vouchers/index'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/vendors/dailyEntries', Yii::app()->user->menuauthlist)))),
                                                        )
                                                    ),
                                                    array(
                                                        'label' => 'Sub Contractors', 'url' => '#', 'visible' => (isset(Yii::app()->user->role) && ((in_array('/subcontractor/newlist', Yii::app()->user->menuauthlist)) || (in_array('/subcontractor/quotations', Yii::app()->user->menuauthlist)) || (in_array('/subcontractorpayment/dailyentries', Yii::app()->user->menuauthlist)))), 'linkOptions' => array('class' => 'dropdown-toggle', 'data-toggle' => 'dropdown'),
                                                        'items' => array(
                                                            array('label' => 'Sub Contractor list', 'url' => array('/subcontractor/newlist'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/subcontractor/newlist', Yii::app()->user->menuauthlist)))),
                                                            array('label' => 'Sub Contractor Quotations', 'url' => array('/subcontractor/quotations'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/subcontractor/quotations', Yii::app()->user->menuauthlist)))),
                                                            array('label' => 'Sub Contractor Payments', 'url' => array('/subcontractorpayment/dailyentries'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/subcontractorpayment/dailyentries', Yii::app()->user->menuauthlist)))),
                                                            array('label' => 'Sub Contractor Bill', 'url' => array('/subcontractorbill/admin'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/subcontractorbill/admin', Yii::app()->user->menuauthlist)))),
                                                        )
                                                    ),
                                                     array(
                                                        'label' => 'RMS', 'url' => '#','title'=> 'Resource Management System', 'visible' => (isset(Yii::app()->user->role) && ((in_array('/dailyReport/create', Yii::app()->user->menuauthlist)) || (in_array('/purchase/itemestimation', Yii::app()->user->menuauthlist))  || (in_array('/wh/warehouse/consumptionrequest', Yii::app()->user->menuauthlist)) || (in_array('/equipmentsEntries/create', Yii::app()->user->menuauthlist)))), 'linkOptions' => array('class' => 'dropdown-toggle', 'data-toggle' => 'dropdown'),
                                                        'items' => array(
                                                            array('label' => 'Consumption Request', 'url' => array('/wh/warehouse/consumptionrequest'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/wh/warehouse/consumptionrequest', Yii::app()->user->menuauthlist)))),
                                                             array('label' => 'Equipments Entries', 'url' => array('/equipmentsEntries/create'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/equipmentsEntries/create', Yii::app()->user->menuauthlist)))),
                                                            array('label' => 'Estimation', 'url' => array('/purchase/itemestimation'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/purchase/itemestimation', Yii::app()->user->menuauthlist)))),
                                                            array('label' => 'Labour Entries', 'url' => array('/dailyReport/create'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/dailyReport/create', Yii::app()->user->menuauthlist)))),
                                                        )
                                                    ),
                                                    array(
                                                        'label' => 'Settings', 'url' => '#', 'visible' => (isset(Yii::app()->user->role) && ((in_array('/projects/newlist', Yii::app()->user->menuauthlist)) ||
                                                        (in_array('/projects/completedprojects', Yii::app()->user->menuauthlist)) || (in_array('/clients/newlist', Yii::app()->user->menuauthlist)) ||
                                                        (in_array('/projectType/newlist', Yii::app()->user->menuauthlist)) || (in_array('/userRoles/newlist', Yii::app()->user->menuauthlist)) ||
                                                        (in_array('/users/index', Yii::app()->user->menuauthlist)) || (in_array('/expensetype/newlist', Yii::app()->user->menuauthlist)) ||
                                                        (in_array('/companyexpensetype/admin', Yii::app()->user->menuauthlist)) || (in_array('/workType/newlist', Yii::app()->user->menuauthlist)) ||
                                                        (in_array('/company/newlist', Yii::app()->user->menuauthlist)) || (in_array('/purchaseCategory/newList', Yii::app()->user->menuauthlist)) ||
                                                        (in_array('/unit/newlist', Yii::app()->user->menuauthlist)) || (in_array('/bank/newList', Yii::app()->user->menuauthlist)) ||
                                                        (in_array('/cashbalance/newlist', Yii::app()->user->menuauthlist)) || (in_array('/cashbalance/newlist', Yii::app()->user->menuauthlist)) ||
                                                        (in_array('/log/historylog', Yii::app()->user->menuauthlist)) || (in_array('/bank/reconciliation', Yii::app()->user->menuauthlist)) ||
                                                        (in_array('/expenses/menuPermissions/create', Yii::app()->user->menuauthlist)) || (in_array('/expenses/duplicateentry', Yii::app()->user->menuauthlist)) ||
                                                        (in_array('/quotationcategory/quotationCategoryMaster/index', Yii::app()->user->menuauthlist)) ||
                                                        (in_array('quotationitem/quotationItemMaster/index', Yii::app()->user->menuauthlist)) ||
                                                        (in_array('/finish/quotationFinish/index', Yii::app()->user->menuauthlist)) ||
                                                        (in_array('/worktype/quotationWorktype/index', Yii::app()->user->menuauthlist) ||
                                                        in_array('/location/location/index', Yii::app()->user->menuauthlist) || in_array('/salesexecutive/salesexecutive/index', Yii::app()->user->menuauthlist) || in_array('/rateCard/index', Yii::app()->user->menuauthlist) ))), 'linkOptions' => array('class' => 'dropdown-toggle', 'data-toggle' => 'dropdown'),
                                                        'items' => array(
                                                            // array('label' => 'Import', 'url' => array('purchaseCategory/importcsv')),
                                                             array('label' => 'Application Error log', 'url' => array('/site/applicationlog'), 'visible' => (isset(Yii::app()->user->role) && (Yii::app()->user->role <= 1))),
                                                             array('label' => 'Bank Reconciliation', 'url' => array('/bank/reconciliation'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/bank/reconciliation', Yii::app()->user->menuauthlist)))),
                                                             array('label' => 'Bank', 'url' => array('/bank/newList'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/bank/newList', Yii::app()->user->menuauthlist)))),
                                                            array('label' => 'Cash Balance', 'url' => array('/cashbalance/newlist'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/cashbalance/newlist', Yii::app()->user->menuauthlist)))),
                                                             array('label' => 'Clients', 'url' => array('/clients/newlist'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/clients/newlist', Yii::app()->user->menuauthlist)))),
                                                            array('label' => 'Client Type', 'url' => array('/projectType/newlist'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/projectType/newlist', Yii::app()->user->menuauthlist)))),
                                                            array('label' => 'Company Edit Log', 'url' => array('/dashboard/companyeditlog'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/dashboard/companyeditlog', Yii::app()->user->menuauthlist)))),
                                                            array('label' => 'Company', 'url' => array('/company/newlist'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/company/newlist', Yii::app()->user->menuauthlist)))),
                                                            
                                                            array('label' => 'Completed Projects', 'url' => array('/projects/completedprojects'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/projects/completedprojects', Yii::app()->user->menuauthlist)))),
                                                            
                                                             array('label' => 'Daily Expense Type', 'url' => array('/companyexpensetype/admin'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/companyexpensetype/admin', Yii::app()->user->menuauthlist)))),
                                                              array('label' => 'Delete Requests', 'url' => array('/expenses/deleterequest'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/expenses/deleterequest', Yii::app()->user->menuauthlist)))),
                                                             array('label' => 'Deposit', 'url' => array('/deposit/newList'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/deposit/newlist', Yii::app()->user->menuauthlist)))),
                                                            
                                                            array('label' => 'Duplicate Entries', 'url' => array('/expenses/duplicateentry'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/expenses/duplicateentry', Yii::app()->user->menuauthlist)))),
                                                            array('label' => 'Equipments', 'url' => array('/equipments/index'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/equipments/index', Yii::app()->user->menuauthlist)))),
                                                             array('label' => 'Expense Head', 'url' => array('/expensetype/newlist'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/expensetype/newlist', Yii::app()->user->menuauthlist)))),
                                                            
                                                           
                                                             array('label' => 'General Settings', 'url' => array('/dashboard/generalsettings'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/dashboard/generalsettings', Yii::app()->user->menuauthlist)))),
                                                             array('label' => 'Location', 'url' => array('/location/location/index'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/location/location/index', Yii::app()->user->menuauthlist)))),
                                                              array('label' => 'Materials', 'url' => array('/materials/index'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/materials/index', Yii::app()->user->menuauthlist)))),
                                                               array('label' => 'Media Settings', 'url' => array('/dashboard/mediasettings'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/dashboard/mediasettings', Yii::app()->user->menuauthlist)))),
                                                              array('label' => 'Menu Permissions', 'url' => array('/menu/menuPermissions/create'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/menu/menuPermissions/create', Yii::app()->user->menuauthlist)))),
                                                              array('label' => 'PDF Layouts', 'url' => array('/projectTemplate/index'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/projectTemplate/index', Yii::app()->user->menuauthlist)))),
                                                              array('label' => 'Petty Cash Payment', 'url' => array('/expenses/pettycashpayment')),
                                                              array('label' => 'Projects', 'url' => array('/projects/newlist'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/projects/newlist', Yii::app()->user->menuauthlist)))),
                                                              array('label' => 'Purchase Item', 'url' => array('/purchaseCategory/newList'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/purchaseCategory/newList', Yii::app()->user->menuauthlist)))),
                                                              array('label' => 'Quotation Category', 'url' => array('/quotationcategory/quotationCategoryMaster/index'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/quotationcategory/quotationCategoryMaster/index', Yii::app()->user->menuauthlist)))),
                                                              array('label' => 'Quotation Finish', 'url' => array('/finish/quotationFinish/index'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/finish/quotationFinish/index', Yii::app()->user->menuauthlist)))),
                                                              array('label' => 'Quotation Footer Template', 'url' => array('/dashboard/quotationtemplate'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/dashboard/quotationtemplate', Yii::app()->user->menuauthlist)))),
                                                               array('label' => 'Quotation Item', 'url' => array('/quotationitem/quotationItemMaster/index'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/quotationitem/quotationItemMaster/index', Yii::app()->user->menuauthlist)))),
                                                               
                                                            array('label' => 'Quotation Item Rate', 'url' => array('/quotationitem/quotationItemMaster/rateindex'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/quotationitem/quotationItemMaster/index', Yii::app()->user->menuauthlist)))),
                                                            
                                                            array('label' => 'Quotation Worktype', 'url' => array('/worktype/quotationWorktype/index'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/worktype/quotationWorktype/index', Yii::app()->user->menuauthlist)))),
                                                               
                                                              
                                                            
                                                               array('label' => 'Ratecard', 'url' => array('/rateCard/index'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/rateCard/index', Yii::app()->user->menuauthlist)))),
                                                                 array('label' => 'Sales Executive', 'url' => array('/salesexecutive/salesexecutive/index'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/salesexecutive/salesexecutive/index', Yii::app()->user->menuauthlist)))),
                                                                array('label' => 'Stock Opening Balance', 'url' => array('/wh/warehousestock/StockOpeningBalance'), 'visible' => (isset(Yii::app()->user->role) && (Yii::app()->user->role < 3))),
                                                                array('label' => 'Tax Slab', 'url' => array('/taxSlabs/admin'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/taxSlabs/admin', Yii::app()->user->menuauthlist)))),
                                                                array('label' => 'Unit', 'url' => array('/unit/newList'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/unit/newlist', Yii::app()->user->menuauthlist)))),
                                                                array('label' => 'Update/Delete Log', 'url' => array('/log/historylog'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/log/historylog', Yii::app()->user->menuauthlist)))),
                                                                array('label' => 'User Roles', 'url' => array('/userRoles/newlist'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/userRoles/newlist', Yii::app()->user->menuauthlist)))),
                                                            array('label' => 'Users', 'url' => array('/users/index'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/users/index', Yii::app()->user->menuauthlist)))),
                                                            
                                                            
                                                                                                                     
                                                            array('label' => 'Work Type', 'url' => array('/workType/newlist'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/workType/newlist', Yii::app()->user->menuauthlist)))),
                                                            
                                                           
                                                            
                                                        )
                                                    ),
                                                    array(
                                                        'label' => 'Reports', 'url' => '#', 'visible' => (isset(Yii::app()->user->role) && ((in_array('/projects/financialreport', Yii::app()->user->menuauthlist)) || (in_array('/expenses/paymentreport', Yii::app()->user->menuauthlist)) ||
                                                        (in_array('/projects/projectreport', Yii::app()->user->menuauthlist)) || (in_array('/dailyReport/weeklypayment', Yii::app()->user->menuauthlist)) || (in_array('/projects/expenselist', Yii::app()->user->menuauthlist)) ||
                                                        (in_array('/expenses/newlist', Yii::app()->user->menuauthlist)) || (in_array('/dailyexpense/dailyexpensereport', Yii::app()->user->menuauthlist)) ||
                                                        (in_array('/vendors/paymentReport', Yii::app()->user->menuauthlist)) || (in_array('/subcontractorpayment/report', Yii::app()->user->menuauthlist)) ||
                                                        (in_array('/expenses/profitandloss', Yii::app()->user->menuauthlist)) || (in_array('/cashbalance/cashbalance_report', Yii::app()->user->menuauthlist)) || (in_array('/bank/banktransaction_report', Yii::app()->user->menuauthlist)) ||
                                                        (in_array('/bills/taxreport', Yii::app()->user->menuauthlist)) || (in_array('/bank/reconciliationreport', Yii::app()->user->menuauthlist)) ||
                                                        (in_array('/bills/purchasebillreport', Yii::app()->user->menuauthlist)) || (in_array('/dailyexpense/pettycash', Yii::app()->user->menuauthlist)) ||
                                                        (in_array('/expenses/expensereportbyuser', Yii::app()->user->menuauthlist)) || (in_array('/dailyexpense/dailyexpensebyuser', Yii::app()->user->menuauthlist)) ||
                                                        (in_array('/vendors/vendorpaymentsbyuser', Yii::app()->user->menuauthlist)) || (in_array('/subcontractorpayment/subcontractorpaymentbyuser', Yii::app()->user->menuauthlist)) ||
                                                        (in_array('/dailyexpense/salaryreport', Yii::app()->user->menuauthlist)) || (in_array('/cashbalance/dailyfinancialreport', Yii::app()->user->menuauthlist)) ||
                                                        (in_array('/cashbalance/dailyfinancialsummary', Yii::app()->user->menuauthlist)) || (in_array('/projects/projectexpensereport', Yii::app()->user->menuauthlist)) ||
                                                        (in_array('/exportdata/index', Yii::app()->user->menuauthlist)) || (in_array('/salesquotation/salesQuotation/report', Yii::app()->user->menuauthlist)))), 'linkOptions' => array('class' => 'dropdown-toggle', 'data-toggle' => 'dropdown'),
                                                        'itemOptions' => array('class' => 'report-list'),
                                                        'items' => array(
                                                            array('label' => 'Financial Reports', 'url' => array('/projects/financialreport'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/projects/financialreport', Yii::app()->user->menuauthlist)))),
                                                            array('label' => 'Receipt Report', 'url' => array('/expenses/paymentreport'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/expenses/paymentreport', Yii::app()->user->menuauthlist)))),
                                                            array('label' => 'Work Status Report', 'url' => array('/projects/projectreport'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/projects/projectreport', Yii::app()->user->menuauthlist)))),
                                                            array('label' => 'Labour Report', 'url' => array('/dailyReport/weeklypayment'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/dailyReport/weeklypayment', Yii::app()->user->menuauthlist)))),
                                                            array('label' => 'Project Report', 'url' => array('/projects/expenselist'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/projects/expenselist', Yii::app()->user->menuauthlist)))),
                                                            array('label' => 'Day Book Transactions', 'url' => array('/expenses/newlist'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/expenses/newlist', Yii::app()->user->menuauthlist)))),
                                                            array('label' => 'Daily Expense Report', 'url' => array('/Dailyexpense/Dailyexpensereport'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/dailyexpense/dailyexpensereport', Yii::app()->user->menuauthlist)))),
                                                            array('label' => 'Vendor Payment Report', 'url' => array('/vendors/paymentReport'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/vendors/paymentReport', Yii::app()->user->menuauthlist)))),
                                                            array('label' => 'Subcontractor Payment Report', 'url' => array('/subcontractorpayment/report'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/subcontractorpayment/report', Yii::app()->user->menuauthlist)))),
                                                            array('label' => 'P/L Report', 'url' => array('/expenses/profitandloss'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/expenses/profitandloss', Yii::app()->user->menuauthlist)))),
                                                            array('label' => 'Cashbalance  Report', 'url' => array('/cashbalance/cashbalance_report&ledger=0'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/cashbalance/cashbalance_report', Yii::app()->user->menuauthlist))))
                                                            ,
                                                            array('label' => 'Bank Transaction  Report', 'url' => array('/bank/banktransaction_report&ledger=0'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/bank/banktransaction_report', Yii::app()->user->menuauthlist)))),
                                                            array('label' => 'Tax Report', 'url' => array('/bills/taxreport'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/bills/taxreport', Yii::app()->user->menuauthlist)))),
                                                            array('label' => 'TDS Report', 'url' => array('/bills/tdsreport'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/bills/tdsreport', Yii::app()->user->menuauthlist)))),
                                                            array('label' => 'Quantity Estimation Report', 'url' => array('/projects/projectsquantityreport'), 'visible' => (isset(Yii::app()->user->role) && (Yii::app()->user->role <= 2))),
                                                            array('label' => 'Purchase Report', 'url' => array('/purchaseCategory/purchasereport'), 'visible' => (isset(Yii::app()->user->role) && (Yii::app()->user->role <= 2))),
                                                            array('label' => 'Reconciliation Report', 'url' => array('/bank/reconciliationreport'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/bank/reconciliationreport', Yii::app()->user->menuauthlist)))),
                                                            array('label' => 'Purchase bill Report', 'url' => array('/bills/purchasebillreport'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/bills/purchasebillreport', Yii::app()->user->menuauthlist)))),
                                                            array('label' => 'Ageing Report', 'url' => array('/bills/ageingreport'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/bills/ageingreport', Yii::app()->user->menuauthlist)))
                                                            ),
                                                            array('label' => 'Petty Cash Report', 'url' => array('/dailyexpense/pettycash'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/dailyexpense/pettycash', Yii::app()->user->menuauthlist)))),
                                                            array('label' => 'Salary Report', 'url' => array('/dailyexpense/salaryreport'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/dailyexpense/salaryreport', Yii::app()->user->menuauthlist)))),
                                                            array('label' => 'Daily Financial Report', 'url' => array('/cashbalance/dailyfinancialreport'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/cashbalance/dailyfinancialreport', Yii::app()->user->menuauthlist)))),
                                                            array('label' => 'Daily Financial Summary Report', 'url' => array('/cashbalance/dailyfinancialsummary'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/cashbalance/dailyfinancialsummary', Yii::app()->user->menuauthlist)))),
                                                            array('label' => 'Daybook', 'url' => array('/expenses/expensereportbyuser'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/expenses/expensereportbyuser', Yii::app()->user->menuauthlist)))),
                                                            array('label' => 'Daily Expense', 'url' => array('/dailyexpense/dailyexpensebyuser'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/dailyexpense/dailyexpensebyuser', Yii::app()->user->menuauthlist)))),
                                                            array('label' => 'Bulk Vendor Payment', 'url' => array('/vendors/vendorpaymentsbyuser'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/vendors/vendorpaymentsbyuser', Yii::app()->user->menuauthlist)))),
                                                            array('label' => 'Subcontractor Payment', 'url' => array('/subcontractorpayment/subcontractorpaymentbyuser'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/subcontractorpayment/subcontractorpaymentbyuser', Yii::app()->user->menuauthlist)))),
                                                            array('label' => 'New Projects', 'url' => array('/projects/projectexpensereport'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/projects/projectexpensereport', Yii::app()->user->menuauthlist)))),
                                                            array('label' => 'Export Data', 'url' => array('/exportdata/index'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/exportdata/index', Yii::app()->user->menuauthlist)))),
                                                            array('label' => 'Project Payment Report', 'url' => array('/projects/paymentreport'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/projects/paymentreport', Yii::app()->user->menuauthlist)))),
                                                            array('label' => 'Buyer Report', 'url' => array('/buyer/buyers/buyerreport'), 'visible' => (isset(Yii::app()->user->role) && ($buyer_module) && (in_array('/buyer/buyers/buyerreport', Yii::app()->user->menuauthlist)))),
                                                            array('label' => 'Stock Status Report', 'url' => array('/report/reports/stockStatusReport'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/report/reports/stockStatusReport', Yii::app()->user->menuauthlist)))),
                                                            array('label' => 'Stock Value Report', 'url' => array('/report/reports/stockValueReport'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/report/reports/stockValueReport', Yii::app()->user->menuauthlist)))),
                                                            array('label' => 'Stock Correction Report', 'url' => array('/report/reports/stockCorrectionReport'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/report/reports/stockCorrectionReport', Yii::app()->user->menuauthlist)))),
                                                            array('label' => 'Material Consumption Report', 'url' => array('/projects/materialconsumptionreport'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/projects/materialconsumptionreport', Yii::app()->user->menuauthlist)))),
                                                            array('label' => 'Transfer Report', 'url' => array('/report/reports/transferreport'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/report/reports/transferreport', Yii::app()->user->menuauthlist)))),
                                                            array('label' => 'Pending Bills', 'url' => array('/report/reports/pendingbills'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/report/reports/pendingbills', Yii::app()->user->menuauthlist)))), array('label' => 'Sales Quotation  Report', 'url' => array('/salesquotation/salesQuotation/report'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/salesquotation/salesQuotation/report', Yii::app()->user->menuauthlist)))),
                                                        )
                                                    ),
                                                    array(
                                                        'label' => 'Accounts', 'url' => '#', 'visible' => (defined('LOGIN_USER_TABLE') && LOGIN_USER_TABLE != ''), 'linkOptions' => array('class' => 'dropdown-toggle', 'data-toggle' => 'dropdown'),
                                                        'items' => $allowed_apps_menu,
                                                        'itemCssClass' => 'app_switchmenu',
                                                        'htmlOptions' => array('class' => 'nav navbar-nav  navbar-right'),
                                                    ),
                                                    array('label' => '<span class="badge">' . $remincount['count'] . '</span>', 'url' => array('/paymentReminders/newlist'), 'linkOptions' => array('class' => 'icon icon-clock'), 'visible' => (isset(Yii::app()->user->role) && (in_array('/paymentReminders/newlist', Yii::app()->user->menuauthlist)))),
                                                    array('label' => 'Login', 'url' => array('/site/login'), 'visible' => Yii::app()->user->isGuest),
                                                ),
                                                'submenuHtmlOptions' => array('class' => 'dropdown-menu'),
                                                'htmlOptions' => array('class' => 'nav navbar-nav  navbar-right'),
                                            ));
                                            ?>

                                            <?php
                                        }
                                        ?>
                                    </div>
                                </div>
                            </nav>
                            <?php 
                             $pending_reconcillation_count = Reconciliation::model()->pendingreconciliation()->getTotalItemCount();
                             $duplicate_entries_count = Expenses::model()->getTotalDuplicateEntryCount();
                             $pending_bills_count = count($this->getPendingBills());        
                             $pending_del_data = $this->getPendingDeliveries();
                             $pending_deliveries_count = $pending_del_data['pending_count']; 
                             $sql = "SELECT COUNT(*) as COUNT FROM `pms_acc_wpr_item_consumed` WHERE `status` = 0";
                             $pending_consumption_request = Yii::app()->db->createCommand($sql)->queryScalar();
                             $payment_aging_array = Controller::getVendorAgingSummary();
                             $receipt_aging_array = Controller::getProjectAgingSummary();
                             $unassigned_wh_count = Warehouse::model()->count('assigned_to IS NULL');

                             $dueBillsSql = "SELECT COUNT(*) FROM jp_dailyexpense_bill WHERE paid_status = 0";
                             $due_bills_count = Yii::app()->db->createCommand($dueBillsSql)->queryScalar();

                             
                            ?>
                            <!-- header -->
                            <div id="mainSidebar">
                            <div class="dashboard-contents">
                                <a href="<?php echo Yii::app()->createUrl('dashboard/index'); ?>" 
                                class="<?php echo (Yii::app()->controller->id == 'dashboard' && Yii::app()->controller->action->id == 'index') ? 'active' : ''; ?>">
                                    <i class="fa fa-th-large"></i>
                                </a>
                                <a href="<?php echo Yii::app()->createUrl('dashboard/notification'); ?>" title="Notifications"  
                                class="<?php echo (Yii::app()->controller->id == 'dashboard' && Yii::app()->controller->action->id == 'notification') ? 'active' : ''; ?>">
                                    <i class="fa fa-bell"></i>
                                </a>
                                <a href="<?php echo Yii::app()->createUrl('dashboard/cashbalance'); ?>" title="Cash Balance" 
                                class="<?php echo (Yii::app()->controller->id == 'dashboard' && Yii::app()->controller->action->id == 'cashbalance') ? 'active' : ''; ?>">
                                    <i class="fa fa-bar-chart"></i>
                                </a>
                                <a href="<?php echo Yii::app()->createUrl('dashboard/financialreport'); ?>" title="Financial Report" 
                                class="<?php echo (Yii::app()->controller->id == 'dashboard' && Yii::app()->controller->action->id == 'financialreport') ? 'active' : ''; ?>">
                                    <i class="fa fa-dollar"></i>
                                </a>
                            </div>
                                <div class="dashboard-contents page-navigation">
                                <?php
                                    if ($pending_consumption_request > 0) {
                                        $badgeCount = ($pending_consumption_request > 100) ? '99+' : $pending_consumption_request;
                                        $url = Yii::app()->createAbsoluteUrl('wh/warehouse/consumptionrequest');
                                        ?>
                                        <div class="pending-consumption pull-right">
                                            <a href="<?php echo $url; ?>" title="! Pending Consumption Request" target="_blank">
                                                <i class="fa fa-shopping-cart"></i>
                                                <span class="badge"><?php echo $badgeCount; ?></span>
                                            </a>
                                        </div>
                                        <?php
                                    }
                                ?>
                                <?php
                                    if ($pending_deliveries_count > 0) {
                                        $badgeCount = ($pending_deliveries_count > 100) ? '99+' : $pending_deliveries_count;
                                        $url = Yii::app()->createAbsoluteUrl('/report/reports/pendingdeliveries');
                                        ?>
                                        <div class="pending-deliveries pull-right">
                                            <a href="<?php echo $url; ?>" title="! Pending Deliveries" target="_blank">
                                                <i class="fa fa-truck"></i>
                                                <span class="badge"><?php echo $badgeCount; ?></span>
                                            </a>
                                        </div>
                                        <?php
                                    }
                                ?>
                                <?php
                                    if ($pending_bills_count > 0) {
                                        $badgeCount = ($pending_bills_count > 100) ? '99+' : $pending_bills_count;
                                        $url = Yii::app()->createAbsoluteUrl('/report/reports/pendingbills');
                                        ?>
                                        <div class="pending-bill pull-right">
                                            <a href="<?php echo $url; ?>" title="! Pending Bills" target="_blank">
                                                <i class="fa fa-clipboard"></i>
                                                <span class="badge"><?php echo $badgeCount; ?></span>
                                            </a>
                                        </div>
                                        <?php
                                    }
                                ?>
                                <?php
                                    if ($duplicate_entries_count > 0) {
                                        $badgeCount = ($duplicate_entries_count > 100) ? '99+' : $duplicate_entries_count;
                                        $url = Yii::app()->createAbsoluteUrl('expenses/duplicateentry');
                                        ?>
                                        <div class="duplicate-entries pull-right">
                                            <a href="<?php echo $url; ?>" title="Duplicate Data" target="_blank">
                                                <i class="fa fa-clone"></i>
                                                <span class="badge"><?php echo $badgeCount; ?></span>
                                            </a>
                                        </div>
                                        <?php
                                    }
                                    ?>

                                <?php
                                    if ($unassigned_wh_count > 0) {
                                        $badgeCount = ($unassigned_wh_count > 100) ? '99+' : $unassigned_wh_count;
                                        $url = Yii::app()->createAbsoluteUrl('wh/warehouse/unassignedwarehouse');
                                        ?>
                                        <div class="unassigned-warehouse pull-right">
                                            <a href="<?php echo $url; ?>" onclick="scrollToBottom(); return false;" title="! Unassigned Warehouse" target="_blank">
                                                <i class="fa fa-building"></i>
                                                <span class="badge"><?php echo $badgeCount; ?></span>
                                            </a>
                                        </div>
                                        <?php
                                    }
                                ?>
                                <?php
                                    if ($pending_reconcillation_count > 0) {
                                        $badgeCount = ($pending_reconcillation_count > 100) ? '99+' : $pending_reconcillation_count;
                                        $url =  Yii::app()->createAbsoluteUrl('bank/reconciliation');
                                        ?>
                                        <div class="pending-reconciliation pull-right">
                                            <a href="<?php echo $url; ?>" title="! Pending Reconciliations" target="_blank">
                                                <i class="fa fa-institution"></i>
                                                <span class="badge"><?php echo $badgeCount; ?></span>
                                            </a>
                                        </div>
                                        <?php
                                    }
                                ?>

                                <?php
                                if ($due_bills_count > 0) {
                                    $badgeCount = ($due_bills_count > 100) ? '99+' : $due_bills_count;
                                    $url = Yii::app()->createAbsoluteUrl('dailyexpense/expenses');
                                    ?>
                                    <div class="pending-due-bills pull-right">
                                        <a href="<?php echo $url; ?>" title="! Due Bills" target="_blank" onclick="scrollToBottom(); return false;">
                                            <i class="fa fa-list-alt"></i>
                                            <span class="badge"><?php echo $badgeCount; ?></span>
                                        </a>
                                    </div>
                                    <?php
                                }
                                ?>

                                </div>
                            </div>
                            <button id="sidebarToggle"><i class="fa fa-angle-double-left"></i></button>

                            <!-- <div class="dashboard-content"> -->
                                <!-- mainmenu -->
                                <!-- <div class="span-5 last"> -->
                                    <!-- <div id="sidebar">
                                        <?php
                                        $this->beginWidget('zii.widgets.CPortlet', array(
                                            'title' => '',
                                        ));
                                        $this->widget('zii.widgets.CMenu', array(
                                            'items' => $this->menu,
                                            'htmlOptions' => array('class' => 'operations'),
                                        ));
                                        $this->endWidget();
                                        ?>
                                    </div> -->
                                    <!-- sidebar -->
                                <!-- </div> -->
                                <?php echo $content; ?>
                                <div id="releaseModal" class="modal fade version-modal" tabindex="-1" role="dialog" aria-labelledby="releaseModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="model-header">
                                                <h5 class="modal-title" id="releaseModalLabel">Release Note</h5>
                                                <button type="button" class="close closebtn pull-right" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">×</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                            <!-- </div> -->

                            <footer class="footer-row footer">
                                <div class="footer-left">
                                <!-- <div class="footer-dev">Dev</div> -->
                                <?php
                                    if (ENV != 'production') {
                                        echo '<!--DB:' . _DB . '-->';
                                        ?> 
                                        <div class="footer-dev">
                                            <?php echo ENV; ?>
                                        </div>
                                    <?php } ?>
                                    <?php
                                    $app_root = YiiBase::getPathOfAlias('webroot');
                                    $filePath = $app_root . "/RELEASENOTE.md";
                                    $icon='';
                                    $pms_api_integration_model = ApiSettings::model()->findByPk(1);
                                        if ($pms_api_integration_model && $pms_api_integration_model->api_integration_settings == 1) {
                                            $icon = '<span class="fa fa-info-circle  large-icon" title="Integrated System"></span>';
                                        }

                                    if (file_exists($filePath)) {
                                        $markdownContent = file_get_contents($filePath);
                                        $pattern = '/\bVersion +[\w\.]+.*$/m';
                                        if (preg_match($pattern, $markdownContent, $matches)) {
                                            echo "<button class='viewreleasenote'>{$matches[0]} $icon</button>";
                                        } else {
                                            echo "<button class='viewreleasenote'>Version Not Found $icon</button>";
                                        }
                                    } else {
                                        $gitLogOutput = shell_exec("cd $app_root && git log -1 --format='%at'");
                                        if ($gitLogOutput !== null) {
                                           
        

                                            $timestamp = trim($gitLogOutput);
                                            $lastUpdateDate = date('Y-m-d H:i:s', $timestamp);
                                            echo "<button class='viewreleasenote'>Last update: $lastUpdateDate $icon</button>";
                                        } else {
                                            echo "<button class='viewreleasenote'>Last update details not available.$icon</button>";
                                        }
                                    }
                                    ?>
                                </div>

                                <div class="footer-center">
                                    <div style="display: none;">bhiappne_stagescv2</div>
                                    Powered by <a href="http://www.bluehorizoninfotech.com">bluehorizoninfotech.com</a>&nbsp;<br />
                                    Copyright &copy; 2025. All Rights Reserved.<br />
                                </div>
                                <div class="footer-right">
                                    <div class="user-select">
                                        <?php
                                        if (isset(Yii::app()->user->mainuser_id) && isset(Yii::app()->user->mainuser_role) && (Yii::app()->user->mainuser_role == 1)) {
                                            $tblpx = Yii::app()->db->tablePrefix;
                                            $sql = "SELECT `t`.`userid`, concat_ws(' ',`t`.`first_name`, `t`.`last_name`) as full_name,"
                                                    . "`{$tblpx}user_roles`.`role` "
                                                    . "FROM `{$tblpx}users` `t` JOIN {$tblpx}user_roles ON {$tblpx}user_roles.id= t.user_type "
                                                    . "WHERE status=0  ORDER BY user_type, full_name ASC";
                                            $result = Yii::app()->db->createCommand($sql)->queryAll();
                                            $listdata = CHtml::listData($result, 'userid', 'full_name', 'role');
                                            echo CHtml::dropDownList('shift_userid', '', $listdata, array(
                                                'style' => 'border: 1px solid #ccc;', 'options' => array(Yii::app()->user->id => array('selected' => true)),
                                                'ajax' => array(
                                                    'type' => 'POST',
                                                    'url' => Yii::app()->createUrl('users/usershift'),
                                                    'success' => 'js:function(data){
                                                        location.reload();
                                                    }',
                                                    'data' => array('shift_userid' => 'js:this.value'),
                                                )
                                            ));
                                            ?>
                                            <?php if (isset(Yii::app()->user->mainuser_id) && Yii::app()->user->mainuser_id != Yii::app()->user->id) { ?>
                                                <span style="color: red; font-style: italic; text-decoration: blink; margin-left: 20px;">(Note: Other user's session)</span>
                                            <?php } ?>
                                        <?php } ?>
                                    </div>
                                    
                                </div>

                            </footer>

                            <!-- footer -->
                        </div>
                        <!-- page -->
                        <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/dashboard/dashboard.js"></script>
                        <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/dashboard/dashboard-am-charts.js"></script>
                    </body>

                    </html>

                    <script>
                        function viewReleaseNote(show = 0) {
                            var url = "<?php echo $this->createUrl('/site/viewReleaseNote', ['show' => '']) ?>" + show;
                            $.ajax({
                                type: "GET",
                                url: url,
                                success: function (response) {
                                    if (response) {
                                      $("#releaseModal .modal-body").html(response);
                                      $("#releaseModal").modal('show');
                                    }
                                }
                            });
                        }

                        $(document).ready(function () {
                            viewReleaseNote(0);

                            $('.viewreleasenote').click(function (event) {
                                viewReleaseNote(1);
                            });
                            $(document).delegate(".closebtn", "click", function () {
                                $("#releaseModal").modal('hide');
                            });

                        });

                        $(function () {
                            function Ascending_sort(a, b) {
                                return ($(b).text().toUpperCase()) <
                                        ($(a).text().toUpperCase()) ? 1 : -1;
                            }

                            $("li.report-list ul.dropdown-menu li").sort(Ascending_sort).appendTo('li.report-list ul.dropdown-menu');

                            $("#rate,#purchase_rate").blur(function () {
                                // alert("jsadhjksd");
                                if ($(this).parents(".row").find(".sgstp").val() == "" &&
                                        $(this).parents(".row").find(".cgstp").val() == "") {
                                    $("#tax_slab").trigger("change");
                                }

                            });
                            $("#tax_slab").change(function () {
                                var tax_slab = $(this).val();
                                var gst = parseFloat(tax_slab) / 2;
                                $(this).parents(".row").find(".sgstp").val(gst).trigger("blur");
                                $(this).parents(".row").find(".cgstp").val(gst).trigger("blur");
                            })

                            $(document).on("change", ".tax_slab", function () {
                                var tax_slab = $(this).val();
                                var gst = tax_slab / 2;
                                $(this).parents("tr").find(".sgstp").val(gst).trigger("blur");
                                $(this).parents("tr").find(".cgstp").val(gst).trigger("blur");
                            })
                        })

                    </script>