<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$this->breadcrumbs=array(
	'performa invoice',)
?>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/tableHeadFixer.js"></script>

<?php
$receipt = 0;
$paid    = 0;
$grandtotal= 0;
$grandgst =0;
foreach($newmodel as $expenses) {
    $receipt = $receipt + $expenses["dailyexpense_receipt"];
    $paid    = $paid + $expenses["dailyexpense_paidamount"];
    $grandtotal    = $grandtotal + $expenses["amount"];
    $grandgst= $grandgst + ($expenses["dailyexpense_sgst"]+$expenses["dailyexpense_cgst"]+ $expenses["dailyexpense_igst"]);
}
?>
<div class="" id="newlist">
<?php
    $sort = new CSort;
    $sort->defaultOrder = 'dailyexp_id DESC';
    $sort->attributes = array('dailyexp_id' => 'Daily Expense Id',
        'bill_id'=> 'Bill Id',
        'amount' => 'Total Amount',
        'dailyexpense_paidamount' => 'Paid Amount');
    $dataProvider=new CArrayDataProvider($newmodel, array(
        'id'=>'dailyexp_id', //this is an identifier for the array data provider
        'sort'=>$sort,
        'keyField'=>'dailyexp_id', //this is what will be considered your key field
        'pagination'=>false
    ));
    $this->widget('zii.widgets.CListView', array(
        'dataProvider'=>$dataProvider,
        //'data'=>$newmodel, 
       'viewData' => array( 'receipt' => $receipt, 'paid' => $paid,'grandtotal'=>$grandtotal, 'grandgst'=>$grandgst), 
        'itemView'=>'_newview',
        'id'=>'daybooklist',
        'enableSorting'=>1,
        'enablePagination' => true,
        'template' => '<div>{summary}{sorter}</div><div id="parent"><table width="100" id="fixTable" class="table total-table">{items}</table></div>{pager}',
        //'itemView' => '_newview', 'template' => '<div>{summary}{sorter}</div><div class="tblwrap scrollTable"><div class="container1"><table cellpadding="10" class="table  list-view sorter">{items}</table></div></div>{pager}',
        'enablePagination' => false
    )); ?>
</div>

<script>
    $(document).ready(function() {
        $("#fixTable").tableHeadFixer({'left' : 2, 'foot' : true, 'head' : true}); 
        //$("#fixTable").tableHeadFixer({'foot' : true, 'head' : true}); 
	});
    $(document).ready(function() {
       $(".popover-test").popover({
            html: true,
                content: function() {
                  //return $('#popover-content').html();
                  return $(this).next('.popover-content').html();
                }
        });
    $('[data-toggle=popover]').on('click', function (e) {
       $('[data-toggle=popover]').not(this).popover('hide');
    });
    $('body').on('hidden.bs.popover', function (e) {
        $(e.target).data("bs.popover").inState.click = false;
    });
    $('body').on('click', function (e) {
        $('[data-toggle=popover]').each(function () {
            // hide any open popovers when the anywhere else in the body is clicked
            if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                $(this).popover('hide');
            }
        });
    });

});
</script>
<style>
    #parent {max-height: 400px;}
    th{background: #eee;}
    .pro_back{background: #fafafa;}
    .request_back{background-color: #FF6347; color: #FFFFFF;}
    .pending-icon{color:#000000; font-size: 18px;}
    .total-table{margin-bottom: 0px;}
    th,td {border-right: 1px solid #ccc !important;}
</style>

