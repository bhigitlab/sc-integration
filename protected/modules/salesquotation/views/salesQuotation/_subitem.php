<div class="worktype_label_row  position-relative">
    <i class="fa fa-trash text-danger delete_subitem text-lg"  style="position: absolute;left: 43%;top: 85px"></i>
    <div class="row">
    <div class="col-md-2">
            <label>Work Type </label> 
            <input type="hidden" id="worktype_label_id" value="">                         
            <?php
            echo CHtml::textField('',$main_wtype_model['worktype_label'], array('class' => 'form-control require', 'id' => 'worktype_label', 'placeholder' => 'Work Type'));
            ?>
            <div class="errorMessage"></div>
        </div>
        <div class="col-md-2">
            <label>&nbsp;</label>
            <?php
            echo CHtml::dropDownList('',$main_wtype_model['master_cat_id'],
                    CHtml::listData(QuotationCategoryMaster::model()->findAll(), 'id', 'name'),
                    array('class' => 'form-control js-example-basic-single field_change require QuotationGenWorktype_master_cat_id', 'empty' => '-Select Category-', 'style' => 'width:100%'));
            ?>
            <?php echo CHtml::hiddenField('category_id_data', '', array('id' => 'category_id_data')); ?>
        </div>
        <div class="col-md-1">
            <label>&nbsp;</label>
            <?php
            echo CHtml::dropDownList('',$main_wtype_model['unit'], CHtml::listData(Unit::model()->findAll(), 'id', 'unit_name'), array('class' => 'form-control js-example-basic-single   require ', 'empty' => '-Select Unit-', 'name' => 'unit', 'style' => 'width:100%', 'id' => 'wrktype_unit')
            );
            ?>
        </div>
        <?php
        $activeProjectTemplate = $this->getActiveTemplate();
        if ($activeProjectTemplate == 'TYPE-5') {
        ?>
            <div class="col-md-2">
                <label>&nbsp;</label>
                <?php
                echo CHtml::textField('',$main_wtype_model['hsn_code'], 
                array('class' => 'form-control', 'name' => 'hsn_code','placeholder'=>'HSN Code', 
                'style' => 'width:100%', 'id' => 'hsn_code_sub'));
                ?>
            </div>
        <?php } ?>
        <div class="col-md-1">
            <label>&nbsp;</label>
            <?php echo CHtml::textField('',$main_wtype_model['quantity'], array('class' => 'form-control allownumericdecimal quantity_sub', 'autocomplete' => 'off', 'placeholder' => 'qty', 'name' => 'quantity', 'id' => 'wrktype_quantity')); ?>
            <div class="errorMessage"></div>
        </div>
        <?php 
        $activeProjectTemplate = $this->getActiveTemplate();
        if ($activeProjectTemplate != 'TYPE-5') {
        ?>
        <div class="col-md-1">
            <label>&nbsp;</label>
            <?php echo CHtml::textField('',$main_wtype_model['quantity_nos'], array('class' => 'form-control allownumericdecimal ', 'autocomplete' => 'off', 'placeholder' => 'qty nos', 'name' => 'quantity_nos', 'id' => 'wrktype_quantity_nos')); ?>
            <div class="errorMessage"></div>
        </div>
        <?php } ?>
        <div class="col-md-2">
            <label>&nbsp;</label>
            <?php echo CHtml::textField('',$main_wtype_model['mrp'], array('class' => 'form-control allownumericdecimal ', 'autocomplete' => 'off', 'placeholder' => 'MRP', 'name' => 'mrp', 'id' => 'wrktype_mrp')); ?>
            <div class="errorMessage"></div>
        </div>
        <div class="col-md-2">
            <label>&nbsp;</label>
            <?php echo CHtml::textField('',$main_wtype_model['amount_after_discount'], array('class' => 'form-control allownumericdecimal amountafter-discount', 'autocomplete' => 'off', 'name' => 'amount_after_discount', 'placeholder' => 'amt after disc', 'id' => 'wrktype_amount_after_discount')); ?>
            <div class="errorMessage"></div>
        </div>
        <div class="col-md-5">
            <!-- <label>&nbsp;</label> -->
            <textarea rows="1" class="form-control mb-2" name="description" id="wrktype_desc"></textarea>
        </div>
    </div>
    <br>
    <div class="sub_worktype_selector" id="quotation_item"></div>
    
</div>