<?php

/**
 * This is the model class for table "{{specification}}".
 *
 * The followings are the available columns in table '{{specification}}':
 * @property integer $id
 * @property integer $cat_id
 * @property integer $brand_id
 * @property string $specification
 * @property string $hsn_code
 * @property string $unit
 * @property string $dieno
 * @property string $length
 * @property string $specification_type
 * @property string $filename
 * @property string $company_id
 * @property integer $created_by
 * @property string $created_date
 *
 * The followings are the available model relations:
 * @property Brand $brand
 * @property PurchaseCategory $cat
 * @property Users $createdBy
 */
class Specification extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */


	 public $filename;
	public function tableName()
	{
		return '{{specification}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('cat_id, unit', 'required'),
			array('cat_id, brand_id,sub_cat_id, created_by', 'numerical', 'integerOnly' => true),
			array('specification,filename', 'length', 'max' => 255),
			array('specification','checkexist'),
			array('hsn_code', 'length', 'max' => 200),
			array('unit, dieno, length', 'length', 'max' => 20),
			array('specification_type', 'length', 'max' => 1),
			array('company_id', 'length', 'max' => 100),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, cat_id,sub_cat_id, brand_id, specification, hsn_code, unit, dieno, length, specification_type, filename, company_id, created_by, created_date', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'brand' => array(self::BELONGS_TO, 'Brand', 'brand_id'),
			'cat' => array(self::BELONGS_TO, 'PurchaseCategory', 'cat_id'),
			'sub' => array(self::BELONGS_TO, 'PurchaseSubCategory', 'sub_cat_id'),
			'createdBy' => array(self::BELONGS_TO, 'Users', 'created_by'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'cat_id' => 'Cat',
			'sub_cat_id' => 'Sub',
			'brand_id' => 'Brand',
			'specification' => 'Specification',
			'hsn_code' => 'Hsn Code',
			'unit' => 'Unit',
			'dieno' => 'Dieno',
			'length' => 'Length',
			'specification_type' => 'Specification Type',
			'filename' => 'Filename',
			'company_id' => 'Company',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('cat_id', $this->cat_id);
		if($this->sub_cat_id != ''){
			$criteria->compare('sub_cat_id', $this->sub_cat_id);
		}
		
		$criteria->compare('brand_id', $this->brand_id);
		$criteria->compare('specification', $this->specification, true);
		$criteria->compare('hsn_code', $this->hsn_code, true);
		$criteria->compare('unit', $this->unit, true);
		$criteria->compare('dieno', $this->dieno, true);
		$criteria->compare('length', $this->length, true);
		$criteria->compare('specification_type', $this->specification_type, true);
		$criteria->compare('filename', $this->filename, true);
		$criteria->compare('company_id', $this->company_id, true);
		$criteria->compare('created_by', $this->created_by);
		$criteria->compare('created_date', $this->created_date, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			//'pagination' => array('pageSize' => 20,),

		));
	}

	public function specificationsearch()
	{
		$tblpx = Yii::app()->db->tablePrefix;
		$criteria = new CDbCriteria;
		$criteria->select = 'child.id,t.id,t.cat_id,t.sub_cat_id, t.specification, t.unit, t.created_date, t.brand_id,t.company_id,t.hsn_code,t.specification_type,t.dieno,t.filename';
		$criteria->join = ' LEFT JOIN `' . $tblpx . 'purchase_category` AS `child` ON t.cat_id = child.id';
		$user = Users::model()->findByPk(Yii::app()->user->id);
		$arrVal = explode(',', $user->company_id);
		$newQuery = "";
		foreach ($arrVal as $arr) {
			if ($newQuery)
				$newQuery .= ' OR';
			$newQuery .= " FIND_IN_SET('" . $arr . "', t.company_id)";
		}
		$criteria->addCondition('t.spec_status = "1" AND (' . $newQuery . ')');
		$criteria->order = 't.id ASC';
		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'pagination' => false,
		));
	}
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Specification the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function checkexist($attribute, $params) {

		$id = Specification::model()->findByAttributes(array('id' => $this->id));
		if(empty($id)){
			if(!empty($this->sub_cat_id)){
				$name = Specification::model()->findByAttributes(array('specification' => $this->specification),array('condition'=>'cat_id = '.$this->cat_id.' AND brand_id = '.$this->brand_id.' AND sub_cat_id ='.$this->sub_cat_id.''));
			}else{
				$name = Specification::model()->findByAttributes(array('specification' => $this->specification),array('condition'=>'cat_id = '.$this->cat_id.' AND brand_id = '.$this->brand_id.' AND sub_cat_id IS NULL'));
		
			}
			
			if(!empty($name)) {
				$this->addError("specification", ("Specification already exists"));
		   }
		} 
	}
}
