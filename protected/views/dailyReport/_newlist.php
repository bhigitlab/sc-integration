<div id="msg_box"></div>
<?php
if ($index == 0) {


    ?>
    <thead>
        <tr>
            <th>SI No</th>
            <th>Date</th>
            <th>Company</th>
            <th>Project</th>
            <th>Subcontractor</th>
            <th>Expense Head</th>
            <th>Total Amount</th>
            <?php
                if((isset(Yii::app()->user->role) && ((in_array('/dailyReport/update', Yii::app()->user->menuauthlist)) || (in_array('/dailyReport/view', Yii::app()->user->menuauthlist))  ) )){
             ?>
            <th></th>
                <?php } ?>


        </tr>
    </thead>
<?php } ?>

<?php 
	$edit_request_model = Editrequest::model()->find(["condition"=>"parent_id=".$data->dr_id." AND editrequest_table='jp_dailyreport' AND editrequest_status =0"]);
	
?>


    <tr class="">
        <td><?php echo $index+1; ?></td>
        <td><?php echo $data->date;?></td>
		<td class="<?= !empty($edit_request_model)?'company_edit_admin_approval_class':''?>">
		<?php
		$company = Company::model()->findByPk($data->company_id);
		 echo $company['name']; ?>
		</td>
        <td><?php 
        $project = Projects::model()->findByPk($data->projectid);
        echo $project['name'];
        ?></td>
        <td><?php
         $subcontractor = Subcontractor::model()->findByPk($data->subcontractor_id);
         echo $subcontractor['subcontractor_name'];
        ?></td>
        <td><?php
         $expensehead = ExpenseType::model()->findByPk($data->expensehead_id);
         echo $expensehead['type_name'];
        ?></td>
		<!-- <td><?php echo Controller::money_format_inr($data->amount,2,1);?></td> -->

        <td class="text-right"><span class="popover-test tax_amnt" data-toggle="popover" data-trigger="hover" data-placement="left" data-trigger="hover" type="button" data-html="true"><?php echo $data->amount? Controller::money_format_inr($data->amount, 2):""; ?></span>
            <div class="popover-content hide">
                <div><span>SGST: </span></div>
                <div><span>CGST: </span></div>
                <div><span>IGST: </span></div>                                
            </div>
        </td>
		
                     <?php
                        if((isset(Yii::app()->user->role) && ((in_array('/dailyReport/view', Yii::app()->user->menuauthlist)) ) )){
                     ?>
                <td>
                     <span class="icon icon-options-vertical popover-test" data-toggle="popover"   data-placement="left" type="button" data-html="true" style="cursor: pointer;" ></span>
                    <div class="popover-content hide">
                        <ul class="tooltip-hiden">
                            <?php
                            if((in_array('/dailyReport/view', Yii::app()->user->menuauthlist))){
                            ?>
                             <li><button id="<?php echo $data->dr_id; ?>" class="btn btn-xs btn-default view_purchase">View</button></li>
                            <?php  } ?>
                        </ul>
                    </div>
                    </td>
                        <?php } ?>


</tr>
<?php
if ($index == 0) {
    ?>
    <tfoot>
        <tr>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <?php
                if((isset(Yii::app()->user->role) && ((in_array('/dailyReport/view', Yii::app()->user->menuauthlist))  ) )){
             ?>
            <th></th>
                <?php } ?>



        </tr>
    </tfoot>
<?php } ?>
<style>
td span.fa {
    font-size: 15px;
    color: #2e6da4;
    margin-left: 5px !important;
}
.permission_style {
  background: #f8cbcb !important;
}
.company_edit_admin_approval_class{
	background: red !important;
    color: white;
}
</style>
