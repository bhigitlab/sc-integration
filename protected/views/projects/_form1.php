<?php
/* @var $this ProjectsController */
/* @var $model Projects */
/* @var $form CActiveForm */
?>

<div class="modal-body">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'projects-form',
        'enableAjaxValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true,
            'validateOnType' => false,),
    ));
    ?>



    <!-- Popup content-->



    <div class="clearfix">
        <div class="add-box">
            <div class="row addRow">					

                <div class="col-md-6 col-sm-6">
                    <?php echo $form->labelEx($model, 'name'); ?>
                    <?php echo $form->textField($model, 'name', array('size' => 60, 'maxlength' => 100, 'style' => "width:350px")); ?>
                    <?php echo $form->error($model, 'name'); ?>
                </div>
                <div class="col-md-3 col-sm-3">
                    <?php echo $form->labelEx($model, 'status'); ?>
                    <div class="radio_btn">
                        <?php
                        echo $form->radioButtonList($model, 'status', CHtml::listData(Status::model()->findAll(
                                                array(
                                                    'select' => array('sid,caption'),
                                                    'condition' => 'status_type="active_status"',
                                                    'order' => 'caption',
                                                    'distinct' => true
                                        )), 'sid', 'caption'), array('separator' => ''));
                        ?>
                    </div>
                    <?php echo $form->error($model, 'status'); ?>
                </div>
            </div>
            <div class="row addRow">
                <div class="col-md-6 col-sm-6 textArea-box">
                    <?php echo $form->labelEx($model, 'description'); ?>
                    <?php echo $form->textArea($model, 'description', array('rows' => 6, 'cols' => 50, 'style' => "width:350px")); ?>
                    <?php echo $form->error($model, 'description'); ?>
                </div>
                <div class="col-md-3 col-sm-6">						
                    <table style="border-collapse: collapse;" class="tableborder">
                        <tr style="background-color: #c2c2c2"><th><input type='checkbox' id='select_all' value='0'  /></th><th>Name</th></tr>
                        <?php
                        $userslist = Users::model()->findAll();
                        $assigned_users_array = array();
                        if (!$model->isNewRecord) {
                            $assigned_users = ProjectAssign::model()->findAll(array('condition' => 'projectid=' . $model->pid));
                            foreach ($assigned_users as $auser) {
                                $assigned_users_array[] = $auser['userid'];
                            }
                        }

                        foreach ($userslist as $user) {
                            ?>
                            <tr><td width='20'><input type="checkbox" class="checkbox" <?php echo (in_array($user->userid, $assigned_users_array) ? 'checked="checked"' : ''); ?> name="user[]" value='<?php echo $user->userid ?>' /></td><td><?php echo $user->first_name ?></td></tr>

                            <?php
                        }
                        ?>
                    </table>
                </div>
                <div class="col-md-3 col-sm-12 save-btnHold">						
                    <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>								
                    <button data-dismiss="modal" onclick="javascript:window.location.reload()">Close</button>
                </div>
            </div>
        </div>



        <style>
            .tableborder td{
                border:1px solid #c2c2c2;
            }
        </style>



        <?php $this->endWidget(); ?>

    </div><!-- form -->

    <script>
        $(document).ready(function () {

            //select all checkboxes
            $("#select_all").change(function () {  //"select all" change
                var status = this.checked; // "select all" checked status
                $('.checkbox').each(function () { //iterate all listed checkbox items
                    this.checked = status; //change ".checkbox" checked status
                });
            });

            $('.checkbox').change(function () { //".checkbox" change
                //uncheck "select all", if one of the listed checkbox item is unchecked
                if (this.checked == false) { //if this item is unchecked
                    $("#select_all")[0].checked = false; //change "select all" checked status to false
                }

                //check "select all" if all checkbox items are checked
                if ($('.checkbox:checked').length == $('.checkbox').length) {
                    $("#select_all")[0].checked = true; //change "select all" checked status to true
                }
            });


        });


    </script>
