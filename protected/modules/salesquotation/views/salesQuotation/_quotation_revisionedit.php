<style>
    .edittable td,th{
        padding:   5px !important ; 
    } 

    .edittable tr td:nth-child(2){
        border-left: none;
    } 

    .edittable tr:last-child{
        border-bottom: none;
    } 

</style>
<div class="alert alert-success" id="alert-success" role="alert" style="display:none;">
</div>
<div class="alert alert-danger" id="alert-danger" role="alert" style="display:none;">
</div>
<div class="alert alert-warning" id="alert-warning" role="alert" style="display:none;"> </div>
<span id="error-edit"></span>
<div id="previous_details" style="display: none;"></div>
<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'item_update_form',
    'enableAjaxValidation' => true,
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
        'validateOnChange' => true,
        'validateOnType' => false,
    ),
        ));
?>
<?php echo CHtml::hiddenField('rev_no', $rev_no, array('id' => 'rev_no')); ?>
<?php echo CHtml::hiddenField('quotation_id', $qid, array('id' => 'quotation_id')); ?>
<div class="panel panel-default">
    <div class="panel-heading">
        <h4><b>Edit Items</b></h4>
    </div>    
    <div class="panel-body">
        <?php if (!empty($modelsales)) { ?>
            <div class="row" id="container">
                <div class="row-margin">
                    <table class="edittable">
                        <?php
                        foreach ($modelsales as $key => $items) {
                            $model = SalesQuotation::model()->findByPk($items['id']);
                            $work_type_label = QuotationWorktypeMaster::model()->findByPk($model->work_type);
                            $template_id = $work_type_label['template_id'];
                            $style = '';
                            $class = 'form-control allownumericdecimal require common-val amountafter-discount';
                            if ($model->deleted_status == 2) {
                                $style = 'background-color:#ffeb3b78';
                                $class = 'form-control allownumericdecimal require';
                            }
                            ?>
                            <tr style = '<?php echo $style; ?>'>
                                <td>
                                    <label>Category Label</label>
                                    <?php echo $form->textField($model, 'category_label', array('class' => 'form-control require ', 'id' => 'category_label' . $key, 'readonly' => true, 'name' => 'category_label[]')); ?>
                                    <?php echo CHtml::hiddenField('cat_id[]', $model->category_id, array('id' => 'cat_id' . $key)); ?>
                                    <?php echo CHtml::hiddenField('master_id', $model->master_id, array('id' => 'hiddenmasterid' . $key)); ?>
                                    <?php echo CHtml::hiddenField('item_id[]', $model->item_id, array('id' => 'hiddenitem' . $key)); ?>
                                    <?php echo CHtml::hiddenField('work_type_id[]', $model->work_type, array('id' => 'hiddenworktype' . $key)); ?>
                                </td>
                                <td class="worktypedata" data-id ='<?php echo $model->work_type; ?>' data-incremnt = '<?php echo $key; ?>'>
                                    <label>Work Type Label</label>
                                    <?php
                                    $newQuery = 'work_type_id =' . $model->work_type . ' AND quotation_category_id =' . $model->category_id . '';
                                    echo $form->dropDownList(
                                            $model, 'worktype_label', CHtml::listData(QuotationItemMaster::model()->findAll(array('condition' => $newQuery)), 'worktype_label', 'worktype_label'), array('class' => 'form-control js-example-basic-single field_change invoice_add require worktype_label common-val', 'empty' => '-Select Worktype Label-', 'id' => 'worktype_label' . $key, 'style' => 'width:100%', 'name' => 'worktype_label[]', 'data-cat' => $model->category_id, 'data-id' => $model->work_type, 'data-template' => $template_id)
                                    );
                                    ?>

                                    <?php if ($work_type_label['template_id'] == 1) { ?>
                                    <td>
                                        <label>Shutter Work</label>
                                        <?php echo $form->textField($model, 'shutterwork_description', array('class' => 'form-control require ', 'id' => 'shutterwork_description' . $key, 'readonly' => true, 'name' => 'shutterwork_description[]')); ?>

                                    </td>
                                    <td>
                                        <label>Carcass Box Work</label>
                                        <?php echo $form->textField($model, 'carcass_description', array('class' => 'form-control require ', 'id' => 'caracoss_description' . $key, 'name' => 'carcass_description[]', 'readonly' => true)); ?>

                                    </td>
                                    <?php echo $form->hiddenField($model, 'shutterwork_material', array('class' => 'form-control require', 'id' => 'shutterwork_material' . $key, 'name' => 'shutterwork_material[]')); ?>
                                    <?php echo $form->hiddenField($model, 'shutterwork_finish', array('class' => 'form-control require', 'id' => 'shutterwork_finish' . $key, 'name' => 'shutterwork_finish[]')); ?>
                                    <?php echo $form->hiddenField($model, 'carcass_material', array('class' => 'form-control require', 'id' => 'carcass_material' . $key, 'name' => 'carcass_material[]')); ?>
                                    <?php echo $form->hiddenField($model, 'carcass_finish', array('class' => 'form-control require', 'id' => 'carcass_finish' . $key, 'name' => 'carcass_finish[]')); ?>
                                    <?php echo $form->hiddenField($model, 'description', array('class' => 'form-control require', 'name' => 'description[]', 'value' => '', 'id' => 'description' . $key)); ?>
                                    <?php echo $form->hiddenField($model, 'material', array('class' => 'form-control require', 'name' => 'material[]', 'value' => '', 'id' => 'material' . $key)); ?>
                                    <?php echo $form->hiddenField($model, 'finish', array('class' => 'form-control require', 'name' => 'finish[]', 'value' => '', 'id' => 'finish' . $key)); ?>
                                <?php } else { ?>
                                    <td></td>
                                    <td>
                                        <?php echo $form->hiddenField($model, 'shutterwork_description', array('class' => 'form-control require', 'name' => 'shutterwork_description[]', 'id' => 'shutterwork_description' . $key,)); ?>
                                        <?php echo $form->hiddenField($model, 'carcass_description', array('class' => 'form-control require', 'name' => 'carcass_description[]', 'id' => 'caracoss_description' . $key)); ?>
                                        <?php echo $form->hiddenField($model, 'shutterwork_material', array('class' => 'form-control require', 'name' => 'shutterwork_material[]', 'id' => 'shutterwork_material' . $key)); ?>
                                        <?php echo $form->hiddenField($model, 'shutterwork_finish', array('class' => 'form-control require', 'name' => 'shutterwork_finish[]', 'id' => 'shutterwork_finish' . $key)); ?>
                                        <?php echo $form->hiddenField($model, 'carcass_material', array('class' => 'form-control require', 'name' => 'carcass_material[]', 'id' => 'carcass_material' . $key)); ?>
                                        <?php echo $form->hiddenField($model, 'carcass_finish', array('class' => 'form-control require', 'name' => 'carcass_finish[]', 'id' => 'carcass_finish' . $key)); ?>
                                        <label>Description</label>
                                        <?php echo $form->textField($model, 'description', array('class' => 'form-control require common-val', 'id' => 'description' . $key, 'name' => 'description[]', 'readonly' => true)); ?>

                                    </td>
                                    <?php
                                    echo $form->hiddenField($model, 'material', array('class' => 'form-control require', 'id' => 'material' . $key, 'name' => 'material[]'));
                                    echo $form->hiddenField($model, 'finish', array('class' => 'form-control require', 'id' => 'finish' . $key, 'name' => 'finish[]'));
                                }
                                ?>
                                <td>
                                    <label>Unit</label>
                                    <?php
                                    echo $form->dropDownList(
                                            $model, 'unit', CHtml::listData(Unit::model()->findAll(), 'id', 'unit_name'), array('class' => 'form-control js-example-basic-single field_change invoice_add require common-val', 'empty' => '-Select Unit-', 'id' => 'unit' . $key, 'name' => 'unit[]', 'style' => 'width:100%')
                                    );
                                    ?>

                                </td>
                                <td>
                                    <label>Quantity</label>
                                    <?php echo $form->textField($model, 'quantity', array('class' => 'form-control allownumericdecimal require quantity-sales common-val', 'id' => 'qty' . $key, 'name' => 'quantity[]', 'data-itemid' => $model->item_id, 'data-incrementid' => $key)); ?>
                                    <?php echo $form->error($model, 'quantity'); ?>
                                </td>
                                <td>
                                    <label>Quantity Nos</label>
                                    <?php echo $form->textField($model, 'quantity_nos', array('class' => 'form-control allownumericdecimal require common-val', 'name' => 'quantity_nos[]', 'id' => 'quantity_nos' . $key,)); ?>
                                    <?php echo $form->error($model, 'quantity_nos'); ?>
                                </td>
                                <td>
                                    <label>Mrp</label>
                                    <?php echo $form->textField($model, 'mrp', array('class' => 'form-control allownumericdecimal require common-val', 'id' => 'mrp' . $key, 'name' => 'mrp[]')); ?>
                                    <?php echo $form->error($model, 'mrp'); ?>
                                </td>
                                <td>
                                    <label>After Discount</label>
                                    <?php echo $form->textField($model, 'amount_after_discount', array('class' => $class, 'id' => 'amount_after_discount' . $key, 'name' => 'amount_after_discount[]')); ?>
                                    <?php echo $form->error($model, 'amount_after_discount'); ?>
                                </td>
                                <td>
                                    <?php echo $form->hiddenField($model, 'sub_status', array('class' => 'form-control require', 'id' => 'sub_status' . $key, 'name' => 'sub_status[]')); ?>
                                    <?php echo $form->hiddenField($model, 'subitem_label', array('class' => 'form-control require', 'id' => 'subitem_label' . $key, 'name' => 'subitem_label[]')); ?>
                                    <?php echo $form->hiddenField($model, 'deleted_status', array('class' => 'form-control require', 'id' => 'deleted_status' . $key, 'name' => 'deleted_status[]')); ?>
                                    <label>&nbsp;&nbsp;</label>
                                    <?php if ($model->deleted_status == 1) { ?>
                                        <button id="<?php echo $model->id ?>" data-del ="<?php echo $model->deleted_status ?>"   class="removebtn"><span class="fa fa-trash deleteSpecification"></span></button>
                                    <?php } else { ?>
                                        <button id="<?php echo $model->id ?>" data-del ="<?php echo $model->deleted_status ?>"  class="removebtn"><span class="fa fa-edit">Restore</span></button>
                                    <?php } ?>
                                </td>

                            </tr>
                        <?php } ?>
                    </table>
                </div>
            </div>
            <?php
        } if (!empty($sales_extra)) {
            ?>

            <div class="row" id="container_extrawork">
                <div class="row-margin">
                    <table class="edittable">
                        <?php
                        foreach ($sales_extra as $key1 => $items) {
                            $model = SalesQuotation::model()->findByPk($items['id']);
                            $style = '';
                            $class = 'form-control allownumericdecimal common-val amount-after-discount-extra';
                            if ($model->deleted_status == 2) {
                                $style = 'background-color:#ffeb3b78';
                                $class = 'form-control allownumericdecimal';
                            }
                            ?>
                            <tr style=<?php echo $style; ?>>
                                <td>
                                    <?php
                                    echo $form->labelEx($model, 'Extra Work');
                                    ?>
                                    <?php echo $form->hiddenField($model, 'category_label', array('class' => 'form-control require', 'id' => 'category_label_extra' . $key1, 'name' => 'SalesQuotation[' . $key1 . '][category_label]')); ?>
                                    <?php echo $form->hiddenField($model, 'category_id', array('class' => 'form-control require', 'id' => 'category_id' . $key1, 'name' => 'SalesQuotation[' . $key1 . '][category_id]')); ?>

                                </td>
                                <td>
                                    <?php echo $form->textField($model, 'description', array('class' => 'form-control common-val', 'placeholder' => 'description', 'id' => 'other-description' . $key1, 'name' => 'SalesQuotation[' . $key1 . '][description]')); ?>
                                    <?php echo $form->error($model, 'description'); ?>
                                </td>
                                <td>
                                    <?php
                                    echo $form->dropDownList($model, 'unit', CHtml::listData(Unit::model()->findAll(), 'id', 'unit_name'), array('class' => 'form-control js-example-basic-single field_change invoice_add require common-val', 'empty' => '-Select Unit-', 'style' => 'width:100%', 'name' => 'SalesQuotation[' . $key1 . '][unit]')
                                    );
                                    ?>
                                </td>
                                <td>
                                    <?php echo $form->textField($model, 'quantity', array('class' => 'form-control allownumericdecimal common-val', 'placeholder' => 'qty', 'name' => 'SalesQuotation[' . $key1 . '][quantity]')); ?>
                                    <?php echo $form->error($model, 'quantity'); ?>
                                </td>
                                <td>
                                    <?php echo $form->textField($model, 'quantity_nos', array('class' => 'form-control allownumericdecimal common-val', 'placeholder' => 'qty nos', 'name' => 'SalesQuotation[' . $key1 . '][quantity_nos]')); ?>
                                    <?php echo $form->error($model, 'quantity_nos'); ?>
                                </td>
                                <td>
                                    <?php echo $form->textField($model, 'mrp', array('class' => 'form-control allownumericdecimal common-val', 'placeholder' => 'MRP', 'name' => 'SalesQuotation[' . $key1 . '][mrp]')); ?>
                                    <?php echo $form->error($model, 'mrp'); ?>
                                </td>
                                <td>
                                    <?php echo $form->textField($model, 'amount_after_discount', array('class' => $class, 'placeholder' => 'amt after disc', 'name' => 'SalesQuotation[' . $key1 . '][amount_after_discount]')); ?>
                                    <?php echo $form->error($model, 'amount_after_discount'); ?>

                                </td>

                                <td>
                                    <?php echo $form->hiddenField($model, 'deleted_status', array('class' => 'form-control require', 'id' => 'deleted_status_extra' . $key1, 'name' => 'SalesQuotation[' . $key1 . '][deleted_status]')); ?>
                                    <?php if ($model->deleted_status == 1) { ?>
                                        <button id="<?php echo $model->id ?>" data-del ="<?php echo $model->deleted_status ?>"  class="removebtn"><span class="fa fa-trash deleteSpecification"></span></button>
                                    <?php } else { ?>
                                        <button id="<?php echo $model->id ?>" data-del ="<?php echo $model->deleted_status ?>"  class="removebtn"><span class="fa fa-edit ">Restore</span></button>
                                    <?php } ?>
                                </td>
                            </tr>
                        <?php } ?>
                    </table>
                </div>
            </div>
        <?php } ?>
    </div>
</div>
<label>Remarks</label>
<?php echo $form->textField($model, 'revision_remarks', array('class' => 'form-control common-val', 'id' => 'revision_remarks', 'value' => $model->revision_remarks)); ?>
<?php echo $form->error($model, 'revision_remarks'); ?><br>
<div class='row'>
    <div class="col-md-2" >
        <div class="form-group">
            <label>Tax Slab:</label>
            <?php
            $datas = TaxSlabs::model()->getAllDatas();
            ?>
            <select class="form-control " name="tax_slab" id="add_tax_slab">
                <option value="">Select one</option>
                <?php
                foreach ($datas as $value) {
                    if ($model->tax_slab == $value['tax_slab_value']) {
                        echo ' <option data-id="' . $value['id'] . '" value="' . $value['tax_slab_value'] . '" selected>' . $value['tax_slab_value'] . '%</option>';
                    } else {
                        echo ' <option  data-id="' . $value['id'] . '" value="' . $value['tax_slab_value'] . '">' . $value['tax_slab_value'] . '%</option>';
                    }
                }
                ?>
            </select>
        </div>
    </div>

    <div class="col-md-1" >                       
        <div class="form-group">
            <label>SGST% :</label>
            <?php echo $form->textField($model, 'sgst_percent', array('class' => 'inputs target small_class txtBox  tax_percentage sgstp allownumericdecimal form-control calculation ', 'autocomplete' => 'off', 'name' => 'sgst_percent', 'id' => 'sgstp')); ?>
            <?php echo $form->error($model, 'sgst_percent'); ?>
            <div id="sgst_amount" class="padding-box"><?php echo $model->sgst_amount ?></div>
            <?php echo CHtml::hiddenField('sgst_amount', $model->sgst_amount, array('id' => 'sgst_amount_hidden')); ?>
        </div>
    </div>		
    <div class="col-md-1" >
        <div class="form-group">
            <label>CGST% :</label>
            <?php echo $form->textField($model, 'cgst_percent', array('class' => 'inputs target small_class txtBox tax_percentage cgstp allownumericdecimal form-control calculation ', 'autocomplete' => 'off', 'name' => 'cgst_percent', 'id' => 'cgstp')); ?>
            <?php echo $form->error($model, 'cgst_percent'); ?>
            <div id="cgst_amount" class="padding-box"><?php echo $model->cgst_amount ?></div>
            <?php echo CHtml::hiddenField('cgst_amount', $model->cgst_amount, array('id' => 'cgst_amount_hidden')); ?>
        </div>
    </div> 
    <div class="col-md-1" >
        <div class="form-group">
            <label>IGST% :</label>
            <?php echo $form->textField($model, 'igst_percent', array('class' => 'inputs target small_class txtBox tax_percentage igstp allownumericdecimal form-control calculation ', 'autocomplete' => 'off', 'name' => 'igst_percent', 'id' => 'igstp')); ?>
            <?php echo $form->error($model, 'igst_percent'); ?>
            <div id="igst_amount" class="padding-box"><?php echo $model->igst_amount ?></div>
            <?php echo CHtml::hiddenField('igst_amount', $model->igst_amount, array('id' => 'igst_amount_hidden')); ?>
        </div>
    </div>
    <div iv class="col-md-2" >
        <label>Total After Discount</label>
        <div class="form-group">
            <?php echo $form->textField($model, 'mainitem_total', array('class' => 'inputs target small_class txtBox tax_percentage igstp allownumericdecimal form-control calculation', 'autocomplete' => 'off', 'name' => 'mainitem_total', 'id' => 'total-amount-val', 'readonly' => 'readonly')); ?>
            <?php echo $form->error($model, 'mainitem_total'); ?>
        </div>
    </div>
    <div class="col-md-2" >
        <label>Total Tax Amount</label>
        <div class="form-group">
            <?php echo $form->textField($model, 'mainitem_tax', array('class' => 'inputs target small_class txtBox tax_percentage igstp allownumericdecimal form-control calculation ', 'autocomplete' => 'off', 'name' => 'mainitem_tax', 'id' => 'total-tax', 'readonly' => 'readonly')); ?>
            <?php echo $form->error($model, 'mainitem_tax'); ?>

        </div>
    </div>

    <div class="col-md-2" >
        <label>Total Amount With Tax</label>
        <div class="form-group">
        <?php echo $form->textField($model, 'maintotal_withtax', array('class' => 'inputs target small_class txtBox tax_percentage igstp allownumericdecimal form-control calculation text-right', 'autocomplete' => 'off', 'name' => 'maintotal_withtax', 'id' => 'total-amount', 'readonly' => 'readonly')); ?>
            <?php echo $form->error($model, 'maintotal_withtax'); ?>

        </div>
    </div> 
    <div class="col-md-1">
        <label class="inline">Round Off:</label>
        <input type="text" name="Quotation[round_off]" id="Quotation_round_off" value="0.00"  class="text-right form-control">
    </div>           
</div> 
<div class="row clearfix">
    <div class="col-md-2 pull-right">
        <label class="inline">Total Amount:</label>
        <?php echo $form->textField($model, 'maintotal_withtax', array('class' => 'inputs target small_class txtBox tax_percentage igstp allownumericdecimal form-control calculation text-right', 'autocomplete' => 'off', 'name' => 'maintotal_withtax', 'id' => 'final-total-amount', 'readonly' => 'readonly')); ?>
    </div>
</div> 
<div class="panel-footer save-btnHold text-center button-panel d-block">
    <?php
    echo CHtml::Button('Update', array('class' => 'btn btn-info', 'id' => 'updateitem'));
    ?>
</div>
<?php $this->endWidget(); ?>
<script>
    function closeupdate() {
        $("#item_update_form").hide();
    }
    $(document).on('keyup', '.quantity-sales', function () {
        var element = $(this);
        var qty = element.val();
        var item_id = element.attr('data-itemid');
        var id = element.attr('data-incrementid');
        var total_amount = $("#total-amount").val();
        $.ajax({
            url: "<?php echo Yii::app()->createAbsoluteUrl('salesquotation/SalesQuotation/getamountdetails'); ?>",
            data: {
                item_id: item_id,
                qty: qty
            },
            type: "POST",
            dataType: 'json',
            success: function (result) {
                if (result != '') {
                    $("#amount_after_discount" + id).val(result.profit_amount);
                    var mrp_after_discount = parseInt(result.profit_amount) + parseInt((result.profit_amount * result.discount) / 100);
                    $("#mrp" + id).val(mrp_after_discount);
                    var sum_value = 0;
                    $('.amountafter-discount').each(function () {
                        sum_value += +$(this).val();
                    })
                    $('.amount-after-discount-extra').each(function () {
                        sum_value += +$(this).val();
                    })
                    $('#total-amount-val').val(sum_value);
                    if (total_amount != '') {
                        $("#sgstp").keyup();
                        $("#cgstp").keyup();
                        $("#igstp").keyup();
                    }

                    if (sum_value > 0) {                                                
                        var total_tax = $("#total-tax").val();   
                        var roundoff = $("#Quotation_round_off").val();               
                        var total_with_tax = parseFloat(sum_value) + parseFloat(total_tax);
                        $('#total-amount').val(total_with_tax.toFixed(2));
                        var final_total =  total_with_tax + parseFloat(roundoff);
                        $('#final-total-amount').val(final_total.toFixed(2));
                    }
                }
            }
        });
    });
    $(document).on('change', '.worktype_label', function () {
        var element = $(this);
        var worktypelabel = element.val();
        var cat_id = $(this).attr('data-cat');
        var worktypeid = $(this).attr('data-id');
        var template_id = $(this).attr('data-template');
        var id = element.parent('.worktypedata').attr('data-incremnt');
        var qty = $('#qty' + id).val();
        $.ajax({
            url: '<?php echo Yii::app()->createAbsoluteUrl('salesquotation/SalesQuotation/getdatasofworktype'); ?>',
            type: 'GET',
            dataType: 'json',
            data: {
                cat_id: cat_id,
                worktypelabel: worktypelabel,
                worktypeid: worktypeid,
                template_id: template_id
            },
            success: function (result) {
                if (result.status == 1) {
                    var itemid = result.item_id;
                    $('#hiddenitem' + id).val(itemid);
                    $('#previous_details').html(result.html);
                    $("#previous_details").show();
                    $("#pre_fixtable").tableHeadFixer();
                    getamount(id, qty, itemid);

                } else {
                    $('#previous_details').html(result.html);
                    $("#pre_fixtable").tableHeadFixer();
                }
                if (template_id == 1) {
                    $('#shutterwork_description' + id).val(result.shutterdesc);
                    $('#caracoss_description' + id).val(result.carcassdesc);
                    $('#shutterwork_material' + id).val(result.shuttermaterial);
                    $('#shutterwork_finish' + id).val(result.shutterfinish);
                    $('#carcass_material' + id).val(result.carcasmaterial);
                    $('#carcass_finish' + id).val(result.carcassfinish);
                } else {
                    if (template_id == 3) {
                        $('#material' + id).val(result.material);
                        $('#finish' + id).val(result.finish);
                    }
                    $('#description' + id).val(result.description);

                }

                if(result.description ==""){
                    $inputs = $('#description' + id).parent().siblings();                   
                    $inputs.find("#unit" + id).val("");
                    $inputs.find("#qty" + id).val("0");
                    $inputs.find("#quantity_nos" + id).val("");
                    $inputs.find("#mrp" + id).val("0");
                    $inputs.find("#amount_after_discount" + id).val("0");
                    $inputs.find("#hiddenitem" + id).val("");
                    $('#description' + id).parent().siblings("#material" + id).val("");
                    $('#description' + id).parent().siblings("#finish" + id).val("");
                    
                }
                getTotalAmount();
            }
        });
    });

    function getamount(id, qty, item_id) {
        var total_amount = $("#total-amount").val();
        $.ajax({
            url: "<?php echo Yii::app()->createAbsoluteUrl('salesquotation/SalesQuotation/getamountdetails'); ?>",
            data: {
                item_id: item_id,
                qty: qty
            },
            type: "POST",
            dataType: 'json',
            success: function (result) {
                if (result != '') {
                    $("#amount_after_discount" + id).val(result.profit_amount);
                    var mrp_after_discount = parseInt(result.profit_amount) + parseInt((result.profit_amount * result.discount) / 100);
                    $("#mrp" + id).val(mrp_after_discount);
                    var sum_value = 0;
                    $('.amountafter-discount').each(function () {
                        sum_value += +$(this).val();
                    })
                    $('.amount-after-discount-extra').each(function () {
                        sum_value += +$(this).val();
                    })
                    $('#total-amount-val').val(sum_value);
                    if (total_amount != '') {
                        $("#sgstp").keyup();
                        $("#cgstp").keyup();
                        $("#igstp").keyup();
                    }
                }
            }
        });
    }
    $('#updateitem').click(function () {
        $(".quantity-sales").trigger("change");
        var data = $("#item_update_form").serialize();
        var error = '';
        $('.common-val').each(function () {
            var count = 1;
            if ($(this).val() == '')
            {
                error += "<p>Please Enter Mandatory Fields</p>";
                $(this).css('border', "1px solid red");
                return false;
            }
            count = count + 1;
        });

        if (error == '') {
            $('#error-edit').html('');
            $.ajax({
                url: '<?php echo Yii::app()->createAbsoluteUrl("salesquotation/salesQuotation/updatequotationitems") ?>',
                data: data,
                type: "POST",
                dataType: 'json',
                success: function (result) {
                    $('html, body').animate({
                        scrollTop: $("#content").position().top - 100
                    }, 500);
                    if (result.success == '1') {
                        $("#alert-success").show().html('Item Updated Successfully').delay(3000).fadeOut();
                        location.reload();
                    } else {
                        $("#alert-danger").show().html('Something went wrong in update').delay(3000).fadeOut();
                    }
                    ;

                }
            });
        } else {
            $('#error-edit').html('<div class="alert alert-danger">' + error + '</div>');

        }
        return false;
    });

    $(document).on('click', '.removebtn', function (e) {
        e.preventDefault();
        var update_field_count = $("input.update_field").length;
        if(update_field_count > 0){
            var answer = confirm('WARNING: There are unsaved changes');
            if (answer) {        
                return false; 
            }
        }
        
        element = $(this);
        var item_id = $(this).attr('id');
        var deleted_status = $(this).attr('data-del');
        var qid = '<?php echo $qid ?>';
        var rev_no = '<?php echo $rev_no ?>';
        var message = "Are you sure you want to delete?";
        if (deleted_status == 2) {
            var message = "Are you sure you want to restore?";
        }
        var answer = confirm(message);
        if (answer) {
            var data = {
                'item_id': item_id,
                'del_status': deleted_status
            };
            $.ajax({
                method: "GET",
                async: false,
                data: {
                    data: data
                },
                dataType: "json",
                url: '<?php echo Yii::app()->createUrl('salesquotation/salesQuotation/Removeitem'); ?>',
                success: function (response) {
                    $('html, body').animate({
                        scrollTop: $("#content").position().top - 100
                    }, 500);
                    if (response.response == "success") {
                        $("#alert-success").show().html(response.msg).delay(3000).fadeOut();
                        setTimeout(function(){
							window.location.reload(1);
						}, 2000);
                    } else if (response.response == "warning") {
                        $("#alert-warning").show().html(response.msg).delay(3000).fadeOut();
                    } else {
                        $("#alert-danger").show().html(response.msg).delay(3000).fadeOut();
                    }

                    $("#sgstp").keyup();
                    $("#cgstp").keyup();
                    $("#igstp").keyup();
                }
            });

        } else {
            return false;
        }

    });

    $(".edittable").find("input,select").on('keyup',function(){
        $(this).addClass("update_field");
    })
    $(function () {
        var total_amount = $("#total-amount").val();
        var sum_value = 0;
        $('.amountafter-discount').each(function () {
            sum_value += +$(this).val();
        })
        $('.amount-after-discount-extra').each(function () {
            sum_value += +$(this).val();
        })
        var final_amount = $('#final-total-amount').val();
        $('#final-total-amount').val(final_amount);
        $('#total-amount-val').val(sum_value);
        
        $("#sgstp").keyup();
        $("#cgstp").keyup();
        $("#igstp").keyup();

    });



    $(document).on('change', '.amountafter-discount', function () {
        var total_amount = $("#total-amount").val();
        var sum_value = 0;
        $('.amountafter-discount').each(function () {
            sum_value += +$(this).val();
        })
        $('.amount-after-discount-extra').each(function () {
            sum_value += +$(this).val();
        })
        $('#total-amount-val').val(sum_value);
        if (total_amount != '') {
            $("#sgstp").keyup();
            $("#cgstp").keyup();
            $("#igstp").keyup();
        }
    });
    $(document).on('change', '.amount-after-discount-extra', function () {
        var total_amount = $("#total-amount").val();
        var sum_value = 0;
        $('.amountafter-discount').each(function () {
            sum_value += +$(this).val();
        })
        $('.amount-after-discount-extra').each(function () {
            sum_value += +$(this).val();
        })
        $('#total-amount-val').val(sum_value);
        if (total_amount != '') {
            $("#sgstp").keyup();
            $("#cgstp").keyup();
            $("#igstp").keyup();
        }
    });
    $('#sgstp').keyup(function () {
        var sgst_percent = $('#sgstp').val();
        var total_amount = $('#total-amount-val').val();
        var totaltax = $('#total-tax').val();
        var cgst_amount = $('#cgst_amount').text();
        var igst_amount = $('#igst_amount').text();
        var roundoff = $("#Quotation_round_off").val();        
        if (total_amount > 0) {
            var sgst_amount = (total_amount * sgst_percent) / 100;
            $('#sgst_amount').text(sgst_amount.toFixed(2));
            $('#sgst_amount_hidden').val(sgst_amount.toFixed(2));
            var total_tax = parseFloat(cgst_amount) + parseFloat(sgst_amount) + parseFloat(igst_amount);
            $('#total-tax').val(total_tax.toFixed(2));
            var total_with_tax = parseFloat(total_amount) + parseFloat(total_tax);
            $('#total-amount').val(total_with_tax.toFixed(2));
            var final_total =  total_with_tax + parseFloat(roundoff);
            $('#final-total-amount').val(final_total.toFixed(2));
        } else {
            $('#sgst_amount').text('0.00');
        }
    });

    $('#cgstp').keyup(function () {
        var cgst_percent = $('#cgstp').val();
        var total_amount = $('#total-amount-val').val();
        var totaltax = $('#total-tax').val();
        var sgst_amount = $('#sgst_amount').text();
        var igst_amount = $('#igst_amount').text();
        var roundoff = $("#Quotation_round_off").val()        
        if (total_amount > 0) {

            var cgst_amount = (total_amount * cgst_percent) / 100;
            $('#cgst_amount_hidden').val(cgst_amount.toFixed(2));
            $('#cgst_amount').text(cgst_amount.toFixed(2));
            var total_tax = parseFloat(cgst_amount) + parseFloat(sgst_amount) + parseFloat(igst_amount);
            $('#total-tax').val(total_tax.toFixed(2));
            var total_with_tax = parseFloat(total_amount) + parseFloat(total_tax);
            $('#total-amount').val(total_with_tax.toFixed(2));
            var final_total =  total_with_tax + parseFloat(roundoff);
            $('#final-total-amount').val(final_total.toFixed(2));

        } else {
            $('#cgst_amount').text('0.00');

        }
    });

    $('#igstp').keyup(function () {
        var igst_percent = $('#igstp').val();
        var total_amount = $('#total-amount-val').val();
        var totaltax = $('#total-tax').val();
        var sgst_amount = $('#sgst_amount').text();
        var cgst_amount = $('#cgst_amount').text();
        var roundoff = $("#Quotation_round_off").val();        
        if (total_amount > 0) {
            var igst_amount = (total_amount * igst_percent) / 100;
            $('#igst_amount').text(igst_amount.toFixed(2));
            $('#igst_amount_hidden').val(igst_amount.toFixed(2));
            var total_tax = parseFloat(cgst_amount) + parseFloat(sgst_amount) + parseFloat(igst_amount);
            $('#total-tax').val(total_tax.toFixed(2));
            var total_with_tax = parseFloat(total_amount) + parseFloat(total_tax);
            $('#total-amount').val(total_with_tax.toFixed(2));
            var final_total =  total_with_tax + parseFloat(roundoff);
            $('#final-total-amount').val(final_total.toFixed(2));

        } else {
            $('#igst_amount').text('0.00');
        }
    });

    $('#Quotation_round_off').change(function() {
        var roundOff = parseFloat(this.value);       
        var amount = $("#total-amount").val().replace(/,/g, '');
        amount = parseFloat(amount);                           
        var total_amount = amount + roundOff;
        $("#final-total-amount").val(parseFloat(total_amount).toFixed(2));
    });

    $('body').keypress(function(e) {
        if(e.which == 13) {    
            e.preventDefault();
        }
    });

    function getTotalAmount(){
        var sum_value = 0;
        var total_amount = $("#total-amount").val();
        
        $('.amountafter-discount').each(function () {            
            sum_value += +$(this).val();
        })
        $('.amount-after-discount-extra').each(function () {
            sum_value += +$(this).val();
        })
        
        $('#total-amount-val').val(sum_value);
        if (total_amount != '') {
            $("#sgstp").keyup();
            $("#cgstp").keyup();
            $("#igstp").keyup();
        }
    }
    
</script>