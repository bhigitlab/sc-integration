<!--<link rel="stylesheet" type="text/css" href="uploads/new_template/css/newstyle.css" />-->

<!--<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> -->
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.js"></script>

<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/message/css/jquery.message.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/message/js/jquery.message.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/tableHeadFixer.js"></script>

<!--<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/fonts/glyphicons-halflings-regular.eot">
<link rel="stylesheet" href="<?php //echo Yii::app()->theme->baseUrl; ?>/plugins/smoke/css/smoke.min.css">
<script type="text/javascript" src="<?php // echo Yii::app()->theme->baseUrl; ?>/plugins/smoke/js/smoke.min.js"></script>-->
<script>
    var shim = (function(){document.createElement('datalist');})();
</script>
<style>

    .lefttdiv{
            float: left;
    }
    .select2 {
	width : 100%;	
	}
</style>
<script>
    $( function() {
            $( "#datepicker" ).datepicker({dateFormat: 'dd-mm-yy'}).datepicker("setDate", new Date());

    } );

</script>
<style>
    .dropdown{
            position: relative;
            display: inline-block;
    }
    .dropdown::before{
            position: absolute;
            content: " \2193";
            top: 0px;
            right: -8px;
            height: 20px;
            width: 20px;
    }

    button#caret {
            border : none;
            background : none;
            position: absolute;
            top: 0px;
            right: 0px;
    }
    .invoicemaindiv th, .invoicemaindiv td{
        padding: 10px;
        text-align: left;
    }
    .addcolumn {
        cursor: pointer;
        background-color: black;
        border: none;
        color: white;
        text-align: center;
        text-decoration: none;
        margin: 2px;
        font-size: 30px;
        width: 40px;
      }
      .invoicemaindiv .pull-right, .invoicemaindiv .pull-left{
          float: none !important;
      }
      #parent , #parent2, #parent3{max-height: 150px;}
      .select2.select2-container.select2-container--default.select2-container--above,.select2.select2-container.select2-container--default.select2-container--below,.select2.select2-container.select2-container--default.select2-container--focus{
          width:  200px  !important;
      }
      @media(min-width: 767px){
          .invoicemaindiv .pull-right{
          float: right !important;
      }
      .invoicemaindiv .pull-left{
          float: left !important;
      }
      }
      
      .checkek_edit{
		pointer-events: none; 
	 }
	 
	
	 
	 .add_selection {
		 background: #000;
	  }
	  
	  
	
	
	.toast-container {
	  width:350px;
	}
	.toast-position-top-right {
	  top: 57px;
	  right: 6px;
	}
	.toast-item-close { 
	  background-image: none;
	  cursor: pointer;
	  width: 12px;
	  height: 12px;
	  text-align: center;
	  border-radius: 2px;
	}
	.toast-item-image {
	  font-size: 24px;
	}
	.toast-item-close:hover {
	  color: red;
	}
	.toast-item {
	  border: transparent;
	  border-radius: 3px;
	  font-size: 10px;
	  opacity: 1;
	  background-color: rgba(34,45,50,0.8);
	}
	.toast-item-wrapper p {
	  margin: 0px 5px 0px 42px;
	  font-size: 14px;
	  text-align: justify;
	}
	
	  
	.toast-type-success {
	  background-color: #00A65A;
	  border-color: #00A65A;
	}
	.toast-type-error {
	  background-color: #DD4B39;
	  border-color: #DD4B39;
	}
	.toast-type-notice {
	  background-color: #00C0EF;
	  border-color: #00C0EF;
	}
	.toast-type-warning {
	  background-color: #F39C12;
	  border-color: #F39C12;
	}
	
	.span_class {
	   min-width: 70px;
       display: inline-block;
	}
	.purchase-title{
		border-bottom:1px solid #ddd;
	}
		
	.purchase_items h3{
		font-size:18px;
		color:#333;
		margin:0px;
		padding:0px;
		text-align:left;
		padding-bottom:10px;
	}
	.purchase_items{
		padding:15px;
		box-shadow:0 0 13px 1px rgba(0,0,0,0.25);
	}
	.purchaseitem {
		display: inline-block;
		margin-right: 20px;
	}
	.purchaseitem last-child{margin-right:0px;}
	.purchaseitem label{
		font-size:12px;
	}
	.padding-box{padding:3px 0px; min-height:17px; display:inline-block;}
	th{height:auto;}
	.quantity, .rate{max-width:80px;}
	
	*:focus{
		border:1px solid #333;
		box-shadow:0 0 6px 1px rgba(0,0,0,0.25);
	}
	
	.rate_highlight {
		background:#DD1035 !important;
		color: #fff;
	}
	.block_purchase{
		
		padding-left: 15px;
		
	}
    .error_message{
            color:red;
    }
    a.pdf_excel {
            background-color: #6a8ec7;
            display:inline-block;
            padding:8px;
            color: #fff;
            border: 1px solid #6a8ec8;
    }
.table-scroll {
	position: relative;
	max-width: 1280px;
	width:100%;
	margin: auto;
	display:table;
}
.table-wrap {
	width: 100%;
	display:block;
	overflow: auto;
	position:relative;
	z-index:1;
        border: 1px solid #ddd;
}
.table-wrap.fixedON,
.table-wrap.fixedON table,
.faux-table table {
	height: 380px;/* match heights*/
}
.table-scroll table {
	width: 100%;
	margin: auto;
	border-collapse: separate;
	border-spacing: 0;
        border: 1px solid #ddd;
}
.table-scroll th, .table-scroll td {
	padding: 5px 10px;
	border: 1px solid #ddd;
	background: #fff;
	vertical-align: top;
}
.faux-table table {
	position: absolute;
	top: 0;
	left: 0;
	width: 100%;
	pointer-events: none;
}
.faux-table table tbody {
	visibility: hidden;
}
/* shrink cells in cloned table so that the table height is exactly 300px so that the header and footer appear fixed */
.faux-table table tbody th, .faux-table table tbody td {
	padding-top:0;
	padding-bottom:0;
	border-top:none;
	border-bottom:none;
	line-height:0.1;
}
.faux-table table tbody tr + tr th, .faux-table tbody tr + tr td {
	line-height:0;
}
.faux-table thead th, .faux-table tfoot th, .faux-table tfoot td,
.table-wrap thead th, .table-wrap tfoot th, .table-wrap tfoot td{
	background: #eee;
}
.faux-table {
	position:absolute;
	top:0;
	right:0;
	left:0;
	bottom:0;
	overflow-y:scroll;
}
.faux-table thead, .faux-table tfoot, .faux-table thead th, .faux-table tfoot th, .faux-table tfoot td {
	position:relative;
	z-index:2;
}
/* ie bug */
.table-scroll table thead tr,
.table-scroll table thead tr th,
.table-scroll table tfoot  tr,
.table-scroll table tfoot  tr th,
.table-scroll table tfoot  tr td{height:1px;}
</style>
<div class="container"> 
    <div class="invoicemaindiv" >
        <div class='clearfix purchase-title items_separator'>
            <div class="pull-left">
                <h2 class="">Update Item Estimation by Length</h2>
            </div>
            <div class="pull-right">
                <?php echo CHtml::Button('<<', array('class'=>' back_btn btn_top','onclick' => 'javascript:location.href="'. $this->createUrl('viewestimation',array('project_id' => $projectid)).'"'));?>
            </div>
        </div>
        <div id="msg_box"></div>

	<form id="pdfvals1" method="post" action="<?php echo $this->createAbsoluteUrl('purchase/addestimation'); ?>"  >
            <input type="hidden" name="remove" id="remove" value="">
            <input type="hidden" name="estimation_id" id="estimation_id" value="0">
            <div class="block_purchase">
                <div class="row_block" style="margin-left: -16px;">
                    <div class="elem_block">
                        <div class="form-group">
                            <label>PROJECT :  <span class="required">*</span></label>
                            <select name="project" class="inputs target project"  id="project" style="width:190px">
                                <option value="">Choose Project</option>
                                <?php
                                foreach($project as $key => $value){
                                ?>	
                                    <option value="<?php echo $value['pid']; ?>" <?php echo ($value['pid'] == $projectid)?'selected':''; ?>><?php echo $value['name']; ?></option>
                                    <?php		
                                }
                                ?>

                            </select>
			</div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
		<br><br>
		<div id="msg"></div>
                <div class="purchase_items">
                    <h3>Add/Edit Purchase Item</h3>
                    <div class="clearfix">
                        <div class="purchaseitem">
                            <div id="select_div">
				<label>Specification:  <span class="required">*</span></label>
                                <select style="width:200px;" class="txtBox specification description " id="description" name="description[]">
                                    <option value="">Select one</option>
                                    <?php
                                    foreach($specification as $key=> $value) {
                                    ?>
                                    <option value="<?php echo $value['id']; ?>"<?php if($value['id'] == $model->category_id) { echo " selected"; } ?>><?php echo $value['data']; ?></option>
                                    <?php } ?>
                                    <option value="other">Other</option>
				</select>
                            </div>
			</div>
                        <div class="purchaseitem length_div">
                            <label>Length:  <span class="required">*</span></label>
                            <input type="text" class="inputs target txtBox length allownumericdecimal" value="<?php echo isset($model->itemestimation_length)?$model->itemestimation_length:""; ?>" id="length" name="length[]" placeholder=""/></td>
			</div>
			<div class="purchaseitem quantity_div">
                            <label>Quantity:  <span class="required">*</span></label>
                            <input type="text" class="inputs target txtBox quantity allownumericdecimal" value="<?php echo isset($model->itemestimation_quantity)?$model->itemestimation_quantity:""; ?>"  id="quantity" name="quantity[]" placeholder=""/></td>
			</div>
			<div class="purchaseitem">
                            <label>Units:</label>
                            <div id="item_unit" class="item_unit padding-box"><?php echo isset($model->itemestimation_unit)?$model->itemestimation_unit:""; ?></div>
			</div>
			<div class="purchaseitem">
                            <label>Rate:</label>
                            <input type="text" class="inputs target txtBox rate allownumericdecimal" value="<?php echo isset($model->itemestimation_price)?$model->itemestimation_price:""; ?>" id="rate" name="rate[]" placeholder=""/>
			</div>
			<div class="purchaseitem">
                            <label>Amount:</label>
                            <div id="item_amount" class="padding-box"><?php echo isset($model->itemestimation_amount)?$model->itemestimation_amount:"0.00"; ?></div>
			</div>
                        <div class="purchaseitem remark">
                            <label>Remark:</label>
                            <input type="text" class="txtBox " value="<?php echo isset($model->itemestimation_description)?$model->itemestimation_description:""; ?>" id="remark" name="remark[]"/>
			</div>
			<div class="purchaseitem pull-right">
                            <input type="button" class="item_save" id="0" value="Save">
			</div>
                    </div>
                </div>
            </div>
            <input type="hidden" id="estimationId" value="<?php echo isset($model->itemestimation_id)?$model->itemestimation_id:""; ?>">
            <br><br>
            <!--<div id="table-scroll" class="table-scroll">
                <div id="faux-table" class="faux-table" aria="hidden"></div>
                <div id="table-wrap" class="table-wrap">
                    <div class="table-responsive">
                        <table border="1" class="table">
                            <thead>
                                <tr>
                                    <th>Sl.No</th>
                                    <th>Specification</th>
                                    <th>Quantity</th>
                                    <th>Unit</th>
                                    <th>Rate</th>
                                    <th>Amount</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody class="addrow">
                                </tr>
                            </tbody>
                            <tfoot>
				<tr>
                                    <th colspan="5" class="text-right">GRAND TOTAL: </th>
                                    <th class="text-right"><div id="grand_total">0.00</div></th>
                                    <th></th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>-->
            <!--<div style="padding-right: 0px;" class="col-md-offset-9 col-md-3 col-sm-offset-8 col-sm-4 col-xs-offset-6 col-xs-6">
		<input type="hidden" value="<?php  //echo ((isset($subtot) && $subtot != '') ? $subtot : ''); ?>"  class="txtBox pastweek" readonly=ture name="subtot"  /></td>
                <input type="hidden" value="<?php  //echo ((isset($grand) && $grand != '') ? $grand : ''); ?>"  class="txtBox pastweek grand" name="grand"  readonly=true/>
            </div>
            <br><br>-->
        </form>
        <div id="errorMessages"></div>
    </div>
    <!--<input type="hidden" name="final_amount" id="final_amount" value="0">
    <input type="hidden" name="item_amount_temp" id="item_amount_temp" value="0">-->

<script type="text/javascript">
$(document).ready(function() {
    $(".project").select2();
    $(".specification").select2();
    $('#length').focus();
    $("#description").change(function(event){
        var category_id = $(this).val();
        if(category_id != "") {
            $.ajax({
		method: "GET",
		data: {purchase_id:'test'},
		dataType:"json",
		url: '<?php echo Yii::app()->createUrl('purchase/test'); ?>',
		success: function(result) {
                    $("#length").focus();
		}
            });
        } else {
            $.ajax({
		method: "GET",
		data: {purchase_id:'test'},
		dataType:"json",
		url: '<?php echo Yii::app()->createUrl('purchase/test'); ?>',
		success: function(result) {
                    $("#description").focus();
		}
            });
        }
    });
    $(function() {
        var $inp = $('input:text');
        $inp.bind('keydown', function(e) {
            //var key = (e.keyCode ? e.keyCode : e.charCode);
            var key = e.which;
            if (key == 13) {
                e.preventDefault();
                var nxtIdx = $inp.index(this) + 1;
                $(":input:text:eq(" + nxtIdx + ")").focus();
            }
        });
    });
    $("#remark").keydown(function(event) {
        if(event.keyCode == 13) {
            $("input.item_save").focus();
        }
    });
    $("#length, #quantity, #rate").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
             // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
    $("#description").change(function() {
        var catId = $(this).val();
        $.ajax({
            url: "<?php echo Yii::app()->createAbsoluteUrl("purchase/getUnitByCategory"); ?>",
            data: {"catId": catId},
            type: "POST",
            success:function(data){
                $("#item_unit").text(data);
            }
        }); 
    });
    $("#project").change(function() {
        var project = $(this).val();
        $.ajax({
            url: "<?php echo Yii::app()->createAbsoluteUrl("purchase/getCategoryByProject"); ?>",
            data: {"project": project, "type": "A"},
            type: "POST",
            success:function(data) {
                $("#description").html(data);
                $('#description').select2('focus');
            }
        }); 
    });
    $("#length, #quantity, #rate").keyup(function() {
        var length      = $("#length").val();
        var quantity    = $("#quantity").val();
        var rate        = $("#rate").val();
        var amount      = length * quantity * rate;
        $("#item_amount").text(parseFloat(amount).toFixed(2));
    });
    $(".item_save").click(function() {
        var project     = $("#project").val();
        var category    = $("#description").val();
        var width       = "";
        var height      = "";
        var length      = $("#length").val();
        var quantity    = $("#quantity").val();
        var unit        = $("#item_unit").text();
        var rate        = $("#rate").val();
        var amount      = $("#item_amount").text();
        var remark      = $("#remark").val();
        var estimation  = $("#estimationId").val();
        if(project == "" || category == "" || quantity == "" || unit == "") {
            $("#errorMessages").show()
                .html('<div class="alert alert-danger">Enter all mandatory fields.</div>')
                .fadeOut(10000);
        } else {
            $.ajax({
                url: "<?php echo Yii::app()->createAbsoluteUrl("purchase/saveupdateestimation"); ?>",
                data: {"project": project, "type": "A", "category": category, "width": width, "height": height, "length": length, "quantity": quantity, "unit": unit, "rate": rate, "amount": amount, "remark": remark, "estimation": estimation},
                type: "POST",
                success:function(data){
                    if(data == 1) {
                        $("#errorMessages").show()
                            .html('<div class="alert alert-success">Item estimation updated successfully.</div>')
                            .fadeOut(10000);
                        window.location.href = "<?php echo Yii::app()->createAbsoluteUrl("purchase/viewestimation"); ?>&project_id="+project;  
                    } else {
                        $("#errorMessages").show()
                            .html('<div class="alert alert-danger">Failed. Please try again.</div>')
                            .fadeOut(10000);
                    }
                }
            });
        }
        
    });
});

</script>
<script>
(function() {
   var mainTable = document.getElementById("main-table"); 
   var tableHeight = mainTable.offsetHeight; 
   if (tableHeight > 380) { 
  		var fauxTable = document.getElementById("faux-table");
		document.getElementById("table-wrap").className += ' ' + 'fixedON';
  		var clonedElement = mainTable.cloneNode(true); 
  		clonedElement.id = "";
  		fauxTable.appendChild(clonedElement);
   }
})();
</script>

<script>
$(".popover-test").popover({
            html: true,
                content: function() {
                  //return $('#popover-content').html();
                  return $(this).next('.popover-content').html();
                }
        });
    $('[data-toggle=popover]').on('click', function (e) {
       $('[data-toggle=popover]').not(this).popover('hide');
    });
    $('body').on('hidden.bs.popover', function (e) {
        $(e.target).data("bs.popover").inState.click = false;
    });
    $('body').on('click', function (e) {
        $('[data-toggle=popover]').each(function () {
            // hide any open popovers when the anywhere else in the body is clicked
            if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                $(this).popover('hide');
            }
        });
    });
    
     $(document).ajaxComplete(function() {
                   $(".popover-test").popover({
                   html: true,
                   content: function() {
                     return $(this).next('.popover-content').html();
                   }
               });
               

           });
    </script>
    
    <style>
        
        .tooltip-hiden {
    width: auto}
    </style>