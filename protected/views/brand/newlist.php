<style>
.exp-list {
	padding-left: 0px !important;
}

.error {
    color: #ff0000;
  }
</style>
<!--<link rel="stylesheet" href="<?php // echo Yii::app()->theme->baseUrl; ?>/plugins/message/css/jquery.message.css">-->
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/validation/jquery.validate.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/validation/additional-methods.js"></script>
<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$this->breadcrumbs=array(
	'Purchase List',)
?>
<div class="container" id="project">
<h2>Brand</h2>

<?php 
 // $this->renderPartial('_search', array('model' => $model)) ?>
<div class="add-btn">
   
</div>

<span class="error_message"></span>
<form class="form-inline" id="form_brand">
  <div class="form-group">
    <input type="text" name="brand" class="form-control" id="brand" placeholder="Brand">
  </div>
  <button type="button" id="submit" class="btn btn-default">Add</button>
</form>



<?php if(Yii::app()->user->hasFlash('success')):?>
    <div class="info" style="width: 500px;margin-left: 400px;padding: 8px;color: green;font-size: 20px;font-weight: bold; padding-top: 46px;">
        <?php echo Yii::app()->user->getFlash('success'); ?>
    </div>
<?php endif; ?>


<?php if(Yii::app()->user->hasFlash('error')):?>
    <div class="info" style="width: 500px;margin-left: 400px;padding: 8px;color: red;font-size: 20px;font-weight: bold; padding-top: 46px;">
        <?php echo Yii::app()->user->getFlash('error'); ?>
    </div>
<?php endif; ?>


<br>
<br>





<div id="msg_box"></div>
<div class="exp-list">
<?php  $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	//'itemView'=>'_newview',
	/*'sortableAttributes'=>array(
                        'vendor_id' => 'Vendor',
                        'project_id'=>'Project',
                        'purchase_no' => 'Purchase No',

                        ), */

	'itemView' => '_newview', 'template' => '<div>{summary}{sorter}</div><div class="tblwrap scrollTable"><div class="container1"><table cellpadding="10" class="table  list-view sorter">{items}</table></div></div>{pager}',
)); ?>


 <?php
        //echo "<pre>";  print_r($dataProvider);exit;
      /*  $this->widget('zii.widgets.CListView', array(
            'dataProvider' => $dataProvider,
            'itemView' => '_newview', 'template' => '<div>{summary}{sorter}</div><div class="tblwrap scrollTable"><div class="container1"><table cellpadding="10" class="table  list-view sorter">{items}</table></div></div>{pager}',
            //'sortableAttributes' => array('ex_percent' => '% of Expense')
        ));*/
        ?>
        
        
</div>
</div>



<div id='FlickrGallery'></div>



<style>
a.button {
    background-color: #6a8ec7;
    display: block;
    padding: 5px;
    color: #fff;
    cursor: pointer;
    float: right;
    border: 1px solid #6a8ec8;
}
</style>
<script>
	
	$(document).ready(function(){
		
  $("#form_brand").validate({
    rules: {
      brand: {
        required: true,
        remote: {
					url: "<?php echo  Yii::app()->createAbsoluteUrl('brand/checkbrand'); ?>",
					type: "get",
					data: {
						brand: function() {
							return $( "#brand" ).val();
						}
					}
				}
      },
    },
    messages: {
      brand: {
        required: "Please enter brand",
        remote: jQuery.validator.format("{0} is already taken.")
      }
    },
  });

		
		
		/* $(document).keypress(function(event){
                    var keycode         = (event.keyCode ? event.keyCode : event.which);
                    if (keycode == '13') {
                        $('#submit').click();
                    }
                });*/
		
		
		 $('#submit').click(function () {
			  if($("#form_brand").valid()) {
					var urls = '<?php echo  Yii::app()->createAbsoluteUrl('brand/newlist'); ?>'
					var test = $('#brand').val();
					//alert(test);
						$.ajax({
                        url:'<?php echo  Yii::app()->createAbsoluteUrl('brand/create'); ?>',
						type:'GET',
						//dataType:'json',
						data:{data:test},
						async:true,
                        success: function (response)
                        {
							$('.exp-list').html(response);
							$('#brand').val('');
                        }
                    });
				}
                });
                
                
            $('#brand').keypress(function (event) {
			 
			   var keycode         = (event.keyCode ? event.keyCode : event.which);
			   if (keycode == '13') {
                        $('#submit').click();
                        event.preventDefault();
               }
			 
             });
                
		
    })
	
</script>

<style>
 .container1 {
        width: 100%;
        height: 330px;
        overflow: auto;
        position: relative;
    }

th,
.top-left {
	background: #eee;
}
.top-left {
	box-sizing: border-box;
	position: absolute;
	left: 0;
	border-right: 1px solid #ccc;
	border-bottom: 1px solid #ccc;
}

table {
   
	border-spacing: 0;
	//position: absolute;
   
}
th,
td {
	border-right: 1px solid #ccc !important;

}

</style>
