<?php
/* @var $this MaterialEntriesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Material Entries',
);

$this->menu=array(
	array('label'=>'Create MaterialEntries', 'url'=>array('create')),
	array('label'=>'Manage MaterialEntries', 'url'=>array('admin')),
);
?>

<h1>Material Entries</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
