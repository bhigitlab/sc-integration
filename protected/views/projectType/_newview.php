<?php
/* @var $this ProjectsController */
/* @var $data Projects */
?>



<?php

?>

<?php
if ($index == 0) {
    ?>
    <thead>
    <tr> 
        <th>SI No</th>
        <th>Client Type</th>
        <th  style="display: none;"></th>          
    </tr>   
    </thead>
<?php } ?>
    <tr> 
        <td style="width: 50px;"><?php echo $index+1; ?></td>
        
        <td  class="text_black" data-value="<?php echo $data->project_type; ?>" data-id="<?php echo $data->ptid; ?>" >
            <span class="list_item"><?php echo $data->project_type; ?></span>
            <?php
                if(isset(Yii::app()->user->role) && (in_array('/projectType/updateprojecttype', Yii::app()->user->menuauthlist))){
            ?>
            <span class="fa fa-edit editable" onclick="editaction(this,event);" data-id="<?php echo $data->ptid; ?>" data-value="<?php echo CHtml::encode($data->project_type); ?>" style="cursor:pointer;"></span>
            <?php } ?>
            <span class="delete_clientype pull-right"  title="Delete" onclick="deletClientType(this,<?php echo $data->ptid; ?>)">
                <span  class="fa fa-trash deleteClientType" ></span>
            </span>                
        </td>
        
        <td class='editelement test' style="display:none;">
        <input  class='project_type singletextbox' type = 'text'  name='project_type' data-value="<?php echo CHtml::encode($data->project_type); ?>">
        <span class="errorMessage"></span>
        <a class='save btn btn-info btn-xs' onclick="saveaction(this,event);" type = 'submit' data-id="<?php echo $data->ptid; ?>" >Save</a>
        <a class='cancel btn btn-default btn-xs' onclick="cancelaction(this,event);">Cancel</a>
        <br>
        <span class='errorMessage1'></span>
       
        
        </td>		
    
    </tr>  
