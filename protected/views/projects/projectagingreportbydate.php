<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/tableHeadFixer.js"></script>
<?php 
$tblpx  = Yii::app()->db->tablePrefix;
$where = ' where 1=1 ';       
$sql = "SELECT p.* FROM jp_invoice i "
        . " LEFT JOIN jp_projects p "
        . " ON i.project_id = p.pid "        
        . "WHERE p.pid IS NOT NULL group by p.pid order by p.name ASC";
$aging_array = Yii::app()->db->createCommand($sql)->queryAll();
?>
<div class="container">
    <div class="clearfix">
    <br>
        <h5 class="text-center"><?php echo Yii::app()->name; ?></h5>
        <h2 class="text-center" style="padding-top: 3px;">Aging Summary By Invoice Due Date</h2>
        <h5 class="text-center">As of <?php echo date ("d/m/Y")?></h5>
        <a href="<?php echo Yii::app()->createUrl("vendors/paymentReport")?>" class="pull-right save_btn mt-2">Vendor Payment Report</a>
    </div>
        <?php if(count($aging_array) > 0){ ?>
            <div class="clearfix">
                <div class="pull-right">Total <?php echo count($aging_array) ?> records</div>
            </div>
        <?php } ?>
        <div id="parent">            
            <table class="table" id="fixTable">
                <thead>
                    <tr>
                        <th>Project Name</th>
                        <th>Current</th>
                        <th>1-15 Days</th>
                        <th>16-30 Days</th>
                        <th>31-45 Days</th>
                        <th>>45 Days</th> 
                                               
                    </tr>
                </thead>
                <tbody>
                    <?php                     
                    $tblpx  = Yii::app()->db->tablePrefix;
                     
                    
                    foreach ($aging_array as $value) { ?>
                        <tr>
                            <td><?php echo $value['name']; ?></td>                            
                            <td>
                                <?php
                                $where = " AND `date` = DATE_SUB(NOW(), INTERVAL 0 DAY)";

                                $sql = "SELECT SUM(amount+tax_amount) as amount "
                                . " FROM {$tblpx}invoice as i "
                                . " WHERE `invoice_status` = 'saved' AND "
                                . " i.project_id= ".$value['pid'] 
                                . " $where";                                                                                           
                                $data = Yii::app()->db->createCommand($sql)->queryRow();
                                $paidamount = Controller::getPaidInvoiceAmount(1,$value['pid']);
                                echo Controller::money_format_inr(($data['amount']-$paidamount),2);
                                ?>
                            </td>
                            <td>
                                <?php
                                $where = " AND `date` < DATE_SUB(NOW(), INTERVAL 1 DAY) AND `date` >= DATE_SUB(NOW(), INTERVAL 15 DAY) ";
                                $sql = "SELECT SUM(amount+tax_amount) as amount "
                                . " FROM {$tblpx}invoice as i "
                                . " WHERE `invoice_status` = 'saved' AND "
                                . " i.project_id= ".$value['pid'] 
                                . " $where";  
                                
                                // echo $sql;
                                $data = Yii::app()->db->createCommand($sql)->queryRow();                    
                                $paidamount = Controller::getPaidInvoiceAmount(2,$value['pid']);
                                echo Controller::money_format_inr(($data['amount']-$paidamount),2);
                                ?>
                            </td>
                            <td>
                            <?php
                                $where = " AND `date` < DATE_SUB(NOW(), INTERVAL 16 DAY) AND `date` > DATE_SUB(NOW(), INTERVAL 30 DAY) ";
                                $sql = "SELECT SUM(amount+tax_amount) as amount "
                                . " FROM {$tblpx}invoice as i "
                                . " WHERE `invoice_status` = 'saved' AND "
                                . " i.project_id= ".$value['pid'] 
                                . " $where";                                                                                           
                                $data = Yii::app()->db->createCommand($sql)->queryRow();                    
                                $paidamount = Controller::getPaidInvoiceAmount(3,$value['pid']);
                                echo Controller::money_format_inr(($data['amount']-$paidamount),2);
                                ?>
                            </td>
                            <td>
                                <?php
                                $where = " AND `date` < DATE_SUB(NOW(), INTERVAL 31 DAY) AND `date` > DATE_SUB(NOW(), INTERVAL 45 DAY) ";
                                $sql = "SELECT SUM(amount+tax_amount) as amount "
                                . " FROM {$tblpx}invoice as i "
                                . " WHERE `invoice_status` = 'saved' AND "
                                . " i.project_id= ".$value['pid'] 
                                . " $where";                                                                                           
                                $data = Yii::app()->db->createCommand($sql)->queryRow();                    
                                $paidamount = Controller::getPaidInvoiceAmount(4,$value['pid']);
                                echo Controller::money_format_inr(($data['amount']-$paidamount),2);
                                ?></td>
                            <td>
                            <?php
                                $where = " AND `date` < DATE_SUB(NOW(), INTERVAL 45 DAY)";
                                $sql = "SELECT SUM(amount+tax_amount) as amount "
                                . " FROM {$tblpx}invoice as i "
                                . " WHERE `invoice_status` = 'saved' AND "
                                . " i.project_id= ".$value['pid'] 
                                . " $where";                                                                                           
                                $data = Yii::app()->db->createCommand($sql)->queryRow();                    
                                $paidamount = Controller::getPaidInvoiceAmount(5,$value['pid']);
                                echo Controller::money_format_inr(($data['amount']-$paidamount),2);
                                ?>
                            </td>
                            
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        
    </div>
</div>

<script>
    $("#fixTable").tableHeadFixer({
        'foot': true,
        'head': true
    });
</script>

<style>
    #parent {
        max-height: 400px;
        overflow:auto;
    }
</style>
