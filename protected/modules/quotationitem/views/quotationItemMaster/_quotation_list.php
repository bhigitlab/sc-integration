<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.js"></script>

<div class="" id="quotation_item_edit" style="display:none;">
</div>
<div id="msg_box"></div>
<div id="table-scroll" class="table-scroll">
    <div id="faux-table" class="faux-table" aria="hidden"></div>
    <div id="table-wrap" class="table-wrap">
        <div class="table-responsive">

            <table cellpadding="10" class="table">
                <thead>
                    <tr>
                        <th>Sl No</th>
                        <th>Work Type</th>
                        <th>Work Type Label</th>
                        <th>Material</th>
                        <th>Finish</th>
                        <th>Image</th>
                        <th>Description</th>
                        <th>Rate</th>
                        <th>Profit%</th>
                        <th>Discount%</th>
                        <th>Action</th>

                    </tr>
                </thead>
                <tbody>

                    <?php
                    if (!empty($item_datas)) {
                        $i = 1;
                        $mrp = 0;
                        $amount_afetr_discount = 0;
                        $category_id = '';
                        $previous_cat = '';
                        foreach ($item_datas as $view_data) {
                            $datas = $view_data['items_list'];
                            foreach ($datas as $key => $value) {
                                $category_id = $value['quotation_category_id'];
                                $category_name = QuotationCategoryMaster::model()->findByPk($category_id);
                                $work_type_label = QuotationWorktypeMaster::model()->findByPk($value['work_type_id']);
                                if ($category_id != $previous_cat) {
                                    $i = 1;
                                    ?>
                                    <tr>
                                        <td colspan="10">
                                            <?php echo $category_name['name']; ?>
                                        </td>
                                    </tr>
                                <?php } ?>
                                <tr>
                                    <td><?php echo $i; ?></td>
                                    <td><?php echo $work_type_label['name']; ?></td>
                                    <td><?php echo $value['worktype_label']; ?></td>
                                    <?php
                                    if ($work_type_label['template_id'] == 1) {
                                        $data_material_shutter = Controller::getMaterialNames($value['shutter_material_id']);
                                        $data_material_carcass = Controller::getMaterialNames($value['carcass_material_id']);
                                        $finish_data = Controller::getFinishData();
                                        ?>
                                        <td>
                                            <b>Shutter :</b>
                                            <span>
                                                <?php echo $data_material_shutter ?>
                                            </span><br>
                                            <b>Carcass: </b>   
                                            <span>
                                                <?php echo $data_material_carcass ?>
                                            </span>
                                        </td>
                                        <td>
                                            <b>Shutter :</b> 
                                            <span>
                                                <?php echo isset($value['shutter_finish_id']) ? $finish_data[$value['shutter_finish_id']] : ''; ?>
                                            </span><br>
                                            <b>Carcass: </b>  
                                            <span>
                                                <?php echo isset($value['carcass_finish_id']) ? $finish_data[$value['carcass_finish_id']] : ''; ?>
                                            </span>
                                        </td>
                                        <?php
                                    } else if (3 == $work_type_label['template_id']) {
                                        $material = Controller::getMaterialNames($value['material_ids']);
                                        $finish = Controller::getFinishData();
                                        ?>
                                        <td>
                                            <span>
                                                <?php echo $material; ?>
                                            </span><br>
                                        </td>
                                        <td>
                                            <span>
                                                <?php echo isset($value['finish_id']) ? $finish[$value['finish_id']] : ''; ?>
                                            </span>
                                        </td>
                                    <?php } else {  ?>
                                        <td></td>
                                        <td></td>
                                    <?php } ?>
                                    <td> <?php
                                        if ($value["image"] != '') {
                                            $path = realpath(Yii::app()->basePath . '/../uploads/image/' . $value["image"]);
                                            if (file_exists($path)) {
                                                echo CHtml::tag('img',
                                                    array(
                                                        'src' => Yii::app()->request->baseUrl . "/uploads/image/" . $value["image"],
                                                        'alt' => '',
                                                        'class' => 'pop',
                                                        'modal-src' => Yii::app()->request->baseUrl . "/uploads/image/" . $value["image"],
                                                        'width'=>'100px'
                                                    )
                                                );
                                            }
                                        } 
                                        ?>
                                    </td>
                                    <?php if ($work_type_label['template_id'] == 1) { ?>
                                        <td><b>Shutter :</b> 
                                            <span> <?php echo $value['shutterwork_description'] ?></span><br>
                                            <b>Carcass :</b>  <span><?php echo $value['caracoss_description'] ?></span>
                                        </td>
                                    <?php } else { ?>
                                        <td><?php echo $value['description']; ?></td>
                                    <?php } ?>
                                    <td><?php echo $value['rate'] ?></td>
                                    <td><?php echo $value['profit'] ?></td>
                                    <td><?php echo $value['discount'] ?></td>
                                    <td>
                                        <button id="<?php echo $value['id'] ?>" data-catid ="<?php echo $value['quotation_category_id'] ?>" data-wtid ="<?php echo $value['work_type_id'] ?>" class="editbtn"><span class="fa fa-edit editSpecification"></span></button></span>
                                        <button id="<?php echo $value['id'] ?>" data-catid ="<?php echo $value['quotation_category_id'] ?>" data-wtid ="<?php echo $value['work_type_id'] ?>" class="removebtn"><span class="fa fa-trash deleteSpecification"></span></button></td>

                                </tr>
                                <?php
                                $mrp = $mrp + $value['rate'];
                                $i++;
                                $previous_cat = $category_id;
                            }
                        }
                        ?>
                    <?php } else {
                        ?>
                        <tr>
                            <td colspan="11" class="text-center">No results found</td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<script>
    $(document).on('click', '.removebtn', function (e) {
        e.preventDefault();
        element = $(this);
        var item_id = $(this).attr('id');
        var answer = confirm("Are you sure you want to delete?");
        if (answer) {
            var data = {
                'item_id': item_id,
            };
            $.ajax({
                method: "GET",
                async: false,
                data: {
                    data: data
                },
                dataType: "json",
                url: '<?php echo Yii::app()->createUrl('quotationitem/quotationItemMaster/Removeitem'); ?>',
                success: function (response) {
                    $('html, body').animate({
                        scrollTop: $("#content").position().top - 100
                    }, 500);
                    if (response.response == "success") {
                        location.reload();
                        $(".alert-success").show().html(response.msg).delay(3000).fadeOut();
                    } else if (response.response == "warning") {
                        $(".alert-warning").show().html(response.msg).delay(3000).fadeOut();
                    } else {
                        $(".alert-danger").show().html(response.msg).delay(3000).fadeOut();
                    }
                }
            });

        } else {
            return false;
        }

    });

    $(document).on('click', '.editbtn', function (e) {
        e.preventDefault();
        element = $(this);
        var item_id = $(this).attr('id');
        var data = {
            'item_id': item_id,
        };
        $.ajax({
            method: "POST",
            data: {
                data: data
            },
            url: '<?php echo Yii::app()->createUrl('quotationitem/quotationItemMaster/UpdateQuotationItem'); ?>',
            success: function (data) {
                if (data) {
                    $("#quotation_item_edit").show();
                    $("#quotation_item_edit").html(data);

                    $('html, body').animate({
                        scrollTop: $("#content").position().top - 100
                    }, 500);
                    $('.js-example-basic-multiple').select2({
                        width: '100%',
                        placeholder: "Select Material",
                    });


                    $('.shutterfinish').select2({
                        width: '100%',
                        placeholder: "Select Finish",
                    });

                    $('.caracassfinish').select2({
                        width: '100%',
                        placeholder: "Select Finish",
                    });
                }
            }
        });

    });
</script>
