<?php

/**
 * This is the model class for table "{{clients}}".
 *
 * The followings are the available columns in table '{{clients}}':
 * @property integer $cid
 * @property string $name
 * @property string $description
 * @property integer $status
 * @property string $created_date
 * @property integer $created_by
 * @property string $updated_date
 * @property integer $updated_by
 *
 * The followings are the available model relations:
 * @property Users $createdBy
 * @property Users $updatedBy
 * @property Status $status0
 * @property Projects[] $projects
 */
class Clients extends CActiveRecord {

    public $status = 1;  //default radio button selection on create.
    public $username;
    public $password;
    public $gst_no;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Clients the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return '{{clients}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        if(Yii::app()->user->role == 3)
            $required = array('name, project_type,address,company_id,description,phone,email_id', 'required', 'message' => 'Enter {attribute}.');
        else
            $required = array('name, project_type,address,company_id,phone', 'required', 'message' => 'Enter {attribute}.');
        return array(
            $required,
            array('local_address,contact_person','validatedeatils'),
            array('name','unique'),
            array('status', 'required', 'message' => 'Choose one {attribute}.'),
            array('status, project_type', 'numerical', 'integerOnly' => true),
            array('name', 'length', 'max' => 100),
            array('description,phone,email_id,company_id', 'safe'),
            array('username', 'checkexists','on' => 'insert'),
            array('username', 'checkexists','on' => 'insert'),
            array('username', 'checkexistsedit','on' => 'update'),
            array('created_date, gst_no, created_by, updated_date, updated_by', 'safe'),
            array('phone', 'length', 'min' => 10),
            array('phone','match', 'pattern' => '{^\+?[0-9-]+$}',
           'message' => 'Invalid phone number'),
            
            array('email_id', 'email'),
            array('gst_no', 'match', 'pattern' => '{^[0-9]{2}[A-Z]{5}[0-9]{4}[A-Z]{1}[1-9A-Z]{1}Z[0-9A-Z]{1}$}',
                    'message' => 'Invalid GST number'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('cid, name,address,local_address,contact_person,project_type, gst_no, description, status, created_date, created_by, updated_date, updated_by,phone,email_id,username,password', 'safe', 'on' => 'search'),
        );
    }
	
	
	public function validatedeatils($attribute, $params)
       {
				
				
				//if ($this->project_type == 7 ) 
               if ($this->projectType['project_type'] == 'NRI' )  
               {
						
                       if ( $this->local_address == '' )
                       {
                               $this->addError('local_address', 'Please Enter  Local Address');
                       }
                       if ( $this->contact_person == '' )
                       {
                               $this->addError('contact_person', 'Please Enter Contact Person');
                       }
					   
               }
       }
       
    public function checkexists($attribute, $params) {
        if (!$this->hasErrors()) {  // we only want to authenticate when no input errors
            if ($this->username != "") {
                $user = Users::model()->findAll(array('condition' => 'username= "' . $this->username . '"'));
                if ($user != null) {
                    $this->addError("username", ("This username already exists"));
                }
            }
        }
    }
    public function checkexistsedit($attribute, $params) {
        if (!$this->hasErrors()) {  // we only want to authenticate when no input errors
            if ($this->username != "") {
                $exist = Users::model()->findAll(array('condition' => 'client_id= "' . $this->cid . '"'));
                if ($exist == null) {
                    $user = Users::model()->findAll(array('condition' => 'username= "' . $this->username . '"'));
                    if ($user != null) {
                        $this->addError("username", ("This username already exists"));
                    }
                } else {
                    $user = Users::model()->findAll(array('condition' => "username= '$this->username  ' and (client_id != $this->cid or client_id is null)"));
                    if (count($user) >0) {
                        $this->addError("username", ("This username already exists"));
                    }
                }
                
            }

        }
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'projectType' => array(self::BELONGS_TO, 'ProjectType', 'project_type'),
            'createdBy' => array(self::BELONGS_TO, 'Users', 'created_by'),
            'updatedBy' => array(self::BELONGS_TO, 'Users', 'updated_by'),
            'status0' => array(self::BELONGS_TO, 'Status', 'status'),
            'projects' => array(self::HAS_MANY, 'Projects', 'client_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'cid' => 'Client Id',
            'name' => 'Name',
            'description' => 'Description',
            'project_type' => 'Client Type',
            'status' => 'Status',
            'created_date' => 'Created Date',
            'created_by' => 'Created By',
            'updated_date' => 'Updated Date',
            'updated_by' => 'Updated By',
            'phone' => 'Phone',
            'email_id' => 'Email ID',
            'address' => 'Address',
            'local_address' => 'Local Address',
            'gst_no' => 'GST No',
            'contact_person' => 'Contact Person',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.
        $crm_api_integration=ApiSettings::model()->crmIntegrationStatus();

        $criteria = new CDbCriteria;

        $criteria->compare('cid', $this->cid);
        $criteria->compare('name', $this->name, true);
        if($crm_api_integration!='1'){
            $criteria->compare('project_type', $this->project_type);
        }
        $criteria->compare('description', $this->description, true);
        $criteria->compare('status', $this->status);
        $criteria->compare('created_date', $this->created_date, true);
        $criteria->compare('created_by', $this->created_by);
        $criteria->compare('updated_date', $this->updated_date, true);
        $criteria->compare('gst_no', $this->gst_no, true);
        $criteria->compare('updated_by', $this->updated_by);
        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        $newQuery = "";

        if($crm_api_integration!='1'){
            foreach($arrVal as $arr) {
                if ($newQuery) $newQuery .= ' OR';
                $newQuery .= " FIND_IN_SET('".$arr."', company_id)";
            }
            $criteria->addCondition($newQuery);
        }
        
        return new CActiveDataProvider($this, array('pagination' => array('pageSize' => 25,),
            'criteria' => $criteria, 'sort' => array(
                'defaultOrder' => 'name ASC',), 'pagination' => false
        ));
    }

}
