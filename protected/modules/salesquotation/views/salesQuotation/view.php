<?php
/* @var $this SalesQuotationController */
/* @var $model SalesQuotation */

$this->breadcrumbs=array(
	'Sales Quotations'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List SalesQuotation', 'url'=>array('index')),
	array('label'=>'Create SalesQuotation', 'url'=>array('create')),
	array('label'=>'Update SalesQuotation', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete SalesQuotation', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage SalesQuotation', 'url'=>array('admin')),
);
?>

<h1>View SalesQuotation #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'company_id',
		'client_id',
		'project_id',
		'date_quotation',
		'invoice_no',
		'category_id',
		'caregory_label',
		'work_type',
		'shutterwork_description',
		'carcass_description',
		'description',
		'worktype_label',
		'unit',
		'quantity',
		'quantity_nos',
		'mrp',
		'amount_after_discount',
		'status',
		'created_by',
		'created_on',
	),
)); ?>
