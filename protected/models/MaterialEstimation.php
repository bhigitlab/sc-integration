<?php

/**
 * This is the model class for table "{{material_estimation}}".
 *
 * The followings are the available columns in table '{{material_estimation}}':
 * @property integer $id
 * @property integer $project_id
 * @property string $material  // Added field
 * @property string $specification
 * @property string $quantity
 * @property string $unit
 * @property double $rate
 * @property double $amount
 * @property string $remarks
 * @property string $created_at
 *
 * The followings are the available model relations:
 * @property PmsProjects $project
 */
class MaterialEstimation extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{material_estimation}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('project_id, material, specification, quantity, unit, rate, amount', 'required'),
            array('project_id', 'numerical', 'integerOnly'=>true),
            array('rate, amount', 'numerical'),
            array('material, specification, quantity, unit, remarks', 'length', 'max'=>255),
            array('created_at', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, project_id, material, specification, quantity, unit, rate, amount, remarks, created_at', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'project' => array(self::BELONGS_TO, 'PmsProjects', 'project_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'project_id' => 'Project',
            'material' => 'Material',  // Added label
            'specification' => 'Specification',
            'quantity' => 'Total Quantity',
            'unit' => 'Unit',
            'rate' => 'Rate',
            'amount' => 'Total Amount',
            'remarks' => 'Remarks',
            'created_at' => 'Created At',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('project_id', $this->project_id);
        $criteria->compare('material', $this->material, true);  // Added criterion
        $criteria->compare('specification', $this->specification, true);
        $criteria->compare('quantity', $this->quantity, true);
        $criteria->compare('unit', $this->unit, true);
        $criteria->compare('rate', $this->rate);
        $criteria->compare('amount', $this->amount);
        $criteria->compare('remarks', $this->remarks, true);
        $criteria->compare('created_at', $this->created_at, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return MaterialEstimation the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}

