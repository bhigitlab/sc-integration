<?php
/* @var $this ProjectsController */
/* @var $data Projects */
?>

<?php
$tblpx = Yii::app()->db->tablePrefix;
$id = $data['pid'];
//$credit= Projects::model()->getCreditAmount($data['pid']);
//$debit= Projects::model()->getDebitAmount($data['pid']);
// $totexpense=Projects::model()->getexpenseAmount($data['pid']);
//$amntrecvclient=Projects::model()->getclientAmount($data['pid']);
//$amntpayvendor=Projects::model()->getvendorAmount($data['pid']);
?>
<?php
// $receipt= Projects::model()->getReceiptAmount($data['pid']);
$styleclass = "background-color: #fff;";
//$diff = $receipt - $debit;
$projectid = $data['pid'];
$receipt_data = Yii::app()->db->createCommand("SELECT SUM(paidamount) as receipt_amount FROM {$tblpx}expenses WHERE projectid = " . $projectid . " AND type = 72")->queryRow();
if (isset($receipt_data['receipt_amount'])) {
    $data['tot_receipt'] = $receipt_data['receipt_amount'];
} else {
    $data['tot_receipt'] = 0;
}
$percentage = Yii::app()->db->createCommand("SELECT percentage FROM {$tblpx}projects WHERE pid = {$projectid}")->queryRow();
$servise_charge = ($data['tot_expense'] * $percentage['percentage']) / 100;

$diff = $data['tot_receipt'] - ($data['tot_expense'] + $servise_charge);
//$diff = $data['tot_receipt'] - $data['tot_expense'];
$deficit = 0;

$surplus = 0;

if ($diff < 0) {

    $deficit = $diff;
    $styleclass = "background-color: #ECB3B3;";
} elseif ($diff > 0) {
    $surplus = $diff;
    $styleclass = "background-color: #C3EAC3;";
}
/* @var $this ActivityController */
/* @var $data Activity */
if ($index == 0) { ?>
    <thead class="entry-table">
        <tr>
            <th>Sl No.</th>
            <th>Project</th>
            <th>Company</th>
            <th>Start Date</th>
            <th>Completion Date</th>
            <th>Site</th>
            <?php
            if (Yii::app()->user->role == 1) {
                echo '  <th>Assigned To</th>';
            }
            ?>
            <th>Client</th>
            <th>Category</th>
            <th>Duration (Days)</th>
            <th>Status</th>
            <th>Amount Billed To Client</th>
            <th>Amount Paid to Vendors</th>
            <th>Amount Received from client</th>
            <th>Total Project Expense</th>
            <th>Surplus</th>
            <th>Deficit</th>

            <th> % of Expense</th>
            <?php
            if (Yii::app()->user->role == 1) {
                echo '  <th>Created By</th>';
            }
            ?>
            <?php
            if (isset(Yii::app()->user->role) && (in_array('/projects/updateproject', Yii::app()->user->menuauthlist))) {
                ?>
                <th>Action</th>
            <?php } ?>
        </tr>
    </thead>
    <?php
}
?>
<?php
if ($data['pms_project_id'] !== '0' && $data['pms_project_id'] !== null) {
    $styleval = '';
} else {
    $styleval = 'background-color:#A9E9EC;';
}
?>
<tr style="<?php echo $styleval; ?>">
    <td class="text-right">
        <?php echo $index + 1; ?>
    </td>
    <?php
    $res = Yii::app()->db->createCommand("SELECT type_name,{$tblpx}project_exptype.type_id from {$tblpx}project_exptype inner join {$tblpx}expense_type ON {$tblpx}expense_type.type_id={$tblpx}project_exptype.type_id WHERE project_id = " . $data['pid'])->queryAll();


    $newsql = Yii::app()->db->createCommand("SELECT work_type
          from {$tblpx}project_work_type 
          inner join {$tblpx}work_type ON {$tblpx}work_type.wtid={$tblpx}project_work_type.wrk_type_id 
          WHERE project_id = " . $data['pid'])->queryAll();
    ?>

    <!--            <div class="tooltips leadttile"><?php //echo CHtml::encode($data['name']); 
    ?>
                    <span class="tooltiptext">
        <?php
        echo 'ExpenseType:';
        $types = array();
        foreach ($res as $result) {
            array_push($types, $result['type_name']);
        }
        echo implode(', ', $types);

        echo '<br>';
        echo 'Project Types : ' . CHtml::encode(Projects::model()->getStatusname($data['project_status']));
        echo '<br>';
        echo 'Square Feet : ' . CHtml::encode($data['sqft']);
        echo '<br>';
        echo 'Square Feet Rate: ' . CHtml::encode($data['sqft_rate']);
        //echo 'Bill Amount : '.CHtml::encode($data['bill_amount']) ;
        echo '<br>';
        echo 'Percentage : ';
        echo isset($data['percentage']) ? CHtml::encode($data['percentage']) . '%' : '';
        echo '<br>';
        echo 'Work Contract : ' . CHtml::encode(Projects::model()->getStatusname($data['project_type']));
        echo '<br>';
        echo 'Work Type :';
        $wtypes = array();
        foreach ($newsql as $news) {
            array_push($wtypes, $news['work_type']);
        }
        echo implode(', ', $wtypes);
        echo '<br>';
        echo 'Description : ' . CHtml::encode($data['description']);
        echo '<br>';
        echo 'Remarks : ' . CHtml::encode($data['remarks']);
        echo '<br>';
        ?>
                    </span>
                      
                    </div>-->
    <!--</div>-->
    <?php
    $types = array();
    foreach ($res as $result) {
        array_push($types, $result['type_name']);
    }
    $wtypes = array();
    foreach ($newsql as $news) {
        array_push($wtypes, $news['work_type']);
    }
    $text = 'ExpenseType : ' . implode(', ', $types) . "<br>" . 'Project Types : ' . CHtml::encode(Projects::model()->getStatusname($data['project_status'])) . '<br> Square Feet : ' . CHtml::encode($data['sqft']) . '<br> Square Feet Rate: ' . CHtml::encode($data['sqft_rate']) . '<br> Percentage : ' . (isset($data['percentage']) ? CHtml::encode($data['percentage']) . '%' : '') . '<br> Work Contract : ' . CHtml::encode(Projects::model()->getStatusname($data['project_type'])) . '<br> Work Type :' . implode(', ', $wtypes) . '<br> Description : ' . CHtml::encode($data['description']) . '<br> Remarks : ' . CHtml::encode($data['remarks']) . '<br>';
    $breaks = array("<br />", "<br>", "<br/>");
    $text = str_ireplace($breaks, "\r\n", $text);
    ?>



    <td style="cursor:pointer" title="<?php echo $text; ?>"><span>
            <?php echo CHtml::encode($data['name']); ?>
        </span></td>
    <td>

        <?php
        $companyname = array();
        $user = Users::model()->findByPk(Yii::app()->user->id);
        $comVal = explode(',', $user->company_id);
        $arrVal = explode(',', $data['company_id']);
        foreach ($arrVal as $arr) {
            if (in_array($arr, $comVal)) {
                $value = Company::model()->findByPk($arr);
                if (isset($value->name)) {
                    array_push($companyname, $value->name);
                }

            }
        }
        echo implode(', ', $companyname);
        ?>
    </td>
    <!--</td>-->
    <td style="width:80px;">
        <?php echo isset($data['start_date']) ? CHtml::encode(date('d-M-Y', strtotime($data['start_date']))) : ''; ?>
    </td>
    <td>
        <?php echo isset($data['completion_date']) ? CHtml::encode(date('d-M-Y', strtotime($data['completion_date']))) : ''; ?>
    </td>
    <td>
        <?php echo CHtml::encode($data['site']); ?>
    </td>
    <?php
    if (Yii::app()->user->role == 1) {
        echo "<td>" . Projects::model()->getAssignedusers($data['pid']) . "</td>";
    }
    ?>

    <td>
        <?php echo Projects::model()->getClientname($data['client_id']); ?>
    </td>
    <td>
        <?php
        $category = Status::model()->findByPk($data['project_category']);
        if ($category)
            echo $category->caption;
        ?>
    </td>
    <td>
        <?php echo $data['project_duration']; ?>
    </td>
    <td>
        <!--completed,ongoing,all -->
        <?php
        $projectStatus = Status::model()->findByPk($data['project_status']);
        echo CHtml::encode($projectStatus['caption'])
            ?>
    </td>
    <!--    <td class="text-right"><?php echo (($data['tot_expense']) ? Controller::money_format_inr($data['tot_expense'], 2) : 0); ?></td>-->
    <td class="text-right">
        <?php echo CHtml::encode(Controller::money_format_inr($data['billed_to_client'], 2)); ?>
    </td>
    <!--    <td class="text-right"><?php echo (($data['tot_receipt']) ? Controller::money_format_inr($data['tot_receipt'], 2) : 0); ?></td>-->

    <td class="text-right">
        <?php echo (($data['tot_paid_to_vendor']) ? Controller::money_format_inr($data['tot_paid_to_vendor'], 2) : 0); ?>
    </td>
    <td class="text-right">
        <?php echo (($data['tot_receipt']) ? Controller::money_format_inr($data['tot_receipt'], 2) : 0) ?>
    </td>
    <td class="text-right">
        <?php echo (($data['tot_expense']) ? Controller::money_format_inr($data['tot_expense'], 2) : 0) ?>
    </td>
    <td class="text-right">
        <?php echo Controller::money_format_inr($surplus, 2, 1) ?>
    </td>
    <td class="text-right">
        <?php echo Controller::money_format_inr($deficit, 2, 1) ?>
    </td>

    <td class="text-right">
        <?php echo isset($data['ex_percent']) ? $data['ex_percent'] . '%' : '0%'; ?>
    </td>
    <?php
    if (Yii::app()->user->role == 1) {
        echo '<td>' . Projects::model()->getCreatedBy($data['pid']) . '</td>';
    }

    if (isset(Yii::app()->user->role) && (in_array('/projects/updateproject', Yii::app()->user->menuauthlist))) {
        ?>
        <td>
            <a class="fa fa-edit editProject" style="cursor:pointer;" data-id="<?php echo $data['pid']; ?>"
                onclick="editpro(<?php echo $data['pid']; ?>)"></a>
            <?php if (!ANCAPMS_LINKED) { ?>
                <?php
                $pms_api_integration = ApiSettings::model()->pmsIntegrationStatus();

                if ($pms_api_integration != 1) {

                    ?>
                    <span class="delete_pjt" title="Delete" onclick="deletProject(this,<?php echo $data['pid']; ?>)">
                        <span class="fa fa-trash deleteProject"></span>
                    </span>
                    <?php

                }
                ?>
            <?php } ?>
        </td>
    <?php } ?>

</tr>