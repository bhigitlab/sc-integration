<?php
$this->breadcrumbs = array(
    'performa invoice',
);

$url_data = '';
if (isset($_REQUEST["fromdate"])) {
    $url_data .= '&fromdate=' . $_REQUEST["fromdate"];
}
if (isset($_REQUEST["todate"])) {
    $url_data .= '&todate=' . $_REQUEST["todate"];
}

$spacing = "";
if (filter_input(INPUT_GET, 'export') == 'pdf') {
    $spacing = "style='margin:0px 30px'"; ?>
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/pdfstyle.css">
<?php } ?>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/tableHeadFixer.js"></script>
<div class="container" id="project">
    <div class="expenses-heading">
        <div class="clearfix">
            <?php if (!filter_input(INPUT_GET, 'export')) { ?>
                <button type="button" class="btn btn-info pull-right mt-0 mb-10">
                    <a href="<?php echo Yii::app()->request->requestUri . "&export=csv" . $url_data; ?>"
                        style="text-decoration: none; color: white;" title="SAVE AS EXCEL">
                        <i class="fa fa-file-excel-o button-icon" aria-hidden="true"></i>
                    </a>
                </button>
                <button type="button" class="btn btn-info pull-right mt-0 mb-10 margin-right-5">
                    <a href="<?php echo Yii::app()->request->requestUri . "&export=pdf" . $url_data; ?>"
                        style="text-decoration: none; color: white;" title="SAVE AS PDF">
                        <i class="fa fa-file-pdf-o button-icon" aria-hidden="true"></i>
                    </a>
                </button>
                <h3>Daily Expense Report By User</h3>
            <?php } ?>
        </div>
    </div>

    <?php if (!filter_input(INPUT_GET, 'export')) { ?>
        <?php $this->renderPartial('_newdailyexpensesearch', array('model' => $model)) ?>
    <?php } ?>
    <div class="" id="newlist" <?php echo $spacing ?>>
        <?php $this->widget('zii.widgets.CListView', array(
            'dataProvider' => $dataProvider,
            'itemView' => '_newdailyexpenseview',
            'template' => '<div>{summary}{sorter}</div><div id="parent"><div class="report-table-wrapper"><table class="table total-table " id="fixTable">{items}</table></div></div>{pager}',
            'ajaxUpdate' => false,
        )); ?>
    </div>
</div>
<style>
    #parent {
        max-height: 60vh;
    }
</style>
<script>
    $(function () {
        $("#fixTable").tableHeadFixer({ 'head': true });
        $("#Dailyexpense_fromdate").datepicker({ dateFormat: 'dd-mm-yy' });
        $("#Dailyexpense_todate").datepicker({ dateFormat: 'dd-mm-yy' });
    });
    $("#clearForm").click(function () {
        window.location.href = "<?php echo Yii::app()->createAbsoluteUrl("dailyexpense/dailyexpensebyuser") ?>";
    });
</script>