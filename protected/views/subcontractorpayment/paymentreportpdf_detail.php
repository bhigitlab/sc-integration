<?php
$tblpx = Yii::app()->db->tablePrefix;
$newQuery1 = "";
$newQuery2 = "";
$newQuery3 = "";
$newQuery4 = "";
$newQuery5 = "";
$user = Users::model()->findByPk(Yii::app()->user->id);
$arrVal = explode(',', $user->company_id);
if($company_id == ''){
    foreach($arrVal as $arr) {
        if ($newQuery1) $newQuery1 .= ' OR';
        if ($newQuery2) $newQuery2 .= ' OR';
        if ($newQuery3) $newQuery3 .= ' OR';
        $newQuery1 .= " FIND_IN_SET('".$arr."', {$tblpx}scquotation.company_id)";
        $newQuery2 .= " FIND_IN_SET('".$arr."', {$tblpx}subcontractor_payment.company_id)";
        $newQuery3 .= " FIND_IN_SET('".$arr."', {$tblpx}subcontractor.company_id)";
    }
}else{
    $newQuery1 .= " FIND_IN_SET('".$company_id."', {$tblpx}scquotation.company_id)";
    $newQuery2 .= " FIND_IN_SET('".$company_id."', {$tblpx}subcontractor_payment.company_id)";
    $newQuery3 .= " FIND_IN_SET('".$company_id."', {$tblpx}subcontractor.company_id)";
}
if($project_id !=''){
    $newQuery4 = " AND {$tblpx}scquotation.project_id =".$project_id."";
    $newQuery5 = " AND {$tblpx}subcontractor_payment.project_id = ".$project_id."";
}
if($company_id != NULL){
    $company = Company::model()->findbyPk($company_id);
    $company_name = ' - '.$company->name;
}else{
    $company_name = "";
}
?>
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/tablestyles.css" />
<link rel="stylesheet" type="text/css" href="uploads/new_template/css/newstyle.css" />
<style>
    table {
        border-collapse: collapse;
        width: 100%;
    }
    /*.container{max-width: 1050px;margin:0 auto;padding: 15px;border: 1px solid #ddd;}*/
    .text-right{text-align:right}
    .details,.info-table{border: 1px solid #212121;margin-top: 20px;}
    .details td, .info-table td,.info-table th{padding: 6px 8px; }
    .info-table{margin-bottom: 10px;}
    .text-center{text-align:center;}
    .img-hold{width: 10%;}
    .companyhead{font-size: 18px;font-weight:bold;margin-top:0px;color: #789AD1;}
    .vertical_td{
        vertical-align: middle !important;
        text-align: left !important;
    }
</style>
<?php
    $company_address = Company::model()->findBypk(($company_id)?$company_id:Yii::app()->user->company_id);	
?>
    <table border=0>
        <tbody>
                <tr>
                    <td class=text-right>GST NO: <?php echo isset($company_address["company_gstnum"])?$company_address["company_gstnum"]:''; ?></td>
                </tr>
        </tbody>
    </table>
    <table border=0>
        <tbody>
            <tr>
                <td class="img-hold"><img  src="<?php echo Yii::app()->theme->baseUrl; ?>/images/logo.png" alt="img" style='margin-top:-45px;'/></td>
                <td class="text-center"><p class='companyhead'><?php echo isset($company_address['name'])?$company_address['name']:''; ?></p><br>
                    <p><?php echo ( (isset($company_address['address']) && !empty($company_address['address'])) ? ($company_address['address']) : ""); ?><br>
                    PIN : <?php echo ( (isset($company_address['pincode']) && !empty($company_address['pincode'])) ?($company_address['pincode']) : ""); ?>,&nbsp;
                    PHONE : <?php echo ( (isset($company_address['phone']) && !empty($company_address['phone'])) ?( $company_address['phone']) : ""); ?><br>
                    EMAIL : <?php echo ( (isset($company_address['email_id']) && !empty($company_address['email_id'])) ?( $company_address['email_id']) : ""); ?></p><br>                        
                    <h4>PAYMENT Summary of <b> <?php echo $subcontractor; ?> &nbsp; <?php echo $company_name; ?> </b></h4>   
                </td>
            </tr>
        </tbody>
    </table>
<div class="container-fluid">
            
            <div id="contenthtml" class="contentdiv">
            <br><br>
            <div class="container-fluid">

                    <div style="clear:both;width:100%;">
                        <div style="float:left;width:20%;border:1px solid #ddd;padding:5px;font-size:10px;height:50px;"><b>Project Name</b></div>
                        <div style="float:left;width:10%;border:1px solid #ddd;padding:5px;font-size:10px;height:50px;"><b>Date</b></div>
                        <div style="float:left;width:10%;border:1px solid #ddd;padding:5px;font-size:10px;height:50px;"><b>Description</b></div>
                        <div style="float:left;width:10%;border:1px solid #ddd;padding:5px;font-size:10px;height:50px;"><b>Quotation Amount</b></div>
                        <div style="float:left;width:15%;border:1px solid #ddd;padding:5px;font-size:10px;height:50px;"><b>Payment Date</b></div>
                        <div style="float:left;width:10%;border:1px solid #ddd;padding:5px;font-size:10px;height:50px;"><b>Payment</b></div>
                        <div style="float:left;width:10%;border:1px solid #ddd;padding:5px;font-size:10px;height:50px;"><b>Balance</b></div>
                    </div>
                    <?php
                          foreach($final_array as $key=> $value){
                            $j=0;
                            $a =0;
                            $blance = 0;
                            foreach($value['details'] as $key2 => $value2) {
                                if(isset($value2['quotation_amount'])){
                                    $quotation_amount = $value2['quotation_amount'];
                                }else{
                                    $quotation_amount = 0;
                                }
                                if(isset($value2['payment_amount'])){
                                    $payment_amount = $value2['payment_amount'];
                                }else{
                                    $payment_amount = 0;
                                }
                                
                                $blance += $quotation_amount-$payment_amount;
                                if ($a >= 5) {
                                    $blance = $blance+$quotation_amount;
                                } else {
                                    $blance = $blance;
                                }
                                $quotation_total+= $quotation_amount;
                                $payment_total += $payment_amount;
                                $balance_total += $blance;
                                
                          }
                        }
                    ?>
                    <div style="clear:both;width:100%;">
                        <div style="float:left;width:43.5%;border:1px solid #ddd;padding:5px;font-size:10px;height:50px;"><b>Total</b></div>
                       
                        <div style="float:left;width:10%;border:1px solid #ddd;padding:5px;font-size:10px;height:50px;"><b><?php echo ($quotation_total != '') ? Controller::money_format_inr($quotation_total, 2, 1) : '0.00'; ?></b></div>
                        <div style="float:left;width:15%;border:1px solid #ddd;padding:5px;font-size:10px;height:50px;"><b></b></div>
                        <div style="float:left;width:10%;border:1px solid #ddd;padding:5px;font-size:10px;height:50px;"><b><?php echo ($payment_total != '') ? Controller::money_format_inr($payment_total, 2, 1) : '0.00'; ?></b></div>
                        <div style="float:left;width:10%;border:1px solid #ddd;padding:5px;font-size:10px;height:50px;"><b></b></div>
                    </div>
                <?php
                $quotation_total = 0;
                $payment_total = 0;
                $balance_total = 0;
                foreach($final_array as $key=> $value){

            ?>
            <div  style="clear:both;display:flex;">
            <?php
                $j=0;
                $a =0;
                $blance = 0;
                foreach($value['details'] as $key2 => $value2) {
                    if(isset($value2['quotation_amount'])){
                        $quotation_amount = $value2['quotation_amount'];
                    }else{
                        $quotation_amount = 0;
                    }
                    if(isset($value2['payment_amount'])){
                        $payment_amount = $value2['payment_amount'];
                    }else{
                        $payment_amount = 0;
                    }
                    
                    $blance += $quotation_amount-$payment_amount;
                    if ($a >= 5) {
                        $blance = $blance+$quotation_amount;
                    } else {
                        $blance = $blance;
                    }
                    $quotation_total+= $quotation_amount;
                    $payment_total += $payment_amount;
                    $balance_total += $blance;
                    if($a ==0){
                        $style ="";
                    }else{
                        $style ="float:left;width:15%;";
                    }
            ?>
            <?php
            if($a ==0){
            ?>
            <div style="float:left;width:20%;border:1px solid #ddd;padding:5px;font-size:10px;height:50px;border-bottom:1px solid #ddd;border-top:1px solid #ddd;"><?php echo $value['project']; ?></div>
            <?php }else{ ?>
            <div style="float:left;width:20%;border-left:1px solid #ddd;padding:5px;font-size:10px;height:50px;border-bottom:1px solid #ddd;border-top:1px solid #ddd;"><span style="visibility:hidden;"><?php echo $value['project']; ?></span></div>
            <?php } ?>
            <div style ="float:left;width:10%;border:1px solid #ddd;padding:5px;font-size:10px;height:50px;"><?php echo isset($value2['quotation_amount'])?$key2:"&nbsp;"; ?></div>
            <div style ="float:left;width:10%;border:1px solid #ddd;padding:5px;font-size:10px;height:50px;"><?php echo isset($value2['description'])?$value2['description']:"&nbsp;"; ?></div>
            <div style ="float:left;width:10%;border:1px solid #ddd;padding:5px;font-size:10px;height:50px;"><?php echo isset($value2['quotation_amount'])?$value2['quotation_amount']:"&nbsp;"; ?></div>
            <div style ="float:left;width:15%;border:1px solid #ddd;padding:5px;font-size:10px;height:50px;"><?php echo isset($value2['payment_amount'])?$key2:"&nbsp;"; ?></div>
            <div style ="float:left;width:10%;border:1px solid #ddd;padding:5px;font-size:10px;height:50px;"><?php echo isset($value2['payment_amount'])?$value2['payment_amount']:"&nbsp;"; ?></div>
            <div style ="float:left;width:10%;border:1px solid #ddd;padding:5px;font-size:10px;height:50px;"><?php echo isset($value2['payment_amount'])?$blance:"&nbsp;"; ?></div>

            
            <?php $a++; } ?>
            </div>
            <?php 
              $j++;  } 
            ?>
        </div>


        <br><br>

        <h3>Advance Payment Details</h3>
            <div class="container-fluid">

                    <div style="clear:both;width:100%;">
                        <div style="float:left;width:20%;border:1px solid #ddd;padding:5px;font-size:10px;height:50px;"><b>Project Name</b></div>
                        <div style="float:left;width:10%;border:1px solid #ddd;padding:5px;font-size:10px;height:50px;"><b>Date</b></div>
                        <div style="float:left;width:10%;border:1px solid #ddd;padding:5px;font-size:10px;height:50px;"><b>Description</b></div>
                        <div style="float:left;width:10%;border:1px solid #ddd;padding:5px;font-size:10px;height:50px;"><b>Advance Amount</b></div>
                        <div style="float:left;width:15%;border:1px solid #ddd;padding:5px;font-size:10px;height:50px;"><b>Payment Date</b></div>
                        <div style="float:left;width:10%;border:1px solid #ddd;padding:5px;font-size:10px;height:50px;"><b>Payment</b></div>
                        <div style="float:left;width:10%;border:1px solid #ddd;padding:5px;font-size:10px;height:50px;"><b>Balance</b></div>
                    </div>
                    <?php
                          foreach($final_array2 as $key=> $value){
                            $j=0;
                            $a =0;
                            $blance = 0;
                            foreach($value['details'] as $key2 => $value2) {
                                if(isset($value2['advance_amount'])){
                                    $advance_amount = $value2['advance_amount'];
                                }else{
                                    $advance_amount = 0;
                                }
                                if(isset($value2['payment_amount'])){
                                    $payment_amount = $value2['payment_amount'];
                                }else{
                                    $payment_amount = 0;
                                }
                                
                                $blance += $advance_amount-$payment_amount;
                                if ($a >= 5) {
                                    $blance = $blance+$advance_amount;
                                } else {
                                    $blance = $blance;
                                }
                                $advance_total+= $advance_amount;
                                $payment_total2 += $payment_amount;
                                $balance_total2 += $blance;
                                
                          }
                        }
                    ?>
                    <div style="clear:both;width:100%;">
                        <div style="float:left;width:43.5%;border:1px solid #ddd;padding:5px;font-size:10px;height:50px;"><b>Total</b></div>
                       
                        <div style="float:left;width:10%;border:1px solid #ddd;padding:5px;font-size:10px;height:50px;"><b><?php echo ($advance_total != '') ? Controller::money_format_inr($advance_total, 2, 1) : '0.00'; ?></b></div>
                        <div style="float:left;width:15%;border:1px solid #ddd;padding:5px;font-size:10px;height:50px;"><b></b></div>
                        <div style="float:left;width:10%;border:1px solid #ddd;padding:5px;font-size:10px;height:50px;"><b><?php echo ($payment_total2 != '') ? Controller::money_format_inr($payment_total2, 2, 1) : '0.00'; ?></b></div>
                        <div style="float:left;width:10%;border:1px solid #ddd;padding:5px;font-size:10px;height:50px;"><b></b></div>
                    </div>
                <?php
                $advance_total = 0;
                $payment_total2 = 0;
                $balance_total2 = 0;
                foreach($final_array2 as $key=> $value){

            ?>
            <div  style="clear:both;display:flex;">
            <?php
                $j=0;
                $a =0;
                $blance = 0;
                foreach($value['details'] as $key2 => $value2) {
                    if(isset($value2['advance_amount'])){
                        $advance_amount = $value2['advance_amount'];
                    }else{
                        $advance_amount = 0;
                    }
                    if(isset($value2['payment_amount'])){
                        $payment_amount = $value2['payment_amount'];
                    }else{
                        $payment_amount = 0;
                    }
                    
                    $blance += $advance_amount-$payment_amount;
                    if ($a >= 5) {
                        $blance = $blance+$advance_amount;
                    } else {
                        $blance = $blance;
                    }
                    $advance_total+= $advance_amount;
                    $payment_total2 += $payment_amount;
                    $balance_total2 += $blance;
            ?>
            <?php
            if($a ==0){
            ?>
            <div style="float:left;width:20%;border:1px solid #ddd;padding:5px;font-size:10px;height:50px;border-bottom:1px solid #ddd;border-top:1px solid #ddd;"><?php echo $value['project']; ?></div>
            <?php }else{ ?>
            <div style="float:left;width:20%;border-left:1px solid #ddd;padding:5px;font-size:10px;height:50px;border-bottom:1px solid #ddd;border-top:1px solid #ddd;"><span style="visibility:hidden;"><?php echo $value['project']; ?></span></div>
            <?php } ?>
            <div style ="float:left;width:10%;border:1px solid #ddd;padding:5px;font-size:10px;height:50px;"><?php echo isset($value2['quotation_amount'])?$key2:"&nbsp;"; ?></div>
            <div style ="float:left;width:10%;border:1px solid #ddd;padding:5px;font-size:10px;height:50px;"><?php echo isset($value2['description'])?$value2['description']:"&nbsp;"; ?></div>
            <div style ="float:left;width:10%;border:1px solid #ddd;padding:5px;font-size:10px;height:50px;"><?php echo isset($value2['quotation_amount'])?$value2['quotation_amount']:"&nbsp;"; ?></div>
            <div style ="float:left;width:15%;border:1px solid #ddd;padding:5px;font-size:10px;height:50px;"><?php echo isset($value2['payment_amount'])?$key2:"&nbsp;"; ?></div>
            <div style ="float:left;width:10%;border:1px solid #ddd;padding:5px;font-size:10px;height:50px;"><?php echo isset($value2['payment_amount'])?$value2['payment_amount']:"&nbsp;"; ?></div>
            <div style ="float:left;width:10%;border:1px solid #ddd;padding:5px;font-size:10px;height:50px;"><?php echo isset($value2['payment_amount'])?$blance:"&nbsp;"; ?></div>

            
            <?php $a++; } ?>
            </div>
            <?php 
              $j++;  } 
            ?>
        </div>

       <div>
       <!-- <div style="float:left;width:20%;border-left:1px solid #ddd;border-bottom:1px solid #ddd;padding:5px;font-size:10px;height:40px;"></div> -->
       <!-- <div style="float:left;width:10%;border-bottom:1px solid #ddd;padding:5px;font-size:10px;height:40px;"></div>
            <div style ="float:left;width:10%;text-align:right;border-right:1px solid #ddd;border-bottom:1px solid #ddd;font-size:12px;height:40px;padding:5px"><b>Total: </b></div>
            <div style="float:left;width:10%;border:1px solid #ddd;padding:5px;font-size:10px;height:40px;"><b><?php echo ($advance_total != '') ? Controller::money_format_inr($advance_total, 2, 1) : '0.00'; ?></b></div>
            <div style="float:left;width:15%;border:1px solid #ddd;padding:5px;font-size:10px;height:40px;"></div>
            <div style="float:left;width:10%;border:1px solid #ddd;padding:5px;font-size:10px;height:40px;"><b><?php echo ($payment_total2 != '') ? Controller::money_format_inr($payment_total2, 2, 1) : '0.00'; ?></b></div>
            <div style="float:left;width:10%;border:1px solid #ddd;padding:5px;font-size:10px;height:40px;"></div>
        </div> -->

    
</div>

        </div>
