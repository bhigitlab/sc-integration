<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.js"></script>
<div class="page_filter clearfix">
    <?php $form = $this->beginWidget('CActiveForm', array(
        'action' => Yii::app()->createUrl($this->route),
        'method' => 'get',
    )); ?>
    <div class="filter_elem">
        <?php
        echo $form->dropDownList($model, 'voucher_for', array('1' => 'Subcontractor', '2' => 'Vendor'), array('class' => 'select_box', 'empty' => '-Select Voucher for-'));
        ?>
    </div>

    <div class="filter_elem">
        <?php echo $form->textField($model, 'voucher_date', array('class' => 'form-control', 'autocomplete' => 'off', 'placeholder' => 'Voucher Date')); ?>
    </div>

    <div class="filter_elem">
        <?php
        echo $form->dropDownList($model, 'company', CHtml::listData(Company::model()->findAll(array('condition' => 'id IN (' . $user_companies . ')')), 'id', 'name'), array('ajax' => array(
            'type' => 'POST',
            'url' => Yii::app()->createUrl('vouchers/dynamicproject'),
            'dataType' => 'JSON',
            'success' => 'js:function(data)'
                . '{'
                . '    $("#Vouchers_project").html(data.projects_list);'
                . '    $("#Vouchers_voucher_for_user_id").html(data.voucher_for_list);'
                . '}',
            'data' => array('company_id' => 'js:this.value', 'voucher_type' => 'js:$(\'input[name="Vouchers[voucher_for]"]:checked\').val()'),
        ), 'class' => 'select_box', 'empty' => '-Select Company-'));
        ?>
    </div>
    <div class="filter_elem">
        <?php
        echo $form->dropDownList($model, 'project', CHtml::listData(Projects::model()->findAll(array('condition' => 'company_id IN (' . $user_companies . ')')), 'pid', 'name'), array('ajax' => array(
            'type' => 'POST',
            'url' => Yii::app()->createAbsoluteURL('expenses/dynamicDropdown'),
            'dataType' => 'JSON',
            'success' => 'js:function(data)'
                . '{'
                . '    $("#Vouchers_expense_head").html(data.expenses);'
                . '}',
            'data' => array('project' => 'js:this.value', 'bill' => 0, 'expId' => 0)
        ), 'class' => 'select_box', 'empty' => '-Select Project-'));
        ?>
    </div>
    <div class="filter_elem">
        <?php echo $form->textField($model, 'voucher_number', array('class' => 'form-control', 'placeholder' => 'Voucher Number')); ?>
    </div>
    <div class="filter_elem filter_btns">
        <?php echo CHtml::submitButton('Go', array('class' => 'btn-search')); ?>
        <?php echo CHtml::resetButton('Clear', array('onclick' => 'javascript:location.href="' . $this->createUrl('vouchers/index') . '"')); ?>
    </div>
    <?php $this->endWidget(); ?>
</div>
<script>
    $(document).ready(function() {
        $(".select_box").select2();
        $("#Vouchers_voucher_date").datepicker({
            dateFormat: 'dd-mm-yy'
        });
    });
</script>