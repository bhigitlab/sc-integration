<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.min.js"></script>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/message/css/jquery.message.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/message/js/jquery.message.js"></script>
<style>
    .lefttdiv {
        float: left;
    }

    .purchase-title {
        border-bottom: 1px solid #ddd;
    }

    .purchase_items h3 {
        font-size: 18px;
        color: #333;
        margin: 0px;
        padding: 0px;
        text-align: left;
        padding-bottom: 10px;
    }

    .purchase_items {
        padding: 15px;
        box-shadow: 0 0 13px 1px rgba(0, 0, 0, 0.25);
    }

    .purchaseitem {
        display: inline-block;
        margin-right: 20px;
    }

    .purchaseitem last-child {
        margin-right: 0px;
    }

    .purchaseitem label {
        font-size: 12px;
    }

    .remark {
        display: none;
    }

    .padding-box {
        padding: 3px 0px;
        min-height: 17px;
        display: inline-block;
    }

    th {
        height: auto;
    }

    .quantity,
    .rate {
        max-width: 80px;
    }

    .text_align {

        text-align: center;
    }


    *:focus {
        border: 1px solid #333;
        box-shadow: 0 0 6px 1px rgba(0, 0, 0, 0.25);
    }

    .text-right {
        text-align: right;
    }
</style>
<script>
    $(function() {
        $("#datepicker").datepicker({
            dateFormat: 'dd-mm-yy'
        }).datepicker("setDate", new Date());
    });
</script>
<div class="container">
    <div class="invoicemaindiv">
        <!-- <a style="float:right;" id="download" class="pdfbtn1 pdf_excel">
            <div class="save_pdf"></div>SAVE AS PDF
        </a>
        <a style="float:right;;margin-right:2px;" id="download" class="excelbtn1 pdf_excel">
            <div class="save_pdf"></div>SAVE AS EXCEL
        </a> -->



        <h2 class="purchase-title">Sales Book</h2>
        <div class="row">
            <div class="col-sm-12">
                <?php
                foreach (Yii::app()->user->getFlashes() as $key => $message) {
                    echo '<div class="alert alert-success ' . $key . '">' . $message . "</div>\n";
                }
                ?>
            </div>
        </div>
        <form id="pdfvals1" method="post" action="">
            <input type="hidden" name="purchaseview">
            <div class="row item_view">
                <div class="col-md-3">
                    <label>COMPANY : </label>
                    <?php
                    $company = Company::model()->findByPk($model->company_id);
                    echo $company['name'];
                    ?>
                </div>
                <div class="col-md-3">
                    <label>Buyer : </label>
                    <?php echo isset($model->buyer_id) ? $model->buyer->name : ''; ?>
                </div>
                <div class="col-md-3">
                    <label>PROJECT : </label>
                    <?php echo isset($model->project_id) ? $model->project->name : ''; ?>
                </div>
                <div class="col-md-3">
                    <label>Flat No : </label>
                    <?php echo isset($model->flat_id) ? $model->flat->flat_number : ''; ?>
                </div>

                <div class="col-md-3">
                    <label>DATE : </label>
                    <?php echo isset($model->date) ? date('d-m-Y', strtotime($model->date)) : ''; ?>
                </div>
                <div class="col-md-3">
                    <label>INVOICE No : </label>
                    <?php echo isset($model->invoice_no) ? $model->invoice_no : ''; ?>
                </div>
            </div>

            <div class="clearfix"></div>

            <div id="table-scroll" class="table-scroll">
                <div id="faux-table" class="faux-table" aria="hidden"></div>
                <div id="table-wrap" class="table-wrap">
                    <div class="table-responsive">
                        <table border="1" class="table">
                            <thead>
                                <tr>
                                    <th>Sl.No</th>
                                    <th>Description</th>
                                    <th>Transaction Type</th>
                                    <th>SGST (%)</th>
                                    <th>SGST Amount</th>
                                    <th>CGST (%)</th>
                                    <th>CGST Amount</th>
                                    <th>IGST (%)</th>
                                    <th>IGST Amount</th>
                                    <th>Amount</th>
                                    <th>Tax Amount</th>
                                </tr>
                            </thead>
                            <tbody class="addrow">
                                <?php
                                if (!empty($itemmodel)) {
                                    foreach ($itemmodel as $i => $value) {
                                        if ($value->transaction_type == 1) {
                                            $type = 'Sales';
                                        } else {
                                            $type = "Journal";
                                        }
                                ?>

                                        <tr class="tr_class">
                                            <td><input type="hidden" name="sl_No[]" value="<?php echo $i + 1; ?>"> <?php echo $i + 1; ?></td>
                                            <td><input type="hidden" name="description[]" value=""><?php echo $value->description; ?> </td>
                                            <td><input type="hidden" name="type[]" value=""><?php echo $type; ?> </td>
                                            <td class="text-right"><?php echo number_format($value->sgst_p, 2); ?></td>
                                            <td class="text-right"><?php echo Controller::money_format_inr($value->sgst_val, 2, 1); ?></td>
                                            <td class="text-right"><?php echo number_format($value->cgst_p, 2); ?></td>
                                            <td class="text-right"><?php echo Controller::money_format_inr($value->cgst_val, 2, 1); ?></td>
                                            <td class="text-right"><?php echo number_format($value->igst_p, 2); ?></td>
                                            <td class="text-right"><?php echo Controller::money_format_inr($value->igst_val, 2, 1); ?></td>
                                            <td class="text-right"><input type="hidden" name="amount[]" value="<?php echo $value->amount; ?>" readonly><?php echo Controller::money_format_inr($value->amount, 2, 1); ?></td>
                                            <td class="text-right"><input type="hidden" name="amount[]" value="<?php echo $value->amount; ?>" readonly><?php echo Controller::money_format_inr($value->tax_total, 2, 1); ?></td>
                                        </tr>
                                <?php
                                    }
                                }
                                ?>
                            </tbody>
                            <tfoot>
                                <?php
                                $colspan1 = 9;
                                $colspan2 = 11;

                                ?>
                                <tr>
                                    <th style="text-align: right;" colspan="<?php echo $colspan1; ?>" class="text-right">TOTAL: </th>
                                    <th class="text-right">
                                        <div id="grand_total"><?php echo number_format($model->total_amount, 2); ?></div>
                                    </th>
                                    <th class="text-right">
                                        <div id="grand_total"><?php echo number_format($model->tax_amount, 2); ?></div>
                                    </th>
                                </tr>
                                <tr>
                                    <th colspan="<?php echo $colspan2 - 1; ?>" class="text-right">GRAND TOTAL:</th>
                                    <th class="text-right">
                                        <div id="net_amount"><?php echo number_format($model->tax_amount + $model->total_amount, 2, '.', '') ?></div>
                                    </th>
                                </tr>

                                <tr>
                                    <th colspan="<?php echo $colspan2 - 1; ?>" class="text-right">BALANCE:</th>
                                    <th class="text-right">
                                        <div id="net_amount"><?php echo number_format(($model->tax_amount + $model->total_amount), 2, '.', '') ?></div>
                                    </th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
            <div style="padding-right: 0px;" class="col-md-offset-9 col-md-3 col-sm-offset-8 col-sm-4 col-xs-offset-6 col-xs-6"></div>
        </form>
    </div>
    <style>
        .error_message {
            color: red;
        }

        a.pdf_excel {
            background-color: #6a8ec7;
            display: inline-block;
            padding: 8px;
            color: #fff;
            border: 1px solid #6a8ec8;
        }

        .invoicemaindiv th,
        .invoicemaindiv td {
            padding: 10px;
        }
    </style>
    <script>
        $(function() {
            $('.pdfbtn1').click(function() {
                $("form#pdfvals1").attr('action', "<?php echo $this->createAbsoluteUrl('invoice/SaveInvoice', array('invid' => $invoice_id)); ?>");
                $("form#pdfvals1").submit();
            });
            $('.excelbtn1').click(function() {
                $("form#pdfvals1").attr('action', "<?php echo $this->createAbsoluteUrl('invoice/ExportInvoice', array('invid' => $invoice_id)); ?>");
                $("form#pdfvals1").submit();
            });

        });
    </script>
</div>
<style>
    .table-scroll {
        position: relative;
        max-width: 1280px;
        width: 100%;
        margin: auto;
        display: table;
    }

    .table-wrap {
        width: 100%;
        display: block;
        overflow: auto;
        position: relative;
        z-index: 1;
        border: 1px solid #ddd;
    }

    .table-wrap.fixedON,
    .table-wrap.fixedON table,
    .faux-table table {
        height: 380px;
        /* match heights*/
    }

    .table-scroll table {
        width: 100%;
        margin: auto;
        border-collapse: separate;
        border-spacing: 0;
        border: 1px solid #ddd;
    }

    .table-scroll th,
    .table-scroll td {
        padding: 5px 10px;
        border: 1px solid #ddd;
        background: #fff;
        vertical-align: top;
    }

    .faux-table table {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        pointer-events: none;
    }

    .faux-table table tbody {
        visibility: hidden;
    }

    /* shrink cells in cloned table so that the table height is exactly 300px so that the header and footer appear fixed */
    .faux-table table tbody th,
    .faux-table table tbody td {
        padding-top: 0;
        padding-bottom: 0;
        border-top: none;
        border-bottom: none;
        line-height: 0.1;
    }

    .faux-table table tbody tr+tr th,
    .faux-table tbody tr+tr td {
        line-height: 0;
    }

    .faux-table thead th,
    .faux-table tfoot th,
    .faux-table tfoot td,
    .table-wrap thead th,
    .table-wrap tfoot th,
    .table-wrap tfoot td {
        background: #eee;
    }

    .faux-table {
        position: absolute;
        top: 0;
        right: 0;
        left: 0;
        bottom: 0;
        overflow-y: scroll;
    }

    .faux-table thead,
    .faux-table tfoot,
    .faux-table thead th,
    .faux-table tfoot th,
    .faux-table tfoot td {
        position: relative;
        z-index: 2;
    }

    /* ie bug */
    .table-scroll table thead tr,
    .table-scroll table thead tr th,
    .table-scroll table tfoot tr,
    .table-scroll table tfoot tr th,
    .table-scroll table tfoot tr td {
        height: 1px;
    }
</style>
<script>
    (function() {
        var mainTable = document.getElementById("main-table");
        if (mainTable !== null) {
            var tableHeight = mainTable.offsetHeight;
            if (tableHeight > 380) {
                var fauxTable = document.getElementById("faux-table");
                document.getElementById("table-wrap").className += ' ' + 'fixedON';
                var clonedElement = mainTable.cloneNode(true);
                clonedElement.id = "";
                fauxTable.appendChild(clonedElement);
            }
        }
    })();
</script>