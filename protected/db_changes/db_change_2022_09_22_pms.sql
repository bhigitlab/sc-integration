RENAME TABLE `pms_department` TO `jp_department`;
RENAME TABLE `pms_assignpro_work_type` TO `jp_assignpro_work_type`;
RENAME TABLE `pms_work_type` TO `jp_worktype`;


SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


DELIMITER $$

CREATE  PROCEDURE `afterdayworkprogress` (IN `taskid` INT(11), IN `assigned_to` INT(11))  NO SQL INSERT INTO 
`pms_time_entry`(`user_id`, `tskid`, `work_type`, `entry_date`,`billable`, 
`description`, `created_date`, `created_by`, `updated_date`, `updated_by`, `completed_percent`) 
SELECT assigned_to, d1.taskid, d1.work_type, d1.date, "3", d1.description, 
d1.created_date, d1.created_by, d1.created_date, d1.created_by, 
SUM(d2.daily_work_progress) 
FROM `pms_daily_work_progress` as d1 
INNER JOIN 
(SELECT * from `pms_daily_work_progress` WHERE `taskid` = taskid) as d2 
ON d1.taskid = d2.taskid WHERE d1.`taskid` = taskid AND d1.Id=(SELECT LAST_INSERT_ID())$$

DELIMITER ;



CREATE TABLE `pms_accesslog` (
  `logid` double NOT NULL,
  `sqllogid` int(11) NOT NULL,
  `empid` varchar(11) NOT NULL,
  `log_time` datetime NOT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `device_id` int(4) NOT NULL DEFAULT 0,
  `deviceid` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `pms_accesslog_last_sync` (
  `id` int(11) NOT NULL,
  `last_sync_time` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


CREATE TABLE `pms_account_permission` (
  `id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `pms_acc_project_mapping` (
  `id` int(11) NOT NULL,
  `pms_project_id` int(11) NOT NULL,
  `acc_project_id` int(11) NOT NULL,
  `created_from` int(11) NOT NULL COMMENT '0->pms;1->acc',
  `created_by` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `pms_acc_wpr_item_consumed` (
  `id` int(11) NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `item_rate` float NOT NULL,
  `item_qty` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `item_added_by` int(11) NOT NULL,
  `item_approved_by` int(11) DEFAULT NULL,
  `wpr_id` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_date` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `pms_area` (
  `id` int(11) NOT NULL,
  `area_title` varchar(100) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `project_id` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


CREATE TABLE `pms_attendance` (
  `att_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `att_date` date DEFAULT NULL,
  `comments` text NOT NULL,
  `att_entry` int(11) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime NOT NULL,
  `modified_date` timestamp NULL DEFAULT current_timestamp(),
  `type` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DELIMITER $$
CREATE TRIGGER `before_delete_attendance_delete_log` BEFORE DELETE ON `pms_attendance` FOR EACH ROW BEGIN INSERT INTO pms_attendance_delete_log (`att_id`, `user_id`, `att_date`, `comments`, `att_entry`, `created_by`, `created_date`, `modified_date`, `type`) select * from pms_attendance where att_id=old.`att_id`; END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `insert_time_ot_attendance` AFTER INSERT ON `pms_attendance` FOR EACH ROW BEGIN

   REPLACE INTO pms_time_attendance (user_id, att_date, comments, att_entry, att_time, created_by, created_date, modified_date, type) VALUES
    (NEW.user_id , NEW.att_date, NEW.comments,NEW.att_entry,0,NEW.created_by,NEW.created_date,NEW.modified_date,0);




END
$$
DELIMITER ;


CREATE TABLE `pms_attendance_delete_log` (
  `delete_log_id` int(11) NOT NULL,
  `att_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `att_date` date DEFAULT NULL,
  `comments` text NOT NULL,
  `att_entry` int(11) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime NOT NULL,
  `modified_date` timestamp NULL DEFAULT current_timestamp(),
  `type` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `pms_attendance_log` (
  `log_id` int(11) NOT NULL,
  `refer_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `att_date` date DEFAULT NULL,
  `comments` text NOT NULL,
  `att_entry` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `pms_attendance_reject_log` (
  `att_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `att_date` date DEFAULT NULL,
  `comments` text NOT NULL,
  `att_entry` int(11) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime NOT NULL,
  `modified_date` timestamp NULL DEFAULT current_timestamp(),
  `type` tinyint(1) DEFAULT NULL,
  `pending_status` int(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `pms_attendance_requests` (
  `att_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `att_date` date DEFAULT NULL,
  `comments` text NOT NULL,
  `att_entry` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `type` tinyint(1) DEFAULT 2,
  `status` tinyint(4) DEFAULT 0 COMMENT '0-pending,1-approved,2-rejected',
  `decision_by` int(11) DEFAULT NULL,
  `decision_date` date DEFAULT NULL,
  `reason` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `pms_attendance_settings` (
  `settings_id` int(11) NOT NULL,
  `att_day` int(11) DEFAULT NULL,
  `att_month` int(11) DEFAULT NULL COMMENT '''0''-current mnth,1-next month',
  `till_day` int(11) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `pms_avail_compoff` (
  `id` int(11) NOT NULL,
  `userid` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `comment` text DEFAULT NULL,
  `type` enum('1','0.5') DEFAULT NULL COMMENT '1-full day, 0.5 - Half day',
  `status` enum('1','2','3','') DEFAULT '1' COMMENT '1-pending,2-approve,3-rejected',
  `decision_coment` text DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` date DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` date DEFAULT NULL,
  `utilised_status` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `pms_avail_leave` (
  `avail_leave_id` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `actual_el` int(11) DEFAULT NULL,
  `consi_leave` float NOT NULL DEFAULT 0,
  `type` varchar(10) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` date DEFAULT NULL,
  `updated_date` date DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `comment` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `pms_balance_leave` (
  `avail_leave_id` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `cl_ml` float NOT NULL DEFAULT 0,
  `earned` float NOT NULL DEFAULT 0,
  `lieu` float NOT NULL DEFAULT 0,
  `rh` float NOT NULL DEFAULT 0,
  `ml` float DEFAULT NULL COMMENT 'medical leave',
  `comment` varchar(100) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified_date` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `pms_biddings` (
  `bid` int(11) NOT NULL,
  `url` varchar(500) NOT NULL,
  `profile` int(11) NOT NULL,
  `worktype` smallint(6) NOT NULL,
  `budget_rate` double NOT NULL,
  `position` smallint(6) NOT NULL,
  `bid_by` int(11) NOT NULL,
  `bidding_date` datetime NOT NULL,
  `job_code` varchar(15) DEFAULT NULL,
  `status` smallint(6) NOT NULL,
  `remarks` mediumtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `pms_calender_events` (
  `id` int(11) NOT NULL,
  `title_id` int(11) DEFAULT NULL,
  `custom_title` varchar(100) NOT NULL,
  `event_date` date NOT NULL,
  `site_id` int(11) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` date NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `status` int(11) NOT NULL COMMENT '0->master,1->enabled,2=>deleted,3=>disabled'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `pms_clients` (
  `cid` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `nick_name` varchar(100) DEFAULT NULL,
  `project_type` smallint(6) NOT NULL,
  `description` text DEFAULT NULL,
  `status` smallint(6) NOT NULL,
  `created_date` date NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `pms_clientsite` (
  `id` int(11) NOT NULL,
  `site_name` varchar(150) NOT NULL,
  `pid` int(11) DEFAULT NULL,
  `latitude` varchar(50) DEFAULT NULL,
  `longitude` varchar(50) DEFAULT NULL,
  `distance` float(10,2) DEFAULT NULL,
  `account_warehouse_id` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` date DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `pms_clientsite_assigned` (
  `site_id` int(11) NOT NULL DEFAULT 0,
  `user_id` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `pms_comment` (
  `id` int(11) NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `create_time` int(11) DEFAULT NULL,
  `author` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `post_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE `pms_company` (
  `company_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `active_status` smallint(6) DEFAULT NULL,
  `descrp` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `pms_contractors` (
  `id` int(11) NOT NULL,
  `contractor_title` varchar(100) NOT NULL,
  `email_id` varchar(100) DEFAULT NULL,
  `status` int(11) NOT NULL COMMENT '1=>Enable,2=>disable	',
  `description` text DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` date NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `pms_country` (
  `Code` char(3) NOT NULL DEFAULT '',
  `Name` char(52) NOT NULL DEFAULT '',
  `Continent` enum('Asia','Europe','North America','Africa','Oceania','Antarctica','South America') NOT NULL DEFAULT 'Asia',
  `Region` char(26) NOT NULL DEFAULT '',
  `SurfaceArea` float(10,2) NOT NULL DEFAULT 0.00,
  `IndepYear` smallint(6) DEFAULT NULL,
  `Population` int(11) NOT NULL DEFAULT 0,
  `LifeExpectancy` float(3,1) DEFAULT NULL,
  `GNP` float(10,2) DEFAULT NULL,
  `GNPOld` float(10,2) DEFAULT NULL,
  `LocalName` char(45) NOT NULL DEFAULT '',
  `GovernmentForm` char(45) NOT NULL DEFAULT '',
  `HeadOfState` char(60) DEFAULT NULL,
  `Capital` int(11) DEFAULT NULL,
  `Code2` char(2) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `pms_cron` (
  `id` int(11) NOT NULL,
  `date` datetime DEFAULT NULL,
  `type` int(11) DEFAULT NULL COMMENT '1-update,0-insert',
  `function` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `pms_current_task_status` (
  `ctsid` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `taskid` int(11) NOT NULL,
  `start_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `active_status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `pms_custompunch` (
  `customid` int(11) NOT NULL,
  `customdate` date DEFAULT NULL,
  `customtime` time DEFAULT NULL,
  `resource` int(11) DEFAULT NULL,
  `device` int(11) DEFAULT NULL,
  `remark` varchar(200) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `modifieddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `pms_daily_overtime` (
  `id` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `date` date NOT NULL,
  `manual_ot` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `pms_daily_sevice_entries` (
  `entry_id` int(11) NOT NULL,
  `project_id` int(11) DEFAULT NULL,
  `date` date NOT NULL,
  `description` text NOT NULL,
  `created_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `pms_daily_work` (
  `id` int(11) NOT NULL,
  `project_id` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `taskid` int(11) DEFAULT NULL,
  `qty` float(8,2) DEFAULT NULL,
  `skilled_workers` int(11) DEFAULT NULL,
  `unskilled_workers` int(11) DEFAULT NULL,
  `date` date NOT NULL,
  `description` text NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `work_type` smallint(6) DEFAULT NULL,
  `assigned_to` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `pms_daily_work_progress` (
  `id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `taskid` int(11) DEFAULT NULL,
  `work_type` smallint(6) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `qty` float(8,2) DEFAULT NULL,
  `no_of_workers` int(11) DEFAULT NULL,
  `date` date NOT NULL,
  `description` text NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `daily_work_progress` float DEFAULT NULL,
  `approve_status` int(11) NOT NULL DEFAULT 0 COMMENT '0=>not approved,1=>approved,2=>rejected',
  `approve_reject_data` text DEFAULT NULL,
  `img_paths` text DEFAULT NULL,
  `image_label` varchar(100) DEFAULT NULL,
  `current_status` smallint(6) DEFAULT NULL,
  `created_date` datetime NOT NULL,
  `approved_date` datetime DEFAULT NULL,
  `latitude` varchar(100) DEFAULT NULL,
  `longitude` varchar(100) DEFAULT NULL,
  `site_id` int(10) DEFAULT NULL,
  `site_name` varchar(500) DEFAULT NULL,
  `consumed_item_id` int(11) DEFAULT NULL,
  `consumed_item_count` int(11) DEFAULT NULL,
  `consumed_item_status` int(11) DEFAULT NULL,
  `consumed_item_price_id` int(11) DEFAULT NULL,
  `template_item_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `pms_department` (
  `department_id` int(11) NOT NULL,
  `department_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `pms_device_accessids` (
  `id` int(11) NOT NULL,
  `userid` int(11) DEFAULT NULL,
  `deviceid` int(11) DEFAULT NULL,
  `accesscard_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `pms_earned_leave` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `action_by` int(11) DEFAULT NULL,
  `created_date` date DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` date DEFAULT NULL,
  `el_count` tinyint(1) NOT NULL,
  `leave_type` int(11) DEFAULT 10,
  `utilised_status` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `pms_electricity` (
  `eid` int(11) NOT NULL,
  `reading` bigint(20) NOT NULL,
  `reading_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `pms_employeerequest` (
  `id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `request_date` date NOT NULL,
  `message` text NOT NULL,
  `mail_send_status` int(11) DEFAULT NULL,
  `acknoledge_status` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` date DEFAULT NULL,
  `ack_message` varchar(500) DEFAULT NULL,
  `ack_by` int(11) DEFAULT NULL,
  `ack_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `pms_employee_default_data` (
  `default_data_id` int(11) NOT NULL,
  `caption` varchar(30) NOT NULL,
  `label` varchar(40) NOT NULL,
  `data_type` varchar(25) NOT NULL,
  `custome_value` varchar(5) DEFAULT NULL,
  `parent` int(11) DEFAULT NULL,
  `details` varchar(100) NOT NULL,
  `status` smallint(6) NOT NULL,
  `type` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `pms_employee_group` (
  `group_id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `group_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `pms_employee_shift` (
  `shift_id` int(11) NOT NULL,
  `shift_name` varchar(40) NOT NULL,
  `short_name` varchar(20) DEFAULT NULL,
  `shift_starts` varchar(11) DEFAULT NULL,
  `shift_ends` varchar(11) DEFAULT NULL,
  `grace_period` float DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `color_code` varchar(50) DEFAULT NULL,
  `type` tinyint(1) DEFAULT NULL,
  `grace_period_before` float DEFAULT NULL,
  `grace_period_after` float DEFAULT NULL,
  `pt_p` float DEFAULT NULL,
  `pt_ul` float DEFAULT NULL,
  `pt_uhl` float DEFAULT NULL,
  `pt_hp` float DEFAULT NULL,
  `is_continuous_shift` int(11) NOT NULL DEFAULT 0,
  `min_ot_value` decimal(10,0) NOT NULL,
  `max_ot_value` decimal(10,0) NOT NULL,
  `fIxed_ot` int(11) NOT NULL,
  `full_ot` int(11) NOT NULL DEFAULT 0,
  `no_ot` tinyint(4) DEFAULT NULL,
  `twoP` int(10) DEFAULT NULL,
  `sunday_enable` int(10) DEFAULT NULL,
  `cs_ot_description` text NOT NULL,
  `continuous_shift_end` varchar(11) DEFAULT NULL,
  `ot_offset` int(11) DEFAULT NULL,
  `apply_ot_offset` tinyint(4) DEFAULT NULL,
  `min_ot_intime` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `pms_general_settings` (
  `id` int(11) NOT NULL,
  `from_name` varchar(50) NOT NULL,
  `from_email` varchar(50) NOT NULL,
  `smtp_host` varchar(50) NOT NULL,
  `smtp_auth` varchar(50) NOT NULL,
  `smtp_username` varchar(50) NOT NULL,
  `smtp_password` varchar(50) NOT NULL,
  `smtp_secure` varchar(50) NOT NULL,
  `smtp_email_from` varchar(50) NOT NULL,
  `smtp_mailfrom_name` varchar(50) NOT NULL,
  `smtp_port` varchar(20) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `created_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `behind_task_mail_recipients` varchar(100) DEFAULT NULL,
  `dropbox_token` varchar(100) NOT NULL,
  `slider_images` varchar(100) NOT NULL,
  `email_notification_page_size_home` int(11) DEFAULT NULL,
  `escalated_tasks_size_home` int(11) DEFAULT NULL,
  `backlog_days` varchar(50) NOT NULL DEFAULT '2',
  `experience_year` float DEFAULT NULL,
  `experience_title` varchar(250) DEFAULT NULL,
  `project_count` float DEFAULT NULL,
  `project_title` varchar(250) DEFAULT NULL,
  `customers_count` float DEFAULT NULL,
  `customers_title` varchar(250) DEFAULT NULL,
  `engineers_count` float DEFAULT NULL,
  `engineers_title` varchar(250) DEFAULT NULL,
  `ongoing_project_count` float DEFAULT NULL,
  `ongoing_project_title` varchar(250) DEFAULT NULL,
  `mail_template_punches` int(10) DEFAULT NULL,
  `dashboard_status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `pms_groups` (
  `group_id` int(11) NOT NULL,
  `group_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `group_lead` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(150) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `created_by` int(10) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_by` int(10) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `pms_groups_members` (
  `id` int(11) NOT NULL,
  `group_id` int(10) DEFAULT NULL,
  `group_members` int(10) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_by` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `pms_group_leaders` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `pms_holidays` (
  `id` int(11) NOT NULL,
  `title` varchar(200) NOT NULL,
  `holiday_date` date DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` date NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `flag` int(10) DEFAULT NULL,
  `type` int(10) DEFAULT NULL,
  `status` int(10) DEFAULT NULL COMMENT '0-pending 1-approved- 2-rejected'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `pms_ignore_punches` (
  `ig_id` int(11) NOT NULL,
  `empid` varchar(50) DEFAULT NULL,
  `device_id` int(4) DEFAULT 0,
  `acces_logid` int(11) DEFAULT NULL,
  `userid` int(11) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `comment` text DEFAULT NULL,
  `status` tinyint(4) DEFAULT 0,
  `decision_by` int(11) DEFAULT NULL,
  `decision_date` date DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `pms_invoice` (
  `id` int(11) NOT NULL,
  `invoice_id` int(11) NOT NULL,
  `invoice_title` varchar(255) NOT NULL,
  `invoice_no` varchar(25) NOT NULL,
  `client_id` int(11) DEFAULT NULL,
  `tot_entries` int(11) NOT NULL,
  `status` smallint(6) NOT NULL,
  `invoice_date` date NOT NULL,
  `generated_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `pms_invoiced_entry` (
  `id` int(11) NOT NULL,
  `invoice_id` int(11) NOT NULL,
  `teid` int(11) NOT NULL,
  `rate` float(10,2) NOT NULL,
  `work_type` smallint(6) NOT NULL,
  `hours` float(10,2) NOT NULL,
  `new_desc` text DEFAULT NULL,
  `generated_date` datetime NOT NULL,
  `generated_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `pms_issues` (
  `id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `status` smallint(6) NOT NULL,
  `issue` text NOT NULL,
  `updated_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `created_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `pms_items` (
  `id` int(11) NOT NULL,
  `sl_no` varchar(50) NOT NULL,
  `projectid` int(11) DEFAULT NULL,
  `taskid` int(11) DEFAULT NULL,
  `description` varchar(250) DEFAULT NULL,
  `qty` float(8,2) DEFAULT NULL,
  `unit` varchar(20) DEFAULT NULL,
  `rate` float(8,2) DEFAULT NULL,
  `amount` varchar(20) DEFAULT NULL,
  `parentid` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `pms_lead` (
  `lead_id` int(11) NOT NULL,
  `lead_date` date DEFAULT NULL,
  `lead_title` smallint(6) DEFAULT NULL,
  `contact` varchar(50) NOT NULL,
  `designation` varchar(50) NOT NULL,
  `company` varchar(80) NOT NULL,
  `group_company_name` varchar(50) DEFAULT NULL,
  `website` varchar(80) DEFAULT NULL,
  `source` varchar(50) NOT NULL,
  `address` varchar(150) NOT NULL,
  `city` varchar(50) NOT NULL,
  `state` varchar(50) NOT NULL,
  `country` char(3) NOT NULL,
  `zip_code` varchar(8) DEFAULT NULL,
  `email` varchar(50) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `mobile` varchar(15) DEFAULT NULL,
  `default_contact_no` tinyint(4) NOT NULL,
  `fax` varchar(15) DEFAULT NULL,
  `priority` smallint(6) NOT NULL,
  `status` smallint(6) NOT NULL,
  `industry` smallint(6) NOT NULL,
  `opportunity_name` varchar(80) DEFAULT NULL,
  `opportunity_pobability` tinyint(4) DEFAULT NULL,
  `bhi_probability` tinyint(4) DEFAULT NULL,
  `opportunity_type` smallint(6) DEFAULT NULL,
  `opportunity` int(11) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_by` int(11) NOT NULL,
  `updated_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `pms_lead_activity` (
  `activity_id` int(11) NOT NULL,
  `lead_id` int(11) NOT NULL,
  `contacted_person` int(11) DEFAULT NULL,
  `activity_type` smallint(6) NOT NULL,
  `date` date NOT NULL,
  `person` int(11) NOT NULL,
  `purpose` varchar(80) NOT NULL,
  `duration` varchar(10) DEFAULT NULL,
  `summary` mediumtext NOT NULL,
  `followup_action` varchar(30) DEFAULT NULL,
  `followup` smallint(6) NOT NULL,
  `followupdate` date DEFAULT NULL,
  `sales_stage` tinyint(4) NOT NULL,
  `assign_followup` int(11) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_by` int(11) NOT NULL,
  `updated_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `pms_lead_contact` (
  `cid` int(11) NOT NULL,
  `lead_id` int(11) NOT NULL,
  `default_contact` tinyint(1) NOT NULL,
  `title` smallint(6) NOT NULL,
  `contact_name` varchar(25) NOT NULL,
  `designation` varchar(40) NOT NULL,
  `email_id` text DEFAULT NULL,
  `phone1` varchar(15) DEFAULT NULL,
  `phone2` varchar(150) NOT NULL,
  `default_contact_no` tinyint(1) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `pms_leave` (
  `leave_id` int(11) NOT NULL,
  `emp_id` int(11) NOT NULL,
  `leave_submit_date` datetime NOT NULL,
  `leave_type` int(11) DEFAULT NULL,
  `date_from` date NOT NULL,
  `date_to` date NOT NULL,
  `reason_for_leave` varchar(100) NOT NULL,
  `contact_details` varchar(25) DEFAULT NULL,
  `document_if_any` varchar(50) DEFAULT NULL,
  `person_covering_role` varchar(50) DEFAULT NULL,
  `reporting_person` int(11) DEFAULT NULL,
  `reporting_person_status` tinyint(1) DEFAULT NULL,
  `refer_to_admin` int(11) DEFAULT 0,
  `refer_to_hr` int(11) DEFAULT NULL,
  `approval_status` int(11) DEFAULT 11,
  `decision_by` int(11) DEFAULT NULL,
  `decision_date` date DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` date NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `pms_leave_credit` (
  `id` int(11) NOT NULL,
  `clml` tinyint(4) DEFAULT NULL,
  `el` tinyint(4) DEFAULT NULL,
  `enable` tinyint(1) DEFAULT 0,
  `created_by` int(11) DEFAULT NULL,
  `created_date` date DEFAULT NULL,
  `last_updated` datetime NOT NULL,
  `leave_days` int(11) DEFAULT NULL,
  `comp_days` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `pms_leave_cron` (
  `cron_id` int(10) NOT NULL,
  `date` datetime DEFAULT NULL,
  `status` int(10) DEFAULT NULL,
  `comment` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `pms_leave_day` (
  `leave_day_id` int(11) NOT NULL,
  `leave_id` int(11) NOT NULL,
  `leave_date` date NOT NULL,
  `leave_period` int(11) NOT NULL,
  `leave_value` float NOT NULL,
  `comp_date` date DEFAULT NULL,
  `leave_revert_id` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `pms_leave_log` (
  `id` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `cl_credited` float NOT NULL,
  `er_credited` float NOT NULL,
  `rh_credited` float NOT NULL DEFAULT 0,
  `co_credited` float DEFAULT NULL,
  `ml_credited` float DEFAULT NULL,
  `pl_creadeited` float(10,2) DEFAULT NULL,
  `rh_cu` float NOT NULL DEFAULT 0,
  `cu_cl` float NOT NULL,
  `cu_er` float NOT NULL,
  `cu_co` float DEFAULT NULL,
  `cu_ml` float DEFAULT NULL,
  `cu_pl` float(10,2) DEFAULT NULL,
  `tot_lop` float DEFAULT NULL,
  `created_date` datetime NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `comment` varchar(100) NOT NULL,
  `leave_id` int(11) DEFAULT NULL,
  `comp_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DELIMITER $$
CREATE TRIGGER `update_balance_leave` AFTER INSERT ON `pms_leave_log` FOR EACH ROW BEGIN


IF (SELECT userid FROM `pms_balance_leave` WHERE `userid`=new.userid) THEN update pms_balance_leave set cl_ml = cl_ml+(new.cl_credited), earned =earned+(new.er_credited) ,rh=rh+(new.rh_credited),ml=ml+(new.ml_credited),comment=new.comment where userid = new.userid;
ELSE 
INSERT INTO `pms_balance_leave`(`avail_leave_id`, `userid`, `cl_ml`, `earned`, `lieu`, `rh`, `ml`, `comment`, `modified_by`, `modified_date`) VALUES (NULL,new.userid,new.cl_credited,new.er_credited,0,new.rh_credited,new.ml_credited,new.comment,2,now() );

END IF;

END
$$
DELIMITER ;



CREATE TABLE `pms_leave_rules` (
  `rule_id` int(11) NOT NULL,
  `designation_id` int(11) NOT NULL,
  `working_satdays` varchar(50) DEFAULT NULL,
  `sandwich_leave` tinyint(1) NOT NULL COMMENT '1=>yes,0=>no',
  `sunday_leave` varchar(50) DEFAULT NULL,
  `continue_leave` tinyint(1) NOT NULL COMMENT '1=>yes,0=>no'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `pms_leave_setting` (
  `leave_setting_id` int(11) NOT NULL,
  `caption` varchar(30) NOT NULL,
  `setting_type` varchar(15) NOT NULL,
  `setting_description` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `pms_leave_types` (
  `id` int(11) NOT NULL,
  `leave_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `pms_legends` (
  `leg_id` int(11) NOT NULL,
  `short_note` varchar(10) NOT NULL,
  `description` varchar(255) NOT NULL,
  `color_code` varchar(100) NOT NULL,
  `text_color` varchar(10) NOT NULL,
  `class_note` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `pms_lookup` (
  `id` int(11) NOT NULL,
  `name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `code` int(11) NOT NULL,
  `type` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `position` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE `pms_mail_log` (
  `log_id` int(11) NOT NULL,
  `send_to` longtext DEFAULT NULL,
  `send_by` longtext DEFAULT NULL,
  `send_date` datetime DEFAULT NULL,
  `description` text DEFAULT NULL,
  `message` longtext DEFAULT NULL,
  `sendemail_status` int(11) DEFAULT NULL,
  `created_date` date NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `mail_type` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `pms_mail_settings` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `role_id` tinyint(1) NOT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `pms_mail_template` (
  `temp_id` int(11) NOT NULL,
  `temp_name` varchar(200) NOT NULL,
  `temp_content` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_date` datetime NOT NULL,
  `updated_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `pms_manualentry` (
  `id` int(11) NOT NULL,
  `manual_date_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `site_id` int(11) DEFAULT NULL,
  `first_punch` time NOT NULL,
  `last_punch` time NOT NULL,
  `total_out_time` time NOT NULL,
  `added_date` date NOT NULL,
  `created_date` date NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_date` date NOT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `pms_manualentry_date` (
  `id` int(11) NOT NULL,
  `site_id` int(11) DEFAULT NULL,
  `date` date NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `pms_manual_entry` (
  `entry_id` int(11) NOT NULL,
  `emp_id` int(11) DEFAULT NULL,
  `shift_date` date DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `comment` text DEFAULT NULL,
  `status` tinyint(4) DEFAULT 0,
  `reason` varchar(500) DEFAULT NULL,
  `decision_by` int(11) DEFAULT NULL,
  `decision_date` date DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `pms_manual_entry_report_view` (
`entry_id` int(11)
,`entry_id_with_type` varchar(13)
,`emp_id` int(11)
,`date` datetime
,`comment` text
,`status` varchar(8)
,`type` varchar(12)
,`type_char` varchar(1)
,`status_id` tinyint(4)
,`created_by` int(11)
,`decision_by` int(11)
,`employee_name` varchar(61)
,`created_by_name` varchar(61)
,`decision_by_name` varchar(61)
);


CREATE TABLE `pms_manual_gen_attendance_log` (
  `id` int(11) NOT NULL,
  `att_date` date NOT NULL,
  `gen_by` int(11) DEFAULT NULL,
  `gen_date` datetime NOT NULL,
  `gen_success_date` datetime NOT NULL,
  `gstatus` tinyint(4) NOT NULL,
  `details` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `pms_material_requisition` (
  `requisition_id` int(11) NOT NULL,
  `requisition_project_id` int(11) NOT NULL,
  `requisition_task_id` int(11) NOT NULL,
  `requisition_no` varchar(200) NOT NULL,
  `requisition_status` int(11) NOT NULL DEFAULT 0 COMMENT '0->pending;1->approved;2->rejected',
  `created_by` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_by` int(11) NOT NULL,
  `updated_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `approved_rejected_by` int(11) DEFAULT NULL,
  `reject_reason` varchar(50) DEFAULT NULL,
  `purchase_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `pms_material_requisition_items` (
  `id` int(11) NOT NULL,
  `mr_id` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `item_name` varchar(50) DEFAULT NULL,
  `item_unit` varchar(200) DEFAULT NULL,
  `item_quantity` int(11) DEFAULT NULL,
  `item_req_date` date NOT NULL,
  `item_remarks` varchar(200) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `pms_meeting_detailed_reports` (
  `id` int(11) NOT NULL,
  `meeting_id` int(11) DEFAULT NULL,
  `report_id` int(11) DEFAULT NULL,
  `milestone` varchar(200) DEFAULT NULL,
  `contractor` varchar(200) DEFAULT NULL,
  `main_task` varchar(200) DEFAULT NULL,
  `sub_task` varchar(200) DEFAULT NULL,
  `area` varchar(200) DEFAULT NULL,
  `work_type` varchar(200) DEFAULT NULL,
  `total_quantity` float DEFAULT NULL,
  `target_quantity` float DEFAULT NULL,
  `achieved_quantity` float DEFAULT NULL,
  `balance_quantity` float DEFAULT NULL,
  `progress` float DEFAULT NULL,
  `status` varchar(200) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `comments` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



CREATE TABLE `pms_meeting_minutes` (
  `id` int(11) NOT NULL,
  `meeting_id` int(11) DEFAULT NULL,
  `milestone_id` int(11) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `create_task_status` tinyint(1) DEFAULT NULL COMMENT '1=>yes,2=>no',
  `points_discussed` text DEFAULT NULL,
  `submission_date` date DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `meeting_minutes_status` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` date DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `pms_meeting_minutes_users` (
  `id` int(11) NOT NULL,
  `meeting_minutes_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



CREATE TABLE `pms_meeting_participants` (
  `id` int(11) NOT NULL,
  `meeting_id` int(11) DEFAULT NULL,
  `contractor_id` int(11) DEFAULT NULL,
  `participants` text DEFAULT NULL,
  `participant_initial` varchar(50) DEFAULT NULL,
  `designation` text DEFAULT NULL,
  `organization_name` varchar(50) DEFAULT NULL,
  `organization_initial` varchar(50) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` date DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `pms_menu` (
  `menu_id` int(20) NOT NULL,
  `menu_name` varchar(30) NOT NULL,
  `parent_id` int(20) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `controller` varchar(30) NOT NULL,
  `action` varchar(30) NOT NULL,
  `params` varchar(50) NOT NULL,
  `showmenu` tinyint(1) NOT NULL COMMENT '0=>all, 1=>guest, 2=> authenticated',
  `show_list` tinyint(1) NOT NULL COMMENT ' 0=> dont show in list,1=>show in list'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `pms_menu_permissions` (
  `mp_id` int(20) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `menu_id` int(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `pms_milestone` (
  `id` int(11) NOT NULL,
  `milestone_title` varchar(100) NOT NULL,
  `status` int(11) NOT NULL COMMENT '1=>Enable,2=>disable',
  `project_id` int(11) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `ranking` int(11) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` date NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `pms_ot_attendance` (
  `att_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `att_date` date DEFAULT NULL,
  `comments` text NOT NULL,
  `att_entry` int(11) DEFAULT NULL,
  `ot_time` float NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime NOT NULL,
  `modified_date` timestamp NULL DEFAULT current_timestamp(),
  `type` tinyint(1) DEFAULT NULL COMMENT '0-generated,1-manual',
  `system_ot` varchar(255) DEFAULT NULL,
  `pending_status` int(11) DEFAULT 2
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DELIMITER $$
CREATE TRIGGER `befor_delete_ot` BEFORE DELETE ON `pms_ot_attendance` FOR EACH ROW BEGIN
INSERT INTO pms_ot_attendance_log 
(user_id, att_date, comments, att_entry, ot_time, created_by, created_date, modified_date) VALUES(old.user_id,old.att_date,old.comments,old.att_entry,old.ot_time,old.created_by,old.created_date,old.modified_date);

END
$$
DELIMITER ;



CREATE TABLE `pms_ot_attendance_log` (
  `log_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `att_date` date DEFAULT NULL,
  `comments` text NOT NULL,
  `att_entry` int(11) NOT NULL,
  `ot_time` float DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime NOT NULL,
  `modified_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `pms_ot_report` (
  `id` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `date` date NOT NULL,
  `system_ot` int(11) DEFAULT NULL,
  `system_ot_with_grace` int(11) NOT NULL,
  `manual_ot` int(11) DEFAULT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `log_data` text DEFAULT NULL,
  `comments` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `pms_payments` (
  `payid` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `projid` int(11) NOT NULL,
  `taskids` varchar(500) NOT NULL,
  `amount` float NOT NULL,
  `req_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `received_date` timestamp NULL DEFAULT NULL,
  `pay_status` smallint(6) NOT NULL,
  `remarks` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `modified_by` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `pms_payment_report` (
  `pmntid` int(11) NOT NULL,
  `pjctid` int(11) DEFAULT NULL,
  `invid` int(11) DEFAULT NULL,
  `paid_amnt` decimal(8,2) DEFAULT NULL,
  `balance_amnt` decimal(8,2) DEFAULT NULL,
  `paymentadded_date` date DEFAULT NULL,
  `remarks` text DEFAULT NULL,
  `clntid` int(11) DEFAULT NULL,
  `payment_source` varchar(250) DEFAULT NULL,
  `project_total_amnt` decimal(8,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `pms_pay_list` (
  `id` int(11) NOT NULL,
  `client_id` int(11) DEFAULT NULL,
  `pay_date` date DEFAULT NULL,
  `amount_paid` float DEFAULT NULL,
  `balance_amnt` float DEFAULT NULL,
  `pay_source` varchar(50) DEFAULT NULL,
  `created_date` date DEFAULT NULL,
  `updated_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `pms_pay_list_entry` (
  `id` int(11) NOT NULL,
  `pay_id` int(11) DEFAULT NULL,
  `inv_id` int(11) DEFAULT NULL,
  `teid` int(11) DEFAULT NULL,
  `rate` float(10,2) DEFAULT NULL,
  `generated_date` datetime DEFAULT NULL,
  `generated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `pms_photopunch` (
  `aid` double NOT NULL,
  `logid` double DEFAULT NULL,
  `sqllogid` int(11) DEFAULT NULL,
  `empid` int(11) NOT NULL,
  `log_time` datetime NOT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `device_id` int(11) NOT NULL,
  `manual_entry_status` int(11) DEFAULT NULL,
  `image` varchar(100) DEFAULT NULL,
  `latitude` varchar(100) DEFAULT NULL,
  `longitude` varchar(100) DEFAULT NULL,
  `punch_type` int(10) DEFAULT NULL COMMENT '1 punch in 2 punchout',
  `work_site_id` int(10) DEFAULT NULL,
  `work_site_name` varchar(100) DEFAULT NULL,
  `warning_message` varchar(250) DEFAULT NULL,
  `is_manual_updated` int(10) DEFAULT NULL,
  `approved_status` varchar(100) DEFAULT NULL COMMENT '1-approved 2 rejected',
  `reason` varchar(200) DEFAULT NULL,
  `location_type` varchar(100) DEFAULT NULL,
  `decision_by` int(11) DEFAULT NULL,
  `decision_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `pms_post` (
  `id` int(11) NOT NULL,
  `title` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `tags` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL,
  `create_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  `author_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



CREATE TABLE `pms_profiles` (
  `pid` int(11) NOT NULL,
  `profile_name` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `pms_profile_menu_settings` (
  `mp_id` int(20) NOT NULL,
  `role_id` tinyint(1) NOT NULL,
  `menu_id` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `pms_projects` (
  `pid` int(11) NOT NULL,
  `client_id` int(11) DEFAULT NULL,
  `project_no` varchar(250) DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `billable` smallint(6) DEFAULT 0,
  `status` smallint(6) NOT NULL,
  `budget` float(10,2) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `assigned_to` varchar(100) DEFAULT NULL,
  `img_path` text DEFAULT NULL,
  `report_image` text DEFAULT NULL,
  `total_square_feet` varchar(100) DEFAULT NULL,
  `clone_id` int(11) DEFAULT NULL,
  `clone_start_date` date DEFAULT NULL,
  `clone_end_date` date DEFAULT NULL,
  `architect` varchar(200) DEFAULT NULL,
  `template_id` int(11) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_by` int(11) NOT NULL,
  `updated_date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DELIMITER $$
CREATE TRIGGER `afterproject_update` AFTER UPDATE ON `pms_projects` FOR EACH ROW INSERT INTO pms_project_log SELECT 
    NULL,    
     d.pid,
     d.client_id,
     d.project_no,
     d.name,
     d.billable,
     d.status,
     d.budget,
     d.description,
     d.start_date,
     d.end_date,
     d.assigned_to,
     d.img_path,
     d.created_by,
     d.created_date,
     d.updated_by,
    d.updated_date,  
    old.client_id,
    old.project_no,
    old.name,
    old.billable,
    old.status,
    old.budget,
    old.description,
    old.start_date,
    old.end_date,
    old.assigned_to,
    old.updated_by,
    old.updated_date  
    FROM pms_projects AS d WHERE d.pid = NEW.pid
$$
DELIMITER ;



CREATE TABLE `pms_project_comments` (
  `comment_id` int(20) NOT NULL,
  `section_id` int(20) NOT NULL,
  `comment` text NOT NULL,
  `created_date` timestamp NULL DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `type` int(11) DEFAULT NULL COMMENT '1=>project, 2->tasks',
  `status_change_description` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `pms_project_log` (
  `id` int(11) NOT NULL,
  `pid` int(11) DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  `project_no` varchar(255) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `billable` smallint(6) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `budget` float DEFAULT NULL,
  `description` text DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `assigned_to` varchar(100) DEFAULT NULL,
  `img_path` text DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` date DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` date DEFAULT NULL,
  `client_id_old` int(11) DEFAULT NULL,
  `project_no_old` varchar(255) DEFAULT NULL,
  `name_old` varchar(100) DEFAULT NULL,
  `billable_old` int(11) DEFAULT NULL,
  `status_old` int(11) DEFAULT NULL,
  `budget_old` float DEFAULT NULL,
  `description_old` text DEFAULT NULL,
  `start_date_old` date DEFAULT NULL,
  `end_date_old` date DEFAULT NULL,
  `assigned_to_old` varchar(100) DEFAULT NULL,
  `updated_by_old` varchar(100) DEFAULT NULL,
  `updated_date_old` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `pms_project_report` (
  `id` int(11) NOT NULL,
  `project_id` int(11) DEFAULT NULL,
  `from_date` date DEFAULT NULL,
  `to_date` date DEFAULT NULL,
  `report_type` int(11) DEFAULT NULL,
  `report_data` text DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` date DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



CREATE TABLE `pms_project_type` (
  `ptid` smallint(6) NOT NULL,
  `project_type` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `pms_project_work_type` (
  `id` int(11) NOT NULL,
  `project_id` int(11) DEFAULT NULL,
  `work_type_id` smallint(11) DEFAULT NULL,
  `ranking` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` date DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



CREATE TABLE `pms_punching_devices` (
  `id` int(11) NOT NULL,
  `device_id` int(11) NOT NULL,
  `device_name` varchar(25) NOT NULL,
  `shot_name` varchar(15) NOT NULL,
  `site_id` int(11) DEFAULT NULL,
  `start_date` date NOT NULL,
  `end_date` date DEFAULT NULL,
  `nodevice_status` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `pms_punchreport` (
  `id` int(11) NOT NULL,
  `logdate` date NOT NULL,
  `resource_id` int(11) NOT NULL,
  `firstpunch` varchar(25) NOT NULL,
  `lastpunch` varchar(25) NOT NULL,
  `intime` varchar(10) NOT NULL,
  `outtime` varchar(10) NOT NULL,
  `totaltime` varchar(10) NOT NULL,
  `ot_hours` int(11) DEFAULT NULL,
  `late_hrs` int(11) DEFAULT NULL,
  `break_hrs` int(11) DEFAULT NULL,
  `total_punch` int(11) DEFAULT NULL,
  `shift_id` int(11) DEFAULT NULL,
  `standby_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `pms_punch_log` (
  `aid` double NOT NULL,
  `logid` double DEFAULT NULL,
  `sqllogid` int(11) DEFAULT NULL,
  `empid` int(11) NOT NULL,
  `log_time` datetime NOT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `device_id` int(11) NOT NULL,
  `manual_entry_status` int(11) DEFAULT NULL,
  `image` varchar(100) DEFAULT NULL,
  `latitude` varchar(100) DEFAULT NULL,
  `longitude` varchar(100) DEFAULT NULL,
  `punch_type` int(10) DEFAULT NULL COMMENT '1 punch in 2 punchout',
  `work_site_id` int(10) DEFAULT NULL,
  `work_site_name` varchar(100) DEFAULT NULL,
  `request_id` int(10) DEFAULT NULL,
  `punchlogid` varchar(100) DEFAULT NULL,
  `deviceid` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `pms_punch_request` (
  `aid` double NOT NULL,
  `logid` double DEFAULT NULL,
  `sqllogid` int(11) DEFAULT NULL,
  `empid` int(11) NOT NULL,
  `log_time` datetime NOT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `device_id` int(11) NOT NULL,
  `manual_entry_status` int(11) DEFAULT NULL,
  `image` varchar(100) DEFAULT NULL,
  `latitude` varchar(100) DEFAULT NULL,
  `longitude` varchar(100) DEFAULT NULL,
  `punch_type` int(10) DEFAULT NULL COMMENT '1 punch in 2 punchout',
  `work_site_id` int(10) DEFAULT NULL,
  `work_site_name` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `pms_punch_settings` (
  `setting_id` int(11) NOT NULL,
  `absent` time NOT NULL,
  `halfday` time NOT NULL,
  `late_halfday` time DEFAULT NULL,
  `late_fullday` time DEFAULT NULL,
  `minimum_in_time_half_day` float(8,2) DEFAULT NULL,
  `minimum_in_time_full_day` float(8,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `pms_report_images` (
  `id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `task_id` int(11) NOT NULL,
  `wpr_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `pms_role_settings` (
  `rset_id` int(11) NOT NULL,
  `role_id` tinyint(4) DEFAULT NULL,
  `short_name` varchar(20) DEFAULT NULL,
  `ot_formula` tinyint(1) NOT NULL COMMENT '0=>OT not applicable, 1=>Early_coming+Late_going',
  `min_ot` int(11) DEFAULT NULL,
  `max_ot_yes` tinyint(1) DEFAULT NULL,
  `max_ot` int(11) DEFAULT NULL,
  `last_inpunch` tinyint(1) DEFAULT NULL COMMENT '1=>neglect last inpunch',
  `late_coming_gt` int(11) DEFAULT NULL COMMENT 'grace time for late coming',
  `early_going_gt` int(11) DEFAULT NULL COMMENT 'grace time for early going',
  `weekly_off1_yes` tinyint(1) NOT NULL,
  `weekly_off1` varchar(20) NOT NULL,
  `weekly_off2_yes` tinyint(1) NOT NULL,
  `weekly_off2` varchar(20) DEFAULT NULL,
  `weekly_off2_count` varchar(20) DEFAULT NULL,
  `early_coming_punch` tinyint(1) NOT NULL COMMENT '1=>consider early coming punch',
  `late_going_punch` tinyint(1) NOT NULL COMMENT '1=>consider late going punch',
  `first_last_punch` tinyint(1) NOT NULL COMMENT '1=>consider only first and last punch in att calculation',
  `deduct_breakhours` tinyint(1) NOT NULL COMMENT 'Deduct breakhours from work duration',
  `halfday_by_duration` tinyint(1) NOT NULL COMMENT '1=>calculate half day if working duration less than',
  `halfday_duration_mins` int(11) DEFAULT NULL,
  `absent_by_duration` tinyint(1) NOT NULL COMMENT '1=>calculate absent if work duration less than',
  `absent_duration_mins` int(11) DEFAULT NULL,
  `partial_halfday_by_duration` tinyint(1) NOT NULL COMMENT '1=>on partial day, calculate half day if working duration less than',
  `partial_halfday_duration_mins` int(11) DEFAULT NULL,
  `partial_absent_by_duration` tinyint(1) NOT NULL COMMENT '1=>on partial day, calculate absent if work duration less than',
  `partial_absent_duration_mins` int(11) DEFAULT NULL,
  `absent_for_prefix` tinyint(1) NOT NULL COMMENT '1=>mark weekly off and holiday as absent if prefix day is absent',
  `absent_for_suffix` tinyint(1) NOT NULL COMMENT '1=>mark weekly off and holiday as absent ff suffix day is absent',
  `absent_for_both` tinyint(1) NOT NULL COMMENT '1=>mark weekly off and holiday as absent if both prefix and suffix is absent',
  `absent_for_condition` tinyint(1) NOT NULL COMMENT '1=>mark absent when late for',
  `absent_type` varchar(20) DEFAULT NULL,
  `absent_late` int(11) DEFAULT NULL,
  `halfday_by_late` tinyint(1) NOT NULL COMMENT '1=>mark halfday if late by',
  `halfday_late_mins` int(11) DEFAULT NULL,
  `halfday_by_earlygoing` tinyint(1) NOT NULL COMMENT '1=>mark halfday if early going by',
  `halfday_earlygoing_mins` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` date NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `pms_shift_assign` (
  `assign_shift_id` int(11) NOT NULL,
  `shift_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `att_date` date DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `created_date` datetime NOT NULL,
  `modified_date` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DELIMITER $$
CREATE TRIGGER `before_shift_change_happend` BEFORE UPDATE ON `pms_shift_assign` FOR EACH ROW BEGIN
    INSERT INTO pms_shift_assign_change_log values (null,OLD.assign_shift_id, old.shift_id,old.user_id, old.att_date, old.created_by, old.modified_by,OLD.created_date, old.`modified_date`, now());
    
END
$$
DELIMITER ;



CREATE TABLE `pms_shift_changes` (
  `change_id` int(11) NOT NULL,
  `emp1` int(11) DEFAULT NULL,
  `change_to` int(11) DEFAULT NULL,
  `date_from` date DEFAULT NULL,
  `date_to` date DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `type` tinyint(1) DEFAULT NULL,
  `decision_by` int(11) DEFAULT NULL,
  `decision_date` date DEFAULT NULL,
  `actual_log` text DEFAULT NULL,
  `changed_log` text DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `pms_shift_entry` (
  `shiftentry_id` int(11) NOT NULL,
  `emp_id` int(11) DEFAULT NULL,
  `shift_date` date DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `comment` text DEFAULT NULL,
  `status` tinyint(4) DEFAULT 0,
  `decision_by` int(11) DEFAULT NULL,
  `decision_date` date DEFAULT NULL,
  `type` tinyint(1) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `pms_shift_timings` (
  `id` int(11) NOT NULL,
  `accesscard_id` int(11) NOT NULL,
  `start_time` time NOT NULL,
  `end_time` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `pms_site_meetings` (
  `id` int(11) NOT NULL,
  `project_id` int(11) DEFAULT NULL,
  `site_id` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `venue` varchar(200) DEFAULT NULL,
  `objective` text NOT NULL,
  `next_meeting_date` date DEFAULT NULL,
  `next_meeting_time` time DEFAULT NULL,
  `location` varchar(200) DEFAULT NULL,
  `note` text DEFAULT NULL,
  `meeting_time` time DEFAULT NULL,
  `meeting_number` int(11) DEFAULT NULL,
  `approved_status` int(11) DEFAULT NULL,
  `approve_decision_by` int(11) DEFAULT NULL,
  `approve_decision_date` datetime DEFAULT NULL,
  `reason` varchar(100) DEFAULT NULL,
  `approved_by` varchar(100) DEFAULT NULL,
  `meeting_title` varchar(100) DEFAULT NULL,
  `meeting_ref` varchar(100) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` date DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `pms_status` (
  `sid` smallint(6) NOT NULL,
  `caption` varchar(30) NOT NULL,
  `status_type` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `pms_tag` (
  `id` int(11) NOT NULL,
  `name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `frequency` int(11) DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



CREATE TABLE `pms_tasks` (
  `tskid` int(11) NOT NULL,
  `project_id` int(11) DEFAULT NULL,
  `clientsite_id` int(11) DEFAULT NULL,
  `title` varchar(200) NOT NULL,
  `start_date` date NOT NULL,
  `due_date` date DEFAULT NULL,
  `status` smallint(6) NOT NULL,
  `priority` smallint(6) NOT NULL,
  `assigned_to` int(11) DEFAULT NULL,
  `report_to` int(11) DEFAULT NULL,
  `coordinator` int(11) DEFAULT NULL,
  `billable` smallint(6) NOT NULL,
  `total_hrs` float(10,2) DEFAULT NULL,
  `hourly_rate` float(5,2) DEFAULT NULL,
  `min_rate` float(10,2) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `trash` tinyint(1) NOT NULL DEFAULT 1,
  `parent_tskid` int(11) DEFAULT NULL,
  `progress_percent` float DEFAULT NULL,
  `acknowledge_by` int(11) DEFAULT NULL,
  `quantity` float(20,2) DEFAULT NULL,
  `unit` int(11) DEFAULT NULL,
  `rate` float(20,2) DEFAULT NULL,
  `amount` float(20,2) DEFAULT NULL,
  `milestone_id` int(11) DEFAULT NULL,
  `contractor_id` int(11) DEFAULT NULL,
  `task_duration` int(11) DEFAULT NULL,
  `daily_target` float(20,2) DEFAULT NULL,
  `work_type_id` smallint(6) DEFAULT NULL,
  `allowed_workers` float DEFAULT NULL,
  `required_workers` int(11) DEFAULT NULL,
  `email` text DEFAULT NULL,
  `task_type` enum('1','2') NOT NULL DEFAULT '1' COMMENT '1=>External,2=>Internal',
  `area` int(11) DEFAULT NULL,
  `ranking` int(11) DEFAULT NULL,
  `template_id` int(11) DEFAULT NULL,
  `meeting_status` int(11) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` date NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DELIMITER $$
CREATE TRIGGER `aftertask_update` AFTER UPDATE ON `pms_tasks` FOR EACH ROW INSERT INTO task_log SELECT 
    'update', 
    NULL, 
    NOW(),
     d.tskid,
     d.project_id,
     d.clientsite_id,
     d.title,
     d.start_date,
     d.due_date,
     d.status,
     d.priority,
     d.assigned_to,
     d.report_to,
     d.coordinator,
     d.billable,
     d.total_hrs,
     d.hourly_rate,
     d.min_rate,
    d.description,
    d.created_date,
    d.created_by,
    d.updated_date,
    d.updated_by,
    d.trash,
    d.parent_tskid,
    d.progress_percent,
    d.acknowledge_by,
    d.quantity,
    d.unit,
    d.rate,
    d.amount,
    d.milestone_id,
    d.contractor_id,
    d.task_duration,
    d.daily_target,
    d.work_type_id,
    d.allowed_workers,
    d.required_workers,
    d.task_type,
    d.email,
    old.project_id,
    old.clientsite_id,
    old.title,
    old.start_date,
    old.due_date,
    old.status,
    old.priority,
    old.assigned_to,
    old.report_to,
    old.coordinator,
    old.billable,
    old.total_hrs,
    old.hourly_rate,
    old.min_rate,
    old.description,
    old.created_date,
    old.created_by,
    old.updated_date,
    old.updated_by,
    old.trash,
    old.parent_tskid,
    old.progress_percent,
    old.acknowledge_by,
    old.quantity,
    old.unit,
    old.rate,
    old.amount,
    old.milestone_id,
    old.contractor_id,
    old.task_duration,
    old.daily_target,
    old.work_type_id,
    old.allowed_workers,
    old.required_workers,
    old.task_type,
    old.email
    FROM pms_tasks AS d WHERE d.tskid = NEW.tskid
$$
DELIMITER ;



CREATE TABLE `pms_task_dependancy` (
  `id` int(11) NOT NULL,
  `task_id` int(11) DEFAULT NULL,
  `dependant_task_id` int(11) DEFAULT NULL,
  `dependency_percentage` float DEFAULT NULL,
  `dependant_on` enum('1','2') NOT NULL DEFAULT '1' COMMENT '1=>Start_date,2=>Due_date'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `pms_task_expiry` (
  `id` int(11) NOT NULL,
  `day` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` date NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `pms_task_item_estimation` (
  `estimation_id` int(11) NOT NULL,
  `task_id` int(11) NOT NULL,
  `template_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `item_quantity_required` int(11) DEFAULT NULL,
  `item_quantity_used` int(11) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `pms_template` (
  `template_id` int(11) NOT NULL,
  `template_name` varchar(50) NOT NULL,
  `status` int(11) DEFAULT 0,
  `created_by` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `pms_template_items` (
  `id` int(11) NOT NULL,
  `temp_id` int(11) NOT NULL,
  `ref_column` varchar(50) NOT NULL,
  `item_name` varchar(50) NOT NULL,
  `purchase_item` varchar(50) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `pms_time_attendance` (
  `att_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `att_date` date DEFAULT NULL,
  `comments` text NOT NULL,
  `att_entry` int(11) DEFAULT NULL,
  `att_time` float NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime NOT NULL,
  `modified_date` timestamp NULL DEFAULT current_timestamp(),
  `type` tinyint(1) DEFAULT NULL COMMENT '0-generated,1-manual'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DELIMITER $$
CREATE TRIGGER `before_delete_in_time_attendance_delete_log` BEFORE DELETE ON `pms_time_attendance` FOR EACH ROW BEGIN INSERT INTO pms_time_attendance_delete_log (`att_id`, `user_id`, `att_date`, `comments`, `att_entry`, `att_time`, `created_by`, `created_date`, `modified_date`, `type`) select * from pms_time_attendance where att_id=old.`att_id`; END
$$
DELIMITER ;



CREATE TABLE `pms_time_attendance_delete_log` (
  `delete_log_id` int(11) NOT NULL,
  `att_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `att_date` date DEFAULT NULL,
  `comments` text NOT NULL,
  `att_entry` int(11) DEFAULT NULL,
  `att_time` float NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime NOT NULL,
  `modified_date` timestamp NULL DEFAULT current_timestamp(),
  `type` tinyint(1) DEFAULT NULL COMMENT '0-generated,1-manual'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `pms_time_entry` (
  `teid` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `tskid` int(11) NOT NULL,
  `work_type` smallint(6) NOT NULL,
  `entry_date` date NOT NULL,
  `hours` time DEFAULT NULL,
  `billable` tinyint(1) NOT NULL,
  `description` text NOT NULL,
  `created_date` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `completed_percent` float DEFAULT NULL,
  `approve_status` int(11) NOT NULL DEFAULT 0 COMMENT '0=>not approved,1=>approved',
  `current_status` smallint(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DELIMITER $$
CREATE TRIGGER `after_timeentry_insert` AFTER INSERT ON `pms_time_entry` FOR EACH ROW BEGIN
UPDATE pms_tasks SET progress_percent=NEW.completed_percent WHERE tskid= NEW.tskid;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `after_timeentry_update` AFTER UPDATE ON `pms_time_entry` FOR EACH ROW BEGIN
 DECLARE newid INT;
 SELECT teid INTO newid FROM pms_time_entry WHERE tskid = OLD.tskid order by teid DESC Limit 1 ;
IF (newid = OLD.teid) THEN
UPDATE pms_tasks SET progress_percent=NEW.completed_percent WHERE tskid= OLD.tskid;
END IF;
END
$$
DELIMITER ;


CREATE TABLE `pms_time_report` (
`teid` int(11)
,`project_id` int(11)
,`project` varchar(100)
,`task_id` int(11)
,`task` varchar(200)
,`description` text
,`work_type` varchar(30)
,`entry_date` date
,`hours` time
,`billable` tinyint(1)
,`user_id` int(11)
,`username` varchar(20)
);


CREATE TABLE `pms_unit` (
  `id` int(11) NOT NULL,
  `unit_title` varchar(100) NOT NULL,
  `unit_code` varchar(50) NOT NULL,
  `status` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` date NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `pms_user` (
  `id` int(11) NOT NULL,
  `username` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `profile` text COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



CREATE TABLE `pms_users` (
  `userid` int(11) NOT NULL,
  `user_type` tinyint(1) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) DEFAULT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(32) NOT NULL,
  `email` varchar(70) NOT NULL,
  `reporting_person` int(11) DEFAULT NULL,
  `last_modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `reg_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `reg_ip` varchar(15) NOT NULL,
  `activation_key` varchar(32) DEFAULT NULL,
  `email_activation` bit(1) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `joining_date` date DEFAULT NULL,
  `resignation_date` date DEFAULT NULL,
  `accesscard_id` int(11) DEFAULT NULL,
  `pms_access` enum('0','1') DEFAULT NULL,
  `tms_access` enum('0','1') DEFAULT NULL,
  `wpr_access` enum('0','1') DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `designation` varchar(255) DEFAULT NULL,
  `employee_id` varchar(255) DEFAULT NULL,
  `setting_template` int(100) DEFAULT NULL,
  `report_to` int(100) DEFAULT NULL,
  `shift_supervisor` int(10) DEFAULT NULL,
  `company_id` int(100) DEFAULT NULL,
  `employee_code` varchar(100) DEFAULT NULL,
  `type` enum('1','2') DEFAULT '1',
  `company` varchar(100) DEFAULT NULL,
  `contact_number` varchar(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` date DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `pms_usersite` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `site_id` int(11) DEFAULT NULL,
  `assigned_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `pms_user_roles` (
  `id` tinyint(1) NOT NULL,
  `role` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `pms_work_type` (
  `wtid` smallint(6) NOT NULL,
  `work_type` varchar(30) DEFAULT NULL,
  `rate` float(5,2) DEFAULT NULL,
  `daily_throughput` varchar(150) DEFAULT NULL,
  `unit_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `task_log` (
  `action` varchar(8) DEFAULT 'insert',
  `revision` int(6) NOT NULL,
  `dt_datetime` datetime NOT NULL DEFAULT current_timestamp(),
  `tskid` int(11) NOT NULL,
  `project_id` int(11) DEFAULT NULL,
  `clientsite_id` int(11) DEFAULT NULL,
  `title` varchar(200) NOT NULL,
  `start_date` date NOT NULL,
  `due_date` date DEFAULT NULL,
  `status` smallint(6) NOT NULL,
  `priority` smallint(6) NOT NULL,
  `assigned_to` int(11) NOT NULL,
  `report_to` int(11) NOT NULL,
  `coordinator` int(11) DEFAULT NULL,
  `billable` smallint(6) NOT NULL,
  `total_hrs` float(10,2) DEFAULT NULL,
  `hourly_rate` float(5,2) DEFAULT NULL,
  `min_rate` float(10,2) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `created_date` date NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_date` date NOT NULL,
  `updated_by` int(11) NOT NULL,
  `trash` tinyint(1) NOT NULL DEFAULT 1,
  `parent_tskid` int(11) DEFAULT NULL,
  `progress_percent` float DEFAULT NULL,
  `acknowledge_by` int(11) DEFAULT NULL,
  `quantity` float(20,2) DEFAULT NULL,
  `unit` int(11) NOT NULL,
  `rate` float(20,2) DEFAULT NULL,
  `amount` float(20,2) DEFAULT NULL,
  `milestone_id` int(11) DEFAULT NULL,
  `contractor_id` int(11) DEFAULT NULL,
  `task_duration` int(11) NOT NULL,
  `daily_target` float(20,2) NOT NULL,
  `work_type_id` smallint(6) DEFAULT NULL,
  `allowed_workers` float DEFAULT NULL,
  `required_workers` int(11) DEFAULT NULL,
  `email` text DEFAULT NULL,
  `task_type` enum('1','2') NOT NULL DEFAULT '1' COMMENT '1=>External,2=>Internal',
  `project_id_old` int(11) DEFAULT NULL,
  `clientsite_id_old` int(11) DEFAULT NULL,
  `title_old` varchar(200) NOT NULL,
  `start_date_old` varchar(200) NOT NULL,
  `due_date_old` date DEFAULT NULL,
  `status_old` smallint(6) DEFAULT NULL,
  `priority_old` smallint(6) DEFAULT NULL,
  `assigned_to_old` int(11) DEFAULT NULL,
  `report_to_old` int(11) DEFAULT NULL,
  `coordinator_old` int(11) DEFAULT NULL,
  `billable_old` smallint(6) DEFAULT NULL,
  `total_hrs_old` float(10,2) DEFAULT NULL,
  `hourly_rate_old` float(10,2) DEFAULT NULL,
  `min_rate_old` float(10,2) DEFAULT NULL,
  `description_old` text DEFAULT NULL,
  `created_date_old` date DEFAULT NULL,
  `created_by_old` int(11) DEFAULT NULL,
  `updated_date_old` date DEFAULT NULL,
  `updated_by_old` int(11) DEFAULT NULL,
  `trash_old` tinyint(1) DEFAULT NULL,
  `parent_tskid_old` int(11) DEFAULT NULL,
  `progress_percent_old` float DEFAULT NULL,
  `acknowledge_by_old` int(11) DEFAULT NULL,
  `quantity_old` float(20,2) DEFAULT NULL,
  `unit_old` int(11) DEFAULT NULL,
  `rate_old` float(20,2) DEFAULT NULL,
  `amount_old` float(20,2) DEFAULT NULL,
  `milestone_id_old` int(11) DEFAULT NULL,
  `contractor_id_old` int(11) DEFAULT NULL,
  `task_duration_old` int(11) DEFAULT NULL,
  `daily_target_old` float(20,2) DEFAULT NULL,
  `work_type_id_old` smallint(6) DEFAULT NULL,
  `allowed_workers_old` float DEFAULT NULL,
  `required_workers_old` int(11) DEFAULT NULL,
  `task_type_old` enum('1','2') DEFAULT NULL,
  `email_old` text DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `pms_manual_entry_report_view`;

CREATE ALGORITHM=UNDEFINED  SQL SECURITY DEFINER VIEW `pms_manual_entry_report_view`  AS SELECT `t1`.`entry_id` AS `entry_id`, concat_ws('#',`t1`.`entry_id`,'m') AS `entry_id_with_type`, `t1`.`emp_id` AS `emp_id`, `t1`.`date` AS `date`, `t1`.`comment` AS `comment`, if(`t1`.`status` = 2,'Rejected',if(`t1`.`status` = 1,'Approved','Pending')) AS `status`, 'Manual Punch' AS `type`, 'm' AS `type_char`, `t1`.`status` AS `status_id`, `t1`.`created_by` AS `created_by`, `t1`.`decision_by` AS `decision_by`, concat_ws(' ',`e`.`first_name`,`e`.`last_name`) AS `employee_name`, concat_ws(' ',`cby`.`first_name`,`cby`.`last_name`) AS `created_by_name`, concat_ws(' ',`dby`.`first_name`,`dby`.`last_name`) AS `decision_by_name` FROM (((`pms_manual_entry` `t1` left join `pms_users` `e` on(`e`.`userid` = `t1`.`emp_id`)) left join `pms_users` `cby` on(`cby`.`userid` = `t1`.`created_by`)) left join `pms_users` `dby` on(`dby`.`userid` = `t1`.`decision_by`)) union select `t1`.`ig_id` AS `entry_id`,concat_ws('#',`t1`.`ig_id`,'i') AS `entry_id_with_type`,`t1`.`userid` AS `userid`,`t1`.`date` AS `date`,`t1`.`comment` AS `comment`,if(`t1`.`status` = 2,'Rejected',if(`t1`.`status` = 1,'Approved','Pending')) AS `status`,'Ignore Punch' AS `type`,'i' AS `type_char`,`t1`.`status` AS `status_id`,`t1`.`created_by` AS `created_by`,`t1`.`decision_by` AS `decision_by`,concat_ws(' ',`e`.`first_name`,`e`.`last_name`) AS `employee_name`,concat_ws(' ',`cby`.`first_name`,`cby`.`last_name`) AS `created_by_name`,concat_ws(' ',`dby`.`first_name`,`dby`.`last_name`) AS `decision_by_name` from (((`pms_ignore_punches` `t1` left join `pms_users` `e` on(`e`.`userid` = `t1`.`userid`)) left join `pms_users` `cby` on(`cby`.`userid` = `t1`.`created_by`)) left join `pms_users` `dby` on(`dby`.`userid` = `t1`.`decision_by`)) union select `t1`.`shiftentry_id` AS `shiftentry_id`,concat_ws('#',`t1`.`shiftentry_id`,'s') AS `entry_id_with_type`,`t1`.`emp_id` AS `emp_id`,`t1`.`date` AS `date`,`t1`.`comment` AS `comment`,if(`t1`.`status` = 2,'Rejected',if(`t1`.`status` = 1,'Approved','Pending')) AS `status`,'Shift Punch' AS `type`,'s' AS `type_char`,`t1`.`status` AS `status_id`,`t1`.`created_by` AS `created_by`,`t1`.`decision_by` AS `decision_by`,concat_ws(' ',`e`.`first_name`,`e`.`last_name`) AS `employee_name`,concat_ws(' ',`cby`.`first_name`,`cby`.`last_name`) AS `created_by_name`,concat_ws(' ',`dby`.`first_name`,`dby`.`last_name`) AS `decision_by_name` from (((`pms_shift_entry` `t1` left join `pms_users` `e` on(`e`.`userid` = `t1`.`emp_id`)) left join `pms_users` `cby` on(`cby`.`userid` = `t1`.`created_by`)) left join `pms_users` `dby` on(`dby`.`userid` = `t1`.`decision_by`))  ;


DROP TABLE IF EXISTS `pms_time_report`;

CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `pms_time_report`  AS SELECT `te`.`teid` AS `teid`, `p`.`pid` AS `project_id`, `p`.`name` AS `project`, `tsk`.`tskid` AS `task_id`, `tsk`.`title` AS `task`, `te`.`description` AS `description`, `wt`.`work_type` AS `work_type`, `te`.`entry_date` AS `entry_date`, `te`.`hours` AS `hours`, `te`.`billable` AS `billable`, `te`.`user_id` AS `user_id`, `u`.`username` AS `username` FROM ((((`pms_time_entry` `te` join `pms_tasks` `tsk` on(`tsk`.`tskid` = `te`.`tskid`)) left join `pms_projects` `p` on(`p`.`pid` = `tsk`.`project_id`)) join `pms_work_type` `wt` on(`wt`.`wtid` = `te`.`work_type`)) join `pms_users` `u` on(`u`.`userid` = `te`.`user_id`)) ORDER BY `te`.`entry_date` ASC  ;


ALTER TABLE `pms_accesslog`
  ADD PRIMARY KEY (`logid`),
  ADD UNIQUE KEY `empid` (`empid`,`log_time`,`device_id`);


ALTER TABLE `pms_accesslog_last_sync`
  ADD PRIMARY KEY (`id`);


ALTER TABLE `pms_account_permission`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `pms_acc_project_mapping`
  ADD PRIMARY KEY (`id`);


ALTER TABLE `pms_acc_wpr_item_consumed`
  ADD PRIMARY KEY (`id`);


ALTER TABLE `pms_area`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`),
  ADD KEY `project_id` (`project_id`);


ALTER TABLE `pms_attendance`
  ADD PRIMARY KEY (`att_id`),
  ADD UNIQUE KEY `user_id_2` (`user_id`,`att_date`),
  ADD KEY `att_entry` (`att_entry`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `created_by` (`created_by`);


ALTER TABLE `pms_attendance_delete_log`
  ADD PRIMARY KEY (`delete_log_id`),
  ADD KEY `att_entry` (`att_entry`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `user_id` (`user_id`);


ALTER TABLE `pms_attendance_log`
  ADD PRIMARY KEY (`log_id`),
  ADD KEY `refer_id` (`refer_id`),
  ADD KEY `att_entry` (`att_entry`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `user_id` (`user_id`);


ALTER TABLE `pms_attendance_reject_log`
  ADD PRIMARY KEY (`att_id`),
  ADD KEY `att_entry` (`att_entry`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `user_id` (`user_id`);


ALTER TABLE `pms_attendance_requests`
  ADD PRIMARY KEY (`att_id`),
  ADD KEY `att_entry` (`att_entry`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `decision_by` (`decision_by`);


ALTER TABLE `pms_attendance_settings`
  ADD PRIMARY KEY (`settings_id`);


ALTER TABLE `pms_avail_compoff`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`),
  ADD KEY `userid` (`userid`);


ALTER TABLE `pms_avail_leave`
  ADD PRIMARY KEY (`avail_leave_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`),
  ADD KEY `userid` (`userid`);


ALTER TABLE `pms_balance_leave`
  ADD PRIMARY KEY (`avail_leave_id`),
  ADD KEY `modified_by` (`modified_by`),
  ADD KEY `userid` (`userid`);


ALTER TABLE `pms_biddings`
  ADD PRIMARY KEY (`bid`),
  ADD KEY `biddings_worktype_fk` (`worktype`),
  ADD KEY `bidding_profile_fk` (`profile`),
  ADD KEY `bidding_bidby_fk` (`bid_by`),
  ADD KEY `biddings_status_fk` (`status`);


ALTER TABLE `pms_calender_events`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `site_id` (`site_id`),
  ADD KEY `updated_by` (`updated_by`);


ALTER TABLE `pms_clients`
  ADD PRIMARY KEY (`cid`),
  ADD KEY `clients_users_createdby_fk` (`created_by`),
  ADD KEY `clients_users_updatedby_fk` (`updated_by`),
  ADD KEY `clients_status_status_fk` (`status`),
  ADD KEY `clients_projtype_fk` (`project_type`);

ALTER TABLE `pms_clientsite`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pid` (`pid`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);


ALTER TABLE `pms_clientsite_assigned`
  ADD PRIMARY KEY (`site_id`,`user_id`) COMMENT 'Composite key',
  ADD KEY `pms_clientsite_assigned_user_fk` (`user_id`);


ALTER TABLE `pms_comment`
  ADD PRIMARY KEY (`id`);


ALTER TABLE `pms_company`
  ADD PRIMARY KEY (`company_id`),
  ADD KEY `active_status` (`active_status`);


ALTER TABLE `pms_contractors`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);


ALTER TABLE `pms_country`
  ADD PRIMARY KEY (`Code`);


ALTER TABLE `pms_cron`
  ADD PRIMARY KEY (`id`);


ALTER TABLE `pms_current_task_status`
  ADD PRIMARY KEY (`ctsid`),
  ADD KEY `tasks_cts_fk` (`taskid`),
  ADD KEY `tasks_users_cts_fk` (`userid`);


ALTER TABLE `pms_custompunch`
  ADD PRIMARY KEY (`customid`),
  ADD KEY `resource` (`resource`),
  ADD KEY `device` (`device`),
  ADD KEY `createdby` (`createdby`);


ALTER TABLE `pms_daily_overtime`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`),
  ADD KEY `userid` (`userid`);


ALTER TABLE `pms_daily_sevice_entries`
  ADD PRIMARY KEY (`entry_id`),
  ADD KEY `project_id` (`project_id`),
  ADD KEY `created_by` (`created_by`);


ALTER TABLE `pms_daily_work`
  ADD PRIMARY KEY (`id`),
  ADD KEY `project_id` (`project_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `fk_worktype` (`work_type`),
  ADD KEY `assigned_to` (`assigned_to`),
  ADD KEY `fk_taskid` (`taskid`),
  ADD KEY `fk_item_id` (`item_id`);


ALTER TABLE `pms_daily_work_progress`
  ADD PRIMARY KEY (`id`),
  ADD KEY `wpr_daily_work_progress_proj` (`project_id`),
  ADD KEY `item_id` (`item_id`),
  ADD KEY `fk_wpr_daily_work_progress_created_by` (`created_by`),
  ADD KEY `taskid_fk` (`taskid`),
  ADD KEY `worktype` (`work_type`),
  ADD KEY `current_status` (`current_status`),
  ADD KEY `site_id` (`site_id`),
  ADD KEY `template_item_id` (`template_item_id`);


ALTER TABLE `pms_department`
  ADD PRIMARY KEY (`department_id`);


ALTER TABLE `pms_device_accessids`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userid` (`userid`),
  ADD KEY `deviceid` (`deviceid`);


ALTER TABLE `pms_earned_leave`
  ADD PRIMARY KEY (`id`),
  ADD KEY `action_by` (`action_by`),
  ADD KEY `updated_by` (`updated_by`),
  ADD KEY `user_id` (`user_id`);


ALTER TABLE `pms_electricity`
  ADD PRIMARY KEY (`eid`);


ALTER TABLE `pms_employeerequest`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by_fk1` (`created_by`),
  ADD KEY `ack_by_fk2` (`ack_by`);


ALTER TABLE `pms_employee_default_data`
  ADD PRIMARY KEY (`default_data_id`);


ALTER TABLE `pms_employee_group`
  ADD PRIMARY KEY (`group_id`);

ALTER TABLE `pms_employee_shift`
  ADD PRIMARY KEY (`shift_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

ALTER TABLE `pms_general_settings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `updated_by` (`updated_by`);


ALTER TABLE `pms_groups`
  ADD PRIMARY KEY (`group_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

ALTER TABLE `pms_groups_members`
  ADD PRIMARY KEY (`id`),
  ADD KEY `group_id` (`group_id`),
  ADD KEY `group_members` (`group_members`);

ALTER TABLE `pms_group_leaders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

ALTER TABLE `pms_holidays`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

ALTER TABLE `pms_ignore_punches`
  ADD PRIMARY KEY (`ig_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `decision_by` (`decision_by`),
  ADD KEY `userid` (`userid`);

ALTER TABLE `pms_invoice`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `invoice_id` (`invoice_id`),
  ADD KEY `invoice_user_tbl_fk` (`generated_by`),
  ADD KEY `invoice_status_tbl_fk` (`status`),
  ADD KEY `invoice_client_tbl_fk` (`client_id`);


ALTER TABLE `pms_invoiced_entry`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `teid` (`teid`),
  ADD KEY `invoiced_entry_invoice` (`invoice_id`),
  ADD KEY `invoiced_entry_timeentry` (`teid`),
  ADD KEY `invoiced_entry_user` (`generated_by`),
  ADD KEY `invoiced_entry_invoice_worktype` (`work_type`);


ALTER TABLE `pms_issues`
  ADD PRIMARY KEY (`id`),
  ADD KEY `issue_users_updatedby_fk` (`updated_by`),
  ADD KEY `issue_users_userid_fk` (`created_by`),
  ADD KEY `status` (`status`);


ALTER TABLE `pms_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `projectid` (`projectid`),
  ADD KEY `taskid` (`taskid`);

ALTER TABLE `pms_lead`
  ADD PRIMARY KEY (`lead_id`),
  ADD KEY `lead_coutry_fk` (`country`),
  ADD KEY `lead_industry_fk` (`industry`),
  ADD KEY `lead_opportunity_type_fk` (`opportunity_type`),
  ADD KEY `lead_priority_fk` (`priority`),
  ADD KEY `lead_status_fk` (`status`),
  ADD KEY `lead_title_fk` (`lead_title`),
  ADD KEY `lead_users_createdby_fk` (`created_by`),
  ADD KEY `lead_users_updatedby_fk` (`updated_by`);

ALTER TABLE `pms_lead_activity`
  ADD PRIMARY KEY (`activity_id`),
  ADD KEY `lead_activity_users_createdby_fk` (`created_by`),
  ADD KEY `lead_activity_users_updatedby_fk` (`updated_by`),
  ADD KEY `lead_act_followup_fk` (`followup`),
  ADD KEY `lead_act_type_fk` (`activity_type`),
  ADD KEY `lead_leadactivity` (`lead_id`),
  ADD KEY `pms_lead_activity_ibfk_1` (`contacted_person`);

ALTER TABLE `pms_lead_contact`
  ADD PRIMARY KEY (`cid`),
  ADD KEY `lead_contatct` (`lead_id`),
  ADD KEY `pms_lead_contact_ibfk_1` (`title`);


ALTER TABLE `pms_leave`
  ADD PRIMARY KEY (`leave_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `decision_by` (`decision_by`),
  ADD KEY `emp_id` (`emp_id`),
  ADD KEY `reporting_person` (`reporting_person`),
  ADD KEY `updated_by` (`updated_by`);


ALTER TABLE `pms_leave_credit`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`);


ALTER TABLE `pms_leave_cron`
  ADD PRIMARY KEY (`cron_id`);


ALTER TABLE `pms_leave_day`
  ADD PRIMARY KEY (`leave_day_id`),
  ADD KEY `leave_id` (`leave_id`);


ALTER TABLE `pms_leave_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comp_id` (`comp_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `leave_id` (`leave_id`),
  ADD KEY `userid` (`userid`);


ALTER TABLE `pms_leave_rules`
  ADD PRIMARY KEY (`rule_id`),
  ADD KEY `hrms_designation_ibfk_2` (`designation_id`);


ALTER TABLE `pms_leave_setting`
  ADD PRIMARY KEY (`leave_setting_id`);


ALTER TABLE `pms_leave_types`
  ADD PRIMARY KEY (`id`),
  ADD KEY `leavetype_legend` (`leave_id`);


ALTER TABLE `pms_legends`
  ADD PRIMARY KEY (`leg_id`);


ALTER TABLE `pms_lookup`
  ADD PRIMARY KEY (`id`);


ALTER TABLE `pms_mail_log`
  ADD PRIMARY KEY (`log_id`),
  ADD KEY `fk_created_by` (`created_by`);


ALTER TABLE `pms_mail_settings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_id` (`role_id`),
  ADD KEY `user_id` (`user_id`);


ALTER TABLE `pms_mail_template`
  ADD PRIMARY KEY (`temp_id`);


ALTER TABLE `pms_manualentry`
  ADD PRIMARY KEY (`id`),
  ADD KEY `manualentry_created_by_fk` (`created_by`),
  ADD KEY `manualentry_updated_by_fk` (`updated_by`),
  ADD KEY `manualentry_userid_fk` (`user_id`),
  ADD KEY `manualentry_site_id_fk` (`site_id`),
  ADD KEY `manual_date_id_fk` (`manual_date_id`);


ALTER TABLE `pms_manualentry_date`
  ADD PRIMARY KEY (`id`),
  ADD KEY `site_id` (`site_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);


ALTER TABLE `pms_manual_entry`
  ADD PRIMARY KEY (`entry_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `decision_by` (`decision_by`),
  ADD KEY `emp_id` (`emp_id`);


ALTER TABLE `pms_manual_gen_attendance_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `gen_by` (`gen_by`);

ALTER TABLE `pms_material_requisition`
  ADD PRIMARY KEY (`requisition_id`),
  ADD KEY `requisition_project_id` (`requisition_project_id`),
  ADD KEY `requisition_task_id` (`requisition_task_id`);


ALTER TABLE `pms_material_requisition_items`
  ADD PRIMARY KEY (`id`);


ALTER TABLE `pms_meeting_detailed_reports`
  ADD PRIMARY KEY (`id`),
  ADD KEY `meeting_id` (`meeting_id`),
  ADD KEY `report_id` (`report_id`);


ALTER TABLE `pms_meeting_minutes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `meeting_id` (`meeting_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`),
  ADD KEY `milestone_id` (`milestone_id`);


ALTER TABLE `pms_meeting_minutes_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `meeting_minutes_id` (`meeting_minutes_id`),
  ADD KEY `user_id` (`user_id`);


ALTER TABLE `pms_meeting_participants`
  ADD PRIMARY KEY (`id`),
  ADD KEY `meeting_id` (`meeting_id`),
  ADD KEY `contractor_id` (`contractor_id`);

ALTER TABLE `pms_menu`
  ADD PRIMARY KEY (`menu_id`),
  ADD KEY `parent_id` (`parent_id`);

ALTER TABLE `pms_menu_permissions`
  ADD PRIMARY KEY (`mp_id`),
  ADD KEY `fk_menu_id` (`menu_id`),
  ADD KEY `fk_user_id` (`user_id`);

ALTER TABLE `pms_milestone`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`,`updated_by`),
  ADD KEY `updated_by` (`updated_by`),
  ADD KEY `project_id` (`project_id`);

ALTER TABLE `pms_ot_attendance`
  ADD PRIMARY KEY (`att_id`),
  ADD UNIQUE KEY `user_id_2` (`user_id`,`att_date`),
  ADD KEY `att_entry` (`ot_time`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `att_entry_2` (`att_entry`),
  ADD KEY `created_by` (`created_by`);

ALTER TABLE `pms_ot_attendance_log`
  ADD PRIMARY KEY (`log_id`),
  ADD KEY `att_entry` (`att_entry`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `user_id` (`user_id`);

ALTER TABLE `pms_ot_report`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pms_ot_report_ibfk_1` (`userid`),
  ADD KEY `updated_by` (`updated_by`);


ALTER TABLE `pms_payments`
  ADD PRIMARY KEY (`payid`),
  ADD KEY `pay_projects_fk` (`projid`),
  ADD KEY `pay_status_fk` (`pay_status`),
  ADD KEY `pay_userid_fk` (`userid`),
  ADD KEY `pay_created_fk` (`created_by`),
  ADD KEY `pay_modified_fk` (`modified_by`);


ALTER TABLE `pms_payment_report`
  ADD PRIMARY KEY (`pmntid`),
  ADD KEY `clntid` (`clntid`),
  ADD KEY `invid` (`invid`),
  ADD KEY `pjctid` (`pjctid`);


ALTER TABLE `pms_pay_list`
  ADD PRIMARY KEY (`id`),
  ADD KEY `client_id` (`client_id`);


ALTER TABLE `pms_pay_list_entry`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `teid` (`teid`),
  ADD KEY `invoiced_entry_invoice` (`inv_id`),
  ADD KEY `invoiced_entry_timeentry` (`teid`),
  ADD KEY `invoiced_entry_user` (`generated_by`),
  ADD KEY `inv_list_id` (`pay_id`);


ALTER TABLE `pms_photopunch`
  ADD PRIMARY KEY (`aid`),
  ADD KEY `decision_by` (`decision_by`),
  ADD KEY `empid` (`empid`);


ALTER TABLE `pms_post`
  ADD PRIMARY KEY (`id`);


ALTER TABLE `pms_profiles`
  ADD PRIMARY KEY (`pid`);


ALTER TABLE `pms_profile_menu_settings`
  ADD PRIMARY KEY (`mp_id`),
  ADD KEY `menu_id` (`menu_id`),
  ADD KEY `user_id` (`role_id`);


ALTER TABLE `pms_projects`
  ADD PRIMARY KEY (`pid`),
  ADD KEY `projects_users_createdby_fk` (`created_by`),
  ADD KEY `projects_users_updatedby_fk` (`updated_by`),
  ADD KEY `projects_clients_fk` (`client_id`),
  ADD KEY `projects_status_fk` (`status`),
  ADD KEY `projects_billable_status_fk` (`billable`);


ALTER TABLE `pms_project_comments`
  ADD PRIMARY KEY (`comment_id`),
  ADD KEY `fk_comment_user_id` (`user_id`);


ALTER TABLE `pms_project_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `billable` (`billable`),
  ADD KEY `pid` (`pid`),
  ADD KEY `client_id` (`client_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);


ALTER TABLE `pms_project_report`
  ADD PRIMARY KEY (`id`),
  ADD KEY `project_id` (`project_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

ALTER TABLE `pms_project_type`
  ADD PRIMARY KEY (`ptid`);

ALTER TABLE `pms_project_work_type`
  ADD PRIMARY KEY (`id`),
  ADD KEY `project_id` (`project_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`),
  ADD KEY `work_type_id` (`work_type_id`);

ALTER TABLE `pms_punching_devices`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `device_id` (`device_id`),
  ADD KEY `punching_devices_site_id_fk` (`site_id`);

ALTER TABLE `pms_punchreport`
  ADD PRIMARY KEY (`id`),
  ADD KEY `resource_id` (`resource_id`);

ALTER TABLE `pms_punch_log`
  ADD PRIMARY KEY (`aid`),
  ADD KEY `empid` (`empid`),
  ADD KEY `work_site_id` (`work_site_id`);

ALTER TABLE `pms_punch_request`
  ADD PRIMARY KEY (`aid`),
  ADD KEY `work_site_id` (`work_site_id`);

ALTER TABLE `pms_punch_settings`
  ADD PRIMARY KEY (`setting_id`);

ALTER TABLE `pms_report_images`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `pms_role_settings`
  ADD PRIMARY KEY (`rset_id`),
  ADD KEY `role_id` (`role_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

ALTER TABLE `pms_shift_assign`
  ADD PRIMARY KEY (`assign_shift_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `modified_by` (`modified_by`);


ALTER TABLE `pms_shift_changes`
  ADD PRIMARY KEY (`change_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `decision_by` (`decision_by`),
  ADD KEY `emp1` (`emp1`);

ALTER TABLE `pms_shift_entry`
  ADD PRIMARY KEY (`shiftentry_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `decision_by` (`decision_by`),
  ADD KEY `emp_id` (`emp_id`);

ALTER TABLE `pms_shift_timings`
  ADD PRIMARY KEY (`id`);


ALTER TABLE `pms_site_meetings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `project_id` (`project_id`),
  ADD KEY `site_id` (`site_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

ALTER TABLE `pms_status`
  ADD PRIMARY KEY (`sid`);


ALTER TABLE `pms_tag`
  ADD PRIMARY KEY (`id`);


ALTER TABLE `pms_tasks`
  ADD PRIMARY KEY (`tskid`),
  ADD KEY `tasks_users_createdby_fk` (`created_by`),
  ADD KEY `tasks_users_updatedby_fk` (`updated_by`),
  ADD KEY `tasks_projects_fk` (`project_id`),
  ADD KEY `tasks_status_fk` (`status`),
  ADD KEY `tasks_users_assinged_to_fk` (`assigned_to`),
  ADD KEY `tasks_users_report_to_fk` (`report_to`),
  ADD KEY `tasks_priority_fk` (`priority`),
  ADD KEY `tasks_billable_status_fk` (`billable`),
  ADD KEY `tasks_users_coordinator_fk` (`coordinator`),
  ADD KEY `work_type_id` (`work_type_id`),
  ADD KEY `contractor_id` (`contractor_id`),
  ADD KEY `area` (`area`),
  ADD KEY `milestone_id` (`milestone_id`);


ALTER TABLE `pms_task_dependancy`
  ADD PRIMARY KEY (`id`),
  ADD KEY `task_id` (`task_id`),
  ADD KEY `dependant_task_id` (`dependant_task_id`);


ALTER TABLE `pms_task_expiry`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);


ALTER TABLE `pms_task_item_estimation`
  ADD PRIMARY KEY (`estimation_id`),
  ADD KEY `task_id` (`task_id`),
  ADD KEY `template_id` (`template_id`),
  ADD KEY `item_id` (`item_id`);


ALTER TABLE `pms_template`
  ADD PRIMARY KEY (`template_id`),
  ADD KEY `pms_template_ibfk_1` (`created_by`);


ALTER TABLE `pms_template_items`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `pms_time_attendance`
  ADD PRIMARY KEY (`att_id`),
  ADD UNIQUE KEY `user_id_2` (`user_id`,`att_date`),
  ADD KEY `att_entry` (`att_time`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `att_entry_2` (`att_entry`),
  ADD KEY `created_by` (`created_by`);


ALTER TABLE `pms_time_attendance_delete_log`
  ADD PRIMARY KEY (`delete_log_id`),
  ADD KEY `att_entry` (`att_entry`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `user_id` (`user_id`);


ALTER TABLE `pms_time_entry`
  ADD PRIMARY KEY (`teid`),
  ADD KEY `time_users_createdby_fk` (`created_by`),
  ADD KEY `time_users_updatedby_fk` (`updated_by`),
  ADD KEY `time_users_userid_fk` (`user_id`),
  ADD KEY `tasks_task_fk` (`tskid`),
  ADD KEY `tasks_work_type_fk` (`work_type`);


ALTER TABLE `pms_unit`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);


ALTER TABLE `pms_user`
  ADD PRIMARY KEY (`id`);


ALTER TABLE `pms_users`
  ADD PRIMARY KEY (`userid`),
  ADD UNIQUE KEY `username` (`username`,`email`),
  ADD UNIQUE KEY `employee_id` (`employee_id`),
  ADD KEY `users_type_fk` (`user_type`),
  ADD KEY `user_report_to` (`reporting_person`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);


ALTER TABLE `pms_usersite`
  ADD PRIMARY KEY (`id`),
  ADD KEY `usersite_site_id_fk` (`site_id`),
  ADD KEY `usersite_userid_fk` (`user_id`);


ALTER TABLE `pms_user_roles`
  ADD PRIMARY KEY (`id`);


ALTER TABLE `pms_work_type`
  ADD PRIMARY KEY (`wtid`),
  ADD KEY `unit_id` (`unit_id`);


ALTER TABLE `task_log`
  ADD PRIMARY KEY (`tskid`,`revision`),
  ADD KEY `tasks_users_createdby_fk` (`created_by`),
  ADD KEY `tasks_users_updatedby_fk` (`updated_by`),
  ADD KEY `tasks_projects_fk` (`project_id`),
  ADD KEY `tasks_status_fk` (`status`),
  ADD KEY `tasks_users_assinged_to_fk` (`assigned_to`),
  ADD KEY `tasks_users_report_to_fk` (`report_to`),
  ADD KEY `tasks_priority_fk` (`priority`),
  ADD KEY `tasks_billable_status_fk` (`billable`),
  ADD KEY `tasks_users_coordinator_fk` (`coordinator`),
  ADD KEY `work_type_id` (`work_type_id`),
  ADD KEY `contractor_id` (`contractor_id`);


ALTER TABLE `pms_accesslog`
  MODIFY `logid` double NOT NULL AUTO_INCREMENT;


ALTER TABLE `pms_accesslog_last_sync`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `pms_account_permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;


ALTER TABLE `pms_acc_project_mapping`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;


ALTER TABLE `pms_acc_wpr_item_consumed`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;


ALTER TABLE `pms_area`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;


ALTER TABLE `pms_attendance`
  MODIFY `att_id` int(11) NOT NULL AUTO_INCREMENT;


ALTER TABLE `pms_attendance_delete_log`
  MODIFY `delete_log_id` int(11) NOT NULL AUTO_INCREMENT;


ALTER TABLE `pms_attendance_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT;


ALTER TABLE `pms_attendance_reject_log`
  MODIFY `att_id` int(11) NOT NULL AUTO_INCREMENT;


ALTER TABLE `pms_attendance_requests`
  MODIFY `att_id` int(11) NOT NULL AUTO_INCREMENT;


ALTER TABLE `pms_attendance_settings`
  MODIFY `settings_id` int(11) NOT NULL AUTO_INCREMENT;


ALTER TABLE `pms_avail_compoff`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;


ALTER TABLE `pms_avail_leave`
  MODIFY `avail_leave_id` int(11) NOT NULL AUTO_INCREMENT;


ALTER TABLE `pms_balance_leave`
  MODIFY `avail_leave_id` int(11) NOT NULL AUTO_INCREMENT;


ALTER TABLE `pms_biddings`
  MODIFY `bid` int(11) NOT NULL AUTO_INCREMENT;


ALTER TABLE `pms_calender_events`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;


ALTER TABLE `pms_clients`
  MODIFY `cid` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `pms_clientsite`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `pms_comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

ALTER TABLE `pms_company`
  MODIFY `company_id` int(11) NOT NULL AUTO_INCREMENT;


ALTER TABLE `pms_contractors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;


ALTER TABLE `pms_cron`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `pms_current_task_status`
  MODIFY `ctsid` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `pms_custompunch`
  MODIFY `customid` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `pms_daily_overtime`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `pms_daily_sevice_entries`
  MODIFY `entry_id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `pms_daily_work`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `pms_daily_work_progress`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `pms_department`
  MODIFY `department_id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `pms_device_accessids`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `pms_earned_leave`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `pms_electricity`
  MODIFY `eid` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `pms_employeerequest`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `pms_employee_default_data`
  MODIFY `default_data_id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `pms_employee_group`
  MODIFY `group_id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `pms_employee_shift`
  MODIFY `shift_id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `pms_groups`
  MODIFY `group_id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `pms_groups_members`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `pms_group_leaders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `pms_holidays`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `pms_ignore_punches`
  MODIFY `ig_id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `pms_invoice`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `pms_invoiced_entry`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `pms_issues`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `pms_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `pms_lead`
  MODIFY `lead_id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `pms_lead_activity`
  MODIFY `activity_id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `pms_lead_contact`
  MODIFY `cid` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `pms_leave`
  MODIFY `leave_id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `pms_leave_credit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `pms_leave_cron`
  MODIFY `cron_id` int(10) NOT NULL AUTO_INCREMENT;

ALTER TABLE `pms_leave_day`
  MODIFY `leave_day_id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `pms_leave_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `pms_leave_rules`
  MODIFY `rule_id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `pms_leave_setting`
  MODIFY `leave_setting_id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `pms_leave_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `pms_legends`
  MODIFY `leg_id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `pms_lookup`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

ALTER TABLE `pms_mail_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT;


ALTER TABLE `pms_mail_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;


ALTER TABLE `pms_mail_template`
  MODIFY `temp_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;


ALTER TABLE `pms_manualentry`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;


ALTER TABLE `pms_manualentry_date`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;


ALTER TABLE `pms_manual_entry`
  MODIFY `entry_id` int(11) NOT NULL AUTO_INCREMENT;


ALTER TABLE `pms_manual_gen_attendance_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;


ALTER TABLE `pms_material_requisition`
  MODIFY `requisition_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;


ALTER TABLE `pms_material_requisition_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;


ALTER TABLE `pms_meeting_detailed_reports`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;


ALTER TABLE `pms_meeting_minutes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;


ALTER TABLE `pms_meeting_minutes_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;


ALTER TABLE `pms_meeting_participants`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;


ALTER TABLE `pms_menu`
  MODIFY `menu_id` int(20) NOT NULL AUTO_INCREMENT;


ALTER TABLE `pms_menu_permissions`
  MODIFY `mp_id` int(20) NOT NULL AUTO_INCREMENT;


ALTER TABLE `pms_milestone`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;


ALTER TABLE `pms_ot_attendance`
  MODIFY `att_id` int(11) NOT NULL AUTO_INCREMENT;


ALTER TABLE `pms_ot_attendance_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT;


ALTER TABLE `pms_ot_report`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;


ALTER TABLE `pms_payments`
  MODIFY `payid` int(11) NOT NULL AUTO_INCREMENT;


ALTER TABLE `pms_payment_report`
  MODIFY `pmntid` int(11) NOT NULL AUTO_INCREMENT;


ALTER TABLE `pms_pay_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;


ALTER TABLE `pms_pay_list_entry`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;


ALTER TABLE `pms_photopunch`
  MODIFY `aid` double NOT NULL AUTO_INCREMENT;


ALTER TABLE `pms_post`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;


ALTER TABLE `pms_profiles`
  MODIFY `pid` int(11) NOT NULL AUTO_INCREMENT;


ALTER TABLE `pms_profile_menu_settings`
  MODIFY `mp_id` int(20) NOT NULL AUTO_INCREMENT;


ALTER TABLE `pms_projects`
  MODIFY `pid` int(11) NOT NULL AUTO_INCREMENT;


ALTER TABLE `pms_project_comments`
  MODIFY `comment_id` int(20) NOT NULL AUTO_INCREMENT;


ALTER TABLE `pms_project_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;


ALTER TABLE `pms_project_report`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;


ALTER TABLE `pms_project_type`
  MODIFY `ptid` smallint(6) NOT NULL AUTO_INCREMENT;


ALTER TABLE `pms_project_work_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;


ALTER TABLE `pms_punching_devices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;


ALTER TABLE `pms_punchreport`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `pms_punch_log`
  MODIFY `aid` double NOT NULL AUTO_INCREMENT;


ALTER TABLE `pms_punch_request`
  MODIFY `aid` double NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pms_punch_settings`
--
ALTER TABLE `pms_punch_settings`
  MODIFY `setting_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pms_report_images`
--
ALTER TABLE `pms_report_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pms_role_settings`
--
ALTER TABLE `pms_role_settings`
  MODIFY `rset_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pms_shift_assign`
--
ALTER TABLE `pms_shift_assign`
  MODIFY `assign_shift_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pms_shift_changes`
--
ALTER TABLE `pms_shift_changes`
  MODIFY `change_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pms_shift_entry`
--
ALTER TABLE `pms_shift_entry`
  MODIFY `shiftentry_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pms_shift_timings`
--
ALTER TABLE `pms_shift_timings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pms_site_meetings`
--
ALTER TABLE `pms_site_meetings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pms_status`
--
ALTER TABLE `pms_status`
  MODIFY `sid` smallint(6) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pms_tag`
--
ALTER TABLE `pms_tag`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `pms_tasks`
--
ALTER TABLE `pms_tasks`
  MODIFY `tskid` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pms_task_dependancy`
--
ALTER TABLE `pms_task_dependancy`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pms_task_expiry`
--
ALTER TABLE `pms_task_expiry`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pms_task_item_estimation`
--
ALTER TABLE `pms_task_item_estimation`
  MODIFY `estimation_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pms_template`
--
ALTER TABLE `pms_template`
  MODIFY `template_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pms_template_items`
--
ALTER TABLE `pms_template_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pms_time_attendance`
--
ALTER TABLE `pms_time_attendance`
  MODIFY `att_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pms_time_attendance_delete_log`
--
ALTER TABLE `pms_time_attendance_delete_log`
  MODIFY `delete_log_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pms_time_entry`
--
ALTER TABLE `pms_time_entry`
  MODIFY `teid` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pms_unit`
--
ALTER TABLE `pms_unit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pms_user`
--
ALTER TABLE `pms_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `pms_users`
--
ALTER TABLE `pms_users`
  MODIFY `userid` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pms_usersite`
--
ALTER TABLE `pms_usersite`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pms_user_roles`
--
ALTER TABLE `pms_user_roles`
  MODIFY `id` tinyint(1) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pms_work_type`
--
ALTER TABLE `pms_work_type`
  MODIFY `wtid` smallint(6) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `task_log`
--
ALTER TABLE `task_log`
  MODIFY `revision` int(6) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `pms_area`
--
ALTER TABLE `pms_area`
  ADD CONSTRAINT `pms_area_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_area_ibfk_2` FOREIGN KEY (`project_id`) REFERENCES `pms_projects` (`pid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_area_ibfk_3` FOREIGN KEY (`updated_by`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE;

--
-- Constraints for table `pms_attendance`
--
ALTER TABLE `pms_attendance`
  ADD CONSTRAINT `pms_attendance_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `pms_users` (`userid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_attendance_ibfk_2` FOREIGN KEY (`att_entry`) REFERENCES `pms_legends` (`leg_id`) ON UPDATE CASCADE;

--
-- Constraints for table `pms_attendance_delete_log`
--
ALTER TABLE `pms_attendance_delete_log`
  ADD CONSTRAINT `pms_attendance_delete_log_ibfk_1` FOREIGN KEY (`att_entry`) REFERENCES `pms_legends` (`leg_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_attendance_delete_log_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `pms_users` (`userid`) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT `pms_attendance_delete_log_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `pms_users` (`userid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pms_attendance_log`
--
ALTER TABLE `pms_attendance_log`
  ADD CONSTRAINT `pms_attendance_log_ibfk_1` FOREIGN KEY (`att_entry`) REFERENCES `pms_legends` (`leg_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_attendance_log_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_attendance_log_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `pms_users` (`userid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pms_attendance_reject_log`
--
ALTER TABLE `pms_attendance_reject_log`
  ADD CONSTRAINT `pms_attendance_reject_log_ibfk_1` FOREIGN KEY (`att_entry`) REFERENCES `pms_legends` (`leg_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_attendance_reject_log_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `pms_users` (`userid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pms_attendance_requests`
--
ALTER TABLE `pms_attendance_requests`
  ADD CONSTRAINT `pms_attendance_requests_ibfk_1` FOREIGN KEY (`att_entry`) REFERENCES `pms_legends` (`leg_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_attendance_requests_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_attendance_requests_ibfk_3` FOREIGN KEY (`decision_by`) REFERENCES `pms_users` (`userid`) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT `pms_attendance_requests_ibfk_4` FOREIGN KEY (`user_id`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE;

--
-- Constraints for table `pms_avail_compoff`
--
ALTER TABLE `pms_avail_compoff`
  ADD CONSTRAINT `pms_avail_compoff_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_avail_compoff_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_avail_compoff_ibfk_3` FOREIGN KEY (`userid`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE;

--
-- Constraints for table `pms_avail_leave`
--
ALTER TABLE `pms_avail_leave`
  ADD CONSTRAINT `pms_avail_leave_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_avail_leave_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_avail_leave_ibfk_3` FOREIGN KEY (`userid`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE;

--
-- Constraints for table `pms_balance_leave`
--
ALTER TABLE `pms_balance_leave`
  ADD CONSTRAINT `pms_balance_leave_ibfk_1` FOREIGN KEY (`modified_by`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_balance_leave_ibfk_2` FOREIGN KEY (`userid`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE;

--
-- Constraints for table `pms_biddings`
--
ALTER TABLE `pms_biddings`
  ADD CONSTRAINT `pms_biddings_ibfk_1` FOREIGN KEY (`bid_by`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_biddings_ibfk_2` FOREIGN KEY (`profile`) REFERENCES `pms_profiles` (`pid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_biddings_ibfk_3` FOREIGN KEY (`status`) REFERENCES `pms_status` (`sid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_biddings_ibfk_4` FOREIGN KEY (`worktype`) REFERENCES `pms_status` (`sid`) ON UPDATE CASCADE;

ALTER TABLE `pms_calender_events`
  ADD CONSTRAINT `pms_calender_events_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_calender_events_ibfk_2` FOREIGN KEY (`site_id`) REFERENCES `pms_clientsite` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_calender_events_ibfk_3` FOREIGN KEY (`updated_by`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE;


ALTER TABLE `pms_clients`
  ADD CONSTRAINT `pms_clients_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_clients_ibfk_2` FOREIGN KEY (`project_type`) REFERENCES `pms_project_type` (`ptid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_clients_ibfk_3` FOREIGN KEY (`status`) REFERENCES `pms_status` (`sid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_clients_ibfk_4` FOREIGN KEY (`updated_by`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE;


ALTER TABLE `pms_clientsite`
  ADD CONSTRAINT `pms_clientsite_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_clientsite_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE;

ALTER TABLE `pms_clientsite_assigned`
  ADD CONSTRAINT `pms_clientsite_assigned_ibfk_1` FOREIGN KEY (`site_id`) REFERENCES `pms_clientsite` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_clientsite_assigned_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `pms_users` (`userid`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `pms_company`
  ADD CONSTRAINT `pms_company_ibfk_1` FOREIGN KEY (`active_status`) REFERENCES `pms_status` (`sid`) ON DELETE SET NULL ON UPDATE CASCADE;


ALTER TABLE `pms_contractors`
  ADD CONSTRAINT `pms_contractors_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_contractors_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE;


ALTER TABLE `pms_current_task_status`
  ADD CONSTRAINT `pms_current_task_status_ibfk_1` FOREIGN KEY (`taskid`) REFERENCES `pms_tasks` (`tskid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_current_task_status_ibfk_2` FOREIGN KEY (`userid`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE;


ALTER TABLE `pms_custompunch`
  ADD CONSTRAINT `pms_custompunch_ibfk_1` FOREIGN KEY (`createdby`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_custompunch_ibfk_2` FOREIGN KEY (`device`) REFERENCES `pms_punching_devices` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_custompunch_ibfk_3` FOREIGN KEY (`resource`) REFERENCES `pms_users` (`userid`) ON DELETE CASCADE ON UPDATE CASCADE;


ALTER TABLE `pms_daily_overtime`
  ADD CONSTRAINT `pms_daily_overtime_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_daily_overtime_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_daily_overtime_ibfk_3` FOREIGN KEY (`userid`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE;


ALTER TABLE `pms_daily_sevice_entries`
  ADD CONSTRAINT `pms_daily_sevice_entries_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_daily_sevice_entries_ibfk_2` FOREIGN KEY (`project_id`) REFERENCES `pms_projects` (`pid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pms_daily_work`
--
ALTER TABLE `pms_daily_work`
  ADD CONSTRAINT `pms_daily_work_ibfk_1` FOREIGN KEY (`assigned_to`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_daily_work_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_daily_work_ibfk_3` FOREIGN KEY (`item_id`) REFERENCES `pms_items` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_daily_work_ibfk_4` FOREIGN KEY (`project_id`) REFERENCES `pms_projects` (`pid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_daily_work_ibfk_5` FOREIGN KEY (`taskid`) REFERENCES `pms_tasks` (`tskid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_daily_work_ibfk_6` FOREIGN KEY (`work_type`) REFERENCES `pms_work_type` (`wtid`) ON UPDATE CASCADE;

--
-- Constraints for table `pms_daily_work_progress`
--
ALTER TABLE `pms_daily_work_progress`
  ADD CONSTRAINT `pms_daily_work_progress_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `pms_users` (`userid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_daily_work_progress_ibfk_2` FOREIGN KEY (`current_status`) REFERENCES `pms_status` (`sid`) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT `pms_daily_work_progress_ibfk_3` FOREIGN KEY (`item_id`) REFERENCES `pms_items` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_daily_work_progress_ibfk_4` FOREIGN KEY (`project_id`) REFERENCES `pms_projects` (`pid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_daily_work_progress_ibfk_5` FOREIGN KEY (`site_id`) REFERENCES `pms_clientsite` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_daily_work_progress_ibfk_6` FOREIGN KEY (`template_item_id`) REFERENCES `pms_template_items` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pms_device_accessids`
--
ALTER TABLE `pms_device_accessids`
  ADD CONSTRAINT `pms_device_accessids_ibfk_1` FOREIGN KEY (`userid`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE;

--
-- Constraints for table `pms_earned_leave`
--
ALTER TABLE `pms_earned_leave`
  ADD CONSTRAINT `pms_earned_leave_ibfk_1` FOREIGN KEY (`action_by`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_earned_leave_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_earned_leave_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `pms_users` (`userid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pms_employeerequest`
--
ALTER TABLE `pms_employeerequest`
  ADD CONSTRAINT `pms_employeerequest_ibfk_1` FOREIGN KEY (`ack_by`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_employeerequest_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE;

--
-- Constraints for table `pms_employee_shift`
--
ALTER TABLE `pms_employee_shift`
  ADD CONSTRAINT `pms_employee_shift_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_employee_shift_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE;

--
-- Constraints for table `pms_general_settings`
--
ALTER TABLE `pms_general_settings`
  ADD CONSTRAINT `pms_general_settings_ibfk_1` FOREIGN KEY (`updated_by`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE;

--
-- Constraints for table `pms_groups`
--
ALTER TABLE `pms_groups`
  ADD CONSTRAINT `pms_groups_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_groups_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE;

--
-- Constraints for table `pms_groups_members`
--
ALTER TABLE `pms_groups_members`
  ADD CONSTRAINT `pms_groups_members_ibfk_1` FOREIGN KEY (`group_id`) REFERENCES `pms_groups` (`group_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_groups_members_ibfk_2` FOREIGN KEY (`group_members`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE;

--
-- Constraints for table `pms_group_leaders`
--
ALTER TABLE `pms_group_leaders`
  ADD CONSTRAINT `pms_group_leaders_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE;

--
-- Constraints for table `pms_holidays`
--
ALTER TABLE `pms_holidays`
  ADD CONSTRAINT `pms_holidays_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `pms_users` (`userid`) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT `pms_holidays_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `pms_users` (`userid`) ON DELETE SET NULL ON UPDATE SET NULL;

--
-- Constraints for table `pms_ignore_punches`
--
ALTER TABLE `pms_ignore_punches`
  ADD CONSTRAINT `pms_ignore_punches_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_ignore_punches_ibfk_2` FOREIGN KEY (`decision_by`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_ignore_punches_ibfk_3` FOREIGN KEY (`userid`) REFERENCES `pms_users` (`userid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pms_invoice`
--
ALTER TABLE `pms_invoice`
  ADD CONSTRAINT `pms_invoice_ibfk_1` FOREIGN KEY (`client_id`) REFERENCES `pms_clients` (`cid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_invoice_ibfk_2` FOREIGN KEY (`generated_by`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_invoice_ibfk_3` FOREIGN KEY (`status`) REFERENCES `pms_status` (`sid`) ON UPDATE CASCADE;

--
-- Constraints for table `pms_invoiced_entry`
--
ALTER TABLE `pms_invoiced_entry`
  ADD CONSTRAINT `pms_invoiced_entry_ibfk_1` FOREIGN KEY (`generated_by`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_invoiced_entry_ibfk_2` FOREIGN KEY (`invoice_id`) REFERENCES `pms_invoice` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_invoiced_entry_ibfk_3` FOREIGN KEY (`teid`) REFERENCES `pms_time_entry` (`teid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_invoiced_entry_ibfk_4` FOREIGN KEY (`work_type`) REFERENCES `pms_work_type` (`wtid`) ON UPDATE CASCADE;

--
-- Constraints for table `pms_issues`
--
ALTER TABLE `pms_issues`
  ADD CONSTRAINT `pms_issues_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_issues_ibfk_2` FOREIGN KEY (`status`) REFERENCES `pms_status` (`sid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_issues_ibfk_3` FOREIGN KEY (`updated_by`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE;

--
-- Constraints for table `pms_items`
--
ALTER TABLE `pms_items`
  ADD CONSTRAINT `pms_items_ibfk_1` FOREIGN KEY (`projectid`) REFERENCES `pms_projects` (`pid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_items_ibfk_2` FOREIGN KEY (`taskid`) REFERENCES `pms_tasks` (`tskid`) ON UPDATE CASCADE;

--
-- Constraints for table `pms_lead`
--
ALTER TABLE `pms_lead`
  ADD CONSTRAINT `pms_lead_ibfk_1` FOREIGN KEY (`country`) REFERENCES `pms_country` (`Code`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_lead_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_lead_ibfk_3` FOREIGN KEY (`industry`) REFERENCES `pms_status` (`sid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_lead_ibfk_4` FOREIGN KEY (`lead_title`) REFERENCES `pms_status` (`sid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_lead_ibfk_5` FOREIGN KEY (`opportunity_type`) REFERENCES `pms_status` (`sid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_lead_ibfk_6` FOREIGN KEY (`priority`) REFERENCES `pms_status` (`sid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_lead_ibfk_7` FOREIGN KEY (`status`) REFERENCES `pms_status` (`sid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_lead_ibfk_8` FOREIGN KEY (`updated_by`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE;


ALTER TABLE `pms_lead_activity`
  ADD CONSTRAINT `pms_lead_activity_ibfk_1` FOREIGN KEY (`followup`) REFERENCES `pms_status` (`sid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_lead_activity_ibfk_2` FOREIGN KEY (`activity_type`) REFERENCES `pms_status` (`sid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_lead_activity_ibfk_3` FOREIGN KEY (`contacted_person`) REFERENCES `pms_lead_contact` (`cid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_lead_activity_ibfk_4` FOREIGN KEY (`created_by`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_lead_activity_ibfk_5` FOREIGN KEY (`lead_id`) REFERENCES `pms_lead` (`lead_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_lead_activity_ibfk_6` FOREIGN KEY (`updated_by`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE;


ALTER TABLE `pms_lead_contact`
  ADD CONSTRAINT `pms_lead_contact_ibfk_1` FOREIGN KEY (`lead_id`) REFERENCES `pms_lead` (`lead_id`) ON DELETE CASCADE ON UPDATE CASCADE;


ALTER TABLE `pms_leave`
  ADD CONSTRAINT `pms_leave_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_leave_ibfk_2` FOREIGN KEY (`decision_by`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_leave_ibfk_3` FOREIGN KEY (`emp_id`) REFERENCES `pms_users` (`userid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_leave_ibfk_4` FOREIGN KEY (`reporting_person`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_leave_ibfk_5` FOREIGN KEY (`updated_by`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE;


ALTER TABLE `pms_leave_credit`
  ADD CONSTRAINT `pms_leave_credit_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE;


ALTER TABLE `pms_leave_day`
  ADD CONSTRAINT `pms_leave_day_ibfk_1` FOREIGN KEY (`leave_id`) REFERENCES `pms_leave` (`leave_id`) ON DELETE CASCADE ON UPDATE CASCADE;


ALTER TABLE `pms_leave_log`
  ADD CONSTRAINT `pms_leave_log_ibfk_1` FOREIGN KEY (`comp_id`) REFERENCES `pms_company` (`company_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_leave_log_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_leave_log_ibfk_3` FOREIGN KEY (`leave_id`) REFERENCES `pms_leave` (`leave_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_leave_log_ibfk_4` FOREIGN KEY (`userid`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE;


ALTER TABLE `pms_leave_types`
  ADD CONSTRAINT `pms_leave_types_ibfk_1` FOREIGN KEY (`leave_id`) REFERENCES `pms_legends` (`leg_id`) ON UPDATE CASCADE;


ALTER TABLE `pms_mail_log`
  ADD CONSTRAINT `pms_mail_log_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `pms_users` (`userid`) ON DELETE CASCADE ON UPDATE CASCADE;


ALTER TABLE `pms_mail_settings`
  ADD CONSTRAINT `pms_mail_settings_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `pms_user_roles` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_mail_settings_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `pms_users` (`userid`) ON DELETE CASCADE ON UPDATE CASCADE;


ALTER TABLE `pms_manualentry`
  ADD CONSTRAINT `pms_manualentry_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_manualentry_ibfk_2` FOREIGN KEY (`manual_date_id`) REFERENCES `pms_manualentry_date` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_manualentry_ibfk_3` FOREIGN KEY (`site_id`) REFERENCES `pms_clientsite` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_manualentry_ibfk_4` FOREIGN KEY (`updated_by`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_manualentry_ibfk_5` FOREIGN KEY (`user_id`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE;


ALTER TABLE `pms_manualentry_date`
  ADD CONSTRAINT `pms_manualentry_date_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_manualentry_date_ibfk_2` FOREIGN KEY (`site_id`) REFERENCES `pms_clientsite` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_manualentry_date_ibfk_3` FOREIGN KEY (`updated_by`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE;


ALTER TABLE `pms_manual_entry`
  ADD CONSTRAINT `pms_manual_entry_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_manual_entry_ibfk_2` FOREIGN KEY (`decision_by`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_manual_entry_ibfk_3` FOREIGN KEY (`emp_id`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE;


ALTER TABLE `pms_manual_gen_attendance_log`
  ADD CONSTRAINT `pms_manual_gen_attendance_log_ibfk_1` FOREIGN KEY (`gen_by`) REFERENCES `pms_users` (`userid`) ON DELETE SET NULL ON UPDATE SET NULL;


ALTER TABLE `pms_material_requisition`
  ADD CONSTRAINT `pms_material_requisition_ibfk_1` FOREIGN KEY (`requisition_project_id`) REFERENCES `pms_projects` (`pid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_material_requisition_ibfk_2` FOREIGN KEY (`requisition_task_id`) REFERENCES `pms_tasks` (`tskid`) ON UPDATE CASCADE;


ALTER TABLE `pms_meeting_minutes`
  ADD CONSTRAINT `pms_meeting_minutes_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_meeting_minutes_ibfk_2` FOREIGN KEY (`meeting_id`) REFERENCES `pms_site_meetings` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_meeting_minutes_ibfk_3` FOREIGN KEY (`updated_by`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE;


ALTER TABLE `pms_meeting_minutes_users`
  ADD CONSTRAINT `pms_meeting_minutes_users_ibfk_1` FOREIGN KEY (`meeting_minutes_id`) REFERENCES `pms_meeting_minutes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_meeting_minutes_users_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE;


ALTER TABLE `pms_menu_permissions`
  ADD CONSTRAINT `pms_menu_permissions_ibfk_1` FOREIGN KEY (`menu_id`) REFERENCES `pms_menu` (`menu_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_menu_permissions_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `pms_users` (`userid`) ON DELETE CASCADE ON UPDATE CASCADE;


ALTER TABLE `pms_milestone`
  ADD CONSTRAINT `pms_milestone_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_milestone_ibfk_2` FOREIGN KEY (`project_id`) REFERENCES `pms_projects` (`pid`) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT `pms_milestone_ibfk_3` FOREIGN KEY (`updated_by`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE;

ALTER TABLE `pms_ot_attendance`
  ADD CONSTRAINT `pms_ot_attendance_ibfk_1` FOREIGN KEY (`att_entry`) REFERENCES `pms_legends` (`leg_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_ot_attendance_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `pms_users` (`userid`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `pms_ot_attendance_log`
  ADD CONSTRAINT `pms_ot_attendance_log_ibfk_1` FOREIGN KEY (`att_entry`) REFERENCES `pms_legends` (`leg_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_ot_attendance_log_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `pms_users` (`userid`) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT `pms_ot_attendance_log_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `pms_users` (`userid`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `pms_ot_report`
  ADD CONSTRAINT `pms_ot_report_ibfk_1` FOREIGN KEY (`updated_by`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_ot_report_ibfk_2` FOREIGN KEY (`userid`) REFERENCES `pms_users` (`userid`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `pms_payments`
  ADD CONSTRAINT `pms_payments_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_payments_ibfk_2` FOREIGN KEY (`modified_by`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_payments_ibfk_3` FOREIGN KEY (`pay_status`) REFERENCES `pms_status` (`sid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_payments_ibfk_4` FOREIGN KEY (`projid`) REFERENCES `pms_projects` (`pid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_payments_ibfk_5` FOREIGN KEY (`userid`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE;

ALTER TABLE `pms_payment_report`
  ADD CONSTRAINT `pms_payment_report_ibfk_1` FOREIGN KEY (`clntid`) REFERENCES `pms_clients` (`cid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_payment_report_ibfk_2` FOREIGN KEY (`invid`) REFERENCES `pms_invoice` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_payment_report_ibfk_3` FOREIGN KEY (`pjctid`) REFERENCES `pms_projects` (`pid`) ON UPDATE CASCADE;

ALTER TABLE `pms_pay_list`
  ADD CONSTRAINT `pms_pay_list_ibfk_1` FOREIGN KEY (`client_id`) REFERENCES `pms_clients` (`cid`) ON UPDATE CASCADE;

ALTER TABLE `pms_pay_list_entry`
  ADD CONSTRAINT `pms_pay_list_entry_ibfk_1` FOREIGN KEY (`generated_by`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_pay_list_entry_ibfk_2` FOREIGN KEY (`inv_id`) REFERENCES `pms_invoice` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_pay_list_entry_ibfk_3` FOREIGN KEY (`pay_id`) REFERENCES `pms_payments` (`payid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_pay_list_entry_ibfk_4` FOREIGN KEY (`teid`) REFERENCES `pms_time_entry` (`teid`) ON UPDATE CASCADE;

ALTER TABLE `pms_photopunch`
  ADD CONSTRAINT `pms_photopunch_ibfk_1` FOREIGN KEY (`decision_by`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_photopunch_ibfk_2` FOREIGN KEY (`empid`) REFERENCES `pms_users` (`userid`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `pms_profile_menu_settings`
  ADD CONSTRAINT `pms_profile_menu_settings_ibfk_1` FOREIGN KEY (`menu_id`) REFERENCES `pms_menu` (`menu_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_profile_menu_settings_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `pms_user_roles` (`id`) ON UPDATE CASCADE;

ALTER TABLE `pms_projects`
  ADD CONSTRAINT `pms_projects_ibfk_1` FOREIGN KEY (`billable`) REFERENCES `pms_status` (`sid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_projects_ibfk_2` FOREIGN KEY (`client_id`) REFERENCES `pms_clients` (`cid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_projects_ibfk_3` FOREIGN KEY (`created_by`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_projects_ibfk_4` FOREIGN KEY (`status`) REFERENCES `pms_status` (`sid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_projects_ibfk_5` FOREIGN KEY (`updated_by`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE;

ALTER TABLE `pms_project_comments`
  ADD CONSTRAINT `pms_project_comments_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `pms_users` (`userid`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `pms_project_log`
  ADD CONSTRAINT `pms_project_log_ibfk_1` FOREIGN KEY (`billable`) REFERENCES `pms_status` (`sid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_project_log_ibfk_2` FOREIGN KEY (`pid`) REFERENCES `pms_projects` (`pid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_project_log_ibfk_3` FOREIGN KEY (`client_id`) REFERENCES `pms_clients` (`cid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_project_log_ibfk_4` FOREIGN KEY (`created_by`) REFERENCES `pms_users` (`userid`),
  ADD CONSTRAINT `pms_project_log_ibfk_5` FOREIGN KEY (`updated_by`) REFERENCES `pms_users` (`userid`);

ALTER TABLE `pms_project_report`
  ADD CONSTRAINT `pms_project_report_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_project_report_ibfk_2` FOREIGN KEY (`project_id`) REFERENCES `pms_projects` (`pid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_project_report_ibfk_3` FOREIGN KEY (`updated_by`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE;

ALTER TABLE `pms_project_work_type`
  ADD CONSTRAINT `pms_project_work_type_ibfk_1` FOREIGN KEY (`work_type_id`) REFERENCES `pms_work_type` (`wtid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_project_work_type_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_project_work_type_ibfk_3` FOREIGN KEY (`project_id`) REFERENCES `pms_projects` (`pid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_project_work_type_ibfk_4` FOREIGN KEY (`updated_by`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE;

ALTER TABLE `pms_punchreport`
  ADD CONSTRAINT `pms_punchreport_ibfk_1` FOREIGN KEY (`resource_id`) REFERENCES `pms_users` (`userid`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `pms_punch_request`
  ADD CONSTRAINT `pms_punch_request_ibfk_1` FOREIGN KEY (`work_site_id`) REFERENCES `pms_clientsite` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `pms_role_settings`
  ADD CONSTRAINT `pms_role_settings_ibfk_1` FOREIGN KEY (`updated_by`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_role_settings_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `pms_user_roles` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_role_settings_ibfk_3` FOREIGN KEY (`created_by`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE;


ALTER TABLE `pms_shift_assign`
  ADD CONSTRAINT `pms_shift_assign_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_shift_assign_ibfk_2` FOREIGN KEY (`modified_by`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_shift_assign_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `pms_users` (`userid`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `pms_shift_changes`
  ADD CONSTRAINT `pms_shift_changes_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_shift_changes_ibfk_2` FOREIGN KEY (`decision_by`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_shift_changes_ibfk_3` FOREIGN KEY (`emp1`) REFERENCES `pms_users` (`userid`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `pms_shift_entry`
  ADD CONSTRAINT `pms_shift_entry_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_shift_entry_ibfk_2` FOREIGN KEY (`decision_by`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_shift_entry_ibfk_3` FOREIGN KEY (`emp_id`) REFERENCES `pms_users` (`userid`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `pms_site_meetings`
  ADD CONSTRAINT `pms_site_meetings_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `pms_projects` (`pid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_site_meetings_ibfk_2` FOREIGN KEY (`site_id`) REFERENCES `pms_clientsite` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_site_meetings_ibfk_3` FOREIGN KEY (`created_by`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_site_meetings_ibfk_4` FOREIGN KEY (`updated_by`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE;

ALTER TABLE `pms_tasks`
  ADD CONSTRAINT `pms_tasks_ibfk_1` FOREIGN KEY (`area`) REFERENCES `pms_area` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_tasks_ibfk_10` FOREIGN KEY (`report_to`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_tasks_ibfk_11` FOREIGN KEY (`status`) REFERENCES `pms_status` (`sid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_tasks_ibfk_12` FOREIGN KEY (`updated_by`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_tasks_ibfk_13` FOREIGN KEY (`work_type_id`) REFERENCES `pms_work_type` (`wtid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_tasks_ibfk_2` FOREIGN KEY (`assigned_to`) REFERENCES `pms_users` (`userid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_tasks_ibfk_3` FOREIGN KEY (`billable`) REFERENCES `pms_status` (`sid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_tasks_ibfk_4` FOREIGN KEY (`contractor_id`) REFERENCES `pms_contractors` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_tasks_ibfk_5` FOREIGN KEY (`coordinator`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_tasks_ibfk_6` FOREIGN KEY (`created_by`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_tasks_ibfk_7` FOREIGN KEY (`milestone_id`) REFERENCES `pms_milestone` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_tasks_ibfk_8` FOREIGN KEY (`priority`) REFERENCES `pms_status` (`sid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_tasks_ibfk_9` FOREIGN KEY (`project_id`) REFERENCES `pms_projects` (`pid`) ON DELETE CASCADE ON UPDATE CASCADE;


ALTER TABLE `pms_task_dependancy`
  ADD CONSTRAINT `pms_task_dependancy_ibfk_1` FOREIGN KEY (`dependant_task_id`) REFERENCES `pms_tasks` (`tskid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_task_dependancy_ibfk_2` FOREIGN KEY (`task_id`) REFERENCES `pms_tasks` (`tskid`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `pms_task_expiry`
  ADD CONSTRAINT `pms_task_expiry_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_task_expiry_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE;

ALTER TABLE `pms_task_item_estimation`
  ADD CONSTRAINT `pms_task_item_estimation_ibfk_1` FOREIGN KEY (`task_id`) REFERENCES `pms_tasks` (`tskid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_task_item_estimation_ibfk_2` FOREIGN KEY (`template_id`) REFERENCES `pms_template` (`template_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_task_item_estimation_ibfk_3` FOREIGN KEY (`item_id`) REFERENCES `pms_template_items` (`id`) ON UPDATE CASCADE;

ALTER TABLE `pms_template`
  ADD CONSTRAINT `pms_template_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE;

ALTER TABLE `pms_time_attendance`
  ADD CONSTRAINT `pms_time_attendance_ibfk_1` FOREIGN KEY (`att_entry`) REFERENCES `pms_legends` (`leg_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_time_attendance_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `pms_users` (`userid`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `pms_time_attendance_delete_log`
  ADD CONSTRAINT `pms_time_attendance_delete_log_ibfk_1` FOREIGN KEY (`att_entry`) REFERENCES `pms_legends` (`leg_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_time_attendance_delete_log_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `pms_users` (`userid`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `pms_time_entry`
  ADD CONSTRAINT `pms_time_entry_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_time_entry_ibfk_2` FOREIGN KEY (`tskid`) REFERENCES `pms_tasks` (`tskid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_time_entry_ibfk_3` FOREIGN KEY (`updated_by`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_time_entry_ibfk_4` FOREIGN KEY (`user_id`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_time_entry_ibfk_5` FOREIGN KEY (`work_type`) REFERENCES `pms_work_type` (`wtid`) ON UPDATE CASCADE;

ALTER TABLE `pms_unit`
  ADD CONSTRAINT `pms_unit_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_unit_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `pms_users` (`userid`) ON DELETE SET NULL ON UPDATE SET NULL;

ALTER TABLE `pms_users`
  ADD CONSTRAINT `pms_users_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_users_ibfk_2` FOREIGN KEY (`reporting_person`) REFERENCES `pms_users` (`userid`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_users_ibfk_3` FOREIGN KEY (`updated_by`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_users_ibfk_4` FOREIGN KEY (`user_type`) REFERENCES `pms_user_roles` (`id`) ON UPDATE CASCADE;


ALTER TABLE `pms_usersite`
  ADD CONSTRAINT `pms_usersite_ibfk_1` FOREIGN KEY (`site_id`) REFERENCES `pms_clientsite` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pms_usersite_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE;


ALTER TABLE `pms_work_type`
  ADD CONSTRAINT `pms_work_type_ibfk_1` FOREIGN KEY (`unit_id`) REFERENCES `pms_unit` (`id`) ON UPDATE CASCADE;
COMMIT;
