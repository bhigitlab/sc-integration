<?php
/* @var $this BillsController */
/* @var $model Users */
/* @var $form CActiveForm */
?>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.min.js"></script>
<div class="page_filter clearfix">
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'action' => Yii::app()->createUrl($this->route),
        'method' => 'get',
    ));
    ?>

    <?php
    $tblpx = Yii::app()->db->tablePrefix;
    $user = Users::model()->findByPk(Yii::app()->user->id);
    $arrVal = explode(',', $user->company_id);
    $newQuery = "";
    foreach ($arrVal as $arr) {
        if ($newQuery)
            $newQuery .= ' OR';
        $newQuery .= " FIND_IN_SET('" . $arr . "', company_id)";
    }
    $bills_data = Yii::app()->db->createCommand("SELECT * FROM " . $tblpx . "defect_return WHERE (" . $newQuery . ") ORDER BY return_id desc")->queryAll();
    ?>
    <div class="row">
        <div class="form-group col-xs-12 col-sm-3">
            <label>Return No</label>
            <select id="DefecteReturn_return_number" class="select_box form-control" name="DefectReturn[return_number]">
                <option value="">Select Return No</option>
                <?php
                if (!empty($bills_data)) {
                    foreach ($bills_data as $key => $value) {
                        ?>
                        <option value="<?php echo $value['return_number'] ?>" <?php echo ($value['return_number'] == $model->return_number) ? 'selected' : ''; ?>>
                            <?php echo $value['return_number']; ?>
                        </option>
                    <?php }
                }
                ?>
            </select>
        </div>
        <div class="form-group col-xs-12 col-sm-3 text-sm-left text-right">
            <label class="d-sm-block d-none">&nbsp;</label>
            <div>
                <?php echo CHtml::submitButton('Go', array('class' => 'btn btn-info btn-sm')); ?>
                <?php echo CHtml::resetButton('Clear', array('class' => 'btn btn-default btn-sm', 'onclick' => 'javascript:location.href="' . $this->createUrl('warehouse/defectreturn') . '"')); ?>
            </div>
        </div>
    </div>
    <?php $this->endWidget(); ?>
</div>
<script>
    $(document).ready(function () {
        $(".select_box").select2();

    });
</script>