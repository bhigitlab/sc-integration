

    <div  id="container" >
        <div class="row addRow panel" style="padding:10px">
            <div class="row addRow row-margin">
                <div class="col-md-2">
                    <?php echo $form->labelEx($model, 'Work Type Label'); ?>
                    <?php echo $form->textField($model, 'worktype_label', array('class' => 'form-control worktypelabel common', 'name' => 'worktype_label[]')); ?>
                    <?php echo $form->error($model, 'worktype_label'); ?>
                </div>
            </div>
            <div class="row addRow row-margin">
                <div class="col-md-2">
                    <?php echo $form->labelEx($model, 'Shutter Work'); ?>
                </div>

                <div class="col-md-3">
                    <?php echo $form->dropDownList($model, 'id', CHtml::listData($items, 'id', 'data'), array('class' => 'form-control js-example-basic-multiple require shuttermaterial getmaster_rate', 'id' => 'shutter_material_1', 'name' => 'shutter_material[0][]', 'empty' => '-Select material-', 'style' => 'width:100%')); ?>
                    <?php echo $form->error($model, 'data'); ?>
                </div>
                <div class="col-md-3">
                    <?php echo $form->dropDownList($model, 'id', CHtml::listData(QuotationFinishMaster::model()->findAll(), 'id', 'name'), array('class' => 'form-control js-example-basic-single boxwork field_change require shutterfinish ', 'id' => 'shutter_finish_1', 'name' => 'shutter_finish[]', 'empty' => '-Select finish-', 'style' => 'width:100%')); ?>
                    <?php echo $form->error($model, 'name'); ?>
                </div>
                <div class="col-md-4">
                    <?php echo $form->textArea($model, 'shutterwork_description', array('class' => 'form-control boxwork shutterworkdesc', 'id' => 'shutterwork_description_1', 'name' => 'shutterwork_description[]', 'placeholder' => 'Description')); ?>
                    <?php echo $form->error($model, 'shutterwork_description'); ?>
                </div>
            </div>
            <div class="row addRow row-margin">
                <div class="col-md-2">
                    <?php echo $form->labelEx($model, 'Carcass Box Work'); ?>
                </div>
                <div class="col-md-3">
                    <?php echo $form->dropDownList($model, 'id', CHtml::listData($items, 'id', 'data'), array('class' => 'form-control js-example-basic-multiple require caracassmaterial', 'empty' => '-Select material-', 'multiple' => "multiple", 'id' => 'caracoss_material_1', 'name' => 'caracoss_material[0][]', 'style' => 'width:100%')); ?>
                    <?php echo $form->error($model, 'data'); ?>
                </div>

                <div class="col-md-3">
                    <?php echo $form->dropDownList($model, 'id', CHtml::listData(QuotationFinishMaster::model()->findAll(), 'id', 'name'), array('class' => 'form-control js-example-basic-single  boxwork field_change require caracassfinish', 'empty' => '-Select Finish-', 'id' => 'caracoss_finish_1', 'name' => 'caracoss_finish[]', 'style' => 'width:100%')); ?>
                    <?php echo $form->error($model, 'name'); ?>
                </div>
                <div class="col-md-4">
                    <?php echo $form->textArea($model, 'caracoss_description', array('class' => 'form-control boxwork carcassdesc', 'id' => 'caracoss_description_1', 'name' => 'caracoss_description[]', 'placeholder' => 'Description')); ?>
                    <?php echo $form->error($model, 'caracoss_description'); ?>
                </div>
            </div>
            <div class="row addRow row-margin">
                <div class="col-md-2">
                </div>
                <div class="col-md-3">
                    <?php echo $form->textField($model, 'rate', array('class' => 'form-control allownumericdecimal ratedata common ', 'placeholder' => 'Rate', 'name' => 'rate[]')); ?>
                    <?php echo $form->error($model, 'rate'); ?>
                </div>
                <div class="col-md-3">
                    <?php echo $form->textField($model, 'profit', array('class' => 'form-control allownumericdecimal profitdata common', 'name' => 'profit[]', 'placeholder' => 'Profit %')); ?>
                    <?php echo $form->error($model, 'profit'); ?>
                </div>
                <div class="col-md-3">
                    <?php echo $form->textField($model, 'discount', array('class' => 'form-control allownumericdecimal discountdata common', 'name' => 'discount[]', 'placeholder' => 'Discount %')); ?>
                    <?php echo $form->error($model, 'discount'); ?>
                </div>
            </div>
        </div>
    </div>


<div class="btn-holder">
    <p id="add_field"><a><span>&raquo; Add More</span></a></p>
    <p id="add_field_remove"><a><span>&raquo; Remove</span></a></p>
</div>