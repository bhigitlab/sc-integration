<?php
/* @var $this TasksController */
/* @var $model Tasks */

$this->breadcrumbs=array(
	'Tasks'=>array('index'),
	$model->title=>array('view','id'=>$model->title),
	'Update',
);

$this->menu=array(
	array('label'=>'List Tasks', 'url'=>array('index')),
	array('label'=>'Create Tasks', 'url'=>array('create')),
	array('label'=>'View Tasks', 'url'=>array('view', 'id'=>$model->tskid)),
	array('label'=>'Manage Tasks', 'url'=>array('admin')),
);
?>

<h2>Update Tasks <span><?php echo $model->title; ?></span></h2>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>