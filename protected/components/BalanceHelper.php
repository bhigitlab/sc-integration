<?php

class BalanceHelper {

    public static function cashInvoice($payment_type, $company_id, $employee_id, $bank_id, $date) {
        $result = '';
        $message = '';

        if (($payment_type != "88")) {
            $payment_type = "89";
        }

        try {
            $data = self::cashInvoiceCondition(
                            $payment_type,
                            $bank_id,
                            $company_id,
                            $date,
                            $employee_id
            );

            $daybook_income = Expenses::model()->find(array(
                'select' => 'SUM(receipt) as paidamount',
                'condition' => $data['expcondition'],
            ));

            $daybook_income = !empty($daybook_income['paidamount']) ?
                    $daybook_income['paidamount'] : '0';
            
            $dailyexpense_income = Dailyexpense::model()->find(array(
                'select' => 'SUM(dailyexpense_receipt) as dailyexpense_receipt',
                'condition' =>  $data['condition1'] 
                    . ' AND parent_status="1" '
                    . ' AND exp_type = 72 ' ,
            ));

            $dailyexpense_income = !empty($dailyexpense_income['dailyexpense_receipt']) ?
                    $dailyexpense_income['dailyexpense_receipt'] : '0';
                   // die($dailyexpense_income);
            $cash_balance_amount = Cashbalance::model()->find(array(
                'select' => 'SUM(cashbalance_opening_balance) '
                . 'as cashbalance_opening_balance,cashbalance_date',
                'condition' => $data['cashbal_condition'] 
                    .' AND company_id = ' . $company_id,
            ));

                          
            if($cash_balance_amount['cashbalance_date'] > $date){
                $cash_balance_amount = 0;
            }else{
                $cash_balance_amount = !empty($cash_balance_amount['cashbalance_opening_balance']) ?
                    $cash_balance_amount['cashbalance_opening_balance'] : '0';  
            }

            $total_invoice_amount = $daybook_income + $dailyexpense_income + $cash_balance_amount;

            $buyer_module = GeneralSettings::model()->checkBuyerModule();
            if ($buyer_module) {
                $buyer_transactions = BuyerTransactions::model()->findAll(array(
                    'condition' => 'transaction_type  IN(89,88)'
                ));
                foreach ($buyer_transactions as $buyer_transaction) {
                    if ($buyer_transaction['transaction_for'] == 1) {
                        $total_invoice_amount += $buyer_transaction['total_amount'];
                    }
                }
            }
            return $total_invoice_amount;
        } catch (Exception $error) {
            $message = $error->getMessage();
            return $message;
        }
    }

    public static function cashExpense($payment_type, $company_id, $employee_id, $entry_id, $bank_id, $date) {

        if (($payment_type != "88")) {
            $payment_type = "89";
        }
        $result = '';
        $message = '';
        try {
            $data = self::cashExpenseCondition(
                            $payment_type,
                            $employee_id,
                            $company_id,
                            $bank_id,
                            $date,
                            $entry_id
            );


            $daybook = Expenses::model()->find(array(
                'select' => 'SUM(paid) as paidamount',
                'condition' => $data['expcondition1'],
            ));
            $daybook = !empty($daybook['paidamount']) ? $daybook['paidamount'] : '0';
            
            $dailyexpense = Dailyexpense::model()->find(array(
                'select' => 'SUM(dailyexpense_paidamount) as dailyexpense_amount',
                'condition' => 'company_id = ' . $company_id . ' AND parent_status="1"
				AND exp_type = 73 AND ' . $data['dailyexpense_condition'],
            ));

            $dailyexpense = !empty($dailyexpense['dailyexpense_amount']) ?
                    $dailyexpense['dailyexpense_amount'] : '0';
            
            $dailyvendors = Dailyvendors::model()->find(array(
                'select' => 'SUM( IFNULL(amount, 0) + IFNULL(tax_amount, 0) ) as paidamount',
                'condition' => 'company_id = ' . $company_id . ' 
                AND payment_type = ' . $payment_type . $data['dailyvendors_condition']
            ));
            $dailyvendors = !empty($dailyvendors['paidamount']) ? $dailyvendors['paidamount'] : '0';
            
            $subcontractor = SubcontractorPayment::model()->find(array(
                'select' => 'SUM( IFNULL(amount, 0) + IFNULL(tax_amount, 0) ) as paidamount',
                'condition' => 'company_id = ' . $company_id . ' 
                AND payment_type = ' . $payment_type . $data['subcontractor_condition']
            ));
            
            $subcontractor = !empty($dailyvendors['paidamount']) ? $dailyvendors['paidamount'] : '0';
            
            $total_expense_amount = $daybook + $dailyexpense + $dailyvendors + $subcontractor;
            $buyer_module = GeneralSettings::model()->checkBuyerModule();

            if ($buyer_module) {
                $buyer_transactions = BuyerTransactions::model()->findAll(array(
                    'condition' => 'transaction_type IN( 89,88)'
                ));
                foreach ($buyer_transactions as $buyer_transaction) {
                    if ($buyer_transaction['transaction_for'] == 3) {
                        $total_expense_amount += $buyer_transaction['total_amount'];
                    }
                }
            }
            return $total_expense_amount;
        } catch (Exception $error) {
            $message = $error->getMessage();
            return $message;
        }
    }

    public static function cashInvoiceCondition($payment_type, $bank_id, $company_id, $date, $employee_id) {

        $condition = '1 = 1';
        $cashbal_condition = '1 = 1';
        $cashbal_date = '`date`';
        if ($payment_type == 88) {
            $condition = 'reconciliation_status = 1 AND bank_id = ' . $bank_id;
            $cashbal_condition = 'cashbalance_type = 99 AND bank_id = ' . $bank_id;
            $cashbal_model = Cashbalance::model()->find(array(
                'condition' => $cashbal_condition
                . ' AND company_id = ' . $company_id
            ));
            //print_r($cashbal_model);exit;
            $cashbal_date = '`date`';
            $reconcil_condition = "reconciliation_status = 1 "
                    . " AND date "
                    . " BETWEEN '" . $cashbal_model['cashbalance_date'] . "' AND '" . $date . "'";

            $unreconcil_condition = "reconciliation_status = 0 "
                    . " AND reconciliation_date IS NULL";

            $condition1 = "(" . $reconcil_condition . " AND dailyexpense_receipt_type=88 AND bank_id=" . $bank_id ." AND company_id = ". $company_id." ) "
                    . " OR (" . $unreconcil_condition . "  AND dailyexpense_receipt_type=89  "
                    . " AND expensehead_type=5  AND company_id = ". $company_id.") ";
        } elseif ($payment_type == 103 && !empty($employee_id)) {
            $condition = $condition1 = 'employee_id = ' . $employee_id;
        } elseif ($payment_type == 89) {
            $cashbal_condition = 'cashbalance_type = 100';
            $cashbal_model = Cashbalance::model()->find(array(
                'condition' => $cashbal_condition
                . ' AND company_id = ' . $company_id
            ));
            $condition1 = "date BETWEEN '" . $cashbal_model['cashbalance_date'] . "' "
                    . " AND '" . $date . "'  AND (dailyexpense_receipt_type=89 "
                    . " AND dailyexpense_chequeno IS NULL  AND reconciliation_status IS NULL AND company_id =  $company_id) "
                    . " OR (dailyexpense_receipt_type=88 AND expensehead_type=4  AND company_id =  $company_id) ";
        }
        if (!empty($cashbal_model)) {
            $condition .= ($condition != '' ? ' AND ' : '') . '  ' . $cashbal_date . ' >="' . $cashbal_model['cashbalance_date'] . '"';
        }
        if (!empty($date)) {
            $condition .= ($condition != '' ? ' AND ' : '') . ' ' . $cashbal_date . ' <="' . $date . '"';
        }

        $expcondition = $condition .' AND company_id = ' . $company_id
                . ' AND  payment_type = ' . $payment_type
                . ' AND `type` = 72 ';

        return (array(
            'condition' => $condition,
            'condition1' => $condition1,
            'expcondition' => $expcondition,
            'cashbal_condition' => $cashbal_condition
        ));
    }

    public static function cashExpenseCondition($payment_type, $employee_id, $company_id, $bank_id, $date, $entry_id) {

        $daybook_condition = '1 = 1';
        $dailyexpense_condition = '1 = 1';
        $dailyvendors_condition = ' AND 1 = 1';
        $subcontractor_condition = ' AND 1 = 1';
        $cashbal_date = '`date`';
        $entry_condition="";

        if ($payment_type == 103 && !empty($employee_id)) {
            $daybook_condition = 'employee_id = ' . $employee_id;
            $dailyexpense_condition = 'user_id = ' . $employee_id;
        } elseif ($payment_type == 88 && !empty($bank_id)) {
            $cashbal_condition = 'cashbalance_type = 99 AND bank_id = ' . $bank_id;
            $cashbal_model = Cashbalance::model()->find(array(
                'condition' => $cashbal_condition
                . ' AND company_id = ' . $company_id
            ));
            $reconcil_condition = "reconciliation_status = 1 "
                    . " AND date "
                    . " BETWEEN '" . $cashbal_model['cashbalance_date'] . "'"
                    . "  AND '" . $date . "'";

            $unreconcil_condition = "reconciliation_status = 0 "
                    . " AND reconciliation_date IS NULL";
            if (!empty($entry_id)) {                
                $entry_condition = ' AND dailyexp_id != ' . $entry_id; 
            }
            $dailyexpense_condition = "(" . $reconcil_condition
                    . " AND (expense_type=88 OR expense_type=89) $entry_condition "
                    . " AND  bank_id=" . $bank_id.") "
                    . " OR (" . $unreconcil_condition . " AND expense_type=89 "
                    . " AND expensehead_type=4 $entry_condition "
                    . " AND  bank_id=" . $bank_id.") " ;

            $daybook_condition .= ' AND bank_id = ' . $bank_id
                    . ' AND reconciliation_status = 1';
            $dailyvendors_condition .= ' AND bank = ' . $bank_id
                    . ' AND reconciliation_status = 1';
            $subcontractor_condition.= ' AND bank = ' . $bank_id
                    . ' AND reconciliation_status = 1';
            $cashbal_date = '`date`';
        } elseif ($payment_type == 89) {
            $cashbal_condition = 'cashbalance_type = 100';
            $cashbal_model = Cashbalance::model()->find(array(
                'condition' => $cashbal_condition
                . ' AND company_id = ' . $company_id
            ));
            
            if (!empty($entry_id)) {                
                $entry_condition = ' AND dailyexp_id != ' . $entry_id; 
            }

            $dailyexpense_condition = "date BETWEEN '" . $cashbal_model['cashbalance_date'] . "' "
                    . " AND '" . $date . "' AND (expense_type=89) "
                    . " AND dailyexpense_chequeno IS NULL "
                    . " AND reconciliation_status IS NULL ".$entry_condition;

        }

        if (!empty($cashbal_model)) {
            $daybook_condition .= ' AND ' . $cashbal_date . ' >="' . $cashbal_model['cashbalance_date'] . '"';
            $dailyvendors_condition .= ' AND ' . $cashbal_date . ' >="' . $cashbal_model['cashbalance_date'] . '"';
            $subcontractor_condition .= ' AND ' . $cashbal_date . ' >="' . $cashbal_model['cashbalance_date'] . '"';
        }

        if (!empty($entry_id)) {
            $daybook_condition .= ' AND exp_id != ' . $entry_id;
        }

        if (!empty($date)) {
            $daybook_condition .= ' AND ' . $cashbal_date . ' <= "' . $date . '"';
            $dailyvendors_condition .= ' AND ' . $cashbal_date . ' <= "' . $date . '"';
            $subcontractor_condition.= ' AND ' . $cashbal_date . ' <= "' . $date . '"';
        }

        $expcondition1 = 'company_id = ' . $company_id
                . ' AND expense_type = ' . $payment_type
                . ' AND `type` = 73  AND ' . $daybook_condition;

        return (array(
            'expcondition1' => $expcondition1,
            'dailyexpense_condition' => $dailyexpense_condition,
            'dailyvendors_condition' => $dailyvendors_condition,
            'daybook_condition' => $daybook_condition,
            'subcontractor_condition'=>$subcontractor_condition
        ));
    }

}
