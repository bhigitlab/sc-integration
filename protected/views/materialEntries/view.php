<?php
/* @var $this MaterialEntriesController */
/* @var $model MaterialEntries */

$this->breadcrumbs=array(
	'Material Entries'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List MaterialEntries', 'url'=>array('index')),
	array('label'=>'Create MaterialEntries', 'url'=>array('create')),
	array('label'=>'Update MaterialEntries', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete MaterialEntries', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage MaterialEntries', 'url'=>array('admin')),
);
?>

<h1>View MaterialEntries #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'project_id',
		'company_id',
		'subcontractor_id',
		'expensehead_id',
		'description',
		'status',
		'Date',
	),
)); ?>
