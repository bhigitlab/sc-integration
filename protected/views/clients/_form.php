<?php
/* @var $this ClientsController */
/* @var $model Clients */
/* @var $form CActiveForm */
$pms_api_integration = ApiSettings::model()->pmsIntegrationStatus();
$crm_api_integration = ApiSettings::model()->crmIntegrationStatus();
?>

<div class="">
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'clients-form',
        'enableAjaxValidation' => true,
        'enableClientValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true,
            'validateOnType' => false,
        ),
    )); ?>
    <div>
        <div class="row">
            <input type="hidden" id="pms_integration" value="<?php echo $pms_api_integration; ?>">
            <div class="col-md-3 col-sm-4 form-group">
                <?php echo $form->labelEx($model, 'name'); ?>
                <?php
                $readonly = ($crm_api_integration == 1) ? ['readonly' => true] : [];
                echo $form->textField(
                    $model,
                    'name',
                    array_merge(['size' => 66, 'maxlength' => 100, 'class' => 'form-control'], $readonly)
                ); ?>
                <?php echo $form->error($model, 'name'); ?>
            </div>
            <div class="col-md-3 col-sm-4 form-group">
                <?php echo $form->labelEx($model, 'project_type'); ?>
                <?php echo $form->dropDownList($model, 'project_type', CHtml::listData(ProjectType::model()->findAll(array('order' => 'project_type ASC')), 'ptid', 'project_type'), array('class' => 'form-control', 'empty' => '--')); ?>
                <?php echo $form->error($model, 'project_type'); ?>
            </div>
            <div class="col-md-3 col-sm-4 form-group">
                <?php echo $form->labelEx($model, 'phone'); ?>
                <?php echo $form->textField($model, 'phone', array('class' => 'form-control')); ?>
                <?php echo $form->error($model, 'phone'); ?>
            </div>
            <div class="col-md-3 col-sm-4 form-group">
                <?php echo $form->labelEx($model, 'email_id'); ?>
                <?php echo $form->textField($model, 'email_id', array('class' => 'form-control')); ?>
                <?php echo $form->error($model, 'email_id'); ?>
            </div>
            <?php if (!$model->isNewRecord) {
                if ($model->project_type == 2) {
                    ?>
                    <div class="col-md-3 col-sm-4 form-group hidbox">
                        <?php echo $form->labelEx($model, 'contact_person'); ?>
                        <?php echo $form->textField($model, 'contact_person', array('class' => 'form-control')); ?>
                        <?php echo $form->error($model, 'contact_person'); ?>
                    </div>

                    <div class="col-md-3 col-sm-4 form-group hidbox">
                        <?php echo $form->labelEx($model, 'local_address'); ?>
                        <?php echo $form->textArea($model, 'local_address', array('class' => 'form-control')); ?>
                        <?php echo $form->error($model, 'local_address'); ?>
                    </div>

                    <?php
                } else {
                    ?>
                    <div class="col-md-3 col-sm-4 form-group hidbox" style="display: none">
                        <?php echo $form->labelEx($model, 'contact_person'); ?>
                        <?php echo $form->textField($model, 'contact_person', array('class' => 'form-control')); ?>
                        <?php echo $form->error($model, 'contact_person'); ?>
                    </div>
                    <div class="col-md-3 col-sm-4 form-group hidbox" style="display: none">
                        <?php echo $form->labelEx($model, 'local_address'); ?>
                        <?php echo $form->textArea($model, 'local_address', array('class' => 'form-control')); ?>
                        <?php echo $form->error($model, 'local_address'); ?>
                    </div>
                    <?php
                }
            } else { ?>
                <div class="col-md-3 col-sm-4 form-group hidbox" style="display: none">
                    <?php echo $form->labelEx($model, 'contact_person'); ?>
                    <?php echo $form->textField($model, 'contact_person', array('class' => 'form-control')); ?>
                    <?php echo $form->error($model, 'contact_person'); ?>
                </div>


                <div class="col-md-3 col-sm-4 form-group hidbox" style="display: none">
                    <?php echo $form->labelEx($model, 'local_address'); ?>
                    <?php echo $form->textArea($model, 'local_address', array('class' => 'form-control')); ?>
                    <?php echo $form->error($model, 'local_address'); ?>
                </div>

            <?php } ?>
            <div class="col-md-3 col-sm-4 form-group">
                <?php echo $form->labelEx($model, 'gst_no'); ?>
                <?php echo $form->textField($model, 'gst_no', array('class' => 'form-control')); ?>
                <?php echo $form->error($model, 'gst_no'); ?>
            </div>
            <div class="col-md-3 col-sm-4 form-group">
                <?php echo $form->labelEx($model, 'status'); ?>
                <div class="radio_btn">
                    <?php
                    echo $form->radioButtonList($model, 'status', CHtml::listData(Status::model()->findAll(
                        array(
                            'select' => array('sid,caption'),
                            'condition' => 'status_type="active_status"',
                            'order' => 'caption',
                            'distinct' => true
                        )
                    ), 'sid', 'caption'), array('separator' => '', 'display' => 'inline-block', 'class' => 'statusall'));
                    ?>
                </div>
                <?php echo $form->error($model, 'status'); ?>
            </div>
            <!-- </div>
        <div class="row"> -->
            <div class="col-md-3 col-sm-6 form-group">
                <?php echo $form->labelEx($model, 'address'); ?>
                <?php echo $form->textArea($model, 'address', array('rows' => 3, 'cols' => 50, 'class' => 'form-control')); ?>
                <?php echo $form->error($model, 'address'); ?>
            </div>
            <div class="col-md-3 col-sm-6 form-group">
                <?php echo $form->labelEx($model, 'description'); ?>
                <?php echo $form->textArea($model, 'description', array('class' => 'form-control', 'rows' => 3, 'cols' => 50)); ?>
                <?php echo $form->error($model, 'description'); ?>
            </div>
            <div class="col-md-3 col-sm-6 form-group">
                <label>Company </label>&nbsp;<span class="required">*</span>
                <ul class="checkboxList">
                    <?php
                    $user = Users::model()->findByPk(Yii::app()->user->id);
                    $arrVal = explode(',', $user->company_id);
                    $newQuery = "";
                    foreach ($arrVal as $arr) {
                        if ($newQuery)
                            $newQuery .= ' OR';
                        $newQuery .= " FIND_IN_SET('" . $arr . "', id)";
                    }
                    $typelist = Company::model()->findAll(array('condition' => $newQuery));
                    $assigned_company_array = array();
                    if (!$model->isNewRecord) {
                        $assigned_types = Clients::model()->find(array('condition' => 'cid=' . $model->cid));
                        $assigned_company_array = explode(",", $assigned_types->company_id);
                        if ((isset(Yii::app()->user->role) && (in_array('/clients/companypermission', Yii::app()->user->menuauthlist)))) {
                            $readonly = "";
                        } else {
                            $readonly = "readonly";
                        }

                    } else {
                        $assigned_company_array = "";
                        $readonly = "";
                    }
                    echo CHtml::checkBoxList('Clients[company_id]', $assigned_company_array, CHtml::listData($typelist, 'id', 'name'), array('checkAll' => 'Check all', 'template' => '<li class="checkboxtype compnyall">{input}{label}</li>', 'separator' => '', 'readonly' => $readonly));
                    ?>
                </ul>
                <?php echo $form->error($model, 'company_id'); ?>
            </div>

        </div>
        <div class="row">
            <div class="form-group col-xs-12 text-right">
                <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-info', 'id' => 'submit-btn')); ?>
                <?php if (!$model->isNewRecord) {
                    echo CHtml::ResetButton('Close', array('onclick' => 'closeaction(this,event);', 'class' => 'btn btn-other'));
                } else {
                    echo CHtml::ResetButton('Reset', array('style' => 'margin-right:3px;', 'class' => 'btn btn-default'));
                    echo CHtml::ResetButton('Close', array('onclick' => 'closeaction(this,event);', 'class' => 'btn btn-other'));
                }
                echo CHtml::hiddenField('execute_api', 0); ?>
            </div>
        </div>
    </div>
    <?php echo $form->hiddenField($model, 'client_mapping_id'); ?>

    <?php $this->endWidget(); ?>

</div><!-- form -->
<script>
    $(document).on("change", "#Clients_project_type", function () {
        var val = $("#Clients_project_type option:selected").text();
        if (val == 'NRI') {
            $('.hidbox').show();
        } else {
            $('.hidbox').hide();
        }
    });

    function countOfLetters(str) {
        var letter = 0;
        for (i = 0; i < str.length; i++) {
            if ((str[i] >= 'A' && str[i] <= 'Z')
                || (str[i] >= 'a' && str[i] <= 'z'))
                letter++;
        }

        return letter;
    }

    $("#Clients_name").keyup(function () {
        var alphabetCount = countOfLetters(this.value);
        var message = "";
        var disabled = false;

        if (alphabetCount < 1) {
            var message = "Invalid Client Name";
            var disabled = true;
        }

        $(this).siblings(".errorMessage").show().html(message).addClass('d-block');
        $(".btn-info").attr('disabled', disabled);

    });
    $(function () {
        var anca_pms_linked = '<?php echo ANCAPMS_LINKED; ?>';
        if (anca_pms_linked) {
            $('#Clients_name').addClass('nonClickable');
            $('#Clients_project_type').addClass('nonClickable');
            $('#Clients_phone').addClass('nonClickable');
            $('#Clients_address').addClass('nonClickable');
            $('.compnyall').addClass('nonClickable');
            $('.statusall').addClass('nonClickable');
        }
    });
    $(document).ready(function () {
        document.getElementById('clients-form').onsubmit = function () {
            var pms_integration = $("#pms_integration").val();
            var client_mapping_id = $("#Clients_client_mapping_id").val();
            if (pms_integration == '1') {
                if (client_mapping_id == '0') {
                    var confirmApiCall = confirm("Do you want to save the client in Pms?");
                    if (confirmApiCall) {
                        document.getElementById('execute_api').value = 1;
                    }
                }
                else if (client_mapping_id != '0') {
                    document.getElementById('execute_api').value = 1;
                }
            }
        };
    });
</script>