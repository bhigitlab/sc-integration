<?php
/* @var $this SalesInvoiceController */
/* @var $model SalesInvoice */

$this->breadcrumbs = array(
	'Sales Invoices' => array('index'),
	'Manage',
);
?>
<div class="container" id="expense">
	<div class="expenses-heading">
		<div class="clearfix">
			<?php if ((isset(Yii::app()->user->role) && (in_array('/salesInvoice/create', Yii::app()->user->menuauthlist)))) { ?>
				<a href="index.php?r=salesInvoice/create" class="btn btn-primary  pull-right mt">Add Entry</a>
			<?php } ?>
			<h2>Sales Invoices</h2>
		</div>
	</div>
	<div class="daybook form">
		<?php if (Yii::app()->user->hasFlash('success')) : ?>
			<div class="alert alert-success alert-dismissible">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<?php echo Yii::app()->user->getFlash('success'); ?>
			</div>
		<?php endif; ?>


		<?php if (Yii::app()->user->hasFlash('error')) : ?>
			<div class="alert alert-danger  alert-dismissible">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<?php echo Yii::app()->user->getFlash('error'); ?>
			</div>
		<?php endif; ?>
	</div>
	<div>
		<div id="errormessage"></div>
	</div>
	<div class="daybook">
		<div class="">
			<?php
			// $this->renderPartial('_search', array(
			// 	'model' => $model
			// )); 

			?>
			<div id="daybook-entry">
				<?php $this->widget('zii.widgets.CListView', array(
					'id' => 'sales-invoice-grid',
					'dataProvider' => $model->search(),
					'itemView' => '_list',
					'id' => 'id',
					'enableSorting' => 1,
					'enablePagination' => true,
					'template' => '<div>{summary}{sorter}</div><div id="parent"><table width="100" id="fixTable" class="table total-table">{items}</table></div>{pager}',
					'enablePagination' => false
				)); ?>
			</div>
		</div>
	</div>
</div>
<script>
	$(document).on('click', '.edit_invoice', function() {
		var id = $(this).attr('id');
		var url = "<?php echo Yii::app()->createAbsoluteUrl("salesInvoice/update") ?>";
		location.href = url + '&id=' + id;
	})
	$(document).on('click', '.view_invoice', function() {
		var id = $(this).attr('id');
		var url = "<?php echo Yii::app()->createAbsoluteUrl("salesInvoice/view") ?>";
		location.href = url + '&id=' + id;
	})
	$(document).on('click', '.delete_invoice', function() {
		var id = $(this).attr('id');
		console.log(id);
		var url = "<?php echo Yii::app()->createAbsoluteUrl("salesInvoice/deleteinvoice") ?>";
		location.href = url + '&id=' + id;
	})
</script>