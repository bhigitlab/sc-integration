<?php

/**
 * This is the model class for table "{{sales_invoice_item}}".
 *
 * The followings are the available columns in table '{{sales_invoice_item}}':
 * @property integer $id
 * @property integer $sales_invoice_id
 * @property string $main_title
 * @property string $main_description
 * @property double $total_amount
 * @property integer $created_by
 * @property string $created_date
 * @property integer $updated_by
 * @property string $updated_date
 *
 * The followings are the available model relations:
 * @property Users $createdBy
 * @property SalesInvoice $salesInvoice
 * @property Users $updatedBy
 * @property SalesInvoiceSubItem[] $salesInvoiceSubItems
 */
class SalesInvoiceItem extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{sales_invoice_item}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('sales_invoice_id, created_by, updated_by', 'numerical', 'integerOnly'=>true),
			array('total_amount', 'numerical'),
			array('main_title', 'length', 'max'=>100),
			array('main_description, created_date, updated_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, sales_invoice_id, main_title, main_description, total_amount, created_by, created_date, updated_by, updated_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'createdBy' => array(self::BELONGS_TO, 'Users', 'created_by'),
			'salesInvoice' => array(self::BELONGS_TO, 'SalesInvoice', 'sales_invoice_id'),
			'updatedBy' => array(self::BELONGS_TO, 'Users', 'updated_by'),
			'salesInvoiceSubItems' => array(self::HAS_MANY, 'SalesInvoiceSubItem', 'invoice_item_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'sales_invoice_id' => 'Sales Invoice',
			'main_title' => 'Main Title',
			'main_description' => 'Main Description',
			'total_amount' => 'Total Amount',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
			'updated_by' => 'Updated By',
			'updated_date' => 'Updated Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('sales_invoice_id',$this->sales_invoice_id);
		$criteria->compare('main_title',$this->main_title,true);
		$criteria->compare('main_description',$this->main_description,true);
		$criteria->compare('total_amount',$this->total_amount);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('updated_by',$this->updated_by);
		$criteria->compare('updated_date',$this->updated_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SalesInvoiceItem the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
