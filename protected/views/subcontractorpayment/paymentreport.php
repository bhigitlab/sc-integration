<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.min.js"></script>
<?php
if (filter_input(INPUT_GET, 'export')) {
    ?>
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/pdfstyle.css">
<?php } ?>
<style>
    .tbl_holder {
        padding-left: 0px !important;
        padding-right: 0px !important;
    }

    .tbl_holder table {
        width: 100%;
    }

    .tbl_holder table tbody tr {
        border-bottom: 1px solid #ddd;
    }

    .tbl_holder table tbody tr:last-child {
        border-bottom: 0px;
    }

    .tbl_holder table tbody tr td {
        border: none !important;
        padding: 8px;
        white-space: normal;
        height: 10px;
    }

    td {
        vertical-align: middle !important;
        /* padding: 5px !important; */
    }

    ul,
    ol {
        padding-left: 15px;
    }
</style>
<?php
$main_heading = "SUB CONTRACTOR PAYMENT REPORTS";
$tblpx = Yii::app()->db->tablePrefix;
$newQuery = "";
$newQuery_1 = "";
$newQuery1 = "";
$newQuery2 = "";
$newQuery3 = "";
$newQuery4 = "";
$newQuery5 = "";
$user = Users::model()->findByPk(Yii::app()->user->id);
$arrVal = explode(',', $user->company_id);
foreach ($arrVal as $arr) {
    if ($newQuery)
        $newQuery .= ' OR';
    if ($newQuery_1)
        $newQuery_1 .= ' OR';
    $newQuery .= " FIND_IN_SET('" . $arr . "', id)";
    $newQuery_1 .= " FIND_IN_SET('" . $arr . "', company_id)";
}

if ($company_id == '') {
    foreach ($arrVal as $arr) {
        if ($newQuery1)
            $newQuery1 .= ' OR';
        if ($newQuery2)
            $newQuery2 .= ' OR';
        if ($newQuery3)
            $newQuery3 .= ' OR';
        $newQuery1 .= " FIND_IN_SET('" . $arr . "', {$tblpx}scquotation.company_id)";
        $newQuery2 .= " FIND_IN_SET('" . $arr . "', {$tblpx}subcontractor_payment.company_id)";
        $newQuery3 .= " FIND_IN_SET('" . $arr . "', {$tblpx}subcontractor.company_id)";
    }
} else {
    $newQuery1 .= " FIND_IN_SET('" . $company_id . "', {$tblpx}scquotation.company_id)";
    $newQuery2 .= " FIND_IN_SET('" . $company_id . "', {$tblpx}subcontractor_payment.company_id)";
    $newQuery3 .= " FIND_IN_SET('" . $company_id . "', {$tblpx}subcontractor.company_id)";
}
if ($project_id != '') {
    $newQuery4 = " AND {$tblpx}scquotation.project_id =" . $project_id . "";
    $newQuery5 = " AND {$tblpx}subcontractor_payment.project_id = " . $project_id . "";
}
if ($company_id != NULL) {
    $company = Company::model()->findbyPk($company_id);
    $company_address = Company::model()->findBypk($company_id);
    $company_name = ' - ' . $company->name;
} else {
    $company_name = "";
    $company_address = Company::model()->find(array('order' => ' id ASC'));
}
?>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/tableHeadFixer.js"></script>


<?php

function testfunction($data, $subcontractor_id, $project_id)
{
    $payment_details = Yii::app()->db->createCommand("SELECT * FROM `jp_subcontractor_payment` WHERE subcontractor_id =" . $subcontractor_id . " AND project_id=" . $project_id . " AND date='" . $data . "'")->queryRow();
    if (!empty($payment_details)) {
        return array('date' => $data, 'amount' => 10);
    } else {
        return array('date' => $data, 'amount' => 0);
    }
}
?>
<div class="container" id="project">
    <?php if (!filter_input(INPUT_GET, 'export')) { ?>
        <div class="expenses-heading header-container">
            <!--Button changes -->
            <?php if ($subcontractor_id != 0) { ?>
                <h3>
                    <?php echo $main_heading; ?>
                </h3>
                <div class="btn-container"
                    style="<?php echo filter_input(INPUT_GET, 'export') == 'pdf' ? 'display:none' : '' ?>">
                    <?php if (isset($_REQUEST['custId']) && ($_REQUEST['custId'] == 'unreconcil')) { ?>
                        <button id="reconcil-report-btn" style="cursor:pointer;" class="reconciled save_btn btn btn-info"
                            href="<?php echo $this->createAbsoluteUrl('subcontractorpayment/report', array("custId" => 'reconcil', "subcontractor_id" => $subcontractor_id, "project_id" => $project_id, "company_id" => $company_id, )) ?>">Reconciled
                            Report</button>
                    <?php } else { ?>
                        <button id="un-report-btn" style="cursor:pointer;" class="unreconciled save_btn btn btn-info"
                            href="<?php echo $this->createAbsoluteUrl('subcontractorpayment/report', array("custId" => 'unreconcil', "subcontractor_id" => $subcontractor_id, "project_id" => $project_id, "company_id" => $company_id, )) ?>">UnReconciled
                            Report</button>
                    <?php } ?>
                    <button type="button" id="excel-btn" class="btn btn-info" title="SAVE AS EXCEL"
                        href="<?php echo Yii::app()->request->requestUri . "&export=csv&subcontractor_id=" . $subcontractor_id . '&project_id=' . $project_id . '&company_id=' . $company_id ?>">
                        <i class="fa fa-file-excel-o button-icon" aria-hidden="true"></i></button>
                    <button type="button" id="pdf-btn" class="btn btn-info" title="SAVE AS PDF"
                        href="<?php echo Yii::app()->request->requestUri . "&export=pdf&subcontractor_id=" . $subcontractor_id . '&project_id=' . $project_id . '&company_id=' . $company_id ?>">
                        <i class="fa fa-file-pdf-o button-icon" aria-hidden="true"></i>
                    </button>
                </div>
            <?php } else { ?>
                <h3>
                    <?php echo $main_heading; ?>
                </h3>
                <div class="btn-container"
                    style="<?php echo filter_input(INPUT_GET, 'export') == 'pdf' ? 'display:none' : '' ?>">
                    <button type="button" id="payment_report_aging_two"
                        href="<?php echo Yii::app()->createUrl("subcontractorpayment/agingreportbydate") ?>"
                        class="save_btn btn btn-info">Aging
                        Summary By Due Date</button>
                    <button type="button" id="payment_report_aging_one"
                        href="<?php echo Yii::app()->createUrl("vendors/ageingvendorReport") ?>" class="btn btn-info ">Aging
                        Summary</button>
                    <button type="button" id="excel-btn1" class="btn btn-info" title="SAVE AS EXCEL"
                        href="<?php echo Yii::app()->request->requestUri . "&export=csv" ?>">
                        <i class="fa fa-file-excel-o button-icon" aria-hidden="true"></i></button>
                    <button type="button" id="pdf-btn1" class="btn btn-info" title="SAVE AS PDF"
                        href="<?php echo Yii::app()->request->requestUri . "&export=pdf" ?>">
                        <i class="fa fa-file-pdf-o button-icon" aria-hidden="true"></i>
                    </button>
                </div>
            <?php } ?>
        </div>
        <!--Button changes ends -->
        <div class="page_filter clearfix custom-form-style">
            <?php $form = $this->beginWidget('CActiveForm', array(
                'action' => Yii::app()->createUrl($this->route),
                'method' => 'get',
            )); ?>
            <div class="row">
                <div class="form-group col-xs-12 col-sm-3 col-md-2">
                    <label>Sub Contractor </label>
                    <?php
                    echo CHtml::dropDownList('subcontractor_id', 'subcontractor_id', CHtml::listData(Subcontractor::model()->findAll(array(
                        'select' => array('subcontractor_id, subcontractor_name'),
                        'order' => 'subcontractor_name',
                        'condition' => '(' . $newQuery_1 . ')',
                        'distinct' => true
                    )), 'subcontractor_id', 'subcontractor_name'), array('empty' => 'Select Subcontractor', 'id' => 'subcontractor_id', 'class' => 'form-control', 'options' => array($subcontractor_id => array('selected' => true))));
                    ?>
                </div>
                <div class="form-group col-xs-12 col-sm-3 col-md-2">
                    <label>Company</label>
                    <?php
                    echo CHtml::dropDownList('company_id', 'company_id', CHtml::listData(Company::model()->findAll(array(
                        'select' => array('id, name'),
                        'order' => 'id DESC',
                        'condition' => '(' . $newQuery . ')',
                        'distinct' => true

                    )), 'id', 'name'), array('empty' => 'Select Company', 'class' => 'form-control', 'id' => 'company_id', 'options' => array($company_id => array('selected' => true))));

                    ?>
                </div>
                <div class="form-group col-xs-12 col-sm-3 col-md-2">
                    <label>Project</label>
                    <?php
                    echo CHtml::dropDownList('project_id', 'project_id', CHtml::listData(Projects::model()->findAll(array(
                        'select' => array('pid, name'),
                        'order' => 'pid DESC',
                        'condition' => '(' . $newQuery_1 . ')',
                        'distinct' => true

                    )), 'pid', 'name'), array('empty' => 'Select Project', 'class' => 'form-control', 'id' => 'company_id', 'options' => array($project_id => array('selected' => true))));

                    ?>
                </div>
                <div class="form-group col-xs-12 col-sm-3 col-md-2">
                    <label>From</label>
                    <input type="text" id="date_from" name="date_from"
                        value="<?php echo isset($date_from) && !empty($date_from) ? $date_from : date("Y-m-d", strtotime("-3 months")); ?>"
                        class="form-control" placeholder="Date From" autocomplete="off">
                </div>
                <div class="form-group col-xs-12 col-sm-3 col-md-2">
                    <label>To</label>
                    <input type="text" id="date_to" name="date_to"
                        value="<?php echo isset($date_to) && !empty($date_to) ? $date_to : date('Y-m-d'); ?>"
                        class="form-control" placeholder="Date To" autocomplete="off">
                </div>

                <input type="hidden" id="custId" name="custId"
                    value="<?php echo isset($_GET['custId']) ? $_GET['custId'] : 'reconcil' ?>">
                <div class="form-group col-xs-12 col-sm-3 col-md-2 text-sm-left text-right">
                    <label class="d-sm-block d-none">&nbsp;</label>
                    <div>
                        <?php echo CHtml::submitButton('GO', array('id' => 'btSubmit', 'class' => 'btn btn-sm btn-primary')); ?>
                        <?php echo CHtml::resetButton('Clear', array('class' => 'btn btn-sm btn-default', 'onclick' => 'javascript:location.href="' . $this->createUrl('report') . '"')); ?>
                    </div>
                </div>
            </div>
            <?php $this->endWidget(); ?>
        </div>
        <?php
    } elseif (filter_input(INPUT_GET, 'export') === "pdf") {
        ?>

        <style>
            table {
                border-collapse: collapse;
                width: 100%;
                font-family: Arial, Helvetica, sans-serif;

            }

            .details,
            .info-table {
                border: 1px solid #ccc;
                padding: 2rem;
            }

            .details td,
            .details th,
            .info-table td,
            .info-table th {
                padding: 5px 10px;
                font-size: .85rem;

            }

            .info-table {
                margin-bottom: 10px;
            }

            .text-center {
                text-align: center;
            }

            .img-hold {
                width: 10%;
            }

            .companyhead {
                font-size: 1rem;
                font-weight: bold;
                margin-top: 0px;
                color: #789AD1;
            }

            .table-header th,
            .details {

                font-size: .75rem;
                padding-left: 4px;
                padding-right: 4px;
            }

            .medium-font {
                font-size: .85rem;
            }

            .addrow th,
            .addrow td {
                font-size: .75rem;
                border: 1px solid #333;
                border-collapse: collapse;
                padding-left: 4px;
                padding-right: 4px;
            }

            .text-right {
                text-align: right;
            }

            .w-50 {
                width: 50%;
            }

            .text-sm {
                font-size: 11px;
            }

            .dotted-border-bottom {
                border-bottom: 2px dotted black;
            }

            .w-100 {
                width: 100%;
            }

            .border-right {
                border-right: 1px solid #ccc;
            }

            .border-0 {
                border-right: 1px solid #fff !important;
            }

            .header-border {
                border-bottom: 1px solid #000;
            }
        </style>
        <h3 class="text-center">
            <?php echo $main_heading; ?>
        </h3>
    <?php } ?>
    <div id="contenthtml" class="contentdiv">
        <?php
        $tblpx = Yii::app()->db->tablePrefix;
        $scquotationsql = "SELECT * FROM " . $this->tableNameAcc('subcontractor', 0) . " "
            . " INNER JOIN " . $this->tableNameAcc('scquotation', 0) . " "
            . " ON {$tblpx}subcontractor.subcontractor_id={$tblpx}scquotation.subcontractor_id"
            . " LEFT JOIN {$tblpx}projects ON {$tblpx}scquotation.project_id= {$tblpx}projects.pid"
            . " WHERE (" . $newQuery3 . ") GROUP BY {$tblpx}scquotation.subcontractor_id,"
            . " {$tblpx}scquotation.project_id "
            . " ORDER BY {$tblpx}scquotation.scquotation_id";
        $scquotation = Yii::app()->db->createCommand($scquotationsql)->queryAll();
        if ($subcontractor_id != 0) { ?>
            <div class="row margin-top-10">
                <div class="col-xs-12">
                    <div class="heading-title">Payment Summary of <b>
                            <?php echo $name; ?> &nbsp;
                            <?php echo $company_name; ?>
                        </b></div>
                </div>
            </div>

            <div>
                <?php
                if (filter_input(INPUT_GET, 'export') == 'pdf') {
                    ?>
                    <div class="content">
                        <div class="wrapper">
                            <div class="holder">
                                <?php
                }
                if (isset($_REQUEST['custId']) && ($_REQUEST['custId'] == 'reconcil')) {
                    ?>

                                <div class="row margin-top-10">
                                    <div class="col-xs-12">
                                        <div class="heading-title">Payment Against Quotation - Reconcil</div>
                                        <div class="dotted-line"></div>
                                    </div>
                                </div>
                                <div class="reconcil_table table-wrapper margin-top-10">
                                    <div class="d-flex">
                                        <div class="summary text-right margin-left-auto">Total
                                            <?php echo count($final_array); ?> results
                                        </div>
                                    </div>
                                    <div id="table-wrapper">

                                        <table
                                            class="table total-table   <?php echo filter_input(INPUT_GET, 'export') == 'pdf' ? 'detail-table table_one' : 'table-bordered' ?> alt_bg_table">
                                            <?php
                                            $quotation_total = 0;
                                            $payment_total = 0;
                                            $balance_total = 0;
                                            $payment_amount = 0;
                                            $datequery = "";
                                            $datecondition = "";
                                            $bill_total = 0;
                                            $date_array = array();
                                            foreach ($unreconciled_data as $data) {
                                                $recon_date = "'{$data["reconciliation_paymentdate"]}'";
                                                array_push($date_array, $recon_date);
                                            }
                                            $date_array1 = implode(',', $date_array);
                                            if (($date_array1)) {
                                                $datequery .= " date NOT IN( $date_array1) OR date  IS NULL
                                                    ";
                                                $datecondition .= " AND " . $datequery . "";
                                            }
                                            //echo "<pre>";print_r($final_array);exit;
                                            $qA_minus_bill_total_balance = 0;
                                            $bA_minus_paid_total_balance = 0;
                                            $bill_total_amount = 0;
                                            foreach ($final_array as $key => &$value) {
                                                $final_array[$key]['count'] = 1;
                                                $j = 0;
                                                $a = 0;
                                                $writeoffAmt = 0;
                                                $blance = 0;
                                                $check = 0;
                                                $sum = 0;
                                                $payment_sum = 0;
                                                $x = 0;
                                                $payment_done = [];
                                                $bill_total = 0;
                                                $bill_total_amount = 0;
                                                foreach ($value['details'] as $key2 => &$quotations) {
                                                    //echo "<pre>";print_r($quotations);exit;
                                                    foreach ($quotations as $index => &$quotation) {
                                                        $sum += $quotation['amount'];

                                                        $payment_bills_sql = "SELECT sb.id as bill_id,sb.total_amount as bill_amount,sb.bill_number,sb.date as bill_date FROM jp_subcontractorbill sb WHERE sb.scquotation_id='" . $quotation['scquotation_id'] . "'";
                                                        //die($payment_bills_sql);
                                                        $payment_bills = Yii::app()->db->createCommand($payment_bills_sql)->queryAll();

                                                        $paymentsql = "SELECT p.* FROM " . $this->tableNameAcc('subcontractor_payment', 0) . " p "
                                                            . " WHERE p.subcontractor_id ='" . $quotation['subcontractor_id'] . "' "
                                                            . " AND p.quotation_number ='" . $quotation['scquotation_id'] . "' "
                                                            . " AND p.project_id='" . $quotation['project_id'] . "' "
                                                            . " AND (p.reconciliation_status='1' OR p.reconciliation_status IS NULL) "
                                                            . " AND p.approve_status = 'Yes'";
                                                        //  die($paymentsql);
                                                        $payment_details = Yii::app()->db->createCommand($paymentsql)->queryAll();
                                                        $paymentwithoutbillsql = "SELECT p.* FROM " . $this->tableNameAcc('subcontractor_payment', 0) . " p "
                                                            . " WHERE p.subcontractor_id ='" . $quotation['subcontractor_id'] . "' "
                                                            . " AND p.quotation_number ='" . $quotation['scquotation_id'] . "' "
                                                            . " AND p.project_id='" . $quotation['project_id'] . "' "
                                                            . " AND (p.reconciliation_status='1' OR p.reconciliation_status IS NULL) AND p.sc_bill_id IS NULL "
                                                            . " AND p.approve_status = 'Yes'";
                                                        $payment_detailswithoutBill = Yii::app()->db->createCommand($paymentwithoutbillsql)->queryAll();
                                                        $writeoff = Writeoff::model()->find(array('condition' => 'subcontractor_id = ' . $quotation['scquotation_id']));
                                                        $writeoffAmt = ($writeoff) ? $writeoff->amount : 0;



                                                        $payment_sum = $model->getSubcontractorPaymentTotal($quotation, $unreconciled_data);
                                                        foreach ($payment_details as $payment_detail) {
                                                            $payment_total += $payment_detail['amount'];
                                                        }

                                                        foreach ($payment_bills as $payment_bill) {
                                                            $bill_total += $payment_bill['bill_amount'];
                                                        }

                                                        $quotation_total += $quotation['amount'];

                                                        $quotation['totalpayment'] = $payment_sum;
                                                        $quotation['bal_total'] = $quotation['amount'] - $payment_sum - $writeoffAmt;
                                                        $billTotal = 0;
                                                        if (!empty($quotation['total_bill_amount'])) {
                                                            $billTotal = $quotation['total_bill_amount'];
                                                        }
                                                        $qa_minus_ba_amount = $quotation['amount'] - $billTotal;
                                                        $ba_minus_pa_amount = ($billTotal - $payment_sum) - $writeoffAmt;
                                                        // die($quotation['bal_total']);
                                                        $bill_total_amount += $billTotal;
                                                        $quotation['balance_QA_BA'] = $qa_minus_ba_amount;
                                                        $quotation['balance_BA_PA'] = $ba_minus_pa_amount;
                                                        $qA_minus_bill_total_balance += $qa_minus_ba_amount;
                                                        $bA_minus_paid_total_balance += $ba_minus_pa_amount;
                                                        $check++;
                                                        $final_array[$key]['count'] = $check;
                                                        $final_array[$key]['tot_amount'] = $sum;
                                                        $final_array[$key]['payment_total'] = $payment_sum;
                                                        $final_array[$key]['writeoff_amount'] = $writeoffAmt;
                                                        $final_array[$key]['balance_QA_BA'] = $qa_minus_ba_amount;
                                                        $final_array[$key]['balance_BA_PA'] = $ba_minus_pa_amount;

                                                    }

                                                }
                                            }
                                            //  echo "<pre>"; print_r($final_array);exit;
                                            if (empty($writeoffAmt)) {
                                                $writeoffAmt = 0;
                                            }
                                            $balance_total = $quotation_total - ($payment_total + $writeoffAmt);
                                            $bill_total_amount = 0;

                                            foreach ($final_array as $key => &$value) {
                                                foreach ($value['details'] as $key2 => &$quotations) {
                                                    foreach ($quotations as $index => &$quotation) {
                                                        $billTotal = !empty($quotation['total_bill_amount']) ? $quotation['total_bill_amount'] : 0;
                                                        $bill_total_amount += $billTotal;
                                                    }
                                                }
                                            }

                                            //die($balance_total);
                                            ?>
                                            <thead class="entry-table sticky-thead">
                                                <?php if (filter_input(INPUT_GET, 'export') === 'csv') { ?>
                                                    <tr>
                                                        <td>Payment Against Quotation</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-center"><b>Project Name</b></td>
                                                        <td class="text-center"><b>Date</b></td>
                                                        <td class="text-center"><b>Description</b></td>
                                                        <td class="text-center"><b>Quotation Number</b></td>
                                                        <td class="text-center"><b>Quotation Amount(A)</b></td>
                                                        <td class="text-center"><b>Bill Number</b></td>
                                                        <td class="text-center"><b>Bill Date</b></td>
                                                        <td class="text-center"><b>Bill Amount(B)</b></td>

                                                        <td class="text-center"><b>Date</b></td>
                                                        <td class="text-center"><b>Description</b></td>
                                                        <td class="text-center">WriteOff</td>
                                                        <td class="text-center"><b>Paid Amount(C)</b></td>
                                                        <td class="text-center"><b>Balance(A-C)</b></td>
                                                        <td class="text-center"><b>Balance(A-B)</b></td>
                                                        <td class="text-center"><b>Balance(B-C)</b></td>

                                                    </tr>
                                                    <?php
                                                } else {
                                                    ?>
                                                    <tr class="first-thead-row">
                                                        <th rowspan="3" class="text-center">Project Name</th>
                                                        <th colspan="5" class="text-center"> Quotation details </th>
                                                        <th colspan="5" class="text-center"> Subcontractor Bills </th>
                                                        <th colspan="6" class="text-center"> Payment details </th>
                                                        <th rowspan="2" class="text-center"> Balance(A-C) </th>
                                                        <th rowspan="2" class="text-center"> Balance(A-B) </th>
                                                        <th rowspan="2" class="text-center"> Balance(B-C) </th>

                                                    </tr>
                                                    <tr class="second-thead-row">
                                                        <th rowspan="2" class="text-center"> Date </th>
                                                        <th rowspan="2" class="text-center"> Description </th>
                                                        <th rowspan="2" class="text-center"> Quotation Number </th>
                                                        <th class="text-center" colspan="2"> Quotation Amount(A) </th>
                                                        <th rowspan="2" class="text-center"> #Bill ID </th>
                                                        <th rowspan="2" class="text-center"> Bill No </th>
                                                        <th rowspan="2" class="text-center"> Bill Date </th>
                                                        <th class="text-center" colspan="2"> Bill Amount (B) </th>
                                                        <th rowspan="2" class="text-center"> #ID </th>
                                                        <th rowspan="2" class="text-center"> Date </th>
                                                        <th rowspan="2" class="text-center"> Description </th>
                                                        <th rowspan="2" class="text-center"> WriteOff </th>
                                                        <th class="text-center" colspan="2"> Paid Amount(C) </th>
                                                    </tr>
                                                <?php } ?>
                                                <tr class="grandtotal third-thead-row">
                                                    <?php if (filter_input(INPUT_GET, 'export') !== 'csv') { ?>
                                                        <th class="text-right bg_grey" colspan="2">
                                                            <?php echo ($quotation_total != '') ? Yii::app()->Controller->money_format_inr($quotation_total, 2, 1) : '0.00'; ?>
                                                        </th>
                                                        <th class="text-right bg_grey" colspan="2">
                                                            <?php echo ($bill_total_amount != '') ? Yii::app()->Controller->money_format_inr($bill_total_amount, 2, 1) : '0.00'; ?>
                                                        </th>
                                                        <th class="text-right bg_grey" colspan="2">
                                                            <?php echo ($payment_total != '') ? Yii::app()->Controller->money_format_inr($payment_total, 2, 1) : '0.00'; ?>
                                                        </th>
                                                        <th class="text-right bg_grey balance_amount">
                                                            <?php echo ($balance_total != '') ? (($balance_total > 0) ? (Yii::app()->Controller->money_format_inr($balance_total, 2)) : ('<b class="red_bg">' . Yii::app()->Controller->money_format_inr($balance_total, 2) . '</b>')) : '0.00'; ?>
                                                        </th>
                                                        <th class="text-right bg_grey balance_amount">
                                                            <?php echo ($qA_minus_bill_total_balance != '') ? (($qA_minus_bill_total_balance > 0) ? (Yii::app()->Controller->money_format_inr($qA_minus_bill_total_balance, 2)) : ('<b class="red_bg">' . Yii::app()->Controller->money_format_inr($qA_minus_bill_total_balance, 2) . '</b>')) : '0.00'; ?>
                                                        </th>
                                                        <th class="text-right bg_grey balance_amount">
                                                            <?php echo ($bA_minus_paid_total_balance != '') ? (($bA_minus_paid_total_balance > 0) ? (Yii::app()->Controller->money_format_inr($bA_minus_paid_total_balance, 2)) : ('<b class="red_bg">' . Yii::app()->Controller->money_format_inr($bA_minus_paid_total_balance, 2) . '</b>')) : '0.00'; ?>
                                                        </th>
                                                    <?php } else { ?>
                                                        <td>Total</td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td><b>
                                                                <?php echo ($quotation_total != '') ? Yii::app()->Controller->money_format_inr($quotation_total, 2, 1) : '0.00'; ?>
                                                            </b></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td><b>
                                                                <?php echo ($bill_total_amount != '') ? Yii::app()->Controller->money_format_inr($bill_total_amount, 2, 1) : '0.00'; ?>
                                                            </b></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td><b>
                                                                <?php echo ($payment_total != '') ? Yii::app()->Controller->money_format_inr($payment_total, 2, 1) : '0.00'; ?>
                                                            </b></td>
                                                        <td>
                                                            <?php echo ($balance_total != '') ? (($balance_total > 0) ? ('<b>' . Yii::app()->Controller->money_format_inr($balance_total, 2) . '</b>') : ('<b class="red_bg">' . Yii::app()->Controller->money_format_inr($balance_total, 2) . '</b>')) : '0.00'; ?>
                                                        </td>
                                                        <td>
                                                            <?php echo ($qA_minus_bill_total_balance != '') ? (($qA_minus_bill_total_balance > 0) ? ('<b>' . Yii::app()->Controller->money_format_inr($qA_minus_bill_total_balance, 2) . '</b>') : ('<b class="red_bg">' . Yii::app()->Controller->money_format_inr($qA_minus_bill_total_balance, 2) . '</b>')) : '0.00'; ?>
                                                        </td>
                                                        <td>
                                                            <?php echo ($bA_minus_paid_total_balance != '') ? (($bA_minus_paid_total_balance > 0) ? ('<b>' . Yii::app()->Controller->money_format_inr($bA_minus_paid_total_balance, 2) . '</b>') : ('<b class="red_bg">' . Yii::app()->Controller->money_format_inr($bA_minus_paid_total_balance, 2) . '</b>')) : '0.00'; ?>
                                                        </td>
                                                        <?php
                                                    } ?>

                                                </tr>
                                            </thead>
                                            <?php if (filter_input(INPUT_GET, 'export') !== 'csv') { ?>
                                                <tbody>
                                                    <?php
                                                    $quotation_total = 0;
                                                    $payment_total = 0;
                                                    $balance_total = 0;
                                                    $newQuery6 = "";
                                                    $condition6 = "";
                                                    $date_array = array();
                                                    foreach ($unreconciled_data as $data) {
                                                        $recon_date = "'{$data["reconciliation_paymentdate"]}'";
                                                        array_push($date_array, $recon_date);
                                                    }

                                                    $date_array1 = implode(',', $date_array);
                                                    if (!empty($date_array1)) {
                                                        $newQuery6 .= "`date` NOT IN($date_array1) OR `date`  IS NULL";
                                                        $condition6 .= " AND " . $newQuery6 . "";
                                                    }
                                                    $kt = 0;
                                                    foreach ($final_array as $key => $quotation_list) {
                                                        $bamount_val = 0;
                                                        if (isset($quotation_list['tot_amount'], $quotation_list['payment_total'])) {
                                                            $bamount_val = $quotation_list['tot_amount'] - $quotation_list['payment_total'];
                                                        }
                                                        ?>
                                                        <tr>
                                                            <td rowspan="<?php echo $quotation_list['count'] ?>"
                                                                class="text-center prjct">
                                                                <?php echo $quotation_list['project']; ?>
                                                            </td>
                                                            <?php

                                                            $qamount = '    <td rowspan="' . $quotation_list['count'] . '" class="text-right">' . Yii::app()->Controller->money_format_inr(isset($quotation_list['tot_amount']) ? $quotation_list['tot_amount'] : 0, 2, 1) . '</td>    ';
                                                            if (isset($quotation_list['details'])) {
                                                                $qamountindex = 0;
                                                                foreach ($quotation_list['details'] as $key2 => $details) {
                                                                    $balance = 0;
                                                                    foreach ($details as $key3 => $detail) {
                                                                        $paid_amount = 0;
                                                                        if (!empty($date_from) && !empty($date_to)) {
                                                                            $datecondition = " AND p.date BETWEEN '" . date("Y-m-d", strtotime($date_from)) . "' AND '" . date("Y-m-d", strtotime($date_to)) . "'";

                                                                        } else if (!empty($date_from) && empty($date_to)) {
                                                                            $datecondition = " AND p.date >= '" . date("Y-m-d", strtotime($date_from)) . "'";

                                                                        } else if (empty($date_from) && !empty($date_to)) {
                                                                            $datecondition = " AND p.date <= '" . date("Y-m-d", strtotime($date_to)) . "'";

                                                                        } else {
                                                                            $datecondition = "";

                                                                        }
                                                                        $payment_bills_sql = "SELECT sb.id as bill_id,sb.total_amount as bill_amount,sb.bill_number,sb.date as bill_date FROM jp_subcontractorbill sb WHERE sb.scquotation_id='" . $detail['scquotation_id'] . "'";
                                                                        //die($payment_bills_sql);
                                                                        $payment_bills = Yii::app()->db->createCommand($payment_bills_sql)->queryAll();
                                                                        //echo "<pre>";print_r($payment_bills);exit;
                                            
                                                                        $payment_sql = "SELECT p.* FROM " . $this->tableNameAcc('subcontractor_payment', 0) . " p "
                                                                            . " WHERE " . " p.subcontractor_id='" . $detail['subcontractor_id'] . "' "
                                                                            . " AND p.quotation_number='" . $detail['scquotation_id'] . "' "
                                                                            . " AND p.project_id='" . $detail['project_id'] . "' "
                                                                            . " AND (p.reconciliation_status='1' OR p.reconciliation_status IS NULL) "
                                                                            . " AND p.approve_status = 'Yes' $datecondition"
                                                                            . " ORDER by p.date";
                                                                        // die($payment_sql);
                                                                        $payment_details = Yii::app()->db->createCommand($payment_sql)->queryAll();
                                                                        $paymentwithoutbillsql = "SELECT p.* FROM " . $this->tableNameAcc('subcontractor_payment', 0) . " p "
                                                                            . " WHERE p.subcontractor_id ='" . $detail['subcontractor_id'] . "' "
                                                                            . " AND p.quotation_number ='" . $detail['scquotation_id'] . "' "
                                                                            . " AND p.project_id='" . $detail['project_id'] . "' "
                                                                            . " AND (p.reconciliation_status='1' OR p.reconciliation_status IS NULL) AND p.sc_bill_id IS NULL "
                                                                            . " AND p.approve_status = 'Yes'";
                                                                        // die($paymentwithoutbillsql);
                                                                        $payment_detailswithoutBill = Yii::app()->db->createCommand($paymentwithoutbillsql)->queryAll();
                                                                        if (isset($detail['scquotation_decription'])) {
                                                                            $i = 1; ?>
                                                                            <td class="text-center nowrap">
                                                                                <?php echo Yii::app()->Controller->changeDateForamt($key2) ?>
                                                                            </td>
                                                                            <td>
                                                                                <?php echo $detail['scquotation_decription'] ?>
                                                                            </td>
                                                                            <td class="text-center">
                                                                                <?php echo $detail['scquotation_no'] ?>
                                                                            </td>
                                                                            <td class="text-right">
                                                                                <?php echo Yii::app()->Controller->money_format_inr($detail['amount'], 2, 1) ?>
                                                                            </td>
                                                                            <?php
                                                                            if ($qamountindex == 0) {
                                                                                echo $qamount;
                                                                            }
                                                                            $count = '';
                                                                            if (!empty($payment_bills)) {
                                                                                $count = count($payment_bills);
                                                                            } ?>
                                                                            <td class="tbl_holder tr">
                                                                                <table>
                                                                                    <?php if (!empty($payment_bills)) {
                                                                                        foreach ($payment_bills as $payment) {
                                                                                            if (!empty($payment['bill_id'])) {
                                                                                                ?>
                                                                                                <tr>
                                                                                                    <td class="text-center nowrap">
                                                                                                        <?php echo "#" . $payment['bill_id']; ?>
                                                                                                    </td>
                                                                                                </tr>

                                                                                                <?php
                                                                                            } else { ?>
                                                                                                <tr>
                                                                                                    <td class="text-center nowrap">
                                                                                                        <?php echo "#" ?>
                                                                                                    </td>
                                                                                                </tr>

                                                                                            <?php }
                                                                                        }
                                                                                    } ?>
                                                                                    <?php
                                                                                    if (!empty($payment_detailswithoutBill)) {
                                                                                        foreach ($payment_detailswithoutBill as $payment) {
                                                                                            ?>
                                                                                            <tr>
                                                                                                <td class="text-center nowrap">
                                                                                                    <?php echo "-"; ?>
                                                                                                </td>
                                                                                            </tr>

                                                                                            <?php
                                                                                        }
                                                                                    }
                                                                                    ?>
                                                                                </table>
                                                                            </td>
                                                                            <td class="tbl_holder">
                                                                                <table>
                                                                                    <?php
                                                                                    if (!empty($payment_bills)) {
                                                                                        foreach ($payment_bills as $payment) {
                                                                                            ?>
                                                                                            <tr>
                                                                                                <td class="text-center nowrap">
                                                                                                    <?php echo $payment['bill_number'] ?>
                                                                                                </td>
                                                                                            </tr>

                                                                                            <?php
                                                                                        }
                                                                                    }
                                                                                    ?>
                                                                                    <?php
                                                                                    if (!empty($payment_detailswithoutBill)) {
                                                                                        foreach ($payment_detailswithoutBill as $payment) {
                                                                                            ?>
                                                                                            <tr>
                                                                                                <td class="text-center nowrap">
                                                                                                    <?php echo "-"; ?>
                                                                                                </td>
                                                                                            </tr>

                                                                                            <?php
                                                                                        }
                                                                                    }
                                                                                    ?>
                                                                                </table>
                                                                            </td>
                                                                            <td class="tbl_holder">
                                                                                <table>
                                                                                    <?php
                                                                                    if (!empty($payment_bills)) {
                                                                                        foreach ($payment_bills as $payment) {
                                                                                            ?>
                                                                                            <tr>
                                                                                                <td class="text-center nowrap">
                                                                                                    <?php
                                                                                                    echo Yii::app()->Controller->changeDateForamt($payment['bill_date']) ?>
                                                                                                </td>
                                                                                            </tr>

                                                                                            <?php
                                                                                        }
                                                                                    }
                                                                                    ?>
                                                                                    <?php
                                                                                    if (!empty($payment_detailswithoutBill)) {
                                                                                        foreach ($payment_detailswithoutBill as $payment) {
                                                                                            ?>
                                                                                            <tr>
                                                                                                <td class="text-center nowrap">
                                                                                                    <?php echo "-"; ?>
                                                                                                </td>
                                                                                            </tr>

                                                                                            <?php
                                                                                        }
                                                                                    }
                                                                                    ?>
                                                                                </table>
                                                                            </td>
                                                                            <td class="tbl_holder">
                                                                                <table>
                                                                                    <?php
                                                                                    $bill_amount = 0;
                                                                                    if (!empty($payment_bills)) {
                                                                                        foreach ($payment_bills as $payment) {
                                                                                            if (!empty($payment['bill_amount'])) {

                                                                                                $bill_amount = $payment['bill_amount'];
                                                                                                ?>
                                                                                                <tr>
                                                                                                    <td class="text-center nowrap">
                                                                                                        <?php echo Yii::app()->Controller->money_format_inr($bill_amount, 2, 1); ?>
                                                                                                    </td>
                                                                                                </tr>

                                                                                                <?php
                                                                                            } else { ?>
                                                                                                <tr>
                                                                                                    <td class="text-center nowrap">
                                                                                                        <?php echo Yii::app()->Controller->money_format_inr($bill_amount, 2, 1); ?>
                                                                                                    </td>
                                                                                                </tr>

                                                                                            <?php }
                                                                                        }
                                                                                    }
                                                                                    ?>
                                                                                    <?php
                                                                                    if (!empty($payment_detailswithoutBill)) {
                                                                                        foreach ($payment_detailswithoutBill as $payment) {
                                                                                            ?>
                                                                                            <tr>
                                                                                                <td class="text-center nowrap">
                                                                                                    <?php echo Yii::app()->Controller->money_format_inr(0, 2, 1) ?>
                                                                                                </td>
                                                                                            </tr>

                                                                                            <?php
                                                                                        }
                                                                                    }
                                                                                    ?>
                                                                                </table>
                                                                            </td>
                                                                            <td class="text-right">
                                                                                <?php echo isset($detail['total_bill_amount']) ? Yii::app()->Controller->money_format_inr($detail['total_bill_amount'], 2, 1) : '' ?>
                                                                            </td>

                                                                            <td class="tbl_holder">
                                                                                <table>
                                                                                    <?php
                                                                                    if (!empty($payment_bills)) {
                                                                                        foreach ($payment_bills as $billpayments) {
                                                                                            $payment_sql = "SELECT p.* FROM " . $this->tableNameAcc('subcontractor_payment', 0) . " p "
                                                                                                . " WHERE " . " p.subcontractor_id='" . $detail['subcontractor_id'] . "' "
                                                                                                . " AND p.quotation_number='" . $detail['scquotation_id'] . "' "
                                                                                                . " AND p.project_id='" . $detail['project_id'] . "' "
                                                                                                . " AND (p.reconciliation_status='1' OR p.reconciliation_status IS NULL) "
                                                                                                . " AND p.approve_status = 'Yes' AND p.sc_bill_id='" . $billpayments['bill_id'] . "' $datecondition"
                                                                                                . " ORDER by p.date";
                                                                                            // die($payment_sql);
                                                                                            $payment_details = Yii::app()->db->createCommand($payment_sql)->queryAll();

                                                                                            if (!empty($payment_details)) {
                                                                                                foreach ($payment_details as $payment) {
                                                                                                    ?>
                                                                                                    <tr>
                                                                                                        <td class="text-center nowrap">
                                                                                                            #
                                                                                                            <?php echo $payment['payment_id'] ?>
                                                                                                        </td>
                                                                                                    </tr>

                                                                                                    <?php
                                                                                                }
                                                                                            } else { ?>
                                                                                                <tr>
                                                                                                    <td class="text-center nowrap">
                                                                                                        <?php echo '-' ?>
                                                                                                    </td>
                                                                                                </tr>

                                                                                            <?php }
                                                                                        }
                                                                                    }
                                                                                    ?>
                                                                                    <?php
                                                                                    if (!empty($payment_detailswithoutBill)) {
                                                                                        foreach ($payment_detailswithoutBill as $payment) {
                                                                                            ?>
                                                                                            <tr>
                                                                                                <td class="text-center nowrap">
                                                                                                    #
                                                                                                    <?php echo $payment['payment_id'] ?>
                                                                                                </td>
                                                                                            </tr>

                                                                                            <?php
                                                                                        }
                                                                                    }
                                                                                    ?>
                                                                                </table>
                                                                            </td>
                                                                            <td class="tbl_holder">
                                                                                <table>
                                                                                    <?php
                                                                                    if (!empty($payment_bills)) {
                                                                                        foreach ($payment_bills as $billpayments) {
                                                                                            $payment_sql = "SELECT p.* FROM " . $this->tableNameAcc('subcontractor_payment', 0) . " p "
                                                                                                . " WHERE " . " p.subcontractor_id='" . $detail['subcontractor_id'] . "' "
                                                                                                . " AND p.quotation_number='" . $detail['scquotation_id'] . "' "
                                                                                                . " AND p.project_id='" . $detail['project_id'] . "' "
                                                                                                . " AND (p.reconciliation_status='1' OR p.reconciliation_status IS NULL) "
                                                                                                . " AND p.approve_status = 'Yes' AND p.sc_bill_id='" . $billpayments['bill_id'] . "' $datecondition"
                                                                                                . " ORDER by p.date";
                                                                                            // die($payment_sql);
                                                                                            $payment_details = Yii::app()->db->createCommand($payment_sql)->queryAll();

                                                                                            if (!empty($payment_details)) {
                                                                                                foreach ($payment_details as $payment) {
                                                                                                    ?>
                                                                                                    <tr>
                                                                                                        <td class="text-center nowrap">
                                                                                                            <?php echo Yii::app()->Controller->changeDateForamt($payment['date']) ?>
                                                                                                        </td>
                                                                                                    </tr>

                                                                                                    <?php
                                                                                                }
                                                                                            } else { ?>
                                                                                                <tr>
                                                                                                    <td class="text-center nowrap">
                                                                                                        <?php echo '-' ?>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            <?php }
                                                                                        }
                                                                                    }
                                                                                    ?>
                                                                                    <?php
                                                                                    if (!empty($payment_detailswithoutBill)) {
                                                                                        foreach ($payment_detailswithoutBill as $payment) {
                                                                                            ?>
                                                                                            <tr>
                                                                                                <td class="text-center nowrap">
                                                                                                    <?php echo Yii::app()->Controller->changeDateForamt($payment['date']) ?>
                                                                                                </td>
                                                                                            </tr>

                                                                                            <?php
                                                                                        }
                                                                                    }
                                                                                    ?>
                                                                                </table>
                                                                            </td>
                                                                            <td class="tbl_holder">
                                                                                <table>
                                                                                    <?php
                                                                                    if (!empty($payment_bills)) {
                                                                                        foreach ($payment_bills as $billpayments) {
                                                                                            $payment_sql = "SELECT p.* FROM " . $this->tableNameAcc('subcontractor_payment', 0) . " p "
                                                                                                . " WHERE " . " p.subcontractor_id='" . $detail['subcontractor_id'] . "' "
                                                                                                . " AND p.quotation_number='" . $detail['scquotation_id'] . "' "
                                                                                                . " AND p.project_id='" . $detail['project_id'] . "' "
                                                                                                . " AND (p.reconciliation_status='1' OR p.reconciliation_status IS NULL) "
                                                                                                . " AND p.approve_status = 'Yes' AND p.sc_bill_id='" . $billpayments['bill_id'] . "' $datecondition"
                                                                                                . " ORDER by p.date";
                                                                                            // die($payment_sql);
                                                                                            $payment_details = Yii::app()->db->createCommand($payment_sql)->queryAll();

                                                                                            if (!empty($payment_details)) {
                                                                                                foreach ($payment_details as $payment) {

                                                                                                    ?>
                                                                                                    <tr>
                                                                                                        <td class="text-center nowrap">
                                                                                                            <?php echo trim($payment['description']) ?>
                                                                                                        </td>
                                                                                                    </tr>

                                                                                                    <?php
                                                                                                }
                                                                                            } else { ?>
                                                                                                <tr>
                                                                                                    <td class="text-center nowrap">
                                                                                                        <?php echo '-' ?>
                                                                                                    </td>
                                                                                                </tr>

                                                                                            <?php }
                                                                                        }
                                                                                    }

                                                                                    ?>
                                                                                    <?php
                                                                                    if (!empty($payment_detailswithoutBill)) {
                                                                                        foreach ($payment_detailswithoutBill as $payment) {
                                                                                            ?>
                                                                                            <tr>

                                                                                                <td>
                                                                                                    <?php echo trim($payment['description']) ?>
                                                                                                </td>

                                                                                            </tr>

                                                                                            <?php
                                                                                        }
                                                                                    }
                                                                                    ?>

                                                                                </table>
                                                                            </td>
                                                                            <td class="tbl_holder">
                                                                                <table>
                                                                                    <?php

                                                                                    $writeoffAmt = 0;
                                                                                    if (!empty($detail['scquotation_id'])) {
                                                                                        $writeoff = Writeoff::model()->find(array('condition' => 'subcontractor_id = ' . $detail['scquotation_id']));
                                                                                        $writeoffAmt = ($writeoff) ? $writeoff->amount : "0.00";

                                                                                    }

                                                                                    ?>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <?php echo $writeoffAmt; ?>
                                                                                        </td>
                                                                                    </tr>

                                                                                    <?php

                                                                                    ?>

                                                                                </table>
                                                                            </td>
                                                                            <td class="tbl_holder">
                                                                                <table>
                                                                                    <?php
                                                                                    $pamount_sum = 0;
                                                                                    if (!empty($payment_bills)) {
                                                                                        foreach ($payment_bills as $billpayments) {
                                                                                            $payment_sql = "SELECT p.*  FROM " . $this->tableNameAcc('subcontractor_payment', 0) . " p "
                                                                                                . " WHERE " . " p.subcontractor_id='" . $detail['subcontractor_id'] . "' "
                                                                                                . " AND p.quotation_number='" . $detail['scquotation_id'] . "' "
                                                                                                . " AND p.project_id='" . $detail['project_id'] . "' "
                                                                                                . " AND (p.reconciliation_status='1' OR p.reconciliation_status IS NULL) "
                                                                                                . " AND p.approve_status = 'Yes' AND p.sc_bill_id='" . $billpayments['bill_id'] . "' $datecondition"
                                                                                                . " ORDER by p.date";
                                                                                            // die($payment_sql);
                                                                                            $payment_details = Yii::app()->db->createCommand($payment_sql)->queryAll();

                                                                                            if (!empty($payment_details)) {
                                                                                                foreach ($payment_details as $payment) {


                                                                                                    ?>
                                                                                                    <tr>
                                                                                                        <td class="text-right">
                                                                                                            <?php echo Yii::app()->Controller->money_format_inr($payment['paidamount'], 2, 1) ?>
                                                                                                        </td>
                                                                                                    </tr>

                                                                                                    <?php
                                                                                                    $pamount_sum += $payment['paidamount'];
                                                                                                }
                                                                                            } else { ?>
                                                                                                <tr>
                                                                                                    <td class="text-right">
                                                                                                        <?php echo Yii::app()->Controller->money_format_inr(0, 2, 1) ?>
                                                                                                    </td>
                                                                                                </tr>

                                                                                                <?php
                                                                                            }

                                                                                        }
                                                                                    }

                                                                                    ?>
                                                                                    <?php
                                                                                    if (!empty($payment_detailswithoutBill)) {
                                                                                        foreach ($payment_detailswithoutBill as $payments) {
                                                                                            ?>
                                                                                            <tr>

                                                                                                <td class="text-right">
                                                                                                    <?php echo Yii::app()->Controller->money_format_inr($payments['paidamount'], 2, 1) ?>
                                                                                                </td>

                                                                                            </tr>

                                                                                            <?php
                                                                                            $pamount_sum += $payments['paidamount'];
                                                                                        }
                                                                                    } else { ?>
                                                                                        <tr>

                                                                                            <td class="text-right">
                                                                                                <?php echo Yii::app()->Controller->money_format_inr(0, 2, 1) ?>
                                                                                            </td>

                                                                                        </tr>

                                                                                    <?php }
                                                                                    ?>

                                                                                </table>
                                                                            </td>

                                                                            <td class="text-right ">
                                                                                <?php echo isset($detail['totalpayment']) ? Yii::app()->Controller->money_format_inr($detail['totalpayment'], 2, 1) : '' ?>
                                                                            </td>

                                                                            <td class="text-right ">
                                                                                <?php echo isset($detail['bal_total']) ? Yii::app()->Controller->money_format_inr($detail['bal_total'], 2) : '' ?>
                                                                            </td>

                                                                            <td class="text-right ">
                                                                                <?php echo isset($detail['balance_QA_BA']) ? Yii::app()->Controller->money_format_inr($detail['balance_QA_BA'], 2) : '' ?>
                                                                            </td>
                                                                            <td class="text-right ">
                                                                                <?php echo isset($detail['balance_BA_PA']) ? Yii::app()->Controller->money_format_inr($detail['balance_BA_PA'], 2) : '' ?>
                                                                            </td>
                                                                        </tr>
                                                                        <?php
                                                                        }
                                                                        ?>
                                                                    <?php
                                                                    if ($i !== $quotation_list['count']) {
                                                                        echo '<tr>';
                                                                        $kt++;
                                                                    }
                                                                    ?>
                                                                    <?php
                                                                    $i++;
                                                                    $qamountindex++;
                                                                    }
                                                                    ?>

                                                                <?php
                                                                }

                                                                ?>
                                                            <?php
                                                            }
                                                            ?>

                                                        <?php

                                                    }
                                                    ?>
                                                </tbody>
                                            <?php } else {
                                                $this->renderPartial('_report_data_csv', array('final_array' => $final_array, 'final_array2' => $final_array2,'final_array3'=>$final_array3, 'datecondition' => $datecondition, 'reconcile_or_unrecon' => "1"));
                                            } ?>
                                        </table>
                                    </div>
                                </div>
                                <?php
                }
                if (filter_input(INPUT_GET, 'export') == 'pdf') { ?>
                            </div>
                        </div>
                    </div>
                <?php } ?>
                <?php if (filter_input(INPUT_GET, 'export') == 'pdf') { ?>
                    <div class="content">
                        <div class="wrapper">
                            <div class="holder">
                            <?php }
                                if (isset($_REQUEST['custId']) && ($_REQUEST['custId'] == 'unreconcil')) { ?>
                                                <div class="row margin-top-10">
                                                    <div class="col-xs-12">
                                                        <div class="heading-title">Payment Against Quotation- UnReconciled</div>
                                                        <div class="dotted-line"></div>
                                                    </div>
                                                </div>
                                                <div class="unreconcil_table table-wrapper margin-top-10">
                                                    <div class="d-flex">
                                                        <div class="summary text-right margin-left-auto">Total
                                                            <?php echo count($final_array) ?> results.
                                                        </div>

                                                    </div>
                                                    <div id="table-wrapper">
                                                        <table
                                                            class="table total-table   <?php echo filter_input(INPUT_GET, 'export') == 'pdf' ? 'detail-table table_one' : 'table-bordered' ?> alt_bg_table">
                                                            <?php
                                                            $quotation_total = 0;
                                                            $payment_total = 0;
                                                            $balance_total = 0;
                                                            $payment_amount = 0;

                                                            foreach ($final_array as $key => $value) {
                                                                $final_array[$key]['count'] = 1;
                                                                $j = 0;
                                                                $a = 0;
                                                                $blance = 0;
                                                                $check = 0;
                                                                $sum = 0;
                                                                $payment_sum = 0;
                                                                foreach ($value['details'] as $key2 => $quotations) {
                                                                    foreach ($quotations as $quotation) {
                                                                        $sum += $quotation['amount'];
                                                                        $paymsql = "SELECT * FROM " . $this->tableNameAcc('subcontractor_payment', 0) . " "
                                                                            . " WHERE subcontractor_id = '" . $quotation['subcontractor_id'] . "' "
                                                                            . " AND quotation_number = '" . $quotation['scquotation_id'] . "' "
                                                                            . " AND project_id = '" . $quotation['project_id'] . "' "
                                                                            . "  AND ((reconciliation_status = 0  AND approve_status = 'No')"
                                                                            . " OR (reconciliation_status = 0  AND approve_status = 'Yes')"
                                                                            . " OR (reconciliation_status IS NULL  AND approve_status = 'No')) ";

                                                                        $payment_details = Yii::app()->db->createCommand($paymsql)->queryAll();

                                                                        $payment_sum = $model->getSubcontractorPaymentTotal1($quotation);

                                                                        foreach ($payment_details as $payment_detail) {
                                                                            $payment_total += $payment_detail['amount'];
                                                                        }
                                                                        $check++;
                                                                        $final_array[$key]['count'] = $check;
                                                                        $final_array[$key]['tot_amount'] = $sum;
                                                                        $final_array[$key]['payment_total'] = $payment_sum;
                                                                        $quotation_total += $quotation['amount'];
                                                                    }
                                                                }
                                                            }
                                                            $balance_total = $quotation_total - $payment_total;
                                                            ?>
                                                            <thead class="entry-table sticky-thead">
                                                                <?php if (filter_input(INPUT_GET, 'export') === 'csv') {
                                                                    ?>
                                                                    <tr>
                                                                        <td>Payment Against Quotation</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="text-center"><b>Project Name</b></td>
                                                                        <td class="text-center"><b>Date</b></td>
                                                                        <td class="text-center"><b>Description</b></td>
                                                                        <td class="text-center"><b>Quotation Number</b></td>
                                                                        <td class="text-center"><b>Quotation Amount</b></td>


                                                                        <td class="text-center"><b>Date</b></td>
                                                                        <td class="text-center"><b>Bill No</b></td>
                                                                        <td class="text-center"><b>stage</b></td>
                                                                        <td class="text-center"><b>Description</b></td>

                                                                        <td class="text-center"><b>Paid Amount</b></td>
                                                                        <td class="text-center"><b>Balance</b></td>

                                                                    </tr>
                                                                    <?php
                                                                } else {
                                                                    ?>
                                                                    <tr class="first-thead-row">
                                                                        <th rowspan="3" class="text-center"><b>Project Name</b></th>
                                                                        <th colspan="5" class="text-center"><b>Quotation details</b></th>
                                                                        <th colspan="7" class="text-center"><b>Payment details</b></th>
                                                                        <th rowspan="2" class="text-center"><b>Balance</b></th>

                                                                    </tr>
                                                                    <tr class="second-thead-row">

                                                                        <th rowspan="2" class="text-center"><b>Date</b></th>
                                                                        <th rowspan="2" class="text-center"><b>Description</b></th>
                                                                        <th rowspan="2" class="text-center"><b>Quotation Number</b></th>
                                                                        <th class="text-center" colspan="2"><b>Amount</b></th>
                                                                        <th rowspan="2" class="text-center"><b>#ID</b></th>
                                                                        <th rowspan="2" class="text-center"><b>Bill No</b></th>
                                                                        <th rowspan="2" class="text-center"><b>Stage</b></th>
                                                                        <th rowspan="2" class="text-center"><b>Date</b></th>
                                                                        <th rowspan="2" class="text-center"><b>Description</b></th>
                                                                        <th class="text-center" colspan="2"><b>Amount</b></th>

                                                                    </tr>
                                                                    <?php
                                                                } ?>

                                                                <tr class="grandtotal third-thead-row">
                                                                    <?php if (filter_input(INPUT_GET, 'export') !== 'csv') { ?>
                                                                        <th class="text-right bg_grey" colspan="2"><b>
                                                                                <?php echo ($quotation_total != '') ? Yii::app()->Controller->money_format_inr($quotation_total, 2, 1) : '0.00'; ?>
                                                                            </b></th>
                                                                        <th class="text-right bg_grey" colspan="2"><b>
                                                                                <?php echo ($payment_total != '') ? Yii::app()->Controller->money_format_inr($payment_total, 2, 1) : '0.00'; ?>
                                                                            </b></th>
                                                                        <th class="text-right bg_grey balance_amount">

                                                                            <?php echo ($balance_total != '') ? (($balance_total > 0) ? ('<b>' . Yii::app()->Controller->money_format_inr($balance_total, 2) . '</b>') : ('<b class="red_bg">' . Yii::app()->Controller->money_format_inr($balance_total, 2) . '</b>')) : '0.00'; ?>
                                                                        </th>
                                                                    <?php } else {
                                                                        ?>
                                                                        <td>Total</td>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td><b>
                                                                                <?php echo ($quotation_total != '') ? Yii::app()->Controller->money_format_inr($quotation_total, 2, 1) : '0.00'; ?>
                                                                            </b></td>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td><b>
                                                                                <?php echo ($payment_total != '') ? Yii::app()->Controller->money_format_inr($payment_total, 2, 1) : '0.00'; ?>
                                                                            </b></td>
                                                                        <td>
                                                                            <?php echo ($balance_total != '') ? (($balance_total > 0) ? ('<b>' . Yii::app()->Controller->money_format_inr($balance_total, 2) . '</b>') : ('<b class="red_bg">' . Yii::app()->Controller->money_format_inr($balance_total, 2) . '</b>')) : '0.00'; ?>
                                                                        </td>

                                                                        <?php
                                                                    } ?>

                                                                </tr>
                                                            </thead>
                                                            <?php if (filter_input(INPUT_GET, 'export') !== 'csv') { ?>
                                                                <tbody>
                                                                    <?php
                                                                    $quotation_total = 0;
                                                                    $payment_total = 0;
                                                                    $balance_total = 0;
                                                                    $date_array = array();
                                                                    foreach ($unreconciled_data as $data) {
                                                                        $recon_date = "'{$data["reconciliation_paymentdate"]}'";
                                                                        array_push($date_array, $recon_date);
                                                                    }

                                                                    $date_array1 = implode(',', $date_array);

                                                                    //echo "<pre>";print_r($final_array);exit;
                                                                    foreach ($final_array as $key => $quotation_list) {
                                                                        $bamount_val = 0;
                                                                        if (isset($quotation_list['tot_amount'], $quotation_list['payment_total'])) {
                                                                            $bamount_val = $quotation_list['tot_amount'] - $quotation_list['payment_total'];
                                                                        }
                                                                        ?>
                                                                        <tr>
                                                                            <td rowspan="<?php echo $quotation_list['count'] ?>"
                                                                                class="text-center prjct">
                                                                                <?php echo $quotation_list['project']; ?>
                                                                            </td>
                                                                            <?php
                                                                            $qamount = '    <td rowspan="' . $quotation_list['count'] . '" class="text-right">' . Yii::app()->Controller->money_format_inr(isset($quotation_list['tot_amount']) ? $quotation_list['tot_amount'] : 0, 2, 1) . '</td>    ';
                                                                            $pamount = '    <td rowspan="' . $quotation_list['count'] . '" class="text-right">' . Yii::app()->Controller->money_format_inr(isset($quotation_list['payment_total']) ? $quotation_list['payment_total'] : 0, 2, 1) . '</td>    ';
                                                                            $bamount = '    <td rowspan="' . $quotation_list['count'] . '" class="text-right">' . Yii::app()->Controller->money_format_inr($bamount_val, 2) . '</td>    ';
                                                                            ?>

                                                                            <?php

                                                                            if (isset($quotation_list['details'])) {
                                                                                $qamountindex = 0;


                                                                                foreach ($quotation_list['details'] as $key2 => $details) {
                                                                                    $balance = 0;

                                                                                    foreach ($details as $key3 => $detail) {
                                                                                        $paid_amount = 0;

                                                                                        $paydetails_sql = "SELECT * FROM `jp_subcontractor_payment` "
                                                                                            . " WHERE `subcontractor_id` = '" . $detail['subcontractor_id'] . "' "
                                                                                            . " AND `quotation_number` = '" . $detail['scquotation_id'] . "' "
                                                                                            . " AND `project_id` = '" . $detail['project_id'] . "' "
                                                                                            . " AND ((reconciliation_status = 0  AND approve_status = 'No')"
                                                                                            . " OR (reconciliation_status = 0  AND approve_status = 'Yes')"
                                                                                            . " OR (reconciliation_status IS NULL  AND approve_status = 'No')) "
                                                                                            . "  ORDER BY `date` ASC";

                                                                                        $payment_details11 = Yii::app()->db->createCommand($paydetails_sql)->queryAll();


                                                                                        if (isset($detail['scquotation_decription'])) {

                                                                                            $i = 1;
                                                                                            ?>
                                                                                            <td class="text-center nowrap">
                                                                                                <?php echo Yii::app()->Controller->changeDateForamt($key2) ?>
                                                                                            </td>
                                                                                            <td>
                                                                                                <?php echo $detail['scquotation_decription'] ?>
                                                                                            </td>
                                                                                            <td class="text-center">
                                                                                                <?php echo $detail['scquotation_no'] ?>
                                                                                            </td>
                                                                                            <td class="text-right">
                                                                                                <?php echo Yii::app()->Controller->money_format_inr($detail['amount'], 2, 1) ?>
                                                                                            </td>
                                                                                            <?php
                                                                                            if ($qamountindex == 0) {
                                                                                                echo $qamount;
                                                                                            }
                                                                                            ?>
                                                                                            <td class="tbl_holder">
                                                                                                <table>
                                                                                                    <?php
                                                                                                    foreach ($payment_details11 as $payment) {
                                                                                                        ?>
                                                                                                        <tr>
                                                                                                            <td class="text-center nowrap">
                                                                                                                #
                                                                                                                <?php echo $payment['payment_id'] ?>
                                                                                                            </td>
                                                                                                        </tr>

                                                                                                        <?php
                                                                                                    }
                                                                                                    ?>
                                                                                                </table>
                                                                                            </td>
                                                                                            <td class="tbl_holder">
                                                                                                <table>
                                                                                                    <?php
                                                                                                    foreach ($payment_details11 as $payment) {
                                                                                                        ?>
                                                                                                        <tr>
                                                                                                            <td class="text-center nowrap">
                                                                                                                <?php
                                                                                                                $bill = "Nil";
                                                                                                                if (isset($payment['sc_bill_id'])) {
                                                                                                                    $stagebillmodel = Subcontractorbill::model()->findByPk($payment['sc_bill_id']);
                                                                                                                    $bill = $stagebillmodel['bill_number'];

                                                                                                                }

                                                                                                                echo $bill; ?>
                                                                                                            </td>
                                                                                                        </tr>

                                                                                                        <?php
                                                                                                    }
                                                                                                    ?>
                                                                                                </table>
                                                                                            </td>
                                                                                            <td class="tbl_holder">
                                                                                                <table>
                                                                                                    <?php
                                                                                                    foreach ($payment_details11 as $payment) {
                                                                                                        ?>
                                                                                                        <tr>
                                                                                                            <td class="text-center nowrap">
                                                                                                                <?php
                                                                                                                $stage = "Nil";
                                                                                                                if (isset($payment['stage_id'])) {
                                                                                                                    $stagemodel = ScPaymentStage::model()->findByPk($payment['stage_id']);
                                                                                                                    $stage = $stagemodel['payment_stage'];

                                                                                                                }

                                                                                                                echo $stage; ?>
                                                                                                            </td>
                                                                                                        </tr>

                                                                                                        <?php
                                                                                                    }
                                                                                                    ?>
                                                                                                </table>
                                                                                            </td>
                                                                                            <td class="tbl_holder">
                                                                                                <table>
                                                                                                    <?php
                                                                                                    foreach ($payment_details11 as $payment) {
                                                                                                        ?>
                                                                                                        <tr>
                                                                                                            <td class="text-center nowrap">
                                                                                                                <?php echo Yii::app()->Controller->changeDateForamt($payment['date']) ?>
                                                                                                            </td>
                                                                                                        </tr>

                                                                                                        <?php
                                                                                                    }
                                                                                                    ?>
                                                                                                </table>
                                                                                            </td>
                                                                                            <td class="tbl_holder">
                                                                                                <table>
                                                                                                    <?php
                                                                                                    foreach ($payment_details11 as $payment) {

                                                                                                        ?>
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <?php echo $payment['description'] ?>
                                                                                                            </td>
                                                                                                        </tr>

                                                                                                        <?php
                                                                                                    }
                                                                                                    ?>

                                                                                                </table>
                                                                                            </td>
                                                                                            <td class="tbl_holder">
                                                                                                <table>
                                                                                                    <?php
                                                                                                    $pamount_sum = 0;
                                                                                                    foreach ($payment_details11 as $payment) {

                                                                                                        ?>
                                                                                                        <tr>
                                                                                                            <td class="text-right">
                                                                                                                <?php echo Yii::app()->Controller->money_format_inr($payment['paidamount'], 2, 1) ?>
                                                                                                            </td>
                                                                                                        </tr>

                                                                                                        <?php
                                                                                                        $pamount_sum += $payment['paidamount'];
                                                                                                    }

                                                                                                    ?>

                                                                                                </table>
                                                                                            </td>
                                                                                            <?php
                                                                                            if ($qamountindex == 0) {

                                                                                                echo $pamount;
                                                                                            }
                                                                                            ?>
                                                                                            <?php
                                                                                            if ($qamountindex == 0) {
                                                                                                echo $bamount;
                                                                                            }
                                                                                            ?>
                                                                                        </tr>
                                                                                        <?php
                                                                                        }
                                                                                        ?>
                                                                                    <?php
                                                                                    if ($i !== $quotation_list['count']) {
                                                                                        echo '<tr>';
                                                                                    }
                                                                                    ?>
                                                                                    <?php
                                                                                    $i++;
                                                                                    $qamountindex++;
                                                                                    }
                                                                                    ?>

                                                                                <?php
                                                                                }

                                                                                ?>
                                                                            <?php
                                                                            }
                                                                            ?>

                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </tbody>
                                                            <?php } else {


                                                                //echo "<pre>";print_r($final_array);exit;
                                                                $this->renderPartial('_report_data_unreconcil_csv', array('final_array' => $final_array, 'final_array2' => $final_array2,'final_array3'=>$final_array3, 'reconcile_or_unrecon' => "2"));
                                                            } ?>
                                                        </table>
                                                    </div>
                                                </div>
                                                <?php
                                }
                            if (filter_input(INPUT_GET, 'export') == 'pdf') { ?>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                <br /><br />
                <pagebreak></pagebreak>
                <?php if (filter_input(INPUT_GET, 'export') == 'pdf') { ?>
                    <div class="content">
                        <div class="wrapper">
                            <div class="holder">
                            <?php } ?>
                            <?php 
                            if (filter_input(INPUT_GET, 'export') !== 'csv') {
                                if (isset($_REQUEST['custId']) && ($_REQUEST['custId'] == 'reconcil')) { ?>
                                    <div class="row margin-top-10">
                                        <div class="col-xs-12">
                                            <div class="heading-title">Payment Without Quotation - reconcil</div>
                                            <div class="dotted-line"></div>
                                        </div>
                                    </div>
                                    <div class="reconcil_table table-wrapper margin-top-10">
                                        <div class="d-flex">
                                            <div class="summary text-right margin-left-auto">Total
                                                <?php echo count($final_array2) ?> results.
                                            </div>

                                        </div>
                                        <div id="table-wrapper">
                                            <table
                                                class="table total-table <?php echo filter_input(INPUT_GET, 'export') == 'pdf' ? 'detail-table text-sm-table' : 'table-bordered' ?>">
                                                <?php
                                                $advance_total = 0;
                                                $payment_total = 0;
                                                $balance_total = 0;

                                                foreach ($final_array2 as $key => $value) {
                                                    $j = 0;
                                                    $a = 0;
                                                    $blance = 0;
                                                    $expense_sum = 0;
                                                    $adv_sum = 0;
                                                    if (!empty($value['details'])) {
                                                        foreach ($value['details'] as $key2 => $value2) {
                                                            if (isset($value2['advance_amount'])) {
                                                                $advance_amount = $value2['advance_amount'];
                                                                $adv_sum += $advance_amount;
                                                            } else {
                                                                $advance_amount = 0;
                                                            }
                                                            if (isset($value2['payment_amount'])) {
                                                                $payment_amount = $value2['payment_amount'];
                                                                $expense_sum += $payment_amount;
                                                            } else {
                                                                $payment_amount = 0;
                                                            }

                                                            $blance += $advance_amount - $payment_amount;
                                                            if ($a >= 5) {
                                                                $blance = $blance + $advance_amount;
                                                            } else {
                                                                $blance = $blance;
                                                            }
                                                            $advance_total += $advance_amount;
                                                            $payment_total += $payment_amount;
                                                            $balance_total += $blance;
                                                            $final_array2[$key]['expense_amount'] = $expense_sum;
                                                            $final_array2[$key]['adv_amount'] = $adv_sum;
                                                        }
                                                    }
                                                }
                                                $balance_sum = $payment_total - $advance_total;
                                                ?>
                                                <thead class="entry-table sticky-thead">
                                                    <tr class="first-thead-row">
                                                        <th rowspan="3" class="text-center"><b>Project Name</b></th>
                                                       
                                                        <th colspan="5" class="text-center"><b>Payment details</b></th>
                                                        <th rowspan="2" class="text-center"><b>Balance</b></th>
                                                    </tr>
                                                    <tr class="second-thead-row">
                                                       
                                                        <th rowspan="2" class="text-center"><b>#ID</b></th>
                                                        <th rowspan="2" class="text-center"><b>Date</b></th>
                                                        
                                                        <th rowspan="2" class="text-center"><b>Description</b></th>
                                                        <th class="text-center" colspan="2"><b>Amount</b></th>
                                                    </tr>
                                                    <tr class="grandtotal third-thead-rows">
                                                        
                                                        <th class="text-right bg_grey" colspan="2">
                                                            <b>
                                                                <?php echo ($advance_total != '') ? Controller::money_format_inr($advance_total, 2, 1) : '0.00'; ?>
                                                            </b>
                                                        </th>
                                                        <th class="text-right bg_grey">

                                                            <?php echo ($balance_sum != '') ? ($balance_sum > 0 ? '<b>' . Yii::app()->Controller->money_format_inr($balance_sum, 2) . '</b>' : '<b class="red_bg">' . Yii::app()->Controller->money_format_inr($balance_sum, 2) . '</b>') : '0.00'; ?>
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                    <?php
                                                    //echo "<pre>"; print_r($final_array2);exit;
                                                    $advance_amount_total = 0;
                                                    foreach ($final_array2 as $project) { ?>
                                                        <?php if (!empty($project['details'])) {
                                                            $rowCount = count($project['details']);
                                                            $balanceAmount = $project['adv_amount'] - $project['expense_amount'];
                                                            ?>
                                                            <tr>
                                                                <!-- Project Name -->
                                                                <td rowspan="<?php echo $rowCount; ?>">
                                                                    <?php echo htmlspecialchars($project['project']); ?>
                                                                </td>
                                                                <?php
                                                                $isFirstRow = true;
                                                                $advance_amount_total = 0;
                                                                foreach ($project['details'] as $date => $details) {
                                                                    $advanceAmount = isset($details['advance_amount']) ? floatval($details['advance_amount']) : 0;
                                                                    $advance_amount_total += $advanceAmount;
                                                                }
                                                                foreach ($project['details'] as $date => $details) {

                                                                    if (!$isFirstRow)
                                                                        echo '<tr>';
                                                                    ?>
                                                                   
                                                                    
                                                                    <!-- Payment Details -->
                                                                    <td>
                                                                        <?php echo isset($details['payment_id']) ? '#' . htmlspecialchars($details['payment_id']) : ''; ?>
                                                                    </td>
                                                                    <td class="text-center">
                                                                        <?php echo isset($details['advance_amount']) ? Yii::app()->Controller->changeDateForamt($date) : ''; ?>
                                                                    </td>
                                                                    <td>
                                                                        <?php echo htmlspecialchars(isset($details['description']) ? $details['description'] : ''); ?>
                                                                    </td>
                                                                    <td class="text-right">
                                                                        <?php echo isset($details['advance_amount']) ? Yii::app()->Controller->money_format_inr($details['advance_amount'], 2) : '0.00'; ?>
                                                                    </td>

                                                                    <?php if ($isFirstRow) { ?>
                                                                        <!-- Balance Amount (First Row Only) -->
                                                                        <td class="text-right" rowspan="<?php echo $rowCount; ?>">
                                                                            <?php echo Yii::app()->Controller->money_format_inr(isset($advance_amount_total) ? $advance_amount_total : 0, 2); ?>
                                                                        </td>
                                                                    <?php } ?>
                                                                    <?php if ($isFirstRow) { ?>
                                                                        <!-- Balance Amount (First Row Only) -->
                                                                        <td class="text-right" rowspan="<?php echo $rowCount; ?>">
                                                                            <?php echo Yii::app()->Controller->money_format_inr(isset($balanceAmount) ? $balanceAmount : 0, 2); ?>
                                                                        </td>
                                                                    <?php } ?>
                                                                </tr>
                                                                <?php $isFirstRow = false;
                                                                } ?>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </tbody>


                                            </table>
                                        </div>
                                    </div>
                                <?php }
                            } ?>
                            <?php
                            if (filter_input(INPUT_GET, 'export') == 'pdf') {
                                ?>
                            </div>
                        </div>
                    </div>
                <?php } ?>
                <?php if (filter_input(INPUT_GET, 'export') == 'pdf') { ?>
                    <div class="content">
                        <div class="wrapper">
                            <div class="holder">
                            <?php } ?>
                            <?php if (filter_input(INPUT_GET, 'export') !== 'csv') {
                                if (isset($_REQUEST['custId']) && ($_REQUEST['custId'] == 'unreconcil')) { ?>
                                    <div class="row margin-top-10">
                                        <div class="col-xs-12">
                                            <div class="heading-title">Payment Without Quotation - Unreconcil</div>
                                            <div class="dotted-line"></div>
                                        </div>
                                    </div>
                                    <div class="unreconcil_table table-wrapper margin-top-10">
                                        <div class="d-flex">
                                            <div class="summary text-right margin-left-auto">Total
                                                <?php echo count($final_array2) ?> results.
                                            </div>

                                        </div>
                                        <div id="table-wrapper">
                                            <table
                                                class="table total-table  <?php echo filter_input(INPUT_GET, 'export') == 'pdf' ? 'detail-table text-sm-table' : 'table-bordered' ?>">
                                                <?php
                                                $advance_total = 0;
                                                $payment_total = 0;
                                                $balance_total = 0;

                                                foreach ($final_array2 as $key => $value) {
                                                    $j = 0;
                                                    $a = 0;
                                                    $blance = 0;
                                                    $expense_sum = 0;
                                                    $adv_sum = 0;
                                                    if (!empty($value['details'])) {
                                                        foreach ($value['details'] as $key2 => $value2) {
                                                            if (isset($value2['advance_amount'])) {
                                                                $advance_amount = $value2['advance_amount'];
                                                                $adv_sum += $advance_amount;
                                                            } else {
                                                                $advance_amount = 0;
                                                            }
                                                            if (isset($value2['payment_amount'])) {
                                                                $payment_amount = $value2['payment_amount'];
                                                                $expense_sum += $payment_amount;
                                                            } else {
                                                                $payment_amount = 0;
                                                            }

                                                            $blance += $advance_amount - $payment_amount;
                                                            if ($a >= 5) {
                                                                $blance = $blance + $advance_amount;
                                                            } else {
                                                                $blance = $blance;
                                                            }
                                                            $advance_total += $advance_amount;
                                                            $payment_total += $payment_amount;
                                                            $balance_total += $blance;
                                                            $final_array2[$key]['expense_amount'] = $expense_sum;
                                                            $final_array2[$key]['adv_amount'] = $adv_sum;
                                                        }
                                                    }
                                                }
                                                $balance_sum = $advance_total - $payment_total;
                                                ?>
                                                <thead class="entry-table sticky-thead">
                                                    <tr class="first-thead-row">
                                                        <th rowspan="3" class="text-center"><b>Project Name</b></th>
                                                        <th colspan="5" class="text-center"><b>Expense details</b></th>
                                                        <th colspan="5" class="text-center"><b>Advance details</b></th>
                                                        <th rowspan="2" class="text-center"><b>Balance</b></th>
                                                    </tr>
                                                    <tr class="second-thead-row">
                                                        <th rowspan="2" class="text-center"><b>#ID</b></th>
                                                        <th rowspan="2" class="text-center"><b>Date</b></th>
                                                        <th rowspan="2" class="text-center"><b>Description</b></th>
                                                        <th class="text-center" colspan="2"><b>Amount</b></th>
                                                        <th rowspan="2" class="text-center"><b>#ID</b></th>
                                                        <th rowspan="2" class="text-center"><b>Date</b></th>
                                                        <th rowspan="2" class="text-center"><b>Description</b></th>
                                                        <th class="text-center" colspan="2"><b>Amount</b></th>
                                                    </tr>
                                                    <tr class="grandtotal third-thead-rows">
                                                        <th class="text-right bg_grey" colspan="2">
                                                            <b>
                                                                <?php echo ($payment_total != '') ? Controller::money_format_inr($payment_total, 2, 1) : '0.00'; ?>
                                                            </b>
                                                        </th>
                                                        <th class="text-right bg_grey" colspan="2">
                                                            <b>
                                                                <?php echo ($advance_total != '') ? Controller::money_format_inr($advance_total, 2, 1) : '0.00'; ?>
                                                            </b>
                                                        </th>
                                                        <th class="text-right bg_grey">

                                                            <?php echo ($balance_sum != '') ? ($balance_sum > 0 ? '<b>' . Yii::app()->Controller->money_format_inr($balance_sum, 2) . '</b>' : '<b class="red_bg">' . Yii::app()->Controller->money_format_inr($balance_sum, 2) . '</b>') : '0.00'; ?>
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    $advance_total = 0;
                                                    $payment_total = 0;
                                                    $balance_total = 0;

                                                    foreach ($final_array2 as $key => $value) {
                                                        if (!empty($value['details'])) {
                                                            $balance_amount = $value['adv_amount'] - $value['expense_amount'];
                                                            $rowCount = count($value['details']);
                                                            ?>
                                                            <tr>
                                                                <!-- Project Name -->
                                                                <td rowspan="<?php echo $rowCount; ?>">
                                                                    <?php echo htmlspecialchars($value['project']); ?>
                                                                </td>
                                                                <?php
                                                                $isFirstRow = true;
                                                                foreach ($value['details'] as $key2 => $value2) {
                                                                    $advance_amount = isset($value2['advance_amount']) ? $value2['advance_amount'] : 0;
                                                                    $payment_amount = isset($value2['payment_amount']) ? $value2['payment_amount'] : 0;

                                                                    $blance = $advance_amount - $payment_amount;
                                                                    $advance_total += $advance_amount;
                                                                    $payment_total += $payment_amount;
                                                                    $balance_total += $blance;

                                                                    if (!$isFirstRow)
                                                                        echo '<tr>'; // Create a new row for subsequent items
                                                                    ?>
                                                                    <!-- Reconciled Data -->
                                                                    <td>
                                                                        <?php echo isset($value2['dr_id']) ? '#' . htmlspecialchars($value2['dr_id']) : ''; ?>
                                                                    </td>
                                                                    <td class="text-center">
                                                                        <?php echo isset($value2['payment_amount']) ? Yii::app()->Controller->changeDateForamt($key2) : ''; ?>
                                                                    </td>
                                                                    <td>
                                                                        <?php echo isset($value2['payment_description']) ? htmlspecialchars($value2['payment_description']) : ''; ?>
                                                                    </td>
                                                                    <td class="text-right">
                                                                        <?php echo isset($value2['payment_amount']) ? Yii::app()->Controller->money_format_inr($payment_amount, 2) : ''; ?>
                                                                    </td>
                                                                    <?php if ($isFirstRow) { ?>
                                                                        <!-- Expense (First Row Only) -->
                                                                        <td class="text-right" rowspan="<?php echo $rowCount; ?>">
                                                                            <?php echo Yii::app()->Controller->money_format_inr($value['expense_amount'], 2); ?>
                                                                        </td>
                                                                    <?php } ?>
                                                                    <!-- Unreconciled Data -->
                                                                    <td>
                                                                        <?php echo isset($value2['payment_id']) ? '#' . htmlspecialchars($value2['payment_id']) : ''; ?>
                                                                    </td>
                                                                    <td class="text-center">
                                                                        <?php echo isset($value2['advance_amount']) ? Yii::app()->Controller->changeDateForamt($key2) : ''; ?>
                                                                    </td>
                                                                    <td>
                                                                        <?php echo htmlspecialchars(isset($value2['description']) ? $value2['description'] : ''); ?>
                                                                    </td>
                                                                    <td class="text-right">
                                                                        <?php echo isset($value2['advance_amount']) ? Yii::app()->Controller->money_format_inr($advance_amount, 2) : '0.00'; ?>
                                                                    </td>
                                                                    <?php if ($isFirstRow) { ?>
                                                                        <!-- Advance and Balance Totals (First Row Only) -->
                                                                        <td class="text-right" rowspan="<?php echo $rowCount; ?>">
                                                                            <?php echo Yii::app()->Controller->money_format_inr($value['adv_amount'], 2); ?>
                                                                        </td>
                                                                        <td class="text-right" rowspan="<?php echo $rowCount; ?>">
                                                                            <?php echo Yii::app()->Controller->money_format_inr($balance_amount, 2); ?>
                                                                        </td>
                                                                    <?php } ?>
                                                                </tr>
                                                                <?php
                                                                $isFirstRow = false;
                                                                } // End foreach details
                                                        } // End if details
                                                    } // End foreach project
                                                    ?>
                                                </tbody>

                                            </table>
                                        </div>
                                    </div>
                                <?php }
                            } ?>
                            <?php
                            if (filter_input(INPUT_GET, 'export') == 'pdf') {
                                ?>
                            </div>
                        </div>
                    </div>
                <?php } ?>
                <!--Labour entry starts-->
                <br /><br />
                <pagebreak></pagebreak>
                <?php if (filter_input(INPUT_GET, 'export') == 'pdf') { ?>
                    <div class="content">
                        <div class="wrapper">
                            <div class="holder">
                            <?php } ?>
                            <?php if (filter_input(INPUT_GET, 'export') !== 'csv') {
                                if (isset($_REQUEST['custId']) && ($_REQUEST['custId'] == 'reconcil')) { ?>
                                    <div class="row margin-top-10">
                                        <div class="col-xs-12">
                                            <div class="heading-title">Payment With Quotation - reconcil (Labour Entry)</div>
                                            <div class="dotted-line"></div>
                                        </div>
                                    </div>
                                    <div class="reconcil_table table-wrapper margin-top-10">
                                        <div class="d-flex">
                                            <div class="summary text-right margin-left-auto">Total
                                                <?php echo count($final_array3) ?> results.
                                            </div>

                                        </div>
                                        <div id="table-wrapper">
                                            <table
                                                class="table total-table <?php echo filter_input(INPUT_GET, 'export') == 'pdf' ? 'detail-table text-sm-table' : 'table-bordered' ?>">
                                                <?php
                                                $advance_total = 0;
                                                $payment_total = 0;
                                                $balance_total = 0;

                                                foreach ($final_array3 as $key => $value) {
                                                    $j = 0;
                                                    $a = 0;
                                                    $blance = 0;
                                                    $expense_sum = 0;
                                                    $adv_sum = 0;
                                                    if (!empty($value['details'])) {
                                                        foreach ($value['details'] as $key2 => $value2) {
                                                            if (isset($value2['advance_amount'])) {
                                                                $advance_amount = $value2['advance_amount'];
                                                                $adv_sum += $advance_amount;
                                                            } else {
                                                                $advance_amount = 0;
                                                            }
                                                            if (isset($value2['payment_amount'])) {
                                                                $payment_amount = $value2['payment_amount'];
                                                                $expense_sum += $payment_amount;
                                                            } else {
                                                                $payment_amount = 0;
                                                            }

                                                            $blance += $advance_amount - $payment_amount;
                                                            if ($a >= 5) {
                                                                $blance = $blance + $advance_amount;
                                                            } else {
                                                                $blance = $blance;
                                                            }
                                                            $advance_total += $advance_amount;
                                                            $payment_total += $payment_amount;
                                                            $balance_total += $blance;
                                                            $final_array3[$key]['expense_amount'] = $expense_sum;
                                                            $final_array3[$key]['adv_amount'] = $adv_sum;
                                                        }
                                                    }
                                                }
                                                $balance_sum = $payment_total - $advance_total;
                                                ?>
                                                <thead class="entry-table sticky-thead">
                                                    <tr class="first-thead-row">
                                                        <th rowspan="3" class="text-center"><b>Project Name</b></th>
                                                        <th colspan="6" class="text-center"><b>Labour details</b></th>
                                                        <th colspan="5" class="text-center"><b>Payment details</b></th>
                                                        <th rowspan="2" class="text-center"><b>Balance</b></th>
                                                    </tr>
                                                    <tr class="second-thead-row">
                                                        <th rowspan="2" class="text-center"><b>#ID</b></th>
                                                        <th rowspan="2" class="text-center"><b>Date</b></th>
                                                        <th rowspan="2" class="text-center"><b>Quotation No</b></th>
                                                        <th rowspan="2" class="text-center"><b>Description</b></th>
                                                        <th class="text-center" colspan="2"><b>Amount</b></th>
                                                        <th rowspan="2" class="text-center"><b>#ID</b></th>
                                                        <th rowspan="2" class="text-center"><b>Date</b></th>
                                                        <th rowspan="2" class="text-center"><b>Description</b></th>
                                                        <th class="text-center" colspan="2"><b>Amount</b></th>
                                                    </tr>
                                                    <tr class="grandtotal third-thead-rows">
                                                        <th class="text-right bg_grey" colspan="2">
                                                            <b>
                                                                <?php echo ($payment_total != '') ? Controller::money_format_inr($payment_total, 2, 1) : '0.00'; ?>
                                                            </b>
                                                        </th>
                                                        <th class="text-right bg_grey" colspan="2">
                                                            <b>
                                                                <?php echo ($advance_total != '') ? Controller::money_format_inr($advance_total, 2, 1) : '0.00'; ?>
                                                            </b>
                                                        </th>
                                                        <th class="text-right bg_grey">

                                                            <?php echo ($balance_sum != '') ? ($balance_sum > 0 ? '<b>' . Yii::app()->Controller->money_format_inr($balance_sum, 2) . '</b>' : '<b class="red_bg">' . Yii::app()->Controller->money_format_inr($balance_sum, 2) . '</b>') : '0.00'; ?>
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                    <?php
                                                    //echo "<pre>"; print_r($final_array2);exit;
                                                    $advance_amount_total = 0;
                                                    foreach ($final_array3 as $project) { ?>
                                                        <?php if (!empty($project['details'])) {
                                                            $rowCount = count($project['details']);
                                                            $balanceAmount = $project['adv_amount'] - $project['expense_amount'];
                                                            ?>
                                                            <tr>
                                                                <!-- Project Name -->
                                                                <td rowspan="<?php echo $rowCount; ?>">
                                                                    <?php echo htmlspecialchars($project['project']); ?>
                                                                </td>
                                                                <?php
                                                                $isFirstRow = true;
                                                                $advance_amount_total = 0;
                                                                foreach ($project['details'] as $date => $details) {
                                                                    $advanceAmount = isset($details['advance_amount']) ? floatval($details['advance_amount']) : 0;
                                                                    $advance_amount_total += $advanceAmount;
                                                                }
                                                                foreach ($project['details'] as $date => $details) {

                                                                    if (!$isFirstRow)
                                                                        echo '<tr>';
                                                                    ?>
                                                                    <!-- Labour Details -->
                                                                    <td>
                                                                        <?php echo isset($details['dr_id']) ? '#' . htmlspecialchars($details['dr_id']) : ''; ?>
                                                                    </td>
                                                                    <td class="text-center">
                                                                        <?php echo Yii::app()->Controller->changeDateForamt($date); ?>
                                                                    </td>
                                                                    <td class="text-center">
                                                                        <?php echo isset($details['sc_quot_id']) ?$details['sc_quot_id']:''; ?>
                                                                    </td>
                                                                    <td>
                                                                        <?php echo isset($details['payment_amount']) ? htmlspecialchars($details['payment_description']) : ''; ?>
                                                                    </td>
                                                                    <td class="text-right">
                                                                        <?php echo isset($details['payment_amount']) ? Yii::app()->Controller->money_format_inr($details['payment_amount'], 2) : ''; ?>
                                                                    </td>
                                                                    <?php if ($isFirstRow) { ?>
                                                                        <!-- Expense Amount (First Row Only) -->
                                                                        <td class="text-right" rowspan="<?php echo $rowCount; ?>">
                                                                            <?php echo Yii::app()->Controller->money_format_inr(isset($project['expense_amount']) ? $project['expense_amount'] : 0, 2); ?>
                                                                        </td>
                                                                    <?php } ?>
                                                                    <!-- Payment Details -->
                                                                    <td>
                                                                        <?php echo isset($details['payment_id']) ? '#' . htmlspecialchars($details['payment_id']) : ''; ?>
                                                                    </td>
                                                                    <td class="text-center">
                                                                        <?php echo isset($details['advance_amount']) ? Yii::app()->Controller->changeDateForamt($date) : ''; ?>
                                                                    </td>
                                                                    <td>
                                                                        <?php echo htmlspecialchars(isset($details['description']) ? $details['description'] : ''); ?>
                                                                    </td>
                                                                    <td class="text-right">
                                                                        <?php echo isset($details['advance_amount']) ? Yii::app()->Controller->money_format_inr($details['advance_amount'], 2) : '0.00'; ?>
                                                                    </td>

                                                                    <?php if ($isFirstRow) { ?>
                                                                        <!-- Balance Amount (First Row Only) -->
                                                                        <td class="text-right" rowspan="<?php echo $rowCount; ?>">
                                                                            <?php echo Yii::app()->Controller->money_format_inr(isset($advance_amount_total) ? $advance_amount_total : 0, 2); ?>
                                                                        </td>
                                                                    <?php } ?>
                                                                    <?php if ($isFirstRow) { ?>
                                                                        <!-- Balance Amount (First Row Only) -->
                                                                        <td class="text-right" rowspan="<?php echo $rowCount; ?>">
                                                                            <?php echo Yii::app()->Controller->money_format_inr(isset($balanceAmount) ? $balanceAmount : 0, 2); ?>
                                                                        </td>
                                                                    <?php } ?>
                                                                </tr>
                                                                <?php $isFirstRow = false;
                                                                } ?>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </tbody>


                                            </table>
                                        </div>
                                    </div>
                                <?php }
                            } ?>
                            <?php
                            if (filter_input(INPUT_GET, 'export') == 'pdf') {
                                ?>
                            </div>
                        </div>
                    </div>
                <?php } ?>
                <?php if (filter_input(INPUT_GET, 'export') == 'pdf') { ?>
                    <div class="content">
                        <div class="wrapper">
                            <div class="holder">
                            <?php } ?>
                            <?php if (filter_input(INPUT_GET, 'export') !== 'csv') {
                                if (isset($_REQUEST['custId']) && ($_REQUEST['custId'] == 'unreconcil')) { ?>
                                    <div class="row margin-top-10">
                                        <div class="col-xs-12">
                                            <div class="heading-title">Payment Without Quotation - Unreconcil( Labour Entry)</div>
                                            <div class="dotted-line"></div>
                                        </div>
                                    </div>
                                    <div class="unreconcil_table table-wrapper margin-top-10">
                                        <div class="d-flex">
                                            <div class="summary text-right margin-left-auto">Total
                                                <?php echo count($final_array3) ?> results.
                                            </div>

                                        </div>
                                        <div id="table-wrapper">
                                            <table
                                                class="table total-table  <?php echo filter_input(INPUT_GET, 'export') == 'pdf' ? 'detail-table text-sm-table' : 'table-bordered' ?>">
                                                <?php
                                                $advance_total = 0;
                                                $payment_total = 0;
                                                $balance_total = 0;

                                                foreach ($final_array3 as $key => $value) {
                                                    $j = 0;
                                                    $a = 0;
                                                    $blance = 0;
                                                    $expense_sum = 0;
                                                    $adv_sum = 0;
                                                    if (!empty($value['details'])) {
                                                        foreach ($value['details'] as $key2 => $value2) {
                                                            if (isset($value2['advance_amount'])) {
                                                                $advance_amount = $value2['advance_amount'];
                                                                $adv_sum += $advance_amount;
                                                            } else {
                                                                $advance_amount = 0;
                                                            }
                                                            if (isset($value2['payment_amount'])) {
                                                                $payment_amount = $value2['payment_amount'];
                                                                $expense_sum += $payment_amount;
                                                            } else {
                                                                $payment_amount = 0;
                                                            }

                                                            $blance += $advance_amount - $payment_amount;
                                                            if ($a >= 5) {
                                                                $blance = $blance + $advance_amount;
                                                            } else {
                                                                $blance = $blance;
                                                            }
                                                            $advance_total += $advance_amount;
                                                            $payment_total += $payment_amount;
                                                            $balance_total += $blance;
                                                            $final_array3[$key]['expense_amount'] = $expense_sum;
                                                            $final_array3[$key]['adv_amount'] = $adv_sum;
                                                        }
                                                    }
                                                }
                                                $balance_sum = $advance_total - $payment_total;
                                                ?>
                                                <thead class="entry-table sticky-thead">
                                                    <tr class="first-thead-row">
                                                        <th rowspan="3" class="text-center"><b>Project Name</b></th>
                                                        <th colspan="6" class="text-center"><b>Expense details</b></th>
                                                        <th colspan="5" class="text-center"><b>Advance details</b></th>
                                                        <th rowspan="2" class="text-center"><b>Balance</b></th>
                                                    </tr>
                                                    <tr class="second-thead-row">
                                                        <th rowspan="2" class="text-center"><b>#ID</b></th>
                                                         
                                                        <th rowspan="2" class="text-center"><b>Date</b></th>
                                                        <th rowspan="2" class="text-center"><b>Quotation No</b></th>
                                                        <th rowspan="2" class="text-center"><b>Description</b></th>
                                                        <th class="text-center" colspan="2"><b>Amount</b></th>
                                                        <th rowspan="2" class="text-center"><b>#ID</b></th>
                                                        <th rowspan="2" class="text-center"><b>Date</b></th>
                                                        <th rowspan="2" class="text-center"><b>Description</b></th>
                                                        <th class="text-center" colspan="2"><b>Amount</b></th>
                                                    </tr>
                                                    <tr class="grandtotal third-thead-rows">
                                                        <th class="text-right bg_grey" colspan="2">
                                                            <b>
                                                                <?php echo ($payment_total != '') ? Controller::money_format_inr($payment_total, 2, 1) : '0.00'; ?>
                                                            </b>
                                                        </th>
                                                        <th class="text-right bg_grey" colspan="2">
                                                            <b>
                                                                <?php echo ($advance_total != '') ? Controller::money_format_inr($advance_total, 2, 1) : '0.00'; ?>
                                                            </b>
                                                        </th>
                                                        <th class="text-right bg_grey">

                                                            <?php echo ($balance_sum != '') ? ($balance_sum > 0 ? '<b>' . Yii::app()->Controller->money_format_inr($balance_sum, 2) . '</b>' : '<b class="red_bg">' . Yii::app()->Controller->money_format_inr($balance_sum, 2) . '</b>') : '0.00'; ?>
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    $advance_total = 0;
                                                    $payment_total = 0;
                                                    $balance_total = 0;

                                                    foreach ($final_array3 as $key => $value) {
                                                        if (!empty($value['details'])) {
                                                            $balance_amount = $value['adv_amount'] - $value['expense_amount'];
                                                            $rowCount = count($value['details']);
                                                            ?>
                                                            <tr>
                                                                <!-- Project Name -->
                                                                <td rowspan="<?php echo $rowCount; ?>">
                                                                    <?php echo htmlspecialchars($value['project']); ?>
                                                                </td>
                                                                <?php
                                                                $isFirstRow = true;
                                                                foreach ($value['details'] as $key2 => $value2) {
                                                                    $advance_amount = isset($value2['advance_amount']) ? $value2['advance_amount'] : 0;
                                                                    $payment_amount = isset($value2['payment_amount']) ? $value2['payment_amount'] : 0;

                                                                    $blance = $advance_amount - $payment_amount;
                                                                    $advance_total += $advance_amount;
                                                                    $payment_total += $payment_amount;
                                                                    $balance_total += $blance;

                                                                    if (!$isFirstRow)
                                                                        echo '<tr>'; // Create a new row for subsequent items
                                                                    ?>
                                                                    <!-- Reconciled Data -->
                                                                    <td>
                                                                        <?php echo isset($value2['dr_id']) ? '#' . htmlspecialchars($value2['dr_id']) : ''; ?>
                                                                    </td>
                                                                    <td class="text-center">
                                                                        <?php echo isset($value2['payment_amount']) ? Yii::app()->Controller->changeDateForamt($key2) : ''; ?>
                                                                    </td>
                                                                    <td class="text-center">
                                                                        <?php echo isset($details['sc_quot_id']) ?$details['sc_quot_id']:''; ?>
                                                                    </td>
                                                                    <td>
                                                                        <?php echo isset($value2['payment_description']) ? htmlspecialchars($value2['payment_description']) : ''; ?>
                                                                    </td>
                                                                    <td class="text-right">
                                                                        <?php echo isset($value2['payment_amount']) ? Yii::app()->Controller->money_format_inr($payment_amount, 2) : ''; ?>
                                                                    </td>
                                                                    <?php if ($isFirstRow) { ?>
                                                                        <!-- Expense (First Row Only) -->
                                                                        <td class="text-right" rowspan="<?php echo $rowCount; ?>">
                                                                            <?php echo Yii::app()->Controller->money_format_inr($value['expense_amount'], 2); ?>
                                                                        </td>
                                                                    <?php } ?>
                                                                    <!-- Unreconciled Data -->
                                                                    <td>
                                                                        <?php echo isset($value2['payment_id']) ? '#' . htmlspecialchars($value2['payment_id']) : ''; ?>
                                                                    </td>
                                                                    <td class="text-center">
                                                                        <?php echo isset($value2['advance_amount']) ? Yii::app()->Controller->changeDateForamt($key2) : ''; ?>
                                                                    </td>
                                                                    <td>
                                                                        <?php echo htmlspecialchars(isset($value2['description']) ? $value2['description'] : ''); ?>
                                                                    </td>
                                                                    <td class="text-right">
                                                                        <?php echo isset($value2['advance_amount']) ? Yii::app()->Controller->money_format_inr($advance_amount, 2) : '0.00'; ?>
                                                                    </td>
                                                                    <?php if ($isFirstRow) { ?>
                                                                        <!-- Advance and Balance Totals (First Row Only) -->
                                                                        <td class="text-right" rowspan="<?php echo $rowCount; ?>">
                                                                            <?php echo Yii::app()->Controller->money_format_inr($value['adv_amount'], 2); ?>
                                                                        </td>
                                                                        <td class="text-right" rowspan="<?php echo $rowCount; ?>">
                                                                            <?php echo Yii::app()->Controller->money_format_inr($balance_amount, 2); ?>
                                                                        </td>
                                                                    <?php } ?>
                                                                </tr>
                                                                <?php
                                                                $isFirstRow = false;
                                                                } // End foreach details
                                                        } // End if details
                                                    } // End foreach project
                                                    ?>
                                                </tbody>

                                            </table>
                                        </div>
                                    </div>
                                <?php }
                            } ?>
                            <?php
                            if (filter_input(INPUT_GET, 'export') == 'pdf') {
                                ?>
                            </div>
                        </div>
                    </div>
                <?php }


                
        } else { ?>
                <div class="expenses-heading">
                    <div class="clearfix"
                        style="<?php echo filter_input(INPUT_GET, 'export') == 'pdf' ? 'display:none' : '' ?>">
                    </div>
                </div>
                <div class="table-wrapper margin-top-20">
                    <div class="d-flex">
                        <div class="summary text-right margin-left-auto">Total
                            <?php echo count($scquotation); ?>results
                        </div>
                        <?php
                        $final_amount = 0;
                        $mainlist_condition = "HAVING 1=1";
                        $mainlist_condition_labour = "HAVING 1=1";
                        $today = date("Y-m-d");
                        $twoMonthsAgo = date("Y-m-d", strtotime("-3 months"));

                        if (empty($_GET['subcontractor_id'])) {
                            if (isset($_GET['project_id']) && !empty($_GET['project_id'])) {
                                $mainlist_condition .= " AND jp_scquotation.project_id =" . $_GET['project_id'];
                                $mainlist_condition_labour .= " AND jp_dailyreport.projectid =" . $_GET['project_id'];

                            }
                        }
                        if (!empty($date_from) && !empty($date_to)) {
                            $datecondition = " AND scquotation_date BETWEEN '" . date("Y-m-d", strtotime($date_from)) . "' AND '" . date("Y-m-d", strtotime($date_to)) . "'";
                            $datecondition1 = " AND `date` BETWEEN '" . date("Y-m-d", strtotime($date_from)) . "' AND '" . date("Y-m-d", strtotime($date_to)) . "'";
                        } else if (!empty($date_from) && empty($date_to)) {
                            $datecondition = " AND scquotation_date >= '" . date("Y-m-d", strtotime($date_from)) . "'";
                            $datecondition1 = " AND `date` >= '" . date("Y-m-d", strtotime($date_from)) . "'";
                        } else if (empty($date_from) && !empty($date_to)) {
                            $datecondition = " AND scquotation_date <= '" . date("Y-m-d", strtotime($date_to)) . "'";
                            $datecondition1 = " AND `date` <= '" . date("Y-m-d", strtotime($date_to)) . "'";
                        } else {

                            $datecondition = " AND scquotation_date BETWEEN '$twoMonthsAgo' AND '$today'";
                            $datecondition1 = " AND `date` BETWEEN '$twoMonthsAgo' AND '$today'";
                        }


                        $scqsql = "SELECT project_id,{$tblpx}scquotation.subcontractor_id,subcontractor_name,{$tblpx}scquotation.company_id,`name` FROM " . $this->tableNameAcc('subcontractor', 0) . ""
                            . " INNER JOIN " . $this->tableNameAcc('scquotation', 0) . ""
                            . " ON {$tblpx}subcontractor.subcontractor_id={$tblpx}scquotation.subcontractor_id "
                            . " LEFT JOIN {$tblpx}projects "
                            . " ON {$tblpx}scquotation.project_id= {$tblpx}projects.pid "
                            . " WHERE (" . $newQuery3 . ") "
                            . " GROUP BY {$tblpx}scquotation.subcontractor_id,"
                            . " {$tblpx}scquotation.project_id " . $mainlist_condition
                            . " ORDER BY {$tblpx}scquotation.scquotation_id";
                        $scquotation = Yii::app()->db->createCommand($scqsql)->queryAll();
                        $laboursql = "SELECT projectid as project_id,{$tblpx}dailyreport.subcontractor_id,subcontractor_name,{$tblpx}dailyreport.company_id,`name`,1 AS labour_entry  FROM " . $this->tableNameAcc('subcontractor', 0) . ""
                            . " INNER JOIN " . $this->tableNameAcc('dailyreport', 0) . ""
                            . " ON {$tblpx}subcontractor.subcontractor_id={$tblpx}dailyreport.subcontractor_id "
                            . " LEFT JOIN {$tblpx}projects "
                            . " ON {$tblpx}dailyreport.projectid= {$tblpx}projects.pid "
                            . " WHERE (" . $newQuery3 . ") "
                            . " AND {$tblpx}dailyreport.approve_status= 1 "
                            . " GROUP BY {$tblpx}dailyreport.subcontractor_id,"
                            . " {$tblpx}dailyreport.projectid " . $mainlist_condition_labour
                            . " ORDER BY {$tblpx}dailyreport.dr_id";
                        $withoutquotation = Yii::app()->db->createCommand($laboursql)->queryAll();
                        //echo "<pre>";print_r($scquotation);print_r($withoutquotation);
                        $combinedArray = array_merge($scquotation, $withoutquotation);

                        $finalCombinedArray = [];

                        foreach ($combinedArray as $entry) {
                            $projectId = $entry['project_id'];
                            $subcontractorId = $entry['subcontractor_id'];
                            $companyId = $entry['company_id'];

                            $finalCombinedArray["$projectId-$subcontractorId-$companyId"] = $entry;
                        }

                        $scquotation = array_values($finalCombinedArray);
                        // $scquotation = array_slice($scquotation, 0, 100);
                        // print_r($scquotation);die;
                        if (!empty($scquotation)) {
                            foreach ($scquotation as $data) {

                                $scqamtsql = "SELECT SUM(scquotation_amount) "
                                    . " AS scquotation_amount  FROM `jp_scquotation` "
                                    . " WHERE subcontractor_id='" . $data['subcontractor_id'] . "'"
                                    . " AND project_id= '" . $data['project_id'] . "' $datecondition"
                                    . " AND company_id='" . $data['company_id'] . "'";


                                $scquotation_amount = Yii::app()->db->createCommand($scqamtsql)->queryRow();

                                $paymntsql = "SELECT SUM(amount) as payment_amount  "
                                    . " FROM `jp_subcontractor_payment` "
                                    . " WHERE subcontractor_id='" . $data['subcontractor_id'] . "' "
                                    . " and project_id= '" . $data['project_id'] . "' $datecondition1"
                                    . " AND company_id='" . $data['company_id'] . "'"
                                    . " AND payment_quotation_status=1";

                                $payment_amount = Yii::app()->db->createCommand($paymntsql)->queryRow();

                                $qamount = $scquotation_amount['scquotation_amount'];
                                $pamount = $payment_amount['payment_amount'];
                                $balance = $qamount - $pamount;
                                //$final_amount += $balance;
                                //without qtn section
                                $withoutqtnsql = "SELECT SUM(amount) "
                                    . " AS amount  FROM `jp_dailyreport` "
                                    . " WHERE subcontractor_id='" . $data['subcontractor_id'] . "'"
                                    . " AND projectid= '" . $data['project_id'] . "' $datecondition1"
                                    . " AND company_id='" . $data['company_id'] . "'"
                                    . " AND approve_status=1";
                                $withoutqtn_amount1 = Yii::app()->db->createCommand($withoutqtnsql)->queryRow();

                                $withoutqtnpayamountsql = "SELECT SUM(amount) as payment_amount  "
                                    . " FROM `jp_subcontractor_payment` "
                                    . " WHERE subcontractor_id='" . $data['subcontractor_id'] . "' "
                                    . " and project_id= '" . $data['project_id'] . "' $datecondition1"
                                    . " AND company_id='" . $data['company_id'] . "'"
                                    . " AND payment_quotation_status=2";
                                $withoutqtnpayment_amount1 = Yii::app()->db->createCommand($withoutqtnpayamountsql)->queryRow();


                                $withoutqamount = $withoutqtn_amount1['amount'];
                                $withoutpamount = $withoutqtnpayment_amount1['payment_amount'];
                                $balance2 = $withoutqamount - $withoutpamount;
                                $newbalance = $balance + $balance2;
                                $final_amount += $newbalance;
                            }
                        }
                        ?>
                    </div>
                    <div id="table-wrapper" class="pdf_spacing">
                        <table class="table total-table" id="fixTable">
                            <thead class="entry-table">
                                <tr>
                                    <th>Sl No.</th>
                                    <th>Subcontractor</th>
                                    <th>Company</th>
                                    <th>Project</th>
                                    <th>Amount With Quotation / Without Quotation</th>
                                    <th>Payment Done</th>
                                    <th>Balance</th>
                                    <th>Total Balance</th>
                                </tr>
                                <tr class="grandtotal">
                                    <td colspan="7" class="text-right"><b>Total</b></td>
                                    <td class="text-right"><b>
                                            <?php echo ($final_amount != '') ? Controller::money_format_inr($final_amount, 2, 1) : '0.00'; ?>
                                        </b></td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = 0;
                                $final_amount = 0;

                                if (!empty($scquotation)) {//echo "<pre>";print_r($scquotation);die;
                                    foreach ($scquotation as $data) {
                                        $i++;
                                        $scsql = "SELECT SUM( IFNULL(" . $tblpx . "scquotation_items.item_amount,0)) "
                                            . " as total_amount FROM " . $tblpx . "scquotation "
                                            . " LEFT JOIN " . $tblpx . "scquotation_items"
                                            . " ON " . $tblpx . "scquotation.scquotation_id = " . $tblpx . "scquotation_items.scquotation_id "
                                            . " WHERE " . $tblpx . "scquotation.project_id= " . $data['project_id'] . " "
                                            . " AND " . $tblpx . "scquotation.subcontractor_id=" . $data['subcontractor_id'] . " "
                                            . " AND " . $tblpx . "scquotation_items.approve_status = 'Yes' "
                                            . " AND (" . $newQuery1 . ")";
                                        $scquotation_amount = Yii::app()->db->createCommand($scsql)->queryRow();

                                        $paysql = "SELECT SUM(IFNULL(amount, 0) + IFNULL(tax_amount, 0)) as total_amount "
                                            . " FROM {$tblpx}subcontractor_payment INNER JOIN {$tblpx}subcontractor "
                                            . " ON {$tblpx}subcontractor_payment.subcontractor_id= {$tblpx}subcontractor.subcontractor_id"
                                            . " LEFT JOIN {$tblpx}projects ON {$tblpx}subcontractor_payment.project_id= {$tblpx}projects.pid "
                                            . " WHERE {$tblpx}subcontractor_payment.subcontractor_id = " . $data['subcontractor_id'] . " "
                                            . " AND {$tblpx}subcontractor_payment.project_id=" . $data['project_id'] . " "
                                            . " AND {$tblpx}subcontractor_payment.approve_status ='Yes' "
                                            . " AND (" . $newQuery2 . ") ";
                                        $payment_amount = Yii::app()->db->createCommand($paysql)->queryRow();
                                        //  echo "<pre>";print_r($payment_amount);//exit;
                            
                                        $scqtsql = "SELECT SUM(scquotation_amount) "
                                            . " AS scquotation_amount  FROM `jp_scquotation` "
                                            . " WHERE subcontractor_id='" . $data['subcontractor_id'] . "'"
                                            . " AND project_id= '" . $data['project_id'] . "' $datecondition"
                                            . " AND company_id='" . $data['company_id'] . "'";

                                        $scquotation_amount1 = Yii::app()->db->createCommand($scqtsql)->queryRow();

                                        $payamountsql = "SELECT SUM(amount) as payment_amount  "
                                            . " FROM `jp_subcontractor_payment` "
                                            . " WHERE subcontractor_id='" . $data['subcontractor_id'] . "' "
                                            . " and project_id= '" . $data['project_id'] . "' $datecondition1"
                                            . " AND company_id='" . $data['company_id'] . "'"
                                            . " AND payment_quotation_status=1";
                                        $payment_amount1 = Yii::app()->db->createCommand($payamountsql)->queryRow();


                                        $qamount = $scquotation_amount1['scquotation_amount'];
                                        $pamount = $payment_amount1['payment_amount'];
                                        $balance = $qamount - $pamount;

                                        //$final_amount += $balance;
                                        //without quotation section
                                        $withoutqtnsql = "SELECT SUM(amount) "
                                            . " AS amount  FROM `jp_dailyreport` "
                                            . " WHERE subcontractor_id='" . $data['subcontractor_id'] . "'"
                                            . " AND projectid= '" . $data['project_id'] . "'$datecondition1"
                                            . " AND company_id='" . $data['company_id'] . "'"
                                            . " AND approve_status=1";
                                        $withoutqtn_amount1 = Yii::app()->db->createCommand($withoutqtnsql)->queryRow();

                                        $withoutqtnpayamountsql = "SELECT SUM(amount) as payment_amount  "
                                            . " FROM `jp_subcontractor_payment` "
                                            . " WHERE subcontractor_id='" . $data['subcontractor_id'] . "' "
                                            . " and project_id= '" . $data['project_id'] . "' $datecondition1"
                                            . " AND company_id='" . $data['company_id'] . "'"
                                            . " AND payment_quotation_status=2";
                                        $withoutqtnpayment_amount1 = Yii::app()->db->createCommand($withoutqtnpayamountsql)->queryRow();


                                        $withoutqamount = $withoutqtn_amount1['amount'];
                                        $withoutpamount = $withoutqtnpayment_amount1['payment_amount'];
                                        $balance2 = $withoutqamount - $withoutpamount;
                                        $newbalance = $balance + $balance2;
                                        $final_amount += $newbalance;
                                        if (!empty($payment_amount1['payment_amount']) || !empty($withoutqtnpayment_amount1['payment_amount'])) {

                                            ?>
                                            <tr>
                                                <td <?php echo isset($data['labour_entry']) ? 'rowspan=2' : ''; ?>>
                                                    <?php echo $i; ?>
                                                </td>
                                                <td <?php echo isset($data['labour_entry']) ? 'rowspan=2' : ''; ?>>
                                                    <?php echo $data['subcontractor_name']; ?>
                                                </td>
                                                <td <?php echo isset($data['labour_entry']) ? 'rowspan=2' : ''; ?>>
                                                    <?php
                                                    $company_name = "";
                                                    if ($data['company_id'] != NULL) {
                                                        $company = Company::model()->findbyPk($data['company_id']);
                                                        $company_name = $company->name;
                                                    }
                                                    echo $company_name;
                                                    ?>
                                                </td>
                                                <td <?php echo isset($data['labour_entry']) ? 'rowspan=2' : ''; ?>>
                                                    <?php echo $data['name']; ?>
                                                </td>
                                                <?php if (isset($data['labour_entry'])) { ?>
                                                    <td class="text-right">
                                                        <?php echo ($scquotation_amount1['scquotation_amount'] != '') ? Controller::money_format_inr($scquotation_amount1['scquotation_amount'], 2, 1) : '0.00'; ?>
                                                    </td>
                                                    <td class="text-right">
                                                        <?php echo ($payment_amount1['payment_amount'] != '') ? Controller::money_format_inr($payment_amount1['payment_amount'], 2, 1) : '0.00'; ?>
                                                    </td>
                                                    <td class="text-right">
                                                        <?php echo ($balance != '') ? Controller::money_format_inr($balance, 2) : '0.00'; ?>
                                                    </td>
                                                    <td <?php echo isset($data['labour_entry']) ? 'rowspan=2' : ''; ?> class="text-right"><a
                                                            href="<?php echo Yii::app()->createAbsoluteUrl('subcontractorpayment/report', array(
                                                                'subcontractor_id' => $data['subcontractor_id'],
                                                                'company_id' => $data['company_id'],
                                                                'project_id' => $data['project_id'],
                                                                'custId' => 'reconcil'
                                                            )); ?>">
                                                            <?php echo ($newbalance != '') ? Controller::money_format_inr($newbalance, 2) : '0.00'; ?>
                                                        </a></td>
                                                </tr>
                                                <tr>
                                                    <td class="text-right">
                                                        <?php echo ($withoutqtn_amount1['amount'] != '') ? Controller::money_format_inr($withoutqtn_amount1['amount'], 2, 1) : '0.00'; ?>
                                                    </td>
                                                    <td class="text-right">
                                                        <?php echo ($withoutqtnpayment_amount1['payment_amount'] != '') ? Controller::money_format_inr($withoutqtnpayment_amount1['payment_amount'], 2, 1) : '0.00'; ?>
                                                    </td>
                                                    <td class="text-right">
                                                        <?php echo ($balance2 != '') ? Controller::money_format_inr($balance2, 2) : '0.00'; ?>
                                                    </td>
                                                <?php } else { ?>
                                                    <td class="text-right">
                                                        <?php echo ($scquotation_amount1['scquotation_amount'] != '') ? Controller::money_format_inr($scquotation_amount1['scquotation_amount'], 2, 1) : '0.00'; ?>
                                                    </td>
                                                    <td class="text-right">
                                                        <?php echo ($payment_amount1['payment_amount'] != '') ? Controller::money_format_inr($payment_amount1['payment_amount'], 2, 1) : '0.00'; ?>
                                                    </td>
                                                    <td class="text-right">
                                                        <?php echo ($balance != '') ? Controller::money_format_inr($balance, 2) : '0.00'; ?>
                                                    </td>
                                                    <td <?php echo isset($data['labour_entry']) ? 'rowspan=2' : ''; ?> class="text-right"><a
                                                            href="<?php echo Yii::app()->createAbsoluteUrl('subcontractorpayment/report', array(
                                                                'subcontractor_id' => $data['subcontractor_id'],
                                                                'company_id' => $data['company_id'],
                                                                'project_id' => $data['project_id'],
                                                                'custId' => 'reconcil'
                                                            )); ?>">
                                                            <?php echo ($balance != '') ? Controller::money_format_inr($balance, 2) : '0.00'; ?>
                                                        </a></td>
                                                <?php }
                                        } ?>

                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>

            <?php } ?>
        </div>
    </div>
</div>
<style>
    .odd {
        background: #FFF;
    }

    #parent {
        max-height: 500px;
    }

    thead th,
    tfoot td {
        background: #eee;
    }

    .table {
        margin-bottom: 0px;
    }



    .vertical_td {
        vertical-align: middle !important;
        text-align: left !important;
    }

    .quotation-payment {
        padding: 0px !important;
    }

    .quotation-payment div {
        padding: 5px;
        border-bottom: 1px solid #ccc;
    }

    .quotation-payment div:last-child {
        border-bottom: 0 !important;
    }

    .alt {
        background-color: #fdfdfd;
    }
</style>
<script>
    $(function () {
        $("#parent").css('max-height', $(window).height() - $("#myNavbar").height() - $("#vendorform").height() - (2 * $('footer').height()));
        $("#fixTable").tableHeadFixer({
            'foot': true,
            'head': true
        });
        $('.red_bg').parent('td').css('background', '#ff000038');
        $("select").select2();
    });
    $("table.alt_bg_table tr").filter(function () {
        return $(this).children().length > 7;
    }).filter(':even').addClass("alt");

    $("tr.alt td[rowspan]").each(function () {
        $(this).parent().nextAll().slice(0, this.rowSpan - 1).addClass('alt');
    });

    $("#date_from").datepicker({
        dateFormat: 'dd-mm-yy'
    });
    $("#date_to").datepicker({
        dateFormat: 'dd-mm-yy'
    });
    $("#payment_report_aging_two").click(function () {
        var redirecturl = $(this).attr("href");
        window.location.href = redirecturl;
    });
    $("#payment_report_aging_one").click(function () {
        var redirectUrl = $(this).attr("href");
        window.location.href = redirectUrl;
    });
    $("#reconcil-report-btn").click(function () {
        var redirectUrl = $(this).attr("href");
        window.location.href = redirectUrl;
    });
    $("#excel-btn").click(function () {
        var redirectUrl = $(this).attr("href");
        window.location.href = redirectUrl;
    });
    $("#pdf-btn").click(function () {
        var redirectUrl = $(this).attr("href");
        window.location.href = redirectUrl;
    });
    $("#excel-btn1").click(function () {
        var redirectUrl = $(this).attr("href");
        window.location.href = redirectUrl;
    });
    $("#un-report-btn").click(function () {
        var redirectUrl = $(this).attr("href");
        window.location.href = redirectUrl;
    });
    $("#reconcil-report-btn").click(function () {
        var redirectUrl = $(this).attr("href");
        window.location.href = redirectUrl;
    });

    $("#pdf-btn1").click(function () {
        var redirectUrl = $(this).attr("href");
        window.location.href = redirectUrl;
    });
    $("#payment_report_aging_one").click(function () {
        var redirectUrl = $(this).attr("href");
        window.location.href = redirectUrl;
    });
</script>