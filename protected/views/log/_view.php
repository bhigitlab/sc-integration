<?php
/* @var $this LogController */
/* @var $data JpLog */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('log_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->log_id), array('view', 'id'=>$data->log_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('log_data')); ?>:</b>
	<?php echo CHtml::encode($data->log_data); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('log_table')); ?>:</b>
	<?php echo CHtml::encode($data->log_table); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('log_primary_key')); ?>:</b>
	<?php echo CHtml::encode($data->log_primary_key); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('log_datetime')); ?>:</b>
	<?php echo CHtml::encode($data->log_datetime); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('log_action')); ?>:</b>
	<?php echo CHtml::encode($data->log_action); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('log_action_by')); ?>:</b>
	<?php echo CHtml::encode($data->log_action_by); ?>
	<br />


</div>