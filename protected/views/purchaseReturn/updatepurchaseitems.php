<?php
/* @var $this BillsController */
/* @var $model Bills */
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="row">
    <div class="col-sm-12">
        <div class="table-responsive">
            <table class="table">
                <thead class="thead-inverse entry-table">
                    <tr>
                        <th>Specifications/Remark</th>
                        <th>Actual Quantity</th>
                        <th>Billed Quantity</th>
                        <th>Remaining Quantity</th>
                        <th>Quantity</th>
                        <th>Unit</th>
                        <th>Rate</th>
                        <th>Amount</th>
                        <th>Discount Amount</th>
                        <th>Discount (%)</th>
                        <th>CGST</th>
                        <th>CGST (%)</th>
                        <th>SGST</th>
                        <th>SGST (%)</th>
                        <th>IGST</th>
                        <th>IGST (%)</th>
                        <th>Total Tax</th>
                        <th>Tax (%)</th>
                        <th>Total Amount</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $number = 1;
                    $amount = 0;
                    $totalQty = 0;
                    $bTotalQty = 0;
                    //$billId = $billId;
                    //echo $billId;
                    //die;
                    foreach ($purchase as $item) {
                        if ($item['pcategory']) {
                            $categorymodel = Specification::model()->findByPk($item['pcategory']);
                            if ($categorymodel)
                                $categoryName = $categorymodel->specification;
                            else
                                $categoryName = "Not available";
                        } else {
                            $categoryName = $item['bremark'] ? $item['bremark'] : $item['premark'];
                        }
                        $purchasemodel = Billitem::model()->findByPk($item['billitem_id']);
                        $itemQuantity = $purchasemodel->billitem_quantity;
                        $tblpx = Yii::app()->db->tablePrefix;
                        $itemData = Yii::app()->db->createCommand("SELECT SUM(returnitem_quantity) as quantitysum FROM " . $tblpx . "purchase_returnitem WHERE billitem_id = " . $item['billitem_id'])->queryRow();
                        $bItemQuantity = $itemData['quantitysum'] ? $itemData['quantitysum'] : 0;
                        if (strpos($bItemQuantity, ".") !== false) {
                            $bItemQuantity = number_format((float) $bItemQuantity, 2, '.', '');
                        } else {
                            $bItemQuantity = $bItemQuantity;
                        }
                        $quantityAvail = ($itemQuantity - $bItemQuantity) + $item["returnitem_quantity"];
                        $totalQty = $totalQty + $itemQuantity;
                        $bTotalQty = $bTotalQty + $bItemQuantity;
                        ?>
                        <!--<tr>
                    <th scope="row"><?php //if($quantityAvail>0) { ?><input type="checkbox" value ="<?php //echo $number-1; ?>" class="txtBox pastweek chkitem" id="chkitem<?php echo $number - 1; ?>" name="chkitem[<?php //echo $number-1; ?>]" <?php //echo $item["bill_id"] == $billId?"checked":""; ?>/><?php //} else { echo "&#x2612;"; } ?></th>
                    <input type="hidden" id="ids<?php //echo $number-1; ?>" value ="<?php //echo $item["item_id"]; ?>" class="txtBox pastweek ids" name="ids[]"/>
                    <input type="hidden" id="category<?php //echo $number-1; ?>" value ="<?php //echo $item["pcategory"]?$item["pcategory"]:''; ?>" class="txtBox pastweek category" name="category[]"/>
                    <td id="<?php //echo $number-1; ?>"><input type="text" id="categoryname<?php //echo $number-1; ?>" class="txtBox pastweek" name="categoryname[]" value="<?php //echo $item['pcategory']?$categorymodel->category_name:$item['premark']; ?>" readonly="true"/></td>
                    <td id="<?php //echo $number-1; ?>"><input type="text" id="quantity<?php //echo $number-1; ?>" value ="<?php //echo $item["quantity"]; ?>" class="txtBox pastweek quantity" name="quantity[]" placeholder="Please click to edit" readonly="true"/></td>
                    <td id="<?php //echo $number-1; ?>"><input type="text" id="unit<?php //echo $number-1; ?>" value ="<?php //echo $item["unit"]; ?>" class="txtBox pastweek" name="unit[]" placeholder="Please click to edit" readonly="true"/></td>
                    <td id="<?php //echo $number-1; ?>"><input type="text" id="rate<?php //echo $number-1; ?>" value ="<?php //echo $item["rate"]; ?>" class="txtBox pastweek rate" name="rate[]" placeholder="Please click to edit" readonly="true"/></td>
                    <td id="<?php //echo $number-1; ?>"><input type="text" id="amount<?php //echo $number-1; ?>" value ="<?php //echo $item["amount"]; ?>" class="txtBox pastweek amt" name="amount[]" placeholder="Please click to edit" readonly="true"/></td>
                </tr>-->
                        <tr class="pitems">
                            <input type="hidden" id="ids<?php echo $number - 1; ?>"
                                value="<?php echo $item["billitem_id"]; ?>" class="txtBox pastweek ids" name="ids[]" />
                            <input type="hidden" id="category<?php echo $number - 1; ?>"
                                value="<?php echo $item["pcategory"] ? $item["pcategory"] : ''; ?>"
                                class="txtBox pastweek category" name="category[]" />

                            <?php $totalAmount = ($item["returnitem_amount"] ? $item["returnitem_amount"] : 0) - ($item["returnitem_discountamount"] ? $item["returnitem_discountamount"] : 0) + ($item["returnitem_taxamount"] ? $item["returnitem_taxamount"] : 0); ?>
                            <!--<input type="hidden" id="bicategoryname<?php //echo $number-1; ?>" value="<?php //echo $categoryName; ?>" class="txtBox pastweek" name="bicategoryname[]"/>-->
                            <input type="hidden" id="biremquantity<?php echo $number - 1; ?>"
                                value="<?php echo $quantityAvail; ?>" name="biremquantity[]" />
                            <td id="<?php echo $number - 1; ?>">
                                <div id="bicategoryname<?php echo $number - 1; ?>">
                                    <?php echo $categoryName; ?>
                                </div>
                            </td>

                            <td id="<?php echo $number - 1; ?>">
                                <div id="actualquantity<?php echo $number - 1; ?>">
                                    <?php echo $itemQuantity; ?>
                                </div>
                            </td>
                            <td id="<?php echo $number - 1; ?>">
                                <div id="billedquantity<?php echo $number - 1; ?>">
                                    <?php echo $bItemQuantity; ?>
                                </div>
                            </td>
                            <input type="hidden" id="orrate<?php echo $number - 1; ?>"
                                value="<?php echo $item["returnitem_rate"] ? number_format((float) $item["returnitem_rate"], 2, '.', '') : number_format((float) $item["billitem_rate"], 2, '.', ''); ?>"
                                name="orrate[]" />
                            <td id="<?php echo $number - 1; ?>">
                                <div id="availablequantity<?php echo $number - 1; ?>">
                                    <?php echo $quantityAvail; ?>
                                </div>
                            </td>
                            <td id="<?php echo $number - 1; ?>"><input type="text" id="biquantity<?php echo $number - 1; ?>"
                                    value="<?php echo $item["returnitem_quantity"] ? $item["returnitem_quantity"] : 0; ?>"
                                    class="form-control fnext validate[required,custom[number],min[0],max[<?php echo $quantityAvail; ?>]]"
                                    name="biquantity[]" /></td>
                            <td id="<?php echo $number - 1; ?>">
                                <div id="biunit<?php echo $number - 1; ?>">
                                    <?php echo $item["returnitem_unit"] ? $item["returnitem_unit"] : $item["billitem_unit"]; ?>
                                </div>
                            </td>
                            <td id="<?php echo $number - 1; ?>"><input type="text" id="birate<?php echo $number - 1; ?>"
                                    value="<?php echo $item["returnitem_rate"] ? number_format((float) $item["returnitem_rate"], 2, '.', '') : number_format((float) $item["billitem_rate"], 2, '.', ''); ?>"
                                    class="form-control lnext validate[required,custom[number],min[0]]" name="birate[]" />
                            </td>
                            <td id="<?php echo $number - 1; ?>">
                                <div id="biamount<?php echo $number - 1; ?>" class="pricediv">
                                    <?php echo $item["returnitem_amount"] ? number_format((float) $item["returnitem_amount"], 2, '.', '') : 0; ?>
                                </div>
                            </td>
                            <td id="<?php echo $number - 1; ?>">
                                <div id="damount<?php echo $number - 1; ?>" class="pricediv">
                                    <?php echo $item["returnitem_discountamount"] ? number_format((float) $item["returnitem_discountamount"], 2, '.', '') : 0; ?>
                                </div>
                            </td>
                            <td id="<?php echo $number - 1; ?>">
                                <div id="dpercent<?php echo $number - 1; ?>" class="pricediv">
                                    <?php echo $item["returnitem_discountpercent"] ? $item["returnitem_discountpercent"] : 0; ?>
                                </div>
                            </td>
                            <td id="<?php echo $number - 1; ?>">
                                <div id="cgst<?php echo $number - 1; ?>" class="pricediv">
                                    <?php echo $item["returnitem_cgst"] ? number_format((float) $item["returnitem_cgst"], 2, '.', '') : 0; ?>
                                </div>
                            </td>
                            <td id="<?php echo $number - 1; ?>">
                                <div id="cgstpercent<?php echo $number - 1; ?>" class="pricediv">
                                    <?php echo $item["returnitem_cgstpercent"] ? $item["returnitem_cgstpercent"] : 0; ?>
                                </div>
                            </td>
                            <td id="<?php echo $number - 1; ?>">
                                <div id="sgst<?php echo $number - 1; ?>" class="pricediv">
                                    <?php echo $item["returnitem_sgst"] ? number_format((float) $item["returnitem_sgst"], 2, '.', '') : 0; ?>
                                </div>
                            </td>
                            <td id="<?php echo $number - 1; ?>">
                                <div id="sgstpercent<?php echo $number - 1; ?>" class="pricediv">
                                    <?php echo $item["returnitem_sgstpercent"] ? $item["returnitem_sgstpercent"] : 0; ?>
                                </div>
                            </td>

                            <td id="<?php echo $number - 1; ?>">
                                <div id="igst<?php echo $number - 1; ?>" class="pricediv">
                                    <?php echo $item["returnitem_igst"] ? number_format((float) $item["returnitem_igst"], 2, '.', '') : 0; ?>
                                </div>
                            </td>
                            <td id="<?php echo $number - 1; ?>">
                                <div id="igstpercent<?php echo $number - 1; ?>" class="pricediv">
                                    <?php echo $item["returnitem_igstpercent"] ? $item["returnitem_igstpercent"] : 0; ?>
                                </div>
                            </td>

                            <td id="<?php echo $number - 1; ?>">
                                <div id="taxamount<?php echo $number - 1; ?>" class="pricediv">
                                    <?php echo $item["returnitem_taxamount"] ? number_format((float) $item["returnitem_taxamount"], 2, '.', '') : 0; ?>
                                </div>
                            </td>
                            <td id="<?php echo $number - 1; ?>">
                                <div id="taxpercent<?php echo $number - 1; ?>" class="pricediv">
                                    <?php echo $item["returnitem_taxpercent"] ? $item["returnitem_taxpercent"] : 0; ?>
                                </div>
                            </td>
                            <td id="<?php echo $number - 1; ?>">
                                <div id="totalamount<?php echo $number - 1; ?>" class="pricediv">
                                    <?php echo number_format((float) $totalAmount, 2, '.', ''); ?>
                                </div>
                            </td>
                            <input type="hidden" id="billitem<?php echo $number - 1; ?>"
                                value="<?php echo $item["returnitem_id"] ?>" name="billitem[]" />
                            <input type="hidden" id="savedquantity<?php echo $number - 1; ?>"
                                value="<?php echo $item["returnitem_quantity"] ? $item["returnitem_quantity"] : 0; ?>"
                                name="savedquantity[]" />
                            <td>
                                <div id="tickmark<?php echo $number - 1; ?>">
                                    <?php if ($item["returnitem_quantity"] != 0) { ?><i class="fa fa-check"
                                            aria-hidden="true"></i>
                                    <?php } ?>
                                </div>
                            </td>
                        </tr>
                        <?php
                        $amount = $amount + $item["billitem_amount"];
                        $number = $number + 1;
                    }
                    ?>
                    <input type="hidden" name="tqty[]" id="tqty" value="<?php echo $totalQty; ?>" />
                    <input type="hidden" name="btqty[]" id="btqty" value="<?php echo $bTotalQty; ?>" />
                    <input type="hidden" id="totrows" value="<?php echo $number - 1; ?>" class="txtBox pastweek totrows"
                        name="totrows" />
                </tbody>
            </table>
        </div>
        <!--<input type="hidden" name="aj-amount" id="aj-amount" value="<?php //echo $amount; ?>"/>-->
    </div>
</div>