<div class="modal fade" id="rejectModal" tabindex="-1" role="dialog" aria-labelledby="rejectModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="rejectModalLabel">Reject Labour</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="rejectForm">
                    <input type="hidden" id="rejectId" name="id">
                    <div class="form-group">
                        <label for="remarks">Remarks</label>
                        <textarea class="form-control" id="remarks" name="remarks" rows="3" required></textarea>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" onClick="submitReject()">Submit</button>
            </div>
        </div>
    </div>
</div>

<script>
    function openRejectModal(id) {
        $('#rejectId').val(id); 
        $('#rejectModal').modal('show'); 
    }
    
    function submitReject() {
        var id = $('#rejectId').val();
        var remarks = $('#remarks').val();
         
        if (remarks === "") {
            alert("Remarks field cannot be empty.");
            return;
        }

        $.ajax({
            type: "POST",
            url: "index.php?r=DailyReport/rejectLabour",
            data: {
                id: id,
                remarks: remarks,
            },
            success: function(response) {
                var result = JSON.parse(response);
                if (result.response === 'success') {
                    alert('Labour rejected successfully');
                    $('#rejectModal').modal('hide');  
                    location.reload();  
                } else {
                    alert('Error: ' + result.msg);
                }
            },
            error: function() {
                alert('An error occurred. Please try again.');
            }
            });
    }
</script>