<?php
/* @var $this CompanyexpensetypeController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Companyexpensetypes',
);

$this->menu=array(
	array('label'=>'Create Companyexpensetype', 'url'=>array('create')),
	array('label'=>'Manage Companyexpensetype', 'url'=>array('admin')),
);
?>

<h1>Companyexpensetypes</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
