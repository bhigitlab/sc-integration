<?php

/**
 * This is the model class for table "{{dailyexpense}}".
 *
 * The followings are the available columns in table '{{dailyexpense}}':
 * @property integer $dailyexp_id
 * @property string $date
 * @property integer $exp_type_id
 * @property integer $bill_id
 * @property integer $expensehead_id
 * @property integer $expense_type
 * @property integer $vendor_id
 * @property double $dailyexpense_amount
 * @property double $dailyexpense_sgstp
 * @property double $dailyexpense_sgst
 * @property double $dailyexpense_cgstp
 * @property double $dailyexpense_cgst
 * @property double $dailyexpense_igstp
 * @property double $dailyexpense_igst
 * @property double $amount
 * @property string $description
 * @property integer $dailyexpense_receipt_type
 * @property integer $dailyexpense_receipt_head
 * @property double $dailyexpense_receipt
 * @property integer $dailyexpense_purchase_type
 * @property double $dailyexpense_paidamount
 * @property integer $bank_id
 * @property string $dailyexpense_chequeno
 * @property integer $user_id
 * @property integer $created_by
 * @property string $created_date
 * @property integer $updated_by
 * @property string $updated_date
 * @property string $data_entry
 * @property integer $exp_type
 * @property string $dailyexpense_type
 * @property integer $company_id
 * @property integer $reconciliation_status
 * @property string $reconciliation_date
 * @property integer $update_status
 * @property integer $delete_status
 *
 * The followings are the available model relations:
 * @property CompanyExpenseType $expType
 * @property UsersOffline $createdBy
 * @property UsersOffline $updatedBy
 */
class Dailyexpense extends CActiveRecord
{
	public $fromdate;
	public $todate;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{dailyexpense}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('date, amount, description, user_id, created_by, created_date, exp_type, dailyexpense_type', 'required'),
			array('exp_type_id, expensehead_id, expense_type, vendor_id, dailyexpense_receipt_type, dailyexpense_receipt_head, dailyexpense_purchase_type, bank_id, user_id, created_by, updated_by, exp_type, company_id, reconciliation_status, update_status, delete_status', 'numerical', 'integerOnly' => true),
			array('dailyexpense_amount, dailyexpense_sgstp, dailyexpense_sgst, dailyexpense_cgstp, dailyexpense_cgst, dailyexpense_igstp, dailyexpense_igst, amount, dailyexpense_receipt, dailyexpense_paidamount', 'numerical'),
			array('description', 'length', 'max' => 300),
			array('dailyexpense_chequeno', 'length', 'max' => 100),
			array('data_entry', 'length', 'max' => 30),
			array('dailyexpense_type', 'length', 'max' => 7),
			array('updated_date, reconciliation_date', 'safe'),
			array('amount', 'checkNegativeBalance'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('dailyexp_id, date, exp_type_id, bill_id, expensehead_id, expense_type, vendor_id, dailyexpense_amount, dailyexpense_sgstp, dailyexpense_sgst, dailyexpense_cgstp, dailyexpense_cgst, dailyexpense_igstp, dailyexpense_igst, amount, description, dailyexpense_receipt_type, dailyexpense_receipt_head, dailyexpense_receipt, dailyexpense_purchase_type, dailyexpense_paidamount, bank_id, dailyexpense_chequeno, user_id, created_by, created_date, updated_by, updated_date, data_entry, exp_type, dailyexpense_type, company_id, reconciliation_status, reconciliation_date, employee_id, update_status, delete_status', 'safe', 'on' => 'search'),
		);
	}
	public function ValidateType($attribute, $params)
	{
		if ($this->exp_type == "") {
			$this->addError('exp_type', ('Please Choose Type'));
		}
		if ($this->exp_type_id == "") {
			$this->addError('exp_type_id', ('Please Choose Expense Type'));
		}
		if ($this->amount < 0) {
			$this->addError('amount', ('Please Enter Numeric Value'));
		}
	}
	public function checkNegativeBalance() {
		if($this->dailyexp_id !=""){
			$sql = "SELECT dailyexpense_paidamount "
				. " FROM `jp_dailyexpense` "
				. " WHERE dailyexp_id=".$this->dailyexp_id;
			$oldpaidamount = Yii::app()->db->createCommand($sql)->queryScalar();
		}
		if ((($this->dailyexp_id !="") && ($oldpaidamount != $this->dailyexpense_paidamount)) || ($this->dailyexp_id =="")) {
            if ($this->exp_type == 73) {
                $invoice_amount = 0;
                $expense_amount = 0;
                $payment_type = "";
                $available_amount = "";
                $company_id = $this->company_id;
                if ($this->expense_type == '89' || $this->expense_type == '88' || $this->expense_type == '103') {
                    $payment_type = $this->expense_type;
                }
				if($this->expensehead_type==4){ //cash withdrawal
					$payment_type=88;					
				}
				if($this->expensehead_type==5){ //cash deposit
					$payment_type=89;
					$this->bank_id=NULL;
				}
                if (!empty($company_id)) {
                    $invoice_amount = BalanceHelper::cashInvoice(
                                    $payment_type,
                                    $company_id,
                                    $this->user_id,
                                    $this->bank_id,
                                    $this->date
                    );
                    $expense_amount = BalanceHelper::cashExpense(
                                    $payment_type,
                                    $company_id,
                                    $this->user_id,
                                    $this->dailyexp_id,
                                    $this->bank_id,
                                    $this->date
                    );

                    if (is_numeric($invoice_amount) && is_numeric($expense_amount)) {
                        $available_amount = $invoice_amount - $expense_amount;
                    }


                    if (is_numeric($available_amount) && $available_amount >= $this->dailyexpense_paidamount) {
                        $closing_balance = $available_amount - $this->dailyexpense_paidamount;  //initial closing balace            
                        $date = date('Y-m-d', strtotime('+1 day', strtotime($this->date)));
                        $paymentArray = $this->getPaymentArray($date, NULL, $status = 0, $payment_type, $company_id, $this->bank_id);
						if(!empty($paymentArray)){
							
							if (count($paymentArray) < 20000) {

								foreach ($paymentArray as $data) {
									if ($data['type'] == 73) { //expense
										$closing_balance -= $data['paidamount'];
									} else {
										$closing_balance += $data['receipt'];
									}

									if ($closing_balance < 0) {
										$this->addError("amount", "Insufficient balance on " . Controller::formattedcommonDate($data['date']) . " in " . $data['tablename'] . " - ID: #" . $data['entry_id']);
										return;
									}
								}
							} else {
								$this->addError("amount", "Limit Of 20000 Entries Exceeded");
								return;
							}
						}
                    } else if (($this->dailyexpense_paidamount > $available_amount) && $payment_type != "103" && $payment_type != "") {
                        $this->addError("amount", "insufficient balance on " . Controller::formattedcommonDate($this->date));
                        $date = date('Y-m-d', strtotime($this->date));
                        $this->addError("amount", "insufficient balance on " . Controller::formattedcommonDate($this->date));
                    }

                    if (!is_numeric($available_amount)) {
                        $this->addError("", "Something went wrong! Check again or contact support team");
                    }
                }
            }
		}
    }

    /**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'Bills' => array(self::BELONGS_TO, 'Bills', 'bill_id'),
			'expType' => array(self::BELONGS_TO, 'Companyexpensetype', 'exp_type_id'),
			'Bank' => array(self::BELONGS_TO, 'Bank', 'bank_id'),
			'Deposit' => array(self::BELONGS_TO, 'Deposit', 'expensehead_id'),
			'CompExpType' => array(self::BELONGS_TO, 'Companyexpensetype', 'expensehead_id'),
			'ExpenseType' => array(self::BELONGS_TO, 'Status', 'expense_type'),
			'ReceiptType' => array(self::BELONGS_TO, 'Status', 'dailyexpense_receipt_type'),
			'Company' => array(self::BELONGS_TO, 'Company', 'company_id'),
			'createdBy' => array(self::BELONGS_TO, 'UsersOffline', 'created_by'),
			'updatedBy' => array(self::BELONGS_TO, 'UsersOffline', 'updated_by'),
			'employee' => array(self::BELONGS_TO, 'Users', 'employee_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'dailyexp_id' => 'Dailyexp',
			'date' => 'Date',
			'exp_type_id' => 'Exp Type',
			'bill_id' => 'Bill',
			'expensehead_id' => 'Expensehead',
			'expense_type' => 'Expense Type',
			'vendor_id' => 'Vendor',
			'dailyexpense_amount' => 'Dailyexpense Amount',
			'dailyexpense_sgstp' => 'Dailyexpense Sgstp',
			'dailyexpense_sgst' => 'Dailyexpense Sgst',
			'dailyexpense_cgstp' => 'Dailyexpense Cgstp',
			'dailyexpense_cgst' => 'Dailyexpense Cgst',
			'dailyexpense_igstp' => 'Dailyexpense Igstp',
			'dailyexpense_igst' => 'Dailyexpense Igst',
			'amount' => 'Amount',
			'description' => 'Description',
			'dailyexpense_receipt_type' => 'Dailyexpense Receipt Type',
			'dailyexpense_receipt_head' => 'Dailyexpense Receipt Head',
			'dailyexpense_receipt' => 'Dailyexpense Receipt',
			'dailyexpense_purchase_type' => 'Dailyexpense Purchase Type',
			'dailyexpense_paidamount' => 'Dailyexpense Paidamount',
			'bank_id' => 'Bank',
			'dailyexpense_chequeno' => 'Dailyexpense Chequeno',
			'user_id' => 'User',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
			'updated_by' => 'Updated By',
			'updated_date' => 'Updated Date',
			'data_entry' => 'Data Entry',
			'exp_type' => 'Exp Type',
			'dailyexpense_type' => 'Dailyexpense Type',
			'company_id' => 'Company',
			'reconciliation_status' => 'Reconciliation Status',
			'reconciliation_date' => 'Reconciliation Date',
			'employee_id' => 'Employee Name',
			'update_status' => 'Update Status',
			'delete_status' => 'Delete Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;
		$criteria->with = array('expType', 'Deposit', 'CompExpType');

		$criteria->compare('dailyexp_id', $this->dailyexp_id);
		$criteria->compare('date', $this->date, true);
		$criteria->compare('exp_type_id', $this->exp_type_id);
		$criteria->compare('bill_id', $this->bill_id);
		$criteria->compare('expensehead_id', $this->expensehead_id);
		$criteria->compare('expense_type', $this->expense_type);
		$criteria->compare('vendor_id', $this->vendor_id);
		$criteria->compare('dailyexpense_amount', $this->dailyexpense_amount);
		$criteria->compare('dailyexpense_sgstp', $this->dailyexpense_sgstp);
		$criteria->compare('dailyexpense_sgst', $this->dailyexpense_sgst);
		$criteria->compare('dailyexpense_cgstp', $this->dailyexpense_cgstp);
		$criteria->compare('dailyexpense_cgst', $this->dailyexpense_cgst);
		$criteria->compare('dailyexpense_igstp', $this->dailyexpense_igstp);
		$criteria->compare('dailyexpense_igst', $this->dailyexpense_igst);
		$criteria->compare('amount', $this->amount);
		$criteria->compare('description', $this->description, true);
		$criteria->compare('dailyexpense_receipt_type', $this->dailyexpense_receipt_type);
		$criteria->compare('dailyexpense_receipt_head', $this->dailyexpense_receipt_head);
		$criteria->compare('dailyexpense_receipt', $this->dailyexpense_receipt);
		$criteria->compare('dailyexpense_purchase_type', $this->dailyexpense_purchase_type);
		$criteria->compare('dailyexpense_paidamount', $this->dailyexpense_paidamount);
		$criteria->compare('bank_id', $this->bank_id);
		$criteria->compare('dailyexpense_chequeno', $this->dailyexpense_chequeno, true);
		$criteria->compare('user_id', $this->user_id);
		$criteria->compare('created_by', $this->created_by);
		$criteria->compare('created_date', $this->created_date, true);
		$criteria->compare('updated_by', $this->updated_by);
		$criteria->compare('updated_date', $this->updated_date, true);
		$criteria->compare('data_entry', $this->data_entry, true);
		$criteria->compare('exp_type', $this->exp_type);
		$criteria->compare('dailyexpense_type', $this->dailyexpense_type, true);

		$criteria->compare('reconciliation_status', $this->reconciliation_status);
		$criteria->compare('reconciliation_date', $this->reconciliation_date);
		$criteria->compare('employee_id', $this->employee_id);
		$criteria->compare('CompExpType.company_exp_id', $this->expensehead_id);
		$criteria->compare('Deposit.deposit_id', $this->expensehead_id);
		$criteria->compare('expType.company_exp_id', $this->exp_type_id);
		$criteria->compare('update_status', $this->update_status);
		$criteria->compare('delete_status', $this->delete_status);

		if (!empty($this->company_id)) {
			$criteria->compare('t.company_id', $this->company_id);
		} else {
			$user = Users::model()->findByPk(Yii::app()->user->id);
			$arrVal = explode(',', $user->company_id);
			$newQuery = "";
			foreach ($arrVal as $arr) {
				if ($newQuery) $newQuery .= ' OR';
				$newQuery .= " FIND_IN_SET('" . $arr . "', t.company_id)";
			}
			$criteria->addCondition($newQuery);
		}
		if (!empty($this->fromdate) && !empty($this->todate)) {
			$criteria->addCondition("t.date >= '" . date('Y-m-d', strtotime($this->fromdate)) . "'");
			$criteria->addCondition("t.date <= '" . date('Y-m-d', strtotime($this->todate)) . "'");
		} else if (empty($this->fromdate) && !empty($this->todate)) {
			$criteria->addCondition("t.date <= '" . date('Y-m-d', strtotime($this->todate)) . "'");
		} else if (!empty($this->fromdate) && empty($this->todate)) {
			$criteria->addCondition("t.date >= '" . date('Y-m-d', strtotime($this->fromdate)) . "'");
		}

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'pagination' => false,
			'sort' => array(
				'defaultOrder' => 't.dailyexp_id DESC',
				'attributes' => array(
					'expType' => array(
						'asc' => 'expType.name',
						'desc' => 'expType.name DESC',
					),
					'Deposit' => array(
						'asc' => 'Deposit.deposit_name',
						'desc' => 'Deposit.deposit_name DESC',
					),
					'CompExpType' => array(
						'asc' => 'CompExpType.name',
						'desc' => 'CompExpType.name DESC',
					),
					'*',
				),
			),
		));
	}
	public function newsearch()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;
		$criteria->with = array('expType', 'Deposit', 'CompExpType');

		$criteria->compare('dailyexp_id', $this->dailyexp_id);
		$criteria->compare('date', $this->date, true);
		$criteria->compare('exp_type_id', $this->exp_type_id);
		$criteria->compare('bill_id', $this->bill_id);
		$criteria->compare('expensehead_id', $this->expensehead_id);
		if (!empty($model->exp_type)) {
			$criteria->compare('expense_type', $this->expense_type);
			$criteria->compare('dailyexpense_receipt_type', $this->dailyexpense_receipt_type);
		} else {
			if (!empty($this->expense_type) && !empty($this->dailyexpense_receipt_type)) {
				$criteria->addCondition("t.expense_type = " . $this->expense_type . " OR dailyexpense_receipt_type = " . $this->dailyexpense_receipt_type);
			} else if (!empty($this->expense_type) && empty($this->dailyexpense_receipt_type)) {
				$criteria->compare('expense_type', $this->expense_type);
			} else if (empty($this->expense_type) && !empty($this->dailyexpense_receipt_type)) {
				$criteria->compare('dailyexpense_receipt_type', $this->dailyexpense_receipt_type);
			}
		}

		$criteria->compare('vendor_id', $this->vendor_id);
		$criteria->compare('dailyexpense_amount', $this->dailyexpense_amount);
		$criteria->compare('dailyexpense_sgstp', $this->dailyexpense_sgstp);
		$criteria->compare('dailyexpense_sgst', $this->dailyexpense_sgst);
		$criteria->compare('dailyexpense_cgstp', $this->dailyexpense_cgstp);
		$criteria->compare('dailyexpense_cgst', $this->dailyexpense_cgst);
		$criteria->compare('dailyexpense_igstp', $this->dailyexpense_igstp);
		$criteria->compare('dailyexpense_igst', $this->dailyexpense_igst);
		$criteria->compare('amount', $this->amount);
		$criteria->compare('description', $this->description, true);
		$criteria->compare('dailyexpense_receipt_head', $this->dailyexpense_receipt_head);
		$criteria->compare('dailyexpense_receipt', $this->dailyexpense_receipt);
		$criteria->compare('dailyexpense_purchase_type', $this->dailyexpense_purchase_type);
		$criteria->compare('dailyexpense_paidamount', $this->dailyexpense_paidamount);
		$criteria->compare('bank_id', $this->bank_id);
		$criteria->compare('dailyexpense_chequeno', $this->dailyexpense_chequeno, true);
		$criteria->compare('user_id', $this->user_id);
		$criteria->compare('created_by', $this->created_by);
		$criteria->compare('created_date', $this->created_date, true);
		$criteria->compare('updated_by', $this->updated_by);
		$criteria->compare('updated_date', $this->updated_date, true);
		$criteria->compare('data_entry', $this->data_entry, true);
		$criteria->compare('exp_type', $this->exp_type);
		$criteria->compare('dailyexpense_type', $this->dailyexpense_type, true);
		$criteria->compare('expensehead_type', $this->expensehead_type);
		$criteria->compare('reconciliation_status', $this->reconciliation_status);
		$criteria->compare('reconciliation_date', $this->reconciliation_date);
		$criteria->compare('employee_id', $this->employee_id);
		// $criteria->compare('CompExpType.company_exp_id',$this->expensehead_id);
		// $criteria->compare('Deposit.deposit_id',$this->expensehead_id);
		// $criteria->compare('expType.company_exp_id',$this->exp_type_id);
		$criteria->compare('update_status', $this->update_status);
		$criteria->compare('delete_status', $this->delete_status);

		if (!empty($this->company_id)) {
			$criteria->compare('t.company_id', $this->company_id);
		} else {
			$user = Users::model()->findByPk(Yii::app()->user->id);
			$arrVal = explode(',', $user->company_id);
			$newQuery = "";
			foreach ($arrVal as $arr) {
				if ($newQuery) $newQuery .= ' OR';
				$newQuery .= " FIND_IN_SET('" . $arr . "', t.company_id)";
			}
			$criteria->addCondition($newQuery);
		}
		if (!empty($this->fromdate) && !empty($this->todate)) {
			$criteria->addCondition("t.date >= '" . date('Y-m-d', strtotime($this->fromdate)) . "'");
			$criteria->addCondition("t.date <= '" . date('Y-m-d', strtotime($this->todate)) . "'");
		} else if (empty($this->fromdate) && !empty($this->todate)) {
			$criteria->addCondition("t.date <= '" . date('Y-m-d', strtotime($this->todate)) . "'");
		} else if (!empty($this->fromdate) && empty($this->todate)) {
			$criteria->addCondition("t.date >= '" . date('Y-m-d', strtotime($this->fromdate)) . "'");
		}

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'pagination' => false,
			'sort' => array(
				'defaultOrder' => 't.dailyexp_id DESC',
				'attributes' => array(
					'expType' => array(
						'asc' => 'expType.name',
						'desc' => 'expType.name DESC',
					),
					'Deposit' => array(
						'asc' => 'Deposit.deposit_name',
						'desc' => 'Deposit.deposit_name DESC',
					),
					'expType' => array(
						'asc' => 'expType.name',
						'desc' => 'expType.name DESC',
					),
					'Expense' => array(
						'asc' => 'exp_type desc, expType.name',
						'desc' => 'exp_type desc, expType.name DESC',
					),
					'Receipt' => array(
						'asc' => 'exp_type asc, expType.name',
						'desc' => 'exp_type asc, expType.name DESC',
					),

					'Type' => array(
						'asc' => 'expensehead_type',
						'desc' => 'expensehead_type DESC',
					),
					'*',
				),
			),
		));
	}
	public function usersearch()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('dailyexp_id', $this->dailyexp_id);
		$criteria->compare('date', $this->date, true);
		$criteria->compare('exp_type_id', $this->exp_type_id);
		$criteria->compare('bill_id', $this->bill_id);
		$criteria->compare('expensehead_id', $this->expensehead_id);
		$criteria->compare('expense_type', $this->expense_type);
		$criteria->compare('vendor_id', $this->vendor_id);
		$criteria->compare('dailyexpense_amount', $this->dailyexpense_amount);
		$criteria->compare('dailyexpense_sgstp', $this->dailyexpense_sgstp);
		$criteria->compare('dailyexpense_sgst', $this->dailyexpense_sgst);
		$criteria->compare('dailyexpense_cgstp', $this->dailyexpense_cgstp);
		$criteria->compare('dailyexpense_cgst', $this->dailyexpense_cgst);
		$criteria->compare('dailyexpense_igstp', $this->dailyexpense_igstp);
		$criteria->compare('dailyexpense_igst', $this->dailyexpense_igst);
		$criteria->compare('amount', $this->amount);
		$criteria->compare('description', $this->description, true);
		$criteria->compare('dailyexpense_receipt_type', $this->dailyexpense_receipt_type);
		$criteria->compare('dailyexpense_receipt_head', $this->dailyexpense_receipt_head);
		$criteria->compare('dailyexpense_receipt', $this->dailyexpense_receipt);
		$criteria->compare('dailyexpense_purchase_type', $this->dailyexpense_purchase_type);
		$criteria->compare('dailyexpense_paidamount', $this->dailyexpense_paidamount);
		$criteria->compare('bank_id', $this->bank_id);
		$criteria->compare('dailyexpense_chequeno', $this->dailyexpense_chequeno, true);
		$criteria->compare('user_id', $this->user_id);
		$criteria->compare('created_by', $this->created_by);
		$criteria->compare('created_date', $this->created_date, true);
		$criteria->compare('updated_by', $this->updated_by);
		$criteria->compare('updated_date', $this->updated_date, true);
		$criteria->compare('data_entry', $this->data_entry, true);
		$criteria->compare('exp_type', $this->exp_type);
		$criteria->compare('dailyexpense_type', $this->dailyexpense_type, true);
		$criteria->compare('company_id', $this->company_id);
		$criteria->compare('reconciliation_status', $this->reconciliation_status);
		$criteria->compare('reconciliation_date', $this->reconciliation_date);
		$criteria->compare('employee_id', $this->employee_id);
		$criteria->compare('update_status', $this->update_status);
		$criteria->compare('delete_status', $this->delete_status);
		if (!empty($this->fromdate) && !empty($this->todate)) {
			$criteria->addCondition("date >= '" . date('Y-m-d', strtotime($this->fromdate)) . "'");
			$criteria->addCondition("date <= '" . date('Y-m-d', strtotime($this->todate)) . "'");
		} else if (empty($this->fromdate) && !empty($this->todate)) {
			$criteria->addCondition("date <= '" . date('Y-m-d', strtotime($this->todate)) . "'");
		} else if (!empty($this->fromdate) && empty($this->todate)) {
			$criteria->addCondition("date >= '" . date('Y-m-d', strtotime($this->fromdate)) . "'");
		}
		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'pagination' => false,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Dailyexpense the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}
	public function grandtotaldebit($where)
	{

		$tblpx = Yii::app()->db->tablePrefix;
		$data = Yii::app()->db->createCommand("SELECT sum(amount) as sumamt FROM `" . $tblpx . "dailyexpense`
                " . $where . " AND exp_type= 1")->queryAll();
		Yii::app()->user->setState("totaldebit", $data);  
		return $data;
	}
	public function grandtotalcredit($where)
	{
		$tblpx = Yii::app()->db->tablePrefix;
		$data = Yii::app()->db->createCommand("SELECT sum(amount) as sumamt FROM `" . $tblpx . "dailyexpense`
                " . $where . " AND exp_type= 0")->queryAll();
		Yii::app()->user->setState("totalcredit", $data);  
		return $data;
	}
	public function getSum($dataProvider, $field)
	{
		$sum = 0;
		foreach ($dataProvider->getData() as $data) {
			$sum = $sum + $data->$field;
		}
		return $sum;
	}

    public function getPaymentArray($date, $id, $status, $payment_type, $company_id, $bank_id) {
		$current_date = date('Y-m-d');
        //73=>expense , 72=>receipt
        $condition = "";
        $condition1 = "`date` BETWEEN '" . $date . "' AND '" . $current_date . "'";
        $condition2 = "";
		$condition3 = "";
		$dailyexpense_cond="";
		$dailyreciept_cond="";
		$vendor_condition="";
		$subcontractor_condition="";
		$dailyexp = DailyExpense::model()->findByPk($id);
		$ref_id="";
		if($dailyexp['expensehead_type'] == 5 || $dailyexp['expensehead_type'] == 4 ){
			$sql = "SELECT dailyexp_id FROM jp_dailyexpense WHERE `transaction_parent` =".$id;
			$ref_id = Yii::app()->db->createCommand($sql)->queryScalar();
		}

        $current_date = date('Y-m-d');
        //73=>expense , 72=>receipt
        if ($id != "") {
            $condition = " AND  `dailyexp_id` > " . $id;
			if($ref_id!=""){
				$condition = " AND (`dailyexp_id` > " . $id ." AND `dailyexp_id` != $ref_id)";
			}
        }
		
        if ($id != "" && $status == 1) {
            $condition = " AND  `dailyexp_id` >= " . $id;
			if($ref_id!=""){
				$condition = " AND (`dailyexp_id` >= " . $id ." AND `dailyexp_id` != $ref_id)";
			}
            $condition1 = "`date` ='" . $date . "'";
        }
		
        $company_condition = " AND company_id=  $company_id";
        
        if ($payment_type == 88) {
            $condition2 = " AND reconciliation_status = 1 AND bank_id = " . $bank_id;
			$condition3 = "  AND (expense_type=88 OR payment_type=88)  AND subcontractor_id IS NULL";
			$dailyreciept_cond =" AND (dailyexpense_receipt_type=88 AND "
			. " dailyexpense_chequeno  IS NOT NULL  "
			. " AND reconciliation_status =1"
			. " AND company_id=" . $company_id.")  "
			. " OR (dailyexpense_receipt_type=88 "
			. " AND expensehead_type=4  AND "
			. " company_id=" . $company_id.") AND "
			. " parent_status='1' AND "
			. " exp_type=72 ";
			$dailyexpense_cond =" AND (expense_type=88) AND exp_type=73 "
			. " AND parent_status='1' "
			. " AND dailyexpense_chequeno IS NOT NULL "
			. " AND reconciliation_status =1 "
			. " AND company_id=" . $company_id;
			$vendor_condition = " AND payment_type=88 ";
			$subcontractor_condition = " AND payment_type=88 AND approve_status ='Yes'  AND bank IS NOT NULL AND cheque_no IS NOT NULL ";
        }
		
		if ($payment_type == 89) {
            $condition3 = "  AND (expense_type=89 OR payment_type=89)  AND subcontractor_id IS NULL";
			$dailyreciept_cond =" AND (dailyexpense_receipt_type=89 AND "
			. " dailyexpense_chequeno IS NULL "
			. " AND reconciliation_status IS NULL "
			. " AND company_id=" . $company_id.")  "
			. " OR (dailyexpense_receipt_type=88 "
			. " AND expensehead_type=4  AND "
			. " company_id=" . $company_id.") AND "
			. " parent_status='1' AND "
			. " exp_type=72 ";
			$dailyexpense_cond =" AND (expense_type=89) AND exp_type=73 "
			. " AND parent_status='1' "
			. " AND dailyexpense_chequeno IS NULL "
			. " AND reconciliation_status IS NULL "
			. " AND company_id=" . $company_id;
			$vendor_condition = " AND payment_type=89 ";
			$subcontractor_condition = " AND payment_type=89 AND approve_status ='Yes'";
        }
		if ($payment_type == 103) {
			$tblpx = Yii::app()->db->tablePrefix;
           $user = Users::model()->findByPk(Yii::app()->user->id);
           $arrVal = explode(',', $user->company_id);
			$dailyexpense = array();
			$employee_id = '';
			$date_from = '';
			$date_to = '';
			$company_id = '';
			$daybook = array();
			$dailyexpense_refund = array();
			$dailyexpense_exp = array();
			$scpayment = array();
			$chquery1 = "";
			$chquery2 = "";
			$chcondition1 = "";
			$chcondition2 = "";
			$where ='';
			$where1 ='';
			$where2 ='';
			$where .= " AND {$tblpx}dailyexpense.date between '" . $date . "' and '" . $current_date . "'";
                $where1 .= " AND {$tblpx}expenses.date between '" . $date . "' and '" . $current_date . "'";
                $where2 .= " AND {$tblpx}subcontractor_payment.date between '" . $date . "' and '" . $current_date . "'";
			$reconcilsql = "SELECT *  FROM `jp_reconciliation` "
                . " WHERE `reconciliation_status`='0' "
                . " and reconciliation_payment='Dailyexpense Payment' and "
                . "reconciliation_table='jp_dailyexpense'";
            $reconcil_dexpense_payment_data = Yii::app()->db->createCommand($reconcilsql)->queryAll();

            $payment_cheque_array = array();
            foreach ($reconcil_dexpense_payment_data as  $data) {

                $value = "'{$data['reconciliation_chequeno']}'";
                if ($value != '') {
                    $recon_data = $value;
                    array_push($payment_cheque_array, $recon_data);
                }
            }
            $payment_cheque_array1 = implode(',', $payment_cheque_array);
            if (($payment_cheque_array1)) {
                $chquery1 = "(dailyexpense_chequeno NOT IN ( $payment_cheque_array1) OR dailyexpense_chequeno IS NULL)";
                $chcondition1 = " AND " . $chquery1 . "";
            }

            $petty_issued_id = Yii::app()->db->createCommand("SELECT `company_exp_id` 
            FROM `jp_company_expense_type` WHERE  UPPER(`name`)='PETTY CASH ISSUED'")->queryScalar();
            $dailyexpense = array();
            if($petty_issued_id){
                $dailyexpensesql = "SELECT " . $tblpx . "dailyexpense.dailyexp_id as entry_id,". $tblpx . "dailyexpense.exp_type as type,"
                 . $tblpx . "dailyexpense.dailyexpense_receipt as receipt,"
                 . $tblpx . "dailyexpense.dailyexpense_paidamount as paidamount,". $tblpx . "dailyexpense.date  FROM " . $tblpx . "dailyexpense "
                . " LEFT JOIN " . $tblpx . "users "
                . " ON " . $tblpx . "dailyexpense.employee_id= " . $tblpx . "users.userid"
                . " WHERE " . $tblpx . "dailyexpense.exp_type =73 "
                . " AND " . $tblpx . "dailyexpense.expense_type  IN('89','88') "  . $chcondition1 . "  "
                . " AND jp_dailyexpense.exp_type_id= $petty_issued_id " . $where . ""
                . " order by " . $tblpx . "dailyexpense.date asc";

            $dailyexpense = Yii::app()->db->createCommand($dailyexpensesql)->queryAll();
            }
            //petty cash refund
            $reconcil_receipt_sql = "SELECT *  FROM `jp_reconciliation` "
                . " WHERE `reconciliation_status`='0' "
                . " and reconciliation_payment='Dailyexpense Receipt' and "
                . "reconciliation_table='jp_dailyexpense'";
            $reconcil_dexpense_receipt_data = Yii::app()->db->createCommand($reconcil_receipt_sql)->queryAll();
            $receipt_cheque_array = array();
            foreach ($reconcil_dexpense_receipt_data as  $receiptdata) {
                $receiptvalue = "'{$receiptdata['reconciliation_chequeno']}'";
                if ($receiptvalue != '') {
                    $recon_receipt_data = $receiptvalue;
                    array_push($receipt_cheque_array, $recon_receipt_data);
                }
            }
            $receipt_cheque_array1 = implode(',', $receipt_cheque_array);
            if (($receipt_cheque_array1)) {
                $chquery2 = "(dailyexpense_chequeno NOT IN ( $receipt_cheque_array1) OR dailyexpense_chequeno IS NULL)";
                $chcondition2 = " AND " . $chquery2 . "";
            }

            $petty_refund_id = Yii::app()->db->createCommand("SELECT `company_exp_id` 
            FROM `jp_company_expense_type` WHERE  UPPER(`name`)='PETTY CASH REFUND'")->queryScalar();
            $dailyexpense_refund=array();
            if($petty_refund_id){
                $dexp_refund_sql = "SELECT " . $tblpx . "dailyexpense.dailyexp_id as entry_id,". $tblpx . "dailyexpense.exp_type as type,"
				. $tblpx . "dailyexpense.dailyexpense_receipt as receipt,"
				. $tblpx . "dailyexpense.dailyexpense_paidamount as paidamount,". $tblpx . "dailyexpense.date  FROM " . $tblpx . "dailyexpense "
                    . " LEFT JOIN " . $tblpx . "users "
                    . " ON " . $tblpx . "dailyexpense.employee_id= " . $tblpx . "users.userid "
                    . " WHERE " . $tblpx . "dailyexpense.exp_type =72 "
                    . " AND " . $tblpx . "dailyexpense.dailyexpense_receipt_type IN('88','89') "  . $chcondition2 . "  "
                    . "AND jp_dailyexpense.exp_type_id= $petty_refund_id  ".$where
                    . " order by " . $tblpx . "dailyexpense.date asc";
                $dailyexpense_refund = Yii::app()->db->createCommand($dexp_refund_sql)->queryAll();
				//echo "hi <pre>";print_r($dailyexpense_refund);exit;
			}

            //petty cash expense
            $daybook_sql = "SELECT " . $tblpx . "expenses.exp_id as entry_id , " . $tblpx . "expenses.type,"
			 . $tblpx . "expenses.receipt," . $tblpx . "expenses.paid as paidamount," . $tblpx . "expenses.date "
                . " FROM " . $tblpx . "expenses LEFT JOIN " . $tblpx . "users "
                . " ON " . $tblpx . "expenses.employee_id= " . $tblpx . "users.userid "
                . " LEFT JOIN " . $tblpx . "projects "
                . " ON " . $tblpx . "projects.pid = " . $tblpx . "expenses.projectid "
                . " WHERE " . $tblpx . "expenses.type = 73  "
                . " AND " . $tblpx . "expenses.expense_type=103 ".$where1
                . " GROUP BY " . $tblpx . "expenses.projectid";
            $daybook = Yii::app()->db->createCommand($daybook_sql)->queryAll();
            $dailyexpense_exp_sql = "SELECT " . $tblpx . "dailyexpense.dailyexp_id as entry_id,". $tblpx . "dailyexpense.exp_type as type,"
			. $tblpx . "dailyexpense.dailyexpense_receipt as receipt,"
			. $tblpx . "dailyexpense.dailyexpense_paidamount as paidamount,". $tblpx . "dailyexpense.date  FROM " . $tblpx . "dailyexpense "
                . " LEFT JOIN " . $tblpx . "users "
                . " ON " . $tblpx . "dailyexpense.employee_id= " . $tblpx . "users.userid "
                . " WHERE (" . $tblpx . "dailyexpense.dailyexpense_receipt_type = 103 "
                . " OR  " . $tblpx . "dailyexpense.expense_type = 103  )".$where
                . " order by " . $tblpx . "dailyexpense.date asc";
            $dailyexpense_exp = Yii::app()->db->createCommand($dailyexpense_exp_sql)->queryAll();
            $scp_sql = "SELECT  " . $tblpx . "subcontractor_payment.payment_id as entry_id,"
			. " ( IFNULL(jp_subcontractor_payment.amount, 0) + IFNULL(jp_subcontractor_payment.tax_amount, 0) ) 
		 as paidamount ," . $tblpx . "subcontractor_payment.date   FROM " . $tblpx . "subcontractor_payment LEFT JOIN " . $tblpx . "users "
                . " ON " . $tblpx . "subcontractor_payment.employee_id= " . $tblpx . "users.userid "
                . " LEFT JOIN " . $tblpx . "projects "
                . " ON " . $tblpx . "projects.pid = " . $tblpx . "subcontractor_payment.project_id "
                . " WHERE " . $tblpx . "subcontractor_payment.payment_type=103 ".$where2;
				//die($scp_sql)     ;       
            $scpayment = Yii::app()->db->createCommand($scp_sql)->queryAll();
			$expense = !empty($daybook) ? $daybook : array();
		$dailyexpense_only= !empty($dailyexpense_exp) ? $dailyexpense_exp : array();
        $dailyexpense = !empty($dailyexpense) ? $dailyexpense : array();
        $dailyexpense_refund = !empty($dailyexpense_refund) ? $dailyexpense_refund : array();
        $scPayment = !empty($scPayment) ? $scPayment : array();

        $final_array = array();

        foreach ($expense as $expenseData) {
            $expenseData['tablename'] = 'Daybook';
            if ($expenseData['entry_id'] != NULL) {
                $final_array[] = $expenseData;
            }
        }

        foreach ($dailyexpense as $dailyexpenseData) {
            $dailyexpenseData['tablename'] = 'Daily Expense';
            if ($dailyexpenseData['entry_id'] != NULL) {
                $final_array[] = $dailyexpenseData;
            }
        }
		foreach ($dailyexpense_only as $dailyexpenseDatas) {
            $dailyexpenseDatas['tablename'] = 'Daily Expense';
            if ($dailyexpenseDatas['entry_id'] != NULL) {
                $final_array[] = $dailyexpenseDatas;
            }
        }
		foreach ($dailyexpense_refund as $dailyexpenseDatas) {
            $dailyexpenseDatas['tablename'] = 'Daily Expense Refund';
            if ($dailyexpenseDatas['entry_id'] != NULL) {
                $final_array[] = $dailyexpenseDatas;
            }
        }

        foreach ($scPayment as $scPaymentData) {
            $scPaymentData['tablename'] = 'Subcontractor Payment';
            $scPaymentData['type'] = 73;
            $scPaymentData['receipt'] = NULL;
            if ($scPaymentData['entry_id'] != NULL) {
                $final_array[] = $scPaymentData;
            }
        }


        $date = array_column($final_array, 'date');
        array_multisort($date, SORT_ASC, $final_array);
        return $final_array;
        }


        $sql = "SELECT exp_id as entry_id , type,"
		. " receipt,paid as paidamount,date  "
		. " FROM `jp_expenses` WHERE   " . $condition1 . $condition2 .$condition3. "  AND company_id = " . $company_id ;
	    $expense = Yii::app()->db->createCommand($sql)->queryAll();
       //echo "hi <pre>";print_r($expense);exit;	

        $sql = "SELECT dailyexp_id as entry_id,exp_type as type,"
                . " dailyexpense_receipt as receipt,"
                . " dailyexpense_paidamount as paidamount,date "
                . " FROM `jp_dailyexpense`  WHERE   " . $condition1 .$condition. $condition2 . $dailyreciept_cond. " AND company_id = " . $company_id;
        $dailyexpense = Yii::app()->db->createCommand($sql)->queryAll();
       
		$sql = "SELECT dailyexp_id as entry_id,exp_type as type,"
                . " dailyexpense_receipt as receipt,"
                . " dailyexpense_paidamount as paidamount,date "
                . " FROM `jp_dailyexpense`  WHERE   " . $condition1 . $condition. $condition2 . $dailyexpense_cond;
        $dailyexpense_only = Yii::app()->db->createCommand($sql)->queryAll();
		
        if ($payment_type == 88) {
            $condition2 = " AND reconciliation_status = 1 AND bank = " . $bank_id;
			
        }
        //  ( only expense) add receipt and type column
        $sql = "SELECT daily_v_id as entry_id,"
                . " ( IFNULL(amount, 0) + IFNULL(tax_amount, 0) ) "
                . " as paidamount,date FROM `jp_dailyvendors` "
                . " WHERE   " . $condition1 . $condition2 .$vendor_condition . " AND company_id = " . $company_id;
        $vendorPayment = Yii::app()->db->createCommand($sql)->queryAll();
        //  ( only expense) add receipt and type column
        $sql = "SELECT payment_id as entry_id,"
                . " ( IFNULL(amount, 0) + IFNULL(tax_amount, 0) ) "
                . " as paidamount ,date FROM `jp_subcontractor_payment`"
                . " WHERE " . $condition1 .  $condition2 .$subcontractor_condition. " AND company_id = " . $company_id;
        $scPayment = Yii::app()->db->createCommand($sql)->queryAll();
		    
        $expense = !empty($expense) ? $expense : array();
		$dailyexpense_only= !empty($dailyexpense_only) ? $dailyexpense_only : array();
        $dailyexpense = !empty($dailyexpense) ? $dailyexpense : array();
        $vendorPayment = !empty($vendorPayment) ? $vendorPayment : array();
        $scPayment = !empty($scPayment) ? $scPayment : array();

        $final_array = array();

        foreach ($expense as $expenseData) {
            $expenseData['tablename'] = 'Daybook';
            if ($expenseData['entry_id'] != NULL) {
                $final_array[] = $expenseData;
            }
        }

        foreach ($dailyexpense as $dailyexpenseData) {
            $dailyexpenseData['tablename'] = 'Daily Expense';
            if ($dailyexpenseData['entry_id'] != NULL) {
                $final_array[] = $dailyexpenseData;
            }
        }
		foreach ($dailyexpense_only as $dailyexpenseDatas) {
            $dailyexpenseDatas['tablename'] = 'Daily Expense';
            if ($dailyexpenseDatas['entry_id'] != NULL) {
                $final_array[] = $dailyexpenseDatas;
            }
        }

        foreach ($vendorPayment as $vendorPaymentData) {
            $vendorPaymentData['tablename'] = 'Vendor Payment';
            $vendorPaymentData['type'] = 73;
            $vendorPaymentData['receipt'] = NULL;
            if ($vendorPaymentData['entry_id'] != NULL) {
                $final_array[] = $vendorPaymentData;
            }
        }
        foreach ($scPayment as $scPaymentData) {
            $scPaymentData['tablename'] = 'Subcontractor Payment';
            $scPaymentData['type'] = 73;
            $scPaymentData['receipt'] = NULL;
            if ($scPaymentData['entry_id'] != NULL) {
                $final_array[] = $scPaymentData;
            }
        }
        $date = array_column($final_array, 'date');
        array_multisort($date, SORT_ASC, $final_array);
        return $final_array;
 
		
    }

    public function checkDeleteBalance($data) {
        if ($data['exp_type'] == 72) {
            $exp_id = $data['dailyexp_id'];
            $date = date('Y-m-d', strtotime($data['date']));
            $paymentArray = $this->getPaymentArray($date, $exp_id, $status = 0, $data['expense_type'], $data['company_id'], $data['bank_id']); //get the enetries after the selected entry
            //find the closing balance on the selected entry date
            $chekBalance = "";
            $invoice_amount = 0;
            $expense_amount = 0;
            $payment_type = 0;
            $available_amount = "";
            $company_id = $data['company_id'];
            if ($data['expense_type'] == '89' || $data['expense_type'] == '88' || $data['expense_type'] == '103') {
                $payment_type = $data['expense_type'];
            }

            if (!empty($company_id)) {
                $invoice_amount = BalanceHelper::cashInvoice(
                                $payment_type,
                                $company_id,
                                $data['user_id'],
                                $data['bank_id'],
                                $date
                );
                $expense_amount = BalanceHelper::cashExpense(
                                $payment_type,
                                $company_id,
                                $data['user_id'],
                                $data['dailyexp_id'],
                                $data['bank_id'],
                                $date
                );

                if (is_numeric($invoice_amount) && is_numeric($expense_amount)) {
                    $available_amount = $invoice_amount - $expense_amount; //default closing balance
                }

                // get the  available amount just before the deleted entry
                $chekBalanceArray = $this->getPaymentArray($date, $exp_id, $status = 1, $payment_type, $company_id, $data['bank_id']);
                $chekBalance = $available_amount;

                foreach ($chekBalanceArray as $data) {
                    if ($data['type'] == 73) { //expense
                        $chekBalance += $data['paidamount'];
                    } else {
                        $chekBalance -= $data['receipt'];
                    }
                }

                $closingBalance = $chekBalance;
                $checkArray = array();
                foreach ($paymentArray as $data) {
                    if ($data['type'] == 73) { //expense
                        $closingBalance -= $data['paidamount'];
                    } else {
                        $closingBalance += $data['receipt'];
                    }
                    array_push($checkArray, $closingBalance);
                    if ($closingBalance < 0) {
                        return "Can't delete ! Insufficient balance on " . $data['date'] . " in " . $data['tablename'] . " - ID: #" . $data['entry_id'];
                    }
                }
            }
            return '1';
        } else {
            return '1';
        }
    }

}
