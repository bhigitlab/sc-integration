<link rel="stylesheet" type="text/css" href="uploads/new_template/css/newstyle.css" />
	 
<div class="container">

<header class="headerinv">
	<table class="invoiceheader">
		<tr>
		<td><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/logo.png" style="height:100px;padding:5px;"></td>
		<td class="details" style="width:300px;"><b> <?php echo $details; ?></b></td>
		</tr>
	</table>
	<br>
<header>	
	
	<h2>PERFORMA INVOICE</h2>
	<br/>
	
	
	<table style="float:left" border="1">
	<tr>
		<td><label>BILL TO : </label><?php echo $billto; ?></td>
		</tr>
	
	</table>
	
	<br/><br/>

	<table style="float:right" border="1">
	<tr>
		<td>
		<label>DATE : </label> <?php echo $date; ?>
		</td>
		</tr>
	<tr>
		<td>
		<label>INVOICE NO : </label> <?php echo $invoiceno; ?>
		</td>
	</tr>
	</table>
	
	<br/><br/>
	
	
	<table border="1">
		<tr>
			<td><label>PROJECT NAME : </label> <?php echo $project; ?>
		</tr>
	</table>

	<br/><br/>
	
	<table border="1" style="width:100%;">
			<thead>
				<tr>
					<th>Sl.No</th>
					<th>Description</th>
					<th>Quantity</th>
					<th>Unit</th>
					<th>Rate</th>
					<th>Amount</th>
				</tr>
			</thead>
			<tbody class="addrow">
				<?php 
				 for ($i=0; $i < count($sl_No); $i++) {
				 ?>
				<tr>
					<td><?php echo $sl_No[$i]; ?></td>
					<td><?php echo $description[$i]; ?></td>
					<td><?php echo $quantity[$i]; ?></td>
					<td><?php echo $unit[$i]; ?></td>
					<td><?php echo $rate[$i]; ?></td>
					<td><?php echo $amount[$i]; ?></td>
				</tr>
				<?php
			   }
				?>
			</tbody>
	</table>
	<br/><br/>
	
	<table border="1" style="width:100%;">
		<tr>
			<td>SUB TOTAL : <?php echo $subtot; ?></td>
			<td>FEES :  <?php echo $fees; ?></td>
			<td>GRAND TOTAL : <?php echo $grand; ?><td>
		</tr>
	</table>
	
	<h4>Autherised Signatory,</h4>
	<h4>For <?php echo  Yii::app()->name;?></h4>
	<p>Please make cash payment.</p>
	<br/><br/>
<footer class="inv_footer"><?php echo  Yii::app()->params['website_name'];?></footer>
</div>


