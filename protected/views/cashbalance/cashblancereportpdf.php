<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/pdfstyle.css">

<?php
    $company_address = Company::model()->findBypk(Yii::app()->user->company_id);
?>
<br>
    
<div class="container-fluid" style="margin:0px 30px;">
<h4>CASH BALANCE REPORT</h4> 

            <table class="table table-bordered table-striped" border="1">

                <thead>
                    <tr>
                        <th>Sl No.</th>
                        <th>Company</th>
                        <th>Cash Balance Type</th>
                        <th>Bank</th>
                        <th>Date</th>
                        <th>Opening Balance</th>
                        <th>Deposit</th>
                        <th>Withdrawal</th>
                        <th>Closing Balance</th>
                    </tr>
                </thead>
                 <tbody>
					 <?php
					  $tblpx = Yii::app()->db->tablePrefix;
					  $current_date = date("Y-m-d");
                                          $opening_date = date('Y-m-d', strtotime('-1 day', strtotime($current_date)));
					  foreach($cash_blance as $key=> $value){

						if($value['bank_id'] !=''){
                                                    $opening_bank = " AND reconciliation_date <= '" . $opening_date ."'";
						// deposit

						$daybook=  Yii::app()->db->createCommand("SELECT SUM(receipt) as total_amount FROM {$tblpx}expenses WHERE reconciliation_status = 1 AND reconciliation_date BETWEEN '".$value['cashbalance_date']."' AND '".$current_date."' AND payment_type=88 AND bank_id=".$value['bank_id']." AND type=72 AND company_id=".$value['company_id']."")->queryRow();
						$dailyexpense=  Yii::app()->db->createCommand("SELECT SUM(dailyexpense_receipt) as total_amount FROM {$tblpx}dailyexpense WHERE reconciliation_status = 1 AND reconciliation_date BETWEEN '".$value['cashbalance_date']."' AND '".$current_date."' AND dailyexpense_receipt_type=88 AND bank_id=".$value['bank_id']." AND exp_type=72 AND company_id=".$value['company_id']."")->queryRow();
						$daybook_check = $daybook['total_amount'];
                                                $dailyexpense_check = $dailyexpense['total_amount'];
						//$deposit = $daybook_check+$dailyexpense_check+$value['cashbalance_deposit'];
						$deposit = $daybook_check+$dailyexpense_check;


						//withdrawal
						$daybook_with=  Yii::app()->db->createCommand("SELECT SUM(paid) as total_amount FROM {$tblpx}expenses WHERE reconciliation_status = 1 AND reconciliation_date BETWEEN '".$value['cashbalance_date']."' AND '".$current_date."' AND expense_type=88 AND bank_id=".$value['bank_id']." AND type=73 AND company_id=".$value['company_id']."")->queryRow();
						$dailyexpense_with=  Yii::app()->db->createCommand("SELECT SUM(dailyexpense_paidamount) as total_amount FROM {$tblpx}dailyexpense WHERE reconciliation_status = 1 AND reconciliation_date BETWEEN '".$value['cashbalance_date']."' AND '".$current_date."' AND expense_type=88 AND bank_id=".$value['bank_id']." AND exp_type=73 AND company_id=".$value['company_id']."")->queryRow();
						$daybook_with_amount = $daybook_with['total_amount'];
                                                $dailyexpense_with_amount = $dailyexpense_with['total_amount'];

						$dailyvendors=  Yii::app()->db->createCommand("SELECT SUM( IFNULL(amount, 0) + IFNULL(tax_amount, 0) ) as total_amount FROM {$tblpx}dailyvendors WHERE reconciliation_status = 1 AND reconciliation_date BETWEEN '".$value['cashbalance_date']."' AND '".$current_date."' AND payment_type=88  AND bank=".$value['bank_id']." AND company_id=".$value['company_id']."")->queryRow();
						$subcontractor=  Yii::app()->db->createCommand("SELECT SUM( IFNULL(amount, 0) + IFNULL(tax_amount, 0) ) as total_amount FROM {$tblpx}subcontractor_payment WHERE reconciliation_status = 1 AND reconciliation_date BETWEEN '".$value['cashbalance_date']."' AND '".$current_date."' AND payment_type=88  AND bank=".$value['bank_id']." AND company_id=".$value['company_id']."")->queryRow();

                                                $dailyvendors_check = $dailyvendors['total_amount'];
                                                $subcontractor_check = $subcontractor['total_amount'];

                                                //$withdrawal = $dailyvendors_check+$subcontractor_check+$daybook_with_amount+$dailyexpense_with_amount+$value['cashbalance_withdrawal'];
                                                //$withdrawal = $dailyvendors_check+$subcontractor_check+$daybook_with_amount+$dailyexpense_with_amount;
                                                $withdrawal = $dailyvendors_check+$daybook_with_amount+$dailyexpense_with_amount;

                                                // opening balance calculation

                                                // deposit
                                                $daybook_opening=  Yii::app()->db->createCommand("SELECT SUM(receipt) as total_amount FROM {$tblpx}expenses WHERE reconciliation_status = 1 ".$opening_bank." AND payment_type=88 AND bank_id=".$value['bank_id']." AND type=72 AND company_id=".$value['company_id']."")->queryRow();
						$dailyexpense_opening=  Yii::app()->db->createCommand("SELECT SUM(dailyexpense_receipt) as total_amount FROM {$tblpx}dailyexpense WHERE reconciliation_status = 1 ".$opening_bank." AND dailyexpense_receipt_type=88 AND bank_id=".$value['bank_id']." AND exp_type=72 AND company_id=".$value['company_id']."")->queryRow();
						$daybook_check_opening = $daybook_opening['total_amount'];
                                                $dailyexpense_check_opening = $dailyexpense_opening['total_amount'];
						$deposit_opening = $daybook_check_opening+$dailyexpense_check_opening;

//                                                if($deposit_opening ==0){
//                                                     $deposit_opening = $value['cashbalance_opening_balance'];
//                                                 }

                                                //withdrawal
						$daybook_with_opening=  Yii::app()->db->createCommand("SELECT SUM(paid) as total_amount FROM {$tblpx}expenses WHERE reconciliation_status = 1 ".$opening_bank." AND expense_type=88 AND bank_id=".$value['bank_id']." AND type=73 AND company_id=".$value['company_id']."")->queryRow();
						$dailyexpense_with_opening=  Yii::app()->db->createCommand("SELECT SUM(dailyexpense_paidamount) as total_amount FROM {$tblpx}dailyexpense WHERE reconciliation_status = 1 ".$opening_bank." AND expense_type=88 AND bank_id=".$value['bank_id']." AND exp_type=73 AND company_id=".$value['company_id']."")->queryRow();
						$daybook_with_amount_opening = $daybook_with_opening['total_amount'];
                                                $dailyexpense_with_amount_opening = $dailyexpense_with_opening['total_amount'];

						$dailyvendors_opening=  Yii::app()->db->createCommand("SELECT SUM( IFNULL(amount, 0) + IFNULL(tax_amount, 0) ) as total_amount FROM {$tblpx}dailyvendors WHERE reconciliation_status = 1 ".$opening_bank." AND payment_type=88  AND bank=".$value['bank_id']." AND company_id=".$value['company_id']."")->queryRow();

                                                $dailyvendors_check_opening = $dailyvendors_opening['total_amount'];

                                                $withdrawal_opening = $dailyvendors_check_opening+$daybook_with_amount_opening+$dailyexpense_with_amount_opening;

                                                $opening_balance = ($deposit_opening+$value['cashbalance_opening_balance'])-$withdrawal_opening;
					   } else{
						 $opening_hand = " AND date <= '" . $opening_date . "'";
						 //deposit
						 $daybook_cah=  Yii::app()->db->createCommand("SELECT SUM(receipt) as total_amount FROM {$tblpx}expenses WHERE date BETWEEN '".$value['cashbalance_date']."' AND '".$current_date."' AND payment_type=89 AND type=72 AND company_id=".$value['company_id']."")->queryRow();
						 $daybook_cash = $daybook_cah['total_amount'];
						 //$dailyexpense_cah=  Yii::app()->db->createCommand("SELECT SUM(dailyexpense_receipt) as total_amount FROM {$tblpx}dailyexpense WHERE date BETWEEN '".$value['cashbalance_date']."' AND '".$current_date."' AND (dailyexpense_receipt_type=89 OR dailyexpense_receipt_type=103) AND exp_type=72 AND company_id=".$value['company_id']."")->queryRow();
                                                 $dailyexpense_cah=  Yii::app()->db->createCommand("SELECT SUM(dailyexpense_receipt) as total_amount FROM {$tblpx}dailyexpense WHERE date BETWEEN '".$value['cashbalance_date']."' AND '".$current_date."' AND (dailyexpense_receipt_type=89) AND exp_type=72 AND company_id=".$value['company_id']."")->queryRow();
						 $dailyexpense_cash = $dailyexpense_cah['total_amount'];

						 //$deposit = $daybook_cash+ $dailyexpense_cash+$value['cashbalance_deposit'];
						 $deposit = $daybook_cash+ $dailyexpense_cash;
						 // withdrawal

						 $daybook_with=  Yii::app()->db->createCommand("SELECT SUM(paid) as total_amount FROM {$tblpx}expenses WHERE date BETWEEN '".$value['cashbalance_date']."' AND '".$current_date."' AND (expense_type=89 OR expense_type=103) AND type=73 AND company_id=".$value['company_id']."")->queryRow();
						 $daybook_with_amount = $daybook_with['total_amount'];
						 //$dailyexpense_with=  Yii::app()->db->createCommand("SELECT SUM(dailyexpense_paidamount) as total_amount FROM {$tblpx}dailyexpense WHERE date BETWEEN '".$value['cashbalance_date']."' AND '".$current_date."' AND (expense_type=89 OR expense_type=103) AND exp_type=73 AND company_id=".$value['company_id']."")->queryRow();
                                                 $dailyexpense_with=  Yii::app()->db->createCommand("SELECT SUM(dailyexpense_paidamount) as total_amount FROM {$tblpx}dailyexpense WHERE date BETWEEN '".$value['cashbalance_date']."' AND '".$current_date."' AND (expense_type=89) AND exp_type=73 AND company_id=".$value['company_id']."")->queryRow();
						 $dailyexpense_with_amount = $dailyexpense_with['total_amount'];


						 $dailyvendors_cah=  Yii::app()->db->createCommand("SELECT SUM( IFNULL(amount, 0) + IFNULL(tax_amount, 0) ) as total_amount FROM {$tblpx}dailyvendors WHERE date BETWEEN '".$value['cashbalance_date']."' AND '".$current_date."' AND payment_type=89 AND company_id=".$value['company_id']."")->queryRow();
						 $dailyvendors_cash = $dailyvendors_cah['total_amount'];
						 $subcontractor_cah=  Yii::app()->db->createCommand("SELECT SUM( IFNULL(amount, 0) + IFNULL(tax_amount, 0) ) as total_amount FROM {$tblpx}subcontractor_payment WHERE date BETWEEN '".$value['cashbalance_date']."' AND '".$current_date."' AND payment_type=89 AND company_id=".$value['company_id']."")->queryRow();
						 $subcontractor_cash = $subcontractor_cah['total_amount'];

						 //$withdrawal = $dailyvendors_cash+$subcontractor_cash+$daybook_with_amount+$dailyexpense_with_amount+$value['cashbalance_withdrawal'];
						 //$withdrawal = $dailyvendors_cash+$subcontractor_cash+$daybook_with_amount+$dailyexpense_with_amount;
						 $withdrawal = $dailyvendors_cash+$daybook_with_amount+$dailyexpense_with_amount;

                                                 // opening balance calculation

                                                 // deposit
                                                 $daybook_cah_opening=  Yii::app()->db->createCommand("SELECT SUM(receipt) as total_amount FROM {$tblpx}expenses WHERE payment_type=89 AND type=72 ".$opening_hand." AND company_id=".$value['company_id']."")->queryRow();
						 $daybook_cash_opening = $daybook_cah_opening['total_amount'];
						 //$dailyexpense_cah_opening=  Yii::app()->db->createCommand("SELECT SUM(dailyexpense_receipt) as total_amount FROM {$tblpx}dailyexpense WHERE (dailyexpense_receipt_type=89 OR dailyexpense_receipt_type=103) AND exp_type=72 ".$opening_hand." AND company_id=".$value['company_id']."")->queryRow();
                                                 $dailyexpense_cah_opening=  Yii::app()->db->createCommand("SELECT SUM(dailyexpense_receipt) as total_amount FROM {$tblpx}dailyexpense WHERE (dailyexpense_receipt_type=89) AND exp_type=72 ".$opening_hand." AND company_id=".$value['company_id']."")->queryRow();
						 $dailyexpense_cash_opening = $dailyexpense_cah_opening['total_amount'];
                                                 $deposit_opening = $daybook_cash_opening+ $dailyexpense_cash_opening;

//                                                 if($deposit_opening ==0){
//                                                     $deposit_opening = $value['cashbalance_opening_balance'];
//                                                 }

                                                 // withdrawal

						 $daybook_with_opening=  Yii::app()->db->createCommand("SELECT SUM(paid) as total_amount FROM {$tblpx}expenses WHERE (expense_type=89 OR expense_type=103) AND type=73 ".$opening_hand." AND company_id=".$value['company_id']."")->queryRow();
						 $daybook_with_amount_opening = $daybook_with_opening['total_amount'];
						 //$dailyexpense_with_opening=  Yii::app()->db->createCommand("SELECT SUM(dailyexpense_paidamount) as total_amount FROM {$tblpx}dailyexpense WHERE (expense_type=89 OR expense_type=103) AND exp_type=73 ".$opening_hand." AND company_id=".$value['company_id']."")->queryRow();
                                                 $dailyexpense_with_opening=  Yii::app()->db->createCommand("SELECT SUM(dailyexpense_paidamount) as total_amount FROM {$tblpx}dailyexpense WHERE (expense_type=89) AND exp_type=73 ".$opening_hand." AND company_id=".$value['company_id']."")->queryRow();
						 $dailyexpense_with_amount_opening = $dailyexpense_with_opening['total_amount'];


						 $dailyvendors_cah_opening=  Yii::app()->db->createCommand("SELECT SUM( IFNULL(amount, 0) + IFNULL(tax_amount, 0) ) as total_amount FROM {$tblpx}dailyvendors WHERE payment_type=89 ".$opening_hand." AND company_id=".$value['company_id']."")->queryRow();
						 $dailyvendors_cash_opening = $dailyvendors_cah_opening['total_amount'];
						 $withdrawal_opening = $dailyvendors_cash_opening+$daybook_with_amount_opening+$dailyexpense_with_amount_opening;

                                                 $opening_balance = ($deposit_opening+$value['cashbalance_opening_balance'])-$withdrawal_opening;
					   }


					 ?>

					 <tr>
                         <td><?php echo $key+1; ?></td>
                         <td>
                             <?php
                 $company = Company::model()->findByPk($value['company_id']);
		 echo $company['name']; ?>
                         </td>
                         <td><?php echo $value['caption']; ?></td>
                         <td><?php echo $value['bank_name'];?></td>
                         <td><?php echo $value['cashbalance_date'];?></td>
                         <td align="right"><?php echo Controller::money_format_inr($opening_balance,2);?></td>
                         <td align="right"><?php echo Controller::money_format_inr($deposit,2);?></td>
                         <td align="right"><?php echo Controller::money_format_inr($withdrawal,2);?></td>
                         <td align="right"><?php echo number_format(($opening_balance+$deposit)-$withdrawal,2);?></td>
                     </tr>
                     <?php } ?>
                 </tbody>

            </table>


            <br/>

			<?php
			if($date_from !='' || $date_to !='') {
				$heading = '';
				if (!empty($date_from) && !empty($date_to)) {
					$heading    .= date('d-M-Y', strtotime($date_from)).' to '.date('d-M-Y', strtotime($date_to));
				} else {
					if (!empty($date_from)) {
						$heading    .= date('d-M-Y', strtotime($date_from));
					}
					if (!empty($date_to)) {
						$heading    .= date('d-M-Y', strtotime($date_to));
					}
				}

				$cash_blance = Yii::app()->db->createCommand("SELECT {$tblpx}status.caption,{$tblpx}cashbalance.cashbalance_type,{$tblpx}cashbalance.bank_id,{$tblpx}bank.bank_name,{$tblpx}cashbalance.cashbalance_opening_balance,{$tblpx}cashbalance.cashbalance_deposit,{$tblpx}cashbalance.cashbalance_withdrawal,{$tblpx}cashbalance.cashbalance_closing_balance,{$tblpx}cashbalance.cashbalance_date FROM {$tblpx}cashbalance LEFT JOIN {$tblpx}bank ON {$tblpx}cashbalance.bank_id={$tblpx}bank.bank_id LEFT JOIN {$tblpx}status ON {$tblpx}cashbalance.cashbalance_type={$tblpx}status.sid WHERE {$tblpx}cashbalance.company_id=".Yii::app()->user->company_id." ORDER BY bank_id ASC")->queryAll();
				if(!empty($cash_blance)){
			?>
			<h4>Cash Balance Report : <?php echo $heading; ?></h4>
			 <table class="table table-bordered table-striped" border="1">

                <thead>
                    <tr>
                        <th>Sl No.</th>
                        <th>Cash Balance Type</th>
                        <th>Bank</th>
                        <th>Date</th>
                        <th>Opening Balance</th>
                        <th>Deposit</th>
                        <th>Withdrawal</th>
                        <th>Closing Balance</th>
                    </tr>
                </thead>
                 <tbody>
					 <?php
					 $tblpx = Yii::app()->db->tablePrefix;

					  foreach($cash_blance as $key=> $value){

						$tblpx = Yii::app()->db->tablePrefix;
						$where_bank = '';
						$where_hand = '';
						$where_main = '';
						if (!empty($date_from) && !empty($date_to)) {
							$where_main .= " AND {$tblpx}cashbalance.cashbalance_date between '" . $date_from ."' and'" . $date_to . "'";
							$where_bank .= " AND reconciliation_date between '" . $date_from ."' and'" . $date_to . "'";
							$where_hand .= " AND date between '" . $date_from ."' and'" . $date_to . "'";
						} else {
							if (!empty($_POST['date_from'])) {
								$date_from = date('Y-m-d', strtotime($_POST['date_from']));
								$where_main .= " AND {$tblpx}cashbalance.cashbalance_date >= '" . $date_from ."'";
								$where_bank .= " AND reconciliation_date between '" . $date_from ."' and'" . $current_date . "'";
								$where_hand .= " AND date between '" . $date_from ."' and'" . $current_date . "'";
							}
							if (!empty($_POST['date_to'])) {
								$date_to = date('Y-m-d', strtotime($_POST['date_to']));
								$where_main .= " AND {$tblpx}cashbalance.cashbalance_date <= '" . $date_to . "'";
								$where_bank .= " AND reconciliation_date between '" . $value['cashbalance_date'] ."' and'" . $date_to . "'";
								$where_hand .= " AND date between '" . $value['cashbalance_date'] ."' and'" . $date_to . "'";
							}
						}

						if($value['bank_id'] !=''){
						// deposit

						$daybook=  Yii::app()->db->createCommand("SELECT SUM(receipt) as total_amount FROM {$tblpx}expenses WHERE reconciliation_status = 1 ".$where_bank." AND payment_type=88 AND bank_id=".$value['bank_id']." AND type=72 AND company_id=".Yii::app()->user->company_id."")->queryRow();
						$dailyexpense=  Yii::app()->db->createCommand("SELECT SUM(dailyexpense_receipt) as total_amount FROM {$tblpx}dailyexpense WHERE reconciliation_status = 1 ".$where_bank." AND dailyexpense_receipt_type=88 AND bank_id=".$value['bank_id']." AND exp_type=72 AND company_id=".Yii::app()->user->company_id."")->queryRow();
						$daybook_check = $daybook['total_amount'];
					    $dailyexpense_check = $dailyexpense['total_amount'];
						//$deposit = $daybook_check+$dailyexpense_check+$value['cashbalance_deposit'];
						$deposit = $daybook_check+$dailyexpense_check;


						//withdrawal
						$daybook_with=  Yii::app()->db->createCommand("SELECT SUM(paid) as total_amount FROM {$tblpx}expenses WHERE reconciliation_status = 1 ".$where_bank." AND expense_type=88 AND bank_id=".$value['bank_id']." AND type=73 AND company_id=".Yii::app()->user->company_id."")->queryRow();
						$dailyexpense_with=  Yii::app()->db->createCommand("SELECT SUM(dailyexpense_paidamount) as total_amount FROM {$tblpx}dailyexpense WHERE reconciliation_status = 1 ".$where_bank." AND expense_type=88 AND bank_id=".$value['bank_id']." AND exp_type=73 AND company_id=".Yii::app()->user->company_id."")->queryRow();
						$daybook_with_amount = $daybook_with['total_amount'];
					    $dailyexpense_with_amount = $dailyexpense_with['total_amount'];

						$dailyvendors=  Yii::app()->db->createCommand("SELECT SUM( IFNULL(amount, 0) + IFNULL(tax_amount, 0) ) as total_amount FROM {$tblpx}dailyvendors WHERE reconciliation_status = 1 ".$where_bank." AND payment_type=88  AND bank=".$value['bank_id']." AND company_id=".Yii::app()->user->company_id."")->queryRow();
						$subcontractor=  Yii::app()->db->createCommand("SELECT SUM( IFNULL(amount, 0) + IFNULL(tax_amount, 0) ) as total_amount FROM {$tblpx}subcontractor_payment WHERE reconciliation_status = 1 ".$where_bank." AND payment_type=88  AND bank=".$value['bank_id']." AND company_id=".Yii::app()->user->company_id."")->queryRow();

					    $dailyvendors_check = $dailyvendors['total_amount'];
					    $subcontractor_check = $subcontractor['total_amount'];

					    //$withdrawal = $dailyvendors_check+$subcontractor_check+$daybook_with_amount+$dailyexpense_with_amount+$value['cashbalance_withdrawal'];
					    //$withdrawal = $dailyvendors_check+$subcontractor_check+$daybook_with_amount+$dailyexpense_with_amount;
					    $withdrawal = $dailyvendors_check+$daybook_with_amount+$dailyexpense_with_amount;
					   } else{

						 //deposit
						 $daybook_cah=  Yii::app()->db->createCommand("SELECT SUM(receipt) as total_amount FROM {$tblpx}expenses WHERE payment_type=89 AND type=72 ".$where_hand." AND company_id=".Yii::app()->user->company_id."")->queryRow();
						 $daybook_cash = $daybook_cah['total_amount'];
						 //$dailyexpense_cah=  Yii::app()->db->createCommand("SELECT SUM(dailyexpense_receipt) as total_amount FROM {$tblpx}dailyexpense WHERE (dailyexpense_receipt_type=89 OR dailyexpense_receipt_type=103) AND exp_type=72 ".$where_hand." AND company_id=".Yii::app()->user->company_id."")->queryRow();
                                                 $dailyexpense_cah=  Yii::app()->db->createCommand("SELECT SUM(dailyexpense_receipt) as total_amount FROM {$tblpx}dailyexpense WHERE (dailyexpense_receipt_type=89) AND exp_type=72 ".$where_hand." AND company_id=".Yii::app()->user->company_id."")->queryRow();
						 $dailyexpense_cash = $dailyexpense_cah['total_amount'];

						 //$deposit = $daybook_cash+ $dailyexpense_cash+$value['cashbalance_deposit'];
						 $deposit = $daybook_cash+ $dailyexpense_cash;
						 // withdrawal

						 $daybook_with=  Yii::app()->db->createCommand("SELECT SUM(paid) as total_amount FROM {$tblpx}expenses WHERE expense_type=89 AND type=73 ".$where_hand." AND company_id=".Yii::app()->user->company_id."")->queryRow();
						 $daybook_with_amount = $daybook_with['total_amount'];
						 //$dailyexpense_with=  Yii::app()->db->createCommand("SELECT SUM(dailyexpense_paidamount) as total_amount FROM {$tblpx}dailyexpense WHERE (expense_type=89 OR expense_type=103) AND exp_type=73 ".$where_hand." AND company_id=".Yii::app()->user->company_id."")->queryRow();
                                                 $dailyexpense_with=  Yii::app()->db->createCommand("SELECT SUM(dailyexpense_paidamount) as total_amount FROM {$tblpx}dailyexpense WHERE (expense_type=89) AND exp_type=73 ".$where_hand." AND company_id=".Yii::app()->user->company_id."")->queryRow();
						 $dailyexpense_with_amount = $dailyexpense_with['total_amount'];


						 $dailyvendors_cah=  Yii::app()->db->createCommand("SELECT SUM( IFNULL(amount, 0) + IFNULL(tax_amount, 0) ) as total_amount FROM {$tblpx}dailyvendors WHERE payment_type=89 ".$where_hand." AND company_id=".Yii::app()->user->company_id."")->queryRow();
						 $dailyvendors_cash = $dailyvendors_cah['total_amount'];
						 $subcontractor_cah=  Yii::app()->db->createCommand("SELECT SUM( IFNULL(amount, 0) + IFNULL(tax_amount, 0) ) as total_amount FROM {$tblpx}subcontractor_payment WHERE payment_type=89 ".$where_hand." AND company_id=".Yii::app()->user->company_id."")->queryRow();
						 $subcontractor_cash = $subcontractor_cah['total_amount'];

						 //$withdrawal = $dailyvendors_cash+$subcontractor_cash+$daybook_with_amount+$dailyexpense_with_amount+$value['cashbalance_withdrawal'];
						 //$withdrawal = $dailyvendors_cash+$subcontractor_cash+$daybook_with_amount+$dailyexpense_with_amount;
						 $withdrawal = $dailyvendors_cash+$daybook_with_amount+$dailyexpense_with_amount;
					   }


					 ?>

					 <tr>
                         <td><?php echo $key+1; ?></td>
                         <td><?php echo $value['caption']; ?></td>
                         <td><?php echo $value['bank_name'];?></td>
                         <td><?php echo $value['cashbalance_date'];?></td>
                         <td align="right"><?php echo Controller::money_format_inr($value['cashbalance_opening_balance'],2,1);?></td>
                         <td align="right"><?php echo Controller::money_format_inr($deposit,2,1);?></td>
                         <td align="right"><?php echo Controller::money_format_inr($withdrawal,2,1);?></td>
                         <td align="right"><?php echo number_format(($value['cashbalance_opening_balance']+$deposit)-$withdrawal,2);?></td>
                     </tr>
                     <?php } ?>
                 </tbody>

            </table>

			<?php
			} else {

			echo "No data found";
			}}
			?>


        </div>
