ALTER TABLE `jp_purchase_items` ADD `estimation_approval_type` INT(11) NULL COMMENT '1=>rate,2=>quantity' AFTER `sgst_percentage`;
ALTER TABLE `jp_purchase_items` CHANGE `estimation_approval_type` `estimation_approval_type` INT(11) NULL DEFAULT NULL COMMENT '1=>rate,2=>quantity,3=>both';
