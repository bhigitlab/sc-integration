<?php

class WarehouseCategoryController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';
    //private $access_rules = array();

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array(
                'allow',  // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view', 'create', 'update', 'Newlist'),
                'users' => array('@'),
            ),
            array(
                'allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array(
                    'create', 'MergingPurchasecatAndWarehouseCat', 'update', 'Newlist', 'createspecification', 'checkspecification', 'checkcategory', 'test', 'getItemForEdit',
                    'getSpecificationForEdit', 'updatespecification', 'exportItemToCSV', 'purchasereport', 'savepurchasereportpdf', 'ExportPurchaseReportCSV', 'img_delete'
                ),
                'users' => array('@'),
                'expression' => 'Yii::app()->user->role <= 2 || Yii::app()->user->role == 5',
            ),
            array(
                'allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('@'),
                'expression' => 'Yii::app()->user->role <= 2',
            ),
            array(
                'deny',  // deny all users
                'users' => array('*'),
            ),
        );

        /*$this->access_rules = array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view', 'create','update', 'Newlist','createspecification'),
				'users'=>array('*'),
			),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update', 'Newlist','createspecification'),
				'users'=>array('@'),
			),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
            array('deny',  // deny all users
				'users'=>array('*'),
			),
			
			   
            
            
        );
		
		 return $this->access_rules;   */
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */

    public function actionCreate()
    {


        if (isset($_POST['WarehouseCategory'])) {
            if ($_POST['category_id'] == '' || $_POST['category_id'] == 0) {
                $model = new WarehouseCategory;
                $this->performAjaxValidation($model);
                $model->unsetAttributes();
                $model->parent_id = (($_POST['WarehouseCategory']['parent_id'] == '') ? NULL : $_POST['WarehouseCategory']['parent_id']);
                $model->category_name      = $_POST['WarehouseCategory']['category_name'];
                $model->spec_flag = 'N';
                $model->type = 'C';
                $model->created_by = Yii::app()->user->id;
                $model->created_date =  date('Y-m-d');
                if ($model->save()) {
                    Yii::app()->user->setFlash('success', "Added Successfully");
                    $this->redirect(array('WarehouseCategory/newList'));
                }
            } else {
                $id = $_POST['category_id'];
                $model = $this->loadModel($id);
                $this->performAjaxValidation($model);
                //$model->unsetAttributes();
                $model->parent_id = (($_POST['WarehouseCategory']['parent_id'] == '') ? NULL : $_POST['WarehouseCategory']['parent_id']);
                $model->category_name      = $_POST['WarehouseCategory']['category_name'];
                if ($model->save()) {
                    Yii::app()->user->setFlash('success', "Updated Successfully");
                    $this->redirect(array('WarehouseCategory/newList'));
                }
            }
        }
    }


    public function actionCreatespecification()
    {
        $images_path = realpath(Yii::app()->basePath . '/../uploads/warehouse_category');
        $images_path2 = realpath(Yii::app()->basePath . '/../uploads/warehouse_category/thumbnail');
        if (isset($_POST['WarehouseCategory'])) {
            if ($_POST['specification_id'] == '' || $_POST['specification_id'] == 0) {
                $model = new WarehouseCategory;
                $this->performAjaxValidation1($model);
                $model->unsetAttributes();
                if ($_FILES['WarehouseCategory']['name']['filename']) {
                    $filename                   = CUploadedFile::getInstance($model, 'filename');
                    $newfilename                = rand(1000, 9999) . time();
                    $newfilename                = md5($newfilename); //optional
                    $extension                  = $filename->getExtensionName();
                    $model->filename = $newfilename . "." . $extension;
                }
                $model->parent_id = $_POST['WarehouseCategory']['parent_id'];
                $model->specification = $_POST['WarehouseCategory']['specification'];
                $model->brand_id = $_POST['WarehouseCategory']['brand_id'];
                $model->unit = $_POST['WarehouseCategory']['unit'];
                $model->spec_flag = 'Y';
                $model->type = 'S';
                $model->created_by = Yii::app()->user->id;
                $model->created_date =  date('Y-m-d');
                $model->company_id   = Yii::app()->user->company_id;
                $model->specification_type = $_POST['WarehouseCategory']['specification_type'];
                if ($_POST['WarehouseCategory']['specification_type'] == 'A') {
                    $model->dieno = $_POST['WarehouseCategory']['dieno'];
                    $model->specification = $model->dieno;
                }
                if ($model->save()) {
                    if ($_FILES['WarehouseCategory']['name']['filename']) {
                        $uploadfile    = $filename->saveAs($images_path . '/' . $newfilename . "." . $extension);
                        $image = new EasyImage($images_path . '/' . $newfilename . "." . $extension);
                        $image->resize(60, 60);
                        $image->save($images_path2 . '/' . $newfilename . "." . $extension);
                    }
                    Yii::app()->user->setFlash('success', "Added Successfully");
                    $this->redirect(array('WarehouseCategory/newList'));
                }
            } else {
                $id = $_POST['specification_id'];
                $model = $this->loadModel($id);
                $this->performAjaxValidation1($model);
                //$model->unsetAttributes();
                if ($_FILES['WarehouseCategory']['name']['filename'] != '') {
                    $filename                   = CUploadedFile::getInstance($model, 'filename');
                    $newfilename                = rand(1000, 9999) . time();
                    $newfilename                = md5($newfilename); //optional
                    $extension                  = $filename->getExtensionName();
                    $model->filename = $newfilename . "." . $extension;
                }
                $model->parent_id = $_POST['WarehouseCategory']['parent_id'];
                $model->specification = $_POST['WarehouseCategory']['specification'];
                $model->brand_id = $_POST['WarehouseCategory']['brand_id'];
                $model->unit = $_POST['WarehouseCategory']['unit'];
                $model->spec_flag = 'Y';
                $model->company_id   = Yii::app()->user->company_id;
                $model->specification_type = $_POST['WarehouseCategory']['specification_type'];
                if ($_POST['WarehouseCategory']['specification_type'] == 'A') {
                    $model->dieno = $_POST['WarehouseCategory']['dieno'];

                    $model->specification = $model->dieno;
                }

                if ($model->save()) {
                    if ($_FILES['WarehouseCategory']['name']['filename'] != '') {
                        $uploadfile    = $filename->saveAs($images_path . '/' . $newfilename . "." . $extension);
                        $image = new EasyImage($images_path . '/' . $newfilename . "." . $extension);
                        $image->resize(60, 60);
                        $image->save($images_path2 . '/' . $newfilename . "." . $extension);
                    }
                    Yii::app()->user->setFlash('success', "Updated Successfully");
                    $this->redirect(array('WarehouseCategory/newList'));
                }
            }
        }
        $this->redirect(array('WarehouseCategory/newList'));
    }


    public function actionCheckspecification()
    {
        if ($_REQUEST['specification'] != '' && $_REQUEST['parent_id'] != '') {
            $tblpx = Yii::app()->db->tablePrefix;
            if (isset($_REQUEST['brand_id']) && ($_REQUEST['brand_id']) != '') {
                $specification = Yii::app()->db->createCommand("SELECT id FROM {$tblpx}warehouse_category WHERE specification ='" . $_REQUEST['specification'] . "' AND parent_id = " . $_REQUEST['parent_id'] . " AND brand_id=" . $_REQUEST['brand_id'] . "")->queryRow();
            } else {
                $specification = Yii::app()->db->createCommand("SELECT id FROM {$tblpx}warehouse_category WHERE specification ='" . $_REQUEST['specification'] . "' AND parent_id = " . $_REQUEST['parent_id'] . " AND brand_id IS NULL")->queryRow();
            }
            if ($specification) {
                echo 'false';
            } else {
                echo 'true';
            }
        }
    }


    public function actionCheckcategory()
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $specification = Yii::app()->db->createCommand("SELECT id FROM {$tblpx}warehouse_category WHERE category_name ='" . $_REQUEST['category_name'] . "'")->queryRow();
        if ($specification) {
            echo 'false';
        } else {
            echo 'true';
        }
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);
        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);
        if (isset($_REQUEST['data'])) {
            parse_str($_REQUEST['data'], $data);
            $model->parent_id       = (($data['parent_id'] == '') ? NULL : $data['parent_id']);
            $model->category_name   = $data['category_name'];
            if ($model->save()) {

                return true;
            } else {

                echo 1;
                exit;
            }
        }
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $dataProvider = new CActiveDataProvider('WarehouseCategory');
        $model->unsetAttributes();  // clear any default values
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }



    public function actionNewlist()
    {

        //            $image = new EasyImage('/opt/lampp/htdocs/bhi/sherbrooke_final/uploads/warehouse_category/no_img.png');
        //                                            $image->resize(100, 100);
        //                                            $image->save('/opt/lampp/htdocs/bhi/sherbrooke_final/uploads/warehouse_category/thumbnail/no_img.png');
        $model = new WarehouseCategory();
        $model2 = new Brand();
        $model->unsetAttributes();  // clear any default values
        $this->render('newlist', array(
            'model' => $model,
            'model2' => $model2,
            'dataProvider' => $model->search(Yii::app()->user->id),
            'dataProvider1' => $model->specificationsearch(Yii::app()->user->id),
            'dataProvider3' => $model2->search(),
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model = new WarehouseCategory('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['WarehouseCategory']))
            $model->attributes = $_GET['WarehouseCategory'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return WarehouseCategory the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = WarehouseCategory::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    public function actiontest()
    {
        $model = new WarehouseCategory;
        $this->performAjaxValidation1($model);
        $model2 = new Brand();


        /*$this->render('newlist', array(
			'model' => $model,
		   'dataProvider' => $model->search(Yii::app()->user->id),
			'dataProvider1' => $model->specificationsearch(Yii::app()->user->id),
			'dataProvider3' => $model2->search(),
		));*/
    }

    /**
     * Performs the AJAX validation.
     * @param WarehouseCategory $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'purchase-category-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    protected function performAjaxValidation1($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'purchase-category-test') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionGetItemForEdit()
    {
        $categoryId         = $_REQUEST["category"];
        $categoryRs         = WarehouseCategory::model()->findByPk($categoryId);
        $category["parent"] = $categoryRs["parent_id"];
        $category["name"]   = $categoryRs["category_name"];
        $categoryDet        = json_encode($category);
        echo $categoryDet;
    }
    public function actionGetSpecificationForEdit()
    {

        $specification              = $_REQUEST["specification"];
        $categoryRs                 = WarehouseCategory::model()->findByPk($specification);
        $category["parent"]         = $categoryRs["parent_id"];
        $category["brand"]          = $categoryRs["brand_id"];
        $category["type"]           = $categoryRs["specification_type"];
        $category["specification"]  = $categoryRs["specification"];
        $category["dieno"]          = $categoryRs["dieno"];
        $category["length"]         = $categoryRs["length"];
        $category["unit"]           = $categoryRs["unit"];
        $category["filename"]       = $categoryRs["filename"];
        if ($categoryRs["filename"] != NULL) {
            $path = realpath(Yii::app()->basePath . '/../uploads/warehouse_category/' . $categoryRs["filename"]);
            if (file_exists($path)) {
                $category["imd_path"]       = "<img src=" . Yii::app()->request->baseUrl . "/uploads/warehouse_category/thumbnail/" . $categoryRs["filename"] . " alt='' style='cursor:pointer;height:30px;width:30px;' /> &nbsp; &nbsp; <a href='#' id=" . $categoryRs['id'] . " class='img_delete'>Delete</a> ";
            }
        } else {
            $category["imd_path"]       = "";
        }
        $categoryDet                = json_encode($category);
        echo $categoryDet;
    }
    public function actionUpdatespecification($id)
    {

        $model = $this->loadModel($id);
        //$this->performAjaxValidation1($model);
        //$model->unsetAttributes();


        if (isset($_REQUEST['data'])) {
            parse_str($_REQUEST['data'], $data);
            //$model->attributes=$_POST['WarehouseCategory'];
            $model->parent_id = $data['WarehouseCategory']['parent_id'];
            $model->specification = $data['WarehouseCategory']['specification'];
            $model->brand_id = $data['WarehouseCategory']['brand_id'];
            $model->unit = $data['WarehouseCategory']['unit'];
            $model->specification_type = $data['WarehouseCategory']['specification_type'];
            if ($data['WarehouseCategory']['specification_type'] == 'A') {
                $model->dieno = $data['WarehouseCategory']['dieno'];
                //$model->length = $data['WarehouseCategory']['length'];       
                $model->specification = $model->dieno;
            }
            $model->created_by = Yii::app()->user->id;
            $model->created_date =  date('Y-m-d');
            $model->company_id   = Yii::app()->user->company_id;
            if ($model->save()) {
                echo 1;
            }
        }
    }
    public function actionExportItemToCSV()
    {
        $tblpx          = Yii::app()->db->tablePrefix;
        $arraylabel     = array('Record Id', 'Category', 'Brand', 'Type', 'Specification', 'Created Date');
        $finaldata      = array();
        $sql            = "SELECT pc.id, pc.specification, pc.created_date, pc.specification_type, pc1.category_name as categoryname,br.brand_name FROM {$tblpx}warehouse_category pc
                            INNER JOIN {$tblpx}warehouse_category pc1 ON pc.parent_id = pc1.id
                            INNER JOIN {$tblpx}brand br ON pc.brand_id =br.id ORDER BY pc.specification ASC";
        $purchaseItem   = Yii::app()->db->createCommand($sql)->queryAll();
        $i              = 0;
        foreach ($purchaseItem as $item) {
            $finaldata[$i][] = $item["id"]; // $i + 1;
            $finaldata[$i][] = $item["categoryname"];
            $finaldata[$i][] = $item["brand_name"];
            if ($item["specification_type"] == "A") {
                $finaldata[$i][] = "By Length";
            } else if ($item["specification_type"] == "G") {
                $finaldata[$i][] = "By Width * Height";
            } else {
                $finaldata[$i][] = "By Quantity";
            }
            $finaldata[$i][] = '"' . $item["specification"] . '"';
            $finaldata[$i][] = date('Y-m-d',  strtotime($item["created_date"]));
            $i = $i + 1;
        }
        Yii::import('ext.ECSVExport');
        $csv            = new ECSVExport($finaldata);
        $csv->setHeaders($arraylabel);
        $csv->setToAppend();
        $contentdata    = $csv->toCSV();
        $csvfilename    = 'WareHouse_Item_Report_' . date('d_M_Y') . '.csv';
        Yii::app()->getRequest()->sendFile($csvfilename, $contentdata, "text/csv", false);
        exit();
    }
    public function actionMergingPurchasecatAndWarehouseCat()
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $sqlMerge_parents     = "SELECT `id`,`parent_id`, `brand_id`, `category_name`, `specification`, `unit`, `spec_flag`, `type`, `dieno`, `length`, `specification_type`, `filename`, `purchase_mastercategory`, `company_id`, `created_by`, `created_date`, `purchase_cat_id`
                            FROM jp_warehouse_category 
                            WHERE
                                purchase_cat_id IS NULL AND parent_id IS NULL
                            ORDER BY id ASC";
        $sqlMerge_not_parent     = "SELECT `id`,`parent_id`, `brand_id`, `category_name`, `specification`, `unit`, `spec_flag`, `type`, `dieno`, `length`, `specification_type`, `filename`, `purchase_mastercategory`, `company_id`, `created_by`, `created_date`, `purchase_cat_id`
                        FROM jp_warehouse_category 
                        WHERE
                            purchase_cat_id IS NULL AND parent_id IS NOT NULL
                        ORDER BY id ASC";

        $mergedetails_parents          = Yii::app()->db->createCommand($sqlMerge_parents)->queryAll();
        foreach ($mergedetails_parents as $parent_details) {
            $warehouse_cat_id = $parent_details['id'];
            $parent_id = $parent_details['parent_id'];
            $brand_id = $parent_details['brand_id'];
            $category_name = $parent_details['category_name'];
            $specification = $parent_details['specification'];
            $unit = $parent_details['unit'];
            $spec_flag = $parent_details['spec_flag'];
            $type = $parent_details['type'];
            $dieno = $parent_details['dieno'];
            $length = $parent_details['length'];
            $specification_type = $parent_details['specification_type'];
            $filename = $parent_details['filename'];
            $purchase_mastercategory = $parent_details['purchase_mastercategory'];
            $company_id = $parent_details['company_id'];
            $created_by = $parent_details['created_by'];
            $created_date = $parent_details['created_date'];
            $purchase_cat_id = $parent_details['purchase_cat_id'];
            $warehouse_cat_id_check_sql = "SELECT *
                                        FROM jp_purchase_category 
                                        WHERE
                                        warehouse_cat_primary_key ='" . $warehouse_cat_id . "'";
            $warehouse_cat_id_check = Yii::app()->db->createCommand($warehouse_cat_id_check_sql)->queryRow();
            if (empty($warehouse_cat_id_check)) {
                $purchase_model = new PurchaseCategory();
                $purchase_model->parent_id = $parent_id;
                $purchase_model->brand_id = $brand_id;
                $purchase_model->category_name = $category_name;
                $purchase_model->specification = $specification;
                $purchase_model->unit = $unit;
                $purchase_model->spec_flag = $spec_flag;
                $purchase_model->type = $type;
                $purchase_model->dieno = $dieno;
                $purchase_model->length = $length;
                $purchase_model->specification_type = $specification_type;
                $purchase_model->filename = $filename;
                $purchase_model->purchase_mastercategory = $purchase_mastercategory;
                $purchase_model->company_id = $company_id;
                $purchase_model->created_by = $created_by;
                $purchase_model->created_date = $created_date;
                $purchase_model->warehouse_cat_primary_key = $warehouse_cat_id;
                if ($purchase_model->save(false)) {
                    $last_inserted_id = Yii::app()->db->getLastInsertID();
                    $warehouse_model = $this->loadModel($warehouse_cat_id);
                    $warehouse_model->purchase_cat_id = $last_inserted_id;
                    $warehouse_model->save(false);
                } else {
                    $last_inserted_id = NULL;
                }
            }
        }
        $mergedetails_not_parent         = Yii::app()->db->createCommand($sqlMerge_not_parent)->queryAll();
        foreach ($mergedetails_not_parent as $not_parent_details) {
            $warehouse_cat_id2 = $not_parent_details['id'];
            $parent_id2 = $not_parent_details['parent_id'];
            $brand_id2 = $not_parent_details['brand_id'];
            $category_name2 = $not_parent_details['category_name'];
            $specification2 = $not_parent_details['specification'];
            $unit2 = $not_parent_details['unit'];
            $spec_flag2 = $not_parent_details['spec_flag'];
            $type2 = $not_parent_details['type'];
            $dieno2 = $not_parent_details['dieno'];
            $length2 = $not_parent_details['length'];
            $specification_type2 = $not_parent_details['specification_type'];
            $filename2 = $not_parent_details['filename'];
            $purchase_mastercategory2 = $not_parent_details['purchase_mastercategory'];
            $company_id2 = $not_parent_details['company_id'];
            $created_by2 = $not_parent_details['created_by'];
            $created_date2 = $not_parent_details['created_date'];
            $purchase_cat_id2 = $not_parent_details['purchase_cat_id'];
            $warehouse_cat_id_check_sql2 = "SELECT *
                                        FROM jp_purchase_category 
                                        WHERE
                                        warehouse_cat_primary_key ='" . $warehouse_cat_id2 . "'";
            $warehouse_cat_id_check2 = Yii::app()->db->createCommand($warehouse_cat_id_check_sql2)->queryRow();
            if (empty($warehouse_cat_id_check2)) {
                $purchase_model2 = new PurchaseCategory();
                $warehouse_cat_parent_id_sql = "SELECT *
                                                FROM jp_purchase_category 
                                                WHERE
                                                warehouse_cat_primary_key ='" . $parent_id2 . "'";
                $warehouse_cat_parent_id_details = Yii::app()->db->createCommand($warehouse_cat_parent_id_sql)->queryRow();
                $warehouse_cat_parent_id = $warehouse_cat_parent_id_details['id'];
                $purchase_model2->parent_id = $warehouse_cat_parent_id;
                $purchase_model2->brand_id = $brand_id2;
                $purchase_model2->category_name = $category_name2;
                $purchase_model2->specification = $specification2;
                $purchase_model2->unit = $unit2;
                $purchase_model2->spec_flag = $spec_flag2;
                $purchase_model2->type = $type2;
                $purchase_model2->dieno = $dieno2;
                $purchase_model2->length = $length2;
                $purchase_model2->specification_type = $specification_type2;
                $purchase_model2->filename = $filename2;
                $purchase_model2->purchase_mastercategory = $purchase_mastercategory2;
                $purchase_model2->company_id = $company_id2;
                $purchase_model2->created_by = $created_by2;
                $purchase_model2->created_date = $created_date2;
                $purchase_model2->warehouse_cat_primary_key = $warehouse_cat_id2;
                if ($purchase_model2->save(false)) {
                    $last_inserted_id2 = Yii::app()->db->getLastInsertID();
                    $warehouse_model2 = $this->loadModel($warehouse_cat_id2);
                    $warehouse_model2->purchase_cat_id = $last_inserted_id2;
                    $warehouse_model2->save(false);
                } else {
                    $last_inserted_id2 = NULL;
                }
            }
        }
    }
    public function actionPurchasereport()
    {
        $tblpx = Yii::app()->db->tablePrefix;
        if (isset($_POST['project_id']) && !empty($_POST['project_id'])) {
            $project_id     = $_REQUEST['project_id'];
            $sqlProject     = "SELECT pr.*,cl.name as clientname FROM {$tblpx}projects pr LEFT JOIN {$tblpx}clients cl ON pr.client_id = cl.cid WHERE pr.pid = {$project_id}";
            $model          = Yii::app()->db->createCommand($sqlProject)->queryRow();
            $dataProvider   = "";
            $date_from      = isset($_POST['date_from']) && !empty($_POST['date_from']) ? date('Y-m-d', strtotime($_POST['date_from'])) : "";
            $date_to        = isset($_POST['date_to']) && !empty($_POST['date_to']) ? date('Y-m-d', strtotime($_POST['date_to'])) : "";
            if (isset($date_from) && !empty($date_from) && isset($date_to) && !empty($date_to)) {
                $condition = " AND (bl.bill_date BETWEEN '" . $date_from . "' AND '" . $date_to . "')";
            } else if (isset($date_from) && empty($date_from) && isset($date_to) && !empty($date_to)) {
                $condition = " AND bl.bill_date <= '" . $date_to . "'";
            } else if (isset($date_from) && !empty($date_from) && isset($date_to) && empty($date_to)) {
                $condition = " AND bl.bill_date >= '" . $date_from . "'";
            } else {
                $condition = "";
            }
            $sqlQueryCat    = "SELECT pc.id,pc.category_name,a.billitemsum, a.itemtotalamount FROM {$tblpx}warehouse_category pc
                                LEFT JOIN (SELECT SUM(bi.billitem_quantity) as billitemsum, SUM((bi.billitem_amount - bi.billitem_discountamount) + bi.billitem_taxamount) as itemtotalamount, pc3.id as parentid FROM {$tblpx}billitem bi
                                    LEFT JOIN {$tblpx}warehouse_category pc1 ON bi.category_id = pc1.id
                                    LEFT JOIN {$tblpx}warehouse_category pc2 ON pc1.parent_id = pc2.id
                                    LEFT JOIN {$tblpx}warehouse_category pc3 ON pc2.parent_id = pc3.id
                                    LEFT JOIN {$tblpx}bills bl ON bi.bill_id = bl.bill_id
                                    LEFT JOIN {$tblpx}purchase pu ON bl.purchase_id = pu.p_id 
                                    WHERE pu.project_id = {$project_id}" . $condition . " GROUP BY pc3.id) a ON a.parentid = pc.id
                                WHERE pc.parent_id IS NULL AND a.billitemsum != 0 GROUP BY pc.id";
            $itemmodelcat   = Yii::app()->db->createCommand($sqlQueryCat)->queryAll();
            $catCount       = count($sqlQueryCat);
            $dataProvider1  = new CSqlDataProvider($sqlQueryCat, array(
                'totalItemCount' => $catCount,
                'keyField' => 'billitem_id',
                'id' => 'billitem_id',
                'sort' => array(
                    'attributes' => array(
                        'billitem_id', 'bill_id',
                    ),
                ),
                'pagination' => array(
                    'pageSize' => 10,
                ),
            ));
        } else {
            $project_id     = "";
            $model          = new Projects;
            $dataProvider   = $model->search();
            $itemmodelcat   = "";
            $dataProvider1  = "";
            $date_from      = "";
            $date_to        = "";
        }
        $this->render('purchasereport', array(
            'model' => $model,
            'dataProvider' => $dataProvider,
            'project_id' => $project_id,
            'itemmodelcat' => $itemmodelcat,
            'dataProvider1' => $dataProvider1,
            'date_from' => $date_from,
            'date_to' => $date_to,
        ));
    }
    public function actionSavepurchasereportpdf()
    {
        $tblpx          = Yii::app()->db->tablePrefix;
        $project_id     = $_REQUEST['project_id'];
        $sqlProject     = "SELECT pr.*,cl.name as clientname FROM {$tblpx}projects pr LEFT JOIN {$tblpx}clients cl ON pr.client_id = cl.cid WHERE pr.pid = {$project_id}";
        $model          = Yii::app()->db->createCommand($sqlProject)->queryRow();
        $dataProvider   = "";
        $date_from      = isset($_POST['date_from']) && !empty($_POST['date_from']) ? date('Y-m-d', strtotime($_POST['date_from'])) : "";
        $date_to        = isset($_POST['date_to']) && !empty($_POST['date_to']) ? date('Y-m-d', strtotime($_POST['date_to'])) : "";
        if (isset($date_from) && !empty($date_from) && isset($date_to) && !empty($date_to)) {
            $condition = " AND (bl.bill_date BETWEEN '" . $date_from . "' AND '" . $date_to . "')";
        } else if (isset($date_from) && empty($date_from) && isset($date_to) && !empty($date_to)) {
            $condition = " AND bl.bill_date <= '" . $date_to . "'";
        } else if (isset($date_from) && !empty($date_from) && isset($date_to) && empty($date_to)) {
            $condition = " AND bl.bill_date >= '" . $date_from . "'";
        } else {
            $condition = "";
        }
        $sqlQueryCat    = "SELECT pc.id,pc.category_name,a.billitemsum, a.itemtotalamount FROM {$tblpx}warehouse_category pc
                                LEFT JOIN (SELECT SUM(bi.billitem_quantity) as billitemsum, SUM((bi.billitem_amount - bi.billitem_discountamount) + bi.billitem_taxamount) as itemtotalamount, pc3.id as parentid FROM {$tblpx}billitem bi
                                    LEFT JOIN {$tblpx}warehouse_category pc1 ON bi.category_id = pc1.id
                                    LEFT JOIN {$tblpx}warehouse_category pc2 ON pc1.parent_id = pc2.id
                                    LEFT JOIN {$tblpx}warehouse_category pc3 ON pc2.parent_id = pc3.id
                                    LEFT JOIN {$tblpx}bills bl ON bi.bill_id = bl.bill_id
                                    LEFT JOIN {$tblpx}purchase pu ON bl.purchase_id = pu.p_id 
                                    WHERE pu.project_id = {$project_id}" . $condition . " GROUP BY pc3.id) a ON a.parentid = pc.id
                                WHERE pc.parent_id IS NULL AND a.billitemsum != 0 GROUP BY pc.id";
        $itemmodelcat   = Yii::app()->db->createCommand($sqlQueryCat)->queryAll();
        $catCount       = count($sqlQueryCat);
        $dataProvider1  = new CSqlDataProvider($sqlQueryCat, array(
            'totalItemCount' => $catCount,
            'keyField' => 'billitem_id',
            'id' => 'billitem_id',
            'sort' => array(
                'attributes' => array(
                    'billitem_id', 'bill_id',
                ),
            ),
            'pagination' => array(
                'pageSize' => 10,
            ),
        ));
        $mPDF1  = Yii::app()->ePdf->mPDF();
        $mPDF1  = Yii::app()->ePdf->mPDF('', 'A4');
        $mPDF1->WriteHTML($stylesheet, 1);
        $mPDF1->WriteHTML($this->renderPartial('savepurchasereport', array(
            'model' => $model,
            'dataProvider' => $dataProvider,
            'project_id' => $project_id,
            'itemmodelcat' => $itemmodelcat,
            'dataProvider1' => $dataProvider1,
            'date_from' => $date_from,
            'date_to' => $date_to,
        ), true));
        $mPDF1->Output('Purchase Report' .  '.pdf', 'D');
    }
    public function actionExportPurchaseReportCSV()
    {
        $project_id = $_REQUEST["project_id"];
        $date_from  = $_REQUEST["fromdate"];
        $date_to    = $_REQUEST["todate"];
        $tblpx      = Yii::app()->db->tablePrefix;
        $sqlProject = "SELECT pr.*,cl.name as clientname FROM {$tblpx}projects pr LEFT JOIN {$tblpx}clients cl ON pr.client_id = cl.cid WHERE pr.pid = {$project_id}";
        $model      = Yii::app()->db->createCommand($sqlProject)->queryRow();
        if (isset($date_from) && !empty($date_from) && isset($date_to) && !empty($date_to)) {
            $condition = " AND (bl.bill_date BETWEEN '" . $date_from . "' AND '" . $date_to . "')";
        } else if (isset($date_from) && empty($date_from) && isset($date_to) && !empty($date_to)) {
            $condition = " AND bl.bill_date <= '" . $date_to . "'";
        } else if (isset($date_from) && !empty($date_from) && isset($date_to) && empty($date_to)) {
            $condition = " AND bl.bill_date >= '" . $date_from . "'";
        } else {
            $condition = "";
        }
        $sqlQueryCat    = "SELECT pc.id,pc.category_name,a.billitemsum, a.itemtotalamount FROM {$tblpx}warehouse_category pc
                                LEFT JOIN (SELECT SUM(bi.billitem_quantity) as billitemsum, SUM((bi.billitem_amount - bi.billitem_discountamount) + bi.billitem_taxamount) as itemtotalamount, pc3.id as parentid FROM {$tblpx}billitem bi
                                    LEFT JOIN {$tblpx}warehouse_category pc1 ON bi.category_id = pc1.id
                                    LEFT JOIN {$tblpx}warehouse_category pc2 ON pc1.parent_id = pc2.id
                                    LEFT JOIN {$tblpx}warehouse_category pc3 ON pc2.parent_id = pc3.id
                                    LEFT JOIN {$tblpx}bills bl ON bi.bill_id = bl.bill_id
                                    LEFT JOIN {$tblpx}purchase pu ON bl.purchase_id = pu.p_id 
                                    WHERE pu.project_id = {$project_id}" . $condition . " GROUP BY pc3.id) a ON a.parentid = pc.id
                                WHERE pc.parent_id IS NULL AND a.billitemsum != 0 GROUP BY pc.id";
        $itemmodelcat   = Yii::app()->db->createCommand($sqlQueryCat)->queryAll();
        $arraylabel = array("", "");

        $finaldata[0][]  = "Client";
        $finaldata[0][]  = $model["clientname"];
        $finaldata[1][]  = "Project Name";
        $finaldata[1][]  = $model["name"];
        $finaldata[2][]  = "Site";
        $finaldata[2][]  = $model["site"];

        $finaldata[3][]  = "";
        $finaldata[3][]  = "";
        $finaldata[3][]  = "";
        $finaldata[3][]  = "";

        $finaldata[4][]  = "Sl No";
        $finaldata[4][]  = "Category";
        $finaldata[4][]  = "Quantity";
        $finaldata[3][]  = "Amount";

        $k               = 5;
        $i               = 1;
        $quantitySum     = 0;
        $amountSum       = 0;
        foreach ($itemmodelcat as $modelcat) {
            $quantitySum        = $quantitySum + $modelcat["billitemsum"];
            $amountSum          = $amountSum + $modelcat["itemtotalamount"];
            $finaldata[$k][]    = $i;
            $finaldata[$k][]    = $modelcat["category_name"];
            $finaldata[$k][]    = Controller::money_format_inr($modelcat["billitemsum"], 2);
            $finaldata[$k][]    = Controller::money_format_inr($modelcat["itemtotalamount"], 2);
            $k                  = $k + 1;
            $i                  = $i + 1;
        }
        $finaldata[$k][]    = " ";
        $finaldata[$k][]    = " ";
        $finaldata[$k][]    = Controller::money_format_inr($quantitySum, 2);
        $finaldata[$k][]    = Controller::money_format_inr($amountSum, 2);

        Yii::import('ext.ECSVExport');
        $csv = new ECSVExport($finaldata);
        $csv->setHeaders($arraylabel);
        $csv->setToAppend();
        $contentdata = $csv->toCSV();
        $csvfilename = 'Purchase Report' . date('d_M_Y') . '.csv';
        Yii::app()->getRequest()->sendFile($csvfilename, $contentdata, "text/csv", false);
        exit();
    }

    public function actionimg_delete()
    {
        $id = $_GET['id'];
        $model      = WarehouseCategory::model()->findByPk($id);
        $filename   = $model->filename;
        //$filePath   = Yii::app()->basePath.'/uploads/'.$filename;
        $filePath   = 'uploads/warehouse_category/' . $filename;
        $path = Yii::app()->request->baseUrl . "/uploads/warehouse_category/thumbnail/no_img.png";
        if (unlink($filePath)) {
            $model->filename = NULL;
            if ($model->save()) {
                echo json_encode(array('status' => 1, 'path' => $path));
            } else {
                echo 2;
            }
        }
    }
}
