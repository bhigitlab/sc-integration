<style>
    table {
        border-collapse: collapse;
        width: 100%;
    }

    /*.container{max-width: 1050px;margin:0 auto;padding: 15px;border: 1px solid #ddd;}*/
    .text-right {
        text-align: right
    }

    .details,
    .info-table {
        border: 1px solid #212121;
        margin-top: 20px;
    }

    .details td,
    .info-table td,
    .info-table th {
        padding: 6px 8px;
    }

    .info-table {
        margin-bottom: 10px;
    }

    .text-center {
        text-align: center;
    }

    .img-hold {
        width: 10%;
    }

    .companyhead {
        font-size: 18px;
        font-weight: bold;
        margin-top: 0px;
        color: #789AD1;
    }
</style>

<div class="container">
    <table border=0>
        <tbody>
            <tr>
                <td class="img-hold"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/logo.png" alt="img" style='margin-top:-45px;' /></td>
                <td class=text-right>GST NO: <?php echo isset($gstnum) ? $gstnum : ''; ?></td>
            </tr>
        </tbody>
    </table>
    <table border=0>
        <tbody>
            <tr>
                <?php
                $company_address = Company::model()->findBypk($poCompanyId);
                ?>
                <td class="text-center">
                    <p class='companyhead'><?php echo isset($company) ? $company : ''; ?></p><br>
                    <p><?php echo ((isset($company_address['address']) && !empty($company_address['address'])) ? ($company_address['address']) : ""); ?><br>
                        PIN : <?php echo ((isset($company_address['pincode']) && !empty($company_address['pincode'])) ? ($company_address['pincode']) : ""); ?>,&nbsp;
                        PHONE : <?php echo ((isset($company_address['phone']) && !empty($company_address['phone'])) ? ($company_address['phone']) : ""); ?><br>
                        EMAIL : <?php echo ((isset($company_address['email_id']) && !empty($company_address['email_id'])) ? ($company_address['email_id']) : ""); ?></p><br>
                    <h4>Purchase Order</h4>
                </td>
            </tr>
        </tbody>
    </table>
    <table class="details">
        <tbody>
            <tr>
                <td><label>COMPANY :</label><span><?php echo isset($company_name) ? $company_name : ''; ?></span></td>
                <td><label>Shipping Address: </label><span><?php echo isset($purchase_data['shipping_address']) ? nl2br($purchase_data['shipping_address']) : '-'; ?></span></td>
                <td class="text-right"><label>DATE : </label><span><?php echo date("d-m-Y", strtotime($date)); ?></span></td>
            </tr>
            <tr>
                <td><label>PROJECT :</label><span><?php echo $project; ?></span></td>
                <td><label>Contact No: </label><span><?php echo isset($purchase_data['contact_no']) ? $purchase_data['contact_no'] : '-'; ?></span></td>
                <td class="text-right"><label>VENDOR : </label><span><?php echo (isset($vendor)) ? $vendor : ''; ?></span></td>
            </tr>
            <tr>
                <td><label>EXPENSE HEAD : </label><?php echo (isset($expense_head)) ? $expense_head : ''; ?></td>
                <td class="text-right"><label>PURCHASE NO : </label><span><?php echo isset($purchaseno) ? $purchaseno : ''; ?></span></td>
                <td></td>
            </tr>
        </tbody>
    </table>
    <table border="1" class="info-table">
        <thead>
            <tr>
                <th>Sl.No</th>
                <th>Specification</th>
                <th>Quantity</th>
                <th>Unit</th>
                <th>HSN Code</th>
                <th>Rate</th>
                <th>Amount</th>
            </tr>
        </thead>
        <tbody>
            <?php
            for ($i = 0; $i < count($quantity); $i++) {
            ?>
                <tr>
                    <td><?php echo $i + 1; ?></td>
                    <td><?php echo $specification[$i]; ?></td>
                    <td class="text-right"><?php echo $quantity[$i]; ?></td>
                    <td><?php echo $unit[$i]; ?></td>
                    <td><?php echo $hsn_code[$i]; ?></td>
                    <td class="text-right"><?php echo Controller::money_format_inr($rate[$i], 2, 1); ?></td>
                    <td class="text-right"><?php echo Controller::money_format_inr($amount[$i], 2, 1); ?></td>
                </tr>

            <?php
            }
            ?>
        </tbody>
        <tfoot>
            <tr>
                <th style="text-align: right;" colspan="6" class="text-right">GRAND TOTAL: </th>
                <th class="text-right"><?php echo Controller::money_format_inr($grand, 2, 1); ?></th>
            </tr>
            <tr>
                <td colspan="7" align="right">*Exclusive of GST</td>
            </tr>
        </tfoot>

    </table>

    <h4>Authorized Signatory,</h4>
    <h4>For <?php echo Yii::app()->name; ?></h4>
</div>