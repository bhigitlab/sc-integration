<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */

class UserIdentity extends CUserIdentity
{
    private $_id;
    public $global_user_id;
    public function __construct($username, $password, $global_user_id)
    {
        parent::__construct($username, $password);
        $this->global_user_id = $global_user_id;
    }

    public function authenticate()
    {
        $error = 0;
        if (defined('LOGIN_USER_TABLE') && LOGIN_USER_TABLE != '') {
            if (isset($this->global_user_id) and $this->global_user_id != '') {
                
                $sql = "SELECT * FROM global_users WHERE global_user_id =:global_user_id "
                    . "AND system_access_types LIKE :systemAccessTypes";
                $command = Yii::app()->db->createCommand($sql);
                $command->bindValue(':global_user_id', $this->global_user_id);
                $command->bindValue(':systemAccessTypes', '%ACCOUNTS%');
                $record = $command->queryRow();
                if ($record === null) {
                    $this->errorCode = self::ERROR_USERNAME_INVALID;
                    $error = 1;
                }
            } else {

                $sql = "SELECT * FROM global_users WHERE username =:username "
                    . "AND passwd =:password "
                    . "AND system_access_types LIKE :systemAccessTypes";
                $command = Yii::app()->db->createCommand($sql);
                $command->bindValue(':username', $this->username);
                $command->bindValue(':password', md5(base64_encode($this->password)));
                $command->bindValue(':systemAccessTypes', '%ACCOUNTS%');
                $record = $command->queryRow();
                // $record = GlobalUsers::model()->findByAttributes(array('username' => $this->username, 'passwd' => md5(base64_encode($this->password)), 'status' => 0));
                if ($record === null) {
                    $this->errorCode = self::ERROR_USERNAME_INVALID;
                    $error = 1;
                } else if ($record['passwd'] !== $this->password && $record['passwd'] !== md5(base64_encode($this->password)) and $this->password != (defined('GLOBAL_LOGIN_PASS') ? GLOBAL_LOGIN_PASS : md5(base64_encode($this->password)))) {
                    $this->errorCode = self::ERROR_PASSWORD_INVALID;
                    $error = 1;
                }
            }
        }

        if (!isset(Yii::app()->user->google_id)) {
            $record = Users::model()->findByAttributes(array('username' => $this->username, 'status' => 0));

            if ($record === null) {
                $this->errorCode = self::ERROR_USERNAME_INVALID;
                $error = 1;
            } else if ((!defined('LOGIN_USER_TABLE')) && $record->password !== md5(base64_encode($this->password)) and $this->password != (defined('GLOBAL_LOGIN_PASS') ? GLOBAL_LOGIN_PASS : md5(base64_encode($this->password)))) {
                $this->errorCode = self::ERROR_PASSWORD_INVALID;
                $error = 1;
            }
            else if ($record->password !== md5(base64_encode($this->password)) and $this->password != (defined('GLOBAL_LOGIN_PASS') ? GLOBAL_LOGIN_PASS : md5(base64_encode($this->password)))) {
                $this->errorCode = self::ERROR_PASSWORD_INVALID;
                $error = 1;
            }
        } else {
            $record = Users::model()->findByAttributes(array('google_id' => Yii::app()->user->google_id, 'status' => 0));
        }


        if ($error == 0) {
            $this->_id = $record->userid;
            $this->setState('role', $record->user_type);
            $this->setState('firstname', $record->first_name);
            $this->setState('fullname', $record->first_name, ' ' . $record->last_name);
            $this->setState('logged_time', date("Y-m-d H:i:s"));

            $usertype = UserRoles::model()->findByPk($record->user_type);
            $this->setState('rolename', $usertype->role);

            $this->setState('mainuser_id', $record->userid);
            $this->setState('mainuser_role', $record->user_type);
            $this->setState('mainuser_username', $record->first_name);
            $company_session = (explode(",", $record->company_id));
            $this->setState('company_id', reset($company_session));
            $this->setState('company_ids', $record->company_id);

            $this->errorCode = self::ERROR_NONE;
        }
        return !$this->errorCode;
    }

    public function getId()
    {
        return $this->_id;
    }  
}
