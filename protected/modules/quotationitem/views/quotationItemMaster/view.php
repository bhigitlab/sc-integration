<?php
/* @var $this QuotationItemMasterController */
/* @var $model QuotationItemMaster */

$this->breadcrumbs=array(
	'Quotation Item Masters'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List QuotationItemMaster', 'url'=>array('index')),
	array('label'=>'Create QuotationItemMaster', 'url'=>array('create')),
	array('label'=>'Update QuotationItemMaster', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete QuotationItemMaster', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage QuotationItemMaster', 'url'=>array('admin')),
);
?>

<h1>View QuotationItemMaster #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'quotation_category_id',
		'work_type_id',
		'worktype_label',
		'work_type_description',
		'image',
		'material_id',
		'finish_id',
		'description',
		'rate',
		'profit',
		'status',
		'created_by',
		'created_on',
	),
)); ?>
