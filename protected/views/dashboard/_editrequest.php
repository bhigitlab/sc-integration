<?php
$tableName = "";
$editrequest = ApproveRequestHelper::editRequestData();

//echo "<pre>";print_r($editrequest);exit;
foreach ($editrequest as $data) {


    if (is_array($data)){
        $id = $data['id']; 
        $updatedBY  = $data['updated_by'];
    
    if (is_array($data) && array_key_exists('exp_id', $data))
    {  
        $section = 'DAYBOOK';
        $primaryKeyColumn = 'exp_id';       
        $type = 1;
        $tableName = $this->tableNameAcc('pre_expenses', 0);
        $modelName = "PreExpenses";
        $parentModelName = "Expenses";
    } else if (is_array($data) && array_key_exists('daily_v_id', $data)) {
        $section = 'Vendor Payment';
        $primaryKeyColumn = 'daily_v_id';
        $type = 2;
        $tableName = $this->tableNameAcc('pre_dailyvendors', 0);
        $modelName = "PreDailyvendors";
        $parentModelName = "Dailyvendors";
    }else if (is_array($data) && array_key_exists('payment_id', $data)) {
        $section = 'Subcontractor Payment';
        $primaryKeyColumn = 'payment_id';
        $type = 3;
        $tableName = $this->tableNameAcc('pre_subcontractor_payment', 0);
        $modelName = "PreSubcontractorPayment";
        $parentModelName = "SubcontractorPayment";
    }
    else if (is_array($data) && array_key_exists('dailyexp_id', $data)) {
        $section = 'Daily Expense';
        $primaryKeyColumn = 'dailyexp_id';
        $type = 4;
        $tableName = $this->tableNameAcc('pre_dailyexpenses', 0);
        $modelName = "PreDailyexpenses";
        $parentModelName = "Dailyexpense";
    }else if (is_array($data) && array_key_exists('dr_id', $data)) {
        $section = 'Daily Report';
        $primaryKeyColumn = 'dr_id';
        $type = 5;
        $tableName = $this->tableNameAcc('pre_dailyreport', 0);
        $modelName = "PreDailyreport";
        $parentModelName = "Dailyreport";
    }
    ?>
    <div class="parent_row">
        <div class="row box-collapse">
            <div class="col-md-1 logo_sec nowrap">
                <input type="checkbox" name="editrequest" class="editrequestcheck"  data-id ="<?php echo $data['id'] ?>" data-ref_id ="<?php echo $data['ref_id'] ?>"
                                data-table="<?php echo $tableName; ?>" 
                                data-model="<?php echo $modelName; ?>" 
                                data-parent="<?php echo $parentModelName; ?>" 
                       value="<?php echo $id; ?>" />
                <button class="btn_logo">ER</button>
            </div>
            <div class="col-md-7 content_sec">
                <div class="row  clearfix">
                    <div class="col-md-8">
                        <div class="list-item">
                            <?php
                            $user = Users::model()->findByPk($updatedBY);
                            ?>
                            <b><?php echo $user->first_name . ' ' . $user->last_name; ?>
                            </b> Changed <b><?php echo $section; ?></b> data

                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="list-item pull-right">
                            <label>Date:</label>
                            <span><?php echo date('Y-m-d H:i:s', strtotime($data['record_grop_id'])); ?></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4" >
                <div class="list-item text-right act_btn" data-table="<?php echo $tableName ?>">
                    <?php if (Yii::app()->user->role == 1) { ?>
                        <button class="btn btn-xs btn_approve btn_sec approve_edit" 
                                id="<?php echo $data['id']; ?>" 
                                 data-id ="<?php echo $data['id'] ?>" data-ref_id ="<?php echo $data['ref_id'] ?>" 
                                data-table="<?php echo $tableName; ?>" 
                                data-model="<?php echo $modelName; ?>" 
                                data-parent="<?php echo $parentModelName; ?>"
                                >Approve</button>
                            <?php } ?>
                    &nbsp;<button class="btn btn-xs btn_decline btn_sec cancel_edit" 
                    
                    id="<?php echo $data['id']; ?>" 
                                data-id ="<?php echo $data['ref_id'] ?>" 
                                data-table="<?php echo $tableName; ?>" 
                                data-model="<?php echo $modelName; ?>" 
                                data-parent="<?php echo $parentModelName; ?>"
                    >Decline </button>
                    <button class="compare_icon popup btn btn-xs btn-default" title="Compare">Compare</i></button>
                    <button class="box_close btn btn-xs btn-default" style="display:none;">Close</i></button>
                </div>
            </div>
        </div>
        <div class="compare_box popuptext" style="display:none;">
            <div class="box_content" style="overflow: auto;">
                <table class="table w-100">
                    <thead>
                        <tr>
                            <th></th>
                            <th class="text-center">Company</th>
                            <?php if ($type == 4) { ?>
                            <th class="text-center">Bill No / Transaction Head</th>
                            <?php } ?>
                            <?php if ($type == 3 || $type == 5) { ?>
                            <th class="text-center">Subcontractor</th>
                            <?php } ?>
                            <?php if ($type != 4 || $type == 5) { ?>
                            <th class="text-center">Project</th>
                            <?php } ?>
                            <?php if ($type == 2 || $type==1) { ?>
                            <th class="text-center">Vendor</th>
                            <?php } ?>
                            <?php if ($type == 2) { ?>
                            <th class="text-center">Purchase no</th>
                            <?php } ?>
                            <?php if ($type == 1) { ?>
                                <th class="text-center">Bill Number</th>
                                <th class="text-center">Invoice Number</th>
                                <th class="text-center">Return Number</th>                                                          
                                <th class="text-center">Petty Cash By</th>
                            <?php } ?>  
                            <?php if ($type == 1 || $type==5) {  ?>
                            <th class="text-center">Expense Head</th> 
                            <?php } ?>
                            <?php if ($type != 5) { ?>
                                <th class="text-center">
                                    <?php echo (($type == 1) ? "Expense Type" : "Transaction Type"); ?>
                                </th>                                                                                  

                                <th class="text-center">Bank</th>
                                <th class="text-center">Cheque No</th>
                                <th class="text-center">Description</th>
                                <th class="text-center">Amount</th>
                                <th class="text-center">Tax</th>                            
                                <th class="text-center">Amount Paid</th>
                            <?php } ?>
                            <?php if ($type == 5) { ?>
                                <th class="text-center">Amount</th>
                            <?php }  ?>
                            <?php if ($type == 1 || $type == 2 || $type == 3) { ?>
                            <th class="text-center">TDS Amount</th>
                            <?php } ?>
                            <?php if ($type == 1 || $type==4) { ?>
                                <th class="text-center">Total</th> 
                                <th class="text-center">Receipt</th>
                                <th class="text-center">Purchase Type</th>
                            <?php } ?>
                            <?php if ($type == 1) { ?>
                                
                                <th class="text-center">Receipt Type</th>                                                            
                                <th class="text-center">Amount to Beneficiary</th>
                            <?php } ?>

                            <?php if($type ==4){ ?>
                                <th class="text-center">Paid To</th>
                            <?php }?> 
                        </tr>

                        <tr>
                            <?php
                                $olddata = array();
                                $purchaseType = "";
                                $gsttotal = "";                                 
                                $employee = '';
                                
                                $paid="";

                                if ($data['followup_id'] != NULL) {
                                    $condition = "$primaryKeyColumn = '" . $data['followup_id'] . "'";
                                    $olddata = $modelName::model()->find(array('condition' => $condition));
                                }else{
                                    $condition = "$primaryKeyColumn = '" . $data['ref_id'] . "'";
                                    $olddata = $parentModelName::model()->find(array('condition' => $condition));
                                }

                                
                                $company = Company::model()->findByPk($olddata["company_id"]);
                                //project
                                
                                if ($type == 2 || $type==3) {
                                    $oldproject = Projects::model()->findByPk($olddata["project_id"]);
                                }else if ($type == 1 ||$type==5){
                                    $oldproject = Projects::model()->findByPk($olddata["projectid"]);
                                }
                                
                                //payment type

                                if ($type == 4){
                                    $paymentType = ApproveRequestHelper::getPaymentType($olddata["expense_type"]);
                                }else if ($type != 5){
                                    $paymentType = ApproveRequestHelper::getPaymentType($olddata["payment_type"]);
                                }

                                if ($type != 5){                                
                                    $receiptType =  $paymentType['receiptType'];
                                    $expense_type =  $paymentType['expense_type'];
                                }

                                $bank = "";
                                if($type == 1) { 
                                    $gsttotal = $olddata['expense_sgst'] + $olddata['expense_cgst'] + $olddata['expense_igst'];
                                    $paid = $olddata["paid"];
                                    $amount= $olddata["expense_amount"];
                                    $oldreceipt  = $olddata["receipt"];
                                    if (isset($olddata["purchase_type"])) {
                                        $purchaseType = ApproveRequestHelper::getPurchaseType($olddata["purchase_type"]);
                                    }
                                    $bank = Bank::model()->findByPk($olddata["bank_id"]);                                    
                                }else if($type == 4){
                                    $gsttotal = $olddata['dailyexpense_sgst'] + $olddata['dailyexpense_cgst'] + $olddata['dailyexpense_igst'];
                                    $paid =$olddata["dailyexpense_paidamount"];
                                    $amount=$olddata["dailyexpense_amount"];
                                    $oldreceipt  = $olddata["dailyexpense_receipt"];
                                    if ($olddata["employee_id"] != '') {
                                        $users = Users::model()->findByPk($olddata["employee_id"]);
                                        $employee = $users->first_name . ' ' . $users->last_name;
                                    } 
                                    if (isset($olddata["dailyexpense_purchase_type"])) {
                                        $purchaseType = ApproveRequestHelper::getPurchaseType($olddata["dailyexpense_purchase_type"]);
                                    }
                                    $bank = Bank::model()->findByPk($olddata["bank_id"]);
                                    
                                }else if($type != 5){
                                    $paid =$olddata["paidamount"];
                                    $amount= $olddata["amount"];
                                    $bank = Bank::model()->findByPk($olddata["bank"]);
                                    $gsttotal = $olddata['sgst_amount'] + $olddata['cgst_amount'] + $olddata['igst_amount'];
                                    
                                }

                                //expense head 
                                if ($type == 4) { 
                                    if ($olddata->dailyexpense_type == "deposit") {
                                        $depositId = $olddata->expensehead_id;
                                        $deposit   = Deposit::model()->findByPk($depositId);
                                        $oldthead = "Deposit - " . $deposit["deposit_name"];
                                    } else if ($olddata->dailyexpense_type == "expense") {
                                        $expense   = Companyexpensetype::model()->findByPk($olddata->exp_type_id);
                                        $oldthead = "Expense - " . $expense['name'];
                                    } else {
                                        $receipt   = Companyexpensetype::model()->findByPk($olddata->exp_type_id);
                                        $oldthead = "Receipt - " . $receipt['name'];
                                    }  
                                }
                                
                                ?>

                                <th>OLD</th>
                                <td class="text-center"><?php echo $company['name'] ?></td>
                                <?php if ($type == 4) { ?>
                                <td class="text-center"><?php echo $oldthead ?></td>
                                <?php } ?>
                                <?php if ($type == 3 || $type == 5) { 
                                    $oldsubcontractor = Subcontractor::model()->findByPk($olddata['subcontractor_id']);
                                    ?>
                                <td class="text-center"><?php echo $oldsubcontractor['subcontractor_name'] ?></td>
                                <?php } ?>
                                <?php if ($type != 4 || $type == 5) { ?>
                                <td class="text-center"><?php echo $oldproject["name"] ?></td>
                                <?php } ?>
                                <?php if ($type == 2 || $type==1) {
                                $oldvendor = Vendors::model()->findByPk($olddata["vendor_id"]);
                                ?>
                                <td class="text-center"><?php echo $oldvendor["name"] ?></td>
                                <?php }?>
                                <?php if ($type == 2) { 
                                $oldpurchase = Purchase::model()->findByPk($olddata["purchase_id"]);
                                ?>
                                    <td class="text-center">
                                        <?php echo $oldpurchase["purchase_no"] ?>
                                    </td>
                                <?php } ?>
                                <?php if ($type == 1) { 
                                    $exphead = ExpenseType::model()->findByPk($olddata["exptype"]);
                                    ?>
                                    <td class="text-center"><?php echo $olddata["bill_id"]; ?></td>
                                    <td class="text-center"><?php echo $olddata["invoice_id"]; ?></td>
                                    <td class="text-center"><?php echo $olddata["return_id"] ?></td>                                    
                                    <td class="text-center"><?php echo $user->first_name . ' ' . $user->last_name; ?></td>
                                <?php } ?>
                                <?php if ($type == 5) { 
                                    $exphead = ExpenseType::model()->findByPk($olddata["expensehead_id"]);
                                }
                                ?>
                                <?php if ($type == 1 || $type==5) { ?>
                                <td class="text-center"><?php echo isset($exphead) ? $exphead->type_name : ""; ?></td>
                                <?php } ?>
                                <?php if ($type != 5) { ?>
                                <td class="text-center"><?php echo $expense_type ?></td>

                                <td class="text-center"><?php echo $bank["bank_name"] ; ?></td>
                                <td class="text-center"><?php echo (($type == 4) ? $olddata["dailyexpense_chequeno"]: $olddata["cheque_no"]); ?></td>
                                <td class="text-center"><?php echo $olddata["description"] ?></td>
                                <td class="text-center"><?php echo $amount; ?></td>
                                <td class="text-center"><?php echo (($type == 1 || $type == 4) ? $gsttotal : $olddata["tax_amount"]); ?></td>
                                <td class="text-center"><?php echo $paid; ?></td>
                                <?php } ?>

                                <?php if ($type == 5) { ?>
                                <th class="text-center"><?php echo  $olddata->amount;?></th>
                                <?php }  ?>
                                <?php if ($type == 1 || $type == 2 || $type == 3) { ?>
                                <td class="text-center"><?php echo (($type == 1) ? $olddata["expense_tds"] :$olddata["tds_amount"]); ?></td>
                                <?php } ?>
                                <?php if ($type == 1 || $type==4) { ?>
                                    <td class="text-center"><?php echo $olddata["amount"]; ?></td> 
                                    <td class="text-center"><?php echo $oldreceipt;?></td>                                    
                                    <td class="text-center"><?php echo $purchaseType ?></td>
                                <?php } ?>
                                
                                <?php if ($type == 1) { ?>                                    
                                    <td class="text-center"><?php echo $receiptType; ?></td>                                    
                                    <td class="text-center"><?php echo $olddata["paidamount"] ?></td>
                                <?php } ?>      
                                <?php if ($type == 4) { ?>  
                                    <td><?php echo $employee; ?></td>
                                <?php } ?>   

                        </tr>
                        <tr>
                            <th>NEW</th>
                            <?php
                                $gsttotal = "";
                                $purchaseType = "";
                                $employee = '';
                                $vendorName = '';
                                $purchaseNo = '';
                                $company = Company::model()->findByPk($data['company_id']);
                                 if ($type != 4) { 
                                $project = Projects::model()->findByPk($data['projectid']);
                                 }
                                 if ($type != 5){ 
                                $paymentType = ApproveRequestHelper::getPaymentType($data["payment_type"]);
                                $receiptType =  $paymentType['receiptType'];
                                $expense_type =  $paymentType['expense_type'];
                                 }
                                if(isset($data['vendor_id'])){
                                    $vendor = Vendors::model()->findByPk($data['vendor_id']);
                                    $vendorName = $vendor["name"];
                                }
                                if(isset($data['purchase_id'])){
                                    $purchase = Purchase::model()->findByPk($data['purchase_id']);
                                    $purchaseNo = $purchase["purchase_no"];
                                }                            
                                 
                                //expense head 
                                if ($type == 4) { 
                                    $gsttotalnew = $data['dailyexpense_sgst'] + $data['dailyexpense_cgst'] + $data['dailyexpense_igst'];
                                   
                                    if ($data["dailyexpense_type"] == "deposit") {
                                        $depositId = $data["expensehead_id"];
                                        $deposit   = Deposit::model()->findByPk($depositId);
                                        $thead = "Deposit - " . $deposit["deposit_name"];
                                    } else if ($data["dailyexpense_type"] == "expense") {
                                        $expense   = Companyexpensetype::model()->findByPk($data["exp_type_id"]);
                                        $thead = "Expense - " . $expense['name'];
                                    } else {
                                        $receipt   = Companyexpensetype::model()->findByPk($data["exp_type_id"]);
                                        $thead = "Receipt - " . $receipt['name'];
                                    }  
                                }                          
                            ?>
                            <td class="text-center"><?php echo $company["name"] ?></td>
                            <?php if ($type == 4) { ?>
                            <td class="text-center"><?php echo $thead ?></td>
                            <?php } ?>
                            <?php  if ($type == 2) { 
                            $gsttotalnew = $data['sgst_amount'] + $data['cgst_amount'] + $data['igst_amount'];
                            } ?>
                            <?php  if ($type == 5) { 
                                $subcontractor = Subcontractor::model()->findByPk($data['subcontractor_id']);
                            }
                            ?>
                            <?php  if ($type == 3) {
                            $gsttotalnew = $data['sgst_amount'] + $data['cgst_amount'] + $data['igst_amount'];
                            } ?>
                            <?php  if ($type == 3 || $type==5) { 

                                $subcontractor = Subcontractor::model()->findByPk($data['subcontractor_id']);
                                
                                ?>
                                <td class="text-center <?php echo isset($oldsubcontractor['subcontractor_name']) ? (($oldsubcontractor['subcontractor_name'] != $subcontractor['subcontractor_name']) ? "bg_warning" : "bg_none") : "" ?>"><?php echo $subcontractor['subcontractor_name'] ?></td>                            
                            <?php } ?>

                            <?php if ($type != 4 || $type==5) { ?>
                                <td class="text-center <?php echo isset($oldproject["name"]) ? (($oldproject["name"] != $project["name"]) ? "bg_warning" : "bg_none") : "" ?>"><?php echo $project["name"] ?></td>
                            <?php } ?>

                            <?php if ($type == 2 || $type==1) {  ?>
                                <td class="text-center <?php echo isset($oldvendor["name"]) ? (($oldvendor["name"] != $vendorName) ? "bg_warning" : "bg_none") : "" ?>">
                                <?php echo $vendorName ?>
                                </td>
                            <?php } ?>
                            <?php if ($type == 2) { ?>
                                <td class="text-center <?php echo ($oldpurchase["purchase_no"] != $purchaseNo) ? "bg_warning" : "bg_none" ?>">
                                <?php echo $purchaseNo ?>
                                </td>
                            <?php } ?>
                            <?php if ($type == 1) { 
                                $gsttotalnew = $data['expense_sgst'] + $data['expense_cgst'] + $data['expense_igst'];
                                
                                ?>
                                <td class="text-center <?php echo isset($olddata["bill_id"]) ? (($olddata["bill_id"] != $data["bill_id"]) ? "bg_warning" : "bg_none") : "" ?>"><?php echo $data["bill_id"] ?></td>

                                <td class="text-center <?php echo isset($olddata["invoice_id"]) ? (($olddata["invoice_id"] != $data["invoice_id"]) ? "bg_warning" : "bg_none") : "" ?>"><?php echo $data["invoice_id"] ?></td>

                                <td class="text-center <?php echo isset($olddata["return_id"]) ? (($olddata["return_id"] != $data["return_id"]) ? "bg_warning" : "bg_none") : "" ?>"><?php echo $data["return_id"] ?></td>                                                             
                                <td class="text-center"><?php echo $user->first_name . ' ' . $user->last_name; ?></td>
                            <?php } ?>

                            <?php if ($type == 1 || $type==5) {
                                
                                $exphead = ExpenseType::model()->findByPk($data["exptype"]);
                                ?>
                            <td class="text-center <?php echo isset($olddata["exptype"]) ? (($olddata["exptype"] != $data["exptype"]) ? "bg_warning" : "bg_none") : "" ?>"><?php echo isset($data["exptype"]) ? $exphead->type_name : ""; ?></td> 
                            <?php } ?>
                            <?php if ($type != 5) { ?>
                            <td class="text-center"><?php echo $expense_type; ?></td>
                            <?php 
                                $newbank ="";
                                if($type ==1){
                                    $bank = isset($olddata["bank_id"]) ?$olddata["bank_id"]:"";                                                                      
                                    $expense_amount = isset($olddata["expense_amount"])?$olddata["expense_amount"] :"";
                                    $tds = isset($olddata["expense_tds"])?$olddata["expense_tds"] :"";
                                    $paid = isset($olddata["paid"])?$olddata["paid"] :"";
                                    $oldreceipt  = $olddata["receipt"];
                                    if (isset($data['purchase_type'])) {
                                        $purchaseType = ApproveRequestHelper::getPurchaseType($data['purchase_type']);
                                    } 
                                    $newbank = Bank::model()->findByPk($data["bank_id"]) ;
                                }else if($type ==2){

                                    $bank = isset($olddata["bank"]) ?$olddata["bank"]:"";                                    
                                    $expense_amount = isset($olddata["amount"])?$olddata["amount"] :"";
                                    $tds = isset($olddata["tds_amount"])?$olddata["tds_amount"] :"";
                                    $paid = isset($olddata["paidamount"])?$olddata["paidamount"] :"";
                                    $newbank = Bank::model()->findByPk($data["bank"]);

                                }else if($type ==4){

                                    $bank = isset($olddata["bank_id"]) ?$olddata["bank_id"]:"";    
                                    $newbank = Bank::model()->findByPk($data["bank_id"]) ;
                                    $expense_amount = isset($olddata["dailyexpense_amount"])?$olddata["dailyexpense_amount"] :"";
                                    $oldreceipt  = $olddata["dailyexpense_receipt"];
                                    if ($data["employee_id"] != '') {
                                        $users = Users::model()->findByPk($data["employee_id"]);
                                        $employee = $users->first_name . ' ' . $users->last_name;
                                    } 
                                    if (isset($data['dailyexpense_purchase_type'])) {
                                        $purchaseType = ApproveRequestHelper::getPurchaseType($data['dailyexpense_purchase_type']);
                                    } 
                                }else{
                                    $bank = isset($olddata["bank"]) ?$olddata["bank"]:"";    
                                    $newbank = Bank::model()->findByPk($data["bank"]) ;
                                    $expense_amount = isset($olddata["amount"])?$olddata["amount"] :"";
                                }
                                
                            ?>


                            <td class="text-center <?php echo isset($bank) ? (($bank != $data["bank"]) ? "bg_warning" : "bg_none") : "" ?>">
                            <?php echo $newbank['bank_name']; ?>
                            </td>
                            
                            <td class="text-center <?php echo isset($olddata["cheque_no"]) ? (($olddata["cheque_no"] != $data['cheque_no']) ? "bg_warning" : "bg_none") : "" ?>">
                            <?php echo $data["cheque_no"] ?>
                            </td>

                            <td class="text-center <?php echo isset($olddata["description"]) ? (($olddata["description"] != $data['description']) ? "bg_warning" : "bg_none") : "" ?>">
                            <?php echo $data["description"] ?>
                            </td>

                            <td class="text-center <?php echo isset($expense_amount) ? (($expense_amount != $data['expense_amount']) ? "bg_warning" : "bg_none") : "" ?>">
                            <?php echo $data["expense_amount"]; ?>
                            </td>
                             
                            <td class="text-center <?php echo isset($gsttotal) ? (($gsttotal != $gsttotalnew) ? "bg_warning" : "bg_none") : "" ?>"><?php echo $gsttotalnew; ?></td>
                            <td class="text-center <?php echo isset($paid) ? (($paid != $data['paid']) ? "bg_warning" : "bg_none") : "" ?>">
                            <?php echo $data["paid"] ?>
                            </td>
                            <?php } ?>
                            <?php if ($type == 5) { ?>
                                <th class="text-center <?php echo isset( $olddata['amount']) ? (($olddata['amount'] != $data['amount']) ? "bg_warning" : "bg_none") : "" ?>"><?php echo $data['amount'];?></th>
                            <?php }  ?>
                            <?php if ($type == 1 || $type == 2 || $type == 3) { ?>
                                <td class="text-center <?php echo isset( $tds) ? (( $tds != $data['tds_amount']) ? "bg_warning" : "bg_none") : "" ?>">
                                <?php echo $data["tds_amount"] ?>
                                </td>
                            <?php } ?>

                            <?php if ($type == 1 || $type==4) { ?>
                                <td class="text-center <?php echo isset($olddata["amount"]) ? (($olddata["amount"] != $data['amount']) ? "bg_warning" : "bg_none") : "" ?>"><?php echo $data["amount"] ?></td>     
                                <td class="text-center <?php echo isset($oldreceipt) ? (($oldreceipt != $data['receipt']) ? "bg_warning" : "bg_none") : "" ?>"><?php echo $data["receipt"] ?></td>                             
                                <td class="text-center"><?php echo $purchaseType ?></td>
                            <?php } ?>
                            

                            <?php if ($type == 1) { ?>                                                  
                                <td><?php echo $receiptType; ?></td>
                                <td class="text-center <?php echo isset($olddata["paidamount"]) ? (($olddata["paidamount"] != $data['paidamount']) ? "bg_warning" : "bg_none") : "" ?>"><?php echo $data["paidamount"] ?></td>
                            <?php } ?>

                            <?php if ($type == 4) { ?>  
                                <td><?php echo $employee; ?></td>
                            <?php } ?>  
                        </tr>


                        </tdead>
                </table>
            </div>
        </div>
    </div>
    <?php
    }
}
?>
