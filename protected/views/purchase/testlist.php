<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/message/css/jquery.message.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/message/js/jquery.message.js"></script>
<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$this->breadcrumbs = array(
  'Purchase List',
)
?>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/tableHeadFixer.js"></script>
<div class="container" id="project">
  <div class="clearfix">
    <h2 class="pull-left">Purchase</h2>
    <div class="add-btn pull-right">
      <?php
      if (isset(Yii::app()->user->role) && (in_array('/purchase/addpurchase', Yii::app()->user->menuauthlist))) {
      ?>
        <a href="index.php?r=purchase/addpurchase" class="button">Add Purchase</a>
      <?php } ?>
    </div>
  </div>

  <?php
  $user = Users::model()->findByPk(Yii::app()->user->id);
  $arrVal = explode(',', $user->company_id);
  $newQuery = "";
  foreach ($arrVal as $arr) {
    if ($newQuery) $newQuery .= ' OR';
    $newQuery .= " FIND_IN_SET('" . $arr . "', company_id)";
  }


  //$total_billedamount = 0;
  //$total_blanceamount = 0;

  $tblpx    = Yii::app()->db->tablePrefix;

  /*if(isset($_GET['project_id']) || isset($_GET['vendor_id']) || isset($_GET['purchase_no']) || isset($_GET['purchase_status']) || isset($_GET['date_from']) || isset($_GET['date_to'])) 
{
	foreach($dataProvider->getData() as $record) {
		
		 $value = Yii::app()->db->createCommand("SELECT SUM(bill_totalamount) as bill_totalamount  FROM `{$tblpx}bills` WHERE purchase_id=".$record->p_id)->queryRow();
		 if($value['bill_totalamount'] !=''){
			 $bill_totalamount = $value['bill_totalamount'];
		 } else {
			$bill_totalamount = 0.00;	 
		 }
		 $total_billedamount  += $bill_totalamount;
		
			
			
			 $blnvalue = array();
		  $bill_item = array();
		  $max = array();
		  $blanc_amount = '';
		  $blnvalue[] = Yii::app()->db->createCommand("SELECT item_id, quantity, rate, purchase_id FROM {$tblpx}purchase_items WHERE purchase_id=".$record['p_id'])->queryAll();
		  if(!empty($blnvalue)) {
			  foreach($blnvalue as $values) {
						
						
						 foreach($values as $t_data) {
							$bill_item = Yii::app()->db->createCommand("SELECT sum(billitem_quantity) as billitem_quantity FROM {$tblpx}billitem WHERE purchaseitem_id=".$t_data['item_id'])->queryRow();	
								if(!empty($bill_item)){
								 
								   $blance_quantity = $t_data['quantity']-$bill_item['billitem_quantity'];
									  $max = $blance_quantity*$t_data['rate'];
									 $total_blanceamount += $max; 
								  
								  
								} else {
									 if($record['purchase_billing_status'] == 93) {
										 $total_blanceamount += 0.00;
									 } else {
									 $total_blanceamount += $t_data['quantity']*$t_data['rate'];
								    } 
								}  
							
							
							
						}
						
						
						
				}
			}
				
	 }
 
} else {
	
        $datefrom = date("Y-")."01-" . "01";
        $date_to = date("Y-m-d");
        $query = " AND purchase_date >= '".date('Y-m-d',strtotime($datefrom))."' AND purchase_date <= '".date('Y-m-d',strtotime($date_to))."'"; 
    
	$data = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}purchase WHERE (".$newQuery.") AND purchase_no IS NOT NULL ".$query." ")->queryAll();

	 foreach($data as $record) {
		 
		 $tblpx 	 = Yii::app()->db->tablePrefix;
		 $value = Yii::app()->db->createCommand("SELECT SUM(bill_totalamount) as bill_totalamount  FROM `{$tblpx}bills` WHERE purchase_id=".$record['p_id'])->queryRow();
		 if($value['bill_totalamount'] !=''){
			 $bill_totalamount = $value['bill_totalamount'];
		 } else {
			$bill_totalamount = 0.00;	 
		 }
		 $total_billedamount  += $bill_totalamount;
		 
		  
		  $blnvalue = array();
		  $bill_item = array();
		  $max = array();
		  $blanc_amount = '';
		  $total = 0;
		  $blnvalue[] = Yii::app()->db->createCommand("SELECT item_id, quantity, rate, purchase_id FROM {$tblpx}purchase_items WHERE purchase_id=".$record['p_id'])->queryAll();
		  if(!empty($blnvalue)) {
			  foreach($blnvalue as $values) {
				  foreach($values as $t_data) {
							$bill_item = Yii::app()->db->createCommand("SELECT sum(billitem_quantity) as billitem_quantity FROM {$tblpx}billitem WHERE purchaseitem_id=".$t_data['item_id'])->queryRow();	
								if(!empty($bill_item)){
								 
								   $blance_quantity = $t_data['quantity']-$bill_item['billitem_quantity'];
									  $max = $blance_quantity*$t_data['rate'];
									 $total_blanceamount += $max; 
								  
								  
								} else {
									 if($record['purchase_billing_status'] == 93) {
										 $total_blanceamount += 0.00;
									 } else {
									 $total_blanceamount += $t_data['quantity']*$t_data['rate'];
								    } 
								}  
							
							
							
						}
						
				}
			}
		  
		  
		  
		 
      }
	
	
}*/
  ?>

  <?php
  $this->renderPartial('_newsearch', array('model' => $model, 'project' => $project, 'vendor' => $vendor, 'purchase_no' => $purchase_no, 'expense' => $expense)) ?>

  <?php if (Yii::app()->user->hasFlash('success')) : ?>
    <div class="info" style="width: 500px;margin-left: 400px;padding: 8px;color: green;font-size: 20px;font-weight: bold; padding-top: 46px;">
      <?php echo Yii::app()->user->getFlash('success'); ?>
    </div>
  <?php endif; ?>


  <?php if (Yii::app()->user->hasFlash('error')) : ?>
    <div class="info" style="width: 500px;margin-left: 400px;padding: 8px;color: red;font-size: 20px;font-weight: bold; padding-top: 46px;">
      <?php echo Yii::app()->user->getFlash('error'); ?>
    </div>
  <?php endif; ?>


  <div id="msg_box"></div>
  <div class="exp-list">
    <?php $this->widget('zii.widgets.CListView', array(
      'dataProvider' => $dataProvider,
      'id' => 'VideoList',
      'viewData' => array('total_billedamount' => $total_billedamount, 'total_blanceamount' => $total_blanceamount),
      'ajaxUpdate' => true,
      'itemView' => '_testview', 'template' => '<div class="clearfix"><div class="pull-right">{summary}</div><div class="pull-left">{sorter}</div></div><div id="infinite_scroll"><table cellpadding="10" class="table" id="fixtable">{items}</table>{pager}</div>',
      'emptyText' => '<table cellpadding="10" class="table"><thead>
            <tr>
                <th>No</th>
                <th>Purchase No</th>
                <th>Company</th>
                <th>Project</th>
                <th>Expense Head</th>
                <th>Vendor</th>            
                <th>Total Amount</th>
                <th>Date</th>
                <th>Status</th>
                <th>Total amount billed</th>
                <th>Balance to be billed</th>
                <th>Billing Status</th>
                <th></th>
             </tr></thead><tr><td colspan="13" class="text-center">No results found</td></tr></table>',
      'pager' => array(
        'class' => 'ext.yiinfinite-scroll.YiinfiniteScroller',
        'contentSelector' => '#fixtable',
        'itemSelector' => 'table tbody',
        'loadingText' => 'Loading...',
      ),


    ));
    //CHtml::asset(Yii::getPathOfAlias('ext.yiinfinite-scroll.assets').'/infinitescroll.js');  
    ?>


  </div>
</div>
<script type="text/javascript">
  $(document).ready(function() {
    // alert($(window).height()+"-"+$("#infinite_scroll").height());
    $("#infinite_scroll").css('max-height', $(window).height() - $("#myNavbar").height() - $(".search-form").height() - ($('footer').height()));
    //  $('#infinite_scroll').scroll(function(){

    //  $('#fixtable').infinitescroll({'loadingText':'Loading...','itemSelector':'table tbody','navSelector':'div.infinite_navigation','nextSelector':'div.infinite_navigation a:first','bufferPx':'300'});
    //});
  });
</script>
<style>
  a.button {
    background-color: #6a8ec7;
    display: block;
    padding: 5px;
    color: #fff;
    cursor: pointer;
    float: right;
    border: 1px solid #6a8ec8;
  }

  .bill_class {
    background: #FF6347 !important;
  }

  .exp-list {
    padding: 0px;
  }

  .list-view .sorter li {
    margin: 0px;
  }

  #infinite_scroll {
    max-height: 350px;
    overflow-y: scroll;
  }

  #fixtable thead th,
  #fixtable tfoot th {
    background-color: #eee;
  }

  .table {
    margin-bottom: 0px;
  }

  .highlight {
    background-color:
      #DD1035;
    color: #fff;
  }

  .tooltip-hiden {
    width: auto;
  }

  .infinite_navigation {
    display: none;
  }

  .list-view .pager {
    margin-top: 0px;
  }

  .table>thead>tr>th,
  .table>tbody>tr>th,
  .table>tfoot>tr>th,
  .table>thead>tr>td,
  .table>tbody>tr>td,
  .table>tfoot>tr>td {
    border-bottom: 0px;
  }
</style>
<script>
  jQuery.browser = {};
  (function() {
    jQuery.browser.msie = false;
    jQuery.browser.version = 0;
    if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
      jQuery.browser.msie = true;
      jQuery.browser.version = RegExp.$1;
    }
  })();
</script>

<script>
  var url = '<?php echo  Yii::app()->getBaseUrl(true); ?>';

  $(document).ready(function() {
    $("#fixtable").tableHeadFixer({
      'left': false,
      'foot': true,
      'head': true
    });


    $(".popover-test").popover({
      html: true,
      content: function() {
        return $(this).next('.popover-content').html();
      }
    });
    $('[data-toggle=popover]').on('click', function(e) {
      $('[data-toggle=popover]').not(this).popover('hide');
    });
    $('body').on('hidden.bs.popover', function(e) {
      $(e.target).data("bs.popover").inState.click = false;
    });
    $('body').on('click', function(e) {
      $('[data-toggle=popover]').each(function() {
        // hide any open popovers when the anywhere else in the body is clicked
        if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
          $(this).popover('hide');
        }
      });
    });
    $(document).ajaxComplete(function() {
      $(".popover-test").popover({
        html: true,
        content: function() {
          return $(this).next('.popover-content').html();
        }
      });
      $("#fixtable").tableHeadFixer({
        'left': false,
        'foot': true,
        'head': true
      });

    });

    $(document).on('click', '.update_status', function(e) {
      e.preventDefault();
      var element = $(this);
      var purchase_id = $(this).attr('id');
      $.ajax({
        url: '<?php echo  Yii::app()->createAbsoluteUrl('purchase/updatepurchasestatus'); ?>',
        type: 'GET',
        dataType: 'json',
        data: {
          purchase_id: purchase_id
        },
        success: function(response) {
          console.log(response);
          if (response.response == 'success') {
            $(".purchase_status_" + purchase_id).html('Saved');
            $(".edit_option_" + purchase_id).hide();
            element.closest('tr').find('.bill_label').text('Pending to be Billed');
            element.closest('tr').find('.bill_status').addClass('bill_class');
            $("#msg_box").html('<div class="alert alert-success alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><strong> Success!</strong>  &nbsp;' + response.msg + '</div>');
          } else {

            $("#msg_box").html('<div class="alert alert-warning alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><strong> Sorry!</strong>  &nbsp;' + response.msg + '</div>');
          }
        }
      });
    });


    $('.view_purchase').click(function() {
      var val = $(this).attr('id');
      location.href = url + '/index.php?r=purchase/viewpurchase&pid=' + val;
    })
  });

  $('.edit_purchase').click(function() {
    var val = $(this).attr('id');
    location.href = url + '/index.php?r=purchase/updatepurchase&pid=' + val;
  })


  $(document).on('click', '.edit_purchase', function() {
    var val = $(this).attr('id');
    location.href = url + '/index.php?r=purchase/updatepurchase&pid=' + val;
  })

  $(document).on('click', '.view_purchase', function() {
    var val = $(this).attr('id');
    location.href = url + '/index.php?r=purchase/viewpurchase&pid=' + val;
  })


  $(document).on('click', '.permission_item', function(e) {
    e.preventDefault();
    var element = $(this);
    var item_id = $(this).attr('id');
    $.ajax({
      url: '<?php echo  Yii::app()->createAbsoluteUrl('purchase/permission'); ?>',
      type: 'POST',
      dataType: 'json',
      data: {
        item_id: item_id
      },
      success: function(response) {
        $(".popover").removeClass("in");
        if (response.response == 'success') {
          element.closest('tr').removeClass('permission_style');
          $(".approveoption_" + item_id).hide();
          $().toastmessage('showSuccessToast', "" + response.msg + "");
          $('.approvallabel_' + item_id).html('Draft');
          $('.approvallabel_' + item_id).css("cursor", "pointer");
          $('.approvallabel_' + item_id).addClass('update_status');
        } else if (response.response == 'warning') {
          $(".approveoption_" + item_id).hide();
          element.closest('tr').removeClass('permission_style');
          $().toastmessage('showWarningToast', "" + response.msg + "");
        } else {
          $().toastmessage('showErrorToast', "" + response.msg + "");
        }
      }
    });
  });

  $(document).ajaxComplete(function() {
    $('.loading-overlay').removeClass('is-active');
    $('#loading').hide();
  });
</script>

<style>
  .toast-container {
    width: 350px;
  }

  .toast-position-top-right {
    top: 57px;
    right: 6px;
  }

  .toast-item-close {
    background-image: none;
    cursor: pointer;
    width: 12px;
    height: 12px;
    text-align: center;
    border-radius: 2px;
  }

  .toast-item-image {
    font-size: 24px;
  }

  .toast-item-close:hover {
    color: red;
  }

  .toast-item {
    border: transparent;
    border-radius: 3px;
    font-size: 10px;
    opacity: 1;
    background-color: rgba(34, 45, 50, 0.8);
  }

  .toast-item-wrapper p {
    margin: 0px 5px 0px 42px;
    font-size: 14px;
    text-align: justify;
  }

  .toast-type-success {
    background-color: #00A65A;
    border-color: #00A65A;
  }

  .toast-type-error {
    background-color: #DD4B39;
    border-color: #DD4B39;
  }

  .toast-type-notice {
    background-color: #00C0EF;
    border-color: #00C0EF;
  }

  .toast-type-warning {
    background-color: #F39C12;
    border-color: #F39C12;
  }
</style>