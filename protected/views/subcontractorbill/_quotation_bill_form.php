<?php
/* @var $this SubcontractorbillController */
/* @var $model Subcontractorbill */
/* @var $form CActiveForm */

?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'subcontractorbill-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>true,
        'clientOptions'=>array(
            'validateOnSubmit'=>true,
         ),
)); ?>
    <!--ui change -->
    <div class="row">
        <div class="form-group col-xs-12 col-md-3">
                <div class="form-group">
            <?php echo $form->labelEx($model,'company_id'); ?>
            <?php       
                $model->company_id = $quotation_model['company_id'];
                $company_model = Company::model()->findByPk($model->company_id);
                ?>
            <input type="text" name="company_id" value="<?= $company_model->name; ?>" class="form-control" readonly="readonly">
            <input type="hidden" name="Subcontractorbill[company_id]" value="<?= $model->company_id; ?>" class="form-control" >
                    
            <?php echo $form->error($model,'company_id'); ?>
        </div>
        <div class="form-group col-xs-12 col-md-3">
            
                <label for="Subcontractorbill_project" class="required projectlabel">Project <span class="required">*</span></label>
              
                        <input type="text" name="project_id" value="<?= $quotation_model->projects->name; ?>" class="form-control" readonly="readonly">
                   
               
                <div class="errorMessage" id="projectErrorMsg"></div>
           
        </div>
       <div class="form-group col-xs-12 col-md-3">
           
                    <?php echo $form->labelEx($model,'scquotation_id'); ?>
                
                            <input type="text" name="scquotation_id"  value="<?= $quotation_model->scquotation_no; ?>" class="form-control" readonly="readonly">
                            <input type="hidden" name="Subcontractorbill[scquotation_id]"  value="<?= $quotation_model->scquotation_id; ?>" class="form-control" >
                    
                
                    <?php echo $form->error($model,'scquotation_id'); ?>
            
        </div>
        <div class="form-group col-xs-12 col-md-3">
           
            <?php echo $form->labelEx($model,'bill_number'); ?>
            <?php echo $form->textField($model,'bill_number',array('size'=>60,'maxlength'=>100,'class' => 'form-control')); ?>
            <?php echo $form->error($model,'bill_number'); ?>
           
	    </div>
        <div class="form-group col-xs-12 col-md-3">
            
            <?php echo $form->labelEx($model,'date'); ?>
            <?php echo $form->textField($model,'date', array('class' => 'form-control','autocomplete'=>'off')); ?>
            <?php echo $form->error($model,'date'); ?>
            
	    </div>
        
    </div>
    <!--ui change ends-->
        
        
	
  

<?php $this->endWidget(); ?>

</div><!-- form -->
<script type="text/javascript">
$(document).ready(function() {   
    $(function () {
        $("#Subcontractorbill_date").datepicker({dateFormat: 'dd-mm-yy'}).datepicker("setDate", new Date());
    });
    $("#Subcontractorbill_bill_number").change(function() {
         var bill_no = $("#Subcontractorbill_bill_number").val();
         var bill_date = $('#Subcontractorbill_date').val();
         if( bill_no != '' && bill_date != ''){
             $('#subcontractorbill-form').submit();
         }
    });
    $("#Subcontractorbill_date").change(function() {
         var bill_no = $("#Subcontractorbill_bill_number").val();
         var bill_date = $('#Subcontractorbill_date').val();
         if( bill_no != '' && bill_date != ''){
             $('#subcontractorbill-form').submit();
         }
    });
  
  
    $("#submitButton").click(function() {
      // alert('dsfdf');
        var project_id          = $("#project_id").val();
        var subcontractor_id    = $("#subcontractor_id").val();
        if(project_id == "") {
            $("#projectErrorMsg").text("Project cannot be blank.");
            $(".projectlabel").css("color", "red");
        }
        if(subcontractor_id == "") {
            $("#subcontractorErrorMsg").text("Subcontractor cannot be blank.");
            $(".subcontractorlabel").css("color", "red");
        }
    });
});
$('#reset').on('click', function()
    {
      $("#Subcontractorbill_company_id").val('').trigger('change');
			$("#project_id").val('').trigger('change');
			$("#subcontractor_id").val('').trigger('change');
			$("#Subcontractorbill_scquotation_id").val('').trigger('change');
			$("#Subcontractorbillitem_quotationitem_id").val('').trigger('change');
    });
    $("#Subcontractorbill_bill_number").change(function() {
            $('#loading').show();
            var bill_number = $("#Subcontractorbill_bill_number").val();
           $.ajax({
                url: "<?php echo $this->createUrl('subcontractorbill/billnumberexistance') ?>",
                data: {
                    "bill_number": bill_number,
                },
                type: "POST",
                dataType:"json",
                success: function(data) {
                    if(data>0){
                        if (confirm("Bill number already exist.Do you want to continue?")) {
                            $("#Subcontractorbill_bill_number").val(bill_number);
                        }else{
                            $("#Subcontractorbill_bill_number").val('');
                        }
                    }
                   
                }
            });
        });

</script>
