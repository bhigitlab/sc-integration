<?php
if (Yii::app()->user->role != 1) {
    $warehouse = Warehouse::model()->findAll(array('condition' => 'FIND_IN_SET(' . Yii::app()->user->id . ', assigned_to)'));
} else {
    $warehouse = Warehouse::model()->findAll();
}
$vendor = Vendors::model()->findAll();
$user = Users::model()->findAll();
?>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.js"></script>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/message/css/jquery.message.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/message/js/jquery.message.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/tableHeadFixer.js"></script>
<script>
    var shim = (function () {
        document.createElement('datalist');
    })();
</script>

<script>
    $(function () {
        $("#datepicker").datepicker({dateFormat: 'dd-mm-yy'}).datepicker("setDate", new Date());
        var date3 = $('#datepicker').datepicker('getDate');
        date3.setDate(date3.getDate() + 1);
        $("#datepicker_eta").datepicker({dateFormat: 'dd-mm-yy'}).datepicker("setDate", date3);
    });

</script>
<?php $general_settings_for_auto_receipt = Controller::generalSettingsForAutoReceiptToProjectWarehouse($settings = 1);?>
<div class="container">
    <div class="invoicemaindiv" >        
        <div class='sub-heading purchase-title items_separator '>
                <h3 class="">Add Warehouse Despatch</h3>
                <?php echo CHtml::Button('Back', array('class' => 'btn btn-info btn-sm', 'onclick' => 'javascript:location.href="' . $this->createUrl('index') . '"')); ?>
        </div>
        <div id="msg_box"></div>
        <form id="warehousedespatch_form" method="post" action="<?php echo $this->createAbsoluteUrl('warehousedespatch/saveDespatchOrAutoReceipt'); ?>"  >
        <?php echo CHtml::hiddenField('remove', '', array('id' => "remove")); ?>
        <?php echo CHtml::hiddenField('warehousedespatch_id', 0, array('id' => "warehousedespatch_id")); ?>
        <?php echo CHtml::hiddenField('item_unit_id_hidden','', array('id' => "item_unit_id_hidden")); ?>
        <?php echo CHtml::hiddenField('item_unit_batch_hidden',0, array('id' => "item_unit_batch_hidden")); ?>
        <?php echo CHtml::hiddenField('availableQuantity[]',0, array('id' => "availableQuantity",'class'=>"inputs target txtBox availableQuantity allownumericdecimal",'placeholder'=>"")); ?>
        <div class="entries-wrapper">
                  <div class="row">
                    <div class="col-xs-12">
                      <div class="heading-title">Details</div>
                      <div class="dotted-line"></div>
                    </div>
                  </div>
                  <div class="row">
                        <div class="col-xs-12 col-sm-4 col-md-3 form-group col-lg-2">
                            <div class="form-group">
                                <label>WAREHOUSE FROM <span class="required">*</span></label>
                                <?php
                                $warehouse_from = '';
                                if(yii::app()->user->role == 5){
                                $warehouse_data = Controller::getCategoryOptions($type=1);
                                $data = CHtml::listData(Controller::getCategoryOptions($type=1),'id','text','group');
                                }else{
                                $warehouse_data = Controller::getCategoryOptions();
                                $data = CHtml::listData(Controller::getCategoryOptions(),'id','text','group');
                                }
                                if(count($warehouse_data) == 1){
                                $warehouse_selected = $warehouse_data[0]['id'];
                                }else{
                                $warehouse_selected = '';
                                }
                                echo CHtml::dropDownList('warehouse', $warehouse_selected,
                                $data,
                                array('empty' => 'Choose Warehouse', 'class' => 'inputs target warehouse warehouse-from'));
                                ?>                        
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-4 col-md-3 form-group col-lg-2">
                            <div class="form-group">
                            <label class="text-nowrap margin-right-5 desktop-display-block">WAREHOUSE TO <span class="required">*</span></label></label>
                            <?php
                            $warehouse_to = '';
                            $data = CHtml::listData(Controller::getCategoryOptions(), 'id', 'text', 'group');
                            echo CHtml::dropDownList('warehouse_to', $warehouse_to,
                            $data,
                            array('empty' => 'Choose Warehouse', 'class' => 'inputs target warehouse_to form-control'));
                            ?>                        
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-4 col-md-3 form-group col-lg-2">
                            <div class="form-group">
                                <label>RECEIPT CLERK <span class="required">*</span></label>
                                <select name="clerk" class="inputs target clerk"  id="clerk">
                                <?php
                                    if(yii::app()->user->role == 1){?> 
                                        <option value="">Choose Receipt Clerk</option>
                                    <?php
                                        foreach ($user as $key => $value) {
                                            ?>
                                            <option value="<?php echo $value['userid']; ?>"><?php echo $value['first_name'].' '.$value['last_name']; ?></option>
                                            <?php
                                        }
                                    }else{ 
                                        foreach ($user as $key => $value) {
                                            if($value['userid'] == Yii::app()->user->id){
                                    ?>

                                            <option value="<?php echo $value['userid']; ?>"><?php echo $value['first_name'].' '.$value['last_name']; ?></option>
                                <?php		}
                                        }
                            
                                    } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-3 form-group col-lg-2">
                            <div class="form-group">
                                <label>DESPATCH NO <span class="required">*</span></label>
                                <input type="text" value="" id="despatch_no"  name="despatch_no" class="form-control">
                            </div> 
                        </div>  
                        <div class="col-xs-12  col-sm-4 col-md-2">
                            <div class="form-group">
                                <label>DATE <span class="required">*</span></label>
                                <input type="text" value="<?php echo ((isset($date) && $date != '') ? date("d-m-Y", strtotime($date)) : ''); ?>" id="datepicker" class="txtBox date inputs target form-control" name="warehouse_date"  placeholder="Please click to edit" readonly="true">
                            </div> 
                        </div>
                        <div class="col-xs-12  col-sm-4 col-md-2 margin-bottom-16">
                            <div class="form-group">
                                <label>ETA <span class="required">*</span></label>
                                <input type="text" value="<?php echo ((isset($date_eta) && $date_eta != '') ? date("d-m-Y", strtotime($date)) : ''); ?>" id="datepicker_eta" class="txtBox date inputs target form-control" name="warehouse_date"  placeholder="Please click to edit" readonly="true">
                            </div> 
                        </div>
                        <div class="col-xs-12  col-sm-6 col-lg-4">
                            <div class="form-group">
                                <label>REMARKS</label>                            
                                <textarea rows="2" cols="30" name="vendor" id="vendor" class="form-control"></textarea>
                            </div>
                        </div>
                    </div>
            <div id="msg"></div>
            <div id="previous_details" class="prev_details"></div>
            <div class="purchase_items">
                <div class="row">
                    <div class="col-xs-12">
                      <div class="heading-title">Add/Edit Despatch Item</div>
                      <div class="dotted-line"></div>
                    </div>
                </div>                                
                <div class="row">
                    <div class="col-xs-12 col-sm-4 col-md-3 form-group purchaseitem despatch-item">
                        <div id="select_div">
                            <label>Item:</label>
                            <select class="txtBox stock_item form-control" id="stock_item" name="stock_item">
                            <option value="">Select one</option>
                            <?php
                            foreach ($specification as $key => $value) {
                            ?>
                            <option value="<?php echo $value['id']; ?>"><?php echo $value['data']; ?></option>
                            <?php } ?>                               
                            </select>
                        </div>
                        <div>
                            <span id="item_baseunit_data" class="d-none text-blue"><i>Base Unit : </i><span class="txtBox item_unit_span" id="item_unit_additional_data"> </span></span>
                        </div>  
                    </div>

                    <div class="col-xs-12 col-sm-4 col-md-3 form-group purchaseitem dimension_div d-none" style ="display:none">
                        <div id="select_div">
                        <label>Dimension:</label>
                        <select class="txtBox dimension form-control" id="dimension" name="dimension">
                        <option value="">Select Dimension</option>
                        </select>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-4 col-md-3 form-group purchaseitem" >
                        <div id="select_div">
                            <label>Batch:</label>
                            <select  class="txtBox batch form-control" id="batch" name="batch">
                            <option value="">Select batch</option>
                            </select>
                        </div>
                    </div>
                    <!-- <div class="tab-only-clear"></div> -->
                    <div class="col-xs-12 col-sm-4 col-md-3 form-group purchaseitem">
                        <label>Units/Size:</label>
                        <select class="txtBox item_unit form-control" id="item_unit" name="unit" >
                        <option value="">Unit</option>
                        </select>

                        <div id="item_conversion_data" class="d-none text-blue text-right"><i>Conv Fact : </i><span class="txtBox item_unit_span" id="item_unit_additional_conv"> </span></div>
                    </div>

                    <div class="col-xs-12 col-sm-4 col-md-3 form-group purchaseitem quantity_div">
                        <label>Quantity:</label>
                        <input type="text" class="inputs target txtBox quantity allownumericdecimal form-control"  id="quantity" name="quantity[]" placeholder=""/>
                        <div id="available_quantity"></div>
                    </div>

                    <div class="col-xs-12 col-sm-4 col-md-3 form-group purchaseitem quantity_div">
                        <label>Amount:</label>
                        <input type="text" class="inputs target txtBox amount allownumericdecimal form-control" id="amount" name="amount[]" placeholder="" readonly />
                    </div>
                   
                    <!-- <div class="desktop-clear tab-clear"></div> -->
                    <div class="col-xs-12 col-sm-4 col-md-3 form-group purchaseitem base_qty_div  base-group">
                        <label>Base Quantity:</label>
                        <input type="text" class="inputs target txtBox  allownumericdecimal form-control"  id="base_quantity" name="base_quantity[]" placeholder="" />
                        <span id= ""></span>
                    </div>
                    <!-- <div class="tab-only-clear"></div> -->
                    <div class="col-xs-12 col-sm-4 col-md-3 form-group purchaseitem base_unit_div base-group">
                        <label>Base Unit:</label>
                        <input type="text" class="inputs target txtBox  allownumericdecimal form-control"  id="base_unit" name="base_unit[]" value="" placeholder="" readonly/>
                        <span id= ""></span>
                    </div>
                    
                    <div class="col-xs-12 col-sm-4 col-md-3 form-group purchaseitem base_rate_div base-group">
                        <label>Base Rate:</label>
                        <input type="text" class="inputs target txtBox  allownumericdecimal form-control"  id="base_rate" name="base_rate[]" placeholder="" readonly/>
                        <span id= ""></span>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-3 form-group purchaseitem quantity_div">
                        <label>Remark:</label>
                        <input type="text" class="inputs target txtBox form-control"  id="remark" tabindex="1" name="remark[]" placeholder=""/>
                    </div>

                    <div class="col-xs-12 purchaseitem text-right">
                        <label>&nbsp;</label>
                        <input type="button" class="item_save btn btn-info" id="0" value="Save">
                    </div>
                </div>
            </div> 
        </form>   
        <div class="row">        
    <div class="col-xs-12 mb-10">
        <div class="table-responsive">
            <table  class="table">
            <thead class="entry-table">
                    <tr>
                        <th>Sl.No</th>
                        <th>Item</th>
                        <th>Dimension</th>
                        <th>Batch</th>
                        <th>Quantity</th>
                        <th>Unit/Size</th>
                        <th>Base Quantity</th>
                        <th>Base Unit</th>
                        <th>Base Rate</th>
                        <th>Base Amount</th>
                        <th>Remark</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody class="addrow">
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
        <div class="col-md-offset-9 col-md-3 col-sm-offset-8 col-sm-4 col-xs-offset-6 col-xs-6">
            <input type="hidden" value="<?php echo ((isset($subtot) && $subtot != '') ? $subtot : ''); ?>"  
            class="txtBox pastweek" readonly=ture name="subtot"  /></td>
            <input type="hidden" value="<?php echo ((isset($grand) && $grand != '') ? $grand : ''); ?>"  
            class="txtBox pastweek grand" name="grand"  readonly=true/>
        </div>
        <div class="col-xs-12 text-right">
            <?php echo CHtml::Button($model->isNewRecord ? 'Create' : 'Save',
            array('class' => 'buttonsubmit btn btn-info', 'id' => 'buttonsubmit','disabled' => true)); ?>
        </div>
    </div>    
    </div>    
</div>

<input type="hidden" name="final_amount" id="final_amount" value="0">
<input type="hidden" name="item_amount_temp" id="item_amount_temp" value="0">




<script>

    jQuery.extend(jQuery.expr[':'], {
        focusable: function (el, index, selector) {
            return $(el).is('button, :input, [tabindex]');
        }
    });

    $(document).on('keypress', 'input,select,textarea,#remark', function (e) {
        if (e.which == 13) {
            e.preventDefault();
            // Get all focusable elements on the page
            var $canfocus = $(':focusable');
            var index = $canfocus.index(document.activeElement) + 1;
            if (index >= $canfocus.length)
                index = 0;
            $canfocus.eq(index).focus();
        }
    });

    $(document).ready(function () {
        $(".base-group").hide();
        $('#warehouse').change(function () {
            var warehouse_from = $(this).val();
            $("#warehouse_to option[value='" + warehouse_from + "']").remove();
            $("#warehouse_to").focus();
        });
        $(".js-example-basic-single").select2();
        $(".warehouse,.vendor,.clerk,.warehouse_to").select2();
        $(".stock_item").select2();
    });


    $(document).ready(function () {
        $('select').first().focus();
    });



</script>



<script>
    $(document).ready(function () {
        $().toastmessage({
            sticky: false,
            position: 'top-right',
            inEffectDuration: 1000,
            stayTime: 3000,
            closeText: '<i class="icon ion-close-round"></i>',
        });

        //$('.check_class').hide();
        $(".purchase_items").addClass('checkek_edit');


    });


    $(".warehouse").change(function () {
        var val = $(this).val();
        $.ajax({
            method: "POST",
            async: true,
            data: {id: val},
            dataType: "json",
            url: '<?php echo Yii::app()->createUrl('wh/warehousereceipt/getitem'); ?>',
            success: function (result) {
                if (result.status == 'success')
                {
                    $("#stock_item").html(result.html);
                } else {
                    $("#stock_item").html(result.html);
                }
            }
        });
    });


    $(".inputSwitch span").on("click", function () {

        var $this = $(this);

        $this.hide().siblings("input").val($this.text()).show();

    });

    $(".inputSwitch input").bind('blur', function () {

        var $this = $(this);

        $(this).attr('value', $(this).val());

        $this.hide().siblings("span").text($this.val()).show();

    }).hide();


    $(document).on('.dimension','change',function () {
        
        var element = $(this);
        var dimension_val = $(this).val();
        var warehouse_id = $('#warehouse').val();
        var category_id = $('#stock_item').val();
        if (category_id != '') {
           
            var unival = $.trim($('#item_unit_id_hidden').val()); 
            $.ajax({
                url: '<?php echo Yii::app()->createAbsoluteUrl('wh/warehousedespatch/getUnits'); ?>',
                type: 'GET',
                dataType: 'json',
                data: {data: category_id},
                success: function (result) {
                    
                    if (result.status == 1) {
                        if(result.base_unit!=''){
                            $("#item_baseunit_data").show();
                            $("#item_unit_additional_data").html(result.base_unit);
                        }
                       
                        $("#base_unit").val(result.base_unit);
                        $('select[id="item_unit"]').empty();
                        if (result.unit == result.base_unit) {
                            $('select[id="item_unit"]').append('<option value="' + result.unit + '" selected>' + result.units + '</option>');
                        } else {
                            if(result.base_unit!=unival){
                                                $.each(result["unit"], function (key, value) {
                                                if (unival == value.value) {
                                                    var selected = 'selected';
                                                } else {
                                                    var selected = '';
                                                }
                                                $('select[id="item_unit"]').append('<option value="' + value.id + '" ' + selected + '>' + value.value + '</option>');
                                            });
                            }else{
                            
                            $.each(result["unit"], function (key, value) {
                                if (result.base_unit == value.value) {
                                    var selected = 'selected';
                                } else {
                                    var selected = '';
                                }
                                $('select[id="item_unit"]').append('<option value="' + value.id + '" ' + selected + '>' + value.value + '</option>');
                            });
                            }
                        }


                    }
                    $('#quantity').focus();
                }
            });
            
        }
        if (category_id != '' && warehouse_id != '') {
            var itembatch = $('#item_unit_batch_hidden').val();
            $.ajax({
                url: '<?php echo Yii::app()->createAbsoluteUrl('wh/warehousedespatch/getBatch'); ?>',
                type: 'GET',
                async: true,
                data: {data: category_id, warehouse_id: warehouse_id,dimension_val:dimension_val},
                success: function (response) {
                    var result = JSON.parse(response);
                    $('select[id="batch"]').empty();
                    $.each(result["Batchlist"], function (key, value) {
                        if (value.value == '') {
                            value.value = 'Default Batch';
                        }
                        if (itembatch == value.id) {
                            var selected = 'selected';
                        } else {
                            var selected = '';
                        }
                        $('select[id="batch"]').append('<option data-batch ="' + value.rate + '" value="' + value.id + '" ' + selected + '>' + value.value + '</option>');
                    });
                    var batch =$('#batch').children("option:selected").val();
                    var quantity = $('#quantity').val();
                    var unit = $('#item_unit').children("option:selected").val();
                    var despatch_id = $('input[name="warehousedespatch_id"]').val();
                    var remark = $('#remark').val();
                    var despatch_item_id = $('.item_save').attr('id');
                    var data = {'quantity': quantity,'despatch_item_id': despatch_item_id, 'stock_item': category_id, 'batch': batch, 'unit': unit, 'despatch_id': despatch_id, 'remark': remark};
                    chechAvailableQuantity(data);                  
                    
                }
            });
            $('#quantity').focus();
        }
    });

    $('.stock_item').change(function () {
        var element = $(this);
        var category_id = $(this).val();
        var dimension_val = $('#dimension').val();
        var warehouse_id = $('#warehouse').val();
        if (category_id != '' && warehouse_id != '') {
            $.ajax({
                url: '<?php echo Yii::app()->createAbsoluteUrl('wh/warehousedespatch/getDimensions'); ?>',
                type: 'GET',
                async: true,
                data: {data: category_id, warehouse_id: warehouse_id},
                success: function (response) {
                    var result = JSON.parse(response);
                    if(result["dimensionlist"].length != 0){
                        
                        $('.dimension_div').show();
                        $('select[id="dimension"]').empty();
                        var i =0;
                        $.each(result["dimensionlist"], function (key, value) {
                            i++;
                            if(i==1){
                                batch = value.id;
                            }
                            if (value.value == '') {
                                value.value = 'Default Dimension';
                            }
                            $('select[id="dimension"]').append('<option value="' + value.id + '">' + value.value + '</option>');
                        });
                        if (category_id != '') {
                           
                            var unival = $.trim($('#item_unit_id_hidden').val());
                            $.ajax({
                                url: '<?php echo Yii::app()->createAbsoluteUrl('wh/warehousedespatch/getUnits'); ?>',
                                type: 'GET',
                                dataType: 'json',
                                data: {data: category_id},
                                success: function (result) {
                                    
                                    if(result.base_unit!=''){
                                        $("#item_baseunit_data").show();
                                        $("#item_unit_additional_data").html(result.base_unit);
                                    }
                                   $("#base_unit").val(result.base_unit);
                                    if (result.status == 1) {
                                        $('select[id="item_unit"]').empty();
                                        if (result.unit == result.base_unit) {
                                            $('select[id="item_unit"]').append('<option value="' + result.unit + '" selected>' + result.units + '</option>');
                                        } else {

                                            if(result.base_unit!=unival){
                                                
                                                $.each(result["unit"], function (key, value) {
                                                if (unival == value.value) {
                                                    var selected = 'selected';
                                                } else {
                                                    var selected = '';
                                                }
                                                console.log('val:',value.value);
                                                
                                                $('select[id="item_unit"]').append('<option value="' + value.id + '" ' + selected + '>' + value.value + '</option>');
                                            });
                                            }else{
                                               
                                                $.each(result["unit"], function (key, value) {
                                                if (result.base_unit == value.value) {
                                                    var selected = 'selected';
                                                } else {
                                                    var selected = '';
                                                }
                                                $('select[id="item_unit"]').append('<option value="' + value.id + '" ' + selected + '>' + value.value + '</option>');
                                            });

                                            }
                                    }               


                                    }
                                    $('#batch').focus();
                                }
                            });
                            
                        }
                        if (category_id != '' && warehouse_id != '') {
                            var itembatch = $('#item_unit_batch_hidden').val();
                            $.ajax({
                                url: '<?php echo Yii::app()->createAbsoluteUrl('wh/warehousedespatch/getBatch'); ?>',
                                type: 'GET',
                                async: true,
                                data: {data: category_id, warehouse_id: warehouse_id,dimension_val:dimension_val},
                                success: function (response) {
                                    var result = JSON.parse(response);
                                   
                                    $('select[id="batch"]').empty();
                                   
                                    $.each(result["Batchlist"], function (key, value) {
                                        if (value.value == '') {
                                            value.value = 'Default Batch';
                                        }

                                        if (itembatch == value.id) {
                                            var selected = 'selected';
                                        } else {
                                            var selected = '';
                                        }
                                    $('select[id="batch"]').append('<option data-batch ="' + value.rate + '" value="' + value.id + '"' + selected + '>' + value.value + '</option>');
                                        
                                    });
                                    var batch =$('#batch').children("option:selected").val();
                                    var quantity = $('#quantity').val();
                                    var unit = $('#item_unit').children("option:selected").val();
                                    var despatch_id = $('input[name="warehousedespatch_id"]').val();
                                    var remark = $('#remark').val();
                                    var despatch_item_id = $('.item_save').attr('id');
                                    var data = {'quantity': quantity,'despatch_item_id': despatch_item_id, 'stock_item': category_id, 'batch': batch, 'unit': unit, 'despatch_id': despatch_id, 'remark': remark};
                                    chechAvailableQuantity(data);                  
                                    
                                }
                            });
                            
                        }
                    }else{
                        
                        $('.dimension_div').hide();
                        if (category_id != '') {
                            var unival = $.trim($('#item_unit_id_hidden').val());
                            
                            $.ajax({
                                url: '<?php echo Yii::app()->createAbsoluteUrl('wh/warehousedespatch/getUnits'); ?>',
                                type: 'GET',
                                dataType: 'json',
                                data: {data: category_id},
                                success: function (result) {
                                    $("#base_unit").val(result.base_unit);
                                    if(result.base_unit!=''){
                                        $("#item_baseunit_data").show();
                                        $("#item_unit_additional_data").html(result.base_unit);
                                    }
                                    if (result.status == 1) {
                                        $('select[id="item_unit"]').empty();
                                        if (result.unit == result.base_unit) {
                                            $('select[id="item_unit"]').append('<option value="' + result.unit + '" selected>' + result.units + '</option>');
                                        } else {

                                            if(result.base_unit!=unival){
                                                
                                                $.each(result["unit"], function (key, value) {
                                                if (unival == value.value) {
                                                    var selected = 'selected';
                                                } else {
                                                    var selected = '';
                                                }
                                                $('select[id="item_unit"]').append('<option value="' + value.id + '" ' + selected + '>' + value.value + '</option>');
                                            });

                                            }else{
                                                
                                                $.each(result["unit"], function (key, value) {
                                                if (result.base_unit == value.value) {
                                                    var selected = 'selected';
                                                } else {
                                                    var selected = '';
                                                }
                                                $('select[id="item_unit"]').append('<option value="' + value.id + '" ' + selected + '>' + value.value + '</option>');
                                            });

                                            }
                                            
                                        }


                                    }
                                    $('#batch').focus();
                                }
                            });
                            
                        }
                        if (category_id != '' && warehouse_id != '') {
                            var itembatch = $('#item_unit_batch_hidden').val();
                            $.ajax({
                                url: '<?php echo Yii::app()->createAbsoluteUrl('wh/warehousedespatch/getBatch'); ?>',
                                type: 'GET',
                                async: true,
                                data: {data: category_id, warehouse_id: warehouse_id,dimension_val:dimension_val},
                                success: function (response) {
                                    var result = JSON.parse(response);
                                    
                                    $('select[id="batch"]').empty();
                                   
                                    $.each(result["Batchlist"], function (key, value) {
                                        if (itembatch == value.id ) {
                                            var selected = 'selected';
                                        } else {
                                            var selected = '';
                                        }
                                        
                                        if (value.value == '') {
                                            value.value = 'Default Batch';
                                        }
                                        $('select[id="batch"]').append('<option data-batch ="' + value.rate + '" value="' + value.id + '" ' + selected + '>' + value.value + '</option>');
                                    });
                                    var batch =$('#batch').children("option:selected").val();
                                    var quantity = $('#quantity').val();
                                    var unit = $('#item_unit').children("option:selected").val();
                                    var despatch_id = $('input[name="warehousedespatch_id"]').val();
                                    var remark = $('#remark').val();
                                    var despatch_item_id = $('.item_save').attr('id');
                                    var data = {'quantity': quantity,'despatch_item_id': despatch_item_id, 'stock_item': category_id, 'batch': batch, 'unit': unit, 'despatch_id': despatch_id, 'remark': remark};
                                    chechAvailableQuantity(data);                  
                                    
                                }
                            });
                            $('#batch').focus();
                        }
                    }
                    
                                   
                    
                }
            });
            
        }
    });
    $('#batch').change(function () {
       
        var element = $(this);
        var batch = $(this).children("option:selected").val();
        var category_id = $('.stock_item').val();
        var warehouse_id = $('#warehouse').val();
               
        if (category_id != '' && warehouse_id != '') {
            
            var quantity = $('#quantity').val();
            var unit = $('#item_unit').children("option:selected").val();
            var despatch_id = $('input[name="warehousedespatch_id"]').val();
            var remark = $('#remark').val();
            var despatch_item_id = $('.item_save').attr('id');
            var data = {'quantity': quantity,'despatch_item_id': despatch_item_id, 'stock_item': category_id, 'batch': batch, 'unit': unit, 'despatch_id': despatch_id, 'remark': remark};
            chechAvailableQuantity(data);   
            $('#item_unit').focus();
            getconversionfactor();
        }
    });
    
    $('#quantity').change(function () {
        var element = $(this);
        var quantity = $(this).val();
        var availableQuantity = $('#availableQuantity').val();
       
        var unit = $("#item_unit option:selected").text();
        if(quantity == ""){
            
            var remaining_quantity = availableQuantity;         
            $('#available_quantity').text("(Balance: "+remaining_quantity+" "+unit+" )");
            $('#quantity').focus();
       
        }else if(parseFloat(quantity) > parseFloat(availableQuantity) || availableQuantity == 0){
            var remaining_quantity = availableQuantity;
            $('#available_quantity').text("(Balance: "+remaining_quantity+" "+unit+" )");
            $('#quantity').val('');
            $('#quantity').focus();       
        }else{
            var remaining_quantity = availableQuantity-quantity;
            $('#available_quantity').text("(Balance: "+remaining_quantity+" "+unit+ " )");
            $('#remark').focus();
        }
        
    });

    $('#quantity').blur(function () {
        getconversionfactor();
    });

    $('#quantity').change(function () {
    var base_quantity = parseFloat($('#quantity').val()); 
    var batch_rate = $('#batch').find(':selected').attr('data-batch');
        batch_rate  = isNaN(batch_rate) ? 0 : batch_rate;

        console.log(base_quantity,batch_rate);
         if (!isNaN(base_quantity) && !isNaN(batch_rate)) { 
                var amount = base_quantity * batch_rate; 
                $('#amount').val(amount.toFixed(2)); 
            } else {
                $('#amount').val(''); 
                console.log('Nan error');
            }
        });


    $('#item_unit').change(function () {
        var element = $(this);
        var unit = $(this).children("option:selected").val();
        var b_unit=$('#base_unit').val();
        var category_id = $('.stock_item').val();
        var warehouse_id = $('#warehouse').val();
        if (category_id != '' && warehouse_id != '') {
            
            var quantity = $('#quantity').val();
            var batch = $('#batch').children("option:selected").val();
            var despatch_id = $('input[name="warehousedespatch_id"]').val();
            var remark = $('#remark').val();
            var despatch_item_id = $('.item_save').attr('id');
            var data = {'quantity': quantity,'despatch_item_id': despatch_item_id, 'stock_item': category_id, 'batch': batch, 'unit': unit, 'despatch_id': despatch_id, 'remark': remark};
            chechAvailableQuantity(data);
            $('#quantity').focus();
            getconversionfactor();
        }
        
    });

    function getconversionfactor(){
        var purchase_unit = $("#item_unit option:selected").val();
        var base_unit=$('#base_unit').val();
        var itemId = $('.stock_item').val();
        var quantity = parseFloat($("#quantity").val());
        quantity = isNaN(quantity) ? 0 : quantity;
        quantity = quantity.toFixed(4);
        var batch_rate = $('#batch').find(':selected').attr('data-batch');
        batch_rate  = isNaN(batch_rate) ? 0 : batch_rate;
        var amt =  (quantity * batch_rate).toFixed(2);
        if (purchase_unit !== '' && base_unit !== '' && itemId !== "") {
                
                if (base_unit != purchase_unit &&  quantity>0) {
                    $(".base-group").show();
                    $.ajax({
                        method: "POST",
                        data: {
                            'purchase_unit': purchase_unit,
                            'base_unit': base_unit,
                            'item_id': itemId
                        },
                        dataType: "json",
                        url: '<?php echo Yii::app()->createAbsoluteUrl('wh/warehousedespatch/getUnitconversionFactor'); ?>',
                        success: function (result) {
                            
                            if (result != '') {
                                $("#item_conversion_data").show();
                                $("#item_unit_additional_conv").html(result);
                            }
                            base_quantity = result * quantity;
                            $("#base_quantity").val(base_quantity);
                            $("#base_rate").val(batch_rate);
                        }
                    });
                } else {
                    $("#item_conversion_data").hide();
                    $(".base-group").hide();
                    
                    $("#base_quantity").val(quantity);
                    $("#base_rate").val(batch_rate);
                }
            }

    }

    $('#base_quantity').blur(function () {
           
        var itemId = $('.stock_item').val();
        var purchase_unit = $("#item_unit option:selected").val();
        var base_unit = $('#base_unit').val();
        var quantity = parseFloat($("#quantity").val());
        quantity = isNaN(quantity) ? 0 : quantity;
        quantity = quantity.toFixed(4);
        var basequantity = parseFloat($("#base_quantity").val());
        var batch_rate = $('#batch').find(':selected').attr('data-batch');
        batch_rate  = isNaN(batch_rate) ? 0 : batch_rate;
        var amt =  (quantity * batch_rate).toFixed(2);
        if (purchase_unit !== '' && base_unit !== '' && itemId !== "") {
            if(base_unit != purchase_unit) {
                base_quantity = basequantity;
                $("#base_quantity").val(base_quantity);
                $("#base_rate").val(batch_rate);
            }
        }
    });
</script>


<script>

    /* Neethu  */

    $(document).on("change", "#warehouse", function () {
        var element = $(this);
        var despatch_id = $("#warehousedespatch_id").val();
        var default_date = $(".date").val();
        var warehouse = $(this).val();
        var warehouse_to = $("#warehouse_to").val();
        var vendor = $('#vendor').val();
        var despatch_no = $('#despatch_no').val();
        var clerk = $('#clerk').val();
        var through = $('#through').val();
        var eta_date = $('#datepicker_eta').val();
        if ((moment(default_date, 'DD-MM-YYYY', true).isValid()))
        {
            if (warehouse == '' || default_date == '' || despatch_no == '' || clerk == '' || warehouse_to == '' || eta_date == '') {
                $.ajax({
                    method: "GET",
                    dataType: "json",
                    url: '<?php echo Yii::app()->createUrl('wh/warehousedespatch/ajax'); ?>',
                    success: function (result) {
                        $("#warehouse_to").select2("focus");
                    }
                });
            } else {
                $(".purchase_items").removeClass('checkek_edit');
                $('.js-example-basic-single').select2('focus');
                $(".stock_item").select2("focus");
                
            }
        } else {
            $(this).focus();
            $().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
            $(this).focus();
        }
    });
    /* Surumi */
    $(document).on("change", "#warehouse_to", function () {
        var element = $(this);
        var despatch_id = $("#warehousedespatch_id").val();
        var default_date = $(".date").val();
        var warehouse_to = $(this).val();
        var warehouse = $('#warehouse').val();
        var warehouse_to = $("#warehouse_to").val();

        var vendor = $('#vendor').val();
        var despatch_no = $('#despatch_no').val();
        var clerk = $('#clerk').val();
        var through = $('#through').val();
        var eta_date = $('#datepicker_eta').val();
        if ((moment(default_date, 'DD-MM-YYYY', true).isValid()))
        {
            if (warehouse == '' || default_date == '' || despatch_no == '' || clerk == '' || warehouse_to == '' || eta_date == '') {
                $.ajax({
                    method: "GET",
                    dataType: "json",
                    url: '<?php echo Yii::app()->createUrl('wh/warehousedespatch/ajax'); ?>',
                    success: function (result) {
                        $("#clerk").select2("focus");
                    }
                });
            } else {
                $(".purchase_items").removeClass('checkek_edit');
                $('.js-example-basic-single').select2('focus');
                $(".stock_item").select2("focus");
                
            }
        } else {
            $(this).focus();
            $().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
            $(this).focus();
        }
    });
    $(document).on("change", "#vendor", function () {
        var element = $(this);
        var despatch_id = $("#warehousedespatch_id").val();
        var default_date = $(".date").val();
        var vendor = $(this).val();

        var through = $('#through').val();
        var warehouse = $('#warehouse').val();
        var warehouse_to = $("warehouse_to").val();
        var despatch_no = $('#despatch_no').val();
        var clerk = $('#clerk').val();
        var eta_date = $('#datepicker_eta').val();
        if ((moment(default_date, 'DD-MM-YYYY', true).isValid()))
        {

            if (warehouse == '' || default_date == '' || despatch_no == '' || clerk == '' || warehouse_to == '' || eta_date == '') {
                $.ajax({
                    method: "GET",
                    dataType: "json",
                    url: '<?php echo Yii::app()->createUrl('wh/warehousedespatch/ajax'); ?>',
                    success: function (result) {
                        $(".stock_item").select2("focus");
                    }
                });

            } else {
                $(".purchase_items").removeClass('checkek_edit');
                $('.js-example-basic-single').select2('focus');
                $(".stock_item").select2("focus");
                

            }

        } else {
            $(this).focus();
            $().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
            $(this).focus();
        }

    });


    $(document).on("change", "#clerk", function () {
        var element = $(this);
        var despatch_id = $("#warehousedespatch_id").val();
        var default_date = $(".date").val();

        var clerk = $(this).val();
        var through = $('#through').val();
        var warehouse = $('#warehouse').val();
        var warehouse_to = $("#warehouse_to").val();
        var despatch_no = $('#despatch_no').val();
        var vendor = $('#vendor').val();
        var eta_date = $('#datepicker_eta').val();
        if ((moment(default_date, 'DD-MM-YYYY', true).isValid()))
        {
            if (warehouse == '' || default_date == '' || despatch_no == '' || clerk == '' || warehouse_to == '' || eta_date == '') {
                $.ajax({
                    method: "GET",
                    data: {purchase_id: 'test'},
                    dataType: "json",
                    url: '<?php echo Yii::app()->createUrl('wh/warehousedespatch/ajax'); ?>',
                    success: function (result) {
                        $("#datepicker").focus();
                    }
                });

            } else {
                $(".purchase_items").removeClass('checkek_edit');
                $('.js-example-basic-single').select2('focus');
               $(".stock_item").select2("focus");
                

            }

        } else {
            $(this).focus();
            $().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
            $(this).focus();
        }

    });



    $(document).on("change", "#datepicker", function () {
        var element = $(this);
        var despatch_id = $("#warehousedespatch_id").val();
        var default_date = $(this).val();
        var warehouse = $('#warehouse').val();
        var warehouse_to = $("#warehouse_to").val();

        var vendor = $('#vendor').val();
        var despatch_no = $('#despatch_no').val();
        var clerk = $('#clerk').val();
        var through = $('#through').val();
        var eta_date = $('#datepicker_eta').val();
        if ((moment(default_date, 'DD-MM-YYYY', true).isValid()))
        {
            if (warehouse == '' || default_date == '' || despatch_no == '' || clerk == '' || warehouse_to == '' || eta_date == '') {
                $("#despatch_no").focus();
            } else {
                $(".stock_item").select2("focus");
                $(".purchase_items").removeClass('checkek_edit');
                $('.js-example-basic-single').select2('focus');
                

            }
        } else {
            $(this).focus();
            $().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
            $(this).focus();
        }

        //$('#purchaseno').focus();

    });


    $(document).on("change", "#despatch_no", function () {
        var element = $(this);
        var despatch_id = $("#warehousedespatch_id").val();
        var default_date = $(".date").val();
        var despatch_no = $(this).val();
        var through = $('#through').val();
        var warehouse = $('#warehouse').val();
        var warehouse_to = $("#warehouse_to").val();

        var clerk = $('#clerk').val();
        var vendor = $('#vendor').val();
        var through = $('#through').val();
        var eta_date = $('#datepicker_eta').val();
        if ((moment(default_date, 'DD-MM-YYYY', true).isValid()))
        {
            if (warehouse == '' || default_date == '' || despatch_no == '' || clerk == '' || warehouse_to == '' || eta_date == '') {
                $("#datepicker_eta").focus();
            } else {
                $(".purchase_items").removeClass('checkek_edit');
                $('.js-example-basic-single').select2('focus');
                

            }

        } else {
            $(this).focus();
            $().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
            $(this).focus();
        }

    });
    $(document).on("change", "#datepicker_eta", function () {
        var element = $(this);
        var despatch_id = $("#warehousedespatch_id").val();
        var default_date = $('#datepicker').val();
        var warehouse = $('#warehouse').val();
        var warehouse_to = $("#warehouse_to").val();

        var vendor = $('#vendor').val();
        var despatch_no = $('#despatch_no').val();
        var clerk = $('#clerk').val();
        var through = $('#through').val();
        var eta_date = $(this).val();
        if ((moment(default_date, 'DD-MM-YYYY', true).isValid()))
        {
            if (warehouse == '' || default_date == '' || despatch_no == '' || clerk == '' || warehouse_to == '' || eta_date == '') {
                $("#vendor").focus();
            } else {
                $("#vendor").focus();
                $(".purchase_items").removeClass('checkek_edit');
                $('.js-example-basic-single').select2('focus');
                

            }
        } else {
            $(this).focus();
            $().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
            $(this).focus();
        }

        //$('#purchaseno').focus();

    });
    $("#warehouse").change(function () {
        $.ajax({
            method: "GET",
            dataType: "json",
            url: '<?php echo Yii::app()->createUrl('wh/warehousedespatch/ajax'); ?>',
            success: function (result) {
                $("#warehouse_to").focus();
            }
        });
    });

    $("#clerk").change(function () {
        $.ajax({
            method: "GET",
            dataType: "json",
            url: '<?php echo Yii::app()->createUrl('wh/warehousedespatch/ajax'); ?>',
            success: function (result) {
                $("#datepicker").focus();
            }
        });
    })

    
    $("#datepicker_eta").keydown(function (event) {
        if (event.keyCode == 13) {
            $("#vendor").focus();
        }
    });
    
   
var sl_no = 1;
    var howMany = 0;
    $('.item_save').click(function ()
    {
        $("#previous_details").hide();
        var element = $(this);
        var item_id = $(this).attr('id');

        if (item_id == 0) {

            // add
            var stock_item = $('.stock_item').val();
            var quantity = $('#quantity').val();
            var batch = $('#batch').val();
            var unit = $('#item_unit').val();
            var remark = $('#remark').val();
            var dimension = $('#dimension').val();

            var base_quantity = $('#base_quantity').val();
            var base_unit = $('#base_unit').val();
            var base_rate = $('#base_rate').val();
            var base_amount=$('#amount').val();
            var warehouse = $('#warehouse').val();
            var vendor = $('#vendor').val();
            var date = $(".date").val();
            var despatch_no = $('#despatch_no').val();
            var clerk = $('#clerk').val();
            var rowCount = $('.table .addrow tr').length;


            if (warehouse == '' || date == '' || despatch_no == '' || clerk == '') {
                $().toastmessage('showErrorToast', "Please enter Warehouse receipt details");
            } else {
                if ((moment(date, 'DD-MM-YYYY', true).isValid()))
                {

                    if (stock_item == '' || quantity == '')
                    {
                        $().toastmessage('showErrorToast', "Please fill item details");
                    } else {
                        howMany += 1;
                        if (howMany == 1)
                        {
                            var despatch_id_check = $('input[name="warehousedespatch_id"]').val();
                            if(despatch_id_check == "" || despatch_id_check == null || despatch_id_check == 0){
                                var despatch_id = $("#warehousedespatch_id").val();
                                var default_date = $(".date").val();
                                var through = $('#through').val();
                                var warehouse_to = $("#warehouse_to").val();
                                var through = $('#through').val();
                                var eta_date = $('#datepicker_eta').val();
                                var despatch_id = 0;
                                
                                $.ajax({
                                        method: "GET",
                                        data: {despatch_id: despatch_id, despatch_no: despatch_no, default_date: default_date, warehouse: warehouse, vendor: vendor, clerk: clerk, through: through, warehouse_to: warehouse_to, eta_date: eta_date},
                                        dataType: "json",
                                        url: '<?php echo Yii::app()->createUrl('wh/warehousedespatch/createnewdespatch'); ?>',
                                        success: function (result) {
                                            if (result.response == 'success')
                                            {
                                                $(".purchase_items").removeClass('checkek_edit');
                                                $('.js-example-basic-single').select2('focus');
                                                $("#warehousedespatch_id").val(result.despatch_id);
                                                $().toastmessage('showSuccessToast', "" + result.msg + "");
                                                despatch_id = result.despatch_id;
                                                var data = {'sl_no': rowCount, 'quantity': quantity, 'stock_item': stock_item, 'batch': batch, 'unit': unit, 'despatch_id': despatch_id, 'remark': remark,'dimension':dimension,'base_quantity':base_quantity,'base_unit':base_unit,'base_rate':base_rate,'base_amount': base_amount};
                                                createDespatchItem(data);
                                            } else {
                                                $().toastmessage('showErrorToast', "" + result.msg + "");
                                            }

                                            $(".stock_item").select2("focus");
                                        }
                                    });
                            }else{
                                var despatch_id = $('input[name="warehousedespatch_id"]').val();
                                var data = {'sl_no': rowCount, 'quantity': quantity, 'stock_item': stock_item, 'batch': batch, 'unit': unit, 'despatch_id': despatch_id, 'remark': remark,'dimension':dimension,'base_quantity':base_quantity,'base_unit':base_unit,'base_rate':base_rate,'base_amount': base_amount};
                                createDespatchItem(data);
                            }
                             
                                
                        }
                    }

                } else {

                    $(this).focus();
                    $().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
                    $(this).focus();
                }




            }



        } else {
            // update


            var stock_item = $('.stock_item').val();
            var quantity = $('#quantity').val();
            var batch = $('#batch').val();
            var unit = $('#item_unit').val();
            var warehouse = $('#warehouse').val();

            var base_quantity = $('#base_quantity').val();
            var base_unit = $('#base_unit').val();
            var base_rate = $('#base_rate').val();
            var vendor = $('#vendor').val();
            var date = $(".date").val();
            var despatch_no = $('#despatch_no').val();
            var clerk = $('#clerk').val();
            var remark = $('#remark').val();
            if (warehouse == '' || date == '' || despatch_no == '' || clerk == '') {
                $().toastmessage('showErrorToast', "Please enter Warehouse receipt details");
            } else {

                if ((moment(date, 'DD-MM-YYYY', true).isValid()))
                {
                    if (stock_item == '' || quantity == '')
                    {
                        $().toastmessage('showErrorToast', "Please fill item details");
                    } else {
                        howMany += 1;
                        if (howMany == 1)
                        {
                            var despatch_id = $('input[name="warehousedespatch_id"]').val();
                            var data = {'item_id': item_id, 'sl_no': sl_no, 'quantity': quantity, 'stock_item': stock_item, 'batch': batch, 'unit': unit, 'despatch_id': despatch_id, 'remark': remark,'base_quantity':base_quantity,'base_unit':base_unit,'base_rate':base_rate,'base_rate':base_rate};
                            $.ajax({
                                url: '<?php echo Yii::app()->createAbsoluteUrl('wh/warehousedespatch/updatedespatchitem'); ?>',
                                type: 'GET',
                                dataType: 'json',
                                data: {data: data},
                                success: function (response) {
                                    if (response.response == 'success')
                                    {
                                        $('#final_amount').val(response.final_amount);
                                        $().toastmessage('showSuccessToast', "" + response.msg + "");
                                        $('#grand_total').text(response.final_amount);
                                        $('.addrow').html(response.html);
                                    } else {
                                        $().toastmessage('showErrorToast', "" + response.msg + "");
                                    }
                                    howMany = 0;
                                    $('#stock_item').val('').trigger('change');
                                    var quantity = $('#quantity').val('');
                                    var amount = $('#amount').val('');
                                    var unit = $('#item_unit').val('');
                                    $('#item_unit_batch_hidden').val('');
                                    $('#item_unit_id_hidden').val('');
                                    $('select[id="item_unit"]').empty();
                                    $('select[id="item_unit"]').append('<option value="" selected>Unit</option>');
                                    $('select[id="batch"]').empty();
                                    $('select[id="batch"]').append('<option value="" selected>Please choose Batch</option>');
                                    $(".item_save").attr('value', 'Save');
                                    $(".item_save").attr('id', 0);
                                    $('#stock_item').select2('focus');
                                    $('#remark').val('');
                                    $('#available_quantity').html('');
                                    $('#warehouse').prop('disabled', true);
                                    $(".base-group").hide();
                                    $('#item_baseunit_data').hide();
								    $('#item_conversion_data').hide();
                                }
                            });

                            $('.js-example-basic-single').select2('focus');
                        }
                    }

                } else {
                    $(this).focus();
                    $().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
                    $(this).focus();
                }



            }


        }




    });





    $(document).on('click', '.edit_item', function (e) {
        e.preventDefault();
        var item_id = $(this).attr('id');
        var $tds = $(this).closest('tr').find('td');
        var sl_no =  parseInt($tds.eq(0).text());
        var description = $tds.eq(1).text();
        var dimension_text = $.trim($tds.eq(2).text());
        var dimension = $('#warehousestock_itemid_dimension_category'+sl_no).val();
        var batch = $tds.eq(3).text();
        var quantity = $tds.eq(4).text();
        var unit = $tds.eq(5).text();
        var base_qty = $tds.eq(6).text();
        var base_unit = $.trim($tds.eq(7).text());
        var base_rate = $tds.eq(8).text();
        var remark = $tds.eq(9).text();
        var itemunt = $.trim($('#item_unit').val());
        
        $abc = $(this).closest('tr').find('.item_description').attr('id');
        var des_id = $(this).closest('tr').find('.item_description').attr('id');
        var unit_name = $.trim(unit);
        var unit_id = $('#item_unit_id'+sl_no).val();
        var warehousestock_id = $('#warehousestock_id'+sl_no).val();
        <?php $abc; ?>
        
        
        $('#stock_item').val(des_id).trigger('change');
        if(dimension_text != ""){
            $('.dimension_div').show();
            $('#dimension').val(parseInt(dimension)).trigger('change');             
        }else{
            $('.dimension_div').hide();
        }

        if(base_unit!=unit_name){
            
            $('.base-group').show();
            $('#item_conversion_data').show();
        }else{
            
            $('.base-group').hide();
            $('#item_conversion_data').hide();
        }

       
        $('#dimension').val(dimension);
        $('.js-example-basic-single').select2('focus');
        $('#item_unit').val(unit_name);
        $('#item_unit_id_hidden').val(unit_name);
        $('#quantity').val(parseFloat(quantity));
        $('#remark').val(remark);
        $('#base_quantity').val(parseFloat(base_qty));
        $('#base_rate').val(parseFloat(base_rate));
        $('#batch').val(parseInt(warehousestock_id));
        $('#item_unit_batch_hidden').val(parseInt(warehousestock_id));
        $(".item_save").attr('value', 'Update');
        $(".item_save").attr('id', item_id);
       
    });


    $('.item_save').keypress(function (e) {
        if (e.keyCode == 13) {
            $('.item_save').click();
        }
    });


    function filterDigits(eventInstance) {
        eventInstance = eventInstance || window.event;
        key = eventInstance.keyCode || eventInstance.which;
        if ((47 < key) && (key < 58) || key == 8) {
            return true;
        } else {
            if (eventInstance.preventDefault)
                eventInstance.preventDefault();
            eventInstance.returnValue = false;
            return false;
        } //if
    }
    function createDespatchItem(data) {
        $.ajax({
                url: '<?php echo Yii::app()->createAbsoluteUrl('wh/warehousedespatch/despatchitem'); ?>',
                type: 'GET',
                dataType: 'json',
                data: {data: data},
                success: function (response) {
                    if (response.response == 'success')
                    {
                        $().toastmessage('showSuccessToast', "" + response.msg + "");
                        $('.addrow').html(response.html);
                    } else {
                        $().toastmessage('showErrorToast', "" + response.msg + "");
                    }
                    howMany = 0;
                    $('#stock_item').val('').trigger('change');
                    var quantity = $('#quantity').val('');
                    var unit = $('#item_unit').val('');
                    
                    $('#item_unit_id_hidden').val('');
                    $('#item_unit_batch_hidden').val('');
                    $('select[id="item_unit"]').empty();
                    $('select[id="item_unit"]').append('<option value="" selected>Unit</option>');
                    $('select[id="batch"]').empty();
                    $('select[id="batch"]').append('<option value="" selected>Please choose Batch</option>');
                    $('#stock_item').select2('focus');
                    $('#remark').val('');
                    $('select[id="dimension"]').empty();
                    $('select[id="dimension"]').append('<option value="" selected>Please choose Dimension</option>');
                    $('#available_quantity').html('');
                    $('#buttonsubmit').prop('disabled', false);
                    $('#warehouse').prop('disabled', true);
                    $(".base-group").hide();
                    $('#item_baseunit_data').hide();
					$('#item_conversion_data').hide();
                }
            });
        

        $('.js-example-basic-single').select2('focus');
    }
    function chechAvailableQuantity(data) {
        $.ajax({
                url: '<?php echo Yii::app()->createAbsoluteUrl('wh/warehousedespatch/chechAvailableQuantity'); ?>',
                type: 'GET',
                dataType: 'json',
                data: {data: data},
                success: function (result) {
                    if (result.response == "success") {
                        $('#available_quantity').text("(Balance: "+result.available_quantity+")");
                        $('#availableQuantity').val(result.availableQuantity);
                        if(result.availableQuantity=='0'){
                            $('#available_quantity').addClass('blinking');
                        }else{
                            $('#available_quantity').removeClass('blinking');
                        }
                    }
                    
                }
            });
    }

// approve items

    $(document).on('click', '.approve_item', function (e) {
        e.preventDefault();
        var element = $(this);
        var item_id = $(this).attr('id');
        $.ajax({
            url: '<?php echo Yii::app()->createAbsoluteUrl('purchase/permissionapprove'); ?>',
            type: 'POST',
            dataType: 'json',
            data: {item_id: item_id},
            success: function (response) {
                if (response.response == 'success')
                {
                    $(".approveoption_" + item_id).hide();
                    element.closest('tr').find('.rate_highlight').removeClass('rate_highlight');
                    $().toastmessage('showSuccessToast', "" + response.msg + "");
                } else if (response.response == 'warning') {
                    $(".approveoption_" + item_id).hide();
                    element.closest('tr').find('.rate_highlight').removeClass('rate_highlight');
                    $().toastmessage('showWarningToast', "" + response.msg + "");
                } else {
                    $().toastmessage('showErrorToast', "" + response.msg + "");
                }
            }
        });
    });

    $(document).on('mouseover', '.rate_highlight', function (e) {
        var item_id = $(this).attr('id');
        $.ajax({
            url: '<?php echo Yii::app()->createAbsoluteUrl('purchase/previousratedetails'); ?>',
            type: 'GET',
            dataType: 'json',
            data: {item_id: item_id},
            success: function (result) {
                if (result.status == 1) {
                    $('#previous_details').html(result.html);
                    $("#previous_details").show();
                    $("#pre_fixtable2").tableHeadFixer();
                } else {
                    $('#previous_details').html(result.html);
                    $("#previous_details").hide();
                }
            }
        })
    })
    $(document).on('mouseout', '.rate_highlight', function (e) {
        $("#previous_details").hide();
    });

    $("#purchaseno").keyup(function () {
        if (this.value.match(/[^a-zA-Z0-9.:]/g)) {
            this.value = this.value.replace(/[^a-zA-Z0-9.:\-/]/g, '');
        }
    });

    $(document).on('click', '.getprevious', function () {
        var id = $(this).attr('data-id');
        var res = id.split(",");
        var amount = parseFloat(res[4]);
        $('#description').val(res[0]).trigger('change.select2');
        $('#quantity').val(res[1]);
        $('#item_unit').val(res[2]);
        $('#rate').val(res[3]);
        $('#item_amount').text(amount.toFixed(2));
        var total = (res[1] * res[3]);
        if (isNaN(total))
            total = 0;
        $('#previousvalue').text(total.toFixed(2));
    })

    $(document).on('click', '.removebtn', function (e) {
        e.preventDefault();
        element = $(this);
        var item_id = $(this).attr('id');
        var answer = confirm("Are you sure you want to delete?");
        if (answer)
        {
            var warehouse_to = $("#warehouse_to").val();
            var despatch_id = $("#warehousedespatch_id").val();
            var data = {'despatch_id': despatch_id, 'item_id': item_id,'transfer_type':"despatch",'warehouse_to':warehouse_to};
            $.ajax({
                method: "GET",
                async: false,
                data: {data: data},
                dataType: "json",
                url: '<?php echo Yii::app()->createUrl('wh/warehousedespatch/removeitem'); ?>',
                success: function (result) {
                    if (result.response == 'success')
                    {
                        $('.addrow').html(result.html);
                        $().toastmessage('showSuccessToast', "" + result.msg + "");
                    } else {
                        $().toastmessage('showErrorToast', "" + result.msg + "");
                    }
                    $('#stock_item').val('').trigger('change');
                    var quantity = $('#quantity').val('');
                    var unit = $('#item_unit').val('');
                    
                    $('select[id="item_unit"]').empty();
                    $('select[id="item_unit"]').append('<option value="" selected>Unit</option>');
                    $('select[id="batch"]').empty();
                    $('select[id="batch"]').append('<option value="" selected>Please choose Batch</option>');
                    $(".item_save").attr('value', 'Save');
                    $(".item_save").attr('id', 0);
                    $('#stock_item').select2('focus');
                    $('#remark').val('');
                    if(result.warehousedespatch_qty == 0){
                        $('#buttonsubmit').prop('disabled', true);
                        $('#warehouse').prop('disabled', false);
                    }else{
                        $('#buttonsubmit').prop('disabled', false);
                        $('#warehouse').prop('disabled', true);
                    }
                }
            });

        } else
        {

            return false;
        }
    });
    
    $( "#buttonsubmit" ).keypress(function(e) {
		if (e.keyCode == 13) {
          $('.buttonsubmit').click();
      }
    });
    
    $(".buttonsubmit").click(function(){
        var general_settings_for_auto_receipt =<?php echo $general_settings_for_auto_receipt; ?>;
        if(general_settings_for_auto_receipt ==1){
            var answer = confirm("Are you sure you want to auto receipt this item?");
            if (answer)
            {
                $("#warehousedespatch_form").submit();
            }else{
                return false;
            }
        }else{
            $("#warehousedespatch_form").submit();
        }
	});
</script>


<script>
    $(".popover-test").popover({
        html: true,
        content: function () {
            //return $('#popover-content').html();
            return $(this).next('.popover-content').html();
        }
    });
    $('[data-toggle=popover]').on('click', function (e) {
        $('[data-toggle=popover]').not(this).popover('hide');
    });
    $('body').on('hidden.bs.popover', function (e) {
        $(e.target).data("bs.popover").inState.click = false;
    });
    $('body').on('click', function (e) {
        $('[data-toggle=popover]').each(function () {
            // hide any open popovers when the anywhere else in the body is clicked
            if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                $(this).popover('hide');
            }
        });
    });

    $(document).ajaxComplete(function () {
        $(".popover-test").popover({
            html: true,
            content: function () {
                return $(this).next('.popover-content').html();
            }
        });


    });
</script>

