<script
    src="<?php echo $this->customAssets('https://code.jquery.com/jquery-1.12.4.js', '/js/jquery-1.12.4.js'); ?>"></script>
<script
    src="<?php echo $this->customAssets('https://code.jquery.com/ui/1.12.1/jquery-ui.js', '/js/jquery-ui.js'); ?>"></script>
<p id="update_success_message" style="display:none;font-size: 15px;color: green;font-weight: bold;">Successfully
    updated..</p>
<p id="create_success_message" style="display:none;font-size: 15px;color: green;font-weight: bold;">Successfully
    created..</p>
<div>
    <div class="entries-wrapper padding-bottom-10">
        <div class="row">
            <div class="col-xs-12">
                <div class="heading-title">Details</div>
                <div class="dotted-line"></div>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-xs-12 form-group col-xs-12 col-md-3">
                <label>Company</label>
                <?php $company = Company::model()->findByAttributes(array('id' => $model->company_id)); ?>
                <input type="text" class="form-control" readonly value="<?= $company ? $company->name : '' ?>">
                <input type="hidden" value="<?= $project_id ?>" name="MaterialRequisition[project_id]" id="project_id">
            </div>
            <div class="form-group col-xs-12 form-group col-xs-12 col-md-3">
                <label>Project</label>
                <input type="text" class="form-control" readonly value="<?= $project_name ?>">
                <input type="hidden" value="<?= $project_id ?>" name="MaterialRequisition[project_id]" id="project_id">
            </div>
            <div class="form-group col-xs-12 form-group col-xs-12 col-md-3">
                <label>Task</label>
                <input type="text" id="requisition_task_id" value="<?= $model->task_description ?>" class="form-control"
                    readonly>
                <input type="hidden" name="MaterialRequisition[task_id]" id="task_id">
            </div>
            <div class="form-group col-xs-12 form-group col-xs-12 col-md-3">
                <label>Req.No</label>
                <input type="text" id="requisition_no" value="<?= $model->requisition_no ?>" readonly
                    class="form-control">
            </div>
        </div>
        <div class="row" id="hide">
            <div class="form-group col-xs-12 form-group col-xs-12 col-md-3">
                <label>Item</label>
                <input type="hidden" id="item_id">
                <input type="hidden" id="material_req_id" value="<?= $id ?>">
                <select class="form-control req-item" id="requisition_item">
                    <option value="">Select Materials</option>
                    <?php
                    foreach ($account_items as $key => $value) {
                        ?>
                        <option value="<?php echo $value['id']; ?>">
                            <?php echo $value['data']; ?>
                        </option>
                    <?php } ?>
                    <option value="other">Other</option>
                </select>
                <div class="errorMessage" id="item_em_" style="display:none"></div>
            </div>
            <div class="form-group col-xs-12 form-group col-xs-12 col-md-3" id="item_name_id" style="display:none">
                <label>Item Name</label>
                <input type="text" class="form-control" id="requisition_item_name">
                <div class="errorMessage" id="item_name_em_" style="display:none"></div>
            </div>
            <div class="form-group col-xs-12 form-group col-xs-12 col-md-3">
                <label>Unit</label>
                <select class="form-control" id="requisition_unit">
                    <option value="">Select Unit</option>
                    <?php
                    foreach ($unit_list as $unit) {
                        ?>
                        <option value="<?= $unit['unit_name'] ?>">
                            <?= $unit['unit_name'] ?>
                        </option>
                        <?php
                    }
                    ?>
                </select>
                <div class="errorMessage" id="unit_em_" style="display:none"></div>
            </div>
            <div class="form-group col-xs-12 form-group col-xs-12 col-md-3">
                <label>Quantity</label>
                <input type="number" class="form-control" id="requisition_quantity">
                <div class="errorMessage" id="quantity_em_" style="display:none"></div>
            </div>
            <div class="form-group col-xs-12 form-group col-xs-12 col-md-3">
                <label>Date</label>
                <input type="text" id="requisition_date" class="form-control">
                <div class="errorMessage" id="date_em_" style="display:none"></div>
            </div>
            <div class="form-group col-xs-12 col-md-3">
                <label>Remarks </label>
                <textarea rows="2" cols="20" class="form-control" autocomplete="off"
                    id="requisition_remarks"></textarea>
                <div class="errorMessage" id="remarks_em_" style="display:none"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-right" id="update_button">
                <input type="button" class="btn blue add-mr btn btn-primary btn-sm" value="Create" id="update_mr">
                <input type="button" class="btn blue update-mr" value="Update" id="update_mr" style="display:none">
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 subcontractor-bill-table">
                <?php

                $this->widget('zii.widgets.grid.CGridView', array(
                    'id' => 'material-requisition-grid',
                    'dataProvider' => $items_model->search($id),
                    'itemsCssClass' => 'table table-bordered', // Add the desired classes here
                    'columns' => array(
                        array(
                            'header' => 'S.No.',
                            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
                        ),
                        array(
                            'header' => 'ITEM NAME',
                            'name' => 'material.material_name', // Access the 'name' attribute of the related 'material' model
                            'value' => '$data->material ? CHtml::encode($data->material->material_name) : "N/A"', // Display the material name or 'N/A' if not set
                        ),
                        array(
                            'header' => 'UNIT',
                            'name' => 'item_unit',
                            'value' => '$data->item_unit',
                            'type' => 'raw',
                        ),
                        array(
                            'header' => 'QUANTITY',
                            'name' => 'item_quantity',
                            'value' => '$data->item_quantity',
                            'type' => 'raw',
                        ),
                        array(
                            'header' => 'DATE',
                            'name' => 'item_req_date',
                            'value' => 'date("d-M-y", strtotime($data->item_req_date))',
                            'type' => 'raw',
                        ),
                        array(
                            'header' => 'REMARKS',
                            'name' => 'item_remarks',
                            'value' => '$data->item_remarks',
                            'type' => 'raw',
                        ),
                    ),
                ));
                Yii::app()->clientScript->registerScript('customGridStyles', "
                    $('#material-requisition-grid thead').addClass('entry-table');
                "); ?>

            </div>
        </div>
    </div>
</div>

<script>
    function getURLParameter(url, name) {
        return (RegExp(name + '=' + '(.+?)(&|$)').exec(url) || [, null])[1];
    }


    function editMRItem(href) {
        var id = getURLParameter(href, 'id');
        $('.add-mr').hide();
        $('.update-mr').show();
        $.ajax({
            "type": 'post',
            "url": "<?php echo Yii::app()->createUrl('MaterialRequisition/getMRItems'); ?>",
            "data": {
                id: id
            },
            "dataType": "json",
            success: function (result) {
                if (result.stat == 1) {
                    $('#hide').show();
                    $('#update_button').show();

                    if (result.item_id == 0) {
                        $('#requisition_item').val('other')
                        $('#item_name_id').show();
                        $('#requisition_item_name').val(result.item_name);
                    } else {
                        $('#requisition_item').val(result.item_id)
                    }
                    $('#requisition_remarks').val(result.item_remarks);
                    $('#requisition_date').val(result.item_date);
                    $('#requisition_quantity').val(result.item_qty);
                    $('#requisition_unit').val(result.item_unit);
                    $('#item_id').val(result.id);
                }
            }
        });
    }
    $("#update_button").click(function () {
        var id = $('#item_id').val();
        var requisition_item = $('#requisition_item').val();
        var requisition_unit = $('#requisition_unit').val();
        var requisition_quantity = $('#requisition_quantity').val();
        var requisition_date = $('#requisition_date').val();
        var requisition_remarks = $('#requisition_remarks').val();
        var requisition_item_name = $('#requisition_item_name').val();
        var material_req_id = $('#material_req_id').val();

        if (requisition_item != "" && requisition_unit != "" && requisition_quantity != "" && requisition_date != "" && requisition_remarks != "") {
            $("#item_em_").hide();
            $("#unit_em_").hide();
            $("#quantity_em_").hide();
            $("#date_em_").hide();
            $("#remarks_em_").hide();
            $.ajax({
                "type": 'post',
                "url": "<?php echo Yii::app()->createUrl('MaterialRequisition/updateItem'); ?>",
                "data": {
                    id: id,
                    requisition_item: requisition_item,
                    requisition_unit: requisition_unit,
                    requisition_quantity: requisition_quantity,
                    requisition_date: requisition_date,
                    requisition_remarks: requisition_remarks,
                    requisition_item_name: requisition_item_name,
                    material_req_id: material_req_id
                },
                "dataType": "json",
                success: function (result) {

                    if (result.status == 1 && result.type == 1) {
                        $("#update_success_message").fadeIn().delay(1000).fadeOut();
                        setTimeout(location.reload.bind(location), 1000);
                    }
                    if (result.status == 1 && result.type == 0) {
                        $("#create_success_message").fadeIn().delay(1000).fadeOut();
                        setTimeout(location.reload.bind(location), 1000);
                    }

                }
            });
        } else {
            if (requisition_item == "") {
                $("#item_em_").show();
                $("#item_em_").html('Item cannot be blank.');
            }
            if (requisition_unit == "") {
                $("#unit_em_").show();
                $("#unit_em_").html('Unit cannot be blank.');
            }
            if (requisition_quantity == "") {
                $("#quantity_em_").show();
                $("#quantity_em_").html('Quantity cannot be blank.');
            }
            if (requisition_date == "") {
                $("#date_em_").show();
                $("#date_em_").html('Quantity cannot be blank.');
            }
            if (requisition_remarks == "") {
                $("#remarks_em_").show();
                $("#remarks_em_").html('Remark cannot be blank.');
            }
            if (requisition_item_name == "") {
                $('#item_name_em_').show();
                $("#item_name_em_").html('item name cannot be blank.');
            }
        }

    });

    $('.req-item').change(function () {
        var item_id = $('.req-item').val();
        if (item_id == 'other') {
            $('#item_name_id').show();
        } else {
            $('#item_name_id').hide();
        }
        $.ajax({
            url: '<?php echo Yii::app()->createUrl('MaterialRequisition/getRelatedUnit'); ?>',
            method: 'POST',
            data: {
                item_id: item_id
            },
            dataType: "json",
            success: function (response) {
                $("#requisition_unit").html(response.html);
            }
        })
    });

    $('#requisition_date').datepicker({
        dateFormat: 'dd-M-y',
    });
</script>