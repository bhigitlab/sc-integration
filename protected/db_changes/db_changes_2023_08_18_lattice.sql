UPDATE `jp_quotation_gen_worktype` SET `revision_no`='REV-1' WHERE `qid`=15 AND `revision_no`='REV-12';
UPDATE `jp_quotation_gen_category` SET `revision_no`='REV-1' WHERE `qid`=15 AND `revision_no`='REV-12';
UPDATE `jp_quotation_section` SET `revision_no` = 'REV-1' WHERE `jp_quotation_section`.`id` = 54;
DELETE FROM `jp_quotation_section` WHERE `id`=51;
DELETE FROM `jp_quotation_gen_category` WHERE `section_id`=51;
DELETE FROM `jp_quotation_gen_worktype` WHERE `section_id`=51;