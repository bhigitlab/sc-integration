<?php

class PurchaseReturnController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
    {
        return array(
            array(
                'allow',  // allow authenticated users to perform 'index' and 'view' actions
                'actions' => array('index', 'view'),
                'users' => array('@'),
            ),
            array(
                'allow', // allow authenticated user to perform other specified actions
                'actions' => array(
                    'create', 
                    'update', 
                    'view',
                    'getVendor', 
                    'updateItemsToList', 
                    'delete', 
                    'admin', // actionAdmin()
                    'getItemsByPurchase', // actionGetItemsByPurchase()
                    'validateReturnNumber', // actionValidateReturnNumber()
                    'addItemsForPurchaseReturn', // actionaddItemsForPurchaseReturn()
                    'Returnpdf',
                ),
                'users' => array('@'),
            ),
            array(
                'allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('admin'),
            ),
            array(
                'deny',  // deny all users
                'users' => array('*'),
            ),
        );
    }
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
            $model       = $this->loadModel($id);
            $newmodel    = PurchaseReturnitem::model()->findAll(array("condition" => "return_id = ".$id));
            $this->render('viewreturnitems',array(
                'model'=>$model,
                'newmodel'=>$newmodel,
            ));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
    {
        $model = new PurchaseReturn;
        $tblpx = Yii::app()->db->tablePrefix;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['PurchaseReturn'])) {
            $model->attributes = $_POST['PurchaseReturn'];
            
            $model->created_by = Yii::app()->user->id;
            // Format the return_date properly
            $model->return_date = date('Y-m-d', strtotime($_POST['PurchaseReturn']['return_date']));

            if ($model->save()) {
                // Update related records if needed
                if (isset($_POST['billid'])) {
                    $id = $_POST["billid"];
                    $newmodel = $this->loadModel($id);
                    $newmodel->return_status = 2;
                    $newmodel->remarks=$_POST['PurchaseReturn']['remarks'];
                    $newmodel->vendor_id=$_POST['PurchaseReturn']['vendor_id'];
                    if ($newmodel->save()) {
                        // Delete duplicate bills
                        $billNumber = $model->return_number;
                        $deleteBills = Yii::app()->db->createCommand()
                            ->delete("{$tblpx}purchase_return", 'return_number = :billNumber AND return_status = 1', [':billNumber' => $billNumber]);
                        
                        Yii::app()->user->setFlash('success', "Purchase Return details added successfully!");
                    }
                }

                // Redirect to the view page
                $this->redirect(array('admin'));
            } else {
                $errors = $model->getErrors();
                echo '<pre>';
                print_r($errors);
                exit;
            }
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    public function actionGetVendor(){
        if (isset($_POST['id'])) {
            $id = $_POST['id'];
            $purchase_return = PurchaseReturn::model()->findAllByAttributes(array('bill_id' => $id));
            $bill_id = Bills::model()->findByAttributes(array('bill_id' => $id));
            $purchase_id=$bill_id->purchase_id;
            $purchase=Purchase::model()->findByAttributes(array('p_id'=>$purchase_id));
            $vendor_id=$purchase->vendor_id;
            $vendor=Vendors::model()->findByAttributes(array('vendor_id'=>$vendor_id));
            $response = array('id' => $vendor_id, 'name' => $vendor->name);

            echo CJSON::encode($response);
        } else {
            echo CJSON::encode(array('error' => 'No ID provided'));
        }
        Yii::app()->end();
    }
    
	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model    = $this->loadModel($id);
                $client   = '';
                $tblpx    = Yii::app()->db->tablePrefix;
                $purchase = Yii::app()->db->createCommand("SELECT * FROM ".$tblpx."purchase_return WHERE return_id = ".$id)->queryRow();
                $bill_Id      = $purchase["bill_id"];
                $client = $this->GetItemsByBillId($id,$bill_Id);
                $vendorId = $model->vendor_id;
                $vendorName = '';
                
                if ($vendorId) {
                    $vendor = Vendors::model()->findByPk($vendorId);
                    if ($vendor) {
                        $vendorName = $vendor->name;
                    }
                }
		if(isset($_POST['PurchaseReturn']))
		{
			$model->attributes=$_POST['PurchaseReturn'];
                        $model->return_date = date('Y-m-d', strtotime($_POST['PurchaseReturn']['return_date']));
                        $model->updated_date = date('Y-m-d');
                        $model->return_status = 2;
			if($model->save())
				$this->redirect(array('admin','id'=>$model->return_id));
		}
		$this->render('update',array(
			'model'=>$model,'client' => $client,'vendorName' => $vendorName,
		));
	}
        
        public function GetItemsByBillId($itemid,$bill_Id){
            $client = "";
            $tblpx = Yii::app()->db->tablePrefix;
            $pData = Yii::app()->db->createCommand("SELECT a.*,b.*,a.category_id as pcategory,b.category_id as bcategory,a.remark as premark,b.remark as bremark, a.billitem_id as billitem_id FROM {$tblpx}billitem a LEFT JOIN {$tblpx}purchase_returnitem b ON a.billitem_id = b.billitem_id and b.return_id = ".$itemid." WHERE a.bill_id = ".$bill_Id." GROUP by a.billitem_id")->queryAll();
            $client = $this->renderPartial('updatepurchaseitems',array(
			'purchase'=> $pData, 'billId' =>$itemid,
		),true);
            return $client;
        }
        
        
        public function actionUpdateItemsToList() {
			
	    $tblpx = Yii::app()->db->tablePrefix;
            $itemId      = $_REQUEST["itemid"];
            $billId      = $_REQUEST["billid"];
            $desc        = $_REQUEST["desc"];
            $quantity    = $_REQUEST["quantity"];
            $unit        = $_REQUEST["unit"];
            $rate        = $_REQUEST["rate"];
            $amount      = $_REQUEST["amount"];
            $billAmt     = $_REQUEST["billamount"];
            $billTAmt    = $_REQUEST["billtotal"];
            $ustatus     = $_REQUEST["ustatus"];
            $updateBills = Yii::app()->db->createCommand("update {$tblpx}purchase_return SET return_amount = ".$billAmt.", return_totalamount = ".$billTAmt." where return_id = ".$billId);
            $updateBills->execute();
//            if($ustatus == 1) {
//                $updateItems = Yii::app()->db->createCommand("update {$tblpx}purchase_items SET bill_id = ".$billId.", or_description = '".$desc."', or_quantity = ".$quantity.", or_unit = '".$unit."', or_rate = ".$rate.", or_amount = ".$amount." where item_id = ".$itemId);
//                $updateItems->execute();
//            } else {
//                $updateItems = Yii::app()->db->createCommand("update {$tblpx}purchase_items SET bill_id = NULL, or_description = NULL, or_quantity = NULL, or_unit = NULL, or_rate = NULL, or_amount = NULL where item_id = ".$itemId);
//                $updateItems->execute();
//            }
            //echo 1;
        }

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('PurchaseReturn');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new PurchaseReturn('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['PurchaseReturn']))
			$model->attributes=$_GET['PurchaseReturn'];

                
		$this->render('admin',array(
			'model'=>$model,
                        'dataProvider' => $model->search(),
		));
	}
        
         public function actionGetItemsByPurchase(){
            $id = $_GET['id'];
            $client = "";
            $pData  = "";
            $pitems = "";
            //$client = $id;
            //$modelp = Bills::model()->getAllPurchaseItemsById($id);
            $tblpx = Yii::app()->db->tablePrefix;
            //$pData = Yii::app()->db->createCommand("select * from ".$tblpx."purchase_item where purchase_id = ".$pId."")->queryAll();
            $pData = Yii::app()->db->createCommand("SELECT * FROM ".$tblpx."billitem WHERE bill_id = ".$id." ORDER BY billitem_id")->queryAll();
            
            //echo json_encode($pData);
            $pitems = $this->renderPartial('purchaseitems',array(
			'purchase'=>$pData,
		));
           echo $pitems;
        }
        
        public function actionValidateReturnNumber() {
            $billNo   = $_GET["billno"];
            $tblpx    = Yii::app()->db->tablePrefix;
            $billData = Yii::app()->db->createCommand("SELECT * FROM ".$tblpx."purchase_return WHERE return_number = '".$billNo."' and return_status = 2 and company_id=".Yii::app()->user->company_id."")->queryAll();
            if($billData) {
                echo 1;
            } else {
                echo 2;
            }
        }
        
        public function actionaddItemsForPurchaseReturn() {
            $tblpx          = Yii::app()->db->tablePrefix;
//            $bill_Id        = $_GET["bill_id"];
//            $returnNumber   = $_GET["returnnumber"];
//            $returnDate     = $_GET["returndate"];
//            $billNTotal     = $_GET["billamount"];
//            $billNDiscount  = $_GET["billdiscount"];
//            $billNTax       = $_GET["billtax"];
//            $billNGTotal    = $_GET["billtotal"];
//            $itemId         = $_GET["itemid"];
//            $quantity       = $_GET["quantity"];
//            $unit           = $_GET["unit"];
//            $rate           = $_GET["rate"];
//            $newAmt         = $_GET["amount"];
//            $damount        = $_GET["damount"];
//            $newDp          = $_GET["dpercent"];
//            $cgst           = $_GET["cgst"];
//            $newCgstP       = $_GET["cgstpercent"];
//            $sgst           = $_GET["sgst"];
//            $newSgstP       = $_GET["sgstpercent"];
//            $igst           = $_GET["igst"];
//            $newIgstP       = $_GET["igstpercent"];
//            $newTotalTax    = $_GET["totaltax"];
//            $newTaxP        = $_GET["totaltaxp"];
//            $newTotal       = $_GET["totalamount"];
//            $billId         = $_GET["billid"];
//            $aStat          = $_GET["astat"];
//            $categoryId     = $_GET["categoryid"];
//            $categoryName   = $_GET["categoryname"];
//            $availQty       = $_GET["availqty"];
//            $billItemId     = $_GET["billitem"];
//            $purchaseStatus = $_GET["purchasestatus"];
            
            $bill_Id        = $_POST["bill_id"];
            $returnNumber   = $_POST["returnnumber"];
            $returnDate     = $_POST["returndate"];
            $billNTotal     = $_POST["billamount"];
            $billNDiscount  = $_POST["billdiscount"];
            $billNTax       = $_POST["billtax"];
            $billNGTotal    = $_POST["billtotal"];
            $itemId         = $_POST["itemid"];
            $quantity       = $_POST["quantity"];
            $unit           = $_POST["unit"];
            $rate           = $_POST["rate"];
            $newAmt         = $_POST["amount"];
            $damount        = $_POST["damount"];
            $newDp          = $_POST["dpercent"];
            $cgst           = $_POST["cgst"];
            $newCgstP       = $_POST["cgstpercent"];
            $sgst           = $_POST["sgst"];
            $newSgstP       = $_POST["sgstpercent"];
            $igst           = $_POST["igst"];
            $newIgstP       = $_POST["igstpercent"];
            $newTotalTax    = $_POST["totaltax"];
            $newTaxP        = $_POST["totaltaxp"];
            $newTotal       = $_POST["totalamount"];
            $billId         = $_POST["billid"];
            $aStat          = $_POST["astat"];
            $categoryId     = $_POST["categoryid"];
            $categoryName   = $_POST["categoryname"];
            $availQty       = $_POST["availqty"];
            $billItemId     = $_POST["billitem"];
            $purchaseStatus = $_POST["purchasestatus"];
            $bills_details  = Bills::model()->findByPk($bill_Id);
            $returnNDate    = date('Y-m-d', strtotime($returnDate));
            $createdBy      = Yii::app()->user->id;
            $company_id     = $bills_details['company_id'];
            $createdDate    = date('Y-m-d');
            if($billId == "") {
                $addBills   = Yii::app()->db->createCommand("INSERT INTO {$tblpx}purchase_return(bill_id, return_number, return_date, return_amount, return_taxamount, return_discountamount, return_totalamount, created_by, created_date, return_status, company_id) VALUES (".$bill_Id.",'".$returnNumber."','".$returnNDate."','".$billNTotal."','".$billNTax."','".$billNDiscount."','".$billNGTotal."',".$createdBy.",'".$createdDate."',1, ".$company_id.")");
                $addBills->execute();
                $billId     = Yii::app()->db->getLastInsertID();
            } else {
                $updateBills = Yii::app()->db->createCommand("update {$tblpx}purchase_return SET bill_id = ".$bill_Id.", return_number = '".$returnNumber."', return_date = '".$returnNDate."', return_amount = '".$billNTotal."', return_taxamount = '".$billNTax."', return_discountamount = '".$billNDiscount."', return_totalamount = '".$billNGTotal."', updated_date = '".date('Y-m-d')."', return_status = 1, company_id= ".$company_id." where return_id = ".$billId);
                $updateBills->execute();
            }
            if($aStat == 1) {
                if($categoryId != '')
                    $updateBillItems = Yii::app()->db->createCommand("INSERT INTO {$tblpx}purchase_returnitem(return_id, billitem_id, returnitem_quantity, returnitem_unit, returnitem_rate, returnitem_amount, returnitem_discountamount, returnitem_discountpercent, returnitem_cgst, returnitem_cgstpercent, returnitem_sgst, returnitem_sgstpercent, returnitem_taxamount, returnitem_taxpercent, category_id, created_date, returnitem_igst, returnitem_igstpercent) VALUES (".$billId.",".$itemId.",'".$quantity."','".$unit."','".$rate."','".$newAmt."','".$damount."','".$newDp."','".$cgst."','".$newCgstP."','".$sgst."','".$newSgstP."','".$newTotalTax."','".$newTaxP."',".$categoryId.",'".$createdDate."','".$igst."','".$newIgstP."')");
                else
                    $updateBillItems = Yii::app()->db->createCommand("INSERT INTO {$tblpx}purchase_returnitem(return_id, billitem_id, returnitem_quantity, returnitem_unit, returnitem_rate, returnitem_amount, returnitem_discountamount, returnitem_discountpercent, returnitem_cgst, returnitem_cgstpercent, returnitem_sgst, returnitem_sgstpercent, returnitem_taxamount, returnitem_taxpercent, remark, created_date, returnitem_igst, returnitem_igstpercent) VALUES (".$billId.",".$itemId.",'".$quantity."','".$unit."','".$rate."','".$newAmt."','".$damount."','".$newDp."','".$cgst."','".$newCgstP."','".$sgst."','".$newSgstP."','".$newTotalTax."','".$newTaxP."','".$categoryName."','".$createdDate."','".$igst."','".$newIgstP."')");
                $updateBillItems->execute();
                $billItemId  = Yii::app()->db->getLastInsertID();
                $quantityRem = $availQty - $quantity;
            } else if($aStat == 2){
                $updateBillItems = Yii::app()->db->createCommand("DELETE FROM {$tblpx}purchase_returnitem WHERE return_id = ".$billId." and billitem_id = ".$itemId);
                $updateBillItems->execute();
                $quantityRem = $availQty;
            } else if($aStat == 3) {
                if($categoryId != '')
                    $updateBillItems = Yii::app()->db->createCommand("UPDATE {$tblpx}purchase_returnitem SET returnitem_quantity = '".$quantity."', returnitem_unit = '".$unit."', returnitem_rate = '".$rate."', returnitem_amount = '".$newAmt."', returnitem_discountamount = '".$damount."', returnitem_discountpercent = '".$newDp."', returnitem_cgst = '".$cgst."', returnitem_cgstpercent = '".$newCgstP."', returnitem_sgst = '".$sgst."', returnitem_sgstpercent = '".$newSgstP."', returnitem_igst = '".$igst."', returnitem_igstpercent = '".$newIgstP."', returnitem_taxamount = '".$newTotalTax."', returnitem_taxpercent = '".$newTaxP."', category_id = ".$categoryId." WHERE return_id = ".$billId." and billitem_id = ".$itemId);
                else
                    $updateBillItems = Yii::app()->db->createCommand("UPDATE {$tblpx}purchase_returnitem SET returnitem_quantity = '".$quantity."', returnitem_unit = '".$unit."', returnitem_rate = '".$rate."', returnitem_amount = '".$newAmt."', returnitem_discountamount = '".$damount."', returnitem_discountpercent = '".$newDp."', returnitem_cgst = '".$cgst."', returnitem_cgstpercent = '".$newCgstP."', returnitem_sgst = '".$sgst."', returnitem_sgstpercent = '".$newSgstP."', returnitem_igst = '".$igst."', returnitem_igstpercent = '".$newIgstP."', returnitem_taxamount = '".$newTotalTax."', returnitem_taxpercent = '".$newTaxP."', remark = '".$categoryName."' WHERE return_id = ".$billId." and billitem_id = ".$itemId);
                $updateBillItems->execute();
                $quantityRem = $availQty - $quantity;
            } else {
                $updateBillItems = Yii::app()->db->createCommand("DELETE FROM {$tblpx}purchase_returnitem WHERE return_id = ".$billId." and billitem_id = ".$itemId);
                $updateBillItems->execute();
                $quantityRem = $availQty;
            }
            if($quantityRem > 0)
                $itemStatus = 90;
            else
                $itemStatus = 91;
//            $updateItems    = Yii::app()->db->createCommand("UPDATE {$tblpx}purchase_items SET item_status = ".$itemStatus." WHERE item_id =".$itemId);
//            $updateItems->execute();
//            $updatePurchase = Yii::app()->db->createCommand("UPDATE {$tblpx}purchase SET purchase_billing_status = ".$purchaseStatus." WHERE p_id =".$purchaseId);
//            $updatePurchase->execute();
            $result[0] = $billId;
            $result[1] = $billItemId;
            echo json_encode($result);
        }

        public function actionUpdateBillsOnReload() {
	    $tblpx    = Yii::app()->db->tablePrefix;
            $billId      = $_REQUEST["billid"];
            $billNumber  = $_REQUEST["billnumber"];
            $updateBills = Yii::app()->db->createCommand("UPDATE {$tblpx}purchase_return SET return_status = 2 WHERE bill_id =".$billId);
            $updateBills->execute();
            $deleteBills = Yii::app()->db->createCommand("DELETE FROM {$tblpx}purchase_return WHERE return_number = '".$billNumber."' and return_status = 1");
            $deleteBills->execute();
            echo 1;
        }
        
        public function actionReturnpdf($id){
            $this->logo = $this->realpath_logo;
            $model       = $this->loadModel($id);
            $newmodel    = PurchaseReturnitem::model()->findAll(array("condition" => "return_id = ".$id));
            
            $mPDF1        = Yii::app()->ePdf->mPDF();

            $mPDF1        = Yii::app()->ePdf->mPDF('', 'A4');

            $sql = 'SELECT template_name '
                        . 'FROM jp_quotation_template '
                        . 'WHERE status="1"';
            $selectedtemplate =Yii::app()->db->createCommand($sql)->queryScalar();
                
             $template = $this->getTemplate($selectedtemplate);
            $mPDF1->SetHTMLHeader($template['header']);
            $mPDF1->SetHTMLFooter($template['footer']);
                
            $sql = 'SELECT template_name '
                        . 'FROM jp_quotation_template '
                        . 'WHERE status="1"';
			$selectedtemplate =Yii::app()->db->createCommand($sql)->queryScalar();
			if ($selectedtemplate == 'template1'){
				$mPDF1->AddPage('','', '', '', '', 0,0,50, 30, 10,0);
			}else{                    
				$mPDF1->AddPage('','', '', '', '', 0,0,40, 30, 10,0);
			}
            $vendor=Vendors::model()->findByAttributes(array('vendor_id'=>$model['vendor_id']));

            $mPDF1->WriteHTML($this->renderPartial('returnview', array(
                'model' => $model, 'newmodel' => $newmodel,'vendor'=>$vendor), true));

            $mPDF1->Output('Purchasereturn.pdf', 'D');
        }
        
        public function actionreturncsv($id){
            $model       = $this->loadModel($id);
            $newmodel    = PurchaseReturnitem::model()->findAll(array("condition" => "return_id = ".$id));
            $tblpx = Yii::app()->db->tablePrefix;
            $billId     = $model['bill_id'];
            $bill_details=Yii::app()->db->createCommand("select * FROM ".$tblpx."bills WHERE bill_id=".$billId)->queryRow();
            
            $arraylabel = array('Bill NO','RETURN NO','DATE');
            $finaldata = array();
            $finaldata[0][]=$bill_details['bill_number'];
            $finaldata[0][]=$model['return_number'];
            $finaldata[0][]=date("d-m-Y", strtotime($model['return_date']));


            $finaldata[1][] = '';
            $finaldata[1][] = '';
            $finaldata[1][] = '';

            $finaldata[2][] = 'Sl.No';
            $finaldata[2][] = 'Specification/Remark';
            $finaldata[2][] = 'Quantity';
            $finaldata[2][] = 'Unit';
            $finaldata[2][] = 'Rate';
            $finaldata[2][] = 'Amount';
            $finaldata[2][] = 'Discount';
            $finaldata[2][] = 'Discount (%)';
            $finaldata[2][] = 'CGST';
            $finaldata[2][] = 'CGST (%)';
            $finaldata[2][] = 'SGST';
            $finaldata[2][] = 'SGST (%)';
            $finaldata[2][] = 'IGST';
            $finaldata[2][] = 'IGST (%)';
            $finaldata[2][] = 'Total Tax';
            $finaldata[2][] = 'Total Tax (%)';
            $finaldata[2][] = 'Total Amount';
            $i=0;
            if(!empty($newmodel))
            {
                    foreach($newmodel as $k=>$new){
                        $i++;
                        if($new['category_id']) {
                            $categorymodel = Specification::model()->findByPk($new['category_id']);
                            if($categorymodel)
                                $categoryName = $categorymodel->specification;
                            else
                                $categoryName = "Not available";
                        } else {
                           $categoryName  = $new['remark'];
                        }

                    $finaldata[$k+3][]=$i;
                    $finaldata[$k+3][]=$categoryName;
                    $finaldata[$k+3][]=$new['returnitem_quantity'];
                    $finaldata[$k+3][]=$new['returnitem_unit'];
                    $finaldata[$k+3][]=Controller::money_format_inr($new['returnitem_rate'],2,1);
                    $finaldata[$k+3][]=Controller::money_format_inr($new['returnitem_amount'],2,1);
                    $finaldata[$k+3][]=Controller::money_format_inr($new['returnitem_discountamount'],2,1);
                    $finaldata[$k+3][]=$new['returnitem_discountpercent'];
                    $finaldata[$k+3][]=Controller::money_format_inr($new['returnitem_cgst'],2,1);
                    $finaldata[$k+3][]=$new['returnitem_cgstpercent'];
                    $finaldata[$k+3][]=Controller::money_format_inr($new['returnitem_sgst'],2,1);
                    $finaldata[$k+3][]=$new['returnitem_sgstpercent'];
                    $finaldata[$k+3][]=Controller::money_format_inr($new['returnitem_igst'],2,1);
                    $finaldata[$k+3][]=$new['returnitem_igstpercent'];
                    $finaldata[$k+3][]=Controller::money_format_inr($new['returnitem_taxamount'],2,1);
                    $finaldata[$k+3][]=Controller::money_format_inr($new['returnitem_taxpercent'],2,1);
                    $finaldata[$k+3][]=Controller::money_format_inr(($new['returnitem_amount'] - $new['returnitem_discountamount']) + $new['returnitem_taxamount'],2,1);
                    $x=$k+3;
                    }
                    $finaldata[$x+1][] = '';
                    $finaldata[$x+1][] = '';
                    $finaldata[$x+1][] = '';


                    $finaldata[$x+2][] = '';
                    $finaldata[$x+2][] = '';
                    $finaldata[$x+2][] = '';
  

                    $finaldata[$x+3][] = 'Amount';
                    $finaldata[$x+3][] = Controller::money_format_inr($model['return_amount'],2,1);

                    $finaldata[$x+4][] = 'Discount';
                    $finaldata[$x+4][] = Controller::money_format_inr($model['return_discountamount'],2,1);

                    $finaldata[$x+5][] = 'Tax';
                    $finaldata[$x+5][] = Controller::money_format_inr($model['return_taxamount'],2,1);

                    $finaldata[$x+6][] = 'Grand Total';
                    $finaldata[$x+6][] = Controller::money_format_inr($model['return_totalamount'],2,1);
            }
            
            Yii::import('ext.ECSVExport');
            $csv = new ECSVExport($finaldata);
            $csv->setHeaders($arraylabel);
            $csv->setToAppend();
            $contentdata = $csv->toCSV();
            $csvfilename = 'Purchasereturn' . date('d_M_Y') . '.csv';
            Yii::app()->getRequest()->sendFile($csvfilename, $contentdata, "text/csv", false);
            exit();
        }
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return PurchaseReturn the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=PurchaseReturn::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param PurchaseReturn $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='purchase-return-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
