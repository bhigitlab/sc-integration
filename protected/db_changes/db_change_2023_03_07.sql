INSERT INTO `jp_menu` (`menu_id`, `menu_name`, `parent_id`, `status`, `controller`, `action`, `params`, `showmenu`, `show_list`, `related`) VALUES ('787', 'Delete', '233', '0', 'subcontractor', 'subcontractordelete', '', '2', '1', NULL);

ALTER TABLE `jp_scquotation` ADD CONSTRAINT `fk_subcontrator_id` FOREIGN KEY (`subcontractor_id`) REFERENCES `jp_subcontractor`(`subcontractor_id`) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `jp_dailyreport` ADD CONSTRAINT `fk_labour_sc_id` FOREIGN KEY (`subcontractor_id`) REFERENCES `jp_subcontractor`(`subcontractor_id`) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `jp_subcontractor_payment` ADD CONSTRAINT `fk_payment_sc_id` FOREIGN KEY (`subcontractor_id`) REFERENCES `jp_subcontractor`(`subcontractor_id`) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `jp_subcontractor` ADD `delete_approve_status` ENUM('0', '1') NOT NULL DEFAULT '0' COMMENT '0=>approved,1=>Pending for approval' AFTER `created_by`;

ALTER TABLE `jp_subcontractor` CHANGE `subcontractor_email` `subcontractor_email` VARCHAR(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL;
