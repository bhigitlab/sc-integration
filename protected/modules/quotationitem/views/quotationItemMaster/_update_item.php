
<style>
  /* Style for the Remove Image button */
  #remove-image-button {
    padding: 0px 1px;
    background-color: red;
    color: white;
    border: none;
    border-radius: 4px;
    cursor: pointer;
  }
  </style>
<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'item-form-edit',
    'action' => Yii::app()->createAbsoluteUrl("quotationitem/QuotationItemMaster/update", array('id' => $model->id)),
    'htmlOptions' => array('class' => 'form-inline', 'enctype' => 'multipart/form-data'),
    'enableAjaxValidation' => false,
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
        'validateOnChange' => true,
    ///'validateOnType' => false,
    ),
        ));
?>
<div class="panel-body">
    <?php
    $items = Yii::app()->controller->GetItemCategory();
    $work_type_id = $model->work_type_id;
    $worktype = QuotationWorktype::model()->findByPk($work_type_id);
    $category = QuotationCategoryMaster::model()->findByPk($model->quotation_category_id);
   
    $shutter = "";
   
    foreach ($model->shutter_material_id as $arr) {
        if ($shutter) $shutter .= ' OR';
            $shutter .= " FIND_IN_SET('" . $arr . "', material_id)";
    }
    
    $material = "";
   
    foreach ($model->material_ids as $arr) {
        if ($material) $material .= ' OR';
            $material .= " FIND_IN_SET('" . $arr . "', material_id)";
    }
   

    $sql ="SELECT id,count(*) AS ratecount "
        . " FROM `jp_quotation_item_rate_master` "                
        . " WHERE `master_category_id` = $model->quotation_category_id "
        . " AND ($shutter OR $material)";
    $rate_master_count = Yii::app()->db->createCommand($sql)->queryRow();

    $readonly='';
    $text ="";
    if($rate_master_count['ratecount'] > 0){
        $readonly='readonly';
        $text =  CHtml::link('Edit Rate', Yii::app()->createUrl('quotationitem/quotationItemMaster/rateindex&id='.$rate_master_count["id"]),array('target'=>'_blank')); 
    }
                
    ?>
    <div class="panel " style="padding-left:30px;padding-right:30px;">
        <span id="error"></span>
        <div class="row addRow">
            <div class="col-md-2" style="margin-bottom:30px">
                <?php echo CHtml::hiddenField('QuotationItemMaster[quotation_category_id]', $model->quotation_category_id, array('id' => 'hiddencat')); ?>
                <?php echo $form->labelEx($model, 'Category'); ?>
                <?php echo $category->name ?>
            </div>
            <div class="col-md-2">
                <?php echo CHtml::hiddenField('QuotationItemMaster[work_type_id]', $work_type_id, array('id' => 'hiddenworktype')); ?>
                <?php echo $form->labelEx($model, 'Work Type'); ?>
                <?php echo $worktype->name; ?>

            </div>
            <div class="col-md-2">
                <?php echo $form->labelEx($model, 'Work Type Label'); ?>
                <?php echo $form->textField($model, 'worktype_label', array('class' => 'form-control worktypelabel')); ?>
                <?php echo $form->error($model, 'worktype_label'); ?>
            </div>
            <div class="col-md-2 col-sm-6" id="image-view">
                <?php  ?>
                    <?php if($model->image ==''){
                        echo $form->labelEx($model, 'image', ['label' => 'Upload New Image']); 
                       }else{echo $form->labelEx($model, 'image', ['label' => 'Replace Image']);
                    }?>
                   <?php echo $form->fileField($model, 'image', array('id' => 'file')); ?>
                    <?php echo $form->error($model, 'image'); ?>
                </div>
                <?php if($model->image !=''){?>
                <div class="col-md-2 col-sm-6" id="exist-image-view">
                <label>Existing image</label>
                <img id="existing-image" src="<?php echo Yii::app()->request->baseUrl . "/uploads/image/" . $model->image; ?>" style="max-height: 45px;" alt="Existing Image">
                <button id="remove-image-button" type="button">Remove Image</button>
                <input type="hidden" id="image_remove" value=""></div>
                <?php } ?>
            <?php if ($worktype['template_id'] == 3) { ?>

                <div class="col-md-3">
                    <?php echo $form->labelEx($model, 'Material'); ?>
                    <?php echo $form->dropDownList($model, 'material_ids', CHtml::listData($items, 'id', 'data'), array('class' => 'form-control js-example-basic-multiple require material', 'multiple' => "multiple", 'id' => 'material_ids', 'empty' => '-Select material-', 'style' => 'width:100%')); ?>
                    <?php echo $form->error($model, 'material_ids'); ?>
                </div>
                <div class="col-md-3">
                    <?php echo $form->labelEx($model, 'Finish'); ?>
                    <?php echo $form->dropDownList($model, 'finish_id', CHtml::listData(QuotationFinishMaster::model()->findAll(), 'id', 'name'), array('class' => 'form-control js-example-basic-single  boxwork field_change require finish', 'empty' => '-Select Finish-', 'id' => 'finish_id', 'style' => 'width:100%')); ?>
                    <?php echo $form->error($model, 'finish_id'); ?>
                </div>
            <?php } ?>
        </div>
        <?php if ($worktype['template_id'] == 1) { ?>
            <div class="row addRow row-margin">
                <div class="col-md-2">
                    <?php echo $form->labelEx($model, 'Shutter Work'); ?>
                </div>

                <div class="col-md-3">

                    <?php echo $form->labelEx($model, 'Material'); ?>
                    <?php echo $form->dropDownList($model, 'shutter_material_id', CHtml::listData($items, 'id', 'data'), array('class' => 'form-control js-example-basic-multiple require shuttermaterial', 'multiple' => "multiple", 'id' => 'shutter_material', 'style' => 'width:100%')); ?>
                    <?php echo $form->error($model, 'shutter_material_id'); ?>
                </div>
                <div class="col-md-3">
                    <?php echo $form->labelEx($model, 'Finish'); ?>
                    <?php echo $form->dropDownList($model, 'shutter_finish_id', CHtml::listData(QuotationFinishMaster::model()->findAll(), 'id', 'name'), array('class' => 'form-control js-example-basic-single boxwork field_change require shutterfinish ', 'id' => 'shutter_finish_1', 'empty' => '-Select finish-', 'style' => 'width:100%')); ?>
                    <?php echo $form->error($model, 'shutter_finish_id'); ?>
                </div>
                <div class="col-md-4">
                    <?php echo $form->labelEx($model, 'Description'); ?>
                    <?php echo $form->textArea($model, 'shutterwork_description', array('class' => 'form-control boxwork shutterworkdesc', 'placeholder' => 'Description')); ?>
                    <?php echo $form->error($model, 'shutterwork_description'); ?>
                </div>
            </div>

            <div class="row addRow row-margin">
                <div class="col-md-2">
                    <?php echo $form->labelEx($model, 'Carcass Box Work'); ?>
                </div>
                <div class="col-md-3">
                    <?php echo $form->labelEx($model, 'Material'); ?>
                    <?php echo $form->dropDownList($model, 'carcass_material_id', CHtml::listData($items, 'id', 'data'), array('class' => 'form-control js-example-basic-multiple require caracassmaterial', 'empty' => '-Select material-', 'multiple' => "multiple", 'id' => 'caracoss_material', 'style' => 'width:100%')); ?>
                    <?php echo $form->error($model, 'carcass_material_id'); ?>
                </div>

                <div class="col-md-3">
                    <?php echo $form->labelEx($model, 'Finish'); ?>
                    <?php echo $form->dropDownList($model, 'carcass_finish_id', CHtml::listData(QuotationFinishMaster::model()->findAll(), 'id', 'name'), array('class' => 'form-control js-example-basic-single  boxwork field_change require caracassfinish', 'empty' => '-Select Finish-', 'id' => 'caracoss_finish', 'style' => 'width:100%')); ?>
                    <?php echo $form->error($model, 'carcass_finish_id'); ?>
                </div>
                <div class="col-md-4">
                    <?php echo $form->labelEx($model, 'Description'); ?>
                    <?php echo $form->textArea($model, 'caracoss_description', array('class' => 'form-control boxwork carcassdesc', 'id' => 'caracoss_description', 'placeholder' => 'Description')); ?>
                    <?php echo $form->error($model, 'caracoss_description'); ?>
                </div>

            </div>
        <?php } ?>
        <div class="row addRow row-margin">
            <?php if ($worktype['template_id'] != 1) { ?>
                <div class="col-md-3">
                    <?php echo $form->labelEx($model, 'Description'); ?>
                    <?php echo $form->textArea($model, 'description', array('class' => 'form-control description', 'placeholder' => 'Description', 'id' => 'description')); ?>
                    <?php echo $form->error($model, 'description'); ?>
                </div>
            <?php } else { ?>
                <div class="col-md-2"></div>
            <?php } ?>
            <div class="col-md-3">
                <?php echo $form->labelEx($model, 'Rate'); ?>
                <?php echo $form->textField($model, 'rate', array('class' => 'form-control allownumericdecimal ratedata', 'placeholder' => 'Rate','readonly'=>$readonly)); 
                echo $text;
                ?>
                <?php echo $form->error($model, 'rate'); ?>
            </div>
            <div class="col-md-3">
                <?php echo $form->labelEx($model, 'Profit'); ?>
                <?php echo $form->textField($model, 'profit', array('class' => 'form-control allownumericdecimal profitdata', 'placeholder' => 'Profit %')); ?>
                <?php echo $form->error($model, 'profit'); ?>
            </div>
            <div class="col-md-3">
                <?php echo $form->labelEx($model, 'Discount'); ?>
                <?php echo $form->textField($model, 'discount', array('class' => 'form-control allownumericdecimal discountdata', 'placeholder' => 'Discount')); ?>
                <?php echo $form->error($model, 'discount'); ?>
            </div>
        </div><br>

        <div class="save-btnHold text-center button-panel" style="padding-bottom: 10px;">
            <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-info', 'id' => 'additem')); ?>
            <?php
            if (!$model->isNewRecord) {
                echo CHtml::ResetButton('Close', array('onclick' => 'closeaction(this,event);', 'class' => 'btn'));
            } else {
                echo CHtml::ResetButton('Reset', array('style' => 'margin-right:3px;', 'class' => 'btn btn-default'));
                echo CHtml::ResetButton('Close', array('onclick' => 'closeaction(this,event);', 'class' => 'btn'));
            }
            ?>
        </div>
        <?php $this->endWidget(); ?>
    </div>
</div>

<script>
    $(".allownumericdecimal").keydown(function (event) {
        if (event.shiftKey == true) {
            event.preventDefault();
        }
        if ((event.keyCode >= 48 && event.keyCode <= 57) ||
                (event.keyCode >= 96 && event.keyCode <= 105) ||
                event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 ||
                event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190 || event.keyCode == 110 || event.keyCode == 13) {
            var splitfield = $(this).val().split(".");
            if ((splitfield[1] !== undefined && splitfield[1].length >= 2) && event.keyCode != 8 && event.keyCode != 0 && event.keyCode != 13) {
                event.preventDefault();
            }
        } else {
            event.preventDefault();
        }

        if ($(this).val().indexOf('.') !== -1 && (event.keyCode == 190 || event.keyCode == 110))
            event.preventDefault();

    });
    function closeaction() {
        $("#item-form-edit").hide();

    }
    $(function () {
        $('.js-example-basic-multiple').select2({
            width: '100%',
        });
    });
</script>
<script>
  document.getElementById('remove-image-button').addEventListener('click', function() {
    // Clear the file input value and hide the existing image and the remove button
    document.getElementById('file').value = '';
    document.getElementById('existing-image').style.display = 'none';
    this.style.display = 'none';
    document.getElementById('image_remove').value = 1;
  });

  document.getElementById('file').addEventListener('change', function() {
    var input = this;
    var output = document.getElementById('existing-image');
    var removeButton = document.getElementById('remove-image-button');
    
    if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function(e) {
        output.setAttribute('src', e.target.result);
        output.style.display = 'inline-block';
        removeButton.style.display = 'inline-block';
      };

      reader.readAsDataURL(input.files[0]);
    } else {
      output.style.display = 'none';
      removeButton.style.display = 'none';
    }
  });
</script>
