<?php
/* @var $this ExpenseTypeController */
/* @var $model ExpenseType */

$this->breadcrumbs=array(
	'Expense Types'=>array('index'),
	'Create',
);

/*$this->menu=array(
	array('label'=>'List ExpenseType', 'url'=>array('index')),
	array('label'=>'Manage ExpenseType', 'url'=>array('admin')),
);*/
?>

<div class="modal-dialog modal-sm">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" onclick="javascript:window.location.reload()">&times;</button>
            <h4 class="modal-title">Add Category</h4>
        </div>
        <?php echo $this->renderPartial('_form', array('model' => $model, 'category' => $category)); ?>
    </div>

</div>
