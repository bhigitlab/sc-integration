<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<!--<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> -->
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.min.js"></script>
<div class="page_filter clearfix custom-form-style">

        <?php $form=$this->beginWidget('CActiveForm', array(
                'action'=>Yii::app()->createUrl($this->route),
                'method'=>'get',
        )); ?>

        <?php
        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        $newQuery = "";
        foreach($arrVal as $arr) {
            if ($newQuery) $newQuery .= ' OR';
            $newQuery .= " FIND_IN_SET('".$arr."', id)";
        }
        $companyInfo = Company::model()->findAll(array('condition' => $newQuery));
        ?>
        <div class="row">  
          <div class="form-group col-xs-12 col-md-2 filter_elem">
            <label>Company </label>
            <select style="padding: 2.5px 0px;" id="company_id" class="select_box"  name="company_id">
                <option value="">Select company</option>
                <?php
                if(!empty($companyInfo)) {
                    foreach($companyInfo as $key => $value){
                ?>
                        <option value="<?php echo $value['id'] ?>" <?php echo ($value['id'] == $model->company_id)?'selected':''; ?>><?php echo $value['name']; ?></option>

                <?php  } } ?>
            </select>
          </div>
          <div class="form-group col-xs-12 col-md-2 filter_elem">
            <label>Project </label>
            <select style="padding: 2.5px 0px;" id="projectid" class="select_box" name="project_id" class="sel">
                <option value="">Select project</option>
                <?php
                if(!empty($project)) {
                        foreach($project as $key => $value){
                ?>
                <option value="<?php echo $value['pid'] ?>" <?php echo ($value['pid'] == $model->project_id)?'selected':''; ?>><?php echo $value['name']; ?></option>
                <?php } } ?>
            </select>
          </div>
          <div class="form-group col-xs-12 col-md-2 filter_elem">
            <label>Expense Head </label>
              <select style="padding: 2.5px 0px;" id="expense_head" class="select_box" name="expensehead_id" class="sel">
                  <option value="">Select Expense Head</option>
                  <?php
                  if(!empty($expense)) {
                        foreach($expense as $value) {
                      ?>
                        <option value="<?php echo $value['type_id']; ?>" <?php echo($value['type_id'] == $model->expensehead_id)?'selected':''; ?>><?php echo $value['type_name']; ?></option>

                      <?php
                      } }
                  ?>
              </select>
            </div>
          <div class="form-group col-xs-12 col-md-2 filter_elem">
            <label>Vendor </label>
            <select style="padding: 2.5px 0px;" id="vendor_id" class="select_box"  name="vendor_id">
                <option value="">Select vendor</option>
                <?php
                if(!empty($vendor)) {
                    foreach($vendor as $key => $value){
                        ?>
                          <option value="<?php echo $value['vendor_id'] ?>" <?php echo ($value['vendor_id'] == $model->vendor_id)?'selected':''; ?>><?php echo $value['name']; ?></option>

                          <?php }	} ?>
            </select>
          </div>
          <div class="form-group col-xs-12 col-md-2 filter_elem">
            <label>Purchase no </label>
            <select style="padding: 2.5px 0px;" id="purchase_no" class="select_box"  name="purchase_no">
                <option value="">Select purchase no</option>
                <?php
                if(!empty($purchase_no)) {
                        foreach($purchase_no as $key => $value){
                ?>
                <option value="<?php echo $value['purchase_no'] ?>" <?php echo ($value['purchase_no'] == $model->purchase_no)?'selected':''; ?>><?php echo $value['purchase_no'].' - '.date('M-Y',strtotime($value['purchase_date'])); ?></option>
                <?php } } ?>
            </select>
          </div>
          <div class="form-group col-xs-12 col-md-2 filter_elem">
             <label>Status </label>
            <select style="padding: 2.5px 0px;" id="purchase_status" class="select_box"  name="purchase_status">
                <option value="">Select status</option>
                <option value="draft" <?php echo ('draft' == $model->purchase_status)?'selected':''; ?>>Draft</option>
                <option value="saved" <?php echo ('saved' == $model->purchase_status)?'selected':''; ?>>Saved</option>
                <option value="permission_needed" <?php echo ('permission_needed' == $model->purchase_status)?'selected':''; ?>>Permission Needed</option>
            </select>
          </div>
          <div class="form-group col-xs-12 col-md-2 filter_elem">
            <label>Select Billing Status </label>
            <select style="padding: 2.5px 0px;" id="purchase_billing_status" class="select_box"  name="purchase_billing_status">
                <option value="">Select billing status</option>
                <?php
                $tblpx = Yii::app()->db->tablePrefix;
                  $billing_status  = Yii::app()->db->createCommand("SELECT sid,caption FROM {$tblpx}status WHERE status_type='purchase_bill_status'")->queryAll();
                foreach($billing_status as $value){
                ?>
                <option value="<?php echo $value['sid']; ?>" <?php echo ($value['sid'] == $model->purchase_billing_status)?'selected':''; ?>><?php echo $value['caption']; ?></option>
                <?php } ?>
            </select>
          </div>
          <?php
            if (!isset($_GET['date_from']) || $_GET['date_from'] == '') {
               // $datefrom = date("Y-m-") . "01";
                //$datefrom = date("Y-")."01-" . "01";
                //$datefrom = date('d-m-Y', strtotime('today - 30 days'));
                $dateto = date("Y-m-d");
                $datefrom = date("d-m-Y", strtotime("-6 months"));
               
            } else {
                $datefrom = $_GET['date_from'];
            }
            ?>
          <div class="form-group col-xs-12 col-md-2">
              <label >From </label>
              <?php echo CHtml::textField('date_from', $datefrom, array('value' => $datefrom, 'readonly' => true,'class'=>'form-control w-100')); ?>
          </div>
            <?php
            if (!isset($_GET['date_to']) || $_GET['date_to'] == '') {
                //$date_to = date("Y-m-d");
                //$date_to = date("d-m-Y");
                $date_to    = date("d-m-Y");
            } else {
                $date_to = $_GET['date_to'];
            }
            ?>
          <div class="form-group col-xs-12 col-md-2">
              <label >To </label>
                  <?php echo CHtml::textField('date_to', $date_to, array( 'value' => $date_to, 'readonly' => true,'class'=>'form-control w-100')); ?>
          </div>
          <div class="form-group col-xs-12 col-md-2 margin-top-20">
            <input name="yt0" value="Go" type="submit" class=" btn btn-sm btn-primary" id="searchsubmit">
            <input id="reset" name="yt1" value="Clear" type="reset" class=" btn btn-sm btn-default">
          </div>
        </div>
    <?php $this->endWidget(); ?>
</div>
<script>
	$('#reset').click(function(){
		location.href='<?php echo $this->createUrl('admin'); ?>';
	})
	 $( function() {
           // $( "#date_from" ).datepicker({dateFormat: 'dd-mm-yy'}).datepicker("setDate", new Date());

           // $( "#date_to" ).datepicker({dateFormat: 'dd-mm-yy'}).datepicker("setDate", new Date());

    } );

		 $(document).ready(function() {
      
		 $(".select_box").select2();
                 $(function () {
                    $( "#date_from" ).datepicker({dateFormat: 'dd-mm-yy'});

                     $( "#date_to" ).datepicker({dateFormat: 'dd-mm-yy'});

                });
                 $("#date_from").change(function() {
                    $("#date_to").datepicker('option', 'minDate', $(this).val());
                });
                $("#date_to").change(function() {
                    $("#date_from").datepicker('option', 'maxDate', $(this).val());
                });

		});


 /* $("#expense_head").change(function(){
	var val =$(this).val();
	if(val == '') {
	  $("#vendor_id").html('<option value="">Select Vendor</option>');
	} else {
		$.ajax({
				url:'<?php echo Yii::app()->createUrl('purchase/dynamicvendor'); ?>',
				method:'POST',
				data:{exp_id:val},
				dataType:"json",
				success:function(response){
						$("#vendor_id").html(response.html);
				}

		})
	}
})



$("#projectid").change(function(){
	var val =$(this).val();
	if(val == '') {
		$("#vendor_id").html('<option value="">Select Vendor</option>');
		$("#expense_head").html('<option value="">Select Expense Head</option>');
	} else {
		$.ajax({
				url:'<?php echo Yii::app()->createUrl('purchase/dynamicexpensehead'); ?>',
				method:'POST',
				data:{project_id:val},
				dataType:"json",
				success:function(response){
					if(response.status == 'success'){
						$("#expense_head").html(response.html);
						$("#vendor_id").html('<option value="">Select Vendor</option>');
					} else{
						$("#expense_head").html(response.html);
						$("#vendor_id").html('<option value="">Select Vendor</option>');
					}
				}

		})
	}
}) */

</script>
<style>
.search-form{
  background-color: #fafafa;
  padding: 10px;
  box-shadow: 0px 0px 2px 0px rgba(0,0,0,0.1);
  margin-bottom: 10px;
}
.search-form input , .search-form select{
    margin-bottom: 10px;
}
</style>
