<?php
$tblpx = Yii::app()->db->tablePrefix;
if ($index == 0) {
?>
    <thead>
        <tr>
            <?php
            if ($data->voucher->voucher_for == 1) {
            ?>
                <th>Work Label</th>
                <th>No Of Workers</th>

            <?php
            }
            ?>
            <th>Description</th>
            <th>Work Timing/Size</th>
            <th>Net Qty</th>
            <th>Unit</th>

            <?php
            if ((isset(Yii::app()->user->role) && ((in_array('/vendors/paymentedit', Yii::app()->user->menuauthlist)) || (in_array('/vendors/paymentdelete', Yii::app()->user->menuauthlist))))) {
            ?>
                <th></th>
            <?php } ?>

        </tr>
    </thead>
<?php } ?>
<tr id="<?php echo $index; ?>" class="" title="">
    <?php
    if ($data->voucher->voucher_for == 1) {
    ?>
        <td class="pro_back wrap"><?php echo $data->worker_label; ?></td>
        <td class="pro_back wrap"><?php echo $data->no_workers; ?></td>
    <?php } ?>
    <td class="pro_back wrap"><?php echo $data->description; ?></td>
    <td class="pro_back wrap"><?php echo $data->working_time_size; ?></td>
    <td class="pro_back wrap"><?php echo $data->net_quantity . ' '; ?></td>
    <td class="pro_back wrap"><?php echo $data->unit0->unit_name . ' '; ?></td>
    <td class="pro_back">
        <span class="icon icon-options-vertical popover-test" data-toggle="popover" data-placement="left" type="button" data-html="true" style="cursor: pointer;"></span>
        <div class="popover-content hide">
            <ul class="tooltip-hiden">
                <?php
                if ((in_array('/vendors/paymentedit', Yii::app()->user->menuauthlist))) {
                ?>
                    <li><button id="<?php echo $data->id; ?>" class="btn btn-xs btn-default edit_item">Edit</button></li>
                <?php }
                if ((in_array('/vendors/paymentdelete', Yii::app()->user->menuauthlist))) {
                ?>
                    <li><button id="<?php echo $data->id; ?>" class="btn btn-xs btn-default delete_item">Delete</button></li>
                <?php } ?>
            </ul>
        </div>
    </td>
</tr>
<script>
    $(document).ready(function() {
        $(".popover-test").popover({
            html: true,
            content: function() {
                return $(this).next('.popover-content').html();
            }
        });
        $('[data-toggle=popover]').on('click', function(e) {
            $('[data-toggle=popover]').not(this).popover('hide');
        });
        $('body').on('hidden.bs.popover', function(e) {
            $(e.target).data("bs.popover").inState.click = false;
        });
        $('body').on('click', function(e) {
            $('[data-toggle=popover]').each(function() {
                if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                    $(this).popover('hide');
                }
            });
        });

        $(document).on('click', '.edit_item', function() {
            var voucher_id = $('#Vouchers_id').val();
            var item_id = $(this).attr('id');
            var url = "<?php echo Yii::app()->createAbsoluteUrl("vouchers/update") ?>";
            location.href = url + '&id=' + voucher_id + '&item=' + item_id;
        })

    });
</script>