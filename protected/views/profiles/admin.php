<?php
/* @var $this ProfilesController */
/* @var $model Profiles */

$this->breadcrumbs=array(
	'Profiles'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Profiles', 'url'=>array('index')),
	array('label'=>'Create Profiles', 'url'=>array('create')),
);

?>

<h1>Manage Profiles</h1>

<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'profiles-grid',
    'dataProvider' => $model->search(),
    'columns' => array(
        array('class' => 'IndexColumn', 'header' => 'Sl.No.', 'htmlOptions' => array('width' => '60px')),
        'profile_name',
        array(
			'class'=>'CButtonColumn',
                        'template'=>'{update}{delete}'
		),
    ),
    
));
?>
