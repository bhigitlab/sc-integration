-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: May 18, 2022 at 01:04 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `arcaedo_live_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `defect_return`
--

CREATE TABLE `defect_return` (
  `return_id` int(11) NOT NULL,
  `receipt_id` int(11) DEFAULT NULL,
  `return_number` text NOT NULL,
  `return_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `return_amount` float NOT NULL,
  `company_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT NULL,
  `return_status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jp_additional_bill`
--

CREATE TABLE `jp_additional_bill` (
  `id` int(11) NOT NULL,
  `bill_id` int(11) DEFAULT NULL,
  `category` varchar(200) DEFAULT NULL,
  `amount` float DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jp_albums`
--

CREATE TABLE `jp_albums` (
  `album_id` int(11) NOT NULL,
  `title` varchar(20) NOT NULL,
  `description` text NOT NULL,
  `projectid` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jp_bank`
--

CREATE TABLE `jp_bank` (
  `bank_id` int(11) NOT NULL,
  `bank_name` varchar(100) NOT NULL,
  `company_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jp_billitem`
--

CREATE TABLE `jp_billitem` (
  `billitem_id` int(11) NOT NULL,
  `bill_id` int(11) NOT NULL,
  `purchaseitem_id` int(11) DEFAULT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `billitem_description` text,
  `billitem_quantity` float NOT NULL,
  `billitem_unit` varchar(20) NOT NULL,
  `purchaseitem_quantity` double DEFAULT NULL,
  `purchaseitem_unit` varchar(20) DEFAULT NULL,
  `purchaseitem_rate` double DEFAULT NULL,
  `batch` varchar(300) DEFAULT NULL,
  `billitem_hsn_code` varchar(200) DEFAULT NULL,
  `billitem_rate` float NOT NULL,
  `billitem_taxslab` float DEFAULT NULL,
  `billitem_amount` float NOT NULL,
  `billitem_taxpercent` float NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `remark` varchar(100) DEFAULT NULL,
  `billitem_taxamount` float NOT NULL,
  `billitem_discountpercent` float NOT NULL,
  `billitem_cgst` float DEFAULT NULL,
  `billitem_cgstpercent` float(11,2) DEFAULT NULL,
  `billitem_sgst` float DEFAULT NULL,
  `billitem_sgstpercent` float DEFAULT NULL,
  `billitem_igst` float DEFAULT NULL,
  `billitem_igstpercent` float DEFAULT NULL,
  `billitem_discountamount` float NOT NULL,
  `billitem_length` double DEFAULT NULL,
  `billitem_width` double DEFAULT NULL,
  `billitem_height` double DEFAULT NULL,
  `approve_status` tinyint(1) DEFAULT NULL COMMENT '1->pending,2=>approved',
  `rate_approve` tinyint(1) DEFAULT NULL COMMENT '1=>pending,2=>approved',
  `created_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jp_bills`
--

CREATE TABLE `jp_bills` (
  `bill_id` int(11) NOT NULL,
  `purchase_id` int(11) NOT NULL,
  `bill_number` text NOT NULL,
  `bill_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `bill_amount` float(18,2) DEFAULT NULL,
  `bill_taxamount` float(18,2) DEFAULT NULL,
  `bill_discountamount` float(18,2) DEFAULT NULL,
  `bill_totalamount` float(18,2) DEFAULT NULL,
  `bill_additionalcharge` double DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `bill_status` int(11) DEFAULT NULL,
  `round_off` float DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_date` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `warehouse_id` int(11) DEFAULT NULL,
  `duplicate_ignore_status` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0=>Not Ignored, 1=>Ignored',
  `duplicate_delete_status` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0=>Not Deleted, 1=>Deleted'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jp_brand`
--

CREATE TABLE `jp_brand` (
  `id` int(11) NOT NULL,
  `brand_name` varchar(100) NOT NULL,
  `company_id` varchar(100) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jp_brand`
--

INSERT INTO `jp_brand` (`id`, `brand_name`, `company_id`, `created_by`, `created_date`) VALUES
(1, 'Cement- Ultra tech ', NULL, 5, '2022-05-17 18:30:00'),
(2, 'Cement - ACC', NULL, 5, '2022-05-17 18:30:00'),
(3, 'Cement - Ramco', NULL, 5, '2022-05-17 18:30:00'),
(4, 'Cement - Sankar', NULL, 5, '2022-05-17 18:30:00'),
(5, 'Cement -Ambuja', NULL, 5, '2022-05-17 18:30:00'),
(6, 'Cement -Chettinad', NULL, 5, '2022-05-17 18:30:00'),
(7, 'Steel reinforcements bars - Tata ', NULL, 5, '2022-05-17 18:30:00'),
(8, 'Steel reinforcements bars - Vizag', NULL, 5, '2022-05-17 18:30:00'),
(9, 'Steel reinforcements bars - Kalliyath', NULL, 5, '2022-05-17 18:30:00'),
(10, 'Steel reinforcements bars - JSW', NULL, 5, '2022-05-17 18:30:00'),
(11, 'Paint - Asian paints', NULL, 5, '2022-05-17 18:30:00'),
(12, 'Paint - Berger', NULL, 5, '2022-05-17 18:30:00'),
(13, 'Paint - Nerolac', NULL, 5, '2022-05-17 18:30:00'),
(14, 'Paint - National', NULL, 5, '2022-05-17 18:30:00'),
(15, 'Steel tubes - Jindal', NULL, 5, '2022-05-17 18:30:00'),
(16, 'Steel tubes - Tata', NULL, 5, '2022-05-17 18:30:00'),
(17, 'Steel tubes - Bhushan', NULL, 5, '2022-05-17 18:30:00'),
(18, 'Steel tubes - Jain', NULL, 5, '2022-05-17 18:30:00'),
(19, 'Tiles - Kajaria', NULL, 5, '2022-05-17 18:30:00'),
(20, 'Tiles - Somany', NULL, 5, '2022-05-17 18:30:00'),
(21, 'Tiles - Simpolo', NULL, 5, '2022-05-17 18:30:00'),
(22, 'Tiles - Motto', NULL, 5, '2022-05-17 18:30:00'),
(23, 'Tiles - Nitco', NULL, 5, '2022-05-17 18:30:00'),
(24, 'Tiles - RAK', NULL, 5, '2022-05-17 18:30:00'),
(25, 'Tiles - Cera', NULL, 5, '2022-05-17 18:30:00'),
(26, 'CP and sanitary ware - Jaquar', NULL, 5, '2022-05-17 18:30:00'),
(27, 'CP and sanitary ware - Kohler', NULL, 5, '2022-05-17 18:30:00'),
(28, 'CP and sanitary ware - Cera', NULL, 5, '2022-05-17 18:30:00'),
(29, 'CP and sanitary ware - Somany', NULL, 5, '2022-05-17 18:30:00'),
(30, 'CP and sanitary ware - Parryware', NULL, 5, '2022-05-17 18:30:00'),
(31, 'CP and sanitary ware - Grohe', NULL, 5, '2022-05-17 18:30:00'),
(32, 'PVC pipes - Astral', NULL, 5, '2022-05-17 18:30:00'),
(33, 'PVC pipes - Supreme', NULL, 5, '2022-05-17 18:30:00'),
(34, 'PVC pipes - Finolex', NULL, 5, '2022-05-17 18:30:00'),
(35, 'Electrical conduit pipes - Balco', NULL, 5, '2022-05-17 18:30:00'),
(36, 'Electrical cables - Finolex', NULL, 5, '2022-05-17 18:30:00'),
(37, 'Electrical cables - Bonton', NULL, 5, '2022-05-17 18:30:00'),
(38, 'Electrical cables - RR kabel', NULL, 5, '2022-05-17 18:30:00'),
(39, 'Electrical cables - Vguard', NULL, 5, '2022-05-17 18:30:00'),
(40, 'Electrical switches - Legrand', NULL, 5, '2022-05-17 18:30:00'),
(41, 'Electrical switches - Schneider electric', NULL, 5, '2022-05-17 18:30:00'),
(42, 'Electrical switches - GM', NULL, 5, '2022-05-17 18:30:00'),
(43, 'Electrical switches - Crabtree', NULL, 5, '2022-05-17 18:30:00'),
(44, 'DB - L&T', NULL, 5, '2022-05-17 18:30:00'),
(45, 'DB - ABB', NULL, 5, '2022-05-17 18:30:00'),
(46, 'DB - Schneider Electric', NULL, 5, '2022-05-17 18:30:00'),
(47, 'Tile epoxy grout - Laticrete', NULL, 5, '2022-05-17 18:30:00'),
(48, 'Plasticisers - Conplast', NULL, 5, '2022-05-17 18:30:00'),
(49, 'Waterproofing agent - Sika Raintite', NULL, 5, '2022-05-17 18:30:00');

-- --------------------------------------------------------

--
-- Table structure for table `jp_buyers`
--

CREATE TABLE `jp_buyers` (
  `id` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `buyer_type` smallint(6) DEFAULT NULL,
  `contact_person` varchar(30) DEFAULT NULL,
  `local_address` text,
  `phone` varchar(14) DEFAULT NULL,
  `address` text,
  `email_id` varchar(100) DEFAULT NULL,
  `gst_no` varchar(100) DEFAULT NULL,
  `status` smallint(6) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `project` int(11) DEFAULT NULL,
  `flat_numbers` varchar(100) DEFAULT NULL,
  `description` text,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_date` date DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `jp_buyer_invoice`
--

CREATE TABLE `jp_buyer_invoice` (
  `buyer_invoice_id` int(11) NOT NULL,
  `company_id` int(11) DEFAULT NULL,
  `buyer_id` int(11) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `flat_id` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `invoice_no` varchar(100) NOT NULL,
  `total_amount` float DEFAULT NULL,
  `tax_amount` float DEFAULT NULL,
  `invoice_status` enum('draft','saved') NOT NULL DEFAULT 'saved',
  `created_by` int(11) DEFAULT NULL,
  `created_date` date DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `jp_buyer_invoice_list`
--

CREATE TABLE `jp_buyer_invoice_list` (
  `id` int(11) NOT NULL,
  `buyer_inv_id` int(11) DEFAULT NULL,
  `description` text,
  `transaction_type` tinyint(11) DEFAULT NULL COMMENT '1->sales,2->journal',
  `amount` float DEFAULT NULL,
  `sgst_p` float DEFAULT NULL,
  `sgst_val` float DEFAULT NULL,
  `cgst_p` float DEFAULT NULL,
  `cgst_val` float DEFAULT NULL,
  `igst_p` float DEFAULT NULL,
  `igst_val` float DEFAULT NULL,
  `tax_total` float DEFAULT NULL,
  `subtotal` float DEFAULT NULL,
  `discount_amount` float DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` date DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `jp_buyer_transactions`
--

CREATE TABLE `jp_buyer_transactions` (
  `id` int(11) NOT NULL,
  `company_id` int(11) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `buyer_id` int(11) DEFAULT NULL,
  `flat_id` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `transaction_for` int(11) DEFAULT NULL COMMENT '1->advance,2->receipt,3->expense',
  `transaction_no` varchar(100) DEFAULT NULL,
  `transaction_type` smallint(11) DEFAULT NULL,
  `invoice_no` varchar(100) DEFAULT NULL,
  `from_transaction_head` smallint(11) DEFAULT NULL,
  `to_transaction_head` int(11) DEFAULT NULL,
  `account_group_id` int(11) DEFAULT NULL,
  `total_amount` float DEFAULT NULL,
  `amount` float DEFAULT NULL,
  `sgst_p` float DEFAULT NULL,
  `sgst_val` float DEFAULT NULL,
  `cgst_p` float DEFAULT NULL,
  `cgst_val` float DEFAULT NULL,
  `igst_p` float DEFAULT NULL,
  `igst_val` float DEFAULT NULL,
  `description` text,
  `tds_p` float DEFAULT NULL,
  `tds_val` double DEFAULT NULL,
  `bank_id` int(11) DEFAULT NULL,
  `cheque_no` varchar(100) DEFAULT NULL,
  `reconciliation_status` int(11) DEFAULT NULL,
  `reconciliation_date` date DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_date` date DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `jp_cashbalance`
--

CREATE TABLE `jp_cashbalance` (
  `id` int(11) NOT NULL,
  `cashbalance_type` smallint(6) NOT NULL,
  `bank_id` int(11) DEFAULT NULL,
  `cashbalance_date` date DEFAULT NULL,
  `cashbalance_opening_balance` float NOT NULL,
  `cashbalance_deposit` float DEFAULT NULL,
  `cashbalance_withdrawal` float DEFAULT NULL,
  `cashbalance_closing_balance` float DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jp_cashtransfer`
--

CREATE TABLE `jp_cashtransfer` (
  `cashtransfer_id` int(11) NOT NULL,
  `cashtransfer_date` date NOT NULL,
  `sender_company` int(11) NOT NULL,
  `sender_paymenttype` int(11) NOT NULL,
  `sender_bank` int(11) DEFAULT NULL,
  `sender_cheque` varchar(20) DEFAULT NULL,
  `beneficiary_company` int(11) NOT NULL,
  `beneficiary_paymenttype` int(11) NOT NULL,
  `beneficiary_bank` int(11) DEFAULT NULL,
  `beneficiary_cheque` varchar(20) DEFAULT NULL,
  `cashtransfer_amount` float(18,2) NOT NULL,
  `cashtransfer_description` varchar(500) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jp_clients`
--

CREATE TABLE `jp_clients` (
  `cid` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `phone` varchar(13) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `email_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `project_type` smallint(6) NOT NULL,
  `description` text,
  `status` smallint(6) NOT NULL,
  `address` text NOT NULL,
  `local_address` text NOT NULL,
  `contact_person` varchar(30) NOT NULL,
  `gst_no` varchar(100) DEFAULT NULL,
  `company_id` varchar(100) DEFAULT NULL,
  `created_date` date NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jp_company`
--

CREATE TABLE `jp_company` (
  `id` int(11) NOT NULL,
  `name` varchar(500) NOT NULL,
  `description` text NOT NULL,
  `address` text NOT NULL,
  `pincode` varchar(30) NOT NULL,
  `phone` varchar(30) NOT NULL,
  `email_id` varchar(100) NOT NULL,
  `company_gstnum` varchar(50) NOT NULL,
  `company_tolerance` float NOT NULL,
  `company_popermission` int(11) NOT NULL DEFAULT '0',
  `po_email_userid` varchar(255) DEFAULT NULL,
  `subco_email_userid` varchar(255) DEFAULT NULL,
  `subcontractor_limit` float NOT NULL,
  `auto_purchaseno` int(11) NOT NULL DEFAULT '0',
  `purchase_amount` float DEFAULT NULL,
  `expenses_email` text,
  `expenses_percentage` varchar(100) DEFAULT NULL,
  `invoice_email_userid` varchar(255) DEFAULT NULL,
  `company_updateduration` int(11) DEFAULT NULL,
  `purchaseorder_limit` double DEFAULT NULL,
  `company_poedit` int(11) DEFAULT '0',
  `company_subcontractorlimit` float DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jp_company_edit_log`
--

CREATE TABLE `jp_company_edit_log` (
  `id` int(11) NOT NULL,
  `p_id` int(11) NOT NULL COMMENT 'auto-increment id of purchase table',
  `project_id` int(11) NOT NULL,
  `prev_company_id` int(11) NOT NULL,
  `current_company_id` int(11) NOT NULL,
  `log_data` text NOT NULL,
  `log_date` date NOT NULL,
  `changed_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jp_company_expense_type`
--

CREATE TABLE `jp_company_expense_type` (
  `company_exp_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `type` int(11) DEFAULT NULL COMMENT '0=>Receipt;1=>Payment',
  `company_id` int(11) DEFAULT NULL,
  `status` enum('1','0') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jp_company_expense_type`
--

INSERT INTO `jp_company_expense_type` (`company_exp_id`, `name`, `type`, `company_id`, `status`) VALUES
(2, 'Electricity', 2, 1, '1'),
(5, 'SALARY ADVANCE', 1, 1, '1'),
(17, 'ElectricityET', 2, 1, '1'),
(18, 'PETTY CASH REFUND', 0, 1, '1'),
(19, 'Stationary', 2, 1, '1'),
(20, 'PETTY CASH ISSUED', 1, 1, '1'),
(21, 'SALARY', 1, 1, '1'),
(22, 'Salary Adavance', 1, 1, '1'),
(23, 'Office Maintance Work', 1, 1, '1'),
(24, 'OFFICE ASSETS', 1, 1, '1'),
(25, 'KSEB', 1, 1, '1'),
(26, 'DRAWINGS', 1, 1, '1'),
(27, 'Office Expenss', 1, 1, '1'),
(28, 'PICKUP EXPENSES', 1, 1, '1'),
(29, 'LOAN FROM DIRECTORS', 0, 1, '1'),
(30, 'BANK LOAN', 0, 1, '1'),
(31, 'BANK LOAN INTEREST', 1, 1, '1'),
(32, 'INDIRECT INCOME - REFUND', 0, 1, '1'),
(33, 'BANK CHARGES', 1, 1, '1'),
(34, 'Petrol', 2, 1, '1'),
(35, 'Mobile  recharge', 2, 1, '1'),
(36, 'Parcel Charge', 2, 1, '1'),
(37, 'Painting Labour', 2, 1, '1'),
(38, 'Advertisement', 2, 1, '1'),
(39, 'ceiling Labour charge', 2, 1, '1'),
(40, 'Factory Rent', 2, 1, '1'),
(41, 'Monthly Salary', 2, 1, '1'),
(42, 'Ali  Labour', 2, 1, '1'),
(43, 'Sujith', 2, 1, '1'),
(44, 'T  A  for  Sankar', 2, 1, '1'),
(45, 'T  A  For  Manoj', 2, 1, '1'),
(46, 'wall  paper Laying  charge', 2, 1, '1'),
(47, 'Curtain items', 2, 1, '1'),
(48, 'Plumber  Labour', 2, 1, '1'),
(49, 'Dizel', 2, 1, '1'),
(50, 'Picnic  charge', 2, 1, '1'),
(51, 'Tyre  world', 0, 1, '1'),
(52, 'Unicorn retest', 0, 1, '1'),
(53, 'Theradapuzha  Age', 0, 1, '1'),
(54, 'orient  glass', 0, 1, '1'),
(55, 'Nazarene  Hardware', 0, 1, '1'),
(56, 'Nass floorings', 0, 1, '1'),
(57, 'vijaya', 0, 1, '1'),
(58, 'forever  marketting', 0, 1, '1'),
(59, 'plymarc', 0, 1, '1'),
(60, 'website  updation', 0, 1, '1'),
(61, 'Globelia', 0, 1, '1'),
(62, 'fevicol', 0, 1, '1'),
(63, 'pattel', 0, 1, '1'),
(64, 'jain', 0, 1, '1'),
(65, 'offset   mlpy', 0, 1, '1'),
(66, 'fastag', 0, 1, '1'),
(67, 'Video  Add', 0, 1, '1'),
(68, 'sofa', 0, 1, '1'),
(69, 'software', 0, 1, '1'),
(70, 'Adi  sales', 0, 1, '1'),
(71, 'c b  traders', 0, 1, '1'),
(72, 'C B Traders', 0, 1, '1'),
(73, 'Cake purchase', 0, 1, '1'),
(74, 'Flamante joinery', 0, 1, '1'),
(75, 'nellukaran  glass', 0, 1, '1'),
(76, 'water  bil', 0, 1, '1'),
(77, 'Elecrticity  bill', 0, 1, '1'),
(78, 'impressive', 0, 1, '1'),
(79, 'c k  creative', 0, 1, '1'),
(80, 'D R Paints', 0, 1, '1'),
(81, 'Best  sellers', 0, 1, '1'),
(82, 'shivani', 0, 1, '1'),
(83, 'technical', 0, 1, '1'),
(84, 'can', 0, 1, '1'),
(85, 'concord', 0, 1, '1'),
(86, 'purchase  return', 0, 1, '1'),
(87, 'NEFT SC', 0, 1, '1'),
(88, 'Popular motor  world', 0, 1, '1'),
(89, 'Lodge  rent', 0, 1, '1'),
(90, 'Insurance  renewed', 0, 1, '1'),
(91, 'canara bank Machinery  Loan', 0, 1, '1'),
(92, 'Canara  bank new  loan', 0, 1, '1'),
(93, 'Canara bank Eeco loan', 0, 1, '1'),
(94, 'Canara  bank  Vehicle  loan', 0, 1, '1'),
(95, 'Canara bank Agac loan', 0, 1, '1'),
(96, 'c n c  cutting  charge', 0, 1, '1'),
(97, 'Electrician  labour', 0, 1, '1'),
(98, 'Visiting  Card', 0, 1, '1'),
(99, 'ply', 1, 1, '1'),
(100, 'Ecco  service', 1, 1, '1'),
(101, 'carpenter', 1, 1, '1'),
(102, 'Theradapuzha hard', 1, 1, '1'),
(103, 'orient  glass  1', 1, 1, '1'),
(104, 'can -1', 1, 1, '1'),
(105, 'Am de decor', 1, 1, '1'),
(106, 'NEFT SC - C B', 1, 1, '1'),
(107, 'Nass floorings -1', 1, 1, '1'),
(108, 'vijaya - 1', 1, 1, '1'),
(109, 'canara bank Machinery  Loan-1', 1, 1, '1'),
(110, 'canara  bank eeco  loan-1', 1, 1, '1'),
(111, 'canara bank vehicle loan -1', 1, 1, '1'),
(112, 'Canara bank Agac loan-1', 1, 1, '1'),
(113, 'website  updation- 1', 1, 1, '1'),
(114, 'visiting card-  1', 1, 1, '1'),
(115, 'izuzu fastag', 1, 1, '1'),
(116, 'video  add adv payment', 1, 1, '1'),
(117, 'Blue  horizon infotech', 1, 1, '1'),
(118, 'sofa-1', 1, 1, '1'),
(119, 'Cake purchase-1', 1, 1, '1'),
(120, 'GST C A  TONY', 1, 1, '1'),
(121, 'c b traders-1', 1, 1, '1'),
(122, 'Flamante joinery -1', 1, 1, '1'),
(123, 'water bil-1', 1, 1, '1'),
(124, 'Google  promotions', 1, 1, '1'),
(125, 'D R Paints-1', 1, 1, '1'),
(126, 'sms alert  charge', 1, 1, '1'),
(127, 'shivani - 1', 1, 1, '1');

-- --------------------------------------------------------

--
-- Table structure for table `jp_dailyexpense`
--

CREATE TABLE `jp_dailyexpense` (
  `dailyexp_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `exp_type_id` int(11) DEFAULT NULL COMMENT 'company expense type',
  `bill_id` varchar(100) DEFAULT NULL,
  `expensehead_id` int(11) DEFAULT NULL,
  `expense_type` int(11) DEFAULT NULL,
  `vendor_id` int(11) DEFAULT NULL,
  `dailyexpense_amount` float DEFAULT NULL,
  `dailyexpense_sgstp` float DEFAULT NULL,
  `dailyexpense_sgst` float DEFAULT NULL,
  `dailyexpense_cgstp` float DEFAULT NULL,
  `dailyexpense_cgst` float DEFAULT NULL,
  `dailyexpense_igstp` float DEFAULT NULL,
  `dailyexpense_igst` float DEFAULT NULL,
  `amount` float NOT NULL,
  `description` varchar(300) NOT NULL,
  `dailyexpense_receipt_type` int(11) DEFAULT NULL,
  `dailyexpense_receipt_head` int(11) DEFAULT NULL,
  `dailyexpense_receipt` float DEFAULT NULL,
  `dailyexpense_purchase_type` int(11) DEFAULT NULL,
  `dailyexpense_paidamount` float DEFAULT NULL,
  `bank_id` int(11) DEFAULT NULL,
  `dailyexpense_chequeno` varchar(100) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `data_entry` varchar(30) DEFAULT NULL,
  `exp_type` int(4) NOT NULL,
  `dailyexpense_type` enum('expense','deposit','receipt') NOT NULL DEFAULT 'expense',
  `company_id` int(11) DEFAULT NULL,
  `reconciliation_status` int(11) DEFAULT NULL,
  `reconciliation_date` date DEFAULT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `display_flg` enum('Yes','No') NOT NULL DEFAULT 'Yes',
  `transaction_parent` int(11) DEFAULT NULL,
  `parent_status` enum('0','1') NOT NULL DEFAULT '1',
  `expensehead_type` int(11) DEFAULT NULL,
  `transfer_parentid` int(11) DEFAULT NULL,
  `update_status` int(11) DEFAULT '0',
  `delete_status` int(11) DEFAULT '0',
  `approval_status` enum('0','1') NOT NULL DEFAULT '1' COMMENT '0=>Pending for approval, 1=>Approved	',
  `duplicate_ignore_status` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0=>Not Ignored, 1=>Ignored',
  `duplicate_delete_status` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0=>Not Deleted, 1=>Deleted'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jp_dailyreport`
--

CREATE TABLE `jp_dailyreport` (
  `dr_id` int(11) NOT NULL,
  `projectid` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `subcontractor_id` int(11) DEFAULT NULL,
  `expensehead_id` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `works_done` text NOT NULL,
  `materials_unlade` text NOT NULL,
  `wrktype_and_numbers` longtext,
  `extra_work_done` text NOT NULL,
  `amount` float(20,2) NOT NULL,
  `labour` float(20,2) DEFAULT NULL,
  `wage` float(20,2) DEFAULT NULL,
  `wage_rate` float(20,2) DEFAULT NULL,
  `helper` float(20,2) DEFAULT NULL,
  `helper_labour` float(20,2) DEFAULT NULL,
  `lump_sum` float(20,2) DEFAULT NULL,
  `approve_status` enum('1','2') DEFAULT NULL COMMENT '1=>approve, 2=>not approve',
  `description` text,
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `labour_wage` float(20,2) NOT NULL,
  `helper_wage` float(20,2) NOT NULL,
  `approval_status` enum('0','1') DEFAULT '1' COMMENT '0=>Pending for approval, 1=>Approved'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jp_dailyvendors`
--

CREATE TABLE `jp_dailyvendors` (
  `daily_v_id` int(11) NOT NULL,
  `vendor_id` int(11) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `amount` float(15,2) NOT NULL,
  `description` text NOT NULL,
  `date` date NOT NULL,
  `payment_type` smallint(6) DEFAULT NULL,
  `bank` int(11) DEFAULT NULL,
  `cheque_no` varchar(100) DEFAULT NULL,
  `sgst` float DEFAULT NULL,
  `sgst_amount` float DEFAULT NULL,
  `cgst` float DEFAULT NULL,
  `cgst_amount` float DEFAULT NULL,
  `igst` float DEFAULT NULL,
  `igst_amount` float DEFAULT NULL,
  `tds` float DEFAULT NULL,
  `tds_amount` double DEFAULT NULL,
  `tax_amount` float DEFAULT NULL,
  `paidamount` double DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `company_id` int(11) DEFAULT NULL,
  `reconciliation_status` int(11) DEFAULT NULL,
  `reconciliation_date` date DEFAULT NULL,
  `budget_percentage` float DEFAULT NULL,
  `approval_status` enum('0','1') NOT NULL DEFAULT '1' COMMENT '0=>Pending for approval, 1=>Approved',
  `duplicate_ignore_status` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0=>Not Ignored, 1=>Ignored',
  `duplicate_delete_status` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0=>Not Deleted, 1=>Deleted'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jp_daily_work_type`
--

CREATE TABLE `jp_daily_work_type` (
  `wtid` smallint(6) NOT NULL,
  `work_type` varchar(30) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `no_of_labour_label` varchar(200) DEFAULT NULL,
  `wage_label` varchar(200) DEFAULT NULL,
  `wage_rate_label` varchar(200) DEFAULT NULL,
  `helper_label` varchar(200) DEFAULT NULL,
  `helper_labour_label` varchar(200) DEFAULT NULL,
  `labour_status` enum('1','2') NOT NULL DEFAULT '1',
  `wage_status` enum('1','2') NOT NULL DEFAULT '1',
  `wagerate_status` enum('1','2') NOT NULL DEFAULT '1',
  `helper_status` enum('1','2') DEFAULT NULL,
  `helperlabour_status` enum('1','2') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jp_db_changes`
--

CREATE TABLE `jp_db_changes` (
  `db_c_id` int(11) NOT NULL,
  `data_id` int(11) NOT NULL,
  `table_name` varchar(50) NOT NULL,
  `action` varchar(10) NOT NULL,
  `modified_date` datetime NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `primary_field_name` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jp_defect_returnitem`
--

CREATE TABLE `jp_defect_returnitem` (
  `returnitem_id` int(11) NOT NULL,
  `return_id` int(11) NOT NULL,
  `receiptitem_id` int(11) DEFAULT NULL,
  `returnitem_description` mediumtext,
  `returnitem_quantity` float NOT NULL,
  `returnitem_unit` varchar(20) NOT NULL,
  `returnitem_rate` float NOT NULL,
  `returnitem_amount` float NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `remark` varchar(100) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jp_deletepending`
--

CREATE TABLE `jp_deletepending` (
  `deletepending_id` int(11) NOT NULL,
  `deletepending_data` varchar(1000) NOT NULL,
  `deletepending_table` varchar(100) NOT NULL,
  `deletepending_parentid` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `requested_date` date NOT NULL,
  `deletepending_status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jp_deposit`
--

CREATE TABLE `jp_deposit` (
  `deposit_id` int(11) NOT NULL,
  `deposit_name` varchar(100) NOT NULL,
  `company_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jp_editrequest`
--

CREATE TABLE `jp_editrequest` (
  `editrequest_id` int(11) NOT NULL,
  `editrequest_data` text NOT NULL,
  `editrequest_table` varchar(100) NOT NULL,
  `editrequest_headstatus` int(11) DEFAULT NULL,
  `editrequest_headtype` int(11) DEFAULT NULL,
  `editrequest_payment` varchar(100) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `editrequest_date` datetime NOT NULL,
  `editrequest_status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jp_expenses`
--

CREATE TABLE `jp_expenses` (
  `exp_id` int(11) NOT NULL,
  `projectid` int(11) NOT NULL,
  `bill_id` int(11) DEFAULT NULL,
  `invoice_id` int(11) DEFAULT NULL,
  `return_id` int(11) DEFAULT NULL,
  `subcontractor_id` int(11) DEFAULT NULL,
  `userid` int(11) NOT NULL,
  `date` date DEFAULT NULL,
  `amount` float(20,2) NOT NULL,
  `description` varchar(300) NOT NULL,
  `type` tinyint(4) NOT NULL,
  `exptype` int(11) DEFAULT NULL,
  `expense_type` int(11) DEFAULT NULL,
  `vendor_id` int(11) DEFAULT NULL,
  `expense_amount` float DEFAULT NULL,
  `expense_sgstp` float DEFAULT NULL,
  `expense_sgst` float DEFAULT NULL,
  `expense_cgstp` float DEFAULT NULL,
  `expense_cgst` float DEFAULT NULL,
  `expense_igstp` float DEFAULT NULL,
  `expense_igst` float DEFAULT NULL,
  `expense_tdsp` float DEFAULT NULL,
  `expense_tds` double DEFAULT NULL,
  `payment_type` smallint(6) DEFAULT NULL,
  `bank_id` int(11) DEFAULT NULL,
  `cheque_no` varchar(100) DEFAULT NULL,
  `receipt` float(20,2) DEFAULT NULL,
  `works_done` varchar(100) DEFAULT NULL,
  `materials` varchar(100) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `purchase_type` int(11) DEFAULT NULL COMMENT '0=>Partially paid;1=>full paid;',
  `paid` float(20,2) DEFAULT NULL,
  `paidamount` double DEFAULT NULL,
  `data_entry` varchar(30) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `reconciliation_status` int(11) DEFAULT NULL,
  `reconciliation_date` date DEFAULT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `budget_percentage` float DEFAULT NULL,
  `update_status` int(11) DEFAULT '0',
  `delete_status` int(11) DEFAULT '0',
  `payment_quotation_status` enum('1','2') DEFAULT '1' COMMENT '1=>payment against quotation, 2=>payment without quotation ',
  `additional_charge` float(18,2) DEFAULT NULL,
  `approval_status` enum('0','1') NOT NULL DEFAULT '1' COMMENT '0=>Pending for approval, 1=>Approved',
  `duplicate_ignore_status` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0=>Not Ignored, 1=>Ignored',
  `duplicate_delete_status` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0=>Not Deleted, 1=>Deleted'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `jp_expenses`
--
DELIMITER $$
CREATE TRIGGER `after_delete_expenses` AFTER DELETE ON `jp_expenses` FOR EACH ROW BEGIN IF OLD.type =73 THEN SET @c = ( SELECT tot_expense FROM jp_projects WHERE pid = OLD.projectid ) - OLD.paid; SET @v = ( SELECT tot_paid_to_vendor FROM jp_projects WHERE pid = OLD.projectid ) - OLD.paid; UPDATE jp_projects SET tot_expense = @c , tot_paid_to_vendor = @v WHERE pid = OLD.projectid; ELSE SET @c = ( SELECT tot_receipt FROM jp_projects WHERE pid = OLD.projectid ) - OLD.amount; UPDATE jp_projects SET tot_receipt = @c WHERE pid = OLD.projectid; END IF ; END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `after_expense_insert` AFTER INSERT ON `jp_expenses` FOR EACH ROW BEGIN
        IF NEW.type = 73 THEN
    UPDATE
        jp_projects
    SET
        tot_expense =(
        SELECT
            SUM(paid)
        FROM
            jp_expenses
        WHERE
            projectid = NEW.projectid AND TYPE = 73
    ),
    tot_paid_to_vendor =(
    SELECT
        SUM(paid)
    FROM
        jp_expenses
    WHERE
        projectid = NEW.projectid AND TYPE = 73
)
WHERE
    pid = NEW.projectid ; ELSE
UPDATE
    jp_projects
SET
    tot_receipt =(
    SELECT
        SUM(amount)
    FROM
        jp_expenses
    WHERE
        projectid = NEW.projectid AND TYPE = 72
)
WHERE
    pid = NEW.projectid ;
    END IF ;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `after_expense_update` AFTER UPDATE ON `jp_expenses` FOR EACH ROW BEGIN IF NEW.type =73 THEN UPDATE jp_projects SET tot_expense = ( SELECT SUM( paid ) FROM jp_expenses WHERE projectid = NEW.projectid AND TYPE =73 ) , tot_paid_to_vendor = ( SELECT SUM( paid ) FROM jp_expenses WHERE projectid = NEW.projectid AND TYPE =73 ) WHERE pid = NEW.projectid; ELSE UPDATE jp_projects SET tot_receipt = ( SELECT SUM( amount ) FROM jp_expenses WHERE projectid = NEW.projectid AND TYPE =72 ) WHERE pid = NEW.projectid; END IF ;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `jp_expenses_import`
--

CREATE TABLE `jp_expenses_import` (
  `exp_id` int(11) NOT NULL,
  `projectid` varchar(50) DEFAULT NULL,
  `userid` varchar(50) DEFAULT NULL,
  `exp_date` date DEFAULT NULL,
  `amount` float(8,2) DEFAULT NULL,
  `description` varchar(300) DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL,
  `exptype` varchar(50) DEFAULT NULL,
  `vendor_id` varchar(50) DEFAULT NULL,
  `payment_type` varchar(50) DEFAULT NULL,
  `works_done` varchar(100) DEFAULT NULL,
  `materials` varchar(100) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` date DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` date DEFAULT NULL,
  `purchase_type` varchar(50) DEFAULT NULL COMMENT '0=>Partially paid;1=>full paid;',
  `paid` float(8,2) DEFAULT NULL,
  `import_status` varchar(300) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jp_expense_notification`
--

CREATE TABLE `jp_expense_notification` (
  `id` int(11) NOT NULL,
  `project_id` int(11) DEFAULT NULL,
  `expense_perc` int(11) DEFAULT NULL COMMENT 'expense_percentage/payment_limit percentage',
  `expense_amount` float DEFAULT NULL COMMENT 'expense_amount/payment_limit amount',
  `advance_amount` float DEFAULT NULL,
  `generated_date` date DEFAULT NULL,
  `view_status` tinyint(1) DEFAULT NULL COMMENT '1=>not viewed,2->viewed',
  `notification_type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=>expense,2=>payment_limit'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `jp_expense_type`
--

CREATE TABLE `jp_expense_type` (
  `type_id` int(11) NOT NULL,
  `type_name` varchar(30) NOT NULL,
  `expense_type` smallint(6) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `template_type` int(11) DEFAULT NULL,
  `labour_label` varchar(200) DEFAULT NULL,
  `wage_label` varchar(200) NOT NULL,
  `wage_rate_label` varchar(200) NOT NULL,
  `helper_label` varchar(200) NOT NULL,
  `helper_labour_label` varchar(200) NOT NULL,
  `lump_sum_label` varchar(200) NOT NULL,
  `labour_status` enum('1','2') DEFAULT NULL,
  `wage_status` enum('1','2') DEFAULT NULL,
  `wagerate_status` enum('1','2') DEFAULT NULL,
  `helper_status` enum('1','2') DEFAULT NULL,
  `helperlabour_status` enum('1','2') DEFAULT NULL,
  `lump_sum_status` enum('1','2') DEFAULT NULL,
  `labour_wage_label` varchar(200) NOT NULL,
  `helper_wage_label` varchar(200) NOT NULL,
  `helper_wage_status` enum('1','2') DEFAULT NULL,
  `labour_wage_status` enum('1','2') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jp_general_settings`
--

CREATE TABLE `jp_general_settings` (
  `id` int(11) NOT NULL,
  `po_email_from` varchar(100) DEFAULT NULL,
  `buyer_module` int(11) DEFAULT NULL COMMENT '0=> disable buyer module, 1 =>enable buyer  module',
  `description` varchar(100) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '0',
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `jp_general_settings`
--

INSERT INTO `jp_general_settings` (`id`, `po_email_from`, `buyer_module`, `description`, `status`, `updated_by`, `updated_date`) VALUES
(1, 'info@bluehorizoninfotech.com', 0, '', '0', 5, '2021-04-29 21:39:35'),
(2, 'pc@bluehorizoninfotech.com', 1, '', '0', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `jp_image_gallery`
--

CREATE TABLE `jp_image_gallery` (
  `id` int(11) NOT NULL,
  `projectid` int(11) DEFAULT NULL,
  `albumid` int(11) DEFAULT NULL,
  `image` varchar(200) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` date DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` date DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jp_invoice`
--

CREATE TABLE `jp_invoice` (
  `invoice_id` int(11) NOT NULL,
  `project_id` int(11) DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  `date` date NOT NULL,
  `amount` float(20,2) NOT NULL,
  `tax_amount` float DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime NOT NULL,
  `inv_no` varchar(100) NOT NULL,
  `fees` float(16,2) NOT NULL,
  `subtotal` float(20,2) NOT NULL,
  `company_id` int(11) DEFAULT NULL,
  `invoice_status` enum('draft','saved') NOT NULL DEFAULT 'saved',
  `type` enum('quantity_rate','lumpsum') NOT NULL DEFAULT 'quantity_rate'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jp_invoice_list`
--

CREATE TABLE `jp_invoice_list` (
  `id` int(11) NOT NULL,
  `perf_id` int(11) DEFAULT NULL,
  `quantity` float(8,2) NOT NULL,
  `unit` varchar(10) NOT NULL,
  `rate` float(16,2) NOT NULL,
  `amount` float(20,2) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jp_inv_list`
--

CREATE TABLE `jp_inv_list` (
  `id` int(11) NOT NULL,
  `inv_id` int(11) DEFAULT NULL COMMENT 'relaion to jp_invoice->invoice_id',
  `quantity` float(8,2) DEFAULT NULL,
  `unit` varchar(10) DEFAULT NULL,
  `rate` float(16,2) DEFAULT NULL,
  `amount` float DEFAULT NULL,
  `cgst` float DEFAULT NULL,
  `cgst_amount` float DEFAULT NULL,
  `sgst` float DEFAULT NULL,
  `sgst_amount` float DEFAULT NULL,
  `igst` float DEFAULT NULL,
  `igst_amount` float DEFAULT NULL,
  `tax_amount` float DEFAULT NULL,
  `description` text NOT NULL,
  `hsn_code` varchar(200) DEFAULT NULL,
  `created_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jp_itemestimation`
--

CREATE TABLE `jp_itemestimation` (
  `itemestimation_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `purchase_type` enum('A','G','O','') DEFAULT NULL,
  `category_id` int(11) NOT NULL,
  `itemestimation_width` float DEFAULT NULL,
  `itemestimation_height` float DEFAULT NULL,
  `itemestimation_length` float DEFAULT NULL,
  `itemestimation_unit` varchar(50) DEFAULT NULL,
  `itemestimation_quantity` float NOT NULL,
  `itemestimation_price` float(18,2) DEFAULT NULL,
  `itemestimation_amount` float(18,2) DEFAULT NULL,
  `itemestimation_description` varchar(500) DEFAULT NULL,
  `itemestimation_status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jp_location`
--

CREATE TABLE `jp_location` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '1 = Active, 0 = Inactive	',
  `created_by` int(11) DEFAULT NULL COMMENT 'From user table',
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jp_location`
--

INSERT INTO `jp_location` (`id`, `name`, `status`, `created_by`, `created_date`) VALUES
(36, 'Alappuzha', 1, 23, '2021-12-09 00:00:00'),
(40, 'Trivandrum', 1, 23, '2021-12-09 00:00:00'),
(41, 'Kollam', 1, 23, '2021-12-09 00:00:00'),
(42, 'Idukki', 1, 23, '2021-12-09 00:00:00'),
(43, 'Kottayam', 1, 23, '2021-12-09 00:00:00'),
(44, 'Ernakulam', 1, 23, '2021-12-09 00:00:00'),
(45, 'Thrissur', 1, 23, '2021-12-09 00:00:00'),
(46, 'Palakkad', 1, 23, '2021-12-09 00:00:00'),
(47, 'Malappuram', 1, 23, '2021-12-09 00:00:00'),
(48, 'Kozhikode', 1, 23, '2021-12-09 00:00:00'),
(49, 'wayanad', 1, 5, '2021-10-19 00:00:00'),
(50, 'kannur', 1, 5, '2021-10-19 00:00:00'),
(51, 'kasargod', 1, 5, '2021-10-19 00:00:00'),
(52, 'bangalore', 1, 5, '2021-10-19 00:00:00'),
(53, 'chenni', 1, 5, '2021-10-19 00:00:00'),
(54, 'coimbatore', 1, 5, '2021-10-19 00:00:00'),
(55, 'Pathanamthitta', 1, 23, '2021-10-22 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `jp_log`
--

CREATE TABLE `jp_log` (
  `log_id` int(11) NOT NULL,
  `log_data` text NOT NULL,
  `log_table` varchar(100) DEFAULT NULL,
  `log_primary_key` int(11) DEFAULT NULL,
  `log_datetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `log_action` enum('update','delete') NOT NULL,
  `log_action_by` int(11) NOT NULL,
  `company_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jp_menu`
--

CREATE TABLE `jp_menu` (
  `menu_id` int(20) NOT NULL,
  `menu_name` varchar(100) NOT NULL,
  `parent_id` int(20) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `controller` varchar(250) NOT NULL,
  `action` varchar(30) NOT NULL,
  `params` varchar(50) NOT NULL,
  `showmenu` tinyint(1) NOT NULL COMMENT '0=>all, 1=>guest, 2=> authenticated',
  `show_list` tinyint(1) NOT NULL COMMENT ' 0=> dont show in list,1=>show in list',
  `related` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jp_menu`
--

INSERT INTO `jp_menu` (`menu_id`, `menu_name`, `parent_id`, `status`, `controller`, `action`, `params`, `showmenu`, `show_list`, `related`) VALUES
(1, 'User', 0, 0, 'users', '', '', 0, 1, NULL),
(2, 'Listing', 1, 0, 'users', 'index', '', 2, 1, NULL),
(3, 'Purchase', 0, 0, 'purchase', '', '', 0, 1, NULL),
(4, 'Listing', 3, 0, 'purchase', 'admin', '', 2, 1, NULL),
(5, 'defaultdelete', 3, 0, 'purchase', 'delete', '', 0, 0, NULL),
(6, 'defaultcreate', 3, 0, 'purchase', 'create', '', 0, 0, NULL),
(7, 'Create', 3, 0, 'purchase', 'addpurchase', '', 2, 1, NULL),
(8, 'Update', 3, 0, 'purchase', 'Updatepurchase', '', 2, 1, NULL),
(9, 'View', 3, 0, 'purchase', 'viewpurchase', '', 2, 1, NULL),
(10, 'Exportpdf', 3, 0, 'purchase', 'exportpdf', '', 0, 0, NULL),
(11, 'Export Excel', 3, 0, 'purchase', 'exportexcel', '', 0, 0, NULL),
(12, 'Purchaseitem', 3, 0, 'purchase', 'purchaseitem', '', 0, 0, NULL),
(13, 'Previoustransaction', 3, 0, 'purchase', 'previoustransaction', '', 0, 0, NULL),
(14, 'Createnewpurchase', 3, 0, 'purchase', 'createnewpurchase', '', 0, 0, NULL),
(15, 'Updatepurchaseitem', 3, 0, 'purchase', 'updatepurchaseitem', '', 0, 0, NULL),
(16, 'Removepurchaseitem', 3, 0, 'purchase', 'removepurchaseitem', '', 0, 0, NULL),
(17, 'Updatepurchasestatus', 3, 0, 'purchase', 'updatepurchasestatus', '', 0, 0, NULL),
(18, 'Ajax', 3, 0, 'purchase', 'test', '', 0, 0, NULL),
(19, 'Permissionapprove', 3, 0, 'purchase', 'permissionapprove', '', 0, 0, NULL),
(20, 'Previousratedetails', 3, 0, 'purchase', 'previousratedetails', '', 0, 0, NULL),
(21, 'Dynamicvendor', 3, 0, 'purchase', 'dynamicvendor', '', 0, 0, NULL),
(22, 'Dynamicexpensehead', 3, 0, 'purchase', 'dynamicexpensehead', '', 0, 0, NULL),
(23, 'Testransaction', 3, 0, 'purchase', 'testransaction', '', 0, 0, NULL),
(24, 'Getitemcategorysearch', 3, 0, 'purchase', 'getitemcategorysearch', '', 0, 0, NULL),
(25, 'Getunits', 3, 0, 'purchase', 'getunits', '', 0, 0, NULL),
(26, 'Permissionmail', 3, 0, 'purchase', 'permissionmail', '', 0, 0, NULL),
(27, 'Previous Purchase', 3, 0, 'purchase', 'previouspurchase', '', 2, 1, NULL),
(28, 'Previouspurchasepdf', 3, 0, 'purchase', 'previouspurchasepdf', '', 0, 0, NULL),
(29, 'Previouspurchasexcel', 3, 0, 'purchase', 'previouspurchasexcel', '', 0, 0, NULL),
(30, 'defaultindex', 3, 0, 'purchase', 'index', '', 0, 0, NULL),
(31, 'defaultview', 3, 0, 'purchase', 'view', '', 0, 0, NULL),
(32, 'defaultupdate', 3, 0, 'purchase', 'update', '', 0, 0, NULL),
(33, 'Bills', 0, 0, 'bills', '', '', 0, 1, NULL),
(34, 'Listing', 33, 0, 'bills', 'admin', '', 2, 1, NULL),
(35, 'Bill View', 33, 0, 'bills', 'view', '', 2, 1, NULL),
(36, 'defaultindex', 33, 0, 'bills', 'index', '', 0, 0, NULL),
(37, 'defaultdelete', 33, 0, 'bills', 'delete', '', 0, 0, NULL),
(38, 'Bill Create ', 33, 0, 'bills', 'create', '', 2, 1, NULL),
(39, 'Bill Update', 33, 0, 'bills', 'update', '', 2, 1, NULL),
(40, 'GetItemsByPurchase', 33, 0, 'bills', 'GetItemsByPurchase', '', 0, 0, NULL),
(41, 'Savebills', 33, 0, 'bills', 'savebills', '', 0, 0, NULL),
(42, 'Exportbills', 33, 0, 'bills', 'exportbills', '', 0, 0, NULL),
(43, 'UpdateItemsToList', 33, 0, 'bills', 'UpdateItemsToList', '', 0, 0, NULL),
(44, 'ValidateBillNumber', 33, 0, 'bills', 'ValidateBillNumber', '', 0, 0, NULL),
(45, 'AddItemsForBills', 33, 0, 'bills', 'AddItemsForBills', '', 0, 0, NULL),
(46, 'UpdateBillsOnReload', 33, 0, 'bills', 'UpdateBillsOnReload', '', 0, 0, NULL),
(47, 'Taxreport', 561, 0, 'bills', 'Taxreport', '', 2, 1, NULL),
(48, 'Create Bill Without PO', 33, 0, 'bills', 'addbill', '', 2, 1, NULL),
(49, 'Createnewbills', 33, 0, 'bills', 'createnewbills', '', 0, 0, NULL),
(50, 'Billsitem', 33, 0, 'bills', 'billsitem', '', 0, 0, NULL),
(51, 'Removebillitem', 33, 0, 'bills', 'removebillitem', '', 0, 0, NULL),
(52, 'Updatesbillitem', 33, 0, 'bills', 'updatesbillitem', '', 0, 0, NULL),
(53, 'Edit Bill Without PO', 33, 0, 'bills', 'editbill', '', 2, 1, NULL),
(54, 'Newlist', 33, 0, 'bills', 'newlist', '', 0, 0, NULL),
(55, 'View Bill Without PO', 33, 0, 'bills', 'billview', '', 2, 1, NULL),
(56, 'pdfbills', 33, 0, 'bills', 'pdfbills', '', 0, 0, NULL),
(57, 'exportbils', 33, 0, 'bills', 'exportbils', '', 0, 0, NULL),
(58, 'Taxreport CSV', 33, 0, 'bills', 'taxreportcsv', '', 0, 0, NULL),
(59, 'Taxreport PDF', 33, 0, 'bills', 'taxreportpdf', '', 0, 0, NULL),
(60, 'ajax', 33, 0, 'bills', 'ajax', '', 0, 0, NULL),
(61, 'GetItemCategory', 33, 0, 'bills', 'GetItemCategory', '', 0, 0, NULL),
(62, 'GetParent', 33, 0, 'bills', 'GetParent', '', 0, 0, NULL),
(63, 'testransaction', 33, 0, 'bills', 'testransaction', '', 0, 0, NULL),
(64, 'GetItemCategorySearch', 33, 0, 'bills', 'GetItemCategorySearch', '', 0, 0, NULL),
(65, 'Getunits', 33, 0, 'bills', 'Getunits', '', 0, 0, NULL),
(66, 'Previoustransaction', 33, 0, 'bills', 'Previoustransaction', '', 0, 0, NULL),
(67, 'Purchase Bill Report', 561, 0, 'bills', 'purchasebillreport', '', 2, 1, NULL),
(68, 'Purchase Bill Report PDF', 33, 0, 'bills', 'purchasetopdf', '', 0, 0, NULL),
(69, 'Purchase Bill Report Excel', 33, 0, 'bills', 'purchasetoexcel', '', 0, 0, NULL),
(70, 'Invoice', 0, 0, 'invoice', '', '', 0, 1, NULL),
(71, 'Listing', 70, 0, 'invoice', 'admin', '', 2, 1, NULL),
(72, 'defaultdelete', 70, 0, 'invoice', 'delete', '', 0, 0, NULL),
(73, 'defaultview', 70, 0, 'invoice', 'view', '', 0, 0, NULL),
(74, 'getclientByProject', 70, 0, 'invoice', 'getclientByProject', '', 0, 0, NULL),
(75, 'defaultcreate', 70, 0, 'invoice', 'create', '', 0, 0, NULL),
(76, 'defaultupdate', 70, 0, 'invoice', 'update', '', 0, 0, NULL),
(77, 'View', 70, 0, 'invoice', 'viewinvoice', '', 2, 1, NULL),
(78, 'deleteinvdetails', 70, 0, 'invoice', 'deleteinvdetails', '', 0, 0, NULL),
(79, 'deleteperfdetails', 70, 0, 'invoice', 'deleteperfdetails', '', 0, 0, NULL),
(80, 'viewperforma', 70, 0, 'invoice', 'viewperforma', '', 0, 0, NULL),
(81, 'saveinvoice', 70, 0, 'invoice', 'saveinvoice', '', 0, 0, NULL),
(82, 'performainvoice', 70, 0, 'invoice', 'performainvoice', '', 0, 0, NULL),
(83, 'saveperformainvoice', 70, 0, 'invoice', 'saveperformainvoice', '', 0, 0, NULL),
(84, 'exportinvoice', 70, 0, 'invoice', 'exportinvoice', '', 0, 0, NULL),
(85, 'exportperforma', 70, 0, 'invoice', 'exportperforma', '', 0, 0, NULL),
(86, 'addperforma', 70, 0, 'invoice', 'addperforma', '', 0, 0, NULL),
(87, 'updateperforma', 70, 0, 'invoice', 'updateperforma', '', 0, 0, NULL),
(88, 'Add Sales by Quantity x Rate', 70, 0, 'invoice', 'addinvoice', '', 2, 1, NULL),
(89, 'Update by Quantity x Rate', 70, 0, 'invoice', 'updateinvoice', '', 2, 1, NULL),
(90, 'testmail', 70, 0, 'invoice', 'testmail', '', 0, 0, NULL),
(91, 'createnewinvoice', 70, 0, 'invoice', 'createnewinvoice', '', 0, 0, NULL),
(92, 'test', 70, 0, 'invoice', 'test', '', 0, 0, NULL),
(93, 'invoiceitem', 70, 0, 'invoice', 'invoiceitem', '', 0, 0, NULL),
(94, 'updatesinvoiceitem', 70, 0, 'invoice', 'updatesinvoiceitem', '', 0, 0, NULL),
(95, 'removeinvoiceitem', 70, 0, 'invoice', 'removeinvoiceitem', '', 0, 0, NULL),
(96, 'getClient', 70, 0, 'invoice', 'getClient', '', 0, 0, NULL),
(97, 'Ajaxdate', 70, 0, 'invoice', 'Ajaxdate', '', 0, 0, NULL),
(98, 'Quotation', 0, 0, 'quotation', '', '', 0, 1, NULL),
(99, 'Listing', 98, 0, 'quotation', 'admin', '', 2, 1, NULL),
(100, 'defaultview', 98, 0, 'quotation', 'view', '', 0, 0, NULL),
(101, 'defaultcreate', 98, 0, 'quotation', 'create', '', 0, 0, NULL),
(102, 'defaultupdate', 98, 0, 'quotation', 'update', '', 0, 0, NULL),
(103, 'Savetopdf', 98, 0, 'quotation', 'Savetopdf', '', 0, 0, NULL),
(104, 'Savetoexcel', 98, 0, 'quotation', 'Savetoexcel', '', 0, 0, NULL),
(105, 'Create', 98, 0, 'quotation', 'Addquotation', '', 2, 1, NULL),
(106, 'Update', 98, 0, 'quotation', 'Updatequotation', '', 2, 1, NULL),
(107, 'View', 98, 0, 'quotation', 'Viewquotation', '', 2, 1, NULL),
(108, 'getClient', 98, 0, 'quotation', 'getClient', '', 0, 0, NULL),
(109, 'Ajaxdate', 98, 0, 'quotation', 'Ajaxdate', '', 0, 0, NULL),
(110, 'Createnewquotation', 98, 0, 'quotation', 'Createnewquotation', '', 0, 0, NULL),
(111, 'quotationitem', 98, 0, 'quotation', 'quotationitem', '', 0, 0, NULL),
(112, 'updatesquotationitem', 98, 0, 'quotation', 'updatesquotationitem', '', 0, 0, NULL),
(113, 'removequotationitem', 98, 0, 'quotation', 'removequotationitem', '', 0, 0, NULL),
(114, 'SaveQuotation', 98, 0, 'quotation', 'SaveQuotation', '', 0, 0, NULL),
(115, 'ExportQuotation', 98, 0, 'quotation', 'ExportQuotation', '', 0, 0, NULL),
(116, 'getProject', 98, 0, 'quotation', 'getProject', '', 0, 0, NULL),
(117, 'defaultdelete', 98, 0, 'quotation', 'delete', '', 0, 0, NULL),
(118, 'Daybook', 0, 0, 'expenses', '', '', 0, 1, NULL),
(119, 'defaultindex', 118, 0, 'expenses', 'index', '', 0, 0, NULL),
(120, 'defaultview', 118, 0, 'view', 'view', '', 0, 0, NULL),
(121, 'admin', 118, 0, 'expenses', 'admin', '', 0, 0, NULL),
(122, 'defaultdelete', 118, 0, 'expenses', 'delete', '', 0, 0, NULL),
(123, 'defaultcreate', 118, 0, 'expenses', 'create', '', 0, 0, NULL),
(124, 'defaultupdate', 118, 0, 'expenses', 'update', '', 0, 0, NULL),
(125, 'Listing', 118, 0, 'expenses', 'dailyEntries', '', 2, 1, NULL),
(126, 'dynamicDropdown', 118, 0, 'expenses', 'dynamicDropdown', '', 0, 0, NULL),
(127, 'dynamicVendor', 118, 0, 'expenses', 'dynamicVendor', '', 0, 0, NULL),
(128, 'addDaybook', 118, 0, 'expenses', 'addDaybook', '', 0, 0, NULL),
(129, 'updateDaybook', 118, 0, 'expenses', 'updateDaybook', '', 0, 0, NULL),
(130, 'getDataByDate', 118, 0, 'expenses', 'getDataByDate', '', 0, 0, NULL),
(131, 'getBillDetails', 118, 0, 'expenses', 'getBillDetails', '', 0, 0, NULL),
(132, 'Receipt Report', 561, 0, 'expenses', 'paymentreport', '', 2, 1, NULL),
(133, 'savetopdf', 118, 0, 'expenses', 'savetopdf', '', 0, 0, NULL),
(134, 'savetoexcel', 118, 0, 'expenses', 'savetoexcel', '', 0, 0, NULL),
(135, 'Day Book Transaction Report', 561, 0, 'expenses', 'newlist', '', 2, 1, NULL),
(136, 'expensereport', 561, 0, 'expenses', 'expensereport', '', 0, 0, NULL),
(137, 'savetopdfmonthly', 118, 0, 'expenses', 'savetopdfmonthly', '', 0, 0, NULL),
(138, 'savetoexcelmonthly', 118, 0, 'expenses', 'savetoexcelmonthly', '', 0, 0, NULL),
(139, 'savetopdf1', 118, 0, 'expenses', 'savetopdf1', '', 0, 0, NULL),
(140, 'savetoexcel1', 118, 0, 'expenses', 'savetoexcel1', '', 0, 0, NULL),
(141, 'deletedaybookentry', 118, 0, 'expenses', 'deletedaybookentry', '', 0, 0, NULL),
(142, 'getDaybookDetails', 118, 0, 'expenses', 'getDaybookDetails', '', 0, 0, NULL),
(143, 'list', 118, 0, 'expenses', 'list', '', 0, 0, NULL),
(144, 'P/L Report', 561, 0, 'expenses', 'profitandloss', '', 2, 1, NULL),
(145, 'getInvoiceDetails', 118, 0, 'expenses', 'getInvoiceDetails', '', 0, 0, NULL),
(146, 'ajaxcall', 118, 0, 'expenses', 'ajaxcall', '', 0, 0, NULL),
(147, 'deleteexpense', 118, 0, 'expenses', 'deleteexpense', '', 0, 0, NULL),
(148, 'DynamicBillorInvoice', 118, 0, 'expenses', 'DynamicBillorInvoice', '', 0, 0, NULL),
(149, 'Edit', 118, 0, 'expenses', 'expensesedit', '', 2, 1, NULL),
(150, 'Delete', 118, 0, 'expenses', 'expensesdelete', '', 2, 1, NULL),
(151, 'Daily Expenses', 0, 0, 'dailyexpense', '', '', 0, 1, NULL),
(152, 'defaultview', 151, 0, 'dailyexpense', 'view', '', 0, 0, NULL),
(153, 'Listing', 151, 0, 'dailyexpense', 'expenses', '', 2, 1, NULL),
(154, 'defaultcreate', 151, 1, 'dailyexpense', 'create', '', 0, 0, NULL),
(155, 'defaultupdate', 151, 1, 'dailyexpense', 'update', '', 0, 0, NULL),
(156, 'getdata', 151, 0, 'dailyexpense', 'getdata', '', 0, 0, NULL),
(157, 'defaultdelete', 151, 0, 'dailyexpense', 'delete', '', 0, 0, NULL),
(158, 'progressSave', 151, 0, 'dailyexpense', 'progressSave', '', 0, 0, NULL),
(159, 'savedata', 151, 0, 'dailyexpense', 'savedata', '', 0, 0, NULL),
(160, 'expensedata', 151, 0, 'dailyexpense', 'expensedata', '', 0, 0, NULL),
(161, 'handsondelete', 151, 0, 'dailyexpense', 'handsondelete', '', 0, 0, NULL),
(162, 'newlist', 151, 0, 'dailyexpense', 'newlist', '', 0, 0, NULL),
(163, 'Updatedailyexp', 151, 0, 'dailyexpense', 'Updatedailyexp', '', 0, 0, NULL),
(164, 'getexpensetypes', 151, 0, 'dailyexpense', 'getexpensetypes', '', 0, 0, NULL),
(165, 'deletedailyexp', 151, 0, 'dailyexpense', 'deletedailyexp', '', 0, 0, NULL),
(166, 'savetoexcel1', 151, 0, 'dailyexpense', 'savetoexcel1', '', 0, 0, NULL),
(167, 'savetopdf1', 151, 0, 'dailyexpense', 'savetopdf1', '', 0, 0, NULL),
(168, 'dynamicvendor', 151, 0, 'dailyexpense', 'dynamicvendor', '', 0, 0, NULL),
(169, 'getdatabydate', 151, 0, 'dailyexpense', 'getdatabydate', '', 0, 0, NULL),
(170, 'getbilldetails', 151, 0, 'dailyexpense', 'getbilldetails', '', 0, 0, NULL),
(171, 'getdaybookdetails', 151, 0, 'dailyexpense', 'getdaybookdetails', '', 0, 0, NULL),
(172, 'adddailyexpense', 151, 0, 'dailyexpense', 'adddailyexpense', '', 0, 0, NULL),
(173, 'updatedailyexpense', 151, 0, 'dailyexpense', 'updatedailyexpense', '', 0, 0, NULL),
(174, 'Daily Expense Report', 561, 0, 'dailyexpense', 'dailyexpensereport', '', 2, 1, NULL),
(175, 'savetopdfdailyexpense', 151, 0, 'dailyexpense', 'savetopdfdailyexpense', '', 0, 0, NULL),
(176, 'savetoexceldailyexpense', 151, 0, 'dailyexpense', 'savetoexceldailyexpense', '', 0, 0, NULL),
(177, 'ajaxcall', 151, 0, 'dailyexpense', 'ajaxcall', '', 0, 0, NULL),
(178, 'deletexpense', 151, 0, 'dailyexpense', 'deletexpense', '', 0, 0, NULL),
(179, 'Petty Cash Report', 561, 0, 'dailyexpense', 'pettycash', '', 2, 1, NULL),
(180, 'pettycashpdfdetails', 151, 0, 'dailyexpense', 'pettycashpdfdetails', '', 0, 0, NULL),
(181, 'pettycashexceldetails', 151, 0, 'dailyexpense', 'pettycashexceldetails', '', 0, 0, NULL),
(182, 'savetopdfpetty', 151, 0, 'dailyexpense', 'savetopdfpetty', '', 0, 0, NULL),
(183, 'savetoexcelpetty', 151, 0, 'dailyexpense', 'savetoexcelpetty', '', 0, 0, NULL),
(184, 'Salary Report', 561, 0, 'dailyexpense', 'salaryreport', '', 2, 1, NULL),
(185, 'savetopdfsalary', 151, 0, 'dailyexpense', 'savetopdfsalary', '', 0, 0, NULL),
(186, 'savetoexcelsalary', 151, 0, 'dailyexpense', 'savetoexcelsalary', '', 0, 0, NULL),
(187, 'salarypdfdetails', 151, 0, 'dailyexpense', 'salarypdfdetails', '', 0, 0, NULL),
(188, 'salaryexceldetails', 151, 0, 'dailyexpense', 'salaryexceldetails', '', 0, 0, NULL),
(189, 'Edit', 151, 0, 'dailyexpense', 'dailyexpenseedit', '', 2, 1, NULL),
(190, 'Delete', 151, 0, 'dailyexpense', 'dailyexpensedelete', '', 2, 1, NULL),
(191, 'Create', 151, 0, 'dailyexpense', 'dailyexpensecreate', '', 2, 1, NULL),
(192, 'Create', 118, 0, 'expenses', 'expensescreate', '', 2, 1, NULL),
(193, 'Labour Report', 0, 0, 'DailyReport', '', '', 0, 1, NULL),
(194, 'defaultview', 193, 0, 'DailyReport', 'view', '', 0, 0, NULL),
(195, 'completiondate', 193, 0, 'DailyReport', 'completiondate', '', 0, 0, NULL),
(196, 'Daily Report Listing', 193, 0, 'DailyReport', 'admin', '', 0, 0, NULL),
(197, 'defaultdelete', 193, 0, 'DailyReport', 'delete', '', 0, 0, NULL),
(198, 'Create', 193, 0, 'DailyReport', 'createdailyreport', '', 2, 1, NULL),
(199, 'addreport', 193, 0, 'DailyReport', 'addreport', '', 0, 0, NULL),
(200, 'update', 193, 0, 'DailyReport', 'update', '', 0, 0, NULL),
(201, 'create1', 193, 0, 'DailyReport', 'create1', '', 0, 0, NULL),
(202, 'completiondate1', 193, 0, 'DailyReport', 'completiondate1', '', 0, 0, NULL),
(203, 'completiondatedaily', 193, 0, 'DailyReport', 'completiondatedaily', '', 0, 0, NULL),
(204, 'update1', 193, 0, 'DailyReport ', 'update1', '', 0, 0, NULL),
(205, 'getdata', 193, 0, 'DailyReport', 'getdata', '', 0, 0, NULL),
(206, 'redirecturl', 193, 0, 'DailyReport', 'redirecturl', '', 0, 0, NULL),
(207, 'savemobiledata', 193, 0, 'DailyReport', 'savemobiledata', '', 0, 0, NULL),
(208, 'Vendors', 0, 0, 'vendors', '', '', 0, 1, NULL),
(209, 'Listing', 208, 0, 'vendors', 'newlist', '', 2, 1, NULL),
(210, 'Vendor Payment', 208, 0, 'vendors', 'dailyEntries', '', 2, 1, NULL),
(211, 'savetoexcel', 208, 0, 'vendors', 'savetoexcel', '', 0, 0, NULL),
(212, 'vendordetails', 208, 0, 'vendors', 'vendordetails', '', 0, 0, NULL),
(213, 'savedata', 208, 0, 'vendors', 'savedata', '', 0, 0, NULL),
(214, 'handsonDelete', 208, 0, 'vendors', 'handsonDelete', '', 0, 0, NULL),
(215, 'vendorData', 208, 0, 'vendors', 'vendorData', '', 0, 0, NULL),
(216, 'Vendor Payment Report', 561, 0, 'vendors', 'paymentReport', '', 2, 1, NULL),
(217, 'savePaymentreport', 208, 0, 'vendors', 'savePaymentreport', '', 0, 0, NULL),
(218, 'progressSave', 208, 0, 'vendors', 'progressSave', '', 0, 0, NULL),
(219, 'savetopdf1', 208, 0, 'vendors', 'savetopdf1', '', 0, 0, NULL),
(220, 'savetoexcel1', 208, 0, 'vendors', 'savetoexcel1', '', 0, 0, NULL),
(221, 'getDailyVendorDetails', 208, 0, 'vendors', 'getDailyVendorDetails', '', 0, 0, NULL),
(222, 'getDataByDate', 208, 0, 'vendors', 'getDataByDate', '', 0, 0, NULL),
(223, 'addDailyentries', 208, 0, 'vendors', 'addDailyentries', '', 0, 0, NULL),
(224, 'updateDailyentries', 208, 0, 'vendors', 'updateDailyentries', '', 0, 0, NULL),
(225, 'Ajaxcall', 208, 0, 'vendors', 'Ajaxcall', '', 0, 0, NULL),
(226, 'deletdailyvendors', 208, 0, 'vendors', 'deletdailyvendors', '', 0, 0, NULL),
(227, 'Create', 208, 0, 'vendors', 'create', '', 2, 1, NULL),
(228, 'Update', 208, 0, 'vendors', 'update', '', 2, 1, NULL),
(229, 'defaultdelete', 208, 0, 'vendors', 'delete', '', 0, 0, NULL),
(230, 'Vendor Payment Create', 208, 0, 'vendors', 'paymentcreate', '', 2, 1, NULL),
(231, 'Vendor Payment Edit', 208, 0, 'vendors', 'paymentedit', '', 2, 1, NULL),
(232, 'Vendor Payment Delete', 208, 0, 'vendors', 'paymentdelete', '', 2, 1, NULL),
(233, 'Subcontractor', 0, 0, 'subcontractor', '', '', 0, 1, NULL),
(234, 'Listing', 233, 0, 'subcontractor', 'newlist', '', 2, 1, NULL),
(235, 'Create', 233, 0, 'subcontractor', 'subcontractorcreate', '', 2, 1, NULL),
(236, 'Edit', 233, 0, 'subcontractor', 'subcontractoredit', '', 2, 1, NULL),
(237, 'Quotation Approval', 233, 0, 'subcontractor', 'permissionapprove', '', 2, 1, NULL),
(238, 'Quotations', 233, 0, 'subcontractor', 'quotations', '', 2, 1, NULL),
(239, 'create1', 233, 0, 'subcontractor', 'create1', '', 0, 0, NULL),
(240, 'update1', 233, 0, 'subcontractor', 'update1', '', 0, 0, NULL),
(241, 'getQuotations', 233, 0, 'subcontractor', 'getQuotations', '', 0, 0, NULL),
(242, 'deleteQuotations', 233, 0, 'subcontractor', 'deleteQuotations', '', 0, 0, NULL),
(243, 'Create Quotation By Lumpsum', 233, 0, 'subcontractor', 'addquotation', '', 2, 1, NULL),
(244, 'ajax', 233, 0, 'subcontractor', 'ajax', '', 0, 0, NULL),
(245, 'createnewquotation', 233, 0, 'subcontractor', 'createnewquotation', '', 0, 0, NULL),
(246, 'quotationitem', 233, 0, 'subcontractor', 'quotationitem', '', 0, 0, NULL),
(247, 'updatesquotationitem', 233, 0, 'subcontractor', 'updatesquotationitem', '', 0, 0, NULL),
(248, 'removequotationitem', 233, 0, 'subcontractor', 'removequotationitem', '', 0, 0, NULL),
(249, 'Edit Quotation', 233, 0, 'subcontractor', 'editquotation', '', 2, 1, NULL),
(250, 'permissionapprove', 233, 0, 'subcontractor', 'permissionapprove', '', 2, 0, NULL),
(251, 'getproject', 233, 0, 'subcontractor', 'getproject', '', 0, 0, NULL),
(252, 'permissionsave', 233, 0, 'subcontractor', 'permissionsave', '', 0, 0, NULL),
(253, 'defaultindex', 233, 0, 'subcontractor', 'index', '', 0, 0, NULL),
(254, 'defaultview', 233, 0, 'subcontractor', 'view', '', 0, 0, NULL),
(255, 'defaultcreate', 233, 0, 'subcontractor', 'create', '', 0, 0, NULL),
(256, 'defaultupdate', 233, 0, 'subcontractor', 'update', '', 0, 0, NULL),
(257, 'defaultdelete', 233, 0, 'subcontractor', 'delete', '', 0, 0, NULL),
(258, 'admin', 233, 0, 'subcontractor', 'admin', '', 0, 0, NULL),
(259, 'Sub Contractor Payment', 0, 0, 'subcontractorpayment', '', '', 0, 1, NULL),
(260, 'Listing', 259, 0, 'subcontractorpayment', 'dailyentries', '', 2, 1, NULL),
(261, 'Create', 259, 0, 'subcontractorpayment', 'paymentcreate', '', 2, 1, NULL),
(262, 'Edit', 259, 0, 'subcontractorpayment', 'paymentedit', '', 2, 1, NULL),
(263, 'Delete', 259, 0, 'subcontractorpayment', 'paymentdelete', '', 2, 1, NULL),
(264, 'defaultindex', 259, 0, 'subcontractorpayment', 'index', '', 0, 0, NULL),
(265, 'defaultview', 259, 0, 'subcontractorpayment', 'view', '', 0, 0, NULL),
(266, 'newlist', 259, 0, 'subcontractorpayment', 'newlist', '', 0, 0, NULL),
(267, 'quotations', 259, 0, 'subcontractorpayment', 'quotations', '', 0, 0, NULL),
(268, 'addDailyentries', 259, 0, 'subcontractorpayment', 'addDailyentries', '', 0, 0, NULL),
(269, 'getpaymentdetails', 259, 0, 'subcontractorpayment', 'getpaymentdetails', '', 0, 0, NULL),
(270, 'getTotalFromPayment', 259, 0, 'subcontractorpayment', 'getTotalFromPayment', '', 0, 0, NULL),
(271, 'updateDailyentries', 259, 0, 'subcontractorpayment', 'updateDailyentries', '', 0, 0, NULL),
(272, 'GetDataByDate', 259, 0, 'subcontractorpayment', 'GetDataByDate', '', 0, 0, NULL),
(273, 'Sub Contractor Payment Report', 561, 0, 'subcontractorpayment', 'report', '', 2, 1, NULL),
(274, 'Savetopdf1', 259, 0, 'subcontractorpayment', 'Savetopdf1', '', 0, 0, NULL),
(275, 'Savetoexcel1', 259, 0, 'subcontractorpayment', 'Savetoexcel1', '', 0, 0, NULL),
(276, 'savePaymentreport', 259, 0, 'subcontractorpayment', 'savePaymentreport', '', 0, 0, NULL),
(277, 'savetoexcel', 259, 0, 'subcontractorpayment', 'savetoexcel', '', 0, 0, NULL),
(278, 'deletdailyentry', 259, 0, 'subcontractorpayment', 'deletdailyentry', '', 0, 0, NULL),
(279, 'create1', 259, 0, 'subcontractorpayment', 'create1', '', 0, 0, NULL),
(280, 'update1', 259, 0, 'subcontractorpayment', 'update1', '', 0, 0, NULL),
(281, 'dynamicProject', 259, 0, 'subcontractorpayment', 'dynamicProject', '', 0, 0, NULL),
(282, 'ajaxcall', 259, 0, 'subcontractorpayment', 'ajaxcall', '', 0, 0, NULL),
(283, 'totalpaymetcalculation', 259, 0, 'subcontractorpayment', 'totalpaymetcalculation', '', 0, 0, NULL),
(284, 'defaultcreate', 259, 0, 'subcontractorpayment', 'create', '', 0, 0, NULL),
(285, 'defaultupdate', 259, 0, 'subcontractorpayment', 'update', '', 0, 0, NULL),
(286, 'defaultdelete', 259, 0, 'subcontractorpayment', 'delete', '', 0, 0, NULL),
(287, 'admin', 259, 0, 'subcontractorpayment', 'admin', '', 0, 0, NULL),
(288, 'Project', 0, 0, 'projects', '', '', 0, 1, NULL),
(289, 'Listing', 288, 0, 'projects', 'newlist', '', 2, 1, NULL),
(290, 'Create', 288, 0, 'projects', 'createproject', '', 2, 1, NULL),
(291, 'Update', 288, 0, 'projects', 'updateproject', '', 2, 1, NULL),
(292, 'getprojects', 288, 0, 'projects', 'getprojects', '', 0, 0, NULL),
(293, 'getexptypes', 288, 0, 'projects', 'getexptypes', '', 0, 0, NULL),
(294, 'Project Report', 561, 0, 'projects', 'expenselist', '', 2, 1, NULL),
(295, 'savetoexcelstatus', 288, 0, 'projects', 'savetoexcelstatus', '', 0, 0, NULL),
(296, 'savetoexcel', 288, 0, 'projects', 'savetoexcel', '', 0, 0, NULL),
(297, 'savetoexcelcompleted', 288, 0, 'projects', 'savetoexcelcompleted', '', 0, 0, NULL),
(298, 'savetopdf', 288, 0, 'projects', 'savetopdf', '', 0, 0, NULL),
(299, 'savetocompletedpdf', 288, 0, 'projects', 'savetocompletedpdf', '', 0, 0, NULL),
(300, 'exportcsv', 288, 0, 'projects', 'exportcsv', '', 0, 0, NULL),
(301, 'saveexpenselist', 288, 0, 'projects', 'saveexpenselist', '', 0, 0, NULL),
(302, 'projectentries', 288, 0, 'projects', 'projectentries', '', 0, 0, NULL),
(303, 'projectdata', 288, 0, 'projects', 'projectdata', '', 0, 0, NULL),
(304, 'projectdetails', 288, 0, 'projects', 'projectdetails', '', 0, 0, NULL),
(305, 'savedata', 288, 0, 'projects', 'savedata', '', 0, 0, NULL),
(306, 'handsondelete', 288, 0, 'projects', 'handsondelete', '', 0, 0, NULL),
(307, 'progresssave', 288, 0, 'projects', 'progresssave', '', 0, 0, NULL),
(308, 'performainvoice', 288, 0, 'projects', 'performainvoice', '', 0, 0, NULL),
(309, 'saveperformainvoice', 288, 0, 'projects', 'saveperformainvoice', '', 0, 0, NULL),
(310, 'performaexport', 288, 0, 'projects', 'performaexport', '', 0, 0, NULL),
(311, 'savetopdfreport', 288, 0, 'projects', 'savetopdfreport', '', 0, 0, NULL),
(312, 'savetoexcelreport', 288, 0, 'projects', 'savetoexcelreport', '', 0, 0, NULL),
(313, 'defaultcreate', 288, 0, 'projects', 'create', '', 0, 0, NULL),
(314, 'defaultupdate', 288, 0, 'projects', 'update', '', 0, 0, NULL),
(315, 'admin', 288, 0, 'projects', 'admin', '', 0, 0, NULL),
(316, 'updatepro', 288, 0, 'projects', 'updatepro', '', 0, 0, NULL),
(317, 'Financial Report', 561, 0, 'projects', 'financialreport', '', 2, 1, NULL),
(318, 'saveprojectreport', 288, 0, 'projects', 'saveprojectreport', '', 0, 0, NULL),
(319, 'Completed Projects', 561, 0, 'projects', 'completedprojects', '', 2, 1, NULL),
(320, 'Work Status Report', 561, 0, 'projects', 'projectreport', '', 2, 1, NULL),
(321, 'defaultdelete', 288, 0, 'projects', 'delete', '', 0, 0, NULL),
(322, 'deleteprojects', 288, 0, 'projects', 'deleteprojects', '', 0, 0, NULL),
(323, 'totals', 288, 0, 'projects', 'totals', '', 0, 0, NULL),
(324, 'Clients', 0, 0, 'clients', '', '', 0, 1, NULL),
(325, 'Listing', 324, 0, 'clients', 'newlist', '', 2, 1, NULL),
(326, 'Bank', 0, 0, 'bank', '', '', 0, 1, NULL),
(327, 'Listing', 326, 0, 'bank', 'newList', '', 2, 1, NULL),
(328, 'defaultview', 324, 0, 'clients', 'view', '', 0, 0, NULL),
(329, 'viewpartial', 324, 0, 'clients', 'viewpartial', '', 0, 0, NULL),
(330, 'defaultcreate', 324, 0, 'clients', 'create', '', 0, 0, NULL),
(331, 'defaultupdate', 324, 0, 'clients', 'update', '', 0, 0, NULL),
(332, 'defaultdelete', 324, 0, 'clients', 'delete', '', 0, 0, NULL),
(333, 'Create', 324, 0, 'clients', 'createclients', '', 2, 1, NULL),
(334, 'Update', 324, 0, 'clients', 'updateclients', '', 2, 1, NULL),
(335, 'defaultindex', 326, 0, 'bank', 'index', '', 0, 0, NULL),
(336, 'defaultview', 326, 0, 'bank', 'view', '', 0, 0, NULL),
(337, 'defaultcreate', 326, 0, 'bank', 'create', '', 0, 0, NULL),
(338, 'defaultupdate', 326, 0, 'bank', 'update', '', 0, 0, NULL),
(339, 'savetopdf', 326, 0, 'bank', 'savetopdf', '', 0, 0, NULL),
(340, 'savetoexcel', 326, 0, 'bank', 'savetoexcel', '', 0, 0, NULL),
(341, 'Bank Reconciliation', 326, 0, 'bank', 'reconciliation', '', 2, 1, NULL),
(342, 'reconciliationentry', 326, 0, 'bank', 'reconciliationentry', '', 0, 0, NULL),
(343, 'Bank Reconciliation Report', 561, 0, 'bank', 'reconciliationreport', '', 2, 1, NULL),
(344, 'reconciliationedit', 326, 0, 'bank', 'reconciliationedit', '', 0, 0, NULL),
(345, 'admin', 326, 0, 'bank', 'admin', '', 0, 0, NULL),
(346, 'defaultdelete', 326, 0, 'bank', 'delete', '', 0, 0, NULL),
(347, 'Create', 326, 0, 'bank', 'createbank', '', 2, 1, NULL),
(348, 'Update', 326, 0, 'bank', 'updatebank', '', 2, 1, NULL),
(349, 'Client Type', 0, 0, 'projectType', '', '', 0, 1, NULL),
(350, 'defaultview', 349, 0, 'projectType', 'view', '', 0, 0, NULL),
(351, 'Listing', 349, 0, 'projectType', 'newlist', '', 2, 1, NULL),
(352, 'defaultcreate', 349, 0, 'projectType', 'create', '', 0, 0, NULL),
(353, 'defaultupdate', 349, 0, 'projectType', 'update', '', 0, 0, NULL),
(354, 'admin', 349, 0, 'projectType', 'admin', '', 0, 0, NULL),
(355, 'defaultdelete', 349, 0, 'projectType', 'delete', '', 0, 0, NULL),
(356, 'index', 349, 0, 'projectType', 'index', '', 0, 0, NULL),
(357, 'Create', 349, 0, 'projectType', 'createprojecttype', '', 2, 1, NULL),
(358, 'Update', 349, 0, 'projectType', 'updateprojecttype', '', 2, 1, NULL),
(359, 'User Roles', 0, 0, 'userRoles', '', '', 0, 1, NULL),
(360, 'defaultcreate', 359, 0, 'userRoles', 'create', '', 0, 0, NULL),
(361, 'Update', 359, 0, 'userRoles', 'update', '', 2, 1, NULL),
(362, 'Listing', 359, 0, 'userRoles', 'newlist', '', 2, 1, NULL),
(363, 'register', 1, 0, 'users', 'register', '', 0, 0, NULL),
(364, 'pwdrecovery', 1, 0, 'users', 'pwdrecovery', '', 0, 0, NULL),
(365, 'pwdreset', 1, 0, 'users', 'pwdreset', '', 0, 0, NULL),
(366, 'message_page', 1, 0, 'users', 'message_page', '', 0, 0, NULL),
(367, 'activation', 1, 0, 'users', 'activation', '', 0, 0, NULL),
(368, 'googleauth', 1, 0, 'users', 'googleauth', '', 0, 0, NULL),
(369, 'myprofile', 1, 0, 'users', 'myprofile', '', 0, 0, NULL),
(370, 'changepass', 1, 0, 'users', 'changepass', '', 0, 0, NULL),
(371, 'passchange', 1, 0, 'users', 'passchange', '', 0, 0, NULL),
(372, 'editprofile', 1, 0, 'users', 'editprofile', '', 0, 0, NULL),
(373, 'userlist', 1, 0, 'users', 'userlist', '', 0, 0, NULL),
(374, 'defaultcreate', 1, 0, 'users', 'create', '', 0, 0, NULL),
(375, 'update', 1, 0, 'users', 'update', '', 0, 0, NULL),
(376, 'defaultdelete', 1, 0, 'users', 'delete', '', 0, 0, NULL),
(377, 'defaultview', 1, 0, 'users', 'view', '', 0, 0, NULL),
(378, 'deleteuser', 1, 0, 'users', 'deleteuser', '', 0, 0, NULL),
(379, 'usershift', 1, 0, 'users', 'usershift', '', 0, 0, NULL),
(380, 'Create', 1, 0, 'users', 'createusers', '', 2, 1, NULL),
(381, 'Update', 1, 0, 'users', 'updateusers', '', 2, 1, NULL),
(382, 'Expense Head', 0, 0, 'expensetype', '', '', 0, 1, NULL),
(383, 'Listing', 382, 0, 'expensetype', 'newlist', '', 2, 1, NULL),
(384, 'defaultindex', 382, 0, 'expensetype', 'index', '', 0, 0, NULL),
(385, 'defaultview', 382, 0, 'expensetype', 'view', '', 0, 0, NULL),
(386, 'defaultdelete', 382, 0, 'expensetype', 'create', '', 0, 0, NULL),
(387, 'defaultupdate', 382, 0, 'expensetype', 'update', '', 0, 0, NULL),
(388, 'savetopdf', 382, 0, 'expensetype', 'savetopdf', '', 0, 0, NULL),
(389, 'savetoexcel', 382, 0, 'expensetype', 'savetoexcel', '', 0, 0, NULL),
(390, 'admin', 382, 0, 'expensetype', 'admin', '', 0, 0, NULL),
(391, 'defaultdelete', 382, 0, 'expensetype', 'delete', '', 0, 0, NULL),
(392, 'Create', 382, 0, 'expensetype', 'createexpensetype', '', 2, 1, NULL),
(393, 'Update', 382, 0, 'expensetype', 'updateexpensetype', '', 2, 1, NULL),
(394, 'Daily ExpenseType', 0, 0, 'companyexpensetype', '', '', 0, 1, NULL),
(395, 'defaultview', 394, 0, 'companyexpensetype', 'view', '', 0, 0, NULL),
(396, 'Listing', 394, 0, 'companyexpensetype', 'admin', '', 2, 1, NULL),
(397, 'Create', 394, 0, 'companyexpensetype', 'create', '', 2, 1, NULL),
(398, 'Update', 394, 0, 'companyexpensetype', 'update', '', 2, 1, NULL),
(399, 'defaultdelete', 394, 0, 'companyexpensetype', 'delete', '', 0, 0, NULL),
(400, 'Work Type', 0, 0, 'workType', '', '', 0, 1, NULL),
(401, 'Listing', 400, 0, 'workType', 'newlist', '', 2, 1, NULL),
(402, 'defaultview', 400, 0, 'workType', 'view', '', 0, 0, NULL),
(403, 'Create', 400, 0, 'workType', 'create', '', 2, 1, NULL),
(404, 'Update', 400, 0, 'workType', 'update', '', 2, 1, NULL),
(405, 'update2', 400, 0, 'workType', 'update2', '', 0, 0, NULL),
(406, 'delete', 400, 0, 'workType', 'delete', '', 0, 0, NULL),
(407, 'update2', 326, 0, 'bank', 'update2', '', 0, 0, NULL),
(408, 'Log', 0, 0, 'log', '', '', 0, 1, NULL),
(409, 'view', 408, 0, 'log', 'view', '', 0, 0, NULL),
(410, 'create', 408, 0, 'log', 'create', '', 0, 0, NULL),
(411, 'update', 408, 0, 'log', 'update', '', 0, 0, NULL),
(412, 'delete', 408, 0, 'log', 'delete', '', 0, 0, NULL),
(413, 'updatelog', 408, 0, 'log', 'updatelog', '', 0, 0, NULL),
(414, 'deletelog', 408, 0, 'log', 'deletelog', '', 0, 0, NULL),
(415, 'historylog', 408, 0, 'log', 'historylog', '', 2, 1, NULL),
(416, 'Company', 0, 0, 'company', '', '', 0, 1, NULL),
(417, 'Listing', 416, 0, 'company', 'newlist', '', 2, 1, NULL),
(418, 'view', 416, 0, 'company', 'view', '', 0, 0, NULL),
(419, 'Create', 416, 0, 'company', 'create', '', 2, 1, NULL),
(420, 'Update', 416, 0, 'company', 'update', '', 2, 1, NULL),
(421, 'admin', 416, 0, 'company', 'admin', '', 0, 0, NULL),
(422, 'delete', 416, 0, 'company', 'delete', '', 0, 0, NULL),
(423, 'index', 416, 0, 'company', 'index', '', 0, 0, NULL),
(424, 'Purchase Item', 0, 0, 'purchaseCategory', '', '', 0, 1, NULL),
(425, 'Listing', 424, 0, 'purchaseCategory', 'newList', '', 2, 1, NULL),
(426, 'index', 424, 0, 'purchaseCategory', 'index', '', 0, 0, NULL),
(427, 'view', 424, 0, 'purchaseCategory', 'view', '', 0, 0, NULL),
(428, 'create', 424, 0, 'purchaseCategory', 'create', '', 0, 0, NULL),
(429, 'update', 424, 0, 'purchaseCategory', 'update', '', 0, 0, NULL),
(430, 'createspecification', 424, 0, 'purchaseCategory', 'createspecification', '', 0, 0, NULL),
(431, 'checkspecification', 424, 0, 'purchaseCategory', 'checkspecification', '', 0, 0, NULL),
(432, 'checkcategory', 424, 0, 'purchaseCategory', 'checkcategory', '', 0, 0, NULL),
(433, 'test', 424, 0, 'purchaseCategory', 'test', '', 0, 0, NULL),
(434, 'Edit Item', 424, 0, 'purchaseCategory', 'getItemForEdit', '', 2, 1, NULL),
(435, 'Edit Specification', 424, 0, 'purchaseCategory', 'getSpecificationForEdit', '', 2, 1, NULL),
(436, 'updatespecification', 424, 0, 'purchaseCategory', 'updatespecification', '', 0, 0, NULL),
(437, 'Edit Brand', 424, 0, 'purchaseCategory', 'editbrand', '', 2, 1, NULL),
(438, 'admin', 424, 0, 'purchaseCategory', 'admin', '', 0, 0, NULL),
(439, 'delete', 424, 0, 'purchaseCategory', 'delete', '', 0, 0, NULL),
(440, 'Unit', 0, 0, 'unit', '', '', 0, 1, NULL),
(441, 'Listing', 440, 0, 'unit', 'newlist', '', 2, 1, NULL),
(442, 'index', 440, 0, 'unit', 'index', '', 0, 0, NULL),
(443, 'create', 440, 0, 'unit', 'create', '', 0, 0, NULL),
(444, 'update', 440, 0, 'unit', 'update', '', 0, 0, NULL),
(445, 'checkunit', 440, 0, 'unit', 'checkunit', '', 0, 0, NULL),
(446, 'admin', 440, 0, 'unit', 'admin', '', 0, 0, NULL),
(447, 'delete', 440, 0, 'unit', 'delete', '', 0, 0, NULL),
(448, 'view', 440, 0, 'unit', 'view', '', 0, 0, NULL),
(449, 'Cashbalance', 0, 0, 'cashbalance', '', '', 0, 1, NULL),
(450, 'Listing', 449, 0, 'cashbalance', 'newlist', '', 2, 1, NULL),
(451, 'index', 449, 0, 'cashbalance', 'index', '', 0, 0, NULL),
(452, 'view', 449, 0, 'cashbalance', 'view', '', 0, 0, NULL),
(453, 'create', 449, 0, 'cashbalance', 'create', '', 2, 1, NULL),
(454, 'update', 449, 0, 'cashbalance', 'update', '', 2, 1, NULL),
(455, 'Cash Balance Report', 561, 0, 'cashbalance', 'cashbalance_report', '', 2, 1, NULL),
(456, 'cashblancereportpdf', 449, 0, 'cashbalance', 'cashblancereportpdf', '', 0, 0, NULL),
(457, 'cashblancereportexcel', 449, 0, 'cashbalance', 'cashblancereportexcel', '', 0, 0, NULL),
(458, 'savetopdf', 449, 0, 'cashbalance', 'savetopdf', '', 0, 0, NULL),
(459, 'savetoexcel', 449, 0, 'cashbalance', 'savetoexcel', '', 0, 0, NULL),
(460, 'Daily Financial Report', 561, 0, 'cashbalance', 'dailyfinancialreport', '', 2, 1, NULL),
(461, 'dailyfinancialdetailedpdf', 449, 0, 'cashbalance', 'dailyfinancialdetailedpdf', '', 0, 0, NULL),
(462, 'Cashbalance', 449, 0, 'cashbalance', 'dailyfinancialdetailedexcel', '', 0, 0, NULL),
(463, 'Daily Financial Summary Report', 561, 0, 'cashbalance', 'dailyfinancialsummary', '', 2, 1, NULL),
(464, 'dailyfinancialsummarypdf', 449, 0, 'cashbalance', 'dailyfinancialsummarypdf', '', 0, 0, NULL),
(465, 'Cashbalance', 449, 0, 'cashbalance', 'dailyfinancialsummaryexcel', '', 0, 0, NULL),
(466, 'admin', 449, 0, 'cashbalance', 'admin', '', 0, 0, NULL),
(467, 'delete', 449, 0, 'cashbalance', 'delete', '', 0, 0, NULL),
(468, 'Deposit', 0, 0, 'deposit', '', '', 0, 1, NULL),
(469, 'index', 468, 0, 'deposit', 'index', '', 0, 0, NULL),
(470, 'view', 468, 0, 'deposit', 'view', '', 0, 0, NULL),
(471, 'Listing', 468, 0, 'deposit', 'newlist', '', 2, 1, NULL),
(472, 'Create', 468, 0, 'deposit', 'create', '', 2, 1, NULL),
(473, 'Update', 468, 0, 'deposit', 'update', '', 2, 1, NULL),
(474, 'Savetopdf', 468, 0, 'deposit', 'Savetopdf', '', 0, 0, NULL),
(475, 'Savetoexcel', 468, 0, 'deposit', 'Savetoexcel', '', 0, 0, NULL),
(476, 'update2', 468, 0, 'deposit', 'update2', '', 0, 0, NULL),
(477, 'admin', 468, 0, 'deposit', 'admin', '', 0, 0, NULL),
(478, 'delete', 468, 0, 'deposit', 'delete', '', 0, 0, NULL),
(479, 'PO Company', 0, 0, 'poCompany', '', '', 0, 1, NULL),
(480, 'index', 479, 0, 'poCompany', 'index', '', 0, 0, NULL),
(481, 'view', 479, 0, 'poCompany', 'view', '', 0, 0, NULL),
(482, 'Listing', 479, 0, 'poCompany', 'newlist', '', 2, 1, NULL),
(483, 'Create', 479, 0, 'poCompany', 'create', '', 2, 1, NULL),
(484, 'Update', 479, 0, 'poCompany', 'update', '', 2, 1, NULL),
(485, 'admin', 479, 0, 'poCompany', 'admin', '', 0, 0, NULL),
(486, 'delete', 479, 0, 'poCompany', 'delete', '', 0, 0, NULL),
(487, 'Home', 0, 0, 'dashboard', '', '', 0, 1, NULL),
(488, 'Listing', 487, 0, 'dashboard', 'index', '', 0, 0, NULL),
(489, 'setsessioncompany', 487, 0, 'dashboard', 'setsessioncompany', '', 0, 0, NULL),
(490, 'menu permissions', 0, 0, 'menu/menuPermissions', '', '', 0, 1, NULL),
(491, 'Create', 490, 0, 'menu/menuPermissions', 'create', '', 2, 1, NULL),
(494, 'update2', 359, 0, 'userRoles', 'update2', '', 0, 0, NULL),
(495, 'update2', 349, 0, 'projectType', 'update2', '', 0, 0, NULL),
(496, 'Permission', 259, 0, 'subcontractorpayment', 'permissionapprove', '', 2, 1, NULL),
(497, 'Manage Purchase Item', 424, 0, 'purchaseCategory', 'manageitem', '', 2, 1, NULL),
(498, 'GetPurchaseReturnDetails', 118, 0, 'expenses', 'GetPurchaseReturnDetails', '', 0, 0, NULL),
(499, 'Purchase Return', 0, 0, 'purchaseReturn', '', '', 0, 1, NULL),
(500, 'defaultindex', 499, 0, 'purchaseReturn', 'index', '', 0, 0, NULL),
(501, 'View', 499, 0, 'purchaseReturn', 'view', '', 2, 1, NULL),
(502, 'GetItemsByPurchase', 499, 0, 'purchaseReturn', 'GetItemsByPurchase', '', 0, 0, NULL),
(503, 'Create', 499, 0, 'purchaseReturn', 'create', '', 2, 1, NULL),
(504, 'Update', 499, 0, 'purchaseReturn', 'update', '', 2, 1, NULL),
(505, 'ValidateReturnNumber', 499, 0, 'purchaseReturn', 'ValidateReturnNumber', '', 0, 0, NULL),
(506, 'addItemsForPurchaseReturn', 499, 0, 'purchaseReturn', 'addItemsForPurchaseReturn', '', 0, 0, NULL),
(507, 'UpdateBillsOnReload', 499, 0, 'purchaseReturn', 'UpdateBillsOnReload', '', 0, 0, NULL),
(508, 'GetItemsByBillId', 499, 0, 'purchaseReturn', 'GetItemsByBillId', '', 0, 0, NULL),
(509, 'UpdateItemsToList', 499, 0, 'purchaseReturn', 'UpdateItemsToList', '', 0, 0, NULL),
(510, 'returnpdf', 499, 0, 'purchaseReturn', 'returnpdf', '', 0, 0, NULL),
(511, 'returncsv', 499, 0, 'purchaseReturn', 'returncsv', '', 0, 0, NULL),
(512, 'Listing', 499, 0, 'purchaseReturn', 'admin', '', 2, 1, NULL),
(513, 'defaultdelete', 499, 0, 'purchaseReturn', 'delete', '', 0, 0, NULL),
(514, 'Permission', 3, 0, 'purchase', 'permission', '', 2, 1, NULL),
(515, 'Send Mail', 3, 0, 'purchase', 'sendattachment', '', 2, 1, NULL),
(516, 'projectexpensealert', 208, 0, 'vendors', 'projectexpensealert', '', 0, 0, NULL),
(517, 'projectexpensealert', 118, 0, 'expenses', 'projectexpensealert', '', 0, 0, NULL),
(518, 'projectexpensealert', 259, 0, 'subcontractorpayment', 'projectexpensealert', '', 0, 0, NULL),
(519, 'pdfgeneration', 3, 0, 'purchase', 'pdfgeneration', '', 0, 0, NULL),
(520, 'dynamicSubcontractor', 259, 0, 'subcontractorpayment', 'dynamicSubcontractor', '', 0, 0, NULL),
(521, 'dynamicproject', 3, 0, 'purchase', 'dynamicproject', '', 0, 0, NULL),
(522, 'dynamicpurchase', 33, 0, 'bills', 'dynamicpurchase', '', 0, 0, NULL),
(523, 'dynamicproject', 33, 0, 'bills', 'dynamicproject', '', 0, 0, NULL),
(524, 'dynamicproject', 70, 0, 'invoice', 'dynamicproject', '', 0, 0, NULL),
(525, 'dynamicclient', 98, 0, 'quotation', 'dynamicclient', '', 0, 0, NULL),
(526, 'DynamicProject', 118, 0, 'expenses', 'DynamicProject', '', 0, 0, NULL),
(527, 'dynamicproject', 208, 0, 'vendors', 'dynamicproject', '', 0, 0, NULL),
(528, 'dynamicproject', 233, 0, 'subcontractor', 'dynamicproject', '', 0, 0, NULL),
(529, 'dynamicbank', 449, 0, 'cashbalance', 'dynamicbank', '', 0, 0, NULL),
(530, 'updateinvoicestatus', 70, 0, 'invoice', 'updateinvoicestatus', '', 0, 0, NULL),
(531, 'Add Sales by Lumpsum', 70, 0, 'invoice', 'invoicelumpsum', '', 2, 1, NULL),
(532, 'invoicelumpsumitem', 70, 0, 'invoice', 'invoicelumpsumitem', '', 0, 0, NULL),
(533, 'updateslumpsumitem', 70, 0, 'invoice', 'updateslumpsumitem', '', 0, 0, NULL),
(534, 'Update Sales by Lumpsum', 70, 0, 'invoice', 'updateinvoicelumpsum', '', 2, 1, NULL),
(535, 'removeinvoicelumpsum', 70, 0, 'invoice', 'removeinvoicelumpsum', '', 0, 0, NULL),
(536, 'Company Permission', 288, 0, 'projects', 'companypermission', '', 2, 1, NULL),
(537, 'Users Permission', 288, 0, 'projects', 'userspermission', '', 2, 1, NULL),
(538, 'Expense Head Permission', 288, 0, 'projects', 'expensetypepermission', '', 2, 1, NULL),
(539, 'Work Types Permission', 288, 0, 'projects', 'worktypespermission', '', 2, 1, NULL),
(540, 'Project Quote Permission', 288, 0, 'projects', 'projectquotepermission', '', 2, 1, NULL),
(541, 'Profit Margin Permission', 288, 0, 'projects', 'profitmarginpermission', '', 2, 1, NULL),
(542, 'Company Permission', 324, 0, 'clients', 'companypermission', '', 2, 1, NULL),
(543, 'Payment Reminders', 0, 0, 'paymentReminders', '', '', 0, 1, NULL),
(544, 'defaultindex', 543, 0, 'paymentReminders', 'index', '', 0, 0, NULL),
(545, 'defaultview', 543, 0, 'paymentReminders', 'view', '', 0, 0, NULL),
(546, 'Listing ', 543, 0, 'paymentReminders', 'newlist', '', 2, 1, NULL),
(547, 'viewcomment', 543, 0, 'paymentReminders', 'viewcomment', '', 0, 0, NULL),
(548, 'Add Reminder', 543, 0, 'paymentReminders', 'addcomment', '', 2, 1, NULL),
(549, 'Create', 543, 0, 'paymentReminders', 'create', '', 2, 1, NULL),
(550, 'Update', 543, 0, 'paymentReminders', 'update', '', 2, 1, NULL),
(551, 'admin', 543, 0, 'paymentReminders', 'admin', '', 0, 0, NULL),
(552, 'delete', 543, 0, 'paymentReminders', 'delete', '', 0, 0, NULL),
(553, 'dynamicproject', 543, 0, 'paymentReminders', 'dynamicproject', '', 0, 0, NULL),
(554, 'Cashbalance Report - PDF', 449, 0, 'cashbalance', 'cashblance_pdf', '', 0, 0, NULL),
(555, 'Cashbalance Report - Excel', 449, 0, 'cashbalance', 'cashblance_excel', '', 0, 0, NULL),
(556, 'Dynamicbank', 326, 0, 'bank', 'Dynamicbank', '', 0, 0, NULL),
(557, 'dynamicproject', 193, 0, 'DailyReport', 'dynamicproject', '', 0, 0, NULL),
(558, 'addcashtransfer', 151, 0, 'dailyexpense', 'addcashtransfer', '', 0, 0, NULL),
(559, 'getTransferDetails', 151, 0, 'dailyexpense', 'getTransferDetails', '', 0, 0, NULL),
(560, 'getBankByCompany', 151, 0, 'dailyexpense', 'getBankByCompany', '', 0, 0, NULL),
(561, 'Reports', 0, 0, '#', '', '', 2, 1, NULL),
(562, 'getProjectRow', 259, 0, 'subcontractorpayment', 'getProjectRow', '', 0, 0, NULL),
(563, 'validateSubcontractorLimit', 259, 0, 'subcontractorpayment', 'validateSubcontractorLimit', '', 0, 0, NULL),
(564, 'addSubcontractorPayment', 259, 0, 'subcontractorpayment', 'addSubcontractorPayment', '', 0, 0, NULL),
(565, 'TDS Report', 561, 0, 'Bills', 'tdsreport', '', 2, 1, NULL),
(566, 'qspermission', 98, 0, 'quotation', 'qspermission', '', 0, 0, NULL),
(567, 'quotationpermission', 233, 0, 'Subcontractor', 'quotationpermission', '', 0, 0, NULL),
(568, 'deleteconfirmation', 118, 0, 'Expenses', 'deleteconfirmation', '', 0, 0, NULL),
(569, 'Delete Request', 118, 0, 'Expenses', 'deleterequest', '', 2, 1, NULL),
(570, 'deleterequestaction', 118, 0, 'Expenses', 'deleterequestaction', '', 0, 0, NULL),
(571, 'deleteconfirmation', 151, 0, 'Dailyexpense', 'deleteconfirmation', '', 0, 0, NULL),
(572, 'deleteconfirmation', 208, 0, 'Vendors', 'deleteconfirmation', '', 0, 0, NULL),
(573, 'deleteconfirmation', 259, 0, 'subcontractorpayment', 'deleteconfirmation', '', 0, 0, NULL),
(574, 'Daybook', 561, 0, 'Expenses', 'expensereportbyuser', '', 2, 1, NULL),
(575, 'Daily Expense', 561, 0, 'Dailyexpense', 'dailyexpensebyuser', '', 2, 1, NULL),
(576, 'Bulk Vendor Payment', 561, 0, 'Vendors', 'vendorpaymentsbyuser', '', 2, 1, NULL),
(577, 'Subcontractor Payment By User', 561, 0, 'subcontractorpayment', 'subcontractorpaymentbyuser', '', 2, 1, NULL),
(578, 'Duplicate Entry', 118, 0, 'Expenses', 'duplicateentry', '', 2, 1, NULL),
(579, 'Get Duplicate Entries', 118, 0, 'Expenses', 'getduplicateentries', '', 0, 0, NULL),
(580, 'Duplicate Entry', 151, 0, 'Dailyexpense', 'duplicateentry', '', 0, 0, NULL),
(581, 'Get Duplicate Entries', 151, 0, 'Dailyexpense', 'Getduplicateentries', '', 0, 0, NULL),
(582, 'Duplicate Entry', 208, 0, 'Vendors', 'duplicateentry', '', 0, 0, NULL),
(583, 'Get Duplicate Entries', 208, 0, 'Vendors', 'getduplicateentries', '', 0, 0, NULL),
(584, 'Duplicate Entry', 259, 0, 'subcontractorpayment', 'duplicateentry', '', 0, 0, NULL),
(585, 'Get Duplicate Entries', 259, 0, 'subcontractorpayment', 'getduplicateentries', '', 0, 0, NULL),
(586, 'Edit Request', 118, 0, 'Expenses', 'editrequest', '', 2, 1, NULL),
(587, 'Update Request Action', 118, 0, 'Expenses', 'updaterequestaction', '', 0, 0, NULL),
(588, 'Update Request Action', 487, 0, 'Dashboard', 'updaterequestaction', '', 0, 0, NULL),
(589, 'Delete Request Action', 487, 0, 'Dashboard', 'deleterequestaction', '', 0, 0, NULL),
(590, 'All Update Request Action', 487, 0, 'Dashboard', 'allupdaterequestaction', '', 0, 0, NULL),
(591, 'All Delete Request Action', 487, 0, 'Dashboard', 'alldeleterequestaction', '', 0, 0, NULL),
(592, 'Create Bill', 233, 0, 'Subcontractor', 'createbill', '', 2, 1, NULL),
(593, 'Writeoff', 70, 0, 'Invoice', 'writeoff', '', 2, 1, NULL),
(594, 'Subcontractor Bill', 0, 0, 'Subcontractorbill', '', '', 0, 1, NULL),
(595, 'Index', 594, 0, 'Subcontractorbill', 'index', '', 0, 0, NULL),
(596, 'View', 594, 0, 'Subcontractorbill', 'view', '', 2, 1, NULL),
(597, 'Create', 594, 0, 'Subcontractorbill', 'create', '', 2, 1, NULL),
(598, 'Update', 594, 0, 'Subcontractorbill', 'update', '', 2, 1, NULL),
(599, 'Get Project By Company', 594, 0, 'Subcontractorbill', 'getprojectbycompany', '', 0, 0, NULL),
(600, 'Get Quotations By Project', 594, 0, 'Subcontractorbill', 'getquotationsbyproject', '', 0, 0, NULL),
(601, 'Add Bill Item', 594, 0, 'Subcontractorbill', 'addbillitem', '', 0, 0, NULL),
(602, 'Get New Item View', 594, 0, 'Subcontractorbill', 'getNewItemView', '', 0, 0, NULL),
(603, 'Export To PDF', 594, 0, 'Subcontractorbill', 'exporttopdf', '', 2, 1, NULL),
(604, 'Export To Excel', 594, 0, 'Subcontractorbill', 'exporttoexcel', '', 2, 1, NULL),
(605, 'Get Items By ID', 594, 0, 'Subcontractorbill', 'getItemsById', '', 0, 0, NULL),
(606, 'Remove Bill Item', 594, 0, 'Subcontractorbill', 'removeBillItem', '', 0, 0, NULL),
(607, 'Get Subcontractor By Project', 594, 0, 'Subcontractorbill', 'getsubcontractorbyproject', '', 0, 0, NULL),
(608, 'Listing', 594, 0, 'Subcontractorbill', 'admin', '', 2, 1, NULL),
(609, 'Delete', 594, 0, 'Subcontractorbill', 'delete', '', 0, 0, NULL),
(610, 'Write off', 233, 0, 'Subcontractor', 'writeoff', '', 2, 1, NULL),
(611, 'Check Write Off', 233, 0, 'Subcontractor', 'checkwriteoff', '', 0, 0, NULL),
(612, 'New Project', 561, 0, 'Projects', 'projectexpensereport', '', 2, 1, NULL),
(613, 'Save Expense Report', 288, 0, 'Projects', 'saveexpensereport', '', 0, 0, NULL),
(614, 'Export Project CSV', 288, 0, 'Projects', 'ExportProjectCSV', '', 0, 0, NULL),
(615, 'Purchase Permission Bulk', 3, 0, 'Purchase', 'purchasepermissionbulkaction', '', 2, 1, NULL),
(616, 'Get All Data', 151, 0, 'Dailyexpense', 'getAllData', '', 0, 0, NULL),
(617, 'Reconciliation Report PDF', 326, 0, 'Bank', 'savereconciliationpdf', '', 2, 1, NULL),
(618, 'Reconciliation Report Excel', 326, 0, 'Bank', 'savereconciliationexcel', '', 2, 1, NULL),
(619, 'Get All Data', 259, 0, 'subcontractorpayment', 'getAllData', '', 0, 0, NULL),
(620, 'Remove Specification', 424, 0, 'PurchaseCategory', 'removespecification', '', 2, 1, NULL),
(621, 'Get All Data', 118, 0, 'Expenses', 'getAllData', '', 0, 0, NULL),
(622, 'Get All Data', 208, 0, 'Vendors', 'getAllData', '', 0, 0, NULL),
(623, 'TDS To PDF', 33, 0, 'Bills', 'tdstopdf', '', 2, 1, NULL),
(624, 'TDS To Excel', 33, 0, 'Bills', 'tdstoexcel', '', 2, 1, NULL),
(625, 'Export data', 0, 0, 'exportdata', '', '', 0, 0, NULL),
(626, 'Export Data', 561, 0, 'exportdata', 'index', '', 2, 1, NULL),
(627, 'Export Sales data', 561, 0, 'exportdata', 'savetoexcelsales', '', 2, 1, NULL),
(628, 'Export Contra Data', 561, 0, 'exportdata', 'savetoexcelcontra', '', 2, 1, NULL),
(629, 'Export Payment Data', 561, 0, 'exportdata', 'savetoexcelpayment', '', 2, 1, NULL),
(630, 'Export purchase Data', 561, 0, 'exportdata', 'savetoexcelpurchase', '', 2, 1, NULL),
(631, 'Export Receipt Data', 561, 0, 'exportdata', 'savetoexcelexpense', '', 2, 1, NULL),
(632, 'Export Return Data', 561, 0, 'exportdata', 'savetoexcelreturn', '', 2, 1, NULL),
(633, 'Company Edit', 3, 0, 'purchase', 'companyedit', '', 2, 1, NULL),
(634, 'Notification View', 487, 0, 'Dashboard', 'notificationview', '', 0, 0, NULL),
(635, 'List All Notification', 487, 0, 'dashboard', 'listNotifications', '', 2, 1, NULL),
(636, 'Company Edit Log', 416, 0, 'dashboard', 'companyeditlog', '', 2, 1, NULL),
(637, 'PO Company Edit Log', 416, 0, 'dashboard', 'pocompanyeditlog', '', 0, 0, NULL),
(638, 'Company Edit Approve', 487, 0, 'dashboard', 'companyapprove', '', 0, 0, NULL),
(639, 'updateCashtransfer', 151, 0, 'dailyexpense', 'updateCashtransfer', '', 0, 0, NULL),
(640, 'getbank', 151, 0, 'dailyexpense', 'getbank', '', 0, 0, NULL),
(641, 'Update Sales Data', 70, 0, 'invoice', 'updateinvoicenew', '', 2, 1, NULL),
(642, 'PO Fully Bill Edit', 3, 0, 'purchase', 'deletepurchasebill', '', 2, 1, NULL),
(643, 'deletebill', 3, 0, 'purchase', 'deletebill', '', 0, 0, NULL),
(644, 'getexpensehead', 193, 0, 'DailyReport', 'getexpensehead', '', 0, 0, NULL),
(645, 'getfields', 193, 0, 'DailyReport', 'getfields', '', 0, 0, NULL),
(646, 'Create', 193, 0, 'DailyReport', 'addDailyentry', '', 0, 0, NULL),
(647, 'Listing', 193, 0, 'DailyReport', 'create', '', 2, 1, NULL),
(648, 'getAllData', 193, 0, 'DailyReport', 'getAllData', '', 0, 0, NULL),
(649, 'getDailyReportDetails', 193, 0, 'DailyReport', 'getDailyReportDetails', '', 0, 0, NULL),
(650, 'Update', 193, 0, 'DailyReport', 'updateDailyentry', '', 2, 1, NULL),
(651, 'getDataByDate', 193, 0, 'DailyReport', 'getDataByDate', '', 0, 0, NULL),
(652, 'Delete', 193, 0, 'DailyReport', 'deletereport', '', 2, 1, NULL),
(653, 'deleteconfirmation', 193, 0, 'DailyReport', 'deleteconfirmation', '', 0, 0, NULL),
(654, 'savetopdf', 193, 0, 'DailyReport', 'savetopdf', '', 0, 0, NULL),
(655, 'savetoexcel', 193, 0, 'DailyReport', 'savetoexcel', '', 0, 0, NULL),
(656, 'Weekly Payment Report', 561, 0, 'DailyReport', 'weeklypayment', '', 2, 1, NULL),
(657, 'weeklypdf', 193, 0, 'DailyReport', 'weeklypdf', '', 0, 0, NULL),
(658, 'weeklyexcel', 193, 0, 'DailyReport', 'weeklyexcel', '', 0, 0, NULL),
(659, 'Approve', 193, 0, 'DailyReport', 'approve', '', 2, 1, NULL),
(660, 'Create Quotation by quantity * rate', 233, 0, 'subcontractor', 'addquotationquantity', '', 2, 1, NULL),
(661, 'Edit Quotation by quantity * r', 233, 0, 'subcontractor', 'editquotationquantity', '', 0, 0, NULL),
(662, 'quotationitembyquantity', 233, 0, 'subcontractor', 'quotationitembyquantity', '', 0, 0, NULL),
(663, 'updatesquotationitemquantity', 233, 0, 'subcontractor', 'updatesquotationitemquantity', '', 0, 0, NULL),
(664, 'dynamicexpensehead', 233, 0, 'subcontractor', 'dynamicexpensehead', '', 0, 0, NULL),
(665, 'removequotationitemquantity', 233, 0, 'subcontractor', 'removequotationitemquantity', '', 0, 0, NULL),
(666, 'All', 487, 0, 'dashboard', 'listall', '', 2, 1, NULL),
(667, 'PO Approval', 487, 0, 'dashboard', 'poapproval', '', 2, 1, NULL),
(668, 'SC Quotation', 487, 0, 'dashboard', 'scquotation', '', 2, 1, NULL),
(669, 'Edit Request', 487, 0, 'dashboard', 'editrequest', '', 2, 1, NULL),
(670, 'Delete Request', 487, 0, 'dashboard', 'deleterequest', '', 2, 1, NULL),
(671, 'Notification', 487, 0, 'dashboard', 'notification', '', 2, 1, NULL),
(672, 'View', 233, 0, 'subcontractor', 'viewquotation', '', 2, 1, NULL),
(673, 'View Quotation by quantity * r', 233, 0, 'subcontractor', 'viewquotationquantity', '', 0, 0, NULL),
(674, 'Approve bill item', 33, 0, 'bills', 'approvebills', '', 2, 1, NULL);
INSERT INTO `jp_menu` (`menu_id`, `menu_name`, `parent_id`, `status`, `controller`, `action`, `params`, `showmenu`, `show_list`, `related`) VALUES
(675, 'Delete Purchase Order', 3, 0, 'purchase', 'deletepurchaseorder', '', 2, 1, NULL),
(676, 'Convert To Invoice', 98, 0, 'quotation', 'converttoinvoice', '', 2, 1, NULL),
(677, 'Quotation PDF Download', 233, 0, 'subcontractor', 'pdfdownload', '', 2, 1, NULL),
(678, 'General Settings', 487, 0, 'dashboard', 'generalsettings', '', 2, 1, NULL),
(679, 'Sales Invoice', 0, 0, 'salesInvoice', '', '', 0, 1, NULL),
(680, 'Add Sales Invoice', 679, 0, 'salesInvoice', 'create', '', 2, 1, NULL),
(681, 'Listing', 679, 0, 'salesInvoice', 'admin', '', 2, 1, NULL),
(682, 'Update Invoice', 679, 0, 'salesInvoice', 'update', '', 2, 1, NULL),
(683, 'View Invoice', 679, 0, 'salesInvoice', 'view', '', 2, 1, NULL),
(684, 'Project Payment Report', 561, 0, 'projects', 'paymentreport', '', 2, 1, NULL),
(685, 'Tax Slab', 0, 0, 'taxSlabs', '', '', 0, 1, NULL),
(686, 'Tax Slab Create', 685, 0, 'taxslabs', 'create', '', 2, 1, NULL),
(687, 'Listing', 685, 0, 'taxslabs', 'admin', '', 2, 1, NULL),
(688, 'Update', 685, 0, 'taxslabs', 'update', '', 2, 1, NULL),
(689, 'Buyer', 0, 0, 'buyer/buyers', '', '', 0, 1, NULL),
(690, 'Create Buyer', 689, 0, 'buyer/buyers', 'create', '', 2, 1, NULL),
(691, 'Listing', 689, 0, 'buyer/buyers', 'admin', '', 2, 1, NULL),
(692, 'Update Buyer', 689, 0, 'buyer/buyers', 'update', '', 2, 1, NULL),
(693, 'Transactions', 689, 0, 'buyer/buyers', 'transactions', '', 2, 1, NULL),
(694, 'Add Transaction', 689, 0, 'buyer/buyers', 'addTransaction', '', 2, 1, NULL),
(695, 'Buyer Invoice', 689, 0, 'buyer/buyers', 'buyerinvoice', '', 2, 1, NULL),
(696, 'Create Invoice', 689, 0, 'buyer/buyers', 'createInvoice', '', 2, 1, NULL),
(697, 'View Invoice', 689, 0, 'buyer/buyers', 'viewinvoice', '', 2, 1, NULL),
(698, 'Update Invoice', 689, 0, 'buyer/buyers', 'updateinvoice', '', 2, 1, NULL),
(699, 'Update Transaction', 689, 0, 'buyer/buyers', 'updatetransaction', '', 2, 1, NULL),
(700, 'Delete Transaction', 689, 0, 'buyer/buyers', 'deletetransaction', '', 2, 1, NULL),
(701, 'Buyer Report', 689, 0, 'buyer/buyers', 'buyerreport', '', 2, 1, NULL),
(702, 'Get Unit', 424, 0, 'PurchaseCategory', 'getUnit', '', 0, 1, NULL),
(703, 'Edit Unitconversion', 424, 0, 'PurchaseCategory', 'CheckEditUnitconversion', '', 0, 1, NULL),
(704, 'Warehouse', 0, 0, 'wh/warehouse', '', '', 0, 1, NULL),
(705, 'Warehouse', 704, 0, 'wh/warehouse', 'index', '', 2, 1, NULL),
(706, 'Warehouse Receipt', 0, 0, 'wh/warehousereceipt', '', '', 0, 1, NULL),
(707, 'Warehouse Receipt', 706, 0, 'wh/warehousereceipt', 'index', '', 2, 1, NULL),
(708, 'Warehouse Despatch', 0, 0, 'wh/warehousedespatch', '', '', 0, 1, NULL),
(709, 'Warehouse Despatch', 708, 0, 'wh/warehousedespatch', 'index', '', 2, 1, NULL),
(710, 'Item Estimation', 3, 0, 'purchase', 'Itemestimation', '', 2, 1, NULL),
(711, 'View  Estimation', 3, 0, 'purchase', 'Viewestimation', '', 2, 1, NULL),
(712, 'Estimation', 3, 0, 'purchase', 'estimation', '', 2, 1, NULL),
(713, 'getcategory project', 3, 0, 'purchase', 'getCategoryByProject', '', 2, 1, NULL),
(714, 'getunit category', 3, 0, 'purchase', 'getUnitByCategory', '', 2, 1, NULL),
(715, 'Add estimation', 3, 0, 'purchase', 'Addestimation', '', 2, 1, NULL),
(716, 'Update estimation', 3, 0, 'purchase', 'updateestimation', '', 2, 1, NULL),
(717, 'Save updateestimation', 3, 0, 'purchase', 'SaveUpdateestimation', '', 2, 1, NULL),
(718, 'Delete estimation', 3, 0, 'purchase', 'Deleteestimation', '', 2, 1, NULL),
(719, 'Stock Report', 0, 0, 'report/reports', '', '', 0, 1, NULL),
(720, 'Stock Status Report', 561, 0, 'report/reports', 'stockStatusReport', '', 2, 1, NULL),
(721, 'Stock Value Report', 561, 0, 'report/reports', 'stockValueReport', '', 2, 1, NULL),
(722, 'Transfer Reports', 561, 0, 'report/reports', 'transferreport', '', 2, 1, NULL),
(723, 'Material consumption report', 561, 0, 'projects', 'materialconsumptionreport', '', 2, 1, NULL),
(724, 'getitem', 288, 0, 'projects', 'getitem', '', 0, 0, NULL),
(725, 'Add purchase by length', 3, 0, 'purchase', 'Addpurchasebylength', '', 2, 1, NULL),
(726, 'Add purchase width and height', 3, 0, 'purchase', 'addglasspurchase', '', 2, 1, NULL),
(727, 'createpurchase2', 3, 0, 'purchase', 'createnewpurchase2', '', 0, 0, NULL),
(728, 'searchtransaction2', 3, 0, 'purchase', 'searchtransaction2', '', 0, 0, NULL),
(729, 'createpurchase1', 3, 0, 'purchase', 'createnewpurchase1', '', 0, 0, NULL),
(730, 'searchtransaction1', 3, 0, 'purchase', 'searchtransaction1', '', 0, 0, NULL),
(731, 'getUnitconversionFactor', 3, 0, 'purchase', 'getUnitconversionFactor', '', 0, 0, NULL),
(732, 'purchaseitem2', 3, 0, 'purchase', 'purchaseitem2', '', 0, 0, NULL),
(733, 'Previoustransaction2', 3, 0, 'purchase', 'previoustransaction2', '', 0, 0, NULL),
(734, 'updatepurchaseitem2', 3, 0, 'purchase', 'updatepurchaseitem2', '', 0, 0, NULL),
(735, 'removepurchaseitem2', 3, 0, 'purchase', 'removepurchaseitem2', '', 0, 0, NULL),
(736, 'updatepurchasebylength', 3, 0, 'purchase', 'updatepurchasebylength', '', 0, 0, NULL),
(737, 'purchaseitem1', 3, 0, 'purchase', 'purchaseitem1', '', 0, 0, NULL),
(738, 'previoustransaction1', 3, 0, 'purchase', 'previoustransaction1', '', 0, 0, NULL),
(739, 'purchaseitem1', 3, 0, 'purchase', 'purchaseitem1', '', 0, 0, NULL),
(740, 'previoustransaction1', 3, 0, 'purchase', 'previoustransaction1', '', 0, 0, NULL),
(741, 'updatepurchaseitem1', 3, 0, 'purchase', 'updatepurchaseitem1', '', 0, 0, NULL),
(742, 'removepurchaseitem1', 3, 0, 'purchase', 'removepurchaseitem1', '', 0, 0, NULL),
(743, 'updatepurchaseglass', 3, 0, 'purchase', 'updatepurchaseglass', '', 0, 0, NULL),
(744, 'getRelatedUnit', 33, 0, 'bills', 'getRelatedUnit', '', 0, 0, NULL),
(745, 'getUnitconversionFactor', 33, 0, 'bills', 'getUnitconversionFactor', '', 0, 0, NULL),
(746, 'updateSubcontractorPayment', 259, 0, 'subcontractorpayment', 'updateSubcontractorPayment', '', 0, 0, NULL),
(747, 'changestatus', 326, 0, 'bank', 'changestatus', '', 0, 0, NULL),
(748, 'Pending Bills', 561, 0, 'report/reports', 'pendingbills', '', 2, 1, NULL),
(749, 'Sales Quotation Report', 561, 0, 'salesquotation/salesQuotation', 'report', '', 2, 1, NULL),
(750, 'Quotation Category', 0, 0, 'quotationcategory/quotationCategoryMaster', '', '', 0, 1, NULL),
(751, 'List', 750, 0, 'quotationcategory/quotationCategoryMaster', 'index', '', 2, 1, NULL),
(752, 'Quotation Item', 0, 0, 'quotationitem/quotationItemMaster', '', '', 0, 1, NULL),
(753, 'List', 752, 0, 'quotationitem/quotationItemMaster', 'index', '', 2, 1, NULL),
(754, 'Sales Quotation', 0, 0, 'salesquotation/salesQuotation', '', '', 0, 1, NULL),
(755, 'List', 754, 0, 'salesquotation/salesQuotation', 'index', '', 2, 1, NULL),
(756, 'Quotation Finish', 0, 0, 'finish/quotationFinish', '', '', 0, 1, NULL),
(757, 'List', 756, 0, 'finish/quotationFinish', 'index', '', 2, 1, NULL),
(758, 'Quotation Worktype', 0, 0, 'worktype/quotationWorktype', '', '', 0, 1, NULL),
(759, 'List', 758, 0, 'worktype/quotationWorktype', 'index', '', 2, 1, NULL),
(760, 'Location', 0, 0, 'location/location', '', '', 0, 1, NULL),
(761, 'List', 760, 0, 'location/location', 'index', '', 2, 1, NULL),
(762, 'Sales Executive', 0, 0, 'salesexecutive/salesexecutive', '', '', 0, 1, NULL),
(763, 'List', 762, 0, 'salesexecutive/salesexecutive', 'index', '', 2, 1, NULL),
(764, 'Warehouse Receipt Create', 706, 0, 'wh/warehousereceipt', 'create', '', 2, 1, NULL),
(765, 'Quotation Template', 487, 0, 'dashboard', 'quotationtemplate', '', 2, 1, NULL),
(766, 'Quantity estimation report', 561, 0, 'projects', 'projectsquantityreport', '', 2, 1, NULL),
(767, 'Stock Correction', 704, 0, 'wh/warehouse', 'stockcorrection', '', 2, 1, NULL),
(768, 'Stock Correction Report', 561, 0, 'report/reports', 'stockCorrectionReport', '', 2, 1, NULL),
(769, 'Defect Return', 704, 0, 'wh/warehouse', 'defectreturn', '', 2, 1, NULL),
(770, 'Remove Category', 424, 0, 'PurchaseCategory', 'removecategory', '', 2, 1, NULL),
(771, 'getexpensehead', 288, 0, 'projects', 'getexpensehead', '', 0, 0, NULL),
(772, 'Remove ClientType', 349, 0, 'projectType', 'removeclientype', '', 2, 1, NULL),
(773, 'Remove Client', 324, 0, 'clients', 'removeclient', '', 2, 1, NULL),
(774, 'Remove Company', 416, 0, 'company', 'removecompany', '', 2, 1, NULL),
(775, 'Remove UserRole', 359, 0, 'userRoles', 'removeuserole', '', 2, 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `jp_menu_permissions`
--

CREATE TABLE `jp_menu_permissions` (
  `mp_id` int(20) NOT NULL,
  `user_id` int(20) NOT NULL,
  `menu_id` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jp_menu_permissions`
--

INSERT INTO `jp_menu_permissions` (`mp_id`, `user_id`, `menu_id`) VALUES
(2858, 5, 2),
(2876, 5, 380),
(2877, 5, 381),
(2878, 5, 4),
(2881, 5, 7),
(2882, 5, 8),
(2883, 5, 9),
(2901, 5, 27),
(2907, 5, 514),
(2908, 5, 515),
(2911, 5, 615),
(2912, 5, 633),
(2913, 5, 642),
(2915, 5, 675),
(2944, 5, 34),
(2945, 5, 35),
(2948, 5, 38),
(2949, 5, 39),
(2957, 5, 48),
(2962, 5, 53),
(2964, 5, 55),
(2980, 5, 623),
(2981, 5, 624),
(2982, 5, 674),
(2985, 5, 71),
(2991, 5, 77),
(3002, 5, 88),
(3003, 5, 89),
(3014, 5, 531),
(3017, 5, 534),
(3019, 5, 593),
(3020, 5, 641),
(3021, 5, 99),
(3027, 5, 105),
(3028, 5, 106),
(3029, 5, 107),
(3042, 5, 676),
(3049, 5, 125),
(3069, 5, 149),
(3070, 5, 150),
(3071, 5, 192),
(3076, 5, 569),
(3078, 5, 578),
(3080, 5, 586),
(3084, 5, 153),
(3117, 5, 189),
(3118, 5, 190),
(3119, 5, 191),
(3133, 5, 198),
(3147, 5, 647),
(3150, 5, 650),
(3152, 5, 652),
(3158, 5, 659),
(3159, 5, 209),
(3160, 5, 210),
(3176, 5, 227),
(3177, 5, 228),
(3179, 5, 230),
(3180, 5, 231),
(3181, 5, 232),
(3188, 5, 234),
(3189, 5, 235),
(3190, 5, 236),
(3191, 5, 237),
(3192, 5, 238),
(3197, 5, 243),
(3203, 5, 249),
(3215, 5, 592),
(3216, 5, 610),
(3218, 5, 660),
(3224, 5, 672),
(3226, 5, 677),
(3227, 5, 260),
(3228, 5, 261),
(3229, 5, 262),
(3230, 5, 263),
(3254, 5, 496),
(3264, 5, 289),
(3265, 5, 290),
(3266, 5, 291),
(3295, 5, 536),
(3296, 5, 537),
(3297, 5, 538),
(3298, 5, 539),
(3299, 5, 540),
(3300, 5, 541),
(3304, 5, 325),
(3310, 5, 333),
(3311, 5, 334),
(3312, 5, 542),
(3313, 5, 327),
(3320, 5, 341),
(3325, 5, 347),
(3326, 5, 348),
(3329, 5, 617),
(3330, 5, 618),
(3332, 5, 351),
(3338, 5, 357),
(3339, 5, 358),
(3342, 5, 361),
(3343, 5, 362),
(3345, 5, 383),
(3354, 5, 392),
(3355, 5, 393),
(3357, 5, 396),
(3358, 5, 397),
(3359, 5, 398),
(3361, 5, 401),
(3363, 5, 403),
(3364, 5, 404),
(3373, 5, 415),
(3374, 5, 417),
(3376, 5, 419),
(3377, 5, 420),
(3381, 5, 636),
(3383, 5, 425),
(3392, 5, 434),
(3393, 5, 435),
(3395, 5, 437),
(3398, 5, 497),
(3399, 5, 620),
(3402, 5, 441),
(3410, 5, 450),
(3413, 5, 453),
(3414, 5, 454),
(3430, 5, 471),
(3431, 5, 472),
(3432, 5, 473),
(3440, 5, 482),
(3441, 5, 483),
(3442, 5, 484),
(3452, 5, 635),
(3454, 5, 666),
(3455, 5, 667),
(3456, 5, 668),
(3457, 5, 669),
(3458, 5, 670),
(3459, 5, 671),
(3460, 5, 678),
(3461, 5, 491),
(3463, 5, 501),
(3465, 5, 503),
(3466, 5, 504),
(3474, 5, 512),
(3478, 5, 546),
(3480, 5, 548),
(3481, 5, 549),
(3482, 5, 550),
(3486, 5, 47),
(3487, 5, 67),
(3488, 5, 132),
(3489, 5, 135),
(3491, 5, 144),
(3492, 5, 174),
(3493, 5, 179),
(3494, 5, 184),
(3495, 5, 216),
(3496, 5, 273),
(3497, 5, 294),
(3498, 5, 317),
(3499, 5, 319),
(3500, 5, 320),
(3501, 5, 343),
(3502, 5, 455),
(3503, 5, 460),
(3504, 5, 463),
(3505, 5, 565),
(3506, 5, 574),
(3507, 5, 575),
(3508, 5, 576),
(3509, 5, 577),
(3510, 5, 612),
(3511, 5, 626),
(3512, 5, 627),
(3513, 5, 628),
(3514, 5, 629),
(3515, 5, 630),
(3516, 5, 631),
(3517, 5, 632),
(3518, 5, 656),
(3519, 5, 684),
(3525, 5, 596),
(3526, 5, 597),
(3527, 5, 598),
(3532, 5, 603),
(3533, 5, 604),
(3537, 5, 608),
(3543, 5, 686),
(3544, 5, 687),
(3545, 5, 688),
(3567, 25, 674),
(3568, 25, 38),
(3569, 25, 39),
(3570, 25, 35),
(3571, 25, 48),
(3572, 25, 53),
(3573, 25, 34),
(3574, 25, 624),
(3575, 25, 623),
(3576, 25, 55),
(3665, 25, 633),
(3666, 25, 7),
(3668, 25, 675),
(3671, 25, 714),
(3672, 25, 710),
(3673, 25, 4),
(3674, 25, 514),
(3675, 25, 642),
(3676, 25, 27),
(3677, 25, 615),
(3678, 25, 717),
(3679, 25, 515),
(3680, 25, 8),
(3681, 25, 716),
(3682, 25, 9),
(3683, 25, 711),
(3684, 25, 437),
(3685, 25, 434),
(3686, 25, 435),
(3687, 25, 703),
(3688, 25, 702),
(3689, 25, 425),
(3690, 25, 497),
(3691, 25, 620),
(3692, 25, 503),
(3693, 25, 512),
(3694, 25, 504),
(3695, 25, 501),
(3719, 25, 723),
(3774, 25, 227),
(3775, 25, 209),
(3776, 25, 228),
(3777, 25, 210),
(3778, 25, 230),
(3779, 25, 232),
(3780, 25, 231),
(3781, 25, 705),
(3782, 25, 709),
(3783, 25, 707),
(4013, 25, 748),
(4015, 27, 437),
(4016, 27, 434),
(4017, 27, 435),
(4018, 27, 425),
(4019, 27, 497),
(4020, 27, 620),
(4021, 27, 227),
(4022, 27, 209),
(4023, 27, 228),
(4024, 27, 210),
(4025, 27, 230),
(4026, 27, 231),
(4027, 27, 232),
(4029, 27, 674),
(4030, 27, 38),
(4031, 27, 39),
(4032, 27, 35),
(4033, 27, 48),
(4034, 27, 53),
(4035, 27, 34),
(4036, 27, 624),
(4037, 27, 623),
(4038, 27, 55),
(4039, 27, 633),
(4040, 27, 7),
(4041, 27, 675),
(4042, 27, 4),
(4043, 27, 514),
(4044, 27, 642),
(4045, 27, 27),
(4046, 27, 615),
(4047, 27, 515),
(4048, 27, 8),
(4049, 27, 9),
(4050, 27, 503),
(4051, 27, 512),
(4052, 27, 504),
(4053, 27, 501),
(4054, 25, 713),
(4055, 25, 712),
(4056, 28, 341),
(4057, 28, 347),
(4058, 28, 327),
(4059, 28, 618),
(4060, 28, 617),
(4061, 28, 348),
(4062, 28, 674),
(4063, 28, 38),
(4064, 28, 39),
(4065, 28, 35),
(4066, 28, 48),
(4067, 28, 53),
(4068, 28, 34),
(4069, 28, 624),
(4070, 28, 623),
(4071, 28, 55),
(4072, 28, 453),
(4073, 28, 450),
(4074, 28, 454),
(4075, 28, 357),
(4076, 28, 351),
(4077, 28, 358),
(4078, 28, 542),
(4079, 28, 333),
(4080, 28, 325),
(4081, 28, 334),
(4082, 28, 636),
(4083, 28, 419),
(4084, 28, 417),
(4085, 28, 420),
(4086, 28, 191),
(4087, 28, 190),
(4088, 28, 189),
(4089, 28, 153),
(4090, 28, 397),
(4091, 28, 396),
(4092, 28, 398),
(4093, 28, 192),
(4094, 28, 150),
(4095, 28, 569),
(4096, 28, 578),
(4097, 28, 149),
(4098, 28, 586),
(4099, 28, 125),
(4100, 28, 472),
(4101, 28, 471),
(4102, 28, 473),
(4103, 28, 392),
(4104, 28, 383),
(4105, 28, 393),
(4106, 28, 666),
(4107, 28, 670),
(4108, 28, 669),
(4109, 28, 678),
(4110, 28, 635),
(4111, 28, 671),
(4112, 28, 667),
(4113, 28, 668),
(4114, 28, 531),
(4115, 28, 88),
(4116, 28, 71),
(4117, 28, 89),
(4118, 28, 534),
(4119, 28, 641),
(4120, 28, 77),
(4121, 28, 593),
(4122, 28, 659),
(4123, 28, 198),
(4124, 28, 652),
(4125, 28, 647),
(4126, 28, 650),
(4127, 28, 415),
(4128, 28, 491),
(4129, 28, 548),
(4130, 28, 549),
(4131, 28, 546),
(4132, 28, 550),
(4133, 28, 483),
(4134, 28, 482),
(4135, 28, 484),
(4136, 28, 536),
(4137, 28, 290),
(4138, 28, 538),
(4139, 28, 289),
(4140, 28, 541),
(4141, 28, 540),
(4142, 28, 291),
(4143, 28, 537),
(4144, 28, 539),
(4145, 28, 715),
(4146, 28, 725),
(4147, 28, 726),
(4148, 28, 633),
(4149, 28, 7),
(4150, 28, 718),
(4151, 28, 675),
(4152, 28, 712),
(4153, 28, 713),
(4154, 28, 714),
(4155, 28, 710),
(4156, 28, 4),
(4157, 28, 514),
(4158, 28, 642),
(4159, 28, 27),
(4160, 28, 615),
(4161, 28, 717),
(4162, 28, 515),
(4163, 28, 8),
(4164, 28, 716),
(4165, 28, 9),
(4166, 28, 711),
(4167, 28, 437),
(4168, 28, 434),
(4169, 28, 435),
(4170, 28, 703),
(4171, 28, 702),
(4172, 28, 425),
(4173, 28, 497),
(4174, 28, 620),
(4175, 28, 503),
(4176, 28, 512),
(4177, 28, 504),
(4178, 28, 501),
(4179, 28, 676),
(4180, 28, 105),
(4181, 28, 99),
(4182, 28, 106),
(4183, 28, 107),
(4184, 28, 343),
(4185, 28, 576),
(4186, 28, 455),
(4187, 28, 319),
(4188, 28, 575),
(4189, 28, 174),
(4190, 28, 460),
(4191, 28, 463),
(4192, 28, 135),
(4193, 28, 574),
(4194, 28, 628),
(4195, 28, 626),
(4196, 28, 629),
(4197, 28, 630),
(4198, 28, 631),
(4199, 28, 632),
(4200, 28, 627),
(4201, 28, 317),
(4202, 28, 723),
(4203, 28, 612),
(4204, 28, 144),
(4205, 28, 748),
(4206, 28, 179),
(4207, 28, 684),
(4208, 28, 294),
(4209, 28, 67),
(4210, 28, 132),
(4211, 28, 184),
(4212, 28, 720),
(4213, 28, 721),
(4214, 28, 273),
(4215, 28, 577),
(4216, 28, 47),
(4217, 28, 565),
(4218, 28, 722),
(4219, 28, 216),
(4220, 28, 656),
(4221, 28, 320),
(4222, 28, 680),
(4223, 28, 681),
(4224, 28, 682),
(4225, 28, 683),
(4226, 28, 261),
(4227, 28, 263),
(4228, 28, 262),
(4229, 28, 260),
(4230, 28, 496),
(4231, 28, 235),
(4232, 28, 592),
(4233, 28, 243),
(4234, 28, 660),
(4235, 28, 236),
(4236, 28, 249),
(4237, 28, 234),
(4238, 28, 237),
(4239, 28, 677),
(4240, 28, 238),
(4241, 28, 672),
(4242, 28, 610),
(4243, 28, 597),
(4244, 28, 604),
(4245, 28, 603),
(4246, 28, 608),
(4247, 28, 598),
(4248, 28, 596),
(4249, 28, 687),
(4250, 28, 686),
(4251, 28, 688),
(4252, 28, 441),
(4253, 28, 380),
(4254, 28, 2),
(4255, 28, 381),
(4256, 28, 362),
(4257, 28, 361),
(4258, 28, 227),
(4259, 28, 209),
(4260, 28, 228),
(4261, 28, 210),
(4262, 28, 230),
(4263, 28, 232),
(4264, 28, 231),
(4265, 28, 705),
(4266, 28, 709),
(4267, 28, 707),
(4268, 28, 403),
(4269, 28, 401),
(4270, 28, 404),
(4271, 5, 705),
(4272, 5, 709),
(4273, 5, 707),
(4274, 5, 761),
(4275, 5, 751),
(4276, 5, 757),
(4277, 5, 759),
(4278, 5, 763),
(4279, 5, 755),
(4280, 5, 753),
(4281, 22, 676),
(4282, 22, 105),
(4283, 22, 99),
(4284, 22, 106),
(4285, 22, 107),
(4286, 22, 751),
(4287, 22, 757),
(4288, 22, 753),
(4289, 22, 759),
(4290, 26, 341),
(4291, 26, 347),
(4292, 26, 327),
(4293, 26, 618),
(4294, 26, 617),
(4295, 26, 348),
(4296, 26, 674),
(4297, 26, 38),
(4298, 26, 39),
(4299, 26, 35),
(4300, 26, 48),
(4301, 26, 53),
(4302, 26, 34),
(4303, 26, 624),
(4304, 26, 623),
(4305, 26, 55),
(4306, 26, 453),
(4307, 26, 450),
(4308, 26, 454),
(4309, 26, 357),
(4310, 26, 351),
(4311, 26, 358),
(4312, 26, 542),
(4313, 26, 333),
(4314, 26, 325),
(4315, 26, 334),
(4316, 26, 636),
(4317, 26, 419),
(4318, 26, 417),
(4319, 26, 420),
(4320, 26, 191),
(4321, 26, 190),
(4322, 26, 189),
(4323, 26, 153),
(4324, 26, 397),
(4325, 26, 396),
(4326, 26, 398),
(4327, 26, 192),
(4328, 26, 150),
(4329, 26, 569),
(4330, 26, 578),
(4331, 26, 149),
(4332, 26, 586),
(4333, 26, 125),
(4334, 26, 472),
(4335, 26, 471),
(4336, 26, 473),
(4337, 26, 392),
(4338, 26, 383),
(4339, 26, 393),
(4340, 26, 666),
(4341, 26, 670),
(4342, 26, 669),
(4343, 26, 678),
(4344, 26, 635),
(4345, 26, 671),
(4346, 26, 667),
(4347, 26, 668),
(4348, 26, 531),
(4349, 26, 88),
(4350, 26, 71),
(4351, 26, 89),
(4352, 26, 534),
(4353, 26, 641),
(4354, 26, 77),
(4355, 26, 593),
(4356, 26, 659),
(4357, 26, 198),
(4358, 26, 652),
(4359, 26, 647),
(4360, 26, 650),
(4361, 26, 761),
(4362, 26, 415),
(4363, 26, 491),
(4364, 26, 548),
(4365, 26, 549),
(4366, 26, 546),
(4367, 26, 550),
(4368, 26, 483),
(4369, 26, 482),
(4370, 26, 484),
(4371, 26, 536),
(4372, 26, 290),
(4373, 26, 538),
(4374, 26, 289),
(4375, 26, 541),
(4376, 26, 540),
(4377, 26, 291),
(4378, 26, 537),
(4379, 26, 539),
(4380, 26, 715),
(4381, 26, 725),
(4382, 26, 726),
(4383, 26, 633),
(4384, 26, 7),
(4385, 26, 718),
(4386, 26, 675),
(4387, 26, 712),
(4388, 26, 713),
(4389, 26, 714),
(4390, 26, 710),
(4391, 26, 4),
(4392, 26, 514),
(4393, 26, 642),
(4394, 26, 27),
(4395, 26, 615),
(4396, 26, 717),
(4397, 26, 515),
(4398, 26, 8),
(4399, 26, 716),
(4400, 26, 9),
(4401, 26, 711),
(4402, 26, 437),
(4403, 26, 434),
(4404, 26, 435),
(4405, 26, 703),
(4406, 26, 702),
(4407, 26, 425),
(4408, 26, 497),
(4409, 26, 620),
(4410, 26, 503),
(4411, 26, 512),
(4412, 26, 504),
(4413, 26, 501),
(4414, 26, 676),
(4415, 26, 105),
(4416, 26, 99),
(4417, 26, 106),
(4418, 26, 107),
(4419, 26, 751),
(4420, 26, 757),
(4421, 26, 753),
(4422, 26, 759),
(4423, 26, 343),
(4424, 26, 576),
(4425, 26, 455),
(4426, 26, 319),
(4427, 26, 575),
(4428, 26, 174),
(4429, 26, 460),
(4430, 26, 463),
(4431, 26, 135),
(4432, 26, 574),
(4433, 26, 628),
(4434, 26, 626),
(4435, 26, 629),
(4436, 26, 630),
(4437, 26, 631),
(4438, 26, 632),
(4439, 26, 627),
(4440, 26, 317),
(4441, 26, 723),
(4442, 26, 612),
(4443, 26, 144),
(4444, 26, 748),
(4445, 26, 179),
(4446, 26, 684),
(4447, 26, 294),
(4448, 26, 67),
(4449, 26, 132),
(4450, 26, 184),
(4451, 26, 749),
(4452, 26, 720),
(4453, 26, 721),
(4454, 26, 273),
(4455, 26, 577),
(4456, 26, 47),
(4457, 26, 565),
(4458, 26, 722),
(4459, 26, 216),
(4460, 26, 656),
(4461, 26, 320),
(4462, 26, 763),
(4463, 26, 680),
(4464, 26, 681),
(4465, 26, 682),
(4466, 26, 683),
(4467, 26, 755),
(4468, 26, 261),
(4469, 26, 263),
(4470, 26, 262),
(4471, 26, 260),
(4472, 26, 496),
(4473, 26, 235),
(4474, 26, 592),
(4475, 26, 243),
(4476, 26, 660),
(4477, 26, 236),
(4478, 26, 249),
(4479, 26, 234),
(4480, 26, 237),
(4481, 26, 677),
(4482, 26, 238),
(4483, 26, 672),
(4484, 26, 610),
(4485, 26, 597),
(4486, 26, 604),
(4487, 26, 603),
(4488, 26, 608),
(4489, 26, 598),
(4490, 26, 596),
(4491, 26, 687),
(4492, 26, 686),
(4493, 26, 688),
(4494, 26, 441),
(4495, 26, 380),
(4496, 26, 2),
(4497, 26, 381),
(4498, 26, 362),
(4499, 26, 361),
(4500, 26, 227),
(4501, 26, 209),
(4502, 26, 228),
(4503, 26, 210),
(4504, 26, 230),
(4505, 26, 232),
(4506, 26, 231),
(4507, 26, 705),
(4508, 26, 709),
(4509, 26, 707),
(4510, 26, 403),
(4511, 26, 401),
(4512, 26, 404),
(4513, 23, 712),
(4514, 23, 676),
(4515, 23, 105),
(4516, 23, 99),
(4517, 23, 106),
(4518, 23, 107),
(4519, 23, 751),
(4520, 23, 757),
(4521, 23, 753),
(4522, 23, 759),
(4523, 23, 680),
(4524, 23, 681),
(4525, 23, 682),
(4526, 23, 683),
(4527, 23, 243),
(4528, 23, 660),
(4529, 23, 249),
(4530, 23, 237),
(4531, 23, 677),
(4532, 23, 238),
(4533, 23, 755),
(4534, 23, 441),
(4535, 27, 341),
(4536, 27, 347),
(4537, 27, 327),
(4538, 27, 618),
(4539, 27, 617),
(4540, 27, 348),
(4541, 27, 453),
(4542, 27, 450),
(4543, 27, 454),
(4544, 27, 191),
(4545, 27, 190),
(4546, 27, 189),
(4547, 27, 153),
(4548, 27, 397),
(4549, 27, 396),
(4550, 27, 398),
(4551, 27, 192),
(4552, 27, 150),
(4553, 27, 569),
(4554, 27, 578),
(4555, 27, 149),
(4556, 27, 586),
(4557, 27, 125),
(4558, 27, 472),
(4559, 27, 471),
(4560, 27, 473),
(4561, 27, 392),
(4562, 27, 383),
(4563, 27, 393),
(4564, 27, 531),
(4565, 27, 88),
(4566, 27, 71),
(4567, 27, 89),
(4568, 27, 534),
(4569, 27, 641),
(4570, 27, 77),
(4571, 27, 593),
(4572, 27, 659),
(4573, 27, 198),
(4574, 27, 652),
(4575, 27, 647),
(4576, 27, 650),
(4577, 27, 548),
(4578, 27, 549),
(4579, 27, 546),
(4580, 27, 550),
(4581, 27, 483),
(4582, 27, 482),
(4583, 27, 484),
(4584, 27, 536),
(4585, 27, 290),
(4586, 27, 538),
(4587, 27, 289),
(4588, 27, 541),
(4589, 27, 540),
(4590, 27, 291),
(4591, 27, 537),
(4592, 27, 539),
(4593, 27, 715),
(4594, 27, 725),
(4595, 27, 726),
(4596, 27, 718),
(4597, 27, 712),
(4598, 27, 713),
(4599, 27, 714),
(4600, 27, 710),
(4601, 27, 717),
(4602, 27, 716),
(4603, 27, 711),
(4604, 27, 680),
(4605, 27, 681),
(4606, 27, 682),
(4607, 27, 683),
(4608, 27, 261),
(4609, 27, 263),
(4610, 27, 262),
(4611, 27, 260),
(4612, 27, 496),
(4613, 27, 235),
(4614, 27, 592),
(4615, 27, 243),
(4616, 27, 660),
(4617, 27, 236),
(4618, 27, 249),
(4619, 27, 234),
(4620, 27, 237),
(4621, 27, 677),
(4622, 27, 238),
(4623, 27, 672),
(4624, 27, 610),
(4625, 27, 597),
(4626, 27, 604),
(4627, 27, 603),
(4628, 27, 608),
(4629, 27, 598),
(4630, 27, 596),
(4631, 27, 687),
(4632, 27, 686),
(4633, 27, 688),
(4634, 27, 705),
(4635, 27, 709),
(4636, 27, 707),
(4637, 27, 764),
(4638, 23, 357),
(4639, 23, 351),
(4640, 23, 358),
(4641, 22, 357),
(4642, 22, 351),
(4643, 22, 358),
(4644, 23, 542),
(4645, 23, 333),
(4646, 23, 325),
(4647, 23, 334),
(4648, 23, 531),
(4649, 23, 88),
(4650, 23, 71),
(4651, 23, 89),
(4652, 23, 534),
(4653, 23, 641),
(4654, 23, 77),
(4655, 23, 593),
(4656, 23, 659),
(4657, 23, 198),
(4658, 23, 652),
(4659, 23, 647),
(4660, 23, 650),
(4661, 23, 536),
(4662, 23, 290),
(4663, 23, 538),
(4664, 23, 289),
(4665, 23, 541),
(4666, 23, 540),
(4667, 23, 291),
(4668, 23, 537),
(4669, 23, 539),
(4670, 23, 261),
(4671, 23, 263),
(4672, 23, 262),
(4673, 23, 260),
(4674, 23, 496),
(4675, 23, 235),
(4676, 23, 592),
(4677, 23, 236),
(4678, 23, 234),
(4679, 23, 672),
(4680, 23, 610),
(4681, 23, 597),
(4682, 23, 604),
(4683, 23, 603),
(4684, 23, 608),
(4685, 23, 598),
(4686, 23, 596),
(4687, 23, 687),
(4688, 23, 686),
(4689, 23, 688),
(4690, 23, 227),
(4691, 23, 209),
(4692, 23, 228),
(4693, 23, 210),
(4694, 23, 230),
(4695, 23, 232),
(4696, 23, 231),
(4697, 23, 715),
(4698, 23, 725),
(4699, 23, 726),
(4700, 23, 633),
(4701, 23, 7),
(4702, 23, 718),
(4703, 23, 675),
(4704, 23, 713),
(4705, 23, 714),
(4706, 23, 710),
(4707, 23, 4),
(4708, 23, 514),
(4709, 23, 642),
(4710, 23, 27),
(4711, 23, 615),
(4712, 23, 717),
(4713, 23, 515),
(4714, 23, 8),
(4715, 23, 716),
(4716, 23, 9),
(4717, 23, 711),
(4718, 23, 437),
(4719, 23, 434),
(4720, 23, 435),
(4721, 23, 703),
(4722, 23, 702),
(4723, 23, 425),
(4724, 23, 497),
(4725, 23, 620),
(4726, 27, 357),
(4727, 27, 351),
(4728, 27, 358),
(4729, 27, 542),
(4730, 27, 333),
(4731, 27, 325),
(4732, 27, 334),
(4733, 23, 761),
(4734, 23, 341),
(4735, 23, 347),
(4736, 23, 327),
(4737, 23, 618),
(4738, 23, 617),
(4739, 23, 348),
(4740, 23, 674),
(4741, 23, 38),
(4742, 23, 39),
(4743, 23, 35),
(4744, 23, 48),
(4745, 23, 53),
(4746, 23, 34),
(4747, 23, 624),
(4748, 23, 623),
(4749, 23, 55),
(4750, 23, 453),
(4751, 23, 450),
(4752, 23, 454),
(4753, 23, 636),
(4754, 23, 419),
(4755, 23, 417),
(4756, 23, 420),
(4757, 23, 191),
(4758, 23, 190),
(4759, 23, 189),
(4760, 23, 153),
(4761, 23, 397),
(4762, 23, 396),
(4763, 23, 398),
(4764, 23, 192),
(4765, 23, 150),
(4766, 23, 569),
(4767, 23, 578),
(4768, 23, 149),
(4769, 23, 586),
(4770, 23, 125),
(4771, 23, 472),
(4772, 23, 471),
(4773, 23, 473),
(4774, 23, 392),
(4775, 23, 383),
(4776, 23, 393),
(4777, 23, 666),
(4778, 23, 670),
(4779, 23, 669),
(4780, 23, 678),
(4781, 23, 635),
(4782, 23, 671),
(4783, 23, 667),
(4784, 23, 765),
(4785, 23, 668),
(4786, 23, 415),
(4787, 23, 491),
(4788, 23, 548),
(4789, 23, 549),
(4790, 23, 546),
(4791, 23, 550),
(4792, 23, 503),
(4793, 23, 512),
(4794, 23, 504),
(4795, 23, 501),
(4796, 23, 343),
(4797, 23, 576),
(4798, 23, 455),
(4799, 23, 319),
(4800, 23, 575),
(4801, 23, 174),
(4802, 23, 460),
(4803, 23, 463),
(4804, 23, 135),
(4805, 23, 574),
(4806, 23, 628),
(4807, 23, 626),
(4808, 23, 629),
(4809, 23, 630),
(4810, 23, 631),
(4811, 23, 632),
(4812, 23, 627),
(4813, 23, 317),
(4814, 23, 723),
(4815, 23, 612),
(4816, 23, 144),
(4817, 23, 748),
(4818, 23, 179),
(4819, 23, 684),
(4820, 23, 294),
(4821, 23, 67),
(4822, 23, 766),
(4823, 23, 132),
(4824, 23, 184),
(4825, 23, 749),
(4826, 23, 720),
(4827, 23, 721),
(4828, 23, 273),
(4829, 23, 577),
(4830, 23, 47),
(4831, 23, 565),
(4832, 23, 722),
(4833, 23, 216),
(4834, 23, 656),
(4835, 23, 320),
(4836, 23, 763),
(4837, 23, 380),
(4838, 23, 2),
(4839, 23, 381),
(4840, 23, 362),
(4841, 23, 361),
(4842, 23, 705),
(4843, 23, 709),
(4844, 23, 707),
(4845, 23, 764),
(4846, 23, 403),
(4847, 23, 401),
(4848, 23, 404),
(4849, 27, 343),
(4850, 27, 576),
(4851, 27, 455),
(4852, 27, 319),
(4853, 27, 575),
(4854, 27, 174),
(4855, 27, 460),
(4856, 27, 463),
(4857, 27, 135),
(4858, 27, 574),
(4859, 27, 628),
(4860, 27, 626),
(4861, 27, 629),
(4862, 27, 630),
(4863, 27, 631),
(4864, 27, 632),
(4865, 27, 627),
(4866, 27, 317),
(4867, 27, 723),
(4868, 27, 612),
(4869, 27, 144),
(4870, 27, 748),
(4871, 27, 179),
(4872, 27, 684),
(4873, 27, 294),
(4874, 27, 67),
(4875, 27, 766),
(4876, 27, 132),
(4877, 27, 184),
(4878, 27, 749),
(4879, 27, 720),
(4880, 27, 721),
(4881, 27, 273),
(4882, 27, 577),
(4883, 27, 47),
(4884, 27, 565),
(4885, 27, 722),
(4886, 27, 216),
(4887, 27, 656),
(4888, 27, 320),
(4889, 26, 765),
(4890, 27, 380),
(4891, 27, 2),
(4892, 27, 381),
(4893, 27, 362),
(4894, 27, 361),
(4895, 22, 755),
(4896, 5, 749),
(4897, 5, 768),
(4898, 5, 720),
(4899, 5, 721),
(4900, 5, 765),
(4901, 5, 715),
(4902, 5, 725),
(4903, 5, 726),
(4904, 5, 718),
(4905, 5, 712),
(4906, 5, 713),
(4907, 5, 714),
(4908, 5, 710),
(4909, 5, 717),
(4910, 5, 716),
(4911, 5, 711),
(4912, 5, 703),
(4913, 5, 702),
(4914, 5, 723),
(4915, 5, 748),
(4916, 5, 766),
(4917, 5, 722),
(4918, 5, 680),
(4919, 5, 681),
(4920, 5, 682),
(4921, 5, 683),
(4922, 5, 769),
(4923, 5, 767),
(4924, 5, 764);

-- --------------------------------------------------------

--
-- Table structure for table `jp_notifications`
--

CREATE TABLE `jp_notifications` (
  `id` int(11) NOT NULL,
  `action` varchar(100) NOT NULL,
  `message` varchar(500) DEFAULT NULL,
  `parent_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `requested_by` int(11) DEFAULT NULL,
  `approved_by` int(11) DEFAULT NULL,
  `view_status` tinyint(4) DEFAULT '0' COMMENT '0->not_viewed,1->viewed'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jp_payment_reminders`
--

CREATE TABLE `jp_payment_reminders` (
  `id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `project_id` int(11) DEFAULT NULL,
  `amount` float NOT NULL,
  `date` date NOT NULL,
  `status` enum('Open','Extended','Completed') NOT NULL DEFAULT 'Open',
  `description` text NOT NULL,
  `created_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jp_performa_invoice`
--

CREATE TABLE `jp_performa_invoice` (
  `id` int(11) NOT NULL,
  `client_name` varchar(25) NOT NULL,
  `date` date NOT NULL,
  `inv_no` varchar(10) NOT NULL,
  `project_name` varchar(30) NOT NULL,
  `subtotal` float(20,2) NOT NULL,
  `fees` float(16,2) NOT NULL,
  `amount` float(20,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jp_po_company`
--

CREATE TABLE `jp_po_company` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `address` text NOT NULL,
  `pincode` varchar(100) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `email_id` varchar(50) NOT NULL,
  `company_gstnum` varchar(50) NOT NULL,
  `company_id` int(11) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jp_pre_dailyexpenses`
--

CREATE TABLE `jp_pre_dailyexpenses` (
  `dailyexp_id` int(11) NOT NULL,
  `ref_id` int(11) NOT NULL,
  `followup_id` int(11) DEFAULT NULL,
  `record_grop_id` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `date` date NOT NULL,
  `exp_type_id` int(11) DEFAULT NULL COMMENT 'company expense type',
  `bill_id` varchar(100) DEFAULT NULL,
  `expensehead_id` int(11) DEFAULT NULL,
  `expense_type` int(11) DEFAULT NULL,
  `vendor_id` int(11) DEFAULT NULL,
  `dailyexpense_amount` float DEFAULT NULL,
  `dailyexpense_sgstp` float DEFAULT NULL,
  `dailyexpense_sgst` float DEFAULT NULL,
  `dailyexpense_cgstp` float DEFAULT NULL,
  `dailyexpense_cgst` float DEFAULT NULL,
  `dailyexpense_igstp` float DEFAULT NULL,
  `dailyexpense_igst` float DEFAULT NULL,
  `amount` float NOT NULL,
  `description` varchar(300) NOT NULL,
  `dailyexpense_receipt_type` int(11) DEFAULT NULL,
  `dailyexpense_receipt_head` int(11) DEFAULT NULL,
  `dailyexpense_receipt` float DEFAULT NULL,
  `dailyexpense_purchase_type` int(11) DEFAULT NULL,
  `dailyexpense_paidamount` float DEFAULT NULL,
  `bank_id` int(11) DEFAULT NULL,
  `dailyexpense_chequeno` varchar(100) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `data_entry` varchar(30) DEFAULT NULL,
  `exp_type` int(4) NOT NULL,
  `dailyexpense_type` enum('expense','deposit','receipt') NOT NULL DEFAULT 'expense',
  `company_id` int(11) DEFAULT NULL,
  `reconciliation_status` int(11) DEFAULT NULL,
  `reconciliation_date` date DEFAULT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `display_flg` enum('Yes','No') NOT NULL DEFAULT 'Yes',
  `transaction_parent` int(11) DEFAULT NULL,
  `parent_status` enum('0','1') NOT NULL DEFAULT '1',
  `expensehead_type` int(11) DEFAULT NULL,
  `transfer_parentid` int(11) DEFAULT NULL,
  `update_status` int(11) DEFAULT '0',
  `delete_status` int(11) DEFAULT '0',
  `approval_status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '0=>Pending,1=>Approved,2=>Rejected	',
  `approved_by` int(11) NOT NULL,
  `record_action` varchar(300) NOT NULL,
  `approve_notify_status` enum('0','1') NOT NULL DEFAULT '1' COMMENT '0=>no.not visible to admin,1=>yes visible to admin',
  `cancelled_by` int(11) DEFAULT NULL,
  `cancelled_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jp_pre_dailyreport`
--

CREATE TABLE `jp_pre_dailyreport` (
  `dr_id` int(11) NOT NULL,
  `ref_id` int(11) NOT NULL,
  `followup_id` int(11) DEFAULT NULL,
  `record_grop_id` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `projectid` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `subcontractor_id` int(11) DEFAULT NULL,
  `expensehead_id` int(11) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `works_done` text NOT NULL,
  `materials_unlade` text NOT NULL,
  `wrktype_and_numbers` longtext,
  `extra_work_done` text NOT NULL,
  `amount` float(20,2) NOT NULL,
  `labour` float(20,2) DEFAULT NULL,
  `wage` float(20,2) DEFAULT NULL,
  `wage_rate` float(20,2) DEFAULT NULL,
  `helper` float(20,2) DEFAULT NULL,
  `helper_labour` float(20,2) DEFAULT NULL,
  `lump_sum` float(20,2) DEFAULT NULL,
  `approve_status` enum('1','2') DEFAULT NULL COMMENT '1=>approve, 2=>not approve',
  `description` text,
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `labour_wage` float(20,2) NOT NULL,
  `helper_wage` float(20,2) NOT NULL,
  `approval_status` enum('0','1','2') DEFAULT '0' COMMENT '0=>Pending,1=>Approved,2=>Rejected',
  `approved_by` int(1) NOT NULL,
  `record_action` varchar(300) NOT NULL,
  `approve_notify_status` enum('0','1') NOT NULL DEFAULT '1' COMMENT '0=>no.not visible to admin,1=>yes visible to admin',
  `cancelled_by` int(11) DEFAULT NULL,
  `cancelled_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jp_pre_dailyvendors`
--

CREATE TABLE `jp_pre_dailyvendors` (
  `daily_v_id` int(11) NOT NULL,
  `ref_id` int(11) NOT NULL,
  `followup_id` int(11) DEFAULT NULL,
  `record_grop_id` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `vendor_id` int(11) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `amount` float(15,2) NOT NULL,
  `description` text NOT NULL,
  `date` date NOT NULL,
  `payment_type` smallint(6) DEFAULT NULL,
  `bank` int(11) DEFAULT NULL,
  `cheque_no` varchar(100) DEFAULT NULL,
  `sgst` float DEFAULT NULL,
  `sgst_amount` float DEFAULT NULL,
  `cgst` float DEFAULT NULL,
  `cgst_amount` float DEFAULT NULL,
  `igst` float DEFAULT NULL,
  `igst_amount` float DEFAULT NULL,
  `tds` float DEFAULT NULL,
  `tds_amount` double DEFAULT NULL,
  `tax_amount` float DEFAULT NULL,
  `paidamount` double DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `company_id` int(11) DEFAULT NULL,
  `reconciliation_status` int(11) DEFAULT NULL,
  `reconciliation_date` date DEFAULT NULL,
  `budget_percentage` float DEFAULT NULL,
  `approval_status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '0=>Pending,1=>Approved,2=>Rejected',
  `approved_by` int(11) NOT NULL,
  `record_action` varchar(300) NOT NULL,
  `approve_notify_status` enum('0','1') NOT NULL DEFAULT '1' COMMENT '0=>no.not visible to admin,1=>yes visible to admin',
  `cancelled_by` int(11) DEFAULT NULL,
  `cancelled_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jp_pre_expenses`
--

CREATE TABLE `jp_pre_expenses` (
  `exp_id` int(11) NOT NULL,
  `ref_id` int(11) NOT NULL,
  `followup_id` int(11) DEFAULT NULL,
  `record_grop_id` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `projectid` int(11) NOT NULL,
  `bill_id` int(11) DEFAULT NULL,
  `invoice_id` int(11) DEFAULT NULL,
  `return_id` int(11) DEFAULT NULL,
  `subcontractor_id` int(11) DEFAULT NULL,
  `userid` int(11) NOT NULL,
  `date` date DEFAULT NULL,
  `amount` float(20,2) NOT NULL,
  `description` varchar(300) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `type` tinyint(4) NOT NULL,
  `exptype` int(11) DEFAULT NULL,
  `expense_type` int(11) DEFAULT NULL,
  `vendor_id` int(11) DEFAULT NULL,
  `expense_amount` float DEFAULT NULL,
  `expense_sgstp` float DEFAULT NULL,
  `expense_sgst` float DEFAULT NULL,
  `expense_cgstp` float DEFAULT NULL,
  `expense_cgst` float DEFAULT NULL,
  `expense_igstp` float DEFAULT NULL,
  `expense_igst` float DEFAULT NULL,
  `expense_tdsp` float DEFAULT NULL,
  `expense_tds` double DEFAULT NULL,
  `payment_type` smallint(6) DEFAULT NULL,
  `bank_id` int(11) DEFAULT NULL,
  `cheque_no` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `receipt` float(20,2) DEFAULT NULL,
  `works_done` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `materials` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `purchase_type` int(11) DEFAULT NULL COMMENT '0=>Partially paid;1=>full paid;',
  `paid` float(20,2) DEFAULT NULL,
  `paidamount` double DEFAULT NULL,
  `data_entry` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `reconciliation_status` int(11) DEFAULT NULL,
  `reconciliation_date` date DEFAULT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `budget_percentage` float DEFAULT NULL,
  `update_status` int(11) DEFAULT NULL,
  `delete_status` int(11) DEFAULT NULL,
  `payment_quotation_status` enum('1','2') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '1' COMMENT '1=>payment against quotation, 2=>payment without quotation',
  `additional_charge` float(18,2) DEFAULT NULL,
  `approval_status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '0=>Pending,1=>Approved,2=>Rejected',
  `approved_by` int(11) NOT NULL,
  `record_action` varchar(300) NOT NULL,
  `approve_notify_status` enum('0','1') NOT NULL DEFAULT '1' COMMENT '0=>no.not visible to admin,1=>yes visible to admin',
  `cancelled_by` int(11) DEFAULT NULL,
  `cancelled_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jp_pre_subcontractor_payment`
--

CREATE TABLE `jp_pre_subcontractor_payment` (
  `payment_id` int(11) NOT NULL,
  `ref_id` int(11) NOT NULL,
  `followup_id` int(11) DEFAULT NULL,
  `record_grop_id` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `subcontractor_id` int(11) DEFAULT NULL,
  `project_id` int(11) NOT NULL,
  `amount` float(15,2) NOT NULL,
  `description` text NOT NULL,
  `date` datetime NOT NULL,
  `payment_type` smallint(6) NOT NULL,
  `bank` int(11) DEFAULT NULL,
  `cheque_no` varchar(100) DEFAULT NULL,
  `sgst` float DEFAULT NULL,
  `sgst_amount` float DEFAULT NULL,
  `cgst` float DEFAULT NULL,
  `cgst_amount` float DEFAULT NULL,
  `igst` float DEFAULT NULL,
  `igst_amount` float DEFAULT NULL,
  `tax_amount` int(11) DEFAULT NULL,
  `tds` float DEFAULT NULL,
  `tds_amount` double DEFAULT NULL,
  `paidamount` double DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `reconciliation_status` int(11) DEFAULT NULL,
  `reconciliation_date` datetime DEFAULT NULL,
  `approve_status` enum('Yes','No') DEFAULT 'Yes',
  `uniqueid` varchar(200) DEFAULT NULL,
  `rowcount_slno` int(11) DEFAULT NULL,
  `rowcount_total` int(11) DEFAULT NULL,
  `budget_percentage` float DEFAULT NULL,
  `update_status` int(11) DEFAULT NULL,
  `payment_quotation_status` enum('1','2') NOT NULL DEFAULT '1',
  `quotation_number` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) NOT NULL,
  `updated_date` datetime NOT NULL,
  `delete_status` int(11) DEFAULT NULL,
  `approval_status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '	0=>Pending,1=>Approved,2=>Rejected',
  `approved_by` int(11) DEFAULT NULL,
  `record_action` varchar(300) NOT NULL,
  `approve_notify_status` enum('0','1') NOT NULL COMMENT '0=>no.not visible to admin,1=>yes visible to admin',
  `cancelled_by` int(11) DEFAULT NULL,
  `cancelled_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jp_profiles`
--

CREATE TABLE `jp_profiles` (
  `pid` int(11) NOT NULL,
  `profile_name` varchar(20) NOT NULL,
  `company_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jp_profile_menu_settings`
--

CREATE TABLE `jp_profile_menu_settings` (
  `mp_id` int(20) NOT NULL,
  `role_id` tinyint(1) NOT NULL,
  `menu_id` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jp_projectbook`
--

CREATE TABLE `jp_projectbook` (
  `pb_id` int(11) NOT NULL,
  `projectid` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `book_date` date DEFAULT NULL,
  `book_description` varchar(300) NOT NULL,
  `works_done` varchar(100) DEFAULT NULL,
  `exptype` int(11) DEFAULT NULL,
  `vendor` int(11) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) NOT NULL,
  `updated_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `company_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jp_projects`
--

CREATE TABLE `jp_projects` (
  `pid` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `completion_date` date DEFAULT NULL,
  `start_date` date NOT NULL,
  `billable` smallint(6) DEFAULT '0',
  `status` smallint(6) NOT NULL,
  `description` text,
  `created_date` date NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_date` date NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  `project_status` smallint(6) DEFAULT NULL,
  `project_type` smallint(6) DEFAULT NULL,
  `project_category` int(11) DEFAULT NULL,
  `project_duration` int(11) DEFAULT NULL,
  `work_type_id` int(11) DEFAULT NULL COMMENT 'reference from worktype',
  `sqft` varchar(30) DEFAULT NULL,
  `sqft_rate` int(11) DEFAULT NULL,
  `percentage` int(11) DEFAULT NULL,
  `contract` int(11) DEFAULT NULL,
  `bill_amount` float(20,2) DEFAULT NULL,
  `remarks` varchar(100) DEFAULT NULL,
  `site` varchar(30) DEFAULT NULL,
  `billed_to_client` float(20,2) NOT NULL,
  `tot_expense` float(20,2) NOT NULL,
  `tot_receipt` float(20,2) NOT NULL,
  `tot_paid_to_vendor` float(20,2) NOT NULL,
  `company_id` varchar(100) DEFAULT NULL,
  `profit_margin` float DEFAULT NULL,
  `project_quote` float DEFAULT NULL,
  `expense_percentage` float DEFAULT NULL,
  `expense_amount` float DEFAULT NULL,
  `auto_update` int(11) NOT NULL DEFAULT '0',
  `number_of_flats` int(11) DEFAULT NULL,
  `flat_names` text NOT NULL,
  `project_proposals` text,
  `project_orderissueddate` date DEFAULT NULL,
  `project_totalestimate` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jp_project_exptype`
--

CREATE TABLE `jp_project_exptype` (
  `expid` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jp_project_flat_numbers`
--

CREATE TABLE `jp_project_flat_numbers` (
  `id` int(11) NOT NULL,
  `project_id` int(11) DEFAULT NULL,
  `flat_number` varchar(100) DEFAULT NULL,
  `status` int(11) DEFAULT NULL COMMENT '0->inactive,1->active',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_date` date DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `jp_project_template`
--

CREATE TABLE `jp_project_template` (
  `template_id` int(11) NOT NULL,
  `template_name` varchar(100) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '0',
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jp_project_template`
--

INSERT INTO `jp_project_template` (`template_id`, `template_name`, `status`, `description`) VALUES
(1, 'TYPE-1', '1', 'AGAC'),
(2, 'TYPE-2', '0', 'D2R'),
(3, 'TYPE-3', '0', 'AMZER');

-- --------------------------------------------------------

--
-- Table structure for table `jp_project_type`
--

CREATE TABLE `jp_project_type` (
  `ptid` smallint(6) NOT NULL,
  `project_type` varchar(50) NOT NULL,
  `company_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jp_project_type`
--

INSERT INTO `jp_project_type` (`ptid`, `project_type`, `company_id`) VALUES
(1, 'Local', 1),
(2, 'NRI', 1),
(3, 'Corporates', 1),
(4, 'Client Type 2', 1),
(5, 'Client Type 1', 1),
(6, 'Client Type 3', 1),
(7, 'ANN', 1);

-- --------------------------------------------------------

--
-- Table structure for table `jp_project_work_type`
--

CREATE TABLE `jp_project_work_type` (
  `pro_wrk_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `wrk_type_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jp_purchase`
--

CREATE TABLE `jp_purchase` (
  `p_id` int(11) NOT NULL,
  `purchase_no` varchar(100) DEFAULT NULL,
  `vendor_id` int(11) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `expensehead_id` int(11) DEFAULT NULL,
  `sub_total` float DEFAULT NULL,
  `total_amount` float DEFAULT NULL,
  `contact_no` varchar(15) DEFAULT NULL,
  `shipping_address` tinytext,
  `purchase_status` enum('draft','saved','permission_needed') NOT NULL,
  `purchase_date` date NOT NULL,
  `purchase_billing_status` int(11) NOT NULL,
  `company_id` int(11) DEFAULT NULL,
  `mail_status` enum('Y','N') DEFAULT 'N',
  `po_companyid` int(11) DEFAULT NULL,
  `type` enum('po','bill') NOT NULL DEFAULT 'po',
  `permission_status` enum('Yes','No','Declined') NOT NULL DEFAULT 'Yes',
  `unbilled_amount` double NOT NULL DEFAULT '0',
  `purchase_description` text,
  `inclusive_gst` tinyint(1) DEFAULT NULL COMMENT '0->no,1->yes',
  `purchase_type` enum('A','G','O') DEFAULT 'O',
  `created_by` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jp_purchase_category`
--

CREATE TABLE `jp_purchase_category` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `brand_id` int(11) DEFAULT NULL,
  `category_name` varchar(100) DEFAULT NULL,
  `specification` varchar(255) DEFAULT NULL,
  `hsn_code` varchar(200) DEFAULT NULL,
  `unit` varchar(20) DEFAULT NULL,
  `spec_flag` enum('N','Y') NOT NULL,
  `type` enum('C','S') NOT NULL,
  `dieno` varchar(20) NOT NULL,
  `length` varchar(20) NOT NULL,
  `specification_type` enum('A','O','G') DEFAULT NULL,
  `filename` varchar(255) NOT NULL,
  `purchase_mastercategory` int(11) NOT NULL,
  `warehouse_cat_primary_key` int(11) NOT NULL,
  `company_id` varchar(100) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `spec_status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jp_purchase_category`
--

INSERT INTO `jp_purchase_category` (`id`, `parent_id`, `brand_id`, `category_name`, `specification`, `hsn_code`, `unit`, `spec_flag`, `type`, `dieno`, `length`, `specification_type`, `filename`, `purchase_mastercategory`, `warehouse_cat_primary_key`, `company_id`, `created_by`, `created_date`, `spec_status`) VALUES
(1, NULL, NULL, 'Civil material', NULL, NULL, NULL, '', '', '', '', NULL, '', 0, 0, NULL, 5, '2022-05-18 10:58:37', NULL),
(2, NULL, NULL, 'Civil labour', NULL, NULL, NULL, '', '', '', '', NULL, '', 0, 0, NULL, 5, '2022-05-18 10:58:37', NULL),
(3, NULL, NULL, 'Electrical material', NULL, NULL, NULL, '', '', '', '', NULL, '', 0, 0, NULL, 5, '2022-05-18 10:58:37', NULL),
(4, NULL, NULL, 'Electrical labour', NULL, NULL, NULL, '', '', '', '', NULL, '', 0, 0, NULL, 5, '2022-05-18 10:58:37', NULL),
(5, NULL, NULL, 'Plumbing material', NULL, NULL, NULL, '', '', '', '', NULL, '', 0, 0, NULL, 5, '2022-05-18 10:58:37', NULL),
(6, NULL, NULL, 'Plumbing labour', NULL, NULL, NULL, '', '', '', '', NULL, '', 0, 0, NULL, 5, '2022-05-18 10:58:37', NULL),
(7, NULL, NULL, 'Painting material', NULL, NULL, NULL, '', '', '', '', NULL, '', 0, 0, NULL, 5, '2022-05-18 10:58:37', NULL),
(8, NULL, NULL, 'Painting labour', NULL, NULL, NULL, '', '', '', '', NULL, '', 0, 0, NULL, 5, '2022-05-18 10:58:37', NULL),
(9, NULL, NULL, 'Painting material', NULL, NULL, NULL, '', '', '', '', NULL, '', 0, 0, NULL, 5, '2022-05-18 10:58:37', NULL),
(10, NULL, NULL, 'Tile material', NULL, NULL, NULL, '', '', '', '', NULL, '', 0, 0, NULL, 5, '2022-05-18 10:58:37', NULL),
(11, NULL, NULL, 'Tile labour', NULL, NULL, NULL, '', '', '', '', NULL, '', 0, 0, NULL, 5, '2022-05-18 10:58:37', NULL),
(12, NULL, NULL, 'Rental Equipments ', NULL, NULL, NULL, '', '', '', '', NULL, '', 0, 0, NULL, 5, '2022-05-18 10:58:37', NULL),
(13, NULL, NULL, 'Steel fabrication material', NULL, NULL, NULL, '', '', '', '', NULL, '', 0, 0, NULL, 5, '2022-05-18 10:58:37', NULL),
(14, NULL, NULL, 'Steel fabrication labour', NULL, NULL, NULL, '', '', '', '', NULL, '', 0, 0, NULL, 5, '2022-05-18 10:58:37', NULL),
(15, NULL, NULL, 'Joinery material', NULL, NULL, NULL, '', '', '', '', NULL, '', 0, 0, NULL, 5, '2022-05-18 10:58:37', NULL),
(16, NULL, NULL, 'Joinery labour', NULL, NULL, NULL, '', '', '', '', NULL, '', 0, 0, NULL, 5, '2022-05-18 10:58:37', NULL),
(17, NULL, NULL, 'Office equipment', NULL, NULL, NULL, '', '', '', '', NULL, '', 0, 0, NULL, 5, '2022-05-18 10:58:37', NULL),
(18, NULL, NULL, 'Office supply', NULL, NULL, NULL, '', '', '', '', NULL, '', 0, 0, NULL, 5, '2022-05-18 10:58:37', NULL),
(19, NULL, NULL, 'Office furniture', NULL, NULL, NULL, '', '', '', '', NULL, '', 0, 0, NULL, 5, '2022-05-18 10:58:37', NULL),
(20, NULL, NULL, 'Tools and equipment', NULL, NULL, NULL, '', '', '', '', NULL, '', 0, 0, NULL, 5, '2022-05-18 10:58:37', NULL),
(21, NULL, NULL, 'Water tanks', NULL, NULL, NULL, '', '', '', '', NULL, '', 0, 0, NULL, 5, '2022-05-18 10:58:37', NULL),
(22, NULL, NULL, 'Septic tanks', NULL, NULL, NULL, '', '', '', '', NULL, '', 0, 0, NULL, 5, '2022-05-18 10:58:37', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `jp_purchase_items`
--

CREATE TABLE `jp_purchase_items` (
  `item_id` int(11) NOT NULL,
  `purchase_id` int(11) NOT NULL,
  `description` text,
  `quantity` float NOT NULL,
  `unit` varchar(15) NOT NULL,
  `hsn_code` varchar(200) DEFAULT NULL,
  `rate` float NOT NULL,
  `amount` float NOT NULL,
  `bill_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `item_width` varchar(20) DEFAULT NULL,
  `item_height` varchar(20) DEFAULT NULL,
  `item_dieno` varchar(200) DEFAULT NULL,
  `item_length` varchar(200) DEFAULT NULL,
  `item_status` int(11) NOT NULL,
  `permission_status` enum('approved','not_approved') NOT NULL DEFAULT 'approved',
  `created_by` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `or_description` text,
  `or_quantity` float DEFAULT NULL,
  `or_unit` float DEFAULT NULL,
  `or_rate` float DEFAULT NULL,
  `or_amount` float DEFAULT NULL,
  `wstk_id` int(11) DEFAULT NULL,
  `base_qty` double NOT NULL,
  `base_unit` varchar(150) NOT NULL,
  `base_rate` double NOT NULL,
  `tax_slab` float DEFAULT NULL,
  `discount_percentage` float DEFAULT NULL,
  `discount_amount` float DEFAULT NULL,
  `tax_amount` float DEFAULT NULL,
  `tax_perc` float DEFAULT NULL,
  `cgst_amount` float DEFAULT NULL,
  `cgst_percentage` float DEFAULT NULL,
  `igst_amount` float DEFAULT NULL,
  `igst_percentage` float DEFAULT NULL,
  `sgst_amount` float DEFAULT NULL,
  `sgst_percentage` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `jp_purchase_items`
--
DELIMITER $$
CREATE TRIGGER `after_purchase_items_delete` AFTER DELETE ON `jp_purchase_items` FOR EACH ROW BEGIN

UPDATE jp_purchase SET 
sub_total = ( SELECT SUM( amount ) FROM jp_purchase_items  WHERE purchase_id = OLD.purchase_id  ), total_amount = ( SELECT SUM( amount ) FROM jp_purchase_items  WHERE purchase_id = OLD.purchase_id  )
WHERE p_id = OLD.purchase_id;

END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `after_purchase_items_insert` AFTER INSERT ON `jp_purchase_items` FOR EACH ROW BEGIN
UPDATE jp_purchase SET sub_total = ( SELECT SUM( amount ) FROM jp_purchase_items  WHERE purchase_id = NEW.purchase_id  ), total_amount = ( SELECT SUM( amount ) FROM jp_purchase_items  WHERE purchase_id = NEW.purchase_id) WHERE p_id = NEW.purchase_id;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `after_purchase_items_update` AFTER UPDATE ON `jp_purchase_items` FOR EACH ROW BEGIN
UPDATE jp_purchase SET sub_total = ( SELECT SUM( amount ) FROM jp_purchase_items  WHERE purchase_id = NEW.purchase_id  ), total_amount = ( SELECT SUM( amount ) FROM jp_purchase_items  WHERE purchase_id = NEW.purchase_id  ) WHERE p_id = NEW.purchase_id;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `jp_purchase_mastercategory`
--

CREATE TABLE `jp_purchase_mastercategory` (
  `mastercategory_id` int(11) NOT NULL,
  `mastercategory_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jp_purchase_return`
--

CREATE TABLE `jp_purchase_return` (
  `return_id` int(11) NOT NULL,
  `bill_id` int(11) DEFAULT NULL,
  `return_number` text NOT NULL,
  `return_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `return_amount` float DEFAULT NULL,
  `return_taxamount` float DEFAULT NULL,
  `return_discountamount` float DEFAULT NULL,
  `return_totalamount` float DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_date` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `return_status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jp_purchase_returnitem`
--

CREATE TABLE `jp_purchase_returnitem` (
  `returnitem_id` int(11) NOT NULL,
  `return_id` int(11) NOT NULL,
  `billitem_id` int(11) DEFAULT NULL,
  `returnitem_description` text,
  `returnitem_quantity` float NOT NULL,
  `returnitem_unit` varchar(20) NOT NULL,
  `returnitem_rate` float NOT NULL,
  `returnitem_amount` float NOT NULL,
  `returnitem_taxpercent` float NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `remark` varchar(100) DEFAULT NULL,
  `returnitem_taxamount` float NOT NULL,
  `returnitem_discountpercent` float NOT NULL,
  `returnitem_cgst` float DEFAULT NULL,
  `returnitem_cgstpercent` float(11,2) DEFAULT NULL,
  `returnitem_sgst` float DEFAULT NULL,
  `returnitem_sgstpercent` float DEFAULT NULL,
  `returnitem_igst` float DEFAULT NULL,
  `returnitem_igstpercent` float DEFAULT NULL,
  `returnitem_discountamount` float NOT NULL,
  `created_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jp_quotation`
--

CREATE TABLE `jp_quotation` (
  `quotation_id` int(11) NOT NULL,
  `project_id` int(11) DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  `date` date NOT NULL,
  `amount` float(20,2) DEFAULT NULL,
  `tax_amount` float DEFAULT NULL,
  `inv_no` varchar(10) NOT NULL,
  `fees` float(16,2) DEFAULT NULL,
  `subtotal` float(20,2) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime NOT NULL,
  `convet_invoice_status` int(11) DEFAULT NULL COMMENT '1->not_coverted,2->converted'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jp_quotation_category_master`
--

CREATE TABLE `jp_quotation_category_master` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL COMMENT 'If any subcategory',
  `name` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '1 = Active, 0 = Inactive',
  `created_by` int(11) DEFAULT NULL COMMENT 'From user table',
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jp_quotation_finish_master`
--

CREATE TABLE `jp_quotation_finish_master` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '1 = Active, 0 = Inactive',
  `created_by` int(11) DEFAULT NULL COMMENT 'From user table',
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jp_quotation_gen_category`
--

CREATE TABLE `jp_quotation_gen_category` (
  `id` int(11) NOT NULL,
  `ref_id` int(11) DEFAULT NULL,
  `qid` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  `category_label` varchar(200) NOT NULL,
  `master_cat_id` int(11) DEFAULT NULL,
  `unit` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `quantity_nos` int(11) DEFAULT NULL,
  `mrp` float(10,2) DEFAULT NULL,
  `amount_after_discount` float(10,2) DEFAULT NULL,
  `description` varchar(300) DEFAULT NULL,
  `revision_no` varchar(120) NOT NULL,
  `created_date` datetime NOT NULL,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jp_quotation_gen_worktype`
--

CREATE TABLE `jp_quotation_gen_worktype` (
  `id` int(11) NOT NULL,
  `ref_id` int(11) DEFAULT NULL,
  `qid` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  `category_label_id` int(11) NOT NULL,
  `worktype_label` varchar(100) NOT NULL,
  `master_cat_id` int(11) DEFAULT NULL,
  `unit` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `quantity_nos` int(11) DEFAULT NULL,
  `mrp` float(10,2) DEFAULT NULL,
  `amount_after_discount` float(10,2) DEFAULT NULL,
  `description` text,
  `revision_no` varchar(120) NOT NULL,
  `created_date` datetime NOT NULL,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jp_quotation_item_master`
--

CREATE TABLE `jp_quotation_item_master` (
  `id` int(11) NOT NULL,
  `quotation_category_id` int(11) NOT NULL,
  `work_type_id` int(11) NOT NULL,
  `worktype_label` varchar(255) NOT NULL,
  `work_type_description` varchar(255) DEFAULT NULL,
  `image` varchar(500) DEFAULT NULL,
  `additional_image` varchar(250) DEFAULT NULL,
  `shutter_material_id` varchar(250) DEFAULT NULL,
  `shutter_finish_id` int(11) DEFAULT NULL,
  `carcass_material_id` varchar(250) DEFAULT NULL,
  `carcass_finish_id` int(11) DEFAULT NULL,
  `material_ids` varchar(250) DEFAULT NULL,
  `finish_id` int(11) DEFAULT NULL,
  `shutterwork_description` varchar(255) DEFAULT NULL,
  `caracoss_description` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `rate` float(20,2) NOT NULL,
  `profit` float(20,2) NOT NULL,
  `discount` float(20,2) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '1 = Active, 0 = Inactive',
  `created_by` int(11) NOT NULL COMMENT 'From user table',
  `created_date` datetime NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jp_quotation_list`
--

CREATE TABLE `jp_quotation_list` (
  `id` int(11) NOT NULL,
  `qt_id` int(11) DEFAULT NULL COMMENT 'relaion to jp_quotation->quotation_id',
  `quantity` float(8,2) NOT NULL,
  `unit` varchar(10) DEFAULT NULL,
  `rate` float(16,2) NOT NULL,
  `amount` float(20,2) NOT NULL,
  `cgst` float DEFAULT NULL,
  `cgst_amount` float DEFAULT NULL,
  `sgst` float DEFAULT NULL,
  `sgst_amount` float DEFAULT NULL,
  `igst` float DEFAULT NULL,
  `igst_amount` float DEFAULT NULL,
  `tax_amount` float DEFAULT NULL,
  `description` text NOT NULL,
  `quotationitem_status` int(11) NOT NULL DEFAULT '0',
  `hsn_code` varchar(200) DEFAULT NULL,
  `created_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jp_quotation_revision`
--

CREATE TABLE `jp_quotation_revision` (
  `id` int(11) NOT NULL,
  `qid` int(11) NOT NULL,
  `revision_no` varchar(120) NOT NULL,
  `tax_slab` float DEFAULT NULL,
  `sgst_percent` float(11,2) DEFAULT NULL,
  `sgst_amount` float(11,2) DEFAULT NULL,
  `cgst_percent` float(11,2) DEFAULT NULL,
  `cgst_amount` float(11,2) DEFAULT NULL,
  `igst_percent` float(11,2) DEFAULT NULL,
  `igst_amount` float(11,2) DEFAULT NULL,
  `total_after_discount` float(11,2) DEFAULT NULL,
  `round_off` float(11,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jp_quotation_section`
--

CREATE TABLE `jp_quotation_section` (
  `id` int(11) NOT NULL,
  `ref_id` int(11) DEFAULT NULL,
  `qtn_id` int(11) NOT NULL,
  `section_name` varchar(200) NOT NULL,
  `unit` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `quantity_nos` int(11) DEFAULT NULL,
  `mrp` float(10,2) DEFAULT NULL,
  `amount_after_discount` float(10,2) DEFAULT NULL,
  `description` varchar(300) DEFAULT NULL,
  `revision_no` varchar(120) NOT NULL,
  `created_date` datetime NOT NULL,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jp_quotation_template`
--

CREATE TABLE `jp_quotation_template` (
  `template_id` int(11) NOT NULL,
  `template_name` varchar(100) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0->inactive,1->active',
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jp_quotation_template`
--

INSERT INTO `jp_quotation_template` (`template_id`, `template_name`, `status`, `description`) VALUES
(1, 'template1', '0', 'Default template'),
(2, 'template2', '0', 'template2'),
(3, 'template3', '1', 'template3');

-- --------------------------------------------------------

--
-- Table structure for table `jp_quotation_worktype_master`
--

CREATE TABLE `jp_quotation_worktype_master` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `template_id` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '1 = Active, 0 = Inactive',
  `created_by` int(11) DEFAULT NULL COMMENT 'From user table',
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jp_receipt`
--

CREATE TABLE `jp_receipt` (
  `rec_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `receipt_amount` float(10,2) NOT NULL,
  `date` date NOT NULL,
  `description` varchar(50) NOT NULL,
  `payment_type` smallint(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) NOT NULL,
  `updated_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `company_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jp_reconciliation`
--

CREATE TABLE `jp_reconciliation` (
  `reconciliation_id` int(11) NOT NULL,
  `reconciliation_table` varchar(50) NOT NULL,
  `reconciliation_parentid` int(11) NOT NULL,
  `reconciliation_payment` varchar(50) NOT NULL,
  `reconciliation_paymentdate` date DEFAULT NULL,
  `reconciliation_amount` float NOT NULL,
  `reconciliation_chequeno` varchar(50) NOT NULL,
  `reconciliation_bank` int(11) NOT NULL,
  `reconciliation_date` date DEFAULT NULL,
  `created_date` date NOT NULL,
  `reconciliation_status` int(11) DEFAULT NULL,
  `reconciliation_parentids` varchar(200) DEFAULT NULL,
  `reconciliation_parentlist` varchar(200) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jp_remark`
--

CREATE TABLE `jp_remark` (
  `remark_id` int(11) NOT NULL,
  `remark_remark` varchar(500) NOT NULL,
  `remark_postedby` int(11) NOT NULL,
  `remark_posteddate` datetime NOT NULL,
  `purchase_id` int(11) NOT NULL,
  `remark_status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jp_reminder_comment`
--

CREATE TABLE `jp_reminder_comment` (
  `id` int(11) NOT NULL,
  `reminder_id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `status` enum('Extended','Completed') NOT NULL,
  `created_date` date NOT NULL,
  `company_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jp_salesexecutive`
--

CREATE TABLE `jp_salesexecutive` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '1 = Active, 0 = Inactive ',
  `created_by` int(11) DEFAULT NULL COMMENT 'From user table',
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jp_sales_invoice`
--

CREATE TABLE `jp_sales_invoice` (
  `id` int(11) NOT NULL,
  `company_id` int(11) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  `invoice_date` date DEFAULT NULL,
  `invoice_number` varchar(100) DEFAULT NULL,
  `amount` float DEFAULT NULL,
  `cgst_p` float DEFAULT NULL,
  `cgst_amount` float DEFAULT NULL,
  `sgst_p` float DEFAULT NULL,
  `sgst_amount` float DEFAULT NULL,
  `igst_p` float DEFAULT NULL,
  `igst_amount` float DEFAULT NULL,
  `cess_p` float DEFAULT NULL,
  `cess_amount` float DEFAULT NULL,
  `round_off` float DEFAULT NULL,
  `total_amount` float DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `jp_sales_invoice_item`
--

CREATE TABLE `jp_sales_invoice_item` (
  `id` int(11) NOT NULL,
  `sales_invoice_id` int(11) DEFAULT NULL,
  `main_title` varchar(100) DEFAULT NULL,
  `main_description` text,
  `total_amount` float(20,2) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `jp_sales_invoice_sub_item`
--

CREATE TABLE `jp_sales_invoice_sub_item` (
  `id` int(11) NOT NULL,
  `invoice_id` int(11) DEFAULT NULL,
  `invoice_item_id` int(11) DEFAULT NULL,
  `item_description` text,
  `item_type` int(11) DEFAULT NULL COMMENT '1=>quantity*rate,2=>lumpsum,3=>length*width,4=>length*width*height',
  `quantity` float DEFAULT NULL,
  `unit` varchar(100) NOT NULL,
  `rate` float DEFAULT NULL,
  `length` float DEFAULT NULL COMMENT 'in sq ft',
  `width` float DEFAULT NULL COMMENT 'in sq ft',
  `height` float DEFAULT NULL COMMENT 'in sq ft',
  `amount` float DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `jp_sales_quotation`
--

CREATE TABLE `jp_sales_quotation` (
  `id` int(11) NOT NULL,
  `master_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `item_id` int(11) DEFAULT NULL,
  `category_label` varchar(255) NOT NULL,
  `work_type` int(11) DEFAULT NULL,
  `shutterwork_material` varchar(255) DEFAULT NULL,
  `shutterwork_finish` varchar(255) DEFAULT NULL,
  `carcass_material` varchar(255) DEFAULT NULL,
  `carcass_finish` varchar(255) DEFAULT NULL,
  `material` varchar(255) DEFAULT NULL,
  `finish` varchar(255) DEFAULT NULL,
  `shutterwork_description` varchar(255) DEFAULT NULL,
  `carcass_description` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `worktype_label` varchar(255) DEFAULT NULL,
  `unit` int(11) NOT NULL,
  `quantity` float NOT NULL,
  `quantity_nos` float NOT NULL,
  `mrp` float(10,2) NOT NULL,
  `image` varchar(500) DEFAULT NULL,
  `amount_after_discount` float(10,2) NOT NULL,
  `revision_no` varchar(120) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `deleted_status` int(11) NOT NULL DEFAULT '1' COMMENT '1 = not deleted, 0 = deleted',
  `revision_remarks` varchar(255) DEFAULT NULL,
  `revision_status` int(11) NOT NULL DEFAULT '0',
  `revision_approved` int(11) NOT NULL DEFAULT '0',
  `tax_slab` float DEFAULT NULL,
  `cgst_amount` float(11,2) DEFAULT NULL,
  `cgst_percent` float(11,2) DEFAULT NULL,
  `sgst_amount` float(11,2) DEFAULT NULL,
  `sgst_percent` float(11,2) DEFAULT NULL,
  `igst_amount` float(11,2) DEFAULT NULL,
  `igst_percent` float(11,2) DEFAULT NULL,
  `mainitem_total` float(11,2) DEFAULT NULL,
  `mainitem_tax` float(11,2) DEFAULT NULL,
  `maintotal_withtax` float(11,2) DEFAULT NULL,
  `sub_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1=''subitem'',0=''item''',
  `subitem_label` varchar(250) DEFAULT NULL,
  `parent_type` int(11) DEFAULT NULL COMMENT '0=>category,1=>work type',
  `parent_id` int(11) DEFAULT NULL,
  `revision_delete_status` enum('0','1') NOT NULL DEFAULT '1' COMMENT '0=>Deleted, 1=>Not Deleted',
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jp_sales_quotation_master`
--

CREATE TABLE `jp_sales_quotation_master` (
  `id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `client_name` varchar(50) DEFAULT NULL,
  `address` text,
  `phone_no` varchar(50) DEFAULT NULL,
  `email` varchar(70) DEFAULT NULL,
  `location_id` int(11) DEFAULT NULL,
  `sales_executive_id` int(11) DEFAULT NULL,
  `date_quotation` date NOT NULL,
  `invoice_no` varchar(120) DEFAULT NULL,
  `revision_no` varchar(120) DEFAULT NULL,
  `template_type` enum('1','2') NOT NULL DEFAULT '1' COMMENT '1=>agac, 2=>d2r',
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jp_scquotation`
--

CREATE TABLE `jp_scquotation` (
  `scquotation_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `subcontractor_id` int(11) NOT NULL,
  `expensehead_id` int(11) DEFAULT NULL,
  `scquotation_amount` float DEFAULT NULL,
  `scquotation_decription` varchar(300) NOT NULL,
  `scquotation_date` date NOT NULL,
  `company_id` int(11) DEFAULT NULL,
  `mail_status` enum('Y','N') NOT NULL DEFAULT 'N',
  `approve_status` enum('No','Yes') NOT NULL DEFAULT 'No',
  `completion_date` date DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT NULL,
  `scquotation_status` int(11) NOT NULL DEFAULT '1',
  `type` enum('1','2') DEFAULT '1' COMMENT '1=>lumpsum, 2=>quantity*rate',
  `scquotation_no` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jp_scquotation_items`
--

CREATE TABLE `jp_scquotation_items` (
  `item_id` int(11) NOT NULL,
  `scquotation_id` int(11) NOT NULL,
  `item_category_id` int(11) DEFAULT NULL,
  `item_type` int(11) DEFAULT NULL COMMENT '	1=>quantity*rate,2=>lumpsum,',
  `item_description` text,
  `expensehead_id` int(11) DEFAULT NULL,
  `item_date` date DEFAULT NULL,
  `item_quantity` float DEFAULT NULL,
  `item_unit` varchar(150) DEFAULT NULL,
  `item_rate` float DEFAULT NULL,
  `item_amount` float NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` date NOT NULL,
  `approve_status` enum('Yes','No') NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jp_scquotation_payment_entries`
--

CREATE TABLE `jp_scquotation_payment_entries` (
  `id` int(11) NOT NULL,
  `scquotation_id` int(11) DEFAULT NULL,
  `payment_title` varchar(100) DEFAULT NULL,
  `amount` float DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `jp_sc_quotation_item_category`
--

CREATE TABLE `jp_sc_quotation_item_category` (
  `id` int(11) NOT NULL,
  `sc_quotaion_id` int(11) DEFAULT NULL,
  `main_title` varchar(100) DEFAULT NULL,
  `main_description` text,
  `total_amount` float(20,2) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `jp_specification`
--

CREATE TABLE `jp_specification` (
  `id` int(11) NOT NULL,
  `cat_id` int(11) DEFAULT NULL,
  `brand_id` int(11) DEFAULT NULL,
  `specification` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `hsn_code` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `unit` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `dieno` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `length` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `specification_type` enum('A','O','G') CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `filename` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `company_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `spec_status` enum('0','1') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jp_status`
--

CREATE TABLE `jp_status` (
  `sid` smallint(6) NOT NULL,
  `caption` varchar(30) NOT NULL,
  `status_type` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jp_status`
--

INSERT INTO `jp_status` (`sid`, `caption`, `status_type`) VALUES
(1, 'Active', 'active_status'),
(2, 'Inactive', 'active_status'),
(3, 'Yes', 'yesno'),
(4, 'No', 'yesno'),
(5, 'Client review', 'task_status'),
(6, 'Open', 'task_status'),
(7, 'Closed', 'task_status'),
(8, 'Reassign', 'task_status'),
(9, 'Verify & Close', 'task_status'),
(10, 'Medium', 'priority'),
(11, 'Highest', 'priority'),
(12, 'Low', 'priority'),
(13, 'Hourly', 'work_type'),
(14, 'Fixed', 'work_type'),
(15, 'Waiting', 'bidding_status'),
(16, 'Invited', 'bidding_status'),
(17, 'Hired', 'bidding_status'),
(18, 'Declined', 'bidding_status'),
(19, 'Expired', 'bidding_status'),
(20, 'Withdraw', 'bidding_status'),
(21, 'Client Responded', 'bidding_status'),
(22, 'Invoiced', 'invoice_status'),
(23, 'Paid', 'invoice_status'),
(24, 'Mr', 'person_title'),
(25, 'Ms', 'person_title'),
(26, 'Mrs', 'person_title'),
(27, 'High', 'lead_priority'),
(28, 'Medium', 'lead_priority'),
(29, 'Contacted', 'lead_status'),
(30, 'Waiting Reply', 'lead_status'),
(31, 'Never Contacted', 'lead_status'),
(32, 'Waiting response', 'lead_status'),
(33, 'Group Company', 'industry_type'),
(34, 'Agriculture', 'industry_type'),
(35, 'opp type1', 'opp_type'),
(36, 'opp type 2', 'opp_type'),
(37, 'Accounting', 'industry_type'),
(38, 'Advertising', 'industry_type'),
(39, 'Airline', 'industry_type'),
(40, 'Apparel & Accessories', 'industry_type'),
(41, 'Automotive', 'industry_type'),
(42, 'Banking & Financial Services', 'industry_type'),
(43, 'Broadcasting', 'industry_type'),
(44, 'Call Centers', 'industry_type'),
(45, 'Consulting', 'industry_type'),
(46, 'Consumer Products', 'industry_type'),
(47, 'Education', 'industry_type'),
(48, 'Entertainment & Leisure', 'industry_type'),
(49, 'Health Care', 'industry_type'),
(50, 'Legal', 'industry_type'),
(51, 'Manufacturing', 'industry_type'),
(52, 'Miscellaneous', 'industry_type'),
(53, 'Pharmaceuticals', 'industry_type'),
(54, 'Publishing', 'industry_type'),
(55, 'Real Estate', 'industry_type'),
(56, 'Retail & Wholesale', 'industry_type'),
(57, 'Software', 'industry_type'),
(58, 'Telecommunications', 'industry_type'),
(59, 'Transportation & Trucking', 'industry_type'),
(60, 'Low', 'lead_priority'),
(61, 'Do not Call', 'lead_priority'),
(62, 'Call', 'lead_activity_type'),
(63, 'Meeting', 'lead_activity_type'),
(64, 'Email', 'lead_activity_type'),
(65, 'Hotels & Hospitality', 'industry_type'),
(66, 'Travel & Tourism', 'industry_type'),
(67, 'Insurance', 'industry_type'),
(68, 'Construction', 'industry_type'),
(69, 'Food & Beverages', 'industry_type'),
(70, 'Packaging', 'industry_type'),
(71, 'Trading', 'industry_type'),
(72, 'Credit', 'expense'),
(73, 'Debit', 'expense'),
(74, 'Bank', 'withdrawal'),
(75, 'Loan', 'withdrawal'),
(76, 'OD', 'withdrawal'),
(77, 'Axis Bank', 'withdrawal_details'),
(78, 'HDFC', 'withdrawal_details'),
(79, 'CD', 'withdrawal'),
(80, 'Deposit', 'withdrawal'),
(81, 'SBI', 'withdrawal_details'),
(82, 'SBT', 'withdrawal_details'),
(83, 'Upcoming', 'project_status'),
(84, 'Ongoing', 'project_status'),
(85, 'Completed', 'project_status'),
(86, 'Fixed Percentage', 'project_type'),
(87, 'Square Feet Rate', 'project_type'),
(88, 'Cheque/Online Payment', 'payment_type'),
(89, 'Cash', 'payment_type'),
(90, 'Pending', 'purchase_item'),
(91, 'Delivered', 'purchase_item'),
(92, 'Pending to be Billed', 'purchase_bill_status'),
(93, 'Fully Billed', 'purchase_bill_status'),
(94, 'Partially Billed', 'purchase_bill_status'),
(95, 'PO not issued', 'purchase_bill_status'),
(96, 'Vendor', 'expense_head_type'),
(97, 'Sub Contractor', 'expense_head_type'),
(98, 'Both', 'expense_head_type'),
(99, 'Bank', 'cashbalance_type'),
(100, 'In Hand', 'cashbalance_type'),
(101, 'Bank', 'cashbalance_type2'),
(102, 'Hand', 'cashbalance_type2'),
(103, 'Petty Cash', 'payment_type'),
(104, 'Residential', 'project_category'),
(105, 'Commercial', 'project_category'),
(106, 'Closed', 'project_status'),
(107, 'Credit', 'payment_type');

-- --------------------------------------------------------

--
-- Table structure for table `jp_subcontractor`
--

CREATE TABLE `jp_subcontractor` (
  `subcontractor_id` int(11) NOT NULL,
  `subcontractor_name` varchar(100) NOT NULL,
  `subcontractor_phone` varchar(13) NOT NULL,
  `subcontractor_email` varchar(100) NOT NULL,
  `subcontractor_status` smallint(6) NOT NULL,
  `subcontractor_description` text NOT NULL,
  `gst_no` varchar(100) DEFAULT NULL,
  `company_id` varchar(100) DEFAULT NULL,
  `expense_type_id` int(11) DEFAULT NULL,
  `created_date` datetime NOT NULL,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jp_subcontractorbill`
--

CREATE TABLE `jp_subcontractorbill` (
  `id` int(11) NOT NULL,
  `scquotation_id` int(11) NOT NULL,
  `bill_number` varchar(100) NOT NULL,
  `date` date NOT NULL,
  `amount` double DEFAULT NULL,
  `tax_amount` double DEFAULT NULL,
  `total_amount` double DEFAULT NULL,
  `company_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` date NOT NULL,
  `updated_date` date DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jp_subcontractorbillitem`
--

CREATE TABLE `jp_subcontractorbillitem` (
  `id` int(11) NOT NULL,
  `bill_id` int(11) NOT NULL,
  `quotationitem_id` int(11) NOT NULL,
  `description` varchar(300) NOT NULL,
  `item_type` varchar(100) DEFAULT NULL,
  `item_quantity` float DEFAULT NULL,
  `item_unit` varchar(500) DEFAULT NULL,
  `item_rate` float DEFAULT NULL,
  `amount` int(11) NOT NULL,
  `tax_slab` float DEFAULT NULL,
  `cgstp` float DEFAULT NULL,
  `cgst` double DEFAULT NULL,
  `sgstp` float DEFAULT NULL,
  `sgst` double DEFAULT NULL,
  `igstp` float DEFAULT NULL,
  `igst` double DEFAULT NULL,
  `tax_amount` double DEFAULT NULL,
  `total_amount` double NOT NULL,
  `created_date` date NOT NULL,
  `updated_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jp_subcontractor_exptype`
--

CREATE TABLE `jp_subcontractor_exptype` (
  `exptype_id` int(11) NOT NULL,
  `subcontractor_id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jp_subcontractor_payment`
--

CREATE TABLE `jp_subcontractor_payment` (
  `payment_id` int(11) NOT NULL,
  `subcontractor_id` int(11) DEFAULT NULL,
  `project_id` int(11) NOT NULL,
  `amount` float(15,2) NOT NULL,
  `description` text NOT NULL,
  `date` date NOT NULL,
  `payment_type` smallint(6) NOT NULL,
  `bank` int(11) DEFAULT NULL,
  `cheque_no` varchar(100) DEFAULT NULL,
  `sgst` float DEFAULT NULL,
  `sgst_amount` float DEFAULT NULL,
  `cgst` float DEFAULT NULL,
  `cgst_amount` float DEFAULT NULL,
  `igst` float DEFAULT NULL,
  `igst_amount` float DEFAULT NULL,
  `tax_amount` float DEFAULT NULL,
  `tds` float DEFAULT NULL,
  `tds_amount` double DEFAULT NULL,
  `paidamount` double DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `reconciliation_status` int(11) DEFAULT NULL,
  `reconciliation_date` date DEFAULT NULL,
  `approve_status` enum('Yes','No') NOT NULL DEFAULT 'Yes',
  `uniqueid` varchar(200) DEFAULT NULL,
  `rowcount_slno` int(11) DEFAULT NULL,
  `rowcount_total` int(11) DEFAULT NULL,
  `budget_percentage` float DEFAULT NULL,
  `update_status` int(11) DEFAULT '0' COMMENT '0->default,1->pending for approval,2->declined',
  `payment_quotation_status` enum('1','2') NOT NULL DEFAULT '1' COMMENT '1=>payment against quotation, 2=>payment without quotation',
  `quotation_number` int(11) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT NULL,
  `approval_status` enum('0','1') NOT NULL DEFAULT '1' COMMENT '0=>Pending for approval, 1=>Approved	',
  `duplicate_ignore_status` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0=>Not Ignored, 1=>Ignored',
  `duplicate_delete_status` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0=>Not Deleted, 1=>Deleted'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jp_subcontractor_permission`
--

CREATE TABLE `jp_subcontractor_permission` (
  `id` int(11) NOT NULL,
  `subcontractor_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jp_tasks`
--

CREATE TABLE `jp_tasks` (
  `tskid` int(11) NOT NULL,
  `project_id` int(11) DEFAULT NULL,
  `title` varchar(200) NOT NULL,
  `start_date` date NOT NULL,
  `due_date` date DEFAULT NULL,
  `status` smallint(6) NOT NULL,
  `priority` smallint(6) NOT NULL,
  `assigned_to` int(11) NOT NULL,
  `report_to` int(11) NOT NULL,
  `coordinator` int(11) NOT NULL,
  `billable` smallint(6) NOT NULL,
  `total_hrs` float(10,2) DEFAULT NULL,
  `hourly_rate` float(5,2) DEFAULT NULL,
  `min_rate` float(10,2) DEFAULT NULL,
  `description` text,
  `created_date` date NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_date` date NOT NULL,
  `updated_by` int(11) NOT NULL,
  `company_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jp_tax_slabs`
--

CREATE TABLE `jp_tax_slabs` (
  `id` int(11) NOT NULL,
  `tax_slab_value` float DEFAULT NULL,
  `cgst` float DEFAULT NULL,
  `sgst` float DEFAULT NULL,
  `igst` float DEFAULT NULL,
  `status` int(11) DEFAULT NULL COMMENT '1->active,2->inactive',
  `set_default` tinyint(4) DEFAULT NULL COMMENT '1=>set as default',
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `jp_tax_slabs`
--

INSERT INTO `jp_tax_slabs` (`id`, `tax_slab_value`, `cgst`, `sgst`, `igst`, `status`, `set_default`, `created_by`, `created_date`, `updated_by`, `updated_date`) VALUES
(1, 18, NULL, NULL, NULL, 1, 1, 7, '2020-09-10 13:37:24', 7, '2020-09-10 13:37:24'),
(2, 12, NULL, NULL, NULL, 1, 0, 7, '2020-09-10 13:37:33', 7, '2020-09-10 13:37:33'),
(3, 28, NULL, NULL, NULL, 1, 0, 7, '2020-09-10 13:37:41', 7, '2020-09-10 13:37:41'),
(4, 5, NULL, NULL, NULL, 1, 0, 7, '2020-09-10 13:37:49', 7, '2020-09-10 13:37:49'),
(5, 9, NULL, NULL, NULL, 1, 0, 23, '2021-10-07 12:50:33', 23, '2021-10-07 12:50:33');

-- --------------------------------------------------------

--
-- Table structure for table `jp_unit`
--

CREATE TABLE `jp_unit` (
  `id` int(11) NOT NULL,
  `unit_name` varchar(200) NOT NULL,
  `unit_code` varchar(100) NOT NULL,
  `company_id` varchar(255) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jp_unit`
--

INSERT INTO `jp_unit` (`id`, `unit_name`, `unit_code`, `company_id`, `created_by`, `created_date`) VALUES
(1, 'Bags', '', '1', 5, '2022-05-17 18:30:00'),
(2, 'Cubic feet', '', '1', 5, '2022-05-17 18:30:00'),
(3, 'Cubic meter', '', '1', 5, '2022-05-17 18:30:00'),
(4, 'No’s.', '', '1', 5, '2022-05-17 18:30:00'),
(5, 'Meters', '', '1', 5, '2022-05-17 18:30:00'),
(6, 'Feet', '', '1', 5, '2022-05-17 18:30:00'),
(7, 'Square feet', '', '1', 5, '2022-05-17 18:30:00'),
(8, 'Ltr', '', '1', 5, '2022-05-17 18:30:00'),
(9, 'Square meter', '', '1', 5, '2022-05-17 18:30:00'),
(10, 'Kilograms', '', '1', 5, '2022-05-17 18:30:00'),
(11, 'Inch', '', '1', 5, '2022-05-17 18:30:00'),
(12, 'Centimeter', '', '1', 5, '2022-05-17 18:30:00'),
(13, 'Millimetre', '', '1', 5, '2022-05-17 18:30:00');

-- --------------------------------------------------------

--
-- Table structure for table `jp_unit_conversion`
--

CREATE TABLE `jp_unit_conversion` (
  `id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `base_unit` varchar(150) NOT NULL,
  `conversion_factor` float NOT NULL,
  `conversion_unit` varchar(150) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `priority` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0=>low priority,1=>high priority'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jp_users`
--

CREATE TABLE `jp_users` (
  `userid` int(11) NOT NULL,
  `user_type` tinyint(1) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) DEFAULT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(32) NOT NULL,
  `email` varchar(70) DEFAULT NULL,
  `phonenumber` varchar(20) NOT NULL,
  `reporting_person` int(11) DEFAULT NULL,
  `last_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `reg_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `reg_ip` varchar(15) NOT NULL,
  `activation_key` varchar(32) DEFAULT NULL,
  `email_activation` int(4) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '0',
  `accesscard_id` int(11) DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL COMMENT 'Client id from table jp_clients',
  `designation` varchar(255) DEFAULT NULL,
  `company_id` varchar(255) DEFAULT NULL,
  `google_id` varchar(255) DEFAULT NULL,
  `google_link` text,
  `google_picture_link` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jp_users`
--

INSERT INTO `jp_users` (`userid`, `user_type`, `first_name`, `last_name`, `username`, `password`, `email`, `phonenumber`, `reporting_person`, `last_modified`, `reg_date`, `reg_ip`, `activation_key`, `email_activation`, `status`, `accesscard_id`, `client_id`, `designation`, `company_id`, `google_id`, `google_link`, `google_picture_link`) VALUES
(5, 1, 'Admin', 'Admin', 'admin', '700a1e3e2f2683e982287f02f0010b31', '', '9685741230', NULL, '2022-05-18 08:08:21', '2019-06-23 16:01:39', '122.164.234.197', 'e1d1080e48c9d0399ee64802ce34fe8c', 1, 0, NULL, NULL, NULL, '1', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `jp_user_roles`
--

CREATE TABLE `jp_user_roles` (
  `id` tinyint(1) NOT NULL,
  `role` varchar(30) NOT NULL,
  `dept_id` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jp_user_roles`
--

INSERT INTO `jp_user_roles` (`id`, `role`, `dept_id`, `company_id`) VALUES
(1, 'Administrator', NULL, 1),
(2, 'Accountant', NULL, 1),
(3, 'Designer', NULL, 1),
(4, 'Client', NULL, NULL),
(5, 'Site Supervisor', NULL, 1),
(6, 'Sales  Executive', NULL, NULL),
(7, 'Social Media Developer', NULL, 1),
(8, 'estimator', 2, 1),
(9, 'Purchaser', NULL, 1),
(10, 'Project Manager', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `jp_vendors`
--

CREATE TABLE `jp_vendors` (
  `vendor_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `phone` varchar(13) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `email_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` smallint(6) NOT NULL,
  `description` text NOT NULL,
  `address` text NOT NULL,
  `gst_no` varchar(100) DEFAULT NULL,
  `company_id` varchar(100) DEFAULT NULL,
  `vendor_type` enum('1','2') DEFAULT '1' COMMENT '1=>normal,2=>miscellaneousl',
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jp_vendor_exptype`
--

CREATE TABLE `jp_vendor_exptype` (
  `exp_id` int(11) NOT NULL,
  `vendor_id` int(11) DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jp_vouchers`
--

CREATE TABLE `jp_vouchers` (
  `id` int(11) NOT NULL,
  `voucher_for` tinyint(1) DEFAULT NULL COMMENT '1=>subcontractor,2=>vendor',
  `voucher_date` date DEFAULT NULL,
  `company` int(11) DEFAULT NULL,
  `project` int(11) DEFAULT NULL,
  `voucher_for_user_id` int(11) DEFAULT NULL,
  `expense_head` int(11) DEFAULT NULL,
  `vehicle_number` varchar(100) DEFAULT NULL,
  `voucher_number` varchar(100) DEFAULT NULL,
  `staff_id` int(11) DEFAULT NULL,
  `received_by` varchar(100) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `jp_voucher_items`
--

CREATE TABLE `jp_voucher_items` (
  `id` int(11) NOT NULL,
  `voucher_id` int(11) DEFAULT NULL,
  `worker_label` varchar(100) DEFAULT NULL,
  `no_workers` int(11) DEFAULT NULL,
  `description` text,
  `working_time_size` varchar(100) DEFAULT NULL,
  `net_quantity` varchar(100) DEFAULT NULL,
  `unit` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `jp_warehouse`
--

CREATE TABLE `jp_warehouse` (
  `warehouse_id` int(11) NOT NULL,
  `warehouse_name` varchar(200) NOT NULL,
  `warehouse_place` varchar(200) NOT NULL,
  `warehouse_address` text NOT NULL,
  `project_id` int(11) DEFAULT NULL,
  `warehouse_incharge` int(11) NOT NULL,
  `status` enum('1','0') NOT NULL DEFAULT '1',
  `company_id` int(11) NOT NULL,
  `assigned_to` varchar(255) DEFAULT NULL,
  `created_date` date NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_date` date NOT NULL,
  `updated_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jp_warehousedespatch`
--

CREATE TABLE `jp_warehousedespatch` (
  `warehousedespatch_id` int(11) NOT NULL,
  `warehousedespatch_no` varchar(255) NOT NULL,
  `warehousedespatch_date` date NOT NULL,
  `warehousedespatch_vendor` varchar(255) NOT NULL,
  `warehousedespatch_project` int(11) DEFAULT NULL,
  `warehousedespatch_warehouseid` int(11) NOT NULL,
  `warehousedespatch_warehouseid_to` int(11) DEFAULT NULL,
  `warehousedespatch_clerk` int(11) NOT NULL,
  `warehousedespatch_quantity` float DEFAULT '0',
  `warehousedespatch_through` varchar(200) NOT NULL,
  `company_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` date NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_date` date NOT NULL,
  `warehouse_eta_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jp_warehousedespatch_items`
--

CREATE TABLE `jp_warehousedespatch_items` (
  `item_id` int(11) NOT NULL,
  `warehousedespatch_id` int(11) NOT NULL,
  `warehousedespatch_warehouseid` int(11) NOT NULL,
  `warehousedespatch_itemid` int(11) NOT NULL,
  `warehousestock_itemid_dimension_category` int(11) NOT NULL DEFAULT '1',
  `dimension` varchar(200) DEFAULT NULL,
  `warehousedespatch_batch` varchar(300) DEFAULT NULL,
  `warehousedespatch_unit` varchar(100) NOT NULL,
  `warehousedespatch_quantity` float NOT NULL,
  `warehousedespatch_rate` float(11,2) NOT NULL,
  `warehousedespatch_unitConversion_id` int(11) DEFAULT NULL,
  `warehousedespatch_baseunit_quantity` float NOT NULL,
  `warehousedespatch_size` float DEFAULT NULL,
  `warehousedespatch_remarks` varchar(255) DEFAULT NULL,
  `warehousedespatch_baseqty` float(11,2) NOT NULL,
  `warehousedespatch_baseunit` varchar(20) NOT NULL,
  `warehousedespatch_baserate` float(11,2) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jp_warehousereceipt`
--

CREATE TABLE `jp_warehousereceipt` (
  `warehousereceipt_id` int(11) NOT NULL,
  `warehousereceipt_no` varchar(255) NOT NULL,
  `warehousereceipt_date` date NOT NULL,
  `warehousereceipt_remark` text NOT NULL,
  `warehousereceipt_purchasebill_project` int(11) DEFAULT NULL,
  `warehousereceipt_despatch_id` int(11) DEFAULT NULL,
  `warehousereceipt_bill_id` int(11) DEFAULT NULL,
  `warehousereceipt_warehouseid` int(11) NOT NULL,
  `warehousereceipt_warehouseid_from` int(11) DEFAULT NULL,
  `warehousereceipt_clerk` int(11) NOT NULL,
  `warehousereceipt_quantity` float DEFAULT '0',
  `warehousereceipt_transfer_type` int(11) DEFAULT NULL,
  `company_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` date NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_date` date NOT NULL,
  `delete_status` enum('0','1','','') NOT NULL DEFAULT '0' COMMENT '0=>receipt not deleted, 1 => deleted'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jp_warehousereceipt_items`
--

CREATE TABLE `jp_warehousereceipt_items` (
  `item_id` int(11) NOT NULL,
  `warehousereceipt_id` int(11) NOT NULL,
  `warehousereceipt_warehouseid` int(11) NOT NULL,
  `warehousereceipt_itemid` int(11) NOT NULL,
  `warehousestock_itemid_dimension_category` int(11) DEFAULT '1',
  `dimension` varchar(200) DEFAULT NULL,
  `length` int(11) DEFAULT NULL,
  `purchase_item_id` int(11) DEFAULT NULL,
  `warehousereceipt_unit` varchar(100) NOT NULL,
  `warehousereceipt_batch` varchar(300) DEFAULT NULL,
  `warehousereceipt_quantity` float NOT NULL,
  `warehousereceipt_effective_quantity` int(11) DEFAULT NULL,
  `warehousereceipt_rate` float(11,2) NOT NULL,
  `warehousereceipt_accepted_quantity` float DEFAULT NULL,
  `warehousereceipt_accepted_effective_quantity` float DEFAULT NULL,
  `warehousereceipt_rejected_quantity` float DEFAULT NULL,
  `warehousereceipt_rejected_effective_quantity` int(11) DEFAULT NULL,
  `warehousereceipt_unitConversion_id` int(11) DEFAULT NULL,
  `warehousereceipt_baseunit_accepted_quantity` float NOT NULL,
  `warehousereceipt_baseunit_accepted_effective_quantity` float DEFAULT NULL,
  `warehousereceipt_baseunit_quantity` float DEFAULT NULL,
  `warehousereceipt_baseunit_effective_quantity` int(11) DEFAULT NULL,
  `warehousereceipt_baseunit_rejected_quantity` float DEFAULT NULL,
  `warehousereceipt_baseunit_rejected_effective_quantity` int(11) DEFAULT NULL,
  `warehousereceipt_size` float DEFAULT NULL,
  `warehousereceipt_jono` varchar(200) DEFAULT NULL,
  `warehousereceipt_transfer_type_item_id` int(11) DEFAULT NULL,
  `warehousereceipt_itemunit` varchar(100) NOT NULL,
  `warehousereceipt_itemqty` float(11,2) NOT NULL,
  `warehousereceipt_itemrate` float(11,2) NOT NULL,
  `remark_data` varchar(255) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jp_warehousestock`
--

CREATE TABLE `jp_warehousestock` (
  `warehousestock_id` int(11) NOT NULL,
  `warehousestock_warehouseid` int(11) NOT NULL,
  `warehousestock_date` date NOT NULL,
  `warehousestock_itemid` int(11) NOT NULL,
  `warehousestock_itemid_dimension_category` int(11) NOT NULL DEFAULT '1',
  `dimension` varchar(200) DEFAULT NULL,
  `batch` varchar(300) DEFAULT NULL,
  `rate` float(11,2) NOT NULL,
  `warehousestock_unit` varchar(100) NOT NULL,
  `warehousestock_stock_quantity` float NOT NULL,
  `warehousestock_initial_quantity` float DEFAULT NULL,
  `warehousestock_status` enum('1','0') NOT NULL DEFAULT '1',
  `warehousereceipt_itemid` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jp_warehouse_category`
--

CREATE TABLE `jp_warehouse_category` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `brand_id` int(11) DEFAULT NULL,
  `category_name` varchar(100) DEFAULT NULL,
  `specification` varchar(255) DEFAULT NULL,
  `unit` int(11) DEFAULT NULL,
  `spec_flag` enum('N','Y') NOT NULL,
  `type` enum('C','S') NOT NULL,
  `dieno` varchar(20) DEFAULT NULL,
  `length` varchar(20) DEFAULT NULL,
  `specification_type` enum('A','O','G') DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `purchase_mastercategory` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `purchase_cat_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jp_warehouse_stock_correction`
--

CREATE TABLE `jp_warehouse_stock_correction` (
  `id` int(11) NOT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `wh_receipt_id` int(11) DEFAULT NULL,
  `wh_receipt_item_id` int(11) NOT NULL,
  `Item_Id` int(11) NOT NULL,
  `wh_receipt_quantity` float NOT NULL,
  `stock_quantity` float NOT NULL,
  `rate` float(11,2) NOT NULL,
  `approval_status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '0=>Pending,1=>approved,2=>rejected',
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jp_warehouse_transfer_deleted_log`
--

CREATE TABLE `jp_warehouse_transfer_deleted_log` (
  `id` int(11) NOT NULL,
  `warehouse_transfer_type_delete` enum('1','2','','') NOT NULL COMMENT '1=> delete from warehouse receipt , 2=> delete from warehouse despatch',
  `warehouse_transfer_type_deleted_id` int(11) NOT NULL,
  `warehouse_transfer_type_deleted_no` varchar(100) DEFAULT NULL,
  `warehouse_transfer_date` date DEFAULT NULL,
  `warehouse_transfer_remark` text,
  `warehousereceipt_transfer_type` int(11) DEFAULT NULL,
  `warehousereceipt_despatch_id` int(11) DEFAULT NULL,
  `warehousereceipt_bill_id` int(11) DEFAULT NULL,
  `warehouseid_to` int(11) DEFAULT NULL,
  `warehouseid_from` int(11) DEFAULT NULL,
  `warehousereceipt_clerk` int(11) DEFAULT NULL,
  `warehouse_transfer_items` longtext,
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `warehouse_despatch_date` date DEFAULT NULL,
  `warehouse_despatch_eta_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jp_worktype_template`
--

CREATE TABLE `jp_worktype_template` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `page` varchar(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '1 = Active, 0 = Inactive',
  `created_by` int(11) DEFAULT NULL COMMENT 'From user table',
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jp_worktype_template`
--

INSERT INTO `jp_worktype_template` (`id`, `name`, `page`, `status`, `created_by`, `created_date`) VALUES
(1, 'Box Template', '_template1', 1, NULL, '2021-08-26 17:11:14'),
(2, 'Other ', '_template3', 1, NULL, '2021-08-26 17:11:54'),
(3, 'Additional ', '_template2', 1, NULL, '2021-08-26 17:13:15');

-- --------------------------------------------------------

--
-- Table structure for table `jp_work_type`
--

CREATE TABLE `jp_work_type` (
  `wtid` smallint(6) NOT NULL,
  `work_type` varchar(30) DEFAULT NULL,
  `rate` float(5,2) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jp_writeoff`
--

CREATE TABLE `jp_writeoff` (
  `id` int(11) NOT NULL,
  `invoice_id` int(11) DEFAULT NULL,
  `subcontractor_id` int(11) DEFAULT NULL,
  `amount` double NOT NULL,
  `description` varchar(500) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pms_assignpro_work_type`
--

CREATE TABLE `pms_assignpro_work_type` (
  `pro_wrk_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `wrk_type_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pms_department`
--

CREATE TABLE `pms_department` (
  `dept_id` int(11) NOT NULL,
  `dept_name` varchar(30) NOT NULL,
  `company_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pms_work_type`
--

CREATE TABLE `pms_work_type` (
  `wtid` int(11) NOT NULL,
  `work_type` varchar(30) DEFAULT NULL,
  `rate` float(5,2) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `project_assign`
--

CREATE TABLE `project_assign` (
  `paid` int(11) NOT NULL,
  `projectid` int(11) NOT NULL,
  `userid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Stand-in structure for view `warehouse_transfer_details_all`
-- (See below for the actual view)
--
CREATE TABLE `warehouse_transfer_details_all` (
`warehousedespatch_id` int(11)
,`warehousedespatch_no` varchar(255)
,`warehousedespatch_date` date
,`warehousedespatch_vendor` varchar(255)
,`warehousedespatch_project` int(11)
,`warehousedespatch_warehouseid` int(11)
,`warehousedespatch_warehouseid_to` int(11)
,`warehousedespatch_clerk` int(11)
,`warehousedespatch_quantity` float
,`warehousedespatch_through` varchar(200)
,`company_id` int(11)
,`created_by` int(11)
,`created_date` date
,`updated_by` int(11)
,`updated_date` date
,`warehouse_eta_date` date
,`warehousereceipt_id` int(11)
,`warehousereceipt_no` varchar(255)
,`warehousereceipt_date` date
,`id` int(11)
,`warehouse_transfer_type_delete` varchar(1)
,`warehouse_transfer_type_deleted_id` int(11)
,`warehouse_transfer_type_deleted_no` varchar(100)
,`warehouse_transfer_date` date
,`warehouse_transfer_remark` text
,`warehousereceipt_transfer_type` int(11)
,`warehousereceipt_despatch_id` int(11)
,`warehousereceipt_bill_id` int(11)
,`warehouseid_to` int(11)
,`warehouseid_from` int(11)
,`warehousereceipt_clerk` int(11)
,`warehouse_transfer_items` longtext
,`warehouse_delete_created_date` datetime
,`warehouse_delete_created_by` int(11)
,`warehouse_despatch_date` date
,`warehouse_despatch_eta_date` date
,`delete_status` bigint(20)
,`edit_status` bigint(20)
,`all_warehousereceipt_id` int(11)
,`all_warehousereceipt_no` varchar(255)
);

-- --------------------------------------------------------

--
-- Table structure for table `withdrawals`
--

CREATE TABLE `withdrawals` (
  `exp_id` int(11) NOT NULL,
  `projectid` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `date` date NOT NULL,
  `amount` float(8,2) NOT NULL,
  `description` varchar(300) NOT NULL,
  `type` tinyint(4) NOT NULL,
  `details` smallint(6) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) NOT NULL,
  `updated_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure for view `warehouse_transfer_details_all`
--
DROP TABLE IF EXISTS `warehouse_transfer_details_all`;

CREATE ALGORITHM=UNDEFINED DEFINER=`agacinte`@`localhost` SQL SECURITY DEFINER VIEW `warehouse_transfer_details_all`  AS  (select `wd`.`warehousedespatch_id` AS `warehousedespatch_id`,`wd`.`warehousedespatch_no` AS `warehousedespatch_no`,`wd`.`warehousedespatch_date` AS `warehousedespatch_date`,`wd`.`warehousedespatch_vendor` AS `warehousedespatch_vendor`,`wd`.`warehousedespatch_project` AS `warehousedespatch_project`,`wd`.`warehousedespatch_warehouseid` AS `warehousedespatch_warehouseid`,`wd`.`warehousedespatch_warehouseid_to` AS `warehousedespatch_warehouseid_to`,`wd`.`warehousedespatch_clerk` AS `warehousedespatch_clerk`,`wd`.`warehousedespatch_quantity` AS `warehousedespatch_quantity`,`wd`.`warehousedespatch_through` AS `warehousedespatch_through`,`wd`.`company_id` AS `company_id`,`wd`.`created_by` AS `created_by`,`wd`.`created_date` AS `created_date`,`wd`.`updated_by` AS `updated_by`,`wd`.`updated_date` AS `updated_date`,`wd`.`warehouse_eta_date` AS `warehouse_eta_date`,`wr`.`warehousereceipt_id` AS `warehousereceipt_id`,`wr`.`warehousereceipt_no` AS `warehousereceipt_no`,`wr`.`warehousereceipt_date` AS `warehousereceipt_date`,`wtd`.`id` AS `id`,`wtd`.`warehouse_transfer_type_delete` AS `warehouse_transfer_type_delete`,`wtd`.`warehouse_transfer_type_deleted_id` AS `warehouse_transfer_type_deleted_id`,`wtd`.`warehouse_transfer_type_deleted_no` AS `warehouse_transfer_type_deleted_no`,`wtd`.`warehouse_transfer_date` AS `warehouse_transfer_date`,`wtd`.`warehouse_transfer_remark` AS `warehouse_transfer_remark`,`wtd`.`warehousereceipt_transfer_type` AS `warehousereceipt_transfer_type`,`wtd`.`warehousereceipt_despatch_id` AS `warehousereceipt_despatch_id`,`wtd`.`warehousereceipt_bill_id` AS `warehousereceipt_bill_id`,`wtd`.`warehouseid_to` AS `warehouseid_to`,`wtd`.`warehouseid_from` AS `warehouseid_from`,`wtd`.`warehousereceipt_clerk` AS `warehousereceipt_clerk`,`wtd`.`warehouse_transfer_items` AS `warehouse_transfer_items`,`wtd`.`created_date` AS `warehouse_delete_created_date`,`wtd`.`created_by` AS `warehouse_delete_created_by`,`wtd`.`warehouse_despatch_date` AS `warehouse_despatch_date`,`wtd`.`warehouse_despatch_eta_date` AS `warehouse_despatch_eta_date`,(case when (`wr`.`delete_status` = 0) then 0 when (isnull(`wr`.`delete_status`) and (`wtd`.`id` is not null)) then 1 else 0 end) AS `delete_status`,(case when (`wr`.`delete_status` = 0) then 1 when (isnull(`wr`.`delete_status`) and (`wtd`.`id` is not null) and isnull(`wr`.`warehousereceipt_id`) and (`wd`.`warehousedespatch_warehouseid_to` = `wtd`.`warehouseid_to`)) then 1 when (isnull(`wr`.`delete_status`) and isnull(`wtd`.`id`)) then 1 when isnull(`wtd`.`id`) then 1 else 0 end) AS `edit_status`,(case when (`wr`.`warehousereceipt_id` is not null) then `wr`.`warehousereceipt_id` when (isnull(`wr`.`warehousereceipt_id`) and (`wtd`.`warehouse_transfer_type_deleted_id` is not null)) then `wtd`.`warehouse_transfer_type_deleted_id` else NULL end) AS `all_warehousereceipt_id`,(case when (`wr`.`warehousereceipt_no` is not null) then `wr`.`warehousereceipt_no` when (isnull(`wr`.`warehousereceipt_no`) and (`wtd`.`warehouse_transfer_type_deleted_no` is not null)) then `wtd`.`warehouse_transfer_type_deleted_no` else NULL end) AS `all_warehousereceipt_no` from ((`jp_warehousedespatch` `wd` left join `jp_warehousereceipt` `wr` on((`wd`.`warehousedespatch_id` = `wr`.`warehousereceipt_despatch_id`))) left join `jp_warehouse_transfer_deleted_log` `wtd` on((`wd`.`warehousedespatch_id` = `wtd`.`warehousereceipt_despatch_id`)))) union all (select `wd`.`warehousedespatch_id` AS `warehousedespatch_id`,`wd`.`warehousedespatch_no` AS `warehousedespatch_no`,`wd`.`warehousedespatch_date` AS `warehousedespatch_date`,`wd`.`warehousedespatch_vendor` AS `warehousedespatch_vendor`,`wd`.`warehousedespatch_project` AS `warehousedespatch_project`,`wd`.`warehousedespatch_warehouseid` AS `warehousedespatch_warehouseid`,`wd`.`warehousedespatch_warehouseid_to` AS `warehousedespatch_warehouseid_to`,`wd`.`warehousedespatch_clerk` AS `warehousedespatch_clerk`,`wd`.`warehousedespatch_quantity` AS `warehousedespatch_quantity`,`wd`.`warehousedespatch_through` AS `warehousedespatch_through`,`wd`.`company_id` AS `company_id`,`wd`.`created_by` AS `created_by`,`wd`.`created_date` AS `created_date`,`wd`.`updated_by` AS `updated_by`,`wd`.`updated_date` AS `updated_date`,`wd`.`warehouse_eta_date` AS `warehouse_eta_date`,`wr`.`warehousereceipt_id` AS `warehousereceipt_id`,`wr`.`warehousereceipt_no` AS `warehousereceipt_no`,`wr`.`warehousereceipt_date` AS `warehousereceipt_date`,`wtd`.`id` AS `id`,`wtd`.`warehouse_transfer_type_delete` AS `warehouse_transfer_type_delete`,`wtd`.`warehouse_transfer_type_deleted_id` AS `warehouse_transfer_type_deleted_id`,`wtd`.`warehouse_transfer_type_deleted_no` AS `warehouse_transfer_type_deleted_no`,`wtd`.`warehouse_transfer_date` AS `warehouse_transfer_date`,`wtd`.`warehouse_transfer_remark` AS `warehouse_transfer_remark`,`wtd`.`warehousereceipt_transfer_type` AS `warehousereceipt_transfer_type`,`wtd`.`warehousereceipt_despatch_id` AS `warehousereceipt_despatch_id`,`wtd`.`warehousereceipt_bill_id` AS `warehousereceipt_bill_id`,`wtd`.`warehouseid_to` AS `warehouseid_to`,`wtd`.`warehouseid_from` AS `warehouseid_from`,`wtd`.`warehousereceipt_clerk` AS `warehousereceipt_clerk`,`wtd`.`warehouse_transfer_items` AS `warehouse_transfer_items`,`wtd`.`created_date` AS `warehouse_delete_created_date`,`wtd`.`created_by` AS `warehouse_delete_created_by`,`wtd`.`warehouse_despatch_date` AS `warehouse_despatch_date`,`wtd`.`warehouse_despatch_eta_date` AS `warehouse_despatch_eta_date`,(case when (`wr`.`delete_status` = 0) then 0 when (isnull(`wr`.`delete_status`) and (`wtd`.`id` is not null)) then 1 else 0 end) AS `delete_status`,1 AS `edit_status`,(case when (`wr`.`warehousereceipt_id` is not null) then `wr`.`warehousereceipt_id` when (isnull(`wr`.`warehousereceipt_id`) and (`wtd`.`warehouse_transfer_type_deleted_id` is not null)) then `wtd`.`warehouse_transfer_type_deleted_id` else NULL end) AS `all_warehousereceipt_id`,(case when (`wr`.`warehousereceipt_no` is not null) then `wr`.`warehousereceipt_no` when (isnull(`wr`.`warehousereceipt_no`) and (`wtd`.`warehouse_transfer_type_deleted_no` is not null)) then `wtd`.`warehouse_transfer_type_deleted_no` else NULL end) AS `all_warehousereceipt_no` from ((`jp_warehousedespatch` `wd` left join `jp_warehousereceipt` `wr` on((`wd`.`warehousedespatch_id` = `wr`.`warehousereceipt_despatch_id`))) join `jp_warehouse_transfer_deleted_log` `wtd` on((`wd`.`warehousedespatch_id` = `wtd`.`warehousereceipt_despatch_id`))) where ((`wtd`.`warehousereceipt_transfer_type` = 2) and `wtd`.`id` in (select max(`jp_warehouse_transfer_deleted_log`.`id`) from `jp_warehouse_transfer_deleted_log` group by `jp_warehouse_transfer_deleted_log`.`warehousereceipt_despatch_id`) and ((`wr`.`warehousereceipt_id` is not null) or (isnull(`wr`.`warehousereceipt_id`) and (`wd`.`warehousedespatch_warehouseid_to` <> `wtd`.`warehouseid_to`))))) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `defect_return`
--
ALTER TABLE `defect_return`
  ADD PRIMARY KEY (`return_id`);

--
-- Indexes for table `jp_additional_bill`
--
ALTER TABLE `jp_additional_bill`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jp_albums`
--
ALTER TABLE `jp_albums`
  ADD PRIMARY KEY (`album_id`),
  ADD KEY `fk_albums_createdbyfk` (`created_by`),
  ADD KEY `fk_albums_projectidfk` (`projectid`),
  ADD KEY `fk_albums_updatedby_fk` (`updated_by`);

--
-- Indexes for table `jp_bank`
--
ALTER TABLE `jp_bank`
  ADD PRIMARY KEY (`bank_id`);

--
-- Indexes for table `jp_billitem`
--
ALTER TABLE `jp_billitem`
  ADD PRIMARY KEY (`billitem_id`),
  ADD KEY `jp_billitem_ibfk_1` (`warehouse_id`),
  ADD KEY `jp_billitem_bill_id_fk` (`bill_id`);

--
-- Indexes for table `jp_bills`
--
ALTER TABLE `jp_bills`
  ADD PRIMARY KEY (`bill_id`),
  ADD KEY `sb_bills_purchase_id_fk` (`purchase_id`),
  ADD KEY `sb_bills_ibfk_1` (`warehouse_id`);

--
-- Indexes for table `jp_brand`
--
ALTER TABLE `jp_brand`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jp_buyers`
--
ALTER TABLE `jp_buyers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `buyer_type` (`buyer_type`),
  ADD KEY `status` (`status`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `jp_buyer_invoice`
--
ALTER TABLE `jp_buyer_invoice`
  ADD PRIMARY KEY (`buyer_invoice_id`),
  ADD KEY `buyer_id` (`buyer_id`),
  ADD KEY `company_id` (`company_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `flat_id` (`flat_id`),
  ADD KEY `project_id` (`project_id`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `jp_buyer_invoice_list`
--
ALTER TABLE `jp_buyer_invoice_list`
  ADD PRIMARY KEY (`id`),
  ADD KEY `buyer_inv_id` (`buyer_inv_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `jp_buyer_transactions`
--
ALTER TABLE `jp_buyer_transactions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bank_id` (`bank_id`),
  ADD KEY `buyer_id` (`buyer_id`),
  ADD KEY `company_id` (`company_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `flat_id` (`flat_id`),
  ADD KEY `from_transaction_head` (`from_transaction_head`),
  ADD KEY `project_id` (`project_id`),
  ADD KEY `to_transaction_head` (`to_transaction_head`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `jp_cashbalance`
--
ALTER TABLE `jp_cashbalance`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jp_cashtransfer`
--
ALTER TABLE `jp_cashtransfer`
  ADD PRIMARY KEY (`cashtransfer_id`);

--
-- Indexes for table `jp_clients`
--
ALTER TABLE `jp_clients`
  ADD PRIMARY KEY (`cid`),
  ADD KEY `clients_projtype_fk` (`project_type`),
  ADD KEY `clients_status_status_fk` (`status`);

--
-- Indexes for table `jp_company`
--
ALTER TABLE `jp_company`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jp_company_edit_log`
--
ALTER TABLE `jp_company_edit_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `p_id` (`p_id`),
  ADD KEY `project_id` (`project_id`,`prev_company_id`,`current_company_id`,`changed_by`),
  ADD KEY `prev_company_id` (`prev_company_id`),
  ADD KEY `current_company_id` (`current_company_id`),
  ADD KEY `changed_by` (`changed_by`);

--
-- Indexes for table `jp_company_expense_type`
--
ALTER TABLE `jp_company_expense_type`
  ADD PRIMARY KEY (`company_exp_id`);

--
-- Indexes for table `jp_dailyexpense`
--
ALTER TABLE `jp_dailyexpense`
  ADD PRIMARY KEY (`dailyexp_id`),
  ADD KEY `dailyexpense_company_expid_fk` (`exp_type_id`),
  ADD KEY `dailyexpense_createdby_fk` (`created_by`),
  ADD KEY `dailyexpense_updatedby_fk` (`updated_by`),
  ADD KEY `dailyexpense_vendors_fk` (`vendor_id`),
  ADD KEY `dailyexpense_bank_fk` (`bank_id`),
  ADD KEY `dailyexpense_company_fk` (`company_id`),
  ADD KEY `dailyexpense_employee_fk` (`user_id`),
  ADD KEY `transaction_parent_ref_id` (`transaction_parent`);

--
-- Indexes for table `jp_dailyreport`
--
ALTER TABLE `jp_dailyreport`
  ADD PRIMARY KEY (`dr_id`),
  ADD KEY `fk_dailyreportproject` (`projectid`);

--
-- Indexes for table `jp_dailyvendors`
--
ALTER TABLE `jp_dailyvendors`
  ADD PRIMARY KEY (`daily_v_id`),
  ADD KEY `vendor_id_fk` (`vendor_id`);

--
-- Indexes for table `jp_daily_work_type`
--
ALTER TABLE `jp_daily_work_type`
  ADD PRIMARY KEY (`wtid`);

--
-- Indexes for table `jp_db_changes`
--
ALTER TABLE `jp_db_changes`
  ADD PRIMARY KEY (`db_c_id`);

--
-- Indexes for table `jp_defect_returnitem`
--
ALTER TABLE `jp_defect_returnitem`
  ADD PRIMARY KEY (`returnitem_id`);

--
-- Indexes for table `jp_deletepending`
--
ALTER TABLE `jp_deletepending`
  ADD PRIMARY KEY (`deletepending_id`);

--
-- Indexes for table `jp_deposit`
--
ALTER TABLE `jp_deposit`
  ADD PRIMARY KEY (`deposit_id`);

--
-- Indexes for table `jp_editrequest`
--
ALTER TABLE `jp_editrequest`
  ADD PRIMARY KEY (`editrequest_id`);

--
-- Indexes for table `jp_expenses`
--
ALTER TABLE `jp_expenses`
  ADD PRIMARY KEY (`exp_id`),
  ADD KEY `exp_created_by_fk` (`created_by`),
  ADD KEY `exp_payment_type_fk` (`payment_type`),
  ADD KEY `exp_projectid_fk` (`projectid`),
  ADD KEY `exp_updated_by_fk` (`updated_by`),
  ADD KEY `exp_userid_fk` (`userid`);

--
-- Indexes for table `jp_expenses_import`
--
ALTER TABLE `jp_expenses_import`
  ADD PRIMARY KEY (`exp_id`);

--
-- Indexes for table `jp_expense_notification`
--
ALTER TABLE `jp_expense_notification`
  ADD PRIMARY KEY (`id`),
  ADD KEY `project_id` (`project_id`);

--
-- Indexes for table `jp_expense_type`
--
ALTER TABLE `jp_expense_type`
  ADD PRIMARY KEY (`type_id`);

--
-- Indexes for table `jp_general_settings`
--
ALTER TABLE `jp_general_settings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `jp_image_gallery`
--
ALTER TABLE `jp_image_gallery`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_image_gallery_albumidfk` (`albumid`),
  ADD KEY `jp_image_gallery_projectid_fk` (`projectid`),
  ADD KEY `jp_img_gallery_createdby_fk` (`created_by`),
  ADD KEY `jp_img_gallery_updatedby_fk` (`updated_by`);

--
-- Indexes for table `jp_invoice`
--
ALTER TABLE `jp_invoice`
  ADD PRIMARY KEY (`invoice_id`),
  ADD KEY `fk_project_id` (`project_id`);

--
-- Indexes for table `jp_invoice_list`
--
ALTER TABLE `jp_invoice_list`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jp_invoice_list_perfid_fk` (`perf_id`);

--
-- Indexes for table `jp_inv_list`
--
ALTER TABLE `jp_inv_list`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jp_inv_list_invid_fk` (`inv_id`);

--
-- Indexes for table `jp_itemestimation`
--
ALTER TABLE `jp_itemestimation`
  ADD PRIMARY KEY (`itemestimation_id`);

--
-- Indexes for table `jp_location`
--
ALTER TABLE `jp_location`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jp_log`
--
ALTER TABLE `jp_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `jp_menu`
--
ALTER TABLE `jp_menu`
  ADD PRIMARY KEY (`menu_id`),
  ADD KEY `parent_id` (`parent_id`);

--
-- Indexes for table `jp_menu_permissions`
--
ALTER TABLE `jp_menu_permissions`
  ADD PRIMARY KEY (`mp_id`),
  ADD KEY `fk_menu_id` (`menu_id`);

--
-- Indexes for table `jp_notifications`
--
ALTER TABLE `jp_notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jp_payment_reminders`
--
ALTER TABLE `jp_payment_reminders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jp_performa_invoice`
--
ALTER TABLE `jp_performa_invoice`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jp_po_company`
--
ALTER TABLE `jp_po_company`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jp_pre_dailyexpenses`
--
ALTER TABLE `jp_pre_dailyexpenses`
  ADD PRIMARY KEY (`dailyexp_id`);

--
-- Indexes for table `jp_pre_dailyreport`
--
ALTER TABLE `jp_pre_dailyreport`
  ADD PRIMARY KEY (`dr_id`),
  ADD KEY `projectid_fk` (`projectid`),
  ADD KEY `company_id_fk` (`company_id`),
  ADD KEY `ref_id_fk` (`ref_id`),
  ADD KEY `createdby_fk` (`subcontractor_id`),
  ADD KEY `created_by_fk` (`created_by`);

--
-- Indexes for table `jp_pre_dailyvendors`
--
ALTER TABLE `jp_pre_dailyvendors`
  ADD PRIMARY KEY (`daily_v_id`),
  ADD KEY `dailyexpense_vendor_fk` (`vendor_id`),
  ADD KEY `dailyexpense_project_fk` (`project_id`),
  ADD KEY `dailyexpense_createdby_fk` (`created_by`),
  ADD KEY `dailyexpense_updatedby_fk` (`updated_by`),
  ADD KEY `dailyexpense_approvedby_fk` (`approved_by`),
  ADD KEY `dailyexpense_cancelledby_fk` (`cancelled_by`),
  ADD KEY `dailyexpense_company_fk3` (`company_id`),
  ADD KEY `dailyexpense_bank_fk1` (`bank`);

--
-- Indexes for table `jp_pre_expenses`
--
ALTER TABLE `jp_pre_expenses`
  ADD PRIMARY KEY (`exp_id`),
  ADD KEY `exp_projectid_fk` (`projectid`),
  ADD KEY `exp_bill_fk` (`bill_id`),
  ADD KEY `exp_inv_fk` (`invoice_id`),
  ADD KEY `exp_sub_fk` (`subcontractor_id`),
  ADD KEY `exp_user_fk` (`userid`),
  ADD KEY `exp_created_by_fk` (`created_by`),
  ADD KEY `exp_updated_by_fk` (`updated_by`),
  ADD KEY `exp_cancelled_by_fk` (`cancelled_by`),
  ADD KEY `exp_vendor_fk` (`vendor_id`);

--
-- Indexes for table `jp_pre_subcontractor_payment`
--
ALTER TABLE `jp_pre_subcontractor_payment`
  ADD PRIMARY KEY (`payment_id`),
  ADD KEY `FK_project_id` (`project_id`),
  ADD KEY `FK_subcontractor_id` (`subcontractor_id`),
  ADD KEY `FK_ref_id` (`ref_id`),
  ADD KEY `FK_bank` (`bank`),
  ADD KEY `FK_company_id` (`company_id`),
  ADD KEY `FK_created_by` (`created_by`),
  ADD KEY `FK_approved_by` (`approved_by`);

--
-- Indexes for table `jp_profiles`
--
ALTER TABLE `jp_profiles`
  ADD PRIMARY KEY (`pid`);

--
-- Indexes for table `jp_profile_menu_settings`
--
ALTER TABLE `jp_profile_menu_settings`
  ADD PRIMARY KEY (`mp_id`),
  ADD KEY `menu_id` (`menu_id`),
  ADD KEY `user_id` (`role_id`);

--
-- Indexes for table `jp_projectbook`
--
ALTER TABLE `jp_projectbook`
  ADD PRIMARY KEY (`pb_id`),
  ADD KEY `fk_projectbook_expfk` (`exptype`),
  ADD KEY `fk_projectbook_vendorfk` (`vendor`),
  ADD KEY `jp_projectbook_ibfk_1` (`projectid`),
  ADD KEY `jp_projectbook_ibfk_2` (`userid`),
  ADD KEY `jp_projectbook_ibfk_3` (`created_by`),
  ADD KEY `jp_projectbook_ibfk_4` (`updated_by`);

--
-- Indexes for table `jp_projects`
--
ALTER TABLE `jp_projects`
  ADD PRIMARY KEY (`pid`),
  ADD KEY `clientid_fk` (`client_id`),
  ADD KEY `project_status_fk` (`project_status`),
  ADD KEY `project_type_fk` (`project_type`),
  ADD KEY `projects_status_status_fk` (`status`),
  ADD KEY `createdby_fk` (`created_by`);

--
-- Indexes for table `jp_project_exptype`
--
ALTER TABLE `jp_project_exptype`
  ADD PRIMARY KEY (`expid`),
  ADD KEY `projectid_fk` (`project_id`),
  ADD KEY `type_id_fk` (`type_id`);

--
-- Indexes for table `jp_project_flat_numbers`
--
ALTER TABLE `jp_project_flat_numbers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jp_project_template`
--
ALTER TABLE `jp_project_template`
  ADD PRIMARY KEY (`template_id`);

--
-- Indexes for table `jp_project_type`
--
ALTER TABLE `jp_project_type`
  ADD PRIMARY KEY (`ptid`);

--
-- Indexes for table `jp_project_work_type`
--
ALTER TABLE `jp_project_work_type`
  ADD PRIMARY KEY (`pro_wrk_id`);

--
-- Indexes for table `jp_purchase`
--
ALTER TABLE `jp_purchase`
  ADD PRIMARY KEY (`p_id`),
  ADD KEY `projectfk` (`project_id`);

--
-- Indexes for table `jp_purchase_category`
--
ALTER TABLE `jp_purchase_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jp_purchase_items`
--
ALTER TABLE `jp_purchase_items`
  ADD PRIMARY KEY (`item_id`),
  ADD KEY `jp_purchase_items_purchase_id_fk` (`purchase_id`);

--
-- Indexes for table `jp_purchase_mastercategory`
--
ALTER TABLE `jp_purchase_mastercategory`
  ADD PRIMARY KEY (`mastercategory_id`);

--
-- Indexes for table `jp_purchase_return`
--
ALTER TABLE `jp_purchase_return`
  ADD PRIMARY KEY (`return_id`);

--
-- Indexes for table `jp_purchase_returnitem`
--
ALTER TABLE `jp_purchase_returnitem`
  ADD PRIMARY KEY (`returnitem_id`);

--
-- Indexes for table `jp_quotation`
--
ALTER TABLE `jp_quotation`
  ADD PRIMARY KEY (`quotation_id`);

--
-- Indexes for table `jp_quotation_category_master`
--
ALTER TABLE `jp_quotation_category_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jp_quotation_finish_master`
--
ALTER TABLE `jp_quotation_finish_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jp_quotation_gen_category`
--
ALTER TABLE `jp_quotation_gen_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jp_quotation_gen_worktype`
--
ALTER TABLE `jp_quotation_gen_worktype`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jp_quotation_item_master`
--
ALTER TABLE `jp_quotation_item_master`
  ADD PRIMARY KEY (`id`),
  ADD KEY `shutter_finish_id` (`shutter_finish_id`),
  ADD KEY `shutter_material_id` (`shutter_material_id`),
  ADD KEY `carcass_material_id` (`carcass_material_id`),
  ADD KEY `carcass_finish_id` (`carcass_finish_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `quotation_category_id` (`quotation_category_id`),
  ADD KEY `work_type_id` (`work_type_id`),
  ADD KEY `finish_id` (`finish_id`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `jp_quotation_list`
--
ALTER TABLE `jp_quotation_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jp_quotation_revision`
--
ALTER TABLE `jp_quotation_revision`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jp_quotation_section`
--
ALTER TABLE `jp_quotation_section`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jp_quotation_template`
--
ALTER TABLE `jp_quotation_template`
  ADD PRIMARY KEY (`template_id`);

--
-- Indexes for table `jp_quotation_worktype_master`
--
ALTER TABLE `jp_quotation_worktype_master`
  ADD PRIMARY KEY (`id`),
  ADD KEY `template_id` (`template_id`);

--
-- Indexes for table `jp_receipt`
--
ALTER TABLE `jp_receipt`
  ADD PRIMARY KEY (`rec_id`),
  ADD KEY `payment_type` (`payment_type`),
  ADD KEY `project_id` (`project_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `jp_reconciliation`
--
ALTER TABLE `jp_reconciliation`
  ADD PRIMARY KEY (`reconciliation_id`);

--
-- Indexes for table `jp_remark`
--
ALTER TABLE `jp_remark`
  ADD PRIMARY KEY (`remark_id`);

--
-- Indexes for table `jp_reminder_comment`
--
ALTER TABLE `jp_reminder_comment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jp_salesexecutive`
--
ALTER TABLE `jp_salesexecutive`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jp_sales_invoice`
--
ALTER TABLE `jp_sales_invoice`
  ADD PRIMARY KEY (`id`),
  ADD KEY `company_id` (`company_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `project_id` (`project_id`),
  ADD KEY `updated_by` (`updated_by`),
  ADD KEY `client_id` (`client_id`);

--
-- Indexes for table `jp_sales_invoice_item`
--
ALTER TABLE `jp_sales_invoice_item`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `sales_invoice_id` (`sales_invoice_id`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `jp_sales_invoice_sub_item`
--
ALTER TABLE `jp_sales_invoice_sub_item`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `invoice_item_id` (`invoice_item_id`),
  ADD KEY `updated_by` (`updated_by`),
  ADD KEY `invoice_id` (`invoice_id`);

--
-- Indexes for table `jp_sales_quotation`
--
ALTER TABLE `jp_sales_quotation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `master_id` (`master_id`),
  ADD KEY `category_id` (`category_id`),
  ADD KEY `work_type` (`work_type`),
  ADD KEY `unit` (`unit`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `item_id` (`item_id`);

--
-- Indexes for table `jp_sales_quotation_master`
--
ALTER TABLE `jp_sales_quotation_master`
  ADD PRIMARY KEY (`id`),
  ADD KEY `company_id` (`company_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `location_id` (`location_id`),
  ADD KEY `sales_executive_id` (`sales_executive_id`);

--
-- Indexes for table `jp_scquotation`
--
ALTER TABLE `jp_scquotation`
  ADD PRIMARY KEY (`scquotation_id`),
  ADD KEY `expensehead_id` (`expensehead_id`);

--
-- Indexes for table `jp_scquotation_items`
--
ALTER TABLE `jp_scquotation_items`
  ADD PRIMARY KEY (`item_id`);

--
-- Indexes for table `jp_scquotation_payment_entries`
--
ALTER TABLE `jp_scquotation_payment_entries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `scquotation_id` (`scquotation_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `jp_sc_quotation_item_category`
--
ALTER TABLE `jp_sc_quotation_item_category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sc_quotaion_id` (`sc_quotaion_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `jp_specification`
--
ALTER TABLE `jp_specification`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cat_id_fk` (`cat_id`),
  ADD KEY `brand_id` (`brand_id`),
  ADD KEY `created_by` (`created_by`);

--
-- Indexes for table `jp_status`
--
ALTER TABLE `jp_status`
  ADD PRIMARY KEY (`sid`);

--
-- Indexes for table `jp_subcontractor`
--
ALTER TABLE `jp_subcontractor`
  ADD PRIMARY KEY (`subcontractor_id`);

--
-- Indexes for table `jp_subcontractorbill`
--
ALTER TABLE `jp_subcontractorbill`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jp_subcontractorbillitem`
--
ALTER TABLE `jp_subcontractorbillitem`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jp_subcontractor_exptype`
--
ALTER TABLE `jp_subcontractor_exptype`
  ADD PRIMARY KEY (`exptype_id`);

--
-- Indexes for table `jp_subcontractor_payment`
--
ALTER TABLE `jp_subcontractor_payment`
  ADD PRIMARY KEY (`payment_id`),
  ADD KEY `subcontractor_id_fk` (`subcontractor_id`),
  ADD KEY `quotation_number` (`quotation_number`);

--
-- Indexes for table `jp_subcontractor_permission`
--
ALTER TABLE `jp_subcontractor_permission`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jp_tasks`
--
ALTER TABLE `jp_tasks`
  ADD PRIMARY KEY (`tskid`),
  ADD KEY `tasks_billable_status_fk` (`billable`),
  ADD KEY `tasks_priority_fk` (`priority`),
  ADD KEY `tasks_projects_fk` (`project_id`),
  ADD KEY `tasks_status_fk` (`status`),
  ADD KEY `tasks_users_assinged_to_fk` (`assigned_to`),
  ADD KEY `tasks_users_coordinator_fk` (`coordinator`),
  ADD KEY `tasks_users_createdby_fk` (`created_by`),
  ADD KEY `tasks_users_report_to_fk` (`report_to`),
  ADD KEY `tasks_users_updatedby_fk` (`updated_by`);

--
-- Indexes for table `jp_tax_slabs`
--
ALTER TABLE `jp_tax_slabs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `jp_unit`
--
ALTER TABLE `jp_unit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jp_unit_conversion`
--
ALTER TABLE `jp_unit_conversion`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sb_unit_conversion_ibfk_1` (`item_id`);

--
-- Indexes for table `jp_users`
--
ALTER TABLE `jp_users`
  ADD PRIMARY KEY (`userid`),
  ADD KEY `jp_users_client_id_fk` (`client_id`),
  ADD KEY `user_report_to` (`reporting_person`),
  ADD KEY `users_type_fk` (`user_type`);

--
-- Indexes for table `jp_user_roles`
--
ALTER TABLE `jp_user_roles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jp_user_roles_department_fk` (`dept_id`);

--
-- Indexes for table `jp_vendors`
--
ALTER TABLE `jp_vendors`
  ADD PRIMARY KEY (`vendor_id`);

--
-- Indexes for table `jp_vendor_exptype`
--
ALTER TABLE `jp_vendor_exptype`
  ADD PRIMARY KEY (`exp_id`),
  ADD KEY `exp_type_id_fk` (`type_id`),
  ADD KEY `exp_vendor_id_fk` (`vendor_id`);

--
-- Indexes for table `jp_vouchers`
--
ALTER TABLE `jp_vouchers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `company` (`company`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `expense_head` (`expense_head`),
  ADD KEY `project` (`project`),
  ADD KEY `staff_id` (`staff_id`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `jp_voucher_items`
--
ALTER TABLE `jp_voucher_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`),
  ADD KEY `voucher_id` (`voucher_id`),
  ADD KEY `unit` (`unit`);

--
-- Indexes for table `jp_warehouse`
--
ALTER TABLE `jp_warehouse`
  ADD PRIMARY KEY (`warehouse_id`),
  ADD KEY `project_id` (`project_id`);

--
-- Indexes for table `jp_warehousedespatch`
--
ALTER TABLE `jp_warehousedespatch`
  ADD PRIMARY KEY (`warehousedespatch_id`),
  ADD KEY `warehousedespatch_warehouseid_to` (`warehousedespatch_warehouseid_to`),
  ADD KEY `warehousedespatch_project` (`warehousedespatch_project`),
  ADD KEY `warehousedespatch_clerk` (`warehousedespatch_clerk`);

--
-- Indexes for table `jp_warehousedespatch_items`
--
ALTER TABLE `jp_warehousedespatch_items`
  ADD PRIMARY KEY (`item_id`);

--
-- Indexes for table `jp_warehousereceipt`
--
ALTER TABLE `jp_warehousereceipt`
  ADD PRIMARY KEY (`warehousereceipt_id`),
  ADD KEY `warehousereceipt_project` (`warehousereceipt_purchasebill_project`),
  ADD KEY `warehousereceipt_despatch_id` (`warehousereceipt_despatch_id`),
  ADD KEY `warehousereceipt_bill_id` (`warehousereceipt_bill_id`),
  ADD KEY `warehousereceipt_warehouseid_from` (`warehousereceipt_warehouseid_from`);

--
-- Indexes for table `jp_warehousereceipt_items`
--
ALTER TABLE `jp_warehousereceipt_items`
  ADD PRIMARY KEY (`item_id`),
  ADD KEY `warehousereceipt_unitConversion_id` (`warehousereceipt_unitConversion_id`);

--
-- Indexes for table `jp_warehousestock`
--
ALTER TABLE `jp_warehousestock`
  ADD PRIMARY KEY (`warehousestock_id`);

--
-- Indexes for table `jp_warehouse_category`
--
ALTER TABLE `jp_warehouse_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jp_warehouse_stock_correction`
--
ALTER TABLE `jp_warehouse_stock_correction`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jp_warehouse_transfer_deleted_log`
--
ALTER TABLE `jp_warehouse_transfer_deleted_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `warehouseid_from` (`warehouseid_from`),
  ADD KEY `warehouseid_to` (`warehouseid_to`),
  ADD KEY `warehousereceipt_clerk` (`warehousereceipt_clerk`);

--
-- Indexes for table `jp_worktype_template`
--
ALTER TABLE `jp_worktype_template`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`);

--
-- Indexes for table `jp_work_type`
--
ALTER TABLE `jp_work_type`
  ADD PRIMARY KEY (`wtid`);

--
-- Indexes for table `jp_writeoff`
--
ALTER TABLE `jp_writeoff`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pms_assignpro_work_type`
--
ALTER TABLE `pms_assignpro_work_type`
  ADD PRIMARY KEY (`pro_wrk_id`);

--
-- Indexes for table `pms_department`
--
ALTER TABLE `pms_department`
  ADD PRIMARY KEY (`dept_id`);

--
-- Indexes for table `pms_work_type`
--
ALTER TABLE `pms_work_type`
  ADD KEY `pms_work_type_dept_id_fk` (`department_id`);

--
-- Indexes for table `project_assign`
--
ALTER TABLE `project_assign`
  ADD PRIMARY KEY (`paid`),
  ADD KEY `projectid` (`projectid`),
  ADD KEY `userid` (`userid`);

--
-- Indexes for table `withdrawals`
--
ALTER TABLE `withdrawals`
  ADD PRIMARY KEY (`exp_id`),
  ADD KEY `expenses_ibfk_2` (`userid`),
  ADD KEY `expenses_ibfk_1` (`projectid`),
  ADD KEY `expenses_created_ibfk_2` (`created_by`),
  ADD KEY `expenses_updated_ibfk_1` (`updated_by`),
  ADD KEY `details` (`details`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `defect_return`
--
ALTER TABLE `defect_return`
  MODIFY `return_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_additional_bill`
--
ALTER TABLE `jp_additional_bill`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_albums`
--
ALTER TABLE `jp_albums`
  MODIFY `album_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_bank`
--
ALTER TABLE `jp_bank`
  MODIFY `bank_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_billitem`
--
ALTER TABLE `jp_billitem`
  MODIFY `billitem_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_bills`
--
ALTER TABLE `jp_bills`
  MODIFY `bill_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_brand`
--
ALTER TABLE `jp_brand`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT for table `jp_buyers`
--
ALTER TABLE `jp_buyers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_buyer_invoice`
--
ALTER TABLE `jp_buyer_invoice`
  MODIFY `buyer_invoice_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_buyer_invoice_list`
--
ALTER TABLE `jp_buyer_invoice_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_buyer_transactions`
--
ALTER TABLE `jp_buyer_transactions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_cashbalance`
--
ALTER TABLE `jp_cashbalance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_cashtransfer`
--
ALTER TABLE `jp_cashtransfer`
  MODIFY `cashtransfer_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_clients`
--
ALTER TABLE `jp_clients`
  MODIFY `cid` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_company`
--
ALTER TABLE `jp_company`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_company_edit_log`
--
ALTER TABLE `jp_company_edit_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_company_expense_type`
--
ALTER TABLE `jp_company_expense_type`
  MODIFY `company_exp_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=128;

--
-- AUTO_INCREMENT for table `jp_dailyexpense`
--
ALTER TABLE `jp_dailyexpense`
  MODIFY `dailyexp_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_dailyreport`
--
ALTER TABLE `jp_dailyreport`
  MODIFY `dr_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_dailyvendors`
--
ALTER TABLE `jp_dailyvendors`
  MODIFY `daily_v_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_daily_work_type`
--
ALTER TABLE `jp_daily_work_type`
  MODIFY `wtid` smallint(6) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_db_changes`
--
ALTER TABLE `jp_db_changes`
  MODIFY `db_c_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_defect_returnitem`
--
ALTER TABLE `jp_defect_returnitem`
  MODIFY `returnitem_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_deletepending`
--
ALTER TABLE `jp_deletepending`
  MODIFY `deletepending_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_deposit`
--
ALTER TABLE `jp_deposit`
  MODIFY `deposit_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_editrequest`
--
ALTER TABLE `jp_editrequest`
  MODIFY `editrequest_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_expenses`
--
ALTER TABLE `jp_expenses`
  MODIFY `exp_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_expenses_import`
--
ALTER TABLE `jp_expenses_import`
  MODIFY `exp_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_expense_notification`
--
ALTER TABLE `jp_expense_notification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_expense_type`
--
ALTER TABLE `jp_expense_type`
  MODIFY `type_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_general_settings`
--
ALTER TABLE `jp_general_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `jp_image_gallery`
--
ALTER TABLE `jp_image_gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_invoice`
--
ALTER TABLE `jp_invoice`
  MODIFY `invoice_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_invoice_list`
--
ALTER TABLE `jp_invoice_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_inv_list`
--
ALTER TABLE `jp_inv_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_itemestimation`
--
ALTER TABLE `jp_itemestimation`
  MODIFY `itemestimation_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_location`
--
ALTER TABLE `jp_location`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- AUTO_INCREMENT for table `jp_log`
--
ALTER TABLE `jp_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_menu`
--
ALTER TABLE `jp_menu`
  MODIFY `menu_id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=776;

--
-- AUTO_INCREMENT for table `jp_menu_permissions`
--
ALTER TABLE `jp_menu_permissions`
  MODIFY `mp_id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4925;

--
-- AUTO_INCREMENT for table `jp_notifications`
--
ALTER TABLE `jp_notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_payment_reminders`
--
ALTER TABLE `jp_payment_reminders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_performa_invoice`
--
ALTER TABLE `jp_performa_invoice`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_po_company`
--
ALTER TABLE `jp_po_company`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_pre_dailyexpenses`
--
ALTER TABLE `jp_pre_dailyexpenses`
  MODIFY `dailyexp_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_pre_dailyreport`
--
ALTER TABLE `jp_pre_dailyreport`
  MODIFY `dr_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_pre_dailyvendors`
--
ALTER TABLE `jp_pre_dailyvendors`
  MODIFY `daily_v_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_pre_expenses`
--
ALTER TABLE `jp_pre_expenses`
  MODIFY `exp_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_pre_subcontractor_payment`
--
ALTER TABLE `jp_pre_subcontractor_payment`
  MODIFY `payment_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_profiles`
--
ALTER TABLE `jp_profiles`
  MODIFY `pid` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_profile_menu_settings`
--
ALTER TABLE `jp_profile_menu_settings`
  MODIFY `mp_id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_projectbook`
--
ALTER TABLE `jp_projectbook`
  MODIFY `pb_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_projects`
--
ALTER TABLE `jp_projects`
  MODIFY `pid` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_project_exptype`
--
ALTER TABLE `jp_project_exptype`
  MODIFY `expid` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_project_flat_numbers`
--
ALTER TABLE `jp_project_flat_numbers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_project_template`
--
ALTER TABLE `jp_project_template`
  MODIFY `template_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `jp_project_type`
--
ALTER TABLE `jp_project_type`
  MODIFY `ptid` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `jp_project_work_type`
--
ALTER TABLE `jp_project_work_type`
  MODIFY `pro_wrk_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_purchase`
--
ALTER TABLE `jp_purchase`
  MODIFY `p_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_purchase_category`
--
ALTER TABLE `jp_purchase_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `jp_purchase_items`
--
ALTER TABLE `jp_purchase_items`
  MODIFY `item_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_purchase_mastercategory`
--
ALTER TABLE `jp_purchase_mastercategory`
  MODIFY `mastercategory_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_purchase_return`
--
ALTER TABLE `jp_purchase_return`
  MODIFY `return_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_purchase_returnitem`
--
ALTER TABLE `jp_purchase_returnitem`
  MODIFY `returnitem_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_quotation`
--
ALTER TABLE `jp_quotation`
  MODIFY `quotation_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_quotation_category_master`
--
ALTER TABLE `jp_quotation_category_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_quotation_finish_master`
--
ALTER TABLE `jp_quotation_finish_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_quotation_gen_category`
--
ALTER TABLE `jp_quotation_gen_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_quotation_gen_worktype`
--
ALTER TABLE `jp_quotation_gen_worktype`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_quotation_item_master`
--
ALTER TABLE `jp_quotation_item_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_quotation_list`
--
ALTER TABLE `jp_quotation_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_quotation_revision`
--
ALTER TABLE `jp_quotation_revision`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_quotation_section`
--
ALTER TABLE `jp_quotation_section`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_quotation_template`
--
ALTER TABLE `jp_quotation_template`
  MODIFY `template_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `jp_quotation_worktype_master`
--
ALTER TABLE `jp_quotation_worktype_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_receipt`
--
ALTER TABLE `jp_receipt`
  MODIFY `rec_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_reconciliation`
--
ALTER TABLE `jp_reconciliation`
  MODIFY `reconciliation_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_remark`
--
ALTER TABLE `jp_remark`
  MODIFY `remark_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_reminder_comment`
--
ALTER TABLE `jp_reminder_comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_salesexecutive`
--
ALTER TABLE `jp_salesexecutive`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_sales_invoice`
--
ALTER TABLE `jp_sales_invoice`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_sales_invoice_item`
--
ALTER TABLE `jp_sales_invoice_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_sales_invoice_sub_item`
--
ALTER TABLE `jp_sales_invoice_sub_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_sales_quotation`
--
ALTER TABLE `jp_sales_quotation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_sales_quotation_master`
--
ALTER TABLE `jp_sales_quotation_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_scquotation`
--
ALTER TABLE `jp_scquotation`
  MODIFY `scquotation_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_scquotation_items`
--
ALTER TABLE `jp_scquotation_items`
  MODIFY `item_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_scquotation_payment_entries`
--
ALTER TABLE `jp_scquotation_payment_entries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_sc_quotation_item_category`
--
ALTER TABLE `jp_sc_quotation_item_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_specification`
--
ALTER TABLE `jp_specification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_status`
--
ALTER TABLE `jp_status`
  MODIFY `sid` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=108;

--
-- AUTO_INCREMENT for table `jp_subcontractor`
--
ALTER TABLE `jp_subcontractor`
  MODIFY `subcontractor_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_subcontractorbill`
--
ALTER TABLE `jp_subcontractorbill`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_subcontractorbillitem`
--
ALTER TABLE `jp_subcontractorbillitem`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_subcontractor_exptype`
--
ALTER TABLE `jp_subcontractor_exptype`
  MODIFY `exptype_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_subcontractor_payment`
--
ALTER TABLE `jp_subcontractor_payment`
  MODIFY `payment_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_subcontractor_permission`
--
ALTER TABLE `jp_subcontractor_permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_tasks`
--
ALTER TABLE `jp_tasks`
  MODIFY `tskid` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_tax_slabs`
--
ALTER TABLE `jp_tax_slabs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `jp_unit`
--
ALTER TABLE `jp_unit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `jp_unit_conversion`
--
ALTER TABLE `jp_unit_conversion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_users`
--
ALTER TABLE `jp_users`
  MODIFY `userid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `jp_user_roles`
--
ALTER TABLE `jp_user_roles`
  MODIFY `id` tinyint(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `jp_vendors`
--
ALTER TABLE `jp_vendors`
  MODIFY `vendor_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_vendor_exptype`
--
ALTER TABLE `jp_vendor_exptype`
  MODIFY `exp_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_vouchers`
--
ALTER TABLE `jp_vouchers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_voucher_items`
--
ALTER TABLE `jp_voucher_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_warehouse`
--
ALTER TABLE `jp_warehouse`
  MODIFY `warehouse_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_warehousedespatch`
--
ALTER TABLE `jp_warehousedespatch`
  MODIFY `warehousedespatch_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_warehousedespatch_items`
--
ALTER TABLE `jp_warehousedespatch_items`
  MODIFY `item_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_warehousereceipt`
--
ALTER TABLE `jp_warehousereceipt`
  MODIFY `warehousereceipt_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_warehousereceipt_items`
--
ALTER TABLE `jp_warehousereceipt_items`
  MODIFY `item_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_warehousestock`
--
ALTER TABLE `jp_warehousestock`
  MODIFY `warehousestock_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_warehouse_category`
--
ALTER TABLE `jp_warehouse_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_warehouse_stock_correction`
--
ALTER TABLE `jp_warehouse_stock_correction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_warehouse_transfer_deleted_log`
--
ALTER TABLE `jp_warehouse_transfer_deleted_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_worktype_template`
--
ALTER TABLE `jp_worktype_template`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `jp_work_type`
--
ALTER TABLE `jp_work_type`
  MODIFY `wtid` smallint(6) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jp_writeoff`
--
ALTER TABLE `jp_writeoff`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pms_assignpro_work_type`
--
ALTER TABLE `pms_assignpro_work_type`
  MODIFY `pro_wrk_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pms_department`
--
ALTER TABLE `pms_department`
  MODIFY `dept_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `project_assign`
--
ALTER TABLE `project_assign`
  MODIFY `paid` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `withdrawals`
--
ALTER TABLE `withdrawals`
  MODIFY `exp_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `jp_clients`
--
ALTER TABLE `jp_clients`
  ADD CONSTRAINT `clients_projtype_fk` FOREIGN KEY (`project_type`) REFERENCES `jp_project_type` (`ptid`) ON UPDATE CASCADE;

--
-- Constraints for table `jp_dailyexpense`
--
ALTER TABLE `jp_dailyexpense`
  ADD CONSTRAINT `dailyexpense_bank_fk` FOREIGN KEY (`bank_id`) REFERENCES `jp_bank` (`bank_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `dailyexpense_company_fk` FOREIGN KEY (`company_id`) REFERENCES `jp_company` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `dailyexpense_employee_fk` FOREIGN KEY (`user_id`) REFERENCES `jp_users` (`userid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `dailyexpense_users_fk` FOREIGN KEY (`user_id`) REFERENCES `jp_users` (`userid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `dailyexpense_vendors_fk` FOREIGN KEY (`vendor_id`) REFERENCES `jp_vendors` (`vendor_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `transaction_parent_ref_id` FOREIGN KEY (`transaction_parent`) REFERENCES `jp_dailyexpense` (`dailyexp_id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `jp_menu_permissions`
--
ALTER TABLE `jp_menu_permissions`
  ADD CONSTRAINT `fk_menu_id` FOREIGN KEY (`menu_id`) REFERENCES `jp_menu` (`menu_id`) ON UPDATE CASCADE;

--
-- Constraints for table `jp_pre_dailyreport`
--
ALTER TABLE `jp_pre_dailyreport`
  ADD CONSTRAINT `company_id_fk` FOREIGN KEY (`company_id`) REFERENCES `jp_company` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `created_by_fk` FOREIGN KEY (`created_by`) REFERENCES `jp_users` (`userid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `projectid_fk` FOREIGN KEY (`projectid`) REFERENCES `jp_projects` (`pid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `ref_id_fk` FOREIGN KEY (`ref_id`) REFERENCES `jp_dailyreport` (`dr_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `subcontractor_id` FOREIGN KEY (`subcontractor_id`) REFERENCES `jp_subcontractor` (`subcontractor_id`) ON UPDATE CASCADE;

--
-- Constraints for table `jp_pre_dailyvendors`
--
ALTER TABLE `jp_pre_dailyvendors`
  ADD CONSTRAINT `dailyexpense_bank_fk1` FOREIGN KEY (`bank`) REFERENCES `jp_bank` (`bank_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `dailyexpense_cancelledby_fk` FOREIGN KEY (`cancelled_by`) REFERENCES `jp_users` (`userid`),
  ADD CONSTRAINT `dailyexpense_company_fk3` FOREIGN KEY (`company_id`) REFERENCES `jp_company` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `dailyexpense_createdby_fk` FOREIGN KEY (`created_by`) REFERENCES `jp_users` (`userid`),
  ADD CONSTRAINT `dailyexpense_project_fk` FOREIGN KEY (`project_id`) REFERENCES `jp_projects` (`pid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `dailyexpense_updatedby_fk` FOREIGN KEY (`updated_by`) REFERENCES `jp_users` (`userid`),
  ADD CONSTRAINT `dailyexpense_vendor_fk` FOREIGN KEY (`vendor_id`) REFERENCES `jp_vendors` (`vendor_id`) ON UPDATE CASCADE;

--
-- Constraints for table `jp_pre_expenses`
--
ALTER TABLE `jp_pre_expenses`
  ADD CONSTRAINT `exp_bill_fk` FOREIGN KEY (`bill_id`) REFERENCES `jp_bills` (`bill_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `exp_cancelled_by_fk` FOREIGN KEY (`cancelled_by`) REFERENCES `jp_users` (`userid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `exp_created_by_fk` FOREIGN KEY (`created_by`) REFERENCES `jp_users` (`userid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `exp_inv_fk` FOREIGN KEY (`invoice_id`) REFERENCES `jp_invoice` (`invoice_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `exp_projectid_fk` FOREIGN KEY (`projectid`) REFERENCES `jp_projects` (`pid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `exp_updated_by_fk` FOREIGN KEY (`updated_by`) REFERENCES `jp_users` (`userid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `exp_user_fk` FOREIGN KEY (`userid`) REFERENCES `jp_users` (`userid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `exp_vendor_fk` FOREIGN KEY (`vendor_id`) REFERENCES `jp_vendors` (`vendor_id`) ON UPDATE CASCADE;

--
-- Constraints for table `jp_projects`
--
ALTER TABLE `jp_projects`
  ADD CONSTRAINT `clientid_fk` FOREIGN KEY (`client_id`) REFERENCES `jp_clients` (`cid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `createdby_fk` FOREIGN KEY (`created_by`) REFERENCES `jp_users` (`userid`) ON UPDATE CASCADE;

--
-- Constraints for table `jp_purchase`
--
ALTER TABLE `jp_purchase`
  ADD CONSTRAINT `projectfk` FOREIGN KEY (`project_id`) REFERENCES `jp_projects` (`pid`) ON UPDATE CASCADE;

--
-- Constraints for table `jp_quotation_item_master`
--
ALTER TABLE `jp_quotation_item_master`
  ADD CONSTRAINT `jp_quotation_item_master_ibfk_1` FOREIGN KEY (`quotation_category_id`) REFERENCES `jp_quotation_category_master` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `jp_quotation_item_master_ibfk_10` FOREIGN KEY (`finish_id`) REFERENCES `jp_quotation_finish_master` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `jp_quotation_item_master_ibfk_11` FOREIGN KEY (`updated_by`) REFERENCES `jp_users` (`userid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `jp_quotation_item_master_ibfk_2` FOREIGN KEY (`work_type_id`) REFERENCES `jp_quotation_worktype_master` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `jp_quotation_item_master_ibfk_3` FOREIGN KEY (`shutter_finish_id`) REFERENCES `jp_quotation_finish_master` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `jp_quotation_item_master_ibfk_6` FOREIGN KEY (`carcass_finish_id`) REFERENCES `jp_quotation_finish_master` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `jp_quotation_item_master_ibfk_7` FOREIGN KEY (`created_by`) REFERENCES `jp_users` (`userid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `jp_quotation_item_master_ibfk_8` FOREIGN KEY (`quotation_category_id`) REFERENCES `jp_quotation_category_master` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `jp_quotation_item_master_ibfk_9` FOREIGN KEY (`work_type_id`) REFERENCES `jp_quotation_worktype_master` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `jp_quotation_worktype_master`
--
ALTER TABLE `jp_quotation_worktype_master`
  ADD CONSTRAINT `jp_quotation_worktype_master_ibfk_1` FOREIGN KEY (`template_id`) REFERENCES `jp_worktype_template` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `jp_sales_quotation`
--
ALTER TABLE `jp_sales_quotation`
  ADD CONSTRAINT `jp_sales_quotation_ibfk_2` FOREIGN KEY (`category_id`) REFERENCES `jp_quotation_category_master` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `jp_sales_quotation_ibfk_3` FOREIGN KEY (`work_type`) REFERENCES `jp_quotation_worktype_master` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `jp_sales_quotation_ibfk_4` FOREIGN KEY (`unit`) REFERENCES `jp_unit` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `jp_sales_quotation_ibfk_5` FOREIGN KEY (`created_by`) REFERENCES `jp_users` (`userid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `jp_sales_quotation_ibfk_6` FOREIGN KEY (`item_id`) REFERENCES `jp_quotation_item_master` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `jp_sales_quotation_master`
--
ALTER TABLE `jp_sales_quotation_master`
  ADD CONSTRAINT `jp_sales_quotation_master_ibfk_1` FOREIGN KEY (`company_id`) REFERENCES `jp_company` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `jp_sales_quotation_master_ibfk_4` FOREIGN KEY (`created_by`) REFERENCES `jp_users` (`userid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `jp_sales_quotation_master_ibfk_5` FOREIGN KEY (`location_id`) REFERENCES `jp_location` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `jp_sales_quotation_master_ibfk_6` FOREIGN KEY (`sales_executive_id`) REFERENCES `jp_salesexecutive` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `jp_specification`
--
ALTER TABLE `jp_specification`
  ADD CONSTRAINT `jp_specification_ibfk_1` FOREIGN KEY (`cat_id`) REFERENCES `jp_purchase_category` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `jp_specification_ibfk_2` FOREIGN KEY (`brand_id`) REFERENCES `jp_brand` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `jp_specification_ibfk_3` FOREIGN KEY (`created_by`) REFERENCES `jp_users` (`userid`) ON UPDATE SET NULL;

--
-- Constraints for table `jp_users`
--
ALTER TABLE `jp_users`
  ADD CONSTRAINT `jp_users_client_id_fk` FOREIGN KEY (`client_id`) REFERENCES `jp_clients` (`cid`),
  ADD CONSTRAINT `users_type_fk` FOREIGN KEY (`user_type`) REFERENCES `jp_user_roles` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `jp_worktype_template`
--
ALTER TABLE `jp_worktype_template`
  ADD CONSTRAINT `jp_worktype_template_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `jp_users` (`userid`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
