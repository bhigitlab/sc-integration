<?php
/* @var $this ExpensesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'Receipt',
);
?>
<div class="container" id="receipt">
    <h2>Receipt</h2>

    <?php //$this->renderPartial('_newsearch', array('model' => $model))  ?>

    <div class="add-btn">
        <?php if (Yii::app()->user->role <= 2) { ?>
            <button data-toggle="modal" data-target="#addNew2" class="createreceipt">Add Receipt</button>
        <?php } ?>
    </div>

    <div class="exp-list">
        <?php
        $this->widget('zii.widgets.CListView', array(
            'dataProvider' => $dataProvider,
            'itemView' => '_newview',
        ));
        ?>
        <!-- Add New Popup -->

        <div id="addNew2" class="modal fade" role="dialog">

        </div>
        <!-- edit  Popup -->
        <div class="modal fade edit" role="dialog">

        </div>
        <script>
            $(document).ready(function () {


                $('.createreceipt').click(function () {
                    $.ajax({
                        type: "GET",
                        url: "<?php echo $this->createUrl('receipt/create') ?>",
                        success: function (response)
                        {
                            $("#addNew2").html(response);

                        }
                    });
                });




                $('.editReceipt').click(function () {
                    // alert("hai");
                    var id = $(this).attr('data-id');
                    //   alert(id);

                    $.ajax({
                        type: "GET",
                        url: "<?php echo $this->createUrl('receipt/update&id=') ?>" + id,
                        success: function (response)
                        {
                            $(".edit").html(response);

                        }
                    });

                });
                function popUpClosed() {
                    window.location.reload();
                }
            });
        </script>




    </div>
</div>