<?php
/* @var $this UsersController */
/* @var $model Users */

$this->breadcrumbs = array(
    'My profile'
);
?>

<h1>My Profile</h1>
<div class="form">

    <?php
    $flash_message = Yii::app()->user->getFlashes();
    foreach ($flash_message as $key => $message) {
        echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
    }
    ?>
    <div class="row">
        <div class="subrow1"> 
            <div class="row"> <?php echo "<strong>Profile type: </strong>" . $model->userType->role; ?></div> 
            <div class="row"><?php echo "<strong>Username: </strong>" . $model->username; ?> </div>
            <div class="row"><?php echo "<strong>Full name: </strong>" . $model->first_name . " " . $model->last_name; ?> </div>
            <div class="row"><?php echo "<strong>Email id: </strong>" . $model->email; ?> </div>    
        </div>
        <div class="subrow2">
            <?php echo CHtml::link('Edit profile', array('users/editprofile')); ?>
            <?php echo CHtml::link('Change my password', array('users/passchange')); ?>
        </div>
    </div>
    <br clear='all'/>

</div>


