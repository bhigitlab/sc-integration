INSERT INTO `jp_menu` (`menu_id`, `menu_name`, `parent_id`, `status`, `controller`, `action`, `params`, `showmenu`, `show_list`, `related`) VALUES (767, 'Warehouse', '704', '0', 'wh/warehouse', 'stockcorrection', '', '2', '1', NULL);
-- change query

CREATE TABLE `jp_warehouse_stock_correction` ( `id` INT NOT NULL AUTO_INCREMENT , `warehouse_id` INT NOT NULL , `wh_receipt_id` INT NOT NULL , `wh_receipt_item_id` INT NOT NULL, `Item_Id` INT NOT NULL , `wh_receipt_quantity` FLOAT NOT NULL , `stock_quantity` FLOAT NOT NULL , `created_by` INT NOT NULL , `created_date` DATETIME NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;

INSERT INTO `jp_menu` (`menu_id`, `menu_name`, `parent_id`, `status`, `controller`, `action`, `params`, `showmenu`, `show_list`, `related`) VALUES ('768', 'Stock Correction Report', '561', '0', 'report/reports', 'stockCorrectionReport', '', '2', '1', NULL);


INSERT INTO `jp_menu` (`menu_id`, `menu_name`, `parent_id`, `status`, `controller`, `action`, `params`, `showmenu`, `show_list`, `related`) VALUES (769, 'Defect Return', '704', '0', 'wh/warehouse', 'defectreturn', '', '2', '1', NULL);


CREATE TABLE `defect_return` ( `return_id` INT NOT NULL AUTO_INCREMENT , `receipt_id` INT(11) NULL DEFAULT NULL , `return_number` TEXT NOT NULL , `return_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP , `return_amount` FLOAT NOT NULL  , `company_id` INT(11) NOT NULL , `created_by` INT(11) NOT NULL , `created_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP , `updated_by` INT(11) NULL DEFAULT NULL , `updated_date` TIMESTAMP NULL DEFAULT NULL , `return_status` INT(11) NULL DEFAULT NULL , PRIMARY KEY (`return_id`)) ENGINE = InnoDB;

CREATE TABLE `jp_defect_returnitem` ( `returnitem_id` INT(11) NOT NULL AUTO_INCREMENT , `return_id` INT(11) NOT NULL , `receiptitem_id` INT(11) NULL DEFAULT NULL , `returnitem_description` MEDIUMTEXT NULL DEFAULT NULL , `returnitem_quantity` FLOAT NOT NULL , `returnitem_unit` VARCHAR(20) NOT NULL , `returnitem_rate` FLOAT NOT NULL , `returnitem_amount` FLOAT NOT NULL ,  `category_id` INT NULL DEFAULT NULL , `remark` VARCHAR(100) NULL DEFAULT NULL ,  `created_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP , `created_by` INT(11) NOT NULL , PRIMARY KEY (`returnitem_id`)) ENGINE = InnoDB;

ALTER TABLE `jp_warehouse_stock_correction` ADD `rate` FLOAT(11,2) NOT NULL AFTER `stock_quantity`;

