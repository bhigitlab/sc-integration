
ALTER TABLE `jp_itemestimation` ADD `remarks` VARCHAR(255) NULL AFTER `itemestimation_status`;
ALTER TABLE `jp_itemestimation` CHANGE `itemestimation_status` `itemestimation_status` INT(11) NOT NULL COMMENT 'deafult=1 ,approve=2,quantity change=3,reject=4';

DELETE FROM jp_itemestimation WHERE itemestimation_id NOT IN ( SELECT MIN(itemestimation_id) FROM jp_itemestimation GROUP BY project_id );

ALTER TABLE jp_itemestimation ADD pms_project_id INT(11) NULL AFTER itemestimation_status, ADD pms_estimation_id INT(11)  NULL AFTER pms_project_id;
ALTER TABLE jp_itemestimation CHANGE project_id project_id INT(11) NULL;

ALTER TABLE jp_material_estimation ADD estimation_id INT(11) NOT NULL AFTER id;

ALTER TABLE jp_material_estimation
MODIFY estimation_id INT DEFAULT 0 AFTER id;

UPDATE jp_material_estimation as m SET estimation_id = ( SELECT i.itemestimation_id FROM jp_itemestimation as i WHERE i.project_id = m.project_id);


ALTER TABLE jp_material_estimation ADD CONSTRAINT fk_material_estimation FOREIGN KEY (estimation_id) REFERENCES jp_itemestimation(itemestimation_id) ON DELETE CASCADE ON UPDATE NO ACTION;

ALTER TABLE jp_material_estimation ADD pms_material_id INT(11) NULL AFTER remarks, ADD pms_unit_id INT(11) NULL AFTER pms_material_id;
ALTER TABLE jp_material_estimation CHANGE project_id project_id INT(11) NULL;


ALTER TABLE jp_labour_estimation ADD estimation_id INT(11) NOT NULL AFTER id;

ALTER TABLE jp_labour_estimation
MODIFY estimation_id INT DEFAULT 0 AFTER id;

UPDATE jp_labour_estimation as m SET estimation_id = ( SELECT i.itemestimation_id FROM jp_itemestimation as i WHERE i.project_id = m.project_id);

ALTER TABLE jp_labour_estimation ADD CONSTRAINT fk_labour_estimation FOREIGN KEY (estimation_id) REFERENCES jp_itemestimation(itemestimation_id) ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE jp_labour_estimation ADD pms_labour_id INT(11) NULL AFTER remarks, ADD pms_unit_id INT(11) NULL AFTER pms_labour_id;
ALTER TABLE jp_labour_estimation CHANGE project_id project_id INT(11) NULL;


ALTER TABLE jp_equipments_estimation ADD estimation_id INT(11) NOT NULL AFTER id;

ALTER TABLE jp_equipments_estimation
MODIFY estimation_id INT DEFAULT 0 AFTER id;

UPDATE jp_equipments_estimation as m SET estimation_id = ( SELECT i.itemestimation_id FROM jp_itemestimation as i WHERE i.project_id = m.project_id);
ALTER TABLE jp_equipments_estimation ADD CONSTRAINT fk_equipments_estimation FOREIGN KEY (estimation_id) REFERENCES jp_itemestimation(itemestimation_id) ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE jp_equipments_estimation ADD pms_equipment_id INT(11) NULL AFTER remarks, ADD pms_unit_id INT(11) NULL AFTER pms_equipment_id;
ALTER TABLE jp_equipments_estimation CHANGE project_id project_id INT(11) NULL;