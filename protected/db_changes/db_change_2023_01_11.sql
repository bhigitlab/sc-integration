ALTER TABLE `jp_purchase` ADD `decline_comment` TEXT NULL DEFAULT NULL AFTER `expected_delivery_date`;
ALTER TABLE `jp_pre_dailyexpenses` ADD `petty_payment_approval` ENUM('0','1') NOT NULL DEFAULT '1' COMMENT '0=>Not Approved , 1=>Approved' AFTER `approve_notify_status`;

ALTER TABLE `jp_pre_expenses` ADD `petty_payment_approval` ENUM('0','1') NOT NULL DEFAULT '1' COMMENT '0=>Not Approved , 1=>Approved' AFTER `approve_notify_status`;

ALTER TABLE `jp_subcontractor_payment` ADD `employee_id` INT(11) NULL DEFAULT NULL AFTER `duplicate_delete_status`;

ALTER TABLE `jp_pre_subcontractor_payment` ADD `employee_id` INT(11) NULL DEFAULT NULL AFTER `approve_notify_status`;
