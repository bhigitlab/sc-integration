
ALTER TABLE `jp_dailyvendors` ADD `approval_status` ENUM('0','1') NOT NULL DEFAULT '1'
 COMMENT '0=>Pending for approval, 1=>Approved' AFTER `budget_percentage`;

ALTER TABLE `jp_dailyreport` ADD `approval_status` ENUM('0','1') NOT NULL DEFAULT '1' 
 COMMENT '0=>Pending for approval, 1=>Approved' AFTER `helper_wage`;

ALTER TABLE `jp_dailyexpense` ADD `approval_status` ENUM('0','1') NOT NULL DEFAULT '1' 
 COMMENT '0=>Pending for approval, 1=>Approved';

ALTER TABLE `jp_subcontractor_payment` ADD `approval_status` ENUM('0','1') NOT NULL DEFAULT '1' 
 COMMENT '0=>Pending for approval, 1=>Approved ';

