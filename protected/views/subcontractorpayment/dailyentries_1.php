<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<!--<script type="text/javascript" src="<?php //echo Yii::app()->theme->baseUrl; ?>/js/select2.min.js"></script>-->
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.js"></script>

    <?php
$this->breadcrumbs=array(
	'Subcontractors'=>array('index'),
	$model->subcontractor_id=>array('view','id'=>$model->subcontractor_id),
	'Update',
);
$user = Users::model()->findByPk(Yii::app()->user->id);
$arrVal = explode(',', $user->company_id);
$newQuery1 = "";
$newQuery = "";
foreach($arrVal as $arr) {
    if ($newQuery1) $newQuery1 .= ' OR';
    if ($newQuery) $newQuery .= ' OR';
    $newQuery1 .= " FIND_IN_SET('".$arr."', company_id)";
    $newQuery .= " FIND_IN_SET('".$arr."', id)";
}
?>
<style type="text/css">
    *:focus{
		border:1px solid #333;
		box-shadow:0 0 6px 1px rgba(0,0,0,0.25);
	}
        .permission_style {
            background: #f8cbcb !important;
        }
        .permission_style td:first-child{background: #f8cbcb !important;}
        .tooltip-hiden{width:auto;}
        #loader{ display: none; }
        .row_block .elem_block.gsts input{
                width: 100px;
        }
</style>
<div class="container" id="expense">
    <div class="expenses-heading">
        <div class="clearfix">
            <?php if((isset(Yii::app()->user->role) && (in_array('/subcontractorpayment/paymentcreate', Yii::app()->user->menuauthlist)))){ ?>
        <button type="button" class="btn btn-info pull-right addentries collapsed" data-toggle="collapse" data-target="#dailyform"></button>
            <?php } ?>
        <h2>SUBCONTRACTOR PAYMENT ENTRIES</h2>
        </div>
    </div>
    <div class="daybook form">
        <?php $form=$this->beginWidget('CActiveForm', array(
            'id'=>'dailyexpense-form',
            // Please note: When you enable ajax validation, make sure the corresponding
            // controller action is handling ajax validation correctly.
            // There is a call to performAjaxValidation() commented in generated controller code.
            // See class documentation of CActiveForm for details on this.
            'enableAjaxValidation'=>false,
        )); ?>
            <div class="clearfix">
                <div class="datepicker">
                    <label>Entry Date: </label>
                    <?php echo CHtml::activeTextField($model, 'date', array('style' => 'height:25px;', 'size' => 10, 'readonly' => true, 'id' => 'SubcontractorPayment_date', 'value' => date('d-m-Y'), 'onchange' => 'changedate()')); ?>                    
                    <a href="#" id="previous" class="link">Previous</a> | 
                    <a href="#" id="current" class="link">Current</a> | 
                    <a href="#" id="next" class="link">Next</a>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <?php
                    $tblpx        = Yii::app()->db->tablePrefix;
                    $expenseData  = Yii::app()->db->createCommand("select MAX(created_date) as entry_date FROM " . $tblpx . "subcontractor_payment")->queryRow();
                    ?>
                    <div class="pull-right"><span id="lastentry">Last Entry date : <span id="entrydate"><?php echo date("d-m-Y", strtotime($expenseData["entry_date"])); ?></span></span></div>
                </div>
            </div>
            <!--<div class="block_hold shdow_box clearfix collapse" id="dailyform">    
            <div class="row_block">
                <h4>Transaction Details</h4>
                
            <div class="elem_block">
                <div class="form-group">
                    <label for="vendor">Company</label>
                        <?php
                    echo $form->dropDownList($model, 'company_id', CHtml::listData(Company::model()->findAll(array(
                        'select' => array('id, name'),
                        'order' => 'id',
                        'condition' => '('.$newQuery.')',
                        'distinct' => true,
                        )), 'id', 'name'), array('class'=>'form-control js-example-basic-single','empty' => '-Select Company-'));
                    ?>
                    
                </div>
            </div>
                
            <div class="elem_block">
                <div class="form-group">
                    <label for="vendor">Subcontractor</label>
                        <?php
                    echo $form->dropDownList($model, 'subcontractor_id', CHtml::listData(Subcontractor::model()->findAll(array(
                        'select' => array('subcontractor_id, subcontractor_name'),
                        'order' => 'subcontractor_id',
                        'condition' => '('.$newQuery1.')',
                        'distinct' => true,
                        )), 'subcontractor_id', 'subcontractor_name'), array('class'=>'form-control js-example-basic-single','empty' => '-Select Subcontractor-'));
                    ?>
                    
                </div>
            </div>
            
            <div class="elem_block">
                <div class="form-group">
                    <label for="vendor">Project</label>
                        <?php
                    echo $form->dropDownList($model, 'project_id', CHtml::listData(Projects::model()->findAll(array(
                        'select' => array('pid, name'),
                        'order' => 'pid',
                        'condition' => '('.$newQuery1.')',
                        'distinct' => true,
                        )), 'pid', 'name'), array('class'=>'form-control js-example-basic-single','empty' => '-Select Project-'));
                    ?>
                    
                </div>
            </div>
            
             <div class="elem_block">
                <div class="form-group">
                    <label for="receiptType">Transaction Type</label>
                    <?php
                    echo $form->dropDownList($model, 'payment_type', CHtml::listData(Status::model()->findAll(array(
                        'select' => array('sid, caption'),
                        "condition" => 'status_type = "payment_type" AND sid != 103',
                        'order' => 'sid',
                        'distinct' => true,
                        )), 'sid', 'caption'), array('class'=>'form-control js-example-basic-single','empty' => '-Select Transaction Type-'));
                    ?>
                </div>
            </div>
         
        <div class="elem_block hiden_box" style="display:none;">
            <div class="elem_block">
                <div class="form-group">
                    <label for="bank">Bank</label>
                    <?php
                    echo $form->dropDownList($model, 'bank', CHtml::listData(Bank::model()->findAll(array(
                        'select' => array('bank_id, bank_name'),
                        'order' => 'bank_id ASC',
                        'condition' => '('.$newQuery1.')',
                        'distinct' => true,
                        )), 'bank_id', 'bank_name'), array('class'=>'form-control js-example-basic-single','empty' => '-Select Bank-'));
                    ?>
                </div>
            </div>
            </div>
             <div class="elem_block hiden_box" style="display:none;">
            <div class="elem_block">
                <div class="form-group">
                    <label for="cheque">Cheque No</label>
                    <input type="text" class="form-control" id="SubcontractorPayment_cheque_no" name="SubcontractorPayment[cheque_no]"/>
                </div>
            </div>
            </div>
            
            </div>
            
            <div class="row_block daybook-inner">
                 <h4>Amount Details</h4>
                <div class="elem_block">
                    <div class="form-group">
                        <label for="cheque">Amount</label>
                        <input type="text" class="form-control amountvalidation" id="SubcontractorPayment_amount" name="SubcontractorPayment[amount]"/>
                    </div>
                </div>

                <div class="elem_block gsts">
                    <div class="form-group">
                        <label for="cheque">SGST %</label>
                        <input type="text" class="form-control amountvalidation percentage" id="SubcontractorPayment_sgst" name="SubcontractorPayment[sgst]"/>
                        <span  class="gstvalue" id="txtSgst1"></span>
                    </div>
                </div>

                <div class="elem_block amntblock">
                    <div class="form-group">
                        <label for="cheque"></label>
                        <input type="hidden" class="form-control amountvalidation" readonly="true" id="SubcontractorPayment_sgst_amount" name="SubcontractorPayment[sgst_amount]"/>
                    </div>
                </div>

                <div class="elem_block gsts">
                    <div class="form-group">
                        <label for="cheque">CGST %</label>
                        <input type="text" class="form-control amountvalidation percentage" id="SubcontractorPayment_cgst" name="SubcontractorPayment[cgst]"/>
                        <span  class="gstvalue" id="txtCgst1"></span>
                    </div>
                </div>

                <div class="elem_block amntblock">
                    <div class="form-group">
                        <label for="cheque"></label>
                        <input type="text" class="form-control amountvalidation" readonly="true" id="SubcontractorPayment_cgst_amount" name="SubcontractorPayment[cgst_amount]"/>
                    </div>
                </div>

                <div class="elem_block gsts">
                    <div class="form-group">
                        <label for="cheque">IGST %</label>
                        <input type="text" class="form-control amountvalidation percentage" id="SubcontractorPayment_igst" name="SubcontractorPayment[igst]"/>
                        <span  class="gstvalue" id="txtIgst1"></span>
                    </div>
                </div>

                <div class="elem_block amntblock">
                    <div class="form-group">
                        <label for="cheque"></label>
                        <input type="text" class="form-control amountvalidation" readonly="true" id="SubcontractorPayment_igst_amount" name="SubcontractorPayment[igst_amount]"/>
                    </div>
                </div>

                <div class="elem_block totalamnt">
                    <div class="form-group">
                        <label for="cheque"></label>
                        <input type="hidden" class="form-control amountvalidation" readonly="true" id="SubcontractorPayment_tax_amount" name="SubcontractorPayment[tax_amount]"/>
                        <span class="gstvalue" id="txtgstTotal"></span>
                        <span class="gstvalue" id="txtTotal1"></span>
                    </div>
                </div>
            </div>
            
           <div class="row_block">
                <h4>Other Details</h4>
                <div class="elem_block areahold">
                    <div class="form-group">
                        <label for="description">Description</label>
                        <textarea type="text" class="form-control" id="SubcontractorPayment_description" name="SubcontractorPayment[description]"></textarea>
                    </div>
                </div>
            </div>
            <div class="row_block">     
                <input type="hidden" name="SubcontractorPayment[txtPaymentId]" value="" id="txtPaymentId"/>
                <div class="elem_block submit-button">
                    <div class="form-group">
                        <label style="display:block;">&nbsp;</label>
                        <button type="button" class="btn btn-info" id="buttonsubmit">ADD</button>
                        <button type="button" class="btn" id="btnReset">RESET</button>
                        <div id="loader"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/ajax-loader.gif"></div>
                    </div>
                </div>
            </div>
            
        </div>   -->
            
            
        <div class="block_hold shdow_box clearfix collapse" id="dailyform">
            <div class="row_block ">
            <h4>Details</h4>
                <div class="elem_block">
                    <div class="form-group">
                        <label for="vendor">Company</label>
                            <?php
                        echo $form->dropDownList($model, 'company_id', CHtml::listData(Company::model()->findAll(array(
                            'select' => array('id, name'),
                            'order' => 'id',
                            'condition' => '('.$newQuery.')',
                            'distinct' => true,
                            )), 'id', 'name'), array('class'=>'form-control js-example-basic-single','empty' => '-Select Company-'));
                        ?>

                    </div>
                </div>
                <div class="elem_block">
                    <div class="form-group">
                        <label for="vendor">Subcontractor</label>
                            <?php
                        echo $form->dropDownList($model, 'subcontractor_id', CHtml::listData(Subcontractor::model()->findAll(array(
                            'select' => array('subcontractor_id, subcontractor_name'),
                            'order' => 'subcontractor_id',
                            'condition' => '('.$newQuery1.')',
                            'distinct' => true,
                            )), 'subcontractor_id', 'subcontractor_name'), array('class'=>'form-control js-example-basic-single','empty' => '-Select Subcontractor-'));
                        ?>
                    </div>
                </div>
                <div class="elem_block gsts">
                    <div class="form-group">
                        <label for="cheque">SGST %</label>
                        <input type="text" class="form-control amountvalidation percentage" id="SubcontractorPayment_sgst" name="SubcontractorPayment[sgst]"/>                        
                    </div>
                </div>
                <div class="elem_block gsts">
                    <div class="form-group">
                        <label for="cheque">CGST %</label>
                        <input type="text" class="form-control amountvalidation percentage" id="SubcontractorPayment_cgst" name="SubcontractorPayment[cgst]"/>                   
                    </div>
                </div>
                <div class="elem_block gsts">
                    <div class="form-group">
                        <label for="cheque">IGST %</label>
                        <input type="text" class="form-control amountvalidation percentage" id="SubcontractorPayment_igst" name="SubcontractorPayment[igst]"/>
                    </div>
                </div>
            </div>
            <div class="row_block daybook-inner">
                <h4>Amount Details</h4>
                <div class="elem_block">
                    <div class="form-group">
                        <label for="vendor">Project</label>
                            <?php
                        echo $form->dropDownList($model, 'project_id', CHtml::listData(Projects::model()->findAll(array(
                            'select' => array('pid, name'),
                            'order' => 'pid',
                            'condition' => '('.$newQuery1.')',
                            'distinct' => true,
                            )), 'pid', 'name'), array('class'=>'form-control js-example-basic-single','empty' => '-Select Project-'));
                        ?>

                    </div>
                </div>
                <div class="elem_block">
                    <div class="form-group">
                        <label>Amount</label>
                        <input type="text" class="form-control amountvalidation sub_paymentamount" id="SubcontractorPayment_amount" name="SubcontractorPayment[amount]"/>
                    </div>
                </div>
                <div class="elem_block gsts">
                    <div class="form-group">
                        <label>SGST</label>
                        <span  class="gstvalue" id="txtSgst1"></span>
                    </div>
                </div>
                <div class="elem_block gsts">
                    <div class="form-group">
                        <label>CGST</label>
                        <span  class="gstvalue" id="txtCgst1"></span>
                    </div>
                </div>
                <div class="elem_block gsts">
                    <div class="form-group">
                        <label>IGST</label>
                        <span  class="gstvalue" id="txtIgst1"></span>
                    </div>
                </div>
                <div class="elem_block totalamnt">
                    <div class="form-group">
                        <label></label>
                        <input type="hidden" class="form-control amountvalidation" readonly="true" id="SubcontractorPayment_tax_amount" name="SubcontractorPayment[tax_amount]"/>
                        <span class="gstvalue" id="txtgstTotal"></span>
                        <span class="gstvalue" id="txtTotal1"></span>
                    </div>
                </div>
            </div>
            <div class="addmoresection"></div>
            <div class="row_block daybook-inner">
                <h4>Transaction Details</h4>
                <div class="elem_block gsts">
                    <div class="form-group">
                        <label>Grand Total</label>
                        <span  class="gstvalue" style="display:block" id="txtgrandtot">0.00</span>
                    </div>
                </div>
                <div class="elem_block">
                    <div class="form-group">
                        <label for="receiptType">Transaction Type</label>
                        <?php
                        echo $form->dropDownList($model, 'payment_type', CHtml::listData(Status::model()->findAll(array(
                            'select' => array('sid, caption'),
                            "condition" => 'status_type = "payment_type" AND sid != 103',
                            'order' => 'sid',
                            'distinct' => true,
                            )), 'sid', 'caption'), array('class'=>'form-control js-example-basic-single','empty' => '-Select Transaction Type-'));
                        ?>
                    </div>
                </div>
                <div class="elem_block hiden_box" style="display:none;">
                    <div class="elem_block">
                        <div class="form-group">
                            <label for="bank">Bank</label>
                            <?php
                            echo $form->dropDownList($model, 'bank', CHtml::listData(Bank::model()->findAll(array(
                                'select' => array('bank_id, bank_name'),
                                'order' => 'bank_id ASC',
                                'condition' => '('.$newQuery1.')',
                                'distinct' => true,
                                )), 'bank_id', 'bank_name'), array('class'=>'form-control js-example-basic-single','empty' => '-Select Bank-'));
                            ?>
                        </div>
                    </div>
                </div>
                <div class="elem_block hiden_box" style="display:none;">
                    <div class="elem_block">
                        <div class="form-group">
                            <label for="cheque">Cheque No</label>
                            <input type="text" class="form-control" id="SubcontractorPayment_cheque_no" name="SubcontractorPayment[cheque_no]"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row_block">
                <h4>Other Details</h4>
                <div class="elem_block areahold">
                    <div class="form-group">
                        <label for="description">Description</label>
                        <textarea type="text" class="form-control" id="SubcontractorPayment_description" name="SubcontractorPayment[description]"></textarea>
                    </div>
                </div>
            </div>
            <div class="row_block">     
                <input type="hidden" name="SubcontractorPayment[txtPaymentId]" value="" id="txtPaymentId"/>
                <div class="elem_block submit-button">
                    <div class="form-group">
                        <label style="display:block;">&nbsp;</label>
                        <button type="button" class="btn btn-info" id="buttonsubmit">ADD</button>
                        <button type="button" class="btn" id="btnReset">RESET</button>
                        <div id="loader"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/ajax-loader.gif"></div>
                    </div>
                </div>
            </div>
        </div>
            <input type="hidden" name="SubcontractorPayment[approve_status]" id="approve_status" value="Yes">
            <?php $this->endWidget(); ?>
    </div>
    <div class="">
        <div class="">
            <div id="errormessage"></div>
        </div>
    </div>
    <div class="daybook">
        <div class="">
            <div id="daybook-entry">
                <?php  $this->renderPartial('dailyentries_list',array('newmodel'=>$newmodel,)); ?>
            </div>
        </div>
    </div>  
</div>
<?php //$dropdownUrl = Yii::app()->createAbsoluteUrl("expenses/dynamicDropdown");?>
<?php $projectUrl   = Yii::app()->createAbsoluteUrl("subcontractorpayment/dynamicProject");?>
<?php $subcontractorUrl   = Yii::app()->createAbsoluteUrl("subcontractorpayment/dynamicSubcontractor");?>
<?php $getUrl      = Yii::app()->createAbsoluteUrl("subcontractorpayment/getDataByDate");?>
<?php $payment_url  = Yii::app()->createAbsoluteUrl("subcontractorpayment/getTotalFromPayment");?>
<?php $getSubContractor  = Yii::app()->createAbsoluteUrl("subcontractorpayment/getPaymentDetails");?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-dateFormat/1.0/jquery.dateFormat.js"></script>
<?php 
$company = Company::model()->findByPk(Yii::app()->user->company_id);
?>
<script>
    
    var secionNode = '<div class="row_block">'+
                        '<div class="elem_block">'+ 
                            '<div class="form-group">'+  
                                '<label for="vendor">Project</label>'+
                                '<select class="form-control js-example-basic-single multi_projects" name="project_name[]" id="" onchange="myfunc(this)" onclick="exithere(this)">'+'\
                                    <option  value="">-Select Project-</option>'+
                                    '<?php 
                                        $prolist = Projects::model()->findAll(array(
                                        'select' => array('pid, name'),
                                        'order' => 'pid',
                                        'condition' => '('.$newQuery1.')',
                                        'distinct' => true,
                                        ));    
                                        if(!empty($prolist)){
                                                foreach ($prolist as $pro){
                                                    echo '<option value="'.$pro['pid'].'" >'.$pro['name'].'</option>';
                                                }
                                            } ?>'+
                                '</select>'+                            
                            '</div>'+
                        '</div>'+
                        '<div class="elem_block">'+ 
                            '<div class="form-group">'+  
                                  '<label>Amount</label>'+
                                  '<input type="text" class="form-control amountvalidation sub_paymentamount" name="SubcontractorPaymentamount[]" onkeypress ="nextSectionode(this, event)"/>'+                     
                            '</div>'+
                        '</div>'+
                        '<div class="elem_block gsts">'+
                            '<div class="form-group">'+
                                '<label>SGST</label>'+
                                '<span  class="gstvalue"></span>'+
                            '</div>'+
                        '</div>'+
                        '<div class="elem_block gsts">'+
                            '<div class="form-group">'+
                                '<label>CGST</label>'+
                                '<span  class="gstvalue"></span>'+
                            '</div>'+
                        '</div>'+
                        '<div class="elem_block gsts">'+
                            '<div class="form-group">'+
                                '<label>IGST</label>'+
                                '<span  class="gstvalue"></span>'+
                            '</div>'+
                        '</div>'+
                        '<div class="elem_block totalamnt">'+
                            '<div class="form-group">'+
                                '<label></label>'+
                                '<span class="gstvalue" style="display:inline-block;"><b>Total Tax:</b></span>'+
                                '<span class="gstvalue" style="display:inline-block;"><b>Total:</b></span>'+
                            '</div>'+
                        '</div>'+
                    '</div>';
         
    
    
    $(".sub_paymentamount").blur(function() {
        $('.addmoresection').append(secionNode);
           $(".multi_projects").focus();
    });
    function myfunc(elm){
        $(elm).closest(".row_block").find('input').focus();
    }    
    function nextSectionode(elm, e){
        if (!e) e = window.event;
        var keyCode = e.keyCode || e.which;
        if (keyCode == '13'){
            $('.addmoresection').append(secionNode);
            $(elm).parents().next().find('.multi_projects').focus();
        } 
    }   
    function exithere(elm){
        if($(elm).closest(".row_block").find('.multi_projects').val() == '' )
        {
            $("#SubcontractorPayment_payment_type").focus();
        }
        else{
            $(elm).closest(".row_block").find('input').focus();
        }
        
    }
        $(".js-example-basic-single").select2();
	var limit = '<?php echo $company['subcontractor_limit']; ?>';
		$(document).ready(function(){
			$('#dailyform').on('shown.bs.collapse', function () {
               $('select').first().focus();
			});
            if($("#SubcontractorPayment_payment_type").val()== 88){
                $(".hiden_box").show();
                $("#SubcontractorPayment_cheque_no").prop("readonly", false);
                $("#SubcontractorPayment_bank").prop("readonly", false);
            }
        });
	$(".js-example-basic-single").select2();
        $(function () {
            $( "#SubcontractorPayment_date" ).datepicker({dateFormat: 'dd-mm-yy'});
        });
	$('#SubcontractorPayment_project_id').change(function(){
		if(($(this).val()) != '') {
			//$("#SubcontractorPayment_payment_type").select2("focus");
			$.ajax({
				method: "GET",
				data: {invoice_id:'test'},
				dataType:"json",
				url: '<?php echo Yii::app()->createUrl('subcontractorpayment/ajaxcall'); ?>',
				success: function(result) {
					 //$("#SubcontractorPayment_payment_type").select2("focus");
                                         $("#SubcontractorPayment_amount").focus();
				}
			});
		} else{
			$("#SubcontractorPayment_project_id").select2("focus");
		}
	})
	
	$('#SubcontractorPayment_payment_type').change(function(){
		if(($(this).val()) != '') {
			var val = $(this).val();
			if(val == 89) {
				$.ajax({
					method: "GET",
					data: {invoice_id:'test'},
					dataType:"json",
					url: '<?php echo Yii::app()->createUrl('subcontractorpayment/ajaxcall'); ?>',
					success: function(result) {
						 //console.log(result);
						 //$('#SubcontractorPayment_amount').focus();
                                                 $("#SubcontractorPayment_description").focus();
					}
				});
			} else if(val == 88) {
                $(".hiden_box").show();
				$.ajax({
					method: "GET",
					data: {invoice_id:'test'},
					dataType:"json",
					url: '<?php echo Yii::app()->createUrl('subcontractorpayment/ajaxcall'); ?>',
					success: function(result) {
						// console.log(result);
						 $("#SubcontractorPayment_bank").select2("focus");
					}
				});
			}
		} else {
			$.ajax({
					method: "GET",
					data: {invoice_id:'test'},
					dataType:"json",
					url: '<?php echo Yii::app()->createUrl('subcontractorpayment/ajaxcall'); ?>',
					success: function(result) {
						 //console.log(result);
						 $("#SubcontractorPayment_payment_type").select2("focus");
					}
				});
		}
		
	})
	
	
	$('#SubcontractorPayment_bank').change(function(){
		if(($(this).val()) != '') {
			$.ajax({
					method: "GET",
					data: {invoice_id:'test'},
					dataType:"json",
					url: '<?php echo Yii::app()->createUrl('subcontractorpayment/ajaxcall'); ?>',
					success: function(result) {
						 //console.log(result);
						 $('#SubcontractorPayment_cheque_no').focus(); 
					}
				});
		} else {
			$("#SubcontractorPayment_bank").select2('focus');
		}
	});
        
	$(document).ready(function () {
		$('select').first().focus();
	});
</script>
<script type="text/javascript">
function getFullData(newDate) {
    $.ajax({
        url: "<?php echo $getUrl; ?>",
        data: {date: newDate},
        type: "POST",
        success:function(data){
            $("#newlist").html(data);
            //alert(data);
        }
    }); 
}

function getTotalFromPayment(subcontractor_id, project_id, amount, txtPaymentId, company_id) {
	//alert(amount);
    $.ajax({
        url: "<?php echo $payment_url; ?>",
        data: {subcontractor_id: subcontractor_id, project_id:project_id, amount: amount, id: txtPaymentId, company_id:company_id},
        type: "GET",
        dataType: "JSON",
        success:function(data){
			if(data.status == 0) {
				$('#buttonsubmit').attr("disabled",false);
				$("#errormessage").show()
                        .html('<div class="alert alert-danger">'+data.limit+'% limit exceed</div>')
                        .fadeOut(10000);
                                $('#approve_status').val('No');
			} else if(data.status == 1){
				$('#buttonsubmit').attr("disabled",false);
				$('#approve_status').val('Yes');
			} else if(data.status == 2){
				$('#buttonsubmit').attr("disabled",true);
                                $('#approve_status').val('Yes');
				$("#errormessage").show()
                        .html('<div class="alert alert-danger">Permission needed</div>')
                        .fadeOut(10000);
			} else if(data.status == 3){
				$('#buttonsubmit').attr("disabled",true);
                                $('#approve_status').val('Yes');
				$("#errormessage").show()
                        .html('<div class="alert alert-danger">Amount greater than total amount</div>')
                        .fadeOut(10000);
			}
        }
    }); 
}
function changedate() {
    var cDate      = $("#SubcontractorPayment_date").val();
    getFullData(cDate);
    validateDateSelection(cDate);
}
/*function getDropdownList(project_id, bill_id, exptypeid) {
    $.ajax({
        url: "<?php //echo $dropdownUrl; ?>",
        data: {"project": project_id, "bill": bill_id},
        type: "POST",
        success:function(data){
            var result = JSON.parse(data);
            $("#Dailyexpense_bill_id").html(result["bills"]);
            if(bill_id != 0)
                $("#Dailyexpense_bill_id").val(bill_id);
            $("#Dailyexpense_expensehead_id").html(result["expenses"]);
            if(exptypeid != 0)
                $("#Dailyexpense_expensehead_id").val(exptypeid);
        }
    });   
}*/
function getProjectList(expId,project) {
    $.ajax({
        url: "<?php echo $projectUrl; ?>",
        data: {"expid": expId},
        type: "POST",
        success:function(data){
            $("#SubcontractorPayment_project_id").html(data);
            if(project != 0)
                $("#SubcontractorPayment_project_id").val(project);
                //$("#SubcontractorPayment_project_id").select2("focus");
                $("#SubcontractorPayment_sgst").focus();
        }
    });
}

function getSubcontractorList(comId,company) {
    $.ajax({
        url: "<?php echo $subcontractorUrl; ?>",
        data: {"comId": comId},
        type: "POST",
        dataType:'json',
        success:function(data){
            $("#SubcontractorPayment_subcontractor_id").html(data.subcontractor);
            $("#SubcontractorPayment_bank").html(data.bank)
            if(company != 0)
                $("#SubcontractorPayment_subcontractor_id").val(company);
                $("#SubcontractorPayment_subcontractor_id").select2("focus");
        }
    });
}

function getSubcontractorList2(comId,company,bnk) {
    $.ajax({
        url: "<?php echo $subcontractorUrl; ?>",
        data: {"comId": comId},
        type: "POST",
        dataType: "json",
        success:function(data){
            $("#SubcontractorPayment_subcontractor_id").html(data.subcontractor);
            $("#SubcontractorPayment_bank").html(data.bank);
            if(company != 0){
                $("#SubcontractorPayment_subcontractor_id").val(company);
            }
            if(bnk != 0){
                $("#SubcontractorPayment_bank").val(bnk);
            }
        }
    });
}


function getProjectList2(expId,project) {
    $.ajax({
        url: "<?php echo $projectUrl; ?>",
        data: {"expid": expId},
        type: "POST",
        success:function(data){
            $("#SubcontractorPayment_project_id").html(data);
            if(project != 0)
                $("#SubcontractorPayment_project_id").val(project);
                //$("#SubcontractorPayment_project_id").select2("open");
        }
    });
}


$("#SubcontractorPayment_subcontractor_id").change(function() {
        var expId = $("#SubcontractorPayment_subcontractor_id").val();
        var expId = expId?expId:0;
        getProjectList(expId,0);
        $("#SubcontractorPayment_project_id").attr("disabled", false);
    });
    
$("#SubcontractorPayment_company_id").change(function() {
        var comId = $("#SubcontractorPayment_company_id").val();
        var comId = comId?comId:0;
        getSubcontractorList(comId,0);
        $("#SubcontractorPayment_subcontractor_id").attr("disabled", false);
        $('#SubcontractorPayment_project_id').attr("disabled", true);
    });    

$('#SubcontractorPayment_amount').keyup(function(e){
	//var amount = $(this).val();
	var tamount = parseFloat($("#SubcontractorPayment_amount").val());
	var sgstp  = parseFloat($("#SubcontractorPayment_sgst").val());
	var cgstp  = parseFloat($("#SubcontractorPayment_cgst").val());
	var igstp  = parseFloat($("#SubcontractorPayment_igst").val());
	
	var sgst   = (sgstp / 100) * tamount;
	var cgst   = (cgstp / 100) * tamount;
	var igst   = (igstp / 100) * tamount;
	
	if (isNaN(sgst)) sgst = 0;
	if (isNaN(cgst)) cgst = 0;
	if (isNaN(igst)) igst = 0;
	if (isNaN(tamount)) tamount = 0;
	var amount =  (Number(tamount)+ Number(cgst) + Number(sgst) + Number(igst)).toFixed(2) ;
	
	var subcontractor_id = $('#SubcontractorPayment_subcontractor_id').val();
	var project_id = $('#SubcontractorPayment_project_id').val();
	var txtPaymentId = $('#txtPaymentId').val();
        var company_id = $('#SubcontractorPayment_company_id').val();
	if(subcontractor_id != '' && project_id != '' && amount !='' && company_id !='') {
		getTotalFromPayment(subcontractor_id, project_id, amount, txtPaymentId, company_id);
    }
    if(e.keyCode == 13){
		var amount = $(this).val();
		var subcontractor_id = $('#SubcontractorPayment_subcontractor_id').val();
		var project_id = $('#SubcontractorPayment_project_id').val();
		var txtPaymentId = $('#txtPaymentId').val();
                var company_id = $('#SubcontractorPayment_company_id').val();
		if(subcontractor_id != '' && project_id != '' && amount !='' && company_id !='') {
			getTotalFromPayment(subcontractor_id, project_id, amount, txtPaymentId, company_id);
		}
	}
})


$('#SubcontractorPayment_sgst, #SubcontractorPayment_cgst, #SubcontractorPayment_igst').keyup(function(e){
	//var amount = $(this).val();
	var tamount = parseFloat($("#SubcontractorPayment_amount").val());
	var sgstp  = parseFloat($("#SubcontractorPayment_sgst").val());
	var cgstp  = parseFloat($("#SubcontractorPayment_cgst").val());
	var igstp  = parseFloat($("#SubcontractorPayment_igst").val());
	
	var sgst   = (sgstp / 100) * tamount;
	var cgst   = (cgstp / 100) * tamount;
	var igst   = (igstp / 100) * tamount;
	
	if (isNaN(sgst)) sgst = 0;
	if (isNaN(cgst)) cgst = 0;
	if (isNaN(igst)) igst = 0;
	if (isNaN(tamount)) tamount = 0;
	var totalamount =  (Number(tamount)+ Number(cgst) + Number(sgst) + Number(igst)).toFixed(2) ;
	
	//console.log(totalamount);

	var subcontractor_id = $('#SubcontractorPayment_subcontractor_id').val();
	var project_id = $('#SubcontractorPayment_project_id').val();
	var txtPaymentId = $('#txtPaymentId').val();
        var company_id = $('#SubcontractorPayment_company_id').val();
	if(subcontractor_id != '' && project_id != '' && totalamount !='' && company_id !='') {
		getTotalFromPayment(subcontractor_id, project_id, totalamount, txtPaymentId, company_id);
    }
    if(e.keyCode == 13){
		var amount = $(this).val();
		var subcontractor_id = $('#SubcontractorPayment_subcontractor_id').val();
		var project_id = $('#SubcontractorPayment_project_id').val();
		var txtPaymentId = $('#txtPaymentId').val();
                var company_id = $('#SubcontractorPayment_company_id').val();
		if(subcontractor_id != '' && project_id != '' && totalamount !='' && company_id !='') {
			getTotalFromPayment(subcontractor_id, project_id, totalamount, txtPaymentId, company_id);
		}
	}
})


$( document ).ready(function() {
    $("#SubcontractorPayment_bank").attr("disabled", true);
    $("#SubcontractorPayment_cheque_no").attr("readonly", true);
    $("a#next").css("pointer-events","none").css("color","#ccccc");
	$("#SubcontractorPayment_subcontractor_id").focus();
	$("#SubcontractorPayment_project_id").attr("disabled", true);
        $("#SubcontractorPayment_subcontractor_id").attr("disabled", true);
    $( "#buttonsubmit" ).click(function() {
		$("#buttonsubmit").attr('disabled', true);
	if ($(this).hasClass('disabled')) {
        //alert('CLICKED, BUT DISABLED!!');
    } else {
        var crDate       = $("#SubcontractorPayment_date").val();
        var subcont_id   = $("#SubcontractorPayment_subcontractor_id").val();
        var project_id   = $("#SubcontractorPayment_project_id").val();
        var company_id   = $("#SubcontractorPayment_company_id").val();
        var amount       = parseFloat($("#SubcontractorPayment_amount").val());
        var description  = $("#SubcontractorPayment_description").val();
        var payment_type = $("#SubcontractorPayment_payment_type").val();
        var bank         = $("#SubcontractorPayment_bank").val();
        var cheque_no    = $("#SubcontractorPayment_cheque_no").val();
        
        if(subcont_id == "" || project_id == "" || amount == "" || description == "" || payment_type == "" || company_id == "") {
                    $("#errormessage").show()
                        .html('<div class="alert alert-danger">Please fill all mandatory fields.</div>')
                        .fadeOut(5000);
                        $("#buttonsubmit").attr('disabled', false);
                    return false;
        }
        
        if(payment_type == 88) {
			if(bank == "" || cheque_no == "") {
				$("#errormessage").show()
                        .html('<div class="alert alert-danger">Please fill all mandatory fields.</div>')
                        .fadeOut(5000);
                        $("#buttonsubmit").attr('disabled', false);
                    return false;
			}
		}
        
         var dayBookId = $("#txtPaymentId").val();
        var actionUrl;
        if(dayBookId == "") {
            actionUrl = "<?php echo Yii::app()->createAbsoluteUrl("subcontractorpayment/addDailyentries"); ?>";
            localStorage.setItem("action", "add");
        } else {
            actionUrl = "<?php echo Yii::app()->createAbsoluteUrl("subcontractorpayment/updateDailyentries"); ?>";
            localStorage.setItem("action", "update");
        }
        $('#loader').css('display','inline-block');
        var data = $("#dailyexpense-form").serialize();
        $.ajax({
            type: 'GET',
            url: actionUrl,
            data:data,
            success:function(data){
                $('#loader').hide();
                if(data == 1) {
                    $("#errormessage").show()
                        .html('<div class="alert alert-warning"><strong>Failed !</strong> please try again.</div>')
                        .fadeOut(5000);
                        $("#buttonsubmit").attr('disabled', false);
                    return false;
                } else {
					$("#buttonsubmit").text("ADD");
                    var currAction = localStorage.getItem("action");
                    if(currAction == "add") {
                        $("#errormessage").show()
                            .html('<div class="alert alert-success"><strong>Successfully !</strong> record added.</div>')
                            .fadeOut(5000);
                        var today = new Date();
                        var dd    = today.getDate();
                        var mm    = today.getMonth()+1; //January is 0!
                        var yyyy  = today.getFullYear();
                        if(dd<10)
                            dd    = '0'+dd
                        if(mm<10)
                            mm    = '0'+mm
                        today     = dd + '-' + mm + '-' + yyyy;
                        $("#entrydate").text(today);
                        //$(".selectBank").css("display","none");
                    } else if(currAction == "update") {
                        $("#errormessage").show()
                            .html('<div class="alert alert-success"><strong>Successfully !</strong> record updated.</div>')
                            .fadeOut(5000);
                    }
                    document.getElementById("dailyexpense-form").reset();
                    $("#SubcontractorPayment_date").val(crDate);
                    $("#newlist").html(data);
                    $("#SubcontractorPayment_subcontractor_id").focus();
                    $("#buttonsubmit").attr('disabled', false);
                    $("#SubcontractorPayment_subcontractor_id").val("");
                   $('#SubcontractorPayment_subcontractor_id').val('').trigger('change.select2'); 
                   $('#SubcontractorPayment_project_id').val('').trigger('change.select2');
                   $('#SubcontractorPayment_payment_type').val('').trigger('change.select2');  
                   $('#SubcontractorPayment_bank').val('').trigger('change.select2'); 
                   $('#SubcontractorPayment_company_id').val('').trigger('change.select2'); 
                    $('#SubcontractorPayment_project_id').attr('disabled', true);
                    $("#txtSgst1").text("");
                    $("#txtCgst1").text("");
                    $("#txtIgst1").text("");
                    $("#txtgstTotal").text("");
                    $("#txtTotal1").text("");
                    $(".hiden_box").hide();
                } 
                
                $('#txtPaymentId').val("");
                
            },
            error: function(data) { // if error occured
                //alert("Error occured.please try again");
                //alert(data);
            }
        });
        $('.js-example-basic-single').select2('focus');
         
    }
    
    });
    
    $("body").on("click", ".row-daybook", function (e){
    //$(".row-daybook").click(function() {
        $('html, body').animate({ scrollTop: 0 }, 'slow'); 
        $(".daybook .collapse").addClass( "in" );
        $(".addentries").removeClass( "collapsed" );
        if ( $(".daybook .collapse").css('display') == 'visible' ){
            $(".daybook .collapse").css({"height":"auto"}); 
        }else{
            $(".collapse").css({"height":""});
        }
        $(".popover").removeClass( "in" );
        var rowId = $(this).attr("id");
        var expId = $("#paymentid"+rowId).val();
        $("#buttonsubmit").text("UPDATE");
        $("#txtPaymentId").val(expId);
         $.ajax({
            url: "<?php echo $getSubContractor; ?>",
            data: {"paymentid": expId},
            type: "POST",
            success:function(data){
                var result = JSON.parse(data);
                $(".gstvalue").css({"display": "inline-block"});
               // alert(result['subcontractor_id']);
               $("#SubcontractorPayment_company_id").select2('focus');
               $("#SubcontractorPayment_project_id").attr("disabled",false);
                if(!result["subcontractor_id"]) {
					$("#SubcontractorPayment_subcontractor_id").val("");
					$("#SubcontractorPayment_project_id").val("");
					$("#SubcontractorPayment_description").val("");
					$("#SubcontractorPayment_payment_type").val("");
					$("#SubcontractorPayment_amount").val("");
					$("#SubcontractorPayment_sgst").val("");
					$("#SubcontractorPayment_sgst_amount").val("");
					$("#SubcontractorPayment_cgst").val("");
					$("#SubcontractorPayment_cgst_amount").val("");
					$("#SubcontractorPayment_igst").val("");
					$("#SubcontractorPayment_igst_amount").val("");
					$("#SubcontractorPayment_tax_amount").val("");
					
					if(result["payment_type"] == 88) {
						$(".hiden_box").show();
					 $("#SubcontractorPayment_bank").attr("disabled",false);
                        $("#SubcontractorPayment_bank").val(result["bank"]);
                        $("#SubcontractorPayment_cheque_no").attr("readonly",false);
                        $("#SubcontractorPayment_cheque_no").val(result["cheque_no"]);
                    } else {
						$(".hiden_box").hide();  
                        $("#SubcontractorPayment_bank").val("");
                        $("#SubcontractorPayment_cheque_no").val("");
                        $("#SubcontractorPayment_bank").attr("disabled",true);
                        $("#SubcontractorPayment_cheque_no").attr("readonly",true);
                        
                    }
					
				} else {
					 $('#SubcontractorPayment_subcontractor_id').val(result["subcontractor_id"]).trigger('change.select2');
                                         $('#SubcontractorPayment_company_id').val(result["company"]).trigger('change.select2');
					getProjectList2(result["subcontractor_id"],result["project_id"]);
                                        var bnk =  (result["bank"]!='')?result["bank"]:0;
					getSubcontractorList2(result["company"],result["subcontractor_id"],bnk);
					$("#SubcontractorPayment_project_id").attr("disabled",false);
                                        $("#SubcontractorPayment_subcontractor_id").attr("disabled",false);
					$("#SubcontractorPayment_description").val(result["description"]);
					$('#SubcontractorPayment_payment_type').val(result["payment_type"]).trigger('change.select2');
					$("#SubcontractorPayment_amount").val(result["amount"]);
					
					$("#SubcontractorPayment_sgst").val(result["sgst"]);
					$("#SubcontractorPayment_sgst_amount").val(result["sgst_amount"]);
					$("#txtSgst1").text(result["sgst_amount"]);
					$("#SubcontractorPayment_cgst").val(result["cgst"]);
					$("#SubcontractorPayment_cgst_amount").val(result["cgst_amount"]);
					$("#txtCgst1").text(result["cgst_amount"]);
					$("#SubcontractorPayment_igst").val(result["igst"]);
					$("#SubcontractorPayment_igst_amount").val(result["igst_amount"]);
					$("#txtIgst1").text(result["igst_amount"]);
					$("#SubcontractorPayment_tax_amount").val(result["tax_amount"]);
					
					$("#txtgstTotal").html("<b>Total Tax: </b> " + result["tax_amount"] +" ");
					var total =  (Number(result["amount"])+ Number(result["tax_amount"])).toFixed(2) ;
					$("#txtTotal1").html("<b>Total: </b> " + total + " "); 
					
					if(result["payment_type"] == 88) {
						$(".hiden_box").show();
					 $("#SubcontractorPayment_bank").attr("disabled",false);
                        $('#SubcontractorPayment_bank').val(result["bank"]).trigger('change.select2');
                        $("#SubcontractorPayment_cheque_no").attr("readonly",false);
                        $("#SubcontractorPayment_cheque_no").val(result["cheque_no"]);
                    } else {
						$(".hiden_box").hide();  
                        $('#SubcontractorPayment_bank').val('').trigger('change.select2');
                        $("#SubcontractorPayment_cheque_no").val("");
                        $("#SubcontractorPayment_bank").attr("disabled",true);
                        $("#SubcontractorPayment_cheque_no").attr("readonly",true);
                        
                    }
				}
                
            }
        }); 
        
       
    });
    
    
        $(".amountvalidation").keydown(function (event) {

			if (event.shiftKey == true) {
				event.preventDefault();
			}
	
			if ((event.keyCode >= 48 && event.keyCode <= 57) || 
				(event.keyCode >= 96 && event.keyCode <= 105) || 
				event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 ||
				event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190 || event.keyCode == 110 ||  event.keyCode == 13) {
				var splitfield = $(this).val().split(".");
				if(splitfield[1].length >= 2 && event.keyCode != 8 && event.keyCode != 0 && event.keyCode != 13){
					event.preventDefault();
				}
			} else {
				event.preventDefault();
			}
	
			if($(this).val().indexOf('.') !== -1 && (event.keyCode == 190 || event.keyCode == 110 ))
				event.preventDefault(); 
			//if a decimal has been added, disable the "."-button
			
		});
		
		
		$("#SubcontractorPayment_cheque_no").keypress(function(e){
		if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
				   return false;
		}
		})
    
    $("#previous").click(function(e) {
        var cDate      = $("#SubcontractorPayment_date").val();
       
        cDate          = cDate.split("-").reverse().join("-");
        var newDate    = new Date(cDate);
        var prDate     = new Date(newDate.getFullYear(), newDate.getMonth(), newDate.getDate()-1);
        var newPrDate  = $.format.date(prDate, "dd-MM-yyyy");
        $("#SubcontractorPayment_date").val(newPrDate);
        getFullData(newPrDate);
        validateDateSelection(newPrDate);
    });   
    $("#current").click(function() {
        var cDate      = new Date();
        var newDate    = $.format.date(cDate, "dd-MM-yyyy");
        $("#SubcontractorPayment_date").val(newDate);
        getFullData(newDate);
        validateDateSelection(newDate);
    });
    $("#next").click(function() {
        var cDate      = $("#SubcontractorPayment_date").val();
        cDate          = cDate.split("-").reverse().join("-");
        var newDate    = new Date(cDate);
        var nxtDate    = new Date(newDate.getFullYear(), newDate.getMonth(), newDate.getDate()+1);
        var newNxtDate = $.format.date(nxtDate, "dd-MM-yyyy");
        $("#SubcontractorPayment_date").val(newNxtDate);
        getFullData(newNxtDate);
        validateDateSelection(newNxtDate);
    });
    $("#SubcontractorPayment_payment_type").change(function() {
        var receiptType = $("#SubcontractorPayment_payment_type").val();
        if(receiptType == 88) {
            $("#SubcontractorPayment_cheque_no").attr("readonly", false); 
            $("#SubcontractorPayment_bank").attr("disabled", false); 
        } else {
			$("#SubcontractorPayment_cheque_no").attr("readonly", true); 
            $("#SubcontractorPayment_bank").attr("disabled", true); 
        } 
    });
   
    $("#txtPurchaseType").change(function() {
        var purchaseType = $("#txtPurchaseType").val();
        var amount       = $("#txtTotal").val();
        if(purchaseType == 1) {
            $("#txtPaid").val(0);
            $("#txtPaid").attr("readOnly", true);
        } else if (purchaseType == 2) {
           $("#txtPaid").val(amount);
            $("#txtPaid").attr("readOnly", true); 
        } else if (purchaseType == 3) {
           $("#txtPaid").val(0);
            $("#txtPaid").attr("readOnly", false); 
        } else {
            $("#txtPaid").val("0");
            $("#txtPaid").attr("readOnly", false);
        }   
    });

    $("#btnReset").click(function() {
        $("#txtPaymentId").val("");
        $("#buttonsubmit").text("ADD");
        $('#dailyexpense-form').find('input, select, textarea').not("#SubcontractorPayment_date").val('');
        $("#Dailyexpense_bill_id").val("");
        $("#Dailyexpense_expensehead_id").val("");
        $("#Dailyexpense_vendor_id").html('<option value="">-Select Vendor-</option>');
        $("#Dailyexpense_vendor_id").attr("disabled", true);
        $('#SubcontractorPayment_subcontractor_id').val('').trigger('change.select2'); 
	    $('#SubcontractorPayment_project_id').val('').trigger('change.select2');
	    $('#SubcontractorPayment_payment_type').val('').trigger('change.select2');  
	    $('#SubcontractorPayment_bank').val('').trigger('change.select2'); 
	    
	    $("#txtSgst1").text("");
		$("#txtCgst1").text("");
		$("#txtIgst1").text("");
		$("#txtgstTotal").text("");
		$("#txtTotal1").text("");
		$(".hiden_box").hide(); 
    });
    
    $("#SubcontractorPayment_subcontractor_id").click(function() {
        var subcontractor_id = $("#SubcontractorPayment_subcontractor_id").val();
        if(subcontractor_id != "")
            $("#SubcontractorPayment_project_id").focus();
    });
    $("#SubcontractorPayment_project_id").click(function() {
        var project = $("#SubcontractorPayment_project_id").val();
        if(project != "")
            $("#SubcontractorPayment_payment_type").focus();
    });
    
    
    $("#SubcontractorPayment_payment_type").change(function() {
        var rType = $("#SubcontractorPayment_payment_type").val();
        if(rType == 88) {
            $("#SubcontractorPayment_bank").attr("disabled", false);
            $("#SubcontractorPayment_cheque_no").attr("readonly", false);
        } else {
            $("#SubcontractorPayment_bank").val("");
            $("#SubcontractorPayment_cheque_no").val("");
            $("#SubcontractorPayment_bank").attr("disabled", true);
            $("#SubcontractorPayment_cheque_no").attr("readonly", true);
        }
        
    });  
    
    $("#SubcontractorPayment_payment_type").click(function() {
        var rType = $("#SubcontractorPayment_payment_type").val();
        if(rType == 88) {
            $("#SubcontractorPayment_bank").attr("disabled", false);
            $("#SubcontractorPayment_cheque_no").attr("readonly", false);
            $("#SubcontractorPayment_bank").focus();
        } else {
            $("#SubcontractorPayment_bank").val("");
            $("#SubcontractorPayment_cheque_no").val("");
            $("#SubcontractorPayment_bank").attr("disabled", true);
            $("#SubcontractorPayment_cheque_no").attr("readonly", true);
            //$("#SubcontractorPayment_amount").focus();
        }
        
    });
    
        $("#SubcontractorPayment_bank").click(function() {
        var bank = $("#SubcontractorPayment_bank").val();
        if(bank != "")
            $("#SubcontractorPayment_cheque_no").focus();
    });
    
    $("#SubcontractorPayment_cheque_no").keyup(function(e){
		if(e.keyCode == 13)
        {
       var cheque_no = $("#SubcontractorPayment_cheque_no").val();
        if(cheque_no != "")
            //$("#SubcontractorPayment_amount").focus();
              $("#SubcontractorPayment_description").focus();
        }    
    });
    
    
    $("#SubcontractorPayment_amount").keyup(function(e){
		if(e.keyCode == 13)
        {
       var amount = $("#SubcontractorPayment_amount").val();
        if(amount != "")
            //$("#SubcontractorPayment_sgst").focus();
             $("#SubcontractorPayment_payment_type").focus();    
       
            
		}
    });
    
    $("#SubcontractorPayment_sgst").keyup(function(e){
		if(e.keyCode == 13)
        {
      var amount = $("#SubcontractorPayment_sgst").val();
        //if(amount != "")
            $("#SubcontractorPayment_cgst").focus();
            
		}
    });
    
    $("#SubcontractorPayment_cgst").keyup(function(e){
		if(e.keyCode == 13)
        {
       var amount = $("#SubcontractorPayment_cgst").val();
        //if(amount != "")
            $("#SubcontractorPayment_igst").focus();
            
		}
    });
    
    $("#SubcontractorPayment_igst").keyup(function(e){
		if(e.keyCode == 13)
        {
       var amount = $("#SubcontractorPayment_igst").val();
        //if(amount != "")
            //$("#SubcontractorPayment_description").focus();
            $("#SubcontractorPayment_project_id").select2("focus");
            
		}
    });
    
    $("#SubcontractorPayment_description").keyup(function(e){
		if(e.keyCode == 13)
        {
       var description = $("#SubcontractorPayment_description").val();
        if(description != "")
            $("#buttonsubmit").focus();
            
		}
    });
    
    
    

});
function validateDateSelection(cDate) {
    var selDate  = cDate;
    var currDate = new Date();
    currDate     = $.format.date(currDate, "dd-MM-yyyy");
    if(selDate == currDate) {
        $("a#next").css("pointer-events","none").css("color","#ccccc");
    } else {
        $("a#next").css("pointer-events","visible").css("color","#337ab7");
    }
    $("#btnReset").click();
    $("#txtPaymentId").val("");
    $("#SubcontractorPayment_date").val(selDate);
    //$("#Dailyexpense_bill_id").html('<option value="">-Select Bill Number-</option>');
    //$("#Dailyexpense_expensehead_id").html('<option value="">-Select Expense Type-</option>');
    $("#Dailyexpense_vendor_id").html('<option value="">-Select Vendor-</option>');
    $("#Dailyexpense_vendor_id").attr("disabled", true);
}
function validateData() {
    //var projectId    = $("#Expenses_project_id").val();
    var billNumber   = $("#Dailyexpense_bill_id").val();
    var expenseType  = $("#Dailyexpense_expensehead_id").val();
    var vendorId     = $("#Dailyexpense_vendor_id").val();
    var amount       = $("#txtAmount").val();
    var sgstP        = $("#txtSgstp").val();
    var sgst         = $("#txtSgst").val();
    var cgstP        = $("#txtCgstp").val();
    var cgst         = $("#txtCgst").val();
    var total        = $("#txtTotal").val();
    var receiptType  = $("#Dailyexpense_dailyexpense_receipt_type").val();
    var receipt      = $("#txtReceipt").val();
    var purchaseType = $("#txtPurchaseType").val();
    var paid         = $("#txtPaid").val();
    var currId       = $(this).attr(id);
    if(expenseType != "" || expenseType != 0) {
        $("#Dailyexpense_dailyexpense_receipt_type").attr("disabled", true);
        $("#txtReceipt").attr("readonly", true);
        $("#Dailyexpense_vendor_id").val("");
        $("#Dailyexpense_vendor_id").attr("disabled", false);
        $("#txtAmount").attr("readonly", false);
        $("#txtSgstp").attr("readonly", false);
        $("#txtSgst").attr("readonly", false);
        $("#txtCgstp").attr("readonly", false);
        $("#txtCgst").attr("readonly", false);
        $("#txtTotal").attr("readonly", false); 
    }
    if(currId == "Dailyexpense_expensehead_id" && (expenseType == "" || expenseType == 0)) {
        $("#Dailyexpense_dailyexpense_receipt_type").attr("disabled", false);
        $("#txtReceipt").attr("readonly", false);
    }
}

/* $(document).on('change', 'input[name="SubcontractorPayment[amount]"], input[name="SubcontractorPayment[sgst]"], input[name="SubcontractorPayment[cgst]"], input[name="SubcontractorPayment[igst]"]', function(e){
var amount= '';
var cgst='';
var sgst='';
var igst='';
var tax_total ='';

sgst += $("input[name='SubcontractorPayment[sgst]']").val()
		  
cgst += $("input[name='SubcontractorPayment[cgst]']").val()

igst += $("input[name='SubcontractorPayment[igst]']").val()

amount += $("input[name='SubcontractorPayment[amount]']").val()


if(!sgst ==''){
	var tax_amount = amount/100*sgst;
	$("#SubcontractorPayment_sgst_amount").val(tax_amount.toFixed(2));
	 var cgst_am =  $('#SubcontractorPayment_cgst_amount').val();
	 var igst_am =  $('#SubcontractorPayment_igst_amount').val();
	 tax_total = Number(tax_amount)+Number(cgst_am)+Number(igst_am);
	$('#SubcontractorPayment_tax_amount').val(tax_total.toFixed(2)); 
}

if(!cgst ==''){
	var tax_amount = amount/100*cgst;
	$("#SubcontractorPayment_cgst_amount").val(tax_amount.toFixed(2));
	 var sgst_am =  $('#SubcontractorPayment_sgst_amount').val();
	 var igst_am =  $('#SubcontractorPayment_igst_amount').val();
	 tax_total = Number(tax_amount)+Number(sgst_am)+Number(igst_am);
	$('#SubcontractorPayment_tax_amount').val(tax_total.toFixed(2)); 
}

if(!igst ==''){
	var tax_amount = amount/100*igst;
	$("#SubcontractorPayment_igst_amount").val(tax_amount.toFixed(2));
	 var sgst_am =  $('#SubcontractorPayment_sgst_amount').val();
	 var cgst_am =  $('#SubcontractorPayment_cgst_amount').val();
	 tax_total = Number(tax_amount)+Number(sgst_am)+Number(cgst_am);
	$('#SubcontractorPayment_tax_amount').val(tax_total.toFixed(2)); 
}

}); */

$("#SubcontractorPayment_amount, #SubcontractorPayment_sgst, #SubcontractorPayment_cgst, #SubcontractorPayment_igst").blur(function() {
        var amount = parseFloat($("#SubcontractorPayment_amount").val());
        var sgstp  = parseFloat($("#SubcontractorPayment_sgst").val());
        var cgstp  = parseFloat($("#SubcontractorPayment_cgst").val());
        var igstp  = parseFloat($("#SubcontractorPayment_igst").val());
        
        var sgst   = (sgstp / 100) * amount;
        var cgst   = (cgstp / 100) * amount;
        var igst   = (igstp / 100) * amount;
        
        if (isNaN(sgst)) sgst = 0;
        if (isNaN(cgst)) cgst = 0;
        if (isNaN(igst)) igst = 0;
        if (isNaN(amount)) amount = 0;
        
        var tax_total  = sgst + cgst + igst;
        $(".gstvalue").css({"display": "inline-block"});
        $("#SubcontractorPayment_sgst_amount").val(sgst.toFixed(2));
        $("#SubcontractorPayment_cgst_amount").val(cgst.toFixed(2));
        $("#SubcontractorPayment_igst_amount").val(igst.toFixed(2));
        $("#SubcontractorPayment_tax_amount").val(tax_total.toFixed(2));
        
         $("#txtSgst1").text(sgst.toFixed(2));
         $("#txtCgst1").text(cgst.toFixed(2));
         $("#txtIgst1").text(igst.toFixed(2));
         
        $("#txtgstTotal").html("<b>Total Tax: </b>" +tax_total.toFixed(2)+ "");
        var total =  (Number(amount)+ Number(cgst) + Number(sgst) + Number(igst)).toFixed(2) ;

        $("#txtTotal1").html("<b>Total: </b> " + total + ""); 
        
        
    });
    
    // delete row
	$(document).on('click', '.delete_row', function(e){
		var answer = confirm("Are you sure you want to delete?");
		if (answer)
		{
			var rowId = $(this).attr("id");
			var expId = $("#paymentid"+rowId).val();
            var date  = $("#SubcontractorPayment_date").val();
            $.ajax({
				type: "POST",
				data: {expId:expId, payment_date: date},
				url:"<?php echo Yii::app()->createUrl("subcontractorpayment/deletdailyentry") ?>",
				success:function(data){
					if(data == 1) {
						$("#errormessage").show()
							.html('<div class="alert alert-warning"><strong>Failed !</strong> please try again.</div>')
							.fadeOut(5000);
						$("#buttonsubmit").attr('disabled', false);
						return false;
					} else {
						$("#errormessage").show()
                            .html('<div class="alert alert-success"><strong>Success!</strong> Row Deleted.</div>')
                            .fadeOut(5000);
						$("#newlist").html(data);
					}
				}
			})
		}
		
	})
	
	$('.percentage').keyup(function(){
		  if ($(this).val() > 100){
			$(this).val('');
		  }
	});
        
        $(document).on('click','.permission_item', function(e){
	e.preventDefault();
	var element = $(this);
	var item_id = $(this).attr('id');
        element.closest('.tooltip-hiden').css('pointer-events','none');
        $.ajax({
                url: '<?php echo  Yii::app()->createAbsoluteUrl('subcontractorpayment/permissionapprove'); ?>',
                type:'GET',
                dataType:'json',
                data:{item_id:item_id},
                success: function(response) {
                        $(".popover").removeClass( "in" );
                        element.closest('.tooltip-hiden').css('pointer-events','');
                        element.closest('.permission_item').removeAttr('disabled');
                        element.closest('.row-daybook').removeAttr('disabled');
                        element.closest('.delete_row').removeAttr('disabled');
                        if(response.response == 'success')
                        {
                                 $(".approveoption_"+item_id).hide();
                                 element.closest('tr').removeClass('permission_style');
                                 element.closest('td').removeClass('permission_style');
                                 //element.closest('tr td:first-child').css('background-color','yellow');
                                 $("#errormessage").show()
                                .html('<div class="alert alert-success"><strong>Success!</strong>'+response.msg+'</div>')
                                .fadeOut(5000);
                        } else if(response.response == 'warning'){
                                 $(".approveoption_"+item_id).hide();
                                 element.closest('tr').removeClass('permission_style');
                                // $().toastmessage('showWarningToast', ""+response.msg+"");
                                $("#errormessage").show()
                                .html('<div class="alert alert-warning"><strong>Failed !</strong>'+response.msg+'</div>')
                                .fadeOut(5000);
                        } else {
                               // $().toastmessage('showErrorToast', ""+response.msg+"");
                               $("#errormessage").show()
                                .html('<div class="alert alert-error"><strong>Failed !</strong>'+response.msg+'</div>')
                                .fadeOut(5000);
                        }
                }
            });
        });      
</script>
