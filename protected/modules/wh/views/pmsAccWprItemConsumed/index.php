<?php
/* @var $this PmsAccWprItemConsumedController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Pms Acc Wpr Item Consumeds',
);

$this->menu=array(
	array('label'=>'Create PmsAccWprItemConsumed', 'url'=>array('create')),
	array('label'=>'Manage PmsAccWprItemConsumed', 'url'=>array('admin')),
);
?>

<h1>Pms Acc Wpr Item Consumeds</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
