
<?php
/* @var $this ProjectTypeController */
/* @var $model ProjectType */
/* @var $form CActiveForm */
?>

    <div class="">
    <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id'=>'project-type-form',
            'enableAjaxValidation' => true,
            'enableClientValidation' => true,
            'clientOptions' => array(
                'validateOnSubmit' => true,
                'validateOnChange' => true,
                'validateOnType' => false,),
        ));
    ?>

    <!-- Popup content-->

        <div class="panel-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <?php echo $form->labelEx($model,'project_type'); ?>
                        <?php echo $form->textField($model,'project_type',array('size'=>50,'maxlength'=>50,'class'=>'form-control')); ?>
                        <?php echo $form->error($model,'project_type'); ?>
                    </div>
                </div>
                <div class="col-md-6 save-btnHold">
                    <label style="display:block;">&nbsp;</label>
                    <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'btn btn-sm btn-info submit')); ?> 
                    <?php  
                        if(!$model->isNewRecord){
                            echo CHtml::Button('Close', array('class'=>'btn','onclick' => 'javascript:location.href="'. $this->createUrl('newlist').'"'));
                            } 
                            else{
                               echo CHtml::ResetButton('Reset', array('style'=>'margin-right:3px;','class'=>'btn btn-sm btn-default')); 
                               echo CHtml::ResetButton('Close', array('onclick'=>'closeaction(this,event);','class'=>'btn btn-sm'));  
                            }
                    ?>
                </div>
            </div>
        </div>
    <?php $this->endWidget(); ?>
    </div><!-- form -->

    <script>
        function onlySpecialchars(str)
        {        
            var regex = /^[^a-zA-Z0-9]+$/;
            var message ="";      
            var disabled=false;     
            var matchedAuthors = regex.test(str);

            if (matchedAuthors) {
                var message = "Special characters not allowed";
                var disabled=true;           
            } 
            return {"message":message,"disabled":disabled};        
        }      

        $("#ProjectType_project_type").keyup(function () {         
            var response =  onlySpecialchars(this.value);   
            $(this).siblings(".errorMessage").show().html(response.message).addClass('d-block');                
            $(".submit").attr('disabled',response.disabled);
        });
    </script>

    
    
