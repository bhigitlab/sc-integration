<?php
/* @var $this QuotationCategoryMasterController */
/* @var $model QuotationCategoryMaster */

$this->breadcrumbs = array(
    'Quotation Work Type Masters' => array('index'),
    'Manage',
);
?>

<style>
    .greytable {
        width: 100%;
        cellpadding: 0;
        border-collapse: collapse;
    }

    .greytable thead tr:first-child {
        background: #6e6f72;
        color: #fff;
    }

    .greytable thead tr:first-child a:link {
        color: #fff;
    }

    .greytable>tbody tr {
        background: #fff;
        color: #828282;
    }

    .greytable>tbody tr:nth-child(2n) {
        background: #ededed;
    }

    .greytable td,
    th {
        padding: 8px 6px;
    }

    .greytable th {
        color: #fff;
    }

    .greytable tbody th {
        color: #828282;
    }

    .greytable,
    .greytable th,
    .greytable td {
        border: 1px solid #d8d8d8;
    }

    .greytable td.highlight {
        background: #555;
        color: #fff;
    }

    div.flash-success
    {
        background:#E6EFC2;
        color:#264409;
        border-color:#C6D880;
    }
</style>
<div class="alert alert-success" role="alert" style="display:none;">
</div>
<div class="alert alert-danger" role="alert" style="display:none;">
</div>
<div class="alert alert-warning" role="alert" style="display:none;"></div>
<div class="container">
    
    <div class="row">
        <div class="col-md-4">
            <h4><?php echo $formmodel->isNewRecord ? 'Add' : 'Update' ?> WorkType</h4>
            <?php echo $this->renderPartial('_form', array('model' => $formmodel)); ?>
        </div>
        <div class="col-md-8">
            <h4>Worktype List</h4>
            <?php if (Yii::app()->user->hasFlash('success')) : ?>
                <div class="alert alert-success info" style="width: 500px;margin-left: 400px;padding: 8px;color: green;font-size: 20px;font-weight: bold;">
                    <?php echo Yii::app()->user->getFlash('success'); ?>
                </div>
            <?php endif; ?>

            <?php if (Yii::app()->user->hasFlash('error')) : ?>
                <div class="alert alert-danger info" style="color:50px;">
                    <?php echo Yii::app()->user->getFlash('error'); ?>
                </div>
            <?php endif; ?>
            <?php
            $this->widget('zii.widgets.grid.CGridView', array(
                'id' => 'quotation-category-master-grid',
                'dataProvider' => $model->search(),
                'itemsCssClass' => 'greytable',
                'afterAjaxUpdate' => 'true',
                'pager' => array(
                    'id' => 'dataTables-example_paginate', 'header' => '', 'prevPageLabel' => '<<',
                    'nextPageLabel' => '>>'
                ),
                'filter' => $model,
                'columns' => array(
                     array(
                    'value' => '$data->id',
                    'headerHtmlOptions' => array('style' => 'display:none'),
                    'htmlOptions' => array('style' => 'display:none', 'class' => 'rowId'),
                    'filterHtmlOptions' => array('style' => 'display:none'),
                ),
                    array('class' => 'IndexColumn', 'header' => 'Sl.No.', 'htmlOptions' => array('class' => 'snocol')),
                    'name',
                    array(
                        'name' => 'status',
                        'type' => 'raw',
                        'value' =>function($model){
                            if($model->status == 1){
                                return 'Active';
                            }elseif($model->status == 0){
                                return 'Inactive';                                    
                            }else{
                                return '';
                            }
                        },
                        'filter' => CHtml::dropDownList(
                            'QuotationWorktype[status]', $model->status, array('0' => 'Inactive', '1' => 'Active'), array('empty' => 'All',)
                        ),
                    ),
                      
                    array(
                        'class' => 'CButtonColumn',
                        'template' => '{update}',
                        'htmlOptions' => array('width' => '100px', 'style' => 'font-weight: bold;text-align:center', 'class' => 'indexactionbtncolmn'),
                        'buttons' => array(
                            'update' => array(
                                'url' => 'Yii::app()->createUrl("worktype/quotationWorktype/index", array("id"=>$data->id))',
                            ),
                        ),
                    ),
                ),
            ));
            ?>
        </div>
    </div>
</div>
<script type="text/javascript">
  function deleteaction(elem) {
      var rowid = $(elem).closest("tr").find(".rowId").text();
      var url = "<?php echo $this->createUrl('/worktype/quotationWorktype/delete&id=') ?>" + rowid;
      var answer = confirm("Are you sure you want to delete?");
      if (answer) {
            $.ajax({
		type: "POST",
		dataType: "json",
		url: url,
		success: function(response) {
		$('html, body').animate({
                scrollTop: $("#content").position().top - 100
                }, 500);
                $.fn.yiiGridView.update('quotation-category-master-grid');
                if (response.response == "success") {
                        $(".alert-success").show().html(response.msg).delay(3000).fadeOut();
                } else if (response.response == "warning") {
                        $(".alert-warning").show().html(response.msg).delay(3000).fadeOut();
                } else if(response.response == "error"){
                        $(".alert-danger").show().html(response.msg).delay(3000).fadeOut();
		}
		}
                });
		}
	return false;
	};

    function countOfLetters(str) {
        var letter = 0;
        for (i = 0; i < str.length; i++) {
            if ((str[i] >= 'A' && str[i] <= 'Z')
                || (str[i] >= 'a' && str[i] <= 'z'))
            letter++;
        }
        return letter;
    }      

    $("#QuotationWorktype_name").keyup(function () { 
        var alphabetCount =  countOfLetters(this.value);  
        var message =""; 
        var disabled=false;     
        if(alphabetCount < 1){
            var message = "Invalid Label Name";
            var disabled=true;   
        } 

        $(this).siblings(".errorMessage").show().html(message).addClass('d-block');    
        $(".submit").attr('disabled',disabled);
    });
 </script>
