<?php
/* @var $this SubcontractorController */
/* @var $data Subcontractor */
?>

<?php
$tblpx = Yii::app()->db->tablePrefix;
$id= $data['id'];
?>



<?php
if ($index == 0) {
?>

    <thead>
        <tr>
            <th style="width:60px;">Action</th>
            <th style="width:20px;">Sl No.</th>
            <th>Company</th>
            <th>Project</th>
            <th>Sub Contractor</th>
            <th>Expense Head</th>
            <th>Date</th> 
            <th>Description</th>
            <th>Material</th>
                               
        </tr>
    </thead>
    
<?php
}
$class='';
$newclass='';
if($data->status=='1'){
    $newclass='row_class';
}
?>

<tr class="<?php echo $class.''.$newclass; ?>">
    <td>
        <span class="icon icon-options-vertical popover-test" data-toggle="popover" data-placement="right" type="button" data-html="true" style="cursor: pointer;"></span>
        <div class="popover-content hide">
            <ul class="tooltip-hiden">
                
                    <li><a class="click btn btn-xs btn-default" id="<?php echo $data['id']; ?>" href="#" onClick="edit('<?php echo $data['id']; ?>')" title="Update">Edit</a></li>
                
                <!-- quotation view -->
                <li><a class="click btn btn-xs btn-default" data-item-id ="<?php echo $data['id']; ?>" href="#" onClick="actdelete('<?php echo $data['id']; ?>',this)" title="delete">delete</a></li>
            
                <?php if ($data->status != '2') { ?>
                <li><a class="click btn btn-xs btn-default" id="<?php echo $data['id']; ?>" href="#" onClick="approve('<?php echo $data['id']; ?>',this)" title="Approve">Approve</a></li>
                <?php } ?>

            </ul>
        </div>
    </td>
    <td style="width:20px;"><?php echo $index + 1; ?></td>
    <td>
        <?php
        $company = Company::model()->findByPk($data['company_id']);
        echo $company['name'];
        ?>
    </td>
    <td>
        <?php
        $projectModel = Projects::model()->findByPk($data['project_id']);
        echo !empty($projectModel)?CHtml::encode($projectModel->name):"";
        ?>
    </td>
    <td>
        <?php
        $subModel = Subcontractor::model()->findByPk($data['subcontractor_id']);
        echo CHtml::encode($subModel->subcontractor_name);
        ?>
    </td>
    <td>
        <?php
        $expensehead = ExpenseType::model()->findByPk($data["expensehead_id"]);
        echo CHtml::encode($expensehead->type_name);
        ?>
    </td>
    <td style="white-space:nowrap;"><?php echo CHtml::encode($data['Date']); ?></td>
    <td style="white-space:nowrap;"><?php echo CHtml::encode($data['description']); ?></td>
    <td>
    <?php
    // Assuming $id is the material_entry_id
    $materialItems = MaterialItems::model()->findAllByAttributes(['material_entry_id' => $id]);
    $materialData = [];

    if ($materialItems) {
        foreach ($materialItems as $material) {
            // Find the corresponding material in the Materials model
            $materialDetail = Materials::model()->findByPk($material->material);
            
            if ($materialDetail) {
                // Encode the 'material_name' value and add it to the array
                $materialData[] = CHtml::encode($materialDetail->material_name);
            }
        }
        
        echo implode(", ", $materialData);
    } else {
        echo "No material found.";
    }
    ?>


    </td>
    
</tr>

<style>
    .permission_style {
        background: #f8cbcb !important;
    }
   
    .row_class{
        background-color: #f8cbcb !important;
    }

</style>