<?php
/* @var $this UsersController */
/* @var $model Users */
/* @var $form CActiveForm */
?>
<?php Yii::app()->clientScript->registerCss('myscript','
    .row{
        padding-left:25px;
    }
    .form-control{
        padding:5px;
        width:150px;
   }
   row .buttons{
       paddding-left:100px;
   }
   .addRow {
  padding: 0px;
   }
 
.example1consolesucces {
color: white;
background: #247B17;
padding: 5px;
width:200px;
}
  
   ');
?>
<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'users-form',
        'enableAjaxValidation' => true,
        'enableClientValidation' => true,
        'clientOptions' => array('validateOnSubmit' => true,),
            ));
    ?>

    <div class="row">
        <?php echo $form->labelEx($model, 'first_name'); ?>
        <?php echo $form->textField($model, 'first_name', array('size' => 30, 'maxlength' => 30)); ?>
<?php echo $form->error($model, 'first_name'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'last_name'); ?>
        <?php echo $form->textField($model, 'last_name', array('size' => 30, 'maxlength' => 30)); ?>
<?php echo $form->error($model, 'last_name'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'email'); ?>
        <?php echo $form->textField($model, 'email', array('size' => 60, 'maxlength' => 70)); ?>
<?php echo $form->error($model, 'email'); ?>
    </div>

    <div class="row buttons">
        <div class="parl_btn"><?php echo CHtml::submitButton($model->isNewRecord ? 'Submit' : 'Save'); ?></div>
        <div class="parl_btn"><?php echo CHtml::link('Cancel', array('users/myprofile'), array('class'=>'goback_btn')); ?></div>
    <br clear='all'/>
    </div>
   
<?php $this->endWidget(); ?>

</div><!-- form -->