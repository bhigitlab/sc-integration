<?php
/* @var $this BillsController */
/* @var $model Bills */

$this->breadcrumbs = array(
    'Bills' => array('index'),
    $model->bill_id => array('view', 'id' => $model->bill_id),
    'Update',
);

$this->menu = array(
    //array('label'=>'List Bills', 'url'=>array('index')),
    //array('label'=>'Create Bills', 'url'=>array('create')),
    //array('label'=>'View Bills', 'url'=>array('view', 'id'=>$model->bill_id)),
    //array('label'=>'Manage Bills', 'url'=>array('admin')),
);
?>

<div class="container" id="expense">
    <div class="loading-overlay">
        <span class="fa fa-spinner fa-3x fa-spin"></span>
    </div>
    <div class="clearfix">
        <div class="add-btn pull-right">
            <a href="index.php?r=bills/admin" class="button btn btn-primary " title="List Bills">List Bills</a>
        </div>
        <h2>Update Bills</h2>
        <div id="loading"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/ajax-loader.gif"></div>
    </div>

    <div class="">
        <div class="">
            <?php $this->renderPartial('_updateform', array('model' => $model, 'client' => $client, 'additional_items' => $additional_items, 'model2' => $model2)); ?>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#loading').hide();
    });
</script>