<div class="parent_row">
    <div class="row">
        
        <div class="col-md-1 logo_sec">
        <input type="checkbox" name="clearnotificationcheck[]" class="clearnotificationcheck" id="clearnotification<?php $index; ?>" value="<?php echo $data['id']; ?>" data-type="notification" style="vertical-align: text-bottom;"/>
            <button class="btn_logo"><i class="fa fa-bell" aria-hidden="true"></i></button>
        </div>
        <div class="col-md-9 content_sec">
            <div class="row  clearfix">
                <div class="col-md-8">
                    <div class="list-item">
                    <?php  echo $data->message;?>
                    </div>               
                </div>                
                <div class="col-md-4">
                    <div class="list-item pull-right">
                        <label>Date:</label>
                        <span><?php echo date('d-m-Y',strtotime($data['date'])); ?></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-2">
            <div class="list-item text-right act_btn">
            <?php  if($data->requested_by == Yii::app()->user->id){?>
                <span style="padding-left:5px;cursor:pointer" id="<?= $data['id']?>"class="view_not">
                    <i class="fa fa-eye" aria-hidden="true"></i>
                </span>
            <?php  } ?>
            </div>
        </div>
    </div>
</div>
<script>
    
   
    $(document).on('change', '.clearnotificationcheck', function() {
        if ($('.clearnotificationcheck:checked').length > 0) {
            $('#clearNotification').show();
        } else {
            $('#clearNotification').hide();
        }
        });
</script>