ALTER TABLE `jp_bills` ADD CONSTRAINT `fk_bill_warehouse_id` FOREIGN KEY (`warehouse_id`) REFERENCES `jp_warehouse`(`warehouse_id`) ON DELETE RESTRICT ON UPDATE CASCADE; 

ALTER TABLE `jp_warehousereceipt` ADD CONSTRAINT `fk_warehousereceipt_from_id` FOREIGN KEY (`warehousereceipt_warehouseid_from`) REFERENCES `jp_warehouse`(`warehouse_id`) ON DELETE RESTRICT ON UPDATE CASCADE; 

ALTER TABLE `jp_warehousereceipt` ADD CONSTRAINT `fk_warehousereceipt_wh_id` FOREIGN KEY (`warehousereceipt_warehouseid`) REFERENCES `jp_warehouse`(`warehouse_id`) ON DELETE RESTRICT ON UPDATE RESTRICT; 

INSERT INTO `jp_menu` (`menu_id`, `menu_name`, `parent_id`, `status`, `controller`, `action`, `params`, `showmenu`, `show_list`, `related`) VALUES ('788', 'Delete Warehouse', '704', '0', 'wh/warehouse', 'delete', '', '2', '1', NULL);

INSERT INTO `jp_menu` (`menu_id`, `menu_name`, `parent_id`, `status`, `controller`, `action`, `params`, `showmenu`, `show_list`, `related`) VALUES ('789', 'Edit Warehouse', '704', '0', 'wh/warehouse', 'update', '', '2', '1', NULL) ;

INSERT INTO `jp_menu` (`menu_id`, `menu_name`, `parent_id`, `status`, `controller`, `action`, `params`, `showmenu`, `show_list`, `related`) VALUES ('790', 'Delete', '208', '0', 'vendors', 'deletevendor', '', '2', '1', NULL) ;

ALTER TABLE `jp_purchase` ADD CONSTRAINT `fk_purchase_vendor_id` FOREIGN KEY (`vendor_id`) REFERENCES `jp_vendors`(`vendor_id`) ON DELETE RESTRICT ON UPDATE CASCADE; 

ALTER TABLE `jp_dailyvendors` ADD CONSTRAINT `fk_paymennt_vendor_id` FOREIGN KEY (`vendor_id`) REFERENCES `jp_vendors`(`vendor_id`) ON DELETE RESTRICT ON UPDATE CASCADE; 

ALTER TABLE `jp_expenses` ADD CONSTRAINT `fk_expense_vendor_id` FOREIGN KEY (`vendor_id`) REFERENCES `jp_vendors`(`vendor_id`) ON DELETE RESTRICT ON UPDATE CASCADE;