<?php

/**
 * UserRegistration class.
 */
class UserRegister extends Users {

    public $user_type;
    public $first_name;
    public $last_name;
    public $username;
    public $email;
    public $password;
    public $retype_pwd;
    public $verifyCode;

    /**
     * Declares the validation rules.
     */
    public function rules() {
        return array(
            // name, email, subject and body are required
            array('first_name, last_name, username, password, retype_pwd, email', 'required', 'message' => 'Enter your {attribute}'),
            // email has to be a valid email address
            array('user_type', 'required', 'message' => 'Select your user type'),
            array('email', 'email'),
            array('username, email', 'unique'),
            array('user_type', 'in', 'range' => array('5', '6'), 'allowEmpty' => false, 'message' => 'Unknown user type. Refresh page and try again'),
            array('first_name, last_name', 'match', 'pattern' => '/^[A-Za-z\s]+$/u', 'message' => 'Invalid {attribute}'),
            array('username', 'length', 'min' => '5'),
            array('password', 'safe'),
            array('username', 'match', 'pattern' => '/^[A-Za-z0-9_]+$/u'),
            array('retype_pwd', 'compare', 'compareAttribute' => 'password', 'message' => "Password mismatch"),
            // verifyCode needs to be entered correctly
            array('verifyCode', 'captcha', 'allowEmpty' => !CCaptcha::checkRequirements()),
        );
    }

    /**
     * Declares customized attribute labels.
     * If not declared here, an attribute would have a label that is
     * the same as its name with the first letter in upper case.
     */
    public function attributeLabels() {
        return array(
            'user_type' => 'Profile type',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'username' => 'Username',
            'password' => 'Password',
            'email' => 'Email',
            'verifyCode' => 'Verification code',
            'retype_pwd' => 'Retype password',
        );
    }

}