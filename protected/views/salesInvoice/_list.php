<?php
$tblpx = Yii::app()->db->tablePrefix;
if ($index == 0) {
?>
    <thead>
        <tr>
            <th class="">Company</th>
            <th>Project</th>
            <th>Client</th>
            <th>Invoice Date</th>
            <th>Invoice Number</th>
            <th>Total Amount</th>
            <?php
            if ((isset(Yii::app()->user->role) && ((in_array('/vendors/paymentedit', Yii::app()->user->menuauthlist)) || (in_array('/vendors/paymentdelete', Yii::app()->user->menuauthlist))))) {
            ?>
                <th></th>
            <?php } ?>

        </tr>
    </thead>
<?php } ?>
<tr id="<?php echo $index; ?>" class="" title="">
    <td class="pro_back wrap"><?php echo $data->company->name; ?></td>
    <td class="pro_back wrap"><?php echo $data->project->name; ?></td>
    <td class="pro_back wrap"><?php echo $data->client->name; ?></td>
    <td class="pro_back wrap"><?php echo date('d-M-Y', strtotime($data->invoice_date)); ?></td>
    <td class="pro_back wrap"><?php echo $data->invoice_number; ?></td>
    <td class="pro_back wrap"><?php echo $data->getTotal($data); ?></td>
    <td>
        <?php
        if ((in_array('/salesInvoice/update', Yii::app()->user->menuauthlist)) || (in_array('/salesInvoice/view', Yii::app()->user->menuauthlist))) {
        ?>
            <span class="icon icon-options-vertical popover-test" data-toggle="popover" data-placement="left" type="button" data-html="true" style="cursor: pointer;"></span>
            <div class="popover-content hide">
                <ul class="tooltip-hiden">
                    <?php
                    if ((in_array('/salesInvoice/update', Yii::app()->user->menuauthlist))) {
                    ?>
                        <li><button id="<?php echo $data->id; ?>" class="btn btn-xs btn-default edit_invoice">Edit</button></li>

                    <?php }
                    ?>
                    <?php
                    if ((in_array('/salesInvoice/view', Yii::app()->user->menuauthlist))) {
                    ?>
                        <li><button id="<?php echo $data->id; ?>" class="btn btn-xs btn-default view_invoice">View</button></li>
                    <?php
                    }
                    if ((in_array('/vendors/paymentdelete', Yii::app()->user->menuauthlist))) {
                    ?>
                        <!-- <li><button id="<?php echo $data->id; ?>" class="btn btn-xs btn-default delete_invoice">Delete</button></li> -->
                    <?php } ?>
                </ul>
            </div>
        <?php

        }

        ?>

    </td>
</tr>
<script>
    $(document).ready(function() {
        $(".popover-test").popover({
            html: true,
            content: function() {
                return $(this).next('.popover-content').html();
            }
        });
        $('[data-toggle=popover]').on('click', function(e) {
            $('[data-toggle=popover]').not(this).popover('hide');
        });
        $('body').on('hidden.bs.popover', function(e) {
            $(e.target).data("bs.popover").inState.click = false;
        });
        $('body').on('click', function(e) {
            $('[data-toggle=popover]').each(function() {
                if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                    $(this).popover('hide');
                }
            });
        });


    });
</script>