<?php

/**
 * This is the model class for table "{{quotation_section}}".
 *
 * The followings are the available columns in table '{{quotation_section}}':
 * @property integer $id
 * @property integer $qtn_id
 * @property string $section_name
 * @property integer $unit
 * @property integer $quantity
 * @property integer $quantity_nos
 * @property double $mrp
 * @property double $amount_after_discount
 * @property integer $description
 */
class QuotationSection extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{quotation_section}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('qtn_id, section_name', 'required'),
            array('qtn_id, unit, quantity, description', 'numerical', 'integerOnly'=>true),
            array('mrp, amount_after_discount', 'numerical'),
            array('section_name,hsn_code', 'length', 'max'=>200),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, qtn_id, section_name, unit,hsn_code quantity, quantity_nos, mrp, amount_after_discount, description', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'qtn_id' => 'Qtn',
            'section_name' => 'Section Name',
            'unit' => 'Unit',
            'quantity' => 'Quantity',
            'quantity_nos' => 'Quantity Nos',
            'mrp' => 'Mrp',
            'amount_after_discount' => 'Amount After Discount',
            'description' => 'Description',
            'hsn_code'=> 'HSN Code',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('qtn_id',$this->qtn_id);
        $criteria->compare('section_name',$this->section_name,true);
        $criteria->compare('unit',$this->unit);
        $criteria->compare('quantity',$this->quantity);
        $criteria->compare('quantity_nos',$this->quantity_nos);
        $criteria->compare('mrp',$this->mrp);
        $criteria->compare('amount_after_discount',$this->amount_after_discount);
        $criteria->compare('description',$this->description);
        $criteria->compare('hsn_code',$this->hsn_code);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return QuotationSection the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}