<?php
$tblpx = Yii::app()->db->tablePrefix;
$daybookSql = "SELECT p.name as projectname, a.projectcount, "
    . " b.totalamount, c.totalpaid, d.totaltds, e.totalpaidtovendor,"
    . "  f.sumtotalamount, g.sumtotalpaid, h.sumtotaltds, "
    . " i.sumtotalpaidtovendor, bl.bill_number "
    . " as billnumber, bl.bill_totalamount, ex.* ,ex.type as exp_type_id"
    . " FROM {$tblpx}expenses ex"
    . " LEFT JOIN {$tblpx}projects p "
    . " ON ex.projectid = p.pid"
    . " LEFT JOIN {$tblpx}bills bl "
    . " ON ex.bill_id = bl.bill_id"
    . " LEFT JOIN (SELECT projectid, count(*) "
    . " as projectcount FROM {$tblpx}expenses "
    . " GROUP BY projectid) a ON a.projectid = ex.projectid"
    . " LEFT JOIN (SELECT projectid, SUM(amount) as totalamount "
    . " FROM {$tblpx}expenses WHERE  "
    . " (reconciliation_status=1 OR reconciliation_status IS NULL) GROUP BY projectid) b "
    . " ON b.projectid = ex.projectid"
    . " LEFT JOIN (SELECT projectid, SUM(paid) as totalpaid "
    . " FROM {$tblpx}expenses WHERE  "
    . "  (reconciliation_status=1 OR reconciliation_status IS NULL) "
    . " GROUP BY projectid) c ON c.projectid = ex.projectid"
    . " LEFT JOIN (SELECT projectid, SUM(expense_tds) as totaltds "
    . " FROM {$tblpx}expenses WHERE  "
    . "  (reconciliation_status=1 OR reconciliation_status IS NULL) GROUP BY projectid) d "
    . " ON d.projectid = ex.projectid"
    . " LEFT JOIN (SELECT projectid, SUM(paidamount) "
    . " as totalpaidtovendor FROM {$tblpx}expenses "
    . " WHERE expense_type != 0 AND  "
    . "  (reconciliation_status=1 OR reconciliation_status IS NULL) GROUP BY projectid) e "
    . " ON e.projectid = ex.projectid"
    . " LEFT JOIN (SELECT projectid, SUM(amount) as sumtotalamount "
    . " FROM {$tblpx}expenses WHERE  "
    . "  (reconciliation_status=1 OR reconciliation_status IS NULL)) f ON 1 = 1"
    . " LEFT JOIN (SELECT projectid, SUM(paid) as sumtotalpaid "
    . " FROM {$tblpx}expenses WHERE  "
    . "  (reconciliation_status=1 OR reconciliation_status IS NULL)) g ON 1 = 1"
    . " LEFT JOIN (SELECT projectid, SUM(expense_tds)"
    . " as sumtotaltds FROM {$tblpx}expenses "
    . " WHERE "
    . "  (reconciliation_status=1 OR reconciliation_status IS NULL)) h ON 1 = 1"
    . " LEFT JOIN (SELECT projectid, SUM(paidamount) "
    . " as sumtotalpaidtovendor FROM {$tblpx}expenses "
    . " WHERE expense_type != 0  "
    . " AND (reconciliation_status=1 OR reconciliation_status IS NULL)) i ON 1 = 1"
    . " WHERE  (ex.reconciliation_status=1 OR ex.reconciliation_status IS NULL) "
    . " ORDER BY ex.projectid";
// $daybookPayment = Yii::app()->db->createCommand($daybookSql)->queryAll();

// $daybookPayments = !empty($daybookPayment) ? $daybookPayment : array();
$daybookPayments = isset($daybookdata) ? $daybookdata : array();

$final_array = array();
foreach ($daybookPayments as $daybookPayment) {
    $daybookPayment['key'] = 1;
    $final_array[] = $daybookPayment;
}

$id = array_column($final_array, 'exp_id');
array_multisort($id, SORT_ASC, $final_array);
//echo "<pre>";print_r($final_array);exit;
?>
<div class="container">
    <div class="sub-heading secdiv mb-10">
        <h3>Day Book - Ledger View</h3>
        <a style="cursor:pointer; " class="btn btn-info"
            href="<?php echo Yii::app()->createUrl('expenses/newlist') ?>">List View</a>
    </div>
    <div class="table-responsive">
        <table class="table ledger_table ureconcil_table">
            <thead class="entry-table sticky-thead">
                <tr>
                    <th>Date</th>
                    <th>Reff</th>
                    <th>Narration</th>
                    <th>Receipt</th>
                    <th>Payments</th>
                    <th>Balance</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $balanceledger = 0;
                foreach ($final_array as $ledger_summary) {
                    if ($ledger_summary['payment_type'] == 88 || $ledger_summary['payment_type'] == 89) {
                        if ($ledger_summary['exp_type_id'] == 72) {
                            $balanceledger = $balanceledger + $ledger_summary['receipt'];
                        } else {
                            $balanceledger = $balanceledger - $ledger_summary['paid'];
                        }
                    }
                    ?>
                    <tr>
                        <td class="nowrap">
                            <?php echo Yii::app()->controller->changeDateForamt($ledger_summary['date']); ?>
                        </td>

                        <td>
                            <?php
                            if ($ledger_summary['bill_id'] != "") {
                                $billno = Yii::app()->db->createCommand("SELECT bill_number, bill_id from {$tblpx}bills where bill_id='" . $ledger_summary['bill_id'] . "'")->queryAll();
                                foreach ($billno as $key => $value) {
                                    $ledger_summary['billnumber'] = $value['bill_number'];
                                }
                            }
                            if (isset($ledger_summary['billnumber'])) {
                                echo CHtml::link($ledger_summary['billnumber'], Yii::app()->createUrl('bills/view&id=' . $ledger_summary['bill_id']), array('class' => '', 'target' => '_blank'));
                            } else if (isset($ledger_summary['bill_number'])) {
                                echo CHtml::link($ledger_summary['bill_number'], Yii::app()->createUrl('bills/view&id=' . $ledger_summary['bill_id']), array('class' => '', 'target' => '_blank'));
                            } else {
                                echo "--";
                            }
                            ?>
                        </td>
                        <td>
                            <?php

                            if (isset($ledger_summary['des'])) {
                                echo $ledger_summary['des'];
                            } else {
                                echo "--";
                            }
                            ?>

                        </td>
                        <td class="text-right">
                            <?php
                            if ($ledger_summary['exp_type_id'] == 72) {

                                if ($ledger_summary['purchase_type'] == 1) {
                                    echo ($ledger_summary['amount'] ? Controller::money_format_inr($ledger_summary['amount'], 2, 1) : 0);
                                } else {
                                    echo ($ledger_summary['receipt'] ? Controller::money_format_inr($ledger_summary['receipt'], 2, 1) : 0);
                                }
                            } else {
                                echo "0.00";
                            }
                            ?>
                        </td>
                        <td class="text-right">
                            <?php
                            if ($ledger_summary['key'] == 1) {
                                if ($ledger_summary['exp_type_id'] == 73) {
                                    if ($ledger_summary['purchase_type'] == 1) {
                                        echo ($ledger_summary['amount'] ? Controller::money_format_inr($ledger_summary['amount'], 2, 1) : 0);
                                    } else {
                                        echo ($ledger_summary['paid'] ? Controller::money_format_inr($ledger_summary['paid'], 2, 1) : 0);
                                    }
                                }
                            }
                            ?>
                        </td>
                        <td class="text-right">
                            <?php echo Yii::app()->Controller->money_format_inr($balanceledger, 2); ?>
                        </td>
                    </tr>

                    <?php
                    //} 
                }
                ?>
            </tbody>
            <tfoot class="entry-table">
                <tr>
                    <th colspan="5" class="text-right">Closing Balance:</th>
                    <th class="text-right">
                        <?php echo Yii::app()->Controller->money_format_inr($balanceledger, 2) ?>
                    </th>
                </tr>
            </tfoot>
        </table>
    </div>
</div>