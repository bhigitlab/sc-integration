<?php

/**
 * This is the model class for table "{{warehouse_stock_correction}}".
 *
 * The followings are the available columns in table '{{warehouse_stock_correction}}':
 * @property integer $id
 * @property integer $warehouse_id
 * @property integer $wh_receipt_id
 * @property integer $wh_receipt_item_id
 * @property integer $Item_Id
 * @property double $wh_receipt_quantity
 * @property double $stock_quantity
 * @property integer $created_by
 * @property string $created_date
 */
class WarehouseStockCorrection extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{warehouse_stock_correction}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('warehouse_id,  Item_Id, wh_receipt_quantity, stock_quantity, created_by, created_date', 'required'),
            array('warehouse_id, wh_receipt_id, wh_receipt_item_id, Item_Id, created_by', 'numerical', 'integerOnly'=>true),
            array('wh_receipt_quantity, stock_quantity', 'numerical'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, warehouse_id, wh_receipt_id, wh_receipt_item_id, Item_Id, wh_receipt_quantity, stock_quantity, created_by, created_date', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'warehouse_id' => 'Warehouse',
            'wh_receipt_id' => 'Wh Receipt',
            'wh_receipt_item_id' => 'Wh Receipt Item',
            'Item_Id' => 'Item',
            'wh_receipt_quantity' => 'Wh Receipt Quantity',
            'stock_quantity' => 'Stock Quantity',
            'created_by' => 'Created By',
            'created_date' => 'Created Date',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('warehouse_id',$this->warehouse_id);
        $criteria->compare('wh_receipt_id',$this->wh_receipt_id);
        $criteria->compare('wh_receipt_item_id',$this->wh_receipt_item_id);
        $criteria->compare('Item_Id',$this->Item_Id);
        $criteria->compare('wh_receipt_quantity',$this->wh_receipt_quantity);
        $criteria->compare('stock_quantity',$this->stock_quantity);
        $criteria->compare('created_by',$this->created_by);
        $criteria->compare('created_date',$this->created_date,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return WarehouseStockCorrection the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}