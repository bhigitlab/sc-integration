<?php
/* @var $this SubcontractorController */
/* @var $model Subcontractor */
/* @var $form CActiveForm */
?>


	<div class="page_filter clearfix">
		<?php $form=$this->beginWidget('CActiveForm', array(
			'action'=>Yii::app()->createUrl($this->route),
			'method'=>'get',
		)); ?>
		<div class="filter_elem">
			<?php echo $form->textField($model,'subcontractor_name',array('class'=>'form-control','placeholder' => 'Name')); ?>
		</div>
		<div class="filter_elem filter_btns">
    	<input type='reset' value='Clear' id='clear'>
		</div>
		<?php $this->endWidget(); ?>
	</div>
