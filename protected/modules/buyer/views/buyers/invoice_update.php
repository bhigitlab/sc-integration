<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.min.js"></script>

<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/message/css/jquery.message.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/message/js/jquery.message.js"></script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>

<script>
    var shim = (function() {
        document.createElement('datalist');
    })();
</script>
<style>
    .lefttdiv {
        float: left;
    }

    .select2 {
        width: 100%;
    }

    .company_id {
        display: inline;
    }
</style>
<script>
    $(function() {
        var update_date = $('#datepicker').val();
        $("#datepicker").datepicker({
            dateFormat: 'dd-mm-yy'
        }).datepicker("setDate", new Date(update_date));

    });
</script>
<style>
    .dropdown {
        position: relative;
        display: inline-block;
    }

    .dropdown::before {
        position: absolute;
        content: " \2193";
        top: 0px;
        right: -8px;
        height: 20px;
        width: 20px;
    }

    button#caret {
        border: none;
        background: none;
        position: absolute;
        top: 0px;
        right: 0px;
    }

    .invoicemaindiv th,
    .invoicemaindiv td {
        padding: 10px;
        text-align: left;
    }

    .addcolumn {
        cursor: pointer;
        background-color: black;
        border: none;
        color: white;
        text-align: center;
        text-decoration: none;
        margin: 2px;
        font-size: 30px;
        width: 40px;
    }

    .invoicemaindiv .pull-right,
    .invoicemaindiv .pull-left {
        float: none !important;
    }


    .checkek_edit {
        pointer-events: none;
    }



    .add_selection {
        background: #000;
    }




    .toast-container {
        width: 350px;
    }

    .toast-position-top-right {
        top: 57px;
        right: 6px;
    }

    .toast-item-close {
        background-image: none;
        cursor: pointer;
        width: 12px;
        height: 12px;
        text-align: center;
        border-radius: 2px;
    }

    .toast-item-image {
        font-size: 24px;
    }

    .toast-item-close:hover {
        color: red;
    }

    .toast-item {
        border: transparent;
        border-radius: 3px;
        font-size: 10px;
        opacity: 1;
        background-color: rgba(34, 45, 50, 0.8);
    }

    .toast-item-wrapper p {
        margin: 0px 5px 0px 42px;
        font-size: 14px;
        text-align: justify;
    }


    .toast-type-success {
        background-color: #00A65A;
        border-color: #00A65A;
    }

    .toast-type-error {
        background-color: #DD4B39;
        border-color: #DD4B39;
    }

    .toast-type-notice {
        background-color: #00C0EF;
        border-color: #00C0EF;
    }

    .toast-type-warning {
        background-color: #F39C12;
        border-color: #F39C12;
    }

    .span_class {
        min-width: 70px;
        display: inline-block;
    }

    .purchase-title {
        border-bottom: 1px solid #ddd;
    }

    .purchase_items h3 {
        font-size: 18px;
        color: #333;
        margin: 0px;
        padding: 0px;
        text-align: left;
        padding-bottom: 10px;
    }

    .purchase_items h4 {
        margin: 0px 15px 10px 15px !important;
        line-height: 24px;
        background-color: #fafafa;
        font-size: 16px;
    }

    .purchase_items {
        padding: 15px;
        box-shadow: 0 0 13px 1px rgba(0, 0, 0, 0.25);
        margin-bottom: 15px;
    }

    .purchaseitem {
        display: inline-block;
        margin-right: 20px;
    }

    .purchaseitem .form-control {
        height: 28px;
        display: inline-block;
        padding: 6px 3px;
    }

    .purchaseitem label {
        display: block;
    }

    .purchaseitem last-child {
        margin-right: 0px;
    }

    .purchaseitem label {
        font-size: 12px;
    }

    .remark {
        display: none;
    }

    .padding-box {
        padding: 3px 0px;
        min-height: 17px;
        display: inline-block;
    }

    th {
        height: auto;
    }

    .quantity,
    .rate {
        max-width: 80px;
    }

    .p_block {
        margin-bottom: 10px;
        padding: 0px 15px;
    }

    .gsts input {
        width: 50px;
    }

    .amt_sec {
        float: right;
    }

    .amt_sec:after,
    .padding-box:after {
        display: block;
        content: '';
        clear: both;
    }

    .client_data {
        margin-top: 6px;
    }

    #previous_details {
        position: fixed;
        top: 94px;
        left: 70px;
        right: 70px;
        z-index: 2;
        max-width: 1150px;
        margin: 0 auto;
    }

    .text_align {
        text-align: center;
    }

    *:focus {
        border: 1px solid #333;
        box-shadow: 0 0 6px 1px rgba(0, 0, 0, 0.25);
    }

    .tooltip-hiden {
        width: auto;
    }

    @media(max-width: 767px) {
        .purchaseitem {
            display: block;
            padding-bottom: 5px;
            margin-right: 0px;
        }

        .quantity,
        .rate,
        .small_class {
            width: auto !important;
        }

        .p_block {
            margin-bottom: 0px;
        }

        /* .hiden{display:block !important;} */
        .padding-box {
            float: right;
        }

        #tax_amount,
        #item_amount {
            display: inline-block;
            float: none;
            text-align: right;
        }
    }

    @media(min-width: 767px) {
        .invoicemaindiv .pull-right {
            float: right !important;
        }

        .invoicemaindiv .pull-left {
            float: left !important;
        }
    }
</style>
<div class="container">

    <div class="invoicemaindiv">

        <h2 class="purchase-title">Buyer Invoice</h2>
        <br>
        <div id="msg_box"></div>
        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'pdfvals1',
            'enableAjaxValidation' => false,
            'action' => 'buyer/buyers/updateinvoice',
        ));
        ?>
        <input type="hidden" name="remove" id="remove" value="">
        <input type="hidden" name="invoice_id" id="invoice_id" value="<?php echo $model->buyer_invoice_id; ?>">
        <div class="row form-fields">
            <div class="col-md-2  col-sm-6">
                <label>COMPANY : </label>
                <?php
                echo $form->dropDownList($model, 'company_id', $company_list, array('class' => 'inputs target company_id form-control', 'empty' => '-Select Company-'));
                ?>
                <?php
                $tblpx      = Yii::app()->db->tablePrefix;
                $user = Users::model()->findByPk(Yii::app()->user->id);
                $arrVal = explode(',', $user->company_id);
                $newQuery = "";
                $newQuery1 = "";
                foreach ($arrVal as $arr) {
                    if ($newQuery) $newQuery .= ' OR';
                    if ($newQuery1) $newQuery1 .= ' OR';
                    $newQuery .= " FIND_IN_SET('" . $arr . "', company_id)";
                    $newQuery1 .= " FIND_IN_SET('" . $arr . "', p.company_id)";
                }
                //  $company = Company::model()->findByPk($model->company_id);
                if (Yii::app()->user->role == 1 || Yii::app()->user->role == 2) {
                    $project_   = Yii::app()->db->createCommand("SELECT pid, name  FROM {$tblpx}projects WHERE " . $newQuery . " ORDER BY name")->queryAll();
                } else {
                    $project_    = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}projects p JOIN {$tblpx}project_assign pa ON p.pid = pa.projectid WHERE " . $newQuery1 . " AND pa.userid = '" . Yii::app()->user->id . "'")->queryAll();
                }


                //  echo $company['name']; 
                ?>
            </div>
            <div class="col-md-2">
                <label>Buyer : </label>

                <?php
                echo $form->dropDownList($model, 'buyer_id', $buyer_list, array('class' => 'inputs target company_id form-control', 'empty' => '-Select Buyer-'));
                ?>

            </div>
            <div class="col-md-2">
                <label>PROJECT : </label>
                <?php
                echo $form->dropDownList($model, 'project_id', $project_list, array('class' => 'inputs target company_id form-control', 'empty' => '-Select Project-', 'id' => 'project_id', 'disabled' => 'disabled'));
                ?>
                <input type="hidden" id="BuyerInvoice_project_id" name="BuyerInvoice[project_id]" value="<?= $model->project_id ?>">
                <?php //echo $project->name; 
                ?>
            </div>
            <div class="col-md-2">
                <label for="project">Apartment No</label>
                <?php
                echo $form->dropDownList($model, 'flat_id', $flat_list, array('class' => 'inputs target company_id form-control', 'empty' => '-Select Apartment No-', 'id' => 'flat_id', 'disabled' => 'disabled'));
                ?>
                <input type="hidden" id="BuyerInvoice_flat_id" name="BuyerInvoice[flat_id]" value="<?= $model->flat_id ?>">
            </div>
            <div class="col-md-2">
                <label>DATE : </label>

                <input type="text" value="<?= $model->date ?>" id="datepicker" class="txtBox date inputs target form-control" name="date" placeholder="Please click to edit" style="width:190px;">

            </div>
            <div class="col-md-2">
                <label>INVOICE NO : </label>
                <div id='invoice_div'><?php echo $model->invoice_no; ?> </div>
            </div>
            <div class="col-md-2">
                <label style="display:block;">&nbsp;</label>
                <a id="sales_edit_form" class="btn btn-primary btn-sm">Submit</a>
            </div>
        </div>
        <div class="clearfix"></div>
        <br>
        <div id="msg"></div>

        <div id="previous_details"></div>

        <div class="purchase_items">
            <h4>Invoice Entries</h4>
            <div class="p_block clearfix">
                <div class="purchaseitem ">
                    <label>Description:</label>
                    <input type="text" class="inputs target txtBox description form-control" id="description" name="BuyerInvoiceList[description][]" placeholder="" /></td>
                </div>
                <div class="purchaseitem ">
                    <label>Transaction type:</label>
                    <?php
                    $transaction_type = array('1' => 'Sales', '2' => 'Journal');
                    echo $form->dropDownList($invoicelist, 'transaction_type', $transaction_type, array('class' => 'form-control js-example-basic-single', 'empty' => '-Select Transaction Type-'));
                    ?>
                </div>
                <div class="purchaseitem quantity_div">
                    <label>Amount:</label>
                    <input type="text" class="inputs target txtBox amount allownumericdecimal form-control" id="amount" name="BuyerInvoiceList[amount][]" placeholder="" /></td>
                </div>
                <div class="purchaseitem amt_sec">
                    <label>&nbsp;</label>
                    <b>Amount: </b>
                    <div id="item_amount" class="padding-box">0.00</div>
                </div>
            </div>
            <div class="p_block clearfix">

                <div class="purchaseitem gsts">
                    <label>SGST(%):</label>
                    <input type="text" class="inputs target small_class txtBox sgst allownumericdecimal calculation form-control" id="sgst" name="BuyerInvoiceList[sgst_p][]" placeholder="" />
                    <div id="sgst_amount" class="padding-box">0.00</div>
                </div>
                <div class="purchaseitem gsts">
                    <label>CGST(%):</label>
                    <input type="text" class="inputs target txtBox cgst small_class allownumericdecimal calculation form-control" id="cgst" name="BuyerInvoiceList[cgst_p][]" placeholder="" />
                    <div id="cgst_amount" class="padding-box">0.00</div>
                </div>

                <div class="purchaseitem gsts">
                    <label>IGST(%):</label>
                    <input type="text" class="inputs target txtBox igst  small_class allownumericdecimal calculation form-control" id="igst" name="BuyerInvoiceList[igst_p][]" placeholder="" />
                    <div id="igst_amount" class="padding-box">0.00</div>
                </div>
                <div class="purchaseitem quantity_div">
                    <label>Discount Amount:</label>
                    <input type="text" class="inputs target txtBox disc_amount allownumericdecimal form-control" id="discount" name="BuyerInvoiceList[discount_amount][]" placeholder="" /></td>
                </div>

                <div class="purchaseitem amt_sec">
                    <label>&nbsp;</label>
                    <b>Total Amount:</b>
                    <div id="subtotal_amt" class="padding-box">0.00</div>
                </div>
                <div class="purchaseitem amt_sec">
                    <label>&nbsp;</label>
                    <b>Tax Amount:</b>
                    <div id="tax_amount" class="padding-box">0.00</div>
                </div>

            </div>

            <div class="p_block clearfix">
                <div class="purchaseitem">
                    <input type="button" class="item_save btn btn-info" id="0" value="Save">
                </div>
            </div>
        </div>
    </div>
    <br><br>

    <div id="table-scroll" class="table-scroll">
        <div id="faux-table" class="faux-table" aria="hidden"></div>
        <div class="table-wrap">

            <div class="table-responsive">
                <table border="1" class="table main-table" id="main-table">
                    <thead>
                        <tr>


                            <th>Sl.No</th>
                            <th>Description</th>
                            <th>Transaction Type</th>
                            <th>SGST</th>
                            <th>SGST Amount</th>
                            <th>CGST</th>
                            <th>CGST Amount</th>
                            <th>IGST</th>
                            <th>IGST Amount</th>
                            <th>Amount</th>
                            <th>Tax Amount</th>
                            <th>Discount Amount</th>
                            <th>Subtotal Amount</th>

                            <th></th>
                        </tr>
                    </thead>
                    <tbody class="addrow">
                        <?php

                        $i = 1;
                        foreach ($item_model as $new) {
                            if ($new->transaction_type == 1) {
                                $type = 'Sales';
                            } elseif ($new->transaction_type == 2) {
                                $type = "Journal";
                            } else {
                                $type = "";
                            }

                        ?>


                            <tr class="tr_class" id="item_<?php echo $new['id']; ?>">

                                <td>
                                    <div id="item_sl_no"><?php echo $i; ?></div>
                                </td>
                                <td>
                                    <div class="item_description" id=""> <?php echo $new['description']; ?></div>
                                </td>
                                <td>
                                    <div class="item_description" id=""><?php echo $new['transaction_type']; ?></div>
                                </td>
                                <td>
                                    <div class="text-right" id="rate"><?php echo number_format($new['sgst_p'], 2, '.', ''); ?></div>
                                </td>
                                <td>
                                    <div class="text-right" id="rate"><?php echo number_format($new['sgst_val'], 2, '.', ''); ?></div>
                                </td>
                                <td>
                                    <div class="text-right" id="rate"><?php echo number_format($new['cgst_p'], 2, '.', ''); ?></div>
                                </td>
                                <td>
                                    <div class="text-right" id="rate"><?php echo number_format($new['cgst_val'], 2, '.', ''); ?></div>
                                </td>
                                <td>
                                    <div class="text-right" id="rate"><?php echo number_format($new['igst_p'], 2, '.', ''); ?></div>
                                </td>
                                <td>
                                    <div class="text-right" id="rate"><?php echo number_format($new['igst_val'], 2, '.', ''); ?></div>
                                </td>

                                <td>
                                    <div class="amount text-right" id="amount" style="max-width: -3px !important;"> <?php echo number_format($new['amount'], 2, '.', ''); ?></div>
                                </td>

                                <div class="popover-content hide">
                                    <div><span>SGST: (<span class="sgstpval"><?php echo number_format($new['sgst_p'], 2, '.', ''); ?></span>&#37;) <span class="sgstval"><?php echo number_format($new['sgst_val'], 2, '.', ''); ?></span></div>
                                    <div><span>CGST: (<span class="cgstpval"><?php echo number_format($new['cgst_p'], 2, '.', ''); ?></span>&#37;) <span class="cgstval"><?php echo number_format($new['cgst_val'], 2, '.', ''); ?></span></div>
                                    <div><span>IGST: (<span class="igstpval"><?php echo number_format($new['igst_p'], 2, '.', ''); ?></span>&#37;) <span class="igstval"><?php echo number_format($new['igst_val'], 2, '.', ''); ?></span></div>
                                </div>
                                </td>

                                <td>
                                    <div class="amount text-right" id="amount" style="max-width: -3px !important;"> <?php echo number_format($new['tax_total'], 2, '.', ''); ?></div>
                                </td>
                                <td>
                                    <div class="amount text-right" id="discount_a" style="max-width: -3px !important;"> <?php echo number_format($new['discount_amount'], 2, '.', ''); ?></div>
                                </td>
                                <td>
                                    <div class="amount text-right" id="subtotal" style="max-width: -3px !important;"> <?php echo number_format($new['subtotal'], 2, '.', ''); ?></div>
                                </td>

                                <td><span class="icon icon-options-vertical popover-test" data-toggle="popover" data-placement="left" type="button" data-html="true" style="cursor: pointer;"></span>
                                    <div class="popover-content hide">
                                        <ul class="tooltip-hiden">
                                            <li><a href="" id='<?php echo $new['id']; ?>' class="btn btn-xs btn-default removebtn">Delete</a></li>
                                            <li><a href="" id='<?php echo $new['id']; ?>' class=" btn btn-xs btn-default edit_item">Edit</a></li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>

                        <?php
                            $i++;
                        }
                        ?>
                    </tbody>

                    <tfoot>
                        <tr>
                            <th colspan="9" class="text-right">TOTAL: </th>
                            <th class="text-right">
                                <div id="grand_total"><?php echo ($model->total_amount == '') ? '0.00' : number_format($model->total_amount, 2, '.', ''); ?></div>
                            </th>
                            <th class="text-right">
                                <div id="grand_taxamount"><?php echo ($model->tax_amount == '') ? '0.00' : number_format($model->tax_amount, 2, '.', ''); ?>
                            </th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>

                        <tr>
                            <th colspan="13" class="text-right">GRAND TOTAL: <div id="net_amount"><?php echo number_format($model->tax_amount + $model->total_amount, 2, '.', '') ?></div>
                            </th>
                            <th></th>
                        </tr>
                    </tfoot>
                </table>
            </div>


            <input type="hidden" name="final_amount" id="final_amount" value="<?php echo ($model->total_amount == '') ? '0.00' : number_format($model->total_amount, 2, '.', ''); ?>">

            <input type="hidden" name="item_amount_temp" id="item_amount_temp" value="0">

            <input type="hidden" name="final_tax" id="final_tax" value="<?php echo ($model->tax_amount == '') ? '0.00' : number_format($model->tax_amount, 2, '.', ''); ?>">

            <input type="hidden" name="item_tax_temp" id="item_tax_temp" value="0">


        </div>
    </div>


    <div style="padding-right: 0px;" class="col-md-offset-9 col-md-3 col-sm-offset-8 col-sm-4 col-xs-offset-6 col-xs-6">

        <input type="hidden" value="<?php echo ((isset($subtot) && $subtot != '') ? $subtot : ''); ?>" class="txtBox pastweek" readonly=ture name="subtot" /></td>
        <input type="hidden" value="<?php echo ((isset($grand) && $grand != '') ? $grand : ''); ?>" class="txtBox pastweek grand" name="grand" readonly=true />



        <?php //  echo $model->total_amount; 
        ?>

    </div>

    <br><br>

    <?php $this->endWidget(); ?>
</div>



<style>
    .error_message {
        color: red;
    }

    a.pdf_excel {
        background-color: #6a8ec7;
        display: inline-block;
        padding: 8px;
        color: #fff;
        border: 1px solid #6a8ec8;
    }
</style>
<?php $getBuyerList = Yii::app()->createAbsoluteUrl("/buyer/default/getBuyers");
$getprojecstUrl = Yii::app()->createAbsoluteUrl("/buyer/default/getProjects");
$editInvoiceUrl = Yii::app()->createAbsoluteUrl("/buyer/buyers/editinvoice");
$getProjectAndFlats = Yii::app()->createAbsoluteUrl("/buyer/default/getProjectAndFlats");
$addInvoiceItemUrl = Yii::app()->createAbsoluteUrl("/buyer/buyers/addInvoiceItem");
$updateInvoiceItemUrl = Yii::app()->createAbsoluteUrl("/buyer/buyers/updateInvoiceItem");
$removeItemUrl = Yii::app()->createAbsoluteUrl("/buyer/buyers/removeinvoiceItem"); ?>
<script>
    $(document).ready(function() {
        $("#BuyerInvoice_company_id").change(function() {
            var company_id = $("#BuyerInvoice_company_id").val();
            $("#BuyerInvoice_buyer_id").val('').trigger('change.select2');
            $("#project_id").val('').trigger('change.select2');
            $("#BuyerInvoice_project_id").val('');
            $("#flat_id").val('').trigger('change.select2');
            $("#BuyerInvoice_flat_id").val('');
            if (company_id !== "") {
                getBuyerList(company_id);
                getProjectData();

            }
        });
        $("#BuyerInvoice_buyer_id").change(function() {
            var buyer_id = $("#BuyerInvoice_buyer_id").val();
            $("#project_id").val('').trigger('change.select2');
            $("#BuyerInvoice_project_id").val('');
            $("#flat_id").val('').trigger('change.select2');
            $("#BuyerInvoice_flat_id").val('');
            if (buyer_id !== "") {
                setProjectFlatData(buyer_id);

            }
        });
        $("#flat_id").change(function() {
            var flat_id = $(this).val();
            $('#BuyerInvoice_flat_id').val(flat_id);
        });
        $('#sales_edit_form').click(function() {
            var company = $('#BuyerInvoice_company_id').val();
            var buyer = $('#BuyerInvoice_buyer_id').val();
            var project = $('#BuyerInvoice_project_id').val();
            var flat_no = $('#BuyerInvoice_flat_id').val();
            var date = $('#datepicker').val();
            var invoice_id = $('#invoice_id').val();
            if (company != '' && buyer != '' && project != '' && flat_no != '' && date != '' && invoice_id != '') {
                $.ajax({
                    type: "POST",
                    async: false,
                    data: {
                        company: company,
                        buyer: buyer,
                        project: project,
                        flat_no: flat_no,
                        date: date,
                        invoice_id: invoice_id
                    },
                    dataType: "json",
                    url: '<?php echo $editInvoiceUrl; ?>',
                    success: function(response) {
                        if (response.response == 'success') {
                            $().toastmessage('showSuccessToast', "" + response.msg + "");
                            $('#client_name').text(response.client);
                        }

                    }
                });
            } else {
                console.log(company, buyer, project, flat_no, date, invoice_id);
            }


        });
    });

    function getBuyerList(company_id) {
        $.ajax({
            url: "<?php echo $getBuyerList; ?>",
            data: {
                "company_id": company_id
            },
            type: "POST",
            dataType: 'json',
            success: function(data) {
                if (company_id === "") {
                    $("#BuyerInvoice_buyer_id").val('').trigger('change.select2');
                } else {
                    $('#BuyerInvoice_buyer_id').empty();
                    $("#BuyerInvoice_buyer_id").prepend("<option value='' selected='selected'>-Select Buyer-</option>");
                    $.each(data.buyer_list, function(val, text) {
                        $('#BuyerInvoice_buyer_id').append($('<option></option>').val(val).html(text))
                    });

                }
            }
        });
    }

    function getProjectData() {
        var company_id = $("#BuyerInvoice_company_id").val();

        $.ajax({
            url: "<?php echo $getprojecstUrl; ?>",
            data: {
                "company_id": company_id
            },
            type: "POST",
            dataType: 'json',
            success: function(data) {

                if (company_id === "") {
                    $("#BuyerInvoice_project_id").val('').trigger('change.select2');
                } else {
                    $.each(data.project_list, function(val, text) {
                        $('#project_id').append($('<option></option>').val(val).html(text))
                    });
                }
            }
        });

    }

    function setProjectFlatData(buyer_id) {
        $.ajax({
            url: "<?php echo $getProjectAndFlats; ?>",
            data: {
                "buyer_id": buyer_id
            },
            type: "POST",
            dataType: 'json',
            success: function(data) {
                if (buyer_id === "") {
                    $("#project_id").val('').trigger('change.select2');
                    $('#BuyerInvoice_project_id').val('');
                    $("#flat_id").val('').trigger('change.select2');
                    $("#BuyerTransactions_flat_id").val('');
                } else {
                    $("#project_id").val(data.project_id).trigger('change.select2');
                    $('#BuyerInvoice_project_id').val(data.project_id);
                    $("#project_id").attr("disabled", true);
                    if (data.flat_count === 1) {
                        $.each(data.flat_list, function(val, text) {
                            $('#flat_id').append($('<option selected="selected"></option>').val(val).html(text))
                            $("#BuyerInvoice_flat_id").val(val);
                        });
                        $("#flat_id").attr("disabled", true);
                    } else {
                        $('#flat_id').empty();
                        $("#flat_id").prepend("<option value='' selected='selected'>-Select Apartment No-</option>");
                        $("#flat_id").attr("disabled", false);
                        $.each(data.flat_list, function(val, text) {
                            $('#flat_id').append($('<option></option>').val(val).html(text))
                        });
                    }
                }
            }
        });
    }
</script>

<script>
    var invoice_id = <?php echo $model->buyer_invoice_id; ?>;
    jQuery.extend(jQuery.expr[':'], {
        focusable: function(el, index, selector) {
            return $(el).is('button, :input, [tabindex]');
        }
    });

    $(document).on('keypress', 'input,select', function(e) {
        if (e.which == 13) {
            e.preventDefault();
            // Get all focusable elements on the page
            var $canfocus = $(':focusable');
            var index = $canfocus.index(document.activeElement) + 1;
            if (index >= $canfocus.length) index = 0;
            $canfocus.eq(index).focus();
        }
    });

    $(document).ready(function() {
        $(".popover-test").popover({
            html: true,
            content: function() {
                return $(this).next('.popover-content').html();
            }
        });
        $('[data-toggle=popover]').on('click', function(e) {
            $('[data-toggle=popover]').not(this).popover('hide');
        });
        $('body').on('hidden.bs.popover', function(e) {
            $(e.target).data("bs.popover").inState.click = false;
        });
        $('body').on('click', function(e) {
            $('[data-toggle=popover]').each(function() {
                // hide any open popovers when the anywhere else in the body is clicked
                if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                    $(this).popover('hide');
                }
            });
        });
        $(document).ajaxComplete(function() {
            $(".popover-test").popover({
                html: true,
                content: function() {
                    return $(this).next('.popover-content').html();
                }
            });

        });

        $(".js-example-basic-single").select2();
        $(".project").select2();
        $(".client").select2();

    });


    $('.project').change(function() {

        $("#client").select2("open");
    })





    $(document).ready(function() {
        $('#description').first().focus();
        //$('.js-example-basic-single').select2('focus');
    });
</script>



<script>
    $(document).ready(function() {
        $().toastmessage({
            sticky: false,
            position: 'top-right',
            inEffectDuration: 1000,
            stayTime: 3000,
            closeText: '<i class="icon ion-close-round"></i>',
        });




    });


    $(document).on('click', '.removebtn', function(e) {
        e.preventDefault();
        element = $(this);
        var item_id = $(this).attr('id');
        var answer = confirm("Are you sure you want to delete?");
        if (answer) {
            var item_id = $(this).attr('id');
            var invoice_id = $("#invoice_id").val();
            var subtot = $('input[name="subtot"]').val();
            var grand = $('input[name="grand"]').val();
            var tax_grand = $('#grand_taxamount').text();
            var data = {
                'invoice_id': invoice_id,
                'item_id': item_id,
                'grand': grand,
                'subtot': subtot,
                'tax_grand': tax_grand
            };
            $.ajax({
                method: "GET",
                async: false,
                data: {
                    data: data
                },
                dataType: "json",
                url: '<?php echo $removeItemUrl; ?>',
                success: function(response) {
                    if (response.response == 'success') {
                        $('#grand_total').html(response.final_amount);
                        $('#grand_taxamount').html(response.final_tax);
                        $('#net_amount').html(response.grand_total);
                        $().toastmessage('showSuccessToast', "" + response.msg + "");
                        $('.addrow').html(response.html);
                        $('#invoice_div').text(response.invoice_no);
                        $("#invoice_div").load(location.href + " #invoice_div");
                    } else {
                        $().toastmessage('showErrorToast', "" + response.msg + "");
                    }
                }
            });

        } else {

            return false;
        }

    });



    $(document).on("click", ".addcolumn, .removebtn", function() {
        //alert('hi');

        $("table.table  input[name='sl_No[]']").each(function(index, element) {
            $(element).val(index + 1);
            $('.sl_No').html(index + 1);
        });
    });






    $(".inputSwitch span").on("click", function() {

        var $this = $(this);

        $this.hide().siblings("input").val($this.text()).show();

    });

    $(".inputSwitch input").bind('blur', function() {

        var $this = $(this);

        $(this).attr('value', $(this).val());

        $this.hide().siblings("span").text($this.val()).show();

    }).hide();


    $(".allownumericdecimal").keydown(function(event) {



        if (event.shiftKey == true) {
            event.preventDefault();
        }

        if ((event.keyCode >= 48 && event.keyCode <= 57) ||
            (event.keyCode >= 96 && event.keyCode <= 105) ||
            event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 ||
            event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190 || event.keyCode == 110 || event.keyCode == 13) {
            var splitfield = $(this).val().split(".");
            if ((splitfield[1] !== undefined && splitfield[1].length >= 2) && event.keyCode != 8 && event.keyCode != 0 && event.keyCode != 13) {
                event.preventDefault();
            }
        } else {
            event.preventDefault();
        }

        if ($(this).val().indexOf('.') !== -1 && (event.keyCode == 190 || event.keyCode == 110))
            event.preventDefault();
        //if a decimal has been added, disable the "."-button





    });







    $('.other').click(function() {
        if (this.checked) {


            $('#autoUpdate').fadeIn('slow');
            $('#select_div').hide();
        } else {

            $('#autoUpdate').fadeOut('slow');
            $('#select_div').show();
        }

    })


    $(document).on("change", ".other", function() {
        if (this.checked) {
            $(this).closest('tr').find('#autoUpdate').fadeIn('slow');
            $(this).closest('tr').find('#select_div').hide();
        } else {
            $(this).closest('tr').find('#autoUpdate').fadeOut('slow');
            $(this).closest('tr').find('#select_div').show();
        }
    });
</script>


<script>
    var sl_no = 1;
    var howMany = 0;
    $('.item_save').click(function() {
        $("#previous_details").hide();
        var element = $(this);
        var item_id = $(this).attr('id');

        if (item_id == 0) {


            var description = $('#description').val();
            var cgst = $('#cgst').val();
            var cgst_amount = $('#cgst_amount').html();
            var sgst = $('#sgst').val();
            var sgst_amount = $('#sgst_amount').html();
            var igst = $('#igst').val();
            var igst_amount = $('#igst_amount').html();
            var tax_amount = $('#tax_amount').html();
            var amount = $('#amount').val();
            var subtotal_amount = $('#subtotal_amt').text();
            var disc_amt = $('#discount').val();
            var transaction_type = $('#BuyerInvoiceList_transaction_type').val();

            var company = $('#BuyerInvoice_company_id').val();
            var project = $('input[name="BuyerInvoice[project_id]"]').val();
            var date = $('input[name="BuyerInvoice[date]"]').val();
            var inv_no = $('input[name="BuyerInvoice[invoice_no]"]').val();
            var buyer_id = $('input[name="BuyerInvoice[buyer_id]"]').val();
            var flat_id = $('input[name="BuyerInvoice[flat_id]"]').val();
            var rowCount = $('.table .addrow tr').length;

            if (company == '' || buyer_id == '' || project == '' || flat_id == '' || date == '' || inv_no == '') {
                $().toastmessage('showErrorToast', "Please enter purchase details");

            } else {

                if (description != '' && amount != '' && amount > 0) {


                    howMany += 1;
                    if (howMany == 1) {
                        var invoice_id = $('input[name="invoice_id"]').val();
                        // var subtot = $('#grand_total').text();
                        var grand = $('#grand_total').text();
                        var grand_tax = $('#grand_taxamount').text();
                        var data = {
                            'sl_no': rowCount,
                            'description': description,
                            'amount': amount,
                            'sgst': sgst,
                            'sgst_amount': sgst_amount,
                            'cgst': cgst,
                            'cgst_amount': cgst_amount,
                            'igst': igst,
                            'igst_amount': igst_amount,
                            'tax_amount': tax_amount,
                            'invoice_id': invoice_id,
                            'grand': grand,
                            'subtotal_amount': subtotal_amount,
                            'disc_amt': disc_amt,
                            'grand_tax': grand_tax,
                            'transaction_type': transaction_type
                        };
                        $.ajax({
                            url: '<?php echo $addInvoiceItemUrl; ?>',
                            type: 'GET',
                            dataType: 'json',
                            data: {
                                data: data
                            },
                            success: function(response) {
                                if (response.response == 'success') {
                                    $('#grand_total').html(response.final_amount);
                                    $('#grand_taxamount').html(response.final_tax);
                                    $('#net_amount').html(response.grand_total);
                                    $().toastmessage('showSuccessToast', "" + response.msg + "");
                                    $('.addrow').html(response.html);
                                    $('#invoice_div').text(response.invoice_no);
                                    $("#invoice_div").load(location.href + " #invoice_div");
                                } else {
                                    $().toastmessage('showErrorToast', "" + response.msg + "");
                                }
                                howMany = 0;
                                //$('#description').val('');
                                $('#description').val('').trigger('change');
                                $('#remarks').val('');
                                $('#amount').val('');
                                $('#item_amount').html('');
                                $('#tax_amount').html('0.00');
                                $('#sgst').val('');
                                $('#sgst_amount').html('0.00');
                                $('#cgst').val('');
                                $('#cgst_amount').html('0.00');
                                $('#igst').val('');
                                $('#igst_amount').html('0.00');
                                $('#description').focus();
                                $('#subtotal_amt').html('0.00');
                                $('#discount').val('');
                            }
                        });
                    }

                } else {
                    $().toastmessage('showErrorToast', "Please fill the description and amount");
                }



            }
        } else {
            // update 


            var description = $('#description').val();
            var amount = $('#amount').val();


            var cgst = $('#cgst').val();
            var cgst_amount = $('#cgst_amount').text();
            var sgst = $('#sgst').val();
            var sgst_amount = $('#sgst_amount').text();
            var igst = $('#igst').val();
            var igst_amount = $('#igst_amount').text();
            var tax_amount = $('#tax_amount').text();
            var subtotal_amount = $('#subtotal_amt').text();
            var disc_amt = $('#discount').val();
            var transaction_type = $('#BuyerInvoiceList_transaction_type').val();

            var company = $('#BuyerInvoice_company_id').val();
            var project = $('input[name="BuyerInvoice[project_id]"]').val();
            var date = $('input[name="BuyerInvoice[date]"]').val();
            var inv_no = $('input[name="BuyerInvoice[invoice_no]"]').val();
            var buyer_id = $('input[name="BuyerInvoice[buyer_id]"]').val();
            var flat_id = $('input[name="BuyerInvoice[flat_id]"]').val();
            var company = $('#company').val();

            if (company == '' || buyer_id == '' || project == '' || flat_id == '' || date == '' || inv_no == '') {
                $().toastmessage('showErrorToast', "Please enter invoice details");
            } else {
                if (description != '' && amount != '' && amount > 0) {
                    howMany += 1;
                    if (howMany == 1) {
                        var invoice_id = $('input[name="invoice_id"]').val();
                        // var subtot = $('#grand_total').text();
                        var grand = $('#grand_total').text();
                        var grand_tax = $('#grand_taxamount').text();
                        var data = {
                            'item_id': item_id,
                            'sl_no': sl_no,
                            'description': description,
                            'amount': amount,
                            'sgst': sgst,
                            'sgst_amount': sgst_amount,
                            'cgst': cgst,
                            'cgst_amount': cgst_amount,
                            'igst': igst,
                            'igst_amount': igst_amount,
                            'tax_amount': tax_amount,
                            'invoice_id': invoice_id,
                            'grand': grand,
                            'disc_amt': disc_amt,
                            'subtotal_amount': subtotal_amount,
                            'grand_tax': grand_tax,
                            'transaction_type': transaction_type
                        };
                        $.ajax({
                            url: '<?php echo $updateInvoiceItemUrl; ?>',
                            type: 'GET',
                            dataType: 'json',
                            data: {
                                data: data
                            },
                            success: function(response) {
                                //element.closest('tr').hide();
                                if (response.response == 'success') {
                                    $('#grand_total').html(response.final_amount);
                                    $('#grand_taxamount').html(response.final_tax);
                                    $('#net_amount').html(response.grand_total);
                                    $().toastmessage('showSuccessToast', "" + response.msg + "");
                                    $('.addrow').html(response.html);
                                    $('#invoice_div').text(response.invoice_no);
                                    $("#invoice_div").load(location.href + " #invoice_div");
                                } else {
                                    $().toastmessage('showErrorToast', "" + response.msg + "");
                                }

                                howMany = 0;
                                $('#description').val('').trigger('change');
                                $('#remarks').val('');
                                $('#amount').val('');
                                $('#item_amount').html('');
                                $('#tax_amount').html('0.00');
                                $('#sgst').val('');
                                $('#sgst_amount').html('0.00');
                                $('#cgst').val('');
                                $('#cgst_amount').html('0.00');
                                $('#igst').val('');
                                $('#igst_amount').html('0.00');
                                $('#description').focus();
                                $('#item_amount_temp').val(0);
                                $('#item_tax_temp').val(0);
                                $('#subtotal_amt').html('0.00');

                                $(".item_save").attr('value', 'Save');
                                $(".item_save").attr('id', 0);
                                //$(".js-example-basic-single").select2("val", "");
                                $('#description').focus();
                                $('#discount').val('');
                            }
                        });

                    }
                } else {
                    $().toastmessage('showErrorToast', "Please fill the description and amount");

                }



            }


        }




    });





    $(document).on('click', '.edit_item', function(e) {
        e.preventDefault();


        $('.remark').css("display", "inline-block");
        $('#remark').focus();

        var item_id = $(this).attr('id');
        var $tds = $(this).closest('tr').find('td');
        var description = $tds.eq(1).text();
        var type = $tds.eq(2).text();
        type = type.replace(/\s/g, '');
        var sgst = $tds.eq(3).text();
        var sgst_amount = $tds.eq(4).text();
        var cgst = $tds.eq(5).text();
        var cgst_amount = $tds.eq(6).text();

        var igst = $tds.eq(7).text();
        var igst_amount = $tds.eq(8).text();
        var amount = $tds.eq(9).text();
        var tax_amount = $tds.eq(10).text();
        var discount = $tds.eq(11).text();
        var subtotal_amt = $tds.eq(12).text();
        $('#description').val(description.trim());
        $("#BuyerInvoiceList_transaction_type").val(type).trigger('change.select2');;
        $('#item_amount').html(parseFloat(amount))
        $('#item_amount_temp').val(parseFloat(amount))
        $('#amount').val(parseFloat(amount))
        $('#discount').val(parseFloat(discount))
        $('#tax_amount').html(parseFloat(tax_amount))
        $('#subtotal_amt').html(parseFloat(subtotal_amt))
        $('#item_tax_temp').val(parseFloat(tax_amount))
        $('#cgst').val(cgst.trim());
        $('#cgst_amount').text(parseFloat(cgst_amount))
        $('#sgst').val(sgst.trim())
        $('#sgst_amount').text(parseFloat(sgst_amount))
        $('#igst').val(igst.trim())
        $('#igst_amount').text(parseFloat(igst_amount))

        $(".item_save").attr('value', 'Update');
        $(".item_save").attr('id', item_id);
        $(".item_save").attr('id', item_id);
        $('#description').focus();


    })










    $('.item_save').keypress(function(e) {
        if (e.keyCode == 13) {
            $('.item_save').click();
        }
    });


    function filterDigits(eventInstance) {
        eventInstance = eventInstance || window.event;
        key = eventInstance.keyCode || eventInstance.which;
        if ((47 < key) && (key < 58) || key == 8) {
            return true;
        } else {
            if (eventInstance.preventDefault)
                eventInstance.preventDefault();
            eventInstance.returnValue = false;
            return false;
        } //if
    }


    $(document).ready(function() {
        $(".popover-test").popover({
            html: true,
            content: function() {
                //return $('#popover-content').html();
                return $(this).next('.popover-content').html();
            }
        });
        $('[data-toggle=popover]').on('click', function(e) {
            $('[data-toggle=popover]').not(this).popover('hide');
        });
        $('body').on('hidden.bs.popover', function(e) {
            $(e.target).data("bs.popover").inState.click = false;
        });
        $('body').on('click', function(e) {
            $('[data-toggle=popover]').each(function() {
                // hide any open popovers when the anywhere else in the body is clicked
                if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                    $(this).popover('hide');
                }
            });
        });

    });

    // calculation

    $("#amount, #sgst, #cgst, #igst,#discount").blur(function() {
        var amount = parseFloat($("#amount").val());
        var sgst = parseFloat($("#sgst").val());
        var cgst = parseFloat($("#cgst").val());
        var igst = parseFloat($("#igst").val());
        var discount_amt = parseFloat($("#discount").val());
        var new_amount = 0;

        var sgst_amount = (sgst / 100) * amount;
        var cgst_amount = (cgst / 100) * amount;
        var igst_amount = (igst / 100) * amount;

        if (isNaN(sgst_amount)) sgst_amount = 0;
        if (isNaN(cgst_amount)) cgst_amount = 0;
        if (isNaN(igst_amount)) igst_amount = 0;
        if (isNaN(discount_amt)) discount_amt = 0;
        if (isNaN(amount)) amount = 0;

        var tax_amount = sgst_amount + cgst_amount + igst_amount;

        var total_amount = (amount + tax_amount) - (discount_amt);

        $("#sgst_amount").html(sgst_amount.toFixed(2));
        $("#cgst_amount").html(cgst_amount.toFixed(2));
        $("#igst_amount").html(igst_amount.toFixed(2));
        $("#item_amount").html(amount.toFixed(2));
        $("#tax_amount").html(tax_amount.toFixed(2));
        $("#total_amount").html(total_amount.toFixed(2));
        $("#subtotal_amt").html(total_amount.toFixed(2));

    });

    $('.calculation').keyup(function(event) {
        var val = $(this).val();
        if (val > 100) {
            $().toastmessage('showErrorToast', "This percentage must be less than 100%");
            $(this).val('');
        }
    })
</script>

<style>
    .table-scroll {
        position: relative;
        max-width: 1280px;
        width: 100%;
        margin: auto;
        display: table;
    }

    .table-wrap {
        width: 100%;
        display: block;
        overflow: auto;
        position: relative;
        z-index: 1;
        border: 1px solid #ddd;
    }

    .table-wrap.fixedON,
    .table-wrap.fixedON table,
    .faux-table table {
        height: 380px;
        /* match heights*/
    }

    .table-scroll table {
        width: 100%;
        margin: auto;
        border-collapse: separate;
        border-spacing: 0;
        border: 1px solid #ddd;
    }

    .table-scroll th,
    .table-scroll td {
        padding: 5px 10px;
        border: 1px solid #ddd;
        background: #fff;
        vertical-align: top;
    }

    .faux-table table {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        pointer-events: none;
    }

    .faux-table table tbody {
        visibility: hidden;
    }

    /* shrink cells in cloned table so that the table height is exactly 300px so that the header and footer appear fixed */
    .faux-table table tbody th,
    .faux-table table tbody td {
        padding-top: 0;
        padding-bottom: 0;
        border-top: none;
        border-bottom: none;
        line-height: 0.1;
    }

    .faux-table table tbody tr+tr th,
    .faux-table tbody tr+tr td {
        line-height: 0;
    }

    .faux-table thead th,
    .faux-table tfoot th,
    .faux-table tfoot td,
    .table-wrap thead th,
    .table-wrap tfoot th,
    .table-wrap tfoot td {
        background: #eee;
    }

    .faux-table {
        position: absolute;
        top: 0;
        right: 0;
        left: 0;
        bottom: 0;
        overflow-y: scroll;
    }

    .faux-table thead,
    .faux-table tfoot,
    .faux-table thead th,
    .faux-table tfoot th,
    .faux-table tfoot td {
        position: relative;
        z-index: 2;
    }

    /* ie bug */
    .table-scroll table thead tr,
    .table-scroll table thead tr th,
    .table-scroll table tfoot tr,
    .table-scroll table tfoot tr th,
    .table-scroll table tfoot tr td {
        height: 1px;
    }
</style>
<script>
    (function() {
        var mainTable = document.getElementById("main-table");
        if (mainTable !== null) {
            var tableHeight = mainTable.offsetHeight;
            if (tableHeight > 380) {
                var fauxTable = document.getElementById("faux-table");
                document.getElementById("table-wrap").className += ' ' + 'fixedON';
                var clonedElement = mainTable.cloneNode(true);
                clonedElement.id = "";
                fauxTable.appendChild(clonedElement);
            }
        }
    })();
</script>