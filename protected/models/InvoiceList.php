<?php

/**
 * This is the model class for table "{{invoice_list}}".
 *
 * The followings are the available columns in table '{{invoice_list}}':
 * @property integer $id
 * @property integer $perf_id
 * @property integer $quantity
 * @property string $unit
 * @property integer $rate
 * @property integer $amount
 * @property string $description
 *
 * The followings are the available model relations:
 * @property PerformaInvoice $perf
 */
class InvoiceList extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{invoice_list}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('perf_id, quantity, unit, rate, amount, description', 'required'),
			array('perf_id, quantity, rate, amount', 'numerical', 'integerOnly'=>true),
			array('quantity, rate, amount', 'numerical'),
			array('unit', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, perf_id, quantity, unit, rate, amount, description', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'perf' => array(self::BELONGS_TO, 'PerformaInvoice', 'perf_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'perf_id' => 'Perf',
			'quantity' => 'Quantity',
			'unit' => 'Unit',
			'rate' => 'Rate',
			'amount' => 'Amount',
			'description' => 'Description',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('perf_id',$this->perf_id);
		$criteria->compare('quantity',$this->quantity);
		$criteria->compare('unit',$this->unit,true);
		$criteria->compare('rate',$this->rate);
		$criteria->compare('amount',$this->amount);
		$criteria->compare('description',$this->description,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return InvoiceList the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}