<?php
/* @var $this ExpensesController */
/* @var $data Expenses */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('expense_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->expense_id), array('view', 'id'=>$data->expense_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('expense_date')); ?>:</b>
	<?php echo CHtml::encode($data->expense_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('project_id')); ?>:</b>
	<?php echo CHtml::encode($data->project_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bill_id')); ?>:</b>
	<?php echo CHtml::encode($data->bill_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('expensetype_id')); ?>:</b>
	<?php echo CHtml::encode($data->expensetype_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('vendor_id')); ?>:</b>
	<?php echo CHtml::encode($data->vendor_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('expense_amount')); ?>:</b>
	<?php echo CHtml::encode($data->expense_amount); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('expense_sgstp')); ?>:</b>
	<?php echo CHtml::encode($data->expense_sgstp); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('expense_sgst')); ?>:</b>
	<?php echo CHtml::encode($data->expense_sgst); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('expense_cgstp')); ?>:</b>
	<?php echo CHtml::encode($data->expense_cgstp); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('expense_cgst')); ?>:</b>
	<?php echo CHtml::encode($data->expense_cgst); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('expense_totalamount')); ?>:</b>
	<?php echo CHtml::encode($data->expense_totalamount); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('expense_description')); ?>:</b>
	<?php echo CHtml::encode($data->expense_description); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('receipt_type')); ?>:</b>
	<?php echo CHtml::encode($data->receipt_type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('receipt')); ?>:</b>
	<?php echo CHtml::encode($data->receipt); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('purchase_type')); ?>:</b>
	<?php echo CHtml::encode($data->purchase_type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('paidamount')); ?>:</b>
	<?php echo CHtml::encode($data->paidamount); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_id')); ?>:</b>
	<?php echo CHtml::encode($data->user_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_by')); ?>:</b>
	<?php echo CHtml::encode($data->created_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_date')); ?>:</b>
	<?php echo CHtml::encode($data->created_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_by')); ?>:</b>
	<?php echo CHtml::encode($data->updated_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_date')); ?>:</b>
	<?php echo CHtml::encode($data->updated_date); ?>
	<br />

	*/ ?>

</div>