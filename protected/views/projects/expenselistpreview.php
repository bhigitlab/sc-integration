<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/tablestyles.css" />
<link rel="stylesheet" type="text/css" href="uploads/new_template/css/newstyle.css" />
<style>
    table {
        border-collapse: collapse;
        width: 100%;
    }
    /*.container{max-width: 1050px;margin:0 auto;padding: 15px;border: 1px solid #ddd;}*/
    .text-right{text-align:right}
    .details,.info-table{border: 1px solid #212121;margin-top: 20px;}
    .details td, .info-table td,.info-table th{padding: 6px 8px; }
    .info-table{margin-bottom: 10px;}
    .text-center{text-align:center;}
    .img-hold{width: 10%;}
    .companyhead{font-size: 18px;font-weight:bold;margin-top:0px;color: #789AD1;}
</style>
<?php
    $company_address = Company::model()->findBypk(($company_id)?$company_id:Yii::app()->user->company_id);	
?>
    <table border=0>
        <tbody>
                <tr>
                <td class="img-hold"><img  src="<?php echo Yii::app()->theme->baseUrl; ?>/images/logo.png" alt="img" style=''/></td>
                    <td class=text-right>GST NO: <?php echo isset($company_address["company_gstnum"])?$company_address["company_gstnum"]:''; ?></td>
                    
                </tr>
        </tbody>
    </table>
    <table border=0>
        <tbody>
            <tr>
               
                <td class="text-center"><p class='companyhead'><?php echo isset($company_address['name'])?$company_address['name']:''; ?></p><br>
                    <p><?php echo ( (isset($company_address['address']) && !empty($company_address['address'])) ? ($company_address['address']) : ""); ?><br>
                    PIN : <?php echo ( (isset($company_address['pincode']) && !empty($company_address['pincode'])) ?($company_address['pincode']) : ""); ?>,&nbsp;
                    PHONE : <?php echo ( (isset($company_address['phone']) && !empty($company_address['phone'])) ?( $company_address['phone']) : ""); ?><br>
                    EMAIL : <?php echo ( (isset($company_address['email_id']) && !empty($company_address['email_id'])) ?( $company_address['email_id']) : ""); ?></p><br>                        
                    <h4>Project Summary of <?php echo ($name !='')?$name:''; ?></h4>   
                </td>
            </tr>
        </tbody>
    </table>
<div id="contenthtml">
    <div style="margin-left:30px;">
        <table border="1" class="table table-bordered table-striped">
        
            <thead>
<!--               <tr>
                    <th></th>
                    <th align='right'><b>Receipt</b></th>
                    <th align='right'><b>Payment</b></th>
                </tr> -->
            </thead>
            <tbody>
                
                <?= $expense_entries ?>
            </tbody>
        </table>
        
    </div>
</div>
