<?php
/* @var $this PoCompanyController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Po Companies',
);

$this->menu=array(
	array('label'=>'Create PoCompany', 'url'=>array('create')),
	array('label'=>'Manage PoCompany', 'url'=>array('admin')),
);
?>

<h1>Po Companies</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
