<?php
/*
//Code snippets
//Controller file

$is_export_action = boolval(filter_input(INPUT_GET, 'export'));

        $exported_data = $this->render('index', array(
            'model' => $model,
                ), $is_export_action
        );

        if ($is_export_action) {
            $export_filename = 'Clients_list_' . date('Ymd_His');
            Controller::exportGridAsCSV($exported_data, $export_filename);
        }
*/

/*
//Cgridview File - remove filter

$this->widget('zii.widgets.grid.CGridView', array(
......
......
    'filter' => filter_input(INPUT_GET, 'export') ? null : $model,

*/

/*
   //Model search with pagesize false to get all rows from dba_close
   
   $export = filter_input(INPUT_GET, 'export');
        $pagesize = 25;
        if ($export) {
            $pagesize = YourModelName::model()->count();
        }

        return new CActiveDataProvider($this, array('pagination' => array('pageSize' => $pagesize,),
            'criteria' => $criteria, 'sort' => array(
                'defaultOrder' => 'cid desc',),
        ));
*/




/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class ExportHelper extends CController {

    public static function exportGridAsCSVByTr(&$grid_data, $filename) {

        require_once (Yii::app()->basePath . '/components/simple_html_dom.php');
//        $html = str_get_html($grid_data); // give this your HTML string
        header('Content-type: application/ms-excel');
        header('Content-Disposition: attachment; filename=' . $filename . '.csv');

        $fp = fopen("php://output", "w");

        foreach ($grid_data as $rowdata) {
            preg_match_all('/<(th|td).*?>(.*?)<\/(th|td)>/si', $rowdata, $matches);
//            
//            print_r($rowdata);
//            die;
            if (isset($matches[0]) and is_array($matches[0])) {
                foreach ($matches[0] as $k => $row) {
                    if ($k > 0) {
                        echo ',';
                    }
                    echo '"'.strip_tags(str_replace('&nbsp;', '', $row)).'"';
                }
                echo "\n";
            }
//            foreach(){
//                
//            }
        }

//        echo $td;
        exit;
    }

    public static function exportGridAsCSV(&$grid_data, $filename) {

        require_once (Yii::app()->basePath . '/components/simple_html_dom.php');
        $html = str_get_html($grid_data); // give this your HTML string
        header('Content-type: application/ms-excel');
        header('Content-Disposition: attachment; filename=' . $filename . '.csv');

        $fp = fopen("php://output", "w");

        $contents = $html->find('tr');

        foreach ($contents as $element) {
            $td = array();
            foreach ($element->find('th') as $row) {
                if (strpos(trim($row->class), 'actions') === false && strpos(trim($row->class), 'checker') === false) {
                    $td [] = str_replace('&nbsp;', '',$row->plaintext);
                }
            }
            if (!empty($td)) {
                fputcsv($fp, $td);
            }

            $td = array();
            foreach ($element->find('td') as $row) {
                if (strpos(trim($row->class), 'actions') === false && strpos(trim($row->class), 'checker') === false) {
                    $td [] = str_replace('&nbsp;', '', $row->plaintext);
                }
            }
            if (!empty($td)) {
                fputcsv($fp, $td);
            }
        }

        fclose($fp);

//        echo $td;
        exit;
    }

}
