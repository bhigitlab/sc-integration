<?php
/* @var $this VendorsController */
/* @var $model Vendors */

$this->breadcrumbs = array(
    'Vendors' => array('index'),
    'Create',
); ?>

<div class="entries-wrapper">
    <div class="row">
        <div class="col-xs-12">
            <div class="heading-title">Add Vendors</div>
            <div class="dotted-line"></div>
        </div>
    </div>
    <?php echo $this->renderPartial('_form', array('model' => $model)); ?>
</div>