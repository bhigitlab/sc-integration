<?php

class EquipmentsEntriesController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout = '//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		$accessArr = array();
		$accessauthArr = array();
		$controller = Yii::app()->controller->id;
		if (isset(Yii::app()->user->menuall)) {
			if (array_key_exists($controller, Yii::app()->user->menuall)) {
				$accessArr = Yii::app()->user->menuall[$controller];
			}
		}

		if (isset(Yii::app()->user->menuauth)) {
			if (array_key_exists($controller, Yii::app()->user->menuauth)) {
				$accessauthArr = Yii::app()->user->menuauth[$controller];
			}
		}
		$access_privlg = count($accessauthArr);
		return array(
			array(
				'allow', // allow all users to perform 'index' and 'view' actions
				'actions' => $accessArr,
				'users' => array('@'),
				'expression' => "$access_privlg > 0",
			),

			array(
				'allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions' => $accessauthArr,
				'users' => array('@'),
				'expression' => "$access_privlg > 0",
			),

			array(
				'allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions' => array('ajaxcall', 'dynamicVendor', 'GetCompany', 'RefreshButton', 'getEquipmentsByProject','qtybalance','getNewInputRow'),
				'users' => array('*'),
			),
			array(
				'deny',  // deny all users
				'users' => array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render(
			'view',
			array(
				'model' => $this->loadModel($id),
			)
		);
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$id = isset($_GET['id']) ? $_GET['id'] : '';
		if ($id) {
			$model = EquipmentsEntries::model()->findByPk($id);
		} else {
			$model = new EquipmentsEntries;
			$EquipmentsItems = new EquipmentsItems;
		}
		$EquipmentsItems = new EquipmentsItems;
		$id = Yii::app()->user->id;
		$tblpx = Yii::app()->db->tablePrefix;
		$user = Users::model()->findByPk(Yii::app()->user->id);
		$arrVal = explode(',', $user->company_id);

		$newQuery = "";
		foreach ($arrVal as $arr) {
			if ($newQuery)
				$newQuery .= ' OR';
			$newQuery .= " FIND_IN_SET('" . $arr . "', e.company_id)";
		}
		$currDate = date('Y-m-d', strtotime(date("Y-m-d")));
		$expense_data_sql = "SELECT e.*, p.name as pname,s.subcontractor_name as subname,exp.type_name FROM " . $this->tableName('EquipmentsEntries') . " e";
		$expense_data_sql .= " LEFT JOIN " . $this->tableName('Projects') . " p ON e.project_id = p.pid ";
		$expense_data_sql .= "LEFT JOIN " . $this->tableName('Subcontractor') . " s ON e.subcontractor_id = s.subcontractor_id ";
		$expense_data_sql .= "LEFT JOIN " . $this->tableName('ExpenseType') . " exp on exp.type_id= e.expensehead_id ";
		$expense_data_sql .= "WHERE e.date = '" . $currDate . "' AND (" . $newQuery . ")";
		$expenseData = Yii::app()->db->createCommand($expense_data_sql)->queryAll();
		$last_date_sql = "SELECT date FROM " . $this->tableName('EquipmentsEntries') . " ORDER BY date DESC LIMIT 1";
		$lastdate = Yii::app()->db->createCommand($last_date_sql)->queryRow();
		$equipments='';
		$project=$model->project_id;
		$equipment_arr=array();
        $wprData = EquipmentsItems::model()->findAll();
		if(!empty($project)){
            $model_exist= Itemestimation::model()->find(array("condition" => "project_id = '$project'"));
			// echo "<pre>";print_r($model_exist);exit;
            if(!empty($model_exist)){
                 $estimation_id =$model_exist->itemestimation_id;
            
                 $sql="SELECT m.equipment FROM `jp_equipments_estimation` m LEFT JOIN `jp_itemestimation` e ON e.itemestimation_id=m.estimation_id WHERE e.itemestimation_id=".$estimation_id ." AND e.itemestimation_status=2 AND  m.estimation_id=".$estimation_id ."  AND m.project_id=".$project;
                $equipments = Yii::app()->db->createCommand($sql)->queryAll();
                 $equipment_arr=array();
                 if(!empty($equipments)){
                    foreach($equipments as $equipment_item){
                        $items_det= Equipments::Model()->findByPk($equipment_item['equipment']);
                        // echo "<pre>";print_r($items_det);exit;
                        if(!empty($items_det)){
                            $equipment_arr[$items_det->id]=$items_det["equipment_name"];
                        }

                    }
                 }
                // echo "<pre>";print_r($equipment_arr);exit;
            }
           
        }

		// Fetch unit data
		$unitData = CHtml::listData(Unit::model()->findAll(), 'id', 'unit_name');

		$this->render(
			'equipments_form',
			array(
				'model' => $model,
				'equipment_arr'=>$equipment_arr,
				'EquipmentsItems' => $EquipmentsItems,
				'newmodel' => $expenseData,
				'id' => $id,
				'lastdate' => $lastdate,
				'unitData' => $unitData,
			)
		);
	}

	public function actionGetNewInputRow(){
        $index = $_POST['index'];
        if($index==''){
            $index==0;
        }
        $equipments='';
        $project = $_POST['project'];
        $equipment_arr=array();
        $wprData = EquipmentsItems::model()->findAll();
        if(!empty($project)){
            $model_exist= Itemestimation::model()->find(array("condition" => "project_id = '$project'"));
            if(!empty($model_exist)){
                 $estimation_id =$model_exist->itemestimation_id;
            
                 $sql="SELECT m.equipment FROM `jp_equipments_estimation` m LEFT JOIN `jp_itemestimation` e ON e.itemestimation_id=m.estimation_id WHERE e.itemestimation_id=".$estimation_id ." AND e.itemestimation_status=2 AND  m.estimation_id=".$estimation_id ."  AND m.project_id=".$project;
                $equipments = Yii::app()->db->createCommand($sql)->queryAll();
              
                 $equipment_arr=array();
                 if(!empty($equipments)){
                    foreach($equipments as $equipment_item){
                        $items_det= Equipments::Model()->findByPk($equipment_item['equipment']);
                        
                        if(!empty($items_det)){
                            $equipment_arr[$items_det->id]=$items_det["equipment_name"];
                        }

                    }
                 }
            }
           
        }

        $this->renderPartial('_equipmentform_add', array('modelitem' => $wprData, 'index' => $index,'equipments'=>$equipment_arr));

    }

	public function actionGetfieldsForEdit(){
        $equipmentId=$_GET['id'];
        $index = $_GET['index'];
        if($index==''){
            $index==0;
        }
        $equipmentEntry=EquipmentsEntries::model()->findByPk($equipmentId);
        $equipments='';
        $project = $equipmentEntry['project_id'];
        $equipments_arr=array();
        if(!empty($project)){
            $model_exist= Itemestimation::model()->find(array("condition" => "project_id = '$project'"));
            if(!empty($model_exist)){
                 $estimation_id =$model_exist->itemestimation_id;
            
                 $sql="SELECT m.equipment FROM `jp_equipments_estimation` m LEFT JOIN `jp_itemestimation` e ON e.itemestimation_id=m.estimation_id WHERE e.itemestimation_id=".$estimation_id ." AND e.itemestimation_status=2 AND  m.estimation_id=".$estimation_id ."  AND m.project_id=".$project;
                $equipments = Yii::app()->db->createCommand($sql)->queryAll();
              
                 $equipment_arr=array();
                 if(!empty($equipments)){
                    foreach($equipments as $equipment_item){
                        $items_det= Equipments::Model()->findByPk($equipment_item['equipment']);
                        //echo "<pre>";print_r($items_det);exit;
                        if(!empty($items_det)){
                            $equipments_arr[$items_det->id]=$items_det["equipment_name"];
                        }

                    }
                 }
                // echo "<pre>";print_r($equipment_arr);exit;
            }
           
        }
        $equipmentsItems = EquipmentsItems::model()->findAllByAttributes(array('equipment_entry_id' => $equipmentId));
        echo $this->renderPartial('_equipmentform_edit', array(
            'equipmentsItems' => $equipmentsItems,
            'index'=>$index,
            'equipmentEntry'=>$equipmentEntry,
            // 'warehouse_id'=>$consumptionRequest->warehouse_id,
            'equipment'=>$equipments_arr
        ));
    }

	public function actionGetEquipmentsByProject()
	{
		if (Yii::app()->request->isAjaxRequest && isset($_POST['project_id'])) {
			$project = $_POST['project_id'];
			$equipment_arr = array();
			// Check if project exists in Itemestimation
			$model_exist = Itemestimation::model()->find(array(
				"condition" => "project_id = :project_id",
				"params" => array(":project_id" => $project)
			));

			if (!empty($model_exist)) {
				$estimation_id = $model_exist->itemestimation_id;

				$sql = "SELECT m.equipment FROM `jp_equipments_estimation` m 
						LEFT JOIN `jp_itemestimation` e ON e.itemestimation_id = m.estimation_id 
						WHERE e.itemestimation_id = :estimation_id 
						AND e.itemestimation_status = 2 
						AND m.estimation_id = :estimation_id 
						AND m.project_id = :project_id";

				$equipments = Yii::app()->db->createCommand($sql)
					->bindValues(array(":estimation_id" => $estimation_id, ":project_id" => $project))
					->queryAll();

				if (!empty($equipments)) {
					foreach ($equipments as $equipment_item) {
						$items_det = Equipments::model()->findByPk($equipment_item['equipment']);
						if (!empty($items_det)) {
							$equipment_arr[$items_det->id] = $items_det->equipment_name;
						}
					}
				}
			}
			// Return JSON response
			echo CJSON::encode(array(
				'success' => true,
				'equipments' => $equipment_arr
			));
			Yii::app()->end();
		}

		echo CJSON::encode(array('success' => false, 'message' => 'Invalid request.'));
		Yii::app()->end();
	}

	public function actionViewequipment()
	{
		$id = isset($_GET['id']) ? $_GET['id'] : '';
		$model = EquipmentsEntries::model()->findByPk($id);
		$modelItems = EquipmentsItems::model()->findAllByAttributes(['equipment_entry_id' => $model->id]);
		// echo '<pre>';
		// print_r($modelItems);exit;
		if ($model === null) {
            throw new CHttpException(404, 'The requested consumption request does not exist.');
        }
    
        // Pass data to view
        $this->render('equipment_view', array(
            'model' => $model,
            'modelItems' => $modelItems,
        ));
	}

	public function actionGetDataByDate()
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        $newQuery = "";
        foreach ($arrVal as $arr) {
            if ($newQuery) $newQuery .= ' OR';
            $newQuery .= " FIND_IN_SET('" . $arr . "', e.company_id)";
        }

        $newDate = date('Y-m-d', strtotime($_REQUEST["date"]));
        $expenseData = Yii::app()->db->createCommand("SELECT e.*, p.name as pname,s.subcontractor_name as subname,exp.type_name FROM " . $tblpx . "equipments_entries e
                        LEFT JOIN " . $tblpx . "projects p ON e.project_id = p.pid
                        LEFT JOIN " . $tblpx . "subcontractor s ON e.subcontractor_id = s.subcontractor_id LEFT JOIN " . $tblpx . "expense_type exp on
                        exp.type_id= e.expensehead_id
                        WHERE e.date = '" . $newDate . "' AND (" . $newQuery . ")")->queryAll();
						$pms_api_integration=ApiSettings::model()->pmsIntegrationStatus();
        if ($pms_api_integration == 1) {
            $expenseData = Yii::app()->db->createCommand("SELECT e.*, p.name as pname,s.subcontractor_name as subname,exp.type_name FROM " . $tblpx . "equipments_entries e
                        LEFT JOIN " . $tblpx . "projects p ON e.project_id = p.pid
                        LEFT JOIN " . $tblpx . "subcontractor s ON e.subcontractor_id = s.subcontractor_id LEFT JOIN " . $tblpx . "expense_type exp on
                        exp.type_id= e.expensehead_id
                        WHERE e.date = '" . $newDate . "'")->queryAll();
        }

        $client = $this->renderPartial('newlist', array('newmodel' => $expenseData));

        return $client;
        //echo $newDate;
    }
	public function actionDynamicProject()
	{
		$tblpx = Yii::app()->db->tablePrefix;
		$comId = $_POST["comId"];
		$EquipmentsId = $_POST["EquipmentsId"];

		// Retrieve material items and convert to array
		$EquipmentsItems = EquipmentsItems::model()->findAll('equipment_entry_id=:equipment_entry_id', array(':equipment_entry_id' => $EquipmentsId));
		$EquipmentsItemsArray = array();
		foreach ($EquipmentsItems as $item) {
			$EquipmentsItemsArray[] = $item->attributes;
		}
		// set project and subcontractor first option
		$projectOptions = "<option value=''>-Select Project-</option>";
		$sub_option = "<option value=''>-Select Subcontractor-</option>";
		if ($comId) {
			// Retrieve project data
			$projectData = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}projects WHERE FIND_IN_SET(" . $comId . ", company_id) ORDER BY name ASC")->queryAll();
			foreach ($projectData as $vData) {
				$projectOptions .= "<option value='" . $vData["pid"] . "'>" . $vData["name"] . "</option>";
			}
			// Retrieve subcontractor data
			$bankData = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}subcontractor WHERE FIND_IN_SET(" . $comId . ", company_id) ORDER BY subcontractor_name ASC")->queryAll();
			foreach ($bankData as $vData) {
				$sub_option .= "<option value='" . $vData["subcontractor_id"] . "'>" . $vData["subcontractor_name"] . "</option>";
			}
		}


		// Encode the response as JSON
		echo json_encode(
			array(
				'project' => $projectOptions,
				'subcontractor' => $sub_option,
				'EquipmentsItems' => $EquipmentsItemsArray
			)
		);
	}

	public function actionAjaxcall()
	{
		echo json_encode('success');
	}

	public function actionzamicVendor()
	{
		$expId = $_POST["prId"];
		$tblpx = Yii::app()->db->tablePrefix;

		$user = Users::model()->findByPk(Yii::app()->user->id);
		$arrVal = explode(',', $user->company_id);
		$newQuery = "";
		foreach ($arrVal as $arr) {
			if ($newQuery)
				$newQuery .= ' OR';
			$newQuery .= " FIND_IN_SET('" . $arr . "', v.company_id)";
		}

		if ($expId != 0) {
			$vendorData = Yii::app()->db->createCommand("SELECT v.vendor_id as vendorid, v.name as vendorname FROM " . $tblpx . "vendors v LEFT JOIN " . $tblpx . "vendor_exptype vet ON v.vendor_id = vet.vendor_id LEFT JOIN " . $tblpx . "project_exptype pexp ON pexp.type_id=vet.type_id WHERE pexp.project_id = " . $expId . " AND (" . $newQuery . ") GROUP BY v.vendor_id ORDER BY v.name ASC")->queryAll();
		} else {
			$vendorData = Yii::app()->db->createCommand("SELECT v.vendor_id as vendorid, v.name as vendorname FROM " . $tblpx . "vendors v  WHERE (" . $newQuery . ") ORDER BY v.name ASC")->queryAll();
		}
		$vendorOptions = "<option value=''>-Select Vendor-</option>";
		foreach ($vendorData as $vData) {
			$vendorOptions .= "<option value='" . $vData["vendorid"] . "'>" . $vData["vendorname"] . "</option>";
		}
		echo $vendorOptions;
	}

	public function actionGetexpensehead()
	{
		$id = $_REQUEST['id'];
		$tblpx = Yii::app()->db->tablePrefix;

		$user = Users::model()->findByPk(Yii::app()->user->id);
		$arrVal = explode(',', $user->company_id);
		$newQuery = "";
		foreach ($arrVal as $arr) {
			if ($newQuery)
				$newQuery .= ' OR';
			$newQuery .= " FIND_IN_SET('" . $arr . "', sub.company_id)";
		}
		$expOptions = "<option value=''>-Select Expense Head-</option>";
		if ($id) {
			$expheadData = Yii::app()->db->createCommand("SELECT ex.type_id as typeid, ex.type_name FROM " . $tblpx . "expense_type ex LEFT JOIN " . $tblpx . "subcontractor_exptype sbe ON ex.type_id = sbe.type_id LEFT JOIN " . $tblpx . "subcontractor sub ON sub.subcontractor_id=sbe.subcontractor_id WHERE sub.subcontractor_id = " . $id . " AND (" . $newQuery . ") GROUP BY ex.type_id")->queryAll();
			foreach ($expheadData as $eData) {
				$expOptions .= "<option value='" . $eData["typeid"] . "'>" . $eData["type_name"] . "</option>";
			}
		}
		echo $expOptions;
	}
	// Assuming you are querying inside a controller action
	public function actionGetUnit()
	{
		if (isset($_POST['equipments_id'])) {
			$equipments_id = $_POST['equipments_id'];

			// Querying using Yii1's CActiveRecord
			$equipmentsUnit = Equipments::model()->findByAttributes(array('id' => $equipments_id));
			if ($equipmentsUnit !== null) {
				// Material found, do something with $equipmentsUnit
				echo CJSON::encode(
					array(
						'status' => 'success',
						'data' => $equipmentsUnit->attributes, // Example: return attributes as JSON
					)
				);
			} else {
				// Material not found
				echo CJSON::encode(
					array(
						'status' => 'error',
						'data' => 'Material not found',
					)
				);
			}
		} else {
			echo CJSON::encode(
				array(
					'status' => 'error',
					'data' => 'No material ID provided',
				)
			);
		}
		Yii::app()->end();
	}
	public function actionAddEquipmentsEntries()
	{
		$tblpx = Yii::app()->db->tablePrefix;
		$model = new EquipmentsEntries;
		if (!empty($equipment_id)) {
			$model = EquipmentsEntries::model()->findByPk($equipment_id);
		}
		if (isset($_POST['EquipmentsEntries'])) {
			$user = Users::model()->findByPk(Yii::app()->user->id);
			$arrVal = explode(',', $user->company_id);
			$newQuery = "";
			foreach ($arrVal as $arr) {
				if ($newQuery)
					$newQuery .= ' OR';
				$newQuery .= " FIND_IN_SET('" . $arr . "',e.company_id)";
			}

			// Set model attributes
			$model->attributes = $_POST['EquipmentsEntries'];
			if (isset($_POST['EquipmentsEntries']['date'])) {
				$date = DateTime::createFromFormat('d-m-Y', $_POST['EquipmentsEntries']['date']);
				if ($date !== false) {
					$model->Date = $date->format('Y-m-d');
				} else {

					$model->addError('Date', 'Invalid date format.');
				}
			}
			$model->company_id = $_POST['EquipmentsEntries']['company_id'];
			$model->project_id = $_POST['EquipmentsEntries']['project_id'];
			$model->description = $_POST['EquipmentsEntries']['description'];

			// Set additional fields
			$model->subcontractor_id = $_POST['EquipmentsEntries']['subcontractor_id'];
			$model->expensehead_id = $_POST['EquipmentsEntries']['expensehead_id'];



			if (Yii::app()->user->role == 1) {
				$model->status = 1;
			}

			if ($model->save()) {
				// Save EquipmentsItems data
				if (isset($_POST['EquipmentsItems']) && is_array($_POST['EquipmentsItems'])) {
					// echo '<pre>';
						// print_r($_POST['EquipmentsItems']);exit;
						foreach ($_POST['EquipmentsItems'] as $key => $equipmentsItem) {
							if ($key === 'equipments') continue; // Skip the 'equipments' key
						
							$EquipmentsItem = new EquipmentsItems;
							$EquipmentsItem->equipment_entry_id = $model->id;
							$EquipmentsItem->equipments = $_POST['EquipmentsItems']['equipments'][$key]['equipment_id'] ;// Fetch equipment_id
							$EquipmentsItem->quantity = $equipmentsItem['quantity'];
							$EquipmentsItem->rate = $equipmentsItem['rate'] ;
							$EquipmentsItem->amount = $equipmentsItem['amount'];
							$EquipmentsItem->unit = $equipmentsItem['unit'] ;
							$EquipmentsItem->save();
						}
						
				}

				// Fetch the latest data for rendering
				$expDate = date('Y-m-d', strtotime($_POST['EquipmentsEntries']['date']));
				$expenseData = Yii::app()->db->createCommand()
					->select('e.*, p.name as pname, s.subcontractor_name as subname, exp.type_name')
					->from($tblpx . 'material_entries e')
					->leftJoin($tblpx . 'projects p', 'e.project_id = p.pid')
					->leftJoin($tblpx . 'subcontractor s', 'e.subcontractor_id = s.subcontractor_id')
					->leftJoin($tblpx . 'expense_type exp', 'exp.type_id = e.expensehead_id')
					->where('e.date = :date AND (' . $newQuery . ')', array(':date' => $expDate))
					->queryAll();

				$client = $this->renderPartial('newlist', array('newmodel' => $expenseData));
				return $client;
			} else {
				echo 1;
			}
		}
	}

	public function actionGetAllData()
	{
		$date = $_REQUEST["date"];
		$tblpx = Yii::app()->db->tablePrefix;
		$user = Users::model()->findByPk(Yii::app()->user->id);
		$arrVal = explode(',', $user->company_id);
		$newQuery = "";
		foreach ($arrVal as $arr) {
			if ($newQuery)
				$newQuery .= ' OR';
			$newQuery .= " FIND_IN_SET('" . $arr . "', e.company_id)";
		}

		$expDate = date('Y-m-d', strtotime($date));
		$expenseData = Yii::app()->db->createCommand("SELECT e.*, p.name as pname,s.subcontractor_name as subname,exp.type_name FROM " . $tblpx . "dailyreport e
                                LEFT JOIN " . $tblpx . "projects p ON e.projectid = p.pid
                                LEFT JOIN " . $tblpx . "subcontractor s ON e.subcontractor_id = s.subcontractor_id LEFT JOIN " . $tblpx . "expense_type exp on
                                exp.type_id= e.expensehead_id
                                WHERE e.date = '" . $expDate . "' AND (" . $newQuery . ")")->queryAll();
		$client = $this->renderPartial('newlist', array('newmodel' => $expenseData));
		return $client;
	}
	public function actionRefreshButton()
	{

		$attributes = array('project_id' => null, 'request_from' => 2);
		$equipmentEntry = EquipmentsEntries::model()->findAllByAttributes($attributes);
		if (!empty($equipmentEntry)) {
			foreach ($equipmentEntry as $entry) {
				$acc_project_model = Projects::model()->findByAttributes(array('pms_project_id' => $entry->pms_project_id));
				if ($acc_project_model !== null) {
					$entry->project_id = $acc_project_model->pid;
					;
					$entry->update();
				}
			}
		}
		$equipmentItems = EquipmentsItems::model()->findAll(array('condition' => 'unit IS NULL OR equipments IS NULL'));
		if (!empty($equipmentItems)) {
			foreach ($equipmentItems as $item) {
				if ($item->equipments == null) {
					$acc_equipment_model = Equipments::model()->findByAttributes(array('pms_equipment_id' => $item->pms_equipment_id));
					if ($acc_equipment_model !== null) {
						$item->equipments = $acc_equipment_model->id;
						$item->update();
					}
				}
				if ($item->unit == null) {
					$acc_unit_model = Unit::model()->findByAttributes(array('pms_unit_id' => $item->pms_unit_id));
					if ($acc_unit_model !== null) {
						$item->unit = $acc_unit_model->id;
						$item->update();
					}
				}
			}
		}
		$this->redirect(Yii::app()->createUrl('equipmentsEntries/create'));
	}

	public function actionUpdateEquipmentsEntries()
	{
		$tblpx = Yii::app()->db->tablePrefix;
		
		if (isset($_POST['EquipmentsEntries']['id'])) {
			$equipment_id = $_POST['EquipmentsEntries']['id'];
			$model = EquipmentsEntries::model()->findByPk($equipment_id);

			if ($model !== null && isset($_POST['EquipmentsEntries'])) {
				$user = Users::model()->findByPk(Yii::app()->user->id);
				$arrVal = explode(',', $user->company_id);
				$newQuery = "";
				foreach ($arrVal as $arr) {
					if ($newQuery)
						$newQuery .= ' OR';
					$newQuery .= " FIND_IN_SET(:company_id_$arr, e.company_id)";
				}

				// Update model attributes
				$model->attributes = $_POST['EquipmentsEntries'];
				if (isset($_POST['EquipmentsEntries']['date'])) {
					$date = DateTime::createFromFormat('d-m-Y', $_POST['EquipmentsEntries']['date']);
					if ($date !== false) {
						$model->Date = $date->format('Y-m-d');
					} else {
						$model->addError('date', 'Invalid date format.');
					}
				}
				$model->company_id = $_POST['EquipmentsEntries']['company_id'];
				$model->project_id = $_POST['EquipmentsEntries']['project_id'];
				$model->description = $_POST['EquipmentsEntries']['description'];

				// Set additional fields
				$model->subcontractor_id = $_POST['EquipmentsEntries']['subcontractor_id'];
				$model->expensehead_id = $_POST['EquipmentsEntries']['expensehead_id'];

				// Set status based on user role
				$model->status = 1;

				if ($model->save()) {

					// Update EquipmentsItems data
					if (isset($_POST['EquipmentsItems']) && is_array($_POST['EquipmentsItems'])) {
						// First, delete existing EquipmentsItems for this entry
						// EquipmentsItems::model()->deleteAllByAttributes(['equipment_entry_id' => $model->id]);
						// echo '<pre>';print_r($_POST['EquipmentsItems']);exit;
						// Then, save the new EquipmentsItems
						foreach ($_POST['EquipmentsItems']['equipments'] as $key => $equipmentData) {
							// Check if the 'id' exists in POST data
							$id = isset($equipmentData['id']) ? $equipmentData['id'] : null;
						
							// Find the existing model by ID, or create a new instance if not found
							$EquipmentsItem = EquipmentsItems::model()->findByPk($id);

							if (!$EquipmentsItem) {
								$EquipmentsItem = new EquipmentsItems;
							}

							// Assign values from the posted data
							$EquipmentsItem->equipment_entry_id = $model->id;
							$EquipmentsItem->equipments = isset($equipmentData['equipments']) ? $equipmentData['equipments'] : null;
							$EquipmentsItem->quantity = isset($equipmentData['quantity']) ? $equipmentData['quantity'] : null;
							$EquipmentsItem->unit = isset($equipmentData['unit']) ? $equipmentData['unit'] : null;
							$EquipmentsItem->rate = isset($equipmentData['rate']) ? $equipmentData['rate'] : null;
							$EquipmentsItem->amount = isset($equipmentData['amount']) ? $equipmentData['amount'] : null;
						
							// Save the model
							$EquipmentsItem->save();
						}
						foreach ($_POST['EquipmentsItems'] as $key => $equipmentsItem) {
							if ($key === 'equipments') continue; // Skip the 'equipments' key
						
							$EquipmentsItem = new EquipmentsItems;
							$EquipmentsItem->equipment_entry_id = $model->id;
							$EquipmentsItem->equipments = $_POST['EquipmentsItems']['equipments'][$key]['equipment_id'] ;// Fetch equipment_id
							$EquipmentsItem->quantity = $equipmentsItem['quantity'];
							$EquipmentsItem->rate = $equipmentsItem['rate'] ;
							$EquipmentsItem->amount = $equipmentsItem['amount'];
							$EquipmentsItem->unit = $equipmentsItem['unit'] ;
							$EquipmentsItem->save();
						}
					}

					// Fetch the latest data for rendering
					$expDate = date('Y-m-d', strtotime($_POST['EquipmentsEntries']['date']));
					$params = [':date' => $expDate];
					foreach ($arrVal as $index => $arr) {
						$params[":company_id_$arr"] = $arr;
					}

					$expenseData = Yii::app()->db->createCommand()
						->select('e.*, p.name as pname, s.subcontractor_name as subname, exp.type_name')
						->from($tblpx . 'equipments_entries e')
						->leftJoin($tblpx . 'projects p', 'e.project_id = p.pid')
						->leftJoin($tblpx . 'subcontractor s', 'e.subcontractor_id = s.subcontractor_id')
						->leftJoin($tblpx . 'expense_type exp', 'exp.type_id = e.expensehead_id')
						->where("e.date = :date AND ($newQuery)", $params)
						->queryAll();

					$client = $this->renderPartial('newlist', ['newmodel' => $expenseData]);
					return $client;
				} else {
					echo 1;
				}
			} else {
				throw new CHttpException(404, 'The requested equipment entry does not exist.');
			}
		} else {
			throw new CHttpException(400, 'Invalid request. Please provide a valid equipment entry ID.');
		}
	}

	public function actionGetCompany()
	{
		try {
			$projectId = $_GET['project_id'];
			// Fetch the Project Model
			$project = Projects::model()->findByPk($projectId);

			if ($project === null) {
				throw new Exception('project is not found');
			}
			// Extract Company IDs
			$companyIds = explode(',', $project->company_id);
			// Fetch Company Details
			$criteria = new CDbCriteria();
			$criteria->addInCondition('id', $companyIds);
			$companies = Company::model()->findAll($criteria);
			$listdata = CHtml::listData($companies, 'id', 'name');
			// Prepare the response data
			echo json_encode(array('status' => 1, 'companies' => $listdata));
		} catch (Exception $e) {
			echo json_encode(array('status' => 0, 'message' => $e->getMessage()));
		}

	}

	public function actionQtyBalance() {
		if (Yii::app()->request->isPostRequest) {
			$qty = isset($_POST['qty']) ? intval($_POST['qty']) : 0;
			$equipment = isset($_POST['equipment']) ? $_POST['equipment'] : null;
			$project_id = isset($_POST['project_id']) ? $_POST['project_id'] : null;
	
			$estimation = Itemestimation::model()->findByAttributes(array('project_id' => $project_id));
			
			if(!empty($estimation)&& $estimation->itemestimation_status == 2){
				$equipmentEstimationItems = EquipmentsEstimation::model()->findByAttributes(array('equipment' =>$equipment));
			}
			$balanceQty=$equipmentEstimationItems->quantity-$equipmentEstimationItems->used_quantity;
			// Return success response with equipment list
			echo json_encode([
				'success' => true,
				'balanceQty' => $balanceQty
			]);
			Yii::app()->end();
		}
	
		// Return error if request is invalid
		echo json_encode([
			'success' => false,
			'message' => 'Invalid request.'
		]);
		Yii::app()->end();
	}
	
	
	public function actionApprove()
	{
		$id = $_POST['id'];
		echo '<pre>';print_r($id);exit;
		$transaction = Yii::app()->db->beginTransaction();
		try {
			$equipmentEntry = EquipmentsEntries::model()->findByPk($id);
			$equipmentItems = EquipmentsItems::model()->findAll(
				'equipment_entry_id = :equipment_entry_id',
				array(':equipment_entry_id' => $equipmentEntry->id)
			);
			
			// Fetch Estimation based on project_id
			$project=$equipmentEntry->project_id;
			$estimation = Itemestimation::model()->findByAttributes(array('project_id' => $equipmentEntry->project_id));
			$errors = []; 
			if (!empty($estimation)) {
				// Fetch all equipment estimation items
				$equipmentEstimationItems = EquipmentsEstimation::model()->findAll(
					'estimation_id = :estimation_id',
					array(':estimation_id' => $estimation->itemestimation_id)
				);
				// Convert estimation data into an associative array for easy lookup
				$estimationMap = [];
				foreach ($equipmentEstimationItems as $estimationItem) {
					$key = $estimationItem->equipment;
					$estimationMap[$key] = [
						'quantity' => $estimationItem->quantity,
						'amount'   => $estimationItem->amount,
						'model'    => $estimationItem 
					];
				}
				
				// Compare Equipment Items with Estimations
				foreach ($equipmentItems as $item) {
					//new changes
					$equip=$item["equipments"];

					$sql="SELECT m.id,m.equipment,m.quantity,m.amount,m.used_quantity,m.used_amount FROM `jp_equipments_estimation` m LEFT JOIN `jp_itemestimation` e ON e.itemestimation_id=m.estimation_id WHERE e.project_id=".$project." AND e.itemestimation_status=2  AND m.project_id=".$project ." AND m.equipment=".$equip;
					$estimated_det = Yii::app()->db->createCommand($sql)->queryRow();
					
					$equipment_det = Equipments::Model()->findByPk($equip);

					$equipment_name=$equipment_det["equipment_name"];
					if (!$equipment_det) {
						
						$errors[] = "Equipment with Name $equipment_name  not found.";
						continue;
					}
					$estimated_det = Yii::app()->db->createCommand($sql)->queryRow();
					//mew changes
					if (!$estimated_det) {
						$errors[] = "Estimation details not found or estimation may be not approved for Equipments " . $equipment_name;
						continue;
					}
					$available_quantity = $estimated_det["quantity"] - $estimated_det["used_quantity"];
					if (intval($available_quantity) < intval($item['quantity'])) {
						$errors[] = "Requested quantity ({$item['quantity']}) exceeds available estimated quantity ({$available_quantity}) for Equipments " . $equipment_name;
					}
		
					// Check if the requested amount exceeds the available estimated amount
					$available_amount = $estimated_det["amount"] - $estimated_det["used_amount"];
					if ($available_amount < $item['amount']) {
						$errors[] = "Requested amount ({$item['amount']}) exceeds available estimated amount ({$available_amount}) for Equipments " . $equipment_name;
					}
		
							   
		
		
				}
				if (!empty($errors)) {
					// Combine errors into one message string (or you can return an array)
					$msg = implode("<br/>", $errors);
					echo json_encode(array(
						'response' => 'warning',
						'msg' => $msg,
					));
					return;
				}
			
				
				$equipmentEntry->status = 2;
				if ($equipmentEntry->Save()) {
					//used qty and amount update to estimation
					foreach ($equipmentItems as $item) {
						$key = $item->equipments;
						if (isset($estimationMap[$key])) {
							$estimationModel = $estimationMap[$key]['model'];
							$estimationModel->used_quantity += $item->quantity;
							$estimationModel->used_amount += $item->amount;
							if (!$estimationModel->save()) {
								echo json_encode(['status' => 0, 'errors' => $estimationModel->getErrors()]);
								exit;
							}
						}
					}
					// Create a notification
					$notification = new Notifications;
					$notification->action = "Equipment Entry Approved";
					$equipmentEntry = EquipmentsEntries::model()->findByPk($id);
					$notification->message = "Equipment entry with ID " . $id . " has been approved.";
					$notification->parent_id = $equipmentEntry->id;
					$notification->date = date("Y-m-d");
					$notification->requested_by = Yii::app()->user->id;
					$notification->approved_by = Yii::app()->user->id;

					if (!$notification->save()) {
						echo 0;
						throw new Exception($notification->getErrors());
					}

				} else {
					echo 0;
					throw new Exception('Error updating equipment entry status.');
				}
		    }

			// Commit the transaction
			$transaction->commit();
			$pms_api_integration=ApiSettings::model()->pmsIntegrationStatus();
			if ($pms_api_integration == 1 && $equipmentEntry->request_from == 2) {
				$equipments = $equipmentEntry->equipmentsItems();
				$equipment_data = [];
				foreach ($equipments as $equipment) {
					$equipment_data[] = $equipment->pms_template_equipment_id;
				}
				$request = ['type' => 'equipment'];
				$slug = "api/wpr-approve/" . $equipmentEntry->pms_wpr_id;
				$method = "POST";
				$globalapi_response = GlobalApiCall::callApi($slug, $method, $request);
			}
			echo json_encode(['status' => 1, 'message' => 'Equipment Entry Approved Successfully.']);
			exit;
	
		} catch (Exception $error) {
			// Rollback the transaction in case of error
			$transaction->rollback();
			echo 0;
			throw new CHttpException(500, $error->getMessage());
		}
	}

	public function actionRejectEquipment() {
		if (isset($_POST['id']) && isset($_POST['remarks'])) {
			$id = $_POST['id'];
			$remarks = $_POST['remarks'];

			$equipment = EquipmentsEntries::model()->findByPk($id);
			if ($equipment) {
				$equipment->status = '3';
				$equipment->remarks = $remarks;
				$equipment->pms_wpr_status='1';

				if ($equipment->save()) {
					//Api calling  
					$pms_api_integration=ApiSettings::model()->pmsIntegrationStatus();
					if ($pms_api_integration == 1) {
						$equipments = $equipment->equipmentsItems();
						$equipment_data = [];
						foreach ($equipments as $equipmentItem) {
							$equipment_data[] = $equipmentItem->pms_template_equipment_id;
						}
						$request = ['type' => 'equipment'];
						$slug = "api/wpr-reject/" . $equipment->pms_wpr_id;
						$method = "POST";
						$globalapi_response = GlobalApiCall::callApi($slug, $method, $request);
					}
					echo json_encode(array('response' => 'success', 'msg' => 'Equipment rejected successfully'));
				} else {
					echo json_encode(array('response' => 'error', 'msg' => 'Failed to reject the equipment'));
				}
			} else {
				echo json_encode(array('response' => 'error', 'msg' => 'Equipment not found'));
			}
		} else {
			echo json_encode(array('response' => 'error', 'msg' => 'Invalid request'));
		}
	}
	

	public function actionDeleteEquipments()
	{
		$id = Yii::app()->request->getParam('id');
		$transaction = Yii::app()->db->beginTransaction();

		try {
			$model = EquipmentsEntries::model()->findByAttributes(array('id' => $id));

			if ($model !== null) {
				$equipmentItems = EquipmentsItems::model()->findAllByAttributes(array('equipment_entry_id' => $id));

				// Delete each EquipmentsItems
				foreach ($equipmentItems as $item) {
					$item->delete();
				}

				// Now delete the main EquipmentsEntries model
				if ($model->delete()) {
					$transaction->commit();
					$response = [
						'success' => 'success',
						'message' => 'Record deleted successfully.'
					];
				} else {
					throw new Exception(json_encode($model->getErrors()));
				}
			} else {
				throw new Exception('Equipment entry not found.');
			}

			echo json_encode($response);

		} catch (Exception $error) {
			$transaction->rollback();
			$response = [
				'error' => 'error',
				'message' => $error->getMessage()
			];

			echo json_encode($response);
		}
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model = $this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if (isset($_POST['EquipmentsEntries'])) {
			$model->attributes = $_POST['EquipmentsEntries'];
			if ($model->save())
				$this->redirect(array('view', 'id' => $model->id));
		}

		$this->render(
			'update',
			array(
				'model' => $model,
			)
		);
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if (!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider = new CActiveDataProvider('EquipmentsEntries');
		$this->render(
			'index',
			array(
				'dataProvider' => $dataProvider,
			)
		);
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin($status = '')
	{
		
		$model = new EquipmentsEntries('search');
		$user = Users::model()->findByPk(Yii::app()->user->id);
		$model->unsetAttributes();  
		$tblpx = Yii::app()->db->tablePrefix;
		
		
		if (isset($_GET['EquipmentsEntries'])){
			$model->attributes = $_GET['EquipmentsEntries'];
		}
		$approveDatas = $model->search($status = '2');
		$approvedData = $approveDatas->totalItemCount;

		$nonApproveDatas = $model->search($status = '1');
		$nonApproveData = $nonApproveDatas->totalItemCount;

		$allDatas= $model->search($status='3');
		$allData = $allDatas->totalItemCount;

		$status = isset($_GET['status']) ? $_GET['status'] : '';
		$dataProvider = $model->search($status);
		$dataProvider->setPagination(array(
			'pageSize' => 10, 
		));
		$this->render(
			'admin',
			array(
				'model' => $model,
				'allData' => $allData,
				'approvedData' => $approvedData,
				'nonApprovedData' => $nonApproveData,
				'dataProvider' => $dataProvider,
				'user' => $user,
				'status'=>$status,
			)
		);
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return EquipmentsEntries the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model = EquipmentsEntries::model()->findByPk($id);
		if ($model === null)
			throw new CHttpException(404, 'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param EquipmentsEntries $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'equipments-entries-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
