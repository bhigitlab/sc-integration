<?php
$company = Company::model()->findByPk($model->company_id);
if (!isset($_GET['exportpdf'])) { ?>
	<script>
		$(function () {
			$("#datepicker").datepicker({
				dateFormat: 'dd-mm-yy'
			}).datepicker("setDate", new Date());

		});
	</script>

	<div class="container">
		<div class="invoicemaindiv">
			<div class="header-container">
				<h2>Bills</h2>
				<div class="btn-container">
					<?php echo CHtml::link('<i class="fa fa-file-pdf-o button-icon" aria-hidden="true"></i>', Yii::app()->request->requestUri . "&exportpdf=1", array('class' => 'pdfbtn1 btn btn-info pdf_excel', 'title' => "SAVE AS PDF")); ?>
					<a id="download" class="excelbtn1 btn btn-info pdf_excel" title="SAVE AS EXCEL"> <i
							class="fa fa-file-excel-o button-icon" aria-hidden="true"></i></a>
				</div>
			</div>
			<?php
			$purchaseId = $model['purchase_id'];
			$billId = $model['bill_id']; ?>
			<form id="pdfvals1" method="post" action="">
				<input type="hidden" name="purchaseview">
				<div class="page_filter clearfix">
					<div class="d-flex label-group-wrapper">
						<div class="label-group">
							<label>Company: </label>
							<p>
								<?php echo $company['name']; ?>
								<input type="hidden" name="company" id="company" value="<?php echo $company['name']; ?>">
							</p>
						</div>
						<div class="label-group">
							<label>Project: </label>
							<p>
								<?php echo $project->name; ?>
								<input type="hidden" name="project" id="project" value="<?php echo $project->pid; ?>">
							</p>
						</div>
						<div class="label-group">
							<label>Expense Head: </label>
							<p>
								<?php echo (isset($expense_head->type_name)) ? $expense_head->type_name : ''; ?>

								<input type="hidden" name="expensehead" id="expensehead"
									value="<?php echo (isset($expense_head->type_id)) ? $expense_head->type_id : ''; ?>">
							</p>
						</div>
						<div class="label-group">
							<label>Vendor: </label>
							<p>
								<?php echo (isset($vendor->name)) ? $vendor->name : ''; ?>
							</p>
							<input type="hidden" name="vendor" id="vendor"
								value="<?php echo (isset($vendor->name)) ? $vendor->name : ''; ?>">
						</div>
						<div class="label-group">
							<label>Date:</label>
							<p>
								<?php echo date('d-m-Y', strtotime($model->bill_date)); ?>
								<input type="hidden" name="date"
									value="<?php echo date('d-m-Y', strtotime($model->bill_date)); ?>" readonly>
							</p>
						</div>
						<div class="label-group">
							<label>Bill No:</label>
							<p>
								<?php echo $model->bill_number; ?>
								<input type="hidden" name="bill_number" value="<?php echo $model->bill_number; ?>" readonly>
							</p>
						</div>
					</div>
				</div>
				<input type="hidden" name="purchase_id" value="<?php echo $purchaseId; ?>">
				<input type="hidden" name="bill_id" value="<?php echo $billId; ?>">

			<?php } else { ?>
				<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/pdfstyle.css">
				<div class="container" style="margin:0px 30px">
					<h2 class="text-center">Bills</h2>

					<table border="0" class="details">
						<tr>
							<td><label>COMPANY : </label><span>
									<?php echo $company['name']; ?>
								</span></td>
							<td><label>PROJECT : </label><span>
									<?php echo $project->name; ?>
								</span></td>
						</tr>
						<tr>
							<td><label>EXPENSE HEAD : </label><span>
									<?php echo (isset($expense_head->type_name)) ? $expense_head->type_name : ''; ?>
								</span></td>
							<td><label>VENDOR : </label><span>
									<?php echo (isset($vendor->name)) ? $vendor->name : ''; ?>
								</span></td>
						</tr>
						<tr>
							<td><label>DATE : </label><span>
									<?php echo date("d-m-Y", strtotime($model->bill_date)); ?>
								</span></td>
							<td><label>BILL NO : </label><span>
									<?php echo $model->bill_number; ?>
								</span></td>
						</tr>
					</table>
					<br /><br />
				<?php } ?>

				<div id="table-scroll" class="table-scroll">
					<div id="faux-table" class="faux-table" aria="hidden"></div>
					<div id="table-wrap" class="table-wrap">
						<div class="table-responsive table-wrapper margin-top-10">
							<table border="1" class="table total-table">
								<thead class="entry-table">
									<tr>
										<th>Sl.No</th>
										<th>Description</th>
										<th>HSN Code</th>
										<th>Quantity</th>
										<th>Unit</th>
										<th>Rate</th>
										<th>Tax Slab (%)</th>
										<th>SGST Amount</th>
										<th>SGST (%)</th>
										<th>CGST Amount</th>
										<th>CGST (%)</th>
										<th>IGST Amount</th>
										<th>IGST (%)</th>
										<th>Discount (%)</th>
										<th>Discount Amount</th>
										<th>Amount</th>
										<th>Tax Amount</th>
									</tr>
								</thead>
								<tbody class="addrow">
									<?php
									$tblpx = Yii::app()->db->tablePrefix;
									$data1 = Yii::app()->db->createCommand("SELECT SUM(billitem_amount) as bill_amount FROM {$tblpx}billitem WHERE bill_id=" . $model->bill_id . "")->queryRow();
									$data2 = Yii::app()->db->createCommand("SELECT SUM(billitem_taxamount) as bill_taxamount FROM {$tblpx}billitem WHERE bill_id=" . $model->bill_id . "")->queryRow();
									$data3 = Yii::app()->db->createCommand("SELECT SUM(billitem_discountamount) as bill_discountamount FROM {$tblpx}billitem WHERE bill_id=" . $model->bill_id . "")->queryRow();
									$addcharges = Yii::app()->db->createCommand("SELECT SUM(amount) as amount FROM " . $tblpx . "additional_bill WHERE `bill_id`=" . $model->bill_id)->queryRow();
									$roundoff = Yii::app()->db->createCommand("SELECT round_off as round_off FROM {$tblpx}bills WHERE bill_id=" . $model->bill_id . "")->queryRow();

									$sql = "SELECT SUM(billitem_sgst)+SUM(billitem_cgst)+SUM(billitem_igst) "
										. " AS total_tax FROM `jp_billitem` "
										. " WHERE bill_id=" . $_GET['id'];
									$total_tax = Yii::app()->db->createCommand($sql)->queryScalar();

									$net_amount = ($data1['bill_amount'] + $total_tax) - $data3['bill_discountamount'];
									$grand_total = ($net_amount + $addcharges['amount']) + $roundoff['round_off'];
									?>
									<?php
									if (!empty($item_model)) {
										$items = $item_model['add_items'];
										$i = 1;
										foreach ($items as $key => $value) { ?>
											<tr>
												<td colspan="17" class="categ_td">
													<?php echo !empty($value['category_title']) ? $value['category_title'] : '' ?>
												</td>
											</tr>
											<?php foreach ($value as $item) {
												if (isset($item['specification'])) { ?>
													<tr>
														<td>
															<div id="item_sl_no">
																<?php echo ($i); ?>
															</div>
														</td>
														<td>
															<div class="item_description">
																<?php echo $item['specification']; ?>
															</div>
														</td>
														<td class="text-right">
															<div class="" id="hsn_code">
																<?php echo $item['hsn_code']; ?>
															</div>
														</td>
														<td class="text-right">
															<div class="" id="unit">
																<?php echo $item['quantity']; ?>
															</div>
														</td>
														<td>
															<div class="unit" id="unit">
																<?php echo $item['unit']; ?>
															</div>
														</td>
														<td class="text-right">
															<div class="" id="rate">
																<?php echo Yii::app()->controller->money_format_inr($item['rate'], 2); ?>
															</div>
														</td>
														<td class="text-right">
															<div class="" id="tax_slab">
																<?php echo $item['tax_slab']; ?>
															</div>
														</td>
														<td class="text-right">
															<?php echo Yii::app()->controller->money_format_inr($item['sgst'], 2); ?>
														</td>
														<td class="text-right">
															<?php echo Yii::app()->controller->money_format_inr($item['sgstp'], 2); ?>
														</td>
														<td class="text-right">
															<?php echo Yii::app()->controller->money_format_inr($item['cgst'], 2); ?>
														</td>
														<td class="text-right">
															<?php echo Yii::app()->controller->money_format_inr($item['cgstp'], 2); ?>
														</td>
														<td class="text-right">
															<?php echo Yii::app()->controller->money_format_inr($item['igst'], 2); ?>
														</td>
														<td class="text-right">
															<?php echo Yii::app()->controller->money_format_inr($item['igstp'], 2); ?>
														</td>
														<td class="text-right">
															<div class="" id="rate">
																<?php echo Yii::app()->controller->money_format_inr($item['discp'], 2); ?>
															</div>
														</td>
														<td class="text-right">
															<div class="" id="rate">
																<?php echo Yii::app()->controller->money_format_inr($item['disc'], 2); ?>
															</div>
														</td>
														<td class="text-right">
															<?php echo Yii::app()->controller->money_format_inr($item['amount'], 2); ?>
														</td>
														<td class="text-right">
															<?php echo Yii::app()->controller->money_format_inr($item['tot_tax'], 2); ?>
														</td>
													</tr>
													<?php
													$i++;
												}
											}
											?>
											<?php
										}
									}
									?>
								</tbody>
								<tfoot>
									<tr>
										<th colspan="14" class="text-right">TOTAL: </th>
										<th class="text-right">
											<div id="discount_total">
												<?php echo ($data3['bill_discountamount'] != '') ? number_format($data3['bill_discountamount'], 2) : '0.00'; ?>
											</div>
										</th>
										<th class="text-right">
											<div id="amount_total">
												<?php echo ($data1['bill_amount'] != '') ? number_format($data1['bill_amount'], 2) : '0.00'; ?>
										</th>
										<th class="text-right">
											<div id="tax_total">
												<?php

												echo ($total_tax != '') ? number_format($total_tax, 2) : '0.00'; ?>
										</th>
									</tr>

									<tr>
										<th colspan="14" class="text-right">GRAND TOTAL:
										</th>
										<th colspan="3" class="text-right">
											<div id="grand_total">
												<?php echo ($net_amount != '') ? number_format($net_amount, 2) : '0.00'; ?>
											</div>
										</th>

									</tr>
									<tr>
										<th colspan="14" class="text-right">ADDITIONAL CHARGE : </th>
										<th colspan="3" class="text-right">
											<?php echo ($addcharges['amount'] != '') ? Yii::app()->controller->money_format_inr(round($addcharges['amount'], 2), 2) : 0; ?>
										</th>
									</tr>
									<tr>
										<th colspan="14" class="text-right">ROUND OFF : </th>
										<th colspan="3" class="text-right">
											<?php echo ($roundoff['round_off'] != '') ? Yii::app()->controller->money_format_inr(round($roundoff['round_off'], 2), 2) : 0; ?>
										</th>
									</tr>
									<tr>
										<th colspan="14" class="text-right">NET AMOUNT: </th>
										<th colspan="3" class="text-right">
											<?php echo ($grand_total != '') ? Yii::app()->controller->money_format_inr(round($grand_total, 2), 2) : 0; ?>
										</th>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
				</div>
		</form>
	</div>
	<br /><br />
	<?php
	if (isset($_GET['exportpdf'])) {
		?>
		<h4>Authorized Signatory,</h4>
		<h4></h4>
		<br /><br />
		<?php
	}
	?>
	<script>
		$(function () {

			$('.pdfbtn1').click(function () {
				$("form#pdfvals1").attr('action', "<?php echo $this->createAbsoluteUrl('bills/pdfbills', array('bill_id' => $model->bill_id)); ?>");
				$("form#pdfvals1").submit();

			});

			$('.excelbtn1').click(function () {
				$("form#pdfvals1").attr('action', "<?php echo $this->createAbsoluteUrl('bills/Exportbils', array('bill_id' => $model->bill_id)); ?>");
				$("form#pdfvals1").submit();

			});
		});
	</script>
</div>

<script>
	(function () {
		var mainTable = document.getElementById("main-table");
		if (mainTable !== null) {
			var tableHeight = mainTable.offsetHeight;
			if (tableHeight > 380) {
				var fauxTable = document.getElementById("faux-table");
				document.getElementById("table-wrap").className += ' ' + 'fixedON';
				var clonedElement = mainTable.cloneNode(true);
				clonedElement.id = "";
				fauxTable.appendChild(clonedElement);
			}
		}
	})();
</script>