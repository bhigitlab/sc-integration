

<?php
/* @var $this ProjectTypeController */
/* @var $model ProjectType */

$this->breadcrumbs=array(
	'Project Types'=>array('index'),
	$model->ptid=>array('view','id'=>$model->ptid),
	'Update',
);
/*$this->menu=array(
	array('label'=>'List ProjectType', 'url'=>array('index')),
	array('label'=>'Create ProjectType', 'url'=>array('create')),
	array('label'=>'View ProjectType', 'url'=>array('view', 'id'=>$model->ptid)),
	array('label'=>'Manage ProjectType', 'url'=>array('admin')),
);*/
?>


<div class="modal-dialog modal-sm">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" onclick="javascript:window.location.reload()">&times;</button>
			<h4 class="modal-title">Edit Client Type</h4>
		</div>
		<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
	</div>
	
</div>
