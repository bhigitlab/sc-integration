<style>
  .table.margin-top-15 {
    margin-top: 15px; /* Adjust this if necessary */
    margin-bottom: 0; /* Reduce space below the table */
}

.modal-heading-title-summary {
    margin-top: 0; /* Reduce space above the summary heading */
}
</style>
<?php
$scquotation_id=$model->scquotation_id;
if(!empty($scquotation_id)){
  $item_model = ScquotationItems::model()->findAll('scquotation_id=' .
  $scquotation_id, array('order' => 'item_id DESC'));
  $quotation_categories = ScQuotationItemCategory::model()->findAll('sc_quotaion_id=' .
  $scquotation_id, array('order' => 'id DESC'));
  $view_datas = $this->setSCQuotationsList($item_model, $model, $quotation_categories);
}

?>
<div class="modal-header bg-white">
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                              </button>
                              <h4 class="modal-title">Quotation :<?php echo $model->scquotation_id; ?></h4>
                            </div>
                            <div class="modal-body quotation-view">
                              <div class="row">
                                <div class="col-xs-12 col-md-3">
                                  <label>Company:<?php 
                                  $company = Company::model()->findByPk($model->company_id); ?></label>
                                  <p><?php if($company !== null) {
                                          echo $company->name; 
                                      } else {
                                          echo "Company not found";
                                      }
                                      ?></p>
                                </div>
                                <div class="col-xs-12 col-md-3">
                                  <label>Project:</label>
                                  <p><?php
                                    $project = Projects::model()->findByPk($model->project_id);
                                    if($project !== null) {
                                        echo $project->name;
                                    } else {
                                        echo "Project not found";
                                    }
                                    ?></p>
                                </div>
                                <div class="col-xs-12 col-md-3">
                                  <label>Subcontractor:</label>
                                  <p><?php
                                    $subcontractor = Subcontractor::model()->findByPk($model->subcontractor_id);
                                    if($project !== null) {
                                        echo $subcontractor->subcontractor_name;
                                    } else {
                                        echo "subcontractor not found";
                                    }
                                    ?></p>
                                </div>
                                <div class="col-xs-12 col-md-3">
                                  <label>Date:</label>
                                  <p><?php echo $model->scquotation_date = date('d-m-Y',strtotime($model->scquotation_date)) ?></p>
                                </div>
                                <div class="col-xs-12 col-md-3">
                                  <label>SC Quotation No:</label>
                                  <p><?php echo $model->scquotation_no; ?></p>
                                </div>
                                <div class="col-xs-12 col-md-3">
                                  <label>Expense Head:</label>
                                  <p> <?php echo $model->getExpensehead($model); ?>
                                    </p>
                                </div>
                                <div class="col-xs-12 col-md-3">
                                  <label>Completion Date: </label>
                                  <p><?php echo Yii::app()->Controller->changeDateForamt($model->completion_date); ?></p>
                                </div>
                                <div class="col-xs-12 col-md-3">
                                  <label>Description: </label>
                                  <p><?php echo $model->scquotation_decription; ?></p>
                                </div>
                              </div>
                              <div class="row">
                                <div class="col-xs-12">
                                    <div class="modal-heading-title">CATEGORIES</div>
                                    <div class="dotted-line"></div>
                                </div>
                             </div>
                              <div class="row">
                                <div class="col-xs-12 ">
                                  <table class="table margin-top-15">
                                    <thead class="entry-table">
                                      <tr>
                                        <th>Sl No</th>
                                        <th>Item Name </th>
                                        <th>Quantity</th>
                                        <th>Unit</th>
                                        <th>Rate</th>
                                        <th>Amount</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      
                                        <?php
                                        $i = 1;
                                        if (!empty($view_datas)) {
                                            foreach ($view_datas as $view_data) {
                                                $category_details = $view_data['category_details'];
                                                if (!empty($category_details)) {
                                                    $category_id = $category_details['id'];
                                                    $category_total = ScquotationItems::model()->getCategoryTotal($category_details['sc_quotaion_id'], $category_id);
                                                    $datas = $view_data['items_list'];
                                                    $sql = 'SELECT count(*) as count FROM `jp_scquotation_items` '
                                                            . ' WHERE `item_category_id` = '.$category_id;
                                                    $count = Yii::app()->db->createCommand($sql)->queryScalar();
                                                    if(isset($_GET['exportpdf'])){
                                                        if($count >0 ){
                                                    ?>
                                                    <tr class="blue_bg">
                                                        <td colspan="4" class="light_bg_grey">
                                                            <?php echo sprintf("%02d", $i) . '. ' . $category_details['main_title']; ?>
                                                        </td>
                                                        <td colspan="2" class="light_bg_grey text-right">
                                                        ₹ <?php echo $category_total ?>
                                                        </td>
                                                    </tr>
                                                        <?php } }else{?>
                                                            <tr class="blue_bg">
                                                                <td colspan="4" class="light_bg_grey">
                                                                    <?php echo sprintf("%02d", $i) . '. ' . $category_details['main_title']; ?>
                                                                </td>
                                                                <td colspan="2" class="light_bg_grey text-right">
                                                                ₹ <?php echo $category_total ?>
                                                                </td>
                                                            </tr>
                                                    <?php } ?>
                                                    <?php if ($category_details['main_description']) { ?>
                                                        <tr>
                                                            <td colspan="6">
                                                                <?php echo $category_details['main_description']; ?>
                                                            </td>
                                                        </tr>
                                                    <?php } ?>
                                                    
                                                    <?php
                                                    $k = 1;

                                                    foreach ($datas as $item) {
                                                        ?>
                                                        <tr class="">
                                                            <?php if ($item['item_amount'] != 0) { ?>
                                                                <td><?php echo $k; ?></td>
                                                            <?php } ?>

                                                            <?php if ($item['item_amount'] == 0) { ?>
                                                                <td colspan="6"><?php echo $item['item_description']; ?></td>
                                                            <?php } else { ?>
                                                                <td><?php echo $item['item_description']; ?></td>
                                                            <?php } ?>
                                                            

                                                            <?php
                                                            if ((empty($item['item_quantity'])) && (empty($item['item_rate']))) {
                                                                ?>
                                                                <?php if ($item['item_amount'] != 0) { ?>
                                                                    <td colspan="3"><?php echo 'Lumpsum'; ?></td>
                                                                <?php } ?>
                                                            <?php } else { ?>
                                                                <?php if ($item['item_amount'] != 0) { ?>
                                                                    <td><?php echo $item['item_quantity']; ?></td>
                                                                    <td><?php echo $item['item_unit']; ?></td>
                                                                    <td class="text-right">₹ <?php echo $item['item_rate']; ?></td>
                                                                <?php } ?>
                                                            <?php } ?>
                                                            <?php if ($item['item_amount'] != 0) { ?>
                                                                <td class="text-right">₹ <?php echo isset($item['item_amount']) ? Yii::app()->Controller->money_format_inr($item['item_amount'], 2) : '0.00'; ?>
                                                                <?php } ?>
                                                            </td>
                                                        </tr>
                                                        <?php
                                                        if ($item['item_amount'] != 0) {
                                                            $k++;
                                                        }
                                                    }
                                                  }                                           
                                                }
                                            }
                                            ?>
                                    </tbody>
                                  </table>
                                </div>
                              </div>
                              <div class="row">
                                <div class="col-xs-12">
                                  <div class="modal-heading-title">Tax</div>
                                  <div class="dotted-line"></div>
                                </div>
                              </div>
                              <div class="row">
                                <div class="col-xs-12 ">
                                  <table class="table margin-top-15">
                                    <thead class="entry-table">
                                      <tr>
                                        <th>SGST</th>
                                        <th>CGST</th>
                                        <th>IGST</th>
                                        <th>Total</th>
                                      </tr>
                                    </thead>
                                    <tr>
                                      <td><?php echo $model->sgst_percent; ?>%<?php 
                                        $amount = $model->getTotalQuotationAmount($model->scquotation_id);
                                        $sgst_amount = ($model->sgst_percent / 100) * $amount;
                                        echo '('.$sgst_amount.')';
                                        ?></td>
                                      <td><?php echo $model->cgst_percent; ?>%<?php 
                                        $amount = $model->getTotalQuotationAmount($model->scquotation_id);
                                        $cgst_amount = ($model->cgst_percent / 100) * $amount;
                                        echo '('.$cgst_amount.')';
                                        ?></td>
                                      <td><?php echo $model->igst_percent; ?>%<?php 
                                        $amount = $model->getTotalQuotationAmount($model->scquotation_id);
                                        $igst_amount = ($model->igst_percent / 100) * $amount;
                                        echo '('.$igst_amount.')';
                                        ?></td>
                                      <td><?php echo Controller::money_format_inr(($sgst_amount+$cgst_amount+$igst_amount), 2, 1);   ?></td>
                                    </tr>
                                  </table>
                                </div>
                              </div>
                              <div class="row">
                                <div class="col-xs-12">
                                  <div class="modal-heading-title">Summary</div>
                                  <div class="dotted-line"></div>
                                </div>
                              </div>
                              <div class="row">
                              <div class="col-xs-12 ">
                                  <table class="table margin-top-15">
                                      <tbody>
                                          <?php if (!empty($view_datas)): ?>
                                              <?php $count = 1; $total_amount = 0; ?>
                                              <?php foreach ($view_datas as $data): ?>
                                                  <?php
                                                      $main_title = isset($data['category_details']['main_title']) ? $data['category_details']['main_title'] : 'N/A';
                                                  ?>
                                                  <?php if (!empty($data['items_list'])): ?>
                                                      <?php foreach ($data['items_list'] as $item): ?>
                                                          <tr>
                                                              <td><?= sprintf("%02d", $count) ?></td> <!-- Row number, zero-padded -->
                                                              <td><?= $main_title . '-' . strip_tags($item['item_description']) ?></td> <!-- Main title from category details -->
                                                              <td><?= number_format($item['item_amount'], 2) ?></td> <!-- Quantity -->
                                                          </tr>
                                                          <?php
                                                              $total_amount += $item['item_amount'];
                                                              $count++;
                                                          ?>
                                                      <?php endforeach; ?>
                                                  <?php endif; ?>
                                              <?php endforeach; ?>
                                              <tr class="sub-table-heading">
                                                  <td colspan="2" class="text-right">Total Amount</td>
                                                  <td><?= number_format($total_amount, 2) ?></td>
                                              </tr>
                                              <tr class="sub-table-heading">
                                                  <td colspan="2" class="text-right">Total Tax</td>
                                                  <td><?php $tax=$sgst_amount+$cgst_amount+$igst_amount;
                                                  echo Controller::money_format_inr(($tax), 2, 1);   
                                                   ?></td>
                                              </tr>
                                              <!-- Grand Total Row -->
                                              <tr class="final-table-heading">
                                                  <td colspan="2" class="text-right">Grand Total</td>
                                                  <td><?= number_format($total_amount+$tax, 2) ?></td>
                                              </tr>
                                          <?php else: ?>
                                              <tr>
                                                  <td colspan="3">No data available.</td>
                                              </tr>
                                          <?php endif; ?>
                                      </tbody>
                                  </table>
                              </div>

                              </div>
                              <div class="row">
                                <div class="col-xs-12">
                                  <div class="modal-heading-title">Payment Stages</div>
                                  <div class="dotted-line"></div>
                                </div>
                              </div>
                              <div class="row">
                              <?php
                              if(!empty($scquotation_id)){
                                $payment_stage = Yii::app()->db->createCommand("SELECT * FROM `jp_sc_payment_stage` WHERE `quotation_id` = ".$model->scquotation_id)->queryAll();
                                if (!empty($payment_stage)): 
                                ?>
                                <div class="col-xs-12 ">
                                    <table class="table margin-top-15">
                                        <thead class="entry-table">
                                            <tr>
                                                <th>Sl No</th>
                                                <th>Stage</th>
                                                <th>Stage Percentage</th>
                                                <th>Amount</th>
                                                <th>Bill Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            // Fetch the ordered payment stages for the quotation_id
                                            $payment_stage = Yii::app()->db->createCommand("SELECT * FROM `jp_sc_payment_stage` WHERE `quotation_id` = ".$model->scquotation_id." ORDER BY `serial_number` ASC ")->queryAll();
                                            
                                            foreach ($payment_stage as $key => $value):
                                                // Get the total quotation amount
                                                $quotation_amount = $model->getTotalQuotationAmount($model->scquotation_id);

                                                // Calculate the stage amount based on the percentage
                                                $stage_amount = Controller::money_format_inr($quotation_amount * ($value['stage_percent'] / 100), 2);

                                                // Determine bill status (Pending or Bill Created)
                                                $bill_status = ($value['bill_status'] == 0) ? 'Pending' : 'Bill Created';
                                            ?>
                                            <tr>
                                                <td><?= $value['serial_number'] ?></td> <!-- Serial Number -->
                                                <td><?= $value['payment_stage'] ?></td> <!-- Stage Name -->
                                                <td><?= number_format($value['stage_percent'], 2, '.', '') ?></td> <!-- Stage Percentage -->
                                                <td class="text-right"><?= $stage_amount ?></td> <!-- Calculated Amount -->
                                                <td><?= $bill_status ?></td> <!-- Bill Status -->
                                            </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                                <?php else: ?>
                                    <p>No payment stages available.</p>
                                <?php endif; }?>

                              </div>
                              <div class="row">
                                <div class="col-xs-12">
                                  <div class="modal-heading-title">Terms and Conditions</div>
                                  <div class="dotted-line"></div>
                                </div>
                                <div class="form-group col-xs-12 margin-top-15">
                                  <ul>
                                    <li><?php echo $terms['terms_and_conditions'] ?></li>
                                    <!-- <li>FIrst term</li>
                                    <li>FIrst term</li>
                                    <li>FIrst term</li> -->
                                  </ul>
                                </div>
                              </div>
                            </div>