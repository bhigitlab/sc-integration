<?php
/* @var $this UnitConversionController */
/* @var $model UnitConversion */

$this->breadcrumbs=array(
	'Unit Conversions'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List UnitConversion', 'url'=>array('index')),
	array('label'=>'Manage UnitConversion', 'url'=>array('admin')),
);
?>

<h1>Create UnitConversion</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>