
	<div class="extracharge container">
		<div class="row">
            <div class="col-xs-12">
                <div class="heading-title d-flex">
                        Additional Charges
                        <div class="content-toggle-btn" onclick="toggleDiv()"><i class="fa fa-plus addcharge"></i></div>
                      </div>
					  <span class="errorcharge" style="color: red;"></span>
                <div class="dotted-line"></div>
            </div>
        </div>
			

			<div class="row chargeadd margin-top-20" style="display:none;">
				<div class="form-group col-xs-12 col-md-2">
					<input type="text" placeholder="Charge Category" name="packagecharge" id="labelcat" class="form-control textbox" />
				</div>
				<div class="form-group col-xs-12 col-md-2">
				<input type="number" placeholder="Enter Amount" name="packagecharge" id="currentval" class="form-control textbox currentval" />
				</div>
				<div class="form-group col-xs-12 col-md-1 ">
				<input type="button" class=" btn btn-sm btn-primary savecharge" value="save" />
				</div>
			</div>
			<br>
			<div id="addtional_list">
			<?php
			if(isset($_GET['pid'])){
				$sql = "SELECT * FROM `jp_additional_po_charge` WHERE `purchase_id` =".$_GET['pid'];
				$addchargesall = Yii::app()->db->createCommand($sql)->queryAll();

				 if (!empty($addchargesall)) {
					foreach ($addchargesall as $data) { ?>
						<div class="display_additional" style="display:flex;"> 
							<p style="font-weight: bold;width: 100px;">
							<?= $data['category'] ?> : 
							</p> 
							  <span class='totalprice1' style='width: 70px;'>
							<?= Controller::money_format_inr($data['amount'], 2) ?>
							</span>&nbsp;&nbsp;<span class='icon icon-trash deletecharge' title='Delete' id="<?= $data['id'] ?>" purchase-id="<?= $data['purchase_id'] ?>">
							</span>
						</div>
				<?php }
				} 
			}?>

		</div>
	</div>

