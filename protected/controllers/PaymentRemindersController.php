<?php

class PaymentRemindersController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		/*return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','newlist','viewcomment','addcomment','dynamicproject'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		); */
            
            $accessArr = array();
           $accessauthArr = array();
           $controller = Yii::app()->controller->id;
           if(isset(Yii::app()->user->menuall)){
               if (array_key_exists($controller, Yii::app()->user->menuall)) {
                   $accessArr = Yii::app()->user->menuall[$controller];
               } 
           }

           if(isset(Yii::app()->user->menuauth)){
               if (array_key_exists($controller, Yii::app()->user->menuauth)) {
                   $accessauthArr = Yii::app()->user->menuauth[$controller];
               }
           }
           $access_privlg = count($accessauthArr);
           return array(
               array('allow', // allow all users to perform 'index' and 'view' actions
                   'actions' => $accessArr,
                   'users' => array('@'),
                   'expression' => "$access_privlg > 0",
               ),
			   array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','newlist','viewcomment','addcomment','dynamicproject','ignore','deleteRemainder'),
				'users'=>array('*'),
			),

               array('allow', // allow admin user to perform 'admin' and 'delete' actions
                   'actions' => $accessauthArr,
                   'users' => array('@'),
                   'expression' => "$access_privlg > 0",
               ),
               array('deny',  // deny all users
                   'users'=>array('*'),
               ),
			   
           );
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
               if(isset($_GET['layout']))
                $this->layout = false;
		$model=new PaymentReminders;
	        $this->performAjaxValidation($model);
		if(isset($_POST['PaymentReminders']))
		{
			$model->attributes=$_POST['PaymentReminders'];
                        $model->date = date('Y-m-d',  strtotime($_POST['PaymentReminders']['date']));
                        $model->created_date = date('Y-m-d');
                        if($model->save()){
                            $this->redirect(array('newlist'));
                        }
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
            if(isset($_GET['layout']))
                $this->layout = false;
		$model = $this->loadModel($id);
	        $this->performAjaxValidation($model);
		if(isset($_POST['PaymentReminders']))
		{
			$model->attributes=$_POST['PaymentReminders'];
                        $model->date = date('Y-m-d',  strtotime($_POST['PaymentReminders']['date']));
                        if($model->save()){
                            $this->redirect(array('newlist'));
                        }
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('PaymentReminders');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new PaymentReminders('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['PaymentReminders']))
			$model->attributes=$_GET['PaymentReminders'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}
        
        public function actionNewlist() {
            $model=new PaymentReminders('search');
            
            $model->unsetAttributes();  // clear any default values
            if(isset($_GET['company_id']) || isset($_GET['project_id']) || isset($_GET['date_from']) || isset($_GET['date_to'])){
                $model->project_id       = $_GET['project_id'];
                $model->date_from        = $_GET['date_from'];
                $model->date_to          = $_GET['date_to'];
                $model->client_id        = $_GET['client_id'];
                $model->company_id       = $_GET['company_id'];
                $model->status           = $_GET['status'];
            }
                    //$model->attributes=$_GET['PaymentReminders'];

            $this->render('newlist',array(
                    'model' => $model,
                    'dataProvider' => $model->search(),
            ));
        }
        
        
        public function actionviewcomment() {
            $reminder_id    = $_REQUEST["reminder_id"];
            $tblpx          = Yii::app()->db->tablePrefix;
            $remarksList    = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}reminder_comment WHERE reminder_id = ".$reminder_id." ORDER BY id DESC")->queryAll();
            $result         = "";
            $remarks        = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}payment_reminders WHERE id = ".$reminder_id."")->queryRow();
            if($remarks['status'] == 'Completed'){
                $status ='y';
            }else{
                $status ='n';
            }
            foreach($remarksList as $remark) {
                //$users = Users::model()->findByPk($remark["remark_postedby"]);
                $result .= '<div class="ind_remarks">';
                $result .= '<p>'.$remark["comment"].' - '.$remark["status"].'</p>';
                $result .= '<p class="text-right"><span>Date: '.$remark["created_date"].'</span></p>';
                $result .= '</div>';
            }
            
            echo json_encode(array('result' => $result,'status' =>$status));
        }
		public function actionIgnore(){
			
			$reminder_id=$_GET['reminder_id'];
			$model=PaymentReminders::Model()->findByPk($reminder_id);
			$status=0;
			if(!empty($model)){
				$model->viewed_status=1;
				$model->update();
				$status=1;
			}
			echo json_encode(array('status'=>$status));
			
		}
		public function actiondeleteRemainder(){
			$reminder_id=$_GET['reminder_id'];
			$model=PaymentReminders::Model()->findByPk($reminder_id);
			$status=0;
			if(!empty($model)){
				
				$model->delete();
				$status=1;
			}
			echo json_encode(array('status'=>$status));
		}
        
        public function actionaddcomment() {
        $reminder_id                = $_REQUEST["reminder_id"];
        $remarks                    = $_REQUEST["comment"];
        $status                     = $_REQUEST["status"];
        $date                       = $_REQUEST["date"];
        $model                      = new ReminderComment;
        $model->comment             = $remarks;
        $model->reminder_id         = $reminder_id;
        $model->status              = $status;
        $model->created_date        = date("Y-m-d");
        $tblpx                      = Yii::app()->db->tablePrefix;
        if($model->save()) {
            $update = Yii::app()->db->createCommand("UPDATE {$tblpx}payment_reminders SET status ='".$status."',date ='".date('Y-m-d',  strtotime($date))."' WHERE id = '".$reminder_id."'")->execute();
            $remarksList    = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}reminder_comment WHERE reminder_id = ".$reminder_id." ORDER BY id DESC")->queryAll();
            
            $user = Users::model()->findByPk(Yii::app()->user->id);
            $arrVal = explode(',', $user->company_id);
            $newQuery = "";
            foreach($arrVal as $arr) {
                if ($newQuery) $newQuery .= ' OR';
                $newQuery .= " FIND_IN_SET('".$arr."', company_id)";
            }
            
            $reminderList    = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}payment_reminders WHERE (".$newQuery.") AND (status='open' OR status ='payment_reminders') ORDER BY id DESC")->queryAll();
            $result['html']         = "";
            $result['maillist']         = "";
            foreach($remarksList as $remark) {
                $result['html'] .= '<div class="ind_remarks">';
                $result['html'] .= '<p>'.$remark["comment"].' - '.$remark["status"].'</p>';
                $result['html'] .= '<p class="text-right"><span>Date: '.$remark["created_date"].'</span></p>';
                $result['html'] .= '</div>';
            }
        }
        echo json_encode($result);
    }
    
    public function actiondynamicproject(){
        $html['html'] = '';
        $client_id         = $_GET["client_id"];
        $tblpx         = Yii::app()->db->tablePrefix;
        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        $newQuery = "";
        foreach($arrVal as $arr) {
            if ($newQuery) $newQuery .= ' OR';
            $newQuery .= " FIND_IN_SET('".$arr."', company_id)";
        }
        $projectData    = Yii::app()->db->createCommand("SELECT * FROM ".$tblpx."projects  WHERE client_id=".$client_id." AND (".$newQuery.")")->queryAll();
        $html['html'] .= "<option value=''>Select Project</option>";
        if(!empty($projectData)) {
            foreach($projectData as $vData) {
              $html['html'] .= "<option value='".$vData["pid"]."'>".$vData["name"]."</option>";
            }
        }
        echo json_encode($html);
    }

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return PaymentReminders the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=PaymentReminders::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param PaymentReminders $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='payment-reminders-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
