<?php
/* @var $this ExpensesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'Users',
);

?>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/tableHeadFixer.js"></script>

<div class="container" id="user">
    <div class="loading-overlay">
        <span class="fa fa-spinner fa-3x fa-spin"></span>
    </div>
    <div class="header-container">
        <h3>Bank Reconciliation</h3>
        <div class="add-btn btn-container">
            <?php if ((isset(Yii::app()->user->role) && (in_array('/bank/savereconciliationexcel', Yii::app()->user->menuauthlist)))) { ?>
                <!-- Excel Button -->
                <a type="button" class="btn btn-info" style="cursor:pointer;" title="SAVE AS EXCEL"
                    href="<?php echo Yii::app()->createUrl('bank/savereconciliationexcel', array('fromdate' => $model->fromdate, 'todate' => $model->todate, 'payment_name' => $model->reconciliation_payment, 'bank_id' => $model->reconciliation_bank, 'company_id' => $model->company_id)) ?>">
                    <i class="fa fa-file-excel-o button-icon" aria-hidden="true"></i>
                </a>
            <?php } ?>
            <?php if ((isset(Yii::app()->user->role) && (in_array('/bank/savereconciliationpdf', Yii::app()->user->menuauthlist)))) { ?>
                <!-- PDF Button -->
                <a type="button" class="btn btn-info" style="cursor:pointer;" title="SAVE AS PDF"
                    href="<?php echo Yii::app()->createUrl('bank/savereconciliationpdf', array('fromdate' => $model->fromdate, 'todate' => $model->todate, 'payment_name' => $model->reconciliation_payment, 'bank_id' => $model->reconciliation_bank, 'company_id' => $model->company_id)) ?>">
                    <i class="fa fa-file-pdf-o button-icon" aria-hidden="true"></i>
                </a>
            <?php } ?>
        </div>
    </div>
    <div class="search-form">
        <?php $this->renderPartial('_reconnewreportsearch', array('model' => $model)) ?>
    </div>

    <div id="errorMsg"></div>
    <div class="reconciliation-data">
        <?php $this->widget('zii.widgets.CListView', array(
            'dataProvider' => $dataProvider,
            'viewData' => array('paymentamount' => $paymentamount, 'receiptamount' => $receiptamount),
            'itemView' => '_reconreportnewview',
            'template' => '<div class="sub-heading"><div>{sorter}</div>{summary}</div><div id="parent"><table cellpadding="10" class="table  list-view sorter" id="fixTable">{items}</table></div>',
            'enableSorting' => true,
            'sortableAttributes' => array(
                'reconciliation_id' => 'ID',
                'reconciliation_paymentdate' => 'Payment Date',
                'created_date' => 'Created Date',
                'reconciliation_date' => 'Reconciliation Date',
                'reconciliation_chequeno' => 'Cheque Number',
            ),
        )); ?>

    </div>
</div>
<style>
    #parent {
        max-height: 500px;
    }

    .table {
        margin-bottom: 0px;
    }

    table thead th,
    table tfoot th {
        background-color: #eee;
    }

    .list-view .sorter {
        margin-bottom: 0px !important;
    }

    .table>thead>tr>th,
    .table>tbody>tr>th,
    .table>tfoot>tr>th,
    .table>thead>tr>td,
    .table>tbody>tr>td,
    .table>tfoot>tr>td {
        border-bottom: 0px;
    }
</style>
<script>
    $(function () {
        jQuery.extend(jQuery.expr[':'], {
            focusable: function (el, index, selector) {
                return $(el).is('a, button, :input, [tabindex]');
            }
        });
        $("#parent").css('max-height', $(window).height() - $("#myNavbar").height() - $(".search-form").height() - (2 * $('footer').height()));
        $("#fixTable").tableHeadFixer({
            'head': true,
            'foot': true
        });
        $(document).ajaxComplete(function () {
            $("#parent").css('max-height', $(window).height() - $("#myNavbar").height() - $(".search-form").height() - (2 * $('footer').height()));
            $("#fixTable").tableHeadFixer({
                'head': true,
                'foot': true
            });
        });
        $(document).on('keypress', 'input,select', function (e) {
            if (e.which == 13) {
                e.preventDefault();
                var $canfocus = $(':focusable');
                var index = $canfocus.index(this) + 1;
                if (index >= $canfocus.length) index = 0;
                $canfocus.eq(index).focus();
            }
        });
        $(document).on("dblclick", '.editabledate', function (e) {
            $(this).attr("contenteditable", true);
        });
        $(document).on("keypress", '.editabledate', function (e) {
            if (e.which == 13) {
                var rowId = $(this).attr("data-id");
                var reconId = $("#reconciliationId" + rowId).val();
                var recDate = $(this).text();
                var newDate = $.trim(recDate);
                var fromdate = $("#Reconciliation_fromdate").val();
                var todate = $("#Reconciliation_todate").val();
                var payment = $("#Reconciliation_reconciliation_payment").val();
                var bank = $("#Reconciliation_reconciliation_bank").val();
                var company = $("#Reconciliation_company_id").val();
                $('.loading-overlay').addClass('is-active');
                $.ajax({
                    type: "GET",
                    data: {
                        reconId: reconId,
                        recDate: newDate,
                        fromdate: fromdate,
                        todate: todate,
                        payment: payment,
                        bank: bank,
                        company: company
                    },
                    url: "<?php echo $this->createUrl('bank/reconciliationedit') ?>",
                    success: function (data) {
                        if (data == 1) {
                            $("#errorMsg").html('<div class="alert alert-danger">Please enter a valid date.</div>');
                            return false;
                        } else {
                            $(".reconciliation-data").html(data);
                            $("#errorMsg").html('<div class="alert alert-success">Date updated successfully.</div>');
                        }
                    }
                });
            }
        });

    });
    $(document).ajaxComplete(function () {
        $('.loading-overlay').removeClass('is-active');
        $('#loading').hide();
    });
</script>