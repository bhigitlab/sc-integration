<?php
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * 
 */
$exp_types= CHtml::listData(Companyexpensetype::model()->findAll(array(
                                                'select' => array('company_exp_id, name'),
                                                 'condition' => 'type = 1 || type =2',
                                                'order' => 'name',
                                                'distinct' => true
                                            )), 'company_exp_id', 'name');

array_unshift($exp_types, "");
$exp_typeslist=json_encode(array_values($exp_types));
$rec_types= CHtml::listData(Companyexpensetype::model()->findAll(array(
                                                'select' => array('company_exp_id, name'),
                                                 'condition' => 'type = 0 || type =2',
                                                'order' => 'name',
                                                'distinct' => true
                                            )), 'company_exp_id', 'name');

array_unshift($rec_types, "");
$rec_typeslist=json_encode(array_values($rec_types));
$saveurl = Yii::app()->createAbsoluteUrl("dailyexpense/savedata");

/*
?>
<?php Yii::app()->clientScript->registerScriptFile('http://docs.handsontable.com/0.16.1/bower_components/handsontable/dist/handsontable.full.js', CClientScript::POS_END);?>
<script src="http://docs.handsontable.com/0.16.1/bower_components/handsontable/dist/handsontable.full.js"></script>
<link type="text/css" rel="stylesheet" href="http://docs.handsontable.com/0.16.1/bower_components/handsontable/dist/handsontable.full.min.css">
<?php
*/ ?>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/handsontable.full.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/handsontable.full.min.css" />
<?php
Yii::app()->clientScript->registerCss('mycss', '    
     #example1.handsontable table{
       width:100%;
    }
    .handsontable{
        z-index:0 !important;
    }
    
    #example1.handsontable table td{
        line-height: 25px;
    }
    .handsontable .htCore .readonly {
    background-color: #F2F2F2;
    font-style: italic;
}
#example1consolesucces{
        color: white;
        background: #247B17;
        padding: 5px;
        display:none;
    }
    #example1console{
        color: white;
        background: #E10;;
        padding: 5px;
        display:none;
    }
.console{
 /*       margin-top: 10px;
    margin-left: 550px;
 */
    padding-bottom: 10px;
    color: #E10;
	/*
    width: 250px;
    height: 30px;
	*/
}
.btn{
    margin-top: 25px;
    
}
.link{
    color:blue;

    display:inline-block;
    padding:5px 5px;
    margin-left:10px;
}
.link:hover{
    color:#fff;
    background:#6a8ec7;
    text-decoration:none;
}
.row{
    margin-left: 0px;
  margin-right: -15px;
}
/*.handsontable table.htCore{width:1140px !important;}
.wtHider{width: 1140px !important;}
.wtHolder{overflow-x: hidden !important;}*/

.htCore thead tr th:first-child{display:none;width:1240px}
   .hiden_style{display: none}
   .htCore tr td:nth-child(2),.htCore tr th:nth-child(2)  {border-left: 1px solid #CCC !important;}
   .handsontable .handsontable:not(.ht_master) table {
       -webkit-box-shadow: none;
       box-shadow: none;
   }
   .htCore tr td:last-child {text-align:left !important;}
');
?>
<div class="container">
   <br> <div class="row">Date <br>
        <?php echo CHtml::activeTextField($model, 'date', array('style'=>'height:25px;','size' => 10,'id'=>'Expenses_date','value'=>date('d-M-Y'),'onchange' => 'changedate()')); ?>
        <?php
	$this->widget('application.extensions.calendar.SCalendar',  array(
        //'model'=>$model,
        'inputField' => 'Expenses_date',
            'ifFormat' => '%d-%b-%Y'
        
     ));
        ?>
        <a href="#" id="previous" class="link">Previous</a>&nbsp<a href="#" id="current" class="link">Current</a>&nbsp<a href="#" id="next" class="link">Next</a>
   &nbsp&nbsp&nbsp&nbsp <span id="example1console" class="console"></span><span id="example1consolesucces" ></span>
   <span id="lastentry">Last Entry date : <span id="entrydate"></span></span>
   </div>
 
    <br>
    
    <div id="example1" class="hot handsontable htColumnHeaders"></div>
    <button name="save" id="save" class="btn">Save</button>
    
</div>
<script>
    var notEmpty = function (value, callback) {
        if (String(value) < 0) {
            callback(false);
            $('#example1consolesucces').hide();
            $('#example1console').show();
            $("#example1console").text("You have entered incorrect data");
        } else {
            callback(true);
            $('#example1console').hide();
            //$('#example1consolesucces').show();
        }
    };
    getExpensedata();
    
  ///save action starts from here
  $("#save").click(function(e){
    $("#example1console").text("");   
    $('#save').hide();
    var gridData = hot1.getData();
    var cleanedGridData = {};

    $.each( gridData, function( rowKey, object) {
        if (!hot1.isEmptyRow(rowKey)) cleanedGridData[rowKey] = object;
    });
    //console.log(cleanedGridData);
    var date = $("#Expenses_date").val();
    $.ajax({
        url: "<?php echo $saveurl; ?>",
        data: {"data": cleanedGridData,"date":date}, 
        dataType: "json",
        type: "POST",
        success: function (res) {
            
            if (res.result === "ok") {
                getExpensedata();
                  $("#example1consolesucces").show();
                  $("#example1consolesucces").text("Data saved");
                  $("#example1console").hide();
                   window.setTimeout(function(){
                        $("#example1consolesucces").hide();
                    }, 1000);
                  
                  
            }
            else {
                  $("#example1console").show();
                  $("#example1consolesucces").hide();
                  $("#example1console").text("Invalid input");
            }
             $('#save').show();
        },
        error: function () {
            $("#example1consolesucces").hide();
            $("#example1console").show();
          $("#example1console").text("Save error.");
           $('#save').show();
        }
    });
});
//save action end here

   //document.addEventListener("DOMContentLoaded", function() {




var testremoveRenderer= function (instance, td, row, col, prop, value, cellProperties) {
  Handsontable.TextCell.renderer.apply(this, arguments);
  $(td).addClass('hiden_style');
};
var colorRenderer= function (instance, td, row, col, prop, value, cellProperties) {
  Handsontable.TextCell.renderer.apply(this, arguments);
  $(td).addClass("readonly");
};
var removeRenderer= function (instance, td, row, col, prop, value, cellProperties) {
  Handsontable.TextCell.renderer.apply(this, arguments);
  $(td).removeClass("readonly");
};
var totalRenderer = function(instance, td, row, col, prop, value){
           
            if(row == instance.countRows() - 1){
                    //value = sum;
                   value = getTotal();
                }
                  
                Handsontable.NumericRenderer.apply(this, arguments);
               
            
        };
var totalRenderer1 = function(instance, td, row, col, prop, value){
           
            if(row == instance.countRows() - 1){
                    //value = sum;
                   value = getTotal1();
                }
                  
                Handsontable.NumericRenderer.apply(this, arguments);
               
            
        };        
/*--Mod by Rajisha -- Column expense type*/
var globalHeaders = ["ID","Expense type","Expense Amount","Receipt Type", "Receipt Amount","Description"];
var globalColumns = [
	   {type: "numeric",readOnly:true,renderer:testremoveRenderer},
           {type:"autocomplete",allowInvalid: false, strict: true,source:<?php echo $exp_typeslist; ?>,renderer: function(instance, td, row, col, prop, value){
                if(row == instance.countRows() - 1){
                    td.style.fontWeight = "bold";
                    td.style.textAlign = "right";
                    td.textContent = "Total: ";
                    return;
                } else {
                    Handsontable.NumericRenderer.apply(this, arguments);
                }
            }}   ,
           { 
                type: "numeric", allowInvalid: true, renderer:totalRenderer

           },
           {type:"autocomplete",allowInvalid: false, strict: true,source:<?php echo $rec_typeslist; ?>}   ,
           { 
                type: "numeric",allowInvalid: true,renderer:totalRenderer1

           },
           {type: "text"},
    ];
    
var colsToHide = []; colsToHide.push(0);

  var container = document.getElementById("example1"),    
 data = [];
  hot1 = new Handsontable(container, {
    data: data,
	rowHeaders: false,
    contextMenu:false,
    //dataSchema: { projectid: null, expensetype: null, description:null, expense: null},
    //startRows: 5,
    //startCols: 4,
    //rowHeights: 30,
     colWidths: [100,100,100,100,300],
    colHeaders: globalHeaders,
	fillHandle: true,
	stretchH: "all",
    autoWrapRow:true, 
    enterMoves: {row: 0, col: 1},
    columns: globalColumns,
    minSpareRows: 2,
    persistentState: true,
    afterInit:function(){
        //alert(this.getDataAtCell(1,5));
    },
    afterDocumentKeyDown:function(event) {
       //$(document).keypress(function(event){
       var f;
	var keycode = (event.keyCode ? event.keyCode : event.which);
        flag = 0;
    var instance = this;
    var sel = instance.getSelected();
        if(sel != null)
        {
            var prevRow = sel[0]-1;
            if(curentRow != prevRow)
            {
                var f = 0;
                curentRow = prevRow;
            }
            if(!instance.getDataAtCell(sel[0],0))
            {
                var type = this.getDataAtCell(prevRow,1);
                var amount = this.getDataAtCell(prevRow,2);
                var retype = this.getDataAtCell(prevRow,3);
                var reamnt = this.getDataAtCell(prevRow,4);
                var desc = this.getDataAtCell(prevRow,5);
                
//                if((!amount))
//                {
//                    hot1.validateCell(this.getDataAtCell(prevRow, 1), this.getCellMeta(prevRow, 1), function(){hot1.render(); });
//                    
//                }
//                else{    
                    if((amount && type) || (retype && reamnt) && !instance.getDataAtCell(prevRow,0)){
                        flag = 1;
                    }
                     if(f == 0 && keycode == '13'){
                    if(!instance.getDataAtCell(prevRow,0)){
                       f=1;
                       var date = $("#Expenses_date").val();
                        $.ajax({
                            type: "POST",
                            url: "<?php echo Yii::app()->createAbsoluteUrl('dailyexpense/progressSave'); ?>",
                               data: {desc: desc,amnt:amount,date:date,type:type,retype:retype,reamnt:reamnt}, 
                               dataType:"JSON",
                            success:function(data){			 
                                if(data.result == "ok")
                                {
                                    $("tbody tr:nth-child("+sel[0]+")").addClass("savedata");
                                   /* window.setTimeout(function(){
                                        $("tr:nth-child("+sel[0]+")").removeClass("savedata");
                                      }, 10000);*/
                                    instance.setDataAtCell(prevRow,0,data.id);
                                }else{
                                    $('#example1consolesucces').hide();
                                    $('#example1console').show();
                                    $("#example1console").text("You are not entered Expense Type, Receipt Type,Expense, Receipt, Purchase Type or Paid. Please check the entries.");
                                    window.setTimeout(function(){
                                        $("#example1console").hide();
                                    }, 1000);
                                }
                                
                            },
                        });
                   // }
                    }
                    }
            }
            
        }
    },
      afterChange: function (change, source) {
        if(change!=null)
        {
                 
        
            for(var i = 0 ; i < change.length ; i++) {
                var instance = this;
                if(change[i][1] == 1){
                    if(change[i][3] != ''){
                        this.setDataAtCell(change[i][0],3,"");
                        this.setDataAtCell(change[i][0],4,"");
                        var cellPropertiesID = instance.getCellMeta(change[i][0],3);
                        cellPropertiesID.readOnly = true;
                        cellPropertiesID.renderer = colorRenderer;
                        cellPropertiesID = instance.getCellMeta(change[i][0],4);
                        cellPropertiesID.readOnly = true;
                        cellPropertiesID.renderer = colorRenderer;
                    }else{
                        var cellPropertiesID = instance.getCellMeta(change[i][0],3);
                        cellPropertiesID.readOnly = false;
                        cellPropertiesID.renderer = removeRenderer;
                        cellPropertiesID = instance.getCellMeta(change[i][0],4);
                        cellPropertiesID.readOnly = false;
                        cellPropertiesID.renderer = removeRenderer;
                    }   
                }
                if(change[i][1] == 3){
                    if(change[i][3] != ''){
                        this.setDataAtCell(change[i][0],1,"");
                        this.setDataAtCell(change[i][0],2,"");
                        var cellPropertiesID = instance.getCellMeta(change[i][0],1);
                        cellPropertiesID.readOnly = true; 
                        cellPropertiesID.renderer = colorRenderer;
                        cellPropertiesID = instance.getCellMeta(change[i][0],2);
                        cellPropertiesID.readOnly = true; 
                        cellPropertiesID.renderer = colorRenderer;
                    }else{
                        var cellPropertiesID = instance.getCellMeta(change[i][0],1);
                        cellPropertiesID.readOnly = false;
                        cellPropertiesID.renderer = removeRenderer;
                        cellPropertiesID = instance.getCellMeta(change[i][0],2);
                        cellPropertiesID.readOnly = false;
                        cellPropertiesID.renderer = removeRenderer;
                    }
                }
            }
        }
        },
        beforeRemoveRow: function (index, amount) {
            var cellProperties = this.getDataAtRow(index);
            var retVal = confirm("Are you sure You want to delete this row ?");
            if (retVal == true) {
                var data = cellProperties[0];


                var data2 = index + 1;

                $.ajax({
                    url: "<?php echo Yii::app()->createAbsoluteUrl('dailyexpense/handsondelete'); ?>",
                    data: {id: data},
                    dataType: "json",
                    type: "POST",
                    success: function (res) {
                        if (res.result === "ok") {
                            $("#example1consolesucces").show();
                            $('#example1console').hide();
                            $("#example1consolesucces").text("Row Deleted");
                        }
                    }
                });
                return true;
            } else {

                return false;
            }
        },
     /*   beforeKeyDown: function (e) {

            if (e.keyCode === 46) {
                var selection = this.getSelected();
                var instance = this;
                var retVal = confirm("Do you want to delete this row ?");
                if (retVal == true) {
                    var cellProperties = this.getDataAtRow(selection[0]);

                    var data = cellProperties[0];
                    $.ajax({
                        url: "<?php echo Yii::app()->createAbsoluteUrl('dailyexpense/handsondelete'); ?>",
                        data: {id: data},
                        dataType: "json",
                        type: "POST",
                        success: function (res) {
                            if (res.result === "ok") {
                                $("#example1consolesucces").show();
                                $("#example1consolesucces").text("Row Deleted");
                                instance.alter("remove_row", selection[0]);
                            }
                        },
                        error: function () {
                            instance.alter("remove_row", selection[0]);
                        }
                    });
                    return true;
                } else {

                    return false;
                }

            }
        } */
  });



  var res;
 var flag;
  //get expense details
  function getExpensedata(){
  var candate=$("#Expenses_date").val();
    $.ajax({
        type: "POST",
        data:{cadate:candate},
        url: "<?php echo Yii::app()->createAbsoluteUrl('dailyexpense/getdata'); ?>",
       dataType:"JSON",
        success:function(data){	
            if(data.lastdate != ''){
                $('#entrydate').html(data.lastdate); 
            }else{
                $('#lastentry').html('');
            } 
            jsonArr = JSON.parse(JSON.stringify(data));  //alert(jsonArr.proj_exp[0].Project1);
            var data = jsonArr.result;		 
            
            res = data;//delete data[0][0];
            hot1.loadData(data);
            return data;
        },
    });

}  
 var jsonArr;


function getTotal(){
    if (typeof res != "undefined"){
        return res.reduce(function(sum, row){
            var chkval = (row[2])?row[2]:0;
            chkval = $.isNumeric(chkval)?chkval:0;
                 return eval(sum) + eval(chkval); 
             }, 0);
           
    }
}

function getTotal1(){
    if (typeof res != "undefined"){
        return res.reduce(function(sum, row){
            var chkval = (row[4])?row[4]:0;
            chkval = $.isNumeric(chkval)?chkval:0;
                 return eval(sum) + eval(chkval); 
             }, 0);
           
    }
}
var curentRow = 0;


function validDate(d) {
  var bits = d.split("-");
  var t = stringToDate(d);
  return t.getFullYear() == bits[0] && 
         t.getDate() == bits[2];
}


// Convert string in format above to a date object
function stringToDate(s) {
  var bits = s.split("-");
  var monthNum = monthNameToNumber(bits[1]);
  return new Date(bits[2], monthNum, bits[0]);
}

// Convert month names like mar or march to 
// number, capitalisation not important
// Month number is calendar month - 1.
var monthNameToNumber = (function() {
  var monthNames = (
     "jan feb mar apr may jun jul aug sep oct nov dec " +
     "january february march april may june july august " +
     "september october november december"
     ).split(" ");

  return function(month) {
    var i = monthNames.length;
    month = month.toLowerCase(); 

    while (i--) {
      if (monthNames[i] == month) {
        return i % 12;
      }
    }
  }
}());

// Given a date in above format, return
// previous day as a date object
function getYesterday(d) {
  d = stringToDate(d);
  d.setDate(d.getDate() - 1)
  return d;
}
function getTomorrow(d) {
  d = stringToDate(d);
  d.setDate(d.getDate() + 1)
  return d;
}

// Given a date object, format
// per format above
var formatDate = (function() {
  var months = "Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec".split(" ");
  function addZ(n) {
    return n<10? "0"+n : ""+n;
  }
  return function(d) {
    return addZ(d.getDate()) + "-" + 
           months[d.getMonth()] + "-" + 
           d.getFullYear();
  }
}());




$("#previous").click(function(){
var candate=$("#Expenses_date").val();
var prevdate = formatDate(getYesterday(candate));
$("#Expenses_date").val(prevdate) ;
// var candate=$("#Expenses_date").val();
// var d = new Date(candate);
//var prevdte= d.setDate(d.getDate() - 1);
//
//$("#Expenses_date").val(formatDate(d)) ;
  $.ajax({
        type: "POST",
        data:{cadate:prevdate},
        //data:{cadate:formatDate(d)},
        url: "<?php echo Yii::app()->createAbsoluteUrl('dailyexpense/expensedata'); ?>",
       dataType:"JSON",
       success:function(data){			 

            //var data = data.result;
            //res = data;//delete data[0][0];
            
           // hot1.loadData(data);
            
           // return data;
		    getExpensedata();
        },complete: function (data) {
        var divwidth = $(".wtHider").width();
        $( ".htCore" ).css("width",divwidth+"px");
       
        }
        
    });
//    var d1 = new Date();
//    if($("#Expenses_date").val()==formatDate(d1)){
var d1 = new Date();
    var dd = d1.getDate();
    var mm = d1.getMonth();
    var yyyy = d1.getFullYear();
    var months = "Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec".split(" ");
    //alert($("#Expenses_date").val()+"=="+dd+"-"+months[mm]+"-"+yyyy);
    //alert($("#Expenses_date").val());
    if($("#Expenses_date").val() == formatDate(d1))
    {
    $("#next").hide();
    }
else{
    $("#next").show();
}


});

 $("#next").click(function(){
 
 var candate=$("#Expenses_date").val();
 //alert(candate);
 var nxtdate = formatDate(getTomorrow(candate));
 //alert(nxtdate);
$("#Expenses_date").val(nxtdate) ;
// var d = new Date(candate);
// var prevdte= d.setDate(d.getDate() + 1);
//
//$("#Expenses_date").val(formatDate(d)) ;
  $.ajax({
        type: "POST",
        data:{cadate:nxtdate},
        url: "<?php echo Yii::app()->createAbsoluteUrl('dailyexpense/expensedata'); ?>",
       dataType:"JSON",
       success:function(data){			 

            //var data = data.result;
           // res = data;//delete data[0][0];
           // hot1.loadData(data);
           // return data;
		    getExpensedata();
        },
        
    });
    var d1 = new Date();
    var dd = d1.getDate();
    var mm = d1.getMonth();
    var yyyy = d1.getFullYear();
    var months = "Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec".split(" ");
    if($("#Expenses_date").val()==formatDate(d1)){
    $("#next").hide();
    }
    
    });
    $("#next").hide();
    $("#current").click(function(){
 var d = new Date();
$("#Expenses_date").val(formatDate(d)) ;
  $.ajax({
        type: "POST",
        data:{cadate:formatDate(d)},
        url: "<?php echo Yii::app()->createAbsoluteUrl('dailyexpense/expensedata'); ?>",
       dataType:"JSON",
       success:function(data){			 

           // var data = data.result;
           // res = data;//delete data[0][0];
           // hot1.loadData(data);
           // return data;
		    getExpensedata();
        },
        
    });
    $("#next").hide();
    });
 
 
 
function formatDate(date){
    var pdate = new Date(date),
        monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
  "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
];
        month=monthNames[pdate.getMonth()];
        day = "" + pdate.getDate(),
        year = pdate.getFullYear();

    //if (month.length < 2) month = "0" + month;
    if (day.length < 2) day = "0" + day;

    return [day, month, year].join("-");
   
}
var flag;
 var curentRow = 0;
hot1.addHook("afterRender", function(){
    flag = 0;
    var instance = this;
    var sel = instance.getSelected();
        if(sel != null)
        {
            var prevRow = sel[0]-1;
            if(curentRow != prevRow)
            {
                var f = 0;
                curentRow = prevRow;
            }
            if(!instance.getDataAtCell(sel[0],0))
            {
                var type = this.getDataAtCell(prevRow,1);
                var amount = this.getDataAtCell(prevRow,2);
                var retype = this.getDataAtCell(prevRow,3);
                var reamnt = this.getDataAtCell(prevRow,4);
                var description = this.getDataAtCell(prevRow,5);
                
//               if((!amount))
//                {
//                    hot1.validateCell(this.getDataAtCell(prevRow, 1), this.getCellMeta(prevRow, 1), function(){hot1.render(); });
//                    
//                }
//                else{    
                    if((amount && type) || (retype && reamnt) && !instance.getDataAtCell(prevRow,0)){
                        flag = 1;
                    }
                     if(f == 0){
                    if(!instance.getDataAtCell(prevRow,0)){
                       f=1;
                       var date = $("#Expenses_date").val();
                        $.ajax({
                            type: "POST",
                            url: "<?php echo Yii::app()->createAbsoluteUrl('dailyexpense/progressSave'); ?>",
                                data: {desc: description,amnt:amount,date:date,type:type,retype:retype,reamnt:reamnt}, 
                                dataType:"JSON",
                            success:function(data){			 
                                if(data.result == "ok")
                                {
                                    $("tbody tr:nth-child("+sel[0]+")").addClass("savedata");
                                   /* window.setTimeout(function(){
                                        $("tr:nth-child("+sel[0]+")").removeClass("savedata");
                                      }, 10000);*/
                                    instance.setDataAtCell(prevRow,0,data.id);
                                }else{
                                    $('#example1consolesucces').hide();
                                    $('#example1console').show();
                                    $("#example1console").text("You are not entered Expense Type, Receipt Type,Expense, Receipt. Please check the entries.");
                                    window.setTimeout(function(){
                                        $("#example1console").hide();
                                    }, 1000);
                                }
                                
                            },
                        });
                    }
                    //}
                    }
            }
            
        }
       
});
        
  function bindDumpButton() {
      if (typeof Handsontable === "undefined") {
        return;
      }
  
      Handsontable.Dom.addEvent(document.body, "click", function (e) {
  
        var element = e.target || e.srcElement;
  
        if (element.nodeName == "BUTTON" && element.name == "dump") {
          var name = element.getAttribute("data-dump");
          var instance = element.getAttribute("data-instance");
          var hot = window[instance];
          console.log("data of " + name, hot.getData());
        }
      });
    }
  bindDumpButton();
  
function changedate()
    {
      var candate=$("#Expenses_date").val();
        var d1 = new Date();
		var dd = d1.getDate();
		var mm = d1.getMonth();
		var yyyy = d1.getFullYear();
		var months = "Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec".split(" ");
		if(new Date($("#Expenses_date").val()) < new Date(formatDate(d1))){
		$("#next").show();
		}
		else
		{
		$("#next").hide();
		}
    
        $.ajax({
        type: "POST",
        data:{cadate:candate},
        url: "<?php echo Yii::app()->createAbsoluteUrl('dailyexpense/expensedata'); ?>",
       dataType:"JSON",
       success:function(data){	//alert(data.proj_exp);
            // jsonArr = JSON.parse(JSON.stringify(data));  //alert(jsonArr.proj_exp[0].Project1);
            // var data = jsonArr.result;//alert(data);
            // res = data;//delete data[0][0];
            // hot1.loadData(data);
           // return data;
		    getExpensedata();
        },
        
    });
  }
</script>
