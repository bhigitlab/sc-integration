<?php

/**
 * This is the model class for table "{{subcontractor}}".
 *
 * The followings are the available columns in table '{{subcontractor}}':
 * @property integer $subcontractor_id
 * @property string $subcontractor_name
 * @property string $subcontractor_phone
 * @property string $subcontractor_email
 * @property integer $subcontractor_status
 * @property string $subcontractor_description
 * @property string $gst_no
 * @property string $company_id
 * @property integer $expense_type_id
 * @property string $type
 * @property string $pan_no
 * @property string $adhar_no
 * @property string $address
 * @property string $account_details
 * @property string $created_date
 * @property integer $created_by
 */
class Subcontractor extends CActiveRecord
{
	
	public $gst_no;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{subcontractor}}';
	}

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('subcontractor_name, subcontractor_phone, subcontractor_status, subcontractor_description, created_date, created_by', 'required'),
            array('subcontractor_status, expense_type_id, created_by', 'numerical', 'integerOnly'=>true),
            array('subcontractor_name, subcontractor_email, gst_no', 'length', 'max'=>100),
            array('subcontractor_phone', 'length', 'max'=>13),
            array('type', 'length', 'max'=>1),
            array('pan_no, adhar_no', 'length', 'max'=>300),
            array('address, account_details', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('subcontractor_id, subcontractor_name, subcontractor_phone, subcontractor_email, subcontractor_status, subcontractor_description, gst_no, company_id, expense_type_id, type, pan_no, adhar_no, address, account_details, created_date, created_by', 'safe', 'on'=>'search'),
        );
    }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'subcontractor_id' => 'Subcontractor',
            'subcontractor_name' => 'Subcontractor Name',
            'subcontractor_phone' => 'Subcontractor Phone',
            'subcontractor_email' => 'Subcontractor Email',
            'subcontractor_status' => 'Subcontractor Status',
            'subcontractor_description' => 'Subcontractor Description',
            'gst_no' => 'Gst No',
            'company_id' => 'Company',
            'expense_type_id' => 'Expense Type',
            'type' => 'Type',
            'pan_no' => 'Pan No',
            'adhar_no' => 'Adhar No',
            'address' => 'Address',
            'account_details' => 'Account Details',
            'created_date' => 'Created Date',
            'created_by' => 'Created By',
        );
    }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

        $criteria->compare('subcontractor_id',$this->subcontractor_id);
        $criteria->compare('subcontractor_name',$this->subcontractor_name,true);
        $criteria->compare('subcontractor_phone',$this->subcontractor_phone,true);
        $criteria->compare('subcontractor_email',$this->subcontractor_email,true);
        $criteria->compare('subcontractor_status',$this->subcontractor_status);
        $criteria->compare('subcontractor_description',$this->subcontractor_description,true);
        $criteria->compare('gst_no',$this->gst_no,true);
        $criteria->compare('company_id',$this->company_id,true);
        $criteria->compare('expense_type_id',$this->expense_type_id);
        $criteria->compare('type',$this->type,true);
        $criteria->compare('pan_no',$this->pan_no,true);
        $criteria->compare('adhar_no',$this->adhar_no,true);
        $criteria->compare('address',$this->address,true);
        $criteria->compare('account_details',$this->account_details,true);
        $criteria->compare('created_date',$this->created_date,true);
        $criteria->compare('created_by',$this->created_by);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
             'pagination'=>false,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Subcontractor the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        public function getExpensetypes($id){
            $tblpx = Yii::app()->db->tablePrefix;
            $exp = Yii::app()->db->createCommand()
               ->select(['ex.type_name'])
                ->from($tblpx.'subcontractor as s')
                ->leftJoin($tblpx.'subcontractor_exptype se', 's.subcontractor_id = se.subcontractor_id')
                ->leftJoin($tblpx.'expense_type ex', 'ex.type_id = se.type_id')
                ->where('s.subcontractor_id=:subcontractor_id', array(':subcontractor_id'=>$id))
               ->queryAll();
		foreach($exp as $exptype){
             $type[]=$exptype['type_name'];
        }
        $usrnames="";
        if($type!=NULL){
            $usrnames = implode(', ',$type);
        }
        return $usrnames;	 
        
    }
}
