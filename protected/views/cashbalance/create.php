<?php
/* @var $this CashbalanceController */
/* @var $model Cashbalance */

$this->breadcrumbs = array(
    'Cashbalances' => array('index'),
    'Create',
);


?>

<!--<div class="modal-dialog modal-sm">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" onclick="javascript:window.location.reload()">&times;</button>
            <h4 class="modal-title">Add Cashbalance</h4>
        </div>
        <?php //echo $this->renderPartial('_form', array('model' => $model)); 
        ?>
    </div>

</div>-->
<div class="panel panel-gray">
    <div class="panel-heading form-head">
        <h3 class="panel-title">Add Cashbalance</h3>
    </div>
    <div id="loading"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/ajax-loader.gif"></div>
    <?php echo $this->renderPartial('_form', array('model' => $model)); ?>
</div>
<script>
    $(document).ready(function() {
        $('#loading').hide();
    });
</script>