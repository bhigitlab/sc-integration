<div class="table-responsive">
    <table cellpadding="10" class="table  list-view " id="fixtable">
        <thead class="entry-table">
            <tr>
                <th>SL No</th>
                <th>Receipt No</th>
                <th>Item Description</th>
                <th>Qty in Nos</th>
                <th>Rate</th>
                <th>Amount</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td colspan="6">
                    <?php
                    $vendorName = Vendors::model()->findByPk($vendor);
                    echo isset($vendorName->name) ? $vendorName->name : "N/A";
                    ?>
                </td>
            </tr>
            <?php
            $sql = "SELECT * FROM `jp_warehousereceipt_items` i,jp_warehousereceipt r,"
                . " jp_bills b,jp_purchase p,jp_vendors v "
                . " WHERE i.warehousereceipt_id=r.warehousereceipt_id "
                . " AND r.warehousereceipt_bill_id = b.bill_id "
                . " AND b.purchase_id=p.p_id AND p.vendor_id=v.vendor_id "
                . " AND v.vendor_id =" . $vendor;
            $receipt_items = Yii::app()->db->createCommand($sql)->queryAll();
            $i = 1;
            foreach ($receipt_items as $key => $value) {
                ?>
                <tr>
                    <td>
                        <?php echo $i; ?>
                    </td>
                    <td>
                        <?php
                        $warehouse_receipt = Warehousereceipt::model()->findByPk($value['warehousereceipt_id']);

                        echo CHtml::link($warehouse_receipt->warehousereceipt_no, 'index.php?r=wh/warehousereceipt/view&receiptid=' . $warehouse_receipt['warehousereceipt_id'], array('class' => 'link', 'target' => '_blank'));
                        ?>
                    </td>
                    <td>
                        <?php
                        $tblpx = Yii::app()->db->tablePrefix;
                        $spec_sql = "SELECT id, specification, brand_id, cat_id "
                            . " FROM {$tblpx}specification "
                            . "WHERE id=" . $value['warehousereceipt_itemid'];
                        $specification = Yii::app()->db->createCommand($spec_sql)->queryRow();

                        $brand = '';
                        if ($specification['brand_id'] != NULL) {
                            $sql = "SELECT brand_name FROM {$tblpx}brand "
                                . "WHERE id=" . $specification['brand_id'];
                            $brand_details = Yii::app()->db->createCommand($sql)->queryRow();
                            $brand = '-' . ucwords($brand_details['brand_name']);
                        }
                        $category_name = "";
                        if ($specification['cat_id'] != "") {
                            $category = PurchaseCategory::model()->findByPk($specification['cat_id']);
                            $category_name = $category->category_name;
                        }
                        $item = ucwords($category_name) . $brand . '-' . ucwords($specification['specification']);
                        echo $item;
                        ?>
                    </td>
                    <td>
                        <?php echo $value['warehousereceipt_effective_quantity']; ?>
                    </td>
                    <td class="text-right">
                        <?php echo $value['warehousereceipt_rate']; ?>
                    </td>
                    <td class="text-right">
                        <?php echo $value['warehousereceipt_effective_quantity'] * $value['warehousereceipt_rate']; ?>
                    </td>
                </tr>
                <?php
                $i++;
            }
            ?>
        </tbody>

    </table>
</div>