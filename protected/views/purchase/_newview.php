<div id="msg_box"></div>
<?php //echo "<pre>";print_r($data);
if ($index == 0) {


?>
	<thead class="entry-table">
		<tr>
			<?php
			if ((isset(Yii::app()->user->role) && ((in_array('/purchase/viewpurchase', Yii::app()->user->menuauthlist)) || (in_array('/purchase/updatepurchase', Yii::app()->user->menuauthlist))))) {
			?>
				<th></th>
			<?php } ?>
			<th>SI No</th>
			<th>Purchase No</th>
			<th>Company</th>
			<th>Project</th>
			<th>Expense Head</th>
			<th>Vendor</th>
			<th>Total Amount</th>
			<th>Date</th>
			<th>Purchase Type</th>
			<th>Status</th>
			<th>Expected Delivery Date</th>
			<th>Total amount billed</th>
			<th>Balance to be billed</th>
			<th>Billing Status</th>
			<th>Delivery Status</th>
			<th>MR Status</th>
			


		</tr>
	</thead>
<?php } ?>

<?php
$tblpx 	 = Yii::app()->db->tablePrefix;
$edit_request_model = Editrequest::model()->find(["condition" => "parent_id=" . $data->p_id . " AND editrequest_table='jp_purchase' AND editrequest_status =0"]);
$purchase_items = Yii::app()->db->createCommand("SELECT item_id, quantity, rate,tax_amount,discount_amount FROM {$tblpx}purchase_items WHERE purchase_id=" . $data->p_id)->queryAll();
$tax_value = 0;
$discount_value = 0;
foreach ($purchase_items as $p_items) {
	if (!empty($p_items['tax_amount'])) {
		$tax_value += $p_items['tax_amount'];
	}
	if (!empty($p_items['discount_amount'])) {
		$discount_value += $p_items['discount_amount'];
	}
}
if ($data->permission_status == 'No') {
	$permission_class = 'permission_style';
} elseif ($data->permission_status == 'Declined') {
	$permission_class = 'permission_style_decline';
} else {
	$permission_class = '';
}
$purchase_amount = $data->getPurchaseTotalAmount($data->p_id, null, $data->purchase_type);
?>

<tr class="<?php echo $permission_class; ?><?= !empty($edit_request_model) ? 'permission_style' : '' ?>">
	<?php
        $remark = Remark::model()->findAll(array("condition"=>"purchase_id=$data->p_id"));
        $remark_style ="<div>$data->purchase_no</div>";
		
        if(!empty($remark) || !empty($data->decline_comment)){
            $remark_style = "<div title='View Remarks' onclick='addremark($data->p_id,event)' id = 'addr' class=' addr cursor-pointer'>$data->purchase_no<span class=' icon icon-speech badge1'></span></div>"; 
        } 
        ?>
	<td>
		<?php
		$bill_count = Yii::app()->db->createCommand("SELECT count(*)  FROM `jp_bills` WHERE `purchase_id` =".$data->p_id)->queryScalar();
		?>
		<span class="icon icon-options-vertical popover-test" data-toggle="popover" data-placement="right" type="button" data-html="true" style="cursor: pointer;"></span>
		<div class="popover-content hide">
			<ul class="tooltip-hiden">
				<?php if ((in_array('/purchase/viewpurchase', Yii::app()->user->menuauthlist))) { ?>
					<li><button id="<?php echo $data->p_id; ?>" class="btn btn-xs btn-default view_purchase">View</button></li>
				<?php  } ?>

				<?php if ((in_array('/purchase/deletepurchaseorder', Yii::app()->user->menuauthlist))) {
					if ($data->purchase_status == 'draft'
					|| ($data->purchase_status == 'permission_needed' && Yii::app()->user->role == 1 && ($data->status['caption'] == "Pending to be Billed"  || $data->status->caption == 'PO not issued'))
					|| (Yii::app()->user->role == 1 && $data->purchase_status == 'saved' && ($data->status['caption'] == "Pending to be Billed"  || $data->status->caption == 'PO not issued'))) { ?>
						<li><button id="<?php echo $data->p_id; ?>" class="btn btn-xs btn-default delete_po delete<?php echo $data->p_id; ?>">Delete</button></li>
					<?php }
				} ?>

				<?php if ((in_array('/purchase/updatepurchase', Yii::app()->user->menuauthlist))) { ?>
					<li><button id="<?php echo $data->p_id; ?>" class="btn btn-xs btn-default edit_purchase edit_option_<?php echo $data->p_id; ?>" data-id="<?php echo $data->purchase_type; ?>" bill-count ="<?php echo $bill_count ?>">Edit</button></li>
				<?php } ?>

				<?php					
				if ($data->status['caption'] == 'Fully Billed' || $data->status['caption'] == 'Partially Billed') { ?>
					<li><button id="<?php echo $data->p_id; ?>" class="btn btn-xs btn-default delete_bill edit_option_<?php echo $data->p_id; ?>">View Bills</button></li>
				<?php } ?>

				<?php
				$approval_type =Yii::app()->db->createCommand("SELECT estimation_approval_type FROM {$tblpx}purchase_items WHERE purchase_id=" . $data->p_id)->queryRow();
				if ((in_array('/purchase/permission', Yii::app()->user->menuauthlist))) {
					if ($data->permission_status == 'No') { ?>
						<li><button id="<?php echo $data->p_id; ?>" class="btn btn-xs btn-default permission_item approveoption_<?php echo $data->p_id; ?>"><?php echo 'Approve PO'; ?></button></li>
					<?php }
				} ?>
						
				<li>
					<button  id="<?php echo $data->p_id; ?>" class="add_remark btn btn-xs btn-default" onclick="addremark(<?php echo $data->p_id; ?>,event)" title="Add Remark">Add Remark</button>
				</li>
			</ul>
		</div>
	</td>
	<td><?php echo $index + 1; ?></td>
	<td class="pur_remark"><?php echo $remark_style; ?></td>
	<td class="<?= !empty($edit_request_model) ? 'company_edit_admin_approval_class' : '' ?>">
		<?php
		$company = Company::model()->findByPk($data->company_id);
		echo $company['name']; ?>
	</td>
	<td><?php echo $data->project['name'];  ?></td>
	<td><?php echo $data->expensehead['type_name']; ?></td>
	<td><?php echo $data->vendor['name'];  ?></td>
	<td class="text-right"><?php echo Controller::money_format_inr($purchase_amount, 2, 1); ?></td>
	<td class="wrap"><?php echo  date("d-m-Y", strtotime($data->purchase_date)); ?></td>
	<td>
		<?php
		$purchaseType   = $data->purchase_type;
		if ($purchaseType == "A") {
			echo "By Length";
		} else if ($purchaseType == "G") {
			echo "By Width*Height";
		} else {
			echo "By Quantity";
		}
		?>
	</td>
	<?php
	$class = "";
	if ($data->purchase_status == 'permission_needed') {
		$class = "highlight";
	} else {
		$class = "";
	}
	$permission = PurchaseItems::model()->findAll(array("condition" => "purchase_id=$data->p_id AND permission_status=2"));
	if ($permission) {
		$class = "highlight";
	} else {
		$class = "";
	}
	?>


	<td class="<?php echo $class; ?>">
		<?php
		if ($data->purchase_status == 'saved') {
			echo "Saved";
		} else {
			$permission = PurchaseItems::model()->findAll(array("condition" => "purchase_id=$data->p_id AND permission_status=2"));
			if ($permission || $data->purchase_status == 'permission_needed') { 
			
				echo CHtml::link('Permission needed <span class="fa fa-thumbs-up"  style="color: #fff;"></span>', Yii::app()->createUrl('purchase/updatepurchase&pid='.$data->p_id),array('target'=>'_blank','title'=>'First give approval for item!','style'=>'color: #fff;'));

			 } else if ($data->permission_status == 'No') {
				//echo "<div class='approvallabel_".$data->p_id."'>Approval needed </div>";
				echo "<div class='purchase_status_" . $data->p_id . " approvallabel_" . $data->p_id . "' id='" . $data->p_id . "' data-toggle='tooltip'>Approval needed</div>";
			} else if ($data->permission_status == 'Declined') {
				echo 'Declined';
			} else {

				$style = '';
				$items = PurchaseItems::model()->findAll(array("condition" => "purchase_id=$data->p_id"));
				if ($data->purchase_status == 'draft' && !empty($items)) {
		?>
					<div style="cursor:pointer" class="update_status purchase_status_<?php echo $data->p_id; ?>" id="<?php echo $data->p_id; ?>" data-toggle="tooltip" title="Change status to saved">Draft<span class="fa fa-save"></span></div>
				<?php
				} else {
					echo "Draft";
				}

				?>
		<?php }
		} ?>
	</td>
	<td class="wrap"><?php echo ($data->expected_delivery_date !=NULL)?date("d-m-Y", strtotime($data->expected_delivery_date)):""; ?></td>

	<?php

	$value = Yii::app()->db->createCommand("SELECT SUM(bill_totalamount) as bill_totalamount,sum(bill_additionalcharge)  as bill_additionalcharge FROM `{$tblpx}bills` WHERE purchase_id=" . $data->p_id)->queryRow();
	if ($value['bill_totalamount'] != '') {
		$loop_billed_amount =  $value['bill_totalamount'];
	} else {
		$loop_billed_amount = '0.00';
	}
	?>
	<td class="text-right"><?php echo Controller::money_format_inr($loop_billed_amount, 2, 1); ?></td>


	<?php
	$tblpx 	 = Yii::app()->db->tablePrefix;
	$max = array();
	$blanc_amount = 0;
	$loop_blance_amount = 0;
	$tvalue = 0;
	if (!empty($purchase_items)) {
		foreach ($purchase_items as $values) {
			$bill_item = Yii::app()->db->createCommand("SELECT sum(billitem_quantity) as billitem_quantity FROM {$tblpx}billitem WHERE purchaseitem_id=" . $values['item_id'])->queryRow();
			if (!empty($bill_item)) {
				$blance_quantity = $values['quantity'] - $bill_item['billitem_quantity'];
				$max = $blance_quantity * $values['rate'];
				$blanc_amount += $max;
			} else {
				$blanc_amount += $values['quantity'] * $values['rate'];
			}
			// if (!empty($values['tax_amount'])) {
			// 	$blanc_amount += $values['tax_amount'];
			// }
			// if (!empty($values['discount_amount'])) {
			// 	$blanc_amount -= $values['discount_amount'];
			// }
		}

		if ($data->status['caption'] == "Pending to be Billed"  || $data->status->caption == 'PO not issued') {
			$loop_blance_amount = $purchase_amount;
		} elseif (($loop_billed_amount == $data->total_amount) || ($loop_billed_amount > $data->total_amount)) {
			$loop_blance_amount = '0.00';
		} else {
			$loop_blance_amount = $blanc_amount;
		}
	} else {
		$loop_blance_amount = '0.00';
	}

	?>
	<td class="text-right" class="<?php echo $loop_billed_amount ?> sdgsdg">
		<?php echo Yii::app()->Controller->money_format_inr($loop_blance_amount, 2, 1); ?>
	</td>

	<?php
	$color = '';
	$label = '';
	$lable_color = '';
	if ($data->status['caption'] == 'PO not issued') {
		$label = "PO not Issued";
		$color = '#DD1035';
		$lable_color = 'style="color:#fff;"';
	} else if ($data->status['caption'] == 'Fully Billed') {
		$label = "Fully Billed";
		$color = '#2AD300';
		$lable_color = "";
	} else if ($data->status['caption'] == 'Pending to be Billed') {
		$label = "Pending to be Billed";
		$color = '#FF6347';
		$lable_color = "";
	} else if ($data->status['caption'] == 'Partially Billed') {
		$label = "Partially Billed";
		$color = '#FFFF00';
		$lable_color = "";
	}

	?>

	<td class="bill_status" style="background:<?php echo $color; ?>">
		<span class="bill_label" <?php echo $lable_color; ?>>
			<?php echo $data->status['caption']; ?>
		</span>
	</td>
	<td>
		<?php echo $this->getDeliveryStatus($data->p_id) ?>
	</td>
	<td>
		<?php echo $this->getMRStatus($data->p_id) ?>
	</td>	
	


</tr>
<?php
if ($index == 0) {
?>
	<tfoot class="entry-table">
		<tr>
		
			<th colspan="12" class="text-right">Total:</th>
			<th class="text-right">
				<?php
				echo Yii::app()->Controller->money_format_inr($total_billedamount, 2, 1);
				?>
			</th>
			<th class="text-right">
				<?php echo Yii::app()->Controller->money_format_inr($total_blanceamount, 2, 1); ?>
			</th>
			<th></th>
			<th></th>
			<?php
			if ((isset(Yii::app()->user->role) && ((in_array('/purchase/viewpurchase', Yii::app()->user->menuauthlist)) || (in_array('/purchase/updatepurchase', Yii::app()->user->menuauthlist))))) {
			?>
				<th></th>
			<?php } ?>



		</tr>
	</tfoot>
<?php } ?>
