<div style="padding: 30px;">
<div class="form">
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'service-form',
        'enableAjaxValidation' => false,
        'method' => 'post',
        'htmlOptions' => array(
            'enctype' => 'multipart/form-data'
        )
    ));
    ?>
    <div style="float:right"><?= CHtml::link('Download Sample CSV', $url = $this->createAbsoluteUrl('ExpensesImport/downloaddoc')) ?></div>
    <fieldset>
        <!--<legend>
        <p class="note">Fields with <span class="required">*</span> are required.</p>
        </legend>-->
        <?php echo $form->errorSummary($model, 'Opps!!!', null, array('class' => 'alert alert-error ')); ?>
        
        <div class="control-group">
            <div>
                <div class="control-group <?php if ($model->hasErrors('postcode')) echo "error"; ?>"> <?php echo $form->labelEx($model, 'file'); ?> <?php echo $form->fileField($model, 'file'); ?> <?php echo $form->error($model, 'file'); ?> </div>
            </div>
        </div>
        <div class="form-actions"> <?php echo CHtml::submitButton('Upload', array('buttonType' => 'submit', 'type' => 'primary', 'icon' => 'ok white', 'label' => 'UPLOAD')); ?> </div>
    </fieldset>
    <?php $this->endWidget(); ?>
</div>
<!-- form -->

<h4><?php echo Yii::app()->user->getFlash('success'); ?></h4>
<h4><?php echo Yii::app()->user->getFlash('importstatus'); ?></h4>
<?php echo CHtml::ajaxLink("Save", $this->createUrl('ExpensesImport/save'), array(
       "type" => "post",
       "data" => 'js:{theIds : $.fn.yiiGridView.getChecked("expenses-import-grid","selectedIds").toString()}',
       "success" => 'js:function(data){ location.reload();  }' ),array(
       'class' => 'btn-sm btn-info'
       )
       ); ?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'expenses-import-grid',
    'dataProvider'=>$expmodel->search(),
    'itemsCssClass' => 'greytable',
    'columns'=>array(
         array(
            'id' => 'selectedIds',
            'name' => 'check',
            'value' => '$data->exp_id',
            'class' => 'CCheckBoxColumn',
            'selectableRows' => '50',
        ),
        'exp_id',
        'projectid',
        'userid',
        'exp_date',
        'amount',
        'description',
        'type',
        'exptype',
        'vendor_id',
        'payment_type',
        'works_done',
        'materials',
       /* 'created_by',
        'created_date',
        'updated_by',
        'updated_date',*/
        'purchase_type',
        'paid',
        'import_status',
        
        array(
            'class' => 'CButtonColumn',
                    'header' => 'Action',
                    'htmlOptions' => array('style' => 'width:100px;'),
                    'template' => '{view}{update}',
        ),
    ),
)); ?>
    </div>