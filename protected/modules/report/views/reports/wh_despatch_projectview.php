<div class="table-responsive">
    <table cellpadding="10" class="table  list-view " id="fixtable">
        <thead class="entry-table">
            <tr>
                <th>SL No</th>
                <th>Despatch No</th>
                <th>Item Description</th>
                <th>Qty in Nos</th>
                <th>Rate</th>
                <th>Amount</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td colspan="6">
                    <?php
                    $projectData = Projects::model()->findByPk($project);

                    echo $projectData['name'];
                    ?>
                </td>
            </tr>
            <?php
            $sql = "SELECT * FROM `jp_warehousedespatch_items` i,jp_warehousedespatch d "
                . " LEFT JOIN  jp_warehouse w "
                . " ON w.warehouse_id = d.warehousedespatch_warehouseid_to"
                . " WHERE i.warehousedespatch_id=d.warehousedespatch_id "
                . " AND w.project_id =" . $project;
            $despatch_items = Yii::app()->db->createCommand($sql)->queryAll();
            $i = 1;
            foreach ($despatch_items as $key => $value) {
                ?>
                <tr>
                    <td>
                        <?php echo $i; ?>
                    </td>
                    <td>
                        <?php
                        $warehouse_despatch = warehousedespatch::model()->findByPk($value['warehousedespatch_id']);
                        echo $warehouse_despatch->warehousedespatch_no;
                        ?>
                    </td>
                    <td>
                        <?php
                        $tblpx = Yii::app()->db->tablePrefix;
                        $spec_sql = "SELECT id, specification, brand_id, cat_id "
                            . " FROM {$tblpx}specification "
                            . " WHERE id=" . $value['warehousedespatch_itemid'];
                        $specification = Yii::app()->db->createCommand($spec_sql)->queryRow();

                        $brand = '';
                        if ($specification['brand_id'] != NULL) {
                            $sql = "SELECT brand_name FROM {$tblpx}brand "
                                . " WHERE id=" . $specification['brand_id'];
                            $brand_details = Yii::app()->db->createCommand($sql)->queryRow();
                            $brand = '-' . ucwords($brand_details['brand_name']);
                        }
                        $category_name = "";
                        if ($specification['cat_id'] != "") {
                            $category = PurchaseCategory::model()->findByPk($specification['cat_id']);
                            $category_name = $category->category_name;
                        }
                        $item = ucwords($category_name) . $brand . '-' . ucwords($specification['specification']);
                        echo $item;
                        ?>
                    </td>
                    <td>
                        <?php echo $value['warehousedespatch_quantity']; ?>
                    </td>
                    <td>
                        <?php echo $value['warehousedespatch_rate']; ?>
                    </td>
                    <td>
                        <?php echo $value['warehousedespatch_rate'] * $value['warehousedespatch_quantity']; ?>
                    </td>
                </tr>
                <?php
                $i++;
            }
            ?>
        </tbody>
    </table>
</div>