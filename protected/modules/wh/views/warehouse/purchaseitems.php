<?php
/* @var $this BillsController */
/* @var $model Bills */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<style type="text/css">
    tr.pitems td input, tr.pitems td div.pricediv {
        text-align: right!important;
    }
</style>
<div class="row">
    <div class="col-sm-12 table-responsive">
        <table class="table">
            <thead class="thead-inverse">
                <tr>
                    <th>Specifications/Remark</th>
                    <th>Rejected Quantity</th>
                    <th>Billed Quantity</th>
                    <th>Remaining Quantity</th>
                    <th>Defect Return Quantity</th>
                    <th>Unit</th>
                    <th>Rate</th>
                    <th>Amount</th>                    
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php
                $number = 1;
                $amount = 0;
                $totalQty = 0;
                $bTotalQty = 0;
                foreach ($purchase as $item) {
                    if ($item['warehousereceipt_itemid']) {
                        $categorymodel = Specification::model()->findByPk($item['warehousereceipt_itemid']);
                        if ($categorymodel)
                            $categoryName = $categorymodel->specification;
                        else
                            $categoryName = "Not available";
                    } else {
                        $categoryName = $item['remark'];
                    }
                    $purchasemodel = WarehousereceiptItems::model()->findByPk($item['item_id']);
                    $itemQuantity = $purchasemodel->warehousereceipt_baseunit_rejected_effective_quantity;
                    $tblpx = Yii::app()->db->tablePrefix;
                    $itemData = Yii::app()->db->createCommand("SELECT SUM(returnitem_quantity) as quantitysum FROM " . $tblpx . "defect_returnitem WHERE receiptitem_id = " . $item['item_id'])->queryRow();
                    $bItemQuantity = $itemData['quantitysum'] ? $itemData['quantitysum'] : 0;
                    if (strpos($bItemQuantity, ".") !== false) {
                        $bItemQuantity = number_format((float) $bItemQuantity, 2, '.', '');
                    } else {
                        $bItemQuantity = $bItemQuantity;
                    }
                    $quantityAvail = $itemQuantity - $bItemQuantity;
                    $totalQty = $totalQty + $itemQuantity;
                    $bTotalQty = $bTotalQty + $bItemQuantity;
                    ?>

                    <tr class="pitems">
                <input type="hidden" id="ids<?php echo $number - 1; ?>" value ="<?php echo $item["item_id"]; ?>" class="txtBox pastweek ids" name="ids[]"/>
                <input type="hidden" id="category<?php echo $number - 1; ?>" value ="<?php echo $item['warehousereceipt_itemid'] ? $item['warehousereceipt_itemid'] : ''; ?>" class="txtBox pastweek category" name="category[]"/>

                <td id="<?php echo $number - 1; ?>"><div id="bicategoryname<?php echo $number - 1; ?>"><?php echo $categoryName; ?></div></td>
                <input type="hidden" id="availablequantity<?php echo $number - 1; ?>" value ="<?php echo $quantityAvail; ?>" name="availablequantity[]"/>
                <input type="hidden" id="orrate<?php echo $number - 1; ?>" value ="<?php echo number_format((float) $item["warehousereceipt_rate"], 2, '.', ''); ?>" name="orrate[]"/>
                <td id="<?php echo $number - 1; ?>"><div id="actualquantity<?php echo $number - 1; ?>"><?php echo $itemQuantity; ?></div></td>
                <td id="<?php echo $number - 1; ?>"><div id="billedquantity<?php echo $number - 1; ?>"><?php echo $bItemQuantity; ?></div></td>
                <td id="<?php echo $number - 1; ?>"><div id="biremquantity<?php echo $number - 1; ?>"><?php echo $quantityAvail; ?></div></td>
                <td id="<?php echo $number - 1; ?>"><input type="text" id="biquantity<?php echo $number - 1; ?>" class="form-control input fnext validate[required,custom[number],min[0],max[<?php echo $quantityAvail; ?>]]" value ="0" name="biquantity[]"<?php if ($quantityAvail == 0) { ?> readonly="true" <?php } ?>/></td>
                <td id="<?php echo $number - 1; ?>"><div id="biunit<?php echo $number - 1; ?>"><?php echo $item["warehousereceipt_unit"]; ?></div></td>
                <td id="<?php echo $number - 1; ?>"><input type="text" id="birate<?php echo $number - 1; ?>" class="form-control lnext validate[required,custom[number],min[0]]"  value ="<?php echo number_format((float) $item["warehousereceipt_rate"], 2, '.', ''); ?>" name="birate[]"<?php if ($quantityAvail == 0) { ?> readonly="true" <?php } ?>/></td>
                <td id="<?php echo $number - 1; ?>"><div id="biamount<?php echo $number - 1; ?>" class="pricediv">0</div></td>                    
                <input type="hidden" id="billitem<?php echo $number - 1; ?>" value ="" name="billitem[]"/>
                <input type="hidden" id="savedquantity<?php echo $number - 1; ?>" value ="" name="savedquantity[]"/>
                <td><div id="tickmark<?php echo $number - 1; ?>"></div></td>
                </tr>
                <?php
                $amount = $amount + ($item["warehousereceipt_rate"] * $item['warehousereceipt_baseunit_rejected_effective_quantity']);
                $number = $number + 1;
            }
            ?>
            <input type="hidden" name="tqty[]" id="tqty" value="<?php echo $totalQty; ?>"/>
            <input type="hidden" name="btqty[]" id="btqty" value="<?php echo $bTotalQty; ?>"/>
            <input type="hidden" id="totrows" value ="<?php echo $number - 1; ?>" class="txtBox pastweek totrows" name="totrows"/>
            </tbody>
        </table>

    </div>
</div>
