<?php
/* @var $this BuyersController */
/* @var $model Buyers */
/* @var $form CActiveForm */


?>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.js"></script>
<div class="">

	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'buyers-form',
		'enableAjaxValidation' => true,
		'enableClientValidation' => true,
		'clientOptions' => array(
			'validateOnSubmit' => true,
			'validateOnChange' => true,
			'validateOnType' => false,
		),
	)); ?>

	<div class="panel-body">
		<div class="row addRow">
			<div class="col-md-6">
				<?php echo $form->labelEx($model, 'name'); ?>
				<?php echo $form->textField($model, 'name', array('size' => 66, 'maxlength' => 100, 'class' => 'form-control')); ?>
				<?php echo $form->error($model, 'name'); ?>
			</div>
			<div class="col-md-6">
				<?php echo $form->labelEx($model, 'buyer_type'); ?>
				<?php echo $form->dropDownList($model, 'buyer_type', CHtml::listData(ProjectType::model()->findAll(array('order' => 'project_type ASC')), 'ptid', 'project_type'), array('class' => 'form-control', 'empty' => '--')); ?>
				<?php echo $form->error($model, 'buyer_type'); ?>
			</div>
		</div>
		<div class="row addRow">
			<div class="col-md-6">
				<?php echo $form->labelEx($model, 'phone'); ?>
				<?php echo $form->textField($model, 'phone', array('class' => 'form-control')); ?>
				<?php echo $form->error($model, 'phone'); ?>
			</div>
			<div class="col-md-6">
				<?php echo $form->labelEx($model, 'address'); ?>
				<?php echo $form->textArea($model, 'address', array('rows' => 6, 'cols' => 50, 'class' => 'form-control')); ?>
				<?php echo $form->error($model, 'address'); ?>
			</div>
		</div>
		<div class="row addRow">
			<div class="col-md-6">
				<?php echo $form->labelEx($model, 'email_id'); ?>
				<?php echo $form->textField($model, 'email_id', array('class' => 'form-control')); ?>
				<?php echo $form->error($model, 'email_id'); ?>
			</div>
		</div>

		<?php if (!$model->isNewRecord) {
			if ($model->buyer_type == 2) {
		?>
				<div class="row addRow">
					<div class="col-md-6 hidbox">
						<label for="Buyers_contact_person">Contact Person</label>
						<span class="required">*</span>
						<?php echo $form->textField($model, 'contact_person', array('class' => 'form-control')); ?>
						<?php echo $form->error($model, 'contact_person'); ?>
					</div>

					<div class="col-md-6 hidbox">
						<label for="Buyers_local_address">Local Address</label>
						<span class="required">*</span>
						<?php echo $form->textArea($model, 'local_address', array('class' => 'form-control')); ?>
						<?php echo $form->error($model, 'local_address'); ?>
					</div>
				</div>

			<?php
			} else {
			?>
				<div class="row addRow">
					<div class="col-md-6 hidbox" style="display: none">
						<label for="Buyers_contact_person">Contact Person</label>
						<span class="required">*</span>
						<?php echo $form->textField($model, 'contact_person', array('class' => 'form-control')); ?>
						<?php echo $form->error($model, 'contact_person'); ?>
					</div>
					<div class="col-md-6 hidbox" style="display: none">
						<label for="Buyers_local_address">Local Address</label>
						<span class="required">*</span>
						<?php echo $form->textArea($model, 'local_address', array('class' => 'form-control')); ?>
						<?php echo $form->error($model, 'local_address'); ?>
					</div>
				</div>
			<?php
			}
		} else { ?>
			<div class="row addRow">
				<div class="col-md-6 hidbox" style="display: none">
					<label for="Buyers_contact_person">Contact Person</label>
					<span class="required">*</span>
					<?php echo $form->textField($model, 'contact_person', array('class' => 'form-control')); ?>
					<?php echo $form->error($model, 'contact_person'); ?>
				</div>


				<div class="col-md-6 hidbox" style="display: none">
					<label for="Buyers_local_address">Local Address</label>
					<span class="required">*</span>
					<?php echo $form->textArea($model, 'local_address', array('class' => 'form-control')); ?>
					<?php echo $form->error($model, 'local_address'); ?>
				</div>
			</div>

		<?php } ?>
		<div class="row addRow">
			<div class="col-md-6">
				<?php echo $form->labelEx($model, 'gst_no'); ?>
				<?php echo $form->textField($model, 'gst_no', array('class' => 'form-control')); ?>
				<?php echo $form->error($model, 'gst_no'); ?>
			</div>
			<div class="col-md-6">
				<?php echo $form->labelEx($model, 'status'); ?>
				<div class="radio_btn">
					<?php
					echo $form->radioButtonList($model, 'status', CHtml::listData(Status::model()->findAll(
						array(
							'select' => array('sid,caption'),
							'condition' => 'status_type="active_status"',
							'order' => 'caption',
							'distinct' => true
						)
					), 'sid', 'caption'), array('separator' => '', 'display' => 'inline-block'));
					?>
				</div>
				<?php echo $form->error($model, 'status'); ?>
			</div>
		</div>
		<div class="row addRow">
			<div class="col-md-6">
				<?php echo $form->labelEx($model, 'company');
				?>

				<ul class="checkboxList">
					<?php
					$user = Users::model()->findByPk(Yii::app()->user->id);
					$arrVal = explode(',', $user->company_id);
					$newQuery = "";
					foreach ($arrVal as $arr) {
						if ($newQuery) $newQuery .= ' OR';
						$newQuery .= " FIND_IN_SET('" . $arr . "', id)";
					}
					$typelist = Company::model()->findAll(array('condition' => $newQuery));
					$assigned_company_array = array();
					if (!$model->isNewRecord) {
						$assigned_types = Buyers::model()->find(array('condition' => 'id=' . $model->id));
						$assigned_company_array = explode(",", $assigned_types->company);
						if ((isset(Yii::app()->user->role) && (in_array('/clients/companypermission', Yii::app()->user->menuauthlist)))) {
							$readonly = "";
						} else {
							$readonly = "readonly";
						}
					} else {
						$assigned_company_array = "";
						$readonly = "";
					}
					echo CHtml::checkBoxList('Buyers[company]', $assigned_company_array, CHtml::listData($typelist, 'id', 'name'), array('checkAll' => 'Check all', 'template' => '<li class="checkboxtype">{input}{label}</li>', 'separator' => '', 'readonly' => $readonly, 'class' => 'companyCheckbox'));
					?>
				</ul>
				<?php echo $form->error($model, 'company'); ?>
			</div>
		</div>
		<div class="row addRow">
			<div class="col-md-6">
				<?php echo $form->labelEx($model, 'project'); ?>
				<?php echo $form->dropDownList($model, 'project', $projects, array('class' => 'form-control', 'empty' => '--')); ?>
				<?php echo $form->error($model, 'project'); ?>
			</div>
			<div class="col-md-6">
				<?php echo $form->labelEx($model, 'flat_numbers'); ?>
				<input type="hidden" id="selected_flats" value="<?= $model->flat_numbers ?>">
				<?php
				echo $form->dropDownList($model, 'flat_numbers', $flat_numbers, array('class' => 'form-control js-example-basic-multiple', 'empty' => '--', 'multiple' => 'multiple'));

				?>
				<?php echo $form->error($model, 'flat_numbers'); ?>
			</div>
		</div>
		<div class="row addRow">
			<div class="col-md-12">
				<?php echo $form->labelEx($model, 'description'); ?>
				<?php echo $form->textArea($model, 'description', array('class' => 'form-control', 'rows' => 6, 'cols' => 50)); ?>
				<?php echo $form->error($model, 'description'); ?>
			</div>
		</div>
	</div>

	<div class="panel-footer save-btnHold text-center">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-info')); ?>
		<?php if (Yii::app()->user->role == 1) {
			if (!$model->isNewRecord) {
			}
		} ?>

		<?php if (!$model->isNewRecord) {
			echo CHtml::ResetButton('Close', array('onclick' => 'closeaction(this,event);', 'class' => 'btn'));
		} else {
			echo CHtml::ResetButton('Reset', array('style' => 'margin-right:3px;', 'class' => 'btn btn-default'));
			echo CHtml::ResetButton('Close', array('onclick' => 'closeaction(this,event);', 'class' => 'btn'));
		}
		?>
	</div>

	<?php $this->endWidget(); ?>

</div><!-- form -->


<style>
	.addRow label {
		display: inline-block;
	}

	input[type="radio"] {
		margin: 4px 4px 0;
	}

	input[type="checkbox"][readonly] {
		pointer-events: none;
	}

	.select2 select2-container select2-container--default {
		width: 200px !important;
	}
</style>
<script>
	$(document).ready(function() {
		var selected_flats = $('#selected_flats').val();

		if (selected_flats !== "") {
			var array = selected_flats.split(',');
			console.log(array);
			$('#Buyers_flat_numbers').val(array);
			$('#Buyers_flat_numbers').trigger('change');
		}
		$('.js-example-basic-multiple').select2({
			width: '100%',
			placeholder: "--",
		});
	});
	$(document).on("change", "#Buyers_buyer_type", function() {

		var val = $("#Buyers_buyer_type option:selected").text();
		if (val == 'NRI') {
			$('.hidbox').show();
		} else {
			$('.hidbox').hide();
		}

	});
	$('.companyCheckbox').change(function() {
		var company_ids = [];
		$('input:checkbox.companyCheckbox:checked').each(function() {
			company_ids.push($(this).attr('value'));
		});
		if (company_ids.length !== 0) {
			$.ajax({
				type: "POST",
				data: {
					company_ids: company_ids
				},
				dataType: "json",
				url: "<?php echo $this->createUrl('/buyer/buyers/getProjects') ?>",
				success: function(response) {
					$("#Buyers_project").html(response.project);

				}
			});
		}
	});
	$('#Buyers_project').change(function() {
		var project_id = this.value;
		if (project_id !== '') {
			$.ajax({
				type: "POST",
				data: {
					project_id: project_id
				},
				dataType: "json",
				url: "<?php echo $this->createUrl('/buyer/buyers/getFlatNumbers') ?>",
				success: function(response) {
					$('#Buyers_flat_numbers').empty();
					$("#Buyers_flat_numbers").html(response.flats);

				}
			});
		}
	});
</script>