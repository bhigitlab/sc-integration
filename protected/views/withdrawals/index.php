<?php
/* @var $this WithdrawalsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Withdrawals',
);

$this->menu=array(
	array('label'=>'Create Withdrawals', 'url'=>array('create')),
	array('label'=>'Manage Withdrawals', 'url'=>array('admin')),
);
?>

<h1>Withdrawals</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
