<?php

class InvoiceController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout = '//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		/* return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('view','getclientByProject','admin'),
				'users' => array('@'),
                 'expression' => 'yii::app()->user->role <=2',
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','getclientByProject','viewinvoice','deleteinvdetails','deleteperfdetails','viewperforma','saveinvoice','performainvoice','saveperformainvoice','exportinvoice','exportperforma','addperforma','updateperforma','addinvoice','updateinvoice','testmail','createnewinvoice','test','invoiceitem','updatesinvoiceitem','removeinvoiceitem', 'getClient', 'Ajaxdate'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		); */

		$accessArr = array();
		$accessauthArr = array();
		$controller = Yii::app()->controller->id;
		if (isset(Yii::app()->user->menuall)) {
			if (array_key_exists($controller, Yii::app()->user->menuall)) {
				$accessArr = Yii::app()->user->menuall[$controller];
			}
		}

		if (isset(Yii::app()->user->menuauth)) {
			if (array_key_exists($controller, Yii::app()->user->menuauth)) {
				$accessauthArr = Yii::app()->user->menuauth[$controller];
			}
		}
		$access_privlg = count($accessauthArr);
		return array(
			array(
				'allow', // allow all users to perform 'index' and 'view' actions
				'actions' => $accessArr,
				'users' => array('@'),
				'expression' => "$access_privlg > 0",
			),

			array(
				'allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions' => $accessauthArr,
				'users' => array('@'),
				'expression' => "$access_privlg > 0",
			),
			array(
                'allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('UpdateinvoiceDate','updateInvoiceamount','mailtest','GetRateCard','addadditionalcharge','deleteadditionalcharge','getInvoiceNo'),
                'users' => array('*'),
            ),
			array(
				'deny',  // deny all users
				'users' => array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view', array(
			'model' => $this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		/*---------------------Mod by Rajisha---------------------*/
		$model = new Invoice;
		$client = '';
		$tblpx = Yii::app()->db->tablePrefix;
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if (isset($_POST['Invoice'])) {
			$model->attributes = $_POST['Invoice'];
			$model->date = date('Y-m-d', strtotime($_POST['Invoice']['date']));
			$model->created_date = date('Y-m-d');
			$model->created_by = Yii::app()->user->id;
			if ($model->save()) {
				$tot = Yii::app()->db->createCommand()
					->select('sum(amount) as billamount')
					->from($tblpx . 'invoice')
					->where('project_id = ' . $_POST['Invoice']['project_id'])
					->queryRow();
				$modelp = Projects::model()->findByPk($_POST['Invoice']['project_id']);
				$modelp->billed_to_client = $tot['billamount'];
				$modelp->save();
				Yii::app()->user->setFlash('success', "Invoive generated successfully!");
				$this->redirect(array('create'));
			}
		}
		/*---------------------Mod by Rajisha---------------------*/

		$this->render('create', array(
			'model' => $model, 'client' => $client
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model = $this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		$client = '';
		$tblpx = Yii::app()->db->tablePrefix;
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if (isset($_POST['Invoice'])) {
			$model->attributes = $_POST['Invoice'];
			$model->date = date('Y-m-d', strtotime($_POST['Invoice']['date']));
			if ($model->save()) {
				$tot = Yii::app()->db->createCommand()
					->select('sum(amount) as billamount')
					->from($tblpx . 'invoice')
					->where('project_id = ' . $_POST['Invoice']['project_id'])
					->queryRow();
				$modelp = Projects::model()->findByPk($_POST['Invoice']['project_id']);
				$modelp->billed_to_client = $tot['billamount'];
				$modelp->save();
				Yii::app()->user->setFlash('success', "Invoive updated successfully!");
				$this->redirect(array('update', 'id' => $id));
			}
		}
		/*---------------------Mod by Rajisha---------------------*/

		$this->render('update', array(
			'model' => $model, 'client' => $this->clientByProject($model->project_id)
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete()
	{
        $model = $this->loadModel($_POST['id']);
		$payment_stage = PaymentStage::model()->findByPk($model->stage_id);
        $tblpx = Yii::app()->db->tablePrefix;
		$transaction = Yii::app()->db->beginTransaction();
        try {
			if (Yii::app()->user->role != 1) {
				$sql = "SELECT count(*) FROM `jp_expenses` WHERE `invoice_id` =".$_POST['id'];
				$inv_payment = Yii::app()->db->createCommand($sql)->queryScalar();
				if($inv_payment>0){
					if (!$model->delete()) {
						$success_status = 0;
						throw new Exception(json_encode($model->getErrors()));
					}
				}
				$deletedata = new Deletepending;				
				$deletedata->deletepending_table    = $tblpx . "invoice";
				$deletedata->deletepending_data     = json_encode($model->attributes);
				$deletedata->deletepending_parentid = $_POST['id'];
				$deletedata->user_id                = Yii::app()->user->id;
				$deletedata->requested_date         = date("Y-m-d");
				if ($deletedata->save()) {
					if(is_null($model->amount)){
                        $model->amount="0.00";
                    }
					$model->delete_approve_status=1;
					if (!$model->save()) {
						$success_status = 0;
						throw new Exception(json_encode($model->getErrors()));
					} else {
						$success_status = 2;					
					}
				} else {
					$success_status = 0;
					throw new Exception(json_encode($deletedata->getErrors()));
				}
			}else{

				if (!$model->delete()) {
					$success_status = 0;
					throw new Exception(json_encode($model->getErrors()));
				} else {
					$newmodel = new JpLog('search');
                    $newmodel->log_data = json_encode($model->attributes);
                    $newmodel->log_action = 2;
                    $newmodel->log_table = $tblpx . "invoice";
                    $newmodel->log_datetime = date('Y-m-d H:i:s');
                    $newmodel->log_action_by = Yii::app()->user->id;
                    $newmodel->company_id = $model->company_id;
                    if ($newmodel->save()) {
					}
					$success_status = 1;

					if(!empty($payment_stage)){
						$payment_stage->invoice_status = "0";
						if (!$payment_stage->save()) {
							$success_status = 0;
							throw new Exception(json_encode($payment_stage->getErrors()));
						} else {
							$success_status = 1;					
						}
					}					
				}
			}

            $transaction->commit();
        } catch (Exception $error) {
            $transaction->rollBack();
            $success_status = 0;
        } finally {
            if ($success_status == 1) {
                echo json_encode(array('response' => 'success', 'msg' => 'Invoice Deleted Successfully'));
				
            }else if ($success_status == 2) {
				echo json_encode(array('response' => 'success', 'msg' => 'Invoice Delete Request Sent Successfully'));
			} else {
                if (isset($error->errorInfo) && ($error->errorInfo[1] == 1451)) {
                    echo json_encode(array('response' => 'warning', 'msg' => 'Cannot Delete ! This record alredy in use.'));
                } else {
                    echo json_encode(array('response' => 'error', 'msg' => 'Some Error Occured'));
                }
            }
        }
    }

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider = new CActiveDataProvider('Invoice');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model = new Invoice('search');
		$model->unsetAttributes();  // clear any default values
		if (isset($_GET['Invoice'])) {
			//print_r($_GET['Invoice']); die;
			$model->attributes = $_GET['Invoice'];
			$model->fromdate = $_GET['Invoice']['fromdate'];
			$model->todate = $_GET['Invoice']['todate'];
		}

		$this->render('newlist', array(
			'model' => $model,
			'dataProvider' => $model->search(),
		));
	}


	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Invoice the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model = Invoice::model()->findByPk($id);
		if ($model === null)
			throw new CHttpException(404, 'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Invoice $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'invoice-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	/*---------------------Mod by Rajisha---------------------*/
	public function actionGetclientByProject()
	{
		$id = $_POST['id'];
		$client = "";
		$tblpx = Yii::app()->db->tablePrefix;
		$client = Yii::app()->db->createCommand()
			->select('c.name')
			->from($tblpx . 'clients c')
			->join($tblpx . 'projects p', 'c.cid=p.client_id')
			->where('p.pid=:id', array(':id' => $id))
			->queryRow();
		echo $client['name'];
	}
	public function clientByProject($id)
	{
		$client = "";
		$tblpx = Yii::app()->db->tablePrefix;
		$client = Yii::app()->db->createCommand()
			->select('c.name')
			->from($tblpx . 'clients c')
			->join($tblpx . 'projects p', 'c.cid=p.client_id')
			->where('p.pid=:id', array(':id' => $id))
			->queryRow();
		return $client['name'];
	}
	/*---------------------Mod by Rajisha---------------------*/




	public function actionPerformainvoice()
	{

		$model = new PerformaInvoice('search');
		$model->unsetAttributes();  // clear any default values
		if (isset($_GET['PerformaInvoice']))
			$model->attributes = $_GET['PerformaInvoice'];

		$this->render('perf_list', array(
			'model' => $model,
			'dataProvider' => $model->search(),
		));

		//$this->render('performainvoice');


	}

	public function actionAddinvoice()
	{

		$model = new Invoice;
		$newmodel = new InvList;
		$tblpx      = Yii::app()->db->tablePrefix;


		$invoice_no = $this->getInvoiceNumber();
		$user = Users::model()->findByPk(Yii::app()->user->id);
		$arrVal = explode(',', $user->company_id);
		$newQuery = "";
		$newQuery1 = "";
		foreach ($arrVal as $arr) {
			if ($newQuery) $newQuery .= ' OR';
			if ($newQuery1) $newQuery1 .= ' OR';
			$newQuery .= " FIND_IN_SET('" . $arr . "', company_id)";
			$newQuery1 .= " FIND_IN_SET('" . $arr . "', p.company_id)";
		}
		if (Yii::app()->user->role == 1 || Yii::app()->user->role == 2) {
			$project    = Yii::app()->db->createCommand("SELECT pid, name  FROM {$tblpx}projects WHERE " . $newQuery . " ORDER BY name")->queryAll();
		} else {
			$project    = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}projects p JOIN ' . $tblpx . 'project_assign pa ON p.pid = pa.projectid WHERE " . $newQuery1 . " AND pa.userid = '" . Yii::app()->user->id . "'")->queryAll();
		}
		$client     = Yii::app()->db->createCommand("SELECT DISTINCT cid, name FROM {$tblpx}clients ORDER BY name")->queryAll();
		$this->render('invoice', array(
			'model' => $model,
			'newmodel' => $newmodel,
			'project' => $project,
			'client' => $client,
			'invoice_no' => $invoice_no

		));
	}

	function countDigit($n)
	{
		$count = 0;
		while ($n != 0) {
			$n = round($n / 10);
			++$count;
		}
		return $count;
	}

	public function actionCreatenewinvoice()
	{
		$invoice_id = $_REQUEST['invoice_id'];
		if (isset($_REQUEST['inv_no']) && isset($_REQUEST['default_date']) && isset($_REQUEST['client']) && isset($_REQUEST['project'])) {
			if ($invoice_id == 0) {
				$tblpx = Yii::app()->db->tablePrefix;
				$res = Invoice::model()->findAll(array('condition' => 'inv_no = "' . $_REQUEST['inv_no'] . '"'));
				if (empty($res)) {
					$model = new Invoice;
					$model->inv_no  = $_REQUEST['inv_no'];
					$model->stage_id  = isset($_REQUEST['stageId'])?$_REQUEST['stageId']:"";
					$model->project_id  = $_REQUEST['project'];
					$project_model = Projects::model()->findByPk($_REQUEST['project']);
					if (!empty($project_model)) {
						$client_id = $project_model['client_id'];
					} else {
						$client_id = $_REQUEST['client'];
					}
					$model->client_id  = $client_id;
					$model->date  = date('Y-m-d', strtotime($_REQUEST['default_date']));
					$model->created_date = date('Y-m-d');
					$model->created_by   = Yii::app()->user->id;
					$model->company_id   = $_REQUEST['company'];
					$model->type   = isset($_REQUEST['type']) ? $_REQUEST['type'] : 'quantity_rate';
					$model->invoice_status  = 'draft';
					if($model->save(false)){
						if($model->stage_id !=""){
							$payment_stage = PaymentStage::model()->findByPk($model->stage_id);
							$payment_stage->invoice_status = "1";
                        	$payment_stage->save();
						}
						
						$last_id = $model->invoice_id;
						echo json_encode(array('response' => 'success', 'msg' => 'Sales book added successfully', 'invoice_id' => $last_id));
					}else{
						echo json_encode(array('response' => 'error', 'msg' => 'An Error Occured'));
					}
					
				} else {
					echo json_encode(array('response' => 'error', 'msg' => 'Sales book number already exist'));
				}
			} else {
				$model = Invoice::model()->findByPk($invoice_id);
				$model->inv_no  = $_REQUEST['inv_no'];
				$model->project_id  = $_REQUEST['project'];
				$model->client_id  = $_REQUEST['client'];
				$model->date  = date('Y-m-d', strtotime($_REQUEST['default_date']));
				$model->created_date = date('Y-m-d');
				$model->company_id   = Yii::app()->user->company_id;
				$model->created_by   = $_REQUEST['company'];
				if ($model->save(false)) {
					echo json_encode(array('response' => 'success', 'msg' => 'Sales book updated successfully', 'invoice_id' => $invoice_id));
				} else {
					echo json_encode(array('response' => 'error', 'msg' => 'Please enter all details'));
				}
			}
		}
	}

	public function actionAjaxdate()
	{
		echo json_encode('success');
	}

	public function actionInvoiceitem()
	{
		$data = $_REQUEST['data'];

		$result = '';
		// $result['html'] = '';
		$html = '';
		if ($data['invoice_id'] == 0) {
			echo json_encode(array('response' => 'error', 'msg' => 'Please enter sales book details'));
			//$result['result'] .= array('response' => 'error', 'msg' => 'Please enter purchase details');
		} else {
			$subtot = $data['subtot'];
			$grand = $data['grand'];
			$grand_tax = $data['grand_tax'];
			if (isset($data['sl_No'])) {
				$sl_No = $data['sl_No'];
			} else {
				$sl_No = '';
			}
			if (isset($data['description'])) {
				$description = $data['description'];
			} else {
				$description = '';
			}

			if (isset($data['hsn_code'])) {
				$hsn_code = $data['hsn_code'];
			} else {
				$hsn_code = '';
			}

			if (isset($data['quantity'])) {
				$quantity = $data['quantity'];
			} else {
				$quantity  = '';
			}
			if (isset($data['unit'])) {
				$unit = $data['unit'];
			} else {
				$unit  = '';
			}
			if (isset($data['rate'])) {
				$rate = $data['rate'];
			} else {
				$rate  = '';
			}
			if (isset($data['amount'])) {
				$amount = $data['amount'];
			} else {
				$amount  = '';
			}

			if (isset($data['sgst'])) {
				$sgst = $data['sgst'];
			} else {
				$sgst  = '';
			}

			if (isset($data['sgst_amount'])) {
				$sgst_amount = $data['sgst_amount'];
			} else {
				$sgst_amount  = 0.00;
			}

			if (isset($data['cgst'])) {
				$cgst = $data['cgst'];
			} else {
				$cgst  = '';
			}

			if (isset($data['cgst_amount'])) {
				$cgst_amount = $data['cgst_amount'];
			} else {
				$cgst_amount  = 0.00;
			}

			if (isset($data['igst'])) {
				$igst = $data['igst'];
			} else {
				$igst  = '';
			}

			if (isset($data['igst_amount'])) {
				$igst_amount = $data['igst_amount'];
			} else {
				$igst_amount  = 0.00;
			}

			if (isset($data['tax_amount'])) {
				$tax_amount = $data['tax_amount'];
			} else {
				$tax_amount  = '';
			}

			$item_model = new InvList();
			$item_model->type = $data['type'];
			$item_model->inv_id = $data['invoice_id'];
			$item_model->quantity = $quantity;
			$item_model->unit = $unit;
			$item_model->rate = $rate;
			$item_model->amount = $amount;
			$item_model->description = $description;
			$item_model->hsn_code = $hsn_code;
			$item_model->cgst = $cgst;
			$item_model->cgst_amount = $cgst_amount;
			$item_model->sgst = $sgst;
			$item_model->sgst_amount = $sgst_amount;
			$item_model->igst = $igst;
			$item_model->igst_amount = $igst_amount;
			$item_model->tax_amount = $tax_amount;
			$item_model->created_date = date('Y-m-d');
			if ($item_model->save()) {
				$last_id = $item_model->id;
				$tblpx = Yii::app()->db->tablePrefix;
				$data1 = Yii::app()->db->createCommand("SELECT SUM(amount) as qt_amount FROM {$tblpx}inv_list WHERE inv_id=" . $data['invoice_id'] . "")->queryRow();
				$data2 = Yii::app()->db->createCommand("SELECT SUM(tax_amount) as qt_taxamount FROM {$tblpx}inv_list WHERE inv_id=" . $data['invoice_id'] . "")->queryRow();
				$model = Invoice::model()->findByPk($data['invoice_id']);
				$model->amount = $data1['qt_amount'];
				$model->subtotal = $data1['qt_amount'];
				$model->tax_amount = $data2['qt_taxamount'];
				if ($model->save()) {
					$result = '';
					$invoice_details  = Yii::app()->db->createCommand(" SELECT * FROM {$tblpx}inv_list  WHERE inv_id=" . $data['invoice_id'] . "")->queryAll();
					foreach ($invoice_details as $key => $values) {
						$result .= '<tr>';
						$result .= '<td><div id="item_sl_no">' . ($key + 1) . '</div></td>';
						$result .= '<td><div class="item_description">' . $values['description'] . '</div> </td>';
						$result .= '<td><div class="item_hsn_code">' . $values['hsn_code'] . '</div> </td>';
						if($values['type'] == 0){
							$result .= '<td id="'.$values['type'].'"><div class="type">Lumpsum</div> </td>';
						}else{
							$result .= '<td id="'.$values['type'].'"><div class="type">Quantity * Rate</div> </td>';
						}
						$result .= '<td class="text-right"> <div class="" id="quantity"> ' . $values['quantity'] . '</div> </td>';
						$result .= '<td> <div class="unit" id="unit"> ' . $values['unit'] . '</div> </td>';
						$result .= '<td class="text-right"><div class="" id="rate">' . number_format($values['rate'], 2, '.', '') . '</div></td>';

						$result .= '<td class="text-right">' . $values['sgst'] . '</td>';
						$result .= '<td class="text-right">' . number_format($values['sgst_amount'], 2, '.', '') . '</td>';
						$result .= '<td class="text-right">' . $values['cgst'] . '</td>';
						$result .= '<td class="text-right">' . number_format($values['cgst_amount'], 2, '.', '') . '</td>';
						$result .= '<td class="text-right">' . $values['igst'] . '</td>';
						$result .= '<td class="text-right">' . number_format($values['igst_amount'], 2, '.', '') . '</td>';
						$result .= '<td class="text-right"><div class="" id="amount"> ' . number_format($values['amount'], 2, '.', '') . '</div></td>';
						$result .= '<td class="text-right">' . number_format($values['tax_amount'], 2, '.', '') . '</td>';
						$edit_class= ($values['type']==0)?"edit_item_lumpsum":"edit_item";

						$result .= '<td width="60"><span class="icon icon-options-vertical popover-test" data-toggle="popover"   data-placement="left" type="button" data-html="true" style="cursor: pointer;" ></span>
                            <div class="popover-content hide">
                                <ul class="tooltip-hiden">
                                                                       
                                    <li><a href="#" id=' . $values['id'] . ' class="btn btn-xs btn-default removebtn">Delete</a></li> <li><a href="" id=' . $values['id'] . ' class="btn btn-xs btn-default '.$edit_class.'">Edit</a></li></ul></div></td>';

						$result .= '</tr>';
					}
				}

				echo json_encode(array('response' => 'success', 'msg' => 'Sales item save successfully', 'item_id' => $last_id, 'html' => $result, 'final_amount' => number_format($data1['qt_amount'], 2), 'final_tax' => number_format($data2['qt_taxamount'], 2), 'grand_total' => number_format($data2['qt_taxamount'] + $data1['qt_amount'], 2),'grand_total_number'=>($data2['qt_taxamount'] + $data1['qt_amount'])));
			} else {
				echo json_encode(array('response' => 'error', 'msg' => 'Some problem occured'));
				//$result['result'] .= array('response' => 'error', 'msg' => 'Some problem occured');
			}
			//echo json_encode($result);

		}
	}


	public function actionUpdatesinvoiceitem()
	{
		$data = $_REQUEST['data'];
		$grand = $data['grand'];
		$subtot = $data['subtot'];
		$grand_tax = $data['grand_tax'];
		if (isset($data['description'])) {
			$description = $data['description'];
		} else {
			$description = '';
		}

		if (isset($data['hsn_code'])) {
			$hsn_code = $data['hsn_code'];
		} else {
			$hsn_code = '';
		}

		if (isset($data['quantity'])) {
			$quantity = $data['quantity'];
		} else {
			$quantity  = '';
		}
		if (isset($data['unit'])) {
			$unit = $data['unit'];
		} else {
			$unit  = '';
		}
		if (isset($data['rate'])) {
			$rate = $data['rate'];
		} else {
			$rate  = '';
		}
		if (isset($data['amount'])) {
			$amount = $data['amount'];
		} else {
			$amount  = '';
		}

		if (isset($data['sgst'])) {
			$sgst = $data['sgst'];
		} else {
			$sgst  = '';
		}

		if (isset($data['sgst_amount'])) {
			$sgst_amount = $data['sgst_amount'];
		} else {
			$sgst_amount  = '';
		}

		if (isset($data['cgst'])) {
			$cgst = $data['cgst'];
		} else {
			$cgst  = '';
		}

		if (isset($data['cgst_amount'])) {
			$cgst_amount = $data['cgst_amount'];
		} else {
			$cgst_amount  = '';
		}

		if (isset($data['igst'])) {
			$igst = $data['igst'];
		} else {
			$igst  = '';
		}

		if (isset($data['igst_amount'])) {
			$igst_amount = $data['igst_amount'];
		} else {
			$igst_amount  = '';
		}

		if (isset($data['tax_amount'])) {
			$tax_amount = $data['tax_amount'];
		} else {
			$tax_amount  = '';
		}
		$result = '';
		$result['html'] = '';
		$html = '';
		$tblpx = Yii::app()->db->tablePrefix;
		if(ANCAPMS_LINKED){
			if(isset($data['inv_no'])){
				$inv_no =$data['inv_no'];
				$invoice_model = Invoice::Model()->findByPk($data['invoice_id']);
				if(!empty($invoice_model)){
					$invoice_model->inv_no= $inv_no;
					$invoice_model->save();
				}
			}
		}

		if (!empty($data) && $data['item_id'] != 0) {
			$item_model = InvList::model()->findByPk($data['item_id']);
			$item_model->quantity = $quantity;
			$item_model->unit = $unit;
			$item_model->rate = $rate;
			$item_model->amount = $amount;
			$item_model->description = $description;
			$item_model->hsn_code = $hsn_code;
			$item_model->cgst = $cgst;
			$item_model->cgst_amount = $cgst_amount;
			$item_model->sgst = $sgst;
			$item_model->sgst_amount = $sgst_amount;
			$item_model->igst = $igst;
			$item_model->igst_amount = $igst_amount;
			$item_model->tax_amount = $tax_amount;
			if ($item_model->save()) {

				$tblpx = Yii::app()->db->tablePrefix;
				$data1 = Yii::app()->db->createCommand("SELECT SUM(amount) as qt_amount FROM {$tblpx}inv_list WHERE inv_id=" . $data['invoice_id'] . "")->queryRow();
				$data2 = Yii::app()->db->createCommand("SELECT SUM(tax_amount) as qt_taxamount FROM {$tblpx}inv_list WHERE inv_id=" . $data['invoice_id'] . "")->queryRow();
				$model = Invoice::model()->findByPk($data['invoice_id']);
				$model->amount = $data1['qt_amount'];
				$model->subtotal = $data1['qt_amount'];
				$model->tax_amount = $data2['qt_taxamount'];
				$model->save();
				$result = '';
				$invoice_details  = Yii::app()->db->createCommand(" SELECT * FROM {$tblpx}inv_list  WHERE inv_id=" . $data['invoice_id'] . "")->queryAll();
				foreach ($invoice_details as $key => $values) {
					$result .= '<tr>';
					$result .= '<td><div id="item_sl_no">' . ($key + 1) . '</div></td>';
					$result .= '<td><div class="item_description">' . $values['description'] . '</div> </td>';
					$result .= '<td><div class="item_description">' . $values['hsn_code'] . '</div> </td>';
					if($values['type'] == 0){
						$result .= '<td id="'.$values['type'].'"><div class="type">Lumpsum</div> </td>';
					}else{
						$result .= '<td id="'.$values['type'].'"><div class="type">Quantity * Rate</div> </td>';
					}
					$result .= '<td class="text-right"> <div class="" id="unit"> ' . $values['quantity'] . '</div> </td>';
					$result .= '<td> <div class="unit" id="unit"> ' . $values['unit'] . '</div> </td>';
					$result .= '<td class="text-right"><div class="" id="rate">' . number_format($values['rate'], 2, '.', '') . '</div></td>';

					$result .= '<td class="text-right">' . $values['sgst'] . '</td>';
					$result .= '<td class="text-right">' . number_format($values['sgst_amount'], 2, '.', '') . '</td>';
					$result .= '<td class="text-right">' . $values['cgst'] . '</td>';
					$result .= '<td class="text-right">' . number_format($values['cgst_amount'], 2, '.', '') . '</td>';
					$result .= '<td class="text-right">' . $values['igst'] . '</td>';
					$result .= '<td class="text-right">' . number_format($values['igst_amount'], 2, '.', '') . '</td>';
					$result .= '<td class="text-right"><div class="" id="amount"> ' . number_format($values['amount'], 2, '.', '') . '</div></td>';
					$result .= '<td class="text-right">' . number_format($values['tax_amount'], 2, '.', '') . '</td>';

					$edit_class= ($values['type']==0)?"edit_item_lumpsum":"edit_item";
					$result .= '<td width="60"><span class="icon icon-options-vertical popover-test" data-toggle="popover"   data-placement="left" type="button" data-html="true" style="cursor: pointer;" ></span>
                                                            <div class="popover-content hide">
                                                                <ul class="tooltip-hiden">
                                                                       
                                                                        <li><a href="#" id=' . $values['id'] . ' class="btn btn-xs btn-default removebtn">Delete</a></li> <li><a href="" id=' . $values['id'] . ' class="btn btn-xs btn-default '.$edit_class.'">Edit</a></li></ul></div></td>';

					$result .= '</tr>';
				}

				echo json_encode(array('response' => 'success', 'msg' => 'Sales item save successfully', 'item_id' => $data['item_id'], 'html' => $result, 'final_amount' => number_format($data1['qt_amount'], 2), 'final_tax' => number_format($data2['qt_taxamount'], 2), 'grand_total' => number_format($data2['qt_taxamount'] + $data1['qt_amount'], 2),'grand_total_number'=>($data2['qt_taxamount'] + $data1['qt_amount'])));
			} else {
				echo json_encode(array('response' => 'error', 'msg' => 'Some problem occured'));
			}
		}
	}

	public function actionRemoveinvoiceitem()
	{
		$data   = $_REQUEST['data'];
		$tblpx = Yii::app()->db->tablePrefix;
		$del = Yii::app()->db->createCommand()->delete($tblpx . 'inv_list', 'id=:id', array(':id' => $data['item_id']));
		if ($del) {
			$tblpx = Yii::app()->db->tablePrefix;
			$data1 = Yii::app()->db->createCommand("SELECT SUM(amount) as qt_amount FROM {$tblpx}inv_list WHERE inv_id=" . $data['invoice_id'] . "")->queryRow();
			$data2 = Yii::app()->db->createCommand("SELECT SUM(tax_amount) as qt_taxamount FROM {$tblpx}inv_list WHERE inv_id=" . $data['invoice_id'] . "")->queryRow();
			$model = Invoice::model()->findByPk($data['invoice_id']);
			$model->amount = (isset($data1['qt_amount'])) ? $data1['qt_amount'] : 0;
			$model->subtotal = (isset($data1['qt_amount'])) ? $data1['qt_amount'] : 0;;
			$model->tax_amount = (isset($data2['qt_taxamount'])) ? $data2['qt_taxamount'] : 0;
			$model->save();
			$result = '';
			$invoice_details  = Yii::app()->db->createCommand(" SELECT * FROM {$tblpx}inv_list  WHERE inv_id=" . $data['invoice_id'] . "")->queryAll();
			foreach ($invoice_details as $key => $values) {
				$result .= '<tr>';
				$result .= '<td><div id="item_sl_no">' . ($key + 1) . '</div></td>';
				$result .= '<td><div class="item_description">' . $values['description'] . '</div> </td>';
				$result .= '<td><div class="item_hsn_code">' . $values['hsn_code'] . '</div> </td>';
				if($values['type'] == 0){
					$result .= '<td id="'.$values['type'].'"><div class="type">Lumpsum</div> </td>';
				}else{
					$result .= '<td id="'.$values['type'].'"><div class="type">Quantity * Rate</div> </td>';
				}
				$result .= '<td class="text-right"> <div class="" id="unit"> ' . $values['quantity'] . '</div> </td>';
				$result .= '<td> <div class="unit" id="unit"> ' . $values['unit'] . '</div> </td>';
				$result .= '<td class="text-right"><div class="" id="rate">' . number_format($values['rate'], 2, '.', '') . '</div></td>';

				$result .= '<td class="text-right">' . $values['sgst'] . '</td>';
				$result .= '<td class="text-right">' . number_format($values['sgst_amount'], 2, '.', '') . '</td>';
				$result .= '<td class="text-right">' . $values['cgst'] . '</td>';
				$result .= '<td class="text-right">' . number_format($values['cgst_amount'], 2, '.', '') . '</td>';
				$result .= '<td class="text-right">' . $values['igst'] . '</td>';
				$result .= '<td class="text-right">' . number_format($values['igst_amount'], 2, '.', '') . '</td>';
				$result .= '<td class="text-right"><div class="" id="amount"> ' . number_format($values['amount'], 2, '.', '') . '</div></td>';
				$result .= '<td class="text-right">' . number_format($values['tax_amount'], 2, '.', '') . '</td>';

				$edit_class= ($values['type']==0)?"edit_item_lumpsum":"edit_item";
				$result .= '<td width="60"><span class="icon icon-options-vertical popover-test" data-toggle="popover"   data-placement="left" type="button" data-html="true" style="cursor: pointer;" ></span>
                                    <div class="popover-content hide">
                                        <ul class="tooltip-hiden">

                                                <li><a href="#" id=' . $values['id'] . ' class="btn btn-xs btn-default removebtn">Delete</a></li> <li><a href="" id=' . $values['id'] . ' class="btn btn-xs btn-default '.$edit_class.'">Edit</a></li></ul></div></td>';

				$result .= '</tr>';
			}

			echo json_encode(array('response' => 'success', 'msg' => 'Sales deleted successfully', 'item_id' => $data['item_id'], 'html' => $result, 'final_amount' => number_format($data1['qt_amount'], 2), 'final_tax' => number_format($data2['qt_taxamount'], 2), 'grand_total' => number_format($data2['qt_taxamount'] + $data1['qt_amount'], 2),'grand_total_number'=>($data2['qt_taxamount'] + $data1['qt_amount'])));
		} else {
			echo json_encode(array('response' => 'error', 'msg' => 'Some Problem occured'));
		}
	}




	public function actionUpdateinvoice($invid)
	{
		$model 		=  Invoice::model()->find(array("condition" => "invoice_id = '$invid'"));
		if (empty($model)) {
			$this->redirect(array('invoice/admin'));
		}
		$item_model = InvList::model()->findAll(array("condition" => "inv_id = '$invid'"));
		$tblpx		= Yii::app()->db->tablePrefix;
		$project 	= Projects::model()->findByPK($model->project_id);
		$client     = Clients::model()->findByPK($model->client_id);
		$this->render('invoice_update', array(
			'model' => $model,
			'item_model' => $item_model,
			'invoice_id' => $invid,
			'project' => $project,
			'client' => $client
		));
	}
	public function actionAddperforma()
	{

		$model = new PerformaInvoice;
		$newmodel = new InvoiceList;

		if (isset($_POST['performa'])) {
			//print_r($_POST);
			$billto = $_POST['billto'];
			$date = date('Y-m-d', strtotime($_POST['date']));
			$invoiceno = $_POST['invoiceno'];
			$project = $_POST['project'];
			$subtot = $_POST['subtot'];
			$fees = $_POST['fees'];
			$grand = $_POST['grand'];

			if (isset($_POST['sl_No'])) {
				$sl_No = $_POST['sl_No'];
			} else {
				$sl_No = '';
			}
			if (isset($_POST['description'])) {
				$description = $_POST['description'];
			} else {
				$description = '';
			}
			if (isset($_POST['quantity'])) {
				$quantity = $_POST['quantity'];
			} else {
				$quantity  = '';
			}
			if (isset($_POST['unit'])) {
				$unit = $_POST['unit'];
			} else {
				$unit  = '';
			}
			if (isset($_POST['rate'])) {
				$rate = $_POST['rate'];
			} else {
				$rate  = '';
			}
			if (isset($_POST['amount'])) {
				$amount = $_POST['amount'];
			} else {
				$amount  = '';
			}



			$model->client_name = $billto;
			$model->date = $date;
			$model->inv_no = $invoiceno;
			$model->project_name = $project;
			$model->subtotal = $subtot;
			$model->fees = $fees;
			$model->amount = $grand;

			if ($model->save()) {

				$last_id = $model->id;
				if ($description != null || $quantity != null || $unit != null  || $rate != null  || $amount != null) {
					for ($i = 0; $i < sizeof($sl_No); $i++) {
						if ($description[$i] == null && $quantity[$i] == null && $unit[$i] == null  && $rate[$i] == null && $amount[$i] == null) {
							$this->redirect(array('invoice/performainvoice'));
						} else {
							$newmodel = new InvoiceList;
							$newmodel->perf_id = $last_id;
							$newmodel->quantity = $quantity[$i];
							$newmodel->unit = $unit[$i];
							$newmodel->rate = $rate[$i];
							$newmodel->amount = $amount[$i];
							$newmodel->description = $description[$i];
							//$newmodel->isNewRecord = true;
							$newmodel->save(false);
						}
					}
					$this->redirect(array('invoice/performainvoice'));
				} else {
					$this->redirect(array('invoice/performainvoice'));
				}
			} else {
				$error = $model->getErrors();
				//print_r($error); die;
				$this->render('performainvoice', array(
					'model' => $model,
					'newmodel' => $newmodel,
					'error' => $error,
					'billto' => $billto, 'date' => $date, 'invoiceno' => $invoiceno, 'project' => $project, 'subtot' => $subtot,
					'fees' => $fees, 'grand' => $grand, 'sl_No' => $sl_No, 'description' => $description, 'quantity' => $quantity,
					'unit' => $unit, 'rate' => $rate, 'amount' => $amount

				));
			}
		} else {
			$this->render('performainvoice', array(
				'model' => $model,
				'newmodel' => $newmodel,

			));
		}
	}



	public function actionUpdateperforma($pid)
	{

		//echo "hi"; die;
		$model = PerformaInvoice::model()->find(array("condition" => "id = '$pid'"));
		//print_r($model); 
		$newmodel = InvoiceList::model()->findAll(array("condition" => "perf_id = '$pid'"));
		if (isset($_POST['performa'])) {

			$billto = $_POST['billto'];

			$date = date('Y-m-d', strtotime($_POST['date']));
			$invoiceno = $_POST['invoiceno'];
			$project = $_POST['project'];
			$subtot = $_POST['subtot'];
			$fees = $_POST['fees'];
			$grand = $_POST['grand'];

			//	die('success');
			$model->client_name = $billto;
			$model->date = $date;
			$model->inv_no = $invoiceno;
			$model->project_name = $project;
			$model->subtotal = $subtot;
			$model->fees = $fees;
			$model->amount = $grand;

			if (isset($_POST['sl_No'])) {
				$sl_No = $_POST['sl_No'];
			} else {
				$sl_No = '';
			}
			if (isset($_POST['description'])) {
				$description = $_POST['description'];
			} else {
				$description = '';
			}
			if (isset($_POST['quantity'])) {
				$quantity = $_POST['quantity'];
			} else {
				$quantity  = '';
			}
			if (isset($_POST['unit'])) {
				$unit = $_POST['unit'];
			} else {
				$unit  = '';
			}
			if (isset($_POST['rate'])) {
				$rate = $_POST['rate'];
			} else {
				$rate  = '';
			}
			if (isset($_POST['amount'])) {
				$amount = $_POST['amount'];
			} else {
				$amount  = '';
			}
			if (isset($_POST['ids'])) {
				$ids = $_POST['ids'];
			} else {
				$ids = '';
			}

			/*$description = $_POST['description'];
			$quantity = $_POST['quantity'];
			$unit = $_POST['unit'];
			$rate = $_POST['rate'];
			$amount = $_POST['amount'];
			$ids = $_POST['ids'];*/


			if ($model->save()) {


				if ($description != null || $quantity != null || $unit != null  || $rate != null  || $amount != null) {

					foreach ($ids as $k => $id) {


						if ($id != NULL) {
							if ($description[$k] == null && $quantity[$k] == null && $unit[$k] == null  && $rate[$k] == null && $amount[$k] == null) {
								$this->redirect(array('invoice/performainvoice'));
							} else {
								$newmodel = InvoiceList::model()->findByPk($id);
								$newmodel->perf_id = $pid;
								$newmodel->quantity = $quantity[$k];
								$newmodel->unit = $unit[$k];
								$newmodel->rate = $rate[$k];
								$newmodel->amount = $amount[$k];
								$newmodel->description = $description[$k];
								$newmodel->save(false);
							}
						} else {
							if ($description[$k] == null && $quantity[$k] == null && $unit[$k] == null  && $rate[$k] == null && $amount[$k] == null) {
								$this->redirect(array('invoice/performainvoice'));
							} else {
								$newmodel = new InvoiceList;
								$newmodel->perf_id = $pid;
								$newmodel->quantity = $quantity[$k];
								$newmodel->unit = $unit[$k];
								$newmodel->rate = $rate[$k];
								$newmodel->amount = $amount[$k];
								$newmodel->description = $description[$k];
								$newmodel->save(false);
							}
						}
					}
					$this->redirect(array('invoice/performainvoice'));
				} else {
					$this->redirect(array('invoice/performainvoice'));
				}
			} else {
				$error = $model->getErrors();
				$this->render('performa_update', array(
					'model' => $model,
					'newmodel' => $newmodel,
					'error' => $error,
					'billto' => $billto, 'date' => $date, 'invoiceno' => $invoiceno, 'project' => $project, 'subtot' => $subtot,
					'fees' => $fees, 'grand' => $grand, 'ids' => $ids, 'description' => $description, 'quantity' => $quantity,
					'unit' => $unit, 'rate' => $rate, 'amount' => $amount, 'pid' => $pid

				));
			}
		} else {

			$this->render('performa_update', array(
				'model' => $model,
				'newmodel' => $newmodel,
				'pid' => $pid,

			));
		}
	}
	public function actionSaveperformainvoice()
	{

		$details = $_POST['details'];
		$billto = $_POST['billto'];
		$date = date('Y-m-d', strtotime($_POST['date']));
		$invoiceno = $_POST['invoiceno'];
		$project = $_POST['project'];

		if (isset($_POST['sl_No'])) {
			$sl_No = $_POST['sl_No'];
		} else {
			$sl_No = '';
		}
		if (isset($_POST['description'])) {
			$description = $_POST['description'];
		} else {
			$description = '';
		}
		if (isset($_POST['quantity'])) {
			$quantity = $_POST['quantity'];
		} else {
			$quantity  = '';
		}
		if (isset($_POST['unit'])) {
			$unit = $_POST['unit'];
		} else {
			$unit  = '';
		}
		if (isset($_POST['rate'])) {
			$rate = $_POST['rate'];
		} else {
			$rate  = '';
		}
		if (isset($_POST['amount'])) {
			$amount = $_POST['amount'];
		} else {
			$amount  = '';
		}



		$subtot = $_POST['subtot'];
		$fees = $_POST['fees'];
		$grand = $_POST['grand'];



		$mPDF1 = Yii::app()->ePdf->mPDF();

		$mPDF1 = Yii::app()->ePdf->mPDF('', 'A4');

		$mPDF1->WriteHTML($stylesheet, 1);

		$mPDF1->WriteHTML($this->renderPartial('performainvoiceview', array(
			'details' => $details, 'billto' => $billto, 'date' => $date,
			'invoiceno' => $invoiceno, 'project' => $project, 'subtot' => $subtot, 'grand' => $grand, 'fees' => $fees,
			'sl_No' => $sl_No, 'description' => $description, 'quantity' => $quantity, 'unit' => $unit, 'rate' => $rate, 'amount' => $amount

		), true));

		$mPDF1->Output('Performa_Invoice.pdf', 'D');
	}


	public function actionSaveInvoice($invid)
	{
		$this->logo = $this->realpath_logo;
		$model 			= Invoice::model()->find(array("condition" => "invoice_id='$invid'"));
		$item_model 	= InvList::model()->findAll(array("condition" => "inv_id = '$invid'"));
		$project 	    = Projects::model()->findByPK($model->project_id);
		$client         = Clients::model()->findByPK($model->client_id);

		$compid = Yii::app()->user->company_id;
		$compmodel = Company::model()->findBypk($compid);
		$address = $compmodel->address;


		$mPDF1 = Yii::app()->ePdf->mPDF();

		$mPDF1 = Yii::app()->ePdf->mPDF('', 'A4');

		$sql = 'SELECT template_name '
                . 'FROM jp_quotation_template '
                . 'WHERE status="1"';
        $selectedtemplate =Yii::app()->db->createCommand($sql)->queryScalar();
        
        $template = $this->getTemplate($selectedtemplate);
        $mPDF1->SetHTMLHeader($template['header']);
        $mPDF1->SetHTMLFooter($template['footer']);
        $mPDF1->AddPage('','', '', '', '', 0,0,40, 30, 10,0);

		$mPDF1->WriteHTML($this->renderPartial('invoicpdf', array(
			'model' => $model,
			'address' => $address,
			'itemmodel' => $item_model,
			'project'   => $project['name'],
			'client'    => $client['name']
		), true));

		$mPDF1->Output('Invoice.pdf', 'D');
	}


	public function actionExportInvoice($invid)
	{
		$model 			= Invoice::model()->find(array("condition" => "invoice_id='$invid'"));
		$writeoff   = Writeoff::model()->find(array("condition" => "invoice_id='$invid'"));
		if (!$writeoff)
			$writeoff = new Writeoff;
		//echo "<pre>";print_r($writeoff);exit;
		$item_model 	= InvList::model()->findAll(array("condition" => "inv_id = '$invid'"));
		$project 	    = Projects::model()->findByPK($model->project_id);
		$client         = Clients::model()->findByPK($project->client_id);
		$company = Company::model()->findByPk($model->company_id);
		$arraylabel = array('Company', 'Project', 'Client', 'Date', 'Invoice No');
		$finaldata = array();

		$finaldata[0][] = $company['name'];
		$finaldata[0][] = $project->name;
		$finaldata[0][] = $client->name;
		$finaldata[0][] = $model->date;
		$finaldata[0][] = $model->inv_no;

		$finaldata[1][] = '';
		$finaldata[1][] = '';
		$finaldata[1][] = '';
		$finaldata[1][] = '';
		$finaldata[1][] = '';

		$finaldata[2][] = '';
		$finaldata[2][] = '';
		$finaldata[2][] = '';
		$finaldata[2][] = '';
		$finaldata[2][] = '';

		$finaldata[3][] = '';
		$finaldata[3][] = '';
		$finaldata[3][] = '';
		$finaldata[3][] = '';
		$finaldata[3][] = '';

		$finaldata[4][] = 'Sl.No';
		$finaldata[4][] = 'Description';
		$finaldata[4][] = 'HSN Code';
		if ($model->type == 'quantity_rate') {
			$finaldata[4][] = 'Quantity';
			$finaldata[4][] = 'Unit';
			$finaldata[4][] = 'Rate';
		}
		$finaldata[4][] = 'SGST';
		$finaldata[4][] = 'SGST Amount';
		$finaldata[4][] = 'CGST';
		$finaldata[4][] = 'CGST Amount';
		$finaldata[4][] = 'IGST';
		$finaldata[4][] = 'IGST Amount';
		$finaldata[4][] = 'Amount';
		$finaldata[4][] = 'Tax Amount';

		foreach ($item_model as $key => $value) {
			$finaldata[$key + 5][] = $key + 1;
			$finaldata[$key + 5][] = $value['description'];
			$finaldata[$key + 5][] = $value['hsn_code'];
			if ($model->type == 'quantity_rate') {
				$finaldata[$key + 5][] = $value['quantity'];
				$finaldata[$key + 5][] = $value['unit'];
				$finaldata[$key + 5][] = Controller::money_format_inr($value['rate'], 2, 1);
			}
			$finaldata[$key + 5][] = $value['sgst'];
			$finaldata[$key + 5][] = Controller::money_format_inr($value['sgst_amount'], 2, 1);
			$finaldata[$key + 5][] = $value['cgst'];
			$finaldata[$key + 5][] = Controller::money_format_inr($value['cgst_amount'], 2, 1);
			$finaldata[$key + 5][] = $value['igst'];
			$finaldata[$key + 5][] = Controller::money_format_inr($value['igst_amount'], 2, 1);
			$finaldata[$key + 5][] = Controller::money_format_inr($value['amount'], 2, 1);
			$finaldata[$key + 5][] = Controller::money_format_inr($value['tax_amount'], 2, 1);
			$x = $key + 5;
		}

		$finaldata[$x + 1][] = '';
		$finaldata[$x + 1][] = '';
		$finaldata[$x + 1][] = '';
		if ($model->type == 'quantity_rate') {
			$finaldata[$x + 1][] = '';
			$finaldata[$x + 1][] = '';
			$finaldata[$x + 1][] = '';
		}
		$finaldata[$x + 1][] = '';
		$finaldata[$x + 1][] = '';
		$finaldata[$x + 1][] = '';
		$finaldata[$x + 1][] = '';
		$finaldata[$x + 1][] = '';
		$finaldata[$x + 1][] = '';
		$finaldata[$x + 1][] = '';
		$finaldata[$x + 1][] = '';

		$finaldata[$x + 2][] = '';
		$finaldata[$x + 2][] = '';
		$finaldata[$x + 1][] = '';
		if ($model->type == 'quantity_rate') {
			$finaldata[$x + 2][] = '';
			$finaldata[$x + 2][] = '';
			$finaldata[$x + 2][] = '';
		}
		$finaldata[$x + 2][] = '';
		$finaldata[$x + 2][] = '';
		$finaldata[$x + 2][] = '';
		$finaldata[$x + 2][] = '';
		$finaldata[$x + 2][] = '';
		$finaldata[$x + 2][] = 'Total';
		$finaldata[$x + 2][] = Controller::money_format_inr($model->amount, 2, 1);
		$finaldata[$x + 2][] = Controller::money_format_inr($model->tax_amount, 2, 1);

		$finaldata[$x + 3][] = '';
		$finaldata[$x + 3][] = '';
		$finaldata[$x + 1][] = '';
		if ($model->type == 'quantity_rate') {
			$finaldata[$x + 3][] = '';
			$finaldata[$x + 3][] = '';
			$finaldata[$x + 3][] = '';
		}
		$finaldata[$x + 3][] = '';
		$finaldata[$x + 3][] = '';
		$finaldata[$x + 3][] = '';
		$finaldata[$x + 3][] = '';
		$finaldata[$x + 3][] = '';
		$finaldata[$x + 3][] = '';
		$finaldata[$x + 3][] = 'Grand Totals';
		$finaldata[$x + 3][] = Controller::money_format_inr(($model->amount + $model->tax_amount), 2, 1);
       
		$finaldata[$x + 4][] = '';
		$finaldata[$x + 4][] = '';
		$finaldata[$x + 4][] = '';
		$finaldata[$x + 4][] = '';
		$finaldata[$x + 4][] = '';
		$finaldata[$x + 4][] = '';
		$finaldata[$x + 4][] = '';
		$finaldata[$x + 4][] = '';
        $finaldata[$x + 4][] = 'Write Off';
		$finaldata[$x + 4][] = Controller::money_format_inr(($writeoff->amount), 2, 1);
		
		$finaldata[$x + 5][] = '';
		$finaldata[$x + 5][] = '';
		$finaldata[$x + 5][] = '';
		$finaldata[$x + 5][] = '';
		$finaldata[$x + 5][] = '';
		$finaldata[$x + 5][] = '';
		$finaldata[$x + 5][] = '';
		$finaldata[$x + 5][] = '';
       
		$finaldata[$x + 5][] = 'Balance';
		$finaldata[$x + 5][] = Controller::money_format_inr((($model->amount + $model->tax_amount)-$writeoff->amount), 2, 1);
       
		//print_r($finaldata); exit();
		Yii::import('ext.ECSVExport');

		$csv = new ECSVExport($finaldata);
		$csv->setHeaders($arraylabel);
		$csv->setToAppend();
		$contentdata = $csv->toCSV();
		$csvfilename = 'Invoice' . date('d_M_Y') . '.csv';
		Yii::app()->getRequest()->sendFile($csvfilename, $contentdata, "text/csv", false);
		exit();
	}


	public function actionExportPerforma()
	{



		$billto = $_POST['billto'];
		$date = date('Y-m-d', strtotime($_POST['date']));
		$invoiceno = $_POST['invoiceno'];
		$project = $_POST['project'];

		if (isset($_POST['sl_No'])) {
			$sl_No = $_POST['sl_No'];
		} else {
			$sl_No = '';
		}
		if (isset($_POST['description'])) {
			$description = $_POST['description'];
		} else {
			$description = '';
		}
		if (isset($_POST['quantity'])) {
			$quantity = $_POST['quantity'];
		} else {
			$quantity  = '';
		}
		if (isset($_POST['unit'])) {
			$unit = $_POST['unit'];
		} else {
			$unit  = '';
		}
		if (isset($_POST['rate'])) {
			$rate = $_POST['rate'];
		} else {
			$rate  = '';
		}
		if (isset($_POST['amount'])) {
			$amount = $_POST['amount'];
		} else {
			$amount  = '';
		}

		$subtot = $_POST['subtot'];
		$fees = $_POST['fees'];
		$grand = $_POST['grand'];


		$arraylabel = array('Project Name', 'Bill To', 'Date', 'Invoice No', '');
		$finaldata = array();
		$finaldata[0][] = $project;
		$finaldata[0][] = $billto;
		$finaldata[0][] = $date;
		$finaldata[0][] = $invoiceno;

		$finaldata[1][] = '';
		$finaldata[1][] = '';
		$finaldata[1][] = '';
		$finaldata[1][] = '';
		$finaldata[1][] = '';
		$finaldata[1][] = '';

		$finaldata[2][] = 'Sl.No';
		$finaldata[2][] = 'Description';
		$finaldata[2][] = 'Quantity';
		$finaldata[2][] = 'Unit';
		$finaldata[2][] = 'Rate';
		$finaldata[2][] = 'Amount';

		if (!empty($sl_No)) {
			foreach ($sl_No as $k => $sl) {

				$finaldata[$k + 3][] = $sl_No[$k];
				$finaldata[$k + 3][] = $description[$k];
				$finaldata[$k + 3][] = $quantity[$k];
				$finaldata[$k + 3][] = $unit[$k];
				$finaldata[$k + 3][] = $rate[$k];
				$finaldata[$k + 3][] = $amount[$k];
				$x = $k + 3;
			}
			$finaldata[$x + 1][] = '';
			$finaldata[$x + 1][] = '';
			$finaldata[$x + 1][] = '';
			$finaldata[$x + 1][] = '';
			$finaldata[$x + 1][] = '';

			$finaldata[$x + 2][] = '';
			$finaldata[$x + 2][] = '';
			$finaldata[$x + 2][] = '';
			$finaldata[$x + 2][] = '';
			$finaldata[$x + 2][] = '';

			$finaldata[$x + 3][] = 'Sub Total';
			$finaldata[$x + 3][] = $subtot;

			$finaldata[$x + 4][] = 'Fees';
			$finaldata[$x + 4][] = $fees;

			$finaldata[$x + 5][] = 'Grand Total';
			$finaldata[$x + 5][] = $grand;
		} else {
			$finaldata[3][] = '';
			$finaldata[3][] = '';
			$finaldata[3][] = '';
			$finaldata[3][] = '';
			$finaldata[3][] = '';
			$finaldata[3][] = '';


			$finaldata[4][] = '';
			$finaldata[4][] = '';
			$finaldata[4][] = '';
			$finaldata[4][] = '';

			$finaldata[5][] = 'Sub Total';
			$finaldata[5][] = $subtot;

			$finaldata[6][] = 'Fees';
			$finaldata[6][] = $fees;

			$finaldata[7][] = 'Grand Total';
			$finaldata[7][] = $grand;
		}


		Yii::import('ext.ECSVExport');

		$csv = new ECSVExport($finaldata);
		$csv->setHeaders($arraylabel);
		$csv->setToAppend();
		$contentdata = $csv->toCSV();
		$csvfilename = 'Invoice' . date('d_M_Y') . '.csv';
		Yii::app()->getRequest()->sendFile($csvfilename, $contentdata, "text/csv", false);
		exit();
	}

	public function actionviewinvoice($invid)
	{
		$model      = Invoice::model()->find(array("condition" => "invoice_id='$invid'"));
		$item_model = InvList::model()->findAll(array("condition" => "inv_id = '$invid'"));
		$writeoff   = Writeoff::model()->find(array("condition" => "invoice_id='$invid'"));
		if (!$writeoff)
			$writeoff = new Writeoff;
		if (!empty($model)) {
			$project    = Projects::model()->findByPK($model->project_id);
			$client     = Clients::model()->findByPK($model->client_id);
			$pdf_data = ProjectTemplate::model()->find(array("condition" => "status='1'"));
        
			$render_datas = array(
				'model' => $model,
				'itemmodel' => $item_model,
				'project'   => $project,
				'client'    => $client,
				'writeoff'  => $writeoff,
				'invoice_id' => $invid,
				'pdf_data' => $pdf_data->template_format_invoice,          
            
			);
			if (isset($_GET['exportpdf'])) {
				$this->logo = $this->realpath_logo;
				$ptemplate = $this->getActiveTemplate();
				
				$file='invoiceview';//default
				if($ptemplate == 'TYPE-6'){ //Kapcher
					$file='invoiceviewdemo';
				}
				if($pdf_data->template_format_invoice !='')
				{
					$file='dynamic_invoiceview';
					$ptemplate_with_company = $this->getActiveTemplateIDWithCompany($model->company_id);
					if($ptemplate_with_company ==''){
						Yii::app()->user->setFlash('error', 'No active PDF layout for this company!');
						$this->redirect(array("invoice/viewinvoice", "invid" => $invid));
					}
				}
				
                $pdfdata = $this->renderPartial($file, $render_datas, true);
				//echo "<pre>"; print_r($pdfdata);exit;
				$filename = 'Invoice.pdf';
                //$mPDF1 = Yii::app()->ePdf->mPDF();
                $mPDF1 = Yii::app()->ePdf->mPDF('', 'A4');
                
                $sql = 'SELECT template_name '
                        . 'FROM jp_quotation_template '
                        . 'WHERE status="1"';
                $selectedtemplate =Yii::app()->db->createCommand($sql)->queryScalar();
                
                $template = $this->getTemplate($selectedtemplate);
				
				if($ptemplate == 'Type-11'){ 
					$serverPath = Yii::getPathOfAlias('webroot') . '/uploads/company_assets/media/equiplex-footer-update__1_.png';
					
					$path = Yii::app()->request->baseUrl . '/uploads/company_assets/media/';
				
					$image=$path.'equiplex-footer-update__1_.png';
					if (file_exists($serverPath)) {
					$footer= '<div style="padding: 5px 20px 20px;text-align:center;font-size:10px;">
					<img class="w-100" src="'.$image.'" />
					Powered by bluehorizoninfotech.com<br>
					Copyright ©'.date('Y').' by Blue Horizon Infotech, Cochin. All Rights Reserved.
					</div>';
					}else{
						$footer= '<div style="padding: 5px 20px 20px;text-align:center;font-size:10px;">
				Powered by bluehorizoninfotech.com<br>
				Copyright ©'.date('Y').' by Blue Horizon Infotech, Cochin. All Rights Reserved.
				</div>';
					}
				}else{
					$footer= '<div style="padding: 5px 20px 20px;text-align:center;font-size:10px;">
				Powered by bluehorizoninfotech.com<br>
				Copyright ©'.date('Y').' by Blue Horizon Infotech, Cochin. All Rights Reserved.
				</div>';
				}
				
				$mPDF1->SetHTMLFooter($footer, 'C');
                
                $sql = 'SELECT template_name '
                        . 'FROM jp_quotation_template '
                        . 'WHERE status="1"';
				$selectedtemplate =Yii::app()->db->createCommand($sql)->queryScalar();
				if ($selectedtemplate == 'template1'){
				$mPDF1->AddPage('','', '', '', '', 0,0,12, 30, 10,0);
				}else{
					$mPDF1->AddPage('','', '', '', '', 0,0,12, 30, 10,0);

				}
                $mPDF1->WriteHTML($pdfdata);
                $mPDF1->Output($filename, 'D');
            } else {
                $this->render('invoiceview', $render_datas);
            }
			
		} else {
			$this->redirect(array('invoice/admin'));
		}
	}


	public function actionViewperforma($pid)
	{

		$model = PerformaInvoice::model()->find(array("condition" => "id = '$pid'"));
		$newmodel = InvoiceList::model()->findAll(array("condition" => "perf_id = '$pid'"));

		$this->render('viewperforma', array(
			'model' => $model,
			'newmodel' => $newmodel,

		));
	}

	public function actionDeleteinvdetails()
	{
		//print_r($_POST); die;
		$id = $_POST['id'];
		$tblpx = Yii::app()->db->tablePrefix;
		$amount = Yii::app()->db->createCommand()
			->select('inv.amount,inv.subtotal,inv.fees,inlist.amount as am,inv.invoice_id')
			->from($tblpx . 'invoice inv')
			->join($tblpx . 'inv_list inlist', 'inv.invoice_id=inlist.inv_id')
			->where('inlist.id=:id', array(':id' => $id))
			->queryRow();
		// echo $amount['amount'];exit;
		$gamt = $amount['amount'] - $amount['am'];
		$samt = $amount['subtotal'] - $amount['am'];
		$famt = $amount['fees'];

		$del = Yii::app()->db->createCommand("UPDATE {$tblpx}invoice SET amount = '" . $gamt . "', subtotal = '" . $samt . "' WHERE invoice_id = '" . $amount['invoice_id'] . "'")->execute();
		$del = Yii::app()->db->createCommand()
			->delete($tblpx . 'inv_list', 'id=:id', array(':id' => $id));
		if ($del) {
			echo json_encode(array('response' => 'success'));
		}
	}
	public function actionDeleteperfdetails()
	{

		$id = $_POST['id'];
		$tblpx = Yii::app()->db->tablePrefix;
		$amount = Yii::app()->db->createCommand()
			->select('inv.amount,inv.subtotal,inv.fees,perf_id.amount as am,inv.id')
			->from($tblpx . 'performa_invoice inv')
			->join($tblpx . 'invoice_list perf_id', 'inv.id=perf_id.perf_id')
			->where('perf_id.id=:id', array(':id' => $id))
			->queryRow();
		// echo $amount['amount'];exit;
		$gamt = $amount['amount'] - $amount['am'];
		$samt = $amount['subtotal'] - $amount['am'];
		$famt = $amount['fees'];

		$del = Yii::app()->db->createCommand("UPDATE {$tblpx}performa_invoice SET amount = '" . $gamt . "', subtotal = '" . $samt . "' WHERE id = '" . $amount['id'] . "'")->execute();

		$del = Yii::app()->db->createCommand()
			->delete($tblpx . 'invoice_list', 'id=:id', array(':id' => $id));
		if ($del) {
			echo json_encode(array('response' => 'success'));
		}
	}


	public function actionTestmail($invid)
	{


		$tblpx = Yii::app()->db->tablePrefix;
		$model =  Invoice::model()->find(array("condition" => "invoice_id = '$invid'"));
		$newmodel = InvList::model()->findAll(array("condition" => "inv_id = '$invid'"));
		$inv_no = $model['inv_no'];
		$pid = $model['project_id'];
		$sql = Yii::app()->db->createCommand("select {$tblpx}clients.email_id,{$tblpx}clients.name from {$tblpx}clients
		 left join {$tblpx}projects ON {$tblpx}projects.client_id={$tblpx}clients.cid 
		 where {$tblpx}projects.pid=$pid")->queryRow();
		$newemail = $sql['email_id'];




		$mPDF1 = Yii::app()->ePdf->mPDF();

		$mPDF1 = Yii::app()->ePdf->mPDF('', 'A4');

		$mPDF1->WriteHTML($stylesheet, 1);

		$mPDF1->WriteHTML($this->renderPartial('_emailform', array('model' => $model, 'newmodel' => $newmodel), true));
		$newdate = date('Y-m-d');
		$path = $_SERVER['DOCUMENT_ROOT'] . '/documents/Invoice_' . $newdate . '.pdf';

		$mPDF1->Output($path, 'F');

		$filename = $_SERVER['DOCUMENT_ROOT'] . '/documents/Invoice_' . $newdate . '.pdf';

		if (file_exists($filename)) {

			if (!empty($newemail)) {


				//require_once 'PHPMailer/PHPMailerAutoload.php';
				$mail = new JPhpMailer();

				$email = $newemail;
				$subject = Yii::app()->name . "-INVOICE!";
				$headers = Yii::app()->name;

				$bodyContent =  "<p>Hello " . $sql['name'] . ",</p><p>Please Check invoice Attachment.</p><br><br><p>Regards,</p><p>" . Yii::app()->name . "</p>";
				/*
						 	
								$mail->isSMTP();                                   // Set mailer to use SMTP
								$mail->Host = 'smtp.gmail.com';                    // Specify main and backup SMTP servers
								$mail->SMTPAuth = true;                            // Enable SMTP authentication
								$mail->Username = 'testing@bluehorizoninfotech.com';                 // SMTP username
								$mail->Password = 'Test2paSS()';                           // SMTP password
								$mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
								$mail->Port = 465;
								*/
				$newdate = date('Y-m-d');

				$mail->setFrom('info@bhiapp.com', Yii::app()->name);
				$mail->addAddress($email);   // Add a recipient
				$mail->isHTML(true);

				$mail->Subject = "Invoice";
				//$mail->MsgHTML($bodyContent);
				$path = $_SERVER['DOCUMENT_ROOT'] . '/documents/Invoice_' . $newdate . '.pdf';

				$name = 'Invoice_' . $newdate . '.pdf';
				$mail->AddAttachment($path, $name);

				$mail->Body = $bodyContent;

				if (!$mail->Send()) {
					Yii::app()->user->setFlash('error', 'There was an error sending the message!');
					$this->redirect(array('invoice/admin'));
				} else {
					unlink($path);
					Yii::app()->user->setFlash('success', 'Message was sent successfully.');
					$this->redirect(array('invoice/admin'));
				}
			} else {
				Yii::app()->user->setFlash('error', 'Email ID does not exist!');
				$this->redirect(array('invoice/admin'));
			}
		} else {

			Yii::app()->user->setFlash('error', 'Attachment does not exist!');
			$this->redirect(array('invoice/admin'));
		}
	}

	public function actiongetClient()
	{
		if (!empty($_POST["project_id"])) {
			$tblpx = Yii::app()->db->tablePrefix;
			$sql = Yii::app()->db->createCommand("SELECT {$tblpx}clients.* FROM {$tblpx}projects INNER JOIN {$tblpx}clients ON {$tblpx}clients.cid= {$tblpx}projects.client_id WHERE {$tblpx}projects.pid=" . $_POST["project_id"] . "")->queryRow();
			if (!empty($sql)) {
				$result['id'] = $sql['cid'];
				$result['name'] = $sql['name'];
				$result['status'] = 'success';
			} else {
				$result['status'] = 'error';
			}
			echo json_encode($result);
		}
	}

	public function actiondynamicproject()
	{
		$company_id = $_POST['company_id'];
		$tblpx = Yii::app()->db->tablePrefix;
		// $html  = '';
		// $html['html'] = '';
		// $html['status'] = '';
		$expense_type  = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}projects WHERE FIND_IN_SET(" . $company_id . ",company_id)")->queryAll();
		if (!empty($expense_type)) {
			$html['html'] = '<option value="">Select Project</option>';
			foreach ($expense_type as $key => $value) {
				$html['html'] .= '<option value="' . $value['pid'] . '">' . $value['name'] . '</option>';
				$html['status'] = 'success';
			}
		} else {
			$html['status'] = 'no_success';
			$html['html'] .= '<option value="">Select Project</option>';
		}
		echo json_encode($html);
	}

	public function actionUpdateinvoicestatus()
	{
		$tblpx = Yii::app()->db->tablePrefix;
		$data  = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}invoice WHERE invoice_id='" . $_REQUEST['invoice_id'] . "' ")->queryRow();
		$mail_array = array();
		if ($data['invoice_status'] == 'draft') {

			$project = Projects::model()->findByPk($data['project_id']);
			$clients = Clients::model()->findByPk($data['client_id']);
			// $amount  = $data['amount'] + $data['tax_amount'];
			// $compamy = Company::model()->findByPk($data['company_id']);
			// if ($compamy->invoice_email_userid != NULL) {
			// 	$user_email  = Yii::app()->db->createCommand("SELECT email FROM {$tblpx}users WHERE userid IN (" . $compamy->invoice_email_userid . ")")->queryAll();
			// } else {
			// 	$user_email = array();
			// }

			// $useremail = array_map('array_filter', $user_email);
			// $useremail = array_filter($useremail);
		/* Mail comment
			$mail = new JPhpMailer();

			$subject = "" . Yii::app()->name . " :Invoice Alert";
			$headers = "" . Yii::app()->name . "";
			if (!empty($clients)) {
				$client_name = $clients->name;
			} else {
				$client_name = '';
			}
			$bodyContent =  "<p>Hi</p><p>Please find the details.</p><p>Invoice No : " . $data['inv_no'] . "</p><p>Project : " . $project->name . "</p><p>Clients : " . $client_name . "</p><p>Amount : " . $amount . "</p></p><br><br><p>Regards,</p><p>" . Yii::app()->name . "</p>";

			$server = (($_SERVER['HTTP_HOST'] == 'localhost' || $_SERVER['HTTP_HOST'] == 'bhi.localhost.com') ? '0' : '1');

			if ($server == 0) {
				$mail->IsSMTP();
				$mail->Host = SMTPHOST;
				$mail->SMTPSecure = SMTPSECURE;
				$mail->SMTPAuth = true;
				$mail->Username = SMTPUSERNAME;
				$mail->Password = SMTPPASS;
			}

			$mail->setFrom(EMAILFROM, Yii::app()->name);
			if (!empty($useremail)) {
				foreach ($useremail as $key => $value) {
					if ($value['email'] != '') {
						array_push($mail_array, '1');
						$mail->addAddress($value['email']);
					}
				}
			} else {
				$user = Users::model()->findByPk(Yii::app()->user->id);
				if ($user['email'] != '') {
					array_push($mail_array, '1');
					$mail->addAddress($user['email']);
				} else {
					array_push($mail_array, '0');
				}
			}
			$mail->isHTML(true);
			$mail->Subject = "" . Yii::app()->name . " : Invoice Alert!";
			$mail->Body = $bodyContent;

			if (in_array("1", $mail_array)) {
				if ($mail->Send()) {
					$update = Yii::app()->db->createCommand("UPDATE {$tblpx}invoice SET invoice_status = 'saved' WHERE invoice_id = '" . $_REQUEST['invoice_id'] . "'")->execute();
					echo json_encode(array('response' => 'success', 'msg' => 'Invoice status changed to saved'));
				} else {
					echo json_encode(array('response' => 'error', 'msg' => 'some problem occured'));
				}
			} else {
				echo json_encode(array('response' => 'warning', 'msg' => 'Invoice notification Email ID not set'));
			}
		Mail comment */
		//$transaction = Yii::app()->db->beginTransaction();
		$result=array();
		try{
			$update = Yii::app()->db->createCommand("UPDATE {$tblpx}invoice SET invoice_status = 'saved' WHERE invoice_id = '" . $_REQUEST['invoice_id'] . "'")->execute();
			//PMS notification starts here
			if(ANCAPMS_LINKED){
				if($data['pms_invoice_id']>0 && $data['invoice_status']=='draft'){
					$request_data = array(
						'pms_invoice_id' => $data['pms_invoice_id'],
						'account_invoice_id' =>$data['invoice_id'],
						'status'=>'87'
					);
					$slug ='api/invoice-notification';
					$method = 'POST';
					$globalapi_response = GlobalApiCall::callApi($slug, $method,$request_data);
					$api_request_log =new ApiRequestLog();
					$api_request_log->project_id =$data['project_id'];
					$api_request_log->type ='POST';
					$api_request_log->request =json_encode($request_data);
					$api_request_log->response =json_encode($globalapi_response);
					$api_request_log->created_by =Yii::app()->user->id;
					$api_request_log->created_date = date('Y-m-d H:i:s');
					$api_request_log->updated_by =Yii::app()->user->id;
					$api_request_log->updated_date =date('Y-m-d H:i:s');
					if(!$api_request_log->save()){
						throw new Exception($api_request_log->getErrors());					
					}
					if ($globalapi_response['success'] !=true ) {//&& $globalapi_response['status_code']!=201
						throw new Exception(json_encode($globalapi_response));
					}	
				}
			}
			//PMS notification ends
			if($update){
				$result['response']='success';
				$result['msg']='Invoice status changed to saved';
			} else {
				throw new Exception('Error Ocured in Status Update');					
			}
			//$transaction->commit();
		}catch(Exception $e) {
			$result['response']='error';
			$result['msg']=$e->getMessage();
			//$transaction->rollback();
		}
		echo json_encode($result);
		} else {
			echo json_encode(array('response' => 'warning', 'msg' => 'Invoice status already updated'));
		}


	}

	public function actioninvoicelumpsum()
	{
		
		$tblpx      = Yii::app()->db->tablePrefix;
		$quotation_model='';
		$stageId = isset($_REQUEST['stageId'])?$_REQUEST['stageId']:"";
		$quotationId = isset($_REQUEST['quotationId'])?$_REQUEST['quotationId']:"";
		$company_id='';
		if(!empty($quotationId)){
			$quotation_model = Quotation::model()->findByPk($quotationId);
			$company_id=$quotation_model["company_id"];
		}
		
		// echo "<pre>";print_r($quotation_model);exit;
		// die($quotationId);
				$month = date('m');
				$months = array("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L");
				$prefix = '';
				$new_invoice_no = '';
				$i = 1;
				foreach ($months as $key => $value) {
					if ($month == $i) {
						$prefix = $value;
					}
					$i++;
				}

				$sql = "SELECT MAX(CAST(invoice_id as signed)) as largenumber "
						. " FROM {$tblpx}invoice WHERE MONTH(created_date) = MONTH(CURRENT_DATE()) "
						. " AND YEAR(created_date) = YEAR(CURRENT_DATE())";
						//echo "<pre>";print_r($sql);exit;
				$maxData = Yii::app()->db->createCommand($sql)->queryRow();
				$maxNo   = $maxData["largenumber"];
				$newPONo = $maxNo + 1;
				$prNo   = $newPONo;
				$digit = strlen((string) $prNo);
				$activeProjectTemplate = $this->getActiveTemplate();
				if ($this->getActiveTemplate() == 'TYPE-4' && ENV =='production') {
					$invoice_no =$this->getArchInvoiceNo();
						
				}else{
					if ($digit == 1) {
						$invoice_no = $prefix . '00' . $prNo;
					} else if ($digit == 2) {
						$invoice_no = $prefix . '0' . $prNo;
					} else {
						$invoice_no = $prefix . $prNo;
					}
				}
		
		
		if(!empty($company_id)){
			$company = Company::model()->findByPK($company_id);
			if($company["invoice_format"] != NULL){
				
				$invoice_format =trim($company["invoice_format"]);
			$general_inv_format_arr = explode('/',$invoice_format);
			// print_r($general_inv_format_arr);exit;
			$org_prefix = $general_inv_format_arr[0];
			$prefix_key = 0;
			$invoice_key= '';
			$financial_key ='';
			$year_key = '';
			foreach($general_inv_format_arr as $key =>$value){
				
				if($value == '{invoice_sequence}'){
					$invoice_key = $key;
				}else if ($value == '{financial_year}'){
					$financial_key =$key;
				}else if($value == '{year}'){
					$year_key = $key;
				}
			}
			
			$current_year = date('Y');
			$previous_year = date('Y') -1;
			$next_year = date('Y') +1;
			$financial_year = '';
			$where = '';
			if($financial_key != ''){
				if($month > 3){
					$financial_year = $current_year . '-' .$next_year; 
				}else {
					$financial_year = $previous_year . '-' .$current_year; 
				}
				if($financial_key == 2){
					$where .= " AND `inv_no` LIKE '%$financial_year'";
				}else if($financial_key == 1){
					$where .= " AND `inv_no` LIKE '%$financial_year%'";
				}
			}
			if($year_key != ''){
				$where .= " AND `inv_no` LIKE '%$current_year%'";
			}
			
			
			
			$sql = "SELECT * from ".$tblpx."invoice  WHERE `inv_no` LIKE '$org_prefix%' $where  AND company_id = ".$company_id."   ORDER BY `invoice_id` DESC LIMIT 1;" ;
			//echo "<pre>";print_r($sql);exit;
			$previous_inv_no='';
			$invoice_seq_no = 1;
			$previous_data = Yii::app()->db->createCommand($sql)->queryRow();
			if( !empty($previous_data) ){
				$previous_inv_no = $previous_data['inv_no'];
				$prefix_arr = explode('/', $previous_inv_no);
				$prefix = $prefix_arr[0];
				$previous_prefix_seq_no = $prefix_arr[$invoice_key];
				$invoice_seq_no = $previous_prefix_seq_no + 1;
				
			}
			$digit = strlen((string) $invoice_seq_no);
			if ($this->getActiveTemplate() == 'TYPE-4' && ENV =='production') {
				$invoice_no =$this->getArchInvoiceNo();
					
			}else{
				if ($digit == 1) {
					$invoice_no ='00' .$invoice_seq_no;
				} else if ($digit == 2) {
					$invoice_no = '0' . $invoice_seq_no;
				} else {
					$invoice_no =$invoice_seq_no;
				}
			}
			$new_invoice = str_replace('{financial_year}',$financial_year,$invoice_format);
			$new_invoice_yr = str_replace('{year}',$current_year,$new_invoice);
			$new_invoice_no = str_replace('{invoice_sequence}',$invoice_no,$new_invoice_yr);
			$sql = "SELECT * from ".$tblpx."invoice  WHERE `inv_no` = '$new_invoice_no'  ;" ;
			
				$duplicate_data = Yii::app()->db->createCommand($sql)->queryRow();
				if(!empty($duplicate_data)){
					
					$previous_inv_no='';
					$invoice_seq_no = 1;
					$previous_inv_no = $duplicate_data['inv_no'];
					$prefix_arr = explode('/', $previous_inv_no);
					$prefix = $prefix_arr[0];
					$previous_prefix_seq_no_s = $prefix_arr[$invoice_key];
					$previous_prefix_seq_no_s = ltrim($previous_prefix_seq_no_s, '0');
					
					$invoice_seq_no = $previous_prefix_seq_no_s + 1;
					
					$digit = strlen((string) $invoice_seq_no);
				if ($this->getActiveTemplate() == 'TYPE-4' && ENV =='production') {
					$invoice_no =$this->getArchInvoiceNo();
						
				}else{
					if ($digit == 1) {
						$invoice_no ='00' .$invoice_seq_no;
					} else if ($digit == 2) {
						$invoice_no = '0' . $invoice_seq_no;
					} else {
						$invoice_no =$invoice_seq_no;
					}
				}
				$new_invoice = str_replace('{financial_year}',$financial_year,$invoice_format);
				$new_invoice_yr = str_replace('{year}',$current_year,$new_invoice);
				$new_invoice_no = str_replace('{invoice_sequence}',$invoice_no,$new_invoice_yr);
				}
				$sql = "SELECT * from ".$tblpx."invoice  WHERE `inv_no` = '$new_invoice_no'  ;" ;
			
				$duplicate_data = Yii::app()->db->createCommand($sql)->queryRow();
				if(!empty($duplicate_data)){
					
					$previous_inv_no='';
					$invoice_seq_no = 1;
					$previous_inv_no = $duplicate_data['inv_no'];
					$prefix_arr = explode('/', $previous_inv_no);
					$prefix = $prefix_arr[0];
					$previous_prefix_seq_no_s = $prefix_arr[$invoice_key];
					$previous_prefix_seq_no_s = ltrim($previous_prefix_seq_no_s, '0');
					
					$invoice_seq_no = $previous_prefix_seq_no_s + 1;
					
					$digit = strlen((string) $invoice_seq_no);
				if ($this->getActiveTemplate() == 'TYPE-4' && ENV =='production') {
					$invoice_no =$this->getArchInvoiceNo();
						
				}else{
					if ($digit == 1) {
						$invoice_no ='00' .$invoice_seq_no;
					} else if ($digit == 2) {
						$invoice_no = '0' . $invoice_seq_no;
					} else {
						$invoice_no =$invoice_seq_no;
					}
				}
				$new_invoice = str_replace('{financial_year}',$financial_year,$invoice_format);
				$new_invoice_yr = str_replace('{year}',$current_year,$new_invoice);
				$new_invoice_no = str_replace('{invoice_sequence}',$invoice_no,$new_invoice_yr);
				
				}
				$invoice_no='';
				$invoice_no=$new_invoice_no;

			}
		}
		
		
		$model = new Invoice;
		$newmodel = new InvList;
		
		
		$user = Users::model()->findByPk(Yii::app()->user->id);
		$arrVal = explode(',', $user->company_id);
		$newQuery = "";
		$newQuery1 = "";
		foreach ($arrVal as $arr) {
			if ($newQuery) $newQuery .= ' OR';
			if ($newQuery1) $newQuery1 .= ' OR';
			$newQuery .= " FIND_IN_SET('" . $arr . "', company_id)";
			$newQuery1 .= " FIND_IN_SET('" . $arr . "', p.company_id)";
		}
		if (Yii::app()->user->role == 1 || Yii::app()->user->role == 2) {
			$project    = Yii::app()->db->createCommand("SELECT pid, name  FROM {$tblpx}projects WHERE " . $newQuery . " ORDER BY name")->queryAll();
		} else {
			$project    = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}projects p JOIN {$tblpx}project_assign pa ON p.pid = pa.projectid WHERE " . $newQuery1 . " AND pa.userid = '" . Yii::app()->user->id . "'")->queryAll();
		}
		$client     = Yii::app()->db->createCommand("SELECT DISTINCT cid, name FROM {$tblpx}clients ORDER BY name")->queryAll();
		$this->render('invoicelumpsum', array(
			'model' => $model,
			'newmodel' => $newmodel,
			'project' => $project,
			'client' => $client,
			'invoice_no' => $invoice_no,
			'stageId'=>$stageId, 
			'quotationId'=>$quotationId 

		));
	}

	public function actionInvoicelumpsumitem()
	{
		$data = $_REQUEST['data'];

		$result = array('html' => '');
		// $result['html'] = '';
		$html = '';
		if ($data['invoice_id'] == 0) {
			echo json_encode(array('response' => 'error', 'msg' => 'Please enter sales book details'));
		} else {
			$subtot = $data['subtot'];
			$grand = $data['grand'];
			$grand_tax = $data['grand_tax'];
			if (isset($data['sl_No'])) {
				$sl_No = $data['sl_No'];
			} else {
				$sl_No = '';
			}
			if (isset($data['description'])) {
				$description = $data['description'];
			} else {
				$description = '';
			}

			if (isset($data['hsn_code'])) {
				$hsn_code = $data['hsn_code'];
			} else {
				$hsn_code = '';
			}
			if (isset($data['amount'])) {
				$amount = $data['amount'];
			} else {
				$amount  = '';
			}

			if (isset($data['sgst'])) {
				$sgst = $data['sgst'];
			} else {
				$sgst  = '';
			}

			if (isset($data['sgst_amount'])) {
				$sgst_amount = $data['sgst_amount'];
			} else {
				$sgst_amount  = 0.00;
			}

			if (isset($data['cgst'])) {
				$cgst = $data['cgst'];
			} else {
				$cgst  = '';
			}

			if (isset($data['cgst_amount'])) {
				$cgst_amount = $data['cgst_amount'];
			} else {
				$cgst_amount  = 0.00;
			}

			if (isset($data['igst'])) {
				$igst = $data['igst'];
			} else {
				$igst  = '';
			}

			if (isset($data['igst_amount'])) {
				$igst_amount = $data['igst_amount'];
			} else {
				$igst_amount  = 0.00;
			}

			if (isset($data['tax_amount'])) {
				$tax_amount = $data['tax_amount'];
			} else {
				$tax_amount  = '';
			}

			$item_model = new InvList();
			$item_model->type = $data['type'];
			$item_model->inv_id = $data['invoice_id'];
			$item_model->amount = $amount;
			$item_model->description = $description;
			$item_model->hsn_code = $hsn_code;
			$item_model->cgst = $cgst;
			$item_model->cgst_amount = $cgst_amount;
			$item_model->sgst = $sgst;
			$item_model->sgst_amount = $sgst_amount;
			$item_model->igst = $igst;
			$item_model->igst_amount = $igst_amount;
			$item_model->tax_amount = $tax_amount;
			$item_model->created_date = date('Y-m-d');
			$item_model->hours = (isset($data['hours']))?$data['hours']:'';
			$item_model->worktype_id = (isset($data['worktype_id']))?$data['worktype_id']:'';
			$item_model->ratecard_id = (isset($data['ratecard_id']))?$data['ratecard_id']:'';
			if ($item_model->save()) {
				$last_id = $item_model->id;
				$tblpx = Yii::app()->db->tablePrefix;
				$data1 = Yii::app()->db->createCommand("SELECT SUM(amount) as qt_amount FROM {$tblpx}inv_list WHERE inv_id=" . $data['invoice_id'] . "")->queryRow();
				$data2 = Yii::app()->db->createCommand("SELECT SUM(tax_amount) as qt_taxamount FROM {$tblpx}inv_list WHERE inv_id=" . $data['invoice_id'] . "")->queryRow();
				$model = Invoice::model()->findByPk($data['invoice_id']);
				$model->amount = $data1['qt_amount'];
				$model->subtotal = $data1['qt_amount'];
				$model->tax_amount = $data2['qt_taxamount'];
				if ($model->save()) {
					$result = '';
					$invoice_details  = Yii::app()->db->createCommand(" SELECT * FROM {$tblpx}inv_list  WHERE inv_id=" . $data['invoice_id'] . "")->queryAll();
					foreach ($invoice_details as $key => $values) {
						$result .= '<tr>';
						$result .= '<td><div id="item_sl_no">' . ($key + 1) . '</div></td>';
						$result .= '<td><div class="item_description">' . $values['description'] . '</div> </td>';
						$result .= '<td><div class="item_hsn_code">' . $values['hsn_code'] . '</div> </td>';
						if($values['type'] == 0){
							$result .= '<td id="'.$values['type'].'"><div class="type">Lumpsum</div> </td>';
						}else{
							$result .= '<td id="'.$values['type'].'"><div class="type">Quantity * Rate</div> </td>';
						}
						$result .= '<td>' . $values['quantity'] . '</td>';
						$result .= '<td>' . $values['unit'] . '</td>';
						$result .= '<td>' . $values['rate'] . '</td>';
						$result .= '<td class="text-right">' . Yii::app()->Controller->money_format_inr($values['sgst'], 2) . '</td>';
						$result .= '<td class="text-right">' . Yii::app()->Controller->money_format_inr($values['sgst_amount'], 2) . '</td>';
						$result .= '<td class="text-right">' . Yii::app()->Controller->money_format_inr($values['cgst'], 2) . '</td>';
						$result .= '<td class="text-right">' . Yii::app()->Controller->money_format_inr($values['cgst_amount'], 2) . '</td>';
						$result .= '<td class="text-right">' . Yii::app()->Controller->money_format_inr($values['igst'], 2) . '</td>';
						$result .= '<td class="text-right">' . Yii::app()->Controller->money_format_inr($values['igst_amount'], 2) . '</td>';
						if(ANCAPMS_LINKED){
							$worktype=WorkType::model()->findByPk($values['worktype_id']);
							$ratecard=RateCard::model()->findByPk($values['ratecard_id']);
							$result .= '<td class="text-right">' . $values['hours'] . '</td>';
							$result .= '<td class="text-right">' . $worktype['work_type']. '</td>';
							$result .= '<td class="text-right">' . $ratecard['rate'] . '</td>';
						}
						$result .= '<td class="text-right"><div class="" id="amount"> ' . Yii::app()->Controller->money_format_inr($values['amount'], 2) . '</div></td>';
						$result .= '<td class="text-right">' . Yii::app()->Controller->money_format_inr($values['tax_amount'], 2) . '</td>';
						$edit_class= ($values['type']==0)?"edit_item_lumpsum":"edit_item";
						$result .= '<td width="60"><span class="icon icon-options-vertical popover-test" data-toggle="popover"   data-placement="left" type="button" data-html="true" style="cursor: pointer;" ></span>
                            <div class="popover-content hide">
                                <ul class="tooltip-hiden">
                                    <li><a href="#" id=' . $values['id'] . ' class="btn btn-xs btn-default removebtn">Delete</a></li> <li><a href="" id=' . $values['id'] . ' class="btn btn-xs btn-default '.$edit_class.'">Edit</a></li></ul></div></td>';

						$result .= '</tr>';
					}
				}
				if(ANCAPMS_LINKED){
					$invalidRecords = InvList::model()->findAll(
						'inv_id = :invoiceId AND (hours IS NULL OR worktype_id IS NULL OR ratecard_id IS NULL)',
						array(':invoiceId' => $data['invoice_id'])
					);
					if (empty($invalidRecords)) {
						$invoiceModel = Invoice::model()->findByPk($data['invoice_id']);
						if ($invoiceModel !== null) {
							$invoiceModel->status_id = null;
							$invoiceModel->save();
						}
					}
				}
				echo json_encode(array('response' => 'success', 'msg' => 'Sales item save successfully', 'item_id' => $last_id, 'html' => $result, 'final_amount' => number_format($data1['qt_amount'], 2), 'final_tax' => number_format($data2['qt_taxamount'], 2), 'grand_total' => number_format($data2['qt_taxamount'] + $data1['qt_amount'], 2),'grand_total_number'=>($data2['qt_taxamount'] + $data1['qt_amount'])));
			} else {
				echo json_encode(array('response' => 'error', 'msg' => 'Some problem occured'));
			}
		}
	}

	public function actionUpdateslumpsumitem()
	{
		$data = $_REQUEST['data'];
		$grand = $data['grand'];
		$subtot = $data['subtot'];
		$grand_tax = $data['grand_tax'];
		//echo "<pre>";print_r($data);exit;
		if (isset($data['description'])) {
			$description = $data['description'];
		} else {
			$description = '';
		}
		if (isset($data['hsn_code'])) {
			$hsn_code = $data['hsn_code'];
		} else {
			$hsn_code = '';
		}
		if (isset($data['amount'])) {
			$amount = $data['amount'];
		} else {
			$amount  = '';
		}

		if (isset($data['sgst'])) {
			$sgst = $data['sgst'];
		} else {
			$sgst  = '';
		}

		if (isset($data['sgst_amount'])) {
			$sgst_amount = trim($data['sgst_amount']);
		} else {
			$sgst_amount  = 0;
		}

		if (isset($data['cgst'])) {
			$cgst = $data['cgst'];
		} else {
			$cgst  = '';
		}

		if (isset($data['cgst_amount'])) {
			$cgst_amount = trim($data['cgst_amount']);
		} else {
			$cgst_amount  = 0;
		}
		
		if (isset($data['igst'])) {
			$igst = $data['igst'];
		} else {
			$igst  = '';
		}

		if (isset($data['igst_amount'])) {
			$igst_amount = trim($data['igst_amount']);
		} else {
			$igst_amount  = 0;
		}

		if (isset($data['tax_amount'])) {
			$tax_amount = $data['tax_amount'];
		} else {
			$tax_amount  = '';
		}
		$result = '';
		$html = '';
		$tblpx = Yii::app()->db->tablePrefix;
		
		
		if(ANCAPMS_LINKED){
			$hours = isset($data['hours'])?($data['hours']):'';
			$worktype_id = isset($data['worktype_id'])?($data['worktype_id']):'';
			$ratecard_id = isset($data['ratecard_id'])?($data['ratecard_id']):'';
		}

		if (!empty($data) && $data['item_id'] != 0) {
			$item_model = InvList::model()->findByPk($data['item_id']);
			$item_model->amount = $amount;
			$item_model->description = $description;
			$item_model->hsn_code = $hsn_code;
			$item_model->cgst = $cgst;
			$item_model->cgst_amount = $cgst_amount;
			$item_model->sgst = $sgst;
			$item_model->sgst_amount = $sgst_amount;
			$item_model->igst = $igst;
			$item_model->igst_amount = $igst_amount;
			$item_model->tax_amount = $tax_amount;
			if(ANCAPMS_LINKED){
				$item_model->hours = $hours;
				$item_model->ratecard_id = $ratecard_id;
				$item_model->worktype_id = $worktype_id;
			}
			if ($item_model->save()) {

				$last_id = $item_model->id;
				$tblpx = Yii::app()->db->tablePrefix;
				$data1 = Yii::app()->db->createCommand("SELECT SUM(amount) as qt_amount FROM {$tblpx}inv_list WHERE inv_id=" . $data['invoice_id'] . "")->queryRow();
				$data2 = Yii::app()->db->createCommand("SELECT SUM(tax_amount) as qt_taxamount FROM {$tblpx}inv_list WHERE inv_id=" . $data['invoice_id'] . "")->queryRow();
				$model = Invoice::model()->findByPk($data['invoice_id']);
				$model->amount = $data1['qt_amount'];
				$model->subtotal = $data1['qt_amount'];
				$model->tax_amount = $data2['qt_taxamount'];
				if(isset($data['inv_no'])){
					
					$inv_no =$data['inv_no'];
					$model->inv_no= $inv_no;
					
				}
				if(ANCAPMS_LINKED && $model->company_id==null){
					$model->company_id=Projects::model()->findByPk($model->project_id)['company_id'];
				}
				if ($model->save()) {
					$result = '';
					$invoice_details  = Yii::app()->db->createCommand(" SELECT * FROM {$tblpx}inv_list  WHERE inv_id=" . $data['invoice_id'] . "")->queryAll();
					foreach ($invoice_details as $key => $values) {
						$result .= '<tr>';
						$result .= '<td><div id="item_sl_no">' . ($key + 1) . '</div></td>';
						$result .= '<td><div class="item_description">' . $values['description'] . '</div> </td>';
						$result .= '<td><div class="item_hsn_code">' . $values['hsn_code'] . '</div> </td>';
						
						$result .= '<td><div class="quantity">' . $values['quantity'] . '</div> </td>';
						$result .= '<td><div class="unit">' . $values['unit'] . '</div> </td>';
						$result .= '<td><div class="rate">' . $values['rate'] . '</div> </td>';
						if($values['type'] == 0){
							$result .= '<td id="'.$values['type'].'"><div class="type">Lumpsum</div> </td>';
						}else{
							$result .= '<td id="'.$values['type'].'"><div class="type">Quantity * Rate</div> </td>';
						}
						
						$result .= '<td class="text-right">' . $values['sgst'] . '</td>';
						$result .= '<td class="text-right">' . number_format($values['sgst_amount'], 2, '.', '') . '</td>';
						$result .= '<td class="text-right">' . $values['cgst'] . '</td>';
						$result .= '<td class="text-right">' . number_format($values['cgst_amount'], 2, '.', '') . '</td>';
						$result .= '<td class="text-right">' . $values['igst'] . '</td>';
						$result .= '<td class="text-right">' . number_format($values['igst_amount'], 2, '.', '') . '</td>';
						if(ANCAPMS_LINKED){
							$worktype=WorkType::model()->findByPk($values['worktype_id']);
							$ratecard=RateCard::model()->findByPk($values['ratecard_id']);
							$result .= '<td class="text-right">' . $values['hours'] . '</td>';
							$result .= '<td class="text-right">' . $worktype['work_type']. '</td>';
							$result .= '<td class="text-right">' . $ratecard['rate'] . '</td>';
						}
						$result .= '<td class="text-right"><div class="" id="amount"> ' . number_format($values['amount'], 2, '.', '') . '</div></td>';
						$result .= '<td class="text-right">' . number_format($values['tax_amount'], 2, '.', '') . '</td>';

						$edit_class= ($values['type']==0)?"edit_item_lumpsum":"edit_item";

						$result .= '<td width="60"><span class="icon icon-options-vertical popover-test" data-toggle="popover"   data-placement="left" type="button" data-html="true" style="cursor: pointer;" ></span>
                            <div class="popover-content hide">
                                <ul class="tooltip-hiden">
                                                                       
                                    <li><a href="#" id=' . $values['id'] . ' class="btn btn-xs btn-default removebtn">Delete</a></li> <li><a href="" id=' . $values['id'] . ' class="btn btn-xs btn-default '.$edit_class.'">Edit</a></li></ul></div></td>';

						$result .= '</tr>';
					}
					if(ANCAPMS_LINKED){
						$invalidRecords = InvList::model()->findAll(
							'inv_id = :invoiceId AND (hours IS NULL OR worktype_id IS NULL OR ratecard_id IS NULL)',
							array(':invoiceId' => $data['invoice_id'])
						);
						if (empty($invalidRecords)) {
							$invoiceModel = Invoice::model()->findByPk($data['invoice_id']);
							if ($invoiceModel !== null) {
								$invoiceModel->status_id = null;
								$invoiceModel->save();
							}
						}
					}
				}

				echo json_encode(array('response' => 'success', 'msg' => 'Sales item save successfully', 'item_id' => $last_id, 'html' => $result, 'final_amount' => number_format($data1['qt_amount'], 2), 'final_tax' => number_format($data2['qt_taxamount'], 2), 'grand_total' => number_format($data2['qt_taxamount'] + $data1['qt_amount'], 2),'grand_total_number'=>($data2['qt_taxamount'] + $data1['qt_amount'])));
			} else {
				echo json_encode(array('response' => 'error', 'msg' => 'Some problem occured'));
			}
		}
	}

	public function actionUpdateinvoicelumpsum($invid)
	{
		$model 		=  Invoice::model()->find(array("condition" => "invoice_id = '$invid'"));
		$newmodel = new InvList;

		if (empty($model)) {
			$this->redirect(array('invoice/admin'));
		}
		
		$item_model = InvList::model()->findAll(array("condition" => "inv_id = '$invid'"));
		$tblpx		= Yii::app()->db->tablePrefix;
		$project 	= Projects::model()->findByPK($model->project_id);		
		$client     = Clients::model()->findByPK($model->client_id);
		$new_invoice_no=$model->inv_no;
		$org_prefix_slash='';
		if(!empty($model)){
			if(ANCAPMS_LINKED && $model->status_id==108){
				$company_id = $project->company_id;
			$company = Company::model()->findByPK($company_id);
			//echo "<pre>";print_r($company);exit;
			$invoice_no = '';
			$month = date('m');//04
			$tblpx = Yii::app()->db->tablePrefix;
			if($company["invoice_format"] != NULL){
				$invoice_format =trim($company["invoice_format"]);
				$general_inv_format_arr = explode('/',$invoice_format);
				// print_r($general_inv_format_arr);exit;
				$org_prefix = $general_inv_format_arr[0];
				$org_prefix_slash=$general_inv_format_arr[0].'/';
				$prefix_key = 0;
				$invoice_key= '';
				$financial_key ='';
				$year_key = '';
				foreach($general_inv_format_arr as $key =>$value){
					
					if($value == '{invoice_sequence}'){
						$invoice_key = $key;
					}else if ($value == '{financial_year}'){
						$financial_key =$key;
					}else if($value == '{year}'){
						$year_key = $key;
					}
				}
				
				$current_year = date('Y');
				$previous_year = date('Y') -1;
				$next_year = date('Y') +1;
				$financial_year = '';
				$where = '';
				if($financial_key != ''){
					if($month > 3){
						$financial_year = $current_year . '-' .$next_year; 
					}else {
						$financial_year = $previous_year . '-' .$current_year; 
					}
					if($financial_key == 2){
						$where .= " AND `inv_no` LIKE '%$financial_year'";
					}else if($financial_key == 1){
						$where .= " AND `inv_no` LIKE '%$financial_year%'";
					}
				}
				if($year_key != ''){
					$where .= " AND `inv_no` LIKE '%$current_year%'";
				}
				
				
				
				$sql = "SELECT * from ".$tblpx."invoice  WHERE `inv_no` LIKE '$org_prefix_slash%' $where  AND  company_id = ". $company_id." ORDER BY `invoice_id` DESC LIMIT 1;" ;
				$previous_inv_no='';
				$invoice_seq_no = 1;
				$previous_data = Yii::app()->db->createCommand($sql)->queryRow();
				// echo "<pre>";print_r($previous_data);exit;
				if( !empty($previous_data) ){
					$previous_inv_no = $previous_data['inv_no'];
					$prefix_arr = explode('/', $previous_inv_no);
					$prefix = $prefix_arr[0];
					$previous_prefix_seq_no = $prefix_arr[$invoice_key];
					$invoice_seq_no = $previous_prefix_seq_no + 1;
					
				}
				$digit = strlen((string) $invoice_seq_no);
				if ($this->getActiveTemplate() == 'TYPE-4' && ENV =='production') {
					$invoice_no =$this->getArchInvoiceNo();
						
				}else{
					if ($digit == 1) {
						$invoice_no ='00' .$invoice_seq_no;
					} else if ($digit == 2) {
						$invoice_no = '0' . $invoice_seq_no;
					} else {
						$invoice_no =$invoice_seq_no;
					}
				}
				$new_invoice = str_replace('{financial_year}',$financial_year,$invoice_format);
				$new_invoice_yr = str_replace('{year}',$current_year,$new_invoice);
				$new_invoice_no = str_replace('{invoice_sequence}',$invoice_no,$new_invoice_yr);
				$sql = "SELECT * from ".$tblpx."invoice  WHERE `inv_no` = '$new_invoice_no'  ;" ;
			
				$duplicate_data = Yii::app()->db->createCommand($sql)->queryRow();
				if(!empty($duplicate_data)){
					
					$previous_inv_no='';
					$invoice_seq_no = 1;
					$previous_inv_no = $duplicate_data['inv_no'];
					$prefix_arr = explode('/', $previous_inv_no);
					$prefix = $prefix_arr[0];
					$previous_prefix_seq_no_s = $prefix_arr[$invoice_key];
					$previous_prefix_seq_no_s = ltrim($previous_prefix_seq_no_s, '0');
					
					$invoice_seq_no = $previous_prefix_seq_no_s + 1;
					
					$digit = strlen((string) $invoice_seq_no);
				if ($this->getActiveTemplate() == 'TYPE-4' && ENV =='production') {
					$invoice_no =$this->getArchInvoiceNo();
						
				}else{
					if ($digit == 1) {
						$invoice_no ='00' .$invoice_seq_no;
					} else if ($digit == 2) {
						$invoice_no = '0' . $invoice_seq_no;
					} else {
						$invoice_no =$invoice_seq_no;
					}
				}
				$new_invoice = str_replace('{financial_year}',$financial_year,$invoice_format);
				$new_invoice_yr = str_replace('{year}',$current_year,$new_invoice);
				$new_invoice_no = str_replace('{invoice_sequence}',$invoice_no,$new_invoice_yr);
				}
				$sql = "SELECT * from ".$tblpx."invoice  WHERE `inv_no` = '$new_invoice_no'  ;" ;
			
				$duplicate_data = Yii::app()->db->createCommand($sql)->queryRow();
				if(!empty($duplicate_data)){
					
					$previous_inv_no='';
					$invoice_seq_no = 1;
					$previous_inv_no = $duplicate_data['inv_no'];
					$prefix_arr = explode('/', $previous_inv_no);
					$prefix = $prefix_arr[0];
					$previous_prefix_seq_no_s = $prefix_arr[$invoice_key];
					$previous_prefix_seq_no_s = ltrim($previous_prefix_seq_no_s, '0');
					
					$invoice_seq_no = $previous_prefix_seq_no_s + 1;
					
					$digit = strlen((string) $invoice_seq_no);
				if ($this->getActiveTemplate() == 'TYPE-4' && ENV =='production') {
					$invoice_no =$this->getArchInvoiceNo();
						
				}else{
					if ($digit == 1) {
						$invoice_no ='00' .$invoice_seq_no;
					} else if ($digit == 2) {
						$invoice_no = '0' . $invoice_seq_no;
					} else {
						$invoice_no =$invoice_seq_no;
					}
				}
				$new_invoice = str_replace('{financial_year}',$financial_year,$invoice_format);
				$new_invoice_yr = str_replace('{year}',$current_year,$new_invoice);
				$new_invoice_no = str_replace('{invoice_sequence}',$invoice_no,$new_invoice_yr);
				}
				
			}
		}
		}
			$this->render('invoicelumpsum_update', array(
			'model' => $model,
			'item_model' => $item_model,
			'invoice_id' => $invid,
			'project' => $project,
			'client' => $client,
			'newmodel' => $newmodel,
			'invoice_gen_no'=>$new_invoice_no,
		));
	}

	public function actionremoveinvoicelumpsum()
	{
		$data   = $_REQUEST['data'];
		$tblpx = Yii::app()->db->tablePrefix;
		$del = Yii::app()->db->createCommand()->delete($tblpx . 'inv_list', 'id=:id', array(':id' => $data['item_id']));
		if ($del) {
			$tblpx = Yii::app()->db->tablePrefix;
			$data1 = Yii::app()->db->createCommand("SELECT SUM(amount) as qt_amount FROM {$tblpx}inv_list WHERE inv_id=" . $data['invoice_id'] . "")->queryRow();
			$data2 = Yii::app()->db->createCommand("SELECT SUM(tax_amount) as qt_taxamount FROM {$tblpx}inv_list WHERE inv_id=" . $data['invoice_id'] . "")->queryRow();
			$model = Invoice::model()->findByPk($data['invoice_id']);
			$model->amount = (isset($data1['qt_amount'])) ? $data1['qt_amount'] : 0;
			$model->subtotal = (isset($data1['qt_amount'])) ? $data1['qt_amount'] : 0;;
			$model->tax_amount = (isset($data2['qt_taxamount'])) ? $data2['qt_taxamount'] : 0;
			$model->save();
			$result = '';
			$invoice_details  = Yii::app()->db->createCommand(" SELECT * FROM {$tblpx}inv_list  WHERE inv_id=" . $data['invoice_id'] . "")->queryAll();
			foreach ($invoice_details as $key => $values) {
				$result .= '<tr>';
				$result .= '<td><div id="item_sl_no">' . ($key + 1) . '</div></td>';
				$result .= '<td><div class="item_description">' . $values['description'] . '</div> </td>';
				$result .= '<td><div class="item_description">' . $values['hsn_code'] . '</div> </td>';
				$result .= '<td class="text-right"> <div class="" id="quantity"> ' . $values['quantity'] . '</div> </td>';
				$result .= '<td> <div class="unit" id="unit"> ' . $values['unit'] . '</div> </td>';
				$result .= '<td class="text-right"><div class="" id="rate">' . number_format($values['rate'], 2, '.', '') . '</div></td>';
				if($values['type'] == 0){
					$result .= '<td id="'.$values['type'].'"><div class="type">Lumpsum</div> </td>';
				}else{
					$result .= '<td id="'.$values['type'].'"><div class="type">Quantity * Rate</div> </td>';
				}
				$result .= '<td class="text-right">' . $values['sgst'] . '</td>';
				$result .= '<td class="text-right">' . number_format($values['sgst_amount'], 2, '.', '') . '</td>';
				$result .= '<td class="text-right">' . $values['cgst'] . '</td>';
				$result .= '<td class="text-right">' . number_format($values['cgst_amount'], 2, '.', '') . '</td>';
				$result .= '<td class="text-right">' . $values['igst'] . '</td>';
				$result .= '<td class="text-right">' . number_format($values['igst_amount'], 2, '.', '') . '</td>';
				if(ANCAPMS_LINKED){
					$worktype=WorkType::model()->findByPk($values['worktype_id']);
					$ratecard=RateCard::model()->findByPk($values['ratecard_id']);
					$result .= '<td class="text-right">' . $values['hours'] . '</td>';
					$result .= '<td class="text-right">' . $worktype['work_type']. '</td>';
					$result .= '<td class="text-right">' . $ratecard['rate'] . '</td>';
				}
				$result .= '<td class="text-right"><div class="" id="amount"> ' . number_format($values['amount'], 2, '.', '') . '</div></td>';
				$result .= '<td class="text-right">' . number_format($values['tax_amount'], 2, '.', '') . '</td>';

				$edit_class= ($values['type']==0)?"edit_item_lumpsum":"edit_item";

				$result .= '<td width="60"><span class="icon icon-options-vertical popover-test" data-toggle="popover"   data-placement="left" type="button" data-html="true" style="cursor: pointer;" ></span>
                                    <div class="popover-content hide">
                                        <ul class="tooltip-hiden">

                                                <li><a href="#" id=' . $values['id'] . ' class="btn btn-xs btn-default removebtn">Delete</a></li> <li><a href="" id=' . $values['id'] . ' class="btn btn-xs btn-default '.$edit_class.'">Edit</a></li></ul></div></td>';

				$result .= '</tr>';
			}
			if(ANCAPMS_LINKED){
				$invalidRecords = InvList::model()->findAll(
					'inv_id = :invoiceId AND (hours IS NULL OR worktype_id IS NULL OR ratecard_id IS NULL)',
					array(':invoiceId' => $data['invoice_id'])
				);
				if (empty($invalidRecords)) {
					$invoiceModel = Invoice::model()->findByPk($data['invoice_id']);
					if ($invoiceModel !== null) {
						$invoiceModel->status_id = null;
						$invoiceModel->save();
					}
				}
			}
			echo json_encode(array('response' => 'success', 'msg' => 'Sales deleted successfully', 'item_id' => $data['item_id'], 'html' => $result, 'final_amount' => number_format($data1['qt_amount'], 2), 'final_tax' => number_format($data2['qt_taxamount'], 2), 'grand_total' => number_format($data2['qt_taxamount'] + $data1['qt_amount'], 2),'grand_total_number'=>($data2['qt_taxamount'] + $data1['qt_amount'])));
		} else {
			echo json_encode(array('response' => 'error', 'msg' => 'Some Problem occured'));
		}
	}
	public function actionWriteoff()
	{
		if (isset($_POST['Writeoff'])) {
			if (isset($_POST['Writeoff']['id']) && !empty($_POST['Writeoff']['id'])) {
				$model = Writeoff::model()->findByPk($_POST['Writeoff']['id']);
			} else {
				$model = new Writeoff;
			}
			$model->invoice_id  = $_POST['Writeoff']['invoice_id'];
			$model->amount      = $_POST['Writeoff']['amount'];
			$model->description = $_POST['Writeoff']['description'];
			$model->status      = 1;
			if ($model->save()) {
				Yii::app()->user->setFlash('success', "Writeoff succesfully added!");
				$this->redirect(array('viewinvoice&invid=' . $_POST['Writeoff']['invoice_id']));
			} else {
				Yii::app()->user->setFlash('error', "Failed!");
				$this->redirect(array('viewinvoice&invid=' . $_POST['Writeoff']['invoice_id']));
			}
		} else {
			Yii::app()->user->setFlash('error', "Failed!");
			$this->redirect(array('viewinvoice&invid=' . $_POST['Writeoff']['invoice_id']));
		}
	}

	public function actionUpdateinvoicenew()
	{
		$new_invoice_no='';
		if (isset($_POST['company']) && isset($_POST['project']) && isset($_POST['date']) && isset($_POST['invoice_id'])) {
			$invid = $_POST['invoice_id'];
			$model 		=  Invoice::model()->find(array("condition" => "invoice_id = '$invid'"));
			$new_invoice_no =$model->inv_no;
			if(isset($_POST['inv_no']) && !empty($_POST['inv_no'])){
				$new_invoice_no=$_POST['inv_no'];
			}
			
			$model->inv_no=$new_invoice_no;
			$model->company_id = $_POST['company'];
			$model->project_id = $_POST['project'];
			$model->date = date('Y-m-d', strtotime($_POST['date']));
			$project 	= Projects::model()->findByPK($model->project_id);
			$model->client_id = $project->client_id;
		// 	if(ANCAPMS_LINKED){
		// 		$company_id=$_POST['company'];
		// 		$company = Company::model()->findByPK($company_id);
		// //echo "<pre>";print_r($company);exit;
		// $invoice_no = '';
		// $month = date('m');//04
		// $tblpx = Yii::app()->db->tablePrefix;
		// if($company["invoice_format"] != NULL){
		// 	$invoice_format =trim($company["invoice_format"]);
		// 	$general_inv_format_arr = explode('/',$invoice_format);
		// 	// print_r($general_inv_format_arr);exit;
		// 	$org_prefix = $general_inv_format_arr[0];
		// 	$prefix_key = 0;
		// 	$invoice_key= '';
		// 	$financial_key ='';
		// 	$year_key = '';
		// 	foreach($general_inv_format_arr as $key =>$value){
				
		// 		if($value == '{invoice_sequence}'){
		// 			$invoice_key = $key;
		// 		}else if ($value == '{financial_year}'){
		// 			$financial_key =$key;
		// 		}else if($value == '{year}'){
		// 			$year_key = $key;
		// 		}
		// 	}
			
		// 	$current_year = date('Y');
		// 	$previous_year = date('Y') -1;
		// 	$next_year = date('Y') +1;
		// 	$financial_year = '';
		// 	$where = '';
		// 	if($financial_key != ''){
		// 		if($month > 3){
		// 			$financial_year = $current_year . '-' .$next_year; 
		// 		}else {
		// 			$financial_year = $previous_year . '-' .$current_year; 
		// 		}
		// 		if($financial_key == 2){
		// 			$where .= " AND `inv_no` LIKE '%$financial_year'";
		// 		}else if($financial_key == 1){
		// 			$where .= " AND `inv_no` LIKE '%$financial_year%'";
		// 		}
		// 	}
		// 	if($year_key != ''){
		// 		$where .= " AND `inv_no` LIKE '%$current_year%'";
		// 	}
			
			
			
		// 	$sql = "SELECT * from ".$tblpx."invoice  WHERE `inv_no` LIKE '$org_prefix%' $where ORDER BY `invoice_id` DESC LIMIT 1;" ;
		// 	//echo "<pre>";print_r($sql);exit;
		// 	$previous_inv_no='';
		// 	$invoice_seq_no = 1;
		// 	$previous_data = Yii::app()->db->createCommand($sql)->queryRow();
		// 	if( !empty($previous_data) ){
		// 		$previous_inv_no = $previous_data['inv_no'];
		// 		$prefix_arr = explode('/', $previous_inv_no);
		// 		$prefix = $prefix_arr[0];
		// 		$previous_prefix_seq_no = $prefix_arr[$invoice_key];
		// 		$invoice_seq_no = $previous_prefix_seq_no + 1;
				
		// 	}
		// 	$digit = strlen((string) $invoice_seq_no);
		// 	if ($this->getActiveTemplate() == 'TYPE-4' && ENV =='production') {
		// 		//$invoice_no =$this->getArchInvoiceNo();
					
		// 	}else{
		// 		if ($digit == 1) {
		// 			$invoice_no ='00' .$invoice_seq_no;
		// 		} else if ($digit == 2) {
		// 			$invoice_no = '0' . $invoice_seq_no;
		// 		} else {
		// 			$invoice_no =$invoice_seq_no;
		// 		}
		// 	}
		// 	$new_invoice = str_replace('{financial_year}',$financial_year,$invoice_format);
		// 	$new_invoice_yr = str_replace('{year}',$current_year,$new_invoice);
		// 	$new_invoice_no = str_replace('{invoice_sequence}',$invoice_no,$new_invoice_yr);
		// 	$sql = "SELECT * from ".$tblpx."invoice  WHERE `inv_no` = '$new_invoice_no' ;" ;
			
		// 		$duplicate_data = Yii::app()->db->createCommand($sql)->queryRow();
		// 		if(!empty($duplicate_data)){
		// 			$previous_inv_no='';
		// 			$invoice_seq_no = 1;
		// 			$previous_inv_no = $duplicate_data['inv_no'];
		// 			$prefix_arr = explode('/', $previous_inv_no);
		// 			$prefix = $prefix_arr[0];
		// 			$previous_prefix_seq_no_s = $prefix_arr[$invoice_key];
		// 			$previous_prefix_seq_no_s = ltrim($previous_prefix_seq_no_s, '0');
					
		// 			$invoice_seq_no = $previous_prefix_seq_no_s + 1;
		// 			$digit = strlen((string) $invoice_seq_no);
		// 		if ($this->getActiveTemplate() == 'TYPE-4' && ENV =='production') {
		// 			//$invoice_no =$this->getArchInvoiceNo();
						
		// 		}else{
		// 			if ($digit == 1) {
		// 				$invoice_no ='00' .$invoice_seq_no;
		// 			} else if ($digit == 2) {
		// 				$invoice_no = '0' . $invoice_seq_no;
		// 			} else {
		// 				$invoice_no =$invoice_seq_no;
		// 			}
		// 		}
		// 		$new_invoice = str_replace('{financial_year}',$financial_year,$invoice_format);
		// 		$new_invoice_yr = str_replace('{year}',$current_year,$new_invoice);
		// 		$new_invoice_no = str_replace('{invoice_sequence}',$invoice_no,$new_invoice_yr);
		// 		}
				
			
			
		// 	$model->inv_no=$new_invoice_no;
		// }
			
		//}
			$model->save();
			$client     = Clients::model()->findByPK($model->client_id);
			echo json_encode(array('response' => 'success', 'msg' => 'Updated successfully', 'client' => $client->name));
		}
		if (isset($_POST['project_change'])) {
			$project 	= Projects::model()->findByPK($_POST['project_change']);
			$client     = Clients::model()->findByPK($project->client_id);
			echo json_encode(array('response' => 'success', 'client' => $client->name));
		}
	}

	public function actionUpdateinvoiceDate()
	{
		$invoiceId = $_POST['id'];
		$model 		=  Invoice::model()->find(array("condition" => "invoice_id = '$invoiceId'"));
		$model->date = date('Y-m-d', strtotime($_POST['date']));		
		if($model->save()){
			echo json_encode(array('response' => 'success'));
		}else{
			echo json_encode(array('response' => 'error'));
		}
	}

	public function getArchInvoiceNo(){
		$sql = "SELECT count(*)  FROM `jp_invoice`";  
        $counter = Yii::app()->db->createCommand($sql)->queryScalar();
		$futureDate=date('y', strtotime('+1 year'));
		if($counter == 0){
			$invoiceno = str_pad($counter+1,3,"0",STR_PAD_LEFT);			
        	$invoice_no = "INV-".date('y')."/".$futureDate."-".$invoiceno;

		}else{
			// $invoicecount = Yii::app()->db->createCommand("SELECT count(*)  FROM `jp_invoice` WHERE `inv_no` LIKE '".$invoice_no."' ")->queryScalar();
			$last_in_no = Yii::app()->db->createCommand("SELECT inv_no FROM `jp_invoice` ORDER BY invoice_id DESC LIMIT 1")->queryScalar();

			$invArray = explode('-',$last_in_no);
			
			$next_inv = str_pad($invArray[2]+1,3,"0",STR_PAD_LEFT);
			$invoice_no = "INV-".date('y')."/".$futureDate."-".$next_inv;			

		}

		return $invoice_no;

        
	}

	public function actionupdateInvoiceamount(){
		$result = array('status' => 0, 'message' => 'Nothing to submit');		
		$model = $this->loadModel($_POST['invoice_id']);
		$model->round_off = $_POST['round_off'];
			
		if ($model->save()) {
			$totalamount = $model->subtotal + $model->tax_amount + ($model->round_off) ;
			$result = array('status' => 1, 'message' => 'success', 'totalamount' => $totalamount);
		} else {
			$result = array('status' => 0, 'message' => $model->getErrors());
		}
		
		echo json_encode($result);
	}
	public function actionMailtest(){
		$mail = new JPhpMailer();
		$mail->setFrom('from@example.com', 'First Last');
		$mail->addReplyTo('neenu.a@bluehorizoninfotech.com', 'Neenu a');
		$mail->addAddress('neenu.a@bluehorizoninfotech.com'); 
		$mail->isHTML(true);
		$mail->Subject = "PHPMailer SMTP test with exceptions";
		$mail->Body = 'This is a test mail';
		$mail->AltBody = 'This is the plain text version of the email content';
		if(!$mail->send()){
		echo 'Message could not be sent.';
		echo 'Mailer Error: ' .$mail->ErrorInfo;
		}else{
			echo 'Message has been sent';
		}
	}
	public function replaceItemlistWithCondition($extractedContent, $replacements) {
        $grouppattern = '/\[\((.*?)\)\]/';
    
        preg_match_all($grouppattern, $extractedContent, $groupmatches);
    
        foreach ($groupmatches[0] as $match) {
          //  $replaceString = $match;
          $input = str_replace(array("[(", ")]"), '', $match); 
    
            // Check if "length," "width," and "size" are all present
            $variables = preg_match_all('/\{([^}]+)\}/', $match, $matches);
            $requiredVariables = ["length", "width"];
           
            // Check if any required variable is missing
           // if (count(array_intersect($requiredVariables, $matches[0])) == count($requiredVariables)) {
            $i=0;
                foreach ($matches[0] as $variable) { 
                    if (!empty($replacements[$variable])) {$i++;
                        $replaceString = str_replace($variable, $replacements[$variable], $input);
                       
                    } else {
                        // If any variable is missing a value, replace it with an empty string
                        $replaceString = str_replace($variable, '', $match);
                    }
                }
           if(count($matches[0]) != $i) {
                // Any required variable is missing, replace with an empty string
                $replaceString = '';
            }
  
            $extractedContent = str_replace($match, $replaceString, $extractedContent);
           
            //$extractedContent = preg_replace('/\[|\]|\(|\)|\s+/', '', $extractedContent);


           
        }//echo $extractedContent;die;
    
        return $extractedContent;
    }
	public function actionaddadditionalcharge()
	{

		if (isset($_POST)) {


			$invoice_id = $_POST['invoice_id'];
			$category = $_POST['category'];
			$amount = $_POST['amount'];

			$tblpx = Yii::app()->db->tablePrefix;
			$addBills = Yii::app()->db->createCommand(" INSERT INTO " . $tblpx . "invoice_additional_charges(`id`, `invoice_id`, `category`, `amount`, `created_by`, `created_date`)VALUES (null, " . $invoice_id . ",'" . $category . "'," . $amount . "," . yii::app()->user->id . ",'" . date('Y-m-d') . "')");
			$addBills->execute();
			$billItemId = Yii::app()->db->getLastInsertID();
			if ($billItemId) {

				$addcharges = Yii::app()->db->createCommand("SELECT ROUND(SUM(amount), 2) as amount FROM " . $tblpx . "invoice_additional_charges WHERE `invoice_id`=" . $invoice_id)->queryRow();

				$addBillsh = Yii::app()->db->createCommand(" UPDATE " . $tblpx . "invoice SET invoice_additionalcharge=" . $addcharges['amount'] . " WHERE invoice_id=" . $invoice_id . "");
				$addBillsh->execute();

				$data = array('itemid' => $billItemId, 'amount' => $addcharges['amount']);

				echo json_encode($data);
				exit;
			} else {
				echo 1;
				exit;
			}
		}
	}
	public function actiondeleteadditionalcharge()
	{
		$tblpx = Yii::app()->db->tablePrefix;
		if (isset($_POST)) {

			$id = $_POST['id'];
			$invoice_id = $_POST['invoice_id'];
			if ($id != '') {
				$delbill_amount_query = Yii::app()->db->createCommand("SELECT amount FROM " . $tblpx . "invoice_additional_charges WHERE `id`=" . $id)->queryRow();
				$delbill_amount = $delbill_amount_query['amount'];

				$delbill = Yii::app()->db->createCommand("DELETE FROM " . $tblpx . "invoice_additional_charges WHERE `id`=" . $id);
				if ($delbill->execute()) {
					$additional_amount = Yii::app()->db->createCommand("SELECT SUM(amount)as additionalamount FROM " . $tblpx . "invoice_additional_charges WHERE `invoice_id`=" . $invoice_id)->queryRow();

					$addBillsh = Yii::app()->db->createCommand(" UPDATE " . $tblpx . "invoice SET invoice_additionalcharge=" . ($additional_amount['additionalamount'] != '' ? $additional_amount['additionalamount'] : 0) . " WHERE invoice_id=" . $invoice_id . "");
					$addBillsh->execute();
				}

				echo 1;
				exit;
			}
		}
	}
	public function actionGetRateCard(){
		$work_type = $_POST['work_type'];
		$tblpx = Yii::app()->db->tablePrefix;
		$expense_type  = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}rate_card WHERE FIND_IN_SET(" . $work_type . ",worktype)")->queryAll();
		if (!empty($expense_type)) {
			$html['html'] = '<option value="">Select Ratecard</option>';
			foreach ($expense_type as $key => $value) {
				$html['html'] .= '<option value="' . $value['id'] . '">' . $value['rate'] . '</option>';
				$html['status'] = 'success';
			}
		} else {
			$html['status'] = 'no_success';
			$html['html'] .= '<option value="">Select Ratecard</option>';
		}
		echo json_encode($html);
	}
	public function actionGetInvoiceNo(){
		$company_id = $_GET["company_id"];
		$company = Company::model()->findByPK($company_id);
		//echo "<pre>";print_r($company);exit;
		$invoice_no = '';
		$month = date('m');//04
		$tblpx = Yii::app()->db->tablePrefix;
		if($company["invoice_format"] != NULL){
			$invoice_format =trim($company["invoice_format"]);
			$general_inv_format_arr = explode('/',$invoice_format);
			// print_r($general_inv_format_arr);exit;
			$org_prefix = $general_inv_format_arr[0];
			$prefix_key = 0;
			$invoice_key= '';
			$financial_key ='';
			$year_key = '';
			$org_prefix_slash=$general_inv_format_arr[0].'/';
			foreach($general_inv_format_arr as $key =>$value){
				
				if($value == '{invoice_sequence}'){
					$invoice_key = $key;
				}else if ($value == '{financial_year}'){
					$financial_key =$key;
				}else if($value == '{year}'){
					$year_key = $key;
				}
			}
			
			$current_year = date('Y');
			$previous_year = date('Y') -1;
			$next_year = date('Y') +1;
			$financial_year = '';
			$where = '';
			if($financial_key != ''){
				if($month > 3){
					$financial_year = $current_year . '-' .$next_year; 
				}else {
					$financial_year = $previous_year . '-' .$current_year; 
				}
				if($financial_key == 2){
					$where .= " AND `inv_no` LIKE '%$financial_year'";
				}else if($financial_key == 1){
					$where .= " AND `inv_no` LIKE '%$financial_year%'";
				}
			}
			if($year_key != ''){
				$where .= " AND `inv_no` LIKE '%$current_year%'";
			}
			
			
			
			$sql = "SELECT * from ".$tblpx."invoice  WHERE `inv_no` LIKE '$org_prefix_slash%' $where  AND  company_id = ". $company_id." ORDER BY `invoice_id` DESC LIMIT 1;" ;
			//echo "<pre>";print_r($sql);exit;
			$previous_inv_no='';
			$invoice_seq_no = 1;
			$previous_data = Yii::app()->db->createCommand($sql)->queryRow();
			if( !empty($previous_data) ){
				$previous_inv_no = $previous_data['inv_no'];
				$prefix_arr = explode('/', $previous_inv_no);
				$prefix = $prefix_arr[0];
				$previous_prefix_seq_no = $prefix_arr[$invoice_key];
				$invoice_seq_no = $previous_prefix_seq_no + 1;
				
			}
			//die($previous_inv_no);
			$digit = strlen((string) $invoice_seq_no);
			if ($this->getActiveTemplate() == 'TYPE-4' && ENV =='production') {
				$invoice_no =$this->getArchInvoiceNo();
					
			}else{
				if ($digit == 1) {
					$invoice_no ='00' .$invoice_seq_no;
				} else if ($digit == 2) {
					$invoice_no = '0' . $invoice_seq_no;
				} else {
					$invoice_no =$invoice_seq_no;
				}
			}
			$new_invoice = str_replace('{financial_year}',$financial_year,$invoice_format);
			$new_invoice_yr = str_replace('{year}',$current_year,$new_invoice);
			$new_invoice_no = str_replace('{invoice_sequence}',$invoice_no,$new_invoice_yr);
			$sql = "SELECT * from ".$tblpx."invoice  WHERE `inv_no` = '$new_invoice_no'  ;" ;
			//die($sql);
				$duplicate_data = Yii::app()->db->createCommand($sql)->queryRow();
				if(!empty($duplicate_data)){
					
					$previous_inv_no='';
					$invoice_seq_no = 1;
					$previous_inv_no = $duplicate_data['inv_no'];
					$prefix_arr = explode('/', $previous_inv_no);
					$prefix = $prefix_arr[0];
					$previous_prefix_seq_no_s = $prefix_arr[$invoice_key];
					$previous_prefix_seq_no_s = ltrim($previous_prefix_seq_no_s, '0');
					
					$invoice_seq_no = $previous_prefix_seq_no_s + 1;
					
					$digit = strlen((string) $invoice_seq_no);
				if ($this->getActiveTemplate() == 'TYPE-4' && ENV =='production') {
					$invoice_no =$this->getArchInvoiceNo();
						
				}else{
					if ($digit == 1) {
						$invoice_no ='00' .$invoice_seq_no;
					} else if ($digit == 2) {
						$invoice_no = '0' . $invoice_seq_no;
					} else {
						$invoice_no =$invoice_seq_no;
					}
				}
				$new_invoice = str_replace('{financial_year}',$financial_year,$invoice_format);
				$new_invoice_yr = str_replace('{year}',$current_year,$new_invoice);
				$new_invoice_no = str_replace('{invoice_sequence}',$invoice_no,$new_invoice_yr);
				}
				$sql = "SELECT * from ".$tblpx."invoice  WHERE `inv_no` = '$new_invoice_no'  ;" ;
			
				$duplicate_data = Yii::app()->db->createCommand($sql)->queryRow();
				if(!empty($duplicate_data)){
					
					$previous_inv_no='';
					$invoice_seq_no = 1;
					$previous_inv_no = $duplicate_data['inv_no'];
					$prefix_arr = explode('/', $previous_inv_no);
					$prefix = $prefix_arr[0];
					$previous_prefix_seq_no_s = $prefix_arr[$invoice_key];
					$previous_prefix_seq_no_s = ltrim($previous_prefix_seq_no_s, '0');
					
					$invoice_seq_no = $previous_prefix_seq_no_s + 1;
					
					$digit = strlen((string) $invoice_seq_no);
				if ($this->getActiveTemplate() == 'TYPE-4' && ENV =='production') {
					$invoice_no =$this->getArchInvoiceNo();
						
				}else{
					if ($digit == 1) {
						$invoice_no ='00' .$invoice_seq_no;
					} else if ($digit == 2) {
						$invoice_no = '0' . $invoice_seq_no;
					} else {
						$invoice_no =$invoice_seq_no;
					}
				}
				$new_invoice = str_replace('{financial_year}',$financial_year,$invoice_format);
				$new_invoice_yr = str_replace('{year}',$current_year,$new_invoice);
				$new_invoice_no = str_replace('{invoice_sequence}',$invoice_no,$new_invoice_yr);
				}
			echo $new_invoice_no;
		}else{
			$model = new Invoice;
			$newmodel = new InvList;
			$tblpx      = Yii::app()->db->tablePrefix;
			$months = array("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L");
			$prefix = '';
			$i = 1;
			foreach ($months as $key => $value) {
				if ($month == $i) {
					$prefix = $value;
				}
				$i++;
			}

			$sql = "SELECT MAX(CAST(invoice_id as signed)) as largenumber "
					. " FROM {$tblpx}invoice WHERE MONTH(created_date) = MONTH(CURRENT_DATE()) "
					. " AND YEAR(created_date) = YEAR(CURRENT_DATE())";
					//echo "<pre>";print_r($sql);exit;
			$maxData = Yii::app()->db->createCommand($sql)->queryRow();
			$maxNo   = $maxData["largenumber"];
			$newPONo = $maxNo + 1;
			$prNo   = $newPONo;
			$digit = strlen((string) $prNo);
			$activeProjectTemplate = $this->getActiveTemplate();
			if ($this->getActiveTemplate() == 'TYPE-4' && ENV =='production') {
				$invoice_no =$this->getArchInvoiceNo();
					
			}else{
				if ($digit == 1) {
					$invoice_no = $prefix . '00' . $prNo;
				} else if ($digit == 2) {
					$invoice_no = $prefix . '0' . $prNo;
				} else {
					$invoice_no = $prefix . $prNo;
				}
			}
			echo $invoice_no;
		}
	}
}
