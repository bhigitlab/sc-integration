<?php 
if ($index == 0) {
    ?>

    <thead>
        <tr>
	
            <th style="width:40px;">Sl No.</th>
            <th>Company</th>
            <th>Cash Balance Type</th>
            <th>Bank</th>
            <th>Opening Balance</th>            
<!--
            <th>Deposit</th>
            <th>Withdrawal</th>
            <th>Closing Balance</th>
-->
            <?php
            if(isset(Yii::app()->user->role) && (in_array('/cashbalance/update', Yii::app()->user->menuauthlist))){
            ?>
           <th  style="width:40px;">Action</th>
           <?php } ?>
        </tr>   
    </thead>
            <?php
        }
        ?>
       
            <tr>

                <td style="width:40px;"><?php echo $index + 1; ?></td>
                <td>
                    <?php
                 $company = Company::model()->findByPk($data->company_id);	
		 echo $company['name']; ?>
                </td>
                <td><?php echo $data->cashbalanceType->caption; ?></td>
                <td><?php echo ($data->bank_id == '')?'':$data->bank->bank_name; ?></td>
                <td align="right"><?php echo Controller::money_format_inr($data->cashbalance_opening_balance,2); ?></td>
<!--
                <td><?php // echo $data->cashbalance_deposit; ?></td>
                <td><?php // echo $data->cashbalance_withdrawal; ?></td>
                <td><?php // echo $data->cashbalance_closing_balance; ?></td>
-->
                <?php
            if(isset(Yii::app()->user->role) && (in_array('/cashbalance/update', Yii::app()->user->menuauthlist))){
            ?>
                <td  style="width:40px;"> <span class="fa fa-edit editProject" data-toggle="modal" data-target=".edit" data-id="<?php echo $data->id; ?>"></span></td>
                <?php } ?>
            </tr>





