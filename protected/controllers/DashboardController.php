<?php

class DashboardController extends Controller
{

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            //'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        $accessArr = array();
        $accessauthArr = array();
        $controller = Yii::app()->controller->id;
        if (isset(Yii::app()->user->menuall)) {
            if (array_key_exists($controller, Yii::app()->user->menuall)) {
                $accessArr = Yii::app()->user->menuall[$controller];
            }
        }

        if (isset(Yii::app()->user->menuauth)) {
            if (array_key_exists($controller, Yii::app()->user->menuauth)) {
                $accessauthArr = Yii::app()->user->menuauth[$controller];
            }
        }
        $access_privlg = count($accessauthArr);

        if (in_array('index', $accessArr) and $access_privlg == 0 and Yii::app()->controller->action->id !== 'home') {
            $this->redirect(array('/dashboard/home'));
            die;
        } else {
            if (in_array('index', $accessArr) and $access_privlg > 0 and Yii::app()->controller->action->id == 'home') {
                $this->redirect(array('/dashboard/index'));
            }
        }

        return array(
            array(
                'allow', // allow all users to perform 'index' and 'view' actions
                'actions' => $accessArr,
                'users' => array('@'),
                'expression' => "$access_privlg > 0",
            ),
            array(
                'allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => $accessauthArr,
                'users' => array('@'),
                'expression' => "$access_privlg > 0",
            ),
            array(
                'allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('home', 'changeexpenseview','allchangeexpenseview','allnotificationview','changeTemplateStatus','uploadFiles','removeFile','mediaUploadFiles','mediasettings','IPWhiteList','Cashbalance','FinancialReport','Allfilter'),
                'users' => array('@')
            ),
            array(
                'deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionHome()
    {
        $this->render('home');
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionIndex()
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        $newQuery = "";
        $newQuery1 = "";
        foreach ($arrVal as $arr) {
            if ($newQuery)
                $newQuery .= ' OR';
            if ($newQuery1)
                $newQuery1 .= ' OR';
            $newQuery .= " FIND_IN_SET('" . $arr . "', " . $tblpx . "projects.company_id)";
            $newQuery1 .= " FIND_IN_SET('" . $arr . "', " . $tblpx . "expenses.company_id)";
        }

        if (Yii::app()->user->role == 1) {

            $sql = Yii::app()->db->createCommand('select exp_id,name,type,exptype,payment_type,caption,date,amount,first_name,expense_type,' . $tblpx . 'expenses.description from ' . $tblpx . 'expenses
			inner join ' . $tblpx . 'projects on ' . $tblpx . 'expenses.projectid = ' . $tblpx . 'projects.pid
			inner join ' . $tblpx . 'users on ' . $tblpx . 'expenses.userid = ' . $tblpx . 'users.userid
			inner join ' . $tblpx . 'status on ' . $tblpx . 'expenses.type = ' . $tblpx . 'status.sid WHERE (' . $newQuery1 . ') ORDER BY exp_id DESC LIMIT 6')->queryAll();

            //print_r($sql); die();	
            //$model = new Projects;

            $sql1 = Yii::app()->db->createCommand('select pid,name,description from ' . $tblpx . 'projects 
            WHERE (' . $newQuery . ') ORDER BY pid DESC LIMIT 6 ')->queryAll();
            //print_r($sql1); 
        } else {
            //die(Yii::app()->user->id);
            $sql = Yii::app()->db->createCommand('select exp_id,name,type,exptype,payment_type,caption,date,amount,first_name,expense_type,' . $tblpx . 'expenses.description from ' . $tblpx . 'expenses
			inner join ' . $tblpx . 'projects on ' . $tblpx . 'expenses.projectid = ' . $tblpx . 'projects.pid
			inner join ' . $tblpx . 'users on ' . $tblpx . 'expenses.userid = ' . $tblpx . 'users.userid
			inner join ' . $tblpx . 'status on ' . $tblpx . 'expenses.type = ' . $tblpx . 'status.sid 
			where ' . $tblpx . 'users.userid=' . Yii::app()->user->id . '  AND (' . $newQuery1 . ')
			ORDER BY exp_id DESC LIMIT 6')->queryAll();
            //$model = new Projects;

            $sql1 = Yii::app()->db->createCommand('select pid,name,description from ' . $tblpx . 'projects
		    inner join ' . $tblpx . 'project_assign on ' . $tblpx . 'projects.pid=' . $tblpx . 'project_assign.projectid
		    inner join ' . $tblpx . 'users on ' . $tblpx . 'project_assign.userid = ' . $tblpx . 'users.userid
		    where ' . $tblpx . 'users.userid=' . Yii::app()->user->id . ' AND (' . $newQuery . ')
		    ORDER BY pid DESC LIMIT 6 ')->queryAll();
            //print_r($sql1); 
        }
        $limit_count = 5;
        $editmodel = new Editrequest;
        $deletemodel = new Deletepending;
        $notifications = new Notifications;
        $pendingquotation = new Scquotation;
        $purchse = new Purchase;
        $pendingquotation->approve_status = "No";
        $pendingquotation->added_viewed_notification=NULL;
        $this->getExpenseNotifications();
        $expense_notifications = new ExpenseNotification();
       
        $criteria = new CDbCriteria;
        $criteria->select = 't.project_id'; 
        $criteria->join = 'INNER JOIN jp_projects jp ON t.project_id = jp.pid'; // Join with jp_projects table
        $criteria->compare('t.view_status', 1); 
        $expenseNotificationss = ExpenseNotification::model()->findAll($criteria);

        $expense_notifications_count = count($expenseNotificationss);
        $final_array = array();
        $a = 0;
        $b = 0;
        $c = 0;


        $pending_edit = (isset(Yii::app()->user->role) && (in_array('/dashboard/editrequest', Yii::app()->user->menuauthlist))) ? ApproveRequestHelper::editRequestData():array();        
        foreach ($pending_edit as $key => $value) {
            $final_array[$key]['id'] = $value['id'];
            $final_array[$key]['date'] = date('Y-m-d', strtotime($value['record_grop_id']));
            $final_array[$key]['parent_id'] = '';
            $final_array[$key]['type'] = 'edit_request';
            $final_array[$key]['user_id'] = '';
            $final_array[$key]['table'] = '';
        }
        $pending_delete = array();
        if (isset(Yii::app()->user->role) && (in_array('/dashboard/deleterequest', Yii::app()->user->menuauthlist))) {
        $sql = "SELECT * FROM {$tblpx}deletepending "
            . " WHERE deletepending_status = 0 "
            . " AND `deletepending_data` NOT LIKE 'null' "
            . " AND `deletepending_data` IS NOT NULL  "
            . " ORDER BY deletepending_id desc";
        $pending_delete = Yii::app()->db->createCommand($sql)->queryAll();
        }
        $a = count($pending_edit);
        foreach ($pending_delete as $key => $value) {
            $final_array[$a]['id'] = $value['deletepending_id'];
            $final_array[$a]['date'] = $value['requested_date'];
            $final_array[$a]['parent_id'] = $value['deletepending_parentid'];
            $final_array[$a]['type'] = 'delete_request';
            $final_array[$a]['user_id'] = $value['user_id'];
            $final_array[$a]['table'] = $value['deletepending_table'];
            $a++;
        }

        $condition = " AND 1=1";
        if (Yii::app()->user->role != 1) {
            $condition .= " AND requested_by=" . Yii::app()->user->id;
        }
        $notification = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}notifications WHERE view_status = 0 " . $condition . " ORDER BY id desc")->queryAll();
        

        $notification_count = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}notifications WHERE view_status = 0 " . $condition . " ORDER BY id desc")->queryAll();
        $b = count($pending_edit) + count($pending_delete);
        foreach ($notification as $key => $value) {
            $final_array[$b]['id'] = $value['id'];
            $final_array[$b]['date'] = $value['date'];
            $final_array[$b]['parent_id'] = $value['parent_id'];
            $final_array[$b]['type'] = 'notification';
            $final_array[$b]['user_id'] = '';
            $final_array[$b]['table'] = 'notification';
            $b++;
        }

        $this->updateScQuotationStatusBasedOnItemsStatus();
        $scquotation=array();
        if (isset(Yii::app()->user->role) && (in_array('/dashboard/scquotation', Yii::app()->user->menuauthlist))) {
            $scquotation = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}scquotation WHERE approve_status = 'No' AND viewed_status != 1 ORDER BY scquotation_id desc")->queryAll();
        }
        $scquotation_count = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}scquotation WHERE approve_status = 'No' ORDER BY scquotation_id desc")->queryAll();
        $c = count($pending_edit) + count($pending_delete) + count($notification);
        foreach ($scquotation as $key => $value) {
            $final_array[$c]['id'] = $value['scquotation_id'];
            $final_array[$c]['date'] = $value['scquotation_date'];
            $final_array[$c]['parent_id'] = $value['scquotation_id'];
            $final_array[$c]['type'] = 'scquotation';
            $final_array[$c]['user_id'] = $value['created_by'];
            $final_array[$c]['table'] = 'scquotation';
            $final_array[$c]['sc_qtn_amount'] = !empty($value['scquotation_amount']) ? $value['scquotation_amount'] : '0';
            $c++;
        }

        $po_count = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}purchase WHERE permission_status ='No'  ORDER BY p_id desc")->queryAll();
        $poapproval = array();
        if (isset(Yii::app()->user->role) && (in_array('/dashboard/poapproval', Yii::app()->user->menuauthlist))) {
            $poapproval = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}purchase WHERE permission_status ='No' ORDER BY p_id desc")->queryAll();
        }
        $d = count($pending_edit) + count($pending_delete) + count($notification) + count($scquotation);
        foreach ($poapproval as $key => $value) {
            $final_array[$d]['id'] = $value['p_id'];
            $final_array[$d]['date'] = $value['purchase_date'];
            $final_array[$d]['parent_id'] = '';
            $final_array[$d]['type'] = 'po_approval';
            $final_array[$d]['user_id'] = $value['created_by'];
            $final_array[$d]['table'] = 'purchase';
            $d++;
        }
        $expense_notfcts = ExpenseNotification::model()->findAllByAttributes(array('view_status' => 1));
        foreach ($expense_notfcts as $key => $value) {
            $final_array[$d]['id'] = $value['id'];
            $final_array[$d]['date'] = $value['generated_date'];
            $final_array[$d]['parent_id'] = '';
            $final_array[$d]['type'] = 'expense_notification';
            $final_array[$d]['user_id'] = '';
            $final_array[$d]['table'] = 'expense_notification';
            $d++;
        }


        array_multisort(array_map(function ($element) {
            return $element['date'];
        }, $final_array), SORT_DESC, $final_array);

        $alldata = new CArrayDataProvider($final_array, array(
            'id' => 'id',
            'keyField' => 'id',
            'pagination' => array('pageSize' => 5),
        ));

        $all = count($pending_edit) + count($pending_delete) + count($notification) + count($scquotation) + count($poapproval) + $expense_notifications_count;
        if (isset($_GET['Projects']['project_id']) && !empty($_GET['Projects']['project_id'])) {
            $project_id = $_GET['Projects']['project_id'];
        } else {
            $project_id = '';
        }

        if (isset($_GET['Projects']['company_id']) && !empty($_GET['Projects']['company_id'])) {
            $company_id = $_GET['Projects']['company_id'];
        } else {
            $company_id = '';
        }

        
        $buyer_module = GeneralSettings::model()->checkBuyerModule();
        $editrequest = PreExpenses::model()->findAll(array('condition' => 'approval_status="0"'));
        $mrmodel ='';
        $mrrequest_count = 0;
        if($this->getActiveTemplate()=='TYPE-4'){
            $mrmodel =  MaterialRequisition::model()->findAll(array('condition' => 'requisition_status="1" and purchase_id IS NULL'));;
            $mrrequest_count = count($mrmodel);
        }
        /**changes by rev starts */
       
        $pending_invoice = Invoice::model()->findAll(array('condition' => 'invoice_status="draft" and status_id=108'));
        $pending_inv_count=count($pending_invoice); 
        $unassignedWarehouseCount = Warehouse::model()->count('assigned_to IS NULL');
       
        $range = Yii::app()->request->getParam('range');
        $companyId = Yii::app()->request->getParam('company');
        // die($companyId);
        if(!empty($range)||!empty($companyId)){
            if($range==6){ //6 months
                $currentMonthStart = date('Y-m-01', strtotime('-5 months'));
                $currentMonthEnd = date('Y-m-t');
                $previousMonthStart = date('Y-m-01', strtotime('-11 months'));
                $previousMonthEnd = date('Y-m-t', strtotime('-6 months'));

                $payment_aging_array = Controller::getDashboardData($currentMonthStart, $currentMonthEnd, $previousMonthStart, $previousMonthEnd,$companyId,$range);

                $paymentAgingJson = json_encode($payment_aging_array, JSON_HEX_TAG | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_HEX_APOS);
                //line 
                $line_graph_array = Controller::getLineGraphData($currentMonthStart, $currentMonthEnd,$companyId,$range);

                $lineGraphJson = json_encode($line_graph_array, JSON_HEX_TAG | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_HEX_APOS);
                    //receivable
                $line_receivable_array = Controller::getLineGraphReceivable($currentMonthStart, $currentMonthEnd,$companyId,$range);

                $lineGraphReceivableJson = json_encode($line_receivable_array, JSON_HEX_TAG | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_HEX_APOS);
                //cashflow
                $line_cashflow_array = Controller::getLineGraphCashFlow($currentMonthStart, $currentMonthEnd,$companyId,$range);

                $lineGraphCashflowJson = json_encode($line_cashflow_array, JSON_HEX_TAG | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_HEX_APOS);
                //netprofit
                $line_netprofit_array = Controller::getLineGraphNetProfit($currentMonthStart, $currentMonthEnd,$companyId,$range);

                $lineGraphNetprofitJson = json_encode($line_netprofit_array, JSON_HEX_TAG | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_HEX_APOS);
                //grossprofit
                $line_grossprofit_array = Controller::getLineGraphGrossProfit($currentMonthStart, $currentMonthEnd,$companyId,$range);

                $lineGraphGrossprofitJson = json_encode($line_grossprofit_array, JSON_HEX_TAG | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_HEX_APOS);

                $currentSixMonthsStart = date('Y-m-01', strtotime('-1 months')); 
                $currentSixMonthsEnd = date('Y-m-t');
                $currentMonthName = date('M', strtotime('-1 month'))." - ".date('M');
                
                $previousSixMonthsStart = date('Y-m-01', strtotime('-3 months'));
                $previousSixMonthsEnd = date('Y-m-t', strtotime('-2 months')); 
                $previousMonthName = date('M', strtotime('-3 month'))." - ".date('M', strtotime('-2 month'));

                $doublePreviousSixMonthsStart = date('Y-m-01', strtotime('-5 months')); 
                $doublePreviousSixMonthsEnd = date('Y-m-t', strtotime('-4 months'));
                $twoMonthsAgoName = date('M', strtotime('-5 month'))." - ".date('M', strtotime('-4 month'));
                //  print_r($previousSixMonthsStart);print_r($previousSixMonthsEnd);print_r($previousMonthName);exit;
                
                $sales_array = Controller::getSalesGraph( $currentSixMonthsStart,$currentSixMonthsEnd,$currentMonthName, $previousSixMonthsStart,$previousSixMonthsEnd,$previousMonthName, $doublePreviousSixMonthsStart,$doublePreviousSixMonthsEnd, $twoMonthsAgoName,$companyId);
                $salesJson = json_encode($sales_array, JSON_HEX_TAG | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_HEX_APOS);
                $month=5;
                $profitloss_array = Controller::getProfitLossGraph($month,$companyId);
                $profitlossJson = json_encode($profitloss_array, JSON_HEX_TAG | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_HEX_APOS);
                
                $startOfMonth = date('Y-m-01', strtotime('-5 months')); 
                $endOfMonth =  date('Y-m-t');
                $ExpenseGraph = Controller::getExpenseBalance($startOfMonth,$endOfMonth,$companyId);
                $expenseJson = json_encode($ExpenseGraph, JSON_HEX_TAG | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_HEX_APOS);

                $AccountBalance = Controller::getAccountBalanceSummary($currentSixMonthsStart,$currentSixMonthsEnd,$currentMonthName, $previousSixMonthsStart,$previousSixMonthsEnd,$previousMonthName, $doublePreviousSixMonthsStart,$doublePreviousSixMonthsEnd, $twoMonthsAgoName,$companyId);
                // echo'<pre>';print_r($AccountBalance);exit;
                $accountBalanceJson = json_encode($AccountBalance, JSON_HEX_TAG | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_HEX_APOS);
            }
            else if($range==30){ //30 days
                $currentMonthStart = date('Y-m-01');
                $currentMonthEnd = date('Y-m-t'); 
                $previousMonthStart = date('Y-m-01', strtotime('first day of last month')); 
                $previousMonthEnd = date('Y-m-t', strtotime('last day of last month'));
                // die($companyId);
                $payment_aging_array = Controller::getDashboardData($currentMonthStart, $currentMonthEnd, $previousMonthStart, $previousMonthEnd,$companyId,$range);
                $paymentAgingJson = json_encode($payment_aging_array, JSON_HEX_TAG | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_HEX_APOS);
                $line_graph_array = Controller::getLineGraphData($currentMonthStart, $currentMonthEnd,$companyId,$range);

                $lineGraphJson = json_encode($line_graph_array, JSON_HEX_TAG | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_HEX_APOS);
                //receivable
                $line_receivable_array = Controller::getLineGraphReceivable($currentMonthStart, $currentMonthEnd,$companyId,$range);

                $lineGraphReceivableJson = json_encode($line_receivable_array, JSON_HEX_TAG | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_HEX_APOS);
                $line_cashflow_array = Controller::getLineGraphCashFlow($currentMonthStart, $currentMonthEnd,$companyId,$range);

                $lineGraphCashflowJson = json_encode($line_cashflow_array, JSON_HEX_TAG | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_HEX_APOS);
                //netprofit
                $line_netprofit_array = Controller::getLineGraphNetProfit($currentMonthStart, $currentMonthEnd,$companyId,$range);

                $lineGraphNetprofitJson = json_encode($line_netprofit_array, JSON_HEX_TAG | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_HEX_APOS);
                //grossprofit
                $line_grossprofit_array = Controller::getLineGraphGrossProfit($currentMonthStart, $currentMonthEnd,$companyId,$range);

                $lineGraphGrossprofitJson = json_encode($line_grossprofit_array, JSON_HEX_TAG | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_HEX_APOS);
                //Sales graph
                $currentMonthStart = date('Y-m-01');
                $currentMonthEnd = date('Y-m-t');
                $currentMonthName = date('F'); // e.g., "January"
            
                $previousMonthStart = date('Y-m-01', strtotime('-1 month'));
                $previousMonthEnd = date('Y-m-t', strtotime('-1 month'));
                $previousMonthName = date('F', strtotime('-1 month')); // e.g., "December"
            
                $twoMonthsAgoStart = date('Y-m-01', strtotime('first day of -2 months'));
                $twoMonthsAgoEnd = date('Y-m-t', strtotime('first day of -2 months'));
                $twoMonthsAgoName = date('F', strtotime('first day of -2 months'));

                $sales_array = Controller::getSalesGraph($currentMonthStart,$currentMonthEnd, $currentMonthName,$previousMonthStart,$previousMonthEnd,$previousMonthName,$twoMonthsAgoStart,$twoMonthsAgoEnd,$twoMonthsAgoName,$companyId);

                $salesJson = json_encode($sales_array, JSON_HEX_TAG | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_HEX_APOS);
                $month=5;
                $profitloss_array = Controller::getProfitLossGraph($month,$companyId);
                $profitlossJson = json_encode($profitloss_array, JSON_HEX_TAG | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_HEX_APOS);

                $startOfMonth = date('Y-m-01'); 
                $endOfMonth = date('Y-m-t');
                $ExpenseGraph = Controller::getExpenseBalance($startOfMonth,$endOfMonth,$companyId);
                $expenseJson = json_encode($ExpenseGraph, JSON_HEX_TAG | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_HEX_APOS);
                $currentMonthStart = date('Y-m-01'); 
                $currentMonthEnd = date('Y-m-t'); 
                $previousMonthStart = date('Y-m-01', strtotime('-1 month'));
                $previousMonthEnd = date('Y-m-t', strtotime('-1 month'));
                $twoMonthsAgoStart = date('Y-m-01', strtotime('-2 months'));
                $twoMonthsAgoEnd = date('Y-m-t', strtotime('-2 months'));
                $AccountBalance = Controller::getAccountBalanceSummary($currentMonthStart,$currentMonthEnd,$currentMonthName,$previousMonthStart, $previousMonthEnd,$previousMonthName,$twoMonthsAgoStart,$twoMonthsAgoEnd, $twoMonthsAgoName,$companyId);
                // echo'<pre>';print_r($AccountBalance);exit;
                $accountBalanceJson = json_encode($AccountBalance, JSON_HEX_TAG | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_HEX_APOS);
            }
            else if($range==1){ //1 year
                $currentMonthStart = date('Y-m-01', strtotime('-11 months'));
                $currentMonthEnd =  date('Y-m-01');
                $previousMonthStart =  date('Y-m-01', strtotime('-24 months')); 
                $previousMonthEnd = date('Y-m-01', strtotime('-12 months'));  
                $payment_aging_array = Controller::getDashboardData($currentMonthStart, $currentMonthEnd, $previousMonthStart, $previousMonthEnd,$companyId,$range);
                $paymentAgingJson = json_encode($payment_aging_array, JSON_HEX_TAG | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_HEX_APOS);
                $line_graph_array = Controller::getLineGraphData($currentMonthStart, $currentMonthEnd,$companyId,$range);
                //receivable
                $line_receivable_array = Controller::getLineGraphReceivable($currentMonthStart, $currentMonthEnd,$companyId,$range);

                $lineGraphReceivableJson = json_encode($line_receivable_array, JSON_HEX_TAG | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_HEX_APOS);

                $lineGraphJson = json_encode($line_graph_array, JSON_HEX_TAG | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_HEX_APOS);
                 //cashflow
                $line_cashflow_array = Controller::getLineGraphCashFlow($currentMonthStart, $currentMonthEnd,$companyId,$range);

                $lineGraphCashflowJson = json_encode($line_cashflow_array, JSON_HEX_TAG | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_HEX_APOS);
                //netprofit
                $line_netprofit_array = Controller::getLineGraphNetProfit($currentMonthStart, $currentMonthEnd,$companyId,$range);

                $lineGraphNetprofitJson = json_encode($line_netprofit_array, JSON_HEX_TAG | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_HEX_APOS);
                //grossprofit
                $line_grossprofit_array = Controller::getLineGraphGrossProfit($currentMonthStart, $currentMonthEnd,$companyId,$range);

                $lineGraphGrossprofitJson = json_encode($line_grossprofit_array, JSON_HEX_TAG | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_HEX_APOS);
                // Current year
                $currentYearStart = date('Y-01-01'); // Start of the current year
                $currentYearEnd = date('Y-12-31'); // End of the current year
                $currentYearName = date('Y'); // e.g., "2025"
                
                // Previous year
                $previousYearStart = date('Y-01-01', strtotime('-1 year')); // Start of the previous year
                $previousYearEnd = date('Y-12-31', strtotime('-1 year')); // End of the previous year
                $previousYearName = date('Y', strtotime('-1 year')); // e.g., "2024"
                
                // Double previous year (2 years before the current year)
                $doublePreviousYearStart = date('Y-01-01', strtotime('-2 years')); // Start of the double previous year
                $doublePreviousYearEnd = date('Y-12-31', strtotime('-2 years')); // End of the double previous year
                $doublePreviousYearName = date('Y', strtotime('-2 years'));

                $sales_array = Controller::getSalesGraph($currentYearStart,$currentYearEnd, $currentYearName,$previousYearStart,$previousYearEnd,$previousYearName, $doublePreviousYearStart,$doublePreviousYearEnd, $doublePreviousYearName,$companyId);

                $salesJson = json_encode($sales_array, JSON_HEX_TAG | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_HEX_APOS);
                $month=1;
                $profitloss_array = Controller::getProfitLossGraph($month,$companyId);
                $profitlossJson = json_encode($profitloss_array, JSON_HEX_TAG | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_HEX_APOS);

                $startOfYear = date('Y-01-01', strtotime('-1 year'));
                $endOfYear = date('Y-m-d');
                $ExpenseGraph = Controller::getExpenseBalance($startOfYear,$endOfYear,$companyId);
                $expenseJson = json_encode($ExpenseGraph, JSON_HEX_TAG | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_HEX_APOS);
                
                $currentMonthStart = date('Y-m-01');
                $currentMonthEnd = date('Y-m-t');

                // Previous 6 months
                $previous6MonthsStart = date('Y-m-01', strtotime('-12 months'));
                $previous6MonthsEnd = date('Y-m-t', strtotime('-1 month'));

                // Double the previous 6 months (12 months before the previous 6 months)
                $doublePrevious6MonthsStart = date('Y-m-01', strtotime('-24 months'));
                $doublePrevious6MonthsEnd = date('Y-m-t', strtotime('-12 months'));
                $AccountBalance = Controller::getAccountBalanceSummary($currentMonthStart,$currentMonthEnd,$currentYearName,$previous6MonthsStart,$previous6MonthsEnd,$previousYearName,$doublePrevious6MonthsEnd,$doublePrevious6MonthsEnd,$doublePreviousYearName,$companyId);

                // $AccountBalance = Controller::getAccountBalanceSummary($currentYearStart,$currentYearEnd,$previousYearStart,$previousYearEnd,$doublePreviousYearStart,$doublePreviousYearEnd);
                // echo'<pre>';print_r($AccountBalance);exit;
                $accountBalanceJson = json_encode($AccountBalance, JSON_HEX_TAG | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_HEX_APOS);
            }   
        }
        else{
            $range=30;
            //Accounts graph
            $currentMonthStart = date('Y-m-01');
            $currentMonthEnd = date('Y-m-t'); 
            $previousMonthStart = date('Y-m-01', strtotime('first day of last month')); 
            $previousMonthEnd = date('Y-m-t', strtotime('last day of last month'));
            $payment_aging_array = Controller::getDashboardData($currentMonthStart, $currentMonthEnd, $previousMonthStart, $previousMonthEnd,$companyId,$range);
            $paymentAgingJson = json_encode($payment_aging_array, JSON_HEX_TAG | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_HEX_APOS);
            //payable
            $line_graph_array = Controller::getLineGraphData($currentMonthStart, $currentMonthEnd,$companyId,$range);
            $lineGraphJson = json_encode($line_graph_array, JSON_HEX_TAG | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_HEX_APOS);
            //receivable
            $line_receivable_array = Controller::getLineGraphReceivable($currentMonthStart, $currentMonthEnd,$companyId,$range);

            $lineGraphReceivableJson = json_encode($line_receivable_array, JSON_HEX_TAG | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_HEX_APOS);
            //cashflow
            $line_cashflow_array = Controller::getLineGraphCashFlow($currentMonthStart, $currentMonthEnd,$companyId,$range);

            $lineGraphCashflowJson = json_encode($line_cashflow_array, JSON_HEX_TAG | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_HEX_APOS);
            //netprofit
            $line_netprofit_array = Controller::getLineGraphNetProfit($currentMonthStart, $currentMonthEnd,$companyId,$range);

            $lineGraphNetprofitJson = json_encode($line_netprofit_array, JSON_HEX_TAG | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_HEX_APOS);
            //grossprofit
            $line_grossprofit_array = Controller::getLineGraphGrossProfit($currentMonthStart, $currentMonthEnd,$companyId,$range);

            $lineGraphGrossprofitJson = json_encode($line_grossprofit_array, JSON_HEX_TAG | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_HEX_APOS);
            //Sales graph
            $currentMonthStart = date('Y-m-01');
            $currentMonthEnd = date('Y-m-t');
            $currentMonthName = date('F'); // e.g., "January"
        
            $previousMonthStart = date('Y-m-01', strtotime('-1 month'));
            $previousMonthEnd = date('Y-m-t', strtotime('-1 month'));
            $previousMonthName = date('F', strtotime('-1 month')); // e.g., "December"
        
            $twoMonthsAgoStart = date('Y-m-01', strtotime('first day of -2 months'));
            $twoMonthsAgoEnd = date('Y-m-t', strtotime('first day of -2 months'));
            $twoMonthsAgoName = date('F', strtotime('first day of -2 months'));

            $sales_array = Controller::getSalesGraph($currentMonthStart,$currentMonthEnd, $currentMonthName,$previousMonthStart,$previousMonthEnd,$previousMonthName,$twoMonthsAgoStart,$twoMonthsAgoEnd,$twoMonthsAgoName,$companyId);

            $salesJson = json_encode($sales_array, JSON_HEX_TAG | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_HEX_APOS);

             //profit and loss
            $month=5;
            $profitloss_array = Controller::getProfitLossGraph($month,$companyId);
            $profitlossJson = json_encode($profitloss_array, JSON_HEX_TAG | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_HEX_APOS);

            $startOfMonth = date('Y-m-01'); 
            $endOfMonth = date('Y-m-t');
            $ExpenseGraph = Controller::getExpenseBalance($startOfMonth,$endOfMonth,$companyId);
            $expenseJson = json_encode($ExpenseGraph, JSON_HEX_TAG | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_HEX_APOS);

            $currentMonthStart = date('Y-m-01'); 
            $currentMonthEnd = date('Y-m-t'); 
            $previousMonthStart = date('Y-m-01', strtotime('-1 month'));
            $previousMonthEnd = date('Y-m-t', strtotime('-1 month'));
            $twoMonthsAgoStart = date('Y-m-01', strtotime('-2 months'));
            $twoMonthsAgoEnd = date('Y-m-t', strtotime('-2 months'));
            $AccountBalance = Controller::getAccountBalanceSummary($currentMonthStart,$currentMonthEnd,$currentMonthName,$previousMonthStart, $previousMonthEnd,$previousMonthName,$twoMonthsAgoStart,$twoMonthsAgoEnd, $twoMonthsAgoName,$companyId);
            // echo'<pre>';print_r($AccountBalance);exit;
            $accountBalanceJson = json_encode($AccountBalance, JSON_HEX_TAG | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_HEX_APOS);
        }
        // echo'<pre>';print_r($paymentAgingJson);exit;
        $paymentAgingData = json_decode($paymentAgingJson, true);
        $currentPayable = $this->formatAmount($paymentAgingData[0]['current']);
        $accountReceivable=$this->formatAmount($paymentAgingData[1]['current']);
        $accountCashflow=$this->formatAmount($paymentAgingData[2]['current']);
        $accountNetprofit=$this->formatAmount($paymentAgingData[4]['current']);
        $accountGrossprofit=$this->formatAmount($paymentAgingData[3]['current']); 
        // Function to calculate percentage change
        function calculatePercentageChange($current, $previous) {
            if ($previous == 0) {
                return $current > 0 ? '100%' : ($current < 0 ? '-100%' : '0%');
            }
            return round((($current - $previous) / abs($previous)) * 100, 1) . '%';
        }

        // Map the data for easier access
        $metrics = [];
        foreach ($paymentAgingData as $data) {
            if (isset($data['name'])) {
                $metrics[$data['name']] = [
                    'current' => $data['current'],
                    'previous' => $data['previous'],
                    'percentage' => calculatePercentageChange($data['current'], $data['previous'])
                ];
            }
        }

         /**changes by rev ends */
        $this->render('index', array(
            'metrics'=>$metrics,
            'model' => $sql,
            'newmodel' => $sql1,
            'editmodel' => $editmodel->pendingsearch(),
            'editrequest' => $editrequest,
            'deletemodel' => $deletemodel->pendingsearch(),
            'notifications' => $notifications->notificationsearch(),
            'pendingquotation' => $pendingquotation->notification_search(),
            'alldata' => $alldata,
            'edit_count' => count($pending_edit),
            'delete_count' => count($pending_delete),
            'notification_count' => count($notification_count),
            'scquotation_count' => count($scquotation_count),
            'all_count' => $all,
            'purchaseorder' => $purchse->getapprovepo(),
            'po_count' => count($po_count),
            'expensenotifications' => $expense_notifications->notification_search(),
            'expense_notifications_count' => $expense_notifications_count,
            
            'project_id' => $project_id,
            
            'company_id' => $company_id,
            'buyer_module' => $buyer_module,
            'mrrequestcount' => $mrrequest_count,
            'mrmodel' => $mrmodel,
            'pending_invoice_count'=>$pending_inv_count,
            'unassigned_wh_count'=>$unassignedWarehouseCount,
            'paymentAgingJson' => $paymentAgingJson,
            'lineGraphJson' => $lineGraphJson,
            'lineGraphReceivableJson' => $lineGraphReceivableJson,
            'lineGraphCashflowJson' => $lineGraphCashflowJson,
            'lineGraphGrossprofitJson' => $lineGraphGrossprofitJson,
            'lineGraphNetprofitJson' => $lineGraphNetprofitJson,
            'salesJson' => $salesJson,
            'profitlossJson' => $profitlossJson,
            'expenseJson' => $expenseJson,
            'accountBalanceJson' => $accountBalanceJson,
            'range'=>$range,
            'companyId'=> $companyId,
            'currentPayable' => $currentPayable,
            'accountReceivable' => $accountReceivable,
            'accountCashflow' => $accountCashflow,
            'accountNetprofit' => $accountNetprofit,
            'accountGrossprofit' => $accountGrossprofit,
        ));
    }

    private function formatAmount($value) {
        if (!is_numeric($value)) return 'Invalid';
    
        $isNegative = $value < 0;
        $value = abs($value);
    
        if ($value >= 10000000) {
            $formatted = number_format($value / 10000000, 1) . "Cr";
        } elseif ($value >= 100000) {
            $formatted = number_format($value / 100000, 1) . "L";
        } elseif ($value >= 1000) {
            $formatted = number_format($value / 1000, 1) . "K";
        } else {
            $formatted = number_format($value);
        }
    
        return $isNegative ? "-$formatted" : $formatted;
    }

    public function actionAllfilter()
    {
        $selectedDate = Yii::app()->request->getPost('selectedDate');
    
        if ($selectedDate == 6) {
            $currentMonthStart = date('Y-m-01', strtotime('-5 months'));
            $currentMonthEnd = date('Y-m-t');
            $previousMonthStart = date('Y-m-01', strtotime('-11 months'));
            $previousMonthEnd = date('Y-m-t', strtotime('-6 months'));

            $payment_aging_array = Controller::getPayableDashboard($currentMonthStart, $currentMonthEnd, $previousMonthStart, $previousMonthEnd);
    
            // Return the data as JSON
            $response = [
                'status' => 'success',
                'data' => $payment_aging_array,
            ];
            echo CJSON::encode($response);
            Yii::app()->end();
        }
    
        // If the selected date is not 6, handle other logic or return an error
        $response = [
            'status' => 'error',
            'message' => 'Invalid date selected.',
        ];
        echo CJSON::encode($response);
        Yii::app()->end();
    }
    

    public function actionNotification()
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        $newQuery = "";
        $newQuery1 = "";
        foreach ($arrVal as $arr) {
            if ($newQuery)
                $newQuery .= ' OR';
            if ($newQuery1)
                $newQuery1 .= ' OR';
            $newQuery .= " FIND_IN_SET('" . $arr . "', " . $tblpx . "projects.company_id)";
            $newQuery1 .= " FIND_IN_SET('" . $arr . "', " . $tblpx . "expenses.company_id)";
        }

        if (Yii::app()->user->role == 1) {

            $sql = Yii::app()->db->createCommand('select exp_id,name,type,exptype,payment_type,caption,date,amount,first_name,expense_type,' . $tblpx . 'expenses.description from ' . $tblpx . 'expenses
			inner join ' . $tblpx . 'projects on ' . $tblpx . 'expenses.projectid = ' . $tblpx . 'projects.pid
			inner join ' . $tblpx . 'users on ' . $tblpx . 'expenses.userid = ' . $tblpx . 'users.userid
			inner join ' . $tblpx . 'status on ' . $tblpx . 'expenses.type = ' . $tblpx . 'status.sid WHERE (' . $newQuery1 . ') ORDER BY exp_id DESC LIMIT 6')->queryAll();

            //print_r($sql); die();	
            //$model = new Projects;

            $sql1 = Yii::app()->db->createCommand('select pid,name,description from ' . $tblpx . 'projects 
            WHERE (' . $newQuery . ') ORDER BY pid DESC LIMIT 6 ')->queryAll();
            //print_r($sql1); 
        } else {
            //die(Yii::app()->user->id);
            $sql = Yii::app()->db->createCommand('select exp_id,name,type,exptype,payment_type,caption,date,amount,first_name,expense_type,' . $tblpx . 'expenses.description from ' . $tblpx . 'expenses
			inner join ' . $tblpx . 'projects on ' . $tblpx . 'expenses.projectid = ' . $tblpx . 'projects.pid
			inner join ' . $tblpx . 'users on ' . $tblpx . 'expenses.userid = ' . $tblpx . 'users.userid
			inner join ' . $tblpx . 'status on ' . $tblpx . 'expenses.type = ' . $tblpx . 'status.sid 
			where ' . $tblpx . 'users.userid=' . Yii::app()->user->id . '  AND (' . $newQuery1 . ')
			ORDER BY exp_id DESC LIMIT 6')->queryAll();
            //$model = new Projects;

            $sql1 = Yii::app()->db->createCommand('select pid,name,description from ' . $tblpx . 'projects
		    inner join ' . $tblpx . 'project_assign on ' . $tblpx . 'projects.pid=' . $tblpx . 'project_assign.projectid
		    inner join ' . $tblpx . 'users on ' . $tblpx . 'project_assign.userid = ' . $tblpx . 'users.userid
		    where ' . $tblpx . 'users.userid=' . Yii::app()->user->id . ' AND (' . $newQuery . ')
		    ORDER BY pid DESC LIMIT 6 ')->queryAll();
            //print_r($sql1); 
        }
        $limit_count = 5;
        $editmodel = new Editrequest;
        $deletemodel = new Deletepending;
        $notifications = new Notifications;
        $pendingquotation = new Scquotation;
        $purchse = new Purchase;
        $pendingquotation->approve_status = "No";
        $pendingquotation->added_viewed_notification=NULL;
        $this->getExpenseNotifications();
        $expense_notifications = new ExpenseNotification();
       
        $criteria = new CDbCriteria;
        $criteria->select = 't.project_id'; 
        $criteria->join = 'INNER JOIN jp_projects jp ON t.project_id = jp.pid'; // Join with jp_projects table
        $criteria->compare('t.view_status', 1); 
        $expenseNotificationss = ExpenseNotification::model()->findAll($criteria);

        $expense_notifications_count = count($expenseNotificationss);
        $final_array = array();
        $a = 0;
        $b = 0;
        $c = 0;


        $pending_edit = (isset(Yii::app()->user->role) && (in_array('/dashboard/editrequest', Yii::app()->user->menuauthlist))) ? ApproveRequestHelper::editRequestData():array();        
        foreach ($pending_edit as $key => $value) {
            $final_array[$key]['id'] = $value['id'];
            $final_array[$key]['date'] = date('Y-m-d', strtotime($value['record_grop_id']));
            $final_array[$key]['parent_id'] = '';
            $final_array[$key]['type'] = 'edit_request';
            $final_array[$key]['user_id'] = '';
            $final_array[$key]['table'] = '';
        }
        $pending_delete = array();
        if (isset(Yii::app()->user->role) && (in_array('/dashboard/deleterequest', Yii::app()->user->menuauthlist))) {
        $sql = "SELECT * FROM {$tblpx}deletepending "
            . " WHERE deletepending_status = 0 "
            . " AND `deletepending_data` NOT LIKE 'null' "
            . " AND `deletepending_data` IS NOT NULL  "
            . " ORDER BY deletepending_id desc";
        $pending_delete = Yii::app()->db->createCommand($sql)->queryAll();
        }
        $a = count($pending_edit);
        foreach ($pending_delete as $key => $value) {
            $final_array[$a]['id'] = $value['deletepending_id'];
            $final_array[$a]['date'] = $value['requested_date'];
            $final_array[$a]['parent_id'] = $value['deletepending_parentid'];
            $final_array[$a]['type'] = 'delete_request';
            $final_array[$a]['user_id'] = $value['user_id'];
            $final_array[$a]['table'] = $value['deletepending_table'];
            $a++;
        }

        $condition = " AND 1=1";
        if (Yii::app()->user->role != 1) {
            $condition .= " AND requested_by=" . Yii::app()->user->id;
        }
        $notification = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}notifications WHERE view_status = 0 " . $condition . " ORDER BY id desc")->queryAll();
        

        $notification_count = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}notifications WHERE view_status = 0 " . $condition . " ORDER BY id desc")->queryAll();
        $b = count($pending_edit) + count($pending_delete);
        foreach ($notification as $key => $value) {
            $final_array[$b]['id'] = $value['id'];
            $final_array[$b]['date'] = $value['date'];
            $final_array[$b]['parent_id'] = $value['parent_id'];
            $final_array[$b]['type'] = 'notification';
            $final_array[$b]['user_id'] = '';
            $final_array[$b]['table'] = 'notification';
            $b++;
        }

        $this->updateScQuotationStatusBasedOnItemsStatus();
        $scquotation=array();
        if (isset(Yii::app()->user->role) && (in_array('/dashboard/scquotation', Yii::app()->user->menuauthlist))) {
            $scquotation = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}scquotation WHERE approve_status = 'No' AND viewed_status != 1 ORDER BY scquotation_id desc")->queryAll();
        }
        $scquotation_count = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}scquotation WHERE approve_status = 'No' ORDER BY scquotation_id desc")->queryAll();
        $c = count($pending_edit) + count($pending_delete) + count($notification);
        foreach ($scquotation as $key => $value) {
            $final_array[$c]['id'] = $value['scquotation_id'];
            $final_array[$c]['date'] = $value['scquotation_date'];
            $final_array[$c]['parent_id'] = $value['scquotation_id'];
            $final_array[$c]['type'] = 'scquotation';
            $final_array[$c]['user_id'] = $value['created_by'];
            $final_array[$c]['table'] = 'scquotation';
            $final_array[$c]['sc_qtn_amount'] = !empty($value['scquotation_amount']) ? $value['scquotation_amount'] : '0';
            $c++;
        }

        $po_count = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}purchase WHERE permission_status ='No'  ORDER BY p_id desc")->queryAll();
        $poapproval = array();
        if (isset(Yii::app()->user->role) && (in_array('/dashboard/poapproval', Yii::app()->user->menuauthlist))) {
            $poapproval = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}purchase WHERE permission_status ='No' ORDER BY p_id desc")->queryAll();
        }
        $d = count($pending_edit) + count($pending_delete) + count($notification) + count($scquotation);
        foreach ($poapproval as $key => $value) {
            $final_array[$d]['id'] = $value['p_id'];
            $final_array[$d]['date'] = $value['purchase_date'];
            $final_array[$d]['parent_id'] = '';
            $final_array[$d]['type'] = 'po_approval';
            $final_array[$d]['user_id'] = $value['created_by'];
            $final_array[$d]['table'] = 'purchase';
            $d++;
        }
        $expense_notfcts = ExpenseNotification::model()->findAllByAttributes(array('view_status' => 1));
        foreach ($expense_notfcts as $key => $value) {
            $final_array[$d]['id'] = $value['id'];
            $final_array[$d]['date'] = $value['generated_date'];
            $final_array[$d]['parent_id'] = '';
            $final_array[$d]['type'] = 'expense_notification';
            $final_array[$d]['user_id'] = '';
            $final_array[$d]['table'] = 'expense_notification';
            $d++;
        }


        array_multisort(array_map(function ($element) {
            return $element['date'];
        }, $final_array), SORT_DESC, $final_array);

        $alldata = new CArrayDataProvider($final_array, array(
            'id' => 'id',
            'keyField' => 'id',
            'pagination' => array('pageSize' => 5),
        ));

        $all = count($pending_edit) + count($pending_delete) + count($notification) + count($scquotation) + count($poapproval) + $expense_notifications_count;
        if (isset($_GET['Projects']['project_id']) && !empty($_GET['Projects']['project_id'])) {
            $project_id = $_GET['Projects']['project_id'];
        } else {
            $project_id = '';
        }

        if (isset($_GET['Projects']['company_id']) && !empty($_GET['Projects']['company_id'])) {
            $company_id = $_GET['Projects']['company_id'];
        } else {
            $company_id = '';
        }

        $finance_model = new Projects('financialreport1');
        if (isset($_GET['Projects'])) {
            $finance_model->attributes = $_GET['Projects'];
        }
        
        $financialdata1 = $finance_model->financialreport1();
        $fixedprojects = $finance_model->fixedprojects();
        $completed = $finance_model->completed();
        $buyer_module = GeneralSettings::model()->checkBuyerModule();
        $editrequest = PreExpenses::model()->findAll(array('condition' => 'approval_status="0"'));
        $mrmodel ='';
        $mrrequest_count = 0;
        if($this->getActiveTemplate()=='TYPE-4'){
            $mrmodel =  MaterialRequisition::model()->findAll(array('condition' => 'requisition_status="1" and purchase_id IS NULL'));;
            $mrrequest_count = count($mrmodel);
        }
        /**changes by rev starts */
       
        $pending_invoice = Invoice::model()->findAll(array('condition' => 'invoice_status="draft" and status_id=108'));
        $pending_inv_count=count($pending_invoice); 
        $unassignedWarehouseCount = Warehouse::model()->count('assigned_to IS NULL');
         /**changes by rev ends */
        $this->render('notification', array(
            'model' => $sql,
            'newmodel' => $sql1,
            'editmodel' => $editmodel->pendingsearch(),
            'editrequest' => $editrequest,
            'deletemodel' => $deletemodel->pendingsearch(),
            'notifications' => $notifications->notificationsearch(),
            'pendingquotation' => $pendingquotation->notification_search(),
            'alldata' => $alldata,
            'edit_count' => count($pending_edit),
            'delete_count' => count($pending_delete),
            'notification_count' => count($notification_count),
            'scquotation_count' => count($scquotation_count),
            'all_count' => $all,
            'purchaseorder' => $purchse->getapprovepo(),
            'po_count' => count($po_count),
            'expensenotifications' => $expense_notifications->notification_search(),
            'expense_notifications_count' => $expense_notifications_count,
            'finance_model' => $finance_model,
            'project_id' => $project_id,
            'financialdata1' => $financialdata1,
            'company_id' => $company_id,
            'buyer_module' => $buyer_module,
            'fixedprojects' => $fixedprojects,
            'completed' => $completed,
            'mrrequestcount' => $mrrequest_count,
            'mrmodel' => $mrmodel,
            'pending_invoice_count'=>$pending_inv_count,
            'unassigned_wh_count'=>$unassignedWarehouseCount
        ));
    }

    public function updateScQuotationStatusBasedOnItemsStatus()
    {
        $scquotations = Scquotation::model()->findAll(array('condition' => 'approve_status = "No"'));
        if (!empty($scquotations)) {
            foreach ($scquotations as $scquotation) {
                $quotation_items = ScquotationItems::model()->findAll(array('condition' => 'scquotation_id = ' . $scquotation->scquotation_id));
                if (!empty($quotation_items)) {
                    $unapproved_quotation_items = ScquotationItems::model()->findAll(array('condition' => 'scquotation_id = ' . $scquotation->scquotation_id . ' AND approve_status = "No"'));
                    if (empty($unapproved_quotation_items)) {
                        $scquotation->approve_status = 'Yes';
                        $scquotation->save();
                    }
                }
            }
        }
    }

    public function getExpenseNotifications()
    {
        $expense_percentage = 0;

        $companies = Company::model()->findAll(array('condition' => 'expenses_percentage IS NOT NULL'));
        foreach ($companies as $company) {
            $company_expence_percentage = $company['expenses_percentage'];
            $projects = Projects::model()->findAllByAttributes(array('company_id' => $company['id']));
            $expense_percentages_array = explode(',', $company_expence_percentage);
            foreach ($projects as $project) {
                $expense_notification_model = new ExpenseNotification();
                $expense_total = (int) $project['tot_expense'];
                $advance_total = (int) $project['tot_receipt'];

                if (!empty($advance_total) && $advance_total != 0)
                    $expense_percentage =  round(($expense_total / $advance_total) * 100);
                foreach ($expense_percentages_array as $percentage) {

                    if ($expense_percentage >= $percentage) {
                        $check_exist = ExpenseNotification::model()->findByAttributes(array('project_id' => $project['pid'], 'expense_perc' => $percentage));
                        if (empty($check_exist)) {
                            $expense_notification_model->project_id = $project['pid'];
                            $expense_notification_model->expense_perc = $percentage;
                            $expense_notification_model->expense_amount = $expense_total;
                            $expense_notification_model->advance_amount = $advance_total;
                            $expense_notification_model->generated_date = date('Y-m-d');
                            $expense_notification_model->view_status = 1;
                        }
                    }
                }
                

                $expense_notification_model->save();
            }
        }
    }

    public function actionSetsessioncompany()
    {
        $id = $_POST['company_id'];
        Yii::app()->user->setState('company_id', $id);

        echo json_encode(array('response' => 'success'));
    }

    public function actionUpdaterequestaction($id, $status,$tableName, $modelName, $parentName)
    {
        $status = $_REQUEST["status"];
        $usermodel = Users::model()->findByPk(Yii::app()->user->id);
        $premodel = $modelName::model()->findByPk($id);
        $userid = "";
        $payment_type = "";

        $premodel = $modelName::model()->findByPk($id);
            $userid = "";
            $payment_type = "";
            $userid = "";
        $payment_type = "";

        if ($tableName == $this->tableNameAcc('pre_expenses',0) ){
            $type = 1;
            $paymetName = 'Daybook';
            $parentTable = $this->tableNameAcc('expenses', 0);
            $payment_type = $premodel->expense_type;
            $userid = $premodel->userid;
        } else if ($tableName == $this->tableNameAcc('pre_dailyvendors',0)) {
            $type = 2;
            $parentTable = $this->tableNameAcc('dailyvendors', 0);
            $payment_type = $premodel->payment_type;
            $userid = $premodel->updated_by;
            $paymetName = 'Vendor Payment';
        } else if ($tableName == $this->tableNameAcc('pre_subcontractor_payment',0)) {
            $type = 3;
            $parentTable = $this->tableNameAcc('subcontractor_payment', 0);
            $payment_type = $premodel->payment_type;
            $userid = $premodel->updated_by;            
            $paymetName = 'Subcontractor Payment';
        } else  if ($tableName == $this->tableNameAcc('pre_dailyexpenses',0)){
            $type = 4;
            $parentTable = $this->tableNameAcc('dailyexpense', 0);
            $payment_type = $premodel->expense_type;
            $userid = $premodel->updated_by;           
            $paymetName = 'Dailyexpense Payment';
        }else{
            $type = 5;
            $parentTable = $this->tableNameAcc('dailyreport', 0);
            $payment_type = "";
            $userid = $premodel->updated_by;           
            $paymetName = 'Dailyreport Payment';
        }

        $notification = ApproveRequestHelper::notificationData(
                        $id, $userid
        );

        $transaction = Yii::app()->db->beginTransaction();
        try {
            if ($status == 1) {

                $oldmodel = $parentName::model()->findByPk($premodel->ref_id);
                $updateData = $this->updateModelData(
                        $payment_type,
                        $premodel,
                        $oldmodel,
                        $type,
                        $paymetName
                );

                $model = $updateData['model'];
                $reconmodel = $updateData['reconmodel'];

                if ($type == 4) {
                    if($model->exp_type_id ==25 || $model->exp_type_id ==26){
                        $dailyExpChildModel = $updateData['dailyExpChildModel'];
                    }
                }

                if ($type == 3) {
                    $expModel = $updateData['expenseModel'];
                }                

                if ($model->save()) {
                    if ($type == 4) {
                        if($model->exp_type_id ==25 || $model->exp_type_id ==26){
                            if (!$dailyExpChildModel->save()) {
                                $success_status = 0;
                                throw new Exception(json_encode($dailyExpChildModel->getErrors()));
                            }
                        }
                    }

                    if ($type == 3) {
                        if (!$expModel->save()) {
                            $success_status = 0;
                            throw new Exception(json_encode($expModel->getErrors()));
                        }
                    }

                    $premodel->approval_status = 1;
                    $premodel->approved_by = Yii::app()->user->id;
                    $premodel->cancelled_at = date('Y-m-d H:i:s');

                    if (!$premodel->save()) {
                        $success_status = 0;
                        throw new Exception(json_encode($premodel->getErrors()));
                    }

                    $notification->message = "Update request in $paymetName for "
                            . "Rs." . Controller::money_format_inr($premodel->amount, 2) . " "
                            . "of date " . date("d-m-Y", strtotime($premodel->date)) . " "
                            . "is approved by " . $usermodel->first_name . " " .
                            $usermodel->last_name;

                    if ($payment_type == 88) {
                        if(!empty($reconmodel)){
                            if (!$reconmodel->save()) {
                            $success_status = 0;
                            throw new Exception(json_encode($reconmodel->getErrors()));
                        }
                        }
                        
                    }

                    $notification->save();
                    $id = $this->getPaymentId($model,$type);                    

                    ApproveRequestHelper::createLog(
                            $model->attributes,
                            $parentTable,
                            $id
                    );

                    $success_status = 1;
                    $msg = "Success! Request approved successfully.";
                } else {
                    $success_status = 0;
                    throw new Exception(json_encode($model->getErrors()));
                }
            } else if ($status == 2) { //cancel request

                $premodel->approval_status = 2;
                $premodel->approve_notify_status = 0;
                $premodel->cancelled_by = Yii::app()->user->id;
                $premodel->cancelled_at = date('Y-m-d H:i:s');
                if (!$premodel->save()) {
                    $success_status = 0;
                    throw new Exception(json_encode($premodel->getErrors()));
                }
                
                    $model = $parentName::model()->findByPk($premodel->ref_id);
                    $sql = "SELECT COUNT(*) as count "
                        . " FROM " . $tableName . " "
                        . " WHERE `approval_status` = '0' "
                        . " AND `ref_id` = '" . $premodel->ref_id . "' "
                            . " ORDER BY `approve_notify_status` ASC";

                    $pendingcount = Yii::app()->db->createCommand($sql)->queryRow();
                    $count = $pendingcount['count'];
                    if ($count == 0) {
                        $model->approval_status = 1;
                        if ($model->save()) {
                            $success_status = 1;
                            $msg = "Success! Request cancelled successfully.";
                        } else {
                            $success_status = 0;
                            throw new Exception(json_encode($model->getErrors()));
                        }
                    }
                
            }
            $transaction->commit();
        } catch (Exception $error) {
            $transaction->rollBack();
            $success_status = 0;
            $error =  $error->getMessage();
        } 
        finally {
            if ($success_status == 1) {
                Yii::app()->user->setFlash('success', $msg);
                echo json_encode(array('response' => 'success', 'msg' => $msg));
            } else {
                Yii::app()->user->setFlash('error', $error);
            }
        }
    }

    public function actionDeleterequestaction()
    {
        $delId = $_REQUEST["delId"];
        $status = $_REQUEST["status"];
        $tblpx = Yii::app()->db->tablePrefix;
        $request = Deletepending::model()->findByPk($delId);
        $usermodel = Users::model()->findByPk(Yii::app()->user->id);
        $notification = new Notifications;
        $notification->action = "Delete";
        $notification->parent_id = $request->deletepending_id;
        $notification->date = date("Y-m-d");
        $notification->requested_by = $request->user_id;
        $notification->approved_by = Yii::app()->user->id;
        $parentTable = $request->deletepending_table;
        if ($status == 1) {
            $transaction = Yii::app()->db->beginTransaction();
            $parentId = $request->deletepending_parentid;
            
            try{
                if ($parentTable == "{$tblpx}expenses") {
                    $data = Expenses::model()->findByPk($parentId);
                    $tblpx = Yii::app()->db->tablePrefix;
                    if ($data) {
                        $newmodel = new JpLog('search');
                        $newmodel->log_data = json_encode($data->attributes);
                        $newmodel->log_action = 2;
                        $newmodel->log_table = $tblpx . "expenses";
                        $newmodel->log_datetime = date('Y-m-d H:i:s');
                        $newmodel->log_action_by = Yii::app()->user->id;
                        $newmodel->company_id = $data->company_id;
                        if ($newmodel->save()) {
                            $success_status = 1;
                            
                            $model = Expenses::model()->deleteAll(array("condition" => "exp_id=" . $parentId . ""));
                            if($model){
                                $success_status = 1;
                            }else{
                                $success_status = 0;
                                throw new Exception(json_encode($model->getErrors()));
                               }
                            $is_rec_entry = Reconciliation::model()->findAll(array("condition" => "reconciliation_parentid=" . $parentId . ""));
                            if(count($is_rec_entry)>0){
                                $recmodel = Reconciliation::model()->deleteAll(array("condition" => "reconciliation_parentid=" . $parentId . ""));
                                if($recmodel){
                                    $success_status = 1;
                                }else{
                                    $success_status = 0;
                                    throw new Exception(json_encode($recmodel->getErrors()));
                                }
                            }
                        }else{
                            $success_status = 0;
					        throw new Exception(json_encode($newmodel->getErrors()));
                        }
                    }
                    $notification->message = "Delete request in Daybook is approved by " . $usermodel->first_name . " " . $usermodel->last_name;
                } else if ($parentTable == "{$tblpx}dailyexpense") {
                    $data = Dailyexpense::model()->findByPk($parentId);
                    $tblpx = Yii::app()->db->tablePrefix;
                    $user = Users::model()->findByPk(Yii::app()->user->id);
                    $arrVal = explode(',', $user->company_id);
                    $newQuery = "";
                    foreach ($arrVal as $arr) {
                        if ($newQuery)
                            $newQuery .= ' OR';
                        $newQuery .= " FIND_IN_SET('" . $arr . "', e.company_id)";
                    }
                    $transferId = $data->transfer_parentid;
                    if ($transferId > 0) {
                        $transferData = Cashtransfer::model()->findByPk($transferId);
                        $dailyexpense = Dailyexpense::model()->findAll(array('condition' => 'transfer_parentid="' . $transferId . '"'));
                        Dailyexpense::model()->deleteAll(array('condition' => 'transfer_parentid = ' . $transferId));
                        Cashtransfer::model()->deleteAll(array('condition' => 'cashtransfer_id = ' . $transferId));
                    } else {
                        $newmodel = new JpLog('search');
                        $newmodel->log_data = json_encode($data->attributes);
                        $newmodel->log_action = 2;
                        $newmodel->log_table = $tblpx . "dailyexpense";
                        $newmodel->log_datetime = date('Y-m-d H:i:s');
                        $newmodel->log_action_by = Yii::app()->user->id;
                        $newmodel->company_id = $data->company_id;
                        if ($newmodel->save()) {
                            if ($data->expensehead_type == 4 || $data->expensehead_type == 5) {
                                $model1 = Dailyexpense::model()->deleteAll(array("condition" => "transaction_parent=" . $data->dailyexp_id . ""));
                                if($model1){
                                    $success_status = 1;
                                }else{
                                    $success_status = 0;
                                    throw new Exception(json_encode($model1->getErrors()));
                                }
                            }
                            $model = Dailyexpense::model()->deleteAll(array("condition" => "dailyexp_id=" . $parentId . ""));
                            if($model){
                                $success_status = 1;
                            }else{
                                $success_status = 0;
                                throw new Exception(json_encode($model->getErrors()));
                            }
                            $is_rec_entry = Reconciliation::model()->findAll(array("condition" => "reconciliation_parentid=" . $parentId . ""));
                            if(count($is_rec_entry)>0){
                                $recmodel = Reconciliation::model()->deleteAll(array("condition" => "reconciliation_parentid=" . $parentId . ""));
                                if($recmodel){
                                    $success_status = 1;
                                }else{
                                    $success_status = 0;
                                    throw new Exception(json_encode($recmodel->getErrors()));
                                }
                            }
                        }else{
                            $success_status = 0;
                            throw new Exception(json_encode($newmodel->getErrors()));
                        }
                    }
                    $notification->message = "Delete request in Daily Expense is approved by " . $usermodel->first_name . " " . $usermodel->last_name;
                } else if ($parentTable == "{$tblpx}dailyvendors") {
                    $data = Dailyvendors::model()->findByPk($parentId);
                    $tblpx = Yii::app()->db->tablePrefix;
                    if ($data) {
                        $newmodel = new JpLog('search');
                        $newmodel->log_data = json_encode($data->attributes);
                        $newmodel->log_action = 2;
                        $newmodel->log_table = $tblpx . "dailyvendors";
                        $newmodel->log_datetime = date('Y-m-d H:i:s');
                        $newmodel->log_action_by = Yii::app()->user->id;
                        $newmodel->company_id = $data->company_id;
                        if ($newmodel->save()) {
                            $model = Dailyvendors::model()->deleteAll(array("condition" => "daily_v_id=" . $parentId . ""));
                            if($model){
                                $success_status = 1;
                            }else{
                                $success_status = 0;
                                throw new Exception(json_encode($model->getErrors()));
                            }
                            $is_rec_entry = Reconciliation::model()->findAll(array("condition" => "reconciliation_parentid=" . $parentId . ""));
                            if(count($is_rec_entry)>0){
                                $recmodel = Reconciliation::model()->deleteAll(array("condition" => "reconciliation_parentid=" . $parentId . ""));
                                if($recmodel){
                                    $success_status = 1;
                                }else{
                                    $success_status = 0;
                                    throw new Exception(json_encode($recmodel->getErrors()));
                                }
                            }
                        }else{
                            $success_status = 0;
                            throw new Exception(json_encode($newmodel->getErrors()));
                        }
                    }
                    $notification->message = "Delete request in Vendor Payment is approved by " . $usermodel->first_name . " " . $usermodel->last_name;
                } else if ($parentTable == "{$tblpx}subcontractor_payment") {
                    $data = SubcontractorPayment::model()->findByPk($parentId);
                    $uniqueid = $data->uniqueid;
                    $tblpx = Yii::app()->db->tablePrefix;
                    if ($uniqueid) {
                        $paymentdata = SubcontractorPayment::model()->findAll(array("condition" => "uniqueid='" . $uniqueid . "'"));
                        foreach ($paymentdata as $pdata) {
                            $newmodel = new JpLog('search');
                            $newmodel->log_data = json_encode($pdata->attributes);
                            $newmodel->log_action = 2;
                            $newmodel->log_table = $tblpx . "subcontractor_payment";
                            $newmodel->log_datetime = date('Y-m-d H:i:s');
                            $newmodel->log_action_by = Yii::app()->user->id;
                            $newmodel->company_id = $pdata->company_id;
                            if ($newmodel->save()) {
                                $model = SubcontractorPayment::model()->deleteAll(array("condition" => "payment_id=" . $pdata->payment_id . ""));
                                if($model){
                                    $success_status = 1;
                                }else{
                                    $success_status = 0;
                                    throw new Exception(json_encode($model->getErrors()));
                                }
                                $model1 = Expenses::model()->deleteAll(array("condition" => "subcontractor_id=" . $pdata->payment_id . ""));
                                if($model1){
                                    $success_status = 1;
                                }else{
                                    $success_status = 0;
                                    throw new Exception(json_encode($model1->getErrors()));
                                }
                            }else{
                                $success_status = 0;
                                throw new Exception(json_encode($newmodel->getErrors()));
                            }
                        }
                        $is_rec_entry = Reconciliation::model()->findAll(array("condition" => "reconciliation_parentids = '" . $uniqueid . "'"));
                        if(count($is_rec_entry)>0){
                            $recmodel = Reconciliation::model()->deleteAll(array("condition" => "reconciliation_parentids = '" . $uniqueid . "'"));
                            if($recmodel){
                                $success_status = 1;
                            }else{
                                $success_status = 0;
                                throw new Exception(json_encode($recmodel->getErrors()));
                            }
                        }
                    } else {
                        if ($data) {
                            $newmodel = new JpLog('search');
                            $newmodel->log_data = json_encode($data->attributes);
                            $newmodel->log_action = 2;
                            $newmodel->log_table = $tblpx . "subcontractor_payment";
                            $newmodel->log_datetime = date('Y-m-d H:i:s');
                            $newmodel->log_action_by = Yii::app()->user->id;
                            $newmodel->company_id = $data->company_id;
                            if ($newmodel->save()) {
                                $expLoad = Expenses::model()->find(array(
                                    'select' => array('exp_id'),
                                    "condition" => "subcontractor_id='" . $parentId . "'",
                                ));
                                $expOldId = $expLoad["exp_id"];
                                if ($expLoad) {
                                    $recmodel = Reconciliation::model()->deleteAll(array("condition" => "reconciliation_parentid=" . $expOldId . ""));
                                    if($recmodel){
                                        $success_status = 1;
                                    }else{
                                        $success_status = 0;
                                        throw new Exception(json_encode($recmodel->getErrors()));
                                    }
                                }
                                $scmodel = SubcontractorPayment::model()->deleteAll(array("condition" => "payment_id=" . $parentId . ""));
                                if($scmodel){
                                    $success_status = 1;
                                }else{
                                    $success_status = 0;
                                    throw new Exception(json_encode($scmodel->getErrors()));
                                }
                                $exmodel = Expenses::model()->deleteAll(array("condition" => "subcontractor_id=" . $parentId . ""));
                                if($exmodel){
                                    $success_status = 1;
                                }else{
                                    $success_status = 0;
                                    throw new Exception(json_encode($exmodel->getErrors()));
                                }
                            }else{
                                $success_status = 0;
                                throw new Exception(json_encode($newmodel->getErrors()));
                            }
                        }
                    }
                    $notification->message = "Delete request in Subcontractor Payment is approved by " . $usermodel->first_name . " " . $usermodel->last_name;
                } else if ($parentTable == "{$tblpx}dailyreport") {
                    $data = Dailyreport::model()->findByPk($parentId);
                    $tblpx = Yii::app()->db->tablePrefix;
                    if ($data) {
                        $newmodel = new JpLog('search');
                        $newmodel->log_data = json_encode($data->attributes);
                        $newmodel->log_action = 2;
                        $newmodel->log_table = $tblpx . "dailyreport";
                        $newmodel->log_datetime = date('Y-m-d H:i:s');
                        $newmodel->log_action_by = Yii::app()->user->id;
                        $newmodel->company_id = $data->company_id;
                        if ($newmodel->save()) {
                            $model = Dailyreport::model()->deleteAll(array("condition" => "dr_id=" . $parentId . ""));
                            if($model){
                                $success_status = 1;
                            }else{
                                $success_status = 0;
                                throw new Exception(json_encode($model->getErrors()));
                            }
                        }else{
                            $success_status = 0;
                            throw new Exception(json_encode($newmodel->getErrors()));
                        }
                    }
                    $notification->message = "Delete request in Daily Report is approved by " . $usermodel->first_name . " " . $usermodel->last_name;
                }else if ($parentTable == "{$tblpx}invoice") {
                    $data = Invoice::model()->findByPk($parentId);
                    $payment_stage = PaymentStage::model()->findByPk($data->stage_id);                
                    $tblpx = Yii::app()->db->tablePrefix;                
                    if ($data) {
                        $newmodel = new JpLog('search');
                        $newmodel->log_data = json_encode($data->attributes);
                        $newmodel->log_action = 2;
                        $newmodel->log_table = $tblpx . "invoice";
                        $newmodel->log_datetime = date('Y-m-d H:i:s');
                        $newmodel->log_action_by = Yii::app()->user->id;
                        $newmodel->company_id = $data->company_id;
                        if ($newmodel->save()) {
                            $invoice = Invoice::model()->deleteAll(array("condition" => "invoice_id=" . $data->invoice_id . "")); 
                            if($invoice){
                                $success_status = 1;
                                if(!empty($payment_stage)){
                                    $payment_stage->invoice_status = "0";
                                    if (!$payment_stage->save()) {
                                        $success_status = 0;
                                        throw new Exception(json_encode($payment_stage->getErrors()));
                                    } else {
                                        $success_status = 1;					
                                    }
                                }
                            }else{
                                $success_status = 0;
                                throw new Exception(json_encode($invoice->getErrors()));
                            }                           
                        }else{
                            $success_status = 0;
                            throw new Exception(json_encode($newmodel->getErrors()));
                        }
                    }
                    $notification->message = "Delete request in Invoice is approved by " . $usermodel->first_name . " " . $usermodel->last_name;                                      
                } else if ($parentTable == "{$tblpx}quotation") {
                    $data = Quotation::model()->findByPk($parentId);                
                    $tblpx = Yii::app()->db->tablePrefix;
                     $paymentStages = PaymentStage::model()->findAllByAttributes(['quotation_id' => $parentId]);
                  
                            $invoiceExists='';
                            $payment_stage_id= array();
                            $invoiceExists = false; // Initialize invoice existence flag

                        if (!empty($paymentStages)) {
                            foreach ($paymentStages as $stage) {
                                // Check if any invoice exists for the current stage
                                $exist = Invoice::model()->findByAttributes(['stage_id' => $stage->id]);
                                if (!empty($exist)) {
                                    $invoiceExists = true; // Invoice exists for at least one stage
                                    break; // No need to check further
                                }
                            }
                        }
                        
                       
                        if ($invoiceExists) {
                            // Update status to 3
                            $success_status=0;
                            throw new Exception( json_encode(array('response' => 'warning', 'msg' => 'Cannot Delete ! This record alredy in use.')));
                           
                        }else{
                                
                        $newmodel = new JpLog('search');
                        $newmodel->log_data = json_encode($data->attributes);
                        $newmodel->log_action = 2;
                        $newmodel->log_table = $tblpx . "quotation";
                        $newmodel->log_datetime = date('Y-m-d H:i:s');
                        $newmodel->log_action_by = Yii::app()->user->id;
                        $newmodel->company_id = $data->company_id;
                        if ($newmodel->save()) {
                            $qmodel = Quotation::model()->deleteAll(array("condition" => "quotation_id=" . $data->quotation_id . ""));
                            if($qmodel){
                                $success_status = 1;
                            }else{
                                $success_status = 0;
                                throw new Exception(json_encode($qmodel->getErrors()));
                            }                            
                        } else{
                            $success_status = 0;
                            throw new Exception(json_encode($newmodel->getErrors()));
                        }                                       
                    }
                } else if ($parentTable == "{$tblpx}bills") {
                    $data = Bills::model()->findByPk($parentId);                
                    $tblpx = Yii::app()->db->tablePrefix;
                    if ($data) {                                        
                        $newmodel = new JpLog('search');
                        $newmodel->log_data = json_encode($data->attributes);
                        $newmodel->log_action = 2;
                        $newmodel->log_table = $tblpx . "bills";
                        $newmodel->log_datetime = date('Y-m-d H:i:s');
                        $newmodel->log_action_by = Yii::app()->user->id;
                        $newmodel->company_id = $data->company_id;
                        if ($newmodel->save()) {
                            $bmodel = Bills::model()->deleteAll(array("condition" => "bill_id=" . $data->bill_id . "")); 
                            if($bmodel){
                                $success_status = 1;
                            }else{
                                $success_status = 0;
                                throw new Exception(json_encode($bmodel->getErrors()));
                            }                           
                        } else{
                            $success_status = 0;
                            throw new Exception(json_encode($newmodel->getErrors()));
                        }                                       
                    }
                }else if ($parentTable == "{$tblpx}sc_payment_stage") {
                    $data = ScPaymentStage::model()->findByPk($parentId);
                    $tblpx = Yii::app()->db->tablePrefix;
                    if ($data) {
                        $newmodel = new JpLog('search');
                        $newmodel->log_data = json_encode($data->attributes);
                        $newmodel->log_action = 2;
                        $newmodel->log_table = $tblpx . "sc_payment_stage";
                        $newmodel->log_datetime = date('Y-m-d H:i:s');
                        $newmodel->log_action_by = Yii::app()->user->id;
                        $newmodel->company_id = '';
                        if ($newmodel->save()) {
                            $model = ScPaymentStage::model()->deleteAll(array("condition" => "id=" . $parentId . ""));   
                            if($model){
                                $success_status = 1;
                            }else{
                                $success_status = 0;
                                throw new Exception(json_encode($model->getErrors()));
                            }                      
                        }else{
                            $success_status = 0;
                            throw new Exception(json_encode($newmodel->getErrors()));
                        }
                    }
                    $notification->message = "Delete request in Subcontractor Payment stage is approved by " . $usermodel->first_name . " " . $usermodel->last_name;
                }else if ($parentTable == "{$tblpx}subcontractor") {
                    $data = Subcontractor::model()->findByPk($parentId);
                    $tblpx = Yii::app()->db->tablePrefix;
                    if ($data) {
                        $newmodel = new JpLog('search');
                        $newmodel->log_data = json_encode($data->attributes);
                        $newmodel->log_action = 2;
                        $newmodel->log_table = $tblpx . "subcontractor";
                        $newmodel->log_datetime = date('Y-m-d H:i:s');
                        $newmodel->log_action_by = Yii::app()->user->id;
                        $newmodel->company_id = '';
                        if ($newmodel->save()) {
                            $model = Subcontractor::model()->deleteAll(array("condition" => "subcontractor_id=" . $parentId . ""));   
                            if($model){
                                $success_status = 1;
                            }else{
                                $success_status = 0;
                                throw new Exception(json_encode($model->getErrors()));
                            }                      
                        }else{
                            $success_status = 0;
                            throw new Exception(json_encode($newmodel->getErrors()));
                        }
                       
                    }
                    $notification->message = "Delete request in Subcontractor is approved by " . $usermodel->first_name . " " . $usermodel->last_name;
                }

                $request->deletepending_status = 1;
                if($request->save()){
                    $success_status = 1;
                }else{
                    $success_status = 0;
                    throw new Exception(json_encode($request->getErrors()));
                }
                
                if($notification->save()){
                    $success_status = 1;
                }else{
                    $success_status = 0;
                    throw new Exception(json_encode($notification->getErrors()));
                }

                $transaction->commit();
            }catch(Exception $error){
                $transaction->rollBack();
                $success_status = 0;
            }finally{
                
                if ($success_status == 1) {
                    echo json_encode(array('response' => 'success', 'msg' => 'Request successfully granted'));                                       
                } else {
                    if (isset($error->errorInfo) && ($error->errorInfo[1] == 1451)) {                         
                        echo json_encode(array('response' => 'warning', 'msg' => 'Cannot Delete ! This record alredy in use.'));
                    } else {
                        echo json_encode(array('response' => 'error', 'msg' => 'Some Error Occured'));
                    }
                }
            }
            // ends
            
        } else if ($status == 2) {
            $request->deletepending_status = 2;
            $parentId = $request->deletepending_parentid;   
            $transaction = Yii::app()->db->beginTransaction();         
            try{
                if ($parentTable == "{$tblpx}dailyexpense") {
                    $data = Dailyexpense::model()->findByPk($parentId);
                    $notification->message = "Delete request in Daily Expenses is cancelled by " . $usermodel->first_name . " " . $usermodel->last_name;
                } else if ($parentTable == "{$tblpx}subcontractor_payment") {
                    $data = SubcontractorPayment::model()->findByPk($parentId);
                    $notification->message = "Delete request in Subcontractor Payment is cancelled by " . $usermodel->first_name . " " . $usermodel->last_name;
                } else if ($parentTable == "{$tblpx}expenses") {
                    $data = Expenses::model()->findByPk($parentId);
                    $notification->message = "Delete request in Daybook Payment is cancelled by " . $usermodel->first_name . " " . $usermodel->last_name;
                } else if ($parentTable == "{$tblpx}dailyvendors") {
                    $data = Dailyvendors::model()->findByPk($parentId);
                    $notification->message = "Delete request in Vendor Payment is cancelled by " . $usermodel->first_name . " " . $usermodel->last_name;
                } else if ($parentTable == "{$tblpx}dailyreport") {
                    $data= array();
                    $notification->message = "Delete request in Daily Report is cancelled by " . $usermodel->first_name . " " . $usermodel->last_name;
                } else if ($parentTable == "{$tblpx}invoice") {
                    $data= array();
                    $invoiceData = Invoice::model()->findByPk($parentId);
                    $invoiceData->delete_approve_status = '0';
                    $invoiceData->save();
                    $notification->message = "Delete request in Invoice is cancelled by " . $usermodel->first_name . " " . $usermodel->last_name;
                } else if ($parentTable == "{$tblpx}quotation") {
                    $data= array();
                    $quotationData = Invoice::model()->findByPk($parentId);
                    $quotationData->delete_approve_status = '0';
                    $quotationData->save();
                    $notification->message = "Delete request in Quotation is cancelled by " . $usermodel->first_name . " " . $usermodel->last_name;
                } else if ($parentTable == "{$tblpx}bills") {
                    $data= array();
                    $notification->message = "Delete request in Bills is cancelled by " . $usermodel->first_name . " " . $usermodel->last_name;
                }
                if(!empty($data)){
                    $data->duplicate_delete_status = '0';                    
                    if($data->save()){
                        $success_status = 1;
                    }else{
                        $success_status = 0;
                        throw new Exception(json_encode($data->getErrors()));
                    }  
                }

                
                if($request->save()){
                    $success_status = 1;
                }else{
                    $success_status = 0;
                    throw new Exception(json_encode($request->getErrors()));
                }
                
                if($notification->save()){
                    $success_status = 1;
                }else{
                    $success_status = 0;
                    throw new Exception(json_encode($notification->getErrors()));
                }
                $transaction->commit();
            }catch(Exception $error){
                $transaction->rollBack();
                $success_status = 0;
            }finally{
                
                if ($success_status == 1) {
                    echo json_encode(array('response' => 'success', 'msg' => 'Request successfully cancelled'));                                       
                }else{
                    echo json_encode(array('response' => 'error', 'msg' => 'Some Error Occured')); 
                }
            }
        } else {
            echo 1;
        }
    }

    public function actionAllupdaterequestaction()
    {
        $records = $_REQUEST["records"];
        $status = $_REQUEST["status"];
        $tblpx = Yii::app()->db->tablePrefix;
        $usermodel = Users::model()->findByPk(Yii::app()->user->id);
        foreach ($records as $record) {
            $id = $record['id'];
            $status = 1;
            $tableName = $record['tableName'];
            $modelName = $record['modelName'];
            $parentName = $record['parentName'];

            $premodel = $modelName::model()->findByPk($id);
            $userid = "";
            $payment_type = "";
            $userid = "";
        $payment_type = "";

        if ($tableName == $this->tableNameAcc('pre_expenses',0) ){
            $type = 1;
            $paymetName = 'Daybook';
            $parentTable = $this->tableNameAcc('expenses', 0);
            $payment_type = $premodel->expense_type;
            $userid = $premodel->userid;
        } else if ($tableName == $this->tableNameAcc('pre_dailyvendors',0)) {
            $type = 2;
            $parentTable = $this->tableNameAcc('dailyvendors', 0);
            $payment_type = $premodel->payment_type;
            $userid = $premodel->updated_by;
            $paymetName = 'Vendor Payment';
        } else if ($tableName == $this->tableNameAcc('pre_subcontractor_payment',0)) {
            $type = 3;
            $parentTable = $this->tableNameAcc('subcontractor_payment', 0);
            $payment_type = $premodel->payment_type;
            $userid = $premodel->updated_by;            
            $paymetName = 'Subcontractor Payment';
        } else  if ($tableName == $this->tableNameAcc('pre_dailyexpenses',0)){
            $type = 4;
            $parentTable = $this->tableNameAcc('dailyexpense', 0);
            $payment_type = $premodel->expense_type;
            $userid = $premodel->updated_by;           
            $paymetName = 'Dailyexpense Payment';
        }else{
            $type = 5;
            $parentTable = $this->tableNameAcc('dailyreport', 0);
            $payment_type = "";
            $userid = $premodel->updated_by;           
            $paymetName = 'Dailyreport Payment';
        }

        $notification = ApproveRequestHelper::notificationData(
                        $id, $userid
        );

        $transaction = Yii::app()->db->beginTransaction();
        try {
            if ($status == 1) {

                $oldmodel = $parentName::model()->findByPk($premodel->ref_id);
                $updateData = $this->updateModelData(
                        $payment_type,
                        $premodel,
                        $oldmodel,
                        $type,
                        $paymetName
                );

                $model = $updateData['model'];
                $reconmodel = $updateData['reconmodel'];

                if ($type == 4) {
                    if($model->exp_type_id ==25 || $model->exp_type_id ==26){
                        $dailyExpChildModel = $updateData['dailyExpChildModel'];
                    }
                }

                if ($type == 3) {
                    $expModel = $updateData['expenseModel'];
                }                

                if ($model->save()) {
                    if ($type == 4) {
                        if($model->exp_type_id ==25 || $model->exp_type_id ==26){
                            if (!$dailyExpChildModel->save()) {
                                $success_status = 0;
                                throw new Exception(json_encode($dailyExpChildModel->getErrors()));
                            }
                        }
                    }

                    if ($type == 3) {
                        if (!$expModel->save()) {
                            $success_status = 0;
                            throw new Exception(json_encode($expModel->getErrors()));
                        }
                    }

                    $premodel->approval_status = 1;
                    $premodel->approved_by = Yii::app()->user->id;

                    if (!$premodel->save()) {
                        $success_status = 0;
                        throw new Exception(json_encode($premodel->getErrors()));
                    }

                    $notification->message = "Update request in $paymetName for "
                            . "Rs." . Controller::money_format_inr($premodel->amount, 2) . " "
                            . "of date " . date("d-m-Y", strtotime($premodel->date)) . " "
                            . "is approved by " . $usermodel->first_name . " " .
                            $usermodel->last_name;

                    if ($payment_type == 88) {
                        if(!empty($reconmodel)){
                            if (!$reconmodel->save()) {
                            $success_status = 0;
                            throw new Exception(json_encode($reconmodel->getErrors()));
                        }
                        }
                        
                    }

                    $notification->save();
                    $id = $this->getPaymentId($model,$type);                    

                    ApproveRequestHelper::createLog(
                            $model->attributes,
                            $parentTable,
                            $id
                    );

                    $success_status = 1;
                    $msg = "Success! Request approved successfully.";
                } else {
                    $success_status = 0;
                    throw new Exception(json_encode($model->getErrors()));
                }
            } else if ($status == 2) { //cancel request

                $premodel->approval_status = 2;
                $premodel->approve_notify_status = 0;
                $premodel->cancelled_by = Yii::app()->user->id;
                $premodel->cancelled_at = date('Y-m-d H:i:s');
                if (!$premodel->save()) {
                    $success_status = 0;
                    throw new Exception(json_encode($premodel->getErrors()));
                }
                
                    $model = $parentName::model()->findByPk($premodel->ref_id);
                    $sql = "SELECT COUNT(*) as count "
                        . " FROM " . $tableName . " "
                        . " WHERE `approval_status` = '0' "
                        . " AND `ref_id` = '" . $premodel->ref_id . "' "
                            . " ORDER BY `approve_notify_status` ASC";

                    $pendingcount = Yii::app()->db->createCommand($sql)->queryRow();
                    $count = $pendingcount['count'];
                    if ($count == 0) {
                        $model->approval_status = 1;
                        if ($model->save()) {
                            $success_status = 1;
                            $msg = "Success! Request cancelled successfully.";
                        } else {
                            $success_status = 0;
                            throw new Exception(json_encode($model->getErrors()));
                        }
                    }
                
            }
            $transaction->commit();
        } catch (Exception $error) {
            $transaction->rollBack();
            $success_status = 0;
            $error =  $error->getMessage();
        } finally {
            if ($success_status == 1) {
                Yii::app()->user->setFlash('success', $msg);
                echo json_encode(array('response' => 'success', 'msg' => $msg));
            } else {
                Yii::app()->user->setFlash('error', $error);
            }
        }
    }
        $newmodel = new Editrequest;
        return $this->widget('zii.widgets.CListView', array(
            'dataProvider' => $newmodel->pendingsearch(),
            'itemView' => '_neweditrequest', 'template' => '<div class=""><table cellpadding="10" class="table" id="clientab">{items}</table></div>',
            'ajaxUpdate' => false,
        ));
    }

    public function actionAlldeleterequestaction()
    {
        $deleteArray = $_REQUEST["deleteArray"];
        $status = $_REQUEST["status"];
        $tblpx = Yii::app()->db->tablePrefix;
        $usermodel = Users::model()->findByPk(Yii::app()->user->id);
        foreach ($deleteArray as $delId) {
            $request = Deletepending::model()->findByPk($delId);
            $notification = new Notifications;
            $notification->action = "Delete";
            $notification->parent_id = $request->deletepending_id;
            $notification->date = date("Y-m-d");
            $notification->requested_by = $request->user_id;
            $notification->approved_by = Yii::app()->user->id;
            $parentTable = $request->deletepending_table;
            if ($status == 1) {                
                $transaction = Yii::app()->db->beginTransaction();
                $parentId = $request->deletepending_parentid;
                
                try{
                    if ($parentTable == "{$tblpx}expenses") {
                        $data = Expenses::model()->findByPk($parentId);
                        $tblpx = Yii::app()->db->tablePrefix;
                        if ($data) {
                            $newmodel = new JpLog('search');
                            $newmodel->log_data = json_encode($data->attributes);
                            $newmodel->log_action = 2;
                            $newmodel->log_table = $tblpx . "expenses";
                            $newmodel->log_datetime = date('Y-m-d H:i:s');
                            $newmodel->log_action_by = Yii::app()->user->id;
                            $newmodel->company_id = $data->company_id;
                            if ($newmodel->save()) {
                                $success_status = 1;
                                $model = Expenses::model()->deleteAll(array("condition" => "exp_id=" . $parentId . ""));
                                    if(!empty($model)){
                                        if($model){
                                        $success_status = 1;
                                    }else{
                                        $success_status = 0;
                                        throw new Exception(json_encode("Error Occurred"));
                                    }

                                }
                                
                                $recmodel = Reconciliation::model()->deleteAll(array("condition" => "reconciliation_parentid=" . $parentId . ""));
                                 if(!empty($recmodel)){
                                    if($recmodel){
                                        $success_status = 1;
                                    }else{
                                        $success_status = 0;
                                        throw new Exception(json_encode("Some error occurred"));
                                    }
                                 }
                                
                            }else{
                                $success_status = 0;
                                throw new Exception(json_encode($newmodel->getErrors()));
                            }
                        }
                        $notification->message = "Delete request in Daybook is approved by " . $usermodel->first_name . " " . $usermodel->last_name;
                    } else if ($parentTable == "{$tblpx}dailyexpense") {
                        $data = Dailyexpense::model()->findByPk($parentId);
                        $tblpx = Yii::app()->db->tablePrefix;
                        $user = Users::model()->findByPk(Yii::app()->user->id);
                        $arrVal = explode(',', $user->company_id);
                        $newQuery = "";
                        foreach ($arrVal as $arr) {
                            if ($newQuery)
                                $newQuery .= ' OR';
                            $newQuery .= " FIND_IN_SET('" . $arr . "', e.company_id)";
                        }
                        $transferId = $data->transfer_parentid;
                        if ($transferId > 0) {
                            $transferData = Cashtransfer::model()->findByPk($transferId);
                            $dailyexpense = Dailyexpense::model()->findAll(array('condition' => 'transfer_parentid="' . $transferId . '"'));
                            Dailyexpense::model()->deleteAll(array('condition' => 'transfer_parentid = ' . $transferId));
                            Cashtransfer::model()->deleteAll(array('condition' => 'cashtransfer_id = ' . $transferId));
                        } else {
                            $newmodel = new JpLog('search');
                            $newmodel->log_data = json_encode($data->attributes);
                            $newmodel->log_action = 2;
                            $newmodel->log_table = $tblpx . "dailyexpense";
                            $newmodel->log_datetime = date('Y-m-d H:i:s');
                            $newmodel->log_action_by = Yii::app()->user->id;
                            $newmodel->company_id = $data->company_id;
                            if ($newmodel->save()) {
                                if ($data->expensehead_type == 4 || $data->expensehead_type == 5) {
                                    $model1 = Dailyexpense::model()->deleteAll(array("condition" => "transaction_parent=" . $data->dailyexp_id . ""));
                                    if($model1){
                                        $success_status = 1;
                                    }else{
                                        $success_status = 0;
                                        throw new Exception(json_encode("Some Error Occurred"));
                                    }
                                }
                                $model = Dailyexpense::model()->deleteAll(array("condition" => "dailyexp_id=" . $parentId . ""));
                                if($model){
                                    $success_status = 1;
                                }else{
                                    $success_status = 0;
                                    throw new Exception(json_encode($model->getErrors()));
                                }
                                $recmodel = Reconciliation::model()->deleteAll(array("condition" => "reconciliation_parentid=" . $parentId . ""));
                                if(!empty($recmodel)){
                                    if($recmodel){
                                    $success_status = 1;
                                    }else{
                                        $success_status = 0;
                                        throw new Exception(json_encode("Some Error Occured"));
                                    }
                                }
                                
                            }else{
                                $success_status = 0;
                                throw new Exception(json_encode($newmodel->getErrors()));
                            }
                        }
                        $notification->message = "Delete request in Daily expense is approved by " . $usermodel->first_name . " " . $usermodel->last_name;
                    } else if ($parentTable == "{$tblpx}dailyvendors") {
                        $data = Dailyvendors::model()->findByPk($parentId);
                        $tblpx = Yii::app()->db->tablePrefix;
                        if ($data) {
                            $newmodel = new JpLog('search');
                            $newmodel->log_data = json_encode($data->attributes);
                            $newmodel->log_action = 2;
                            $newmodel->log_table = $tblpx . "dailyvendors";
                            $newmodel->log_datetime = date('Y-m-d H:i:s');
                            $newmodel->log_action_by = Yii::app()->user->id;
                            $newmodel->company_id = $data->company_id;
                            if ($newmodel->save()) {
                                $model_count = Dailyvendors::model()->deleteAll(array("condition" => "daily_v_id=" . $parentId . ""));
                                if(!empty($model_count)){
                                    if($model_count>0){
                                    $success_status = 1;
                                    }else{
                                        $success_status = 0;
                                        throw new Exception("Failed to delete from Dailyvendors with ID: $parentId");
                                    }
                                }
                                
                                
                                $recmodel_count = Reconciliation::model()->deleteAll(array("condition" => "reconciliation_parentid=" . $parentId . ""));
                                if(!empty($recmodel_count)){
                                    if($recmodel_count >0 ){
                                    $success_status = 1;
                                    }else{
                                        $success_status = 0;
                                        throw new Exception("Failed to delete from Dailyvendors with ID: $parentId");
                                    }

                                }
                                
                            }else{
                                $success_status = 0;
                                throw new Exception("Failed to delete from Dailyvendors with ID: $parentId");
                            }
                        }
                        $notification->message = "Delete request in Vendor Payment is approved by " . $usermodel->first_name . " " . $usermodel->last_name;
                    } else if ($parentTable == "{$tblpx}subcontractor_payment") {
                        $data = SubcontractorPayment::model()->findByPk($parentId);
                        $uniqueid = $data->uniqueid;
                        $tblpx = Yii::app()->db->tablePrefix;
                        if ($uniqueid) {
                            $paymentdata = SubcontractorPayment::model()->findAll(array("condition" => "uniqueid='" . $uniqueid . "'"));
                            foreach ($paymentdata as $pdata) {
                                $newmodel = new JpLog('search');
                                $newmodel->log_data = json_encode($pdata->attributes);
                                $newmodel->log_action = 2;
                                $newmodel->log_table = $tblpx . "subcontractor_payment";
                                $newmodel->log_datetime = date('Y-m-d H:i:s');
                                $newmodel->log_action_by = Yii::app()->user->id;
                                $newmodel->company_id = $pdata->company_id;
                                if ($newmodel->save()) {
                                    $model = SubcontractorPayment::model()->deleteAll(array("condition" => "payment_id=" . $pdata->payment_id . ""));
                                    if(!empty($model)){
                                        if($model){
                                        $success_status = 1;
                                        }else{
                                            $success_status = 0;
                                            throw new Exception(json_encode("Some Error Occurred"));
                                        }

                                    }
                                    
                                    $model1 = Expenses::model()->deleteAll(array("condition" => "subcontractor_id=" . $pdata->payment_id . ""));
                                    if(!empty($model1)){
                                        if($model1){
                                        $success_status = 1;
                                        }else{
                                            $success_status = 0;
                                            throw new Exception(json_encode("Some Error Occurred"));
                                        }

                                    }
                                    
                                }else{
                                    $success_status = 0;
                                    throw new Exception(json_encode($newmodel->getErrors()));
                                }
                            }
                            $recmodel = Reconciliation::model()->deleteAll(array("condition" => "reconciliation_parentids = '" . $uniqueid . "'"));
                            if(!empty($recmodel)){
                                if($recmodel >0){
                                $success_status = 1;
                            }else{
                                $success_status = 0;
                                throw new Exception(json_encode("Some error occured"));
                            }

                            }
                            
                        } else {
                            if ($data) {
                                $newmodel = new JpLog('search');
                                $newmodel->log_data = json_encode($data->attributes);
                                $newmodel->log_action = 2;
                                $newmodel->log_table = $tblpx . "subcontractor_payment";
                                $newmodel->log_datetime = date('Y-m-d H:i:s');
                                $newmodel->log_action_by = Yii::app()->user->id;
                                $newmodel->company_id = $data->company_id;
                                if ($newmodel->save()) {
                                    $expLoad = Expenses::model()->find(array(
                                        'select' => array('exp_id'),
                                        "condition" => "subcontractor_id='" . $parentId . "'",
                                    ));
                                    $expOldId = $expLoad["exp_id"];
                                    if ($expLoad) {
                                        $recmodel = Reconciliation::model()->deleteAll(array("condition" => "reconciliation_parentid=" . $expOldId . ""));
                                        if(!empty($recmodel)){
                                             if($recmodel){
                                            $success_status = 1;
                                            }else{
                                                $success_status = 0;
                                                throw new Exception("Error");
                                            }
                                        }
                                       
                                    }
                                    $scmodel = SubcontractorPayment::model()->deleteAll(array("condition" => "payment_id=" . $parentId . ""));
                                    if(!empty($scmodel)){
                                        if($scmodel){
                                        $success_status = 1;
                                        }else{
                                            $success_status = 0;
                                            throw new Exception(json_encode("Some error occurred"));
                                        }

                                    }
                                    
                                    $exmodel = Expenses::model()->deleteAll(array("condition" => "subcontractor_id=" . $parentId . ""));
                                    if(!empty($exmodel)){
                                        if($exmodel){
                                        $success_status = 1;
                                        }else{
                                            $success_status = 0;
                                            throw new Exception(json_encode("Some Error occurred"));
                                        }

                                    }
                                    
                                }else{
                                    $success_status = 0;
                                    throw new Exception(json_encode($newmodel->getErrors()));
                                }
                            }
                        }
                        $notification->message = "Delete request in Subcontractor Payment is approved by " . $usermodel->first_name . " " . $usermodel->last_name;
                    } else if ($parentTable == "{$tblpx}dailyreport") {
                        $data = Dailyreport::model()->findByPk($parentId);
                        $tblpx = Yii::app()->db->tablePrefix;
                        if ($data) {
                            $newmodel = new JpLog('search');
                            $newmodel->log_data = json_encode($data->attributes);
                            $newmodel->log_action = 2;
                            $newmodel->log_table = $tblpx . "dailyreport";
                            $newmodel->log_datetime = date('Y-m-d H:i:s');
                            $newmodel->log_action_by = Yii::app()->user->id;
                            $newmodel->company_id = $data->company_id;
                            if ($newmodel->save()) {
                                $model = Dailyreport::model()->deleteAll(array("condition" => "dr_id=" . $parentId . ""));
                                if($model >0){
                                    $success_status = 1;
                                }else{
                                    $success_status = 0;
                                    throw new Exception(json_encode("Error"));
                                }
                            }else{
                                $success_status = 0;
                                throw new Exception(json_encode($newmodel->getErrors()));
                            }
                        }
                        $notification->message = "Delete request in Daily Report is approved by " . $usermodel->first_name . " " . $usermodel->last_name;
                    }else if ($parentTable == "{$tblpx}invoice") {
                        $data = Invoice::model()->findByPk($parentId);                
                        $tblpx = Yii::app()->db->tablePrefix;                
                        if ($data) {
                            $newmodel = new JpLog('search');
                            $newmodel->log_data = json_encode($data->attributes);
                            $newmodel->log_action = 2;
                            $newmodel->log_table = $tblpx . "invoice";
                            $newmodel->log_datetime = date('Y-m-d H:i:s');
                            $newmodel->log_action_by = Yii::app()->user->id;
                            $newmodel->company_id = $data->company_id;
                            if ($newmodel->save()) {
                                $invoice = Invoice::model()->deleteAll(array("condition" => "invoice_id=" . $data->invoice_id . "")); 
                                if(!empty($invoice)){
                                     if($invoice >0){
                                    $success_status = 1;
                                    }else{
                                        $success_status = 0;
                                        throw new Exception(json_encode("Error"));
                                    } 

                                }
                                                         
                            }else{
                                $success_status = 0;
                                throw new Exception(json_encode($newmodel->getErrors()));
                            }
                        }
                        $notification->message = "Delete request in Invoice is approved by " . $usermodel->first_name . " " . $usermodel->last_name;                                      
                    } else if ($parentTable == "{$tblpx}quotation") {
                        $data = Quotation::model()->findByPk($parentId);                
                        $tblpx = Yii::app()->db->tablePrefix;
                        $paymentStages = PaymentStage::model()->findAllByAttributes(['quotation_id' => $parentId]);
                  
                            $invoiceExists='';
                            $payment_stage_id= array();
                            $invoiceExists = false; // Initialize invoice existence flag

                        if (!empty($paymentStages)) {
                            foreach ($paymentStages as $stage) {
                                // Check if any invoice exists for the current stage
                                $exist = Invoice::model()->findByAttributes(['stage_id' => $stage->id]);
                                if (!empty($exist)) {
                                    $invoiceExists = true; // Invoice exists for at least one stage
                                    break; // No need to check further
                                }
                            }
                        }
                        
                        // echo "<pre>";print_r($invoiceExists);exit;
                        if ($invoiceExists) {
                            // Update status to 3
                            $success_status=0;
                            throw new Exception( json_encode(array('response' => 'warning', 'msg' => 'Cannot Delete ! This record alredy in use.')));
                           
                        }else{

                                      
                            $newmodel = new JpLog('search');
                            $newmodel->log_data = json_encode($data->attributes);
                            $newmodel->log_action = 2;
                            $newmodel->log_table = $tblpx . "quotation";
                            $newmodel->log_datetime = date('Y-m-d H:i:s');
                            $newmodel->log_action_by = Yii::app()->user->id;
                            $newmodel->company_id = $data->company_id;
                            if ($newmodel->save()) {
                                $qmodel = Quotation::model()->deleteAll(array("condition" => "quotation_id=" . $data->quotation_id . ""));
                                if($qmodel){
                                    $success_status = 1;
                                }else{
                                    $success_status = 0;
                                    throw new Exception(json_encode("Some error occurred"));
                                }                            
                            } else{
                                $success_status = 0;
                                throw new Exception(json_encode($newmodel->getErrors()));
                            }                                       
                        }
                    } else if ($parentTable == "{$tblpx}bills") {
                        $data = Bills::model()->findByPk($parentId);                
                        $tblpx = Yii::app()->db->tablePrefix;
                        if ($data) {                                        
                            $newmodel = new JpLog('search');
                            $newmodel->log_data = json_encode($data->attributes);
                            $newmodel->log_action = 2;
                            $newmodel->log_table = $tblpx . "bills";
                            $newmodel->log_datetime = date('Y-m-d H:i:s');
                            $newmodel->log_action_by = Yii::app()->user->id;
                            $newmodel->company_id = $data->company_id;
                            if ($newmodel->save()) {
                                $bmodel = Bills::model()->deleteAll(array("condition" => "bill_id=" . $data->bill_id . "")); 
                                if(!empty($bmodel)){
                                    if($bmodel){
                                    $success_status = 1;
                                    }else{
                                        $success_status = 0;
                                        throw new Exception(json_encode("Some error occurred"));
                                    } 

                                }
                                                          
                            } else{
                                $success_status = 0;
                                throw new Exception(json_encode($newmodel->getErrors()));
                            }                                       
                        }
                    }else if ($parentTable == "{$tblpx}sc_payment_stage") {
                        $data = ScPaymentStage::model()->findByPk($parentId);
                        $tblpx = Yii::app()->db->tablePrefix;
                        if ($data) {
                            $newmodel = new JpLog('search');
                            $newmodel->log_data = json_encode($data->attributes);
                            $newmodel->log_action = 2;
                            $newmodel->log_table = $tblpx . "sc_payment_stage";
                            $newmodel->log_datetime = date('Y-m-d H:i:s');
                            $newmodel->log_action_by = Yii::app()->user->id;
                            $newmodel->company_id = '';
                            if ($newmodel->save()) {
                                $model = ScPaymentStage::model()->deleteAll(array("condition" => "id=" . $parentId . "")); 
                                if(!empty($model)){
                                    if($model){
                                    $success_status = 1;
                                    }else{
                                        $success_status = 0;
                                        throw new Exception(json_encode("Some Error Occurred"));
                                    }   
                                }  
                                                   
                            }else{
                                $success_status = 0;
                                throw new Exception(json_encode($newmodel->getErrors()));
                            }
                        }
                        $notification->message = "Delete request in Subcontractor Payment stage is approved by " . $usermodel->first_name . " " . $usermodel->last_name;
                    }else if ($parentTable == "{$tblpx}subcontractor") {
                        $data = Subcontractor::model()->findByPk($parentId);
                        $tblpx = Yii::app()->db->tablePrefix;
                        if ($data) {
                            $newmodel = new JpLog('search');
                            $newmodel->log_data = json_encode($data->attributes);
                            $newmodel->log_action = 2;
                            $newmodel->log_table = $tblpx . "subcontractor";
                            $newmodel->log_datetime = date('Y-m-d H:i:s');
                            $newmodel->log_action_by = Yii::app()->user->id;
                            $newmodel->company_id = '';
                            if ($newmodel->save()) {
                                $model = Subcontractor::model()->deleteAll(array("condition" => "subcontractor_id=" . $parentId . ""));   
                                if(!empty($model)){
                                    $success_status = 1;
                                }else{
                                    $success_status = 0;
                                    throw new Exception(json_encode("Some error occured"));
                                }                      
                            }else{
                                $success_status = 0;
                                throw new Exception(json_encode($newmodel->getErrors()));
                            }
                        
                        }
                        $notification->message = "Delete request in Subcontractor is approved by " . $usermodel->first_name . " " . $usermodel->last_name;
                    }
                    $request->deletepending_status = 1;
                    if($request->save()){
                        $success_status = 1;
                    }else{
                        $success_status = 0;
                        throw new Exception(json_encode($request->getErrors()));
                    }
                
                    if($notification->save()){
                        $success_status = 1;
                    }else{
                        $success_status = 0;
                        throw new Exception(json_encode($notification->getErrors()));
                    }

                $transaction->commit();
                }catch(Exception $error){
                    $transaction->rollBack();
                    $success_status = 0;
                }finally{
                    
                    if ($success_status == 1) {
                        echo json_encode(array('response' => 'success', 'msg' => 'Request successfully granted'));                                       
                    } else {
                        if (isset($error->errorInfo) && ($error->errorInfo[1] == 1451)) {                         
                            echo json_encode(array('response' => 'warning', 'msg' => 'Cannot Delete ! This record alredy in use.'));
                        } else {
                            echo json_encode(array('response' => 'error', 'msg' => 'Some Error Occured'));
                        }
                    }
                }
            } else if ($status == 2) {
                $request->deletepending_status = 2;
                $parentId = $request->deletepending_parentid;

                $transaction = Yii::app()->db->beginTransaction();         
                try{
                    if ($parentTable == "{$tblpx}dailyexpense") {
                        $data = Dailyexpense::model()->findByPk($parentId);
                        $notification->message = "Delete request in Daily Expenses is cancelled by " . $usermodel->first_name . " " . $usermodel->last_name;
                    } else if ($parentTable == "{$tblpx}subcontractor_payment") {
                        $data = SubcontractorPayment::model()->findByPk($parentId);
                        $notification->message = "Delete request in Subcontractor Payment is cancelled by " . $usermodel->first_name . " " . $usermodel->last_name;
                    } else if ($parentTable == "{$tblpx}expenses") {
                        $data = Expenses::model()->findByPk($parentId);
                        $notification->message = "Delete request in Daybook Payment is cancelled by " . $usermodel->first_name . " " . $usermodel->last_name;
                    } else if ($parentTable == "{$tblpx}dailyvendors") {
                        $data = Dailyvendors::model()->findByPk($parentId);
                        $notification->message = "Delete request in Vendor Payment is cancelled by " . $usermodel->first_name . " " . $usermodel->last_name;
                    } else if ($parentTable == "{$tblpx}dailyreport") {
                        $data= array();
                        $notification->message = "Delete request in Daily Report is cancelled by " . $usermodel->first_name . " " . $usermodel->last_name;
                    } else if ($parentTable == "{$tblpx}invoice") {
                        $data= array();
                        $invoiceData = Invoice::model()->findByPk($parentId);
                        $invoiceData->delete_approve_status = '0';
                        $invoiceData->save();
                        $notification->message = "Delete request in Invoice is cancelled by " . $usermodel->first_name . " " . $usermodel->last_name;
                    } else if ($parentTable == "{$tblpx}quotation") {
                        $data= array();
                        $quotationData = Invoice::model()->findByPk($parentId);
                        $quotationData->delete_approve_status = '0';
                        $quotationData->save();
                        $notification->message = "Delete request in Quotation is cancelled by " . $usermodel->first_name . " " . $usermodel->last_name;
                    } else if ($parentTable == "{$tblpx}bills") {
                        $data= array();
                        $notification->message = "Delete request in Bills is cancelled by " . $usermodel->first_name . " " . $usermodel->last_name;
                    }
                    if(!empty($data)){
                        $data->duplicate_delete_status = '0';                    
                        if($data->save()){
                            $success_status = 1;
                        }else{
                            $success_status = 0;
                            throw new Exception(json_encode($data->getErrors()));
                        }  
                    }

                    
                    if($request->save()){
                        $success_status = 1;
                    }else{
                        $success_status = 0;
                        throw new Exception(json_encode($request->getErrors()));
                    }
                    
                    if($notification->save()){
                        $success_status = 1;
                    }else{
                        $success_status = 0;
                        throw new Exception(json_encode($notification->getErrors()));
                    }
                    $transaction->commit();
                }catch(Exception $error){
                    $transaction->rollBack();
                    $success_status = 0;
                }finally{
                    
                    if ($success_status == 1) {
                        echo json_encode(array('response' => 'success', 'msg' => 'Request successfully cancelled'));                                       
                    }else{
                        echo json_encode(array('response' => 'error', 'msg' => 'Some Error Occured')); 
                    }
                }
            }  else{
                echo 1;
            }          
        }
        
    }

    public function actionNotificationview()
    {
        $notification_id = $_POST['id'];
        $model = Notifications::model()->findByPk($notification_id);
        if (!empty($model) && $model->requested_by == Yii::app()->user->id) {
            $model->view_status = 1;
            if ($model->update()) {
                echo 'success';
            } else {
                echo 'error';
            }
        } else {
            echo 'error';
        }
    }
    public function actionAllnotificationview()
{
    $rawPostData = file_get_contents("php://input");
    $data = json_decode($rawPostData, true);
    //echo "<pre>";print_r($data);exit;
    if (isset($data['selectedNotifications']) && !empty($data['selectedNotifications'])) {
        $notification_types = $data['selectedNotifications'];
        $allSuccess = true;

        foreach ($notification_types as $notification_type) {
            if (isset($notification_type['id']) && isset($notification_type['type'])) {
                $id = $notification_type['id'];
                $type = $notification_type['type'];

                switch ($type) {
                    case 'notification':
                        $model = Notifications::model()->findByPk($id);
                        if ($model) {
                            $model->view_status = 1;
                            if (!$model->update()) {
                                $allSuccess = false;
                            }
                        }
                        break;

                    case 'expense_notification':
                        $model = ExpenseNotification::model()->findByPk($id);
                        if ($model) {
                            $model->view_status = 3;
                            if (!$model->update()) {
                                $allSuccess = false;
                            }
                        }
                        break;

                    case 'scquotation':
                        $model = Scquotation::model()->findByPk($id);
                        if ($model) {
                            $model->viewed_status = 1;
                            
                            if (!$model->update()) {
                                $allSuccess = false;
                            }
                        }
                        break;
                        case 'scquotationstab':
                            $model = Scquotation::model()->findByPk($id);
                            if ($model) {
                               
                                $model->added_viewed_notification=1;
                                if (!$model->update()) {
                                    $allSuccess = false;
                                }
                            }
                            break;
    

                    default:
                        $allSuccess = false;
                        break;
                }
            } else {
                $allSuccess = false;
            }
        }

        if ($allSuccess) {
            echo json_encode(['status' => 'success']);
        } else {
            echo json_encode(['status' => 'error']);
        }
    } else {
        echo json_encode(['status' => 'error', 'message' => 'No IDs provided']);
    }
}

    

    public function actionListNotifications()
    {

        $model = new Notifications;
        $model->unsetAttributes();  // clear any default values
        $this->render('_notification', array(
            'model' => $model,
            'dataProvider' => $model->allnotifications(),
        ));
    }

    public function actionCompanyeditlog()
    {
        $model = new CompanyEditLog;
        $model->unsetAttributes();  // clear any default values
        $this->render('_company_edit', array(
            'model' => $model,
            'dataProvider' => $model->search(),
        ));
    }

    public function actionPocompanyeditlog()
    {

        $tblpx = Yii::app()->db->tablePrefix;
        $html = '';
        $html['html'] = '';
        $html['status'] = '';
        $model = new CompanyEditLog;

        $data = CompanyEditLog::model()->findAll(['condition' => 'p_id="' . $_GET['id'] . '"', 'order' => 'log_date DESC', 'limit' => 2]);

        if ($data) {


            if (!empty($data)) {
                $html['html'] .= '<div class="alert alert-success get_prevdata alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <div id="parent2">
                        <table style="width:100%;" id="pre_fixtable2">
                            <thead>
                            <tr>
                                <th style="text-align:center">Sl.No</th>
                                <th style="text-align:center">Project</th>
                                <th style="text-align:center">Purchase No</th>
                                <th style="text-align:center">Prev company</th>
                                <th style="text-align:center">Current commpany</th>
                                <th style="text-align:center">Changed by</th>
                                <th style="text-align:center">date</th>
                            
                            </tr>
                            <thead>
                        <tbody>
                        ';

                $k = 1;

                foreach ($data as $dat) {


                    $log_data = json_decode($dat->log_data);
                    $html['html'] .= '<tr>
                                        <td style="text-align:center;">' . $k . '</td>
                                        <td style="text-align:center;">' . $log_data->project_name . '</td>
                                        <td style="text-align:center;">' . $log_data->purchase_no . '</td>
                                        <td style="text-align:center;">' . $log_data->prev_company . '</td>
                                        <td style="text-align:center">' . $log_data->current_company . '</td>
                                        <td style="text-align:center;">' . $model->changedbyuser($dat->changed_by) . '</td>
                                        <td style="text-align:center">' . date('d-m-Y', strtotime($dat->log_date)) . '</td>
                  
                                        </tr>';
                    $k++;
                }


                $html['html'] .= '</tbody></table></div></div>';
                $html['status'] .= true;
            } else {
                $html['html'] .= '';
                $html['status'] .= false;
            }
        } else {

            $html['html'] .= '';
            $html['status'] .= false;
        }
        echo json_encode($html);
    }

    public function actionCompanyapprove()
    {
        $purchase_id = $_POST['purchase_id'];
        $tblpx = Yii::app()->db->tablePrefix;
        $request = Editrequest::model()->find(['condition' => 'editrequest_table ="' . $tblpx . 'purchase" AND  editrequest_status =0 AND parent_id = ' . $purchase_id]);
        $data = json_decode($request->editrequest_data);
        $purchase_id = $_POST['purchase_id'];
        $purchase_table_data = Purchase::model()->findByPk($purchase_id);
        $usermodel = Users::model()->findByPk(Yii::app()->user->id);
        $notification = new Notifications;
        $notification->action = 'Company Edit Approved';
        $notification->message = "Company edit Approved on PO/" . $purchase_table_data->purchase_no . " in project " . $data->project_name . " to " . $data->company;
        $notification->parent_id = $request->editrequest_id;
        $notification->date = date("Y-m-d");
        $notification->requested_by = $request->user_id;
        $notification->approved_by = Yii::app()->user->id;
        $request->editrequest_status = 1;
        if ($notification->save() && $request->update()) {
            $bill_model = Bills::model()->find(['condition' => 'purchase_id ="' . $purchase_table_data->p_id . '"']);
            if (!empty($bill_model)) {
                $bill_model->company_id = $data->company_id;
                $bill_model->update();
            }
            echo 'success';
        } else {
            echo 'error';
        }
    }

    public function actionchangeExpenseView()
    {
        $id = $_POST['id'];
        if (!empty($id)) {
            $notification_model = ExpenseNotification::model()->findByPk($id);
            $notification_model->view_status = 2;
            if ($notification_model->save()) {
                echo json_encode(array('response' => 'success', 'msg' => 'Viewd successfully'));
            } else {
                echo json_encode(array('response' => 'warning', 'msg' => 'Error Occured'));
            }
        }
    }
    public function actionAllchangeexpenseview(){
        if (isset($_REQUEST['ids']) && is_array($_REQUEST['ids'])) {
            $notification_ids = $_REQUEST['ids'];
            
            $allSuccess = true;
            foreach ($notification_ids as $notification_id) {
                $notification_model = ExpenseNotification::model()->findByPk($notification_id);
                $notification_model->view_status = 2;
                    if (!$notification_model->save()) {
                        $allSuccess = false;
                    }
              
            }
    
            if ($allSuccess) {
                echo json_encode(array('status' => 'success', 'msg' => 'Viewed successfully'));
          
            } else {
                echo json_encode(array('status' => 'warning', 'msg' => 'Error Occured'));     
            }
      
        }else {
            echo json_encode(array('status' => 'warning', 'msg' => 'Error Occured'));     
        }
               

    }

    public function actiongeneralSettings()
    {
        $model = GeneralSettings::model()->findByPk(1);
        $new_model=CroneDeleteLogSettings::model()->findByPk(1);
        $api_settings=ApiSettings::model()->findByPk(1);
        if (isset($_POST['GeneralSettings'])) {
            $model->attributes = $_POST['GeneralSettings'];
            $model->updated_by = Yii::app()->user->id;
            $model->updated_date = date('Y-m-d H:i:s');
            if ($model->save()) {
                Yii::app()->user->setFlash('success', "updated successfully!");
            }
        }
        if (isset($_POST['CroneDeleteLogSettings'])) {
            $new_model->attributes = $_POST['CroneDeleteLogSettings'];
            $new_model->updated_by = Yii::app()->user->id;
            $new_model->updated_at = date('Y-m-d H:i:s');
            if ($new_model->save()) {
                Yii::app()->user->setFlash('success', "updated successfully!");
            }
        }

        $flashMessage = null; // Variable to hold flash messages

    // Check if POST data is set for 'ApiSettings'
    if (isset($_POST['ApiSettings'])) {
        $apiSettings = $_POST['ApiSettings'];

        // Check for API integration settings
        if (isset($apiSettings['api_integration_settings']) && $apiSettings['api_integration_settings'] == 1) {
            
            // Check if GLOBAL_API_DOMAIN is defined and not empty
            if (!defined('GLOBAL_API_DOMAIN') || empty(GLOBAL_API_DOMAIN)) {
                $flashMessage = "GLOBAL_API_DOMAIN not defined in env!";
                // Do not return here; set message and continue to render
            } else {
                // Call the API
                $slug = 'api/check-module-availablity/pms';
                $globalapi_response = GlobalApiCall::callApi($slug, 'GET', array());

                // Check API response
                if (!$globalapi_response['success']) {
                    $flashMessage = "No access to pms module";
                    // Do not return here; set message and continue to render
                }
            }
        }

        // Update api_settings attributes if no errors
        if (!$flashMessage) { // Only proceed if no flash message is set
            $api_settings->attributes = $apiSettings;
            $api_settings->updated_at = date('Y-m-d H:i:s');
            
            if ($api_settings->save()) {
                $flashMessage = "Updated successfully!";
            } else {
                $flashMessage = "Failed to update settings.";
            }
        }

        // Set the flash message
        if ($flashMessage) {
            Yii::app()->user->setFlash('success', $flashMessage);
        }
    }
        $this->render('general_settings', array('model' => $model,'new_model'=>$new_model,'api_settings'=>$api_settings));
    }



    public function updateModelData($payment_type, $premodel, $model, $type, $paymetName) {

        $dailyExpChildModel = array();
        $expenseModel= array();
        
        if ($type == 4 ) { // dailyexp child
            if($model->exp_type_id ==25 || $model->exp_type_id ==26){
                $dailyexpchildOldModel = Dailyexpense::model()->findByAttributes(
                    array('transaction_parent' => $model->dailyexp_id)
                );
                $dailyExpChildModel = $this->updateDailyExpChildData($dailyexpchildOldModel, $premodel);
            }                       
        }

        if($type==3){            
            $expenseOldModel = Expenses::model()->findByAttributes(
                array("subcontractor_id" => $model->payment_id)
            );                
            $expenseModel =  $this->updateExpData($expenseOldModel, $premodel);            
        }    
       
        $model->attributes = $premodel->attributes;
        $model->approval_status = 1;
        $reconmodel = array();

        if ($payment_type == 88) {
            $reconmodel = Reconciliation::model()->findByAttributes(array(
                'reconciliation_parentid' => $premodel->ref_id,
                'reconciliation_payment' => $paymetName
            ));
           
            if ($type == 1) {
                if(!empty($reconmodel)){
                $reconmodel->reconciliation_bank = $model->bank_id;
                $reconmodel->reconciliation_payment = "Payment without Bill";
                $reconmodel->reconciliation_chequeno = $model->cheque_no;
                }
            } else if($type==2){
                 if(!empty($reconmodel)){
                $reconmodel->reconciliation_bank = $model->bank;
                $reconmodel->reconciliation_payment = "Vendor Payment";
                $reconmodel->reconciliation_chequeno = $model->cheque_no;
                 }
            }else if($type==3){
                 if(!empty($reconmodel)){
                $reconmodel->reconciliation_bank = $model->bank;
                $reconmodel->reconciliation_payment = "Subcontractor Payment";
                $reconmodel->reconciliation_chequeno = $model->cheque_no;
                 }
            }else{
                 if(!empty($reconmodel)){
                $reconmodel->reconciliation_bank = $model->bank_id;
                $reconmodel->reconciliation_payment = "Dailyexpense Payment";
                $reconmodel->reconciliation_chequeno = $model->dailyexpense_chequeno;
                 }
            }
            
            // reconciliation_table 
           if(!empty($reconmodel)){
                $reconmodel->reconciliation_status = 0;
            $reconmodel->company_id = $model->company_id;
            $reconmodel->reconciliation_amount = $model->amount;
            $reconmodel->reconciliation_date = NULL;
            $reconmodel->reconciliation_paymentdate = $model->date;
            $reconmodel->created_date = $model->created_date;
       
           }        
        }

        return array(
            'model' => $model, 
            'reconmodel' => $reconmodel, 
            'dailyExpChildModel' => $dailyExpChildModel,
            'expenseModel'=>$expenseModel
        );
        
    }

    public function updateDailyExpChildData($dailyExpModel, $premodel) {

        $purchaseType = $dailyExpModel->dailyexpense_purchase_type;
        $expensetype = Companyexpensetype::model()->findByAttributes(array('name' => 'Cash Deposit'));
        $dailyExpModel->attributes = $premodel->attributes;
        $dailyExpModel->expense_type = $premodel->dailyexpense_receipt_type;
        $dailyExpModel->dailyexpense_paidamount = $premodel->amount;
        $dailyExpModel->dailyexpense_purchase_type = $purchaseType;
        $dailyExpModel->approval_status = 1;
        $dailyExpModel->date = date('Y-m-d', strtotime($premodel->date));
        $dailyExpModel->dailyexpense_type = 1;
        $dailyExpModel->expensehead_id = $expensetype['company_exp_id'];
        $dailyExpModel->dailyexpense_receipt_type = NULL;
        $dailyExpModel->dailyexpense_receipt_head = NULL;
        $dailyExpModel->dailyexpense_receipt = NULL;
        $dailyExpModel->exp_type = 73;

        return $dailyExpModel;
    }

    public function getPaymentId($model,$type) {
        
        if ($type == 1) {
            $id = $model->exp_id;
        } else if ($type == 2) {
            $id = $model->daily_v_id;
        } else if ($type == 3) {
            $id = $model->payment_id;
        }  else if ($type ==4 ) {
            $id = $model->dailyexp_id;
        }else{
            $id = $model->dr_id;
        }
        
        return $id;
    }

    public function updateExpData($expenseModel, $premodel){        
        
        
        $expenseModel->attributes = $premodel->attributes;        
        $expenseModel->projectid = $premodel->project_id;
        $expenseModel->expense_type = $premodel->payment_type;
        $expenseModel->expense_amount = $premodel->amount;
        $expenseModel->expense_sgstp = $premodel->sgst;
        $expenseModel->expense_sgst = $premodel->sgst_amount;
        $expenseModel->expense_cgstp = $premodel->cgst;
        $expenseModel->expense_cgst = $premodel->cgst_amount;
        $expenseModel->expense_igstp = $premodel->igst;
        $expenseModel->expense_igst = $premodel->igst_amount;       
        $expenseModel->type = 73;
        $expenseModel->expense_tdsp = $premodel->tds;
        $expenseModel->expense_tds = $premodel->tds_amount;
        $expenseModel->payment_quotation_status = $premodel->payment_quotation_status;
        $expenseModel->paid = $premodel->amount;        
        $expenseModel->userid = Yii::app()->user->id;
        $expenseModel->approval_status = '1';
        $expenseModel->bank_id = $premodel->bank;
        $expenseModel->cheque_no = $premodel->cheque_no;
        $expenseModel->subcontractor_id = $premodel->payment_id;  

        return $expenseModel;
        
    }

    public function actionQuotationTemplate() {
        $model = QuotationTemplate::model()->findAll();
        $projectmodel = ProjectTemplate::model()->findAll();
        $this->render('quotation_template', array('model' => $model,'projectmodel'=>$projectmodel));
    }

    public function actionchangeTemplateStatus($id) {

        $templateModel = QuotationTemplate::model()->findByPk($id);
        $templateModel->status = '1';

        if ($templateModel->save()) {
            $sql = "UPDATE jp_quotation_template SET status = '0' 
                    WHERE `template_id` != " . $id;
            Yii::app()->db->createCommand($sql)->execute();            
        }
    }

    public function actionchangeProjectTemplate($id) {

        $templateModel = ProjectTemplate::model()->findByPk($id);
        $templateModel->status = '1';

        if ($templateModel->save()) {
            $sql = "UPDATE jp_project_template SET status = '0' 
                    WHERE `template_id` != " . $id;
            Yii::app()->db->createCommand($sql)->execute();            
        }
    }


    public function actionuploadFiles()
    {   
        $top_images_path = realpath(Yii::app()->basePath . '/../uploads/quotation_generator/top-images/');        
        
        $bottom_images_path = realpath(Yii::app()->basePath . '/../uploads/quotation_generator/bottom-images');                
                      
        $countfiles = count($_FILES['top_image']['name']);
        $upload_location = $top_images_path;        
        $files_arr = array();

        for($index = 0;$index <= $countfiles;$index++){

            if(isset($_FILES['top_image']['name'][$index]) && $_FILES['top_image']['name'][$index] != ''){           
                $filename = $_FILES['top_image']['name'][$index];                           
                $ext = strtolower(pathinfo($filename, PATHINFO_EXTENSION));                             
                $valid_ext = array("pdf");
                
                if(in_array($ext, $valid_ext)){
                    $path = $upload_location."/".$filename;                                    
                    if(move_uploaded_file($_FILES['top_image']['tmp_name'][$index],$path)){
                        $filemodel=new QuotationGeneratorImage;   
                        $filemodel->top_bottom_image = $filename; 
                        $filemodel->type = '0'; 
                        $filemodel->created_by = Yii::app()->user->id;
                        $filemodel->created_at = date('d-m-y H:i:s');
                        if(!$filemodel->save()){
                            // echo "<pre>";
                            // print_r($filemodel->getErrors());exit;
                        }                     
                        $files_arr[] = $path;
                    }                    
                }
            
            }
        }

          
        $countfiles1 = count($_FILES['bottom_image']['name']);
        $upload_location1 = $bottom_images_path;        
        $files_arr1 = array();

        for($index = 0;$index <= $countfiles1;$index++){

            if(isset($_FILES['bottom_image']['name'][$index]) && $_FILES['bottom_image']['name'][$index] != ''){           
                $filename = $_FILES['bottom_image']['name'][$index];                           
                $ext = strtolower(pathinfo($filename, PATHINFO_EXTENSION));                             
                $valid_ext = array("pdf");
                
                if(in_array($ext, $valid_ext)){
                    $path = $upload_location1."/".$filename;                      
                                      
                    if(move_uploaded_file($_FILES['bottom_image']['tmp_name'][$index],$path)){
                        $filemodel1=new QuotationGeneratorImage;
                        $filemodel1->top_bottom_image = $filename; 
                        $filemodel1->type = '1';  
                        $filemodel1->created_by = Yii::app()->user->id; 
                        $filemodel1->created_at = date('d-m-y H:i:s');
                        if(!$filemodel1->save()){
                            // echo "<pre>";
                            // print_r($filemodel1->getErrors());exit;
                        }                     
                        $files_arr1[] = $path;
                    }                    
                }
            
            }
        }

        $gmodel = GeneralSettings::model()->findByPk(1);
        $this->redirect(array('generalsettings'));
       
    } 
    
    public function actionremoveFile(){
        $id = $_POST['id'];
        $model = QuotationGeneratorImage::model()->findByPk($id);
        $type = $model->type;
        $filename = $model->top_bottom_image;

        $top_images_path = realpath(Yii::app()->basePath . '/../uploads/quotation_generator/top-images/');
		$bottom_images_path = realpath(Yii::app()->basePath . '/../uploads/quotation_generator/bottom-images/');
		
		$filePath1 = $top_images_path. $filename;
		$filePath2 = $bottom_images_path. $filename;

		if (($type=='0')&& file_exists($filePath1)) 
		{
			unlink($filePath1);
		}
		if (($type=='1')&&file_exists($filePath2)) 
		{
			unlink($filePath2);
		}
        if($model->delete()){

        }
        // echo "<pre>";
        // print_r($model);exit;
    }
    public function actionmediasettings(){
        $this->render('mediasettings');
    }

	public function actionMediaUploadFiles(){
		//echo "<pre>";print_r($_POST);exit;
        if( isset( $_FILES['media_image']) ){
           
            $countfiles = count($_FILES['media_image']['name']);
            $files_arr = array();
            $basePathWithoutProtected = dirname(Yii::app()->basePath);
            for($index = 0;$index <= $countfiles;$index++){
                $company_asset_path = $basePathWithoutProtected . '/uploads/company_assets';
                if( !is_dir($company_asset_path) ){
                    mkdir($company_asset_path, 0777, true);
                    chmod($company_asset_path, 0777);
                }
                if(isset($_FILES['media_image']['name'][$index]) && $_FILES['media_image']['name'][$index] != ''){  
                    $basePathWithoutProtected = dirname(Yii::app()->basePath);
                    $media_image_path = $basePathWithoutProtected . '/uploads/company_assets/media';
                    $upload_location = $media_image_path;
                  
                    if( !is_dir($upload_location)){
                        mkdir($upload_location, 0777, true);
                        chmod($upload_location, 0777);
                    }      
                    $filename = $_FILES['media_image']['name'][$index];                           
                    $ext = strtolower(pathinfo($filename, PATHINFO_EXTENSION));                             
                    $valid_ext = array("jpg","png");
                    
                    if(in_array($ext, $valid_ext)){
                        $path = $upload_location."/".$filename;  
                        if(file_exists( $path )){
                            $msg = "File name Already exist";
                            Yii::app()->user->setFlash('success', $msg);
                        }else{
                            if(move_uploaded_file($_FILES['media_image']['tmp_name'][$index],$path)){
                                $msg="uploaded successfully";
                                 $files_arr[] = $path;
                                 Yii::app()->user->setFlash('success', $msg);
                             }else{
                                $msg = "Some error occured";
                                 Yii::app()->user->setFlash('success', $msg);
                             }     
                        }                                  
                                       
                    }
                
                }
            }
    
        }
       
        $this->redirect(array('mediasettings'));
	}
    public function actionIPWhiteList()
    {
        $slug = 'api/ip-whitelisting/coms';
        $globalapi_response = GlobalApiCall::callApi($slug, 'GET', array());
        $mode = isset($globalapi_response['success']) && $globalapi_response['success'] ? 'success' : 'error';

        $message = isset($globalapi_response['message']) ? $globalapi_response['message'] : 'No message provided';
        Yii::app()->user->setFlash('success', $message);
        $this->redirect(array('generalsettings'));
    }
    public function actionCashbalance(){
        {
        $tblpx = Yii::app()->db->tablePrefix;
        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        $newQuery = "";
        $newQuery1 = "";
        foreach ($arrVal as $arr) {
            if ($newQuery)
                $newQuery .= ' OR';
            if ($newQuery1)
                $newQuery1 .= ' OR';
            $newQuery .= " FIND_IN_SET('" . $arr . "', " . $tblpx . "projects.company_id)";
            $newQuery1 .= " FIND_IN_SET('" . $arr . "', " . $tblpx . "expenses.company_id)";
        }

        if (Yii::app()->user->role == 1) {

            $sql = Yii::app()->db->createCommand('select exp_id,name,type,exptype,payment_type,caption,date,amount,first_name,expense_type,' . $tblpx . 'expenses.description from ' . $tblpx . 'expenses
			inner join ' . $tblpx . 'projects on ' . $tblpx . 'expenses.projectid = ' . $tblpx . 'projects.pid
			inner join ' . $tblpx . 'users on ' . $tblpx . 'expenses.userid = ' . $tblpx . 'users.userid
			inner join ' . $tblpx . 'status on ' . $tblpx . 'expenses.type = ' . $tblpx . 'status.sid WHERE (' . $newQuery1 . ') ORDER BY exp_id DESC LIMIT 6')->queryAll();

            //print_r($sql); die();	
            //$model = new Projects;

            $sql1 = Yii::app()->db->createCommand('select pid,name,description from ' . $tblpx . 'projects 
            WHERE (' . $newQuery . ') ORDER BY pid DESC LIMIT 6 ')->queryAll();
            //print_r($sql1); 
        } else {
            //die(Yii::app()->user->id);
            $sql = Yii::app()->db->createCommand('select exp_id,name,type,exptype,payment_type,caption,date,amount,first_name,expense_type,' . $tblpx . 'expenses.description from ' . $tblpx . 'expenses
			inner join ' . $tblpx . 'projects on ' . $tblpx . 'expenses.projectid = ' . $tblpx . 'projects.pid
			inner join ' . $tblpx . 'users on ' . $tblpx . 'expenses.userid = ' . $tblpx . 'users.userid
			inner join ' . $tblpx . 'status on ' . $tblpx . 'expenses.type = ' . $tblpx . 'status.sid 
			where ' . $tblpx . 'users.userid=' . Yii::app()->user->id . '  AND (' . $newQuery1 . ')
			ORDER BY exp_id DESC LIMIT 6')->queryAll();
            //$model = new Projects;

            $sql1 = Yii::app()->db->createCommand('select pid,name,description from ' . $tblpx . 'projects
		    inner join ' . $tblpx . 'project_assign on ' . $tblpx . 'projects.pid=' . $tblpx . 'project_assign.projectid
		    inner join ' . $tblpx . 'users on ' . $tblpx . 'project_assign.userid = ' . $tblpx . 'users.userid
		    where ' . $tblpx . 'users.userid=' . Yii::app()->user->id . ' AND (' . $newQuery . ')
		    ORDER BY pid DESC LIMIT 6 ')->queryAll();
            //print_r($sql1); 
        }
        $limit_count = 5;
        $editmodel = new Editrequest;
        $deletemodel = new Deletepending;
        $notifications = new Notifications;
        $pendingquotation = new Scquotation;
        $purchse = new Purchase;
        $pendingquotation->approve_status = "No";
        $pendingquotation->added_viewed_notification=NULL;
        $this->getExpenseNotifications();
        $expense_notifications = new ExpenseNotification();
       
        $criteria = new CDbCriteria;
        $criteria->select = 't.project_id'; 
        $criteria->join = 'INNER JOIN jp_projects jp ON t.project_id = jp.pid'; // Join with jp_projects table
        $criteria->compare('t.view_status', 1); 
        $expenseNotificationss = ExpenseNotification::model()->findAll($criteria);

        if (isset($_GET['Projects']['project_id']) && !empty($_GET['Projects']['project_id'])) {
            $project_id = $_GET['Projects']['project_id'];
        } else {
            $project_id = '';
        }

        if (isset($_GET['Projects']['company_id']) && !empty($_GET['Projects']['company_id'])) {
            $company_id = $_GET['Projects']['company_id'];
        } else {
            $company_id = '';
        }

        $finance_model = new Projects('financialreport1');
        if (isset($_GET['Projects'])) {
            $finance_model->attributes = $_GET['Projects'];
        }
        
        $financialdata1 = $finance_model->financialreport1();
        $fixedprojects = $finance_model->fixedprojects();
        $completed = $finance_model->completed();
        $buyer_module = GeneralSettings::model()->checkBuyerModule();
        
        
        
       
        
        $this->render('cashbalance', array(
                        'model' => $finance_model, 'project_id' => $project_id, 'financialdata1' => $financialdata1, 'company_id' => $company_id, 'buyer_module' => $buyer_module, 'fixedprojects' => $fixedprojects,
                        'completed' => $completed,
                    )); 
        } 
    }
     public function actionFinancialReport(){
        {
        $tblpx = Yii::app()->db->tablePrefix;
        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        $newQuery = "";
        $newQuery1 = "";
        foreach ($arrVal as $arr) {
            if ($newQuery)
                $newQuery .= ' OR';
            if ($newQuery1)
                $newQuery1 .= ' OR';
            $newQuery .= " FIND_IN_SET('" . $arr . "', " . $tblpx . "projects.company_id)";
            $newQuery1 .= " FIND_IN_SET('" . $arr . "', " . $tblpx . "expenses.company_id)";
        }

        if (Yii::app()->user->role == 1) {

            $sql = Yii::app()->db->createCommand('select exp_id,name,type,exptype,payment_type,caption,date,amount,first_name,expense_type,' . $tblpx . 'expenses.description from ' . $tblpx . 'expenses
			inner join ' . $tblpx . 'projects on ' . $tblpx . 'expenses.projectid = ' . $tblpx . 'projects.pid
			inner join ' . $tblpx . 'users on ' . $tblpx . 'expenses.userid = ' . $tblpx . 'users.userid
			inner join ' . $tblpx . 'status on ' . $tblpx . 'expenses.type = ' . $tblpx . 'status.sid WHERE (' . $newQuery1 . ') ORDER BY exp_id DESC LIMIT 6')->queryAll();

            //print_r($sql); die();	
            //$model = new Projects;

            $sql1 = Yii::app()->db->createCommand('select pid,name,description from ' . $tblpx . 'projects 
            WHERE (' . $newQuery . ') ORDER BY pid DESC LIMIT 6 ')->queryAll();
            //print_r($sql1); 
        } else {
            //die(Yii::app()->user->id);
            $sql = Yii::app()->db->createCommand('select exp_id,name,type,exptype,payment_type,caption,date,amount,first_name,expense_type,' . $tblpx . 'expenses.description from ' . $tblpx . 'expenses
			inner join ' . $tblpx . 'projects on ' . $tblpx . 'expenses.projectid = ' . $tblpx . 'projects.pid
			inner join ' . $tblpx . 'users on ' . $tblpx . 'expenses.userid = ' . $tblpx . 'users.userid
			inner join ' . $tblpx . 'status on ' . $tblpx . 'expenses.type = ' . $tblpx . 'status.sid 
			where ' . $tblpx . 'users.userid=' . Yii::app()->user->id . '  AND (' . $newQuery1 . ')
			ORDER BY exp_id DESC LIMIT 6')->queryAll();
            //$model = new Projects;

            $sql1 = Yii::app()->db->createCommand('select pid,name,description from ' . $tblpx . 'projects
		    inner join ' . $tblpx . 'project_assign on ' . $tblpx . 'projects.pid=' . $tblpx . 'project_assign.projectid
		    inner join ' . $tblpx . 'users on ' . $tblpx . 'project_assign.userid = ' . $tblpx . 'users.userid
		    where ' . $tblpx . 'users.userid=' . Yii::app()->user->id . ' AND (' . $newQuery . ')
		    ORDER BY pid DESC LIMIT 6 ')->queryAll();
            //print_r($sql1); 
        }
        $limit_count = 5;
        $editmodel = new Editrequest;
        $deletemodel = new Deletepending;
        $notifications = new Notifications;
        $pendingquotation = new Scquotation;
        $purchse = new Purchase;
        $pendingquotation->approve_status = "No";
        $pendingquotation->added_viewed_notification=NULL;
        $this->getExpenseNotifications();
        $expense_notifications = new ExpenseNotification();
       
        $criteria = new CDbCriteria;
        $criteria->select = 't.project_id'; 
        $criteria->join = 'INNER JOIN jp_projects jp ON t.project_id = jp.pid'; // Join with jp_projects table
        $criteria->compare('t.view_status', 1); 
        $expenseNotificationss = ExpenseNotification::model()->findAll($criteria);

        if (isset($_GET['Projects']['project_id']) && !empty($_GET['Projects']['project_id'])) {
            $project_id = $_GET['Projects']['project_id'];
        } else {
            $project_id = '';
        }

        if (isset($_GET['Projects']['company_id']) && !empty($_GET['Projects']['company_id'])) {
            $company_id = $_GET['Projects']['company_id'];
        } else {
            $company_id = '';
        }

        $finance_model = new Projects('financialreport1');
        if (isset($_GET['Projects'])) {
            $finance_model->attributes = $_GET['Projects'];
        }
        
        $financialdata1 = $finance_model->financialreport1();
        $fixedprojects = $finance_model->fixedprojects();
        $completed = $finance_model->completed();
        $buyer_module = GeneralSettings::model()->checkBuyerModule();
        
        $this->render('financialreports', array(
                        'model' => $finance_model, 'project_id' => $project_id, 'financialdata1' => $financialdata1, 'company_id' => $company_id, 'buyer_module' => $buyer_module, 'fixedprojects' => $fixedprojects,
                        'completed' => $completed,
                    ));
        
       
        
        
        } 
    }
    

}
