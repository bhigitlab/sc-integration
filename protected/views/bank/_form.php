<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.js"></script>
<?php
$user = Users::model()->findByPk(Yii::app()->user->id);
$arrVal = explode(',', $user->company_id);
$newQuery = "";
foreach($arrVal as $arr) {
    if ($newQuery) $newQuery .= ' OR';
    $newQuery .= " FIND_IN_SET('".$arr."', id)"; 
}
$company = Company::model()->findAll(array('condition' => $newQuery));
?>

<div class="">
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id'=>'bank-form',
        'enableAjaxValidation' => true,
        'enableClientValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true,
            'validateOnType' => false,),
    ));
    ?>

    <div class="panel-body">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <?php echo $form->labelEx($model,'bank_name'); ?>
                        <?php echo $form->textField($model,'bank_name',array('size'=>60,'maxlength'=>100,'class'=>'form-control')); ?>
                        <?php echo $form->error($model,'bank_name'); ?>
                    </div>
                </div>
                <div class="col-md-4">
                <div class="form-group">
                    <?php echo $form->labelEx($model,'company_id'); ?>
                <select name="Bank[company_id]" id="Bank_company_id" class="form-control js-example-basic-multiple">
                    <option value="">Choose Company</option>
                    <?php
                    foreach ($company as $key => $value) {
                        ?>	
                        <option value="<?php echo $value['id']; ?>" <?php echo ($value['id']== $model->company_id)?'selected':''; ?>><?php echo $value['name']; ?></option>
                        <?php
                    }
                    ?>

                </select>
                <?php echo $form->error($model, 'company_id'); ?>
                </div>
                </div>
                
                <div class="col-md-4">
                    <label style="display:block;">&nbsp;</label>
                    <div class="save-btnHold">

                        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'btn btn-info btn-sm submit')); ?>
                        <?php   echo CHtml::ResetButton('Close', array('onclick'=>'closeaction(this,event);','class'=>'btn btn-sm'));  ?>
                    </div>
                </div>
            </div>
    </div>
            
    

<?php $this->endWidget(); ?>
 </div><!-- form -->                   


    
    <script>
         $(document).ready(function() {
            $('.js-example-basic-multiple').select2({
            width: '200px'
        });
        });
        $(document).ready(function () {

            //select all checkboxes
            $("#select_all").change(function () {  //"select all" change
                var status = this.checked; // "select all" checked status
                $('.checkbox').each(function () { //iterate all listed checkbox items
                    this.checked = status; //change ".checkbox" checked status
                });
            });

            $('.checkbox').change(function () { //".checkbox" change
                //uncheck "select all", if one of the listed checkbox item is unchecked
                if (this.checked == false) { //if this item is unchecked
                    $("#select_all")[0].checked = false; //change "select all" checked status to false
                }

                //check "select all" if all checkbox items are checked
                if ($('.checkbox:checked').length == $('.checkbox').length) {
                    $("#select_all")[0].checked = true; //change "select all" checked status to true
                }
            });


        });

        function countOfLetters(str) {
            var letter = 0;
            for (i = 0; i < str.length; i++) {
                if ((str[i] >= 'A' && str[i] <= 'Z')
                    || (str[i] >= 'a' && str[i] <= 'z'))
                    letter++;
            }
        
            return letter;
        }        
      
        $("#Bank_bank_name").keyup(function () {         
            var alphabetCount =  countOfLetters(this.value);  
            var message ="";      
            var disabled=false;
            if(alphabetCount < 1){
                var message = "Invalid Bank Name";
                var disabled=true;           
            }

            $(this).siblings(".errorMessage").show().html(message).addClass('d-block');                
            $(".submit").attr('disabled',disabled);
       
        });

    </script>
    <style>
        label{
            display:block;
        }
    </style>