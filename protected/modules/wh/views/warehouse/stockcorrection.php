<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.js"></script>
<div class="container">
    <div class="sub-heading mb-10">       
        <h3>STOCK CORRECTION</h3>
    </div>
    <?php echo $this->renderPartial('_stockcorrectionsearch'); ?>
    <div class="stock_item"></div>
    <div class="text-right">
        <span class="color_box bg-warning"></span><span>Pending</span>
        <span class="color_box bg-default"></span><span>Approved</span>
        <span class="color_box bg-danger"></span><span>Rejected</span>
    </div>
    <?php 
    $this->widget('zii.widgets.CListView', array(
        'dataProvider' => $dataProvider,
        'itemView' => '_stockcorrectionview', // Create this file
        'enableSorting' => true,
        'enablePagination' => true,
        'template' => '{summary}<table class="table">{items}</table>{pager}',
        'pager' => array(
            'header' => '',
            'firstPageLabel' => 'First',
            'lastPageLabel' => 'Last',
            'prevPageLabel' => '&laquo; Prev',
            'nextPageLabel' => 'Next &raquo;',
            'maxButtonCount' => 5,
            'cssFile' => false, // Remove default Yii pagination CSS
        ),
    ));
    ?>
</div>
<script>

    $(document).ready(function() {    
        $(document).on("click",".approve_btn",function() {            
            var id = $(this).attr('data-id');        
            $.ajax({
                type: "POST",  
                dataType: "json",                      
                data: {
                    "id": id,
                    "status":'1'
                },
                url: "<?php echo $this->createUrl('warehouse/changeStockCorrectionStatus&id=') ?>" + id,
                success: function (reponse) {

                    alert(reponse.msg)
                    location.reload();
                }
            })
        });   

        $(document).on("click",".btn_reject",function() {            
            var id = $(this).attr('data-id');        
            $.ajax({
                type: "POST",  
                dataType: "json",                      
                data: {
                    "id": id,
                    "status":'2'
                },
                url: "<?php echo $this->createUrl('warehouse/changeStockCorrectionStatus&id=') ?>" + id,
                success: function (reponse) {

                    alert(reponse.msg)
                    location.reload();
                }
            })
        }); 
    
    });

    $(".select_box").select2();
    $(".btn_search").click(function () {
        var warehouse_id = $(".warehouse_id").val();
        var item_id = $(".receiptItems").val();
        $.ajax({
            type: "POST",
            data: {"warehouse_id": warehouse_id, "item_id": item_id},
            url: "<?php echo $this->createUrl('warehouse/getStockItems') ?>",
            success: function (response)
            {
                $(".stock_item").html(response);
            }
        });
    })

    $(".warehouse_id").change(function () {
        var warehouse_id = $(this).val();
        $.ajax({
            type: "POST",
            url: "<?php echo $this->createUrl('warehouse/getReceiptItems&warehouseId=') ?>" + warehouse_id,
            success: function (response)
            {
                $(".receiptItems").html(response).trigger("change");
            }
        });
    })
    $(".popover-test").popover({
        html: true,
        content: function () {
            return $(this).next('.popover-content').html();
        }
    });
    $('[data-toggle=popover]').on('click', function (e) {
        $('[data-toggle=popover]').not(this).popover('hide');
    });
    $('body').on('hidden.bs.popover', function (e) {
        $(e.target).data("bs.popover").inState.click = false;
    });
    $('body').on('click', function (e) {
        $('[data-toggle=popover]').each(function () {
            if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                $(this).popover('hide');
            }
        });
    });
</script>
<style>
    .select2 {
        width:100% !important;
    }
    .color_box{    
        height: 15px;
        width: 15px;
        display: inline-flex;
        margin-right: 3px;
        margin-bottom: -2px;
    }
    
</style>