<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/tableHeadFixer.js"></script>
<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$this->breadcrumbs = array(
    'Sales Executive Report',
)
    ?>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/tableHeadFixer.js"></script>
<div class="container" id="project">
    <div class="sub-heading mb-10">
        <h3>Sales Quotation report</h3>
    </div>

    <?php
    $this->renderPartial('_newsearch', array(
        'model' => $model,
        'sales_executive' => $sales_executive,
        'location' => $location
    ))
        ?>
    <?php if (Yii::app()->user->hasFlash('success')): ?>
        <div class="info"
            style="width: 500px;margin-left: 400px;padding: 8px;color: green;font-size: 20px;font-weight: bold; padding-top: 46px;">
            <?php echo Yii::app()->user->getFlash('success'); ?>
        </div>
    <?php endif; ?>
    <?php if (Yii::app()->user->hasFlash('error')): ?>
        <div class="info"
            style="width: 500px;margin-left: 400px;padding: 8px;color: red;font-size: 20px;font-weight: bold; padding-top: 46px;">
            <?php echo Yii::app()->user->getFlash('error'); ?>
        </div>
    <?php endif; ?>


    <div id="msg_box"></div>
    <div class="weekly-list table-responsive">
        <table id="weekly_tbl" class="weekly_tbl">
            <thead class="entry-table sticky-thead">
                <tr>
                    <th>SI No</th>
                    <th>Quotation No</th>
                    <th>Client</th>
                    <th>Quotation Date</th>
                    <th>Location</th>
                    <th>Executive</th>
                    <th>Amount</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $i = 1;
                foreach ($final_array as $key => $data) {
                    ?>
                    <tr>
                        <th>
                            <?php echo $i; ?>
                        </th>
                        <th>
                            <?php echo $data['invoice_no']; ?>
                        </th>
                        <th>
                            <?php echo $data['client_name']; ?>
                        </th>
                        <th>
                            <?php echo $data['date_quotation']; ?>
                        </th>
                        <th>
                            <?php echo $data['location']; ?>
                        </th>
                        <th>
                            <?php echo $data['executive']; ?>
                        </th>
                        <th>
                            <?php echo $data['amount']; ?>
                        </th>
                    </tr>
                    <?php
                    $i++;
                }
                ?>
            </tbody>
            <tfoot>
            </tfoot>
        </table>
    </div>
</div>


<script>
    // $(document).ready(function () {
    //     $("#weekly_tbl").tableHeadFixer({
    //         'left': false,
    //         'foot': true,
    //         'head': true
    //     });
    // });
</script>