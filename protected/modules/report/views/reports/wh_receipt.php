<div class="table-responsive">
    <table cellpadding="10" class="table  list-view " id="fixtable">
        <thead class="entry-table">
            <tr>
                <th>SL No</th>
                <th>Receipt No</th>
                <th>Warehouse Name</th>
                <th>Goods Received From</th>
                <th>Clerk</th>
                <th>Date</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $daterange = '';
            if (!empty($date_from) || !empty($date_to)) {
                if ((isset($date_from) && !empty($date_from)) && ((isset($date_to) && empty($date_to)) || !isset($date_to))) {
                    $daterange .= ' WHERE `created_date` >="' . $date_from . '"';
                } else if ((isset($date_to) && !empty($date_to)) && ((isset($date_from) && empty($date_from)) || !isset($date_from))) {
                    $daterange .= ' WHERE `created_date` <="' . $date_to . '"';
                } else if ((isset($date_to) && !empty($date_to)) && (isset($date_from) && !empty($date_from))) {
                    $daterange .= 'WHERE created_date >="' . $date_from . '" AND created_date <="' . $date_to . '"';
                } else {
                    $daterange .= '';
                }
            }

            $sql = "SELECT * FROM jp_warehousereceipt " . $daterange;
            $despatch = Yii::app()->db->createCommand($sql)->queryAll();
            $i = 1;
            foreach ($despatch as $key => $value) {
                ?>
                <tr>
                    <td>
                        <?php echo $i; ?>
                    </td>
                    <td>
                        <?php
                        echo CHtml::link($value['warehousereceipt_no'], 'index.php?r=wh/warehousereceipt/view&receiptid=' . $value['warehousereceipt_id'], array('class' => 'link', 'target' => '_blank'));
                        ?>
                    </td>
                    <td>
                        <?php
                        $warehouse = Warehouse::model()->findByPk($value['warehousereceipt_warehouseid']);
                        echo $warehouse->warehouse_name;
                        ?>
                    </td>
                    <td>
                        <?php
                        if ($value['warehousereceipt_warehouseid_from'] != "") {
                            $warehouse_receive_from = Warehouse::model()->findByPk($value['warehousereceipt_warehouseid_from']);
                            echo $warehouse_receive_from->warehouse_name;
                        } else {
                            $vendor = "";
                            $bill = Bills::model()->findByPk($value['warehousereceipt_bill_id']);
                            if ($bill["purchase_id"] != NULL) {
                                $tblpx = Yii::app()->db->tablePrefix;
                                $purchase_data = Yii::app()->db->createCommand("SELECT {$tblpx}projects.name as project_name,{$tblpx}vendors.name as vendor_name FROM {$tblpx}purchase LEFT JOIN {$tblpx}projects ON {$tblpx}purchase.project_id = {$tblpx}projects.pid LEFT JOIN {$tblpx}vendors ON {$tblpx}purchase.vendor_id={$tblpx}vendors.vendor_id  WHERE {$tblpx}purchase.p_id = " . $bill["purchase_id"] . "")->queryRow();
                                $vendor = " / " . $purchase_data['vendor_name'];
                            }

                            echo CHtml::link(
                                $bill['bill_number'],
                                'index.php?r=bills/view&id=' . $value['warehousereceipt_bill_id'],
                                array('class' => 'link', 'target' => '_blank')
                            ) . $vendor;
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        $clerk = Users::model()->findByPk($value['warehousereceipt_clerk']);
                        echo $clerk['first_name'] . ' ' . $clerk['last_name'];
                        ?>
                    </td>
                    <td>
                        <?php echo date("d-m-Y", strtotime($value['warehousereceipt_date'])); ?>
                    </td>
                </tr>
                <?php
                $i++;
            }
            ?>
        </tbody>

    </table>
</div>