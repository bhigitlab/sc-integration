  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header clearfix">
        <h5 class="modal-title pull-left" id="exampleModalLabel" style="line-height:3;">UPLOAD</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <?php
      $form = $this->beginWidget('CActiveForm', array(
            'id' => 'quotation-item-master-form',
            'action' => Yii::app()->createAbsoluteUrl("salesquotation/salesQuotation/uploadBanner"),
            'htmlOptions'=>array('enctype' => 'multipart/form-data'),
            // 'enableAjaxValidation' => true,
            'enableClientValidation' => true,
            'clientOptions' => array(
                'validateOnSubmit' => true,
                'validateOnChange' => true,
                'validateOnType' => false,
            ),
        ));
        ?>
      <div class="modal-body">  
        <div class="row">
          <div class="col-md-6">
            <?php   
            $bannermodel =  QuotationGeneratorImage::model()->find(); 

            $model = !empty($bannermodel) ?$bannermodel:new QuotationGeneratorImage();
            
            echo $form->label($model, 'banner_image');
            echo $form->fileField($model, 'banner_image', array('id' => 'banner_image'));        
            ?>
            <div>(width*height)(1000px*1500px)</div>
          </div>
          <div class="col-md-6">
            <?php                     
            echo $form->label($model, 'footer_image');
            echo $form->fileField($model, 'footer_image', array('id' => 'footer_image'));        
            ?>
            <div>(width*height)(1000px*500px)</div>
          </div>
        </div>      
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
      <?php $this->endWidget(); ?>
    </div>
  </div>

  <script>
    var _URL = window.URL || window.webkitURL;
$("#footer_image").change(function (e) {
    var file, img;
    if ((file = this.files[0])) {
        img = new Image();
        var objectUrl = _URL.createObjectURL(file);
        img.onload = function () {
            alert(this.width + " " + this.height);
            _URL.revokeObjectURL(objectUrl);
        };
        img.src = objectUrl;
    }
});
    </script>