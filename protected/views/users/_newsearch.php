<?php
/* @var $this UsersController */
/* @var $model Users */
/* @var $form CActiveForm */
?>
<!--<h5>Filter By :</h5>-->


	<?php $form=$this->beginWidget('CActiveForm', array(
		'action'=>Yii::app()->createUrl($this->route),
		'method'=>'get',
	)); ?>
	<div class="page_filter clearfix custom-form-style">
		<div class="row">
			<div class="col-md-3 col-sm-6 padding-y-5 filter_elem">
				<?php echo $form->textField($model,'username',array('size'=>30,'maxlength'=>30,'placeholder'=>'User Name','class'=>'form-control')); ?>
			</div>
			<div class="col-md-3 col-sm-6 padding-y-5 filter_elem">
				<?php echo $form->dropDownList($model, 'user_type', CHtml::listData(UserRoles::model()->findAll(array('order' => 'role ASC')), 'id', 'role'), array('empty' => '-Choose a role-','class'=>'form-control')); ?>
			</div>
			<div class="col-md-3 col-sm-6 padding-y-5 filter_elem">
				<?php echo $form->dropDownList($model,'status',array(0=>"Active",1=>"InActive"),array('empty'=>'-Choose a status-','class'=>'form-control')); ?>
			</div>
			<div class="col-md-3 col-sm-6 padding-y-5 filter_elem">
				<?php
					$where ='';
					if(Yii::app()->user->role !=1){
						$where = "AND t.company_id=".Yii::app()->user->company_id."";
					}
					$tblpx = Yii::app()->db->tablePrefix;
					$sql="SELECT `t`.`userid`, concat_ws(' ',`t`.`first_name`, `t`.`last_name`) as full_name,"
					. "`{$tblpx}user_roles`.`role` "
					. "FROM `{$tblpx}users` `t` JOIN {$tblpx}user_roles ON {$tblpx}user_roles.id= t.user_type "
					. "WHERE status=0 and user_type in (1,2) ".$where."  ORDER BY user_type,full_name ASC";
					$result = Yii::app()->db->createCommand($sql)->queryAll();
					$listdata = CHtml::listData($result, 'userid', 'full_name','role');
					echo $form->dropDownList($model, 'reporting_person',
					$listdata,
					array('empty' => '----All-----','class'=>'form-control'));
				?>
			</div>
			<div class="col-md-3 col-sm-6 padding-y-5 filter_elem filter_btns pull-right">
				<input id="clear" class="pull-right" type="reset" value="Clear"/>
			</div>
		</div>
	</div>
	<?php $this->endWidget(); ?>
