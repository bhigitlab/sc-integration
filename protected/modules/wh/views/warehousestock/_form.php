
<?php
Yii::app()->clientscript->scriptMap['jquery.min.js'] = false;
?>


<div class="">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'warehousestock-form',
        'action' =>'#',
        /*'enableAjaxValidation' => true,
        'enableClientValidation'=>true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true,
            'validateOnType' => false,), */
    ));
    ?>
   

<div class="panel-body">
   
    <div class="row addRow">
        <div class="col-md-6">
            <?php echo $form->labelEx($model,'warehousestock_warehouseid'); ?>
            <?php echo $form->dropDownList($model, 'warehousestock_warehouseid', CHtml::listData(Warehouse::model()->findAll(array('select'=>array('warehouse_id','warehouse_name'),'order' => 'warehouse_id DESC')), 'warehouse_id', 'warehouse_name'), array('empty' => '-Choose a Warehouse-', 'class' => 'form-control mandatory')); ?>
            <?php echo $form->error($model,'warehousestock_warehouseid'); ?>
            <div class="req"></div>
        </div>
        <div class="col-md-6">
            <!--<?php echo $form->labelEx($model,'warehousestock_itemid'); ?>
            <select class="form-control mandatory" name="Warehousestock[warehousestock_itemid]" id="Warehousestock_warehousestock_itemid">
            <option value="">-Choose a Item-</option>
            <?php foreach($specification as $key=> $value){ ?>
            <option value="<?php echo $value['id']; ?>" <?php echo ($value['id'] == $model->warehousestock_itemid)?'selected':''; ?>><?php echo $value['data']; ?></option>
            <?php } ?>
            </select>
            <?php echo $form->error($model,'warehousestock_itemid'); ?>
            <div class="req"></div> -->
            
            <?php echo $form->labelEx($model,'warehousestock_date'); ?>
            <?php echo $form->textField($model,'warehousestock_date',array('class'=>'form-control mandatory')); ?>
            <?php echo $form->error($model,'warehousestock_date'); ?>
            <div class="req"></div>
        </div>
        
    </div>
    <div class="row addRow">
        <div class="col-md-6">
            <?php echo $form->labelEx($model,'warehousestock_unit'); ?>
            <?php echo $form->textField($model,'warehousestock_unit',array('size'=>60,'maxlength'=>200,'class' => 'form-control mandatory')); ?>
            <?php echo $form->error($model,'warehousestock_unit'); ?>
            <div class="req"></div>
            
        </div>
        <div class="col-md-6">
            
        </div>

        </div>
    
    <div class="row addRow">
        <div class="col-md-6">
            <?php echo $form->labelEx($model,'warehousestock_stock_quantity'); ?>
		<?php echo $form->textField($model,'warehousestock_stock_quantity',array('class'=>'form-control mandatory')); ?>
		<?php echo $form->error($model,'warehousestock_stock_quantity'); ?>
            <div class="req"></div>
            
        </div>
        <div class="col-md-6">
            <?php // echo $form->labelEx($model,'warehousestock_date'); ?>
            <?php // echo $form->textField($model,'warehousestock_date',array('class'=>'form-control')); ?>
            <?php // echo $form->error($model,'warehousestock_date'); ?>
        </div>

        </div>
            <input type="hidden" name="Warehousestock[warehousestock_id]" value="" id="warehouseStockId"/>
    </div>
    <div class="panel-footer text-center btn_top save-btnHold">
        <?php echo CHtml::Button($model->isNewRecord ? 'Create' : 'Save',array('class'=>'btn btn-info btn_submit')); ?> 
        <?php  
                if(!$model->isNewRecord){
                  
                  echo CHtml::Button('close', array('class'=>'btn ','onclick'=>'closeaction(this,event);'));
                    } 
                    else{
                       echo CHtml::ResetButton('Reset', array('class'=>'btn btn-default btnreset','style'=>'margin-right:3px;')); 
                       echo CHtml::Button('close', array('class'=>'btn','onclick'=>'closeaction(this,event);'));  
                    }
                 ?>
        <!--<button data-dismiss="modal" onclick="javascript:window.location.reload()">Close</button>-->
     <?php // echo CHtml::ResetButton('Reset', array('onclick' => 'javascript:location.href="'. $this->createUrl('create').'"')); ?>
    </div>
    <?php $this->endWidget(); ?>

</div><!-- form -->


<script>
    $('#Warehousestock_warehousestock_itemid').change(function(){
        var element = $(this);
        var category_id= $(this).val();
        $.ajax({
                url:'<?php echo  Yii::app()->createAbsoluteUrl('warehousestock/getunit'); ?>',
                type:'GET',
                dataType:'json',
                data:{data:category_id},
                success: function(result) {
                        $("#Warehousestock_warehousestock_unit").val(result.unit);
                }

        })

    })
$(function () {
        $( "#Warehousestock_warehousestock_date" ).datepicker({dateFormat: 'dd-mm-yy'});
    });
    
    $('#warehousestock-form .btn_submit').unbind().click(function(){
        flag = 0;
         $('.mandatory').each(function() {
             if(this.value == ""){
                 $(this).parent().find('.req').addClass('req_field');
                 flag++;
             }else{
                 $(this).parent().find('.req').removeClass('req_field');
             }
         });
         if (flag >= 1) {
             return false;
         } else {
             //return true;
            var stockId = $("#warehouseStockId").val();
            var actionUrl;
            if(stockId == "") {
                actionUrl = "<?php echo Yii::app()->createAbsoluteUrl("warehousestock/savedata"); ?>";
            } else {
                actionUrl = "<?php echo Yii::app()->createAbsoluteUrl("warehousestock/updatedata"); ?>";
            }
            var data =  $('#warehousestock-form').serialize();
            $.ajax({
                url:actionUrl,
                type:'POST',
                data:data,
                success: function(result) {
                    $("#addclient").slideUp();
                    $("#warehousestock-form").trigger("reset");
                    if(result == 1){
                        
                    }else{
                        $("#newlist").html(result);
                         $("#myTable").dataTable( {
                            "scrollY": "300px",
                            "scrollCollapse": true,
                            "paging": false,
                            "columnDefs": [ {
                                "targets": [0],
                                "searchable": false
                              },
                            ]
                        } );
                    }
                }

            })
         }
     })
</script>
<style>
.mandatoryfld .error{color:red;font-weight:normal}
.mandatory.error{color:inherit;}
.req_field::after{content: " This field is required";}
.req_field{color:red !important;font-weight:normal !important}
.req{color:red !important;font-weight:normal !important}
</style>