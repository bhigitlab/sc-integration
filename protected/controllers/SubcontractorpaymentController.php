<?php

class SubcontractorpaymentController extends Controller
{

    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        $accessArr = array();
        $accessauthArr = array();
        $controller = Yii::app()->controller->id;
        if (isset(Yii::app()->user->menuall)) {
            if (array_key_exists($controller, Yii::app()->user->menuall)) {
                $accessArr = Yii::app()->user->menuall[$controller];
            }
        }

        if (isset(Yii::app()->user->menuauth)) {
            if (array_key_exists($controller, Yii::app()->user->menuauth)) {
                $accessauthArr = Yii::app()->user->menuauth[$controller];
            }
        }
        $access_privlg = count($accessauthArr);
        return array(
            array(
                'allow', // allow all users to perform 'index' and 'view' actions
                'actions' => $accessArr,
                'users' => array('@'),
                'expression' => "$access_privlg > 0",
            ),
            array(
                'allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => $accessauthArr,
                'users' => array('@'),
                'expression' => "$access_privlg > 0",
            ),
            array(
                'allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('getquotations', 'expenseproject', 'checkQuotationDate', 'getstageamount', 'Allentries', 'agingreportbydate'),
                'users' => array('*'),
            ),
            array(
                'deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex()
    {
        $this->render('index');
    }

    public function actionGetPaymentDetails()
    {
        $paymentid = $_REQUEST["paymentid"];
        $expStatus = $_REQUEST["expstatus"];
        $tblpx = Yii::app()->db->tablePrefix;
        $expenseTbl = "" . $tblpx . "subcontractor_payment";
        $modelName = 'SubcontractorPayment';

        if ($expStatus == 0) {
            $expenseTbl = "" . $tblpx . "pre_subcontractor_payment";
            $modelName = 'PreSubcontractorPayment';
        }

        $paymentData = Yii::app()->db->createCommand("SELECT * FROM " . $expenseTbl
            . " WHERE payment_id = " . $paymentid)->queryRow();

        $uniqueid = $paymentData["uniqueid"];
        $final_result = array();

        if ($uniqueid) {

            $paymentdetails = $modelName::model()->findAll(array("condition" => "uniqueid='" . $uniqueid . "'"));

            $result["payment_id"] = $paymentData["payment_id"];
            $result["subcontractor_id"] = $paymentData["subcontractor_id"];
            $result["company"] = $paymentData["company_id"];
            $result["project_id"] = $paymentData["project_id"];
            $result["description"] = $paymentData["description"];
            $result["payment_type"] = $paymentData["payment_type"];
            $result["bank"] = $paymentData["bank"];
            $result["cheque_no"] = $paymentData["cheque_no"];
            $result["sgst"] = $paymentData["sgst"];
            $result["cgst"] = $paymentData["cgst"];
            $result["igst"] = $paymentData["igst"];
            $result["tds"] = $paymentData["tds"];
            $result["tdsamount"] = $paymentData["tds_amount"];
            $result["paydate"] = $paymentData["date"];
            $result["employee_id"] = $paymentData["employee_id"];
            $result["quotation_number"] = $paymentData["quotation_number"];
            $result["uniqueid"] = $uniqueid;
            $projectids = "0";
            $totalPayment = 0;
            $tds_amount_total = 0;
            $rowId = 1;
            $result["row"] = "";
            foreach ($paymentdetails as $payments) {
                $final_result[] = $payments->attributes;
                $subconData = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}scquotation INNER JOIN {$tblpx}subcontractor ON {$tblpx}scquotation.subcontractor_id= {$tblpx}subcontractor.subcontractor_id LEFT JOIN {$tblpx}projects ON {$tblpx}projects.pid={$tblpx}scquotation.project_id WHERE {$tblpx}subcontractor.subcontractor_id=" . $paymentData["subcontractor_id"] . " AND FIND_IN_SET({$paymentData["company_id"]}, {$tblpx}subcontractor.company_id) AND {$tblpx}projects.pid = {$payments->project_id} GROUP BY {$tblpx}scquotation.project_id")->queryAll();
                $projectids .= ", " . $payments->project_id;
                $result["row"] .= '<div class="row_block paymentrow" id="' . $rowId . '">';
                if ($rowId == 1) {
                    $result["row"] .= '<h4>Amount Details</h4><div class="row_block"><div class="elem_block" style="width: 230px;"><label>Project</label></div><div class="elem_block" style="width: 130px;"><label>Amount</label></div><div class="elem_block amounts"><label>SGST</label></div><div class="elem_block amounts"><label>CGST</label></div><div class="elem_block amounts"><label>IGST</label></div><div class="elem_block amounts"><label>Tax Total</label></div><div class="elem_block amountlong"><label>Total Amount</label></div><div class="elem_block gsts" style="width:130px;"><label>TDS (%)</label></div><div class="elem_block amountlong"><label>Amount To Subcontractor</label></div></div>';
                }
                $result["row"] .= '<div class="elem_block">
                        <div class="form-group">
                            ';
                $result["row"] .= '<select class="form-control js-example-basic-single multi_projects singleproject" name="project_name[]" style="width:200px;" id="SubcontractorPayment_project_id' . $rowId . '" data-id="' . $rowId . '">
                                <option  value="">-Select Project-</option>';
                if (!empty($subconData)) {
                    foreach ($subconData as $pro) {
                        if ($payments->project_id == $pro['pid'])
                            $selected = " selected";
                        else
                            $selected = "";
                        $result["row"] .= '<option value="' . $pro['pid'] . '"' . $selected . '>' . $pro['name'] . '</option>';
                    }
                }
                $result["row"] .= '</select>';
                $result["row"] .= '</div>
                    </div>
                    <input type="hidden" class="form-control ccfcf" id="Payment_id' . $rowId . '" name="SubcontractorPayment[payment_id]" data-id="' . $rowId . '" value="' . $payments->payment_id . '"/>
                    <div class="elem_block amountfld">
                        <div class="form-group">
                            <input type="text" class="form-control amountvalidation sub_paymentamount" name="SubcontractorPaymentamount[]" id="SubcontractorPayment_amount' . $rowId . '" onkeypress ="nextSectionode(this, event)" data-id="' . $rowId . '" value="' . $payments->amount . '"/>
                        </div>
                    </div>
                    <div class="elem_block gsts amounts">
                        <div class="form-group">
                            <span  class="gstvalue" style="display:inline-block;" id="txtSgst' . $rowId . '">' . $payments->sgst_amount . '</span>
                        </div>
                    </div>
                    <div class="elem_block gsts amounts">
                        <div class="form-group">
                            <span  class="gstvalue" style="display:inline-block;" id="txtCgst' . $rowId . '">' . $payments->cgst_amount . '</span>
                        </div>
                    </div>
                    <div class="elem_block gsts amounts">
                        <div class="form-group">
                            <span  class="gstvalue" style="display:inline-block;" id="txtIgst' . $rowId . '">' . $payments->igst_amount . '</span>
                        </div>
                    </div>
                    <div class="elem_block amounts">
                        <div class="form-group">
                            <input type="hidden" class="form-control amountvalidation" readonly="true" id="SubcontractorPayment_tax_amount' . $rowId . '" name="SubcontractorPayment[tax_amount]" value="' . $payments->tax_amount . '"/>
                            <span class="gstvalue" style="display:inline-block;" id="txtgstTotal' . $rowId . '">' . $payments->tax_amount . '</span>
                        </div>
                    </div>
                     <div class="elem_block amountlong">
                        <div class="form-group">
                            <span class="gstvalue" style="display:inline-block;" id="txtTotal' . $rowId . '">' . Controller::money_format_inr(($payments->amount + $payments->tax_amount), 2) . '</span>
                        </div>
                    </div>
                    <div class="elem_block gsts">
                        <div class="form-group">
                            <input type="text" class="form-control amountvalidation percentage tdsrate" id="SubcontractorPayment_tds' . $rowId . '" name="SubcontractorPayment[tds]" data-id="' . $rowId . '" value="' . $payments->tds . '"/>
                            <span class="gstvalue" style="display:inline-block;" id="txtTds' . $rowId . '">' . Controller::money_format_inr(($payments->tds_amount), 2) . '</span>
                            <input type="hidden" class="form-control amountvalidation percentage" id="SubcontractorPayment_tds_amount' . $rowId . '" name="SubcontractorPayment[tds_amount]" value="' . $payments->tds_amount . '"/>
                        </div>
                    </div>
                    <div class="elem_block amountlong">
                        <div class="form-group">
                            <span class="gstvalue" style="display:inline-block;" id="txtPaidAmount' . $rowId . '">' . Controller::money_format_inr((($payments->amount + $payments->tax_amount) - $payments->tds_amount), 2) . '</span>
                            <input type="hidden" class="form-control amountvalidation percentage" id="SubcontractorPayment_paidamount' . $rowId . '" name="SubcontractorPayment[paidamount]" value="' . (($payments->amount + $payments->tax_amount) - $payments->tds_amount) . '"/>
                        </div>
                    </div>
                    <input type="hidden" class="permission" name="txtPermission' . $rowId . '" id="txtPermission' . $rowId . '" value="' . $payments->approve_status . '"/>
                </div><div id="errorMessage' . $rowId . '" class="rowerror"></div>';
                $totalPayment = $totalPayment + ($payments->amount + $payments->tax_amount - $payments->tds_amount);
                $tds_amount_total += $payments->tds_amount;
                $rowId = $rowId + 1;
            }
            $result["total"] = $totalPayment;
            $result['tds_amount_total'] = $tds_amount_total;
            $result["totalrow"] = $rowId - 1;
        } else {
            $result["payment_id"] = $paymentData["payment_id"];
            $result["paydate"] = $paymentData["date"];
            $result["subcontractor_id"] = $paymentData["subcontractor_id"];
            $result["project_id"] = $paymentData["project_id"];
            $result["amount"] = $paymentData["amount"];
            $result["description"] = $paymentData["description"];
            $result["payment_type"] = $paymentData["payment_type"];
            $result["bank"] = $paymentData["bank"];
            $result["cheque_no"] = $paymentData["cheque_no"];
            $result["sgst"] = $paymentData["sgst"];
            $result["sgst_amount"] = $paymentData["sgst_amount"];
            $result["cgst_amount"] = $paymentData["cgst_amount"];
            $result["cgst"] = $paymentData["cgst"];
            $result["igst"] = $paymentData["igst"];
            $result["igst_amount"] = $paymentData["igst_amount"];
            $result["tax_amount"] = $paymentData["tax_amount"];
            $result["permission"] = $paymentData["approve_status"];
            $result["company"] = $paymentData["company_id"];
            $result["uniqueid"] = "";
            $result["payment_quotation_status"] = $paymentData["payment_quotation_status"];
            $result["quotation_number"] = $paymentData["quotation_number"];
            $final_result = !empty($paymentData) ? $paymentData : array();
        }
        $result['final_result'] = $final_result;
        echo json_encode($result);
    }

    public function actionGetTotalFromPayment()
    {
        $subcontractor_id = $_REQUEST['subcontractor_id'];
        $project_id = $_REQUEST['project_id'];
        $amount = $_REQUEST['amount'];
        $company_id = $_REQUEST['company_id'];
        $id = $_REQUEST['id'];
        $tblpx = Yii::app()->db->tablePrefix;
        $company = Company::model()->findByPk($company_id);
        $paymentData = Yii::app()->db->createCommand("SELECT SUM(IFNULL(amount, 0) +IFNULL(tax_amount, 0))as totalsum FROM " . $tblpx . "subcontractor_payment WHERE subcontractor_id = " . $subcontractor_id . " AND project_id =" . $project_id . "")->queryRow();
        if (isset($id) && $id != '') {
            $currentdata = Yii::app()->db->createCommand("SELECT SUM(IFNULL(amount, 0) +IFNULL(tax_amount, 0)) as amount FROM " . $tblpx . "subcontractor_payment WHERE payment_id=" . $id . " AND subcontractor_id = " . $subcontractor_id . " AND project_id =" . $project_id . "")->queryRow();
            $pay_amount = ($paymentData['totalsum'] + $amount) - $currentdata['amount'];
        } else {
            $pay_amount = ($paymentData['totalsum'] + $amount);
        }
        //$subcontractorData    = Yii::app()->db->createCommand("SELECT SUM(scquotation_amount)as totalsum FROM " .$tblpx."scquotation WHERE subcontractor_id = ".$subcontractor_id." AND project_id =".$project_id."")->queryRow();
        $subcontractorData = Yii::app()->db->createCommand("SELECT SUM( IFNULL(" . $tblpx . "scquotation_items.item_amount,0)) as totalsum FROM " . $tblpx . "scquotation LEFT JOIN " . $tblpx . "scquotation_items ON " . $tblpx . "scquotation.scquotation_id = " . $tblpx . "scquotation_items.scquotation_id WHERE " . $tblpx . "scquotation.project_id= " . $project_id . " AND " . $tblpx . "scquotation.subcontractor_id=" . $subcontractor_id . " AND " . $tblpx . "scquotation_items.approve_status = 'Yes'")->queryRow();
        $total_amount = ($subcontractorData['totalsum'] / 100) * $company['subcontractor_limit'];
        if (($subcontractorData['totalsum'] != 0)) {
            //                    $subcontractorpermission = SubcontractorPermission::model()->findAll(array('condition' => 'subcontractor_id='.$subcontractor_id.' AND project_id ='.$project_id.''));
            //                    if(!empty($subcontractorpermission)) {
            //
            //                        if($pay_amount <= $subcontractorData['totalsum']){
            //				$result = 1;
            //			} else {
            //				$result = 3;
            //			}
            //                    }else{
            if ($pay_amount <= $subcontractorData['totalsum']) {
                if ($pay_amount <= $total_amount) {
                    $result = 1;
                } else {
                    $result = 0;
                }
            } else {
                $result = 3;
            }

            //}
        } else {
            $result = 2;
        }
        echo json_encode(array('status' => $result, 'limit' => $company->subcontractor_limit));
    }

    public function actionDailyEntries()
    {
        $model = new SubcontractorPayment;
        $tblpx = Yii::app()->db->tablePrefix;
        //$model->unsetAttributes();
        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        $newQuery = "";
        foreach ($arrVal as $arr) {
            if ($newQuery)
                $newQuery .= ' OR';
            $newQuery .= " FIND_IN_SET('" . $arr . "', p.company_id)";
        }
        $currDate = date('Y-m-d', strtotime(date("Y-m-d")));
        $expenseData = Yii::app()->db->createCommand("SELECT p.*,p.created_date as created, s.*,pr.*,ba.*,st.*,p.description as payment_description,p.company_id as company_id FROM " . $tblpx . "subcontractor_payment  p
                        INNER JOIN " . $tblpx . "subcontractor s ON s.subcontractor_id = p.subcontractor_id
                        LEFT JOIN " . $tblpx . "projects pr ON pr.pid = p.project_id
                        LEFT JOIN " . $tblpx . "bank ba ON p.bank = ba.bank_id
                        LEFT JOIN " . $tblpx . "status st ON p.payment_type = st.sid
                        WHERE p.date = '" . $currDate . "' AND (" . $newQuery . ")")->queryAll();
        $this->render('dailyentries', array(
            'model' => $model,
            'newmodel' => $expenseData
        ));
    }

    public function actionAddDailyentries()
    {
        $model = new SubcontractorPayment;
        $newmodel = new Expenses;
        $reconmodel = new Reconciliation;
        $tblpx = Yii::app()->db->tablePrefix;

        // expense alert mail
        $allocated_budget = array();
        $project_model = Projects::model()->findByPk($_GET['SubcontractorPayment']['project_id']);
        $company_model = Company::model()->findByPk($_GET['SubcontractorPayment']['company_id']);
        $project_id = $_GET['SubcontractorPayment']['project_id'];
        $expense_amount = Yii::app()->db->createCommand("SELECT SUM(amount) as amount FROM " . $tblpx . "expenses WHERE projectid = " . $project_id . " AND subcontractor_id IS NULL")->queryRow();
        $sub_amount = Yii::app()->db->createCommand("SELECT SUM( IFNULL(amount, 0) + IFNULL(sgst_amount, 0) + IFNULL(cgst_amount, 0) + IFNULL(igst_amount, 0)) as amount FROM " . $tblpx . "subcontractor_payment WHERE project_id = " . $project_id . " AND approve_status ='Yes'")->queryRow();
        $vendor_amount = Yii::app()->db->createCommand("SELECT SUM( IFNULL(amount, 0) + IFNULL(sgst_amount, 0) + IFNULL(cgst_amount, 0) + IFNULL(igst_amount, 0)) as amount FROM " . $tblpx . "dailyvendors WHERE project_id = " . $project_id . " AND project_id IS NOT NULL")->queryRow();

        if ($project_model->profit_margin != NULL && $project_model->project_quote != NULL && $company_model->expenses_percentage != NULL && $company_model->expenses_email != NULL) {
            $project_expense = (100 - ($project_model->profit_margin)) * (($project_model->project_quote) / 100);
            $amount = ($_GET['SubcontractorPayment']['sgst_amount'] + $_GET['SubcontractorPayment']['cgst_amount'] + $_GET['SubcontractorPayment']['igst_amount']) + $_GET['SubcontractorPayment']['amount'] + $expense_amount['amount'] + $sub_amount['amount'] + $vendor_amount['amount'];
            $expenses_percentage = explode(",", $company_model->expenses_percentage);
            $percentage_array = array();
            foreach ($expenses_percentage as $key => $percentage) {
                $final_amount = ($project_expense * $percentage / 100);
                if ($amount >= $final_amount) {
                    $percentage_array[] = $percentage;
                }
            }
            if (!empty($percentage_array)) {
                $percentage = max($percentage_array);
                if ($project_model->expense_percentage != $percentage) {
                    $allocated_budget['payment_alert'] = array('percentage' => $percentage, 'amount' => $amount, 'profit_margin' => $project_model->profit_margin, 'project_quote' => $project_model->project_quote, 'project_id' => $_GET['SubcontractorPayment']['project_id']);
                    $this->savePaymentAlert($allocated_budget['payment_alert']);
                }
                $amt_value = ($_GET['SubcontractorPayment']['sgst_amount'] + $_GET['SubcontractorPayment']['cgst_amount'] + $_GET['SubcontractorPayment']['igst_amount']) + $_GET['SubcontractorPayment']['amount'];
                $expense_alert = $this->saveExpenseNotifications($project_model, $percentage_array, $amt_value);
                if (!empty($expense_alert)) {
                    $allocated_budget['expense_advance_alert'] = $expense_alert;
                }
            } else {
                $budget_percentage = NULL;
            }
            $update2 = Yii::app()->db->createCommand()->update($tblpx . 'projects', array('expense_amount' => $amount), 'pid=:pid', array(':pid' => $project_id));
        }

        if (isset($_GET['SubcontractorPayment'])) {
            $model->attributes = $_GET['SubcontractorPayment'];
            $model->date = date('Y-m-d', strtotime($_GET['SubcontractorPayment']['date']));
            $model->created_by = Yii::app()->user->id;
            $model->created_date = date('Y-m-d');
            $model->updated_date = date('Y-m-d');
            $model->reconciliation_status = 0;
            $model->approve_status = $_GET['SubcontractorPayment']['approve_status'];
            $model->subcontractor_id = $_GET['SubcontractorPayment']['subcontractor_id'];

            if ($model->approve_status == 'No') {
                $this->sendmail($model->subcontractor_id, $model->project_id, $model->amount, $model->company_id);
            }
            if ($model->save()) {
                $last_id = $model->payment_id;
                Yii::app()->user->setFlash('success', "Payment added successfully! Id is #" . $last_id);

                if ($model->approve_status == 'Yes') {
                    $data = $model->attributes;
                    $newmodel->setAttributes($data);

                    if ($model->payment_type == 88) {
                        $newmodel->bank_id = $model->bank;
                        $reconmodel->reconciliation_table = $tblpx . "expenses";
                        $reconmodel->reconciliation_paymentdate = date('Y-m-d', strtotime($model->date));
                        $reconmodel->reconciliation_bank = $model->bank;
                        $reconmodel->reconciliation_chequeno = $model->cheque_no;
                        $reconmodel->created_date = date("Y-m-d H:i:s");
                        $reconmodel->reconciliation_status = 0;
                        $reconmodel->company_id = $model->company_id;
                    }
                    $expense_totalamount = ($model->sgst_amount + $model->cgst_amount + $model->igst_amount) + $model->amount;

                    $newmodel->bank_id = NULL;
                    $newmodel->expense_sgstp = $model->sgst;
                    $newmodel->expense_sgst = $model->sgst_amount;
                    $newmodel->expense_cgstp = $model->cgst;
                    $newmodel->expense_cgst = $model->cgst_amount;
                    $newmodel->expense_igstp = $model->igst;
                    $newmodel->expense_igst = $model->igst_amount;
                    $newmodel->amount = $expense_totalamount;
                    $newmodel->type = 73;
                    $newmodel->paid = $model->amount + $model->tax_amount;
                    $newmodel->userid = Yii::app()->user->id;
                    $newmodel->approval_status = $model->approve_status;

                    $reconmodel->reconciliation_payment = "Subcontractor Payment";
                    $reconmodel->reconciliation_amount = $expense_totalamount;

                    if ($newmodel->save()) {
                        $lastInsExpId = Yii::app()->db->getLastInsertID();
                        if (!empty($allocated_budget)) {
                            $this->projectexpensealert($allocated_budget, $last_id, 'daybook');
                        }

                        $reconmodel->reconciliation_parentid = $lastInsExpId;
                        if ($model->payment_type == 88) {
                            $reconmodel->save();
                        }
                    }
                }

                $payDate = date('Y-m-d', strtotime($model->date));
                $user = Users::model()->findByPk(Yii::app()->user->id);
                $arrVal = explode(',', $user->company_id);
                $newQuery = "";
                foreach ($arrVal as $arr) {
                    if ($newQuery)
                        $newQuery .= ' OR';
                    $newQuery .= " FIND_IN_SET('" . $arr . "', p.company_id)";
                }
                $expenseData = Yii::app()->db->createCommand("SELECT p.*,p.created_date as created, s.*,pr.*,ba.*,st.*,p.description as payment_description,p.company_id as company_id FROM " . $tblpx . "subcontractor_payment  p
                        LEFT JOIN " . $tblpx . "subcontractor s ON s.subcontractor_id = p.subcontractor_id
                        LEFT JOIN " . $tblpx . "projects pr ON pr.pid = p.project_id
                        LEFT JOIN " . $tblpx . "bank ba ON p.bank = ba.bank_id
                        LEFT JOIN " . $tblpx . "status st ON p.payment_type = st.sid
                        WHERE p.date = '" . $payDate . "' AND (" . $newQuery . ")")->queryAll();
                $client = $this->renderPartial('dailyentries_list', array('newmodel' => $expenseData));
                return $client;
            } else {
                echo 1;
            }
        }
    }

    public function actionUpdateDailyentries()
    {
        if (isset($_GET['SubcontractorPayment'])) {
            $model = new SubcontractorPayment;
            $newmodel = new Expenses;
            $model_data = SubcontractorPayment::model()->findByPk($_GET['SubcontractorPayment']['txtPaymentId']);
            $created_by = $model_data->created_by;
            $created_date = $model_data->created_date;
            $budget_percentage = $model_data->budget_percentage;
            $id = $_GET['SubcontractorPayment']['txtPaymentId'];

            $reconmodel = new Reconciliation;
            $tblpx = Yii::app()->db->tablePrefix;

            // expense alert mail
            $allocated_budget = array();
            $project_model = Projects::model()->findByPk($_GET['SubcontractorPayment']['project_id']);
            $company_model = Company::model()->findByPk($_GET['SubcontractorPayment']['company_id']);
            $project_id = $_GET['SubcontractorPayment']['project_id'];
            //                                $expense_amount = Yii::app()->db->createCommand("SELECT SUM(amount) as amount FROM ".$tblpx."expenses WHERE projectid = ".$project_id." AND subcontractor_id IS NULL AND company_id=".Yii::app()->user->company_id."")->queryRow();
            //                                $sub_amount = Yii::app()->db->createCommand("SELECT SUM( IFNULL(amount, 0) + IFNULL(sgst_amount, 0) + IFNULL(cgst_amount, 0) + IFNULL(igst_amount, 0)) as amount FROM ".$tblpx."subcontractor_payment WHERE project_id = ".$project_id." AND approve_status ='Yes' AND company_id=".Yii::app()->user->company_id." AND payment_id !=".$model_data->payment_id."")->queryRow();
            //                                $vendor_amount = Yii::app()->db->createCommand("SELECT SUM( IFNULL(amount, 0) + IFNULL(sgst_amount, 0) + IFNULL(cgst_amount, 0) + IFNULL(igst_amount, 0)) as amount FROM ".$tblpx."dailyvendors WHERE project_id = ".$project_id." AND project_id IS NOT NULL AND company_id=".Yii::app()->user->company_id."")->queryRow();

            $expense_amount = Yii::app()->db->createCommand("SELECT SUM(amount) as amount FROM " . $tblpx . "expenses WHERE projectid = " . $project_id . " AND subcontractor_id IS NULL")->queryRow();
            $sub_amount = Yii::app()->db->createCommand("SELECT SUM( IFNULL(amount, 0) + IFNULL(sgst_amount, 0) + IFNULL(cgst_amount, 0) + IFNULL(igst_amount, 0)) as amount FROM " . $tblpx . "subcontractor_payment WHERE project_id = " . $project_id . " AND approve_status ='Yes' AND payment_id !=" . $model_data->payment_id . "")->queryRow();
            $vendor_amount = Yii::app()->db->createCommand("SELECT SUM( IFNULL(amount, 0) + IFNULL(sgst_amount, 0) + IFNULL(cgst_amount, 0) + IFNULL(igst_amount, 0)) as amount FROM " . $tblpx . "dailyvendors WHERE project_id = " . $project_id . " AND project_id IS NOT NULL")->queryRow();


            if ($project_model->profit_margin != NULL && $project_model->project_quote != NULL && $company_model->expenses_percentage != NULL && $company_model->expenses_email != NULL) {
                $project_expense = (100 - ($project_model->profit_margin)) * (($project_model->project_quote) / 100);
                $amount = ($_GET['SubcontractorPayment']['sgst_amount'] + $_GET['SubcontractorPayment']['cgst_amount'] + $_GET['SubcontractorPayment']['igst_amount']) + $_GET['SubcontractorPayment']['amount'] + $expense_amount['amount'] + $sub_amount['amount'] + $vendor_amount['amount'];
                $expenses_percentage = explode(",", $company_model->expenses_percentage);
                $percentage_array = array();
                foreach ($expenses_percentage as $key => $percentage) {
                    $final_amount = ($project_expense * $percentage / 100);
                    if ($amount >= $final_amount) {
                        $percentage_array[] = $percentage;
                    }
                }
                if (!empty($percentage_array)) {
                    $percentage = max($percentage_array);
                    if ($project_model->expense_percentage != $percentage) {
                        $allocated_budget['payment_alert'] = array('percentage' => $percentage, 'amount' => $amount, 'profit_margin' => $project_model->profit_margin, 'project_quote' => $project_model->project_quote, 'project_id' => $_GET['SubcontractorPayment']['project_id']);
                        $this->savePaymentAlert($allocated_budget['payment_alert']);
                    }
                    $amt_value = ($_GET['SubcontractorPayment']['sgst_amount'] + $_GET['SubcontractorPayment']['cgst_amount'] + $_GET['SubcontractorPayment']['igst_amount']) + $_GET['SubcontractorPayment']['amount'];
                    $expense_alert = $this->saveExpenseNotifications($project_model, $percentage_array, $amt_value);
                    if (!empty($expense_alert)) {
                        $allocated_budget['expense_advance_alert'] = $expense_alert;
                    }
                } else {
                    $budget_percentage = NULL;
                }
                $update2 = Yii::app()->db->createCommand()->update($tblpx . 'projects', array('expense_amount' => $amount), 'pid=:pid', array(':pid' => $project_id));
            }

            if ($_GET['SubcontractorPayment']['payment_type'] == 88) {
                $getRecon = SubcontractorPayment::model()->find(array(
                    'select' => array('reconciliation_status,reconciliation_date'),
                    "condition" => "payment_id='$id'",
                ));
                if ($getRecon) {
                    $rDate = $getRecon['reconciliation_date'];
                    $rStatus = $getRecon['reconciliation_status'];
                } else {
                    $rDate = NULL;
                    $rStatus = 0;
                }
            } else {
                $rStatus = 0;
                $rDate = NULL;
            }
            $reconTabName = $tblpx . "expenses";

            $model->attributes = $_GET['SubcontractorPayment'];
            $model->date = date('Y-m-d', strtotime($_GET['SubcontractorPayment']['date']));
            $model->created_by = $created_by;
            $model->created_date = $created_date;
            $model->updated_by = Yii::app()->user->id;
            $model->updated_date = date('Y-m-d');
            $model->budget_percentage = $budget_percentage;
            $model->reconciliation_status = $rStatus;
            $model->reconciliation_date = $rDate;
            $model->approve_status = $_GET['SubcontractorPayment']['approve_status'];
            if ($_GET['SubcontractorPayment']['payment_type'] == 89) {
                $model->cheque_no = NULL;
                $model->bank = NULL;
            } else {
                $model->cheque_no = $_GET['SubcontractorPayment']['cheque_no'];
                $model->bank = $_GET['SubcontractorPayment']['bank'];
                $reconmodel->reconciliation_table = $tblpx . "expenses";
                $reconmodel->reconciliation_paymentdate = date('Y-m-d', strtotime($_GET['SubcontractorPayment']['date']));
                $reconmodel->reconciliation_bank = $_GET['SubcontractorPayment']['bank'];
                $reconmodel->reconciliation_chequeno = $_GET['SubcontractorPayment']['cheque_no'];
                $reconmodel->created_date = date("Y-m-d H:i:s");
                $reconmodel->reconciliation_status = $rStatus;
                $reconmodel->reconciliation_date = $rDate;
                $reconmodel->company_id = $_GET['SubcontractorPayment']['company_id'];
            }
            //$this->totalpaymetcalculation($_GET['SubcontractorPayment']['subcontractor_id'], $_GET['SubcontractorPayment']['project_id'],$_GET['SubcontractorPayment']['amount'],$_GET['SubcontractorPayment']['txtPaymentId']);
            if ($_GET['SubcontractorPayment']['approve_status'] == 'No') {
                $this->sendmail($_GET['SubcontractorPayment']['subcontractor_id'], $_GET['SubcontractorPayment']['project_id'], $_GET['SubcontractorPayment']['amount'], $_GET['SubcontractorPayment']['company_id']);
            }
            if ($model->save()) {
                $last_id = $model->payment_id;

                if (!empty($allocated_budget)) {
                    $this->projectexpensealert($allocated_budget, $last_id, 'subcontractor_payment');
                }

                if ($_GET['SubcontractorPayment']['approve_status'] == 'Yes') {
                    $newmodel->reconciliation_status = $rStatus;
                    $newmodel->reconciliation_date = $rDate;

                    $newmodel->projectid = $_GET['SubcontractorPayment']['project_id'];
                    $newmodel->subcontractor_id = $last_id;
                    $newmodel->expense_type = $_GET['SubcontractorPayment']['payment_type'];
                    if ($_GET['SubcontractorPayment']['payment_type'] == 89) {
                        $newmodel->cheque_no = NULL;
                        $newmodel->bank_id = NULL;
                    } else {
                        $newmodel->cheque_no = $_GET['SubcontractorPayment']['cheque_no'];
                        $newmodel->bank_id = $_GET['SubcontractorPayment']['bank'];
                    }
                    $expense_totalamount = ($_GET['SubcontractorPayment']['sgst_amount'] + $_GET['SubcontractorPayment']['cgst_amount'] + $_GET['SubcontractorPayment']['igst_amount']) + $_GET['SubcontractorPayment']['amount'];
                    $newmodel->expense_amount = $_GET['SubcontractorPayment']['amount'];
                    $newmodel->expense_sgstp = $_GET['SubcontractorPayment']['sgst'];
                    $newmodel->expense_sgst = $_GET['SubcontractorPayment']['sgst_amount'];
                    $newmodel->expense_cgstp = $_GET['SubcontractorPayment']['cgst'];
                    $newmodel->expense_cgst = $_GET['SubcontractorPayment']['cgst_amount'];
                    $newmodel->expense_igstp = $_GET['SubcontractorPayment']['igst'];
                    $newmodel->expense_igst = $_GET['SubcontractorPayment']['igst_amount'];
                    $newmodel->amount = $expense_totalamount;
                    $newmodel->description = $_GET['SubcontractorPayment']['description'];
                    $newmodel->date = date('Y-m-d', strtotime($_GET['SubcontractorPayment']['date']));
                    $newmodel->type = 73;
                    $newmodel->created_by = Yii::app()->user->id;
                    $newmodel->created_date = date('Y-m-d');
                    $newmodel->company_id = $_GET['SubcontractorPayment']['company_id'];
                    $newmodel->paid = $_GET['SubcontractorPayment']['amount'] + $_GET['SubcontractorPayment']['tax_amount'];
                    $newmodel->userid = Yii::app()->user->id;

                    $reconmodel->reconciliation_payment = "Subcontractor Payment";
                    $reconmodel->reconciliation_amount = $expense_totalamount;

                    if ($newmodel->save()) {
                        $lastInsExpId = Yii::app()->db->getLastInsertID();
                        $reconmodel->reconciliation_parentid = $lastInsExpId;
                        if ($_GET['SubcontractorPayment']['payment_type'] == 88) {
                            $reconmodel->save();
                        }
                    }
                }


                // insert log
                $newmodel = new JpLog('search');
                $newmodel->log_data = json_encode($model_data->attributes);
                $newmodel->log_action = 1;
                $newmodel->log_table = $tblpx . "subcontractor_payment";
                $newmodel->log_primary_key = $last_id;
                $newmodel->log_datetime = date('Y-m-d H:i:s');
                $newmodel->log_action_by = Yii::app()->user->id;
                $newmodel->company_id = $_GET['SubcontractorPayment']['company_id'];
                $newmodel->save();
                // delete dailyexpenses
                $expLoad = Expenses::model()->find(array(
                    'select' => array('exp_id'),
                    "condition" => "subcontractor_id='$id'",
                ));
                $expOldId = $expLoad["exp_id"];

                $value = SubcontractorPayment::model()->deleteAll(array("condition" => "payment_id=" . $id . ""));
                Expenses::model()->deleteAll(array("condition" => "subcontractor_id=" . $id . ""));
                Reconciliation::model()->deleteAll(array("condition" => "reconciliation_parentid=" . $expOldId . " AND reconciliation_table = '" . $reconTabName . "'"));
                // list data

                $payDate = date('Y-m-d', strtotime($_GET['SubcontractorPayment']['date']));
                $tblpx = Yii::app()->db->tablePrefix;
                $user = Users::model()->findByPk(Yii::app()->user->id);
                $arrVal = explode(',', $user->company_id);
                $newQuery = "";
                foreach ($arrVal as $arr) {
                    if ($newQuery)
                        $newQuery .= ' OR';
                    $newQuery .= " FIND_IN_SET('" . $arr . "', p.company_id)";
                }
                $expenseData = Yii::app()->db->createCommand("SELECT p.*, p.created_date as created, s.*,pr.*,ba.*,st.*,p.description as payment_description,p.company_id as company_id FROM " . $tblpx . "subcontractor_payment  p
                        LEFT JOIN " . $tblpx . "subcontractor s ON s.subcontractor_id = p.subcontractor_id
                        LEFT JOIN " . $tblpx . "projects pr ON pr.pid = p.project_id
                        LEFT JOIN " . $tblpx . "bank ba ON p.bank = ba.bank_id
                        LEFT JOIN " . $tblpx . "status st ON p.payment_type = st.sid
                        WHERE p.date = '" . $payDate . "' AND (" . $newQuery . ")")->queryAll();
                $client = $this->renderPartial('dailyentries_list', array('newmodel' => $expenseData));
                return $client;
            } else {
                echo 1;
            }
        }
    }

    public function actionGetDataByDate()
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $newDate = '';
        if (isset($_POST['date']) && !empty($_POST['date'])) {
            $newDate = date('Y-m-d', strtotime($_POST['date']));
        }
        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        $newQuery = "";
        foreach ($arrVal as $arr) {
            if ($newQuery)
                $newQuery .= ' OR';
            $newQuery .= " FIND_IN_SET('" . $arr . "', p.company_id)";
        }
        $expenseData = Yii::app()->db->createCommand("SELECT p.*,p.created_date as created,p.date as paid_date, s.*,pr.*,ba.*,st.*,p.description as payment_description,p.company_id as company_id FROM " . $tblpx . "subcontractor_payment  p
                        LEFT JOIN " . $tblpx . "subcontractor s ON s.subcontractor_id = p.subcontractor_id
                        LEFT JOIN " . $tblpx . "projects pr ON pr.pid = p.project_id
                        LEFT JOIN " . $tblpx . "bank ba ON p.bank = ba.bank_id
                        LEFT JOIN " . $tblpx . "status st ON p.payment_type = st.sid
                        WHERE p.date = '" . $newDate . "' AND (" . $newQuery . ")")->queryAll();
        $client = $this->renderPartial('dailyentries_list', array('newmodel' => $expenseData));
        return $client;
    }

    public function actionReport()
    {
        $date_from = !empty($_REQUEST['date_from']) ? $_REQUEST['date_from'] : "";
        $date_to = !empty($_REQUEST['date_to']) ? $_REQUEST['date_to'] : "";
        $company_id = !empty($_REQUEST['company_id']) ? $_REQUEST['company_id'] : '';
        $project_id = !empty($_REQUEST['project_id']) ? $_REQUEST['project_id'] : '';

        if (isset($_REQUEST['subcontractor_id']) && !empty($_REQUEST['subcontractor_id'])) {
            $user = Users::model()->findByPk(Yii::app()->user->id);
            $arrVal = explode(',', $user->company_id);
            $subcontractor_id = $_REQUEST['subcontractor_id'];
            $tblpx = Yii::app()->db->tablePrefix;
            $subsql = "SELECT * FROM {$tblpx}subcontractor "
                . " WHERE subcontractor_id = " . $subcontractor_id . "";
            $subcontractor = Yii::app()->db->createCommand($subsql)->queryRow();
            $name = $subcontractor['subcontractor_name'];
            $newQuery1 = "";
            $newQuery2 = "";

            $newQuery3 = "";
            $newQuery4 = "";
            $newQuery4_labor = "";
            $newQuery5 = "";
            $condition5 = "";
            $newQuery6 = "";
            $condition6 = "";
            $newQuery7 = "";
            $newQuery8 = "";
            $newQuery9 = "";
            $project3sql='';
            $project_array3='';
            if ($company_id == '') {
                $c = count($arrVal);
                $i = 1;
                foreach ($arrVal as $arr) {
                    if ($newQuery1)
                        $newQuery1 .= ' OR';
                    $newQuery1 .= " FIND_IN_SET('" . $arr . "', s.company_id)";

                    $newQuery3 .= " FIND_IN_SET('" . $arr . "', sp.company_id)";
                    if ($i != $c)
                        $newQuery3 .= "OR";
                    $i++;
                }
            } else {
                $newQuery1 .= " FIND_IN_SET('" . $company_id . "', s.company_id)";
                $newQuery3 .= " FIND_IN_SET('" . $company_id . "', sp.company_id)";

            }

            if ($project_id != '') {
                $newQuery2 = " AND s.project_id =" . $project_id . "";
                $newQuery4 = " AND sp.project_id =" . $project_id . "";
                $newQuery4_labor = " AND sp.projectid =" . $project_id . "";

            }

            if (!empty($date_from) && !empty($date_to)) {

                $newQuery2 .= " AND s.scquotation_date BETWEEN '" . date("Y-m-d", strtotime($date_from)) . "' AND '" . date("Y-m-d", strtotime($date_to)) . "'";
                $newQuery4 .= " AND sp.date BETWEEN '" . date("Y-m-d", strtotime($date_from)) . "' AND '" . date("Y-m-d", strtotime($date_to)) . "'";
                $newQuery4_labor .= " AND sp.date BETWEEN '" . date("Y-m-d", strtotime($date_from)) . "' AND '" . date("Y-m-d", strtotime($date_to)) . "'";
                $newQuery7 .= " AND {$tblpx}scquotation.scquotation_date BETWEEN '" . date("Y-m-d", strtotime($date_from)) . "' AND '" . date("Y-m-d", strtotime($date_to)) . "'";
                $newQuery8 .= " AND e.date BETWEEN '" . date("Y-m-d", strtotime($date_from)) . "' AND '" . date("Y-m-d", strtotime($date_to)) . "'";
                $newQuery9 .= " AND {$tblpx}subcontractor_payment.date BETWEEN '" . date("Y-m-d", strtotime($date_from)) . "' AND '" . date("Y-m-d", strtotime($date_to)) . "'";

            } else if (!empty($date_from) && empty($date_to)) {

                $newQuery2 .= " AND s.scquotation_date >= '" . date("Y-m-d", strtotime($date_from)) . "'";
                $newQuery4 .= " AND sp.date >= '" . date("Y-m-d", strtotime($date_from)) . "'";
                $newQuery4_labor .= " AND sp.date >= '" . date("Y-m-d", strtotime($date_from)) . "'";
                $newQuery7 .= " AND {$tblpx}scquotation.scquotation_date >= '" . date("Y-m-d", strtotime($date_from)) . "'";
                $newQuery8 .= " AND e.date >= '" . date("Y-m-d", strtotime($date_from)) . "'";
                $newQuery9 .= " AND {$tblpx}subcontractor_payment.date >= '" . date("Y-m-d", strtotime($date_from)) . "'";
            } else if (empty($date_from) && !empty($date_to)) {

                $newQuery2 .= " AND s.scquotation_date <= '" . date("Y-m-d", strtotime($date_to)) . "'";
                $newQuery4 .= " AND sp.date <= '" . date("Y-m-d", strtotime($date_to)) . "'";
                $newQuery4_labor .= " AND sp.date <= '" . date("Y-m-d", strtotime($date_to)) . "'";
                $newQuery7 .= " AND {$tblpx}scquotation.scquotation_date <= '" . date("Y-m-d", strtotime($date_to)) . "'";
                $newQuery8 .= " AND e.date <= '" . date("Y-m-d", strtotime($date_to)) . "'";
                $newQuery9 .= " AND {$tblpx}subcontractor_payment.date <= '" . date("Y-m-d", strtotime($date_to)) . "'";
            } else {

                $newQuery2 .= "";
                $newQuery4 .= "";
                $newQuery4_labor .= "";
                $newQuery8 = '';
                $newQuery9 = '';
            }

            // array creation

            $report_array = array();
            $report_array2 = array();
            $report_array3 = array();

            $projectsql = "SELECT * FROM `jp_projects`as p INNER JOIN jp_scquotation "
                . " as s ON p.pid = s.project_id WHERE (" . $newQuery1 . ") "
                . " AND s.subcontractor_id = " . $subcontractor_id . "  " . $newQuery2 . "  "
                . " GROUP BY p.pid";
            $project_array = Yii::app()->db->createCommand($projectsql)->queryAll();

            $project2sql = "SELECT * FROM `jp_projects` as p INNER JOIN jp_subcontractor_payment "
                . " as sp ON p.pid = sp.project_id WHERE (" . $newQuery3 . ") "
                . " AND sp.subcontractor_id = " . $subcontractor_id . " "
                . " AND sp.payment_quotation_status = 2 " . $newQuery4 . " "
                . " GROUP BY p.pid";
            $project_array2 = Yii::app()->db->createCommand($project2sql)->queryAll();
            if (empty($project_array2)) {
                $project2sql_labor_report = "SELECT p.name,sp.projectid as project_id,sp.subcontractor_id FROM `jp_projects` as p INNER JOIN jp_dailyreport "
                    . " as sp ON p.pid = sp.projectid WHERE (" . $newQuery3 . ") "
                    . " AND sp.subcontractor_id = " . $subcontractor_id . " "
                    . $newQuery4_labor . " "
                    . " GROUP BY p.pid";
                $project_array2 = Yii::app()->db->createCommand($project2sql_labor_report)->queryAll();

            }
            $i = 0;
            foreach ($project_array as $key => $value) {
                $report_array[]['project'] = $value['name'];

                $quotation1sql = "SELECT {$tblpx}scquotation.scquotation_amount as sc_amount , sum({$tblpx}scquotation_items.item_amount) as amount,"
                    . " {$tblpx}scquotation.project_id,{$tblpx}scquotation.scquotation_date,"
                    . " {$tblpx}subcontractor.subcontractor_id,{$tblpx}scquotation.scquotation_decription,"
                    . " {$tblpx}scquotation.scquotation_no,{$tblpx}scquotation.scquotation_id "
                    . " FROM {$tblpx}subcontractor INNER JOIN {$tblpx}scquotation "
                    . " ON {$tblpx}subcontractor.subcontractor_id = {$tblpx}scquotation.subcontractor_id inner join {$tblpx}scquotation_items  ON {$tblpx}scquotation.scquotation_id = {$tblpx}scquotation_items.scquotation_id "
                    . " LEFT JOIN {$tblpx}projects ON {$tblpx}scquotation.project_id = {$tblpx}projects.pid "
                    . " WHERE {$tblpx}subcontractor.subcontractor_id = " . $value['subcontractor_id'] . " "
                    . " AND {$tblpx}scquotation.project_id = " . $value['project_id'] . "  " . $newQuery7
                    . " GROUP BY {$tblpx}scquotation.scquotation_id ORDER BY {$tblpx}scquotation.scquotation_date ASC";
                $quotation = Yii::app()->db->createCommand($quotation1sql)->queryAll();
                //die($quotation1sql);
                $report_array[$i]['quotation'] = array();
                foreach ($quotation as $key2 => $value2) {

                    $payment_bills_sql = "SELECT IFNULL(SUM(sb.total_amount), 0) as total_bill_amount 
                              FROM {$tblpx}subcontractorbill sb 
                              WHERE sb.scquotation_id='" . $value2['scquotation_id'] . "'";
                    $payment_bills = Yii::app()->db->createCommand($payment_bills_sql)->queryRow();
                    $value2['total_bill_amount'] = $payment_bills['total_bill_amount'];
                    if (array_key_exists($value2['scquotation_date'], $report_array[$i]['quotation'])) {
                        array_push($report_array[$i]['quotation'][$value2['scquotation_date']], $value2);
                    } else {
                        $report_array[$i]['quotation'][$value2['scquotation_date']][] = $value2;
                    }
                }
                $i++;
            }

            $final_array = array();
            foreach ($report_array as $key => $values) {
                $final_array[]['project'] = $values['project'];
                if (!empty($values['quotation']) && !empty($values['payment'])) {
                    $final_array[$key]['details'] = array_merge_recursive($values['quotation'], $values['payment']);
                } else {
                    $final_array[$key]['details'] = $values['quotation'];
                }
            }
            $j = 0;


            foreach ($project_array2 as $key => $value) {
                $report_array2[]['project'] = $value['name'];
               
                $quotationsql = "SELECT sum({$tblpx}scquotation.scquotation_amount) "
                    . " as amount , {$tblpx}scquotation.project_id,{$tblpx}scquotation.scquotation_date,"
                    . " {$tblpx}subcontractor.subcontractor_id,{$tblpx}scquotation.scquotation_decription"
                    . " FROM {$tblpx}subcontractor INNER JOIN {$tblpx}scquotation "
                    . " ON {$tblpx}subcontractor.subcontractor_id = {$tblpx}scquotation.subcontractor_id "
                    . " LEFT JOIN {$tblpx}projects ON {$tblpx}scquotation.project_id = {$tblpx}projects.pid "
                    . " WHERE {$tblpx}subcontractor.subcontractor_id = " . $value['subcontractor_id'] . " "
                    . " AND {$tblpx}scquotation.project_id = " . $value['project_id'] . " "
                    . " GROUP BY {$tblpx}scquotation.project_id,{$tblpx}scquotation.scquotation_date "
                    . " ORDER BY {$tblpx}scquotation.scquotation_date ASC";
                $quotation = Yii::app()->db->createCommand($quotationsql)->queryAll();

                $resultsql = "SELECT sum(e.amount) as amount,e.date,e.description,e.dr_id "
                    . " FROM " . $tblpx . "dailyreport e  LEFT JOIN " . $tblpx . "projects p "
                    . " ON e.projectid = p.pid LEFT JOIN " . $tblpx . "subcontractor s "
                    . " ON e.subcontractor_id = s.subcontractor_id LEFT JOIN " . $tblpx . "expense_type exp "
                    . " ON exp.type_id = e.expensehead_id WHERE e.projectid = " . $value['project_id'] . " "
                    . " AND e.subcontractor_id = " . $value['subcontractor_id'] . " " . $newQuery8
                    . " AND e.approve_status = 1 AND  e.sc_quot_id IS  NULL  "
                    . " group by e.projectid,e.date "
                    . " ORDER BY e.date ASC";
                    //die($resultsql);
                $result = Yii::app()->db->createCommand($resultsql)->queryAll();
                
                $reconcil_status = "{$tblpx}subcontractor_payment.reconciliation_status = 0";
                $approve_status = 'No';
                if (isset($_REQUEST['custId']) && ($_REQUEST['custId'] == 'reconcil')) {
                    $reconcil_status = "({$tblpx}subcontractor_payment.reconciliation_status=1 OR {$tblpx}subcontractor_payment.reconciliation_status IS NULL)";
                    $approve_status = 'Yes';
                }


                $sql = "SELECT  sum({$tblpx}subcontractor_payment.paidamount)"
                    . " as amount,{$tblpx}subcontractor_payment.project_id,{$tblpx}subcontractor_payment.date,"
                    . " {$tblpx}subcontractor_payment.description,{$tblpx}subcontractor_payment.payment_id "
                    . " FROM {$tblpx}subcontractor_payment INNER JOIN {$tblpx}subcontractor "
                    . " ON {$tblpx}subcontractor_payment.subcontractor_id = {$tblpx}subcontractor.subcontractor_id "
                    . " LEFT JOIN {$tblpx}projects ON {$tblpx}subcontractor_payment.project_id = {$tblpx}projects.pid "
                    . "  WHERE {$tblpx}subcontractor.subcontractor_id = " . $value['subcontractor_id'] . " "
                    . " AND {$tblpx}subcontractor_payment.approve_status = '" . $approve_status . "' "
                    . " AND {$tblpx}subcontractor_payment.payment_quotation_status = 2 "
                    . " AND {$tblpx}subcontractor_payment.project_id = " . $value['project_id']
                    . " AND " . $reconcil_status . $newQuery9 . " GROUP BY {$tblpx}subcontractor_payment.project_id,"
                    . " {$tblpx}subcontractor_payment.date";

                $payment = Yii::app()->db->createCommand($sql)->queryAll();

                foreach ($result as $key2 => $value2) {
                    $report_array2[$j]['payment'][$value2['date']]['payment_amount'] = $value2['amount'];
                    $report_array2[$j]['payment'][$value2['date']]['payment_description'] = $value2['description'];
                    $report_array2[$j]['payment'][$value2['date']]['dr_id'] = $value2['dr_id'];
                }

                foreach ($payment as $key3 => $value3) {
                    $report_array2[$j]['advance'][$value3['date']]['advance_amount'] = $value3['amount'];
                    $report_array2[$j]['advance'][$value3['date']]['description'] = $value3['description'];
                    $report_array2[$j]['advance'][$value3['date']]['payment_id'] = $value3['payment_id'];
                }
                 
                 


                $j++;
            }
            //echo "<pre>";print_r($report_array2);exit;
            $final_array2 = array();
            foreach ($report_array2 as $key => $values) {
                $final_array2[]['project'] = $values['project'];
                if (!empty($values['advance']) && !empty($values['payment'])) {
                    $final_array2[$key]['details'] = array_merge_recursive($values['advance'], $values['payment']);
                } else if (!empty($values['payment'])) {
                    $final_array2[$key]['details'] = isset($values['advance']) ? $values['advance'] : $values['payment'];
                }else if (!empty($values['advance'])) {
                    $final_array2[$key]['details'] = isset($values['advance']) ? $values['advance'] : $values['payment'];
                }
            }
            $project3sql = "SELECT * FROM `jp_projects` as p INNER JOIN jp_subcontractor_payment "
                . " as sp ON p.pid = sp.project_id WHERE (" . $newQuery3 . ") "
                . " AND sp.subcontractor_id = " . $subcontractor_id . " "
                . " AND sp.payment_quotation_status = 1 
                    AND sp.stage_id IS NULL AND sp.sc_bill_id IS NULL " . $newQuery4 . " "
                . " GROUP BY p.pid";
            $project_array3 = Yii::app()->db->createCommand($project2sql)->queryAll();
            if (empty($project_array3)) {
                $project3sql_labor_report = "SELECT p.name,sp.projectid as project_id,sp.subcontractor_id FROM `jp_projects` as p INNER JOIN jp_dailyreport "
                    . " as sp ON p.pid = sp.projectid WHERE (" . $newQuery3 . ") "
                    . " AND sp.subcontractor_id = " . $subcontractor_id . " "
                    . $newQuery4_labor . " "
                    . " GROUP BY p.pid";
                $project_array3 = Yii::app()->db->createCommand($project3sql_labor_report)->queryAll();

            }
            $k=0;
            $report_array3=array();
            foreach ($project_array3 as $key => $value) {
                
                 $report_array3[]['project'] = $value['name'];
                
                 $result_scquot_sql = "SELECT sum(e.amount) as amount,e.date,e.description,e.dr_id ,e.sc_quot_id"
                    . " FROM " . $tblpx . "dailyreport e  LEFT JOIN " . $tblpx . "projects p "
                    . " ON e.projectid = p.pid LEFT JOIN " . $tblpx . "subcontractor s "
                    . " ON e.subcontractor_id = s.subcontractor_id LEFT JOIN " . $tblpx . "expense_type exp "
                    . " ON exp.type_id = e.expensehead_id WHERE e.projectid = " . $value['project_id'] . " "
                    . " AND e.subcontractor_id = " . $value['subcontractor_id'] . " " . $newQuery8
                    . " AND e.approve_status = 1 AND  e.sc_quot_id IS NOT NULL  "
                    . " group by e.projectid,e.date "
                    . " ORDER BY e.date ASC";
                    //die($resultsql);
                $result_quot = Yii::app()->db->createCommand($result_scquot_sql)->queryAll();
                $reconcil_status = "{$tblpx}subcontractor_payment.reconciliation_status = 0";
                $approve_status = 'No';
                if (isset($_REQUEST['custId']) && ($_REQUEST['custId'] == 'reconcil')) {
                    $reconcil_status = "({$tblpx}subcontractor_payment.reconciliation_status=1 OR {$tblpx}subcontractor_payment.reconciliation_status IS NULL)";
                    $approve_status = 'Yes';
                }


                $sql = "SELECT  sum({$tblpx}subcontractor_payment.paidamount)"
                    . " as amount,{$tblpx}subcontractor_payment.project_id,{$tblpx}subcontractor_payment.date,"
                    . " {$tblpx}subcontractor_payment.description,{$tblpx}subcontractor_payment.payment_id "
                    . " FROM {$tblpx}subcontractor_payment INNER JOIN {$tblpx}subcontractor "
                    . " ON {$tblpx}subcontractor_payment.subcontractor_id = {$tblpx}subcontractor.subcontractor_id "
                    . " LEFT JOIN {$tblpx}projects ON {$tblpx}subcontractor_payment.project_id = {$tblpx}projects.pid "
                    . "  WHERE {$tblpx}subcontractor.subcontractor_id = " . $value['subcontractor_id'] . " "
                    . " AND {$tblpx}subcontractor_payment.approve_status = '" . $approve_status . "' "
                    . " AND {$tblpx}subcontractor_payment.payment_quotation_status = 1
                    AND {$tblpx}subcontractor_payment.stage_id IS NULL AND {$tblpx}subcontractor_payment.sc_bill_id IS NULL  "
                    . " AND {$tblpx}subcontractor_payment.project_id = " . $value['project_id']
                    . " AND " . $reconcil_status . $newQuery9 . " GROUP BY {$tblpx}subcontractor_payment.project_id,"
                    . " {$tblpx}subcontractor_payment.date";

                $payment = Yii::app()->db->createCommand($sql)->queryAll();

               
                 
                 foreach ($result_quot as $key3 => $value3) {
                    $report_array3[$k]['payment'][$value3['date']]['payment_amount'] = $value3['amount'];
                    $report_array3[$k]['payment'][$value3['date']]['payment_description'] = $value3['description'];
                    $report_array3[$k]['payment'][$value3['date']]['dr_id'] = $value3['dr_id'];
                    $quot = Scquotation::Model()->findByPk($value3['sc_quot_id']);
                    $scquotation_id='';
                    if(!empty($quot)){
                         $scquotation_id=$quot["scquotation_no"];
                    }
                   
                    $report_array3[$k]['payment'][$value3['date']]['sc_quot_id'] = $scquotation_id;
                }

                foreach ($payment as $key4 => $value4) {
                    $report_array3[$k]['advance'][$value4['date']]['advance_amount'] = $value4['amount'];
                    $report_array3[$k]['advance'][$value4['date']]['description'] = $value4['description'];
                    $report_array3[$k]['advance'][$value4['date']]['payment_id'] = $value4['payment_id'];
                }


                $k++;
            }
             $final_array3 = array();
            foreach ($report_array3 as $key => $values) {
                $final_array3[]['project'] = $values['project'];
                if (!empty($values['advance']) && !empty($values['payment'])) {
                    $final_array3[$key]['details'] = array_merge_recursive($values['advance'], $values['payment']);
                } else if (!empty($values['payment'])) {
                    $final_array3[$key]['details'] = isset($values['advance']) ? $values['advance'] : $values['payment'];
                }
            }
            //echo "<pre>";print_r( $final_array3);exit;
            $final_array3 = array();
            foreach ($report_array3 as $key => $values) {
                $final_array3[]['project'] = $values['project'];
                if (!empty($values['advance']) && !empty($values['payment'])) {
                    $final_array3[$key]['details'] = array_merge_recursive($values['advance'], $values['payment']);
                } else if (!empty($values['payment'])) {
                    $final_array3[$key]['details'] = isset($values['advance']) ? $values['advance'] : $values['payment'];
                }
            }
           // echo "<pre>";print_r( $final_array3);exit;


            $unreconcilsql = "SELECT O.*, P.*, I.*,s.*   FROM jp_reconciliation O "
                . " JOIN jp_expenses I ON O.reconciliation_parentid = I.exp_id "
                . " JOIN jp_subcontractor_payment P ON P.payment_id = I.subcontractor_id "
                . " JOIN jp_subcontractor s on P.subcontractor_id = s.subcontractor_id "
                . " WHERE O.reconciliation_status = 0 "
                . " and P.subcontractor_id = '" . $_REQUEST['subcontractor_id'] . "' "
                . " and O.reconciliation_payment = 'Subcontractor Payment' "
                . " ORDER BY O.reconciliation_id";
            $unreconciled_data = Yii::app()->db->createCommand($unreconcilsql)->queryAll();
        } else {
            $subcontractor_id = 0;
            $name = '';
            $final_array = array();
            $final_array2 = array();
            $final_array3 = array();
            $unreconciled_data = array();
        }
        $model = new SubcontractorPayment();
        //echo "<pre>";print_r($final_array);exit;
        $render_datas = array(
            'subcontractor_id' => $subcontractor_id,
            'name' => $name,
            'company_id' => $company_id,
            'project_id' => $project_id,
            'final_array' => $final_array,
            'final_array2' => $final_array2,
            'final_array3' => $final_array3,
            'model' => $model,
            'unreconciled_data' => $unreconciled_data,
            'date_from' => $date_from,
            'date_to' => $date_to
        );
        if (filter_input(INPUT_GET, 'export') == 'pdf') {
            $this->logo = $this->realpath_logo;
            $mPDF1 = Yii::app()->ePdf->mPDF();
            $mPDF1 = Yii::app()->ePdf->mPDF('', 'A4');
            //$mPDF1 = Yii::app()->ePdf->mPDF('', 'A4-L');
            $sql = 'SELECT template_name '
                . 'FROM jp_quotation_template '
                . 'WHERE status="1"';
            $selectedtemplate = Yii::app()->db->createCommand($sql)->queryScalar();

            $template = $this->getTemplate($selectedtemplate);
            $mPDF1->SetHTMLHeader($template['header']);
            $mPDF1->SetHTMLFooter($template['footer']);
            $mPDF1->AddPage('', '', '', '', '', 5, 5, 50, 30, 10, 0);
            $mPDF1->SetAutoPageBreak(true, 15);
            // echo $this->renderPartial('paymentreport', $render_datas, true);die;
            $mPDF1->WriteHTML($this->renderPartial('paymentreport', $render_datas, true));

            $mPDF1->Output('SUB CONTRACTOR PAYMENTS.pdf', 'D');
        } elseif (filter_input(INPUT_GET, 'export') == 'csv') {
            $exported_data = $this->renderPartial('paymentreport', $render_datas, true);
            $export_filename = 'Subcontractor-Payment-Report' . date('Ymd_His');

            // ExportHelper::exportGridAsCSV($exported_data, $export_filename);
            require_once(Yii::app()->basePath . '/components/simple_html_dom.php');
            $html = str_get_html($exported_data); // give this your HTML string
            header('Content-type: application/ms-excel');
            header('Content-Disposition: attachment; filename=' . $export_filename . '.csv');

            $fp = fopen("php://output", "w");
            $contents = $html->find('tr');

            foreach ($contents as $element) {
                $td = array();
                foreach ($element->find('th') as $row) {
                    if (strpos(trim($row->class), 'actions') === false && strpos(trim($row->class), 'checker') === false) {
                        $td[] = str_replace('&nbsp;', '', $row->plaintext);
                    }
                }
                if (!empty($td)) {
                    fputcsv($fp, $td);
                }

                $td = array();
                foreach ($element->find('td') as $row) {
                    if (strpos(trim($row->class), 'actions') === false && strpos(trim($row->class), 'checker') === false) {
                        $td[] = str_replace('&nbsp;', '', $row->plaintext);
                    }
                }
                if (!empty($td)) {
                    fputcsv($fp, $td);
                }
            }

            fclose($fp);
            exit;
        } else {
            $this->render('paymentreport', $render_datas);
        }
    }

    public function actionSavetopdf1()
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $newQuery = "";
        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        foreach ($arrVal as $arr) {
            if ($newQuery)
                $newQuery .= ' OR';
            $newQuery .= " FIND_IN_SET('" . $arr . "', {$tblpx}subcontractor.company_id)";
        }
        $scquotation = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}subcontractor
         INNER JOIN {$tblpx}scquotation ON {$tblpx}subcontractor.subcontractor_id={$tblpx}scquotation.subcontractor_id 
         LEFT JOIN {$tblpx}projects ON {$tblpx}scquotation.project_id= {$tblpx}projects.pid WHERE (" . $newQuery . ") 
         GROUP BY {$tblpx}scquotation.subcontractor_id,{$tblpx}scquotation.project_id 
         ORDER BY {$tblpx}scquotation.scquotation_id")->queryAll();
        $mPDF1 = Yii::app()->ePdf->mPDF();
        # You can easily override default constructor's params
        $mPDF1 = Yii::app()->ePdf->mPDF('', 'A4');
        $mPDF1->WriteHTML($stylesheet, 1);
        //$mPDF1->SetHtmlHeader("Your header text here");
        //$mPDF1->SetHtmlFooter("Your footer text here");
        $mPDF1->WriteHTML($this->renderPartial('paymentreportpdf', array(
            'scquotation' => $scquotation
        ), true));
        $filename = "SUB CONTRACTOR PAYMENTS";
        $mPDF1->Output($filename . '.pdf', 'D');
    }

    public function actionSavetoexcel1()
    {

        $tblpx = Yii::app()->db->tablePrefix;
        $newQuery = "";
        $newQuery1 = "";
        $newQuery2 = "";
        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        foreach ($arrVal as $arr) {
            if ($newQuery)
                $newQuery .= ' OR';
            if ($newQuery1)
                $newQuery1 .= ' OR';
            if ($newQuery2)
                $newQuery2 .= ' OR';
            $newQuery .= " FIND_IN_SET('" . $arr . "', {$tblpx}subcontractor.company_id)";
            $newQuery1 .= " FIND_IN_SET('" . $arr . "', {$tblpx}scquotation.company_id)";
            $newQuery2 .= " FIND_IN_SET('" . $arr . "', {$tblpx}subcontractor_payment.company_id)";
        }
        $scquotation = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}subcontractor INNER JOIN {$tblpx}scquotation ON {$tblpx}subcontractor.subcontractor_id={$tblpx}scquotation.subcontractor_id LEFT JOIN {$tblpx}projects ON {$tblpx}scquotation.project_id= {$tblpx}projects.pid WHERE (" . $newQuery . ") GROUP BY {$tblpx}scquotation.subcontractor_id,{$tblpx}scquotation.project_id ORDER BY {$tblpx}scquotation.scquotation_id")->queryAll();

        $vendor_debit_data_array = 15;
        $final_amount = 0;
        foreach ($scquotation as $key => $data) {
            //$scquotation_amount = Yii::app()->db->createCommand("SELECT SUM(scquotation_amount) as total_amount FROM {$tblpx}scquotation WHERE subcontractor_id=".$data['subcontractor_id']." and project_id=".$data['project_id']."")->queryRow();
            $scquotation_amount = Yii::app()->db->createCommand("SELECT SUM( IFNULL(" . $tblpx . "scquotation_items.item_amount,0)) as total_amount FROM " . $tblpx . "scquotation LEFT JOIN " . $tblpx . "scquotation_items ON " . $tblpx . "scquotation.scquotation_id = " . $tblpx . "scquotation_items.scquotation_id WHERE " . $tblpx . "scquotation.project_id= " . $data['project_id'] . " AND " . $tblpx . "scquotation.subcontractor_id=" . $data['subcontractor_id'] . " AND " . $tblpx . "scquotation_items.approve_status = 'Yes' AND (" . $newQuery1 . ")")->queryRow();

            //    $payment_amount = Yii::app()->db->createCommand("SELECT SUM(IFNULL(amount, 0) +
            //     IFNULL(tax_amount, 0)) as total_amount FROM {$tblpx}subcontractor_payment INNER JOIN 
            //     {$tblpx}subcontractor ON {$tblpx}subcontractor_payment.subcontractor_id= 
            //     {$tblpx}subcontractor.subcontractor_id LEFT JOIN {$tblpx}projects ON {$tblpx}subcontractor_payment.project_id=
            //      {$tblpx}projects.pid WHERE {$tblpx}subcontractor_payment.subcontractor_id = " . $data['subcontractor_id'] . "
            //       AND {$tblpx}subcontractor_payment.project_id=" . $data['project_id'] . " 
            //       AND (".$newQuery2.")")->queryRow();

            $payment_amount = Yii::app()->db->createCommand("SELECT SUM(IFNULL(amount, 0) +
              IFNULL(tax_amount, 0)) as total_amount FROM {$tblpx}subcontractor_payment INNER 
              JOIN {$tblpx}subcontractor ON {$tblpx}subcontractor_payment.subcontractor_id= 
              {$tblpx}subcontractor.subcontractor_id LEFT JOIN {$tblpx}projects 
              ON {$tblpx}subcontractor_payment.project_id= {$tblpx}projects.pid 
              WHERE {$tblpx}subcontractor_payment.subcontractor_id = " . $data['subcontractor_id'] . "
               AND {$tblpx}subcontractor_payment.project_id=" . $data['project_id'] . " 
               AND {$tblpx}subcontractor_payment.approve_status ='Yes' AND (" . $newQuery2 . ") ")->queryRow();
            $balance = $scquotation_amount['total_amount'] - $payment_amount['total_amount'];
            $final_amount += $balance;
        }
        $arraylabel = array('sl no', 'Subcontractor', 'Project', 'Quotation Amount', 'Payment Done', 'Balance');

        $finaldata = array();
        $finaldata[0][] = '';
        $finaldata[0][] = '';
        $finaldata[0][] = '';
        $finaldata[0][] = '';
        $finaldata[0][] = 'Total';
        $finaldata[0][] = ($final_amount != '') ? Controller::money_format_inr($final_amount, 2, 1) : '0.00';
        $j = 0;
        $totalcredit = 0;
        $totaldebit = 0;
        $final_amount = 0;
        foreach ($scquotation as $key => $data) {
            $j++;
            //$scquotation_amount = Yii::app()->db->createCommand("SELECT SUM(scquotation_amount) as total_amount FROM {$tblpx}scquotation WHERE subcontractor_id=".$data['subcontractor_id']." and project_id=".$data['project_id']."")->queryRow();
            $scquotation_amount = Yii::app()->db->createCommand("SELECT SUM( IFNULL(" . $tblpx . "scquotation_items.item_amount,0)) as total_amount FROM " . $tblpx . "scquotation LEFT JOIN " . $tblpx . "scquotation_items ON " . $tblpx . "scquotation.scquotation_id = " . $tblpx . "scquotation_items.scquotation_id WHERE " . $tblpx . "scquotation.project_id= " . $data['project_id'] . " AND " . $tblpx . "scquotation.subcontractor_id=" . $data['subcontractor_id'] . " AND " . $tblpx . "scquotation_items.approve_status = 'Yes' AND (" . $newQuery1 . ")")->queryRow();
            $payment_amount = Yii::app()->db->createCommand("SELECT SUM(IFNULL(amount, 0) +IFNULL(tax_amount, 0)) as total_amount FROM {$tblpx}subcontractor_payment INNER JOIN {$tblpx}subcontractor ON {$tblpx}subcontractor_payment.subcontractor_id= {$tblpx}subcontractor.subcontractor_id LEFT JOIN {$tblpx}projects ON {$tblpx}subcontractor_payment.project_id= {$tblpx}projects.pid WHERE {$tblpx}subcontractor_payment.subcontractor_id = " . $data['subcontractor_id'] . " AND {$tblpx}subcontractor_payment.project_id=" . $data['project_id'] . " AND (" . $newQuery2 . ")")->queryRow();
            $balance = $scquotation_amount['total_amount'] - $payment_amount['total_amount'];
            $final_amount += $balance;
            $finaldata[$key + 1][] = $j;
            $finaldata[$key + 1][] = $data['subcontractor_name'];
            $finaldata[$key + 1][] = $data['name'];
            $finaldata[$key + 1][] = ($scquotation_amount['total_amount'] != '') ? Controller::money_format_inr($scquotation_amount['total_amount'], 2, 1) : '0.00';
            $finaldata[$key + 1][] = ($payment_amount['total_amount'] != '') ? Controller::money_format_inr($payment_amount['total_amount'], 2, 1) : '0.00';
            $finaldata[$key + 1][] = ($balance != '') ? Controller::money_format_inr($balance, 2, 1) : '0.00';
        }

        $finaldata[$key + 1][] = '';
        $finaldata[$key + 1][] = '';
        $finaldata[$key + 1][] = '';
        $finaldata[$key + 1][] = '';
        $finaldata[$key + 1][] = 'Total';
        $finaldata[$key + 1][] = ($final_amount != '') ? Controller::money_format_inr($final_amount, 2, 1) : '0.00';


        Yii::import('ext.ECSVExport');

        $csv = new ECSVExport($finaldata);

        $csv->setHeaders($arraylabel);
        $csv->setToAppend();
        $contentdata = $csv->toCSV();
        $csvfilename = 'subcontractor_payments' . date('d_M_Y') . '.csv';
        Yii::app()->getRequest()->sendFile($csvfilename, $contentdata, "text/csv", false);
        exit();
    }

    public function actionsavePaymentreport($subcontractor_id, $company_id, $project_id)
    {
        $mPDF1 = Yii::app()->ePdf->mPDF();
        $tblpx = Yii::app()->db->tablePrefix;
        $sql = "SELECT * FROM {$tblpx}subcontractor WHERE subcontractor_id = " . $subcontractor_id . "";
        $subcontractor = Yii::app()->db->createCommand($sql)->queryRow();
        $name = $subcontractor['subcontractor_name'];

        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        if (!empty($company_id)) {
            $company_id = $company_id;
        } else {
            $company_id = '';
        }
        if (!empty($project_id)) {
            $project_id = $project_id;
        } else {
            $project_id = '';
        }
        $newQuery1 = "";
        $newQuery2 = "";
        $newQuery3 = "";
        $newQuery4 = "";
        if ($company_id == '') {
            foreach ($arrVal as $arr) {
                if ($newQuery1 != '') {
                    $newQuery1 .= ' OR';
                }
                if ($newQuery3 != '') {
                    $newQuery3 .= ' OR ';
                }

                $newQuery1 .= " FIND_IN_SET('" . $arr . "', s.company_id)";
                $newQuery3 .= " FIND_IN_SET('" . $arr . "', sp.company_id)";
            }
        } else {
            $newQuery1 .= " FIND_IN_SET('" . $company_id . "', s.company_id)";
            $newQuery3 .= " FIND_IN_SET('" . $company_id . "', sp.company_id)";
        }

        if ($project_id != '') {
            $newQuery2 = " AND s.project_id =" . $project_id . "";
            $newQuery4 = " AND sp.project_id =" . $project_id . "";
        }

        // array creation

        $report_array = array();
        $report_array2 = array();
        $sql = "SELECT * FROM `jp_projects`as p "
            . "INNER JOIN jp_scquotation as s ON p.pid=s.project_id "
            . "WHERE (" . $newQuery1 . ") "
            . "AND s.subcontractor_id = " . $subcontractor_id . " " . $newQuery2
            . "  GROUP BY p.pid";

        $project_array = Yii::app()->db->createCommand($sql)->queryAll();
        $sql = "SELECT * FROM `jp_projects` as p INNER JOIN "
            . "jp_subcontractor_payment as sp ON p.pid=sp.project_id "
            . "WHERE (" . $newQuery3 . ") "
            . "AND sp.subcontractor_id=" . $subcontractor_id
            . " AND sp.payment_quotation_status=2 " . $newQuery4
            . " GROUP BY p.pid";

        $project_array2 = Yii::app()->db->createCommand($sql)->queryAll();

        $i = 0;
        foreach ($project_array as $key => $value) {
            $report_array[]['project'] = $value['name'];
            $quotation = Yii::app()->db->createCommand("SELECT sum({$tblpx}scquotation.scquotation_amount) as amount , {$tblpx}scquotation.project_id,{$tblpx}scquotation.scquotation_date,{$tblpx}subcontractor.subcontractor_id,{$tblpx}scquotation.scquotation_decription FROM {$tblpx}subcontractor INNER JOIN {$tblpx}scquotation ON {$tblpx}subcontractor.subcontractor_id= {$tblpx}scquotation.subcontractor_id LEFT JOIN {$tblpx}projects ON {$tblpx}scquotation.project_id= {$tblpx}projects.pid WHERE {$tblpx}subcontractor.subcontractor_id=" . $value['subcontractor_id'] . " AND {$tblpx}scquotation.project_id = " . $value['project_id'] . " GROUP BY {$tblpx}scquotation.project_id,{$tblpx}scquotation.scquotation_date ORDER BY {$tblpx}scquotation.scquotation_date ASC")->queryAll();
            $payment = Yii::app()->db->createCommand("SELECT  sum({$tblpx}subcontractor_payment.paidamount)as amount,{$tblpx}subcontractor_payment.project_id,{$tblpx}subcontractor_payment.date FROM {$tblpx}subcontractor_payment INNER JOIN {$tblpx}subcontractor ON {$tblpx}subcontractor_payment.subcontractor_id={$tblpx}subcontractor.subcontractor_id LEFT JOIN {$tblpx}projects ON {$tblpx}subcontractor_payment.project_id={$tblpx}projects.pid WHERE {$tblpx}subcontractor.subcontractor_id=" . $value['subcontractor_id'] . " AND {$tblpx}subcontractor_payment.approve_status='Yes' AND {$tblpx}subcontractor_payment.project_id = " . $value['project_id'] . " GROUP BY {$tblpx}subcontractor_payment.project_id, {$tblpx}subcontractor_payment.date")->queryAll();
            foreach ($quotation as $key2 => $value2) {
                $report_array[$i]['quotation'][$value2['scquotation_date']]['quotation_amount'] = $value2['amount'];
                $report_array[$i]['quotation'][$value2['scquotation_date']]['description'] = $value2['scquotation_decription'];
            }
            foreach ($payment as $key3 => $value3) {
                $report_array[$i]['payment'][$value3['date']]['payment_amount'] = $value3['amount'];
            }
            $i++;
        }
        $final_array = array();
        foreach ($report_array as $key => $values) {
            $final_array[]['project'] = $values['project'];
            if (!empty($values['quotation']) && !empty($values['payment'])) {
                $final_array[$key]['details'] = array_merge_recursive($values['quotation'], $values['payment']);
            } else {
                $final_array[$key]['details'] = $values['quotation'];
            }
        }

        $j = 0;
        foreach ($project_array2 as $key => $value) {
            $report_array2[]['project'] = $value['name'];
            $quotation = Yii::app()->db->createCommand("SELECT sum({$tblpx}scquotation.scquotation_amount) as amount , {$tblpx}scquotation.project_id,{$tblpx}scquotation.scquotation_date,{$tblpx}subcontractor.subcontractor_id,{$tblpx}scquotation.scquotation_decription FROM {$tblpx}subcontractor INNER JOIN {$tblpx}scquotation ON {$tblpx}subcontractor.subcontractor_id= {$tblpx}scquotation.subcontractor_id LEFT JOIN {$tblpx}projects ON {$tblpx}scquotation.project_id= {$tblpx}projects.pid WHERE {$tblpx}subcontractor.subcontractor_id=" . $value['subcontractor_id'] . " AND {$tblpx}scquotation.project_id = " . $value['project_id'] . " GROUP BY {$tblpx}scquotation.project_id,{$tblpx}scquotation.scquotation_date ORDER BY {$tblpx}scquotation.scquotation_date ASC")->queryAll();

            $result = Yii::app()->db->createCommand("SELECT sum(e.amount) as amount,e.date FROM " . $tblpx . "dailyreport e
            LEFT JOIN " . $tblpx . "projects p ON e.projectid = p.pid
            LEFT JOIN " . $tblpx . "subcontractor s ON e.subcontractor_id = s.subcontractor_id LEFT JOIN " . $tblpx . "expense_type exp on
            exp.type_id= e.expensehead_id
            WHERE e.projectid = " . $value['project_id'] . " AND e.subcontractor_id =" . $value['subcontractor_id'] . " AND e.approve_status =1  group by e.projectid,e.date ORDER BY e.date ASC")->queryAll();

            $payment = Yii::app()->db->createCommand("SELECT  sum({$tblpx}subcontractor_payment.paidamount)as amount,{$tblpx}subcontractor_payment.project_id,{$tblpx}subcontractor_payment.date FROM {$tblpx}subcontractor_payment INNER JOIN {$tblpx}subcontractor ON {$tblpx}subcontractor_payment.subcontractor_id={$tblpx}subcontractor.subcontractor_id LEFT JOIN {$tblpx}projects ON {$tblpx}subcontractor_payment.project_id={$tblpx}projects.pid WHERE {$tblpx}subcontractor.subcontractor_id=" . $value['subcontractor_id'] . " AND {$tblpx}subcontractor_payment.approve_status='Yes' AND {$tblpx}subcontractor_payment.payment_quotation_status=2 AND {$tblpx}subcontractor_payment.project_id = " . $value['project_id'] . " GROUP BY {$tblpx}subcontractor_payment.project_id, {$tblpx}subcontractor_payment.date")->queryAll();
            foreach ($result as $key2 => $value2) {
                $report_array2[$j]['payment'][$value2['date']]['payment_amount'] = $value2['amount'];
                $report_array2[$j]['payment'][$value2['date']]['description'] = "";
            }
            foreach ($payment as $key3 => $value3) {
                $report_array2[$j]['advance'][$value3['date']]['advance_amount'] = $value3['amount'];
            }
            $j++;
        }

        //print_r($report_array2);

        $final_array2 = array();
        foreach ($report_array2 as $key => $values) {
            $final_array2[]['project'] = $values['project'];
            if (!empty($values['advance']) && !empty($values['payment'])) {
                $final_array2[$key]['details'] = array_merge_recursive($values['advance'], $values['payment']);
            } else {
                $final_array2[$key]['details'] = $values['advance'];
            }
        }

        // print_r($this->renderPartial('paymentreportpdf_detail', array(
        //     'subcontractor_id' => $subcontractor_id,
        //     'subcontractor' => $name,
        //     'company_id' => $company_id,
        //     'project_id' => $project_id,
        //     'final_array' => $final_array,
        //     'final_array2' => $final_array2
        //         ), true));
        //         exit();
        # You can easily override default constructor's params
        $mPDF1 = Yii::app()->ePdf->mPDF('', 'A4');
        $mPDF1->WriteHTML($stylesheet, 1);

        // echo $this->renderPartial('paymentreportpdf_detail', array(
        //     'subcontractor_id' => $subcontractor_id,
        //     'subcontractor' => $name,
        //     'company_id' => $company_id,
        //     'project_id' => $project_id,
        //     'final_array' => $final_array,
        //         ), true);
        //         exit();
        //$mPDF1->SetHtmlHeader("Your header text here");
        //$mPDF1->SetHtmlFooter("Your footer text here");
        $mPDF1->WriteHTML($this->renderPartial('paymentreportpdf_detail', array(
            'subcontractor_id' => $subcontractor_id,
            'subcontractor' => $name,
            'company_id' => $company_id,
            'project_id' => $project_id,
            'final_array' => $final_array,
            'final_array2' => $final_array2
        ), true));
        $filename = "SUB CONTRACTOR PAYMENTS";
        $mPDF1->Output($filename . '.pdf', 'D');
    }

    public function actionSavetoexcel($subcontractor_id, $company_id, $project_id)
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        if (!empty($company_id)) {
            $company_id = $company_id;
        } else {
            $company_id = '';
        }
        if (!empty($project_id)) {
            $project_id = $project_id;
        } else {
            $project_id = '';
        }
        $newQuery1 = "";
        $newQuery2 = "";
        $newQuery3 = "";
        $newQuery4 = "";
        if ($company_id == '') {
            foreach ($arrVal as $arr) {
                if ($newQuery1)
                    $newQuery1 .= ' OR';
                $newQuery1 .= " FIND_IN_SET('" . $arr . "', s.company_id)";
                $newQuery3 .= " FIND_IN_SET('" . $arr . "', sp.company_id)";
            }
        } else {
            $newQuery1 .= " FIND_IN_SET('" . $company_id . "', s.company_id)";
            $newQuery3 .= " FIND_IN_SET('" . $company_id . "', sp.company_id)";
        }

        if ($project_id != '') {
            $newQuery2 = " AND s.project_id =" . $project_id . "";
            $newQuery4 = " AND sp.project_id =" . $project_id . "";
        }

        // array creation

        $report_array = array();

        $report_array2 = array();

        $project_array = Yii::app()->db->createCommand("SELECT * FROM `jp_projects`as p INNER JOIN jp_scquotation as s ON p.pid=s.project_id WHERE (" . $newQuery1 . ") AND s.subcontractor_id = " . $subcontractor_id . " " . $newQuery2 . "  GROUP BY p.pid")->queryAll();

        $project_array2 = Yii::app()->db->createCommand("SELECT * FROM `jp_projects` as p INNER JOIN jp_subcontractor_payment as sp ON p.pid=sp.project_id WHERE (" . $newQuery3 . ") AND sp.subcontractor_id=" . $subcontractor_id . " AND sp.payment_quotation_status=2 " . $newQuery4 . " GROUP BY p.pid")->queryAll();

        $i = 0;
        foreach ($project_array as $key => $value) {
            $report_array[]['project'] = $value['name'];
            $quotation = Yii::app()->db->createCommand("SELECT sum({$tblpx}scquotation.scquotation_amount) as amount , {$tblpx}scquotation.project_id,{$tblpx}scquotation.scquotation_date,{$tblpx}subcontractor.subcontractor_id,{$tblpx}scquotation.scquotation_decription FROM {$tblpx}subcontractor INNER JOIN {$tblpx}scquotation ON {$tblpx}subcontractor.subcontractor_id= {$tblpx}scquotation.subcontractor_id LEFT JOIN {$tblpx}projects ON {$tblpx}scquotation.project_id= {$tblpx}projects.pid WHERE {$tblpx}subcontractor.subcontractor_id=" . $value['subcontractor_id'] . " AND {$tblpx}scquotation.project_id = " . $value['project_id'] . " GROUP BY {$tblpx}scquotation.project_id,{$tblpx}scquotation.scquotation_date ORDER BY {$tblpx}scquotation.scquotation_date ASC")->queryAll();
            $payment = Yii::app()->db->createCommand("SELECT  sum({$tblpx}subcontractor_payment.paidamount)as amount,{$tblpx}subcontractor_payment.project_id,{$tblpx}subcontractor_payment.date FROM {$tblpx}subcontractor_payment INNER JOIN {$tblpx}subcontractor ON {$tblpx}subcontractor_payment.subcontractor_id={$tblpx}subcontractor.subcontractor_id LEFT JOIN {$tblpx}projects ON {$tblpx}subcontractor_payment.project_id={$tblpx}projects.pid WHERE {$tblpx}subcontractor.subcontractor_id=" . $value['subcontractor_id'] . " AND {$tblpx}subcontractor_payment.approve_status='Yes' AND {$tblpx}subcontractor_payment.project_id = " . $value['project_id'] . " GROUP BY {$tblpx}subcontractor_payment.project_id, {$tblpx}subcontractor_payment.date")->queryAll();
            foreach ($quotation as $key2 => $value2) {
                $report_array[$i]['quotation'][$value2['scquotation_date']]['quotation_amount'] = $value2['amount'];
                $report_array[$i]['quotation'][$value2['scquotation_date']]['description'] = $value2['scquotation_decription'];
            }
            foreach ($payment as $key3 => $value3) {
                $report_array[$i]['payment'][$value3['date']]['payment_amount'] = $value3['amount'];
            }
            $i++;
        }
        $final_array = array();
        foreach ($report_array as $key => $values) {
            $final_array[]['project'] = $values['project'];
            if (!empty($values['quotation']) && !empty($values['payment'])) {
                $final_array[$key]['details'] = array_merge_recursive($values['quotation'], $values['payment']);
            } else {
                $final_array[$key]['details'] = $values['quotation'];
            }
        }

        $j = 0;
        foreach ($project_array2 as $key => $value) {
            $report_array2[]['project'] = $value['name'];
            $quotation = Yii::app()->db->createCommand("SELECT sum({$tblpx}scquotation.scquotation_amount) as amount , {$tblpx}scquotation.project_id,{$tblpx}scquotation.scquotation_date,{$tblpx}subcontractor.subcontractor_id,{$tblpx}scquotation.scquotation_decription FROM {$tblpx}subcontractor INNER JOIN {$tblpx}scquotation ON {$tblpx}subcontractor.subcontractor_id= {$tblpx}scquotation.subcontractor_id LEFT JOIN {$tblpx}projects ON {$tblpx}scquotation.project_id= {$tblpx}projects.pid WHERE {$tblpx}subcontractor.subcontractor_id=" . $value['subcontractor_id'] . " AND {$tblpx}scquotation.project_id = " . $value['project_id'] . " GROUP BY {$tblpx}scquotation.project_id,{$tblpx}scquotation.scquotation_date ORDER BY {$tblpx}scquotation.scquotation_date ASC")->queryAll();

            $result = Yii::app()->db->createCommand("SELECT sum(e.amount) as amount,e.date FROM " . $tblpx . "dailyreport e
            LEFT JOIN " . $tblpx . "projects p ON e.projectid = p.pid
            LEFT JOIN " . $tblpx . "subcontractor s ON e.subcontractor_id = s.subcontractor_id LEFT JOIN " . $tblpx . "expense_type exp on
            exp.type_id= e.expensehead_id
            WHERE e.projectid = " . $value['project_id'] . " AND e.subcontractor_id =" . $value['subcontractor_id'] . " AND e.approve_status =1  group by e.projectid,e.date ORDER BY e.date ASC")->queryAll();

            $payment = Yii::app()->db->createCommand("SELECT  sum({$tblpx}subcontractor_payment.paidamount)as amount,{$tblpx}subcontractor_payment.project_id,{$tblpx}subcontractor_payment.date FROM {$tblpx}subcontractor_payment INNER JOIN {$tblpx}subcontractor ON {$tblpx}subcontractor_payment.subcontractor_id={$tblpx}subcontractor.subcontractor_id LEFT JOIN {$tblpx}projects ON {$tblpx}subcontractor_payment.project_id={$tblpx}projects.pid WHERE {$tblpx}subcontractor.subcontractor_id=" . $value['subcontractor_id'] . " AND {$tblpx}subcontractor_payment.approve_status='Yes' AND {$tblpx}subcontractor_payment.payment_quotation_status=2 AND {$tblpx}subcontractor_payment.project_id = " . $value['project_id'] . " GROUP BY {$tblpx}subcontractor_payment.project_id, {$tblpx}subcontractor_payment.date")->queryAll();
            foreach ($result as $key2 => $value2) {
                $report_array2[$j]['payment'][$value2['date']]['payment_amount'] = $value2['amount'];
                $report_array2[$j]['payment'][$value2['date']]['description'] = "";
            }
            foreach ($payment as $key3 => $value3) {
                $report_array2[$j]['advance'][$value3['date']]['advance_amount'] = $value3['amount'];
            }
            $j++;
        }

        //print_r($report_array2);

        $final_array2 = array();
        foreach ($report_array2 as $key => $values) {
            $final_array2[]['project'] = $values['project'];
            if (!empty($values['advance']) && !empty($values['payment'])) {
                $final_array2[$key]['details'] = array_merge_recursive($values['advance'], $values['payment']);
            } else {
                $final_array2[$key]['details'] = $values['advance'];
            }
        }

        if ($company_id != NULL) {
            $company = Company::model()->findbyPk($company_id);
            $company_name = ' - ' . $company->name;
        } else {
            $company_name = "";
        }

        $subcontractor = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}subcontractor WHERE subcontractor_id = " . $subcontractor_id . "")->queryRow();
        $name = $subcontractor['subcontractor_name'];
        $finaldata = array();
        $a = 0;
        $x = 0;
        $l = 0;
        $m = 0;
        $n = 0;
        $o = 0;
        $p = 0;
        $arraylabel = array('PAYMENT SUMMARY OF # ' . $name, '', '', '', '', '', '', '', '', '', '', '');
        $finaldata[0][] = 'Project Name';
        $finaldata[0][] = 'Date';
        $finaldata[0][] = 'Description';
        $finaldata[0][] = 'Quotation Amount';
        $finaldata[0][] = 'Payment Date';
        $finaldata[0][] = 'Payment';
        $finaldata[0][] = 'Balance';
        $quotation_total = 0;
        $payment_total = 0;
        $balance_total = 0;
        if (!empty($final_array)) {
            foreach ($final_array as $key => $value) {

                $a = 0;
                $blance = 0;
                foreach ($value['details'] as $key2 => $value2) {
                    if (isset($value2['quotation_amount'])) {
                        $quotation_amount = $value2['quotation_amount'];
                    } else {
                        $quotation_amount = 0;
                    }
                    if (isset($value2['payment_amount'])) {
                        $payment_amount = $value2['payment_amount'];
                    } else {
                        $payment_amount = 0;
                    }

                    $blance += $quotation_amount - $payment_amount;
                    if ($a >= 5) {
                        $blance = $blance + $quotation_amount;
                    } else {
                        $blance = $blance;
                    }
                    $quotation_total += $quotation_amount;
                    $payment_total += $payment_amount;
                    $balance_total += $blance;
                }
            }
        }
        $finaldata[1][] = '';
        $finaldata[1][] = '';
        $finaldata[1][] = 'Total = ';
        $finaldata[1][] = Controller::money_format_inr($quotation_total, 2, 1);
        $finaldata[1][] = '';
        $finaldata[1][] = Controller::money_format_inr($payment_total, 2, 1);
        Yii::import('ext.ECSVExport');
        $quotation_total = 0;
        $payment_total = 0;
        $balance_total = 0;
        $k = 2;
        if (!empty($final_array)) {
            foreach ($final_array as $key => $value) {
                $j = 0;
                $a = 0;
                $blance = 0;
                foreach ($value['details'] as $key2 => $value2) {
                    if (isset($value2['quotation_amount'])) {
                        $quotation_amount = $value2['quotation_amount'];
                    } else {
                        $quotation_amount = 0;
                    }
                    if (isset($value2['payment_amount'])) {
                        $payment_amount = $value2['payment_amount'];
                    } else {
                        $payment_amount = 0;
                    }

                    $blance += $quotation_amount - $payment_amount;
                    if ($a >= 5) {
                        $blance = $blance + $quotation_amount;
                    } else {
                        $blance = $blance;
                    }
                    $quotation_total += $quotation_amount;
                    $payment_total += $payment_amount;
                    $balance_total += $blance;

                    $finaldata[$k][] = ($a == 0) ? $value['project'] : "";
                    $finaldata[$k][] = isset($value2['quotation_amount']) ? $key2 : "";
                    $finaldata[$k][] = isset($value2['description']) ? $value2['description'] : "";
                    $finaldata[$k][] = isset($value2['quotation_amount']) ? $value2['quotation_amount'] : "";
                    $finaldata[$k][] = isset($value2['payment_amount']) ? $key2 : "";
                    $finaldata[$k][] = isset($value2['payment_amount']) ? $value2['payment_amount'] : "";
                    $finaldata[$k][] = isset($value2['payment_amount']) ? $blance : "";
                    ;
                    $a++;
                    $k++;
                    $x = $k;
                }
                $j++;
            }
        } else {
            $x = $k;
        }

        $l = $x + 1;

        $finaldata[$x][] = 'Project Name';
        $finaldata[$x][] = 'Date';
        $finaldata[$x][] = 'Description';
        $finaldata[$x][] = 'Advance Amount';
        $finaldata[$x][] = 'Payment Date';
        $finaldata[$x][] = 'Payment';
        $finaldata[$x][] = 'Balance';


        $advance_total = 0;
        $payment_total2 = 0;
        $balance_total2 = 0;
        if (!empty($final_array2)) {
            foreach ($final_array2 as $key => $value) {

                $a = 0;
                $blance = 0;
                foreach ($value['details'] as $key2 => $value2) {
                    if (isset($value2['advance_amount'])) {
                        $advance_amount = $value2['advance_amount'];
                    } else {
                        $advance_amount = 0;
                    }
                    if (isset($value2['payment_amount'])) {
                        $payment_amount = $value2['payment_amount'];
                    } else {
                        $payment_amount = 0;
                    }

                    $blance += $advance_amount - $payment_amount;
                    if ($a >= 5) {
                        $blance = $blance + $advance_amount;
                    } else {
                        $blance = $blance;
                    }
                    $advance_total += $advance_amount;
                    $payment_total2 += $payment_amount;
                    $balance_total2 += $blance;
                }
            }
        }

        $finaldata[$l][] = '';
        $finaldata[$l][] = '';
        $finaldata[$l][] = 'Total = ';
        $finaldata[$l][] = Controller::money_format_inr($advance_total, 2, $l);
        $finaldata[$l][] = '';
        $finaldata[$l][] = Controller::money_format_inr($payment_total2, 2, 1);
        Yii::import('ext.ECSVExport');
        $advance_total = 0;
        $payment_total2 = 0;
        $balance_total = 0;
        $m = $l + 1;
        if (!empty($final_array2)) {
            foreach ($final_array2 as $key => $value) {
                $j = 0;
                $a = 0;
                $blance = 0;
                foreach ($value['details'] as $key2 => $value2) {
                    if (isset($value2['advance_amount'])) {
                        $advance_amount = $value2['advance_amount'];
                    } else {
                        $advance_amount = 0;
                    }
                    if (isset($value2['payment_amount'])) {
                        $payment_amount = $value2['payment_amount'];
                    } else {
                        $payment_amount = 0;
                    }

                    $blance += $advance_amount - $payment_amount;
                    if ($a >= 5) {
                        $blance = $blance + $advance_amount;
                    } else {
                        $blance = $blance;
                    }
                    $advance_total += $advance_amount;
                    $payment_total2 += $payment_amount;
                    $balance_total += $blance;

                    $finaldata[$m][] = ($a == 0) ? $value['project'] : "";
                    $finaldata[$m][] = isset($value2['advance_amount']) ? $key2 : "";
                    $finaldata[$m][] = isset($value2['description']) ? $value2['description'] : "";
                    $finaldata[$m][] = isset($value2['advance_amount']) ? $value2['advance_amount'] : "";
                    $finaldata[$m][] = isset($value2['payment_amount']) ? $key2 : "";
                    $finaldata[$m][] = isset($value2['payment_amount']) ? $value2['payment_amount'] : "";
                    $finaldata[$m][] = isset($value2['payment_amount']) ? $blance : "";
                    $a++;
                    $k++;
                    $x = $k;
                    $m++;
                }
                $j++;
            }
        } else {
            $x = $k;
        }



        // $finaldata[$x][] = '';
        // $finaldata[$x][] = '';
        // $finaldata[$x][] = 'Total = ';
        // $finaldata[$x][] = Controller::money_format_inr($quotation_total, 2,1);
        // $finaldata[$x][] = '';
        // $finaldata[$x][] = Controller::money_format_inr($payment_total, 2,1);
        Yii::import('ext.ECSVExport');
        $csv = new ECSVExport($finaldata);
        $csv->setHeaders($arraylabel);
        $csv->setToAppend();
        $contentdata = $csv->toCSV();
        $csvfilename = 'Subcontractor Payment Report' . date('d_M_Y') . '.csv';
        Yii::app()->getRequest()->sendFile($csvfilename, $contentdata, "text/csv", false);
        exit();
    }

    public function actionDynamicProject()
    {

        $tblpx = Yii::app()->db->tablePrefix;
        $expId = $_REQUEST["expid"];
        $company = $_REQUEST["company"];
        $tblpx = Yii::app()->db->tablePrefix;
        $vendorData = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}scquotation INNER JOIN {$tblpx}subcontractor ON {$tblpx}scquotation.subcontractor_id= {$tblpx}subcontractor.subcontractor_id LEFT JOIN {$tblpx}projects ON {$tblpx}projects.pid={$tblpx}scquotation.project_id WHERE {$tblpx}subcontractor.subcontractor_id=" . $expId . " GROUP BY {$tblpx}scquotation.project_id ORDER BY {$tblpx}projects.name ASC")->queryAll();
        $vendorOptions = "<option value=''>-Select Project-</option>";
        foreach ($vendorData as $vData) {
            $vendorOptions .= "<option value='" . $vData["pid"] . "'>" . $vData["name"] . "</option>";
        }
        echo $vendorOptions;
    }

    public function actionExpenseProject()
    {
        $advance_value = $expense_value = 0;
        $options = "<option value=''>-Select Project-</option>";
        $contrator_id = $_REQUEST["expid"];
        $company = $_REQUEST["company"];
        if (!empty($contrator_id) && !empty($company)) {
            $criteria = new CDbCriteria();
            $criteria->condition = 'subcontractor_id = ' . $contrator_id . ' AND company_id = ' . $company;
            $criteria->group = 'projectid';
            $expense_datas = Dailyreport::model()->findAll($criteria);
            foreach ($expense_datas as $data) {
                $advance_sum = SubcontractorPayment::model()->findAll(array(
                    'select' => 'SUM(amount) as amount',
                    'condition' => 'subcontractor_id= ' . $data['subcontractor_id']
                        . ' AND company_id = ' . $data['company_id']
                        . ' AND project_id = ' . $data['projectid']
                        . ' AND quotation_number IS NULL'
                ));


                $expense_sum = Dailyreport::model()->findAll(array(
                    'select' => 'SUM(amount) as amount',
                    'condition' => 'subcontractor_id= ' . $data['subcontractor_id'] . ' AND company_id = ' . $data['company_id'] . ' AND projectid = ' . $data['projectid']
                ));

                if ($advance_sum[0]['amount'] != '') {
                    $advance_value = $advance_sum[0]['amount'];
                } else {
                    $advance_value = 0;
                }
                if (isset($expense_sum[0]['amount'])) {
                    $expense_value = $expense_sum[0]['amount'];
                } else {
                    $expense_value = 0;
                }

                if ($expense_value > $advance_value) {
                    $options .= "<option value='" . $data["projectid"] . "'>" . $data->project->name . "</option>";
                }
            }
            echo $options;
        }


    }

    public function actionAjaxcall()
    {
        echo json_encode('success');
    }

    public function actionDeletdailyentry()
    {
        $data = SubcontractorPayment::model()->findByPk($_REQUEST['expId']);
        $uniqueid = $data->uniqueid;
        $tblpx = Yii::app()->db->tablePrefix;
        if ($uniqueid) {
            $paymentdata = SubcontractorPayment::model()->findAll(array("condition" => "uniqueid='" . $uniqueid . "'"));
            foreach ($paymentdata as $pdata) {
                $newmodel = new JpLog('search');
                $newmodel->log_data = json_encode($pdata->attributes);
                $newmodel->log_action = 2;
                $newmodel->log_table = $tblpx . "subcontractor_payment";
                $newmodel->log_datetime = date('Y-m-d H:i:s');
                $newmodel->log_action_by = Yii::app()->user->id;
                $newmodel->company_id = $pdata->company_id;
                if ($newmodel->save()) {
                    SubcontractorPayment::model()->deleteAll(array("condition" => "payment_id=" . $pdata->payment_id . ""));
                    Expenses::model()->deleteAll(array("condition" => "subcontractor_id=" . $pdata->payment_id . ""));
                }
            }
            Reconciliation::model()->deleteAll(array("condition" => "reconciliation_parentids = '" . $uniqueid . "'"));
            $payDate = date('Y-m-d', strtotime($_REQUEST['payment_date']));
            $user = Users::model()->findByPk(Yii::app()->user->id);
            $arrVal = explode(',', $user->company_id);
            $newQuery = "";
            foreach ($arrVal as $arr) {
                if ($newQuery)
                    $newQuery .= ' OR';
                $newQuery .= " FIND_IN_SET('" . $arr . "', p.company_id)";
            }

            $expenseData = Yii::app()->db->createCommand("SELECT p.*,p.created_date as created, s.*,pr.*,ba.*,st.*,p.description as payment_description,p.company_id as company_id FROM " . $tblpx . "subcontractor_payment  p
                    LEFT JOIN " . $tblpx . "subcontractor s ON s.subcontractor_id = p.subcontractor_id
                    LEFT JOIN " . $tblpx . "projects pr ON pr.pid = p.project_id
                    LEFT JOIN " . $tblpx . "bank ba ON p.bank = ba.bank_id
                    LEFT JOIN " . $tblpx . "status st ON p.payment_type = st.sid
                    WHERE p.date = '" . $payDate . "' AND (" . $newQuery . ")")->queryAll();
            $client = $this->renderPartial('dailyentries_list', array('newmodel' => $expenseData));
            return $client;
        } else {
            if ($data) {
                $newmodel = new JpLog('search');
                $newmodel->log_data = json_encode($data->attributes);
                $newmodel->log_action = 2;
                $newmodel->log_table = $tblpx . "subcontractor_payment";
                $newmodel->log_datetime = date('Y-m-d H:i:s');
                $newmodel->log_action_by = Yii::app()->user->id;
                $newmodel->company_id = $data->company_id;
                if ($newmodel->save()) {
                    $expLoad = Expenses::model()->find(array(
                        'select' => array('exp_id'),
                        "condition" => "subcontractor_id='" . $_REQUEST['expId'] . "'",
                    ));
                    $expOldId = $expLoad["exp_id"];
                    if ($expLoad) {
                        Reconciliation::model()->deleteAll(array("condition" => "reconciliation_parentid=" . $expOldId . ""));
                    }

                    SubcontractorPayment::model()->deleteAll(array("condition" => "payment_id=" . $_REQUEST['expId'] . ""));
                    Expenses::model()->deleteAll(array("condition" => "subcontractor_id=" . $_REQUEST['expId'] . ""));
                    $payDate = date('Y-m-d', strtotime($_REQUEST['payment_date']));
                    $user = Users::model()->findByPk(Yii::app()->user->id);
                    $arrVal = explode(',', $user->company_id);
                    $newQuery = "";
                    foreach ($arrVal as $arr) {
                        if ($newQuery)
                            $newQuery .= ' OR';
                        $newQuery .= " FIND_IN_SET('" . $arr . "', p.company_id)";
                    }

                    $expenseData = Yii::app()->db->createCommand("SELECT p.*,p.created_date as created, s.*,pr.*,ba.*,st.*,p.description as payment_description,p.company_id as company_id FROM " . $tblpx . "subcontractor_payment  p
                            LEFT JOIN " . $tblpx . "subcontractor s ON s.subcontractor_id = p.subcontractor_id
                            LEFT JOIN " . $tblpx . "projects pr ON pr.pid = p.project_id
                            LEFT JOIN " . $tblpx . "bank ba ON p.bank = ba.bank_id
                            LEFT JOIN " . $tblpx . "status st ON p.payment_type = st.sid
                            WHERE p.date = '" . $payDate . "' AND (" . $newQuery . ")")->queryAll();
                    $client = $this->renderPartial('dailyentries_list', array('newmodel' => $expenseData));
                    return $client;
                } else {
                    echo 1;
                }
            }
        }
    }

    private function totalpaymetcalculation($subcontractor_id, $project_id, $amount, $id)
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $company = Company::model()->findByPk(Yii::app()->user->company_id);
        $paymentData = Yii::app()->db->createCommand("SELECT SUM(IFNULL(amount, 0) +IFNULL(tax_amount, 0))as totalsum FROM " . $tblpx . "subcontractor_payment WHERE subcontractor_id = " . $subcontractor_id . " AND project_id =" . $project_id . "")->queryRow();
        if (isset($id) && $id != '') {
            $currentdata = Yii::app()->db->createCommand("SELECT SUM(IFNULL(amount, 0) +IFNULL(tax_amount, 0)) as amount FROM " . $tblpx . "subcontractor_payment WHERE payment_id=" . $id . " AND subcontractor_id = " . $subcontractor_id . " AND project_id =" . $project_id . "")->queryRow();
            $pay_amount = ($paymentData['totalsum'] + $amount) - $currentdata['amount'];
        } else {
            $pay_amount = ($paymentData['totalsum'] + $amount);
        }
        $subcontractorData = Yii::app()->db->createCommand("SELECT SUM(scquotation_amount)as totalsum FROM " . $tblpx . "scquotation WHERE subcontractor_id = " . $subcontractor_id . " AND project_id =" . $project_id . "")->queryRow();
        $total_amount = ($subcontractorData['totalsum'] / 100) * $company['subcontractor_limit'];
        if (($subcontractorData['totalsum'] != 0)) {
            if ($pay_amount <= $total_amount) {
                $result = 1;
                if ($pay_amount == $total_amount) {
                    $this->sendmail($subcontractor_id, $project_id, $amount);
                }
            } else {
                $result = 0;
            }
        } else {
            $result = 1;
        }
    }

    private function sendmail($subcontractor_id, $project_id, $amount, $company_id)
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $mailsendcheck = Yii::app()->db->createCommand("SELECT SUM(item.item_amount) as scquotation_amount FROM {$tblpx}scquotation as scq LEFT JOIN {$tblpx}scquotation_items as item ON scq.scquotation_id=item.scquotation_id WHERE scq.subcontractor_id = " . $subcontractor_id . " AND scq.project_id =" . $project_id . " AND item.approve_status='Yes'")->queryRow();
        //if($mailsendcheck['mail_status'] == 'N') {
        $subcontractor = Subcontractor::model()->findByPk($subcontractor_id);
        $paymentData = Yii::app()->db->createCommand("SELECT SUM(IFNULL(amount, 0) +IFNULL(tax_amount, 0))as totalsum FROM " . $tblpx . "subcontractor_payment WHERE subcontractor_id = " . $subcontractor_id . " AND project_id =" . $project_id . " AND approve_status ='Yes'")->queryRow();
        $project = Projects::model()->findByPk($project_id);
        $mail = new JPhpMailer();
        $compamy = Company::model()->findByPk($company_id);
        if (isset($compamy['subco_email_userid'])) {
            $sql = "SELECT email FROM {$tblpx}users WHERE userid IN (" . $compamy['subco_email_userid'] . ")";

            $user_email = Yii::app()->db->createCommand($sql)->queryAll();
            $useremail = array_map('array_filter', $user_email);
            $useremail = array_filter($useremail);
        }
        $mail_array = array();
        $subject = "" . Yii::app()->name . " :Subcontractor Payment Alert!";
        $headers = "" . Yii::app()->name . "";
        $bodyContent = "<p>Hi</p><p>" . $compamy['subcontractor_limit'] . "% limit exceed, Please find the details.</p><p>Subcontractor : " . $subcontractor['subcontractor_name'] . "</p><p>Project : " . $project['name'] . "</p><p>Total Amount : " . $mailsendcheck['scquotation_amount'] . "</p><p>Paid Amount : " . (isset($paymentData['totalsum']) ? $paymentData['totalsum'] : '0') . "</p><p>Payment Amount : " . $amount . "</p><br><br><p>Regards,</p><p>" . Yii::app()->name . "</p>";
        $server = (($_SERVER['HTTP_HOST'] == 'localhost' || $_SERVER['HTTP_HOST'] == 'bhi.localhost.com') ? '0' : '1');
        if ($server == 0) {
            $mail->IsSMTP();
            $mail->Host = SMTP_HOST;
            $mail->SMTPSecure = SMTP_SECURE;
            $mail->SMTPAuth = true;
            $mail->Username = SMTP_USERNAME;
            $mail->Password = SMTP_PASSWORD;
        }

        $newdate = date('Y-m-d');

        $mail->setFrom(EMAILFROM, Yii::app()->name);
        if (!empty($useremail)) {
            foreach ($useremail as $key => $value) {
                if ($value['email'] != '') {
                    array_push($mail_array, '1');
                    $mail->addAddress($value['email']);
                }
            }
        } else {
            $user = Users::model()->findByPk(Yii::app()->user->id);
            if ($user['email'] != '') {
                array_push($mail_array, '1');
                $mail->addAddress($user['email']);
            } else {
                array_push($mail_array, '0');
            }
        }
        $mail->isHTML(true);

        $mail->Subject = "" . Yii::app()->name . " :Subcontractor Payment Alert!";
        //$mail->MsgHTML($bodyContent);
        //$path=$_SERVER['DOCUMENT_ROOT'].'/documents/Invoice_'.$newdate.'.pdf';
        //$name='Invoice_'.$newdate.'.pdf';
        //$mail->AddAttachment($path,$name);

        $mail->Body = $bodyContent;
        if (in_array("1", $mail_array)) {
            if ($mail->Send()) {
                //$update = Yii::app()->db->createCommand()->update('jp_scquotation', array('mail_status'=>"Y"),'scquotation_id=:scquotation_id',array(':scquotation_id'=>$mailsendcheck['scquotation_id']));
            }
        }
        //}
    }

    public function actionpermissionapprove()
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $data = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}subcontractor_payment WHERE payment_id='" . $_GET['item_id'] . "' ")->queryRow();
        if ($data['approve_status'] == 'No') {
            $update = Yii::app()->db->createCommand("UPDATE {$tblpx}subcontractor_payment SET approve_status = 'Yes' WHERE payment_id = '" . $_GET['item_id'] . "'")->execute();
            if ($update) {

                // expense alert mail
                $allocated_budget = array();
                $project_model = Projects::model()->findByPk($data['project_id']);
                $company_model = Company::model()->findByPk($data['company_id']);
                $project_id = $data['project_id'];
                $expense_amount = Yii::app()->db->createCommand("SELECT SUM(amount) as amount FROM " . $tblpx . "expenses WHERE projectid = " . $project_id . " AND subcontractor_id IS NULL AND company_id=" . $data['company_id'] . "")->queryRow();
                $sub_amount = Yii::app()->db->createCommand("SELECT SUM( IFNULL(amount, 0) + IFNULL(sgst_amount, 0) + IFNULL(cgst_amount, 0) + IFNULL(igst_amount, 0)) as amount FROM " . $tblpx . "subcontractor_payment WHERE project_id = " . $project_id . " AND approve_status ='Yes' AND company_id=" . $data['company_id'] . "")->queryRow();
                $vendor_amount = Yii::app()->db->createCommand("SELECT SUM( IFNULL(amount, 0) + IFNULL(sgst_amount, 0) + IFNULL(cgst_amount, 0) + IFNULL(igst_amount, 0)) as amount FROM " . $tblpx . "dailyvendors WHERE project_id = " . $project_id . " AND project_id IS NOT NULL AND company_id=" . $data['company_id'] . "")->queryRow();


                if ($project_model->profit_margin != NULL && $project_model->project_quote != NULL && $company_model->expenses_percentage != NULL && $company_model->expenses_email != NULL) {
                    $project_expense = (100 - ($project_model->profit_margin)) * (($project_model->project_quote) / 100);
                    $amount = $expense_amount['amount'] + $sub_amount['amount'] + $vendor_amount['amount'];
                    $expenses_percentage = explode(",", $company_model->expenses_percentage);
                    $percentage_array = array();
                    foreach ($expenses_percentage as $key => $percentage) {
                        $final_amount = ($project_expense * $percentage / 100);
                        if ($amount >= $final_amount) {
                            $percentage_array[] = $percentage;
                        }
                    }
                    if (!empty($percentage_array)) {
                        $percentage = max($percentage_array);
                        if ($project_model->expense_percentage != $percentage) {
                            $allocated_budget['payment_alert'] = array('percentage' => $percentage, 'amount' => $amount, 'profit_margin' => $project_model->profit_margin, 'project_quote' => $project_model->project_quote, 'project_id' => $project_id);
                            $this->savePaymentAlert($allocated_budget['payment_alert']);
                        }
                        $expense_alert = $this->saveExpenseNotifications($project_model, $percentage_array, 0);
                        if (!empty($expense_alert)) {
                            $allocated_budget['expense_advance_alert'] = $expense_alert;
                        }
                    } else {
                        $budget_percentage = NULL;
                    }
                    $update2 = Yii::app()->db->createCommand()->update($tblpx . 'projects', array('expense_amount' => $amount), 'pid=:pid', array(':pid' => $project_id));
                }

                $newmodel = new Expenses;
                $reconmodel = new Reconciliation;
                $newmodel->projectid = $data['project_id'];
                $newmodel->subcontractor_id = $data['payment_id'];
                $newmodel->expense_type = $data['payment_type'];
                if ($data['payment_type'] == 89 || $data['payment_type'] == 103) {
                    $newmodel->cheque_no = NULL;
                    $newmodel->bank_id = NULL;
                    $newmodel->reconciliation_status = NULL;
                } else {
                    $newmodel->cheque_no = $data['cheque_no'];
                    $newmodel->bank_id = $data['bank'];
                    $reconmodel->reconciliation_table = $tblpx . "expenses";
                    $reconmodel->reconciliation_paymentdate = date('Y-m-d', strtotime($data['date']));
                    $reconmodel->reconciliation_bank = $data['bank'];
                    $reconmodel->reconciliation_chequeno = $data['cheque_no'];
                    $reconmodel->created_date = date("Y-m-d H:i:s");
                    $reconmodel->reconciliation_status = 0;
                    $reconmodel->company_id = $data['company_id'];
                }
                $expense_totalamount = ($data['sgst_amount'] + $data['cgst_amount'] + $data['igst_amount']) + $data['amount'];
                $expense_paidamount = $expense_totalamount - $data['tds_amount'];
                $newmodel->expense_amount = $data['amount'];
                $newmodel->expense_sgstp = $data['sgst'];
                $newmodel->expense_sgst = $data['sgst_amount'];
                $newmodel->expense_cgstp = $data['cgst'];
                $newmodel->expense_cgst = $data['cgst_amount'];
                $newmodel->expense_igstp = $data['igst'];
                $newmodel->expense_igst = $data['igst_amount'];
                $newmodel->expense_tdsp = $data['tds'];
                $newmodel->expense_tds = $data['tds_amount'];
                $newmodel->amount = $expense_totalamount;
                $newmodel->description = $data['description'];
                $newmodel->date = date('Y-m-d', strtotime($data['date']));
                $newmodel->type = 73;
                $newmodel->created_by = Yii::app()->user->id;
                $newmodel->created_date = date('Y-m-d');
                //  $newmodel->updated_date = date('Y-m-d');
                $newmodel->company_id = $data['company_id'];
                $newmodel->paid = ($data['amount'] + $data['tax_amount']);
                $newmodel->paidamount = ($data['amount'] + $data['tax_amount']) - $data['tds_amount'];
                $newmodel->userid = Yii::app()->user->id;
                $reconmodel->reconciliation_payment = "Subcontractor Payment";
                $reconmodel->reconciliation_amount = ($data['amount'] + $data['tax_amount']) - $data['tds_amount'];
                $reconmodel->reconciliation_parentids = $data['uniqueid'];
                if ($newmodel->save()) {
                    $lastInsExpId = Yii::app()->db->getLastInsertID();

                    if (!empty($allocated_budget)) {
                        $this->projectexpensealert($allocated_budget, $_GET['item_id'], 'daybook');
                    }

                    $reconmodel->reconciliation_parentid = $lastInsExpId;
                    $reconmodel->reconciliation_parentlist = $lastInsExpId;
                    if ($data['payment_type'] == 88) {
                        $reconmodel->save();
                    }
                }
                echo json_encode(array('response' => 'success', 'msg' => 'Permission approved Successfully '));
            } else {
                echo json_encode(array('response' => 'error', 'msg' => 'Some problem occured'));
            }
        } else {
            echo json_encode(array('response' => 'warning', 'msg' => 'Permission already approved'));
        }
    }

    public function actiondynamicSubcontractor()
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $comId = $_POST["comId"];
        $tblpx = Yii::app()->db->tablePrefix;
        $projectData = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}subcontractor WHERE FIND_IN_SET(" . $comId . ", company_id);")->queryAll();
        $projectOptions = "<option value=''>-Select Project-</option>";
        foreach ($projectData as $vData) {
            $projectOptions .= "<option value='" . $vData["subcontractor_id"] . "'>" . $vData["subcontractor_name"] . "</option>";
        }
        $bankData = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}bank WHERE FIND_IN_SET(" . $comId . ", company_id);")->queryAll();
        $bankOptions = "<option value=''>-Select Bank-</option>";
        foreach ($bankData as $vData) {
            $bankOptions .= "<option value='" . $vData["bank_id"] . "'>" . $vData["bank_name"] . "</option>";
        }
        echo json_encode(array('project' => $projectOptions, 'bank' => $bankOptions));
    }

    public function actionGetProjectRow()
    {
        $rowOldId = $_REQUEST["row"];
        $company = $_REQUEST["company"];
        $subcon = $_REQUEST["subcon"];
        $projectids = json_decode($_REQUEST["projectids"]);
        $projectids = rtrim($projectids, ',');
        $tblpx = Yii::app()->db->tablePrefix;
        $rowId = $rowOldId + 1;
        $subconData = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}scquotation INNER JOIN {$tblpx}subcontractor ON {$tblpx}scquotation.subcontractor_id= {$tblpx}subcontractor.subcontractor_id LEFT JOIN {$tblpx}projects ON {$tblpx}projects.pid={$tblpx}scquotation.project_id WHERE {$tblpx}subcontractor.subcontractor_id=" . $subcon . " AND {$tblpx}projects.pid NOT IN(" . $projectids . ") GROUP BY {$tblpx}scquotation.project_id")->queryAll();
        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        $newQuery1 = "";
        $newQuery = "";
        foreach ($arrVal as $arr) {
            if ($newQuery1)
                $newQuery1 .= ' OR';
            if ($newQuery)
                $newQuery .= ' OR';
            $newQuery1 .= " FIND_IN_SET('" . $arr . "', company_id)";
            $newQuery .= " FIND_IN_SET('" . $arr . "', id)";
        }
        $result = '<div class="row_block paymentrow" id="' . $rowId . '">
            <div class="elem_block">
                <div class="form-group">
                    ';
        $result .= '<select class="form-control js-example-basic-single multi_projects singleproject" name="project_name[]" style="width:200px;" id="SubcontractorPayment_project_id' . $rowId . '" data-id="' . $rowId . '">
                        <option  value="">-Select Project-</option>';
        /* $prolist = Projects::model()->findAll(array(
          'select' => array('pid, name'),
          'order' => 'pid',
          'condition' => '('.$newQuery1.')',
          'distinct' => true,
          )); */
        if (!empty($subconData)) {
            foreach ($subconData as $pro) {
                $result .= '<option value="' . $pro['pid'] . '" >' . $pro['name'] . '</option>';
            }
        }
        $result .= '</select>';
        $result .= '</div>
            </div>
            <div class="elem_block amountfld">
                <div class="form-group">
                    <input type="text" class="form-control amountvalidation sub_paymentamount" name="SubcontractorPaymentamount[]" id="SubcontractorPayment_amount' . $rowId . '" onkeypress ="nextSectionode(this, event)" data-id="' . $rowId . '"/>
                </div>
            </div>
            <div class="elem_block gsts amounts">
                <div class="form-group">
                    <span  class="gstvalue" id="txtSgst' . $rowId . '"></span>
                </div>
            </div>
            <div class="elem_block gsts amounts">
                <div class="form-group">
                    <span  class="gstvalue" id="txtCgst' . $rowId . '"></span>
                </div>
            </div>
            <div class="elem_block gsts amounts">
                <div class="form-group">
                    <span  class="gstvalue" id="txtIgst' . $rowId . '"></span>
                </div>
            </div>
            <div class="elem_block amounts">
                <div class="form-group">
                    <input type="hidden" class="form-control amountvalidation" readonly="true" id="SubcontractorPayment_tax_amount' . $rowId . '" name="SubcontractorPayment[tax_amount]"/>
                    <span class="gstvalue" id="txtgstTotal' . $rowId . '"></span>
                </div>
            </div>
            <div class="elem_block amountlong">
                <div class="form-group">
                    <span class="gstvalue" id="txtTotal' . $rowId . '"></span>
                </div>
            </div>
            <div class="elem_block gsts">
                <div class="form-group">
                    <input type="text" class="form-control amountvalidation percentage tdsrate" id="SubcontractorPayment_tds' . $rowId . '" name="SubcontractorPayment[tds]" data-id="' . $rowId . '"/>
                    <span class="gstvalue" id="txtTds' . $rowId . '"></span>
                    <input type="hidden" class="form-control amountvalidation percentage" id="SubcontractorPayment_tds_amount' . $rowId . '" name="SubcontractorPayment[tds_amount]"/>
                </div>
            </div>
            <div class="elem_block amountlong">
                <div class="form-group">
                    <span class="gstvalue" id="txtPaidAmount' . $rowId . '"></span>
                    <input type="hidden" class="form-control amountvalidation percentage" id="SubcontractorPayment_paidamount' . $rowId . '" name="SubcontractorPayment[paidamount]"/>
                </div>
            </div>
            <input type="hidden" class="permission" name="txtPermission' . $rowId . '" id="txtPermission' . $rowId . '" value=""/>
        </div><div  id="errorMessage' . $rowId . '" class="rowerror"></div>';
        echo $result;
    }

    public function actionValidateSubcontractorLimit()
    {
        $company = $_REQUEST["company"];
        $subcon = $_REQUEST["subcon"];
        $totRow = $_REQUEST["totRow"];
        $project = $_REQUEST["project"];
        $rowTotal = $_REQUEST["rowTotal"];
        $status = $_REQUEST["status"];
        $rownum = $_REQUEST["rownum"];
        $paymentId = $_REQUEST["paymentId"];
        $quotation_num = $_REQUEST["quotation_num"];
        $tblpx = Yii::app()->db->tablePrefix;
        $companyData = Company::model()->findByPk($company);
        if (isset($companyData) && !empty($companyData)) {
            $subconLimit = $companyData->subcontractor_limit;
        } else {
            $subconLimit = 80;
        }
        $quotation_query = $quotation_query1 = '';
        if (!empty($quotation_num)) {
            $quotation_query = ' AND sp.quotation_number = ' . $quotation_num;
            $quotation_query1 = ' AND sq.scquotation_id = ' . $quotation_num;
        }
        if ($paymentId != "") {
            $currentUID = SubcontractorPayment::model()->findByPk($paymentId);
            $uniqueId = $currentUID["uniqueid"];
            $currsql = "SELECT * FROM jp_subcontractor_payment sp WHERE sp.uniqueid = '{$uniqueId}' AND sp.project_id = {$project} AND sp.subcontractor_id = {$subcon} AND sp.company_id = {$company}";
            $currentData = Yii::app()->db->createCommand($currsql)->queryRow();
            $oldAmount = $currentData["amount"] + $currentData["tax_amount"];
        } else {
            $oldAmount = 0;
        }

        $sql = "SELECT SUM(IFNULL(qi.item_amount, 0)) as totalquotation, a.totalpaid, b.writeoff FROM {$tblpx}scquotation_items qi
                            LEFT JOIN {$tblpx}scquotation sq  ON qi.scquotation_id = sq.scquotation_id " . $quotation_query1 . "
                            LEFT JOIN (SELECT SUM(IFNULL(sp.amount,0) + IFNULL(sp.tax_amount,0)) as totalpaid FROM {$tblpx}subcontractor_payment sp WHERE sp.subcontractor_id = {$subcon} AND sp.project_id = {$project}" . $quotation_query . ") a ON 1=1
                            LEFT JOIN (SELECT SUM(IFNULL(w.amount,0)) as writeoff FROM {$tblpx}writeoff w LEFT JOIN {$tblpx}scquotation sq1 ON w.subcontractor_id = sq1.scquotation_id WHERE sq1.subcontractor_id = {$subcon} AND sq1.project_id = {$project}) b ON 1=1
                            WHERE qi.approve_status = 'Yes' AND sq.project_id = {$project} AND sq.subcontractor_id = {$subcon} AND sq.scquotation_status = 1";

        $paymentData = Yii::app()->db->createCommand($sql)->queryRow();
        $quotAmount = $paymentData["totalquotation"];
        $paidAmount = $paymentData["totalpaid"];
        $writeOff = $paymentData["writeoff"];
        $balanceTotal = $quotAmount - $writeOff;
        if ($paidAmount)
            $newPaid = ($paidAmount - $oldAmount) + $rowTotal;
        else
            $newPaid = $rowTotal;
        $paymentLimit = ($subconLimit / 100) * $balanceTotal;
        if ($newPaid > $paymentLimit) {
            if ($balanceTotal > 0)
                $result["message"] = $subconLimit . "% of {$balanceTotal} is exceeded. Need permission.";
            else
                $result["message"] = "There is no approved quotation for this subcontractor in this project. So, this payment need permission.";
            $result["rownum"] = $rownum;
        } else {
            $result["message"] = 1;
            $result["rownum"] = $rownum;
        }
        echo json_encode($result);
    }

    public function actionAddSubcontractorPayment($preData = 0)
    {

        $bank = $_REQUEST['bank'];
        $count = $_REQUEST["count"];
        $rowpaid = $_REQUEST["rowpaid"];
        $totPaid = $_REQUEST["totPaid"];
        $payment_type = $_REQUEST["payment_mode"];
        $quotation_no = $_REQUEST['quotation_no'];
        $stage = $_REQUEST['stage'];
        $append_desc = "";
        $inserted_ids = '';
        if ($payment_type == 2) {
            $append_desc = " - Payment without quotation";
        }

        $tblpx = Yii::app()->db->tablePrefix;
        $j = 0;

        $transaction = Yii::app()->db->beginTransaction();
        try {

            for ($i = 0; $i < $count; $i++) {
                $model = new SubcontractorPayment;
                $model->attributes = $_REQUEST;
                $model->project_id = $_REQUEST["projects"][$i];
                $model->amount = $_REQUEST["amount"][$i];
                $model->description = $_REQUEST["description"] . $append_desc;
                $model->date = date("Y-m-d", strtotime($_REQUEST["date"]));
                $model->reconciliation_status = NULL;

                if ($model->payment_type == 88) {
                    $model->reconciliation_status = 0;
                }

                $model->sgst_amount = $_REQUEST["sgst_amount"][$i];
                $model->cgst_amount = $_REQUEST["cgst_amount"][$i];
                $model->igst_amount = $_REQUEST["igst_amount"][$i];
                $model->tax_amount = $_REQUEST["totalTax"][$i];
                $model->tds = $_REQUEST["tds"][$i];
                $model->tds_amount = $_REQUEST["tdsamount"][$i];
                $model->paidamount = $rowpaid[$i];
                $model->created_by = Yii::app()->user->id;
                $model->created_date = date("Y-m-d");
                $model->approve_status = $_REQUEST["permission"][$i];
                $model->uniqueid = strtotime(date("Y-m-d H:i:s")) . "" . md5($model->company_id . "" . $model->subcontractor_id);
                $model->rowcount_slno = $i + 1;
                $model->rowcount_total = $count;
                $model->payment_quotation_status = $payment_type[$i];
                $model->quotation_number = $quotation_no[$i];
                $stage_data = explode(',', $stage[$i]);
                if ($stage_data[0] == 2) {
                    $model->stage_id = NULL;
                    $model->sc_bill_id = $stage_data[1];
                } else if ($stage_data[0] == 1) {
                    $model->stage_id = $stage_data[1];
                    $model->sc_bill_id = NULL;
                }



                $data = $model->attributes;
                $newmodel = $this->addExpenseData(
                    $data,
                    $_REQUEST["rowTotal"][$i],
                    $append_desc,
                    $model->tds,
                    $model->tds_amount,
                    $payment_type[$i],
                    $rowpaid[$i],
                    $model,
                    0,
                    NULL
                );



                if ($_REQUEST["permission"][$i] == 'No') {
                    //   $this->sendmail($model->subcontractor_id, $model->project_id, $_REQUEST["rowTotal"][$i], $model->company_id);
                }

                if ($model->save()) {
                    $status = 1;
                    $lastId[$i] = $model->payment_id;

                    $newmodel->subcontractor_id = $model->payment_id;
                    if ($model->approve_status == "Yes") {
                        if ($newmodel->save()) {
                            $status = 1;
                            $lastEId[$j] = $newmodel->exp_id;
                            if (!empty($allocated_budget)) {
                                $this->projectexpensealert($allocated_budget, $lastEId[$j], 'subcontractor_payment');
                            }
                            $j = $j + 1;
                        } else {
                            $status = 0;
                            //print_r($newmodel->getErrors());exit;
                            throw new Exception(json_encode($newmodel->getErrors()));
                        }
                    }
                    $status = 1;
                } else {
                    $status = 0;
                    //print_r($model->getErrors());exit;
                    throw new Exception(json_encode($model->getErrors()));
                }
            }


            $inserted_ids = implode(',', $lastId);
            if (count($lastId) > 0) {
                if ($model->payment_type == 88 && $j > 0) {
                    $reconmodel = new Reconciliation;
                    $reconmodel->reconciliation_table = $tblpx . "expenses";
                    $reconmodel->reconciliation_payment = "Subcontractor Payment";
                    $reconmodel->reconciliation_paymentdate = date("Y-m-d", strtotime($model->date));
                    $reconmodel->reconciliation_amount = $totPaid;
                    $reconmodel->reconciliation_chequeno = $model->cheque_no;
                    $reconmodel->reconciliation_bank = $model->bank;
                    $reconmodel->created_date = date("Y-m-d");
                    $reconmodel->reconciliation_status = 0;
                    $reconmodel->reconciliation_parentids = $model->uniqueid;
                    $reconmodel->company_id = $model->company_id;
                    $parentIds = implode(", ", $lastEId);
                    $reconmodel->reconciliation_parentlist = $parentIds;
                    $reconmodel->reconciliation_parentid = $lastEId[0];
                    if ($reconmodel->save()) {
                        $status = 1;
                    } else {
                        $status = 0;
                        throw new Exception(json_encode($model->getErrors()));
                    }
                }
                $payDate = date('Y-m-d', strtotime($model->date));
                $user = Users::model()->findByPk(Yii::app()->user->id);
            }
            $transaction->commit();
        } catch (Exception $error) {
            $transaction->rollback();
        } finally {
            if (isset($status) && ($status == 1)) {
                echo json_encode(array('status' => '', 'id' => $inserted_ids));
            } else {
                $error_message = $this->setErrorMessage($model->getErrors());
                if ($error_message == 'insufficient balance') {
                    $result_ = array('status' => 5);
                } else if ($error_message == 'Something went wrong! Check again or contact support team') {
                    $result_ = array('status' => 'a');
                } else {
                    $result_ = array('status' => 1, 'error_message' => $error_message);
                }
                echo json_encode($result_);
                exit;
            }
        }
    }

    public function actionupdateSubcontractorPayment()
    {

        $bank = $_REQUEST['bank'];
        $count = $_REQUEST["count"];
        $rowpaid = $_REQUEST["rowpaid"];
        $totPaid = $_REQUEST["totPaid"];
        $payment_type = $_REQUEST["payment_mode"];
        $quotation_no = $_REQUEST['quotation_no'];
        $stage = $_REQUEST['stage'];
        $append_desc = "";
        if ($payment_type == 2) {
            $append_desc = " - Payment without quotation";
        }
        $tblpx = Yii::app()->db->tablePrefix;
        $j = 0;
        $i = 0;
        $paymentid = $_REQUEST["paymentId"];
        $tblpx = Yii::app()->db->tablePrefix;
        $paymentData = Yii::app()->db->createCommand("SELECT * FROM " . $tblpx . "subcontractor_payment "
            . "WHERE payment_id = " . $paymentid)->queryRow();
        $uniqueid = $paymentData["uniqueid"];

        if (Yii::app()->user->role == 1) {
            $paymentdetails = SubcontractorPayment::model()->findAll(array("condition" => "uniqueid='" . $uniqueid . "'"));
            foreach ($paymentdetails as $model) {
                $model->attributes = $_REQUEST;
                $model->project_id = $_REQUEST["projects"][$i];
                $model->amount = $_REQUEST["amount"][$i];
                $model->description = $_REQUEST["description"] . $append_desc;
                $model->date = date("Y-m-d", strtotime($_REQUEST["date"]));

                $model->reconciliation_status = NULL;

                if ($model->payment_type == 88) {
                    $model->reconciliation_status = 0;
                }

                $model->sgst_amount = $_REQUEST["sgst_amount"][$i];
                $model->cgst_amount = $_REQUEST["cgst_amount"][$i];
                $model->igst_amount = $_REQUEST["igst_amount"][$i];
                $model->tax_amount = $_REQUEST["totalTax"][$i];
                $model->tds = $_REQUEST["tds"][$i];
                $model->tds_amount = $_REQUEST["tdsamount"][$i];
                $model->paidamount = $rowpaid[$i];
                $model->created_by = Yii::app()->user->id;
                $model->created_date = date("Y-m-d");
                $model->approve_status = $_REQUEST["permission"][$i];
                $model->uniqueid = strtotime(date("Y-m-d H:i:s")) . "" . md5($model->company_id . "" . $model->subcontractor_id);
                $model->rowcount_slno = $i + 1;
                $model->rowcount_total = $count;
                $model->payment_quotation_status = $payment_type[$i];
                $model->quotation_number = $quotation_no[$i];
                $model->stage_id = $stage[$i];

                if ($_REQUEST["permission"][$i] == 'No') {
                    $this->sendmail($model->subcontractor_id, $model->project_id, $_REQUEST["rowTotal"][$i], $model->company_id);
                }

                if ($model->save()) {
                    $lastId[$i] = $model->payment_id;
                    Yii::app()->user->setFlash('success', "Subcontractor payment updated ");
                    $result_ = array('status' => '');
                    echo json_encode($result_);

                } else {
                    $success_status = 0;
                    Yii::app()->user->setFlash('error', 'Error Occured');
                    $result_ = array('status' => 1);
                    echo json_encode($result_);
                    exit;
                }
                $i++;
            }

            $inserted_ids = implode("','", $lastId);
            $expenseDetails = Expenses::model()->findAll(array("condition" => "subcontractor_id IN('" . $inserted_ids . "')"));

            foreach ($expenseDetails as $expensemodel) {
                $data = $model->attributes;
                $newmodel = $this->addExpenseData(
                    $data,
                    $model->amount,
                    $append_desc,
                    $model->tds,
                    $model->tds_amount,
                    $model->payment_type,
                    $model->paidamount,
                    $model,
                    1,
                    $expensemodel
                );
                $i++;
            }
            if ($newmodel->save()) {
                $success_status = 1;
            } else {
                $success_status = 0;
                Yii::app()->user->setFlash('error', 'Error Occured');
                $result_ = array('status' => 1);
                echo json_encode($result_);
                exit;
            }

            if (count($lastId) > 0) {

                if ($model->payment_type == 88) {
                    $reconmodelData = Reconciliation::model()->findAll(array("condition" => "reconciliation_parentid IN('" . $inserted_ids . "')"));
                    foreach ($reconmodelData as $key => $reconmodel) {
                        $reconmodel->reconciliation_payment = "Subcontractor Payment";
                        $reconmodel->reconciliation_amount = $totPaid;
                        $reconmodel->reconciliation_chequeno = $model->cheque_no;
                        $reconmodel->reconciliation_bank = $model->bank;
                        $reconmodel->reconciliation_status = 0;
                        $reconmodel->company_id = $model->company_id;

                        if ($reconmodel->save()) {
                            $success_status = 1;
                        } else {
                            $success_status = 0;
                            Yii::app()->user->setFlash('error', 'Error Occured');
                            $result_ = array('status' => 1);
                            echo json_encode($result_);
                            exit;
                        }
                        break;
                    }
                }
            }

        } else {
            $paymentdetails = SubcontractorPayment::model()->findAll(array(
                "condition" => "uniqueid='" . $uniqueid . "'"
            ));

            foreach ($paymentdetails as $model) {
                $model->approval_status = 0;

                if (!$model->save()) {
                    $success_status = 0;
                    Yii::app()->user->setFlash('error', 'Error Occured');
                    $result_ = array('status' => 1);
                    echo json_encode($result_);
                    exit;

                } else {
                    $lastId[$i] = $model->payment_id;
                    $success_status = 1;
                }
                $i++;
            }

            $inserted_ids = implode(',', $lastId);
            $this->addPreSubcontractorPaymentData(
                $_REQUEST,
                $bank,
                $count,
                $rowpaid,
                $totPaid,
                $payment_type,
                $quotation_no,
                $stage,
                $append_desc,
                $inserted_ids
            );
        }
    }

    public function setErrorMessage($modelErrors)
    {
        if (!empty($modelErrors)) {
            foreach ($modelErrors as $key => $value) {
                return $value[0];
            }
        }
    }

    public function actionDeleteconfirmation()
    {
        $expId = $_REQUEST["expId"];
        $date = $_REQUEST["expense_date"];
        $status = $_REQUEST["status"];
        $delId = $_REQUEST["delId"];
        $action = $_REQUEST["action"];
        $tblpx = Yii::app()->db->tablePrefix;
        $success = "";
        if ($status == 1) {
            $deletedata = new Deletepending;
            $model = SubcontractorPayment::model()->findByPk($expId);
            $deletedata->deletepending_table = $tblpx . "subcontractor_payment";
            $deletedata->deletepending_data = json_encode($model->attributes);
            $deletedata->deletepending_parentid = $expId;
            $deletedata->user_id = Yii::app()->user->id;
            $deletedata->requested_date = date("Y-m-d");
            if ($deletedata->save()) {
                $success = "Yes";
            } else {
                echo 1;
            }
        } else if ($status == 2) {
            $model = Deletepending::model()->findByPk($delId);
            if ($model->delete()) {
                $success = "Yes";
            } else {
                echo 1;
            }
        }

        if ($success == "Yes") {
            $expDate = date('Y-m-d', strtotime($_REQUEST['expense_date']));
            $user = Users::model()->findByPk(Yii::app()->user->id);
            $arrVal = explode(',', $user->company_id);
            $newQuery = "";
            foreach ($arrVal as $arr) {
                if ($newQuery)
                    $newQuery .= ' OR';
                $newQuery .= " FIND_IN_SET('" . $arr . "', p.company_id)";
            }
            $expenseData = Yii::app()->db->createCommand("SELECT p.*,p.created_date as created, s.*,pr.*,ba.*,st.*,p.description as payment_description,p.company_id as company_id FROM " . $tblpx . "subcontractor_payment  p
                                LEFT JOIN " . $tblpx . "subcontractor s ON s.subcontractor_id = p.subcontractor_id
                                LEFT JOIN " . $tblpx . "projects pr ON pr.pid = p.project_id
                                LEFT JOIN " . $tblpx . "bank ba ON p.bank = ba.bank_id
                                LEFT JOIN " . $tblpx . "status st ON p.payment_type = st.sid
                                WHERE p.date = '" . $expDate . "' AND (" . $newQuery . ")")->queryAll();
            $client = $this->renderPartial('dailyentries_list', array('newmodel' => $expenseData));
            return $client;
        }
    }

    public function actionSubcontractorpaymentbyuser()
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $currUser = Yii::app()->user->id;
        $model = new SubcontractorPayment;
        $model->unsetAttributes();
        $model->created_by = $currUser;
        if (isset($_GET["SubcontractorPayment"])) {
            $model->fromdate = $_REQUEST["SubcontractorPayment"]["fromdate"];
            $model->todate = $_REQUEST["SubcontractorPayment"]["todate"];
        }

        $this->render('subcontractorpaymentbyuser', array(
            'model' => $model,
            'dataProvider' => $model->usersearch(),
        ));
    }

    public function actionDuplicateentry()
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $model = new SubcontractorPayment;
        $entry = Yii::app()->db->createCommand("SELECT  count(s.payment_id) as rowcount, s.* FROM {$tblpx}subcontractor_payment s
                    GROUP BY s.subcontractor_id, s.project_id, s.amount, s.date, s.description
                    HAVING COUNT(s.payment_id) > 1")->queryAll();
        $this->render('duplicateentry', array(
            'model' => $model,
            'duplicateentry' => $entry,
        ));
    }

    public function actionGetduplicateentries()
    {
        $subcontractor = $_REQUEST["subcontractor"];
        $project = $_REQUEST["project"];
        $amount = $_REQUEST["amount"];
        $date = $_REQUEST["date"];
        $description = $_REQUEST["description"];
        $tblpx = Yii::app()->db->tablePrefix;
        $duplicate = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}subcontractor_payment WHERE subcontractor_id = '{$subcontractor}' AND project_id = '{$project}' AND  amount = '{$amount}' AND date = '{$date}' AND description='{$description}' ")->queryAll();
        $result = '<button type="button" class="close"  aria-hidden="true">×</button><h4>Subcontractor payment</h4><table class="table table-responsive table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Sl No</th>
                                    <th>Company</th>
                                    <th>Subcontractor</th>
                                    <th>Project</th>
                                    <th>Description</th>
                                    <th>Transaction Type</th>
                                    <th>Bank</th>
                                    <th>Cheque No</th>
                                    <th>Amount</th>
                                    <th>Tax Amount</th>
                                    <th>TDS Amount</th>
                                    <th>Paid Amount</th>
                                </tr>
                            </thead>
                            <tbody>';
        $i = 1;
        foreach ($duplicate as $entry) {
            $result .= '         <tr>
                                    <td>' . $i . '</td>
                                    <td>';
            $company = Company::model()->findByPk($entry["company_id"]);
            if ($company)
                $result .= $company->name;
            $result .= '            </td>
                                    <td>';
            $subcon = Subcontractor::model()->findByPk($entry["subcontractor_id"]);
            if ($subcon)
                $result .= $subcon->subcontractor_name;
            $result .= '            </td>
                                    <td>';
            $project = Projects::model()->findByPk($entry["project_id"]);
            if ($project)
                $result .= $project->name;
            $result .= '            </td>
                                    <td>' . $entry["description"] . '</td>
                                    <td>';
            $ptype = Status::model()->findByPk($entry["payment_type"]);
            if ($ptype)
                $result .= $ptype->caption;
            $result .= '            </td>
                                    <td>';
            $bank = Bank::model()->findByPk($entry["bank"]);
            if ($bank)
                $result .= $bank->bank_name;
            $result .= '            </td>
                                    <td>' . $entry["cheque_no"] . '</td>
                                    <td>' . Controller::money_format_inr($entry["amount"], 2) . '</td>
                                    <td><span class="popover-test tax_amnt" data-toggle="popover" data-trigger="hover" data-placement="left" data-trigger="hover" type="button" data-html="true">' . Controller::money_format_inr($entry["tax_amount"], 2) . '</span>
                                    <td><span class="popover-test tds_amnt" data-toggle="popover" data-trigger="hover" data-placement="left" data-trigger="hover" type="button" data-html="true">' . Controller::money_format_inr($entry["tds_amount"], 2) . '</span>
                                    <td><span class="popover-test paid_amnt" data-toggle="popover" data-trigger="hover" data-placement="left" data-trigger="hover" type="button" data-html="true">' . Controller::money_format_inr($entry["paidamount"], 2) . '</span>
                                        <div class="popover-content hide">
                                            <div><span>SGST: (' . $entry["sgst"] . '&#37;) ' . Controller::money_format_inr($entry["sgst_amount"], 2) . '</span></div>
                                            <div><span>CGST: (' . $entry["cgst"] . '&#37;) ' . Controller::money_format_inr($entry["cgst_amount"], 2) . '</span></div>
                                            <div><span>IGST: (' . $entry["igst"] . '&#37;) ' . Controller::money_format_inr($entry["igst_amount"], 2) . '</span></div>
                                        </div>
                                    </td>
                                </tr>';
            $i++;
        }
        $result .= '     </tbody>
                       </table>';
        echo $result;
    }

    public function actionGetAllData()
    {
        $expDate = date('Y-m-d', strtotime($_REQUEST['date']));
        $tblpx = Yii::app()->db->tablePrefix;
        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        $newQuery = "";
        foreach ($arrVal as $arr) {
            if ($newQuery)
                $newQuery .= ' OR';
            $newQuery .= " FIND_IN_SET('" . $arr . "', p.company_id)";
        }
        $expenseData = Yii::app()->db->createCommand("SELECT p.*,p.created_date as created, s.*,pr.*,ba.*,st.*,p.description as payment_description,p.company_id as company_id FROM " . $tblpx . "subcontractor_payment  p
                            LEFT JOIN " . $tblpx . "subcontractor s ON s.subcontractor_id = p.subcontractor_id
                            LEFT JOIN " . $tblpx . "projects pr ON pr.pid = p.project_id
                            LEFT JOIN " . $tblpx . "bank ba ON p.bank = ba.bank_id
                            LEFT JOIN " . $tblpx . "status st ON p.payment_type = st.sid
                            WHERE p.date = '" . $expDate . "' AND (" . $newQuery . ")")->queryAll();
        $client = $this->renderPartial('dailyentries_list', array('newmodel' => $expenseData));
        return $client;
    }

    public function actiongetQuotations()
    {
        $quotationList = "<option value=''>-Select Quotation Number-</option>";
        if (isset($_POST['subconId']) && isset($_POST['companyId']) && isset($_POST['project_id'])) {
            $project_id = $_POST['project_id'];
            $company_id = $_POST['companyId'];
            $subcontractor_id = $_POST['subconId'];
            $quotations = Scquotation::model()->findAllByAttributes(array('project_id' => $project_id, 'subcontractor_id' => $subcontractor_id, 'company_id' => $company_id));

            if (!empty($quotations)) {
                foreach ($quotations as $quotation) {
                    if (!empty($quotation['scquotation_no'])) {
                        $quotationList .= "<option value='" . $quotation["scquotation_id"] . "'>" . $quotation["scquotation_no"] . "</option>";
                    }
                }
            }
            echo json_encode(array('quotations' => $quotationList));
        }
    }

    public function addExpenseData($data, $rowTotal, $append_desc, $tds, $tdsamount, $payment_type, $rowpaid, $model, $isNewRecord, $expensemodel)
    {
        $newmodel = new Expenses;
        if ($isNewRecord == 1) {
            $newmodel = $expensemodel;
        }

        $newmodel->setAttributes($data);
        $newmodel->projectid = $model->project_id;
        $newmodel->expense_type = $model->payment_type;
        $newmodel->expense_amount = $model->amount;
        $newmodel->expense_sgstp = $model->sgst;
        $newmodel->expense_sgst = $model->sgst_amount;
        $newmodel->expense_cgstp = $model->cgst;
        $newmodel->expense_cgst = $model->cgst_amount;
        $newmodel->expense_igstp = $model->igst;
        $newmodel->expense_igst = $model->igst_amount;
        $newmodel->amount = $rowTotal;
        $newmodel->description = $model->description . $append_desc;
        $newmodel->type = 73;
        $newmodel->expense_tdsp = $tds;
        $newmodel->expense_tds = $tdsamount;
        $newmodel->paid = $rowTotal;
        $newmodel->paidamount = $rowpaid;
        $newmodel->payment_quotation_status = $payment_type;
        $newmodel->userid = Yii::app()->user->id;
        $newmodel->approval_status = '1';
        $newmodel->bank_id = $model->bank;
        $newmodel->cheque_no = $model->cheque_no;
        $newmodel->subcontractor_id = $model->payment_id;

        return $newmodel;
    }

    public function addPreSubcontractorPaymentData($data, $bank, $count, $rowpaid, $totPaid, $payment_type, $quotation_no, $stage, $append_desc, $ids)
    {

        if ($payment_type == 2) {
            $append_desc = " - Payment without quotation";
        }

        $tblpx = Yii::app()->db->tablePrefix;
        $j = 0;


        $transaction = Yii::app()->db->beginTransaction();
        try {

            for ($i = 0; $i < $count; $i++) {
                $premodel = new PreSubcontractorPayment();
                $sql = "SELECT count(*) as count,MAX(payment_id) as lastId "
                    . " FROM " . $this->tableNameAcc('pre_subcontractor_payment', 0) . " "
                    . " WHERE ref_id='" . $_REQUEST["refIds"][$i] . "'";
                $followupdata = Yii::app()->db->createCommand($sql)->queryRow();
                if ($followupdata['count'] > 0) {
                    $premodel->followup_id = $followupdata['lastId'];
                } else {
                    $premodel->followup_id = NULL;
                }
                $premodel->attributes = $_REQUEST;
                $premodel->project_id = $_REQUEST["projects"][$i];
                $premodel->amount = $_REQUEST["amount"][$i];
                $premodel->description = $_REQUEST["description"] . $append_desc;
                $premodel->date = date("Y-m-d", strtotime($_REQUEST["date"]));
                $premodel->reconciliation_status = NULL;

                if ($premodel->payment_type == 88) {
                    $premodel->reconciliation_status = 0;
                }
                $premodel->ref_id = $_REQUEST["refIds"][$i];
                $premodel->record_grop_id = date('Y-m-d H:i:s');
                $premodel->updated_by = Yii::app()->user->id;
                $premodel->updated_date = date('Y-m-d H:i:s');
                $premodel->record_action = "update";
                $premodel->approve_notify_status = 1;

                $premodel->sgst_amount = $_REQUEST["sgst_amount"][$i];
                $premodel->cgst_amount = $_REQUEST["cgst_amount"][$i];
                $premodel->igst_amount = $_REQUEST["igst_amount"][$i];
                $premodel->tax_amount = $_REQUEST["totalTax"][$i];
                $premodel->tds = $_REQUEST["tds"][$i];
                $premodel->tds_amount = $_REQUEST["tdsamount"][$i];
                $premodel->paidamount = $rowpaid[$i];
                $premodel->created_by = Yii::app()->user->id;
                $premodel->created_date = date("Y-m-d");
                $premodel->approve_status = $_REQUEST["permission"][$i];
                $premodel->uniqueid = strtotime(date("Y-m-d H:i:s")) . "" . md5($premodel->company_id . "" . $premodel->subcontractor_id);
                $premodel->rowcount_slno = $i + 1;
                $premodel->rowcount_total = $count;
                $premodel->payment_quotation_status = $payment_type[$i];
                $premodel->quotation_number = $quotation_no[$i];
                $premodel->stage_id = $stage[$i];

                if ($_REQUEST["permission"][$i] == 'No') {
                    $this->sendmail($model->subcontractor_id, $model->project_id, $_REQUEST["rowTotal"][$i], $model->company_id);
                }

                if ($premodel->save()) {

                    $lastId[$i] = $premodel->payment_id;
                    $status = 1;
                } else {
                    print_r($premodel->getErrors());

                    $status = 0;
                    throw new Exception(json_encode($premodel->getErrors()));
                }
            }


            $transaction->commit();
        } catch (Exception $error) {
            $transaction->rollback();
        } finally {
            if ($status == 1) {
                echo json_encode(array('status' => '', 'id' => $ids));
            } else {

            }
        }
    }

    public function actioncheckQuotationDate()
    {

        if ($_POST['scquotation_id'] != "") {
            $sql = "SELECT * FROM jp_scquotation "
                . " WHERE scquotation_id =" . $_POST['scquotation_id'];
            $sc_quotation_row = Yii::app()->db->createCommand($sql)->queryRow();

            $sql = 'SELECT scquotation_date FROM jp_scquotation '
                . ' WHERE scquotation_id =' . $_POST['scquotation_id'];
            $scquotation_date = Yii::app()->db->createCommand($sql)->queryScalar();

            $scQDate = strtotime(date("d-m-Y", strtotime($scquotation_date)));
            $entry_date = strtotime($_POST['entry_date']);

            if ($entry_date < $scQDate) {
                echo json_encode(
                    array(
                        'msg' => 'Selected Quotation is valid only from ' . date("d-m-Y", strtotime($scquotation_date)),
                        'response' => 'error',
                        'stage' => array(),
                        'sgst' => $sc_quotation_row['sgst_percent'],
                        'cgst' => $sc_quotation_row['cgst_percent'],
                        'igst' => $sc_quotation_row['igst_percent']
                    )
                );
            } else {
                $qid = $_POST['scquotation_id'];
                $stage = array();
                $selection = "";

                $criteria = new CDbCriteria();
                $criteria->select = '*';
                $criteria->condition = "`quotation_id` = " . $qid;

                $stagemodel = ScPaymentStage::model()->findAll($criteria);
                $stageData = $this->getstageData($qid);


                echo json_encode(array(
                    'msg' => '',
                    'response' => 'success',
                    'stage' => $stageData,
                    'stage_count' => count($stagemodel),
                    'sgst' => $sc_quotation_row['sgst_percent'],
                    'cgst' => $sc_quotation_row['cgst_percent'],
                    'igst' => $sc_quotation_row['igst_percent']
                ));
            }
        } else {
            echo json_encode(
                array(
                    'msg' => '',
                    'response' => 'success',
                    'stage' => array(),
                    'sgst' => $sc_quotation_row['sgst_percent'],
                    'cgst' => $sc_quotation_row['cgst_percent'],
                    'igst' => $sc_quotation_row['igst_percent']
                )
            );
        }
    }

    public function actiongetstageamount()
    {


        $data = explode(',', $_POST['id']);
        $type = $data[0];
        $id = isset($data[1]) ? $data[1] : "";
        $final_stage_amount = '';

        if ($id != "") {
            if ($type == 1) {
                $model = ScPaymentStage::model()->findByPk($id);
                $quotation_amount = Scquotation::model()->getTotalQuotationAmount($model->quotation_id);
                $stage_amount = $quotation_amount * ($model->stage_percent / 100);

                $sub_sum = SubcontractorPayment::model()->findAll(array(
                    'select' => 'SUM(amount) as amount',
                    'condition' => 'stage_id = ' . $model['id']
                ));

                $final_stage_amount = $stage_amount - $sub_sum[0]['amount'];
            } else {
                $model = Subcontractorbill::model()->findByPk($id);
                $bill_amount = $model->total_amount;

                $sub_sum = SubcontractorPayment::model()->findAll(array(
                    'select' => 'SUM(amount) as amount',
                    'condition' => 'sc_bill_id = ' . $model['id']
                ));

                $final_stage_amount = $bill_amount - $sub_sum[0]['amount'];
            }
        }

        echo $final_stage_amount;
    }


    public function getstageData($qid)
    {

        $options = "<option value=''>-Select Bill/Stage-</option>";
        $criteria = new CDbCriteria();
        $criteria->select = '*';
        $criteria->condition = "`quotation_id` = " . $qid . " AND `bill_status` = '0'";
        $stagedata = ScPaymentStage::model()->findAll($criteria);

        $criteria = new CDbCriteria();
        $criteria->select = '*';
        $criteria->condition = "`scquotation_id` = " . $qid;
        $billdata = Subcontractorbill::model()->findAll($criteria);


        if (!empty($stagedata)) {
            $options .= "<optgroup label='Payment Stage'>";
            foreach ($stagedata as $data) {
                $sub_sum = SubcontractorPayment::model()->findAll(array(
                    'select' => 'SUM(amount) as amount',
                    'condition' => 'stage_id = ' . $data['id']
                        . ' AND quotation_number =' . $qid
                ));


                $quotation_amount = Scquotation::model()->getTotalQuotationAmount($data->quotation_id);
                $stage_amount = $quotation_amount * ($data->stage_percent / 100);
                $sub_value = 0;
                if ($sub_sum[0]['amount'] != '') {
                    $sub_value = $sub_sum[0]['amount'];
                }
                $stage_value = $stage_amount;

                if ($stage_value > $sub_value) {
                    $options .= "<option value='1," . $data["id"] . "'>" . $data->payment_stage . "</option>";
                }
            }
            $options .= "</optgroup>";
        }

        if (!empty($billdata)) {

            $options .= "<optgroup label='Bills'>";
            foreach ($billdata as $data) {
                $sub_sum = SubcontractorPayment::model()->findAll(array(
                    'select' => 'SUM(amount) as amount',
                    'condition' => 'sc_bill_id = ' . $data['id']
                        . ' AND quotation_number =' . $qid
                ));

                $bill_amount = $data->total_amount;

                $sub_value = 0;
                if ($sub_sum[0]['amount'] != '') {
                    $sub_value = $sub_sum[0]['amount'];
                }

                if ($bill_amount > $sub_value) {
                    $options .= "<option value='2," . $data["id"] . "'>" . $data->bill_number . "</option>";
                }
            }
            $options .= "</optgroup>";
        }

        return $options;
    }

    public function actionAllentries()
    {
        $model = new SubcontractorPayment('search');
        $date_from = '';
        $date_to = date("Y-m-d");
        $newQuery1 = '';
        if (isset($_GET['date_from'])) {
            $date_from = isset($_GET['date_from']) ? $_GET['date_from'] : "";

            $date_from = date("Y-m-d", strtotime($date_from));
            $model->date_from = $date_from;


        }
        if (isset($_GET['date_to'])) {
            $date_to = $_GET['date_to'];
            $date_to = date("Y-m-d", strtotime($date_to));

            $model->date_to = $date_to;
        }


        $tblpx = Yii::app()->db->tablePrefix;
        //$model->unsetAttributes();
        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        $newQuery = "";
        foreach ($arrVal as $arr) {
            if ($newQuery)
                $newQuery .= ' OR';
            $newQuery .= " FIND_IN_SET('" . $arr . "', p.company_id)";
        }
        $currDate = date('Y-m-d', strtotime(date("Y-m-d")));
        if ($date_to == '1970-01-01') {
            $date_to = $currDate; // Default to the current date
        }
        if (!empty($date_from) && !empty($date_to)) {
            $newQuery1 .= " AND p.date BETWEEN '" . $date_from . "' AND '" . $date_to . "'";
        } elseif (!empty($date_from)) {
            $newQuery1 .= " AND p.date > '" . $date_from . "'";
        } elseif (!empty($date_to)) {
            $newQuery1 .= " AND p.date <=  '" . $date_to . "'";
        }
        //p.date = '" . $currDate . "' AND
        $sql = "SELECT p.*,p.created_date as created,p.date as paid_date, s.*,pr.*,ba.*,st.*,p.description as payment_description,p.company_id as company_id FROM " . $tblpx . "subcontractor_payment  p
                        INNER JOIN " . $tblpx . "subcontractor s ON s.subcontractor_id = p.subcontractor_id
                        LEFT JOIN " . $tblpx . "projects pr ON pr.pid = p.project_id
                        LEFT JOIN " . $tblpx . "bank ba ON p.bank = ba.bank_id
                        LEFT JOIN " . $tblpx . "status st ON p.payment_type = st.sid
                        WHERE  (" . $newQuery . ")" . $newQuery1;
        //die($sql);
        $expenseData = Yii::app()->db->createCommand($sql)->queryAll();
        $this->render('allentries_list', array(
            'model' => $model,
            'newmodel' => $expenseData
        ));
    }
    public function actionAgingreportbydate()
    {

        $this->render('ageing_report_due_date');
    }
}
