<?php
/* @var $this BankController */
/* @var $model Bank */

$this->breadcrumbs=array(
	'Deposit'=>array('index'),
	$model->deposit_id=>array('view','id'=>$model->deposit_id),
	'Update',
);

/*$this->menu=array(
	array('label'=>'List Bank', 'url'=>array('index')),
	array('label'=>'Create Bank', 'url'=>array('create')),
	array('label'=>'View Bank', 'url'=>array('view', 'id'=>$model->bank_id)),
	array('label'=>'Manage Bank', 'url'=>array('admin')),
);*/
?>

<div class="modal-dialog modal-sm">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" onclick="javascript:window.location.reload()">&times;</button>
			<h4 class="modal-title">Edit Deposit</h4>
		</div>
		<?php $this->renderPartial('_form', array('model'=>$model)); ?>
	</div>
	
</div>
