<?php
$this->breadcrumbs = array(
    'Subcontractor',
);
$page = Yii::app()->request->getParam('Subcontractor_page');
if ($page != '') {
    Yii::app()->user->setReturnUrl($page);
} else {
    Yii::app()->user->setReturnUrl(0);
}
$fifth_column_width = Yii::app()->user->role == 1 || Yii::app()->user->role == 2 ? ["width" => "400px", "targets" => 5] : '';
?>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<div class="container" id="vendors">
    <div class="loading-overlay">
        <span class="fa fa-spinner fa-3x fa-spin"></span>
    </div>
    <div class="expenses-heading sub-heading mb-10">
        <h3>Subcontractors</h3>
        <?php if ((isset(Yii::app()->user->role) && (in_array('/subcontractor/subcontractorcreate', Yii::app()->user->menuauthlist)))) { ?>
            <button class="create_contract btn btn-primary">Add Sub Contractor</button>
        <?php } ?>
        <!-- remove addentries class -->
        <div id="loading"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/ajax-loader.gif"></div>
    </div>
    <div id='subcontform' style='display:none;'></div>
    <?php //$this->renderPartial('_search', array('model' => $model)) ?>
    <div id="msg_box"></div>
    <div class="table-wrapper">
        <?php $this->widget('zii.widgets.CListView', array(
            'dataProvider' => $dataProvider,
            'itemView' => '_view',
            'template' => '<div class=""><table cellpadding="10" class="table" id="subcontable">{items}</table></div>',
            'ajaxUpdate' => false,
            'viewData' => array('fifth_column_width' => $fifth_column_width)
        )); ?>

        <div class="modal fade edit" role="dialog">
            <div class="modal-dialog modal-lg">
            </div>
        </div>
        <?php
        Yii::app()->clientScript->registerScript('myjavascript', '
                $(document).ready(function(){
                    $("#subcontable").dataTable( {
                        "scrollY": "300px",
                        "scrollX": true,
                        "scrollCollapse": true,
                        "columnDefs"        : [       
                            {
                                "searchable"    : false, 
                                "targets"       : [0,3] 
                            },
                            { "bSortable": false, "aTargets": [-1]  },
                        '. json_encode( $fifth_column_width) .',
                        ],
                    } );
                    var table = $("#subcontable").DataTable();
                    $("#Subcontractor_subcontractor_name").on( "keyup", function () {
                    var subconname = $(this).val();
                        table
                            .columns( 1 )
                            .search( subconname )
                            .draw();
                    } );
                    $("#clear").on( "click", function () {
                    var selectedText=" ";
                        table
                            .columns( 1 )
                            .search( selectedText )
                            .draw();
                    } );
                });

            ');
        ?>

    </div>
</div>

<script>
    function closeaction() {
        $('#subcontform').slideUp();
        $('.create_contract').attr('disabled', false);
    }

    $(document).ready(function () {
        $('#loading').hide();
        $('.create_contract').click(function () {
            $(this).attr('disabled', 'disabled');
            // $('.loading-overlay').addClass('is-active');
            $.ajax({
                type: "GET",
                url: "<?php echo $this->createUrl('subcontractor/create&layout=1') ?>",
                success: function (response) {
                    $("#subcontform").html(response).slideDown();

                }
            });
        });

        $(document).delegate('.editSubcontractor', 'click', function () {
            $("html, body").animate({
                scrollTop: 0
            }, "slow");
            var id = $(this).attr('data-id');
            $('.loading-overlay').addClass('is-active');
            $.ajax({
                type: "GET",
                url: "<?php echo $this->createUrl('subcontractor/update&layout=1&id=') ?>" + id,
                success: function (response) {
                    $("#subcontform").html(response).slideDown();

                }
            });
        });

        $(document).delegate('.deleteSubcontractor', 'click', function () {

            if (confirm("Are you sure you want to delete?")) {
                var id = $(this).attr('data-id');
                $('.loading-overlay').addClass('is-active');
                $.ajax({
                    method: "POST",
                    data: {
                        id: id,
                    },
                    dataType: "json",
                    url: "<?php echo $this->createUrl('subcontractor/removeSubcontractor') ?>",
                    success: function (response) {
                        $("#msg_box").html('<div class="alert alert-' + response.response + ' alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>' + response.msg + '</div>');
                        setTimeout(function () {
                            window.location.reload(1);
                        }, 1000);

                    }
                });
            }
        });

        $(document).delegate('.permissionSubcontractor', 'click', function () {
            var id = $(this).attr('data-id');
            $('.loading-overlay').addClass('is-active');
            $.ajax({
                type: "GET",
                url: "<?php echo $this->createUrl('subcontractor/getproject&id=') ?>" + id,
                success: function (response) {
                    $("#subcontform").html(response).slideDown();

                }
            });

        });

        jQuery(function ($) {
            $('#subcontractor').on('keydown', function (event) {
                if (event.keyCode == 13) {
                    $("#subcontractor").submit();
                }
            });
        });
    });
    $(document).ajaxComplete(function () {
        $('.loading-overlay').removeClass('is-active');
        $('#loading').hide();
    });
</script>

<style>
    .page-body h3 {
        margin: 4px 0px;
        color: inherit;
        text-align: left;
    }

    table.dataTable tbody td {
        padding: 12px 10px;
    }

    .tooltip-hiden {
        width: auto;
    }

    .panel {
        border: 1px solid #ddd;
    }

    .panel-heading {
        background-color: #eee;
        height: 40px;
    }
</style>