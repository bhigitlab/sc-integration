<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.min.js"></script>
<div class="page_filter">
    <?php $form = $this->beginWidget('CActiveForm', array(
        'action' => Yii::app()->createUrl($this->route),
        'method' => 'get',
    )); ?>
    <div class="row">
        <div class="col-sm-4 col-md-3 col-lg-2 form-group">
            <label>Item </label>
            <select id="item_id" name="item_id" class="select2 form-control">
                <option value="">Select Item</option>
                <?php foreach ($items as $value) { ?>
                    <option value="<?php echo $value['id']; ?>" <?php echo ($item_id == $value['id']) ? 'selected' : ''; ?>>
                        <?php echo $value['data']; ?>
                    </option>
                <?php } ?>
            </select>
        </div>
        <div class="col-sm-4 col-md-3 col-lg-2 form-group">
            <label>Vendor </label>
            <?php
            $user = Users::model()->findByPk(Yii::app()->user->id);
            $arrVal = explode(',', $user->company_id);
            $newQuery = "";
            foreach ($arrVal as $arr) {
                if ($newQuery)
                    $newQuery .= ' OR';
                $newQuery .= " FIND_IN_SET('" . $arr . "', company_id)";
            }
            echo CHtml::dropDownList('vendor_id', 'vendor_id', CHtml::listData(Vendors::model()->findAll(array(
                'select' => array('vendor_id, name'),
                'order' => 'name',
                'condition' => '(' . $newQuery . ')',
                'distinct' => true
            )), 'vendor_id', 'name'), array('empty' => 'Select Vendor', 'class' => 'select2 form-control', 'id' => 'vendor_id', 'options' => array($vendor_id => array('selected' => true))));
            ?>
        </div>
        <div class="col-sm-4 col-md-3 col-lg-2 form-group">
            <label>From </label>
            <?php echo CHtml::textField('date_from', (!empty($date_from) ? date('d-M-Y', strtotime($date_from)) : ''), array('placeholder' => 'Date From', "id" => "date_from", "class" => "form-control w-100 white-bg", "readonly" => true)); ?>
        </div>
        <div class="col-sm-4 col-md-3 col-lg-2 form-group">
            <label>To </label>
            <?php echo CHtml::textField('date_to', (!empty($date_to) ? date('d-M-Y', strtotime($date_to)) : ''), array('placeholder' => 'Date To', "id" => "date_to", "class" => "form-control w-100 white-bg", "readonly" => true)); ?>
        </div>
        <div class="col-xs-12 col-lg-2 form-group text-right text-sm-left">
            <label>&nbsp;</label>
            <div>
                <input name="yt0" value="Go" type="submit" class="btn btn-info btn-sm">
                <input id="reset" name="yt1" value="Clear" type="reset" class="btn btn-other btn-sm">
            </div>
        </div>
    </div>
    <?php $this->endWidget(); ?>
</div>
<script>
    $('#reset').click(function () {
        location.href = '<?php echo $this->createUrl('purchase/previouspurchase'); ?>';
    })
    $(document).ready(function () {
        $(".select_box").select2();
    });
</script>