<?php
$project_list = array();

?>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/tableHeadFixer.js"></script>
<style>
    .main_headd {
        background-color: rgba(212, 206, 206, 0.75);
        position: relative;
        top: 0px;
        text-align: center;
        font-size: 15px;
    }
</style>
<div class="container" id="project">
    <h2>Buyer REPORTS</h2>
    <div class="page_filter clearfix">
        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'debit-credit-search-form',
            'action' => Yii::app()->createAbsoluteUrl('buyer/buyers/buyerReport'),
            'method' => 'POST',
        ));
        ?>
        <div class="filter_elem">

            <?php
            echo $form->dropDownList($model, 'company_id', $company_list, array('class' => 'form-control js-example-basic-single', 'empty' => '-Select Company-'));
            ?>
        </div>
        <div class="filter_elem">

            <?php
            echo $form->dropDownList($model, 'buyer_id', $buyer_list, array('class' => 'form-control js-example-basic-single', 'empty' => '-Select Buyer-'));
            ?>
        </div>
        <div class="filter_elem">

            <?php
            echo $form->dropDownList($model, 'project_id', $project_list, array('class' => 'form-control js-example-basic-single', 'empty' => '-Select Project-'));
            ?>
        </div>


        <div class="filter_elem filter_btns">
            <?php echo CHtml::submitButton('GO', array('id' => 'btSubmit')); ?>
            <?php echo CHtml::resetButton('Clear', array('onclick' => 'javascript:location.href="' . $this->createUrl('buyerreport') . '"')); ?>
        </div>
        <?php $this->endWidget(); ?>
    </div>
    <div id="contenthtml" class="contentdiv">

        <?php
        $debit_amount = 0;
        $credit_amount = 0;
        ?>
        <div class="">
            <div class="clearfix">

            </div>
            <div class="clearfix">
                <div id="parent">

                    <table class="table" id="fixTable">
                        <thead>
                            <!-- <th colspan=8 class="main_headd">DEBIT</th> -->

                        </thead>
                        <thead>
                            <th>Sl No</th>
                            <th>Date</th>
                            <th>Party Name</th>
                            <th></th>
                            <th>Vch Type</th>
                            <th>Apartment No</th>
                            <th>Project Name</th>
                            <th>Debit</th>
                            <th>Credit</th>
                        </thead>
                        <tbody>
                            <?php
                            $x = 0;
                            $total_debit_amount = 0;
                            $total_credit_amount = 0;
                            foreach ($final_array as $dat) {
                                if ($dat['type'] == 1) {
                                    $total_debit_amount += $dat['debit'];
                                } elseif ($dat['type'] == 2) {
                                    $total_credit_amount += $dat['credit'];
                                }

                                $x++;
                            ?>
                                <tr>
                                    <td><?= $x ?></td>
                                    <td><?= date('d-M-Y', strtotime($dat['date'])) ?></td>
                                    <td><?= $dat['buyer'] ?></td>
                                    <td><?= $dat['desc'] ?></td>
                                    <td><?= $dat['voucher_type'] ?></td>
                                    <td><?= isset($dat['flat_no']) ? $dat['flat_no'] : '' ?></td>
                                    <td><?= $dat['name'] ?></td>
                                    <td><?= $dat['debit'] ?></td>
                                    <td><?= $dat['credit'] ?></td>
                                </tr>
                            <?php
                            }
                            ?>

                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="7"><b>Total</b></td>
                                <td style="text-align: right;"><b><i class="fa fa-inr"></i><?= $total_debit_amount ?></td>
                                </td>
                                <td style="text-align: right;"><b><i class="fa fa-inr"></i> <?= $total_credit_amount ?></td>
                                </td>
                            </tr>
                        </tfoot>
                    </table>



                </div>
            </div>
        </div>
    </div>
    <style>
        .odd {
            background: #FFF;
        }

        #parent {
            max-height: 400px;
        }

        thead th,
        tfoot td {
            background: #eee;
        }

        .table {
            margin-bottom: 0px;
        }

        .table>thead>tr>th,
        .table>tbody>tr>th,
        .table>tfoot>tr>th,
        .table>thead>tr>td,
        .table>tbody>tr>td,
        .table>tfoot>tr>td {
            border-bottom: 0px;
        }
    </style>
    <?php $getBuyerList = Yii::app()->createAbsoluteUrl("/buyer/default/getBuyers"); ?>
    <?php $getprojecstUrl = Yii::app()->createAbsoluteUrl("/buyer/default/getProjects"); ?>
    <script>
        $(document).ready(function() {
            getProjectData();
            getBuyerList();
            // $("#parent").css('max-height', $(window).height() - ($("#myNavbar").height() + $(".page_filter").height() + ("h2").height()));
            $("#fixTable").tableHeadFixer({
                'foot': true,
                'head': true
            });
            $('#BuyerTransactions_company_id').change(function() {

                getBuyerList();
                getProjectData();
            })

        });

        function getBuyerList() {
            var company_id = $('#BuyerTransactions_company_id').val();

            $.ajax({
                url: "<?php echo $getBuyerList; ?>",
                data: {
                    "company_id": company_id
                },
                type: "POST",
                dataType: 'json',
                success: function(data) {
                    var buyer_id = $('#BuyerTransactions_buyer_id').val();



                    $('#BuyerTransactions_buyer_id').empty();
                    $("#BuyerTransactions_buyer_id").prepend("<option value='' selected='selected'>-Select Buyer -</option>");
                    if (buyer_id === '') {
                        $.each(data.buyer_list, function(val, text) {

                            $('#BuyerTransactions_buyer_id').append($('<option></option>').val(val).html(text))
                        });
                    } else {
                        $.each(data.buyer_list, function(val, text) {
                            if (val == buyer_id) {
                                $('#BuyerTransactions_buyer_id').append($('<option selected="selected"></option>').val(val).html(text))
                            } else {
                                $('#BuyerTransactions_buyer_id').append($('<option></option>').val(val).html(text))
                            }

                        });
                    }




                }
            });
        }

        function getProjectData() {
            var company_id = $("#BuyerTransactions_company_id").val();

            $.ajax({
                url: "<?php echo $getprojecstUrl; ?>",
                data: {
                    "company_id": company_id
                },
                type: "POST",
                dataType: 'json',
                success: function(data) {

                    $('#BuyerTransactions_project_id').empty();
                    $("#BuyerTransactions_project_id").prepend("<option value='' selected='selected'>-Select Project -</option>");
                    $.each(data.project_list, function(val, text) {
                        $('#BuyerTransactions_project_id').append($('<option></option>').val(val).html(text))
                    });

                }
            });

        }
    </script>