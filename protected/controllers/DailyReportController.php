<?php

class DailyReportController extends Controller
{

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        $accessArr = array();
        $accessauthArr = array();
        $controller = Yii::app()->controller->id;
        if (isset(Yii::app()->user->menuall)) {
            if (array_key_exists($controller, Yii::app()->user->menuall)) {
                $accessArr = Yii::app()->user->menuall[$controller];
            }
        }

        if (isset(Yii::app()->user->menuauth)) {
            if (array_key_exists($controller, Yii::app()->user->menuauth)) {
                $accessauthArr = Yii::app()->user->menuauth[$controller];
            }
        }
        $access_privlg = count($accessauthArr);
        return array(
            array(
                'allow', // allow all users to perform 'index' and 'view' actions
                'actions' => $accessArr,
                'users' => array('@'),
                'expression' => "$access_privlg > 0",
            ),

            array(
                'allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => $accessauthArr,
                'users' => array('@'),
                'expression' => "$access_privlg > 0",
            ),

            array(
                'allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('ajaxcall', 'dynamicVendor','getDynamicScQuotation','getTemplateDetails','getfieldsForEdit','getNewInputRow','bulkapproval','getProjectTemplate','RefreshButton','GetCompany','Viewlabour','rejectLabour','estimateBal'),
                'users' => array('@'),
            ),
            array(
                'deny',  // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate($id='')
    {
        $id = isset($_GET['id'])?$_GET['id']:'';
        if($id){
            $model = Dailyreport::model()->findByPk($id);
        }else{
            $model = new Dailyreport;
        }
        
        $id = Yii::app()->user->id;
        $tblpx = Yii::app()->db->tablePrefix;
        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        $pms_api_integration_model = ApiSettings::model()->findByPk(1);
        $pms_api_integration = $pms_api_integration_model->api_integration_settings;
        $newQuery = "";
        foreach ($arrVal as $arr) {
            if ($newQuery) $newQuery .= ' OR';
            $newQuery .= " FIND_IN_SET('" . $arr . "', e.company_id)";
        }
        $currDate = date('Y-m-d', strtotime(date("Y-m-d")));
        $expense_data_sql = "SELECT e.*, p.name as pname,s.subcontractor_name as subname,exp.type_name FROM " . $this->tableName('Dailyreport') . " e";
        $expense_data_sql .= " LEFT JOIN " . $this->tableName('Projects') . " p ON e.projectid = p.pid ";
        $expense_data_sql .= "LEFT JOIN " . $this->tableName('Subcontractor') . " s ON e.subcontractor_id = s.subcontractor_id ";
        $expense_data_sql .= "LEFT JOIN " . $this->tableName('ExpenseType') . " exp on exp.type_id= e.expensehead_id ";
        if($pms_api_integration==1){
            $expense_data_sql .= "WHERE e.date = '" . $currDate . "'";
        }else{
            $expense_data_sql .= "WHERE e.date = '" . $currDate . "' AND (" . $newQuery . ")";
        }
        
        $expenseData    = Yii::app()->db->createCommand($expense_data_sql)->queryAll();
        $last_date_sql = "SELECT date FROM " . $this->tableName('Dailyreport') . " ORDER BY date DESC LIMIT 1";
        $lastdate = Yii::app()->db->createCommand($last_date_sql)->queryRow();
        $this->render('dailyreport_form', array('model' => $model, 'newmodel' => $expenseData, 'id' => $id, 'lastdate' => $lastdate));
    }



    /* added by minu of create daily report for mobile version on 09-07-17 */

    public function actionAddreport($id = 0)
    {

        $tblpx = Yii::app()->db->tablePrefix;
        $completion_date = '';
        $worktype_list = '';
        if (isset($_GET['pid'])) {
            $proid = $_GET['pid'];

            $types = $list = Yii::app()->db->createCommand("SELECT DISTINCT {$tblpx}work_type.wtid,{$tblpx}work_type.work_type FROM {$tblpx}work_type LEFT JOIN {$tblpx}project_work_type ON {$tblpx}project_work_type.wrk_type_id = {$tblpx}work_type.wtid WHERE {$tblpx}project_work_type.project_id = '" . $proid . "'")->queryAll();
            if ($list != NULL) {
                $worktype_list = "<option >Select Type</option>";

                // each $row is an array representing a row of data
                foreach ($list as $type)
                    $worktype_list .= CHtml::tag('option', array('value' => $type['wtid']), CHtml::encode($type['work_type']), true);
            } else {
                $worktype_list = "<option >No Types</option>";
            }
        }
        if ($id == 0) {
            $model = new Dailyreport;
        } else {
            $model = $this->loadModel($id);
            $proid = $model->projectid;
            $projectmodel = Projects::model()->findByPk($proid);
            if ($projectmodel === null)
                throw new CHttpException(404, 'The requested page does not exist.');
            $completion_date = $projectmodel->completion_date;
            $types = $list = Yii::app()->db->createCommand("SELECT DISTINCT {$tblpx}work_type.wtid,{$tblpx}work_type.work_type FROM {$tblpx}work_type LEFT JOIN {$tblpx}project_work_type ON {$tblpx}project_work_type.wrk_type_id = {$tblpx}work_type.wtid WHERE {$tblpx}project_work_type.project_id = '" . $proid . "'")->queryAll();
            if ($list != NULL) {
                $worktype_list = "<option >Select Type</option>";

                // each $row is an array representing a row of data
                foreach ($list as $type)
                    $worktype_list .= CHtml::tag('option', array('value' => $type['wtid']), CHtml::encode($type['work_type']), true);
            } else {
                $worktype_list = "<option >No Types</option>";
            }
        }
        $tblpx = Yii::app()->db->tablePrefix;
        $jsonArr = "";
        if (isset($_POST['Dailyreport'])) {

            $model->attributes = $_POST['Dailyreport'];
            if (isset($_POST['work_done_type']) && !empty($_POST['work_done_type'])) {
                foreach ($_POST['work_done_type'] as $key => $value) {
                    if (isset($_POST['work_done_type'][$key]) && isset($_POST['type'][$key]) && isset($_POST['numbers'][$key]) && isset($_POST['work_done'][$key])) {

                        if ((!empty($_POST['type'][$key])) && (!empty($_POST['numbers'][$key])) && (!empty($_POST['work_done'][$key]))) {

                            $jsonArr[] = array('work_done' => $_POST['work_done_type'][$key], 'work_type' => $_POST['type'][$key], 'numbers' => $_POST['numbers'][$key], 'works_done' => $_POST['work_done'][$key]);
                        }
                    }
                }
            }
            if ($jsonArr == "") {
                Yii::app()->user->setFlash('error', "Daily report error!");
            } else {
                $model->wrktype_and_numbers = json_encode($jsonArr);
                $model->works_done = '';
                $model->extra_work_done = '';
                $model->book_date = date('Y-m-d', strtotime($_POST['Dailyreport']['book_date']));
                $model->created_by = isset($model->created_by) ? $model->created_by : Yii::app()->user->id;
                $model->created_date = isset($model->created_date) ? $model->created_date : date('Y-m-d');
                $model->updated_by = Yii::app()->user->id;
                $model->updated_date = date('Y-m-d');
                //$model->company_id = Yii::app()->user->company_id;
                if ($model->save()) {
                    $id = $model->dr_id;
                    Yii::app()->user->setFlash('success', "Daily report saved successfully!");
                    $this->redirect(array('update', 'id' => $id));
                } else {
                    Yii::app()->user->setFlash('error', "Daily report save error!");
                }
            }
        }


        $this->render('dailyreport_mobileform', array(
            'model' => $model, 'worktype_list' => $worktype_list, 'completiondate' => $completion_date,
        ));
    }
    public function actionEstimateBal(){
        $worktype='';
        $worktype=$_POST["worktype"];
        $project =$_POST["project"];
        $estimated_bal_quantity=0;
        $balance='';
        if(!empty([$_POST["worktype"]])){
            
             $sql="SELECT m.labour,m.quantity,m.amount,m.used_quantity,m.used_amount FROM `jp_labour_estimation` m LEFT JOIN `jp_itemestimation` e ON e.itemestimation_id=m.estimation_id WHERE e.project_id=".$project." AND e.itemestimation_status=2  AND m.project_id=".$project ." AND m.labour=".$worktype;
            $estimated_det = Yii::app()->db->createCommand($sql)->queryRow();
            
           
            $estimated_bal_quantity=$estimated_det["quantity"]-$estimated_det["used_quantity"];
            $balance = "[Estimated Bal:".$estimated_bal_quantity."]";
        }
        
        echo json_encode(array(
                'response' => 'success',
                'msg' => $balance
            ));
            return;
    }

    /* added by minu of create daily report for mobile version on 09-07-17 */

    public function actionCreate1()
    {
        $model = new Dailyreport;
        $completion_date = '';
        if (isset($_POST['Dailyreport'])) {

            $model->attributes = $_POST['Dailyreport'];
            $model->book_date = date('Y-m-d', strtotime($_POST['Dailyreport']['book_date']));
            $model->created_by = Yii::app()->user->id;
            $model->created_date = date('Y-m-d');
            if ($model->save()) {
                Yii::app()->user->setFlash('success', "Daily report saved successfully!");
                $this->redirect(array('create'));
            }
        }

        $this->render('create', array(
            'model' => $model,
            'completion_date' => $completion_date,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate1($id)
    {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Dailyreport'])) {
            $model->attributes = $_POST['Dailyreport'];
            $model->book_date = date('Y-m-d', strtotime($_POST['Dailyreport']['book_date']));
            $model->updated_by = Yii::app()->user->id;
            $model->updated_date = date('Y-m-d');
            if ($model->save()) {
                Yii::app()->user->setFlash('success', "Daily report updated successfully!");
                $this->redirect(array('update', 'id' => $id));
            }
        }
        $tblpx = Yii::app()->db->tablePrefix;
        $modelp = Yii::app()->db->createCommand()
            ->select('completion_date')
            ->from($tblpx . 'projects')
            ->where('pid=:pid', array(':pid' => $model->projectid))
            ->queryRow();

        $this->render('update', array(
            'model' => $model, 'completion_date' => $modelp['completion_date'],
        ));
    }

    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);
        $tblpx = Yii::app()->db->tablePrefix;
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Dailyreport'])) {
            $tblpx = Yii::app()->db->tablePrefix;
            $date = date('Y-m-d', strtotime($_POST['Dailyreport']['book_date']));
            $com_date = Yii::app()->db->createCommand("select dr_id from {$tblpx}dailyreport WHERE projectid='" . $_POST['Dailyreport']['projectid'] . "' AND book_date = '" . $date . "'")->queryRow();
            if ($com_date['dr_id'] != '') {
                $model = $this->loadModel($com_date['dr_id']);
            } else {
                $model = new Dailyreport;
            }
            $model->attributes = $_POST['Dailyreport'];
            $jsonArr = array();
            $arr = array();
            $desc = $desc1 = '';
            if (isset($_POST['hdtest'])) {

                $arr = json_decode($_POST['hdtest'], true);
                $desc = isset($arr[0][2]) ? $arr[0][2] : '';
                foreach ($arr as $val) {
                    if (!empty($val)) {
                        if (isset($val[0]) && isset($val[1]) && ($val[1] > 0)) {
                            $wrk_type = Yii::app()->db->createCommand("select `wtid` from " . $tblpx . "work_type where `work_type`='$val[0]'")->queryRow();
                            $jsonArr[] = array('work_done' => 0, 'work_type' => $wrk_type['wtid'], 'numbers' => $val[1], 'works_done' => (isset($val[2]) ? $val[2] : ""), 'wage' => (isset($val[3]) ? $val[3] : ""), 'wage_rate' => (isset($val[4]) ? $val[4] : ""), 'total_amount' => (isset($val[5]) ? $val[5] : ""));
                        }
                    }
                }
            }
            $arr1 = array();
            if (isset($_POST['hdtest1'])) {

                $arr1 = json_decode($_POST['hdtest1'], true);
                $desc1 = isset($arr1[0][2]) ? $arr1[0][2] : '';
                foreach ($arr1 as $val1) {
                    if (!empty($val1)) {
                        if (isset($val1[0]) && isset($val1[1]) && ($val1[1] > 0)) {
                            $wrk_type1 = Yii::app()->db->createCommand("select `wtid` from " . $tblpx . "work_type where `work_type`='$val1[0]'")->queryRow();
                            $jsonArr[] = array('work_done' => 1, 'work_type' => $wrk_type1['wtid'], 'numbers' => $val1[1], 'works_done' => $val1[2], 'wage' => (isset($val1[3]) ? $val1[3] : ""), 'wage_rate' => (isset($val1[4]) ? $val1[4] : ""), 'total_amount' => (isset($val1[5]) ? $val1[5] : ""));
                        }
                    }
                }
            }
            $model->wrktype_and_numbers = json_encode($jsonArr);

            $model->works_done = $desc;
            $model->extra_work_done = $desc1;
            $model->book_date = date('Y-m-d', strtotime($_POST['Dailyreport']['book_date']));
            $model->updated_by = Yii::app()->user->id;
            $model->updated_date = date('Y-m-d');
            if ($model->save()) {
                Yii::app()->user->setFlash('success', "Daily report saved successfully!");
                $this->redirect(array('create'));
            }
        }
        $tblpx = Yii::app()->db->tablePrefix;
        $modelp = Yii::app()->db->createCommand()
            ->select('completion_date')
            ->from($tblpx . 'projects')
            ->where('pid=:pid', array(':pid' => $model->projectid))
            ->queryRow();
        
        $this->render('dailyreport_form', array(
            'model' => $model, 'completion_date' => $modelp['completion_date'],
        ));
    }

    public function actionRefreshButton(){
        $attributes = array('projectid' => null, 'request_from' => 2);
		$labour_Entry = Dailyreport::model()->findAllByAttributes($attributes);
		if (!empty($labour_Entry)) {
			foreach ($labour_Entry as $entry) {
				$acc_project_model = Projects::model()->findByAttributes(array('pms_project_id' => $entry->pms_project_id));
				if ($acc_project_model !== null) {
					$entry->projectid = $acc_project_model->pid;
					;
					$entry->update();
				}
			}
		}
        $attributes = array('labour_template' => null, 'request_from' => 2);
		$labourEntry = Dailyreport::model()->findAllByAttributes($attributes);
        if (!empty($labourEntry)) {
			foreach ($labourEntry as $entry) {
                $labourTemplate=LabourTemplate::model()->findByAttributes(array('pms_labour_template_id' => $entry->pms_labour_template));
                if ($labourTemplate !== null) {
					$entry->labour_template = $labourTemplate->id;
					$entry->update();
				}
            }
        }
        $dailyLabourData = Yii::app()->db->createCommand()
        ->select('*')
        ->from('jp_daily_report_labour_details')
        ->where('labour_id IS NULL OR worktype IS NULL')
        ->queryAll();
        if (!empty($dailyLabourData)) {
            foreach ($dailyLabourData as $labour) {
                $labourData = LabourWorktype::model()->findByAttributes(array('pms_labour_id' => $labour['pms_labour_id']));
                if ($labourData !== null) {
                    $labour['labour_id'] = $labourData->type_id;
                    $labour['worktype'] = $labourData->worktype;
                    // Update 
                    Yii::app()->db->createCommand()
                        ->update('jp_daily_report_labour_details', array(
                            'labour_id' => $labourData->type_id,
                            'worktype' => $labourData->worktype
                        ), 'id = :id', array(':id' => $labour['id']));
                }
            }
        }

        $this->redirect(Yii::app()->createUrl('DailyReport/create'));
        
    }

    public function actionGetCompany()
	{
		try {
			$projectId = $_GET['project_id'];
			// Fetch the Project Model
			$project = Projects::model()->findByPk($projectId);

			if ($project === null) {
				throw new Exception('project is not found');
			}
			// Extract Company IDs
			$companyIds = explode(',', $project->company_id);
			// Fetch Company Details
			$criteria = new CDbCriteria();
			$criteria->addInCondition('id', $companyIds);
			$companies = Company::model()->findAll($criteria);
			$listdata = CHtml::listData($companies, 'id', 'name');
			// Prepare the response data
			echo json_encode(array('status' => 1, 'companies' => $listdata));
		} catch (Exception $e) {
			echo json_encode(array('status' => 0, 'message' => $e->getMessage()));
		}

	}
    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $model      = new Dailyreport('search');
        $tblpx      = Yii::app()->db->tablePrefix;
        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        $newQuery = "";
        foreach ($arrVal as $arr) {
            if ($newQuery) $newQuery .= ' OR';
            $newQuery .= " FIND_IN_SET('" . $arr . "', company_id)";
        }
        if (Yii::app()->user->role == 1 || Yii::app()->user->role == 2) {
            $project = Yii::app()->db->createCommand("SELECT DISTINCT pid,name FROM {$tblpx}projects WHERE (" . $newQuery . ") ORDER BY name")->queryAll();
        } else {
            $project = Yii::app()->db->createCommand("SELECT DISTINCT pid,name FROM {$tblpx}projects WHERE (" . $newQuery . ") AND pid IN(SELECT projectid FROM {$tblpx}project_assign WHERE userid = " . Yii::app()->user->id . ") ORDER BY name")->queryAll();
        }
        $subcontractor  = Yii::app()->db->createCommand("SELECT DISTINCT subcontractor_id,subcontractor_name FROM {$tblpx}subcontractor WHERE (" . $newQuery . ") ORDER BY subcontractor_id")->queryAll();
        $expense  = Yii::app()->db->createCommand("SELECT DISTINCT type_id,type_name FROM {$tblpx}expense_type WHERE (" . $newQuery . ") ORDER BY type_name")->queryAll();

        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['project_id'])) {
            $model->projectid        = $_GET['project_id'];
            $model->company_id       = $_GET['company_id'];
            $model->expensehead_id   = $_GET['expensehead_id'];
            $model->subcontractor_id = $_GET['subcontractor_id'];
            $model->datefrom         = isset($_GET['date_from']) ? $_GET['date_from'] : "";
            $model->dateto           = isset($_GET['date_to']) ? $_GET['date_to'] : "";
        }
        if (isset($_GET['date_from']) && isset($_GET['date_to']) && $_GET['date_to'] != '' && $_GET['date_from'] != '') {
            $datefrom = $_GET['date_from'];
            $dateto = $_GET['date_to'];
        } else {
            $datefrom = date("Y-") . "01-" . "01";
            $dateto = date("Y-m-d");
        }
        $this->render('index', array(
            'model' => $model,
            'dataProvider'     => $model->search(),
            'project'         => $project,
            'subcontractor' => $subcontractor,
            'expense'       => $expense,
            'datefrom'      => $datefrom,
            'dateto'        => $dateto,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin($status='')
{
    $model = new Dailyreport('search');
    $user = Users::model()->findByPk(Yii::app()->user->id);
    $model->unsetAttributes();  // clear any default values
    $tblpx = Yii::app()->db->tablePrefix;
    $unapproved_count='';
    $data  = Yii::app()->db->createCommand("SELECT count(*) as unapproved_count FROM {$tblpx}dailyreport WHERE approve_status='2' ")->queryRow();
    $unapproved_count=$data["unapproved_count"];
    
    if (isset($_GET['Dailyreport'])) {
        $model->attributes = $_GET['Dailyreport'];
    }
    $status = isset($_GET['status']) ? $_GET['status'] : '';
    $dataProvider = $model->search($status);
    $approveDatas = $model->search($status = '1');
    $approvedData = $approveDatas->totalItemCount;

    $nonApproveDatas = $model->search($status = '2');
    $nonApproveData = $nonApproveDatas->totalItemCount;

    $allDatas= $model->search($status='3');
    $allData = $allDatas->totalItemCount;
    $dataProvider->setPagination(array(
        'pageSize' => 10, // Set the number of items per page
    ));

    $this->render('admin', array(
        'model' => $model,
        'allData' => $allData,
        'approvedData' => $approvedData,
        'nonApprovedData' => $nonApproveData,
        'dataProvider' => $dataProvider,
        'user' => $user,
        'status'=>$status,
        'unapprovedCount'=>$unapproved_count
    ));
}

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Dailyreport the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Dailyreport::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Dailyreport $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'dailyreport-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actioncompletiondate()
    {
        $arr = $arr1 = array();
        $expHeadArr = explode('/', $_POST['id']);
        $id = $expHeadArr[0];
        if ($expHeadArr[1] == '') {
            $company_id = '';
        } else {
            $company_id = $expHeadArr[1];
        }

        if ($company_id == '') {
            $newQuery = "";
            $user = Users::model()->findByPk(Yii::app()->user->id);
            $arrVal = explode(',', $user->company_id);
            foreach ($arrVal as $arrs) {
                if ($newQuery) $newQuery .= ' OR';
                $newQuery .= " FIND_IN_SET('" . $arrs . "', company_id)";
            }
        } else {
            $newQuery = " FIND_IN_SET('" . $company_id . "', company_id)";
        }

        $date = date("Y-m-d", strtotime($_POST['date']));
        $completion_date = "";
        $tblpx = Yii::app()->db->tablePrefix;
        $com_date = Yii::app()->db->createCommand("select completion_date from jp_projects WHERE pid=$id AND (" . $newQuery . ")")->queryRow();
        $completion_date = date("d-M-Y", strtotime($com_date['completion_date']));
        $list = Yii::app()->db->createCommand("SELECT DISTINCT {$tblpx}work_type.wtid,{$tblpx}work_type.work_type 
			FROM {$tblpx}work_type 
			LEFT JOIN {$tblpx}project_work_type ON {$tblpx}project_work_type.wrk_type_id = {$tblpx}work_type.wtid 
			WHERE {$tblpx}project_work_type.project_id = '" . $id . "'")->queryAll();
        $rs = array();
        $rs[] = '';
        foreach ($list as $item) {
            //process each item here
            $rs[] = $item['work_type'];
        }

        $tblpx = Yii::app()->db->tablePrefix;
        $query = Yii::app()->db->createCommand("SELECT wrktype_and_numbers,works_done,extra_work_done FROM {$tblpx}dailyreport
				WHERE projectid = '" . $id . "' AND book_date = '" . $date . "' AND (" . $newQuery . ")")->queryAll();
        foreach ($query as $val) {
            $desc = $val['works_done'];
            $extradesc = $val['extra_work_done'];
            $obj = json_decode($val['wrktype_and_numbers']);
            foreach ($obj as $value) {
                if (isset($value->{'works_done'})) {
                    $desc = $value->{'works_done'};
                    $extradesc = $value->{'works_done'};
                }
                $list = Yii::app()->db->createCommand("SELECT DISTINCT work_type FROM {$tblpx}work_type  WHERE wtid = '" . $value->{'work_type'} . "'")->queryRow();
                if ($value->{'work_done'} == 0) {
                    $arr[] = array($list['work_type'], $value->{'numbers'}, $desc, $value->{'wage'}, $value->{'wage_rate'}, $value->{'total_amount'});
                } else {
                    $arr1[] = array($list['work_type'], $value->{'numbers'}, $extradesc, $value->{'wage'}, $value->{'wage_rate'}, $value->{'total_amount'});
                }
            }
        }
        echo json_encode(array('completion_date' => $completion_date, 'worktype' => $rs, 'result' => $arr, 'result1' => $arr1));
    }

    public function actionCompletiondatedaily()
    {
        $arr = $arr1 = array();
        $id = $_POST['id'];
        $date = date("Y-m-d", strtotime($_POST['date']));
        $completion_date = "";
        $tblpx = Yii::app()->db->tablePrefix;
        $com_date = Yii::app()->db->createCommand("select completion_date from jp_projects WHERE pid=$id")->queryRow();
        $completion_date = date("d-M-Y", strtotime($com_date['completion_date']));
        $list = Yii::app()->db->createCommand("SELECT DISTINCT {$tblpx}work_type.wtid,{$tblpx}work_type.work_type FROM {$tblpx}work_type LEFT JOIN {$tblpx}project_work_type ON {$tblpx}project_work_type.wrk_type_id = {$tblpx}work_type.wtid WHERE {$tblpx}project_work_type.project_id = '" . $id . "'")->queryAll();
        if ($list != NULL) {
            $worktype_list = "<option value=''>Select Type</option>";


            foreach ($list as $type)
                $worktype_list .= CHtml::tag('option', array('value' => $type['wtid']), CHtml::encode($type['work_type']), true);
        } else {
            $worktype_list = "<option value=''>No Types</option>";
        }

        echo json_encode(array('completion_date' => $completion_date, 'worktype' => $worktype_list));
    }

    public function actioncompletiondate1()
    {

        $id = $_POST['id'];
        $completion_date = "";
        $com_date = Yii::app()->db->createCommand("select completion_date from jp_projects WHERE pid=$id")->queryRow();
        $completion_date = date("d-M-Y", strtotime($com_date['completion_date']));


        $model = new Dailyreport;
        if (isset($_POST['Dailyreport'])) {

            $model->attributes = $_POST['Dailyreport'];
            $model->book_date = date('Y-m-d', strtotime($_POST['Dailyreport']['book_date']));
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->dr_id));
        }
        echo $completion_date;
    }

    public function actionGetdata()
    {
        $arr = $arr1 = array();
        if (isset($_REQUEST['projectid'])) {
            $tblpx = Yii::app()->db->tablePrefix;
            $list = Yii::app()->db->createCommand("SELECT DISTINCT {$tblpx}work_type.wtid,{$tblpx}work_type.work_type 
			FROM {$tblpx}work_type 
			LEFT JOIN {$tblpx}project_work_type ON {$tblpx}project_work_type.wrk_type_id = {$tblpx}work_type.wtid 
			WHERE {$tblpx}project_work_type.project_id = '" . $_REQUEST['projectid'] . "'")->queryAll();
            $rs = array();
            $rs[] = '';
            foreach ($list as $item) {

                $rs[] = $item['work_type'];
            }
            $query = Yii::app()->db->createCommand("SELECT wrktype_and_numbers,works_done,extra_work_done FROM {$tblpx}dailyreport WHERE projectid = '" . $_REQUEST['projectid'] . "' AND dr_id = '" . $_REQUEST['dr_id'] . "'")->queryAll();
            foreach ($query as $val) {
                $desc = $val['works_done'];
                $extradesc = $val['extra_work_done'];
                $obj = json_decode($val['wrktype_and_numbers']);
                foreach ($obj as $value) {
                    if (isset($value->{'works_done'})) {
                        $desc = $value->{'works_done'};
                        $extradesc = $value->{'works_done'};
                    }
                    $list = Yii::app()->db->createCommand("SELECT DISTINCT work_type FROM {$tblpx}work_type  WHERE wtid = '" . $value->{'work_type'} . "'")->queryRow();
                    if ($value->{'work_done'} == 0) {
                        $arr[] = array($list['work_type'], $value->{'numbers'}, $desc, $value->{'wage'}, $value->{'wage_rate'}, $value->{'total_amount'});
                    } else {
                        $arr1[] = array($list['work_type'], $value->{'numbers'}, $extradesc, $value->{'wage'}, $value->{'wage_rate'}, $value->{'total_amount'});
                    }
                }
            }
            echo json_encode(array('worktype' => $rs, 'result' => $arr, 'result1' => $arr1));
        }
    }
    public function actionRedirecturl()
    {
        $arr = $arr1 = array();

        $expHeadArr = explode('/', $_REQUEST['projectid']);
        $id = $expHeadArr[0];
        if ($expHeadArr[1] == '') {
            $company_id = '';
        } else {
            $company_id = $expHeadArr[1];
        }

        if ($company_id == '') {
            $newQuery = "";
            $user = Users::model()->findByPk(Yii::app()->user->id);
            $arrVal = explode(',', $user->company_id);
            foreach ($arrVal as $arrs) {
                if ($newQuery) $newQuery .= ' OR';
                $newQuery .= " FIND_IN_SET('" . $arrs . "', company_id)";
            }
        } else {
            $newQuery = " FIND_IN_SET('" . $company_id . "', company_id)";
        }

        $userid = $_REQUEST['dailyid'];
        $completion_date = "";
        $date = date("Y-m-d", strtotime($_REQUEST['bookdate']));
        $tblpx = Yii::app()->db->tablePrefix;
        $record = Yii::app()->db->createCommand("select dr_id from {$tblpx}dailyreport WHERE projectid=$id and book_date='" . $date . "' 
				 AND (" . $newQuery . ")")->queryRow();  /* and created_by=$userid */
        $com_date = Yii::app()->db->createCommand("select completion_date from jp_projects WHERE pid=$id  AND (" . $newQuery . ")")->queryRow();
        $record = $record['dr_id'];
        $completion_date = date("d-M-Y", strtotime($com_date['completion_date']));

        $list = Yii::app()->db->createCommand("SELECT DISTINCT {$tblpx}work_type.wtid,{$tblpx}work_type.work_type FROM {$tblpx}work_type LEFT JOIN {$tblpx}project_work_type ON {$tblpx}project_work_type.wrk_type_id = {$tblpx}work_type.wtid WHERE {$tblpx}project_work_type.project_id = '" . $id . "'")->queryAll();
        if ($list != NULL) {
            $worktype_list = "<option value=''>Select Type</option>";
            foreach ($list as $type)
                $worktype_list .= CHtml::tag('option', array('value' => $type['wtid']), CHtml::encode($type['work_type']), true);
        } else {
            $worktype_list = "<option value=''>No Types</option>";
        }
        if ($record != NULL) {
            echo json_encode(array('record' => $record, 'completion_date' => $completion_date, 'worktype_list' => $worktype_list));
        } else {
            echo json_encode(array('record' => 0, 'completion_date' => $completion_date, 'worktype_list' => $worktype_list));
        }
    }

    public function actionSavemobiledata($id = 0, $pid = 0)
    {


        if ($id == 0) {
            $model = new Dailyreport;
            $status = 'empty';
        } else {
            $model = $this->loadModel($id);
            $proid = $model->projectid;
            $projectmodel = Projects::model()->findByPk($proid);
            $status = 'none';
        }

        $tblpx = Yii::app()->db->tablePrefix;
        $jsonArr = "";
        if (isset($_POST['Dailyreport'])) {
            $model->attributes = $_POST['Dailyreport'];
            if (isset($_POST['work_done_type']) && !empty($_POST['work_done_type'])) {
                foreach ($_POST['work_done_type'] as $key => $value) {
                    if (isset($_POST['work_done_type'][$key]) && isset($_POST['type'][$key]) && isset($_POST['numbers'][$key]) && isset($_POST['work_done'][$key])) {

                        if ((!empty($_POST['type'][$key])) && (!empty($_POST['numbers'][$key])) && (!empty($_POST['work_done'][$key]))) {

                            $jsonArr[] = array('work_done' => $_POST['work_done_type'][$key], 'work_type' => $_POST['type'][$key], 'numbers' => $_POST['numbers'][$key], 'works_done' => $_POST['work_done'][$key]);
                        }
                    }
                }
            }
            if ($jsonArr == "") {
                echo json_encode(array('response' => 'fail'));
            } else {
                $model->wrktype_and_numbers = json_encode($jsonArr);
                $model->works_done = '';
                $model->extra_work_done = '';
                $model->book_date = date('Y-m-d', strtotime($_POST['Dailyreport']['book_date']));
                $model->created_by = isset($model->created_by) ? $model->created_by : Yii::app()->user->id;
                $model->created_date = isset($model->created_date) ? $model->created_date : date('Y-m-d');
                $model->updated_by = Yii::app()->user->id;
                $model->updated_date = date('Y-m-d');

                if ($model->save()) {
                    $id = $model->dr_id;
                    echo json_encode(array('response' => 'success', 'id' => $id, 'proid' => $pid, 'status' => $status));
                } else {
                    echo json_encode(array('response' => 'error'));
                }
            }
        }
    }

    public function actionDynamicProject()
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $comId         = $_POST["comId"];
        $tblpx         = Yii::app()->db->tablePrefix;
        $projectData    = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}projects WHERE FIND_IN_SET(" . $comId . ", company_id) ORDER BY name ASC")->queryAll();
        $projectOptions = "<option value=''>-Select Project-</option>";
        foreach ($projectData as $vData) {
            $projectOptions  .= "<option value='" . $vData["pid"] . "'>" . $vData["name"] . "</option>";
        }
        $projectOptions;

        $bankData    = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}subcontractor WHERE FIND_IN_SET(" . $comId . ", company_id) ORDER BY subcontractor_name ASC")->queryAll();
        $sub_option = "<option value=''>-Select Subcontractor-</option>";
        
        foreach ($bankData as $vData) {
            $sub_option  .= "<option value='" . $vData["subcontractor_id"] . "'>" . $vData["subcontractor_name"] . "</option>";
        }
        $sub_option.="<option value='-1'>No SubContractor</option>";
        echo json_encode(array('project' => $projectOptions, 'subcontractor' => $sub_option));
    }

    public function actiongetDynamicScQuotation()
    {
       
        $tblpx = Yii::app()->db->tablePrefix;
        $company_id         = $_POST["company_id"];
        $project_id         = $_POST["project_id"];
        $subcontractor_id         = $_POST["subcontractor_id"];
        $response_no=0;
        $tblpx         = Yii::app()->db->tablePrefix;
        $scQuotNoData    = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}scquotation WHERE project_id='{$project_id}' AND company_id='{$company_id}' AND subcontractor_id='{$subcontractor_id}' AND labour_template_rate_change='0' ORDER BY scquotation_id DESC")->queryAll();
        $scQuotOptions = "<option value=''>-Select SC Quotation No-</option>";
        if(!empty($scQuotNoData)){
            $response_no=1 ;
        }
        foreach ($scQuotNoData as $vData) {
            $scQuotOptions  .= "<option value='" . $vData["scquotation_id"] . "'>" . $vData["scquotation_no"] . "</option>";
        }
       
        echo json_encode(array('scQuotNo' =>  $scQuotOptions,'response_no'=>$response_no));
    }

    public function actiongetTemplateDetails(){
        $type=1;
        $scquotation_id='';
        $project='';
        $template='';
        $templatedt_id='';
        $status=0;
        if(isset($_REQUEST['type'])){
            $type=$_REQUEST['type'];
        }
       
        if(isset($_REQUEST['scquotation_id'])){
            $scquotation_id=$_REQUEST['scquotation_id'];
        }
        if(isset($_REQUEST['template'])){
            $templatedt_id=$_REQUEST['template'];
        }
        if( $type == 1){
            $template_det = Scquotation::Model()->findByPk($scquotation_id);
            if(!empty($template_det)){
                $template= $template_det["labour_template"]; 
            
            }
            
           
            if(!empty($template)){
                echo $this->renderPartial('_sc_quotation_template',array('template_id'=>$template,'type'=>$type,'project'=>$project,'scquotation_id'=>$scquotation_id,'type'=>$type));
            }else{
                echo json_encode($status);
                exit;
            }
            
        }elseif($type==2){
            $template_exist_val='';
            $template_exist = Projects::Model()->findByPk($scquotation_id);
            //die('hi');
            $template_exist_val =$template_exist->labour_template;
            if(!empty($template_exist_val)){
                $project= $scquotation_id;
            
                $template= $templatedt_id; 
                echo $this->renderPartial('_sc_quotation_template',array('template_id'=>$template,'type'=>$type,'project'=>$project,'scquotation_id'=>$scquotation_id,'type'=>$type));
            }else{
                $status=0;
                echo json_encode($status);
            }
                
            
            
          
        }else{
            $type=3;
            $template_det = Projects::Model()->findByPk($scquotation_id);
            $project= $scquotation_id;
            if(!empty($template_det)){
                $template= $template_det["labour_template"]; 
                echo $this->renderPartial('_sc_quotation_template',array('template_id'=>$template,'type'=>$type,'project'=>$project,'scquotation_id'=>$scquotation_id,'type'=>$type));
            
            }
        }
        
   
    }

    public function actionAjaxcall()
    {
        $company_id = $_GET['company_id'];
		$tblpx = Yii::app()->db->tablePrefix;
		$html = '';
		$html['html'] = '';
		$html['status'] = '';
		$html = array('html' => '', 'status' => '');
		$expense_type = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}projects WHERE FIND_IN_SET(" . $company_id . ",company_id)")->queryAll();
		if (!empty($expense_type)) {
			$html['html'] = '<option value="">Select Project</option>';
			foreach ($expense_type as $key => $value) {
				$html['html'] .= '<option value="' . $value['pid'] . '">' . $value['name'] . '</option>';
				$html['status'] = 'success';
			}
		} else {
			$html['status'] = 'no_success';
			$html['html'] .= '<option value="">Select Project</option>';
		}
		echo json_encode($html);
       
    }

    public function actionDynamicVendor()
    {
        $expId = $_POST["prId"];
        $tblpx = Yii::app()->db->tablePrefix;

        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        $newQuery = "";
        foreach ($arrVal as $arr) {
            if ($newQuery) $newQuery .= ' OR';
            $newQuery .= " FIND_IN_SET('" . $arr . "', v.company_id)";
        }

        if ($expId != 0) {
            $vendorData = Yii::app()->db->createCommand("SELECT v.vendor_id as vendorid, v.name as vendorname FROM " . $tblpx . "vendors v LEFT JOIN " . $tblpx . "vendor_exptype vet ON v.vendor_id = vet.vendor_id LEFT JOIN " . $tblpx . "project_exptype pexp ON pexp.type_id=vet.type_id WHERE pexp.project_id = " . $expId . " AND (" . $newQuery . ") GROUP BY v.vendor_id ORDER BY v.name ASC")->queryAll();
        } else {
            $vendorData = Yii::app()->db->createCommand("SELECT v.vendor_id as vendorid, v.name as vendorname FROM " . $tblpx . "vendors v  WHERE (" . $newQuery . ") ORDER BY v.name ASC")->queryAll();
        }
        $vendorOptions = "<option value=''>-Select Vendor-</option>";
        foreach ($vendorData as $vData) {
            $vendorOptions .= "<option value='" . $vData["vendorid"] . "'>" . $vData["vendorname"] . "</option>";
        }
        echo $vendorOptions;
    }

    public function actionGetexpensehead()
    {
        $id = $_REQUEST['id'];
        $tblpx = Yii::app()->db->tablePrefix;
        $company_id = $_REQUEST['company_id'];
        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        $newQuery = "";
        $expheadData="";
        foreach ($arrVal as $arr) {
            if ($newQuery) $newQuery .= ' OR';
            $newQuery .= " FIND_IN_SET('" . $arr . "', sub.company_id)";
        }
        if($id !== '-1'){
            $expheadData = Yii::app()->db->createCommand("SELECT ex.type_id as typeid, ex.type_name FROM " . $tblpx . "expense_type ex LEFT JOIN " . $tblpx . "subcontractor_exptype sbe ON ex.type_id = sbe.type_id LEFT JOIN " . $tblpx . "subcontractor sub ON sub.subcontractor_id=sbe.subcontractor_id WHERE sub.subcontractor_id = " . $id . " AND (" . $newQuery . ") GROUP BY ex.type_id")->queryAll();
        }else{
            if(!empty($company_id)){
                $expheadData = Yii::app()->db->createCommand("SELECT sub.type_id as typeid, sub.type_name FROM " . $tblpx . "expense_type sub WHERE (" . $newQuery . ") GROUP BY sub.type_id")->queryAll();
               
            }
                $pms_api_integration_model = ApiSettings::model()->findByPk(1);
                $pms_api_integration = $pms_api_integration_model->api_integration_settings;
                if ($pms_api_integration == 1) {
                    $expheadData = Yii::app()->db->createCommand("SELECT sub.type_id as typeid, sub.type_name FROM " . $tblpx . "expense_type sub GROUP BY sub.type_id")->queryAll();
                }
        
        }
        $expOptions = "<option value=''>-Select Expense Head-</option>";
        foreach ($expheadData as $eData) {
            $expOptions .= "<option value='" . $eData["typeid"] . "'>" . $eData["type_name"] . "</option>";
        }
        echo $expOptions;
    }

    public function actionGetfields()
    {
        $scquotation_id = $_REQUEST['scquotation_id'];
        $project='';
        $template_detid='';
        $type = $_REQUEST['type'];
        if(isset($_REQUEST['template'])){
            $template_detid=$_REQUEST['template'];
        }
        if($type==1){
            $template_det = Scquotation::Model()->findByPk($scquotation_id);
            if(!empty($template_det)){
                $template= $template_det["labour_template"]; 
                $project=$template_det["project_id"]; 
            }
       
        echo $this->renderPartial('_sc_daily_report_labour_form',array('template_id'=>$template,'type'=>$type,'project'=>$project,'scquotation_id'=>$scquotation_id));
        }elseif($type == 2){
            $template='';
            $project=$scquotation_id;   
            $template=$template_detid;
               
                $project=$scquotation_id; 
               
                echo $this->renderPartial('_project_daily_report_labour_form',array('template_id'=>$template,'type'=>$type,'project'=>$project,'scquotation_id'=>$project));
                return;
            

          
        }
      
   
    }
    public function actiongetNewInputRow(){
        $index = $_POST['index'];
        if($index==''){
            $index==0;
        }
        $labourData = LabourWorktype::model()->findAll();

        $this->renderPartial('_default_labour_details_form', array('labourData' => $labourData, 'index' => $index));

    }
    public function actionGetfieldsForEdit()
    {
        $dr_id = $_REQUEST['dr_id'];
        $project='';
        $template='';
        $type = $_REQUEST['type'];
        $model = Dailyreport::Model()->findByPk($dr_id);
        $labourData = LabourWorktype::model()->findAll();
        if($model->default_labour_type == '1'){
            $labourDetails = Yii::app()->db->createCommand()
            ->select('*')
            ->from('jp_daily_report_labour_details')
            ->where('daily_report_id=:dr_id', array(':dr_id' => $dr_id))
            ->queryAll();

            echo $this->renderPartial('_default_labour_details_form_edit', array(
                'labourDetails' => $labourDetails,
                'type' => $type,
                'dr_id' => $dr_id,
                'labourData' => $labourData,
            ));
        }else{
            $scquotation_id=$model->sc_quot_id;
            $template_det = Scquotation::Model()->findByPk($scquotation_id);
            if(!empty($template_det)){
                $template= $template_det["labour_template"]; 
                $project=$template_det["project_id"]; 
            }
        
        
            echo $this->renderPartial('_sc_daily_report_labour_form',array('template_id'=>$template,'type'=>$type,'project'=>$project,'scquotation_id'=>$scquotation_id,'dr_id' => $dr_id));
        }
        
   
    }

    public function actionAddDailyentry()
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $model = new Dailyreport;

        if (isset($_REQUEST['Dailyreport'])) {
            $user = Users::model()->findByPk(Yii::app()->user->id);
            $arrVal = explode(',', $user->company_id);
            $newQuery = "";
            foreach ($arrVal as $arr) {
                if ($newQuery) $newQuery .= ' OR';
                $newQuery .= " FIND_IN_SET('" . $arr . "', company_id)";
            }
            $project_model = Projects::model()->findByPk($_REQUEST['Dailyreport']['projectid']);
            $company_model = Company::model()->findByPk($_REQUEST['Dailyreport']['company_id']);
            $project_id = $_REQUEST['Dailyreport']['projectid'];


            $model->attributes = $_REQUEST['Dailyreport'];

            $model->date = date('Y-m-d', strtotime($_REQUEST['Dailyreport']['date']));

            if($_REQUEST['Dailyreport']['subcontractor_id'] != '-1'){
                $model->subcontractor_id = $_REQUEST['Dailyreport']['subcontractor_id'];
            }
            if(isset($_REQUEST['Dailyreport']['labour_template_id'])){
                $model->labour_template = $_REQUEST['Dailyreport']['labour_template_id'];
            }
            if(isset($_REQUEST['Dailyreport']['overtime'])){
                $model->overtime = $_REQUEST['Dailyreport']['overtime'];
            }
           
            $model->expensehead_id = $_REQUEST['Dailyreport']['expensehead_id'];
            $model->sc_quot_id= isset($_REQUEST['Dailyreport']['scquotation_id']) ? $_REQUEST['Dailyreport']['scquotation_id']: "";
            $model->amount = isset($_REQUEST['Dailyreport']['amount']) ? $_REQUEST['Dailyreport']['amount'] : "";
            $model->labour = isset($_REQUEST['Dailyreport']['labour']) ? $_REQUEST['Dailyreport']['labour'] : "";
            $model->wage = isset($_REQUEST['Dailyreport']['wage']) ? $_REQUEST['Dailyreport']['wage'] : "";
            $model->wage_rate = isset($_REQUEST['Dailyreport']['wage_rate']) ? $_REQUEST['Dailyreport']['wage_rate'] : "";
            $model->helper = isset($_REQUEST['Dailyreport']['helper']) ? $_REQUEST['Dailyreport']['helper'] : "";
            $model->helper_labour = isset($_REQUEST['Dailyreport']['helper_labour']) ? $_REQUEST['Dailyreport']['helper_labour'] : "";
            $model->lump_sum = isset($_REQUEST['Dailyreport']['lump_sum']) ? $_REQUEST['Dailyreport']['lump_sum'] : "";
            $model->created_by = Yii::app()->user->id;
            $model->labour_wage = isset($_REQUEST['Dailyreport']['labour_wage']) ? $_REQUEST['Dailyreport']['labour_wage'] : "0.00";
            $model->helper_wage = isset($_REQUEST['Dailyreport']['helper_wage']) ? $_REQUEST['Dailyreport']['helper_wage'] : "0.00";
            $model->created_date = date('Y-m-d');
            // if (Yii::app()->user->role == 1) {
            //     $model->approve_status = 1;
            // } else {
            //     $model->approve_status = 2;
            // }
            $model->approve_status = 2;
            if ($model->save()) {
                $lastInsExpId = Yii::app()->db->getLastInsertID();
                $user = Users::model()->findByPk(Yii::app()->user->id);
                $arrVal = explode(',', $user->company_id);
                $newQuery = "";
                foreach ($arrVal as $arr) {
                    if ($newQuery) $newQuery .= ' OR';
                    $newQuery .= " FIND_IN_SET('" . $arr . "', e.company_id)";
                }
                $default_labour_type='';
                $labour_query = [];
                if(isset($_REQUEST['Dailyreport']['subcontractor_id']) && ($_REQUEST['Dailyreport']['subcontractor_id'] !='0')){
                    if (isset($_REQUEST['Dailyreport']['lab_templates'])) {
                        $res = Yii::app()->db->createCommand("SELECT DISTINCT daily_report_id from " . $tblpx . "daily_report_labour_details WHERE daily_report_id  = " . $lastInsExpId)->queryScalar();
                        if ($res) {                        
                            Yii::app()->db->createCommand('delete from ' . $tblpx . 'daily_report_labour_details where daily_report_id=' .  $lastInsExpId )->query();                        
                        }
                        foreach($_REQUEST['Dailyreport']['lab_templates'] as $val){
                            $labour_id=$val["labour_id"];
                             $count=0;
                            $count=isset($val["count"])? $val["count"]:0;
                            $default_labour_type=$val["default_labour_type"];
                            $worktype_det=LabourWorktype::Model()->findByPk($labour_id);
                            $worktype= isset($worktype_det->worktype) ? $worktype_det->worktype:'';
                            $rate = isset($val['labour_amount']) ? floatval(str_replace(',', '', $val['labour_amount'])) : 0;
                            $labour_query[] = '(null, ' . $lastInsExpId . ', ' . $labour_id . ',' . $rate . ',' . $count . ', \'' . $worktype . '\')';
                        }
                        if (!empty($labour_query)) {
                            $insertassign = 'insert into ' . $tblpx . 'daily_report_labour_details(id, daily_report_id, labour_id, labour_total_rate, labour_count, worktype) values ' . implode(',', $labour_query);
                            Yii::app()->db->createCommand($insertassign)->query();
                        }
                    }
                }
                $drmodel = Dailyreport::Model()->findByPk($lastInsExpId);
                $drmodel->default_labour_type=$default_labour_type;
                $drmodel->save();
                $expDate = date('Y-m-d', strtotime($_REQUEST['Dailyreport']['date']));

                $expenseData = Yii::app()->db->createCommand("SELECT e.*, p.name as pname,s.subcontractor_name as subname,exp.type_name FROM " . $tblpx . "dailyreport e
                                LEFT JOIN " . $tblpx . "projects p ON e.projectid = p.pid
                                LEFT JOIN " . $tblpx . "subcontractor s ON e.subcontractor_id = s.subcontractor_id LEFT JOIN " . $tblpx . "expense_type exp on
                                exp.type_id= e.expensehead_id
                                WHERE e.date = '" . $expDate . "' AND (" . $newQuery . ")")->queryAll();
                $client = $this->renderPartial('newlist', array('newmodel' => $expenseData));
                return $client;
            } else {
               // echo "<pre>"; print_r($model->getErrors());exit;
                echo 1;
            }
        }
    }

    public function actionGetAllData()
    {
        $date       = $_REQUEST["date"];
        $tblpx      = Yii::app()->db->tablePrefix;
        $user       = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal     = explode(',', $user->company_id);
        $newQuery   = "";
        foreach ($arrVal as $arr) {
            if ($newQuery) $newQuery .= ' OR';
            $newQuery .= " FIND_IN_SET('" . $arr . "', e.company_id)";
        }

        $expDate        = date('Y-m-d', strtotime($date));
        $expenseData = Yii::app()->db->createCommand("SELECT e.*, p.name as pname,s.subcontractor_name as subname,exp.type_name FROM " . $tblpx . "dailyreport e
                                LEFT JOIN " . $tblpx . "projects p ON e.projectid = p.pid
                                LEFT JOIN " . $tblpx . "subcontractor s ON e.subcontractor_id = s.subcontractor_id LEFT JOIN " . $tblpx . "expense_type exp on
                                exp.type_id= e.expensehead_id
                                WHERE e.date = '" . $expDate . "' AND (" . $newQuery . ")")->queryAll();
        $client         = $this->renderPartial('newlist', array('newmodel' => $expenseData));
        return $client;
    }

    public function actiongetDailyReportDetails()
    {
        $expenseId = $_REQUEST["dailyreport_id"];
        
        $tblpx = Yii::app()->db->tablePrefix;

        $expenseTbl=  "" . $tblpx . "dailyreport"; 
        if(isset($_REQUEST["expStatus"])){
            $expStatus = $_REQUEST["expStatus"];
            if ($expStatus == 0) {
                $expenseTbl = "" . $tblpx . "pre_dailyreport";
            } 
        }
        

        $expenseData = Yii::app()->db->createCommand("SELECT * FROM " . $expenseTbl . " WHERE dr_id = " . $expenseId)->queryRow();

        $result['company_id'] = $expenseData['company_id'];
        $result['projectid'] = $expenseData['projectid'];
        $result['subcontractor_id'] = $expenseData['subcontractor_id'];
        $result['amount'] = $expenseData['amount'];
        $result['expensehead_id'] = $expenseData['expensehead_id'];
        $result['sc_quot_id'] = isset($expenseData['sc_quot_id']) ? $expenseData['sc_quot_id'] : "";
        //$result;
        $quot_no='';
        $scQuotOptions='';
        $expenseHeadOptions='';
        $templateOptions='';
        if(!empty($expenseData['sc_quot_id'])){
            $quot_no_det =  Scquotation::Model()->findByPk($expenseData['sc_quot_id']);
            $quot_no =  $quot_no_det["scquotation_no"];
            $scQuotOptions='';
            $scQuotOptions  .= "<option value='" . $expenseData['sc_quot_id'] . "' selected>" . $quot_no . "</option>";
        }else{
            $expenseHead=$expenseData['expensehead_id'];
            $model_expense = ExpenseType::model()->findByPk($expenseHead);
            $expenseHeadOptions.="<option value='" . $model_expense['type_id'] . "' selected>" .  $model_expense['type_name'] . "</option>";
        }
        if( $expenseData['default_labour_type'] == 2){
           
            $result['labour_template'] = isset($expenseData['labour_template']) ? $expenseData['labour_template'] : "";
            if(!empty($expenseData['labour_template'])){
                $templates = Yii::app()->db->createCommand("SELECT * FROM `jp_labour_template` WHERE `id` = ".$expenseData['labour_template'])->queryRow();
                $templateOptions  .= "<option value='" . $expenseData['labour_template'] . "' selected>" . $templates["template_label"] . "</option>";
                
            }

           
        }

        $result["templateOptions"]=$templateOptions;
        $result["expenseHeadOptions"]=$expenseHeadOptions;
        $result["scQuotOptions"]=$scQuotOptions;
        $result['sc_quot_no']=$quot_no;
        $result['overtime']=$expenseData['overtime'];
        $result['date'] = isset($expenseData['date']) ? date('d-m-Y', strtotime($expenseData['date'])) : "";
       
        $result['description'] = isset($expenseData['description']) ? $expenseData['description'] : "";

        $html='';
        $result['html'] = $html;

        echo json_encode($result);
    }

    public function actionUpdateDailyentry()
    {
        
        $id = $_REQUEST['Dailyreport']['dr_id'];
        $model = Dailyreport::model()->findByPk($id);
        $tblpx = Yii::app()->db->tablePrefix;
        $date = $_REQUEST['Dailyreport']['date'];
        $premodel = new PreDailyreport;
        $created_by         = $model->created_by;
        $created_date       = $model->created_date;
        $model->updated_by = Yii::app()->user->role;
        $model->updated_date = date('Y-m-d', strtotime($date));


        $transaction = Yii::app()->db->beginTransaction();
        try {
            if (isset($_REQUEST['Dailyreport'])) {
              
                if (Yii::app()->user->role == 1) {
                    $model->attributes = $_REQUEST['Dailyreport'];
                    $model->date = date('Y-m-d', strtotime($date));
                    $model->attributes = $_REQUEST['Dailyreport'];
                    $model->date = date('Y-m-d', strtotime($date));
                    if($_REQUEST['Dailyreport']['subcontractor_id'] != '-1'){
                        $model->subcontractor_id = $_REQUEST['Dailyreport']['subcontractor_id'];
                    }
                    if(isset($_REQUEST['Dailyreport']['labour_template_id'])){
                        $model->labour_template = $_REQUEST['Dailyreport']['labour_template_id'];
                    }
                    $pms_api_integration_model=ApiSettings::model()->findByPk(1);
                    $pms_api_integration =$pms_api_integration_model->api_integration_settings;
                    if($pms_api_integration==1){
                        $model->approve_status=2;
                    }
                   
                    $model->expensehead_id = $_REQUEST['Dailyreport']['expensehead_id'];
                    if(isset($_REQUEST['Dailyreport']['overtime'])){
                        $model->overtime = $_REQUEST['Dailyreport']['overtime'];
                    }
           
                    $model->sc_quot_id= isset($_REQUEST['Dailyreport']['scquotation_id']) ? $_REQUEST['Dailyreport']['scquotation_id']: "";
                    $model->amount = isset($_REQUEST['Dailyreport']['amount']) ? $_REQUEST['Dailyreport']['amount'] : "";
                    $model->labour = isset($_REQUEST['Dailyreport']['labour']) ? $_REQUEST['Dailyreport']['labour'] : "";
                    $model->wage = isset($_REQUEST['Dailyreport']['wage']) ? $_REQUEST['Dailyreport']['wage'] : "";
                    $model->wage_rate = isset($_REQUEST['Dailyreport']['wage_rate']) ? $_REQUEST['Dailyreport']['wage_rate'] : "";
                    $model->helper = isset($_REQUEST['Dailyreport']['helper']) ? $_REQUEST['Dailyreport']['helper'] : "";
                    $model->helper_labour = isset($_REQUEST['Dailyreport']['helper_labour']) ? $_REQUEST['Dailyreport']['helper_labour'] : "";
                    $model->lump_sum = isset($_REQUEST['Dailyreport']['lump_sum']) ? $_REQUEST['Dailyreport']['lump_sum'] : "";
                     $labour_query = [];
                     $default_labour_type='';
                if(isset($_REQUEST['Dailyreport']['subcontractor_id']) && ($_REQUEST['Dailyreport']['subcontractor_id'] !='0')){
                    if (isset($_REQUEST['Dailyreport']['lab_templates'])) {
                        $res = Yii::app()->db->createCommand("SELECT DISTINCT daily_report_id from " . $tblpx . "daily_report_labour_details WHERE daily_report_id  = " . $id)->queryScalar();
                        if ($res) {                        
                            Yii::app()->db->createCommand('delete from ' . $tblpx . 'daily_report_labour_details where daily_report_id=' . $id )->query();                        
                        }

                        foreach($_REQUEST['Dailyreport']['lab_templates'] as $val){
                            $labour_id=$val["labour_id"];
                            $count=0;
                            $count=isset($val["count"])? $val["count"]:0;
                            $worktype_det=LabourWorktype::Model()->findByPk($labour_id);
                            $worktype= isset($worktype_det->worktype) ? $worktype_det->worktype:'';
                            $default_labour_type=$val["default_labour_type"];
                            $rate = isset($val['labour_amount']) ? floatval(str_replace(',', '', $val['labour_amount'])) : 0;
                            $labour_query[] = '(null, ' . $id . ', ' . $labour_id . ',' . $rate . ',' . $count . ', \'' . $worktype . '\')';
                        }
                        if (!empty($labour_query)) {
                            $insertassign = 'insert into ' . $tblpx . 'daily_report_labour_details(id, daily_report_id, labour_id, labour_total_rate, labour_count, worktype) values ' . implode(',', $labour_query);
                            Yii::app()->db->createCommand($insertassign)->query();
                        }
                       
                    }
                    $drmodel = Dailyreport::Model()->findByPk($id);
                    $drmodel->default_labour_type=$default_labour_type;
                    $drmodel->save();
                }else{
                    $drmodel = Dailyreport::Model()->findByPk($id);
                    if(isset($_REQUEST['Dailyreport']['labour_template_id'])){
                        $drmodel->labour_template = $_REQUEST['Dailyreport']['labour_template_id'];
                    }
                    if (isset($_REQUEST['Dailyreport']['lab_templates'])) {
                        $res = Yii::app()->db->createCommand("SELECT DISTINCT daily_report_id from " . $tblpx . "daily_report_labour_details WHERE daily_report_id  = " . $id)->queryScalar();
                        if ($res) {                        
                            Yii::app()->db->createCommand('delete from ' . $tblpx . 'daily_report_labour_details where daily_report_id=' . $id )->query();                        
                        }

                        foreach($_REQUEST['Dailyreport']['lab_templates'] as $val){
                            $labour_id=$val["labour_id"];
                            $count=0;
                            $count=isset($val["count"])? $val["count"]:0;
                            $worktype_det=LabourWorktype::Model()->findByPk($labour_id);
                            $worktype= isset($worktype_det->worktype) ? $worktype_det->worktype:'';
                            $default_labour_type=$val["default_labour_type"];
                            $rate = isset($val['labour_amount']) ? floatval(str_replace(',', '', $val['labour_amount'])) : 0;
                            $labour_query[] = '(null, ' . $id . ', ' . $labour_id . ',' . $rate . ',' . $count . ', \'' . $worktype . '\')';
                        }
                        if (!empty($labour_query)) {
                            $insertassign = 'insert into ' . $tblpx . 'daily_report_labour_details(id, daily_report_id, labour_id, labour_total_rate, labour_count, worktype) values ' . implode(',', $labour_query);
                            Yii::app()->db->createCommand($insertassign)->query();
                        }
                       
                    }
                    
                    $drmodel->default_labour_type=$default_labour_type;
                    $drmodel->save();


                }
                if (!$model->save()) {
                    echo '<pre>';print_r($model->getErrors());exit;
                        $success_status = 1;
                        throw new Exception(json_encode($model->getErrors()));
                    } else {
                        $success_status = 2;
                    }

                }else{
                   
                    $model->approval_status = 0;
                    $premodel->attributes = $_REQUEST['Dailyreport'];
                    $premodel->ref_id = $id;
                    $premodel->followup_id = NULL;

                    $sql = "SELECT count(*) as count,MAX(dr_id) as lastId "
                            . " FROM " . $this->tableNameAcc('pre_dailyreport', 0) . " "
                            . " WHERE ref_id='" . $id . "'";

                    $followupdata = Yii::app()->db->createCommand($sql)->queryRow();

                    if ($followupdata['count'] > 0) {
                        $premodel->followup_id = $followupdata['lastId'];
                    } 

                    $premodel->record_grop_id = date('Y-m-d H:i:s');
                    $premodel->approval_status = 0;
                    $premodel->record_action = "update";
                    $premodel->approve_notify_status = 1;
                    $premodel->created_by = $created_by;
                    $premodel->created_date = $created_date;
                    $premodel->updated_by = Yii::app()->user->id;
                    $premodel->updated_date = date('Y-m-d H:i:s');                    
                    $premodel->date = date('Y-m-d', strtotime($date));

                   
                    if (!$model->save()) {
                        $success_status = 1;
                        throw new Exception(json_encode($model->getErrors()));
                    } else {
                        $success_status = 2;
                        if (!$premodel->save()) {
                            $success_status = 1;
                            throw new Exception(json_encode($premodel->getErrors()));
                        } 
                    }
                }
            }
            $transaction->commit();
        }catch(Exception $error){
            $transaction->rollBack();
            $success_status = 1;
            $error = $error->getMessage();
        }finally{
            echo $success_status;
        }
    }

    public function actionGetDataByDate()
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        $newQuery = "";
        foreach ($arrVal as $arr) {
            if ($newQuery) $newQuery .= ' OR';
            $newQuery .= " FIND_IN_SET('" . $arr . "', e.company_id)";
        }

        $newDate = date('Y-m-d', strtotime($_REQUEST["date"]));
        $expenseData = Yii::app()->db->createCommand("SELECT e.*, p.name as pname,s.subcontractor_name as subname,exp.type_name FROM " . $tblpx . "dailyreport e
                        LEFT JOIN " . $tblpx . "projects p ON e.projectid = p.pid
                        LEFT JOIN " . $tblpx . "subcontractor s ON e.subcontractor_id = s.subcontractor_id LEFT JOIN " . $tblpx . "expense_type exp on
                        exp.type_id= e.expensehead_id
                        WHERE e.date = '" . $newDate . "' AND (" . $newQuery . ")")->queryAll();
         $pms_api_integration_model = ApiSettings::model()->findByPk(1);
         $pms_api_integration = $pms_api_integration_model->api_integration_settings;
        if ($pms_api_integration == 1) {
            $expenseData = Yii::app()->db->createCommand("SELECT e.*, p.name as pname,s.subcontractor_name as subname,exp.type_name FROM " . $tblpx . "dailyreport e
                        LEFT JOIN " . $tblpx . "projects p ON e.projectid = p.pid
                        LEFT JOIN " . $tblpx . "subcontractor s ON e.subcontractor_id = s.subcontractor_id LEFT JOIN " . $tblpx . "expense_type exp on
                        exp.type_id= e.expensehead_id
                        WHERE e.date = '" . $newDate . "'")->queryAll();
        }

        $client = $this->renderPartial('newlist', array('newmodel' => $expenseData));

        return $client;
        //echo $newDate;
    }


    public function actiondeletereport()
    {
        $data = Dailyreport::model()->findByPk($_POST['expId']);
        $tblpx = Yii::app()->db->tablePrefix;
        if ($data) {
            $expDate    = date('Y-m-d', strtotime($_REQUEST['expense_date']));
            $newmodel = new JpLog('search');
            $newmodel->log_data = json_encode($data->attributes);
            $newmodel->log_action = 2;
            $newmodel->log_table = $tblpx . "dailyreport";
            $newmodel->log_datetime = date('Y-m-d H:i:s');
            $newmodel->log_action_by = Yii::app()->user->id;
            $user       = Users::model()->findByPk(Yii::app()->user->id);
            $newmodel->company_id = $data->company_id;
            if ($newmodel->save()) {
                $model = Dailyreport::model()->deleteAll(array("condition" => "dr_id=" . $_POST['expId'] . ""));
                $arrVal = explode(',', $user->company_id);
                $newQuery = "";
                foreach ($arrVal as $arr) {
                    if ($newQuery) $newQuery .= ' OR';
                    $newQuery .= " FIND_IN_SET('" . $arr . "', e.company_id)";
                }
                $expenseData = Yii::app()->db->createCommand("SELECT e.*, p.name as pname,s.subcontractor_name as subname,exp.type_name FROM " . $tblpx . "dailyreport e
                    LEFT JOIN " . $tblpx . "projects p ON e.projectid = p.pid
                    LEFT JOIN " . $tblpx . "subcontractor s ON e.subcontractor_id = s.subcontractor_id LEFT JOIN " . $tblpx . "expense_type exp on
                    exp.type_id= e.expensehead_id
                    WHERE e.date = '" . $expDate . "' AND (" . $newQuery . ")")->queryAll();
                $client = $this->renderPartial('newlist', array('newmodel' => $expenseData));
                return $client;
            } else {
                echo 1;
            }
        }
    }

    public function actionDeleteconfirmation()
    {
        $expId      = $_REQUEST["expId"];
        $date       = $_REQUEST["expense_date"];
        $status     = $_REQUEST["status"];
        $delId      = $_REQUEST["delId"];
        $action     = $_REQUEST["action"];
        $tblpx      = Yii::app()->db->tablePrefix;
        $success    = "";
        if ($status == 1) {
            $deletedata = new Deletepending;
            $model      = Dailyreport::model()->findByPk($expId);
            $deletedata->deletepending_table    = $tblpx . "dailyreport";
            $deletedata->deletepending_data     = json_encode($model->attributes);
            $deletedata->deletepending_parentid = $expId;
            $deletedata->user_id                = Yii::app()->user->id;
            $deletedata->requested_date         = date("Y-m-d");
            if ($deletedata->save()) {
                $success = "Yes";
            } else {
                echo 1;
            }
        } else if ($status == 2) {
            $model  = Deletepending::model()->findByPk($delId);
            if ($model->delete()) {
                $success = "Yes";
            } else {
                echo 1;
            }
        }

        if ($success == "Yes") {
            $expDate    = date('Y-m-d', strtotime($_REQUEST['expense_date']));
            $user       = Users::model()->findByPk(Yii::app()->user->id);
            $arrVal     = explode(',', $user->company_id);
            $newQuery   = "";
            foreach ($arrVal as $arr) {
                if ($newQuery) $newQuery .= ' OR';
                $newQuery .= " FIND_IN_SET('" . $arr . "', e.company_id)";
            }
            $expenseData = Yii::app()->db->createCommand("SELECT e.*, p.name as pname,s.subcontractor_name as subname,exp.type_name FROM " . $tblpx . "dailyreport e
                    LEFT JOIN " . $tblpx . "projects p ON e.projectid = p.pid
                    LEFT JOIN " . $tblpx . "subcontractor s ON e.subcontractor_id = s.subcontractor_id LEFT JOIN " . $tblpx . "expense_type exp on
                    exp.type_id= e.expensehead_id
                    WHERE e.date = '" . $expDate . "' AND (" . $newQuery . ")")->queryAll();
            $client = $this->renderPartial('newlist', array('newmodel' => $expenseData));
            return $client;
            $client         = $this->renderPartial('newlist', array('newmodel' => $expenseData));
            return $client;
        }
    }

    public function actionSavetopdf($expense_head, $sub_contractor, $datefrom, $dateto)
    {
        $model      = new Dailyreport('search');
        $tblpx      = Yii::app()->db->tablePrefix;
        $final_array = array();
        $head_array = array();
        $new_query = "";
        $datefrom         = isset($datefrom) ? $datefrom : "";
        $dateto           = isset($dateto) ? $dateto : "";
        $subcontractor_id = "";
        $expensehead_id = "";
        if (isset($expense_head) && isset($expense_head)) {
            if ($expense_head != '' && $expense_head != '') {
                $expensehead_id   = $expense_head;
                $subcontractor_id = $sub_contractor;
                if (!empty($datefrom) && empty($dateto)) {
                    $new_query .= " AND e.date >= '" . date('Y-m-d', strtotime($datefrom)) . "'";
                } elseif (!empty($dateto) && empty($datefrom)) {
                    $new_query .= " AND e.date <= '" . date('Y-m-d', strtotime($dateto)) . "'";
                } elseif (!empty($dateto) && !empty($datefrom)) {
                    $new_query .= " AND e.date between '" . date('Y-m-d', strtotime($datefrom)) . "' and  '" . date('Y-m-d', strtotime($dateto)) . "'";
                }
                $result = Yii::app()->db->createCommand("SELECT e.*, p.name as pname,s.subcontractor_name as subname,exp.type_name FROM " . $tblpx . "dailyreport e
                    LEFT JOIN " . $tblpx . "projects p ON e.projectid = p.pid
                    LEFT JOIN " . $tblpx . "subcontractor s ON e.subcontractor_id = s.subcontractor_id LEFT JOIN " . $tblpx . "expense_type exp on
                    exp.type_id= e.expensehead_id
                    WHERE 1=1 AND e.expensehead_id = " . $expense_head . " AND e.subcontractor_id =" . $sub_contractor . " " . $new_query . " AND e.approve_status =1  group by e.projectid")->queryAll();

                foreach ($result as $key => $data) {
                    $project = Projects::model()->findByPk($data['projectid']);
                    $final_array[$key]['project'] = $project['name'];
                    $payment_deatils = $model->getdailyreportDetails($sub_contractor, $expense_head, $data['projectid'], $datefrom, $dateto);
                    $total_amount = 0;
                    foreach ($payment_deatils as $key2 => $values) {
                        $total_amount += $values['amount'];
                    }
                    $final_array[$key]['total_amount'] = $total_amount;
                    $final_array[$key]['expense_head'] = $expense_head;
                    foreach ($payment_deatils as $key2 => $values) {
                        $final_array[$key]['payment'][$key2]['date'] = $values['date'];
                        $final_array[$key]['payment'][$key2]['amount'] = $values['amount'];
                        $model2 = ExpenseType::model()->findByPk($expense_head);

                        if ($model2->labour_label != '' && $model2->labour_status == 1) {
                            $final_array[$key]['payment'][$key2]['items'][] = $values['labour'];
                        }
                        if ($model2->wage_label != '' && $model2->wage_status == 1) {
                            $final_array[$key]['payment'][$key2]['items'][] = $values['wage'];
                        }
                        if ($model2->wage_rate_label != '' && $model2->wagerate_status == 1) {
                            $final_array[$key]['payment'][$key2]['items'][] = $values['wage_rate'];
                        }
                        if ($model2->helper_label != '' && $model2->helper_status == 1) {
                            $final_array[$key]['payment'][$key2]['items'][] = $values['helper'];
                        }
                        if ($model2->helper_labour_label != '' && $model2->helperlabour_status == 1) {
                            $final_array[$key]['payment'][$key2]['items'][] = $values['helper_labour'];
                        }
                        if ($model2->lump_sum_label != '' && $model2->lump_sum_status == 1) {
                            $final_array[$key]['payment'][$key2]['items'][] = $values['lump_sum'];
                        }
                    }
                }
            }
        }

        if (!empty($final_array)) {
            foreach ($final_array as $key => $value) {
                $model = ExpenseType::model()->findByPk($value['expense_head']);
                $html = "";
                if ($key == 0) {
                    if ($model->labour_label != '' && $model->labour_status == 1) {
                        $head_array[] = $model->labour_label;
                    }
                    if ($model->wage_label != '' && $model->wage_status == 1) {
                        $head_array[] = $model->wage_label;
                    }
                    if ($model->wage_rate_label != '' && $model->wagerate_status == 1) {
                        $head_array[] = $model->wage_rate_label;
                    }
                    if ($model->helper_label != '' && $model->helper_status == 1) {
                        $head_array[] = $model->helper_label;
                    }
                    if ($model->helper_labour_label != '' && $model->helperlabour_status == 1) {
                        $head_array[] =  $model->helper_labour_label;
                    }
                    if ($model->lump_sum_label != '' && $model->lump_sum_status == 1) {
                        $head_array[] = $model->lump_sum_label;
                    }
                }
            }
        }

        $mPDF1 = Yii::app()->ePdf->mPDF();
        # You can easily override default constructor's params
        $mPDF1 = Yii::app()->ePdf->mPDF('', 'A4');
        $mPDF1->WriteHTML($stylesheet, 1);

        $mPDF1->WriteHTML($this->renderPartial('downloadpdf', array(
            'model' => $model,
            'final_array' => $final_array,
            'head_array' => $head_array
        ), true));
        $filename = "Workstatusreport" . $duration;
        $mPDF1->Output($filename . '.pdf', 'D');
    }

    public function actionSavetoexcel($expense_head, $sub_contractor, $datefrom, $dateto)
    {

        $model      = new Dailyreport('search');
        $tblpx      = Yii::app()->db->tablePrefix;
        $final_array = array();
        $head_array = array();
        $new_query = "";
        $datefrom         = isset($datefrom) ? $datefrom : "";
        $dateto           = isset($dateto) ? $dateto : "";
        $subcontractor_id = "";
        $expensehead_id = "";
        if (isset($expense_head) && isset($expense_head)) {
            if ($expense_head != '' && $expense_head != '') {
                $expensehead_id   = $expense_head;
                $subcontractor_id = $sub_contractor;
                if (!empty($datefrom) && empty($dateto)) {
                    $new_query .= " AND e.date >= '" . date('Y-m-d', strtotime($datefrom)) . "'";
                } elseif (!empty($dateto) && empty($datefrom)) {
                    $new_query .= " AND e.date <= '" . date('Y-m-d', strtotime($dateto)) . "'";
                } elseif (!empty($dateto) && !empty($datefrom)) {
                    $new_query .= " AND e.date between '" . date('Y-m-d', strtotime($datefrom)) . "' and  '" . date('Y-m-d', strtotime($dateto)) . "'";
                }
                $result = Yii::app()->db->createCommand("SELECT e.*, p.name as pname,s.subcontractor_name as subname,exp.type_name FROM " . $tblpx . "dailyreport e
                    LEFT JOIN " . $tblpx . "projects p ON e.projectid = p.pid
                    LEFT JOIN " . $tblpx . "subcontractor s ON e.subcontractor_id = s.subcontractor_id LEFT JOIN " . $tblpx . "expense_type exp on
                    exp.type_id= e.expensehead_id
                    WHERE 1=1 AND e.expensehead_id = " . $expense_head . " AND e.subcontractor_id =" . $sub_contractor . " " . $new_query . "  AND e.approve_status =1 group by e.projectid")->queryAll();

                foreach ($result as $key => $data) {
                    $project = Projects::model()->findByPk($data['projectid']);
                    $final_array[$key]['project'] = $project['name'];
                    $payment_deatils = $model->getdailyreportDetails($sub_contractor, $expense_head, $data['projectid'], $datefrom, $dateto);
                    $total_amount = 0;
                    foreach ($payment_deatils as $key2 => $values) {
                        $total_amount += $values['amount'];
                    }
                    $final_array[$key]['total_amount'] = $total_amount;
                    $final_array[$key]['expense_head'] = $expense_head;
                    foreach ($payment_deatils as $key2 => $values) {
                        $final_array[$key]['payment'][$key2]['date'] = $values['date'];
                        $final_array[$key]['payment'][$key2]['amount'] = $values['amount'];
                        $model2 = ExpenseType::model()->findByPk($expense_head);

                        if ($model2->labour_label != '' && $model2->labour_status == 1) {
                            $final_array[$key]['payment'][$key2]['items'][] = $values['labour'];
                        }
                        if ($model2->wage_label != '' && $model2->wage_status == 1) {
                            $final_array[$key]['payment'][$key2]['items'][] = $values['wage'];
                        }
                        if ($model2->wage_rate_label != '' && $model2->wagerate_status == 1) {
                            $final_array[$key]['payment'][$key2]['items'][] = $values['wage_rate'];
                        }
                        if ($model2->helper_label != '' && $model2->helper_status == 1) {
                            $final_array[$key]['payment'][$key2]['items'][] = $values['helper'];
                        }
                        if ($model2->helper_labour_label != '' && $model2->helperlabour_status == 1) {
                            $final_array[$key]['payment'][$key2]['items'][] = $values['helper_labour'];
                        }
                        if ($model2->lump_sum_label != '' && $model2->lump_sum_status == 1) {
                            $final_array[$key]['payment'][$key2]['items'][] = $values['lump_sum'];
                        }
                    }
                }
            }
        }

        if (!empty($final_array)) {
            foreach ($final_array as $key => $value) {
                $model = ExpenseType::model()->findByPk($value['expense_head']);
                $html = "";
                if ($key == 0) {
                    if ($model->labour_label != '' && $model->labour_status == 1) {
                        $head_array[] = $model->labour_label;
                    }
                    if ($model->wage_label != '' && $model->wage_status == 1) {
                        $head_array[] = $model->wage_label;
                    }
                    if ($model->wage_rate_label != '' && $model->wagerate_status == 1) {
                        $head_array[] = $model->wage_rate_label;
                    }
                    if ($model->helper_label != '' && $model->helper_status == 1) {
                        $head_array[] = $model->helper_label;
                    }
                    if ($model->helper_labour_label != '' && $model->helperlabour_status == 1) {
                        $head_array[] =  $model->helper_labour_label;
                    }
                    if ($model->lump_sum_label != '' && $model->lump_sum_status == 1) {
                        $head_array[] = $model->lump_sum_label;
                    }
                }
            }
        }
        $grand_total = 0;
        $arraylabel = array(' ', '', '', '', '', '', ' ', ' ', ' ', ' ', ' ', ' ', ' ');
        $total_amount = 0;

        $finaldata[0][] = 'Sl No';
        $finaldata[0][] = 'Project';
        $finaldata[0][] = '';
        if (!empty($head_array)) {
            foreach ($head_array as $key2 => $head) {
                $finaldata[0][] = $head;
            }
        }
        $finaldata[0][] = 'Total Amount';

        $i = 1;
        $j = 0;
        $k = 0;
        foreach ($final_array as $key => $data) {
            $grand_total += $data['total_amount'];
            $finaldata[$i][] = $key + 1;
            $finaldata[$i][] = $data['project'];
            $finaldata[$i][] = '';
            if (!empty($head_array)) {
                foreach ($head_array as $key2 => $head) {
                    $finaldata[$i][] = '';
                }
            }
            $finaldata[$i][] = Yii::app()->Controller->money_format_inr($data['total_amount'], 2);
            $j = $i + 1;
            if (isset($data['payment']) && !empty($data['payment'])) {
                foreach ($data['payment'] as $key2 => $values) {
                    $finaldata[$j][] = "";
                    $finaldata[$j][] = "";
                    $finaldata[$j][] = $values['date'];
                    // $k=$j;
                    if (!empty($values['items'])) {
                        foreach ($values['items'] as $key3 => $value3) {
                            $finaldata[$j][] = $value3;
                        }
                    }
                    $finaldata[$j][] = Yii::app()->Controller->money_format_inr($values['amount'], 2);
                    $i = $j;
                    $j++;
                }
            }

            $i++;
        }
        $finaldata[$i][] = '';
        $finaldata[$i][] = '';
        $finaldata[$i][] = 'Total';
        if (!empty($head_array)) {
            foreach ($head_array as $key2 => $head) {
                $finaldata[$i][] = '';
            }
        }
        $finaldata[$i][] = Yii::app()->Controller->money_format_inr($grand_total, 2, 1);

        Yii::import('ext.ECSVExport');
        $csv = new ECSVExport($finaldata);
        $csv->setHeaders($arraylabel);
        $csv->setToAppend();
        $contentdata = $csv->toCSV();
        $csvfilename = 'Work status report' . date('d_M_Y') . '.csv';
        Yii::app()->getRequest()->sendFile($csvfilename, $contentdata, "text/csv", false);
        exit();
    }

    public function actionWeeklypayment()
    {
        $model = new Dailyreport;
        $tblpx = Yii::app()->db->tablePrefix;
        $new_query = "";
        $new_query2 = "";
        $new_query3 = "";
        $projectid = "";
        $subcontractor_id = "";
         $datefrom         = isset($_GET['date_from']) ? $_GET['date_from'] : "";
        $dateto           = isset($_GET['date_to']) ? $_GET['date_to'] : "";
        if (empty($datefrom) && empty($dateto)) {
            $current = date('d-m-Y'); // Get the current date in d-m-Y format

            // Get the previous Saturday
            $previousSaturday = date('d-m-Y', strtotime('last Saturday', strtotime($current)));

            // If today is Saturday, we need the current date as Saturday
            if (date('l', strtotime($current)) === 'Saturday') {
                $previousSaturday = $current;
            }

            // Get the upcoming Friday
            $upcomingFriday = date('d-m-Y', strtotime('next Friday', strtotime($current)));

            // If today is Friday, we need the current date as Friday
            if (date('l', strtotime($current)) === 'Friday') {
                $upcomingFriday = $current;
            }

            // Assign values to $datefrom and $dateto
            $datefrom = $previousSaturday;
            $dateto = $upcomingFriday;
        }
        if (!empty($datefrom) && empty($dateto)) {
                $new_query .= " AND e.date >= '" . date('Y-m-d', strtotime($datefrom)) . "'";
                $new_query3 .= " AND date >= '" . date('Y-m-d', strtotime($datefrom)) . "'";
            } elseif (!empty($dateto) && empty($datefrom)) {
                $new_query .= " AND e.date <= '" . date('Y-m-d', strtotime($dateto)) . "'";
                $new_query3 .= " AND date <= '" . date('Y-m-d', strtotime($dateto)) . "'";
            } elseif (!empty($dateto) && !empty($datefrom)) {
                $new_query .= " AND e.date between '" . date('Y-m-d', strtotime($datefrom)) . "' and  '" . date('Y-m-d', strtotime($dateto)) . "'";
                $new_query3 .= " AND date between '" . date('Y-m-d', strtotime($datefrom)) . "' and  '" . date('Y-m-d', strtotime($dateto)) . "'";
            }
            
        if (isset($_GET['subcontractor_id']) || isset($_GET['project_id'])) {
            $projectid        = $_GET['project_id'];
            $subcontractor_id = $_GET['subcontractor_id'];
            $datefrom         = isset($_GET['date_from']) ? $_GET['date_from'] : "";
            $dateto           = isset($_GET['date_to']) ? $_GET['date_to'] : "";
            $model->projectid        = $_GET['project_id'];
            $model->subcontractor_id = $_GET['subcontractor_id'];
            if ($subcontractor_id != '') {
                $new_query .= ' AND e.subcontractor_id =' . $subcontractor_id . '';
                $new_query3 .= ' AND subcontractor_id =' . $subcontractor_id . '';
            }
            if ($projectid != '') {
            }

            if (!empty($datefrom) && empty($dateto)) {
                $new_query .= " AND e.date >= '" . date('Y-m-d', strtotime($datefrom)) . "'";
                $new_query3 .= " AND date >= '" . date('Y-m-d', strtotime($datefrom)) . "'";
            } elseif (!empty($dateto) && empty($datefrom)) {
                $new_query .= " AND e.date <= '" . date('Y-m-d', strtotime($dateto)) . "'";
                $new_query3 .= " AND date <= '" . date('Y-m-d', strtotime($dateto)) . "'";
            } elseif (!empty($dateto) && !empty($datefrom)) {
                $new_query .= " AND e.date between '" . date('Y-m-d', strtotime($datefrom)) . "' and  '" . date('Y-m-d', strtotime($dateto)) . "'";
                $new_query3 .= " AND date between '" . date('Y-m-d', strtotime($datefrom)) . "' and  '" . date('Y-m-d', strtotime($dateto)) . "'";
            }
        }
        
        $result = Yii::app()->db->createCommand("SELECT e.subcontractor_id,e.company_id FROM " . $tblpx . "dailyreport e
            LEFT JOIN " . $tblpx . "projects p ON e.projectid = p.pid
            LEFT JOIN " . $tblpx . "subcontractor s ON e.subcontractor_id = s.subcontractor_id LEFT JOIN " . $tblpx . "expense_type exp on
            exp.type_id= e.expensehead_id
            WHERE 1=1 " . $new_query . " group by e.subcontractor_id, e.date")->queryAll();
        
        $result2 = Yii::app()->db->createCommand("SELECT subcontractor_id,company_id  FROM " . $tblpx . "subcontractor_payment WHERE 1=1 " . $new_query3 . " group by subcontractor_id,date")->queryAll();
       
        $test =  array_merge_recursive($result, $result2);

        $result = array_map("unserialize", array_unique(array_map("serialize", $test)));


        $final_array = array();
        foreach ($result as $key => $data) {
            $project_id = isset($_GET['project_id']) ? $_GET['project_id'] : "";

            $subcontractor = Subcontractor::model()->findByPk($data['subcontractor_id']);
            $payment_deatils1 = $model->getpayment($data['subcontractor_id'], $data['company_id'], $project_id,$new_query);

            $payment_deatils2 = $model->getpayment2($data['subcontractor_id'], $data['company_id'], $project_id,$new_query3);

            $test2 =  array_merge_recursive($payment_deatils1, $payment_deatils2);

            $payment_deatils = array_map("unserialize", array_unique(array_map("serialize", $test2)));

            $payment_total = 0;
            $adv_total = 0;
            $paid_total = 0;
            $unapprove_payment = 0;
            //echo "<pre>";print_r($payment_deatils);exit;
            foreach ($payment_deatils as $key3 => $values) {
                $project = Projects::model()->findByPk($values['projectid']);
                $advance_deatils = DailyReport::model()->getadvpayment($data['subcontractor_id'], $data['company_id'], $values['projectid'],$new_query3);
                $unapprove_deatils = DailyReport::model()->getunapproveexpense($data['subcontractor_id'], $data['company_id'], $values['projectid'],$new_query);

                
                $approve_deatils = DailyReport::model()->getapproveexpense($data['subcontractor_id'], $data['company_id'], $values['projectid'],$new_query);
                
                //echo "<pre>";print_r($approve_ot_deatils);exit;
                $payment_total += $approve_deatils['total'];
                $adv_total += $advance_deatils['total_adv'];
                $paid_total += ( $approve_deatils['total']-$advance_deatils['total_adv'] );
                $unapprove_payment += $unapprove_deatils['total'];
                 $getadvpaymentindetail =DailyReport::model()->getadvpaymentindetailfun($data['subcontractor_id'], $data['company_id'], $values['projectid'],$new_query3);
            }
            $final_array[$key]['subcontractor'] = $subcontractor['subcontractor_name'];
            $final_array[$key]['total_payment'] = $payment_total;
            $final_array[$key]['total_advance'] = $adv_total;
            $final_array[$key]['total_paid']    = $paid_total;
            $final_array[$key]['total_unapprove']    = $unapprove_payment;

            //echo "<pre>";print_r($payment_deatils);exit;
            foreach ($payment_deatils as $key2 => $values) {
                $project = Projects::model()->findByPk($values['projectid']);
                $advance_deatils = DailyReport::model()->getadvpayment($data['subcontractor_id'], $data['company_id'], $values['projectid'],$new_query3);
                //echo "<pre>";print_r($advance_deatils);exit;
                $unapprove_deatils = DailyReport::model()->getunapproveexpense($data['subcontractor_id'], $data['company_id'], $values['projectid'],$new_query);
                $approve_deatils = DailyReport::model()->getapproveexpense($data['subcontractor_id'], $data['company_id'], $values['projectid'],$new_query);

               
               
                $getadvpaymentindetail =DailyReport::model()->getadvpaymentindetailfun($data['subcontractor_id'], $data['company_id'], $values['projectid'],$new_query3);
               // echo "<pre>";print_r($getadvpaymentindetail);exit;
               $advance_details = [];
                if (!empty($getadvpaymentindetail)) {
                    //echo "<pre>";print_r($getadvpaymentindetail);exit;
                    foreach ($getadvpaymentindetail as $detail) {
                        $advance_details[] = [
                            'date' => $detail['date'], // Adjust key if different
                            'amount' => $detail['amount'], // Adjust key if different
                            'description' => $detail['description'], // Adjust key if different
                        ];
                    }
                }
                $approve_reg_labour_count = DailyReport::model()->getapproveexpense_labourcount($data['subcontractor_id'], $data['company_id'], $values['projectid'],$new_query);
                $approve_ot_labour_count = DailyReport::model()->getOTapproveexpense_labourcount($data['subcontractor_id'], $data['company_id'], $values['projectid'],$new_query);
                $unapprove_reg_labour_count = DailyReport::model()->getunapproveexpense_labourcount($data['subcontractor_id'], $data['company_id'], $values['projectid'],$new_query);
                $unapprove_ot_labour_count = DailyReport::model()->getOTunapproveexpense_labourcount($data['subcontractor_id'], $data['company_id'], $values['projectid'],$new_query);
                $approve_labour='';
                 $approve_labour_arr=array();
                if(!empty($approve_reg_labour_count)){
                    $approve_labour_arr = array_column($approve_reg_labour_count,'labour_with_count');
                    $approve_labour= implode(',',$approve_labour_arr);
                   
                }
                //die($approve_labour);
                 $un_approve_labour='';
                 $un_approve_labour_arr=array();
                 if(!empty($unapprove_reg_labour_count)){
                    $un_approve_labour_arr = array_column($unapprove_reg_labour_count,'labour_with_count');
                    $un_approve_labour= implode(',', $un_approve_labour_arr);
                   
                }

                $approve_ot_labour='';
                $approve_ot_labour_arr=array();
                if(!empty($approve_ot_labour_count)){
                    $approve_ot_labour_arr = array_column($approve_ot_labour_count,'labour_with_count');
                    $approve_ot_labour= implode(',',$approve_ot_labour_arr);
                   
                }
                $un_approve_ot_labour='';
                 $un_approve_ot_labour_arr=array();
                 if(!empty($unapprove_ot_labour_count)){
                    $un_approve_ot_labour_arr = array_column($unapprove_ot_labour_count,'labour_with_count');
                    $un_approve_ot_labour= implode(',', $un_approve_ot_labour_arr);
                   
                }
                $final_array[$key]['payment'][$key2]['project'] = $project['name'];
                
                //echo "<pre>";print_r($un_approve_labour);exit;
                $final_array[$key]['payment'][$key2]['payment'] = $approve_deatils['total'];
                $final_array[$key]['payment'][$key2]['description'] = $approve_deatils['description'];
                $final_array[$key]['payment'][$key2]['advance'] = $advance_deatils['total_adv'];
                $final_array[$key]['payment'][$key2]['paid'] = ($approve_deatils['total']-$advance_deatils['total_adv']);
                $final_array[$key]['payment'][$key2]['unapprove'] = $unapprove_deatils['total'];
                $final_array[$key]['payment'][$key2]['approved_reg_headcount'] = $approve_labour;
                $final_array[$key]['payment'][$key2]['approved_ot_headcount'] = $approve_ot_labour;
                $final_array[$key]['payment'][$key2]['unapproved_reg_headcount'] = $un_approve_labour;
                $final_array[$key]['payment'][$key2]['unapproved_ot_headcount'] = $un_approve_ot_labour;
                 $unapprove_reg_deatils = DailyReport::model()->getRegunapproveexpense($data['subcontractor_id'], $data['company_id'], $values['projectid'],$new_query);
                
                $unapprove_ot_deatils = DailyReport::model()->getOTunapproveexpense($data['subcontractor_id'], $data['company_id'], $values['projectid'],$new_query);
                $approve_reg_deatils = DailyReport::model()->getRegapproveexpense($data['subcontractor_id'], $data['company_id'], $values['projectid'],$new_query);
                $approve_ot_deatils = DailyReport::model()->getOTapproveexpense($data['subcontractor_id'], $data['company_id'], $values['projectid'],$new_query);
                $final_array[$key]['payment'][$key2]['approved_reg_expense'] = $approve_reg_deatils['total'];
                $final_array[$key]['payment'][$key2]['approved_ot_expense'] = $approve_ot_deatils['total'];
                $final_array[$key]['payment'][$key2]['unapproved_reg_expense'] =$unapprove_reg_deatils['total'];
                $final_array[$key]['payment'][$key2]['unapproved_ot_expense'] = $unapprove_ot_deatils['total'];
               
                $final_array[$key]['payment'][$key2]['advance_details'] = $advance_details;
            }
        }
        //echo "<pre>";print_r($final_array);exit;
        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        $newQuery = "";
        foreach ($arrVal as $arr) {
            if ($newQuery) $newQuery .= ' OR';
            $newQuery .= " FIND_IN_SET('" . $arr . "', company_id)";
        }
        if (Yii::app()->user->role == 1 || Yii::app()->user->role == 2) {
            $project = Yii::app()->db->createCommand("SELECT DISTINCT pid,name FROM {$tblpx}projects WHERE (" . $newQuery . ") ORDER BY name")->queryAll();
        } else {
            $project = Yii::app()->db->createCommand("SELECT DISTINCT pid,name FROM {$tblpx}projects WHERE (" . $newQuery . ") AND pid IN(SELECT projectid FROM {$tblpx}project_assign WHERE userid = " . Yii::app()->user->id . ") ORDER BY name")->queryAll();
        }
        $subcontractor  = Yii::app()->db->createCommand("SELECT DISTINCT subcontractor_id,subcontractor_name FROM {$tblpx}subcontractor WHERE (" . $newQuery . ") ORDER BY subcontractor_id")->queryAll();
        $expense  = Yii::app()->db->createCommand("SELECT DISTINCT type_id,type_name FROM {$tblpx}expense_type WHERE (" . $newQuery . ") ORDER BY type_name")->queryAll();

        if (isset($_GET['date_from']) && isset($_GET['date_to']) && $_GET['date_to'] != '' && $_GET['date_from'] != '') {
            $datefrom = $_GET['date_from'];
            $dateto = $_GET['date_to'];
        } else {
           
            $current = date('d-m-Y'); // Get the current date in d-m-Y format

            // Get the previous Saturday
            $previousSaturday = date('d-m-Y', strtotime('last Saturday', strtotime($current)));

            // If today is Saturday, we need the current date as Saturday
            if (date('l', strtotime($current)) === 'Saturday') {
                $previousSaturday = $current;
            }

            // Get the upcoming Friday
            $upcomingFriday = date('d-m-Y', strtotime('next Friday', strtotime($current)));

            // If today is Friday, we need the current date as Friday
            if (date('l', strtotime($current)) === 'Friday') {
                $upcomingFriday = $current;
            }

            // Assign values to $datefrom and $dateto
            $datefrom = $previousSaturday;
            $dateto = $upcomingFriday;

        }
        $this->render('weeklyreport', array(
            'model' => $model,
            'dataProvider'     => $model->search(),
            'project'         => $project,
            'subcontractor' => $subcontractor,
            'expense'       => $expense,
            'datefrom'      => $datefrom,
            'dateto'        => $dateto,
            'final_array'   => $final_array,
            'projectid'     => $projectid,
            'subcontractor_id' => $subcontractor_id

        ));
    }

    public function actionweeklypdf($project_id, $subcontractor_id, $datefrom, $dateto)
    {
        $model = new Dailyreport;
        $tblpx = Yii::app()->db->tablePrefix;
        $new_query = "";
        $new_query2 = "";
        $projectid = "";
        $new_query3 = "";
        if (isset($subcontractor_id) || isset($project_id)) {
            $projectid        = $project_id;
            $subcontractor_id = $subcontractor_id;
            $datefrom         = isset($datefrom) ? $datefrom : "";
            $dateto           = isset($dateto) ? $dateto : "";
            $model->projectid        = $project_id;
            $model->subcontractor_id = $subcontractor_id;
            if ($subcontractor_id != '') {
                $new_query .= ' AND e.subcontractor_id =' . $subcontractor_id . '';
                $new_query3 .= ' AND subcontractor_id =' . $subcontractor_id . '';
            }
            if ($projectid != '') {
            }

            if (!empty($datefrom) && empty($dateto)) {
                $new_query .= " AND e.date >= '" . date('Y-m-d', strtotime($datefrom)) . "'";
                $new_query3 .= " AND date >= '" . date('Y-m-d', strtotime($datefrom)) . "'";
            } elseif (!empty($dateto) && empty($datefrom)) {
                $new_query .= " AND e.date <= '" . date('Y-m-d', strtotime($dateto)) . "'";
                $new_query3 .= " AND date <= '" . date('Y-m-d', strtotime($dateto)) . "'";
            } elseif (!empty($dateto) && !empty($datefrom)) {
                $new_query .= " AND e.date between '" . date('Y-m-d', strtotime($datefrom)) . "' and  '" . date('Y-m-d', strtotime($dateto)) . "'";
                $new_query3 .= " AND date between '" . date('Y-m-d', strtotime($datefrom)) . "' and  '" . date('Y-m-d', strtotime($dateto)) . "'";
            }
        }

        $result = Yii::app()->db->createCommand("SELECT e.subcontractor_id,e.company_id FROM " . $tblpx . "dailyreport e
            LEFT JOIN " . $tblpx . "projects p ON e.projectid = p.pid
            LEFT JOIN " . $tblpx . "subcontractor s ON e.subcontractor_id = s.subcontractor_id LEFT JOIN " . $tblpx . "expense_type exp on
            exp.type_id= e.expensehead_id
            WHERE 1=1 " . $new_query . " group by e.subcontractor_id, e.date")->queryAll();

        $result2 = Yii::app()->db->createCommand("SELECT subcontractor_id,company_id  FROM " . $tblpx . "subcontractor_payment WHERE 1=1 " . $new_query3 . " group by subcontractor_id,date")->queryAll();

        $test =  array_merge_recursive($result, $result2);

        $result = array_map("unserialize", array_unique(array_map("serialize", $test)));


        $final_array = array();
        foreach ($result as $key => $data) {
            $project_id = isset($_GET['project_id']) ? $_GET['project_id'] : "";

            $subcontractor = Subcontractor::model()->findByPk($data['subcontractor_id']);
            $payment_deatils1 = $model->getpayment($data['subcontractor_id'], $data['company_id'], $project_id,$new_query);

            $payment_deatils2 = $model->getpayment2($data['subcontractor_id'], $data['company_id'], $project_id,$new_query3);

            $test2 =  array_merge_recursive($payment_deatils1, $payment_deatils2);

            $payment_deatils = array_map("unserialize", array_unique(array_map("serialize", $test2)));

            $payment_total = 0;
            $adv_total = 0;
            $paid_total = 0;
            $unapprove_payment = 0;
            foreach ($payment_deatils as $key3 => $values) {
                $project = Projects::model()->findByPk($values['projectid']);
                $advance_deatils = DailyReport::model()->getadvpayment($data['subcontractor_id'], $data['company_id'], $values['projectid'],$new_query3);
                $unapprove_deatils = DailyReport::model()->getunapproveexpense($data['subcontractor_id'], $data['company_id'], $values['projectid'],$new_query);

                $approve_deatils = DailyReport::model()->getapproveexpense($data['subcontractor_id'], $data['company_id'], $values['projectid'],$new_query);
                $payment_total += $approve_deatils['total'];
                $adv_total += $advance_deatils['total_adv'];
                $paid_total += ($approve_deatils['total']-$advance_deatils['total_adv'] );
                $unapprove_payment += $unapprove_deatils['total'];
            }
            $final_array[$key]['subcontractor'] = $subcontractor['subcontractor_name'];
            $final_array[$key]['total_payment'] = $payment_total;
            $final_array[$key]['total_advance'] = $adv_total;
            $final_array[$key]['total_paid']    = $paid_total;
            $final_array[$key]['total_unapprove']    = $unapprove_payment;

            foreach ($payment_deatils as $key2 => $values) {
                $project = Projects::model()->findByPk($values['projectid']);
                $advance_deatils = DailyReport::model()->getadvpayment($data['subcontractor_id'], $data['company_id'], $values['projectid'],$new_query3);
                $unapprove_deatils = DailyReport::model()->getunapproveexpense($data['subcontractor_id'], $data['company_id'], $values['projectid'],$new_query);
                $approve_deatils = DailyReport::model()->getapproveexpense($data['subcontractor_id'], $data['company_id'], $values['projectid'],$new_query);
                 
                $getadvpaymentindetail =DailyReport::model()->getadvpaymentindetailfun($data['subcontractor_id'], $data['company_id'], $values['projectid'],$new_query3);
               // echo "<pre>";print_r($getadvpaymentindetail);exit;
               $advance_details = [];
                if (!empty($getadvpaymentindetail)) {
                    //echo "<pre>";print_r($getadvpaymentindetail);exit;
                    foreach ($getadvpaymentindetail as $detail) {
                        $advance_details[] = [
                            'date' => $detail['date'], // Adjust key if different
                            'amount' => $detail['amount'], // Adjust key if different
                            'description' => $detail['description'], // Adjust key if different
                        ];
                    }
                }
          
                  $approve_reg_labour_count = DailyReport::model()->getapproveexpense_labourcount($data['subcontractor_id'], $data['company_id'], $values['projectid'],$new_query);
                $approve_ot_labour_count = DailyReport::model()->getOTapproveexpense_labourcount($data['subcontractor_id'], $data['company_id'], $values['projectid'],$new_query);
                $unapprove_reg_labour_count = DailyReport::model()->getunapproveexpense_labourcount($data['subcontractor_id'], $data['company_id'], $values['projectid'],$new_query);
                $unapprove_ot_labour_count = DailyReport::model()->getOTunapproveexpense_labourcount($data['subcontractor_id'], $data['company_id'], $values['projectid'],$new_query);
                $approve_labour='';
                 $approve_labour_arr=array();
                if(!empty($approve_reg_labour_count)){
                    $approve_labour_arr = array_column($approve_reg_labour_count,'labour_with_count');
                    $approve_labour= implode(',',$approve_labour_arr);
                   
                }
                //die($approve_labour);
                 $un_approve_labour='';
                 $un_approve_labour_arr=array();
                 if(!empty($unapprove_reg_labour_count)){
                    $un_approve_labour_arr = array_column($unapprove_reg_labour_count,'labour_with_count');
                    $un_approve_labour= implode(',', $un_approve_labour_arr);
                   
                }

                $approve_ot_labour='';
                $approve_ot_labour_arr=array();
                if(!empty($approve_ot_labour_count)){
                    $approve_ot_labour_arr = array_column($approve_ot_labour_count,'labour_with_count');
                    $approve_ot_labour= implode(',',$approve_ot_labour_arr);
                   
                }
                $un_approve_ot_labour='';
                 $un_approve_ot_labour_arr=array();
                 if(!empty($unapprove_ot_labour_count)){
                    $un_approve_ot_labour_arr = array_column($unapprove_ot_labour_count,'labour_with_count');
                    $un_approve_ot_labour= implode(',', $un_approve_ot_labour_arr);
                   
                }

                $final_array[$key]['payment'][$key2]['project'] = $project['name'];
                $final_array[$key]['payment'][$key2]['payment'] = $approve_deatils['total'];
                $final_array[$key]['payment'][$key2]['advance'] = $advance_deatils['total_adv'];
                $final_array[$key]['payment'][$key2]['paid'] = ( $approve_deatils['total']-$advance_deatils['total_adv'] );
                $final_array[$key]['payment'][$key2]['unapprove'] = $unapprove_deatils['total'];
                $final_array[$key]['payment'][$key2]['approved_reg_headcount'] = $approve_labour;
                $final_array[$key]['payment'][$key2]['approved_ot_headcount'] = $approve_ot_labour;
                $final_array[$key]['payment'][$key2]['unapproved_reg_headcount'] = $un_approve_labour;
                $final_array[$key]['payment'][$key2]['unapproved_ot_headcount'] = $un_approve_ot_labour;
                 $unapprove_reg_deatils = DailyReport::model()->getRegunapproveexpense($data['subcontractor_id'], $data['company_id'], $values['projectid'],$new_query);
                
                $unapprove_ot_deatils = DailyReport::model()->getOTunapproveexpense($data['subcontractor_id'], $data['company_id'], $values['projectid'],$new_query);
                $approve_reg_deatils = DailyReport::model()->getRegapproveexpense($data['subcontractor_id'], $data['company_id'], $values['projectid'],$new_query);
                $approve_ot_deatils = DailyReport::model()->getOTapproveexpense($data['subcontractor_id'], $data['company_id'], $values['projectid'],$new_query);
                $final_array[$key]['payment'][$key2]['approved_reg_expense'] = $approve_reg_deatils['total'];
                $final_array[$key]['payment'][$key2]['approved_ot_expense'] = $approve_ot_deatils['total'];
                $final_array[$key]['payment'][$key2]['unapproved_reg_expense'] =$unapprove_reg_deatils['total'];
                $final_array[$key]['payment'][$key2]['unapproved_ot_expense'] = $unapprove_ot_deatils['total'];
               
                $final_array[$key]['payment'][$key2]['advance_details'] = $advance_details;
         
            }
        }
       // echo "<pre>";print_r($final_array);exit;

      
        $this->logo = $this->realpath_logo;
        $mPDF1 = Yii::app()->ePdf->mPDF();
         $mPDF1 = Yii::app()->ePdf->mPDF('', 'A4-L');
         //  $mPDF1 = Yii::app()->ePdf->mPDF('', 'A4');
       
        $sql = 'SELECT template_name '
                . 'FROM jp_quotation_template '
                . 'WHERE status="1"';
        $selectedtemplate =Yii::app()->db->createCommand($sql)->queryScalar();
        
        $template = $this->getTemplate($selectedtemplate);
        //die($selectedtemplate);
        $mPDF1->SetHTMLHeader($template['header']);
        $mPDF1->SetHTMLFooter($template['footer']);
        //$mPDF1->AddPage('','', '', '', '', 0,0,25, 30, 10,0);
        //$mPDF1->AddPage('', '', '', '', '', 0, 0, 25, 30, -3, 0);
        $mPDF1->AddPage('','', '', '', '', 0,0,50, 40, 10,0); 
        $mPDF1->WriteHTML($this->renderPartial('weeklypdf', array(
            'model' => $model,
            'final_array' => $final_array,
        ), true));
        
        $filename = "Labour_Report" . $duration;
        $mPDF1->Output($filename . '.pdf', 'D');
    }

    public function actionweeklyexcel($project_id, $subcontractor_id, $datefrom, $dateto)
    {
        $model = new Dailyreport;
        $tblpx = Yii::app()->db->tablePrefix;
        $new_query = "";
        $new_query2 = "";
        $projectid = "";
        $new_query3 = "";
        
        if (isset($subcontractor_id) || isset($project_id)) {
            $projectid        = $project_id;
            $subcontractor_id = $subcontractor_id;
            $datefrom         = isset($datefrom) ? $datefrom : "";
            $dateto           = isset($dateto) ? $dateto : "";
            $model->projectid        = $project_id;
            $model->subcontractor_id = $subcontractor_id;
            if ($subcontractor_id != '') {
                $new_query .= ' AND e.subcontractor_id =' . $subcontractor_id . '';
                $new_query3 .= ' AND subcontractor_id =' . $subcontractor_id . '';
            }
            if ($projectid != '') {
            }

            if (!empty($datefrom) && empty($dateto)) {
                $new_query .= " AND e.date >= '" . date('Y-m-d', strtotime($datefrom)) . "'";
                $new_query3 .= " AND date >= '" . date('Y-m-d', strtotime($datefrom)) . "'";
            } elseif (!empty($dateto) && empty($datefrom)) {
                $new_query .= " AND e.date <= '" . date('Y-m-d', strtotime($dateto)) . "'";
                $new_query3 .= " AND date <= '" . date('Y-m-d', strtotime($dateto)) . "'";
            } elseif (!empty($dateto) && !empty($datefrom)) {
                $new_query .= " AND e.date between '" . date('Y-m-d', strtotime($datefrom)) . "' and  '" . date('Y-m-d', strtotime($dateto)) . "'";
                $new_query3 .= " AND date between '" . date('Y-m-d', strtotime($datefrom)) . "' and  '" . date('Y-m-d', strtotime($dateto)) . "'";
            }
        }

        $result = Yii::app()->db->createCommand("SELECT e.subcontractor_id,e.company_id FROM " . $tblpx . "dailyreport e
            LEFT JOIN " . $tblpx . "projects p ON e.projectid = p.pid
            LEFT JOIN " . $tblpx . "subcontractor s ON e.subcontractor_id = s.subcontractor_id LEFT JOIN " . $tblpx . "expense_type exp on
            exp.type_id= e.expensehead_id
            WHERE 1=1 " . $new_query . " group by e.subcontractor_id, e.date")->queryAll();

        $result2 = Yii::app()->db->createCommand("SELECT subcontractor_id,company_id  FROM " . $tblpx . "subcontractor_payment WHERE 1=1 " . $new_query3 . " group by subcontractor_id,date")->queryAll();

        $test =  array_merge_recursive($result, $result2);

        $result = array_map("unserialize", array_unique(array_map("serialize", $test)));


        $final_array = array();
        foreach ($result as $key => $data) {
            $project_id = isset($_GET['project_id']) ? $_GET['project_id'] : "";

            $subcontractor = Subcontractor::model()->findByPk($data['subcontractor_id']);
            $payment_deatils1 = $model->getpayment($data['subcontractor_id'], $data['company_id'], $project_id,$new_query);

            $payment_deatils2 = $model->getpayment2($data['subcontractor_id'], $data['company_id'], $project_id,$new_query3);

            $test2 =  array_merge_recursive($payment_deatils1, $payment_deatils2);

            $payment_deatils = array_map("unserialize", array_unique(array_map("serialize", $test2)));

            $payment_total = 0;
            $adv_total = 0;
            $paid_total = 0;
            $unapprove_payment = 0;
            foreach ($payment_deatils as $key3 => $values) {
                $project = Projects::model()->findByPk($values['projectid']);
                $advance_deatils = DailyReport::model()->getadvpayment($data['subcontractor_id'], $data['company_id'], $values['projectid'],$new_query3);
                $unapprove_deatils = DailyReport::model()->getunapproveexpense($data['subcontractor_id'], $data['company_id'], $values['projectid'],$new_query);

                $approve_deatils = DailyReport::model()->getapproveexpense($data['subcontractor_id'], $data['company_id'], $values['projectid'],$new_query);
                $payment_total += $approve_deatils['total'];
                $adv_total += $advance_deatils['total_adv'];
                $paid_total += ( $approve_deatils['total']-$advance_deatils['total_adv'] );
                $unapprove_payment += $unapprove_deatils['total'];
            }
            $final_array[$key]['subcontractor'] = $subcontractor['subcontractor_name'];
            $final_array[$key]['total_payment'] = $payment_total;
            $final_array[$key]['total_advance'] = $adv_total;
            $final_array[$key]['total_paid']    = $paid_total;
            $final_array[$key]['total_unapprove']    = $unapprove_payment;

            foreach ($payment_deatils as $key2 => $values) {
                $project = Projects::model()->findByPk($values['projectid']);
                $advance_deatils = DailyReport::model()->getadvpayment($data['subcontractor_id'], $data['company_id'], $values['projectid'],$new_query3);
                $unapprove_deatils = DailyReport::model()->getunapproveexpense($data['subcontractor_id'], $data['company_id'], $values['projectid'],$new_query);
                $approve_deatils = DailyReport::model()->getapproveexpense($data['subcontractor_id'], $data['company_id'], $values['projectid'],$new_query);
                $getadvpaymentindetail =DailyReport::model()->getadvpaymentindetailfun($data['subcontractor_id'], $data['company_id'], $values['projectid'],$new_query3);
               // echo "<pre>";print_r($getadvpaymentindetail);exit;
               $advance_details = [];
                if (!empty($getadvpaymentindetail)) {
                    //echo "<pre>";print_r($getadvpaymentindetail);exit;
                    foreach ($getadvpaymentindetail as $detail) {
                        $advance_details[] = [
                            'date' => $detail['date'], // Adjust key if different
                            'amount' => $detail['amount'], // Adjust key if different
                            'description' => $detail['description'], // Adjust key if different
                        ];
                    }
                }
                $approve_reg_labour_count = DailyReport::model()->getapproveexpense_labourcount($data['subcontractor_id'], $data['company_id'], $values['projectid'],$new_query);
                $approve_ot_labour_count = DailyReport::model()->getOTapproveexpense_labourcount($data['subcontractor_id'], $data['company_id'], $values['projectid'],$new_query);
                $unapprove_reg_labour_count = DailyReport::model()->getunapproveexpense_labourcount($data['subcontractor_id'], $data['company_id'], $values['projectid'],$new_query);
                $unapprove_ot_labour_count = DailyReport::model()->getOTunapproveexpense_labourcount($data['subcontractor_id'], $data['company_id'], $values['projectid'],$new_query);
                $approve_labour='';
                 $approve_labour_arr=array();
                if(!empty($approve_reg_labour_count)){
                    $approve_labour_arr = array_column($approve_reg_labour_count,'labour_with_count');
                    $approve_labour= implode(',',$approve_labour_arr);
                   
                }
                //die($approve_labour);
                 $un_approve_labour='';
                 $un_approve_labour_arr=array();
                if(!empty($unapprove_reg_labour_count)){
                    $un_approve_labour_arr = array_column($unapprove_reg_labour_count,'labour_with_count');
                    $un_approve_labour= implode(',', $un_approve_labour_arr);
                   
                }

                $approve_ot_labour='';
                $approve_ot_labour_arr=array();
                if(!empty($approve_ot_labour_count)){
                    $approve_ot_labour_arr = array_column($approve_ot_labour_count,'labour_with_count');
                    $approve_ot_labour= implode(',',$approve_ot_labour_arr);
                   
                }
                $un_approve_ot_labour='';
                 $un_approve_ot_labour_arr=array();
                 if(!empty($unapprove_ot_labour_count)){
                    $un_approve_ot_labour_arr = array_column($unapprove_ot_labour_count,'labour_with_count');
                    $un_approve_ot_labour= implode(',', $un_approve_ot_labour_arr);
                   
                }

                $final_array[$key]['payment'][$key2]['project'] = $project['name'];
                $final_array[$key]['payment'][$key2]['payment'] = $approve_deatils['total'];
                $final_array[$key]['payment'][$key2]['advance'] = $advance_deatils['total_adv'];
                $final_array[$key]['payment'][$key2]['paid'] = ($approve_deatils['total']-$advance_deatils['total_adv']);
                $final_array[$key]['payment'][$key2]['unapprove'] = $unapprove_deatils['total'];
                $final_array[$key]['payment'][$key2]['approved_reg_headcount'] = $approve_labour;
                $final_array[$key]['payment'][$key2]['approved_ot_headcount'] = $approve_ot_labour;
                $final_array[$key]['payment'][$key2]['unapproved_reg_headcount'] = $un_approve_labour;
                $final_array[$key]['payment'][$key2]['unapproved_ot_headcount'] = $un_approve_ot_labour;
                 $unapprove_reg_deatils = DailyReport::model()->getRegunapproveexpense($data['subcontractor_id'], $data['company_id'], $values['projectid'],$new_query);
                
                $unapprove_ot_deatils = DailyReport::model()->getOTunapproveexpense($data['subcontractor_id'], $data['company_id'], $values['projectid'],$new_query);
                $approve_reg_deatils = DailyReport::model()->getRegapproveexpense($data['subcontractor_id'], $data['company_id'], $values['projectid'],$new_query);
                $approve_ot_deatils = DailyReport::model()->getOTapproveexpense($data['subcontractor_id'], $data['company_id'], $values['projectid'],$new_query);
                $final_array[$key]['payment'][$key2]['approved_reg_expense'] = $approve_reg_deatils['total'];
                $final_array[$key]['payment'][$key2]['approved_ot_expense'] = $approve_ot_deatils['total'];
                $final_array[$key]['payment'][$key2]['unapproved_reg_expense'] =$unapprove_reg_deatils['total'];
                $final_array[$key]['payment'][$key2]['unapproved_ot_expense'] = $unapprove_ot_deatils['total'];
               
                $final_array[$key]['payment'][$key2]['advance_details'] = $advance_details;

            }
        }

        $total_amount = 0;
        $i = 0;
        $arraylabel = array('Sl No', 'Subcontractor', 'Project', 'Approved Head Count','REGULAR / OT Expense', 'Total Expense','Paid Amount', 'Payment to be done','Unpproved Head Count','REGULAR / OT Expense', 'Unapprove Expense', ' ', ' ', ' ', ' ', ' ', ' ');
        foreach ($final_array as $key => $data) {
            if ($data['total_advance'] > 0 || $data['total_payment'] > 0 || $data['total_unapprove'] > 0 || $data['total_paid'] > 0) {   
                $total_amount += $data['total_paid'];
                $finaldata[$i][] = $key + 1;
                $finaldata[$i][] = isset($data['subcontractor'])?$data['subcontractor']:'No Subcontractor';
                $finaldata[$i][] = 'Total';
                 $finaldata[$i][]='';
               
               
                $finaldata[$i][]='';
                $finaldata[$i][] = Yii::app()->Controller->money_format_inr($data['total_payment'], 2);
                 $finaldata[$i][] = Yii::app()->Controller->money_format_inr($data['total_advance'], 2);
                $finaldata[$i][] = Yii::app()->Controller->money_format_inr($data['total_paid'], 2);
                $finaldata[$i][]='';
                $finaldata[$i][]='';
            
                $finaldata[$i][] = Yii::app()->Controller->money_format_inr($data['total_unapprove'], 2);
                $j = $i + 1;
                if (isset($data['payment']) && !empty($data['payment'])) {
                    foreach ($data['payment'] as $key => $values) {
                        $finaldata[$j][] = "";
                        $finaldata[$j][] = "";
                        $finaldata[$j][] = $values['project'];
                       
                        $finaldata[$j][] = 
                    (!empty($values['approved_reg_headcount']) 
                        ? 'REG: ' . $values['approved_reg_headcount'] . ' ' 
                        : '') . 
                    (!empty($values['approved_ot_headcount']) 
                        ? 'OT: ' . $values['approved_ot_headcount'] 
                        : '');
                        $finaldata[$j][] = 
                    (!empty($values['approved_reg_expense']) 
                        ? 'REG: ' . Yii::app()->Controller->money_format_inr($values['approved_reg_expense'], 2) . ' ' 
                        : '') . 
                    (!empty($values['approved_ot_expense']) 
                        ? 'OT: ' . Yii::app()->Controller->money_format_inr($values['approved_ot_expense'], 2) 
                        : '');
                                    $finaldata[$j][] = Yii::app()->Controller->money_format_inr($values['payment'], 2);
                                     $finaldata[$j][] = Yii::app()->Controller->money_format_inr($values['advance'], 2);
                            $finaldata[$j][] = Yii::app()->Controller->money_format_inr($values['paid'], 2);
                            $finaldata[$j][] = 
                        (!empty($values['unapproved_reg_headcount']) 
                            ? 'REG: ' . $values['unapproved_reg_headcount'] . ' ' 
                            : '') . 
                        (!empty($values['unapproved_ot_headcount']) 
                            ? 'OT: ' . $values['unapproved_ot_headcount'] 
                            : '');
                            
                            $finaldata[$j][] = 
                        (!empty($values['unapproved_reg_expense']) 
                            ? 'REG: ' . Yii::app()->Controller->money_format_inr($values['unapproved_reg_expense'], 2) . ' ' 
                            : '') . 
                        (!empty($values['unapproved_ot_expense']) 
                            ? 'OT: ' . Yii::app()->Controller->money_format_inr($values['unapproved_ot_expense'], 2) 
                            : '');
                            $finaldata[$j][] = Yii::app()->Controller->money_format_inr($values['unapprove'], 2);
                        $i = $j;
                        $j++;
                    }
                }

                $i++;
            }
        }
        $finaldata[$i][] = '';
        $finaldata[$i][] = '';
        $finaldata[$i][] = '';
        $finaldata[$i][] = '';
        $finaldata[$i][] = '';
        $finaldata[$i][] = 'Total Amount';
        $finaldata[$i][] = Yii::app()->Controller->money_format_inr($total_amount,2);
        $finaldata[$i][] = '';

          $k = $i+2;
        $finaldata[$k][] = '';
        $finaldata[$k][] = '';
        $finaldata[$k][] = '';
        $finaldata[$k][] = '';
        $finaldata[$k][] = '';
        $finaldata[$k][] = 'Advance in Details';
        $finaldata[$k][] = '';
        $finaldata[$k][] = '';
         $c = $k+1;
        $finaldata[$c][] = 'SI No';
        $finaldata[$c][] = 'Subcontractor';
        $finaldata[$c][] = 'Date';
        $finaldata[$c][] = 'Description';
        $finaldata[$c][] = 'Project';
        $finaldata[$c][] = 'Advance Amount';
        
         $advance_count = 1;

        $m=$c+1;
        foreach ($final_array as $key => $data) {
            if ($data['total_advance'] > 0 || $data['total_payment'] > 0 || $data['total_unapprove'] > 0 || $data['total_paid'] > 0) {
                $finaldata[$m][] = $key + 1;
                $finaldata[$m][] = isset($data['subcontractor'])?$data['subcontractor']:'No Subcontractor';
                $finaldata[$m][]='';
                $finaldata[$m][]='';
                $finaldata[$m][] = 'Total';
                $finaldata[$m][] = Yii::app()->Controller->money_format_inr($data['total_advance'], 2);
                        if (isset($data['payment']) && !empty($data['payment'])) {
                            $m++;
                            
                            foreach ($data['payment'] as $payment_key => $values) {
                                $l=0;
                                if (!empty($values['advance_details'])) {
                                    
                                    foreach ($values['advance_details'] as $advance) {
                                        $finaldata[$m][] = $l + 1;
                                        $finaldata[$m][] ='';
                                        $finaldata[$m][] =  date('d-m-Y',strtotime($advance["date"]));
                                        $finaldata[$m][]=$advance['description'];;
                                        $finaldata[$m][]=$values['project'];
                                        
                                        $finaldata[$m][]=isset($data['subcontractor'])? Yii::app()->Controller->money_format_inr($advance['amount'], 2):"0.00";
                                        
                                        $m++;
                                    }
                                }
                            }
                        }
            
                
            

            
            }
        }
   
        Yii::import('ext.ECSVExport');
        $csv = new ECSVExport($finaldata);
        $csv->setHeaders($arraylabel);
        $csv->setToAppend();
        $contentdata = $csv->toCSV();
        $csvfilename = 'labour_report' . date('d_M_Y') . '.csv';
        Yii::app()->getRequest()->sendFile($csvfilename, $contentdata, "text/csv", false);
        exit();
    }

    public function actionapprove()
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $data  = Yii::app()->db->createCommand("SELECT approve_status FROM {$tblpx}dailyreport WHERE dr_id='" . $_POST['item_id'] . "' ")->queryRow();
        $id = $_POST['item_id'];
		$labour = Dailyreport::model()->findByPk($id);
        $project='';
        if(!empty($labour)){
            $project=$labour["projectid"];

        }
        $sql="SELECT * FROM " . $tblpx . "daily_report_labour_details WHERE daily_report_id=".$id;
        $childItems=Yii::app()->db->createCommand($sql)->queryAll();
        if (empty($childItems)) {
            echo json_encode(array(
                'response' => 'error',
                'msg' => 'No items found for this Labour Entry'
            ));
            return;
        }
        $errors = []; 
        foreach ($childItems as $child) {
            if($child["labour_count"] != '0'){
                $labour=$child["labour_id"];
                $sql="SELECT m.id,m.labour,m.quantity,m.amount,m.used_quantity,m.used_amount FROM `jp_labour_estimation` m LEFT JOIN `jp_itemestimation` e ON e.itemestimation_id=m.estimation_id WHERE e.project_id=".$project." AND e.itemestimation_status=2  AND m.project_id=".$project ." AND m.labour=".$child["labour_id"];
                $estimated_det = Yii::app()->db->createCommand($sql)->queryRow();
                $labour_det = LabourWorktype::model()->findByPk($labour);
                $labour_name=$labour_det["worktype"];
                if (!$labour_det) {
                    
                    $errors[] = "Labour with ID $labour_name  not found.";
                    continue;
                }
                $estimated_det = Yii::app()->db->createCommand($sql)->queryRow();

                if (!$estimated_det) {
                    $errors[] = "Estimation details not found or estimation may not be approved for Labour " . $labour_name;
                    continue;
                }
                $available_quantity = $estimated_det["quantity"] - $estimated_det["used_quantity"];
                if (intval($available_quantity) < intval($child['labour_count'])) {
                    $errors[] = "Requested quantity ({$child['labour_count']}) exceeds available estimated quantity ({$available_quantity}) for Labour " . $labour_name;
                }

                // Check if the requested amount exceeds the available estimated amount
                $available_amount = $estimated_det["amount"] - $estimated_det["used_amount"];
                if ($available_amount < $child['labour_total_rate']) {
                    $errors[] = "Requested amount ({$child['labour_total_rate']}) exceeds available estimated amount ({$available_amount}) for Labour " . $labour_name;
                }


            }
            
                       


        }
        if (!empty($errors)) {
            // Combine errors into one message string (or you can return an array)
            $msg = implode("<br/>", $errors);
            echo json_encode(array(
                'response' => 'warning',
                'msg' => $msg,
            ));
            return;
        }

        foreach ($childItems as $child) {
             $labour=$child["labour_id"];
            $sql="SELECT m.id,m.labour,m.quantity,m.amount,m.used_quantity,m.used_amount FROM `jp_labour_estimation` m LEFT JOIN `jp_itemestimation` e ON e.itemestimation_id=m.estimation_id WHERE e.project_id=".$project." AND e.itemestimation_status=2  AND m.project_id=".$project ." AND m.labour=".$child["labour_id"];
            $estimated_det = Yii::app()->db->createCommand($sql)->queryRow();
            $labour_det = LabourWorktype::model()->findByPk($labour);
            $labour_name=$labour_det["worktype"];
            if ($estimated_det) {
                $mt_id = $estimated_det["id"];
                $mtmodel = LabourEstimation::model()->findByPk($mt_id);
                if ($mtmodel) {
                    // Update the used quantity and amount
                    $mtmodel->used_quantity += $child["labour_count"];
                    $mtmodel->used_amount += $child['labour_total_rate'];
                    if (!$mtmodel->save()) {
                        echo json_encode([
                            'response' => 'error', 
                            'msg' => 'Failed to update estimation for labour ' .  $labour_name,
                        ]);
                        return;
                    }
                }
            }


        }



        $includeInReportValue='';

        $pms_api_integration_model = ApiSettings::model()->findByPk(1);
		$pms_api_integration = $pms_api_integration_model->api_integration_settings;
        
        $includeInReportValue=isset($_POST["includeInReportValue"])?$_POST["includeInReportValue"]:0;
        $mail_send = '';
        
        if ($data['approve_status'] == '2') {
            if(!empty($includeInReportValue)){
                if($includeInReportValue==1){
                $update = Yii::app()->db->createCommand("UPDATE {$tblpx}dailyreport SET approve_status = 1 WHERE dr_id = '" . $_POST['item_id'] . "'")->execute();
           
            }else{
                $update = Yii::app()->db->createCommand("UPDATE {$tblpx}dailyreport SET approve_status = 3 WHERE dr_id = '" . $_POST['item_id'] . "'")->execute();
           
            }
             if ($update) {
                //Approve Api
					if ($pms_api_integration == 1) {
                        $request = ['type' => 'labour'];
						$slug = "api/wpr-approve/" . $labour->pms_wpr_id;
						$method = "POST";
						$globalapi_response = GlobalApiCall::callApi($slug, $method, $request);
                    }
                echo json_encode(array('response' => 'success', 'msg' => 'Permission approved Successfully '));
            } else {
                echo json_encode(array('response' => 'error', 'msg' => 'Some problem occured'));
            }

            }
            
        } else {
            echo json_encode(array('response' => 'warning', 'msg' => 'Permission already approved'));
        }
    }

    public function actionViewlabour()
	{
		$id = isset($_GET['id']) ? $_GET['id'] : '';
		$model = Dailyreport::model()->findByPk($id);
		// $modelItems = EquipmentsItems::model()->findAllByAttributes(['equipment_entry_id' => $model->id]);
        $labourEntries = Yii::app()->db->createCommand()
        ->select('*')
        ->from('jp_daily_report_labour_details')
        ->where('daily_report_id = :daily_report_id', array(':daily_report_id' => $model->dr_id))
        ->queryAll();
		if ($model === null) {
            throw new CHttpException(404, 'The requested consumption request does not exist.');
        }
    
        // Pass data to view
        $this->render('daily_report_view', array(
            'model' => $model,
            'labourEntries' => $labourEntries,
        ));
	}
    
    public function actionrejectLabour(){
        if (isset($_POST['id']) && isset($_POST['remarks'])) {
			$id = $_POST['id'];
			$remarks = $_POST['remarks'];

			$labour = Dailyreport::model()->findByPk($id);
			if ($labour) {
				$labour->approve_status = '5';
				$labour->remarks = $remarks;
				$labour->pms_wpr_status='1';

				if ($labour->save()) {
					//Api calling  
					$pms_api_integration_model = ApiSettings::model()->findByPk(1);
					$pms_api_integration = $pms_api_integration_model->api_integration_settings;
					if ($pms_api_integration == 1) {
						// $labours = $labour->laboursItems();
						// $labour_data = [];
						// foreach ($labours as $labourItem) {
						// 	$labour_data[] = $labourItem->pms_template_labour_id;
						// }
						$request = ['type' => 'labour'];
						$slug = "api/wpr-reject/" . $labour->pms_wpr_id;
						$method = "POST";
						$globalapi_response = GlobalApiCall::callApi($slug, $method, $request);
					}
					echo json_encode(array('response' => 'success', 'msg' => 'Labour rejected successfully'));
				} else {
					echo json_encode(array('response' => 'error', 'msg' => 'Failed to reject the labour'));
				}
			} else {
				echo json_encode(array('response' => 'error', 'msg' => 'Labour not found'));
			}
		} else {
			echo json_encode(array('response' => 'error', 'msg' => 'Invalid request'));
		}
    }
    public function actionbulkapproval()
    {
        $selectedIds="";
        $selectedIds=$_POST["ids"];
        foreach($selectedIds as $selectedId){
            $tblpx = Yii::app()->db->tablePrefix;
            $data  = Yii::app()->db->createCommand("SELECT approve_status FROM {$tblpx}dailyreport WHERE dr_id='" . $selectedId . "' ")->queryRow();
            if ($data['approve_status'] == '2') {
                
                $update = Yii::app()->db->createCommand("UPDATE {$tblpx}dailyreport SET approve_status = 1 WHERE dr_id = '" . $selectedId . "'")->execute();
               
            }    
        }
        
        if ($update) {
                echo json_encode(array('response' => 'success', 'msg' => 'Permission approved Successfully '));
        } else {
                echo json_encode(array('response' => 'error', 'msg' => 'Some problem occured'));
        }
        
    }
    public function actionGetProjectTemplate(){
        $tblpx = Yii::app()->db->tablePrefix;
        
        $project_id         = $_GET["scquotation_id"];//project_id
        $templates_arr= [];
        $templateOptions='';
        $response_no=0;
        $exist='';
        $tblpx         = Yii::app()->db->tablePrefix;
        $templateNoData    = Yii::app()->db->createCommand("SELECT *  FROM {$tblpx}projects WHERE pid='{$project_id}'")->queryRow();
        $exist=$templateNoData["labour_template"];
       
        
         $templateOptions = "<option value=''>-Select Project Template-</option>";
       
        if(!empty( $exist)){
            $templates_arr= explode(',',$templateNoData["labour_template"]);
            if(!empty($templates_arr)){
                $response_no=1 ;
            }
            foreach ($templates_arr as $vData) {
                $templates = Yii::app()->db->createCommand("SELECT * FROM `jp_labour_template` WHERE `id` = ".$vData)->queryRow();
                $templateOptions  .= "<option value='" . $templates["id"] . "'>" . $templates["template_label"] . "</option>";
            }
        }
       
        echo json_encode(array('templateOptions' =>  $templateOptions,'response_no'=>$response_no));
    
    }
}
