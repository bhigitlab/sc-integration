<?php
/* @var $this CashbalanceController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Cashbalances',
);

$this->menu=array(
	array('label'=>'Create Cashbalance', 'url'=>array('create')),
	array('label'=>'Manage Cashbalance', 'url'=>array('admin')),
);
?>

<h1>Cashbalances</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
