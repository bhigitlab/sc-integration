<?php

/**
 * This is the model class for table "{{buyer_invoice}}".
 *
 * The followings are the available columns in table '{{buyer_invoice}}':
 * @property integer $buyer_invoice_id
 * @property integer $company_id
 * @property integer $buyer_id
 * @property integer $project_id
 * @property integer $flat_id
 * @property string $date
 * @property string $invoice_no
 * @property double $total_amount
 * @property double $tax_amount
 * @property string $invoice_status
 * @property integer $created_by
 * @property string $created_date
 * @property integer $updated_by
 * @property string $updated_date
 *
 * The followings are the available model relations:
 * @property Buyers $buyer
 * @property Company $company
 * @property Users $createdBy
 * @property ProjectFlatNumbers $flat
 * @property Projects $project
 * @property Users $updatedBy
 * @property BuyerInvoiceList[] $buyerInvoiceLists
 */
class BuyerInvoice extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{buyer_invoice}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('invoice_no', 'required'),
			array('company_id, buyer_id, project_id, flat_id, created_by, updated_by', 'numerical', 'integerOnly' => true),
			array('total_amount, tax_amount', 'numerical'),
			array('invoice_no', 'length', 'max' => 100),
			array('invoice_status', 'length', 'max' => 5),
			array('date, created_date, updated_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('buyer_invoice_id, company_id, buyer_id, project_id, flat_id, date, invoice_no, total_amount, tax_amount, invoice_status, created_by, created_date, updated_by, updated_date', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'buyer' => array(self::BELONGS_TO, 'Buyers', 'buyer_id'),
			'company' => array(self::BELONGS_TO, 'Company', 'company_id'),
			'createdBy' => array(self::BELONGS_TO, 'Users', 'created_by'),
			'flat' => array(self::BELONGS_TO, 'ProjectFlatNumbers', 'flat_id'),
			'project' => array(self::BELONGS_TO, 'Projects', 'project_id'),
			'updatedBy' => array(self::BELONGS_TO, 'Users', 'updated_by'),
			'buyerInvoiceLists' => array(self::HAS_MANY, 'BuyerInvoiceList', 'buyer_inv_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'buyer_invoice_id' => 'Buyer Invoice',
			'company_id' => 'Company',
			'buyer_id' => 'Buyer',
			'project_id' => 'Project',
			'flat_id' => 'Flat',
			'date' => 'Date',
			'invoice_no' => 'Invoice No',
			'total_amount' => 'Total Amount',
			'tax_amount' => 'Tax Amount',
			'invoice_status' => 'Invoice Status',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
			'updated_by' => 'Updated By',
			'updated_date' => 'Updated Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('buyer_invoice_id', $this->buyer_invoice_id);
		$criteria->compare('company_id', $this->company_id);
		$criteria->compare('buyer_id', $this->buyer_id);
		$criteria->compare('project_id', $this->project_id);
		$criteria->compare('flat_id', $this->flat_id);
		$criteria->compare('date', $this->date, true);
		$criteria->compare('invoice_no', $this->invoice_no, true);
		$criteria->compare('total_amount', $this->total_amount);
		$criteria->compare('tax_amount', $this->tax_amount);
		// $criteria->compare('invoice_status',$this->invoice_status,true);
		$criteria->compare('created_by', $this->created_by);
		$criteria->compare('created_date', $this->created_date, true);
		$criteria->compare('updated_by', $this->updated_by);
		$criteria->compare('updated_date', $this->updated_date, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return BuyerInvoice the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}
}
