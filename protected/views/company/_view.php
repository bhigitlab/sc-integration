<?php
/* @var $this ProjectsController */
/* @var $data Projects */
?>

<?php
$tblpx = Yii::app()->db->tablePrefix;
$id = $data->id;
?>

<?php if ($index == 0) { ?>
    <thead class="entry-table">
        <tr>
            <th style="width:20px;">Sl No.</th>
            <th>Name</th>
            <th>Phone</th>
            <th>Email ID</th>
            <th>GST NO</th>
            <th>Tolerance (%) </th>
            <th>Subcontractor Payment Limit (%) </th>
            <th>PO Permission</th>
            <th>Auto Purchase NO</th>
            <th>PO Permission Limit</th>
            <th>Pin code</th>
            <th>Address</th>
            <th>Description</th>
            <th>Logo</th>
            <?php
            if (isset(Yii::app()->user->role) && (in_array('/company/update', Yii::app()->user->menuauthlist))) {
                ?>
                <th>Action</th>
            <?php } ?>
        </tr>
    </thead>
<?php } ?>
<tr>
    <td style="width:20px;">
        <?php echo $index + 1; ?>
    </td>
    <td style="width:20px;">
        <?php echo CHtml::encode($data->name); ?>
    </td>
    <td style="width:20px;">
        <?php echo CHtml::encode($data->phone); ?>
    </td>
    <td style="width:30px;">
        <?php echo CHtml::encode($data->email_id); ?>
    </td>
    <td style="width:30px;">
        <?php echo CHtml::encode($data->company_gstnum); ?>
    </td>
    <td style="width:30px;">
        <?php echo CHtml::encode($data->company_tolerance); ?>
    </td>
    <td style="width:30px;">
        <?php echo CHtml::encode($data->subcontractor_limit); ?>
    </td>
    <td style="width:30px;">
        <?php
        if ($data->company_popermission == 1) {
            echo "Yes";
        } else {
            echo "No";
        }
        ?>
    </td>
    <td style="width:30px" ;>
        <?php
        if ($data->auto_purchaseno == 1) {
            echo "Yes";
        } else {
            echo "No";
        }
        ?>
    </td>
    <td style="width:30px;">
        <?php echo CHtml::encode($data->purchaseorder_limit); ?>
    </td>
    <td style="width:30px;">
        <?php echo CHtml::encode($data->pincode); ?>
    </td>
    <td style="width:30px;">
        <?php echo CHtml::encode($data->address); ?>
    </td>
    <td style="width:30px;">
        <?php echo CHtml::encode($data->description); ?>
    </td>
    <td>
        <?php
        if ($data->logo != '') {
            $path = realpath(Yii::app()->basePath . '/../uploads/image/' . $data->logo);
            if (file_exists($path)) {
                echo CHtml::tag(
                    'img',
                    array(
                        'src' => Yii::app()->request->baseUrl . "/uploads/image/" . $data->logo,
                        'alt' => '',
                        'class' => 'pop',
                        'modal-src' => Yii::app()->request->baseUrl . "/uploads/image/" . $data->logo,
                        'width' => '80px'
                    )
                );
            }
        }
        ?>
    </td>
    <?php
    if (isset(Yii::app()->user->role) && (in_array('/company/update', Yii::app()->user->menuauthlist))) {
        ?>
        <td style="width:40px;">
            <a class="fa fa-edit editVendors" onclick="editcomp(<?php echo $id; ?>)" data-id="<?php echo $id; ?>"></a>
            <span class="delete_company" title="Delete" onclick="deletCompany(<?php echo $id; ?>)">
                <span class="fa fa-trash deleteCompany"></span>
            </span>
        </td>
    <?php } ?>
</tr>