<?php
$user = Users::model()->findByPk(Yii::app()->user->id);
$arrVal = explode(',', $user->company_id);
$newQuery = "";
foreach ($arrVal as $arr) {
  if ($newQuery)
    $newQuery .= ' OR';
  $newQuery .= " FIND_IN_SET('" . $arr . "', company_id)";
}
?>




<div class="page_filter clearfix">
  <?php $form = $this->beginWidget('CActiveForm', array(
    'action' => Yii::app()->createUrl($this->route),
    'method' => 'get',
  )); ?>
  <?php //echo $form->label($model,'Filter By'); ?>
  <div class="row">
    <div class="form-group col-xs-12 col-sm-3 col-md-2">
      <label>Project </label>

      <?php echo $form->dropDownList($model, 'project_id', CHtml::listData(Projects::model()->findAll(array(
        'select' => array('pid, name'),
        'order' => 'name',
        'condition' => '(' . $newQuery . ')',
        'distinct' => true
      )), 'pid', 'name'), array('style' => 'padding: 2.5px 0px; width:100% !important;', 'empty' => 'Select project', 'id' => 'projectid', 'class' => 'form-control'));
      ?>
    </div>
    <div class="form-group col-xs-12 col-sm-3 col-md-2 ">
      <label>Client </label>

      <?php echo $form->dropDownList($model, 'client_id', CHtml::listData(Clients::model()->findAll(array(
        'select' => array('cid, name'),
        'order' => 'name',
        'condition' => '(' . $newQuery . ')',
        'distinct' => true
      )), 'cid', 'name'), array('style' => 'padding: 2.5px 0px; width:100% !important;', 'empty' => 'Select Client', 'id' => 'projectid', 'class' => 'form-control'));
      ?>
    </div>
    <div class="form-group col-xs-12 col-sm-2 col-md-2 ">
      <label class="">From</label>
      <?php echo CHtml::activeTextField($model, 'fromdate', array('placeholder' => 'Date From', 'class' => 'form-control', 'autocomplete' => 'off', 'style' => 'display:inline-block;width: 100% !important;', 'size' => 10, 'value' => isset($_REQUEST['Quotation']['fromdate']) ? $_REQUEST['Quotation']['fromdate'] : '')); ?>
    </div>
    <div class="form-group col-xs-12 col-sm-2 col-md-2">
      <label class="">To</label>
      <?php echo CHtml::activeTextField($model, 'todate', array('placeholder' => 'Date To', 'class' => 'form-control', 'autocomplete' => 'off', 'style' => 'display:inline-block;width: 100% !important;', 'size' => 10, 'value' => isset($_REQUEST['Quotation']['todate']) ? $_REQUEST['Quotation']['todate'] : '')); ?>
    </div>
    <div class="form-group col-xs-12 col-sm-2 col-md-2 text-sm-left text-right">
      <label class="d-sm-block d-none">&nbsp;</label>
      <div>
        <?php echo CHtml::submitButton('Go', array('class' => 'btn btn-sm btn-primary')); ?>
        <?php echo CHtml::resetButton('Clear', array('class' => 'btn btn-sm btn-default', 'onclick' => 'javascript:location.href="' . $this->createUrl('admin') . '"')); ?>
      </div>
    </div>
  </div>
  <?php $this->endWidget(); ?>
</div>


<script>
  $(function () {
    $("#Quotation_fromdate").datepicker({ dateFormat: 'dd-mm-yy' });
    $("#Quotation_todate").datepicker({ dateFormat: 'dd-mm-yy' });
  });
  $("#Quotation_fromdate").change(function () {
    $("#Quotation_todate").datepicker('option', 'minDate', $(this).val());
  });
  $("#Quotation_todate").change(function () {
    $("#Quotation_fromdate").datepicker('option', 'maxDate', $(this).val());
  });
</script>