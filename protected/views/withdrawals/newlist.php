<?php
/* @var $this ExpensesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Withdrawals',
);

?>

<div class="container" id="withdrawal">
<?php



Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('withdrawals-grid', {
		data: $(this).serialize()
	});
	return false;
});

function reinstallDatePicker(id, data) {
        //use the same parameters that you had set in your widget else the datepicker will be refreshed by default
    $('#datepicker_for_date').datepicker(jQuery.extend({showMonthAfterYear:false},jQuery.datepicker.regional['en-GB'],{'dateFormat':'yy-mm-dd'}));
}


calculate_totals();

$(document).ajaxStart(function(){
    //

    var user_select = $('.filters select:first').val();
    var dates_selct = $('.filters input:first').val();
    //
    var amounts_sel = $('.filters input:eq(1)').val();
    var typesin_sel = $('.filters select:eq(1)').val();
    var detailsin_sel = $('.filters select:eq(2)').val();
    var project_sel = $('.filters select:eq(3)').val();
    //

    if(user_select == ''){user_select = 0;}
    if(dates_selct == ''){dates_selct = 0;}
    if(amounts_sel == ''){amounts_sel = 0;}
    if(typesin_sel == ''){typesin_sel = 0;}
    if(detailsin_sel == ''){detailsin_sel = 0;}
    if(project_sel == ''){project_sel = 0;}
    //
    calculate_totals(user_select,dates_selct,amounts_sel,typesin_sel,detailsin_sel,project_sel);
    //        
});

function calculate_totals(user_select,dates_selct,amounts_sel,typesin_sel,detailsin_sel,project_sel){
    //
    $.ajax({
        url: '".$this->createUrl('withdrawals/returncalc')."&user_sel='+user_select+'&date='+dates_selct+'&amount='+amounts_sel+'&type='+typesin_sel+'&detail='+detailsin_sel+'&project='+project_sel,
        dataType: 'html'
    }).success(function(data){
        $('#dataprinthere').html('');
        $('#dataprinthere').append(data);
    });
    //
}



");
?>

<h2>Withdrawals</h2>

<div style="float:right"><?php echo CHtml::link('Settings',array('withdrawals/settings'),array('class'=>'btn del-btn')); ?></div>

<div class="row balance-box">
		<div class="" style="float:left;">
			<span class="box" id="dataprinthere"></span>
		</div>
		
</div>



<?php $this->renderPartial('_newsearch', array('model' => $model)) ?>

<div class="add-btn">
					<button data-toggle="modal" data-target="#addNew" class="createwithdrawal">Add withdrawal</button>
				</div>

<div class="exp-list">
<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_newview',
)); ?>


<!-- Add New Popup -->
		<div id="addNew" class="modal fade" role="dialog">
			
		</div>
		<!-- edit  Popup -->
		<div class="modal fade edit" role="dialog">
			
		</div>
<script>
 $(document).ready(function () {
	 $('.createwithdrawal').click(function () {
		 $.ajax({
               type: "GET",
               
               url: "<?php echo $this->createUrl('withdrawals/create') ?>",
               success: function (response)
               {
                   $("#addNew").html(response);

               }
           });
	 });
	 

	
	
	 $('.editWithdrawal').click(function () {
          // alert("hai");
           var id=$(this).attr('data-id');
       //   alert(id);
          
           $.ajax({
               type: "GET",
               url: "<?php echo $this->createUrl('withdrawals/update&id=') ?>" + id,
               success: function (response)
               {
                   $(".edit").html(response);

               }
           });

       });
});
</script>
		



</div>
</div>

