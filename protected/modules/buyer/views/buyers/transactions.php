<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.js"></script>
<?php
if ($model->isNewRecord) {
    $invoice_no = array();
}
?>
<style type="text/css">
    .select2-selection.select2-selection--single {
        min-width: 100px;
    }

    *:focus {
        border: 1px solid #333;
        box-shadow: 0 0 6px 1px rgba(0, 0, 0, 0.25);
    }

    .btn.addentries:focus,
    .btn.addentries:hover {
        border: 1px solid transparent;
        box-shadow: none;
    }

    .avoid_clicks {
        pointer-events: none;
    }

    .amntblock,
    .expvalue {
        display: none;
    }

    #loader {
        display: none;
    }

    .deleteclass {
        background-color: #efca92;
    }

    .filter_elem.links_hold {
        margin-top: 6px;
    }

    .payment_approval {
        background: #f8cbcb !important;
    }

    .error_block {
        color: red;
        margin-bottom: 10px;
        height: 10px;
    }
</style>
<div class="container" id="expense">
    <div class="expenses-heading">
        <div class="clearfix">
            <?php
            if (isset(Yii::app()->user->role) && (in_array('/buyer/buyers/addTransaction', Yii::app()->user->menuauthlist))) {
            ?>
                <button type="button" class="btn btn-info pull-right addentries collapsed" data-toggle="collapse" data-target="#daybookform"></button>
            <?php } ?>
            <h2>BUYER TRANSACTIONS</h2>
        </div>
    </div>
    <div class="daybook form">
        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'buyer-transaction-form',
            'enableAjaxValidation' => false,
        ));
        echo $form->hiddenField($model, 'id');
        ?>
        <div class="clearfix">
            <div class="datepicker">
                <div class="page_filter clearfix">
                    <div class="filter_elem">
                        <label>Entry Date:</label>
                        <?php echo CHtml::activeTextField($model, 'date', array('class' => 'form-control', 'readonly' => true, 'autocomplete' => 'off', 'value' => date('d-m-Y'), 'onchange' => 'changedate()')); ?>
                    </div>
                    <div class="filter_elem links_hold">
                        <a href="#" id="previous" class="link">Previous</a> |
                        <a href="#" id="current" class="link">Current</a> |
                        <a href="#" id="next" class="link">Next</a>
                    </div>
                    <div class="pull-right">
                        <span id="lastentry">Last Entry date : <span id="entrydate"><?php echo (isset($lastdate['date']) ? date("d-M-Y", strtotime($lastdate['date'])) : '');
                                                                                    ?></span></span>
                    </div>
                </div>
            </div>
        </div>


        <div class="block_hold shdow_box clearfix collapse" id="daybookform">
            <div class="row_block">
                <h4>Transaction Details</h4>
                <div class="elem_block">
                    <div class="form-group">
                        <label for="project">Company</label>
                        <?php
                        echo $form->dropDownList($model, 'company_id', $company_list, array('class' => 'form-control js-example-basic-single', 'empty' => '-Select Company-'));
                        ?>
                        <input type="hidden" id="company_reset_value">
                    </div>
                </div>
                <div class="elem_block">
                    <div class="form-group">
                        <label for="project">Buyer</label>
                        <?php
                        echo $form->dropDownList($model, 'buyer_id', $buyer_list, array('class' => 'form-control js-example-basic-single', 'empty' => '-Select Buyer-'));
                        ?>
                    </div>
                </div>
                <div class="elem_block">
                    <div class="form-group">
                        <label for="project">Project</label>
                        <?php
                        echo $form->dropDownList($model, 'project_id', $buyer_list, array('class' => 'form-control js-example-basic-single', 'empty' => '-Select Project-', 'id' => 'project_id'));
                        ?>
                        <input type="hidden" id="BuyerTransactions_project_id" name="BuyerTransactions[project_id]">
                    </div>
                </div>
                <div class="elem_block">
                    <div class="form-group">
                        <label for="project">Apartment No</label>
                        <?php
                        echo $form->dropDownList($model, 'flat_id', $buyer_list, array('class' => 'form-control js-example-basic-single', 'empty' => '-Select Apartment No-', 'id' => 'flat_id'));
                        ?>
                        <input type="hidden" id="BuyerTransactions_flat_id" name="BuyerTransactions[flat_id]">
                    </div>
                </div>
                <div class="elem_block">
                    <div class="form-group">
                        <label for="project">Advance/Receipt/Expense</label>
                        <?php
                        $transaction_type = array('1' => 'Advance', '2' => 'Receipt', '3' => 'Expense');
                        echo $form->dropDownList($model, 'transaction_for', $transaction_type, array('class' => 'form-control js-example-basic-single', 'empty' => '-Select Transaction Type-'));
                        ?>


                    </div>
                </div>
                <div class="elem_block hide" id="invoice_div">
                    <div class="form-group">
                        <label for="billNumber">Invoice No</label>
                        <?php
                        echo $form->dropDownList($model, 'invoice_no', $invoice_no, array('class' => 'form-control js-example-basic-single', 'empty' => '-Select Invoice No-'));
                        ?>
                    </div>
                </div>

                <div class="elem_block" id="transaction_no_div">
                    <div class="form-group">
                        <label for="transaction_no" id="transaction_no">Transaction No</label>

                        <?php echo $form->textField($model, 'transaction_no', array('class' => 'form-control check', 'readonly' => true)); ?>

                    </div>
                </div>
                <div class="elem_block">
                    <div class="form-group">
                        <label for="transaction_type" id="transaction_type">Transaction Type</label>
                        <?php
                        echo $form->dropDownList($model, 'transaction_type', CHtml::listData(Status::model()->findAll(array(
                            'select' => array('sid,caption'),
                            'order' => 'caption ASC',
                            'condition' => 'status_type =   "payment_type" AND sid IN (88,89)',
                            'distinct' => true,
                        )), 'sid', 'caption'), array('class' => 'form-control js-example-basic-single transaction_type_', 'empty' => '-Select Transaction Type-'));
                        ?>
                    </div>
                </div>
            </div>

            <div class="row_block daybook-inner">
                <h4>Amount Details</h4>
                <div class="elem_block">
                    <div class="form-group">
                        <label for="amount">Amount</label>
                        <?php echo $form->textField($model, 'amount', array('class' => 'form-control check')); ?>
                        <p class="error_block" id="amount_error" style="display:none">test</p>
                    </div>
                </div>
                <div class="elem_block gsts">
                    <div class="form-group">
                        <label for="sgstp">SGST(%)</label>
                        <?php echo $form->textField($model, 'sgst_p', array('class' => 'form-control check percentage')); ?>
                        <span class="" id="sgst_val" name="sgst_val"></span>
                    </div>
                </div>
                <div class="elem_block amntblock">
                    <div class="form-group">
                        <label for="sgst"></label>
                        <input type="hidden" class="form-control" id="BuyerTransactions_sgst_val" name="BuyerTransactions[sgst_val]" readonly="true" />
                    </div>
                </div>
                <div class="elem_block gsts">
                    <div class="form-group">
                        <label for="cgstp">CGST(%)</label>
                        <?php echo $form->textField($model, 'cgst_p', array('class' => 'form-control check percentage')); ?>
                        <span class="" id="cgst_val" name="cgst_val"></span>
                    </div>
                </div>
                <div class="elem_block amntblock">
                    <div class="form-group">
                        <label for="cgst"></label>
                        <input type="hidden" class="form-control" id="BuyerTransactions_cgst_val" name="BuyerTransactions[cgst_val]" readonly="true" />
                    </div>
                </div>
                <div class="elem_block gsts">
                    <div class="form-group">
                        <label for="cgstp">IGST(%)</label>
                        <?php echo $form->textField($model, 'igst_p', array('class' => 'form-control check percentage')); ?>
                        <span class="" id="igst_val" name="igst_val"></span>
                    </div>
                </div>

                <div class="elem_block amntblock">
                    <div class="form-group">
                        <label for="igst"></label>
                        <input type="hidden" class="form-control" id="BuyerTransactions_igst_val" name="BuyerTransactions[igst_val]" readonly="true" />
                    </div>
                </div>
                <div class="elem_block totalamnt">
                    <div class="form-group">
                        <label for="total"></label>
                        <input type="hidden" class="form-control" id="BuyerTransactions_total_amount" name="BuyerTransactions[total_amount]" readonly="true" />

                        <span class="" id="total_gst_amount"></span>

                        <span class="" id="total_transaction_amount" name="total_transaction_amount"></span>
                    </div>
                </div>
            </div>

            <div class="row_block daybook-inner">
                <h4>Payment Details</h4>
                <div class="elem_block areahold">
                    <div class="form-group">
                        <label for="description">Description</label>
                        <textarea class="form-control" id="BuyerTransactions_description" name="BuyerTransactions[description]"></textarea>
                    </div>
                </div>


                <div class="elem_block gsts">
                    <div class="form-group">
                        <label for="tdsp">TDS(%)</label>
                        <input type="text" class="form-control check percentage" id="txtTdsp" name="Expenses[expense_tdsp]" />
                        <span class="gstvalue" id="txtTds1" name="Expenses[expense_tds1]"></span>
                    </div>
                </div>

                <div class="elem_block amntblock">
                    <div class="form-group">
                        <label for="tds"></label>
                        <input type="hidden" class="form-control" id="txtTds" name="Expenses[expense_tds]" readonly="true" />
                    </div>
                </div>
                <div class="elem_block">
                    <div class="form-group">
                        <label for="bank">Bank</label>
                        <?php
                        echo $form->dropDownList($model, 'bank_id', CHtml::listData(Bank::model()->findAll(array(
                            'select' => array('bank_id, bank_name'),
                            'order' => 'bank_name ASC',
                            'distinct' => true,
                        )), 'bank_id', 'bank_name'), array('class' => 'form-control js-example-basic-single', 'empty' => '-Select Bank-'));
                        ?>
                    </div>
                </div>
                <div class="elem_block">
                    <div class="form-group">
                        <label for="cheque">Cheque No</label>
                        <?php echo $form->textField($model, 'cheque_no', array('class' => 'form-control check')); ?>
                    </div>
                </div>
            </div>


            <div class="row_block">
                <div class="elem_block submit-button">
                    <div class="form-group">
                        <label style="display:block;">&nbsp;</label>
                        <button type="button" class="btn btn-info" id="buttonsubmit">ADD</button>
                        <button type="button" class="btn btn-warning" id="btnReset">RESET</button>
                        <div id="loader"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/ajax-loader.gif"></div>
                    </div>
                </div>
            </div>

        </div>
        <?php $this->endWidget(); ?>
    </div>
    <div class="">
        <div class="">
            <div id="errormessage"></div>
        </div>
    </div>
    <div class="">
        <div id="daybook-entry">
            <?php $this->renderPartial('newlist', array('newmodel' => $newmodel,)); ?>
        </div>
    </div>
</div>
<?php $getprojecstUrl = Yii::app()->createAbsoluteUrl("/buyer/default/getProjects"); ?>
<?php $getBuyerList = Yii::app()->createAbsoluteUrl("/buyer/default/getBuyers"); ?>
<?php $getProjectAndFlats = Yii::app()->createAbsoluteUrl("/buyer/default/getProjectAndFlats"); ?>
<?php $getTransactionHeadsUrl = Yii::app()->createAbsoluteUrl("/buyer/default/getTransactionHeads");
$getInvoicesUrl = Yii::app()->createAbsoluteUrl("/buyer/default/getInvoices");
$getInvoicesDetails = Yii::app()->createAbsoluteUrl("/buyer/default/getInvoicesDetails");
$getTransaction = Yii::app()->createAbsoluteUrl("/buyer/buyers/getTransactionDetails");
$getTransactionNo = Yii::app()->createAbsoluteUrl("/buyer/default/getTransactionNo");
$getUrl = Yii::app()->createAbsoluteUrl("/buyer/buyers/getDataByDate");
$checkInvoiceAmount = Yii::app()->createAbsoluteUrl("/buyer/default/checkInvoiceAmount");
?>

<?php $dropdownUrl = Yii::app()->createAbsoluteUrl("expenses/dynamicDropdown"); ?>

<?php $invoiceUrl = Yii::app()->createAbsoluteUrl("expenses/GetInvoiceDetails"); ?>
<?php $returnUrl = Yii::app()->createAbsoluteUrl("expenses/GetPurchaseReturnDetails"); ?>

<?php $getBillUrl = Yii::app()->createAbsoluteUrl("expenses/GetBillDetails"); ?>
<?php ?>
<?php $bill_invoice = Yii::app()->createAbsoluteUrl("expenses/DynamicBillorInvoice"); ?>



<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-dateFormat/1.0/jquery.dateFormat.js"></script>
<script>
    $(".js-example-basic-single").select2();
    $(function() {
        $("#BuyerTransactions_date").datepicker({
            dateFormat: 'dd-mm-yy',
            maxDate: new Date()
        });
    });
    $(document).ready(function() {
        $('#daybookform').on('shown.bs.collapse', function() {
            $('select').first().focus();
        });
    });
</script>
<script type="text/javascript">
    $(document).ready(function() {
        getProjectData();
        $("#BuyerTransactions_bank_id").attr("disabled", true);
        $("#BuyerTransactions_cheque_no").attr("disabled", true);
        $("a#next").css("pointer-events", "none").css("color", "#ccccc");

        $("#BuyerTransactions_company_id").change(function() {
            $('#BuyerTransactions_buyer_id').val('').trigger('change.select2');
            $('#project_id').val('').trigger('change.select2');
            $('#BuyerTransactions_project_id').val('');
            $('#flat_id').val('').trigger('change.select2');
            $('#BuyerTransactions_flat_id').val('');
            $('#BuyerTransactions_transaction_for').val('').trigger('change.select2');

            var company_id = $("#BuyerTransactions_company_id").val();
            if (company_id !== "") {
                getBuyerList(company_id);
                getProjectData();
            }
        });
        $("#BuyerTransactions_buyer_id").change(function() {
            $('#project_id').val('').trigger('change.select2');
            $('#BuyerTransactions_project_id').val('');
            $('#flat_id').val('').trigger('change.select2');
            $('#BuyerTransactions_flat_id').val('');
            $('#BuyerTransactions_transaction_for').val('').trigger('change.select2');

            // getProjectData();
            var buyer_id = $("#BuyerTransactions_buyer_id").val();
            var company_id = $("#BuyerTransactions_company_id").val();
            if (buyer_id !== "" && company_id !== '') {
                $("#project_id").val('').trigger('change.select2');
                setProjectFlatData(buyer_id, company_id);
            }
        });
        $('#flat_id').change(function() {

            $('#BuyerTransactions_transaction_for').val('').trigger('change.select2');

            var flat_id = $(this).val();
            $('#BuyerTransactions_flat_id').val(flat_id);
        });
        $('#BuyerTransactions_invoice_no').change(function() {
            var invoice_id = $(this).val();

            if (invoice_id !== '') {
                getInvoiceDetails(invoice_id);
            }
        });
        $("#BuyerTransactions_transaction_for").change(function() {
            var transaction_type = $("#BuyerTransactions_transaction_for").val();
            if (transaction_type === '3') {
                $('#transaction_no_div').addClass('hide');
            } else {
                $('#transaction_no_div').removeClass('hide');
            }
            if (transaction_type === '2') {
                $('#invoice_div').removeClass('hide');
                getInvoices();
            } else {
                $('#invoice_div').addClass('hide');
            }

            getTransactionNo(transaction_type);

        });
        // decimal point check
        $(".check").keydown(function(event) {
            if (event.shiftKey == true) {
                event.preventDefault();
            }
            if ((event.keyCode >= 48 && event.keyCode <= 57) ||
                (event.keyCode >= 96 && event.keyCode <= 105) ||
                event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 ||
                event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190 || event.keyCode == 110 || event.keyCode == 13) {
                var splitfield = $(this).val().split(".");
                if ((splitfield[1] !== undefined && splitfield[1].length >= 2) && event.keyCode != 8 && event.keyCode != 0 && event.keyCode != 13) {
                    event.preventDefault();
                }
            } else {
                event.preventDefault();
            }
            if ($(this).val().indexOf('.') !== -1 && (event.keyCode == 190 || event.keyCode == 110))
                event.preventDefault();
        });
        $('#BuyerTransactions_amount, #BuyerTransactions_sgst_p,#BuyerTransactions_cgst_p,#BuyerTransactions_igst_p').change(function() {
            var amount = parseFloat($("#BuyerTransactions_amount").val());
            var sgstp = parseFloat($("#BuyerTransactions_sgst_p").val());
            var cgstp = parseFloat($("#BuyerTransactions_cgst_p").val());
            var igstp = parseFloat($("#BuyerTransactions_igst_p").val());

            var sgst = (sgstp / 100) * amount;
            var cgst = (cgstp / 100) * amount;
            var igst = (igstp / 100) * amount;
            if (isNaN(sgst)) sgst = 0;
            if (isNaN(cgst)) cgst = 0;
            if (isNaN(igst)) igst = 0;
            if (isNaN(amount)) amount = 0;
            var total = amount + sgst + cgst + igst;

            var gsttotal = (parseFloat(sgst) + parseFloat(cgst) + parseFloat(igst)).toFixed(2);
            $("#sgst_val").text(sgst.toFixed(2));
            $("#BuyerTransactions_sgst_val").val(sgst.toFixed(2));
            $("#cgst_val").text(cgst.toFixed(2));
            $("#BuyerTransactions_cgst_val").val(cgst.toFixed(2));
            $("#igst_val").text(igst.toFixed(2));
            $("#BuyerTransactions_igst_val").val(igst.toFixed(2));
            $("#BuyerTransactions_total_amount").val(total.toFixed(2));
            $("#total_transaction_amount").html("<b>Total: </b>" + total.toFixed(2) + "");
            if (gsttotal !== "0.00")
                $("#total_gst_amount").html("<b>Total Tax: </b>" + gsttotal + "");
        });
        $('#BuyerTransactions_amount').blur(function() {
            var transaction_for = $('#BuyerTransactions_transaction_for').val();

            if (transaction_for == 2) {
                checkInvoiceAmount();
            }

        });
        $('.percentage').keyup(function() {
            if ($(this).val() > 100) {
                $(this).val('');
            }
        });
        $('.transaction_type_').change(function() {

            var transaction_type_value = $('#BuyerTransactions_transaction_type').val();
            var transaction_for = $('#BuyerTransactions_transaction_for').val();
            if ((transaction_type_value === '88')) {
                $("#BuyerTransactions_bank_id").attr("disabled", false);
                $('#BuyerTransactions_bank_id').val();
                $("#BuyerTransactions_cheque_no").attr("disabled", false);
            } else {
                $('#BuyerTransactions_bank_id').val('').trigger('change.select2');
                $("#BuyerTransactions_cheque_no").val('');
                $("#BuyerTransactions_bank_id").attr("disabled", true);
                $("#BuyerTransactions_cheque_no").attr("disabled", true);
            }
        });
        // $('#BuyerTransactions_transaction_type').change(function() {
        //     var from_transaction_type_value = this.value;
        //     var from_transaction_type_value = from_transaction_type_value.split('-');

        //     if (from_transaction_type_value[1] === '5') {
        //         $("#BuyerTransactions_bank_id").attr("disabled", false);
        //         $('#BuyerTransactions_bank_id').val(from_transaction_type_value[0]).trigger('change.select2');
        //         $("#BuyerTransactions_cheque_no").attr("disabled", false);
        //     } else {
        //         $('#BuyerTransactions_bank_id').val('').trigger('change.select2');
        //         $("#BuyerTransactions_cheque_no").val('');
        //         $("#BuyerTransactions_bank_id").attr("disabled", true);
        //         $("#BuyerTransactions_cheque_no").attr("disabled", true);
        //     }
        // });
        $("#buttonsubmit").click(function(event) {
            event.preventDefault()
            $("#buttonsubmit").attr('disabled', true);
            var crDate = $("#BuyerTransactions_date").val();
            var company = $("#BuyerTransactions_company_id").val();
            var buyer = $("#BuyerTransactions_buyer_id").val();
            var project = $("#BuyerTransactions_project_id").val();
            var apartment_no = $("#BuyerTransactions_flat_id").val();
            var transaction_for = $("#BuyerTransactions_transaction_for").val();

            var invoice_id = $("#BuyerTransactions_invoice_no").val();
            var transaction_type = $("#BuyerTransactions_transaction_type").val();
            var amount = $("#BuyerTransactions_amount").val();
            var totalamount = parseFloat($("#BuyerTransactions_total_amount").val());
            var desc = $("#BuyerTransactions_description").val();
            var bank = $("#BuyerTransactions_bank_id").val();

            var cheque = $("#BuyerTransactions_cheque_no").val();
            if (company == "") {
                $("#errormessage").show()
                    .html('<div class="alert alert-danger">Please select a company from the dropdown.</div>')
                    .fadeOut(5000);
                $("#buttonsubmit").attr('disabled', false);
                return false;
            }
            if (buyer == "") {
                $("#errormessage").show()
                    .html('<div class="alert alert-danger">Please select a buyer from the dropdown.</div>')
                    .fadeOut(5000);
                $("#buttonsubmit").attr('disabled', false);
                return false;
            }
            if (project == "") {
                $("#errormessage").show()
                    .html('<div class="alert alert-danger">Please select a project from the dropdown.</div>')
                    .fadeOut(5000);
                $("#buttonsubmit").attr('disabled', false);
                return false;
            }
            if (apartment_no == "") {
                $("#errormessage").show()
                    .html('<div class="alert alert-danger">Aprtment number cannot be blank</div>')
                    .fadeOut(5000);
                $("#buttonsubmit").attr('disabled', false);
                return false;
            }
            if (transaction_for == "") {
                $("#errormessage").show()
                    .html('<div class="alert alert-danger">Please select type from the dropdown.</div>')
                    .fadeOut(5000);
                $("#buttonsubmit").attr('disabled', false);
                return false;
            } else {
                if (transaction_for == "2") {
                    if (invoice_id == "") {
                        $("#errormessage").show()
                            .html('<div class="alert alert-danger">Please select invoice no from the dropdown.</div>')
                            .fadeOut(5000);
                        $("#buttonsubmit").attr('disabled', false);
                        return false;
                    }


                    checkInvoiceAmount();
                } else if (transaction_for == "3") {

                    if (transaction_type == "") {
                        $("#errormessage").show()
                            .html('<div class="alert alert-danger">Please select from transaction head from the dropdown.</div>')
                            .fadeOut(5000);
                        $("#buttonsubmit").attr('disabled', false);
                        return false;
                    }
                }
            }

            if (amount == "") {
                $("#errormessage").show()
                    .html('<div class="alert alert-danger">Amount cannot be blank</div>')
                    .fadeOut(5000);
                $("#buttonsubmit").attr('disabled', false);
                return false;
            }



            if (transaction_type === '88') {
                var bank_id = $("#BuyerTransactions_bank_id").val();
                if (bank_id == "") {
                    $("#errormessage").show()
                        .html('<div class="alert alert-danger">Please choose bank</div>')
                        .fadeOut(5000);
                    $("#buttonsubmit").attr('disabled', false);
                    return false;
                }
                var cheque_noo = $("#BuyerTransactions_cheque_no").val()
                if (cheque_noo == "") {
                    $("#errormessage").show()
                        .html('<div class="alert alert-danger">please enter cheque no</div>')
                        .fadeOut(5000);
                    $("#buttonsubmit").attr('disabled', false);
                    return false;
                }

            }
            $('#loader').css('display', 'inline-block');
            var transaction_id = $("#BuyerTransactions_id").val();
            var actionUrl;
            if (transaction_id == "") {
                actionUrl = "<?php echo Yii::app()->createAbsoluteUrl("/buyer/buyers/addTransaction"); ?>";
                localStorage.setItem("action", "add");
            } else {
                $("#buttonsubmit").attr('disabled', false);
                actionUrl = "<?php echo Yii::app()->createAbsoluteUrl("/buyer/buyers/updateTransaction"); ?>";
                localStorage.setItem("action", "update");
            }
            var data = $("#buyer-transaction-form").serialize();
            $.ajax({
                type: 'POST',
                url: actionUrl,
                data: data,
                dataType: 'json',
                success: function(data) {
                    $('#loader').hide();
                    if (data.status === 0) {
                        $("#errormessage").show()
                            .html('<div class="alert alert-warning"><strong>Failed !</strong> please try again. (' + data.message + ')</div>')
                            .fadeOut(10000);
                        $("#buttonsubmit").attr('disabled', false);
                        return false;
                    } else {
                        getDynamicBillorInvoice();
                        var currAction = localStorage.getItem("action");
                        if (currAction == "add") {
                            if (data == "insufficient amount") {
                                $("#errormessage").show()
                                    .html('<div class="alert alert-warning"><strong>Insufficient amount for transaction</strong> </div>')
                                    .fadeOut(5000);
                            } else {
                                $("#errormessage").show()
                                    .html('<div class="alert alert-success"><strong>Successfully !</strong> record added.</div>')
                                    .fadeOut(10000);
                            }
                            var today = new Date();
                            var dd = today.getDate();
                            var mm = today.getMonth() + 1; //January is 0!
                            var yyyy = today.getFullYear();
                            if (dd < 10)
                                dd = '0' + dd
                            if (mm < 10)
                                mm = '0' + mm
                            today = dd + '-' + mm + '-' + yyyy;
                            $("#entrydate").text(today);
                            $.ajax({
                                type: "POST",
                                data: {
                                    date: crDate
                                },
                                url: "<?php echo Yii::app()->createUrl("expenses/getAllData") ?>",
                                success: function(response) {
                                    document.getElementById("buyer-transaction-form").reset();
                                    $("#BuyerTransactions_date").val(crDate);
                                    $("#newlist").html(response);
                                    $("#Expenses_projectid").focus();
                                    $("#Expenses_bill_id").attr("disabled", false);
                                    $("#Expenses_expensetype_id").attr("disabled", false);
                                    $("#Expenses_payment_type").attr("disabled", false);
                                    $("#txtReceipt").attr("readonly", false);
                                    $("#buttonsubmit").attr("disabled", false);



                                    $('#Expenses_bill_id').val('0').trigger('change.select2');
                                    $('#Expenses_expensetype_id').html('<option value="">-Select Expense Head-</option>');
                                    $('#txtPurchaseType').val("").trigger('change.select2');
                                    $('#Expenses_expense_type').val('').trigger('change.select2');
                                    $('#Expenses_employee_id').val('').trigger('change.select2');
                                    $('#Expenses_bank_id').val('').trigger('change.select2');
                                    $('#Expenses_vendor_id').html('<option value="">-Select Vendor-</option>');

                                    $("#Expenses_vendor_id").attr("disabled", true);
                                    $("#txtAmount").attr("readonly", true);
                                    $("#txtSgstp").attr("readonly", true);
                                    $("#txtCgstp").attr("readonly", true);
                                    $("#txtIgstp").attr("readonly", true);
                                    $("#txtPurchaseType").attr("disabled", true);
                                    $("#txtPaid").attr("readonly", true);
                                    $("#Expenses_bank_id").attr("disabled", true);
                                    $("#txtChequeno").attr("readonly", true);

                                    $("#txtSgst1").text("");
                                    $("#txtCgst1").text("");
                                    $("#txtIgst1").text("");
                                    $("#txtTotal1").html("");
                                    $("#txtgstTotal").text("");
                                }
                            });
                            //$(".selectBank").css("display","none");
                        } else if (currAction == "update") {
                            $("#txtDaybookId").val('');
                            $("#buttonsubmit").text("ADD");
                            if (data.status === '1') {
                                $("#errormessage").show()
                                    .html('<div class="alert alert-success"><strong>Successfully !</strong> Update request send succesfully. Please wait for approval</div>')
                                    .fadeOut(10000);
                                $.ajax({
                                    type: "POST",
                                    data: {
                                        date: crDate
                                    },
                                    url: "<?php echo Yii::app()->createUrl("expenses/getAllData") ?>",
                                    success: function(response) {
                                        document.getElementById("buyer-transaction-form").reset();
                                        $("#BuyerTransactions_date").val(crDate);
                                        $("#newlist").html(response);
                                        $("#Expenses_projectid").focus();
                                        $("#Expenses_bill_id").attr("disabled", false);
                                        $("#Expenses_expensetype_id").attr("disabled", false);
                                        $("#Expenses_payment_type").attr("disabled", false);
                                        $("#txtReceipt").attr("readonly", false);
                                        $("#buttonsubmit").attr("disabled", false);



                                        $('#Expenses_bill_id').val('0').trigger('change.select2');
                                        $('#Expenses_expensetype_id').html('<option value="">-Select Expense Head-</option>');
                                        $('#txtPurchaseType').val("").trigger('change.select2');
                                        $('#Expenses_expense_type').val('').trigger('change.select2');
                                        $('#Expenses_employee_id').val('').trigger('change.select2');
                                        $('#Expenses_bank_id').val('').trigger('change.select2');
                                        $('#Expenses_vendor_id').html('<option value="">-Select Vendor-</option>');

                                        $("#Expenses_vendor_id").attr("disabled", true);
                                        $("#txtAmount").attr("readonly", true);
                                        $("#txtSgstp").attr("readonly", true);
                                        $("#txtCgstp").attr("readonly", true);
                                        $("#txtIgstp").attr("readonly", true);
                                        $("#txtPurchaseType").attr("disabled", true);
                                        $("#txtPaid").attr("readonly", true);
                                        $("#Expenses_bank_id").attr("disabled", true);
                                        $("#txtChequeno").attr("readonly", true);

                                        $("#txtSgst1").text("");
                                        $("#txtCgst1").text("");
                                        $("#txtIgst1").text("");
                                        $("#txtTotal1").html("");
                                        $("#txtgstTotal").text("");
                                    }
                                });
                            } else if (data == 3) {
                                $("#errormessage").show()
                                    .html('<div class="alert alert-danger"><strong>Failed !</strong> Cannot send update request.</div>')
                                    .fadeOut(10000);
                            } else if (data == 4) {
                                $("#errormessage").show()
                                    .html('<div class="alert alert-warning"><strong>Failed !</strong> Cannot send update request. Already have a request for this.</div>')
                                    .fadeOut(10000);
                            } else {
                                $("#errormessage").show()
                                    .html('<div class="alert alert-success"><strong>Successfully !</strong> record updated.</div>')
                                    .fadeOut(10000);
                            }
                        }
                        document.getElementById("buyer-transaction-form").reset();
                        $("#BuyerTransactions_date").val(crDate);
                        if (data == 2 || data == 3 || data == 4) {

                        } else {
                            if (data != 'insufficient amount')
                                $("#newlist").html(data);
                        }
                        $('#BuyerTransactions_company_id').val('').trigger('change.select2');
                        $('#BuyerTransactions_buyer_id').val('').trigger('change.select2');
                        $('#project_id').val('').trigger('change.select2');
                        $('#BuyerTransactions_project_id').val('');
                        $('#flat_id').val('').trigger('change.select2');
                        $('#BuyerTransactions_flat_id').val('');
                        $('#flat_id').val('').trigger('change.select2');
                        $('#BuyerTransactions_transaction_for').val('').trigger('change.select2');
                        $('#BuyerTransactions_invoice_no').val('').trigger('change.select2');


                        $('#BuyerTransactions_transaction_type').val('').trigger('change.select2');
                        $('#BuyerTransactions_bank_id').val('').trigger('change.select2');
                        $('#BuyerTransactions_transaction_no').val('');

                        $('#total_gst_amount').val('');
                        $('#sgst_val').text('');
                        $('#cgst_val').text('');
                        $('#igst_val').text('');
                        $('#total_transaction_amount').text('');

                        $("#txtAmount").attr("readonly", true);
                        $("#txtSgstp").attr("readonly", true);
                        $("#txtCgstp").attr("readonly", true);
                        $("#txtIgstp").attr("readonly", true);
                        $("#txtPurchaseType").attr("disabled", true);
                        $("#txtPaid").attr("readonly", true);
                        $("#Expenses_bank_id").attr("disabled", true);
                        $("#txtChequeno").attr("readonly", true);

                        $("#txtSgst1").text("");
                        $("#txtCgst1").text("");
                        $("#txtIgst1").text("");
                        $("#txtTotal1").html("");
                        $("#txtgstTotal").text("");
                        $("#txtTdsPaid").html("");
                    }
                },
                error: function(data) { // if error occured
                    //alert("Error occured.please try again");
                    //alert(data);
                }
            });
        });

    });

    function checkInvoiceAmount() {
        var amount = parseFloat($("#BuyerTransactions_amount").val());
        var invoice_no = $('#BuyerTransactions_invoice_no').val();
        $.ajax({
            url: "<?php echo $checkInvoiceAmount; ?>",
            data: {
                "invoice_no": invoice_no,
                "amount": amount
            },
            type: "POST",
            dataType: 'json',
            success: function(data) {
                if (data.flag == 2) {
                    $('#amount_error').css('display', 'block');
                    $('#amount_error').text(data.msg);
                    $("#buttonsubmit").attr('disabled', true);
                } else {
                    $('#amount_error').css('display', 'none');
                    $('#amount_error').text('');
                    $("#buttonsubmit").attr('disabled', false);
                }
            }
        });
    }

    function getBuyerList(company_id) {
        $.ajax({
            url: "<?php echo $getBuyerList; ?>",
            data: {
                "company_id": company_id
            },
            type: "POST",
            dataType: 'json',
            success: function(data) {
                if (company_id === "") {
                    $("#BuyerTransactions_buyer_id").val('').trigger('change.select2');
                } else {
                    $('#BuyerTransactions_buyer_id').empty();
                    $("#BuyerTransactions_buyer_id").prepend("<option value='' selected='selected'>-Select Buyer -</option>");
                    $.each(data.buyer_list, function(val, text) {
                        $('#BuyerTransactions_buyer_id').append($('<option></option>').val(val).html(text))
                    });
                }
            }
        });
    }

    function setProjectFlatData(buyer_id, company_id) {
        $.ajax({
            url: "<?php echo $getProjectAndFlats; ?>",
            data: {
                "buyer_id": buyer_id,
                "company_id": company_id
            },
            type: "POST",
            dataType: 'json',
            success: function(data) {
                if (buyer_id === "" || data.project_id === "") {
                    $('#project_id').empty();
                    $("#project_id").prepend("<option value='' selected='selected'>-Select Project  -</option>");
                    $("#project_id").val('').trigger('change.select2');
                    $('#BuyerTransactions_project_id').val('');
                    $("#flat_id").val('').trigger('change.select2');
                    $("#BuyerTransactions_flat_id").val('');
                } else {
                    $("#project_id").val(data.project_id).trigger('change.select2');
                    $('#BuyerTransactions_project_id').val(data.project_id);
                    // $("#project_id").attr("disabled", true);
                    if (data.flat_count === 1) {
                        $.each(data.flat_list, function(val, text) {
                            $('#flat_id').append($('<option selected="selected"></option>').val(val).html(text))
                            $("#BuyerTransactions_flat_id").val(val);
                        });
                        $("#flat_id").attr("disabled", true);
                    } else {
                        $("#flat_id").attr("disabled", false);
                        $('#flat_id').empty();
                        $("#flat_id").prepend("<option value='' selected='selected'>-Select Apartment No  -</option>");
                        $.each(data.flat_list, function(val, text) {
                            $('#flat_id').append($('<option></option>').val(val).html(text))
                        });
                    }
                }
            }
        });
    }

    function getProjectData() {
        var company_id = $("#BuyerTransactions_company_id").val();
        var buyer_id = $('#BuyerTransactions_buyer_id').val();

        $.ajax({
            url: "<?php echo $getprojecstUrl; ?>",
            data: {
                "company_id": company_id,
                'buyer_id': buyer_id
            },
            type: "POST",
            dataType: 'json',
            success: function(data) {

                if (company_id === "") {
                    $("#BuyerTransactions_project_id").val('').trigger('change.select2');
                } else {
                    $('#project_id').empty();
                    $("#project_id").prepend("<option value='' selected='selected'>-Select Project -</option>");
                    $.each(data.project_list, function(val, text) {
                        $('#project_id').append($('<option></option>').val(val).html(text))
                    });
                }
            }
        });

    }



    function getInvoices() {
        var buyer_id = $('#BuyerTransactions_buyer_id').val();
        var project_id = $('#BuyerTransactions_project_id').val();
        var flat_id = $('#BuyerTransactions_flat_id').val();
        if (buyer_id != "" && project_id != "" && flat_id != "") {
            $.ajax({
                url: "<?php echo $getInvoicesUrl; ?>",
                data: {
                    'buyer_id': buyer_id,
                    'project_id': project_id,
                    'flat_id': flat_id

                },
                type: "POST",
                dataType: 'json',
                success: function(data) {
                    $('#BuyerTransactions_invoice_no').empty();
                    $("#BuyerTransactions_invoice_no").prepend("<option value='' selected='selected'>-Select Invoice No-</option>");
                    $.each(data.invoice_list, function(val, text) {
                        $('#BuyerTransactions_invoice_no').append($('<option></option>').val(val).html(text))
                    });

                }
            });
        }


    }

    function getInvoiceDetails(invoice_id) {
        $.ajax({
            url: "<?php echo $getInvoicesDetails; ?>",
            data: {
                'invoice_id': invoice_id,
            },
            type: "POST",
            dataType: 'json',
            success: function(data) {
                $('#BuyerTransactions_amount').val(data.total_sum);
                $('#BuyerTransactions_total_amount').val(data.total_sum);
            }
        });
    }

    function getTransactionNo(transaction_type) {
        var transaction_id = $("#BuyerTransactions_id").val();

        $.ajax({
            url: "<?php echo $getTransactionNo; ?>",
            data: {
                'transaction_type': transaction_type,
                'transaction_id': transaction_id
            },
            type: "POST",
            dataType: 'json',
            success: function(data) {
                $('#BuyerTransactions_transaction_no').val(data.transaction_no);

            }
        });


    }

    function getFullData(newDate) {
        $.ajax({
            url: "<?php echo $getUrl; ?>",
            data: {
                "date": newDate
            },
            type: "POST",
            success: function(data) {
                $("#newlist").html(data);
            }
        });
    }
    /******************************************** */


    function changedate() {
        var cDate = $("#BuyerTransactions_date").val();
        getFullData(cDate);
        validateDateSelection(cDate);
    }

    function getDropdownList(project_id, bill_id, exptypeid, edit, expId) {
        if (edit == undefined || edit == '') {
            edit = '';
        }
        $.ajax({
            url: "<?php echo $dropdownUrl; ?>",
            data: {
                "project": project_id,
                "bill": bill_id,
                "expId": expId
            },
            type: "POST",
            success: function(data) {
                var result = JSON.parse(data);
                $("#invoicee_client_id").val(result["client_id"]);
                $("#Expenses_bill_id").html(result["bills"]);
                if (edit == 'edit') {
                    $("#Expenses_projectid").select2("focus");
                } else {
                    $("#Expenses_bill_id").select2("focus");
                    if (project_id == '') {
                        $("#Expenses_expensetype_id").attr("disabled", false);
                        $("#Expenses_vendor_id").attr("disabled", true);
                    }
                }

                if (exptypeid == 0) {
                    $("#Expenses_bill_id").val(0);
                    $("#Expenses_vendor_id").html('<option value="">-Select Vendor-</option>');
                } else {
                    if (bill_id === null) {
                        $("#Expenses_bill_id").val(0);
                    } else {
                        if (result['type'] == 'bill') {
                            $("#Expenses_bill_id").val('1,' + bill_id);
                        } else if (result['type'] == 'invoice') {
                            $("#Expenses_bill_id").val('2,' + bill_id);
                        } else {
                            $("#Expenses_bill_id").val('3,' + bill_id);
                        }
                    }

                }

                $("#Expenses_expensetype_id").html(result["expenses"]);
                $('#Expenses_expensetype_id').find('option').each(function() {
                    var data = $(this).text();
                    var result = data.split('-');
                    var head = '<b>' + result[0] + '</b>';
                    var title = '<i>' + result[1] + '</i>';

                    //   $(this).css('color','red');
                });
                if (exptypeid != 0)
                    $("#Expenses_expensetype_id").val(exptypeid);


            },
        });


    }


    function getDynamicBillorInvoice() {
        $.ajax({
            url: "<?php echo $bill_invoice; ?>",
            type: "POST",
            success: function(data) {
                $("#Expenses_bill_id").html(data);
            }
        });
    }

    function getProjectList(company_id, opt, pro, bank) {
        $.ajax({
            url: "<?php echo $getprojecstUrl; ?>",
            data: {
                "company_id": company_id
            },
            type: "POST",
            dataType: 'json',
            success: function(data) {
                if (company_id == 0) {
                    $("#Expenses_projectid").val('').trigger('change.select2');
                    $("#Expenses_bank_id").val('').trigger('change.select2');
                } else {
                    $("#Expenses_projectid").html(data.project);
                    $("#Expenses_bank_id").html(data.bank);
                }
                $('#Expenses_bill_id').val('0').trigger('change.select2');
                if (opt != 0)
                    $("#Expenses_bank_id").val(bank).trigger('change.select2');
                $("#Expenses_projectid").val(pro).trigger('change.select2');
            }
        });
    }





    $(document).ready(function() {
        $("#Expenses_projectid").focus();
        $("#Expenses_vendor_id").attr("disabled", true);
        $("#Expenses_bank_id").attr("disabled", true);
        $("#txtChequeno").attr("readonly", true);
        $("#txtPurchaseType").attr("disabled", true);
        $("#txtPaid").attr("readonly", true);
        $("a#next").css("pointer-events", "none").css("color", "#ccccc");
        // add and edit daybook

        $("#txtPaid").keyup(function() {
            var totalamount = parseFloat($("#txtTotal").val());
            var purchasetype = $("#txtPurchaseType").val();
            var paid = parseFloat($("#txtPaid").val());
            if (purchasetype == 3) {
                if (paid >= totalamount) {
                    $("#errormessage").show()
                        .html('<div class="alert alert-warning">You have selected the purchase type as Partially Paid.<br/>The paid amount must be less than the total amount.</div>')
                        .fadeOut(10000);
                    $("#buttonsubmit").attr('disabled', true);
                    return false;
                } else if (paid <= 0) {
                    $("#errormessage").show()
                        .html('<div class="alert alert-warning">You have selected the purchase type as Partially Paid.<br/>The paid amount must be greater than 0.</div>')
                        .fadeOut(10000);
                    $("#buttonsubmit").attr('disabled', true);
                    return false;
                } else {

                    $("#buttonsubmit").attr('disabled', false);
                }
            }
            if (paid == "") {
                if (totalamount == "") {
                    getTdsCalculations(0);
                } else {
                    getTdsCalculations(totalamount);
                }
            } else {
                getTdsCalculations(paid);
            }
        });


        $("body").on("click", ".row-daybook", function(e) {
            $('html, body').animate({
                scrollTop: 0
            }, 'slow');
            $(".daybook .collapse").addClass("in");
            $(".addentries").removeClass("collapsed");
            if ($(".daybook .collapse").css('display') == 'visible') {
                $(".daybook .collapse").css({
                    "height": "auto"
                });
            } else {
                $(".daybook .collapse").css({
                    "height": ""
                });
            }
            $(".popover").removeClass("in");
            var rowId = $(this).attr("id");
            var expId = $("#expenseid" + rowId).val();
            $("#buttonsubmit").text("UPDATE");
            $("#txtDaybookId").val(expId);
            $.ajax({
                url: "<?php echo $getTransaction; ?>",
                data: {
                    "expenseid": expId
                },
                type: "POST",
                success: function(data) {
                    //$('select').first().focus();
                    $(".gstvalue").css({
                        'display': 'inline-block'
                    });
                    var result = JSON.parse(data);
                    $("#BuyerTransactions_id").val(result.result["id"]);

                    $("#BuyerTransactions_company_id").select2("focus");
                    $('#BuyerTransactions_company_id').val(result.result["company_id"]).trigger('change.select2');
                    $.each(result.buyer_list, function(val, text) {
                        $('#BuyerTransactions_buyer_id').append($('<option></option>').val(val).html(text))
                    });
                    $('#BuyerTransactions_buyer_id').val(result.result["buyer_id"]).trigger('change.select2');
                    $.each(result.project_list, function(val, text) {
                        $('#project_id').append($('<option></option>').val(val).html(text))
                    });
                    $('#project_id').val(result.result["project_id"]).trigger('change.select2');
                    $("#BuyerTransactions_project_id").val(result.result["project_id"]);
                    $("#project_id").attr("disabled", true);
                    $.each(result.flat_list, function(val, text) {
                        $('#flat_id').append($('<option></option>').val(val).html(text))
                    });
                    $('#flat_id').val(result.result["flat_id"]).trigger('change.select2');
                    $("#BuyerTransactions_flat_id").val(result.result["flat_id"]);
                    $("#flat_id").attr("disabled", true);
                    $('#BuyerTransactions_transaction_for').val(result.result["transaction_for"]).trigger('change.select2');
                    if (result.result["transaction_for"] == 1) {
                        $("#transaction_no_div").css("display", "block");
                        $("#BuyerTransactions_transaction_no").val(result.result["transaction_no"]);

                    } else if (result.result["transaction_for"] == 2) {
                        $("#transaction_no_div").css("display", "block");
                        $("#invoice_div").css("display", "block");
                        $("#BuyerTransactions_transaction_no").val(result.result["transaction_no"]);
                        $.each(result.invoice_list, function(val, text) {
                            $('#BuyerTransactions_invoice_no').append($('<option></option>').val(val).html(text))
                        });
                        $('#BuyerTransactions_invoice_no').val(result.result["invoice_no"]).trigger('change.select2');
                    }
                    $('#BuyerTransactions_transaction_type').val(result.result["transaction_type"]).trigger('change.select2');
                    $("#BuyerTransactions_amount").val(result.result["amount"]);
                    $("#BuyerTransactions_sgst_p").val(result.result["sgst_p"]);
                    $("#sgst_val").text(result.result["sgst_val"]);

                    $("#BuyerTransactions_cgst_p").val(result.result["cgst_p"]);
                    $("#cgst_val").text(result.result["cgst_val"]);

                    $("#BuyerTransactions_igst_p").val(result.result["igst_p"]);
                    $("#igst_val").text(result.result["igst_val"]);
                    $("#BuyerTransactions_description").val(result.result["description"]);
                    $("#BuyerTransactions_total_amount").val(result.result["total_amount"]);
                    $("#total_gst_amount").html('<b>Total Tax: </b>' + result.result["tax_total"]);
                    $("#total_transaction_amount").html("<b>Total: </b>" + result.result["total_amount"]);
                    $('#BuyerTransactions_bank_id').val(result.result["bank_id"]).trigger('change.select2');
                    $('#BuyerTransactions_cheque_no').val(result.result['cheque_no']);

                }
            });
        });



        $("#previous").click(function(e) {
            var cDate = $("#BuyerTransactions_date").val();
            cDate = cDate.split("-").reverse().join("-");
            var newDate = new Date(cDate);
            var prDate = new Date(newDate.getFullYear(), newDate.getMonth(), newDate.getDate() - 1);
            var newPrDate = $.format.date(prDate, "dd-MM-yyyy");
            $("#BuyerTransactions_date").val(newPrDate);
            getFullData(newPrDate);
            validateDateSelection(newPrDate);
        });
        $("#current").click(function() {
            var cDate = new Date();
            var newDate = $.format.date(cDate, "dd-MM-yyyy");
            $("#BuyerTransactions_date").val(newDate);
            getFullData(newDate);
            validateDateSelection(newDate);
        });
        $("#next").click(function() {
            var cDate = $("#BuyerTransactions_date").val();
            cDate = cDate.split("-").reverse().join("-");
            var newDate = new Date(cDate);
            var nxtDate = new Date(newDate.getFullYear(), newDate.getMonth(), newDate.getDate() + 1);
            var newNxtDate = $.format.date(nxtDate, "dd-MM-yyyy");
            $("#BuyerTransactions_date").val(newNxtDate);
            getFullData(newNxtDate);
            validateDateSelection(newNxtDate);
        });
        $("#Expenses_payment_type").change(function() {
            var receiptType = $("#Expenses_payment_type").val();
            if (receiptType != "") {
                $("#Expenses_bill_id").attr("disabled", true);
                $("#Expenses_expensetype_id").val("");
                $("#Expenses_expense_type").attr("disabled", true);
                $("#Expenses_expense_type").val("");
                $("#txtPurchaseType").val("");
                $("#txtPaid").val("");
                $("#Expenses_expensetype_id").attr("disabled", true);
                $("#txtPurchaseType").attr("disabled", true);
                $("#txtPaid").attr("readonly", true);
            } else {
                $("#Expenses_bill_id").attr("disabled", false);
                $("#Expenses_expensetype_id").attr("disabled", false);
                $("#Expenses_expense_type").attr("disabled", false);
                $("#txtReceipt").val("");
                $("#txtPurchaseType").attr("disabled", false);
                $("#txtPaid").attr("readonly", false);
            }
            if (receiptType == 88) {
                $("#Expenses_bank_id").attr("disabled", false);
                $("#txtChequeno").attr("readonly", false);
            } else {
                $("#Expenses_bank_id").val("");
                $("#txtChequeno").val("");
                $("#Expenses_bank_id").attr("disabled", true);
                $("#txtChequeno").attr("readonly", true);
            }
        });
        $("#Expenses_bill_id").change(function() {
            var val = $(this).val();
            var typeId = $(this).find(':selected').data('id');
            var url = '';
            var project_id = $('#Expenses_projectid').val();
            if (typeId == 1 || val == 0) {
                url = "<?php echo $getBillUrl; ?>";
                //$('#Expenses_expense_type').html('<option value="">-Select Transaction Type-</option><option value="0">Credit</option><option value="88">Cheque</option><option value="89">Cash</option>');
                //$('#txtPurchaseType').html('<option value="">-Select Transaction Type-</option><option value="1">Credit</option><option value="2">Full Paid</option><option value="3">Partially Paid</option>');
                if (val == 0) {
                    $('#Expenses_expense_type').html('<option value="">-Select Transaction Type-</option><option value="0">Credit</option><option value="88">Cheque</option><option value="89">Cash</option><option value="103">Petty Cash</option>');
                    $('#txtPurchaseType').html('<option value="">-Select Transaction Type-</option><option value="2">Full Paid</option><option value="3">Partially Paid</option>');
                } else {
                    $('#Expenses_expense_type').html('<option value="">-Select Transaction Type-</option><option value="0">Credit</option><option value="88">Cheque</option><option value="89">Cash</option><option value="103">Petty Cash</option>');
                    $('#txtPurchaseType').html('<option value="">-Select Transaction Type-</option><option value="1">Credit</option><option value="2">Full Paid</option><option value="3">Partially Paid</option>');
                }

            } else if (typeId == 2) {
                url = "<?php echo $invoiceUrl; ?>";
                $('#Expenses_expense_type').html('<option value="">-Select Transaction Type-</option><option value="88">Cheque</option><option value="89">Cash</option><option value="103">Petty Cash</option>');
                $('#txtPurchaseType').html('<option value="">-Select Transaction Type-</option><option value="2">Full Paid</option><option value="3">Partially Paid</option>');
            } else {
                url = "<?php echo $returnUrl; ?>";
                $('#Expenses_expense_type').html('<option value="">-Select Transaction Type-</option><option value="88">Cheque</option><option value="89">Cash</option><option value="103">Petty Cash</option>');
                $('#txtPurchaseType').html('<option value="">-Select Transaction Type-</option><option value="2">Full Paid</option><option value="3">Partially Paid</option>');
            }
            if (val == 0) {
                // no mandatory

                $("#txtAmount").attr("readonly", false);
                $("#txtSgstp").attr("readonly", false);
                $("#txtSgst").attr("readonly", true);
                $("#txtCgstp").attr("readonly", false);
                $("#txtCgst").attr("readonly", true);
                $("#txtTotal").attr("readonly", true);
                $("#txtIgstp").attr("readonly", false);
                $("#txtAmount").removeClass("avoid_clicks");
                $("#txtSgstp").removeClass("avoid_clicks");
                $("#txtCgstp").removeClass("avoid_clicks");
                $("#txtIgstp").removeClass("avoid_clicks");
                $("#txtAmount").val('');
                $("#txtSgstp").val('');
                $("#txtSgst").val('');
                $("#txtSgst1").text('');
                $("#txtCgstp").val('');
                $("#txtCgst").val('');
                $("#txtCgst1").text('');
                $("#txtTotal").val('');
                $("#txtTotal1").text('');
                $("#txtIgstp").val('');
                $("#txtIgst").val('');
                $("#txtIgst1").text('');
                $("#txtgstTotal").text('');
                $("#Expenses_expensetype_id").attr("disabled", false);
                $("#Expenses_vendor_id").attr("disabled", false);
                $("#txtPurchaseType").attr("disabled", false);
                $("#txtPaid").attr("disabled", false);
                $("#txtPurchaseType").attr("disabled", true);
                $("#txtPaid").attr("readonly", true);
                if (project_id != '') {
                    getDropdownList(project_id, 0, 0, '', 0);
                }
                $("#Expenses_vendor_id").html('<option value="">-Select Vendor-</option>');
                $.ajax({
                    method: "POST",
                    dataType: "json",
                    url: '<?php echo Yii::app()->createUrl('expenses/ajaxcall'); ?>',
                    success: function(result) {
                        $("#Expenses_expensetype_id").select2("focus");
                    }
                });

            } else {
                // bill/invoice no
                $("#txtAmount").attr("readonly", true);
                $("#txtSgstp").attr("readonly", true);
                $("#txtSgst").attr("readonly", true);
                $("#txtCgstp").attr("readonly", true);
                $("#txtCgst").attr("readonly", true);
                $("#txtTotal").attr("readonly", true);
                $("#txtIgstp").attr("readonly", true);

                $("#txtAmount").addClass("avoid_clicks");
                $("#txtSgstp").addClass("avoid_clicks");
                $("#txtCgstp").addClass("avoid_clicks");
                $("#txtIgstp").addClass("avoid_clicks");

                $("#Expenses_bank_id").val("");
                $("#txtChequeno").val("");
                $("#Expenses_bank_id").attr("disabled", true);
                $("#txtChequeno").attr("readonly", true);
                $("#txtPaid").val("");
                $("#txtPaid").attr("readonly", true);
                $("#Expenses_bank_id").val("").trigger("change.select2");
                $('#Expenses_expensetype_id').val("").trigger("change.select2");
                $('#Expenses_vendor_id').val("").trigger("change.select2");
                var billId = $("#Expenses_bill_id").val();
                if (typeId == 1) {
                    $.ajax({
                        url: url,
                        data: {
                            "billid": billId
                        },
                        type: "POST",
                        success: function(data) {
                            $(".gstvalue").css({
                                'display': 'inline-block'
                            });
                            var result = JSON.parse(data);
                            $("#Expenses_expensetype_id").attr("disabled", true);
                            $("#Expenses_vendor_id").attr("disabled", true);
                            $("#Expenses_expense_type").attr("disabled", false);
                            $("#Expenses_payment_type").attr("disabled", true);
                            $("#txtReceipt").attr("readonly", true);
                            $("#txtPurchaseType").attr("disabled", false);
                            $("#txtPaid").attr("readonly", false);
                            $("#txtAmount").val(result["amount"].toFixed(2));
                            $("#txtSgstp").val(result["sgstp"]);
                            $("#txtSgst").val(result["sgst"].toFixed(2));
                            $("#txtCgstp").val(result["cgstp"]);
                            $("#txtCgst").val(result["cgst"].toFixed(2));
                            $("#txtIgstp").val(result["igstp"]);
                            $("#txtIgst").val(result["igst"].toFixed(2));
                            $("#txtTotal").val(result["total"].toFixed(2));
                            $("#Expenses_expense_type").select2("focus");
                            $('#Expenses_expensetype_id').val(result["expensehead_id"]).trigger('change.select2');
                            // $('#Expenses_expensetype_id').html("<option value=" + result["expensehead_id"] + ">" + result["expense_name"] + "</option>");
                            $('#Expenses_vendor_id').html("<option value=" + result["vendor_id"] + ">" + result["vendor_name"] + "</option>");
                            $("#bill_exphead").val(result["expensehead_id"]);
                            $("#bill_vendor").val(result["vendor_id"]);
                            $("#txtSgst1").html((result["sgst"]).toFixed(2));
                            $("#txtCgst1").html((result["cgst"]).toFixed(2));
                            $("#txtIgst1").html((result["igstp"]).toFixed(2));
                            $("#txtTdsp").val("");
                            $("#txtTdsp").attr("readonly", false);
                            $("#txtTds1").text("");
                            $("#txtTds").val("");
                            $("#txtTdsPaid").html("<b>Amount To Be Paid: </b>" + result["total"].toFixed(2) + "");
                            $("#Expenses_paidamount").val(result["total"]);
                            $("#txtTotal1").html("<b>Total: </b>" + (result["total"]).toFixed(2) + "");
                            var gsttotal = (parseFloat(result["sgst"]) + parseFloat(result["cgst"]) + parseFloat(result["igst"])).toFixed(2);
                            $("#txtgstTotal").html("<b>Total Tax: </b>" + gsttotal + "");
                            var project = $('#Expenses_projectid').val();
                            $('#Expenses_projectid').val(result["project_id"]).trigger('change.select2');
                            var company = $('#Expenses_company_id').val();
                            if (company == '') {
                                $('#Expenses_company_id').val(result["company"]).trigger('change.select2');
                            }

                        }
                    });
                } else if (typeId == 2) {
                    $('#Expenses_expensetype_id').val("").trigger("change.select2");
                    $('#Expenses_vendor_id').val("").trigger("change.select2");
                    $.ajax({
                        url: url,
                        data: {
                            "billid": billId
                        },
                        type: "POST",
                        success: function(data) {
                            $(".gstvalue").css({
                                'display': 'inline-block'
                            });
                            var result = JSON.parse(data);
                            $("#invoicee_client_id").val(result["client_id"]);
                            $("#txtAmount").val(result["amount"].toFixed(2));
                            $("#txtSgstp").val(result["sgstp"].toFixed(2));
                            $("#txtSgst").val(result["sgst"].toFixed(2));
                            $("#txtCgstp").val(result["cgstp"].toFixed(2));
                            $("#txtCgst").val(result["cgst"].toFixed(2));
                            $("#txtIgstp").val(result["igstp"].toFixed(2));
                            $("#txtIgst").val(result["igst"].toFixed(2));
                            $("#txtTotal").val(result["total"].toFixed(2));
                            $("#Expenses_expense_type").select2("focus");
                            $("#Expenses_expensetype_id").html(result['ledger']);
                            $("#Expenses_vendor_id").attr("disabled", true);
                            $("#txtPurchaseType").attr("disabled", false);
                            $("#txtPaid").attr("readonly", false);
                            $("#txtSgst1").html(result["sgst"].toFixed(2));
                            $("#txtCgst1").html(result["cgst"].toFixed(2));
                            $("#txtIgst1").html(result["igstp"].toFixed(2));
                            $("#txtTdsp").val("");
                            $("#txtTdsp").attr("readonly", true);
                            $("#txtTds1").text("");
                            $("#txtTds").val("");
                            $("#txtTdsPaid").html("");
                            $("#Expenses_paidamount").val("");
                            $("#txtTotal1").html("<b>Total: </b>" + (result["total"]).toFixed(2) + "");
                            var gsttotal = (parseFloat(result["sgst"]) + parseFloat(result["cgst"]) + parseFloat(result["igst"])).toFixed(2);
                            $("#txtgstTotal").html("<b>Total Tax: </b>" + gsttotal + "");
                            $('#Expenses_projectid').val(result["project_id"]).trigger('change.select2');
                            var company = $('#Expenses_company_id').val();
                            if (company == '') {
                                $('#Expenses_company_id').val(result["company"]).trigger('change.select2');
                            }
                        }
                    });
                } else {
                    $('#Expenses_expensetype_id').val("").trigger("change.select2");
                    $('#Expenses_vendor_id').val("").trigger("change.select2");
                    $.ajax({
                        url: url,
                        data: {
                            "billid": billId
                        },
                        type: "POST",
                        success: function(data) {
                            $(".gstvalue").css({
                                'display': 'inline-block'
                            });
                            var result = JSON.parse(data);
                            $("#txtAmount").val(result["amount"].toFixed(2));
                            $("#txtSgstp").val(result["sgstp"].toFixed(2));
                            $("#txtSgst").val(result["sgst"].toFixed(2));
                            $("#txtCgstp").val(result["cgstp"].toFixed(2));
                            $("#txtCgst").val(result["cgst"].toFixed(2));
                            $("#txtIgstp").val(result["igstp"].toFixed(2));
                            $("#txtIgst").val(result["igst"].toFixed(2));
                            $("#txtTotal").val(result["total"].toFixed(2));
                            $("#Expenses_expense_type").select2("focus");
                            $("#Expenses_expensetype_id").attr("disabled", true);
                            $("#Expenses_vendor_id").attr("disabled", true);
                            $("#txtPurchaseType").attr("disabled", false);
                            $("#txtPaid").attr("readonly", false);
                            $("#txtSgst1").html(result["sgst"].toFixed(2));
                            $("#txtCgst1").html(result["cgst"].toFixed(2));
                            $("#txtIgst1").html(result["igstp"].toFixed(2));
                            $("#txtTotal1").html("<b>Total: </b>" + (result["total"]).toFixed(2) + "");
                            var gsttotal = (parseFloat(result["sgst"]) + parseFloat(result["cgst"]) + parseFloat(result["igst"])).toFixed(2);
                            $("#txtgstTotal").html("<b>Total Tax: </b>" + gsttotal + "");
                            $('#Expenses_projectid').val(result["project_id"]).trigger('change.select2');
                            var company = $('#Expenses_company_id').val();
                            if (company == '') {
                                $('#Expenses_company_id').val(result["company"]).trigger('change.select2');
                            }
                        }
                    });
                }
            }
        });

        $('.numbersOnly').keyup(function() {
            this.value = this.value.replace(/[^0-9\.]/g, '');
        });
        $("#Expenses_expensetype_id").change(function() {
            var expType = $("#Expenses_expensetype_id").val();
            var bill = $("#Expenses_bill_id").val();
            if (expType != "" || expType != 0) {
                $("#Expenses_payment_type").attr("disabled", true);
                $("#txtReceipt").attr("disabled", true);
                $("#Expenses_vendor_id").val("");
                $("#Expenses_vendor_id").attr("disabled", false);
                $("#txtAmount").attr("disabled", false);
                $("#txtSgstp").attr("disabled", false);
                $("#txtSgst").attr("disabled", false);
                $("#txtCgstp").attr("disabled", false);
                $("#txtCgst").attr("disabled", false);
                $("#txtTotal").attr("disabled", false);
                $("#txtPurchaseType").attr("disabled", false);
                $("#txtPaid").attr("disabled", false);
            } else {
                if (bill == 0) {
                    $("#txtAmount").attr("readonly", false);
                    $("#txtSgstp").attr("readonly", false);
                    $("#txtCgstp").attr("readonly", false);
                    $("#txtCgstp").attr("readonly", false);
                    $("#Expenses_vendor_id").val("");
                    $("#Expenses_vendor_id").attr("disabled", false);
                } else {
                    $("#txtAmount").val("");
                    $("#txtAmount").attr("readonly", true);
                    $("#txtSgstp").val("");
                    $("#txtSgstp").attr("readonly", true);
                    $("#txtCgstp").val("");
                    $("#txtCgstp").attr("readonly", true);
                    $("#txtIgstp").val("");
                    $("#txtCgstp").attr("readonly", true);
                    $("#Expenses_vendor_id").val("");
                    $("#Expenses_vendor_id").attr("disabled", true);
                }

                $("#Expenses_payment_type").attr("disabled", false);
                $("#txtReceipt").attr("disabled", false);
                $("#txtSgst").attr("readonly", true);
                $("#txtCgst").attr("readonly", true);
                $("#txtCgst").attr("readonly", true);
                $("#txtTotal").attr("readonly", true);
                $("#txtPurchaseType").val("");
                $("#txtPurchaseType").attr("disabled", true);
                $("#txtPaid").val("");
                $("#txtPaid").attr("disabled", true);
            }
        });
        $("#txtPurchaseType").change(function() {
            var purchaseType = $("#txtPurchaseType").val();
            var amount = parseFloat($("#txtTotal").val());
            var expType = $("#Expenses_expense_type").val();
            if (purchaseType == 1) {
                $.ajax({
                    method: "POST",
                    dataType: "json",
                    url: '<?php echo Yii::app()->createUrl('expenses/ajaxcall'); ?>',
                    success: function(result) {
                        $("#buttonsubmit").focus();
                    }
                });
                $("#txtPaid").val(0);
                $("#txtPaid").attr("readOnly", true);
                $("#txtTdsp").val("");
                $("#txtTdsp").attr("readonly", true);
                $("#txtTds1").text("");
                $("#txtTds").val("");
                $("#txtTdsPaid").html("<b>Amount To Be Paid: </b>0.00");
                $("#Expenses_paidamount").val("");
            } else if (purchaseType == 2) {

                if (expType == 88) {
                    $.ajax({
                        method: "POST",
                        dataType: "json",
                        url: '<?php echo Yii::app()->createUrl('expenses/ajaxcall'); ?>',
                        success: function(result) {
                            $("#Expenses_bank_id").select2("focus");
                        }
                    });

                } else if (expType == 89 || expType == 103) {
                    $.ajax({
                        method: "POST",
                        dataType: "json",
                        url: '<?php echo Yii::app()->createUrl('expenses/ajaxcall'); ?>',
                        success: function(result) {
                            $("#buttonsubmit").focus();
                        }
                    });
                }
                $("#txtPaid").val(amount.toFixed(2));
                $("#txtPaid").attr("readOnly", true);
                $("#txtTdsp").attr("readOnly", false);
                getTdsCalculations(amount.toFixed(2));
            } else if (purchaseType == 3) {
                $.ajax({
                    method: "POST",
                    dataType: "json",
                    url: '<?php echo Yii::app()->createUrl('expenses/ajaxcall'); ?>',
                    success: function(result) {
                        $("#txtPaid").focus();
                    }
                });
                $("#txtPaid").val(0);
                $("#txtPaid").attr("readOnly", false);
                $("#txtTdsp").attr("readOnly", false);
                getTdsCalculations(amount.toFixed(2));
            } else {
                $("#txtPaid").val("0");
                $("#txtPaid").attr("readOnly", false);
                $("#txtTdsp").attr("readOnly", false);
                getTdsCalculations(amount.toFixed(2));
            }

        });
        $("#Expenses_projectid").change(function() {

            var project_id = $("#Expenses_projectid").val();
            project_id = project_id ? project_id : 0;
            getDropdownList(project_id, 0, 0, '', 0);

            $("#txtAmount").attr("readonly", false);
            $("#txtSgstp").attr("readonly", false);
            $("#txtCgstp").attr("readonly", false);
            $("#txtIgstp").attr("readonly", false);
            $("#Expenses_vendor_id").attr("disabled", false);

        });
        $("#btnReset, [data-toggle='collapse']").click(function() {
            $("#txtDaybookId").val("");
            $("#buttonsubmit").text("ADD");
            $('#buyer-transaction-form').find('input, select, textarea').not("#BuyerTransactions_date").val('');
            $('#BuyerTransactions_company_id').val('').trigger('change.select2');
            $('#BuyerTransactions_buyer_id').val('').trigger('change.select2');
            $('#project_id').val('').trigger('change.select2');
            $('#BuyerTransactions_project_id').val('').trigger('change.select2');
            $('#flat_id').val('').trigger('change.select2');
            $('#BuyerTransactions_flat_id').val('').trigger('change.select2');
            $('#BuyerTransactions_invoice_no').val('').trigger('change.select2');

            $('#BuyerTransactions_transaction_type').val('').trigger('change.select2');
            $('#BuyerTransactions_amount').val('').trigger('change.select2');
            $('#BuyerTransactions_sgst_p').val('').trigger('change.select2');
            $('#BuyerTransactions_sgst_val').val('').trigger('change.select2');
            $('#BuyerTransactions_cgst_p').val('').trigger('change.select2');
            $('#BuyerTransactions_cgst_val').val('').trigger('change.select2');
            $('#BuyerTransactions_igst_p').val('').trigger('change.select2');
            $('#BuyerTransactions_igst_val').val('').trigger('change.select2');
        });
        $("#Expenses_expensetype_id").change(function() {
            var expId = $("#Expenses_expensetype_id").val();
            var expId = expId ? expId : 0;
            getVendorList(expId, 0);
        });
        $("#Expenses_projectid").click(function() {
            var project = $("#Expenses_projectid").val();
            if (project != "")
                $("#Expenses_bill_id").focus();
        });
        $("#Expenses_bill_id").click(function() {
            var typeId = $(this).find(':selected').data('id');
            var bill = $("#Expenses_bill_id").val();
            if (bill != "") {
                if (typeId == 1) {
                    $("#Expenses_expensetype_id").focus();
                } else {
                    $("#Expenses_expense_type").focus();
                }
            }

        });
        $("#Expenses_expensetype_id").click(function() {
            var exptypeid = $("#Expenses_expensetype_id").val();
            if (exptypeid != "")
                $("#Expenses_vendor_id").focus();
        });
        $("#Expenses_vendor_id").change(function() {
            var vendor = $("#Expenses_vendor_id").val();
            if (vendor != "")
                $.ajax({
                    method: "POST",
                    dataType: "json",
                    url: '<?php echo Yii::app()->createUrl('expenses/ajaxcall'); ?>',
                    success: function(result) {
                        $('#Expenses_expense_type').select2("focus");
                    }
                });
        });
        $("#Expenses_expense_type").change(function() {
            var bill_data = $("#Expenses_bill_id").val();
            var expType = $("#Expenses_expense_type").val();
            if (expType != "") {
                if (bill_data == 0) {
                    if (expType == 103) {
                        $.ajax({
                            method: "POST",
                            dataType: "json",
                            url: '<?php echo Yii::app()->createUrl('expenses/ajaxcall'); ?>',
                            success: function(result) {
                                $('#Expenses_employee_id').select2("focus");
                            }
                        });
                    } else {
                        $.ajax({
                            method: "POST",
                            dataType: "json",
                            url: '<?php echo Yii::app()->createUrl('expenses/ajaxcall'); ?>',
                            success: function(result) {
                                $('#txtAmount').focus();
                            }
                        });
                    }

                } else {
                    if (expType == 103) {
                        $.ajax({
                            method: "POST",
                            dataType: "json",
                            url: '<?php echo Yii::app()->createUrl('expenses/ajaxcall'); ?>',
                            success: function(result) {
                                $('#Expenses_employee_id').select2("focus");
                            }
                        });
                    } else {
                        $.ajax({
                            method: "POST",
                            dataType: "json",
                            url: '<?php echo Yii::app()->createUrl('expenses/ajaxcall'); ?>',
                            success: function(result) {
                                $('#txtDescription').focus();
                            }
                        });
                    }
                }
            }

        });

        $("#Expenses_employee_id").change(function() {
            var typeId = $("#Expenses_bill_id").find(':selected').data('id');
            if (typeId == 1 || typeId == 2) {
                if ($(this).val() != '') {
                    $.ajax({
                        method: "POST",
                        dataType: "json",
                        url: '<?php echo Yii::app()->createUrl('dailyexpense/ajaxcall'); ?>',
                        success: function(result) {
                            $("#txtDescription").focus();
                        }
                    });
                }
            } else {
                if ($(this).val() != '') {
                    $.ajax({
                        method: "POST",
                        dataType: "json",
                        url: '<?php echo Yii::app()->createUrl('dailyexpense/ajaxcall'); ?>',
                        success: function(result) {
                            $("#txtAmount").focus();
                        }
                    });
                }
            }
        });

        $("#txtDescription").keyup(function(e) {
            if (e.keyCode == 13) {
                var bills = $("#Expenses_bill_id").val();
                var expense_type = $("#Expenses_expense_type").val();
                var expense_head = $("#Expenses_expensetype_id").val();
                if (bills == 0) {
                    if (expense_head == "") {
                        if (expense_type != "") {
                            if (expense_type == 0) {
                                $("#buttonsubmit").focus();
                            } else {
                                $.ajax({
                                    method: "POST",
                                    dataType: "json",
                                    url: '<?php echo Yii::app()->createUrl('expenses/ajaxcall'); ?>',
                                    success: function(result) {
                                        $("#txtPurchaseType").select2("focus");
                                    }
                                });
                            }
                        }
                    } else {

                        $.ajax({
                            method: "POST",
                            dataType: "json",
                            url: '<?php echo Yii::app()->createUrl('expenses/ajaxcall'); ?>',
                            success: function(result) {
                                if (expense_type == 0) {
                                    $("#buttonsubmit").focus();
                                } else {
                                    $("#txtPurchaseType").select2("focus");
                                }
                            }
                        });
                    }
                } else {
                    $.ajax({
                        method: "POST",
                        dataType: "json",
                        url: '<?php echo Yii::app()->createUrl('expenses/ajaxcall'); ?>',
                        success: function(result) {
                            $("#txtPurchaseType").select2("focus");
                        }
                    });
                }
            }
        });
        $("#Expenses_payment_type").click(function() {
            var receipt = $("#Expenses_payment_type").val();
            if (receipt != "") {
                $("#txtReceipt").focus();
            }
        });
        $("#txtReceipt").keyup(function(e) {
            if (e.keyCode == 13) {
                var rType = $("#Expenses_payment_type").val();
                if (rType == 88)
                    $("#Expenses_bank_id").focus();
                else
                    $("#buttonsubmit").focus();
            }
        });
        $("#txtPaid").keyup(function(e) {
            if (e.keyCode == 13) {
                var expType = $("#Expenses_expense_type").val();
                if (expType == 88)
                    $("#Expenses_bank_id").focus();
                else
                    $("#buttonsubmit").focus();
            }
        });
        $("#Expenses_bank_id").change(function() {
            var bank = $("#Expenses_bank_id").val();
            if (bank != "")
                $.ajax({
                    method: "POST",
                    dataType: "json",
                    url: '<?php echo Yii::app()->createUrl('expenses/ajaxcall'); ?>',
                    success: function(result) {
                        $("#txtChequeno").focus();
                    }
                });
        });
        $("#txtChequeno").keyup(function(e) {
            if (e.keyCode == 13) {
                $("#buttonsubmit").focus();
            }
        });
        $("#Expenses_expense_type").change(function() {
            var expType = $("#Expenses_expense_type").val();
            if (expType == 88) {
                $("#Expenses_bank_id").attr("disabled", false);
                $("#txtChequeno").attr("readonly", false);
                $(".employee_box").hide();
                $("#Expenses_employee_id").attr("disabled", true);
            } else {
                if (expType == 103) {
                    $(".employee_box").show();
                    $("#Expenses_employee_id").attr("disabled", false);
                    $('#Expenses_bank_id').val("").trigger('change.select2');
                    $("#txtChequeno").val("");
                    $("#Expenses_bank_id").attr("disabled", true);
                    $("#txtChequeno").attr("readonly", true);
                } else {
                    $('#Expenses_bank_id').val("").trigger('change.select2');
                    $("#txtChequeno").val("");
                    $("#Expenses_bank_id").attr("disabled", true);
                    $("#txtChequeno").attr("readonly", true);
                    $(".employee_box").hide();
                    $("#Expenses_employee_id").attr("disabled", true);
                }
            }
            if (expType == 0) {
                $("#txtPurchaseType").html('<option value="1">Credit</option>');
                $("#txtPurchaseType").attr("disabled", true);
                $("#txtPaid").val(0);
                $("#txtPaid").attr("readOnly", true);
                $("#txtTdsp").val("");
                $("#txtTdsp").attr("readOnly", true);
                $("#txtTds1").text("");
                $("#txtTds").val("");
                $("#txtTdsPaid").html("<b>Amount To Be Paid: </b>0.00");
                $("#Expenses_paidamount").val("");
            } else {
                $("#txtPurchaseType").html('<option value="">-Select Purchase Type-</option><option value="2">Full Paid</option><option value="3">Partially Paid</option>');
                $("#txtPurchaseType").attr("disabled", false);
                $("#txtPaid").val("");
                $("#txtPaid").attr("readOnly", false);
                $("#txtTdsp").val("");
                $("#txtTdsp").attr("readOnly", false);
            }

        });
        $('#Expenses_company_id').change(function() {
            var company_id = this.value;
            $('#company_reset_value').val(company_id);

        })
    });

    function validateDateSelection(cDate) {
        var selDate = cDate;
        var currDate = new Date();
        currDate = $.format.date(currDate, "dd-MM-yyyy");
        if (selDate == currDate) {
            $("a#next").css("pointer-events", "none").css("color", "#ccccc");
        } else {
            $("a#next").css("pointer-events", "visible").css("color", "#337ab7");
        }
        $("#btnReset").click();
        $("#txtDaybookId").val("");
        $("#BuyerTransactions_date").val(selDate);
        //$("#Expenses_bill_id").html('<option value="">-Select Bill Number-</option>');
        $('#Expenses_bill_id').val('0').trigger('change.select2');
        $("#Expenses_expensetype_id").html('<option value="">-Select Expense Head-</option>');
        $("#Expenses_vendor_id").html('<option value="">-Select Vendor-</option>');
        $("#Expenses_vendor_id").attr("disabled", true);
    }

    function validateData() {
        var projectId = $("#Expenses_projectid").val();
        var billNumber = $("#Expenses_bill_id").val();
        var expenseType = $("#Expenses_expensetype_id").val();
        var vendorId = $("#Expenses_vendor_id").val();
        var amount = $("#txtAmount").val();
        var sgstP = $("#txtSgstp").val();
        var sgst = $("#txtSgst").val();
        var cgstP = $("#txtCgstp").val();
        var cgst = $("#txtCgst").val();
        var total = $("#txtTotal").val();
        var receiptType = $("#Expenses_payment_type").val();
        var receipt = $("#txtReceipt").val();
        var purchaseType = $("#txtPurchaseType").val();
        var paid = $("#txtPaid").val();
        var currId = $(this).attr(id);
        if (expenseType != "" || expenseType != 0) {
            $("#Expenses_payment_type").attr("disabled", true);
            $("#txtReceipt").attr("readonly", true);
            $("#Expenses_vendor_id").val("");
            $("#Expenses_vendor_id").attr("disabled", false);
            $("#txtAmount").attr("readonly", false);
            $("#txtSgstp").attr("readonly", false);
            $("#txtSgst").attr("readonly", false);
            $("#txtCgstp").attr("readonly", false);
            $("#txtCgst").attr("readonly", false);
            $("#txtTotal").attr("readonly", false);
        }
        if (currId == "Expenses_expensetype_id" && (expenseType == "" || expenseType == 0)) {
            $("#Expenses_payment_type").attr("disabled", false);
            $("#txtReceipt").attr("readonly", false);
        }
    }

    // tax calculation

    $("#txtAmount, #txtSgstp, #txtCgstp, #txtIgstp").blur(function() {

        var expType = $("#Expenses_expense_type").val();
        if (expType == 0) {
            $("#txtPurchaseType").html('<option value="1">Credit</option>');
        } else {
            $("#txtPurchaseType").html('<option value="">-Select Purchase Type-</option><option value="2">Full Paid</option><option value="3">Partially Paid</option>');
        }

        var amount = parseFloat($("#txtAmount").val());
        var sgstp = parseFloat($("#txtSgstp").val());
        var cgstp = parseFloat($("#txtCgstp").val());
        var igstp = parseFloat($("#txtIgstp").val());


        var sgst = (sgstp / 100) * amount;
        var cgst = (cgstp / 100) * amount;
        var igst = (igstp / 100) * amount;

        if (isNaN(sgst))
            sgst = 0;
        if (isNaN(cgst))
            cgst = 0;
        if (isNaN(igst))
            igst = 0;
        if (isNaN(amount))
            amount = 0;

        var total = amount + sgst + cgst + igst;
        var gsttotal = (parseFloat(sgst) + parseFloat(cgst) + parseFloat(igst)).toFixed(2);


        $("#txtSgst").val(sgst.toFixed(2));
        $("#txtCgst").val(cgst.toFixed(2));
        $("#txtIgst").val(igst.toFixed(2));
        $("#txtTotal").val(total.toFixed(2));

        $(".gstvalue").css({
            "display": "inline-block"
        });
        $("#txtSgst1").text(sgst.toFixed(2));
        $("#txtCgst1").text(cgst.toFixed(2));
        $("#txtIgst1").text(igst.toFixed(2));
        $("#txtTotal1").html("<b>Total: </b>" + total.toFixed(2) + " ");
        $("#txtgstTotal").html("<b>Total Tax: </b>" + gsttotal + " ");
        /*var pType   = $("txtPurchaseType").val();
        if(pType == 2) {
            $("#txtPaid").val(total.toFixed(2));
        } else {
            $("#txtPaid").val("");
        }*/
        getTdsCalculations(total);
    });
    $("#txtTdsp").blur(function() {
        var tdsp = parseFloat($("#txtTdsp").val());
        var total = parseFloat($("#txtTotal").val());
        var paid = parseFloat($("#txtPaid").val());
        var tds = (tdsp / 100) * paid;
        if (isNaN(tds))
            tds = 0;
        //alert(tds);
        var paidamt = paid - tds;
        $("#txtTds1").text(tds.toFixed(2));
        $("#txtTdsPaid").html("<b>Amount To Be Paid: </b>" + paidamt.toFixed(2) + "");
        $("#Expenses_paidamount").val(paidamt.toFixed(2));
        $("#txtTds").val(tds.toFixed(2));
        /* var pType   = $("#txtPurchaseType").val();
        if(pType == 2) {
            $("#txtPaid").val(paidamt.toFixed(2));
        } else {
            $("#txtPaid").val("");
        } */
    });
    $("#txtAmount").keyup(function(e) {
        if (e.keyCode == 13) {
            $("#txtSgstp").focus();
        }
    });


    $("#txtSgstp").keyup(function(e) {
        if (e.keyCode == 13) {
            $("#txtCgstp").focus();
        }
    });

    $("#txtCgstp").keyup(function(e) {
        if (e.keyCode == 13) {
            $("#txtIgstp").focus();
        }
    });

    $("#txtIgstp").keyup(function(e) {
        if (e.keyCode == 13) {
            $("#txtDescription").focus();
        }
    });

    $("#Expenses_company_id").change(function() {
        var company_id = $("#Expenses_company_id").val();
        if (company_id != "")
            $.ajax({
                method: "POST",
                dataType: "json",
                url: '<?php echo Yii::app()->createUrl('expenses/ajaxcall'); ?>',
                success: function(result) {
                    $("#Expenses_projectid").select2("focus");
                }
            });
    });



    $('.percentage').keyup(function() {
        if ($(this).val() > 100) {
            $(this).val('');
        }
    });
    // delete confirmation

    // delete restore

    // delete row
    $(document).on('click', '.delete_row', function(e) {
        var answer = confirm("Are you sure you want to delete?");
        if (answer) {
            var rowId = $(this).attr("id");
            var expId = $("#expenseid" + rowId).val();
            var date = $("#BuyerTransactions_date").val();
            //alert(expId);
            $.ajax({
                type: "POST",
                data: {
                    expId: expId,
                    expense_date: date
                },
                url: "<?php echo Yii::app()->createUrl("/buyer/buyers/deletetransaction") ?>",
                success: function(data) {
                    if (data == 1) {
                        $("#errormessage").show()
                            .html('<div class="alert alert-warning"><strong>Failed !</strong> please try again.</div>')
                            .fadeOut(5000);
                        $("#buttonsubmit").attr('disabled', false);
                        return false;
                    } else {
                        $("#errormessage").show()
                            .html('<div class="alert alert-success"><strong>Success!</strong> Row Deleted.</div>')
                            .fadeOut(5000);
                        $("#newlist").html(data);
                    }

                    $('#Expenses_projectid').val('').trigger('change.select2');
                    //$('#Expenses_bill_id').html('<option value="">-Select Bill Number-</option>');
                    $('#Expenses_bill_id').val('0').trigger('change.select2');
                    $('#Expenses_expensetype_id').html('<option value="">-Select Expense Head-</option>');
                    $('#txtPurchaseType').val("").trigger('change.select2');
                    $('#Expenses_expense_type').val('').trigger('change.select2');
                    $('#Expenses_employee_id').val('').trigger('change.select2');
                    $('#Expenses_bank_id').val('').trigger('change.select2');
                    $('#Expenses_vendor_id').html('<option value="">-Select Vendor-</option>');
                    $('#Expenses_company_id').val('').trigger('change.select2');
                    $("#Expenses_vendor_id").attr("disabled", true);
                    $("#txtAmount").attr("readonly", true);
                    $("#txtSgstp").attr("readonly", true);
                    $("#txtCgstp").attr("readonly", true);
                    $("#txtIgstp").attr("readonly", true);
                    $("#txtPurchaseType").attr("disabled", true);
                    $("#txtPaid").attr("readonly", true);
                    $("#Expenses_bank_id").attr("disabled", true);
                    $("#txtChequeno").attr("readonly", true);

                    $("#txtSgst1").text("");
                    $("#txtCgst1").text("");
                    $("#txtIgst1").text("");
                    $("#txtTotal1").html("");
                    $("#txtgstTotal").text("");
                }
            })
        }

    })

    $(document).on('click', '.approval_payment', function(e) {
        e.preventDefault();
        var rowId = $(this).attr("id");
        var expId = $("#expenseid" + rowId).val();
        var date = $("#BuyerTransactions_date").val();
        $.ajax({
            url: '<?php echo  Yii::app()->createAbsoluteUrl('expenses/expensesalert'); ?>',
            type: 'POST',
            //dataType:'json',
            data: {
                item_id: expId,
                expense_date: date
            },
            success: function(response) {
                if (response) {
                    $("#newlist").html(response);
                } else {
                    $("#errormessage").show()
                        .html('<div class="alert alert-warning"><strong>Failed !</strong> please try again.</div>')
                        .fadeOut(5000);
                    $("#buttonsubmit").attr('disabled', false);
                    return false;
                }
            }
        });
    });

    function getTdsCalculations(total) {
        if ($("#txtTdsp").val() != "") {
            var tdsp = parseFloat($("#txtTdsp").val());
        } else {
            var tdsp = 0;
        }

        var tdsamount = (tdsp / 100) * total;
        if (isNaN(tdsamount))
            tdsamount = 0;
        var paidamount = total - parseFloat(tdsamount);
        if (isNaN(paidamount))
            paidamount = 0;

        $("#txtTds1").text(tdsamount.toFixed(2));
        $("#txtTds").val(tdsamount.toFixed(2));
        $("#txtTdsPaid").html("<b>Amount To Be Paid: </b>" + paidamount.toFixed(2) + "");
        $("#Expenses_paidamount").val(paidamount.toFixed(2));
    }
</script>