<?php

class ExpensesImportController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view', 'downloaddoc', 'save'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new ExpensesImport;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['ExpensesImport'])) {
            $model->attributes = $_POST['ExpensesImport'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->exp_id));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['ExpensesImport'])) {
            $model->attributes = $_POST['ExpensesImport'];
            if ($model->save())
                $this->redirect(array('admin'));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('ExpensesImport');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    public function actionSave() {

        $project = "";
        $userid = "";
        $exptype = "";
        $tempstatus = array();
        

        $expids = explode(',', $_POST['theIds']);
        //print_r($expids);die;
        $arr = array();
        foreach ($expids as $expid) {
            $expense = ExpensesImport::model()->findAllByPk($expid);
            //print_r($expense);die;
            $project = $expense[0]['projectid'];
            $projectid = Projects::model()->findAll(array('condition' => 'pid = "' . $project . '"'));
            $model = new Expenses();
            if (!empty($projectid)) {
                $model->projectid = $projectid[0]['pid'];
            } else {
                $st = "Project not exists";
                array_push($tempstatus, $st);
            }
            $userid = $expense[0]['userid'];
            $user = Users::model()->findAll(array('condition' => 'userid = "' . $userid . '"'));
            if (!empty($user)) {
                $model->userid = $user[0]['userid'];
            } else {
                $st = "User not exists";
                array_push($tempstatus, $st);
            }
            $model->date = isset($expense[0]['exp_date']) ? $expense[0]['exp_date'] : "";
            $model->amount = isset($expense[0]['amount']) ? $expense[0]['amount'] : "";
            $model->description = isset($expense[0]['description']) ? $expense[0]['description'] : "";
            if ($expense[0]['type'] == 72 || $expense[0]['type'] == 73) {
                $model->type = isset($expense[0]['type']) ? $expense[0]['type'] : "";
            }
            if ($expense[0]['type'] == 73) {
                $exptype = $expense[0]['exptype'];
                $exp_type = ExpenseType::model()->find(array('condition' => 'type_name = "' . $exptype . '"'));
                if (!empty($exp_type)) {
                    $model->exptype = isset($exp_type['type_id']) ? $exp_type['type_id'] : "";
                } else {
                    $st = "Expense Type not exists";
                    array_push($tempstatus, $st);
                }
            } else {
                $model->exptype = "";
            }
            $model->vendor_id = isset($expense[0]['vendor_id']) ? $expense[0]['vendor_id'] : "";

            if ($expense[0]['payment_type']) {
                $paymenttype = Status::model()->findAll(array('condition' => 'status_type = "payment_type" and sid = ' . $expense[0]['payment_type']));
                if (!empty($paymenttype)) {
                    $model->payment_type = isset($expense[0]['payment_type']) ? $expense[0]['payment_type'] : "";
                } else {
                    $st = "Payment Type not exists";
                    array_push($tempstatus, $st);
                }
            }
            $model->works_done = isset($expense[0]['works_done']) ? $expense[0]['works_done'] : "";
            $model->materials = isset($expense[0]['materials']) ? $expense[0]['materials'] : "";
            $model->purchase_type = 1;
            $model->paid = isset($expense[0]['paid']) ? $expense[0]['paid'] : "";
            $model->created_by = yii::app()->user->id;
            $model->created_date = date('Y-m-d');
            $model->updated_by = yii::app()->user->id;
            $model->updated_date = date('Y-m-d H:i:s');
            if ($model->save()) {
                $id = $expense[0]['exp_id'];
                Yii::app()->user->setFlash('importstatus', "New Expense created!");
                ExpensesImport::model()->findByPk($id)->delete();
            } else {
                Yii::app()->user->setFlash('importstatus', "Error Occurred!");
                $newarr = array();
                foreach ($model->getErrors() as $err) {
                    foreach ($err as $er) {
                        array_push($newarr, $er);
                    }
                }
                $arr = array_merge($newarr, $tempstatus);
                $import_status = implode(",", $arr);
                $id = $expense[0]['exp_id'];
                $oldmodel = ExpensesImport::model()->findByPk($id);
                $oldmodel->import_status = $import_status;
                $oldmodel->save();
                $newarr = array();
                $tempstatus = array();
            }
        }
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        /* $model=new ExpensesImport('search');
          $model->unsetAttributes();  // clear any default values
          if(isset($_GET['ExpensesImport']))
          $model->attributes=$_GET['ExpensesImport'];

          $this->render('admin',array(
          'model'=>$model,
          )); */

        $model = new ImportForm();

        if (isset($_POST['ImportForm'])) {
            // print_r($_POST);die;
            $model->attributes = $_POST['ImportForm'];
            if ($model->validate()) {

                $csvFile = CUploadedFile::getInstance($model, 'file');
                $tempLoc = $csvFile->getTempName();
                try {

                    $transaction = Yii::app()->db->beginTransaction();
                    $handle = fopen("$tempLoc", "r");
                    $row = 1;
                    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                        //print_r($data);die;
                        if ($row > 1) {

                            $newmodel = new ExpensesImport;
                            $newmodel->projectid = $data[1];
                            $newmodel->userid = $data[2];

                            $daterep = str_replace('/', '-', $data[3]);
                            $newmodel->exp_date = isset($data[3]) ? Yii::app()->format->date(strtotime($daterep)) : "";
//print_r(Yii::app()->dateFormatter->format("y-MM-dd",strtotime($daterep)));die;
                            $newmodel->amount = $data[4];
                            $newmodel->description = $data[5];
                            $newmodel->type = $data[6];
                            $newmodel->exptype = $data[7];
                            $newmodel->vendor_id = $data[8];
                            $newmodel->payment_type = $data[9];
                            $newmodel->works_done = $data[10];
                            $newmodel->materials = $data[11];
                            $newmodel->purchase_type = $data[12];
                            $newmodel->paid = $data[13];
                            $newmodel->created_by = yii::app()->user->id;
                            $newmodel->created_date = date('Y-m-d');
                            $newmodel->updated_by = yii::app()->user->id;
                            $newmodel->updated_date = date('Y-m-d');
                            $newmodel->save();
                        }
                        $row++;
                    }
                    $transaction->commit();
                } catch (Exception $error) {
                    print_r($error);
                    $transaction->rollback();
                }
            }
            Yii::app()->user->setFlash('success', "Data saved!");
            $this->redirect(array('admin'));
        }
        $expmodel = new ExpensesImport('search');
        $this->render("admin", array('model' => $model, 'expmodel' => $expmodel));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return ExpensesImport the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = ExpensesImport::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param ExpensesImport $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'expenses-import-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionDownloaddoc() {
        $doc = 'sample_exp.csv';
        $path = Yii::getPathOfAlias('webroot') . '/uploads/';
        $file = $path . '' . $doc;
        if (file_exists($file)) {
            Yii::app()->getRequest()->sendFile($doc, file_get_contents($file));
        }
    }

}
