<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.min.js"></script>

<style type="text/css">
    table.total-table {
        font-size: 14px;
    }

    table.total-table div {
        text-align: right;
    }

    table.table .form-control {
        padding: 1px 1px;
        font-size: inherit;
        min-width: 70px;
    }

    table.table tr.pitems td div {
        padding-top: 8px;
    }

    table.table tr.pitems td div .fa {
        color: #060;
    }

    .formError .formErrorArrow div {
        display: none !important;
    }

    .formErrorContent {
        background-color: #333333 !important;
        border: 1px solid #ddd !important;
    }

    .delbil,
    .deleteitem {

        color: #000;
        background-color: #eee;
        padding: 3px;
        cursor: pointer;
    }

    .extracharge .select2 {
        width: 100% !important;
    }
</style>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/jquery.validationEngine.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/languages/jquery.validationEngine-en.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/validationEngine.jquery.min.css" />
<?php if (Yii::app()->user->hasFlash('success')) : ?><br>
    <div class="example1consolesucces">
        <?php echo Yii::app()->user->getFlash('success'); ?>
    </div>
<?php endif; ?>
<div class="form">
    <?php
    $tblpx = Yii::app()->db->tablePrefix;

    ?>

    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'bills-form',
        'enableAjaxValidation' => false,
    )); ?>
    <div class="">
        <div class="" id="alert"></div>
    </div>
    <div class="clearfix">
        <div class="box_holder">
            <div class="row">
                <div class="col-sm-3">
                    <?php echo $form->labelEx($model, 'company_id'); ?>
                    <?php
                    echo $form->dropDownList($model, 'company_id', CHtml::listData(Company::model()->findAll(array(
                        'select' => array('id, name'),
                        "condition" => 'id = ' . $model->company_id,
                        'order' => 'id',
                        'distinct' => true
                    )), 'id', 'name'), array('class' => 'form-control validate[required]'));

                    ?>
                    <?php echo $form->error($model, 'company_id'); ?>
                </div>
                <div class="col-sm-3">
                    <?php echo $form->labelEx($model, 'purchase_id'); ?>
                    <?php
                    echo $form->dropDownList($model, 'purchase_id', CHtml::listData(Purchase::model()->findAll(array(
                        'select' => array('p_id, purchase_no'),
                        "condition" => 'p_id = ' . $model->purchase_id,
                        'order' => 'p_id',
                        'distinct' => true
                    )), 'p_id', 'purchase_no'), array('class' => 'form-control validate[required]'));
                    ?>
                    <?php echo $form->error($model, 'purchase_id'); ?>
                </div>
                <div class="col-sm-3">
                    <?php echo $form->labelEx($model, 'bill_number'); ?>
                    <?php echo $form->textField($model, 'bill_number', array('class' => 'form-control validate[required]', 'readonly' => !$model->isNewRecord)); ?>
                    <?php echo $form->error($model, 'bill_number'); ?>
                </div>
                <div class="col-sm-3">
                    <?php echo $form->labelEx($model, 'bill_date'); ?>
                    <?php echo CHtml::activeTextField($model, 'bill_date', array('readonly' => 'true', "value" => (($model->isNewRecord) ? date('d-M-Y') : date('d-M-Y', strtotime($model->bill_date))), 'size' => 10, 'class' => 'form-control')); ?>
                    <?php
                    $this->widget('application.extensions.calendar.SCalendar', array(
                        'inputField' => 'Bills_bill_date',
                        'ifFormat' => '%d-%b-%Y',
                    ));
                    ?>
                    <?php echo $form->error($model, 'bill_date'); ?>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="warehousestock_warehouseid" class="required">Warehouse
                            <!-- <span class="required">*</span> -->
                        </label>
                        <?php
                        if (yii::app()->user->role == 5) {
                            $warehouse_data = Controller::getCategoryOptions($type = 1);
                            $data = CHtml::listData(Controller::getCategoryOptions($type = 1), 'id', 'text', 'group');
                        } else {
                            $warehouse_data = Controller::getCategoryOptions();
                            $data = CHtml::listData(Controller::getCategoryOptions(), 'id', 'text', 'group');
                        }
                        if (count($warehouse_data) == 1) {
                            $warehouse_selected = $warehouse_data[0]['id'];
                        } else {
                            $warehouse_selected = $model['warehouse_id'];
                        }
                        $res = Warehousereceipt::model()->findAll(array('condition' => 'warehousereceipt_bill_id = "' . $model->bill_id .  '"'));
                        $warehouseReceiptCreated = count($res) > 0;
                       
                        ?>
                        <?php if ($warehouseReceiptCreated) {
                        echo CHtml::activeDropDownList($model2, 'warehousestock_warehouseid', $data, array(
                            'readonly' => 'true',
                            'empty' => '-Choose a Warehouse-',
                            'class' => 'form-control mandatory js-example-basic-single',
                            'options' => array($warehouse_selected => array('selected' => true)),
                            'disabled' => 'disabled', // This line disables the dropdown
                        ));
                    } else {
                        echo CHtml::activeDropDownList($model2, 'warehousestock_warehouseid', $data, array(
                            'empty' => '-Choose a Warehouse-',
                            'class' => 'form-control mandatory js-example-basic-single',
                            'options' => array($warehouse_selected => array('selected' => true)),
                        ));
                    } ?>

                        <div class="errorMessage" id="Warehousestock_warehousestock_warehouseid_em_" style='display:none;'></div>

                    </div>
                </div>
            </div>
        </div>
        <div id="client" class="mt"><?php echo ((!$model->isNewRecord) ? $client : '') ?></div>
        <?php

        $addchargesall   =  Yii::app()->db->createCommand("SELECT * FROM " . $tblpx . "additional_bill WHERE `bill_id`=" . $model->bill_id)->queryAll();
        ?>

        <div class="extracharge">
            <h4>Additional Charges <span class="icon icon-plus addcharge"></span></h4>
            <?php if (!empty($addchargesall)) {
                foreach ($addchargesall as $data) { ?>
                    <div> <span style="font-weight: bold;"><?= $data['category'] ?></span> : <span class='totalprice1'><?= Controller::money_format_inr($data['amount'], 2) ?></span>&nbsp;&nbsp;<span class='icon icon-trash delbil' title='Delete' id="<?= $data['id'] ?>"></span></div>
            <?php }
            } ?>
            <div class="chargeadd box_holder" style="display:none;">
                <div class="row">
                    <div class="col-md-2">
                        <input type="text" placeholder="Charge Category" name="packagecharge" id="labelcat" class="form-control textbox" />
                    </div>
                    <div class="col-md-2">
                        <input type="number" placeholder="Enter Amount" name="packagecharge" id="currentval" class="form-control textbox currentval" />
                    </div>
                    <div class="col-md-2">
                        <input type="button" class=" btn btn-sm btn-primary savecharge" value="save" />
                    </div>
                </div>
            </div>
            <div id="addtional_list"></div>
        </div>

        <div class="extracharge">
            <?php echo $this->renderPartial('_additional_item_form', array()); ?>
        </div>

        <div class="row">
            <div class="new_items">
                <?php $this->renderPartial('_additional_item_list', array('items' => $additional_items,)); ?>
            </div>
        </div>

        <div class="data-amnts box_holder mt">
            <div>
                <label class="inline">Amount:</label>
                <span id="Bills_bill_amount"> <?php echo $model->bill_amount ?  Yii::app()->Controller->money_format_inr($model->bill_amount, 2) : 0; ?></span>
            </div>
            <div>
                <label class="inline">Discount Amount:</label>
                <span id="Bills_bill_discountamount"> <?php echo $model->bill_discountamount ? Yii::app()->Controller->money_format_inr($model->bill_discountamount, 2) : 0; ?></span>
            </div>
            <div>
                <label class="inline">Tax Amount:</label>
                <span id="Bills_bill_taxamount"> <?php echo $model->bill_taxamount ? Yii::app()->Controller->money_format_inr($model->getBillTaxTotal($model->bill_id), 2) : 0; ?></span>
            </div>

            <?php
            $addcharges  =  Yii::app()->db->createCommand("SELECT ROUND(SUM(amount), 2) as amount FROM " . $tblpx . "additional_bill WHERE `bill_id`=" . $model->bill_id)->queryRow();

            $bill_amount = str_replace(',', '', $model->bill_totalamount);
            if (!empty($addcharges['amount'])) {
                $model->bill_totalamount =  $bill_amount + $addcharges['amount'] ;
            }
            
            ?>
            <div <?php if (empty($addcharges['amount'])) { ?> style="display: none;" <?php } ?> class="additional_charge">
                <label class="inline">Additional charges :</label>
                <span id="totaladditional_charge"><?= (!empty($addcharges)) ? $addcharges['amount'] : 0; ?></span>
            </div>
            <div>
                <label class="inline">Round Off:</label>
                <input type="text" name="Bills[round_off]" id="Bills_round_off" value="<?php echo $model->round_off ? $model->round_off : 0; ?>" style="width: 100px; border-radius: 05px;" class="text-right">
            </div>
            <div>
                <label class="inline">Total Amount:</label>
                <input type="text" name="Bills[bill_totalamount]" id="Bills_bill_totalamount" value="<?php echo $model->bill_totalamount ? Yii::app()->Controller->money_format_inr($model->bill_totalamount, 2) : 0; ?>" style="border: none;width:100px;" class="text-right" readonly=true>
            </div>
        </div>
        <input type="hidden" name="billid" id="billid" value="<?php echo $model["bill_id"]; ?>" />
        <div class="row">
            <div class="col-sm-12 text-center"><?php echo CHtml::Button($model->isNewRecord ? 'Create' : 'Save', array('class' => 'buttonsubmit btn btn-primary', 'id' => 'buttonsubmit')); ?></div>
        </div>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->


<script>
    <?php $urlUpdate = Yii::app()->createAbsoluteUrl("bills/updateItemsToList"); ?>
    <?php $addUrl    = Yii::app()->createAbsoluteUrl("bills/addItemsForBills"); ?>
    <?php $addbillurl = Yii::app()->createAbsoluteUrl("bills/additionalbill"); ?>
    <?php $delbillurl = Yii::app()->createAbsoluteUrl("bills/deletebill1"); ?>
    <?php $approveUrl    = Yii::app()->createAbsoluteUrl("bills/approvebills"); ?>

    $(".js-example-basic-single").select2();

    $(document).ready(function() {
        $(".tax_slab").select2();
        $("#bills-form").validationEngine({
            'custom_error_messages': {
                'custom[number]': {
                    'message': 'Invalid number'
                }
            }
        });
        $('.addcharge').click(function() {
            $(".chargeadd").toggle();
            $(this).toggleClass("icon-minus hidecharge");
        });
        $(".delbil").click(function() {
            $('.loading-overlay').addClass('is-active');
            var id = $(this).attr('id');
            var billid = $("#billid").val();
            $.ajax({
                url: "<?php echo $delbillurl; ?>",
                data: {
                    id: id,
                    billid: billid
                },
                type: "POST",
                success: function(data) {
                    window.location.reload();
                }
            });
        });
        $(".savecharge").on('keyup keypress keydown click', function(e) {
            var label = $("#labelcat").val();
            var currentval = $("#currentval").val();
            var billid = $("#billid").val();
            var totalamount = $("#Bills_bill_totalamount").val().replace(/,/g, '');
            totalamount = parseFloat(totalamount).toFixed(2);
            console.log(totalamount);
            var addtot = parseFloat($("#totaladditional_charge").text());
            if (billid == '') {
                $('.errorcharge').html('Please add item');

            } else if (label != '' && currentval != '') {
                $(".savecharge").attr('disabled', 'disabled');
                $('.loading-overlay').addClass('is-active');
                $.ajax({
                    url: "<?php echo $addbillurl; ?>",
                    data: {
                        category: label,
                        amount: currentval,
                        billid: billid
                    },
                    type: "POST",
                    dataType: 'json',
                    success: function(data) {
                        $(".savecharge").removeAttr('disabled');
                        if (data != '') {
                            $("#addtional_list").append("<div>" + label + " : " + "<span class='totalprice'>" + parseFloat(currentval).toFixed(2) + "</span> &nbsp;&nbsp&nbsp;<span class='icon icon-trash deleteitem' title='Delete' id=" + data.itemid + "></span> " + "</div>");
                            $(".additional_total").show();
                            $('#labelcat').val('');
                            $('#currentval').val('');
                            if (isNaN(addtot)) {
                                var totaladd = parseFloat(currentval).toFixed(2);
                            } else {
                                var totaladd = parseFloat(addtot) + parseFloat(currentval);
                                totaladd = parseFloat(totaladd).toFixed(2);;
                            }

                            $('.additional_charge').show();
                            $("#totaladditional_charge").text(totaladd);
                            var tot = parseFloat(totalamount) + parseFloat(currentval);

                            $("#Bills_bill_totalamount").val(tot.toFixed(2));

                            $(".deleteitem").click(function() {
                                var billid = $("#billid").val();
                                $(this).parent().remove();

                                var id = $(this).attr('id');
                                $('.loading-overlay').addClass('is-active');
                                $.ajax({
                                    url: "<?php echo $delbillurl; ?>",
                                    data: {
                                        id: id,
                                        billid: billid
                                    },
                                    type: "POST",
                                    success: function(data) {
                                        $(".savecharge").removeAttr('disabled');
                                        window.location.reload();

                                    }
                                });
                            });

                        }
                    }
                });
            } else {

                if (label == '' || currentval == '') {
                    $('.errorcharge').html('Both fields are required');
                }
            }
        });
        $("#biquantity0").focus();

        function resizeInput() {
            $(this).attr('size', $(this).val().length);
        }

        $('input[type="text"]')

            .keyup(resizeInput)

            .each(resizeInput);



        $("body").on('change', '.table tr.pitems input,.table tr.pitems select.purchase_unitvendor, .table tr.pitems select.tax_slab', function(e) {
            var rowId = $(this).parents("td").attr("id");

            if (($(this).hasClass("biquantity")) || ($(this).hasClass("birate")) || ($(this).hasClass("purchase_unitvendor"))) {
                var type = 1;
                getconversionFactor(rowId, type);

            }

            if (($(this).hasClass("basequantity"))) {
                var type = 2;
                getconversionFactor(rowId, type);
            }
            var billNo = $("#Bills_bill_number").val();
            var quantity = parseFloat($("#biquantity" + rowId).val());
            var amount = parseFloat($("#biamount" + rowId).text());
            var damount = parseFloat($("#damount" + rowId).val());
            var dpercent = parseFloat($("#dpercent" + rowId).text());
            var newDAmt = amount * (dpercent / 100);
            var warehouseid = $('#Warehousestock_warehousestock_warehouseid').val();
            if (newDAmt != 0)
                newDAmt = newDAmt.toFixed(2);
            var availQty = parseFloat($("#availablequantity" + rowId).text());
            if (amount < damount && (amount != 0 || quantity == 0)) {
                $("#alert").addClass("alert alert-danger").html("<strong>Discount amount must be less than the amount<strong>");
                $("#damount" + rowId).val(newDAmt);
            } else if (billNo == "") {
                $("#alert").addClass("alert alert-danger").html("<strong>Please enter bill number<strong>");
                return false;
            } else if (warehouseid == "") {
                $("#alert").addClass("alert alert-danger").html("<strong>Please Choose Warehouse<strong>");
                return false;

            } else {
                $("#alert").removeClass("alert alert-danger").html("");
                validateBillNumber(billNo, rowId, 2);
            }
        });

        $('#bills-form').on('keypress', ':input', function(event) {
            if (event.keyCode == 13) {
                var inputs = $(this).parents("form").eq(0).find(":input");
                var idx = inputs.index(this);
                if (idx == inputs.length - 1) {} else {
                    inputs[idx + 1].focus();
                    inputs[idx + 1].select();
                }
                return false;
            }
        });
        $("#Bills_bill_date").keypress(function(e) {
            if (e.keyCode == 13) {
                $("#biquantity0").focus();
                $("#biquantity0").select();
            }
        });
        $('#bills-form').on('keypress', '.lnext', function(e) {
            if (e.keyCode == 13) {
                var rowId = parseInt($(this).parent().attr("id"));
                var totrows = $("#totrows").val();
                var nextId = rowId + 1;
                if (nextId == totrows) {
                    $("#buttonsubmit").focus();
                } else {
                    $("#biquantity" + nextId).focus();
                    $("#biquantity" + nextId).select();
                }
            }
        });


        $("#buttonsubmit").keypress(function(e) {
            if (e.keyCode == 13) {
                $('.buttonsubmit').click();
            }
        });

        $(".buttonsubmit").click(function() {
            $("#bills-form").submit();
        })

        $('#Bills_round_off').change(function() {
            var roundOff = parseFloat(this.value);
            var amount = $("#Bills_bill_amount").text().replace(/,/g, '');
            amount = parseFloat(amount);
            var disc_amount = parseFloat($("#Bills_bill_discountamount").text());
            var tax_amount = $("#Bills_bill_taxamount").text().replace(/,/g, '');
            tax_amount = parseFloat(tax_amount);
            var additional_charge = $("#totaladditional_charge").text().replace(/,/g, '');
            additional_charge = parseFloat(additional_charge);
            disc_amount = isNaN(disc_amount) ? 0 : disc_amount;
            tax_amount = isNaN(tax_amount) ? 0 : tax_amount;
            additional_charge = isNaN(additional_charge) ? 0 : additional_charge;
            roundOff = isNaN(roundOff) ? 0 : roundOff;
            var total_amount = (amount + tax_amount + (roundOff) + additional_charge) - disc_amount;
            $("#Bills_bill_totalamount").val(parseFloat(total_amount).toFixed(2));
        });
    });

    function addItemToBills(rowId, aStat) {
        $(".formError").hide();
        var purchaseId = $("#Bills_purchase_id").val();
        var billitembatch = $("#bibatch" + rowId).val();
        var company_id = $("#Bills_company_id").val();
        var billNumber = $("#Bills_bill_number").val();
        var billDate = $("#Bills_bill_date").val();
        var totrows = parseInt($("#totrows").val());
        var billId = $("#billid").val();
        var itemId = parseInt($("#ids" + rowId).val());
        var quantity = parseFloat($("#biquantity" + rowId).val());
        quantity = isNaN(quantity) ? 0 : quantity;
        $("#biquantity" + rowId).val(quantity);
        var hsn_code = $("#hsn_code" + rowId).text();
        var unit = $("#purchase_unitvendor" + rowId + " option:selected").text();
        var rate = parseFloat($("#birate" + rowId).val());
        var orRate = parseFloat($("#orrate" + rowId).val());
        var purch_unit = $("#biunit" + rowId).text();

        var Warehouse_id = $('#Warehousestock_warehousestock_warehouseid').val();
        if (Warehouse_id == "") {
            $('#Warehousestock_warehousestock_warehouseid_em_').show();
            $('#Warehousestock_warehousestock_warehouseid_em_').html('Warehouse is required');
        } else {
            $('#Warehousestock_warehousestock_warehouseid_em_').hide();
            $('#Warehousestock_warehousestock_warehouseid_em_').html('');
        }
        rate = isNaN(rate) ? orRate : rate;
        if (rate < 0)
            rate = orRate;
        if (quantity % 1 == 0)
            quantity = quantity;
        else
            quantity = quantity.toFixed(2);
        $("#biquantity" + rowId).val(quantity)
        if (rate != 0)
            rate = parseFloat(rate.toFixed(2));
        $("#birate" + rowId).val(rate);

        if (purch_unit != unit) {

            var purchasequantity = $("#basequantity" + rowId).val();
            var purchaseunit = $.trim(purch_unit);
            var purchaserate = $("#baserate" + rowId).text();
        } else {

            var purchasequantity = quantity;
            var purchaseunit = $.trim(purch_unit);
            var purchaserate = rate;
        }


        var amount = parseFloat($("#biamount" + rowId).text());
        var damount = parseFloat($("#damount" + rowId).val());
        damount = isNaN(damount) ? 0 : damount;
        if (damount < 0)
            damount = 0;
        if (damount != 0)
            damount = parseFloat(damount.toFixed(2));
        $("#damount" + rowId).val(damount);
        var dpercent = parseFloat($("#dpercent" + rowId).text());
        var cgstpercent = parseFloat($("#cgstpercent" + rowId).val());
        cgstpercent = isNaN(cgstpercent) ? 0 : cgstpercent;
        if (cgstpercent < 0)
            cgstpercent = 0;
        if (cgstpercent != 0)
            cgstpercent = parseFloat(cgstpercent.toFixed(2));
        $("#cgstpercent" + rowId).val(cgstpercent);
        var cgst = parseFloat($("#cgst" + rowId).text());
        var sgstpercent = parseFloat($("#sgstpercent" + rowId).val());
        sgstpercent = isNaN(sgstpercent) ? 0 : sgstpercent;
        if (sgstpercent < 0)
            sgstpercent = 0;
        if (sgstpercent != 0)
            sgstpercent = parseFloat(sgstpercent.toFixed(2));
        $("#sgstpercent" + rowId).val(sgstpercent);

        var igstpercent = parseFloat($("#igstpercent" + rowId).val());
        igstpercent = isNaN(igstpercent) ? 0 : igstpercent;
        if (igstpercent < 0)
            igstpercent = 0;
        if (igstpercent != 0)
            igstpercent = parseFloat(igstpercent.toFixed(2));
        $("#igstpercent" + rowId).val(igstpercent);

        var sgst = parseFloat($("#sgst" + rowId).text());
        var igst = parseFloat($("#igst" + rowId).text());
        var taxtotal = parseFloat($("#taxamount" + rowId).text());
        var taxpercent = parseFloat($("#taxpercent" + rowId).text());
        var totalamount = parseFloat($("#totalamount" + rowId).text());
        var categoryId = $("#category" + rowId).val();
        var billTotal = parseFloat($("#Bills_bill_amount").text());
        var billDiscount = parseFloat($("#Bills_bill_discountamount").text());
        var billTax = parseFloat($("#Bills_bill_taxamount").text());
        var billGTotal = parseFloat($("#Bills_bill_totalamount").val());
        var categoryName = $("#bicategoryname" + rowId).text();
        var availQty = parseFloat($("#availablequantity" + rowId).text());
        var actualquantity = parseFloat($("#actualquantity" + rowId).text());
        var billItem = $("#billitem" + rowId).val();
        var tax_slab = parseFloat($("#tax_slab" + rowId).val());

        var length = $("#bilength" + rowId).text();
        var width = $("#biwidth" + rowId).text();
        var height = $("#biheight" + rowId).text();
        var purchase_type = $("#purchase_type").val();


        if (quantity > 0) {

            if (purchase_type == 'A') {
                var newAmt = (quantity * rate * length).toFixed(2);
            } else if (purchase_type == 'G') {
                var newAmt = (quantity * rate * width * height).toFixed(2);
            } else if (purchase_type == 'O') {
                var newAmt = (quantity * rate).toFixed(2);
            }

            var newDp = ((damount / newAmt) * 100);
            if (newDp % 1 !== 0)
                newDp = newDp.toFixed(2);
            var newAmtD = newAmt - damount;
            var newCgst = ((cgstpercent / 100) * newAmtD);
            if (newCgst % 1 !== 0)
                newCgst = newCgst.toFixed(2);
            var newSgst = ((sgstpercent / 100) * newAmtD);
            if (newSgst % 1 !== 0)
                newSgst = newSgst.toFixed(2);
            var newIgst = ((igstpercent / 100) * newAmtD);
            if (newIgst % 1 !== 0)
                newIgst = newIgst.toFixed(2);

            var newTotalTax = parseFloat(newCgst) + parseFloat(newSgst) + parseFloat(newIgst);
            newTotalTax = newTotalTax.toFixed(2);
            var newTaxP = ((newTotalTax / newAmtD) * 100);
            if (newTaxP % 1 !== 0)
                newTaxP = newTaxP.toFixed(2);

            if (rate == 0) {

            }
            newAmt = isNaN(newAmt) ? 0 : newAmt;
            newDp = isNaN(newDp) ? 0 : newDp;
            newCgst = isNaN(newCgst) ? 0 : newCgst;
            newSgst = isNaN(newSgst) ? 0 : newSgst;
            newIgst = isNaN(newIgst) ? 0 : newIgst;
            newTaxP = isNaN(newTaxP) ? 0 : newTaxP;

            var newTotal = parseFloat(newAmtD) + parseFloat(newTotalTax);
            newTotal = isNaN(newTotal) ? 0 : newTotal;
            newTotal = newTotal.toFixed(2);
        } else {
            var newAmt = 0;
            var newDp = 0;
            var newAmtD = 0;
            var newCgst = 0;
            var newSgst = 0;
            var newIgst = 0;
            var newTotalTax = 0;
            var newTaxP = 0
            var newTotal = 0;
        }
        $("#biamount" + rowId).text(newAmt);
        $("#dpercent" + rowId).text(newDp);
        $("#cgst" + rowId).text(newCgst);
        $("#sgst" + rowId).text(newSgst);
        $("#igst" + rowId).text(newIgst);
        $("#taxamount" + rowId).text(newTotalTax);
        $("#taxpercent" + rowId).text(newTaxP);
        $("#totalamount" + rowId).text(newTotal);
        var billNTotal = 0;
        var billNDiscount = 0;
        var billNTax = 0;
        var billNGTotal = 0;
        for (var i = 0; i < totrows; i++) {
            if ($("#biquantity" + i).val() > 0) {
                billNTotal = billNTotal + parseFloat($("#biamount" + i).text());
                billNDiscount = billNDiscount + parseFloat($("#damount" + i).val());
                billNTax = billNTax + parseFloat($("#taxamount" + i).text());
                billNGTotal = billNGTotal + parseFloat($("#totalamount" + i).text());
            }
        }
        console.log(billNTotal);

        if (quantity == 0 && billItem == "") {
            var opStatus = 0;
        } else if (quantity == 0 && billItem != "") {
            var opStatus = 2;
        } else if (quantity > 0 && billItem == "") {
            var opStatus = 1;
        } else if (quantity > 0 && billItem != "") {
            var opStatus = 3;
        } else {
            var opStatus = 2;
        }
        var tqty = parseFloat($("#tqty").val());
        var btqty = parseFloat($("#btqty").val());
        var newQT = 0;
        for (var i = 0; i < totrows; i++) {
            newQT = newQT + parseFloat($("#biquantity" + i).val());
        }
        var newTotalQty = (btqty + newQT) - $("#savedquantity" + rowId).val();
        if (tqty > newTotalQty)
            var purchaseStatus = 94;
        else
            var purchaseStatus = 93;
        $('.loading-overlay').addClass('is-active');
        $.ajax({
            url: "<?php echo $addUrl; ?>",
            data: {
                "pid": purchaseId,
                "billnumber": billNumber,
                "billdate": billDate,
                "billitembatch": billitembatch,
                "billamount": billNTotal,
                "billdiscount": billNDiscount,
                "billtax": billNTax,
                "billtotal": billNGTotal,
                "itemid": itemId,
                "quantity": quantity,
                "unit": unit,
                "rate": rate,
                "purchasequantity": purchasequantity,
                "purchaseunit": purchaseunit,
                "purchaserate": purchaserate,
                "amount": newAmt,
                "damount": damount,
                "dpercent": newDp,
                "cgst": newCgst,
                "cgstpercent": cgstpercent,
                "sgst": newSgst,
                "sgstpercent": sgstpercent,
                "igst": newIgst,
                "igstpercent": igstpercent,
                "totaltax": newTotalTax,
                "totaltaxp": newTaxP,
                "totalamount": newTotal,
                "billid": billId,
                "astat": opStatus,
                "categoryid": categoryId,
                "categoryname": categoryName,
                "availqty": availQty,
                "billitem": billItem,
                "purchasestatus": purchaseStatus,
                "company_id": company_id,
                "tax_slab": tax_slab,
                'actualquantity': actualquantity,
                "hsn_code": hsn_code,
                "length": length,
                "width": width,
                "height": height,
                "Warehouse_id": Warehouse_id
            },
            type: "POST",
            success: function(data) {
                var result = JSON.parse(data);
                if (result.amount !== '') {
                    $('#Bills_bill_amount').text(result.amount.toFixed(2));
                }
                if (result.disc_amount !== '') {
                    $('#Bills_bill_discountamount').text(result.disc_amount.toFixed(2));
                }
                if (result.tax_amount !== '') {
                    $('#Bills_bill_taxamount').text(result.tax_amount.toFixed(2));
                }
                if (result.tot_amount !== '') {
                    $('#Bills_bill_totalamount').val(result.tot_amount.toFixed(2));
                }

                $("#billid").val(result[0]);
                $("#billitem" + rowId).val(result[1]);
                $("#savedquantity" + rowId).val(quantity);
                if (quantity > 0) {
                    $("#tickmark" + rowId).html('<i class="fa fa-check" aria-hidden="true"></i>');
                } else {
                    $("#tickmark" + rowId).html("");
                }
            },
            error: function() {
                $("#save_status" + rowId).val(2);
            }
        });
    }

    function validateBillNumber(billNo, rowId, stat) {
        var aStat;
        if (stat == 1) {
            if ($("input[name='chkitem[" + rowId + "]']").is(':checked')) {
                aStat = 1;
            } else {
                aStat = 2;
            }
            addItemToBills(rowId, aStat);
        } else if (stat == 2) {
            if ($("input[name='chkitem[" + rowId + "]']").is(':checked')) {
                aStat = 3;
            } else {
                aStat = 4;
            }
            addItemToBills(rowId, aStat);
        }
    }

    function approveQuantity(elm) {

        if (confirm('Are you sure,Want to approve ?')) {


            var id = $(elm).attr('data-id');
            $('.loading-overlay').addClass('is-active');
            $.ajax({
                url: "<?php echo $approveUrl; ?>",
                data: {
                    id: id
                },
                type: "POST",
                dataType: "json",
                success: function(data) {
                    if (data.status == 1) {
                        $('.stsmsg').show();
                        $('.stsmsg').html(data.msg);
                        // setTimeout(function() {
                        window.location.reload();
                        // }, 1000);
                    } else if (data.status == 0) {
                        $("#alert").addClass("alert alert-danger").html(data.msg);
                    }


                }
            });


        }
    }
    $(document).ajaxComplete(function() {
        $('.loading-overlay').removeClass('is-active');
        $('#loading').hide();
    });

    function getconversionFactor(rowId, type) {

        var itemId = parseInt($("#category" + rowId).val());
        var base_unit = $.trim($("#biunit" + rowId).text());
        var amt = $("#biamount" + rowId).text();
        var quantity = parseFloat($("#biquantity" + rowId).val());
        quantity = isNaN(quantity) ? 0 : quantity;
        var purchase_unit = $("#purchase_unitvendor" + rowId + " option:selected").text();
        var rate = parseFloat($("#birate" + rowId).val());
        var orRate = parseFloat($("#orrate" + rowId).val());
        rate = isNaN(rate) ? orRate : rate;
        if (rate < 0)
            rate = orRate;
        if (quantity % 1 == 0)
            quantity = quantity;
        else
            quantity = quantity.toFixed(4);
        if (rate != 0)
            rate = parseFloat(rate.toFixed(2));

        var length = $("#bilength" + rowId).text();
        var width = $("#biwidth" + rowId).text();
        var height = $("#biheight" + rowId).text();
        var purchase_type = $("#purchase_type").val();
        var baseqty = parseFloat($("#basequantity" + rowId).val());
        var baserate = 0;



        if (quantity > 0) {
            if (purchase_type == 'A') {
                var amt = (quantity * rate * length).toFixed(2);
            } else if (purchase_type == 'G') {
                var amt = (quantity * rate * width * height).toFixed(2);
            } else if (purchase_type == 'O') {
                var amt = (quantity * rate).toFixed(2);
            }
        } else {
            amt = 0;
        }

        if (purchase_unit !== '' && base_unit !== '' && itemId !== "") {
            if (base_unit != purchase_unit) {

                $.ajax({
                    method: "POST",
                    data: {
                        'purchase_unit': purchase_unit,
                        'base_unit': base_unit,
                        'item_id': itemId
                    },
                    dataType: "json",
                    url: '<?php echo Yii::app()->createAbsoluteUrl('bills/getUnitconversionFactor'); ?>',
                    success: function(result) {

                        if (type == 1) {
                            var base_quantity = parseFloat(result * quantity).toFixed(2);
                        } else {
                            var base_quantity = baseqty;
                        }

                        if (isNaN(base_quantity))
                            base_quantity = 0;
                        if (base_quantity != 0) {
                            baserate = amt / base_quantity;

                            baserate = baserate.toFixed(2);
                            if (isNaN(baserate))
                                baserate = 0;
                        }
                        $("#basequantity" + rowId).attr("readonly", false);
                        $("#basequantity" + rowId).val(base_quantity);
                        $("#baserate" + rowId).text(baserate);
                    }
                });
            } else {

                var baseval = 0;
                $("#basequantity" + rowId).attr("readonly", true);
                $("#basequantity" + rowId).val(quantity);
                $("#baserate" + rowId).text(rate);
            }
        }
    }
</script>

<?php $url = Yii::app()->createAbsoluteUrl("bills/getItemsByPurchase"); ?>
<?php Yii::app()->clientScript->registerScript('myscript', '
$(document).ready(function(){
    /*var p_id = $("#Bills_purchase_id").val();
    if(p_id != "") {
        getAllItems(p_id);
    }
    getAllItems();*/
    $("#Bills_purchase_id").change(function(){
        var p_id= $("#Bills_purchase_id").val();
        if(p_id == "")
            p_id = 0;
        getAllItems(p_id);
         
    });
});
function getAllItems(p_id) {
        $.ajax({
           url: "' . $url . '",
            data: {"id": p_id}, 
            //dataType: "json",
            type: "GET",
            success:function(data){
                //alert(data);
                $("#client").html(data); 
                //var amount = $("#aj-amount").val();
                //alert(amount);
                //$("#Bills_bill_amount").val(amount);
                //$("#Bills_bill_totalamount").val(amount);
            }
        });
}
        '); ?>