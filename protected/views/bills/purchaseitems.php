<?php
/* @var $this BillsController */
/* @var $model Bills */
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<style type="text/css">
    tr.pitems td input,
    tr.pitems td div.pricediv {
        text-align: right !important;
    }
</style>
<div class="row">
    <div class="col-sm-12 table-responsive">
        <table class="table border-separate">
            <thead class="thead-inverse">
                <tr>
                    <th>Specifications/Remark</th>
                    <th>HSN Code</th>
                    <th>Batch</th>
                    <th>Actual Quantity</th>
                    <th>Billed Quantity</th>
                    <th>Remaining Quantity</th>
                    <?php if ($purchase_type  == 'A') { ?>
                        <th>Length</th>
                    <?php } ?>
                    <?php if ($purchase_type  == 'G') { ?>
                        <th>Width</th>
                        <th>Height</th>
                    <?php } ?>
                    <th>Quantity</th>
                    <th>Rate</th>
                    <th>Unit</th>
                    <th>Base Quantity</th>
                    <th>Base Unit</th>
                    <th>Base Rate</th>
                    <th>Amount</th>
                    <th>Tax Slab (%)</th>
                    <th>Discount Amount</th>
                    <th>Discount (%)</th>
                    <th>CGST (%)</th>
                    <th>CGST</th>
                    <th>SGST (%)</th>
                    <th>SGST</th>
                    <th>IGST (%)</th>
                    <th>IGST</th>
                    <th>Total Tax</th>
                    <th>Tax (%)</th>
                    <th>Total Amount</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php
                $number    = 1;
                $amount    = 0;
                $totalQty  = 0;
                $bTotalQty = 0;
                $billed_quantity = 0;
                $i = 1;



                foreach ($purchase as $item) {


                    if ($item['category_id']) {
                        $categorymodel = $this->GetItemCategoryById(
                            $parent = 0,
                            $spacing = '',
                            $user_tree_array = '',
                            $item['category_id']
                        );

                        if ($categorymodel) {
                            $categoryName = $categorymodel["data"];
                            $baseunit = $categorymodel["base_unit"];
                        } else {
                            $categoryName = "Not available";
                            $baseunit = $item['base_unit'];
                        }
                    } else {
                        $categoryName  = $item['remark'];
                        $baseunit = $item['base_unit'];
                    }

                    $purchasemodel = PurchaseItems::model()->findByPk($item['item_id']);
                    $itembaseqty = $purchasemodel->base_qty;
                    $itembaserate = $purchasemodel->base_rate;
                    $itembaseunit = $purchasemodel->base_unit;
                    $itemunit = $purchasemodel->unit;
                    if ($itembaseqty != '' && $itembaserate != '' && $itembaseunit != '') {
                        if ($itembaseunit != $itemunit) {
                            $itemQuantity = $itembaseqty;
                        } else {
                            $itemQuantity = $itembaseqty;
                        }
                    } else {
                        $itemQuantity = $purchasemodel->quantity;
                    }

                    if (!empty($bills)) {
                        $bill = isset($bills[$item['item_id']]) ? $bills[$item['item_id']] : 0;
                    } else {
                        $bill = 0;
                    }

                    if (!empty($bill_datas)) {
                        $bill_data = isset($bill_datas[$item['item_id']]) ? $bill_datas[$item['item_id']] : 0;
                    } else {
                        $bill_data = 0;
                    }

                    $billsum1 = $bill + $bill_data;
                    $billsum =
                        $bItemQuantity = $billsum1 ? $billsum1 : 0;

                    if (strpos($bItemQuantity, ".") !== false) {
                        $bItemQuantity = number_format((float) $bItemQuantity, 4, '.', '');
                    } else {
                        $bItemQuantity = $bItemQuantity;
                    }
                    $quantityAvail = $itemQuantity - $bItemQuantity;
                    if ($quantityAvail < 0) {
                        $quantityAvail = 0;
                    }
                    $totalQty = $totalQty + $itemQuantity;
                    $bTotalQty = $bTotalQty + $bItemQuantity;
                    $billed_quantity += $bItemQuantity;



                ?>
                    <tr class="pitems">

                        <?php echo CHtml::hiddenField('ids[]', $item["item_id"], array('id' => "ids" . ($number - 1), 'class' => "txtBox pastweek ids")); ?>

                        <?php echo CHtml::hiddenField('category[]', $item['category_id'] ? $item['category_id'] : '', array('id' => "category" . ($number - 1), 'class' => "txtBox pastweek category")); ?>

                        <td id="<?php echo $number - 1; ?>">
                            <div id="bicategoryname<?php echo $number - 1; ?>"><?php echo $categoryName; ?></div>
                        </td>

                        <td id="<?php echo $number - 1; ?>">
                            <div id="hsn_code<?php echo $number - 1; ?>"><?php echo $item["hsn_code"]; ?></div>

                            <?php echo CHtml::hiddenField('hsn_code[]', $item['hsn_code']); ?>
                        </td>

                        <td id="<?php echo $number - 1; ?>">

                            <?php echo CHtml::textField('bibatch[]', '', array('value' => '', 'class' => 'form-control bibatch', 'id' => "bibatch" . ($number - 1))); ?>
                        </td>

                        <?php echo CHtml::hiddenField('availablequantity[]', $quantityAvail, array('id' => "availablequantity" . ($number - 1))); ?>

                        <?php echo CHtml::hiddenField('orrate[]', number_format((float) $item["rate"], 2, '.', ''), array('id' => "orrate" . ($number - 1))); ?>

                        <td id="<?php echo $number - 1; ?>">
                            <div id="actualquantity<?php echo $number - 1; ?>"><?php echo $itemQuantity; ?></div>
                        </td>


                        <td id="<?php echo $number - 1; ?>">
                            <div id="billedquantity<?php echo $number - 1; ?>"><?php echo $bItemQuantity; ?></div>
                        </td>
                        <td id="<?php echo $number - 1; ?>">
                            <div id="biremquantity<?php echo $number - 1; ?>"><?php echo $itemQuantity > $bItemQuantity ? $quantityAvail : '0'; ?></div>

                            <?php echo CHtml::hiddenField('biremquantity[]', $quantityAvail, array('id' => "biremquantity" . ($number - 1))); ?>
                        </td>
                        <?php if ($purchase_type  == 'A') { ?>
                            <td id="<?php echo $number - 1; ?>">
                                <div id="bilength<?php echo $number - 1; ?>"><?php echo $item["item_length"]; ?></div>
                            </td>
                        <?php } ?>
                        <?php if ($purchase_type  == 'G') { ?>
                            <td id="<?php echo $number - 1; ?>">
                                <div id="biwidth<?php echo $number - 1; ?>"><?php echo $item["item_width"]; ?></div>
                            </td>

                            <td id="<?php echo $number - 1; ?>">
                                <div id="biheight<?php echo $number - 1; ?>"><?php echo $item["item_height"]; ?></div>
                            </td>
                        <?php } ?>


                        <td id="<?php echo $number - 1; ?>">
                            <?php echo CHtml::textField('biquantity[]', '0', array(
                                'value' => '0', 'class' => 'form-control fnext biquantity validate[required,custom[number],min[0]]', 'id' => "biquantity" . ($number - 1),
                                'readonly' => ($quantityAvail <= 0) ? true : false
                            )); ?>
                        </td>

                        <td id="<?php echo $number - 1; ?>">

                            <?php echo CHtml::textField('birate[]', number_format((float) $item["rate"], 2, '.', ''), array(
                                'value' => number_format((float) $item["rate"], 2, '.', ''), 'class' => 'form-control birate validate[required,custom[number],min[0]]', 'id' => "birate" . ($number - 1),
                                'readonly' => ($quantityAvail == 0) ? true : false
                            )); ?>
                        </td>

                        <td id="<?php echo $number - 1; ?>">
                            <div id="vendorunit<?php echo $number - 1; ?>" style="min-width:80px;">
                                <?php
                                $val = $number - 1;
                                $purchase_unitvendor = $item['unit'];
                                $options = CHtml::listData(UnitConversion::model()->findAll(array(
                                    'select' => array('conversion_unit'),
                                    'condition' => 'item_id = ' . $item["category_id"] . '',
                                    'order' => 'conversion_unit'
                                )), 'conversion_unit', 'conversion_unit');

                                if (empty($options)) {
                                    $options[$purchase_unitvendor] = $purchase_unitvendor;
                                }

                                echo CHtml::dropDownList('purchase_unitvendor ', $purchase_unitvendor, $options, array('empty' => 'Select Unit', 'class' => 'inputs target txtBox   form-control purchase_unitvendor', 'id' => 'purchase_unitvendor' . $val));
                                ?>

                            </div>
                        </td>


                        <td id="<?php echo $number - 1; ?>">
                            <?php echo CHtml::textField('basequantity[]', '0', array(
                                'value' => '0', 'class' => 'form-control  fnext basequantity validate[required,custom[number],min[0]]', 'id' => "basequantity" . ($number - 1),
                                'readonly' =>  true
                            )); ?>
                        </td>


                        <td id="<?php echo $number - 1; ?>">
                            <div id="biunit<?php echo $number - 1; ?>">
                                <?php echo $baseunit; ?>
                            </div>
                        </td>
                        <td id="<?php echo $number - 1; ?>" style="min-width:80px;">
                            <div id="baserate<?php echo $number - 1; ?>">0</div>
                        </td>
                        <td id="<?php echo $number - 1; ?>">
                            <div id="biamount<?php echo $number - 1; ?>" class="pricediv">0</div>
                        </td>
                        <td id="<?php echo $number - 1; ?>">
                            <?php

                            $taxSlabsData = TaxSlabs::model()->findAll(array('select' => 'tax_slab_value as id, concat_ws("",tax_slab_value,"%") as tax_slab_value ', 'condition' => 'status=1'));
                            $taxSlabData = CHtml::listData($taxSlabsData, 'id', 'tax_slab_value');
                            echo CHtml::dropDownList('tax_slab[]', $item['tax_slab'], $taxSlabData, array('id' => 'tax_slab' . ($number - 1), 'class' => 'txtBox tax_slab', 'empty' => 'select one'))
                            ?>


                        </td>
                        <td id="<?php echo $number - 1; ?>">

                            <?php echo CHtml::textField('damount[]', number_format((float) $item["discount_amount"], 2, '.', ''), array(
                                'value' => number_format((float) $item["discount_amount"], 2, '.', ''), 'class' => 'form-control validate[required,custom[number],min[0]]', 'id' => "damount" . ($number - 1),
                                'readonly' => ($quantityAvail == 0) ? true : false
                            )); ?>
                        </td>

                        <td id="<?php echo $number - 1; ?>">
                            <div id="dpercent<?php echo $number - 1; ?>" class="pricediv"><?php echo $item['discount_percentage']; ?></div>
                        </td>
                        <td id="<?php echo $number - 1; ?>">

                            <?php echo CHtml::textField('cgstpercent[]', $item['cgst_percentage'], array(
                                'value' => $item['cgst_percentage'], 'class' => 'form-control validate[required,custom[number],min[0]]  cgstp', 'id' => "cgstpercent" . ($number - 1),
                                'readonly' => ($quantityAvail == 0) ? true : false
                            )); ?>
                        </td>
                        <td id="<?php echo $number - 1; ?>">
                            <div id="cgst<?php echo $number - 1; ?>" class="pricediv"><?php echo number_format((float) $item["cgst_amount"], 2, '.', ''); ?></div>
                        </td>

                        <td id="<?php echo $number - 1; ?>">

                            <?php echo CHtml::textField('sgstpercent[]', $item['sgst_percentage'], array(
                                'value' => $item['sgst_percentage'], 'class' => 'form-control validate[required,custom[number],min[0]] sgstp', 'id' => "sgstpercent" . ($number - 1),
                                'readonly' => ($quantityAvail == 0) ? true : false
                            )); ?>
                        </td>
                        <td id="<?php echo $number - 1; ?>">
                            <div id="sgst<?php echo $number - 1; ?>" class="pricediv"><?php echo number_format((float) $item["sgst_amount"], 2, '.', ''); ?></div>
                        </td>

                        <td id="<?php echo $number - 1; ?>">

                            <?php echo CHtml::textField('igstpercent[]', $item['igst_percentage'], array(
                                'value' => $item['igst_percentage'], 'class' => 'form-control lnext validate[required,custom[number],min[0]]', 'id' => "igstpercent" . ($number - 1),
                                'readonly' => ($quantityAvail == 0) ? true : false
                            )); ?>
                        </td>
                        <td id="<?php echo $number - 1; ?>">
                            <div id="igst<?php echo $number - 1; ?>" class="pricediv"><?php echo number_format((float) $item["igst_amount"], 2, '.', ''); ?></div>
                        </td>

                        <td id="<?php echo $number - 1; ?>">
                            <div id="taxamount<?php echo $number - 1; ?>" class="pricediv"><?php echo number_format((float) $item["tax_amount"], 2, '.', ''); ?></div>
                        </td>
                        <td id="<?php echo $number - 1; ?>">
                            <div id="taxpercent<?php echo $number - 1; ?>" class="pricediv"><?php echo $item['tax_perc']; ?></div>
                        </td>
                        <td id="<?php echo $number - 1; ?>">
                            <div id="totalamount<?php echo $number - 1; ?>" class="pricediv">0</div>
                        </td>

                        <?php echo CHtml::hiddenField('billitem[]', '', array('id' => "billitem" . ($number - 1))); ?>

                        <?php echo CHtml::hiddenField('savedquantity[]', '', array('id' => "savedquantity" . ($number - 1))); ?>
                        <td>
                            <div id="tickmark<?php echo $number - 1; ?>"></div>
                            <?php echo CHtml::hiddenField('save_status[]', '', array('id' => "save_status" . ($number - 1))); ?>
                        </td>
                    </tr>
                <?php
                    $amount = $amount + $item["amount"];
                    $number = $number + 1;
                }
                ?>
                <?php echo CHtml::hiddenField('tqty[]', $totalQty, array('id' => "tqty")); ?>
                <?php echo CHtml::hiddenField('btqty[]', $bTotalQty, array('id' => "btqty")); ?>
                <?php echo CHtml::hiddenField('totrows', $number - 1, array('id' => "totrows", 'class' => "txtBox pastweek totrows")); ?>
                <?php echo CHtml::hiddenField('purchase_type', $purchase_type, array('id' => 'purchase_type')); ?>
            </tbody>
        </table>
    </div>
</div>