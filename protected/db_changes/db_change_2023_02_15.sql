CREATE TABLE `jp_labours` (`id` INT(11) NOT NULL AUTO_INCREMENT , `labour_label` VARCHAR(300) NOT NULL , `labour_rate` FLOAT(11,2) NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;

CREATE TABLE `jp_labour_template` (`id` INT NOT NULL AUTO_INCREMENT , `template_label` VARCHAR(300) NOT NULL , `labour_type` VARCHAR(300) NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;


CREATE TABLE `jp_project_labour` (`id` INT(11) NOT NULL AUTO_INCREMENT , `project_id` INT(11) NOT NULL , `template_id` INT(11) NOT NULL , `labour_id` INT(11) NOT NULL , `labour_rate` FLOAT(11,2) NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;

ALTER TABLE `jp_project_labour` ADD CONSTRAINT `fk_labour_template_id` FOREIGN KEY (`template_id`) REFERENCES `jp_labour_template`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE; ALTER TABLE `jp_project_labour` ADD CONSTRAINT `fk_labour_label_id` FOREIGN KEY (`labour_id`) REFERENCES `jp_labours`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `jp_project_labour` ADD CONSTRAINT `fk_labour_project_id` FOREIGN KEY (`project_id`) REFERENCES `jp_projects`(`pid`) ON DELETE RESTRICT ON UPDATE CASCADE;


INSERT INTO `jp_labours` (`id`, `labour_label`, `labour_rate`) VALUES (NULL, 'Mason', '1500'), (NULL, 'Electrician', '1250'),(NULL, 'Carpenter', '1000'),(NULL, 'Helper', '850');

INSERT INTO `jp_labour_template` (`id`, `template_label`, `labour_type`) VALUES (NULL, 'Civil', '1,2,4'), (NULL, 'Electrical', '2,4'), (NULL, 'Civil+Electrical', '1,2,3,4');
