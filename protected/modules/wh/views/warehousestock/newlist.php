<div class="" id="newlist">
<?php
    $this->widget('zii.widgets.CListView', array(
        'dataProvider' => $dataProvider,
        'itemView' => '_view', 
        'template' => '<div class="clearfix"><div class="pull-left">{sorter}</div></div><div class=""><table cellpadding="10" id="myTable" class="table">{items}</table></div>{pager}',

        //'template' => '<div>{sorter}</div><div class=""><table cellpadding="10" class="table" id="myTable">{items}</table></div>'
        //'ajaxUpdate' => false,
    ));

?> 
</div>