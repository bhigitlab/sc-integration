<script src="<?php echo Yii::app()->baseUrl . '/ckeditor/ckeditor.js'; ?>"></script>
<style>
    .cke_wysiwyg_frame{
        height:300px !important;
    }
    
</style>
<div class="container">
    <!-- <h4>Terms and Conditions</h4> -->
    <div class="row">

        <div class="form-group col-xs-12 col-md-12"> 
            <label>Terms and Conditions<span class="errorMessage">*</span></label>
            <input type="hidden" value="<?php echo $terms['id'] ?>" class="term_id">
            <textarea id="editor1" name="editor1"><?php echo $terms['terms_and_conditions'] ?></textarea>
        </div>        
       
    </div> 
    <div class="row">
        <div class="form-group col-xs-12 text-right">
        <button type="button" class="btn btn-primary save_terms">Save</button>
        </div>
  </div>                                  
</div>
<script type="text/javascript">    
    CKEDITOR.replace( 'editor1' );
   
    $(".save_terms").click(function(){
        var term_id =$(".term_id").val();           
        var terms = $("#cke_editor1").find("iframe").contents().find("body").html();
        $.ajax({
            url: '<?php echo  Yii::app()->createAbsoluteUrl('subcontractor/saveterms'); ?>',
            type: 'POST',            
            data: {
                term_id: term_id,
                terms:terms
            },
            success: function(response) {
                alert(response);
                window.location.href = "<?php echo Yii::app()->createAbsoluteUrl('subcontractor/quotations'); ?>";
                
            }
        })        
   })
   

</script>
