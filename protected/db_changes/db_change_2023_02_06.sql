
ALTER TABLE `jp_purchase` ADD `declined_by` INT(11) NULL DEFAULT NULL AFTER `decline_comment`, ADD `declined_date` DATETIME NULL DEFAULT NULL AFTER `declined_by`;

ALTER TABLE `jp_purchase` ADD CONSTRAINT `fk_declined_user` FOREIGN KEY (`declined_by`) REFERENCES `jp_users`(`userid`) ON DELETE RESTRICT ON UPDATE CASCADE;