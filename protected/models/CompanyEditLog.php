<?php

/**
 * This is the model class for table "{{company_edit_log}}".
 *
 * The followings are the available columns in table '{{company_edit_log}}':
 * @property integer $id
 * @property integer $p_id
 * @property integer $project_id
 * @property integer $prev_company_id
 * @property integer $current_company_id
 * @property string $log_date
 * @property integer $changed_by
 *
 * The followings are the available model relations:
 * @property Purchase $p
 * @property Projects $project
 * @property Company $prevCompany
 * @property Company $currentCompany
 * @property PmsUsers $changedBy
 */
class CompanyEditLog extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{company_edit_log}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('p_id, project_id, prev_company_id, current_company_id, log_date, changed_by', 'required'),
			array('p_id, project_id, prev_company_id, current_company_id, changed_by', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, p_id, project_id, prev_company_id, current_company_id, log_date, changed_by', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'p' => array(self::BELONGS_TO, 'Purchase', 'p_id'),
			'project' => array(self::BELONGS_TO, 'Projects', 'project_id'),
			'prevCompany' => array(self::BELONGS_TO, 'Company', 'prev_company_id'),
			'currentCompany' => array(self::BELONGS_TO, 'Company', 'current_company_id'),
			'changedBy' => array(self::BELONGS_TO, 'Users', 'changed_by'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'p_id' => 'P',
			'project_id' => 'Project',
			'prev_company_id' => 'Prev Company',
			'current_company_id' => 'Current Company',
			'log_date' => 'Date',
			'changed_by' => 'Changed By',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('p_id',$this->p_id);
		$criteria->compare('project_id',$this->project_id);
		$criteria->compare('prev_company_id',$this->prev_company_id);
		$criteria->compare('current_company_id',$this->current_company_id);
		$criteria->compare('log_date',$this->log_date,true);
		$criteria->compare('changed_by',$this->changed_by);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CompanyEditLog the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	public function changedbyuser($id){
		$user = Users::model()->findByPk($id);
		if(!empty($user)){
			return $user->first_name;
		}else{
			return '';
		}
	}
}
