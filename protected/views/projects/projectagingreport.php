<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/tableHeadFixer.js"></script>
<?php
    $aging_array = Controller::getProjectAgingSummary();
?>
<div class="container">
    <div class="sub-heading">
        <h3>Aging Summary Reports</h3>
        <a href="<?php echo Yii::app()->createUrl("projects/expenselist")?>" class="btn btn-info">
        Project Report</a>
    </div>
        <?php if(count($aging_array) > 0){ ?>
            <div class="clearfix">
                <div class="pull-right">Total <?php echo count($aging_array) ?> records</div>
            </div>
        <?php } ?>
        <div id="parent">
            
            <table class="table" id="fixTable">
                <thead>
                    <tr>
                        <th>Invoice Number</th>
                        <th>Date</th>
                        <th>Project</th>                                
                        <th>No of days Delayed</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                    $i=1;
                    foreach ($aging_array as $value) { ?>
                        <?php
                            $now = time(); 
                            $your_date = strtotime($value['date']);
                            $datediff = ceil(($now - $your_date)/86400);
                                            
                            ?>
                            <tr>
                                <td><?php echo $value['inv_no']; ?></td>
                                <td><?php echo date("d-m-Y", strtotime($value['date'])); ?></td>
                                <td><?php echo $value['name']; ?></td>                                            
                                <td class="diff"><?php echo $datediff ; ?></td>                        
                            </tr>
                        <?php  
                        $i++; 
                    }?>
                    
                </tbody>
            </table>
        </div>    
</div>

<script>
    $(function(){
        var sorted = $('table tbody tr').sort(function(a, b) {
            var a = $(a).find('td.diff').text(), b = $(b).find('td.diff').text();
            return b.localeCompare(a, false, {numeric: true})
        });
                
        $('table tbody').append(sorted);                
    })
    
    $("#fixTable").tableHeadFixer({
        'foot': true,
        'head': true
    });
</script>

<style>
    #parent {
        max-height: 400px;
        overflow:auto;
    }
</style>
