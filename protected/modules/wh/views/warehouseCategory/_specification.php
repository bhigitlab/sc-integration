<div id="msg_box"></div>
<?php

?>

<?php
if ($index == 0) {
    ?>
    <thead>
        <tr>
            <th>No</th>
            <th>Image</th>
            <th>Category</th>
            <th>Brand</th>
            <th>Type</th>
            <th>Specification</th>
            <th>Created Date</th>
            <th></th>
            

        </tr>   
    </thead>
<?php } ?>
<?php
$brand = Brand::model()->findByPk($data->brand_id);
//print_r($data);
?>
    <tr>
        <td class="text-right"><?php echo $data->id; ?></td>
                        <td class="imd_div2" id="<?php echo $data->id; ?>">
                    <?php
                        if($data->filename != ''){
                            $path = realpath(Yii::app()->basePath . '/../uploads/warehouse_category/'.$data->filename);
                            if(file_exists($path)){
                    ?>
                    <img class="pop" src="<?php  echo Yii::app()->request->baseUrl."/uploads/warehouse_category/thumbnail/".$data->filename ?>" alt='' style=''/>
                    <?php } else{ ?>
                    <img src="<?php echo Yii::app()->request->baseUrl."/uploads/warehouse_category/thumbnail/no_img.png"?>" alt='' style='cursor:pointer;height:60px;width:60px;' />        
                    <?php } ?>
                        <?php } else{ ?>
                    <img class="pop" src="<?php echo Yii::app()->request->baseUrl."/uploads/warehouse_category/thumbnail/no_img.png"?>" alt='' style='cursor:pointer;height:60px;width:60px;' />        
                    <?php } ?>
                </td>
		<td><?php echo $data->category_name; ?></td>
		<td><?php echo ($data->brand_id == '')?'':$brand['brand_name']; ?></td>
                <td>
                <?php
                if($data["specification_type"] == "A") {
                    echo "By Length";
                } else if($data["specification_type"] == "G") {
                    echo "By Width * Height";
                } else {
                    echo "By Quantity";
                }
                ?>
                </td>
		<td><?php echo $data->specification ; ?></td>
		<td><?php echo date('d-m-Y', strtotime($data->created_date));  ?></td>
		<td>
		<div class="edit_purchase edit_option_<?php echo $data->id; ?>" id="<?php echo $data->id; ?>"  data-toggle="tooltip" title="Edit"><span class="fa fa-edit editSpecification" data-toggle="modal" data-target=".edit" data-id="<?php echo $data->id; ?>"></span></div>
		</td>
</tr>  




