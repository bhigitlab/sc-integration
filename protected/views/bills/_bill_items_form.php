<?php
$general_settings_for_auto_receipt = Controller::generalSettingsForAutoReceiptToProjectWarehouse($settings = 1);

?>
<div class="bill-items">
<div class="clearfix"></div>
			<div id="msg"></div>
			<div id="previous_details"></div>
			<div class="bill_items  panel panel-default">
			<input type="hidden" name="remove" id="remove" value="">
			<input type="hidden" name="bill_id" id="bill_id" value="<?php echo isset($billModel)? $billModel->bill_id:'0'?>">
			<?php if(isset($billModel) && is_object($billModel)) {?>
    <input type="hidden" name="bill_date" id="bill_date" class="date" value="<?php echo isset($billModel) ? date('d-m-Y', strtotime($billModel->bill_date)) : '' ?>">
	<input type="hidden" name="date" id="date" class="date" value="<?php echo isset($billModel) ? date('d-m-Y', strtotime($billModel->bill_date)) : '' ?>">
    
    <input type="hidden" name="warehouse_id" id="warehouse_id" value="<?php echo isset($billModel) ? $billModel->warehouse_id : '0' ?>">
<?php } ?>

<?php if(isset($purchase) && is_object($purchase)) {?>
    <input type="hidden" name="project" id="project" value="<?php echo isset($purchase) ? $purchase->project_id : '0' ?>">
    <input type="hidden" name="vendor" id="vendor" value="<?php echo isset($purchase) ? $purchase->vendor_id : '0' ?>">
    <input type="hidden" name="expense_head" id="expense_head" value="<?php echo isset($purchase) ? $purchase->expensehead_id : '0' ?>">
<?php } ?>

				<div class="panel-heading">
					<h3>Bill Items</h3>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-md-9">
							<div class="row">
								<div class="purchaseitem col-sm-4 col-lg-3">
									<div id="select_div" class="form-group">
										<label>Specification:</label>
										<select class="txtBox specification description " style='width:200px;' id="description" name="description[]">
											<option value="">Select one</option>
											<?php
											foreach ($specification as $key => $value) {
											?>
												<option value="<?php echo $value['id']; ?>"><?php echo $value['data']; ?></option>
											<?php } ?>
											<option value="other">Other</option>
										</select>
									</div>
								</div>
								<div class="purchaseitem remark col-sm-4 col-lg-3">
									<div class="form-group">
										<label>Remarks:</label>
										<input type="text" class="txtBox " id="remarks" name="remark[]" placeholder="Remark" />
									</div>
								</div>
								<input type="hidden" name="purchaseitem_unit_hidden" id="purchaseitem_unit_hidden" />


								<div class="purchaseitem quantity_div col-sm-4 col-lg-2">
									<div class="form-group">
										<label>Quantity:</label>
										<input type="text" class="inputs target txtBox quantity biquantity allownumericdecimal form-control" id="purchase_quantity" name="purchase_quantity" placeholder="" /></td>
									</div>
								</div>

								<div class="purchaseitem col-sm-4 col-lg-2">
									<div class="form-group">
										<label>Rate:</label>
										<input type="text" class="inputs target txtBox rate  birate allownumericdecimal form-control" id="purchase_rate" name="purchase_rate" placeholder="" />
									</div>
								</div>

								<div class="purchaseitem col-sm-4 col-lg-2">
									<div class="form-group">
										<label>Batch:</label>
										<input type="text" class="inputs target txtBox bibatch allownumericdecimal form-control" id="bibatch" name="bibatch" placeholder="" /></td>
									</div>
								</div>
								<div class="purchaseitem col-sm-2 col-lg-1">
									<div class="form-group">
										<label>HSN Code:</label>
										<div id="hsn_code" class="hsn_code padding-box">&nbsp;&nbsp;</div>
									</div>
								</div>
								<div class="purchaseitem  col-sm-2 form-group">
									<label>Units:</label>
									<?php
									$purchase_unit = '';
									$options = CHtml::listData(Unit::model()->findAll(), 'unit_name', 'unit_name');
									echo CHtml::dropDownList('purchaseitem_unit', $purchase_unit, $options, array('empty' => 'Select Unit', 'class' => 'inputs target txtBox  allownumericdecimal form-control'));
									?>
								</div>
								<div class="purchaseitem quantity_div base  col-sm-4 col-lg-2  form-group">
									<label>Base Quantity:</label>
									<input type="text" class="inputs target txtBox quantity allownumericdecimal form-control" id="quantity" name="quantity" placeholder="" /></td>
								</div>
								<div class="purchaseitem base  col-sm-4 col-lg-2  form-group">
									<label>Base Units:</label>
									<input type="text" class="inputs target txtBox quantity allownumericdecimal form-control" id="item_unit" name="purchasebaseitem_unit" placeholder="" readonly="true" />
								</div>
								<div class="purchaseitem base  col-sm-4 col-lg-2  form-group">
									<label>Base Rate:</label>
									<input type="text" class="inputs target txtBox rate allownumericdecimal form-control" id="rate" name="rate" placeholder="" readonly=true />
								</div>
								<div class="purchaseitem gsts  col-sm-4 col-lg-2 mobile-margin-bottom-0 tab-margin-bottom-0  form-group">
										<label>Dis amount :</label>
										<input type="text" class="inputs target txtBox sgst allownumericdecimal form-control calculation" id="dis_amount" name="dis_amount" placeholder="" />
										<div id="disp" class="padding-box">0.00</div>(%)
								</div>
									<div class="clearfix d-tab-block"></div>
								<div class="purchaseitem col-sm-2 col-lg-1 tab-margin-bottom-20 padding-left-5-15  form-group mobile-margin-bottom-0">
										<div id="select_div" class="select-div-full">
											<label>Tax Slab:</label>
											<?php
											$datas = TaxSlabs::model()->getAllDatas();
											?>
											<select class="txtBox tax_slab" id="tax_slab"  name="tax_slab[]">
												<option value="">Select one</option>
												<?php
												foreach ($datas as  $value) {
													if ($value['set_default'] == 1) {
														echo ' <option data-id="' . $value['id'] . '" value="' . $value['tax_slab_value']  . '" selected>' . $value['tax_slab_value'] . '%</option>';
													} else {
														echo ' <option  data-id="' . $value['id'] . '" value="' . $value['tax_slab_value'] . '">' . $value['tax_slab_value'] . '%</option>';
													}
												}
												?>
											</select>
											<span class="d-mobile-block">&nbsp;</span>
									</div>
								</div>
								<div class="purchaseitem gsts col-sm-2 mobile-margin-bottom-0">
									<div class="form-group mobile-margin-bottom-0">
										<label>SGST :</label>
										<input type="text" class="inputs target small_class txtBox sgstp allownumericdecimal form-control calculation" id="sgstp" name="sgstp" placeholder="" />
										<div id="sgst_amount" class="padding-box">0.00</div>
									</div>
								</div>

								<div class="purchaseitem gsts col-sm-2 mobile-margin-bottom-0">
									<div class="form-group mobile-margin-bottom-0">
										<label>CGST :</label>
										<input type="text" class="inputs target txtBox cgstp small_class allownumericdecimal form-control calculation" id="cgstp" name="cgstp" placeholder="" />
										<div id="cgst_amount" class="padding-box">0.00</div>
									</div>
								</div>

								<div class="purchaseitem gsts col-sm-2 mobile-margin-bottom-0">
									<div class="form-group mobile-margin-bottom-0">
										<label>IGST :</label>
										<input type="text" class="inputs target txtBox igstp  small_class allownumericdecimal form-control calculation" id="igstp" name="igstp" placeholder="" />
										<div id="igst_amount" class="padding-box">0.00</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-3 text-right">
							<div class="mb-1">
								<b>Amount: </b>
								<div id="item_amount" class="padding-box">0.00</div>
							</div>
							<div class="mb-1">
								<b>Discount Amount: </b>
								<div id="disc_amount" class="padding-box">0.00</div>
							</div>
							<div class="mb-1">
								<b>Tax Amount: </b>
								<div id="tax_amount" class="padding-box">0.00</div>
							</div>
							<div class="mb-1">
								<b>Total Amount: </b>
								<div id="total_amount" class="padding-box">0.00</div>
							</div>
						</div>
					</div>
					<div>
						<span id="item_baseunit_data" class="d-none text-blue"><i>Base Unit : </i><span class="txtBox item_unit_span" id="item_unit_additional_data"> </span></span>&nbsp;&nbsp;


						<span id="item_conversion_data" class="d-none text-blue"><i>Conv Fact : </i><span class="txtBox item_unit_span" id="item_unit_additional_conv"> </span></span>
					</div>
					<div class="text-right">
						<input type="button" class="item_save btn btn-info" id="0" value="Save">
					</div>
				</div>
			</div>
</div>
<script>
	function taxSlab(){
		$(".tax_slab[value='18']").each(function() {
		var gst = parseFloat($(this).val()) / 2;
		var tax_slab = $('#tax_slab').val();
		alert('hi');
		if(tax_slab==18){
			$("#sgstp").val(gst);
			$("#cgstp").val(gst);
			$(".sgstp").trigger("change");
			$(".cgstp").trigger("change");
		}
				//$(this).parents(".row").find(".sgstp").val(gst).trigger("change");
				//$(this).parents(".row").find(".cgstp").val(gst).trigger("change");
		});
	}
    $('#grand_total').change(function() {
		var amount = this.value;
		$("#grand_total").val(parseFloat(amount).toFixed(2));
	});
	$(document).on("change", "#bill_round_off", function(event) {

		var roundOff = parseFloat(this.value);
		var bill_id = $('#bill_id').val();
		if (isNaN(roundOff)) roundOff = 0;
		var additional_amount = parseFloat($('#totaladditional_charge').text());
		if (isNaN(additional_amount)) additional_amount = 0;
		if (bill_id !== '') {
			$('.loading-overlay').addClass('is-active');
			$.ajax({
				url: "<?php echo Yii::app()->createAbsoluteUrl("bills/updatebillamount"); ?>",
				data: {
					'Bills[round_off]': roundOff,
					bill_id: bill_id
				},
				type: "POST",
				dataType: 'json',
				success: function(data) {
					if (data.status === 1) {
						var bill_totalamount = data.values['bill_totalamount'] + additional_amount;
						$('#grand_total').val(parseFloat(bill_totalamount).toFixed(2));
						$().toastmessage('showSuccessToast', "Total amount updated successfully");
					} else {
						$().toastmessage('showErrorToast', "Error occured");
					}

				}
			});
		}

	});
    $('#dis_amount').keyup(function(event) {
		var amount = parseFloat($('#item_amount').html());
		var val = $(this).val();
		if (amount < val) {
			$().toastmessage('showErrorToast', "This amount must be less than the amount");
			$(this).val('');
		}
	})
    $('.other').click(function() {
		if (this.checked) {
			$('#autoUpdate').fadeIn('slow');
			$('#select_div').hide();
		} else {
			$('#autoUpdate').fadeOut('slow');
			$('#select_div').show();
		}

	})


	$(document).on("change", ".other", function() {
		if (this.checked) {
			$(this).closest('tr').find('#autoUpdate').fadeIn('slow');
			$(this).closest('tr').find('#select_div').hide();
		} else {
			$(this).closest('tr').find('#autoUpdate').fadeOut('slow');
			$(this).closest('tr').find('#select_div').show();
		}
	});


	$(".specification").select2({
		ajax: {
			url: '<?php echo  Yii::app()->createAbsoluteUrl('bills/testransaction'); ?>',
			type: "POST",
			dataType: 'json',
			delay: 0,
			data: function(params) {
				return {
					searchTerm: params.term // search term
				};
			},
			processResults: function(response) {
				$('#previous_details').html(response.html);
				$("#previous_details").show();
				$("#pre_fixtable2").tableHeadFixer();
				$('#item_unit').val(response.unit);
				$('#hsn_code').text(response.hsn_code);
				return {
					results: response.data
				};
			},
			cache: true
		}
	});


	$('.specification').change(function() {
		$('#loading').show();
		var bill_id = $("#bill_id").val();
		$('#purchaseitem_unit').val('');
		$("#purchaseitem_unit_hidden").val('');
		var element = $(this);
		var category_id = $(".specification option:selected").val();
		var val = $(".specification option:selected").val();
		
		$.ajax({
			url: '<?php echo Yii::app()->createUrl('bills/getRelatedUnit'); ?>',
			method: 'POST',
			data: {
				cat_id: val
			},
			dataType: "json",
			success: function(response) {
				$("#purchaseitem_unit").html(response.html);
			}
		})

		var element = $(this);
		var category_id = $(this).val();
		$.ajax({
			url: '<?php echo  Yii::app()->createAbsoluteUrl('bills/previoustransaction'); ?>',
			type: 'GET',
			dataType: 'json',
			data: {
				data: category_id,
				bill_id: bill_id
			},
			success: function(result) {
				if (result.status == 1) {
					$('#previous_details').html(result.html);
					$("#previous_details").show();
					$("#pre_fixtable").tableHeadFixer();
				} else {
					$('#previous_details').html(result.html);
					$("#pre_fixtable").tableHeadFixer();
				}
				if (result.unit != '') {
					$("#item_baseunit_data").show();
					$("#item_unit_additional_data").html(result.unit);
				}

				$('#hsn_code').text(result.hsn_code);

				var save = $('.item_save').attr("id");
				if (save == 0) {
					$("#purchaseitem_unit").val(result.unit);
					$('#item_unit').val(result.unit);
				} else {
					var purch_unit_hidden = $("#purchaseitem_unit_hidden").val();
					if (purch_unit_hidden != '') {
						$("#purchaseitem_unit").val(purch_unit_hidden);
						$('#item_unit').val(result.purchaseitem_unit);
					} else {
						$("#purchaseitem_unit").val(result.unit);
						$('#item_unit').val(result.unit);
					}
				}
				if (category_id == 'other') {
					$('.remark').css("display", "inline-block");
					$('#remarks').focus();
				} else if (category_id == '') {
					$('.js-example-basic-single').select2('focus');
					$('.remark').css("display", "none");
				} else {
					$('#quantity').focus();
					$('.remark').css("display", "none");
				}
				taxSlab();
				// debugger;

			}

		})

	})


	/* bills add  */

	
	// add bill itme
	var sl_no = 1;
	var howMany = 0;
	$('.item_save').click(function() {
		var element = $(this);
		var item_id = $(this).attr('id');
		

		if (item_id == 0) {
			var purchaseitem_unit = $('#purchaseitem_unit').find("option:selected").text();
			var description = $('#description').val();
			var quantity = $('#quantity').val();
			var unit = $('#item_unit').val();
			var hsn_code = $('#hsn_code').text();
			var remark = $('#remarks').val();
			var rate = $('#rate').val();
			var bibatch = $('#bibatch').val();
			var dis_amount = $('#dis_amount').val();
			var disp = $('#disp').html();
			var cgstp = $('#cgstp').val();
			var cgst_amount = $('#cgst_amount').html();
			var sgstp = $('#sgstp').val();
			var sgst_amount = $('#sgst_amount').html();
			var igstp = $('#igstp').val();
			var igst_amount = $('#igst_amount').html();
			var tax_amount = $('#tax_amount').html();
			var item_amount = $('#item_amount').html()
			var discount_amount = $('#disc_amount').html();
			var total_amount = $('#total_amount').html();
			var bill_number = $('#bill_number').val();
			var bill_date = $('#bill_date').val();
			var date = $('input[name="date"]').val();
			var bill_id = $('#bill_id').val();
			var project = $('#project').val();
			var vendor = $('#vendor').val();
			var expense_head = $('#expense_head').val();
			var rowCount = $('.table .addrow tr').length;
			var company = $("#company_id").val();
			var tax_slab = $("#tax_slab").val();
			var warehouse_id = $('#Warehousestock_warehousestock_warehouseid').val();
			var purchase_quantity = $('#purchase_quantity').val();
			var purchase_rate = $('#purchase_rate').val();
			
			if (bill_number == '' || bill_date == '' || project == '' || vendor == '' || expense_head == '' || company == '') {
				$().toastmessage('showErrorToast', "Please enter bills details");
			} else {
				if ((moment(date, 'DD-MM-YYYY', true).isValid())) {

					if (description != '' && purchase_rate != '' && purchase_rate > 0 && purchase_quantity != '' && tax_slab != '') {
						howMany += 1;
						if (howMany == 1) {
							var data = {
								'item_id': item_id,
								'quantity': quantity,
								'description': description,
								'unit': unit,
								'rate': rate,
								'bibatch': bibatch,
								'item_amount': item_amount,
								'dis_amount': dis_amount,
								'disp': disp,
								'sgstp': sgstp,
								'sgst_amount': sgst_amount,
								'cgstp': cgstp,
								'cgst_amount': cgst_amount,
								'igstp': igstp,
								'igst_amount': igst_amount,
								'tax_amount': tax_amount,
								'bill_id': bill_id,
								'discount_amount': discount_amount,
								'total_amount': total_amount,
								'remark': remark,
								'tax_slab': tax_slab,
								'hsn_code': hsn_code,
								'purchase_quantity': purchase_quantity,
								'purchaseitem_unit': purchaseitem_unit,
								'purchase_rate': purchase_rate
							};
							$('.loading-overlay').addClass('is-active');
							$.ajax({
								url: '<?php echo  Yii::app()->createAbsoluteUrl('bills/billsitem'); ?>',
								type: 'GET',
								dataType: 'json',
								data: {
									data: data
								},
								success: function(response) {
									if (response.response == 'success') {
										$('#discount_total').html(response.discount_total);
										$('#amount_total').html(response.amount_total);
										$('#tax_total').html(response.tax_total);
										$('#grand_total').val(response.grand_total);
										$().toastmessage('showSuccessToast', "" + response.msg + "");
										$('.addrow').html(response.html);
										$('#buttonsubmit').prop('disabled', false);
									} else {
										$().toastmessage('showErrorToast', "" + response.msg + "");
									}
									howMany = 0;
									$('#description').val('').trigger('change');
									$('#quantity').val('');
									$('#unit').val('');
									$('#bibatch').val('');
									$('#hsn_code').text('');
									$('#rate').val('');
									$('#dis_amount').val('');
									$('#disp').html('0.00');
									$('#item_amount').html('0.00');
									$('#tax_amount').html('0.00');
									$('#sgstp').val('');
									$('#sgst_amount').html('0.00');
									$('#cgstp').val('');
									$('#cgst_amount').html('0.00');
									$('#igstp').val('');
									$('#igst_amount').html('0.00');
									$('#disc_amount').html('0.00');
									$('#total_amount').html('0.00');
									$('#description').focus();
									$('.item_save').attr("id", "0");
									$('#tax_slab').val("18").trigger('change');
									$('#purchase_quantity').html('');
									$('#purchaseitem_unit').val('');
									$('#purchase_rate').html('');
									$('#purchaseitem_unit_hidden').val('');
									$('#item_baseunit_data').hide();
									$('#item_conversion_data').hide();
									$('.base').hide();

								}
							});

						}
					} else {
						$().toastmessage('showErrorToast', "Please fill the details");
					}

				} else {

					$(this).focus();
					$().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
					$(this).focus();
				}




			}



		} else {
			// update 


			var description = $('#description').val();
			var quantity = $('#quantity').val();
			var unit = $('#item_unit').val();
			var bibatch = $('#bibatch').val();
			var hsn_code = $('#hsn_code').text();
			var remark = $('#remarks').val();
			var rate = $('#rate').val();
			var dis_amount = $('#dis_amount').val();
			var disp = $('#disp').html();
			var cgstp = $('#cgstp').val();
			var cgst_amount = $('#cgst_amount').html();
			var sgstp = $('#sgstp').val();
			var sgst_amount = $('#sgst_amount').html();
			var igstp = $('#igstp').val();
			var igst_amount = $('#igst_amount').html();
			var tax_amount = $('#tax_amount').html();
			var item_amount = $('#item_amount').html()
			var discount_amount = $('#disc_amount').html();
			var total_amount = $('#total_amount').html();
			var bill_number = $('#bill_number').val();
			var bill_date = $('#bill_date').val();
			var date = $('input[name="date"]').val();
			var bill_id = $('#bill_id').val();
			var project = $('#project').val();
			var vendor = $('#vendor').val();
			var expense_head = $('#expense_head').val();
			var rowCount = $('.table .addrow tr').length;
			var company = $("#company_id").val();
			var tax_slab = $("#tax_slab").val();
			var purchase_quantity = $('#purchase_quantity').val();
			var purchaseitem_unit = $('#purchaseitem_unit').val();
			var purchase_rate = $('#purchase_rate').val();

			if (bill_number == '' || bill_date == '' || project == '' || vendor == '' || expense_head == '' || company == '') {
				$().toastmessage('showErrorToast', "Please enter bill details");
			} else {

				if ((moment(date, 'DD-MM-YYYY', true).isValid())) {
					if (description != '' && purchase_rate != '' && purchase_rate > 0 && purchase_quantity != '' && tax_slab != '') {
						howMany += 1;
						if (howMany == 1) {
							var data = {
								'item_id': item_id,
								'quantity': quantity,
								'description': description,
								'unit': unit,
								'rate': rate,
								'bibatch': bibatch,
								'item_amount': item_amount,
								'dis_amount': dis_amount,
								'disp': disp,
								'sgstp': sgstp,
								'sgst_amount': sgst_amount,
								'cgstp': cgstp,
								'cgst_amount': cgst_amount,
								'igstp': igstp,
								'igst_amount': igst_amount,
								'tax_amount': tax_amount,
								'bill_id': bill_id,
								'discount_amount': discount_amount,
								'total_amount': total_amount,
								'remark': remark,
								'tax_slab': tax_slab,
								'hsn_code': hsn_code,
								'purchase_quantity': purchase_quantity,
								'purchaseitem_unit': purchaseitem_unit,
								'purchase_rate': purchase_rate
							};
							$('.loading-overlay').addClass('is-active');
							$.ajax({
								url: '<?php echo  Yii::app()->createAbsoluteUrl('bills/updatesbillitem'); ?>',
								type: 'GET',
								dataType: 'json',
								data: {
									data: data
								},
								success: function(response) {
									if (response.response == 'success') {
										$('#discount_total').html(response.discount_total);
										$('#amount_total').html(response.amount_total);
										$('#tax_total').html(response.tax_total);
										$('#grand_total').val(response.grand_total);
										$().toastmessage('showSuccessToast', "" + response.msg + "");
										$('.addrow').html(response.html);
										$('#buttonsubmit').prop('disabled', false);
									} else {
										$().toastmessage('showErrorToast', "" + response.msg + "");
									}
									howMany = 0;
									$('#description').val('').trigger('change');
									$('#quantity').val('');
									$('#unit').val('');
									$('#rate').val('');
									$('#hsn_code').text('');
									$('#bibatch').val('');
									$('#dis_amount').val('');
									$('#disp').html('0.00');
									$('#item_amount').html('0.00');
									$('#tax_amount').html('0.00');
									$('#sgstp').val('');
									$('#sgst_amount').html('0.00');
									$('#cgstp').val('');
									$('#cgst_amount').html('0.00');
									$('#igstp').val('');
									$('#igst_amount').html('0.00');
									$('#disc_amount').html('0.00');
									$('#total_amount').html('0.00');
									$('#description').focus();
									$('.item_save').attr("id", "0");
									$('#tax_slab').val("18").trigger('change');
									$('#purchase_quantity').val('');
									$('#purchaseitem_unit').val('');
									$('#purchase_rate').val('');
									$('.item_save').attr("id", "0");
									$(".item_save").attr('value', 'Save');
									$('#purchaseitem_unit_hidden').val('');
									$('.base').hide();
								}
							});
						}
					} else {

						$().toastmessage('showErrorToast', "Please fill the details");
					}
				} else {
					$(this).focus();
					$().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
					$(this).focus();
				}



			}


		}




	});

    $('.item_save').keypress(function(e) {
		if (e.keyCode == 13) {
			$('.item_save').click();
		}
	});
    $("#purchase_quantity, #purchase_rate, #sgstp, #cgstp, #igstp, #dis_amount").on("blur change", function() {
    var quantity = parseFloat($("#purchase_quantity").val());
    var rate = parseFloat($("#purchase_rate").val());
	var taxslab =parseFloat($("#tax_slab").val());
	//alert(taxslab);
	var gst= taxslab/2;
	$("#sgstp").val(gst);
	$("#cgstp").val(gst);
    var sgst = parseFloat($("#sgstp").val());
    var cgst = parseFloat($("#cgstp").val());
    var igst = parseFloat($("#igstp").val());
    var dis_amount = parseFloat($("#dis_amount").val());
    dis_amount = isNaN(dis_amount) ? 0 : dis_amount;
    new_amount = isNaN(new_amount) ? 0 : new_amount;
    if ($(this).hasClass("biquantity") || $(this).hasClass("birate")) {
        getconversion_factor();
    }
    
    var amount = quantity * rate;
    var new_amount = amount - dis_amount;
	
    sgst = isNaN(sgst) ? 0 : sgst;
    cgst = isNaN(cgst) ? 0 : cgst;
    igst = isNaN(igst) ? 0 : igst;
    

    var sgst_amount = (sgst / 100) * new_amount;
    var cgst_amount = (cgst / 100) * new_amount;

    var igst_amount = (igst / 100) * new_amount;
    var disp = (dis_amount / amount) * 100;

    sgst_amount = isNaN(sgst_amount) ? 0 : sgst_amount;
    cgst_amount = isNaN(cgst_amount) ? 0 : cgst_amount;
    igst_amount = isNaN(igst_amount) ? 0 : igst_amount;
    amount = isNaN(amount) ? 0 : amount;
    disp = isNaN(disp) ? 0 : disp;

    var tax_amount = sgst_amount + cgst_amount + igst_amount;
    var total_amount = new_amount + tax_amount;

    $("#sgst_amount").html(sgst_amount.toFixed(2));
    $("#cgst_amount").html(cgst_amount.toFixed(2));
    $("#igst_amount").html(igst_amount.toFixed(2));
    $("#disp").html(disp.toFixed(2));
    $("#item_amount").html(amount.toFixed(2));
    $("#tax_amount").html(tax_amount.toFixed(2));
    $("#disc_amount").html(dis_amount.toFixed(2));
    $("#total_amount").html(total_amount.toFixed(2));
});


	$('#purchaseitem_unit').change(function() {
		getconversion_factor();

	});

	function getconversion_factor() {

		var purchase_unit = $.trim($("#purchaseitem_unit option:selected").text());
		var base_unit = $.trim($("#item_unit").val());
		var item_id = $('#description').val();
		var quantity = $("#purchase_quantity").val();
		var rate = $("#purchase_rate").val();
		var taxslab =parseFloat($("#tax_slab").val());
		var gst= taxslab/2;
			$("#sgstp").val(gst);
			$("#cgstp").val(gst);
		var sgst_p = parseFloat($("#sgstp").val());
		var cgst_p = parseFloat($("#cgstp").val());
		var igst_p = parseFloat($("#igstp").val());
		var dis_p = parseFloat($("#disp").val());
		if (purchase_unit !== '' && base_unit !== '' && item_id !== "" && quantity != '') {

			if (purchase_unit != base_unit) {
				console.log('hi');
				$.ajax({
					method: "POST",
					data: {
						'purchase_unit': purchase_unit,
						'base_unit': base_unit,
						'item_id': item_id
					},
					dataType: "json",
					url: '<?php echo Yii::app()->createAbsoluteUrl('bills/getUnitconversionFactor'); ?>',
					success: function(result) {

						if (result != '') {

							$("#item_conversion_data").show();
							$("#item_unit_additional_conv").html(result);
						}
						var conversionvalue = result;
						$(".base").show();
						var amount = quantity * rate;
						var base_rate = base_quantity = 0;
						var base_quantity = conversionvalue * quantity;
						if (isNaN(base_quantity))
							base_quantity = 0;
						if (base_quantity != 0) {
							base_rate = amount / base_quantity;
						}
						base_rate = parseFloat(base_rate).toFixed(2);
						base_quantity = parseFloat(base_quantity).toFixed(2);
						if (isNaN(base_rate))
							base_rate = 0;

						var amount = quantity * rate;
						var dis_amount = ((dis_p / 100) * amount); // discount percent to amount 
						var new_amount = 0;
						if (isNaN(amount))
							amount = 0;
						if (isNaN(dis_amount))
							dis_amount = 0;
						new_amount = amount - dis_amount;

						if (isNaN(new_amount))
							new_amount = 0;

						var sgst_amount = (sgst_p / 100) * new_amount;
						var cgst_amount = (cgst_p / 100) * new_amount;
						var igst_amount = (igst_p / 100) * new_amount;
						console.log("quantity: " + quantity);

						if (isNaN(sgst_amount))
							sgst_amount = 0;
						if (isNaN(cgst_amount))
							cgst_amount = 0;
						if (isNaN(igst_amount))
							igst_amount = 0;
						if (isNaN(dis_amount))
							dis_amount = 0;

						var tax_amount = sgst_amount + cgst_amount + igst_amount;
						var total_amount = new_amount + tax_amount;
						$("#sgst_amount").html(sgst_amount.toFixed(2));
						$("#cgst_amount").html(cgst_amount.toFixed(2));
						$("#igst_amount").html(igst_amount.toFixed(2));
						$("#dis_amount").html(dis_amount.toFixed(2));
						$("#item_amount").html(amount.toFixed(2));
						$("#tax_amount").html(tax_amount.toFixed(2));
						$("#disc_amount").html(dis_amount.toFixed(2));
						$("#total_amount").html(total_amount.toFixed(2));
						$('#quantity').val(base_quantity);
						$('#rate').val(base_rate);

					}

				});

			} else {
				
				$('#quantity').val(quantity);
				$('#rate').val(rate);
				$(".base").hide();
				$("#item_conversion_data").hide();
				$('.base-data').hide();
			}
		}


	}

	$(document).on('click', '.getprevious', function(e) {
		var id = $(this).attr('data-id');
		var res = id.split(",");
		var amount = parseFloat(res[5]);
		var tax_amount = parseFloat(res[6]);
		var disp = parseFloat(res[7]);
		var dis_amount = parseFloat(res[8]);
		var cgst_amount = parseFloat(res[9]);
		var cgstp = parseFloat(res[10]);
		var sgst_amount = parseFloat(res[11]);
		var sgstp = parseFloat(res[12]);
		var taxslab =parseFloat($("#tax_slab").val());
		var gst= taxslab/2;
			
		var igst_amount = parseFloat(res[13]);
		var igstp = parseFloat(res[14]);
		var totalamount = (amount + tax_amount) - dis_amount;

		$('#description').val(res[0]).trigger('change.select2');
		$('#purchase_quantity').val(res[1]);



		$.ajax({
			url: '<?php echo Yii::app()->createUrl('bills/getRelatedUnit'); ?>',
			method: 'POST',
			data: {
				cat_id: res[0],
				selected_unit: res[2]
			},
			dataType: "json",
			success: function(response) {
				$("#purchaseitem_unit").html(response.html);
				$("#purchaseitem_unit").val(res[2]);
				$("#item_unit").val($.trim(response.unit));
				if (response.unit != '') {
					$("#item_baseunit_data").show();
					$("#item_unit_additional_data").html(response.unit);
				}
			},
			complete: function(reponse) {
				$('#purchase_quantity').val(res[1]);
				$('#purchase_rate').val(res[4]);
				$("#purchaseitem_unit").val(res[2]);
				$('#hsn_code').html(res[3]);
				$('#item_amount').text(amount.toFixed(2));
				$('#tax_amount').text(tax_amount.toFixed(2));
				$('#total_amount').text(totalamount.toFixed(2));
				$('#disc_amount').text(dis_amount.toFixed(2));
				$('#dis_amount').val(dis_amount.toFixed(2));
				$('#disp').text(disp.toFixed(2));
				$('#sgst_amount').val(sgst_amount.toFixed(2));
				$('#sgstp').text(sgstp.toFixed(2)).trigger('change');
				$('#cgst_amount').val(cgst_amount.toFixed(2));
				$('#cgstp').text(cgstp.toFixed(2)).trigger('change');
				$('#igst_amount').val(igst_amount.toFixed(2));
				$('#igstp').text(igstp.toFixed(2));
				getconversion_factor();
				//taxSlab();
			}
		})

		// $('#description').val(res[0]).trigger('change.select2');
		// $('#quantity').val(res[1]);
		// $('#item_unit').html(res[2]);
		// $('#hsn_code').html(res[3]);
		// $('#rate').val(res[4]);
		// $('#item_amount').text(amount.toFixed(2));
		// $('#tax_amount').text(tax_amount.toFixed(2));
		// $('#total_amount').text(totalamount.toFixed(2));
		// $('#disc_amount').text(dis_amount.toFixed(2));
		// $('#dis_amount').val(dis_amount.toFixed(2));
		// $('#disp').text(disp.toFixed(2));
		// $('#sgst_amount').text(sgst_amount.toFixed(2));
		// $('#sgstp').val(sgstp.toFixed(2));
		// $('#cgst_amount').text(cgst_amount.toFixed(2));
		// $('#cgstp').val(cgstp.toFixed(2));
		// $('#igst_amount').text(igst_amount.toFixed(2));
		// $('#igstp').val(igstp.toFixed(2));
	})
    $(document).ajaxComplete(function() {
		$('.loading-overlay').removeClass('is-active');
		$('#loading').hide();
	});
	$(document).on('blur', '#quantity', function(e) {
		var newbase_quantity = $('#quantity').val();
		var amount = $('#item_amount').text();
		var base_rate = amount / newbase_quantity;
		base_rate = parseFloat(base_rate).toFixed(2);
		$('#rate').val(base_rate);
	});
	$("#buttonsubmit").keypress(function(e) {
            if (e.keyCode == 13) {
                $('.buttonsubmit').click();
            }
        });

        $(".buttonsubmit").click(function() {
            var general_settings_for_auto_receipt = <?php echo $general_settings_for_auto_receipt; ?>;
            if (general_settings_for_auto_receipt == 1) {
                var answer = confirm("Are you sure you want to auto receipt this item?");
                if (answer) {
					var data = $("#pdfvals1").serialize();					
					$.ajax({
						url: '<?php echo Yii::app()->createUrl('bills/autoReceiptBillWithoutPo'); ?>',
						method: 'POST',
						data: data,						
						success: function(response) {       
							$().toastmessage('showSuccessToast', 'Warehouse Receipt Added Successfully!');
							setTimeout(function(){
							window.location.reload(1);
							}, 2000);
						}
					})                   
                } else {
                    return false;
                }
            } else{
				var data = $("#pdfvals1").serialize();					
				$.ajax({
					url: '<?php echo Yii::app()->createAbsoluteUrl('bills/autoReceiptBillWithoutPo'); ?>',
					method: 'POST',
					data:  data,
					dataType: 'json',											
					success: function(response) {
						console.log(response);
						$().toastmessage('showSuccessToast', 'Billed details added successfully!');	
						setTimeout(function(){
							window.location.reload(1);
						}, 2000);						
					}
				}) 
			}
        });
		$(document).ready(function() {
			
			
			// $(".tax_slab").change(function(){  
				                 
			// 	var tax_slab = $(this).val();
			// 	var gst = parseFloat(tax_slab) / 2;
			// 	if(tax_slab==18){
			// 		$("#sgstp").val('9');
			// 		$("#cgstp").val('9');
			// 		alert($("#cgstp").val());
			// 	}
			// 	$(this).parents(".row").find(".sgstp").val(gst).trigger("change");
			// 	$(this).parents(".row").find(".cgstp").val(gst).trigger("change");
			// });
		});
   

 </script>