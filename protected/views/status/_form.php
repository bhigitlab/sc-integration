<?php
/* @var $this StatusController */
/* @var $model Status */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'status-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'caption'); ?>
		<?php echo $form->textField($model,'caption',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'caption'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'status_type'); ?>
            
            <?php
            echo $form->dropDownList($model, 'status_type', CHtml::listData(Status::model()->findAll(array(
                                'select' => array('status_type'),
                                'order' => 'status_type',
                                'distinct' => true
                            )), 'status_type', 'status_type'), array('empty' => 'Choose a type', 'style' => 'width:120px'));
            ?>
            
            
		<?php echo $form->error($model,'status_type'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->