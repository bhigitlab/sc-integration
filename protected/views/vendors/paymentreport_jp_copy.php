<?php
Yii::app()->clientScript->registerScript('search', "

    $('#btSubmit').click(function(){

            $('#vendorform').submit();

    });
");

?>

<div class="container" id="project">

    <br /><br />

    <h3>Vendors Payment Reports</h3>

    <br /><br />

    <form id="vendorform" action="<?php $url = Yii::app()->createAbsoluteUrl("vendors/paymentReport"); ?>" method="POST">

        <div class="search-form" >

            <?php echo CHtml::label('Vendors : ', ''); ?>

            <?php

                echo CHtml::dropDownList('vendor_id', 'vendor_id', CHtml::listData(Vendors::model()->findAll(array(

                                'select' => array('vendor_id, name'),

                                'order' => 'name',
								'condition' => 'company_id ='.Yii::app()->user->company_id,
                                'distinct' => true

                            )), 'vendor_id', 'name'), array('empty'=>'Select Vendor','id' => 'vendor_id', 'options' => array($vendor_id => array('selected' => true))));

            ?>

            <?php

                echo CHtml::submitButton('GO', array('id' => 'btSubmit'));

            ?> 

        </div>

    </form>

    <br/><br/>



<?php if ((!empty($sql) || !empty($client)) && ($vendor_id != 0)) { ?>

        

    <div id="contenthtml" class="contentdiv">

        <div class="pull-right">

                <a style="cursor:pointer;" id="download" class="save_btn" href="<?php echo $this->createAbsoluteUrl('vendors/savePaymentreport', array("vendor_id"=>$vendor_id)) ?>">SAVE AS PDF</a>

                <a style="cursor:pointer;" id="download" class="save_btn" href="<?php echo $this->createAbsoluteUrl('vendors/savetoexcel', array("vendor_id"=>$vendor_id)) ?>">SAVE AS EXCEL</a>
            
            <br/>

            </div>

        <h2><center>Vendor Summary of <b><?php echo $name;?></b></center></h2>
        <br><br>
        <div class="container-fluid">

                

                <table class="table table-bordered ">

                    <tr>
                         <td colspan="2"><b>Vendor Name: </b><?php echo $name; ?></td>
<!--                     <td><b>Vendor Name: </b><?php //echo $name;?></td>
                     <td></td>-->
                     </tr>
                     <tr>
                     <td colspan="2" class="whiteborder">&nbsp;</td>
                     </tr>
                        <!--<td align="right"><b>Receipt</b></td>-->
                     <tr>
                         <td></td>
                        <td align="right"><b>Amount</b></td>
                        
                     </tr>
                    
                    

                <?php

                $grand = 0;
                
                foreach ($sql as $new) {

                    ?>



                        <tr>

                            <th colspan="2" ><?php print_r($new['type_name']); ?></th>

                        </tr>

                        <?php

                        $tblpx = Yii::app()->db->tablePrefix;
						
						 $sqlexp = Yii::app()->db->createCommand("select * from " . $tblpx . "expenses

                left join " . $tblpx . "projects as p ON p.pid=" . $tblpx . "expenses.projectid  WHERE amount!=0 AND exptype=" . $new['type_id'] . "  AND vendor_id=" . $new['vendor_id'] . "

                ORDER BY exptype,projectid")->queryAll();
						
                       
                
                $sqlexpsum = Yii::app()->db->createCommand("select sum(amount) as sumamt,count(*) as count,exptype,projectid from " . $tblpx . "expenses

                INNER join " . $tblpx . "projects as p ON p.pid=" . $tblpx . "expenses.projectid  WHERE amount!=0 AND exptype=" . $new['type_id'] . "  AND vendor_id=" . $new['vendor_id'] . "

                Group By projectid")->queryAll();

                        $total = 0;
                        $projectname = $projectnamesub = '';
                        $totalpro = 0;
                        $i = 0;

                        foreach ($sqlexp as $newsql) {

                            if ($newsql['amount'] != 0) {
								
								if ($projectname != $newsql['name']) {
                                     $i++;
                                    if($i%2==0){
                                        $class='even';
                                    }else{
                                         $class='odd';
                                    }
                                    if($i==1){
                                        $projectnamesub=$newsql['name'];
                                    }
                                    $totalpro = $totalpro + $newsql['amount'];
                                    ?>
                        <tr class="<?= $class ?>">
                                        <td><b>Project Name :</b> &nbsp;<?php echo $newsql['name'] ?><b><?php
                                        foreach ($sqlexpsum as $sqsum) {
                                        if(($newsql['projectid'] == $sqsum['projectid'])){?>
                                        </td><td align="right"> Sub total :  <b><?php print_r(Controller::money_format_inr($sqsum['sumamt']));
                                        }} ?></b> </td>
                                        
                                    </tr>

                                    <?php
                                    $projectname = $newsql['name'];
                                }
                                

                                ?>

                                <tr>

                                    <td><?php echo date('d-M-Y', strtotime($newsql['date'])); ?> <?php echo " ".$newsql['description'];  //print_r($newsql['description']);  ?></td>

                                    <!--<td></td>-->

                                    <td align="right"><?php print_r(Controller::money_format_inr($newsql['amount'])); ?></td>

                                    <?php $total += $newsql['amount']; ?>

                                </tr>

                                <?php

                            }

                        }

                        ?>
                        <tr>
                        
                        <td align="right" colspan="2"><?php echo '<b>' . Controller::money_format_inr($total,0) . '</b>'; ?></td>
                        
                        <!--<td ></td>-->
                        </tr>        

                    
                    <?php //echo '<b>' . $total . '</b>';

                $grand +=$total; ?>




                    <?php

                }

                ?>

                    <tr>
                        <td><b>Net Expense</b></td>
                        <td align="right"><?php echo '<b>' . Controller::money_format_inr($grand,0) . '</b>'; ?></td>
                        
                        <!--<td ></td>-->
                    </tr>
            

                    <tr>
                        <td colspan="2" class="whiteborder">&nbsp;</td>
                    </tr>
                    <tr>

                        <td colspan="2"><b>Payments</b></td>
                    </tr>

                    <?php
                   
                    $clienttotal = 0;

                    foreach ($client as $newclient) {

                        ?>

                        <tr>

                            <td><?php echo date('d-M-Y', strtotime($newclient['date'])); ?> <?php print_r($newclient['description']); ?></td>

                            <td align="right" border="1"><?php print_r(Controller::money_format_inr($newclient['amount'])); ?></td>

                            <!--<td></td>-->

                        <?php $clienttotal += $newclient['amount']; ?>

                        </tr>

                        <?php }

                    ?>
                    <tr>
                        <td align="right" colspan="2"><?php echo '<b>' . Controller::money_format_inr($clienttotal,0) . '</b>'; ?></td>
                        
                        
                    </tr>
                    <tr>

                        <td colspan="2"><b>Partial Payments</b></td>
                    </tr>
                    <?php
                    $total = 0;
                    foreach ($sql as $new) {
                        $tblpx = Yii::app()->db->tablePrefix;

                        $sqlexp = Yii::app()->db->createCommand("select * from " . $tblpx . "expenses

                WHERE amount!=0 AND exptype=" . $new['type_id'] . "  AND vendor_id=" . $new['vendor_id'] . "

                ")->queryAll();

                        

                        foreach ($sqlexp as $newsql) {

                            if ($newsql['paid'] != 0) {

                                ?>

                                <tr>

                                    <td><?php echo date('d-M-Y', strtotime($newsql['date'])); ?> <?php echo " ".$newsql['description'];  //print_r($newsql['description']);  ?></td>

                                    <!--<td></td>-->

                                    <td align="right"><?php print_r(Controller::money_format_inr($newsql['paid'])); ?></td>

                                    <?php $total += $newsql['paid']; ?>

                                </tr>

                                <?php

                            }

                        }
                        ?>
                              
                                <?php
                    }
$clienttotal = $clienttotal+$total;
                        ?>
                                <tr>

                        
                        <td align="right" colspan="2"><?php echo '<b>' . Controller::money_format_inr($total,0) . '</b>'; ?></td>
                        
                    </tr>  
                                <tr>

                                    <td><b>Net Payments</b></td>
                        <td align="right"><?php echo '<b>' . Controller::money_format_inr($clienttotal,0) . '</b>'; ?></td>
                        
                    </tr>
                        <tr>
                            <td colspan="2" class="whiteborder">&nbsp;</td>
                        </tr>
                    <tr>
                        
                        <td>TOTAL AMOUNT TO BE PAID: </td>
                        <td align="right"><?php echo '<b>' . Controller::money_format_inr($grand,0) . '</b>'; ?></td>
                        
                        <!--<td ></td>-->
                    </tr>

                    <tr>

                        <td>PAID :</td>
                        <td align="right"><?php echo '<b>' . Controller::money_format_inr($clienttotal,0) . '</b>'; ?></td>
                        
                        <!--<td ></td>-->
                    </tr>

                    <?php $balance = $grand - $clienttotal ?>

                    <tr>

                        <td><?php 

                    echo "REMAINING AMOUNT";

                ?> : </td><td align="right"><?php echo '<b>' . Controller::money_format_inr($balance,0) . '</b>'; ?></td>
                    <!--<td></td>-->
                    </tr>
                </table>

            </div>

        </div>

    <?php

}else{
	
   // echo "<p> No results found.</p>";
     // echo '<pre>';
        //print_r($sqlexp);
        // print_r($client);
        //die();
        ?>
        <div id="contenthtml" class="contentdiv">
            <div class="pull-right">
        <a style="cursor:pointer; " class="save_btn"  href="<?php echo Yii::app()->createUrl('vendors/savetopdf1') ?>">SAVE AS PDF</a>
        <a style="cursor:pointer; " class="save_btn"  href="<?php echo Yii::app()->createUrl('vendors/savetoexcel1') ?>">SAVE AS EXCEL</a>
         <br><br>
    </div>
            <table class="table table-bordered ">

                <thead>
                    <tr>
                        <th>Sl No.</th>
                        <th>Vendor Name</th>
                        <th align="right" style="text-align:right">Credit</th>
                        <th align="right" style="text-align:right">Debit</th>
                        <th align="right" style="text-align:right">Balance</th>
                    </tr>
                </thead>
                 <tbody>
                <?php 
                $vendor = Yii::app()->db->createCommand("
                    SELECT sum(jp_expenses.amount) as credit,jp_vendors.name,
                    sum(jp_expenses.paid) as debit,jp_vendors.vendor_id 
                    FROM `jp_expenses`
					INNER JOIN jp_vendors ON jp_vendors.vendor_id= jp_expenses.vendor_id 
					WHERE jp_expenses.type=73 AND jp_expenses.company_id =".Yii::app()->user->company_id." 
					AND jp_vendors.company_id =".Yii::app()->user->company_id."
					GROUP by jp_expenses.vendor_id ORDER BY name")->queryAll();
					
               $vendor_debit_data =  Yii::app()->db->createCommand("
                   SELECT SUM( jp_dailyvendors.amount ) AS debitamt ,jp_vendors.vendor_id 
                   FROM jp_dailyvendors 
                   INNER JOIN jp_vendors ON jp_dailyvendors.vendor_id = jp_vendors.vendor_id 
                   WHERE jp_dailyvendors.company_id =".Yii::app()->user->company_id. "
                   AND jp_vendors.company_id =".Yii::app()->user->company_id."
                   GROUP by jp_vendors.vendor_id")->queryAll();
                             
                $vendor_debit_data_array = array();
               foreach ($vendor_debit_data as $vendebit){
                    
                    $vendor_debit_data_array[$vendebit['vendor_id']] = $vendebit['debitamt'];
               }
                  //print_r($vendor_debit_data_array);       
                $i=0;$totalcredit=0;$totaldebit= $totalremaining=0;
               foreach ($vendor as $ven){
                   $i++;
                   ?>
                     <tr>
                         <td><?= $i;?></td>
                         <td><?php echo $ven['name'];?></td>
                         <td align="right"><?php 
                         $totalcredit= $totalcredit+$ven['credit'];
                         echo Controller::money_format_inr($ven['credit'],0,".","");?></td>
                          <td align="right"><?php
                          $ven['debit'] = $ven['debit'] + (isset($vendor_debit_data_array[$ven['vendor_id']]) ? $vendor_debit_data_array[$ven['vendor_id']] : 0);
                           $totaldebit= $totaldebit+$ven['debit'];
                          echo Controller::money_format_inr($ven['debit'],0,".","");?></td>
                       <td align="right"><?php  
                       $totalremaining = $totalremaining + ($ven['credit']-$ven['debit']);
                      // echo Controller::money_format_inr(abs($ven['credit']-$ven['debit']),0,".",""); 
                      echo Controller::money_format_inr($ven['credit']-$ven['debit']);
                       ?></td>
                     </tr>
                    
                
               <?php }?>
                     <tr>
                         <td colspan="2" align="right"><b>Total</b></td>
                         <td align="right"><?php echo Controller::money_format_inr($totalcredit); //echo Controller::money_format_inr($totalcredit,0,".",""); ?></td>
                         <td align="right"><?php echo Controller::money_format_inr($totaldebit); //echo Controller::money_format_inr($totaldebit,0,".",""); ?></td>
                         <td align="right"><?php echo Controller::money_format_inr($totalremaining);//echo Controller::money_format_inr($totalremaining,0,".",""); ?></td>
                     </tr>
                 </tbody>

            </table>

        </div>


        <?php
    }



?>

</div>
