ALTER TABLE `jp_sales_quotation` ADD `revision_status` INT NOT NULL DEFAULT '0' AFTER `revision_remarks`;

ALTER TABLE `jp_sales_quotation` ADD `revision_approved` INT NOT NULL DEFAULT '0' AFTER `revision_status`;