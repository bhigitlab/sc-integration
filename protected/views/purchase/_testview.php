<div id="msg_box"></div>
<?php
if ($index == 0) {
    ?>
    <thead>
        <tr>
            <!--<th style="visibility:hidden"></th>-->
            <th>No</th>
            <th>Purchase No</th>
            <th>Company</th>
            <th>Project</th>
            <th>Expense Head</th>
            <th>Vendor</th>            
            <th>Total Amount</th>
            <th>Date</th>
            <th>Status</th>
            <th>Total amount billed</th>
            <th>Balance to be billed</th>
            <th>Billing Status</th>
            <?php
                if((isset(Yii::app()->user->role) && ((in_array('/purchase/viewpurchase', Yii::app()->user->menuauthlist)) || (in_array('/purchase/updatepurchase', Yii::app()->user->menuauthlist))  ) )){
             ?>
            <th></th>
                <?php } ?>
            
            
        </tr>   
    </thead>
<?php } 
if(isset($_REQUEST['Purchase_page'])){
  $index = (($_REQUEST['Purchase_page']-1)*20)+$index;
}else{
    $index = $index;
}
?>




    <tr class="<?php echo ($data->permission_status == 'No')?'permission_style':''; ?>">
        <td class="text-right"><?php  echo $index+1; ?></td>
		<td><?php echo $data->purchase_no; ?></td>
		<td>
		<?php
		$company = Controller::getCompanyName($model->company_id);
		echo $company; ?>
		</td>
		<td><?php echo $data->project['name'];  ?></td>
		<td><?php echo $data->expensehead['type_name'];?></td>
		<td><?php echo $data->vendor['name'];  ?></td>
		<td style="text-align: right;"><?php echo Controller::money_format_inr($data->total_amount,2,1); ?></td>
		<td class="wrap"><?php echo  date("d-m-Y", strtotime($data->purchase_date)); ?></td>
		<?php
			$class="";
			if($data->purchase_status == 'permission_needed') {
				$class="highlight";
			} else {
				$class="";
			}
			$permission = Controller::getPurchaseItemsForPermission($data->p_id,2);
			if($permission) {
				$class="highlight";
			} else {
				$class="";
			}
		?>			

		
		<td class="<?php echo $class; ?>">
		<?php
			if($data->purchase_status == 'saved')
			{
				echo "Saved";
			} else{
			$permission = Controller::getPurchaseItemsForPermission($data->p_id,2);
			if($permission || $data->purchase_status == 'permission_needed') {
				 echo "Permission needed";	
                                } else if($data->permission_status == 'No'){
                                    //echo "<div class='approvallabel_".$data->p_id."'>Approval needed </div>";
                                    echo "<div class='purchase_status_".$data->p_id." approvallabel_".$data->p_id."' id='".$data->p_id."' data-toggle='tooltip'>Approval needed</div>";
                                } else {
					
				$style ='';
				$items = PurchaseItems::model()->findAll(array("condition"=>"purchase_id=$data->p_id"));	
				if($data->purchase_status == 'draft' && !empty($items)){
				?>
				<div style="cursor:pointer" class="update_status purchase_status_<?php echo $data->p_id; ?>" id="<?php echo $data->p_id; ?>" data-toggle="tooltip" title="Change status to saved">Draft</div>
				<?php
				} else {
					echo "Draft";
				}	
				
			?>
			<?php } } ?>
		</td>
		
		<td style="text-align: right;">
		<?php
		$tblpx 	 = Yii::app()->db->tablePrefix;
		$value = Yii::app()->db->createCommand("SELECT SUM(bill_totalamount) as bill_totalamount  FROM `{$tblpx}bills` WHERE purchase_id=".$data->p_id)->queryRow();
		 if($value['bill_totalamount'] !=''){
			 $loop_billed_amount =  $value['bill_totalamount'];
		 } else {
		    $loop_billed_amount = '0.00';	 
		 }
		 
		 echo Controller::money_format_inr($loop_billed_amount,2,1);
		?>
		</td>
		
		<td style="text-align: right;">
		<?php
		$blance_total = 0;
		$tblpx 	 = Yii::app()->db->tablePrefix;
		
		
		$value_data = Yii::app()->db->createCommand("SELECT SUM(bill_totalamount) as bill_totalamount  FROM `{$tblpx}bills` WHERE purchase_id=".$data->p_id)->queryRow();
		 if($value_data['bill_totalamount'] !=''){
			 $current_billed_amount =  $value_data['bill_totalamount'];
		 } else {
		    $current_billed_amount = '0.00';	 
		 }
		
		$loop_blance_amount = '0.00';
//		$value = Yii::app()->db->createCommand("SELECT item_id, quantity, rate FROM {$tblpx}purchase_items WHERE purchase_id=".$data->p_id)->queryAll();
//		$max = array();
//		$blanc_amount = '';
//		$loop_blance_amount = 'dd';
//		$tvalue = 0;
//		if(!empty($value)) {
//		foreach($value as $values) {
//			$bill_item = Yii::app()->db->createCommand("SELECT sum(billitem_quantity) as billitem_quantity FROM {$tblpx}billitem WHERE purchaseitem_id=".$values['item_id'])->queryRow();
//           
//
//           if(!empty($bill_item)){
//				  $blance_quantity = $values['quantity']-$bill_item['billitem_quantity'];
//				  $max = $blance_quantity*$values['rate'];
//				  $blanc_amount += $max; 
//		    } else {
//				 $blanc_amount += $values['quantity']*$values['rate'];
//			}  
//		}
//		
//		
//		if($loop_billed_amount == $data->total_amount)
//		{
//			$loop_blance_amount = '0.00';
//  	    } else {
//			$loop_blance_amount = $blanc_amount;
//		} 
//		
//	    } else {
//			$loop_blance_amount = '0.00';
//		}
		$loop_blance_amount = $data->getloopbalanceamount($data->p_id,$data->total_amount,$loop_billed_amount);
		echo Controller::money_format_inr($loop_blance_amount,2,1);
		?>
		</td>
		
		<?php
		$color = '';
		$label = '';
		$lable_color = '';
		
		  if($data->status['caption'] == 'PO not issued') {
			 $label = "PO not Issued";
			 $color = '#DD1035';
			 $lable_color = 'style="color:#fff;"';
		 } else if($data->status['caption'] == 'Fully Billed') {
			 $label = "Fully Billed";
			 $color = '#2AD300';
			 $lable_color = "";
		 } else if($data->status['caption'] == 'Pending to be Billed') {
			$label = "Pending to be Billed";
			$color = '#FF6347';
			$lable_color = "";
		 } else if($data->status['caption'] == 'Partially Billed') {
			 $label = "Partially Billed";
			 $color = '#FFFF00';
			 $lable_color = "";
		  }
		  
		?>
		
		<td class="bill_status" style="background:<?php echo $color; ?>"><span class="bill_label" <?php echo $lable_color; ?>><?php echo $data->status['caption']; // echo $label; ?></span></td>
		
<!--		<td><div id="<?php echo $data->p_id; ?>" class="view_purchase"  data-toggle="tooltip" title="View"><span class="fa fa-eye viewExpense" data-toggle="modal" data-target=".edit"></span></div>   
		</td>
		<td>
		<?php
		if (Yii::app()->user->role != 3) {
		if($data->purchase_status != 'saved')
		{
		?>
		<div class="edit_purchase edit_option_<?php echo $data->p_id; ?>" id="<?php echo $data->p_id; ?>"  data-toggle="tooltip" title="Edit"><span class="fa fa-edit editExpense" data-toggle="modal" data-target=".edit" data-id="<?php echo $data->p_id; ?>"></span></div>
		<?php } } ?>
		</td>-->
                 
                     <?php
                        if((isset(Yii::app()->user->role) && ((in_array('/purchase/viewpurchase', Yii::app()->user->menuauthlist)) || (in_array('/purchase/updatepurchase', Yii::app()->user->menuauthlist)) || (in_array('/purchase/permission', Yii::app()->user->menuauthlist))  ) )){
                     ?>
                <td>
                     <span class="icon icon-options-vertical popover-test" data-toggle="popover"   data-placement="left" type="button" data-html="true" style="cursor: pointer;" ></span>
                    <div class="popover-content hide">
                        <ul class="tooltip-hiden">
                            <?php
                            if((in_array('/purchase/viewpurchase', Yii::app()->user->menuauthlist))){
                            ?>
                             <li><button id="<?php echo $data->p_id; ?>" class="btn btn-xs btn-default view_purchase">View</button></li>
                            <?php  }
                            if((in_array('/purchase/updatepurchase', Yii::app()->user->menuauthlist))){
                                if($data->purchase_status != 'saved')
                                { ?>
                            <li><button id="<?php echo $data->p_id; ?>" class="btn btn-xs btn-default edit_purchase edit_option_<?php echo $data->p_id; ?>">Edit</button></li>
                            <?php } } ?>
                            <?php
                            if((in_array('/purchase/permission', Yii::app()->user->menuauthlist))){
                                if($data->permission_status == 'No')
                                    { ?>
                            <li><button id="<?php echo $data->p_id; ?>" class="btn btn-xs btn-default permission_item approveoption_<?php echo $data->p_id; ?>">Rate Approve</button></li>
                            <?php } } ?>
                        </ul>
                    </div>
                    </td>
                        <?php } ?>


</tr>
<?php
if ($index == 0) {
    ?>
    <tfoot>
        <tr>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>            
            <th></th>
            <th></th>
            <th>Total:</th>          
            <th style="text-align: right;">
            <?php
				echo Controller::money_format_inr($total_billedamount,2,1);
            ?>
            </th>
            <th style="text-align: right;">
            <?php echo Controller::money_format_inr($total_blanceamount,2,1); ?>
            </th>
            <th></th>
            <?php
                if((isset(Yii::app()->user->role) && ((in_array('/purchase/viewpurchase', Yii::app()->user->menuauthlist)) || (in_array('/purchase/updatepurchase', Yii::app()->user->menuauthlist))  ) )){
             ?>
            <th></th>
                <?php } ?>
           
            

        </tr>   
    </tfoot>
<?php } ?>
<style>
    .permission_style {
            background: #f8cbcb !important;
        }   
</style>