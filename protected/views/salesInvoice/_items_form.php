<style>
    .cke_contents {
        height: 80px !important;
    }

    .panel-body>.panel {
        margin-top: 10px;
    }

    .pb-2 {
        padding-bottom: 10px;
    }

    .h-head {
        height: 45px;
    }

    .add_new_sec {
        margin-bottom: 10px;
        padding: 6px 10px;
    }

    .add_new_sec button {
        border: none;
        box-shadow: none;
    }

    .add_new_sec h4 {
        margin: 0px !important;
        padding-top: 10px;
    }


    .panel-heading h4 {
        margin: 0px !important;
    }

    h4.categ_item {
        color: #337ab7;
        line-height: 26px;
        font-size: 1.5rem;
        font-weight: bold;
    }

    i.toggle-caret {
        font-size: 2rem;
        font-weight: bold;
        color: #3383ca;
    }

    input.total_sum {
        width: 50px;
        margin-right: 3rem;
    }

    .sum_sec {
        max-width: 400px;
        text-align: right;
        margin-left: auto;
        background: #fafafa;
        margin-bottom: 10px;
        font-weight: bold;
        white-space: nowrap;
        padding: 10px;
    }

    .toast-container {
        width: 350px;
    }

    .toast-position-top-right {
        top: 57px;
        right: 6px;
    }

    .toast-item-close {
        background-image: none;
        cursor: pointer;
        width: 12px;
        height: 12px;
        text-align: center;
        border-radius: 2px;
    }

    .toast-item-image {
        font-size: 24px;
    }

    .toast-item-close:hover {
        color: red;
    }

    .toast-item {
        border: transparent;
        border-radius: 3px;
        font-size: 10px;
        opacity: 1;
        background-color: rgba(34, 45, 50, 0.8);
    }

    .toast-item-wrapper p {
        margin: 0px 5px 0px 42px;
        font-size: 14px;
        text-align: justify;
    }


    .toast-type-success {
        background-color: #00A65A;
        border-color: #00A65A;
    }

    .toast-type-error {
        background-color: #DD4B39;
        border-color: #DD4B39;
    }

    .toast-type-notice {
        background-color: #00C0EF;
        border-color: #00C0EF;
    }

    .toast-type-warning {
        background-color: #F39C12;
        border-color: #F39C12;
    }
</style>
<?php
$main_items = $model->getMainItems($model->id);
?>
<div class="add_new_sec clearfix border-bottom">
    <div class="pull-left">
        <h4>Category</h4>
    </div>
    <?php
    if (!empty($main_items)) {
    ?>
        <div class="pull-right"><button class="btn btn-primary addcateg" data-toggle="collapse" data-target="#addCategory">Add New</button></div>
    <?php
    }
    ?>

</div>
<?php
if (!$model->isNewRecord) {
    if (!empty($main_items)) {

        foreach ($main_items as $key => $main_item) {
            $sub_items = $model->getSubItems($main_item->id);
?>
            <div class="panel panel-default">


                <div class="panel-heading categ_toggle" data-toggle="collapse" data-target="#collapseOne_<?php echo $key; ?>">
                    <div class="clearfix">
                        <div class="pull-left">
                            <h4 class="categ_item" id="categ_title_<?php echo $key; ?>"><?php echo $main_item->main_title; ?></h4>
                        </div>
                        <div class="pull-right">
                            <i class="toggle-caret fa fa-angle-up"></i>
                        </div>
                        <div class="pull-right">
                            <label>Total:</label>
                            <input type="text" name="" value="<?php echo $main_item->total_amount; ?>" class="total_sum" id="total_sum_<?php echo $main_item->id; ?>">
                        </div>
                    </div>
                </div>
                <div id="collapseOne_<?php echo $key; ?>" class="panel-collapse collapse">
                    <div class="panel-body">
                        <?php $form = $this->beginWidget('CActiveForm', array(
                            'id' => 'sales-invoice-item-form' . $key,
                            'enableAjaxValidation' => false,
                            // 'action' => array('addItem'),
                            'htmlOptions' => array(
                                'class' => 'item_form',
                            )
                        )); ?>
                        <input type="hidden" name="SalesInvoiceItem[sales_invoice_id]" id="SalesInvoiceItem_sales_invoice_id" value="<?php echo $main_item->sales_invoice_id; ?>">
                        <input type="hidden" name="SalesInvoiceItem[id]" id="SalesInvoiceItem_id" value="<?php echo $main_item->id; ?>">
                        <div class="row">
                            <div class="col-md-2">
                                <?php echo $form->textField($main_item_model, 'main_title', array('class' => 'form-control require', 'value' => $main_item->main_title, 'id' => 'SalesInvoiceItem_main_title-' . $key)); ?>
                                <div class="text-danger hide">Field required</div>
                            </div>
                            <div class="col-md-6">
                                <label for="">Description</label>
                                <?php echo $form->textArea($main_item_model, 'main_description', array('class' => 'input_data text_editor',  'id' => 'SalesInvoiceItem_main_description-' . $key, 'value' => $main_item->main_description)); ?>
                                <?php echo $form->error($main_item_model, 'main_description'); ?>
                            </div>
                        </div>
                        <?php $this->endWidget(); ?>
                        <div class="clearfix">
                            <div class="pull-left">
                                <h4>Item details</h4>
                            </div>

                            <?php
                            if (!empty($main_items)) {
                            ?>
                            <?php
                            }
                            ?>

                        </div>
                        <div class="panel panel-default addItem">
                            <div class="panel-heading">
                                <h4>Add Item</h4>
                            </div>
                            <div class="panel-body">
                                <?php $form = $this->beginWidget('CActiveForm', array(
                                    'id' => 'sales-invoice-sub-item-form' . $key,
                                    'action' => array('addSubItem'),
                                    'htmlOptions' => array(
                                        'class' => 'sub_item',
                                    )
                                )); ?>

                                <input type="hidden" name="SalesInvoiceSubItem[invoice_id]" id="SalesInvoiceSubItem_sales_invoice_id" value="<?php echo $main_item->sales_invoice_id; ?>">
                                <input type="hidden" name="SalesInvoiceSubItem[invoice_item_id]" id="SalesInvoiceSubItem_invoice_item_id" value="<?php echo $main_item->id; ?>">
                                <div class="clone_section-<?php echo $main_item->id; ?>">


                                    <div class="item_div-<?php echo $main_item->id; ?>" id="item_div1-<?php echo $main_item->id; ?>">
                                        <div class="row">
                                            <input type="hidden" name="SalesInvoiceSubItem[id][]" id="SalesInvoiceSubItem_id" value="<?php echo $sub_item_model->id; ?>">
                                            <div class="col-md-6">
                                                <?php echo $form->labelEx($sub_item_model, 'item_description'); ?>
                                                <?php echo $form->textArea($sub_item_model, 'item_description[]', array('class' => 'input_data text_editor',  'id' => 'SalesInvoiceSubItem_item_description_' . $main_item->id . '-0', 'value' => $sub_item_model->item_description)); ?>
                                                <?php echo $form->error($sub_item_model, 'main_title'); ?>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label>Type</label>
                                                            <div>
                                                                <?php
                                                                echo CHtml::activeDropDownList(
                                                                    $sub_item_model,
                                                                    'item_type[]',
                                                                    array('1' => 'Qty*rate', '2' => 'lumpsum', '3' => 'Length*width', '4' => 'Length*width*height'),
                                                                    array('empty' => 'Select Type', 'class' => 'form-control item_type', 'id' => 'SalesInvoiceSubItem_item_type_' . $main_item->id . '-0')
                                                                );
                                                                ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3" id="quantity_0">
                                                        <div class="form-group">
                                                            <label for="">Quantity</label>
                                                            <?php echo $form->textField($sub_item_model, 'quantity[]', array('class' => 'form-control calculate', 'id' => 'SalesInvoiceSubItem_quantity_' . $main_item->id . '-0', 'main-id' => $main_item->id)); ?>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 " id="unit_0">
                                                        <div class="form-group">
                                                            <label for="">Unit</label>
                                                            <?php echo $form->textField($sub_item_model, 'unit[]', array('class' => 'form-control', 'id' => 'SalesInvoiceSubItem_unit_' . $main_item->id . '-0', 'main-id' => $main_item->id)); ?>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3" id="rate_0">
                                                        <div class="form-group">
                                                            <label for="">Rate</label>
                                                            <?php echo $form->textField($sub_item_model, 'rate[]', array('class' => 'form-control calculate', 'id' => 'SalesInvoiceSubItem_rate_' . $main_item->id . '-0', 'main-id' => $main_item->id)); ?>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-3 " id="length_0">
                                                        <div class="form-group">
                                                            <label for="">Length</label>
                                                            <?php echo $form->textField($sub_item_model, 'length[]', array('class' => 'form-control calculate', 'id' => 'SalesInvoiceSubItem_length_' . $main_item->id . '-0', 'main-id' => $main_item->id)); ?>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 " id="width_0">
                                                        <div class="form-group">
                                                            <label for="">Width</label>
                                                            <?php echo $form->textField($sub_item_model, 'width[]', array('class' => 'form-control calculate', 'id' => 'SalesInvoiceSubItem_width_' . $main_item->id . '-0', 'main-id' => $main_item->id)); ?>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 " id="height_0">
                                                        <div class="form-group">
                                                            <label for="">Height</label>
                                                            <?php echo $form->textField($sub_item_model, 'height[]', array('class' => 'form-control calculate', 'id' => 'SalesInvoiceSubItem_height_' . $main_item->id . '-0', 'main-id' => $main_item->id)); ?>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <label class="text-right">Amount</label>
                                                        <?php echo $form->textField($sub_item_model, 'amount[]', array('class' => 'form-control', 'id' => 'SalesInvoiceSubItem_amount_' . $main_item->id . '-0', 'main-id' => $main_item->id)); ?>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <div class="text-right">
                                    <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-info')); ?>
                                </div>
                                <?php $this->endWidget(); ?>
                                <a href="" class="add_new_row" id="row-<?php echo $main_item->id; ?>">Add new Row</a>
                            </div>
                        </div>
                        <?php if (!empty($sub_items)) {
                            $this->renderPartial('_sub_items_list', array('model' => $model, 'sub_items' => $sub_items, 'main_item' => $main_item));
                        } ?>

                    </div>
                </div>
            </div>
<?php
        }
    }
}
?>

<div class="panel panel-default  <?php echo  empty($main_items) ? 'collapse in' : 'collapse' ?>" id="addCategory">
    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'sales-invoice-item-form',
        'enableAjaxValidation' => false,
        'action' => array('addItem'),
        'htmlOptions' => array(
            'class' => 'item_form',
        )
    )); ?>
    <input type="hidden" name="SalesInvoiceItem[sales_invoice_id]" id="SalesInvoiceItem_sales_invoice_id" value="<?php echo $model->id; ?>">
    <input type="hidden" name="SalesInvoiceItem[id]" id="SalesInvoiceItem_id" value="<?php echo $main_item_model->id; ?>">
    <div class="panel-heading h-head">
        <div class="row">
            <div class="col-md-2">
                <?php echo $form->textField($main_item_model, 'main_title', array('class' => 'form-control require', 'placeholder' => 'category')); ?>
                <?php echo $form->error($main_item_model, 'main_title'); ?>
            </div>
        </div>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-6">
                <label for="">Description</label>
                <?php echo $form->textArea($main_item_model, 'main_description', array('class' => 'text_editor')); ?>
                <?php echo $form->error($main_item_model, 'main_description'); ?>
            </div>
        </div>
        <?php $this->endWidget(); ?>
    </div>
</div>


<div class="text-right">
    <div class="sum_sec">
        <?php $form = $this->beginWidget('CActiveForm', array(
            'id' => 'sales-invoice-form',
            'enableClientValidation' => true,
            'clientOptions' => array('validateOnChange' => true),
            'enableAjaxValidation' => false,
        )); ?>

        <div class="row">
            <div class="col-md-3">Total Amount:</div>
            <div class="col-md-3"><b id="amount_val"><?php echo  Yii::app()->controller->money_format_inr($model->amount, 2); ?></b></div>
            <div class="col-md-6"> <input class="form-control" name="SalesInvoice[amount]" id="SalesInvoice_amount" type="hidden" value="<?php echo $model->amount; ?>"></div>
        </div>
        <div class="row mt">
            <div class="col-md-3">CGST:</div>
            <div class="col-md-3"> <?php echo $form->textField($model, 'cgst_p', array('class' => 'form-control tax_items')); ?></div>
            <div class="col-md-3"> <b id="cgst_amount"><?php echo isset($model->cgst_amount) ?  Yii::app()->controller->money_format_inr($model->cgst_amount, 2) : '0.00'; ?></b><input class="form-control" name="SalesInvoice[cgst_amount]" id="SalesInvoice_cgst_amount" type="hidden"></div>
        </div>
        <div class="row mt">
            <div class="col-md-3">SGST:</div>
            <div class="col-md-3"> <?php echo $form->textField($model, 'sgst_p', array('class' => 'form-control tax_items')); ?></div>
            <div class="col-md-3"><b id="sgst_amount"><?php echo  isset($model->sgst_amount) ?  Yii::app()->controller->money_format_inr($model->sgst_amount, 2) : '0.00'; ?></b><input class="form-control" name="SalesInvoice[sgst_amount]" id="SalesInvoice_sgst_amount" type="hidden"></div>
        </div>
        <div class="row mt">
            <div class="col-md-3">IGST:</div>
            <div class="col-md-3"> <?php echo $form->textField($model, 'igst_p', array('class' => 'form-control tax_items')); ?></div>
            <div class="col-md-3"><b id="igst_amount"><?php echo isset($model->igst_amount) ?  Yii::app()->controller->money_format_inr($model->igst_amount, 2) : '0.00'; ?></b> <input class="form-control" name="SalesInvoice[igst_amount]" id="SalesInvoice_igst_amount" type="hidden"></div>
        </div>
        <div class="row mt">
            <div class="col-md-3">Cess:</div>
            <div class="col-md-3"> <?php echo $form->textField($model, 'cess_p', array('class' => 'form-control tax_items')); ?></div>
            <div class="col-md-3"><b id="cess_amount"><?php echo isset($model->cess_amount) ?  Yii::app()->controller->money_format_inr($model->cess_amount, 2) : '0.00'; ?></b> <input class="form-control" name="SalesInvoice[cess_amount]" id="SalesInvoice_cess_amount" type="hidden" value="<?php echo $model->cess_amount; ?>"></div>
        </div>
        <div class="row mt">
            <div class="col-md-3">Round Off:</div>
            <div class="col-md-3"> <?php echo $form->textField($model, 'round_off', array('class' => 'form-control')); ?></div>
        </div>
        <div class="row mt">
            <div class="col-md-3">Grand Total:</div>
            <div class="col-md-3"><b id="total_amount_val"><?php echo  $model->getTotal($model); ?></b></div>
            <div class="col-md-3"><input name="SalesInvoice[total_amount]" id="SalesInvoice_total_amount" value="<?php echo $model->getTotal($model); ?>" type="hidden"></div>

        </div>
        <div class="row buttons">
            <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-info')); ?>
        </div>
        <?php $this->endWidget(); ?>
    </div>
</div>