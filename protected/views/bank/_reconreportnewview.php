<?php
if ($index == 0) { ?>
    <thead class="entry-table">
        <tr width="100%">
            <th width="2%">Sl No.</th>
            <th width="13%">Payment Name</th>
            <th width="11%">Company</th>
            <th width="10%">Bill No/Transaction head</th>
            <th width="8%">Vendors/Subcontractors</th>
            <th width="6%">Withdrawal</th>
            <th width="6%">Deposit</th>
            <th width="6%">Bank</th>
            <th width="9%">Cheque Number</th>
            <th width="9%">Description</th>
            <th width="6%">Payment Date</th>
            <th width="6%">Created Date</th>
            <th width="8%">Reconciliation Date</th>
        <tr class="grandtotal">
            <th></th>
            <th colspan="4" class="text-right">Total</th>

            <th class="text-right">
                <?php echo Controller::money_format_inr($paymentamount, 2); ?>
            </th>
            <th class="text-right">
                <?php echo Controller::money_format_inr($receiptamount, 2); ?>
            </th>
            <th colspan="6"></th>

        </tr>
        </tr>
    </thead>
<?php } ?>
<tbody>
    <tr>
        <td>
            <?php echo $index + 1; ?>
        </td>
        <td>
            <?php echo $data->reconciliation_payment; ?>
        </td>
        <td>
            <?php
            $company = Company::model()->findByPk($data->company_id);
            echo $company->name;
            ?>
        </td>
        <td>
            <?php
            if ($data->reconciliation_table == 'jp_dailyvendors') {

            } else if ($data->reconciliation_table == 'jp_dailyexpense') {
                $data->reconciliation_parentid;
                $value = Dailyexpense::model()->findByPk($data->reconciliation_parentid);
                if ($value['exp_type_id'] != '') {
                    $value_data = Companyexpensetype::model()->findByPk($value['exp_type_id']);
                    echo $value_data['name'];
                } else {
                    $value_data = Deposit::model()->findByPk($value['expensehead_id']);
                    echo $value_data['deposit_name'];
                }
            } else if ($data->reconciliation_table == 'jp_expenses') {
                $value = Expenses::model()->findByPk($data->reconciliation_parentid);
                if ($value['bill_id'] != '') {
                    $value_data = Bills::model()->findByPk($value['bill_id']);
                    echo $value_data['bill_number'] . ' / ';
                }
                if ($value['exptype'] != '') {
                    $value_data = ExpenseType::model()->findByPk($value['exptype']);
                    echo $value_data['type_name'];
                }
            }
            ?>
        </td>
        <td>
            <?php
            if ($data->reconciliation_table == 'jp_dailyvendors') {
                $value = Dailyvendors::model()->findByPk($data->reconciliation_parentid);
                if ($value['vendor_id'] != '') {
                    $value_data = Vendors::model()->findByPk($value['vendor_id']);
                    echo $value_data['name'];
                }
            } else if ($data->reconciliation_table == 'jp_expenses') {
                $value = Expenses::model()->findByPk($data->reconciliation_parentid);
                if ($value['vendor_id'] != '') {
                    $value_data = Vendors::model()->findByPk($value['vendor_id']);
                    echo $value_data['name'];
                } else if ($value['subcontractor_id'] != '') {
                    $value1 = SubcontractorPayment::model()->findByPk($value['subcontractor_id']);
                    if (!empty($value1)) {
                        $datasub = Subcontractor::Model()->findByPk($value1["subcontractor_id"]);
                        echo $datasub["subcontractor_name"];
                    }

                }
            }
            ?>
        </td>
        <td class="text-right">
            <?php
            $paymentType = $data->reconciliation_payment;
            if (strpos($paymentType, 'Payment') !== false) {
                echo Controller::money_format_inr($data->reconciliation_amount, 2);
            }
            ?>
        </td>
        <td class="text-right">
            <?php
            if (strpos($paymentType, 'Receipt') !== false) {
                echo Controller::money_format_inr($data->reconciliation_amount, 2);
            }
            ?>
        </td>
        <td>
            <?php
            $bank = Bank::model()->findByPk($data->reconciliation_bank);
            echo $bank->bank_name;
            ?>
        </td>
        <td>
            <?php echo $data->reconciliation_chequeno; ?>
        </td>
        <td>
            <?php
            if ($data->reconciliation_table == 'jp_dailyvendors') {
                $value = Dailyvendors::model()->findByPk($data->reconciliation_parentid);
                echo $value['description'];
            } else if ($data->reconciliation_table == 'jp_dailyexpense') {
                $value = Dailyexpense::model()->findByPk($data->reconciliation_parentid);
                echo $value['description'];
            } else if ($data->reconciliation_table == 'jp_expenses') {
                $value = Expenses::model()->findByPk($data->reconciliation_parentid);
                echo $value['description'];
            }
            ?>
        </td>
        <td>
            <?php echo date("d-m-Y", strtotime($data->reconciliation_paymentdate)); ?>
        </td>
        <td>
            <?php echo date("d-m-Y", strtotime($data->created_date)); ?>
        </td>
        <?php if (Yii::app()->user->role == 1) {
            ?>
            <td class="editabledate" data-id="<?php echo $index; ?>">
                <?php echo date("d-m-Y", strtotime($data->reconciliation_date)); ?>
            </td>
            <input type="hidden" name="reconciliationId<?php echo $index; ?>" id="reconciliationId<?php echo $index; ?>"
                value="<?php echo $data->reconciliation_id; ?>" />
        <?php } ?>
    </tr>
</tbody>