<?php

/**
 * This is the model class for table "tc_users".
*/
class UserRecovery extends CFormModel {

    public $myemail_or_username; //for forgot password

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.

        return array(
            array('myemail_or_username', 'required'),
//            array('myemail_or_username', 'either_one', 'on' => 'pwdrecovery'),
            //array('myusername, myemail', 'either_one', 'on' => 'pwdrecovery'),
        );
    }

    //Custom validation functions

    public function either_one($attribute_name, $params) {
        if (empty($this->myusername) && empty($this->myemail)) {
            $this->addError($attribute_name, 'Enter either username or email');
            return false;
        }
    }
    //End of custome validation functions
    
public function attributeLabels() {
        return array(
            'myusername' => 'Username',
            'myemail' => 'Email id',
        );
    }    



}