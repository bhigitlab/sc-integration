<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/tablestyles.css" />
<link rel="stylesheet" type="text/css" href="uploads/new_template/css/newstyle.css" />
<style>
    table {
        border-collapse: collapse;
        width: 100%;
    }

    .text-right {
        text-align: right
    }

    .details,
    .info-table {
        border: 1px solid #212121;
        margin-top: 20px;
    }

    .details td,
    .info-table td,
    .info-table th {
        padding: 6px 8px;
    }

    .info-table {
        margin-bottom: 10px;
    }

    .text-center {
        text-align: center;
    }

    .img-hold {
        width: 10%;
    }

    .companyhead {
        font-size: 18px;
        font-weight: bold;
        margin-top: 0px;
        color: #789AD1;
    }
</style>
<?php
$company_address = Company::model()->findBypk(($company_id) ? $company_id : Yii::app()->user->company_id);
?>
<br><br>
<h4 class="text-center">Previous Purchase</h4>
<table border="1" class="table table-bordered table-striped" style="margin:0px 30px">
    <thead>
        <tr>
            <th>Sl No.</th>
            <th>Bill No</th>
            <th>Purchase No</th>
            <th>Bill date</th>
            <th>Vendor</th>
            <th>Item</th>
            <th>Quantity</th>
            <th>Rate</th>
            <th>Amount</th>
            <th>Total Amount</th>

        </tr>
    </thead>
    <tbody>
        <?php
        $tblpx = Yii::app()->db->tablePrefix;
        foreach ($result as $key => $data) {
        ?>
            <tr>
                <td><?php echo ($key + 1); ?></td>
                <td><?php echo $data['bill_number']; ?></td>
                <td><?php echo $data['purchase_no']; ?></td>
                <td><?php echo date('Y-m-d', strtotime($data['bill_date'])); ?></td>
                <td>
                    <?php
                    $vendor = Vendors::model()->findByPk($data['vendor_id']);
                    echo $vendor['name']; ?>
                </td>
                <td>
                    <?php
                    if ($data['category_id'] != NULL) {
                        $sql = "SELECT id, cat_id, brand_id, specification, unit "
                                . " FROM {$tblpx}specification WHERE id=" . $data['category_id'] ;
                        $specification  = Yii::app()->db->createCommand($sql)->queryRow();

                        $brand = '';
                        if ($specification['brand_id'] != NULL) {
                            $sql = "SELECT brand_name FROM {$tblpx}brand "
                                    . " WHERE id=" . $specification['brand_id'];
                            $brand_details = Yii::app()->db->createCommand($sql)->queryRow();
                            $brand = '-' . ucwords($brand_details['brand_name']);
                        }

                        $sql = "SELECT category_name FROM {$tblpx}purchase_category "
                                . " WHERE id=" . $specification['cat_id'];
                        $category  = Yii::app()->db->createCommand($sql)->queryRow();
                        echo ucwords($category['category_name']) . $brand . '-' . ucwords($specification['specification']);
                    } else {
                    }
                    ?>
                </td>
                <td style="text-align: right"><?php echo $data['billitem_quantity']; ?></td>
                <td style="text-align: right"><?php echo Controller::money_format_inr($data['billitem_rate'], 2); ?></td>
                <td style="text-align: right"><?php echo Controller::money_format_inr($data['billitem_amount'], 2); ?></td>
                <td style="text-align: right"><?php echo Controller::money_format_inr(($data['billitem_amount'] + $data['billitem_taxamount']) - $data['billitem_discountamount'], 2); ?></td>
            </tr>
        <?php } ?>
    </tbody>
</table>