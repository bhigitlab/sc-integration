ALTER TABLE jp_purchase_return ADD COLUMN remarks TEXT AFTER return_status;
ALTER TABLE jp_purchase_return ADD COLUMN vendor_id INT AFTER remarks;