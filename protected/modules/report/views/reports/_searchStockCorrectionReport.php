<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.min.js"></script>
<script>

    $(function () {
        $("#date_from").datepicker({
            dateFormat: 'dd-mm-yy'
        });

        $("#date_till").datepicker({
            dateFormat: 'dd-mm-yy'
        });

        $("#date_from").change(function () {
            $("#date_till").datepicker('option', 'minDate', $(this).val());
        });

        $("#date_till").change(function () {
            $("#date_from").datepicker('option', 'maxDate', $(this).val());
        });

        $('#reset').click(function () {
            location.href = '<?php echo $this->createUrl('stockCorrectionReport'); ?>';
        })
    });

    $(document).ready(function () {
        $(".select_box").select2();

    });
</script>

<div class="page_filter search-form custom-form-style">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'action' => Yii::app()->createUrl($this->route),
        'method' => 'get',
    ));
    ?>
    <div class="search-content">
        <div class="row">
            <div class="form-group col-xs-12 col-sm-3 col-md-2 ">
                <label for="warehouse">Warehouse</label>
                <?php
                if (yii::app()->user->role == 5) {
                    $warehouse_data = Controller::getCategoryOptions($type = 1);
                    $data = CHtml::listData(Controller::getCategoryOptions($type = 1), 'id', 'text', 'group');
                } else {
                    $warehouse_data = Controller::getCategoryOptions();
                    $data = CHtml::listData(Controller::getCategoryOptions(), 'id', 'text', 'group');
                }

                if (!empty($_GET['warehouse_id'])) {
                    $warehouse = $_GET['warehouse_id'];
                } else {
                    if (count($warehouse_data) == 1) {
                        $warehouse = $warehouse_data[0]['id'];
                    } else {
                        $warehouse = '';
                    }
                }

                if (!empty($_GET['item_id'])) {
                    $item_id = $_GET['item_id'];
                } else {
                    $item_id = '';
                }
                echo CHtml::dropDownList(
                    'warehouse_id',
                    $warehouse,
                    $data,
                    array('empty' => 'Choose Warehouse ', 'id' => 'warehouse', 'class' => ' select_box inputs target warehouse warehouse-from form-control')
                );
                ?>
            </div>


            <div class="form-group col-xs-12 col-sm-3 col-md-2 ">
                <label for="item">Item</label>
                <select id="item_id" class="select_box form-control" name="item_id">
                    <option value="">Select Item</option>
                    <?php
                    if (!empty($specification)) {
                        foreach ($specification as $key => $value) {
                            ?>
                            <option value="<?php echo $value['id'] ?>" <?php echo ($value['id'] == $item_id) ? 'selected' : ''; ?>>
                                <?php echo $value['data']; ?>
                            </option>
                        <?php }
                    }
                    ?>
                </select>
            </div>
            <div class="form-group col-xs-12 col-sm-3 col-md-2 ">
                <label for="date">From</label>
                <?php
                if (!isset($_GET['date_from']) || $_GET['date_from'] == '') {
                    $datefrom = "";
                } else {
                    $datefrom = $_GET['date_from'];
                }
                ?>
                <?php echo CHtml::textField('date_from', $datefrom, array("id" => "date_from", "placeholder" => "From", "readonly" => true, "class" => "form-control ")); ?>
            </div>
            <div class="form-group col-xs-12 col-sm-3 col-md-2 ">
                <label for="to">To</label>
                <?php
                if (!isset($_GET['date_to']) || $_GET['date_to'] == '') {

                    $date_to = '';
                } else {
                    $date_to = $_GET['date_to'];
                }
                ?>
                <?php echo CHtml::textField('date_to', $date_to, array("id" => "date_till", "placeholder" => "To", "readonly" => true, "class" => "form-control")); ?>
            </div>

            <?php
            if (!isset($_GET['rate']) || $_GET['rate'] == '') {

                $rate = '';
            } else {
                $rate = $_GET['rate'];
            }
            ?>

            <div class="form-group col-xs-12 col-sm-3 col-md-2 text-sm-left text-right">
                <label class="d-sm-block d-none">&nbsp;</label>
                <div>
                    <?php
                    echo CHtml::hiddenField('rate', $rate, array('id' => 'hiddenInput'));

                    echo CHtml::submitButton('Go', array('id' => 'usersearchbtn', 'class' => 'btn btn-sm btn-primary', 'style' => 'margin-left: 25px;'));

                    echo CHtml::ResetButton('reset', array('style' => 'margin-left: 10px;', 'class' => ' btn btn-sm btn-default', 'id' => 'reset'));
                    ?>
                </div>
            </div>
        </div>
    </div>
    <?php $this->endWidget(); ?>
</div>