ALTER TABLE `jp_project_template` ADD `template_format_invoice` BLOB NULL DEFAULT NULL AFTER `template_format`;
ALTER TABLE `jp_company` ADD `logo` VARCHAR(500) NULL DEFAULT NULL AFTER `created_date`;
ALTER TABLE `jp_project_template_revision` ADD `invoice_template_version` BLOB NULL DEFAULT NULL AFTER `template_version`;
ALTER TABLE `jp_project_template_revision` CHANGE `template_version` `template_version` BLOB NULL;
ALTER TABLE `jp_project_template` ADD `company_id` INT NULL DEFAULT NULL AFTER `template_format_invoice`;
