<?php
if(Yii::app()->user->role !=1){
  $warehouse = Warehouse::model()->findAll();
  //$warehouse = Warehouse::model()->findAll(array('condition' => 'FIND_IN_SET('.Yii::app()->user->id.', assigned_to)'));
}else{
  $warehouse = Warehouse::model()->findAll();
}
?>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.min.js"></script>
<div class="search-form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'action' => Yii::app()->createUrl($this->route),
        'method' => 'get',
    ));
    ?>
    <!--    <label for="Purchase_Filter_By">Filter  By</label>		-->
    <select style="padding: 2.5px 0px;" id="warehouse_id" class="select_box" name="warehouse_id" class="sel">
        <option value="">Select warehouse</option>
        <?php
        if (!empty($warehouse)) {
            foreach ($warehouse as $key => $value) {
                ?>
                <option value="<?php echo $value['warehouse_id'] ?>" <?php echo ($value['warehouse_id'] == $model->warehousestock_warehouseid) ? 'selected' : ''; ?>><?php echo $value['warehouse_name']; ?></option>
    <?php }
} ?>
    </select>
    <select style="padding: 2.5px 0px;width:25%" id="item_id" class="select_box" name="item_id" class="sel">
        <option value="">Select Item</option>
        <?php
        if (!empty($specification)) {
            foreach ($specification as $key => $value) {
                ?>
                <option value="<?php echo $value['id'] ?>" <?php echo ($value['id'] == $model->warehousestock_itemid) ? 'selected' : ''; ?>><?php echo $value['data']; ?></option>
    <?php }
} ?>
    </select>

    <?php
    if (!isset($_GET['date_from']) || $_GET['date_from'] == '') {
        // $datefrom = date("Y-m-") . "01";
        $datefrom =  "01-" . "01" . date("-Y");
    } else {
        $datefrom = $_GET['date_from'];
    }
    ?>

    <label>Date From :</label>
    <?php echo CHtml::textField('date_from', $datefrom, array("id" => "date_from", "readonly" => true)); ?>
    <?php //echo CHtml::image("images/calendar-month.png", "calendar", array("id" => "c_button3", "class" => "pointer", "style" => "cursor:pointer;width:25px;"));
    ?>
    <?php
    /*$this->widget('application.extensions.calendar.SCalendar', array(
        'inputField' => 'date_from',
        'button' => 'c_button3',
        'ifFormat' => '%Y-%m-%d',
    ));*/
    ?>




    <?php
    if (!isset($_GET['date_to']) || $_GET['date_to'] == '') {
        $date_to = date("d-m-Y");
    } else {
        $date_to = $_GET['date_to'];
    }
    ?>
    &nbsp;
    <label>Date To :</label>
    <?php echo CHtml::textField('date_to', $date_to, array("id" => "date_till", "readonly" => true)); ?>
    <?php //echo CHtml::image("images/calendar-month.png", "calendar", array("id" => "c_button2", "class" => "pointer", "style" => "cursor:pointer;width:25px;"));
    ?>
    <?php
    /*$this->widget('application.extensions.calendar.SCalendar', array(
        'inputField' => 'date_till',
        'button' => 'c_button2',
        'ifFormat' => '%Y-%m-%d',
    ));*/
    ?>

    <input name="yt0" value="Go" type="submit" class="btn-search">
    <input id="reset" name="yt1" value="Clear" type="reset" class="btn-search">

<?php $this->endWidget(); ?>
</div>
<script>
    $('#reset').click(function () {
        location.href = '<?php echo $this->createUrl('index'); ?>';
    })
    $(function () {
        $( "#date_from" ).datepicker({dateFormat: 'dd-mm-yy'});

         $( "#date_till" ).datepicker({dateFormat: 'dd-mm-yy'});

            $("#date_from").change(function() {
                $("#date_till").datepicker('option', 'minDate', $(this).val());
             });

            $("#date_till").change(function() {
                   $("#date_from").datepicker('option', 'maxDate', $(this).val());
            });


    });

    $(document).ready(function () {
        $(".select_box").select2();

    });

</script>
<style>
    .search-form{
        background-color: #fafafa;
        padding: 10px;
        box-shadow: 0px 0px 2px 0px rgba(0,0,0,0.1);
        margin-bottom: 10px;
    }
</style>
