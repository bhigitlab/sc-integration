<?php
$tblpx = Yii::app()->db->tablePrefix;
$user = Users::model()->findByPk(Yii::app()->user->id);
$arrVal = explode(',', $user->company_id);
$newQuery = "";
$newQuery1 = "";
$newQuery2 = "";
$newQuery3 = "";
$newQuery4 = "";
foreach ($arrVal as $arr) {
    if ($newQuery)
        $newQuery .= ' OR';
    if ($newQuery1)
        $newQuery1 .= ' OR';
    if ($newQuery2)
        $newQuery2 .= ' OR';
    if ($newQuery3)
        $newQuery3 .= ' OR';
    if ($newQuery4)
        $newQuery4 .= ' OR';
    $newQuery .= " FIND_IN_SET('" . $arr . "', company_id)";
    $newQuery1 .= " FIND_IN_SET('" . $arr . "', id)";
    $newQuery2 .= " FIND_IN_SET('" . $arr . "', b.company_id)";
    $newQuery3 .= " FIND_IN_SET('" . $arr . "', i.company_id)";
    $newQuery4 .= " FIND_IN_SET('" . $arr . "', " . $tblpx . "purchase_return.company_id)";
}
$billsData="";
$invoicesData="";
$purchasereturnData="";
$bill_id = isset($_REQUEST['bill_id'])?$_REQUEST['bill_id']:"";
echo CHtml::hiddenField('',$bill_id,array('class'=>'request_bill_id'));

?>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.js"></script>

<style type="text/css">
    .select2-selection.select2-selection--single {
        min-width: 100px;
    }

    *:focus {
        border: 1px solid #333;
        box-shadow: 0 0 6px 1px rgba(0, 0, 0, 0.25);
    }

    .btn.addentries:focus,
    .btn.addentries:hover {
        border: 1px solid transparent;
        box-shadow: none;
    }

    .avoid_clicks {
        pointer-events: none;
    }

    .amntblock,
    .expvalue {
        display: none;
    }


    .deleteclass {
        background-color: #efca92;
    }

    .filter_elem.links_hold {
        margin-top: 6px;
    }
</style>
<div class="container" id="expense">
   <div class="loader-overlay" id="loading" >
                <i class="fa fa-spinner loader"></i>
    </div>
   
    <div class="expenses-heading">
        <div class="clearfix">
            <?php
            if (isset(Yii::app()->user->role) && (in_array('/expenses/expensescreate', Yii::app()->user->menuauthlist))) {
            ?>
                <button type="button" class="btn btn-info pull-right addentries collapsed" data-toggle="collapse" data-target="#daybookform"></button>
            <?php } ?>
            <h3>DAYBOOK ENTRIES</h3>
        </div>
        
        
    </div>
    <!-- <div class="daybook form"> -->
        
        
        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'expenses-form',
            'enableAjaxValidation' => false,
             'htmlOptions' => array(
                    'class' => 'daybook form', // add your classes here
              ),
        ));
        ?>
        <!--ui changes -->
        <div class="clearfix">
            <div class="datepicker">
                <div class="page_filter clearfix filter-wrapper">
                    
                     <?php
                    $tblpx = Yii::app()->db->tablePrefix;
                    $expenseData = Yii::app()->db->createCommand("select MAX(date) as entry_date FROM " . $tblpx . "expenses")->queryRow();
                    ?>
                      <!-- removed pull-right and added ml-auto -->
                    <div class="ml-auto">
                        <span id="lastentry">Last Entry date : <span id="entrydate"><?php echo (isset($lastdate['date']) ? date("d-M-Y", strtotime($lastdate['date'])) : ''); //echo date("d-m-Y", strtotime($expenseData["entry_date"]));   
                    ?></span>
                    </div>
                </div>
            </div>
        </div>
        <!--ui changes ends-->
        

        <div id="errormessage"></div>
        <div class="entries-wrapper collapse" id="daybookform">
            <div class="row">
                <div class="col-xs-12">
                    <div class="heading-title">Transaction Details</div>
                    <div class="dotted-line"></div>
                </div>
            </div>
            <div class="row">
                
                <!-- <div id="billloading"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/ajax-loader.gif"></div> -->
                <!-- <div class="row"> -->
                    <div class="form-group col-xs-12 col-md-3">
                        
                            <label for="project">Company</label>
                            <?php
                            echo $form->dropDownList($model, 'company_id', CHtml::listData(Company::model()->findAll(array(
                                                'select' => array('id, name'),
                                                'order' => 'name ASC',
                                                'condition' => '(' . $newQuery1 . ')',
                                                'distinct' => true,
                                            )), 'id', 'name'), array('class' => 'form-control js-example-basic-single', 'empty' => '-Select Company-', 'style' => 'width:100%'));
                            ?>
                       
                    </div>
                    <div class="form-group col-xs-12 col-md-3">
                       
                        <label for="project">Project</label>
                        <?php
                            echo $form->dropDownList($model, 'projectid', CHtml::listData(Projects::model()->findAll(array(
                                                'select' => array('pid, name'),
                                                'order' => 'name ASC',
                                                'condition' => '(' . $newQuery . ')',
                                                'distinct' => true,
                                            )), 'pid', 'name'), array('class' => 'form-control js-example-basic-single', 'empty' => '-Select Project-', 'style' => 'width:100%'));
                        ?>
                       
                    </div>
   
                    <div class="form-group col-xs-12 col-md-3">
                    
                       
                            <label for="billNumber">Bill / Invoice No</label>
                            <select class="form-control js-example-basic-single" name="Expenses[bill_id]" id="Expenses_bill_id" style="width:100%">
                                <option value="0">-Select Bill No/ Invoice No-</option>
                                <?php
                                if (!empty($billsData)) {
                                    ?>
                                    <optgroup label='Bill Number'>
                                        <?php foreach ($billsData as $key => $value) { 
                                            $acc_paidexp_amount = Controller::getAccepetedAmount($value['billid']);
                                            if($acc_paidexp_amount['receipt_amount'] > $acc_paidexp_amount['paidamount']){
                                            ?>
                                            <option data-id="1" value="1,<?php echo $value['billid']; ?>"><?php echo $value['billnumber']; ?></option>
                                        <?php } } ?>
                                    </optgroup>
                                    <?php
                                }
                                if (!empty($invoicesData)) {
                                    ?>
                                    <optgroup label='Invoice Number'>
                                        <?php foreach ($invoicesData as $iData) { ?>
                                            <option data-id="2" value="2,<?php echo $iData["invoice_id"]; ?>"><?php echo $iData["invoice_no"]; ?></option>
                                        <?php } ?>
                                    </optgroup>
                                <?php } ?>

                                <?php if (!empty($purchasereturnData)) {
                                    ?>
                                    <optgroup label='Return Number'>
                                        <?php foreach ($purchasereturnData as $iData) { ?>
                                            <option data-id="3" value="3,<?php echo $iData["return_id"]; ?>"><?php echo $iData["return_number"]; ?></option>
                                        <?php } ?>
                                    </optgroup>
                                <?php } ?>
                            </select>
                       
                    </div>
                   <div class="form-group col-xs-12 col-md-3">
                        
                            <label for="expenseHead">Expense Head</label>
                            <select class="form-control js-example-basic-single" name="Expenses[expensetype_id]" id="Expenses_expensetype_id" style="width:100%">
                                <option value="">-Select Expense Head-</option>
                            </select>
                            <input type="hidden" name="bill_exphead" id="bill_exphead" value="">
                        
                    </div>
                   <div class="form-group col-xs-12 col-md-3">
                        
                            <label for="vendor">Vendor</label>
                            <select class="form-control js-example-basic-single" name="Expenses[vendor_id]" id="Expenses_vendor_id" style="width:100%">
                                <option value="">-Select Vendor-</option>
                            </select>
                            <input type="hidden" name="bill_vendor" id="bill_vendor" value="">
                       
                    </div>
                     <div class="form-group col-xs-12 col-md-2">
                                
                        <label>Entry Date: </label>
                       
                         <?php echo CHtml::activeTextField($model, 'date', array('class' => 'form-control','style'=>'height: 25px',  'autocomplete' => 'off', 'id' => 'Expenses_expense_date', 'value' => date('d-m-Y'))); ?>
                                
                    </div> 
                    <div class="form-group col-xs-12 col-md-3">
                       
                            <label for="expenseType">Transaction Type</label>
                            <select class="form-control js-example-basic-single" 0="1" name="Expenses[expense_type]" id="Expenses_expense_type" style="width:100%">
                                <option value="">-Select Transaction Type-</option>
                                <option value="0">Credit</option>
                                <option value="88">Cheque/Online Payment</option>
                                <option value="89">Cash</option>
                                <option value="103">Petty Cash</option>
                            </select>
                       
                    </div>
                    <div class="form-group col-xs-12 col-md-3 employee_box"  style="display:none;">
                            <label for="bank">Petty Cash By</label>
                            <?php
                            echo $form->dropDownList($model, 'employee_id', CHtml::listData(Users::model()->findAll(array(
                                                'select' => array('userid, concat_ws(" ",first_name,last_name) as first_name'),
                                                'order' => 'first_name ASC',
                                                'condition' => '(' . $newQuery . ') AND user_type NOT IN (1)',
                                                'distinct' => true,
                                            )), 'userid', 'first_name'), array('class' => 'form-control js-example-basic-single', 'empty' => '-Select Employee-', 'style' => 'width:100%'));
                            ?>
                    </div>
                <!-- </div> -->
               
                
            </div>
            <div class="row">
                <div class="col-xs-12">
                      <div class="heading-title">Amount Details</div>
                      <div class="dotted-line"></div>
                </div>
            </div>
            <div class="row d-flex">
              
                
                <div class="col-xs-12 col-md-9">
                    <b class="accepted_detais text-danger"></b>
                    <div class="row child-row">
                       
                        <div class="form-group col-xs-12 col-md-2">
                            <label for="amount" class="d-block">Amount</label>
                            <input type="hidden" class="form-control check" id="accepted_amount" />
                            <input type="hidden"  name="Expenses[accepted_amount]" id="accepted_without_tax"/>
                            <input type="hidden" id="total_accepted_amount"/>
                            <input type="text" class="form-control check" id="txtAmount" name="Expenses[expense_amount]" readonly="true" />
                        </div>
                        
                        <div class="form-group col-md-2 gsts">
                            
                                <label for="sgstp" class="d-block">SGST(%)</label>
                                <div class="input-info">
                                    <input type="text" class="form-control check percentage  w-70" id="txtSgstp" name="Expenses[expense_sgstp]" readonly="true" />
                                    <span class="gstvalue value-label" id="txtSgst1" name="Expenses[expense_sgst]"></span>
                                </div>
                           
                        </div>
                        <div class="amntblock">
                            <div class="form-group">
                                <label for="sgst" class="d-block"></label>
                                <input type="hidden" class="form-control" id="txtSgst" name="Expenses[expense_sgst]" readonly="true" />
                            </div>
                        </div>
                        <div class="form-group col-md-2 gsts">
                           
                            <label for="cgstp" class="d-block">CGST(%)</label>
                            <div class="input-info">
                                <input type="text" class="form-control check percentage  w-70" id="txtCgstp" name="Expenses[expense_cgstp]" readonly="true" />
                                <span class="gstvalue value-label" id="txtCgst1" name="Expenses[expense_cgst]"></span>
                            </div>
                        </div>
                        <div class="amntblock">
                            <div class="form-group">
                                <label for="cgst" class="d-block"></label>
                                <input type="hidden" class="form-control" id="txtCgst" name="Expenses[expense_cgst]" readonly="true" />
                            </div>
                        </div>
                        <div class="form-group col-md-2 gsts">
                           
                                <label for="cgstp" class="d-block">IGST(%)</label>
                            <div class="input-info">
                                <input type="text" class="form-control check percentage  w-70" id="txtIgstp" name="Expenses[expense_igstp]" readonly="true" />
                                <span class="gstvalue value-label" id="txtIgst1" name="Expenses[expense_igst]"></span>
                            </div>
                        </div>

                        <div class="amntblock">
                            <div class="form-group">
                                <label for="cgst" class="d-block"></label>
                                <input type="hidden" class="form-control" id="txtIgst" name="Expenses[expense_igst]" readonly="true" />
                            </div>
                        </div>
                       


                    </div>
                    
                </div>
                <div class="col-xs-12 col-md-3 bottom-flex totalamnt ">
                    <div class="form-group pull-right total-value-wrapper margin-top-18">
                                <div class="input-info margin-bottom-10">
                                    <label for="total" class="d-block"></label>
                                    <input type="hidden" class="form-control" id="txtTotal" name="Expenses[amount]" readonly="true" />
                                </div>
                                
                                <input type="hidden" class="form-control" id="additionalamount" name="additionalamount" readonly="true" />
                                <input type="hidden" class="form-control" id="billamount" name="billamount" readonly="true" />
                                <input type="hidden" class="form-control" id="roundoff" name="roundoff" readonly="true" />
                                <div class="input-info margin-bottom-10" id="txtgstTotal">

                                </div>
                                <div id="txtTotal1" class="input-info margin-bottom-10" name="Expenses[amount]"></div>
                                <div id="txtTdsPaid" class="input-info margin-bottom-10"></div>
                                <input type="hidden" class="form-control" id="Expenses_paidamount" name="Expenses[paidamount]" readonly="true" />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                      <div class="heading-title">Payment Details</div>
                      <div class="dotted-line"></div>
                 </div>
            </div>
            <div class="row daybook-inner ">
               <div class="col-xs-12 col-md-6 ">
                   <div class="row zero-padding select2width">
                        <div class="form-group col-xs-12 col-md-4 ">
                            <label for="purchaseType">Purchase Type</label>
                            <select class="form-control js-example-basic-single" id="txtPurchaseType" name="Expenses[purchase_type]">
                                <option value="">-Select Purchase Type-</option>
                                <option value="1">Credit</option>
                                <option value="2">Full Paid</option>
                                <option value="3">Partially Paid</option>
                            </select>
                        </div> 
                        
                        <div class="form-group col-xs-12 col-md-4 ">
                            <label>Paid / Receipt</label>
                           <input type="text" class="form-control check" id="txtPaid" name="Expenses[paid]" />
                        </div> 
                        <div class="form-group col-xs-12 col-md-4 gsts">
                           <label for="tdsp">TDS(%)</label>
                            <div class="input-info">
                                <input type="text" class="form-control check percentage" id="txtTdsp" name="Expenses[expense_tdsp]" />
                                <span class="gstvalue value-label" id="txtTds1" name="Expenses[expense_tds1]"></span>
                            </div>
                        </div>
                        <div class="form-group col-xs-12 col-md-4">
                            
                            <label for="bank">Bank</label>
                            <?php
                            echo $form->dropDownList($model, 'bank_id', CHtml::listData(Bank::model()->findAll(array(
                                                'select' => array('bank_id, bank_name'),
                                                'order' => 'bank_name ASC',
                                                'condition' => '(' . $newQuery . ')',
                                                'distinct' => true,
                                            )), 'bank_id', 'bank_name'), array('class' => 'form-control js-example-basic-single', 'empty' => '-Select Bank-', 'style' => 'width:100%;'));
                            ?>
                        
                        </div>
                        <div class="form-group col-xs-12 col-md-4">
                            <div class="form-group">
                                <label for="cheque">Cheque No/ Transaction ID</label>
                                <input type="text" class="form-control numbersOnly" id="txtChequeno" name="Expenses[cheque_no]" />
                                <div class="chq_error text-danger"></div>
                            </div>
                        </div>
                        


                    </div>                         
                </div>
                <div class="col-xs-12 col-md-6">
                      <div class="row zero-padding">
                        <div class="form-group col-xs-12">
                          <label>Description</label>
                          <textarea rows="5" cols="50" class="form-control" autocomplete="off" id="txtDescription" name="Expenses[description]"></textarea>
                        </div>
                      </div>
                </div>
               
                <div class="row">
                    <div class="col-md-2 amntblock">
                        <div class="form-group">
                            <label for="tds"></label>
                            <input type="hidden" class="form-control" id="txtTds" name="Expenses[expense_tds]" readonly="true" />
                        </div>
                    </div>
                   
                   
                    <input type="hidden" name="Expenses[txtDaybookId]" value="" id="txtDaybookId" />
                </div>
            </div>
            <div class="row">
                <div class="form-group col-xs-12 text-right">
                    <button type="submit" class="btn btn-primary" id="buttonsubmit">ADD</button>
                    <button type="button" class="btn btn-default" id="btnReset">RESET</button>
                </div>
            </div>
        </div>
        <?php $this->endWidget(); ?>
    <!-- </div> -->
        
    <div id="daybook-entry">
    <?php $this->renderPartial('newlist', array('newmodel' => $newmodel,)); ?>
    </div>
</div>
<?php $dropdownUrl = Yii::app()->createAbsoluteUrl("expenses/dynamicDropdown"); ?>
<?php $vendorUrl = Yii::app()->createAbsoluteUrl("expenses/dynamicVendor"); ?>
<?php $invoiceUrl = Yii::app()->createAbsoluteUrl("expenses/GetInvoiceDetails"); ?>
<?php $returnUrl = Yii::app()->createAbsoluteUrl("expenses/GetPurchaseReturnDetails"); ?>
<?php $getUrl = Yii::app()->createAbsoluteUrl("expenses/getDataByDate"); ?>
<?php $getBillUrl = Yii::app()->createAbsoluteUrl("expenses/GetBillDetails"); ?>
<?php $getDaybook = Yii::app()->createAbsoluteUrl("expenses/getDaybookDetails"); ?>
<?php $bill_invoice = Yii::app()->createAbsoluteUrl("expenses/DynamicBillorInvoice"); ?>
<?php $bill_invoice_return = Yii::app()->createAbsoluteUrl("expenses/DynamicBillorInvoiceorReturn"); ?>
<?php $bill_pending = Yii::app()->createAbsoluteUrl("expenses/PendingBillsDetails"); ?>
<?php $projectUrl = Yii::app()->createAbsoluteUrl("expenses/DynamicProject"); ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-dateFormat/1.0/jquery.dateFormat.js"></script>
<script>
    $(".js-example-basic-single").select2();
    $(function () {
        $("#Expenses_expense_date").datepicker({
            dateFormat: 'dd-mm-yy',
            maxDate: new Date()
        });
    });
    $(function () {
        $(".expense_date").datepicker({
            dateFormat: 'dd-mm-yy',
            maxDate: new Date()
        });
    });
    $(document).ready(function () {
        $('#loading').hide();
        $('#daybookform').on('shown.bs.collapse', function () {
            $('select').first().focus();
        });
    });
</script>
<script type="text/javascript">
    getDynamicBillorInvoiceorPurchaseReturn();
    function getFullData(newDate) {
        $('#loading').show();
        $.ajax({
            url: "<?php echo $getUrl; ?>",
            data: {
                "date": newDate
            },
            type: "POST",
            success: function (data) {
                $("#newlist").html(data);
            }
        });
    }

    function changedate() {
        var cDate = $("#Expenses_expense_date").val();
        getFullData(cDate);
        validateDateSelection(cDate);
    }
    // function pendingBills(){
    //     const urlParams = new URLSearchParams(window.location.search);
    //     const billIdValue = urlParams.get('bill_id');
    //     $('#loading').show();
       
    //     if (urlParams.has('bill_id')) {
    //         $.ajax({
    //             url: "<?php echo $bill_pending; ?>",
    //             type: "GET",
    //             data:{'billId':billIdValue},
    //             success: function (data) {
                   
    //                 $('#loading').hide();
    //                 if(data){
    //                     $("#Expenses_company_id").val(data.company_id).trigger('change');
    //                      getDropdownList(data.project_id,data.bill_id);
    //                       $("#Expenses_projectid").val(data.project_id).trigger('change');
                       
                        
    //                 }
    //                 if (urlParams.has('bill_id')) {
    //                    // $("#Expenses_bill_id").val(billIdValue).trigger('change');
    //                 }
    //             }
    //         });
    //     }
    // }
    //  pendingBills();

    function getDropdownList(project_id, bill_id, exptypeid, edit, expId) {
        $('#loading').show();
        if (edit == undefined || edit == '') {
            edit = '';
        }
        $.ajax({
            url: "<?php echo $dropdownUrl; ?>",
            data: {
                "project": project_id,
                "bill": bill_id,
                "expId": expId
            },
            type: "POST",
            success: function (data) {
               
                var result = JSON.parse(data);
                $("#Expenses_bill_id").html(result["bills"]);
                if (edit == 'edit') {
                    $("#Expenses_projectid").select2("focus");
                } else {
                    $("#Expenses_bill_id").select2("focus");
                    if (project_id == '') {
                        $("#Expenses_expensetype_id").attr("disabled", false);
                        $("#Expenses_vendor_id").attr("readonly", true);
                    }
                }

                if (exptypeid == 0) {
                    $("#Expenses_bill_id").val(0);
                    $("#Expenses_vendor_id").html('<option value="">-Select Vendor-</option>');
                } else {
                    if (bill_id === null) {
                        $("#Expenses_bill_id").val(0);
                    } else {
                       // alert('hi');
                       console.log("bill id"+bill_id);
                        if (result['type'] == 'bill') {
                            $("#Expenses_bill_id").val('1,' + bill_id);
                        } else if (result['type'] == 'invoice') {
                            $("#Expenses_bill_id").val('2,' + bill_id);
                        } else {
                            $("#Expenses_bill_id").val('3,' + bill_id);
                        }
                    }

                }

                $("#Expenses_expensetype_id").html(result["expenses"]);
                if (exptypeid != 0)
                    $("#Expenses_expensetype_id").val(exptypeid);


            },
        });


    }

    function getVendorList(expId, expvendor, edit) {
        $('#loading').show();
        if (edit == undefined) {
            edit = '';
        }
        if (expId === null) {
            expId = 0;
        } else {
            expId = expId;
        }

        $.ajax({
            url: "<?php echo $vendorUrl; ?>",
            data: {
                "expid": expId
            },
            type: "POST",
            success: function (data) {
                $("#Expenses_vendor_id").html(data);
                if (edit == 'edit') {
                    $("#Expenses_company_id").select2("focus");
                } else {
                    $("#Expenses_vendor_id").select2("focus");
                }
                if (expvendor != 0)
                    $("#Expenses_vendor_id").val(expvendor);
            }
        });
    }

    function getDynamicBillorInvoice() {
        $('#loading').show();
        $.ajax({
            url: "<?php echo $bill_invoice; ?>",
            type: "POST",
            success: function (data) {
                $("#Expenses_bill_id").html(data);
            }
        });
    }
    function getDynamicBillorInvoiceorPurchaseReturn() {
         const urlParams = new URLSearchParams(window.location.search);
          const billIdValue = urlParams.get('bill_id');
        $('#loading').show();
        $.ajax({
            url: "<?php echo $bill_invoice_return; ?>",
            type: "POST",
            success: function (data) {
                $('#loading').hide();
                $("#Expenses_bill_id").html(data);
                if (urlParams.has('bill_id')) {
                    const billIdExists = $("#Expenses_bill_id option[value='" + billIdValue + "']").length > 0;

                    if (billIdExists) {
                        $("#Expenses_bill_id").val(billIdValue).trigger('change');
                    }else{
                        $.ajax({
                            url: "<?php echo $bill_pending; ?>",
                            type: "GET",
                            data:{'billId':billIdValue},
                            success: function (data) {
                            
                                $('#loading').hide();
                                if(data){
                                    if (typeof data === "string") {
                                        data = JSON.parse(data);
                                    }
                                   // alert(data["project_id"]);
                                    $("#Expenses_company_id").val(data.company_id).trigger('change');
                                   
                                    getProjectList(data.company_id,'',data.project_id);
                                   
                                    $("#Expenses_projectid").val(data.project_id).trigger('change');
                                    
                                     $("#Expenses_bill_id").val(billIdValue).trigger('change');
                                     getDropdownList(data.project_id, data.billid, 0, '', 0);
                                
                                    
                                }
                                
                            }
                        });

                    }
                   //$("#Expenses_bill_id").val(billIdValue).trigger('change');
                }
            }
        });
    }
    function getProjectList(comId, opt, pro, bank) {
        $('#loading').show();
        $.ajax({
            url: "<?php echo $projectUrl; ?>",
            data: {
                "comId": comId
            },
            type: "POST",
            dataType: 'json',
            success: function (data) {
                if (comId == 0) {
                    $("#Expenses_projectid").val('').trigger('change.select2');
                    $("#Expenses_bank_id").val('').trigger('change.select2');
                } else {
                    $("#Expenses_projectid").html(data.project);
                    $("#Expenses_bank_id").html(data.bank);
                }
                $('#Expenses_bill_id').val('0').trigger('change.select2');
                if (opt != 0)
                    $("#Expenses_bank_id").val(bank).trigger('change.select2');
                $("#Expenses_projectid").val(pro).trigger('change.select2');
            }
        });
    }

    $("#Expenses_company_id").change(function () {
        var comId = $("#Expenses_company_id").val();
        var comId = comId ? comId : 0;
        getProjectList(comId, 0, 0, 0);
    });

    $(document).ready(function () {
        $("#Expenses_projectid").focus();
        $("#Expenses_vendor_id").attr("readonly", true);
        $("#Expenses_bank_id").attr("disabled", true);
        $("#txtChequeno").attr("readonly", true);
        $("#txtPurchaseType").attr("disabled", true);
        $("#txtPaid").attr("readonly", true);
        $("a#next").css("pointer-events", "none").css("color", "#ccccc");
        $("#buttonsubmit").click(function (event) {
            event.preventDefault();
            $("#buttonsubmit").attr('disabled', true);
            var crDate = $("#Expenses_expense_date").val();
            var project = $("#Expenses_projectid").val();
            var company = $("#Expenses_company_id").val();
            var bill = $("#Expenses_bill_id").val();
            var exptype = $("#Expenses_expensetype_id").val();
            var eType = $("#Expenses_expense_type").val();
            var vendor = $("#Expenses_vendor_id").val();
            var totalamount = parseFloat($("#txtTotal").val());
            var desc = $("#txtDescription").val();
            var receipttype = $("#Expenses_payment_type").val();
            var receipt = $("#txtReceipt").val();
            var purchasetype = $("#txtPurchaseType").val();
            var paid = $("#txtPaid").val();
            var bank = $("#Expenses_bank_id").val();
            var amount = $("#txtAmount").val();
            var cheque = $("#txtChequeno").val();
            var newEType = parseInt(eType);
            var newRType = parseInt(receipttype);
            var balance = $("#txtBalance").val();
            var employee_id = $("#Expenses_employee_id").val();

            var typeId = $("#Expenses_bill_id").find(':selected').data('id');
            if (company == "") {
                $("#errormessage").show()
                        .html('<div class="alert alert-danger">Please select a company from the dropdown.</div>')
                        .fadeOut(5000);
                $("#buttonsubmit").attr('disabled', false);
                return false;
            }
            if (project == "") {
                $("#errormessage").show()
                        .html('<div class="alert alert-danger">Please select a project from the dropdown.</div>')
                        .fadeOut(5000);
                $("#buttonsubmit").attr('disabled', false);
                return false;
            }

            if (bill === null || bill == "") {
                $("#errormessage").show()
                        .html('<div class="alert alert-danger">Please select a bill type from the dropdown.</div>')
                        .fadeOut(5000);
                $("#buttonsubmit").attr('disabled', false);
                return false;
            } else {


                if (eType == "") {
                    $("#errormessage").show()
                            .html('<div class="alert alert-danger">Please fill all mandatory fields.</div>')
                            .fadeOut(5000);
                    $("#buttonsubmit").attr('disabled', false);
                    return false;
                }

                if (bill == 0) {
                    if (exptype == "") {
                        // Receipt
                        if (newEType == 88) {
                            if (desc == "" || bank == "" || cheque == "" || amount == "" || eType == "") {
                                $("#errormessage").show()
                                        .html('<div class="alert alert-danger">Please fill all mandatory fields.</div>')
                                        .fadeOut(5000);
                                $("#buttonsubmit").attr('disabled', false);
                                return false;
                            }
                        } else if (newEType == 89) {
                            if (desc == "" || amount == "" || eType == "") {
                                $("#errormessage").show()
                                        .html('<div class="alert alert-danger">Please fill all mandatory fields.</div>')
                                        .fadeOut(5000);
                                $("#buttonsubmit").attr('disabled', false);
                                return false;
                            }
                        } else if (newEType == 103) {
                            if (employee_id == "" || eType == "") {
                                $("#errormessage").show()
                                        .html('<div class="alert alert-danger">Please fill all mandatory fields.</div>')
                                        .fadeOut(5000);
                                $("#buttonsubmit").attr('disabled', false);
                                return false;
                            }
                        } else {
                            if (desc == "" || amount == "" || eType == "") {
                                $("#errormessage").show()
                                        .html('<div class="alert alert-danger">Please fill all mandatory fields.</div>')
                                        .fadeOut(5000);
                                $("#buttonsubmit").attr('disabled', false);
                                return false;
                            }
                        }
                    } else {
                        if (newEType == 88) {
                            if (exptype == "" || vendor == "" || desc == "" || purchasetype == "" || paid == "" || bank == "" || cheque == "" || eType == "" || amount == "") {
                                $("#errormessage").show()
                                        .html('<div class="alert alert-danger">Please fill all mandatory fields.</div>')
                                        .fadeOut(5000);
                                $("#buttonsubmit").attr('disabled', false);
                                return false;
                            }
                        } else if (newEType == 89) {
                            if (exptype == "" || vendor == "" || desc == "" || purchasetype == "" || paid == "" || eType == "" || amount == "") {
                                $("#errormessage").show()
                                        .html('<div class="alert alert-danger">Please fill all mandatory fields.</div>')
                                        .fadeOut(5000);
                                $("#buttonsubmit").attr('disabled', false);
                                return false;
                            }
                        } else {
                            if (exptype == "" || vendor == "" || desc == "" || purchasetype == "" || eType == "" || amount == "") {
                                $("#errormessage").show()
                                        .html('<div class="alert alert-danger">Please fill all mandatory fields.</div>')
                                        .fadeOut(5000);
                                $("#buttonsubmit").attr('disabled', false);
                                return false;
                            }
                        }
                    }

                } else {
                    if (typeId == 1) {
                        if (newEType == 88) {
                            if (vendor == "" || desc == "" || purchasetype == "" || paid == "" || bank == "" || cheque == "" || eType == "") {
                                $("#errormessage").show()
                                        .html('<div class="alert alert-danger">Please fill all mandatory fields.</div>')
                                        .fadeOut(5000);
                                $("#buttonsubmit").attr('disabled', false);
                                return false;
                            }
                        } else if (newEType == 89) {
                            if (vendor == "" || desc == "" || purchasetype == "" || paid == "" || eType == "") {
                                $("#errormessage").show()
                                        .html('<div class="alert alert-danger">Please fill all mandatory fields.</div>')
                                        .fadeOut(5000);
                                $("#buttonsubmit").attr('disabled', false);
                                return false;
                            }
                        } else if (newEType == 103) {
                            if (employee_id == "" || eType == "") {
                                $("#errormessage").show()
                                        .html('<div class="alert alert-danger">Please fill all mandatory fields.</div>')
                                        .fadeOut(5000);
                                $("#buttonsubmit").attr('disabled', false);
                                return false;
                            }
                        } else {
                            if (vendor == "" || desc == "" || purchasetype == "" || eType == "") {
                                $("#errormessage").show()
                                        .html('<div class="alert alert-danger">Please fill all mandatory fields.</div>')
                                        .fadeOut(5000);
                                $("#buttonsubmit").attr('disabled', false);
                                return false;
                            }
                        }


                    } else {
                        if (newEType == 88) {
                            if (desc == "" || purchasetype == "" || paid == "" || bank == "" || cheque == "" || eType == "") {
                                $("#errormessage").show()
                                        .html('<div class="alert alert-danger">Please fill all mandatory fields.</div>')
                                        .fadeOut(5000);
                                $("#buttonsubmit").attr('disabled', false);
                                return false;
                            }
                        } else if (newEType == 89) {
                            if (desc == "" || purchasetype == "" || eType == "") {
                                $("#errormessage").show()
                                        .html('<div class="alert alert-danger">Please fill all mandatory fields.</div>')
                                        .fadeOut(5000);
                                $("#buttonsubmit").attr('disabled', false);
                                return false;
                            }
                        }
                    }


                }

            }

            paid = parseFloat(paid);
            if (purchasetype == 3) {
                if (paid >= totalamount) {
                    $("#errormessage").show()
                            .html('<div class="alert alert-warning">You are seleted the purchase type as Partially Paid.<br/>Paid amount should be less than total amount.</div>')
                            .fadeOut(10000);
                    $("#buttonsubmit").attr('disabled', false);
                    return false;
                } else if (paid <= 0) {
                    $("#errormessage").show()
                            .html('<div class="alert alert-warning">You have selected the purchase type as Partially Paid.<br/>The paid amount must be greater than 0.</div>')
                            .fadeOut(10000);
                    return false;
                }
            }
            $('.loading-overlay').addClass('is-active');
            var dayBookId = $("#txtDaybookId").val();
            var actionUrl;
            if (dayBookId == "") {
                actionUrl = "<?php echo Yii::app()->createAbsoluteUrl("expenses/addDaybook"); ?>";
                localStorage.setItem("action", "add");
            } else {
                actionUrl = "<?php echo Yii::app()->createAbsoluteUrl("expenses/updateDaybook"); ?>";
                localStorage.setItem("action", "update");
            }
            var data = $("#expenses-form").find(":input:not('.no-serialize')").serialize();
            $.ajax({
                type: 'POST',
                dataType: "JSON",
                url: actionUrl,
                data: data,
                success: function (data) {
                    console.log(data);
                    if (data.status == 1) {
                        var message = data.error_message;
                    } else if (data.status == 5) {
                        var message = "Insufficient balance!! Please try again";
                    }
                    if (data.status == 1 || data.status == 5) {
                        $("#errormessage").show()
                                .html('<div class="alert alert-warning"><strong>Failed !</strong> ' + message + '</div>')
                                .fadeOut(20000);
                        $("#buttonsubmit").attr('disabled', false);

                    } else {
                        getDynamicBillorInvoice();
                        var currAction = localStorage.getItem("action");
                        if (currAction == "add") {
                            $("#errormessage").show()
                                    .html('<div class="alert alert-success"><strong>Successfully !</strong> record added. ID is #' + data.id + '</div>')
                                    .fadeOut(4000);
                            var today = new Date();
                            var dd = today.getDate();
                            var mm = today.getMonth() + 1;
                            var yyyy = today.getFullYear();
                            if (dd < 10)
                                dd = '0' + dd
                            if (mm < 10)
                                mm = '0' + mm
                            today = dd + '-' + mm + '-' + yyyy;
                            $.ajax({
                                type: "POST",
                                data: {
                                    date: crDate
                                },
                                url: "<?php echo Yii::app()->createUrl("expenses/getAllData") ?>",
                                success: function (response) {
                                    // document.getElementById("expenses-form").reset();
                                    $("#Expenses_expense_date").val(crDate);
                                    $("#newlist").html(response);
                                    $("#Expenses_projectid").focus();
                                    $("#Expenses_bill_id").attr("disabled", false);
                                    $("#Expenses_expensetype_id").attr("disabled", false);
                                    $("#Expenses_payment_type").attr("disabled", false);
                                    $("#txtReceipt").attr("readonly", false);
                                    $("#buttonsubmit").attr("disabled", false);
                                    $('#Expenses_projectid').val('').trigger('change.select2');
                                    $('#Expenses_company_id').val('').trigger('change.select2');
                                    $('#Expenses_bill_id').val('0').trigger('change.select2');
                                    $('#Expenses_expensetype_id').html('<option value="">-Select Expense Head-</option>');
                                    $('#txtPurchaseType').val("").trigger('change.select2');
                                    $('#Expenses_expense_type').val('').trigger('change.select2');
                                    $('#Expenses_employee_id').val('').trigger('change.select2');
                                    $('#Expenses_bank_id').val('').trigger('change.select2');
                                    $('#Expenses_vendor_id').html('<option value="">-Select Vendor-</option>');

                                    $("#Expenses_vendor_id").attr("readonly", true);
                                    $("#txtAmount").attr("readonly", true);
                                    $("#txtSgstp").attr("readonly", true);
                                    $("#txtCgstp").attr("readonly", true);
                                    $("#txtIgstp").attr("readonly", true);
                                    $("#txtPurchaseType").attr("disabled", true);
                                    $("#txtPaid").attr("readonly", true);
                                    $("#Expenses_bank_id").attr("disabled", true);
                                    $("#txtChequeno").attr("readonly", true);
                                    $("#txtSgst1").text("");
                                    $("#txtCgst1").text("");
                                    $("#txtIgst1").text("");
                                    $("#txtTotal1").html("");
                                    $("#txtgstTotal").text("");
                                }
                            });
                            $("#entrydate").text(today);
                            location.href = "<?php echo $this->createUrl('dailyEntries')?>";
                        } else if (currAction == "update") {
                            $("#txtDaybookId").val('');
                            $("#buttonsubmit").text("ADD");
                            if (data.status == 2) {
                                $("#errormessage").show()
                                        .html('<div class="alert alert-success"><strong>Successfully !</strong> Update request send succesfully. Please wait for approval.#ID:' +
                                                data.id + '</div>')
                                        .fadeOut(5000);
                                $.ajax({
                                    type: "POST",
                                    data: {
                                        date: crDate
                                    },
                                    url: "<?php echo Yii::app()->createUrl("expenses/getAllData") ?>",
                                    success: function (response) {
                                        document.getElementById("expenses-form").reset();
                                        $("#Expenses_expense_date").val(crDate);
                                        $("#newlist").html(response);
                                        $("#Expenses_projectid").focus();
                                        $("#Expenses_bill_id").attr("disabled", false);
                                        $("#Expenses_expensetype_id").attr("disabled", false);
                                        $("#Expenses_payment_type").attr("disabled", false);
                                        $("#txtReceipt").attr("readonly", false);
                                        $("#buttonsubmit").attr("disabled", false);


                                        $('#Expenses_projectid').val('').trigger('change.select2');
                                        $('#Expenses_company_id').val('').trigger('change.select2');
                                        $('#Expenses_bill_id').val('0').trigger('change.select2');
                                        $('#Expenses_expensetype_id').html('<option value="">-Select Expense Head-</option>');
                                        $('#txtPurchaseType').val("").trigger('change.select2');
                                        $('#Expenses_expense_type').val('').trigger('change.select2');
                                        $('#Expenses_employee_id').val('').trigger('change.select2');
                                        $('#Expenses_bank_id').val('').trigger('change.select2');
                                        $('#Expenses_vendor_id').html('<option value="">-Select Vendor-</option>');

                                        $("#Expenses_vendor_id").attr("readonly", true);
                                        $("#txtAmount").attr("readonly", true);
                                        $("#txtSgstp").attr("readonly", true);
                                        $("#txtCgstp").attr("readonly", true);
                                        $("#txtIgstp").attr("readonly", true);
                                        $("#txtPurchaseType").attr("disabled", true);
                                        $("#txtPaid").attr("readonly", true);
                                        $("#Expenses_bank_id").attr("disabled", true);
                                        $("#txtChequeno").attr("readonly", true);
                                        $("#txtSgst1").text("");
                                        $("#txtCgst1").text("");
                                        $("#txtIgst1").text("");
                                        $("#txtTotal1").html("");
                                        $("#txtgstTotal").text("");
                                    }
                                });
                            } else if (data.status == 3) {
                                $("#errormessage").show()
                                        .html('<div class="alert alert-danger"><strong>Failed !</strong> Cannot send update request.</div>')
                                        .fadeOut(5000);
                            } else if (data.status == 4) {
                                $("#errormessage").show()
                                        .html('<div class="alert alert-warning"><strong>Failed !</strong> Cannot send update request. Already have a request for this.</div>')
                                        .fadeOut(5000);
                            } else {
                                $("#errormessage").show()
                                .html('<div class="alert alert-success"><strong>Successfully!</strong> record updated.</div>')
                                .fadeOut(3000, function() {
                                    // This function will be called after fadeOut finishes
                                    window.location.reload();
                                });

                                $(".daybook .collapse").removeClass("in");  // Close the collapsed section
                                $(".addentries").addClass("collapsed");
                                  

                            }
                        }
                        $("#txtTdsPaid").html('<label class="final-section-label w-100p">Amount To Be Paid: <label><span class="total-value-label w-100p">0.00</span>');
                        document.getElementById("expenses-form").reset();
                        $("#Expenses_expense_date").val(crDate);
                        if (data == 2 || data == 3 || data == 4) {

                        } else {
                            $("#newlist").html(data);
                        }
                        $("#Expenses_projectid").focus();
                        $("#Expenses_bill_id").attr("disabled", false);
                        $("#Expenses_expensetype_id").attr("disabled", false);
                        $("#Expenses_payment_type").attr("disabled", false);
                        $("#txtReceipt").attr("readonly", false);
                        $("#buttonsubmit").attr("disabled", false);


                        $('#Expenses_projectid').val('').trigger('change.select2');
                        $('#Expenses_company_id').val('').trigger('change.select2');
                        $('#Expenses_bill_id').val('0').trigger('change.select2');
                        $('#Expenses_expensetype_id').html('<option value="">-Select Expense Head-</option>');
                        $('#txtPurchaseType').val("").trigger('change.select2');
                        $('#Expenses_expense_type').val('').trigger('change.select2');
                        $('#Expenses_employee_id').val('').trigger('change.select2');
                        $('#Expenses_bank_id').val('').trigger('change.select2');
                        $('#Expenses_vendor_id').html('<option value="">-Select Vendor-</option>');

                        $("#Expenses_vendor_id").attr("readonly", true);
                        $("#txtAmount").attr("readonly", true);
                        $("#txtSgstp").attr("readonly", true);
                        $("#txtCgstp").attr("readonly", true);
                        $("#txtIgstp").attr("readonly", true);
                        $("#txtPurchaseType").attr("disabled", true);
                        $("#txtPaid").attr("readonly", true);
                        $("#Expenses_bank_id").attr("disabled", true);
                        $("#txtChequeno").attr("readonly", true);

                        $("#txtSgst1").text("");
                        $("#txtCgst1").text("");
                        $("#txtIgst1").text("");
                        $("#txtTotal1").html("");
                        $("#txtgstTotal").text("");
                    }

                },
                error: function (data) {
                    console.log(data, 'error');
                }
            });
        });
        $("#txtPaid").keyup(function () {
            var totalamount = parseFloat($("#txtTotal").val());
            var purchasetype = $("#txtPurchaseType").val();
            var paid = parseFloat($("#txtPaid").val());
            if (purchasetype == 3) {
                if (paid >= totalamount) {
                    $("#errormessage").show()
                            .html('<div class="alert alert-warning">You have selected the purchase type as Partially Paid.<br/>The paid amount must be less than the total amount.</div>')
                            .fadeOut(10000);
                    $("#buttonsubmit").attr('disabled', true);
                    return false;
                } else if (paid <= 0) {
                    $("#errormessage").show()
                            .html('<div class="alert alert-warning">You have selected the purchase type as Partially Paid.<br/>The paid amount must be greater than 0.</div>')
                            .fadeOut(10000);
                    $("#buttonsubmit").attr('disabled', true);
                    return false;
                } else {

                    $("#buttonsubmit").attr('disabled', false);
                }
            }else{
                $("#buttonsubmit").attr('disabled', false);
            }
            if (paid == "") {
                if (totalamount == "") {
                    getTdsCalculations(0);
                } else {
                    getTdsCalculations(totalamount);
                }
            } else {
                getTdsCalculations(paid);
            }
        });

        $("body").on("click", ".row-daybook", function (e) {

            var reconStatus = $(this).parents("td").attr("data-status");
            if(reconStatus==1){
                alert("Can't update !Reconciled Entry");
                return;
            }
            $('.loading-overlay').addClass('is-active');
            $('html, body').animate({
                scrollTop: 0
            }, 'slow');


            $(".popover").hide();
            var rowId = $(this).attr("id");
            var expenseId = $(this).parents("tr").attr('data-id');
            var expId = $(this).parents("tr").attr('id');
            var expStatus = $(this).parents("tr").attr('data-status');
           
            $("#buttonsubmit").text("UPDATE");
            $("#txtDaybookId").val(expenseId);
            $.ajax({
                url: "<?php echo $getDaybook; ?>",
                data: {
                    "expenseid": expId,
                    "expstatus": expStatus

                },
                type: "POST",
                success: function (data) {                                                                
                    $(".daybook .collapse").addClass("in");
                    $(".addentries").removeClass("collapsed");
                    if ($(".daybook .collapse").css('display') == 'visible') {
                        $(".daybook .collapse").css({
                            "height": "auto"
                        });
                    } else {
                        $(".daybook .collapse").css({
                            "height": ""
                        });
                    }

                    $('.loading-overlay').removeClass('is-active');
                    $(".gstvalue").css({
                        'display': 'inline-block'
                    });
                    var result = JSON.parse(data);
                    $('#Expenses_expensetype_id').val(result["exptypeid"]).trigger('change.select2');
                    $('#Expenses_projectid').val(result["expproject"]).trigger('change.select2');
                    $('#Expenses_company_id').val(result["expcomp"]).trigger('change.select2');
                    $("#Expenses_company_id").select2("focus");
                    $("#Expenses_projectid").val(result["expproject"]);
                    $("#Expenses_vendor_id").attr("disabled", false);
                    var bnk = (result["bank"] != '') ? result["bank"] : 0;
                    getProjectList(result["expcomp"], 1, result["expproject"], bnk);
                    if (!result["expbill"]) {
                        
                        $invReturnId = result["expreturn"];
                        if(result["expinvc"] != ""){
                            $invReturnId = result["expinvc"]; 
                        }
                        getDropdownList(result["expproject"], $invReturnId, result["exptypeid"], 'edit', expId);
                        getVendorList(result["exptypeid"], result["expvendor"], 'edit');
                        $("#txtAmount").val(result["expamount"]);
                        $("#txtSgstp").val(result["expsgstp"]);
                        $("#txtSgst").val(result["expsgst"]);
                        $("#txtCgstp").val(result["expcgstp"]);
                        $("#txtCgst").val(result["expcgst"]);
                        $("#txtIgstp").val(result["expigstp"]);
                        $("#txtIgst").val(result["expigst"]);
                        $("#txtTotal").val(result["exptotal"]);
                        $("#txtDescription").val(result["expdesc"]);
                        $("#Expenses_payment_type").attr("disabled", false);
                        $("#txtReceipt").attr("readonly", false);
                        $("#Expenses_payment_type").val(result["exprtype"]);
                        $("#txtReceipt").val(result["expreceipt"]);
                        $("#txtPurchaseType").val(result["expptype"]);
                        $('#txtPurchaseType').val(result["expptype"]).trigger('change.select2');
                        $("#txtPaid").val(result["exppaid"]);
                        $("#txtPurchaseType").attr("disabled", false);
                        $("#txtSgst1").text(result["expsgst"]);
                        $("#txtCgst1").text(result["expcgst"]);
                        $("#txtIgst1").text(result["expigst"]);
                        $("#txtTotal1").html('<label class="final-section-label w-100p">Total: </label><span class="total-value-label w-100p">' + result["exptotal"] + '</span>');
                        var sgst = parseFloat(result["expsgst"]);
                        var cgst = parseFloat(result["expcgst"]);
                        var igst = parseFloat(result["expigst"]);
                        var gsttotal = sgst + cgst + igst;
                        $("#txtgstTotal").html('<label class="final-section-label w-100p">Tax Total: </label><span class="total-value-label w-100p">' + gsttotal.toFixed(2) + '</span>');

                        if (result["expinvc"] === null && result["expreturn"] === null) {
                            
                            if (result["exptypeid"] === null) {
                               
                                $("#txtAmount").attr("readonly", false);
                                $("#txtSgstp").attr("readonly", false);
                                $("#txtCgstp").attr("readonly", false);
                                $("#txtIgstp").attr("readonly", false);

                                if (result["exprtype"] === null) {
                                    $('#Expenses_expense_type').val(0).trigger('change.select2');
                                } else {
                                    $('#Expenses_expense_type').val(result["exprtype"]).trigger('change.select2');
                                }



                                $("#txtPurchaseType").attr("disabled", true);
                                $("#txtPaid").attr("readonly", true);
                                $("#txtPurchaseType").val("").trigger("change.select2");
                                $("#txtPaid").val("");
                                $("#Expenses_expensetype_id").attr("disabled", false);
                                $("#Expenses_vendor_id").attr("disabled", false);
                                if (result["exprtype"] == 88) {
                                    $("#Expenses_bank_id").attr("disabled", false);
                                    $("#Expenses_bank_id").val(result["bank"]);
                                    $('#Expenses_bank_id').val(result["bank"]).trigger('change.select2');
                                    $("#txtChequeno").attr("readonly", false);
                                    $("#txtChequeno").val(result["chequeno"]);
                                    $(".employee_box").hide();
                                    $("#Expenses_employee_id").attr("disabled", true);
                                } else {
                                    if (result["exprtype"] == 103) {
                                        $(".employee_box").show();
                                        $("#Expenses_employee_id").attr("disabled", false);
                                        $('#Expenses_employee_id').val(result["expemployee"]).trigger('change.select2');
                                        $("#Expenses_bank_id").val("");
                                        $("#txtChequeno").val("");
                                        $("#Expenses_bank_id").attr("disabled", true);
                                        $("#txtChequeno").attr("readonly", true);
                                    } else {
                                        $("#Expenses_bank_id").val("");
                                        $("#txtChequeno").val("");
                                        $("#Expenses_bank_id").attr("disabled", true);
                                        $("#txtChequeno").attr("readonly", true);
                                        $(".employee_box").hide();
                                        $("#Expenses_employee_id").attr("disabled", true);
                                    }
                                }
                            } else {
                                console.log(result);
                                // expense
                                $("#Expenses_expensetype_id").attr("disabled", false);
                                $("#Expenses_vendor_id").attr("disabled", false);
                                $("#txtPurchaseType").attr("disabled", false);
                                $("#txtPaid").attr("readonly", false);
                                $("#txtPurchaseType").val(result["expptype"]).trigger("change.select2");
                                $("#txtPaid").val(result["exppaid"]);
                                $('#Expenses_expense_type').val(result["exptypecc"]).trigger('change.select2');
                                $("#txtAmount").attr("readonly", false);
                                $("#txtSgstp").attr("readonly", false);
                                $("#txtCgstp").attr("readonly", false);
                                $("#txtIgstp").attr("readonly", false);
                                $("#txtTdsp").attr("readonly", false);
                                $("#txtTdsp").val(result["exptdsp"]);
                                $("#txtTds").val(result["exptds"]);
                                $("#txtTds1").text(result["exptds"]);
                                $("#txtTdsPaid").html('<label class="final-section-label w-100p">Amount To Be Paid: </label><span class="total-value-label w-100p">' + result["exppaid"] + '</span>');
                                $("#Expenses_paidamount").val(result["paid"]);
                                if (result["exptypecc"] == 88) {
                                    $("#Expenses_bank_id").attr("disabled", false);
                                    $("#Expenses_bank_id").val(result["bank"]);
                                    $('#Expenses_bank_id').val(result["bank"]).trigger('change.select2');
                                    $("#txtChequeno").attr("readonly", false);
                                    $("#txtChequeno").val(result["chequeno"]);
                                    $(".employee_box").hide();
                                    $("#Expenses_employee_id").attr("disabled", true);
                                } else {
                                    if (result["exptypecc"] == 103) {
                                        $(".employee_box").show();
                                        $("#Expenses_employee_id").attr("disabled", false);
                                        $('#Expenses_employee_id').val(result["expemployee"]).trigger('change.select2');
                                        $("#Expenses_bank_id").val("");
                                        $("#txtChequeno").val("");
                                        $("#Expenses_bank_id").attr("disabled", true);
                                        $("#txtChequeno").attr("readonly", true);
                                    } else {
                                        $("#Expenses_bank_id").val("");
                                        $("#txtChequeno").val("");
                                        $("#Expenses_bank_id").attr("disabled", true);
                                        $("#txtChequeno").attr("readonly", true);
                                        $(".employee_box").hide();
                                        $("#Expenses_employee_id").attr("disabled", true);
                                    }
                                }
                            }

                        } else {

                            
                            // invoice no

                            if (result["expinvc"] != null) {
                                $("#txtAmount").attr("readonly", true);
                                $("#txtSgstp").attr("readonly", true);
                                $("#txtCgstp").attr("readonly", true);
                                $("#txtIgstp").attr("readonly", true);
                                $("#txtPurchaseType").attr("disabled", false);
                                $("#txtPaid").attr("readonly", false);
                                if (result["exprtype"] == 88) {
                                    $("#Expenses_bank_id").attr("disabled", false);
                                    $("#Expenses_bank_id").val(result["bank"]);
                                    $('#Expenses_bank_id').val(result["bank"]).trigger('change.select2');
                                    $("#txtChequeno").attr("readonly", false);
                                    $("#txtChequeno").val(result["chequeno"]);
                                    $(".employee_box").hide();
                                    $("#Expenses_employee_id").attr("disabled", true);
                                } else {
                                    if (result["exprtype"] == 103) {
                                        $(".employee_box").show();
                                        $("#Expenses_employee_id").attr("disabled", false);
                                        $('#Expenses_employee_id').val(result["expemployee"]).trigger('change.select2');
                                        $("#Expenses_bank_id").val("");
                                        $("#txtChequeno").val("");
                                        $("#Expenses_bank_id").attr("disabled", true);
                                        $("#txtChequeno").attr("readonly", true);
                                    } else {
                                        $("#Expenses_bank_id").val("");
                                        $("#txtChequeno").val("");
                                        $("#Expenses_bank_id").attr("disabled", true);
                                        $("#txtChequeno").attr("readonly", true);
                                        $(".employee_box").hide();
                                        $("#Expenses_employee_id").attr("disabled", true);
                                    }
                                }
                                $('#Expenses_expense_type').html('<option value="">-Select Transaction Type-</option><option value="88">Cheque/Online Payment</option><option value="89">Cash</option>');
                                $('#txtPurchaseType').html('<option value="">-Select Purchase Type-</option><option value="2">Full Paid</option><option value="3">Partially Paid</option>');
                                $('#Expenses_expense_type').val(result["exprtype"]).trigger('change.select2');
                                $('#txtPurchaseType').val(result["expptype"]).trigger('change.select2');
                                $("#Expenses_expensetype_id").attr("readonly", true);
                                $("#Expenses_vendor_id").attr("readonly", true);
                            } else {

                                // return
                                getDropdownList(result["expproject"], result["expreturn"], result["exptypeid"], 'edit', expId);
                                $("#txtAmount").attr("readonly", true);
                                $("#txtSgstp").attr("readonly", true);
                                $("#txtCgstp").attr("readonly", true);
                                $("#txtIgstp").attr("readonly", true);
                                $("#txtPurchaseType").attr("disabled", false);
                                $("#txtPaid").attr("readonly", false);
                                if (result["exprtype"] == 88) {
                                    $("#Expenses_bank_id").attr("disabled", false);
                                    $("#Expenses_bank_id").val(result["bank"]);
                                    $('#Expenses_bank_id').val(result["bank"]).trigger('change.select2');
                                    $("#txtChequeno").attr("readonly", false);
                                    $("#txtChequeno").val(result["chequeno"]);
                                    $(".employee_box").hide();
                                    $("#Expenses_employee_id").attr("disabled", true);
                                } else {
                                    if (result["exprtype"] == 103) {
                                        $(".employee_box").show();
                                        $("#Expenses_employee_id").attr("disabled", false);
                                        $('#Expenses_employee_id').val(result["expemployee"]).trigger('change.select2');
                                        $("#Expenses_bank_id").val("");
                                        $("#txtChequeno").val("");
                                        $("#Expenses_bank_id").attr("disabled", true);
                                        $("#txtChequeno").attr("readonly", true);
                                    } else {
                                        $("#Expenses_bank_id").val("");
                                        $("#txtChequeno").val("");
                                        $("#Expenses_bank_id").attr("disabled", true);
                                        $("#txtChequeno").attr("readonly", true);
                                        $(".employee_box").hide();
                                        $("#Expenses_employee_id").attr("disabled", true);
                                    }
                                }
                                $('#Expenses_expense_type').html('<option value="">-Select Transaction Type-</option><option value="107">Credit</option><option value="88">Cheque/Online Payment</option><option value="89">Cash</option><option value="103">Petty Cash</option>');
                                $('#txtPurchaseType').html('<option value="">-Select Purchase Type-</option><option value="2">Full Paid</option><option value="3">Partially Paid</option>');
                                $('#Expenses_expense_type').val(result["exprtype"]).trigger('change.select2');
                                $('#txtPurchaseType').val(result["expptype"]).trigger('change.select2');
                                $("#Expenses_expensetype_id").attr("readonly", true);
                                $("#Expenses_vendor_id").attr("readonly", true);
                            }

                        }

                        if (result["expptype"] == 2) {
                            $("#txtPaid").attr("readonly", true);
                        } else if (result["expptype"] == 3) {
                            $("#txtPaid").attr("readonly", false);
                        } else {
                            $("#txtPaid").attr("readonly", true);
                        }
                        if (result["expreceipt"] !== null) {
                            $("#txtPaid").val(result["expreceipt"]);
                        }

                    } else {
                        
                        getDropdownList(result["expproject"], result["expbill"], result["exptypeid"], 'edit', expId);
                        getVendorList(result["exptypeid"], result["expvendor"], 'edit');
                        $("#bill_exphead").val(result["exptypeid"]);
                        $("#bill_vendor").val(result["expvendor"]);
                        $("#Expenses_expensetype_id").attr("readonly", true);
                        $("#Expenses_vendor_id").attr("readonly", true);
                        $("#Expenses_expense_type").val(result["exptypecc"]);
                        $('#Expenses_expense_type').val(result["exptypecc"]).trigger('change.select2');
                        $("#txtAmount").val(result["expamount"]);
                        $("#txtSgstp").val(result["expsgstp"]);
                        $("#txtSgst").val(result["expsgst"]);
                        $("#txtCgstp").val(result["expcgstp"]);
                        $("#txtCgst").val(result["expcgst"]);
                        $("#txtIgstp").val(result["expigstp"]);
                        $("#txtIgst").val(result["expigst"]);
                        $("#txtTdsp").attr("readonly", false);
                        $("#txtTdsp").val(result["exptdsp"]);
                        $("#txtTds").val(result["exptds"]);
                        $("#txtTds1").text(result["exptds"]);
                        $("#txtTdsPaid").html('<label class="final-section-label w-100p">Amount To Be Paid: </label><span class="total-value-label w-100p">' + result["exppaid"] + '</span>');
                        $("#Expenses_paidamount").val(result["paid"]);

                        $("#txtTotal").val(result["exptotal"]);
                        $("#txtSgst1").text(result["expsgst"]);
                        $("#txtCgst1").text(result["expcgst"]);
                        $("#txtIgst1").text(result["expigst"]);
                        $("#txtTotal1").html('<label class="final-section-label w-100p">Total Amount: </label><span class="total-value-label w-100p">' + result["exptotal"] + '</span>');
                        var sgst = parseFloat(result["expsgst"]);
                        var cgst = parseFloat(result["expcgst"]);
                        var igst = parseFloat(result["expigst"]);
                        var gsttotal = sgst + cgst + igst;
                        $("#txtgstTotal").html('<label class="final-section-label w-100p">Tax Total: </label><span class="total-value-label w-100p">' + gsttotal + '</span>');
                        $("#txtDescription").val(result["expdesc"]);
                        $("#Expenses_payment_type").val("");
                        $("#txtReceipt").val("");
                        $("#Expenses_payment_type").attr("disabled", true);
                        $("#txtReceipt").attr("readonly", true);
                        if (result["exptypecc"] == 0) {
                            $("#txtPurchaseType").attr("disabled", true);
                        } else {
                            $("#txtPurchaseType").attr("disabled", false);
                        }
                        $("#txtPaid").attr("readonly", false);
                        $("#txtPurchaseType").val(result["expptype"]);
                        $('#txtPurchaseType').val(result["expptype"]).trigger('change.select2');
                        $("#txtAmount").attr("readonly", true);
                        $("#txtSgstp").attr("readonly", true);
                        $("#txtSgst").attr("readonly", true);
                        $("#txtCgstp").attr("readonly", true);
                        $("#txtCgst").attr("readonly", true);
                        $("#txtTotal").attr("readonly", true);
                        $("#txtIgstp").attr("readonly", true);
                        if (result["exptypecc"] == 88) {
                            $("#Expenses_bank_id").attr("disabled", false);
                            $("#Expenses_bank_id").val(result["bank"]);
                            $('#Expenses_bank_id').val(result["bank"]).trigger('change.select2');
                            $("#txtChequeno").attr("readonly", false);
                            $("#txtChequeno").val(result["chequeno"]);
                            $(".employee_box").hide();
                            $("#Expenses_employee_id").attr("disabled", true);
                        } else {
                            if (result["exptypecc"] == 103) {
                                $(".employee_box").show();
                                $("#Expenses_employee_id").attr("disabled", false);
                                $('#Expenses_employee_id').val(result["expemployee"]).trigger('change.select2');
                                $("#Expenses_bank_id").val("");
                                $("#txtChequeno").val("");
                                $("#Expenses_bank_id").attr("disabled", true);
                                $("#txtChequeno").attr("readonly", true);
                            } else {
                                $("#Expenses_bank_id").val("");
                                $("#txtChequeno").val("");
                                $("#Expenses_bank_id").attr("disabled", true);
                                $("#txtChequeno").attr("readonly", true);
                                $(".employee_box").hide();
                                $("#Expenses_employee_id").attr("disabled", true);
                            }
                        }
                        if (result["expptype"] == 2) {
                            $("#txtPaid").attr("readonly", true);
                        } else if (result["expptype"] == 3) {
                            $("#txtPaid").attr("readonly", false);
                        } else {
                            $("#txtPaid").attr("readonly", true);
                        }
                        if (result["expreceipt"] !== null) {
                            $("#txtPaid").val(result["expreceipt"]);
                        } else {
                            $("#txtPaid").val(result["exppaid"]);
                        }
                    }

                    $("#Expenses_projectid").select2("focus");
                }
            });
        });



        $("#previous").click(function (e) {
            var cDate = $("#Expenses_expense_date").val();
            cDate = cDate.split("-").reverse().join("-");
            var newDate = new Date(cDate);
            var prDate = new Date(newDate.getFullYear(), newDate.getMonth(), newDate.getDate() - 1);
            var newPrDate = $.format.date(prDate, "dd-MM-yyyy");
            $("#Expenses_expense_date").val(newPrDate);
            getFullData(newPrDate);
            validateDateSelection(newPrDate);
        });
        $("#current").click(function () {
            var cDate = new Date();
            var newDate = $.format.date(cDate, "dd-MM-yyyy");
            $("#Expenses_expense_date").val(newDate);
            getFullData(newDate);
            validateDateSelection(newDate);
        });
        $("#next").click(function () {
            var cDate = $("#Expenses_expense_date").val();
            cDate = cDate.split("-").reverse().join("-");
            var newDate = new Date(cDate);
            var nxtDate = new Date(newDate.getFullYear(), newDate.getMonth(), newDate.getDate() + 1);
            var newNxtDate = $.format.date(nxtDate, "dd-MM-yyyy");
            $("#Expenses_expense_date").val(newNxtDate);
            getFullData(newNxtDate);
            validateDateSelection(newNxtDate);
        });
        $("#Expenses_payment_type").change(function () {
            var receiptType = $("#Expenses_payment_type").val();
            if (receiptType != "") {
                $("#Expenses_bill_id").attr("disabled", true);
                $("#Expenses_expensetype_id").val("");
                $("#Expenses_expense_type").attr("disabled", true);
                $("#Expenses_expense_type").val("");
                $("#txtPurchaseType").val("");
                $("#txtPaid").val("");
                $("#Expenses_expensetype_id").attr("readonly", true);
                $("#txtPurchaseType").attr("disabled", true);
                $("#txtPaid").attr("readonly", true);
            } else {
                $("#Expenses_bill_id").attr("disabled", false);
                $("#Expenses_expensetype_id").attr("disabled", false);
                $("#Expenses_expense_type").attr("disabled", false);
                $("#txtReceipt").val("");
                $("#txtPurchaseType").attr("disabled", false);
                $("#txtPaid").attr("readonly", false);
            }
            if (receiptType == 88) {
                $("#Expenses_bank_id").attr("disabled", false);
                $("#txtChequeno").attr("readonly", false);
            } else {
                $("#Expenses_bank_id").val("");
                $("#txtChequeno").val("");
                $("#Expenses_bank_id").attr("disabled", true);
                $("#txtChequeno").attr("readonly", true);
            }
        });
        $("#Expenses_bill_id").change(function () {
            $('#loading').show();
            var val = $(this).val();
            var typeId = $(this).find(':selected').data('id');
            var url = '';
            var project_id = $('#Expenses_projectid').val();
            if (typeId == 1 || val == 0) {
                url = "<?php echo $getBillUrl; ?>";
                if (val == 0) {
                    $('#Expenses_expense_type').html('<option value="">-Select Transaction Type-</option><option value="0">Credit</option><option value="88">Cheque/Online Payment</option><option value="89">Cash</option><option value="103">Petty Cash</option>');
                    $('#txtPurchaseType').html('<option value="">-Select Transaction Type-</option><option value="2">Full Paid</option><option value="3">Partially Paid</option>');
                } else {
                    $('#Expenses_expense_type').html('<option value="">-Select Transaction Type-</option><option value="0">Credit</option><option value="88">Cheque/Online Payment</option><option value="89">Cash</option><option value="103">Petty Cash</option>');
                    $('#txtPurchaseType').html('<option value="">-Select Transaction Type-</option><option value="1">Credit</option><option value="2">Full Paid</option><option value="3">Partially Paid</option>');
                }

            } else if (typeId == 2) {
                url = "<?php echo $invoiceUrl; ?>";
                $('#Expenses_expense_type').html('<option value="">-Select Transaction Type-</option><option value="88">Cheque/Online Payment</option><option value="89">Cash</option>');
                $('#txtPurchaseType').html('<option value="">-Select Transaction Type-</option><option value="2">Full Paid</option><option value="3">Partially Paid</option>');
            } else {
                url = "<?php echo $returnUrl; ?>";
                $('#Expenses_expense_type').html('<option value="">-Select Transaction Type-</option><option value="107">Credit</option><option value="88">Cheque/Online Payment</option><option value="89">Cash</option><option value="103">Petty Cash</option>');
                $('#txtPurchaseType').html('<option value="">-Select Transaction Type-</option><option value="2">Full Paid</option><option value="3">Partially Paid</option>');
            }
            if (val == 0) {
                // no mandatory

                $("#txtAmount").attr("readonly", false);
                $("#txtSgstp").attr("readonly", false);
                $("#txtSgst").attr("readonly", true);
                $("#txtCgstp").attr("readonly", false);
                $("#txtCgst").attr("readonly", true);
                $("#txtTotal").attr("readonly", true);
                $("#txtIgstp").attr("readonly", false);
                $("#txtAmount").removeClass("avoid_clicks");
                $("#txtSgstp").removeClass("avoid_clicks");
                $("#txtCgstp").removeClass("avoid_clicks");
                $("#txtIgstp").removeClass("avoid_clicks");
                $("#txtAmount").val('');
                $("#txtSgstp").val('');
                $("#txtSgst").val('');
                $("#txtSgst1").text('');
                $("#txtCgstp").val('');
                $("#txtCgst").val('');
                $("#txtCgst1").text('');
                $("#txtTotal").val('');
                $("#txtTotal1").text('');
                $("#txtIgstp").val('');
                $("#txtIgst").val('');
                $("#txtIgst1").text('');
                $("#txtgstTotal").text('');
                $("#additionalamount").val('');
                $("#roundoff").val('');
                //$("#txtAmount").val(result["billamount"]);
                $("#Expenses_expensetype_id").attr("disabled", false);
                $("#Expenses_vendor_id").attr("disabled", false);
                $("#txtPurchaseType").attr("disabled", false);
                $("#txtPaid").attr("disabled", false);
                $("#txtPurchaseType").attr("disabled", true);
                $("#txtPaid").attr("readonly", true);
                if (project_id != '') {
                    getDropdownList(project_id, 0, 0, '', 0);
                }
                $("#Expenses_vendor_id").html('<option value="">-Select Vendor-</option>');
                $.ajax({
                    method: "POST",
                    dataType: "json",
                    url: '<?php echo Yii::app()->createUrl('expenses/ajaxcall'); ?>',
                    success: function (result) {
                        $("#Expenses_expensetype_id").select2("focus");
                    }
                });

            } else {
                // bill/invoice no
                $("#txtAmount").attr("readonly", true);
                $("#txtSgstp").attr("readonly", true);
                $("#txtSgst").attr("readonly", true);
                $("#txtCgstp").attr("readonly", true);
                $("#txtCgst").attr("readonly", true);
                $("#txtTotal").attr("readonly", true);
                $("#txtIgstp").attr("readonly", true);

                $("#txtAmount").addClass("avoid_clicks");
                $("#txtSgstp").addClass("avoid_clicks");
                $("#txtCgstp").addClass("avoid_clicks");
                $("#txtIgstp").addClass("avoid_clicks");

                $("#Expenses_bank_id").val("");
                $("#txtChequeno").val("");
                $("#Expenses_bank_id").attr("disabled", true);
                $("#txtChequeno").attr("readonly", true);
                $("#txtPaid").val("");
                $("#txtTdsPaid").html("");
                $("#additionalamount").val('');
                $("#txtPaid").attr("readonly", true);
                $("#Expenses_bank_id").val("").trigger("change.select2");
                $('#Expenses_expensetype_id').val("").trigger("change.select2");
                $('#Expenses_vendor_id').val("").trigger("change.select2");
                var billId = $("#Expenses_bill_id").val();
                if (typeId == 1) {
                    $.ajax({
                        url: url,
                        data: {
                            "billid": billId
                        },
                        type: "POST",
                        success: function (data) {
                            $(".gstvalue").css({
                                'display': 'inline-block'
                            });
                            var result = JSON.parse(data);
                            $("#Expenses_expensetype_id").attr("readonly", true);
                            $("#Expenses_vendor_id").attr("readonly", true);
                            $("#Expenses_expense_type").attr("disabled", false);
                            $("#Expenses_payment_type").attr("disabled", true);
                            $("#txtReceipt").attr("readonly", true);
                            $("#txtPurchaseType").attr("disabled", false);
                            $("#txtPaid").attr("readonly", false);
                            $("#additionalamount").val(result["additional_amount"]);
                            $("#roundoff").val(result["roundoff"]);
                            //$("#txtAmount").val(result["billamount"]);
                            $("#txtAmount").val(result.amount.toFixedDown(2));
                            $("#txtSgstp").val(result["sgstp"].toFixedDown(2));
                            $("#txtSgst").val(result["sgst"].toFixed(2));
                            $("#txtCgstp").val(result["cgstp"].toFixedDown(2));
                            $("#txtCgst").val(result["cgst"].toFixed(2));
                            $("#txtIgstp").val(result["igstp"].toFixedDown(2));
                            $("#txtIgst").val(result["igst"].toFixed(2));
                            $("#txtTotal").val(result["total"].toFixed(2));
                            $("#Expenses_expense_type").select2("focus");
                            $('#Expenses_expensetype_id').html("<option value=" + result["expensehead_id"] + ">" + result["expense_name"] + "</option>");
                            $('#Expenses_vendor_id').html("<option value=" + result["vendor_id"] + ">" + result["vendor_name"] + "</option>");
                            $("#bill_exphead").val(result["expensehead_id"]);
                            $("#bill_vendor").val(result["vendor_id"]);
                            $("#txtSgst1").html((result["sgst"]).toFixed(2));
                            $("#txtCgst1").html((result["cgst"]).toFixed(2));
                            $("#txtIgst1").html((result["igstp"]).toFixed(2));
                            $("#txtTdsp").val("");
                            $("#txtTdsp").attr("readonly", false);
                            $("#txtTds1").text("");
                            $("#txtTds").val("");
                            $("#txtTdsPaid").html("<label class='final-section-label w-100p'>Amount To Be Paid: </label><span class='total-value-label w-100p'>" + result["total"].toFixed(2) + "</span>");
                            $("#Expenses_paidamount").val(result["total"]);
                            $("#txtTotal1").html("<label class='final-section-label w-100p'>Total: </label><span class='total-value-label w-100p'>" + (result["total"]).toFixed(2) + "</span>");
                            var gsttotal = (parseFloat(result["sgst"]) + parseFloat(result["cgst"]) + parseFloat(result["igst"])).toFixed(2);
                            $("#txtgstTotal").html('<label class="final-section-label w-100p">Total Tax: </label> <span class="total-value-label w-100p">' + gsttotal + '</span>');
                            var project = $('#Expenses_projectid').val();
                            $('#Expenses_projectid').val(result["project_id"]).trigger('change.select2');
                            var company = $('#Expenses_company_id').val();
                            if (company == '') {
                                $('#Expenses_company_id').val(result["company"]).trigger('change.select2');
                            }
                            var accepted_amount = $("#accepted_amount").val();
                            if(accepted_amount !=""){
                                $("#txtSgstp").trigger("blur");
                            }

                        }
                    });
                } else if (typeId == 2) {
                    $('#Expenses_expensetype_id').val("").trigger("change.select2");
                    $('#Expenses_vendor_id').val("").trigger("change.select2");
                    $.ajax({
                        url: url,
                        data: {
                            "billid": billId
                        },
                        type: "POST",
                        success: function (data) {
                            $(".gstvalue").css({
                                'display': 'inline-block'
                            });
                            var result = JSON.parse(data);
                            $("#additionalamount").val(result["additional_amount"]);
                            $("#roundoff").val(result["roundoff"]);
                            //$("#txtAmount").val(result["billamount"]);
                            $("#txtAmount").val(parseFloat(result['amount']).toFixed(2));
                            $("#txtSgstp").val(parseFloat(result["sgstp"]).toFixed(2));
                            $("#txtSgst").val(parseFloat(result["sgst"]).toFixed(2));
                            $("#txtCgstp").val(parseFloat(result["cgstp"]).toFixed(2));
                            $("#txtCgst").val(parseFloat(result["cgst"]).toFixed(2));
                            $("#txtIgstp").val(parseFloat(result["igstp"]).toFixed(2));
                            $("#txtIgst").val(parseFloat(result["igst"]).toFixed(2));
                            $("#txtTotal").val(parseFloat(result["total"]).toFixed(2));
                            $("#Expenses_expense_type").select2("focus");
                            $("#Expenses_expensetype_id").attr("readonly", true);
                            $("#Expenses_vendor_id").attr("readonly", true);
                            $("#txtPurchaseType").attr("disabled", false);
                            $("#txtPaid").attr("readonly", false);
                            $("#txtSgst1").html(parseFloat(result["sgst"]).toFixed(2));
                            $("#txtCgst1").html(parseFloat(result["cgst"]).toFixed(2));
                            $("#txtIgst1").html(parseFloat(result["igstp"]).toFixed(2));
                            $("#txtTdsp").val("");
                            $("#txtTdsp").attr("readonly", true);
                            $("#txtTds1").text("");
                            $("#txtTds").val("");
                            $("#txtTdsPaid").html("");
                            $("#Expenses_paidamount").val("");
                            
                            var gsttotal = (parseFloat(result["sgst"]) + parseFloat(result["cgst"]) + parseFloat(result["igst"])).toFixed(2);

                            var amount_with_tax = parseFloat(result["amount"]) + parseFloat(result["sgst"]) + parseFloat(result["cgst"]) + parseFloat(result["igst"]);
                            
                            
                            $("#txtTotal1").html('<label class="final-section-label w-100p">Total: </label><span class="total-value-label w-100p">' + amount_with_tax.toFixed(2) + '</span>');
                            $("#txtgstTotal").html('<label class="final-section-label w-100p">Total Tax: </label> <span class="total-value-label w-100p"> ' + gsttotal + '</span>');
                            $('#Expenses_projectid').val(result["project_id"]).trigger('change.select2');
                            var company = $('#Expenses_company_id').val();
                            if (company == '') {
                                $('#Expenses_company_id').val(result["company"]).trigger('change.select2');
                            }
                        }
                    });
                } else {
                    $('#Expenses_expensetype_id').val("").trigger("change.select2");
                    $('#Expenses_vendor_id').val("").trigger("change.select2");
                    $.ajax({
                        url: url,
                        data: {
                            "billid": billId
                        },
                        type: "POST",
                        success: function (data) {
                            $(".gstvalue").css({
                                'display': 'inline-block'
                            });
                            var result = JSON.parse(data);
                            $("#Expenses_expensetype_id").attr("readonly", true);
                            $("#Expenses_vendor_id").attr("readonly", true);
                            $("#additionalamount").val(result["additional_amount"]);
                            $("#roundoff").val(result["roundoff"]);
                            //$("#txtAmount").val(result["billamount"]);
                            $("#txtAmount").val(result["amount"].toFixed(2));
                            $("#txtSgstp").val(result["sgstp"].toFixed(2));
                            $("#txtSgst").val(result["sgst"].toFixed(2));
                            $("#txtCgstp").val(result["cgstp"].toFixed(2));
                            $("#txtCgst").val(result["cgst"].toFixed(2));
                            $("#txtIgstp").val(result["igstp"].toFixed(2));
                            $("#txtIgst").val(result["igst"].toFixed(2));
                            $("#txtTotal").val(result["total"].toFixed(2));
                            $("#bill_vendor").val(result["vendor_id"]);
                            $("#Expenses_expense_type").select2("focus");
                            $('#Expenses_expensetype_id').html("<option value=" + result["expensehead_id"] + ">" + result["expense_name"] + "</option>");
                            $('#Expenses_vendor_id').html("<option value=" + result["vendor_id"] + ">" + result["vendor_name"] + "</option>");
                            $("#txtPurchaseType").attr("disabled", false);
                            $("#txtPaid").attr("readonly", false);
                            $("#txtSgst1").html(result["sgst"].toFixed(2));
                            $("#txtCgst1").html(result["cgst"].toFixed(2));
                            $("#txtIgst1").html(result["igstp"].toFixed(2));
                            $("#txtTotal1").html('<label class="final-section-label w-100p" </label>Total: </label><span class="total-value-label w-100p">' + (result["total"]).toFixed(2) + '</span>');
                            var gsttotal = (parseFloat(result["sgst"]) + parseFloat(result["cgst"]) + parseFloat(result["igst"])).toFixed(2);
                            $("#txtgstTotal").html('<label class="final-section-label w-100p">Total Tax: </label><span class="total-value-label w-100p">' +  gsttotal + '</span>');
                            $('#Expenses_projectid').val(result["project_id"]).trigger('change.select2');
                            var company = $('#Expenses_company_id').val();
                            if (company == '') {
                                $('#Expenses_company_id').val(result["company"]).trigger('change.select2');
                            }
                        }
                    });
                }
            }
        });
        Number.prototype.toFixedDown = function (digits) {
            var re = new RegExp("(\\d+\\.\\d{" + digits + "})(\\d)"),
                    m = this.toString().match(re);
            return m ? parseFloat(m[1]) : this.valueOf();
        };
        $("#Expenses_expensetype_id").change(function () {
            var expType = $("#Expenses_expensetype_id").val();
            var bill = $("#Expenses_bill_id").val();
            if (expType != "" || expType != 0) {
                $("#Expenses_payment_type").attr("disabled", true);
                $("#txtReceipt").attr("disabled", true);
                $("#Expenses_vendor_id").val("");
                $("#Expenses_vendor_id").attr("disabled", false);
                $("#txtAmount").attr("disabled", false);
                $("#txtSgstp").attr("disabled", false);
                $("#txtSgst").attr("disabled", false);
                $("#txtCgstp").attr("disabled", false);
                $("#txtCgst").attr("disabled", false);
                $("#txtTotal").attr("disabled", false);
                $("#txtPurchaseType").attr("disabled", false);
                $("#txtPaid").attr("disabled", false);
            } else {
                if (bill == 0) {
                    $("#txtAmount").attr("readonly", false);
                    $("#txtSgstp").attr("readonly", false);
                    $("#txtCgstp").attr("readonly", false);
                    $("#txtCgstp").attr("readonly", false);
                    $("#Expenses_vendor_id").val("");
                    $("#Expenses_vendor_id").attr("disabled", false);
                } else {
                    $("#txtAmount").val("");
                    $("#txtAmount").attr("readonly", true);
                    $("#txtSgstp").val("");
                    $("#txtSgstp").attr("readonly", true);
                    $("#txtCgstp").val("");
                    $("#txtCgstp").attr("readonly", true);
                    $("#txtIgstp").val("");
                    $("#txtCgstp").attr("readonly", true);
                    $("#Expenses_vendor_id").val("");
                    $("#Expenses_vendor_id").attr("readonly", true);
                }

                $("#Expenses_payment_type").attr("disabled", false);
                $("#txtReceipt").attr("disabled", false);
                $("#txtSgst").attr("readonly", true);
                $("#txtCgst").attr("readonly", true);
                $("#txtCgst").attr("readonly", true);
                $("#txtTotal").attr("readonly", true);
                $("#txtPurchaseType").val("");
                $("#txtPurchaseType").attr("disabled", true);
                $("#txtPaid").val("");
                $("#txtPaid").attr("disabled", true);
            }
        });
        $("#txtPurchaseType").change(function () {
            $('#loading').show();
            var purchaseType = $("#txtPurchaseType").val();
            var accepted_amount = $("#accepted_amount").val();
            // if(accepted_amount ==""){
                var amount = parseFloat($("#txtTotal").val());
            // }else{
            //     var amount = parseFloat(accepted_amount);
            // }
            
            var expType = $("#Expenses_expense_type").val();
            if (purchaseType == 1) {
                $.ajax({
                    method: "POST",
                    dataType: "json",
                    url: '<?php echo Yii::app()->createUrl('expenses/ajaxcall'); ?>',
                    success: function (result) {
                        $("#buttonsubmit").focus();
                    }
                });
                $("#txtPaid").val(0);
                $("#txtPaid").attr("readOnly", true);
                $("#txtTdsp").val("");
                $("#txtTdsp").attr("readonly", true);
                $("#txtTds1").text("");
                $("#txtTds").val("");
                $("#txtTdsPaid").html('<label class="final-section-label w-100p">Amount To Be Paid: </label><span class="total-value-label w-100p">0.00</span>');
                $("#Expenses_paidamount").val("");
            } else if (purchaseType == 2) {
                $("#buttonsubmit").attr('disabled',false);
                if (expType == 88) {
                    $.ajax({
                        method: "POST",
                        dataType: "json",
                        url: '<?php echo Yii::app()->createUrl('expenses/ajaxcall'); ?>',
                        success: function (result) {
                            $("#Expenses_bank_id").select2("focus");
                        }
                    });

                } else if (expType == 89 || expType == 103) {
                    $.ajax({
                        method: "POST",
                        dataType: "json",
                        url: '<?php echo Yii::app()->createUrl('expenses/ajaxcall'); ?>',
                        success: function (result) {
                            $("#buttonsubmit").focus();
                        }
                    });
                }
                $("#txtPaid").val(amount.toFixed(2));
                $("#txtPaid").attr("readOnly", true);
                $("#txtTdsp").attr("readOnly", false);
                getTdsCalculations(amount.toFixed(2));
            } else if (purchaseType == 3) {
                $.ajax({
                    method: "POST",
                    dataType: "json",
                    url: '<?php echo Yii::app()->createUrl('expenses/ajaxcall'); ?>',
                    success: function (result) {
                        $("#txtPaid").focus();
                    }
                });
                $("#txtPaid").val(0);
                $("#txtPaid").attr("readOnly", false);
                $("#txtTdsp").attr("readOnly", false);
                getTdsCalculations(amount.toFixed(2));
            } else {
                $("#txtPaid").val("0");
                $("#txtPaid").attr("readOnly", false);
                $("#txtTdsp").attr("readOnly", false);
                getTdsCalculations(amount.toFixed(2));
            }

        });
        $("#Expenses_projectid").change(function () {

            var project_id = $("#Expenses_projectid").val();
            project_id = project_id ? project_id : 0;
            getDropdownList(project_id, 0, 0, '', 0);

            $("#txtAmount").attr("readonly", false);
            $("#txtSgstp").attr("readonly", false);
            $("#txtCgstp").attr("readonly", false);
            $("#txtIgstp").attr("readonly", false);
            $("#Expenses_vendor_id").attr("disabled", false);

        });
        $("#btnReset, [data-toggle='collapse']").click(function () {
            $("#txtDaybookId").val("");
            $("#buttonsubmit").text("ADD");
            $('#expenses-form').find('input, select, textarea').not("#Expenses_expense_date").val('');
            $("#Expenses_expensetype_id").html('<option value="">-Select Expense Head-</option>');
            $("#Expenses_vendor_id").html('<option value="">-Select Vendor-</option>');
            $("#Expenses_vendor_id").attr("readonly", true);
            $("#Expenses_expensetype_id").attr("disabled", false);
            $("#txtPurchaseType").attr("disabled", true);
            $('#Expenses_projectid').val('').trigger('change.select2');
            $('#Expenses_bill_id').val('0').trigger('change.select2');
            $('#Expenses_expensetype_id').val('').trigger('change.select2');
            $('#txtPurchaseType').val('').trigger('change.select2');
            $('#Expenses_expense_type').val('').trigger('change.select2');
            $('#Expenses_bank_id').val('').trigger('change.select2');
            $('#Expenses_vendor_id').val('').trigger('change.select2');
            $('#Expenses_employee_id').val('').trigger('change.select2');
            $('#Expenses_company_id').val('').trigger('change.select2');
            $('.employee_box').hide();
            $("#txtSgst1").text("");
            $("#txtCgst1").text("");
            $("#txtIgst1").text("");
            $("#txtTotal1").html("");
            $("#txtgstTotal").text("");
            $("#txtTdsp").val("");
            $("#txtTdsp").attr("readonly", false);
            $("#txtTds1").text("");
            $("#txtTds").val("");
            $("#txtTdsPaid").html("");
            $("#Expenses_paidamount").val("");
        });
        $("#Expenses_expensetype_id").change(function () {
            var expId = $("#Expenses_expensetype_id").val();
            var expId = expId ? expId : 0;
            getVendorList(expId, 0);
        });
        $("#Expenses_projectid").click(function () {
            var project = $("#Expenses_projectid").val();
            if (project != "")
                $("#Expenses_bill_id").focus();
        });
        $("#Expenses_bill_id").click(function () {
            var typeId = $(this).find(':selected').data('id');
            var bill = $("#Expenses_bill_id").val();
            if (bill != "") {
                if (typeId == 1) {
                    $("#Expenses_expensetype_id").focus();
                } else {
                    $("#Expenses_expense_type").focus();
                }
            }

        });
        $("#Expenses_expensetype_id").click(function () {
            var exptypeid = $("#Expenses_expensetype_id").val();
            if (exptypeid != "")
                $("#Expenses_vendor_id").focus();
        });
        $("#Expenses_vendor_id").change(function () {
            $('#loading').show();
            var vendor = $("#Expenses_vendor_id").val();
            if (vendor != "")
                $.ajax({
                    method: "POST",
                    dataType: "json",
                    url: '<?php echo Yii::app()->createUrl('expenses/ajaxcall'); ?>',
                    success: function (result) {
                        $('#Expenses_expense_type').select2("focus");
                    }
                });
        });
        $("#Expenses_expense_type").change(function () {
            $('#loading').show();
            var bill_data = $("#Expenses_bill_id").val();
            var expType = $("#Expenses_expense_type").val();
            if (expType != "") {
                if (bill_data == 0) {
                    if (expType == 103) {
                        $.ajax({
                            method: "POST",
                            dataType: "json",
                            url: '<?php echo Yii::app()->createUrl('expenses/ajaxcall'); ?>',
                            success: function (result) {
                                $('#Expenses_employee_id').select2("focus");
                            }
                        });
                    } else {
                        $.ajax({
                            method: "POST",
                            dataType: "json",
                            url: '<?php echo Yii::app()->createUrl('expenses/ajaxcall'); ?>',
                            success: function (result) {
                                $('#txtAmount').focus();
                            }
                        });
                    }

                } else {
                    if (expType == 103) {
                        $.ajax({
                            method: "POST",
                            dataType: "json",
                            url: '<?php echo Yii::app()->createUrl('expenses/ajaxcall'); ?>',
                            success: function (result) {
                                $('#Expenses_employee_id').select2("focus");
                            }
                        });
                    } else {
                        $.ajax({
                            method: "POST",
                            dataType: "json",
                            url: '<?php echo Yii::app()->createUrl('expenses/ajaxcall'); ?>',
                            success: function (result) {
                                $('#txtDescription').focus();
                            }
                        });
                    }
                }
            }

        });

        $("#Expenses_employee_id").change(function () {
            $('#loading').show();
            var typeId = $("#Expenses_bill_id").find(':selected').data('id');
            if (typeId == 1 || typeId == 2) {
                if ($(this).val() != '') {
                    $.ajax({
                        method: "POST",
                        dataType: "json",
                        url: '<?php echo Yii::app()->createUrl('dailyexpense/ajaxcall'); ?>',
                        success: function (result) {
                            $("#txtDescription").focus();
                        }
                    });
                }
            } else {
                if ($(this).val() != '') {
                    $.ajax({
                        method: "POST",
                        dataType: "json",
                        url: '<?php echo Yii::app()->createUrl('dailyexpense/ajaxcall'); ?>',
                        success: function (result) {
                            $("#txtAmount").focus();
                        }
                    });
                }
            }
        });

        $("#txtDescription").keyup(function (e) {
            $('#loading').show();
            if (e.keyCode == 13) {
                var bills = $("#Expenses_bill_id").val();
                var expense_type = $("#Expenses_expense_type").val();
                var expense_head = $("#Expenses_expensetype_id").val();
                if (bills == 0) {
                    if (expense_head == "") {
                        if (expense_type != "") {
                            if (expense_type == 0) {
                                $("#buttonsubmit").focus();
                            } else {
                                $.ajax({
                                    method: "POST",
                                    dataType: "json",
                                    url: '<?php echo Yii::app()->createUrl('expenses/ajaxcall'); ?>',
                                    success: function (result) {
                                        $("#txtPurchaseType").select2("focus");
                                    }
                                });
                            }
                        }
                    } else {

                        $.ajax({
                            method: "POST",
                            dataType: "json",
                            url: '<?php echo Yii::app()->createUrl('expenses/ajaxcall'); ?>',
                            success: function (result) {
                                if (expense_type == 0) {
                                    $("#buttonsubmit").focus();
                                } else {
                                    $("#txtPurchaseType").select2("focus");
                                }
                            }
                        });
                    }
                } else {
                    $.ajax({
                        method: "POST",
                        dataType: "json",
                        url: '<?php echo Yii::app()->createUrl('expenses/ajaxcall'); ?>',
                        success: function (result) {
                            $("#txtPurchaseType").select2("focus");
                        }
                    });
                }
            }
        });
        $("#Expenses_payment_type").click(function () {
            var receipt = $("#Expenses_payment_type").val();
            if (receipt != "") {
                $("#txtReceipt").focus();
            }
        });
        $("#txtReceipt").keyup(function (e) {
            if (e.keyCode == 13) {
                var rType = $("#Expenses_payment_type").val();
                if (rType == 88)
                    $("#Expenses_bank_id").focus();
                else
                    $("#buttonsubmit").focus();
            }
        });
        $("#txtPaid").keyup(function (e) {
            if (e.keyCode == 13) {
                var expType = $("#Expenses_expense_type").val();
                if (expType == 88)
                    $("#Expenses_bank_id").focus();
                else
                    $("#buttonsubmit").focus();
            }
        });
        $("#Expenses_bank_id").change(function () {
            $('#loading').show();
            var bank = $("#Expenses_bank_id").val();
            if (bank != "")
                $.ajax({
                    method: "POST",
                    dataType: "json",
                    url: '<?php echo Yii::app()->createUrl('expenses/ajaxcall'); ?>',
                    success: function (result) {
                        $("#txtChequeno").focus();
                    }
                });
        });
        $("#txtChequeno").keyup(function (e) {
            if (e.keyCode == 13) {
                $("#buttonsubmit").focus();
            }
        });
        $("#Expenses_expense_type").change(function () {
            var expType = $("#Expenses_expense_type").val();
            if (expType == 88) {
                $("#Expenses_bank_id").attr("disabled", false);
                $("#txtChequeno").attr("readonly", false);
                $(".employee_box").hide();
                $("#Expenses_employee_id").attr("disabled", true);
            } else {
                if (expType == 103) {
                    $(".employee_box").show();
                    $("#Expenses_employee_id").attr("disabled", false);
                    $('#Expenses_bank_id').val("").trigger('change.select2');
                    $("#txtChequeno").val("");
                    $("#Expenses_bank_id").attr("disabled", true);
                    $("#txtChequeno").attr("readonly", true);
                } else {
                    $('#Expenses_bank_id').val("").trigger('change.select2');
                    $("#txtChequeno").val("");
                    $("#Expenses_bank_id").attr("disabled", true);
                    $("#txtChequeno").attr("readonly", true);
                    $(".employee_box").hide();
                    $("#Expenses_employee_id").attr("disabled", true);
                }
            }
            if (expType == 0) {
                $("#txtPurchaseType").html('<option value="1">Credit</option>');
                $("#txtPurchaseType").attr("disabled", true);
                $("#txtPaid").val(0);
                $("#txtPaid").attr("readOnly", true);
                $("#txtTdsp").val("");
                $("#txtTdsp").attr("readOnly", true);
                $("#txtTds1").text("");
                $("#txtTds").val("");
                $("#txtTdsPaid").html('<label class="final-section-label w-100p">Amount To Be Paid: </label><span class="total-value-label w-100p">0.00</span>');
                $("#Expenses_paidamount").val("");
            } else {
                $("#txtPurchaseType").html('<option value="">-Select Purchase Type-</option><option value="2">Full Paid</option><option value="3">Partially Paid</option>');
                $("#txtPurchaseType").attr("disabled", false);
                $("#txtPaid").val("");
                $("#txtPaid").attr("readOnly", false);
                $("#txtTdsp").val("");
                $("#txtTdsp").attr("readOnly", false);
            }

        });
    });

    function validateDateSelection(cDate) {
        var selDate = cDate;
        var currDate = new Date();
        currDate = $.format.date(currDate, "dd-MM-yyyy");
        if (selDate == currDate) {
            $("a#next").css("pointer-events", "none").css("color", "#ccccc");
        } else {
            $("a#next").css("pointer-events", "visible").css("color", "#337ab7");
        }
        $("#btnReset").click();
        $("#txtDaybookId").val("");
        $("#Expenses_expense_date").val(selDate);
        $('#Expenses_bill_id').val('0').trigger('change.select2');
        $("#Expenses_expensetype_id").html('<option value="">-Select Expense Head-</option>');
        $("#Expenses_vendor_id").html('<option value="">-Select Vendor-</option>');
        $("#Expenses_vendor_id").attr("readonly", true);
    }

    function validateData() {
        var projectId = $("#Expenses_projectid").val();
        var billNumber = $("#Expenses_bill_id").val();
        var expenseType = $("#Expenses_expensetype_id").val();
        var vendorId = $("#Expenses_vendor_id").val();
        var amount = $("#txtAmount").val();
        var sgstP = $("#txtSgstp").val();
        var sgst = $("#txtSgst").val();
        var cgstP = $("#txtCgstp").val();
        var cgst = $("#txtCgst").val();
        var total = $("#txtTotal").val();
        var receiptType = $("#Expenses_payment_type").val();
        var receipt = $("#txtReceipt").val();
        var purchaseType = $("#txtPurchaseType").val();
        var paid = $("#txtPaid").val();
        var currId = $(this).attr(id);
        if (expenseType != "" || expenseType != 0) {
            $("#Expenses_payment_type").attr("disabled", true);
            $("#txtReceipt").attr("readonly", true);
            $("#Expenses_vendor_id").val("");
            $("#Expenses_vendor_id").attr("disabled", false);
            $("#txtAmount").attr("readonly", false);
            $("#txtSgstp").attr("readonly", false);
            $("#txtSgst").attr("readonly", false);
            $("#txtCgstp").attr("readonly", false);
            $("#txtCgst").attr("readonly", false);
            $("#txtTotal").attr("readonly", false);
        }
        if (currId == "Expenses_expensetype_id" && (expenseType == "" || expenseType == 0)) {
            $("#Expenses_payment_type").attr("disabled", false);
            $("#txtReceipt").attr("readonly", false);
        }
    }

    $("#txtAmount, #txtSgstp, #txtCgstp, #txtIgstp").blur(function () {

        var expType = $("#Expenses_expense_type").val();
        if (expType == 0) {
            $("#txtPurchaseType").html('<option value="1">Credit</option>');
        } else {
            $("#txtPurchaseType").html('<option value="">-Select Purchase Type-</option><option value="2">Full Paid</option><option value="3">Partially Paid</option>');
        }

        var amount = parseFloat($("#txtAmount").val());
        var sgstp = parseFloat($("#txtSgstp").val());
        var cgstp = parseFloat($("#txtCgstp").val());
        var igstp = parseFloat($("#txtIgstp").val());
        var sgst = (sgstp / 100) * amount;
        var cgst = (cgstp / 100) * amount;
        var igst = (igstp / 100) * amount;

        if (isNaN(sgst))
            sgst = 0;
        if (isNaN(cgst))
            cgst = 0;
        if (isNaN(igst))
            igst = 0;
        if (isNaN(amount))
            amount = 0;

        var accepted_amount = $("#accepted_amount").val();
        var additional_amount = parseFloat($("#additionalamount").val());
        var roundoff = $("#roundoff").val();

        if(isNaN(roundoff) || roundoff==''){
            roundoff = 0;
        }
        
        if(accepted_amount ==""){
            var amount = parseFloat($("#txtAmount").val());
            if(isNaN(amount)){
                amount = 0;
            }
            if(isNaN(additional_amount)){
                additional_amount = 0;
            }
            var total = amount + sgst + cgst + igst + additional_amount + roundoff;
        }else{
            var amount = parseFloat(accepted_amount);
            if(isNaN(amount)){
                amount = 0;
            }
            if(isNaN(additional_amount)){
                additional_amount = 0;
            }
            var total = amount + additional_amount + roundoff;
        }
        
        var gsttotal = (parseFloat(sgst) + parseFloat(cgst) + parseFloat(igst)).toFixed(2);
        var total_accepted_amount = $("#total_accepted_amount").val();
        if(total_accepted_amount ==""){
            $("#txtTotal1").html('<label class="final-section-label w-100p">Total: </label><span class="total-value-label w-100p">' + total.toFixed(2) + '</span>');
        }else{
            $("#txtTotal1").html('<label class="final-section-label w-100p">Total: </label><span class="total-value-label w-100p">' + total_accepted_amount + '</span>');
        }

        $("#txtSgst").val(sgst.toFixed(2));
        $("#txtCgst").val(cgst.toFixed(2));
        $("#txtIgst").val(igst.toFixed(2));
        $("#txtTotal").val(total.toFixed(2));

        $(".gstvalue").css({
            "display": "inline-block"
        });
        $("#txtSgst1").text(sgst.toFixed(2));
        $("#txtCgst1").text(cgst.toFixed(2));
        $("#txtIgst1").text(igst.toFixed(2));
        
        $("#txtgstTotal").html('<label class="final-section-label w-100p">Total Tax: </label><span class="total-value-label w-100p">'  + gsttotal + '</span>');
        $("#txtTdsPaid").html('<label class="final-section-label w-100p">Amount To Be Paid: </label><span class="total-value-label w-100p">' + total.toFixed(2) + '</span>');                
        getTdsCalculations(total);
    });
    $("#txtTdsp").blur(function () {
        var tdsp = parseFloat($("#txtTdsp").val());
        var total = parseFloat($("#txtTotal").val());
        var paid = parseFloat($("#txtPaid").val());
        var tds = (tdsp / 100) * paid;
        if (isNaN(tds))
            tds = 0;
        var paidamt = paid - tds;
        $("#txtTds1").text(tds.toFixed(2));
        $("#txtTdsPaid").html('<label class="final-section-label w-100p">Amount To Be Paid: </label><span class="total-value-label w-100p">' + paidamt.toFixed(2) + '</span>');
        $("#Expenses_paidamount").val(paidamt.toFixed(2));
        $("#txtTds").val(tds.toFixed(2));
    });
    $("#txtAmount").keyup(function (e) {
        if (e.keyCode == 13) {
            $("#txtSgstp").focus();
        }
    });


    $("#txtSgstp").keyup(function (e) {
        if (e.keyCode == 13) {
            $("#txtCgstp").focus();
        }
    });

    $("#txtCgstp").keyup(function (e) {
        if (e.keyCode == 13) {
            $("#txtIgstp").focus();
        }
    });

    $("#txtIgstp").keyup(function (e) {
        if (e.keyCode == 13) {
            $("#txtDescription").focus();
        }
    });

    $("#Expenses_company_id").change(function () {
        $('#loading').show();
        var company_id = $("#Expenses_company_id").val();
        if (company_id != "")
            $.ajax({
                method: "POST",
                dataType: "json",
                url: '<?php echo Yii::app()->createUrl('expenses/ajaxcall'); ?>',
                success: function (result) {
                    $("#Expenses_projectid").select2("focus");
                }
            });
    });

    $(".check").keydown(function (event) {
        if (event.shiftKey == true) {
            event.preventDefault();
        }
        if ((event.keyCode >= 48 && event.keyCode <= 57) ||
                (event.keyCode >= 96 && event.keyCode <= 105) ||
                event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 ||
                event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190 || event.keyCode == 110 || event.keyCode == 13) {
            var splitfield = $(this).val().split(".");
            if ((splitfield[1] !== undefined && splitfield[1].length >= 2) && event.keyCode != 8 && event.keyCode != 0 && event.keyCode != 13) {
                event.preventDefault();
            }
        } else {
            event.preventDefault();
        }
        if ($(this).val().indexOf('.') !== -1 && (event.keyCode == 190 || event.keyCode == 110))
            event.preventDefault();
    });

    $('.percentage').keyup(function () {
        if ($(this).val() > 100) {
            $(this).val('');
        }
    });

    $(document).on('click', '.delete_confirmation', function (e) {
        var rowId = $(this).attr("id");
        var expId = $("#expenseid" + rowId).val();
        var date = $("#Expenses_expense_date").val();
        var answer = confirm("Are you sure you want to delete?");
        if (answer) {
            $('.loading-overlay').addClass('is-active');
            $.ajax({
                type: "POST",
                data: {
                    expId: expId,
                    expense_date: date,
                    status: 1,
                    delId: "",
                    action: 1
                },
                url: "<?php echo Yii::app()->createUrl("expenses/deleteconfirmation") ?>",
                success: function (data) {
                    // $('.loading-overlay').removeClass('is-active');
                    if (data == 1) {
                        $("#errormessage").show()
                                .html('<div class="alert alert-danger"><strong>Failed!</strong> This request unable to process now. Please try again later.</div>')
                                .fadeOut(5000);
                        return false;
                    } else {
                        $("#errormessage").show()
                                .html('<div class="alert alert-success"><strong>Success!</strong> Request for deleting this row is send to admin. Please wait for response.</div>')
                                .fadeOut(5000);
                        $("#newlist").html(data);
                    }
                }
            });
        } else {
            return false;
        }
    });

    $(document).on('click', '.delete_restore', function (e) {
        var rowId = $(this).attr("id");
        var delId = $(this).attr("data-id");
        var expId = $("#expenseid" + rowId).val();
        var date = $("#Expenses_expense_date").val();
        var answer = confirm("Are you sure you want to restore?");
        if (answer) {
            $('.loading-overlay').addClass('is-active');
            $.ajax({
                type: "POST",
                data: {
                    expId: expId,
                    expense_date: date,
                    status: 2,
                    delId: delId,
                    action: 1
                },
                url: "<?php echo Yii::app()->createUrl("expenses/deleteconfirmation") ?>",
                success: function (data) {
                    // $('.loading-overlay').removeClass('is-active');
                    if (data == 1) {
                        $("#errormessage").show()
                                .html('<div class="alert alert-danger"><strong>Failed!</strong> This request unable to process now. Please try again later.</div>')
                                .fadeOut(5000);
                        return false;
                    } else {
                        $("#errormessage").show()
                                .html('<div class="alert alert-success"><strong>Success!</strong> Restored data successfully.</div>')
                                .fadeOut(5000);
                        $("#newlist").html(data);
                    }
                }
            });
        } else {
            return false;
        }
    });
    
    // delete row
    $(document).on('click', '.delete_row', function (e) {
        var reconStatus = $(this).parents("td").attr("data-status");
        if(reconStatus==1){
            alert("Can't delete !Reconciled Entry");
            return;
        }
        var answer = confirm("Are you sure you want to delete?");
        if (answer) {
            $('.loading-overlay').addClass('is-active');
            var rowId = $(this).attr("id");
            var expId = $("#expenseid" + rowId).val();
            var date = $("#Expenses_expense_date").val();
            $.ajax({
                type: "POST",
                data: {
                    expId: expId,
                    expense_date: date
                },
                dataType:'json',
                url: "<?php echo Yii::app()->createUrl("expenses/deleteexpense") ?>",
                success: function (data) {
                    if (data.response == 1) {
                        $("#errormessage").show()
                                .html('<div class="alert alert-warning"><strong>Failed !</strong> please try again.</div>')
                                .fadeOut(5000);
                        $("#buttonsubmit").attr('disabled', false);
                        return false;
                    } else if (data.response == 2) {
                        $("#errormessage").show()
                                .html('<div class="alert alert-warning"><strong>'+data.data+'</div>')
                                .fadeOut(2000);
                    }
                    else{
                        $("#errormessage").show()
                                .html('<div class="alert alert-success"><strong>Success!</strong> Row Deleted.</div>')
                                .fadeOut(2000);
                        $("#newlist").html(data.data);
                        setTimeout(function(){
                            window.location.reload(1);
                        }, 500);
                    }

                    $('#Expenses_projectid').val('').trigger('change.select2');
                    $('#Expenses_bill_id').val('0').trigger('change.select2');
                    $('#Expenses_expensetype_id').html('<option value="">-Select Expense Head-</option>');
                    $('#txtPurchaseType').val("").trigger('change.select2');
                    $('#Expenses_expense_type').val('').trigger('change.select2');
                    $('#Expenses_employee_id').val('').trigger('change.select2');
                    $('#Expenses_bank_id').val('').trigger('change.select2');
                    $('#Expenses_vendor_id').html('<option value="">-Select Vendor-</option>');
                    $('#Expenses_company_id').val('').trigger('change.select2');
                    $("#Expenses_vendor_id").attr("readonly", true);
                    $("#txtAmount").attr("readonly", true);
                    $("#txtSgstp").attr("readonly", true);
                    $("#txtCgstp").attr("readonly", true);
                    $("#txtIgstp").attr("readonly", true);
                    $("#txtPurchaseType").attr("disabled", true);
                    $("#txtPaid").attr("readonly", true);
                    $("#Expenses_bank_id").attr("disabled", true);
                    $("#txtChequeno").attr("readonly", true);

                    $("#txtSgst1").text("");
                    $("#txtCgst1").text("");
                    $("#txtIgst1").text("");
                    $("#txtTotal1").html("");
                    $("#txtgstTotal").text("");
                }
            })
        }

    })

    function getTdsCalculations(total) {
        if ($("#txtTdsp").val() != "") {
            var tdsp = parseFloat($("#txtTdsp").val());
        } else {
            var tdsp = 0;
        }

        var tdsamount = (tdsp / 100) * total;
        if (isNaN(tdsamount))
            tdsamount = 0;
        var paidamount = total - parseFloat(tdsamount);
        if (isNaN(paidamount))
            paidamount = 0;

        $("#txtTds1").text(tdsamount.toFixed(2));
        $("#txtTds").val(tdsamount.toFixed(2));
        $("#txtTdsPaid").html('<label class="final-section-label w-100p">Amount To Be Paid: </label><span class="total-value-label w-100p">' + paidamount.toFixed(2) + '</span>');
        $("#Expenses_paidamount").val(paidamount.toFixed(2));
    }
    $(document).ajaxComplete(function () {
        $('.loading-overlay').removeClass('is-active');
        $('#loading').hide();
    });

    $("#txtChequeno").change(function (e) {
        var cheque_no = $("#txtChequeno").val();
        var bank = $("#Expenses_bank_id").val();
        $.ajax({
            type:'POST',               
            data: {
                cheque_no: cheque_no, 
                bank:bank                   
            },
            url: "<?php echo Yii::app()->createUrl("expenses/testChequeNumber") ?>",
            success: function (data) {
                $('.chq_error').html(data);
                return;                
            }
        })
    });
    $("#Expenses_bill_id").change(function(){
        $("#accepted_amount").val("");
        $(".accepted_detais").html("");
        var type = $(this).val().split(',')[0].trim();
        if(type == 1){
            var id =$(this).val().split(',')[1].trim();
            var entry_date = $("#Expenses_expense_date").val();
            $.ajax({
                type:'POST',
                dataType:'json',
                data:{
                    bill_id:id,
                    entry_date:entry_date
                },
                url:"<?php echo Yii::app()->createUrl("expenses/checkBillDate") ?>",
                success:function(response){
                    $("#accepted_amount").val("");
                    $(".accepted_detais").html("");
                    if(response.response=="error"){  
                        console.log('err');
                        $("#errormessage").show().html('<div class="alert alert-danger">'+response.msg+'</div>');
                        $("#buttonsubmit").attr('disabled', true);                    
                    }else{
                        if(response.accepted!=""){
                            $("#accepted_amount").val(response.accepted.toFixed(2));
                            var acceptedAmount = parseFloat(response.accepted).toFixed(2);
                            $(".accepted_detais").html("Amount of accepted items: "+acceptedAmount);
                            $("#total_accepted_amount").val(response.total_accepted.toFixed(2));
                            $("#accepted_without_tax").val(response.accepted_without_tax);
                        }
                                        
                        $("#errormessage").hide()
                        $("#buttonsubmit").attr('disabled', false);
                        return false;
                    }
                }
            })
        }
    })

    $("body").on("click", ".generate_voucher", function (e) {
        var id = $(this).attr('data-id');
        window.location.href='<?php echo Yii::app()->createUrl("expenses/generateVoucher&id=") ?>'+id;    
    })
    $(function(){
        var bill_id = $(".request_bill_id").val();        
        if(bill_id !=""){
            $(".addentries").trigger("click");
            $("#Expenses_bill_id").val(bill_id);
            
            $("#Expenses_bill_id").val(bill_id).trigger("change");
        }
    })
</script>
