<div id="msg_box"></div>
<?php
if ($index == 0) {
?>
    <div class="filter_sec pull-right col-sm-6 col-md-4 padding-0">
        <label>Search:</label>
        <input id="sub_cat_search" type="text" class="form-control">
    </div>
    <thead class="entry-table sticky-thead">
        <tr>

            <th style="width: 20px;">No</th>
            <th>Sub Category Name</th>
            <th>Company</th>
            <th>Created Date</th>
            <?php
            if (isset(Yii::app()->user->role) && (in_array('/purchaseCategory/manageitem', Yii::app()->user->menuauthlist))) {
            ?>
                <th></th>
            <?php } ?>

        </tr>
    </thead>
<?php } ?>

<tr>

    <td style="width: 20px;"><?php echo $index + 1; ?></td>
    <td><?php echo $data->sub_category_name; ?></td>
    <td>
        <?php
        $companyname = array();
        $arrVal = explode(',', $data->company_id);
        foreach ($arrVal as $arr) {
            $value = Company::model()->findByPk($arr);
            if ($value !== NULL) {
                array_push($companyname, $value['name']);
            }
        }
        echo rtrim(trim(implode(', ', $companyname)), ",");
        ?>
    </td>
    <td><?php echo date('d-m-Y', strtotime($data->created_date));  ?></td>
    <?php
    if (isset(Yii::app()->user->role) && (in_array('/purchaseCategory/manageitem', Yii::app()->user->menuauthlist))) {
    ?>
        <td class="wrap">
            <span class="edit_purchase edit_option_<?php echo $data->id; ?>" id="<?php echo $data->id; ?>" data-toggle="tooltip" title="Edit"><span class="fa fa-edit editsubItems" data-toggle="modal" data-target=".edit" data-id="<?php echo $data->id; ?>"></span></span>
            <?php
            if (isset(Yii::app()->user->role) && (in_array('/purchaseCategory/removecategory', Yii::app()->user->menuauthlist))) {
            ?>
                <span class="delete_subcat"  title="Delete" onclick="deletSubCategory(this,<?php echo $data->id; ?>)">
                <span  class="fa fa-trash deleteCat" ></span>
                </span>
            <?php } ?>            
        </td>
    <?php } ?>


</tr>