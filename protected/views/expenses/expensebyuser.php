<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$this->breadcrumbs = array(
    'performa invoice',
)
    ?>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/tableHeadFixer.js"></script>
<div class="container" id="project">
    <div class="expenses-heading mb-10">
        <h3>Expense Report By User</h3>
    </div>
    <?php $this->renderPartial('_newexpensesearch', array('model' => $model)) ?>
    <div class=" margin-top-10" id="newlist">
        <?php $this->widget('zii.widgets.CListView', array(
            'dataProvider' => $dataProvider,
            //'itemView'=>'_view',
            'itemView' => '_newexpenseview',
            'template' => '<div>{summary}{sorter}</div><div class="table-responsive" id="parent"><table class="table" id="fixTable">{items}</table></div>{pager}',
            'ajaxUpdate' => false,
        )); ?>
    </div>
</div>
<style>
    #parent {
        max-height: 60vh;
        border: 1px solid #ddd;
        border-top: 0px;
        border-right: 0px;
    }

    table thead th {
        background: #eee;
    }

    .table {
        margin-bottom: 0px;
    }

    td.left_fix {
        background-color: #fafafa !important;
    }
</style>
<script>
    // $(document).ready(function () {
    //     $("#fixTable").tableHeadFixer({
    //         'left': 3,
    //         'head': true
    //     });
    // });
    $(function () {
        $("#Expenses_fromdate").datepicker({
            dateFormat: 'dd-mm-yy'
        });
    });
    $(function () {
        $("#Expenses_todate").datepicker({
            dateFormat: 'dd-mm-yy'
        });
    });
    $("#clearForm").click(function () {
        window.location.href = "<?php echo Yii::app()->createAbsoluteUrl("expenses/expensereportbyuser") ?>";
    });
    $(document).delegate('.bill_view', 'click', function (e) {
        var bill_id = $(this).attr('id');
        window.open("<?php echo Yii::app()->createUrl('bills/view&id=') ?>" + bill_id, '_blank');
    });
</script>