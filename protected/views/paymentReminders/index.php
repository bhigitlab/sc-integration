<?php
/* @var $this PaymentRemindersController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Payment Reminders',
);

$this->menu=array(
	array('label'=>'Create PaymentReminders', 'url'=>array('create')),
	array('label'=>'Manage PaymentReminders', 'url'=>array('admin')),
);
?>

<h1>Payment Reminders</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
