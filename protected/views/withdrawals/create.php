<?php
/* @var $this WithdrawalsController */
/* @var $model Withdrawals */

$this->breadcrumbs=array(
	'Withdrawals'=>array('index'),
	'Create',
);

?>
<div class="modal-dialog modal-lg">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" onclick="javascript:window.location.reload()">&times;</button>
			<h4 class="modal-title">Add Withdrawal</h4>
		</div>


<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
	</div>
	
</div>
