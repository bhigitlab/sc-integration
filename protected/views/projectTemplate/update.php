<?php
/* @var $this ClientsController */
/* @var $model Clients */

$this->breadcrumbs = array(
    'Project Template' => array('index'),
    $model->template_name => array('view', 'id' => $model->template_id),
    'Update',
);
?>
<div class="panel panel-gray">
    <div class="panel-heading form-head">
        <h3 class="panel-title">Edit Template</h3>
    </div>
        <?php echo $this->renderPartial('_form', array('model' => $model, 'count_revisions' => $count_revisions)); ?>
</div>
