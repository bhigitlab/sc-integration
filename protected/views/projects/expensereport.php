<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/tableHeadFixer.js"></script>
<?php

/* @var $this ProjectsController */

/* @var $model Projects */


Yii::app()->clientScript->registerScript('search', "

    $('#btSubmit').click(function(){

            $('#projectform').submit();

    });



");
$tblpx = Yii::app()->db->tablePrefix;
$newQuery = "";
$newQuery1 = "";
$newQuery2 = "";
$newQuery3 = "";
$user = Users::model()->findByPk(Yii::app()->user->id);
$arrVal = explode(',', $user->company_id);
foreach ($arrVal as $arr) {
    if ($newQuery)
        $newQuery .= ' OR';
    $newQuery .= " FIND_IN_SET('" . $arr . "', id)";
}
$report_flag = isset($project_id) && !empty($project_id) && $project_id != 0;
?>

<div class="container" id="project">
    <div class="header-container">
        <h3>Project Report</h3>
        <div class="btn-container">
            <?php if ($report_flag) { ?>
                <a style="cursor:pointer;" id="download" class="btn btn-info" title="SAVE AS EXCEL"
                    href="<?php echo $this->createAbsoluteUrl('projects/ExportProjectCSV', array("project_id" => $project_id, "date_from" => $fromdate, "date_to" => $todate)) ?>">
                    <i class="fa fa-file-excel-o button-icon" aria-hidden="true"></i></a>
                <a style="cursor:pointer;" id="download" class="btn btn-info" title="SAVE AS PDF"
                    href="<?php echo $this->createAbsoluteUrl('projects/saveexpensereport', array("project_id" => $project_id, "date_from" => $fromdate, "date_to" => $todate)) ?>">
                    <i class="fa fa-file-pdf-o button-icon" aria-hidden="true"></i></a>
            <?php } else { ?>
                <a style="cursor:pointer; " id="download" class="btn btn-info" title="SAVE AS PDF"
                    href="<?php echo Yii::app()->createUrl('projects/savetopdfreport') ?>"><i
                        class="fa fa-file-pdf-o button-icon" aria-hidden="true"></i></a>
                <a style="cursor:pointer; " id="download" class="btn btn-info" title="SAVE AS EXCEL"
                    href="<?php echo Yii::app()->createUrl('projects/savetoexcelreport') ?>"><i
                        class="fa fa-file-excel-o button-icon" aria-hidden="true"></i></a>
            <?php } ?>

        </div>
    </div>
    <?php
    $fromdate = (!empty($date_from) ? date('d-M-Y', strtotime($date_from)) : '');
    $todate = (!empty($date_to) ? date('d-M-Y', strtotime($date_to)) : '');
    ?>
    <div class="page_filter clearfix custom-form-style">
        <form id="projectform" action="<?php $url = Yii::app()->createAbsoluteUrl("projects/projectexpensereport"); ?>"
            method="POST">
            <div class="row">
                <div class="form-group col-xs-12 col-sm-3">
                    <?php echo CHtml::label('Projects : ', ''); ?>
                    <?php
                    echo CHtml::dropDownList('project_id', 'project_id', CHtml::listData(Projects::model()->findAll(array(
                        'select' => array('pid, name'),
                        'order' => 'name',
                        'condition' => 'company_id=' . Yii::app()->user->company_id . '',
                        'distinct' => true
                    )), 'pid', 'name'), array('empty' => 'Select Project', 'id' => 'project_id', 'class' => 'form-control', 'options' => array($project_id => array('selected' => true))));
                    ?>
                </div>
                <?php if ($project_id == 0) { ?>
                    <div class="form-group col-xs-12 col-sm-3">
                        <label>Project Status</label>
                        <?php
                        echo CHtml::dropDownList('project_status', 'project_status', CHtml::listData(Status::model()->findAll(array(
                            'select' => array('sid, caption'),
                            'order' => 'sid',
                            'condition' => 'status_type = "project_status"',
                            'distinct' => true
                        )), 'sid', 'caption'), array('empty' => 'Select Status', 'class' => 'form-control', 'id' => 'project_status', 'options' => array($project_status => array('selected' => true))));
                        ?>
                    </div>
                <?php } ?>
                <div class="form-group col-xs-12 col-sm-3">
                    <?php echo CHtml::label('From ', ''); ?>
                    <?php echo CHtml::textField('date_from', (!empty($date_from) ? date('d-M-Y', strtotime($date_from)) : ''), array("id" => "date_from", 'class' => 'form-control', 'autocomplete' => 'off', 'style' => 'display:inline-block;', "readonly" => true)); ?>
                </div>
                <div class="form-group col-xs-12 col-sm-3">

                    <?php echo CHtml::label('To ', ''); ?>
                    <?php echo CHtml::textField('date_to', (!empty($date_to) ? date('d-M-Y', strtotime($date_to)) : ''), array("id" => "date_to", 'class' => 'form-control', 'autocomplete' => 'off', 'style' => 'display:inline-block;', "readonly" => true)); ?>
                </div>
                <div class="form-group col-xs-12 text-right">
                    <div>
                        <?php echo CHtml::submitButton('GO', array('id' => 'btSubmit', 'class' => 'btn btn-info btn-sm')); ?>
                        <?php echo CHtml::resetButton('Clear', array('onclick' => 'javascript:location.href="' . $this->createUrl('expenselist') . '"', 'class' => 'btn btn-default btn-sm')); ?>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <?php if ($report_flag) {
        $daybookVT = !empty($daybookvendor) ? $daybookvendor[0]["totalvendorpaid"] : 0;
        $daybookSunT = !empty($daybooksubcon) ? $daybooksubcon[0]["subcontractortotalpaid"] : 0;
        $vendorVT = !empty($vendorpayment) ? $vendorpayment[0]["vendortotalsum"] : 0;
        $totalPayment = $daybookVT + $daybookSunT + $vendorVT;
        $totalInvoice = !empty($invoicedata) ? $invoicedata[0]["invoicetotal"] : 0;
        $invoiceReceipt = !empty($daybookreceipt) ? $daybookreceipt[0]["receiptsum"] : 0;
        $balanceInvoice = $totalInvoice - $invoiceReceipt;
        $totalBillAmt = !empty($billdata) ? $billdata[0]["billsum"] : 0;
        $totalQuotSum = !empty($quotationdata) ? $quotationdata[0]["quotationsum"] : 0;
        $totalExpense = $totalBillAmt + $daybookSunT;
        $balanceExpense = $totalExpense - $totalPayment;
        $subbilltotal = 0;
        $unbilledtotal = 0;
        ?>
        <div id="contenthtml" class="contentdiv">

            <h2 class="text-center">Project Summary of
                <?php echo $projectdata["name"]; ?>
            </h2>
            <div class="container-fluid">
                <table class="table table-bordered ">
                    <tr>
                        <td colspan="4"><b>Client : </b><?php echo $projectdata["client"]; ?>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3"><b>Project Name: </b><?php echo $projectdata["name"]; ?>
                        </td>
                        <td><b>Square Feet: </b><?php echo $projectdata["sqft"]; ?>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3"><b>Work Contract: </b><?php echo $projectdata["caption"]; ?>
                        </td>
                        <?php if ($projectdata["project_type"] == 87) { ?>
                            <td><b>Square Feet Rate :</b><?php echo $projectdata["sqft_rate"]; ?>
                            </td>
                        <?php } else if ($projectdata["project_type"] == 86) { ?>
                                <td><b>Percentage : </b>
                                <?php echo ($projectdata["percentage"]) ? $projectdata["percentage"] . '%' : ''; ?>
                                </td>
                        <?php } else if ($projectdata["project_type"] == 104) { ?>
                                    <td><b>Total Estimate : </b><?php echo $projectdata["project_totalestimate"]; ?>
                                    </td>
                        <?php } ?>
                    </tr>
                    <tr>
                        <td colspan="4"><b>Site: </b><?php echo $projectdata["site"]; ?>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" class="whiteborder">&nbsp;</td>
                    </tr>
                </table>
                <?php if (!empty($invoicedata)) { ?>
                    <h4>Invoice</h4>
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead class="entry-table sticky-thead">
                                <tr>
                                    <th width="5%">Sl No</th>
                                    <th width="50%">Invoice Number</th>
                                    <th width="25%">Date</th>
                                    <th width="20%">Invoice Amount</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = 1;
                                foreach ($invoicedata as $invoice) { ?>
                                    <tr>
                                        <td>
                                            <?php echo $i; ?>
                                        </td>
                                        <td>
                                            <?php echo $invoice["inv_no"]; ?>
                                        </td>
                                        <td>
                                            <?php echo $invoice["date"]; ?>
                                        </td>
                                        <td class="text-right">
                                            <?php echo Controller::money_format_inr($invoice["subtotal"] + $invoice["tax_amount"], 2); ?>
                                        </td>
                                    </tr>
                                    <?php
                                    $i = $i + 1;
                                } ?>
                            </tbody>
                            <tfoot class="entry-table">
                                <tr>
                                    <th colspan="3" class="text-right">Total</th>
                                    <th class="text-right">
                                        <?php echo Controller::money_format_inr($invoicedata[0]["invoicetotal"], 2); ?>
                                    </th>
                                </tr>
                            </tfoot>
                        </table>
                        <table class="table table-bordered">
                            <thead class="entry-table">
                                <tr>
                                    <th width="80%">Total Invoice Amount</th>
                                    <th class="text-right">
                                        <?php echo Controller::money_format_inr(round($invoicedata[0]["invoicetotal"]), 2); ?>
                                    </th>
                                </tr>
                            </thead>
                        </table>
                    </div>

                <?php } ?>
                <?php if (!empty($daybookreceipt)) { ?>
                    <h4>Invoice Receipt</h4>
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead class="entry-table sticky-thead">
                                <tr>
                                    <th width="5%">Sl No</th>
                                    <th width="50%">Description</th>
                                    <th width="15%">Date</th>
                                    <th width="15%">Receipt</th>
                                    <th width="15%">Paid</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = 1;
                                foreach ($daybookreceipt as $receipt) { ?>
                                    <tr>
                                        <td>
                                            <?php echo $i; ?>
                                        </td>
                                        <td>
                                            <?php echo $receipt["description"]; ?>
                                        </td>
                                        <td>
                                            <?php echo $receipt["date"]; ?>
                                        </td>
                                        <td class="text-right">
                                            <?php echo Controller::money_format_inr($receipt["receipt"], 2); ?>
                                        </td>
                                        <td></td>
                                    </tr>
                                    <?php
                                    $i = $i + 1;
                                } ?>
                            </tbody>
                            <tfoot class="entry-table">
                                <tr>
                                    <th colspan="3" class="text-right">Total</th>
                                    <th class="text-right">
                                        <?php echo Controller::money_format_inr($daybookreceipt[0]["receiptsum"], 2); ?>
                                    </th>
                                    <th></th>
                                </tr>
                            </tfoot>
                        </table>
                        <table class="table table-bordered">
                            <thead class="entry-table">
                                <tr>
                                    <th width="80%">Total Amount Received From Client</th>
                                    <th class="text-right">
                                        <?php echo Controller::money_format_inr(round($daybookreceipt[0]["receiptsum"]), 2); ?>
                                    </th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                <?php } ?>
                <?php if (!empty($billdata)) { ?>
                    <h4>Bill Details</h4>
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead class="entry-table sticky-thead">
                                <tr>
                                    <th>Sl No</th>
                                    <th>Purchase Number</th>
                                    <th>Bill Number</th>
                                    <th>Vendor</th>
                                    <th>Date</th>
                                    <th>Bill Amount</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = 1;
                                // echo '<pre>';
                                // print_r($billdata);
                                // exit;
                                foreach ($billdata as $bill) {
                                    $amount = $bill["bill_totalamount"] + $bill['bill_additionalcharge'];
                                    if (!empty($bill['bill_additionalcharge'])) {
                                        $totalBillAmt += $bill['bill_additionalcharge'];
                                        $total_amount = $billdata[0]["billsum"] + $bill['bill_additionalcharge'];
                                    } else {
                                        $total_amount = $billdata[0]["billsum"];
                                    }

                                    ?>
                                    <tr>
                                        <td width="5%">
                                            <?php echo $i; ?>
                                        </td>
                                        <td width="20%">
                                            <?php echo CHtml::link($bill["purchase_no"], Yii::app()->createUrl('purchase/viewpurchase&pid=' . $bill['purchase_id']), array('class' => '', 'target' => '_blank'));
                                            ; ?>
                                        </td>
                                        <td width="20%">
                                            <?php
                                            echo CHtml::link($bill["bill_number"], Yii::app()->createUrl('bills/view&id=' . $bill['bill_id']), array('class' => '', 'target' => '_blank'));

                                            ?>
                                        </td>
                                        <td width="20%">
                                            <?php echo $bill["vendorname"]; ?>
                                        </td>
                                        <td width="15%">
                                            <?php echo $bill["bill_date"]; ?>
                                        </td>
                                        <td width="20%" class="text-right">
                                            <?php echo Controller::money_format_inr($amount, 2); ?>
                                        </td>
                                    </tr>
                                    <?php
                                    $i = $i + 1;
                                } ?>
                            </tbody>
                            <tfoot class="entry-table">
                                <tr>
                                    <th colspan="5" class="text-right">Total</th>
                                    <th class="text-right">
                                        <?php echo Controller::money_format_inr($total_amount, 2); ?>
                                    </th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                <?php } ?>
                <?php if (!empty($subconbill)) { ?>
                    <h4>Subcontractor Bill</h4>
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead class="entry-table sticky-thead">
                                <tr>
                                    <th>Sl No</th>
                                    <th>Quotation</th>
                                    <th>Bill Number</th>
                                    <th>Sub Contractor</th>
                                    <th>Date</th>
                                    <th>Bill Amount</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = 1;
                                foreach ($subconbill as $subbill) { ?>
                                    <tr>
                                        <td width="5%">
                                            <?php echo $i; ?>
                                        </td>
                                        <td width="20%">
                                            <?php echo $subbill["scquotation_decription"]; ?>
                                        </td>
                                        <td width="20%">
                                            <?php echo $subbill["bill_number"]; ?>
                                        </td>
                                        <?php
                                        $subcontractor = Subcontractor::model()->findByPk($subbill["subcontractor_id"]);
                                        ?>
                                        <td width="20%">
                                            <?php echo $subcontractor->subcontractor_name; ?>
                                        </td>
                                        <td width="15%">
                                            <?php echo $subbill["date"]; ?>
                                        </td>
                                        <td width="20%" class="text-right">
                                            <?php echo Controller::money_format_inr($subbill["total_amount"], 2); ?>
                                        </td>
                                    </tr>
                                    <?php
                                    $subbilltotal = $subbilltotal + $subbill["total_amount"];
                                    $i = $i + 1;
                                } ?>
                            </tbody>
                            <tfoot class="entry-table">
                                <tr>
                                    <th colspan="5" class="text-right">Total</th>
                                    <th class="text-right">
                                        <?php echo Controller::money_format_inr($subbilltotal, 2); ?>
                                    </th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                <?php } ?>
                <?php if (!empty($unbilledpo)) { ?>
                    <h4>Un billed Purchase Order</h4>
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead class="entry-table sticky-thead">
                                <tr>
                                    <th>Sl No</th>
                                    <th>Purchase Number</th>
                                    <th>Vendor</th>
                                    <th>Expense Head</th>
                                    <th>Date</th>
                                    <th>Amount</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = 1;
                                foreach ($unbilledpo as $unbill) { ?>
                                    <tr>
                                        <td width="5%">
                                            <?php echo $i; ?>
                                        </td>
                                        <td width="20%">
                                            <?php echo $unbill["purchase_no"]; ?>
                                        </td>
                                        <td width="20%">
                                            <?php echo $unbill["vname"]; ?>
                                        </td>
                                        <td width="20%">
                                            <?php echo $unbill["expensehead"]; ?>
                                        </td>
                                        <td width="15%">
                                            <?php echo $unbill["purchase_date"]; ?>
                                        </td>
                                        <td width="20%" class="text-right">
                                            <?php echo Controller::money_format_inr($unbill["unbilled_amount"], 2); ?>
                                        </td>
                                    </tr>
                                    <?php
                                    $unbilledtotal = $unbilledtotal + $unbill["unbilled_amount"];
                                    $i = $i + 1;
                                } ?>
                            </tbody>
                            <tfoot class="entry-table">
                                <tr>
                                    <th colspan="5" class="text-right">Total</th>
                                    <th class="text-right">
                                        <?php echo Controller::money_format_inr($unbilledtotal, 2); ?>
                                    </th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                <?php } ?>
                <?php
                $totalBillExpense = $totalBillAmt + $subbilltotal + $unbilledtotal;
                $balanceBillExpense = $totalBillExpense - $totalPayment;
                ?>
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th width="80%">Total Bill Amount</th>
                            <th class="text-right">
                                <?php echo Controller::money_format_inr(round($totalBillExpense), 2); ?>
                            </th>
                        </tr>
                    </thead>
                </table>
                <?php if (!empty($quotationdata)) { ?>
                    <h4>Subcontractor Quotation</h4>
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead class="entry-table sticky-thead">
                                <tr>
                                    <th width="5%">Sl No</th>
                                    <th width="20%">Subcontractor</th>
                                    <th width="40%">Description</th>
                                    <th width="15%">Date</th>
                                    <th width="20%">Quotation Amount</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = 1;
                                foreach ($quotationdata as $quotation) { ?>
                                    <tr>
                                        <td>
                                            <?php echo $i; ?>
                                        </td>
                                        <td>
                                            <?php echo $quotation["subcontractor_name"]; ?>
                                        </td>
                                        <td>
                                            <?php echo $quotation["scquotation_decription"]; ?>
                                        </td>
                                        <td>
                                            <?php echo $quotation["scquotation_date"]; ?>
                                        </td>
                                        <td class="text-right">
                                            <?php echo Controller::money_format_inr($quotation["scquotation_amount"], 2); ?>
                                        </td>
                                    </tr>
                                    <?php
                                    $i = $i + 1;
                                } ?>
                            </tbody>
                            <tfoot class="entry-table">
                                <tr>
                                    <th colspan="4" class="text-right">Total</th>
                                    <th class="text-right">
                                        <?php echo Controller::money_format_inr($quotationdata[0]["quotationsum"], 2); ?>
                                    </th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                <?php } ?>
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th width="80%">Total Subcontractor Quotation</th>
                            <th class="text-right">
                                <?php echo Controller::money_format_inr(round($totalQuotSum), 2); ?>
                            </th>
                        </tr>
                    </thead>
                </table>
                <?php if (!empty($daybookvendor)) { ?>
                    <div class="table-responsive">
                        <h4>Daybook Vendor Payment</h4>
                        <table class="table table-bordered">
                            <thead class="entry-table sticky-thead">
                                <tr>
                                    <th width="5%">Sl No</th>
                                    <th width="35%">Description</th>
                                    <th width="15%">Expense Head</th>
                                    <th width="15%">Date</th>
                                    <th width="15%">Receipt</th>
                                    <th width="15%">Paid</th>
                                </tr>
                            </thead>
                            <?php
                            $c = 1;
                            foreach ($daybookvendor as $vendor) {
                                if ($c != $vendor["vendorcount"]) {
                                    $c = $c + 1;
                                } else {
                                    $vendorCount[] = $vendor["vendorcount"];
                                    $c = 1;
                                }
                            }
                            $vendorCount = array_values($vendorCount);
                            $vendorNum = count($vendorCount);
                            $totalV = 0;
                            for ($i = 0; $i < $vendorNum; $i++) {
                                $vendorC = $vendorCount[$i];
                                for ($j = 0; $j < $vendorC; $j++) {
                                    $vCount = $daybookvendor[$j]["vendorcount"];
                                    if ($j == 0) {
                                        ?>
                                        <tr>
                                            <td colspan="6" width="100%"><b>
                                                    <?php echo $daybookvendor[$totalV]["vendor"]; ?>
                                                </b></td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                    <tr>
                                        <td width="5">
                                            <?php echo $j + 1; ?>
                                        </td>
                                        <td width="50">
                                            <?php echo $daybookvendor[$totalV]["description"]; ?>
                                        </td>
                                        <td width="50">
                                            <?php echo $daybookvendor[$totalV]["expensetype"]; ?>
                                        </td>
                                        <td width="15">
                                            <?php echo $daybookvendor[$totalV]["date"]; ?>
                                        </td>
                                        <td width="15"></td>
                                        <td width="15" class="text-right">
                                            <?php echo Controller::money_format_inr($daybookvendor[$totalV]["paid"], 2); ?>
                                        </td>
                                    </tr>
                                    <?php
                                    if ($vendorC == $j + 1) {
                                        ?>
                                        <tr>
                                            <th colspan="5" class="text-right"><b>Total</b></th>
                                            <th class="text-right"><b>
                                                    <?php echo Controller::money_format_inr(round($daybookvendor[$totalV]["vendorpaid"]), 2); ?>
                                                </b></th>
                                        </tr>
                                        <tr>
                                            <th colspan="5" class="text-right"><b>Balance To Be Paid</b></th>
                                            <th class="text-right"><b>
                                                    <?php echo Controller::money_format_inr(round($daybookvendor[$totalV]["vendorsubsum"] - $daybookvendor[$totalV]["vendorpaid"]), 2); ?>
                                                </b></th>
                                        </tr>
                                        <?php
                                    }
                                    $totalV = $totalV + 1;
                                }
                            }
                            ?>
                        </table>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th width="80%">Total Payment to Vendor</th>
                                    <th class="text-right">
                                        <?php echo Controller::money_format_inr(round($daybookVT), 2); ?>
                                    </th>
                                </tr>
                                <tr>
                                    <th width="80%">Balance to be Paid</th>
                                    <th class="text-right">
                                        <?php echo Controller::money_format_inr(round($totalBillAmt - $daybookVT), 2); ?>
                                    </th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                <?php } ?>
                <?php if (!empty($daybooksubcon)) { ?>
                    <h4>Subcontractor Payment</h4>
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead class="entry-table sticky-thead">
                                <tr>
                                    <th width="5%">Sl No</th>
                                    <th width="50%">Description</th>
                                    <th width="15%">Date</th>
                                    <th width="15%">Receipt</th>
                                    <th width="15%">Paid</th>
                                </tr>
                            </thead>
                            <?php
                            $c = 1;
                            foreach ($daybooksubcon as $subcon) {
                                if ($c != $subcon["subcontractorcount"]) {
                                    $c = $c + 1;
                                } else {
                                    $subconCount[] = $subcon["subcontractorcount"];
                                    $c = 1;
                                }
                            }
                            $subconCount = array_values($subconCount);
                            $subconNum = count($subconCount);
                            $totalS = 0;
                            for ($i = 0; $i < $subconNum; $i++) {
                                $subconC = $subconCount[$i];
                                for ($j = 0; $j < $subconC; $j++) {
                                    $sCount = $daybooksubcon[$j]["subcontractorcount"];
                                    if ($j == 0) {
                                        ?>
                                        <tr>
                                            <td colspan="5" width="100%"><b>
                                                    <?php echo $daybooksubcon[$totalS]["sname"]; ?>
                                                </b></td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                    <tr>
                                        <td width="5">
                                            <?php echo $j + 1; ?>
                                        </td>
                                        <td width="50">
                                            <?php echo $daybooksubcon[$totalS]["description"]; ?>
                                        </td>
                                        <td width="15">
                                            <?php echo $daybooksubcon[$totalS]["date"]; ?>
                                        </td>
                                        <td width="15"></td>
                                        <td width="15" class="text-right">
                                            <?php echo Controller::money_format_inr($daybooksubcon[$totalS]["paid"], 2); ?>
                                        </td>
                                    </tr>
                                    <?php
                                    if ($subconC == $j + 1) {
                                        ?>
                                        <tr>
                                            <th colspan="4" class="text-right"><b>Total</b></th>
                                            <th class="text-right"><b>
                                                    <?php echo Controller::money_format_inr(round($daybooksubcon[$totalS]["subcontractorpaid"]), 2); ?>
                                                </b></th>
                                        </tr>
                                        <?php
                                    }
                                    $totalS = $totalS + 1;
                                }
                            }
                            ?>
                        </table>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th width="80%">Total Payment to Subcontractor</th>
                                    <th class="text-right">
                                        <?php echo Controller::money_format_inr(round($daybooksubcon[0]["subcontractortotalpaid"]), 2); ?>
                                    </th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                <?php } ?>
                <?php if (!empty($vendorpayment)) { ?>
                    <h4>Bulk Vendor Payment</h4>
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead class="entry-table sticky-thead">
                                <tr>
                                    <th width="5%">Sl No</th>
                                    <th width="50%">Description</th>
                                    <th width="15%">Date</th>
                                    <th width="15%">Receipt</th>
                                    <th width="15%">Paid</th>
                                </tr>
                            </thead>
                            <?php
                            $c = 1;
                            foreach ($vendorpayment as $vendor) {
                                if ($c != $vendor["vendorcount"]) {
                                    $c = $c + 1;
                                } else {
                                    $vendorCount1[] = $vendor["vendorcount"];
                                    $c = 1;
                                }
                            }
                            $vendorCount1 = array_values($vendorCount1);
                            $vendorNum1 = count($vendorCount1);
                            $totalV1 = 0;
                            for ($i = 0; $i < $vendorNum1; $i++) {
                                $vendorC1 = $vendorCount1[$i];
                                for ($j = 0; $j < $vendorC1; $j++) {
                                    $vCount1 = $vendorpayment[$j]["vendorcount"];
                                    if ($j == 0) {
                                        ?>
                                        <tr>
                                            <td colspan="5" width="100%"><b>
                                                    <?php echo $vendorpayment[$totalV1]["vendor"]; ?>
                                                </b></td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                    <tr>
                                        <td width="5">
                                            <?php echo $j + 1; ?>
                                        </td>
                                        <td width="50">
                                            <?php echo $vendorpayment[$totalV1]["description"]; ?>
                                        </td>
                                        <td width="15">
                                            <?php echo $vendorpayment[$totalV1]["date"]; ?>
                                        </td>
                                        <td width="15"></td>
                                        <td width="15" class="text-right">
                                            <?php echo Controller::money_format_inr($vendorpayment[$totalV1]["amount"] + $vendorpayment[$totalV1]["tax_amount"], 2); ?>
                                        </td>
                                    </tr>
                                    <?php
                                    if ($vendorC1 == $j + 1) {
                                        ?>
                                        <tr>
                                            <th colspan="4" class="text-right"><b>Total</b></th>
                                            <th class="text-right"><b>
                                                    <?php echo Controller::money_format_inr(round($vendorpayment[$totalV1]["vendorsum"]), 2); ?>
                                                </b></th>
                                        </tr>
                                        <?php
                                    }
                                    $totalV1 = $totalV1 + 1;
                                }
                            }
                            ?>
                        </table>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th width="80%">Total Payment to Vendor</th>
                                    <th class="text-right">
                                        <?php echo Controller::money_format_inr(round($vendorpayment[0]["vendortotalsum"]), 2); ?>
                                    </th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                <?php } ?>
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th width="80%">Total Payment</th>
                            <th class="text-right">
                                <?php echo Controller::money_format_inr(round($totalPayment), 2); ?>
                            </th>
                        </tr>
                    </thead>
                </table>
                <table class="table table-bordered">
                    <tr>
                        <td width="80%">TOTAL INVOICE</td>
                        <td class="text-right">
                            <?php echo Controller::money_format_inr(round($totalInvoice), 2); ?>
                        </td>
                    </tr>
                    <tr>
                        <td width="80%">AMOUNT RECEIVED</td>
                        <td class="text-right">
                            <?php echo Controller::money_format_inr(round($invoiceReceipt), 2); ?>
                        </td>
                    </tr>
                    <tr>
                        <td width="80%">BALANCE TO BE RECEIVED</td>
                        <td class="text-right">
                            <?php echo Controller::money_format_inr(round($balanceInvoice), 2); ?>
                        </td>
                    </tr>
                    <tr>
                        <td width="80%">TOTAL EXPENSE</td>
                        <td class="text-right">
                            <?php echo Controller::money_format_inr(round($totalBillExpense), 2); ?>
                        </td>
                    </tr>
                    <tr>
                        <td width="80%">AMOUNT PAID</td>
                        <td class="text-right">
                            <?php echo Controller::money_format_inr(round($totalPayment), 2); ?>
                        </td>
                    </tr>
                    <tr>
                        <td width="80%">BALANCE EXPENSE TO BE PAID</td>
                        <td class="text-right">
                            <?php echo Controller::money_format_inr(round($balanceBillExpense), 2); ?>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <?php
    } else { ?>
        <div id="contenthtml" class="contentdiv">
            <?php
            $this->widget('zii.widgets.CListView', array(
                'dataProvider' => $model->search(),
                'itemView' => '_newexpenselistview',
                'template' => '<div>{summary}</div><div class="table-responsive" id="parent"><table class="table" id="fixTable">{items}</table></div>',
            ));
            ?>
        </div>

    <?php } ?>
</div>

<script>
    $(document).ready(function () {
        $("#date_from").datepicker({
            dateFormat: 'dd-M-yy',
            maxDate: $("#date_to").val()
        });
        $("#date_to").datepicker({
            dateFormat: 'dd-M-yy',
        });
        $("#date_from").change(function () {
            $("#date_to").datepicker('option', 'minDate', $(this).val());
        });
        $("#date_to").change(function () {
            $("#date_from").datepicker('option', 'maxDate', $(this).val());
        });
    });
    /*(function () {
        'use strict';
        var container = document.querySelector('.container1');
        var table = document.querySelector('table');
        var leftHeaders = [].concat.apply([], document.querySelectorAll('tbody th'));
        var topHeaders = [].concat.apply([], document.querySelectorAll('thead th'));
        var topLeft = document.createElement('div');
        var computed = window.getComputedStyle(topHeaders[0]);
        container.appendChild(topLeft);
        topLeft.classList.add('top-left');
        topLeft.style.width = computed.width;
        topLeft.style.height = computed.height;
 
        container.addEventListener('scroll', function (e) {
            var x = container.scrollLeft;
            var y = container.scrollTop;
 
            leftHeaders.forEach(function (leftHeader) {
                leftHeader.style.transform = translate(x, 0);
            });
            topHeaders.forEach(function (topHeader, i) {
                if (i === 0) {
                    topHeader.style.transform = translate(0, y);
                } else {
                    topHeader.style.transform = translate(0, y);
                }
            });
            topLeft.style.transform = translate(x, y);
        });
 
        function translate(x, y) {
            return 'translate(' + x + 'px, ' + y + 'px)';
        }
    })();*/
    $(document).ready(function () {
        $("#fixTable").tableHeadFixer({
            'left': false,
            'foot': false,
            'head': true
        });
    });
</script>
<style>
    #parent {
        max-height: 300px;
    }
</style>