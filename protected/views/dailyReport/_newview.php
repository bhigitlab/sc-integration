<?php
$tblpx = Yii::app()->db->tablePrefix;
$pms_api_integration_model = ApiSettings::model()->findByPk(1);
$pms_api_integration = $pms_api_integration_model->api_integration_settings;
if ($index == 0) {
?>
<style>
    .legend {
        list-style: none;
        padding-left: 0px;

    }

    .legend li {
        float: left;
        margin-right: 10px;
    }

    .legend span {
        border: 1px solid #ccc;
        float: left;
        width: 12px;
        height: 12px;
        margin: 2px;
    }

    .legend .project_not_mapped {
        background-color: #A9E9EC;
        border: 2px solid #f8cbcb;
        margin-right: 4px;
    }
</style>
<?php if ($pms_api_integration == 1): ?>
    <ul class="legend">
        <li><span class="project_not_mapped"></span> Project/PMS Template Not Mapped In Integration</li>
    </ul>

<?php endif; ?>
    <thead class="entry-table">
        <tr>
            <th></th>
            <th>#ID</th>
            <th>Company</th>
            <th>Project</th>
            <th>Subcontractor</th>
            <th>Expense Head</th>
            <th>Description</th>
            <th>Requested From</th>
            <th>Regular/Over Time</th>
            <th>status</th>
            <th>Amount</th>
            
        </tr>
    </thead>
<?php } ?>
<?php

$bgcolor = '';
$newbgclass='';

$approval_status = $data["approval_status"];
$id = 'dr_id';
$tbl =$this->tableNameAcc('pre_dailyreport', 0);
$modelName= 'PreDailyreport';

$final = Yii::app()->controller->actiongetDailyReportFinalData($data, $approval_status,$id,$tbl,$modelName);

$data = $final['data'];
$approval_status = $final['approval_status'];
$count =$final['count'];
$approve_status=$data['approve_status'] ;
if ($data['approve_status'] == 2 || $data['approve_status'] == 4) {
    $newbgclass = 'row_class';
}
$expenseId      = $data["dr_id"];
$deleteconfirm  = Deletepending::model()->find(array("condition" => "deletepending_parentid = " . $expenseId . " AND deletepending_status = 0 AND deletepending_table = '{$tblpx}dailyreport'"));
$updateconfirm  = Editrequest::model()->find(array("condition" => "parent_id = " . $expenseId . " AND editrequest_status = 0 AND editrequest_table = '{$tblpx}dailyreport'"));
$confirmcount   = !empty($deleteconfirm) ? count($deleteconfirm->attributes) : 0;
$updatecount    = !empty($updateconfirm) ? count($updateconfirm->attributes) : 0;
if ($confirmcount > 0 || $updatecount > 0) {
    $deleteclass = "deleteclass";
    $deletetitle = "Waiting for delete confirmation from admin.";
} else {
    $deleteclass = "";
    $deletetitle = "";
}

?>
<?php
    if ($pms_api_integration == 1){
        if($data['request_from'] ==2){
            if($data['pms_project_id'] ==0||$data['pms_labour_template']==0){
                $newbgclass = 'row_class_mapped';
             }
        }
        
    }
 ?>
<tr id="<?php echo  $data['dr_id'] ?>" class="<?php echo  $deleteclass." ". (($approve_status == 2 || $approve_status == 4) ? $newbgclass : ''); ?>"  title="<?php echo $deletetitle; ?>" data-status="<?php echo $data["approval_status"] ?>" data-id="<?php echo ($data["approval_status"] == 0) ? $data['ref_id'] : $data['dr_id']; ?>">
<td>
        <?php

        if ($updatecount > 0) {
            $requestmessage = "Already an update request is pending";
        ?>
            <span class="icon icon-options-vertical popover-test" data-toggle="popover" data-placement="right" type="button" data-html="true" style="cursor: pointer;"></span>
            <div class="popover-content hide">
                <ul class="tooltip-hiden">
                    <li><span id="<?php echo $index; ?>" class="pending-icon fa fa-question" title="<?php echo $requestmessage; ?>"></span></li>
                </ul>
            </div>
            <?php
        } else {
            $payDate    = date("Y-m-d", strtotime($data["date"]));
            $company    = Company::model()->findByPk($data["company_id"]);
            $updateDays='';
            if(isset($company)){
                $updateDays = ($company->company_updateduration) - 1;
            }
            $editDate   = date('Y-m-d ', strtotime($payDate . ' + ' . $updateDays . ' days'));
            $addedDate   = date("Y-m-d 23:59:59", strtotime($data["date"]));
            $currentDate = date("Y-m-d");
            if (Yii::app()->user->role == 1) {
            ?>
                <span class="icon icon-options-vertical popover-test" data-toggle="popover" data-placement="right" type="button" data-html="true" style="cursor: pointer;"></span>
                <div class="popover-content hide">
                    <ul class="tooltip-hiden">
                        <?php
                        if ((in_array('/dailyReport/updateDailyentry', Yii::app()->user->menuauthlist))) {
                            
                            if ($data['approve_status'] != 1) {
                        ?>
                            <li><button id="<?php echo $index; ?>" class="btn btn-xs btn-default row-daybook margin-right-5">Edit</button></li>
                        <?php } } ?>
                       
                        <li><button class="click btn btn-xs btn-default margin-right-5" id="<?php echo $data['dr_id']; ?>" href="#"
                            onClick="view('<?php echo $data['dr_id']; ?>')" title="Update">View</button></li>
                       
                        <?php
                        if ((in_array('/dailyReport/deletereport', Yii::app()->user->menuauthlist))) {
                            if ($data['request_from'] != 2 ) {
                        ?>
                            <li><button id="<?php echo $index; ?>" class="btn btn-xs btn-default delete_row margin-right-5">Delete</button></li>
                        <?php }
                          } ?>

                        <?php
                        if ($data['approve_status'] == 2 ) {
                            if ((in_array('/dailyReport/approve', Yii::app()->user->menuauthlist))) {
                        ?>
                                <li><button id="<?php echo $data['dr_id']; ?>" data-id="<?php echo $data['dr_id']; ?>"class="btn btn-xs btn-default approve margin-right-5  approveoption_<?php echo $data['dr_id']; ?>">Approve</button></li>

                                <li><a class="click btn btn-xs btn-default margin-right-5" id="<?php echo $data['dr_id']; ?>" href="#" onClick="openRejectModal('<?php echo $data['dr_id']; ?>')" title="Reject">Reject</a></li>
                        <?php }
                        } ?>
                    </ul>
                </div>
                <?php
                // }
            } else {
                if (isset($company->company_updateduration) && !empty($company->company_updateduration)) {
                    if ($editDate >= $currentDate) {
                ?>
                        <span class="icon icon-options-vertical popover-test " data-toggle="popover" data-placement="right" type="button" data-html="true" style="cursor: pointer;"></span>
                        <div class="popover-content hide">
                            <ul class="tooltip-hiden">
                                <li><button id="<?php echo $index; ?>" class="btn btn-xs btn-default row-daybook margin-right-5">Edit</button></li>
                                <?php if ($confirmcount == 0) { ?>
                                    <li><button id="<?php echo $index; ?>" class=" margin-right-5 btn btn-xs btn-default delete_confirmation">Delete</button></li>
                                <?php } else { ?>
                                    <li><button id="<?php echo $index; ?>" data-id="<?php echo $deleteconfirm["deletepending_id"]; ?>" class="btn btn-xs btn-default delete_restore margin-right-5">Restore</button></li>
                                <?php } ?>
                                <?php
                        if ($data['approve_status'] == 2) {
                            if ((in_array('/dailyReport/approve', Yii::app()->user->menuauthlist))) {
                        ?>
                                <li><button id="<?php echo $data['dr_id']; ?>" class="btn btn-xs btn-default approve margin-right-5 approveoption_<?php echo $data['dr_id']; ?>">Approve</button></li>
                        <?php }
                        } ?>
                            </ul>
                        </div>
                    <?php
                    }
                } else {
                    if ($currentDate <= $addedDate) {
                    ?>
                        <span class="icon icon-options-vertical popover-test" data-toggle="popover" data-placement="right" type="button" data-html="true" style="cursor: pointer;"></span>
                        <div class="popover-content hide">
                            <ul class="tooltip-hiden">
                                <?php
                                if ((in_array('/dailyReport/updateDailyentry', Yii::app()->user->menuauthlist))) {
                                ?>
                                    <li><button id="<?php echo $index; ?>" class="btn btn-xs btn-default row-daybook margin-right-5">Edit</button></li>
                                <?php } ?>
                                <?php
                                if ((in_array('/dailyReport/deletereport', Yii::app()->user->menuauthlist))) {
                                ?>
                                    <li><button id="<?php echo $index; ?>" class="btn btn-xs btn-default delete_row margin-right-5">Delete</button></li>
                                <?php } ?>
                                <?php
                                    if ($data['approve_status'] == 2) {
                                        if ((in_array('/dailyReport/approve', Yii::app()->user->menuauthlist))) {
                                    ?>
                                            <li><button id="<?php echo $data['dr_id']; ?>" class="btn btn-xs btn-default approve margin-right-5 approveoption_<?php echo $data['dr_id']; ?>">Approve</button></li>
                                    <?php }
                                    } ?>
                            </ul>
                        </div>
        <?php
                    }
                }
            }
        } ?>
    </td>    
    <td><b>#<?php echo ($data["approval_status"] == 0) ? $data['ref_id'] : $data['dr_id']; ?></b></td>
    <td>
        <?php
        $company = Company::model()->findByPk($data["company_id"]);
        if(isset($company)){
            echo $company->name;
        }
        ?>
    </td>
    <td><?php echo $data['pname']; ?></td>
    <td><?php echo $data['subname']?$data['subname']:'No Subcontractor' ; ?></td>
    <td><?php echo $data['type_name']; ?></td>
    <td><?php echo $data['description']; ?></td>
    <td style="white-space:nowrap;">
        <?php 
        if($data['request_from'] == 1){
            echo CHtml::encode( 'ACCOUNTS');
        }else {
            echo CHtml::encode( 'PMS');
        } ?>
    </td>
     <td style="white-space:nowrap;">
        <?php 
        if($data['overtime'] == 1){
            echo CHtml::encode( 'Overtime');
        }else {
            echo CHtml::encode( 'Regular');
        } ?>
    </td>
    <td style="white-space:nowrap;">
        <?php 
        if($data['approve_status'] == 2){
            echo CHtml::encode( 'Pending');
        }else if($data['approve_status'] == 1){
            echo CHtml::encode( 'Approved');
        }else if($data['approve_status'] == 3){
            echo CHtml::encode( 'Approved');
        }
        else if($data['approve_status'] == 4){
            echo CHtml::encode( 'Pending');
        }
        else if($data['approve_status'] == 5){
            echo CHtml::encode( 'Rejected');
        }
             ?>
    </td>
    <td class="text-right"><span class="popover-test tax_amnt" data-toggle="popover" data-trigger="hover" data-placement="left" data-trigger="hover" type="button" data-html="true"><?php echo $data['amount'] ? Controller::money_format_inr($data['amount'], 2) : ""; ?></span>
        <div class="popover-content hide">
            <?php
            $model2 = ExpenseType::model()->findByPk($data['expensehead_id']);
            ?>
            <?php
            if (isset($model2->labour_status) && $model2->labour_label != '' && $model2->labour_status == 1) {
                $head_array[] = $model2->labour_label;
                echo "<div><span>" . $model2->labour_label . ": " . $data['labour'] . "</span></div>";
            }
            if (isset($model2->wage_label) && $model2->wage_label != '' && $model2->wage_status == 1) {
                echo "<div><span>" . $model2->wage_label . ": " . $data['wage'] . "</span></div>";
            }

            if (isset($model2->wage_rate_label) && $model2->wage_rate_label != '' && $model2->wagerate_status == 1) {
                echo "<div><span>" . $model2->wage_rate_label . ": " . $data['wage_rate'] . "</span></div>";
            }

            if (isset($model2->labour_wage_label) && $model2->labour_wage_label != '' && $model2->labour_wage_status == 1) {
                echo "<div><span>" . $model2->labour_wage_label . ": " . $data['labour_wage'] . "</span></div>";
            }
            if (isset($model2->helper_label) && $model2->helper_label != '' && $model2->helper_status == 1) {
                echo "<div><span>" . $model2->helper_label . ": " . $data['helper'] . "</span></div>";
            }
            if (isset($model2->helper_labour_label) && $model2->helper_labour_label != '' && $model2->helperlabour_status == 1) {
                echo "<div><span>" . $model2->helper_labour_label . ": " . $data['helper_labour'] . "</span></div>";
            }

            if (isset($model2->helper_wage_label) && $model2->helper_wage_label != '' && $model2->helper_wage_status == 1) {
                echo "<div><span>" . $model2->helper_wage_label . ": " . $data['helper_wage'] . "</span></div>";
            }
            if (isset($model2->lump_sum_label) && $model2->lump_sum_label != '' && $model2->lump_sum_status == 1) {
                echo "<div><span>" . $model2->lump_sum_label . ": " . $data['lump_sum'] . "</span></div>";
            }

            ?>
        </div>
    </td>
   
    <input type="hidden" name="reportid[]" id="dlyreportid<?php echo $index; ?>" value="<?php echo $data["dr_id"]; ?>" />
</tr>
<?php
if ($index == 0) {
?>
    <tfoot>
        <tr>
            <th></th>
            <th class="total_amnt"></th>
            <th colspan="8" class="text-right total_amnt">Total Amount:</th>
            <th class="text-right"><?php echo Controller::money_format_inr($total_amount, 2); ?></th>            
        </tr>
    </tfoot>
<?php }
?>

<style>
    /* .approve_section {
        background: #f8cbcb !important;
    } */
    .row_class{
        background-color: #f8cbcb !important;
    }
    .row_class_mapped{
        background-color:#A9E9EC !important;
    }
</style>