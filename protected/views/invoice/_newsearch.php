<?php
/* @var $this ProjectsController */
/* @var $model Projects */
/* @var $form CActiveForm */
?>


<div class="page_filter clearfix">

    <?php $form = $this->beginWidget('CActiveForm', array(
        'action' => Yii::app()->createUrl($this->route),
        'method' => 'get',
    )); ?>
    <div class="row">
        <div class="form-group col-xs-12 col-sm-3 col-md-2">
            <label>Project </label>
            <?php
            $user = Users::model()->findByPk(Yii::app()->user->id);
            $arrVal = explode(',', $user->company_id);
            $newQuery = "";
            foreach ($arrVal as $arr) {
                if ($newQuery)
                    $newQuery .= ' OR';
                $newQuery .= " FIND_IN_SET('" . $arr . "', company_id)";
            }
            echo $form->dropDownList($model, 'project_id', CHtml::listData(Projects::model()->findAll(array(
                'select' => array('pid, name'),
                'order' => 'name',
                'condition' => '(' . $newQuery . ')',
                'distinct' => true
            )), 'pid', 'name'), array('class' => 'form-control js-example-basic-single', 'empty' => 'Select project', 'id' => 'projectid'));
            ?>
        </div>
        <div class="form-group col-xs-12 col-sm-3 col-md-2">
            <label class="">From</label>
            <?php echo CHtml::activeTextField($model, 'fromdate', array('class' => 'form-control', 'placeholder' => 'Date From', 'size' => 10, 'autocomplete' => 'off', 'readonly' => true, 'value' => isset($_REQUEST['Invoice']['fromdate']) ? $_REQUEST['Invoice']['fromdate'] : '')); ?>
        </div>
        <div class="form-group col-xs-12 col-sm-3 col-md-2 ">
            <label class="">To</label>
            <?php echo CHtml::activeTextField($model, 'todate', array('class' => 'form-control', 'placeholder' => 'Date To', 'size' => 10, 'autocomplete' => 'off', 'readonly' => true, 'value' => isset($_REQUEST['Invoice']['todate']) ? $_REQUEST['Invoice']['todate'] : '')); ?>
        </div>
        <div class="form-group col-xs-12 col-sm-3 col-md-2 text-sm-left text-right">
            <label class="d-sm-block d-none">&nbsp;</label>
            <div>
                <?php echo CHtml::submitButton('Go', array('class' => 'btn btn-sm btn-primary')); ?>
                <?php echo CHtml::resetButton('Clear', array('onclick' => 'javascript:location.href="' . $this->createUrl('admin') . '"', 'class' => 'btn btn-sm btn-default')); ?>
            </div>
        </div>
        <?php $this->endWidget(); ?>
    </div>
</div>
<script>
    $(function () {
        $("#Invoice_fromdate").datepicker({
            dateFormat: 'dd-mm-yy'
        });
        $("#Invoice_todate").datepicker({
            dateFormat: 'dd-mm-yy'
        });
        $("#Invoice_fromdate").change(function () {
            $("#Invoice_todate").datepicker('option', 'minDate', $(this).val());
        });
        $("#Invoice_todate").change(function () {
            $("#Invoice_fromdate").datepicker('option', 'maxDate', $(this).val());
        });

    });


</script>