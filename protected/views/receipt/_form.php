<?php
/* @var $this ReceiptController */
/* @var $model Receipt */
/* @var $form CActiveForm */
?>

<div class="modal-body">

<?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'receipt-form',
        'enableAjaxValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true,
            'validateOnType' => false,),
    ));
 ?>
    <div class="clearfix">
        <div class="row addRow">	
            <div class="col-md-12">
                    <?php echo $form->labelEx($model,'project_id'); ?>
                    <?php echo $form->dropDownList($model, 'project_id', CHtml::listData(Projects::model()->findAll(array(
                                                'select' => array('pid, name'),
                                                "condition" => "pid in (select projectid from jp_project_assign)",
                                                'order' => 'name',
                                                'distinct' => true
                                            )), 'pid', 'name'), array('empty' => '-----------')); ?>
                    <?php echo $form->error($model,'project_id'); ?>
            </div>

            <div class="col-md-6">
                    <?php echo $form->labelEx($model,'receipt_amount'); ?>
                    <?php echo $form->textField($model,'receipt_amount'); ?>
                    <?php echo $form->error($model,'receipt_amount'); ?>
            </div>

            <div class="col-md-6">
                    <?php echo $form->labelEx($model,'date'); ?>
                    <?php echo CHtml::activeTextField($model, 'date', array('size' => 10, 'value' => (($model->isNewRecord) ? date('d-M-Y') : date('d-M-Y', strtotime($model->date))))); ?>
                    <?php
                    $this->widget('application.extensions.calendar.SCalendar', array(
                        'inputField' => 'Receipt_date',
                        'ifFormat' => '%d-%b-%Y',
                    ));
                    ?>
                    <?php echo $form->error($model,'date'); ?>
            </div>

            <div class="col-md-12">
                    <?php echo $form->labelEx($model,'description'); ?>
                    <?php echo $form->textArea($model, 'description', array('rows' => 5, 'cols' => 40)); ?>
                    <?php echo $form->error($model,'description'); ?>
            </div>

            <div class="col-md-12">
                    <?php echo $form->labelEx($model,'payment_type'); ?>
                    <div class="radio_btn">
                        <?php
                        echo $form->radioButtonList($model, 'payment_type', CHtml::listData(Status::model()->findAll(
                                                array(
                                                    'select' => array('sid,caption'),
                                                    'condition' => 'status_type="payment_type"',
                                                    'order' => 'caption',
                                                    'distinct' => true
                                        )), 'sid', 'caption'), array('separator' => ''));
                        ?>
                    </div>
                    <?php echo $form->error($model,'payment_type'); ?>
            </div>
        </div>
            <div class="modal-footer save-btnHold">
                <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?> 
                <?php if (Yii::app()->user->role == 1) {
                	if (!$model->isNewRecord){
                	/*
                	 ?> 
                    <a class="btn del-btn" href="<?php echo $this->createUrl('projects/deleteprojects', array('id' => $model->rec_id)) ?>" class="deletebtn" onclick="return confirm('Are you sure that you want to delete?');">Delete </a>                           
                <?php */  }} ?>
                <button data-dismiss="modal">Close</button>
            </div>
        
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->