<?php

/**
 * This is the model class for table "{{scbillitems}}".
 *
 * The followings are the available columns in table '{{scbillitems}}':
 * @property integer $id
 * @property integer $scquotation_id
 * @property integer $item_category_id
 * @property integer $item_id
 * @property integer $item_type
 * @property string $item_description
 * @property integer $item_quantity
 * @property double $item_rate
 * @property integer $executed_quantity
 * @property double $item_amount
 * @property integer $created_by
 * @property string $created_date
 * @property double $completed_percentage
 * @property string $bill_number
 */
class Scbillitems extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{scbillitems}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('item_category_id, item_id, item_type, item_description, item_amount, created_by, created_date, completed_percentage, bill_number', 'required'),
            array('scquotation_id, item_category_id, item_id, item_type, created_by', 'numerical', 'integerOnly'=>true),
            array('item_rate, item_amount, completed_percentage', 'numerical'),
            array('bill_number', 'length', 'max'=>100),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, scquotation_id, item_category_id, item_id, item_type, item_description, item_quantity, item_rate, executed_quantity, item_amount, created_by, created_date, completed_percentage, bill_number', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'scquotation_id' => 'Scquotation',
            'item_category_id' => 'Item Category',
            'item_id' => 'Item',
            'item_type' => 'Item Type',
            'item_description' => 'Item Description',
            'item_quantity' => 'Item Quantity',
            'item_rate' => 'Item Rate',
            'executed_quantity' => 'Executed Quantity',
            'item_amount' => 'Item Amount',
            'created_by' => 'Created By',
            'created_date' => 'Created Date',
            'completed_percentage' => 'Completed Percentage',
            'bill_number' => 'Bill Number',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('scquotation_id',$this->scquotation_id);
        $criteria->compare('item_category_id',$this->item_category_id);
        $criteria->compare('item_id',$this->item_id);
        $criteria->compare('item_type',$this->item_type);
        $criteria->compare('item_description',$this->item_description,true);
        $criteria->compare('item_quantity',$this->item_quantity);
        $criteria->compare('item_rate',$this->item_rate);
        $criteria->compare('executed_quantity',$this->executed_quantity);
        $criteria->compare('item_amount',$this->item_amount);
        $criteria->compare('created_by',$this->created_by);
        $criteria->compare('created_date',$this->created_date,true);
        $criteria->compare('completed_percentage',$this->completed_percentage);
        $criteria->compare('bill_number',$this->bill_number,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Scbillitems the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}