<?php
$tblpx = Yii::app()->db->tablePrefix;

$user = Users::model()->findByPk(Yii::app()->user->id);
$arrVal = explode(',', $user->company_id);
$newQuery = "";
$newQuery1 = "";
$newQuery2 = "";
$newQuery3 = "";
$newQuery4 = "";
$newQuery5 = "";
foreach($arrVal as $arr) {
    if ($newQuery) $newQuery .= ' OR';
    $newQuery .= " FIND_IN_SET('".$arr."', id)";
}

if($company_id == ''){
    foreach($arrVal as $arr) {
        if ($newQuery1) $newQuery1 .= ' OR';
        if ($newQuery2) $newQuery2 .= ' OR';
        if ($newQuery3) $newQuery3 .= ' OR';
        if ($newQuery4) $newQuery4 .= ' OR';
        if ($newQuery5) $newQuery5 .= ' OR';
        $newQuery1 .= " FIND_IN_SET('".$arr."', {$tblpx}cashbalance.company_id)";
        $newQuery2 .= " FIND_IN_SET('".$arr."', company_id)";
        $newQuery3 .= " FIND_IN_SET('".$arr."', {$tblpx}expenses.company_id)";
        $newQuery4 .= " FIND_IN_SET('".$arr."', {$tblpx}dailyvendors.company_id)";
        $newQuery5 .= " FIND_IN_SET('".$arr."', {$tblpx}subcontractor_payment.company_id)";
    }
}else{
    $newQuery1 .= " FIND_IN_SET('".$company_id."', {$tblpx}cashbalance.company_id)";
    $newQuery2 .= " FIND_IN_SET('".$company_id."', company_id)";
    $newQuery3 .= " FIND_IN_SET('".$company_id."', {$tblpx}expenses.company_id)";
    $newQuery4 .= " FIND_IN_SET('".$company_id."', {$tblpx}dailyvendors.company_id)";
    $newQuery5 .= " FIND_IN_SET('".$company_id."', {$tblpx}subcontractor_payment.company_id)";
}
?>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/pdfstyle.css">

<?php
    $company_address = Company::model()->findBypk(($company_id)?$company_id:Yii::app()->user->company_id);
?>
<h3 class="text-center">Daily Financial Detailed Report</h3>
<div class="container-fluid pdf_spacing">


                <table border="1" class="table table-bordered ">

                    <tr>
                        <td colspan="2" class="whiteborder">&nbsp;</td>
                    </tr>
                    <tr>
                        <td><b>Opening Balance</b></td>
                        <td align="right"></td>

                    </tr>

                    <?php
                    $current_date = date('Y-m-d', strtotime('-1 day', strtotime($date)));
                    $cash_blance = Yii::app()->db->createCommand("SELECT {$tblpx}status.caption,{$tblpx}cashbalance.cashbalance_type,{$tblpx}cashbalance.bank_id,{$tblpx}bank.bank_name,{$tblpx}cashbalance.cashbalance_opening_balance,{$tblpx}cashbalance.cashbalance_deposit,{$tblpx}cashbalance.cashbalance_withdrawal,{$tblpx}cashbalance.cashbalance_closing_balance,{$tblpx}cashbalance.cashbalance_date,{$tblpx}cashbalance.company_id FROM {$tblpx}cashbalance LEFT JOIN {$tblpx}bank ON {$tblpx}cashbalance.bank_id={$tblpx}bank.bank_id LEFT JOIN {$tblpx}status ON {$tblpx}cashbalance.cashbalance_type={$tblpx}status.sid WHERE (".$newQuery1.") ORDER BY bank_id ASC")->queryAll();
                    foreach ($cash_blance as $key => $value) {


                        if($value['bank_id'] !=''){
                            //recepit
                            $daybook=  Yii::app()->db->createCommand("SELECT SUM(receipt) as total_amount FROM {$tblpx}expenses WHERE reconciliation_status = 1 AND reconciliation_date BETWEEN '".$value['cashbalance_date']."' AND '".$current_date."' AND payment_type=88 AND bank_id=".$value['bank_id']." AND type=72 AND company_id=".$value['company_id']."")->queryRow();
                            $dailyexpense=  Yii::app()->db->createCommand("SELECT SUM(dailyexpense_receipt) as total_amount FROM {$tblpx}dailyexpense WHERE reconciliation_status = 1 AND reconciliation_date BETWEEN '".$value['cashbalance_date']."' AND '".$current_date."' AND dailyexpense_receipt_type=88 AND bank_id=".$value['bank_id']." AND exp_type=72 AND company_id=".$value['company_id']."")->queryRow();
                            $daybook_check = $daybook['total_amount'];
                            $dailyexpense_check = $dailyexpense['total_amount'];
                            //$deposit = $daybook_check+$dailyexpense_check+$value['cashbalance_opening_balance'];
                            $deposit = $daybook_check+$dailyexpense_check;

                            //expense
                            $daybook_with=  Yii::app()->db->createCommand("SELECT SUM(paid) as total_amount FROM {$tblpx}expenses WHERE reconciliation_status = 1 AND reconciliation_date BETWEEN '".$value['cashbalance_date']."' AND '".$current_date."' AND expense_type=88 AND bank_id=".$value['bank_id']." AND type=73 AND company_id=".$value['company_id']."")->queryRow();
                            $dailyexpense_with=  Yii::app()->db->createCommand("SELECT SUM(dailyexpense_paidamount) as total_amount FROM {$tblpx}dailyexpense WHERE reconciliation_status = 1 AND reconciliation_date BETWEEN '".$value['cashbalance_date']."' AND '".$current_date."' AND expense_type=88 AND bank_id=".$value['bank_id']." AND exp_type=73 AND company_id=".$value['company_id']."")->queryRow();
                            $daybook_with_amount = $daybook_with['total_amount'];
                            $dailyexpense_with_amount = $dailyexpense_with['total_amount'];
                            $dailyvendors=  Yii::app()->db->createCommand("SELECT SUM( IFNULL(amount, 0) + IFNULL(tax_amount, 0) ) as total_amount FROM {$tblpx}dailyvendors WHERE reconciliation_status = 1 AND reconciliation_date BETWEEN '".$value['cashbalance_date']."' AND '".$current_date."' AND payment_type=88  AND bank=".$value['bank_id']." AND company_id=".$value['company_id']."")->queryRow();
                            $subcontractor=  Yii::app()->db->createCommand("SELECT SUM( IFNULL(amount, 0) + IFNULL(tax_amount, 0) ) as total_amount FROM {$tblpx}subcontractor_payment WHERE reconciliation_status = 1 AND reconciliation_date BETWEEN '".$value['cashbalance_date']."' AND '".$current_date."' AND payment_type=88  AND bank=".$value['bank_id']." AND company_id=".$value['company_id']."")->queryRow();
                            $dailyvendors_check = $dailyvendors['total_amount'];
                            $subcontractor_check = $subcontractor['total_amount'];
                            $withdrawal = $dailyvendors_check+$daybook_with_amount+$dailyexpense_with_amount;

                        } else {
                            //receipt
                            $daybook_cah=  Yii::app()->db->createCommand("SELECT SUM(receipt) as total_amount FROM {$tblpx}expenses WHERE date BETWEEN '".$value['cashbalance_date']."' AND '".$current_date."' AND payment_type=89 AND type=72 AND company_id=".$value['company_id']."")->queryRow();
                            $daybook_cash = $daybook_cah['total_amount'];
                            //$dailyexpense_cah=  Yii::app()->db->createCommand("SELECT SUM(dailyexpense_receipt) as total_amount FROM {$tblpx}dailyexpense WHERE date BETWEEN '".$value['cashbalance_date']."' AND '".$current_date."' AND (dailyexpense_receipt_type=89 OR dailyexpense_receipt_type=103) AND exp_type=72 AND company_id=".$value['company_id']."")->queryRow();
                            $dailyexpense_cah=  Yii::app()->db->createCommand("SELECT SUM(dailyexpense_receipt) as total_amount FROM {$tblpx}dailyexpense WHERE date BETWEEN '".$value['cashbalance_date']."' AND '".$current_date."' AND (dailyexpense_receipt_type=89) AND exp_type=72 AND company_id=".$value['company_id']."")->queryRow();
                            $dailyexpense_cash = $dailyexpense_cah['total_amount'];
                            //$deposit = $daybook_cash+ $dailyexpense_cash+$value['cashbalance_opening_balance'];
                            $deposit = $daybook_cash+ $dailyexpense_cash;


                            // expense
                            $daybook_with=  Yii::app()->db->createCommand("SELECT SUM(paid) as total_amount FROM {$tblpx}expenses WHERE date BETWEEN '".$value['cashbalance_date']."' AND '".$current_date."' AND (expense_type=89 OR expense_type =103) AND type=73 AND company_id=".$value['company_id']."")->queryRow();
                            $daybook_with_amount = $daybook_with['total_amount'];
                            //$dailyexpense_with=  Yii::app()->db->createCommand("SELECT SUM(dailyexpense_paidamount) as total_amount FROM {$tblpx}dailyexpense WHERE date BETWEEN '".$value['cashbalance_date']."' AND '".$current_date."' AND (expense_type=89 OR expense_type=103) AND exp_type=73 AND company_id=".$value['company_id']."")->queryRow();
                            $dailyexpense_with=  Yii::app()->db->createCommand("SELECT SUM(dailyexpense_paidamount) as total_amount FROM {$tblpx}dailyexpense WHERE date BETWEEN '".$value['cashbalance_date']."' AND '".$current_date."' AND (expense_type=89) AND exp_type=73 AND company_id=".$value['company_id']."")->queryRow();
                            $dailyexpense_with_amount = $dailyexpense_with['total_amount'];
                            $dailyvendors_cah=  Yii::app()->db->createCommand("SELECT SUM( IFNULL(amount, 0) + IFNULL(tax_amount, 0) ) as total_amount FROM {$tblpx}dailyvendors WHERE date BETWEEN '".$value['cashbalance_date']."' AND '".$current_date."' AND payment_type=89 AND company_id=".$value['company_id']."")->queryRow();
                            $dailyvendors_cash = $dailyvendors_cah['total_amount'];
                            $subcontractor_cah=  Yii::app()->db->createCommand("SELECT SUM( IFNULL(amount, 0) + IFNULL(tax_amount, 0) ) as total_amount FROM {$tblpx}subcontractor_payment WHERE date BETWEEN '".$value['cashbalance_date']."' AND '".$current_date."' AND payment_type=89 AND company_id=".$value['company_id']."")->queryRow();
                            $subcontractor_cash = $subcontractor_cah['total_amount'];

                            $withdrawal = $dailyvendors_cash+$daybook_with_amount+$dailyexpense_with_amount;
                        }


                    ?>

                        <tr class="odd">
                            <?php
                             $company = Company::model()->findByPk($value['company_id']);
                            ?>
                            <td><?php echo ($value['caption'] == 'Bank') ? $value['bank_name'] : $value['caption']; ?> &nbsp; &nbsp;- <?php echo $company->name; ?></td>
                            <td class="text-right"><?php echo Controller::money_format_inr(($value['cashbalance_opening_balance']+$deposit)-$withdrawal,2); ?></td>
                        </tr>
                    <?php } ?>


                    <tr>
                        <td colspan="2" class="whiteborder">&nbsp;</td>
                    </tr>

                    <tr>
                        <td colspan="2"><b>Invoice</b></td>
                    </tr>

                    <?php
                    $invoice_amount = 0;
                    $invoice = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}expenses LEFT JOIN {$tblpx}projects ON {$tblpx}expenses.projectid = {$tblpx}projects.pid WHERE {$tblpx}expenses.date = '" . $date . "' AND (".$newQuery3.") AND {$tblpx}expenses.invoice_id IS NOT NULL AND (({$tblpx}expenses.reconciliation_status =1 AND {$tblpx}expenses.payment_type =88) OR ({$tblpx}expenses.payment_type =89)) AND {$tblpx}expenses.type= 72 GROUP BY {$tblpx}projects.pid ORDER BY {$tblpx}expenses.exp_id DESC")->queryAll();
                    foreach ($invoice as $key => $value) {
                        $invoice_amount += $value['receipt'];
                        ?>
                    <tr class="odd">
                      <td><?php echo $value['name']; ?> &nbsp;&nbsp; - <?php echo $value['description']; ?> </td>
                      <td align="right"><?php  echo Controller::money_format_inr($value['receipt'], 2); ?></td>
                    </tr>
                    <?php } ?>


                    <tr>
                        <td><b>Total</b></td>
                        <td align="right"><b><?php echo Controller::money_format_inr($invoice_amount, 2); ?></b></td>
                    </tr>

                    <tr>
                        <td colspan="2" class="whiteborder">&nbsp;</td>
                    </tr>

                    <tr>
                        <td colspan="2"><b>Receipt</b></td>
                    </tr>

                    <tr class="div_color">
                        <td colspan="2"><b>Daybook</b></td>
                    </tr>

                    <tr>
                        <td colspan="2"><b>Cash Receipt</b></td>
                    </tr>

                     <?php
                    $daybook_cash = 0;
                    $receipt_amount = 0;
                    $receipt_daybook = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}expenses LEFT JOIN {$tblpx}projects ON {$tblpx}expenses.projectid = {$tblpx}projects.pid WHERE {$tblpx}expenses.date = '" . $date . "' AND (".$newQuery3.") AND {$tblpx}expenses.type=72 AND {$tblpx}expenses.payment_type=89 AND {$tblpx}expenses.invoice_id IS NULL GROUP BY {$tblpx}projects.pid ORDER BY {$tblpx}expenses.exp_id DESC")->queryAll();
                    foreach ($receipt_daybook as $key => $value) {
                        $project_amount = Yii::app()->db->createCommand("SELECT SUM(receipt) as total_amount FROM {$tblpx}expenses WHERE projectid =" . $value['pid'] . " and date='" . $date . "' AND type=72 AND payment_type= 89 AND invoice_id IS NULL AND (".$newQuery2.") ORDER BY exp_id DESC")->queryRow();
                        ?>
                        <tr>
                            <td class='text-uppercase'>Project : <?php echo $value['name']; ?></td>
                            <td class='text-right'><b><?php echo Controller::money_format_inr($project_amount['total_amount'], 2); ?></b></td>
                        </tr>

                        <?php
                        $details_1 = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}expenses WHERE projectid =" . $value['pid'] . " and date='" . $date . "' AND type=72 AND payment_type= 89 AND invoice_id IS NULL AND (".$newQuery2.") ORDER BY exp_id DESC")->queryAll();
                        foreach ($details_1 as $key => $values) {
                            $daybook_cash += $values['receipt'];
                            ?>
                            <tr>
                                <td><?php echo $values['description']; ?></td>
                                <td class='text-right'><?php echo Controller::money_format_inr($values['receipt'], 2); ?></td>
                            </tr>
                            <?php
                        }
                    }
                    ?>

                    <tr>
                        <td><b>Cash Total</b></td>
                        <td align="right"><b><?php echo Controller::money_format_inr($daybook_cash, 2); ?></b></td>
                    </tr>


                    <tr>
                        <td colspan="2"><b>Bank Receipt</b></td>
                    </tr>

                     <?php
                    $daybook_bank = 0;
                    $receipt_daybook2 = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}expenses LEFT JOIN {$tblpx}projects ON {$tblpx}expenses.projectid = {$tblpx}projects.pid WHERE {$tblpx}expenses.date = '" . $date . "' AND (".$newQuery3.") AND {$tblpx}expenses.type=72 AND {$tblpx}expenses.payment_type=88 AND {$tblpx}expenses.invoice_id IS NULL AND {$tblpx}expenses.reconciliation_status=1 GROUP BY {$tblpx}projects.pid ORDER BY {$tblpx}expenses.exp_id DESC")->queryAll();
                    foreach ($receipt_daybook2 as $key => $value) {
                        $project_amount = Yii::app()->db->createCommand("SELECT SUM(receipt) as total_amount FROM {$tblpx}expenses WHERE projectid =" . $value['pid'] . " and date='" . $date . "' AND type=72 AND payment_type= 88 AND invoice_id IS NULL AND reconciliation_status=1 AND (".$newQuery2.") ORDER BY exp_id DESC")->queryRow();
                        ?>
                        <tr>
                            <td class='text-uppercase'>Project : <?php echo $value['name']; ?></td>
                            <td class='text-right'><b><?php echo Controller::money_format_inr($project_amount['total_amount'], 2); ?></b></td>
                        </tr>

                        <?php
                        $details_2 = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}expenses WHERE projectid =" . $value['pid'] . " and date='" . $date . "' AND type=72 AND payment_type= 88 AND invoice_id IS NULL AND reconciliation_status=1 AND (".$newQuery2.") ORDER BY exp_id DESC")->queryAll();
                        foreach ($details_2 as $key => $values) {
                            $daybook_bank += $values['receipt'];
                            ?>
                            <tr>
                                <td><?php echo $values['description']; ?></td>
                                <td class='text-right'><?php echo Controller::money_format_inr($values['receipt'], 2); ?></td>
                            </tr>
                            <?php
                        }
                    }
                    ?>

                    <tr>
                        <td><b>Bank Total</b></td>
                        <td class='text-right'><b><?php echo Controller::money_format_inr($daybook_bank, 2); ?></b></td>
                    </tr>

                    <tr>
                        <td><b>Net Amount</b></td>
                        <td class='text-right'><b><?php echo Controller::money_format_inr($daybook_bank + $daybook_cash, 2); ?></b></td>
                    </tr>

                    <tr class="div_color">
                        <td colspan="2"><b>Daily Expenses</b></td>
                    </tr>

                    <tr>
                        <td colspan="2"><b>Cash Receipt</b></td>
                    </tr>

                     <?php
                    $dailyexp_cash = 0;
                    $receipt_dailyexp = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}dailyexpense WHERE date ='" . $date . "' AND dailyexpense_receipt_type=89 AND (".$newQuery2.") ORDER BY dailyexp_id DESC")->queryAll();
                    foreach ($receipt_dailyexp as $key => $value) {
                        $dailyexp_cash += $value['dailyexpense_receipt'];
                        ?>
                        <tr>
                            <td><?php echo $value['description']; ?></td>
                            <td class='text-right'><?php echo Controller::money_format_inr($value['dailyexpense_receipt'], 2); ?></td>
                        </tr>
                    <?php } ?>

                    <tr>
                        <td><b>Cash Total</b></td>
                        <td class='text-right'><b><?php echo Controller::money_format_inr($dailyexp_cash, 2); ?></b></td>
                    </tr>

                    <tr>
                        <td colspan="2"><b>Bank Receipt</b></td>
                    </tr>

                     <?php
                    $dailyexp_bank = 0;
                    $receipt_dailyexp = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}dailyexpense WHERE date ='" . $date . "' AND dailyexpense_receipt_type=88 AND (".$newQuery2.") AND reconciliation_status=1 ORDER BY dailyexp_id DESC")->queryAll();
                    foreach ($receipt_dailyexp as $key => $value) {
                        $dailyexp_bank += $value['dailyexpense_receipt'];
                        ?>
                        <tr>
                            <td><?php echo $value['description']; ?></td>
                            <td class='text-right'><?php echo Controller::money_format_inr($value['dailyexpense_receipt'], 2); ?></td>
                        </tr>
                    <?php } ?>

                    <tr>
                        <td><b>Bank Total</b></td>
                        <td class='text-right'><b><?php echo Controller::money_format_inr($dailyexp_bank, 2); ?></b></td>
                    </tr>

                    <tr>
                        <td><b>Net Amount</b></td>
                        <td class='text-right'><b><?php echo Controller::money_format_inr($dailyexp_bank + $dailyexp_cash, 2); ?></b></td>
                    </tr>
                    <?php
                    $receipt_amount = $dailyexp_bank + $dailyexp_cash + $daybook_bank + $daybook_cash;
                    ?>
                    <tr>
                        <td><b>Grand Total</b></td>
                        <td class='text-right'><b><?php echo Controller::money_format_inr($receipt_amount, 2); ?></b></td>
                    </tr>


                    <tr>
                        <td colspan="2" class="whiteborder">&nbsp;</td>
                    </tr>


                    <tr>
                        <td colspan="2"><b>Expense</b></td>
                    </tr>

                    <tr class="div_color">
                        <td colspan="2"><b>Day Book</b></td>
                    </tr>

                    <?php
                    $daybook_expense = 0;
                    $expense_amount = 0;
                    $expense_daybook = array();
                    $expense_daybook = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}expenses LEFT JOIN {$tblpx}projects ON {$tblpx}expenses.projectid = {$tblpx}projects.pid WHERE {$tblpx}expenses.date = '" . $date . "' AND (".$newQuery3.") AND {$tblpx}expenses.type=73 AND {$tblpx}expenses.subcontractor_id IS NULL AND {$tblpx}expenses.invoice_id IS NULL AND (({$tblpx}expenses.reconciliation_status =1 AND {$tblpx}expenses.expense_type =88) OR ({$tblpx}expenses.expense_type =89 OR {$tblpx}expenses.expense_type =103)) GROUP BY {$tblpx}projects.pid ORDER BY {$tblpx}expenses.exp_id DESC")->queryAll();
                    foreach ($expense_daybook as $key => $value) {
                        $project_amount = Yii::app()->db->createCommand("SELECT SUM(paid) as project_amount FROM {$tblpx}expenses WHERE projectid =" . $value['pid'] . " and date='" . $date . "' AND type=73 AND invoice_id IS NULL AND subcontractor_id IS NULL AND ((reconciliation_status =1 AND expense_type =88) OR (expense_type =89 OR expense_type=103)) ORDER BY exp_id DESC")->queryRow();
                        ?>
                        <tr>
                            <td class='text-uppercase'>Project : <?php echo $value['name']; ?></td>
                            <td class='text-right'><b><?php echo Controller::money_format_inr($project_amount['project_amount'], 2); ?></b></td>
                        </tr>

                        <?php
                        $details_1 = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}expenses WHERE projectid =" . $value['pid'] . " and date='" . $date . "' AND type=73 AND invoice_id IS NULL AND subcontractor_id IS NULL AND ((reconciliation_status =1 AND expense_type =88) OR (expense_type =89 OR expense_type =103)) ORDER BY exp_id DESC")->queryAll();
                        foreach ($details_1 as $key => $values) {
                            $daybook_expense += $values['paid'];
                            ?>
                            <tr>
                                <td><?php echo $values['description']; ?></td>
                                <td class='text-right'><?php echo Controller::money_format_inr($values['paid'], 2); ?></td>
                            </tr>
                            <?php
                        }
                    }
                    $expense_amount += $daybook_expense;
                    ?>

                    <tr>
                        <td><b>Net Amount</b></td>
                        <td class='text-right'><b><?php echo Controller::money_format_inr($daybook_expense, 2); ?></b></td>
                    </tr>


                    <tr class="div_color">
                        <td colspan="2"><b>Daily Expenses</b></td>
                    </tr>

                    <?php
                    $dailyexp_expense = 0;
                    $receipt_dailyexp = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}dailyexpense WHERE date ='" . $date . "' AND exp_type=73 AND (".$newQuery2.") AND ((expense_type = 88 AND reconciliation_status =1)OR (expense_type = 89)) ORDER BY dailyexp_id DESC")->queryAll();
                    foreach ($receipt_dailyexp as $key => $value) {
                        $dailyexp_expense += $value['dailyexpense_paidamount'];
                        ?>
                        <tr>
                            <td><?php echo $value['description']; ?></td>
                            <td class='text-right'><?php echo Controller::money_format_inr($value['dailyexpense_paidamount'], 2); ?></td>
                        </tr>
                    <?php }
                    $expense_amount += $dailyexp_expense;
                    ?>

                    <tr>
                        <td><b>Net Amount</b></td>
                        <td class='text-right'><b><?php echo Controller::money_format_inr($dailyexp_expense, 2); ?></b></td>
                    </tr>


                    <tr class="div_color">
                        <td colspan="2"><b>Vendor Payment</b></td>
                    </tr>

                     <?php
                    $vendor_expense = 0;
                    $expense_vendor = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}dailyvendors LEFT JOIN {$tblpx}projects ON {$tblpx}dailyvendors.project_id = {$tblpx}projects.pid WHERE {$tblpx}dailyvendors.date = '" . $date . "' AND (".$newQuery4.") AND {$tblpx}dailyvendors.project_id IS NOT NULL AND (({$tblpx}dailyvendors.payment_type = 88 AND {$tblpx}dailyvendors.reconciliation_status =1) OR ({$tblpx}dailyvendors.payment_type = 89)) GROUP BY {$tblpx}projects.pid ORDER BY {$tblpx}dailyvendors.daily_v_id DESC")->queryAll();
                    foreach ($expense_vendor as $key => $value) {
                        $project_amount = Yii::app()->db->createCommand("SELECT SUM( IFNULL(tax_amount, 0) + IFNULL(amount, 0) ) as project_amount FROM {$tblpx}dailyvendors WHERE project_id =" . $value['pid'] . " and date='" . $date . "' AND ((payment_type = 88 AND reconciliation_status =1) OR (payment_type = 89)) ORDER BY daily_v_id DESC")->queryRow();

                        ?>
                        <tr>
                            <td class='text-uppercase'>Project : <?php echo $value['name']; ?></td>
                            <td class='text-right'><b><?php echo Controller::money_format_inr($project_amount['project_amount'], 2); ?></b></td>
                        </tr>

                        <?php
                        $details_1 = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}dailyvendors WHERE project_id =" . $value['pid'] . " and date='" . $date . "' AND ((payment_type = 88 AND reconciliation_status =1) OR (payment_type = 89)) ORDER BY daily_v_id DESC")->queryAll();
                        foreach ($details_1 as $key => $values) {
                            $vendor_expense += $values['amount']+$values['tax_amount'];
                            ?>
                            <tr>
                                <td><?php echo $values['description']; ?></td>
                                <td class='text-right'><?php echo Controller::money_format_inr($values['amount']+$values['tax_amount'], 2); ?></td>
                            </tr>
                            <?php
                        }
                    } ?>
                    <tr class="">
                        <td colspan="2"><b>Other Expenses</b></td>
                    </tr>
                    <?php
                    $vendor_expense2 = 0;
                    $details_2 = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}dailyvendors WHERE project_id IS NULL and date='" . $date . "' AND (".$newQuery4.") AND ((payment_type = 88 AND reconciliation_status =1) OR (payment_type = 89)) ORDER BY daily_v_id DESC")->queryAll();
                        foreach ($details_2 as $key => $values) {
                            $vendor_expense2 += $values['amount']+$values['tax_amount'];
                            ?>
                            <tr>
                                <td><?php echo $values['description']; ?></td>
                                <td class='text-right'><?php echo Controller::money_format_inr($values['amount']+$values['tax_amount'], 2); ?></td>
                            </tr>
                    <?php
                        }

                    $expense_amount += $vendor_expense + $vendor_expense2;
                    ?>

                    <tr>
                        <td><b>Net Amount</b></td>
                        <td class='text-right'><b><?php echo Controller::money_format_inr($vendor_expense+$vendor_expense2, 2); ?></b></td>
                    </tr>


                         <tr class="div_color">
                        <td colspan="2"><b>Subcontractor Payment</b></td>
                    </tr>

                    <?php
                    $subcontractor_expense = 0;
                    $expense_subcontractor = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}subcontractor_payment LEFT JOIN {$tblpx}projects ON {$tblpx}subcontractor_payment.project_id = {$tblpx}projects.pid WHERE {$tblpx}subcontractor_payment.date = '" . $date . "' AND (".$newQuery5.") AND (({$tblpx}subcontractor_payment.payment_type = 88 AND {$tblpx}subcontractor_payment.reconciliation_status = 1) OR ({$tblpx}subcontractor_payment.payment_type = 89)) GROUP BY {$tblpx}projects.pid ORDER BY {$tblpx}subcontractor_payment.payment_id DESC")->queryAll();
                    foreach ($expense_subcontractor as $key => $value) {
                        $project_amount = Yii::app()->db->createCommand("SELECT SUM( IFNULL(tax_amount, 0) + IFNULL(amount, 0) ) as project_amount FROM {$tblpx}subcontractor_payment WHERE project_id =" . $value['pid'] . " and date='" . $date . "' AND ((payment_type = 88 AND reconciliation_status = 1) OR (payment_type = 89)) ORDER BY payment_id DESC")->queryRow();
                        ?>
                        <tr>
                            <td class='text-uppercase'>Project : <?php echo $value['name']; ?></td>
                            <td class='text-right'><b><?php echo Controller::money_format_inr($project_amount['project_amount'], 2); ?></b></td>
                        </tr>

                        <?php
                        $details_1 = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}subcontractor_payment WHERE project_id =" . $value['pid'] . " and date='" . $date . "' AND ((payment_type = 88 AND reconciliation_status = 1) OR (payment_type = 89)) ORDER BY payment_id DESC")->queryAll();
                        foreach ($details_1 as $key => $values) {
                            $subcontractor_expense += $values['amount']+$values['tax_amount'];
                            ?>
                            <tr>
                                <td><?php echo $values['description']; ?></td>
                                <td class='text-right'><?php echo Controller::money_format_inr($values['amount']+$values['tax_amount'], 2); ?></td>
                            </tr>
                            <?php
                        }
                    }
                    $expense_amount += $subcontractor_expense;
                    ?>
                    <tr>
                        <td><b>Net Amount</b></td>
                        <td class='text-right'><b><?php echo Controller::money_format_inr($subcontractor_expense, 2); ?></b></td>
                    </tr>

                    <tr>
                        <td><b>Grand Total</b></td>
                        <td class='text-right'><b><?php echo Controller::money_format_inr($expense_amount, 2); ?></b></td>
                    </tr>


                    <tr>
                        <td colspan="2" class="whiteborder">&nbsp;</td>
                    </tr>




                    <tr>
                        <td colspan="2" class="whiteborder">&nbsp;</td>
                    </tr>
                    <tr>
                        <td><b>Closing Balance</b></td>
                        <td align="right"></td>

                    </tr>

                    <?php
                    $current_date = $date;
                    $cash_blance = Yii::app()->db->createCommand("SELECT {$tblpx}status.caption,{$tblpx}cashbalance.cashbalance_type,{$tblpx}cashbalance.bank_id,{$tblpx}bank.bank_name,{$tblpx}cashbalance.cashbalance_opening_balance,{$tblpx}cashbalance.cashbalance_deposit,{$tblpx}cashbalance.cashbalance_withdrawal,{$tblpx}cashbalance.cashbalance_closing_balance,{$tblpx}cashbalance.cashbalance_date,{$tblpx}cashbalance.company_id FROM {$tblpx}cashbalance LEFT JOIN {$tblpx}bank ON {$tblpx}cashbalance.bank_id={$tblpx}bank.bank_id LEFT JOIN {$tblpx}status ON {$tblpx}cashbalance.cashbalance_type={$tblpx}status.sid WHERE (".$newQuery1.") ORDER BY bank_id ASC")->queryAll();
                    foreach ($cash_blance as $key => $value) {


                        if($value['bank_id'] !=''){
                            //recepit
                            $daybook=  Yii::app()->db->createCommand("SELECT SUM(receipt) as total_amount FROM {$tblpx}expenses WHERE reconciliation_status = 1 AND reconciliation_date BETWEEN '".$value['cashbalance_date']."' AND '".$current_date."' AND payment_type=88 AND bank_id=".$value['bank_id']." AND type=72 AND company_id=".$value['company_id']."")->queryRow();
                            $dailyexpense=  Yii::app()->db->createCommand("SELECT SUM(dailyexpense_receipt) as total_amount FROM {$tblpx}dailyexpense WHERE reconciliation_status = 1 AND reconciliation_date BETWEEN '".$value['cashbalance_date']."' AND '".$current_date."' AND dailyexpense_receipt_type=88 AND bank_id=".$value['bank_id']." AND exp_type=72 AND company_id=".$value['company_id']."")->queryRow();
                            $daybook_check = $daybook['total_amount'];
                            $dailyexpense_check = $dailyexpense['total_amount'];
                            //$deposit = $daybook_check+$dailyexpense_check+$value['cashbalance_opening_balance'];
                            $deposit = $daybook_check+$dailyexpense_check;

                            //expense
                            $daybook_with=  Yii::app()->db->createCommand("SELECT SUM(paid) as total_amount FROM {$tblpx}expenses WHERE reconciliation_status = 1 AND reconciliation_date BETWEEN '".$value['cashbalance_date']."' AND '".$current_date."' AND expense_type=88 AND bank_id=".$value['bank_id']." AND type=73 AND company_id=".$value['company_id']."")->queryRow();
                            $dailyexpense_with=  Yii::app()->db->createCommand("SELECT SUM(dailyexpense_paidamount) as total_amount FROM {$tblpx}dailyexpense WHERE reconciliation_status = 1 AND reconciliation_date BETWEEN '".$value['cashbalance_date']."' AND '".$current_date."' AND expense_type=88 AND bank_id=".$value['bank_id']." AND exp_type=73 AND company_id=".$value['company_id']."")->queryRow();
                            $daybook_with_amount = $daybook_with['total_amount'];
                            $dailyexpense_with_amount = $dailyexpense_with['total_amount'];
                            $dailyvendors=  Yii::app()->db->createCommand("SELECT SUM( IFNULL(amount, 0) + IFNULL(tax_amount, 0) ) as total_amount FROM {$tblpx}dailyvendors WHERE reconciliation_status = 1 AND reconciliation_date BETWEEN '".$value['cashbalance_date']."' AND '".$current_date."' AND payment_type=88  AND bank=".$value['bank_id']." AND company_id=".$value['company_id']."")->queryRow();
                            $subcontractor=  Yii::app()->db->createCommand("SELECT SUM( IFNULL(amount, 0) + IFNULL(tax_amount, 0) ) as total_amount FROM {$tblpx}subcontractor_payment WHERE reconciliation_status = 1 AND reconciliation_date BETWEEN '".$value['cashbalance_date']."' AND '".$current_date."' AND payment_type=88  AND bank=".$value['bank_id']." AND company_id=".$value['company_id']."")->queryRow();
                            $dailyvendors_check = $dailyvendors['total_amount'];
                            $subcontractor_check = $subcontractor['total_amount'];
                            $withdrawal = $dailyvendors_check+$daybook_with_amount+$dailyexpense_with_amount;


                        } else {
                            //receipt
                            $daybook_cah=  Yii::app()->db->createCommand("SELECT SUM(receipt) as total_amount FROM {$tblpx}expenses WHERE date BETWEEN '".$value['cashbalance_date']."' AND '".$current_date."' AND payment_type=89 AND type=72 AND company_id=".$value['company_id']."")->queryRow();
                            $daybook_cash = $daybook_cah['total_amount'];
                            //$dailyexpense_cah=  Yii::app()->db->createCommand("SELECT SUM(dailyexpense_receipt) as total_amount FROM {$tblpx}dailyexpense WHERE date BETWEEN '".$value['cashbalance_date']."' AND '".$current_date."' AND (dailyexpense_receipt_type=89 OR dailyexpense_receipt_type=103) AND exp_type=72 AND company_id=".$value['company_id']."")->queryRow();
                            $dailyexpense_cah=  Yii::app()->db->createCommand("SELECT SUM(dailyexpense_receipt) as total_amount FROM {$tblpx}dailyexpense WHERE date BETWEEN '".$value['cashbalance_date']."' AND '".$current_date."' AND (dailyexpense_receipt_type=89) AND exp_type=72 AND company_id=".$value['company_id']."")->queryRow();
                            $dailyexpense_cash = $dailyexpense_cah['total_amount'];
                            //$deposit = $daybook_cash+ $dailyexpense_cash+$value['cashbalance_opening_balance'];
                            $deposit = $daybook_cash+ $dailyexpense_cash;
                            //echo $current_date;
                            //echo $daybook_cash;

                            // expense
                            $daybook_with=  Yii::app()->db->createCommand("SELECT SUM(paid) as total_amount FROM {$tblpx}expenses WHERE date BETWEEN '".$value['cashbalance_date']."' AND '".$current_date."' AND (expense_type=89 OR expense_type=103) AND type=73 AND company_id=".$value['company_id']."")->queryRow();
                            $daybook_with_amount = $daybook_with['total_amount'];
                            //$dailyexpense_with=  Yii::app()->db->createCommand("SELECT SUM(dailyexpense_paidamount) as total_amount FROM {$tblpx}dailyexpense WHERE date BETWEEN '".$value['cashbalance_date']."' AND '".$current_date."' AND (expense_type=89 OR expense_type=103) AND exp_type=73 AND company_id=".$value['company_id']."")->queryRow();
                            $dailyexpense_with=  Yii::app()->db->createCommand("SELECT SUM(dailyexpense_paidamount) as total_amount FROM {$tblpx}dailyexpense WHERE date BETWEEN '".$value['cashbalance_date']."' AND '".$current_date."' AND (expense_type=89) AND exp_type=73 AND company_id=".$value['company_id']."")->queryRow();
                            $dailyexpense_with_amount = $dailyexpense_with['total_amount'];
                            $dailyvendors_cah=  Yii::app()->db->createCommand("SELECT SUM( IFNULL(amount, 0) + IFNULL(tax_amount, 0) ) as total_amount FROM {$tblpx}dailyvendors WHERE date BETWEEN '".$value['cashbalance_date']."' AND '".$current_date."' AND payment_type=89 AND company_id=".$value['company_id']."")->queryRow();
                            $dailyvendors_cash = $dailyvendors_cah['total_amount'];
                            $subcontractor_cah=  Yii::app()->db->createCommand("SELECT SUM( IFNULL(amount, 0) + IFNULL(tax_amount, 0) ) as total_amount FROM {$tblpx}subcontractor_payment WHERE date BETWEEN '".$value['cashbalance_date']."' AND '".$current_date."' AND payment_type=89 AND company_id=".$value['company_id']."")->queryRow();
                            $subcontractor_cash = $subcontractor_cah['total_amount'];

                            $withdrawal = $dailyvendors_cash+$daybook_with_amount+$dailyexpense_with_amount;

                        }


                    ?>
                     <tr class="odd">
                            <?php
                             $company = Company::model()->findByPk($value['company_id']);
                            ?>
                            <td><?php echo ($value['caption'] == 'Bank') ? $value['bank_name'] : $value['caption']; ?>&nbsp; &nbsp;- <?php echo $company->name; ?></td>
                            <td class="text-right"><?php echo Controller::money_format_inr(($value['cashbalance_opening_balance']+$deposit)-$withdrawal,2); ?></td>
                        </tr>
                    <?php } ?>


                    <tr>
                        <td colspan="2" class="whiteborder">&nbsp;</td>
                    </tr>


                </tbody></table>


</div>
<style>
    .text-right{
        text-align: right;
    }
</style>
