<?php
/* @var $labourData LabourWorktype[] */
/* @var $index int */
?>

<div class="row input-group">
<div class="col-md-3 element-style">
<p><label style="margin-left:8px; !important">Labour Type</label></p>
<select name="Dailyreport[lab_templates][<?php echo $index; ?>][labour_id]" class="labour_worktype form-control select2" onchange="setLabourId(this)">
    <option value="" selected disabled>Select Labour</option>
    <?php foreach ($labourData as $labour): ?>
        <option value="<?php echo CHtml::encode($labour->type_id); ?>" data-rate="<?php echo CHtml::encode($labour->rate); ?>">
            <?php echo CHtml::encode($labour->worktype); ?>
        </option>
    <?php endforeach; ?>
</select>
    </div>
    <div class="col-md-3 element-style">
        <p><label>No Of Labours</label></p>
        <input type="number" class="labourcount form-control" name="Dailyreport[lab_templates][<?php echo $index; ?>][count]" min="0" value="0" onblur="calculateAmount(this)" onkeyup="validateInput(this)">
        <div class="error"></div>
    </div>
    <div class="col-md-3 element-style">
        <p><label>Amount</label></p>
        <input type="text" class="labour_total_amount form-control" name="Dailyreport[lab_templates][<?php echo $index; ?>][labour_amount]" readonly>
        <div class="error"></div>
    </div>
    <input type="hidden" name="Dailyreport[lab_templates][<?php echo $index; ?>][default_labour_type]" value='1'>
   
</div>


