<?php
/* @var $this SubcontractorbillController */
/* @var $model Subcontractorbill */

$this->breadcrumbs = array(
    'Subcontractorbills' => array('index'),
    'Manage',
);

?>
<!-- <div class="search-form" style="display:none">
<?php /* $this->renderPartial('_search',array(
'model'=>$model,
)); */ ?>

</div> -->
<!-- search-form -->
<?php if (Yii::app()->user->hasFlash('success')): ?>
    <div class="info" style="width: 500px;margin-left: 400px;padding: 8px;color: green;font-size: 20px;font-weight: bold;">
        <?php echo Yii::app()->user->getFlash('success'); ?>
    </div>
<?php endif; ?>


<?php if (Yii::app()->user->hasFlash('error')): ?>
    <div class="info" style="width: 500px;margin-left: 400px;padding: 8px;color: red;font-size: 20px;font-weight: bold;">
        <?php echo Yii::app()->user->getFlash('error'); ?>
    </div>
<?php endif; ?>
<div class="container" id="vendors">
    <div class="header-container">
        <h3>Subcontractor Bill</h3>
        <div class="btn-container">
            <?php if ((isset(Yii::app()->user->role) && (in_array('/subcontractorbill/create', Yii::app()->user->menuauthlist)))) { ?>
                <button id="bill1" href="<?php echo $this->createUrl('subcontractorbill/create') ?>"
                    class="btn btn-info ">Add New</button>
            <?php } ?>
            <?php if ((isset(Yii::app()->user->role) && (in_array('/subcontractorbill/createwithItem', Yii::app()->user->menuauthlist)))) { ?>
                <button id="bill2" href="<?php echo $this->createUrl('subcontractorbill/createwithItem') ?>"
                    class="btn btn-info ">Add Item wise</button>
            <?php } ?>
        </div>
    </div>
    <?php $this->renderPartial('_newsearch', array('model' => $model)) ?>
    <div id="msg_box"></div>
    <?php
    $this->widget('zii.widgets.CListView', array(
        'dataProvider' => $model->search(),
        'itemView' => '_view',
        'template' => '<div>{summary}{sorter}</div><div class=" table-wrapper margin-top-10"><div id="table-wrapper"><table cellpadding="10" class="table total-table id="fixtable">{items}</table></div>{pager}',
    ));
    ?>
</div>
<script type="text/javascript">
    $(document).ready(function () {


        $(document).delegate("#editBill", "click", function () {
            var id = $(this).attr("data-id");
            window.location.href = "<?php echo $this->createUrl('subcontractorbill/update&id=') ?>" + id;
        });
        $(document).delegate("#viewBill", "click", function () {
            var id = $(this).attr("data-id");
            window.location.href = "<?php echo $this->createUrl('subcontractorbill/view&id=') ?>" + id;
        });
        $(document).delegate("#deleteBill", "click", function () {
            var id = $(this).attr("data-id");
            $.ajax({
                type: "POST",
                data: {
                    id: id
                },
                dataType: "json",
                url: "<?php echo $this->createUrl('Subcontractorbill/Delete&id=') ?>" + id,
                success: function (response) {
                    $("#msg_box").html('<div class="alert alert-' + response.response + ' alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><strong> Success!</strong>  &nbsp;' + response.msg + '</div>');
                    setTimeout(
                        function () {
                            location.reload();
                        },
                        1000);
                }
            });
        });
    });
    $("#bill1").click(function () {
        let redirecturl = $(this).attr("href");
        window.location.href = redirecturl;
    });
    $("#bill2").click(function () {
        let redirecturl = $(this).attr("href");
        window.location.href = redirecturl;
    });

</script>