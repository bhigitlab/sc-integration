<?php
/* @var $this SubcontractorController */
/* @var $model Subcontractor */
/* @var $form CActiveForm */
?>

<!-- <div class="pull-right" ><span><b>* Balance Amount = Amount + Tax Amount</b></span></div> -->
<div class="scquotations form">

<?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'scquotation-form',
        'enableAjaxValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true,
            'validateOnType' => false,),
    ));
    ?>



    <!-- Popup content-->

            
		<div class="block_hold clearfix collapse" id="subcontraqtn">    
                    <div class="row_block">
			<div class="elem_block">
                            <div class="form-group">
				<?php echo $form->labelEx($model, 'scquotation_date'); ?>
                                <?php echo CHtml::activeTextField($model, 'scquotation_date', array('size' => 10, 'class' => 'form-control', 'value' => $model->isNewRecord ? date("d-m-Y") : date("d-m-Y", strtotime($model->scquotation_date)))); ?>
                                <?php
                                $this->widget('application.extensions.calendar.SCalendar', array(
                                    'inputField' => 'Scquotation_scquotation_date',
                                    'ifFormat' => '%d-%m-%Y',
                                ));
                                ?>
                                <?php echo $form->error($model, 'scquotation_date'); ?>
                            </div>
			</div>
			<div class="elem_block">
                            <div class="form-group">
				<?php echo $form->labelEx($model, 'project_id'); ?>
                                <?php echo $form->dropDownList($model, 'project_id', CHtml::listData(Projects::model()->findAll(array('order' => 'name ASC')), 'pid', 'name'), array('empty' => '-Select Project-', 'class' => 'form-control')); ?>
                                <?php echo $form->error($model, 'project_id'); ?>			
                            </div>
			</div>
			<div class="elem_block">
                            <div class="form-group">
				<?php echo $form->labelEx($model, 'subcontractor_id'); ?>
                                <?php echo $form->dropDownList($model, 'subcontractor_id', CHtml::listData(Subcontractor::model()->findAll(array('order' => 'subcontractor_name ASC')), 'subcontractor_id', 'subcontractor_name'), array('empty' => '-Select Subcontractor-', 'class' => 'form-control')); ?>
                                <?php echo $form->error($model, 'subcontractor_id'); ?>							
                            </div>
			</div>
			<div class="elem_block">
                            <div class="form-group">
                                <?php echo $form->labelEx($model, 'scquotation_amount'); ?>
                                <?php echo $form->textField($model, 'scquotation_amount', array('class' => 'form-control Scquotation_scquotation_amount')); ?>
                                <?php echo $form->error($model, 'scquotation_amount'); ?>					
                            </div>
			</div>
                    </div>
                    <div class="row_block">
                        <div class="elem_block areahold">
                            <div class="form-group">
                                <?php echo $form->labelEx($model, 'scquotation_decription'); ?>
                                <?php echo $form->textArea($model, 'scquotation_decription', array('rows' => 6, 'cols' => 30, 'class' => 'form-control')); ?>
                                <?php echo $form->error($model, 'scquotation_decription'); ?>
                            </div>
			</div>
                    </div>
                    
                    <div class="row_block">
			<div class="elem_block submit-button">
                            <div class="form-group">
                                <?php echo CHtml::htmlButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-info btnsubmit', 'id' => 'buttonsubmit')); ?>
                            </div>
			</div>
                    </div>
		</div>

        <?php $this->endWidget(); ?>

</div><!-- form -->
<script>
        $(document).ready(function () {
            //select all checkboxes
            $("#select_all").change(function () {  //"select all" change
                var status = this.checked; // "select all" checked status
                $('.checkbox').each(function () { //iterate all listed checkbox items
                    this.checked = status; //change ".checkbox" checked status
                });
            });

            $('.checkbox').change(function () { //".checkbox" change
                //uncheck "select all", if one of the listed checkbox item is unchecked
                if (this.checked == false) { //if this item is unchecked
                    $("#select_all")[0].checked = false; //change "select all" checked status to false
                }

                //check "select all" if all checkbox items are checked
                if ($('.checkbox:checked').length == $('.checkbox').length) {
                    $("#select_all")[0].checked = true; //change "select all" checked status to true
                }
            });
            
            
            $("#select_alltype").change(function () {  //"select all" change
                var status = this.checked; // "select all" checked status
                $('.checkboxtype').each(function () { //iterate all listed checkbox items
                    this.checked = status; //change ".checkbox" checked status
                });
            });
            
            
            $('.checkboxtype').change(function () { //".checkbox" change
                //uncheck "select all", if one of the listed checkbox item is unchecked
                if (this.checked == false) { //if this item is unchecked
                    $("#select_alltype")[0].checked = false; //change "select all" checked status to false
                }

                //check "select all" if all checkbox items are checked
                if ($('.checkboxtype:checked').length == $('.checkboxtype').length) {
                    $("#select_alltype")[0].checked = true; //change "select all" checked status to true
                }
            });


        });


    </script>
