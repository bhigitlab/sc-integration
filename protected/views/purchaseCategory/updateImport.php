
<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'clients-import-form',
   'enableAjaxValidation'=>false,
)); ?>

	<div class="row">
	    <div class="col-md-6">
			<?php echo $form->labelEx($model,'main_category'); ?>
			<?php echo $form->textField($model,'main_category',array('class'=>'form-control','size'=>60,'maxlength'=>100)); ?>
			<?php echo $form->error($model,'main_category'); ?>
		</div>
		<div class="col-md-6">
			<?php echo $form->labelEx($model,'sub_category'); ?>
			<?php echo $form->textField($model,'sub_category',array('class'=>'form-control','size'=>20,'maxlength'=>20)); ?>
			<?php echo $form->error($model,'sub_category'); ?>
	    </div>						
	</div>

	<div class="row">
        <div class="col-md-6">
			<?php echo $form->labelEx($model,'brand'); ?>
			<?php echo $form->textField($model,'brand',array('class'=>'form-control','size'=>20,'maxlength'=>20)); ?>
			<?php echo $form->error($model,'brand'); ?>
	    </div>
	    <div class="col-md-6">
			<?php echo $form->labelEx($model,'specification'); ?>
			<?php echo $form->textField($model,'specification',array('class'=>'form-control')); ?>
			<?php echo $form->error($model,'specification'); ?>
		</div>        	
	</div>	
	<div class="row">
		<div class="col-md-6">
			<?php echo $form->labelEx($model,'company'); ?>
				<?php echo $form->textField($model,'company',array('class'=>'form-control','size'=>20,'maxlength'=>20)); ?>
				<?php echo $form->error($model,'company'); ?>
			</div>

	</div>
    <div class="row buttons text-center mt-2">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'btn btn-primary')); ?>
	</div>

<?php $this->endWidget(); ?>
</div><!-- form -->
