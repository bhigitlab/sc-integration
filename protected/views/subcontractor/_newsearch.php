<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.min.js"></script>
<?php
/* @var $this SubcontractorController */
/* @var $model Subcontractor */
/* @var $form CActiveForm */
?>
<?php
$user = Users::model()->findByPk(Yii::app()->user->id);
$arrVal = explode(',', $user->company_id);
$query_company = "";
$query_company1 = "";
foreach ($arrVal as $arr) {
    if ($query_company)
        $query_company .= ' OR';
    if ($query_company1)
        $query_company1 .= ' OR';
    $query_company .= " FIND_IN_SET('" . $arr . "', id)";
    $query_company1 .= " FIND_IN_SET('" . $arr . "', company_id)";
}
$tblpx = Yii::app()->db->tablePrefix;
$qoutation_data = Yii::app()->db->createCommand("SELECT * FROM " . $tblpx . "scquotation WHERE (" . $query_company1 . ")   ORDER BY scquotation_id desc")->queryAll();
?>
<div class="page_filter">
    <?php $form = $this->beginWidget('CActiveForm', array(
        'action' => Yii::app()->createUrl($this->route),
        'method' => 'get',
    )); ?>
    <div class="row">
        <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-2">
            <?php echo $form->dropDownList($model, 'company_id', CHtml::listData(Company::model()->findAll(array(
                'select' => array('id, name'),
                "condition" => '(' . $query_company . ')',
                'order' => 'name',
                'distinct' => true
            )), 'id', 'name'), array('class' => 'form-control js-example-basic-single', 'empty' => 'Select Company')); ?>
        </div>
        <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-2">
            <?php echo $form->dropDownList($model, 'project_id', CHtml::listData(Projects::model()->findAll(array(
                'select' => array('pid, name'),
                "condition" => '(' . $query_company1 . ')',
                'order' => 'name',
                'distinct' => true
            )), 'pid', 'name'), array('class' => 'form-control js-example-basic-single', 'empty' => 'Select Project')); ?>
        </div>
        <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-2">
            <?php echo $form->dropDownList($model, 'subcontractor_id', CHtml::listData(Subcontractor::model()->findAll(array(
                'select' => array('subcontractor_id, subcontractor_name'),
                "condition" => '(' . $query_company1 . ')',
                'order' => 'subcontractor_name',
                'distinct' => true
            )), 'subcontractor_id', 'subcontractor_name'), array('class' => 'form-control js-example-basic-single', 'empty' => 'Select Subcontractor')); ?>
        </div>
        <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-2">
            <select id="scquotation_no" class="select_box form-control js-example-basic-single"
                name="Scquotation[scquotation_no]">
                <option value="">Select Quotation no</option>
                <?php
                if (!empty($qoutation_data)) {
                    foreach ($qoutation_data as $key => $value) {
                        ?>
                        <option value="<?php echo $value['scquotation_no'] ?>" <?php echo ($value['scquotation_no'] == $model->scquotation_no) ? 'selected' : ''; ?>>
                            <?php echo $value['scquotation_no']; ?>
                        </option>
                    <?php }
                } ?>
            </select>
        </div>
        <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-2">
            <?php
            // Map the database values to display labels
            $approveStatusOptions = array(
                'Yes' => 'Approved',
                'No' => 'Unapproved',
            );

            echo $form->dropDownList(
                $model,
                'approve_status',
                $approveStatusOptions, // Use the mapped options
                array(
                    'class' => 'form-control js-example-basic-single',
                    'empty' => 'Select Approval Status', // Placeholder
                )
            ); ?>
        </div>
        <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-2">
            <?php echo $form->textField($model, 'scquotation_amount', array('class' => 'form-control scquotation_amount', 'placeholder' => 'Amount', 'pattern' => '\d*', 'title' => 'Please enter a valid number')); ?>
        </div>
        <div class="form-group col-xs-12 col-sm-6 col-lg-12 text-right">
            <input type="submit" id="submitBotton" value="Go" class="btn btn-primary btn-sm" />
            <input type="reset" id="clear" value="Clear" class="btn btn-default btn-sm" />
        </div>
    </div>
    <?php $this->endWidget(); ?>
</div>
<script>
    $(document).ready(function () {
        $('.js-example-basic-single').select2({
            width: '100%',
        });

    });
    var elements = document.getElementsByClassName('scquotation_amount');
    for (var i = 0; i < elements.length; i++) {
        elements[i].addEventListener('input', function (e) {
            this.value = this.value.replace(/[^0-9]/g, ''); // Replace any non-numeric characters with an empty string
        });
    }

</script>