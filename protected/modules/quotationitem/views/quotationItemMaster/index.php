<?php
/* @var $this QuotationItemMasterController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
	'Quotation Item Masters',
);
?>

<div class="container">
	<div class="row">
		<h1>Quotation Item</h1>
	</div>
</div>
<?php
$this->renderPartial('_form', array('model' => $model));
?>