
<div class="container" id="project">
    <h2><?php echo $model[0]['title']; ?></h2>
    <div class="list_item"><span>Project :</span><?php echo $model[0]['name']; ?></div>
    <div class="list_item"><span>Description :</span><?php echo $model[0]['description']; ?></div>
   
    
    <div class="exp-list">
    
<div class="row">
	
	<?php foreach($model as $mod)
	{?>
	<div class="col-md-2 col-sm-2 col-xs-2">
		<div style="width: 100px; height: 86px;" class="fileupload-new thumbnail">
		<?php
		$output_dir = Yii::app()->request->baseUrl . '/uploads/image/';
		if (file_exists($output_dir).$mod['image']) { ?>
		<span><?php echo CHtml::link('', array('/ImageGallery/delete&id=' . $mod['id'].'&albumid='.$model[0]['album_id']), array('class' => 'fa fa-trash-o deleteicon','style' => 'margin-left:80px;','title'=>'Delete')); ?></span>
		<img src="<?php echo Yii::app()->request->baseUrl . '/uploads/image/' . $mod['image']; ?>" alt="" style="height:60px;width:90px;cursor:pointer"  class="largeimage img" data-toggle="modal" data-target="#myModal"/>
		<?php 
		}
		else
		{
		?>
		
		<img src="<?php echo Yii::app()->theme->baseUrl . '/images/emptyimg.jpg' ?>" alt="" style="height:60px;width:90px;cursor:pointer"  class="largeimage img" data-toggle="modal" data-target="#myModal" />
		<?php
		}
		?>
		</div>  	
		<br>
	</div>
	<?php
	}
	?>

</div>


		
		
    </div>
</div>

<script type="text/javascript">
$(document).ready(function () {

$('.deleteicon').on('click', function (event) {
	
	event.preventDefault();
	//var albumid = <?php echo $model[0]['album_id'];?>
	//alert(albumid);
	//exit();
	var url = $(this).attr('href');
	
	var answer = confirm ("Are you sure you want to delete?");
	if (answer)
	{
		
					$.ajax({
					type: "POST",
					dataType: "json",
					url: url,
					success: function (response)
					{
						if(response.response == "success"){
							
							alert("Image Deleted");
							location.reload();
						}else
						{
							alert("Image Deleted");
							window.location.href = '<?php echo $this->createUrl('imageGallery/admin') ?>'
						}
						

					}
				});
				
		}
		else
		{
			

		}
		
		   });
		   

});

</script>



<div class="container">
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-sm modal-lg">
      <div class="modal-content">
	   <div class="modal-header" id="previewimg_head">
         <!-- <button type="button" class="close" data-dismiss="modal">&times;</button>
           <h4 class="modal-title">Large Image</h4>-->
         </div>
          
        <div id="previewimg" class="img_style" >
		
		</div>
		<br>
        <div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
</div>
 
 <style>
	 #previewimg img{
		 width:880px;
		 height:410px;
		 margin:10px;
		 }
</style>
 
 
<?php Yii::app()->clientScript->registerScript("myscript","
    $(document).ready(function(){
    
     var add_button      = $('.largeimage');	 
	 $(add_button).click(function(e){	
		var src_image = $(this).attr('src');
		//alert(src_image);
		
		var imgdiner = $( '<img />' );
		imgdiner.attr( 'src', src_image );
		
		str = src_image;    
		arr = str.split('/');
		strFine = arr[arr.length-1];
		//$('#previewimg_head').html('<button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button><h4 class=\"modal-title preview_head_cls\">'+strFine+'</h4>');	
		
		//$('#previewimg').html('<img src='+src_image+' alt='n/a' />');
		
		$('#previewimg').html(imgdiner);
		
		 
		
		});	 

});

");
?>

