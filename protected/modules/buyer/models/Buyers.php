<?php

/**
 * This is the model class for table "{{buyers}}".
 *
 * The followings are the available columns in table '{{buyers}}':
 * @property integer $id
 * @property string $name
 * @property integer $buyer_type
 * @property string $phone
 * @property string $address
 * @property string $email_id
 * @property string $gst_no
 * @property integer $status
 * @property string $company
 * @property integer $project
 * @property string $flat_numbers
 * @property integer $created_by
 * @property integer $updated_by
 * @property string $created_date
 * @property string $updated_date
 */
class Buyers extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{buyers}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, buyer_type,address,company,description,phone,email_id', 'required', 'message' => 'Enter {attribute}.'),
			array('local_address,contact_person', 'validatedeatils'),
			array('name', 'unique'),
			array('status', 'required', 'message' => 'Choose one {attribute}.'),
			array('buyer_type, status, project, created_by, updated_by', 'numerical', 'integerOnly' => true),
			array('name, email_id, gst_no', 'length', 'max' => 100),
			array('phone', 'length', 'max' => 14),
			array('address, created_date, updated_date,company', 'safe'),
			array('email_id', 'email'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, buyer_type, phone, address, email_id, gst_no, status, company, project, flat_numbers, created_by, updated_by, created_date, updated_date', 'safe', 'on' => 'search'),
		);
	}
	public function validatedeatils($attribute, $params)
	{


		//if ($this->project_type == 7 ) 
		if ($this->buyerType['project_type'] == 'NRI') {

			if ($this->local_address == '') {
				$this->addError('local_address', 'Please Enter  Local Address');
			}
			if ($this->contact_person == '') {
				$this->addError('contact_person', 'Please Enter Contact Person');
			}
		}
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'buyerType' => array(self::BELONGS_TO, 'ProjectType', 'buyer_type'),
			'status0' => array(self::BELONGS_TO, 'Status', 'status'),
			'createdBy' => array(self::BELONGS_TO, 'Users', 'created_by'),
			'updatedBy' => array(self::BELONGS_TO, 'Users', 'updated_by'),
			'project0' => array(self::BELONGS_TO, 'Projects', 'project'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'buyer_type' => 'Buyer Type',
			'phone' => 'Phone',
			'address' => 'Address',
			'email_id' => 'Email',
			'gst_no' => 'Gst No',
			'status' => 'Status',
			'company' => 'Company',
			'project' => 'Project',
			'flat_numbers' => 'Flat Numbers',
			'created_by' => 'Created By',
			'updated_by' => 'Updated By',
			'created_date' => 'Created Date',
			'updated_date' => 'Updated Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('name', $this->name, true);
		$criteria->compare('buyer_type', $this->buyer_type);
		$criteria->compare('phone', $this->phone, true);
		$criteria->compare('address', $this->address, true);
		$criteria->compare('email_id', $this->email_id, true);
		$criteria->compare('gst_no', $this->gst_no, true);
		$criteria->compare('status', $this->status);
		$criteria->compare('company', $this->company, true);
		$criteria->compare('project', $this->project);
		$criteria->compare('flat_numbers', $this->flat_numbers, true);
		$criteria->compare('created_by', $this->created_by);
		$criteria->compare('updated_by', $this->updated_by);
		$criteria->compare('created_date', $this->created_date, true);
		$criteria->compare('updated_date', $this->updated_date, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Buyers the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}
}
