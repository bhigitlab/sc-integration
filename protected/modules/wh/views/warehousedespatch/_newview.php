<!--<div id="msg_box"></div>-->
<?php
// echo "<pre>";print_r($data);exit();
$warehousedespatch = Warehousedespatch::model()->findByPk($data['warehousedespatch_id']);
if ($data['delete_status'] == 0 && $data['edit_status'] == 0) {
    //  $warehousereceipt_no_link = !empty($data['warehousereceipt_no'])?CHtml::link($data['warehousereceipt_no'], 'index.php?r=warehousereceipt/deletedWarehouseView&receiptid='.$data['warehousereceipt_id'], array('class' => 'link','target'=>'_blank')):'';
    $warehousedespatch_no = !empty($data['warehousedespatch_id']) ? CHtml::link($data['warehousedespatch_no'], 'index.php?r=wh/warehousedespatch/view&despatchid=' . $data['warehousedespatch_id'], array('class' => 'link', 'target' => '_blank')) : '';
    if ($data['warehousereceipt_id'] == '' && $data['warehousedespatch_warehouseid_to'] != $data['warehouseid_to']) {
        $warehousereceipt_no_link = !empty($data['all_warehousereceipt_id']) ? CHtml::link("", 'index.php?r=wh/warehousereceipt/deletedWarehouseView&receiptid=' . $data['all_warehousereceipt_id'], array('class' => 'link_deleted', 'target' => '_blank')) : '';
        $eta_date = isset($data['warehouse_eta_date']) ? date("d-m-Y", strtotime($data['warehouse_eta_date'])) : '-';
    } else {
        $warehousereceipt_no_link = !empty($data['all_warehousereceipt_id']) ? CHtml::link("(Deleted)", 'index.php?r=wh/warehousereceipt/deletedWarehouseView&receiptid=' . $data['warehouse_transfer_type_deleted_id'], array('class' => 'link_deleted', 'target' => '_blank')) : '';
        $eta_date = isset($data['warehouse_despatch_eta_date']) ? date("d-m-Y", strtotime($data['warehouse_despatch_eta_date'])) : '-';
    }
    $warehousereceipt_no = "<span style='color: red;font-size: 12px;'>" . $warehousereceipt_no_link . "  </span>";
    $warehousedespatch_delete = WarehouseTransferDeletedLog::model()->findByAttributes(array('warehousereceipt_despatch_id' => $data['warehousedespatch_id'], 'warehouseid_to' => $data['warehouseid_to'], 'warehouse_transfer_type_delete' => 1, 'warehousereceipt_transfer_type' => 2));
    $warehouse_to = $warehousedespatch_delete->warehouseidTo['warehouse_name'];
    $warehouse_from = $warehousedespatch_delete->warehouseidFrom['warehouse_name'];
    $clerk = $warehousedespatch->clerk['first_name'] . ' ' . $warehousedespatch->clerk['last_name'];
    $date = date("d-m-Y", strtotime($data['warehouse_despatch_date']));
} elseif ($data['delete_status'] == 0) {
    $warehousedespatch_no = !empty($data['warehousedespatch_id']) ? CHtml::link($data['warehousedespatch_no'], 'index.php?r=wh/warehousedespatch/view&despatchid=' . $data['warehousedespatch_id'], array('class' => 'link', 'target' => '_blank')) : '';
    $warehouse_to = $warehousedespatch->warehousedespatchWarehouseidTo['warehouse_name'];
    $warehouse_from = $warehousedespatch->warehouse['warehouse_name'];
    $clerk = $warehousedespatch->clerk['first_name'] . ' ' . $warehousedespatch->clerk['last_name'];
    $date = date("d-m-Y", strtotime($data['warehousedespatch_date']));
    $eta_date = isset($data['warehouse_eta_date']) ? date("d-m-Y", strtotime($data['warehouse_eta_date'])) : '-';
    $warehousereceipt_no = !empty($data['all_warehousereceipt_id']) ? CHtml::link($data['all_warehousereceipt_no'], 'index.php?r=wh/warehousereceipt/view&receiptid=' . $data['all_warehousereceipt_id'], array('class' => 'link', 'target' => '_blank')) : '';
} elseif ($data['delete_status'] == 1 && $data['edit_status'] == 1) {
    //  $warehousereceipt_no_link = !empty($data['warehousereceipt_no'])?CHtml::link($data['warehousereceipt_no'], 'index.php?r=warehousereceipt/deletedWarehouseView&receiptid='.$data['warehousereceipt_id'], array('class' => 'link','target'=>'_blank')):'';
    $warehousedespatch_no = !empty($data['warehousedespatch_id']) ? CHtml::link($data['warehousedespatch_no'], 'index.php?r=wh/warehousedespatch/view&despatchid=' . $data['warehousedespatch_id'], array('class' => 'link', 'target' => '_blank')) : '';
    if ($data['warehousereceipt_id'] == '' && $data['warehousedespatch_warehouseid_to'] != $data['warehouseid_to']) {
        $warehousereceipt_no_link = !empty($data['all_warehousereceipt_id']) ? CHtml::link("", 'index.php?r=wh/warehousereceipt/deletedWarehouseView&receiptid=' . $data['all_warehousereceipt_id'], array('class' => 'link_deleted', 'target' => '_blank')) : '';
        $eta_date = isset($data['warehouse_eta_date']) ? date("d-m-Y", strtotime($data['warehouse_eta_date'])) : '-';
    } else {
        $warehousereceipt_no_link = !empty($data['all_warehousereceipt_id']) ? CHtml::link("(Deleted)", 'index.php?r=wh/warehousereceipt/deletedWarehouseView&receiptid=' . $data['warehouse_transfer_type_deleted_id'], array('class' => 'link_deleted', 'target' => '_blank')) : '';
        $eta_date = isset($data['warehouse_despatch_eta_date']) ? date("d-m-Y", strtotime($data['warehouse_despatch_eta_date'])) : '-';
    }
    $warehousereceipt_no = "<span style='color: red;font-size: 12px;'>" . $warehousereceipt_no_link . "  </span>";
    $warehousedespatch_delete = WarehouseTransferDeletedLog::model()->findByAttributes(array('warehousereceipt_despatch_id' => $data['warehousedespatch_id'], 'warehouseid_to' => $data['warehouseid_to'], 'warehouse_transfer_type_delete' => 1, 'warehousereceipt_transfer_type' => 2));
    $warehouse_to = $warehousedespatch->warehousedespatchWarehouseidTo['warehouse_name'];
    $warehouse_from = $warehousedespatch_delete->warehouseidFrom['warehouse_name'];
    $clerk = $warehousedespatch->clerk['first_name'] . ' ' . $warehousedespatch->clerk['last_name'];
    $date = date("d-m-Y", strtotime($data['warehousedespatch_date']));
} else {
    //  $warehousereceipt_no_link = !empty($data['warehousereceipt_no'])?CHtml::link($data['warehousereceipt_no'], 'index.php?r=warehousereceipt/deletedWarehouseView&receiptid='.$data['warehousereceipt_id'], array('class' => 'link','target'=>'_blank')):'';
    $warehousedespatch_no = !empty($data['warehousedespatch_id']) ? CHtml::link($data['warehousedespatch_no'], 'index.php?r=wh/warehousedespatch/view&despatchid=' . $data['warehousedespatch_id'], array('class' => 'link', 'target' => '_blank')) : '';
    $warehousereceipt_no_link = !empty($data['all_warehousereceipt_id']) ? CHtml::link("(Deleted)", 'index.php?r=wh/warehousereceipt/deletedWarehouseView&receiptid=' . $data['warehouse_transfer_type_deleted_id'], array('class' => 'link_deleted', 'target' => '_blank')) : '';
    $warehousereceipt_no = "<span style='color: red;font-size: 12px;'>" . $warehousereceipt_no_link . "  </span>";
    $warehousedespatch_delete = WarehouseTransferDeletedLog::model()->findByAttributes(array('warehousereceipt_despatch_id' => $data['warehousedespatch_id'], 'warehouseid_to' => $data['warehouseid_to'], 'warehouse_transfer_type_delete' => 1, 'warehousereceipt_transfer_type' => 2));
    $warehouse_to = $warehousedespatch_delete->warehouseidTo['warehouse_name'];
    $warehouse_from = $warehousedespatch_delete->warehouseidFrom['warehouse_name'];
    $clerk = $warehousedespatch->clerk['first_name'] . ' ' . $warehousedespatch->clerk['last_name'];
    $date = date("d-m-Y", strtotime($data['warehouse_despatch_date']));
    $eta_date = isset($data['warehouse_despatch_eta_date']) ? date("d-m-Y", strtotime($data['warehouse_despatch_eta_date'])) : '-';
}

if ($index == 0) {
    ?>
    <thead class="entry-table">
        <tr>
            <th></th>
            <th>No</th>
            <th>Despatch No</th>
            <th>Warehouse From</th>
            <th>Receipt No</th>
            <th>Warehouse To</th>
            <th>clerk</th>
            <th>Date</th>
            <th>ETA</th>

        </tr>
    </thead>
<?php } ?>




<tr>
    <td><span class="icon icon-options-vertical popover-test" data-toggle="popover" data-placement="right" type="button"
            data-html="true" style="cursor: pointer;"></span>
        <div class="popover-content hide">
            <ul class="tooltip-hiden">
                <?php if ($data['delete_status'] == 0) { ?>
                    <li>
                        <a href="index.php?r=wh/warehousedespatch/view&despatchid=<?php echo $data['warehousedespatch_id']; ?>"
                            id="<?php echo $data['warehousedespatch_id']; ?>" class="view_purchase btn btn-xs btn-default"
                            title="View">View</a>
                    </li>
                    <li>
                        <a href="#" id="<?php echo $data['warehousedespatch_id']; ?>"
                            data-id="<?php echo $data['warehousedespatch_id']; ?>"
                            class=" edit_purchase btn btn-xs btn-default" title="Edit">Edit</a>
                    </li>
                <?php } else {
                    if ($data['edit_status'] == 1) { ?>
                        <li>
                            <a href="index.php?r=wh/warehousedespatch/view&despatchid=<?php echo $data['warehousedespatch_id']; ?>"
                                id="<?php echo $data['warehousedespatch_id']; ?>" class="view_purchase btn btn-xs btn-default"
                                title="View">View</a>
                        </li>
                        <li>
                            <a href="#" id="<?php echo $data['warehousedespatch_id']; ?>"
                                data-id="<?php echo $data['warehousedespatch_id']; ?>"
                                class=" edit_purchase btn btn-xs btn-default" title="Edit">Edit</a>
                        </li>
                    <?php } else { ?>
                        <li>
                            <a href="index.php?r=wh/warehousedespatch/view&despatchid=<?php echo $data['warehousedespatch_id']; ?>"
                                id="<?php echo $data['warehousedespatch_id']; ?>" class="view_purchase btn btn-xs btn-default"
                                title="View">View</a>
                        </li>
                    <?php }
                } ?>
            </ul>
        </div>
    </td>
    <td class="text-right">
        <?php echo $index + 1; ?>
    </td>
    <td>
        <?php echo $warehousedespatch_no ?>
    </td>
    <td>
        <?php echo $warehouse_from; ?>
    </td>
    <td>
        <?php echo $warehousereceipt_no; ?>
    </td>
    <td>
        <?php echo $warehouse_to; ?>
    </td>

    <td>
        <?php echo $clerk; ?>
    </td>
    <td class="wrap">
        <?php echo $date; ?>
    </td>
    <td class="wrap">
        <?php echo $eta_date; ?>
    </td>

</tr>