<?php
/* @var $this ExpensesImportController */
/* @var $model ExpensesImport */

$this->breadcrumbs=array(
	'Expenses Imports'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List ExpensesImport', 'url'=>array('index')),
	array('label'=>'Manage ExpensesImport', 'url'=>array('admin')),
);
?>

<h1>Create ExpensesImport</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>