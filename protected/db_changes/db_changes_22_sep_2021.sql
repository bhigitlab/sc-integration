
ALTER TABLE `jp_sales_quotation` ADD `revision_status` INT NOT NULL DEFAULT '0' AFTER `revision_remarks`;

ALTER TABLE `jp_sales_quotation` ADD `revision_approved` INT NOT NULL DEFAULT '0' AFTER `revision_status`;

ALTER TABLE `jp_quotation_worktype_master` ADD `template_id` INT  NULL AFTER `name`;

CREATE TABLE `jp_worktype_template` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `page` varchar(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '1 = Active, 0 = Inactive',
  `created_by` int(11) DEFAULT NULL COMMENT 'From user table',
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `jp_worktype_template` (`id`, `name`, `page`, `status`, `created_by`, `created_date`) VALUES
(1, 'Box Template', '_template1', 1, NULL, '2021-08-26 17:11:14'),
(2, 'Additional ', '_template2', 1, NULL, '2021-08-26 17:11:54'),
(3, 'Other ', '_template3', 1, NULL, '2021-08-26 17:13:15');

ALTER TABLE `jp_worktype_template`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`);

  ALTER TABLE `jp_worktype_template`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

  ALTER TABLE `jp_worktype_template`
  ADD CONSTRAINT `jp_worktype_template_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `jp_users` (`userid`) ON UPDATE CASCADE;

ALTER TABLE `jp_quotation_worktype_master` ADD FOREIGN KEY (`template_id`) REFERENCES `jp_worktype_template`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

UPDATE `jp_quotation_worktype_master`
   SET template_id = CASE id 
                      WHEN 1 THEN 1 
                      WHEN 2 THEN 3 
                      WHEN 3 THEN 3 
                      WHEN 4 THEN 2 
                      END
 WHERE id IN('1', '2','3','4');

ALTER TABLE `jp_sales_quotation` ADD `tax_slab` FLOAT NULL AFTER `revision_approved`,
 ADD `cgst_amount` FLOAT(11,2) NULL AFTER `tax_slab`,
  ADD `cgst_percent` FLOAT(11,2) NULL AFTER `cgst_amount`, 
  ADD `sgst_amount` FLOAT(11,2) NULL AFTER `cgst_percent`, 
  ADD `sgst_percent` FLOAT(11,2) NULL AFTER `sgst_amount`, 
  ADD `igst_amount` FLOAT(11,2)  NULL AFTER `sgst_percent`,
   ADD `igst_percent` FLOAT(11,2)  NULL AFTER `igst_amount`,
   ADD `mainitem_total` FLOAT(11,2) NULL AFTER `igst_percent`,
   ADD `mainitem_tax` FLOAT(11,2) NULL AFTER `mainitem_total`,
   ADD `maintotal_withtax` FLOAT(11,2) NULL AFTER `mainitem_tax`;
