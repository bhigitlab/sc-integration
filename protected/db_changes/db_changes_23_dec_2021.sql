CREATE TABLE  `jp_quotation_section` ( `id` INT NOT NULL AUTO_INCREMENT , `qtn_id` INT NOT NULL , `section_name` VARCHAR(200) NOT NULL , `unit` INT NULL DEFAULT NULL , `quantity` INT NULL DEFAULT NULL , `quantity_nos` INT NULL DEFAULT NULL , `mrp` FLOAT(10,2) NULL DEFAULT NULL , `amount_after_discount` FLOAT(10,2) NULL DEFAULT NULL , `description` VARCHAR(300)  NULL DEFAULT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;

CREATE TABLE  `jp_quotation_gen_category` ( `id` INT NOT NULL AUTO_INCREMENT , `qid` INT NOT NULL , `section_id` INT NOT NULL , `category_label` VARCHAR(200) NOT NULL , `master_cat_id` INT NULL DEFAULT NULL , `unit` INT NULL DEFAULT NULL , `quantity` INT NULL DEFAULT NULL , `quantity_nos` INT NULL DEFAULT NULL , `mrp` FLOAT(10,2) NULL DEFAULT NULL , `amount_after_discount` FLOAT(10,2) NULL DEFAULT NULL , `description` VARCHAR(300) NULL DEFAULT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;

ALTER TABLE `jp_sales_quotation` ADD `parent_type` ENUM('0','1') NULL DEFAULT NULL COMMENT '0=>category,1=>work type' AFTER `subitem_label`, ADD `parent_id` INT NULL DEFAULT NULL AFTER `parent_type`;

CREATE TABLE  `jp_quotation_gen_worktype` ( `id` INT NOT NULL AUTO_INCREMENT , `qid` INT NOT NULL , `section_id` INT NOT NULL , `category_gen_id` INT NOT NULL , `unit` INT NULL DEFAULT NULL , `quantity` INT NULL DEFAULT NULL , `quantity_nos` INT NULL DEFAULT NULL , `mrp` FLOAT(10,2) NULL DEFAULT NULL , `amount_after_discount` FLOAT(10,2) NULL DEFAULT NULL , `description` VARCHAR(300) NULL DEFAULT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;

ALTER TABLE `jp_quotation_section` ADD `created_date` DATETIME NOT NULL AFTER `description`, ADD `created_by` INT NOT NULL AFTER `created_date`;
ALTER TABLE `jp_quotation_gen_category` ADD `created_date` DATETIME NOT NULL AFTER `description`, ADD `created_by` INT NOT NULL AFTER `created_date`;
ALTER TABLE `jp_quotation_gen_worktype` ADD `created_date` DATETIME NOT NULL AFTER `description`, ADD `created_by` INT NOT NULL AFTER `created_date`;
ALTER TABLE `jp_quotation_gen_worktype` ADD `worktype_label` VARCHAR(100) NOT NULL AFTER `category_gen_id`;
ALTER TABLE `jp_quotation_gen_worktype` CHANGE `category_gen_id` `category_label_id` INT(11) NOT NULL;
ALTER TABLE `jp_quotation_gen_worktype` ADD `master_cat_id` INT(11) NULL DEFAULT NULL AFTER `worktype_label`;
ALTER TABLE `jp_sales_quotation` CHANGE `parent_type` `parent_type` INT NULL DEFAULT NULL COMMENT '0=>category,1=>work type';
ALTER TABLE `jp_quotation_gen_worktype` CHANGE `description` `description` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;
ALTER TABLE `jp_quotation_section` ADD `revision_no` VARCHAR(120) NOT NULL AFTER `description`;
ALTER TABLE `jp_quotation_gen_category` ADD `revision_no` VARCHAR(120) NOT NULL AFTER `description`;
ALTER TABLE `jp_quotation_gen_worktype` ADD `revision_no` VARCHAR(120) NOT NULL AFTER `description`;
ALTER TABLE `jp_quotation_section` ADD `ref_id` INT NULL DEFAULT NULL AFTER `id`;
ALTER TABLE `jp_quotation_gen_category` ADD `ref_id` INT NULL DEFAULT NULL AFTER `id`;
ALTER TABLE `jp_quotation_gen_worktype` ADD `ref_id` INT NULL DEFAULT NULL AFTER `id`;
ALTER TABLE  `jp_sales_quotation` DROP FOREIGN KEY jp_sales_quotation_ibfk_1;