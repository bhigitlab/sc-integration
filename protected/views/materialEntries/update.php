<?php
/* @var $this MaterialEntriesController */
/* @var $model MaterialEntries */

$this->breadcrumbs=array(
	'Material Entries'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List MaterialEntries', 'url'=>array('index')),
	array('label'=>'Create MaterialEntries', 'url'=>array('create')),
	array('label'=>'View MaterialEntries', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage MaterialEntries', 'url'=>array('admin')),
);
?>

<h1>Update MaterialEntries <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>