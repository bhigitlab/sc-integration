DELETE FROM `jp_pre_expenses` WHERE invoice_id IS NOT NULL;

DELETE FROM `jp_expenses` WHERE invoice_id IS NOT NULL;

ALTER TABLE jp_pre_expenses DROP FOREIGN KEY exp_inv_fk;

ALTER TABLE jp_expenses DROP FOREIGN KEY fk_invoice_id;

TRUNCATE TABLE `jp_invoice`;

ALTER TABLE jp_pre_expenses  ADD CONSTRAINT exp_inv_fk FOREIGN KEY (invoice_id) REFERENCES jp_invoice (invoice_id) ON UPDATE CASCADE;

ALTER TABLE jp_expenses
  ADD CONSTRAINT fk_invoice_id FOREIGN KEY (invoice_id) REFERENCES jp_invoice (invoice_id) ON UPDATE CASCADE;