<?php
Yii::app()->clientScript->registerScript('re-install-date-picker', "
function reinstallDatePicker(id, data) {
        //use the same parameters that you had set in your widget else the datepicker will be refreshed by default
    $('#datepicker_for_date').datepicker(jQuery.extend({showMonthAfterYear:false},jQuery.datepicker.regional['en-GB'],{'dateFormat':'yy-mm-dd'}));
}


cratecalcdata();

$(document).ajaxStart(function(){
    //

    var user_select = $('.filters select:first').val();

    var dates_selct = $('.filters input:first').val();
    
    //
    var amounts_sel = $('.filters input:eq(1)').val();
    var typesin_sel = $('.filters select:eq(1)').val();
    var project_sel = $('.filters select:eq(2)').val();
    //

    if(user_select == ''){user_select = 0;}
    if(dates_selct == ''){dates_selct = 0;}
    if(amounts_sel == ''){amounts_sel = 0;}
    if(typesin_sel == ''){typesin_sel = 0;}
    if(project_sel == ''){project_sel = 0;}
    //
    cratecalcdata(user_select,dates_selct,amounts_sel,typesin_sel,project_sel);
    //        
});

function cratecalcdata(user_select,dates_selct,amounts_sel,typesin_sel,project_sel){
    //
    $.ajax({
        url: '" . $this->createUrl('expenses/returncalc') . "&user_sel='+user_select+'&date='+dates_selct+'&amount='+amounts_sel+'&type='+typesin_sel+'&project='+project_sel,
        dataType: 'JSON'
    }).success(function(data){
      //  alert(data.Credit);
		
        $('#dataprinthere').html(data.Credit);
        $('#debit').html(data.Debit);
        $('#balance').html(data.Balance);
      
        
    });

    $.ajax({
        url: '" . $this->createUrl('expenses/returncalc') . "&user_sel='+user_select+'&date='+1 +'&amount='+amounts_sel+'&type='+typesin_sel+'&project='+project_sel,
        dataType: 'JSON'
    }).success(function(data){
      //  alert(data.Debit);
       $('#currentbalance').html('Today '+ data.Debit);
        
    });
    
    //
}

");

Yii::app()->clientScript->registerCss('listcss', "
.project-table .row,.activity-table .row,.exp-list .row{
  min-height:100px;
   } 
 ");
?>
<!--<div style="float:right"><a style="cursor:pointer" id="download" class="save_btn">
        <div class="save_pdf"></div>
        SAVE AS PDF</a></div>-->
<div id="contenthtml">
    <div class="container">
        <div class="loading-overlay">
            <span class="fa fa-spinner fa-3x fa-spin"></span>
        </div>
        <!--<div class="row balance-box">

            <div class="col-md-3 col-sm-6 col-xs-6 box-hold">
                <span class="box" id="dataprinthere"></span>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-6 box-hold">
                <span class="box" id="debit"></span>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-6 box-hold">
                <span class="box" id="balance"></span>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-6 box-hold">
                <b><span class="box" id="currentbalance"></span></b>
            </div>
        </div>-->

        <div class="row list-project">
            <div class="col-md-6 project-table">
                <h2>Recent Projects</h2>
                <div style="margin:0 10px;">
                    <?php
                    if (count($newmodel) > 0) {
                        foreach ($newmodel as $new) {
                            $tblpx = Yii::app()->db->tablePrefix;

                            $id = $new['pid'];
                            //echo $id;
                            $credit = Yii::app()->db->createCommand('select project_id,sum(expense_totalamount) as credit from `' . $tblpx . 'expenses`
			where type=72 and project_id=' . $id)->queryAll();
                            //print_r($credit[0]['credit']); 

                            $debit = Yii::app()->db->createCommand('select project_id,sum(expense_totalamount) as debit from `' . $tblpx . 'expenses`
			where type=73 and project_id=' . $id)->queryAll();
                            //print_r($debit); 
                    ?>
                            <?php if (Yii::app()->user->role == 1) { ?>
                                <div class="row-hold projectview" data-id="<?php echo $id; ?>" data-toggle="modal" data-target=".projectView">

                                <?php } else { ?>
                                    <div class="row-hold projectview">
                                    <?php } ?>
                                    <div class="row">
                                        <div class="col-md-8 col-sm-8 col-xs-6">
                                            <div class="list_item"><span>Project: </span><?php echo $new['name']; ?></div>
                                            <div class="list_item desc" id="desc_<?php echo $id; ?>"><span>Description: </span><?php echo $new['description']; ?></div>
                                            <div>&nbsp</div>
                                            <div>&nbsp</div>

                                        </div>
                                        <div class="col-md-4 col-sm-4 col-xs-6">

                                            <div class="list_item">Credit: <?php echo ($credit[0]['credit'] ? number_format($credit[0]['credit']) : 00) ?></div>
                                            <div class="list_item">Debit: <?php echo ($debit[0]['debit'] ? number_format($debit[0]['debit']) : 00) ?></div>
                                            <div>&nbsp</div>
                                        </div>
                                    </div>
                                    </div>

                            <?php
                        }
                    } else {
                        echo "No records found";
                    }
                            ?>
                                </div>
                </div>

                <!--div class="col-md-6 activity-table">
                    <h4>Recent Transactions</h4>
<div style="margin:0 10px;">
<?php
if (count($model) > 0) {
    foreach ($model as $mod) {
        $id = $mod['expense_id'];
        $type = $mod['type'];
        $tr_type = "";

        if ($type == 72) { //receipt
            $ptype = $mod['receipt_type'];
            $sql = Status::model()->find(array(
                'select' => array('caption'),
                "condition" => "sid='$ptype'",
            ));
            $tr_type = "<span>Receipt Type: </span>" . $sql['caption'];
        } else { // expense
            $etype = $mod['expensetype_id'];
            $sql = ExpenseType::model()->find(array(
                'select' => array('type_name'),
                "condition" => "type_id='$etype'",
            ));
            $tr_type = "<span>Expense Type: </span>" . $sql['type_name'];
        }

?>		<?php if (Yii::app()->user->role == 1) { ?>
                            <div class="row-hold expenseview" data-id="<?php echo $id; ?>" data-toggle="modal" data-target=".expenseView">
                                
                        <?php } else { ?>
                                <div class="row-hold expenseview">
                        <?php } ?>
                                <div class="row">
                                    <div class="col-md-8 col-sm-8 col-xs-6">
                                        <div class="list_item"><span>Date: </span><?php if ($mod['expense_date'] != "") {
                                                                                        echo date("d-M-Y", strtotime($mod['expense_date']));
                                                                                    } ?></div>
                                        <div class="list_item"><span>Project: </span><?php echo $mod['name']; ?></div>
                                        <div class="list_item desc" id="desc_<?php echo $id; ?>"><span>Description: </span><?php echo $mod['expense_description']; ?></div>
                                        <div class="list_item"><?php echo $tr_type; ?></div>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-6">
                                        <div class="list_item"><span>Amount: </span><?php echo number_format($mod['expense_totalamount']); ?></div>
                                        <div class="list_item"><span>Type: </span><?php echo $mod['caption']; ?></div>
                                        <div class="list_item"><span>User: </span><?php echo $mod['first_name']; ?></div>
                                        
                                    </div>							
                                </div>
                            </div>	
                                
    <?php
        // print_r($model[0]['exp_id']);
        //echo $id;
    ?>
                            <?php
                        }
                    } else {
                        echo "No records found";
                    }
                            ?></div>
                    </div-->
            </div>
        </div>
    </div>
</div>
</div>
<!-- Expense view Popup -->
<!--<div class="modal fade expenseView" role="dialog">
    <div class="modal-dialog">


    </div>
</div>		
 Project view Popup 
<div class="modal fade projectView" role="dialog">



</div>		-->


<script type="text/javascript">
    $(document).ready(function() {
        $('.projectview').click(function() {
            $('.loading-overlay').addClass('is-active');
            var id = $(this).attr('data-id');
            $.ajax({
                type: "GET",
                url: "<?php echo $this->createUrl('projects/updatepro&id=') ?>" + id,
                success: function(response) {
                    $(".projectView").html(response);
                }
            });

        });


        $('.expenseview').click(function() {
            $('.loading-overlay').addClass('is-active');
            var id = $(this).attr('data-id');
            $.ajax({
                type: "GET",
                url: "<?php echo $this->createUrl('expenses/updateexp&id=') ?>" + id,
                success: function(response) {
                    $(".expenseView").html(response);
                    // $( "#Expenses_date" ).datepicker();


                }
            });

        });
        $('#download').click(function() {
            /* $('#addgroups').css("display","none");
             $('#headcontent').css("display","inline-block"); */

            html2canvas($("#contenthtml"), {
                onrendered: function(canvas) {
                    var imgData = canvas.toDataURL('image/jpeg');

                    var imgWidth = 210;
                    var pageHeight = 295;
                    var imgHeight = canvas.height * imgWidth / canvas.width;
                    var heightLeft = imgHeight;
                    var doc = new jsPDF('p', 'mm');
                    var position = 0;
                    doc.page = 1; // use this as a counter.

                    doc.addImage(imgData, 'jpeg', 0, position, imgWidth, imgHeight);
                    heightLeft -= pageHeight;

                    while (heightLeft >= 0) {
                        position = heightLeft - imgHeight;
                        doc.addPage();
                        doc.addImage(imgData, 'jpeg', 0, position, imgWidth, imgHeight);
                        heightLeft -= pageHeight;
                    }
                    doc.save('expenses.pdf');

                }
            });
        });
        var lengthText = 30;
        var shortText;
        var id;
        //$('.desc').text(shortText);

        $('.desc').hover(function() {
            $(this).text(text);
            //alert($(this).attr('id'));
            id = $(this).attr('id');

            var text = $('#' + id).text();
            shortText = $.trim(text).substring(0, lengthText).split(" ").slice(0, -1).join(" ") + "...";

        }, function() {
            $(id).text(shortText);
        });

    });
    $(document).ajaxComplete(function() {
        $('.loading-overlay').removeClass('is-active');
        $('#loading').hide();
    });
</script>
<style>
    .desc {
        text-overflow: ellipsis;
        /*width: 250px;*/
        white-space: nowrap;
        overflow: hidden;
    }

    .desc:hover {
        text-overflow: clip;
        width: auto;
        white-space: normal;
    }
</style>