<?php
/* @var $this BillsController */
/* @var $model Bills */

$this->breadcrumbs = array(
    'Bills' => array('index'),
    'Create',
);

$this->menu = array();
?>

<div class="container" id="expense">
    <div class="loading-overlay">
        <span class="fa fa-spinner fa-3x fa-spin"></span>
    </div>
    <div class="clearfix">
        <div class="add-btn pull-right">
            <a href="index.php?r=bills/admin" class="button btn btn-primary">List Bills</a>
        </div>
        <h2>Bills</h2>
        <div id="loading"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/ajax-loader.gif"></div>
    </div>
    <?php $this->renderPartial('_form', array('model' => $model, 'model2' => $model2)); ?>
</div>
<script>
    $(document).ready(function() {
        $('#loading').hide();
    });
    $("#Bills_purchase_id").change(function(){        
        $("#tax_slab").trigger("change");
    }) 
</script>