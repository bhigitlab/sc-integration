<?php

class ProjectsController extends Controller
{
    /*
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning

     * using two-column layout. See 'protected/views/layouts/column2.php'.

     */

    public $layout = '//layouts/column2';

    /*
     * @return array action filters

     */

    public function filters()
    {

        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /*
     * Specifies the access control rules.

     * This method is used by the 'accessControl' filter.

     * @return array access control rules

     */

    public function accessRules()
    {
        $controller = Yii::app()->controller->id;
        $hidden_actions =  array('dynamicproject','ExportItemQuantityReportCSV','saveitemquantityreportpdf','removeproject','getTemplateDetails');
        $rules = AccessRulesHelper::getRules($controller, $hidden_actions);
        //Additional action rules

        return $rules;
    }

    public function actiongetProjects()
    {



        $userid = $_POST['userid'];



        if (Yii::app()->user->role == 1 || Yii::app()->user->role == 2) {

            $projects = CHtml::listData(Projects::model()->findAll(array(
                'select' => array('pid, name'),                
                'order' => 'name',
                'distinct' => true
            )), 'pid', 'name');
        } else {



            $projects = CHtml::listData(Projects::model()->findAll(array(
                'select' => array('pid, name'),
                "condition" => 'pid IN (SELECT projectid FROM ' . $tblpx . 'project_assign WHERE  userid=' . $userid . ' AND  projectid in (SELECT projectid FROM ' . $tblpx . 'project_assign WHERE userid=' . Yii::app()->user->id . '))',
                'order' => 'name',
                'distinct' => true
            )), 'pid', 'name');
        }

        if (count($projects)) {

            $projlist = '<option value="">Select Project</option>';

            foreach ($projects as $pid => $pname) {

                $projlist .= '<option value="' . $pid . '">' . $pname . '</option>';
            }
        } else {

            $projlist = '<option value="">No Project assigned</option>';
        }

        $projarray = array('projects' => $projlist);

        echo json_encode($projarray);
    }

    /**

     * Displays a particular model.

     * @param integer $id the ID of the model to be displayed

     */

    public function actionView($id)
    {

        $pid = $id;
        $expmodel = new Expenses('search');
        $expmodel->unsetAttributes();  // clear any default values
        if (isset($_GET['Expenses']))
            $expmodel->attributes = $_GET['Expenses'];
        $this->render('view', array(
            'model' => $this->loadModel($id), 'pid' => $pid, 'expmodel' => $expmodel
        ));
    }

    /**

     * Creates a new model.

     * If creation is successful, the browser will be redirected to the 'view' page.

     */
    public function actionCreate()
    {
        $tblpx  = Yii::app()->db->tablePrefix;
        if (isset($_GET['layout']))
            $this->layout = false;
        $model  = new Projects;
        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);
        if (isset($_POST['Projects'])) {
            $model->attributes      = $_POST['Projects'];
            $model->created_by      = yii::app()->user->id;
            $model->created_date    = date('Y-m-d');
            $model->updated_by      = yii::app()->user->id;
            $model->updated_date    = date('Y-m-d');
            $model->completion_date = date("Y-m-d", strtotime($_POST['Projects']['completion_date']));
            $model->start_date      = date("Y-m-d", strtotime($_POST['Projects']['start_date']));
            if (isset($_POST['Projects']['labour_template']) && !empty($_POST['Projects']['labour_template'])) {
                $selected_templates = isset($_POST['Projects']['labour_template']) ? $_POST['Projects']['labour_template'] : [];
                $template_ids_str = implode(',', $selected_templates);
                $model->labour_template=$template_ids_str;
            }
            if (Yii::app()->user->role == 10)
                $model->project_status = 83;
            if (Yii::app()->user->role != 10) {
                $model->sqft            = $_POST['Projects']['sqft'];
                //Added By Athira
                if ($model->project_type == 87) {
                    $model->sqft_rate   = $_POST['Projects']['sqft_rate'];
                    $model->percentage  = "";
                }
                if ($model->project_type == 86) {
                    $model->percentage  = $_POST['Projects']['percentage'];
                    $model->sqft_rate   = "";
                }
                //Added By Athira
                $model->bill_amount     = isset($_POST['Projects']['bill_amount']) ? $_POST['Projects']['bill_amount'] : 0;
                $model->project_quote   = $_POST['Projects']['project_quote'];
                $model->profit_margin   = $_POST['Projects']['profit_margin'];
            }
            $model->auto_update = $_POST['Projects']['auto_update'];
            if (isset($_POST['Projects']['project_category']) && !empty($_POST['Projects']['project_category']))
                $model->project_category = $_POST['Projects']['project_category'];
            if (isset($_POST['Projects']['project_duration']) && !empty($_POST['Projects']['project_duration']))
                $model->project_duration = $_POST['Projects']['project_duration'];
            $model->remarks         = $_POST['Projects']['remarks'];
            if (!empty($_POST['Projects']['company_id'])) {
                $company            = implode(",", $_POST['Projects']['company_id']);
                $model->company_id  = $company;
            } else {
                $model->company_id  = NULL;
            }

            $assigned_to_arr = array();
            $assigned_to = NULL;
            $warehouse_place = NULL;
            $warehouse_address = NULL;

            if (!empty($_POST['user'])) {
                $assigned_to_arr_all = $_POST['user'];
                $assigned_to = NULL;
                foreach ($assigned_to_arr_all as $key => $value) {
                    if($value!=""){
                        $sql = "SELECT * FROM {$tblpx}users WHERE userid=" . $value;
                        $users = Yii::app()->db->createCommand($sql)->queryRow();
                        if ($users['user_type'] == 5) {
                            array_push($assigned_to_arr,$value);                        
                        }
                    }
                }
                if(!empty($assigned_to_arr)){
                    $assigned_to = implode(",", $assigned_to_arr);
                }                                 
            } 
            if (!empty($_POST['Projects']['site'])) {
                $warehouse_place = $_POST['Projects']['site'];
            } 
            if (!empty($_POST['Projects']['description'])) {
                $warehouse_address = $_POST['Projects']['description'];
            } 
            $model->incharge = isset($_POST['Projects']['incharge'])?$_POST['Projects']['incharge']:'';
            
            if ($model->save()) {
                $projectid  = Yii::app()->db->getLastInsertID();
                $model2 = new Warehouse;
                $model2->warehouse_name = "warehouse-" . $_POST['Projects']['name'];
                $model2->warehouse_place = $warehouse_place;
                $model2->warehouse_address = $warehouse_address;
                $model2->assigned_to = $assigned_to;
                $model2->warehouse_incharge = yii::app()->user->id;
                $model2->status = 1;
                $model2->created_by = yii::app()->user->id;
                $model2->project_id = $projectid;
                $model2->created_date = date('Y-m-d');
                $model2->company_id = Yii::app()->user->company_id;
                $model2->updated_by = yii::app()->user->id;
                $model2->updated_date = date('Y-m-d');
                $model2->save(false);
                
                if (!empty($model->flat_names) && !empty($model->number_of_flats)) {
                    $this->saveFlatDetails($model->flat_names, $projectid);
                }
                $query      = array();
                if (isset($_POST['wrk_type']) && count($_POST['wrk_type'])) {
                    $type           = $_POST['wrk_type'];
                    foreach ($type as $ty) {
                        $query4[]   = '(null, ' . $projectid . ', ' . $ty . ')';
                    }
                    $insertassign2  = 'insert into ' . $tblpx . 'project_work_type values ' . implode(',', $query4);
                    Yii::app()->db->createCommand($insertassign2)->query();
                }
                if (Yii::app()->user->role != 10) {
                    if (isset($_POST['wrk_typepms']) && count($_POST['wrk_typepms'])) {
                        $type1          = $_POST['wrk_typepms'];
                        foreach ($type1 as $ty) {
                            $query6[]   = '(null, ' . $projectid . ', ' . $ty . ')';
                        }
                        if (!empty($query6)) {
                            $tableName = $this->getAssignProjectTableName();
                            $insertassign2 = 'insert into '.$tableName.' values ' . implode(',', $query6);
                            Yii::app()->db->createCommand($insertassign2)->query();
                        }
                    }
                }
                if (isset($_POST['user'])) {
                    $user = $_POST['user'];
                    foreach ($user as $u) {
                        $query[] = '(null, ' . $projectid . ', ' . $u . ')';
                    }
                    $insertassign = 'insert into ' . $tblpx . 'project_assign values ' . implode(',', $query);
                    Yii::app()->db->createCommand($insertassign)->query();
                }

                $labour_query      = array();
                if (isset($_POST['Projects']['labour_template'])) {
                    
                    $res = Yii::app()->db->createCommand("SELECT DISTINCT template_id from " . $tblpx . "project_labour WHERE project_id = " . $projectid)->queryScalar();
                    if ($res) {                        
                        Yii::app()->db->createCommand('delete from ' . $tblpx . 'project_labour where project_id=' . $projectid )->query();                        
                    }

                    // $template = Yii::app()->db->createCommand("SELECT * FROM `jp_labour_template` WHERE `id` = ".$_POST['Projects']['template'])->queryRow();

                    // if(!empty($template)){
                    //     $template_id=$_POST['Projects']['template'];
                    //     $arrVal = explode(',', $template['labour_type']);                    
                    //     foreach ($arrVal as $val) {
                    //     $labour_rate = Yii::app()->db->createCommand("SELECT worktype_rate FROM `jp_labour_template_worktype` WHERE `template_id`= $template_id AND `worktype_id` = $val")->queryScalar();
                       
                    //     }
                    // }
                    
                   
                    if(!empty($_POST['Projects']['lab_templates'])){
                        
                         foreach($_POST['Projects']['lab_templates'] as $template_id => $labour_types){
                            foreach ($labour_types as $key => $val) {
                            $rate = floatval(str_replace(',', '', $val["rate"]));
                            $labour_query[] = '(null, ' . $projectid . ', ' . $val["template_id"] . ',' . $val["type_id"] . ',' .  $rate . ')';
                            }
                         }
                       
                    }
                    //echo "<pre>";print_r($labour_query);exit;
                    if (!empty($labour_query)) {
                        $insertassign = 'insert into ' . $tblpx . 'project_labour values ' . implode(',', $labour_query);
                        Yii::app()->db->createCommand($insertassign)->query();
                    }
                } else {
                    if (empty($_POST['Projects']['labour_template'])) {
                        Yii::app()->db->createCommand('delete from ' . $tblpx . 'project_labour where project_id=' . $projectid)->query();
                        $newmod = Projects::Model()->findByPk($projectid);
                        $newmod->labour_template='';
                        $newmod->save();

                    }
                }
                //Api integration 
                $pms_api_integration=ApiSettings::model()->pmsIntegrationStatus();

                if($pms_api_integration ==1){
                    $labourTemplateIds = explode(',', $model->labour_template);
                    $labourTemplateIds = explode(',', $model->labour_template);
                    $labourTemplates = CHtml::listData(LabourTemplate::model()->findAllByPk($labourTemplateIds),'pms_labour_template_id','pms_labour_template_id');
                    $pmsLabourTemplateIds = implode(',', $labourTemplates);
                    $request = [
                        'project_id' => $model->pid,
                        'client_id' => $model->client_id,
                        'name' => $model->name,
                        'start_date' => $model->start_date,
                        'end_date' => $model->completion_date,
                        'origin' => 'coms',
                        'pms_labour_templates'=>$pmsLabourTemplateIds,
                    ];
                    $slug="api/create-project";
                    $method="POST";
                        
                    $globalapi_response = GlobalApiCall::callApi($slug, $method, $request);
                }

                if (isset($_POST['type']) && count($_POST['type'])) {
                    $type = $_POST['type'];
                    foreach ($type as $ty) {
                        $query2[]   = '(null, ' . $projectid . ', ' . $ty . ')';
                    }
                    $insertassign2  = 'insert into ' . $tblpx . 'project_exptype values ' . implode(',', $query2);
                    Yii::app()->db->createCommand($insertassign2)->query();
                }
            }            
            $this->redirect(array('newList'));
        }
        $this->render('create2', array(
            'model' => $model,
        ));
    }

    /**

     * Updates a particular model.

     * If update is successful, the browser will be redirected to the 'view' page.

     * @param integer $id the ID of the model to be updated

     */
    public function actionUpdate($id)
    {
        $tblpx = Yii::app()->db->tablePrefix;
        if (isset($_GET['layout']))
            $this->layout = false;
        $model = $this->loadModel($id);



        // Uncomment the following line if AJAX validation is needed

        $this->performAjaxValidation($model);

        

        if (isset($_POST['Projects'])) {
           
            $model->attributes = $_POST['Projects'];
            $model->updated_by = yii::app()->user->id;
            $model->updated_date = date('Y-m-d');
            //Added By Athira
            if ($model->project_type == 87) {
                $model->sqft_rate = $_POST['Projects']['sqft_rate'];
                $model->percentage = "";
            }
            if ($model->project_type == 86) {
                $model->percentage = $_POST['Projects']['percentage'];
                $model->sqft_rate = "";
            }
            //Added By Athira
            $model->sqft = $_POST['Projects']['sqft'];
            $model->bill_amount = isset($_POST['Projects']['bill_amount']) ? $_POST['Projects']['bill_amount'] : 0;
            $model->remarks = $_POST['Projects']['remarks'];
            $model->completion_date = date("Y-m-d", strtotime($_POST['Projects']['completion_date']));
            $model->start_date = date("Y-m-d", strtotime($_POST['Projects']['start_date']));
            $model->project_quote = $_POST['Projects']['project_quote'];
            $model->profit_margin = $_POST['Projects']['profit_margin'];
            $model->auto_update = $_POST['Projects']['auto_update'];
            if (isset($_POST['Projects']['labour_template'])) {

                $selected_templates = isset($_POST['Projects']['labour_template']) ? $_POST['Projects']['labour_template'] : [];
                $template_ids_str = implode(',', $selected_templates);
                $model->labour_template=$template_ids_str;
            }
            if (isset($_POST['Projects']['project_category']) && !empty($_POST['Projects']['project_category']))
                $model->project_category = $_POST['Projects']['project_category'];
            if (isset($_POST['Projects']['project_duration']) && !empty($_POST['Projects']['project_duration']))
                $model->project_duration = $_POST['Projects']['project_duration'];
            if (!empty($_POST['Projects']['company_id'])) {
                $company = implode(",", $_POST['Projects']['company_id']);
                $model->company_id = $company;
            } else {
                $model->company_id = NULL;
            }
            $model->incharge = isset($_POST['Projects']['incharge'])?$_POST['Projects']['incharge']:'';
            if ($model->save()) {
                $projectid = $id;
                if (!empty($model->flat_names) && !empty($model->number_of_flats)) {
                    $this->saveFlatDetails($model->flat_names, $projectid);
                }

                $query_part = array();
                $tbl_px = Yii::app()->db->tablePrefix;
                if (isset($_POST['user']) && count($_POST['user'])) {
                    $userArr = array();
                    $res = Yii::app()->db->createCommand("SELECT userid from " . $tblpx . "project_assign WHERE projectid = " . $projectid)->queryAll();
                    foreach ($res as $value) {
                        if (!in_array($value['userid'], $_POST['user'])) {
                            Yii::app()->db->createCommand('delete from ' . $tblpx . 'project_assign where projectid=' . $projectid . ' AND userid =' . $value['userid'])->query();
                        }
                        $userArr[] = $value['userid'];
                    }
                    foreach ($_POST['user'] as $uid) {

                        if (!in_array($uid, $userArr)) {
                            $query_part[] = '(null, ' . $projectid . ', ' . $uid . ')';
                        }
                    }

                    if (!empty($query_part)) {
                        $insertassign = 'insert into ' . $tblpx . 'project_assign values ' . implode(',', $query_part);
                        Yii::app()->db->createCommand($insertassign)->query();
                    }
                } else {
                    if (empty($_POST['user'])) {
                        Yii::app()->db->createCommand('delete from ' . $tblpx . 'project_assign where projectid=' . $projectid)->query();
                    }
                }

                $labour_query      = array();
                if (isset($_POST['Projects']['labour_template'])) {
                    
                    $res = Yii::app()->db->createCommand("SELECT DISTINCT template_id from " . $tblpx . "project_labour WHERE project_id = " . $projectid)->queryScalar();
                    if ($res) {                        
                        Yii::app()->db->createCommand('delete from ' . $tblpx . 'project_labour where project_id=' . $projectid )->query();                        
                    }

                    
                    
                   
                    if(!empty($_POST['Projects']['lab_templates'])){
                        
                         foreach($_POST['Projects']['lab_templates'] as $template_id => $labour_types){
                            foreach ($labour_types as $key => $val) {
                            $rate = floatval(str_replace(',', '', $val["rate"]));
                            $labour_query[] = '(null, ' . $projectid . ', ' . $val["template_id"] . ',' . $val["type_id"] . ',' .  $rate . ')';
                            }
                         }
                       
                    }
                    //echo "<pre>";print_r($labour_query);exit;
                    if (!empty($labour_query)) {
                        $insertassign = 'insert into ' . $tblpx . 'project_labour values ' . implode(',', $labour_query);
                        Yii::app()->db->createCommand($insertassign)->query();
                    }
                } else {
                    if (empty($_POST['Projects']['labour_template'])) {
                        Yii::app()->db->createCommand('delete from ' . $tblpx . 'project_labour where project_id=' . $projectid)->query();
                        $newmod = Projects::Model()->findByPk($projectid);
                        $newmod->labour_template='';
                        $newmod->save();

                    }
                }


                if (isset($_POST['type']) && count($_POST['type'])) {

                    $type = $_POST['type'];

                    $userArr = array();
                    $res = Yii::app()->db->createCommand("SELECT type_id from " . $tbl_px . "project_exptype WHERE project_id = " . $projectid)->queryAll();
                    foreach ($res as $value) {
                        if (!in_array($value['type_id'], $_POST['type'])) {
                            Yii::app()->db->createCommand('delete from ' . $tbl_px . 'project_exptype where project_id=' . $projectid . ' AND type_id =' . $value['type_id'])->query();
                        }
                        $userArr[] = $value['type_id'];
                    }
                    foreach ($type as $ty) {
                        if (!in_array($ty, $userArr)) {
                            $query2[] = '(null, ' . $projectid . ', ' . $ty . ')';
                        }
                    }
                    if (!empty($query2)) {
                        $insertassign2 = 'insert into ' . $tbl_px . 'project_exptype values ' . implode(',', $query2);

                        Yii::app()->db->createCommand($insertassign2)->query();
                    }
                } else {
                    if (empty($_POST['type'])) {
                        Yii::app()->db->createCommand('delete from ' . $tbl_px . 'project_exptype where project_id=' . $projectid)->query();
                    }
                }

                //Api integration 
                $pms_api_integration=ApiSettings::model()->pmsIntegrationStatus();

                if ($_POST['execute_api'] == 1) {
                    if($pms_api_integration ==1){

                        $labourTemplateIds = explode(',', $model->labour_template);
                        $labourTemplates = CHtml::listData(LabourTemplate::model()->findAllByPk($labourTemplateIds),'pms_labour_template_id','pms_labour_template_id');
                        $pmsLabourTemplateIds = implode(',', $labourTemplates);
                        
                        $request = [
                            'project_id' => $projectid,
                            'client_id' => $model->client_id,
                            'coms_api_log_id'=>$model->pid,
                            'name' => $model->name,
                            'start_date' => $model->start_date,
                            'end_date' => $model->completion_date,
                            'origin' => 'coms',
                            'pms_labour_templates'=>$pmsLabourTemplateIds,
                            'pms_project_id'=>$model->pms_project_id
                        ];

                        if (!empty($model->pms_project_id)) {
                            $slug = "api/update-project";
                            $request['pms_project_id'] = $model->pms_project_id;
                        } else {
                            $slug = "api/create-project";
                        }
                        $method="POST";
                            
                        $globalapi_response = GlobalApiCall::callApi($slug, $method, $request);
                    }
                }
                if (isset($_POST['wrk_type']) && count($_POST['wrk_type'])) {
                    $type = $_POST['wrk_type'];
                    $userArr = array();
                    $res = Yii::app()->db->createCommand("SELECT wrk_type_id from " . $tbl_px . "project_work_type WHERE project_id = " . $projectid)->queryAll();
                    foreach ($res as $value) {
                        if (!in_array($value['wrk_type_id'], $_POST['wrk_type'])) {
                            Yii::app()->db->createCommand('delete from ' . $tbl_px . 'project_work_type where project_id=' . $projectid . ' AND wrk_type_id =' . $value['wrk_type_id'])->query();
                        }
                        $userArr[] = $value['wrk_type_id'];
                    }
                    foreach ($type as $ty) {
                        if (!in_array($ty, $userArr)) {
                            $query2[] = '(null, ' . $projectid . ', ' . $ty . ')';
                        }
                    }
                    if (!empty($query2)) {
                        $insertassign2 = 'insert into ' . $tbl_px . 'project_work_type values ' . implode(',', $query2);

                        Yii::app()->db->createCommand($insertassign2)->query();
                    }
                } else {
                    if (empty($_POST['wrk_type'])) {
                        Yii::app()->db->createCommand('delete from ' . $tbl_px . 'project_work_type where project_id=' . $projectid)->query();
                    }
                }


                if (isset($_POST['wrk_typepms']) && count($_POST['wrk_typepms'])) {

                    $type1 = $_POST['wrk_typepms'];

                    $userArr = array();
                    $tableName = $this->getAssignProjectTableName();
                    $sql = "SELECT wrk_type_id from ".$tableName
                            ."  WHERE project_id = " . $projectid;
                    $res = Yii::app()->db->createCommand($sql)->queryAll();
                    foreach ($res as $value) {
                        if (!in_array($value['wrk_type_id'], $_POST['wrk_typepms'])) {
                            Yii::app()->db->createCommand('delete from '.$tableName.' where project_id=' . $projectid . ' AND wrk_type_id =' . $value['wrk_type_id'])->query();
                        }
                        $userArr[] = $value['wrk_type_id'];
                    }
                    foreach ($type1 as $ty) {
                        if (!in_array($ty, $userArr)) {
                            $query2[] = '(null, ' . $projectid . ', ' . $ty . ')';
                        }
                    }
                    if (!empty($query2)) {
                        $tableName = $this->getAssignProjectTableName();
                        $insertassign2 = 'insert into ' . $tableName
                        . ' values ' . implode(',', $query2);

                        Yii::app()->db->createCommand($insertassign2)->query();
                    }
                } else {
                    if (empty($_POST['wrk_typepms'])) {
                        $tableName = $this->getAssignProjectTableName();
                        $sql = 'delete from '.$tableName.' where project_id=' . $projectid;
                        Yii::app()->db->createCommand($sql)->query();
                    }
                }

                if (Yii::app()->user->returnUrl != 0) {
                    $this->redirect(array('newlist', 'user_page' => Yii::app()->user->returnUrl));
                } else {
                    $this->redirect(array('newlist'));
                }
            }
        }
        $this->render('update', array(
            'model' => $model,
        ));
    }

    public function actionUpdatepro($id)
    {

        $model = $this->loadModel($id);
        $tblpx = Yii::app()->db->tablePrefix;


        // Uncomment the following line if AJAX validation is needed

        $this->performAjaxValidation($model);



        if (isset($_POST['Projects'])) {

            $model->attributes = $_POST['Projects'];
            $model->updated_by = yii::app()->user->id;
            $model->updated_date = date('Y-m-d');
            $model->sqft = $_POST['Projects']['sqft'];
            $model->bill_amount = isset($_POST['Projects']['bill_amount']) ? $_POST['Projects']['bill_amount'] : 0;

            $model->remarks = $_POST['Projects']['remarks'];
            if ($model->save()) {
                $projectid = $id;
                $query_part = array();
                if (isset($_POST['user']) && count($_POST['user'])) {
                    foreach ($_POST['user'] as $uid) {
                        $query_part[] = '(null, ' . $projectid . ', ' . $uid . ')';
                    }
                    $insertassign = 'insert into ' . $tblpx . 'project_assign values ' . implode(',', $query_part);
                    Yii::app()->db->createCommand($insertassign)->query();
                }

                if (isset($_POST['type']) && count($_POST['type'])) {

                    $type = $_POST['type'];
                    foreach ($type as $ty) {
                        $query2[] = '(null, ' . $projectid . ', ' . $ty . ')';
                    }

                    $insertassign2 = 'insert into ' . $tblpx . 'project_exptype values ' . implode(',', $query2);



                    //Yii::app()->db->createCommand('delete from '.$tblpx.'project_exptype where project_id=' . $projectid)->query();

                    Yii::app()->db->createCommand($insertassign2)->query();
                }



                $this->redirect(array('expenses/list'));
            }
        }



        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**

     * Deletes a particular model.

     * If deletion is successful, the browser will be redirected to the 'admin' page.

     * @param integer $id the ID of the model to be deleted

     */

    /**

     * Lists all models.

     */



    public function actionIndex()
    {



        $model2 = new Projects;



        // Uncomment the following line if AJAX validation is needed

        $this->performAjaxValidation($model2);



        if (isset($_POST['Projects'])) {

            $model2->attributes = $_POST['Projects'];



            $model2->created_by = yii::app()->user->id;

            $model2->created_date = date('Y-m-d');



            $model2->updated_by = yii::app()->user->id;

            $model2->updated_date = date('Y-m-d');



            //print_r($_POST);



            if ($model2->save()) {



                $projectid = Yii::app()->db->getLastInsertID();



                $query_part = array();

                if (isset($_POST['user']) && count($_POST['user'])) {

                    foreach ($_POST['user'] as $uid) {

                        $query_part[] = '(null, ' . $projectid . ', ' . $uid . ')';
                    }



                    $insertassign = 'insert into ' . $tblpx . 'project_assign values ' . implode(',', $query_part);



                    //Yii::app()->db->createCommand('delete from project_assign where projectid=' . $projectid)->query();

                    Yii::app()->db->createCommand($insertassign)->query();
                }



                $this->redirect(array('index'));
            }
        }



        $model = new Projects('search');

        $model->unsetAttributes();  // clear any default values

        if (isset($_GET['Projects']))
            $model->attributes = $_GET['Projects'];



        $this->render('index', array(
            'model' => $model, 'model2' => $model2,
        ));
    }

    public function actionNewlist()
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $model = new Projects('customsearch');
        $model->unsetAttributes();  // clear any default values
        if (isset($_REQUEST['Projects']))
            $model->attributes = $_REQUEST['Projects'];


        $where = 'WHERE 1=1';

        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        $newQuery = "";
        foreach ($arrVal as $arr) {
            if ($newQuery) $newQuery .= ' OR';
            $newQuery .= " FIND_IN_SET('" . $arr . "', company_id)";
        }

        $where .= ' AND (' . $newQuery . ')';

        if (isset($_REQUEST['Projects']['name'])) {
            $pname = $_REQUEST['Projects']['name'];
            $where .= ' AND name LIKE "%' . $pname . '%"';
        }
        /*-------------------Mod by Rajisha---------------------*/
        if (!empty($_REQUEST['Projects']['project_status'])) {
            $where .= ' AND project_status = "' . $_REQUEST['Projects']['project_status'] . '"';
        }
        /*-------------------Mod by Rajisha---------------------*/
        if (Yii::app()->user->role == 1) {

            $data = Yii::app()->db->createCommand("SELECT * FROM `{$tblpx}projects`" . $where)->queryAll();
            $crm_api_integration=ApiSettings::model()->crmIntegrationStatus();
            if($crm_api_integration==1){
                $data = Yii::app()->db->createCommand("SELECT * FROM `{$tblpx}projects`" )->queryAll();
            }
            foreach ($data as $key => $value) {

                $data[$key]['ex_percent'] = $this->getPercentageExpense($value['pid']);
            }
            $sort = new CSort();
            $sort->attributes = array(
                'ex_percent' => array(
                    'asc' => 'ex_percent ASC',
                    //'desc'=>'ex_percent DESC',
                )
            );

            $dataprovider = new CArrayDataProvider($data, array(
                'id' => 'user',
                'keyField' => 'pid',
                'sort' =>$sort,
                'pagination' => false

            ));
            $this->render('newlist', array(
                'model' => $model,
                'dataProvider' => $dataprovider //$model->customsearch(),
            ));
        } else {
            $data = Yii::app()->db->createCommand("SELECT * FROM `{$tblpx}projects` p JOIN `{$tblpx}project_assign` pa ON p.pid = pa.projectid " . $where . " AND (pa.userid = '" . Yii::app()->user->id . "' OR p.created_by =  '" . Yii::app()->user->id . "') GROUP BY p.pid")->queryAll();
            foreach ($data as $key => $value) {
                $data[$key]['ex_percent'] = $this->getPercentageExpense($value['pid']);
            }
            $sort = new CSort();
            $sort->attributes = array(
                'ex_percent' => array(
                    'asc' => 'ex_percent ASC',
                    //'desc'=>'ex_percent DESC',
                )
            );
            $dataprovider = new CArrayDataProvider($data, array(
                'id' => 'user',
                'keyField' => 'pid',
                'sort' =>$sort,
                'pagination' => false

            ));
            $this->render('newlist', array(
                'model' => $model,
                'dataProvider' => $dataprovider,
            ));
        }
        /*------------------MOdified By Rajisha----------------*/
    }
    public function getPercentageExpense($id)
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $receipt = Yii::app()->db->createCommand("SELECT SUM(amount) as recamount FROM {$tblpx}expenses WHERE projectid = {$id} AND type=72")->queryScalar();
        $expense = Yii::app()->db->createCommand("SELECT SUM(amount) as recamount FROM {$tblpx}expenses WHERE projectid = {$id} AND type=73")->queryScalar();


        /*------------------Mod By Rajisha calculate percentage debit/credit---------------------*/
        if ($receipt > 0) {
            return  number_format($expense / $receipt * 100);
        }
        /*------------------Mod By Rajisha calculate percentage debit/credit---------------------*/
    }
    /*

     * Manages all models.

     */

    public function actionAdmin()
    {

        $model = new Projects('search');

        $model->unsetAttributes();  // clear any default values

        if (isset($_GET['Projects']))
            $model->attributes = $_GET['Projects'];



        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /*

     * Returns the data model based on the primary key given in the GET variable.

     * If the data model is not found, an HTTP exception will be raised.

     * @param integer the ID of the model to be loaded

     */

    public function loadModel($id)
    {

        $model = Projects::model()->findByPk($id);

        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');

        return $model;
    }

    /*

     * Performs the AJAX validation.

     * @param CModel the model to be validated

     */

    protected function performAjaxValidation($model)
    {

        if (isset($_POST['ajax']) && $_POST['ajax'] === 'projects-form') {

            echo CActiveForm::validate($model);

            Yii::app()->end();
        }
    }

    public function actionprojectList()
    {
        $tblpx = Yii::app()->db->tablePrefix;
        if (isset($_POST['project']) && !empty($_POST['project'])) {
            $name = $_POST['project'];
            $sql = Yii::app()->db->createCommand("select pid,name,caption from " . $tblpx . "projects

				inner join " . $tblpx . "status on " . $tblpx . "projects.status = " . $tblpx . "status.sid WHERE name LIKE '%$name%' order by pid DESC

				")->queryAll();
        } else {
            $sql = Yii::app()->db->createCommand("select pid,name,caption from " . $tblpx . "projects

			inner join " . $tblpx . "status on " . $tblpx . "projects.status = " . $tblpx . "status.sid order by pid DESC

			")->queryAll();
        }

        $this->render('list', array(
            'model' => $sql,
        ));
    }

    public function actionGetexptypes()
    {

        $tblpx = Yii::app()->db->tablePrefix;
        $pid = $_POST['pid'];


        $tblpx = Yii::app()->db->tablePrefix;


        $exptypes = CHtml::listData(ExpenseType::model()->findAll(array(
            'select' => array('type_id, type_name'),
            "condition" => 'type_id in (select type_id from ' . $tblpx . 'project_exptype where project_id=' . $pid . ')',
            'order' => 'type_name',
            'distinct' => true
        )), 'type_id', 'type_name');



        if (count($exptypes)) {

            $projexplist = '<option value="">Select Expense type</option>';

            foreach ($exptypes as $tid => $tname) {

                $projexplist .= '<option value="' . $tid . '">' . $tname . '</option>';
            }
        } else {

            $projexplist = '<option value="">No Expense types to select</option>';
        }

        $exparray = array('exptypes' => $projexplist);

        echo json_encode($exparray);
    }

    public function actionFinancialreport()
    {

        $model = new Projects('financialreport1');

        $model->unsetAttributes();

        if (isset($_GET['Projects'])) {
            $model->attributes = $_GET['Projects'];
        }

        $financialdata1 = $model->financialreport1();
        $fixedprojects = $model->fixedprojects();
        $completed = $model->completed();
        $buyer_module = GeneralSettings::model()->checkBuyerModule();
        $render_datas = array(
            'model' => $model,
            'financialdata1' => $financialdata1,
            'fixedprojects' => $fixedprojects,
            'completed' => $completed,
            'buyer_module' => $buyer_module
        );
        if (filter_input(INPUT_GET, 'export') == 'pdf') {
            $this->logo = $this->realpath_logo;
            $mPDF1 = Yii::app()->ePdf->mPDF();
            $mPDF1 = Yii::app()->ePdf->mPDF('', 'A4');
            $sql = 'SELECT template_name '
                . 'FROM jp_quotation_template '
                . 'WHERE status="1"';
            $selectedtemplate =Yii::app()->db->createCommand($sql)->queryScalar();
        
            $template = $this->getTemplate($selectedtemplate);
            $mPDF1->SetHTMLHeader($template['header']);
            $mPDF1->SetHTMLFooter($template['footer']);
            $mPDF1->AddPage('','', '', '', '', 0,0,40, 30, 10,0);

            $mPDF1->WriteHTML($this->renderPartial('financialreportview', $render_datas, true));
            $mPDF1->Output('FINANCIAL-REPORT-VIEW.pdf', 'D');
        } elseif (filter_input(INPUT_GET, 'export') == 'csv') {
            $exported_data = $this->renderPartial('financialreportview', $render_datas, true);
            $export_filename = 'FINANCIAL-REPORT-VIEW' . date('Ymd_His');
            ExportHelper::exportGridAsCSV($exported_data, $export_filename);
        } else {
            $this->render('financialreportview', $render_datas);
        }
    }
    public function actionCompletedprojects()
    {

        $model = new Projects('financialreportcompleted1');

        $model->unsetAttributes();

        if (isset($_GET['Projects'])) {



            $model->attributes = $_GET['Projects'];
        } else {
        }



        $financialdata1 = $model->financialreportcompleted1();

        $fixedprojects = $model->fixedprojectscompleted();

        $completed = $model->completed();

        $this->render('completedprojectsview', array(
            'model' => $model,
            'financialdata1' => $financialdata1,
            'fixedprojects' => $fixedprojects,
            'completed' => $completed,
        ));
    }

    public function actionSavetopdf()
    {

        $model = new Projects('financialreport1');

        $model->unsetAttributes();


        $financialdata1 = $model->financialreport1();


        $fixedprojects = $model->fixedprojects();

        $completed = $model->completed();



        $entry_data = json_decode($this->entries($financialdata1, 'rate'), true);
        $prev_entries = $entry_data['final_data'];
        $entry_data_2 = json_decode($this->entries($fixedprojects, 'percentage'), true);
        $prev_entries2 = $entry_data_2['final_data'];
        $prev_entries3 = $this->entries($completed);
        $mPDF1 = Yii::app()->ePdf->mPDF();
        $mPDF1 = Yii::app()->ePdf->mPDF('', 'A4');

        $mPDF1->WriteHTML($stylesheet, 1);

        $mPDF1->WriteHTML($this->renderPartial('financialreportpdf', array(
            'model' => $model,
            'entries' => $prev_entries,
            'entries2' => $prev_entries2,
            'entries3' => $prev_entries3,
            'receipt_total' => $entry_data['recipt_total'],
            'deficit_total' => $entry_data['deficit_total'],
            'surplus_total' => $entry_data['surplus_total'],
            'receipt_total_perc' => $entry_data_2['recipt_total'],
            'deficit_total_perc' => $entry_data_2['deficit_total'],
            'surplus_total_perc' => $entry_data_2['surplus_total'],

        ), true));

        $mPDF1->Output('Financial Report' .  '.pdf', 'D');
    }
    public function actionSavetocompletedpdf()
    {

        $this->logo = $this->realpath_logo;
        $model = new Projects('financialreport1');

        $model->unsetAttributes();

        $financialdata1 = $model->financialreportcompleted1();

        $fixedprojects = $model->fixedprojectscompleted();

        $completed = $model->completed();

        $prev_entries = $this->entries1($financialdata1, 'rate');

        $prev_entries2 = $this->entries1($fixedprojects, 'percentage');

        $prev_entries3 = $this->entries1($completed);

        $mPDF1 = Yii::app()->ePdf->mPDF();
        $mPDF1 = Yii::app()->ePdf->mPDF('', 'A4');
        $sql = 'SELECT template_name '
                . 'FROM jp_quotation_template '
                . 'WHERE status="1"';
        $selectedtemplate =Yii::app()->db->createCommand($sql)->queryScalar();
        
        $template = $this->getTemplate($selectedtemplate);
        $mPDF1->SetHTMLHeader($template['header']);
        $mPDF1->SetHTMLFooter($template['footer']);
        $mPDF1->AddPage('','', '', '', '', 0,0,40, 30, 10,0);



        $mPDF1->WriteHTML($this->renderPartial('completedprojectsfinancialreportpdf', array(
            'model' => $model,
            'entries' => $prev_entries,
            'entries2' => $prev_entries2,
            'entries3' => $prev_entries3,
        ), true));

        $mPDF1->Output('Financial Report' .  '.pdf', 'D');
    }

    public function ExportCSVpdf()
    {

        $model = new Projects('financialreport1');

        $model->unsetAttributes();
        $financialdata1 = $model->financialreport1();
        $fixedprojects = $model->fixedprojects();
        $completed = $model->completed();
        $prev_entries = $this->entries($financialdata1, 'rate');

        $prev_entries2 = $this->entries($fixedprojects, 'percentage');

        $prev_entries3 = $this->entries($completed);

        $mPDF1 = Yii::app()->ePdf->mPDF();


        $mPDF1 = Yii::app()->ePdf->mPDF('', 'A4');

        $mPDF1->WriteHTML($stylesheet, 1);


        $mPDF1->WriteHTML($this->renderPartial('financialreportpdf', array(
            'model' => $model,
            'entries' => $prev_entries,
            'entries2' => $prev_entries2,
            'entries3' => $prev_entries3,
        ), true));

        $mPDF1->Output('Financial Report' .  '.pdf', 'D');
    }

    public function actionExpenselist()
    {
        $name = "";
        $clientname = "";
        $site = "";
        $wc = "";
        $sqft = "";
        $wc_type = "";
        $sqft_rate = "";
        $percentage = "";
        $tblpx = Yii::app()->db->tablePrefix;
        $consumptionreq_details="";
        $date_from = '';
        $unApprovedSQCount='';
        $date_to = '';
        $project_status = '';
        $scquot_date_date='';
        $dispatch_details=array();
        $consumptionreq_details=array();
        $unconsumptionreq_details=array();
        $pending_sc_bills =array();
        $sc_quotations =array();
        $pendingbills=array();
        $client_gst_total=0;
        $clientgstresult='';
        $company='';
        if (isset($_REQUEST['project_id']) && !empty($_REQUEST['project_id'])) {
            
            $where = '';
            $where1 = '';
            $where2 = '';
            $newQuery1 = "";
            $newQuery2 = "";
            $newQuery3 = "";
            $user = Users::model()->findByPk(Yii::app()->user->id);
            $arrVal = explode(',', $user->company_id);
             $project_id = $_REQUEST['project_id'];
            //dispatch details
            $warehouse_ids = Warehouse::model()->findAll(array('condition' => 'project_id = ' . $_REQUEST['project_id']));
            $project_det=Projects::Model()->findByPk($project_id);
            if(!empty($project_det)){
                $pro_company_str=$project_det->company_id;
                if(!empty($pro_company_str)){
                    $company_arr=explode(',',$pro_company_str);
                    $company = $company_arr['0'];
                }
            }
           

            $ids_array=array();
            if (!empty($warehouse_ids)) {
                foreach ($warehouse_ids as $warehouse) {
                    array_push($ids_array, $warehouse['warehouse_id']);
                }
                $warehouse_ids = implode(',', $ids_array);
            }
            
            $dispatchcriteria = new CDbCriteria();
            $consumptionReqCriteria= new CDbCriteria();
            $unconsumptionReqCriteria= new CDbCriteria();
            if (!empty($warehouse_ids)) {
                $dispatchcriteria->condition = "warehousedespatch_warehouseid_to IN (" . $warehouse_ids . ")";               
                $consumptionReqCriteria->condition="warehouse_id IN (" . $warehouse_ids . ")"; 
                $unconsumptionReqCriteria->condition="warehouse_id IN (" . $warehouse_ids . ")";   
            }

            if (!empty($_REQUEST['company_id'])) {
                $company_id = $_REQUEST['company_id'];
                $newQuery1 .= " FIND_IN_SET('" . $company_id . "', {$tblpx}expenses.company_id)";
                $newQuery2 .= " FIND_IN_SET('" . $company_id . "', company_id)";
                $newQuery3 .= " FIND_IN_SET('" . $company_id . "', {$tblpx}bills.company_id)";
            } else {
                $company_id = "";
                foreach ($arrVal as $arr) {
                    if ($newQuery1) $newQuery1 .= ' OR';
                    if ($newQuery2) $newQuery2 .= ' OR';
                    if ($newQuery3) $newQuery3 .= ' OR';
                    $newQuery1 .= " FIND_IN_SET('" . $arr . "', {$tblpx}expenses.company_id)";
                    $newQuery2 .= " FIND_IN_SET('" . $arr . "', company_id)";
                    $newQuery3 .= " FIND_IN_SET('" . $arr . "', {$tblpx}bills.company_id)";
                }
            }

            $where3='';
            if (!empty($_REQUEST['date_from']) && !empty($_REQUEST['date_to'])) {
                $date_from = date('Y-m-d', strtotime($_REQUEST['date_from']));
                $date_to = date('Y-m-d', strtotime($_REQUEST['date_to']));
                $where .= " AND {$tblpx}expenses.date between '" . $date_from . "' and '" . $date_to . "'";
                $where1 .= " AND {$tblpx}bills.bill_date between '" . $date_from . "' and '" . $date_to . "'";
                $where2 .= " AND {$tblpx}invoice.date between '" . $date_from . "' and '" . $date_to . "'";
                $dispatchcriteria->addCondition('warehousedespatch_date >= "' . $date_from . '" AND warehousedespatch_date <= "' . $date_to . '"');
                $consumptionReqCriteria->addCondition('date >= "' . $date_from . '" AND date <= "' . $date_to . '"');
                $unconsumptionReqCriteria->addCondition('date >= "' . $date_from . '" AND date <= "' . $date_to . '"');
                $where3 .= " AND {$tblpx}scquotation.scquotation_date between '" . $date_from . "' and '" . $date_to . "'";
            } else {
                if (!empty($_REQUEST['date_from'])) {
                    $date_from = date('Y-m-d', strtotime($_REQUEST['date_from']));
                    $where .= " AND {$tblpx}expenses.date >= '" . $date_from . "'";
                    $where1 .= " AND {$tblpx}bills.bill_date >= '" . $date_from . "'";
                    $where2 .= " AND {$tblpx}invoice.date >= '" . $date_from . "'";
                    $dispatchcriteria->addCondition('warehousedespatch_date >= "' . $date_from . '"');
                    $consumptionReqCriteria->addCondition('date >= "' . $date_from . '"');
                    $unconsumptionReqCriteria->addCondition('date >= "' . $date_from . '"');
                    $where3 .= " AND {$tblpx}scquotation.scquotation_date >= '" . $date_from . "'";
                }
                if (!empty($_REQUEST['date_to'])) {
                    $date_to = date('Y-m-d', strtotime($_REQUEST['date_to']));
                    $where .= " AND {$tblpx}expenses.date <= '" . $date_to . "'";
                    $where1 .= " AND {$tblpx}bills.bill_date <= '" . $date_to . "'";
                    $where2 .= " AND {$tblpx}invoice.date <= '" . $date_to . "'";
                    $dispatchcriteria->addCondition('warehousedespatch_date <= "' . $date_to . '"');
                    $consumptionReqCriteria->addCondition('date <= "' . $date_to . '"');
                    $unconsumptionReqCriteria->addCondition('date <= "' . $date_to . '"');
                    $where3 .= " AND {$tblpx}scquotation.scquotation_date <= '" . $date_to . "'";
                }
            }
             $project_id = $_REQUEST['project_id'];
            $dispatch_details = Warehousedespatch::model()->findAll($dispatchcriteria);
            $consumptionReqCriteria->addCondition('status =1');
            $consumptionReqCriteria->addCondition('coms_project_id = '.$project_id);

            

            $unconsumptionReqCriteria->addCondition('status =0');
           
            $unconsumptionReqCriteria->addCondition('coms_project_id = '.$project_id);

           

            
            $consumptionreq_details = ConsumptionRequest::model()->findAll($consumptionReqCriteria);


            $unconsumptionreq_details = ConsumptionRequest::model()->findAll($unconsumptionReqCriteria);
            
           
            $model = Projects::model()->findByPk($project_id);
            $projectsql =  "SELECT *,caption FROM {$tblpx}projects "
                . " inner join {$tblpx}status "
                . " on {$tblpx}projects.project_type ={$tblpx}status.sid  "
                . " WHERE pid = {$project_id}";
            $project = Yii::app()->db->createCommand($projectsql)->queryRow();
            $sc_quotations_sql = "SELECT * FROM `jp_scquotation` WHERE project_id=$project_id AND approve_status='Yes' $where3 ORDER BY scquotation_id DESC;";
           
            $sc_quotations=Yii::app()->db->createCommand($sc_quotations_sql)->queryAll();
            $unapproved_scquot_sql ="SELECT count(*) as count FROM `jp_scquotation` WHERE project_id=$project_id AND approve_status='No' $where3 ORDER BY scquotation_id DESC;";
            //die($unapproved_scquot_sql);
             $unapproved_sc_quotations_Count=Yii::app()->db->createCommand($unapproved_scquot_sql)->queryRow();
             $unApprovedSQCount='';
             if(!empty( $unapproved_sc_quotations_Count)){
                $unApprovedSQCount=$unapproved_sc_quotations_Count["count"];
             }
             
            $creditsql=array();
            $name = $project['name'];
            $site = $project['site'];
            $clientid = $project['client_id'];
            $wc = $project['caption'];
            $wc_type = $project['project_type'];
            $sqft = $project['sqft'];
            $sqft_rate = $project['sqft_rate'];
            $percentage = $project['percentage'];
            $clientdetailssql = "SELECT * FROM {$tblpx}clients WHERE cid = {$clientid}";
            $clientdetails = Yii::app()->db->createCommand($clientdetailssql)->queryRow();
            $clientname = $clientdetails['name'];

            $unreconciled_datasql = "SELECT O.*, E.*  FROM jp_reconciliation O "
                . " JOIN jp_expenses E ON O.reconciliation_parentid = E.exp_id  AND (reconciliation_table ='jp_expenses' ) "
                . " WHERE O.reconciliation_status=0 and E.projectid='" . $project_id . "'";
            $unreconciled_data = Yii::app()->db->createCommand($unreconciled_datasql)->queryAll();
            // echo "<pre>";print_r($unreconciled_data);exit;
            $exptypesql = "select exptype from " . $tblpx . "expenses"
                . " WHERE 	projectid =" . $project_id . " AND " . $tblpx . "expenses.type = 73  "
                . " "  . $where . " AND exptype IS NOT NULL";
            $exptype = Yii::app()->db->createCommand($exptypesql)->queryAll();

            $data = array();

            foreach ($exptype as $exp) {

                array_push($data, $exp['exptype']);
            }

            $ex = implode(',', $data);

            if (!empty($exptype)) {
                
                $sqldata = "select DISTINCT " . $tblpx . "project_exptype.type_id,"
                    . " project_id,name,type_name from " . $tblpx . "project_exptype"
                    . " inner join " . $tblpx . "projects "
                    . " on " . $tblpx . "project_exptype.project_id = " . $tblpx . "projects.pid"
                    . " inner join " . $tblpx . "expense_type "
                    . " on " . $tblpx . "expense_type.type_id = " . $tblpx . "project_exptype.type_id"
                    . " inner join " . $tblpx . "expenses "
                    . " on " . $tblpx . "expenses.projectid = " . $tblpx . "projects.pid "
                    . " WHERE  project_id=" . $project_id . " " . $where . " AND (" . $newQuery1 . ") "
                    . " ORDER BY " . $tblpx . "expense_type.type_name";
                $sql = Yii::app()->db->createCommand($sqldata)->queryAll();

                $new_exp_sql=  "select DISTINCT " . $tblpx . "project_exptype.type_id,"
                    . " project_id,name,type_name from " . $tblpx . "project_exptype"
                    . " inner join " . $tblpx . "projects "
                    . " on " . $tblpx . "project_exptype.project_id = " . $tblpx . "projects.pid"
                    . " inner join " . $tblpx . "expense_type "
                    . " on " . $tblpx . "expense_type.type_id = " . $tblpx . "project_exptype.type_id"
                    . " inner join " . $tblpx . "expenses "
                    . " on " . $tblpx . "expenses.projectid = " . $tblpx . "projects.pid "
                    . " WHERE  project_id=" . $project_id . " " . $where . " AND (" . $newQuery1 . ") "
                    . " ORDER BY " . $tblpx . "expense_type.type_name";
                $creditsql = Yii::app()->db->createCommand($new_exp_sql)->queryAll();
   
            } else {

                $sql = array();
                $creditsql=array();
            }
            
            $bill_array = array();
            $unrecon_arr=array();
            foreach ($unreconciled_data as  $data) {                
                $recon_bill = "'{$data["bill_id"]}'";                
                array_push($bill_array, $recon_bill);
                array_push($unrecon_arr, $recon_bill);
            }
           
            $bill_array1 = implode(',', $bill_array);

            $pendingsql = 'SELECT bill_id FROM jp_expenses '
            . ' WHERE projectid ='.$project_id . ' AND  '
            . ' (reconciliation_status =1 AND bank_id IS NOT NULL) OR  (reconciliation_status IS NULL AND bank_id IS  NULL)';

            $pending_bills_array = Yii::app()->db->createCommand($pendingsql)->queryAll();
            
            $pending_bills =array();
            foreach ($pending_bills_array as  $data) {                
                $billid = "'{$data["bill_id"]}'";                
                array_push($pending_bills, $billid);
            }
            $pending_bills1 = implode(',', $pending_bills);
                        
            $clientsql = "select * from " . $tblpx . "expenses"
                . " WHERE amount !=0 AND projectid=" . $project_id . " "
                . " AND type = '72' " . $where . " AND return_id IS NULL "
                . " AND (" . $newQuery2 . ") "
                . " AND (reconciliation_status IS NULL OR reconciliation_status=1) "
                . " ORDER BY date";
            $client = Yii::app()->db->createCommand($clientsql)->queryAll();
            $clientgstsql = "select COALESCE(SUM(expense_sgst + expense_cgst + expense_igst), 0) as gst from " . $tblpx . "expenses"
                . " WHERE amount !=0 AND projectid=" . $project_id . " "
                . " AND type = '72' " . $where . " AND return_id IS NULL "
                . " AND (" . $newQuery2 . ") "
                . " AND (reconciliation_status IS NULL OR reconciliation_status=1) "
                . " ORDER BY date";
            $clientgstresult = Yii::app()->db->createCommand($clientgstsql)->queryRow();

            $client = Yii::app()->db->createCommand($clientsql)->queryAll();

            $client1sql = "select * from " . $tblpx . "expenses"
                . " WHERE amount !=0 AND projectid=" . $project_id . " AND "
                . " type = '72' " . $where . " AND return_id IS NULL "
                . " AND (" . $newQuery2 . ")  AND reconciliation_status= 0 "
                . " ORDER BY date";
            $client1 = Yii::app()->db->createCommand($client1sql)->queryAll();


            $expreceipt_returnsql = "select * from " . $tblpx . "expenses"
                . " WHERE amount !=0 AND projectid=" . $project_id . " AND"
                . "  type = '72' " . $where . " AND return_id IS NOT NULL "
                . " AND (" . $newQuery2 . ") ORDER BY date";
            $expreceipt_return = Yii::app()->db->createCommand($expreceipt_returnsql)->queryAll();
            if ($bill_array1) {
                $bill_condition = " 1=1";
                $bill_condition1 = " 1=1";
                if(!empty($pending_bills1)){
                    $bill_condition = " bill_id IN ($pending_bills1)";
                    $bill_condition1 = "bill_id NOT IN($pending_bills1)";
                }

                $purchasesql = "SELECT * FROM  " . $tblpx . "bills "
                    . " LEFT JOIN " . $tblpx . "purchase "
                    . " ON " . $tblpx . "bills.purchase_id=" . $tblpx . "purchase.p_id "
                    . " WHERE " . $tblpx . "purchase.project_id=" . $project_id . " "
                    . " AND " . $tblpx . "bills.purchase_id IS NOT NULL " . $where1 . " "
                    . " AND (" . $newQuery3 . ") AND bill_id NOT IN ($bill_array1) "
                    . " AND ".$bill_condition;                    
                $purchase = Yii::app()->db->createCommand($purchasesql)->queryAll(); //reconcil
                $pendingpurchasesql = "SELECT * FROM  " . $tblpx . "bills "
                    . " LEFT JOIN " . $tblpx . "purchase "
                    . " ON " . $tblpx . "bills.purchase_id=" . $tblpx . "purchase.p_id "
                    . " WHERE " . $tblpx . "purchase.project_id=" . $project_id . " "
                    . " AND " . $tblpx . "bills.purchase_id IS NOT NULL " . $where1 . " "
                    . " AND (" . $newQuery3 . ") "
                    . " AND ".$bill_condition1;  ;
                                 
                $pendingbills = Yii::app()->db->createCommand($pendingpurchasesql)->queryAll();
                

                $purchase1sql = "SELECT * FROM  " . $tblpx . "bills "
                    . " LEFT JOIN " . $tblpx . "purchase "
                    . " ON " . $tblpx . "bills.purchase_id=" . $tblpx . "purchase.p_id "
                    . " WHERE " . $tblpx . "purchase.project_id=" . $project_id . " "
                    . " AND " . $tblpx . "bills.purchase_id IS NOT NULL " . $where1 . " "
                    . " AND (" . $newQuery3 . ")  AND ".$bill_condition1;                    
                $purchase1 = Yii::app()->db->createCommand($purchase1sql)->queryAll(); //un reconcil
            } else {

                $bill_condition = " 1=1";
                $bill_condition1 = " 1=1";
                if(!empty($pending_bills1)){
                    $bill_condition = " bill_id IN($pending_bills1)";
                    $bill_condition1 = "bill_id NOT IN($pending_bills1)";
                }

                $purchasesql = "SELECT * FROM  " . $tblpx . "bills "
                    . " LEFT JOIN " . $tblpx . "purchase "
                    . " ON " . $tblpx . "bills.purchase_id=" . $tblpx . "purchase.p_id "
                    . " WHERE " . $tblpx . "purchase.project_id=" . $project_id . " "
                    . " AND " . $tblpx . "bills.purchase_id IS NOT NULL " . $where1 . " "
                    . " AND (" . $newQuery3 . ")  and ".$bill_condition;
                $purchase = Yii::app()->db->createCommand($purchasesql)->queryAll();

                $purchase1sql = "SELECT * FROM  " . $tblpx . "bills "
                    . " LEFT JOIN " . $tblpx . "purchase "
                    . " ON " . $tblpx . "bills.purchase_id=" . $tblpx . "purchase.p_id "
                    . " WHERE " . $tblpx . "purchase.project_id=" . $project_id . " "
                    . " AND " . $tblpx . "bills.purchase_id IS NOT NULL " . $where1 . " "
                    . " AND (" . $newQuery3 . ")  and ".$bill_condition1;
                $purchase1 = Yii::app()->db->createCommand($purchase1sql)->queryAll(); //un reconcil
            }

            $daybook_expensesql = "select * from " . $tblpx . "expenses"
                . "  WHERE amount !=0 AND projectid=" . $project_id . " "
                . " AND type = '73' " . $where . " AND exptype IS NULL "
                . " AND vendor_id IS NULL AND bill_id IS NULL AND (" . $newQuery2 . ") "
                . " AND (reconciliation_status IS NULL OR reconciliation_status=1)  ORDER BY date";            
            $daybook_expense = Yii::app()->db->createCommand($daybook_expensesql)->queryAll();

            $daybook_expense1sql = "select * from " . $tblpx . "expenses"
                    . " WHERE amount !=0 AND projectid=" . $project_id . " "
                    . " AND type = '73' " . $where . " AND exptype IS NULL "
                    . " AND vendor_id IS NULL AND bill_id IS NULL AND (" . $newQuery2 . ") "
                    . " AND reconciliation_status= 0  ORDER BY date";
            $daybook_expense1 = Yii::app()->db->createCommand($daybook_expense1sql)->queryAll();


            $invoicesql = "SELECT * FROM {$tblpx}invoice "
                . " WHERE project_id = " . $project_id . " "
                . " AND invoice_status ='saved' AND (" . $newQuery2 . ") " . $where2 . "";
            $invoice = Yii::app()->db->createCommand($invoicesql)->queryAll();

            $uinvoicesql = "SELECT * FROM {$tblpx}invoice "
                . " WHERE project_id = " . $project_id . " "
                . " AND invoice_status ='draft' AND (" . $newQuery2 . ") " . $where2 . "";  
             //   die($uinvoicesql)  
                 
            $uinvoice = Yii::app()->db->createCommand($uinvoicesql)->queryAll();
            // echo "<pre>";print_r($uinvoice);exit;  ; 
        } else {
            $model = new Projects;
            $model->unsetAttributes();
            if (isset($_REQUEST['company_id']) && !empty($_REQUEST['company_id'])) {
                $model->companyId = $_REQUEST['company_id'];
            }
            if (isset($_REQUEST['project_status']) && !empty($_REQUEST['project_status'])) {
                $model->project_status  = $_REQUEST['project_status'];
                $project_status         = $_REQUEST['project_status'];
            }
            if (isset($_REQUEST['date_from']) && !empty($_REQUEST['date_from'])) {
                $model->fromdate = $_REQUEST['date_from'];
                $date_from = $_REQUEST['date_from'];
            }
            if (isset($_REQUEST['date_to']) && !empty($_REQUEST['date_to'])) {
                $model->todate = $_REQUEST['date_to'];
                $date_to = $_REQUEST['date_to'];
            }
            $sql = array();

            $client = array();
            $client1 = array();
            $purchase = array();
            $purchase1 = array();
            $daybook_expense = array();
            $daybook_expense1 = array();
            $expreceipt_return = array();
            $invoice = array();
            $uinvoice = array();
            $project_id = 0;
            $company_id = $model->companyId;
            $unreconciled_data = array();
            $consumptionreq_details=array();
            $unconsumptionreq_details=array();
            $sc_quotations=array();
            $creditsql=array();
            $pendingbills=array();
        }

       /* dispatch details getting function */
       $ids_array = array();
       $warehouse_ids = Warehouse::model()->findAll(array('condition' => 'project_id = ' . $project_id));
       
       $dispatch_datas = '';
       $receipt_datas = '';
       $item_report_data = '';
       if (!empty($warehouse_ids)) {
           foreach ($warehouse_ids as $warehouse) {
               array_push($ids_array, $warehouse['warehouse_id']);
           }
           $warehouse_ids = implode(',', $ids_array);
           $item_report_data = $this->itemStock($warehouse_ids, $date_from, $date_to);          
       }
        $render_datas = array(
            'sql' => $sql,
            'client' => $client,
            'client1' => $client1,
            'project_id' => $project_id,
            'site' => $site,
            'name' => $name,
            'clientname' => $clientname,
            'wc' => $wc,
            'sqft' => $sqft,
            'wc_type' => $wc_type,
            'sqft_rate' => $sqft_rate,
            'percentage' => $percentage,
            'date_from' => $date_from,
            'date_to' => $date_to,
            'purchase' => $purchase,
            'purchase1' => $purchase1,
            'daybook_expense' => $daybook_expense,
            'expreceipt_return' => $expreceipt_return,
            'company_id' => $company_id,
            'model' => $model,
            'project_status' => $project_status,
            'invoice_dtls' => $invoice,
            'uinvoice_dtls'=>$uinvoice,
            'unreconciled_data' => $unreconciled_data,
            'daybook_expense1' => $daybook_expense1,
            'item_report_data' => $item_report_data,
            'dispatch_details'=>$dispatch_details,
            'consumptionreq_details'=>$consumptionreq_details,
            'unconsumptionreq_details'=>$unconsumptionreq_details,
            'sc_quotations'=>$sc_quotations,
            'unApprovedSQCount'=>$unApprovedSQCount,
            'creditsql'=>$creditsql,
            'pendingbills'=>$pendingbills,
            'clientgstresult'=>$clientgstresult,
            'company'=>$company
        );

        if (filter_input(INPUT_GET, 'export') == 'pdf') {
            $mPDF1 = Yii::app()->ePdf->mPDF();
            $mPDF1 = Yii::app()->ePdf->mPDF('', 'A4');
            $sql = 'SELECT template_name '
                . 'FROM jp_quotation_template '
                . 'WHERE status="1"';
            $selectedtemplate =Yii::app()->db->createCommand($sql)->queryScalar();
            
            $template = $this->getTemplate($selectedtemplate);
            $mPDF1->SetHTMLHeader($template['header'],'O',true);
            $mPDF1->SetHTMLFooter($template['footer']);
            $mPDF1->AddPage('','', '', '', '', 0,0,50, 40, 10,0);            
            $mPDF1->WriteHTML($this->renderPartial('expenselist', $render_datas, true));
           
            $mPDF1->Output('Project Report.pdf', 'D');
        } elseif (filter_input(INPUT_GET, 'export') == 'csv') {
            $exported_data = $this->renderPartial('expenselist', $render_datas, true);
            $export_filename = 'ProjectReport' . date('Ymd_His');
            //echo "<pre>";print_r($exported_data);exit;
            ExportHelper::exportGridAsCSV($exported_data, $export_filename);
        } else { 
            if(isset($_REQUEST['aging']) && $_REQUEST['aging']==1){
                $this->render('projectagingreport');
            } else if(isset($_REQUEST['aging']) && $_REQUEST['aging']==2){
                $this->render('projectagingreportbydate');
            } else{          
                $this->render('expenselist', $render_datas);            
            }
        }
    }

    public function actionSaveexpenselist($project_id, $from_date, $to_date, $company_id)
    {

        $tblpx = Yii::app()->db->tablePrefix;
        $fromdate = date('Y-m-d', strtotime($from_date));
        $todate = date('Y-m-d', strtotime($to_date));
        $expense_entries = $this->expense_entries($project_id, $fromdate, $todate, $company_id);
        $project = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}projects WHERE pid = {$project_id}")->queryRow();
        $name = $project['name'];

        $mPDF1 = Yii::app()->ePdf->mPDF();

        $mPDF1 = Yii::app()->ePdf->mPDF('', 'A4');

        $mPDF1->WriteHTML($stylesheet, 1);

        $mPDF1->WriteHTML($this->renderPartial('expenselistpreview', array(
            'expense_entries' => $expense_entries, 'name' => $name, 'company_id' => $company_id
        ), true));

        $mPDF1->Output('Project_Report_' . $name . '.pdf', 'D');
    }


    public function actionProjectreport()
    {
        $model      = new Dailyreport('search');
        $tblpx      = Yii::app()->db->tablePrefix;
        $final_array = array();
        $head_array = array();
        $new_query = "";
        $datefrom         = isset($_GET['date_from']) ? $_GET['date_from'] : "";
        $dateto           = isset($_GET['date_to']) ? $_GET['date_to'] : "";
        $subcontractor_id = "";
        $expensehead_id = "";
        if (isset($_GET['expensehead_id']) && isset($_GET['expensehead_id'])) {
            if ($_GET['expensehead_id'] != '' && $_GET['expensehead_id'] != '') {
                $expensehead_id   = $_GET['expensehead_id'];
                $subcontractor_id = $_GET['subcontractor_id'];
                if (!empty($datefrom) && empty($dateto)) {
                    $new_query .= " AND e.date >= '" . date('Y-m-d', strtotime($datefrom)) . "'";
                } elseif (!empty($dateto) && empty($datefrom)) {
                    $new_query .= " AND e.date <= '" . date('Y-m-d', strtotime($dateto)) . "'";
                } elseif (!empty($dateto) && !empty($datefrom)) {
                    $new_query .= " AND e.date between '" . date('Y-m-d', strtotime($datefrom)) . "' and  '" . date('Y-m-d', strtotime($dateto)) . "'";
                }
                $sql = "SELECT e.*, p.name as pname,s.subcontractor_name as subname,exp.type_name FROM " . $tblpx . "dailyreport e
                LEFT JOIN " . $tblpx . "projects p ON e.projectid = p.pid
                LEFT JOIN " . $tblpx . "subcontractor s ON e.subcontractor_id = s.subcontractor_id LEFT JOIN " . $tblpx . "expense_type exp on
                exp.type_id= e.expensehead_id
                WHERE 1=1 AND e.expensehead_id = " . $_GET['expensehead_id'] . " AND e.subcontractor_id =" . $_GET['subcontractor_id'] . " " . $new_query . " AND e.approve_status =1 group by e.projectid";
                $result = Yii::app()->db->createCommand($sql)->queryAll();
                foreach ($result as $key => $data) {
                    $project = Projects::model()->findByPk($data['projectid']);
                    $final_array[$key]['project'] = $project['name'];
                    $payment_deatils = $model->getdailyreportDetails($_GET['subcontractor_id'], $_GET['expensehead_id'], $data['projectid'], $datefrom, $dateto);
                    $total_amount = $row_count = 0;
                    foreach ($payment_deatils as $key2 => $values) {

                        if (!empty($values['labour']) && !empty($values['helper'])) {
                            $row_count += 2;
                        } else {
                            $row_count += 1;
                        }
                        $total_amount += $values['amount'];
                    }
                    $final_array[$key]['total_amount'] = $total_amount;
                    $final_array[$key]['row_count'] = $row_count;
                    $final_array[$key]['expense_head'] = $_GET['expensehead_id'];
                    $final_array[$key]['expense_head_title'] = $values['type_name'];

                    foreach ($payment_deatils as $key2 => $values) {
                        $final_array[$key]['payment'][$key2]['date'] = $values['date'];
                        $final_array[$key]['payment'][$key2]['amount'] = $values['amount'];
                        $model2 = ExpenseType::model()->findByPk($_GET['expensehead_id']);
                        if ($model2->labour_label != '' && $model2->labour_status == 1) {
                            $final_array[$key]['payment'][$key2]['items'][] = $values['labour'];
                        }
                        if ($model2->wage_label != '' && $model2->wage_status == 1) {
                            $final_array[$key]['payment'][$key2]['items'][] = $values['wage'];
                        }
                        if ($model2->wage_rate_label != '' && $model2->wagerate_status == 1) {
                            $final_array[$key]['payment'][$key2]['items'][] = $values['wage_rate'];
                        }
                        if ($model2->helper_label != '' && $model2->helper_status == 1) {
                            $final_array[$key]['payment'][$key2]['items'][] = $values['helper'];
                        }
                        if ($model2->helper_labour_label != '' && $model2->helperlabour_status == 1) {
                            $final_array[$key]['payment'][$key2]['items'][] = $values['helper_labour'];
                        }
                        if ($model2->lump_sum_label != '' && $model2->lump_sum_status == 1) {
                            $final_array[$key]['payment'][$key2]['items'][] = $values['lump_sum'];
                        }
                    }
                }
                // exit;
            }
        }

        if (!empty($final_array)) {
            foreach ($final_array as $key => $value) {
                $model = ExpenseType::model()->findByPk($value['expense_head']);
                $html = "";
                if ($key == 0) {
                    if ($model->labour_label != '' && $model->labour_status == 1) {
                        $head_array[] = $model->labour_label;
                    }
                    if ($model->wage_label != '' && $model->wage_status == 1) {
                        $head_array[] = $model->wage_label;
                    }
                    if ($model->wage_rate_label != '' && $model->wagerate_status == 1) {
                        $head_array[] = $model->wage_rate_label;
                    }
                    if ($model->helper_label != '' && $model->helper_status == 1) {
                        $head_array[] = $model->helper_label;
                    }
                    if ($model->helper_labour_label != '' && $model->helperlabour_status == 1) {
                        $head_array[] =  $model->helper_labour_label;
                    }
                    if ($model->lump_sum_label != '' && $model->lump_sum_status == 1) {
                        $head_array[] = $model->lump_sum_label;
                    }
                }
            }
        }
        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        $newQuery = "";
        foreach ($arrVal as $arr) {
            if ($newQuery) $newQuery .= ' OR';
            $newQuery .= " FIND_IN_SET('" . $arr . "', company_id)";
        }
        $subcontractor  = Yii::app()->db->createCommand("SELECT DISTINCT subcontractor_id,subcontractor_name FROM {$tblpx}subcontractor WHERE (" . $newQuery . ") ORDER BY subcontractor_id")->queryAll();
        $expense  = Yii::app()->db->createCommand("SELECT DISTINCT type_id,type_name FROM {$tblpx}expense_type WHERE (" . $newQuery . ") ORDER BY type_name")->queryAll();
        $render_datas = array(
            'model' => $model,
            'dataProvider'     => $model->search(),
            'subcontractor' => $subcontractor,
            'expense'       => $expense,
            'datefrom'      => $datefrom,
            'dateto'        => $dateto,
            'final_array'   => $final_array,
            'head_array'    => $head_array,
            'expensehead_id' => $expensehead_id,
            'subcontractor_id' => $subcontractor_id,
        );

        if (filter_input(INPUT_GET, 'export') == 'pdf') {
            $this->logo = $this->realpath_logo;
            $mPDF1 = Yii::app()->ePdf->mPDF();
            $mPDF1 = Yii::app()->ePdf->mPDF('', 'A4');
            $sql = 'SELECT template_name '
            . 'FROM jp_quotation_template '
            . 'WHERE status="1"';
            $selectedtemplate =Yii::app()->db->createCommand($sql)->queryScalar();
    
            $template = $this->getTemplate($selectedtemplate);
            $mPDF1->SetHTMLHeader($template['header']);            
            $mPDF1->SetHTMLFooter($template['footer']);
            $mPDF1->AddPage('','', '', '', '', 0,0,40, 30, 10,0);
            $mPDF1->WriteHTML($this->renderPartial('projectreport', $render_datas, true));
            $mPDF1->Output('WORK-STATUS-REPORT.pdf', 'D');
        } elseif (filter_input(INPUT_GET, 'export') == 'csv') {
            $exported_data = $this->renderPartial('projectreport', $render_datas, true);
            $export_filename = 'WORK-STATUS-REPORT' . date('Ymd_His');
            ExportHelper::exportGridAsCSV($exported_data, $export_filename);
        } else {
            $this->render('projectreport', $render_datas);
        }
    }

    public function actionSaveprojectreport()
    {

        $tblpx = Yii::app()->db->tablePrefix;

        $project_id = $_POST['project_id'];

        $fromdate = $_POST['fromdate'];

        $todate = $_POST['todate'];

        $pastweek = $_POST['pastweek'];

        $nextweek = $_POST['nextweek'];

        $issues = $_POST['issues'];

        $company_id = $_POST['company_id'];

        $query = '';
        if (isset($_POST['company_id']) && !empty($_POST['company_id'])) {
            $query .= '' . $tblpx . 'dailyreport.company_id = ' . $company_id . '';
        } else {
            $user = Users::model()->findByPk(Yii::app()->user->id);
            $arrVal = explode(',', $user->company_id);
            $query = "";
            foreach ($arrVal as $arr) {
                if ($query) $query .= ' OR';
                $query .= " FIND_IN_SET('" . $arr . "', " . $tblpx . "dailyreport.company_id)";
            }
        }

        $company_name = "";
        if (isset($company_id)) {
            $cname = Company::model()->findByPk($company_id);
            $company_name = $cname['name'];
        }


        $work_desc_count  = $_POST['work_desc_count'];
        $extra_work_desc_count  = $_POST['extra_work_desc_count'];
        $work_descriptions = $extra_work_descriptions = array();
        if ($work_desc_count > 0) {
            for ($i = 1; $i < $work_desc_count; $i++) {
                $work_descriptions[$i] = $_POST['work_description_' . $i];
            }
        }
        if ($extra_work_desc_count > 0) {
            for ($j = 1; $j < $extra_work_desc_count; $j++) {
                $extra_work_descriptions[$j] = $_POST['extra_work_description_' . $j];
            }
        }

        $date_from = date('Y-m-d', strtotime($fromdate));

        $date_to = date('Y-m-d', strtotime($todate));


        //$sql = Yii::app()->db->createCommand("select " . $tblpx . "clients.name as client_name , " . $tblpx . "projects.name, " . $tblpx . "projects.pid," . $tblpx . "projects.created_date," . $tblpx . "projects.site from " . $tblpx . "projects inner join " . $tblpx . "clients on " . $tblpx . "clients.cid = " . $tblpx . "projects.client_id inner join " . $tblpx . "expenses on " . $tblpx . "expenses.projectid = " . $tblpx . "projects.pid  inner join " . $tblpx . "dailyreport ON " . $tblpx . "dailyreport.projectid = " . $tblpx . "projects.pid WHERE pid=" . $project_id . " AND " . $tblpx . "dailyreport.book_date BETWEEN '" . $date_from . "' AND '" . $date_to . "' ")->queryRow();
        $sql = Yii::app()->db->createCommand("select " . $tblpx . "dailyreport.*," . $tblpx . "clients.name as client_name , " . $tblpx . "projects.name, " . $tblpx . "projects.pid," . $tblpx . "projects.created_date," . $tblpx . "projects.site from " . $tblpx . "projects inner join " . $tblpx . "clients on " . $tblpx . "clients.cid = " . $tblpx . "projects.client_id inner join " . $tblpx . "dailyreport ON " . $tblpx . "dailyreport.projectid = " . $tblpx . "projects.pid WHERE pid=" . $project_id . " AND " . $tblpx . "dailyreport.book_date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND (" . $query . ") ORDER BY book_date ASC")->queryAll();


        $project_entries = $this->project_entries($project_id, $fromdate, $todate, $sql, $work_descriptions, $extra_work_descriptions);

        $project_name = (isset($sql[0]['name']) ? $sql[0]['name'] : '');

        $duration = strftime(" %d/%m ", strtotime($date_from)) . " --" . strftime(" %d/%m/%y ", strtotime($date_to));


        $mPDF1 = Yii::app()->ePdf->mPDF();

        $mPDF1 = Yii::app()->ePdf->mPDF('', 'A4');

        $mPDF1->WriteHTML($stylesheet, 1);

        $mPDF1->WriteHTML($this->renderPartial('projectreportpreview', array(
            'project_entries' => $project_entries, 'sql' => $sql, 'fromdate' => $date_from, 'todate' => $date_to,
            'pastweek' => $pastweek, 'nextweek' => $nextweek, 'issues' => $issues, 'duration' => $duration, 'company_name' => $company_name
        ), true));

        $mPDF1->Output($project_name . '-' . $duration . '.pdf', 'D');
    }

    private function project_entries($project_id, $date_from, $date_to, $sql, $work_descriptions = array(), $extra_work_descriptions = array())
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $finaldata = "";
        if (isset($sql[0]['pid'])) {
            $finaldata .= '<table class="projtable">
                    <tr>
                        <th colspan="4">WORKS DONE</th>
                    </tr>
                    <tr>
                        <th>Date/ Day</th>
                        <th>Work Type</th>
                        <th>Numbers</th>
                        <th>Description</th>
                    </tr>';
            $tblpx = Yii::app()->db->tablePrefix;
            $i = 1;
            $j = 1;
            foreach ($sql as $row) {
                $i = 1;
                $desc = $row['works_done'];
                if (!empty($row['wrktype_and_numbers'])) {
                    $obj = json_decode($row['wrktype_and_numbers']);
                    foreach ($obj as $value) {
                        if (isset($value->{'works_done'})) {
                            $desc =  $value->{'works_done'};
                        }
                        $list = Yii::app()->db->createCommand("SELECT DISTINCT work_type FROM {$tblpx}work_type  WHERE wtid = '" . $value->{'work_type'} . "'")->queryRow();
                        if ($value->{'work_done'} == 0) {

                            $finaldata .= '            <tr>

                <td>' . date('d-M-Y', strtotime($row['book_date'])) . ' <br/>' . date("l", strtotime($row['book_date'])) . '</td>'
                                . '<td>' . $list['work_type'] . '</td>

                <td>' . $value->{'numbers'} . '</td>
                <td>' . (isset($work_descriptions[$j]) ? $work_descriptions[$j] : $desc) . ' </td>


            </tr>';
                            $j++;
                        }
                    }
                    $i++;
                }
            }

            $finaldata .= ' <tr>

                        <th colspan="4">EXTRA WORKS DONE</th>
                    </tr>
                    <tr>
                        <th>Date/ Day</th>
                        <th>Work Type</th>
                        <th>Numbers</th>
                        <th>Description</th>
                    </tr>';
            $k = 1;
            foreach ($sql as $row) {
                $extradesc = $row['extra_work_done'];
                if (!empty($row['wrktype_and_numbers'])) {
                    $obj = json_decode($row['wrktype_and_numbers']);
                    foreach ($obj as $value) {
                        if (isset($value->{'works_done'})) {
                            $extradesc =  $value->{'works_done'};
                        }
                        $list = Yii::app()->db->createCommand("SELECT DISTINCT work_type FROM {$tblpx}work_type  WHERE wtid = '" . $value->{'work_type'} . "'")->queryRow();
                        if ($value->{'work_done'} == 1) {

                            $finaldata .= ' <tr>

                <td>' . date('d-M-Y', strtotime($row['book_date'])) . '<br/>' . date("l", strtotime($row['book_date'])) . '</td>

                <td>' . $list['work_type'] . '</td>

                <td>' . $value->{'numbers'} . '</td>
                <td>' . (isset($extra_work_descriptions[$k]) ? $extra_work_descriptions[$k] : $extradesc) . ' </td>


            </tr>';
                            $k++;
                        }
                    }
                }
            }
            $finaldata .= '</table>';
        }

        return $finaldata;
    }
    private function entries($datamodel, $type = '')
    {



        $tblpx = Yii::app()->db->tablePrefix;

        $finaldata = "";



        $reportdata = $datamodel->getData();


        $i = 0;

        $receipt_grandtotal = 0;

        $deficit_grandtotal = 0;

        $surplus_grandtotal = 0;

        $paid_total = 0;
        if (!empty($reportdata)) {

            foreach ($reportdata as $data) {

                $i++;

                $companyname = array();
                $user = Users::model()->findByPk(Yii::app()->user->id);
                $comVal = explode(',', $user->company_id);
                $arrVal = explode(',', $data['company_id']);
                foreach ($arrVal as $arr) {
                    if (in_array($arr, $comVal)) {
                        $value = Company::model()->findByPk($arr);
                        if($value){
                            array_push($companyname, $value->name);
                        }
                        
                    }
                }

                $projectid = $data['pid'];

                $inv_dtls = Yii::app()->db->createCommand("SELECT SUM(amount) as total_amount,SUM(tax_amount) as tax_amount FROM {$tblpx}invoice WHERE project_id = " . $projectid . " AND invoice_status ='saved'")->queryRow();
                if (($inv_dtls)) {
                    if (($inv_dtls['total_amount'] + $inv_dtls['tax_amount']) == 0) {
                        $invoice = 0;
                    } else {
                        $invoice = $inv_dtls['total_amount'] + $inv_dtls['tax_amount'];
                    }
                } else {
                    $invoice = 0;
                }

                $rec = '';
                if ($data['project_type'] == 87) {
                    $rec = "<td align='right'>" . $data['sqft_rate'] . "</td>";
                }
                if ($data['project_type'] == 86) {
                    $rec = "<td align='right'>" . (($data['percentage']) ? $data['percentage'] . '%' : '') . "</td>";
                }
                if ($data['project_status'] == 85) {
                    $rec = "<td align='right'>" . $data['sqft_rate'] . "</td><td align='right'>" . (($data['percentage']) ? $data['percentage'] . '%' : '') . "</td>";
                }


                $percentage = Yii::app()->db->createCommand("SELECT percentage FROM {$tblpx}projects WHERE pid = {$projectid}")->queryRow();
                $servise_charge = ($data['tot_expense'] * $percentage['percentage']) / 100;

                $receipt_grandtotal += $data['tot_receipt'];

                $paid_total += $data['tot_paid_to_vendor'];

                if ($type == 'rate') {

                    if ($invoice == 0) {

                        $diff = ($data['tot_expense'] + $servise_charge) - $data['tot_receipt'];
                    } else {

                        $diff = $invoice - $data['tot_receipt'];
                    }

                    $deficit = 0;

                    $surplus = 0;

                    if ($diff < 0) {

                        $surplus = $diff;
                    } else {

                        $deficit = $diff;
                    }

                    $deficit_grandtotal += $deficit;
                    $surplus_grandtotal += $surplus;
                    $finaldata .=

                        "<tr>
                                <td>" . $i . "</td>
                                <td>" . $data['name'] . "</td>
                                <td>" . implode(', ', $companyname) . "</td>
                                <td>" . $data['sqft'] . "</td>

                                " . $rec . "
                                <td align='right'>" . Controller::money_format_inr($data['tot_receipt'], 2) . "</td>
                                <td align='right'>" . Controller::money_format_inr($data['tot_expense'], 2) . "</td>
                                <td align='right'>" . Controller::money_format_inr($invoice, 2) . "</td>
                                <td align='right'>" . Controller::money_format_inr(abs($deficit), 2) . "</td>
                                <td align='right'>" . Controller::money_format_inr(abs($surplus), 2)  . "</td>
                                <td>" . $data['remarks'] . "</td>
                            </tr>";
                } else if ($type == 'percentage') {


                    if ($invoice == 0) {

                        $diff = ($data['tot_expense'] + $servise_charge) - $data['tot_receipt'];
                    } else {

                        $diff = $invoice - $data['tot_receipt'];
                    }

                    $deficit = 0;

                    $surplus = 0;

                    if ($diff < 0) {
                        $surplus = $diff;
                    } else {
                        $deficit = $diff;
                    }

                    $deficit_grandtotal += $deficit;
                    $surplus_grandtotal += $surplus;
                    $finaldata .=

                        "<tr>

                                <td>" . $i . "</td>
                                <td>" . $data['name'] . "</td>
                                <td>" . implode(', ', $companyname) . "</td>
                                <td>" . $data['sqft'] . "</td>
                                " . $rec . "
                                <td align='right'>" . Controller::money_format_inr($data['tot_receipt'], 2) . "</td>
                                <td align='right'>" . Controller::money_format_inr($data['tot_expense'], 2) . "</td>

                                <td align='right'>" . Controller::money_format_inr($servise_charge, 2) . "</td>
                                <td align='right'>" . Controller::money_format_inr($invoice, 2) . "</td>
                                <td align='right'>" . Controller::money_format_inr(abs($deficit), 2) . "</td>
                                <td align='right'>" . Controller::money_format_inr(abs($surplus), 2)  . "</td>
                                <td>" . $data['remarks'] . "</td>

                            </tr>";
                }
            }

            if ($data['project_status'] == 85) {
                $finaldata .= "<td></td>";
            }
            $finaldata .= "
                            </tr>";
        } else {

            $finaldata .= "<tr>

                                <td colspan='12'>No results found.</td>

                            </tr>";
        }
        $return_data = array(
            'final_data' => $finaldata, 'recipt_total' => $receipt_grandtotal,
            'deficit_total' => $deficit_grandtotal, 'surplus_total' => $surplus_grandtotal
        );
        return json_encode($return_data);
    }
    private function entries1($datamodel, $type = '')
    {
        $tblpx      = Yii::app()->db->tablePrefix;
        $finaldata  = "";
        $reportdata = $datamodel->getData();
        if (!empty($reportdata)) {
            $i                  = 0;
            $receipt_grandtotal = 0;
            $deficit_grandtotal = 0;
            $surplus_grandtotal = 0;
            $paid_total         = 0;
            foreach ($reportdata as $data) {
                $i++;
                $companyname    = array();
                $user           = Users::model()->findByPk(Yii::app()->user->id);
                $comVal         = explode(',', $user->company_id);
                $arrVal         = explode(',', $data['company_id']);
                foreach ($arrVal as $arr) {
                    if (in_array($arr, $comVal)) {
                        $value = Company::model()->findByPk($arr);
                        if($value){
                            array_push($companyname, $value->name);
                        }
                    }
                }
                $projectid  = $data['pid'];
                $inv_dtls   = Yii::app()->db->createCommand("SELECT SUM(amount) as total_amount,SUM(tax_amount) as tax_amount FROM {$tblpx}invoice WHERE project_id = " . $projectid . " AND invoice_status ='saved'")->queryRow();
                if (($inv_dtls)) {
                    if (($inv_dtls['total_amount'] + $inv_dtls['tax_amount']) == 0) {
                        $invoice = 0;
                    } else {
                        $invoice = $inv_dtls['total_amount'] + $inv_dtls['tax_amount'];
                    }
                } else {
                    $invoice = 0;
                }
                $rec    = '';
                if ($data['project_type'] == 87) {
                    $rec = "<td align='right'>" . $data['sqft_rate'] . "</td>";
                }
                if ($data['project_type'] == 86) {
                    $rec = "<td align='right'>" . ($data['percentage']) ? $data['percentage'] . '%' : '' . "</td>";
                }
                if ($data['project_status'] == 85) {
                    $rec = "<td align='right'>" . $data['sqft_rate'] . "</td>";
                }
                $percentage     = Yii::app()->db->createCommand("SELECT percentage FROM {$tblpx}projects WHERE pid = {$projectid}")->queryRow();
                $servise_charge = ($data['tot_expense'] * $percentage['percentage']) / 100;
                $receipt_grandtotal += $data['tot_receipt'];
                $paid_total         += $data['tot_paid_to_vendor'];
                if ($type == 'rate') {
                    if ($invoice == 0) {
                        $diff = ($data['tot_expense'] + $servise_charge) - $data['tot_receipt'];
                    } else {
                        $diff = $invoice - $data['tot_receipt'];
                    }
                    $deficit = 0;
                    $surplus = 0;
                    if ($diff < 0) {
                        $surplus = $diff;
                    } else {
                        $deficit = $diff;
                    }
                    $deficit_grandtotal += $deficit;
                    $surplus_grandtotal += $surplus;
                    $finaldata .=   "<tr>
                                        <td>" . $i . "</td>
                                        <td>" . $data['name'] . "</td>
                                        <td>" . $data['sqft'] . "</td>
                                        <td>" . $rec . "</td>
                                        <td align='right'>" . Controller::money_format_inr($data['tot_receipt'], 2) . "</td>
                                        <td align='right'>" . Controller::money_format_inr($data['tot_expense'], 2) . "</td>
                                        <td align='right'>" . Controller::money_format_inr($invoice, 2) . "</td>
                                        <td align='right'>" . Controller::money_format_inr(abs($deficit), 2) . "</td>
                                        <td align='right'>" . Controller::money_format_inr(abs($surplus), 2)  . "</td>
                                        <td>" . $data['remarks'] . "</td>
                                    </tr>";
                } else if ($type == 'percentage') {
                    if ($invoice == 0) {
                        $diff = ($data['tot_expense'] + $servise_charge) - $data['tot_receipt'];
                    } else {
                        $diff = $invoice - $data['tot_receipt'];
                    }
                    $deficit = 0;
                    $surplus = 0;
                    if ($diff < 0) {
                        $surplus = $diff;
                    } else {
                        $deficit = $diff;
                    }
                    $deficit_grandtotal += $deficit;
                    $surplus_grandtotal += $surplus;
                    $finaldata .=   "<tr>
                                        <td>" . $i . "</td>
                                        <td>" . $data['name'] . "</td>
                                        <td>" . $data['sqft'] . "</td>
                                        " . $rec . "
                                        <td align='right'>" . Controller::money_format_inr($data['tot_receipt'], 2) . "</td>
                                        <td align='right'>" . Controller::money_format_inr($data['tot_expense'], 2) . "</td>

                                        <td align='right'>" . Controller::money_format_inr($servise_charge, 2) . "</td>
                                        <td align='right'>" . Controller::money_format_inr($invoice, 2) . "</td>
                                        <td align='right'>" . Controller::money_format_inr(abs($deficit), 2) . "</td>
                                        <td align='right'>" . Controller::money_format_inr(abs($surplus), 2)  . "</td>
                                        <td>" . $data['remarks'] . "</td>

                                    </tr>";
                }
            }
            if ($type == 'rate') {
                $finaldata .=   "<tr class='grandtotal'>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td align='right'>" . Controller::money_format_inr($receipt_grandtotal, 2) . "</td>
                                    <td></td>
                                    <td></td>
                                    <td align='right'>" . Controller::money_format_inr(abs($deficit_grandtotal), 2) . "</td>
                                    <td align='right'>" .  Controller::money_format_inr(abs($surplus_grandtotal), 2)  . "</td>";
            } else if ($type == 'percentage') {
                $finaldata .=   "<tr class='grandtotal'>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td align='right'>" . Controller::money_format_inr($receipt_grandtotal, 2) . "</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td align='right'>" . Controller::money_format_inr(abs($deficit_grandtotal), 2) . "</td>
                                    <td align='right'>" .  Controller::money_format_inr(abs($surplus_grandtotal), 2)  . "</td>";
            }
            if ($data['project_status'] == 85) {
                $finaldata .= "<td></td>";
            }

            $finaldata .=   "
                            </tr>";
        } else {
            $finaldata .=   "<tr>
                                <td colspan='12'>No results found.</td>
                            </tr>";
        }
        return $finaldata;
    }

    private function expense_entries($project_id, $fromdate, $todate, $company_id)
    {

        $tblpx = Yii::app()->db->tablePrefix;
        $project = Yii::app()->db->createCommand("SELECT *,caption FROM {$tblpx}projects inner join {$tblpx}status on {$tblpx}projects.project_type ={$tblpx}status.sid  WHERE pid = {$project_id}")->queryRow();

        $name = $project['name'];
        $site = $project['site'];
        $clientid = $project['client_id'];
        $wc = $project['caption'];
        $wc_type = $project['project_type'];
        $sqft = $project['sqft'];
        $sqft_rate = $project['sqft_rate'];
        $percentage = $project['percentage'];
        $clientdetails = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}clients WHERE cid = {$clientid}")->queryRow();
        $clientname = $clientdetails['name'];

        $where = '';
        $where1 = '';
        $where2 = '';
        if (!empty($fromdate) && !empty($todate)) {
            $where .= " AND {$tblpx}expenses.date between '" . $fromdate . "' and'" . $todate . "'";
            $where1 .= " AND {$tblpx}bills.bill_date between '" . $fromdate . "' and'" . $todate . "'";
            $where2 .= " AND {$tblpx}invoice.date between '" . $fromdate . "' and'" . $todate . "'";
        } else {
            if (!empty($fromdate)) {
                $where .= " AND {$tblpx}expenses.date >= '" . $fromdate . "'";
                $where1 .= " AND {$tblpx}bills.bill_date >= '" . $fromdate . "'";
                $where2 .= " AND {$tblpx}invoice.date >= '" . $fromdate . "'";
            }
            if (!empty($todate)) {
                $date_to = date('Y-m-d', strtotime($todate));
                $where .= " AND {$tblpx}expenses.date <= '" . $todate . "'";
                $where1 .= " AND {$tblpx}bills.bill_date <= '" . $todate . "'";
                $where2 .= " AND {$tblpx}invoice.date <= '" . $todate . "'";
            }
        }

        $newQuery1 = "";
        $newQuery2 = "";
        $newQuery3 = "";
        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);

        if ($company_id != NULL) {
            $newQuery1 .= " FIND_IN_SET('" . $company_id . "', {$tblpx}expenses.company_id)";
            $newQuery2 .= " FIND_IN_SET('" . $company_id . "', company_id)";
            $newQuery3 .= " FIND_IN_SET('" . $company_id . "', {$tblpx}bills.company_id)";
        } else {
            foreach ($arrVal as $arr) {
                if ($newQuery1) $newQuery1 .= ' OR';
                if ($newQuery2) $newQuery2 .= ' OR';
                if ($newQuery3) $newQuery3 .= ' OR';
                $newQuery1 .= " FIND_IN_SET('" . $arr . "', {$tblpx}expenses.company_id)";
                $newQuery2 .= " FIND_IN_SET('" . $arr . "', company_id)";
                $newQuery3 .= " FIND_IN_SET('" . $arr . "', {$tblpx}bills.company_id)";
            }
        }

        if ($wc_type == 87) {
            $sqft_rate_percentage = "<b>Square Feet Rate :</b>" . $project['sqft_rate'];
        } else if ($wc_type == 86) {
            $sqft_rate_percentage = "<b>Percentage :</b>" . (($project['percentage']) ? $project['percentage'] . '%' : '');
        }



        $exptype = Yii::app()->db->createCommand("select exptype from " . $tblpx . "expenses

				WHERE projectid =" . $project_id . " AND " . $tblpx . "expenses.type = 73 " . $where . "")->queryAll();

        $data = array();
        foreach ($exptype as $exp) {

            array_push($data, $exp['exptype']);
        }

        $ex = implode(',', $data);


        if (!empty($exptype)) {

            $sql = Yii::app()->db->createCommand("select DISTINCT " . $tblpx . "project_exptype.type_id,project_id,name,type_name
            from " . $tblpx . "project_exptype
            inner join " . $tblpx . "projects on " . $tblpx . "project_exptype.project_id = " . $tblpx . "projects.pid
            inner join " . $tblpx . "expense_type on " . $tblpx . "expense_type.type_id = " . $tblpx . "project_exptype.type_id
            inner join " . $tblpx . "expenses on " . $tblpx . "expenses.projectid = " . $tblpx . "projects.pid
            WHERE  project_id=" . $project_id . " " . $where . " AND (" . $newQuery1 . ") ORDER BY " . $tblpx . "expense_type.type_name")->queryAll();
        } else {
            $sql = array();
        }


        $purchase = Yii::app()->db->createCommand("SELECT * FROM  " . $tblpx . "bills LEFT JOIN " . $tblpx . "purchase ON " . $tblpx . "bills.purchase_id=" . $tblpx . "purchase.p_id WHERE " . $tblpx . "purchase.project_id=" . $project_id . " AND " . $tblpx . "bills.purchase_id IS NOT NULL " . $where1 . " AND (" . $newQuery3 . ")")->queryAll();

        $client = Yii::app()->db->createCommand("select * from " . $tblpx . "expenses
            WHERE amount !=0 AND projectid=" . $project_id . " AND type = '72' " . $where . " AND return_id IS NULL AND (" . $newQuery2 . ") ORDER BY date")->queryAll();

        $expreceipt_return = Yii::app()->db->createCommand("select * from " . $tblpx . "expenses
        WHERE amount !=0 AND projectid=" . $project_id . " AND type = '72' " . $where . " AND return_id IS NOT NULL AND (" . $newQuery2 . ") ORDER BY date")->queryAll();

        $daybook_expense = Yii::app()->db->createCommand("select * from " . $tblpx . "expenses
            WHERE amount !=0 AND projectid=" . $project_id . " AND type = '73' " . $where . " AND exptype IS NULL AND vendor_id IS NULL AND bill_id IS NULL AND (" . $newQuery2 . ") ORDER BY date")->queryAll();

        $invoice_dtls = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}invoice WHERE project_id = " . $project_id . " AND invoice_status ='saved' AND (" . $newQuery2 . ") " . $where2 . "")->queryAll();

        $finaldata = "";


        $clienttotal = 0;
        $invoice = 0;

        if ((!empty($sql) || !empty($client) || !empty($daybook_expense) || !empty($purchase)) && ($project_id != 0)) {



            if (!empty($clientdetails)) {

                $finaldata .= "

                    <tr>

                        <td colspan='5'><b>Client : </b>$clientname</td>

                    </tr> <tr>

                        <td colspan='3'><b>Project Name: </b>$name </td>
                        <td colspan='2'><b>Square Feet: </b> $sqft</td>

                    </tr><tr>

                        <td colspan='3'><b>Work Contract: </b> $wc</td>

                       <td colspan='2'>$sqft_rate_percentage</td>

                    </tr>
                    <tr><td colspan='5'><b>Site: </b> $site</td></tr>


                    <tr>

                        <td></td>
                        <td align='right'><b>Receipt</b></td>

                        <td colspan='3' align='right'><b>Payment</b></td> ";

                $finaldata .= " </tr> <tr><td colspan='5'><b>Salesbook entry / Invoice</b></td></tr>";
                foreach ($invoice_dtls as $invoice_data) {

                    $invoice += $invoice_data['amount'] + $invoice_data['tax_amount']; //$newclient['receipt_amount'];



                    $finaldata .= "<tr>

                                        <td>" . date('d-M-Y', strtotime($invoice_data['date'])) . "</td>

                                        <td align='right'>" . Controller::money_format_inr($invoice_data['amount'] + $invoice_data['tax_amount'], 2) . "</td>
                                        <td colspan='2'></td>



                                    </tr>";
                }
                $finaldata .= "<tr><td align='right' colspan='1'><b>Total</b></td><td align='right' ><b>" . Controller::money_format_inr($invoice, 2) . "</b></td><td colspan='2'></td></tr>";

                $finaldata .= " </tr> <tr><td colspan='5'><b>Daybook Receipt</b></td></tr>";

                foreach ($client as $newclient) {

                    $clienttotal += $newclient['receipt']; //$newclient['receipt_amount'];



                    $finaldata .= "<tr>

                                        <td>" . date('d-M-Y', strtotime($newclient['date'])) . " " . $newclient['description'] . "</td>

                                        <td align='right'>" . Controller::money_format_inr($newclient['receipt'], 2) . "</td>
                                        <td colspan='3'></td>



                                    </tr>";
                }
                $finaldata .= "<tr><td align='right' colspan='1'><b>Total</b></td><td align='right' ><b>" . Controller::money_format_inr($clienttotal, 2) . "</b></td><td colspan='3'></td></tr>";
            }
            $receipt_return = 0;
            foreach ($expreceipt_return as $newclient) {

                $clienttotal += $newclient['receipt']; //$newclient['receipt_amount'];



                $finaldata .= "<tr>

                                        <td>" . date('d-M-Y', strtotime($newclient['date'])) . " " . $newclient['description'] . "</td>

                                        <td align='right'>" . Controller::money_format_inr($newclient['receipt'], 2) . "</td>
                                        <td colspan='3'></td>



                                    </tr>";
            }
            $finaldata .= "<tr><td align='right' colspan='1'><b>Total</b></td><td align='right' ><b>" . Controller::money_format_inr($clienttotal, 2) . "</b></td><td colspan='3'></td></tr>";
            $clienttotal = $clienttotal + $receipt_return;
            $finaldata .= "<tr><td align='right' colspan='1'><b>Net Amount </b></td><td align='right' ><b>" . Controller::money_format_inr($clienttotal, 2) . "</b></td><td colspan='3'></td></tr>";

            $finaldata .= "</tr> <tr><td colspan='5'><b>Purchase Bills</b></td></tr>";
            $finaldata .= "<tr><td><b>Date</b></td><td><b>Bill No & Purchase No</b></td><td><b>Vendor</b></td><td colspan='3'><b>Amount</b></td></tr> ";
            $bill_amount = 0;
            if (!empty($purchase)) {
                foreach ($purchase as $value) {
                    $vendor = Vendors::model()->findByPk($value['vendor_id']);
                    $bill_amount += $value['bill_totalamount']; //$newclient['receipt_amount'];
                    $finaldata .= "<tr>
                                        <td>" . date('d-M-Y', strtotime($value['bill_date'])) . " </td>
                                        <td>" . $value['bill_number'] . ' - ' . $value['purchase_no'] . "</td>
                                        <td>" . $vendor->name . "</td>
                                        <td colspan='3' align='right'>" . Controller::money_format_inr($value['bill_totalamount'], 2) . "</td>
                                    </tr>";
                }
                $finaldata .= "<tr><td align='right' colspan='3'><b>Total</b></td><td align='right' ><b>" . Controller::money_format_inr($bill_amount, 2) . "</b></td></tr>";
            }

            $finaldata .= "<tr><td colspan='5'><b>Daybook Expense</b></td></tr>";

            $grand = 0;
            $total = 0;
            $daybook_exp_total = 0;
            foreach ($sql as $new) {

                $sqlexp = Yii::app()->db->createCommand("select * from " . $tblpx . "expenses

                WHERE amount!=0 AND exptype=" . $new['type_id'] . "  AND projectid=" . $new['project_id'] . " " . $where . " AND exptype IS NOT NULL AND vendor_id IS NOT NULL AND bill_id IS NULL AND (" . $newQuery2 . ")
 ORDER BY date ")->queryAll();

                if (!empty($sqlexp)) {

                    $finaldata .= "<tr>

                                    <td colspan='5' ><b>" . $new['type_name'] . "</b></td>

                                </tr>";







                    $total = 0;
                    foreach ($sqlexp as $newsql) {

                        if ($newsql['paid'] != 0) {

                            $total += $newsql['paid'];

                            $finaldata .= "<tr>

                                            <td>" . date('d-M-Y', strtotime($newsql['date'])) . " " . $newsql['works_done'] . " " . $newsql['description'] . "</td>

                                            <td></td>

                                            <td colspan='3' align='right'>" . Controller::money_format_inr($newsql['paid'], 2) . "</td>

                                        </tr>";
                        }
                    }
                    $finaldata .= '<tr><td align="right" colspan="2"><b>Sub total</b></td><td colspan="3" align="right" ><b>' . Controller::money_format_inr($total, 2) . '</b></td></tr>';

                    $grand += $total;
                    $daybook_exp_total += $total;
                }
            }

            $finaldata .= "<tr>
                <td colspan='5' class='whiteborder' >&nbsp;</td>
            </tr>";

            $finaldata .= '<tr><td align="right" colspan="2"><b>Total</b></td><td colspan="3" align="right" ><b>' . Controller::money_format_inr($daybook_exp_total, 2) . '</b></td></tr>';
            $finaldata .= "<tr>
                <td colspan='5' class='whiteborder' >&nbsp;</td>
            </tr>
            <tr>
                <td colspan='5'><b>Subcontractor Payment<b></td>
            </tr>";
            $daybook_expenseamount = 0;
            $check_array = array();
            foreach ($daybook_expense as $newdata) {
                $payment = SubcontractorPayment::model()->findByPk($newdata['subcontractor_id']);
                if (isset($payment->subcontractor_id)) {
                    $subcontractor_data = Subcontractor::model()->findByPk($payment->subcontractor_id);
                }
                $tblpx = Yii::app()->db->tablePrefix;
                $payment = SubcontractorPayment::model()->findByPk($newdata['subcontractor_id']);
                $query = "";
                $subcontractor_id = '';
                if (isset($payment->subcontractor_id)) {
                    $query = ' AND b.subcontractor_id=' . $payment->subcontractor_id . '';
                    $subcontractor_id = $payment->subcontractor_id;
                }
                $sub_total1 = Yii::app()->db->createCommand("select SUM(a.paid) as paid_amount,count(a.exp_id) as total_count from jp_expenses a LEFT JOIN jp_subcontractor_payment b ON a.subcontractor_id=b.payment_id WHERE a.amount !=0 AND a.projectid=" . $project_id . " AND a.type = '73' AND a.exptype IS NULL AND a.vendor_id IS NULL AND a.bill_id IS NULL AND a.date ='" . $newdata['date'] . "' " . $query . " GROUP BY a.date,b.subcontractor_id")->queryRow();

                $check_data = $newdata['date'] . '-' . $subcontractor_id;
                if (in_array($check_data, $check_array)) {
                    $flag = 0;
                    $row_count = "";
                } else {
                    $flag = 1;
                    $row_count = $sub_total1['total_count'];
                }
                array_push($check_array, $check_data);
                $finaldata .= "<tr>
                    <td>" . date('d-M-Y', strtotime($newdata['date'])) . " " . $newdata['description'] . "</td>
                    <td>" . $subcontractor_data['subcontractor_name'] . "</td>
                    <td align='right'>" . Controller::money_format_inr($newdata['paid'], 2) . "</td>
                    <td colspan=" . $row_count . " align='right'>";
                $finaldata .= ($flag == 1) ? Controller::money_format_inr($sub_total1['paid_amount'], 2, 1) : "";
                $finaldata .= "</td>
                                    </tr>";
                $daybook_expenseamount += $newdata['paid'];
            }
            $grand += $daybook_expenseamount;
            $finaldata .= "<tr><td align='right' colspan='3'><b>Total</b></td><td align='right' ><b>" . Controller::money_format_inr($daybook_expenseamount, 2) . "</b></td></tr>";



            $percentage = Yii::app()->db->createCommand("SELECT percentage
             FROM {$tblpx}projects
             inner join " . $tblpx . "expenses on " . $tblpx . "expenses.projectid = " . $tblpx . "projects.pid
              WHERE pid = {$project_id} " . $where . "")->queryRow();

            $servise_charge = (($bill_amount + $grand) * $percentage['percentage']) / 100;


            if ($invoice == 0) {

                $balance = ($bill_amount + $grand + $servise_charge) - $clienttotal;
            } else {

                $balance = $invoice - $clienttotal;    // Invoice - Receipt

            }


            $finaldata .= " <tr>
                                <td>&nbsp;</td>
                                <td align='right'>----------------------</td>
                                <td colspan='3' align='right'>----------------------</td>
                            </tr>

                            <tr>
                                <td>Grand Total</td>
                                <td align='right'><b>" . Controller::money_format_inr($clienttotal, 2) . "</b></td>

                                <td colspan='3' align='right'><b>" . Controller::money_format_inr($grand + $bill_amount, 2) . "</b></td>
                            </tr>
	                        <tr>  
                                <td>&nbsp;</td>
                                <td align='right'>==========</td>
                                <td colspan='3' align='right'>==========</td>
                            </tr>
							<tr>
                                <td>INVOICE : </td>
                                <td align='right'><b>" . Controller::money_format_inr($invoice, 2) . "</b></td>
                                <td colspan='3'></td>
                            </tr>
                            <tr>
                                <td>AMOUNT RECEIVED : </td>
                                <td align='right'><b>" . Controller::money_format_inr($clienttotal, 2) . "</b></td>
                                <td colspan='3'></td>
                            </tr>
                            <tr>
                                <td>LESS EXPENSES :</td>
                                <td align='right'><b>" . Controller::money_format_inr(($grand + $bill_amount), 2) . "</b></td>
                                <td colspan='3'></td>
                            </tr>";

            $finaldata .= " <tr>
                                <td>SERVICE CHARGE : </td>
                                <td align='right'><b>" . Controller::money_format_inr($servise_charge, 2) . "</b></td>
                                <td colspan='3'></td>
                            </tr>.";
            if ($balance > 0) {

                $titlebal = "DEFICIT AMOUNT";
            } else {

                $titlebal = "SURPLUS AMOUNT";
            }

            $finaldata .= "<tr>
                                <td>" . $titlebal . "</td>
                                <td align='right'><b>" . Controller::money_format_inr(abs($balance), 2) . "</b></td>
                                <td colspan='3'></td>

                            </tr>";
        } else {

            $finaldata .= "<tr>
                                <td colspan='5'>No results found.</td>
                            </tr>";
        }
        return $finaldata;
    }


    public function actionProjectEntries()
    {
        $model = new Expenses;
        $id = Yii::app()->user->id;
        $this->render('projectbook', array('model' => $model, 'id' => $id));
    }

    public function actionProjectData()
    {

        $tblpx = Yii::app()->db->tablePrefix;
        if (isset($_REQUEST['cadate']) || isset($_REQUEST['userid']))
            $cadate = date("Y-m-d", strtotime($_REQUEST['cadate']));
        $userid = isset($_REQUEST['userid']) ? $_REQUEST['userid'] : 0;


        if ($userid != 0) {
            $data = Yii::app()->db->createCommand("SELECT p.name,pb.pb_id, pb.works_done, t.type_name,v.name as vname,pb.book_description
			   FROM " . $tblpx . "projectbook as pb
			   left join " . $tblpx . "projects as p on p.pid=pb.projectid "
                . " left join " . $tblpx . "expense_type as t on t.type_id=pb.exptype"
                . " left join " . $tblpx . "vendors as v on v.vendor_id=pb.vendor"
                . " where userid='" . $userid . "' AND pb.book_date='" . $cadate . "' ORDER BY pb.pb_id")->queryAll();
        } else {
            $data = Yii::app()->db->createCommand("SELECT p.name,pb.pb_id, pb.works_done, t.type_name,v.name as vname,pb.book_description
			   FROM " . $tblpx . "projectbook as pb
			   left join " . $tblpx . "projects as p on p.pid=pb.projectid "
                . " left join " . $tblpx . "expense_type as t on t.type_id=pb.exptype"
                . " left join " . $tblpx . "vendors as v on v.vendor_id=pb.vendor"
                . " where pb.book_date='" . $cadate . "' ORDER BY pb.pb_id")->queryAll();
        }
        $exp_pro = Yii::app()->db->createCommand("SELECT extype.type_name,p.name FROM {$tblpx}expense_type extype LEFT JOIN {$tblpx}project_exptype pe ON pe.type_id = extype.type_id LEFT JOIN {$tblpx}projects p ON p.pid = pe.project_id ORDER BY p.name")->queryAll();
        $data1 = array();
        $pname = '';
        foreach ($exp_pro as $d) {
            if ($pname != $d['name']) {
                $pname = $d['name'];
                $arr = array();
                array_unshift($arr, "");
            }
            if ($pname == $d['name']) {
                $arr[] = $d['type_name'];
            }
            $proj = str_replace(' ', '', $d['name']);
            $data1[$proj] = $arr;
        }

        $proj_exp[] = $data1;

        $vendor_pro = Yii::app()->db->createCommand("SELECT v.name as vname,exp.type_name FROM {$tblpx}vendors v LEFT JOIN {$tblpx}vendor_exptype ve ON ve.vendor_id = v.vendor_id LEFT JOIN {$tblpx}expense_type exp ON exp.type_id = ve.type_id ORDER BY exp.type_name")->queryAll();
        //echo "SELECT v.name as vname,exp.type_name FROM {$tblpx}vendors v LEFT JOIN {$tblpx}vendor_exptype ve ON ve.vendor_id = v.vendor_id LEFT JOIN {$tblpx}expense_type exp ON exp.type_id = ve.type_id ORDER BY exp.type_name";exit();
        $data1 = array();
        $vname = '';

        foreach ($vendor_pro as $d) {
            if ($vname != $d['type_name']) {
                $vname = $d['type_name'];
                $arr2 = array();
                array_unshift($arr2, "");
            }
            if ($vname == $d['type_name']) {
                $arr2[] = $d['vname'];
            }
            $exp = str_replace(' ', '', $d['type_name']);
            $data1[$exp] = $arr2;
        }

        $vendor_exp[] = $data1;

        if ($data) {
            $data1 = array();

            foreach ($data as $dt) {

                $res = array($dt['pb_id'], $dt['name'], $dt['type_name'], $dt['vname'], $dt['works_done'], $dt['book_description']);
                array_push($data1, $res);
            }
            echo json_encode(array('result' => $data1, 'proj_exp' => $proj_exp, 'vendor_exp' => $vendor_exp));
        } else {
            $data1 = array();
            $res = array("", "", "", "", "", "", "");
            array_push($data1, $res);
            echo json_encode(array("result" => $data1, 'proj_exp' => $proj_exp, 'vendor_exp' => $vendor_exp));
        }
    }

    public function actionProjectdetails()
    {
        $tblpx = Yii::app()->db->tablePrefix;
        if (isset($_REQUEST['cadate']) || isset($_REQUEST['userid']))
            $cadate = date("Y-m-d", strtotime($_REQUEST['cadate']));
        $userid = isset($_REQUEST['userid']) ? $_REQUEST['userid'] : 0;

        $lastdate = Yii::app()->db->createCommand("SELECT book_date FROM " . $tblpx . "projectbook WHERE userid = '" . Yii::app()->user->id . "' ORDER BY book_date DESC LIMIT 1")->queryRow();
        if ($userid != 0) {
            $qry = "SELECT p.name,pb.pb_id, pb.works_done, t.type_name,v.name as vname ,pb.book_description
			   FROM " . $tblpx . "projectbook as pb
			   left join " . $tblpx . "projects as p on p.pid=pb.projectid
                           left join " . $tblpx . "expense_type as t on t.type_id=pb.exptype
                           left join " . $tblpx . "vendors as v on v.vendor_id=pb.vendor
                           where userid= '$userid' AND pb.book_date='" . $cadate . "' ORDER BY pb.pb_id";
        } else {
            $qry = "SELECT p.name,pb.pb_id, pb.works_done,  t.type_name,v.name as vname,pb.book_description
			   FROM " . $tblpx . "projectbook as pb
			   left join " . $tblpx . "projects as p on p.pid=pb.projectid "
                . " left join " . $tblpx . "expense_type as t on t.type_id=pb.exptype"
                . " left join " . $tblpx . "vendors as v on v.vendor_id=pb.vendor"
                . " where pb.book_date='" . $cadate . "' ORDER BY pb.pb_id";
        }

        $data = Yii::app()->db->createCommand($qry)->queryAll();

        $exp_pro = Yii::app()->db->createCommand("SELECT extype.type_name,p.name FROM {$tblpx}expense_type extype LEFT JOIN {$tblpx}project_exptype pe ON pe.type_id = extype.type_id LEFT JOIN {$tblpx}projects p ON p.pid = pe.project_id ORDER BY p.name")->queryAll();
        $data1 = array();
        $pname = '';
        foreach ($exp_pro as $d) {
            if ($pname != $d['name']) {
                $pname = $d['name'];
                $arr = array();
                array_unshift($arr, "");
            }
            if ($pname == $d['name']) {
                $arr[] = $d['type_name'];
            }
            $proj = str_replace(' ', '', $d['name']);
            $data1[$proj] = $arr;
        }

        $proj_exp[] = $data1;

        $vendor_pro = Yii::app()->db->createCommand("SELECT v.name as vname,exp.type_name FROM {$tblpx}vendors v LEFT JOIN {$tblpx}vendor_exptype ve ON ve.vendor_id = v.vendor_id LEFT JOIN {$tblpx}expense_type exp ON exp.type_id = ve.type_id ORDER BY exp.type_name")->queryAll();
        //echo "SELECT v.name as vname,exp.type_name FROM {$tblpx}vendors v LEFT JOIN {$tblpx}vendor_exptype ve ON ve.vendor_id = v.vendor_id LEFT JOIN {$tblpx}expense_type exp ON exp.type_id = ve.type_id ORDER BY exp.type_name";exit();
        $data1 = array();
        $vname = '';

        foreach ($vendor_pro as $d) {
            if ($vname != $d['type_name']) {
                $vname = $d['type_name'];
                $arr2 = array();
                array_unshift($arr2, "");
            }
            if ($vname == $d['type_name']) {
                $arr2[] = $d['vname'];
            }
            $exp = str_replace(' ', '', $d['type_name']);
            $data1[$exp] = $arr2;
        }

        $vendor_exp[] = $data1;

        if ($data) {
            $data1 = array();

            foreach ($data as $dt) {
                $res = array($dt['pb_id'], $dt['name'], $dt['type_name'], $dt['vname'], $dt['works_done'], $dt['book_description']);
                array_push($data1, $res);
            }
            echo json_encode(array('result' => $data1, 'proj_exp' => $proj_exp, 'vendor_exp' => $vendor_exp, 'lastdate' => isset($lastdate['book_date']) ? date("d-M-Y", strtotime($lastdate['book_date'])) : ''));
        } else {
            $data1 = array();
            $res = array("", "", "", "", "", "", "");
            array_push($data1, $res);
            echo json_encode(array("result" => $data1, 'proj_exp' => $proj_exp, 'vendor_exp' => $vendor_exp, 'lastdate' => isset($lastdate['book_date']) ? date("d-M-Y", strtotime($lastdate['book_date'])) : ''));
        }
    }

    public function actionSavedata()
    {

        $tblpx = Yii::app()->db->tablePrefix;
        $userid = Yii::app()->user->id;

        if ($userid != "") {
            if (!empty($_POST['data'])) {

                ////Insert details to expense and receipt table coding starts from here
                $data = $_POST['data'];
                /* echo "<pre>";
                  print_r($data);die; */
                foreach ($data as $dt) {
                    $pb_id = $dt[0];
                    $proj_name = $dt[1];
                    $project_id_q = Yii::app()->db->createCommand("select `pid` from " . $tblpx . "projects where `name`='$proj_name'")->queryAll();

                    foreach ($project_id_q as $proj) {
                        $proj_id = $proj['pid'];
                    }
                    $vid = NULL;
                    $exp_found = Yii::app()->db->createCommand("select pb_id from " . $tblpx . "projectbook where pb_id='$pb_id'")->queryAll();
                    if (count($exp_found) == 0) {
                        $project_model = new Projectbook();
                        $project_model->projectid = $proj_id;
                        $project_model->userid = $userid;
                        $project_model->created_by = $userid;
                        $project_model->works_done = $dt[4];

                        $exptype  = Yii::app()->db->createCommand("select `type_id` from " . $tblpx . "expense_type where `type_name`='$dt[2]'")->queryAll();
                        foreach ($exptype as $expid) {
                            $exid = $expid['type_id'];
                        }
                        $project_model->exptype = $exid;
                        $vendortype  = Yii::app()->db->createCommand("select `vendor_id` from " . $tblpx . "vendors where `name`='$dt[3]'")->queryAll();
                        foreach ($vendortype as $vendorid) {
                            $vid = $vendorid['vendor_id'];
                        }
                        $project_model->vendor = $vid;
                        $project_model->book_description = $dt[5];
                        $project_model->book_date = date("Y-m-d", strtotime($_POST['date']));
                        $project_model->updated_by = $userid;
                        $project_model->updated_date = date('Y-m-d H:i:s');
                        $project_model->pb_id = null;
                        $project_model->save(false);
                        //echo json_encode(array("result" => "ok"));
                    } else {
                        $project_model = Projectbook::model()->findByPk($pb_id);
                        $project_model->projectid = $proj_id;
                        $project_model->userid = $userid;
                        $project_model->works_done = $dt[4];
                        $exptype  = Yii::app()->db->createCommand("select `type_id` from " . $tblpx . "expense_type where `type_name`='$dt[2]'")->queryAll();
                        foreach ($exptype as $expid) {
                            $exid = $expid['type_id'];
                        }
                        $project_model->exptype = $exid;
                        $vendortype  = Yii::app()->db->createCommand("select `vendor_id` from " . $tblpx . "vendors where `name`='$dt[3]'")->queryAll();
                        foreach ($vendortype as $vendorid) {
                            $vid = $vendorid['vendor_id'];
                        }
                        $project_model->vendor = $vid;
                        $project_model->book_description = $dt[5];
                        $project_model->book_date = date("Y-m-d", strtotime($_POST['date']));
                        $project_model->updated_by = $userid;
                        $project_model->updated_date = date('Y-m-d H:i:s');
                        $project_model->save(false);
                        //echo json_encode(array("result" => "ok"));
                    }
                }
                ////Insertion end
                //echo json_encode(array("result" => "ok"));
            }
            echo json_encode(array("result" => "ok"));
        } else {
            echo json_encode(array("result" => "empty"));
        }
    }

    public function actionHandsonDelete()
    {

        $id = $_POST['id'];
        //print_r($id);
        $tblpx = Yii::app()->db->tablePrefix;
        $query = Yii::app()->db->createCommand('DELETE FROM ' . $tblpx . 'projectbook WHERE pb_id=' . $id)->execute();
        $out = array(
            'result' => 'ok'
        );
        echo json_encode($out);
    }


    public function actionProgressSave()
    {

        $tblpx = Yii::app()->db->tablePrefix;
        $userid = Yii::app()->user->id;

        if ($userid != "") {

            $proj_name = $_REQUEST['projectid'];
            $project_id_q = Yii::app()->db->createCommand("select `pid` from " . $tblpx . "projects where `name`='$proj_name'")->queryAll();

            foreach ($project_id_q as $proj) {
                $proj_id = $proj['pid'];
            }
            $project_model = new Projectbook();
            $project_model->projectid = $proj_id;
            $project_model->userid = $userid;
            $project_model->created_by = $userid;

            $worksdone = $_REQUEST['worksdone'];
            $exp = $_REQUEST['exptype'];
            $desc = $_REQUEST['description'];
            $vendor = $_REQUEST['vendor'];
            $project_model->works_done = $worksdone;
            $exptype  = Yii::app()->db->createCommand("select `type_id` from " . $tblpx . "expense_type where `type_name`='$exp'")->queryAll();
            foreach ($exptype as $expid) {
                $exid = $expid['type_id'];
            }
            $project_model->exptype = $exid;
            $vendortype  = Yii::app()->db->createCommand("select `vendor_id` from " . $tblpx . "vendors where `name`='$vendor'")->queryAll();
            foreach ($vendortype as $vendorid) {
                $vid = $vendorid['vendor_id'];
            }
            $project_model->vendor = $vid;
            $project_model->book_description = $desc;
            $project_model->book_date = date("Y-m-d", strtotime($_REQUEST['date']));
            $project_model->updated_by = $userid;
            $project_model->updated_date = date('Y-m-d H:i:s');
            $project_model->pb_id = null;
            $project_model->save(false);


            echo json_encode(array("result" => "ok", "id" => Yii::app()->db->getLastInsertId()));
        } else {
            echo json_encode(array("result" => "empty"));
        }
    }
    public function  actiontotals()
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $payed_vendor = $tot_receipt = $tot_expense = $biled_to_client = 0;
        $project_id = 0;
        $project_name = "";

        if (isset($_REQUEST['project_id'])) {
            $project_id = Yii::app()->db->createCommand("SELECT pid FROM {$tblpx}projects WHERE pid='" . $_REQUEST['project_id'] . "'")->queryScalar();

            if ($project_id != "") {
                $project_name = Yii::app()->db->createCommand("SELECT name FROM {$tblpx}projects WHERE pid=$project_id")->queryScalar();
                $biled_to_client = Yii::app()->db->createCommand("SELECT SUM(amount) FROM {$tblpx}invoice WHERE project_id=$project_id")->queryScalar();
                $tot_expense = Yii::app()->db->createCommand("SELECT SUM(expense_totalamount) FROM {$tblpx}expenses WHERE project_id=$project_id AND type=73")->queryScalar();
                $tot_receipt = Yii::app()->db->createCommand("SELECT SUM(expense_totalamount) FROM {$tblpx}expenses WHERE project_id=$project_id AND type=72")->queryScalar();
                $payed_vendor = Yii::app()->db->createCommand("SELECT SUM(paidamount) FROM {$tblpx}expenses WHERE project_id=$project_id AND type=73")->queryScalar();
                $biled_to_client = ($biled_to_client != "" ? $biled_to_client : 0);
                $tot_expense = ($tot_expense != "" ? $tot_expense : 0);
                $tot_receipt = ($tot_receipt != "" ? $tot_receipt : 0);
                $payed_vendor = ($payed_vendor != "" ? $payed_vendor : 0);

                $sql_update_qry = "UPDATE {$tblpx}projects SET tot_expense='" . $tot_expense . "', tot_receipt='" . $tot_receipt . "', billed_to_client='" . $biled_to_client . "', tot_paid_to_vendor='" . $payed_vendor . "' WHERE pid='" . $project_id . "'";
                $updatequery = Yii::app()->db->createCommand($sql_update_qry)->execute();
                //die($sql_update_qry);
            } else {
                echo "Invalid project";
            }
        }


        $this->render('total', array(
            'biled_to_client' => $biled_to_client,
            'tot_expense' => $tot_expense,
            'tot_receipt' => $tot_receipt,
            'payed_vendor' => $payed_vendor,
            'id' => $project_id,
            'project_name' => $project_name,
        ));
    }
    public function actionSavetoExcel()
    {
        setlocale(LC_MONETARY, 'en_IN');
        //echo "Hello";
        $tblpx = Yii::app()->db->tablePrefix;
        $model = new Projects('financialreport1');

        $model->unsetAttributes();

        $financialdata1 = $model->financialreport1();

        $fixedprojects = $model->fixedprojects();

        $completed = $model->completed();

        $data = array();
        $data2 = array();
        $data3 = array();
        $receipt_grandtotal = 0;

        $deficit_grandtotal = 0;

        $surplus_grandtotal = 0;
        $paid_total = 0;
        $x = 0;
        $i = 0;
        $k = 0;

        $arraylabel = array('Fixed Rate Contracts', '', '', '', '', '', '', '', '', '', '', '');


        foreach ($financialdata1->getData() as $record) {

            $data[] = $record;
        }
        foreach ($fixedprojects->getData() as $record) {

            $data2[] = $record;
        }
        foreach ($completed->getData() as $record) {

            $data3[] = $record;
        }


        $finaldata = array();
        $finaldata[0][] = 'Project Name';
        $finaldata[0][] = 'Company';
        $finaldata[0][] = 'SQ-FT';
        $finaldata[0][] = 'SQ.FT.Rate or Lumpsum';
        $finaldata[0][] = 'Receipt';
        $finaldata[0][] = 'Expenses';
        $finaldata[0][] = 'Invoice';
        $finaldata[0][] = 'Deficit';
        $finaldata[0][] = 'Surplus';
        $finaldata[0][] = 'Remarks';
        $tot_data_1 = $this->getTotal($data);
        $finaldata[1][] = '';
        $finaldata[1][] = '';
        $finaldata[1][] = '';
        $finaldata[1][] = 'Total';
        $finaldata[1][] = Controller::money_format_inr($tot_data_1['receipt_tot'], 2);
        $finaldata[1][] = '';
        $finaldata[1][] = '';
        $finaldata[1][] = Controller::money_format_inr(abs($tot_data_1['deficit_total']), 2);
        $finaldata[1][] = Controller::money_format_inr(abs($tot_data_1['surplus_total']), 2);
        $finaldata[1][] = '';

        foreach ($data as $key => $datavalue) {
            $companyname = array();
            $user = Users::model()->findByPk(Yii::app()->user->id);
            $comVal = explode(',', $user->company_id);
            $arrVal = explode(',', $datavalue['company_id']);
            foreach ($arrVal as $arr) {
                if (in_array($arr, $comVal)) {
                    $value = Company::model()->findByPk($arr);
                    if($value){
                        array_push($companyname, $value->name);
                    }
                }
            }


            $inv_dtls = Yii::app()->db->createCommand("SELECT SUM(amount) as total_amount,SUM(tax_amount) as tax_amount FROM {$tblpx}invoice WHERE project_id = " . $datavalue['pid'] . "")->queryRow();
            if (($inv_dtls)) {
                if (($inv_dtls['total_amount'] + $inv_dtls['tax_amount']) == 0) {
                    $invoice = 0;
                } else {
                    $invoice = $inv_dtls['total_amount'] + $inv_dtls['tax_amount'];
                }
            } else {
                $invoice = 0;
            }

            $finaldata[$key + 1][] = $datavalue['name'];
            $finaldata[$key + 1][] = implode(', ', $companyname);
            $finaldata[$key + 1][] = $datavalue['sqft'];
            $finaldata[$key + 1][] = $datavalue['sqft_rate'];
            $finaldata[$key + 1][] = Controller::money_format_inr($datavalue['tot_receipt'], 2);
            $finaldata[$key + 1][] = Controller::money_format_inr($datavalue['tot_expense'], 2);

            $percentage = Yii::app()->db->createCommand("SELECT percentage FROM {$tblpx}projects WHERE pid = {$datavalue['pid']}")->queryScalar();
            $servise_charge = ($datavalue['tot_expense'] * $percentage) / 100;
            $finaldata[$key + 1][] = Controller::money_format_inr($invoice, 2);



            $receipt_grandtotal += $datavalue['tot_receipt'];



            if ($invoice == 0) {

                $diff = ($datavalue['tot_expense'] + $servise_charge) - $datavalue['tot_receipt'];
            } else {

                $diff = $invoice - $datavalue['tot_receipt'];
            }

            $deficit = 0;

            $surplus = 0;

            if ($diff < 0) {

                $surplus = $diff;
            } else {

                $deficit = $diff;
            }

            $deficit_grandtotal += $deficit;
            $surplus_grandtotal += $surplus;


            $finaldata[$key + 1][] = Controller::money_format_inr(abs($deficit), 2); //abs($deficit);
            $finaldata[$key + 1][] = Controller::money_format_inr(abs($surplus), 2); //abs($surplus);


            $finaldata[$key + 1][] = $datavalue['remarks'];
            $x = $key + 1;
        }
        //$finaldata[$x+1][]='';
        //  $finaldata[$x+1][]='';
        //  $finaldata[$x+1][]='';
        //  $finaldata[$x+1][]='';
        //  $finaldata[$x+1][]='';
        //  $finaldata[$x+1][]= Controller::money_format_inr($receipt_grandtotal,2); //$receipt_grandtotal;
        //  $finaldata[$x+1][]='';
        //  $finaldata[$x+1][]='';
        //  $finaldata[$x+1][]= Controller::money_format_inr(abs($deficit_grandtotal),2); //abs($deficit_grandtotal);
        //  $finaldata[$x+1][]= Controller::money_format_inr(abs($surplus_grandtotal),2); //abs($surplus_grandtotal);
        //$finaldata[$x+1][]=$paid_total;
        $finaldata[$x + 1][] = '';
        $finaldata[$x + 2][] = 'Fixed Percentage Contracts';
        $finaldata[$x + 2][] = '';
        $finaldata[$x + 2][] = '';
        $finaldata[$x + 2][] = '';
        $finaldata[$x + 2][] = '';
        $finaldata[$x + 2][] = '';
        $finaldata[$x + 2][] = '';
        $finaldata[$x + 2][] = '';
        $finaldata[$x + 2][] = '';
        $finaldata[$x + 2][] = '';
        $finaldata[$x + 2][] = '';
        $finaldata[$x + 2][] = '';
        $finaldata[$x + 2][] = '';

        $finaldata[$x + 3][] = 'Project Name';
        $finaldata[$x + 3][] = 'Company';
        $finaldata[$x + 3][] = 'SQ. FT';
        $finaldata[$x + 3][] = 'Fixed Percentage';
        $finaldata[$x + 3][] = 'Receipt';
        $finaldata[$x + 3][] = 'Expenses';
        $finaldata[$x + 3][] = 'Service Charge';
        $finaldata[$x + 3][] = 'Invoice';
        $finaldata[$x + 3][] = 'Deficit';
        $finaldata[$x + 3][] = 'Surplus';

        $finaldata[$x + 3][] = 'Remarks';
        $tot_data_2 = $this->getTotal($data2);
        $finaldata[$x + 4][] = '';
        $finaldata[$x + 4][] = '';
        $finaldata[$x + 4][] = '';
        $finaldata[$x + 4][] = 'Total';
        $finaldata[$x + 4][] = Controller::money_format_inr($tot_data_2['receipt_tot'], 2);
        $finaldata[$x + 4][] = '';
        $finaldata[$x + 4][] = '';
        $finaldata[$x + 4][] = '';
        $finaldata[$x + 4][] = Controller::money_format_inr(abs($tot_data_2['deficit_total']), 2);
        $finaldata[$x + 4][] = Controller::money_format_inr(abs($tot_data_2['surplus_total']), 2);

        $finaldata[$x + 4][] = '';

        $i = $x + 5;
        $receipt_grandtotal = 0;

        $deficit_grandtotal = 0;

        $surplus_grandtotal = 0;
        $paid_total = 0;
        foreach ($data2 as $datavalue) {

            $companyname = array();
            $user = Users::model()->findByPk(Yii::app()->user->id);
            $comVal = explode(',', $user->company_id);
            $arrVal = explode(',', $datavalue['company_id']);
            foreach ($arrVal as $arr) {
                if (in_array($arr, $comVal)) {
                    $value = Company::model()->findByPk($arr);
                    if($value){
                        array_push($companyname, $value->name);
                    }
                }
            }

            $inv_dtls = Yii::app()->db->createCommand("SELECT SUM(amount) as total_amount,SUM(tax_amount) as tax_amount FROM {$tblpx}invoice WHERE project_id = " . $datavalue['pid'] . "")->queryRow();
            if (($inv_dtls)) {
                if (($inv_dtls['total_amount'] + $inv_dtls['tax_amount']) == 0) {
                    $invoice = 0;
                } else {
                    $invoice = $inv_dtls['total_amount'] + $inv_dtls['tax_amount'];
                }
            } else {
                $invoice = 0;
            }

            $finaldata[$i][] = $datavalue['name'];
            $finaldata[$i][] = implode(', ', $companyname);
            $finaldata[$i][] = $datavalue['sqft'];
            $finaldata[$i][] = $datavalue['percentage'];
            $finaldata[$i][] = Controller::money_format_inr($datavalue['tot_receipt'], 2);
            $finaldata[$i][] = Controller::money_format_inr($datavalue['tot_expense'], 2);

            $percentage = Yii::app()->db->createCommand("SELECT percentage FROM {$tblpx}projects WHERE pid = {$datavalue['pid']}")->queryScalar();
            $servise_charge = ($datavalue['tot_expense'] * $percentage) / 100;

            $finaldata[$i][] = $servise_charge;




            $receipt_grandtotal += $datavalue['tot_receipt'];

            if ($invoice == 0) {

                $diff = ($datavalue['tot_expense'] + $servise_charge) - $datavalue['tot_receipt'];
            } else {

                $diff = $invoice - $datavalue['tot_receipt'];
            }

            $deficit = 0;

            $surplus = 0;

            if ($diff < 0) {

                $surplus = $diff;
            } else {

                $deficit = $diff;
            }

            $deficit_grandtotal += $deficit;
            $surplus_grandtotal += $surplus;
            $finaldata[$i][] = Controller::money_format_inr($invoice, 2);
            $finaldata[$i][] = Controller::money_format_inr(abs($deficit), 2);
            $finaldata[$i][] = Controller::money_format_inr(abs($surplus), 2);
            $finaldata[$i][] = $datavalue['remarks'];
            $i++;
        }

        //  $finaldata[$i][]='';
        //  $finaldata[$i][]='';
        //  $finaldata[$i][]='';
        //  $finaldata[$i][]='';
        //  $finaldata[$i][]= Controller::money_format_inr($receipt_grandtotal,2);
        //  $finaldata[$i][]='';
        //  $finaldata[$i][]='';
        //  $finaldata[$i][]='';
        //  $finaldata[$i][]= Controller::money_format_inr(abs($deficit_grandtotal),2);// abs($deficit_grandtotal);
        //  $finaldata[$i][]= Controller::money_format_inr(abs($surplus_grandtotal),2); //abs($surplus_grandtotal);

        $finaldata[$i][] = '';
        $finaldata[$i + 1][] = '';
        $finaldata[$i + 1][] = '';
        $finaldata[$i + 1][] = '';
        $finaldata[$i + 1][] = '';
        $finaldata[$i + 1][] = '';
        $finaldata[$i + 1][] = '';
        $finaldata[$i + 1][] = '';
        $finaldata[$i + 1][] = '';
        $finaldata[$i + 1][] = '';
        $finaldata[$i + 1][] = '';
        $finaldata[$i + 1][] = '';
        $finaldata[$i + 1][] = '';
        $finaldata[$i + 1][] = '';






        // echo "<pre>";print_r($finaldata);die();
        Yii::import('ext.ECSVExport');

        $csv = new ECSVExport($finaldata);

        $csv->setHeaders($arraylabel);
        $csv->setToAppend();
        $contentdata = $csv->toCSV();
        $csvfilename = 'Financial report' . date('d_M_Y') . '.csv';
        Yii::app()->getRequest()->sendFile($csvfilename, $contentdata, "text/csv", false);
        exit();
    }
    public function actionSavetoExcelCompleted()
    {
        setlocale(LC_MONETARY, 'en_IN');
        //echo "Hello";
        $tblpx = Yii::app()->db->tablePrefix;
        $model = new Projects('financialreport1');

        $model->unsetAttributes();

        $financialdata1 = $model->financialreportcompleted1();

        $fixedprojects = $model->fixedprojectscompleted();

        $completed = $model->completed();

        $data = array();
        $data2 = array();
        $data3 = array();
        $receipt_grandtotal = 0;

        $deficit_grandtotal = 0;

        $surplus_grandtotal = 0;
        $paid_total = 0;
        $x = 0;
        $i = 0;
        $k = 0;

        $arraylabel = array('Fixed Rate Contracts', '', '', '', '', '', '', '', '', '', '', '');


        foreach ($financialdata1->getData() as $record) {

            $data[] = $record;
        }
        foreach ($fixedprojects->getData() as $record) {

            $data2[] = $record;
        }
        foreach ($completed->getData() as $record) {

            $data3[] = $record;
        }


        $finaldata = array();
        $finaldata[0][] = 'Project Name';
        $finaldata[0][] = 'SQ-FT';
        $finaldata[0][] = 'SQ.FT.Rate or Lumpsum';
        $finaldata[0][] = 'Receipt';
        $finaldata[0][] = 'Expenses';
        $finaldata[0][] = 'Invoice';
        $finaldata[0][] = 'Deficit';
        $finaldata[0][] = 'Surplus';
        $finaldata[0][] = 'Remarks';


        foreach ($data as $key => $datavalue) {

            $inv_dtls = Yii::app()->db->createCommand("SELECT SUM(amount) as total_amount,SUM(tax_amount) as tax_amount FROM {$tblpx}invoice WHERE project_id = " . $datavalue['pid'] . "")->queryRow();
            if (($inv_dtls)) {
                if (($inv_dtls['total_amount'] + $inv_dtls['tax_amount']) == 0) {
                    $invoice = 0;
                } else {
                    $invoice = $inv_dtls['total_amount'] + $inv_dtls['tax_amount'];
                }
            } else {
                $invoice = 0;
            }

            $finaldata[$key + 1][] = $datavalue['name'];
            $finaldata[$key + 1][] = $datavalue['sqft'];
            $finaldata[$key + 1][] = $datavalue['sqft_rate'];
            $finaldata[$key + 1][] = Controller::money_format_inr($datavalue['tot_receipt'], 2);
            $finaldata[$key + 1][] = Controller::money_format_inr($datavalue['tot_expense'], 2);

            $percentage = Yii::app()->db->createCommand("SELECT percentage FROM {$tblpx}projects WHERE pid = {$datavalue['pid']}")->queryScalar();
            $servise_charge = ($datavalue['tot_expense'] * $percentage) / 100;
            $finaldata[$key + 1][] = Controller::money_format_inr($invoice, 2);



            $receipt_grandtotal += $datavalue['tot_receipt'];



            if ($invoice == 0) {

                $diff = ($datavalue['tot_expense'] + $servise_charge) - $datavalue['tot_receipt'];
            } else {

                $diff = $invoice - $datavalue['tot_receipt'];
            }

            $deficit = 0;

            $surplus = 0;

            if ($diff < 0) {

                $surplus = $diff;
            } else {

                $deficit = $diff;
            }

            $deficit_grandtotal += $deficit;
            $surplus_grandtotal += $surplus;


            $finaldata[$key + 1][] = Controller::money_format_inr(abs($deficit), 2); //abs($deficit);
            $finaldata[$key + 1][] = Controller::money_format_inr(abs($surplus), 2); //abs($surplus);


            $finaldata[$key + 1][] = $datavalue['remarks'];
            $x = $key + 1;
        }
        //$finaldata[$x+1][]='';
        $finaldata[$x + 1][] = ' ';
        $finaldata[$x + 1][] = ' ';
        $finaldata[$x + 1][] = ' ';
        $finaldata[$x + 1][] = Controller::money_format_inr($receipt_grandtotal, 2); //$receipt_grandtotal;
        $finaldata[$x + 1][] = ' ';
        $finaldata[$x + 1][] = ' ';
        $finaldata[$x + 1][] = Controller::money_format_inr(abs($deficit_grandtotal), 2); //abs($deficit_grandtotal);
        $finaldata[$x + 1][] = Controller::money_format_inr(abs($surplus_grandtotal), 2); //abs($surplus_grandtotal);
        //$finaldata[$x+1][]=$paid_total;
        $finaldata[$x + 1][] = ' ';
        $finaldata[$x + 2][] = 'Fixed Percentage Contracts';
        $finaldata[$x + 2][] = ' ';
        $finaldata[$x + 2][] = ' ';
        $finaldata[$x + 2][] = ' ';
        $finaldata[$x + 2][] = ' ';
        $finaldata[$x + 2][] = ' ';
        $finaldata[$x + 2][] = ' ';
        $finaldata[$x + 2][] = ' ';
        $finaldata[$x + 2][] = ' ';
        $finaldata[$x + 2][] = ' ';
        $finaldata[$x + 2][] = ' ';
        $finaldata[$x + 2][] = ' ';
        $finaldata[$x + 2][] = ' ';

        $finaldata[$x + 3][] = 'Project Name';
        //$finaldata[$x+3][]='Status';
        $finaldata[$x + 3][] = 'SQ. FT';
        $finaldata[$x + 3][] = 'Fixed Percentage';
        $finaldata[$x + 3][] = 'Receipt';
        $finaldata[$x + 3][] = 'Expenses';
        $finaldata[$x + 3][] = 'Service Charge';
        $finaldata[$x + 3][] = 'Invoice';
        $finaldata[$x + 3][] = 'Deficit';
        $finaldata[$x + 3][] = 'Surplus';

        $finaldata[$x + 3][] = 'Remarks';


        $i = $x + 4;
        $receipt_grandtotal = 0;

        $deficit_grandtotal = 0;

        $surplus_grandtotal = 0;
        $paid_total = 0;
        foreach ($data2 as $datavalue) {

            $inv_dtls = Yii::app()->db->createCommand("SELECT SUM(amount) as total_amount,SUM(tax_amount) as tax_amount FROM {$tblpx}invoice WHERE project_id = " . $datavalue['pid'] . "")->queryRow();
            if (($inv_dtls)) {
                if (($inv_dtls['total_amount'] + $inv_dtls['tax_amount']) == 0) {
                    $invoice = 0;
                } else {
                    $invoice = $inv_dtls['total_amount'] + $inv_dtls['tax_amount'];
                }
            } else {
                $invoice = 0;
            }

            $finaldata[$i][] = $datavalue['name'];
            $finaldata[$i][] = $datavalue['sqft'];
            $finaldata[$i][] = $datavalue['percentage'];
            $finaldata[$i][] = Controller::money_format_inr($datavalue['tot_receipt'], 2);
            $finaldata[$i][] = Controller::money_format_inr($datavalue['tot_expense'], 2);

            $percentage = Yii::app()->db->createCommand("SELECT percentage FROM {$tblpx}projects WHERE pid = {$datavalue['pid']}")->queryScalar();
            $servise_charge = ($datavalue['tot_expense'] * $percentage) / 100;

            $finaldata[$i][] = Controller::money_format_inr($servise_charge, 2);




            $receipt_grandtotal += $datavalue['tot_receipt'];

            if ($invoice == 0) {

                $diff = ($datavalue['tot_expense'] + $servise_charge) - $datavalue['tot_receipt'];
            } else {

                $diff = $invoice - $datavalue['tot_receipt'];
            }

            $deficit = 0;

            $surplus = 0;

            if ($diff < 0) {

                $surplus = $diff;
            } else {

                $deficit = $diff;
            }

            $deficit_grandtotal += $deficit;
            $surplus_grandtotal += $surplus;
            $finaldata[$i][] = Controller::money_format_inr($invoice, 2);
            $finaldata[$i][] = Controller::money_format_inr(abs($deficit), 2);
            $finaldata[$i][] = Controller::money_format_inr(abs($surplus), 2);
            $finaldata[$i][] = $datavalue['remarks'];
            $i++;
        }

        $finaldata[$i][] = ' ';
        $finaldata[$i][] = ' ';
        $finaldata[$i][] = ' ';
        $finaldata[$i][] = Controller::money_format_inr($receipt_grandtotal, 2);
        $finaldata[$i][] = ' ';
        $finaldata[$i][] = ' ';
        $finaldata[$i][] = ' ';
        $finaldata[$i][] = Controller::money_format_inr(abs($deficit_grandtotal), 2); // abs($deficit_grandtotal);
        $finaldata[$i][] = Controller::money_format_inr(abs($surplus_grandtotal), 2); //abs($surplus_grandtotal);
        $finaldata[$i][] = ' ';
        $finaldata[$i + 1][] = ' ';
        $finaldata[$i + 1][] = ' ';
        $finaldata[$i + 1][] = ' ';
        $finaldata[$i + 1][] = ' ';
        $finaldata[$i + 1][] = ' ';
        $finaldata[$i + 1][] = ' ';
        $finaldata[$i + 1][] = ' ';
        $finaldata[$i + 1][] = ' ';
        $finaldata[$i + 1][] = ' ';
        $finaldata[$i + 1][] = ' ';
        $finaldata[$i + 1][] = ' ';
        $finaldata[$i + 1][] = ' ';
        $finaldata[$i + 1][] = ' ';






        // echo "<pre>";print_r($finaldata);die();
        Yii::import('ext.ECSVExport');

        $csv = new ECSVExport($finaldata);

        $csv->setHeaders($arraylabel);
        $csv->setToAppend();
        $contentdata = $csv->toCSV();
        $csvfilename = 'Financial report' . date('d_M_Y') . '.csv';
        Yii::app()->getRequest()->sendFile($csvfilename, $contentdata, "text/csv", false);
        exit();
    }

    public function actionSavetoExcelStatus()
    {

        $tblpx = Yii::app()->db->tablePrefix;
        $project_id = $_POST['project_id'];

        $fromdate = $_POST['fromdate'];

        $todate = $_POST['todate'];

        $pastweek = $_POST['pastweek'];

        $nextweek = $_POST['nextweek'];

        $issues = $_POST['issues'];

        $company_id = $_POST['company_id'];

        $query = '';
        if (isset($_POST['company_id']) && !empty($_POST['company_id'])) {
            $query .= '' . $tblpx . 'dailyreport.company_id = ' . $company_id . '';
        } else {
            $user = Users::model()->findByPk(Yii::app()->user->id);
            $arrVal = explode(',', $user->company_id);
            $query = "";
            foreach ($arrVal as $arr) {
                if ($query) $query .= ' OR';
                $query .= " FIND_IN_SET('" . $arr . "', " . $tblpx . "dailyreport.company_id)";
            }
        }

        $work_desc_count  = $_POST['work_desc_count'];
        $extra_work_desc_count  = $_POST['extra_work_desc_count'];
        $work_descriptions = $extra_work_descriptions = array();
        if ($work_desc_count > 0) {
            for ($i = 1; $i < $work_desc_count; $i++) {
                $work_descriptions[$i] = $_POST['work_description_' . $i];
            }
        }
        if ($extra_work_desc_count > 0) {
            for ($j = 1; $j < $extra_work_desc_count; $j++) {
                $extra_work_descriptions[$j] = $_POST['extra_work_description_' . $j];
            }
        }

        $date_from = date('Y-m-d', strtotime($fromdate));

        $date_to = date('Y-m-d', strtotime($todate));

        $data = array();

        $finaldata = '';
        $sql = Yii::app()->db->createCommand("select " . $tblpx . "clients.name as client_name , " . $tblpx . "projects.name, " . $tblpx . "projects.pid," . $tblpx . "projects.created_date," . $tblpx . "projects.site from " . $tblpx . "projects inner join " . $tblpx . "clients on " . $tblpx . "clients.cid = " . $tblpx . "projects.client_id  inner join " . $tblpx . "dailyreport ON " . $tblpx . "dailyreport.projectid = " . $tblpx . "projects.pid WHERE pid=" . $project_id . " AND " . $tblpx . "dailyreport.book_date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND (" . $query . ") ORDER BY book_date ASC")->queryRow();

        $arraylabel = array("Project:" . $sql['name'], '', '', '', '');
        //$sqlexp = Yii::app()->db->createCommand("select GROUP_CONCAT( works_done SEPARATOR '<br/>') as works_done,GROUP_CONCAT( materials_unlade SEPARATOR '<br/>') as materials,book_date, GROUP_CONCAT( book_description SEPARATOR '<br/>') as book_description from " . $tblpx . "dailyreport WHERE projectid=" . $project_id . " AND " . $tblpx . "dailyreport.book_date BETWEEN '" . date('Y-m-d', strtotime($date_from)) . "' AND '" . date('Y-m-d', strtotime($date_to)) . "' GROUP BY " . $tblpx . "dailyreport.book_date")->queryAll();
        $sqlexp = Yii::app()->db->createCommand("select * from " . $tblpx . "dailyreport WHERE projectid=" . $project_id . " AND " . $tblpx . "dailyreport.book_date BETWEEN '" . date('Y-m-d', strtotime($date_from)) . "' AND '" . date('Y-m-d', strtotime($date_to)) . "' AND (" . $query . ") GROUP BY " . $tblpx . "dailyreport.book_date")->queryAll();

        if (!empty($sqlexp)) {

            $finaldata[0][] = 'Site:' . $sql['site'];
            $finaldata[1][] = 'Status Report' . strftime(" %d/%m ", strtotime($date_from)) . '----' . strftime(" %d/%m/%y ", strtotime($date_to));
            $finaldata[2][] = 'PROLOGUE:';
            $finaldata[3][] = 'PROJECT START DATE: ' . strftime(" %dth %B '%y, %A", strtotime($sql['created_date']));
            $finaldata[4][] = 'SCHEDULE';
            $finaldata[5][] = 'Works Done';
            $finaldata[6][] = 'Date';
            $finaldata[6][] = 'Work Type';
            $finaldata[6][] = 'Numbers';
            $finaldata[6][] = 'Description';
            $x = 6;
            $j = 1;
            foreach ($sqlexp as $key => $row) {
                if (!empty($row['wrktype_and_numbers'])) {
                    $obj = json_decode($row['wrktype_and_numbers']);
                    foreach ($obj as $value) {
                        $list = Yii::app()->db->createCommand("SELECT DISTINCT work_type FROM {$tblpx}work_type  WHERE wtid = '" . $value->{'work_type'} . "'")->queryRow();
                        if ($value->{'work_done'} == 0) {

                            $finaldata[$x + 1][] = date('d-M-Y', strtotime($row['book_date']));

                            $finaldata[$x + 1][] = $list['work_type'];
                            $finaldata[$x + 1][] = $value->{'numbers'};
                            //$finaldata[$x+1][]= $row['works_done'];
                            $finaldata[$x + 1][] = (isset($work_descriptions[$j])) ? $work_descriptions[$j] : (isset($value->{'works_done'}) ? $value->{'works_done'} : $row['works_done']);
                            $x++;
                            $j++;
                        }
                    }
                }



                //$x=$key+8;


            }
            $finaldata[$x + 1][] = 'Extra Works Done';
            $finaldata[$x + 2][] = 'Date';
            $finaldata[$x + 2][] = 'Work Type';
            $finaldata[$x + 2][] = 'Numbers';
            $finaldata[$x + 2][] = 'Description';
            $i = $x + 3;
            $k = 1;
            foreach ($sqlexp as $key => $row) {
                if (!empty($row['wrktype_and_numbers'])) {
                    $obj = json_decode($row['wrktype_and_numbers']);
                    foreach ($obj as $value) {
                        $list = Yii::app()->db->createCommand("SELECT DISTINCT work_type FROM {$tblpx}work_type  WHERE wtid = '" . $value->{'work_type'} . "'")->queryRow();
                        if ($value->{'work_done'} == 1) {

                            $finaldata[$i][] = date('d-M-Y', strtotime($row['book_date']));

                            $finaldata[$i][] = $list['work_type'];
                            $finaldata[$i][] = $value->{'numbers'};
                            //$finaldata[$i][]= $row['extra_work_done'];
                            $finaldata[$i][] = (isset($extra_work_descriptions[$k])) ? $extra_work_descriptions[$k] : (isset($value->{'works_done'}) ? $value->{'works_done'} : $row['extra_work_done']);
                            $i++;
                            $k++;
                        }
                    }
                }



                // $x=$i+1;


            }

            $finaldata[$i][0] = ' ';

            if ($pastweek != "" && $nextweek != "" && $issues != "") {
                $finaldata[$i + 1][0] = 'PAST WEEK:';
                $finaldata[$i + 2][0] = $pastweek;

                $finaldata[$i + 3][0] = 'FORECAST NEXT WEEK:';
                $finaldata[$i + 4][0] = $nextweek;

                $finaldata[$i + 5][0] = 'OPEN ISSUES:';
                $finaldata[$i + 6][0] = $issues;
            } elseif ($pastweek != "" && $nextweek != "" && $issues == "") {
                $finaldata[$i + 1][0] = 'PAST WEEK:';
                $finaldata[$i + 2][0] = $pastweek;

                $finaldata[$i + 3][0] = 'FORECAST NEXT WEEK:';
                $finaldata[$i + 4][0] = $nextweek;
            } elseif ($pastweek != "" && $nextweek == "" && $issues != "") {
                $finaldata[$i + 1][0] = 'PAST WEEK:';
                $finaldata[$i + 2][0] = $pastweek;

                $finaldata[$i + 3][0] = 'OPEN ISSUES:';
                $finaldata[$i + 4][0] = $issues;
            } elseif ($pastweek == "" && $nextweek != "" && $issues != "") {

                $finaldata[$i + 1][0] = 'FORECAST NEXT WEEK:';
                $finaldata[$i + 2][0] = $nextweek;

                $finaldata[$i + 3][0] = 'OPEN ISSUES:';
                $finaldata[$i + 4][0] = $issues;
            }
        }




        Yii::import('ext.ECSVExport');

        $csv = new ECSVExport($finaldata);

        $csv->setHeaders($arraylabel);
        $csv->setToAppend();
        $contentdata = $csv->toCSV();
        $csvfilename = 'Status Report' . date('d_M_Y') . '.csv';
        Yii::app()->getRequest()->sendFile($csvfilename, $contentdata, "text/csv", false);
        exit();
    }


    public function actionSavetopdfreport()
    {
        $this->logo = $this->realpath_logo;
        $model = new Projects;
        $model->unsetAttributes();
        if (isset($_REQUEST['company_id']) && !empty($_REQUEST['company_id'])) {
            $model->companyId = $_REQUEST['company_id'];
        }
        if (isset($_REQUEST['fromdate']) && !empty($_REQUEST['fromdate'])) {
            $model->fromdate = $_REQUEST['fromdate'];
        }
        if (isset($_REQUEST['todate']) && !empty($_REQUEST['todate'])) {
            $model->todate = $_REQUEST['todate'];
        }
        if (isset($_REQUEST['project_status']) && !empty($_REQUEST['project_status'])) {
            $model->project_status = $_REQUEST['project_status'];
        }

        $mPDF1 = Yii::app()->ePdf->mPDF();
        $mPDF1 = Yii::app()->ePdf->mPDF('', 'A4');
        $sql = 'SELECT template_name '
                . 'FROM jp_quotation_template '
                . 'WHERE status="1"';
        $selectedtemplate =Yii::app()->db->createCommand($sql)->queryScalar();
        
        $template = $this->getTemplate($selectedtemplate);
        $mPDF1->SetHTMLHeader($template['header']);
        $mPDF1->SetHTMLFooter($template['footer']);
        $mPDF1->AddPage('','', '', '', '', 0,0,40, 30, 10,0);
        $mPDF1->WriteHTML($this->renderPartial('savetopdfreport', array(
            'model' => $model,
        ), true));
        $filename = "Project Report" . $duration;
        $mPDF1->Output($filename . '.pdf', 'D');
    }

    public function actionSavetoexcelreport()
    {
        /* $companyArray = explode(",",Yii::app()->user->company_ids);
                $condition  = "";
                foreach($companyArray as $company) {
                    $condition .= "FIND_IN_SET('".$company."',company_id) OR ";
                }
                $condition = substr($condition, 0, -3); */
        $finaldata = array();
        $arraylabel = array("Sl. NO", "Project Name", "Start Date", "Created Date");
        // $projects = Projects::model()->findAll(array("condition" => "".$condition.""));
        $model = new Projects;
        $model->unsetAttributes();
        if (isset($_REQUEST['company_id']) && !empty($_REQUEST['company_id'])) {
            $model->companyId = $_REQUEST['company_id'];
        }
        if (isset($_REQUEST['fromdate']) && !empty($_REQUEST['fromdate'])) {
            $model->fromdate = $_REQUEST['fromdate'];
        }
        if (isset($_REQUEST['todate']) && !empty($_REQUEST['todate'])) {
            $model->todate = $_REQUEST['todate'];
        }
        if (isset($_REQUEST['project_status']) && !empty($_REQUEST['project_status'])) {
            $model->project_status = $_REQUEST['project_status'];
        }
        $key = 0;
        foreach ($model->search()->getData() as $value) {
            $finaldata[$key][] = $key + 1;
            $finaldata[$key][] = $value->name;
            $finaldata[$key][] = date("d-m-Y", strtotime($value->start_date));
            $finaldata[$key][] = date("d-m-Y", strtotime($value->created_date));
            $key++;
        }
        Yii::import('ext.ECSVExport');

        $csv = new ECSVExport($finaldata);
        $csv->setHeaders($arraylabel);
        $csv->setToAppend();
        $contentdata = $csv->toCSV();
        $csvfilename = 'Project Report' . date('d_M_Y') . '.csv';
        Yii::app()->getRequest()->sendFile($csvfilename, $contentdata, "text/csv", false);
        exit();
    }
    public function actionProjectexpensereport()
    {
        $tblpx          = Yii::app()->db->tablePrefix;
        $project_status = '';
        if (isset($_POST['project_id']) && !empty($_POST['project_id'])) {
            if (isset($_POST["date_from"]) && !empty($_POST["date_from"]) && isset($_POST["date_to"]) && !empty($_POST["date_to"])) {
                $date_from  = date("Y-m-d", strtotime($_POST["date_from"]));
                $date_to    = date("Y-m-d", strtotime($_POST["date_to"]));
                $condition1 = " AND (ex.date BETWEEN '" . $date_from . "' AND '" . $date_to . "')";
                $condition2 = " AND (date BETWEEN '" . $date_from . "' AND '" . $date_to . "')";
                $condition3 = " AND (bl.bill_date BETWEEN '" . $date_from . "' AND '" . $date_to . "')";
                $condition4 = " AND (inv.date BETWEEN '" . $date_from . "' AND '" . $date_to . "')";
                $condition5 = " AND (dv.date BETWEEN '" . $date_from . "' AND '" . $date_to . "')";
                $condition6 = " AND (sq.scquotation_date BETWEEN '" . $date_from . "' AND '" . $date_to . "')";
                $condition7 = " AND (sp.date BETWEEN '" . $date_from . "' AND '" . $date_to . "')";
                $condition8 = " AND (pu.purchase_date BETWEEN '" . $date_from . "' AND '" . $date_to . "')";
            } else if (isset($_POST["date_from"]) && empty($_POST["date_from"]) && isset($_POST["date_to"]) && !empty($_POST["date_to"])) {
                $date_from  = "";
                $date_to    = date("Y-m-d", strtotime($_POST["date_to"]));;
                $condition1 = " AND ex.date <= '" . $date_to . "'";
                $condition2 = " AND date <= '" . $date_to . "'";
                $condition3 = " AND bl.bill_date <= '" . $date_to . "'";
                $condition4 = " AND inv.date <= '" . $date_to . "'";
                $condition5 = " AND dv.date <= '" . $date_to . "'";
                $condition6 = " AND sq.scquotation_date <= '" . $date_to . "'";
                $condition7 = " AND sp.date <= '" . $date_to . "'";
                $condition8 = " AND pu.purchase_date <= '" . $date_to . "'";
            } else if (isset($_POST["date_from"]) && !empty($_POST["date_from"]) && isset($_POST["date_to"]) && empty($_POST["date_to"])) {
                $date_from  = date("Y-m-d", strtotime($_POST["date_from"]));
                $date_to    = "";
                $condition1 = " AND ex.date >= '" . $date_from . "'";
                $condition2 = " AND date >= '" . $date_from . "'";
                $condition3 = " AND bl.bill_date >= '" . $date_from . "'";
                $condition4 = " AND inv.date >= '" . $date_from . "'";
                $condition5 = " AND dv.date >= '" . $date_from . "'";
                $condition6 = " AND sq.scquotation_date >= '" . $date_from . "'";
                $condition7 = " AND sp.date >= '" . $date_from . "'";
                $condition8 = " AND pu.purchase_date >= '" . $date_from . "'";
            } else {
                $condition1 = "";
                $condition2 = "";
                $condition3 = "";
                $condition4 = "";
                $condition5 = "";
                $condition6 = "";
                $condition7 = "";
                $condition8 = "";
                $date_from  = "";
                $date_to    = "";
            }

            $project_id     = $_POST['project_id'];
            $projectdata    = Yii::app()->db->createCommand("SELECT pr.*, cl.name as client, st.caption from {$tblpx}projects pr LEFT JOIN {$tblpx}clients cl ON pr.client_id = cl.cid LEFT JOIN {$tblpx}status st ON pr.project_type = st.sid WHERE pr.pid = {$project_id}")->queryRow();
            $billdata       = Yii::app()->db->createCommand("SELECT bl.*, pu.purchase_no, a.billsum,ve.name as vendorname FROM {$tblpx}bills bl LEFT JOIN {$tblpx}purchase pu ON bl.purchase_id = pu.p_id LEFT JOIN (SELECT SUM(bl.bill_totalamount) as billsum FROM {$tblpx}bills bl LEFT JOIN {$tblpx}purchase pu ON bl.purchase_id = pu.p_id WHERE pu.project_id = {$project_id}" . $condition3 . ") a ON 1 = 1 LEFT JOIN {$tblpx}vendors ve ON pu.vendor_id = ve.vendor_id WHERE pu.project_id = {$project_id}" . $condition3 . "")->queryAll();
            $subconbill     = Yii::app()->db->createCommand("SELECT sp.*,sq.scquotation_decription,sq.subcontractor_id FROM {$tblpx}subcontractorbill sp LEFT JOIN {$tblpx}scquotation sq ON sp.scquotation_id = sq.scquotation_id WHERE sq.project_id = {$project_id}" . $condition7)->queryAll();
            $unbilledpo     = Yii::app()->db->createCommand("SELECT pu.*,ve.name as vname,et.type_name as expensehead FROM {$tblpx}purchase pu LEFT JOIN {$tblpx}expense_type et ON pu.expensehead_id = et.type_id LEFT JOIN {$tblpx}vendors ve ON pu.vendor_id = ve.vendor_id WHERE pu.project_id = {$project_id} AND pu.unbilled_amount > 0" . $condition8)->queryAll();

            $quotationdata  = Yii::app()->db->createCommand("SELECT sq.*,sc.*,a.quotationsum FROM {$tblpx}scquotation sq LEFT JOIN {$tblpx}subcontractor sc ON sq.subcontractor_id = sc.subcontractor_id LEFT JOIN (SELECT sq.subcontractor_id, SUM(sq.scquotation_amount) as quotationsum FROM {$tblpx}scquotation sq LEFT JOIN {$tblpx}subcontractor sc ON sq.subcontractor_id = sc.subcontractor_id WHERE sq.project_id = {$project_id}" . $condition6 . ") a ON 1 = 1 WHERE sq.project_id = {$project_id}" . $condition6 . "")->queryAll();
            $invoicedata    = Yii::app()->db->createCommand("SELECT inv.*, a.invoicetotal FROM {$tblpx}invoice inv LEFT JOIN (SELECT SUM(tax_amount + subtotal) as invoicetotal FROM {$tblpx}invoice WHERE project_id = {$project_id}" . $condition2 . ") a ON 1 = 1 WHERE project_id = {$project_id}" . $condition4 . "")->queryAll();
            $daybookreceipt = Yii::app()->db->createCommand("SELECT ex.*,a.receiptsum FROM {$tblpx}expenses ex LEFT JOIN (SELECT SUM(receipt) as receiptsum FROM {$tblpx}expenses WHERE type = 72 AND projectid = {$project_id}" . $condition2 . ") a ON 1 = 1 WHERE ex.type = 72 AND ex.projectid = {$project_id}" . $condition1 . "")->queryAll();
            $daybookvendor  = Yii::app()->db->createCommand("SELECT ex.*, v.name as vendor, a.vendorcount, b.vendorpaid, c.totalvendorpaid, ext.type_name as expensetype,d.vendorsubsum FROM {$tblpx}expenses ex
                                    LEFT JOIN {$tblpx}vendors v ON v.vendor_id = ex.vendor_id
                                    LEFT JOIN {$tblpx}expense_type ext ON ex.exptype = ext.type_id
                                    LEFT JOIN (SELECT COUNT(*) as vendorcount,vendor_id FROM {$tblpx}expenses WHERE projectid = {$project_id} AND type = 73 AND vendor_id IS NOT NULL" . $condition2 . " GROUP BY vendor_id) a ON a.vendor_id = ex.vendor_id
                                    LEFT JOIN (SELECT ex.vendor_id,SUM(ex.paid) as vendorpaid FROM {$tblpx}expenses ex WHERE ex.projectid = {$project_id} AND ex.type = 73 AND ex.vendor_id IS NOT NULL" . $condition1 . " GROUP BY ex.vendor_id ORDER BY ex.vendor_id) b ON b.vendor_id = ex.vendor_id
                                    LEFT JOIN (SELECT ex.vendor_id,SUM(ex.paid) as totalvendorpaid FROM {$tblpx}expenses ex WHERE ex.projectid = {$project_id} AND ex.type = 73 AND ex.vendor_id IS NOT NULL" . $condition1 . " ORDER BY ex.vendor_id) c ON 1 = 1
                                    LEFT JOIN (SELECT pu.vendor_id as vendorid, SUM(IFNULL(bl.bill_totalamount,0)) as vendorsubsum FROM {$tblpx}bills bl LEFT JOIN {$tblpx}purchase pu ON bl.purchase_id = pu.p_id WHERE pu.project_id = {$project_id}" . $condition3 . " GROUP BY pu.vendor_id) d ON d.vendorid = ex.vendor_id
                                    WHERE ex.projectid = {$project_id} AND ex.type = 73 AND ex.vendor_id IS NOT NULL" . $condition1 . " ORDER BY v.name")->queryAll();
            $daybooksubcon  = Yii::app()->db->createCommand("SELECT ex.*, a.subcontractorcount, b.subcontractorpaid, c.subcontractortotalpaid, d.sname, d.subconid FROM {$tblpx}expenses ex
                                    LEFT JOIN {$tblpx}subcontractor_payment sp ON sp.payment_id = ex.subcontractor_id
                                    LEFT JOIN {$tblpx}subcontractor sc ON sp.subcontractor_id = sc.subcontractor_id
                                    LEFT JOIN (SELECT COUNT(*) as subcontractorcount,sp.payment_id,sc.subcontractor_id FROM {$tblpx}expenses ex LEFT JOIN {$tblpx}subcontractor_payment sp ON ex.subcontractor_id = sp.payment_id LEFT JOIN {$tblpx}subcontractor sc ON sp.subcontractor_id = sc.subcontractor_id WHERE ex.projectid = {$project_id} AND ex.type = 73 AND ex.subcontractor_id IS NOT NULL" . $condition1 . " GROUP BY sc.subcontractor_id) a ON a.subcontractor_id = sc.subcontractor_id
                                    LEFT JOIN (SELECT SUM(ex.paid) as subcontractorpaid,sp.payment_id,sc.subcontractor_id FROM {$tblpx}expenses ex LEFT JOIN {$tblpx}subcontractor_payment sp ON ex.subcontractor_id = sp.payment_id LEFT JOIN {$tblpx}subcontractor sc ON sp.subcontractor_id = sc.subcontractor_id WHERE ex.projectid = {$project_id} AND ex.type = 73 AND ex.subcontractor_id IS NOT NULL" . $condition1 . " GROUP BY sc.subcontractor_id) b ON b.subcontractor_id = sc.subcontractor_id
                                    LEFT JOIN (SELECT subcontractor_id, SUM(paid) as subcontractortotalpaid FROM {$tblpx}expenses WHERE projectid = {$project_id} AND type = 73 AND subcontractor_id IS NOT NULL" . $condition2 . ") c ON 1 = 1
                                    LEFT JOIN (SELECT sp.payment_id,sc.subcontractor_id as subconid,sc.subcontractor_name as sname FROM {$tblpx}subcontractor sc LEFT JOIN {$tblpx}subcontractor_payment sp ON sc.subcontractor_id = sp.subcontractor_id WHERE 1 = 1" . $condition7 . ") d ON d.payment_id = ex.subcontractor_id
                                    WHERE ex.projectid = {$project_id} AND ex.type = 73 AND ex.subcontractor_id IS NOT NULL" . $condition1 . " ORDER BY sc.subcontractor_name")->queryAll();
            $vendorpayment  = Yii::app()->db->createCommand("SELECT dv.*, v.name as vendor, a.vendorcount, b.vendorsum, c.vendortotalsum FROM {$tblpx}dailyvendors dv
                                    LEFT JOIN {$tblpx}vendors v ON v.vendor_id = dv.vendor_id
                                    LEFT JOIN (SELECT vendor_id, COUNT(*) as vendorcount FROM {$tblpx}dailyvendors WHERE project_id = {$project_id}" . $condition2 . " GROUP BY vendor_id ORDER BY vendor_id) a ON a.vendor_id = dv.vendor_id
                                    LEFT JOIN (SELECT vendor_id, SUM(tax_amount + amount) as vendorsum FROM {$tblpx}dailyvendors WHERE project_id = {$project_id}" . $condition2 . " GROUP BY vendor_id ORDER BY vendor_id) b ON b.vendor_id = dv.vendor_id
                                    LEFT JOIN (SELECT vendor_id, SUM(tax_amount + amount) as vendortotalsum FROM {$tblpx}dailyvendors WHERE project_id = {$project_id}" . $condition2 . " ORDER BY vendor_id) c ON 1 = 1
                                    WHERE dv.project_id = {$project_id}" . $condition5 . " ORDER BY v.name")->queryAll();
            $model          = array();
        } else {
            $model          = new Projects;
            $model->unsetAttributes();
            if (isset($_REQUEST['date_from']) && !empty($_REQUEST['date_from'])) {
                $model->fromdate = $_REQUEST['date_from'];
                $date_from = $_REQUEST['date_from'];
            } else {
                $date_from = "";
            }
            if (isset($_REQUEST['date_to']) && !empty($_REQUEST['date_to'])) {
                $model->todate = $_REQUEST['date_to'];
                $date_to = $_REQUEST['date_to'];
            } else {
                $date_to = "";
            }
            if (isset($_REQUEST['project_status']) && !empty($_REQUEST['project_status'])) {
                $model->project_status  = $_REQUEST['project_status'];
                $project_status         = $_REQUEST['project_status'];
            }
            $project_id     = 0;
            $projectdata    = "";
            $billdata       = "";
            $subconbill     = "";
            $unbilledpo     = "";
            $quotationdata  = "";
            $invoicedata    = "";
            $daybookreceipt = "";
            $daybookvendor  = "";
            $daybooksubcon  = "";
            $vendorpayment  = "";
        }

        $this->render('expensereport', array(
            'model' => $model,
            'project_id' => $project_id,
            'projectdata' => $projectdata,
            'date_from' => $date_from,
            'date_to' => $date_to,
            'billdata' => $billdata,
            'subconbill' => $subconbill,
            'unbilledpo' => $unbilledpo,
            'quotationdata' => $quotationdata,
            'invoicedata' => $invoicedata,
            'daybookreceipt' => $daybookreceipt,
            'daybookvendor' => $daybookvendor,
            'daybooksubcon' => $daybooksubcon,
            'vendorpayment' => $vendorpayment,
            'project_status' => $project_status,
        ));
    }
    public function actionSaveexpensereport()
    {
        $this->logo = $this->realpath_logo;
        $tblpx      = Yii::app()->db->tablePrefix;
        $project_id = $_REQUEST['project_id'];
        if (isset($_REQUEST["date_from"]) && !empty($_REQUEST["date_from"]) && isset($_REQUEST["date_to"]) && !empty($_REQUEST["date_to"])) {
            $date_from  = date("Y-m-d", strtotime($_REQUEST["date_from"]));
            $date_to    = date("Y-m-d", strtotime($_REQUEST["date_to"]));
            $condition1 = " AND (ex.date BETWEEN '" . $date_from . "' AND '" . $date_to . "')";
            $condition2 = " AND (date BETWEEN '" . $date_from . "' AND '" . $date_to . "')";
            $condition3 = " AND (bl.bill_date BETWEEN '" . $date_from . "' AND '" . $date_to . "')";
            $condition4 = " AND (inv.date BETWEEN '" . $date_from . "' AND '" . $date_to . "')";
            $condition5 = " AND (dv.date BETWEEN '" . $date_from . "' AND '" . $date_to . "')";
            $condition6 = " AND (sq.scquotation_date BETWEEN '" . $date_from . "' AND '" . $date_to . "')";
            $condition7 = " AND (sp.date BETWEEN '" . $date_from . "' AND '" . $date_to . "')";
            $condition8 = " AND (pu.purchase_date BETWEEN '" . $date_from . "' AND '" . $date_to . "')";
        } else if (isset($_REQUEST["date_from"]) && empty($_REQUEST["date_from"]) && isset($_REQUEST["date_to"]) && !empty($_REQUEST["date_to"])) {
            $date_from  = "";
            $date_to    = date("Y-m-d", strtotime($_POST["date_to"]));;
            $condition1 = " AND ex.date <= '" . $date_to . "'";
            $condition2 = " AND date <= '" . $date_to . "'";
            $condition3 = " AND bl.bill_date <= '" . $date_to . "'";
            $condition4 = " AND inv.date <= '" . $date_to . "'";
            $condition5 = " AND dv.date <= '" . $date_to . "'";
            $condition6 = " AND sq.scquotation_date <= '" . $date_to . "'";
            $condition7 = " AND sp.date <= '" . $date_to . "'";
            $condition8 = " AND pu.purchase_date <= '" . $date_to . "'";
        } else if (isset($_REQUEST["date_from"]) && !empty($_REQUEST["date_from"]) && isset($_REQUEST["date_to"]) && empty($_REQUEST["date_to"])) {
            $date_from  = date("Y-m-d", strtotime($_POST["date_from"]));
            $date_to    = "";
            $condition1 = " AND ex.date >= '" . $date_from . "'";
            $condition2 = " AND date >= '" . $date_from . "'";
            $condition3 = " AND bl.bill_date >= '" . $date_from . "'";
            $condition4 = " AND inv.date >= '" . $date_from . "'";
            $condition5 = " AND dv.date >= '" . $date_from . "'";
            $condition6 = " AND sq.scquotation_date >= '" . $date_from . "'";
            $condition7 = " AND sp.date >= '" . $date_from . "'";
            $condition8 = " AND pu.purchase_date >= '" . $date_from . "'";
        } else {
            $condition1 = "";
            $condition2 = "";
            $condition3 = "";
            $condition4 = "";
            $condition5 = "";
            $condition6 = "";
            $condition7 = "";
            $condition8 = "";
            $date_from  = "";
            $date_to    = "";
        }
        $projectdata    = Yii::app()->db->createCommand("SELECT pr.*, cl.name as client, st.caption from {$tblpx}projects pr LEFT JOIN {$tblpx}clients cl ON pr.client_id = cl.cid LEFT JOIN {$tblpx}status st ON pr.project_type = st.sid WHERE pr.pid = {$project_id}")->queryRow();
        $billdata       = Yii::app()->db->createCommand("SELECT bl.*, pu.purchase_no, a.billsum,ve.name as vendorname FROM {$tblpx}bills bl LEFT JOIN {$tblpx}purchase pu ON bl.purchase_id = pu.p_id LEFT JOIN (SELECT SUM(bl.bill_totalamount) as billsum FROM {$tblpx}bills bl LEFT JOIN {$tblpx}purchase pu ON bl.purchase_id = pu.p_id WHERE pu.project_id = {$project_id}" . $condition3 . ") a ON 1 = 1 LEFT JOIN {$tblpx}vendors ve ON pu.vendor_id = ve.vendor_id WHERE pu.project_id = {$project_id}" . $condition3 . "")->queryAll();
        $subconbill     = Yii::app()->db->createCommand("SELECT sp.*,sq.scquotation_decription,sq.subcontractor_id FROM {$tblpx}subcontractorbill sp LEFT JOIN {$tblpx}scquotation sq ON sp.scquotation_id = sq.scquotation_id WHERE sq.project_id = {$project_id}" . $condition7)->queryAll();
        $unbilledpo     = Yii::app()->db->createCommand("SELECT pu.*,ve.name as vname,et.type_name as expensehead FROM {$tblpx}purchase pu LEFT JOIN {$tblpx}expense_type et ON pu.expensehead_id = et.type_id LEFT JOIN {$tblpx}vendors ve ON pu.vendor_id = ve.vendor_id WHERE pu.project_id = {$project_id} AND pu.unbilled_amount > 0" . $condition8)->queryAll();
        $quotationdata  = Yii::app()->db->createCommand("SELECT sq.*,sc.*,a.quotationsum FROM {$tblpx}scquotation sq LEFT JOIN {$tblpx}subcontractor sc ON sq.subcontractor_id = sc.subcontractor_id LEFT JOIN (SELECT sq.subcontractor_id, SUM(sq.scquotation_amount) as quotationsum FROM {$tblpx}scquotation sq LEFT JOIN {$tblpx}subcontractor sc ON sq.subcontractor_id = sc.subcontractor_id WHERE sq.project_id = {$project_id}" . $condition6 . ") a ON 1 = 1 WHERE sq.project_id = {$project_id}" . $condition6 . "")->queryAll();
        $invoicedata    = Yii::app()->db->createCommand("SELECT inv.*, a.invoicetotal FROM {$tblpx}invoice inv LEFT JOIN (SELECT SUM(tax_amount + subtotal) as invoicetotal FROM {$tblpx}invoice WHERE project_id = {$project_id}" . $condition2 . ") a ON 1 = 1 WHERE project_id = {$project_id}" . $condition4 . "")->queryAll();
        $daybookreceipt = Yii::app()->db->createCommand("SELECT ex.*,a.receiptsum FROM {$tblpx}expenses ex LEFT JOIN (SELECT SUM(receipt) as receiptsum FROM {$tblpx}expenses WHERE type = 72 AND projectid = {$project_id}" . $condition2 . ") a ON 1 = 1 WHERE ex.type = 72 AND ex.projectid = {$project_id}" . $condition1 . "")->queryAll();
        $daybookvendor  = Yii::app()->db->createCommand("SELECT ex.*, v.name as vendor, a.vendorcount, b.vendorpaid, c.totalvendorpaid, ext.type_name as expensetype,d.vendorsubsum FROM {$tblpx}expenses ex
                                LEFT JOIN {$tblpx}vendors v ON v.vendor_id = ex.vendor_id
                                LEFT JOIN {$tblpx}expense_type ext ON ex.exptype = ext.type_id
                                LEFT JOIN (SELECT COUNT(*) as vendorcount,vendor_id FROM {$tblpx}expenses WHERE projectid = {$project_id} AND type = 73 AND vendor_id IS NOT NULL" . $condition2 . " GROUP BY vendor_id) a ON a.vendor_id = ex.vendor_id
                                LEFT JOIN (SELECT ex.vendor_id,SUM(ex.paid) as vendorpaid FROM {$tblpx}expenses ex WHERE ex.projectid = {$project_id} AND ex.type = 73 AND ex.vendor_id IS NOT NULL" . $condition1 . " GROUP BY ex.vendor_id ORDER BY ex.vendor_id) b ON b.vendor_id = ex.vendor_id
                                LEFT JOIN (SELECT ex.vendor_id,SUM(ex.paid) as totalvendorpaid FROM {$tblpx}expenses ex WHERE ex.projectid = {$project_id} AND ex.type = 73 AND ex.vendor_id IS NOT NULL" . $condition1 . " ORDER BY ex.vendor_id) c ON 1 = 1
                                LEFT JOIN (SELECT pu.vendor_id as vendorid, SUM(IFNULL(bl.bill_totalamount,0)) as vendorsubsum FROM {$tblpx}bills bl LEFT JOIN {$tblpx}purchase pu ON bl.purchase_id = pu.p_id WHERE pu.project_id = {$project_id}" . $condition3 . " GROUP BY pu.vendor_id) d ON d.vendorid = ex.vendor_id
                                WHERE ex.projectid = {$project_id} AND ex.type = 73 AND ex.vendor_id IS NOT NULL" . $condition1 . " ORDER BY v.name")->queryAll();
        $daybooksubcon  = Yii::app()->db->createCommand("SELECT ex.*, a.subcontractorcount, b.subcontractorpaid, c.subcontractortotalpaid, d.sname, d.subconid FROM {$tblpx}expenses ex
                                LEFT JOIN {$tblpx}subcontractor_payment sp ON sp.payment_id = ex.subcontractor_id
                                LEFT JOIN {$tblpx}subcontractor sc ON sp.subcontractor_id = sc.subcontractor_id
                                LEFT JOIN (SELECT COUNT(*) as subcontractorcount,sp.payment_id,sc.subcontractor_id FROM {$tblpx}expenses ex LEFT JOIN {$tblpx}subcontractor_payment sp ON ex.subcontractor_id = sp.payment_id LEFT JOIN {$tblpx}subcontractor sc ON sp.subcontractor_id = sc.subcontractor_id WHERE ex.projectid = {$project_id} AND ex.type = 73 AND ex.subcontractor_id IS NOT NULL" . $condition1 . " GROUP BY sc.subcontractor_id) a ON a.subcontractor_id = sc.subcontractor_id
                                LEFT JOIN (SELECT SUM(ex.paid) as subcontractorpaid,sp.payment_id,sc.subcontractor_id FROM {$tblpx}expenses ex LEFT JOIN {$tblpx}subcontractor_payment sp ON ex.subcontractor_id = sp.payment_id LEFT JOIN {$tblpx}subcontractor sc ON sp.subcontractor_id = sc.subcontractor_id WHERE ex.projectid = {$project_id} AND ex.type = 73 AND ex.subcontractor_id IS NOT NULL" . $condition1 . " GROUP BY sc.subcontractor_id) b ON b.subcontractor_id = sc.subcontractor_id
                                LEFT JOIN (SELECT subcontractor_id, SUM(paid) as subcontractortotalpaid FROM {$tblpx}expenses WHERE projectid = {$project_id} AND type = 73 AND subcontractor_id IS NOT NULL" . $condition2 . ") c ON 1 = 1
                                LEFT JOIN (SELECT sp.payment_id,sc.subcontractor_id as subconid,sc.subcontractor_name as sname FROM {$tblpx}subcontractor sc LEFT JOIN {$tblpx}subcontractor_payment sp ON sc.subcontractor_id = sp.subcontractor_id WHERE 1 = 1" . $condition7 . ") d ON d.payment_id = ex.subcontractor_id
                                WHERE ex.projectid = {$project_id} AND ex.type = 73 AND ex.subcontractor_id IS NOT NULL" . $condition1 . " ORDER BY sc.subcontractor_name")->queryAll();
        $vendorpayment  = Yii::app()->db->createCommand("SELECT dv.*, v.name as vendor, a.vendorcount, b.vendorsum, c.vendortotalsum FROM {$tblpx}dailyvendors dv
                                LEFT JOIN {$tblpx}vendors v ON v.vendor_id = dv.vendor_id
                                LEFT JOIN (SELECT vendor_id, COUNT(*) as vendorcount FROM {$tblpx}dailyvendors WHERE project_id = {$project_id}" . $condition2 . " GROUP BY vendor_id ORDER BY vendor_id) a ON a.vendor_id = dv.vendor_id
                                LEFT JOIN (SELECT vendor_id, SUM(tax_amount + amount) as vendorsum FROM {$tblpx}dailyvendors WHERE project_id = {$project_id}" . $condition2 . " GROUP BY vendor_id ORDER BY vendor_id) b ON b.vendor_id = dv.vendor_id
                                LEFT JOIN (SELECT vendor_id, SUM(tax_amount + amount) as vendortotalsum FROM {$tblpx}dailyvendors WHERE project_id = {$project_id}" . $condition2 . " ORDER BY vendor_id) c ON 1 = 1
                                WHERE dv.project_id = {$project_id}" . $condition5 . " ORDER BY v.name")->queryAll();

        $mPDF1 = Yii::app()->ePdf->mPDF();
        $mPDF1 = Yii::app()->ePdf->mPDF('', 'A4');
        $sql = 'SELECT template_name '
                . 'FROM jp_quotation_template '
                . 'WHERE status="1"';
        $selectedtemplate =Yii::app()->db->createCommand($sql)->queryScalar();
        
        $template = $this->getTemplate($selectedtemplate);
        $mPDF1->SetHTMLHeader($template['header']);
        $mPDF1->SetHTMLFooter($template['footer']);
        $mPDF1->AddPage('','', '', '', '', 0,0,52, 50, 10,0);
        
        $mPDF1->WriteHTML($this->renderPartial('expensereportpreview', array(
            'project_id' => $project_id,
            'projectdata' => $projectdata,
            'date_from' => $date_from,
            'date_to' => $date_to,
            'billdata' => $billdata,
            'subconbill' => $subconbill,
            'unbilledpo' => $unbilledpo,
            'quotationdata' => $quotationdata,
            'invoicedata' => $invoicedata,
            'daybookreceipt' => $daybookreceipt,
            'daybookvendor' => $daybookvendor,
            'daybooksubcon' => $daybooksubcon,
            'vendorpayment' => $vendorpayment
        ), true));
        $mPDF1->Output('Project_Report_' . $projectdata["name"] . '.pdf', 'D');
    }
    public function actionExportProjectCSV()
    {
        $tblpx      = Yii::app()->db->tablePrefix;
        $project_id = $_REQUEST['project_id'];
        if (isset($_REQUEST["date_from"]) && !empty($_REQUEST["date_from"]) && isset($_REQUEST["date_to"]) && !empty($_REQUEST["date_to"])) {
            $date_from  = date("Y-m-d", strtotime($_REQUEST["date_from"]));
            $date_to    = date("Y-m-d", strtotime($_REQUEST["date_to"]));
            $condition1 = " AND (ex.date BETWEEN '" . $date_from . "' AND '" . $date_to . "')";
            $condition2 = " AND (date BETWEEN '" . $date_from . "' AND '" . $date_to . "')";
            $condition3 = " AND (bl.bill_date BETWEEN '" . $date_from . "' AND '" . $date_to . "')";
            $condition4 = " AND (inv.date BETWEEN '" . $date_from . "' AND '" . $date_to . "')";
            $condition5 = " AND (dv.date BETWEEN '" . $date_from . "' AND '" . $date_to . "')";
            $condition6 = " AND (sq.scquotation_date BETWEEN '" . $date_from . "' AND '" . $date_to . "')";
            $condition7 = " AND (sp.date BETWEEN '" . $date_from . "' AND '" . $date_to . "')";
            $condition8 = " AND (pu.purchase_date BETWEEN '" . $date_from . "' AND '" . $date_to . "')";
        } else if (isset($_REQUEST["date_from"]) && empty($_REQUEST["date_from"]) && isset($_REQUEST["date_to"]) && !empty($_REQUEST["date_to"])) {
            $date_from  = "";
            $date_to    = date("Y-m-d", strtotime($_POST["date_to"]));;
            $condition1 = " AND ex.date <= '" . $date_to . "'";
            $condition2 = " AND date <= '" . $date_to . "'";
            $condition3 = " AND bl.bill_date <= '" . $date_to . "'";
            $condition4 = " AND inv.date <= '" . $date_to . "'";
            $condition5 = " AND dv.date <= '" . $date_to . "'";
            $condition6 = " AND sq.scquotation_date <= '" . $date_to . "'";
            $condition7 = " AND sp.date <= '" . $date_to . "'";
            $condition8 = " AND pu.purchase_date <= '" . $date_to . "'";
        } else if (isset($_REQUEST["date_from"]) && !empty($_REQUEST["date_from"]) && isset($_REQUEST["date_to"]) && empty($_REQUEST["date_to"])) {
            $date_from  = date("Y-m-d", strtotime($_POST["date_from"]));
            $date_to    = "";
            $condition1 = " AND ex.date >= '" . $date_from . "'";
            $condition2 = " AND date >= '" . $date_from . "'";
            $condition3 = " AND bl.bill_date >= '" . $date_from . "'";
            $condition4 = " AND inv.date >= '" . $date_from . "'";
            $condition5 = " AND dv.date >= '" . $date_from . "'";
            $condition6 = " AND sq.scquotation_date >= '" . $date_from . "'";
            $condition7 = " AND sp.date >= '" . $date_from . "'";
            $condition8 = " AND pu.purchase_date >= '" . $date_from . "'";
        } else {
            $condition1 = "";
            $condition2 = "";
            $condition3 = "";
            $condition4 = "";
            $condition5 = "";
            $condition6 = "";
            $condition7 = "";
            $condition8 = "";
            $date_from  = "";
            $date_to    = "";
        }
        $projectdata    = Yii::app()->db->createCommand("SELECT pr.*, cl.name as client, st.caption from {$tblpx}projects pr LEFT JOIN {$tblpx}clients cl ON pr.client_id = cl.cid LEFT JOIN {$tblpx}status st ON pr.project_type = st.sid WHERE pr.pid = {$project_id}")->queryRow();
        $billdata       = Yii::app()->db->createCommand("SELECT bl.*, pu.purchase_no, a.billsum,ve.name as vendorname FROM {$tblpx}bills bl LEFT JOIN {$tblpx}purchase pu ON bl.purchase_id = pu.p_id LEFT JOIN (SELECT SUM(bl.bill_totalamount) as billsum FROM {$tblpx}bills bl LEFT JOIN {$tblpx}purchase pu ON bl.purchase_id = pu.p_id WHERE pu.project_id = {$project_id}" . $condition3 . ") a ON 1 = 1 LEFT JOIN {$tblpx}vendors ve ON pu.vendor_id = ve.vendor_id WHERE pu.project_id = {$project_id}" . $condition3 . "")->queryAll();
        $subconbill     = Yii::app()->db->createCommand("SELECT sp.*,sq.scquotation_decription,sq.subcontractor_id FROM {$tblpx}subcontractorbill sp LEFT JOIN {$tblpx}scquotation sq ON sp.scquotation_id = sq.scquotation_id WHERE sq.project_id = {$project_id}" . $condition7)->queryAll();
        $unbilledpo     = Yii::app()->db->createCommand("SELECT pu.*,ve.name as vname,et.type_name as expensehead FROM {$tblpx}purchase pu LEFT JOIN {$tblpx}expense_type et ON pu.expensehead_id = et.type_id LEFT JOIN {$tblpx}vendors ve ON pu.vendor_id = ve.vendor_id WHERE pu.project_id = {$project_id} AND pu.unbilled_amount > 0" . $condition8)->queryAll();
        $quotationdata  = Yii::app()->db->createCommand("SELECT sq.*,sc.*,a.quotationsum FROM {$tblpx}scquotation sq LEFT JOIN {$tblpx}subcontractor sc ON sq.subcontractor_id = sc.subcontractor_id LEFT JOIN (SELECT sq.subcontractor_id, SUM(sq.scquotation_amount) as quotationsum FROM {$tblpx}scquotation sq LEFT JOIN {$tblpx}subcontractor sc ON sq.subcontractor_id = sc.subcontractor_id WHERE sq.project_id = {$project_id}" . $condition6 . ") a ON 1 = 1 WHERE sq.project_id = {$project_id}" . $condition6 . "")->queryAll();
        $invoicedata    = Yii::app()->db->createCommand("SELECT inv.*, a.invoicetotal FROM {$tblpx}invoice inv LEFT JOIN (SELECT SUM(tax_amount + subtotal) as invoicetotal FROM {$tblpx}invoice WHERE project_id = {$project_id}" . $condition2 . ") a ON 1 = 1 WHERE project_id = {$project_id}" . $condition4 . "")->queryAll();
        $daybookreceipt = Yii::app()->db->createCommand("SELECT ex.*,a.receiptsum FROM {$tblpx}expenses ex LEFT JOIN (SELECT SUM(receipt) as receiptsum FROM {$tblpx}expenses WHERE type = 72 AND projectid = {$project_id}" . $condition2 . ") a ON 1 = 1 WHERE ex.type = 72 AND ex.projectid = {$project_id}" . $condition1 . "")->queryAll();
        $daybookvendor  = Yii::app()->db->createCommand("SELECT ex.*, v.name as vendor, a.vendorcount, b.vendorpaid, c.totalvendorpaid, ext.type_name as expensetype,d.vendorsubsum FROM {$tblpx}expenses ex
                                LEFT JOIN {$tblpx}vendors v ON v.vendor_id = ex.vendor_id
                                LEFT JOIN {$tblpx}expense_type ext ON ex.exptype = ext.type_id
                                LEFT JOIN (SELECT COUNT(*) as vendorcount,vendor_id FROM {$tblpx}expenses WHERE projectid = {$project_id} AND type = 73 AND vendor_id IS NOT NULL" . $condition2 . " GROUP BY vendor_id) a ON a.vendor_id = ex.vendor_id
                                LEFT JOIN (SELECT ex.vendor_id,SUM(ex.paid) as vendorpaid FROM {$tblpx}expenses ex WHERE ex.projectid = {$project_id} AND ex.type = 73 AND ex.vendor_id IS NOT NULL" . $condition1 . " GROUP BY ex.vendor_id ORDER BY ex.vendor_id) b ON b.vendor_id = ex.vendor_id
                                LEFT JOIN (SELECT ex.vendor_id,SUM(ex.paid) as totalvendorpaid FROM {$tblpx}expenses ex WHERE ex.projectid = {$project_id} AND ex.type = 73 AND ex.vendor_id IS NOT NULL" . $condition1 . " ORDER BY ex.vendor_id) c ON 1 = 1
                                LEFT JOIN (SELECT pu.vendor_id as vendorid, SUM(IFNULL(bl.bill_totalamount,0)) as vendorsubsum FROM {$tblpx}bills bl LEFT JOIN {$tblpx}purchase pu ON bl.purchase_id = pu.p_id WHERE pu.project_id = {$project_id}" . $condition3 . " GROUP BY pu.vendor_id) d ON d.vendorid = ex.vendor_id
                                WHERE ex.projectid = {$project_id} AND ex.type = 73 AND ex.vendor_id IS NOT NULL" . $condition1 . " ORDER BY v.name")->queryAll();
        $daybooksubcon  = Yii::app()->db->createCommand("SELECT ex.*, a.subcontractorcount, b.subcontractorpaid, c.subcontractortotalpaid, d.sname, d.subconid FROM {$tblpx}expenses ex
                                LEFT JOIN {$tblpx}subcontractor_payment sp ON sp.payment_id = ex.subcontractor_id
                                LEFT JOIN {$tblpx}subcontractor sc ON sp.subcontractor_id = sc.subcontractor_id
                                LEFT JOIN (SELECT COUNT(*) as subcontractorcount,sp.payment_id,sc.subcontractor_id FROM {$tblpx}expenses ex LEFT JOIN {$tblpx}subcontractor_payment sp ON ex.subcontractor_id = sp.payment_id LEFT JOIN {$tblpx}subcontractor sc ON sp.subcontractor_id = sc.subcontractor_id WHERE ex.projectid = {$project_id} AND ex.type = 73 AND ex.subcontractor_id IS NOT NULL" . $condition1 . " GROUP BY sc.subcontractor_id) a ON a.subcontractor_id = sc.subcontractor_id
                                LEFT JOIN (SELECT SUM(ex.paid) as subcontractorpaid,sp.payment_id,sc.subcontractor_id FROM {$tblpx}expenses ex LEFT JOIN {$tblpx}subcontractor_payment sp ON ex.subcontractor_id = sp.payment_id LEFT JOIN {$tblpx}subcontractor sc ON sp.subcontractor_id = sc.subcontractor_id WHERE ex.projectid = {$project_id} AND ex.type = 73 AND ex.subcontractor_id IS NOT NULL" . $condition1 . " GROUP BY sc.subcontractor_id) b ON b.subcontractor_id = sc.subcontractor_id
                                LEFT JOIN (SELECT subcontractor_id, SUM(paid) as subcontractortotalpaid FROM {$tblpx}expenses WHERE projectid = {$project_id} AND type = 73 AND subcontractor_id IS NOT NULL" . $condition2 . ") c ON 1 = 1
                                LEFT JOIN (SELECT sp.payment_id,sc.subcontractor_id as subconid,sc.subcontractor_name as sname FROM {$tblpx}subcontractor sc LEFT JOIN {$tblpx}subcontractor_payment sp ON sc.subcontractor_id = sp.subcontractor_id WHERE 1 = 1" . $condition7 . ") d ON d.payment_id = ex.subcontractor_id
                                WHERE ex.projectid = {$project_id} AND ex.type = 73 AND ex.subcontractor_id IS NOT NULL" . $condition1 . " ORDER BY sc.subcontractor_id")->queryAll();
        $vendorpayment  = Yii::app()->db->createCommand("SELECT dv.*, v.name as vendor, a.vendorcount, b.vendorsum, c.vendortotalsum FROM {$tblpx}dailyvendors dv
                                LEFT JOIN {$tblpx}vendors v ON v.vendor_id = dv.vendor_id
                                LEFT JOIN (SELECT vendor_id, COUNT(*) as vendorcount FROM {$tblpx}dailyvendors WHERE project_id = {$project_id}" . $condition2 . " GROUP BY vendor_id ORDER BY vendor_id) a ON a.vendor_id = dv.vendor_id
                                LEFT JOIN (SELECT vendor_id, SUM(tax_amount + amount) as vendorsum FROM {$tblpx}dailyvendors WHERE project_id = {$project_id}" . $condition2 . " GROUP BY vendor_id ORDER BY vendor_id) b ON b.vendor_id = dv.vendor_id
                                LEFT JOIN (SELECT vendor_id, SUM(tax_amount + amount) as vendortotalsum FROM {$tblpx}dailyvendors WHERE project_id = {$project_id}" . $condition2 . " ORDER BY vendor_id) c ON 1 = 1
                                WHERE dv.project_id = {$project_id}" . $condition5 . " ORDER BY v.name")->queryAll();

        $daybookVT      = !empty($daybookvendor) ? $daybookvendor[0]["totalvendorpaid"] : 0;
        $daybookSunT    = !empty($daybooksubcon) ? $daybooksubcon[0]["subcontractortotalpaid"] : 0;
        $vendorVT       = !empty($vendorpayment) ? $vendorpayment[0]["vendortotalsum"] : 0;
        $totalPayment   = $daybookVT + $daybookSunT + $vendorVT;
        $totalInvoice   = !empty($invoicedata) ? $invoicedata[0]["invoicetotal"] : 0;
        $invoiceReceipt = !empty($daybookreceipt) ? $daybookreceipt[0]["receiptsum"] : 0;
        $balanceInvoice = $totalInvoice - $invoiceReceipt;
        $totalBillAmt   = !empty($billdata) ? $billdata[0]["billsum"] : 0;
        $totalQuotSum   = !empty($quotationdata) ? $quotationdata[0]["quotationsum"] : 0;
        $totalExpense   = $totalBillAmt + $daybookSunT;
        $balanceExpense = $totalExpense - $totalPayment;
        $subbilltotal   = 0;
        $unbilledtotal  = 0;

        $arraylabel = array('Client', 'Project Name', 'Square Feet', 'Work Contract', 'Percentage', 'Site');
        $finaldata  = array();

        $finaldata[0][] = !empty($projectdata["client"]) ? $projectdata["client"] : "";
        $finaldata[0][] = !empty($projectdata['name']) ? $projectdata['name'] : "";
        $finaldata[0][] = !empty($projectdata['sqft']) ? $projectdata['sqft'] : "";
        $finaldata[0][] = !empty($projectdata['caption']) ? $projectdata['caption'] : "";
        $finaldata[0][] = !empty($projectdata['percentage']) ? $projectdata['percentage'] : "";
        $finaldata[0][] = !empty($projectdata['site']) ? $projectdata['site'] : "";

        $finaldata[1][] = '';
        $finaldata[1][] = '';
        $finaldata[1][] = '';
        $finaldata[1][] = '';
        $row    = 2;
        if (!empty($invoicedata)) {
            $finaldata[$row][] = '';
            $finaldata[$row][] = 'Invoice';
            $finaldata[$row][] = '';
            $finaldata[$row][] = '';
            $row    = $row + 1;
            $finaldata[$row][] = 'Sl No';
            $finaldata[$row][] = 'Invoice Number';
            $finaldata[$row][] = 'Date';
            $finaldata[$row][] = 'Invoice Amount';
            $i      = 1;
            $row    = $row + 1;
            foreach ($invoicedata as $invoice) {
                $finaldata[$row][] = $i;
                $finaldata[$row][] = $invoice["inv_no"];
                $finaldata[$row][] = $invoice["date"];
                $finaldata[$row][] = Controller::money_format_inr($invoice["subtotal"] + $invoice["tax_amount"], 2);
                $i      = $i + 1;
                $row    = $row + 1;
            }
            $finaldata[$row][] = '';
            $finaldata[$row][] = '';
            $finaldata[$row][] = 'Total';
            $finaldata[$row][] = Controller::money_format_inr($invoicedata[0]["invoicetotal"], 2);
            $row    = $row + 1;
        }
        if (!empty($daybookreceipt)) {
            $finaldata[$row][] = '';
            $finaldata[$row][] = 'Invoice Receipt';
            $finaldata[$row][] = '';
            $finaldata[$row][] = '';
            $finaldata[$row][] = '';
            $row    = $row + 1;
            $finaldata[$row][] = 'Sl No';
            $finaldata[$row][] = 'Description';
            $finaldata[$row][] = 'Date';
            $finaldata[$row][] = 'Receipt';
            $finaldata[$row][] = 'Paid';
            $i      = 1;
            $row    = $row + 1;
            foreach ($daybookreceipt as $receipt) {
                $finaldata[$row][] = $i;
                $finaldata[$row][] = $receipt["description"];;
                $finaldata[$row][] = $receipt["date"];
                $finaldata[$row][] = Controller::money_format_inr($receipt["receipt"], 2);
                $finaldata[$row][] = '';
                $i      = $i + 1;
                $row    = $row + 1;
            }
            $finaldata[$row][] = '';
            $finaldata[$row][] = '';
            $finaldata[$row][] = 'Total';
            $finaldata[$row][] = Controller::money_format_inr($daybookreceipt[0]["receiptsum"], 2);
            $finaldata[$row][] = '';
            $row    = $row + 1;
        }
        if (!empty($billdata)) {
            $finaldata[$row][] = '';
            $finaldata[$row][] = 'Bill Details';
            $finaldata[$row][] = '';
            $finaldata[$row][] = '';
            $finaldata[$row][] = '';
            $finaldata[$row][] = '';
            $row    = $row + 1;
            $finaldata[$row][] = 'Sl No';
            $finaldata[$row][] = 'Purchase Number';
            $finaldata[$row][] = 'Bill Number';
            $finaldata[$row][] = 'Vendor';
            $finaldata[$row][] = 'Date';
            $finaldata[$row][] = 'Bill Amount';
            $i      = 1;
            $row    = $row + 1;
            foreach ($billdata as $bill) {
                $finaldata[$row][] = $i;
                $finaldata[$row][] = $bill["purchase_no"];
                $finaldata[$row][] = $bill["bill_number"];
                $finaldata[$row][] = $bill["vendorname"];
                $finaldata[$row][] = $bill["bill_date"];
                $finaldata[$row][] = Controller::money_format_inr($bill["bill_totalamount"], 2);;
                $i      = $i + 1;
                $row    = $row + 1;
            }
            $finaldata[$row][] = '';
            $finaldata[$row][] = '';
            $finaldata[$row][] = '';
            $finaldata[$row][] = 'Total';
            $finaldata[$row][] = Controller::money_format_inr($billdata[0]["billsum"], 2);
            $row    = $row + 1;
        }
        if (!empty($subconbill)) {
            $finaldata[$row][] = '';
            $finaldata[$row][] = 'Subcontractor Bill';
            $finaldata[$row][] = '';
            $finaldata[$row][] = '';
            $finaldata[$row][] = '';
            $finaldata[$row][] = '';
            $row    = $row + 1;
            $finaldata[$row][] = 'Sl No';
            $finaldata[$row][] = 'Quotation';
            $finaldata[$row][] = 'Bill Number';
            $finaldata[$row][] = 'Subcontractor';
            $finaldata[$row][] = 'Date';
            $finaldata[$row][] = 'Bill Amount';
            $i      = 1;
            $row    = $row + 1;
            foreach ($subconbill as $subbill) {
                $finaldata[$row][] = $i;
                $finaldata[$row][] = $subbill["scquotation_decription"];
                $finaldata[$row][] = $subbill["bill_number"];
                $subcontractor    = Subcontractor::model()->findByPk($subbill["subcontractor_id"]);
                $finaldata[$row][] = $subcontractor->subcontractor_name;
                $finaldata[$row][] = $subbill["date"];
                $finaldata[$row][] = Controller::money_format_inr($subbill["total_amount"], 2);;
                $i                = $i + 1;
                $row              = $row + 1;
                $subbilltotal     = $subbilltotal + $subbill["total_amount"];
            }
            $finaldata[$row][] = '';
            $finaldata[$row][] = '';
            $finaldata[$row][] = '';
            $finaldata[$row][] = 'Total';
            $finaldata[$row][] = Controller::money_format_inr($subbilltotal, 2);
            $row    = $row + 1;
        }
        if (!empty($subconbill)) {
            $finaldata[$row][] = '';
            $finaldata[$row][] = 'Un billed Purchase Order';
            $finaldata[$row][] = '';
            $finaldata[$row][] = '';
            $finaldata[$row][] = '';
            $finaldata[$row][] = '';
            $row    = $row + 1;
            $finaldata[$row][] = 'Sl No';
            $finaldata[$row][] = 'Purchase Number';
            $finaldata[$row][] = 'Vendor';
            $finaldata[$row][] = 'Expense Head';
            $finaldata[$row][] = 'Date';
            $finaldata[$row][] = 'Amount';
            $i      = 1;
            $row    = $row + 1;

            foreach ($unbilledpo as $unbill) {
                $finaldata[$row][] = $i;
                $finaldata[$row][] = $unbill["purchase_no"];
                $finaldata[$row][] = $unbill["vname"];
                $finaldata[$row][] = $unbill["expensehead"];
                $finaldata[$row][] = $unbill["purchase_date"];
                $finaldata[$row][] = Controller::money_format_inr($unbill["unbilled_amount"], 2);;
                $i                = $i + 1;
                $row              = $row + 1;
                $unbilledtotal    = $unbilledtotal + $unbill["unbilled_amount"];
            }
            $finaldata[$row][] = '';
            $finaldata[$row][] = '';
            $finaldata[$row][] = '';
            $finaldata[$row][] = 'Total';
            $finaldata[$row][] = Controller::money_format_inr($unbilledtotal, 2);
            $row    = $row + 1;
        }
        $totalBillExpense   = $totalBillAmt + $subbilltotal + $unbilledtotal;
        $balanceBillExpense = $totalBillExpense - $totalPayment;
        $finaldata[$row][] = '';
        $finaldata[$row][] = '';
        $finaldata[$row][] = '';
        $finaldata[$row][] = 'Total Bill Amount';
        $finaldata[$row][] = Controller::money_format_inr(round($totalBillExpense), 2);
        $row    = $row + 1;
        if (!empty($quotationdata)) {
            $finaldata[$row][] = '';
            $finaldata[$row][] = 'Subcontractor Quotation';
            $finaldata[$row][] = '';
            $finaldata[$row][] = '';
            $finaldata[$row][] = '';
            $row    = $row + 1;
            $finaldata[$row][] = 'Sl No';
            $finaldata[$row][] = 'Subcontractor';
            $finaldata[$row][] = 'Description';
            $finaldata[$row][] = 'Date';
            $finaldata[$row][] = 'Quotation Amount';
            $i      = 1;
            $row    = $row + 1;
            foreach ($quotationdata as $quotation) {
                $finaldata[$row][] = $i;
                $finaldata[$row][] = $quotation["subcontractor_name"];
                $finaldata[$row][] = $quotation["scquotation_decription"];
                $finaldata[$row][] = $quotation["scquotation_date"];
                $finaldata[$row][] = Controller::money_format_inr($quotation["scquotation_amount"], 2);;
                $i      = $i + 1;
                $row    = $row + 1;
            }
            $finaldata[$row][] = '';
            $finaldata[$row][] = '';
            $finaldata[$row][] = '';
            $finaldata[$row][] = 'Total';
            $finaldata[$row][] = Controller::money_format_inr($quotationdata[0]["quotationsum"], 2);
            $row    = $row + 1;
        }
        $finaldata[$row][] = '';
        $finaldata[$row][] = '';
        $finaldata[$row][] = '';
        $finaldata[$row][] = 'Total Subcontractor Quotation';
        $finaldata[$row][] = Controller::money_format_inr(round($totalQuotSum), 2);
        $row    = $row + 1;
        if (!empty($daybookvendor)) {
            $finaldata[$row][] = '';
            $finaldata[$row][] = 'Daybook Vendor Payment';
            $finaldata[$row][] = '';
            $finaldata[$row][] = '';
            $finaldata[$row][] = '';
            $row    = $row + 1;
            $finaldata[$row][] = 'Sl No';
            $finaldata[$row][] = 'Description';
            $finaldata[$row][] = 'Expense Head';
            $finaldata[$row][] = 'Date';
            $finaldata[$row][] = 'Receipt';
            $finaldata[$row][] = 'Paid';
            $row    = $row + 1;
            $c  = 1;
            foreach ($daybookvendor as $vendor) {
                if ($c != $vendor["vendorcount"]) {
                    $c = $c + 1;
                } else {
                    $vendorCount[] = $vendor["vendorcount"];
                    $c = 1;
                }
            }
            $vendorCount    = array_values($vendorCount);
            $vendorNum      = count($vendorCount);
            $totalV         = 0;
            for ($i = 0; $i < $vendorNum; $i++) {
                $vendorC    = $vendorCount[$i];
                for ($j = 0; $j < $vendorC; $j++) {
                    $vCount = $daybookvendor[$j]["vendorcount"];
                    if ($j == 0) {
                        $finaldata[$row][] = '';
                        $finaldata[$row][] = $daybookvendor[$totalV]["vendor"];
                        $finaldata[$row][] = '';
                        $finaldata[$row][] = '';
                        $finaldata[$row][] = '';
                        $finaldata[$row][] = '';
                        $row    = $row + 1;
                    }
                    $finaldata[$row][] = $j + 1;
                    $finaldata[$row][] = $daybookvendor[$totalV]["description"];
                    $finaldata[$row][] = $daybookvendor[$totalV]["expensetype"];
                    $finaldata[$row][] = $daybookvendor[$totalV]["date"];
                    $finaldata[$row][] = '';
                    $finaldata[$row][] = Controller::money_format_inr($daybookvendor[$totalV]["paid"], 2);
                    $row    = $row + 1;
                    if ($vendorC == $j + 1) {
                        $finaldata[$row][] = '';
                        $finaldata[$row][] = '';
                        $finaldata[$row][] = '';
                        $finaldata[$row][] = '';
                        $finaldata[$row][] = 'Total';
                        $finaldata[$row][] = Controller::money_format_inr(round($daybookvendor[$totalV]["vendorpaid"]), 2);
                        $row    = $row + 1;
                        $finaldata[$row][] = '';
                        $finaldata[$row][] = '';
                        $finaldata[$row][] = '';
                        $finaldata[$row][] = '';
                        $finaldata[$row][] = 'Balance To Be Paid';
                        $finaldata[$row][] = Controller::money_format_inr(round($daybookvendor[$totalV]["vendorsubsum"] - $daybookvendor[$totalV]["vendorpaid"]), 2);
                        $row    = $row + 1;
                    }
                    $totalV = $totalV + 1;
                }
            }
            $finaldata[$row][] = '';
            $finaldata[$row][] = '';
            $finaldata[$row][] = '';
            $finaldata[$row][] = 'Grand Total';
            $finaldata[$row][] = Controller::money_format_inr(round($daybookvendor[0]["totalvendorpaid"]), 2);
            $row    = $row + 1;
            $finaldata[$row][] = '';
            $finaldata[$row][] = '';
            $finaldata[$row][] = '';
            $finaldata[$row][] = 'Balance To Be Paid';
            $finaldata[$row][] = Controller::money_format_inr(round($totalBillAmt - $daybookVT), 2);
            $row    = $row + 1;
        }
        if (!empty($daybooksubcon)) {
            $finaldata[$row][] = '';
            $finaldata[$row][] = 'Subcontractor Payment';
            $finaldata[$row][] = '';
            $finaldata[$row][] = '';
            $finaldata[$row][] = '';
            $row    = $row + 1;
            $finaldata[$row][] = 'Sl No';
            $finaldata[$row][] = 'Description';
            $finaldata[$row][] = 'Date';
            $finaldata[$row][] = 'Receipt';
            $finaldata[$row][] = 'Paid';
            $row    = $row + 1;
            $c  = 1;
            foreach ($daybooksubcon as $subcon) {
                if ($c != $subcon["subcontractorcount"]) {
                    $c = $c + 1;
                } else {
                    $subconCount[] = $subcon["subcontractorcount"];
                    $c = 1;
                }
            }
            $subconCount    = array_values($subconCount);
            $subconNum      = count($subconCount);
            $totalS = 0;
            for ($i = 0; $i < $subconNum; $i++) {
                $subconC    = $subconCount[$i];
                for ($j = 0; $j < $subconC; $j++) {
                    $sCount = $daybooksubcon[$j]["subcontractorcount"];
                    if ($j == 0) {
                        $finaldata[$row][] = '';
                        $finaldata[$row][] = $daybooksubcon[$totalS]["sname"];
                        $finaldata[$row][] = '';
                        $finaldata[$row][] = '';
                        $finaldata[$row][] = '';
                        $row    = $row + 1;
                    }
                    $finaldata[$row][] = $j + 1;
                    $finaldata[$row][] = $daybooksubcon[$totalS]["description"];
                    $finaldata[$row][] = $daybooksubcon[$totalS]["date"];
                    $finaldata[$row][] = '';
                    $finaldata[$row][] = Controller::money_format_inr($daybooksubcon[$totalS]["paid"], 2);
                    $row    = $row + 1;
                    if ($subconC == $j + 1) {
                        $finaldata[$row][] = '';
                        $finaldata[$row][] = '';
                        $finaldata[$row][] = '';
                        $finaldata[$row][] = 'Total';
                        $finaldata[$row][] = Controller::money_format_inr(round($daybooksubcon[$totalS]["subcontractorpaid"]), 2);
                        $row    = $row + 1;
                    }
                    $totalS = $totalS + 1;
                }
            }
            $finaldata[$row][] = '';
            $finaldata[$row][] = '';
            $finaldata[$row][] = '';
            $finaldata[$row][] = 'Grand Total';
            $finaldata[$row][] = Controller::money_format_inr(round($daybooksubcon[0]["subcontractortotalpaid"]), 2);
            $row    = $row + 1;
        }
        if (!empty($vendorpayment)) {
            $finaldata[$row][] = '';
            $finaldata[$row][] = 'Bulk Vendor Payment';
            $finaldata[$row][] = '';
            $finaldata[$row][] = '';
            $finaldata[$row][] = '';
            $row    = $row + 1;
            $finaldata[$row][] = 'Sl No';
            $finaldata[$row][] = 'Description';
            $finaldata[$row][] = 'Date';
            $finaldata[$row][] = 'Receipt';
            $finaldata[$row][] = 'Paid';
            $row    = $row + 1;
            $c  = 1;
            foreach ($vendorpayment as $vendor) {
                if ($c != $vendor["vendorcount"]) {
                    $c = $c + 1;
                } else {
                    $vendorCount1[] = $vendor["vendorcount"];
                    $c = 1;
                }
            }
            $vendorCount1    = array_values($vendorCount1);
            $vendorNum1     = count($vendorCount1);
            $totalV1        = 0;
            for ($i = 0; $i < $vendorNum1; $i++) {
                $vendorC1    = $vendorCount1[$i];
                for ($j = 0; $j < $vendorC1; $j++) {
                    $vCount1 = $vendorpayment[$j]["vendorcount"];
                    if ($j == 0) {
                        $finaldata[$row][] = '';
                        $finaldata[$row][] = $vendorpayment[$totalV1]["vendor"];
                        $finaldata[$row][] = '';
                        $finaldata[$row][] = '';
                        $finaldata[$row][] = '';
                        $row    = $row + 1;
                    }
                    $finaldata[$row][] = $j + 1;
                    $finaldata[$row][] = $vendorpayment[$totalV1]["description"];
                    $finaldata[$row][] = $vendorpayment[$totalV1]["date"];
                    $finaldata[$row][] = '';
                    $finaldata[$row][] = Controller::money_format_inr($vendorpayment[$totalV1]["amount"] + $vendorpayment[$totalV1]["tax_amount"], 2);
                    $row    = $row + 1;
                    if ($vendorC1 == $j + 1) {
                        $finaldata[$row][] = '';
                        $finaldata[$row][] = '';
                        $finaldata[$row][] = '';
                        $finaldata[$row][] = 'Total';
                        $finaldata[$row][] = Controller::money_format_inr(round($vendorpayment[$totalV1]["vendorsum"]), 2);
                        $row    = $row + 1;
                    }
                    $totalV1 = $totalV1 + 1;
                }
            }
            $finaldata[$row][] = '';
            $finaldata[$row][] = '';
            $finaldata[$row][] = '';
            $finaldata[$row][] = 'Grand Total';
            $finaldata[$row][] = Controller::money_format_inr(round($vendorpayment[0]["vendortotalsum"]), 2);
            $row    = $row + 1;
        }
        $finaldata[$row][] = '';
        $finaldata[$row][] = '';
        $finaldata[$row][] = '';
        $finaldata[$row][] = 'Total Payments';
        $finaldata[$row][] = Controller::money_format_inr(round($totalPayment), 2);
        $row    = $row + 1;

        $finaldata[$row][] = '';
        $finaldata[$row][] = '';
        $finaldata[$row][] = '';
        $finaldata[$row][] = '';
        $finaldata[$row][] = '';
        $row    = $row + 1;

        $finaldata[$row][] = '';
        $finaldata[$row][] = 'TOTAL INVOICE';
        $finaldata[$row][] = Controller::money_format_inr(round($totalInvoice), 2);
        $finaldata[$row][] = '';
        $finaldata[$row][] = '';
        $row    = $row + 1;

        $finaldata[$row][] = '';
        $finaldata[$row][] = 'AMOUNT RECEIVED';
        $finaldata[$row][] = Controller::money_format_inr(round($invoiceReceipt), 2);
        $finaldata[$row][] = '';
        $finaldata[$row][] = '';
        $row    = $row + 1;

        $finaldata[$row][] = '';
        $finaldata[$row][] = 'BALANCE TO BE RECEIVED';
        $finaldata[$row][] = Controller::money_format_inr(round($balanceInvoice), 2);
        $finaldata[$row][] = '';
        $finaldata[$row][] = '';
        $row    = $row + 1;

        $finaldata[$row][] = '';
        $finaldata[$row][] = '';
        $finaldata[$row][] = '';
        $finaldata[$row][] = '';
        $finaldata[$row][] = '';
        $row    = $row + 1;

        $finaldata[$row][] = '';
        $finaldata[$row][] = 'TOTAL EXPENSE';
        $finaldata[$row][] = Controller::money_format_inr(round($totalBillExpense), 2);
        $finaldata[$row][] = '';
        $finaldata[$row][] = '';
        $row    = $row + 1;

        $finaldata[$row][] = '';
        $finaldata[$row][] = 'AMOUNT PAID';
        $finaldata[$row][] = Controller::money_format_inr(round($totalPayment), 2);
        $finaldata[$row][] = '';
        $finaldata[$row][] = '';
        $row    = $row + 1;

        $finaldata[$row][] = '';
        $finaldata[$row][] = 'BALANCE EXPENSE TO BE PAID';
        $finaldata[$row][] = Controller::money_format_inr(round($balanceBillExpense), 2);
        $finaldata[$row][] = '';
        $finaldata[$row][] = '';
        $row    = $row + 1;


        Yii::import('ext.ECSVExport');

        $csv = new ECSVExport($finaldata);

        $csv->setHeaders($arraylabel);
        $csv->setToAppend();
        $contentdata = $csv->toCSV();
        $csvfilename = 'Projects Report' . date('d_M_Y') . '.csv';
        Yii::app()->getRequest()->sendFile($csvfilename, $contentdata, "text/csv", false);
        exit();
    }

    public function getTotal($data)
    {
        $receipt_grandtotal = 0;
        $tblpx = Yii::app()->db->tablePrefix;
        $deficit_grandtotal = 0;
        $surplus_grandtotal = 0;
        foreach ($data as $key => $datavalue) {
            $receipt_grandtotal += $datavalue['tot_receipt'];
            $percentage = Yii::app()->db->createCommand("SELECT percentage FROM {$tblpx}projects WHERE pid = {$datavalue['pid']}")->queryScalar();
            $servise_charge = ($datavalue['tot_expense'] * $percentage) / 100;
            $inv_dtls = Yii::app()->db->createCommand("SELECT SUM(amount) as total_amount,SUM(tax_amount) as tax_amount FROM {$tblpx}invoice WHERE project_id = " . $datavalue['pid'] . "")->queryRow();
            if (($inv_dtls)) {
                if (($inv_dtls['total_amount'] + $inv_dtls['tax_amount']) == 0) {
                    $invoice = 0;
                } else {
                    $invoice = $inv_dtls['total_amount'] + $inv_dtls['tax_amount'];
                }
            } else {
                $invoice = 0;
            }
            if ($invoice == 0) {
                $diff = ($datavalue['tot_expense'] + $servise_charge) - $datavalue['tot_receipt'];
            } else {
                $diff = $invoice - $datavalue['tot_receipt'];
            }
            $deficit = 0;
            $surplus = 0;
            if ($diff < 0) {
                $surplus = $diff;
            } else {
                $deficit = $diff;
            }

            $deficit_grandtotal += $deficit;
            $surplus_grandtotal += $surplus;
        }
        $data = array(
            'receipt_tot' => $receipt_grandtotal, 'deficit_total' => $deficit_grandtotal,
            'surplus_total' => $surplus_grandtotal
        );
        return $data;
    }

    public function actionPaymentReport()
    {
        if (isset($_REQUEST['project_id']) && !empty($_REQUEST['project_id'])) {
            $project_id = $_REQUEST['project_id'];
            $project_model = Projects::model()->findByPk($project_id);
            $receipt_datas = Expenses::model()->findAll(array(
                'condition' => 'projectid = ' . $project_id . ' AND type = 72 AND return_id IS NULL',
            ));
            $quotation_datas =  Quotation::model()->findAll(array(
                'condition' => 'project_id = ' . $project_id,

            ));
            $newQuery2 = "";
            $user = Users::model()->findByPk(Yii::app()->user->id);
            $arrVal = explode(',', $user->company_id);
            foreach ($arrVal as $index => $arr) {
                if ($index > 0) {
                    $newQuery2 .= ' OR ';
                }
                $newQuery2 .= "FIND_IN_SET('" . $arr . "', company_id)";
            }
           
            $invoice_datas =  Invoice::model()->findAll(array(
                'condition' => 'project_id = ' . $project_id .' AND   invoice_status ="saved"  AND ('. $newQuery2 .')',
            ));

            $render_datas = array('project_id' => $project_id, 'receipt_datas' => $receipt_datas, 'quotation_datas' => $quotation_datas,'invoice_datas'=>$invoice_datas, 'model' => $project_model);
        } else {
            $project_model = new Projects;
            $render_datas = array('model' => $project_model);
        }

        if (isset($_GET['exportpdf'])) {
            $this->logo = $this->realpath_logo;
            $pdfdata = $this->renderPartial('payment_report', $render_datas, true);
            $filename = 'Payment-Report-'  . '.pdf';
            if (!empty($project_model->pid)) {
                $filename = 'Payment-Report-' . str_replace(" ", "_", $project_model->name) . '.pdf';
            }
            $mPDF1 = Yii::app()->ePdf->mPDF();
            $mPDF1 = Yii::app()->ePdf->mPDF('', 'A4');
            $sql = 'SELECT template_name '
                . 'FROM jp_quotation_template '
                . 'WHERE status="1"';
            $selectedtemplate =Yii::app()->db->createCommand($sql)->queryScalar();
            
            $template = $this->getTemplate($selectedtemplate);
            $mPDF1->SetHTMLHeader($template['header']);
            $mPDF1->SetHTMLFooter($template['footer']);
            $mPDF1->AddPage('','', '', '', '', 0,0,52, 30, 10,0);
            $mPDF1->WriteHTML($pdfdata);
            $mPDF1->Output($filename, 'D');
        } else {
           
            $this->render('payment_report', $render_datas);
        }
    }

    public function saveFlatDetails($flat_numbers, $project_id)
    {

        $buyer_flat_data = Buyers::model()->findAll(['condition' => 'project = ' . $project_id]);

        $buyer_flat_numbers = array();
        foreach ($buyer_flat_data as $buyer) {
            $buyer_flats_ = array();
            if (!empty($buyer['flat_numbers'])) {
                $buyer_flats = explode(',', $buyer['flat_numbers']);
                foreach ($buyer_flats as $flat) {
                    if (!in_array($flat, $buyer_flat_numbers)) {
                        array_push($buyer_flat_numbers, $flat);
                    }
                }
            }
        }

        $buyer_flat_numbers = implode(',', $buyer_flat_numbers);
        $flat_models = ProjectFlatNumbers::model()->findAll(['condition' => 'project_id =' . $project_id]);
        $flat_name_array = array();
        $flat_id_array = array();
        foreach ($flat_models as $flat_model) {
            if (!in_array($flat_model['id'], $flat_id_array)) {
                array_push($flat_name_array, $flat_model['flat_number']);
                array_push($flat_id_array, $flat_model['id']);
            }
        }

        $flat_number_array = array();
        $flat_numbers_data = $flat_numbers;
        $flat_number_array = explode(',', $flat_numbers_data);

        foreach ($flat_number_array as $flat_number) {
            if (!in_array($flat_number, $flat_name_array)) {
                $flat_detail_model = new ProjectFlatNumbers();
                $flat_detail_model->project_id = $project_id;
                $flat_detail_model->flat_number = $flat_number;
                $flat_detail_model->status = 1;
                $flat_detail_model->created_by = yii::app()->user->id;
                $flat_detail_model->updated_by = yii::app()->user->id;
                $flat_detail_model->created_date = date('Y-m-d');
                $flat_detail_model->updated_date = date('Y-m-d H:i:s');
                $flat_detail_model->save();
                // array_push($flat_id_array, $flat_detail_model['id']);
            }
        }
    }
    public function actiondynamicProject()
    {
        $company_id = filter_input(INPUT_POST, 'company_id', FILTER_SANITIZE_SPECIAL_CHARS);
        $projects_list = "<option value=''>Select Project</option>";
        if (!empty($company_id)) {
            $projects = Projects::model()->findAll(
                'FIND_IN_SET(:company_id,company_id)',
                array(':company_id' => (int) $_POST['company_id'])
            );
            $projects = CHtml::listData($projects, 'pid', 'name');
            foreach ($projects as $value => $name)
                $projects_list .= CHtml::tag('option', array('value' => $value), CHtml::encode($name), true);

            $subcontractor_list = "<option value=''>Select Subcontractor</option>";
            $subcontractors = Subcontractor::model()->findAll(
                'FIND_IN_SET(:company_id,company_id)',
                array(':company_id' => (int) $_POST['company_id'])
            );
            $subcontractors = CHtml::listData($subcontractors, 'subcontractor_id', 'subcontractor_name');
            foreach ($subcontractors as $value => $name)
                $subcontractor_list .= CHtml::tag('option', array('value' => $value), CHtml::encode($name), true);
        }
        $result = array('projects_list' => $projects_list, 'subcontractor_list' => $subcontractor_list);
        echo json_encode($result);
    }

    public function dispatchDetails($warehouse_ids, $date_from, $date_to,$search_item_id) {
        $tblpx = Yii::app()->db->tablePrefix;
        $disaptch_data_array = array();
        $criteria = new CDbCriteria();
        if (!empty($warehouse_ids)) {
            $criteria->condition = "warehousedespatch_warehouseid IN (" . $warehouse_ids . ")";
        }
        if (!empty($date_from) && !empty($date_to)) {
            $date_from = date('Y-m-d', strtotime($date_from));
            $date_to = date('Y-m-d', strtotime($date_to));
            $criteria->addCondition('warehousedespatch_date >= "' . $date_from . '" AND warehousedespatch_date <= "' . $date_to . '"');
        } else {
            if (!empty($date_from)) {
                $date_from = date('Y-m-d', strtotime($date_from));
                $criteria->addCondition('warehousedespatch_date >= "' . $date_from . '"');
            } else {
                $date_from = '';
                $criteria->compare('warehousedespatch_date', $date_from, true);
            }
            if (!empty($date_to)) {
                $date_to = date('Y-m-d', strtotime($date_to));
                $criteria->addCondition('warehousedespatch_date <= "' . $date_to . '"');
            } else {
                $date_to = '';
                $criteria->compare('warehousedespatch_date', $date_to, true);
            }
        }

        $dispatch_datas = Warehousedespatch::model()->findAll($criteria);

        if (!empty($dispatch_datas)) {
            $data_array = array();

            foreach ($dispatch_datas as $dispatch_data) {
                $data_array['dispatch_id'] = $dispatch_data['warehousedespatch_id'];
                $data_array['warehousedespatch_no'] = $dispatch_data['warehousedespatch_no'];
                $data_array['warehouse'] = $dispatch_data->warehouse->warehouse_name;
                $data_array['warehousedespatch_to'] = !empty($dispatch_data['warehousedespatch_warehouseid_to']) ? $dispatch_data->warehousedespatchWarehouseidTo->warehouse_name : '-';
                $data_array['dispatch_date'] = $dispatch_data['warehousedespatch_date'];
                $criteria_item = new CDbCriteria();

                if (isset($search_item_id) && $search_item_id != '') {
                    $criteria_item->condition = "warehousedespatch_id=:warehousedespatch_id AND warehousedespatch_itemid=:warehousedespatch_itemid";
                    $criteria_item->params = array(':warehousedespatch_id' => $dispatch_data['warehousedespatch_id'], ':warehousedespatch_itemid' => $search_item_id);
                } else {
                    $criteria_item->condition = "warehousedespatch_id=:warehousedespatch_id";
                    $criteria_item->params = array(':warehousedespatch_id' => $dispatch_data['warehousedespatch_id']);
                }
                $criteria_item->group = 'warehousedespatch_itemid,dimension';
                $dispatch_items = WarehousedespatchItems::model()->findAll($criteria_item);
                $data_array['item'] = array();
                if (!empty($dispatch_items)) {
                    $item_array = array();
                    $i = 0;
                    foreach ($dispatch_items as $item) {
                        if ($item['dimension'] != '') {
                            $receipt_item_cond = 'warehousedespatch_id = ' . $dispatch_data['warehousedespatch_id'] . ' AND warehousedespatch_itemid = ' . $item['warehousedespatch_itemid'] . ' AND dimension LIKE "%' . $item['dimension'] . '%"';
                        } else {
                            $receipt_item_cond = 'warehousedespatch_id = ' . $dispatch_data['warehousedespatch_id'] . ' AND warehousedespatch_itemid = ' . $item['warehousedespatch_itemid'] . ' AND (dimension IS NULL OR dimension ="")';
                        }
                        $despatch_total_qty = Yii::app()->db->createCommand()
                                ->select('sum(warehousedespatch_baseunit_quantity) as qty                        ')
                                ->from($tblpx . 'warehousedespatch_items')
                                ->where($receipt_item_cond)
                                ->queryRow();
                        $item_array[$i]['item'] = Controller::itemName($item['warehousedespatch_itemid']);
                        $item_array[$i]['dimension'] = $item['dimension'];
                        $item_array[$i]['unit'] = $item['warehousedespatch_unit'];
                        $item_array[$i]['qty'] = $despatch_total_qty['qty'];
                        $item_array[$i]['remark'] = $item['warehousedespatch_remarks'];
                        $item_array[$i]['item_id'] = $item['warehousedespatch_itemid'];
                        $item_array[$i]['rate'] = $item['warehousedespatch_rate'];
                        $i++;
                    }
                    $data_array['item'] = $item_array;
                }
                if (!empty($data_array)) {
                    array_push($disaptch_data_array, $data_array);
                }
            }
        }

        return $disaptch_data_array;
    }

    public function openingStockWarehouseId($warehouse_id) {
        $warehouse = Warehouse::model()->findByPk($warehouse_id);
        $warehouse_name = $warehouse->warehouse_name;
        return $warehouse_name;
    }

    public function receiptDetails($warehouse_ids, $date_from, $date_to,$search_item_id,$expensehead) {
        $tblpx = Yii::app()->db->tablePrefix;
        $receipt_data_array = array();
        $criteria = new CDbCriteria();
        if (!empty($warehouse_ids)) {
            $criteria->condition = "warehousereceipt_warehouseid IN (".$warehouse_ids.")";            
        }
        
        
        if (!empty($date_from) && !empty($date_to)) {
            $date_from = date('Y-m-d', strtotime($date_from));
            $date_to = date('Y-m-d', strtotime($date_to));
            $criteria->addCondition('warehousereceipt_date >= "' . $date_from . '" AND warehousereceipt_date <= "' . $date_to . '"');
        }else{
            if (!empty($date_from)) {
                $date_from = date('Y-m-d', strtotime($date_from));
                $criteria->addCondition('warehousereceipt_date >= "' . $date_from .'"');
            }else {
                $date_from = '';
                $criteria->compare('warehousereceipt_date',$date_from,true);
            }
            if (!empty($date_to)) {
                $date_to = date('Y-m-d', strtotime($date_to));
                $criteria->addCondition('warehousereceipt_date <= "' . $date_to . '"');
            }else {
                $date_to = '';
                $criteria->compare('warehousereceipt_date',$date_to,true);
            }
        }
        
        if(!empty($expensehead)){
            $criteria->join = 'LEFT JOIN ' . $this->tableName('Bills') . ' b ON warehousereceipt_bill_id = b.bill_id';
            $criteria->join .= ' LEFT JOIN ' . $this->tableName('Purchase') . ' p ON b.purchase_id = p.p_id';
            $criteria->join .= ' LEFT JOIN ' . $this->tableName('ExpenseType') . ' e ON p.expensehead_id =e.type_id';
            $criteria->addCondition('e.type_id ='.$expensehead);
        }
       
        
        
        $receipt_datas = Warehousereceipt::model()->findAll($criteria);       
        if (!empty($receipt_datas)) {
            $data_array = array();
          
            foreach ($receipt_datas as $receipt_data) {
                $data_array['receipt_id']   = $receipt_data['warehousereceipt_id'];
                $data_array['bill_id'] = $receipt_data['warehousereceipt_bill_id'];
                $data_array['despatch_id'] = $receipt_data['warehousereceipt_despatch_id'];
                $data_array['warehousereceipt_no'] = $receipt_data['warehousereceipt_no'];
                $data_array['warehouse'] = $receipt_data->warehouse->warehouse_name;
                $data_array['warehousereceipt_from'] = '' ;
                
                if(!empty($receipt_data['warehousereceipt_warehouseid_from'])){
                    $data_array['warehousereceipt_from'] = $receipt_data->warehousereceiptwarehouseidfrom->warehouse_name;
                }                                
                $data_array['remarks'] = $receipt_data['warehousereceipt_remark'];       
                $data_array['receipt_date'] = $receipt_data['warehousereceipt_date'];
                $criteria_item = new CDbCriteria();
                if (isset($search_item_id) && $search_item_id != '') {
                    $criteria_item->condition = "warehousereceipt_id=:warehousereceipt_id AND warehousereceipt_itemid=:warehousereceipt_itemid";
                    $criteria_item->params = array(':warehousereceipt_id' => $receipt_data['warehousereceipt_id'],':warehousereceipt_itemid' => $search_item_id);
                }else{
                    $criteria_item->condition = "warehousereceipt_id=:warehousereceipt_id";
                    $criteria_item->params = array(':warehousereceipt_id' => $receipt_data['warehousereceipt_id']);
                }
                
                $criteria_item->group = 'warehousereceipt_itemid,dimension';
                $receipt_items = WarehousereceiptItems::model()->findAll($criteria_item);               
                
                $data_array['item'] = array();
                if (!empty($receipt_items)) {
                    $item_array = array();
                    $i = 0;
                    foreach ($receipt_items as $item) { 
                        if($item['dimension'] !=''){                            
                            $receipt_item_cond =  'warehousereceipt_id = ' . $receipt_data['warehousereceipt_id'].' AND warehousereceipt_itemid = '.$item['warehousereceipt_itemid'].' AND dimension LIKE "%'.$item['dimension'].'%"';                    
                        }else{
                            $receipt_item_cond =  'warehousereceipt_id = ' . $receipt_data['warehousereceipt_id'].' AND warehousereceipt_itemid = '.$item['warehousereceipt_itemid'].' AND (dimension IS NULL OR dimension ="")' ;                    
                        }
                        $multiplying_quantity =1;
                        if($item['dimension'] != null){
                            $multiplying_quantity_arr = explode(" x ",$item['dimension']);
                            $multiplying_quantity = $multiplying_quantity_arr['0'] * $multiplying_quantity_arr['1'];
                        }                      
                        
                       $receipt_total_qty = Yii::app()->db->createCommand()
                        ->select('sum(warehousereceipt_baseunit_accepted_effective_quantity) as qty')
                        ->from($tblpx.'warehousereceipt_items')
                        ->where($receipt_item_cond)
                        ->queryRow();                                           
                        $item_array[$i]['item'] = Controller::itemName($item['warehousereceipt_itemid']);
                        $item_array[$i]['item_id'] = $item['warehousereceipt_itemid'];
                        $item_array[$i]['dimension'] = $item['dimension'];
                        $item_array[$i]['unit'] = $item['warehousereceipt_unit'];
                        $item_array[$i]['qty'] = $receipt_total_qty['qty'];
                        $item_array[$i]['rate'] = $item['warehousereceipt_rate'];  
                        $item_array[$i]['amount']  = $receipt_total_qty['qty'] * $item['warehousereceipt_rate']*$multiplying_quantity; 
                        $i++;
                        
                    }
                    $data_array['item'] = $item_array;
                }
                if (!empty($data_array)) {
                    array_push($receipt_data_array, $data_array);
                }
            }
        }
                        
        return $receipt_data_array;
    }

    public function actionGetItemCategory($parent = 0, $spacing = '', $user_tree_array = '') {
        
        if (!is_array($user_tree_array))
            $user_tree_array = array();
        $data = array();
        $final = array();
        $tblpx = Yii::app()->db->tablePrefix;
        $sql = "SELECT id, specification, brand_id, cat_id FROM {$tblpx}specification";
        $specification = Yii::app()->db->createCommand($sql)->queryAll();
                
        foreach ($specification as $key => $value) {

            $brand = '';
            if ($value['brand_id'] != NULL) {
                $brand_details = Yii::app()->db->createCommand("SELECT brand_name FROM {$tblpx}brand WHERE id=" . $value['brand_id'] . "")->queryRow();
                $brand = '-' . ucfirst($brand_details['brand_name']);
            }

            if ($value['cat_id'] != '') {
                $result = $this->actionGetParent($value['cat_id']);
                $final[$key]['data'] = ucfirst($result['category_name']) . $brand . '-' . ucfirst($value['specification']);
            } else {
                $final[$key]['data'] = $brand . '-' . $value['specification'];
            }

            $final[$key]['id'] = $value['id'];
        }
        return $final;
    }

    public function actionGetParent($id) {
        $tblpx = Yii::app()->db->tablePrefix;
        $sql = "SELECT parent_id, id , category_name "
                . "FROM {$tblpx}purchase_category WHERE id=" . $id;
        $category = Yii::app()->db->createCommand($sql)->queryRow();

        return $category;
    }
    public function actiongetitem() {
        $project_id = $_POST['project_id'];
        $data = array();
        $html['html'] = '';
        $html['status'] = '';
        
        echo CHtml::dropDownList('item_id','',$data,
            array('empty' => 'Select Item')
        );
        
        if ($project_id != '' || $project_id != 0 || $project_id != null) {
            $tblpx = Yii::app()->db->tablePrefix;
            $specification_details = $this->actionGetItemCategory($parent = 0, $spacing = '', $user_tree_array = '');
                        
            if (!empty($specification_details)) {
                foreach ($specification_details as $specifications) {
                    $data[$specifications['id']] = $specifications['data'];
                }
                echo CHtml::dropDownList('item_id','',$data,
                    array('empty' => 'Select Item')
                );
            } else {
                echo CHtml::dropDownList('item_id','',$data,
                    array('empty' => 'Select Item')
                );
            }
        } 
    }

    public function actiongetexpensehead() {
        
        $expensehead = $_POST['expensehead'];
        $data = array();
        $html['html'] = '';
        $html['status'] = '';
        
        echo CHtml::dropDownList('item_id','',$data,
            array('empty' => 'Select Item')
        );
        
        if ($expensehead != '' || $expensehead != 0 || $expensehead != null) {
            $tblpx = Yii::app()->db->tablePrefix;
            $specification_details = $this->actionGetItemCategory($parent = 0, $spacing = '', $user_tree_array = '');
                        
            if (!empty($specification_details)) {
                foreach ($specification_details as $specifications) {
                    $data[$specifications['id']] = $specifications['data'];
                }
                echo CHtml::dropDownList('item_id','',$data,
                    array('empty' => 'Select Item')
                );
            } else {
                echo CHtml::dropDownList('item_id','',$data,
                    array('empty' => 'Select Item')
                );
            }
        } 
    }

    public function actionmaterialConsumptionReport() {        
        $final_data = array();
        $final_data_array = array();
        $receipt_total = 0;
        $dispatch_total = 0;
        $net_total = 0;
        $receipt_datas = array();
        $date_from = isset($_POST['date_from']) ? $_POST['date_from'] : '';
        $project_id = isset($_POST['project_id']) ? $_POST['project_id'] : '';
        $expensehead = isset($_POST['type_id']) ? $_POST['type_id'] : '';
        $date_to = isset($_POST['date_to']) ? $_POST['date_to'] : '';
        $search_item_id = isset($_POST['item_id']) ? $_POST['item_id'] : '';
        $ids_array = array();
        $specification = '';
        if (!empty($project_id)) {
            $specification = $this->actionGetItemCategory($parent = 0, $spacing = '', $user_tree_array = '');
            $warehouse_ids = Warehouse::model()->findAll(array('condition' => 'project_id = ' . $project_id));
            

            if (!empty($warehouse_ids)) {
                foreach ($warehouse_ids as $warehouse) {
                    array_push($ids_array, $warehouse['warehouse_id']);
                }
                $warehouse_ids = implode(',', $ids_array);
                $criteria = new CDbCriteria();
                if (!empty($warehouse_ids)) {

                    $criteria->condition = "warehousestock_warehouseid IN (" . $warehouse_ids . ") AND warehousestock_initial_quantity IS NOT NULL";

                    if (isset($_POST['date_from']) && !empty($_POST['date_from'])) {
                        $date_from = date('Y-m-d', strtotime($_POST['date_from']));
                        $criteria->addCondition("warehousestock_date >= '" . $date_from . "'");
                    } else {
                        $date_from = '';
                        $criteria->compare('warehousestock_date', $date_from, true);
                    }

                    if (isset($_POST['date_to']) && !empty($_POST['date_to'])) {
                        $date_to = date('Y-m-d', strtotime($_POST['date_to']));
                        $criteria->addCondition("warehousestock_date <= '" . $date_to . "'");
                    } else {
                        $date_to = '';
                        $criteria->compare('warehousestock_date', $date_to, true);
                    }

                    if (isset($_POST['item_id']) && !empty($_POST['item_id'])) {
                        $criteria->addCondition("warehousestock_itemid = " . $_POST['item_id']);
                    }

                    if (isset($_POST['type_id']) && !empty($_POST['type_id'])) {
                        $expensehead = $_POST['type_id'];
                    }

                    $criteria->order = 'warehousestock_itemid,warehousestock_date DESC';
                    $receipt_criteria = new CDbCriteria();
                    $receipt_criteria->condition = "warehousereceipt_warehouseid=:warehouse_ids";
                    $receipt_criteria->params = array(':warehouse_ids' => $warehouse_ids);
                }
                $opening_stock_data = Warehousestock::model()->findAll($criteria);
                $receipt_items = $this->receiptDetails($warehouse_ids, $date_from, $date_to, $search_item_id,$expensehead);
                $dispatch_items = $this->dispatchDetails($warehouse_ids, $date_from, $date_to, $search_item_id);

                foreach ($opening_stock_data as $data) {
                    $warehouse_to = $this->openingStockWarehouseId($data['warehousestock_warehouseid']);
                    $item_model = PurchaseCategory::model()->findByPk($data['warehousestock_itemid']);
                    if (!empty($item_model)) {
                        if ($data['warehousestock_initial_quantity'] > 0) {
                            $amount = $data['rate'] * $data['warehousestock_initial_quantity'];
                            $data = ['receipt_deispatch_no' => '-', 'id_val' => '', 'type' => 1, 'date' => $data['warehousestock_date'], 'from' => '-', 'to' => $warehouse_to, 'remark' => '-', 'item' => Controller::itemName($data['warehousestock_itemid']), 'dimension' => $data['dimension'], 'qty' => $data['warehousestock_initial_quantity'], 'rate' => $data['rate'], 'bill_id' => '-', 'item_id' => $data['warehousestock_itemid'], 'unit' => $data['warehousestock_unit'], 'amount' => $amount];
                            array_push($final_data, $data);
                        }
                    }
                }
                foreach ($receipt_items as $receipt_item) {
                    if (!empty($receipt_item['item'])) {
                        $items = $receipt_item['item'];
                        foreach ($items as $item) {
                            $rate = '';
                            $amount = '';
                            if (!empty($item['rate'])) {
                                $rate = $item['rate'];
                            }
                            if (!empty($item['amount'])) {
                                $amount = $item['amount'];
                            }
                            $bill_id = '';
                            if (!empty($receipt_item['bill_id']) || !empty($receipt_item['despatch_id'])) {
                                if (!empty($receipt_item['bill_id'])){
                                    $bill_id = $receipt_item['bill_id'];
                                }
                                
                                if ($item['qty'] > 0) {
                                    if ($receipt_item['despatch_id'] == '') {
                                        $data = ['receipt_deispatch_no' => $receipt_item['warehousereceipt_no'], 'id_val' => $receipt_item['receipt_id'], 'type' => 2, 'date' => $receipt_item['receipt_date'], 'from' => $receipt_item['warehousereceipt_from'], 'to' => $receipt_item['warehouse'], 'remark' => $receipt_item['remarks'], 'item' => $item['item'], 'dimension' => $item['dimension'], 'qty' => $item['qty'], 'rate' => $rate, 'bill_id' => $bill_id, 'item_id' => $item['item_id'], 'unit' => $item['unit'], 'amount' => $amount];
                                    } else {
                                        $data = ['receipt_deispatch_no' => $receipt_item['warehousereceipt_no'], 'id_val' => $receipt_item['receipt_id'], 'type' => 4, 'date' => $receipt_item['receipt_date'], 'from' => $receipt_item['warehousereceipt_from'], 'to' => $receipt_item['warehouse'], 'remark' => $receipt_item['remarks'], 'item' => $item['item'], 'dimension' => $item['dimension'], 'qty' => $item['qty'], 'rate' => $rate, 'bill_id' => $bill_id, 'item_id' => $item['item_id'], 'unit' => $item['unit'], 'amount' => $amount];
                                    }
                                    array_push($final_data, $data);
                                }
                            }
                        }
                    }
                }

                foreach ($dispatch_items as $dispatch_item) {
                    if (!empty($dispatch_item['item'])) {
                        $items = $dispatch_item['item'];
                        foreach ($items as $item) {
                            $amount = '';
                            $multiplying_quantity = 1;
                            if ($item['dimension'] != null) {
                                $multiplying_quantity_arr = explode(" x ", $item['dimension']);
                                $multiplying_quantity = $multiplying_quantity_arr['0'] * $multiplying_quantity_arr['1'];
                            }

                            if (!empty($item['rate'])) {
                                $amount = $item['qty'] * $item['rate'] * $multiplying_quantity;
                            }
                            if ($item['qty'] > 0) {
                                $data = ['id' => 0, 'receipt_deispatch_no' => $dispatch_item['warehousedespatch_no'], 'id_val' => $dispatch_item['dispatch_id'], 'type' => 3, 'date' => $dispatch_item['dispatch_date'], 'from' => $dispatch_item['warehouse'], 'to' => $dispatch_item['warehousedespatch_to'], 'remark' => $item['remark'], 'item' => $item['item'], 'dimension' => $item['dimension'], 'qty' => $item['qty'], 'rate' => $item['rate'], 'bill_id' => '', 'item_id' => $item['item_id'], 'unit' => $item['unit'], 'amount' => $amount];
                                array_push($final_data, $data);
                            }
                        }
                    }
                }
                $sort = array_column($final_data, 'date');
                array_multisort($sort, SORT_DESC, $final_data);
                foreach ($final_data as $dat) {
                    if ($dat['type'] == 2 || $dat['type'] == 1 || $dat['type'] == 4) {
                        if (!empty($dat['amount']))
                            $receipt_total += $dat['amount'];
                    } elseif ($dat['type'] == 3) {
                        if (!empty($dat['amount']))
                            $dispatch_total += $dat['amount'];
                    }
                }
                $net_total = $receipt_total - $dispatch_total;
            }
        }


        foreach ($final_data as $value) {
            $expensehead ='N/A';
            if($value['bill_id']!=""){
                $expensehead = Controller::getExpenseHead($value['bill_id']);
            }
                        
            $dimension = "N/A";
            if ($value['dimension'] != '') {             
                $dimension = $value['dimension'];
            }

            if (array_key_exists(($value['item_id'] . "-" . $dimension), $final_data_array)) {
                $i = key(array_slice($final_data_array[($value['item_id'] . "-" . $dimension)], -1, 1, true));

                $i++;
                $final_data_array[($value['item_id'] . "-" . $dimension)][$i]['receipt_deispatch_no'] = $value['receipt_deispatch_no'];
                $final_data_array[($value['item_id'] . "-" . $dimension)][$i]['id_val'] = $value['id_val'];
                $final_data_array[($value['item_id'] . "-" . $dimension)][$i]['type'] = $value['type'];
                $final_data_array[($value['item_id'] . "-" . $dimension)][$i]['date'] = $value['date'];
                $final_data_array[($value['item_id'] . "-" . $dimension)][$i]['from'] = $value['from'];
                $final_data_array[($value['item_id'] . "-" . $dimension)][$i]['to'] = $value['to'];
                $final_data_array[($value['item_id'] . "-" . $dimension)][$i]['remark'] = $value['remark'];
                $final_data_array[($value['item_id'] . "-" . $dimension)][$i]['item'] = $value['item'];
                $final_data_array[($value['item_id'] . "-" . $dimension)][$i]['qty'] = $value['qty'];
                $final_data_array[($value['item_id'] . "-" . $dimension)][$i]['rate'] = $value['rate'];
                $final_data_array[($value['item_id'] . "-" . $dimension)][$i]['bill_id'] = $value['bill_id'];
                $final_data_array[($value['item_id'] . "-" . $dimension)][$i]['item_id'] = $value['item_id'];
                $final_data_array[($value['item_id'] . "-" . $dimension)][$i]['dimension'] = $value['dimension'];
                $final_data_array[($value['item_id'] . "-" . $dimension)][$i]['unit'] = $value['unit'];
                $final_data_array[($value['item_id'] . "-" . $dimension)][$i]['amount'] = $value['amount'];
                $final_data_array[($value['item_id'] . "-" . $dimension)][$i]['expensehead'] =$expensehead;
            } else {
                $i = 0;
                $final_data_array[($value['item_id'] . "-" . $dimension)] = array();
                $final_data_array[($value['item_id'] . "-" . $dimension)][$i]['receipt_deispatch_no'] = $value['receipt_deispatch_no'];
                $final_data_array[($value['item_id'] . "-" . $dimension)][$i]['id_val'] = $value['id_val'];
                $final_data_array[($value['item_id'] . "-" . $dimension)][$i]['type'] = $value['type'];
                $final_data_array[($value['item_id'] . "-" . $dimension)][$i]['date'] = $value['date'];
                $final_data_array[($value['item_id'] . "-" . $dimension)][$i]['from'] = $value['from'];
                $final_data_array[($value['item_id'] . "-" . $dimension)][$i]['to'] = $value['to'];
                $final_data_array[($value['item_id'] . "-" . $dimension)][$i]['remark'] = $value['remark'];
                $final_data_array[($value['item_id'] . "-" . $dimension)][$i]['item'] = $value['item'];
                $final_data_array[($value['item_id'] . "-" . $dimension)][$i]['qty'] = $value['qty'];
                $final_data_array[($value['item_id'] . "-" . $dimension)][$i]['rate'] = $value['rate'];
                $final_data_array[($value['item_id'] . "-" . $dimension)][$i]['bill_id'] = $value['bill_id'];
                $final_data_array[($value['item_id'] . "-" . $dimension)][$i]['item_id'] = $value['item_id'];
                $final_data_array[($value['item_id'] . "-" . $dimension)][$i]['dimension'] = $value['dimension'];
                $final_data_array[($value['item_id'] . "-" . $dimension)][$i]['unit'] = $value['unit'];
                $final_data_array[($value['item_id'] . "-" . $dimension)][$i]['amount'] = $value['amount'];
                $final_data_array[($value['item_id'] . "-" . $dimension)][$i]['expensehead'] = $expensehead;
            }
        }

        $dataProvider = new CArrayDataProvider($final_data, array(
            'keyField' => 'receipt_deispatch_no',
            'sort' => array(
                'attributes' => array(
                    'warehousestock_id', 'warehousestock_itemid', 'warehousestock_date', 'warehousestock_stock_quantity', 'warehousestock_initial_quantity'
                ),
            ),
            'pagination' => false
        ));

        $this->render('_material_report', array(
            'dataProvider' => $dataProvider,
            'specification' => $specification,
            'result_data' => $final_data_array,
            'project_id' => $project_id,
            'expensehead'=>$expensehead,
            'receipt_total' => $receipt_total,
            'dispatch_total' => $dispatch_total,
            'date_from' => $date_from,
            'date_to' => $date_to,
            'search_item_id' => $search_item_id,
            'net_total' => $net_total
        ));
    }

    public function actionProjectsquantityreport() {
        $tblpx = Yii::app()->db->tablePrefix;
        $project_id = "";
        $model = new Projects;
        $dataProvider = $model->search();
        $itemmodelcat = "";
        $itemmodelnocat = "";
        $dataProvider1 = "";
        $dataProvider2 = "";
        $date_from = "";
        $date_to = "";
        if (isset($_POST['project_id']) && !empty($_POST['project_id'])) {
            $project_id = $_REQUEST['project_id'];

            $sqlProject = "SELECT pr.*,cl.name as clientname "
                    . " FROM {$tblpx}projects pr LEFT JOIN {$tblpx}clients cl "
                    . " ON pr.client_id = cl.cid WHERE pr.pid = {$project_id}";
            $model = Yii::app()->db->createCommand($sqlProject)->queryRow();
            $dataProvider = "";
            $date_from = isset($_POST['date_from']) && !empty($_POST['date_from']) ? date('Y-m-d', strtotime($_POST['date_from'])) : "";
            $date_to = isset($_POST['date_to']) && !empty($_POST['date_to']) ? date('Y-m-d', strtotime($_POST['date_to'])) : "";


            $sqlQueryCat = "SELECT it.*,it.itemestimation_quantity,a.category_name,a.category_id,a.specification,"
                    . " a.brand_name,b.billitemquantity,b.billitemamount,b.billitemtaxamount,b.billitemdiscountamount,"
                    . " c.itemestimationsum,SUM(b.billitemamount),b.billitemtaxamount,b.billitemdiscountamount "
                    . " FROM {$tblpx}itemestimation it"
                    . " LEFT JOIN (SELECT pc.id, pc.category_name, pc1.id as category_id, pc1.specification,br.brand_name "
                    . " FROM {$tblpx}purchase_category pc"
                    . " LEFT JOIN {$tblpx}specification pc1 ON pc.id = pc1.cat_id"
                    . " LEFT JOIN {$tblpx}brand br ON pc1.brand_id = br.id) a ON a.category_id = it.category_id"
                    . " LEFT JOIN (SELECT bi.billitem_id,bi.category_id,SUM(bi.billitem_quantity) "
                    . " as billitemquantity,SUM(bi.billitem_amount) as billitemamount,SUM(bi.billitem_taxamount) "
                    . " as billitemtaxamount,SUM(bi.billitem_discountamount) as billitemdiscountamount "
                    . " FROM {$tblpx}billitem bi"
                    . " LEFT JOIN {$tblpx}bills bl ON bi.bill_id = bl.bill_id"
                    . " LEFT JOIN {$tblpx}purchase pu ON bl.purchase_id = pu.p_id "
                    . " WHERE pu.project_id = {$project_id} GROUP BY bi.category_id) b "
                    . " ON b.category_id = it.category_id"
                    . " LEFT JOIN (SELECT itemestimation_id,SUM(itemestimation_amount) as itemestimationsum "
                    . " FROM {$tblpx}itemestimation WHERE project_id = {$project_id}) as c ON 1 = 1"
                    . " WHERE it.project_id = {$project_id} GROUP BY it.category_id";
            $itemmodelcat = Yii::app()->db->createCommand($sqlQueryCat)->queryAll();
            $catCount = Yii::app()->db->createCommand($sqlQueryCat)->queryScalar();
            $dataProvider1 = new CSqlDataProvider($sqlQueryCat, array(
                'totalItemCount' => $catCount,
                'keyField' => 'itemestimation_id',
                'id' => 'itemestimation_id',
                'sort' => array(
                    'attributes' => array(
                        'itemestimation_id', 'category_id',
                    ),
                ),
                'pagination' => array(
                    'pageSize' => 10,
                ),
            ));
        }

        $this->render('projectsquantityreport', array(
            'model' => $model,
            'dataProvider' => $dataProvider,
            'project_id' => $project_id,
            'itemmodelcat' => $itemmodelcat,
            'dataProvider1' => $dataProvider1,
            'date_from' => $date_from,
            'date_to' => $date_to,
        ));
    }

    public function actionExportItemQuantityReportCSV() {
        $project_id = $_REQUEST["project_id"];
        $date_from = $_REQUEST["fromdate"];
        $date_to = $_REQUEST["todate"];
        $tblpx = Yii::app()->db->tablePrefix;
        $sqlProject = "SELECT pr.*,cl.name as clientname FROM {$tblpx}projects pr LEFT JOIN {$tblpx}clients cl ON pr.client_id = cl.cid WHERE pr.pid = {$project_id}";
        $model = Yii::app()->db->createCommand($sqlProject)->queryRow();
        $sqlQueryCat = "SELECT it.*,it.itemestimation_quantity,a.category_name,a.category_id,"
                . " a.specification,a.brand_name,b.billitemquantity,b.billitemamount,"
                . " b.billitemtaxamount,b.billitemdiscountamount,c.itemestimationsum,"
                . " SUM(b.billitemamount),b.billitemtaxamount,b.billitemdiscountamount "
                . " FROM {$tblpx}itemestimation it"
                . " LEFT JOIN (SELECT pc.id, pc.category_name, pc1.id "
                . " as category_id, pc1.specification,br.brand_name "
                . " FROM {$tblpx}purchase_category pc"
                . " LEFT JOIN {$tblpx}purchase_category pc1 "
                . " ON pc.id = pc1.parent_id "
                . " LEFT JOIN {$tblpx}brand br ON pc1.brand_id = br.id) a "
                . " ON a.category_id = it.category_id "
                . " LEFT JOIN (SELECT bi.billitem_id,bi.category_id,"
                . " SUM(bi.billitem_quantity) as billitemquantity,"
                . " SUM(bi.billitem_amount) as billitemamount,"
                . " SUM(bi.billitem_taxamount) as billitemtaxamount,"
                . " SUM(bi.billitem_discountamount) as billitemdiscountamount "
                . " FROM {$tblpx}billitem bi"
                . " LEFT JOIN {$tblpx}bills bl ON bi.bill_id = bl.bill_id"
                . " LEFT JOIN {$tblpx}purchase pu ON bl.purchase_id = pu.p_id "
                . " WHERE pu.project_id = {$project_id} "
                . " GROUP BY bi.category_id) b ON b.category_id = it.category_id "
                . " LEFT JOIN (SELECT itemestimation_id,"
                . " SUM(itemestimation_amount) as itemestimationsum "
                . " FROM {$tblpx}itemestimation WHERE project_id = {$project_id}) as c ON 1 = 1"
                . " WHERE it.project_id = {$project_id} GROUP BY it.category_id";
        $itemmodelcat = Yii::app()->db->createCommand($sqlQueryCat)->queryAll();
        $arraylabel = array("", "");

        $finaldata[0][] = "Client";
        $finaldata[0][] = $model["clientname"];
        $finaldata[1][] = "Project Name";
        $finaldata[1][] = $model["name"];
        $finaldata[2][] = "Site";
        $finaldata[2][] = $model["site"];

        $finaldata[3][] = "";
        $finaldata[3][] = "";
        $finaldata[3][] = "";

        $finaldata[4][] = "Sl No";
        $finaldata[4][] = "Item Name";
        $finaldata[4][] = "Unit";
        $finaldata[4][] = "Type";
        $finaldata[4][] = "Estimation Quantity";
        $finaldata[4][] = "Estimation Amount";
        $finaldata[4][] = "Actual Quantity";
        $finaldata[4][] = "Actual Amount";
        $finaldata[4][] = "Quantity Variation";
        $finaldata[4][] = "Amount Variation";
        $k = 5;
        $i = 1;
        $billItemGrandTotal = 0;
        foreach ($itemmodelcat as $modelcat) {
            $billitemTotal = ($modelcat["billitemamount"] - $modelcat["billitemdiscountamount"]) + $modelcat["billitemtaxamount"];
            $billItemGrandTotal = $billItemGrandTotal + $billitemTotal;
            if ($modelcat["purchase_type"] == "A") {
                $itemType = "By Length";
            } else if ($modelcat["purchase_type"] == "G") {
                $itemType = "By Width and height";
            } else if ($modelcat["purchase_type"] == "O") {
                $itemType = "By Quantity";
            } else {
                $itemType = "";
            }
            $quantityVariation = $modelcat["itemestimation_quantity"] - $modelcat["billitemquantity"];
            $amountVariation = $modelcat["itemestimation_amount"] - $billitemTotal;
            $finaldata[$k][] = $i;
            $finaldata[$k][] = $modelcat["category_name"] . " - " . $modelcat["brand_name"] . " - " . $modelcat["specification"];
            $finaldata[$k][] = $modelcat["itemestimation_unit"];
            $finaldata[$k][] = $itemType;
            $finaldata[$k][] = $modelcat["itemestimation_quantity"];
            $finaldata[$k][] = $modelcat["itemestimation_amount"];
            $finaldata[$k][] = Controller::money_format_inr($modelcat["billitemquantity"], 2);
            $finaldata[$k][] = Controller::money_format_inr($billitemTotal, 2);
            $finaldata[$k][] = Controller::money_format_inr($quantityVariation, 2);
            $finaldata[$k][] = Controller::money_format_inr($amountVariation, 2);
            $k = $k + 1;
            $i = $i + 1;
        }
        $totalAmountVariation = $itemmodelcat[0]["itemestimationsum"] - $billItemGrandTotal;
        $finaldata[$k][] = " ";
        $finaldata[$k][] = " ";
        $finaldata[$k][] = " ";
        $finaldata[$k][] = " ";
        $finaldata[$k][] = " ";
        $finaldata[$k][] = $itemmodelcat[0]["itemestimationsum"];
        $finaldata[$k][] = " ";
        $finaldata[$k][] = Controller::money_format_inr($billItemGrandTotal, 2);
        $finaldata[$k][] = " ";
        $finaldata[$k][] = Controller::money_format_inr($totalAmountVariation, 2);
        Yii::import('ext.ECSVExport');

        $csv = new ECSVExport($finaldata);

        $csv->setHeaders($arraylabel);
        $csv->setToAppend();
        $contentdata = $csv->toCSV();
        $csvfilename = 'Project Item Quantity Report' . date('d_M_Y') . '.csv';
        Yii::app()->getRequest()->sendFile($csvfilename, $contentdata, "text/csv", false);
        exit();
    }

    public function actionSaveitemquantityreportpdf() {
        $project_id = $_REQUEST["project_id"];
        $date_from = $_REQUEST["fromdate"];
        $date_to = $_REQUEST["todate"];
        $tblpx = Yii::app()->db->tablePrefix;
        $sqlProject = "SELECT pr.*,cl.name as clientname FROM {$tblpx}projects pr LEFT JOIN {$tblpx}clients cl ON pr.client_id = cl.cid WHERE pr.pid = {$project_id}";
        $model = Yii::app()->db->createCommand($sqlProject)->queryRow();
        $sqlQueryCat = "SELECT it.*,it.itemestimation_quantity,a.category_name,a.category_id,"
                . " a.specification,a.brand_name,b.billitemquantity,b.billitemamount,"
                . " b.billitemtaxamount,b.billitemdiscountamount,c.itemestimationsum,"
                . " SUM(b.billitemamount),b.billitemtaxamount,b.billitemdiscountamount "
                . " FROM {$tblpx}itemestimation it"
                . " LEFT JOIN (SELECT pc.id, pc.category_name, pc1.id as category_id, "
                . " pc1.specification,br.brand_name FROM {$tblpx}purchase_category pc"
                . " LEFT JOIN {$tblpx}purchase_category pc1 ON pc.id = pc1.parent_id"
                . " LEFT JOIN {$tblpx}brand br ON pc1.brand_id = br.id) a"
                . " ON a.category_id = it.category_id"
                . " LEFT JOIN (SELECT bi.billitem_id,bi.category_id,SUM(bi.billitem_quantity) "
                . " as billitemquantity,SUM(bi.billitem_amount) as billitemamount,"
                . " SUM(bi.billitem_taxamount) as billitemtaxamount,SUM(bi.billitem_discountamount) "
                . " as billitemdiscountamount FROM {$tblpx}billitem bi"
                . " LEFT JOIN {$tblpx}bills bl ON bi.bill_id = bl.bill_id"
                . " LEFT JOIN {$tblpx}purchase pu ON bl.purchase_id = pu.p_id "
                . " WHERE pu.project_id = {$project_id} GROUP BY bi.category_id) b"
                . " ON b.category_id = it.category_id"
                . " LEFT JOIN (SELECT itemestimation_id,SUM(itemestimation_amount) as itemestimationsum "
                . "FROM {$tblpx}itemestimation WHERE project_id = {$project_id}) as c ON 1 = 1"
                . " WHERE it.project_id = {$project_id} GROUP BY it.category_id";
        $itemmodelcat = Yii::app()->db->createCommand($sqlQueryCat)->queryAll();

        $mPDF1 = Yii::app()->ePdf->mPDF();
        $mPDF1 = Yii::app()->ePdf->mPDF('', 'A4');

        $mPDF1->WriteHTML($this->renderPartial('saveitemquantityreportpdf', array(
                    'model' => $model,
                    'itemmodelcat' => $itemmodelcat,
                    'itemmodelnocat' => $itemmodelnocat,
                        ), true));
        $mPDF1->Output('Item Estimation Report' . '.pdf', 'D');
    }

    public function actionremoveproject() {

        $id = $_POST['pjId'];
        $model = $this->loadModel($id);
        $transaction = Yii::app()->db->beginTransaction();
        try {
            if (!$model->delete()) {
                $success_status = 0;
                throw new Exception(json_encode($model->getErrors()));
            } else {
                $success_status = 1;
            }
            $transaction->commit();
        } catch (Exception $error) {
            $transaction->rollBack();
            $success_status = 0;
        } finally {
            if ($success_status == 1) {
                echo json_encode(array('response' => 'success', 'msg' => 'Data Deleted Successfully'));
            } else {
                if ($error->errorInfo[1] == 1451) {
                    echo json_encode(array('response' => 'warning', 'msg' => 'Cannot Delete ! This record alredy in use.'));
                } else {
                    echo json_encode(array('response' => 'error', 'msg' => 'Some Error Occured'));
                }
            }
        }
    }

    public function itemStock($warehouse_ids, $date_from, $date_to) {
        $tblpx = Yii::app()->db->tablePrefix;
        $stock_array = array();
        $criteria = new CDbCriteria();
        if (!empty($warehouse_ids)) {
            $criteria->condition = "warehousestock_warehouseid IN (".$warehouse_ids.")";            
            $criteria->order = 'warehousestock_id DESC';            
        }
      
        if (!empty($date_from) && !empty($date_to)) {
            $date_from = date('Y-m-d', strtotime($date_from));
            $date_to = date('Y-m-d', strtotime($date_to));
            $criteria->addCondition('warehousestock_date >= "' . $date_from . '" AND warehousestock_date <= "' . $date_to . '"');
        }      
        $stock_datas = Warehousestock::model()->findAll($criteria);
        $qty_total = 0;
        $total_amount = 0;
       
        foreach ($stock_datas as $stock_data) {
            if($stock_data['dimension'] != null){
                $multiplying_quantity_arr = explode(" x ",$stock_data['dimension']);
                $multiplying_quantity = $multiplying_quantity_arr['0'] * $multiplying_quantity_arr['1'];
            }else{
                $multiplying_quantity =1;
            }
            if($stock_data['dimension'] == ''){
                $dimension = "N/A";
            }else{
                $dimension = $stock_data['dimension'];
            }
            $stock_key = $stock_data['warehousestock_itemid']."_".$dimension;
            
            if ($stock_data['warehousestock_stock_quantity'] > 0) {
                if (array_key_exists($stock_key, $stock_array)) {
                    $item_name = Controller::itemName($stock_data['warehousestock_itemid']);
                    $amount = $stock_data['warehousestock_stock_quantity'] * $stock_data['rate'] * $multiplying_quantity;
                    $qty = $stock_array[$stock_key]['qty'] + $stock_data['warehousestock_stock_quantity'];
                    $qty_total += $stock_data['warehousestock_stock_quantity'];
                    $total_amount += $amount ;
                    $stock_array[$stock_key] = ['qty' => $qty,'dimension' => $dimension, 'unit' => $stock_data['warehousestock_unit'], 'amount' => $amount + $stock_array[$stock_key]['amount'], 'item' => $item_name];
                } else {
                
                    $item_model = Specification::model()->findByPk($stock_data['warehousestock_itemid']);
                    if (!empty($item_model)) {
                        $item_name = Controller::itemName($stock_data['warehousestock_itemid']);
                        $amount = $stock_data['warehousestock_stock_quantity'] * $stock_data['rate']* $multiplying_quantity;
                        $qty_total += $stock_data['warehousestock_stock_quantity'];
                        $total_amount += $amount;
                        $stock_array[$stock_key] = ['qty' => $stock_data['warehousestock_stock_quantity'],'dimension' => $dimension, 'unit' => $stock_data['warehousestock_unit'], 'amount' => $amount, 'item' => $item_name,];
                    }
                }
            }
        }
        $sort_data = array_column($stock_array, 'item');
        array_multisort($sort_data, SORT_ASC, $stock_array);        
        $stock_array['total_qty'] = $qty_total;
        $stock_array['total_amount'] = number_format((float)$total_amount, 2, '.', '');        
        return $stock_array;
    }

    public function actionGetTemplateDetails() {
        $template_ids = $_POST['template_ids'];  // Array of selected template IDs
        $type = isset($_POST['type']) ? $_POST['type'] : 1;
        $project = isset($_POST['project']) ? $_POST['project'] : '';
    
        $html = ''; // Variable to store the combined HTML
    
        // Loop through each template ID and fetch its details
        foreach ($template_ids as $template_id) {
            // Render partial view for each template and append to the $html
            $html .= $this->renderPartial('_template_labour', array('template_id' => $template_id, 'type' => $type, 'project' => $project), true);
        }
    
        // Return the combined HTML for all templates
        echo $html;
    }
    


}
