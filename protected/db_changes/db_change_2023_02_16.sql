ALTER TABLE `jp_deletepending` CHANGE `deletepending_status` `deletepending_status` INT(11) NOT NULL DEFAULT '0' COMMENT '0=>pending,1=>approved,2=cancelled';

UPDATE jp_invoice i LEFT JOIN jp_deletepending d ON d.deletepending_parentid=i.invoice_id  
SET i.delete_approve_status = "0"  
WHERE deletepending_table="jp_invoice" AND deletepending_status=2;    


UPDATE jp_quotation q LEFT JOIN jp_deletepending d ON d.deletepending_parentid= q.quotation_id  
SET q.delete_approve_status = "0"  
WHERE deletepending_table="jp_quotation" AND deletepending_status=2;

ALTER TABLE `jp_pre_dailyreport` CHANGE `works_done` `works_done` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;
ALTER TABLE `jp_pre_dailyreport` CHANGE `materials_unlade` `materials_unlade` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;

ALTER TABLE `jp_pre_dailyreport` CHANGE `extra_work_done` `extra_work_done` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;

ALTER TABLE `jp_pre_dailyreport` CHANGE `labour_wage` `labour_wage` FLOAT(20,2) NULL DEFAULT NULL;
ALTER TABLE `jp_pre_dailyreport` CHANGE `helper_wage` `helper_wage` FLOAT(20,2) NULL DEFAULT NULL;
ALTER TABLE `jp_pre_dailyreport` CHANGE `approved_by` `approved_by` INT(11) NULL DEFAULT NULL;
ALTER TABLE `jp_pre_dailyreport` CHANGE `record_action` `record_action` VARCHAR(300) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;
ALTER TABLE `jp_pre_dailyreport` CHANGE `cancelled_at` `cancelled_at` DATETIME NULL DEFAULT NULL;

UPDATE jp_warehousereceipt w LEFT JOIN
       jp_bills b
       ON b.bill_id = w.warehousereceipt_bill_id
    SET warehousereceipt_bill_id = NULL  
WHERE b.bill_id IS NULL;


ALTER TABLE `jp_warehousereceipt` ADD CONSTRAINT `fk_warehousereceipt_bill_id1` FOREIGN KEY (`warehousereceipt_bill_id`) REFERENCES `jp_bills`(`bill_id`) ON DELETE RESTRICT ON UPDATE CASCADE;

CREATE TABLE `jp_additional_po_charge` (`id` INT(11) NOT NULL AUTO_INCREMENT , `purchase_id` INT(11) NOT NULL , `category` VARCHAR(200) NOT NULL , `amount` FLOAT(11,2) NOT NULL , `created_by` INT(11) NOT NULL , `created_date` DATETIME NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;


ALTER TABLE `jp_scquotation` ADD `sgst_percent` FLOAT(11,2) NULL DEFAULT NULL AFTER `scquotation_no`, ADD `cgst_percent` FLOAT(11,2) NULL DEFAULT NULL AFTER `sgst_percent`, ADD `igst_percent` FLOAT(11,2) NULL DEFAULT NULL AFTER `cgst_percent`;
