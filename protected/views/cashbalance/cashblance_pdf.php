<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function sortByDate($a, $b)
{
    $a = $a['payment_date'];
    $b = $b['payment_date'];

    if ($a == $b) return 0;
    return ($a > $b) ? -1 : 1;
}
?>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/pdfstyle.css">

<?php
    $company_address = Company::model()->findBypk(($company_id)?$company_id:Yii::app()->user->company_id);	
?>
    
    <h3 class="text-center">Cash Balance Report</h3>
        <div class="container-fluid pdf_spacing">            
            <?php
                $tblpx = Yii::app()->db->tablePrefix;
                $heading = '';
                if (empty($date_from) && empty($date_to)) {
                    $heading    .= date('d-m-Y');
                } else{
                   if (!empty($date_from) && !empty($date_to)) {
                            $heading    .= date('d-M-Y', strtotime($date_from)).' to '.date('d-M-Y', strtotime($date_to));
                    } else {
                            if (!empty($date_from)) {
                                    $heading    .= date('d-M-Y', strtotime($date_from));
                            }
                            if (!empty($date_to)) {
                                    $heading    .= date('d-M-Y', strtotime($date_to));
                            }
                    }
                }
            ?>
            <h4>Cash In-Hand Report : <?php echo $heading; ?></h4>	
            <table class="table table-bordered table-striped" border="1">
                <?php
                $where = '';

                $user = Users::model()->findByPk(Yii::app()->user->id);
                $arrVal = explode(',', $user->company_id);
                $newQuery = "";
                foreach($arrVal as $arr) {
                    if ($newQuery) $newQuery .= ' OR';
                    $newQuery .= " FIND_IN_SET('".$arr."', company_id)";
                }
                if (empty($date_from) && empty($date_to)) {
                        $where .= " AND date ='".date("Y-m-d")."'";
                } else {
                    if (!empty($date_from) && !empty($date_to)) {
                            $where .= " AND date between '" . $date_from ."' and'" . $date_to . "'";
                    } else{
                        if (!empty($date_from)) {
                                $where .= " AND date >= '" . $date_from ."'";
                        }
                        if (!empty($date_to)) {
                                $where .= " AND date <= '" . $date_to . "'";
                        }
                    }
                } 
                $data_array = array();
                // daybook

                $daybook_data=  Yii::app()->db->createCommand("SELECT * FROM {$tblpx}expenses WHERE (".$newQuery.") ".$where." AND (expense_type=89 OR payment_type=89 OR expense_type=103) AND subcontractor_id IS NULL")->queryAll();
                foreach($daybook_data as $key=> $value){
                    $data_array[$key]['payment_type'] = 'Daybook';
                    $data_array[$key]['expense_amount'] = $value['paid'];
                    $data_array[$key]['receipt_amount'] = $value['receipt'];
                    $data_array[$key]['payment_date'] = $value['date'];
                    if($value['type'] == 73){
                        $vendor = Vendors::model()->findByPk($value['vendor_id']);
                        $vendor_name = $vendor->name;
                        if($value['bill_id'] != NULL){
                            $bills = Bills::model()->findByPk($value['bill_id']);
                            $expense_head = ExpenseType::model()->findByPk($value['exptype']);
                            $bill_thead  = $bills->bill_number.' / '.$expense_head->type_name;
                        }else{
                            $expense_head = ExpenseType::model()->findByPk($value['exptype']);
                            $bill_thead  = $expense_head->type_name;
                        }
                    }else{
                        $vendor_name = '';
                        $bill_thead  = '';
                    }
                    $data_array[$key]['vendor'] = $vendor_name;
                    $data_array[$key]['description'] = $value['description'];
                    $data_array[$key]['bill_exphead'] = $bill_thead;
                    $data_array[$key]['company_id'] = $value['company_id'];
                }
                        
                // dailyexpense
                $dailyexpense_data = array();

                //$dailyexpense_data=  Yii::app()->db->createCommand("SELECT * FROM {$tblpx}dailyexpense WHERE (".$newQuery.") ".$where." AND (expense_type=89 OR dailyexpense_receipt_type=89 OR expense_type=103 OR dailyexpense_receipt_type=103)")->queryAll();
                $dailyexpense_data=  Yii::app()->db->createCommand("SELECT * FROM {$tblpx}dailyexpense WHERE (".$newQuery.") ".$where." AND (expense_type=89 OR dailyexpense_receipt_type=89)")->queryAll();
                $a = count($daybook_data);
                foreach($dailyexpense_data as $key=> $value){
                    $data_array[$a]['payment_type'] = 'Dailyexpense';
                    $data_array[$a]['expense_amount'] = $value['dailyexpense_paidamount'];
                    $data_array[$a]['receipt_amount'] = $value['dailyexpense_receipt'];
                    $data_array[$a]['payment_date'] = $value['date'];
                    if($value["dailyexpense_type"] == "deposit") {
                        $depositId = $value["expensehead_id"];
                        $deposit   = Deposit::model()->findByPk($depositId);
                        $expense_head = $deposit->deposit_name;
                    } else if($value["dailyexpense_type"] == "expense") {
                        $expense   = Companyexpensetype::model()->findByPk($value['expensehead_id']);
                        $expense_head = $expense->name;
                    } else {
                        $receipt   = Companyexpensetype::model()->findByPk($value['exp_type_id']);
                        $expense_head = $receipt->name;
                    }
                    if($value['bill_id'] != NULL){
                        $bill_thead  =  $value['bill_id'].' / '.$expense_head;
                    } else {
                        $bill_thead  = $expense_head;
                    }
                            
                    $data_array[$a]['vendor'] = '';
                    $data_array[$a]['description'] = $value['description'];
                    $data_array[$a]['bill_exphead'] = $bill_thead;
                    $data_array[$a]['company_id'] = $value['company_id'];
                    $a++;
                }

                // Vendor payment
                $vendorpayment_data = array();
                $vendorpayment_data=  Yii::app()->db->createCommand("SELECT * FROM {$tblpx}dailyvendors WHERE (".$newQuery.") ".$where." AND payment_type=89")->queryAll();
                $b = count($daybook_data)+count($dailyexpense_data);
                foreach($vendorpayment_data as $key=> $value){
                    $data_array[$b]['payment_type'] = 'Vendor  Payment';
                    $data_array[$b]['expense_amount'] = $value['amount']+$value['tax_amount'];
                    $data_array[$b]['receipt_amount'] = '';
                    $data_array[$b]['payment_date'] = $value['date'];
                    $vendor = Vendors::model()->findByPk($value['vendor_id']);
                    $vendor_name = $vendor->name;
                    $data_array[$b]['vendor'] = $vendor_name;
                    $data_array[$b]['description'] = $value['description'];
                    $data_array[$b]['bill_exphead'] = '';
                    $data_array[$b]['company_id'] = $value['company_id'];
                    $b++;
                }

                // Subcontractor payment
                $subcontractorpayment_data = array();
                $subcontractorpayment_data=  Yii::app()->db->createCommand("SELECT * FROM {$tblpx}subcontractor_payment WHERE (".$newQuery.") ".$where." AND payment_type=89 AND approve_status ='Yes'")->queryAll();
                $c = count($daybook_data)+count($dailyexpense_data)+count($vendorpayment_data);
                foreach($subcontractorpayment_data as $key=> $value){
                    $data_array[$c]['payment_type'] = 'Subcontractor  Payment';
                    $data_array[$c]['expense_amount'] = $value['amount']+$value['tax_amount'];
                    $data_array[$c]['receipt_amount'] = '';
                    $data_array[$c]['payment_date'] = $value['date'];
                    $data_array[$c]['vendor'] = '';
                    $data_array[$c]['description'] = $value['description'];
                    $data_array[$c]['bill_exphead'] = '';
                    $data_array[$c]['company_id'] = $value['company_id'];
                    $c++;
                }
                //exit();
                $total_expense = 0;
                $total_recept  = 0;
                if (!empty($data_array)){
                    $total_expense  = array_sum(array_column($data_array, 'expense_amount'));
                    $total_recept   = array_sum(array_column($data_array, 'receipt_amount'));
                }          
                usort($data_array, 'sortByDate');
                ?>
                <thead>
                    <tr>
                        <th>Sl No.</th>
                        <th>Payment Name</th>
                        <th>Company</th>
                        <th>Bill No. / Transaction head</th>
                        <th>Vendors</th>
                        <th>Description</th>
                        <th>Payment date</th>
                        <th>Expense Amount</th>
                        <th>Receipt Amounts</th>
                    </tr>
                    <tr>
                        <th colspan="7" style="text-align: right;">Total</th>
                        <th style="text-align: right;"><?php echo Controller::money_format_inr($total_expense,2) ?></th>
                        <th style="text-align: right;"><?php echo Controller::money_format_inr($total_recept,2) ?></th>
                    </tr>
                </tfoot>
                <tbody>
                <?php
                if (!empty($data_array)){
                    foreach($data_array as $key=> $data){
                        $total_expense += $data['expense_amount'];
                        $total_recept += $data['receipt_amount'];
                    ?>    
                        <tr>
                            <td><?php echo $key+1 ?></td>
                            <td><?php echo $data['payment_type']; ?></td>
                            <td>
                                <?php
                                $company = Company::model()->findByPk($data['company_id']);
                                echo $company->name;
                                ?>
                            </td>
                            <td><?php echo $data['bill_exphead']; ?></td>
                            <td><?php echo $data['vendor']; ?></td>
                            <td><?php echo $data['description']; ?></td>
                            <td><?php echo date('d-m-Y',strtotime($data['payment_date'])); ?></td>
                            <td align="right"><?php echo ($data['expense_amount'] !=0)?Controller::money_format_inr($data['expense_amount'],2):'0'; ?></td>
                            <td align="right"><?php echo ($data['receipt_amount'] !=0)?Controller::money_format_inr($data['receipt_amount'],2):'0'; ?></td>
                        </tr>
                    <?php } ?>
                 </tbody>
            <?php } else{  ?>
                
                <tr>
                     <td colspan="9" class="text-center">No data found</td>
                 </tr>
            <?php } ?>
             </table>
            <br/>

			<?php
			if($date_from !='' || $date_to !='') { 
                                $newQuery = "";
                                $user = Users::model()->findByPk(Yii::app()->user->id);
                                $arrVal = explode(',', $user->company_id);
                                foreach($arrVal as $arr) {
                                    if ($newQuery) $newQuery .= ' OR';
                                    $newQuery .= " FIND_IN_SET('".$arr."', {$tblpx}cashbalance.company_id)";
                                }
				$heading = '';
				if (!empty($date_from) && !empty($date_to)) {
					$heading    .= date('d-M-Y', strtotime($date_from)).' to '.date('d-M-Y', strtotime($date_to));
				} else {
					if (!empty($date_from)) {
						$heading    .= date('d-M-Y', strtotime($date_from));
					}
					if (!empty($date_to)) {
						$heading    .= date('d-M-Y', strtotime($date_to));
					}
				}
				
				$cash_blance = Yii::app()->db->createCommand("SELECT {$tblpx}status.caption,{$tblpx}cashbalance.cashbalance_type,{$tblpx}cashbalance.bank_id,{$tblpx}bank.bank_name,{$tblpx}cashbalance.cashbalance_opening_balance,{$tblpx}cashbalance.cashbalance_deposit,{$tblpx}cashbalance.cashbalance_withdrawal,{$tblpx}cashbalance.cashbalance_closing_balance,{$tblpx}cashbalance.cashbalance_date,{$tblpx}cashbalance.company_id FROM {$tblpx}cashbalance LEFT JOIN {$tblpx}bank ON {$tblpx}cashbalance.bank_id={$tblpx}bank.bank_id LEFT JOIN {$tblpx}status ON {$tblpx}cashbalance.cashbalance_type={$tblpx}status.sid WHERE (".$newQuery.") ORDER BY bank_id ASC")->queryAll();
				if(!empty($cash_blance)){
			?>
			<h4>Balance Report : <?php echo $heading; ?></h4>	
			 <table class="table table-bordered table-striped" border="1">

                <thead>
                    <tr>
                        <th>Sl No.</th>
                        <th>Company</th>
                        <th>Cash Balance Type</th>
                        <th>Bank</th>
                        <th>Date</th>
                        <th>Opening Balance</th>
                        <th>Expense</th>
                        <th>Receipt</th>
                        <th>Closing Balance</th>
                    </tr>
                </thead>
                 <tbody>
					 <?php
					 $tblpx = Yii::app()->db->tablePrefix;

					  foreach($cash_blance as $key=> $value){
						$current_date = date("Y-m-d");  
						$tblpx = Yii::app()->db->tablePrefix;
						$where_bank = '';
						$where_hand = '';
						$where_main = '';
                                                $opening_hand = '';
                                                $opening_bank = '';
						if (!empty($date_from) && !empty($date_to)) {
                                                        $opening_date = date('Y-m-d', strtotime('-1 day', strtotime($date_from)));
							$where_main .= " AND {$tblpx}cashbalance.cashbalance_date between '" . $date_from ."' and'" . $date_to . "'";
							$where_bank .= " AND reconciliation_date between '" . $date_from ."' and'" . $date_to . "'";
							$where_hand .= " AND date between '" . $date_from ."' and'" . $date_to . "'";
                                                        $opening_hand .= " AND date <= '" . $opening_date ."'";
                                                        $opening_bank .= " AND reconciliation_date <= '" . $opening_date ."'";
						} else {
							if (!empty($date_from)) {
                                                                $opening_date = date('Y-m-d', strtotime('-1 day', strtotime($date_from)));
								$date_from = date('Y-m-d', strtotime($date_from));
								$where_main .= " AND {$tblpx}cashbalance.cashbalance_date >= '" . $date_from ."'";
								$where_bank .= " AND reconciliation_date between '" . $date_from ."' and'" . $current_date . "'";
								$where_hand .= " AND date between '" . $date_from ."' and'" . $current_date . "'";
                                                                $opening_hand .= " AND date <= '" . $opening_date ."'";
                                                                $opening_bank .= " AND reconciliation_date <= '" . $opening_date ."'";
							}
							if (!empty($date_to)) {
                                                                $opening_date = date('Y-m-d', strtotime('-1 day', strtotime($date_to)));
								$date_to = date('Y-m-d', strtotime($date_to));
								$where_main .= " AND {$tblpx}cashbalance.cashbalance_date <= '" . $date_to . "'";
								$where_bank .= " AND reconciliation_date between '" . $value['cashbalance_date'] ."' and'" . $date_to . "'";
								$where_hand .= " AND date between '" . $value['cashbalance_date'] ."' and'" . $date_to . "'";
                                                                $opening_hand .= " AND date <= '" . $opening_date . "'";
                                                                $opening_bank .= " AND reconciliation_date <= '" . $opening_date . "'";
							}
						}    
						  
						if($value['bank_id'] !=''){
						// deposit
							
						$daybook=  Yii::app()->db->createCommand("SELECT SUM(receipt) as total_amount FROM {$tblpx}expenses WHERE reconciliation_status = 1 ".$where_bank." AND payment_type=88 AND bank_id=".$value['bank_id']." AND type=72 AND company_id=".$value['company_id']."")->queryRow();
						$dailyexpense=  Yii::app()->db->createCommand("SELECT SUM(dailyexpense_receipt) as total_amount FROM {$tblpx}dailyexpense WHERE reconciliation_status = 1 ".$where_bank." AND dailyexpense_receipt_type=88 AND bank_id=".$value['bank_id']." AND exp_type=72 AND company_id=".$value['company_id']."")->queryRow();
						$daybook_check = $daybook['total_amount'];
					    $dailyexpense_check = $dailyexpense['total_amount'];
						//$deposit = $daybook_check+$dailyexpense_check+$value['cashbalance_deposit'];
						$deposit = $daybook_check+$dailyexpense_check;
						
						
						//withdrawal
						$daybook_with=  Yii::app()->db->createCommand("SELECT SUM(paid) as total_amount FROM {$tblpx}expenses WHERE reconciliation_status = 1 ".$where_bank." AND expense_type=88 AND bank_id=".$value['bank_id']." AND type=73 AND company_id=".$value['company_id']."")->queryRow();
						$dailyexpense_with=  Yii::app()->db->createCommand("SELECT SUM(dailyexpense_paidamount) as total_amount FROM {$tblpx}dailyexpense WHERE reconciliation_status = 1 ".$where_bank." AND expense_type=88 AND bank_id=".$value['bank_id']." AND exp_type=73 AND company_id=".$value['company_id']."")->queryRow();
						$daybook_with_amount = $daybook_with['total_amount'];
                                                $dailyexpense_with_amount = $dailyexpense_with['total_amount'];
					    
						$dailyvendors=  Yii::app()->db->createCommand("SELECT SUM( IFNULL(amount, 0) + IFNULL(tax_amount, 0) ) as total_amount FROM {$tblpx}dailyvendors WHERE reconciliation_status = 1 ".$where_bank." AND payment_type=88  AND bank=".$value['bank_id']." AND company_id=".$value['company_id']."")->queryRow();  
						$subcontractor=  Yii::app()->db->createCommand("SELECT SUM( IFNULL(amount, 0) + IFNULL(tax_amount, 0) ) as total_amount FROM {$tblpx}subcontractor_payment WHERE reconciliation_status = 1 ".$where_bank." AND payment_type=88  AND bank=".$value['bank_id']." AND company_id=".$value['company_id']."")->queryRow();  
					    
                                                $dailyvendors_check = $dailyvendors['total_amount'];
                                                $subcontractor_check = $subcontractor['total_amount'];    

                                                //$withdrawal = $dailyvendors_check+$subcontractor_check+$daybook_with_amount+$dailyexpense_with_amount+$value['cashbalance_withdrawal'];
                                                //$withdrawal = $dailyvendors_check+$subcontractor_check+$daybook_with_amount+$dailyexpense_with_amount;
                                                $withdrawal = $dailyvendors_check+$daybook_with_amount+$dailyexpense_with_amount;
                                                
                                                
                                                
                                                // opening balance calculation
                                                
                                                // deposit
                                                $daybook_opening=  Yii::app()->db->createCommand("SELECT SUM(receipt) as total_amount FROM {$tblpx}expenses WHERE reconciliation_status = 1 ".$opening_bank." AND payment_type=88 AND bank_id=".$value['bank_id']." AND type=72 AND company_id=".$value['company_id']."")->queryRow();
						$dailyexpense_opening=  Yii::app()->db->createCommand("SELECT SUM(dailyexpense_receipt) as total_amount FROM {$tblpx}dailyexpense WHERE reconciliation_status = 1 ".$opening_bank." AND dailyexpense_receipt_type=88 AND bank_id=".$value['bank_id']." AND exp_type=72 AND company_id=".$value['company_id']."")->queryRow();
						$daybook_check_opening = $daybook_opening['total_amount'];
                                                $dailyexpense_check_opening = $dailyexpense_opening['total_amount'];
						$deposit_opening = $daybook_check_opening+$dailyexpense_check_opening;
                                                
//                                                if($deposit_opening ==0){
//                                                     $deposit_opening = $value['cashbalance_opening_balance'];
//                                                 }
                                                
                                                //withdrawal
						$daybook_with_opening=  Yii::app()->db->createCommand("SELECT SUM(paid) as total_amount FROM {$tblpx}expenses WHERE reconciliation_status = 1 ".$opening_bank." AND expense_type=88 AND bank_id=".$value['bank_id']." AND type=73 AND company_id=".$value['company_id']."")->queryRow();
						$dailyexpense_with_opening=  Yii::app()->db->createCommand("SELECT SUM(dailyexpense_paidamount) as total_amount FROM {$tblpx}dailyexpense WHERE reconciliation_status = 1 ".$opening_bank." AND expense_type=88 AND bank_id=".$value['bank_id']." AND exp_type=73 AND company_id=".$value['company_id']."")->queryRow();
						$daybook_with_amount_opening = $daybook_with_opening['total_amount'];
                                                $dailyexpense_with_amount_opening = $dailyexpense_with_opening['total_amount'];
					    
						$dailyvendors_opening=  Yii::app()->db->createCommand("SELECT SUM( IFNULL(amount, 0) + IFNULL(tax_amount, 0) ) as total_amount FROM {$tblpx}dailyvendors WHERE reconciliation_status = 1 ".$opening_bank." AND payment_type=88  AND bank=".$value['bank_id']." AND company_id=".$value['company_id']."")->queryRow();  
					    
                                                $dailyvendors_check_opening = $dailyvendors_opening['total_amount'];

                                                $withdrawal_opening = $dailyvendors_check_opening+$daybook_with_amount_opening+$dailyexpense_with_amount_opening;
                                                
                                                $opening_balance = ($deposit_opening+$value['cashbalance_opening_balance'])-$withdrawal_opening;
					   } else{
						 
						 //deposit
						 $daybook_cah=  Yii::app()->db->createCommand("SELECT SUM(receipt) as total_amount FROM {$tblpx}expenses WHERE payment_type=89 AND type=72 ".$where_hand." AND company_id=".$value['company_id']."")->queryRow(); 
						 $daybook_cash = $daybook_cah['total_amount'];
						 //$dailyexpense_cah=  Yii::app()->db->createCommand("SELECT SUM(dailyexpense_receipt) as total_amount FROM {$tblpx}dailyexpense WHERE (dailyexpense_receipt_type=89 OR dailyexpense_receipt_type=103) AND exp_type=72 ".$where_hand." AND company_id=".$value['company_id']."")->queryRow();
                                                 $dailyexpense_cah=  Yii::app()->db->createCommand("SELECT SUM(dailyexpense_receipt) as total_amount FROM {$tblpx}dailyexpense WHERE (dailyexpense_receipt_type=89) AND exp_type=72 ".$where_hand." AND company_id=".$value['company_id']."")->queryRow(); 
						 $dailyexpense_cash = $dailyexpense_cah['total_amount'];
						 
						 //$deposit = $daybook_cash+ $dailyexpense_cash+$value['cashbalance_deposit'];
						 $deposit = $daybook_cash+ $dailyexpense_cash;
						 // withdrawal
						 
						 $daybook_with=  Yii::app()->db->createCommand("SELECT SUM(paid) as total_amount FROM {$tblpx}expenses WHERE (expense_type=89 OR expense_type=103) AND type=73 ".$where_hand." AND company_id=".$value['company_id']."")->queryRow(); 
						 $daybook_with_amount = $daybook_with['total_amount'];
						 //$dailyexpense_with=  Yii::app()->db->createCommand("SELECT SUM(dailyexpense_paidamount) as total_amount FROM {$tblpx}dailyexpense WHERE (expense_type=89 OR expense_type=103) AND exp_type=73 ".$where_hand." AND company_id=".$value['company_id']."")->queryRow();
                                                 $dailyexpense_with=  Yii::app()->db->createCommand("SELECT SUM(dailyexpense_paidamount) as total_amount FROM {$tblpx}dailyexpense WHERE (expense_type=89) AND exp_type=73 ".$where_hand." AND company_id=".$value['company_id']."")->queryRow(); 
						 $dailyexpense_with_amount = $dailyexpense_with['total_amount'];
						 
						 
						 $dailyvendors_cah=  Yii::app()->db->createCommand("SELECT SUM( IFNULL(amount, 0) + IFNULL(tax_amount, 0) ) as total_amount FROM {$tblpx}dailyvendors WHERE payment_type=89 ".$where_hand." AND company_id=".$value['company_id']."")->queryRow(); 
						 $dailyvendors_cash = $dailyvendors_cah['total_amount'];
						 $subcontractor_cah=  Yii::app()->db->createCommand("SELECT SUM( IFNULL(amount, 0) + IFNULL(tax_amount, 0) ) as total_amount FROM {$tblpx}subcontractor_payment WHERE payment_type=89 ".$where_hand." AND company_id=".$value['company_id']."")->queryRow(); 
						 $subcontractor_cash = $subcontractor_cah['total_amount']; 
						
						 //$withdrawal = $dailyvendors_cash+$subcontractor_cash+$daybook_with_amount+$dailyexpense_with_amount+$value['cashbalance_withdrawal'];
						 //$withdrawal = $dailyvendors_cash+$subcontractor_cash+$daybook_with_amount+$dailyexpense_with_amount;
						 $withdrawal = $dailyvendors_cash+$daybook_with_amount+$dailyexpense_with_amount;  
                                                 
                                                 
                                                 // opening balance calculation
                                                 
                                                 // deposit
                                                 $daybook_cah_opening=  Yii::app()->db->createCommand("SELECT SUM(receipt) as total_amount FROM {$tblpx}expenses WHERE payment_type=89 AND type=72 ".$opening_hand." AND company_id=".$value['company_id']."")->queryRow(); 
						 $daybook_cash_opening = $daybook_cah_opening['total_amount'];
						 //$dailyexpense_cah_opening=  Yii::app()->db->createCommand("SELECT SUM(dailyexpense_receipt) as total_amount FROM {$tblpx}dailyexpense WHERE (dailyexpense_receipt_type=89 OR dailyexpense_receipt_type=103) AND exp_type=72 ".$opening_hand." AND company_id=".$value['company_id']."")->queryRow();
                                                 $dailyexpense_cah_opening=  Yii::app()->db->createCommand("SELECT SUM(dailyexpense_receipt) as total_amount FROM {$tblpx}dailyexpense WHERE (dailyexpense_receipt_type=89) AND exp_type=72 ".$opening_hand." AND company_id=".$value['company_id']."")->queryRow(); 
						 $dailyexpense_cash_opening = $dailyexpense_cah_opening['total_amount'];
                                                 $deposit_opening = $daybook_cash_opening+ $dailyexpense_cash_opening;
                                                 
//                                                 if($deposit_opening ==0){
//                                                     $deposit_opening = $value['cashbalance_opening_balance'];
//                                                 }
//                                                 
                                                 // withdrawal
						 
						 $daybook_with_opening=  Yii::app()->db->createCommand("SELECT SUM(paid) as total_amount FROM {$tblpx}expenses WHERE (expense_type=89 OR expense_type = 103) AND type=73 ".$opening_hand." AND company_id=".$value['company_id']."")->queryRow(); 
						 $daybook_with_amount_opening = $daybook_with_opening['total_amount'];
						 //$dailyexpense_with_opening=  Yii::app()->db->createCommand("SELECT SUM(dailyexpense_paidamount) as total_amount FROM {$tblpx}dailyexpense WHERE (expense_type=89 OR expense_type=103) AND exp_type=73 ".$opening_hand." AND company_id=".$value['company_id']."")->queryRow(); 
                                                 $dailyexpense_with_opening=  Yii::app()->db->createCommand("SELECT SUM(dailyexpense_paidamount) as total_amount FROM {$tblpx}dailyexpense WHERE (expense_type=89) AND exp_type=73 ".$opening_hand." AND company_id=".$value['company_id']."")->queryRow(); 
						 $dailyexpense_with_amount_opening = $dailyexpense_with_opening['total_amount'];
						 
						 
						 $dailyvendors_cah_opening=  Yii::app()->db->createCommand("SELECT SUM( IFNULL(amount, 0) + IFNULL(tax_amount, 0) ) as total_amount FROM {$tblpx}dailyvendors WHERE payment_type=89 ".$opening_hand." AND company_id=".$value['company_id']."")->queryRow(); 
						 $dailyvendors_cash_opening = $dailyvendors_cah_opening['total_amount'];
						 $withdrawal_opening = $dailyvendors_cash_opening+$daybook_with_amount_opening+$dailyexpense_with_amount_opening;  
                                                 
                                                 $opening_balance = ($deposit_opening+$value['cashbalance_opening_balance'])-$withdrawal_opening;
					   }
					
					
					 ?>
					 
					 <tr>
                         <td><?php echo $key+1; ?></td>
                         <td>
                          <?php
                            $company = Company::model()->findByPk($value['company_id']);	
                            echo $company->name; ?>
                         </td>
                         <td><?php echo $value['caption']; ?></td>
                         <td><?php echo $value['bank_name'];?></td>
                         <td><?php echo $value['cashbalance_date'];?></td>
                         <td align="right"><?php echo Controller::money_format_inr($opening_balance,2);?></td>
                         <td align="right"><?php echo Controller::money_format_inr($withdrawal,2);?></td>
                         <td align="right"><?php echo Controller::money_format_inr($deposit,2);?></td>
                         <td align="right"><?php echo number_format(($opening_balance+$deposit)-$withdrawal,2);?></td>
                     </tr>
                     <?php } ?>
                 </tbody>

            </table>
			
			<?php
			} else {
				
			echo "No data found";	
			}}
			?>


        </div>
