<?php
$tblpx = Yii::app()->db->tablePrefix;
if ($index == 0) {
    ?>
    <thead class="entry-table">
        <tr>
            <?php
            if ((isset(Yii::app()->user->role) && ((in_array('/subcontractorpayment/paymentedit', Yii::app()->user->menuauthlist)) || (in_array('/subcontractorpayment/paymentdelete', Yii::app()->user->menuauthlist)) || (in_array('/subcontractorpayment/permissionapprove', Yii::app()->user->menuauthlist))))) {
                ?>
                <th></th>
            <?php } ?>
            <th>#ID</th>
            <th>Company</th>
            <th>Subcontractor</th>
            <th>Project</th>
            <th>Payment Against Quotation</th>
            <th>Quotation</th>
            <th>Paid Date</th>
            <th>Payment Stage</th>
            <th>Bill Number</th>
            <th>Description</th>
            <th>Transaction Type</th>
            <th>Petty Cash By</th>
            <th>Bank</th>
            <th>Cheque No</th>
            <th>Amount</th>
            <th>Tax Amount</th>
            <th>TDS Amount</th>
            <th>Paid Amount</th>
            <th>Permission</th>
            
        </tr>
    </thead>
<?php } ?>
<?php

$unreconcilbgcolor ="";
if(($data["payment_type"]==88 )&& ($data["reconciliation_status"] == 0)) { 
    $unreconcilbgcolor =" background:#5bc0de3b;";
}
if ($index == 0) {
    ?>
    
<?php } ?>
<?php
$bgcolor = '';
$approval_status = $data["approval_status"];
$id = 'payment_id';
$tbl = $this->tableNameAcc('pre_subcontractor_payment', 0);
$modelName = 'PreSubcontractorPayment';

$final = Yii::app()->controller->actiongetScFinalData($data, $approval_status, $id, $tbl, $modelName);


$data = !empty($final['data'])?$final['data']:$data;
//foreach ($datas as $key => $data) {

$approval_status = $final['approval_status'];
$count = $final['count'];
$class = '';
$date = strtotime("+2 days", strtotime($data["date"]));
$check_date = date("Y-m-d", $date);
if ($data['approve_status'] == 'No') {
    $class = 'permission_style';
} else {
    $class = '';
}
$expenseId = $data["payment_id"];
$deleteconfirm = Deletepending::model()->find(array("condition" => "deletepending_parentid = " . $expenseId . " AND deletepending_status = 0 AND deletepending_table = '{$tblpx}subcontractor_payment'"));
$confirmcount = !empty($deleteconfirm) ? count($deleteconfirm) : 0;
if ($confirmcount > 0) {
    $deleteclass = "deleteclass";
    $deletetitle = "Waiting for delete confirmation from admin.";
} else {
    $deleteclass = "";
    $deletetitle = "";
}
if ($data["update_status"] == 1) {
    $pro_back = "";
    $request_back = " request_back";
} else if ($data["update_status"] == 2) {
    $pro_back = "";
    $request_back = " declined";
} else {
    $pro_back = "pro_back";
    $request_back = "";
}

if ($data["employee_id"] != '') {
    $users = Users::model()->findByPk($data["employee_id"]);
    $employee = $users->first_name . ' ' . $users->last_name;
} else {
    $employee = '';
}

?>
<tr id="<?php echo $data['payment_id']; ?>" class="<?php echo $class; ?> <?php echo $deleteclass . " " . (($approval_status == 0) ? 'bg_pending' : ''); ?><?php echo $request_back; ?>" title="<?php echo $deletetitle; ?>"  data-status="<?php echo $data["approval_status"] ?>" data-id="<?php echo ($data["approval_status"] == 0 && isset($data['ref_id'])) ? $data['ref_id'] : $data['payment_id']; ?>"  style="<?php echo $unreconcilbgcolor ?>">
<?php
    if ((isset(Yii::app()->user->role) && ((in_array('/subcontractorpayment/paymentedit', Yii::app()->user->menuauthlist)) || (in_array('/subcontractorpayment/paymentdelete', Yii::app()->user->menuauthlist)) || (in_array('/subcontractorpayment/permissionapprove', Yii::app()->user->menuauthlist))))) {
        ?>
        <?php
        if ($data["rowcount_slno"] == $data["rowcount_total"]) {
            ?>
            <td rowspan="<?php echo $data["rowcount_total"]; ?>" class="vertical-td" data-status ="<?php echo $data['reconciliation_status'] ?>">
                <?php
                if (yii::app()->user->role != 1 && $data["update_status"] == 1) {
                    if ($data["update_status"] == 1) {
                        $requestmessage = "Already an update request is pending";
                    }
                    ?>
                    <span class="icon icon-options-vertical popover-test" data-toggle="popover" data-placement="right" type="button" data-html="true" style="cursor: pointer;"></span>
                    <div class="popover-content hide">
                        <ul class="tooltip-hiden">
                            <li><span id="<?php echo $index; ?>" class="pending-icon fa fa-question" title="<?php echo $requestmessage; ?>"></span></li>
                        </ul>
                    </div>
                    <?php
                } else {
                    $payDate = date("Y-m-d", strtotime($data["date"]));
                    $company = Company::model()->findByPk($data["company_id"]);
                    $updateDays = ($company->company_updateduration) - 1;
                    //$editDate   = date("Y-m-d", strtotime("-".$updateDays." days"));
                    $editDate = date('Y-m-d ', strtotime($payDate . ' + ' . $updateDays . ' days'));
                    $addedDate = date("Y-m-d 23:59:59", strtotime($data["date"]));
                    $currentDate = date("Y-m-d");
                    if (Yii::app()->user->role == 1) {
                        ?>
                        <span class="icon icon-options-vertical popover-test" data-toggle="popover" data-placement="right" type="button" data-html="true" style="cursor: pointer;"></span>
                        <div class="popover-content hide">
                            <ul class="tooltip-hiden">
                              
                                <?php
                                if ((in_array('/subcontractorpayment/paymentdelete', Yii::app()->user->menuauthlist))) {
                                    ?>
                                    <li><button id="<?php echo $index; ?>" class="btn btn-xs btn-default delete_row">Delete</button></li>
                                <?php } ?>
                            </ul>
                        </div>
                        <?php
                    } else {
                        if (isset($company->company_updateduration) && !empty($company->company_updateduration)) {
                            if ($editDate >= $currentDate) {
                                ?>
                                <span class="icon icon-options-vertical popover-test" data-toggle="popover" data-placement="right" type="button" data-html="true" style="cursor: pointer;"></span>
                                <div class="popover-content hide">
                                    <ul class="tooltip-hiden">
                                        <li><button id="<?php echo $index; ?>" class="btn btn-xs btn-default row-daybook">Edit</button></li>
                                        <?php if ($confirmcount == 0) { ?>
                                            <li><button id="<?php echo $index; ?>" class="btn btn-xs btn-default delete_confirmation">Delete</button></li>
                                        <?php } else { ?>
                                            <li><button id="<?php echo $index; ?>" data-id="<?php echo $deleteconfirm["deletepending_id"]; ?>" class="btn btn-xs btn-default delete_restore">Restore</button></li>
                                        <?php } ?>
                                    </ul>
                                </div>
                                <?php
                            }
                        } else {
                            if ($currentDate <= $addedDate) {
                                ?>
                                <span class="icon icon-options-vertical popover-test" data-toggle="popover" data-placement="right" type="button" data-html="true" style="cursor: pointer;"></span>
                                <div class="popover-content hide">
                                    <ul class="tooltip-hiden">
                                        <?php
                                        if ((in_array('/subcontractorpayment/paymentedit', Yii::app()->user->menuauthlist))) {
                                            ?>
                                            <li><button id="<?php echo $index; ?>" class="btn btn-xs btn-default row-daybook">Edit</button></li>
                                        <?php } ?>
                                        <?php
                                        if ((in_array('/subcontractorpayment/paymentdelete', Yii::app()->user->menuauthlist))) {
                                            ?>
                                            <li><button id="<?php echo $index; ?>" class="btn btn-xs btn-default delete_row">Delete</button></li>
                                        <?php } ?>
                                    </ul>
                                </div>
                                <?php
                            }
                        }
                    }
                    ?>
                </td>
                <?php
            }
        }
        ?>
    <?php }
    ?>
    <td><b>#<?php echo ($data["approval_status"] == 0 && isset($data['ref_id'])) ? $data['ref_id'] : $data['payment_id']; ?></b></td>
    <?php
    if ($data["rowcount_slno"] == $data["rowcount_total"]) {
        ?>
        <td rowspan="<?php echo $data["rowcount_total"]; ?>" class="<?php //echo $pro_back;     ?> wrap vertical-td" id="<?php echo "subcontractor_id" . $index; ?>">
            <?php
            $company = Company::model()->findByPk($data['company_id']);
            echo $company['name'];
            ?>
        </td>
        <td rowspan="<?php echo $data["rowcount_total"]; ?>" id="<?php echo "subcontractor_id" . $index; ?>" class="vertical-td"><?php echo $data["subcontractor_name"] ? $data["subcontractor_name"] : ""; ?></td>
    <?php }
    ?>
    <td id="<?php echo "project_id" . $index; ?>"><?php echo $data["name"] ? $data["name"] : ""; ?></td>

    <td id="<?php echo "project_id" . $index; ?>" class="vertical-td"><?php echo ($data["payment_quotation_status"] == '1') ? "Yes" : "No"; ?></td>
    <td class="vertical-td">
        <?php
        if($data["quotation_number"] !=""){
            $scmodel = Scquotation::model()->findByPk($data["quotation_number"]);
            echo isset($scmodel->scquotation_no)?$scmodel->scquotation_no:"Nil";
        }            
        ?>
    </td>
    <td class="nowrap vertical-td">
    <?php  
    echo isset($data["paid_date"]) ? Date('d-m-Y', strtotime($data["paid_date"])) : "Not Paid"; 
    ?>
    </td>

    <td class="nowrap vertical-td">
    <?php
        if($data["stage_id"] !=""){
            $stagemodel = ScPaymentStage::model()->findByPk($data["stage_id"]);
            echo $stagemodel->payment_stage;
        }            
        ?>
    </td>
    <td class="nowrap vertical-td">
    <?php
    $bill_no = '';
        if(isset($data["sc_bill_id"]) && $data["sc_bill_id"] !=""){
            $billmodel = Subcontractorbill::model()->findByPk($data["sc_bill_id"]);
            echo $billmodel->bill_number;
        }else{
           echo  $bill_no;
        }            
        ?>
    </td>
    <?php
    if ($data["rowcount_slno"] == $data["rowcount_total"]) {
        $user_datas = SubcontractorPayment::model()->getUserDatas($data);
        ?>
        <td rowspan="<?php echo $data["rowcount_total"]; ?>" id="<?php echo "description" . $index; ?>" class="vertical-td">
            <span class="popover-test" data-toggle="popover" data-trigger="hover" data-placement="left" data-trigger="hover" type="button" data-html="true">
                <?php echo $data["payment_description"] ? $data["payment_description"] : ""; ?>
            </span>
            <div class="popover-content hide">
                <div><span>Created By: <?php echo $user_datas['created']; ?> </span>
                </div>
                <div><span>Updated By: <?php echo $user_datas['updated']; ?> </span>
                </div>
        </td>
        <td rowspan="<?php echo $data["rowcount_total"]; ?>" id="<?php echo "payment_type" . $index; ?>" class="vertical-td"><?php echo $data["caption"] ? $data["caption"] : ""; ?></td>
        <td rowspan="<?php echo $data["rowcount_total"]; ?>" id="<?php echo "payment_type" . $index; ?>" class="vertical-td"><?php echo $employee ?></td>
        <td rowspan="<?php echo $data["rowcount_total"]; ?>" id="<?php echo "bank" . $index; ?>" class="vertical-td"><?php echo $data["bank_name"] ? $data["bank_name"] : ""; ?></td>
        <td rowspan="<?php echo $data["rowcount_total"]; ?>" id="<?php echo "cheque_no" . $index; ?>" class="vertical-td"><?php echo $data["cheque_no"] ? $data["cheque_no"] : ""; ?></td>
    <?php }
    ?>

    <td class="text-right vertical-td" id="<?php echo "amount" . $index; ?>"><?php echo $data["amount"] ? Controller::money_format_inr($data["amount"], 2) : ""; ?></td>
    <td class="text-right vertical-td">
        <span class="popover-test tax_amnt" data-toggle="popover" data-trigger="hover" data-placement="left" data-trigger="hover" type="button" data-html="true"><?php echo $data["tax_amount"] ? Controller::money_format_inr($data["tax_amount"], 2) : ""; ?></span>
    </td>
    <td class="text-right vertical-td"><span class="popover-test tds_amnt" data-toggle="popover" data-trigger="hover" data-placement="left" data-trigger="hover" type="button" data-html="true"><?php echo $data["tds_amount"] ? Controller::money_format_inr($data["tds_amount"], 2) : ""; ?></span>
    <td class="text-right vertical-td"><span class="popover-test paid_amnt" data-toggle="popover" data-trigger="hover" data-placement="left" data-trigger="hover" type="button" data-html="true"><?php echo $data["paidamount"] ? Controller::money_format_inr($data["paidamount"], 2) : ""; ?></span>
        <div class="popover-content hide">
            <div><span>SGST: (<?php echo $data["sgst"] ? $data["sgst"] : ""; ?>&#37;) <?php echo $data["sgst_amount"] ? Controller::money_format_inr($data["sgst_amount"], 2) : ""; ?></span></div>
            <div><span>CGST: (<?php echo $data["cgst"] ? $data["cgst"] : ""; ?>&#37;) <?php echo $data["cgst_amount"] ? Controller::money_format_inr($data["cgst_amount"], 2) : ""; ?></span></div>
            <div><span>IGST: (<?php echo $data["igst"] ? $data["igst"] : ""; ?>&#37;) <?php echo $data["igst_amount"] ? Controller::money_format_inr($data["igst_amount"], 2) : ""; ?></span></div>
        </div>
    </td>
    <td class="vertical-td">
        <?php
        if ((in_array('/subcontractorpayment/permissionapprove', Yii::app()->user->menuauthlist))) {
            if ($data['approve_status'] == 'No') {
                ?>
                <button id="<?php echo $data['payment_id']; ?>" class="btn btn-xs btn-default permission_item approveoption_<?php echo $data['payment_id']; ?>">Approve</button>
                <?php
            }
        }
        ?>
    </td>
    
<input type="hidden" name="paymentid[]" id="paymentid<?php echo $index; ?>" value="<?php echo $data["payment_id"]; ?>" />
<input type="hidden" name="rowid[]" id="rowid<?php echo $index; ?>" value="<?php echo $index; ?>" />
</tr>
<?php
// } ?>