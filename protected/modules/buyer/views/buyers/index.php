<?php
/* @var $this BuyersController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Buyers',
);

$this->menu=array(
	array('label'=>'Create Buyers', 'url'=>array('create')),
	array('label'=>'Manage Buyers', 'url'=>array('admin')),
);
?>

<h1>Buyers</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
