<?php
/* @var $this ExpenseTypeController */
/* @var $model ExpenseType */

$this->breadcrumbs=array(
	'Expense Types'=>array('index'),
	'Create',
);

//print_r($category);

function fetchCategoryTree($parent = 'NULL', $spacing = '', $user_tree_array = '') {

		  if (!is_array($user_tree_array))
		  $user_tree_array = array();
		 $tblpx = Yii::app()->db->tablePrefix;
		 if($parent == 'NULL') {
			 		 $category = Yii::app()->db->createCommand("SELECT id , category_name FROM {$tblpx}purchase_category WHERE parent_id IS NULL AND type !='S'")->queryAll();
		 } else {
			 		 $category = Yii::app()->db->createCommand("SELECT id , category_name FROM {$tblpx}purchase_category WHERE  parent_id='".$parent."' AND type !='S'")->queryAll();
		}
		 //$category = Yii::app()->db->createCommand("SELECT id , category_name FROM {$tblpx}purchase_category WHERE parent_id".$query." AND type !='S'")->queryAll();
		 foreach($category as $key=> $value)
		 {
			  $user_tree_array[] = array("id" => $value['id'], "name" => $spacing . $value['category_name']);
			  $user_tree_array = fetchCategoryTree($value['id'], $spacing. '&nbsp;&nbsp; -', $user_tree_array);
	}
		
		return $user_tree_array;
}


 $tblpx = Yii::app()->db->tablePrefix;
 $unit = Yii::app()->db->createCommand("SELECT id , unit_name FROM {$tblpx}unit")->queryAll();


/*function fetchCategoryTree($parent = NULL, $spacing = '', $user_tree_array = '') {
		  if (!is_array($user_tree_array))
		  $user_tree_array = array();
		 $tblpx = Yii::app()->db->tablePrefix;
		 $category = Yii::app()->db->createCommand("SELECT id , category_name FROM {$tblpx}purchase_category WHERE parent_id='".$parent."' AND type !='S'")->queryAll();
		 foreach($category as $key=> $value)
		 {
			  $user_tree_array[] = array("id" => $value['id'], "name" => $spacing . $value['category_name']);
			  $user_tree_array = fetchCategoryTree($value['id'], $spacing. '&nbsp;&nbsp; -', $user_tree_array);
	}
		
		return $user_tree_array;
}*/

$categoryList = fetchCategoryTree();
//print_r($categoryList);
?>

<div class="modal-dialog modal-sm">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" onclick="javascript:window.location.reload()">&times;</button>
            <h4 class="modal-title">Add Specification</h4>
        </div>
        <div class="modal-body">
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id'=>'purchase-category-form',
        'enableAjaxValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true,
            'validateOnType' => false,),
    ));
    ?>

    <!-- Popup content-->



    <div class="clearfix">

            <div class="row addRow">

                <div class="col-md-12">

			
					<?php //echo $form->labelEx($model,'parent_id'); ?>
                    <select name="parent_id" id="parent_id" style="width:200px;height:35px;border:1px solid #6d37b0;padding:5px;" required>
                                            <option value="">Choose Category</option>
                                            <?php
											foreach($categoryList as $key => $value){
													?>	
													<option value="<?php echo $value['id']; ?>" ><?php echo $value['name']; ?></option>
													<?php		
										}		
                                            ?>

                                    </select>
                                     <?php //echo $form->error($model,'parent_id'); ?>
                </div>
                
                <div class="col-md-12">
                    <?php echo $form->labelEx($model,'specification'); ?>
                    <?php echo $form->textArea($model,'specification'); ?>
                    <?php echo $form->error($model,'specification'); ?>
               </div>
               
               <div class="col-md-12" style="padding-top: 20px;">
                    <!--<?php //echo $form->labelEx($model,'unit'); ?>
                    <?php //echo $form->textField($model,'unit'); ?>
                    <?php //echo $form->error($model,'unit'); ?> -->
                    
                    <select name="unit" id="unit" style="width:200px;height:35px;border:1px solid #6d37b0;padding:5px;" required>
						<option value="">Choose Unit</option>
						<?php
						foreach($unit as $key => $value){
								?>	
								<option value="<?php echo $value['id']; ?>" ><?php echo $value['unit_name']; ?></option>
								<?php		
					}		
						?>

				</select>
               </div>

            </div>
            <div class="modal-footer save-btnHold">
                <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?> 
                <?php if (Yii::app()->user->role == 1) {
                	if(!$model->isNewRecord){ /*
                 ?> 
                    <a class="btn del-btn" href="<?php echo $this->createUrl('projects/deleteprojects', array('id' => $model->type_id)) ?>" class="deletebtn" onclick="return confirm('Are you sure that you want to delete?');">Delete </a>                           
                <?php */ }} ?>
                <button data-dismiss="modal" onclick="javascript:window.location.reload()">Close</button>
            </div>
            
    </div><!-- form -->

<?php $this->endWidget(); ?>
    </div>

</div>
