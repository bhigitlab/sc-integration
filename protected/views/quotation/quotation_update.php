<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.min.js"></script>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/message/css/jquery.message.css">
<script type="text/javascript"
	src="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/message/js/jquery.message.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
<script>
	var shim = (function () {
		document.createElement('datalist');
	})();
</script>
<style>
	.select2 {
		width: 100%;
	}
</style>
<script>
	$(function () {
		$("#datepicker").datepicker({
			dateFormat: 'dd-mm-yy'
		});

	});
</script>
<div class="container">
	<?php
	$sql = "SELECT SUM(stage_percent)  FROM `jp_payment_stage` WHERE `quotation_id` = " . $model->quotation_id;
	$total_stagepercent = Yii::app()->db->createCommand($sql)->queryScalar();
	$Amtsql = "SELECT SUM(stage_amount)  FROM `jp_payment_stage` WHERE `quotation_id` = ".$model->quotation_id;
	$total_stageamount = Yii::app()->db->createCommand($Amtsql)->queryScalar();
	$payment_stage_sql = "SELECT * FROM `jp_payment_stage` WHERE `quotation_id` = ".$model->quotation_id." AND `invoice_status`='1' ORDER BY `serial_number` ASC ";
	$inv_generated_payment_stage = Yii::app()->db->createCommand($payment_stage_sql)->queryAll();
	$all_payment_stages_sql = "SELECT * FROM `jp_payment_stage` WHERE `quotation_id` = " . $model->quotation_id . "  ORDER BY `serial_number` ASC ";
	$all_payment_stage = Yii::app()->db->createCommand($all_payment_stages_sql)->queryAll();
	$readonly = '';
	$project_cost_readonly = '';
	$disabled = '';
	if (!empty($inv_generated_payment_stage)) {
		$readonly = 'readonly';
		$disabled = '';
	}
	if (!empty($all_payment_stage)) {
		$project_cost_readonly = 'readonly';
		
	}
	?>
	<input type="hidden" value="<?php echo (!is_null($total_stagepercent)?$total_stagepercent:"0.00") ?>" class="total_stagepercent">
	<input type="hidden" value="<?php echo (!is_null($total_stageamount)?$total_stageamount:"0.00") ?>" class="total_stageamount">
	<div class="loading-overlay">
		<span class="fa fa-spinner fa-3x fa-spin"></span>
	</div>
	<div class="expenses-heading">
		<div class="clearfix">
			<h3>Quotation</h3>
		</div>
	</div>
	<div class="entries-wrapper">
		<div class="row">
			<div class="col-xs-12">
				<div class="heading-title">Add Details</div>
				<div class="dotted-line"></div>
			</div>
		</div>
		<div id="loading"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/ajax-loader.gif"></div>
		<div id="msg_box"></div>


		<form id="pdfvals1" method="post" action="<?php echo $this->createAbsoluteUrl('quotation/quotationupdate'); ?>">
			<input type="hidden" name="remove" id="remove" value="">
			<input type="hidden" name="quotation_id" id="quotation_id" value="<?php echo $model->quotation_id; ?>">
			<div class="row">
				<div class="form-group col-xs-12 col-sm-3 col-xs-12 col-sm-3 col-md-2">
					<label>COMPANY : <span id="client_label"></span></label>
					<?php
					$user = Users::model()->findByPk(Yii::app()->user->id);
					$arrVal = explode(',', $user->company_id);
					$newQuery = "";
					foreach ($arrVal as $arr) {
						if ($newQuery)
							$newQuery .= ' OR';
						$newQuery .= " FIND_IN_SET('" . $arr . "', id)";
					}
					$companyInfo = Company::model()->findAll(array('condition' => $newQuery));
					?>
					<select name="company_id" class="inputs target company_id form-control js-example-basic-single"
						id="company_id" style="width:190px" <?php echo $disabled ?>>
						<option value="">Choose company</option>
						<?php
						foreach ($companyInfo as $key => $value) {
							$selected = "";
							if ($value['id'] == $model->company_id) {
								$selected = "selected";
							}
							?>
							<option value="<?php echo $value['id']; ?>" <?php echo $selected ?>>
								<?php echo $value['name']; ?>
							</option>
							<?php
						}
						?>

					</select>

				</div>
				<div class="form-group col-xs-12 col-sm-3 col-xs-12 col-sm-3 col-md-2">

					<label>CLIENT : <span id="client_label"></span></label>
					<select name="client" class="inputs target client" id="client" style="width:190px" <?php echo $disabled ?>>
						<option value="">Choose client</option>
						<?php
						foreach ($client as $key => $value) {
							$selected = "";
							if ($value['cid'] == $model->client_id) {
								$selected = "selected";
							}
							?>
							<option value="<?php echo $value['cid']; ?>" <?php echo $selected ?>>
								<?php echo $value['name']; ?>
							</option>
						<?php } ?>
					</select>

				</div>
				<div class="form-group col-xs-12 col-sm-3 col-xs-12 col-sm-3 col-md-2">

					<label>PROJECT : <span id="project_label"></span></label>
					<select name="project" class="inputs target project" id="project" style="width:190px" <?php echo $disabled ?>>
						<option value="">No Project</option>
					</select>

				</div>
				<div class="form-group col-xs-12 col-sm-3 col-xs-12 col-sm-3 col-md-2">

					<label>DATE : </label>
					<input type="text" value="<?php echo date("d-m-Y", strtotime($model->date)); ?>" id="datepicker"
						class="txtBox date inputs target form-control" name="date" placeholder="Please click to edit"
						<?php echo $readonly ?>>

				</div>
				<div class="form-group col-xs-12 col-sm-3 col-xs-12 col-sm-3 col-md-2">

					<label>Quotation No : </label>
					<?php
					$readonly = '';
					$activeProjectTemplate = $this->getActiveTemplate();
					if (($activeProjectTemplate == 'TYPE-4' && ENV == 'production') || (!empty($inv_generated_payment_stage))) {
						$readonly = 'readonly';
					}

					?>
					<input type="text" required value="<?php echo $model->inv_no; ?>"
						class="txtBox inputs target check_type inv_no  form-control" name="inv_no" id="inv_no"
						placeholder="" <?php echo $readonly ?>>

				</div>
				<div class="form-group col-xs-12 col-sm-3 col-xs-12 col-sm-3 col-md-2">

					<label>Total Project Cost : </label>
					<input type="text" required value="<?php echo $model->total_project_cost ?>"
						class="txtBox inputs target check_type project_cost  form-control" name="project_cost"
						id="project_cost" autocomplete="off" <?php echo $project_cost_readonly ?>>

				</div>
			</div>
			<div class="clearfix"></div>
			<br>
			<div id="msg"></div>


		</form>
		<div class="row">
			<div class="col-xs-12">
				<div class="heading-title">Payment Details</div>
				<div class="dotted-line"></div>
			</div>
		</div>

		<!-- Payment Stages -->


		<div class="panel-body">
			<div class="row">
				<form class="stage-form" method="POST">
					<div class="col-xs-12 col-sm-3 col-xs-12 col-sm-3 col-md-2">
						<label>Payment Stage:</label>
						<input type="hidden" class="stage_id" />
						<input type="hidden" name="quotation_id" id="quotation_id"
							value="<?php echo $model->quotation_id; ?>">
						<input type="text" class="payment_stage form-control" />
					</div>
					<div class="col-xs-12 col-sm-3 col-xs-12 col-sm-3 col-md-2">
						<label>Percentage:</label>
						<input type="hidden" class="selected_percent" value="0">
						<input type="number" class="percentage form-control" max="100" />
						<div class="error"></div>
					</div>
					<div class="col-xs-12 col-sm-3 col-xs-12 col-sm-2 col-md-2">
						<label>Amount:</label>
						<input type="hidden" class="selected_amount" value="0">
						<input type="text" class="stage_amount form-control" />
						<div class="error"></div>
					</div>
					<div class="col-xs-12 col-sm-2 col-md-2">
						<label>Sl Number:</label>
						<input type="text" class="serial_number form-control" value="<?php echo $serial_number; ?>"
							readonly />
						<div class="error"></div>
					</div>
					<div class="col-xs-12 col-sm-2 col-md-2 more_sec">
						<label class="d-block">&nbsp;</label>
						<button type="button" class="btn btn-primary btn-sm add_more_stage">
							<i class="fa fa-plus"></i></button>
						<button type="button" class="btn btn-danger btn-sm remove_stage">
							<i class="fa fa-minus"></i></button>
					</div>
				</form>
			</div>
		</div>
		<div class="form-group col-xs-12 text-right">
			<button class="btn btn-primary  addstage" type="button">Save</button>
			<button class="btn btn-default  resetStage" type="reset">Reset</button>
		</div>

		<div class="row">
			<div class="col-xs-12">
				<div class="heading-title">Payment Stages</div>
				<div class="dotted-line"></div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="table-responsive">
					<table border="1" class="table">
						<thead class="entries-wrapper">
							<tr>
								<th>Sl Number</th>
								<th>Stage</th>
								<th>Stage Percentage</th>
								<th>Amount</th>
								<th>Invoice Status</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							<?php
							$payment_stage = Yii::app()->db->createCommand("SELECT * FROM `jp_payment_stage` WHERE `quotation_id` = " . $model->quotation_id . " ORDER BY `serial_number` ASC ")->queryAll();
							foreach ($payment_stage as $key => $value) {
								?>
								<?php
								$bgcolor = "";

								if ($value['delete_approve_status'] == 1) {
									$bgcolor = "style=background:#ff000036";
								}
								?>
								<tr <?php echo $bgcolor ?>>
									<td>
										<?php echo $value['serial_number']; ?>
									</td>
									<td>
										<?php echo $value['payment_stage']; ?>
									</td>
									<td>
										<?php echo number_format($value['stage_percent'], 2, '.', ''); ?>
									</td>
									<td class="text-right">
										<?php echo $value['stage_amount']; ?>
									</td>
									<td>
										<?php echo ($value['invoice_status'] == 0) ? 'Pending' : 'Invoice Generated'; ?>
									</td>
									<td>
										<?php if ($value['invoice_status'] == 0) { ?>
											<span class="icon icon-options-vertical popover-test" data-toggle="popover"
												data-placement="left" type="button" data-html="true"
												style="cursor: pointer;"></span>
											<div class="popover-content hide">
												<ul class="tooltip-hiden">

													<li>
														<a href="#" class="btn btn-xs btn-default editStage"
															data-id="<?php echo $value['id'] ?>"
															data-qid="<?php echo $value['quotation_id'] ?>">Edit</a>
													</li>
													<li>
														<a href="index.php?r=invoice/invoicelumpsum&stageId=<?php echo $value['id'] ?>&quotationId=<?php echo $value['quotation_id'] ?>"
															class="btn btn-xs btn-default convert_invoice"
															qtid="<?php echo $value['quotation_id'] ?>" target="_blank">Convert
															To
															Invoice</a>
													</li>
													<?php if ($value['delete_approve_status'] == 0) { ?>
														<li>
															<button class="btn btn-xs btn-default deletePaymentStage"
																data-id="<?php echo $value['id'] ?>">Delete</button>

														</li>
													<?php } ?>


												</ul>
											</div>
										<?php } ?>
									</td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<script>

	$(document).on('click', '.deletePaymentStage', function (e) {

		var element = $(this);
		var stage_id = $(this).attr('data-id');
		$.ajax({
			url: '<?php echo Yii::app()->createAbsoluteUrl('quotation/deletePaymentStage'); ?>',
			type: 'POST',
			dataType: 'json',
			data: { id: stage_id },
			success: function (response) {
				$("#msg_box").html('<div class="alert alert-' + response.response + ' alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>' + response.msg + '</div>');
				setTimeout(function () {
					window.location.reload(1);
				}, 1000);

			}
		});
	});

	var quotation_id = <?php echo $model->quotation_id; ?>;
	jQuery.extend(jQuery.expr[':'], {
		focusable: function (el, index, selector) {
			return $(el).is('button, :input, [tabindex]');
		}
	});

	$(document).on('keypress', 'input,select', function (e) {
		if (e.which == 13) {
			e.preventDefault();
			var $canfocus = $(':focusable');
			var index = $canfocus.index(document.activeElement) + 1;
			if (index >= $canfocus.length) index = 0;
			$canfocus.eq(index).focus();
		}
	});

	$(document).ready(function () {

		$(".popover-test").popover({
			html: true,
			content: function () {
				return $(this).next('.popover-content').html();
			}
		});
		$('[data-toggle=popover]').on('click', function (e) {
			$('[data-toggle=popover]').not(this).popover('hide');
		});
		$('body').on('hidden.bs.popover', function (e) {
			$(e.target).data("bs.popover").inState.click = false;
		});
		$('body').on('click', function (e) {
			$('[data-toggle=popover]').each(function () {
				if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
					$(this).popover('hide');
				}
			});
		});
		$(document).ajaxComplete(function () {
			$(".popover-test").popover({
				html: true,
				content: function () {
					return $(this).next('.popover-content').html();
				}
			});

		});
		$(".js-example-basic-single").select2();
		$(".project").select2();
		$(".client").select2();

	});


	$('.project').change(function () {

		$("#client").select2("open");
	})





	$(document).ready(function () {
		$('#description').first().focus();
	});

	$(document).ready(function () {
		$().toastmessage({
			sticky: false,
			position: 'top-right',
			inEffectDuration: 1000,
			stayTime: 3000,
			closeText: '<i class="icon ion-close-round"></i>',
		});


	});


	// remove item

	$(document).on('click', '.removebtn', function (e) {
		e.preventDefault();
		element = $(this);
		var item_id = $(this).attr('id');
		var answer = confirm("Are you sure you want to delete?");
		if (answer) {
			$('.loading-overlay').addClass('is-active');
			var item_id = $(this).attr('id');
			var quotation_id = $("#quotation_id").val();
			var data = {
				'quotation_id': quotation_id,
				'item_id': item_id
			};
			$.ajax({
				method: "GET",
				async: false,
				data: {
					data: data
				},
				dataType: "json",
				url: '<?php echo Yii::app()->createUrl('quotation/removequotationitem'); ?>',
				success: function (response) {
					if (response.response == 'success') {
						element.closest('tr').remove();
						$('#amount_total').html(response.amount_total);
						$('#tax_total').html(response.tax_total);
						$('#grand_total').html(response.grand_total);
						$().toastmessage('showSuccessToast', "" + response.msg + "");
						$('.addrow').html(response.html);
					} else { }
				}
			});

		} else {

			return false;
		}

	});



	$(document).on("click", ".addcolumn, .removebtn", function () {
		$("table.table  input[name='sl_No[]']").each(function (index, element) {
			$(element).val(index + 1);
			$('.sl_No').html(index + 1);
		});
	});






	$(".inputSwitch span").on("click", function () {

		var $this = $(this);

		$this.hide().siblings("input").val($this.text()).show();

	});

	$(".inputSwitch input").bind('blur', function () {

		var $this = $(this);

		$(this).attr('value', $(this).val());

		$this.hide().siblings("span").text($this.val()).show();

	}).hide();


	$(".allownumericdecimal").keydown(function (event) {



		if (event.shiftKey == true) {
			event.preventDefault();
		}

		if ((event.keyCode >= 48 && event.keyCode <= 57) ||
			(event.keyCode >= 96 && event.keyCode <= 105) ||
			event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 ||
			event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190 || event.keyCode == 110 || event.keyCode == 13) {
			var splitfield = $(this).val().split(".");
			if ((splitfield[1] !== undefined && splitfield[1].length >= 2) && event.keyCode != 8 && event.keyCode != 0 && event.keyCode != 13) {
				event.preventDefault();
			}
		} else {
			event.preventDefault();
		}

		if ($(this).val().indexOf('.') !== -1 && (event.keyCode == 190 || event.keyCode == 110))
			event.preventDefault();

	});







	$('.other').click(function () {
		if (this.checked) {

			$('#autoUpdate').fadeIn('slow');
			$('#select_div').hide();
		} else {
			$('#autoUpdate').fadeOut('slow');
			$('#select_div').show();
		}

	})


	$(document).on("change", ".other", function () {
		if (this.checked) {
			$(this).closest('tr').find('#autoUpdate').fadeIn('slow');
			$(this).closest('tr').find('#select_div').hide();
		} else {
			$(this).closest('tr').find('#autoUpdate').fadeOut('slow');
			$(this).closest('tr').find('#select_div').show();
		}
	});
</script>


<script>
	var sl_no = 1;
	var howMany = 0;
	$('.item_save').click(function () {
		$("#previous_details").hide();
		var element = $(this);
		var item_id = $(this).attr('id');

		if (item_id == 0) {


			var description = $('#description').val();
			var hsn_code = $('#hsn_code').val();
			var quantity = $('#quantity').val();
			var unit = $('#unit').val();
			var rate = $('#rate').val();

			var cgst = $('#cgst').val();
			var cgst_amount = $('#cgst_amount').html();
			var sgst = $('#sgst').val();
			var sgst_amount = $('#sgst_amount').html();
			var igst = $('#igst').val();
			var igst_amount = $('#igst_amount').html();
			var tax_amount = $('#tax_amount').html();

			var amount = $('#item_amount').html();
			var project = $('input[name="project"]').val();
			var client = $('input[name="client"]').val();
			var date = $('input[name="date"]').val();
			var inv_no = $('input[name="inv_no"]').val();
			var rowCount = $('.table .addrow tr').length;


			if (project == '' || client == '' || date == '' || inv_no == '') {
				$().toastmessage('showErrorToast', "Please enter purchase details");
			} else {

				if (description == '' || quantity == '' || rate == '' || amount == 0) {
					if (amount == 0) {
						$().toastmessage('showErrorToast', "Please fill valid quantity and rate");
					} else {
						$().toastmessage('showErrorToast', "Please fill the details");
					}
				} else {
					howMany += 1;
					if (howMany == 1) {
						var quotation_id = $('input[name="quotation_id"]').val();
						var subtot = $('#grand_total').text();
						var grand = $('#grand_total').text();
						var grand_tax = $('#grand_taxamount').val(); //alert($('#grand_taxamount').val());
						var data = {
							'sl_no': rowCount,
							'quantity': quantity,
							'description': description,
							'unit': unit,
							'rate': rate,
							'amount': amount,
							'sgst': sgst,
							'sgst_amount': sgst_amount,
							'cgst': cgst,
							'cgst_amount': cgst_amount,
							'igst': igst,
							'igst_amount': igst_amount,
							'tax_amount': tax_amount,
							'quotation_id': quotation_id,
							'grand': grand,
							'subtot': subtot,
							'grand_tax': grand_tax,
							'hsn_code': hsn_code
						};
						$('.loading-overlay').addClass('is-active');
						$.ajax({
							url: '<?php echo Yii::app()->createAbsoluteUrl('quotation/quotationitem'); ?>',
							type: 'GET',
							dataType: 'json',
							data: {
								data: data
							},
							success: function (response) {
								if (response.response == 'success') {
									$('#discount_total').html(response.discount_total);
									$('#amount_total').html(response.amount_total);
									$('#tax_total').html(response.tax_total);
									$('#grand_total').html(response.grand_total);
									$().toastmessage('showSuccessToast', "" + response.msg + "");
									$('.addrow').html(response.html);
								} else {
									$().toastmessage('showErrorToast', "" + response.msg + "");
								}
								howMany = 0;
								$('#description').val('').trigger('change');
								$('#remarks').val('');
								$('#quantity').val('');
								$('#unit').val('');
								$('#rate').val('');
								$('#item_amount').html('0.00');
								$('#tax_amount').html('0.00');
								$('#sgst').val('');
								$('#sgst_amount').html('0.00');
								$('#cgst').val('');
								$('#cgst_amount').html('0.00');
								$('#igst').val('');
								$('#hsn_code').val('');
								$('#igst_amount').html('0.00');
								$('#description').focus();
							}
						});
					}

				}



			}



		} else {
			// update 


			var description = $('#description').val();
			var hsn_code = $('#hsn_code').val();
			var quantity = $('#quantity').val();
			var unit = $('#unit').val();
			var rate = $('#rate').val();
			var amount = $('#item_amount').html();
			var project = $('input[name="project"]').val();
			var client = $('input[name="client"]').val();
			var date = $('input[name="date"]').val();
			var inv_no = $('input[name="inv_no"]').val();

			var cgst = $('#cgst').val();
			var cgst_amount = $('#cgst_amount').html();
			var sgst = $('#sgst').val();
			var sgst_amount = $('#sgst_amount').html();
			var igst = $('#igst').val();
			var igst_amount = $('#igst_amount').html();
			var tax_amount = $('#tax_amount').html();

			if (project == '' || client == '' || date == '' || inv_no == '') {

			} else {

				if (description == '' || quantity == '' || rate == '' || amount == 0) {
					if (amount == 0) {
						$().toastmessage('showErrorToast', "Please fill valid quantity and rate");
					} else {
						$().toastmessage('showErrorToast', "Please fill the details");
					}
				} else {
					howMany += 1;
					if (howMany == 1) {
						var quotation_id = $('input[name="quotation_id"]').val();
						var subtot = $('#grand_total').text();
						var grand = $('#grand_total').text();
						var grand_tax = $('#grand_taxamount').val();
						var data = {
							'item_id': item_id,
							'sl_no': sl_no,
							'quantity': quantity,
							'description': description,
							'unit': unit,
							'rate': rate,
							'amount': amount,
							'sgst': sgst,
							'sgst_amount': sgst_amount,
							'cgst': cgst,
							'cgst_amount': cgst_amount,
							'igst': igst,
							'igst_amount': igst_amount,
							'tax_amount': tax_amount,
							'quotation_id': quotation_id,
							'grand': grand,
							'subtot': subtot,
							'grand_tax': grand_tax,
							'hsn_code': hsn_code
						};
						$('.loading-overlay').addClass('is-active');
						$.ajax({
							url: '<?php echo Yii::app()->createAbsoluteUrl('quotation/updatesquotationitem'); ?>',
							type: 'GET',
							dataType: 'json',
							data: {
								data: data
							},
							success: function (response) {
								if (response.response == 'success') {
									$('#amount_total').html(response.amount_total);
									$('#tax_total').html(response.tax_total);
									$('#grand_total').html(response.grand_total);
									$().toastmessage('showSuccessToast', "" + response.msg + "");
									$('.addrow').html(response.html);
								} else {
									$().toastmessage('showErrorToast', "" + response.msg + "");
								}

								howMany = 0;
								$('#description').val('').trigger('change');
								$('#remarks').val('');
								$('#quantity').val('');
								$('#unit').val('');
								$('#rate').val('');
								$('#item_amount').html('0.00');
								$('#tax_amount').html('0.00');
								$('#sgst').val('');
								$('#sgst_amount').html('0.00');
								$('#cgst').val('');
								$('#cgst_amount').html('0.00');
								$('#igst').val('');
								$('#igst_amount').html('0.00');
								$('#description').focus();
								$('#item_amount_temp').val(0);
								$('#item_tax_temp').val(0);
								$('#hsn_code').val('');
								$(".item_save").attr('value', 'Save');
								$(".item_save").attr('id', 0);
								$('#description').focus();
							}
						});

					}
				}



			}


		}




	});




	$(document).on('click', '.edit_permission', function (e) {
		var item_id = $(this).attr("id");
		$('#loading').show();
		$.ajax({
			url: '<?php echo Yii::app()->createAbsoluteUrl('quotation/qspermission'); ?>',
			type: 'GET',
			data: {
				item_id: item_id,
				status: 1
			},
			success: function (data) {
				if (data == 1) {
					$().toastmessage('showSuccessToast', "Edit permission granted for quantity surveyor");
					$(".permission").html('<a id="' + item_id + '" class="btn btn-xs btn-default remove_permission">Remove QS Permission</a>');
				} else {
					$().toastmessage('showErrorToast', "Failed!");
				}
			}
		});
	});
	$(document).on('click', '.remove_permission', function (e) {
		$('#loading').show();
		var item_id = $(this).attr("id");
		$.ajax({
			url: '<?php echo Yii::app()->createAbsoluteUrl('quotation/qspermission'); ?>',
			type: 'GET',
			data: {
				item_id: item_id,
				status: 2
			},
			success: function (data) {
				if (data == 1) {
					$().toastmessage('showSuccessToast', "Edit permission removed for quantity surveyor");
					$(".permission").html('<a id="' + item_id + '" class="btn btn-xs btn-default edit_permission">Grant QS Permission</a>');

				} else {
					$().toastmessage('showErrorToast', "Failed!");
				}
			}
		});
	});
	$(document).on('click', '.edit_item', function (e) {
		e.preventDefault();


		$('.remark').css("display", "inline-block");
		$('#remark').focus();

		var item_id = $(this).attr('id');
		var $tds = $(this).closest('tr').find('td');
		var description = $tds.eq(1).text();
		var hsn_code = $tds.eq(2).text();
		var quantity = $tds.eq(3).text();
		var unit = $tds.eq(4).text();
		var rate = $tds.eq(5).text();
		var sgst = $tds.eq(6).text();
		var sgst_amount = $tds.eq(7).text();
		var cgst = $tds.eq(8).text();
		var cgst_amount = $tds.eq(9).text();

		var igst = $tds.eq(10).text();
		var igst_amount = $tds.eq(11).text();
		var tax_amount = $tds.eq(12).text();
		var amount = $tds.eq(13).text();
		var sgsct1 = eval($tds.eq(7).text()).toFixed(2);
		var sgsct2 = eval($tds.eq(9).text()).toFixed(2);
		var sgsct3 = eval($tds.eq(11).text()).toFixed(2);
		var tax_amount = (eval(sgsct1) + eval(sgsct2) + eval(sgsct3));

		$('#description').val(description.trim())
		$('#hsn_code').val(parseFloat(hsn_code))
		$('#quantity').val(parseFloat(quantity))
		$('#unit').val(unit.trim())
		$('#item_amount').html(parseFloat(amount))
		$('#item_amount_temp').val(parseFloat(amount))
		$('#rate').val(parseFloat(rate))

		$('#tax_amount').html(parseFloat(tax_amount.toFixed(2)))
		$('#item_tax_temp').val(parseFloat(tax_amount))
		$('#cgst').val(cgst.trim())
		$('#cgst_amount').text(cgst_amount)
		$('#sgst').val(sgst.trim())
		$('#sgst_amount').text(sgst_amount)
		$('#igst').val(igst.trim())
		$('#igst_amount').text(igst_amount)
		$('#tax_amount').text(tax_amount.toFixed(2))

		$(".item_save").attr('value', 'Update');
		$(".item_save").attr('id', item_id);
		$(".item_save").attr('id', item_id);
		$('#description').focus();


	})



	$("#quantity, #rate, #sgst, #cgst, #igst").blur(function () {
		var quantity = parseFloat($("#quantity").val());
		var rate = parseFloat($("#rate").val());
		var sgst = parseFloat($("#sgst").val());
		var cgst = parseFloat($("#cgst").val());
		var igst = parseFloat($("#igst").val());
		var amount = quantity * rate;
		var new_amount = 0;

		var sgst_amount = (sgst / 100) * amount;
		var cgst_amount = (cgst / 100) * amount;
		var igst_amount = (igst / 100) * amount;

		if (isNaN(sgst_amount)) sgst_amount = 0;
		if (isNaN(cgst_amount)) cgst_amount = 0;
		if (isNaN(igst_amount)) igst_amount = 0;
		if (isNaN(amount)) amount = 0;

		var tax_amount = sgst_amount + cgst_amount + igst_amount;

		var total_amount = amount + tax_amount;

		$("#sgst_amount").html(sgst_amount.toFixed(2));
		$("#cgst_amount").html(cgst_amount.toFixed(2));
		$("#igst_amount").html(igst_amount.toFixed(2));
		$("#item_amount").html(amount.toFixed(2));
		$("#tax_amount").html(tax_amount.toFixed(2));
		$("#total_amount").html(total_amount.toFixed(2));

	});

	$('.item_save').keypress(function (e) {
		if (e.keyCode == 13) {
			$('.item_save').click();
		}
	});


	function filterDigits(eventInstance) {
		eventInstance = eventInstance || window.event;
		key = eventInstance.keyCode || eventInstance.which;
		if ((47 < key) && (key < 58) || key == 8) {
			return true;
		} else {
			if (eventInstance.preventDefault)
				eventInstance.preventDefault();
			eventInstance.returnValue = false;
			return false;
		}
	}
</script>

<script>
	(function () {
		var mainTable = document.getElementById("main-table");
		if (mainTable !== null) {
			var tableHeight = mainTable.offsetHeight;
			if (tableHeight > 380) {
				var fauxTable = document.getElementById("faux-table");
				document.getElementById("table-wrap").className += ' ' + 'fixedON';
				var clonedElement = mainTable.cloneNode(true);
				clonedElement.id = "";
				fauxTable.appendChild(clonedElement);
			}
		}
	})();
</script>
<script>
	$(document).ready(function () {
		$('#loading').hide();
	});
	$(document).ajaxComplete(function () {
		$('.loading-overlay').removeClass('is-active');
		$('#loading').hide();
	});

	$(document).on("click", ".addstage", function () {
		var total_input_percentage = 0;
		var total_input_amount = 0;
		var total_quot_amount=0;
		total_quot_amount = $(".project_cost").val();
		$(".panel-body .row .percentage").each(function () {
			var percentage = parseFloat($(this).val());
			if (!isNaN(percentage)) {
				total_input_percentage = total_input_percentage + percentage;
			}
		});
		$(".panel-body .row .stage_amount").each(function () {
			var stage_amt = parseFloat($(this).val());
			if(!isNaN(stage_amt)){
				total_input_amount = total_input_amount + stage_amt;
			}
		});
		
		var total_stagepercent = $(".total_stagepercent").val();
		var total_stageamount = $(".total_stageamount").val();
		
		var input1 = parseFloat(total_stageamount);
		
  		var input2 = parseFloat(total_input_amount);
		var input3 = parseFloat($(".selected_amount").val());
		
		if (isNaN(input1) || isNaN(input2)) {
			$('#total').text('Both inputs must be numbers');
		} else {
			var total = input1 + input2 - input3;
		}
		total = parseFloat(total.toFixed(2));
		
		//total = Math.round(total * 100) / 100;
		if(total > total_quot_amount){
			alert("The total entered stage amount is " + total +". Sum of percentages must  be <= 100");
			return false;
		} else {
			$(this).attr("disabled", "disabled");
			var submitData = $(".stage-form");
			$(submitData).each(function () {
				var id = $(this).find(".stage_id").val();
				var quotation_id = $(this).find("#quotation_id").val();
				var payment_stage = $(this).find(".payment_stage").val();
				var payment_stage_amount = $(this).find(".stage_amount").val();
				var stage_percentage = $(this).find(".percentage").val();
				var serial_number = $(this).find(".serial_number").val();
				var project_cost = $("#project_cost").val();
				$.ajax({
					type: 'POST',
					url: '<?php echo Yii::app()->createAbsoluteUrl('quotation/addPaymentStage'); ?>',
					data: {
						id: id,
						quotation_id: quotation_id,
						payment_stage: payment_stage,
						stage_percent: stage_percentage,
						project_cost: project_cost,
						serial_number: serial_number,
						payment_stage_amount: payment_stage_amount
					},
					dataType: 'json',
					success: function (result) {
					},
					complete: function () {
						$().toastmessage('showSuccessToast', "Payment Stage Added Successfully");
						setTimeout(function () {
							window.location.reload(1);
						}, 1000);
					}
				})
			})
		}
	});

	$(document).on("click", ".editStage", function () {
		var id = $(this).attr("data-id");
		$.ajax({
			url: '<?php echo Yii::app()->createUrl('quotation/getPaymentStage'); ?>',
			method: 'POST',
			data: {
				id: id,
			},
			dataType: "json",
			success: function (response) {
				$(".stage_id").val(response.id);
				$(".payment_stage").val(response.payment_stage);
				$(".percentage").val(response.stage_percent);
				$(".selected_percent").val(response.stage_percent);
				$(".selected_amount").val(response.stage_amount);
				$(".stage_amount").val(response.stage_amount);
				$(".serial_number").val(response.serial_number);
				$(".more_sec").hide();
			}
		})
	})

	$(document).on("keyup change", ".percentage", function () {
		var stage_percentage = $(this).val();
		var total_input_percentage = 0;
		var total_input_amount=0;
		$(this).parents(".panel-body").find(".percentage").each(function(){			
			total_input_percentage = total_input_percentage + parseFloat($(this).val());
		})
		
		$(this).parents(".panel-body").find(".stage_amount").each(function(){	
			var stage_amt = parseFloat($(this).val());
			if(!isNaN(stage_amt)){
				total_input_amount = total_input_amount + stage_amt;
			}
		});
		

		
        var project_cost = $("#project_cost").val();
		var total_quot_amount = $("#project_cost").val();
		var stage_amount = project_cost * (stage_percentage / 100);
		$(this).parents(".row").find(".stage_amount").val(stage_amount.toFixed(2));

		var total_stagepercent = $(".total_stagepercent").val();
		var total_stageamount = $(".total_stageamount").val();
		
		var input1 = parseFloat(total_stageamount);
		
  		var input2 = parseFloat(total_input_amount);
		var input3 = parseFloat($(".selected_amount").val());
		if (isNaN(input1) || isNaN(input2)) {
			$('#total').text('Both inputs must be numbers');
		} else {
			var total = input1 + input2 - input3;
		}
		total = parseFloat(total.toFixed(2));
		
		
		if(total > total_quot_amount){
			$(this).siblings('.error').text("Sum of percentages must  be <= 100");
			$(".addstage").attr("disabled", true);
			$(this).parents(".row").find(".add_more_stage").attr("disabled", true);
		} else {
			$(this).siblings('.error').text("");
			$(".addstage").attr("disabled", false);
			$(this).parents(".row").find(".add_more_stage").attr("disabled", false);
		}

		if (stage_percentage.length === 0) {
			$(this).siblings('.error').text("");
			$(".addstage").attr("disabled", false);
			$(this).parents(".row").find(".add_more_stage").attr("disabled", false);
		}

	})
	$(document).find(".resetStage").click(function () {
		var rows = $(this).parents(".panel-footer").siblings(".panel-body");
		var fieldToKeep = 'serial_number form-control'
		$('form.stage-form :input[type="text"],form.stage-form :input[type="number"]').each(function () {
			var fieldName = $(this).attr('class');
			if (fieldName !== fieldToKeep) {
				$(this).val(""); // Reset the input field value
			}
		});
		$(rows).find(".row:not(:first)").remove();
		$(rows).find(".row:first-child").find(".more_sec").show();
	})
	$(function () {
		$('.client').trigger("change");
	})

	$('.client').change(function () {
		$('#loading').show();
		var project_id = '<?php echo $model->project_id; ?>';
		var val = $(this).val();
		$.ajax({
			type: "POST",
			url: '<?php echo Yii::app()->createUrl('quotation/getProject'); ?>',
			data: {
				client_id: val,
				project_id: project_id
			},
			dataType: 'json',
			success: function (data) {
				if (data.status === 'success') {
					$('#project').html(data.projectlist);
				} else {
					$('#project').val('No Projects');
				}

			},
		})
	})

	$(document).on("click", ".convert_invoice", function () {

		var stageId = $(this).attr('id');
		var qtid = $(this).attr('qtid');
		$.ajax({
			type: "POST",
			url: '<?php echo Yii::app()->createUrl('invoice/invoicelumpsum'); ?>',
			data: {
				stageId: stageId,
				qtid: qtid
			},
			success: function (data) {
				console.log(data);

			},
		})
	})

	$(document).on("change", "#project_cost", function () {
		var element = $(this);
		var project_cost = $("#project_cost").val();
		var quotation_id = $("#quotation_id").val();
		var default_date = $(".date").val();
		var company = $('#company_id').val();
		var client = $('#client').val();
		var project = $('#project').val();
		var inv_no = $('#inv_no').val();

		if ((moment(default_date, 'DD-MM-YYYY', true).isValid())) {
			if (client == '' || default_date == '' || inv_no == '' || company == '') { } else {
				$('.loading-overlay').addClass('is-active');
				$.ajax({
					method: "GET",
					async: false,
					data: {
						quotation_id: quotation_id,
						inv_no: inv_no,
						default_date: default_date,
						project: project,
						client: client,
						company: company,
						project_cost: project_cost
					},
					dataType: "json",
					url: '<?php echo Yii::app()->createUrl('quotation/createnewquotation'); ?>',
					success: function (result) {
						if (result.response == 'success') {
							$(".purchase_items").removeClass('checkek_edit');
							$('#client').select2('focus');
							$("#quotation_id").val(result.quotation_id);
							$().toastmessage('showSuccessToast', "" + result.msg + "");
						} else {
							$().toastmessage('showErrorToast', "" + result.msg + "");
							element.val('');
						}

					}
				});
			}
		} else {
			$(this).focus();
			$().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
			$(this).focus();
		}
	});

	$(".remove_stage").hide();
	$(document).on("click", ".add_more_stage", function () {
		var clone = $(this).parents(".row").clone();
		$(clone).find("input[type=text], input[type=number]").each(function () {
			if (!$(this).hasClass("serial_number")) {
				$(this).val("");
			} else {
				var currentValue = $(this).val();
				var incrementedValue = parseInt(currentValue) + 1; // Increment the value
				$(this).val(incrementedValue);
			}
		});
		$(clone).appendTo(".panel-body");
		$(this).parents(".more_sec").hide();
		var row_len = $(".panel-body").find(".row").length;

		if (row_len > 1) {
			$(clone).find(".remove_stage").show();
		}
	});

	$(document).on("click", ".remove_stage", function () {
		var selectedrow = $(this).parents(".row");
		var row_len = $(".panel-body").children(".row").length;
		if (row_len > 1) {
			$(selectedrow).remove();
		}
		$(".panel-body").find(".row:last-child").find(".more_sec").show();
		$(".panel-body").find(".row:last-child").find(".percentage").trigger("keyup");

	});

	$(document).on("change", ".serial_number", function () {
		var serial_no = $(this).val();
		var quotation_id = $(this).find("#quotation_id").val();
		$.ajax({
			type: 'POST',
			url: '<?php echo Yii::app()->createAbsoluteUrl('quotation/checkPaymentStage'); ?>',
			data: {
				serial_no: serial_no,
				quotation_id: quotation_id
			},
			dataType: 'json',
			success: function (result) {

			}
		})
	});

	$("#company_id").change(function () {
		var val = $(this).val();
		$("#client").html('<option value="">Select Client</option>');
		$('#loading').show();
		$.ajax({
			url: '<?php echo Yii::app()->createUrl('quotation/dynamicclient'); ?>',
			method: 'POST',
			data: {
				company_id: val
			},
			dataType: "json",
			success: function (response) {
				if (response.status == 'success') {
					$("#client").html(response.html);
				} else {
					$("#client").html(response.html);
				}
			}

		})
	})
	<?php
	if (empty($inv_generated_payment_stage)) {
		?>

		$(document).ready(function () {
			$('#pdfvals1').change(function () {
				//alert('hi');
				$('#pdfvals1').submit();
			});
			$('#pdfvals1').on('submit', function (e) {
				e.preventDefault();
				var form_data = {
					quotation_id: $('#quotation_id').val(),
					company_id: $("#company_id").val(),
					client: $("#client").val(),
					project: $('#project').val(),
					date: $('#datepicker').val(),
					inv_no: $('#inv_no').val(),
					project_cost: $('#project_cost').val()
				};
				var isValid = true;
				$.each(form_data, function (key, value) {
					if (value === '') {
						isValid = false;
						return false;
					}
				});
				// alert(form_data);
				if (isValid) {
					$.ajax({
						type: "POST",
						url: "<?php echo $this->createAbsoluteUrl('quotation/quotationupdate'); ?>",
						data: form_data,
						success: function (response) {
							console.log(response);
							if (response == 1) {
								$().toastmessage('showSuccessToast', "Quotation updated successfully");

							} else {
								$().toastmessage('showErrorToast', "Some Error Occured");

							}
						}
					});
				}


			});
		});
	<?php } ?>
	$(document).on("keyup change", ".stage_amount", function () {
		var stage_amount = parseFloat($(this).val());
		var quotation_amount = parseFloat($("#project_cost").val());
		var percentage = parseFloat((stage_amount / quotation_amount) * 100);
		if (stage_amount < 0) {
			$(this).siblings('.error').text("Amount should not be less than zero");
			$(this).parents(".row").find(".add_more_stage").attr("disabled", true);
		} else if (stage_amount > quotation_amount) {
			$(this).siblings('.error').text("Amount should not be greater than Quotation amount");
			$(this).parents(".row").find(".add_more_stage").attr("disabled", true);

		} else {
			$(this).siblings('.error').text("");
			$(".addstage").attr("disabled", false);
			$(this).parents(".row").find(".add_more_stage").attr("disabled", false);

		}
		if (percentage > 100) {
			$(this).parents(".row").find(".percentage").siblings('.error').text("Percentage should be less than 100");

		} else {
			$(this).parents(".row").find(".percentage").siblings('.error').text("");
			$(".addstage").attr("disabled", false);
			$(this).parents(".row").find(".add_more_stage").attr("disabled", false);
		}
		$(this).parents(".row").find(".percentage").val(isNaN(percentage) ? "" : percentage.toFixed(2));
		$(this).parents(".row").find(".floated_percentage").val(isNaN(percentage) ? "" : percentage);
		var total_input_percentage = 0;
		$(this).parents(".append_div").find(".percentage").each(function () {
			total_input_percentage = total_input_percentage + parseFloat($(this).val());
		});
		var stage_percentage = parseFloat($(this).parents(".row").find(".percentage").val());
		var total_stagepercent = parseFloat($(".total_stagepercent").val());
		var input1 = parseFloat(total_stagepercent);
		var input2 = parseFloat(total_input_percentage);
		var input3 = parseFloat($(".selected_percent").val());
		if (isNaN(input1) || isNaN(input2)) {
			$('#total').text('Both inputs must be numbers');
		} else {
			var total = input1 + input2 - input3;
		}
		if (total > 100) {
			$(this).parents(".row").find(".percentage").siblings('.error').text("Sum of percentages must  be <= 100");
			$(".addstage").attr("disabled", true);
			$(this).parents(".row").find(".add_more_stage").attr("disabled", true);
		} else {
			$(this).parents(".row").find(".percentage").siblings('.error').text("");
			$(".addstage").attr("disabled", false);
			$(this).parents(".row").find(".add_more_stage").attr("disabled", false);
		}
	});
</script>