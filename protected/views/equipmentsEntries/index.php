<?php
/* @var $this EquipmentsEntriesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Equipments Entries',
);

$this->menu=array(
	array('label'=>'Create EquipmentsEntries', 'url'=>array('create')),
	array('label'=>'Manage EquipmentsEntries', 'url'=>array('admin')),
);
?>

<h1>Equipments Entries</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
