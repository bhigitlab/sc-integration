
<?php
/* @var $this QuotationItemMasterController */
/* @var $model QuotationItemMaster */
/* @var $form CActiveForm */
?>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.js"></script>
<style>  
    .highlight{border: 1px solid red !important;}  

</style> 
<div class="example1consolesucces">
    <p id='#alert'></p>
</div>
<div class="alert alert-success multi_lead_div1" style="display:none"></div>
<span id="error"></span>
<div class="">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'quotation-item-master-form',
        'action' => Yii::app()->createAbsoluteUrl("quotationitem/QuotationItemMaster/create"),
        'htmlOptions'=>array('enctype' => 'multipart/form-data'),
        'enableAjaxValidation' => true,
        'enableClientValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true,
            'validateOnType' => false,
        ),
    ));
    ?>
    <div class="panel-body">
        <?php $items = Yii::app()->controller->GetItemCategory();
        ?>
        <div class="panel " style="padding-left:30px;padding-right:30px;">
            <span id="error"></span>
            <div class="row addRow">
                <div class="col-md-3 col-sm-6" style="margin-bottom:30px">
                    <?php echo $form->labelEx($model, 'Category'); ?>

                    <?php echo $form->dropDownList($model, 'quotation_category_id', CHtml::listData(QuotationCategoryMaster::model()->findAll(), 'id', 'name'), array('class' => 'form-control js-example-basic-single field_change require', 'empty' => '-Select Category-', 'style' => 'width:100%')); ?>
                </div>
            </div>
            <div class="row addRow" style="padding-top:20px;padding-bottom:20px;">

                <div class="col-md-3 col-sm-6">
                    <?php echo $form->labelEx($model, 'Work Type'); ?>
                    <?php echo $form->dropDownList($model, 'work_type_id', CHtml::listData(QuotationWorktypeMaster::model()->findAll(array("condition" => "status = 1")), 'id', 'name'), array('class' => 'form-control js-example-basic-single field_change require', 'empty' => '-Select WorkType-', 'id' => 'worktype', 'style' => 'width:100%','data-template'=>'')); ?>
                    <?php echo $form->error($model, 'Work Type'); ?>
                </div>

                <div class="col-md-3 col-sm-6" id="image-view">
                    <?php echo $form->labelEx($model, 'image'); ?>
                    <?php echo $form->fileField($model, 'image', array('id' => 'file')); ?>
                    <?php echo $form->error($model, 'image'); ?>
                </div>
            </div>
            <div id="boxwork" style="display:none">
                <?php
                $this->renderPartial('_template1', array('model' => $model, 'itemdata' => $itemdata,
                    'itemfinish' => $itemfinish, 'form' => $form, 'items' => $items));
                ?>
            </div>
            <div  id="additionalwork" style="display:none">
                <?php
                $this->renderPartial('_template2', array('model' => $model, 'itemdata' => $itemdata,
                    'itemfinish' => $itemfinish, 'form' => $form, 'items' => $items));
                ?>
            </div>
            <div id="anotherwork" style="display:none">
                <?php
                $this->renderPartial('_template3', array('model' => $model, 'itemdata' => $itemdata,
                    'itemfinish' => $itemfinish, 'form' => $form, 'items' => $items));
                ?>
            </div>
            <div class="panel-footer save-btnHold text-center" style="margin-top:30px">
                <input type="submit" class="btn btn-info" id="additem-button" value="Add">
            </div>
            <?php $this->endWidget(); ?>
        </div>
    </div>
</div>

<style>
    .addRow label {
        display: inline-block;
    }

    input[type="radio"] {
        margin: 4px 4px 0;
    }

    input[type="checkbox"][readonly] {
        pointer-events: none;
    }

    .row-margin {
        margin-top: 20px;
    }

    .row-margin-another {
        margin-top: 20px;
        margin-bottom: 20px;
    }
</style>
<?php $itemtUrl = Yii::app()->createAbsoluteUrl("quotationitem/quotationItemMaster/getItem"); ?>
<script>
    var itemdata = '<?php echo $itemdata; ?>';
    var itemfinish = '<?php echo $itemfinish; ?>';
    var count = 1;
    $(function () {
        $('p#add_field').click(function () {
            count += 1;

            $('#container').append(
                    '<div class="row addRow panel" style="padding:10px">' +
                    '<div class="row addRow row-margin">' +
                    '<div class="col-md-2">' +
                    '<input class="form-control worktypelabel common" id="worktype_label' + count + '"   name="worktype_label[]" type="text" placeholder="Work Type Label" "required"="required"></input>      ' +
                    '<div class="errorMessage" id="QuotationItemMaster_worktype_label_em_" style="display: none;"></div></div></div> ' +
                    '<div class="row addRow row-margin">' +
                    '<div class="col-md-2">' +
                    '<label for="Clients_Shutter_Work">Shutter  Work</label></div>' +
                    '<div class="col-md-3">' +
                    '<select class="form-control js-example-basic-multiple shuttermaterial" id="shutter_material_' + count + '" multiple="multiple" name="shutter_material[' + count + '][]"><option value="">Select  material</option>' + itemdata + '</select>      ' +
                    ' </div>' +
                    '<div class="col-md-3">' +
                    '<select class="form-control js-example-basic-single shutterfinish" id="shutter_finish_' + count + '" name="shutter_finish[]' + '" type="text"><option value="">Select finish</option>' + itemfinish + '</select> ' +
                    ' </div>' +
                    '<div class="col-md-4">' +
                    '<textarea class="form-control shutterworkdesc" id="shutterwork_description_' + count + '" name="shutterwork_description[]' + '" type="text" placeholder="Description"></textarea>      ' +
                    ' </div></div>' +
                    '<div class="row addRow row-margin">' +
                    '<div class="col-md-2">' +
                    '<label for="Clients_Shutter_Work">Caracoss  Work</label>  </div>' +
                    '<div class="col-md-3">' +
                    '<select class="form-control js-example-basic-multiple caracassmaterial" id="caracoss_material_' + count + '" multiple="multiple" name="caracoss_material[' + count + '][]" ><option value="">Select  material</option>' + itemdata + '</select>' +
                    '</div>' +
                    '<div class="col-md-3">' +
                    '<select class="form-control js-example-basic-single caracassfinish" id="caracoss_finish_' + count + '" name="caracoss_finish[]' + '" type="text"><option value="">Select  finish</option>' + itemfinish + '</select>      ' +
                    '  </div>' +
                    '<div class="col-md-4">' +
                    '<textarea class="form-control carcassdesc" id="caracoss_description_' + count + '" name="caracoss_description[]' + '" type="text" placeholder="Description"></textarea>      ' +
                    '</div></div>' +
                    '<div class="row addRow row-margin">' +
                    '<div class="col-md-2">' +
                    '</div>' +
                    '<div class="col-md-3">' +
                    '<input class="form-control allownumericdecimal ratedata common" id="rate_value_' + count + '" name="rate[]' + '" type="text" placeholder="Rate"></input>      ' +
                    '           </div>' +
                    '<div class="col-md-3">' +
                    '<input class="form-control allownumericdecimal common" id="profit_percent_value_' + count + '" name="profit[]' + '" type="text" placeholder="Profit %"></input>      ' +
                    ' </div>' +
                    '<div class="col-md-3">' +
                    '<input class="form-control allownumericdecimal common" id="discount_value_' + count + '" name="discount[]' + '" type="text" placeholder="Discount"></input>      ' +
                    ' </div></div></div>' +
                    '');
            $('.js-example-basic-multiple').select2({
                width: '100%',
                placeholder: "Select Material",
            });


            $('.shutterfinish').select2({
                width: '100%',
                placeholder: "Select Finish",
            });

            $('.caracassfinish').select2({
                width: '100%',
                placeholder: "Select Finish",
            });
        });

    });

    var count = 0;
    $(function () {
        $('p#add_field_another').click(function () {
            count += 1;
            $('#container_anotherwork').append(
                    '<div class="row addRow panel" style="padding:10px">' +
                    '<div class="col-md-3">' +
                    '<input class="form-control worktypelabel_other common-another" id="worktype_label_other' + count + '" name="worktype_label_other[]' + '" type="text" placeholder="Work Type Label"></input>' +
                    '<div class="errorMessage" id="QuotationItemMaster_worktype_label_em_" style="display: none;"></div></div> ' +
                    '<div class="col-md-3">' +
                    '<textarea class="form-control description" id="description_' + count + '" name="description[]" placeholder="Description"></textarea>      ' +
                    ' </div>' +
                    '<div class="col-md-2">' +
                    '<input class="form-control allownumericdecimal other_rate common-another" id="other_rate_' + count + '" name="other_rate[]' + '" type="text" placeholder="Rate"></input>      ' +
                    '           </div>' +
                    '<div class="col-md-2">' +
                    '<input class="form-control allownumericdecimal common-another" id="other_profit_' + count + '" name="other_profit[]' + '" type="text" placeholder="Profit %"></input>      ' +
                    '</div>' +
                    '<div class="col-md-2">' +
                    '<input class="form-control allownumericdecimal common-another" id="other_discount_' + count + '" name="other_discount[]' + '" type="text" placeholder="Discount"></input>      ' +
                    '</div></div></div>' +
                    '<br/>');
        });
    });

    var count = 0;
    $(function () {
        $('p#add_field_additionalwork').click(function () {
            count += 1;
            $('#container_additionalwork').append(
                    '<div class="row addRow panel" style="padding:10px">' +
                    '<div class="col-md-3">' +
                    '<input class="form-control worktypelabel_additional common-additional"  id="worktype_label_additional' + count + '" name="worktype_label_additional[]' + '" type="text" placeholder="Work Type Label"></input>' +
                    '</div>' +
                    '<div class="col-md-3">' +
                    '<select class="form-control js-example-basic-multiple material" id="material_' + count + '" multiple="multiple" name="material[' + count + '][]"><option value="">Select  material</option>' + itemdata + '</select>      ' +
                    ' </div>' +
                    '<div class="col-md-3">' +
                    '<select class="form-control js-example-basic-single finish" id="finish_' + count + '" name="finish[]' + '" type="text"><option value="">Select finish</option>' + itemfinish + '</select> ' +
                    ' </div>' +
                    '<div class="col-md-3">' +
                    '<textarea class="form-control description" id="additional_description_' + count + '" name="additional_description[]" placeholder="Description"></textarea>      ' +
                    ' </div>' +
                    '<div class="col-md-3">' +
                    '<input class="form-control allownumericdecimal additional_rate common-additional" id="additional_rate_' + count + '" name="additional_rate[]' + '" type="text" placeholder="Rate"></input>      ' +
                    '           </div>' +
                    '<div class="col-md-3">' +
                    '<input class="form-control allownumericdecimal common-additional" id="additional_profit_' + count + '" name="additional_profit[]' + '" type="text" placeholder="Profit %"></input>      ' +
                    '</div>' +
                    '<div class="col-md-3">' +
                    '<input class="form-control allownumericdecimal common-additional" id="additional_discount_' + count + '" name="additional_discount[]' + '" type="text" placeholder="Discount"></input>      ' +
                    '</div>' +
                    '<div class="col-md-3">' +
                    '<input class="form-control" id="additional_image_' + count + '" name="QuotationItemMaster[additional_image][]' + '" type="file" placeholder="Image"></input>      ' +
                    '</div></div></div>' +
                    '<br/>');

            $('.js-example-basic-multiple').select2({
                width: '100%',
                placeholder: "Select Material",
            });


            $('.finish').select2({
                width: '100%',
                placeholder: "Select Finish",
            });
        });
    });

    $(".allownumericdecimal").keydown(function (event) {
        if (event.shiftKey == true) {
            event.preventDefault();
        }
        if ((event.keyCode >= 48 && event.keyCode <= 57) ||
                (event.keyCode >= 96 && event.keyCode <= 105) ||
                event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 ||
                event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190 || event.keyCode == 110 || event.keyCode == 13) {
            var splitfield = $(this).val().split(".");
            if ((splitfield[1] !== undefined && splitfield[1].length >= 2) && event.keyCode != 8 && event.keyCode != 0 && event.keyCode != 13) {
                event.preventDefault();
            }
        } else {
            event.preventDefault();
        }

        if ($(this).val().indexOf('.') !== -1 && (event.keyCode == 190 || event.keyCode == 110))
            event.preventDefault();

    });

    $(function () {
        $('p#add_field_remove').click(function () {
            var rows = $(this).parent(".btn-holder").siblings('#container');
            var row_len = $(rows).children().length - 1;
            if (row_len >= 1) {
                $(rows).find('.panel:last-child').remove();
                return false;
            }
        });
    });
    $(function () {
        $('p#add_field_another_remove').click(function () {
            var rows = $(this).parent(".btn-holder-another").siblings('#container_anotherwork');
            var row_len = $(rows).children('.panel').length - 1;
            if (row_len >= 1) {
                $(rows).children('.panel').last().remove();
                return false;
            }
        });
    });

    $(function () {
        $('p#add_field_additionalwork_remove').click(function () {
            var rows = $(this).parent(".btn-holder-additional").siblings('#container_additionalwork');
            var row_len = $(rows).children('.panel').length - 1;
            if (row_len >= 1) {
                $(rows).children('.panel').last().remove();
                return false;
            }
        });
    });

    $(document).on("change", "#Clients_project_type", function () {
        var val = $("#Clients_project_type option:selected").text();
        if (val == 'NRI') {
            $('.hidbox').show();
        } else {
            $('.hidbox').hide();
        }
    });
<?php $get_worktype_layout = Yii::app()->createAbsoluteUrl("quotationitem/QuotationItemMaster/getworktypelayout"); ?>
    $('#worktype').change(function () {
        var val = $("#worktype option:selected").val();
        $.ajax({
            url: "<?php echo $get_worktype_layout; ?>",
            method: "POST",
            data: {worktypeid: val},
            success: function (result) {
                $('#worktype').attr('data-template',result);
                if (result == 1) {
                    $('#boxwork').show();
                    $('#anotherwork').hide();
                    $('#additionalwork').hide();
                } else if (result == 3) {
                    $('#image-view').hide();
                    $('#additionalwork').show();
                    $('#boxwork').hide();
                    $('#anotherwork').hide();
                } else {
                    $('#anotherwork').show();
                    $('#boxwork').hide();
                    $('#additionalwork').hide();
                }
            }});

    });

<?php $checkduplicate = Yii::app()->createAbsoluteUrl("quotationitem/QuotationItemMaster/checkduplicate"); ?>
    $(document).on('change', '.worktypelabel', function () {
        var $t = $(this);
        var val = $t.val();
        var z = 0;
        $(".worktypelabel").each(function () {
            var y = $(this).val();
            if (val == y) {
                z = z + 1;
            }
        });
        if (z > 1) {
            $t.siblings('.errorMessage').show().html('Already Exist!').fadeOut(1500);
            $t.val('');
            return false;
        }
        $.ajax({
            url: "<?php echo $checkduplicate; ?>",
            method: "POST",
            data: {name: val},
            success: function (result) {

                if (result != 0) {
                    $t.siblings('.errorMessage').show().html('Already Exist!').fadeOut(1500);
                    $t.val('');
                }
            }

        });
    });

    $(document).on('change', '.worktypelabel_other', function () {
        var $t = $(this);
        var val = $t.val();
        var z = 0;
        $(".worktypelabel_other").each(function () {
            var y = $(this).val();
            if (val == y) {
                z = z + 1;
            }
        });
        if (z > 1) {
            $t.siblings('.errorMessage').show().html('Already Exist!').fadeOut(1500);
            $t.val('');
            return false;
        }

        $.ajax({
            url: "<?php echo $checkduplicate; ?>",
            method: "POST",
            data: {name: val},
            success: function (result) {
                console.log(result);
                if (result != 0) {
                    $t.siblings('.errorMessage').show().html('Already Exist!').fadeOut(1500);
                    $t.val('');
                }
            }

        });
    });

    $(document).on('change', '.worktypelabel_additional', function () {
        var $t = $(this);
        var val = $t.val();
        var z = 0;
        $(".worktypelabel_additional").each(function () {
            var y = $(this).val();
            if (val == y) {
                z = z + 1;
            }
        });
        if (z > 1) {
            $t.siblings('.errorMessage').show().html('Already Exist!').fadeOut(1500);
            $t.val('');
            return false;
        }

        $.ajax({
            url: "<?php echo $checkduplicate; ?>",
            method: "POST",
            data: {name: val},
            success: function (result) {
                console.log(result);
                if (result != 0) {
                    $t.siblings('.errorMessage').show().html('Already Exist!').fadeOut(1500);
                    $t.val('');
                }
            }

        });
    });

    $(document).ready(function () {

        $('.js-example-basic-multiple').select2({
            width: '100%',
            placeholder: "Select Material",
        });

        $('.shutterfinish').select2({
            width: '100%',
            placeholder: "Select Finish",
        });

        $('.caracassfinish').select2({
            width: '100%',
            placeholder: "Select Finish",
        });
        $('.finish').select2({
            width: '100%',
            placeholder: "Select Finish",
        });
    });

    $(document).on('click', '#additem-button', function (event) {
        event.preventDefault();
        var error = '';
        var worktype = $("#worktype").val();
        var template =  $('#worktype').attr('data-template');
        if (template == 1) {
            $('.common').each(function () {
                var count = 1;
                if ($(this).val() == '')
                {
                    error += "<p>Please Enter Mandatory Fields</p>";
                    $('.common').css('border', "1px solid red");
                    return false;
                }
                count = count + 1;
            });
        } else if(template == 3) {
            $('.common-additional').each(function () {
                var count = 1;
                if ($(this).val() == '')
                {
                    error += "<p>Please Enter Mandatory Fields</p>";
                    $('.common-additional').css('border', "1px solid red");
                    return false;
                }
                count = count + 1;
            });
        } else {
            $('.common-another').each(function () {
                var count = 1;
                if ($(this).val() == '')
                {
                    error += "<p>Please Enter Mandatory Fields</p>";
                    $('.common-another').css('border', "1px solid red");
                    return false;
                }
                count = count + 1;
            });
        }

        if (error == '')
        {
            $('#error').html('');
            $('#quotation-item-master-form').submit();
        } else {
            $('#error').html('<div class="alert alert-danger">' + error + '</div>');
        }
    })

    function onlySpecialchars(str)
    {        
        var regex = /^[^a-zA-Z0-9]+$/;
        var message ="";                           
        var disabled=false;                         
        var matchedAuthors = regex.test(str);
         
        if (matchedAuthors) {
            var message ="Special characters not allowed";
            var disabled=true;   
        } 
                
        return {"message":message,"disabled":disabled};        
    }
      
    $(".worktypelabel,.worktypelabel_other ,.worktypelabel_additional").keyup(function () {              
        var response =  onlySpecialchars(this.value);        
        $(this).siblings(".errorMessage").show().html(response.message).addClass('d-block');    
        $("#additem-button").attr('disabled',response.disabled);
    });


</script>
