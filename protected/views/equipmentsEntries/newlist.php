<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$this->breadcrumbs = array(
    'performa invoice',
)
?>
<?php 
$pms_api_integration_model = ApiSettings::model()->findByPk(1);
$pms_api_integration = $pms_api_integration_model->api_integration_settings;
?>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/tableHeadFixer.js"></script>
<?php if ($pms_api_integration == 1): ?>
		<ul class="legend">
			<li><span class="project_not_mapped"></span> Project/PMS Template Not Mapped In Integration</li>
		</ul>

		<?php endif; ?>
<?php
$sort = new CSort;
$sort->defaultOrder = 'id DESC';
$sort->attributes = array('id' => 'Equipment Id');
$dataProvider = new CArrayDataProvider($newmodel, array(
    'id' => 'id', 
    'sort' => $sort,
    'keyField' => 'id', 
    'pagination' => false
));
$this->widget('zii.widgets.CListView', array(
    'dataProvider' => $dataProvider,
    'itemView' => '_newview',
    'id' => 'equipment',
    'enableSorting' => 1,
    'enablePagination' => true,
    'template' => '<div>{summary}{sorter}</div><div class="table-responsive" id="parent"><table width="100" class="table total-table" id="fixTable">{items}</table></div>{pager}',
    'enablePagination' => false
)); ?>

<script>
    $(document).ready(function() {
        $("#fixTable").tableHeadFixer({
            'left': 1,
            'foot': true,
            'head': true
        });
    });
    $(document).ready(function() {
        $(".popover-test").popover({
            html: true,
            content: function() {
                return $(this).next('.popover-content').html();
            }
        });
        $('[data-toggle=popover]').on('click', function(e) {
            $('[data-toggle=popover]').not(this).popover('hide');
        });
        $('body').on('hidden.bs.popover', function(e) {
            $(e.target).data("bs.popover").inState.click = false;
        });
        $('body').on('click', function(e) {
            $('[data-toggle=popover]').each(function() {
                if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                    $(this).popover('hide');
                }
            });
        });
    });
</script>
<style>
    #parent {
        max-height: 400px;
    }

    th {
        background: #eee;
    }

    .pro_back {
        background: #fafafa;
    }

    .total-table {
        margin-bottom: 0px;
    }

    th,
    td {
        border-right: 1px solid #ccc !important;
    }
</style>