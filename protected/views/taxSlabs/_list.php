<?php
// echo '<pre>';
// print_r(Yii::app()->user->menuauthlist);
// exit;
if ($index == 0) {
?>
    <thead>
        <tr>
            <th>SI No</th>
            <th>Tax Slab</th>
            <!-- <th>CGST</th>
            <th>SGST</th>
            <th>IGST</th> -->
            <th>status</th>
            <th>Default</th>

            <?php
            if (isset(Yii::app()->user->role) && (in_array('/taxSlabs/update', Yii::app()->user->menuauthlist))) {
            ?>
                <th>Action</th>
            <?php } ?>
        </tr>
    </thead>
<?php } ?>
<tr>

    <td style="width: 50px;"><?php echo $index + 1; ?></td>
    <td>
        <span><?php echo $data->tax_slab_value ?></span>
    </td>
    <!-- <td>
        <span><?php echo $data->cgst ?></span>
    </td>
    <td>
        <span><?php echo $data->sgst ?></span>
    </td>
    <td>
        <span><?php echo $data->igst ?></span>
    </td> -->
    <td>
        <span><?php echo $data->status == 1 ? 'Active' : 'status_values' ?></span>
    </td>
    <td>
        <span><?php echo $data->set_default == 1 ? 'Yes' : 'NO' ?></span>
    </td>


    <?php
    if (isset(Yii::app()->user->role) && (in_array('/taxSlabs/update', Yii::app()->user->menuauthlist))) {
    ?>
        <td style="width: 50px;">
            <a class="fa fa-edit editProject" data-toggle="modal" data-target=".edit" data-id="<?php echo $data->id; ?>"></a>
        </td>
    <?php } ?>
</tr>