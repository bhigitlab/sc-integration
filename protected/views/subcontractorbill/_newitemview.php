<?php
/* @var $this SubcontractorbillController */
/* @var $data Subcontractorbill */
$item_id = $data->quotationitem_id;
$tblpx          = Yii::app()->db->tablePrefix;  
$item_name      = Yii::app()->db->createCommand("SELECT item_description FROM {$tblpx}scquotation_items WHERE item_id = {$item_id}")->queryScalar();

?>
<?php if ($index == 0) { ?>
<thead>
    <tr>

        <th>Sl No</th>
        <th>Item</th>
        <th>Type</th>
        <th>Quantity</th>
        <th>Unit</th>
        <th>Rate</th>
        <th>Description</th>
        <th>Amount</th>
        <th>Tax Slab (%)</th>
        <th>SGST (%)</th>
        <th>SGST</th>
        <th>CGST (%)</th>
        <th>CGST</th>
        <th>IGST (%)</th>
        <th>IGST</th>
        <th>Total Tax</th>
        <th>Total Amount</th>
        <th></th>
    </tr>   
</thead>
<tfoot>
    <tr>
        <th></th>
        <th></th>
        <th></th>        
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th class="text-right"><?php echo Controller::money_format_inr($amount,2); ?></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th class="text-right">
        
        <?php
         echo Controller::money_format_inr($tax_amount,2); ?>        
        </th>
        
        <th class="text-right"><?php echo Controller::money_format_inr($total_amount,2); ?></th>
        <th></th>
    </tr>   
</tfoot>
<?php }
?>
<tr>
    <td><?php echo $index+1; ?></td>
    <td><?php echo $item_name  ?></td>
    <td><?php echo isset($data->item_type)?($data->item_type==0?"Quantity * Rate":"Lumpsum"):""; ?></td>
    <td><?php echo $data->item_quantity; ?></td>
    <td><?php echo $data->item_unit; ?></td>
    <td><?php echo $data->item_rate; ?></td>
    <td><?php echo $data->description; ?></td>
    <td class="text-right"><?php echo Controller::money_format_inr($data->amount,2);  ?></td>
    <td><?php echo $data->tax_slab ?></td>
    <td><?php echo $data->sgstp; ?></td>
    <td class="text-right"><?php echo Controller::money_format_inr($data->sgst,2);  ?></td>
    <td><?php echo $data->cgstp; ?></td>
    <td class="text-right"><?php echo Controller::money_format_inr($data->cgst,2);  ?></td>
    <td><?php echo $data->igstp; ?></td>
    <td class="text-right"><?php echo Controller::money_format_inr($data->igst,2);  ?></td>
    <td class="text-right"><?php echo Controller::money_format_inr($data->tax_amount,2); ?></td>
    <td class="text-right"><?php echo Controller::money_format_inr($data->total_amount,2);  ?></td>
    <td>
        <a class="fa fa-edit editItem" id="editItem" style="cursor:pointer;" data-id="<?php echo $data->id; ?>"></a>
        <a class="fa fa-trash removeItem" id="removeItem" style="cursor:pointer;" data-id="<?php echo $data->id; ?>"></a>
    </td>
</tr>