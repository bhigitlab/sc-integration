<?php 
$id=$data->exp_id; 
	//echo $id; die();
				
?>
					<div class="row">
						<div class="col-md-4 col-sm-4 col-xs-4">
							<div class="list_item"><span>User: </span><?php echo  $data->user['first_name'];?></div>
							<div class="list_item"><span>Date: </span><?php if($data->date!= ""){ echo date("d-M-Y", strtotime($data->date)); } ?></div>
							<div class="list_item"><span>Amount: </span><?php echo ($data->amount  ? $data->amount:0);?></div>
							
						</div>
						<div class="col-md-3 col-sm-3 col-xs-3">
							<div class="list_item"><span>Status: </span><?php echo $data->status0['caption'];?></div>
							<div class="list_item"><span>Project: </span><?php echo $data->project['name'];?></div>
							<div class="list_item"><span>Description: </span><?php echo $data->description;?></div>
							
						</div>
						<div class="col-md-4 col-sm-4 col-xs-3">
							<div class="list_item"><span>Details: </span><?php echo $data->status1['caption'];?></div>
							<div class="list_item"><span>Created By: </span><?php echo $data->createdBy['first_name'];?></div>
							<div class="list_item"><span>Created Date: </span><?php echo date('d-M-Y h:i a',strtotime($data->created_date));?></div>
							
						    
						</div>
						<div class="col-md-1 col-sm-1 col-xs-2">
							<span class="fa fa-edit editWithdrawal" data-toggle="modal" data-target=".edit" data-id="<?php echo $id; ?>"></span>
						</div>
					</div>
