<?php

/**
 * This is the model class for table "{{invoice}}".
 *
 * The followings are the available columns in table '{{invoice}}':
 * @property integer $invoice_id
 * @property integer $project_id
 * @property string $date
 * @property double $amount
 * @property integer $created_by
 * @property string $created_date
 */
class Invoice extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{invoice}}';
	}
	
	public $fromdate;
	public $todate;

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('project_id,date, amount,inv_no', 'required', 'message' => 'Please Enter {attribute}.'),
			array('project_id,date', 'required', 'message' => 'Choose {attribute}.'),
			array('project_id, created_by', 'numerical', 'integerOnly'=>true),
			array('inv_no', 'unique'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('invoice_id,fromdate,todate,inv_no,fees,subtotal, project_id, date, amount, created_by, created_date, invoice_status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'project' => array(self::BELONGS_TO, 'Projects', 'project_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		
		return array(
			'invoice_id' => 'Invoice',
			'inv_no' => 'Invoice Number',
			'project_id' => 'Project',
			'date' => 'Date',
			'amount' => 'Grand Total',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
			'fees' => 'Fees',
			'subtotal' => 'SubTotal',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('invoice_id',$this->invoice_id);
		$criteria->compare('inv_no',$this->inv_no);
		$criteria->compare('project_id',$this->project_id);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('amount',$this->amount);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('fees',$this->fees,true);
		$criteria->compare('subtotal',$this->subtotal,true);
                $criteria->addCondition('project_id IS NOT NULL');
                $criteria->order = 'invoice_id DESC';
                $criteria->compare('invoice_status',$this->invoice_status);
                $criteria->order = 'invoice_id DESC, created_date DESC';
				$user = Users::model()->findByPk(Yii::app()->user->id);
                $arrVal = explode(',', $user->company_id);
				if(!ANCAPMS_LINKED){
					$newQuery = "";
					foreach($arrVal as $arr) {
						if ($newQuery) $newQuery .= ' OR';
						$newQuery .= " FIND_IN_SET('".$arr."', company_id)";
					}
					$criteria->addCondition($newQuery);
				}
                        
		if(!empty($this->fromdate) && empty($this->todate))
		{
			$criteria->addCondition("date >= '".date('Y-m-d',strtotime($this->fromdate))."'");  // date is database date column field
		}elseif(!empty($this->todate) && empty($this->fromdate))
		{
			$criteria->addCondition("date <= '".date('Y-m-d',strtotime($this->todate))."'");
		}elseif(!empty($this->todate) && !empty($this->fromdate))
		{
			$criteria->addCondition("date between '".date('Y-m-d',strtotime($this->fromdate))."' and  '".date('Y-m-d',strtotime($this->todate))."'");
		}
		//echo "<pre>";print_r($criteria);die;
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            //'pagination' => array('pageSize' => 10,),
            'pagination' =>false,
                       
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Invoice the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
