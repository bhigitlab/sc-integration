<?php
/* @var $this QuotationCategoryMasterController */
/* @var $model QuotationCategoryMaster */

$this->breadcrumbs=array(
	'Quotation Work Type Masters'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List QuotationWorkTypeMaster', 'url'=>array('index')),
	array('label'=>'Manage QuotationWorkTypeMaster', 'url'=>array('admin')),
);
?>

<h1>Create QuotationWorkTypeMaster</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>