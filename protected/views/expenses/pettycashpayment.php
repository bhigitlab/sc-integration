
<div class="container">    
    <div class="expenses-heading">
        <div class="clearfix">
            <div class="pull-right">
                <label>&nbsp;</label>
                <select class="form-control payment_method">
                    <option value="">Select Payment Method</option>
                    <option value="1">Daybook</option>
                    <option value="2">Dailyexpense</option>
                </select>                    
            </div>
            <h2 class="pull-left">PETTY CASH PAYMENT</h2>
        </div>
        <br>
        
        <div class="expense_form hide">
            <?php echo $this->renderPartial('_expense_form',array('model'=>$model))?>
        </div>
        <div class="dailyexpense_form hide">
            <?php echo $this->renderPartial('_dailyexpense_form',array('model'=>$dailymodel))?>
        </div>
    </div>    
</div>

<script>
    $(".payment_method").change(function(){
        var payment_method = $(this).val();
        if(payment_method == 1){
            $(".expense_form").removeClass("hide");
            $(".dailyexpense_form").addClass("hide");
        }else{
            $(".dailyexpense_form").removeClass("hide");
            $(".expense_form").addClass("hide");
        }
    })
</script>
