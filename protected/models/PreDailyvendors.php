<?php

/**
 * This is the model class for table "{{pre_dailyvendors}}".
 *
 * The followings are the available columns in table '{{pre_dailyvendors}}':
 * @property integer $daily_v_id
 * @property integer $ref_id
 * @property integer $followup_id
 * @property string $record_grop_id
 * @property integer $vendor_id
 * @property integer $project_id
 * @property double $amount
 * @property string $description
 * @property string $date
 * @property integer $payment_type
 * @property integer $bank
 * @property string $cheque_no
 * @property double $sgst
 * @property double $sgst_amount
 * @property double $cgst
 * @property double $cgst_amount
 * @property double $igst
 * @property double $igst_amount
 * @property double $tds
 * @property double $tds_amount
 * @property double $tax_amount
 * @property double $paidamount
 * @property integer $created_by
 * @property string $created_date
 * @property integer $updated_by
 * @property string $updated_date
 * @property integer $company_id
 * @property integer $reconciliation_status
 * @property string $reconciliation_date
 * @property double $budget_percentage
 * @property string $approval_status
 * @property integer $approved_by
 * @property string $record_action
 * @property string $approve_notify_status
 * @property integer $cancelled_by
 * @property string $cancelled_at
 *
 * The followings are the available model relations:
 * @property Users $approvedBy
 * @property Bank $bank0
 * @property Users $cancelledBy
 * @property Company $company
 * @property Users $createdBy
 * @property Projects $project
 * @property Users $updatedBy
 * @property Vendors $vendor
 */


class PreDailyvendors extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{pre_dailyvendors}}';
	}

        
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ref_id, record_grop_id, amount, description, date, created_by,record_action', 'required'),
			array('ref_id, followup_id, vendor_id, project_id, payment_type, bank, created_by, updated_by, company_id, reconciliation_status, approved_by, cancelled_by,purchase_id', 'numerical', 'integerOnly'=>true),
			array('amount, sgst, sgst_amount, cgst, cgst_amount, igst, igst_amount, tds, tds_amount, tax_amount, paidamount, budget_percentage', 'numerical'),
			array('cheque_no', 'length', 'max'=>100),
			array('approval_status, approve_notify_status', 'length', 'max'=>1),
			array('record_action', 'length', 'max'=>300),
			array('created_date, updated_date, reconciliation_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('daily_v_id, ref_id, followup_id, record_grop_id, vendor_id, project_id, amount, description, date, payment_type, bank, cheque_no, sgst, sgst_amount, cgst, cgst_amount, igst, igst_amount, tds, tds_amount, tax_amount, paidamount, created_by, created_date, updated_by, updated_date, company_id, reconciliation_status, reconciliation_date, budget_percentage, approval_status, approved_by, record_action, approve_notify_status, cancelled_by, cancelled_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'approvedBy' => array(self::BELONGS_TO, 'Users', 'approved_by'),
			'bank0' => array(self::BELONGS_TO, 'Bank', 'bank'),
			'cancelledBy' => array(self::BELONGS_TO, 'Users', 'cancelled_by'),
			'company' => array(self::BELONGS_TO, 'Company', 'company_id'),
			'createdBy' => array(self::BELONGS_TO, 'Users', 'created_by'),
			'project' => array(self::BELONGS_TO, 'Projects', 'project_id'),
			'updatedBy' => array(self::BELONGS_TO, 'Users', 'updated_by'),
			'vendor' => array(self::BELONGS_TO, 'Vendors', 'vendor_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'daily_v_id' => 'Daily V',
			'ref_id' => 'Ref',
			'followup_id' => 'Followup',
			'record_grop_id' => 'Record Grop',
			'vendor_id' => 'Vendor',
			'project_id' => 'Project',
			'amount' => 'Amount',
			'description' => 'Description',
			'date' => 'Date',
			'payment_type' => 'Payment Type',
			'bank' => 'Bank',
			'cheque_no' => 'Cheque No',
			'sgst' => 'Sgst',
			'sgst_amount' => 'Sgst Amount',
			'cgst' => 'Cgst',
			'cgst_amount' => 'Cgst Amount',
			'igst' => 'Igst',
			'igst_amount' => 'Igst Amount',
			'tds' => 'Tds',
			'tds_amount' => 'Tds Amount',
			'tax_amount' => 'Tax Amount',
			'paidamount' => 'Paidamount',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
			'updated_by' => 'Updated By',
			'updated_date' => 'Updated Date',
			'company_id' => 'Company',
			'reconciliation_status' => 'Reconciliation Status',
			'reconciliation_date' => 'Reconciliation Date',
			'budget_percentage' => 'Budget Percentage',
			'approval_status' => 'Approval Status',
			'approved_by' => 'Approved By',
			'record_action' => 'Record Action',
			'approve_notify_status' => 'Approve Notify Status',
			'cancelled_by' => 'Cancelled By',
			'cancelled_at' => 'Cancelled At',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('daily_v_id',$this->daily_v_id);
		$criteria->compare('ref_id',$this->ref_id);
		$criteria->compare('followup_id',$this->followup_id);
		$criteria->compare('record_grop_id',$this->record_grop_id,true);
		$criteria->compare('vendor_id',$this->vendor_id);
		$criteria->compare('project_id',$this->project_id);
		$criteria->compare('amount',$this->amount);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('payment_type',$this->payment_type);
		$criteria->compare('bank',$this->bank);
		$criteria->compare('cheque_no',$this->cheque_no,true);
		$criteria->compare('sgst',$this->sgst);
		$criteria->compare('sgst_amount',$this->sgst_amount);
		$criteria->compare('cgst',$this->cgst);
		$criteria->compare('cgst_amount',$this->cgst_amount);
		$criteria->compare('igst',$this->igst);
		$criteria->compare('igst_amount',$this->igst_amount);
		$criteria->compare('tds',$this->tds);
		$criteria->compare('tds_amount',$this->tds_amount);
		$criteria->compare('tax_amount',$this->tax_amount);
		$criteria->compare('paidamount',$this->paidamount);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('updated_by',$this->updated_by);
		$criteria->compare('updated_date',$this->updated_date,true);
		$criteria->compare('company_id',$this->company_id);
		$criteria->compare('reconciliation_status',$this->reconciliation_status);
		$criteria->compare('reconciliation_date',$this->reconciliation_date,true);
		$criteria->compare('budget_percentage',$this->budget_percentage);
		$criteria->compare('approval_status',$this->approval_status,true);
		$criteria->compare('approved_by',$this->approved_by);
		$criteria->compare('record_action',$this->record_action,true);
		$criteria->compare('approve_notify_status',$this->approve_notify_status,true);
		$criteria->compare('cancelled_by',$this->cancelled_by);
		$criteria->compare('cancelled_at',$this->cancelled_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PreDailyvendors the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
