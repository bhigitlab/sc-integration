<?php

class LogController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout = '//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{


		$accessArr = array();
		$accessauthArr = array();
		$controller = Yii::app()->controller->id;
		if (isset(Yii::app()->user->menuall)) {
			if (array_key_exists($controller, Yii::app()->user->menuall)) {
				$accessArr = Yii::app()->user->menuall[$controller];
			}
		}

		if (isset(Yii::app()->user->menuauth)) {
			if (array_key_exists($controller, Yii::app()->user->menuauth)) {
				$accessauthArr = Yii::app()->user->menuauth[$controller];
			}
		}
		$access_privlg = count($accessauthArr);
		return array(
			array(
				'allow', // allow all users to access 'Cronedeletelog' action
				'actions' => array('Cronedeletelog','PendingApiRequestCrone'),
				'users' => array('*'), // allow access to all users
			),
			array(
				'allow', // allow all users to perform 'index' and 'view' actions
				'actions' => array('databackup',),
				'users' => array('@'),
				/* 'expression' => "$access_privlg > 0", */
			),

			array(
				'allow', // allow all users to perform 'index' and 'view' actions
				'actions' => $accessArr,
				'users' => array('@'),
				'expression' => "$access_privlg > 0",
			),

			array(
				'allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions' => $accessauthArr,
				'users' => array('@'),
				'expression' => "$access_privlg > 0",
			),
			array(
				'deny',  // deny all users
				'users' => array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view', array(
			'model' => $this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model = new JpLog;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if (isset($_POST['JpLog'])) {
			$model->attributes = $_POST['JpLog'];
			if ($model->save())
				$this->redirect(array('view', 'id' => $model->log_id));
		}

		$this->render('create', array(
			'model' => $model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	/*public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['JpLog']))
		{
			$model->attributes=$_POST['JpLog'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->log_id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}*/
	/*public function actionUpdatelog()
	{
            $model = new JpLog;
            $this->render('updatelog',array(
                'model'=>$model,
            ));
	}
        public function actionDeletelog()
	{
            $model = new JpLog;
            $this->render('deletelog',array(
                'model'=>$model,
            ));
	}*/
	public function actionHistorylog()
	{
		$company = Yii::app()->user->company_id;

		$model = new JpLog('search');

		//$model->unsetAttributes();

		if(isset($_GET['JpLog']))
			$model->attributes = $_GET['JpLog'];

		$this->render('updatelog',array(
			'model'=>$model,
			'dataProvider'=>$model->search(),
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	/*public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}*/

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('JpLog');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new JpLog('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['JpLog']))
			$model->attributes=$_GET['JpLog'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return JpLog the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=JpLog::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	public function actionDatabackup()
	{
		$logId = $_REQUEST['logId'];
		$sql = "SELECT * FROM  " . $this->tableNameAcc('log', 0) . "  WHERE `log_id`= $logId";
		$logData = Yii::app()->db->createCommand($sql)->queryRow();
		$restoreData = $logData['log_data'];
		$restoreTable = $logData['log_table'];
		$json =  $restoreData;
		$multiple = json_decode($json, true);
		$restore = array();

		foreach ($multiple  as $key => $value) {

			$restore[$key] = ($value != null) ? "'$value'" : "NULL";
		}

		if ($restoreTable == "" . $this->tableNameAcc('subcontractor_payment', 0) . "") {
			if (!array_key_exists("quotation_number", $restore)) {
				$restore['quotation_number'] = "NULL";
				$restore['payment_quotation_status'] = "2";
			}
		}

		$key =  implode(", ", array_keys($restore));
		$keyval =  implode(", ", array_values($restore));

		$restoreSql = "INSERT INTO $restoreTable ($key) VALUES ($keyval)";
		$removeSql = "DELETE FROM " . $this->tableNameAcc('log', 0) . " WHERE log_id=$logId";
		$insertQuery = Yii::app()->db->createCommand($restoreSql)->execute();
		if ($insertQuery == 1) {
			Yii::app()->db->createCommand($removeSql)->execute();
			Yii::app()->user->setFlash('success', "Data Restored Successfully");
		} else {
			Yii::app()->user->setFlash('error', "Data Restore Error!");
		}
	}
	/**
	 * Performs the AJAX validation.
	 * @param JpLog $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='jp-log-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	public function actionCronedeletelog(){
		$model= CroneDeleteLogSettings::model()->findByPk(1);
		if(!empty($model)){
			if(!empty($model->no_of_days)){
				$no_of_days_taken=$model->no_of_days;
				$today = new DateTime(); // Get today's date as a DateTime object
				$today->modify('-' . $no_of_days_taken . ' days'); // Subtract the number of days
				$resulting_date = $today->format('Y-m-d');
				if(!empty($resulting_date)){
					$sql = "DELETE FROM jp_log WHERE log_datetime < :resulting_date";
					$command = Yii::app()->db->createCommand($sql);
					$command->bindValue(':resulting_date', $resulting_date, PDO::PARAM_STR);
					$exec_field = $command->execute(); // Use execute() for DELETE operation
					if ($exec_field) {
						echo "Deleted Successfully";
					}
				}

			}
		}
	}
	public function actionPendingApiRequestCrone(){
		
        echo ApiLogs::apiPendingRequestRun();
    }
}
