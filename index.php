<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
// change the following paths if necessary
$env = dirname(__FILE__) . '/protected/config/env.php';
require_once($env);
// change the following paths if necessary
$yii = dirname(__FILE__) . '/framework/yii.php';
$config = dirname(__FILE__) . '/protected/config/main.php';

@date_default_timezone_set('Asia/Kolkata');

if (ENV != 'production') {
    // remove the following lines when in production mode
    defined('YII_DEBUG') or define('YII_DEBUG', true);
    // specify how many levels of call stack should be shown in each log message
    defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 3);
}
define('LIVEURL', 'http://localhost/bhi/jpv/');

require_once($yii);
Yii::createWebApplication($config)->run();
