<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/validation/jquery.validate.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/validation/additional-methods.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/tableHeadFixer.js"></script>

<div class="container">
    <div class="expenses-heading header-container">
    <h3>LABOUR ENTRY</h3>
    <?php if(($model['approve_status'] != 1) && $model['approve_status'] != 3){ ?>
        <div class="btn-container">
        <button id="<?php echo $model['dr_id']; ?>" data-id="<?php echo $model['dr_id']; ?>" style="" class="btn btn-xs btn-info approve approveoption_<?php echo $model['dr_id']; ?>">Approve</button>
        </div>
    
    <?php } ?>
    </div>
    <div id="errormessage"></div>
    <br><br>
    <div class="row">
        <div class="col-md-6">
            <p><strong>PROJECT :</strong> <?php 
                $pmodel = Projects::model()->findByPk($model->projectid);
                echo isset($pmodel->name) ? CHtml::encode($pmodel->name) : 'N/A';
            ?></p>
            <p><strong>COMPANY :</strong> <?php 
                $cmodel = Company::model()->findByPk($model->company_id);
                echo isset($cmodel->name) ? CHtml::encode($cmodel->name) : 'N/A';
            ?></p>
            <p><strong>DESCRIPTION :</strong> <?= CHtml::encode($model->description); ?></p>
        </div>
        <div class="col-md-6">
            <p><strong>DATE :</strong> <?= CHtml::encode(date('d-m-Y', strtotime($model->date))); ?></p>
            <p><strong>STATUS :</strong> <?= CHtml::encode(in_array($model->approve_status ,[1,3])? 'Approved' : 'Pending'); ?></p>
            <p><strong>REMARKS :</strong> <?= CHtml::encode($model->remarks); ?></p>
        </div>
    </div>
     <ul class="legend">
            <li><span style="background-color:yellow;"></span> Labour Not Estimated For This Project in Estimation</li>
        </ul>
    <table class="table table-bordered">
        <thead>
            <tr>
                <th>Sl.No</th>
                <th>Work Type</th>
                <th>Labour Count</th>
                <th>Amount</th>
            </tr>
        </thead>
        <tbody>
           

            <?php 
            $bgcolor_status='';
            $totalAmount = 0; // Initialize total amount
            
            foreach ($labourEntries as $index => $entry): 
                $totalAmount += $entry['labour_total_rate']; // 
                // Accumulate the total
                $labour=$entry["labour_id"];
            $sql="SELECT m.id,m.labour,m.quantity,m.amount,m.used_quantity,m.used_amount FROM `jp_labour_estimation` m LEFT JOIN `jp_itemestimation` e ON e.itemestimation_id=m.estimation_id WHERE e.project_id=".$model->projectid." AND e.itemestimation_status=2  AND m.project_id=".$model->projectid ." AND m.labour=".$entry["labour_id"];
            $estimated_det = Yii::app()->db->createCommand($sql)->queryRow();
            $labour_det = LabourWorktype::model()->findByPk($labour);
            $labour_name=$labour_det["worktype"];
           
            $estimated_det = Yii::app()->db->createCommand($sql)->queryRow();

            if (!$estimated_det) {
                $bgcolor_status = 'style ="background-color:yellow"';
            }
            ?>
                <tr>
                    <td><?= $index + 1; ?></td>
                    <td <?php echo $bgcolor_status; ?>><?= CHtml::encode($entry['worktype']); ?></td>
                    <td><?= CHtml::encode($entry['labour_count']); ?></td>
                    <td><?= CHtml::encode(number_format($entry['labour_total_rate'], 2)); ?></td>
                </tr>
            <?php endforeach; ?>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="3" style="text-align: right;"><strong>Total Amount:</strong></td>
                <td><strong><?= CHtml::encode(number_format($totalAmount, 2)); ?></strong></td>
            </tr>
        </tfoot>
    </table>
</div>

<style>
    .container {
        width: 80%;
        margin: auto;
    }
    .table {
        width: 100%;
        margin-top: 20px;
        border-collapse: collapse;
    }
    .table, .table th, .table td {
        border: 1px solid #000;
        text-align: center;
    }
    .total {
        margin-top: 20px;
        text-align: right;
    }
</style>
<script>
     $(document).on('click', '.approve', function (e) {
        e.preventDefault();

        var element = $(this);
        var item_id = $(this).attr('id');
        var includeInReport = confirm("Whether this entry should be added to project cost? ");
        var includeInReportValue = includeInReport ? 1 : 0;
        // alert(includeInReportValue);
        $('.loading-overlay').addClass('is-active');
        $.ajax({
            url: '<?php echo Yii::app()->createAbsoluteUrl('dailyReport/approve'); ?>',
            type: 'POST',
            dataType: 'json',
            data: {
                item_id: item_id,
                includeInReportValue: includeInReportValue,
            },
            success: function (response) {
                // alert('hi');
                if (response.response == 'success') {
                    $(".approveoption_" + item_id).hide();
                    element.closest("tr").removeClass("approve_section");
                    element.closest("tr").addClass("tobe_approve");
                    element.closest("tr").removeClass("row_class");
                    $("#errormessage").show()
                        .html('<div class="alert alert-success"><strong>Success!</strong>' + response.msg + '</div>')
                        .fadeOut(5000, function () {
                            location.reload();
                        });
                } else if (response.response == 'warning') {

                    var warningAlert = '<div class="alert alert-warning alert-dismissible" role="alert">' +
                           '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                               '<span aria-hidden="true">&times;</span>' +
                           '</button>' +
                           '<strong>Sorry!</strong> ' + response.msg +
                       '</div>';
                    $("#errormessage").html(warningAlert).show();
                } else {
                    $("#errormessage").show()
                        .html('<div class="alert alert-danger"><strong>Sorry!</strong> ' + response.msg + '</div>')
                        .fadeOut(5000);
                }
            }
        });
    });
</script>
