<!--<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" onclick="javascript:window.location.reload()">&times;</button>
            <h4 class="modal-title">Add Company</h4>
        </div>
        <?php //echo $this->renderPartial('_form', array('model' => $model)); ?>
    </div>

</div>-->
<div class="panel panel-gray">
    <div class="panel-heading form-head">
        <h3 class="panel-title">Add Company</h3>
    </div>
    <?php echo $this->renderPartial('_form', array('model' => $model)); ?>
</div>
