SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


CREATE TABLE `jp_project_template` (
  `template_id` int(11) NOT NULL,
  `template_name` varchar(100) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '0',
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



INSERT INTO `jp_project_template` (`template_id`, `template_name`, `status`, `description`) VALUES
(1, 'AGAC', '0', 'AGAC'),
(2, 'D2R', '0', 'D2R'),
(3, 'AMZER', '1', 'AMZER');


ALTER TABLE `jp_project_template`
  ADD PRIMARY KEY (`template_id`);

ALTER TABLE `jp_project_template`
  MODIFY `template_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;
