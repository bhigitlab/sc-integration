<?php

/**
 * This is the model class for table "{{purchase_items_import}}".
 *
 * The followings are the available columns in table '{{purchase_items_import}}':
 * @property integer $import_id
 * @property string $main_category
 * @property string $sub_category
 * @property string $brand
 * @property string $specification_type
 * @property string $specification
 * @property string $unit
 * @property string $hsn_code
 * @property string $company
 * @property integer $created_by
 * @property string $created_on
 */
class PurchaseItemsImport extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{purchase_items_import}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('main_category, brand, specification, unit, company', 'required'),
            array('created_by', 'numerical', 'integerOnly'=>true),
            array('main_category, sub_category, brand, unit, company', 'length', 'max'=>100),
            array('specification', 'length', 'max'=>255),
            array('hsn_code', 'length', 'max'=>200),
            array('created_on', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('import_id, main_category, sub_category, brand, specification_type, specification, unit, hsn_code, company, created_by, created_on', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'import_id' => 'Import',
            'main_category' => 'Main Category',
            'sub_category' => 'Sub Category',
            'brand' => 'Brand',
            'specification_type' => 'Specification Type',
            'specification' => 'Specification',
            'unit' => 'Unit',
            'hsn_code' => 'Hsn Code',
            'company' => 'Company',
            'created_by' => 'Created By',
            'created_on' => 'Created On',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('import_id',$this->import_id);
        $criteria->compare('main_category',$this->main_category,true);
        $criteria->compare('sub_category',$this->sub_category,true);
        $criteria->compare('brand',$this->brand,true);
        $criteria->compare('specification_type',$this->specification_type,true);
        $criteria->compare('specification',$this->specification,true);
        $criteria->compare('unit',$this->unit,true);
        $criteria->compare('hsn_code',$this->hsn_code,true);
        $criteria->compare('company',$this->company,true);
        $criteria->compare('created_by',$this->created_by);
        $criteria->compare('created_on',$this->created_on,true);
        $criteria->order = 'import_id DESC';
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => isset($_GET['pageSize']) ? (int) $_GET['pageSize'] : 5,    
            ),
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return PurchaseItemsImport the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}