<?php

/**
 * This is the model class for table "{{subcontractorbill}}".
 *
 * The followings are the available columns in table '{{subcontractorbill}}':
 * @property integer $id
 * @property integer $scquotation_id
 * @property string $bill_number
 * @property string $date
 * @property double $amount
 * @property double $tax_amount
 * @property double $total_amount
 * @property integer $company_id
 * @property integer $created_by
 * @property string $created_date
 * @property string $updated_date
 * @property integer $status
 */
class Subcontractorbill extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public $date_from;
	public $date_to;
	public $created_date_from;
	public $created_date_to;
	public $scquotation_no;
	public $subcontractor_name;
	public $project_id;
	public $vendor_id;
	
	public function tableName()
	{
		return '{{subcontractorbill}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('scquotation_id', 'required', 'message'=>'Please fill above field.'),
			array(' bill_number, date, company_id, created_by, created_date', 'required', 'message'=>'{attribute} cannot be blank.'),
			array('scquotation_id, company_id, created_by, status,stage_id', 'numerical', 'integerOnly'=>true),
			array('amount, tax_amount, total_amount', 'numerical'),
			array('bill_number', 'length', 'max'=>100),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, scquotation_id, bill_number,project_id, date, amount, scquotation_no,tax_amount, total_amount, company_id, created_by, created_date, updated_date, status,stage_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'quotation' => array(self::BELONGS_TO, 'Scquotation', 'scquotation_id'),
                    'company' => array(self::BELONGS_TO, 'Company', 'company_id'),
                    'user' => array(self::BELONGS_TO, 'Users', 'created_by'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'scquotation_id' => 'SC Quotation',
			'bill_number' => 'Bill No',
			'date' => 'Date',
			'amount' => 'Amount',
			'tax_amount' => 'Tax Amount',
			'total_amount' => 'Total Amount',
			'company_id' => 'Company',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
			'updated_date' => 'Updated Date',
			'status' => 'Status',
			'stage_id'=>'Stage'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('scquotation_id',$this->scquotation_id);
		$criteria->compare('bill_number',$this->bill_number,true);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('amount',$this->amount);
		$criteria->compare('tax_amount',$this->tax_amount);
		$criteria->compare('total_amount',$this->total_amount);
		$criteria->compare('t.company_id', $this->company_id);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('updated_date',$this->updated_date,true);
		$criteria->compare('status',$this->status);
		$userId         = Yii::app()->user->id;
		$user = Users::model()->findByPk(Yii::app()->user->id);
		$tblpx = Yii::app()->db->tablePrefix;
		$arrVal = explode(',', $user->company_id);
		$newQuery = "";
		$newQuery1 = " AND 1=1";
		if($this->company_id!=''){
			$newQuery .= "t.company_id =" . $this->company_id;
		}else{
			foreach ($arrVal as $arr) {
				if ($newQuery) $newQuery .= ' OR';
				$newQuery .= " FIND_IN_SET('" . $arr . "', t.company_id)";
			}
		}
		if (!empty($this->date_from) && empty($this->date_to)) {
			$newQuery1 .= " AND t.date  >= '" . date("Y-m-d", strtotime($this->date_from)) . "'";
		} elseif (!empty($this->date_to) && empty($this->date_from)) {
			$newQuery1 .= " AND t.date  <= '" . date("Y-m-d", strtotime($this->date_to)) . "'";
		} elseif (!empty($this->date_to) && !empty($this->date_from)) {
			$newQuery1 .= " AND t.date between '" . date('Y-m-d', strtotime($this->date_from)) . "' and  '" . date('Y-m-d', strtotime($this->date_to)) . "'";
		}
		
		if (!empty($this->scquotation_no)) {
			$newQuery1 .= " AND pu.scquotation_no ='" . $this->scquotation_no . "' ";
		}

		if (!empty($this->subcontractor_name)) {
			$newQuery1 .= " AND spr.subcontractor_name ='" . $this->subcontractor_name . "' ";
		}
		
		if (!empty($this->project_id)) {
			$newQuery1 .= " AND pu.project_id =" . $this->project_id ;
		}

		
		$criteria->select = 't.*,pu.scquotation_no,pu.project_id,spr.subcontractor_name';
		$criteria->join = 'LEFT JOIN ' . $tblpx . 'scquotation pu ON t.scquotation_id = pu.scquotation_id  LEFT JOIN ' . $tblpx . 'projects pr ON pr.pid = pu.project_id LEFT JOIN ' . $tblpx . 'project_assign pa ON pr.pid = pa.projectid LEFT JOIN ' . $tblpx . 'subcontractor spr ON spr.subcontractor_id = pu.subcontractor_id';
		$criteria->addCondition('pa.userid = ' . $userId . ' AND (' . $newQuery . ') ' . $newQuery1 . '');
		
		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'pagination' => array('pageSize' => 20,),
			//   'pagination'=>false
			'sort'=>array(
	  
			  'defaultOrder'=>'id DESC',
			
			),
		));

		
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Subcontractorbill the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
