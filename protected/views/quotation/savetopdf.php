<?php
$company = Yii::app()->db->createCommand("SELECT address,phone,email_id FROM ".Yii::app()->db->tablePrefix."company WHERE id='".Yii::app()->user->company_id."'")->queryRow();
//print_r($company);exit;
?>
<link rel="stylesheet" type="text/css" href="uploads/new_template/css/newstyle.css" />
	 
<div class="container">


	
	<table style="float: left;color: #87050">
        <tr>
            <td>
                <h2><?php echo Yii::app()->name; ?></h2>
            </td>
        </tr>
        <tr>
            <td>
                <?php echo $company['address']; ?>
            </td>
        </tr>
        
    </table>
         
    <table style="float: right;margin-left: 450px;margin-top:-100px;color: #87050" align="right">
        <tr>
            <td>
                <?php if(Yii::app()->params['website_name']){ echo "Website:".Yii::app()->params['website_name'];} ?><br>
                <?php if($company['email_id']){echo "Email:".$company['email_id']; }?><br>
                <?php if($company['phone']){echo "Phone:".$company['phone']; }?><br>
            </td>
        </tr>
    </table>
    <br>
    <hr>
	<?php  
        if(!empty($project_id)){
            echo "Project ".$project_id;
        }
        if(!empty($from_date)){
        echo "From ". $from_date.' to '.$to_date;
        }
        ?>
	

	
	<table border="1" style="width:100%;font-size: 10px;">
			<thead>
				<tr>
					<th>Sl.No</th>
                                        <th>Quotation #</th>
                                        <th>Client Name</th>
                                        <th>Date</th>
                                        <th align="center">Sub Total</th>
                                        <th align="center">Tax Total</th>
                                        <th align="center">Total Amount</th>
                                        
				</tr>
			</thead>
			<tbody class="addrow">
				<?php 
                                $i = 1;
				 foreach ($expense_entries as $data) {
                                      $pmodel  = Clients::model()->findBypk($data['client_id']);	
                                      $clientname = $pmodel->name;
				 ?>
				<tr>
					<td><?php echo $i; ?></td>
                                        <td><?php echo $data['inv_no']; ?></td>
					<td><?php echo $clientname; ?></td>
					<td><?php echo date("d-m-Y", strtotime($data['date'])); ?></td>
                                        <td align="right"><?php echo number_format((float)$data['subtotal'], 2, '.', ''); ?></td>
                                        <td align="right"><?php echo number_format((float)$data['tax_amount'], 2, '.', ''); ?></td>
					<td align="right"><?php echo number_format((float)$data['amount'] + $data['tax_amount'], 2, '.', ''); ?></td>
                                       
				</tr>
				<?php
			  $i++;
                                }
				?>
			</tbody>
	</table>
	<br/><br/>
        
	
	
</div>


