<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.min.js"></script>
<style>
    .add-btn,.remove-btn{
        text-align: right;
        margin-top: 21px;
        margin-right:2px;
        font-weight: bolder;
        border-radius:2px;
    }
</style>
<div class="form">
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'consumption-form',
        'enableAjaxValidation' => false,
    ));
    ?>
    <input type="hidden" id="consumption_id" name="Model[id]" value="<?php echo $model->id; ?>">

    <div class="entries-wrapper">
        <div class="row">
            <div class="col-xs-12">
                <div class="heading-title">Add Consumption Request</div>
                <div class="dotted-line"></div>
            </div>
        </div>
        
            
            <div class="row">
                <!-- Project Dropdown -->
                <div class="form-group col-xs-12 col-md-2">
                    <div class="form-group">
                        <label for="project">Project</label>
                        <?php
                        echo $form->dropDownList($model, 'coms_project_id', CHtml::listData(Projects::model()->findAll(array(
                            'select' => array('pid, name'),
                            'order' => 'name ASC',
                        )), 'pid', 'name'), array(
                            'class' => 'form-control js-example-basic-single',
                            'empty' => '-Select Project-',
                            'style' => 'width:100%;',
                            // Model value will automatically pre-select in edit case
                        ));
                        ?>
                    </div>
                </div>
                <!-- Warehouse Dropdown -->
                <div class="form-group col-xs-12 col-md-2">
                    <div class="form-group">
                        <label for="warehouse">Warehouse</label>
                        <?php
                        echo $form->dropDownList($model, 'warehouse_id', CHtml::listData(Warehouse::model()->findAll(array(
                            'select' => array('warehouse_id, warehouse_name'),
                            'order' => 'warehouse_name ASC',
                        )), 'warehouse_id', 'warehouse_name'), array('class' => 'form-control js-example-basic-single', 'empty' => '-Select Warehouse-', 'style' => 'width:100%;'));
                        ?>
                    </div>
                </div>
                <div class="form-group col-xs-12 col-md-2">
                    <div class="form-group">
                        <label for="date">Date</label>
                        <?php
                        $savedDate = !empty($model->date) ? date('d-m-Y', strtotime($model->date)) : date('d-m-Y');
                        echo $form->textField($model, 'date', array(
                            'class' => 'form-control',
                            'type' => 'date', 
                            'style' => 'width:100%;',
                            'value' => $savedDate, 
                        ));
                        ?>
                    </div>
                </div>
            </div>
            <div id="dynamicInputs" style="width: 100%;"></div>
            <!-- Save Button -->
            <div class="row">
                <div class="form-group col-xs-12 text-right save-btnHold">
                    <label style="display:block;">&nbsp;</label>
                    <?php if ($model->status != 1):?>
                    <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-primary submit', 'id' => 'submitButton')); ?>
                    <?php
                    if (!$model->isNewRecord) {
                        echo CHtml::ResetButton('Close', array('onclick' => 'closeaction(this,event);', 'class' => 'btn btn-other'));
                    } else {
                        echo CHtml::ResetButton('Reset', array('style' => 'margin-right:3px;', 'class' => 'btn btn-default'));
                        echo CHtml::ResetButton('Close', array('onclick' => 'closeaction(this,event);', 'class' => 'btn btn-other'));
                    }
                    ?>
                    <?php endif; ?>
                </div>
            </div>
      
    </div>

    <?php $this->endWidget(); ?>
</div>

<script>

$(function() {
        $("#ConsumptionRequest_date").datepicker({
            dateFormat: 'dd-mm-yy',
            maxDate: new Date()
        });
    });



$(document).ready(function() {
    var consumptionId = $('#consumption_id').val();

    if(consumptionId==''){
       // addNewInputRow(true);
    }
    else{
        getFieldsForEdit(consumptionId);
    }
    
});

$('#dynamicInputs').on('click', '.add-btn', function() {
        addNewInputRow(false);
});
$('#dynamicInputs').on('click', '.remove-btn', function() {
            $(this).closest('.dynamic-row').remove();
    });
    
function addNewInputRow(isFirstRow) {
    let index = $('#dynamicInputs .dynamic-row').length;
    if(index === '') {
        index = 0;
    }
    var project = $("#ConsumptionRequest_coms_project_id").val();
    //alert(project);
    $.ajax({
        url: '<?php echo $this->createUrl("warehouse/getNewInputRow"); ?>',
        type: 'POST',
        data: { index: index, project:project },
        success: function(response) {
             $('#dynamicInputs').append(response);
            $('.coms_material_id').select2();
            $('.specification_id').select2();
            $('.item_rate').select2();
            
        }
    });
}

    function handleRateChange(index) {
       
        var rateSelect = document.getElementById("PmsAccWprItemConsumed_item_rate_" + index);
        var selectedRate = rateSelect.value;
        var warehouseId = $('#ConsumptionRequest_warehouse_id').val();
       
        var specSelect = document.querySelector('select[name="PmsAccWprItemConsumed[materials][' + index + '][specification_id]"]');
        var selectedSpecification = specSelect ? specSelect.value : '';

       
        var materialSelect = document.querySelector('select[name="PmsAccWprItemConsumed[materials][' + index + '][material_id]"]');
        var selectedMaterial = materialSelect ? materialSelect.value : '';

        var project = $("#ConsumptionRequest_coms_project_id").val();
        var postData = {
            rate: selectedRate,
            specification_id: selectedSpecification,
            material_id: selectedMaterial,
            project:project,
            warehouseId:warehouseId
        };
        $('#rateQuantityError_' + index).html('');
       if( selectedRate !== '-1'){
            $.ajax({
                url: '<?php echo $this->createUrl('warehouse/getStockEstimatedQuantity'); ?>',
                type: 'POST',
                data: postData,
                dataType: 'json',
                success: function(response) {
                    
                    $('#rateQuantityError_' + index).html(response.msg);
                },
                error: function(xhr, status, error) {
                    console.error("AJAX error:", error);
                    $('#rateQuantityError_' + index).html("An error occurred.");
                }
            });

       }else{
        $('#rateQuantityError_' + index).html('');
       }
        
    }

function getFieldsForEdit(id){
    let index = $('#dynamicInputs .dynamic-row').length;
    if(index === '') {
        index = 0;
    }
        $('#loading').show();
        $.ajax({
            type: "GET",
            data: {
                id :id ,
                index:index,
            },
            url: "<?php echo $this->createUrl('warehouse/getfieldsForEdit'); ?>",
            success: function(response) {
                $('#dynamicInputs').append(response);
                $('.coms_material_id').select2();
                $('.specification_id').select2();
                $('.item_rate').select2();
            }
        });
    }


// $(document).ready(function() {
//     // Add new row
//     $(document).on('click', '.add-row', function() {
//         var $row = $(this).closest('.dynamic-row');
//         var $clone = $row.clone();
//         $clone.find('input, select').val('');
//         $clone.find('.remove-row').show();
//         $clone.find('.add-row').remove(); 
//         $('#dynamic-rows').append($clone);
//     });

//     // Remove row
//     $(document).on('click', '.remove-row', function() {
//         $(this).closest('.dynamic-row').remove(); // Remove the row
//     });
// });

    $(".js-example-basic-single").select2();
    $(document).ready(function() {
        $('.js-example-basic-multiple').select2({
            dropdownParent: $('#myModal'),
        });
    });
    $(document).ready(function(){
        $('#ConsumptionRequest_coms_project_id').change(function(){
            var projectId = $(this).val();
            $.ajax({
            type: 'POST',
            url: '<?php echo $this->createUrl('warehouse/GetWarehouses'); ?>',
            data: {projectId: projectId},
            dataType: 'json',
            success: function(response){
                if (response.status === 'success') {
                    var warehouses = response.warehouses;
                        var dropdown = $('#ConsumptionRequest_warehouse_id');
                        dropdown.empty();
                        dropdown.append('<option value="">-Select Warehouse-</option>');
                        warehouses.forEach(function(warehouse) {
                            dropdown.append('<option value="' + warehouse.id + '">' + warehouse.name + '</option>');
                        });
                } else {
                    // Handle error if needed
                }
               
            },
            error: function(){
                // Handle error
            }
            });
        });
        $(document).on('change', '.coms_material_id', function(){
            var material_id = $(this).val();
            var project_id =$('#ConsumptionRequest_coms_project_id').val();
            // Find the corresponding index from the name attribute of the selected material dropdown
            var nameAttr = $(this).attr('name');
            var index = nameAttr.match(/\[(\d+)\]/)[1]; // Extract index from name like [0], [1], etc.

            $.ajax({
                type: 'POST',
                url: '<?php echo $this->createUrl('warehouse/getSpecifications'); ?>',
                data: { material_id: material_id,project_id:project_id },
                dataType: 'json',
                success: function(response){
                    if (response.status === 'success') {
                        var specifications = response.specifications;
                        var rateDropdown = $('#PmsAccWprItemConsumed_item_rate_' + index);
                        rateDropdown.empty();
                        // Use the correct dropdown id for specifications based on the extracted index
                        var dropdown = $('#PmsAccWprItemConsumed_item_id_' + index);
                        
                        dropdown.empty();
                        dropdown.append('<option value="">-Select Specification-</option>');
                        specifications.forEach(function(spec) {
                            dropdown.append('<option value="' + spec.id + '">' + spec.specification + '</option>');
                        });
                    } else {
                        // Handle error if needed
                    }
                },
                error: function(){
                    // Handle error
                }
            });
        });

    });

    $('.specification_id').change(function() {
        updateRateBasedOnSelection();
    });
    $(document).on('change', '.specification_id', function() {
        updateRateBasedOnSelection();
    });


    $('#ConsumptionRequest_warehouse_id').change(function() {
        var index=0;
        updateRateBasedOnSelection(index);
         addNewInputRow(0);
    });

    $(document).on('change', '.specification_id', function() {
        var index = $(this).attr('id').split('_').pop(); 
        updateRateBasedOnSelection(index); 
    });

    function updateRateBasedOnSelection(index) {
        var specificationId = $('#PmsAccWprItemConsumed_item_id_' + index).val();
        var warehouseId = $('#ConsumptionRequest_warehouse_id').val(); 
         $('#rateQuantityError_' + index).html('');
        $.ajax({
            type: 'POST',
            url: '<?php echo $this->createUrl('warehouse/getRate'); ?>',
            data: {
                specification_id: specificationId,
                warehouse_id: warehouseId,
            },
            dataType: 'json',
            success: function(response) {
                var rateDropdown = $('#PmsAccWprItemConsumed_item_rate_' + index);
                var rateErrorDiv = $('#rateError_' + index);

                rateDropdown.empty();
                rateErrorDiv.text('');
                $('#saveChanges').prop('disabled', false); 
                 rateDropdown.append('<option value="-1" selected>Choose Rates</option>');
                if (response.status === 'success') {
                    response.rates.forEach(function(rate) {
                        rateDropdown.append('<option value="' + rate.rate + '">' + rate.rate + '</option>');
                    });
                } else {
                    // Show error message and disable the save button if there's an issue
                    rateErrorDiv.text(response.message);
                    $('#saveChanges').prop('disabled', true); 
                }
               // handleRateChange(index);
            },
            error: function(xhr, status, error) {
                console.error('AJAX Error:', status, error);
                alert('An error occurred while fetching the rate.');
            }
        });
    }
    function handleQuantityChange(element, index) {
    const quantity = parseInt(element.value, 10) || 0; // Parse quantity, default to 0 if invalid

    // Select the rate dropdown element by ID
    const rateDropdown = document.getElementById('PmsAccWprItemConsumed_item_rate_' + index);


    if (rateDropdown) {
        // Get the selected rate value
        const selectedRate = parseFloat(rateDropdown.value) || 0;

        // Calculate amount
        const amount = quantity * selectedRate;

        // Update the amount field
        const amountInput = document.querySelector(
            `input[name="PmsAccWprItemConsumed[materials][${index}][item_amount]"]`
        );

        if (amountInput) {
            amountInput.value = amount.toFixed(2); // Display amount with 2 decimal places
        } else {
            console.error(`Amount input field not found for index ${index}`);
        }
    } else {
        console.error(`Rate dropdown not found for index ${index}`);
    }
}

</script>
