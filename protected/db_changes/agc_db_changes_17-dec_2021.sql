SET FOREIGN_KEY_CHECKS=0;


TRUNCATE TABLE jp_purchase;
TRUNCATE TABLE jp_purchase_items;

TRUNCATE TABLE jp_warehousereceipt;
TRUNCATE TABLE jp_warehousereceipt_items;

TRUNCATE TABLE jp_warehousedespatch;
TRUNCATE TABLE jp_warehousedespatch_items;
TRUNCATE TABLE jp_warehousestock;



TRUNCATE TABLE jp_sales_invoice;
TRUNCATE TABLE jp_sales_invoice_item;
TRUNCATE TABLE jp_sales_invoice_sub_item;

TRUNCATE TABLE jp_pre_expenses;
TRUNCATE TABLE jp_billitem;
TRUNCATE TABLE jp_bills;
TRUNCATE TABLE jp_expenses;
TRUNCATE TABLE jp_additional_bill;

TRUNCATE TABLE jp_invoice;
TRUNCATE TABLE jp_invoice_list;

TRUNCATE TABLE jp_scquotation;
TRUNCATE TABLE jp_scquotation_items;