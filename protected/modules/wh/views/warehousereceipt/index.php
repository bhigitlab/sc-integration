<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$this->breadcrumbs = array(
    'Purchase List',
)
    ?>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/message/css/jquery.message.css">
<script type="text/javascript"
    src="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/message/js/jquery.message.js"></script>

<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/tableHeadFixer.js"></script>
<div class="container" id="project">
    <div class="expenses-heading">
        <div class="clearfix">
            <!-- remove addentries class -->
            <?php if ((isset(Yii::app()->user->role) && (in_array('/wh/warehousereceipt/create', Yii::app()->user->menuauthlist)))) { ?>
                <a href="<?php echo Yii::app()->createAbsoluteUrl('wh/warehousereceipt/create') ?>"
                    class="button btn btn-info pull-right mt-0 mb-10">
                    Add Receipt
                </a>
            <?php } ?>
            <h3>Warehouse Receipt</h3>
        </div>
    </div>
    <?php
    $this->renderPartial('_newsearch', array('model' => $model, 'warehouse' => $warehouse, 'bill_or_despatch_id' => $bill_or_despatch_id, 'transfer_type' => $transfer_type, 'vendor' => $vendor, 'clerk' => $clerk, 'specification' => $specification, 'item_id' => $item_id)) ?>

    <?php if (Yii::app()->user->hasFlash('success')): ?>
        <div class="info"
            style="width: 500px;margin-left: 400px;padding: 8px;color: green;font-size: 20px;font-weight: bold; padding-top: 46px;">
            <?php echo Yii::app()->user->getFlash('success'); ?>
        </div>
    <?php endif; ?>


    <?php if (Yii::app()->user->hasFlash('error')): ?>
        <div class="info"
            style="width: 500px;margin-left: 400px;padding: 8px;color: red;font-size: 20px;font-weight: bold; padding-top: 46px;">
            <?php echo Yii::app()->user->getFlash('error'); ?>
        </div>
    <?php endif; ?>

    <div style="">
        <div id="msg_box"></div>
        <div class="exp-list table-wrapper margin-top-20">
            <?php $this->widget('zii.widgets.CListView', array(
                'dataProvider' => $dataProvider,
                'pager' => array('header' => '', ),
                'itemView' => '_newview',
                'summaryCssClass' => 'summary text-right margin-left-auto',
                'template' => '<div class="clearfix">{sorter}{summary}</div><div id="parent"><div class="table-responsive report-table-wrapper"><table cellpadding="10" class="table total-table list-view " id="fixtable">{items}</table></div></div>{pager}',
                'emptyText' => '<table cellpadding="10" class="table"><thead class="entry-table sticky-thead">
            <tr>
            <th>No</th>
            <th>Warehouse</th>
            <th>Goods received from</th>
            <th>Clerk</th>
            <th>Date</th>            
            <th></th>            
             </tr></thead><tr><td colspan="13" class="text-center">No results found</td></tr></table>'
            )); ?>

        </div>
    </div>

    <style>
        .table a.link {
            color: #015986db;
        }

        .toast-type-error {
            background-color: #B32B2B;
        }

        .toast-type-success {
            background-color: #00A65A;
        }

        #fixtable thead th,
        #fixtable tfoot th {
            background-color: #eee;
            z-index: 1;
        }

        .bill_class {
            background: #FF6347 !important;
        }

        .table a.link_deleted {
            color: #da3727 !important;
        }

        .exp-list {
            padding: 0px;
        }

        .page-body h3 {
            margin: 4px 0px;
            color: inherit;
            text-align: left;
        }

        tfoot th {
            background-color: #eee;
        }

        .panel {
            border: 1px solid #ddd;
            box-shadow: 0px 0px 12px 1px rgba(0, 0, 0, 0.25);
        }

        .panel-heading {
            background-color: #6A8EC7;
            height: 40px;
            color: #fff;
        }

        .panel label {
            display: block;
        }

        .head_remarks {
            padding-left: 5px;
        }

        .ind_remarks {
            padding: 5px;
            font-size: 13px;
        }

        .ind_remarks p:first-child {
            font-size: 11px;
        }

        .ind_remarks p:last-child {
            background: #f6f6f6;
            padding: 5px;
            margin-top: -5px;
        }

        .pur_remark {
            font-weight: 600;
            vertical-align: middle !important;
            white-space: nowrap;
            position: relative;
        }

        .cursor-pointer {
            cursor: pointer;
        }

        .pur_remark .icon {}

        .form-head {
            position: relative;
        }



        #addremark .all_remarks {
            overflow: auto;
            margin-top: 5px;
            max-height: 150px;
            border-top: 1px solid #ddd;
        }

        .tooltip-hiden {
            width: auto;
        }

        .badge1 {
            /*color: #555;background-color: #ccc;*/
            position: absolute;
            right: 0px;
            top: 0px;
            color: #0093dd;
            font-weight: 900;
            font-size: 11px;
        }

        .addnow {
            cursor: pointer;
            font-size: 14px;
            float: right;
        }

        .highlight {
            background-color: #DD1035;
            color: #fff;
        }

        /* #parent{max-height: 350px;} */
        #parent .table {
            margin-bottom: 0px;
        }

        .list-view .sorter li {
            margin: 0px !important;
        }

        @media(min-width: 1400px) {
            #addremark .all_remarks {
                max-height: 300px;
            }
        }
    </style>


    <script>
        $('div#addr').click(function () {
            $('.remarkadd').hide();
            $('.addnow').show();
        });

        $('.addnow').click(function () {
            $(this).hide();
            $('.closermrk').show();
            $('.remarkadd').show();
        });
        $(document).on('click', '.delete_warehousereceipt', function (e) {
            e.preventDefault();
            var warehouse_receipt_id = $(this).attr('id');
            var answer = confirm("Are you sure you want to Delete this Receipt?");
            if (answer) {
                $.ajax({
                    type: "GET",
                    dataType: "json",
                    data: {
                        receiptid: warehouse_receipt_id
                    },
                    url: '<?php echo Yii::app()->createUrl('wh/warehousereceipt/warehoueReceiptDelete'); ?>',
                    success: function (response) {
                        if (response.response == 'success') {
                            location.reload();
                            $().toastmessage('showSuccessToast', "" + response.msg + "");
                        } else {
                            $().toastmessage('showErrorToast', "" + response.msg + "");
                        }
                    }
                });
            } else {
                return false;
            }

        });
        $('.closermrk').click(function () {
            $(this).hide();
            $('.addnow').show();
            $('.remarkadd').hide();
            $('#remark').val('');
        });

        function closeaction() {
            $('#remark').val('');
            $('#addremark').hide("slide", {
                direction: "right"
            }, 200);
        }

        function addremark(pur_order, event) {
            var id = pur_order;
            $(".popover").removeClass("in");
            $('.remarkadd').show();
            $('.addnow').hide();
            $('#addremark').show("slide", {
                direction: "right"
            }, 500);
            $("#txtPurchaseId").val(id);
            event.preventDefault();
            $.ajax({
                type: "GET",
                data: {
                    purchaseid: id
                },
                dataType: 'json',
                url: '<?php echo Yii::app()->createUrl('purchase/viewremarks'); ?>',
                success: function (response) {
                    $("#remarkList").html(response.result);
                    $(".po_no").html(response.po_no)
                    $("#txtPurchaseId").val(id);
                }
            });

        }
        var url = '<?php echo Yii::app()->getBaseUrl(true); ?>';



        $(document).on('click', '.edit_purchase', function () {
            var val = $(this).attr('id');
            location.href = url + '/index.php?r=wh/warehousereceipt/update&receiptid=' + val;
        });

        $(document).on('click', '#remarkSubmit', function (e) {
            var purchaseId = $("#txtPurchaseId").val();
            var remark = $("#remark").val();
            $.ajax({
                method: "GET",
                data: {
                    purchaseid: purchaseId,
                    remark: remark
                },
                url: '<?php echo Yii::app()->createUrl('purchase/addremarks'); ?>',
                success: function (response) {
                    $("#remarkList").html(response);
                    $('#addremark').show("slide", {
                        direction: "right"
                    }, 1000);
                    $("#remark").val("");
                }
            });
        });
        $(document).on('click', '.view_purchase', function () {
            var val = $(this).attr('id');
            location.href = url + '/index.php?r=wh/warehousereceipt/view&receiptid=' + val;
        });

        function myfunction(elem) {
            var id = "";
            id = $(elem).attr('id');
            var element = elem;
            if (confirm('Are you sure you want to delete this?')) {
                $.ajax({
                    url: '<?php echo Yii::app()->createAbsoluteUrl('purchase/deletepurchase'); ?>',
                    type: 'GET',
                    dataType: 'json',
                    data: {
                        purchase_id: id
                    },
                    success: function (response) {
                        console.log(response);
                        if (response == 1) {
                            $("#msg_box").html('<div class="alert alert-success alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><strong> Success!</strong>  &nbsp;Deleted successfully </div>').fadeOut(5000);
                            element.closest('tr').remove();
                            setTimeout(
                                function () {
                                    //                         location.reload();

                                }, 500);
                        } else {
                            $("#msg_box").html('<div class="alert alert-warning alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><strong> Sorry!</strong>  &nbsp; Some Problem occured</div>');
                        }
                    }

                });

            }
        }

        $(document).ready(function () {
            //$("#fixtable").tableHeadFixer({'left' : false, 'foot' : true, 'head' : true}); 
            $(".popover-test").popover({
                html: true,
                content: function () {
                    //return $('#popover-content').html();
                    return $(this).next('.popover-content').html();
                }
            });
            $('[data-toggle=popover]').on('click', function (e) {
                $('[data-toggle=popover]').not(this).popover('hide');
            });
            $('body').on('hidden.bs.popover', function (e) {
                $(e.target).data("bs.popover").inState.click = false;
            });
            $('body').on('click', function (e) {
                $('[data-toggle=popover]').each(function () {
                    // hide any open popovers when the anywhere else in the body is clicked
                    if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                        $(this).popover('hide');
                    }
                });
            });
            $(document).ajaxComplete(function () {
                $().toastmessage({
                    sticky: false,
                    position: 'top-right',
                    inEffectDuration: 1000,
                    stayTime: 3000,
                    closeText: '<i class="icon ion-close-round"></i>',
                });
                //$("#fixtable").tableHeadFixer({'left' : false, 'foot' : true, 'head' : true}); 
                $(".popover-test").popover({
                    html: true,
                    content: function () {
                        return $(this).next('.popover-content').html();
                    }
                });

            });
            $('.update_status').click(function (e) {
                e.preventDefault();
                var element = $(this);
                var answer = confirm("Are you sure you want to change the status?");
                if (answer) {
                    var purchase_id = $(this).attr('id');
                    $.ajax({
                        url: '<?php echo Yii::app()->createAbsoluteUrl('purchase/updatepurchasestatus'); ?>',
                        type: 'GET',
                        dataType: 'json',
                        data: {
                            purchase_id: purchase_id
                        },
                        success: function (response) {
                            console.log(response);
                            if (response.response == 'success') {
                                $(".purchase_status_" + purchase_id).html('Saved');
                                $(".edit_option_" + purchase_id).hide();
                                element.closest('tr').find('.bill_label').text('Pending to be Billed');
                                element.closest('tr').find('.bill_status').addClass('bill_class');
                                $("#msg_box").html('<div class="alert alert-success alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><strong> Success!</strong>  &nbsp;' + response.msg + '</div>');
                            } else {

                                $("#msg_box").html('<div class="alert alert-warning alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><strong> Sorry!</strong>  &nbsp;' + response.msg + '</div>');
                            }
                        }
                    });
                } else {
                    return false;
                }
            });

        });
    </script>