<?php
/* @var $this DailyWorkTypeController */
/* @var $model DailyWorkType */

$this->breadcrumbs=array(
	'Daily Work Types'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List DailyWorkType', 'url'=>array('index')),
	array('label'=>'Create DailyWorkType', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#daily-work-type-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Daily Work Types</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'daily-work-type-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'wtid',
		'work_type',
		'company_id',
		'no_of_labour_label',
		'wage_label',
		'wage_rate_label',
		/*
		'helper_label',
		'helper_labour_label',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
