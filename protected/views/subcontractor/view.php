<?php
/* @var $this SubcontractorController */
/* @var $model Subcontractor */

$this->breadcrumbs=array(
	'Subcontractors'=>array('index'),
	$model->subcontractor_id,
);

$this->menu=array(
	array('label'=>'List Subcontractor', 'url'=>array('index')),
	array('label'=>'Create Subcontractor', 'url'=>array('create')),
	array('label'=>'Update Subcontractor', 'url'=>array('update', 'id'=>$model->subcontractor_id)),
	array('label'=>'Delete Subcontractor', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->subcontractor_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Subcontractor', 'url'=>array('admin')),
);
?>

<h1>View Subcontractor #<?php echo $model->subcontractor_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'subcontractor_id',
		'subcontractor_name',
		'subcontractor_phone',
		'subcontractor_email',
		'subcontractor_status',
		'subcontractor_description',
		'created_date',
		'created_by',
	),
)); ?>
