<div class ="bill-details">
<h2 class="purchase-title">Bills</h2>

		<div id="loading"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/ajax-loader.gif"></div>
		<div id="msg_box"></div>
	<form id="pdfvals1" method="post" action="<?php echo $this->createAbsoluteUrl('purchase/addpurchase'); ?>">
			<input type="hidden" name="remove" id="remove" value="">
			<input type="hidden" name="bill_id" id="bill_id" value="0">
			<input type="hidden" name="purchase_id" id="purchase_id" value="0">
			<input type="hidden" name="mrId" id="mrId" value="<?php echo isset($mrId)? $mrId:''?>">
			<input type="hidden" name="selectedItems" id="selectedItems" value="<?php echo isset($selectedItems)? $selectedItems:''?>">
		<div class="block_purchase">
			<div class="row main-form ">
				<div class="col-sm-4 col-md-3 ">
					<div class="form-group flexcol">
							<label>COMPANY : </label>
							<?php
						if(!empty($mrId)){
							$newQuery = ""; 
							$materialreq = MaterialRequisition::model()->findByPk($mrId) ;
							  if(!empty($materialreq)){
								$project =Projects::model()->findByPk( $materialreq->project_id);
								if(!empty($project)){
									$company_arr = explode(',', $project->company_id);
									if(!empty($company_arr)){
										$newQuery = ""; 
										foreach ($company_arr as $arr) {
											if ($newQuery) $newQuery .= ' OR';
											$newQuery .= " FIND_IN_SET('" . $arr . "', id)";
										}
									}
								}
							   }
											  
						}else{	
							$user = Users::model()->findByPk(Yii::app()->user->id);
							$arrVal = explode(',', $user->company_id);
							$newQuery = "";
							foreach ($arrVal as $arr) {
								if ($newQuery) $newQuery .= ' OR';
								$newQuery .= " FIND_IN_SET('" . $arr . "', id)";
							}
						}
							$companyInfo = Company::model()->findAll(array('condition' => $newQuery));
							?>
							<select name="company_id" class="inputs target company_id" id="company_id"  >
								<option value="">Choose Company</option>
								<?php
                                foreach ($companyInfo as $key => $value) {
                                    if(!empty($model)){?>
                                    <option value="<?php echo $value['id'] ?>" <?php echo ($value['id'] == $model->company_id) ? 'selected' : ''; ?>><?php echo $value['name']; ?></option>
                                 <?php   }else{
                                ?>
                                    <option value="<?php echo $value['id'] ?>"><?php echo $value['name']; ?></option>
                                <?php
                                    }
                                }
                                ?>

							</select>
						</div>
					</div>
					
					<div class="col-sm-4 col-md-3 ">
						<div class="form-group flexcol">
							<label>PROJECT : </label>
							<select name="project" class="inputs target project" id="project"  >
							<?php if(!empty($materialreq)){  
								//print_r($materialreq->project_id);exit;
                                if (!empty($project)) {?>
                                <option value="<?php echo $project->pid; ?>" selected ><?php echo $project['name']; ?></option>
                                <?php } }else { ?>
                            <option value="">Choose Project</option>
                                <?php } ?>
							</select>
						</div>
					</div>
					<div class="col-sm-4 col-md-3 ">
						<div class="form-group flexcol">
							<label>EXPENSE HEAD : </label>
							<select name="expense_head" class="inputs target expense_head"  id="expense_head">
							<?php if(!empty($purchase)){  
                                if (!empty($expense_head)) {
                                    foreach ($expense_head as $value) {
                                ?>
                                <option value="<?php echo $value['type_id']; ?>" <?php echo ($value['type_id'] == $purchase->expensehead_id) ? 'selected' : ''; ?>><?php echo $value['type_name']; ?></option>
                                <?php } }}else{?>
                            <option value="">Choose Expense Head</option>
                                <?php } ?>
							</select>
						</div>
					</div>
					<div class="col-sm-4 col-md-3 ">
						<div class="form-group flexcol">
							<label>VENDOR : </label>
							<select name="vendor" class="inputs target vendor" id="vendor"  >
							<?php if(!empty($purchase)){  
                                if (!empty($vendor)) {
                                    foreach ($vendor as $value) {
                                ?>
                                	<option value="<?php echo $value['vendorid']; ?>" <?php echo ($value['vendorid'] == $purchase->vendor_id) ? 'selected' : ''; ?>><?php echo $value['vendorname']; ?></option>
                                <?php } }}else{?>  
                            <option value="">Choose Vendor</option>
                            <?php } ?>
							</select>
						</div>
					</div>
					<div class="col-sm-4 col-md-3 " >
						<div class="form-group flexcol">
							<label>DATE : </label>
							<input type="text" value="<?php echo ((isset($model->bill_date) && $model->bill_date != '') ? date("d-m-Y", strtotime($model->bill_date)) : date("d-m-Y")); ?>" id="datepicker" class="txtBox date inputs target form-control"  name="date" placeholder="Please click to edit">
						</div>
					</div>
					<div class="col-sm-4 col-md-3  warehouse_list" >
						<div class="form-group flexcol">
							<label for="warehousestock_warehouseid" class="required">Warehouse</label>

							<?php 
							$selectedWarehouse='';
							if(isset($model) && isset($model->warehouse_id)){

								$selectedWarehouse = $model->warehouse_id;
							}else{
								$selectedWarehouse=null;
							}
							$data = CHtml::listData(Controller::getCategoryOptions(), 'id', 'text', 'group'); ?>
							<?php echo CHtml::activeDropDownList($model2, 'warehousestock_warehouseid', $data, array(
								'empty' => '-Choose a Warehouse-',
								'class' => 'form-control mandatory',
								'options' => array(
									$selectedWarehouse => array('selected' => 'selected')
								),
							)); ?>
						</div>
					</div>
					<div class="col-sm-4 col-md-3 ">
						<div class="form-group flexcol">
							<label>BILL NO : </label>
							<input type="text" required value="<?php echo ((isset($model) && $model->bill_number != '') ? $model->bill_number : ''); ?>" class="txtBox inputs target check_type purchaseno form-control"  name="bill_number" id="bill_number" placeholder="">
						</div>
				</div>

			</div>
		</div>
    </form>  

</div>
<style>
	.flexcol{
		display:flex;
		flex-direction:column;
	}
</style>
<script>
  $("#expense_head").change(function() {
		var val = $(this).val();
		$('#loading').show();
		$("#vendor").html('<option value="">Select Vendor</option>');
		$.ajax({
			url: '<?php echo Yii::app()->createUrl('purchase/dynamicvendor'); ?>',
			method: 'POST',
			data: {
				exp_id: val
			},
			dataType: "json",
			success: function(response) {
				$("#vendor").html(response.html);
			}
		})
	})



	$("#project").change(function() {
		var val = $(this).val();
		$("#vendor").html('<option value="">Select Vendor</option>');
		$("#expense_head").html('<option value="">Select Expense Head</option>');
		$('#loading').show();
		$.ajax({
			url: '<?php echo Yii::app()->createUrl('purchase/dynamicexpensehead'); ?>',
			method: 'POST',
			data: {
				project_id: val
			},
			dataType: "json",
			success: function(response) {
				if (response.status == 'success') {
					$("#expense_head").html(response.html);
					$("#vendor").html('<option value="">Select Vendor</option>');
				} else {
					$("#expense_head").html(response.html);
					$("#vendor").html('<option value="">Select Vendor</option>');
				}
			}
		})
	})

	$(document).ready(function() {
		$('select').first().focus();
	});
    $(document).on("change", "#company_id", function() {
		var element = $(this);
		var bill_id = $("#bill_id").val();
		var mrId=$("#mrId").val();
		var selected_items="";
		selected_items=$("#selectedItems").val();
		var purchase_id = $("#purchase_id").val();
		var default_date = $(".date").val();
		var project = $("#project").val();
		var company = $(this).val();
		var vendor = $('#vendor').val();
		var bill_number = $('#bill_number').val();
		var expense_head = $('#expense_head').val();
		var expense_head = $('#expense_head').val();
		var company_id =$("#company_id").val();
		var warehouse_id = $('#Warehousestock_warehousestock_warehouseid').val();
		var mrId = $('#mrId').val();

		if ((moment(default_date, 'DD-MM-YYYY', true).isValid())) {
			if (project == '' || vendor == '' || default_date == '' || bill_number == '' || expense_head == '' || company == '') {
				$.ajax({
					method: "GET",
					dataType: "json",
					url: '<?php echo Yii::app()->createUrl('bills/ajax'); ?>',
					success: function(result) {
						$("#project").select2("focus");
					}
				});
			} else {
				$('.loading-overlay').addClass('is-active');
				$.ajax({
					method: "GET",
					async: true,
					data: {
						bill_id: bill_id,
						bill_number: bill_number,
						default_date: default_date,
						project_id: project,
						mrId:mrId,
						vendor_id: vendor,
						expensehead_id: expense_head,
						purchase_id: purchase_id,
						company_id: company,
						warehouse_id: warehouse_id,
						selected_items:selected_items,
					},
					dataType: "json",
					url: '<?php echo Yii::app()->createUrl('bills/createnewbills'); ?>',
					success: function(result) {
						if (result.response == 'success') {
							$("#purchase_id").val(result.p_id);
							$(".bill_items").removeClass('checkek_edit');
							$('.js-example-basic-single').select2('focus');
							$("#bill_id").val(result.bill_id);
							$().toastmessage('showSuccessToast', "" + result.msg + "");
						} else {
							$().toastmessage('showErrorToast', "" + result.msg + "");
							element.val('');
						}

						$("#project").select2("focus");
					}
				});
			}
		} else {
			$(this).focus();
			$().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
			$(this).focus();
		}
	});

	$(document).on("change", "#project", function() {
		var element = $(this);
		var bill_id = $("#bill_id").val();
		var purchase_id = $("#purchase_id").val();
		var default_date = $(".date").val();
		var project = $(this).val();
		var vendor = $('#vendor').val();
		var selected_items="";
		selected_items=$("#selectedItems").val();
		var company = $("#company_id").val();
		var bill_number = $('#bill_number').val();
		var expense_head = $('#expense_head').val();
		var mrId = $('#mrId').val();
		var warehouse_id = $('#Warehousestock_warehousestock_warehouseid').val();
		if ((moment(default_date, 'DD-MM-YYYY', true).isValid())) {
			if (project == '' || vendor == '' || default_date == '' || bill_number == '' || expense_head == '' || company == '') {
				$.ajax({
					method: "GET",
					dataType: "json",
					url: '<?php echo Yii::app()->createUrl('bills/ajax'); ?>',
					success: function(result) {
						$("#expense_head").select2("focus");
					}
				});
			} else {
				$('.loading-overlay').addClass('is-active');
				$.ajax({
					method: "GET",
					async: true,
					data: {
						bill_id: bill_id,
						mrId:mrId,
						bill_number: bill_number,
						default_date: default_date,
						project_id: project,
						vendor_id: vendor,
						expensehead_id: expense_head,
						purchase_id: purchase_id,
						company_id: company,
						warehouse_id: warehouse_id,
						selected_items:selected_items,
					},
					dataType: "json",
					url: '<?php echo Yii::app()->createUrl('bills/createnewbills'); ?>',
					success: function(result) {
						if (result.response == 'success') {
							$("#purchase_id").val(result.p_id);
							$(".bill_items").removeClass('checkek_edit');
							$('.js-example-basic-single').select2('focus');
							$("#bill_id").val(result.bill_id);
							$().toastmessage('showSuccessToast', "" + result.msg + "");
						} else {
							$().toastmessage('showErrorToast', "" + result.msg + "");
							element.val('');
						}

						$("#expense_head").select2("focus");
					}
				});
			}
		} else {
			$(this).focus();
			$().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
			$(this).focus();
		}
	});

	$(document).on("change", "#vendor", function() {
		var element = $(this);
		var bill_id = $("#bill_id").val();
		var default_date = $(".date").val();
		var vendor = $(this).val();
		var selected_items="";
		selected_items=$("#selectedItems").val();
		var purchase_id = $("#purchase_id").val();
		var project = $('#project').val();
		var bill_number = $('#bill_number').val();
		var company = $("#company_id").val();
		var expense_head = $('#expense_head').val();
		var warehouse_id = $('#Warehousestock_warehousestock_warehouseid').val();
		var mrId = $('#mrId').val();
		if ((moment(default_date, 'DD-MM-YYYY', true).isValid())) {

			if (project == '' || vendor == '' || default_date == '' || bill_number == '' || expense_head == '' || company == '') {
				$.ajax({
					method: "GET",
					dataType: "json",
					url: '<?php echo Yii::app()->createUrl('bills/ajax'); ?>',
					success: function(result) {
						$(".date").focus();
					}
				});

			} else {
				$('.loading-overlay').addClass('is-active');
				$.ajax({
					method: "GET",
					data: {
						bill_id: bill_id,
						bill_number: bill_number,
						default_date: default_date,
						project_id: project,
						vendor_id: vendor,
						expensehead_id: expense_head,
						purchase_id: purchase_id,
						company_id: company,
						mrId:mrId,
						warehouse_id: warehouse_id,
						selected_items:selected_items,

					},
					dataType: "json",
					url: '<?php echo Yii::app()->createUrl('bills/createnewbills'); ?>',
					success: function(result) {
						if (result.response == 'success') {
							$("#purchase_id").val(result.p_id);
							$(".bill_items").removeClass('checkek_edit');
							$('.js-example-basic-single').select2('focus');
							$("#bill_id").val(result.bill_id);
							$().toastmessage('showSuccessToast', "" + result.msg + "");
						} else {
							$().toastmessage('showErrorToast', "" + result.msg + "");
						}

						$(".date").focus();
					}
				});

			}

		} else {
			$(this).focus();
			$().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
			$(this).focus();
		}

	});


	$(document).on("change", "#expense_head", function() {
		var element = $(this);
		var bill_id = $("#bill_id").val();
		var default_date = $(".date").val();
		var expense_head = $(this).val();
		var selected_items="";
		selected_items=$("#selectedItems").val();
		var purchase_id = $("#purchase_id").val();
		var project = $('#project').val();
		var bill_number = $('#bill_number').val();
		var company = $("#company_id").val();
		var vendor = $('#vendor').val();
		var mrId = $('#mrId').val();
		var warehouse_id = $('#Warehousestock_warehousestock_warehouseid').val();
		if ((moment(default_date, 'DD-MM-YYYY', true).isValid())) {
			if (project == '' || vendor == '' || default_date == '' || bill_number == '' || expense_head == '' || company == '') {
				$.ajax({
					method: "GET",
					dataType: "json",
					url: '<?php echo Yii::app()->createUrl('bills/ajax'); ?>',
					success: function(result) {
						$(".vendor").select2("focus");
					}
				});

			} else {
				$('.loading-overlay').addClass('is-active');
				$.ajax({
					method: "GET",
					data: {
						bill_id: bill_id,
						bill_number: bill_number,
						mrId:mrId,
						default_date: default_date,
						project_id: project,
						vendor_id: vendor,
						expensehead_id: expense_head,
						purchase_id: purchase_id,
						company_id: company,
						warehouse_id: warehouse_id,
						selected_items:selected_items,
					},
					dataType: "json",
					url: '<?php echo Yii::app()->createUrl('bills/createnewbills'); ?>',
					success: function(result) {
						if (result.response == 'success') {
							$("#purchase_id").val(result.p_id);
							$(".bill_items").removeClass('checkek_edit');
							$('.js-example-basic-single').select2('focus');
							$("#bill_id").val(result.bill_id);
							$().toastmessage('showSuccessToast', "" + result.msg + "");
						} else {
							$().toastmessage('showErrorToast', "" + result.msg + "");
						}

						$(".vendor").select2("focus");
					}
				});

			}

		} else {
			$(this).focus();
			$().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
			$(this).focus();
		}

	});



	$(document).on("change", ".date", function() {
		var element = $(this);
		var bill_id = $("#bill_id").val();
		var default_date = $(this).val();
		var selected_items="";
		selected_items=$("#selectedItems").val();
		var project = $('#project').val();
		var vendor = $('#vendor').val();
		var purchase_id = $("#purchase_id").val();
		var bill_number = $('#bill_number').val();
		var expense_head = $('#expense_head').val();
		var company = $("#company_id").val();
		var mrId = $('#mrId').val();
		var warehouse_id = $('#Warehousestock_warehousestock_warehouseid').val();
		if ((moment(default_date, 'DD-MM-YYYY', true).isValid())) {
			if (project == '' || vendor == '' || default_date == '' || bill_number == '' || expense_head == '' || company == '') {

			} else {
				$('.loading-overlay').addClass('is-active');
				$.ajax({
					method: "GET",
					async: false,
					data: {
						bill_id: bill_id,
						mrId:mrId,
						bill_number: bill_number,
						default_date: default_date,
						project_id: project,
						vendor_id: vendor,
						expensehead_id: expense_head,
						purchase_id: purchase_id,
						company_id: company,
						warehouse_id: warehouse_id,
						selected_items:selected_items,
					},
					dataType: "json",
					url: '<?php echo Yii::app()->createUrl('bills/createnewbills'); ?>',
					success: function(result) {
						if (result.response == 'success') {
							$("#purchase_id").val(result.p_id);
							$(".bill_items").removeClass('checkek_edit');
							$('.js-example-basic-single').select2('focus');
							$("#bill_id").val(result.bill_id);
							if (result.mrId) {
								
								$("#bill_id").val(0);
							}
							$().toastmessage('showSuccessToast', "" + result.msg + "");
						} else {
							$().toastmessage('showErrorToast', "" + result.msg + "");
						}
					}

				});

			}
		} else {
			$(this).focus();
			$().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
			$(this).focus();
		}

		$('#purchaseno').focus();

	});

	$(document).on("change", "#Warehousestock_warehousestock_warehouseid", function(e) {
		e.preventDefault();

		var Warehouse_id = $(this).val();
		var element = $(this);
		var bill_id = $("#bill_id").val();
		var default_date = $(".date").val();
		var vendor = $('#vendor').val();
		var selected_items="";
		selected_items=$("#selectedItems").val();
		var purchase_id = $("#purchase_id").val();
		var project = $('#project').val();
		var bill_number = $('#bill_number').val();
		var expense_head = $('#expense_head').val();
		var mrId = $('#mrId').val();

		if ((moment(default_date, 'DD-MM-YYYY', true).isValid())) {

			if (project == '' || vendor == '' || default_date == '' || bill_number == '' || expense_head == '') {
				$.ajax({
					method: "GET",
					dataType: "json",
					url: '<?php echo Yii::app()->createUrl('bills/ajax'); ?>',
					success: function(result) {
						$('#description').focus();

					}
				});

			} else {
				$.ajax({
					method: "GET",
					data: {
						bill_id: bill_id,
						mrId:mrId,
						bill_number: bill_number,
						default_date: default_date,
						project_id: project,
						vendor_id: vendor,
						expensehead_id: expense_head,
						purchase_id: purchase_id,
						warehouse_id: Warehouse_id,
						selected_items:selected_items,
					},
					dataType: "json",
					url: '<?php echo Yii::app()->createUrl('bills/createnewbills'); ?>',
					success: function(result) {
						if (result.response == 'success') {
							$("#purchase_id").val(result.p_id);
							$(".bill_items").removeClass('checkek_edit');
							$('.js-example-basic-single').select2('focus');
							$("#bill_id").val(result.bill_id);
							if (result.mrId) {
							
							$("#bill_id").val(0);
							}
							$().toastmessage('showSuccessToast', "" + result.msg + "");
						} else {
							$().toastmessage('showErrorToast', "" + result.msg + "");
						}
						$('#description').focus();

					}
				});

			}

		} else {
			$(this).focus();
			$().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
			$(this).focus();
		}

	});


	$(document).on("blur", "#bill_number", function(event) {

		var element = $(this);
		var bill_id = $("#bill_id").val();
		var default_date = $(".date").val();
		var bill_number = $(this).val();
		var selected_items="";
		selected_items=$("#selectedItems").val();
		var purchase_id = $("#purchase_id").val();
		var expense_head = $('#expense_head').val();
		var warehouse_id = $('#Warehousestock_warehousestock_warehouseid').val();
		var mrId = $('#mrId').val();


		if ((moment(default_date, 'DD-MM-YYYY', true).isValid())) {
			if (bill_number == '') {
				event.preventDefault();
			} else {
				var project = $('#project').val();
				var vendor = $('#vendor').val();
				var date = $('.date').val();
				var company = $("#company_id").val();
				if (project == '' || vendor == '' || default_date == '' || expense_head == '' || company == '') {

				} else {
					$('.loading-overlay').addClass('is-active');
					$.ajax({
						method: "GET",
						data: {
							bill_id: bill_id,
							mrId:mrId,
							bill_number: bill_number,
							default_date: default_date,
							project_id: project,
							vendor_id: vendor,
							expensehead_id: expense_head,
							purchase_id: purchase_id,
							company_id: company,
							warehouse_id: warehouse_id,
							selected_items:selected_items,
						},
						dataType: "json",
						url: '<?php echo Yii::app()->createUrl('bills/createnewbills'); ?>',
						success: function(result) {
							if (result.response == 'success') {
								$("#purchase_id").val(result.p_id);
								$(".bill_items").removeClass('checkek_edit');
								
								
								$("#bill_id").val(result.bill_id);
								
								if (result.mrId) {
   
    							$("#bill_id").val(0);
								}
								$().toastmessage('showSuccessToast', "" + result.msg + "");
								
								
							} else {
								$().toastmessage('showErrorToast', "" + result.msg + "");
							}
							$('#description').select2('focus');
							$('.js-example-basic-single').select2('focus');
						}
					});



				}
			}

		} else {
			$(this).focus();
			$().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
			$(this).focus();
		}

	});
    $("#company_id").change(function() {
		$('#loading').show();
		var val = $(this).val();
		var mrId = $('#mrId').val(); // Get the MR ID
    var companyId = $(this).val(); // Get the selected company ID

    if (mrId && companyId) {
        $.ajax({
            url: '<?php echo Yii::app()->createUrl('purchase/getProjectByMrId'); ?>',
            method: 'POST',
            data: {
                mrId: mrId,
                companyId: companyId
            },
            dataType: "json",
            success: function(response) {
                if (response) {
                    
                    // Set the value of the project select box to the retrieved project ID
                    $('#project').html(response.html);
                    $('#project').trigger('change');
                }
            }
        });
    }else{
		$("#project").html('<option value="">Select Project</option>');
		$.ajax({
			url: '<?php echo Yii::app()->createUrl('bills/dynamicproject'); ?>',
			method: 'POST',
			data: {
				company_id: val
			},
			dataType: "json",
			success: function(response) {
				if (response.status == 'success') {
					$("#project").html(response.html);
				} else {
					$("#project").html(response.html);
				}
			}

		})
		}
	})

  
</script>