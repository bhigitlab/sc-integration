-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jul 23, 2021 at 02:14 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `scv2_new`
--

-- --------------------------------------------------------

--
-- Table structure for table `jp_pre_subcontractor_payment`
--

CREATE TABLE `jp_pre_subcontractor_payment` (
  `payment_id` int(11) NOT NULL,
  `ref_id` int(11) NOT NULL,
  `followup_id` int(11) DEFAULT NULL,
  `record_grop_id` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `subcontractor_id` int(11) DEFAULT NULL,
  `project_id` int(11) NOT NULL,
  `amount` float(15,2) NOT NULL,
  `description` text NOT NULL,
  `date` datetime NOT NULL,
  `payment_type` smallint(6) NOT NULL,
  `bank` int(11) DEFAULT NULL,
  `cheque_no` varchar(100) DEFAULT NULL,
  `sgst` float DEFAULT NULL,
  `sgst_amount` float DEFAULT NULL,
  `cgst` float DEFAULT NULL,
  `cgst_amount` float DEFAULT NULL,
  `igst` float DEFAULT NULL,
  `igst_amount` float DEFAULT NULL,
  `tax_amount` int(11) DEFAULT NULL,
  `tds` float DEFAULT NULL,
  `tds_amount` double DEFAULT NULL,
  `paidamount` double DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `reconciliation_status` int(11) DEFAULT NULL,
  `reconciliation_date` datetime DEFAULT NULL,
  `approve_status` enum('Yes','No') DEFAULT 'Yes',
  `uniqueid` varchar(200) DEFAULT NULL,
  `rowcount_slno` int(11) DEFAULT NULL,
  `rowcount_total` int(11) DEFAULT NULL,
  `budget_percentage` float DEFAULT NULL,
  `update_status` int(11) DEFAULT NULL,
  `payment_quotation_status` enum('1','2') NOT NULL DEFAULT '1',
  `quotation_number` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) NOT NULL,
  `updated_date` datetime NOT NULL,
  `delete_status` int(11) DEFAULT NULL,
  `approval_status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '	0=>Pending,1=>Approved,2=>Rejected',
  `approved_by` int(11) DEFAULT NULL,
  `record_action` varchar(300) NOT NULL,
  `approve_notify_status` enum('0','1') NOT NULL COMMENT '0=>no.not visible to admin,1=>yes visible to admin',
  `cancelled_by` int(11) DEFAULT NULL,
  `cancelled_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jp_pre_subcontractor_payment`
--

INSERT INTO `jp_pre_subcontractor_payment` (`payment_id`, `ref_id`, `followup_id`, `record_grop_id`, `subcontractor_id`, `project_id`, `amount`, `description`, `date`, `payment_type`, `bank`, `cheque_no`, `sgst`, `sgst_amount`, `cgst`, `cgst_amount`, `igst`, `igst_amount`, `tax_amount`, `tds`, `tds_amount`, `paidamount`, `company_id`, `reconciliation_status`, `reconciliation_date`, `approve_status`, `uniqueid`, `rowcount_slno`, `rowcount_total`, `budget_percentage`, `update_status`, `payment_quotation_status`, `quotation_number`, `created_by`, `created_date`, `updated_by`, `updated_date`, `delete_status`, `approval_status`, `approved_by`, `record_action`, `approve_notify_status`, `cancelled_by`, `cancelled_at`) VALUES
(1, 1, NULL, '2021-07-20 14:24:53', 1, 1, 96.00, 'test desc - updated sds', '2021-07-20 00:00:00', 89, NULL, '', NULL, 0, NULL, 0, NULL, 0, 0, 0, 0, 96, 1, NULL, NULL, 'Yes', '16267910936512bd43d9caa6e02c990b0a82652dca', 1, 1, NULL, NULL, '1', 1, 101, '2021-07-20 00:00:00', 101, '2021-07-20 19:54:53', NULL, '1', 100, 'update', '1', NULL, '0000-00-00 00:00:00'),
(2, 1, 1, '2021-07-20 14:26:35', 1, 1, 108.00, 'test desc - updated sds', '2021-07-20 00:00:00', 89, NULL, '', NULL, 0, NULL, 0, NULL, 0, 0, 0, 0, 108, 1, NULL, NULL, 'Yes', '16267911956512bd43d9caa6e02c990b0a82652dca', 1, 1, NULL, NULL, '1', 1, 101, '2021-07-20 00:00:00', 101, '2021-07-20 19:56:35', NULL, '1', 100, 'update', '1', NULL, '0000-00-00 00:00:00'),
(3, 3, NULL, '2021-07-23 08:36:00', 1, 1, 55.00, 'test', '2021-07-23 00:00:00', 89, NULL, '', NULL, 0, NULL, 0, NULL, 0, 0, 0, 0, 55, 1, NULL, NULL, 'Yes', '16270293606512bd43d9caa6e02c990b0a82652dca', 1, 1, NULL, NULL, '1', 1, 101, '2021-07-23 00:00:00', 101, '2021-07-23 14:06:00', NULL, '2', NULL, 'update', '0', 101, '2021-07-23 14:06:13');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `jp_pre_subcontractor_payment`
--
ALTER TABLE `jp_pre_subcontractor_payment`
  ADD PRIMARY KEY (`payment_id`),
  ADD KEY `FK_project_id` (`project_id`),
  ADD KEY `FK_subcontractor_id` (`subcontractor_id`),
  ADD KEY `FK_ref_id` (`ref_id`),
  ADD KEY `FK_bank` (`bank`),
  ADD KEY `FK_company_id` (`company_id`),
  ADD KEY `FK_created_by` (`created_by`),
  ADD KEY `FK_approved_by` (`approved_by`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `jp_pre_subcontractor_payment`
--
ALTER TABLE `jp_pre_subcontractor_payment`
  MODIFY `payment_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
