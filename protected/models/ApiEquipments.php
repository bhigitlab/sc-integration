<?php

/**
 * This is the model class for table "{{api_equipments}}".
 *
 * The followings are the available columns in table '{{api_equipments}}':
 * @property integer $id
 * @property string $equipment_name
 * @property integer $template_equipment_id
 * @property integer $equipment_id
 * @property integer $project_id
 * @property string $unit
 * @property integer $count
 * @property string $created_at
 * @property string $updated_at
 * @property integer $accounts_equipment_id
 * @property integer $accounts_unit_id
 */
class ApiEquipments extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{api_equipments}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('template_equipment_id, equipment_id, project_id, count, accounts_equipment_id, accounts_unit_id', 'numerical', 'integerOnly'=>true),
			array('equipment_name, unit', 'length', 'max'=>255),
			array('created_at, updated_at', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, equipment_name, template_equipment_id, equipment_id, project_id, unit, count, created_at, updated_at, accounts_equipment_id, accounts_unit_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'equipment_name' => 'Equipment Name',
			'template_equipment_id' => 'Template Equipment',
			'equipment_id' => 'Equipment',
			'project_id' => 'Project',
			'unit' => 'Unit',
			'count' => 'Count',
			'created_at' => 'Created At',
			'updated_at' => 'Updated At',
			'accounts_equipment_id' => 'Accounts Equipment',
			'accounts_unit_id' => 'Accounts Unit',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('equipment_name',$this->equipment_name,true);
		$criteria->compare('template_equipment_id',$this->template_equipment_id);
		$criteria->compare('equipment_id',$this->equipment_id);
		$criteria->compare('project_id',$this->project_id);
		$criteria->compare('unit',$this->unit,true);
		$criteria->compare('count',$this->count);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('updated_at',$this->updated_at,true);
		$criteria->compare('accounts_equipment_id',$this->accounts_equipment_id);
		$criteria->compare('accounts_unit_id',$this->accounts_unit_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ApiEquipments the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
