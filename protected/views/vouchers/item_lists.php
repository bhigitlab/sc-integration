<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/tableHeadFixer.js"></script>
<div class="" id="newlist">
    <?php
    $sort = new CSort;
    $sort->defaultOrder = 'id DESC';
    $dataProvider = new CArrayDataProvider($voucher_items, array(
        'id' => 'id', 
        'sort' => $sort,
        'keyField' => 'id', 
        'pagination' => false
    ));
    $this->widget('zii.widgets.CListView', array(
        'dataProvider' => $dataProvider,
        'itemView' => '_item_view',
        'id' => 'id',
        'enableSorting' => 1,
        'enablePagination' => true,
        'template' => '<div>{summary}{sorter}</div><div id="parent"><table width="100" id="fixTable" class="table total-table">{items}</table></div>{pager}',
        'enablePagination' => false
    )); ?>


</div>
<script>
    $(document).ready(function() {
        $("#fixTable").tableHeadFixer({
            'left': 1,
            'foot': true,
            'head': true
        });
    });
</script>
<style>
    #parent {
        max-height: 300px;
    }

    th {
        background: #eee;
    }

    .pro_back {
        background: #fafafa;
    }

    .total-table {
        margin-bottom: 0px;
    }

    th,
    td {
        border-right: 1px solid #ccc !important;
    }
</style>