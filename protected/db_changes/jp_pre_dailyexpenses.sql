-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jul 23, 2021 at 02:11 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `scv2_new`
--

-- --------------------------------------------------------

--
-- Table structure for table `jp_pre_dailyexpenses`
--

CREATE TABLE `jp_pre_dailyexpenses` (
  `dailyexp_id` int(11) NOT NULL,
  `ref_id` int(11) NOT NULL,
  `followup_id` int(11) DEFAULT NULL,
  `record_grop_id` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `date` date NOT NULL,
  `exp_type_id` int(11) DEFAULT NULL COMMENT 'company expense type',
  `bill_id` varchar(100) DEFAULT NULL,
  `expensehead_id` int(11) DEFAULT NULL,
  `expense_type` int(11) DEFAULT NULL,
  `vendor_id` int(11) DEFAULT NULL,
  `dailyexpense_amount` float DEFAULT NULL,
  `dailyexpense_sgstp` float DEFAULT NULL,
  `dailyexpense_sgst` float DEFAULT NULL,
  `dailyexpense_cgstp` float DEFAULT NULL,
  `dailyexpense_cgst` float DEFAULT NULL,
  `dailyexpense_igstp` float DEFAULT NULL,
  `dailyexpense_igst` float DEFAULT NULL,
  `amount` float NOT NULL,
  `description` varchar(300) NOT NULL,
  `dailyexpense_receipt_type` int(11) DEFAULT NULL,
  `dailyexpense_receipt_head` int(11) DEFAULT NULL,
  `dailyexpense_receipt` float DEFAULT NULL,
  `dailyexpense_purchase_type` int(11) DEFAULT NULL,
  `dailyexpense_paidamount` float DEFAULT NULL,
  `bank_id` int(11) DEFAULT NULL,
  `dailyexpense_chequeno` varchar(100) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `data_entry` varchar(30) DEFAULT NULL,
  `exp_type` int(4) NOT NULL,
  `dailyexpense_type` enum('expense','deposit','receipt') NOT NULL DEFAULT 'expense',
  `company_id` int(11) DEFAULT NULL,
  `reconciliation_status` int(11) DEFAULT NULL,
  `reconciliation_date` date DEFAULT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `display_flg` enum('Yes','No') NOT NULL DEFAULT 'Yes',
  `transaction_parent` int(11) DEFAULT NULL,
  `parent_status` enum('0','1') NOT NULL DEFAULT '1',
  `expensehead_type` int(11) DEFAULT NULL,
  `transfer_parentid` int(11) DEFAULT NULL,
  `update_status` int(11) DEFAULT '0',
  `delete_status` int(11) DEFAULT '0',
  `approval_status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '0=>Pending,1=>Approved,2=>Rejected	',
  `approved_by` int(11) NOT NULL,
  `record_action` varchar(300) NOT NULL,
  `approve_notify_status` enum('0','1') NOT NULL DEFAULT '1' COMMENT '0=>no.not visible to admin,1=>yes visible to admin',
  `cancelled_by` int(11) DEFAULT NULL,
  `cancelled_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jp_pre_dailyexpenses`
--

INSERT INTO `jp_pre_dailyexpenses` (`dailyexp_id`, `ref_id`, `followup_id`, `record_grop_id`, `date`, `exp_type_id`, `bill_id`, `expensehead_id`, `expense_type`, `vendor_id`, `dailyexpense_amount`, `dailyexpense_sgstp`, `dailyexpense_sgst`, `dailyexpense_cgstp`, `dailyexpense_cgst`, `dailyexpense_igstp`, `dailyexpense_igst`, `amount`, `description`, `dailyexpense_receipt_type`, `dailyexpense_receipt_head`, `dailyexpense_receipt`, `dailyexpense_purchase_type`, `dailyexpense_paidamount`, `bank_id`, `dailyexpense_chequeno`, `user_id`, `created_by`, `created_date`, `updated_by`, `updated_date`, `data_entry`, `exp_type`, `dailyexpense_type`, `company_id`, `reconciliation_status`, `reconciliation_date`, `employee_id`, `display_flg`, `transaction_parent`, `parent_status`, `expensehead_type`, `transfer_parentid`, `update_status`, `delete_status`, `approval_status`, `approved_by`, `record_action`, `approve_notify_status`, `cancelled_by`, `cancelled_at`) VALUES
(1, 1, NULL, '2021-07-20 13:23:44', '2021-07-20', 26, '', NULL, NULL, NULL, 459, NULL, 0, NULL, 0, NULL, 0, 459, 'deposit cheque-updated approval', 88, 26, 459, NULL, NULL, 1, '000005', 101, 100, '2021-07-19 18:30:00', 101, '2021-07-20 13:23:44', NULL, 72, 'receipt', 1, NULL, NULL, NULL, 'No', NULL, '1', 5, NULL, 0, 0, '1', 100, 'update', '1', NULL, '0000-00-00 00:00:00'),
(2, 3, NULL, '2021-07-23 08:41:22', '2021-07-23', 6, '', 6, 89, NULL, 78, NULL, 0, NULL, 0, NULL, 0, 78, 'test', NULL, NULL, NULL, 2, 78, NULL, '', 101, 101, '2021-07-22 18:30:00', 101, '2021-07-23 08:41:22', NULL, 73, 'expense', 1, 0, NULL, NULL, 'Yes', NULL, '1', 1, NULL, 0, 0, '2', 0, 'update', '0', 101, '2021-07-23 14:11:33');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `jp_pre_dailyexpenses`
--
ALTER TABLE `jp_pre_dailyexpenses`
  ADD PRIMARY KEY (`dailyexp_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `jp_pre_dailyexpenses`
--
ALTER TABLE `jp_pre_dailyexpenses`
  MODIFY `dailyexp_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
