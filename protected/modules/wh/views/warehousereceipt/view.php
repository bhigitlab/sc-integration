<?php
$tblpx = Yii::app()->db->tablePrefix;
$project =Warehouse::model()->findByPK($model->warehousereceipt_warehouseid)['project_id']; //Yii::app()->user->company_id;
$company =Projects::model()->findByPK($project)['company_id'];
$companyInfo = Company::model()->findByPK($company);
$companyName = $companyInfo["name"];
$companyGst = $companyInfo["company_gstnum"];
if($model->warehousereceipt_transfer_type==2){
		$datasql="SELECT 
		wi.*,
		COALESCE(wi.warehousereceipt_itemqty, di.warehousedespatch_quantity) AS warehousereceipt_itemqty,
        COALESCE(wi.warehousereceipt_itemid, di.warehousedespatch_itemid) AS warehousereceipt_itemid,
		di.warehousedespatch_rate AS billitem_rate,
		IFNULL(wi.warehousereceipt_accepted_quantity, 0)
	FROM 
		jp_warehousedespatch d
	LEFT JOIN 
		jp_warehousedespatch_items di 
		ON d.warehousedespatch_id = di.warehousedespatch_id
	LEFT JOIN 
		jp_warehousereceipt wr 
		ON wr.warehousereceipt_id = {$model->warehousereceipt_id}
	LEFT JOIN 
		jp_warehousereceipt_items wi
		ON wi.warehousereceipt_id=wr.warehousereceipt_id
	WHERE 
		d.warehousedespatch_id = {$model->warehousereceipt_despatch_id}";
	
	
	$item_model = Yii::app()->db->createCommand($datasql)->queryAll();
	
}elseif($model->warehousereceipt_transfer_type==1){
	//echo "<pre>";print_r($model);exit;
		$datasql = "
    SELECT 
        w.*, 
        b.billitem_id AS billitem_id, 
        b.category_id AS warehousereceipt_itemid,
        COALESCE(w.warehousereceipt_itemqty, billitem_quantity) AS warehousereceipt_itemqty,
        COALESCE(w.warehousereceipt_itemid, b.category_id) AS warehousereceipt_itemid,
		IFNULL(w.warehousereceipt_accepted_quantity, 0),
		b.billitem_rate as billitem_rate
		FROM jp_billitem b 
    LEFT JOIN jp_warehousereceipt_items w 
        ON b.billitem_id = w.warehousereceipt_transfer_type_item_id 
        AND w.warehousereceipt_id = {$model->warehousereceipt_id}
    LEFT JOIN jp_bills bi 
        ON bi.bill_id = b.bill_id
    LEFT JOIN jp_warehousereceipt r 
        ON r.warehousereceipt_bill_id = bi.bill_id
    WHERE b.bill_id = {$model->warehousereceipt_bill_id}";

$item_model = Yii::app()->db->createCommand($datasql)->queryAll();
//echo "<pre>"; print_r($item_model); exit;

	
}


$purchase_id = $model->warehousereceiptBillId['purchase_id'];
$bill_check = Controller::billWithOrWithoutPO($purchase_id);
if ($bill_check == 'po') {
	$bill_link = 'index.php?r=bills/view&id=' . $model['warehousereceipt_bill_id'];
} else {
	$bill_link = 'index.php?r=bills/billview&id=' . $model['warehousereceipt_bill_id'];
}
?>
<!DOCTYPE html>
<html>

<head>
	<title>Receipt Pdf</title>
	<link href="https://fonts.googleapis.com/css?family=Diplomata+SC|Limelight" rel="stylesheet">
	<style>
		table {
			width: 100%;
			border-collapse: collapse;
			/*font-size: 12px;*/
		}

		table td,
		table th {
			padding: 6px 0px;
		}

		.item_table td,
		.item_table th {
			border: 1px solid #ccc;
			padding: 8px;
			text-align: center
		}

		/* .item_table tr:last-child td{border: 1px solid transparent;} */
		table tbody tr:last-child {
			border-bottom: 0px;
		}

		table tbody tr td:nth-child(2) {
			border-left: 0px;
		}

		h2 {
			margin: 0px;
			font-family: 'Limelight', cursive;
			padding: 0px;
		}

		td.text-right {
			text-align: right;
		}

		h2,
		h4 {
			text-align: center;
			text-transform: uppercase;
		}

		td.text-uppercase {
			text-transform: uppercase;
		}

		b.receipt_text {
			color: red;
			font-size: 18px;
		}

		.text-sm {
			font-size: 12px;
		}
	</style>
</head>

<body>
	<div class="container padding-top-50" style="<?php echo isset($_GET['export'])?'margin:0px 20px':'' ?>">
		<div class="clearfix">
			<div class="add-btn pull-right">

				<?php if (!filter_input(INPUT_GET, 'export')) { ?>

					<?php echo CHtml::link('SAVE AS PDF', Yii::app()->request->requestUri . "&export=pdf", array('class' => 'save_btn')); ?>
					<?php echo CHtml::link('SAVE AS EXCEL', Yii::app()->request->requestUri . "&export=csv", array('class' => 'save_btn')); ?>
				<?php } ?>
			</div>
		</div>
		<table>
			<tr>
				<td>
				<?php if(!isset($_GET['export'])){ ?>
					GSTIN:<b> <?php echo isset($companyGst) ? $companyGst : ""; ?></b></td>
				<?php } ?>
				</td>
				<td class="text-right">Phone:<b> <?php echo isset($companyInfo['phone']) ? $companyInfo['phone'] : ""; ?></b></td>
			</tr>
		</table>
		<div style="text-transform:uppercase;text-align:center;">
			<h2><?php echo $companyInfo['name'] ?></h2>
			<h4 style="margin:0px;"><?php //echo $companyInfo['address'] 
									?> <?php echo $warehouse_address; ?></h4>
		</div>
		<table>
			<tr>
				<td>RECEIPT NOTE NO.<b class="receipt_text"> <?php echo $model->warehousereceipt_no; ?></b></td>
				<td class="text-right">Date:<b> <?php echo date("d-m-Y", strtotime($model->warehousereceipt_date)); ?></b></td>
			</tr>
			<tr>
				<td colspan="2">Stock of Goods Received From:
					<b>
						<?php if (!filter_input(INPUT_GET, 'export')) { ?>
							<?php echo ($model->warehousereceiptwarehouseidfrom['warehouse_name'] != "") ? $model->warehousereceiptwarehouseidfrom['warehouse_name'] . '(' . CHtml::link($model->warehousereceiptDespatch['warehousedespatch_no'], 'index.php?r=wh/warehousedespatch/view&despatchid=' . $model['warehousereceipt_despatch_id'], array('class' => 'link', 'target' => '_blank')) . ')' :
								CHtml::link($model->warehousereceiptBillId['bill_number'], $bill_link, array('class' => 'link', 'target' => '_blank')); ?>
						<?php
						} else {
							echo ($model->warehousereceiptwarehouseidfrom['warehouse_name'] != "") ? $model->warehousereceiptwarehouseidfrom['warehouse_name'] . '(' . $model->warehousereceiptDespatch['warehousedespatch_no'] . ')' :
								$model->warehousereceiptBillId['bill_number'];
						} ?>
					</b>
				</td>
			</tr>
		</table>
		<br>
		<table class="item_table">
			<tr>
				<th style="width: 40px;">Sl No</th>
				<th>Item</th>
				<th style="width: 100px;">Dimension</th>
				<th style="width: 100px;">Batch</th>
				<th style="width: 100px;">Unit</th>
				<th style="width: 100px;">Quantity</th>
				<th style="width: 100px;">Rate</th>
				<th style="width: 100px;">Base Unit</th>
				<th style="width: 100px;">Base Quantity</th>
				<th style="width: 100px;">Base Rate</th>
				<th style="width: 100px;">Amount</th>
				<th style="width: 100px;">Accepted Quantity</th>
				<th style="width: 100px;">Accepted Rate</th>
				<th style="width: 100px;">Accepted Amount</th>
				<th style="width: 100px;">Tax</th>
				<th style="width: 100px;">Total Accepted Amount</th>
				<th style="width: 100px;">Rejected Quantity</th>
				<th style="width: 100px;">Remark</th>
			</tr>
			<?php
			$count_disp = 0;
			if (!empty($item_model)) {
				$count_disp = 0;
				foreach ($item_model as $key => $values) {
					
					$specificsql = "SELECT id, cat_id,brand_id, specification,unit"
						. " FROM {$tblpx}specification "
						. " WHERE id=" . $values['warehousereceipt_itemid'] . "";
					$specification = Yii::app()->db->createCommand($specificsql)->queryRow();
					$parent_categorysql = "SELECT * FROM {$tblpx}purchase_category "
						. " WHERE id='" . $specification['cat_id'] . "'";
					$parent_category = Yii::app()->db->createCommand($parent_categorysql)->queryRow();

					if ($specification['brand_id'] != NULL) {
						$brandsql = "SELECT brand_name FROM {$tblpx}brand 
							WHERE id=" . $specification['brand_id'] . "";
						$brand_details = Yii::app()->db->createCommand($brandsql)->queryRow();
						$brand = '-' . $brand_details['brand_name'];
					} else {
						$brand = '';
					}
					if(!empty($specification['specification'])){
						$spc_details = $parent_category['category_name'] . $brand . ' - ' . $specification['specification'];
					}else{
						$spc_details = 'Others';
					}
					
					if ($values['dimension'] != null) {
						$multiplying_quantity_arr = explode(" x ", $values['dimension']);
						$multiplying_quantity = $multiplying_quantity_arr['0'] * $multiplying_quantity_arr['1'];
					} else {
						$multiplying_quantity = 1;
					}

					$base_unit = Controller::GetStockItemunit($values['warehousereceipt_itemid']);
					if ($base_unit != null) {
						$basewarehouse_unit = $base_unit;
					} else {
						$basewarehouse_unit = $values['warehousereceipt_unit'];
					}

					if ($values['warehousereceipt_itemunit'] != null) {
						$warehousereceiptitem_unit_id = $values['warehousereceipt_itemunit'];
					} else {
						$warehousereceiptitem_unit_id = $values['warehousereceipt_unit'];
					}


					$warehouse_qty = $values['warehousereceipt_quantity'];
					$warehouse_rate = $values['warehousereceipt_rate'];
					$warehouse_unit = $values['warehousereceipt_unit'];

					if ($basewarehouse_unit != $warehousereceiptitem_unit_id) {

						if ($values['warehousereceipt_itemqty'] > 0) {
							$billedqty  = $values['warehousereceipt_itemqty'];
						} else if($values['warehousereceipt_itemqty'] ==0){
							$billedqty  = 0;
						}else{
							$billedqty  = $values['warehousereceipt_quantity'];
						}

						if ($values['warehousereceipt_itemunit'] != '') {
							$billedunit = $values['warehousereceipt_itemunit'];
						} else {
							$billedunit = $values['warehousereceipt_unit'];
						}

						if ($values['warehousereceipt_itemrate'] > 0) {
							$billedrate = $values['warehousereceipt_itemrate'];
						} else {
							$billedrate = $values['warehousereceipt_rate'];
						}
						$amount = $billedqty * $billedrate * $multiplying_quantity;
					} else {

						$billedunit = $values['warehousereceipt_unit'];
						$billedqty  = $values['warehousereceipt_quantity'];
						$billedrate = $values['warehousereceipt_rate'];
						$warehouse_unit = $values['warehousereceipt_unit'];
						$amount = $billedqty * $billedrate * $multiplying_quantity;
					}
					$amount_converted = $warehouse_qty * $warehouse_rate * $multiplying_quantity;
					

			?>
					<tr>
						<td><?php echo ($key + 1); ?></td>
						<td style="text-align:justify;"><?php echo $spc_details; ?></td>
						<td><?php echo ($values['dimension'] != '') ? "(" . $values['dimension'] . ")" : "N/A"; ?></td>
						<td><?php echo $values['warehousereceipt_batch']; ?></td>
						<td><?php echo $billedunit; ?></td>
						<td><?php echo $billedqty; ?></td>
						<td><?php echo isset($billedrate)?Controller::money_format_inr($billedrate, 2, 1):$values['billitem_rate']; ?></td>
						<td><?php echo $warehouse_unit; ?></td>
						<td><?php echo $warehouse_qty; ?></td>
						<td><?php echo Controller::money_format_inr($warehouse_rate, 2, 1);
							if ($amount != $amount_converted) {
								$count_disp++; ?><sup class="sup_rate">*</sup><?php } ?></td>
						<td><?php echo Controller::money_format_inr($amount, 2, 1); ?></td>
						<td><?php echo isset($values['warehousereceipt_accepted_quantity'])?$values['warehousereceipt_accepted_quantity']:0; ?></td>
						<td><?php echo Controller::money_format_inr($warehouse_rate, 2, 1); ?></td>
						<td><?php echo Controller::money_format_inr($amount, 2, 1); ?></td>
						<td>
							<?php 
							$tax_amount="" ;
							if($values['warehousereceipt_transfer_type_item_id']!=""){
								$billItem = Billitem::model()->findByPk($values['warehousereceipt_transfer_type_item_id']);
								$tax_amount = ($billItem['billitem_taxpercent']*$values['warehousereceipt_rate']*$values['warehousereceipt_accepted_quantity'])/100;
							}
							echo Controller::money_format_inr($tax_amount, 2, 1); ?>
						</td>
						<td><?php echo Controller::money_format_inr(($amount+$tax_amount), 2, 1); ?></td>
						<td><?php echo $values['warehousereceipt_rejected_quantity']; ?></td>
						<td><?php echo $values['remark_data']; ?></td>

						<!-- <td><?php echo $values['warehousereceipt_jono']; ?></td> -->
					</tr>
				<?php } ?>
			<?php } else { ?>
				<tr>
					<td colspan="19">No Items Receipted</td>
				</tr>
			<?php } ?>
		</table>
		<table>
			<tr>
				<td colspan="2" style="padding-left:0px;text-align:left;" class="no-border">Receipt Clerk : <b class="text-uppercase text-sm"><?php echo $model->clerk->first_name . ' ' . $model->clerk->last_name ?></b></td>
				<td colspan="3" class="no-border text-right">For <b class="text-uppercase text-sm"><?php echo $companyName; ?></b></td>
			</tr>
		</table>

		<div class="text-sm">
			<b>Remarks:</b><?php echo $model['warehousereceipt_remark']; ?><br>
			<?php if ($count_disp > 0) { ?>
				<b class="sup_rate">*</b> Value round off to 2 decimal places
			<?php } ?>
		</div>
	</div>
</body>

</html>