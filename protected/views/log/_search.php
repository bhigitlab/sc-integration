<?php
/* @var $this LogController */
/* @var $model JpLog */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'log_id'); ?>
		<?php echo $form->textField($model,'log_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'log_data'); ?>
		<?php echo $form->textArea($model,'log_data',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'log_table'); ?>
		<?php echo $form->textField($model,'log_table',array('size'=>60,'maxlength'=>100)); ?>
	</div>


	<div class="row">
		<?php echo $form->label($model,'log_action'); ?>
		<?php echo $form->textField($model,'log_action',array('size'=>6,'maxlength'=>6)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'log_action_by'); ?>
		<?php echo $form->textField($model,'log_action_by'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->