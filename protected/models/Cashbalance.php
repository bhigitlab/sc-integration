<?php

/**
 * This is the model class for table "{{cashbalance}}".
 *
 * The followings are the available columns in table '{{cashbalance}}':
 * @property integer $id
 * @property integer $cashbalance_type
 * @property integer $bank_id
 * @property double $cashbalance_opening_balance
 * @property double $cashbalance_deposit
 * @property double $cashbalance_withdrawal
 * @property double $cashbalance_closing_balance
 * @property integer $created_by
 * @property integer $created_date
 *
 * The followings are the available model relations:
 * @property Status $cashbalanceType
 * @property Bank $bank
 */
class Cashbalance extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
    
         
	public function tableName()
	{
		return '{{cashbalance}}';
	}

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        return array(
            array('cashbalance_type, cashbalance_opening_balance, created_by, created_date, cashbalance_date, company_id', 'required'),
            array('cashbalance_type, bank_id, created_by', 'numerical', 'integerOnly' => true),
            array('cashbalance_opening_balance, cashbalance_deposit, cashbalance_withdrawal, cashbalance_closing_balance', 'numerical'),
            array('cashbalance_opening_balance', 'numerical', 'integerOnly'=>false, 'min'=>1),
            array('cashbalance_deposit, cashbalance_withdrawal', 'numerical', 'integerOnly' => true, 'min' => 0),
            array('company_id', 'safe'),
            array('bank_id', 'checkexit'),
            array('bank_id', 'checkcashtype'),
            array('cashbalance_type', 'checkexit2'),
            array('id, cashbalance_type, bank_id, cashbalance_opening_balance, cashbalance_date, cashbalance_deposit, cashbalance_withdrawal, cashbalance_closing_balance, created_by, created_date', 'safe', 'on' => 'search'),
        );
    }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'cashbalanceType' => array(self::BELONGS_TO, 'Status', 'cashbalance_type'),
			'bank' => array(self::BELONGS_TO, 'Bank', 'bank_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'cashbalance_type' => 'Cashbalance Type',
			'bank_id' => 'Bank',
			'cashbalance_opening_balance' => 'Opening Balance',
			'cashbalance_deposit' => 'Deposit',
			'cashbalance_withdrawal' => 'Withdrawal',
			'cashbalance_closing_balance' => 'Closing Balance',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
			'cashbalance_date'=> 'Date',
                        'company_id' => 'Company',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('cashbalance_type',$this->cashbalance_type);
		$criteria->compare('bank_id',$this->bank_id);
		$criteria->compare('cashbalance_date',$this->cashbalance_date);
		$criteria->compare('cashbalance_opening_balance',$this->cashbalance_opening_balance);
		$criteria->compare('cashbalance_deposit',$this->cashbalance_deposit);
		$criteria->compare('cashbalance_withdrawal',$this->cashbalance_withdrawal);
		$criteria->compare('cashbalance_closing_balance',$this->cashbalance_closing_balance);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('created_date',$this->created_date);
                $criteria->compare('company_id',$this->company_id);
		$criteria->order = 'bank_id ASC';
		$user = Users::model()->findByPk(Yii::app()->user->id);
                $arrVal = explode(',', $user->company_id);
                $newQuery = "";
                foreach($arrVal as $arr) {
                    if ($newQuery) $newQuery .= ' OR';
                    $newQuery .= " FIND_IN_SET('".$arr."', company_id)";
                }
                $criteria->addCondition($newQuery);
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                        'pagination'=>false,
		));
	}
        
        public function checkexit($attribute, $params) {

            $id = Cashbalance::model()->findByAttributes(array('id' => $this->id));
            if(!empty($id)){
                if($this->bank_id != NULL){
                    $bank = Cashbalance::model()->findByAttributes(array('bank_id' => $this->bank_id),array('condition'=>'id != '.$this->id.' AND company_id = '.$this->company_id.''));
                    if(!empty($bank)) {
                        $this->addError("bank_id", ("Bank already exist"));
                    }
                }
            } else{
                if($this->bank_id != NULL){
                    $bank = Cashbalance::model()->findByAttributes(array('bank_id' => $this->bank_id,'company_id' => $this->company_id));
                    if(!empty($bank)) {
                        $this->addError("bank_id", ("Bank already exist"));
                    }
                }
            }
        }
        
        public function checkexit2($attribute, $params) {

            $id = Cashbalance::model()->findByAttributes(array('id' => $this->id));
            if(!empty($id)){
                if($this->cashbalance_type == 100){
                    $cashbalance_type = Cashbalance::model()->findByAttributes(array('cashbalance_type' => 100),array('condition'=>'id != '.$this->id.' AND company_id = '.$this->company_id.''));
                    if(!empty($cashbalance_type)) {
                        $this->addError("cashbalance_type", ("Cash balance already exist"));
                    }
                }
            } else{
                if($this->cashbalance_type == 100){
                    $cashbalance_type = Cashbalance::model()->findByAttributes(array('cashbalance_type' => 100,'company_id' => $this->company_id));
                    if(!empty($cashbalance_type)) {
                        $this->addError("cashbalance_type", ("Cash balance already exist"));
                    }
                }
            }
        }

        public function checkcashtype() {
            if ($this->cashbalance_type == 99) {
                if (empty($this->bank_id)) {
                    $this->addError("bank_id", ("Bank Name Required "));
                }
            }
        }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Cashbalance the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
