<!--------------ER------------------------->
<?php

$tblpx      = Yii::app()->db->tablePrefix;
$section    = '';
if ($data['type'] == 'edit_request') {
    if ($data['table'] == "{$tblpx}purchase") {
        $section = "Company Edit";
    }
    if ($data['table'] == "{$tblpx}expenses") {
        $section = "Daybook";
    } else if ($data['table'] == "{$tblpx}dailyvendors") {
        $section    = "Vendor Payment";
    }
    if ($data['table'] == "{$tblpx}subcontractor_payment") {
        $section    = "Subcontractor Payment";
    }
    if ($data['table'] == "{$tblpx}dailyexpense") {
        $section    = "Daily Expense";
    }

    if ($data['table'] == "{$tblpx}dailyreport") {
        $section    = "Daily Report";
    }
}
?>
<?php
if (isset(Yii::app()->user->role) && (in_array('/dashboard/poapproval', Yii::app()->user->menuauthlist))) {
if ($data['type'] == 'po_approval') {
?>
    <div class="parent_row">
        <div class="row box-collapse">
            
            <div class="col-sm-12 col-md-1 logo_sec nowrap">
            
                <button class="btn_logo" style="margin-left:23px;"> PO </button>
            </div>
            <div class="col-sm-12 col-md-8 content_sec">
                <div class="row  clearfix">
                    <div class="col-sm-12  col-md-8">
                        <div class="list-item">
                            <?php
                            $purchase_data = Purchase::model()->findByPk($data['id']);
                            $user = Users::model()->findByPk($data['user_id']);
                            ?>
                            <?php
                            if ($purchase_data->permission_status == 'No') {
                            ?>
                                <b><?php echo $user->first_name . ' ' . $user->last_name; ?></b> requested rate approval for <b>
                                    PO <?php echo $purchase_data->purchase_no ?></b> for <b><?php echo $purchase_data->project->name; ?></b>
                            <?php } else { ?>
                                <b><?php echo $user->first_name . ' ' . $user->last_name; ?></b> requested item approval for <b>
                                    PO <?php echo $purchase_data->purchase_no ?></b> for <b><?php echo $purchase_data->project->name; ?></b>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-4">
                        <div class="list-item pull-right">
                            <label class="w-auto">Date:</label>
                            <span><?php echo date('d-m-Y', strtotime($data['date'])); ?></span>

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-3">
                <div class="list-item text-right act_btn">
                    <?php
                    if ($purchase_data->permission_status == 'No') {
                    ?>
                        <button class="btn btn-xs btn_approve btn_sec approve_po" id="<?php echo $data['id']; ?>">Rate Approve</button>
                    <?php } else { ?>
                        <a class="btn btn-xs btn_approve btn_sec" target="_blank" href="<?php echo Yii::app()->createAbsoluteUrl('purchase/updatepurchase', array('pid' => $purchase_data->p_id)); ?>">Item Approve</a>
                    <?php } ?>
                    <button class="btn btn-xs btn_decline btn_sec po_rate_decline" id="<?php echo $data['id']; ?>">Decline </button>
                    <button class="compare_icon popup btn btn-xs btn-default" title="Compare">Details</i></button>
                    <button class="box_close btn btn-xs btn-default" style="display:none;">Close</i></button>
                </div>
            </div>
        </div>
        <div class="box">
            <div class="box-inner">
                <div class="panel panel-default">
                    <div class="panel-heading clearfix">                   
                        <h5 class="panel-title pull-left">Add Comments</h5>
                        <div class="panel-close pull-right">×</div>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-9">
                                <textarea rows="1" cols="12" class="form-control decline_remarks"></textarea>
                            </div>
                            <div class="col-sm-12 col-md-2">
                                <button class="btn btn-primary btn-sm decline_comment_submit" id="<?php echo $data['id']; ?>">submit</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="compare_box popuptext" style="display:none;">
            <div class="box_content">
                <table class="table w-100">
                    <thead>
                        <tr>
                            <th class="text-center">Company</th>
                            <th class="text-center">Project</th>
                            <th class="text-center">Expense Head</th>
                            <th class="text-center">Vendor</th>
                            <th class="text-center">Date</th>
                            <th class="text-center">Purchase No</th>
                            <th class="text-center">Amount</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <?php
                            $company = Company::model()->findByPk($purchase_data->company_id);
                            ?>
                            <td class="text-center"><?php echo $company->name; ?></td>
                            <td class="text-center"><?php echo $purchase_data->project->name; ?></td>
                            <td class="text-center"><?php echo $purchase_data->expensehead->type_name; ?></td>
                            <td class="text-center"><?php echo $purchase_data->vendor->name; ?></td>
                            <td class="text-center"><?php echo date('d-m-Y', strtotime($purchase_data->purchase_date)); ?></td>
                            <td class="text-center"><?php
                                                    echo CHtml::link($purchase_data->purchase_no,  $this->createAbsoluteUrl('purchase/viewpurchase', array('pid' => $purchase_data->p_id)), array('class' => 'link-text', 'target' => '_blank'));
                                                    ?></td>
                            <td class="text-center"><?php echo $purchase_data->total_amount; ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
<?php } }?>

<?php
/*  ER */
if (isset(Yii::app()->user->role) && (in_array('/dashboard/editrequest', Yii::app()->user->menuauthlist))) {
if ($data['type'] == 'edit_request') {
    echo $this->renderPartial(
        '_notifify_edit_request'        
    );    
 } } ?>
<?php
/* DR  */
if (isset(Yii::app()->user->role) && (in_array('/dashboard/deleterequest', Yii::app()->user->menuauthlist))) {

if ($data['type'] == 'delete_request') {
    $editrequest  = Deletepending::model()->findByPK($data['id']);
        $deletedata   = json_decode($editrequest->deletepending_data);    
        if(!empty($deletedata)){
        $tblpx      = Yii::app()->db->tablePrefix;
        $class="";
        $scquotationNo="";
        if($data['table'] == "{$tblpx}expenses") {
            $section = "Daybook";
            if ($deletedata->type == 72)
                $amount = $deletedata->receipt;
            else
                $amount = $deletedata->paid;
        } else if ($data['table'] == "{$tblpx}dailyexpense") {
            $section = "Dailyexpense";
            if ($deletedata->exp_type == 72)
                $amount = $deletedata->dailyexpense_receipt;
            else if ($deletedata->exp_type == 73)
                $amount = $deletedata->dailyexpense_paidamount;
            else
                $amount = 0;
        } else if ($data['table'] == "{$tblpx}dailyvendors") {
            $section    = "Vendor Payment";
            $amount     = $deletedata->amount + $deletedata->tax_amount;
        } else if ($data['table'] == "{$tblpx}subcontractor_payment") {
            $section    = "Subcontractor Payment";
            $amount     = $deletedata->amount + $deletedata->tax_amount;
        } else if($data['table'] == "{$tblpx}dailyreport") {
            $section    = "Daily Report";
            $amount     = $deletedata->amount;
        } else if($data['table'] == "{$tblpx}bills") {
            $section    = "Bills";
            $amount     = "";
        } else if($data['table'] == "{$tblpx}quotation") {
            $section    = "Quotation";
            $amount     = $deletedata->amount;
        } else if($data['table'] == "{$tblpx}invoice") {
            $section    = "Invoice";
            $amount     = $deletedata->amount;
        } else if($data['table'] == "{$tblpx}sc_payment_stage") {
            $parent_scquotation =Scquotation::model()->findByPk($deletedata->quotation_id) ;
            if(empty($parent_scquotation)){
                $class="hide";
            }else{
                $scquotationNo = "Quotation No : <b>".$parent_scquotation['scquotation_no']."</b>  ";
            }
            $section    = "Subcontractor Payment Stage";            
            $quotation_amount =Scquotation::model()->getTotalQuotationAmount($deletedata->quotation_id) ;
            $amount  = $quotation_amount * ($deletedata->stage_percent / 100);                        
        } else if($data['table'] == "{$tblpx}subcontractor") {
            $section    = "Subcontractor";
            $amount     = "";
        } else {
            $section    = "";
            $amount     = "";
        }
        if($section != "") {
        ?>
    <div >
        <div class="row">
            <div class="col-sm-12 col-md-1 logo_sec nowrap">
            <input type="checkbox" name="deleterequest" class="deleterequestcheck" id="deleterequest<?php $index; ?>" value="<?php echo $data['id']; ?>" style="vertical-align: text-bottom;margin-right: 3px;"/>
                <button class="btn_logo">DR</button>
            </div>
            <div class="col-sm-12 col-md-8 content_sec">
                <div class="row  clearfix">
                    <div class="col-sm-12 col-md-8">
                    <?php
                        $users = Users::model()->findByPk($data['user_id']);
                        ?>
                        <div class="list-item"><b><?php echo $users->first_name." ".$users->last_name; ?></b> Requested to delete <b><?php echo $section; ?></b> section.</div>
                                
                        <div class="list-item"><?php echo $scquotationNo ?>&nbsp; &nbsp;Description : <b><?php echo isset($deletedata->description)?$deletedata->description:""; ?></b> <?php if($amount !=""){ ?> &nbsp; &nbsp; Amount : <b><?php echo $amount; ?></b><?php } ?></div>               
                    </div>            
                    <div class="col-sm-12 col-md-4">
                        <div class="list-item pull-right">
                            <label>Date:</label>
                            <span><?php echo date('d-m-Y',strtotime($data['date'])); ?></span>
                    
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-3">
                <div class="list-item text-right act_btn">
                    <button class="btn btn-xs btn_approve btn_sec approve_delete" id="<?php echo $data['id']; ?>">Approve</button>&nbsp;<button class="btn btn-xs btn_decline btn_sec cancel_delete  <?php echo $class?>" id="<?php echo $data['id']; ?>">Decline </button>
                    <!-- <button class="compare_icon popup btn btn-xs btn-default  <?php echo $class?>" title="Compare">Details</i></button>
                    <button class="box_close btn btn-xs btn-default" style="display:none;">Close</i></button> -->
                </div>
            </div>
        </div>
        <div class="compare_box popuptext" style="display:none;">
            <div class="box_content">
                
            </div>
        </div>
    </div>
        <?php } }
} } ?>
<?php
/*  sc  */
if (isset(Yii::app()->user->role) && (in_array('/dashboard/scquotation', Yii::app()->user->menuauthlist))) {
if ($data['type'] == 'scquotation') {
    $quotation  = Scquotation::model()->findByPK($data['parent_id']);
?>
    <div class="parent_row">
        <div class="row">
        
            <div class="col-sm-12 col-md-1 logo_sec nowrap">
            <input type="checkbox" name="clearnotificationcheck[]" class="clearnotificationcheck clearcheck_all" id="clearnotification<?php $index; ?>" data-type="<?php echo $data['type']; ?>" value="<?php echo $data['parent_id']; ?>" style="vertical-align: text-bottom;"/>
                <button class="btn_logo">SC</button>
            </div>
            <div class="col-sm-12 col-md-8 content_sec">
                <div class="row  clearfix">
               
                    <div class="col-md-6">
                        <div class="list-item">
                            Quotation by <?php echo isset($quotation->subcontractor->subcontractor_name)?$quotation->subcontractor->subcontractor_name:""; ?> for project <?php echo isset($quotation->projects->name)?$quotation->projects->name:""; ?> needs permission. <br>Quotation Added by <?php echo $quotation->addedBy->first_name . " " . $quotation->addedBy->last_name; ?> on <?php echo date("d-m-Y", strtotime($quotation->scquotation_date)) ?>. to give permission.
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-3">
                        <div class="list-item pull-right">
                            <label class="w-auto">Date:</label>
                            <span><?php echo date('d-m-Y', strtotime($data['date'])); ?></span>

                        </div>
                    </div>
                    <div class="col-sm-12 col-md-3">
                        <div class="list-item pull-right">
                            <label class="w-auto">Amount: </label>
                            <span><?php echo Yii::app()->controller->money_format_inr(Scquotation::model()->getTotalQuotationAmount($quotation->scquotation_id), 2); ?></span>

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-3">
                <div class="list-item text-right act_btn">
                    <a href="<?php echo Yii::app()->createUrl("subcontractor/addquotation", array('scquotation_id' => $data['parent_id'])) ?>" class="btn btn-xs btn_approve btn_sec" target="_blank">View</a>
                </div>
            </div>
        </div>
    </div>
<?php } } ?>
<?php
/*  notification  */
if ($data['type'] == 'notification') {
    $notifications = Notifications::model()->findByPk($data['id']);
?>
    <div class="parent_row">
        <div class="row">
            <div class="col-sm-12 col-md-1 logo_sec nowrap">
            <input type="checkbox" name="clearnotificationcheck[]" class="clearnotificationcheck clearcheck_all" id="clearnotification<?php $index; ?>" data-type ="<?php echo $data['type']; ?>" value="<?php echo $data['id']; ?>" style="vertical-align: text-bottom;"/>
                <button class="btn_logo"><i class="fa fa-bell" aria-hidden="true"></i></button>
            </div>
            <div class="col-sm-12 col-md-8 content_sec">
                <div class="row  clearfix">
                    <div class="col-sm-12  col-md-8">
                        <div class="list-item">
                            <?php echo $notifications->message; ?>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-4">
                        <div class="list-item pull-right">
                            <label class="w-auto">Date:</label>
                            <span><?php echo date('d-m-Y', strtotime($data['date'])); ?></span>

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-3">
                <div class="list-item text-right act_btn">
                    <?php if ($notifications->requested_by == Yii::app()->user->id) { ?>
                        <span style="padding-left:5px;cursor:pointer" id="<?= $data['id'] ?>" class="view_not">
                            <i class="fa fa-eye" aria-hidden="true"></i>
                        </span>
                    <?php  } ?>
                </div>
            </div>
        </div>
    </div>
<?php }
if ($data['type'] == 'expense_notification') {
    $notifications = ExpenseNotification::model()->findByPk($data['id']);
    if(!empty($notifications)){
        if(isset($notifications->project->name)){
?>
    <div class="parent_row">
        <div class="row  box-collapse">
            
        
       
            <div class="col-sm-12 col-md-1 logo_sec nowrap">
            <input type="checkbox" name="clearnotificationcheck[]" class="clearnotificationcheck clearcheck_all" id="clearnotification<?php $index; ?>" data-type="<?php echo $data['type'];?>" value="<?php echo $data['id']; ?>" style="vertical-align: text-bottom;"/>
                <button class="btn_logo">EA</button>
            </div>
            <div class="col-sm-12 col-md-8 content_sec">
                <div class="row  clearfix">
                    <div class="col-sm-12  col-md-8">
                        <div class="list-item <?php echo $notifications->view_status == 1 ? 'blinking' : '' ?>">
                            <?php
                            if ($notifications->notification_type == 1) {
                                if (!empty($notifications->expense_perc)) {
                            ?>
                                    Expense reached <b>
                                        <?php echo isset($notifications['expense_perc']) ? $notifications['expense_perc'] . '%' :"0%" ?></b> of received amount in 
                                        <b><?php echo isset($notifications->project->name)?$notifications->project->name:""; ?></b>.
                                <?php
                                } else {
                                ?>
                                    Total expense exceeded received amount in <b><?php echo isset($notifications->project->name)?$notifications->project->name:""; ?></b>.
                                <?php
                                }
                            } elseif ($notifications->notification_type == 2) {
                                ?>
                                Payment limit <b><?php echo isset($notifications['expense_perc']) ? $notifications['expense_perc'] . '%' :"0%" ?></b> exceed in <b><?php echo isset($notifications->project->name)?$notifications->project->name:""; ?></b>.
                            <?php
                            }

                            ?>

                        </div>
                    </div>
                    <div class="col-sm-12 col-md-4">
                        <div class="list-item pull-right">
                            <label class="w-auto">Date:</label>
                            <span><?php echo date('d-m-Y', strtotime($notifications['generated_date'])); ?></span>

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-2">
                <div class="list-item text-right act_btn">
                    <?php
                    if ($notifications->view_status == 1) {
                    ?>
                        <button class="btn btn-xs btn_approve btn_sec view_expense" id="<?php echo $notifications['id']; ?>">Mark as read</button>
                    <?php } ?>
                    <button class="compare_icon popup btn btn-xs btn-default" title="Compare">Details</i></button>
                    <button class="box_close btn btn-xs btn-default" style="display:none;">Close</i></button>
                </div>
            </div>
        </div>
        <div class="compare_box popuptext" style="display:none;">
            <div class="box_content">
                <table class="table w-100">
                    <thead>
                        <tr>
                            <th class="text-center">Project</th>
                            <?php
                            if ($notifications->notification_type == 1) {
                                $balance = $notifications->advance_amount - $notifications->expense_amount;
                            ?>
                                <th class="text-center">Advance</th>
                                <th class="text-center">Expense</th>
                                <th class="text-center">Balance</th>
                            <?php
                            } elseif ($notifications->notification_type == 2) {
                            ?>
                                <th class="text-center">Amount</th>
                                <th class="text-center">Profit Percentage</th>
                                <th class="text-center">Project Quote</th>
                            <?php } ?>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="text-center"><?php echo isset($notifications->project->name)?$notifications->project->name : ""; ?></td>
                            <?php
                            if ($notifications->notification_type == 1) {
                            ?>
                                <td class="text-center"><?php echo $notifications->advance_amount; ?></td>
                                <td class="text-center"><?php echo $notifications->expense_amount; ?></td>
                                <td class="text-center"><?php echo $balance; ?></td>
                            <?php
                            } elseif ($notifications->notification_type == 2) {
                            ?>
                                <td class="text-center"><?php echo $notifications->expense_amount; ?></td>
                                <td class="text-center"><?php echo $notifications->project->profit_margin; ?></td>
                                <td class="text-center"><?php echo $notifications->project->project_quote; ?></td>
                            <?php
                            }
                            ?>



                        </tr>
                    </tbody>
                </table>
            </div>

        </div>
    </div>
<?php
        }
}
}
?>

<script>
    $(document).ready(function() {
        $(document).on('click', '.poapproval', function() {
            $('.notification_div').hide();
            $('.delete_request_div').hide();
            $('.edit_request_div').hide();
            $('.scquotation_div').hide();
            $('.allist_div').hide();
            $('.mr_request_div').hide();
            $('.poapproval_div').show();
            $('.expense_notification_div').hide();
        })
        
    $(document).on("click", "#clearNotification", function (e) {
    $('.loading-overlay').addClass('is-active');
   
    let selectedNotifications = [];
    $('input[name="clearnotificationcheck[]"]:checked').each(function() {
        let notificationId = $(this).val();
        let notificationType = $(this).data('type');
        selectedNotifications.push({id: notificationId, type: notificationType});
    });
    console.log(selectedNotifications);
    
    e.stopImmediatePropagation();
    e.preventDefault();
    $.ajax({
        type: 'POST',
        url: "<?php echo Yii::app()->createUrl('dashboard/allnotificationview') ?>",
        data: JSON.stringify({ selectedNotifications: selectedNotifications }),
        contentType: 'application/json',
        success: function(response) {
            let res = JSON.parse(response);
            if (res.status === 'success') {
                location.reload();
            } else {
                alert('Error clearing notifications.');
            }
        },
        error: function (data) {
            alert('Error occurred. Please try again.');
        }
    });
});



        $(document).on('click', '.mr_request', function() {
            $('.notification_div').hide();
            $('.delete_request_div').hide();
            $('.edit_request_div').hide();
            $('.scquotation_div').hide();
            $('.allist_div').hide();
            $('.mr_request_div').show();
            $('.poapproval_div').hide();
            $('.expense_notification_div').hide();
        })

        $(document).on('click', '.allist', function() {
            $('.allist_div').show();
            $('.scquotation_div').hide();
            $('.edit_request_div').hide();
            $('.delete_request_div').hide();
            $('.notification_div').hide();
            $('.poapproval_div').hide();
            $('.expense_notification_div').hide();
            $('.mr_request_div').hide();
        })

        $(document).on('click', '.scquotation', function() {
            $('.scquotation_div').show();
            $('.allist_div').hide();
            $('.edit_request_div').hide();
            $('.delete_request_div').hide();
            $('.notification_div').hide();
            $('.poapproval_div').hide();
            $('.expense_notification_div').hide();
            $('.mr_request_div').hide();
        })

        $(document).on('click', '.edit_request', function() {
            $('.edit_request_div').show();
            $('.scquotation_div').hide();
            $('.allist_div').hide();
            $('.delete_request_div').hide();
            $('.notification_div').hide();
            $('.poapproval_div').hide();
            $('.expense_notification_div').hide();
            $('.mr_request_div').hide();
        })

        $(document).on('click', '.delete_request', function() {
            $('.delete_request_div').show();
            $('.edit_request_div').hide();
            $('.scquotation_div').hide();
            $('.allist_div').hide();
            $('.notification_div').hide();
            $('.poapproval_div').hide();
            $('.expense_notification_div').hide();
            $('.mr_request_div').hide();
        })

        $(document).on('click', '.notification', function() {
            $('.notification_div').show();
            $('.delete_request_div').hide();
            $('.edit_request_div').hide();
            $('.scquotation_div').hide();
            $('.allist_div').hide();
            $('.poapproval_div').hide();
            $('.expense_notification_div').hide();
            $('.mr_request_div').hide();
        })

        $(document).on('click', '.expense_notification', function() {
            $('.notification_div').hide();
            $('.delete_request_div').hide();
            $('.edit_request_div').hide();
            $('.scquotation_div').hide();
            $('.allist_div').hide();
            $('.poapproval_div').hide();
            $('.expense_notification_div').show();
            $('.mr_request_div').hide();
        })

        $(document).on('click', '.tab_btn', function() {
            $(this).addClass("active");
            $(this).siblings().removeClass("active");
        });
        $('.compare_icon').click(function() {
            $(this).hide();
            $(this).siblings('.box_close').show();
            $(this).parents(".box-collapse").siblings('.compare_box').slideDown();

        });
        $('.box_close').click(function() {
            $(this).hide();
            $(this).siblings('.compare_icon').show();
            $(this).parents(".box-collapse").siblings('.compare_box').slideUp();
        });
        $(".parent_row").css("background-color", "#fafafa");
        $(".parent_row:even").css("background-color", "#fff");
    });

    
</script>
