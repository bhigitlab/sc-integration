<?php

class UsersController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';
    private $access_rules = array();

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
           // 'postOnly + delete', // we only allow deletion via POST request
        );
    }

    public function actions() {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xEBF4FB,
                'fontFile' => dirname(dirname(__FILE__)) . '\extensions\fonts\nimbus.ttf',
            ),
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
       /*  $this->access_rules = array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('register', 'pwdrecovery', 'pwdreset', 'message_page', 'activation', 'googleauth'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('myprofile', 'changepass', 'passchange', 'editprofile','userlist'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('create', 'update','delete', 'index', 'view','deleteuser'),
                'users' => array('@'),
                'expression' => 'yii::app()->user->role<=2',
            ),
            array('allow', // allow super admin user to perform this shift user option
                'actions' => array('usershift',),
                'users' => array('@'),
                'expression' => 'yii::app()->user->mainuser_role==1',
            ),            
            
            
        ); */
        
            $accessArr = array();
           $accessauthArr = array();
           $controller = Yii::app()->controller->id;
           if(isset(Yii::app()->user->menuall)){
               if (array_key_exists($controller, Yii::app()->user->menuall)) {
                   $accessArr = Yii::app()->user->menuall[$controller];
               } 
           }

           if(isset(Yii::app()->user->menuauth)){
               if (array_key_exists($controller, Yii::app()->user->menuauth)) {
                   $accessauthArr = Yii::app()->user->menuauth[$controller];
               }
           }
           $access_privlg = count($accessauthArr);
           
           return array(
                array('allow', // allow all users to perform 'index' and 'view' actions
                    'actions' => $accessArr,
                    'users' => array('@'),
                    'expression' => "$access_privlg > 0",

                ),

                array('allow', // allow admin user to perform 'admin' and 'delete' actions
                    'actions' => $accessauthArr,
                    'users' => array('@'),
                    'expression' => "$access_privlg > 0",

                ),
                array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('myprofile', 'changepass', 'passchange', 'editprofile'),
                'users' => array('@'),
                ),
                 array('allow', // allow super admin user to perform this shift user option
                'actions' => array('usershift'),
                'users' => array('@'),
                'expression' => 'yii::app()->user->mainuser_role==1',
                 ),   
                array('deny', // deny all users
                    'users' => array('*'),
                ),

            );

        //this will include in the register user. because captcha is not working if we use this.
        //if remove, other pages wont work.
        if (trim(strtolower(Yii::app()->controller->action->id) != 'captcha')) {
            // deny all users
            $this->access_rules[] = array('deny', 'users' => array('*'),);
        }

        return $this->access_rules;
    }

    public function actionEditprofile() {
        $model = $this->loadModel(Yii::app()->user->id);

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['Users'])) {
            $model->attributes = $_POST['Users'];

            $command = Yii::app()->db->createCommand('UPDATE ' . ($model->tableName()) . " SET first_name='" . $model->first_name . "',last_name='" . $model->last_name . "',email='" . $model->email . "', last_modified=now() WHERE userid=" . Yii::app()->user->id);

            if ($command->execute())
                Yii::app()->user->setFlash('success', 'Successfully updated.');
            else
                Yii::app()->user->setFlash('error', 'Update failed. Please try again.');

            $this->redirect(array('myprofile'));
        }

        $this->render('editprofile', array(
            'model' => $model,
        ));
    }

    public function actionUsershift()
    {
         $sftuser = Users::model()->findByPk($_POST['shift_userid']);
         //Set current user to new user details
         Yii::app()->user->id = $_POST['shift_userid'];
         Yii::app()->user->role = $sftuser->user_type;
         Yii::app()->user->firstname = $sftuser->first_name;
         Yii::app()->user->setState('fullname', $sftuser->first_name." ".$sftuser->last_name);
         Yii::app()->user->name = $sftuser->username;
         
         //Set company id
         
         $company = (explode(",",$sftuser->company_id));
         Yii::app()->user->setState('company_id', reset($company));
         
        $process = Yii::app()->createController('Site'); //create instance of FirstController
        $process=$process[0];
        //$process->menupermissions(); //call function 
        Controller::menuPermissionSet(); 
         
         echo "session_changed";
        
    }   
    //Google Through Login 

    public function actionGoogleAuth() {
        //Insert your cient ID and secret 
        //You can get it from : https://console.developers.google.com/

        $client_id = Yii::app()->params['client_id'];
        $client_secret = Yii::app()->params['client_secret'];
        $redirect_uri = Yii::app()->createAbsoluteUrl('users/googleauth');

        //Allowed Domain email id        
        $default_domain_authcheck = Yii::app()->params['default_domain_authcheck'];
     
        
        spl_autoload_unregister(array('YiiBase', 'autoload')); //Diable Default YII framewor's autoload class
        require(__DIR__ . '/../extensions/libraries/Google/autoload.php');

        /*         * **********************************************
          Make an API request on behalf of a user. In
          this case we need to have a valid OAuth 2.0
          token for the user, so we need to send them
          through a login flow. To do this we need some
          information from our API console project.
         * ********************************************** */
        $client = new Google_Client();
        $client->setClientId($client_id);
        $client->setClientSecret($client_secret);
        $client->setRedirectUri($redirect_uri);
        $client->addScope("email");
        $client->addScope("profile");
        

        /*         * *********************************************
          When we create the service here, we pass the
          client to it. The client then queries the service
          for the required scopes, and uses that when
          generating the authentication URL later.
         * ********************************************** */
        $service = new Google_Service_Oauth2($client);
        
        /*         * *********************************************
          If we have a code back from the OAuth 2.0 flow,
          we need to exchange that with the authenticate()
          function. We store the resultant access token
          bundle in the session, and redirect to ourself.
         */

        if (isset($_GET['code'])) {
       
            $client->authenticate($_GET['code']);
            
            //die("tetster");
            $access_token = $client->getAccessToken();
            
            spl_autoload_register(array('YiiBase', 'autoload'));
            Yii::app()->user->setState('access_token', $access_token);
            spl_autoload_unregister(array('YiiBase', 'autoload')); //Diable Default YII framewor's autoload class
            header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL));
            exit;
        }

        /*         * **********************************************
          If we have an access token, we can make
          requests, else we generate an authentication URL.
         * ********************************************** */
        spl_autoload_register(array('YiiBase', 'autoload'));

        if (isset(Yii::app()->user->access_token) && Yii::app()->user->access_token) {
            $access_token = Yii::app()->user->access_token;
            spl_autoload_unregister(array('YiiBase', 'autoload')); //Diable Default YII framewor's autoload class
            $client->setAccessToken($access_token);
            $unregFlag = 1;
        }

        if (!isset($unregFlag)) {
            spl_autoload_unregister(array('YiiBase', 'autoload'));
        }

        $user = $service->userinfo->get(); //get user info 

       // echo '<img src="' . $user->picture . '" style="float: right;margin-top: 33px;" />';

        if (isset($unregFlag)) {
            spl_autoload_register(array('YiiBase', 'autoload'));
        }

        
        $google_userarray = array('email' => $user->email,
            'last_name' => $user->familyName,
            'first_name' => $user->givenName,
            'google_id' => $user->id,
            'email_activation' => $user->verifiedEmail
        );

        spl_autoload_register(array('YiiBase', 'autoload'));
        
        
       $splitemail = explode('@',$google_userarray['email']);
        
        if($splitemail[1]!==$default_domain_authcheck){
            
            Yii::app()->user->setFlash('unauth_email', "Login Restricted. your emailid is not associated with ".ucfirst($default_domain_authcheck));
            $this->redirect(array('site/login'));
            Yii::app()->end();
        }
        
        

        extract($google_userarray);

        
        //Default User type while creating new user
        $google_userarray['user_type'] = Yii::app()->params['default_user_type'];

        $userrecord = Users::model()->findByAttributes(array('email' => trim($email)));

      
        if ($userrecord === null) {
			/*
			//user creation restricted
            $model = new Users;

            $current_date = date('Y-m-d H:i:s');

            $model->password = md5(base64_encode($email));
            $model->attributes = $google_userarray;
            $model->status = 0;
            $model->username = str_replace('@' . $default_domain_authcheck, '', $email);
            $model->reg_date = $current_date;
            $activation_key = md5(base64_encode($email . "-" . Yii::app()->params['enc_key']));
            $model->activation_key = $activation_key;

            if ($model->save()) {
                $success = 1;
            }
			*/
        } else {

            $model = Users::model()->findByPk($userrecord->userid);

            $model->google_id = $google_id;
            $model->google_link = $google_link;
            $model->google_picture_link = $google_picture_link;

            if ($model->save()) {
                $success = 1;
            }
        }
        
        if (isset($success)) {

            $userarray = array();
            // $userarray[''] = $model->userid;
            $userarray['google_id'] = $model->google_id;
            $userarray['username'] = $model->username;
            $userarray['password'] = $model->email;

            Yii::app()->user->setState('google_id', $model->google_id);

            $loginmodel = new LoginForm;

            $loginmodel->attributes = $userarray;
            // validate user input and redirect to the previous page if valid
            if ($loginmodel->validate() && $loginmodel->login())
                $this->redirect(Yii::app()->user->returnUrl);
        } else {
            $this->redirect(array('site/login'));
        }
    }

    public function actionPasschange() {
        $model = new Users;
        if (isset($_POST['Users'])) {
            $model->attributes = $_POST['Users'];
            if ($model->validate()) {
                $newpasword = md5(base64_encode($_POST['Users']['password']));
                $old_password = md5(base64_encode($_POST['Users']['old_password']));
                $is_password_changed=($newpasword!=$old_password)?1:0;
                $id = Yii::app()->user->id;
                $transaction = Yii::app()->db->beginTransaction();
                try {
                    $command = Yii::app()->db->createCommand('UPDATE ' . ($model->tableName()) . " SET password='$newpasword', last_modified=now() WHERE userid = '" . Yii::app()->user->id . "' AND password='$old_password'");
                    if ($command->execute()) {
                        if (defined('LOGIN_USER_TABLE') && LOGIN_USER_TABLE != '') {
                            //save to global users
                            if($is_password_changed){
                                $allowed_apps = Controller::getAllowedApps();
                                $globalcommand = Yii::app()->db->createCommand("UPDATE global_users SET passwd='$newpasword',updated_by='". Yii::app()->user->id."', updated_date=now() WHERE global_user_id = '" . Yii::app()->user->id . "' AND passwd='$old_password'");
                                if($globalcommand->execute()){ 
                                    if (is_array($allowed_apps)) {
                                        foreach ($allowed_apps as $value) { 
                                            $value = strtoupper($value);
                                            $query = "SELECT table_prefix FROM global_settings "
                                                . "WHERE access_types = '" . $value . "'";
                                            $command = Yii::app()->db->createCommand($query);
                                            $result = $command->queryRow();
                                            switch ($value) {
                                                case 'HRMS':
                                                    $user_exist = Yii::app()->db->createCommand()
                                                        ->select('*')
                                                        ->from("{$result['table_prefix']}employee")
                                                        ->where('emp_id=:emp_id', array(':emp_id' => $id))
                                                        ->queryRow();
                                                    if (!empty($user_exist)) {
                                                        $sql =  "UPDATE " . $result['table_prefix'] . "employee "
                                                            . " SET passwd = '" . $newpasword . "' WHERE emp_id = " . $id;
                                                        $hrmscommand = Yii::app()->db->createCommand($sql);
                                                        if(!$hrmscommand->execute()){
                                                            throw new Exception('some error occured');
                                                        }
                                                    }
                                                    break;
                                                case 'PMS':
                                                    $user_exist = Yii::app()->db->createCommand()
                                                        ->select('*')
                                                        ->from("{$result['table_prefix']}users")
                                                        ->where('userid=:userid', array(':userid' => $id))
                                                        ->queryRow();
                                                    if (!empty($user_exist)) {
                                                        $sql =  "UPDATE " . $result['table_prefix'] . "users "
                                                            . " SET password = '" . $newpasword. "' WHERE userid = " . $id;
                                                        $pmscommand = Yii::app()->db->createCommand($sql);
                                                        if(!$pmscommand->execute()){
                                                            throw new Exception('some error occured');
                                                        }
                                                    }
                                                    break;
                                                case 'CRM':
                                                    break;
                                            }
                                        }
                                    }
                                }else{
                                    throw new Exception('some error occured'); 
                                }
                            }
                        }
                        $transaction->commit();
                        Yii::app()->user->setFlash('success', 'Your new password updated successfully');
                        $this->redirect(array('users/passchange'));
                    } else { 
                        Yii::app()->user->setFlash('error', 'Error occurred while updating your new password. ');
                    }
                }catch (Exception $e) {
                    $transaction->rollBack();
                    Yii::app()->user->setFlash('error', 'Error occurred while updating your new password. ');
                                            
                } 
                $this->redirect(array('users/passchange'));
                Yii::app()->end();
            }
        }
        $this->render("_change_pwd_form", array('model' => $model));
        Yii::app()->end();
    }

    public function actionMessage_page() {
        $this->render('message_page');
    }

    public function actionMyProfile() {
        $model = new Users;
        $myid = Yii::app()->user->id;
        $this->render('myprofile', array('model' => $this->loadModel($myid),));
    }

    public function actionRegister() {
        if (!Yii::app()->user->isGuest) {
            $this->redirect(array('/'));
        }

        $model1 = new UserRegister;
        if (isset($_POST['UserRegister'])) {
            $model1->attributes = $_POST['UserRegister'];
            if ($model1->validate()) {
                $model = new Users;
                $current_date = date('Y-m-d H:i:s');

                $_POST['UserRegister']['password'] = md5(base64_encode($_POST['UserRegister']['password']));
                $model->attributes = $_POST['UserRegister'];
                $model->reg_ip = $_SERVER['REMOTE_ADDR'];
                $model->reg_date = $current_date;
                $activation_key = md5(base64_encode($_POST['UserRegister']['email'] . "-" . Yii::app()->params['enc_key']));
                $model->activation_key = $activation_key;

                if ($model->save()) {
                    $activation_url = 'http://' . $_SERVER['HTTP_HOST'] . $this->createUrl('users/activation', array("avkey" => $activation_key));
                    $subject = "Please verify your email address to activate your tourcorner account";
                    $message = 'Hello,';
                    $message .= "Your activation link to activate and to use Tourcorner.com account: " . $activation_url . ".<br />";
                    $message .= "If clicking the link above does not work, copy and paste the URL in a new browser window instead. ";
                    $message .= Yii::app()->params['mail_footer_msg'];

                    $email = Yii::app()->email;
                    $email->to = $_POST['UserRegister']['email'];
                    $email->subject = $subject;
                    $email->view = '/template1';
                    $email->viewVars = array('message' => $message);
                    if ($email->send()) {
                        Yii::app()->user->setFlash('flash_msg', 'Please check your email. For email verification we have sent instructions to your email address. <b>' . $_POST['UserRegister']['email'] . "</b>");
                    } else {
                        Yii::app()->user->setFlash('flash_msg', 'Mail sending error. Please try again.');
                    }
                }

                $this->redirect(array('users/message_page'));
                return;
            }
        }
        $this->render('register', array('model' => $model1));
    }

    public function actionActivation() {
        if (!Yii::app()->user->isGuest || !isset($_GET['avkey']))
            $this->redirect(array('/'));
        $model = new Users;

        if (isset($_GET['avkey']) && strlen($_GET['avkey']) == 32) {
            $activation_key = $_GET['avkey'];
            $row = Users::model()->find("activation_key = '$activation_key' AND status IS NULL AND email_activation IS NULL");

            if (count($row) == 1) {
                $command = Yii::app()->db->createCommand('UPDATE ' . ($model->tableName()) . " SET email_activation='1',status=1, last_modified=now() WHERE activation_key = '$activation_key' AND status IS NULL");
                if ($command->execute()) {
                    Yii::app()->user->setFlash('success', 'Successfully verified your email and now activated your account.');
                    $this->redirect(array('site/login'));
                } else {
                    Yii::app()->user->setFlash('error', 'Error occurred while email verification. Please try again with same link. <br /> ' . CHtml::link('Go Home', array('/')));
                    $this->redirect(array('users/message_page'));
                }
            } else {
                Yii::app()->user->setFlash('error', 'Wrong activation URL. Please try again with same link  from your email. <br /> ' . CHtml::link('Go Home', array('/')));
                $this->redirect(array('users/message_page'));
            }
        } else {
            Yii::app()->user->setFlash('error', 'Wrong activation URL. Please try again with same link from your email. <br /> ' . CHtml::link('Go Home', array('/')));
            $this->redirect(array('users/message_page'));
        }
    }

    public function actionPwdrecovery() {

        if (!Yii::app()->user->isGuest) {
            $this->redirect(array('/'));
            Yii::app()->end();
        }

        $model = new Users;

        if (isset($_POST['Users'])) {
            $model->attributes = $_POST['Users'];
            if ($model->validate()) {

                $command = Yii::app()->db->createCommand('UPDATE ' . ($model->tableName()) . " SET activation_key=md5(now()), last_modified=now() WHERE userid=" . $model->ruser_id);
                $command->execute();

                $user = $model->findbyPk($model->ruser_id);

                //Creating activation id
                $key_part1 = md5(base64_encode($user->email . "-" . $user->activation_key . "-" . $user->last_modified));
                $key_part2 = md5(Yii::app()->params['enc_key'] . "^" . $user->email);
                $new_activation_key = $key_part1 . $key_part2;
                //End activation id
                $activation_url = 'http://' . $_SERVER['HTTP_HOST'] . $this->createUrl('users/pwdreset', array("vkey" => $new_activation_key));
                $subject = "You have requested the password recovery site " . Yii::app()->name;
                $message = 'Hello,';
                $message .= "You have requested the password recovery site " . Yii::app()->name . ". To receive a new password, go to " . $activation_url . ".<br />";
                $message .= "If clicking the link above does not work, copy and paste the URL in a new browser window instead. The URL will expire in 24 hours for security reasons.";
                $message .= Yii::app()->params['mail_footer_msg'];

                $email = Yii::app()->email;
                $email->to = $user->email;
                $email->subject = $subject;
                $email->view = '/template1';
                $email->viewVars = array('message' => $message);
                if ($email->send()) {
                    Yii::app()->user->setFlash('flash_msg', 'Please check your email. An instructions was sent to your email address. ' . CHtml::link('Go to home', array('/')));
                } else {
                    Yii::app()->user->setFlash('flash_msg', 'Mail sending error. Please try again.');
                }
                $this->refresh();

                // form inputs are valid, do something here
                // return;
            }
//            echo Yii::app()->ruserid;
        }
        $this->render('pwdrecovery', array('model' => $model));
    }

    public function actionPwdreset() {
        if (!Yii::app()->user->isGuest) {
            $this->redirect(array('/'));
        }

        $model = new Users;
        $show_form = 0;

        if (isset($_POST['ajax']) && $_POST['ajax'] === 'users-pwdrest-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        if (isset($_POST['Users'])) {
            $show_form = 2;
            $model->attributes = $_POST['Users'];
            if ($model->validate()) {

                $vkey_email = (substr($_GET['vkey'], 32));

                $newpasword = md5(base64_encode($_POST['Users']['new_pwd']));

                $command = Yii::app()->db->createCommand('UPDATE ' . ($model->tableName()) . " SET password='$newpasword', last_modified=now() WHERE md5( concat( \"" . Yii::app()->params['enc_key'] . "\", \"^\", `email` )) = '$vkey_email'");
                if ($command->execute()) {
                    Yii::app()->user->setFlash('success', 'Your new password updated successfully. Now you can login with your new password.');
                    $this->redirect(array('site/login'));
                } else {
                    Yii::app()->user->setFlash('error', 'Error occurred while updating your new password. ' . CHtml::link('Please try again', array('users/pwdrecovery')) . ' OR ' . CHtml::link('Go Home', array('/')));
                    $this->redirect(array('users/message_page'));
                }
                Yii::app()->end();
            }
        }

        if (isset($_GET['vkey']) && strlen($_GET['vkey']) == 64) {

            $vkey_email = (substr($_GET['vkey'], 32));
            $verification_key = (substr($_GET['vkey'], 0, 32));

            $row = Users::model()->find("status = 1 AND email_activation=1 AND md5( concat( \"" . Yii::app()->params['enc_key'] . "\", \"^\", `email` )) = '$vkey_email'");

            if (count($row) == 1) {
                $vkey_match = md5(base64_encode($row->email . "-" . $row->activation_key . "-" . $row->last_modified));
                if ($vkey_match == $verification_key) {
                    $show_form = 1;
                    $this->render('pwdreset', array('model' => $model, 'act_status' => $show_form));
                }
            }
        }

        if ($show_form == 0) {
            Yii::app()->user->setFlash('error', "Verification code is not matching with any record. Please try again.");
            $this->redirect(array('users/pwdrecovery'));
            Yii::app()->end();
        } elseif ($show_form == 2) {
            $this->render('pwdreset', array('model' => $model));
        }
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array('model' => $this->loadModel($id),));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        if(isset($_GET['layout']))
                $this->layout = false;
        $model = new Users;

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['Users'])) {

            $current_date = date('Y-m-d H:i:s');

            $_POST['Users']['password'] = md5(base64_encode($_POST['Users']['password']));
            $model->attributes = $_POST['Users'];

            $model->reg_ip = $_SERVER['REMOTE_ADDR'];
            $model->reg_date = $current_date;
            $activation_key = md5(base64_encode($_POST['Users']['email'] . "-" . Yii::app()->params['enc_key']));
            $model->activation_key = $activation_key;
            $model->email_activation = 1;
            if (!empty($_POST['Users']['company_id'])) {
				$company = implode(",",$_POST['Users']['company_id']);
				$model->company_id = $company;
			} else {
				$model->company_id = NULL;
			}
            $model->last_modified =$current_date;
            if ($model->save())
                $this->redirect(array('index'));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
		
        if(isset($_GET['layout']))
                $this->layout = false;
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
         $this->performAjaxValidation($model);
         
        if (isset($_POST['Users'])) {
            $is_login_changed = 0;
            if((trim($_POST['Users']['password'])!='' && (md5(base64_encode($_POST['Users']['password'])) !=$model->password))|| ($model->username !=$_POST['Users']['username'])){
                $is_login_changed = 1;
            }
            if(trim($_POST['Users']['password'])=='')
                $_POST['Users']['password']=$model->password;
            else
                $_POST['Users']['password'] = md5(base64_encode($_POST['Users']['password']));
            
            $model->attributes = $_POST['Users'];
            if (is_array($model->company_id)) {
                $company = implode(",", $model->company_id);
                $model->company_id = $company;
            }
            $transaction = Yii::app()->db->beginTransaction();
            try {
                if ($model->save()){
                    if (defined('LOGIN_USER_TABLE') && LOGIN_USER_TABLE != '') {
                        //save to global users
                        if($is_login_changed){
                            $globalmodel = GlobalUsers::model()->findByPk($id);
                            $allowed_apps = Controller::getAllowedApps();
                            $globalmodel->username = isset($_POST['Users']['username'])?$_POST['Users']['username']:$globalmodel->username;
                            $globalmodel->passwd = $_POST['Users']['password'];
                            $globalmodel->firstname =isset($_POST['Users']['first_name'])?$_POST['Users']['first_name']:$globalmodel->firstname;
                            if($globalmodel->save()){ 
                                if (is_array($allowed_apps)) {
                                    foreach ($allowed_apps as $value) { 
                                        $value = strtoupper($value);
                                        $query = "SELECT table_prefix FROM global_settings "
                                            . "WHERE access_types = '" . $value . "'";
                                        $command = Yii::app()->db->createCommand($query);
                                        $result = $command->queryRow();
                                        switch ($value) {
                                            case 'HRMS':
                                                $user_exist = Yii::app()->db->createCommand()
                                                    ->select('*')
                                                    ->from("{$result['table_prefix']}employee")
                                                    ->where('emp_id=:emp_id', array(':emp_id' => $id))
                                                    ->queryRow();
                                                if (!empty($user_exist)) {
                                                    $sql =  "UPDATE " . $result['table_prefix'] . "employee "
                                                        . "SET username = '" . $globalmodel->username . "'"
                                                        . " , passwd = '" . $globalmodel->passwd . "' WHERE emp_id = " . $id;
                                                    $hrmsusermodel = Yii::app()->db->createCommand($sql);
                                                    if(!$hrmsusermodel->execute()){
                                                        throw new Exception("Some error occured");
                                                    }
                                                }
                                                break;
                                            case 'PMS':
                                                $user_exist = Yii::app()->db->createCommand()
                                                    ->select('*')
                                                    ->from("{$result['table_prefix']}users")
                                                    ->where('userid=:userid', array(':userid' => $id))
                                                    ->queryRow();
                                                if (!empty($user_exist)) {
                                                    $sql =  "UPDATE " . $result['table_prefix'] . "users "
                                                        . "SET username = '" . $globalmodel->username . "'"
                                                        . " , password = '" . $globalmodel->passwd . "' WHERE userid = " . $id;
                                                    $pmsusermodel = Yii::app()->db->createCommand($sql);
                                                    if(!$pmsusermodel->execute()){
                                                        throw new Exception("Some error occured");
                                                    }
                                                }
                                                break;
                                            case 'CRM':
                                                break;
                                        }
                                    }
                                }
                            }else{
                                throw new Exception($globalmodel->getErrors()); 
                            }
                        }
                    }    
                }else{ 
                    throw new Exception($model->getErrors());
                }
                $transaction->commit();
                Yii::app()->user->setFlash('success', "Updated successfully");
                $this->redirect(array('index'));
            }catch (Exception $e) {
                $transaction->rollBack();
                Yii::app()->user->setFlash('danger', "An error occurred!");
                $this->redirect(array('index'));                    
            } 
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete() {
        $id = $_POST['roleId'];
        $model = $this->loadModel($id);
        $transaction = Yii::app()->db->beginTransaction();
        try {
            if (!$model->delete()) {
                $success_status = 0;
                throw new Exception(json_encode($model->getErrors()));
            } else {
                $success_status = 1;
            }
            $transaction->commit();
        } catch (Exception $error) {
            $transaction->rollBack();
            $success_status = 0;
        } finally {
            if ($success_status == 1) {
                echo json_encode(array('response' => 'success', 'msg' => 'Data Deleted Successfully'));
            } else {
                if ($error->errorInfo[1] == 1451) {
                    echo json_encode(array('response' => 'warning', 'msg' => 'Cannot Delete ! This record alredy in use.'));
                } else {
                    echo json_encode(array('response' => 'error', 'msg' => 'Some Error Occured'));
                }
            }
        }
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $model = new Users('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Users']))
            $model->attributes = $_GET['Users'];

        $this->render('newlist', array(
            'model' => $model,
             'dataProvider' =>  $model->search(),
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Users('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Users']))
            $model->attributes = $_GET['Users'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model = Users::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'users-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
    
    
    public function actionuserList()
		{
		$tblpx = Yii::app()->db->tablePrefix;	
		
		if(isset($_GET['Users']))
			{
			 $menus=$_GET['Users'];
			}
			
			  $sql= Yii::app()->db->createCommand('select parent.userid, parent.first_name,parent.last_name,parent.status,
			  parent.username,parent.phonenumber,parent.email,
			  u.first_name as parent_name
			  from '.$tblpx.'users as u
			  inner join '.$tblpx.'users as parent on u.userid=parent.reporting_person order by userid DESC
			  
			  ')->queryAll();	
			//	print_r($sql); die();
					
				
				
			 $this->render('list',array(
					  'model'=>$sql,
			  ));
		}
		
		
//	 public function actionDeleteuser($id) {
//	  $tblpx = Yii::app()->db->tablePrefix;	
//	  $query=Yii::app()->db->createCommand('DELETE FROM '.$tblpx.'users WHERE userid='.$id)->execute();
//	  $this->redirect(array('index'));
//		
//	
//	}

}
