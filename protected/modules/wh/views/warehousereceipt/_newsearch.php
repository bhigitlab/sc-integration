<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.min.js"></script>
<div class="page_filter">
    <?php
    $search_item_id = $item_id;
    $transfer_type = ($transfer_type != '') ? $transfer_type : '';
    $bill_or_despatch_id = ($bill_or_despatch_id != '') ? $bill_or_despatch_id : '';
    $form = $this->beginWidget('CActiveForm', array(
        'action' => Yii::app()->createUrl($this->route),
        'method' => 'get',
    ));
    ?>
    <div class="row">
        <div class="form-group col-xs-12 col-md-2">
            <label>Receipt No</label>
            <input type="text" name="receipt_no" id="receipt_no" value="<?php echo $model->warehousereceipt_no ?>" placeholder="Receipt No" class="form-control">
            <input type="hidden" name="bill_or_despatch_data" id="bill_or_despatch_data" value="">
        </div>
        <div class="form-group col-xs-12 col-md-2">
            <label>Transfer Type</label>
            <?php
            $transfer_type_arr = ['1' => 'Purchase', '2' => 'Despatch'];
            echo CHtml::dropDownList(
                'transfer_type',
                $transfer_type,
                $transfer_type_arr,
                array(
                    'empty' => 'Choose Transfer Type',
                    'class' => 'inputs target transfer_type form-control',
                    'ajax' => array(
                        'url' => array('warehousereceipt/getBillOrDespatchId'),
                        'type' => 'POST',
                        'data' => array('transfer_type' => 'js:this.value'),
                        'update' => '#bill_or_despatch_id',
                    )
                )
            );
            $data = CHtml::listData(Controller::getCategoryOptions(), 'id', 'text', 'group');
            ?>
        </div>
        <div class="form-group col-xs-12 col-md-2">
            <label>Warehouse Name</label>
            <?php
            if (yii::app()->user->role == 5) {
                $warehouse_data = Controller::getCategoryOptions($type = 1);
                $data = CHtml::listData(Controller::getCategoryOptions($type = 1), 'id', 'text', 'group');
            } else {
                $warehouse_data = Controller::getCategoryOptions();
                $data = CHtml::listData(Controller::getCategoryOptions(), 'id', 'text', 'group');
            }
            if (!empty($model['warehousereceipt_warehouseid'])) {
                $warehouse_to = $model['warehousereceipt_warehouseid'];
            } else {
                if (count($warehouse_data) == 1) {
                    $warehouse_to = $warehouse_data[0]['id'];
                } else {
                    $warehouse_to = '';
                }
            }

            echo CHtml::dropDownList(
                'warehouse',
                $warehouse_to,
                $data,
                array('empty' => 'Choose Warehouse To', 'class' => 'inputs target warehouse_to form-control')
            );
            ?>
        </div>
        <div class="form-group col-xs-12 col-md-2">
            <label>Bill/ Despatch Number</label>
            <?php
            $bill_or_despatch_id_arr = CHtml::listData(Controller::getCategoryBillAndDespatch($transfer_type), 'id', 'text', 'group');
            echo CHtml::dropDownList(
                'bill_or_despatch_id',
                $bill_or_despatch_id,
                $bill_or_despatch_id_arr,
                array(
                    'empty' => 'Choose Bill/ Despatch Number',
                    'class' => 'inputs target bill_or_despatch_id form-control',
                    'ajax' => array(
                        'url' => array('warehousereceipt/getTransferType'),
                        'type' => 'POST',
                        'data' => array('bill_or_despatch_id' => 'js:this.value', 'bill_or_despatch_data' => 'js:$(\'#bill_or_despatch_id option:selected\').text()'),
                        'update' => '#transfer_type',
                    )
                )
            );

            ?>
        </div>
        <div class="form-group col-xs-12 col-md-2">
            <label>Warehouse From</label>
            <?php
            if (!empty($model['warehousereceipt_warehouseid_from'])) {
                $warehouse_from = $model['warehousereceipt_warehouseid_from'];
            } else {
                $warehouse_from = '';
            }
            $data = CHtml::listData(Controller::getCategoryOptions(), 'id', 'text', 'group');
            echo CHtml::dropDownList(
                'warehouse_from',
                $warehouse_from,
                $data,
                array('empty' => 'Choose Warehouse From', 'class' => 'inputs target warehouse warehouse-from form-control')
            );
            ?>
        </div>
        <div class="form-group col-xs-12 col-md-2">
            <label>Item </label>
            <select style="padding: 2.5px 0px;" id="item_id" class="select_box form-control" name="item_id">
                <option value="">Select Item</option>
                <?php

                if (!empty($specification)) {
                    foreach ($specification as $key => $value) {
                        ?>
                        <option value="<?php echo $value['id'] ?>" <?php echo ($value['id'] == $search_item_id) ? 'selected' : ''; ?>>
                            <?php echo $value['data']; ?>
                        </option>
                    <?php }
                } ?>
            </select>
        </div>
        <div class="form-group col-xs-12 col-md-2">
            <label>Clerk</label>
            <select id="clerk" class="select_box form-control" name="clerk">
                <option value="">Select Clerk</option>
                <?php
                if (!empty($clerk)) {
                    foreach ($clerk as $key => $value) {
                        ?>
                        <option value="<?php echo $value['userid'] ?>" <?php echo ($value['userid'] == $model->warehousereceipt_clerk) ? 'selected' : ''; ?>>
                            <?php echo $value['first_name'] . ' ' . $value['last_name']; ?>
                        </option>

                    <?php }
                } ?>
            </select>
        </div>
        <div class="form-group col-xs-12 col-md-2">
            <?php
            if (!isset($_GET['date_from']) || $_GET['date_from'] == '') {
                $datefrom = "";
            } else {
                $datefrom = $_GET['date_from'];
            }
            ?>

            <label>Date From :</label>
            <?php echo CHtml::textField('date_from', $datefrom, array("id" => "date_from", "readonly" => true, "placeholder" => "From", "class" => "form-control")); ?>
        </div>
        <div class="form-group col-xs-12 col-md-2">
        <label>To</label>
            <?php
            if (!isset($_GET['date_to']) || $_GET['date_to'] == '') {
                $date_to = "";
            } else {
                $date_to = $_GET['date_to'];
            }
            ?>
            <?php echo CHtml::textField('date_to', $date_to, array("id" => "date_till", "readonly" => true, "placeholder" => "To", "class" => "form-control")); ?>
        </div>
        <div class="form-group col-xs-12 col-md-2 margin-top-20">
            <button type="submit" name="yt0" class="btn btn-sm btn-primary">Go</button>
            <button type="reset" id="reset" name="yt1" class="btn btn-sm btn-default">Reset</button>
        </div>
    </div>
    <?php $this->endWidget(); ?>
</div>

<script>
    $('#reset').click(function () {
        location.href = '<?php echo $this->createUrl('index'); ?>';
    })
    $(function () {
        $("#date_from").datepicker({ dateFormat: 'dd-mm-yy' });

        $("#date_till").datepicker({ dateFormat: 'dd-mm-yy' });

        $("#date_from").change(function () {
            $("#date_till").datepicker('option', 'minDate', $(this).val());
        });

        $("#date_till").change(function () {
            $("#date_from").datepicker('option', 'maxDate', $(this).val());
        });


    });

    $(document).ready(function () {
        $(".select_box").select2();
        $("#transfer_type").select2();
        $("#warehouse").select2();
        $("#bill_or_despatch_id").select2();
        $("#warehouse_from").select2();

    });

</script>
