<?php

/**
 * This is the model class for table "{{warehousebilled_stock}}".
 *
 * The followings are the available columns in table '{{warehousebilled_stock}}':
 * @property integer $id
 * @property integer $warehouse_id
 * @property integer $category_id
 * @property integer $warehousestock_id
 * @property integer $purchase_item_id
 * @property integer $bill_item_id
 * @property double $quantity
 * @property integer $unit_id
 * @property string $batch
 * @property double $rate
 * @property double $amount
 * @property string $item_entry_type
 * @property string $created_date
 * @property string $updated_date
 *
 * The followings are the available model relations:
 * @property PurchaseCategory $category
 * @property PurchaseItems $purchaseItem
 * @property Billitem $billItem
 * @property Warehousestock $warehousestock
 * @property Unit $unit
 * @property Warehouse $warehouse
 */ 
class WarehousebilledStock extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public $date_from;
	public $date_to;
	public function tableName()
	{
		return '{{warehousebilled_stock}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('warehouse_id, category_id, warehousestock_id, quantity, unit_id, created_date, updated_date', 'required'),
			array('warehouse_id, category_id, warehousestock_id, purchase_item_id, bill_item_id, unit_id', 'numerical', 'integerOnly'=>true),
			array('quantity, rate, amount', 'numerical'),
			array('batch', 'length', 'max'=>300),
			array('item_entry_type', 'length', 'max'=>1),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, warehouse_id, category_id, warehousestock_id, purchase_item_id, bill_item_id, quantity, unit_id, batch, rate, amount, item_entry_type, created_date, updated_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'category' => array(self::BELONGS_TO, 'PurchaseCategory', 'category_id'),
			'purchaseItem' => array(self::BELONGS_TO, 'PurchaseItems', 'purchase_item_id'),
			'billItem' => array(self::BELONGS_TO, 'Billitem', 'bill_item_id'),
			'warehousestock' => array(self::BELONGS_TO, 'Warehousestock', 'warehousestock_id'),
			'unit' => array(self::BELONGS_TO, 'Unit', 'unit_id'),
			'warehouse' => array(self::BELONGS_TO, 'Warehouse', 'warehouse_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'warehouse_id' => 'Warehouse',
			'category_id' => 'Category',
			'warehousestock_id' => 'Warehousestock',
			'purchase_item_id' => 'Purchase Item',
			'bill_item_id' => 'Bill Item',
			'quantity' => 'Quantity',
			'unit_id' => 'Unit',
			'batch' => 'Batch',
			'rate' => 'Rate',
			'amount' => 'Amount',
			'item_entry_type' => 'Item Entry Type',
			'created_date' => 'Created Date',
			'updated_date' => 'Updated Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('warehouse_id',$this->warehouse_id);
		$criteria->compare('category_id',$this->category_id);
		$criteria->compare('warehousestock_id',$this->warehousestock_id);
		$criteria->compare('purchase_item_id',$this->purchase_item_id);
		$criteria->compare('bill_item_id',$this->bill_item_id);
		$criteria->compare('quantity',$this->quantity);
		$criteria->compare('unit_id',$this->unit_id);
		$criteria->compare('batch',$this->batch,true);
		$criteria->compare('rate',$this->rate);
		$criteria->compare('amount',$this->amount);
		$criteria->compare('item_entry_type',$this->item_entry_type,true);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('updated_date',$this->updated_date,true);
		if(!empty($this->date_from) && empty($this->date_to)) {
			$criteria->addCondition("created_date >= '".date('Y-m-d',strtotime($this->date_from))."'");  // date is database date column field
		} else if(!empty($this->date_to) && empty($this->date_from)) {
					$criteria->addCondition("created_date <= '".date('Y-m-d',strtotime($this->date_to))."'");
		} else if(!empty($this->date_to) && !empty($this->date_from)) {
					$criteria->addCondition("created_date between '".date('Y-m-d',strtotime($this->date_from))."' and  '".date('Y-m-d',strtotime($this->date_to))."'");
		}
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return WarehousebilledStock the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
