<?php
$arrVal='';
if(!empty($project) && !empty($scquotation_id)){
    $sql="";
    $sql="SELECT DISTINCT
        wk.id as wk_id ,wk.worktype_id as labour_id,
        wk.worktype_name,
        wk.template_id,
        COALESCE(pl.labour_rate, wk.worktype_rate) AS labour_rate,COALESCE(pl.id ,wk.id) AS id
    FROM 
        jp_labour_template_worktype wk
    LEFT JOIN 
        (SELECT id,labour_id, template_id, labour_rate -- Subquery without aggregation to get labour_rate for each worktype_id
        FROM jp_project_labour 
        WHERE project_id = " . $project . " AND template_id=" . $template_id . ") pl
    ON 
        pl.template_id = wk.template_id 
        AND pl.labour_id = wk.worktype_id -- Join on both template_id and worktype_id
    WHERE 
        wk.template_id = " . $template_id . ";";
    $arrVal = Yii::app()->db->createCommand($sql)->queryAll();
    } 
    if ($arrVal) {
        ?>
                <div class="row">
                        <div class="col-md-2 element-style " style="text-align:center">
                            <label>Labour Type</label>
                        </div>
                        <div class="col-md-2 element-style" style="text-align:center">
                            <label>No Of Labours</label>
                        </div>
                        <div class="col-md-2 element-style" style="text-align:center">
                            <label>Amount</label>
                        </div>
                    </div>
                <?php
                foreach ($arrVal as $val) {
                    $labour_data = Yii::app()->db->createCommand("SELECT * FROM `jp_labour_worktype` WHERE `type_id` = " . $val['labour_id'])->queryRow();
                ?>
                    <input type="hidden" class="sc_quotation"/>
                                <input type="hidden" name="sc_quotation_id" id="" value="<?php echo $scquotation_id; ?>">
                              
                            </div>
                        <div class="row">
                            <div class="col-md-2 element-style">
                               
                                <input type="hidden" class="labour_type" Name ="Dailyreport[lab_templates][<?php echo $val['id']; ?>][labour_id]" value="<?php echo $val['labour_id']; ?>">
                                <input type="text" class="labour_worktype form-control" name="Dailyreport[lab_templates][<?php echo $val["id"]; ?>][worktype]" value="<?php echo $labour_data['worktype'];?> " readonly/>
                                
                                <div class="error"></div>
                            </div>
                            <div class="col-md-2 element-style">
                              
                                <input type="number" class="labour_count form-control" min="0" name="Dailyreport[lab_templates][<?php echo $val['id']; ?>][count]" value=0  data-rate="<?php echo $val['labour_rate']; ?>" onkeyup="validateInput(this)"/>
                                <span class="error_count"></span>
                            </div>
                            <div class="col-md-2 element-style">
                            
                                <input type="text" class="labour_total_amount form-control" name="Dailyreport[lab_templates][<?php echo $val['id']; ?>][labour_amount]" readonly/>
                                <div class="error"></div>
                            </div>
                            <input type="hidden" name="Dailyreport[lab_templates][<?php echo $val['id']; ?>][default_labour_type]" value='2'>
                        </div>
                <?php
                }
                ?>
               
        <?php
    }?>
