
<div class="container1">
<h4>Equipment Estimation</h4>
    <table cellpadding="10" id="materialEstimationTbl" class="table">
        <?php if ($dataProvider2->getItemCount() > 0) : ?>
            <thead>
                    <tr>
                        <th>SI No</th>
                       
                        <th>Equipment Type</th>
                        <th>Remarks</th>
                        <th>Total Quantity</th>
                        <th>Actual Quantity</th>

                        <th>Balance Quantity</th>
                        <th>unit</th>
                        <th>Rate</th>
                        <th>Total Amount</th>
                        <th>Actual Amount</th>

                        <th>Balance Amount</th>
                        <th>Action</th>
                    </tr>
            </thead>
            <tbody>
                    <?php 
                    $totalAmount = 0;
                    foreach ($dataProvider2->getData() as $index => $data): 
                    $totalAmount += $data->amount;
                    ?>
                        <tr>
                            <td style="width: 50px;"><?php echo $index + 1; ?></td>
                            
                            <td>
                                <span><?php
                                $pms_api_integration_model=ApiSettings::model()->findByPk(1);
                                $pms_api_integration =$pms_api_integration_model->api_integration_settings;
                                if($pms_api_integration==0){
                                    $equipmentModel = Equipments::model()->findByPk($data->equipment);
                                        echo !empty($equipmentModel) ? CHtml::encode($equipmentModel->equipment_name) : "";
                                }
                                else{
                                    $equipmentModel = Equipments::model()->findByPk($data->equipment);
                                        echo !empty($equipmentModel) ? CHtml::encode($equipmentModel->equipment_name) : "";
                                }
                                        
                                    ?>
                                    </span>
                            </td>
                            <td>
                                <span><?php echo CHtml::encode($data->remarks); ?></span>
                            </td>
                            <td>
                                <span><?php echo CHtml::encode($data->quantity); ?></span>
                            </td>
                            <td>
                            <span><?php echo CHtml::encode($data->used_quantity); ?></span>
                            </td>
                            <td>
                            <?php $bal_qauntity =0;
                                $bal_qauntity =$data->quantity-$data->used_quantity;
                            ?>
                            <span><?php echo CHtml::encode($bal_qauntity ) ;?></span>
                            </td>

                            <td>
                                <span>
                                <?php 
                                    $unitModel = Unit::model()->findByPk($data->unit);
                                    echo !empty($unitModel) ? CHtml::encode($unitModel->unit_name) : "Unit not found"; 
                                    ?>
                            </td>
                            <td>
                                <span><?php echo CHtml::encode($data->rate); ?></span>
                            </td>
                            <td>
                                <span><?php echo CHtml::encode($data->amount); ?></span>
                            </td>
                            <td>
                            <span><?php echo CHtml::encode($data->used_amount); ?></span>
                            </td>
                            <td>
                            <?php $bal_amount =0;
                                $bal_amount =$data->amount-$data->used_amount;
                            ?>
                            <span><?php echo CHtml::encode($bal_amount ) ;?></span>
                            </td>

                            <td style="width: 50px;">
                                <a class="fa fa-edit editProject" data-toggle="modal" data-target=".edit" data-id="<?php echo $data->id; ?>" data-estimation="equipment_estimation" data-project_id="<?php echo $data->project_id; ?>"></a>
                                <?php if (empty($data['pms_equipment_id'])): ?>
                                <a class="fa fa-trash deleteProject" data-toggle="modal" data-target=".delete" data-id="<?php echo $data->id; ?>" data-estimation="equipment_estimation"></a>
                                <?php endif; ?>
                            </td>
                            
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            <tfoot>
                <tr style="background-color: #f2f2f2;">
                    <td colspan="8" style="text-align: right;"><strong>Total:</strong></td>
                    <td colspan="1"><strong><?php echo number_format($totalAmount, 2); ?></strong></td>
                    <td colspan="3"></td>
                </tr>
            </tfoot>
        <?php else: ?>
            <tr>
                <td colspan="9">No data found</td>
            </tr>
        <?php endif; ?>
    </table>
</div>
<script>
    $('.editProject').click(function() {
                    $('.loading-overlay').addClass('is-active');
                    var id = $(this).attr('data-id');
                    var estimation = $(this).attr('data-estimation');
                    var project_id = $(this).attr('data-project_id');
                    
                    $.ajax({
                        type: "POST",
                        url: "<?php echo $this->createUrl('materialEstimation/update&layout=1&id=') ?>" + id,
                        data: {
                            estimation: estimation,
                            project_id:project_id,
                        },
                        success: function(response) {
                            $('.loading-overlay').removeClass('is-active');
                            $('#addMaterialEstimation').html(response).slideDown();
                             
                        },
                    });
                });
                
                $('.deleteProject').click(function() {
                    if (confirm("Are you sure you want to delete this material estimation?")) {
                        $('.loading-overlay').addClass('is-active');
                        var id = $(this).attr('data-id');
                        var estimation = $(this).attr('data-estimation');
                        $.ajax({
                            type: "POST",
                            url: "<?php echo $this->createUrl('materialEstimation/delete&layout=1&id=') ?>" + id,
                            data: {
                            estimation: estimation
                            },
                            dataType: 'json',
                            success: function(response) {
                                console.log();
                                $('html, body').animate({ scrollTop: 0 }, 'slow');
                                $("#errMsg").show()
                                    .html('<div class="alert alert-' + response.success + '">' + response.message + '</div>')
                                    .fadeOut(10000);
                                setTimeout(function() {
                                    location.reload(true);
                                }, 1000);
                            },
                            error: function() {
                                $('.loading-overlay').removeClass('is-active');
                                alert('There was an error deleting the material estimation. Please try again.');
                            }
                        });
                    }
                });
</script>
