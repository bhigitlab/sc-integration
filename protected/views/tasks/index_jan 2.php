<?php
/* @var $this TasksController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'Tasks',
);

if (yii::app()->user->role <= 2) {
    $this->menu = array(
        array('label' => 'Manage Tasks', 'url' => array('admin')),
        array('label' => 'Create Tasks', 'url' => array('create')),
    );
}

if (yii::app()->user->role == 6) {
    $this->menu = array(
        array('label' => 'Create Tasks', 'url' => array('create')),
    );
}
?>

<h1>Tasks</h1>

<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'tasks-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        array('class' => 'IndexColumn', 'header' => 'Sl.No.',),
        array('name' => 'tskid', 'htmlOptions' => array('width' => '40px', 'style' => 'font-weight: bold;text-align:center')),
        array(
            'name' => 'project_id',
            'value' => '(isset($data->project->name)?$data->project->name:"")',
            'type' => 'raw',
            'filter' => CHtml::listData(Projects::model()->findAll(
                            array(
                                'select' => array('pid,name'),
                                'order' => 'name',
                                'distinct' => true
                    )), "pid", "name")
        ),
        /*   array(
          'name' => 'work_type',
          'value' => '$data->worktype0->work_type',
          'type' => 'raw',
          'filter' => CHtml::listData(WorkType::model()->findAll(
          array(
          'select' => array('wtid, work_type'),
          'order' => 'work_type',
          'distinct' => true
          )), "wtid", "work_type")
          ), */
        'title',
        array(
            'name' => 'assigned_to',
            'value' => '$data->assignedTo->first_name',
            'type' => 'raw',
            'filter' => CHtml::listData(Users::model()->findAll(
                            array(
                                'select' => array('userid,first_name'),
                                'order' => 'first_name',
                                'distinct' => true
                    )), "userid", "first_name")
        ),
        array(
            'name' => 'report_to',
            'value' => '$data->reportTo->first_name',
            'type' => 'raw',
            'filter' => CHtml::listData(Users::model()->findAll(
                            array(
                                'select' => array('userid,first_name'),
                                'order' => 'first_name',
                                'distinct' => true
                    )), "userid", "first_name")
        ),
        array(
            'name' => 'coordinator',
            'value' => '$data->coordinator0->first_name',
            'type' => 'raw',
            'filter' => CHtml::listData(Users::model()->findAll(
                            array(
                                'select' => array('userid,first_name'),
                                'order' => 'first_name',
                                'distinct' => true
                    )), "userid", "first_name"),
            'htmlOptions' => array( 'style' => 'border-right:1px solid #c2c2c2;')
        ),
        array(
            'header' => 'Billable hrs',
            'value' => 'TimeEntry::model()->find(
                            array(
                                "select" => array("sum(hours) as hours"),
                                "condition" => "billable=\"3\" AND tskid=$data->tskid"
                    ))->hours',
            'type' => 'raw',
            'htmlOptions' => array('width' => '40px', 'style' => 'border:1px solid #c2c2c2;font-weight: bold;text-align:center')
        ),
        array(
            'header' => 'Non-billable hrs',
            'value' => 'TimeEntry::model()->find(
                            array(
                                "select" => array("sum(hours) as hours"),
                                "condition" => "billable=\"4\" AND tskid=$data->tskid"
                    ))->hours',
            'type' => 'raw',
            'htmlOptions' => array('width' => '40px', 'style' => 'border:1px solid #c2c2c2;font-weight: bold;text-align:center')
        ),
        array(
            'header' => 'Total Hours',
            'value' => 'TimeEntry::model()->find(
                            array(
                                "select" => array("sum(hours) as hours"),
                                "condition" => "tskid=$data->tskid"
                    ))->hours',
            'type' => 'raw',
            'htmlOptions' => array('width' => '40px', 'style' => 'border:1px solid #c2c2c2;font-weight: bold;text-align:center')
        ),
        array('name' => 'total_hrs',
               'header' => 'Estimated hrs',
            'htmlOptions' => array('width' => '40px', 'style' => 'border:1px solid #c2c2c2;font-weight: bold;text-align:center')
            ),
        
        array(
            'name' => 'status',
            'value' => '$data->status0->caption',
            'type' => 'raw',
            'filter' => CHtml::listData(Status::model()->findAll(
                            array(
                                'select' => array('sid,caption'),
                                'condition' => 'status_type="task_status"',
                                'order' => 'status_type',
                                'distinct' => true
                    )), "sid", "caption")
        ),
        array(
            'class' => 'CButtonColumn',
            'template' => '{view}{update}',
        ),
    ),
));
?>