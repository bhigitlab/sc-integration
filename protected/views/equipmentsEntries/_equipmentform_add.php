<div class="row dynamic-row">
    <!-- Equipment Dropdown -->
    <div class="form-group col-xs-12 col-md-2">
        <label>Equipment</label>
        <select name="EquipmentsItems[equipments][<?php echo $index; ?>][equipment_id]" class="coms_equipment_id form-control Equipments_add">
            <option value="" selected disabled>Select Equipment</option>
            <?php foreach ($equipments as $id => $equipmentName): ?>
                <option value="<?php echo CHtml::encode($id); ?>">
                    <?php echo CHtml::encode($equipmentName); ?>
                </option>
            <?php endforeach; ?>
        </select>
        <?php if (empty($equipments)): ?>
            <div style="color:red; margin-top:5px;">Equipment Estimation not created for this Project</div>
        <?php endif; ?>
    </div>

    <div class="form-group col-xs-12 col-md-2">
    <label>Unit</label>
    <?php
    $unitData = CHtml::listData(Unit::model()->findAll(), 'id', 'unit_name');
    ?>
    <select name="EquipmentsItems[<?php echo $index; ?>][unit]" class="form-control">
        <option value="">-- Select Unit --</option>
        <?php foreach ($unitData as $id => $unit_name): ?>
            <option value="<?php echo CHtml::encode($id); ?>">
                <?php echo CHtml::encode($unit_name); ?>
            </option>
        <?php endforeach; ?>
    </select>
</div>

<div class="form-group col-xs-12 col-md-2">
    <label>Quantity</label>
    <input type="number" class="equipment_qty form-control" name="EquipmentsItems[<?php echo $index; ?>][quantity]" 
           min="0" value="0" onchange="handleQuantityChange(this, <?php echo $index; ?>)">
    <span style="color:red;" id="rateQuantityError_<?php echo $index; ?>"></span>
    <span class="error_count"></span>
</div>

<div class="form-group col-xs-12 col-md-2">
    <label>Rate</label>
    <input type="text" class="equipment_rate form-control" name="EquipmentsItems[<?php echo $index; ?>][rate]" 
           id="PmsAccWprItemConsumed_item_rate_<?php echo $index; ?>" 
           onchange="handleQuantityChange(this, <?php echo $index; ?>)">
</div>

<div class="form-group col-xs-12 col-md-2">
    <label>Amount</label>
    <input type="text" class="equipment_amount form-control" name="EquipmentsItems[<?php echo $index; ?>][amount]" readonly>
</div>


<!-- <input type="hidden" name="EquipmentsItems[<?php echo $index; ?>][id]" value="">
<input type="hidden" name="EquipmentsItems[<?php echo $index; ?>][default_equipment_type]" value="1"> -->


    <input type="hidden" name="EquipmentsItems[<?php echo $index; ?>][default_equipment_type]" value="1">

    <!-- Add/Remove Buttons -->
    <div class="col-xs-12 col-md-2">
        <button type="button" class="add-btn btn btn-info btn-sm">+</button>
        <button type="button" class="remove-btn btn btn-danger btn-sm">-</button>
    </div>
</div>
