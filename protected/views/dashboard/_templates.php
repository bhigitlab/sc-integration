<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/pdfstyle.css">
<div class="template_sec">
    <?php
    $template = $this->getTemplate($activeTemplate);
    ?>
    <div class="header_sec">
        <?php echo $template['header']; ?>
    </div>
    <div class="footer_sec w-100">
        <?php echo $template['footer']; ?>
    </div>
</div>
<style>
    table tbody tr,table tbody tr td {
        border: 0px  !important;
        font-size:10px;
    }
    h1{
        font-size:2.3rem;    
    }
    td h2{
        padding: 0px !important;
        font-size: 15px;
        font-weight: bold;
    }
    .template_sec{
        box-shadow: 1px 3px 10px 9px #ddd;
        height: 70vh;
        position:relative;
        /* margin-top:-30px; */
    }
    .footer_sec{
        position:absolute;
        bottom:0px;
    }
    .header_sec{
        position:absolute;
        top:0px;
        width:100%;
    }
    table tbody tr td div{
        font-size:11px !important;
    }

    /* ************************** */
    .nav-tabs > li > a {
        /* color: #fff !important; */
        background: #ddd;
        border:none;
    }

    .nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus {
        color: #fff !important;
        background: #72abdd;
    }

</style>
