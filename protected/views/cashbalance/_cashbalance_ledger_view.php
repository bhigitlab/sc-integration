<?php if (filter_input(INPUT_GET, 'export')) { ?>
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/pdfstyle.css">
<?php } ?>
<?php

$date = array_column($data_array, 'payment_date');
array_multisort($date, SORT_ASC, $data_array);

$company_address = Company::model()->find(array('order' => ' id ASC'));

?>
<div class="table-responsive" id="table-wrapper">
    <table class="table ledger_table">
        <thead class="entry-table sticky-thead">
            <tr>
                <th colspan="7">
                    <div>
                        <?php echo isset($company_address->name) ? $company_address->name : ''; ?>
                    </div>
                    <div>
                        <?php echo isset($company_address->address) ? $company_address->address : ''; ?>
                    </div>
                </th>
            </tr>
            <tr>
                <th width="10%">Date</th>
                <th width="15%">Reff</th>
                <th width="15%">Narration</th>
                <th width="20%">Cash Invoice</th>
                <th width="20%">Cash Expense</th>
                <th width="20%">Cash Balance</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $newQuery = "";
            $user = Users::model()->findByPk(Yii::app()->user->id);
            $arrVal = explode(',', $user->company_id);
            foreach ($arrVal as $arr) {
                if ($newQuery)
                    $newQuery .= ' OR';
                $newQuery .= " FIND_IN_SET('" . $arr . "', company_id)";
            }

            $sql = "SELECT * FROM `jp_cashbalance` "
                . " WHERE bank_id IS NULL AND (" . $newQuery . ") ";
            $cash_blance = Yii::app()->db->createCommand($sql)->queryRow();


            $opening_balance = floatval($cash_blance['cashbalance_opening_balance']);
            $balanceledger = $opening_balance;
            ?>
            <tr>
                <td width="10%">
                    <?php echo Yii::app()->controller->changeDateForamt($cash_blance['cashbalance_date']); ?>
                </td>
                <td width="15%"></td>
                <td width="15%">Opening Balance</td>
                <td width="20%" class="text-right">
                    <?php echo Yii::app()->Controller->money_format_inr($opening_balance, 2) ?>
                </td>
                <td width="20%" class="text-right"></td>
                <td width="20%" class="text-right">
                    <?php echo Yii::app()->Controller->money_format_inr($balanceledger, 2) ?>
                </td>
            </tr>
            <?php
            $receipt_sum = 0;
            $expense_sum = 0;

            foreach ($data_array as $key => $value) {
                if ($value['receipt_amount'] != "") { //invoice
                    $balanceledger += $value['receipt_amount'];
                } else { //expense
                    $balanceledger -= $value['expense_amount'];
                }

                $receipt_sum += $value['receipt_amount'];
                $expense_sum += $value['expense_amount'];
                ?>

                <tr>
                    <td width="10%">
                        <?php echo Yii::app()->controller->changeDateForamt($value['payment_date']); ?>
                    </td>
                    <td width="15%">
                        <?php echo $value['bill_exphead'] ?>
                    </td>
                    <td width="15%">
                        <?php echo $value['description'] ?>
                    </td>
                    <td width="20%" class="text-right">
                        <?php echo Yii::app()->Controller->money_format_inr($value['receipt_amount'], 2) ?>
                    </td>
                    <td width="20%" class="text-right">
                        <?php echo Yii::app()->Controller->money_format_inr($value['expense_amount'], 2) ?>
                    </td>
                    <td width="20%" class="text-right">
                        <?php echo Yii::app()->Controller->money_format_inr($balanceledger, 2) ?>
                    </td>
                </tr>
            <?php } ?>
        </tbody>
        <tfoot class="entry-table sticky-tfoot">
            <tr>
                <th colspan="3" width="40%"></th>
                <th width="20%" class="text-right">
                    <?php echo Yii::app()->Controller->money_format_inr(($receipt_sum + $opening_balance), 2) ?>
                </th>
                <th width="20%" class="text-right">
                    <?php echo Yii::app()->Controller->money_format_inr($expense_sum, 2) ?>
                </th>
                <th width="20%"></th>
            </tr>
            <tr>
                <th colspan="5" class="text-right" width="80%">Closing Balance:</th>
                <th class="text-right" width="20%">
                    <?php echo Yii::app()->Controller->money_format_inr($balanceledger, 2) ?>
                </th>
            </tr>
        </tfoot>
    </table>
</div>