<?php
/* @var $this CashbalanceController */
/* @var $model Cashbalance */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cashbalance_type'); ?>
		<?php echo $form->textField($model,'cashbalance_type'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'bank_id'); ?>
		<?php echo $form->textField($model,'bank_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cashbalance_opening_balance'); ?>
		<?php echo $form->textField($model,'cashbalance_opening_balance'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cashbalance_deposit'); ?>
		<?php echo $form->textField($model,'cashbalance_deposit'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cashbalance_withdrawal'); ?>
		<?php echo $form->textField($model,'cashbalance_withdrawal'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cashbalance_closing_balance'); ?>
		<?php echo $form->textField($model,'cashbalance_closing_balance'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created_by'); ?>
		<?php echo $form->textField($model,'created_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created_date'); ?>
		<?php echo $form->textField($model,'created_date'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->