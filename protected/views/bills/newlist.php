<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$this->breadcrumbs = array(
    'performa invoice',
)
    ?>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/tableHeadFixer.js"></script>

<div class="container" id="project">
   
    <div class="expenses-heading header-container">
        <!-- remove addentries class -->
        <?php if (!filter_input(INPUT_GET, 'export')) { ?>
            <h3>Bills</h3>
            <div class="btn-container">
                <?php if ((isset(Yii::app()->user->role) && (in_array('/bills/create', Yii::app()->user->menuauthlist)))) { ?>
                    <a href="index.php?r=bills/create" class="btn btn-info">Add Bill</a>
                <?php }
                if ((isset(Yii::app()->user->role) && (in_array('/bills/addbill', Yii::app()->user->menuauthlist)))) { ?>
                    <a href="index.php?r=bills/Addbill" class="btn btn-info">Add Bill
                        Without PO</a>
                <?php }
                if ((isset(Yii::app()->user->role) && (in_array('/bills/adddailyexpensebill', Yii::app()->user->menuauthlist)))) { ?>
                    <a href="index.php?r=bills/AddDailyexpensebill" class="btn btn-info">
                        Add Daily Expense Bill</a>
                <?php } ?>
                <?= CHtml::link(
                    '<i class="fa fa-file-pdf-o button-icon" aria-hidden="true"></i>',
                    array('bills/admin', 'Bills' => '', 'date_to' => $model->date_to, 'date_from' => $model->date_from, 'bill_number' => $model->bill_number, 'purchase_no' => $model->purchase_no, 'project_id' => $model->project_id, 'vendor_id' => $model->vendor_id, 'export' => 'pdf'),
                    array("class" => "btn btn-info", "title" => "Save as PDF")
                ); ?>
                <?= CHtml::link(
                    '<i class="fa fa-file-excel-o button-icon" aria-hidden="true"></i>',
                    array(
                        'bills/adminlistexcel',
                        'date_to' => $model->date_to,
                        'date_from' => $model->date_from,
                        'bill_number' => $model->bill_number,
                        'purchase_no' => $model->purchase_no,
                        'project_id' => $model->project_id,
                        'vendor_id' => $model->vendor_id
                    ),
                    array("class" => "btn btn-info", "title" => "Save as Excel")
                ); ?>
            </div>
            <?php
        } ?>
    </div>

    <?php
    $pager = '';
    if (!filter_input(INPUT_GET, 'export')) {
        $this->renderPartial('_newsearch', array('model' => $model, 'expensehead' => $expensehead));
        $pager = '{pager}';
    }
    ?>
     
    <div class="alert alert-success info1" style="display:none" role="alert">
  
    </div>
    <?php if (Yii::app()->user->hasFlash('success')): ?>
        <div class="info"
            style="width: 500px;margin-left: 400px;padding: 8px;color: green;font-size: 20px;font-weight: bold;">
            <?php echo Yii::app()->user->getFlash('success'); ?>
        </div>
    <?php endif; ?>


    <?php if (Yii::app()->user->hasFlash('error')): ?>
        <div class="info"
            style="width: 500px;margin-left: 400px;padding: 8px;color: red;font-size: 20px;font-weight: bold;">
            <?php echo Yii::app()->user->getFlash('error'); ?>
        </div>
    <?php endif; ?>
    <div>
        <div id="msg_box">  </div>
        <?php
        $dataProvider->sort->defaultOrder = 'bill_id DESC';
        $this->widget('zii.widgets.CListView', array(
            'dataProvider' => $dataProvider,
            'pager' => array('header' => '', ),
            'summaryCssClass' => 'summary text-right margin-left-auto',
            'itemView' => '_newview',
            'template' => '<div class="clearfix">{summary}{sorter}</div><div id="parent"><table cellpadding="10" class="table total-table" id="fixtable">{items}</table></div>' . $pager,

            'emptyText' => '<table cellpadding="10" class="table"><thead class="entry-table">
            <tr>
                <th>No</th>
                <th>Purchase No</th>
                <th>Company</th>
                <th>Project</th>
                <th>Expense Head</th>
                <th>Vendor</th>            
                <th>Total Amount</th>
                <th>Date</th>
                <th>Status</th>
                <th>Total amount billed</th>
                <th>Balance to be billed</th>
                <th>Billing Status</th>
                <th></th>
             </tr></thead><tr><td colspan="13" class="text-center">No results found</td></tr></table>',
        )); ?>
    </div>
</div>



<style>
    ul.yiiPager .first,
    ul.yiiPager .last {
        display: inline;
    }

    .exp-list {
        padding: 0px;
        position: relative;
        margin-top: 25px;
    }

    .list-view .sorter li {
        margin: 0px;
    }

    #parent {
        max-height: 500px;
    }

    #fixtable thead th,
    #fixtable tfoot th {
        background-color: #eee;
    }

    .table {
        margin-bottom: 0px;
    }

    .highlight {
        background-color:
            #DD1035;
        color: #fff;
    }

    a.button {
        background-color: #6a8ec7;
        display: block;
        padding: 5px;
        color: #fff;
        cursor: pointer;
        float: right;
        border: 1px solid #6a8ec8;
    }

    table.dataTable tbody td {
        padding: 12px 10px;
    }

    .tooltip-hiden {
        width: auto;
    }

    .pager .next>a,
    .pager .next>span {
        float: none;
    }

    .pager .previous>a,
    .pager .previous>span {
        float: none;
    }
</style>
<script>
    $(document).ready(function () {
        $("#parent").css('max-height', $(window).height() - $("#myNavbar").height() - $(".search-form").height() - (2 * $('footer').height()));

        $("#fixtable").tableHeadFixer({ 'left': false, 'foot': true, 'head': true });

        $(".popover-test").popover({
            html: true,
            content: function () {
                return $(this).next('.popover-content').html();
            }
        });
        $('[data-toggle=popover]').on('click', function (e) {
            $('[data-toggle=popover]').not(this).popover('hide');
        });
        $('body').on('hidden.bs.popover', function (e) {
            $(e.target).data("bs.popover").inState.click = false;
        });
        $('body').on('click', function (e) {
            $('[data-toggle=popover]').each(function () {
                // hide any open popovers when the anywhere else in the body is clicked
                if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                    $(this).popover('hide');
                }
            });
        });
        $(document).ajaxComplete(function () {
            $(".popover-test").popover({
                html: true,
                content: function () {
                    return $(this).next('.popover-content').html();
                }
            });
            $("#fixtable").tableHeadFixer({ 'left': false, 'foot': true, 'head': true });
        });


        $(document).on("click", ".delete_purchasebill", function () {
            var id = $(this).attr('id');
            if (id != '') {
                if (confirm('Are you sure you want to delete the bill and its items ?')) {
                    $('.loading-overlay').addClass('is-active');
                    $.ajax({
                        url: "<?php echo Yii::app()->createAbsoluteUrl('purchase/deletebill') ?>",
                        type: 'GET',
                        dataType: "json",
                        data: {
                            bill_id: id
                        },
                        async: true,
                        success: function (data) {
                            if (data.status == 1) {
                                alert("Purchase bill deleted successfully!");
                                setTimeout(function () {
                                    window.location.reload(1);
                                }, 1000);
                            } else if (data.status == 2) {
                                alert("Sorry!..Some problem occured");
                            } else if (data.status == 4) {
                                alert("Cannot Delete ! This record alredy in use.");
                            } else {
                                alert("No bill found");
                            }
                        }
                    })
                } else {
                    return false;
                }
            }
        });
    });
    
    $(document).off("click", ".approve_quant_rate").on("click", ".approve_quant_rate", function(){
           var bill_id = $(this).attr("id"); // Get bill ID

        // Disable button to prevent multiple clicks
        $(this).prop("disabled", true);

        $.ajax({
            url: "<?php echo Yii::app()->createAbsoluteUrl('bills/approveRateQuantity'); ?>",
            type: "GET",
            data: { bill_id: bill_id },
            success: function(response){
               $('.info1').text(' Approved Successfully !').show();
               setTimeout(function(){
                    location.reload();
                }, 2000);
              
            },
            error: function(){
                alert("Error processing request.");
            },
            complete: function() {
                // Re-enable button after request is complete
                $(".approve_quant_rate").prop("disabled", false);
                 
            }
        }); 
    });

</script>