<?php
$base_url = Yii::app()->theme->baseUrl;
$terms = Yii::app()->db->createCommand("SELECT * FROM `jp_terms_conditions` WHERE `template_id` = 1")->queryRow();
$terms_and_conditions = $terms['terms_and_conditions'];
$company_details = Company::model()->findByPk($model->company_id);
$logo = '<img src="' . Yii::app()->request->baseUrl . '/uploads/image/' . $company_details['logo'] . '" alt="" class="pop" modal-src="' . Yii::app()->request->baseUrl . '/uploads/image/' . $company_details['logo'] . '" style="max-height:45px;">';

$company = $company_details['name'];
$company_email = $company_details['email_id'];
$company_address = $company_details['address'];
$company_gst = $company_details['company_gstnum'];
$company_bank = $company_details['bank_details'];
$company_bank = str_replace('.','<br>',$company_bank);
$tax_in_words = Yii::app()->Controller->getDisplaywords($model->tax_amount);
$project = isset($project->name)?$project->name:'';
$client_address = isset($client->address)?$client->address:'';
$client_gst = isset($client->gst_no)?$client->gst_no:'';
$client = isset($client->name)?$client->name:'';
$date = isset($model->date)?date('d-m-Y', strtotime($model->date)):'' ;
$invoice_number = isset($model->inv_no)?$model->inv_no:'';
$payment_stage = PaymentStage::model()->findByPk($model->stage_id)['payment_stage'];
$amount = Controller::money_format_inr(($model->amount + $model->tax_amount),2,1);
$total_without_tax=0;
$app_name = Yii::app()->name;
$item_list= '';
$total_amt_without_tax=0;
$total_amt_after_disc=0;
$total_sgst=$total_cgst=$total_igst=0;
if(!empty($itemmodel)){
    foreach($itemmodel as $i => $value){
        $item_list .= '<tr  class="tr_class">';
        $item_list .= '<td>'.($i+1).'</td>';
        $item_list .= '<td>'.$value->description.'</td>';
        // if($model->type == 'quantity_rate') {

        // $item_list .= '<td>'.$value->quantity.'</td>';
        // $item_list .= '<td>'.$value->unit.'</td>';
        // $item_list .= '<td>'.$value->rate.'</td>';
        //     }
        $item_list .= '<td>'.$value->hsn_code.'</td>';
        $item_list .= '<td>'.number_format($value->sgst,2).'</td>';
        $total_sgst +=$value->sgst_amount;
        $total_cgst +=$value->cgst_amount;
        $total_igst +=$value->igst_amount;
        $total_amt_without_tax +=$value->amount;
        $item_list .= '<td>'.Controller::money_format_inr($value->sgst_amount,2,1).'</td>';
        $item_list .= '<td>'.number_format($value->cgst,2).'</td>';
        $item_list .= '<td>'.Controller::money_format_inr($value->cgst_amount,2,1).'</td>';
        $item_list .= '<td>'.number_format($value->igst,2).'</td>';
        $item_list .= '<td>'.Controller::money_format_inr($value->igst_amount,2,1).'</td>';
        $item_list .= '<td>'.Controller::money_format_inr($value->amount,2,1).'</td>';
        $item_list .= '<td>'.Controller::money_format_inr($value->tax_amount,2,1).'</td>';
        $item_list .= '</tr>';
    }	
}
$additional_charge_disc='';

$addcharges  =  Yii::app()->db->createCommand("SELECT category as discount, ROUND(SUM(amount), 2) as amount FROM jp_invoice_additional_charges WHERE `invoice_id`=" . $model->invoice_id)->queryRow();
$additional_charge= Controller::money_format_inr($addcharges['amount'],2,1);
$additional_charge_disc= strtoupper($addcharges['discount']);
$amount_in_words='';
$total_gst=Controller::money_format_inr($total_sgst+$total_cgst+$total_igst,2,1);

$total_sgst =number_format($total_sgst, 2);
$total_cgst =number_format($total_cgst, 2);
$total_igst =number_format($total_igst, 2);
//echo "<pre>";print_r($model);exit;
$total_amount = Controller::money_format_inr($model->amount, 2,1);
$total_amt_without_tax+=$model->amount;
$total_tax = Controller::money_format_inr($model->tax_amount, 2,1);
$round_off = number_format($model->round_off, 2, '.', '');
$grand_total = Controller::money_format_inr($model->tax_amount+$model->amount+($model->round_off), 2, 1);
$total_amt_after_disc=Controller::money_format_inr($model->tax_amount+$model->amount+($model->round_off)+$additional_charge,2,1);
$total_amt_without_tax=Controller::money_format_inr($model->amount, 2, 1);
$write_off = number_format($writeoff->amount, 2, '.', '');
$balance = Controller::money_format_inr(($model->tax_amount+$model->amount+($model->round_off)) - $writeoff->amount, 2,1);


$balance_with_additional = Controller::money_format_inr((($model->tax_amount+$model->amount+($model->round_off)) - $writeoff->amount)+$additional_charge, 2, 1);
$balance_in_words = Yii::app()->Controller->getDisplaywords(($model->tax_amount+$model->amount+($model->round_off)) - $writeoff->amount);
$total_amount_words=Yii::app()->Controller->getDisplaywords($model->tax_amount+$model->amount+($model->round_off));
$balance_additional_in_words = Yii::app()->Controller->getDisplaywords((($model->tax_amount+$model->amount+($model->round_off)) - $writeoff->amount)+$additional_charge);

$modifiedTemplate = preg_replace('/\[\[ITEMTEMPLATE(.*?)ENDITEMTEMPLATE\]\]/s', '', $pdf_data);
$pattern = '/\[\[ITEMTEMPLATE(.*?)ENDITEMTEMPLATE\]\]/s';
            if (preg_match($pattern, $pdf_data, $matches)) {
                $extractedContent = $matches[1];
            } else {
                $extractedContent = '';
            }
$replacements = array();
$resultItem='';
if(!empty($itemmodel)){
    foreach($itemmodel as $i => $value){
        $replacements = array(
            '{sino}' => $i+1
        );
        if($value->description!=''){
            $replacements = array_merge($replacements, array(
                '{description}' => $value->description
            ));
        }else{
            $replacements = array_merge($replacements, array(
                '{description}' => ''
            ));
        }
        if($value->hsn_code!=''){
            $replacements = array_merge($replacements, array(
                '{hsn_code}' => $value->hsn_code
            ));
        }else{
            $replacements = array_merge($replacements, array(
                '{hsn_code}' => ''
            ));
        }
        if($value->sgst!=''){
            $replacements = array_merge($replacements, array(
                '{sgst}' => number_format($value->sgst,2)
            ));
        }else{
            $replacements = array_merge($replacements, array(
                '{sgst}' => ''
            ));
        }
        if($value->sgst_amount!=''){
            $replacements = array_merge($replacements, array(
                '{sgst_amount}' => Controller::money_format_inr($value->sgst_amount,2,1)
            ));
        }else{
            $replacements = array_merge($replacements, array(
                '{sgst_amount}' => ''
            ));
        }
        if($value->cgst!=''){
            $replacements = array_merge($replacements, array(
                '{cgst}' => number_format($value->cgst,2)
            ));
        }else{
            $replacements = array_merge($replacements, array(
                '{cgst}' => ''
            ));
        }
        if($value->cgst_amount!=''){
            $replacements = array_merge($replacements, array(
                '{cgst_amount}' => Controller::money_format_inr($value->cgst_amount,2,1)
            ));
        }else{
            $replacements = array_merge($replacements, array(
                '{cgst_amount}' => ''
            ));
        }
        if($value->igst!=''){
            $replacements = array_merge($replacements, array(
                '{igst}' => number_format($value->igst,2)
            ));
        }else{
            $replacements = array_merge($replacements, array(
                '{igst}' => ''
            ));
        }
        if($value->igst_amount!=''){
            $replacements = array_merge($replacements, array(
                '{igst_amount}' => Controller::money_format_inr($value->igst_amount,2,1)
            ));
        }else{
            $replacements = array_merge($replacements, array(
                '{igst_amount}' => ''
            ));
        }
        if($value->amount!=''){
            $replacements = array_merge($replacements, array(
                '{amount}' => Controller::money_format_inr($value->amount)
            ));
        }else{
            $replacements = array_merge($replacements, array(
                '{amount}' => ''
            ));
        }
        if($value->tax_amount!=''){
            $replacements = array_merge($replacements, array(
                '{tax_amount}' => Controller::money_format_inr($value->tax_amount)
            ));
        }else{
            $replacements = array_merge($replacements, array(
                '{tax_amount}' => ''
            ));
        } 
        if($value->amount!=''){
            $replacements = array_merge($replacements, array(
                '{amount_with_tax}' => Controller::money_format_inr(($value->tax_amount+$value->amount))
            ));
        }else{
            $replacements = array_merge($replacements, array(
                '{amount_with_tax}' => ''
            ));
        }
        if($model->type == 'quantity_rate') {

            if($value->quantity!=''){
                $replacements = array_merge($replacements, array(
                    '{quantity}' => number_format($value->quantity,2)
                ));
            }else{
                $replacements = array_merge($replacements, array(
                    '{quantity}' => ''
                ));
            }

            if($value->unit!=''){
                $replacements = array_merge($replacements, array(
                    '{unit}' => $value->unit
                ));
            }else{
                $replacements = array_merge($replacements, array(
                    '{unit}' => ''
                ));
            }

            if($value->rate!=''){
                $replacements = array_merge($replacements, array(
                    '{rate}' => number_format($value->rate,2)
                ));
            }else{
                $replacements = array_merge($replacements, array(
                    '{rate}' => ''
                ));
            }
        }
        $html_string = $this->replaceItemlistWithCondition($extractedContent, $replacements);
                    $itemRow = str_replace(array_keys($replacements), array_values($replacements), $html_string);
                    $resultItem .= $itemRow;  
    }
}
$pattern = '/{image::\s*([^|]+)\|\|width:\'(\d+px;)\'}/';

//$path = Yii::app()->request->baseUrl . '/uploads/company_assets/media/';
$path = Yii::app()->basePath . '/../uploads/company_assets/media/';
$modifiedPdfData = preg_replace_callback($pattern, function($matches) use ($path) {
    $image_name = $matches[1];
    $width_value = $matches[2];

    $replacement_image = "<img src='" . $path . "$image_name' alt='$image_name' class='dynamic_media_image' style='width:$width_value;'>";
    return $replacement_image;
}, $modifiedTemplate);
$pdf_data = str_replace(
    array('{base_url}','{terms_and_conditions}','{logo}','{item_list}','{dynamic_item_list}','{company}','{project}','{client}','{client_address}','{client_gst}','{date}','{invoice_number}','{payment_stage}','{amount}','{total_amount}','{total_tax}','{round_off}','{grand_total}','{total_amount_words}','{write_off}','{balance}','{balance_with_additional}','{app_name}','{company_email}','{company_address}','{company_gst}','{company_bank}','{balance_in_words}','{balance_additional_in_words}','{tax_in_words}','{total_sgst}','{total_cgst}','{total_igst}','{additional_charge}','{total_gst_amt}','{total_amt_without_tax}','{discount_in_add_charge}','{discount_amount}','{total_amt_after_disc}'
   ),
    array($base_url, $terms_and_conditions,$logo,$item_list,$resultItem,$company,$project,$client,$client_address,$client_gst,$date,$invoice_number,$payment_stage,$amount,$total_amount,$total_tax,$round_off,$grand_total,$total_amount_words,$write_off,$balance,$balance_with_additional,$app_name,$company_email,$company_address,$company_gst,$company_bank,$balance_in_words,$balance_additional_in_words,$tax_in_words,$total_sgst,$total_cgst,$total_igst,$additional_charge,$total_gst,$total_amt_without_tax,$additional_charge_disc,$additional_charge,$total_amt_after_disc
), $modifiedPdfData);

$str = html_entity_decode($pdf_data, ENT_QUOTES, 'UTF-8');
eval('?>'.$str.'');
?>