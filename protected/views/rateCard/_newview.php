<?php
/* @var $this ProjectsController */
/* @var $data Projects */
?>
<?php
if ($index == 0) {
    ?>
    <thead>
    <tr> 
        <th>SI No</th>
        <th>Worktype</th>
        <th>Rate</th>
        <?php 
        if(isset(Yii::app()->user->role) && (in_array('/rateCard/update', Yii::app()->user->menuauthlist))){
        ?>
        <th>Action</th>
        <?php } ?>
    </tr>   
    </thead>
<?php } ?>
<tr> 
    <td style="width: 50px;"><?php echo $index+1; ?></td>
        <td>
            <span><?php echo isset($data->worktype0)?$data->worktype0->work_type:''; ?></span>            
        </td>        
        <td>
        <span><?php echo isset($data->rate)?$data->rate:''; ?></span>
        </td>
        <?php 
            if(isset(Yii::app()->user->role) && (in_array('/rateCard/update', Yii::app()->user->menuauthlist))){
            ?>
        <td style="width: 50px;">
           <a class="fa fa-edit editratecard"  data-id="<?php echo $data->id; ?>"></a>
        </td>
    <?php } ?>
</tr> 