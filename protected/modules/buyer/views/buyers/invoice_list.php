<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$this->breadcrumbs = array(
    'performa invoice',
)
?>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<div class="container" id="project">
    <div class="clearfix">
        <div class="add-btn pull-right">
            <?php
            if ((isset(Yii::app()->user->role) && (in_array('/buyer/buyers/createInvoice', Yii::app()->user->menuauthlist)))) {
            ?>
                <a href="index.php?r=/buyer/buyers/addInvoice" class="button">Add Invoice</a>
            <?php } ?>
            &nbsp;

        </div>
        <h2>Buyer Invoice</h2>
    </div>

    <?php

    // $this->renderPartial('_newsearch', array('model' => $model)) 
    ?>





    <?php if (Yii::app()->user->hasFlash('success')) : ?>
        <div class="info" style="width: 500px;margin-left: 400px;padding: 8px;color: green;font-size: 20px;font-weight: bold;">
            <?php echo Yii::app()->user->getFlash('success'); ?>
        </div>
    <?php endif; ?>


    <?php if (Yii::app()->user->hasFlash('error')) : ?>
        <div class="info" style="width: 500px;margin-left: 400px;padding: 8px;color: red;font-size: 20px;font-weight: bold;">
            <?php echo Yii::app()->user->getFlash('error'); ?>
        </div>
    <?php endif; ?>

    <div class="">
        <div id="msg_box"></div>
        <?php
        $dataProvider->sort->defaultOrder = 'buyer_invoice_id DESC';
        $this->widget('zii.widgets.CListView', array(
            'dataProvider' => $dataProvider,
            'itemView' => '_newview',
            'template' => '<div class=""><table cellpadding="10" class="table" id="salestable">{items}</table></div>',
            'emptyText' => '<table cellpadding="10" class="table"><thead><tr>
            <th>Invoice No</th>
            <th>Client</th>
            <th>Date</th>
            <th>Sub Total</th>
            <th>Total Amount </th>
            <th></th>
        </tr></thead><tr><td colspan="6" class="text-center">No results found</td></tr></table>',
        )); ?>
    </div>
</div>
<?php
Yii::app()->clientScript->registerScript('myjavascript', '
             $(document).ready(function(){
                $("#salestable").dataTable( {
                    "scrollY": "300px",
                    "scrollCollapse": true,
                    "paging": false,
                     "columnDefs"        : [       
                        { 
                            "searchable"    : false, 
                            "targets"       : [0] 
                        },
                        { "bSortable": false, "aTargets": [-1]  }
                    ],
                } );
                
            });

            ');
?>

<style>
    a.button {
        background-color: #6a8ec7;
        display: block;
        padding: 5px;
        color: #fff;
        cursor: pointer;
        float: right;
        border: 1px solid #6a8ec8;
    }

    table.dataTable tbody td {
        padding: 12px 10px;
    }

    .add-btn a {
        margin-right: 4px;
    }

    #loader {
        display: none;
    }
</style>
<script>
    $(document).ready(function() {
        $(".popover-test").popover({
            html: true,
            content: function() {
                return $(this).next('.popover-content').html();
            }
        });
        $('[data-toggle=popover]').on('click', function(e) {
            $('[data-toggle=popover]').not(this).popover('hide');
        });
        $('body').on('hidden.bs.popover', function(e) {
            $(e.target).data("bs.popover").inState.click = false;
        });
        $('body').on('click', function(e) {
            $('[data-toggle=popover]').each(function() {
                // hide any open popovers when the anywhere else in the body is clicked
                if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                    $(this).popover('hide');
                }
            });
        });
        $(document).ajaxComplete(function() {
            $(".popover-test").popover({
                html: true,
                content: function() {
                    return $(this).next('.popover-content').html();
                }
            });

        });

    });

    $(document).on('click', '.update_status', function(e) {
        e.preventDefault();
        var element = $(this);
        var invoice_id = $(this).attr('id');
        $('.loader_' + invoice_id).css('display', 'inline-block');
        $.ajax({
            url: '<?php echo  Yii::app()->createAbsoluteUrl('invoice/updateinvoicestatus'); ?>',
            type: 'GET',
            dataType: 'json',
            data: {
                invoice_id: invoice_id
            },
            success: function(response) {
                $('.loader_' + invoice_id).hide();
                if (response.response == 'success') {
                    $(".invoice_status_" + invoice_id).html('Saved');
                    $("#msg_box").html('<div class="alert alert-success alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><strong> Success!</strong>  &nbsp;' + response.msg + '</div>');
                } else {
                    $("#msg_box").html('<div class="alert alert-warning alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><strong> Sorry!</strong>  &nbsp;' + response.msg + '</div>');
                }
            }
        });
    });
</script>