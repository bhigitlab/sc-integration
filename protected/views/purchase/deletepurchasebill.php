<?php
$tblpx = Yii::app()->db->tablePrefix;
if (Yii::app()->user->role == 1) {
    $bill_status = Yii::app()->db->createCommand("SELECT *  FROM {$tblpx}purchase LEFT JOIN {$tblpx}status ON 
    {$tblpx}purchase.purchase_billing_status = {$tblpx}status.sid WHERE p_id = " . $model->p_id . " AND 
    ({$tblpx}status.caption='Pending to be Billed' OR {$tblpx}status.caption='Fully Billed' OR {$tblpx}status.caption='Partially Billed') AND 
    {$tblpx}status.status_type='purchase_bill_status'")->queryRow();
} else {
    $bill_status = Yii::app()->db->createCommand("SELECT *  FROM {$tblpx}purchase LEFT JOIN {$tblpx}status ON 
{$tblpx}purchase.purchase_billing_status = {$tblpx}status.sid WHERE p_id = " . $model->p_id . " AND 
{$tblpx}status.caption='Pending to be Billed'  AND 
{$tblpx}status.status_type='purchase_bill_status'")->queryRow();
}


?>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/message/css/jquery.message.css">
<script type="text/javascript"
    src="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/message/js/jquery.message.js"></script>
<script>
    $(function () {
        $("#datepicker").datepicker({
            dateFormat: 'dd-mm-yy'
        }).datepicker("setDate", new Date());

    });
</script>
<?php
$company = Company::model()->findBypk($model->company_id);
?>
<div class="container">
    <div class="loading-overlay">
        <span class="fa fa-spinner fa-3x fa-spin"></span>
    </div>
    <div class="sub-heading mb-10">
        <h3 class="pull-left">Bills</h3>
        <?php
        if (!empty($bill_model)) { ?>
            <a href="#" id=<?php echo $model->p_id; ?> class="button btn btn-danger delete_purchasebill"
                style="text-transform: capitalize;">Delete Bill</a>
        <?php } ?>
    </div>

    <form id="pdfvals1" method="post" action="<?php echo $this->createAbsoluteUrl('purchase/addpurchase'); ?>">
        <input type="hidden" name="purchaseview">

        <div class="page_filter">
            <div id="errormessage"></div>
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label>COMPANY :</label>

                        <div class="po_company <?= !empty($admin_approval_data) ? 'user_company_edit_class' : '' ?>"
                            id="<?= $model->p_id ?>">
                            <?php echo ucfirst($company['name']); ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>GST NO</label>
                        <div class="gstnum">
                            <?php echo ucfirst($company['company_gstnum']); ?>
                        </div>
                        <input type="hidden" class="gstnum" name="txtCompanyGst"
                            value="<?php echo ucfirst($company['company_gstnum']); ?>" />
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>PROJECT : </label>
                        <div>
                            <?php echo isset($project->name) ? $project->name : ''; ?>
                        </div>
                        <input type="hidden" name="project" id="project"
                            value="<?php echo isset($project->pid) ? $project->pid : ''; ?>">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>EXPENSE HEAD : </label>
                        <div>
                            <?php echo (isset($expense_head->type_name)) ? $expense_head->type_name : ''; ?>
                        </div>
                        <input type="hidden" name="expensehead" id="expensehead"
                            value="<?php echo (isset($expense_head->type_id)) ? $expense_head->type_id : ''; ?>">
                    </div>
                </div>
            </div>
            <div class='row'>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>VENDOR : </label>
                        <div>
                            <?php echo (isset($vendor->name)) ? $vendor->name : ''; ?>
                        </div>
                        <input type="hidden" name="vendor" id="vendor"
                            value="<?php echo (isset($vendor->vendor_id)) ? $vendor->vendor_id : ''; ?>">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>DATE : </label>
                        <div>
                            <?php echo date('d-m-Y', strtotime($model->purchase_date)); ?>
                            <input type="hidden" name="date"
                                value="<?php echo date('d-m-Y', strtotime($model->purchase_date)); ?>" readonly>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>PURCHASE NO : </label>
                        <div>
                            <?php echo isset($model->purchase_no) ? $model->purchase_no : ''; ?>
                            <input type="hidden" name="purchaseno"
                                value="<?php echo isset($model->purchase_no) ? $model->purchase_no : ''; ?>" readonly>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="bill_section">


            <br>
            <table border="1" class="table">
                <thead>
                    <tr>
                        <th>Sl No</th>
                        <th>Bill Number</th>
                        <th>Company</th>
                        <th>Project</th>
                        <th>Vendor</th>
                        <th>Bill Date</th>
                        <th>Created Date</th>
                        <th>Amount</th>
                        <th>Total Amount</th>
                    </tr>
                </thead>
                <tbody class="addrow">
                    <?php
                    if (!empty($bill_model)) {
                        foreach ($bill_model as $i => $data) {
                            ?>
                            <tr>
                                <td>
                                    <?php echo $i + 1; ?>
                                </td>
                                <td>
                                    <?php echo CHtml::link($data["bill_number"], 'index.php?r=bills/view&id=' . $data['bill_id'], array('class' => 'link', 'target' => '_blank')); ?>
                                </td>
                                <td>
                                    <?php
                                    $company = Company::model()->findByPk($data["company_id"]);
                                    echo $company['name'];
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    if ($data["purchase_id"] != NULL) {
                                        $tblpx = Yii::app()->db->tablePrefix;
                                        $purchase_data = Yii::app()->db->createCommand("SELECT {$tblpx}projects.name as project_name,{$tblpx}vendors.name as vendor_name FROM {$tblpx}purchase LEFT JOIN {$tblpx}projects ON {$tblpx}purchase.project_id = {$tblpx}projects.pid LEFT JOIN {$tblpx}vendors ON {$tblpx}purchase.vendor_id={$tblpx}vendors.vendor_id  WHERE {$tblpx}purchase.p_id = " . $data["purchase_id"] . "")->queryRow();
                                        echo $purchase_data['project_name'];
                                    }
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    if ($data["purchase_id"] != NULL) {
                                        $tblpx = Yii::app()->db->tablePrefix;
                                        $purchase_data = Yii::app()->db->createCommand("SELECT {$tblpx}projects.name as project_name,{$tblpx}vendors.name as vendor_name FROM {$tblpx}purchase LEFT JOIN {$tblpx}projects ON {$tblpx}purchase.project_id = {$tblpx}projects.pid LEFT JOIN {$tblpx}vendors ON {$tblpx}purchase.vendor_id={$tblpx}vendors.vendor_id  WHERE {$tblpx}purchase.p_id = " . $data["purchase_id"] . "")->queryRow();
                                        echo $purchase_data['vendor_name'];
                                    }
                                    ?>
                                </td>
                                <td>
                                    <?php echo date("d-m-Y", strtotime($data["bill_date"])); ?>
                                </td>
                                <td>
                                    <?php echo date("d-m-Y", strtotime($data["created_date"])); ?>
                                </td>
                                <td class="text-right">
                                    <?php echo ($data["bill_amount"] != '') ? Controller::money_format_inr($data["bill_amount"], 2, 1) : '0.00'; ?>
                                </td>
                                <td class="text-right">
                                    <?php echo ($data["bill_totalamount"] != '') ? Controller::money_format_inr($data["bill_totalamount"], 2, 1) : '0.00'; ?>
                                </td>
                            </tr>
                            <?php
                        }
                    }
                    ?>
                </tbody>
            </table>
        </div>

        <br><br>
    </form>
</div>
<style>
    .error_message {
        color: red;
    }

    a.pdf_excel {
        background-color: #6a8ec7;
        display: inline-block;
        padding: 8px;
        color: #fff;
        border: 1px solid #6a8ec8;
    }

    .invoicemaindiv th,
    .invoicemaindiv td {
        padding: 10px;
    }

    .page-body h3 {
        margin: 4px 0px;
        color: inherit;
        text-align: left;
    }

    .panel {
        border: 1px solid #ddd;
        box-shadow: 0px 0px 0px 0px;
    }

    .panel-heading {
        background-color: #eee;
        height: 40px;
    }
</style>


</div>

<style>
    .edit_button {
        pointer-events: none;
    }
</style>
<script>
    $('.delete_purchasebill').click(function () {
        // alert("chggh");return;
        var id = $(this).attr('id');
        if (id != '') {
            if (confirm('Are you sure you want to delete this item from bill?')) {
                $('.loading-overlay').addClass('is-active');
                $.ajax({
                    url: "<?php echo Yii::app()->createAbsoluteUrl('purchase/deletebill') ?>",
                    type: 'GET',
                    dataType: "json",
                    data: {
                        id: id
                    },
                    async: true,
                    success: function (data) {
                        if (data.status == 1) {
                            $().toastmessage('showSuccessToast', "Purchase bill deleted successfully!");
                            $('.edit').removeClass('edit_button');
                            $('.bill_section').css('display', 'none');
                        } else if (data.status == 2) {
                            $().toastmessage('showErrorToast', "Sorry!..Some problem occured");
                        } else if (data.status == 4) {
                            $().toastmessage('showErrorToast', "Cannot Delete ! This record alredy in use.");
                        } else {
                            $().toastmessage('showErrorToast', "No bill found");
                        }
                    }
                })
            } else {
                return false;
            }
        }
    });
    $(document).ajaxComplete(function () {
        $('.loading-overlay').removeClass('is-active');
        $('#loading').hide();
    });
</script>