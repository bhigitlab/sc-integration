<?php
/* @var $this ExpensesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'Company Expense Type',
);
$page = Yii::app()->request->getParam('Companyexpensetype_page');
if ($page != '') {
    Yii::app()->user->setReturnUrl($page);
} else {
    Yii::app()->user->setReturnUrl(0);
}
?>
<head>
    <!-- Other head elements -->
    <meta name="csrf-token" content="<?= Yii::app()->request->csrfToken ?>">
</head>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<div class="container" id="expensetype">
    <div class="loading-overlay">
        <span class="fa fa-spinner fa-3x fa-spin"></span>
    </div>
    <div class="clearfix">
        <div class="add-btn pull-right">
            <?php
            if (isset(Yii::app()->user->role) && (in_array('/companyexpensetype/create', Yii::app()->user->menuauthlist))) {
            ?>
                <a class="button addexpensedaily" id="addexpensedaily">Add Daily Expense Type</a>
                <a class="button deleteexpensedaily" id="deleteButton" style="display:none">Delete Daily Expense Type</a>
            <?php } ?>
        </div>
        <h2>Daily Expense Type</h2>
    </div>
    <div id="addexpensedailybox" style="display:none;"></div>
    <div id="errMsg"></div>

    <?php //$this->renderPartial('_search', array('model' => $model)) 
    ?>



    <div class="row">
        <div class="col-md-12">
            <?php
            $this->widget('zii.widgets.CListView', array(
                'dataProvider' => $dataProvider,
                'itemView' => '_view',
                'template' => '<div>{summary}{sorter}</div><div class="container1"><table cellpadding="10" id="clientable" class="table">{items}</table></div>{pager}',
                'ajaxUpdate' => false,
            ));
            ?>
        </div>

        <!-- Add Expense type Popup -->
        <!--        <div id="addCompanyExpensetype" class="modal fade" role="dialog">
            <div class="modal-dialog modal-lg">

            </div>
        </div>-->


        <!-- Edit Expense type Popup -->

        <!--        <div class="modal fade edit" role="dialog">
            <div class="modal-dialog modal-lg">


            </div>
        </div>-->
        <?php
        Yii::app()->clientScript->registerScript('myjavascript', '
     $(document).ready(function(){
    
       $("#dailytble").dataTable( {
       
            "scrollY": "300px",
            "scrollCollapse": true,
            "paging": false,
            "columnDefs"        : [       
                { 
                    "searchable"    : false, 
                    "targets"       : [0] 
                },{ "bSortable": false, "aTargets": [-1]  }
            ],
                       
	} );
	
	
   
        
	});
        
    ');
        ?>
        <script>
            function closeaction() {
                $('#addexpensedaily').slideUp(500);
            }
            $(document).ready(function() {

                $('.addexpensedaily').click(function() {
                    //alert('hi');
                    $('.loading-overlay').addClass('is-active');
                    var id = $(this).attr('data-id');
                    $.ajax({
                        type: "POST",
                        //dataType: "JSON",
                        url: "<?php echo $this->createUrl('companyexpensetype/create&layout=1') ?>",
                        success: function(response) {
                            $('#addexpensedailybox').html(response).slideDown();
                        },
                    });
                });



                $('.editcmpexpdaily').click(function() {

                    $('.loading-overlay').addClass('is-active');
                    var id = $(this).attr('data-id');
                    $.ajax({
                        type: "POST",
                        //dataType: "JSON",
                        url: "<?php echo $this->createUrl('companyexpensetype/update&layout=1&id=') ?>" + id,
                        success: function(response) {
                            $('#addexpensedailybox').html(response).slideDown();
                            $('.loading-overlay').removeClass('is-active');
                        },
                    });
                });
 $(document).ready(function() {
    // Bind change event to checkboxes
    $('input[type="checkbox"]').change(function() {
        // Check if any checkbox is checked
        var isChecked = $('input[type="checkbox"]:checked').length > 0;
        console.log('Checkbox state:', isChecked); // Output checkbox state to console for debugging

        // Toggle visibility of delete button based on checkbox status
        $('#deleteButton').toggle(isChecked);
        console.log('Delete button visibility:', $('#deleteButton').is(':visible')); // Output delete button visibility to console for debugging
    });
    $('#selectAll').click(function(){
        $('.recordCheckbox').prop('checked', $(this).prop('checked'));
    });
    $(".deletecmpexpdaily").click(function(){
        var data_id = $(this).attr('data-id');
        var confirmation  = confirm('Are you sure you want to delete?');
        if( confirmation ){
               var id = $(this).attr('data-id');
                    $.ajax({
                        type: "POST",
                        url: "<?php echo $this->createUrl('companyexpensetype/delete&layout=1&id=') ?>" + id,
                        dataType: 'json',
                        success: function(response) {
                            console.log(response.msg);
                            $('html, body').animate({ scrollTop: 0 }, 'slow');
                            $("#errMsg").show()
                                .html('<div class="alert alert-'+response.success+'">'+response.message+'</div>')
                                .fadeOut(10000);
                            setTimeout(function () {                        
                                location.reload(true);
                            }, 3000);
                                
                           
                        },
                    });
        }
        
    });
});

                
            });
            $(document).ajaxComplete(function() {
                $('.loading-overlay').removeClass('is-active');
                $('#loading').hide();
            });
            $('#deleteButton').click(function(){
               var ConfirmDelete = confirm("Are you sure you want to delete selected records");
               var checkedIds = [];
               var csrfToken = $('meta[name="csrf-token"]').attr("content");

               if(ConfirmDelete){
                $('input[type="checkbox"].recordCheckbox:checked').not('#selectAll').each(function() {
                    checkedIds.push($(this).val());
                });
                if(checkedIds.length > 0){
                   $.ajax({
                    type:"POST",
                    url:"<?php echo $this->createUrl('companyexpensetype/bulkdelete') ?>",
                    data:{ids:checkedIds, YII_CSRF_TOKEN: csrfToken},
                    dataType:"json",
                    success : function(response){
                        console.log(response.msg);
                        $('html, body').animate({ scrollTop: 0 }, 'slow');
                        $("#errMsg").show()
                        .html('<div class="alert alert-'+response.success+'">'+response.message+'</div>')
                        .fadeOut(10000);
                        setTimeout(function () {                        
                        location.reload(true);
                        }, 3000);
                    }
                   });
                }
               }

            });
       

        </script>
        <style>
            .page-body h3 {
                margin: 4px 0px;
                color: inherit;
                text-align: left;
            }

            .panel {
                border: 1px solid #ddd;
            }

            .panel-heading {
                background-color: #eee;
                height: 40px;
            }

            div.form label {
                display: inherit;
            }

            input[type="radio"] {
                margin: 4px 4px 0px;
            }
        </style>


    </div>
</div>