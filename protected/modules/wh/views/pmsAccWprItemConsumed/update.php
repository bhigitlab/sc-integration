<?php
/* @var $this PmsAccWprItemConsumedController */
/* @var $model PmsAccWprItemConsumed */
$this->breadcrumbs=array(
    'Consumption Request'=>array('index'),
    $model->id=>array('view','id'=>$model->id),
    'Update',
);
?>

<div class="panel panel-gray">
    
    <?php echo $this->renderPartial('_form', array('model' => $model,'rates' => $rates)); ?>
</div>