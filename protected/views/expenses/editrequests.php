<?php
/* @var $this ExpensesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'Transactions',
);
?>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/tableHeadFixer.js"></script>

<div class="container" id="expense">
    <div class="loading-overlay">
        <span class="fa fa-spinner fa-3x fa-spin"></span>
    </div>
    <div class="clearfix secdiv">
        <h2 class="pull-left">Pending Edit Request</h2>
    </div>
    <div class="modal fade edit" role="dialog">
        <div class="modal-dialog modal-lg">

        </div>
    </div>
    <div id="errormessage"></div>
    <div id="newlist">
        <?php
        $this->widget('zii.widgets.CListView', array(
            'dataProvider' => $dataProvider,
            'itemView' => '_neweditrequest', 'template' => '<div class=""><table cellpadding="10" class="table" id="clientab">{items}</table></div>',
            'ajaxUpdate' => false,
        ));
        ?>
    </div>
</div>

<script>
    $(document).ready(function() {
        $("#fixTable").tableHeadFixer({
            'left': 1,
            'foot': true,
            'head': true
        });
        //$("#fixTable").tableHeadFixer({'foot' : true, 'head' : true});
        $(document).on('click', '.approve_request', function(e) {
            var id = $(this).attr("id");
            var answer = confirm("Are you sure you want to approve this request?");
            if (answer) {
                updateRequestAction(id, 1);
            } else {
                return false;
            }
        });
        $(document).on('click', '.cancel_request', function(e) {
            var id = $(this).attr("id");
            var answer = confirm("Are you sure you want to cancel this request?");
            if (answer) {
                updateRequestAction(id, 2);
            } else {
                return false;
            }
        });
    });
    $(document).ready(function() {
        $(".popover-test").popover({
            html: true,
            content: function() {
                //return $('#popover-content').html();
                return $(this).next('.popover-content').html();
            }
        });
        $('[data-toggle=popover]').on('click', function(e) {
            $('[data-toggle=popover]').not(this).popover('hide');
        });
        $('body').on('hidden.bs.popover', function(e) {
            $(e.target).data("bs.popover").inState.click = false;
        });
        $('body').on('click', function(e) {
            $('[data-toggle=popover]').each(function() {
                // hide any open popovers when the anywhere else in the body is clicked
                if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                    $(this).popover('hide');
                }
            });
        });
    });

    function updateRequestAction(id, status) {
        $('.loading-overlay').removeClass('is-active');
        $.ajax({
            type: "GET",
            data: {
                id: id,
                status: status
            },
            url: "<?php echo Yii::app()->createUrl("expenses/updaterequestaction") ?>",
            success: function(data) {
                if (data == 2) {
                    $("#errormessage").show()
                        .html('<div class="alert alert-warning">Failed! Please try again.</div>')
                        .fadeOut(10000);
                    return false;
                } else {
                    $("#newlist").html(data);
                    if (status == 1) {
                        $("#errormessage").show()
                            .html('<div class="alert alert-success">Success! Request approved successfully.</div>')
                            .fadeOut(10000);
                    } else if (status == 2) {
                        $("#errormessage").show()
                            .html('<div class="alert alert-success">Success! Request cancelled successfully.</div>')
                            .fadeOut(10000);
                    }
                }
            }
        });
    }
    $(document).ajaxComplete(function() {
        $('.loading-overlay').removeClass('is-active');
        $('#loading').hide();
    });
</script>