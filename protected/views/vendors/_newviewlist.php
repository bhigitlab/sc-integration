<?php
$tblpx = Yii::app()->db->tablePrefix;
if ($index == 0) {
    ?>
    <thead class="entry-table">
        <tr>
        <?php
            if ((isset(Yii::app()->user->role) && ((in_array('/vendors/paymentedit', Yii::app()->user->menuauthlist)) || (in_array('/vendors/paymentdelete', Yii::app()->user->menuauthlist))))) {
                ?>
                <th></th>
            <?php } ?>
            <th>#ID</th>
            <th>Company</th>
            <th>Project</th>
            <th>Vendor</th>
            <th>Purchase Number</th>
            <th>Description</th>
            <th>Transaction Type</th>
            <th>Bank</th>
            <th>Cheque No</th>
            <th>Amount</th>
            <th>Tax</th>
            <th>TDS Amount</th>
            <th>Paid Amount</th>
            

        </tr>
    </thead>
<?php } ?>
<?php
$unreconcilbgcolor ="";
if(($data["payment_type"]==88 )&& ($data["reconciliation_status"] == 0)) { 
    $unreconcilbgcolor =" background:#5bc0de3b;";
}
$class = '';
$date = strtotime("+2 days", strtotime($data["date"]));
$check_date = date("Y-m-d", $date);
if (date('Y-m-d') <= $check_date) {
    $class = 'row-daybook';
} else {
    $class = '';
}
$expenseId = $data["daily_v_id"];
$deleteconfirm = Deletepending::model()->find(array("condition" => "deletepending_parentid = " . $expenseId . " AND deletepending_status = 0 AND deletepending_table = '{$tblpx}dailyvendors'"));
$updateconfirm = Editrequest::model()->find(array("condition" => "parent_id = " . $expenseId . " AND editrequest_status = 0 AND editrequest_table = '{$tblpx}dailyvendors'"));
$confirmcount = isset($deleteconfirm) ? count($deleteconfirm) : 0;
$updatecount = isset($updateconfirm) ? count($updateconfirm) : 0;
if ($confirmcount > 0 || $updatecount > 0) {
    $deleteclass = "deleteclass";
    $deletetitle = "Waiting for delete confirmation from admin.";
} else {
    $deleteclass = "";
    $deletetitle = "";
}
$bgcolor = '';
$approval_status = $data["approval_status"];
$id = 'daily_v_id';
$tbl = $this->tableNameAcc('pre_dailyvendors', 0);
$modelName = 'PreDailyvendors';

$final = Yii::app()->controller->actiongetFinalData($data, $approval_status, $id, $tbl, $modelName);

$data = $final['data'];

$approval_status = $final['approval_status'];
?>

<tr id="<?php echo $data['daily_v_id'] ?>" class="<?php echo $deleteclass . $approval_status . ' ' . (($approval_status == 0) ? 'bg_pending' : ''); ?>" 
    title="<?php echo $deletetitle; ?>" 
    data-status="<?php echo $data["approval_status"] ?>" 
    data-id="<?php echo ($data["approval_status"] == 0) ? $data['ref_id'] : $data['daily_v_id']; ?>" style="<?php echo $unreconcilbgcolor ?>">
    <?php
    if ((isset(Yii::app()->user->role) && ((in_array('/vendors/paymentedit', Yii::app()->user->menuauthlist)) || (in_array('/vendors/paymentdelete', Yii::app()->user->menuauthlist))))) {
        ?>
        
        <td data-status="<?php echo $data['reconciliation_status']; ?>">
            <?php
            if ($updatecount > 0) {
                $requestmessage = "Already an update request is pending";
                ?>
                <span class="icon icon-options-vertical popover-test" data-toggle="popover" data-placement="right" type="button" data-html="true" style="cursor: pointer;"></span>
                <div class="popover-content hide">
                    <ul class="tooltip-hiden">
                        <li><span id="<?php echo $index; ?>" class="pending-icon fa fa-question" title="<?php echo $requestmessage; ?>"></span></li>
                    </ul>
                </div>
                <?php
            } else {
                $payDate = date("Y-m-d", strtotime($data["date"]));
                $company = Company::model()->findByPk($data["company_id"]);
                $updateDays = ($company->company_updateduration) - 1;
                $editDate = date('Y-m-d ', strtotime($payDate . ' + ' . $updateDays . ' days'));
                $addedDate = date("Y-m-d 23:59:59", strtotime($data["date"]));
                $currentDate = date("Y-m-d");
                if (Yii::app()->user->role == 1) {
                    ?>
                    <span class="icon icon-options-vertical popover-test" data-toggle="popover" data-placement="right" type="button" data-html="true" style="cursor: pointer;"></span>
                    <div class="popover-content hide">
                        <ul class="tooltip-hiden">
                            <?php
                            if ((in_array('/vendors/paymentedit', Yii::app()->user->menuauthlist))) {
                                ?>
                                <li><button id="<?php echo $index; ?>" class="btn btn-xs btn-default row-daybook margin-right-5">Edit</button></li>
                                <?php
                            }
                            if ((in_array('/vendors/paymentdelete', Yii::app()->user->menuauthlist))) {
                                ?>
                                <li><button id="<?php echo $index; ?>" class="btn btn-xs btn-default delete_row margin-right-5">Delete</button></li>
                            <?php } ?>
                        </ul>
                    </div>
                    <?php
                } else {
                    if (isset($company->company_updateduration) && !empty($company->company_updateduration)) {
                        if ($editDate >= $currentDate) {
                            ?>
                            <span class="icon icon-options-vertical popover-test" data-toggle="popover" data-placement="right" type="button" data-html="true" style="cursor: pointer;"></span>
                            <div class="popover-content hide">
                                <ul class="tooltip-hiden">
                                    <li><button id="<?php echo $index; ?>" class="btn btn-xs btn-default row-daybook">Edit</button></li>
                                    <?php if ($confirmcount == 0) { ?>
                                        <li><button id="<?php echo $index; ?>" class="btn btn-xs btn-default delete_confirmation margin-right-5">Delete</button></li>
                                    <?php } else { ?>
                                        <li><button id="<?php echo $index; ?>" data-id="<?php echo $deleteconfirm["deletepending_id"]; ?>" class="btn btn-xs btn-default delete_restore margin-right-5">Restore</button></li>
                                    <?php } ?>
                                </ul>
                            </div>
                            <?php
                        }
                    } else {
                        if ($currentDate <= $addedDate) {
                            ?>
                            <span class="icon icon-options-vertical popover-test" data-toggle="popover" data-placement="right" type="button" data-html="true" style="cursor: pointer;"></span>
                            <div class="popover-content hide">
                                <ul class="tooltip-hiden">
                                    <?php
                                    if ((in_array('/vendors/paymentedit', Yii::app()->user->menuauthlist))) {
                                        ?>
                                        <li><button id="<?php echo $index; ?>" class="btn btn-xs btn-default row-daybook margin-right-5">Edit</button></li>
                                        <?php
                                    }
                                    if ((in_array('/vendors/paymentdelete', Yii::app()->user->menuauthlist))) {
                                        ?>
                                        <li><button id="<?php echo $index; ?>" class="btn btn-xs btn-default delete_row margin-right-5">Delete</button></li>
                                    <?php } ?>
                                </ul>
                            </div>
                            <?php
                        }
                    }
                }
            }
            ?>
        </td>
    <?php } ?>
    <td><b>#<?php echo ($data["approval_status"] == 0) ? $data['ref_id'] : $data['daily_v_id']; ?></b></td>
    <td class="pro_back wrap" id="<?php echo "project_id" . $index; ?>">
        <?php
        $company = Company::model()->findByPk($data['company_id']);
        echo $company['name'];
        ?>
    </td>

    <td id="<?php echo "project_id" . $index; ?>"><?php echo $data["project_name"] ? $data["project_name"] : ""; ?></td>
    <td id="<?php echo "vendor_id" . $index; ?>"><?php echo $data["name"] ? $data["name"] : ""; ?></td>
    <td id="<?php echo "purchase_id" . $index; ?>">
    <?php 
    $purchase = Purchase::model()->findByPk($data["purchase_id"]);
    echo !empty($purchase) ? $purchase["purchase_no"] : ""; ?>
    </td>
    <td id="<?php echo "description" . $index; ?>"><?php echo $data["vendor_description"] ? $data["vendor_description"] : ""; ?></td>
    <td id="<?php echo "payment_type" . $index; ?>"><?php echo $data["caption"] ? $data["caption"] : ""; ?></td>
    <td id="<?php echo "bank" . $index; ?>"><?php echo $data["bank_name"] ? $data["bank_name"] : ""; ?></td>
    <td id="<?php echo "cheque_no" . $index; ?>"><?php echo $data["cheque_no"] ? $data["cheque_no"] : ""; ?></td>
    <td class="text-right" id="<?php echo "amount" . $index; ?>"><?php echo $data["amount"] ? Controller::money_format_inr($data["amount"], 2) : ""; ?></td>
    <td class="text-right"><span class="popover-test tax_amnt" data-toggle="popover" data-trigger="hover" data-placement="left" data-trigger="hover" type="button" data-html="true"><?php echo $data["tax_amount"] ? Controller::money_format_inr($data["tax_amount"], 2) : ""; ?></span>
        <div class="popover-content hide">
            <div><span>SGST: (<?php echo $data["sgst"] ? $data["sgst"] : ""; ?>&#37;) <?php echo $data["sgst_amount"] ? Controller::money_format_inr($data["sgst_amount"], 2) : ""; ?></span>
            </div>
            <div><span>CGST: (<?php echo $data["cgst"] ? $data["cgst"] : ""; ?>&#37;) <?php echo $data["cgst_amount"] ? Controller::money_format_inr($data["cgst_amount"], 2) : ""; ?></span>
            </div>
            <div><span>IGST: (<?php echo $data["igst"] ? $data["igst"] : ""; ?>&#37;) <?php echo $data["igst_amount"] ? Controller::money_format_inr($data["igst_amount"], 2) : ""; ?></span>
            </div>
        </div>
    </td>
    <?php
    if ((isset(Yii::app()->user->role) && ((in_array('/vendors/paymentedit', Yii::app()->user->menuauthlist)) || (in_array('/vendors/paymentdelete', Yii::app()->user->menuauthlist))))) {
    ?>
    <td class="text-right" id="<?php echo "tds" . $index; ?>"><?php echo Controller::money_format_inr($data["tds_amount"], 2); ?></td>
    <td class="text-right" id="<?php echo "paidamount" . $index; ?>"><?php echo Controller::money_format_inr($data["paidamount"], 2); ?></td>
    <?php } ?>
    
<input type="hidden" name="dailyvendorid[]" id="dailyvendorid<?php echo $index; ?>" value="<?php echo $data["daily_v_id"]; ?>" />
<input type="hidden" name="rowid[]" id="rowid<?php echo $index; ?>" value="<?php echo $index; ?>" />
</tr>
<?php
if ($index == 0) {
    ?>
    <tfoot>
        <tr>
            <th></th>
            <th class="total_amnt"></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th class="wrap text-right">Total = <?php echo Controller::money_format_inr($total_amount, 2); ?> </th>
            <th class="wrap text-right">Total = <?php echo Controller::money_format_inr($tax_amount, 2); ?> </th>
            <?php
            if ((isset(Yii::app()->user->role) && ((in_array('/vendors/paymentedit', Yii::app()->user->menuauthlist)) || (in_array('/vendors/paymentdelete', Yii::app()->user->menuauthlist))))) {
                ?>
                <th class="wrap text-right">Total = <?php echo Controller::money_format_inr($total_tds, 2); ?> </th>
                <th class="wrap text-right">Total = <?php echo Controller::money_format_inr($total_paid, 2); ?> </th>
                
            <?php } ?>
        </tr>
        <tr>
            <th></th>
            <th class="total_amnt"></th>

            <th colspan="7"></th>
            <th class="wrap text-right" colspan='2'>Grand Total = <?php echo Controller::money_format_inr($total_amount + $tax_amount, 2); ?> </th>
                <?php
                if ((isset(Yii::app()->user->role) && ((in_array('/vendors/paymentedit', Yii::app()->user->menuauthlist)) || (in_array('/vendors/paymentdelete', Yii::app()->user->menuauthlist))))) {
                    ?>
                <th colspan="3"></th>
            <?php } ?>
        </tr>
    </tfoot>
<?php } ?>
<script>
    $(document).ready(function () {
        $(".popover-test").popover({
            html: true,
            content: function () {
                return $(this).next('.popover-content').html();
            }
        });
        $('[data-toggle=popover]').on('click', function (e) {
            $('[data-toggle=popover]').not(this).popover('hide');
        });
        $('body').on('hidden.bs.popover', function (e) {
            $(e.target).data("bs.popover").inState.click = false;
        });
        $('body').on('click', function (e) {
            $('[data-toggle=popover]').each(function () {
                if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                    $(this).popover('hide');
                }
            });
        });

    });
</script>
