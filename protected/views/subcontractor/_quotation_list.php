<table class="table mt" id="item_table"> 
    <thead>
        <th>SI No</th>
        <th>Item Name</th>
        <th>Type</th>
        <th>Quantity</th>
        <th>Unit</th>
        <th>Rate</th>
        <th>Amount</th>
        <th>Action</th>
    </thead>
    <tbody>
        <?php
        $i = 1;
        foreach ($quotation_list as $sub_item) {
            $category_id = $sub_item->item_category_id;
            $category_total = ScquotationItems::model()->getCategoryTotal($model->scquotation_id, $category_id);
            if ($sub_item->item_amount  == 0) {
                $span = "7";
                $hide = "display:none";
            } else {
                $span = "";
                $hide = "";
            }
        ?>
            <tr class="<?php echo ($sub_item['approve_status'] == 'No') ? 'permission_style' : ''; ?>">
                <td style="<?php echo  $hide; ?>"><?php echo $i; ?></td>
                <td colspan="<?php echo $span ?>"><?php echo $sub_item->item_description; ?></td>
                <td style="<?php echo  $hide; ?>"><?php echo $sub_item->getType($sub_item->item_type);
                    ?></td>
                <td style="<?php echo  $hide; ?>"><?php echo $sub_item->item_quantity; ?></td>
                <td style="<?php echo  $hide; ?>"><?php echo $sub_item->item_unit; ?></td>
                <td style="<?php echo  $hide; ?>" class="text-right"><?php echo  Yii::app()->controller->money_format_inr($sub_item->item_rate, 2); ?></td>
                <td style="<?php echo  $hide; ?>" class="text-right"><?php echo  Yii::app()->controller->money_format_inr($sub_item->item_amount, 2); ?></td>
                <td>
                <?php if ($sub_item['approve_status'] == 'No') { ?>
                    <span class="icon icon-options-vertical popover-test" data-toggle="popover" data-placement="left" type="button" data-html="true" style="cursor: pointer;"></span>
                    <div class="popover-content hide">
                        <ul class="tooltip-hiden">
                        
                        <li><button type="button" id="<?php echo $sub_item->item_id; ?>" class="btn btn-xs btn-default edit">Edit</button></li>
                       
                        <li><button type="button" id="<?php echo $sub_item->item_id; ?>" class="btn btn-xs btn-default delete_item">Delete</button></li>
                            <?php
                            if ((isset(Yii::app()->user->role) && (in_array('/subcontractor/permissionapprove', Yii::app()->user->menuauthlist)))) {
                                if ($sub_item['approve_status'] == 'No') {
                            ?>
                                    
                                    <li><a href="" id="<?php echo $sub_item['item_id']; ?>" class="btn btn-xs btn-default permission_item approveoption_<?php echo $sub_item['item_id']; ?>">Approve</a></li>
                            <?php }
                            } ?>

                        </ul>
                    </div>
                 <?php } ?>
                </td>
            </tr>
        <?php
            if($sub_item->item_amount  != 0){
                $i++;
            }
        }
        ?>

    </tbody>
    <tfoot>
        <th colspan="6" class="text-right">Total</th>
        <th class="text-right"><?php echo $category_total; ?></th>
        <th></th>
    </tfoot>
</table>
<script>
    $(document).ready(function() {
        $(".popover-test").popover({
            html: true,
            content: function() {
                return $(this).next('.popover-content').html();
            }
        });
        $('[data-toggle=popover]').on('click', function(e) {
            $('[data-toggle=popover]').not(this).popover('hide');
        });
        $('body').on('hidden.bs.popover', function(e) {
            $(e.target).data("bs.popover").inState.click = false;
        });
        $('body').on('click', function(e) {
            $('[data-toggle=popover]').each(function() {
                if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                    $(this).popover('hide');
                }
            });
        });


    });
</script>