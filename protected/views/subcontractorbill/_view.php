<?php
/* @var $this SubcontractorbillController */
/* @var $data Subcontractorbill */
?>
<?php if ($index == 0) { ?>
<thead class="entry-table first-thead-row">
    <tr>

        <th>Sl No</th>
        <th>Company</th>
        <th>Project</th>
        <th>Subcontractor</th>
        <th>Quotation</th>
        <th>Stage</th>
        <th>Bill Number</th>
        <th>Date</th>
        <th>Amount</th>
        <th>Payment Status</th>
        <th>Created By</th>
        <th>Created Date</th>
        <th></th>
    </tr>
</thead>
<?php } ?>
<tr>
    <td><?php echo $index+1; ?></td>
    <td><?php echo $data->company->name; ?></td>
    <td><?php echo isset($data->quotation->project_id)?
    (!empty(Projects::model()->findByPk($data->quotation->project_id))?Projects::model()->findByPk($data->quotation->project_id)->name:""):""; ?></td>
    <td><?php echo isset($data->quotation->subcontractor_id)?(!empty(Subcontractor::model()->findByPk($data->quotation->subcontractor_id))?Subcontractor::model()->findByPk($data->quotation->subcontractor_id)->subcontractor_name:""):""; ?></td>
    <td><?php echo isset($data->quotation)?$data->quotation->scquotation_no:""; ?></td>
    <td><?php 
    if($data->stage_id!=""){
      $stage = ScPaymentStage::model()->findByPk($data->stage_id);
      echo $stage['payment_stage'];
  }
    ?></td>
    <td><?php echo $data->bill_number ?></td>
    <td><?php echo date("d-m-Y", strtotime($data->date)); ?></td>       
    <td><?php echo Controller::money_format_inr($data->total_amount,2);  ?></td>
    <?php 
      $status = Controller::getScBillPaymentStatus($data->id); 
        $bgcolor ="";
      if($status=="Pending"){
        $bgcolor ="style=background-color:#FF6347";
      }else if($status=="Partially Paid"){
        $bgcolor ="style=background-color:#FFFF00";
      }else if($status=="Fully Paid"){
        $bgcolor ="style=background-color:#2AD3008F";
      }
      ?>
    <td <?php echo $bgcolor ?>>
    
      <?php echo $status; ?>
    </td>
    <td><?php echo $data->user->first_name." ".$data->user->last_name; ?></td>
    <td><?php echo date("d-m-Y", strtotime($data->created_date));  ?></td>
    <td class="text-nowrap">
      <?php  if((isset(Yii::app()->user->role) && (in_array('/subcontractorbill/view', Yii::app()->user->menuauthlist)))){ ?>
        <a data-id="<?php echo $data->id; ?>" class="fa fa-eye viewBill" id="viewBill"></a>
      <?php } ?>
      <?php  if((isset(Yii::app()->user->role) && (in_array('/subcontractorbill/update', Yii::app()->user->menuauthlist)))){ ?>
        <a data-id="<?php echo $data->id; ?>" class="fa fa-edit editBill" id="editBill"></a>
      <?php } ?>
      <a data-id="<?php echo $data->id; ?>" class="fa fa-trash deleteBill" id="deleteBill"></a>
    </td>
</tr>
