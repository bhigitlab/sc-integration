<?php

/**
 * This is the model class for table "{{warehouse_transfer_deleted_log}}".
 *
 * The followings are the available columns in table '{{warehouse_transfer_deleted_log}}':
 * @property integer $id
 * @property string $warehouse_transfer_type_delete
 * @property integer $warehouse_transfer_type_deleted_id
 * @property integer $warehouse_transfer_type_deleted_no
 * @property integer $warehousereceipt_transfer_type
 * @property integer $warehousereceipt_despatch_id
 * @property integer $warehousereceipt_bill_id
 * @property integer $warehouseid_to
 * @property integer $warehouseid_from
 * @property integer $warehousereceipt_clerk
 * @property string $warehouse_transfer_items
 *
 * The followings are the available model relations:
 * @property Warehouse $warehouseidFrom
 * @property Warehouse $warehouseidTo
 */
class WarehouseTransferDeletedLog extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{warehouse_transfer_deleted_log}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('warehouse_transfer_type_delete, warehouse_transfer_type_deleted_id', 'required'),
			array('warehouse_transfer_type_deleted_id, warehouse_transfer_type_deleted_no, warehousereceipt_transfer_type, warehousereceipt_despatch_id, warehousereceipt_bill_id, warehouseid_to, warehouseid_from, warehousereceipt_clerk', 'numerical', 'integerOnly'=>true),
			array('warehouse_transfer_type_delete', 'length', 'max'=>1),
			array('warehouse_transfer_items', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, warehouse_transfer_type_delete, warehouse_transfer_type_deleted_id, warehouse_transfer_type_deleted_no, warehousereceipt_transfer_type, warehousereceipt_despatch_id, warehousereceipt_bill_id, warehouseid_to, warehouseid_from, warehousereceipt_clerk, warehouse_transfer_items', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'warehouseidFrom' => array(self::BELONGS_TO, 'Warehouse', 'warehouseid_from'),
			'warehouseidTo' => array(self::BELONGS_TO, 'Warehouse', 'warehouseid_to'),
			'clerk' => array(self::BELONGS_TO, 'Users', 'warehousereceipt_clerk'),
            
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'warehouse_transfer_type_delete' => 'Warehouse Transfer Type Delete',
			'warehouse_transfer_type_deleted_id' => 'Warehouse Transfer Type Deleted',
			'warehouse_transfer_type_deleted_no' => 'Warehouse Transfer Type Deleted No',
			'warehousereceipt_transfer_type' => 'Warehousereceipt Transfer Type',
			'warehousereceipt_despatch_id' => 'Warehousereceipt Despatch',
			'warehousereceipt_bill_id' => 'Warehousereceipt Bill',
			'warehouseid_to' => 'Warehouseid To',
			'warehouseid_from' => 'Warehouseid From',
			'warehousereceipt_clerk' => 'Warehousereceipt Clerk',
			'warehouse_transfer_items' => 'Warehouse Transfer Items',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('warehouse_transfer_type_delete',$this->warehouse_transfer_type_delete,true);
		$criteria->compare('warehouse_transfer_type_deleted_id',$this->warehouse_transfer_type_deleted_id);
		$criteria->compare('warehouse_transfer_type_deleted_no',$this->warehouse_transfer_type_deleted_no);
		$criteria->compare('warehousereceipt_transfer_type',$this->warehousereceipt_transfer_type);
		$criteria->compare('warehousereceipt_despatch_id',$this->warehousereceipt_despatch_id);
		$criteria->compare('warehousereceipt_bill_id',$this->warehousereceipt_bill_id);
		$criteria->compare('warehouseid_to',$this->warehouseid_to);
		$criteria->compare('warehouseid_from',$this->warehouseid_from);
		$criteria->compare('warehousereceipt_clerk',$this->warehousereceipt_clerk);
		$criteria->compare('warehouse_transfer_items',$this->warehouse_transfer_items,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return WarehouseTransferDeletedLog the static model class
	 */
	public function warehousereceiptBillIdDetails($warehousereceipt_bill_id) {
		$bill = Bills::model()->findByPk($warehousereceipt_bill_id);
		$bill_id_details['purchase_id'] = !empty($bill)?$bill->purchase_id:"";
		$bill_id_details['bill_number'] = !empty($bill)?$bill->bill_number:"";
    	return $bill_id_details;
	}
	public function warehousereceiptDespatchDetails($warehousereceipt_despatch_id) {
		$despatch = Warehousedespatch::model()->findByPk($warehousereceipt_despatch_id);
		$despatch_id_details['warehousedespatch_no'] = !empty($despatch)?$despatch->warehousedespatch_no:"";
		return $despatch_id_details;
    }
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
