<div id="dashboard-content">
    <div class="dashboard-title-area">
        <h4 class="fw-bold margin-zero">Notifications</h4>
        <div class="dashboard-title-selects">

        </div>
    </div>

    <div id="content-wrapper" class="p-relative responsive-content-base">
        <div>
            <div class="dashboard-buttons pull-left">
            </div>
            <div class="dashboard-icon-buttons pull-right">

            </div>
        </div>

        <div id="notifications" class="detail_sec tab-pane active in">
            <div id="dasboard_data">
                <div class="flex-changes">
                    <ul class="nav nav-tabs tab_section responsive-list">
                        <?php if (isset(Yii::app()->user->role) && (in_array('/dashboard/listall', Yii::app()->user->menuauthlist))) { ?>
                            <li class="nav-item mb-3 tab_btn active">

                                <a class="nav-link allist tab_btn_1" data-id="1" href="#all">
                                    <div class="notify_checkbox">
                                        <?php
                                        if ($all_count != 0) { ?><input type="checkbox"
                                                class="checkallnotification mt-0" id="checkallnotifi_al" value="1" />
                                        <?php } ?><span class="mt3">All</span>
                                    </div>
                                    <?php
                                    if ($all_count != 0) { ?>

                                        <?php

                                        if ($all_count > 100) {
                                            ?>
                                            <span class="badge"> 99+</span>
                                            <?php
                                        } else { ?>
                                            <span class="badge">
                                                <?php echo $all_count; ?>
                                            </span>
                                        <?php }
                                    }
                                    ?>
                                </a>
                            </li>
                        <?php } ?>
                        <?php
                        if (isset(Yii::app()->user->role) && (in_array('/dashboard/poapproval', Yii::app()->user->menuauthlist))) {
                            ?>
                            <li class="nav-item mb-3 tab_btn">
                                <a class="nav-link  poapproval tab_btn_7" data-id="7" href="#poapproval">PO APPROVAL
                                    <?php
                                    if ($po_count != 0) {
                                        if ($po_count > 100) {
                                            ?>
                                            <span class="badge">99+ </span>
                                            <?php
                                        } else {
                                            ?>
                                            <span class="badge">
                                                <?php echo $po_count; ?>
                                            </span>
                                            <?php
                                        }
                                    }
                                    ?>
                                </a>
                            </li>
                        <?php } ?>
                        <li class="nav-item mb-3 tab_btn">
                            <a class="nav-link expense_notification tab_btn_2" data-id="2" href="#expensenotification">
                                <div class="notify_checkbox">
                                    <?php
                                    if ($expense_notifications_count != 0) { ?>
                                        <input type="checkbox" class="checkallnotification mt-0" id="checkallnotifi_ea"
                                            value="1"><?php } ?><span class="mt3">EXPENSE ALERT </span>
                                </div>
                                <?php
                                if ($expense_notifications_count != 0) {
                                    if ($expense_notifications_count > 100) {
                                        ?>
                                        <span class="badge">99+ </span>
                                        <?php
                                    } else {
                                        ?>
                                        <span class="badge">
                                            <?php echo $expense_notifications_count; ?>
                                        </span>
                                        <?php
                                    }
                                }
                                ?>
                            </a>
                        </li>
                        <?php
                        if (isset(Yii::app()->user->role) && (in_array('/dashboard/scquotation', Yii::app()->user->menuauthlist))) {
                            ?>
                            <li class="nav-item mb-3 tab_btn">
                                <a class="nav-link  scquotation tab_btn_3" data-id="3" href="#editrequest">
                                    <div class="notify_checkbox">
                                        <?php
                                        if ($scquotation_count != 0) { ?><input type="checkbox"
                                                class="checkallnotification mt-0" id="checkallnotifi_sc" value="1">
                                        <?php } ?><span class="mt3">SC QUOTATION</span>
                                    </div>
                                    <?php
                                    if ($scquotation_count != 0) {
                                        if ($scquotation_count > 100) {
                                            ?>
                                            <span class="badge">99+ </span>
                                            <?php
                                        } else {
                                            ?>
                                            <span class="badge">
                                                <?php echo $scquotation_count; ?>
                                            </span>
                                            <?php
                                        }
                                    }
                                    ?>
                                </a>
                            </li>
                        <?php } ?>
                        <?php
                        if (isset(Yii::app()->user->role) && (in_array('/dashboard/editrequest', Yii::app()->user->menuauthlist))) {
                            ?>
                            <li class="nav-item mb-3 tab_btn">
                                <a class="nav-link edit_request tab_btn_3" data-id="3" href="#editrequest">
                                    <div class="notify_checkbox">
                                        <?php
                                        $editrequest = ApproveRequestHelper::editRequestData();
                                        $edit_count = count($editrequest);

                                        if ($edit_count > 0) { ?>
                                            <input type="checkbox" class="checkalledit mt-0" id="checkalledit" value="1" />
                                        <?php } ?>
                                        <span class="mt3">EDIT REQUEST</span>
                                    </div>
                                    <?php
                                    if ($edit_count != 0) {
                                        if ($edit_count > 100) {
                                            ?>
                                            <span class="badge">99+ </span>
                                            <?php
                                        } else {
                                            ?>
                                            <span class="badge">
                                                <?php echo $edit_count; ?>
                                            </span>
                                            <?php
                                        }
                                    }
                                    ?>
                                </a>
                            </li>
                        <?php } ?>
                        <?php
                        if (isset(Yii::app()->user->role) && (in_array('/dashboard/deleterequest', Yii::app()->user->menuauthlist))) {
                            ?>
                            <li class="nav-item mb-3 tab_btn">
                                <a class="nav-link delete_request tab_btn_4" data-id="4" href="#deleterequest">
                                    <div class="notify_checkbox">
                                        <?php if ($deletemodel->totalItemCount > 0) { ?>
                                            <input type="checkbox" class="checkalldelete mt-0" id="checkalldelete" value="1" />
                                        <?php } ?>
                                        <span>DELETE REQUEST</span>
                                    </div>
                                    <?php
                                    if ($delete_count != 0) {
                                        if ($delete_count > 100) {
                                            ?>
                                            <span class="badge">99+</span>
                                            <?php
                                        } else {
                                            ?>
                                            <span class="badge">
                                                <?php echo $delete_count; ?>
                                            </span>
                                            <?php
                                        }
                                    }
                                    ?>
                                </a>
                            </li>
                        <?php } ?>


                        <?php
                        if (isset(Yii::app()->user->role) && (in_array('/dashboard/notification', Yii::app()->user->menuauthlist))) {
                            ?>
                            <li class="nav-item mb-3 tab_btn">
                                <a class="nav-link notification tab_btn_5" data-id="5" href="#notification">

                                    <div class="notify_checkbox">
                                        <?php
                                        if ($notification_count != 0) { ?><input type="checkbox"
                                                class="checkallnotification mt-0" id="checkallnotifi_no" value="1">
                                        <?php } ?><span class="mt3">NOTIFICATIONS</span>
                                    </div>
                                    <?php
                                    if ($notification_count != 0) {
                                        if ($notification_count > 100) {
                                            ?>
                                            <span class="badge">99+</span>
                                            <?php
                                        } else {
                                            ?>
                                            <span class="badge">
                                                <?php echo $notification_count ?>
                                            </span>

                                            <?php
                                        }
                                    }
                                    ?>
                                </a>
                            </li>
                        <?php } ?>
                    </ul>



                    <div id="errormessage"></div>
                    <button class="button btn btn-info" class="mb-10" style="display:none;" id="clearNotification">Clear
                        Notifications</button>
                </div>

                <div class="allist_div">
                    <?php
                    $this->widget('zii.widgets.CListView', array(
                        'dataProvider' => $alldata,
                        'itemView' => '_alllisting',
                        'template' => '<div class="">{items}</div>{pager}',
                    ));
                    ?>
                </div>


                <div class="poapproval_div" style="display:none;">
                    <div>
                        <div class="permission_box">
                            <?php
                            $this->widget('zii.widgets.CListView', array(
                                'dataProvider' => $purchaseorder,
                                'itemView' => '_newpurchase',
                                'template' => '<div>{items}</div>{pager}',
                                'ajaxUpdate' => true,
                            ));
                            ?>
                        </div>
                    </div>
                </div>


                <div class="expense_notification_div" style="display:none;">
                    <div>
                        <div class="permission_box">
                            <?php
                            if ($expense_notifications_count === 0) {

                                echo '<p style="text-align:center">No results found.</p>';
                            } else {
                                $this->widget('zii.widgets.CListView', array(
                                    'dataProvider' => $expensenotifications,
                                    'itemView' => '_expensenotifications',
                                    'template' => '<div class="">{items}</div>{pager}',
                                    'ajaxUpdate' => true,
                                ));
                            }
                            ?>
                        </div>
                    </div>
                </div>


                <div class="scquotation_div" style="display:none;">
                    <div>
                        <div class="permission_box">
                            <?php
                            $this->widget('zii.widgets.CListView', array(
                                'dataProvider' => $pendingquotation,
                                'itemView' => '_newquotations',
                                'template' => '<div class="">{items}</div>{pager}',
                                'ajaxUpdate' => true,
                            ));
                            ?>
                        </div>
                    </div>
                </div>


                <div class="edit_request_div" style="display:none;">
                    <div class="edit_area">
                        <div class="clearfix">
                            <div class="pull-right">
                                <input type="button" class="btn btn-sm btn-danger" id="cancelAllEdit" value="Cancel"
                                    style="display:none;margin-bottom:10px;">
                                <input type="button" class="btn btn-sm btn-success" id="approveAllEdit" value="Approve"
                                    style="display:none;margin-bottom:10px;">
                            </div>
                        </div>
                        <div class="permission_box">
                            <div id="newlist">
                                <?php
                                echo $this->renderPartial(
                                    '_editrequest'

                                );
                                ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="delete_request_div" style="display:none;">
                    <div class=" delete_area">
                        <div class="clearfix">
                            <div class="pull-right">
                                <input type="button" class="btn btn-sm btn-danger" id="cancelAllDelete" value="Cancel"
                                    style="display:none;margin-bottom:10px;">
                                <input type="button" class="btn btn-sm btn-success" id="approveAllDelete"
                                    value="Approve" style="display:none;margin-bottom:10px;">
                            </div>
                        </div>
                        <div class="permission_box">
                            <div id="newlist1">
                                <?php
                                $this->widget('zii.widgets.CListView', array(
                                    'dataProvider' => $deletemodel,
                                    'itemView' => '_newdeleterequest',
                                    'template' => '<div class="">{items}</div>{pager}',
                                    'ajaxUpdate' => true,
                                ));
                                ?>
                            </div>
                        </div>
                    </div>
                </div>


                <?php
                $display_style = "";
                $display_style1 = "display:none;";
                ?>


                <div class="notification_div" style="<?php echo $display_style1; ?>">
                    <div class="list-project">
                        <div class="clearfix">
                            <?php if ((isset(Yii::app()->user->role) && (in_array("/dashboard/listNotifications", Yii::app()->user->menuauthlist)))) { ?>

                                <a target="_blank" href="<?php echo Yii::app()->createUrl('dashboard/listNotifications') ?>"
                                    class="btn_notify">All Notifications</a>
                            <?php } ?>
                        </div>
                        <div class="permission_box">
                            <?php
                            $this->widget('zii.widgets.CListView', array(
                                'dataProvider' => $notifications,
                                'itemView' => '_newnotifications',
                                'template' => '<div class="">{items}</div>{pager}',
                                'ajaxUpdate' => true,
                            ));
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>

</div>
</div>
<script type="text/javascript">


    $(document).ready(function () { })
    //graphes 



    function updateRequestAction(id, status, tableName, modelName, parentName) {
        $('.loading-overlay').addClass('is-active');
        $.ajax({
            type: "GET",
            data: {
                id: id,
                status: status,
                tableName: tableName,
                modelName: modelName,
                parentName: parentName

            },
            url: "<?php echo Yii::app()->createUrl("dashboard/updaterequestaction") ?>",
            success: function (data) {
                $('.compare_box').hide();
                if (data.response == 'success') {
                    $("#errormessage").show()
                        .html('<div class="alert alert-success">' + data.msg + '</div>')
                        .fadeOut(10000);
                    setTimeout(function () {
                        window.location.reload(1);
                    }, 3000);

                }
                if (data == 2) {
                    $("#errormessage").show()
                        .html('<div class="alert alert-warning">Failed! Please try again.</div>')
                        .fadeOut(10000);
                    setTimeout(function () {
                        window.location.reload(1);
                    }, 3000);
                    return false;
                } else {
                    setTimeout(location.reload, 200);
                    // $("#newlist").html(data);
                    if (status == 1) {
                        $("#errormessage").show()
                            .html('<div class="alert alert-success">Success! Request approved successfully.</div>')
                            .fadeOut(10000);
                    } else if (status == 2) {
                        $("#errormessage").show()
                            .html('<div class="alert alert-success">Success! Request cancelled successfully.</div>')
                            .fadeOut(10000);
                    }
                    setTimeout(function () {
                        window.location.reload(1);
                    }, 3000);
                }
                location.reload();
            }

        });
        location.reload();

    }

    function deleteRequestAction(delId, status) {
        $('.loading-overlay').addClass('is-active');
        $.ajax({
            type: "GET",
            dataType: "json",
            data: {
                delId: delId,
                status: status
            },
            url: "<?php echo Yii::app()->createUrl("dashboard/deleterequestaction") ?>",
            success: function (data) {
                $("#errormessage").show()
                    .html('<div class="alert alert-' + data.response + '">' + data.msg + '</div>')
                    .fadeOut(5000);
                setTimeout(function () {
                    window.location.reload(1);
                }, 3000);

            }
        });
    }

    function updateAllRequestAction(editArray, status) {
        $('.loading-overlay').addClass('is-active');
        $.ajax({
            type: "GET",
            data: {
                records: editArray,
                status: status
            },
            url: "<?php echo Yii::app()->createUrl("dashboard/allupdaterequestaction") ?>",
            success: function (data) {
                $("#newlist").html(data);
                if (status == 1) {
                    $("#errormessage").show()
                        .html('<div class="alert alert-success">Success! Selected requests are approved successfully.</div>')
                        .fadeOut(10000, function () {
                            location.reload();
                        });

                } else if (status == 2) {
                    $("#errormessage").show()
                        .html('<div class="alert alert-success">Success! Selected requests are cancelled successfully.</div>')
                        .fadeOut(10000);
                }
            }
        });
    }

    function deleteAllRequestAction(deleteArray, status) {
        $('.loading-overlay').addClass('is-active');
        $.ajax({
            type: "GET",
            dataType: "json",
            data: {
                deleteArray: deleteArray,
                status: status
            },

            url: "<?php echo Yii::app()->createUrl("dashboard/alldeleterequestaction") ?>",
            success: function (data) {
                $("#errormessage").show()
                    .html('<div class="alert alert-' + data.response + '">' + data.msg + '</div>')
                    .fadeOut(5000);
            }

        });
        setTimeout(function () {
            window.location.reload(1);
        }, 3000);
    }

    $(document).on('click', '.cancel_edit', function () {
        var id = $(this).attr("id");
        var tableName = $(this).attr('data-table');
        var modelName = $(this).attr('data-model');
        var parentName = $(this).attr('data-parent');


        if (confirm("Do you really want to cancel this request?")) {
            updateRequestAction(id, 2, tableName, modelName, parentName);
        } else {
            return false;
        }
    });
    $(document).on('click', '.approve_edit', function () {
        var id = $(this).attr("id");
        var tableName = $(this).attr('data-table');
        var modelName = $(this).attr('data-model');
        var parentName = $(this).attr('data-parent');

        if (confirm("Are you sure you want to accept this request?")) {
            updateRequestAction(id, 1, tableName, modelName, parentName);
        } else {
            return false;
        }
    });

    $(document).on('click', '.cancel_delete', function () {
        var id = $(this).attr("id");
        if (confirm("Do you really want to cancel this request?")) {
            deleteRequestAction(id, 2);
        } else {
            return false;
        }
    });
    $(document).on('click', '.approve_delete', function () {
        var id = $(this).attr("id");
        if (confirm("Are you sure you want to accept this request?")) {
            deleteRequestAction(id, 1);
        } else {
            return false;
        }
    });
    $(document).on('click', '#approveAllEdit', function () {
        var editArray = [];
        $("input:checkbox[name=editrequest]:checked").each(function () {
            editArray.push({
                id: $(this).data('id'), ref_id: $(this).data('ref_id'),
                tableName: $(this).data('table'),
                modelName: $(this).data('model'),
                parentName: $(this).data('parent')
            });

        });
        //alert(editArray);
        if (editArray.length > 0)
            updateAllRequestAction(editArray, 1);
        else
            alert("Please select atleast one item.");
    });
    $(document).on('click', '#cancelAllEdit', function () {
        var editArray = [];
        $("input:checkbox[name=editrequest]:checked").each(function () {
            editArray.push($(this).val());
        });
        if (editArray.length > 0)
            updateAllRequestAction(editArray, 2);
        else
            alert("Please select atleast one item.");
    });
    $(document).on('click', '#approveAllDelete', function () {
        var deleteArray = [];
        $("input:checkbox[name=deleterequest]:checked").each(function () {
            deleteArray.push($(this).val());
        });
        if (deleteArray.length > 0)
            deleteAllRequestAction(deleteArray, 1);
        else
            alert("Please select atleast one item.");
    });
    $(document).on('change', '.clearnotificationcheck', function () {
        if ($('.clearnotificationcheck:checked').length > 0) {
            $('#clearNotification').show();
        } else {
            $('#clearNotification').hide();
        }
    });


    $(document).on('click', '#cancelAllDelete', function () {
        var deleteArray = [];
        $("input:checkbox[name=deleterequest]:checked").each(function () {
            deleteArray.push($(this).val());
        });
        if (deleteArray.length > 0)
            deleteAllRequestAction(deleteArray, 2);
        else
            alert("Please select atleast one item.");
    });
    $(document).on('click', '#checkalledit', function () {
        if ($(this).prop('checked')) {
            $('.editrequestcheck').prop('checked', true);
        } else {
            $('.editrequestcheck').prop('checked', false);
        }
    });
    $(document).on('click', '#checkallnotifi_al', function () {
        if ($(this).prop('checked')) {
            $(this).prop('checked', true);
            $('.clearcheck_all').prop('checked', true);
            $('#clearNotification').show();
        } else {
            $('.clearcheck_all').prop('checked', false);
            $('#clearNotification').hide();
        }
    });
    $(document).on('click', '#checkallnotifi_ea', function () {
        if ($(this).prop('checked')) {
            $(this).prop('checked', true);
            $('.clearcheck_ea').prop('checked', true);
            $('#clearNotification').show();
        } else {
            $('.clearcheck_ea').prop('checked', false);
            $('#clearNotification').hide();
        }
    });
    $(document).on('click', '#checkallnotifi_sc', function () {
        if ($(this).prop('checked')) {
            $(this).prop('checked', true);
            $('.clearcheck_sa').prop('checked', true);
            $('#clearNotification').show();
        } else {
            $('.clearcheck_sa').prop('checked', false);
            $('#clearNotification').hide();
        }
    });
    $(document).on('click', '#checkallnotifi_no', function () {
        if ($(this).prop('checked')) {
            $(this).prop('checked', true);
            $('.clearcheck_no').prop('checked', true);
            $('#clearNotification').show();
        } else {
            $('.clearcheck_no').prop('checked', false);
            $('#clearNotification').hide();
        }
    });
    $(document).on('click', '#checkalldelete', function () {
        if ($(this).prop('checked')) {
            $('.deleterequestcheck').prop('checked', true);
        } else {
            $('.deleterequestcheck').prop('checked', false);
        }
    });
    $(document).on('click', '.editrequestcheck', function () {
        if (!$(this).prop('checked')) {
            $('#checkalledit').prop('checked', false);
        }
    });
    $(document).on('click', '.deleterequestcheck', function () {
        if (!$(this).prop('checked')) {
            $('#checkalldelete').prop('checked', false);
        }
    });

    $('#download').click(function () {
        html2canvas($("#contenthtml"), {
            onrendered: function (canvas) {
                var imgData = canvas.toDataURL('image/jpeg');

                var imgWidth = 210;
                var pageHeight = 295;
                var imgHeight = canvas.height * imgWidth / canvas.width;
                var heightLeft = imgHeight;
                var doc = new jsPDF('p', 'mm');
                var position = 0;
                doc.page = 1;

                doc.addImage(imgData, 'jpeg', 0, position, imgWidth, imgHeight);
                heightLeft -= pageHeight;

                while (heightLeft >= 0) {
                    position = heightLeft - imgHeight;
                    doc.addPage();
                    doc.addImage(imgData, 'jpeg', 0, position, imgWidth, imgHeight);
                    heightLeft -= pageHeight;
                }
                doc.save('expenses.pdf');

            }
        });
    });

    $(document).ready(function () {
        var lengthText = 30;
        var shortText;
        var id;
        $('.desc').hover(function () {
            $(this).text(text);
            id = $(this).attr('id');
            var text = $('#' + id).text();
            shortText = $.trim(text).substring(0, lengthText).split(" ").slice(0, -1).join(" ") + "...";

        }, function () {
            $(id).text(shortText);
        });
    });
    $(document).delegate(".edit_area input[type=checkbox]", "click", function () {
        if ($(".edit_area").find("input:checked").length > 0) {
            $("#cancelAllEdit,#approveAllEdit").show();
        } else {
            $("#cancelAllEdit,#approveAllEdit").hide();
        }
    });
    $(document).delegate(".delete_area input[type=checkbox]", "click", function () {
        if ($(".delete_area").find("input:checked").length > 0) {
            $("#cancelAllDelete,#approveAllDelete").show();
        } else {
            $("#cancelAllDelete,#approveAllDelete").hide();
        }
    });
    $(document).delegate("#checkalledit", "click", function () {
        if (this.checked) {
            $("#cancelAllEdit,#approveAllEdit").show();
        } else {
            $("#cancelAllEdit,#approveAllEdit").hide();
        }
        return true;
    });
    $(document).delegate("#checkalldelete", "click", function () {
        if (this.checked) {
            $("#cancelAllDelete,#approveAllDelete").show();
        } else {
            $("#cancelAllDelete,#approveAllDelete").hide();
        }
        return true;
    });
    $(document).delegate("#newlist input[type=checkbox]", "change", function () {
        var total_box = $("#newlist").find("input[type=checkbox]").length;
        var checked_box = $("#newlist").find("input:checked").length;
        if (total_box == checked_box) {
            $("#checkalledit").prop('checked', true);
        } else {
            $("#checkalledit").prop('checked', false);
        }

    });
    $(document).delegate("#newlist1 input[type=checkbox]", "change", function () {
        var total_box = $("#newlist1").find("input[type=checkbox]").length;
        var checked_box = $("#newlist1").find("input:checked").length;
        if (total_box == checked_box) {
            $("#checkalldelete").prop('checked', true);
        } else {
            $("#checkalldelete").prop('checked', false);
        }

    });

    $(document).on("click", ".view_not", function (e) {
        $('.loading-overlay').addClass('is-active');
        var notification_id = this.id;
        e.stopImmediatePropagation();
        e.preventDefault();
        $.ajax({
            type: 'POST',
            url: "<?php echo Yii::app()->createUrl("dashboard/notificationview") ?>",
            data: {
                id: notification_id
            },
            success: function (data) {
                if (data == "success") {
                    location.reload();
                }
            },
            error: function (data) {
                alert('Error occured.please try again');
            },

        });
    })

    $(document).on('click', '.approve_po', function (e) {
        $('.loading-overlay').addClass('is-active');
        e.preventDefault();
        var element = $(this);
        var item_id = $(this).attr('id');
        $.ajax({
            url: '<?php echo Yii::app()->createAbsoluteUrl('purchase/permission'); ?>',
            type: 'POST',
            dataType: 'json',
            data: {
                item_id: item_id
            },
            success: function (response) {
                if (response.response == 'success') {
                    location.reload();
                } else if (response.response == 'warning') {

                } else {
                }
            }
        });
    });
    $(document).on('click', '.view_expense', function (e) {
        e.preventDefault();
        var element = $(this);
        var id = $(this).attr('id');
        $.ajax({
            url: '<?php echo Yii::app()->createAbsoluteUrl('dashboard/changeexpenseview'); ?>',
            type: 'POST',
            dataType: 'json',
            data: {
                id: id
            },
            success: function (response) {
                if (response.response == 'success') {
                    location.reload();
                } else if (response.response == 'warning') {

                } else {
                }
            }
        });
    });
    $(document).on('change', '.expenseclearnotificationcheck', function () {
        if ($('.expenseclearnotificationcheck:checked').length > 0) {
            $('#clearNotification').hide();
            $('#expenseclearNotification').show();

        } else {
            $('#expenseclearNotification').hide();
        }
    });


    $(document).on('click', '.po_rate_decline', function (e) {
        if (confirm("Do you really want to decline this request ?")) {
            $(this).parents(".parent_row").find(".box").animate({
                width: "400px"
            });
        } else {
            return false;
        }

    });

    $(document).on('click', '.panel-close', function (e) {
        $(this).parents(".parent_row").find(".box").animate({
            width: "0px"
        });
    });

    $(document).on('click', '.decline_comment_submit', function (e) {
        $('.loading-overlay').addClass('is-active');
        e.preventDefault();
        var element = $(this);
        var item_id = $(this).attr('id');
        var comments = $(this).parents(".row").find(".decline_remarks").val();

        $.ajax({
            url: '<?php echo Yii::app()->createAbsoluteUrl('purchase/ratedecline'); ?>',
            type: 'POST',
            dataType: 'json',
            data: {
                id: item_id,
                comments: comments
            },
            success: function (response) {
                if (response.response == 'success') {
                    $("#dashboard_data_holder").load(location.href + " #dasboard_data");
                    $().toastmessage('showSuccessToast', response.msg);
                } else if (response.response == 'error') {
                    $().toastmessage('showErrorToast', "Error occured");
                } else {
                }
                setTimeout(function () {
                    location.reload();
                }, 3000);
            }
        });

    });
    $(function () {
        $('.tab_btn').click(function () {
            $(this).addClass("active");
            $(this).siblings().removeClass("active");
        });
        $(".parent_row").css("background-color", "#fafafa");
        $(".parent_row:even").css("background-color", "#fff");
        $('.compare_icon').click(function (e) {
            $(this).hide();
            $(this).siblings('.box_close').show();
            $(this).parents(".box-collapse").siblings('.compare_box').slideDown();
        });
        $('.box_close').click(function () {
            $(this).hide();
            $(this).siblings('.compare_icon').show();
            $(this).parents(".box-collapse").siblings('.compare_box').slideUp();
        });
    })
    $(document).ajaxComplete(function () {
        $('.loading-overlay').removeClass('is-active');
        $('#loading').hide();
    });

</script>
<style>
    .parent_row {
        position: relative;
    }

    .box {
        position: absolute;
        right: 0;
        overflow: hidden;
        z-index: 1;
        width: 0px;
    }

    .box-inner {
        padding: 10px;
    }
</style>