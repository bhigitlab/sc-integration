<?php

/**
 * This is the model class for table "{{notifications}}".
 *
 * The followings are the available columns in table '{{notifications}}':
 * @property integer $id
 * @property string $action
 * @property string $message
 * @property integer $parent_id
 * @property string $date
 * @property integer $requested_by
 * @property integer $approved_by
 */
class Notifications extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{notifications}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('action, parent_id, date, requested_by, approved_by', 'required'),
			array('parent_id, requested_by, approved_by', 'numerical', 'integerOnly'=>true),
			array('action', 'length', 'max'=>100),
                        array('message', 'length', 'max'=>500),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, action, message, parent_id, date, requested_by, approved_by', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'action' => 'Action',
                        'message' => 'Message',
			'parent_id' => 'Parent',
			'date' => 'Date',
			'requested_by' => 'Requested By',
			'approved_by' => 'Approved By',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('action',$this->action,true);
                $criteria->compare('message',$this->message,true);
		$criteria->compare('parent_id',$this->parent_id);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('requested_by',$this->requested_by);
		$criteria->compare('approved_by',$this->approved_by);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        public function notificationsearch()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$criteria->compare('id',$this->id);
		$criteria->compare('action',$this->action,true);
        $criteria->compare('message',$this->message,true);
		$criteria->compare('parent_id',$this->parent_id);
		$criteria->compare('date',$this->date,true);
		if(Yii::app()->user->role != 1)
		$criteria->compare('requested_by',Yii::app()->user->id);
		$criteria->compare('approved_by',$this->approved_by);
		$criteria->compare('view_status',0);
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'sort' => array(
                'defaultOrder' => 'id DESC',
            ),
            'pagination'=>array('pageSize'=>20),
		));
	}
	public function allnotifications()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('action',$this->action,true);
                $criteria->compare('message',$this->message,true);
		$criteria->compare('parent_id',$this->parent_id);
		$criteria->compare('date',$this->date,true);
		if(Yii::app()->user->role != 1)
		$criteria->compare('requested_by',Yii::app()->user->id);
		$criteria->compare('approved_by',$this->approved_by);
		
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                        'sort' => array(
                            'defaultOrder' => 'id DESC',
                        ),
                        'pagination'=>false,
		));
	}

	public function approveddata($id){
$user = Users::model()->findByPk($id);
if(!empty($user)){
	return $user->first_name;
}else{
	return '';
}


	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Notifications the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
