<?php
/* @var $this MaterialEstimationController */
/* @var $model MaterialEstimation */

if ($estimation == 'material_estimation') {
    $this->breadcrumbs = array(
        'Material Estimations' => array('index'),
        'Create',
    );
    $heading = 'Material Estimation';
} elseif ($estimation == 'labour_estimation') {
    $this->breadcrumbs = array(
        'Labour Estimations' => array('index'),
        'Create',
    );
    $heading = 'Labour Estimation';
} elseif ($estimation == 'equipment_estimation') {
    $this->breadcrumbs = array(
        'Equipment Estimations' => array('index'),
        'Create',
    );
    $heading = 'Equipment Estimation';
} else {
    $heading = 'Estimation';
}
?>

<div class="panel panel-gray">
    <div class="panel-heading form-head">
        <h3 class="panel-title"><?php echo CHtml::encode($heading); ?></h3>
    </div>
    <?php echo $this->renderPartial('_form', array('model' => $model,'model_labour'=>$model_labour,'model_equipment'=>$model_equipment,'estimation'=>$estimation,'project_id'=>$project_id)); ?>
</div>
