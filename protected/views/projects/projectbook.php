<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * 
 */
$projects = CHtml::listData(Projects::model()->findAll(array(
    'select' => array('pid, name'),
    "condition" => 'pid in (select projectid from jp_project_assign where userid=" ' . Yii::app()->user->id . ' ")',
    //'order' => 'name',
    'distinct' => true
)), 'pid', 'name');


$projectlist = json_encode(array_values($projects)); //print_r($projectlist);


$saveurl = Yii::app()->createAbsoluteUrl("projects/savedata");
/*
?>
<!--<script src="http://docs.handsontable.com/0.16.1/scripts/jquery.min.js"></script>-->
<?php //Yii::app()->clientScript->registerScriptFile('http://docs.handsontable.com/0.16.1/bower_components/handsontable/dist/handsontable.full.js', CClientScript::POS_END); ?>
<script src="http://docs.handsontable.com/0.16.1/bower_components/handsontable/dist/handsontable.full.js"></script>
<link type="text/css" rel="stylesheet" href="http://docs.handsontable.com/0.16.1/bower_components/handsontable/dist/handsontable.full.min.css">
<?php
*/
?>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/handsontable.full.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/handsontable.full.min.css" />
<?php
Yii::app()->clientScript->registerCss('mycss', '    
    #example1.handsontable table{
       width:100%;
    }
    .handsontable{
        z-index:0 !important;
    }
    
    #example1.handsontable table td{
        line-height: 25px;
    }
    .handsontable .htCore .readonly {
    background-color: #F2F2F2;
    font-style: italic;
}
#example1consolesucces{
        color: white;
        background: #247B17;
        padding: 5px;
        display:none;
    }
    #example1console{
        color: white;
        background: #E10;;
        padding: 5px;
        display:none;
    }
.console{
 /*       margin-top: 10px;
    margin-left: 550px;
  */
    padding-bottom: 10px;
    color: #E10;
  /*	
    width: 250px;
    height: 30px;
   */	
}
.btn{
    margin-top: 25px;
    float:right;
}
.link{
    color:blue;
    text-decoration:underline;
}
.row{
  margin-left: 0px;
  margin-right: -15px;
}
.htCore tr td:nth-child(5),.htCore tr td:last-child {text-align:left !important;}

.htCore thead tr th:first-child{display:none;width:1240px}
   .hiden_style{display: none}
   .htCore tr td:nth-child(2),.htCore tr th:nth-child(2)  {border-left: 1px solid #CCC !important;}
   .handsontable .handsontable:not(.ht_master) table {
       -webkit-box-shadow: none;
       box-shadow: none;
   }

/*.handsontable table.htCore{width:1140px !important;}
.wtHider{width: 1140px !important;}
.wtHolder{overflow-x: hidden !important;}*/
');
?>
<div class="container">
    <div class="loading-overlay">
        <span class="fa fa-spinner fa-3x fa-spin"></span>
    </div>
    <br>
    <div id="loading"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/ajax-loader.gif"></div>
    <div class="row">Date <br>
        <?php echo CHtml::activeTextField($model, 'date', array('style' => 'height:25px;', 'size' => 10, 'id' => 'Projects_date', 'value' => date('d-M-Y'), 'onchange' => 'changedate()')); ?>
        <?php
        $this->widget('application.extensions.calendar.SCalendar', array(
            //'model'=>$model,
            'inputField' => 'Projects_date',
            'ifFormat' => '%d-%b-%Y'
        ));
        ?>
        <a href="#" id="previous" class="link">Previous</a>&nbsp<a href="#" id="current" class="link">Current</a>&nbsp<a href="#" id="next" class="link">Next</a>

        <?php if (Yii::app()->user->role == 1) { ?>
            <?php
            $tblpx = Yii::app()->db->tablePrefix;
            $sql = "SELECT `t`.`userid`, concat_ws(' ',`t`.`first_name`, `t`.`last_name`) as full_name,"
                . "`{$tblpx}user_roles`.`role` "
                . "FROM `{$tblpx}users` `t` JOIN {$tblpx}user_roles ON {$tblpx}user_roles.id= t.user_type "
                . "WHERE status=0  ORDER BY user_type,full_name ASC";
            $result = Yii::app()->db->createCommand($sql)->queryAll();
            $listdata = CHtml::listData($result, 'userid', 'full_name');
            echo CHtml::dropDownList('projects_userid', '', $listdata, array(
                'style' => 'border: 1px solid #ccc; ',
                'ajax' => array(
                    'type' => 'POST', //request type
                    'url' => CController::createUrl('projects/projectdata'), //url to call.
                    'dataType' => 'JSON',
                    'success' => 'js:function(data){
                            jsonArr = JSON.parse(JSON.stringify(data));  //alert(jsonArr.proj_exp[0].Project1);
                            var data = jsonArr.result;//alert(data);
                            res = data;//delete data[0][0];
                            hot1.loadData(data);
                            return data;
                    }',
                    'data' => array('userid' => 'js:this.value', 'cadate' => 'js:$("#Projects_date").val()'),

                ), 'empty' => 'All'
            ));
            ?>

        <?php } else { ?>
            <input type="hidden" id="projects_userid" value="<?php echo Yii::app()->user->id; ?>" />

        <?php } ?>
        &nbsp&nbsp&nbsp&nbsp<span id="example1console" class="console"></span><span id="example1consolesucces"></span>
        Last Entry date : <span id="entrydate"></span>
    </div>



    <br>

    <div id="example1" class="hot handsontable htColumnHeaders"></div>
    <button name="save" id="save" class="btn">Save</button>

</div>
<?php //Yii::app()->clientScript->registerScript('myjavascript', ' 
?>
<script type="text/javascript">
    getExpensedata();

    ///save action starts from here
    $("#save").click(function(e) {
        $("#example1console").text("");
        $('.loading-overlay').addClass('is-active');
        var gridData = hot1.getData();
        var cleanedGridData = {};

        $.each(gridData, function(rowKey, object) {
            if (!hot1.isEmptyRow(rowKey)) cleanedGridData[rowKey] = object;
        });
        //console.log(cleanedGridData);
        var date = $("#Projects_date").val();
        $.ajax({
            url: "<?php echo $saveurl; ?>",
            data: {
                "data": cleanedGridData,
                "date": date
            },
            dataType: "json",
            type: "POST",
            success: function(res) {
                if (res.result === "ok") {
                    $('#example1consolesucces').show();
                    $('#example1console').hide();
                    $("#example1consolesucces").text("Data saved");
                    getExpensedata();

                } else {
                    $('#example1console').show();
                    $('#example1consolesucces').hide();
                    $("#example1console").text("Invalid input");
                }
            },
            error: function() {
                $('#example1console').show();
                $('#example1consolesucces').hide();
                $("#example1console").text("Save error.");
            }
        });
    });
    //save action end here

    //document.addEventListener("DOMContentLoaded", function() {
    var testremoveRenderer = function(instance, td, row, col, prop, value, cellProperties) {
        Handsontable.TextCell.renderer.apply(this, arguments);
        $(td).addClass('hiden_style');
    };
    var classRenderer = function(instance, td, row, col, prop, value, cellProperties) {
        Handsontable.TextCell.renderer.apply(this, arguments);
        $(td).addClass("clas");
    };


    var globalHeaders = ["ID", "Project", "Expense Type", "Vendors", "Works Done", "Description"];
    var globalColumns = [{
            type: "numeric",
            readOnly: true,
            renderer: testremoveRenderer
        },
        {
            type: "dropdown",
            source: <?php echo $projectlist; ?>,
            renderer: classRenderer

        },
        {
            type: "dropdown"
        },
        {
            type: "dropdown"
        },
        {
            type: "text"
        },

        {
            type: "text"
        }

    ];

    var colsToHide = [];
    colsToHide.push(0);

    var container = document.getElementById("example1"),
        data = [];
    hot1 = new Handsontable(container, {
        data: data,
        rowHeaders: false,
        contextMenu: true,
        //dataSchema: { projectid: null, expensetype: null, description:null, expense: null},
        //startRows: 5,
        //startCols: 4,
        //rowHeights: 30,
        colWidths: [30, 50, 50, 50, 60],
        colHeaders: globalHeaders,
        fillHandle: true,
        stretchH: "all",
        autoWrapRow: true,
        enterMoves: {
            row: 0,
            col: 1
        },
        columns: globalColumns,
        minSpareRows: 1,
        persistentState: true,

        afterChange: function(change, source) {
            if (change != null) {


                for (var i = 0; i < change.length; i++) {
                    var instance = this;
                    if (change[i][1] == 1) {
                        this.setDataAtCell(change[i][0], 2, "");


                        var term = change[i][3].replace(/\s/g, "");

                        if (jsonArr) {
                            var array = $.map(jsonArr.proj_exp[0][term], function(value, index) {
                                return [value];
                            });
                            var cellProperties = instance.getCellMeta(change[i][0], 2);
                            cellProperties.source = array;

                            this.render();
                        }

                    } else if (change[i][1] == 2) {

                        this.setDataAtCell(change[i][0], 3, "");


                        var term = change[i][3].replace(/\s/g, "");

                        if (jsonArr) {
                            var array = $.map(jsonArr.vendor_exp[0][term], function(value, index) {
                                return [value];
                            });
                            var cellProperties = instance.getCellMeta(change[i][0], 3);

                            cellProperties.source = array;
                            this.render();
                        }

                    }
                }
            }
        },

        beforeRemoveRow: function(index, amount) {
            var cellProperties = this.getDataAtRow(index);
            var retVal = confirm("Are you sure You want to delete this row ?");
            if (retVal == true) {
                $('.loading-overlay').addClass('is-active');
                var data = cellProperties[0];
                var data2 = index + 1;

                $.ajax({
                    url: "<?php echo Yii::app()->createAbsoluteUrl('projects/handsondelete'); ?>",
                    data: {
                        id: data
                    },
                    dataType: "json",
                    type: "POST",
                    success: function(res) {
                        if (res.result === "ok") {
                            $('#example1console').hide();
                            $("#example1consolesucces").show();
                            $("#example1consolesucces").text("Row Deleted");
                        }
                    }
                });
                return true;
            } else {
                return false;
            }
        },
        beforeKeyDown: function(e) {

            if (e.keyCode === 46) {
                var selection = this.getSelected();
                var instance = this;
                var retVal = confirm("Do you want to delete this row ?");
                if (retVal == true) {
                    var cellProperties = this.getDataAtRow(selection[0]);
                    $('.loading-overlay').addClass('is-active');
                    var data = cellProperties[0];
                    $.ajax({
                        url: "<?php echo Yii::app()->createAbsoluteUrl('projects/handsondelete'); ?>",
                        data: {
                            id: data
                        },
                        dataType: "json",
                        type: "POST",
                        success: function(res) {
                            if (res.result === "ok") {
                                $("#example1consolesucces").text("Row Deleted");
                                instance.alter("remove_row", selection[0]);
                            }
                        },
                        error: function() {
                            instance.alter("remove_row", selection[0]);
                        }
                    });
                    return true;
                } else {

                    return false;
                }

            }
        }




    });



    var res;
    var flag;
    //get expense details
    function getExpensedata() {
        var candate = $("#Projects_date").val();
        var userid = $("#projects_userid").val();
        //alert(candate);
        $('.loading-overlay').addClass('is-active');
        $.ajax({
            type: "POST",
            data: {
                cadate: candate,
                userid: userid
            },
            url: "<?php echo Yii::app()->createAbsoluteUrl('projects/projectdetails'); ?>",
            dataType: "JSON",
            success: function(data) {
                $('#entrydate').html(data.lastdate);
                jsonArr = JSON.parse(JSON.stringify(data)); //alert(jsonArr.proj_exp[0].Project1);
                var data = jsonArr.result;

                res = data; //delete data[0][0];
                hot1.loadData(data);
                return data;
            },
        });

    }
    var jsonArr;



    var curentRow = 0;
    hot1.addHook("afterRender", function() {
        flag = 0;
        var instance = this;
        var sel = instance.getSelected();
        if (sel != null) {
            var prevRow = sel[0] - 1;
            if (curentRow != prevRow) {
                var f = 0;
                curentRow = prevRow;
            }
            if (!instance.getDataAtCell(sel[0], 1)) {
                var project = this.getDataAtCell(prevRow, 1);
                var exptype = this.getDataAtCell(prevRow, 2);
                var vendor = this.getDataAtCell(prevRow, 3);
                var worksdone = this.getDataAtCell(prevRow, 4);
                var description = this.getDataAtCell(prevRow, 5);

                if ((project) && (!exptype)) {
                    hot1.validateCell(this.getDataAtCell(prevRow, 2), this.getCellMeta(prevRow, 2), function() {
                        hot1.render();
                    });

                } else if ((project) && (!worksdone)) {
                    hot1.validateCell(this.getDataAtCell(prevRow, 3), this.getCellMeta(prevRow, 3), function() {
                        hot1.render();
                    });
                } else {
                    if ((project && exptype) || (project && worksdone) && !instance.getDataAtCell(prevRow, 0)) {
                        flag = 1;
                    }
                    if (f == 0) {
                        if (((project && exptype) || (project && worksdone)) && !instance.getDataAtCell(prevRow, 0) && project) {
                            f = 1;
                            var date = $("#Projects_date").val();
                            $('#loading').show();
                            $.ajax({
                                type: "POST",
                                url: "<?php echo Yii::app()->createAbsoluteUrl('projects/progressSave'); ?>",
                                data: {
                                    projectid: project,
                                    worksdone: worksdone,
                                    exptype: exptype,
                                    vendor: vendor,
                                    description: description,
                                    date: date
                                },
                                dataType: "JSON",
                                success: function(data) {
                                    if (data.result == "ok") {
                                        $("tbody tr:nth-child(" + sel[0] + ")").addClass("savedata");
                                        window.setTimeout(function() {
                                            $("tr:nth-child(" + sel[0] + ")").removeClass("savedata");
                                        }, 10000);
                                        instance.setDataAtCell(prevRow, 0, data.id);
                                    }

                                },
                            });
                        }
                    }
                }
            }

        }
        //        if(this.getDataAtCell(0,1)){
        //	hot1.validateCell(this.getDataAtCell(0,6 ), this.getCellMeta(0, 6), function(){hot1.render(); });
        //  }

    });

    function validDate(d) {
        var bits = d.split("-");
        var t = stringToDate(d);
        return t.getFullYear() == bits[0] &&
            t.getDate() == bits[2];
    }

    // Convert string in format above to a date object
    function stringToDate(s) {
        var bits = s.split("-");
        var monthNum = monthNameToNumber(bits[1]);
        return new Date(bits[2], monthNum, bits[0]);
    }

    // Convert month names like mar or march to 
    // number, capitalisation not important
    // Month number is calendar month - 1.
    var monthNameToNumber = (function() {
        var monthNames = (
            "jan feb mar apr may jun jul aug sep oct nov dec " +
            "january february march april may june july august " +
            "september october november december"
        ).split(" ");

        return function(month) {
            var i = monthNames.length;
            month = month.toLowerCase();

            while (i--) {
                if (monthNames[i] == month) {
                    return i % 12;
                }
            }
        }
    }());

    // Given a date in above format, return
    // previous day as a date object
    function getYesterday(d) {
        d = stringToDate(d);
        d.setDate(d.getDate() - 1)
        return d;
    }

    function getTomorrow(d) {
        d = stringToDate(d);
        d.setDate(d.getDate() + 1)
        return d;
    }

    // Given a date object, format
    // per format above
    var formatDate = (function() {
        var months = "Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec".split(" ");

        function addZ(n) {
            return n < 10 ? "0" + n : "" + n;
        }
        return function(d) {
            return addZ(d.getDate()) + "-" +
                months[d.getMonth()] + "-" +
                d.getFullYear();
        }
    }());




    $("#previous").click(function() {
        var candate = $("#Projects_date").val();
        var prevdate = formatDate(getYesterday(candate));
        $("#Projects_date").val(prevdate);
        var userid = $("#projects_userid").val();
        $('.loading-overlay').addClass('is-active');
        $.ajax({
            type: "POST",
            data: {
                cadate: prevdate,
                userid: userid
            },
            url: "<?php echo  Yii::app()->createAbsoluteUrl('projects/projectdata'); ?>",
            dataType: "JSON",
            success: function(data) {
                var data = data.result;
                res = data; //delete data[0][0];

                hot1.loadData(data);

                return data;
            },
            complete: function(data) {
                var divwidth = $(".wtHider").width();
                $(".htCore").css("width", divwidth + "px");

            }

        });
        //    var d1 = new Date();
        //    if($("#Projects_date").val()==formatDate(d1)){
        var d1 = new Date();
        var dd = d1.getDate();
        var mm = d1.getMonth();
        var yyyy = d1.getFullYear();
        var months = "Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec".split(" ");
        //alert($("#Projects_date").val()+"=="+dd+"-"+months[mm]+"-"+yyyy);
        //alert($("#Projects_date").val());
        if ($("#Projects_date").val() == formatDate(d1)) {
            $("#next").hide();
        } else {
            $("#next").show();
        }


    });

    $("#next").click(function() {

        var candate = $("#Projects_date").val();
        //alert(candate);
        var nxtdate = formatDate(getTomorrow(candate));
        //alert(nxtdate);
        $("#Projects_date").val(nxtdate);
        var userid = $("#projects_userid").val();
        $('.loading-overlay').addClass('is-active');
        $.ajax({
            type: "POST",
            data: {
                cadate: nxtdate,
                userid: userid
            },
            url: "<?php echo Yii::app()->createAbsoluteUrl('projects/projectdata'); ?>",
            dataType: "JSON",
            success: function(data) {
                var data = data.result;
                res = data; //delete data[0][0];
                hot1.loadData(data);
                return data;
            },

        });
        //var d1 = new Date();
        var d1 = new Date();
        var dd = d1.getDate();
        var mm = d1.getMonth();
        var yyyy = d1.getFullYear();
        var months = "Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec".split(" ");
        if ($("#Projects_date").val() == formatDate(d1)) {
            $("#next").hide();
        }

    });
    $("#next").hide();
    $("#current").click(function() {
        var d = new Date();
        $("#Projects_date").val(formatDate(d));
        $('.loading-overlay').addClass('is-active');
        $.ajax({
            type: "POST",
            data: {
                cadate: formatDate(d)
            },
            url: "<?php echo Yii::app()->createAbsoluteUrl('projects/projectdata'); ?>",
            dataType: "JSON",
            success: function(data) {
                var data = data.result;
                res = data; //delete data[0][0];
                hot1.loadData(data);
                return data;
            },

        });
        $("#next").hide();
    });



    function formatDate(date) {
        var pdate = new Date(date),
            monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
                "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
            ];
        month = monthNames[pdate.getMonth()];
        day = "" + pdate.getDate(),
            year = pdate.getFullYear();

        //if (month.length < 2) month = "0" + month;
        if (day.length < 2) day = "0" + day;

        return [day, month, year].join("-");

    }

    document.addEventListener("DOMContentLoaded", function() {

        function bindDumpButton() {
            if (typeof Handsontable === "undefined") {
                return;
            }

            Handsontable.Dom.addEvent(document.body, "click", function(e) {

                var element = e.target || e.srcElement;

                if (element.nodeName == "BUTTON" && element.name == "dump") {
                    var name = element.getAttribute("data-dump");
                    var instance = element.getAttribute("data-instance");
                    var hot = window[instance];
                    console.log("data of " + name, hot.getData());
                }
            });
        }
        bindDumpButton();



    });

    function changedate() {
        var candate = $("#Projects_date").val();
        var userid = $("#projects_userid").val();
        $('.loading-overlay').addClass('is-active');
        $.ajax({
            type: "POST",
            data: {
                cadate: candate,
                userid: userid
            },
            url: "<?php echo Yii::app()->createAbsoluteUrl('projects/projectdata'); ?>",
            dataType: "JSON",
            success: function(data) { //alert(data.proj_exp);
                jsonArr = JSON.parse(JSON.stringify(data)); //alert(jsonArr.proj_exp[0].Project1);
                var data = jsonArr.result; //alert(data);
                res = data; //delete data[0][0];
                hot1.loadData(data);
                return data;
            },
        });
    }
    $(document).ready(function() {
        $('#loading').hide();
    });
    $(document).ajaxComplete(function() {
        $('.loading-overlay').removeClass('is-active');
        $('#loading').hide();
    });
</script>