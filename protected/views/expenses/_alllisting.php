<?php
                if($index==0){
                ?>
                <thead class="entry-table">
                    <tr>
                        <th></th>
                        <th>Sl No.</th>
                        <th>Section</th>
                        <th>Company</th>
                        <th>Project</th>
                        <th>Subcontractor</th>
                        <th>Bill Number</th>
                        <th>Invoice Number</th>
                        <th>Date</th>
                        <th>Expense Head / Transaction Head</th>
                        <th>Vendor</th>
                        <th>Expense Type / Transaction Type</th>
                        <th>Petty Cash By</th>
                        <th>Bank</th>
                        <th>Cheque No</th>
                        <th>Description</th>
                        <th>Amount</th>
                        <th>Tax</th>
                        <th>Total</th>
                        <th>Amount Paid</th>
                        <th>TDS</th>
                        <th>Receipt Type</th>
                        <th>Receipt</th>
                        <th>Purchase Type / Payment Type</th>
                        <th>Amount To Beneficiary</th>
                        <th>Paid To</th>
                        
                    </tr>
                </thead>
                <?php } ?>
                <?php
                    $i = $index+1;
                    if($data['section'] == 'Daybook'){
                        $class = 'duplicaterow';
                    }else if($data['section'] == 'Dailyexpense'){
                        $class = 'duplicaterow2';
                    }else if($data['section'] == 'Subcontractor Payment'){
                        $class = 'duplicaterow3';
                    }else if($data['section'] == 'Vendor Payment'){
                        $class = 'duplicaterow4';
                    }else{
                        $class = 'duplicaterow5';
                    }

                    $bg_color ="";
                    if($data['duplicate_delete_status'] == '1'){
                        $bg_color = "bg_pending";
                    }

                ?>
                    <tr id="duplicaterow<?php echo $i; ?>" title="Click here to view <?php echo $data["rowcount"]; ?> similar entries." class="<?php echo $bg_color ?>" data-id="<?php echo $i; ?>">
                        <td>
                            <span class="icon icon-options-vertical popover-test" data-toggle="popover" data-placement="right" type="button" data-html="true" style="cursor: pointer;"></span>
                            <div class="popover-content hide">
                                <ul class="tooltip-hiden d-flex">                                
                                    <li><button id="<?php echo $index; ?>" class="btn btn-xs btn-default btn-ignore" data-section = "<?php echo $data['section']?>" data-id= "<?php echo $data['id'] ?>">Ignore</button></li>
                                    <?php  if($data['duplicate_delete_status'] == '0'){ ?> 
                                        &nbsp;                              
                                    <li><button id="<?php echo $index; ?>" class="btn btn-xs btn-default btn-delete"  data-section = "<?php echo $data['section']?>" data-id= "<?php echo $data['id'] ?>">Delete</button></li> 
                                    <?php } ?>                              
                                </ul>
                            </div>
                        </td>
                        <td class="left_fix  <?php echo $class . ' ' . $bg_color; ?>"  data-id="<?php echo $i; ?>"><?php echo $i; ?></td>
                        <td class="left_fix <?php echo $class . ' ' . $bg_color ?>"><?php echo $data['section']; ?></td>
                        <td class="left_fix <?php echo $class . ' ' . $bg_color ?>"><?php echo $data['company']; ?></td>

                        <td class="left_fix <?php echo $class . ' ' . $bg_color ?>"><?php echo $data['project']; ?></td>
                        <td class="left_fix  <?php echo $class . ' ' . $bg_color ?>"><?php echo $data['subcontractor']; ?></td>
                        <td class="left_fix  <?php echo $class . ' ' . $bg_color ?>"><?php echo $data['billno']; ?></td>
                        <td class="left_fix  <?php echo $class . ' ' . $bg_color ?>"><?php echo $data['invoiceno']; ?></td>
                        <td class="left_fix nowrap  <?php echo $class . ' ' . $bg_color ?>"><?php echo date("d-m-Y", strtotime($data['date'])); ?></td>
                        <td class="left_fix  <?php echo $class . ' ' . $bg_color ?>"><?php echo $data['expense_head']; ?></td>
                        <td class="left_fix  <?php echo $class . ' ' . $bg_color ?>"><?php echo $data['vendor']; ?></td>
                        <td class="left_fix  <?php echo $class . ' ' . $bg_color ?>"><?php echo $data['expense_type']; ?></td>
                        <td class="left_fix  <?php echo $class . ' ' . $bg_color ?>"><?php echo $data['pettycash_by']; ?></td>
                        <td class="left_fix  <?php echo $class . ' ' . $bg_color ?>"><?php echo $data['Bank']; ?></td>
                        <td class="left_fix  <?php echo $class . ' ' . $bg_color ?>"><?php echo $data['check']; ?></td>
                        <td class="left_fix  <?php echo $class . ' ' . $bg_color ?>"><?php echo $data['description']; ?></td>
                        <td class="left_fix  <?php echo $class . ' ' . $bg_color ?>"><?php echo Controller::money_format_inr($data['amount'],2); ?></td>
                        <td class="left_fix  <?php echo $class . ' ' . $bg_color ?>"><?php echo $data['tax']; ?></td>
                        <td class="left_fix  <?php echo $class . ' ' . $bg_color ?>"><?php echo Controller::money_format_inr($data['total_amount'],2); ?></td>
                        <td class="left_fix  <?php echo $class . ' ' . $bg_color ?>"><?php echo Controller::money_format_inr($data['paid_amount'],2); ?></td>
                        <td class="left_fix  <?php echo $class . ' ' . $bg_color ?>"><?php echo $data['tds']; ?></td>
                        <td class="left_fix  <?php echo $class . ' ' . $bg_color ?>"><?php echo $data['receipt_type']; ?></td>
                        <td class="left_fix  <?php echo $class . ' ' . $bg_color ?>"><?php echo $data['receipt']; ?></td>
                        <td class="left_fix  <?php echo $class . ' ' . $bg_color ?>"><?php echo $data['purchase_type']; ?></td>
                        <td class="left_fix  <?php echo $class . ' ' . $bg_color ?>"><?php echo $data['beneficiary_amount']; ?></td>
                        <td class="left_fix  <?php echo $class . ' ' . $bg_color ?>"><?php echo $data['paid_to']; ?></td>
                        
                        <input type="hidden" name="vendor[]" id="vendor<?php echo $i; ?>" value="<?php echo $data["vendor_id"]; ?>"/>
                        <input type="hidden" name="subcontractor[]" id="subcontractor<?php echo $i; ?>" value="<?php echo $data["subcontractor_id"]; ?>"/>
                        <input type="hidden" name="projectid[]" id="projectid<?php echo $i; ?>" value="<?php echo $data["projectid"]; ?>"/>
                        <input type="hidden" name="amount[]" id="amount<?php echo $i; ?>" value="<?php echo $data["total_amount"]; ?>"/>
                        <input type="hidden" name="paid[]" id="paid<?php echo $i; ?>" value="<?php echo $data["paid_amount"]; ?>"/>
                        <input type="hidden" name="date[]" id="date<?php echo $i; ?>" value="<?php echo $data["date"]; ?>"/>
                        <input type="hidden" name="description[]" id="description<?php echo $i; ?>" value="<?php echo $data["description"]; ?>"/>
                        <input type="hidden" name="billno[]" id="billno<?php echo $i; ?>" value="<?php echo $data["billno"]; ?>"/>
                    </tr>
               
