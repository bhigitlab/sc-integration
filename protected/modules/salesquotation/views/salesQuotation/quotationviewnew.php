<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/validation/jquery.validate.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/validation/additional-methods.js"></script>

<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/message/css/jquery.message.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/message/js/jquery.message.js"></script>
<style>
    .lefttdiv {
        float: left;
    }

    .purchase-title {
        border-bottom: 1px solid #ddd;
    }

    .purchase_items h3 {
        font-size: 18px;
        color: #333;
        margin: 0px;
        padding: 0px;
        text-align: left;
        padding-bottom: 10px;
    }

    .purchase_items {
        padding: 15px;
        box-shadow: 0 0 13px 1px rgba(0, 0, 0, 0.25);
    }

    .purchaseitem {
        display: inline-block;
        margin-right: 20px;
    }

    .purchaseitem last-child {
        margin-right: 0px;
    }

    .purchaseitem label {
        font-size: 12px;
    }

    .remark {
        display: none;
    }

    .padding-box {
        padding: 3px 0px;
        min-height: 17px;
        display: inline-block;
    }

    th {
        height: auto;
    }

    .quantity,
    .rate {
        max-width: 80px;
    }

    .text_align {

        text-align: center;
    }


    *:focus {
        border: 1px solid #333;
        box-shadow: 0 0 6px 1px rgba(0, 0, 0, 0.25);
    }

    .text-right {
        text-align: right;
    }

    .light_bg_grey {
        background: #88888838 !important;
    }
    table.item_table tbody tr.catehead td {
        background-color: #c5c5dd;
        font-weight: 600;
        color: #000;
        padding: 15px 10px;
    }
</style>
<script>
    $(function () {
        $("#datepicker").datepicker({
            dateFormat: 'dd-mm-yy'
        }).datepicker("setDate", new Date());

    });
</script>

<br /><br />


<div class="container">
    <div class="loading-overlay">
        <span class="fa fa-spinner fa-3x fa-spin"></span>
    </div>
    <div class="invoicemaindiv">
        <div id="loading"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/ajax-loader.gif"></div>
        <div class="pull-right">
            <a style="cursor: pointer" id="upload_image" class="pdfbtn1 pdf_excel" 
            href="<?php echo $this->createUrl("salesQuotation/uploadImages&qtid=".$qtid."&rev_no=".$rev_no) ?>">
                UPLOAD IMAGES
            </a>
            <a style="cursor: pointer" id="download" class="pdfbtn1 pdf_excel">
                <div class="save_pdf"></div>SAVE AS PDF
            </a>
            <a  data-id="<?php echo $qtid; ?>"  data-rev="<?php echo $rev_no; ?>" style="cursor: pointer" id="sendmail" class="pdf_excel sendmail">
                SEND MAIL
            </a>
        </div>

        <h2 class="purchase-title">Quotation</h2>
        <br>
        <?php
            $rev_no = $_GET['rev_no'];
        ?>
        <div id="errormessage"></div>
        <div id="send_form" style="display:none;">
            <div class="panel panel-gray">
                <div class="panel-heading form-head">
                    <h3 class="panel-title">Send Mail</h3>
                </div>
                <form id="form_send">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-6">
                                <input type="hidden" name="quotation_id" id="quotation_id" value="<?php echo $qtid; ?>">
                                <input type="hidden" name="revision_no" id="revision_no" value="<?php echo $rev_no; ?>">
                                <input type="hidden" name="pdf_path" id="pdf_path" value="">
                                <input type="hidden" name="pdf_name" id="pdf_name" value="">
                                <div class="form-group">
                                    <label for="ProjectType_project_type" class="required">To</label>
                                    <input id="mail_to" name="mail_to" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">

                                <div class="form-group">
                                    <label for="ProjectType_project_type" class="required">Message</label>
                                    <textarea id="mail_content" name="mail_content" class="form-control"></textarea>
                                </div>
                            </div>
                            <?php
                            if (!empty($settings['po_email_from'])) {
                                ?>
                                <div class="col-md-6">
                                    <input type="checkbox" class="form-check-input change_val " checked='true' id="send_copy" name="send_copy">
                                    <label class="form-check-label" for="inclusive">Send copy to (<?php echo $settings['po_email_from']; ?>)
                                    </label>
                                </div>
                            <?php } ?>
                            <div class="col-md-6">
                                <label style="display:block;">&nbsp;</label>
                                <button class="btn btn-info btn-sm send_btn">Send</button>
                                <?php echo CHtml::ResetButton('Close', array('onclick' => 'closeaction(this,event);', 'class' => 'btn btn-sm')); ?>
                                <div id="loader1" style="display:none;"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/ajax-loader.gif"></div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <form id="pdfvals1" method="post" action="">
            <input type="hidden" name="purchaseview">
            <div class="row">
                <div class="col-md-2">
                    <label>COMPANY : </label>
                    <?php
                    $company = Controller::getCompanyName($model->company_id);
                    echo $company;
                    ?>
                </div>
                <div class="col-md-2">
                    <label>CLIENT : </label>
                    <?php echo $model->client_name; ?>
                </div>
                <div class="col-md-2">
                    <label>DATE : </label>
                    <?php echo isset($model->date_quotation) ? date('d-m-Y', strtotime($model->date_quotation)) : ''; ?>
                </div>
                <div class="col-md-2">
                    <label>Quotation No : </label>
                    <?php echo isset($model->invoice_no) ? $model->invoice_no : ''; ?>
                </div>
                <div class="col-md-2">
                    <label>Revision No : </label>
                    <?php echo isset($_GET['rev_no']) ? $_GET['rev_no'] : ''; ?>
                </div>
            </div>
            <div class="clearfix"></div>
            <br><br>
            <div id="table-scroll" class="table-scroll">
                <div id="faux-table" class="faux-table" aria="hidden"></div>
                <div id="table-wrap" class="table-wrap">
                    <div class="table-responsive">
                        <table cellpadding="10" class="item_table">
                            <thead>
                                <tr>

                                    <th>S.NO</th>
                                    <th>DESCRIPTION OF ITEM</th>
                                    <?php
                                    if ($model->template_type == '5') { ?>
                                    <th>HSN CODE</th>
                                    <?php } ?>
                                    <th>UNIT</th>
                                    <th>LENGTH</th>
                                    <th>WIDTH</th>
                                    <th>QTY</th>                                    
                                    <th>MRP</th>
                                    <th>AFTER DISCOUNT</th>
                                    

                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $sql = 'SELECT id,unit,section_name,hsn_code,quantity_nos,quantity,mrp,amount_after_discount FROM `jp_quotation_section` '
                                        . 'WHERE `qtn_id` = ' . $qtid
                                        . ' AND revision_no = "' . $rev_no . '"';
                                $sectionArray = Yii::app()->db->createCommand($sql)->queryAll();
                                $i = 1;
                                foreach ($sectionArray as $key => $value1) {
                                    $sql = 'SELECT id,unit,section_id,qid,category_label,hsn_code,quantity_nos,quantity,mrp,amount_after_discount FROM `jp_quotation_gen_category` '
                                            . 'WHERE `qid` = ' . $qtid . ' AND `section_id` = ' . $value1["id"]
                                            . ' AND revision_no = "' . $rev_no . '"';
                                    $subsectionArray = Yii::app()->db->createCommand($sql)->queryAll();
                                    $unit = Controller::getUnitName($value1['unit']);
                                    ?>
                                    <tr>
                                    <?php if ($model->template_type == '5') { ?>
                                        <th colspan="<?php echo empty($subsectionArray) ? '2' : '8' ?>" style="background:#c00000;color:#fff;"><?php echo $value1['section_name'] ?></th>
                                    <?php }else{ ?>
                                        <th colspan="<?php echo empty($subsectionArray) ? '2' : '8' ?>" style="background:#c00000;color:#fff;"><?php echo $value1['section_name'] ?></th>
                                    <?php } ?>
                                        <?php if (empty($subsectionArray)) { ?>
                                            <?php
                                            if ($model->template_type == '5') { ?>
                                            <td style="background:#c00000;color:#fff;"><?php echo $value1['hsn_code'] ?></td>
                                            <?php } ?>
                                            <td style="background:#c00000;color:#fff;"><?php  if($value1['quantity_nos']!=''){
                                                                                                    echo "nos";
                                                                                                }else{
                                                                                                    echo isset($unit) ? $unit : "";
                                                                                                } ?>
                                            </td>
                                            <td style="background:#c00000;color:#fff;"><?php echo ''; ?></td>
                                            <td style="background:#c00000;color:#fff;"><?php echo ''; ?></td>
                                            <td style="background:#c00000;color:#fff;"><?php if($model->template_type == '5'){ echo $value1['quantity'];
                                                                                            }else if($value1['quantity_nos']!=''){
                                                                                                echo $value1['quantity_nos'];
                                                                                            }else{
                                                                                                echo $value1['quantity'];
                                                                                            } ?>
                                            </td>
                                            <td style="background:#c00000;color:#fff;"><?php echo $value1['mrp'] ?></td>
                                            <td style="background:#c00000;color:#fff;"><?php echo $value1['amount_after_discount'] ?></td>
                                            
                                        <?php } ?>
                                        
                                    </tr>
                                    <?php
                                    foreach ($subsectionArray as $key => $value2) {
                                        $sql = 'SELECT worktype_label,id,unit,hsn_code,quantity_nos,quantity,mrp,amount_after_discount,description,shutterwork_description,carcass_description,length,width FROM `jp_sales_quotation` WHERE `parent_type` = 0 '
                                                . ' AND `parent_id` = ' . $value2['id']
                                                . ' AND revision_no="' . $_GET['rev_no'] . '" AND `deleted_status` = 1';
                                        $itemArray = Yii::app()->db->createCommand($sql)->queryAll();

                                        $sql = 'SELECT worktype_label,id,unit,hsn_code,quantity_nos,quantity,mrp,amount_after_discount,description FROM `jp_quotation_gen_worktype` '
                                                . ' WHERE `qid` = ' . $value2["qid"]
                                                . ' AND `section_id` = ' . $value2["section_id"]
                                                . ' AND `category_label_id` = ' . $value2["id"]
                                                . ' AND revision_no = "' . $rev_no . '"';
                                        $subitemArray = Yii::app()->db->createCommand($sql)->queryAll();
                                        $unit = Controller::getUnitName($value2['unit']);
                                        ?>
                                        <tr>
                                        
                                            <th style="background:#c5d9f1;"><?php echo $i; ?></th>

                                            <th colspan="<?php echo ($model->template_type == '5') ? '5':empty($subitemArray) ? '1' : '7' ?>" style="background:#c5d9f1;"><?php echo $value2['category_label'] ?></th>
                                           
                                            
                                            <?php if (empty($subitemArray)) { ?>
                                                <?php
                                               
                                                if ($model->template_type == '5')  { ?>
                                                <td style="background:#c5d9f1;"><?php echo $value2['hsn_code'] ?></td>
                                                <?php } ?>
                                                <td  style="background:#c5d9f1;"><?php if($value2['quantity_nos']!=''){
                                                                                            echo "nos";
                                                                                        }else{
                                                                                            echo isset($unit) ? $unit : "";
                                                                                        } ?>
                                                </td>
                                                <td  style="background:#c5d9f1;"><?php echo ""; ?></td>
                                                <td  style="background:#c5d9f1;"><?php echo ""; ?></td>
                                                <td  style="background:#c5d9f1;"><?php if($model->template_type == '5'){ echo $value2['quantity'];
                                                                                        }else if($value2['quantity_nos']!=''){
                                                                                            echo $value2['quantity_nos'];
                                                                                        }else{
                                                                                            echo $value2['quantity'];
                                                                                        } ?>
                                                </td>
                                                <td  style="background:#c5d9f1;"><?php echo $value2['mrp'] ?></td>
                                                <td  style="background:#c5d9f1;"><?php echo $value2['amount_after_discount'] ?></td>
                                                
                                            <?php } ?>
                                            
                                        </tr>

                                        <?php
                                        foreach ($itemArray as $key => $value) {
                                            ?>
                                            <tr>
                                                <td><?php echo '*' ?></td>
                                                <td colspan="8"><?php echo $value['worktype_label'] ?></td>
                                            </tr>

                                        <?php }
                                        ?>
                                        <?php
                                        $letters = range('a', 'z');
                                        $k = 0;
                                        foreach ($subitemArray as $key => $value) {
                                            $sql = 'SELECT unit,amount_after_discount,mrp,description,quantity,shutterwork_description,carcass_description,worktype_label,hsn_code,quantity_nos,length,width FROM `jp_sales_quotation` '
                                                    . 'WHERE `parent_type` = 1 AND `parent_id` = ' . $value['id']
                                                    . ' AND revision_no="' . $_GET['rev_no'] . '" AND `deleted_status` = 1';
                                            $wrktypeitemArray = Yii::app()->db->createCommand($sql)->queryAll();
                                            $unit = Controller::getUnitName($value['unit']);
                                            ?>
                                            <tr class="td_group">
                                                <th><?php echo $letters[$k]; ?></th>
                                                <td colspan="<?php echo empty($wrktypeitemArray) ? '1' : '7'?>">
                                                    <b><?php echo $value['worktype_label'] ?></b>
                                                </td>
                                                <?php if (empty($wrktypeitemArray)) { ?>
                                                    <?php
                                                    
                                                    if ($model->template_type == '5') { ?>
                                                    <td><?php echo $value['hsn_code'] ?></td>
                                                    <?php } ?>
                                                    <td><?php  if($value['quantity_nos']!=''){
                                                                    echo "nos";
                                                                }else{
                                                                    echo isset($unit) ? $unit : "";
                                                                } ?>
                                                    </td>
                                                    <td></td>
                                                    <td></td>
                                                    <td><?php if($model->template_type == '5'){ echo $value['quantity'];
                                                                }else if($value['quantity_nos']!=''){
                                                                    echo $value['quantity_nos'];
                                                                }else{
                                                                    echo $value['quantity'];
                                                                } ?>
                                                    </td>
                                                    <td><?php echo $value['mrp'] ?></td>
                                                    <td><?php echo $value['amount_after_discount'] ?></td>
                                                                                                       
                                                <?php } ?>
                                                
                                            </tr>
                                            <?php if ($value['description'] != "") { ?>
                                                <tr class="td_group">
                                                    <th>*</th>
                                                    <td colspan="7"><?php echo $value['description'] ?></td>
                                                    
                                                </tr>
                                                <?php
                                            }
                                            $sum = 0;//echo "<pre>";print_r($wrktypeitemArray);die;
                                            foreach ($wrktypeitemArray as $key => $value) {
                                                $sum += $value['amount_after_discount'];
                                                $unit = Controller::getUnitName($value['unit']);
                                                $desc = ($value['description'] !="")?" - ".$value['description']:"";
                                                if($value['description']=="" && ($value['shutterwork_description'] !="" || $value['carcass_description']!="")){
                                                    $desc = " - <b>Shutter:</b> ".$value['shutterwork_description']." , <b>Carcass:</b> ".$value['carcass_description'];
                                                }
                                                ?>
                                                <tr class="td_group">
                                                    <th><?php echo '*' ?></th>
                                                    <td><?php echo $value['worktype_label'].$desc ?></td>
                                                    <?php
                                                    $activeProjectTemplate = Controller::getActiveTemplate();
                                                    if ($model->template_type == '5')  { ?>
                                                    <td><?php echo $value['hsn_code'] ?></td>
                                                    <?php } ?>
                                                    <td><?php if($value['quantity_nos']!=''){
                                                                    echo "nos";
                                                                }else{
                                                                    echo isset($unit) ? $unit: "";
                                                                } ?>
                                                    </td>
                                                    <td class="text-right"><?php echo (floatval($value['length'])>0)?floatval($value['length']):$value['length']; ?></td>
                                                    <td class="text-right"><?php echo (floatval($value['width'])>0)?floatval($value['width']):$value['width']; ?></td>
                                                    <td><?php if($model->template_type == '5'){ echo $value['quantity'];
                                                        }else if($value['quantity_nos']!=''){
                                                            echo $value['quantity_nos'];
                                                        }else{
                                                            echo $value['quantity'];
                                                        } ?>
                                                    </td>
                                                    <td class="text-right"><?php echo $value['mrp'] ?></td>
                                                    <td class="text-right"><?php echo $value['amount_after_discount'] ?></td>
                                                    
                                                </tr>

                                                <?php
                                            }
                                            if (!empty($wrktypeitemArray)) {
                                                ?>
                                                <?php
                                            } $k++;
                                        }
                                        ?>
                                        <?php
                                        $i++;
                                        ?>
                                        
                                        <?php
                                    }
                                    
                                    ?>
                                    
                                <?php } ?>
                                
                            </tbody>  
                            <tfoot>
                                
                                <tr>
                                    <th colspan="<?php echo ($model->template_type == '5')? '8':'7' ?>" class="text-right">MRP</th>
                                    <th class="text-right">
                                        <?php
                                        //$total_amount = Controller::getMrpSum($qtid, $_GET['rev_no'],NULL);
                                        $total_amount = Controller::getQuotationSum($qtid, $_GET['rev_no'],NULL,1);
                                        echo Controller::money_format_inr($total_amount, 2);
                                        ?>
                                    </th>
                                </tr><tr>
                                <th colspan="<?php echo ($model->template_type == '5')? '8':'7' ?>" class="text-right">AFTER DISCOUNT</th>
                                    <th class="text-right">
                                        <?php
                                        $total_amount = Controller::getQuotationSum($qtid, $_GET['rev_no'],NULL);
                                        echo Controller::money_format_inr($total_amount, 2);
                                        ?>
                                    </th>
                                </tr>
                                <tr>
                                <?php
                                $revision_model = QuotationRevision::model()->getRevisionData($model->id, $model->revision_no);
                                         $total_tax = $revision_model['sgst_amount']+
                                        $revision_model['cgst_amount']+
                                        $revision_model['igst_amount'];

                                        $total_tax_percent = $revision_model['sgst_percent']+
                                        $revision_model['cgst_percent']+
                                        $revision_model['igst_percent'];


                                        $total_amoun_withtax = $total_amount + $total_tax;
                                    ?>
                                    <th colspan="<?php echo ($model->template_type == '5')? '7':'7' ?>" class="text-right">GST  <?php if($total_tax_percent >0) {
                                    echo "(".$total_tax_percent."%)";
                                }else{
                                    echo "";
                                } ?></th>
                                    <th  class="text-right">
                                    
                                        <?php
                                        echo Controller::money_format_inr($total_amoun_withtax, 2);//total_amoun_withtax
                                        ?>
                                    </th>
                                </tr>
                                <tr>
                                    <th colspan="7" class="text-right">Special Discount</th>
                                    <th class="text-right">
                                    <?php 
                                        $discount_amount = ($total_amoun_withtax * $revision_model['discount_precent']) / 100;
                                        $discount_amount_new =((float)$revision_model['total_after_additionl_discount']>0) ?($total_amoun_withtax -(float)$revision_model['total_after_additionl_discount']):0;
                                        echo Controller::money_format_inr($discount_amount_new , 2);                                        
                                    ?>
                                        
                                    </th>
                                </tr>
                                
                                <tr>
                                    <th colspan="<?php echo ($model->template_type == '5')? '7':'7' ?>" class="text-right">Total Payable Amount</th>
                                    <th class="text-right">
                                        <?php
                                        $total_after_discount = $total_amoun_withtax - $discount_amount;
                                        if($revision_model['total_after_additionl_discount']!=NULL){
                                            $total_after_discount = $revision_model['total_after_additionl_discount'];
                                        }
                                        echo Controller::money_format_inr($total_after_discount , 2);                                        
                                        ?>
                                    </th>
                                </tr>
                                
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
            <div style="padding-right: 0px;" class="col-md-offset-9 col-md-3 col-sm-offset-8 col-sm-4 col-xs-offset-6 col-xs-6">
            </div>
            <br><br>
        </form>
    </div>
    <style>
        .error_message {
            color: red;
        }

        a.pdf_excel {
            background-color: #6a8ec7;
            display: inline-block;
            padding: 8px;
            color: #fff;
            border: 1px solid #6a8ec8;
        }

        .invoicemaindiv th,
        .invoicemaindiv td {
            padding: 10px;
        }
    </style>


    <script>

        $(document).ready(function () {
            $('#loading').hide();
        })
        function closeaction() {
            $('#send_form').slideUp(500);
        }

        $('#sendmail').click(function () {
            $('#send_form').slideDown();
        });

        $("#form_send").validate({
            rules: {
                mail_content: {
                    required: true,
                },
                mail_to: {
                    required: true,
                    email: true
                },
            },
            messages: {
                mail_content: {
                    required: "Please enter mesage",
                },
                mail_to: {
                    required: "Please enter email",
                    email: "Please enter valid email"
                }
            },
        });

        $('.send_btn').click(function (e) {
            e.preventDefault();
            var data = $('#form_send').serialize();
            if ($("#form_send").valid()) {
                $('#loader').css('display', 'inline-block');
                if ($("#send_copy").prop('checked') == true) {
                    var send_copy = 1;
                } else {
                    var send_copy = 0;
                }
                var qid = $('#quotation_id').val();
                var revision_no = $('#revision_no').val();
                var mail_to = $('#mail_to').val();
                var mail_content = $('#mail_content').val();
                var pdf_path = $('#pdf_path').val();
                var pdf_name = $('#pdf_name').val();
                $('.send_btn').prop('disabled', true)
                $('.loading-overlay').addClass('is-active');
                $.ajax({
                    url: '<?php echo Yii::app()->createAbsoluteUrl('salesquotation/salesQuotation/sendattachment'); ?>',
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        qid: qid,
                        revision_no: revision_no,
                        mail_to: mail_to,
                        mail_content: mail_content,
                        pdf_path: pdf_path,
                        pdf_name: pdf_name,
                        send_copy: send_copy
                    },
                    success: function (response) {
                        if (response.status == '1') {
                            $('#loader').hide();
                            $('.send_btn').prop('disabled', false)
                            $('#send_form').slideUp(500);
                            $("#form_send").trigger('reset');
                            $("#errormessage").show()
                                    .html('<div class="alert alert-success">' + response.message + '</div>')
                                    .fadeOut(10000);
                        } else {
                            $('#loader').hide();
                            $('.send_btn').prop('disabled', false)
                            $("#errormessage").show()
                                    .html('<div class="alert alert-danger">' + response.message + '</div>')
                                    .fadeOut(10000);
                        }
                    }
                })
            }
        })

        $(".sendmail").click(function () {
            var q_id = $(this).attr("data-id");
            var revision_no = $(this).attr("data-rev");
            $('#loading').show();
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: '<?php echo Yii::app()->createUrl('salesquotation/salesQuotation/pdfgeneration'); ?>',
                data: {
                    q_id: q_id,
                    revision_no: revision_no,
                },
                success: function (response) {
                    $('#pdf_path').val(response.path);
                    $('#pdf_name').val(response.name);
                }
            })
        })
        $(function () {
            $('.pdfbtn1').click(function () {
                $("form#pdfvals1").attr('action', "<?php echo $this->createAbsoluteUrl('SalesQuotation/SaveQuotation', array('qid' => $model['id'], 'rev_no' => $rev_no));
                                ?>");
                $("form#pdfvals1").submit();
            });
        });


    </script>

</div>

<style>
    .table-scroll {
        position: relative;
        max-width: 1280px;
        width: 100%;
        margin: auto;
        display: table;
    }

    .table-wrap {
        width: 100%;
        display: block;
        overflow: auto;
        position: relative;
        z-index: 1;
        border: 1px solid #ddd;
    }

    .table-wrap.fixedON,
    .table-wrap.fixedON table,
    .faux-table table {
        height: 380px;
        /* match heights*/
    }

    .table-scroll table {
        width: 100%;
        margin: auto;
        border-collapse: collapse;
        border-spacing: 0;
        border: 1px solid #ddd;
    }

    .table-scroll th,
    .table-scroll td {
        padding: 5px 10px;
        border: 1px solid #ddd;
        background: #fff;
        vertical-align: middle;
    }
    h5{
        margin: 10px 0px !important;
    }

    .faux-table table {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        pointer-events: none;
    }

    .faux-table table tbody {
        visibility: hidden;
    }

    /* shrink cells in cloned table so that the table height is exactly 300px so that the header and footer appear fixed */
    .faux-table table tbody th,
    .faux-table table tbody td {
        padding-top: 0;
        padding-bottom: 0;
        border-top: none;
        border-bottom: none;
        line-height: 0.1;
    }

    .faux-table table tbody tr+tr th,
    .faux-table tbody tr+tr td {
        line-height: 0;
    }

    .faux-table thead th,
    .faux-table tfoot th,
    .faux-table tfoot td,
    .table-wrap thead th,
    .table-wrap tfoot th,
    .table-wrap tfoot td {
        background: #eee;
    }

    .faux-table {
        position: absolute;
        top: 0;
        right: 0;
        left: 0;
        bottom: 0;
        overflow-y: scroll;
    }

    .faux-table thead,
    .faux-table tfoot,
    .faux-table thead th,
    .faux-table tfoot th,
    .faux-table tfoot td {
        position: relative;
        z-index: 2;
    }

    /* ie bug */
    .table-scroll table thead tr,
    .table-scroll table thead tr th,
    .table-scroll table tfoot tr,
    .table-scroll table tfoot tr th,
    .table-scroll table tfoot tr td {
        height: 1px;
    }
</style>
<script>
    (function () {
        var mainTable = document.getElementById("main-table");
        if (mainTable !== null) {
            var tableHeight = mainTable.offsetHeight;
            if (tableHeight > 380) {
                text - right
                var fauxTable = document.getElementById("faux-table");
                document.getElementById("table-wrap").className += ' ' + 'fixedON';
                var clonedElement = mainTable.cloneNode(true);
                clonedElement.id = "";
                fauxTable.appendChild(clonedElement);
            }
        }
    })();

    $(document).ajaxComplete(function () {
        $('.loading-overlay').removeClass('is-active');
        $('#loading').hide();
        $('#loader1').hide();
    });
</script>