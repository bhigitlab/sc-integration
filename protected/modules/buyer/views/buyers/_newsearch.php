<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.min.js"></script>

<div class="page_filter clearfix">

		<?php $form=$this->beginWidget('CActiveForm', array(
			'action'=>Yii::app()->createUrl($this->route),
			'method'=>'get',
		)); ?>
		<div class="filter_elem">
				<?php
            $user = Users::model()->findByPk(Yii::app()->user->id);
            $arrVal = explode(',', $user->company_id);
            $newQuery = "";
            foreach($arrVal as $arr) {
                if ($newQuery) $newQuery .= ' OR';
                $newQuery .= " FIND_IN_SET('".$arr."', company_id)";
            }
          echo $form->dropDownList($model, 'project_id', CHtml::listData(Projects::model()->findAll(array(
							'select' => array('pid, name'),
							'order' => 'name',
							'condition' =>'('.$newQuery.')',
							//'condition' => 'created_by='.Yii::app()->user->id,
							//"condition" => 'pid in (select projectid from project_assign where userid=' . Yii::app()->user->id . ')',
							'distinct' => true
						)), 'pid', 'name'), array('class'=>'form-control select_box','empty' => 'Select project', 'id' => 'projectid'));
				?>
			</div>
			<div class="filter_elem">
				<?php echo CHtml::activeTextField($model, 'fromdate', array('class'=>'form-control','placeholder'=>'Date From','size'=>10,'autocomplete'=>'off','readonly' => true,'value'=>isset($_REQUEST['Invoice']['fromdate']) ? $_REQUEST['Invoice']['fromdate']: '')); ?>
			</div>
			<div class="filter_elem">
				<?php echo CHtml::activeTextField($model, 'todate', array('class'=>'form-control','placeholder'=>'Date To','size'=>10, 'autocomplete'=>'off', 'readonly' => true,'value'=>isset($_REQUEST['Invoice']['todate']) ? $_REQUEST['Invoice']['todate']: '')); ?>
			</div>
			<div class="filter_elem filter_btns">
           <?php echo CHtml::submitButton('Go'); ?>
           <?php echo CHtml::resetButton('Clear', array('onclick' => 'javascript:location.href="'. $this->createUrl('admin').'"')); ?>
			</div>
               <?php $this->endWidget(); ?>
	</div>
<script>
$(".select_box").select2();
    $( function() {
            $( "#Invoice_fromdate" ).datepicker({
                dateFormat: 'dd-mm-yy'
            });
            $( "#Invoice_todate" ).datepicker({
                dateFormat: 'dd-mm-yy'
            });
            $("#Invoice_fromdate").change(function() {
                $("#Invoice_todate").datepicker('option', 'minDate', $(this).val());
            });
            $("#Invoice_todate").change(function() {
                $("#Invoice_fromdate").datepicker('option', 'maxDate', $(this).val());
            });
    } );

</script>
