<?php

/**
 * This is the model class for table "{{warehouse_category}}".
 *
 * The followings are the available columns in table '{{warehouse_category}}':
 * @property integer $id
 * @property integer $parent_id
 * @property integer $brand_id
 * @property string $category_name
 * @property string $specification
 * @property integer $unit
 * @property string $spec_flag
 * @property string $type
 * @property string $dieno
 * @property string $length
 * @property string $specification_type
 * @property integer $company_id
 * @property integer $created_by
 * @property string $created_date
 */
class WarehouseCategory extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */

	public $rules_array = array();
	public $dieno;

	public function tableName()
	{
		return '{{warehouse_category}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		
		$action =  Yii::app()->controller->action->id;
        if ($action == 'create') {
		$this->rules_array = array(
                array('category_name, spec_flag, created_by, created_date', 'required'),
				array('parent_id, created_by', 'numerical', 'integerOnly'=>true),
				array('category_name', 'length', 'max'=>100),
				array('specification', 'length', 'max'=>255),
				array('spec_flag', 'length', 'max'=>1),
				array('category_name', 'unique'),
				// The following rule is used by search().
				// @todo Please remove those attributes that should not be searched.
				array('id, parent_id, category_name, specification, spec_flag, created_by, created_date , dieno ', 'safe', 'on'=>'search'),
            );
		}elseif ($action == 'update') {
		$this->rules_array = array(
                array('category_name, spec_flag, created_by, created_date', 'required'),
				array('parent_id, created_by', 'numerical', 'integerOnly'=>true),
				array('category_name', 'length', 'max'=>100),
				array('specification', 'length', 'max'=>255),
				array('spec_flag', 'length', 'max'=>1),
				array('category_name', 'unique'),
				array('category_name', 'categorycheck'),
				// The following rule is used by search().
				// @todo Please remove those attributes that should not be searched.
				array('id, parent_id, category_name, specification, spec_flag, created_by, created_date, dieno', 'safe', 'on'=>'search'),
            );
		} 

		elseif ($action == 'createspecification') {
                
			$this->rules_array = array(
                array('parent_id, unit', 'required'),
				array('parent_id, created_by, brand_id', 'numerical', 'integerOnly'=>true),
				//array('category_name', 'length', 'max'=>100),
				array('specification', 'length', 'max'=>255),
				array('unit', 'length', 'max'=>255),
				array('spec_flag', 'length', 'max'=>1),
				array('parent_id', 'checkexists'),
				array('dieno,filename','safe'),
				array('specification_type', 'checkspc'),
                                //array('filename', 'file', 'types'=>'jpg, gif, png', 'allowEmpty'=>false, 'wrongType'=>'Only jpg, gif, png files.'),
                                //array('filename', 'file', 'types'=>'pdf '),
                                //array('filename', 'file','types'=>'jpg, gif, png', 'allowEmpty'=>true),
                                //array('filename', 'file','types'=>'jpg, gif, png', 'allowEmpty'=>true), // this will allow empty field when page is update (remember here i create scenario update)
				
				// The following rule is used by search().
				// @todo Please remove those attributes that should not be searched.
				array('id, parent_id, brand_id, category_name, specification, unit, spec_flag, type, dieno, length, specification_type, filename, company_id, created_by, created_date', 'safe', 'on'=>'search'),
            );
		} 
		
		 return $this->rules_array;
		
	}
	

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'parent_id' => 'Parent',
			'brand_id' => 'Brand',
			'category_name' => 'Category Name',
			'specification' => 'Specification',
			'unit' => 'Unit',
			'spec_flag' => 'Spec Flag',
			'type' => 'Type',
			'dieno' => 'Dieno',
			'length' => 'Length',
			'specification_type' => 'Specification Type',
			'company_id' => 'Company',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('parent_id',$this->parent_id);
		$criteria->compare('brand_id',$this->brand_id);
		$criteria->compare('category_name',$this->category_name,true);
		$criteria->compare('specification',$this->specification,true);
		$criteria->compare('unit',$this->unit);
		$criteria->compare('spec_flag',$this->spec_flag,true);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('dieno',$this->dieno,true);
		$criteria->compare('length',$this->length,true);
		$criteria->compare('specification_type',$this->specification_type,true);
		$criteria->compare('company_id',$this->company_id);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->addCondition('spec_flag= "N"');
                $criteria->order = 'id DESC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>false,
			//'pagination'=>array('pageSize'=>10),
		));
	}

	    public function checkexists($attribute, $params){
		
		    $tblpx = Yii::app()->db->tablePrefix;
			if($this->id == '') {

	            if($this->parent_id  !='' && $this->specification !='' && $this->brand_id !=''){
					$data = WarehouseCategory::model()->findByAttributes(array('parent_id' => $this->parent_id, 'specification' => $this->specification, 'brand_id' => $this->brand_id));	
				}
				
				if($this->parent_id  !='' && $this->specification !='' && $this->brand_id ==''){
					 $data = Yii::app()->db->createCommand()
	                   ->select('*')
	                   ->from(''.$tblpx.'warehouse_category')
	                   ->where('parent_id=:parent_id and specification=:specification and brand_id IS NULL', array(':parent_id' => $this->parent_id, ':specification' => $this->specification))
	                   ->queryRow();
					//$data = WarehouseCategory::model()->find(array('condition' => 'parent_id ='. $this->parent_id.' AND specification ="'. $this->specification.'" AND brand_id IS NULL' ));
				}
				
		}else {
				 if($this->parent_id  !='' && $this->specification !='' && $this->brand_id !=''){
					$data = WarehouseCategory::model()->findByAttributes(array('parent_id' => $this->parent_id, 'specification' => $this->specification, 'brand_id' => $this->brand_id),'id !='.$this->id.'');	
				}
				
				if($this->parent_id  !='' && $this->specification !='' && $this->brand_id ==''){
					 $data = Yii::app()->db->createCommand()
	                   ->select('*')
	                   ->from(''.$tblpx.'warehouse_category')
	                   ->where('parent_id=:parent_id and specification=:specification and brand_id IS NULL AND id != '.$this->id.'', array(':parent_id' => $this->parent_id, ':specification' => $this->specification))
	                   ->queryRow();
					//$data = WarehouseCategory::model()->find(array('condition' => 'parent_id ='. $this->parent_id.' AND specification ="'. $this->specification.'" AND brand_id IS NULL' ));
				}
		}
			if (!empty($data)) {
                 $this->addError("specification", ("specification is already taken"));
             }
			
            
    }

    public function specificationsearch()
	{       

            $tblpx = Yii::app()->db->tablePrefix;
            $criteria=new CDbCriteria;
            $criteria->select = 'child.id,t.category_name, child.specification, child.unit, child.created_date, child.brand_id,child.specification_type,child.filename ';
            $criteria->join = ' LEFT JOIN `'.$tblpx.'warehouse_category` AS `child` ON t.id = child.parent_id';
            //$criteria->join = ' LEFT JOIN `'.$tblpx.'warehouse_category` AS `child` ON t.id = child.parent_id';
            $criteria->addCondition('child.parent_id IS NOT NULL');
            $criteria->addCondition('child.specification IS NOT NULL AND child.company_id = '.Yii::app()->user->company_id.'');
            $criteria->order = 'id DESC';
            return new CActiveDataProvider($this, array(
		'criteria'=>$criteria,
		'pagination'=>false,
		//'pagination'=>array('pageSize'=>10),
            ));
		
		//$criteria->join = 'LEFT JOIN '.$tblpx.'warehouse_category AS parent';
/*$criteria->join = 'LEFT JOIN '.$tblpx.'warehouse_category AS child 
         ON child.parent_id = '.$tblpx.'warehouse_category.id';*/
         
         
	//$criteria->addCondition = "test_sections.id = test_section_questions.test_section_id AND questions.id = test_section_questions.question_id";
	
		/*$criteria->join = 'SELECT parent.category_name,child.specification FROM '.$tblpx.'warehouse_category AS parent
         LEFT JOIN '.$tblpx.'warehouse_category AS child 
         ON child.parent_id = parent.id
   WHERE parent.parent_id IS NULL
ORDER BY parent.id, child.id';*/
		/*return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));*/
	}

		public function categorycheck(){

		    $tblpx = Yii::app()->db->tablePrefix;
            if($this->id!='' ){


            	$data1 = Yii::app()->db->createCommand("SELECT id FROM {$tblpx}warehouse_category WHERE id ='".$this->id."'")->queryRow();


            	if($data1['category_name'] != $this->category_name ){

            		$data = $specification = Yii::app()->db->createCommand("SELECT id FROM {$tblpx}warehouse_category WHERE category_name ='".$this->category_name."'")->queryRow();

            		print_r("SELECT id FROM {$tblpx}warehouse_category WHERE category_name ='".$this->category_name."'");exit;
			
					if (!empty($data)) {

		                 $this->addError("category_name", ("Brand is already taken"));
		             }
            	}

            	
            }
    
       }

     public function checkspc($attribute, $params){

          

         	if($this->specification_type=='A'){
                if($this->dieno ==''){

         		  $this->addError("dieno", (" Profile/Dieno Required"));
         	    }
         	}else{


               
                if($this->specification ==''){
                	$this->addError("specification", (" Specification Required"));
                }
         		
         	}


       }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return WarehouseCategory the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


}
