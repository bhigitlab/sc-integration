<?php

/**
 * This is the model class for table "{{vouchers}}".
 *
 * The followings are the available columns in table '{{vouchers}}':
 * @property integer $id
 * @property integer $voucher_for
 * @property string $voucher_date
 * @property integer $company
 * @property integer $project
 * @property integer $voucher_for_user_id
 * @property integer $expense_head
 * @property string $vehicle_number
 * @property string $voucher_number
 * @property integer $staff_id
 * @property string $received_by
 * @property integer $created_by
 * @property string $created_date
 * @property integer $updated_by
 * @property string $updated_date
 *
 * The followings are the available model relations:
 * @property VoucherItems[] $voucherItems
 * @property Company $company0
 * @property Users $createdBy
 * @property ExpenseType $expenseHead
 * @property Projects $project0
 * @property Users $staff
 * @property Users $updatedBy
 * @property Users $voucherForUser
 */
class Vouchers extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{vouchers}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('voucher_for, company, project, voucher_for_user_id, expense_head, staff_id, created_by, updated_by', 'numerical', 'integerOnly' => true),
			array('voucher_for, company, project, voucher_for_user_id, expense_head, staff_id,voucher_number,voucher_date', 'required'),
			array('vehicle_number, voucher_number, received_by', 'length', 'max' => 100),
			array('voucher_date, created_date, updated_date,', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, voucher_for, voucher_date, company, project, voucher_for_user_id, expense_head, vehicle_number, voucher_number, staff_id, received_by, created_by, created_date, updated_by, updated_date', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'voucherItems' => array(self::HAS_MANY, 'VoucherItems', 'voucher_id'),
			'company0' => array(self::BELONGS_TO, 'Company', 'company'),
			'createdBy' => array(self::BELONGS_TO, 'Users', 'created_by'),
			'expenseHead' => array(self::BELONGS_TO, 'ExpenseType', 'expense_head'),
			'project0' => array(self::BELONGS_TO, 'Projects', 'project'),
			'staff' => array(self::BELONGS_TO, 'Users', 'staff_id'),
			'updatedBy' => array(self::BELONGS_TO, 'Users', 'updated_by'),
			'voucherForUser' => array(self::BELONGS_TO, 'Users', 'voucher_for_user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'voucher_for' => 'Voucher For',
			'voucher_date' => 'Voucher Date',
			'company' => 'Company',
			'project' => 'Project',
			'voucher_for_user_id' => 'Voucher For User',
			'expense_head' => 'Expense Head',
			'vehicle_number' => 'Vehicle Number',
			'voucher_number' => 'Voucher Number',
			'staff_id' => 'Site Engineer',
			'received_by' => 'Received By',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
			'updated_by' => 'Updated By',
			'updated_date' => 'Updated Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('voucher_for', $this->voucher_for);
		if (!empty($this->voucher_date))
			$criteria->compare('voucher_date', date('Y-m-d', strtotime($this->voucher_date)), true);
		$criteria->compare('company', $this->company);
		$criteria->compare('project', $this->project);
		$criteria->compare('voucher_for_user_id', $this->voucher_for_user_id);
		$criteria->compare('expense_head', $this->expense_head);
		$criteria->compare('vehicle_number', $this->vehicle_number, true);
		$criteria->compare('voucher_number', $this->voucher_number, true);
		$criteria->compare('staff_id', $this->staff_id);
		$criteria->compare('received_by', $this->received_by, true);
		$criteria->compare('created_by', $this->created_by);
		$criteria->compare('created_date', $this->created_date, true);
		$criteria->compare('updated_by', $this->updated_by);
		$criteria->compare('updated_date', $this->updated_date, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Vouchers the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function getMaxId($filter = null)
	{
		$criteria               = new CDbCriteria;
		$criteria->order = 'id DESC';
		$model  = $this->find($criteria);
		return !empty($model) ? ($model->id) + 1 : '1';
	}

	public function scVendorData($type, $voucher_user_id)
	{
		if ($type == 1) {
			$model = Subcontractor::model()->findByPk($voucher_user_id);
			if (!empty($model))
				return $model->subcontractor_name;
		} elseif ($type == 2) {
			$model = Vendors::model()->findByPk($voucher_user_id);
			if (!empty($model))
				return $model->name;
		} else {
			return '';
		}
	}
}
