<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/tableHeadFixer.js"></script>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.min.js"></script>
<style>
    #warehousestock-entry {
        max-height: 350px;
        overflow: auto;
    }
</style>
<?php
/* @var $this ExpensesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'Clients',
);
$page = Yii::app()->request->getParam('Clients_page');
if ($page != '') {
    Yii::app()->user->setReturnUrl($page);
} else {
    Yii::app()->user->setReturnUrl(0);
}
?>


<div class="container" id="project">
    <div class="sub-heading mb-10">
        <h3>Warehouse Stock Status Report</h3>
    </div>
    <div id="addclient" style="display:none;">

    </div>
    <div class="clearfix">
        <?php $this->renderPartial('_search', array('model' => $model, 'specification' => $specification, 'units' => $units)); ?>
        <div id="search" class="pull-right mb-10">
            <label>Search</label>
            <input type="text" id="myInput" placeholder="Search" title="Type in a name" class="form-control">
        </div>
    </div>
    <div class="warehousestock">
        <div id="warehousestock-entry" class="table-responsive">
            <?php $this->renderPartial('_stock_status_report_data', array('result_data' => $result_data)); ?>
        </div>
    </div>

</div>




<script>
    function closeaction() {
        $('#addclient').slideUp('slow');
    }

    $(document).ready(function () {


        $('.addclient').click(function () {
            // alert('hi');
            $.ajax({
                type: "POST",
                url: "<?php echo $this->createUrl('warehousestock/create&layout=1') ?>",
                success: function (response) {
                    $("#addclient").html(response).slideDown();
                }
            });
        });
        $(document).delegate('.editstock', 'click', function () {
            var id = $(this).attr('data-id');
            $.ajax({
                type: "POST",
                url: "<?php echo $this->createUrl('warehousestock/update&id=') ?>" + id,
                success: function (response) {
                    $("#addclient").html(response).slideDown();
                    $("#warehouseStockId").val(id);
                }
            });
        });

        $(document).on('click', '.deletestock', function () {
            var id = $(this).attr('data-id');
            $.ajax({
                type: "POST",
                url: "<?php echo $this->createUrl('warehousestock/delete&id=') ?>" + id,
                success: function (result) {
                    $("#addclient").slideUp();
                    if (result == 1) {

                    } else {
                        $("#newlist").html(result);
                        $("#myTable").dataTable({
                            "scrollY": "300px",
                            "scrollCollapse": true,
                            "paging": false,
                            "columnDefs": [{
                                "targets": [0],
                                "searchable": false
                            },
                            ]
                        });
                    }
                }
            });
        });

        jQuery(function ($) {
            $('#client').on('keydown', function (event) {
                if (event.keyCode == 13) {
                    $("#clientsearch").submit();
                }
            });
        });
    });


    $(document).ready(function () {
        $('#myInput').keyup(function () {
            // Search Text
            var search = $(this).val();

            // Hide all table tbody rows
            $('table tbody tr').hide();

            // Count total search result
            var len = $('table tbody tr:not(.notfound) td:nth-child(1):contains("' + search + '")').length;
            var len1 = $('table tbody tr:not(.notfound) td:nth-child(3):contains("' + search + '")').length;
            var len2 = $('table tbody tr:not(.notfound) td:nth-child(5):contains("' + search + '")').length;
            var len3 = $('table tbody tr:not(.notfound) td:nth-child(8):contains("' + search + '")').length;

            if (len > 0 || len1 > 0 || len2 > 0 || len3 > 0) {
                // Searching text in columns and show match row
                $('table tbody tr:not(.notfound) td:contains("' + search + '")').each(function () {
                    $(this).closest('tr').show();
                });
            } else {
                $('.notfound').show();
            }

        });

    });
    $.expr[":"].contains = $.expr.createPseudo(function (arg) {
        return function (elem) {
            return $(elem).text().toUpperCase().indexOf(arg.toUpperCase()) >= 0;
        };
    });

</script>