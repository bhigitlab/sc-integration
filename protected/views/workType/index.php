<?php
/* @var $this WorkTypeController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Work Types',
);

$this->menu=array(
	array('label'=>'Create WorkType', 'url'=>array('create')),
	array('label'=>'Manage WorkType', 'url'=>array('admin')),
);
?>

<h1>Work Types</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'work-type-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'wtid',
		'work_type',
		'rate',
		array(
			'class'=>'CButtonColumn',
			'template' => '{update} {delete}',
		),
	),
)); ?>