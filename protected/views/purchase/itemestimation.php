<style>
    .permission_style {
        background: #f8cbcb !important;
    }
   
    .row_class{
        background-color: #f8cbcb !important;
    }
    .reject_class{
        background-color: #f57275 !important;
    }

</style>
<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$this->breadcrumbs=array(
	'Estimation List',)
?>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/tableHeadFixer.js"></script>
<div class="container" id="project">
<div class="clearfix">
    <h2 class="pull-left">Estimation</h2>
    <div class="add-btn pull-right">
        <a href="index.php?r=/materialEstimation/index" class="button btn btn-primary">Add Estimation</a>
        <!-- <a href="index.php?r=purchase/glassestimation" class="button" style="margin-right: 10px;">Add Estimation by Width x Height</a>
        <a href="index.php?r=purchase/estimationbylength" class="button" style="margin-right: 10px;">Add  Estimation by Length</a> -->
        <?php
       $pms_api_integration_model=ApiSettings::model()->findByPk(1);
       $pms_api_integration =$pms_api_integration_model->api_integration_settings;
       
       if($pms_api_integration==1){
            $currentUrl = Yii::app()->request->getUrl();
            echo CHtml::link('Refresh', array('MaterialEstimation/refreshButton', 'type' => 1,'returnUrl' => $currentUrl), array('class' => 'btn btn-info ml-10 pull-right','style'=>'margin-right:40px;  margin-top:15px;' ,'id' => 'materialrequest_refresh')); 
        }
        ?>
    </div>
</div>

<?php 
//$this->renderPartial('_newsearch', array('model' => $model, 'project' => $project, 'vendor' => $vendor, 'purchase_no' => $purchase_no,'expense' => $expense)) ?>
<div class="row">
    <div class="col-sm-12">
        <div id="errorMsg"></div>
    </div>
</div>
<?php if(Yii::app()->user->hasFlash('success')):?>
<div class="info" style="width: 500px;margin-left: 400px;padding: 8px;color: green;font-size: 20px;font-weight: bold; padding-top: 46px;">
    <?php echo Yii::app()->user->getFlash('success'); ?>
</div>
<?php endif; ?>


<?php if(Yii::app()->user->hasFlash('error')):?>
<div class="info" style="width: 500px;margin-left: 400px;padding: 8px;color: red;font-size: 20px;font-weight: bold; padding-top: 46px;">
<?php echo Yii::app()->user->getFlash('error'); ?>
</div>
<?php endif; ?>
<div id="errMsg"></div>

<div  style="">
    <div id="addremark" class="panel panel-gray" style="display:none;position: fixed;top: 15%; bottom: 25%;right: 0px; z-index: 6;width:500px; margin:0 auto;background-color: #fff;">
        <div class="panel-heading form-head">
            <h3 class="panel-title">Remark For Purchase No: <span class="po_no"></span></h3>
            <button type="button" onclick="closeaction(this)" class="close" data-dismiss="alert" aria-label="Close" ><span aria-hidden="true">×</span></button>
        </div>
        <div class="panel-body">
            <div class="clearfix">
                <a class="addnow" style="display:none;">Add Remark</a>
                <div class="remarkadd">
                    
                    <div class="form-group">
                        <label>Add Remark</label>
                        <textarea type="text" name="remark" id="remark" class="form-control"></textarea>
                    </div> 
                    <div class="text-right">
                        <input type="submit" value="Save" id="remarkSubmit" class="btn btn-sm btn-info"/>
                        <input type="button" value="Close" class="closermrk btn  btn-sm btn-default" style="display:none;"/>
                    </div>
                </div>
                <input type="hidden" name="txtPurchaseId" id="txtPurchaseId" value=""/>
                <h4 class="head_remarks">Remarks</h4>
                <div class="all_remarks">
                    <div id="remarkList">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="msg_box"></div>
    <div id="parent">
    <?php 
    $this->widget('zii.widgets.CListView', array(
        'dataProvider' => $dataProvider,
        'itemView' => '_itemestimationview', // Ensure this file exists
        'template' => '
         
           
            <table class="table" id="fixtable">
                <thead>
                    <tr>
                        <th width="8%">Actions</th>
                        <th width="10%">Sl.No</th>
                        <th>Project</th>
                        <th>Request From</th>
                    </tr>
                </thead>
                <tbody>{items}</tbody>
            </table>
           {pager}<!-- Pagination at the bottom -->
        ',
        'pager' => array(
            'header' => '',
            'firstPageLabel' => 'First',
            'lastPageLabel' => 'Last',
            'prevPageLabel' => '&laquo; Prev',
            'nextPageLabel' => 'Next &raquo;',
            'maxButtonCount' => 5,
            'cssFile' => false, // Disable Yii default styles
        ),
    )); 
    ?>
</div>


    <div class="modal fade" id="rejectModal" tabindex="-1" role="dialog" aria-labelledby="rejectModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="rejectModalLabel">Reject Estimation</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="rejectForm">
                    <input type="hidden" id="rejectId" name="id">
                    <div class="form-group">
                        <label for="remarks">Remarks</label>
                        <textarea class="form-control" id="remarks" name="remarks" rows="3" required></textarea>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" onClick="submitReject()">Submit</button>
            </div>
        </div>
    </div>
</div>


        
        <?php /* $this->widget('zii.widgets.CListView', array(
        'dataProvider'=>$dataProvider,
        //'itemView'=>'_newview',
        'viewData' => array( 'total_billedamount' => $total_billedamount, 'total_blanceamount' => $total_blanceamount, 'purchasesum' => $purchasesum),
        'sortableAttributes'=>array(
            'vendor_id' => 'Vendor',
            'project_id'=>'Project',
            'purchase_no' => 'Purchase No',
        ),
        

        'itemView' => '_newview', 'template' => '<div class="clearfix"><div class="pull-right">{summary}</div><div class="pull-left">{sorter}</div></div><div id="parent"><table cellpadding="10" class="table  list-view sorter" id="fixtable">{items}</table></div>',
        'emptyText'=>'<table cellpadding="10" class="table"><thead>
            <tr>
            <th>No</th>
            <th>Purchase No</th>
            <th>Project</th>
            <th>Expense Head</th>
            <th>Vendor</th>            
            <th>Total Amount</th>
            <th>Date</th>
            <th>Purchase Type</th>
            <th>Status</th>
            <th>Total amount billed</th>
            <th>Balance to be billed</th>
            <th>Billing Status</th>
            <th></th>            
             </tr></thead><tr><td colspan="13" class="text-center">No results found</td></tr></table>'
            )); */?>

    </div>
</div>

<style>
    #fixtable thead th ,#fixtable tfoot th{background-color: #eee; z-index: 1;}
    
a.button {
    background-color: #6a8ec7;
    display: block;
    padding: 5px;
    color: #fff;
    cursor: pointer;
    float: right;
    border: 1px solid #6a8ec8;
}
.bill_class {
	background: #FF6347 !important;
}
.exp-list{padding: 0px;}
.page-body h3{
        margin:4px 0px;
        color:inherit;
        text-align: left;
    }
    tfoot th{background-color: #eee;}
.panel{border:1px solid #ddd;box-shadow: 0px 0px 12px 1px rgba(0,0,0,0.25);}
.panel-heading{background-color: #6A8EC7;height: 40px;color: #fff;}    
.panel label{display:block;}

.head_remarks{
    padding-left: 5px;
}  
    
.ind_remarks{
    padding: 5px;    
    font-size: 13px;
}
.ind_remarks p:first-child{font-size: 11px;}
.ind_remarks p:last-child {
background: #f6f6f6;
padding: 5px;
margin-top: -5px;
}
.pur_remark{
    font-weight:600;
    vertical-align:middle !important;
    white-space: nowrap;
    position: relative; 
}
.cursor-pointer{cursor: pointer;}
.pur_remark .icon{}
.form-head {
    position:relative;
}
.close{
    position: absolute;
    top: 7px;
    right: 6px;
}
#addremark .all_remarks{
    overflow: auto;
    margin-top: 5px;
    max-height: 150px;
    border-top: 1px solid #ddd;
}
.badge1{/*color: #555;background-color: #ccc;*/
    position:absolute;
   right: 0px;
    top: 0px;
    color:#0093dd;
    font-weight:900;
    font-size:11px;
}

.addnow {cursor:pointer;font-size:14px;float:right;}
.highlight {
    background-color:#DD1035;
    color:#fff;	
}
#parent{max-height: 500px;}
#parent .table{margin-bottom: 0px;}
@media(min-width: 1400px){
   #addremark .all_remarks{
        max-height: 300px;
   }
}


</style>

<script>
    function openRejectModal(id) {
        $('#rejectId').val(id); 
        $('#rejectModal').modal('show'); 
    }
    
    function submitReject() {
        var id = $('#rejectId').val();
        var remarks = $('#remarks').val();
         
        if (remarks === "") {
            alert("Remarks field cannot be empty.");
            return;
        }

        $.ajax({
            type: "POST",
            url: "index.php?r=purchase/rejectEstimation",
            data: {
                id: id,
                remarks: remarks,
            },
            success: function(response) {
                var result = JSON.parse(response);
                if (result.response === 'success') {
                    alert('Labour rejected successfully');
                    $('#rejectModal').modal('hide');  
                    location.reload();  
                } else {
                    alert('Error: ' + result.msg);
                }
            },
            error: function() {
                alert('An error occurred. Please try again.');
            }
            });
    }
</script>

<script>
    
$('div#addr').click(function(){
    $('.remarkadd').hide();
    $('.addnow').show();       
});

$('.addnow').click(function(){
    $(this).hide();
    $('.closermrk').show();
    $('.remarkadd').show();
});
$('.closermrk').click(function(){
    $(this).hide();
    $('.addnow').show();
    $('.remarkadd').hide();
    $('#remark').val('');
});

function closeaction(){
    $('#remark').val('');
    $('#addremark').hide("slide", { direction: "right" }, 200);
}
function addremark(pur_order,event){
    var id  = pur_order;
    $(".popover").removeClass( "in" );
    $('.remarkadd').show();
    $('.addnow').hide();
    $('#addremark').show("slide", { direction: "right" }, 500);
    $("#txtPurchaseId").val(id);
    event.preventDefault();
    $.ajax({
        type: "GET",
        data: {purchaseid: id},
        dataType:'json',
        url: '<?php echo Yii::app()->createUrl('purchase/viewremarks'); ?>',
        success: function (response) {
            $("#remarkList").html(response.result);
            $(".po_no").html(response.po_no)
            $("#txtPurchaseId").val(id);
       }
    });

}
var url = '<?php echo  Yii::app()->getBaseUrl(true); ?>';

$(document).ready(function(){
    $('.update_status').click(function(e){
        e.preventDefault();
        var element = $(this);
        var answer = confirm("Are you sure you want to change the status?");
        if (answer)
        {
            var purchase_id = $(this).attr('id');	
            $.ajax({
                url: '<?php echo  Yii::app()->createAbsoluteUrl('purchase/updatepurchasestatus'); ?>',
                type:'GET',
                dataType:'json',
                data:{purchase_id:purchase_id},
                success: function(response) {
                        console.log(response);
                        if(response.response == 'success')
                        {
                                 $(".purchase_status_"+purchase_id).html('Saved');
                                 $(".edit_option_"+purchase_id).hide();
                                 element.closest('tr').find('.bill_label').text('Pending to be Billed');
                                 element.closest('tr').find('.bill_status').addClass('bill_class');
                                 $("#msg_box").html('<div class="alert alert-success alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><strong> Success!</strong>  &nbsp;'+response.msg+'</div>');	
                        } else {

                                 $("#msg_box").html('<div class="alert alert-warning alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><strong> Sorry!</strong>  &nbsp;'+response.msg+'</div>');	
                        }
                }
            });
        }else{
            return false;
        }
    });


});

$(document).on('click', '.edit_purchase', function(){
        var val = $(this).attr('id');
        var pType = $(this).attr('data-id');
        if(pType == "A") {
            location.href= url+'/index.php?r=purchase/updatepurchasebylength&pid='+val;
        } else if(pType == "G") {
            location.href= url+'/index.php?r=purchase/updatepurchaseglass&pid='+val;
        } else if(pType == "O") {
            location.href= url+'/index.php?r=purchase/updatepurchase&pid='+val;
        } else {
            location.href= url+'/index.php?r=purchase/updatepurchase&pid='+val;
        }
});

function approve(id, element) {

    $('.loading-overlay').addClass('is-active');
    $.ajax({
        url: '<?php echo Yii::app()->createAbsoluteUrl('purchase/approve'); ?>',
        type: 'POST',
        dataType: 'json',
        data: {
            id: id
        },
        success: function(data) {
                if (data == 1) {
                    $("#errorMsg").show()
                        .html('<div class="alert alert-success"><b>Success!</b> Material Entry Approved Successfully.</div>')
                        .fadeOut(5000);
                    setTimeout(function() {
                        location.reload();
                    }, 3000);
                } else {
                    $("#errorMsg").show()
                        .html('<div class="alert alert-danger"><b>Error Occurred.</b></div>')
                        .fadeOut(5000);
                }
            },
        complete: function() {
            $('.loading-overlay').removeClass('is-active');
        }
    });
}


$(document).on('click', '#remarkSubmit', function(e) {
    var purchaseId  = $("#txtPurchaseId").val();
    var remark      = $("#remark").val();
    $.ajax({
        method: "GET",
        data: {purchaseid: purchaseId, remark: remark},
        url: '<?php echo Yii::app()->createUrl('purchase/addremarks'); ?>',
        success: function (response) {
            $("#remarkList").html(response);
            $('#addremark').show("slide", { direction: "right" }, 1000);
            $("#remark").val("");
        }
    });
});
$(document).on('click', '.view_purchase', function(){
    var val = $(this).attr('id');
    location.href= url+'/index.php?r=purchase/viewpurchase&pid='+val;
});

function myfunction(elem){
    var id= "";
    id = $(elem).attr('id');
    var element = elem;
    if (confirm('Are you sure you want to delete this?')) {
    $.ajax({
        url: '<?php echo  Yii::app()->createAbsoluteUrl('purchase/deletepurchase'); ?>',
        type:'GET',
        dataType:'json',
        data:{purchase_id:id},
        success: function(response) {
            console.log(response);
                if(response == 1)
                {
                    $("#msg_box").html('<div class="alert alert-success alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><strong> Success!</strong>  &nbsp;Deleted successfully </div>').fadeOut(5000);	
                    element.closest('tr').remove();
                    setTimeout(
                    function() 
                      {
//                         location.reload();

                      }, 500); 
                } else {
                    $("#msg_box").html('<div class="alert alert-warning alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><strong> Sorry!</strong>  &nbsp; Some Problem occured</div>');	
                }
        }
        
    });
    
    }
}
  
$(document).ready(function() {
$("#fixtable").tableHeadFixer({'head' : true}); 
    $(".popover-test").popover({
        html: true,
            content: function() {
              //return $('#popover-content').html();
              return $(this).next('.popover-content').html();
            }
    });
    $('[data-toggle=popover]').on('click', function (e) {
       $('[data-toggle=popover]').not(this).popover('hide');
    });
    $('body').on('hidden.bs.popover', function (e) {
        $(e.target).data("bs.popover").inState.click = false;
    });
    $('body').on('click', function (e) {
        $('[data-toggle=popover]').each(function () {
            // hide any open popovers when the anywhere else in the body is clicked
            if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                $(this).popover('hide');
            }
        });
    });
    $(document).ajaxComplete(function() {
        $("#fixtable").tableHeadFixer({'left' : false, 'foot' : true, 'head' : true}); 
        $(".popover-test").popover({
            html: true,
            content: function() {
              return $(this).next('.popover-content').html();
            }
        });
    
    });
   
});
$(document).on('click', '.deleteProject', function() {
if (confirm("Are you sure you want to delete this estimation?")) {
        $('.loading-overlay').addClass('is-active');
        var estimation_id = $(this).attr('data-id');
        var project_id = $(this).attr('data-project');
            $.ajax({
                type: "POST",
                url: "<?php echo $this->createUrl('purchase/Deleteestimation') ?>",
                data: {
                    estimation_id: estimation_id,
                    project_id:project_id
                },
                dataType: 'json',
                success: function(response) {
                console.log();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
                $("#errMsg").show()
                    .html('<div class="alert alert-' + response.success + '">' + response.message + '</div>')
                    .fadeOut(10000);
                    setTimeout(function() {
                    location.reload(true);
                    }, 1000);
                },
                error: function() {
                    $('.loading-overlay').removeClass('is-active');
                    alert('There was an error deleting the estimation. Please try again.');
             }
        });
    }
});                 
</script>