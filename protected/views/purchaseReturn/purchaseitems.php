<?php
/* @var $this BillsController */
/* @var $model Bills */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="row">
    <div class="col-sm-12">
        <div class="table-responsive">
            <table class="table">
                <thead class="thead-inverse entry-table">
                    <tr>
                        <th>Specifications/Remark</th>
                        <th>Actual Quantity</th>
                        <th>Billed Quantity</th>
                        <th>Remaining Quantity</th>
                        <th>Quantity</th>
                        <th>Unit</th>
                        <th>Rate</th>
                        <th>Amount</th>
                        <th>Discount Amount</th>
                        <th>Discount (%)</th>
                        <th>CGST</th>
                        <th>CGST (%)</th>
                        <th>SGST</th>
                        <th>SGST (%)</th>
                        <th>IGST</th>
                        <th>IGST (%)</th>
                        <th>Total Tax</th>
                        <th>Tax (%)</th>
                        <th>Total Amount</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $number = 1;
                    $amount = 0;
                    $totalQty = 0;
                    $bTotalQty = 0;
                    foreach ($purchase as $item) {
                        if ($item['category_id']) {
                            $specName = '';
                            $brand = '';
                            $catName = '';

                            $categorymodel = Specification::model()->findByPk($item['category_id']);
                            if ($categorymodel) {
                                $specName = $categorymodel->specification;
                                if ($categorymodel['brand_id'] != NULL) {
                                    $brand_details = Brand::model()->findByPk($categorymodel['brand_id']);
                                    $brand = ' - ' . ucwords($brand_details['brand_name']);
                                }
                                if ($categorymodel['cat_id'] != "") {
                                    $categoryData = PurchaseCategory::model()->findByPk($categorymodel['cat_id']);
                                    $catName = $categoryData['category_name'];
                                }
                                $categoryName = ucwords($catName) . $brand . ' - ' . ucwords($specName);
                            } else {
                                $categoryName = "Not available";
                            }
                        } else {
                            $categoryName = $item['remark'];
                        }
                        $purchasemodel = Billitem::model()->findByPk($item['billitem_id']);
                        $itemQuantity = $purchasemodel->billitem_quantity;
                        $tblpx = Yii::app()->db->tablePrefix;
                        $itemData = Yii::app()->db->createCommand("SELECT SUM(returnitem_quantity) as quantitysum FROM " . $tblpx . "purchase_returnitem WHERE billitem_id = " . $item['billitem_id'])->queryRow();
                        $bItemQuantity = $itemData['quantitysum'] ? $itemData['quantitysum'] : 0;
                        if (strpos($bItemQuantity, ".") !== false) {
                            $bItemQuantity = number_format((float) $bItemQuantity, 2, '.', '');
                        } else {
                            $bItemQuantity = $bItemQuantity;
                        }
                        $quantityAvail = $itemQuantity - $bItemQuantity;
                        $totalQty = $totalQty + $itemQuantity;
                        $bTotalQty = $bTotalQty + $bItemQuantity;
                        ?>

                        <tr class="pitems">
                            <input type="hidden" id="ids<?php echo $number - 1; ?>"
                                value="<?php echo $item["billitem_id"]; ?>" class="txtBox pastweek ids" name="ids[]" />
                            <input type="hidden" id="category<?php echo $number - 1; ?>"
                                value="<?php echo $item['category_id'] ? $item['category_id'] : ''; ?>"
                                class="txtBox pastweek category" name="category[]" />
                            <td id="<?php echo $number - 1; ?>">
                                <div id="bicategoryname<?php echo $number - 1; ?>">
                                    <?php echo $categoryName; ?>
                                </div>
                            </td>
                            <input type="hidden" id="availablequantity<?php echo $number - 1; ?>"
                                value="<?php echo $quantityAvail; ?>" name="availablequantity[]" />
                            <input type="hidden" id="orrate<?php echo $number - 1; ?>"
                                value="<?php echo number_format((float) $item["billitem_rate"], 2, '.', ''); ?>"
                                name="orrate[]" />
                            <td id="<?php echo $number - 1; ?>">
                                <div id="actualquantity<?php echo $number - 1; ?>">
                                    <?php echo $itemQuantity; ?>
                                </div>
                            </td>
                            <td id="<?php echo $number - 1; ?>">
                                <div id="billedquantity<?php echo $number - 1; ?>">
                                    <?php echo $bItemQuantity; ?>
                                </div>
                            </td>
                            <td id="<?php echo $number - 1; ?>">
                                <div id="biremquantity<?php echo $number - 1; ?>">
                                    <?php echo $quantityAvail; ?>
                                </div>
                            </td>
                            <td id="<?php echo $number - 1; ?>"><input type="text" id="biquantity<?php echo $number - 1; ?>"
                                    class="form-control input fnext validate[required,custom[number],min[0],max[<?php echo $quantityAvail; ?>]]"
                                    value="0" name="biquantity[]" <?php if ($quantityAvail == 0) { ?> readonly="true" <?php } ?> /></td>
                            <td id="<?php echo $number - 1; ?>">
                                <div id="biunit<?php echo $number - 1; ?>">
                                    <?php echo $item["billitem_unit"]; ?>
                                </div>
                            </td>
                            <td id="<?php echo $number - 1; ?>"><input type="text" id="birate<?php echo $number - 1; ?>"
                                    class="form-control lnext validate[required,custom[number],min[0]]"
                                    value="<?php echo number_format((float) $item["billitem_rate"], 2, '.', ''); ?>"
                                    name="birate[]" <?php if ($quantityAvail == 0) { ?> readonly="true" <?php } ?> /></td>
                            <td id="<?php echo $number - 1; ?>">
                                <div id="biamount<?php echo $number - 1; ?>" class="pricediv">0</div>
                            </td>
                            <td id="<?php echo $number - 1; ?>">
                                <div id="damount<?php echo $number - 1; ?>" class="pricediv">0</div>
                            </td>
                            <td id="<?php echo $number - 1; ?>">
                                <div id="dpercent<?php echo $number - 1; ?>" class="pricediv">
                                    <?php echo number_format($item["billitem_discountpercent"], 2, '.', '') ?>
                                </div>
                            </td>
                            <td id="<?php echo $number - 1; ?>">
                                <div id="cgst<?php echo $number - 1; ?>" class="pricediv">0</div>

                            </td>
                            <td id="<?php echo $number - 1; ?>">
                                <div id="cgstpercent<?php echo $number - 1; ?>" class="pricediv">
                                    <?php echo number_format($item["billitem_cgstpercent"], 2, '.', '') ?>
                                </div>
                            </td>
                            <td id="<?php echo $number - 1; ?>">
                                <div id="sgst<?php echo $number - 1; ?>" class="pricediv">0</div>

                            </td>
                            <td id="<?php echo $number - 1; ?>">
                                <div id="sgstpercent<?php echo $number - 1; ?>" class="pricediv">
                                    <?php echo number_format($item["billitem_sgstpercent"], 2, '.', '') ?>
                                </div>
                            </td>

                            <td id="<?php echo $number - 1; ?>">
                                <div id="igst<?php echo $number - 1; ?>" class="pricediv">0
                                </div>
                            </td>
                            <td id="<?php echo $number - 1; ?>">
                                <div id="igstpercent<?php echo $number - 1; ?>" class="pricediv">
                                    <?php echo number_format($item["billitem_igstpercent"], 2, '.', '') ?>
                                </div>
                            </td>

                            <td id="<?php echo $number - 1; ?>">
                                <div id="taxamount<?php echo $number - 1; ?>" class="pricediv">0</div>
                            </td>
                            <td id="<?php echo $number - 1; ?>">
                                <div id="taxpercent<?php echo $number - 1; ?>" class="pricediv">
                                    <?php echo $item['billitem_taxpercent']; ?>
                                </div>
                            </td>
                            <td id="<?php echo $number - 1; ?>">
                                <div id="totalamount<?php echo $number - 1; ?>" class="pricediv">0</div>
                            </td>
                            <input type="hidden" id="billitem<?php echo $number - 1; ?>" value="" name="billitem[]" />
                            <input type="hidden" id="savedquantity<?php echo $number - 1; ?>" value=""
                                name="savedquantity[]" />
                            <td>
                                <div id="tickmark<?php echo $number - 1; ?>"></div>
                            </td>
                        </tr>
                        <?php
                        $amount = $amount + $item["billitem_amount"];
                        $number = $number + 1;
                    }
                    ?>
                    <input type="hidden" name="tqty[]" id="tqty" value="<?php echo $totalQty; ?>" />
                    <input type="hidden" name="btqty[]" id="btqty" value="<?php echo $bTotalQty; ?>" />
                    <input type="hidden" id="totrows" value="<?php echo $number - 1; ?>" class="txtBox pastweek totrows"
                        name="totrows" />
                </tbody>
            </table>
            <!--<input type="hidden" name="aj-amount" id="aj-amount" value="<?php //echo $amount; ?>"/>-->
        </div>
    </div>
</div>