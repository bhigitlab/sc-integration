<?php
/* @var $this TaxSlabsController */
/* @var $model TaxSlabs */

$this->breadcrumbs=array(
	'Tax Slabs'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List TaxSlabs', 'url'=>array('index')),
	array('label'=>'Create TaxSlabs', 'url'=>array('create')),
	array('label'=>'Update TaxSlabs', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete TaxSlabs', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage TaxSlabs', 'url'=>array('admin')),
);
?>

<h1>View TaxSlabs #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'tax_slab_value',
		'cgst',
		'sgst',
		'igst',
		'status',
		'created_by',
		'created_date',
		'updated_by',
		'updated_date',
	),
)); ?>
