<?php
/* @var $this ClientsController */
/* @var $model Clients */

$this->breadcrumbs=array(
	'Clients'=>array('index'),
	'Create',
);

/*$this->menu=array(
	array('label'=>'List Clients', 'url'=>array('index')),
	array('label'=>'Manage Clients', 'url'=>array('admin')),
);*/
?>

<!--<div class="modal-dialog modal-md">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" onclick="javascript:window.location.reload()">&times;</button>
            <h4 class="modal-title">Add Clients</h4>
        </div>
        <?php //echo $this->renderPartial('_form', array('model' => $model)); ?>
    </div>

</div>-->
<div class="panel panel-gray">
    <div class="panel-heading form-head">
        <h3 class="panel-title">Add payment reminders</h3>
    </div>
        <?php echo $this->renderPartial('_form', array('model' => $model)); ?>
</div>
