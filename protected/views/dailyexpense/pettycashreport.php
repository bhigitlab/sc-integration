<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/tableHeadFixer.js"></script>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.js"></script>

<style>
	.grandtotal td {
		background-color: rgb(247, 240, 240) !important;
	}
</style>
<?php
Yii::app()->clientScript->registerScript('search', "
	$('#btSubmit').click(function(){
	$('#pettyform').submit();
	});
	");
$tblpx = Yii::app()->db->tablePrefix;
$user = Users::model()->findByPk(Yii::app()->user->id);
$arrVal = explode(',', $user->company_id);
$newQuery = "";
$newQuery1 = "";
$newQuery2 = "";
$newQuery3 = "";
foreach ($arrVal as $arr) {
	if ($newQuery)
		$newQuery .= ' OR';
	if ($newQuery1)
		$newQuery1 .= ' OR';
	if ($newQuery2)
		$newQuery2 .= ' OR';
	if ($newQuery3)
		$newQuery3 .= ' OR';
	$newQuery .= " FIND_IN_SET('" . $arr . "', company_id)";
	$newQuery1 .= " FIND_IN_SET('" . $arr . "', " . $tblpx . "dailyexpense.company_id)";
	$newQuery2 .= " FIND_IN_SET('" . $arr . "', " . $tblpx . "expenses.company_id)";
	$newQuery3 .= " FIND_IN_SET('" . $arr . "', id)";
}
?>
<div class="container" id="project">
	<div class="header-container">
		<h3>Petty Cash Report</h3>
		<div class="btn-container">
			<a class="btn btn-info" href="<?php echo Yii::app()->createUrl('dailyexpense/savetopdfpetty'); ?>"
				style="text-decoration: none; color: white;">
				<i class="fa fa-file-pdf-o button-icon" aria-hidden="true"></i>
			</a>
			<a class="btn btn-info" href="<?php echo Yii::app()->createUrl('dailyexpense/savetoexcelpetty'); ?>"
				style="text-decoration: none; color: white;">
				<i class="fa fa-file-excel-o button-icon" aria-hidden="true"></i>
			</a>
		</div>
	</div>
	<div class="page_filter clearfix custom-form-style">
		<?php $form = $this->beginWidget('CActiveForm', array(
			'action' => Yii::app()->createUrl($this->route),
			'method' => 'get',
		)); ?>
		<!-- <form id="pettyform" action="<?php echo Yii::app()->createUrl($this->route) ?>" method="GET"> -->
		<div class="row">
			<div class="form-group col-xs-12 col-sm-3 col-md-2 ">
				<label for="employee">Employee</label>
				<?php
				echo CHtml::dropDownList('employee_id', 'employee_id', CHtml::listData(Users::model()->findAll(array(
					'select' => array('userid, concat_ws(" ",first_name,last_name) as first_name'),
					'order' => 'userid DESC',
					'condition' => '(' . $newQuery . ') AND user_type NOT IN (1)',
					'distinct' => true
				)), 'userid', 'first_name'), array('empty' => 'Select Employee', 'id' => 'employee_id', 'class' => 'form-control', 'style' => 'padding: 2.5px 0px;', 'options' => array($employee_id => array('selected' => true))));
				?>
			</div>
			<div class="form-group col-xs-12 col-sm-3 col-md-2 ">
				<label for="company">Company</label>
				<?php
				echo CHtml::dropDownList('company_id', 'company_id', CHtml::listData(Company::model()->findAll(array(
					'select' => array('id, name'),
					'order' => 'id DESC',
					'condition' => '(' . $newQuery3 . ')',
					'distinct' => true
				)), 'id', 'name'), array('empty' => 'Select Company', 'id' => 'company_id', 'class' => 'form-control', 'style' => 'padding: 2.5px 0px;', 'options' => array($company_id => array('selected' => true))));
				?>
			</div>
			<div class="form-group col-xs-12 col-sm-2 col-md-2 ">
				<?php echo CHtml::label('From ', ''); ?>
				<?php echo CHtml::textField('date_from', (!empty($date_from) ? date('d-M-Y', strtotime($date_from)) : ''), array('placeholder' => 'Date From', "id" => "date_from", "readonly" => true, 'class' => 'form-control', 'autocomplete' => 'off', 'style' => 'display:inline-block;')); ?>
			</div>
			<div class="form-group col-xs-12 col-sm-2 col-md-2 ">
				<?php echo CHtml::label('To ', ''); ?>
				<?php echo CHtml::textField('date_to', (!empty($date_to) ? date('d-M-Y', strtotime($date_to)) : ''), array('placeholder' => 'Date To', "id" => "date_to", "readonly" => true, 'class' => 'form-control', 'autocomplete' => 'off', 'style' => 'display:inline-block;')); ?>
			</div>
			<input type="hidden" name="ledger" value="<?php echo isset($_GET['ledger']) ? $_GET['ledger'] : 0; ?>">
			<div class="form-group col-xs-12 col-sm-2 col-md-2 text-sm-left text-right">
				<label class="d-sm-block d-none">&nbsp;</label>
				<div>
					<?php echo CHtml::submitButton('GO', array('id' => 'btSubmit', 'class' => 'btn btn-sm btn-primary')); ?>
					<?php echo CHtml::resetButton('Clear', array('onclick' => 'javascript:location.href="' . $this->createUrl('dailyexpense/pettycash') . '"', 'class' => 'btn btn-sm btn-default')); ?>
				</div>
			</div>
		</div>
		<?php $this->endWidget(); ?>
	</div>
	<?php if ((!empty($dailyexpense_exp) || !empty($dailyexpense) || !empty($daybook) || !empty($dailyexpense_refund)) && ($employee_id != 0)) { ?>
		<?php
		$users = Users::model()->findByPk($employee_id);
		?>
		<div id="contenthtml" class="contentdiv">
			<div class="clearfix">
				<div class="btn container pull-right">

					<a style="cursor:pointer;" id="download" class="btn btn-info" title="SAVE AS EXCEL"
						href="<?php echo $this->createAbsoluteUrl('dailyexpense/pettycashexceldetails', array("employee_id" => $employee_id, 'date_from' => $date_from, 'date_to' => $date_to, 'company_id' => $company_id)) ?>">
						<i class="fa fa-file-excel-o button-icon" aria-hidden="true"></i></a>
					<?php
					if (isset($_GET['ledger'])) {
						if ($_GET['ledger'] == 1) {
							?>
							<a style="cursor:pointer;" id="download" class="btn btn-info" title="SAVE AS PDF"
								href="<?php echo $this->createAbsoluteUrl('dailyexpense/pettycashpdfdetails', array("employee_id" => $employee_id, 'date_from' => $date_from, 'date_to' => $date_to, 'company_id' => $company_id, 'ledger' => 1, 'export' => 'PDF')) ?>">
								<i class="fa fa-file-pdf-o button-icon" aria-hidden="true"></i></a>
							<a style="cursor:pointer;" id="download" class="btn btn-info"
								href="<?php echo $this->createAbsoluteUrl('dailyexpense/pettycash', array("employee_id" => $employee_id, 'date_from' => $date_from, 'date_to' => $date_to, 'company_id' => $company_id, 'ledger' => 0)) ?>">List
								View</a>
						<?php } else { ?>
							<a style="cursor:pointer;" id="download" class="btn btn-info" title="SAVE AS PDF"
								href="<?php echo $this->createAbsoluteUrl('dailyexpense/pettycashpdfdetails', array("employee_id" => $employee_id, 'date_from' => $date_from, 'date_to' => $date_to, 'company_id' => $company_id, 'ledger' => 0)) ?>">
								<i class="fa fa-file-pdf-o button-icon" aria-hidden="true"></i></a>
							<a style="cursor:pointer;" id="download" class="btn btn-info"
								href="<?php echo $this->createAbsoluteUrl('dailyexpense/pettycash', array("employee_id" => $employee_id, 'date_from' => $date_from, 'date_to' => $date_to, 'company_id' => $company_id, 'ledger' => 1)) ?>">Ledger
								View</a>
						<?php }
					} ?>
				</div>
			</div>
			<?php if ($_GET['ledger'] == 0) { ?>
				<h3>Petty Cash Report of <b>
						<?php echo $users->first_name; ?>
					</b></h3>
				<div class="container-fluid">
					<table class="table total-table">
						<tr>
							<td><b>Employee Name: </b><?php echo $users->first_name . ' ' . $users->last_name; ?> &nbsp; &nbsp;
								<?php
								if (!empty($company_id)) {
									$company = Company::model()->findByPk($company_id);
									echo "Company: " . $company->name;
								}
								?>
							</td>
						</tr>
					</table>
					<br>
					<div class="table-responsive">
						<table class="table total-table">
							<thead class="entry-table sticky-thead">
								<tr>
									<td colspan="5"><b>Petty Cash Advance</b></td>
								</tr>
								<tr>
									<th>Date</th>
									<th>Description</th>
									<th>Payment Type</th>
									<th>Reconciliation Date</th>
									<th class="text-right"><b>Amount</b></th>
								</tr>
							</thead>
							<?php
							$dailyexpense_amount = 0;
							foreach ($dailyexpense as $value) {
								?>
								<tr>
									<td>
										<?php echo Yii::app()->Controller->changeDateForamt($value['date']); ?>
									</td>
									<td>
										<?php print_r($value['description']); ?>
									</td>
									<td>

										<?php
										if ($value['dailyexpense_chequeno'] != "") {
											echo 'Cheque';
										} else {
											echo 'Cash';
										}
										?>
									</td>
									<td>

										<?php
										if ($value['dailyexpense_chequeno'] != "") {
											echo $value['reconciliation_date'];
										} else {
											echo 'N/A';
										}
										?>
									</td>
									<td class="text-right" border="1">
										<?php echo Controller::money_format_inr($value['dailyexpense_paidamount'], 2); ?>
									</td>
									<?php $dailyexpense_amount += $value['dailyexpense_paidamount']; ?>
								</tr>
							<?php }
							?>
							<tr>
								<td colspan="4"><b>Net Amount</b></td>
								<td class="text-right">
									<?php echo '<b>' . Controller::money_format_inr($dailyexpense_amount, 2) . '</b>'; ?>
								</td>
							</tr>
						</table>
					</div>
					<br>
					<table class="table total-table">

						<tr>
							<td colspan="2"><b>Petty Cash Expense</b></td>
						</tr>
						<!-- Daybook expense -->
						<?php
						$daybook_amount = 0;
						if (!empty($daybook)) { ?>
							<tr>
								<th colspan="2" bgcolor="#eee">DAYBOOK</th>
							</tr>
						<?php }
						foreach ($daybook as $value) {
							?>

							<tr>
								<th colspan="2">
									<?php echo $value['name']; ?>
								</th>
							</tr>
							<?php
							$tblpx = Yii::app()->db->tablePrefix;

							$daybook_details = Yii::app()->db->createCommand("SELECT * FROM " . $tblpx . "expenses LEFT JOIN " . $tblpx . "users ON " . $tblpx . "expenses.employee_id= " . $tblpx . "users.userid WHERE " . $tblpx . "expenses.type = 73  AND " . $tblpx . "expenses.expense_type=103 " . $where1 . " AND " . $tblpx . "expenses.projectid = " . $value['pid'] . " order by " . $tblpx . "expenses.date asc")->queryAll();
							$i = 0;
							$item_amount = 0;
							foreach ($daybook_details as $newsql) {
								$i++;
								if ($i % 2 == 0) {
									$class = 'even';
								} else {
									$class = 'odd';
								}
								$item_amount += $newsql['paid'];
								?>
								<tr class="<?= $class ?>">
									<td>
										<?php echo Yii::app()->Controller->changeDateForamt($newsql['date']); ?>
										<?php echo " " . $newsql['description']; ?>
									</td>
									<td class="text-right">
										<?php print_r(Controller::money_format_inr($newsql['paid'], 2)); ?>
									</td>
								</tr>
								<?php
							}
							$daybook_amount += $item_amount;
							?>
							<tr>
								<td class="text-right" colspan="2">
									<?php echo '<b>' . Controller::money_format_inr($item_amount, 2) . '</b>'; ?>
								</td>
							</tr>
							<?php
						}
						?>
						<tr>
							<td><b>Daybook Total</b></td>
							<td class="text-right">
								<?php echo '<b>' . Controller::money_format_inr($daybook_amount, 2) . '</b>'; ?>
							</td>
						</tr>


						<!-- Daily expense -->

						<?php
						$dailyexpense_exp_amount = 0;
						if (!empty($dailyexpense_exp)) { ?>
							<tr>
								<th colspan="2" bgcolor="#eee">DAILY EXPENSE</th>
							</tr>

							<?php
							foreach ($dailyexpense_exp as $value) {
								?>
								<tr>
									<td>
										<?php echo Yii::app()->Controller->changeDateForamt($value['date']); ?>
										<?php print_r($value['description']); ?>
									</td>
									<td class="text-right" border="1">
										<?php echo Controller::money_format_inr($value['dailyexpense_paidamount'], 2); ?>
									</td>
									<?php $dailyexpense_exp_amount += $value['dailyexpense_paidamount']; ?>
								</tr>
							<?php }
							?>
							<tr>
								<td><b>Daily Expense Total</b></td>
								<td class="text-right">
									<?php echo '<b>' . Controller::money_format_inr($dailyexpense_exp_amount, 2) . '</b>';
									?>
								</td>
							</tr>
						<?php } ?>
						<!-- subcontractor expense -->
						<?php
						$scpayment_amount = 0;
						if (!empty($scpayment)) { ?>
							<tr>
								<th colspan="2" bgcolor="#eee">Subcontractor Payment</th>
							</tr>

							<?php
							foreach ($scpayment as $value) {
								?>
								<tr>
									<td>
										<?php echo Yii::app()->Controller->changeDateForamt($value['date']); ?>
										<?php print_r($value['description']); ?>
									</td>
									<td class="text-right" border="1">
										<?php echo Controller::money_format_inr($value['paidamount'], 2); ?>
									</td>
									<?php $scpayment_amount += $value['paidamount']; ?>
								</tr>
							<?php }
							?>
							<tr>
								<td><b>Subcontractor Expense Total</b></td>
								<td class="text-right">
									<?php echo '<b>' . Controller::money_format_inr($scpayment_amount, 2) . '</b>';
									?>
								</td>
							</tr>
						<?php } ?>


						<tr>
							<td><b>Net Amount</b></td>
							<td class="text-right">
								<?php echo '<b>' . Controller::money_format_inr(($dailyexpense_exp_amount + $daybook_amount + $scpayment_amount), 2) . '</b>';
								?>
							</td>
						</tr>


					</table>
					<br>
					<div class="table-responsive">
						<table class="table total-table">
							<thead class="entry-table sticky-thead">
								<tr>
									<td colspan="5"><b>Petty Cash Refund</b></td>
								</tr>
								<tr>
									<th>Date</th>
									<th>Description</th>
									<th>Payment Type</th>
									<th>Reconciliation Date</th>
									<th class="text-right"><b>Amount</b></th>
								</tr>
							</thead>
							<?php
							$dailyexpense_refunds = 0;
							foreach ($dailyexpense_refund as $value) {
								?>
								<tr>
									<td>
										<?php echo Yii::app()->Controller->changeDateForamt($value['date']); ?>
									</td>
									<td>
										<?php print_r($value['description']); ?>
									</td>
									<td>

										<?php
										if ($value['dailyexpense_chequeno'] != "") {
											echo 'Cheque';
										} else {
											echo 'Cash';
										}
										?>
									</td>
									<td>

										<?php
										if ($value['dailyexpense_chequeno'] != "") {
											echo $value['reconciliation_date'];
										} else {
											echo 'N/A';
										}
										?>
									</td>
									<td class="text-right" border="1">
										<?php echo Controller::money_format_inr($value['dailyexpense_receipt'], 2); ?>
									</td>
									<?php $dailyexpense_refunds += $value['dailyexpense_receipt']; ?>
								</tr>
							<?php }
							?>
							<tr>
								<td colspan="4"><b>Net Amount</b></td>
								<td class="text-right">
									<?php echo '<b>' . Controller::money_format_inr($dailyexpense_refunds, 2) . '</b>'; ?>
								</td>
							</tr>
						</table>
					</div>
					<br>
					<table class="table total-table">
						<tr>
							<td>Petty Cash Advance: </td>
							<td class="text-right">
								<?php echo '<b>' . Controller::money_format_inr($dailyexpense_amount, 2) . '</b>'; ?>
							</td>
						</tr>
						<tr>
							<td>Petty Cash Expense :</td>
							<td class="text-right">
								<?php echo '<b>' . Controller::money_format_inr($dailyexpense_exp_amount + $daybook_amount + $scpayment_amount, 2) . '</b>'; ?>
							</td>
						</tr>
						<tr>
							<td>Petty Cash Refund :</td>
							<td class="text-right">
								<?php echo '<b>' . Controller::money_format_inr($dailyexpense_refunds, 2) . '</b>'; ?>
							</td>
						</tr>
						<tr>
							<td>
								<?php
								echo "Cash In Hand";
								?> :
							</td>
							<td class="text-right">
								<?php echo '<b>' . Controller::money_format_inr(($dailyexpense_amount - ($daybook_amount + $dailyexpense_exp_amount + $scpayment_amount + $dailyexpense_refunds)), 2) . '</b>'; ?>
							</td>
						</tr>
					</table>
				</div>
			<?php } else {
				$this->renderPartial(
					'_petty_ledger_view',
					array(
						'employee_id' => $employee_id,
						'dailyexpense' => $dailyexpense,
						'dailyexpense_refund' => $dailyexpense_refund,
						'dailyexpense_exp' => $dailyexpense_exp,
						'scpayment' => $scpayment,
						'daybook' => $daybook,
						'where1' => $where1
					)
				);
			} ?>
		</div>
	<?php } ?>
	<?php if ((empty($dailyexpense) && empty($daybook) && empty($dailyexpense_refund)) && ($employee_id != 0)) {
		echo "No data found";
	} ?>
	<?php
	if ($employee_id == "") { ?>
		<div id="contenthtml" class="contentdiv">
			<!-- <div class="clearfix mt">
				
			</div> -->
			<div>
				<?php
				$advance = 0;
				$expense = 0;
				$balance = 0;
				$refund = 0;
				$chquery1 = "";
				$chquery2 = "";
				$chcondition1 = "";
				$chcondition2 = "";
				$employee = Yii::app()->db->createCommand("SELECT * FROM " . $tblpx . "users WHERE (" . $newQuery . ") AND user_type NOT IN (1) ORDER BY userid DESC")->queryAll();
				$reconcil_dexpense_payment_data = Yii::app()->db->createCommand("SELECT *  FROM `jp_reconciliation` "
					. " WHERE `reconciliation_status`='0' "
					. " and reconciliation_payment='Dailyexpense Payment' and "
					. "reconciliation_table='jp_dailyexpense'")->queryAll();

				$payment_cheque_array = array();
				foreach ($reconcil_dexpense_payment_data as $data) {

					$value = "'{$data['reconciliation_chequeno']}'";
					if ($value != '') {
						$recon_data = $value;
						array_push($payment_cheque_array, $recon_data);
					}
				}
				$payment_cheque_array1 = implode(',', $payment_cheque_array);
				if (($payment_cheque_array1)) {
					$chquery1 = "(dailyexpense_chequeno NOT IN ( $payment_cheque_array1) OR dailyexpense_chequeno IS NULL)";
					$chcondition1 = " AND " . $chquery1 . "";
				}
				$reconcil_dexpense_receipt_data = Yii::app()->db->createCommand("SELECT *  FROM `jp_reconciliation` "
					. " WHERE `reconciliation_status`='0' "
					. " and reconciliation_payment='Dailyexpense Receipt' and "
					. "reconciliation_table='jp_dailyexpense'")->queryAll();
				$receipt_cheque_array = array();
				foreach ($reconcil_dexpense_receipt_data as $receiptdata) {
					$receiptvalue = "'{$receiptdata['reconciliation_chequeno']}'";
					if ($receiptvalue != '') {
						$recon_receipt_data = $receiptvalue;
						array_push($receipt_cheque_array, $recon_receipt_data);
					}
				}
				$receipt_cheque_array1 = implode(',', $receipt_cheque_array);
				if (($receipt_cheque_array1)) {
					$chquery2 = "(dailyexpense_chequeno NOT IN ( $receipt_cheque_array1) OR dailyexpense_chequeno IS NULL)";
					$chcondition2 = " AND " . $chquery2 . "";
				}
				foreach ($employee as $key => $value) {
					$dailyexpense = array();
					if ($petty_issued_id) {
						$dailyexpensesql = "SELECT SUM(" . $tblpx . "dailyexpense.dailyexpense_paidamount) "
							. " as amount FROM " . $tblpx . "dailyexpense LEFT JOIN " . $tblpx . "users "
							. " ON " . $tblpx . "dailyexpense.employee_id= " . $tblpx . "users.userid "
							. " WHERE " . $tblpx . "dailyexpense.exp_type =73 "
							. " AND " . $tblpx . "dailyexpense.expense_type "
							. " IN('89','88') " . $chcondition1 . " "
							. " AND " . $tblpx . "dailyexpense.employee_id = " . $value['userid'] . " "
							. " AND jp_dailyexpense.exp_type_id= $petty_issued_id " . $where;
						$dailyexpense = Yii::app()->db->createCommand($dailyexpensesql)->queryRow();
					}
					$dailyexpense_refund = array();
					if ($petty_refund_id) {
						$dailyexpense_refundsql = "SELECT SUM(" . $tblpx . "dailyexpense.dailyexpense_receipt) "
							. " as amount FROM " . $tblpx . "dailyexpense LEFT JOIN " . $tblpx . "users"
							. " ON " . $tblpx . "dailyexpense.employee_id= " . $tblpx . "users.userid "
							. " WHERE " . $tblpx . "dailyexpense.exp_type =72 "
							. " AND " . $tblpx . "dailyexpense.dailyexpense_receipt_type "
							. " IN ('88','89') " . $chcondition2
							. " AND jp_dailyexpense.exp_type_id=$petty_refund_id "
							. " AND " . $tblpx . "dailyexpense.employee_id = " . $value['userid'] . $where;
						$dailyexpense_refund = Yii::app()->db->createCommand($dailyexpense_refundsql)->queryRow();
					}


					$dailyexpense_exp_sql = "SELECT  SUM(" . $tblpx . "dailyexpense.dailyexpense_paidamount) "
						. " as expamount  FROM " . $tblpx . "dailyexpense "
						. " LEFT JOIN " . $tblpx . "users "
						. " ON " . $tblpx . "dailyexpense.employee_id= " . $tblpx . "users.userid "
						. " WHERE (" . $tblpx . "dailyexpense.dailyexpense_receipt_type = 103 "
						. " OR  " . $tblpx . "dailyexpense.expense_type = 103  ) "
						. " AND " . $tblpx . "dailyexpense.employee_id = " . $value['userid'] . $where
						. " order by " . $tblpx . "dailyexpense.date asc";
					$dailyexpense_exp = Yii::app()->db->createCommand($dailyexpense_exp_sql)->queryRow();

					$daybooksql = "SELECT SUM(" . $tblpx . "expenses.paid) as amount "
						. " FROM " . $tblpx . "expenses LEFT JOIN " . $tblpx . "users "
						. " ON " . $tblpx . "expenses.employee_id= " . $tblpx . "users.userid "
						. " WHERE " . $tblpx . "expenses.type = 73  "
						. " AND " . $tblpx . "expenses.expense_type=103 "
						. " AND " . $tblpx . "expenses.employee_id = " . $value['userid'] . $where1;
					$daybook = Yii::app()->db->createCommand($daybooksql)->queryRow();

					$scsql = "SELECT SUM(" . $tblpx . "subcontractor_payment.paidamount) as amount "
						. " FROM " . $tblpx . "subcontractor_payment LEFT JOIN " . $tblpx . "users "
						. " ON " . $tblpx . "subcontractor_payment.employee_id= " . $tblpx . "users.userid "
						. " WHERE " . $tblpx . "subcontractor_payment.payment_type=103 "
						. " AND " . $tblpx . "subcontractor_payment.employee_id = " . $value['userid'] . $where2;
					$scpayment = Yii::app()->db->createCommand($scsql)->queryRow();

					if (isset($dailyexpense['amount'])) {
						$advance += $dailyexpense['amount'];
					}
					if (isset($daybook['amount'])) {
						$expense += $daybook['amount'];
					}
					if (isset($dailyexpense_exp['expamount'])) {
						$expense += $dailyexpense_exp['expamount'];
					}
					if (isset($scpayment['amount'])) {
						$expense += $scpayment['amount'];
					}
					$balance += $advance - $expense;
					if (isset($dailyexpense_refund['amount'])) {
						$refund += $dailyexpense_refund['amount'];
					}
				}
				?>
				<div class="text-right mt">Total
					<?php echo count($employee); ?> results
				</div>
				<div id="parent">
					<table class="table" id="fixTable">
						<thead>
							<tr>
								<th>Sl No.</th>
								<th>Employee Name</th>
								<th>Company</th>
								<th class="text-right">Petty Cash Advance</th>
								<th class="text-right">Petty Cash Expense</th>
								<th class="text-right">Petty Cash Refund</th>
								<th class="text-right">Cash In Hand</th>
							</tr>

							<tr class="grandtotal">
								<td colspan="3" class="text-right"><b>Total</b></td>
								<td class="text-right"><b>
										<?php echo Controller::money_format_inr($advance, 2); ?>
									</b></td>
								<td class="text-right"><b>
										<?php echo Controller::money_format_inr($expense, 2); ?>
									</b></td>
								<td class="text-right"><b>
										<?php echo Controller::money_format_inr($refund, 2); ?>
									</b></td>
								<td class="text-right"><b>
										<?php echo Controller::money_format_inr($advance - ($expense + $refund), 2); ?>
									</b></td>
							</tr>
						</thead>
						<tbody>
							<?php
							$advance = 0;
							$expense = 0;
							$balance = 0;
							$refund = 0;
							$chquery1 = "";
							$chquery2 = "";
							$chcondition1 = "";
							$chcondition2 = "";
							$employee = Yii::app()->db->createCommand("SELECT * FROM " . $tblpx . "users WHERE (" . $newQuery . ") AND user_type NOT IN (1) ORDER BY userid DESC")->queryAll();
							$reconcil_dexpense_payment_data = Yii::app()->db->createCommand("SELECT *  FROM `jp_reconciliation` "
								. " WHERE `reconciliation_status`='0' "
								. " and reconciliation_payment='Dailyexpense Payment' and "
								. "reconciliation_table='jp_dailyexpense'")->queryAll();

							$payment_cheque_array = array();
							foreach ($reconcil_dexpense_payment_data as $data) {

								$value = "'{$data['reconciliation_chequeno']}'";
								if ($value != '') {
									$recon_data = $value;
									array_push($payment_cheque_array, $recon_data);
								}
							}
							$payment_cheque_array1 = implode(',', $payment_cheque_array);
							if (($payment_cheque_array1)) {
								$chquery1 = "(dailyexpense_chequeno NOT IN ( $payment_cheque_array1) OR dailyexpense_chequeno IS NULL)";
								$chcondition1 = " AND " . $chquery1 . "";
							}
							$reconcil_dexpense_receipt_data = Yii::app()->db->createCommand("SELECT *  FROM `jp_reconciliation` "
								. " WHERE `reconciliation_status`='0' "
								. " and reconciliation_payment='Dailyexpense Receipt' and "
								. "reconciliation_table='jp_dailyexpense'")->queryAll();
							$receipt_cheque_array = array();
							foreach ($reconcil_dexpense_receipt_data as $receiptdata) {
								$receiptvalue = "'{$receiptdata['reconciliation_chequeno']}'";
								if ($receiptvalue != '') {
									$recon_receipt_data = $receiptvalue;
									array_push($receipt_cheque_array, $recon_receipt_data);
								}
							}
							$receipt_cheque_array1 = implode(',', $receipt_cheque_array);
							if (($receipt_cheque_array1)) {
								$chquery2 = "(dailyexpense_chequeno NOT IN ( $receipt_cheque_array1) OR dailyexpense_chequeno IS NULL)";
								$chcondition2 = " AND " . $chquery2 . "";
							}
							foreach ($employee as $key => $value) {
								$dailyexpense = array();
								if ($petty_issued_id) {
									$dailyexpensesql = "SELECT SUM(" . $tblpx . "dailyexpense.dailyexpense_paidamount) "
										. " as amount FROM " . $tblpx . "dailyexpense LEFT JOIN " . $tblpx . "users "
										. " ON " . $tblpx . "dailyexpense.employee_id= " . $tblpx . "users.userid "
										. " WHERE " . $tblpx . "dailyexpense.exp_type =73 "
										. " AND " . $tblpx . "dailyexpense.expense_type IN('89','88') " . $chcondition1 . " "
										. " AND " . $tblpx . "dailyexpense.employee_id = " . $value['userid'] . ""
										. "  AND jp_dailyexpense.exp_type_id= $petty_issued_id " . $where;
									$dailyexpense = Yii::app()->db->createCommand($dailyexpensesql)->queryRow();
								}
								$dailyexpense_refund = array();
								if ($petty_refund_id) {
									$dailyexpense_refundsql = "SELECT SUM(" . $tblpx . "dailyexpense.dailyexpense_receipt) "
										. " as amount FROM " . $tblpx . "dailyexpense "
										. " LEFT JOIN " . $tblpx . "users "
										. " ON " . $tblpx . "dailyexpense.employee_id= "
										. $tblpx . "users.userid"
										. " WHERE " . $tblpx . "dailyexpense.exp_type =72 "
										. " AND " . $tblpx . "dailyexpense.dailyexpense_receipt_type "
										. " IN ('88','89') " . $chcondition2 . " "
										. " AND jp_dailyexpense.exp_type_id=$petty_refund_id "
										. " AND " . $tblpx . "dailyexpense.employee_id = "
										. $value['userid'] . $where;
									$dailyexpense_refund = Yii::app()->db->createCommand($dailyexpense_refundsql)->queryRow();
								}


								$dailyexpense_exp_sql = "SELECT  SUM(" . $tblpx . "dailyexpense.dailyexpense_paidamount) as expamount  FROM " . $tblpx . "dailyexpense "
									. " LEFT JOIN " . $tblpx . "users "
									. " ON " . $tblpx . "dailyexpense.employee_id= " . $tblpx . "users.userid "
									. " WHERE (" . $tblpx . "dailyexpense.dailyexpense_receipt_type = 103 "
									. " OR  " . $tblpx . "dailyexpense.expense_type = 103  ) "
									. " AND " . $tblpx . "dailyexpense.employee_id = " . $value['userid'] . $where
									. " order by " . $tblpx . "dailyexpense.date asc";
								$dailyexpense_exp = Yii::app()->db->createCommand($dailyexpense_exp_sql)->queryRow();

								$daybooksql = "SELECT SUM(" . $tblpx . "expenses.paid) as amount "
									. " FROM " . $tblpx . "expenses LEFT JOIN " . $tblpx . "users "
									. " ON " . $tblpx . "expenses.employee_id= " . $tblpx . "users.userid "
									. " WHERE " . $tblpx . "expenses.type = 73  "
									. " AND " . $tblpx . "expenses.expense_type=103 "
									. " AND " . $tblpx . "expenses.employee_id = " . $value['userid'] . $where1;
								$daybook = Yii::app()->db->createCommand($daybooksql)->queryRow();

								$scsql = "SELECT SUM(" . $tblpx . "subcontractor_payment.paidamount) as amount "
									. " FROM " . $tblpx . "subcontractor_payment LEFT JOIN " . $tblpx . "users "
									. " ON " . $tblpx . "subcontractor_payment.employee_id= " . $tblpx . "users.userid "
									. " WHERE " . $tblpx . "subcontractor_payment.payment_type=103 "
									. " AND " . $tblpx . "subcontractor_payment.employee_id = " . $value['userid'] . $where2;
								$scpayment = Yii::app()->db->createCommand($scsql)->queryRow();

								if (isset($dailyexpense['amount'])) {
									$advance += $dailyexpense['amount'];
								}
								if (isset($daybook['amount'])) {
									$expense += $daybook['amount'];
								}
								if (isset($dailyexpense_exp['expamount'])) {
									$expense += $dailyexpense_exp['expamount'];
								}
								if (isset($scpayment['amount'])) {
									$expense += $scpayment['amount'];
								}
								$balance += $advance - $expense;
								if (isset($dailyexpense_refund['amount'])) {
									$refund += $dailyexpense_refund['amount'];
								}
								?>
								<tr>
									<td>
										<?php echo $key + 1; ?>
									</td>
									<td>
										<?php echo $value['first_name'] . ' ' . $value['last_name']; ?>
									</td>
									<td>
										<?php
										$companyname = array();
										$user = Users::model()->findByPk(Yii::app()->user->id);
										$comVal = explode(',', $user->company_id);
										$arrVal = explode(',', $value['company_id']);
										foreach ($arrVal as $arr) {
											if (in_array($arr, $comVal)) {
												$value = Company::model()->findByPk($arr);
												if ($value) {
													array_push($companyname, $value->name);
												}
											}
										}
										echo implode(', ', $companyname);
										?>
									</td>
									<td class="text-right">
										<?php echo (isset($dailyexpense['amount'])) ? Controller::money_format_inr($dailyexpense['amount'], 2) : 0; ?>
									</td>
									<?php $total_exp = 0;
									if (isset($daybook['amount'])) {
										$total_exp += $daybook['amount'];
									}
									if (isset($dailyexpense_exp['expamount'])) {
										$total_exp += $dailyexpense_exp['expamount'];
									}
									if (isset($scpayment['amount'])) {
										$total_exp += $scpayment['amount'];
									} ?>
									<td class="text-right">
										<?php echo Controller::money_format_inr($total_exp, 2); ?>
									</td>
									<td class="text-right">
										<?php echo (isset($dailyexpense_refund['amount'])) ? Controller::money_format_inr($dailyexpense_refund['amount'], 2) : 0; ?>
									</td>
									<?php $bal = 0;
									if (isset($dailyexpense['amount'])) {
										$bal = $dailyexpense['amount'] - $total_exp;
									}
									if (isset($dailyexpense_refund['amount'])) {
										$bal = $bal - $dailyexpense_refund['amount'];
									}
									?>
									<td class="text-right">
										<?php echo Controller::money_format_inr($bal, 2); ?>
									</td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<?php
	}
	?>
</div>
<style>
	#parent {
		max-height: 500px;
	}

	thead th,
	tfoot td {
		background: #eee;
	}

	.table {
		margin-bottom: 0px;
	}
</style>
<script>
	$(document).ready(function () {
		$("#employee_id").select2();
		$("#parent").css('max-height', $(window).height() - $("#myNavbar").height() - $(".search-form").height() - (2 * $('footer').height()));
		$("#fixTable").tableHeadFixer({
			'foot': true,
			'head': true
		});
	});
	$(document).ready(function () {
		$("#date_from").datepicker({
			dateFormat: 'dd-M-yy',
			changeYear: true,
			changeMonth: true,
			maxDate: $("#date_to").val()
		});
		$("#date_to").datepicker({
			dateFormat: 'dd-M-yy',
			changeYear: true,
			changeMonth: true
		});
		$("#date_from").change(function () {
			$("#date_to").datepicker('option', 'minDate', $(this).val());
		});
		$("#date_to").change(function () {
			$("#date_from").datepicker('option', 'maxDate', $(this).val());
		});
	});
</script>