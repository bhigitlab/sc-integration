<?php
/* @var $this WarehousereceiptController */
/* @var $model Warehousereceipt */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'warehousereceipt_id'); ?>
		<?php echo $form->textField($model,'warehousereceipt_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'warehousereceipt_no'); ?>
		<?php echo $form->textField($model,'warehousereceipt_no'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'warehousereceipt_date'); ?>
		<?php echo $form->textField($model,'warehousereceipt_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'warehousereceipt_vendorid'); ?>
		<?php echo $form->textField($model,'warehousereceipt_vendorid'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'warehousereceipt_stockid'); ?>
		<?php echo $form->textField($model,'warehousereceipt_stockid'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'warehousereceipt_clark'); ?>
		<?php echo $form->textField($model,'warehousereceipt_clark'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created_by'); ?>
		<?php echo $form->textField($model,'created_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created_date'); ?>
		<?php echo $form->textField($model,'created_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'updated_by'); ?>
		<?php echo $form->textField($model,'updated_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'updated_date'); ?>
		<?php echo $form->textField($model,'updated_date'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->