<?php
/* @var $this ExpensesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
	'Transactions',
);
$tblpx = Yii::app()->db->tablePrefix;
$pms_api_integration_model = ApiSettings::model()->findByPk(1);
$pms_api_integration = $pms_api_integration_model->api_integration_settings;
?>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/message/js/jquery.message.js"></script>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/message/css/jquery.message.css">

<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/tableHeadFixer.js"></script>
<div class="container" id="expense">
    <div class="loading-overlay">
        <span class="fa fa-spinner fa-3x fa-spin"></span>
    </div>
    <div class="expenses-heading">
        <div class="clearfix">
            <?php 

            $allBadge = '';
            $approveBadge = '';
            $nonApproveBadge = '';

            if ($_GET['status'] == 3) {
                $allBadge = 'badge-selected';
            } elseif ($_GET['status'] == 1) {
                $approveBadge = 'badge-selected';
            } elseif ($_GET['status'] == 2) {
                $nonApproveBadge = 'badge-selected';
            }

            ?>
            <?php if ($allData > 0) { ?>
                <button type="button" id="allbutton" class="btn btn-default pull-right mt-0 mb-10" onclick="filterData(3);" title="All Data" data-toggle="tooltip" data-placement="top">
                    All <span class="badge <?php echo $allBadge; ?>"><?php echo ($allData > 100) ? '99+' : $allData; ?></span>
                </button>
            <?php } ?>

            <?php if ($approvedData > 0) { ?>
                <button type="button" id="approved" class="btn btn-default pull-right mt-0 mb-10 margin-right-5" onclick="filterData(1); return false;" title="Approved Data" data-toggle="tooltip" data-placement="top">
                    Approved <span class="badge badge-success <?php echo $approveBadge; ?>"><?php echo ($approvedData > 100) ? '99+' : $approvedData; ?></span>
                </button>
            <?php } ?> 

            <?php if ($nonApprovedData > 0) { ?>
                <button type="button" id="nonapproved" class="btn btn-default pull-right mt-0 mb-10 margin-right-5" onclick="filterData(2); return false;" title="Non Approved Data" data-toggle="tooltip" data-placement="top">
                    Non-Approved <span class="badge badge-danger <?php echo $nonApproveBadge; ?>"><?php echo ($nonApprovedData > 100) ? '99+' : $nonApprovedData; ?></span>
                </button>
            <?php } ?>

            <button type="button" id="bulkApprove" class="btn btn-info pull-right mt-0 mb-10 margin-right-5">Approve</button>
            <h3>Labour Entries</h3>
        </div>
    </div>
				
    <div id="errormessage"></div>

    <div class="table-wrapper margin-top-25">
        <div  class="new-entry-table">
            <?php if ($pms_api_integration == 1): ?>
                <ul class="legend status-legend">
                    <li><span class="project_not_mapped"></span> Project/PMS Template Not Mapped In Integration</li>
                </ul>
            <?php endif; ?>

            <?php
            $this->widget('zii.widgets.CListView', array(
                'dataProvider' => $dataProvider,
                'pager' => array(
                    'header' => '',
                    'maxButtonCount' => 5,
                    'pageSize' => 10,
                ),
                'itemView' => '_view',
                'viewData' => array('unapprovedCount' => $unapprovedCount, 'status' => $status),
                'template' => '<div class="clearfix">
                                 <div class="pull-right">{summary}</div>
                                 <div class="pull-left">{sorter}</div>
                               </div>
                               <div id="parent">
                                 <table cellpadding="10" class="table total-table" id="fixtable">{items}</table>
                               </div>
                               {pager}',
                'emptyText' => '<table cellpadding="10" class="table">
                                 <thead>
                                   <tr>
                                     <th>No</th>
                                     <th>Company</th>
                                     <th>Project</th>
                                     <th>Subcontractor</th>
                                     <th>Expense Head</th>
                                     <th>Description</th>
                                     <th>Total Amount</th>
                                     <th>&nbsp;</th>
                                   </tr>
                                 </thead>
                                 <tr>
                                   <td colspan="8" class="text-center">No results found</td>
                                 </tr>
                               </table>',
            ));
            ?>
        </div>
    </div>
</div>

<?php
 $this->renderPartial('_reject_modal', array('model' => $model)) 
 ?>

</div>
</div>
<script>
	$(document).ready(function() {
    // Function to handle badge-selected class toggle
    function toggleBadgeSelection(buttonId) {

        // Remove badge-selected class from all badges
        $('.badge').removeClass('badge-selected');
        
        // Add badge-selected class to the selected button's badge
        $(`#${buttonId} .badge`).addClass('badge-selected');
    }

    // Handle All Data button click
    $('#allbutton').on('click', function() {
        toggleBadgeSelection('allbutton'); // Call function for All button
    });

    // Handle Approved button click
    $('#approved').on('click', function() {
        toggleBadgeSelection('approved'); 
    });

    // Handle Non-Approved button click
    $('#nonapproved').on('click', function() {
        toggleBadgeSelection('nonapproved'); // Call function for Non-Approved button
    });
});

	$(document).ready(function() {
		$("#filter-status").change(function() {
        var filterStatus = $(this).val();
        var url = "<?php echo Yii::app()->createUrl('dailyReport/admin'); ?>";
        window.location.href = url + "&status=" + filterStatus;
    	});
		window.filterData = function(filterStatus) {
        var url = "<?php echo Yii::app()->createUrl('dailyReport/admin'); ?>";
        window.location.href = url + "&status=" + filterStatus;
    };

		function updateApproveButton(){
			if($(".bulkapprovecheck:checked").length > 0){
				$("#bulkApprove").show();
			}else{
				$("#bulkApprove").hide();
			}
		}
		updateApproveButton();
		$(document).on('change','.bulkapprovecheck',function(){
			if($(".bulkapprovecheck:checked").length > 0){
				$("#bulkApprove").show();
			}else{
				$("#bulkApprove").hide();
			}
		});
		$(document).on('change','.checkallbulkapprove',function(){
			if ($(this).is(":checked")) {
				$(".bulkapprovecheck").prop('checked', true);
				$("#bulkApprove").show();
			} else {
				$(".bulkapprovecheck").prop('checked', false);
				$("#bulkApprove").hide();
			}
    	});
		$("#bulkApprove").click(function(){
			let selectedIds =[];
			if ($(".bulkapprovecheck:checked").length > 0) {
				$(".bulkapprovecheck:checked").each(function(){
				selectedIds.push($(this).data('id'));
			});
			$.ajax({
				url: '<?php echo  Yii::app()->createAbsoluteUrl('dailyReport/bulkapproval'); ?>',
				type: 'POST',
				data: {
					ids: selectedIds
				},
				success: function(response) {
					
					$("#errormessage").show()
                            .html('<div class="alert alert-success"><strong>Success!</strong> Approved Successfully.</div>')
                            .fadeOut(5000);
					setTimeout(function(){
							location.reload();
						},1000);		
				},
				error: function(xhr, status, error) {
					alert(response.msg);
					setTimeout(function(){
							location.reload();
					},1000);
				}
        	});

			}else{
				alert("Labour entries not selected");
			}
		});
	});
	$(document).ready(function() {
		$("#fixTable").tableHeadFixer({
			'left': 1,
			'foot': true,
			'head': true
		});
	});
	$(document).ready(function() {
		$(".popover-test").popover({
			html: true,
			content: function() {
				return $(this).next('.popover-content').html();
			}
		});
		$('[data-toggle=popover]').on('click', function(e) {
			$('[data-toggle=popover]').not(this).popover('hide');
		});
		$('body').on('hidden.bs.popover', function(e) {
			$(e.target).data("bs.popover").inState.click = false;
		});
		$('body').on('click', function(e) {
			$('[data-toggle=popover]').each(function() {
				if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
					$(this).popover('hide');
				}
			});
		});
	});
	$("body").on("click", ".row-daybook", function(e) {
		var rowId = $(this).attr("id");
		var expId = $("#dlyreportid" + rowId).val();
		console.log(expId);
		<?php $url = Yii::app()->createAbsoluteUrl("dailyReport/create"); ?>
		window.open("<?php echo $url ?>" + '&id=' + expId, '_blank')
	});
	$(document).on('click', '.approve', function(e) {
        e.preventDefault();
       
        var element = $(this);
        var item_id = $(this).attr('id');
        var includeInReport = confirm("Whether this entry should be added to project cost? ");
        var includeInReportValue = includeInReport ? 1 : 0;
       // alert(includeInReportValue);
        $('.loading-overlay').addClass('is-active');
        $.ajax({
            url: '<?php echo  Yii::app()->createAbsoluteUrl('dailyReport/approve'); ?>',
            type: 'POST',
            dataType: 'json',
            data: {
                item_id: item_id,
                includeInReportValue:includeInReportValue,
            },
            success: function(response) {
                if (response.response == 'success') {
                    $(".approveoption_" + item_id).hide();
                    element.closest("tr").removeClass("approve_section");
                    element.closest("tr").addClass("tobe_approve");
                    element.closest("tr").removeClass("row_class");
                    $("#errormessage").show()
                        .html('<div class="alert alert-success"><strong>Success!</strong>' + response.msg + '</div>')
                        .fadeOut(5000,function(){
							window.location.reload();
						});
                } else if (response.response == 'warning') {

                    $("#errormessage").show()
                        .html('<div class="alert alert-warning"><strong>Sorry!</strong>' + response.msg + '</div>')
                        .fadeOut(5000,function(){
							window.location.reload();
						});
                } else {
                    $("#errormessage").show()
                        .html('<div class="alert alert-danger"><strong>Sorry!</strong> ' + response.msg + '</div>')
                        .fadeOut(5000);
                }
            }
        });
    });
	$(document).ajaxComplete(function() {
		$('.loading-overlay').removeClass('is-active');
		$('#loading').hide();
	});

	$(document).on('click', '.delete_row', function(e) {
        var answer = confirm("Are you sure you want to delete?");
        if (answer) {
            $('.loading-overlay').addClass('is-active');
            var rowId = $(this).attr("id");
            var expId = $("#dlyreportid" + rowId).val();
            var date = $(this).attr("entry-date");
            $.ajax({
                type: "POST",
                data: {
                    expId: expId,
                    expense_date: date
                },
                url: "<?php echo Yii::app()->createUrl("dailyReport/deletereport") ?>",
                success: function(data) {
                    $('.loading-overlay').removeClass('is-active');
                    if (data == 1) {
                        $("#errormessage").show()
                            .html('<div class="alert alert-warning"><strong>Failed !</strong> please try again.</div>')
                            .fadeOut(5000);
                        $("#buttonsubmit").attr('disabled', false);
                        return false;
                    } else {
                        $("#errormessage").show()
                            .html('<div class="alert alert-success"><strong>Success!</strong> Row Deleted.</div>')
                            .fadeOut(5000);
                        $("#newlist").html(data);
						setTimeout(function(){
							location.reload();
						},1000);

                    }

                    $('#Dailyreport_projectid').val('').trigger('change.select2');
                    $('#Dailyreport_bill_id').val('0').trigger('change.select2');
                    $('#Dailyreport_expensetype_id').html('<option value="">-Select Expense Head-</option>');
                    $('#txtPurchaseType').val("").trigger('change.select2');
                    $('#Dailyreport_expense_type').val('').trigger('change.select2');
                    $('#Dailyreport_employee_id').val('').trigger('change.select2');
                    $('#Dailyreport_bank_id').val('').trigger('change.select2');
                    $('#Dailyreport_vendor_id').html('<option value="">-Select Vendor-</option>');
                    $('#Dailyreport_company_id').val('').trigger('change.select2');
                    $("#txtAmount").attr("readonly", true);
                    $("#txtSgstp").attr("readonly", true);
                    $("#txtCgstp").attr("readonly", true);
                    $("#txtIgstp").attr("readonly", true);
                    $("#txtPurchaseType").attr("disabled", true);
                    $("#txtPaid").attr("readonly", true);
                    $("#Dailyreport_bank_id").attr("disabled", true);
                    $("#txtChequeno").attr("readonly", true);
                    $("#txtSgst1").text("");
                    $("#txtCgst1").text("");
                    $("#txtIgst1").text("");
                    $("#txtTotal1").html("");
                    $("#txtgstTotal").text("");
                }
            })
        }

    })
	function view(id) {
        window.location.href = '<?php echo Yii::app()->createAbsoluteUrl('DailyReport/viewlabour&id='); ?>' + id;
    }
</script>