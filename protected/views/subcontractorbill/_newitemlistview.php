<?php
/* @var $this SubcontractorbillController */
/* @var $data Subcontractorbill */
?>
<?php if ($index == 0) { ?>
<thead>
    <tr>

        <th>Sl No</th>
        <th>Item</th>
        <th>Amount</th>
        <th>Tax Slab (%)</th>
        <th>SGST (%)</th>
        <th>SGST</th>
        <th>CGST (%)</th>
        <th>CGST</th>
        <th>IGST (%)</th>
        <th>IGST</th>
        <th>Total Tax</th>
        <th>Total Amount</th>
    </tr>   
</thead>
<tfoot>
    <tr>
        <th></th>
        <th></th>
        <th class="text-right"><?php echo Controller::money_format_inr($amount,2); ?></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th class="text-right"><?php echo Controller::money_format_inr($tax_amount,2); ?></th>
        <th class="text-right"><?php echo Controller::money_format_inr($total_amount,2); ?></th>
    </tr>   
</tfoot>
<?php } ?>
<tr>
    <td><?php echo $index+1; ?></td>
    <td><?php echo $data->description; ?></td>
    <td class="text-right"><?php echo Controller::money_format_inr($data->amount,2);  ?></td>
    <td><?php echo $data->tax_slab ?></td>
    <td><?php echo $data->sgstp; ?></td>
    <td class="text-right"><?php echo Controller::money_format_inr($data->sgst,2);  ?></td>
    <td><?php echo $data->cgstp; ?></td>
    <td class="text-right"><?php echo Controller::money_format_inr($data->cgst,2);  ?></td>
    <td><?php echo $data->igstp; ?></td>
    <td class="text-right"><?php echo Controller::money_format_inr($data->igst,2);  ?></td>
    <td class="text-right"><?php echo Controller::money_format_inr($data->tax_amount,2); ?></td>
    <td class="text-right"><?php echo Controller::money_format_inr($data->total_amount,2);  ?></td>
</tr>