<?php

class ExpensesController extends Controller
{

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        $controller = Yii::app()->controller->id;
        $hidden_actions =  array();
        $rules = AccessRulesHelper::getRules($controller, $hidden_actions);
        //Additional action rules
        return $rules;
    }

    public function actionDailyEntries()
    {
        $model = new Expenses;
        $id = Yii::app()->user->id;
        $tblpx = Yii::app()->db->tablePrefix;
        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        $newQuery = "";
        foreach ($arrVal as $arr) {
            if ($newQuery) $newQuery .= ' OR';
            $newQuery .= " FIND_IN_SET('" . $arr . "', e.company_id)";
        }
        $currDate = date('Y-m-d', strtotime(date("Y-m-d")));
        $sql = "SELECT e.*, p.name as pname, b.bill_number as billno, "
            . " et.type_name as typename,v.name vname,ba.bank_name as bankname,"
            . " i.inv_no as invoice_no,scp.subcontractor_id as scid "
            . " FROM " . $tblpx . "expenses e "
            . " LEFT JOIN " . $tblpx . "projects p ON e.projectid = p.pid "
            . " LEFT JOIN " . $tblpx . "bills b ON e.bill_id = b.bill_id "
            . " LEFT JOIN " . $tblpx . "expense_type et ON e.exptype = et.type_id "
            . " LEFT JOIN " . $tblpx . "vendors v ON e.vendor_id = v.vendor_id "
            . " LEFT JOIN " . $tblpx . "bank ba ON e.bank_id = ba.bank_id "
            . " LEFT JOIN " . $tblpx . "invoice i ON e.invoice_id = i.invoice_id "
            . " LEFT JOIN " . $tblpx . "subcontractor_payment scp "
            . " ON e.subcontractor_id = scp.payment_id "
            . " WHERE e.date = '" . $currDate . "' AND (" . $newQuery . ")";
        $expenseData = Yii::app()->db->createCommand($sql)->queryAll();

        $sql = "SELECT date FROM " . $tblpx . "expenses "
                . " WHERE created_by =".Yii::app()->user->id
                . " ORDER BY date DESC LIMIT 1";
        $lastdate = Yii::app()->db->createCommand($sql)->queryRow();
        $this->render('daybook', array('model' => $model, 'newmodel' => $expenseData, 'id' => $id, 'lastdate' => $lastdate));
    }
    public function actionDailyEntriesofpending(){
        $model = new Expenses;
        $id = Yii::app()->user->id;
        $tblpx = Yii::app()->db->tablePrefix;
        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        $newQuery = "";
        foreach ($arrVal as $arr) {
            if ($newQuery) $newQuery .= ' OR';
            $newQuery .= " FIND_IN_SET('" . $arr . "', e.company_id)";
        }
        $currDate = date('Y-m-d', strtotime(date("Y-m-d")));
        $sql = "SELECT e.*, p.name as pname, b.bill_number as billno, "
            . " et.type_name as typename,v.name vname,ba.bank_name as bankname,"
            . " i.inv_no as invoice_no,scp.subcontractor_id as scid "
            . " FROM " . $tblpx . "expenses e "
            . " LEFT JOIN " . $tblpx . "projects p ON e.projectid = p.pid "
            . " LEFT JOIN " . $tblpx . "bills b ON e.bill_id = b.bill_id "
            . " LEFT JOIN " . $tblpx . "expense_type et ON e.exptype = et.type_id "
            . " LEFT JOIN " . $tblpx . "vendors v ON e.vendor_id = v.vendor_id "
            . " LEFT JOIN " . $tblpx . "bank ba ON e.bank_id = ba.bank_id "
            . " LEFT JOIN " . $tblpx . "invoice i ON e.invoice_id = i.invoice_id "
            . " LEFT JOIN " . $tblpx . "subcontractor_payment scp "
            . " ON e.subcontractor_id = scp.payment_id "
            . " WHERE e.date = '" . $currDate . "' AND (" . $newQuery . ")";
        $expenseData = Yii::app()->db->createCommand($sql)->queryAll();

        $sql = "SELECT date FROM " . $tblpx . "expenses "
                . " WHERE created_by =".Yii::app()->user->id
                . " ORDER BY date DESC LIMIT 1";
        $lastdate = Yii::app()->db->createCommand($sql)->queryRow();
        $this->render('daybook_pendingbills', array('model' => $model, 'newmodel' => $expenseData, 'id' => $id, 'lastdate' => $lastdate));

    }

    public function actionGetBillDetails()
    {
        $data = explode(",", $_POST["billid"]);
        $billId = $data[1];
        $tblpx = Yii::app()->db->tablePrefix;
        $bill_model = Bills::model()->findByPk($billId);

        $billData = Yii::app()->db->createCommand("SELECT * from " . $tblpx . "billitem WHERE bill_id = " . $billId . " AND (purchaseitem_id IS NOT NULL AND purchaseitem_id != 0) GROUP BY purchaseitem_id")->queryAll();

        $billData_add = Yii::app()->db->createCommand("SELECT * from " . $tblpx . "billitem WHERE bill_id = " . $billId . " AND (purchaseitem_id IS  NULL OR purchaseitem_id = 0)")->queryAll();

        $bill_items = array_merge($billData, $billData_add);

        $purchase_data = Yii::app()->db->createCommand("SELECT {$tblpx}purchase.vendor_id,{$tblpx}purchase.expensehead_id, {$tblpx}purchase.project_id, {$tblpx}bills.company_id FROM {$tblpx}bills LEFT JOIN {$tblpx}purchase ON {$tblpx}bills.purchase_id= {$tblpx}purchase.p_id WHERE {$tblpx}bills.bill_id=" . $billId . "")->queryRow();

        $vendor_data = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}vendors WHERE vendor_id=" . $purchase_data['vendor_id'])->queryRow();

        $billed_amount = Yii::app()->db->createCommand("SELECT SUM(IFNULL(paid,0))as paid_amount FROM {$tblpx}expenses WHERE bill_id=" . $billId)->queryRow();

        $roundoff = Yii::app()->db->createCommand("SELECT round_off as round_off FROM {$tblpx}bills WHERE bill_id=" . $billId)->queryRow();

        $additional_amount =  Yii::app()->db->createCommand()
            ->select('bill_id, sum(amount) as amt')
            ->from($tblpx . 'additional_bill')
            ->where('bill_id = ' . $billId)
            ->queryRow();
        //        $paid_amount = (isset($billed_amount['paid_amount']) || $billed_amount['paid_amount'] != '') ? $billed_amount['paid_amount'] : 0;
        $result["amount"] = 0;
        $result["sgstp"] = 0;
        $result["sgst"] = 0;
        $result["cgstp"] = 0;
        $result["cgst"] = 0;
        $result["igstp"] = 0;
        $result["igst"] = 0;
        $result["total"] = 0;
        $result["expense_name"] = '';
        

        $amount = 0;
        $sgstp = 0;
        $sgst = 0;
        $cgstp = 0;
        $cgst = 0;
        $igstp = 0;
        $igst = 0;
        $sgst_amount = 0;
        $cgst_amount = 0;
        $igst_amount = 0;

        $result["vendor_id"] = $purchase_data['vendor_id'];
        $result["project_id"] = $purchase_data['project_id'];
        $result["company"] = $purchase_data['company_id'];
        $result["vendor_name"] = $vendor_data['name'];
        $result["expensehead_id"] = $purchase_data['expensehead_id'];
        if ($purchase_data['expensehead_id'] != NULL) {
            $expense_data = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}expense_type WHERE type_id=" . $purchase_data['expensehead_id'])->queryRow();
            $result["expense_name"] = $expense_data['type_name'];
        }
        $j = 0;
        if (!empty($bill_items)) {
            foreach ($bill_items as $item) {
                $amount += $result["amount"] + ($item["billitem_amount"] - $item["billitem_discountamount"]);
                $sgstp += $result["sgstp"] + $item["billitem_sgstpercent"];
                $sgst += $result["sgst"] + $item["billitem_sgst"];
                $cgstp += $result["cgstp"] + $item["billitem_cgstpercent"];
                $cgst += $result["cgst"] + $item["billitem_cgst"];
                $igstp += $result["igstp"] + $item["billitem_igstpercent"];
                $igst += $result["igst"] + $item["billitem_igst"];
                $j++;
            }
            $sgstp = $sgstp / $j;
            $cgstp = $cgstp / $j;
            $igstp = $igstp / $j;
        }

        if ($amount != 0 || $amount != '') {
            $sgst_p = ($sgst / $amount) * 100;
            $cgst_p = ($cgst / $amount) * 100;
            $igst_p = ($igst / $amount) * 100;

            $sgst_amount = ($amount * $sgst_p) / 100;
            $cgst_amount = ($amount * $cgst_p) / 100;
            $igst_amount = ($amount * $igst_p) / 100;
        } else {
            $sgst_amount = 0;
            $cgst_amount = 0;
            $igst_amount = 0;
        }

        


        $result["amount"] = $amount;
        $result["sgstp"] = $sgstp;
        $result["sgst"] = $sgst_amount;
        $result["cgstp"] = $cgstp;
        $result["cgst"] = $cgst_amount;
        $result["igstp"] = $igstp;
        $result["igst"] = $igst_amount;
        $result["additional_amount"] = isset($additional_amount['amt'])?$additional_amount['amt']:'';
        $result["billamount"] = isset($bill_model['bill_amount'])?$bill_model['bill_amount']:0;;
        $result["roundoff"] = isset($roundoff['round_off'])?$roundoff['round_off']:'';

        //        $result["paid"] = number_format($paid_amount, 2, '.', '');
        //        $final_amount = ($amount + $sgst_amount + $cgst_amount + $igst_amount);
        //        $balance = $final_amount - $paid_amount;
        //        $balance = ($balance > 0) ? $balance : 0;
        //        $result["balance"] = number_format($balance, 2, '.', '');
        $result["total"] = ($amount + $sgst_amount + $cgst_amount + $igst_amount + $bill_model->round_off);
        if (!empty($additional_amount['amt'])) {
            $result["total"] += $additional_amount['amt'];
        }
        echo json_encode($result);
    }

    public function actionGetDataByDate()
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        $newQuery = "";
        foreach ($arrVal as $arr) {
            if ($newQuery) $newQuery .= ' OR';
            $newQuery .= " FIND_IN_SET('" . $arr . "', e.company_id)";
        }
        $newDate = date('Y-m-d', strtotime($_REQUEST["date"]));
        $expenseData = Yii::app()->db->createCommand("SELECT e.*, p.name as pname, b.bill_number as billno, et.type_name as typename,v.name vname, ba.bank_name as bankname, i.inv_no as invoice_no,scp.subcontractor_id as scid FROM " . $tblpx . "expenses e
                                LEFT JOIN " . $tblpx . "projects p ON e.projectid = p.pid
                                LEFT JOIN " . $tblpx . "bills b ON e.bill_id = b.bill_id
                                LEFT JOIN " . $tblpx . "expense_type et ON e.exptype = et.type_id
                                LEFT JOIN " . $tblpx . "vendors v ON e.vendor_id = v.vendor_id
                                LEFT JOIN " . $tblpx . "bank ba ON e.bank_id = ba.bank_id
                                LEFT JOIN " . $tblpx . "invoice i ON e.invoice_id = i.invoice_id
                                LEFT JOIN " . $tblpx . "subcontractor_payment scp ON e.subcontractor_id = scp.payment_id
                                WHERE e.date = '" . $newDate . "' AND (" . $newQuery . ")")->queryAll();
        $client = $this->renderPartial('newlist', array('newmodel' => $expenseData));
        return $client;
        //echo $newDate;
    }

    public function actionDynamicDropdown()
    {
        $project_id   = $_POST["project"];
        $bill_id      = $_POST["bill"];
        $expId        = $_POST["expId"];
        $tblpx        = Yii::app()->db->tablePrefix;

        $user = Users::model()->findByPk(Yii::app()->user->id);
        $projects = Projects::model()->findByPk($project_id);
        
        $arrVal = explode(',', $user->company_id);
        $newQuery = "";
        $newQuery1 = "";
        $newQuery2 = "";
        foreach ($arrVal as $arr) {
            if ($newQuery) $newQuery .= ' OR';
            if ($newQuery1) $newQuery1 .= ' OR';
            if ($newQuery2) $newQuery2 .= ' OR';
            $newQuery .= " FIND_IN_SET('" . $arr . "', b.company_id)";
            $newQuery1 .= " FIND_IN_SET('" . $arr . "', i.company_id)";
            $newQuery2 .= " FIND_IN_SET('" . $arr . "', et.company_id)";
        }

        if ($bill_id != 0) {
            $data_bills = Yii::app()->db->createCommand("SELECT * FROM " . $tblpx . "expenses LEFT JOIN " . $tblpx . "bills ON " . $tblpx . "expenses.bill_id = " . $tblpx . "bills.bill_id   WHERE " . $tblpx . "expenses.projectid = " . $project_id . " AND " . $tblpx . "expenses.bill_id = " . $bill_id . " AND " . $tblpx . "expenses.type=73 ORDER BY " . $tblpx . "bills.bill_number ASC")->queryRow();
            if (!empty($data_bills)) {
                $data_bills = $data_bills;
            } else {
                $data_bills = array();
            }

            $data_invoices = Yii::app()->db->createCommand("SELECT * FROM " . $tblpx . "expenses LEFT JOIN " . $tblpx . "invoice ON " . $tblpx . "expenses.invoice_id =" . $tblpx . "invoice.invoice_id   WHERE " . $tblpx . "expenses.projectid = " . $project_id . " AND " . $tblpx . "expenses.invoice_id = " . $bill_id . " AND " . $tblpx . "expenses.type=72 ORDER BY " . $tblpx . "invoice.inv_no")->queryRow();
            if (!empty($data_invoices)) {
                $data_invoice = $data_invoices;
            } else {
                $data_invoice = array();
            }

            $data_returns = Yii::app()->db->createCommand("SELECT * FROM " . $tblpx . "expenses LEFT JOIN " . $tblpx . "purchase_return ON " . $tblpx . "expenses.return_id =" . $tblpx . "purchase_return.return_id   WHERE " . $tblpx . "expenses.projectid = " . $project_id . " AND " . $tblpx . "expenses.return_id = " . $bill_id . " AND " . $tblpx . "expenses.type=72 ORDER BY " . $tblpx . "purchase_return.return_number")->queryRow();
            if (!empty($data_returns)) {
                $data_return = $data_returns;
            } else {
                $data_return = array();
            }
        }
        $sql = "SELECT b.bill_id as billid, b.bill_number as "
                . " billnumber FROM " . $tblpx . "bills b "
                . " LEFT JOIN " . $tblpx . "purchase p ON b.purchase_id =p.p_id "
                . " WHERE (" . $newQuery . ") AND b.bill_totalamount IS NOT NULL "
                . " AND b.purchase_id IS NOT NULL AND p.project_id = " . $project_id 
                . " AND b.bill_id NOT IN (Select bill_id FROM " . $tblpx . "expenses "
                . " WHERE bill_id IS NOT NULL "
                . " GROUP BY bill_id HAVING b.bill_amount = SUM(paid)) "
                . " GROUP BY b.bill_id  "
                . " ORDER BY b.bill_id DESC";
        $billData = Yii::app()->db->createCommand($sql)->queryAll();
        $sql = "SELECT  i.invoice_id,i.inv_no AS invoice_no "
                . " FROM " . $tblpx . "invoice i LEFT JOIN " . $tblpx . "projects p "
                . " ON i.project_id = p.pid WHERE (" . $newQuery1 . ") "
                . " AND p.pid = " . $project_id . " AND i.amount >0  "
                . " AND i.invoice_id NOT IN(Select invoice_id from " . $tblpx . "expenses "
                . " WHERE invoice_id IS NOT NULL "
                . " GROUP BY invoice_id HAVING i.amount = SUM(receipt)) "
                . " AND i.invoice_status='saved' "
                . " GROUP BY i.invoice_id ORDER BY i.invoice_id DESC";
        $invoiceData = Yii::app()->db->createCommand($sql)->queryAll();

        $purchase_return = Yii::app()->db->createCommand("SELECT * FROM " . $tblpx . "purchase_return LEFT JOIN " . $tblpx . "bills ON " . $tblpx . "purchase_return.bill_id= " . $tblpx . "bills.bill_id LEFT JOIN " . $tblpx . "purchase ON " . $tblpx . "bills.purchase_id=" . $tblpx . "purchase.p_id WHERE " . $tblpx . "purchase.project_id=" . $project_id . " AND " . $tblpx . "bills.purchase_id IS NOT NULL AND " . $tblpx . "purchase.project_id=" . $project_id . " AND " . $tblpx . "purchase_return.return_id NOT IN(Select return_id from " . $tblpx . "expenses WHERE return_id IS NOT NULL GROUP BY return_id 
        HAVING return_amount = SUM(paidamount)) AND " . $tblpx . "purchase_return.return_totalamount >0 GROUP BY " . $tblpx . "purchase_return.return_id ORDER BY " . $tblpx . "purchase_return.return_number ASC")->queryAll();

        if ($projects->auto_update == 1) {
            $expData      = Yii::app()->db->createCommand("SELECT et.type_id as typeid, et.type_name as typename FROM " . $tblpx . "expense_type et ORDER BY et.type_name ASC")->queryAll();
        } else {
            $expData      = Yii::app()->db->createCommand("SELECT et.type_id as typeid, et.type_name as typename FROM " . $tblpx . "expense_type et LEFT JOIN " . $tblpx . "project_exptype pet ON et.type_id = pet.type_id WHERE pet.project_id = " . $project_id . " ORDER BY et.type_name ASC")->queryAll();
        }

        $billOptions  = "<option value='0'>-Select Bill No/ Invoice No-</option>";
        $expOptions   = "<option value=''>-Select Expense Head-</option>";

        if ($bill_id) {

            if (!empty($billData) || !empty($data_bills)) {
                $billOptions .= " <optgroup label = 'Bill Number'>";
                foreach ($billData as $bData) {
                    $billOptions .= "<option data-id='1' value='1," . $bData["billid"] . "'>" . $bData["billnumber"] . "</option>";
                }
                if (!empty($data_bills)) {
                    $billOptions .= "<option data-id='1' value='1," . $data_bills["bill_id"] . "'>" . $data_bills["bill_number"] . "</option>";
                }
                $billOptions .= " </optgroup>";
            }
            if (!empty($invoiceData) || !empty($data_invoice)) {
                $billOptions .= " <optgroup label = 'Invoice Number'>";
                foreach ($invoiceData as $iData) {
                    $billOptions .= "<option data-id='2' value='2," . $iData["invoice_id"] . "'>" . $iData["invoice_no"] . "</option>";
                }
                if (!empty($data_invoice)) {
                    $billOptions .= "<option data-id='2' value='2," . $data_invoice["invoice_id"] . "'>" . $data_invoice["inv_no"] . "</option>";
                }
                $billOptions .= " </optgroup>";
            }
            if (!empty($purchase_return) || !empty($data_return)) {
                $billOptions .= " <optgroup label = 'Return Number'>";
                foreach ($purchase_return as $iData) {
                    $billOptions .= "<option data-id='3' value='3," . $iData["return_id"] . "'>" . $iData["return_number"] . "</option>";
                }
                if (!empty($data_return)) {
                    $billOptions .= "<option data-id='3' value='3," . $data_return["return_id"] . "'>" . $data_return["return_number"] . "</option>";
                }
                $billOptions .= " </optgroup>";
            }
        } else {

            if (!empty($billData)) {
                $billOptions .= " <optgroup label = 'Bill Number'>";
                foreach ($billData as $bData) {
                    $billOptions .= "<option data-id='1' value='1," . $bData["billid"] . "'>" . $bData["billnumber"] . "</option>";
                }
                if (!empty($data_bills)) {
                    $billOptions .= "<option data-id='1' value='1," . $data_bills["bill_id"] . "'>" . $data_bills["bill_number"] . "</option>";
                }
                $billOptions .= " </optgroup>";
            }
            if (!empty($invoiceData)) {
                $billOptions .= " <optgroup label = 'Invoice Number'>";
                foreach ($invoiceData as $iData) {
                    $billOptions .= "<option data-id='2' value='2," . $iData["invoice_id"] . "'>" . $iData["invoice_no"] . "</option>";
                }
                if (!empty($data_invoice)) {
                    $billOptions .= "<option data-id='2' value='2," . $data_invoice["invoice_id"] . "'>" . $data_invoice["inv_no"] . "</option>";
                }
                $billOptions .= " </optgroup>";
            }
            if (!empty($purchase_return)) {
                $billOptions .= " <optgroup label = 'Return Number'>";
                foreach ($purchase_return as $iData) {
                    $billOptions .= "<option data-id='3' value='3," . $iData["return_id"] . "'>" . $iData["return_number"] . "</option>";
                }
                if (!empty($data_return)) {
                    $billOptions .= "<option data-id='3' value='3," . $data_return["return_id"] . "'>" . $data_return["return_number"] . "</option>";
                }
                $billOptions .= " </optgroup>";
            }
        }




        foreach ($expData as $eData) {
            $expOptions  .= "<option value='" . $eData["typeid"] . "'>" . $eData["typename"] . "</option>";
        }

        if ($bill_id != '') {
            $billsData     = Yii::app()->db->createCommand("SELECT * FROM " . $tblpx . "expenses WHERE projectid = " . $project_id . " AND bill_id = " . $bill_id . " AND type=73 AND exp_id=" . $expId . "")->queryRow();
            $invoicesData     = Yii::app()->db->createCommand("SELECT * FROM " . $tblpx . "expenses WHERE projectid = " . $project_id . " AND invoice_id = " . $bill_id . " AND type=72 AND exp_id=" . $expId . "")->queryRow();
            $returnData     = Yii::app()->db->createCommand("SELECT * FROM " . $tblpx . "expenses WHERE projectid = " . $project_id . " AND return_id = " . $bill_id . " AND type=72 AND exp_id=" . $expId . "")->queryRow();
            if (!empty($billsData)) {
                $result["type"] = 'bill';
            } else if (!empty($invoicesData)) {
                $result["type"] = 'invoice';
            } else {
                $result["type"] = 'return';
            }
        }

        $result["bills"]    = $billOptions;
        $result["expenses"] = $expOptions;
        $result["h"] = 'ddd';
        echo json_encode($result);
    }

    public function actionDynamicBillorInvoice()
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $billData = Yii::app()->db->createCommand("SELECT b.bill_id as billid, b.bill_number as
                        billnumber FROM " . $tblpx . "bills b LEFT JOIN " . $tblpx . "purchase p ON b.purchase_id =
                        p.p_id WHERE b.company_id=" . Yii::app()->user->company_id . " AND b.bill_totalamount IS NOT NULL AND
                        b.purchase_id IS NOT NULL AND b.bill_id NOT IN (Select bill_id 
                                               from " . $tblpx . "expenses WHERE bill_id IS NOT NULL
                                               ) GROUP BY b.bill_id  ORDER BY b.bill_id DESC")->queryAll();
        $invoiceData = Yii::app()->db->createCommand("SELECT i.invoice_id, i.inv_no as invoice_no FROM " . $tblpx . "invoice i LEFT JOIN " . $tblpx . "projects p ON i.project_id = p.pid WHERE i.company_id=" . Yii::app()->user->company_id . " AND i.amount >0 AND i.invoice_id NOT IN(Select invoice_id from " . $tblpx . "expenses WHERE invoice_id IS NOT NULL) GROUP BY i.invoice_id ORDER BY i.invoice_id DESC")->queryAll();
        $billOptions  = "<option value='0'>-Select Bill No/ Invoice No-</option>";
        if (!empty($billData)) {
            $billOptions .= " <optgroup label = 'Bill Number'>";
            foreach ($billData as $bData) {
                $billOptions .= "<option data-id='1' value='1," . $bData["billid"] . "'>" . $bData["billnumber"] . "</option>";
            }
            $billOptions .= " </optgroup>";
        }
        if (!empty($invoiceData)) {
            $billOptions .= " <optgroup label = 'Invoice Number'>";
            foreach ($invoiceData as $iData) {
                $billOptions .= "<option data-id='2' value='2," . $iData["invoice_id"] . "'>" . $iData["invoice_no"] . "</option>";
            }
            $billOptions .= " </optgroup>";
        }
        echo $billOptions;
    }
    public function actionPendingBillsDetails(){
        
        $bill_id='';
        $billval="";
        $bill_arr=array();
       if(isset($_GET['billId'])){
            $bill_id=$_GET['billId'];
       }
       if(!empty($bill_id)){
        $bill_arr = explode(',',$bill_id);
       }
       if(!empty($bill_arr[1])){
            $billval = $bill_arr[1];
       }
       
        $tblpx = Yii::app()->db->tablePrefix;
        $billData = Yii::app()->db->createCommand("SELECT b.bill_id as billid, b.bill_number as
                        billnumber,b.company_id,p.project_id FROM " . $tblpx . "bills b LEFT JOIN " . $tblpx . "purchase p ON b.purchase_id =
                        p.p_id WHERE b.bill_id =".$billval."  AND b.bill_totalamount IS NOT NULL AND
                        b.purchase_id IS NOT NULL ")->queryRow();
        // echo "<pre>" ; print_r($billData)   ;exit;     
        echo json_encode($billData)  ;
        exit;    

    }
    public function actionDynamicBillorInvoiceorReturn()
    {   
//         $tblpx = Yii::app()->db->tablePrefix;
//         $companyId = Yii::app()->user->company_id;
    
//         // Optimized query for bills
//         $billData = Yii::app()->db->createCommand("
//             SELECT b.bill_id as billid, b.bill_number as billnumber
//             FROM " . $tblpx . "bills b
//             LEFT JOIN " . $tblpx . "purchase p ON b.purchase_id = p.p_id
//             WHERE b.company_id = :companyId
//             AND b.bill_totalamount IS NOT NULL
//             AND b.purchase_id IS NOT NULL 
//             AND b.bill_date >= DATE_SUB(CURDATE(), INTERVAL 2 MONTH)
//             AND NOT EXISTS (
//                 SELECT 1
//                 FROM " . $tblpx . "expenses e
//                 WHERE e.bill_id = b.bill_id
//                 HAVING SUM(e.paid) = b.bill_amount
//             )
//             ORDER BY b.bill_id DESC
//         ")->bindParam(':companyId', $companyId)->queryAll();
    
//         // Optimized query for invoices
//         $invoiceData = Yii::app()->db->createCommand("SELECT i.invoice_id, i.inv_no AS invoice_no
//     FROM " . $tblpx . "invoice i
//     LEFT JOIN " . $tblpx . "projects p ON i.project_id = p.pid
//     LEFT JOIN jp_inv_list il ON i.invoice_id = il.inv_id
//     WHERE i.company_id = :companyId
//       AND i.amount > 0
//       AND NOT EXISTS (
//           SELECT 1
//           FROM " . $tblpx . "expenses e
//           WHERE e.invoice_id = i.invoice_id
//           GROUP BY e.invoice_id
//           HAVING ROUND(SUM(il.amount + il.sgst_amount + il.cgst_amount + il.igst_amount), 2) = SUM(e.receipt)
//       )
//       AND i.invoice_status = 'saved'
//       AND i.date >= DATE_SUB(CURDATE(), INTERVAL 2 MONTH)
//     GROUP BY i.invoice_id
//     ORDER BY i.invoice_id DESC")->bindParam(':companyId', $companyId)->queryAll();
    
//         // Generate dropdown options
//         $billOptions = "<option value='0'>-Select Bill No/ Invoice No-</option>";
        
//         $purchase_return = Yii::app()->db->createCommand("
//     SELECT pr.return_id as return_id, pr.return_number as return_number
//     FROM " . $tblpx . "purchase_return pr
//     LEFT JOIN " . $tblpx . "bills b ON pr.bill_id = b.bill_id
//     WHERE pr.company_id = :companyId
//     AND pr.return_totalamount > 0
//     AND pr.return_date >= DATE_SUB(CURDATE(), INTERVAL 2 MONTH)
//     AND NOT EXISTS (
//         SELECT 1
//         FROM " . $tblpx . "expenses e
//         WHERE e.return_id = pr.return_id
//         HAVING SUM(e.paidamount) = pr.return_amount
//     )
//     ORDER BY pr.return_id DESC
// ")->bindParam(':companyId', $companyId)->queryAll();


//         if (!empty($billData)) {
//             $billOptions .= " <optgroup label='Bill Number'>";
//             foreach ($billData as $bData) {
//                 $billOptions .= "<option data-id='1' value='1," . $bData['billid'] . "'>" . $bData['billnumber'] . "</option>";
//             }
//             $billOptions .= " </optgroup>";
//         }
    
//         if (!empty($invoiceData)) {
//             $billOptions .= " <optgroup label='Invoice Number'>";
//             foreach ($invoiceData as $iData) {
//                 $billOptions .= "<option data-id='2' value='2," . $iData['invoice_id'] . "'>" . $iData['invoice_no'] . "</option>";
//             }
//             $billOptions .= " </optgroup>";
//         }
//         if (!empty($purchase_return)) {
//             $billOptions .= " <optgroup label = 'Return Number'>";
//             foreach ($purchase_return as $iData) {
//                 $billOptions .= "<option data-id='3' value='3," . $iData["return_id"] . "'>" . $iData["return_number"] . "</option>";
//             }
            
//             $billOptions .= " </optgroup>";
//         }
    
//         echo $billOptions;

  /** Original Query OF DAYBOOK.php*/
  $tblpx = Yii::app()->db->tablePrefix;
$user = Users::model()->findByPk(Yii::app()->user->id);
$arrVal = explode(',', $user->company_id);
$newQuery = "";
$newQuery1 = "";
$newQuery2 = "";
$newQuery3 = "";
$newQuery4 = "";
foreach ($arrVal as $arr) {
    if ($newQuery)
        $newQuery .= ' OR';
    if ($newQuery1)
        $newQuery1 .= ' OR';
    if ($newQuery2)
        $newQuery2 .= ' OR';
    if ($newQuery3)
        $newQuery3 .= ' OR';
    if ($newQuery4)
        $newQuery4 .= ' OR';
    $newQuery .= " FIND_IN_SET('" . $arr . "', company_id)";
    $newQuery1 .= " FIND_IN_SET('" . $arr . "', id)";
    $newQuery2 .= " FIND_IN_SET('" . $arr . "', b.company_id)";
    $newQuery3 .= " FIND_IN_SET('" . $arr . "', i.company_id)";
    $newQuery4 .= " FIND_IN_SET('" . $arr . "', " . $tblpx . "purchase_return.company_id)";
}
        $companyId = Yii::app()->user->company_id;
    
        // Optimized query for bills
        $sql = "SELECT b.bill_id as billid, b.bill_number as "
    . " billnumber,b.bill_date as billdate "
    . " FROM " . $tblpx . "bills b "
    . " LEFT JOIN " . $tblpx . "purchase p ON b.purchase_id = p.p_id "
    . " WHERE (" . $newQuery2 . ") AND b.bill_totalamount IS NOT NULL AND b.bill_date >= DATE_SUB(CURDATE(), INTERVAL 3 MONTH) AND "
    . " b.purchase_id IS NOT NULL AND b.bill_id NOT IN (Select bill_id "
    . " from " . $tblpx . "expenses e WHERE e.bill_id IS NOT NULL "
    .  " GROUP BY e.bill_id HAVING b.bill_amount = SUM(e.paid)" 
    . "  ) GROUP BY b.bill_id  ORDER BY b.bill_id DESC";
        $billData = Yii::app()->db->createCommand($sql)->queryAll();
    //die($sql);
        // Optimized query for invoices
        $inv_sql = "SELECT i.invoice_id, i.inv_no as invoice_no "
        . " FROM " . $tblpx . "invoice i "
        . " LEFT JOIN jp_inv_list il ON i.invoice_id = il.inv_id"
        . " LEFT JOIN " . $tblpx . "projects p "
        . " ON i.project_id = p.pid WHERE (" . $newQuery3 . ") "
        . " AND i.amount >0 AND i.date >= DATE_SUB(CURDATE(), INTERVAL 3 MONTH) AND i.invoice_id "
        . " NOT IN(Select invoice_id FROM " . $tblpx . "expenses "
        . " WHERE invoice_id IS NOT NULL GROUP BY invoice_id "
        . " HAVING ROUND(il.amount+il.sgst_amount+il.cgst_amount+il.igst_amount,2) = SUM(receipt)) "
        . " AND i.invoice_status='saved'  "
        . " GROUP BY i.invoice_id "
        . " ORDER BY i.invoice_id DESC";
        $invoicesData = Yii::app()->db->createCommand($inv_sql)->queryAll();
        // Generate dropdown options
        $billOptions = "<option value='0'>-Select Bill No/ Invoice No-</option>";
       
        $purchase_return = Yii::app()->db->createCommand("SELECT return_id,return_number FROM " . $tblpx . "purchase_return 
        LEFT JOIN " . $tblpx . "bills 
        ON " . $tblpx . "purchase_return.bill_id=" . $tblpx . "bills.bill_id 
        WHERE " . $tblpx . "purchase_return.return_totalamount >0 AND jp_purchase_return.return_date >= DATE_SUB(CURDATE(), INTERVAL 3 MONTH) 
        AND (" . $newQuery4 . ") AND  " . $tblpx . "purchase_return.return_id 
        NOT IN(Select return_id from " . $tblpx . "expenses WHERE  return_id IS NOT NULL GROUP BY return_id 
        HAVING return_amount = SUM(paidamount)) 
        GROUP BY " . $tblpx . "purchase_return.return_id 
        ORDER BY " . $tblpx . "purchase_return.return_id")->queryAll();
        if (!empty($billData)) {
            $billOptions .= " <optgroup label='Bill Number'>";
            foreach ($billData as $bData) {
                $billOptions .= "<option data-id='1' value='1," . $bData['billid'] . "'>" . $bData['billnumber'] . "</option>";
            }
            $billOptions .= " </optgroup>";
        }
    
        if (!empty($invoiceData)) {
            $billOptions .= " <optgroup label='Invoice Number'>";
            foreach ($invoiceData as $iData) {
                $billOptions .= "<option data-id='2' value='2," . $iData['invoice_id'] . "'>" . $iData['invoice_no'] . "</option>";
            }
            $billOptions .= " </optgroup>";
        }
        if (!empty($purchase_return)) {
            $billOptions .= " <optgroup label = 'Return Number'>";
            foreach ($purchase_return as $iData) {
                $billOptions .= "<option data-id='3' value='3," . $iData["return_id"] . "'>" . $iData["return_number"] . "</option>";
            }
            
            $billOptions .= " </optgroup>";
        }
    
        echo $billOptions;


     }


    public function actionDynamicVendor()
    {
        $expId = $_POST["expid"];
        $tblpx = Yii::app()->db->tablePrefix;

        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        $newQuery = "";
        foreach ($arrVal as $arr) {
            if ($newQuery) $newQuery .= ' OR';
            $newQuery .= " FIND_IN_SET('" . $arr . "', v.company_id)";
        }

        if ($expId != 0) {
            $vendorData = Yii::app()->db->createCommand("SELECT v.vendor_id as vendorid, v.name as vendorname FROM " . $tblpx . "vendors v LEFT JOIN " . $tblpx . "vendor_exptype vet ON v.vendor_id = vet.vendor_id WHERE vet.type_id = " . $expId . " AND (" . $newQuery . ") ORDER BY v.name ASC")->queryAll();
        } else {
            $vendorData = Yii::app()->db->createCommand("SELECT v.vendor_id as vendorid, v.name as vendorname FROM " . $tblpx . "vendors v  WHERE (" . $newQuery . ") ORDER BY v.name ASC")->queryAll();
        }
        $vendorOptions = "<option value=''>-Select Vendor-</option>";
        foreach ($vendorData as $vData) {
            $vendorOptions .= "<option value='" . $vData["vendorid"] . "'>" . $vData["vendorname"] . "</option>";
        }
        echo $vendorOptions;
    }

    public function actionAddDaybook()
    {
        $model = new Expenses;
        $reconmodel = new Reconciliation;
        $tblpx = Yii::app()->db->tablePrefix;
        //echo '<pre>';print_r($_POST['Expenses']);exit;
        if (isset($_POST['Expenses'])) {
            $user = Users::model()->findByPk(Yii::app()->user->id);
            $arrVal = explode(',', $user->company_id);
            $newQuery = "";
            foreach ($arrVal as $arr) {
                if ($newQuery) $newQuery .= ' OR';
                $newQuery .= " FIND_IN_SET('" . $arr . "', company_id)";
            }
            // expense alert mail
            $allocated_budget = array();
            $project_model = Projects::model()->findByPk($_POST['Expenses']['projectid']);
            $company_model = Company::model()->findByPk($_POST['Expenses']['company_id']);
            $project_id = $_POST['Expenses']['projectid'];
            $cheque_no = $_POST['Expenses']['cheque_no'];
            $expense_amount_sql = "SELECT SUM(amount) as amount "
                . " FROM " . $tblpx . "expenses "
                . " WHERE projectid = " . $project_id . " "
                . " AND subcontractor_id IS NULL";
            $expense_amount = Yii::app()->db->createCommand($expense_amount_sql)->queryRow();

            $sub_amount_sql = "SELECT SUM( IFNULL(amount, 0) +"
                . " IFNULL(sgst_amount, 0) + IFNULL(cgst_amount, 0) +"
                . " IFNULL(igst_amount, 0))"
                . " as amount "
                . " FROM " . $tblpx . "subcontractor_payment "
                . " WHERE project_id = " . $project_id . " "
                . " AND approve_status ='Yes'";

            $sub_amount = Yii::app()->db->createCommand($sub_amount_sql)->queryRow();

            $vendor_amount_sql = "SELECT SUM( IFNULL(amount, 0) +"
                . " IFNULL(sgst_amount, 0) + IFNULL(cgst_amount, 0) + "
                . " IFNULL(igst_amount, 0))"
                . " as amount "
                . " FROM " . $tblpx . "dailyvendors "
                . " WHERE project_id = " . $project_id . " "
                . " AND project_id IS NOT NULL";
            $vendor_amount = Yii::app()->db->createCommand($vendor_amount_sql)->queryRow();

            if ($project_model->profit_margin != NULL && $project_model->project_quote != NULL && $company_model->expenses_percentage != NULL && $company_model->expenses_email != NULL) {

                $project_expense = (100 - ($project_model->profit_margin)) * (($project_model->project_quote) / 100);
                $amount = $_POST['Expenses']['amount'] + $expense_amount['amount'] + $sub_amount['amount'] + $vendor_amount['amount'];

                $expenses_percentage = explode(",", $company_model->expenses_percentage);
                $percentage_array = array();
                foreach ($expenses_percentage as $key => $percentage) {
                    $final_amount = ($project_expense * $percentage / 100);

                    if ($amount >= $final_amount) {
                        $percentage_array[] = $percentage;
                    }
                }

                if (!empty($percentage_array)) {
                    $percentage = max($percentage_array);
                    if ($project_model->expense_percentage != $percentage) {
                        $allocated_budget['payment_alert'] = array('percentage' => $percentage, 'amount' => $amount, 'profit_margin' => $project_model->profit_margin, 'project_quote' => $project_model->project_quote, 'project_id' => $_POST['Expenses']['projectid']);
                        $this->savePaymentAlert($allocated_budget['payment_alert']);
                    }
                    $expense_alert = $this->saveExpenseNotifications($project_model, $percentage_array, $_POST['Expenses']['amount']);
                    if (!empty($expense_alert)) {
                        $allocated_budget['expense_advance_alert'] = $expense_alert;
                    }
                } else {
                    $budget_percentage = NULL;
                }
                $update2 = Yii::app()->db->createCommand()->update(
                    $tblpx . 'projects',
                    array('expense_amount' => $amount),
                    'pid=:pid',
                    array(':pid' => $project_id)
                );
            }

            $data = explode(",", $_POST['Expenses']["bill_id"]);
            $billId = $data[0];
            $model->attributes = $_POST['Expenses'];
            if(isset($_POST['Expenses']['accepted_amount']) && $_POST['Expenses']['accepted_amount']!=''){
              $expanse_amount = $_POST['Expenses']['accepted_amount'];
            }else{
                $expanse_amount =  $_POST['Expenses']['expense_amount'];
            }
            $model->expense_amount = $expanse_amount;
            $model->date = date('Y-m-d', strtotime($_POST['Expenses']['date']));
            $model->userid = Yii::app()->user->id;
            $model->created_by = Yii::app()->user->id;
            $model->created_date = date('Y-m-d');
        //    $model->updated_date = date('Y-m-d');
            $model->exptype = (isset($_POST['Expenses']['expensetype_id']) ? $_POST['Expenses']['expensetype_id'] : "");
            $model->reconciliation_status = NULL;
            $model->approval_status = 1;
            if (isset($_POST['Expenses']['petty_type'])) {
                $model->petty_payment_approval = $_POST['Expenses']['petty_type'];
            }
            if ($model->attributes['expense_type'] == 88) {
                $model->bank_id = $model->attributes['bank_id'];
                $model->cheque_no = $cheque_no;
                $model->employee_id = NULL;
                $model->reconciliation_status = 0;
                $reconmodel->reconciliation_table = $tblpx . "expenses";
                $reconmodel->reconciliation_paymentdate = date('Y-m-d', strtotime($_POST['Expenses']['date']));
                $reconmodel->reconciliation_bank = $model->attributes['bank_id'];
                $reconmodel->reconciliation_chequeno = $cheque_no;
                $reconmodel->created_date = date("Y-m-d H:i:s");
                $reconmodel->reconciliation_status = 0;
                $reconmodel->company_id = $model->attributes['company_id'];
            } else {
                if ($model->attributes['expense_type'] == 103) {
                    $model->employee_id = $_POST['Expenses']['employee_id'];
                    $model->bank_id = NULL;
                    $model->cheque_no = NULL;
                } else {
                    $model->bank_id = NULL;
                    $model->cheque_no = NULL;
                    $model->employee_id = NULL;
                }
            }

            if ($model->attributes['bill_id'] == 0) {

                if ($_POST['Expenses']['expensetype_id'] != '') {
                    $model->type = 73;
                    $model->expense_type = $model->attributes['expense_type'];
                    $model->bill_id = NULL;
                    $model->invoice_id = NULL;
                    if ($model->attributes['expense_type'] == 0) {
                        $model->payment_type = NULL;
                        $model->purchase_type = 1;
                    } else {
                        $model->purchase_type = $model->attributes['purchase_type'];
                        $model->payment_type = $model->attributes['expense_type'];
                    }
                    $model->paid = (isset($model->attributes['paid']) ? $model->attributes['paid'] : "");
                    $reconmodel->reconciliation_amount = (isset($model->attributes['paidamount']) ? $model->attributes['paidamount'] : "");
                    $reconmodel->reconciliation_payment = "Payment without Bill";
                } else {
                    $model->type = 72;
                    $model->exptype = NULL;
                    $model->vendor_id = NULL;
                    $model->paid = NULL;
                    if ($model->attributes['expense_type'] == 0) {
                        $model->receipt = 0;
                        $model->purchase_type = 1;
                        $model->payment_type = NULL;
                        $reconmodel->reconciliation_amount = 0;
                    } else {
                        $model->receipt = $model->attributes['amount'];
                        $model->purchase_type = 2;
                        $model->payment_type = $model->attributes['expense_type'];
                        $reconmodel->reconciliation_amount = $model->attributes['amount'];
                    }
                    $model->invoice_id = NULL;
                    $model->bill_id = NULL;
                    $model->expense_type = NULL;
                    $reconmodel->reconciliation_payment = "Receipt without Invoice";
                }
            } else {
                if ($billId == 1) {
                    $model->type = 73;
                    $model->expense_type = $model->attributes['expense_type'];
                    if ($model->attributes['expense_type'] == 0) {
                        $model->payment_type = NULL;
                        $model->purchase_type = 1;
                    } else {
                        $model->purchase_type = $model->attributes['purchase_type'];
                        $model->payment_type = $model->attributes['expense_type'];
                    }
                    $model->bill_id = $data[1];
                    $model->invoice_id = NULL;
                    $model->return_id = NULL;
                    $model->paid = (isset($model->attributes['paid']) ? $model->attributes['paid'] : "");
                    $model->exptype = (isset($_POST['bill_exphead']) ? $_POST['bill_exphead'] : "");
                    $model->vendor_id = (isset($_POST['bill_vendor']) ? $_POST['bill_vendor'] : "");
                    $reconmodel->reconciliation_amount = (isset($model->attributes['paidamount']) ? $model->attributes['paidamount'] : "");
                    $reconmodel->reconciliation_payment = "Bill Payment";
                } else if ($billId == 2) {
                    $model->type = 72;
                    $model->payment_type = $model->attributes['expense_type'];
                    $model->exptype  = (isset($_POST['expensetype_id']) ? $_POST['expensetype_id'] : "");
                    $model->vendor_id = (isset($_POST['vendor_id']) ? $_POST['vendor_id'] : "");;;
                    $model->paid = NULL;
                    $model->receipt = $model->attributes['paidamount'];
                    $model->invoice_id = $data[1];
                    $model->bill_id = NULL;
                    $model->return_id = NULL;
                    $model->expense_type = NULL;
                    $reconmodel->reconciliation_amount = $model->attributes['paidamount'];
                    $reconmodel->reconciliation_payment = "Invoice Receipt";
                } else {
                    $model->type = 72;
                    if ($model->attributes['expense_type'] != 0) {
                        $model->payment_type = $model->attributes['expense_type'];
                    } else {
                        $model->payment_type = NULL;
                        $model->purchase_type = 2;
                    }

                    $model->exptype = $model->exptype;
                    $model->vendor_id = (isset($_POST['bill_vendor']) ? $_POST['bill_vendor'] : "");
                    $model->paid = NULL;
                    $model->receipt = $model->attributes['paidamount'];
                    $model->return_id = $data[1];
                    $model->invoice_id = NULL;
                    $model->bill_id = NULL;
                    $model->expense_type = NULL;
                    $reconmodel->reconciliation_amount = $model->attributes['paidamount'];
                    $reconmodel->reconciliation_payment = "Purchase Return";
                }
            }

            if ($model->payment_type == "103") {
                if ($model->employee_id == "") {
                    $model->addError('employee_id', 'Employee Not selected');
                    $result_ = array('status' => '9');
                    echo json_encode($result_);
                    exit;
                    return false;
                }
            }


            if ($model->save()) {
                //echo '<pre>';print_r($model);exit;
                $lastInsExpId = Yii::app()->db->getLastInsertID();
                if (!empty($allocated_budget)) {
                    $this->projectexpensealert($allocated_budget, $lastInsExpId, 'daybook');
                }
                $reconmodel->reconciliation_parentid = $lastInsExpId;
                if ($_POST['Expenses']['expense_type'] == 88) {
                    $reconmodel->save();
                }
                $user = Users::model()->findByPk(Yii::app()->user->id);
                $arrVal = explode(',', $user->company_id);
                $newQuery = "";
                foreach ($arrVal as $arr) {
                    if ($newQuery) $newQuery .= ' OR';
                    $newQuery .= " FIND_IN_SET('" . $arr . "', e.company_id)";
                }
                $expDate = date('Y-m-d', strtotime($_POST['Expenses']['date']));

                $invoice_amount = BalanceHelper::cashInvoice(
                    $model->payment_type,
                    $model->company_id,
                    $model->employee_id,
                    $model->bank_id,
                    $model->date
                );

                $expense_amount = BalanceHelper::cashExpense(
                    $model->payment_type,
                    $model->company_id,
                    $model->employee_id,
                    $model->exp_id,
                    $model->bank_id,
                    $model->date
                );

                $available_amount = $invoice_amount - $expense_amount;

                if ($model->payment_type == "103") {
                    if ($model->paidamount > $available_amount) {
                        $result_ = array('status' => '6', 'id' => $model->exp_id);
                    } else {
                        $result_ = array('status' => '7', 'id' => $model->exp_id);
                    }
                } else {
                    $result_ = array('status' => '', 'id' => $model->exp_id);
                }
                echo json_encode($result_);
                exit;
            } else {
                $error_message =   $this->setErrorMessage($model->getErrors());
                if ($error_message == 'insufficient balance') {
                    $result_ = array('status' => 5);
                    echo json_encode($result_);
                    exit;
                } else if ($error_message == 'Something went wrong! Check again or contact support team') {
                    $result_ = array('status' => 'a');
                    echo json_encode($result_);
                    exit;
                } else {
                    $result_ = array('status' =>1,'error_message'=>$error_message);
                    echo json_encode($result_);
                    exit;
                }
            }
        }
    }

    public function actionUpdateDaybook() {
        $tblpx = Yii::app()->db->tablePrefix;
        $id = $_POST['Expenses']['txtDaybookId'];        
        $data = explode(",", $_POST['Expenses']["bill_id"]);
        $billId = $data[0];
        $paymentType = $_POST['Expenses']['expense_type'];
        $date = $_POST['Expenses']['date'];
        $amount = $_POST['Expenses']['amount'];
        $expTypeId = $_POST['Expenses']['expensetype_id'];
        $model = Expenses::model()->findByPk($id);
        $premodel = new PreExpenses;
        $reconmodel = Reconciliation::model()->findByAttributes(array(
            'reconciliation_parentid' => $id,
        ));
        
        $created_by = $model->created_by;
        $created_date = $model->created_date;
        $description = $model->description;
        $type = $model->type;
        $cheque_no = $_POST['Expenses']['cheque_no'];

        $transaction = Yii::app()->db->beginTransaction();
        try {
            if (isset($_POST['Expenses'])) {
                if (Yii::app()->user->role == 1) {
                    $model->attributes = $_POST['Expenses'];
                    $model->date   = date('Y-m-d', strtotime($date));
                    $model->exptype = (isset($_POST['Expenses']['expensetype_id']) ? $_POST['Expenses']['expensetype_id'] : "");
                    $model->vendor_id = (isset($_POST['Expenses']['vendor_id']) ? $_POST['Expenses']['vendor_id'] : "");                    
                    $model->cheque_no = $cheque_no;
                    if(empty($reconmodel)){
                        $reconmodel = new Reconciliation;
                        $reconmodel->reconciliation_parentid =$id;
                        $reconmodel->reconciliation_table = $tblpx . "expenses";
                        $reconmodel->created_date = date("Y-m-d H:i:s");
                    }
                    if ($model->expense_type == 88) {                                                                       
                        $reconmodel->reconciliation_bank = $model->bank_id;
                        $reconmodel->reconciliation_chequeno = $model->cheque_no;
                        $reconmodel->reconciliation_status = 0;
                        $reconmodel->company_id = $model->company_id;                        
                        $reconmodel->reconciliation_amount = $model->amount;
                        $reconmodel->reconciliation_payment = "Payment without Bill";                        
                        $reconmodel->reconciliation_paymentdate = date('Y-m-d', strtotime($_POST['Expenses']['date']));                      
                    }

                    $this->getBillData($billId, $expTypeId, $model, $paymentType, $amount, $data);

                    if (!$model->save()) {
                        $success_status = 0;
                        throw new Exception(json_encode($model->getErrors()));
                    } else {
                        $success_status = 1;
                        if ($model->expense_type == 88) {                           
                            if (!$reconmodel->save()) {                                
                                $success_status = 0;
                                throw new Exception(json_encode($reconmodel->getErrors()));
                            } else {                               
                                $success_status = 1;
                            }
                        }
                    }
                } else {
                    
                    $model->approval_status = 0;
                    $premodel->attributes = $_POST['Expenses'];
                    $premodel->ref_id = $id;
                    $this->getBillData($billId, $expTypeId, $premodel, $paymentType, $amount, $data);
                    $sql = "SELECT count(*) as count,MAX(exp_id) as lastId "
                        . " FROM " . $this->tableNameAcc('pre_expenses', 0) . " "
                        . " WHERE ref_id='" . $id . "'";
                    $followupdata = Yii::app()->db->createCommand($sql)->queryRow();

                    if ($followupdata['count'] > 0) {
                        $premodel->followup_id = $followupdata['lastId'];
                    } else {
                        $premodel->followup_id = NULL;
                    }

                    $premodel->payment_type = $paymentType;
                    $premodel->record_grop_id = date('Y-m-d H:i:s');
                    $premodel->approval_status = 0;
                    $premodel->record_action = "update";
                    $premodel->approve_notify_status = 1;
                    $premodel->userid    = Yii::app()->user->id;
                    $premodel->created_by = $created_by;
                    $premodel->created_date = $created_date;
                    $premodel->updated_by = Yii::app()->user->id;
                    $premodel->updated_date = date('Y-m-d H:i:s');                   
                    $premodel->type = $type;
                    $premodel->date = date('Y-m-d', strtotime($date));
                    $premodel->exptype = (isset($_POST['Expenses']['expensetype_id']) ? $_POST['Expenses']['expensetype_id'] : "");
                   
                    if (!$model->save()) {
                        $success_status = 0;
                        throw new Exception(json_encode($model->getErrors()));
                    } else {
                        $success_status = 1;
                        if (!$premodel->save()) {
                            $success_status = 0;
                            throw new Exception(json_encode($premodel->getErrors()));
                        } else {
                            $success_status = 1;
                        }
                    }
                }
            }
            $transaction->commit();
        } catch (Exception $error) {
            $transaction->rollBack();
            $success_status = 0;
            $error =  $error->getMessage();
        } finally {
            if ($success_status == 1) {
                Yii::app()->user->setFlash('success', "Daybook data updated ");
                $result_ = array('status' => '');
                echo json_encode($result_);
                exit;
            } else {
                Yii::app()->user->setFlash('error', $error);
                $result_ = array('status' => 1);
                echo json_encode($result_);
                exit;
            }
        }
    }

    public function setErrorMessage($modelErrors)
    {
        if (!empty($modelErrors)) {
            foreach ($modelErrors as $key => $value) {
                return $value[0];
            }
        }
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $model = new Expenses;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Expenses'])) {
            $model->attributes = $_POST['Expenses'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->expense_id));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Expenses'])) {
            $model->attributes = $_POST['Expenses'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->expense_id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $dataProvider = new CActiveDataProvider('Expenses');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model = new Expenses('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Expenses']))
            $model->attributes = $_GET['Expenses'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Expenses the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Expenses::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Expenses $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'expenses-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionPaymentreport()
    {
        $model = new Expenses();
        $model->unsetAttributes();
        if (isset($_GET['Expenses'])) {

            $model->attributes  = $_GET['Expenses'];
            $fromdate           = $_GET['Expenses']['fromdate'];
            $todate             = $_GET['Expenses']['todate'];
            $company_id         = $_GET['Expenses']['company_id'];
            $companystatus      = 1;
            if (empty($company_id)) {
                $company_id         = Yii::app()->user->company_ids;;
                $companystatus      = 2;
            }
            $projectSort        = $_GET['Expenses']['project_order'];
        } else {
            $fromdate   = date('d-M-Y');
            $todate     = date('d-M-Y');
            $company_id = Yii::app()->user->company_ids;
            $companystatus      = 2;
            $projectSort        = "";
        }
        $paymentcash = $model->paymentreportcash($fromdate, $todate, $company_id, $companystatus, $projectSort);
        $paymentbank = $model->paymentreportbank($fromdate, $todate, $company_id, $companystatus, $projectSort);


                //echo "<pre>";
            // print_r($financialdata1->getData()); die;

        $this->render('paymentreportview', array(
            'model' => $model,
            'paymentcash' => $paymentcash,
            'paymentbank' => $paymentbank,
            'fromdate' => $fromdate,
            'todate' => $todate,
            'companystatus' => $companystatus,
            'company_id' => $company_id,
            'project_order' => $projectSort,
        ));
    }

    public function actionSavetopdf($fromdate, $todate, $company_id, $companystatus)
    {
        $this->logo = $this->realpath_logo;
        $model = new Expenses();
        $model->unsetAttributes();


        if (!empty($fromdate) && !empty($todate)) {
            $duration = "from $fromdate to $todate";
        } else if (!empty($fromdate) && empty($todate)) {
            $duration = "from $fromdate onwards";
        } else if (empty($fromdate) && !empty($todate)) {
            $duration = "as on $todate";
        } else {
            $start = date('d-M-Y');
            $to = date('d-M-Y');
            $duration = "from $start to $to";
        }

        $paymentcash = $model->paymentreportcash($fromdate, $todate, $company_id, $companystatus, "");
        $paymentbank = $model->paymentreportbank($fromdate, $todate, $company_id, $companystatus, "");

        $cash_data = json_decode($this->entries($fromdate, $todate, $paymentcash), true);
        $prev_entries = $cash_data['finaldata'];
        $bank_data = json_decode($this->entries($fromdate, $todate, $paymentbank), true);
        $prev_entries2 = $bank_data['finaldata'];
        $mPDF1 = Yii::app()->ePdf->mPDF();
        $mPDF1 = Yii::app()->ePdf->mPDF('', 'A4');
        
        $sql = 'SELECT template_name '
                . 'FROM jp_quotation_template '
                . 'WHERE status="1"';
        $selectedtemplate =Yii::app()->db->createCommand($sql)->queryScalar();
        
        $template = $this->getTemplate($selectedtemplate);
        $mPDF1->SetHTMLHeader($template['header']);
        $mPDF1->SetHTMLFooter($template['footer']);
        //$mPDF1->AddPage('','', '', '', '', 0,0,40, 30, 10,0);
        $mPDF1->AddPage('','', '', '', '', 0,0,50, 40, 10,0);
        $mPDF1->WriteHTML($this->renderPartial('paymentreportpdf', array(
            'model' => $model,
            'duration' => $duration,
            'entries' => $prev_entries,
            'entries2' => $prev_entries2,
            'company_id' => $company_id,
            'cash_total' => $cash_data['cash_total'],
            'bank_total' => $bank_data['cash_total'],
        ), true));
        $filename = "Receiptreport" . $duration;
        $mPDF1->Output($filename . '.pdf', 'D');
    }

    private function entries($fromdate, $todate, $datamodel)
    {

        $tblpx = Yii::app()->db->tablePrefix;
        $finaldata = "";
        $cash_grandtotal = 0.00;
        $reportdata = $datamodel->getData();
        if (!empty($reportdata)) {
            $i = 0;

            foreach ($reportdata as $data) {
                $i++;
                $projectid = $data['projectid'];
                $client = Yii::app()->db->createCommand("SELECT {$tblpx}clients.name as client, {$tblpx}projects.site, {$tblpx}projects.name as projectname FROM {$tblpx}projects LEFT JOIN {$tblpx}clients  ON {$tblpx}projects.client_id = {$tblpx}clients.cid  WHERE {$tblpx}projects.pid = {$projectid}")->queryRow();



                $cash_grandtotal += $data['paidamount'];

                $finaldata .= "<tr>
                                    <td>" . $i . "</td>
                                    <td>" . $client['projectname'] . "</td>
                                    <td>" . $client['client'] . "</td>
                                    <td>" . $client['site'] . "</td>
                                    <td>" . date('d-M-Y', strtotime($data['date'])) . "</td>
                                    <td align='right'>" . Controller::money_format_inr($data['paidamount'], 2) . "</td>
                                </tr>";
            }
        } else {

            $finaldata .= "<tr>
                                <td colspan='6'>No results found.</td>
                            </tr>";
        }
        $return_data = array('finaldata' => $finaldata, 'cash_total' => $cash_grandtotal);
        return json_encode($return_data);
    }

    public function actionSavetoExcel($fromdate, $todate, $company_id, $companystatus)
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $model = new Expenses();
        $model->unsetAttributes();

        if (!empty($fromdate) && !empty($todate)) {
            $duration = "from $fromdate to $todate";
        } else if (!empty($fromdate) && empty($todate)) {
            $duration = "from $fromdate onwards";
        } else if (empty($fromdate) && !empty($todate)) {
            $duration = "as on $todate";
        } else {
            $start = date('d-M-Y');
            $to = date('d-M-Y');
            $duration = "from $start to $to";
        }

        $paymentcash = $model->paymentreportcash($fromdate, $todate, $company_id, $companystatus, "");
        $paymentbank = $model->paymentreportbank($fromdate, $todate, $company_id, $companystatus, "");

        //echo "<pre>";print_r($fromdate);print_r($todate);print_r($paymentcash);die();

        $data = array();
        $data2 = array();
        $arraylabel = array('Cash In Hand ' . $fromdate . ' to ' . $todate, '', '', '');

        foreach ($paymentcash->getData() as $record) {

            $data[] = $record;
        }
        foreach ($paymentbank->getData() as $record) {

            $data2[] = $record;
        }

        $finaldata = array();
        $cash_grandtotal = 0;
        $x = 0;
        $i = 0;
        $k = 0;
        $finaldata[0][] = 'Project';
        $finaldata[0][] = 'Party Name';
        $finaldata[0][] = 'Site';
        $finaldata[0][] = 'Date';
        $finaldata[0][] = 'Receipt Amount';
        $total_amount = $this->getData($data);
        $finaldata[1][] = '';
        $finaldata[1][] = '';
        $finaldata[1][] = '';
        $finaldata[1][] = 'Total';
        $finaldata[1][] = Controller::money_format_inr($total_amount, 2);

        foreach ($data as $key => $datavalue) {
            $projectid = $datavalue['projectid'];
            $client = Yii::app()->db->createCommand("SELECT {$tblpx}clients.name as client, {$tblpx}projects.site, {$tblpx}projects.name as projectname FROM {$tblpx}projects LEFT JOIN {$tblpx}clients  ON {$tblpx}projects.client_id = {$tblpx}clients.cid  WHERE {$tblpx}projects.pid = {$projectid}")->queryRow();

            $finaldata[$key + 2][] = $client['projectname'];
            $finaldata[$key + 2][] = $client['client'];
            $finaldata[$key + 2][] = $client['site'];
            $finaldata[$key + 2][] = date('d-M-Y', strtotime($datavalue['date']));
            $finaldata[$key + 2][] = Yii::app()->Controller->money_format_inr($datavalue['paidamount'], 2);

            $cash_grandtotal += $datavalue['paidamount'];
            $x = $key + 3;
        }

        $finaldata[$x][] = '';
        $finaldata[$x][] = '';
        $finaldata[$x][] = '';
        $finaldata[$x][] = '';
        $finaldata[$x + 1][] = 'Bank Balance ' . $fromdate . ' to ' . $todate;
        $finaldata[$x + 2][] = 'Project';
        $finaldata[$x + 2][] = 'Party Name';
        $finaldata[$x + 2][] = 'Site';
        $finaldata[$x + 2][] = 'Date';
        $finaldata[$x + 2][] = 'Receipt Amount';
        $bank_total_amount = $this->getData($data2);
        $finaldata[$x + 3][] = '';
        $finaldata[$x + 3][] = '';
        $finaldata[$x + 3][] = '';
        $finaldata[$x + 3][] = '';
        $finaldata[$x + 3][] = Controller::money_format_inr($bank_total_amount, 2);

        $i = $x + 4;


        $cash_grandtotal = 0;
        foreach ($data2 as $datavalue) {
            $projectid = $datavalue['projectid'];
            $client = Yii::app()->db->createCommand("SELECT {$tblpx}clients.name as client, {$tblpx}projects.site, {$tblpx}projects.name as projectname FROM {$tblpx}projects LEFT JOIN {$tblpx}clients  ON {$tblpx}projects.client_id = {$tblpx}clients.cid  WHERE {$tblpx}projects.pid = {$projectid}")->queryRow();

            $finaldata[$i][] = $client['projectname'];
            $finaldata[$i][] = $client['client'];
            $finaldata[$i][] = $client['site'];
            $finaldata[$i][] = date('d-M-Y', strtotime($datavalue['date']));
            $finaldata[$i][] = Yii::app()->Controller->money_format_inr($datavalue['paidamount'], 2);

            $cash_grandtotal += $datavalue['paidamount'];
            $i++;
        }
        $finaldata[$i][] = '';
        $finaldata[$i][] = '';
        $finaldata[$i][] = '';
        $finaldata[$i][] = '';



        //echo "<pre>";print_r($data2);die();
        Yii::import('ext.ECSVExport');

        $csv = new ECSVExport($finaldata);

        $csv->setHeaders($arraylabel);
        $csv->setToAppend();
        $contentdata = $csv->toCSV();
        $csvfilename = 'Payment Report' . date('d_M_Y') . '.csv';
        Yii::app()->getRequest()->sendFile($csvfilename, $contentdata, "text/csv", false);
        exit();
    }

    public function actionNewlist()
    {

        $model = new Expenses('search');
        $tblpx = Yii::app()->db->tablePrefix;
        $model->unsetAttributes();  // clear any default values
        //echo "<pre>"; print_r($_REQUEST);exit;
        if (isset($_REQUEST['Expenses']))
            $model->attributes = $_REQUEST['Expenses'];
        if(isset($_REQUEST['ledger']) && $_REQUEST['ledger']==1){
            $page = 'daybookledger';
        }else{ 
            $page = 'newlist1';
        }
        $where = 'WHERE 1=1';
        if (isset($_REQUEST['Expenses']['userid'])) {
            $userid = $_REQUEST['Expenses']['userid'];
            $where .= ' AND ' . $tblpx . 'expenses.userid LIKE "' . $userid . '%"';
        }

        if (!empty($_REQUEST['Expenses']['projectid'])) {
            $where .= ' AND projectid = "' . $_REQUEST['Expenses']['projectid'] . '"';
        }
        if (!empty($_REQUEST['Expenses']['vendor_id'])) {
            $where .= ' AND ' . $tblpx . 'expenses.vendor_id = ' . $_REQUEST['Expenses']['vendor_id'] . '';
        }
        if (!empty($_REQUEST['Expenses']['purchase_type'])) {
            $where .= ' AND ' . $tblpx . 'expenses.purchase_type = ' . $_REQUEST['Expenses']['purchase_type'] . '';
        }
        if (!empty($_REQUEST['Expenses']['expense_type'])) {
           if($_REQUEST['Expenses']['expense_type'] == '107'){
            $where .= ' AND (' . $tblpx . 'expenses.expense_type = ' . 0 . ' )';
           }else{
            $where .= ' AND (' . $tblpx . 'expenses.expense_type = ' . $_REQUEST['Expenses']['expense_type'] . ' )';
           }
            
        }

        if (!empty($_REQUEST['Expenses']['bill_retunid'])) {
            $no_type = explode(',', $_REQUEST['Expenses']['bill_retunid']);
            if ($no_type['0'] == 1) {
                $where .= ' AND (' . $tblpx . 'expenses.bill_id = ' . $no_type['1'] . ' )';
            } else {
                $where .= ' AND (' . $tblpx . 'expenses.return_id = ' . $no_type['1'] . ' )';
            }
        }
        if (!empty($_REQUEST['Expenses']['subcontractor_id'])) {
            $paymentid = Yii::app()->db->createCommand("SELECT payment_id FROM {$tblpx}subcontractor_payment WHERE subcontractor_id = " . $_REQUEST['Expenses']['subcontractor_id'] . "")->queryAll();
            foreach ($paymentid as $id) {
                $pid[] = $id["payment_id"];
            }
            $paymentids = implode(", ", $pid);
            $where .= ' AND (' . $tblpx . 'expenses.subcontractor_id IN (' . $paymentids . ') )';
        }

        if (!empty($_REQUEST['Expenses']['type'])) {
            
            $where .= ' AND jp_expenses.type = ' . $_REQUEST['Expenses']['type'] ;
        }
        //die($where);

        $fromdate    = (isset($_REQUEST['Expenses']['fromdate']) ? 
                        ( date('Y-m-d', strtotime($_REQUEST['Expenses']['fromdate']))):
                        (date("Y-m-d", strtotime('-30 days', strtotime(date("Y-m-d"))))));

        $todate      = (isset($_REQUEST['Expenses']['todate']) ?
                        (date('Y-m-d', strtotime($_REQUEST['Expenses']['todate']))): 
                        date("Y-m-d"));
                    
                       
       // if(!isset($_REQUEST['ledger'])){
            if (!empty($fromdate) && !empty($todate)) {           
                $where .= ' AND date between "' . $fromdate . '" and"' . $todate . '"';
            } else {
                if (!empty($fromdate)) {               
                    $where .= ' AND date >= "' . $fromdate . '"';
                }
                if (!empty($todate)) {                
                    $where .= ' AND date <= "' . $todate . '"';
                }
            }
       // }
       if(isset($_REQUEST['ledger']) && $_REQUEST['ledger']==1){
            $fromdate    = (isset($_REQUEST['fromdate']) ? 
                        ( date('Y-m-d', strtotime($_REQUEST['fromdate']))):
                        (date("Y-m-d", strtotime('-30 days', strtotime(date("Y-m-d"))))));

            $todate      = (isset($_REQUEST['todate']) ?
                            (date('Y-m-d', strtotime($_REQUEST['todate']))): 
                            date("Y-m-d"));
                            if (!empty($fromdate) && !empty($todate)) {           
                $where .= ' AND date between "' . $fromdate . '" and"' . $todate . '"';
            } else {
                if (!empty($fromdate)) {               
                    $where .= ' AND date >= "' . $fromdate . '"';
                }
                if (!empty($todate)) {                
                    $where .= ' AND date <= "' . $todate . '"';
                }
            }
       }

        if (!empty($_REQUEST['company_id'])) {
            $company_id = $_REQUEST['company_id'];
            $newQuery = " FIND_IN_SET('" . $company_id . "', {$tblpx}expenses.company_id)";
        } else {
            $company_id = "";
            $user = Users::model()->findByPk(Yii::app()->user->id);
            $arrVal = explode(',', $user->company_id);
            $newQuery = "";
            foreach ($arrVal as $arr) {
                if ($newQuery) $newQuery .= ' OR';
                $newQuery .= " FIND_IN_SET('" . $arr . "', {$tblpx}expenses.company_id)";
            }
        }

        $where .= ' AND (' . $newQuery . ')';

        $grantotal = $model->grandtotalcredit($where, $company_id);
        $granddebitamount = $model->grandtotaldebit($where, $company_id);
        
        
        $data = Controller::getDaybookTransctions($where);
        // echo $where;die;
        // echo "<pre>";print_r($data);exit;
       
        $dataprovider = new CArrayDataProvider($data, array(
            'id' => 'user',
            'keyField' => 'exp_id',
            //'pagination' => array('pageSize' => 20)
            'pagination' => false,
        ));
        //echo "<pre>";print_r ($dataprovider);exit;
        
        $this->render($page, array(
            'model' => $model, 'grandtotal' => $grantotal, 'granddebitamount'=>$granddebitamount,'company_id' => $company_id,
            'dataProvider' => $dataprovider ,'fromdate'=>$fromdate,'todate'=>$todate,'daybookdata'=>$data
        ));
    }

    public function actionExpensereport()
    {

        $year = date('Y');
        $month = date('m');
        $monthpre = number_format((date('m') - 1), 2);

        if (isset($_POST['monthlyreport'])) {
            $year = $_POST['year'];
            $month = $_POST['month'];
        }

        $date = $year . '-' . $month . '-01';
        $predate = date('Y-m-d', strtotime($date));

        /* display data as table */
        $tblpx = Yii::app()->db->tablePrefix;
        $qry = "SELECT cl.name as clientname,e.expense_date, p.name,e.expense_totalamount,e.expense_description,t.type_name,s.caption,e.expense_id,e.type,e.vendor_id,e.purchase_type,e.paidamount,CONCAT('',u.first_name,u.last_name) as username
                               FROM " . $tblpx . "expenses as e
                               left join " . $tblpx . "projects as p on p.pid=e.project_id
                               left join " . $tblpx . "expense_type as t on t.type_id=e.expensetype_id
                               left join " . $tblpx . "status as s on s.sid=e.payment_type 
                               left join " . $tblpx . "users as u on u.userid=e.user_id  
                               left join " . $tblpx . "clients as cl on cl.cid=p.client_id  
                               where YEAR(e.expense_date) = '" . $year . "' AND MONTH(e.expense_date) = '" . $month . "'  ORDER BY t.type_id ASC,e.project_id ASC";
        //echo $qry; die;
        $data = Yii::app()->db->createCommand($qry)->queryAll();



        //,user.first_name  as first_name
        $totalqry = "SELECT *,p.name as expense_name,e.exp_type as expensetype FROM " . $tblpx . "dailyexpense  as e
                    left join " . $tblpx . "company_expense_type as p on p.company_exp_id=e.exp_type_id"
            //  inner join " . $tblpx . "users as user on e.created_by = user.userid"
            . " WHERE YEAR(e.date)='$year' AND MONTH(e.date)='$month' order by company_exp_id";


        $dailyexpenses = Yii::app()->db->createCommand($totalqry)->queryAll();

        /* display data as table */



        /* calculation for opening balanace */

        $opening_balance = 0;
        $credit = $debit = $totalresult = $receiptexpense = $paymentexpense = $creditdebitdiffer = $receiptpaymentdiff = 0;




        $qrydebitcredit = "SELECT e.type,e.expense_totalamount FROM " . $tblpx . "expenses as e WHERE   e.expense_date < '$predate'";

        // echo $qrydebitcredit;die();
        $monthdebitcredit = Yii::app()->db->createCommand($qrydebitcredit)->queryAll();
        if ($monthdebitcredit != NULL) {
            foreach ($monthdebitcredit as $debitcredit) {
                if ($debitcredit['type'] == 72) {
                    $credit = $credit + $debitcredit['expense_totalamount'];
                } else if ($debitcredit['type'] == 73) {
                    $debit = $debit + $debitcredit['expense_totalamount'];
                }
            }
        }

        $creditdebitdiffer = $credit - $debit;



        $totalqrys = "SELECT *,p.name as expense_name,e.exp_type as expensetype FROM " . $tblpx . "dailyexpense  as e
                    left join " . $tblpx . "company_expense_type as p on p.company_exp_id=e.exp_type_id
                    WHERE date < '$predate'";

        //    "SELECT date,sum(amount) as amt FROM " . $tblpx . "dailyexpense WHERE date < '$predate'";
        $totaldailyexpenses = Yii::app()->db->createCommand($totalqrys)->queryAll();

        //echo '<pre>';print_r($totaldailyexpenses);die();

        foreach ($totaldailyexpenses as $totaldaily) {
            if ($totaldaily['expensetype'] == 0) {
                $receiptexpense = $receiptexpense + $totaldaily['amount'];
            } else if ($totaldaily['expensetype'] == 1) {
                $paymentexpense = $paymentexpense + $totaldaily['amount'];
            }
        }

        $receiptpaymentdiff = $receiptexpense - $paymentexpense;
        //        echo $receiptexpense."<br>"; 
        //        echo $paymentexpense."<br>";
        //        echo $credit."<br>";
        //        echo $debit;exit();

        $opening_balance = ($creditdebitdiffer) + ($receiptpaymentdiff);
        //echo $opening_balance;die();
        //$opening_balance= ($credit-$debit) +($receiptexpense-$paymentexpense)+($receiptamt-$paymentamt);


        /* calculation for opening balance */


        $model = new Expenses('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Expenses']))
            $model->attributes = $_GET['Expenses'];

        $this->render('expensereport', array('currentYear' => $year, 'currentmonth' => $month, 'opening_balance' => $opening_balance, 'model' => $data, 'dailyexpenses' => $dailyexpenses));
    }

    public function actionSavetopdfmonthly($year, $month)
    {
        $date = $year . '-' . $month . '-01';
        $predate = date('Y-m-d', strtotime($date));



        /* display data as table */
        $tblpx = Yii::app()->db->tablePrefix;
        $qry = "SELECT cl.name as clientname,e.expense_date, p.name,e.expense_totalamount,e.expense_description,t.type_name,s.caption,e.expense_id,e.type,e.vendor_id,e.purchase_type,e.paidamount,CONCAT('',u.first_name,u.last_name) as username
                               FROM " . $tblpx . "expenses as e
                               left join " . $tblpx . "projects as p on p.pid=e.project_id
                               left join " . $tblpx . "expense_type as t on t.type_id=e.expensetype_id
                               left join " . $tblpx . "status as s on s.sid=e.payment_type 
                               left join " . $tblpx . "users as u on u.userid=e.user_id  
                               left join " . $tblpx . "clients as cl on cl.cid=p.client_id  
                               where YEAR(e.expense_date) = '" . $year . "' AND MONTH(e.expense_date) = '" . $month . "'  ORDER BY t.type_id ASC,e.project_id ASC";
        //echo $qry; die;
        $data = Yii::app()->db->createCommand($qry)->queryAll();




        $totalqry = "SELECT *,p.name as expense_name,e.exp_type as expensetype FROM " . $tblpx . "dailyexpense  as e
                    left join " . $tblpx . "company_expense_type as p on p.company_exp_id=e.exp_type_id"
            //inner join " . $tblpx . "users as user on e.created_by = user.userid "
            . " WHERE YEAR(e.date)='$year' AND MONTH(e.date)='$month' order by company_exp_id";


        $dailyexpenses = Yii::app()->db->createCommand($totalqry)->queryAll();

        /* display data as table */



        /* calculation for opening balanace */

        $opening_balance = 0;
        $credit = $debit = $totalresult = $receiptexpense = $paymentexpense = 0;




        $qrydebitcredit = "SELECT e.type,e.expense_totalamount FROM " . $tblpx . "expenses as e WHERE   e.expense_date < '$predate'";

        // echo $qrydebitcredit;die();
        $monthdebitcredit = Yii::app()->db->createCommand($qrydebitcredit)->queryAll();
        foreach ($monthdebitcredit as $debitcredit) {
            if ($debitcredit['type'] == 72) {
                $credit = $credit + $debitcredit['expense_totalamount'];
            } else if ($debitcredit['type'] == 73) {
                $debit = $debit + $debitcredit['expense_totalamount'];
            }
        }




        $totalqrys = "SELECT *,p.name as expense_name,e.exp_type as expensetype FROM " . $tblpx . "dailyexpense  as e
                    left join " . $tblpx . "company_expense_type as p on p.company_exp_id=e.exp_type_id
                    WHERE date < '$predate'";

        //    "SELECT date,sum(amount) as amt FROM " . $tblpx . "dailyexpense WHERE date < '$predate'";
        $totaldailyexpenses = Yii::app()->db->createCommand($totalqrys)->queryAll();
        //echo "<pre>"; print_r($dailyexpenses);

        foreach ($totaldailyexpenses as $totaldaily) {
            if ($totaldaily['expensetype'] == 0) {
                $receiptexpense = $receiptexpense + $totaldaily['amount'];
            } else if ($totaldaily['expensetype'] == 1) {
                $paymentexpense = $paymentexpense + $totaldaily['amount'];
            }
        }

        //        echo $receiptexpense."<br>"; 
        //        echo $paymentexpense."<br>";
        //        echo $credit."<br>";
        //        echo $debit;exit();

        $opening_balance = ($credit - $debit) + ($receiptexpense - $paymentexpense);

        //$opening_balance= ($credit-$debit) +($receiptexpense-$paymentexpense)+($receiptamt-$paymentamt);


        /* calculation for opening balance */


        $mPDF1 = Yii::app()->ePdf->mPDF();
        # You can easily override default constructor's params
        $mPDF1 = Yii::app()->ePdf->mPDF('', 'A4');
        //$mPDF1->WriteHTML($stylesheet, 1);
        //$mPDF1->SetHtmlHeader("Your header text here");
        //$mPDF1->SetHtmlFooter("Your footer text here");
        $mPDF1->WriteHTML($this->renderPartial('monthlyreportpdf', array(
            'model' => $data,
            'dailyexpenses' => $dailyexpenses,
            'opening_balance' => $opening_balance,
            'currentmonth' => $month,
            'currentYear' => $year,
        ), true));
        $filename = "Monthly Report" . $duration;
        $mPDF1->Output($filename . '.pdf', 'D');
    }

    public function actionSavetoexcelmonthly($year, $month)
    {


        $date = $year . '-' . $month . '-01';
        $predate = date('Y-m-d', strtotime($date));
        setlocale(LC_MONETARY, 'en_IN');
        /* display data as table */
        $tblpx = Yii::app()->db->tablePrefix;
        $qry = "SELECT cl.name as clientname,e.expense_date, p.name,e.expense_amount,e.expense_description,t.type_name,s.caption,e.expense_id,e.type,e.vendor_id,e.purchase_type,e.paidamount,CONCAT('',u.first_name,u.last_name) as username
                               FROM " . $tblpx . "expenses as e
                               left join " . $tblpx . "projects as p on p.pid=e.project_id
                               left join " . $tblpx . "expense_type as t on t.type_id=e.expensetype_id
                               left join " . $tblpx . "status as s on s.sid=e.payment_type 
                               left join " . $tblpx . "users as u on u.userid=e.user_id  
                               left join " . $tblpx . "clients as cl on cl.cid=p.client_id  
                               where YEAR(e.expense_date) = '" . $year . "' AND MONTH(e.expense_date) = '" . $month . "'   ORDER BY  t.type_id ASC,e.project_id ASC";
        //  echo $qry; die;
        $data = Yii::app()->db->createCommand($qry)->queryAll();




        $totalqry = "SELECT *,p.name as expense_name,e.exp_type as expensetype FROM " . $tblpx . "dailyexpense  as e
                    left join " . $tblpx . "company_expense_type as p on p.company_exp_id=e.exp_type_id"
            //inner join " . $tblpx . "users as user on e.created_by = user.userid"
            . " WHERE YEAR(e.date)='$year' AND MONTH(e.date)='$month' order by company_exp_id";


        $dailyexpenses = Yii::app()->db->createCommand($totalqry)->queryAll();

        /* display data as table */



        /* calculation for opening balanace */

        $opening_balance = 0;
        $credit = $debit = $totalresult = $receiptexpense = $paymentexpense = 0;




        $qrydebitcredit = "SELECT e.type,e.expense_totalamount FROM " . $tblpx . "expenses as e WHERE   e.expense_date < '$predate'";

        // echo $qrydebitcredit;die();
        $monthdebitcredit = Yii::app()->db->createCommand($qrydebitcredit)->queryAll();
        foreach ($monthdebitcredit as $debitcredit) {
            if ($debitcredit['type'] == 72) {
                $credit = $credit + $debitcredit['expense_totalamount'];
            } else if ($debitcredit['type'] == 73) {
                $debit = $debit + $debitcredit['expense_totalamount'];
            }
        }




        $totalqrys = "SELECT *,p.name as expense_name,e.exp_type as expensetype FROM " . $tblpx . "dailyexpense  as e
                    left join " . $tblpx . "company_expense_type as p on p.company_exp_id=e.exp_type_id
                    WHERE date < '$predate'";

        //    "SELECT date,sum(amount) as amt FROM " . $tblpx . "dailyexpense WHERE date < '$predate'";
        $totaldailyexpenses = Yii::app()->db->createCommand($totalqrys)->queryAll();
        //echo "<pre>"; print_r($dailyexpenses);

        foreach ($totaldailyexpenses as $totaldaily) {
            if ($totaldaily['expensetype'] == 0) {
                $receiptexpense = $receiptexpense + $totaldaily['amount'];
            } else if ($totaldaily['expensetype'] == 1) {
                $paymentexpense = $paymentexpense + $totaldaily['amount'];
            }
        }

        //        echo $receiptexpense."<br>"; 
        //        echo $paymentexpense."<br>";
        //        echo $credit."<br>";
        //        echo $debit;exit();

        $opening_balance = ($credit - $debit) + ($receiptexpense - $paymentexpense);

        //$opening_balance= ($credit-$debit) +($receiptexpense-$paymentexpense)+($receiptamt-$paymentamt);


        /* calculation for opening balance */

        $finaldata = array();
        $arraylabel = array("DAILY EXPENSE REPORT Of " . $month . ',' . $year, '', '', '', '');

        $credit = 0;
        $debit = 0;
        $typenew = '';
        $flag = 0;
        $flag1 = 0;
        // $finaldata[0][]='Opening Balance';
        // $finaldata[0][]=$opening_balance;
        $finaldata[0][] = '';
        $finaldata[0][] = '';
        $finaldata[0][] = '';
        $finaldata[0][] = '';
        $finaldata[0][] = '';

        $finaldata[1][] = 'Daily Expense';
        $finaldata[1][] = '';
        $finaldata[1][] = '';
        $finaldata[1][] = '';
        $finaldata[1][] = '';
        $finaldata[1][] = '';
        $finaldata[1][] = '';


        $finaldata[2][] = 'Sl no';
        $finaldata[2][] = 'Date';
        $finaldata[2][] = 'Expense Type';
        $finaldata[2][] = 'Receipt Type';
        $finaldata[2][] = 'Receipt';
        $finaldata[2][] = 'Expenses';
        $finaldata[2][] = 'Description';
        $finaldata[2][] = 'Data Entry';

        $receiptamt = 0;
        $paymentamt = 0;
        $k = 0;
        $count = 0;
        $type_exp = '';
        $flag = 0;
        $x = 0;
        $t = 0;
        if ($dailyexpenses != NULL) {

            $countdaily = 0;
            $receiptamt = 0;
            $paymentamt = 0;

            foreach ($dailyexpenses as $key => $row) {
                $x = $key + 3;
                $finaldata[$x][] = '';

                $type = $row['expense_name'];
                $x = $key + 3;

                $finaldata[$x + 1][] = $key + 1;
                //$finaldata[$x + 1][] = $row['expense_name'];
                $finaldata[$x + 1][] = $row['date'];
                if ($row['expensetype'] == 1) {
                    $finaldata[$x + 1][] = $row['expense_name'];
                } else {
                    $finaldata[$x + 1][] = '';
                }
                if ($row['expensetype'] == 0) {
                    $finaldata[$x + 1][] = $row['expense_name'];
                } else {
                    $finaldata[$x + 1][] = '';
                }

                if ($row['expensetype'] == 0) {
                    $finaldata[$x + 1][] = $row['amount'];
                    $receiptamt = $receiptamt + $row['amount'];
                } else {
                    $finaldata[$x + 1][] = '';
                }

                if ($row['expensetype'] == 1) {
                    $finaldata[$x + 1][] = $row['amount'];
                    $paymentamt = $paymentamt + $row['amount'];
                } else {
                    $finaldata[$x + 1][] = '';
                }

                $finaldata[$x + 1][] = $row['description'];
                $finaldata[$x + 1][] = $row['data_entry'];
                //                   $finaldata[$count+1][]=$row['first_name'];
                $k = $count + 2;

                $tblpx = Yii::app()->db->tablePrefix;
                $totalqry1 = "SELECT sum(amount) as sum FROM " . $tblpx . "dailyexpense  as e
                                                            left join " . $tblpx . "company_expense_type as p on p.company_exp_id=e.exp_type_id
                                                            WHERE YEAR(e.date)='$year' AND MONTH(e.date)='$month' AND p.name = '" . $type . "' AND p.type = 0 order by company_exp_id";
                $sql1 = Yii::app()->db->createCommand($totalqry1)->queryRow();
                //print_r($sql1);

                $totalqry2 = "SELECT sum(amount) as sum FROM " . $tblpx . "dailyexpense  as e
                                                            left join " . $tblpx . "company_expense_type as p on p.company_exp_id=e.exp_type_id
                                                            WHERE YEAR(e.date)='$year' AND MONTH(e.date)='$month' AND p.name = '" . $type . "' AND p.type = 1 order by company_exp_id";
                $sql2 = Yii::app()->db->createCommand($totalqry2)->queryRow();

                $sum1 = $sql1['sum'];
                $sum2 = $sql2['sum'];
                if ($type_exp != $type) {
                    $finaldata[$x + 1][] = '';
                    $finaldata[$x + 1][] = 'Sub Total';
                    $finaldata[$x + 1][] = 'Receipt :' . $sum1;
                    $finaldata[$x + 1][] = 'Payment :' . $sum2;
                    $type_exp = $type;
                }
                $t = $x;
            }

            $finaldata[$t + 2][] = '';
            $finaldata[$t + 2][] = '';
            $finaldata[$t + 2][] = '';
            $finaldata[$t + 2][] = 'Total';
            $finaldata[$t + 2][] = $receiptamt;
            $finaldata[$t + 2][] = $paymentamt;
        }




        //  echo"<pre>";       print_r( $finaldata);die;

        Yii::import('ext.ECSVExport');

        $csv = new ECSVExport($finaldata);

        $csv->setHeaders($arraylabel);
        $csv->setToAppend();
        $contentdata = $csv->toCSV();
        $csvfilename = 'Monthly report' . date('d_M_Y') . '.csv';
        Yii::app()->getRequest()->sendFile($csvfilename, $contentdata, "text/csv", false);
        exit();
    }

    public function actionSavetopdf1()
    {
        $this->logo = $this->realpath_logo;
        $tblpx = Yii::app()->db->tablePrefix;
        $model = new Expenses('search');

        $model->unsetAttributes();  // clear any default values
        if (isset($_REQUEST['Expenses']))
            $model->attributes = $_REQUEST['Expenses'];
        $where = 'WHERE 1=1';
        if (isset($_REQUEST['userid'])) {
            $userid = $_REQUEST['userid'];
            $where .= ' AND ' . $tblpx . 'expenses.userid LIKE "' . $userid . '%"';
        }

        if (!empty($_REQUEST['projectid'])) {
            $where .= ' AND projectid = "' . $_REQUEST['projectid'] . '"';
        }

        if (!empty($_REQUEST['vendor_id'])) {
            $where .= ' AND ' . $tblpx . 'expenses.vendor_id = ' . $_REQUEST['vendor_id'] . '';
        }
        if (!empty($_REQUEST['purchase_type'])) {
            $where .= ' AND ' . $tblpx . 'expenses.purchase_type = ' . $_REQUEST['purchase_type'] . '';
        }
        if (!empty($_REQUEST['expense_type'])) {
            if($_REQUEST['expense_type'] == '107'){
                 $where .= ' AND (' . $tblpx . 'expenses.expense_type = ' . 0 . ' )';
            }else{
                 $where .= ' AND (' . $tblpx . 'expenses.expense_type = ' . $_REQUEST['expense_type'] . ' )';
            }
           
        }
        if (!empty($_REQUEST['subcontractor_id'])) {
            $paymentid = Yii::app()->db->createCommand("SELECT payment_id FROM {$tblpx}subcontractor_payment WHERE subcontractor_id = " . $_REQUEST['subcontractor_id'] . "")->queryAll();
            foreach ($paymentid as $id) {
                $pid[] = $id["payment_id"];
            }
            $paymentids = implode(", ", $pid);
            $where .= ' AND (' . $tblpx . 'expenses.subcontractor_id IN (' . $paymentids . ') )';
        }

        if (!empty($_REQUEST['type'])) {
            $where .= ' AND '. $tblpx.'expenses.type = "' . $_REQUEST['type'] . '"';
        }
        if (!empty($_REQUEST['fromdate']) && !empty($_REQUEST['todate'])) {
            $fromdate = date('Y-m-d', strtotime($_REQUEST['fromdate']));
            $todate = date('Y-m-d', strtotime($_REQUEST['todate']));
            $where .= ' AND date between "' . $fromdate . '" and"' . $todate . '"';
        }
        if (!empty($_REQUEST['fromdate'])) {
            $fromdate = date('Y-m-d', strtotime($_REQUEST['fromdate']));
            $where .= ' AND date >= "' . $fromdate . '"';
        }
        if (!empty($_REQUEST['todate'])) {
            $todate = date('Y-m-d', strtotime($_REQUEST['todate']));
            $where .= ' AND date <= "' . $todate . '"';
        }

        if (!empty($_REQUEST['company_id'])) {
            $company_id = $_REQUEST['company_id'];
            $newQuery = " FIND_IN_SET('" . $company_id . "', {$tblpx}expenses.company_id)";
        } else {
            $company_id = "";
            $user = Users::model()->findByPk(Yii::app()->user->id);
            $arrVal = explode(',', $user->company_id);
            $newQuery = "";
            foreach ($arrVal as $arr) {
                if ($newQuery) $newQuery .= ' OR';
                $newQuery .= " FIND_IN_SET('" . $arr . "', {$tblpx}expenses.company_id)";
            }
        }

        $grantotal = $model->grandtotalcredit($where);
        $granddebitamount = $model->grandtotaldebit($where);

        $data = Yii::app()->db->createCommand("SELECT " . $tblpx . "expenses.*," . $tblpx . "vendors.name as vendor_name," . $tblpx . "projects.name as project_name," . $tblpx . "expenses.company_id as companyid FROM `" . $tblpx . "expenses` left join " . $tblpx . "projects on projectid = pid 
            left join " . $tblpx . "users on " . $tblpx . "users.userid = " . $tblpx . "expenses.userid 
            left join " . $tblpx . "status on sid = type 
            left join " . $tblpx . "vendors on " . $tblpx . "vendors.vendor_id = " . $tblpx . "expenses.vendor_id 
            left join " . $tblpx . "expense_type on exptype = type_id " . $where . " AND (" . $newQuery . ") ORDER BY exp_id desc")->queryAll();
        
        $mPDF1 = Yii::app()->ePdf->mPDF();
        $mPDF1 = Yii::app()->ePdf->mPDF('', 'A4');
        $sql = 'SELECT template_name '
                . 'FROM jp_quotation_template '
                . 'WHERE status="1"';
        $selectedtemplate =Yii::app()->db->createCommand($sql)->queryScalar();
        
        $template = $this->getTemplate($selectedtemplate);
        $mPDF1->SetHTMLHeader($template['header']);
        $mPDF1->SetHTMLFooter($template['footer']);
        $mPDF1->AddPage('','', '', '', '', 0,0,52, 30, 10,0);
        $mPDF1->SetAutoPageBreak(true, 40);
        $mPDF1->WriteHTML($this->renderPartial('daybooktransactions', array(
            'model' => $data, 'grandtotal' => $grantotal, 'company_id' => $company_id
        ), true));
        $filename = "Daybook Transactions";
        $mPDF1->Output($filename . '.pdf', 'D');
    }

    public function actionSavetoexcel1()
    {

        $tblpx = Yii::app()->db->tablePrefix;
        $model = new Expenses('search');

        $model->unsetAttributes();  // clear any default values
        if (isset($_REQUEST['Expenses']))
            $model->attributes = $_REQUEST['Expenses'];
        $data = array();

        $where = 'WHERE 1=1';
        if (isset($_REQUEST['userid'])) {
            $userid = $_REQUEST['userid'];
            $where .= ' AND ' . $tblpx . 'expenses.userid LIKE "' . $userid . '%"';
        }

        if (!empty($_REQUEST['projectid'])) {
            $where .= ' AND projectid = "' . $_REQUEST['projectid'] . '"';
        }

        if (!empty($_REQUEST['vendor_id'])) {
            $where .= ' AND ' . $tblpx . 'expenses.vendor_id = ' . $_REQUEST['vendor_id'] . '';
        }
        if (!empty($_REQUEST['purchase_type'])) {
            $where .= ' AND ' . $tblpx . 'expenses.purchase_type = ' . $_REQUEST['purchase_type'] . '';
        }
        if (!empty($_REQUEST['expense_type'])) {
            if($_REQUEST['expense_type'] == '107'){
                 $where .= ' AND (' . $tblpx . 'expenses.expense_type = ' . 0 . ' )';
            }else{
            $where .= ' AND (' . $tblpx . 'expenses.expense_type = ' . $_REQUEST['expense_type'] . ' )';
            }
        }
        if (!empty($_REQUEST['subcontractor_id'])) {
            $paymentid = Yii::app()->db->createCommand("SELECT payment_id FROM {$tblpx}subcontractor_payment WHERE subcontractor_id = " . $_REQUEST['subcontractor_id'] . "")->queryAll();
            foreach ($paymentid as $id) {
                $pid[] = $id["payment_id"];
            }
            $paymentids = implode(", ", $pid);
            $where .= ' AND (' . $tblpx . 'expenses.subcontractor_id IN (' . $paymentids . ') )';
        }

        if (!empty($_REQUEST['type'])) {
            $where .= ' AND ' . $tblpx . 'expenses.type = "' . $_REQUEST['type'] . '"';
        }
        if (!empty($_REQUEST['fromdate']) && !empty($_REQUEST['todate'])) {
            $fromdate = date('Y-m-d', strtotime($_REQUEST['fromdate']));
            $todate = date('Y-m-d', strtotime($_REQUEST['todate']));
            $where .= ' AND date between "' . $fromdate . '" and"' . $todate . '"';
        }
        if (!empty($_REQUEST['fromdate'])) {
            $fromdate = date('Y-m-d', strtotime($_REQUEST['fromdate']));
            $where .= ' AND date >= "' . $fromdate . '"';
        }
        if (!empty($_REQUEST['todate'])) {
            $todate = date('Y-m-d', strtotime($_REQUEST['todate']));
            $where .= ' AND date <= "' . $todate . '"';
        }

        if (!empty($_REQUEST['company_id'])) {
            $company_id = $_REQUEST['company_id'];
            $newQuery = " FIND_IN_SET('" . $company_id . "', {$tblpx}expenses.company_id)";
        } else {
            $company_id = "";
            $user = Users::model()->findByPk(Yii::app()->user->id);
            $arrVal = explode(',', $user->company_id);
            $newQuery = "";
            foreach ($arrVal as $arr) {
                if ($newQuery) $newQuery .= ' OR';
                $newQuery .= " FIND_IN_SET('" . $arr . "', {$tblpx}expenses.company_id)";
            }
        }

        $grantotal = $model->grandtotalcredit($where);
        $granddebitamount = $model->grandtotaldebit($where);
        $data = Yii::app()->db->createCommand("SELECT " . $tblpx . "expenses.*," . $tblpx . "vendors.name as vendor_name," . $tblpx . "projects.name as project_name," . $tblpx . "expenses.company_id as companyid FROM `" . $tblpx . "expenses` left join " . $tblpx . "projects on projectid = pid 
            left join " . $tblpx . "users on " . $tblpx . "users.userid = " . $tblpx . "expenses.userid 
            left join " . $tblpx . "status on sid = type 
            left join " . $tblpx . "vendors on " . $tblpx . "vendors.vendor_id = " . $tblpx . "expenses.vendor_id 
            left join " . $tblpx . "expense_type on exptype = type_id " . $where . " AND (" . $newQuery . ") ORDER BY exp_id desc")->queryAll();
        

        //print_r($dataids);die;
        $arraylabel = array(' ', 'Day book transactions', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ');
        //        foreach ($dataids->getData() as $record) {
        //            $data[] = $record;
        //        }
        $i = 0;
        $finaldata = array();
        $finaldata[$i][] = ' ';
        $finaldata[$i][] = ' ';
        $finaldata[$i][] = ' ';
        $finaldata[$i][] = ' ';
        $finaldata[$i][] = ' ';
        $finaldata[$i][] = ' ';
        $finaldata[$i][] = ' ';
        $finaldata[$i][] = ' ';
        $finaldata[$i][] = ' ';
        $finaldata[$i][] = ' ';
        $finaldata[$i][] = ' ';
        $finaldata[$i][] = ' ';
        $i = $i + 1;

        $finaldata[$i][] = 'Sl No';
        $finaldata[$i][] = 'Date';
        $finaldata[$i][] = 'Project Name';
        $finaldata[$i][] = 'Company';
        $finaldata[$i][] = 'Vendor';
        $finaldata[$i][] = 'Subcontractor';
        $finaldata[$i][] = 'Description';
        $finaldata[$i][] = 'Transaction Typel';
        $finaldata[$i][] = 'Purchase Type';
        $finaldata[$i][] = 'Credit Amount';
        $finaldata[$i][] = 'Debit Amount';
        $finaldata[$i][] = 'Paid Amount';
        $i = $i + 1;

        $finaldata[$i][] = ' ';
        $finaldata[$i][] = ' ';
        $finaldata[$i][] = ' ';
        $finaldata[$i][] = ' ';
        $finaldata[$i][] = ' ';
        $finaldata[$i][] = ' ';
        $finaldata[$i][] = ' ';
        $finaldata[$i][] = 'Grand Total';
        $finaldata[$i][] = ' ';
        $finaldata[$i][] = Controller::money_format_inr(Yii::app()->user->grandtotalcredit[0]['sumreceived'], 2, 1);
        $finaldata[$i][] = Controller::money_format_inr(Yii::app()->user->grandtotaldebit[0]['sumamt'], 2, 1);
        $finaldata[$i][] = Controller::money_format_inr(Yii::app()->user->grandtotaldebit[0]['sumpaid'], 2, 1);
        // $finaldata[$i][] = '';
        $i = $i + 1;

        //echo "<pre>";print_r($data);die;
        foreach ($data as $key => $datavalue) {

            $id = $datavalue['exp_id'];
            $type = $datavalue['type'];
            $tr_type = "";

            if ($type == 72) { //receipt
                $ptype = $datavalue['payment_type'];
                $sql = Status::model()->find(array(
                    'select' => array('caption'),
                    "condition" => "sid='$ptype'",
                ));
                $tr_type = $sql['caption'];
            } else { // expense
                $etype = $datavalue['exptype'];
                $sql = ExpenseType::model()->find(array(
                    'select' => array('type_name'),
                    "condition" => "type_id='$etype'",
                ));
                $tr_type = $sql['type_name'];
                $ptype = $datavalue['expense_type'];
                $sql = Status::model()->find(array(
                    'select' => array('caption'),
                    "condition" => "sid='$ptype'",
                ));
                $tr_type = $sql['caption'];
            }



            $finaldata[$i][] = $i - 2;
            $finaldata[$i][] = ($datavalue['date'] != "") ? date("d-M-Y", strtotime($datavalue['date'])) : " ";
            $finaldata[$i][] = ($datavalue['projectid'] != "") ? $datavalue['project_name'] : " ";
            $company = Company::model()->findByPk($datavalue['companyid']);
            $finaldata[$i][] = $company->name;
            $finaldata[$i][] = ($datavalue['vendor_id'] != "") ? $datavalue['vendor_name'] : " ";
            if (!empty($datavalue['subcontractor_id'])) {
                $scpayment = SubcontractorPayment::model()->findByPk($datavalue['subcontractor_id']);
                if ($scpayment)
                    $finaldata[$i][] = $scpayment->subcontractor->subcontractor_name;
                else
                    $finaldata[$i][] = " ";
            } else {
                $finaldata[$i][] = " ";
            }
            $finaldata[$i][] = ($datavalue['description'] != "") ? $datavalue['description'] : " ";
            $finaldata[$i][] = $tr_type;
            //$finaldata[$key][] =($datavalue['type'] != "") ? $datavalue['caption'] : "";
            if ($datavalue['type'] == 73) {
?> <?php

                if ($datavalue['purchase_type'] == 2) {
                    $finaldata[$i][] = 'Full Paid';
                } elseif ($datavalue['purchase_type'] == 1) {
                    $finaldata[$i][] = 'Credit';
                } elseif ($datavalue['purchase_type'] == 3) {
                    $finaldata[$i][] = 'Partially Paid';
                }else{
                     $finaldata[$i][] = '';
                }
    ?><?php

            } else {
               if ($datavalue['purchase_type'] == 2) {
                    $finaldata[$i][] = 'Full Paid';
                } elseif ($datavalue['purchase_type'] == 1) {
                    $finaldata[$i][] = 'Credit';
                } elseif ($datavalue['purchase_type'] == 3) {
                    $finaldata[$i][] = 'Partially Paid';
                }else{
                     $finaldata[$i][] = '';
                }
               
            }
            if ($datavalue['type'] == 72) { 
                
                if ($datavalue['purchase_type'] == 1) {
                    $finaldata[$i][]=$datavalue['amount'];
                }else{
                    $finaldata[$i][]=$datavalue['receipt'];
                }
            }else{
               
                
                     $finaldata[$i][]='0.00';
                
                 
            }
            if ($datavalue['type'] == 73) { 
                if ($datavalue['purchase_type'] == 1) {
                    $finaldata[$i][]=$datavalue['amount'];
                }else{
                    $finaldata[$i][]=$datavalue['paid'];
                }
            }else{
               
                
                     $finaldata[$i][]='0.00';
                
                 
            }

            
            if ($datavalue['type'] == 73) {
                $finaldata[$i][] = ($datavalue['paid'] != "") ? Controller::money_format_inr($datavalue['paid'], 2) : "0.00 ";
            }
            
            //$finaldata[$key][] = ($datavalue['userid'] != "") ? $datavalue['first_name'] : "";
            //$finaldata[$key+1][] = ($datavalue['data_entry'] != "") ? $datavalue['data_entry'] : "";
            $i++;
        }

        Yii::import('ext.ECSVExport');

        $csv = new ECSVExport($finaldata);

        $csv->setHeaders($arraylabel);
        $csv->setToAppend();
        $contentdata = $csv->toCSV();
        $csvfilename = 'Daybook Transactions' . date('d_M_Y') . '.csv';
        Yii::app()->getRequest()->sendFile($csvfilename, $contentdata, "text/csv", false);
        exit();
    }

    public function actionDeletedaybookentry($id)
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $query = Yii::app()->db->createCommand('DELETE FROM ' . $tblpx . 'expenses WHERE exp_id=' . $id)->execute();
        if ($query) {
            echo json_encode(array('response' => 'success'));
        }
    }

    public function actionlist()
    {
        $tblpx = Yii::app()->db->tablePrefix;
        if (isset($_GET['Expenses'])) {
            $menus = $_GET['Expenses'];
        }

        if (Yii::app()->user->role == 1) {

            $sql = Yii::app()->db->createCommand('select expense_id,name,type,expensetype_id,payment_type,caption,expense_date,expense_totalamount,first_name,' . $tblpx . 'expenses.expense_description from ' . $tblpx . 'expenses
                            inner join ' . $tblpx . 'projects on ' . $tblpx . 'expenses.project_id = ' . $tblpx . 'projects.pid
                            inner join ' . $tblpx . 'users on ' . $tblpx . 'expenses.user_id = ' . $tblpx . 'users.userid
                            inner join ' . $tblpx . 'status on ' . $tblpx . 'expenses.type = ' . $tblpx . 'status.sid ORDER BY expense_id DESC LIMIT 6')->queryAll();

            //print_r($sql); die();	
            //$model = new Projects;

            $sql1 = Yii::app()->db->createCommand('select pid,name,description from ' . $tblpx . 'projects ORDER BY pid DESC LIMIT 6 ')->queryAll();
            //print_r($sql1); 
        } else {
            //die(Yii::app()->user->id);
            $sql = Yii::app()->db->createCommand('select expense_id,name,type,expensetype_id,payment_type,caption,expense_date,expense_totalamount,first_name,' . $tblpx . 'expenses.expense_description from ' . $tblpx . 'expenses
                            inner join ' . $tblpx . 'projects on ' . $tblpx . 'expenses.project_id = ' . $tblpx . 'projects.pid
                            inner join ' . $tblpx . 'users on ' . $tblpx . 'expenses.user_id = ' . $tblpx . 'users.userid
                            inner join ' . $tblpx . 'status on ' . $tblpx . 'expenses.type = ' . $tblpx . 'status.sid 
                            where ' . $tblpx . 'users.userid=' . Yii::app()->user->id . '
                            ORDER BY expense_id DESC LIMIT 6')->queryAll();
            //$model = new Projects;

            $sql1 = Yii::app()->db->createCommand('select pid,name,description from ' . $tblpx . 'projects
                        inner join ' . $tblpx . 'project_assign on ' . $tblpx . 'projects.pid=' . $tblpx . 'project_assign.projectid
                        inner join ' . $tblpx . 'users on ' . $tblpx . 'project_assign.userid = ' . $tblpx . 'users.userid
                        where ' . $tblpx . 'users.userid=' . Yii::app()->user->id . '
                        ORDER BY pid DESC LIMIT 6 ')->queryAll();
            //print_r($sql1); 
        }

        $this->render('list', array(
            'model' => $sql, 'newmodel' => $sql1,
        ));
    }

    public function actionGetDaybookDetails()
    {
        $expenseId = $_REQUEST["expenseid"];
        $expStatus = $_REQUEST["expstatus"];
        $tblpx = Yii::app()->db->tablePrefix;
        $expenseTbl=  "" . $tblpx . "expenses"; 
        if ($expStatus == 0) {
            $expenseTbl = "" . $tblpx . "pre_expenses";
        }       
        $expenseData = Yii::app()->db->createCommand("SELECT * FROM ".$expenseTbl." WHERE exp_id = " . $expenseId)->queryRow();
        $result["expid"] = $expenseData["exp_id"];
        $result["expdate"] = $expenseData["date"];
        $result["expproject"] = $expenseData["projectid"];
        $result["expbill"] = $expenseData["bill_id"];
        $result["exptypeid"] = $expenseData["exptype"];
        $result["exptypecc"] = $expenseData["expense_type"];
        $result["expvendor"] = $expenseData["vendor_id"];
        $result["expamount"] = $expenseData["expense_amount"];
        $result["expsgstp"] = $expenseData["expense_sgstp"];
        $result["expsgst"] = $expenseData["expense_sgst"];
        $result["expcgstp"] = $expenseData["expense_cgstp"];
        $result["expcgst"] = $expenseData["expense_cgst"];
        $result["exptotal"] = $expenseData["amount"];
        $result["expdesc"] = $expenseData["description"];
        $result["exptype"] = $expenseData["type"];
        $result["exprtype"] = $expenseData["payment_type"];
        $result["bank"] = $expenseData["bank_id"];
        $result["expreceipt"] = $expenseData["receipt"];
        $result["expptype"] = $expenseData["purchase_type"];
        $result["exppaid"] = $expenseData["paid"];
        $result["bank"] = $expenseData["bank_id"];
        $result["chequeno"] = $expenseData["cheque_no"];
        $result["expuser"] = $expenseData["userid"];
        $result["expinvc"] = $expenseData["invoice_id"];
        $result["expigstp"] = $expenseData["expense_igstp"];
        $result["expigst"] = $expenseData["expense_igst"];
        $result["exptdsp"] = isset($expenseData["expense_tdsp"]) ? $expenseData["expense_tdsp"] : 0;
        $result["exptds"] = isset($expenseData["expense_tds"]) ? $expenseData["expense_tds"] : 0;
        $result["expemployee"]    = $expenseData["employee_id"];
        $result["expreturn"] = $expenseData["return_id"];
        $result["expcomp"] = $expenseData["company_id"];
        $result["paid"] = $expenseData["paidamount"];
        $result["balance"] = '';
        if ($expenseData['bill_id'] != '') {
            $exp_paid = Yii::app()->db->createCommand("SELECT SUM(IFNULL(paid,0)) as paid_amount from " . $tblpx . "expenses WHERE bill_id = " . $expenseData['bill_id'])->queryRow();
            $paid_amount = (isset($exp_paid['paid_amount']) || $exp_paid['paid_amount'] != '') ? $exp_paid['paid_amount'] : 0;
            $bill_amount = Yii::app()->db->createCommand("SELECT bill_totalamount FROM {$tblpx}bills WHERE bill_id=" . $expenseData['bill_id'])->queryRow();
            $balance = $bill_amount['bill_totalamount'] - $paid_amount;
            $balance = ($balance > 0) ? $balance : 0;
            $result["paid"] =  number_format($paid_amount, 2, '.', '');
            $result["balance"] = number_format($balance, 2, '.', '');
        }

        if ($expenseData['invoice_id'] != '') {
            $exp_paid = Yii::app()->db->createCommand("SELECT SUM(IFNULL(receipt,0)) as paid_amount from " . $tblpx . "expenses WHERE invoice_id = " . $expenseData['invoice_id'])->queryRow();
            $paid_amount = (isset($exp_paid['paid_amount']) || $exp_paid['paid_amount'] != '') ? $exp_paid['paid_amount'] : 0;
            $invoice_amount = Yii::app()->db->createCommand("SELECT SUM( IFNULL(amount, 0) + IFNULL(tax_amount, 0) ) as total FROM {$tblpx}invoice WHERE invoice_id=" . $expenseData['invoice_id'])->queryRow();
            $balance = $invoice_amount['total'] - $paid_amount;
            $balance = ($balance > 0) ? $balance : 0;
            $result["paid"] =  number_format($paid_amount, 2, '.', '');
            $result["balance"] = number_format($balance, 2, '.', '');
        }

        echo json_encode($result);
    }

    public function actionProfitandloss()
    {
        $company_id = "";
        $date_from = "";
        $date_to = "";
        $daybook_expense_tot_amount=0;
        $newQuery3="";
        $newQuery4="";
        if (isset($_REQUEST['date_from']) && isset($_REQUEST['date_to'])) {
            $date_from = $_REQUEST['date_from'];
            $date_to = $_REQUEST['date_to'];
            
            if ($_REQUEST['company_id'] != NULL) {
                $company_id = $_REQUEST['company_id'];
            }
            $exp_cal=0;
            if(!empty($company_id )){
                $company_det=Company::Model()->findByPk($company_id );
                $exp_cal =$company_det->expense_calculation;
                       
            }

            $tblpx = Yii::app()->db->tablePrefix;
            if ($company_id == NULL) {
                $user = Users::model()->findByPk(Yii::app()->user->id);
                $arrVal = explode(',', $user->company_id);
                $newQuery = "";
                $newQuery1 = "";
                $newQuery3="";
                $newQuery4="";
                foreach ($arrVal as $arr) {
                    if ($newQuery) $newQuery .= ' OR';
                    if ($newQuery1) $newQuery1 .= ' OR';
                     if ($newQuery3) $newQuery3 .= ' OR';
                    if ($newQuery4) $newQuery4 .= ' OR';
                    $newQuery .= " FIND_IN_SET('" . $arr . "', company_id)";
                    $newQuery1 .= " FIND_IN_SET('" . $arr . "', {$tblpx}invoice.company_id)";
                    $newQuery3 .= " FIND_IN_SET('" . $arr . "', E.company_id)";
                    $newQuery4 .= " FIND_IN_SET('" . $arr . "', jp_purchase.company_id)";
                }
            } else {
                $newQuery = " FIND_IN_SET('" . $company_id . "', company_id)";
                $newQuery1 = " FIND_IN_SET('" . $company_id . "', {$tblpx}invoice.company_id)";
                $newQuery3 .= " FIND_IN_SET('" . $company_id . "', E.company_id)";
                $newQuery4 .= " FIND_IN_SET('" . $company_id . "', jp_purchase.company_id)";
            }
            $consume_total_amount=0;
            if($exp_cal==1){
                 $consumesql = "
                SELECT 
                    SUM(c.item_qty * c.item_rate) AS total_amount
                FROM 
                    pms_acc_wpr_item_consumed c
                LEFT JOIN 
                    jp_consumption_request r ON r.id = c.consumption_id
                LEFT JOIN 
                    jp_projects E ON E.pid = r.coms_project_id
                WHERE 
                    r.status = 1
                    AND ".$newQuery3;
                $consume_total = Yii::app()->db->createCommand($consumesql)->queryRow();;
            
                $consume_total_amount = isset($consume_total['total_amount']) ? $consume_total['total_amount'] : 0;
                    
             

            }
            $laboursql = "
            SELECT 
                SUM(amount) AS total_amount
            FROM 
                jp_dailyreport
            WHERE 
                approval_status = '1'
                AND approve_status = 1
                AND (subcontractor_id IS NULL OR subcontractor_id = -1) AND ".$newQuery;
            
            $command = Yii::app()->db->createCommand($laboursql);
            $result = $command->queryRow();


            $total_labour_amount = isset($result['total_amount']) ? $result['total_amount'] : 0;
           
            
            $daily_expenses = Yii::app()->db->createCommand("SELECT SUM(amount) as total_amount FROM {$tblpx}dailyexpense WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND expense_type IN(88,89) AND (" . $newQuery . ")")->queryRow();
            $daybook = Yii::app()->db->createCommand("SELECT SUM(receipt)as total_amount FROM {$tblpx}expenses WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND payment_type IN(88,89) AND type=72 AND (" . $newQuery . ") AND return_id IS NULL")->queryRow();
            //$recepit = 	$daily_expenses['total_amount']+$daybook['total_amount'];
            $recepit = $daybook['total_amount'];

            $purchasereturn = Yii::app()->db->createCommand("SELECT SUM(receipt)as total_amount FROM {$tblpx}expenses WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND payment_type IN(88,89) AND type=72 AND (" . $newQuery . ") AND return_id IS NOT NULL")->queryRow();
            $return = $purchasereturn['total_amount'];
            $inv_details = Yii::app()->db->createCommand("SELECT SUM({$tblpx}invoice.amount) as total_amount FROM {$tblpx}invoice LEFT JOIN {$tblpx}inv_list ON {$tblpx}invoice.invoice_id= {$tblpx}inv_list.inv_id WHERE {$tblpx}invoice.date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND (" . $newQuery1 . ") AND {$tblpx}invoice.invoice_status ='saved'")->queryRow();
            $inv_tax_amount = Yii::app()->db->createCommand("SELECT SUM({$tblpx}invoice.tax_amount) as total_amount FROM {$tblpx}invoice LEFT JOIN {$tblpx}inv_list ON {$tblpx}invoice.invoice_id= {$tblpx}inv_list.inv_id WHERE {$tblpx}invoice.date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND (" . $newQuery1 . ") AND {$tblpx}invoice.invoice_status ='saved'")->queryRow();
            $invoice = $inv_details['total_amount'] + $inv_tax_amount['total_amount'];
            //$bill_details = Yii::app()->db->createCommand("SELECT sum({$tblpx}bills.bill_amount) as total_amount FROM {$tblpx}bills INNER JOIN {$tblpx}purchase ON {$tblpx}bills.purchase_id ={$tblpx}purchase.p_id WHERE {$tblpx}bills.bill_date BETWEEN '".$date_from."' AND '".$date_to."' AND {$tblpx}bills.company_id=".Yii::app()->user->company_id."")->queryRow();
            $sql="SELECT SUM(paidamount) as total_amount FROM {$tblpx}expenses WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND expense_type IN(88,89) AND type=73 AND (" . $newQuery . ")";
            $expense_details = Yii::app()->db->createCommand($sql)->queryRow();
            $vendor_details = Yii::app()->db->createCommand("SELECT SUM(amount)as total_amount FROM {$tblpx}dailyvendors WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND payment_type IN(88,89) AND (" . $newQuery . ")")->queryRow();
           // die($sql);
            $total_expense = $expense_details['total_amount'] + $vendor_details['total_amount'];

            $daily_expense_details = Yii::app()->db->createCommand("SELECT SUM(dailyexpense_paidamount) as total_amount FROM {$tblpx}dailyexpense WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND dailyexpense_type = 'expense' AND (" . $newQuery . ")")->queryRow();
            $daily_expense_petty_receipt = Yii::app()->db->createCommand("SELECT SUM(dailyexpense_receipt) as total_amount FROM {$tblpx}dailyexpense WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND dailyexpense_type = 'receipt' AND dailyexpense_receipt_type IN(103) AND (" . $newQuery . ")")->queryRow();

            $total_daily_expense = $daily_expense_details['total_amount'] - $daily_expense_petty_receipt['total_amount'];


            $depositt_details = Yii::app()->db->createCommand("SELECT SUM(dailyexpense_paidamount) as total_amount FROM {$tblpx}dailyexpense WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND dailyexpense_type ='deposit' AND (" . $newQuery . ")")->queryRow();
            $total_deposit = $depositt_details['total_amount'];
            $sql="SELECT SUM(paidamount) as total_amount FROM {$tblpx}expenses WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND expense_type IN(88,89) AND type=73 AND (" . $newQuery . ")";
            $expense_details = Yii::app()->db->createCommand($sql)->queryRow();
            //die($sql);
            $daybook_expense_tot_amount=0;
            $vendor_sql="SELECT SUM(amount)as total_amount FROM {$tblpx}dailyvendors WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND payment_type IN(88,89) AND (" . $newQuery . ")";
            $daybooksql = "select * from " . $tblpx . "expenses "
                                    . " WHERE amount!=0" 
                                    . "  AND   exptype IS NOT NULL AND vendor_id IS NOT NULL "
                                    . " AND bill_id IS NULL AND (" . $newQuery . ")  "
                                    . " AND (reconciliation_status IS NULL OR reconciliation_status=1) "
                                    . " ORDER BY date ";
            $daybook_expense_only = Yii::app()->db->createCommand($daybooksql)->queryAll();
            //echo "<pre>"; print_r($daybook_expense);exit;
            if(!empty($daybook_expense_only)){
                
                foreach ($daybook_expense_only as $newsql) {
                   
                    $daybook_expense_tot_amount += $newsql['paidamount'];
                }
            }
            /** Subcontractor Payment starts */
            $check_array = array();
            $daybook_subcontractor_expenseamount = 0;
            
            $daybook_sub_expensesql = "select * from " . $tblpx . "expenses"
            . "  WHERE amount !=0  "
            . " AND type = '73'  AND exptype IS NULL "
            . " AND vendor_id IS NULL AND bill_id IS NULL AND (" . $newQuery . ") "
            . " AND (reconciliation_status IS NULL OR reconciliation_status=1)  ORDER BY date"; 
            $daybook_expense = Yii::app()->db->createCommand( $daybook_sub_expensesql)->queryAll();
            $subcontractor_payment_lists = Projects::model()->setSubcontractorPayment($daybook_expense);
            //echo "<pre>";print_r( $subcontractor_payment_lists);exit;
           
                foreach ($subcontractor_payment_lists as $daybook_expense) {
                $scpayment_group_count = count($daybook_expense) - 1;
                $sc_index = 1;
                foreach ($daybook_expense as $newdata) {
                    if (is_array($newdata)) {
                        
                        $daybook_subcontractor_expenseamount+=$newdata['paid'];
                        $tblpx = Yii::app()->db->tablePrefix;
                        $payment = SubcontractorPayment::model()->findByPk($newdata['subcontractor_id']);
                        $query = "";
                        $subcontractor_id = '';
                        if (isset($payment->subcontractor_id)) {
                            $query = ' AND b.subcontractor_id=' . $payment->subcontractor_id . '';
                            $subcontractor_id = $payment->subcontractor_id;
                        }
                        $sub_total1 = Yii::app()->db->createCommand("select SUM(a.paidamount) as paid_amount,count(a.exp_id) as total_count,b.* from jp_expenses a LEFT JOIN jp_subcontractor_payment b ON a.subcontractor_id=b.payment_id WHERE a.amount !=0 AND a.projectid=" . $newdata['projectid'] . " AND a.type = '73' AND a.exptype IS NULL AND a.vendor_id IS NULL AND a.bill_id IS NULL AND a.date ='" . $newdata['date'] . "' " . $query . " GROUP BY a.date,b.subcontractor_id")->queryRow();
                        $check_data = $newdata['date'] . '-' . $subcontractor_id;
                        if (in_array($check_data, $check_array)) {
                            $flag = 0;
                            $row_count = "";
                        } else {
                            $flag = 1;
                            $row_count = $sub_total1['total_count'];
                        }
                        array_push($check_array, $check_data);
                       
                                $payment = SubcontractorPayment::model()->findByPk($newdata['subcontractor_id']);
                                if (isset($payment->subcontractor_id)) {
                                    $subcontractor_data = Subcontractor::model()->findByPk($payment->subcontractor_id);
                                }
                               
                        $sc_index++;
                    }
                }
            }
            $bill_amount=0;
            //echo $daybook_subcontractor_expenseamount;exit;
           
            /** Subcontractor payment ends */
            //echo($daybook_subcontractor_expenseamount);exit;
           // echo ($daybook_expense_tot_amount);exit;
           /* Purchase Bill Starts*/
           $unreconciled_datasql = "SELECT O.*, E.*  FROM jp_reconciliation O "
                . " JOIN jp_expenses E ON O.reconciliation_parentid = E.exp_id  "
                . " WHERE O.reconciliation_status=0 and (" . $newQuery3 . ") ";
            $unreconciled_data = Yii::app()->db->createCommand($unreconciled_datasql)->queryAll();

            $bill_array = array();

            foreach ($unreconciled_data as  $data) {                
                $recon_bill = "'{$data["bill_id"]}'";                
                array_push($bill_array, $recon_bill);
            }
           
            $bill_array1 = implode(',', $bill_array);

            $pendingsql = 'SELECT bill_id FROM jp_expenses '
            . ' WHERE  '
            . ' reconciliation_status =1 OR  reconciliation_status IS NULL';
            $pending_bills_array = Yii::app()->db->createCommand($pendingsql)->queryAll();

            $pending_bills =array();
            foreach ($pending_bills_array as  $data) {                
                $billid = "'{$data["bill_id"]}'";                
                array_push($pending_bills, $billid);
            }
            $pending_bills1 = implode(',', $pending_bills);
            if ($bill_array1) {
                $bill_condition = " 1=1";
                $bill_condition1 = " 1=1";
                if(!empty($pending_bills1)){
                    $bill_condition = " bill_id IN($pending_bills1)";
                    $bill_condition1 = "bill_id NOT IN($pending_bills1)";
                }

                $purchasesql = "SELECT * FROM  " . $tblpx . "bills "
                    . " LEFT JOIN " . $tblpx . "purchase "
                    . " ON " . $tblpx . "bills.purchase_id=" . $tblpx . "purchase.p_id "
                    . " WHERE "
                    . $tblpx . "bills.purchase_id IS NOT NULL  "
                    . " AND (" . $newQuery4 . ") AND bill_id NOT IN ($bill_array1) "
                    . " AND ".$bill_condition;                    
                $purchase = Yii::app()->db->createCommand($purchasesql)->queryAll(); //reconcil


                $purchase1sql = "SELECT * FROM  " . $tblpx . "bills "
                    . " LEFT JOIN " . $tblpx . "purchase "
                    . " ON " . $tblpx . "bills.purchase_id=" . $tblpx . "purchase.p_id "
                    . " WHERE  "
                    .  $tblpx . "bills.purchase_id IS NOT NULL  "
                    . " AND (" . $newQuery4 . ")  AND ".$bill_condition1;                    
                $purchase1 = Yii::app()->db->createCommand($purchase1sql)->queryAll(); //un reconcil
            } else {

                $bill_condition = " 1=1";
                $bill_condition1 = " 1=1";
                if(!empty($pending_bills1)){
                    $bill_condition = " bill_id IN($pending_bills1)";
                    $bill_condition1 = "bill_id NOT IN($pending_bills1)";
                }

                $purchasesql = "SELECT * FROM  " . $tblpx . "bills "
                    . " LEFT JOIN " . $tblpx . "purchase "
                    . " ON " . $tblpx . "bills.purchase_id=" . $tblpx . "purchase.p_id "
                    . " WHERE  "
                     . $tblpx . "bills.purchase_id IS NOT NULL  "
                    . " AND (" . $newQuery4 . ")  and ".$bill_condition;
                $purchase = Yii::app()->db->createCommand($purchasesql)->queryAll();

                $purchase1sql = "SELECT * FROM  " . $tblpx . "bills "
                    . " LEFT JOIN " . $tblpx . "purchase "
                    . " ON " . $tblpx . "bills.purchase_id=" . $tblpx . "purchase.p_id "
                    . " WHERE  "
                    . $tblpx . "bills.purchase_id IS NOT NULL  "
                    . " AND (" . $newQuery4 . ")  and ".$bill_condition1;
                $purchase1 = Yii::app()->db->createCommand($purchase1sql)->queryAll(); //un reconcil
            }

            $bill_amount = 0;
            $purchase_list = Projects::model()->groupPurchaseBillItems($purchase);
            //$expense_types_listing = Projects::model()->setExpenseTypesListingofPurchaseBill($purchase_list);

                foreach ($purchase_list as $purchase) {
                    $count = count($purchase);
                    $count = ($count - 2);
                    $index = 1;
                    foreach ($purchase as $key => $value) {
                         if (is_array($value)) {
                            $amount =  ($value['bill_totalamount'] + $value['bill_additionalcharge']);
                            $vendor = Vendors::model()->findByPk($value['vendor_id']);
                            $bill_amount += $amount; 
                        }
                     $index++;
                    }
                }
            //echo ($bill_amount);exit;

            /*Purchase Bill Ends*/
            $vendor_details = Yii::app()->db->createCommand($vendor_sql)->queryRow();
            if($exp_cal==1){
                $total_expense = $daybook_expense_tot_amount+$daybook_subcontractor_expenseamount+$total_labour_amount+$consume_total_amount;
            }else{
                $total_expense = $daybook_expense_tot_amount+$daybook_subcontractor_expenseamount+$bill_amount+$total_labour_amount;
            }
            

        } else {
            $company_id = '';
            $date_from = date("Y-") . "01-" . "01";
            $date_to = date("Y-m-d");
            $tblpx = Yii::app()->db->tablePrefix;

            $user = Users::model()->findByPk(Yii::app()->user->id);
            $arrVal = explode(',', $user->company_id);
            $newQuery = "";
            $newQuery1 = "";
            $newQuery3 = "";
            $newQuery4="";
            $company =$arrVal["0"];
            foreach ($arrVal as $arr) {
                if ($newQuery) $newQuery .= ' OR';
                if ($newQuery1) $newQuery1 .= ' OR';
                if ($newQuery3) $newQuery3 .= ' OR';
                if ($newQuery4) $newQuery4 .= ' OR';
                $newQuery .= " FIND_IN_SET('" . $arr . "', company_id)";
                $newQuery1 .= " FIND_IN_SET('" . $arr . "', {$tblpx}invoice.company_id)";
                $newQuery3 .= " FIND_IN_SET('" . $arr . "', E.company_id)";
                $newQuery4 .= " FIND_IN_SET('" . $arr . "', jp_purchase.company_id)";
            }
            $exp_cal=0;
            if(!empty($company )){
                $company_det=Company::Model()->findByPk($company );
                $exp_cal =$company_det->expense_calculation;
                       
            }
            $daily_expenses = Yii::app()->db->createCommand("SELECT SUM(amount) as total_amount FROM {$tblpx}dailyexpense WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND expense_type IN(88,89) AND (" . $newQuery . ")")->queryRow();
            $daybook = Yii::app()->db->createCommand("SELECT SUM(receipt)as total_amount FROM {$tblpx}expenses WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND payment_type IN(88,89) AND type=72 AND (" . $newQuery . ") AND return_id IS NULL")->queryRow();
            //$recepit = 	$daily_expenses['total_amount']+$daybook['total_amount'];
            $recepit = $daybook['total_amount'];
            $purchasereturn = Yii::app()->db->createCommand("SELECT SUM(receipt)as total_amount FROM {$tblpx}expenses WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND payment_type IN(88,89) AND type=72 AND (" . $newQuery . ") AND return_id IS NOT NULL")->queryRow();
            $return = $purchasereturn['total_amount'];
            $inv_details = Yii::app()->db->createCommand("SELECT SUM({$tblpx}invoice.amount) as total_amount FROM {$tblpx}invoice LEFT JOIN {$tblpx}inv_list ON {$tblpx}invoice.invoice_id= {$tblpx}inv_list.inv_id WHERE {$tblpx}invoice.date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND (" . $newQuery1 . ") AND {$tblpx}invoice.invoice_status ='saved'")->queryRow();
            $inv_tax_amount = Yii::app()->db->createCommand("SELECT SUM({$tblpx}invoice.tax_amount) as total_amount FROM {$tblpx}invoice LEFT JOIN {$tblpx}inv_list ON {$tblpx}invoice.invoice_id= {$tblpx}inv_list.inv_id WHERE {$tblpx}invoice.date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND (" . $newQuery1 . ") AND {$tblpx}invoice.invoice_status ='saved'")->queryRow();
            $invoice = $inv_details['total_amount'] + $inv_tax_amount['total_amount'];
            //$bill_details = Yii::app()->db->createCommand("SELECT sum({$tblpx}bills.bill_amount) as total_amount FROM {$tblpx}bills INNER JOIN {$tblpx}purchase ON {$tblpx}bills.purchase_id ={$tblpx}purchase.p_id WHERE {$tblpx}bills.bill_date BETWEEN '".$date_from."' AND '".$date_to."' AND {$tblpx}bills.company_id=".Yii::app()->user->company_id."")->queryRow();
            //print_r($bill_details);
            $sql="SELECT SUM(paidamount) as total_amount FROM {$tblpx}expenses WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND expense_type IN(88,89) AND type=73 AND (" . $newQuery . ")";
            $expense_details = Yii::app()->db->createCommand($sql)->queryRow();
            //die($sql);
            $daybook_expense_tot_amount=0;
            $vendor_sql="SELECT SUM(amount)as total_amount FROM {$tblpx}dailyvendors WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND payment_type IN(88,89) AND (" . $newQuery . ")";
            $daybooksql = "select * from " . $tblpx . "expenses "
                                    . " WHERE amount!=0" 
                                    . "  AND   exptype IS NOT NULL AND vendor_id IS NOT NULL "
                                    . " AND bill_id IS NULL AND (" . $newQuery . ")  "
                                    . " AND (reconciliation_status IS NULL OR reconciliation_status=1) "
                                    . " ORDER BY date ";
            $daybook_expense_only = Yii::app()->db->createCommand($daybooksql)->queryAll();
            //echo "<pre>"; print_r($daybook_expense);exit;
            if(!empty($daybook_expense_only)){
                
                foreach ($daybook_expense_only as $newsql) {
                   
                    $daybook_expense_tot_amount += $newsql['paidamount'];
                }
            }
            /** Subcontractor Payment starts */
            $check_array = array();
            $daybook_subcontractor_expenseamount = 0;
            
            $daybook_sub_expensesql = "select * from " . $tblpx . "expenses"
            . "  WHERE amount !=0  "
            . " AND type = '73'  AND exptype IS NULL "
            . " AND vendor_id IS NULL AND bill_id IS NULL AND (" . $newQuery . ") "
            . " AND (reconciliation_status IS NULL OR reconciliation_status=1)  ORDER BY date"; 
            $daybook_expense = Yii::app()->db->createCommand( $daybook_sub_expensesql)->queryAll();
            $subcontractor_payment_lists = Projects::model()->setSubcontractorPayment($daybook_expense);
            //echo "<pre>";print_r( $subcontractor_payment_lists);exit;
           
                foreach ($subcontractor_payment_lists as $daybook_expense) {
                $scpayment_group_count = count($daybook_expense) - 1;
                $sc_index = 1;
                foreach ($daybook_expense as $newdata) {
                    if (is_array($newdata)) {
                        
                        $daybook_subcontractor_expenseamount+=$newdata['paid'];
                        $tblpx = Yii::app()->db->tablePrefix;
                        $payment = SubcontractorPayment::model()->findByPk($newdata['subcontractor_id']);
                        $query = "";
                        $subcontractor_id = '';
                        if (isset($payment->subcontractor_id)) {
                            $query = ' AND b.subcontractor_id=' . $payment->subcontractor_id . '';
                            $subcontractor_id = $payment->subcontractor_id;
                        }
                        $sub_total1 = Yii::app()->db->createCommand("select SUM(a.paidamount) as paid_amount,count(a.exp_id) as total_count,b.* from jp_expenses a LEFT JOIN jp_subcontractor_payment b ON a.subcontractor_id=b.payment_id WHERE a.amount !=0 AND a.projectid=" . $newdata['projectid'] . " AND a.type = '73' AND a.exptype IS NULL AND a.vendor_id IS NULL AND a.bill_id IS NULL AND a.date ='" . $newdata['date'] . "' " . $query . " GROUP BY a.date,b.subcontractor_id")->queryRow();
                        $check_data = $newdata['date'] . '-' . $subcontractor_id;
                        if (in_array($check_data, $check_array)) {
                            $flag = 0;
                            $row_count = "";
                        } else {
                            $flag = 1;
                            $row_count = $sub_total1['total_count'];
                        }
                        array_push($check_array, $check_data);
                       
                                $payment = SubcontractorPayment::model()->findByPk($newdata['subcontractor_id']);
                                if (isset($payment->subcontractor_id)) {
                                    $subcontractor_data = Subcontractor::model()->findByPk($payment->subcontractor_id);
                                }
                               
                        $sc_index++;
                    }
                }
            }
            $bill_amount=0;
            //echo $daybook_subcontractor_expenseamount;exit;
           
            /** Subcontractor payment ends */
            //echo($daybook_subcontractor_expenseamount);exit;
           // echo ($daybook_expense_tot_amount);exit;
           /* Purchase Bill Starts*/
           $unreconciled_datasql = "SELECT O.*, E.*  FROM jp_reconciliation O "
                . " JOIN jp_expenses E ON O.reconciliation_parentid = E.exp_id  "
                . " WHERE O.reconciliation_status=0 and (" . $newQuery3 . ") ";
            $unreconciled_data = Yii::app()->db->createCommand($unreconciled_datasql)->queryAll();

            $bill_array = array();

            foreach ($unreconciled_data as  $data) {                
                $recon_bill = "'{$data["bill_id"]}'";                
                array_push($bill_array, $recon_bill);
            }
           
            $bill_array1 = implode(',', $bill_array);

            $pendingsql = 'SELECT bill_id FROM jp_expenses '
            . ' WHERE  '
            . ' reconciliation_status =1 OR  reconciliation_status IS NULL';
            $pending_bills_array = Yii::app()->db->createCommand($pendingsql)->queryAll();

            $pending_bills =array();
            foreach ($pending_bills_array as  $data) {                
                $billid = "'{$data["bill_id"]}'";                
                array_push($pending_bills, $billid);
            }
            $pending_bills1 = implode(',', $pending_bills);
            if ($bill_array1) {
                $bill_condition = " 1=1";
                $bill_condition1 = " 1=1";
                if(!empty($pending_bills1)){
                    $bill_condition = " bill_id IN($pending_bills1)";
                    $bill_condition1 = "bill_id NOT IN($pending_bills1)";
                }

                $purchasesql = "SELECT * FROM  " . $tblpx . "bills "
                    . " LEFT JOIN " . $tblpx . "purchase "
                    . " ON " . $tblpx . "bills.purchase_id=" . $tblpx . "purchase.p_id "
                    . " WHERE "
                     . $tblpx . "bills.purchase_id IS NOT NULL  "
                    . " AND (" . $newQuery4 . ") AND bill_id NOT IN ($bill_array1) "
                    . " AND ".$bill_condition;                    
                $purchase = Yii::app()->db->createCommand($purchasesql)->queryAll(); //reconcil


                $purchase1sql = "SELECT * FROM  " . $tblpx . "bills "
                    . " LEFT JOIN " . $tblpx . "purchase "
                    . " ON " . $tblpx . "bills.purchase_id=" . $tblpx . "purchase.p_id "
                    . " WHERE  "
                    .  $tblpx . "bills.purchase_id IS NOT NULL  "
                    . " AND (" . $newQuery4 . ")  AND ".$bill_condition1;                    
                $purchase1 = Yii::app()->db->createCommand($purchase1sql)->queryAll(); //un reconcil
            } else {

                $bill_condition = " 1=1";
                $bill_condition1 = " 1=1";
                if(!empty($pending_bills1)){
                    $bill_condition = " bill_id IN($pending_bills1)";
                    $bill_condition1 = "bill_id NOT IN($pending_bills1)";
                }

                $purchasesql = "SELECT * FROM  " . $tblpx . "bills "
                    . " LEFT JOIN " . $tblpx . "purchase "
                    . " ON " . $tblpx . "bills.purchase_id=" . $tblpx . "purchase.p_id "
                    . " WHERE  "
                     . $tblpx . "bills.purchase_id IS NOT NULL  "
                    . " AND (" . $newQuery4 . ")  and ".$bill_condition;
                $purchase = Yii::app()->db->createCommand($purchasesql)->queryAll();

                $purchase1sql = "SELECT * FROM  " . $tblpx . "bills "
                    . " LEFT JOIN " . $tblpx . "purchase "
                    . " ON " . $tblpx . "bills.purchase_id=" . $tblpx . "purchase.p_id "
                    . " WHERE  "
                    . $tblpx . "bills.purchase_id IS NOT NULL  "
                    . " AND (" . $newQuery4 . ")  and ".$bill_condition1;
                $purchase1 = Yii::app()->db->createCommand($purchase1sql)->queryAll(); //un reconcil
            }

            $bill_amount = 0;
            $purchase_list = Projects::model()->groupPurchaseBillItems($purchase);
            //$expense_types_listing = Projects::model()->setExpenseTypesListingofPurchaseBill($purchase_list);

                foreach ($purchase_list as $purchase) {
                    $count = count($purchase);
                    $count = ($count - 2);
                    $index = 1;
                    foreach ($purchase as $key => $value) {
                         if (is_array($value)) {
                            $amount = ($value['bill_totalamount'] + $value['bill_additionalcharge']);
                            $vendor = Vendors::model()->findByPk($value['vendor_id']);
                            $bill_amount += $amount; 
                        }
                     $index++;
                    }
                }
            //echo ($bill_amount);exit;

            /*Purchase Bill Ends*/
            $vendor_details = Yii::app()->db->createCommand($vendor_sql)->queryRow();
            if($exp_cal==0){
                 $total_expense = $daybook_expense_tot_amount+$daybook_subcontractor_expenseamount+$bill_amount;

            }else{
                 $total_expense = $daybook_expense_tot_amount+$daybook_subcontractor_expenseamount;

            }
           
            $daily_expense_details = Yii::app()->db->createCommand("SELECT SUM(dailyexpense_paidamount) as total_amount FROM {$tblpx}dailyexpense WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND dailyexpense_type = 'expense' AND (" . $newQuery . ")")->queryRow();
            $daily_expense_petty_receipt = Yii::app()->db->createCommand("SELECT SUM(dailyexpense_receipt) as total_amount FROM {$tblpx}dailyexpense WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND dailyexpense_type = 'receipt' AND dailyexpense_receipt_type IN(103) AND (" . $newQuery . ")")->queryRow();

            $total_daily_expense = $daily_expense_details['total_amount'] - $daily_expense_petty_receipt['total_amount'];

            $depositt_details = Yii::app()->db->createCommand("SELECT SUM(dailyexpense_paidamount) as total_amount FROM {$tblpx}dailyexpense WHERE date BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND dailyexpense_type ='deposit' AND (" . $newQuery . ")")->queryRow();
            $total_deposit = $depositt_details['total_amount'];
            

            $consume_total_amount=0;
            if($exp_cal==1){
                 $consumesql = "
                SELECT 
                    SUM(c.item_qty * c.item_rate) AS total_amount
                FROM 
                    pms_acc_wpr_item_consumed c
                LEFT JOIN 
                    jp_consumption_request r ON r.id = c.consumption_id
                LEFT JOIN 
                    jp_projects E ON E.pid = r.coms_project_id
                WHERE 
                    r.status = 1
                    AND ".$newQuery3;
            $consume_total = Yii::app()->db->createCommand($consumesql)->queryRow();;
           
            $consume_total_amount = isset($consume_total['total_amount']) ? $consume_total['total_amount'] : 0;
                
                if($exp_cal==1){
                $total_expense+=$consume_total_amount;
                }
            }
            $laboursql = "
            SELECT 
                SUM(amount) AS total_amount
            FROM 
                jp_dailyreport
            WHERE 
                approval_status = '1'
                AND approve_status = 1
                AND (subcontractor_id IS NULL OR subcontractor_id = -1) AND ".$newQuery;
           
            $command = Yii::app()->db->createCommand($laboursql);
            $result = $command->queryRow();
            


        $total_labour_amount = isset($result['total_amount']) ? $result['total_amount'] : 0;
         $total_expense+=$total_labour_amount;
           
            
        }
        $render_data = array(
            'date_from' => $date_from, 
            'date_to' => $date_to, 
            'recepit' => $recepit, 
            'invoice' => $invoice, 
            'total_expense' => $total_expense, 
            'total_daily_expense' => $total_daily_expense, 
            'total_deposit' => $total_deposit, 
            'return' => $return, 
            'company_id' => $company_id,
            'total_labour_amount'=>$total_labour_amount,
            'consume_total_amount'=>$consume_total_amount,

        );
        if (filter_input(INPUT_GET, 'export') == 'pdf') {
            $this->logo = $this->realpath_logo;
            $mPDF1 = Yii::app()->ePdf->mPDF();
            $mPDF1 = Yii::app()->ePdf->mPDF('', 'A4'); 
            $sql = 'SELECT template_name '
                . 'FROM jp_quotation_template '
                . 'WHERE status="1"';
            $selectedtemplate =Yii::app()->db->createCommand($sql)->queryScalar();
        
            $template = $this->getTemplate($selectedtemplate);
            $mPDF1->SetHTMLHeader($template['header']);
            $mPDF1->SetHTMLFooter($template['footer']);
            $mPDF1->AddPage('','', '', '', '', 0,0,60, 30, 10,0);           
            $mPDF1->WriteHTML($this->renderPartial('profitandloss', $render_data, true));            
            $mPDF1->Output('P/L REPORT.pdf', 'D');
        } elseif (filter_input(INPUT_GET, 'export') == 'csv') {
            $exported_data = $this->renderPartial('profitandloss', $render_data, true);
            $export_filename = 'P/L-REPORT' . date('Ymd_His');
            ExportHelper::exportGridAsCSV($exported_data, $export_filename);
        }  else {
            $this->render('profitandloss', $render_data);
        }        
    }

    public function actionGetInvoiceDetails()
    {
        $data = explode(",", $_POST["billid"]);
        $billId = $data[1];
        $invoice_model = Invoice::model()->findByPk($billId);
        
        $sql = "SELECT SUM(cgst) as cgst_p ,SUM(cgst_amount) as cgst_amount,"
            . " SUM(sgst) as sgst_p ,SUM(sgst_amount) as sgst_amount,"
            . " SUM(igst) as igst_p ,SUM(igst_amount) as igst_amount "
            . " FROM `jp_inv_list` WHERE inv_id =".$invoice_model['invoice_id'];
        $inv_details = Yii::app()->db->createCommand($sql)->queryRow();

        $expsql = "SELECT SUM(receipt) FROM `jp_expenses` "
            . " WHERE invoice_id=".$billId;
        $invamount = Yii::app()->db->createCommand($expsql)->queryScalar();

        $result["amount"] = $invoice_model['amount'];
        $result["sgstp"] = (isset($inv_details['sgst_p']) && !is_null($inv_details['sgst_p'])) ? $inv_details['sgst_p']:'0.00' ;
        $result["sgst"] = (isset($inv_details['sgst_amount']) && !is_null($inv_details['sgst_amount'])) ? $inv_details['sgst_amount']:'0.00' ;
        $result["cgstp"] =  (isset($inv_details['cgst_p']) && !is_null($inv_details['cgst_p'])) ? $inv_details['cgst_p']:'0.00' ;
        $result["cgst"] =  (isset($inv_details['cgst_amount']) && !is_null($inv_details['cgst_amount'])) ? $inv_details['cgst_amount']:'0.00' ;
        $result["igstp"] =  (isset($inv_details['igst_p']) && !is_null($inv_details['igst_p'])) ? $inv_details['igst_p']:'0.00' ;
        $result["igst"] =  (isset($inv_details['igst_amount']) && !is_null($inv_details['igst_amount'])) ? $inv_details['igst_amount']:'0.00' ;
        $result["total"] = ($invoice_model['subtotal'] + $result["sgst"] + $result["cgst"] + $result["igst"]) - $invamount;
        $result['project_id'] = $invoice_model['project_id'];
        $result['company'] = $invoice_model['company_id'];
        
        echo json_encode($result);
    }

    public function actionAjaxcall()
    {
        echo json_encode('success');
    }

    public function actionDeleteexpense()
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $data = Expenses::model()->findByPk($_POST['expId']);
        $checkBalance = Expenses::model()->checkDeleteBalance($data);
        $sql = "SELECT count(ref_id) as expcount  FROM {$tblpx}pre_expenses WHERE approval_status=0 and  ref_id =".$_POST['expId'];
        $preexpanse_details = Yii::app()->db->createCommand($sql)->queryRow();
        $preexpanse_count = $preexpanse_details['expcount'] ;
        
        if($checkBalance == "1" && $preexpanse_count==0){
            if ($data) {
                $newmodel = new JpLog('search');
                $newmodel->log_data = json_encode($data->attributes);
                $newmodel->log_action = 2;
                $newmodel->log_table = $tblpx . "expenses";
                $newmodel->log_datetime = date('Y-m-d H:i:s');
                $newmodel->log_action_by = Yii::app()->user->id;
                $newmodel->company_id = $data->company_id;
                if ($newmodel->save()) {
                    $model = Expenses::model()->deleteAll(array("condition" => "exp_id=" . $_POST['expId'] . ""));
                    Reconciliation::model()->deleteAll(array("condition" => "reconciliation_parentid=" . $_POST['expId'] . ""));
                    $expDate = date('Y-m-d', strtotime($_POST['expense_date']));
                    $user = Users::model()->findByPk(Yii::app()->user->id);
                    $arrVal = explode(',', $user->company_id);
                    $newQuery = "";
                    foreach ($arrVal as $arr) {
                        if ($newQuery) $newQuery .= ' OR';
                        $newQuery .= " FIND_IN_SET('" . $arr . "', e.company_id)";
                    }

                    $sql = "SELECT e.*, p.name as pname, b.bill_number as billno, "
                            . " et.type_name as typename,v.name vname,ba.bank_name as bankname, "
                            . " i.inv_no as invoice_no FROM " . $tblpx . "expenses e "
                            . " LEFT JOIN " . $tblpx . "projects p ON e.projectid = p.pid "
                            . " LEFT JOIN " . $tblpx . "bills b ON e.bill_id = b.bill_id "
                            . " LEFT JOIN " . $tblpx . "expense_type et ON e.exptype = et.type_id "
                            . " LEFT JOIN " . $tblpx . "vendors v ON e.vendor_id = v.vendor_id "
                            . " LEFT JOIN " . $tblpx . "bank ba ON e.bank_id = ba.bank_id "
                            . " LEFT JOIN " . $tblpx . "invoice i ON e.invoice_id = i.invoice_id "
                            . " WHERE e.date = '" . $expDate . "' AND (" . $newQuery . ")";
                    // $expenseData = Yii::app()->db->createCommand($sql)->queryAll();
                    // $client = $this->renderPartial('newlist', array('newmodel' => $expenseData));
                    $client='';
                    echo json_encode(array('response'=>0,'data'=>$client));                     
                } else {
                    echo json_encode(array('response'=>1,'data'=>''));                    
                }
            }
        }else{
            echo json_encode(array('response'=>2,'data'=>$checkBalance));
        }
    }

    public function actionGetPurchaseReturnDetails()
    {
        $data = explode(",", $_POST["billid"]);
        $billId = $data[1];
        $tblpx = Yii::app()->db->tablePrefix;
        $returnData = Yii::app()->db->createCommand("SELECT * from " . $tblpx . "purchase_returnitem WHERE return_id = " . $billId)->queryAll();
        $purchase_return = Yii::app()->db->createCommand("SELECT *," . $tblpx . "bills.company_id as companyid FROM " . $tblpx . "purchase_return LEFT JOIN " . $tblpx . "bills ON " . $tblpx . "purchase_return.bill_id= " . $tblpx . "bills.bill_id LEFT JOIN " . $tblpx . "purchase ON " . $tblpx . "bills.purchase_id=" . $tblpx . "purchase.p_id WHERE " . $tblpx . "purchase_return.return_id=" . $billId . " AND " . $tblpx . "bills.purchase_id IS NOT NULL ORDER BY " . $tblpx . "purchase_return.return_id DESC")->queryRow();
        $vendor_data = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}vendors WHERE vendor_id=" . $purchase_return['vendor_id'])->queryRow();

        $expsql = "SELECT SUM(paidamount) FROM `jp_expenses` "
            . " WHERE return_id=".$billId;
        $retamount = Yii::app()->db->createCommand($expsql)->queryScalar();

        $result["amount"] = 0;
        $result["sgstp"] = 0;
        $result["sgst"] = 0;
        $result["cgstp"] = 0;
        $result["cgst"] = 0;
        $result["igstp"] = 0;
        $result["igst"] = 0;
        $result["total"] = 0;
        $result['project_id'] = $purchase_return['project_id'];
        $result['company'] = $purchase_return['companyid'];
        $amount = 0;
        $sgstp = 0;
        $sgst = 0;
        $cgstp = 0;
        $cgst = 0;
        $igstp = 0;
        $igst = 0;
        $sgst_amount = 0;
        $cgst_amount = 0;
        $igst_amount = 0;

        foreach ($returnData as $item) {
            $amount += $result["amount"] + ($item["returnitem_amount"] - $item["returnitem_discountamount"]);
            $sgstp += $result["sgstp"] + $item["returnitem_sgstpercent"];
            $sgst += $result["sgst"] + $item["returnitem_sgst"];
            $cgstp += $result["cgstp"] + $item["returnitem_cgstpercent"];
            $cgst += $result["cgst"] + $item["returnitem_cgst"];
            $igstp += $result["igstp"] + $item["returnitem_igstpercent"];
            $igst += $result["igst"] + $item["returnitem_igst"];
        }
        if ($amount != 0 || $amount != '') {
            $sgst_p = ($sgst / $amount) * 100;
            $cgst_p = ($cgst / $amount) * 100;
            $igst_p = ($igst / $amount) * 100;

            $sgst_amount = ($amount * $sgst_p) / 100;
            $cgst_amount = ($amount * $cgst_p) / 100;
            $igst_amount = ($amount * $igst_p) / 100;
        } else {
            $sgst_amount = 0;
            $cgst_amount = 0;
            $igst_amount = 0;
        }
        $result["vendor_id"] = $purchase_return['vendor_id'];
        $result["project_id"] = $purchase_return['project_id'];
        $result["company"] = $purchase_return['company_id'];
        $result["vendor_name"] = $vendor_data['name'];
        $result["expensehead_id"] = $purchase_return['expensehead_id'];
        if ($purchase_return['expensehead_id'] != NULL) {
            $expense_data = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}expense_type WHERE type_id=" . $purchase_return['expensehead_id'])->queryRow();
            $result["expense_name"] = $expense_data['type_name'];
        }

        $result["amount"] = $amount;
        $result["sgstp"] = $sgstp;
        $result["sgst"] = $sgst_amount;
        $result["cgstp"] = $cgstp;
        $result["cgst"] = $cgst_amount;
        $result["igstp"] = $igstp;
        $result["igst"] = $igst_amount;
        $result["total"] = ($amount + $sgst_amount + $cgst_amount + $igst_amount);
        echo json_encode($result);
    }



    public function actionDynamicProject()
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $comId         = $_POST["comId"];
        $tblpx         = Yii::app()->db->tablePrefix;
        $projectData    = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}projects WHERE FIND_IN_SET(" . $comId . ", company_id) ORDER BY name ASC")->queryAll();
        $projectOptions = "<option value=''>-Select Project-</option>";
        foreach ($projectData as $vData) {
            $projectOptions  .= "<option value='" . $vData["pid"] . "'>" . $vData["name"] . "</option>";
        }
        $projectOptions;

        $bankData    = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}bank WHERE FIND_IN_SET(" . $comId . ", company_id) ORDER BY bank_name ASC")->queryAll();
        $bankOptions = "<option value=''>-Select Bank-</option>";
        foreach ($bankData as $vData) {
            $bankOptions  .= "<option value='" . $vData["bank_id"] . "'>" . $vData["bank_name"] . "</option>";
        }
        echo json_encode(array('project' => $projectOptions, 'bank' => $bankOptions));
    }
    public function actionDeleteconfirmation()
    {
        $expId      = $_REQUEST["expId"];
        $date       = $_REQUEST["expense_date"];
        $status     = $_REQUEST["status"];
        $delId      = $_REQUEST["delId"];
        $action     = $_REQUEST["action"];
        $tblpx      = Yii::app()->db->tablePrefix;
        $success    = "";
        if ($status == 1) {
            $deletedata = new Deletepending;
            $model      = Expenses::model()->findByPk($expId);
            $deletedata->deletepending_table    = $tblpx . "expenses";
            $deletedata->deletepending_data     = json_encode($model->attributes);
            $deletedata->deletepending_parentid = $expId;
            $deletedata->user_id                = Yii::app()->user->id;
            $deletedata->requested_date         = date("Y-m-d");
            if ($deletedata->save()) {
                $success = "Yes";
            } else {
                echo 1;
            }
        } else if ($status == 2) {
            $model  = Deletepending::model()->findByPk($delId);
            if ($model->delete()) {
                $success = "Yes";
            } else {
                echo 1;
            }
        }

        if ($success == "Yes") {
            $expDate    = date('Y-m-d', strtotime($_REQUEST['expense_date']));
            $user       = Users::model()->findByPk(Yii::app()->user->id);
            $arrVal     = explode(',', $user->company_id);
            $newQuery   = "";
            foreach ($arrVal as $arr) {
                if ($newQuery) $newQuery .= ' OR';
                $newQuery .= " FIND_IN_SET('" . $arr . "', e.company_id)";
            }
            $expenseData    = Yii::app()->db->createCommand("SELECT e.*, p.name as pname, b.bill_number as billno, et.type_name as typename,v.name vname,ba.bank_name as bankname, i.inv_no as invoice_no FROM " . $tblpx . "expenses e
                                LEFT JOIN " . $tblpx . "projects p ON e.projectid = p.pid
                                LEFT JOIN " . $tblpx . "bills b ON e.bill_id = b.bill_id
                                LEFT JOIN " . $tblpx . "expense_type et ON e.exptype = et.type_id
                                LEFT JOIN " . $tblpx . "vendors v ON e.vendor_id = v.vendor_id
                                LEFT JOIN " . $tblpx . "bank ba ON e.bank_id = ba.bank_id
                                LEFT JOIN " . $tblpx . "invoice i ON e.invoice_id = i.invoice_id
                                WHERE e.date = '" . $expDate . "' AND (" . $newQuery . ")")->queryAll();
            $client         = $this->renderPartial('newlist', array('newmodel' => $expenseData));
            return $client;
        }
    }
    public function actionDeleterequest()
    {
        $model = new Deletepending;
        $this->render('deleterequests', array(
            'model' => $model,
            'dataProvider' => $model->search(),
        ));
    }
    public function actionDeleterequestaction()
    {
        $delId      = $_REQUEST["delId"];
        $status     = $_REQUEST["status"];
        $tblpx      = Yii::app()->db->tablePrefix;
        $request    = Deletepending::model()->findByPk($delId);
        if ($status == 1) {
            $parentTable    = $request->deletepending_table;
            $parentId       = $request->deletepending_parentid;
            if ($parentTable == "{$tblpx}expenses") {
                $data = Expenses::model()->findByPk($parentId);
                $tblpx = Yii::app()->db->tablePrefix;
                if ($data) {
                    $newmodel = new JpLog('search');
                    $newmodel->log_data = json_encode($data->attributes);
                    $newmodel->log_action = 2;
                    $newmodel->log_table = $tblpx . "expenses";
                    $newmodel->log_datetime = date('Y-m-d H:i:s');
                    $newmodel->log_action_by = Yii::app()->user->id;
                    $newmodel->company_id = $data->company_id;
                    if ($newmodel->save()) {
                        $model = Expenses::model()->deleteAll(array("condition" => "exp_id=" . $parentId . ""));
                        Reconciliation::model()->deleteAll(array("condition" => "reconciliation_parentid=" . $parentId . ""));
                    }
                }
            } else if ($parentTable == "{$tblpx}dailyexpense") {
                $data       = Dailyexpense::model()->findByPk($parentId);
                $tblpx      = Yii::app()->db->tablePrefix;
                $user       = Users::model()->findByPk(Yii::app()->user->id);
                $arrVal     = explode(',', $user->company_id);
                $newQuery   = "";
                foreach ($arrVal as $arr) {
                    if ($newQuery)
                        $newQuery .= ' OR';
                    $newQuery .= " FIND_IN_SET('" . $arr . "', e.company_id)";
                }
                $transferId = $data->transfer_parentid;
                if ($transferId > 0) {
                    $transferData   = Cashtransfer::model()->findByPk($transferId);
                    $dailyexpense   = Dailyexpense::model()->findAll(array('condition' => 'transfer_parentid="' . $transferId . '"'));
                    Dailyexpense::model()->deleteAll(array('condition' => 'transfer_parentid = ' . $transferId));
                    Cashtransfer::model()->deleteAll(array('condition' => 'cashtransfer_id = ' . $transferId));
                } else {
                    $newmodel = new JpLog('search');
                    $newmodel->log_data = json_encode($data->attributes);
                    $newmodel->log_action = 2;
                    $newmodel->log_table = $tblpx . "dailyexpense";
                    $newmodel->log_datetime = date('Y-m-d H:i:s');
                    $newmodel->log_action_by = Yii::app()->user->id;
                    $newmodel->company_id = $data->company_id;
                    if ($newmodel->save()) {
                        if ($data->expensehead_type == 4 || $data->expensehead_type == 5) {
                            $model1 = Dailyexpense::model()->deleteAll(array("condition" => "transaction_parent=" . $data->dailyexp_id . ""));
                        }
                        $model = Dailyexpense::model()->deleteAll(array("condition" => "dailyexp_id=" . $parentId . ""));
                        Reconciliation::model()->deleteAll(array("condition" => "reconciliation_parentid=" . $parentId . ""));
                    }
                }
            } else if ($parentTable == "{$tblpx}dailyvendors") {
                $data = Dailyvendors::model()->findByPk($parentId);
                $tblpx = Yii::app()->db->tablePrefix;
                if ($data) {
                    $newmodel = new JpLog('search');
                    $newmodel->log_data = json_encode($data->attributes);
                    $newmodel->log_action = 2;
                    $newmodel->log_table = $tblpx . "dailyvendors";
                    $newmodel->log_datetime = date('Y-m-d H:i:s');
                    $newmodel->log_action_by = Yii::app()->user->id;
                    $newmodel->company_id = $data->company_id;
                    if ($newmodel->save()) {
                        $model = Dailyvendors::model()->deleteAll(array("condition" => "daily_v_id=" . $parentId . ""));
                        Reconciliation::model()->deleteAll(array("condition" => "reconciliation_parentid=" . $parentId . ""));
                    }
                }
            } else if ($parentTable == "{$tblpx}subcontractor_payment") {
                $data       = SubcontractorPayment::model()->findByPk($parentId);
                $uniqueid   = $data->uniqueid;
                $tblpx      = Yii::app()->db->tablePrefix;
                if ($uniqueid) {
                    $paymentdata = SubcontractorPayment::model()->findAll(array("condition" => "uniqueid='" . $uniqueid . "'"));
                    foreach ($paymentdata as $pdata) {
                        $newmodel                   = new JpLog('search');
                        $newmodel->log_data         = json_encode($pdata->attributes);
                        $newmodel->log_action       = 2;
                        $newmodel->log_table        = $tblpx . "subcontractor_payment";
                        $newmodel->log_datetime     = date('Y-m-d H:i:s');
                        $newmodel->log_action_by    = Yii::app()->user->id;
                        $newmodel->company_id       = $pdata->company_id;
                        if ($newmodel->save()) {
                            SubcontractorPayment::model()->deleteAll(array("condition" => "payment_id=" . $pdata->payment_id . ""));
                            Expenses::model()->deleteAll(array("condition" => "subcontractor_id=" . $pdata->payment_id . ""));
                        }
                    }
                    Reconciliation::model()->deleteAll(array("condition" => "reconciliation_parentids = '" . $uniqueid . "'"));
                } else {
                    if ($data) {
                        $newmodel = new JpLog('search');
                        $newmodel->log_data = json_encode($data->attributes);
                        $newmodel->log_action = 2;
                        $newmodel->log_table = $tblpx . "subcontractor_payment";
                        $newmodel->log_datetime = date('Y-m-d H:i:s');
                        $newmodel->log_action_by = Yii::app()->user->id;
                        $newmodel->company_id = $data->company_id;
                        if ($newmodel->save()) {
                            $expLoad = Expenses::model()->find(array(
                                'select' => array('exp_id'),
                                "condition" => "subcontractor_id='" . $parentId . "'",
                            ));
                            $expOldId = $expLoad["exp_id"];
                            if ($expLoad) {
                                Reconciliation::model()->deleteAll(array("condition" => "reconciliation_parentid=" . $expOldId . ""));
                            }
                            SubcontractorPayment::model()->deleteAll(array("condition" => "payment_id=" . $parentId . ""));
                            Expenses::model()->deleteAll(array("condition" => "subcontractor_id=" . $parentId . ""));
                        }
                    }
                }
            }
            $delmodel = new Deletepending;
            $result = $this->widget('zii.widgets.CListView', array(
                'dataProvider' => $delmodel->search(),
                'itemView' => '_newdeleterequest', 'template' => '<div class=""><table cellpadding="10" class="table" id="clientab">{items}</table></div>',
                'ajaxUpdate' => false,
            ));
            return $result;
        } else if ($status == 2) {
            $request->delete();
            $delmodel = new Deletepending;
            $result = $this->widget('zii.widgets.CListView', array(
                'dataProvider' => $delmodel->search(),
                'itemView' => '_newdeleterequest', 'template' => '<div class=""><table cellpadding="10" class="table" id="clientab">{items}</table></div>',
                'ajaxUpdate' => false,
            ));
            return $result;
        } else {
            echo 1;
        }
    }
    public function actionExpensereportbyuser()
    {
        $tblpx      = Yii::app()->db->tablePrefix;
        $currUser   = Yii::app()->user->id;
        $model      = new Expenses;
        $model->unsetAttributes();
        $model->created_by = $currUser;
        if (isset($_GET["Expenses"])) {
            $model->fromdate   = $_REQUEST["Expenses"]["fromdate"];
            $model->todate     = $_REQUEST["Expenses"]["todate"];
        }

        $this->render('expensebyuser', array(
            'model' => $model,
            'dataProvider' => $model->usersearch(),
        ));
    }
    public function actionDuplicateentry()
    {
        $tblpx  = Yii::app()->db->tablePrefix;
        $model  = new Expenses;
        $final_array = array();
        $a = 0;
        $b = 0;
        $c = 0;
        $d = 0;
        $e = 0;
        $f = 0;
        // daybook
        $entrysql = "SELECT  count(e.projectid) as rowcount, e.* "
            . " FROM {$tblpx}expenses e "
            . " WHERE subcontractor_id IS NULL "
            . " AND  duplicate_ignore_status ='0' "
            . " GROUP BY e.projectid, e.amount, e.paid, e.date,e.description"
            . " HAVING COUNT(e.projectid) > 1";
        $entry  = Yii::app()->db->createCommand($entrysql)->queryAll();

        foreach ($entry as $key => $value) {
            $company = Company::model()->findByPk($value["company_id"]);
            $project = Projects::model()->findByPk($value["projectid"]);
            $bill = Bills::model()->findByPk($value["bill_id"]);
            $invoice = Invoice::model()->findByPk($value["invoice_id"]);
            $exphead = ExpenseType::model()->findByPk($value["exptype"]);
            $vendor = Vendors::model()->findByPk($value["vendor_id"]);
            $exptype = Status::model()->findByPk($value["expense_type"]);
            $pettycash = Users::model()->findByPk($value["employee_id"]);
            $bank = Bank::model()->findByPk($value["bank_id"]);
            $receipttype = Status::model()->findByPk($value["payment_type"]);
            if ($value["purchase_type"] == 0) {
                $purchase_type = "Credit";
            } else if ($value["purchase_type"] == 1) {
                $purchase_type = "Full Paid";
            } else if ($value["purchase_type"] == 2) {
                $purchase_type = "Partially Paid";
            }

            $final_array[$key]['projectid'] = $value['projectid'];
            $final_array[$key]['vendor_id'] = $value['vendor_id'];
            $final_array[$key]['subcontractor_id'] = "";
            $final_array[$key]['rowcount'] = $value['rowcount'];
            $final_array[$key]['id'] = $value['exp_id'];
            $final_array[$key]['date'] = $value['date'];
            $final_array[$key]['section'] = 'Daybook';
            $final_array[$key]['company'] = isset($company->name) ? $company->name : "";
            $final_array[$key]['project'] = isset($project->name) ? $project->name : "";
            $final_array[$key]['subcontractor'] = "";
            $final_array[$key]['billno'] = isset($bill->bill_number) ? $bill->bill_number : "";
            $final_array[$key]['invoiceno'] = isset($invoice->inv_no) ? $invoice->inv_no : "";
            $final_array[$key]['expense_head'] = isset($exphead->type_name) ? $exphead->type_name : "";
            $final_array[$key]['vendor'] = isset($vendor->name) ? $vendor->name : "";
            $final_array[$key]['expense_type'] = isset($exptype->caption) ? $exptype->caption : "";
            $final_array[$key]['pettycash_by'] = isset($pettycash->first_name) ? $pettycash->first_name . " " . $pettycash->last_name : "";
            $final_array[$key]['Bank'] = isset($bank->bank_id) ? $bank->bank_id : "";
            $final_array[$key]['check'] = $value["cheque_no"];
            $final_array[$key]['description'] = $value["description"];
            $final_array[$key]['amount'] = isset($value["expense_amount"]) ? $value["expense_amount"] : "";
            $final_array[$key]['tax'] = Controller::money_format_inr($value["expense_cgst"] + $value["expense_sgst"] + $value["expense_igst"], 2);
            $final_array[$key]['total_amount'] = isset($value["amount"]) ? $value["amount"] : "";
            $final_array[$key]['paid_amount'] = isset($value["paid"]) ? $value["paid"] : "";
            $final_array[$key]['tds'] = isset($value["expense_tds"]) ? Controller::money_format_inr($value["expense_tds"], 2) : "";
            $final_array[$key]['receipt_type'] = isset($receipttype->caption) ? $receipttype->caption : "";
            $final_array[$key]['receipt'] = isset($value["receipt"]) ? Controller::money_format_inr($value["receipt"], 2) : "";
            $final_array[$key]['purchase_type'] = $purchase_type;
            $final_array[$key]['beneficiary_amount'] = isset($value["paidamount"]) ? Controller::money_format_inr($value["paidamount"], 2) : "";
            $final_array[$key]['paid_to'] = "";
            $final_array[$key]['duplicate_delete_status'] = $value['duplicate_delete_status'];
        }

        // dailyexpense
        $entry2sql = "SELECT  count(d.dailyexp_id) as rowcount, d.* "
        . " FROM {$tblpx}dailyexpense d "
        . " WHERE d.dailyexpense_paidamount IS NOT NULL "
        . " AND  duplicate_ignore_status ='0' "
        . " GROUP BY d.amount, d.dailyexpense_paidamount, d.date,d.description "
        . " HAVING COUNT(d.dailyexp_id) > 1";
        $entry2  = Yii::app()->db->createCommand($entry2sql)->queryAll();
        $a = count($entry);
        foreach ($entry2 as $key => $value) {
            $company = Company::model()->findByPk($value["company_id"]);
            if ($value["dailyexpense_type"] == 'expense') {
                $expense = ExpenseType::model()->findByPk($value["expensehead_id"]);
                if ($expense) {
                    $expense_head = "Expense - " . $expense->type_name;
                }
            } else if ($value["dailyexpense_type"] == 'receipt') {
                $receipt = Companyexpensetype::model()->findByPk($value["dailyexpense_receipt_head"]);
                if ($receipt) {
                    $expense_head = "Receipt - " . $receipt->name;
                }
            } else if ($value["dailyexpense_type"] == 'deposit') {
                $deposit = Companyexpensetype::model()->findByPk($value["exp_type_id"]);
                if ($deposit) {
                    $expense_head = "Deposit - " . $deposit->deposit_name;
                }
            }
            if ($value["dailyexpense_type"] == 'receipt') {
                $exptype = Status::model()->findByPk($value["dailyexpense_receipt_type"]);
            } else {
                $exptype = Status::model()->findByPk($value["expense_type"]);
            }
            $bank = Bank::model()->findByPk($value["bank_id"]);
            if ($value["dailyexpense_purchase_type"] == 1) {
                $purchase_type = "Credit";
            } else if ($value["dailyexpense_purchase_type"] == 2) {
                $purchase_type = "Full Paid";
            } else if ($value["dailyexpense_purchase_type"] == 3) {
                $purchase_type = "Partially Paid";
            }
            $employee = Users::model()->findByPk($value["employee_id"]);
            $final_array[$a]['projectid'] = "";
            $final_array[$a]['vendor_id'] = "";
            $final_array[$a]['subcontractor_id'] = "";
            $final_array[$a]['rowcount'] = $value['rowcount'];
            $final_array[$a]['id'] = $value['dailyexp_id'];
            $final_array[$a]['date'] = $value['date'];
            $final_array[$a]['section'] = 'Dailyexpense';
            $final_array[$a]['company'] = isset($company->name) ? $company->name : "";
            $final_array[$a]['project'] = "";
            $final_array[$a]['subcontractor'] = "";
            $final_array[$a]['billno'] = $value['bill_id'];
            $final_array[$a]['invoiceno'] = "";
            $final_array[$a]['expense_head'] = isset($expense_head) ? $expense_head : "";
            $final_array[$a]['vendor'] = "";
            $final_array[$a]['expense_type'] = isset($exptype->caption) ? $exptype->caption : "";
            $final_array[$a]['pettycash_by'] = "";
            $final_array[$a]['Bank'] = isset($bank->bank_id) ? $bank->bank_id : "";
            $final_array[$a]['check'] = $value["dailyexpense_chequeno"];
            $final_array[$a]['description'] = $value["description"];
            $final_array[$a]['amount'] = isset($value["dailyexpense_amount"]) ? $value["dailyexpense_amount"] : "";
            $final_array[$a]['tax'] = Controller::money_format_inr($value["dailyexpense_cgst"] + $value["dailyexpense_sgst"] + $value["dailyexpense_igst"], 2);
            $final_array[$a]['total_amount'] = isset($value["amount"]) ? $value["amount"] : "";
            $final_array[$a]['paid_amount'] = isset($value["dailyexpense_paidamount"]) ? $value["dailyexpense_paidamount"] : "";
            $final_array[$a]['tds'] = "";
            $final_array[$a]['receipt_type'] = "";
            $final_array[$a]['receipt'] = isset($value["dailyexpense_receipt"]) ? Controller::money_format_inr($value["dailyexpense_receipt"], 2) : "";
            $final_array[$a]['purchase_type'] = $purchase_type;
            $final_array[$a]['beneficiary_amount'] = "";
            $final_array[$a]['paid_to'] = isset($employee->first_name) ? $employee->first_name . " " . $employee->last_name : "";
            $final_array[$a]['duplicate_delete_status'] = $value['duplicate_delete_status'];
            $a++;
        }

        // sub-contractor payment
        $entry3sql = "SELECT  count(s.payment_id) as rowcount, s.* "
        . " FROM {$tblpx}subcontractor_payment s WHERE "
        . " duplicate_ignore_status ='0'"
        . " GROUP BY s.subcontractor_id, s.project_id, s.amount, "
        . " s.date, s.description "
        . " HAVING COUNT(s.payment_id) > 1";
        $entry3  = Yii::app()->db->createCommand($entry3sql)->queryAll();
        $b = count($entry) + count($entry2);
        foreach ($entry3 as $key => $value) {
            $company = Company::model()->findByPk($value["company_id"]);
            $project = Projects::model()->findByPk($value["project_id"]);
            $subcon = Subcontractor::model()->findByPk($value["subcontractor_id"]);
            $ptype = Status::model()->findByPk($value["payment_type"]);
            $bank = Bank::model()->findByPk($value["bank"]);
            $final_array[$b]['projectid'] = $value['project_id'];
            $final_array[$b]['vendor_id'] = "";
            $final_array[$b]['subcontractor_id'] = $value['subcontractor_id'];
            $final_array[$b]['rowcount'] = $value['rowcount'];
            $final_array[$b]['id'] = $value['payment_id'];
            $final_array[$b]['date'] = $value['date'];
            $final_array[$b]['section'] = 'Subcontractor Payment';
            $final_array[$b]['company'] = isset($company->name) ? $company->name : "";
            $final_array[$b]['project'] = isset($project->name) ? $project->name : "";
            $final_array[$b]['subcontractor'] = isset($subcon->subcontractor_name) ? $subcon->subcontractor_name : "";
            $final_array[$b]['billno'] = "";
            $final_array[$b]['invoiceno'] = "";
            $final_array[$b]['expense_head'] = "";
            $final_array[$b]['vendor'] = "";
            $final_array[$b]['expense_type'] = isset($ptype->caption) ? $ptype->caption : "";
            $final_array[$b]['pettycash_by'] = "";
            $final_array[$b]['Bank'] = isset($bank->bank_name) ? $bank->bank_name : "";
            $final_array[$b]['check'] = $value["cheque_no"];
            $final_array[$b]['description'] = $value["description"];
            $final_array[$b]['amount'] = isset($value["amount"]) ? $value["amount"] : "";
            $final_array[$b]['tax'] = isset($value["tax_amount"]) ? Controller::money_format_inr($value["tax_amount"], 2) : "";
            $final_array[$b]['total_amount'] =  isset($value["amount"]) ? $value["amount"] : "";
            $final_array[$b]['paid_amount'] = isset($value["paidamount"]) ? $value["paidamount"] : "";
            $final_array[$b]['tds'] = isset($value["tds_amount"]) ? Controller::money_format_inr($value["tds_amount"], 2) : "";
            $final_array[$b]['receipt_type'] = "";
            $final_array[$b]['receipt'] = "";
            $final_array[$b]['purchase_type'] = "";
            $final_array[$b]['beneficiary_amount'] = "";
            $final_array[$b]['paid_to'] = "";
            $final_array[$b]['duplicate_delete_status'] = $value['duplicate_delete_status'];
            $b++;
        }

        // vensoy payment
        $c = count($entry) + count($entry2) + count($entry3);
        $entry4sql = "SELECT  count(d.daily_v_id) as rowcount, d.* "
            . " FROM {$tblpx}dailyvendors d WHERE "
            . " duplicate_ignore_status ='0' "
            . " GROUP BY d.vendor_id, d.date, d.amount, d.description "
            . " HAVING COUNT(d.daily_v_id) > 1";
        $entry4  = Yii::app()->db->createCommand($entry4sql)->queryAll();
        foreach ($entry4 as $key => $value) {
            $company = Company::model()->findByPk($value["company_id"]);
            $project = Projects::model()->findByPk($value["project_id"]);
            $exptype = Status::model()->findByPk($value["payment_type"]);
            $bank = Bank::model()->findByPk($value["bank"]);
            $vendor = Vendors::model()->findByPk($value["vendor_id"]);
            $final_array[$c]['projectid'] = $value['project_id'];
            $final_array[$c]['vendor_id'] = $value['vendor_id'];
            $final_array[$c]['subcontractor_id'] = "";
            $final_array[$c]['rowcount'] = $value['rowcount'];
            $final_array[$c]['id'] = $value['daily_v_id'];
            $final_array[$c]['date'] = $value['date'];
            $final_array[$c]['section'] = 'Vendor Payment';
            $final_array[$c]['company'] = isset($company->name) ? $company->name : "";
            $final_array[$c]['project'] = isset($project->name) ? $project->name : "";
            $final_array[$c]['subcontractor'] = "";
            $final_array[$c]['billno'] = "";
            $final_array[$c]['invoiceno'] = "";
            $final_array[$c]['expense_head'] = "";
            $final_array[$c]['vendor'] = isset($vendor->name) ? $vendor->name : "";
            $final_array[$c]['expense_type'] = isset($exptype->caption) ? $exptype->caption : "";
            $final_array[$c]['pettycash_by'] = "";
            $final_array[$c]['Bank'] = isset($bank->bank_id) ? $bank->bank_id : "";
            $final_array[$c]['check'] = $value["cheque_no"];
            $final_array[$c]['description'] = $value["description"];
            $final_array[$c]['amount'] = isset($value["amount"]) ? $value["amount"] : "";
            $final_array[$c]['tax'] = isset($value["tax_amount"]) ? Controller::money_format_inr($value["tax_amount"], 2) : "";
            $final_array[$c]['total_amount'] = isset($value["amount"]) ? $value["amount"] : "";
            $final_array[$c]['paid_amount'] = isset($value["paidamount"]) ? $value["paidamount"]: "";
            $final_array[$c]['tds'] = isset($value["tds_amount"]) ? Controller::money_format_inr($value["tds_amount"], 2) : "";
            $final_array[$c]['receipt_type'] = "";
            $final_array[$c]['receipt'] = "";
            $final_array[$c]['purchase_type'] = "";
            $final_array[$c]['beneficiary_amount'] = "";
            $final_array[$c]['paid_to'] = "";
            $final_array[$c]['duplicate_delete_status'] = $value['duplicate_delete_status'];
            $c++;
        }

        //purchase bill
        $entry5sql = "SELECT count(b.bill_number) as rowcount, b.* ,p.* "
            . " FROM jp_bills b LEFT JOIN jp_purchase p "
            . " ON b.purchase_id=p.p_id  WHERE "
            . "  b.duplicate_ignore_status ='0' "
            . " GROUP BY b.bill_number, p.vendor_id "
            . " HAVING COUNT(b.bill_number) > 1";
        $entry5  = Yii::app()->db->createCommand($entry5sql)->queryAll();
        $d = count($entry) + count($entry2) + count($entry3) + count($entry4);
        foreach ($entry5 as $key => $value) {
            $company = Company::model()->findByPk($value["company_id"]);
            $project = Projects::model()->findByPk($value["project_id"]);
            $vendor = Vendors::model()->findByPk($value["vendor_id"]);
            $final_array[$d]['projectid'] = $value['project_id'];
            $final_array[$d]['vendor_id'] = $value['vendor_id'];
            $final_array[$d]['subcontractor_id'] = "";
            $final_array[$d]['rowcount'] = $value['rowcount'];
            $final_array[$d]['id'] = $value['bill_id'];
            $final_array[$d]['date'] = $value['bill_date'];
            $final_array[$d]['section'] = 'Purchase Bill';
            $final_array[$d]['company'] = isset($company->name) ? $company->name : "";
            $final_array[$d]['project'] = isset($project->name) ? $project->name : "";
            $final_array[$d]['subcontractor'] = "";
            $final_array[$d]['billno'] =  $value['bill_number'];
            $final_array[$d]['invoiceno'] = "";
            $final_array[$d]['expense_head'] = "";
            $final_array[$d]['vendor'] = isset($vendor->name) ? $vendor->name : "";
            $final_array[$d]['expense_type'] = "";
            $final_array[$d]['pettycash_by'] = "";
            $final_array[$d]['Bank'] = "";
            $final_array[$d]['check'] = "";
            $final_array[$d]['description'] = "";
            $final_array[$d]['amount'] = isset($value["bill_amount"]) ? $value["bill_amount"] : "";
            $final_array[$d]['tax'] = isset($value["bill_taxamount"]) ? Controller::money_format_inr($value["bill_taxamount"], 2) : "";
            $final_array[$d]['total_amount'] = isset($value["bill_totalamount"]) ? Controller::money_format_inr($value["bill_totalamount"], 2) : "";
            $final_array[$d]['paid_amount'] = "";
            $final_array[$d]['tds'] = "";
            $final_array[$d]['receipt_type'] = "";
            $final_array[$d]['receipt'] = "";
            $final_array[$d]['purchase_type'] = "";
            $final_array[$d]['beneficiary_amount'] = "";
            $final_array[$d]['paid_to'] = "";
            $final_array[$d]['duplicate_delete_status'] = $value['duplicate_delete_status'];
            $d++;
        }


        array_multisort(array_map(function ($element) {
            return $element['date'];
        }, $final_array), SORT_DESC, $final_array);

        $alldata = new CArrayDataProvider($final_array, array(
            'id' => 'id',
            'keyField' => 'id',
            'pagination' => array('pageSize' => 20),
        ));

        $this->render('duplicateentry', array(
            'model' => $model,
            'duplicateentry' => $entry,
            'final_array' => $final_array,
            'alldata' => $alldata,
        ));
    }
    public function actionGetduplicateentries()
    {
        $projectid  = $_REQUEST["projectid"];
        $amount     = $_REQUEST["amount"];
        $paid       = $_REQUEST["paid"];
        $date       = $_REQUEST["date"];
        $description = $_REQUEST["description"];
        $tblpx      = Yii::app()->db->tablePrefix;
        $duplicate  = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}expenses e WHERE e.projectid = {$projectid} AND amount = {$amount} AND paid = {$paid} AND  date = '{$date}' AND description='{$description}' ")->queryAll();
        $result     = '<button type="button" class="close"  aria-hidden="true">×</button><h4>Daybook ('.count($duplicate).')</h4><div class="table-responsive"><table class="table table-bordered table-hover">
                            <thead class="entry-table">
                                <tr>
                                    <th>Sl No.</th>
                                    <th>Company</th>
                                    <th>Project</th>
                                    <th>Bill Number</th>
                                    <th>Invoice Number</th>            
                                    <th>Expense Head</th>
                                    <th>Vendor</th>
                                    <th>Expense Type</th>
                                    <th>Bank</th>
                                    <th>Cheque No</th>
                                    <th>Description</th>
                                    <th>Total</th>
                                    <th>Paid</th>
                                    <th>Receipt Type</th>
                                    <th>Receipt</th>
                                    <th>Purchase Type</th>
                                </tr>   
                            </thead>
                            <tbody>';
        $i = 1;
        foreach ($duplicate as $entry) {
            $result .= '         <tr>
                                    <td>' . $i . '</td>
                                    <td>';
            $company = Company::model()->findByPk($entry["company_id"]);
            if ($company)
                $result .= '        ' . $company->name;
            $result .= '             </td>
                                    <td>';
            $project = Projects::model()->findByPk($entry["projectid"]);
            if ($project)
                $result .= '        ' . $project->name;
            $result .= '             </td>
                                    <td>';
            $bill = Bills::model()->findByPk($entry["bill_id"]);
            if ($bill)
                $result .= '        ' . $bill->bill_number;
            $result .= '             </td>
                                    <td>';
            $invoice = Invoice::model()->findByPk($entry["invoice_id"]);
            //if($invoice)
            //$result .='        '.$invoice->invoice_id;
            $result .= '             </td>
                                    <td>';
            $exphead = ExpenseType::model()->findByPk($entry["exptype"]);
            if ($exphead)
                $result .= '        ' . $exphead->type_name;
            $result .= '             </td>
                                    <td>';
            $vendor = Vendors::model()->findByPk($entry["vendor_id"]);
            if ($vendor)
                $result .= '        ' . $vendor->name;
            $result .= '             </td>
                                    <td>';
            $exptype = Status::model()->findByPk($entry["expense_type"]);
            if ($exptype)
                $result .= '        ' . $exptype->caption;
            $result .= '             </td>
                                    <td>';
            $bank = Bank::model()->findByPk($entry["bank_id"]);
            if ($bank)
                $result .= '        ' . $bank->bank_id;
            $result .= '             </td>';
            $result .= '             <td>' . $entry["cheque_no"] . '</td>
                                    <td>' . $entry["description"] . '</td>
                                    <td class="text-right">' . Controller::money_format_inr($entry["amount"], 2) . '</td>
                                    <td class="text-right">' . Controller::money_format_inr($entry["paid"], 2) . '</td>
                                    <td>';
            $exptype = Status::model()->findByPk($entry["payment_type"]);
            if ($exptype)
                $result .= '        ' . $exptype->caption;
            $result .= '             </td>
                                    <td class="text-right">' . Controller::money_format_inr($entry["receipt"], 2) . '</td>
                                    <td>';
            if ($entry["purchase_type"] == 0)
                $result .= "Credit";
            else if ($entry["purchase_type"] == 1)
                $result .= "Full Paid";
            else if ($entry["purchase_type"] == 2)
                $result .= "Partially Paid";
            $result .= '             </td>
                                </tr>';
            $i++;
        }
        $result     .= '     </tbody>
                       </table></div>';
        echo $result;
    }
    public function actionEditrequest()
    {
        $model = new Editrequest;
        $this->render('editrequests', array(
            'model' => $model,
            'dataProvider' => $model->pendingsearch(),
        ));
    }
    public function actionUpdaterequestaction()
    {
        $id         = $_REQUEST["id"];
        $status     = $_REQUEST["status"];
        $tblpx      = Yii::app()->db->tablePrefix;
        $request    = Editrequest::model()->findByPk($id);
        if ($status == 1) {
            $parentTable    = $request->editrequest_table;
            $parentId       = $request->parent_id;
            if ($parentTable == "{$tblpx}expenses") {
                $model              = new Expenses;
                $data               = json_decode($request->editrequest_data);
                foreach ($data as $key => $value) {
                    $model->$key = $value;
                }
                //$model->attributes  = json_decode($request->editrequest_data);
                if ($data->expense_type == 88 || $data->payment_type == 88) {
                    $reconmodel = new Reconciliation;
                    $reconmodel->reconciliation_table       = $request->editrequest_table;
                    $reconmodel->reconciliation_payment     = $request->editrequest_payment;
                    $reconmodel->reconciliation_paymentdate = $data->date;
                    $reconmodel->reconciliation_amount      = $data->paid;
                    $reconmodel->reconciliation_chequeno    = $data->cheque_no;
                    $reconmodel->reconciliation_bank        = $data->bank_id;
                    $reconmodel->created_date               = $data->created_date;
                    $reconmodel->reconciliation_status      = 0;
                    $reconmodel->company_id                 = $data->company_id;
                }
                // expense alert mail
                $allocated_budget   = array();
                $project_model      = Projects::model()->findByPk($model->projectid);
                $company_model      = Company::model()->findByPk($model->company_id);
                $project_id         = $model->projectid;
                $expense_amount     = Yii::app()->db->createCommand("SELECT SUM(amount) as amount FROM " . $tblpx . "expenses WHERE projectid = " . $project_id . " AND subcontractor_id IS NULL AND exp_id !=" . $parentId . "")->queryRow();
                $sub_amount         = Yii::app()->db->createCommand("SELECT SUM( IFNULL(amount, 0) + IFNULL(sgst_amount, 0) + IFNULL(cgst_amount, 0) + IFNULL(igst_amount, 0)) as amount FROM " . $tblpx . "subcontractor_payment WHERE project_id = " . $project_id . " AND approve_status ='Yes'")->queryRow();
                $vendor_amount      = Yii::app()->db->createCommand("SELECT SUM( IFNULL(amount, 0) + IFNULL(sgst_amount, 0) + IFNULL(cgst_amount, 0) + IFNULL(igst_amount, 0)) as amount FROM " . $tblpx . "dailyvendors WHERE project_id = " . $project_id . " AND project_id IS NOT NULL")->queryRow();

                if ($project_model->profit_margin != NULL && $project_model->project_quote != NULL && $company_model->expenses_percentage != NULL && $company_model->expenses_email != NULL) {
                    $project_expense        = (100 - ($project_model->profit_margin)) * (($project_model->project_quote) / 100);
                    $amount                 = $model->amount + $expense_amount['amount'] + $sub_amount['amount'] + $vendor_amount['amount'];
                    $expenses_percentage    = explode(",", $company_model->expenses_percentage);
                    $percentage_array       = array();
                    foreach ($expenses_percentage as $key => $percentage) {
                        $final_amount           = ($project_expense * $percentage / 100);
                        if ($amount >= $final_amount) {
                            $percentage_array[] = $percentage;
                        }
                    }
                    if (!empty($percentage_array)) {
                        $percentage     = max($percentage_array);
                        if ($project_model->expense_percentage != $percentage) {
                            $allocated_budget['payment_alert'] = array('percentage' => $percentage, 'amount' => $amount, 'profit_margin' => $project_model->profit_margin, 'project_quote' => $project_model->project_quote, 'project_id' => $_POST['Expenses']['projectid']);
                            $this->savePaymentAlert($allocated_budget['payment_alert']);
                        }
                        $expense_alert = $this->saveExpenseNotifications($project_model, $percentage_array, $_POST['Expenses']['amount']);
                        if (!empty($expense_alert)) {
                            $allocated_budget['expense_advance_alert'] = $expense_alert;
                        }
                    } else {
                        $budget_percentage = NULL;
                        $update2    = Yii::app()->db->createCommand()->update($tblpx . 'projects', array('expense_percentage' => NULL, 'expense_amount' => $amount), 'pid=:pid', array(':pid' => $project_id));
                    }
                    $update2        = Yii::app()->db->createCommand()->update($tblpx . 'projects', array('expense_amount' => $amount), 'pid=:pid', array(':pid' => $project_id));
                }

                if ($model->save()) {
                    if (!empty($allocated_budget)) {
                        $this->projectexpensealert($allocated_budget, $model->exp_id, 'daybook');
                    }
                    $newmodel                   = new JpLog('search');
                    $newmodel->log_data         = json_encode($model->attributes);
                    $newmodel->log_action       = 1;
                    $newmodel->log_table        = $tblpx . "expenses";
                    $newmodel->log_primary_key  = $model->exp_id;
                    $newmodel->log_datetime     = date('Y-m-d H:i:s');
                    $newmodel->log_action_by    = Yii::app()->user->id;
                    $newmodel->company_id       = $model->company_id;
                    $newmodel->save();
                    Expenses::model()->deleteAll(array("condition" => "exp_id=" . $parentId . ""));
                    if ($data->expense_type == 88 || $data->payment_type == 88) {
                        $reconmodel->save();
                        Reconciliation::model()->deleteAll(array("condition" => "reconciliation_parentid=" . $parentId . " AND reconciliation_table = '" . $parentTable . "'"));
                    }
                    $request->editrequest_status = 1;
                    if ($request->save()) {
                        $newmodel = new Editrequest;
                        return $this->widget('zii.widgets.CListView', array(
                            'dataProvider' => $newmodel->pendingsearch(),
                            'itemView' => '_neweditrequest', 'template' => '<div class=""><table cellpadding="10" class="table" id="clientab">{items}</table></div>',
                            'ajaxUpdate' => false,
                        ));
                    }
                } else {
                    echo 2;
                }
            } else if ($parentTable == "{$tblpx}dailyvendors") {
                $model              = new Dailyvendors;
                $data               = json_decode($request->editrequest_data);
                foreach ($data as $key => $value) {
                    $model->$key = $value;
                }
                if ($data->payment_type == 88) {
                    $reconmodel                             = new Reconciliation;
                    $reconmodel->reconciliation_table       = $request->editrequest_table;
                    $reconmodel->reconciliation_payment     = $request->editrequest_payment;
                    $reconmodel->reconciliation_paymentdate = $data->date;
                    $reconmodel->reconciliation_amount      = $data->amount + $data->tax_amount;
                    $reconmodel->reconciliation_chequeno    = $data->cheque_no;
                    $reconmodel->reconciliation_bank        = $data->bank;
                    $reconmodel->created_date               = $data->created_date;
                    $reconmodel->reconciliation_status      = 0;
                    $reconmodel->company_id                 = $data->company_id;
                }
                // expense alert mail
                $allocated_budget = array();
                if (isset($model->project_id) && $model->project_id != NULL) {
                    $project_id         = $model->project_id;
                    $project_model      = Projects::model()->findByPk($model->project_id);
                    $company_model      = Company::model()->findByPk($model->company_id);
                    $expense_amount     = Yii::app()->db->createCommand("SELECT SUM(amount) as amount FROM " . $tblpx . "expenses WHERE projectid = " . $project_id . " AND subcontractor_id IS NULL")->queryRow();
                    $sub_amount         = Yii::app()->db->createCommand("SELECT SUM( IFNULL(amount, 0) + IFNULL(sgst_amount, 0) + IFNULL(cgst_amount, 0) + IFNULL(igst_amount, 0)) as amount FROM " . $tblpx . "subcontractor_payment WHERE project_id = " . $project_id . " AND approve_status ='Yes'")->queryRow();
                    $vendor_amount      = Yii::app()->db->createCommand("SELECT SUM( IFNULL(amount, 0) + IFNULL(sgst_amount, 0) + IFNULL(cgst_amount, 0) + IFNULL(igst_amount, 0)) as amount FROM " . $tblpx . "dailyvendors WHERE project_id = " . $project_id . " AND project_id IS NOT NULL AND daily_v_id !=" . $parentId . "")->queryRow();

                    $project_id         = $model->project_id;



                    if ($project_model->profit_margin != NULL && $project_model->project_quote != NULL && $company_model->expenses_percentage != NULL && $company_model->expenses_email != NULL) {
                        $project_expense        = (100 - ($project_model->profit_margin)) * (($project_model->project_quote) / 100);
                        $amount                 = $model->amount + $model->tax_amount + $expense_amount['amount'] + $sub_amount['amount'] + $vendor_amount['amount'];
                        $expenses_percentage    = explode(",", $company_model->expenses_percentage);
                        $percentage_array       = array();
                        foreach ($expenses_percentage as $key => $percentage) {
                            $final_amount = ($project_expense * $percentage / 100);
                            if ($amount >= $final_amount) {
                                $percentage_array[] = $percentage;
                            }
                        }
                        if (!empty($percentage_array)) {
                            $percentage = max($percentage_array);
                            if ($project_model->expense_percentage != $percentage) {
                                $allocated_budget['payment_alert'] = array('percentage' => $percentage, 'amount' => $amount, 'profit_margin' => $project_model->profit_margin, 'project_quote' => $project_model->project_quote, 'project_id' => $_POST['Expenses']['projectid']);
                                $this->savePaymentAlert($allocated_budget['payment_alert']);
                            }
                            $expense_alert = $this->saveExpenseNotifications($project_model, $percentage_array, $_POST['Expenses']['amount']);
                            if (!empty($expense_alert)) {
                                $allocated_budget['expense_advance_alert'] = $expense_alert;
                            }
                        } else {
                            $budget_percentage  = NULL;
                            $update2            = Yii::app()->db->createCommand()->update($tblpx . 'projects', array('expense_percentage' => NULL, 'expense_amount' => $amount), 'pid=:pid', array(':pid' => $project_id));
                        }
                        $update2                = Yii::app()->db->createCommand()->update($tblpx . 'projects', array('expense_amount' => $amount), 'pid=:pid', array(':pid' => $project_id));
                    }
                }

                if ($model->save()) {
                    if (!empty($allocated_budget)) {
                        $this->projectexpensealert($allocated_budget, $model->exp_id, 'daybook');
                    }
                    $newmodel                   = new JpLog('search');
                    $newmodel->log_data         = json_encode($model->attributes);
                    $newmodel->log_action       = 1;
                    $newmodel->log_table        = $tblpx . "dailyvendors";
                    $newmodel->log_primary_key  = $model->daily_v_id;
                    $newmodel->log_datetime     = date('Y-m-d H:i:s');
                    $newmodel->log_action_by    = Yii::app()->user->id;
                    $newmodel->company_id       = $model->company_id;
                    $newmodel->save();
                    Dailyvendors::model()->deleteAll(array("condition" => "daily_v_id=" . $parentId . ""));
                    if ($data->payment_type == 88) {
                        $reconmodel->save();
                        Reconciliation::model()->deleteAll(array("condition" => "reconciliation_parentid=" . $parentId . " AND reconciliation_table = '" . $parentTable . "'"));
                    }
                    $request->editrequest_status = 1;
                    if ($request->save()) {
                        $newmodel = new Editrequest;
                        return $this->widget('zii.widgets.CListView', array(
                            'dataProvider' => $newmodel->pendingsearch(),
                            'itemView' => '_neweditrequest', 'template' => '<div class=""><table cellpadding="10" class="table" id="clientab">{items}</table></div>',
                            'ajaxUpdate' => false,
                        ));
                    }
                } else {
                    echo 2;
                }
            }
        } else {
            $request->editrequest_status = 2;
            if ($request->save()) {
                $newmodel = new Editrequest;
                return $this->widget('zii.widgets.CListView', array(
                    'dataProvider' => $newmodel->pendingsearch(),
                    'itemView' => '_neweditrequest', 'template' => '<div class=""><table cellpadding="10" class="table" id="clientab">{items}</table></div>',
                    'ajaxUpdate' => false,
                ));
            }
        }
    }
    public function actionGetAllData()
    {
        $date       = $_REQUEST["date"];
        $tblpx      = Yii::app()->db->tablePrefix;
        $user       = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal     = explode(',', $user->company_id);
        $newQuery   = "";
        foreach ($arrVal as $arr) {
            if ($newQuery) $newQuery .= ' OR';
            $newQuery .= " FIND_IN_SET('" . $arr . "', e.company_id)";
        }

        $expDate        = date('Y-m-d', strtotime($date));
        $expenseData    = Yii::app()->db->createCommand("SELECT e.*, p.name as pname, b.bill_number as billno, et.type_name as typename,v.name vname,ba.bank_name as bankname, i.inv_no as invoice_no FROM " . $tblpx . "expenses e
                                LEFT JOIN " . $tblpx . "projects p ON e.projectid = p.pid
                                LEFT JOIN " . $tblpx . "bills b ON e.bill_id = b.bill_id
                                LEFT JOIN " . $tblpx . "expense_type et ON e.exptype = et.type_id
                                LEFT JOIN " . $tblpx . "vendors v ON e.vendor_id = v.vendor_id
                                LEFT JOIN " . $tblpx . "bank ba ON e.bank_id = ba.bank_id
                                LEFT JOIN " . $tblpx . "invoice i ON e.invoice_id = i.invoice_id
                                WHERE e.date = '" . $expDate . "' AND (" . $newQuery . ")")->queryAll();
        $client         = $this->renderPartial('newlist', array('newmodel' => $expenseData));

        return $client;
    }
    public function getData($data)
    {
        $cash_grandtotal = 0;
        foreach ($data as $key => $datavalue) {
            $cash_grandtotal += $datavalue['paidamount'];
        }
        return $cash_grandtotal;
    }

    public function getBillData($billId, $expTypeId, $model, $paymentType, $amount, $data)
    {
        if ($billId == 0) {
            if ($expTypeId != '') {
                $model->type                        = 73;
                $model->expense_type                = $paymentType;
                $model->bill_id                     = NULL;
                $model->invoice_id                  = NULL;
                $model->payment_type                = NULL;
                $model->paid                        = (isset($_POST['Expenses']['paid']) ? $_POST['Expenses']['paid'] : "");
            } else {
                // receipt
                $model->type        = 72;
                $model->exptype     = NULL;
                $model->vendor_id   = NULL;
                $model->paid        = NULL;
                if ($paymentType == 0) {
                    $model->receipt                     = 0;
                    $model->purchase_type               = 1;
                    $model->payment_type                = NULL;
                } else {
                    $model->receipt                     = $amount;
                    $model->purchase_type               = 2;
                    $model->payment_type                = $paymentType;
                }
                $model->invoice_id                  = NULL;
                $model->bill_id                     = NULL;
                $model->expense_type                = NULL;
            }
        } else {
            if ($billId == 1) {
                $model->type                        = 73;
                $model->expense_type                = $paymentType;
                if ($paymentType == 0) {
                    $model->purchase_type = 1;
                } else {
                    $model->purchase_type = $_POST['Expenses']['purchase_type'];
                }
                $model->bill_id                     = $data[1];
                $model->invoice_id                  = NULL;
                $model->return_id                   = NULL;
                $model->payment_type                = $paymentType;
                $model->paid                        = (isset($_POST['Expenses']['paid']) ? $_POST['Expenses']['paid'] : "");
                $model->exptype                     = (isset($_POST['bill_exphead']) ? $_POST['bill_exphead'] : "");
                $model->vendor_id                   = (isset($_POST['bill_vendor']) ? $_POST['bill_vendor'] : "");
            } else if ($billId == 2) {
                $model->type                        = 72;
                $model->payment_type                = $_POST['Expenses']['expense_type'];
                $model->exptype                     = $_POST['Expenses']['expensetype_id'];
                $model->vendor_id                   = $_POST['Expenses']['vendor_id'];
                $model->paid                        = NULL;
                $model->receipt                     = $_POST['Expenses']['paidamount'];
                $model->invoice_id                  = $data[1];
                $model->bill_id                     = NULL;
                $model->return_id                   = NULL;
                $model->expense_type                = NULL;
            } else {
                $model->type                        = 72;
                if ($paymentType != 0) {
                    $model->payment_type = $paymentType;
                } else {
                    $model->payment_type = NULL;
                    $model->purchase_type = 2;
                }
                $model->exptype                     = (isset($expTypeId) ? $expTypeId : "");
                $model->vendor_id                   = (isset($_POST['Expenses']['vendor_id']) ? $_POST['Expenses']['vendor_id'] : "");
                $model->paid                        = NULL;
                $model->receipt                     = $_POST['Expenses']['paidamount'];
                $model->invoice_id                  = NULL;
                $model->bill_id                     = NULL;
                $model->return_id                   = $data[1];
                $model->expense_type                = NULL;
            }
        }
    }

    public function actiontestChequeNumber(){
        $cheque_no = $_POST['cheque_no'];
        $bank = $_POST['bank'];
        $result = $this->checkDuplicateEntry($cheque_no,$bank);        
        echo $result;        
    }

    public function actioncheckBillDate() {
        $pendingBills = $this->getPendingDeliveries();

        $billArray=array();
        foreach ($pendingBills['pending_deliveries_array'] as $key => $data) {
            foreach ($data as $key => $value) { 
                array_push($billArray,$value['bill_id']) ;
            }
        }
                
        if(in_array($_POST['bill_id'],$billArray)){
            echo json_encode(array(
                'msg' => 'Selected Bill is not receipted',
                'response' => 'error',
                'accepted'=>'',
                'total_accepted'=>'',
                'accepted_without_tax'=>''
            ));
        }else{
           $status_sql = "SELECT * FROM `jp_bills`  b "
                . " LEFT JOIN jp_warehousereceipt wr "
                . " ON b.bill_id=wr.warehousereceipt_bill_id "
                . " LEFT JOIN jp_defect_return d "
                . " ON wr.warehousereceipt_id = d.receipt_id "
                . " WHERE b.bill_id = ".$_POST['bill_id'];                       
            $bill_status= Yii::app()->db->createCommand($status_sql)->queryRow();

             $sql ="SELECT 
                SUM(wri.warehousereceipt_rejected_quantity) AS `rejected_quantity`,
                SUM((wri.warehousereceipt_rate * wri.warehousereceipt_accepted_quantity) - bi.billitem_discountamount) 
                AS accepted_without_tax,
                SUM((wri.warehousereceipt_rate * wri.warehousereceipt_accepted_quantity) - bi.billitem_discountamount + 
                    ((wri.warehousereceipt_rate * wri.warehousereceipt_accepted_quantity * bi.billitem_taxpercent) / 100)
                ) AS `accepted_amount`
            FROM jp_warehousereceipt_items wri 
            LEFT JOIN jp_billitem bi 
                ON bi.billitem_id = wri.warehousereceipt_transfer_type_item_id
            WHERE wri.warehousereceipt_id = " . (int)$bill_status['warehousereceipt_id'];
               
            $receipt_item_data=Yii::app()->db->createCommand($sql)->queryRow();
            

            $sql = "SELECT SUM(di.returnitem_quantity) "
                . " FROM `jp_defect_return` d "
                . " LEFT JOIN jp_defect_returnitem di "
                . " ON d.return_id = di.return_id "
                . " WHERE `d`.`receipt_id` = ".$bill_status['warehousereceipt_id'];
            $defected_qty=Yii::app()->db->createCommand($sql)->queryScalar();

            $approved_sql = "SELECT SUM(di.returnitem_quantity) "
                . " FROM `jp_defect_return` d "
                . " LEFT JOIN jp_defect_returnitem di "
                . " ON d.return_id = di.return_id "
                . " WHERE `d`.`receipt_id` = ".$bill_status['warehousereceipt_id']
                . " AND d.approval_status=1 ";
            $approved_defected_qty = Yii::app()->db->createCommand($approved_sql)->queryScalar();            

            $expsql = "SELECT SUM(paid) FROM `jp_expenses` WHERE bill_id=".$_POST['bill_id'];
            $expamount = Yii::app()->db->createCommand($expsql)->queryScalar();
            
            if($receipt_item_data['rejected_quantity'] > 0 ){
                if ($defected_qty == $receipt_item_data['rejected_quantity']) {
                    if($defected_qty == $approved_defected_qty){
                        $sql = 'SELECT bill_date FROM jp_bills '
                        . ' WHERE bill_id =' . $_POST['bill_id'];
                        $bill_date = Yii::app()->db->createCommand($sql)->queryScalar();
                        $entry_date = $_POST['entry_date'];
                        $billDate = strtotime(date("d-m-Y", strtotime($bill_date)));
                        $entry_date = strtotime($_POST['entry_date']);
            
                        if ($entry_date < $billDate) {
                            echo json_encode(array(
                            'msg' => 'Selected Bill / Invoice valid only from ' . date("d-m-Y", strtotime($bill_date)),
                            'response' => 'error',
                            'accepted'=>'',
                            'total_accepted'=>'',
                            'accepted_without_tax'=>''
                        ));
                        } else {                            
                            echo json_encode(array(
                            'msg' => '',
                            'response' => 'success',
                            'accepted'=> (($receipt_item_data["accepted_amount"]) - $expamount),
                            'total_accepted'=>($receipt_item_data["accepted_amount"]),
                            'accepted_without_tax'=>$receipt_item_data["accepted_without_tax"]
                            ));
                        }
                    } else {
                        echo json_encode(array(
                            'msg' => 'Defect return not approved',
                            'response' => 'error',
                            'accepted'=>'',
                            'total_accepted'=>'',
                            'accepted_without_tax'=>''
                        ));                         
                    }
                } else{
                    echo json_encode(array(
                        'msg' => 'Defect items not returned',
                        'response' => 'error',
                        'accepted'=>'',
                        'total_accepted'=>'',
                        'accepted_without_tax'=>''
                    ));                    
                }
            } else {
                $sql = 'SELECT bill_date FROM jp_bills '
                        . ' WHERE bill_id =' . $_POST['bill_id'];
                        $bill_date = Yii::app()->db->createCommand($sql)->queryScalar();
                        $entry_date = $_POST['entry_date'];
                        $billDate = strtotime(date("d-m-Y", strtotime($bill_date)));
                        $entry_date = strtotime($_POST['entry_date']);
            
                if ($entry_date < $billDate) {
                    echo json_encode(array(
                        'msg' => 'Selected Bill / Invoice valid only from 
                        ' . date("d-m-Y", strtotime($bill_date)),
                        'response' => 'error',
                        'accepted'=>'',
                        'total_accepted'=>'',
                        'accepted_without_tax'=>''
                    ));
                } else {
                    echo json_encode(array(
                        'msg' => '',
                        'response' => 'success',
                        'accepted'=> (($receipt_item_data["accepted_amount"]) - $expamount),
                        'total_accepted'=>($receipt_item_data["accepted_amount"]),
                        'accepted_without_tax'=>$receipt_item_data["accepted_without_tax"]
                    ));
                }
            }                        
        }
    }

    public function actionignoreDuplicateEntry(){
        $id= $_POST['id'];
        $section =  $_POST['section'];
        
        if($section=='Daybook'){
            $model =Expenses::model()->findByPk($id);                        
        }else if($section=='Purchase Bill'){
            $model =Bills::model()->findByPk($id); 
        }else if($section=='Subcontractor Payment'){
            $model =SubcontractorPayment::model()->findByPk($id); 
        }else if($section=='Dailyexpense'){
            $model =Dailyexpense::model()->findByPk($id); 
        }else if($section=='Vendor Payment'){
            $model =Dailyvendors::model()->findByPk($id); 
        }

        $model->duplicate_ignore_status = '1';
        if($model->save()){
            echo json_encode(array("msg"=>"Entry Ignored Suceessfully "));
        } else{
            $error_message =   $this->setErrorMessage($model->getErrors());            
            echo json_encode(array("msg"=>"Can't Ignore !!".$error_message));
        }      

    }

    public function actiondeleteDuplicateEntry(){
        $id= $_POST['id'];
        $section =  $_POST['section'];

        if($section=='Daybook'){
            $model =Expenses::model()->findByPk($id);                        
            $table = "jp_expenses"; 
            $type = $model->expense_type;                    
        }else if($section=='Purchase Bill'){
            $model =Bills::model()->findByPk($id); 
            $table = "jp_bills";
            $type = '';
        }else if($section=='Subcontractor Payment'){
            $model =SubcontractorPayment::model()->findByPk($id); 
            $table = "jp_subcontractor_payment";
            $type = $model->payment_type;
        }else if($section=='Dailyexpense'){
            $model =Dailyexpense::model()->findByPk($id); 
            $table = "jp_dailyexpense";
            $type = $model->expense_type;
        }else if($section=='Vendor Payment'){
            $model =Dailyvendors::model()->findByPk($id); 
            $table = "jp_dailyvendors";
            $type = $model->payment_type;
        }
       
        $user = yii::app()->user->role;
        $data = $model;
        $transaction = Yii::app()->db->beginTransaction();
        try {
            if($user == 1){                
                if($model->delete()){
                    if($type==88){                        
                        Reconciliation::model()->deleteAll(array("condition" => "reconciliation_parentid=" . $id . " AND reconciliation_table = '".$table."'"));
                    }                   
                    $newmodel = new JpLog('search');
                    $newmodel->log_data = json_encode($data->attributes);
                    $newmodel->log_action = 2;
                    $newmodel->log_table = $table;
                    $newmodel->log_datetime = date('Y-m-d H:i:s');
                    $newmodel->log_action_by = Yii::app()->user->id;
                    $newmodel->company_id = $data->company_id;
                    if ($newmodel->save()) {
                        $result = array('msg' => 'Entry Deleted successfully From '. $section);                                           
                    }
                }else{
                    throw new Exception(json_encode("Some Error Occured"));                                   
                }
            }else{
                $model->duplicate_delete_status = '1';
                if(!$model->save()){
                    throw new Exception(json_encode("Some Error Occured")); 
                }
                $deletepending= new Deletepending;
                $deletepending->deletepending_data = json_encode($model->attributes);
                $deletepending->deletepending_table =  $table;;
                $deletepending->deletepending_parentid = $id;
                $deletepending->user_id = Yii::app()->user->id;
                $deletepending->requested_date = date('Y-m-d H:i:s');           
                if($deletepending->save()){                   
                    $result = array('msg' => 'Delete Request Sent !');  
                }else{                    
                    $result = array('msg' => 'Error Occured !');
                }
            }
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollBack();
        } finally {            
            echo json_encode($result);
        } 
    }

    public function actiongenerateVoucher($id){
        if (filter_input(INPUT_GET, 'export') == 'pdf') {
            $this->logo = $this->realpath_logo;
            $mPDF1 = Yii::app()->ePdf->mPDF();
            $mPDF1 = Yii::app()->ePdf->mPDF('', 'A4');
            $sql = 'SELECT template_name '
                        . 'FROM jp_quotation_template '
                        . 'WHERE status="1"';
            $selectedtemplate =Yii::app()->db->createCommand($sql)->queryScalar();
                
            $template = $this->getTemplate($selectedtemplate);
            // $mPDF1->SetHTMLHeader($template['header']);
            $mPDF1->SetHTMLFooter($template['footer']);
                
            $sql = 'SELECT template_name '
                        . 'FROM jp_quotation_template '
                        . 'WHERE status="1"';
			$selectedtemplate =Yii::app()->db->createCommand($sql)->queryScalar();
			if ($selectedtemplate == 'template1'){
				$mPDF1->AddPage('','', '', '', '', 0,0,10, 30, 10,0);
			}else{                    
				$mPDF1->AddPage('','', '', '', '', 0,0,10, 30, 10,0);
			}
            $mPDF1->WriteHTML($this->renderPartial('generateVoucher', array('id'=>$id), true));            
            $mPDF1->Output('DAYBOOK VOUCHER.pdf', 'D');
        }
        $this->render('generateVoucher',array('id'=>$id));        
    }

    public function actionsendattachment()
    {
        $p_id = $_POST['p_id'];
        $mail_to = $_POST['mail_to'];
        $mail_content = $_POST['mail_content'];
        $path = $_POST['pdf_path'];
        $name = $_POST['pdf_name'];
        $mail = new JPhpMailer();
        $subject = "" . Yii::app()->name . ":DAYBOOK VOUCHER";
        $headers = "" . Yii::app()->name . "";
        $bodyContent = "<p>Hi</p><p>" . $mail_content . "</p><p>Please find the attachment</p>";
        $server = (($_SERVER['HTTP_HOST'] == 'localhost' || $_SERVER['HTTP_HOST'] == 'bhi.localhost.com') ? '0' : '1');
        $newdate = date('Y-m-d');
        $mail->setFrom(EMAILFROM, Yii::app()->name);
        $allfiles = glob(Yii::getPathOfAlias('webroot') . '/documents/*');
        $mailSendData = array();
        $mailSendData['email_from_name'] = Yii::app()->name;
        $mailSendData['mail_to'] = $mail_to;
        $mailSendData['subject'] = Yii::app()->name . " : RECEIPT VOUCHER";
        $mailSendData['send_copy_status'] = $_POST['send_copy'];
        if (file_exists($path)) {
            $mailSendData['attachment'][] = array($path, $name);
        }
        $mailSendData['message'] = $bodyContent;
        $mail_send_status = $this->sendSMTPMail($mailSendData);
        if ($mail_send_status['status'] == 1) {
            foreach ($allfiles as $file) {
                if (is_file($file))
                    unlink($file);
            }
        }
        echo json_encode($mail_send_status);
    }

    public function actionpdfgeneration()
    {
        $id = $_POST['p_id'];
        
        $mPDF1 = Yii::app()->ePdf->mPDF();
        $mPDF1 = Yii::app()->ePdf->mPDF('', 'A4');
        $mPDF1->WriteHTML($this->renderPartial('generateVoucher',array('id'=>$id,'export'=>'mail'), true));
        $newdate = date('Y-m-d');
        $path = Yii::getPathOfAlias('webroot') . '/documents/Expenses_' . $newdate . '.pdf';
        $mPDF1->Output($path, 'F');
        echo json_encode(array('path' => $path, 'name' => 'Expenses_' . $newdate . '.pdf'));
    }

    public function actionpettycashpayment(){
        $model = new Expenses;
        $dailymodel      = new Dailyexpense;
        $this->render('pettycashpayment',array('model'=>$model,'dailymodel'=>$dailymodel));
    }
}
