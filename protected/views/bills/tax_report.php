<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/tableHeadFixer.js"></script>

<div class="container" id="project">
  <?php
  $tblpx = Yii::app()->db->tablePrefix;

  $user = Users::model()->findByPk(Yii::app()->user->id);
  $arrVal = explode(',', $user->company_id);
  $query_company = "";
  foreach ($arrVal as $arr) {
    if ($query_company)
      $query_company .= ' OR';
    $query_company .= " FIND_IN_SET('" . $arr . "', id)";
  }

  $sales_totalamount = 0;
  foreach ($dataProvider->rawData as $key => $value) {
    $sales_totalamount += $value['totalamount'];
  }

  $purchase_totalamount = 0;
  foreach ($dataProvider1->rawData as $key => $data) {
    if ($data['type'] == 'bills') {
      $item_amount = Yii::app()->db->createCommand("SELECT sum(billitem_amount) as billitem_amount FROM {$tblpx}billitem WHERE bill_id=" . $data['id'] . " AND ((billitem_cgstpercent > 0 AND billitem_sgstpercent > 0) OR (billitem_igstpercent > 0))")->queryRow();
      $item_discount = Yii::app()->db->createCommand("SELECT sum(billitem_discountamount) as billitem_discountamount FROM {$tblpx}billitem WHERE bill_id=" . $data['id'] . " AND ((billitem_cgstpercent > 0 AND billitem_sgstpercent > 0) OR (billitem_igstpercent > 0))")->queryRow();
      $item_taxamount = Yii::app()->db->createCommand("SELECT sum(billitem_taxamount) as billitem_taxamount FROM {$tblpx}billitem WHERE bill_id=" . $data['id'] . " AND ((billitem_cgstpercent > 0 AND billitem_sgstpercent > 0) OR (billitem_igstpercent > 0))")->queryRow();
      //Controller::money_format_inr($data['bill_totalamount'],2,1)
      $total_amount = ($item_amount['billitem_amount'] - $item_discount['billitem_discountamount']) + $item_taxamount['billitem_taxamount'];
    } else if ($data['type'] == 'daybook') {
      $total_amount = $data['totalamount'];
    } else if ($data['type'] == 'dailyexpense') {
      $total_amount = $data['totalamount'];
    } else if ($data['type'] == 'vendorpayment') {
      $item_taxamount = Yii::app()->db->createCommand("SELECT tax_amount FROM {$tblpx}dailyvendors WHERE 	daily_v_id=" . $data['id'] . "")->queryRow();
      $total_amount = $data['totalamount'] + $item_taxamount['tax_amount'];
    }
    $purchase_totalamount += $total_amount;
  }

  if ($company_id != NULL) {
    $company = Company::model()->findbyPk($company_id);
    $company_name = $company->name;
  } else {
    $company_name = "";
  }
  ?>

  <div class="expenses-heading header-container">
    <h3>Tax Report</h3>
    <div class="btn-container">
      <a type="button" class="btn btn-info" style="cursor:pointer;" id="download"
        href="<?php echo Yii::app()->createUrl('bills/taxreportcsv', array('date_from' => $date_from, 'date_to' => $date_to, 'company_id' => $company_id)); ?>">
        <i class="fa fa-file-excel-o button-icon" aria-hidden="true"></i>
      </a>
      <!-- PDF Button -->
      <a type="button" class="btn btn-info" style="cursor:pointer;" id="download"
        href="<?php echo Yii::app()->createUrl('bills/taxreportpdf', array('date_from' => $date_from, 'date_to' => $date_to, 'company_id' => $company_id)) ?>">
        <i class="fa fa-file-pdf-o button-icon" aria-hidden="true"></i>
      </a>
      <!-- Excel Button -->
    </div>
  </div>
  <div id="contenthtml" class="">

    <?php
    $from = date("Y-m-") . "01";
    ?>
    <div class="page_filter clearfix custom-form-style">
      <form id="projectform" action="<?php $url = Yii::app()->createAbsoluteUrl("bills/Taxreport"); ?>" method="POST">
        <div class="search-form">
          <div class="row">
            <div class="form-group col-xs-12 col-sm-3 col-md-2">
              <?php echo CHtml::label('From ', ''); ?>
              <?php echo CHtml::textField('date_from', (isset($date_from) ? date('Y-m-d', strtotime($date_from)) : $from), array("id" => "date_from", 'class' => 'form-control')); ?>
            </div>
            <div class="form-group col-xs-12 col-sm-3 col-md-2">
              <?php echo CHtml::label('To ', ''); ?>
              <?php echo CHtml::textField('date_to', (isset($date_to) ? date('Y-m-d', strtotime($date_to)) : date('Y-m-d')), array("id" => "date_to", 'class' => 'form-control')); ?>
            </div>
            <div class="form-group col-xs-12 col-sm-3 col-md-2">
              <label for="company">Company</label>
              <?php
              echo CHtml::dropDownList('company_id', 'company_id', CHtml::listData(Company::model()->findAll(array(
                'select' => array('id, name'),
                'order' => 'id DESC',
                'condition' => '(' . $query_company . ')',
                'distinct' => true
              )), 'id', 'name'), array('empty' => 'Select Company', 'id' => 'company_id', 'class' => 'form-control', 'options' => array($company_id => array('selected' => true))));
              ?>
            </div>
            <div class="form-group col-xs-12 col-sm-3 col-md-2 text-sm-left text-right">
              <label class="d-sm-block d-none">&nbsp;</label>
              <div>
                <?php echo CHtml::submitButton('GO', array('id' => 'btSubmit', 'class' => 'btn btn-sm btn-primary')); ?>
                <?php echo CHtml::resetButton('Clear', array('onclick' => 'javascript:location.href="' . $this->createUrl('bills/taxreport') . '"', 'class' => ' btn btn-sm btn-default', )); ?>
              </div>
            </div>
          </div>
        </div>
      </form>
    </div>
    <h4 class='head_secon margin-bottom-0'>Purchase &nbsp;
      <?php echo (($company_name != '')) ? ' - ' . $company_name : ''; ?>
    </h4>
    <?php
    $this->widget('zii.widgets.CListView', array(
      'dataProvider' => $dataProvider1,
      'viewData' => array('purchase_head' => $purchase_head, 'purchase_head_igst' => $purchase_head_igst, 'date_from' => $date_from, 'date_to' => $date_to, 'purchase_totalamount' => $purchase_totalamount, 'company_id' => $company_id),
      'itemView' => '_billsnewview',
      'template' => '<div>{summary}{sorter}</div><div id="parent" class="table-responsive"><table class="table table-bordered list-view sorter" id="fixTable">{items}</table></div>',
      'emptyText' => '<table cellpadding="10" class="table"><thead class="entry-table">
        <tr>
        <th>Sl No.</th>
        <th>Payment mode</th>
        <th>Bill date</th>
        <th>Vendor</th>
        <th>GST No</th>
        <th>Commodity</th>
        <th>Commodity Code</th>
        <th>Bill no</th>
        <th>Bill amount</th>
        <th>Other changers</th>
        <th>Net amount</th>
        </tr></thead><tr><td colspan="11" class="text-center">NO results found</td></tr></table>',
    ));
    ?>
    <h4 class='head_secon margin-bottom-0'>Sales&nbsp;
      <?php echo (($company_name != '')) ? ' - ' . $company_name : ''; ?>
    </h4>
    </h3>
    <?php
    $this->widget('zii.widgets.CListView', array(
      'dataProvider' => $dataProvider,
      'viewData' => array('sales_head' => $sales_head, 'igst_head_sales' => $igst_head_sales, 'sales_totalamount' => $sales_totalamount, 'date_from' => $date_from, 'date_to' => $date_to, 'company_id' => $company_id),
      'itemView' => '_salesnewview',
      'template' => '<div>{summary}{sorter}</div><div id="parent2" class="table-responsive"><table class="table table-bordered list-view sorter" id="fixTable2">{items}</table></div>',
      'emptyText' => '<table cellpadding="10" class="table"><thead class="entry-table">
        <th>Sl No.</th>
        <th>Invoice Date</th>
        <th>Client</th>
        <th>Invoice No</th>
        <th>GST No</th>
        <th>Amount</th>
        <th>Net Amount</th></thead><tr><td colspan="7" class="text-center">NO results found</td></tr></table>',
    ));
    ?>
  </div>
</div>
<script>
  $(document).ready(function () {
    $("#fixTable").tableHeadFixer({
      'left': false,
      'foot': true,
      'head': true
    });
    $("#fixTable2").tableHeadFixer({
      'left': false,
      'foot': true,
      'head': true
    });

    $("#date_from").datepicker({
      dateFormat: 'yy-mm-dd',
      maxDate: $("#date_to").val()
    });
    $("#date_to").datepicker({
      dateFormat: 'yy-mm-dd',
    });
    $("#date_from").change(function () {
      $("#date_to").datepicker('option', 'minDate', $(this).val());
    });
    $("#date_to").change(function () {
      $("#date_from").datepicker('option', 'maxDate', $(this).val());
    });
  });
  $(document).delegate('.bill_view', 'click', function (e) {
    var bill_id = $(this).attr('id');
    window.open("<?php echo Yii::app()->createUrl('bills/view&id=') ?>" + bill_id, '_blank');
  });
</script>

<style>
  #parent,
  #parent2 {
    max-height: 400px;
  }

  #parent table th,
  #parent2 table th {
    background-color: #eee;
  }

  .list-view .sorter {
    margin-bottom: 0px;
  }
</style>