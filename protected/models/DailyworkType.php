<?php

/**
 * This is the model class for table "jp_daily_work_type".
 *
 * The followings are the available columns in table 'jp_daily_work_type':
 * @property integer $wtid
 * @property string $work_type
 * @property integer $company_id
 * @property string $labour_label
 * @property string $wage_label
 * @property string $wage_rate_label
 * @property string $helper_label
 * @property string $helper_labour_label
 */
class DailyworkType extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'jp_daily_work_type';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('company_id', 'numerical', 'integerOnly'=>true),
			array('work_type', 'length', 'max'=>30),
            array('labour_label, wage_label, wage_rate_label, helper_label, helper_labour_label', 'length', 'max'=>200),
            array('work_type', 'required'),
            array('helper_label, helper_labour_label', 'my_equired'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('wtid, work_type, company_id, labour_label, wage_label, wage_rate_label, helper_label, helper_labour_label,amount_label,labour_status,wage_status,wagerate_status,helper_status,helperlabour_status,amount_status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'wtid' => 'Wtid',
			'work_type' => 'Work Type',
			'company_id' => 'Company',
			'labour_label' => 'No Of Labour',
			'wage_label' => 'Corresponding Wage',
			'wage_rate_label' => 'Wage rate',
			'helper_label' => 'Helper',
            'helper_labour_label' => 'Helper Labour',
            'amount_label'=>'lump-sum'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('wtid',$this->wtid);
		$criteria->compare('work_type',$this->work_type,true);
		$criteria->compare('company_id',$this->company_id);
		$criteria->compare('labour_label',$this->labour_label,true);
		$criteria->compare('wage_label',$this->wage_label,true);
		$criteria->compare('wage_rate_label',$this->wage_rate_label,true);
		$criteria->compare('helper_label',$this->helper_label,true);
		$criteria->compare('helper_labour_label',$this->helper_labour_label,true);

        $criteria->compare('amount_label',$this->amount_label,true);
        $criteria->compare('labour_status',$this->labour_status,true);
        $criteria->compare('wage_status',$this->wage_status,true);
        $criteria->compare('wagerate_status',$this->wagerate_status,true);
        $criteria->compare('helper_status',$this->helper_status,true);
        $criteria->compare('helperlabour_status',$this->helperlabour_status,true);
        $criteria->compare('amount_status',$this->amount_status,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
    }
    
    public function my_equired($attribute_name, $params){
        
        if(($this->helper_label =='' ) && ($this->helper_labour_label !='') ) {
            $this->addError("helper_label", ("Please enter Helper"));
        }
        if(($this->helper_labour_label == '' ) && ($this->helper_label !='') ) {
            $this->addError("helper_labour_label", ("Please enter Helper labour"));
        }
    }

    

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return DailyWorkType the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
