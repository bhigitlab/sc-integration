<?php
/* @var $this WarehousedespatchController */
/* @var $model Warehousedespatch */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'warehousedespatch-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'warehousedespatch_no'); ?>
		<?php echo $form->textField($model,'warehousedespatch_no',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'warehousedespatch_no'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'warehousedespatch_date'); ?>
		<?php echo $form->textField($model,'warehousedespatch_date'); ?>
		<?php echo $form->error($model,'warehousedespatch_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'warehousedespatch_vendor'); ?>
		<?php echo $form->textField($model,'warehousedespatch_vendor',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'warehousedespatch_vendor'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'warehousedespatch_warehouseid'); ?>
		<?php echo $form->textField($model,'warehousedespatch_warehouseid'); ?>
		<?php echo $form->error($model,'warehousedespatch_warehouseid'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'warehousedespatch_clerk'); ?>
		<?php echo $form->textField($model,'warehousedespatch_clerk'); ?>
		<?php echo $form->error($model,'warehousedespatch_clerk'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'warehousedespatch_quantity'); ?>
		<?php echo $form->textField($model,'warehousedespatch_quantity'); ?>
		<?php echo $form->error($model,'warehousedespatch_quantity'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'warehousedespatch_through'); ?>
		<?php echo $form->textField($model,'warehousedespatch_through',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'warehousedespatch_through'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'company_id'); ?>
		<?php echo $form->textField($model,'company_id'); ?>
		<?php echo $form->error($model,'company_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'created_by'); ?>
		<?php echo $form->textField($model,'created_by'); ?>
		<?php echo $form->error($model,'created_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'created_date'); ?>
		<?php echo $form->textField($model,'created_date'); ?>
		<?php echo $form->error($model,'created_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'updated_by'); ?>
		<?php echo $form->textField($model,'updated_by'); ?>
		<?php echo $form->error($model,'updated_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'updated_date'); ?>
		<?php echo $form->textField($model,'updated_date'); ?>
		<?php echo $form->error($model,'updated_date'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->