<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/d2rtablestyles.css" />
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/d2rpdfstyle.css">


<!-- page 2 -->
<div class="">
    <div class="red_border p_2 detail_sec border-bottom-none">

        <table class="detail_table">
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td class="bg_light_grey">&nbsp;</td>
                <td class="bg_light_grey">&nbsp;</td>
                <td class="bg_light_grey">&nbsp;</td>
            </tr>
            <tr>
                <td width="19%">
                    <label for="client">CLIENT NAME</label>
                </td>
                <td width="1%">:</td>
                <td width="30%">
                <?php echo $client; ?>
                </td>
                <td class="bg_light_grey" colspan="3" width="50%">
                    <label for="date">DATE:
                        <?php echo isset($model->date_quotation) ? date('d-m-Y', strtotime($model->date_quotation)) : ''; ?></label>
                    <label for="ref"> REF NO:
                        <?php echo isset($model->invoice_no) ? $model->invoice_no : ''; ?></label>
                </td>
            </tr>
            <tr>
                <td class="v_topalign"  width="19%"><label for="siteaddress">SITE ADDRESS</label></td>
                <td width="1%">:</td>
                <td class="v_topalign" width="30%"><?php echo $model->address ?></td>               
                <?php
                    $sales_ex ="";
                    if($model->sales_executive_id !=""){
                        $salesExecutive = Salesexecutive::model()->findByPk($model->sales_executive_id);
                        $sales_ex = $salesExecutive['name'];
                    }                     
                ?>
                <td class="bg_light_grey" width="19%"><label for="salesex">SALES EXECUTIVE</label></td>
                <td class="bg_light_grey" width="1%">:</td>
                <td class="bg_light_grey" width="30%"><?php echo $sales_ex ?></td>
            </tr>
            <tr>
                <td><label for="sitemobile"  width="19%" >MOBILE</label></td>
                <td width="1%">:</td>
                <td width="30%"><?php echo $model->phone_no ?></td>
                <?php
                    $phone ="";
                    if($model->sales_executive_id !=""){
                        $salesExecutive = Salesexecutive::model()->findByPk($model->sales_executive_id);
                        $phone = $salesExecutive['phone'];
                    }
                     
                ?>
                <td class="bg_light_grey"  width="19%"><label for="salesexphone">MOBILE</label></td>
                <td class="bg_light_grey" width="1%">:</td>
                <td class="bg_light_grey" width="30%"><?php echo $phone ?></td>
            </tr>
            <tr>
                <td width="19%"><label for="siteemail">EMAIL</label></td>
                <td width="1%">:</td>
                <td width="30%" style="word-wrap: break-word;"><?php echo $model->email ?></td>
                <?php
                    $designation ="";
                    if($model->sales_executive_id !=""){
                        $salesExecutive = Salesexecutive::model()->findByPk($model->sales_executive_id);
                        $designation = $salesExecutive['designation'];
                    }
                     
                ?>
                <td class="bg_light_grey" width="19%"><label for="salesexdesc">Designation</label></td>
                <td class="bg_light_grey" width="1%">:</td>
                <td class="bg_light_grey" width="30%"><?php echo $designation ?></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td class="bg_light_grey">&nbsp;</td>
                <td class="bg_light_grey">&nbsp;</td>
                <td class="bg_light_grey">&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td class="bg_light_grey">&nbsp;</td>
                <td class="bg_light_grey">&nbsp;</td>
                <td class="bg_light_grey">&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>                
                <td>&nbsp;</td>
                <td class="bg_light_grey">&nbsp;</td>
                <td class="bg_light_grey">&nbsp;</td>
                <td class="bg_light_grey">&nbsp;</td>
            </tr>
        </table>
    </div>
    <img src="<?php echo realpath(Yii::app()->basePath . '/../uploads/media/d2r_images/image2.png')?>" alt="">
    <div class="text_brown text-center p-5">
        <h1 class="light_font">MEASURING DREAMS FOR</h1>
        <h1 class="bold_font">BUILDING FUTURE</h1>
    </div>
</div>

<!-- page 3 -->
<div class="dot_bg">
    <div class="table_h">
        <table cellpadding="10" class="item_table">
            <thead>
                <tr>
                    <th colpsna="7"></th>
                </tr>
            </thead>
            <tbody>
                <tr class="thead">

                    <th>S.NO</th>
                    <th>DESCRIPTION OF ITEM</th>
                    <th>UNIT</th>
                    <th>QTY</th>                                    
                    <th>MRP</th>
                    <th>AFTER DISCOUNT</th>
                    

                </tr>
                <?php
                $sql = 'SELECT * FROM `jp_quotation_section` '
                        . 'WHERE `qtn_id` = ' . $qtid
                        . ' AND revision_no = "' . $rev_no . '"';
                $sectionArray = Yii::app()->db->createCommand($sql)->queryAll();
                $i = 1;
                foreach ($sectionArray as $key => $value) {
                    $sql = 'SELECT * FROM `jp_quotation_gen_category` '
                            . 'WHERE `qid` = ' . $qtid . ' AND `section_id` = ' . $value["id"]
                            . ' AND revision_no = "' . $rev_no . '"';
                    $subsectionArray = Yii::app()->db->createCommand($sql)->queryAll();
                    $unit = Unit::model()->findByPk($value['unit']);
                    $secsum = Controller::getSectionSum($value['id'], $qtid, $rev_no);
                    if($secsum > 0){
                    ?>
                    <tr>
                        <th colspan="<?php echo empty($subsectionArray) ? '2' : '6' ?>" style="background:#c00000;color:#fff;"><?php echo $value['section_name'] ?></th>
                        <?php if (empty($subsectionArray)) { ?>
                            <td style="background:#c00000;color:#fff;"><?php echo isset($unit) ? $unit->unit_name : "" ?></td>
                            <td style="background:#c00000;color:#fff;"><?php echo $value['quantity_nos'] ?></td>
                            <td style="background:#c00000;color:#fff;"><?php echo $value['mrp'] ?></td>
                            <td style="background:#c00000;color:#fff;"><?php echo $value['amount_after_discount'] ?></td>
                            
                        <?php } ?>
                        
                    </tr>
                    <?php
                    }
                    foreach ($subsectionArray as $key => $value1) {
                        $sql = 'SELECT * FROM `jp_sales_quotation` WHERE `parent_type` = 0 '
                                . ' AND `parent_id` = ' . $value1['id']
                                . ' AND revision_no="' . $rev_no . '" AND `deleted_status` = 1';
                        $itemArray = Yii::app()->db->createCommand($sql)->queryAll();

                        $sql = 'SELECT * FROM `jp_quotation_gen_worktype` '
                                . ' WHERE `qid` = ' . $value1["qid"]
                                . ' AND `section_id` = ' . $value1["section_id"]
                                . ' AND `category_label_id` = ' . $value1["id"]
                                . ' AND revision_no = "' . $rev_no . '"';
                        $subitemArray = Yii::app()->db->createCommand($sql)->queryAll();
                        $unit = Unit::model()->findByPk($value1['unit']);
                        ?>
                        <tr>
                            <th  style="background:#c5d9f1;"><?php echo $i; ?></th>
                            <th colspan="<?php echo (empty($itemArray)) && (empty($subitemArray)) ? '1' : '5' ?>" style="background:#c5d9f1;"><?php echo $value1['category_label'] ?></th>
                            <?php if ((empty($itemArray)) && (empty($subitemArray))) { ?>
                                <td  style="background:#c5d9f1;"><?php echo isset($unit) ? $unit->unit_name : "" ?></td>
                                <td  style="background:#c5d9f1;"><?php echo $value1['quantity_nos'] ?></td>
                                <td  style="background:#c5d9f1;"><?php echo $value1['mrp'] ?></td>
                                <td  style="background:#c5d9f1;"><?php echo $value1['amount_after_discount'] ?></td>
                                
                            <?php } ?>                                                                                        
                        </tr>

                        <?php
                        foreach ($itemArray as $key => $value2) {
                            ?>
                            <tr>
                                <td><?php echo '*' ?></td>
                                <td colspan="5"><?php echo $value2['worktype_label'] ?></td>
                            </tr>

                        <?php }
                        ?>
                        <?php
                        $letters = range('a', 'z');
                        $k = 0;
                        foreach ($subitemArray as $key => $value3) {
                            $sql = 'SELECT * FROM `jp_sales_quotation` '
                                    . 'WHERE `parent_type` = 1 AND `parent_id` = ' . $value3['id']
                                    . ' AND revision_no="' . $rev_no . '" AND `deleted_status` = 1';
                            $wrktypeitemArray = Yii::app()->db->createCommand($sql)->queryAll();
                            $unit = Unit::model()->findByPk($value3['unit']);
                            ?>
                            <tr class="td_group">
                                <th><?php echo $letters[$k]; ?></th>
                                <td colspan="<?php echo empty($wrktypeitemArray) ? '1' : '5' ?>"><b><?php echo $value3['worktype_label'] ?></b></td>
                                <?php if (empty($wrktypeitemArray)) { ?>
                                    <td><?php echo isset($unit) ? $unit->unit_name : "" ?></td>
                                    <td><?php echo $value3['quantity_nos'] ?></td>
                                    <td><?php echo $value3['mrp'] ?></td>
                                    <td><?php echo $value3['amount_after_discount'] ?></td>                                                    
                                <?php } ?>
                                
                            </tr>
                            <?php if ($value3['description'] != "") { ?>
                                <tr class="td_group">
                                    <th>*</th>
                                    <td colspan="5"><?php echo $value3['description'] ?></td>
                                    
                                </tr>
                                <?php
                            }
                            $sum = 0;
                            foreach ($wrktypeitemArray as $key => $value4) {
                                $sum += $value4['amount_after_discount'];
                                $unit = Unit::model()->findByPk($value4['unit']);

                                $desc = ($value4['description'] !="")?" - ".$value4['description']:"";
                                if($value4['description']=="" && ($value4['shutterwork_description'] !="" || $value4['carcass_description']!="")){
                                    $desc = " - <b>Shutter:</b> ".$value4['shutterwork_description']." , <b>Carcass:</b> ".$value4['carcass_description'];
                                }
                                 
                                ?>
                                <tr class="td_group">
                                    <th><?php echo '*' ?></th>
                                    <td><?php echo $value4['worktype_label'].$desc ?></td>
                                    <td><?php echo isset($unit) ? $unit->unit_name : "" ?></td>
                                    <td><?php echo $value4['quantity_nos'] ?></td>
                                    <td class="text-right"><?php echo $value4['mrp'] ?></td>
                                    <td class="text-right"><?php echo $value4['amount_after_discount'] ?></td>
                                    
                                </tr>

                                <?php
                            }
                            if (!empty($wrktypeitemArray)) {
                                ?>
                                <?php
                            } $k++;
                        }
                        ?>
                        <?php
                        $i++;
                        ?>
                        
                        <?php
                    }
                    ?>
                
                <?php } ?>
                
                <tr>
                    <td colspan="4" class="text-right text-lg"><h5>MRP</h5></td>
                    <th class="text-right  text-lg">
                        <h5><?php
                        $total_amount = Controller::getMrpSum($qtid, $rev_no,NULL);
                        echo Controller::money_format_inr($total_amount, 2);
                        ?></h5>
                    </th>
                    <th class="text-right text-lg">
                        <h5><?php
                        $total_amount = Controller::getQuotationSum($qtid, $rev_no,NULL);
                        echo Controller::money_format_inr($total_amount, 2);
                        ?></h5>
                    </th>
                </tr>
                <tr>
                    <?php
                        $sql = 'SELECT * FROM jp_quotation_revision '
                                . ' WHERE qid='.$qtid
                                . ' AND revision_no="'.$rev_no.'"';
                        $revision_model = Yii::app()->db->createCommand($sql)->queryRow();
                        $total_tax = $revision_model['sgst_amount']+
                                    $revision_model['cgst_amount']+
                                    $revision_model['igst_amount'];

                        $total_tax_percent = $revision_model['sgst_percent']+
                                    $revision_model['cgst_percent']+
                                    $revision_model['igst_percent'];

                        $total_amoun_withtax = $total_amount + $total_tax;
                    ?>
                    <th colspan="5" class="text-right">GST  
                        <?php if($total_tax_percent >0) {
                                echo "(".$total_tax_percent."%)";
                            }else{
                                echo "";
                            } ?></th>
                    <th  class="text-right">
                    <?php
                        echo Controller::money_format_inr($total_tax, 2);//total_amoun_withtax ?>
                    </th>
                </tr>
                <tr>
                    <th colspan="5" class="text-right">D2r Offer Price</th>
                    <th class="text-right">
                        <?php 
                        $discount_amount = ($total_amoun_withtax * $revision_model['discount_precent']) / 100;
                        echo Controller::money_format_inr($discount_amount , 2);                                        
                        ?>
                                        
                    </th>
                </tr>
                
                <tr>
                    <td colspan="5" class="text-right text-lg"><h5>Grand Total</h5></td>
                    <th class="text-right text-lg">
                        <h5><?php
                        $total_after_discount = $total_amoun_withtax - $discount_amount;
                        if($revision_model['total_after_additionl_discount']!=NULL){
                            $total_after_discount = $revision_model['total_after_additionl_discount'];
                        }
                        echo Controller::money_format_inr($total_after_discount , 2);                                        
                        ?></h5>
                    </th>
                </tr>
                
            </tbody>
        </table>
    </div>
</div>

<style>
    .text-lg{
        font-size:15px;
    }
</style>