<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/pdfstyle.css">

<div class="container" style="margin:0px 30px">
    <?php
    $company_address = Company::model()->findBypk($model->company_id);
    ?>
    <br>
    <h3 class="text-center">Quotation</h3>
    <table class="details"> 
        <tr>
            <td><label>PROJECT : </label><b><?php echo $project; ?></b></td>
            <td><label>CLIENT : </label><b><?php echo (isset($client)) ? $client : ''; ?></b></td>
        </tr>
        <tr>
            <td><label>COMPANY : </label><b><?php echo $company_address["name"] ?></b></td>            
            <td><label>GST NO : </label><b><?php echo (isset($model->inv_no)) ? $model->inv_no : ''; ?></b></td>
        </tr> 
        <tr>
            <td><label>DATE : </label><b><?php echo isset($model->date) ? date('d-m-Y', strtotime($model->date)) : ''; ?> </b></td>
            <td><label>Quotation No : </label><b><?php echo isset($model->inv_no) ? $model->inv_no : ''; ?></b></td>
        </tr>       
    </table>	
    <br>
    <table class="detail-table">
        <thead>
            <tr bgcolor="#ddd">
                <th>Sl.No</th>
                <th>Description</th>
                <th>Quantity</th>
                <th>Unit</th>
                <th>Rate</th>
                <th>SGST (%)</th>
                <th>SGST Amount</th>
                <th>CGST (%)</th>
                <th>CGST Amount</th>
                <th>IGST (%)</th>
                <th>IGST Amount</th>
                <th>Tax Amount</th>
                <th>Amount</th>
            </tr>            
        </thead>  
        <tbody class="addrow">
            <?php
            if (!empty($itemmodel)) {
                foreach ($itemmodel as $i => $value) {
                    ?>

                    <tr  class="tr_class">
                        <td><input type="hidden" name="sl_No[]" value="<?php echo $i + 1; ?>"> <?php echo $i + 1; ?></td>
                        <td><input type="hidden" name="description[]" value=""><?php echo $value->description; ?> </td>
                        <td class="text-right"><input type="hidden" name="quantity[]" value="<?php echo $value->quantity; ?>"> <?php echo $value->quantity; ?> </td>
                        <td><input type="hidden" name="unit[]" value="<?php echo $value->unit; ?>" readonly><?php echo $value->unit; ?></td>
                        <td class="text-right"><input type="hidden" name="rate[]" value="<?php echo $value->rate; ?>" readonly><?php echo number_format($value->rate, 2); ?></td>
                        <td class="text-right"><?php echo Controller::money_format_inr($value->sgst, 2); ?></td>
                        <td class="text-right"><?php echo Controller::money_format_inr($value->sgst_amount, 2); ?></td>
                        <td class="text-right"><?php echo Controller::money_format_inr($value->cgst, 2); ?></td>
                        <td class="text-right"><?php echo Controller::money_format_inr($value->cgst_amount, 2); ?></td>
                        <td class="text-right"><?php echo Controller::money_format_inr($value->igst, 2); ?></td>
                        <td class="text-right"><?php echo Controller::money_format_inr($value->igst_amount, 2); ?></td>
                        <td class="text-right"><?php echo Controller::money_format_inr($value->tax_amount, 2); ?></td>
                        <td class="text-right"><input type="hidden" name="amount[]" value="<?php echo $value->amount; ?>" readonly><?php echo Controller::money_format_inr($value->amount, 2); ?></td>

                    </tr>
                    <?php
                }
            }
            ?>
        </tbody>
        <tfoot>
            <tr>
                <th style="text-align: right;" colspan="11" class="text-right">TOTAL: </th>
                <th class="text-right"><div id="grand_total"><?php echo Controller::money_format_inr($model->tax_amount, 2); ?></div></th>
                <th class="text-right"><div id="grand_total"><?php echo Controller::money_format_inr($model->amount, 2); ?></div></th>
            </tr>
            <tr>
                <th colspan="12" class="text-right">GRAND TOTAL: <div id="net_amount"><?php echo Controller::money_format_inr($model->tax_amount + $model->amount, 2, '.', '') ?></div></th>
                <th></th>
            </tr>
        </tfoot> 
    </table>

    <br><br>

    <h5>Thanking you and assuring you the best services at all times.</h5>
    <h6>For <?php echo Yii::app()->name; ?></h6>
</div>
