
<?php
/* @var $this QuotationItemMasterController */
/* @var $model QuotationItemMaster */

$this->breadcrumbs = array(
    'Quotation Item Masters' => array('index'),
    'Manage',
);
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#quotation-item-master-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<?php if (Yii::app()->user->hasFlash('success')) : ?><br>
    <div class="info" style="width: 500px;margin-left: 400px;padding: 8px;color: green;font-size: 20px;font-weight: bold;">
        <?php echo Yii::app()->user->getFlash('success'); ?>
    </div>
<?php endif; ?>

<?php if (Yii::app()->user->hasFlash('error')) : ?>
    <div class="info" style="width: 500px;margin-left: 400px;padding: 8px;color: red;font-size: 20px;font-weight: bold;">
        <?php echo Yii::app()->user->getFlash('error'); ?>
    </div>
<?php endif; ?>
<div class="panel-body">
    <h1>Manage Quotation Item </h1>
    <div class="alert alert-success" role="alert" style="display:none;">
    </div>
    <div class="alert alert-danger" role="alert" style="display:none;">
    </div>
    <div class="alert alert-warning" role="alert" style="display:none;">    
    </div>
    <div class="add-btn pull-right">
        <?php
        echo CHtml::link('Add Quotation Item', array('quotationItemMaster/additem'), array('class' => 'button addworktype'));
        ?>
    </div>
    <br></br>

    <?php
    $this->renderPartial('_quotation_list', array(
        'model' => $model,
        'item_datas' => $item_datas
    ));
    ?>
</div>