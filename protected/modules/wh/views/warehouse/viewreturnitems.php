<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<style>  
    table.main_list tbody tr td:nth-child(2){
        border-left:none;
    }
    .invoicemaindiv th, .invoicemaindiv td{
        padding: 10px;
        text-align: left;
    }
    table.table tr td input {

    }
    .form-control[readonly] {
        border: 0;
        background-color: #fff;
    }
    table.total-table tr td {
        font-size:14px;
    }
    table.table .form-control {
        padding: 1px 1px;
        font-size: inherit;
        box-shadow: none;
    }
    .bill_title{border-bottom: 1px solid #ddd;}
    .bill_view{padding: 15px 0px;}
    .table-inverse thead{background-color: #eee;}
    .small_box input{width: 80px;}
    .bill_view input{border:0;}
    .amount_values input{border: 0;text-align:right;}
    .amn_data{
        background-color: #fafafa;
        border-bottom: 1px dashed #eee;margin-bottom:2px;}
    </style>
    <?php
    $tblpx = Yii::app()->db->tablePrefix;
    $billId = $model['receipt_id'];
    $bill_details = Yii::app()->db->createCommand("select * FROM " . $tblpx . "warehousereceipt WHERE warehousereceipt_id=" . $billId)->queryRow();
    ?>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/pdfstyle.css">
<?php
$style="";
if (filter_input(INPUT_GET, 'export')) {
    $style = "style='margin:0px 20px'";
}
    ?>
    <div class="container" <?php echo $style ?>>
    <div class="invoicemaindiv" >
        <div class="clearfix">
            <?php
            if (!filter_input(INPUT_GET, 'export')) {
            echo CHtml::link('SAVE AS EXCEL', Yii::app()->request->requestUri . "&export=csv", array('class' => 'save_btn pull-right mt-2 ml-1'));
            echo CHtml::link('SAVE AS PDF', Yii::app()->request->requestUri . "&export=pdf", array('class' => 'save_btn pull-right mt-2'));
           
            }
            ?>
           
            <h2 class="bill_title">DEFECT RETURN DETAILS</h2>
        </div>
        <?php
        $id = $model['receipt_id'];
        ?>


        <form id="pdfvals2" method="post" action="">
            <table class="main_list">
                <tbody>
                    <tr>
                        <td>
                            <label>BILL No:</label> 
                            <?php echo $bill_details['warehousereceipt_no']; ?>
                        </td>
                        <td>
                            <label>RETURN No:</label> 
                            <?php echo $model['return_number']; ?>
                        </td>
                        <td>
                            <label>DATE:</label> 
                            <?php echo date("d-m-Y", strtotime($model['return_date'])); ?>
                        </td>
                    </tr>
                </tbody>
            </table>        

            <div class="table-responsive">
                <table class="table table-inverse">
                    <thead>
                        <tr>
                            <th>Sl.No</th>
                            <th>Specifications/Remark</th>
                            <th>Quantity</th>
                            <th>Unit</th>
                            <th>Rate</th>
                            <th>Amount</th>                    
                        </tr>
                    </thead>
                    <tbody class="addrow">
                        <?php
                        $i = 1;
                        foreach ($newmodel as $new) {
                            //extract($new);
                            if ($new['category_id']) {
                                $categorymodel = Specification::model()->findByPk($new['category_id']);
                                if ($categorymodel)
                                    $categoryName = $categorymodel->specification;
                                else
                                    $categoryName = "Not available";
                            } else {
                                $categoryName = $new['remark'];
                            }
                            ?>
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td><?php echo $categoryName; ?></td>
                                <td class="quantity"><?php echo $new['returnitem_quantity']; ?></td>
                                <td><?php
                                    echo $new['returnitem_unit'];
                                    ;
                                    ?></td>
                                <td class="rate"><?php echo number_format((float) $new['returnitem_rate'], 2, '.', ''); ?></td>
                                <td><?php echo Controller::money_format_inr($new['returnitem_amount'], 2, 1); ?></td>

                            </tr>

                            <?php
                            $i++;
                        }
                        ?>
                    </tbody>
                </table>
            </div>
            <div class="row">
                <div class="col-md-offset-8 col-md-4 col-sm-offset-8 col-sm-4 col-xs-offset-6 col-xs-6 text-right amount_values">
                    <div class="amn_data"><label>Amount:</label> <span><?php echo Controller::money_format_inr($model['return_amount'], 2, 1); ?></span></div>


                </div>
            </div>

            <br><br>


        </form>
    </div>
    <script>

        $('.save_btn2').click(function () {
            $("form#pdfvals2").attr('action', "<?php echo $this->createAbsoluteUrl('purchaseReturn/returnpdf', array('id' => $model->return_id)); ?>");
            $("form#pdfvals2").submit();
        });

        $('.excelbtn2').click(function () {

            $("form#pdfvals2").attr('action', "<?php echo $this->createAbsoluteUrl('purchaseReturn/returncsv', array('id' => $model->return_id)); ?>");
            $("form#pdfvals2").submit();

        });



    </script>

    <style>
        a.pdf_excel {
            background-color: #6a8ec7;
            display:inline-block;
            padding:8px;
            color: #fff;
            border: 1px solid #6a8ec8;
        }
    </style>

    <?php $url = Yii::app()->createAbsoluteUrl("invoice/getclientByProject"); ?>
    <?php Yii::app()->clientScript->registerScript('myscript', '
    $(document).ready(function(){
    $("#project").change(function(){
    var pro_id= $("#project").val();
    $.ajax({
       url: "' . $url . '",
        data: {"id": pro_id}, 
        //dataType: "json",
        type: "POST",
        success:function(data){
       // alert(data);
           $("#billto").val(data); 
            
        }
        
});

});
$(".flash-success").animate({opacity: 1.0}, 5000).fadeOut("slow");
  
    });
        '); ?>


</div>
