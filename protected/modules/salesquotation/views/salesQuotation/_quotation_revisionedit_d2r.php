<div class="updatedetails">
    <?php
    $inter_rev = $_GET['selected_revision'].$_GET['rev'];
    $sql = 'SELECT count(*) FROM `jp_sales_quotation` '
            . ' WHERE master_id =' . $_GET['qid']
            . ' AND revision_no="' . $inter_rev . '"';
    $inter_rev_count = Yii::app()->db->createCOmmand($sql)->queryScalar();

    $latest_rev = 'REV-'.$_GET['rev'];
    $sql = 'SELECT count(*) FROM `jp_sales_quotation` '
            . ' WHERE master_id =' . $_GET['qid']
            . ' AND revision_no="' . $latest_rev . '"';
    $latest_rev_count = Yii::app()->db->createCOmmand($sql)->queryScalar();

    
    ?>
    <input type="hidden" value="<?php echo $inter_rev ?>" data-count = "<?php echo $inter_rev_count ?>" class="inter-rev">
    <input type="hidden" value="<?php echo $latest_rev ?>" data-count = "<?php echo $latest_rev_count ?>" class="latest-rev">
    <h4 class="ml-2" style="font-weight:bold">Edit Details</h4>
    <div class="add_sec">                
        <div class="sections_block">
                                   
                <?php
                $sql = 'SELECT * FROM `jp_quotation_section` '
                        . 'WHERE `qtn_id` = ' . $_GET['qid']
                        . ' AND revision_no IN( "' . $inter_rev . '","' . 'REV-'.$_GET['rev'] . '")';
                        
                $sectionArray = Yii::app()->db->createCommand($sql)->queryAll();
                                    
                foreach ($sectionArray as $key => $value) {
                    $sql = 'SELECT * FROM `jp_quotation_gen_category` '
                            . 'WHERE `qid` = ' . $_GET['qid'] . ' AND `section_id` = ' . $value["id"];                            
                    $subsectionArray = Yii::app()->db->createCommand($sql)->queryAll();
                ?>
                <div class="quotation_sec"> 
                <div class="panel" style="position:relative">
                    <div class="panel-heading">
                        <h5 class="bold">Main Section</h4>
                    </div>
                    <div class="panel-body">
                        <div class="row mt-2"> 
                            <?php echo CHtml::hiddenField('',$value['revision_no'], array('id' => 'row_revision')); ?>       
                            <div class="col-md-3">                                
                                <?php echo CHtml::hiddenField('',$value['id'], array('id' => 'section_id')); ?>
                                <?php echo CHtml::textField('',$value['section_name'],  array('class' => 'form-control  section_name', 'autocomplete' => 'off', 'placeholder' => 'Section', 'name' => 'section_name', 'id' => 'section_name')); ?>
                                <div class="errorMessage"></div>
                            </div>
                            
                            <div class="col-md-2">
                                <?php
                                echo CHtml::dropDownList('',$value['unit'], CHtml::listData(Unit::model()->findAll(), 'id', 'unit_name'), array('class' => 'form-control js-example-basic-single   require ', 'empty' => '-Select Unit-', 'name' => 'unit', 'style' => 'width:100%', 'id' => 'sec_unit')
                                );
                                ?>
                            </div>
                            <div class="col-md-1">
                                <?php echo CHtml::textField('', $value['quantity'], array('class' => 'form-control allownumericdecimal quantity_sub', 'autocomplete' => 'off', 'placeholder' => 'qty', 'name' => 'quantity', 'id' => 'sec_quantity')); ?>
                                
                            </div>
                            <div class="col-md-1">
                                <?php echo CHtml::textField('', $value['quantity_nos'], array('class' => 'form-control allownumericdecimal ', 'autocomplete' => 'off', 'placeholder' => 'qty nos', 'name' => 'quantity_nos', 'id' => 'sec_quantity_nos')); ?>                            
                            </div>
                            <div class="col-md-1">
                                <?php echo CHtml::textField('', $value['mrp'], array('class' => 'form-control allownumericdecimal ', 'autocomplete' => 'off', 'placeholder' => 'MRP', 'name' => 'mrp', 'id' => 'sec_mrp')); ?>
                                
                            </div>
                            <div class="col-md-2">
                                <?php echo CHtml::textField('', $value['amount_after_discount'], array('class' => 'form-control allownumericdecimal amountafter-discount', 'autocomplete' => 'off', 'name' => 'amount_after_discount', 'placeholder' => 'amt after disc', 'id' => 'sec_amount_after_discount')); ?>
                                
                            </div>
                        </div> 
                        <div class="add_sub_section toggle_btn" title="Add Sub Section">        
                            <i class="toggle-caret fa fa-plus"></i>
                        </div>
                        <!-- SUBSECTION  -->
                        <div class="sub_sec_block" style="display:none;"> 
                            <div class="sub_section">                           
                            <br>
                                <?php
                                
                                foreach ($subsectionArray as $key => $value) {
                                    $sql = 'SELECT * FROM `jp_quotation_gen_worktype` '
                                            . ' WHERE `qid` = ' . $value["qid"]
                                            . ' AND `section_id` = ' . $value["section_id"]
                                            . ' AND `category_label_id` = ' . $value["id"];
                                            
                                    $subitemArray = Yii::app()->db->createCommand($sql)->queryAll();
                                ?>
                        
                                <div id="items_holder" class="position-relative items_holder panel">
                                    <div class="panel-heading clearfix">
                                        <h5 class="bold pull-left">Sub Section</h5>
                                        <i class="fa fa-trash text-danger delete_sub pull-right text-lg" 
                                        rev-no="<?php echo $value['revision_no']; ?>" data-id="<?php echo $value['id']?>"></i>
                                    </div>
                                    <div class="quotationmaindiv panel-body" id="items_list">
                                        <div class="add_new_sec ">                        
                                            <div class="h-head">
                                                <div class="row">
                                                    <div class="col-md-2">
                                                        <label class="bold"></label>   
                                                        <input type="hidden" id="category_label_id" value="<?php echo $value['id']?>">
                                                        <input type="hidden" id="category_label_id" value="">                     
                                                        <?php echo CHtml::textField('', $value['category_label'], array('class' => 'form-control require', 'id' => 'category_label', 'placeholder' => 'Sub Section'));?>
                                                        <div class="errorMessage"></div>
                                                    </div>                                    
                                                    <div class="col-md-2">
                                                        <?php echo CHtml::dropDownList('', $value['unit'], CHtml::listData(Unit::model()->findAll(), 'id', 'unit_name'), array('class' => 'form-control js-example-basic-single   require ', 'empty' => '-Select Unit-', 'name' => 'unit', 'style' => 'width:100%', 'id' => 'cat_unit'));?>
                                                    </div>
                                                    <div class="col-md-1">
                                                    <?php echo CHtml::textField('', $value['quantity'], array('class' => 'form-control allownumericdecimal quantity_sub', 'autocomplete' => 'off', 'placeholder' => 'qty', 'name' => 'quantity', 'id' => 'cat_quantity')); ?>                                                    
                                                    </div>
                                                    <div class="col-md-1">
                                                        <?php echo CHtml::textField('', $value['quantity_nos'], array('class' => 'form-control allownumericdecimal ', 'autocomplete' => 'off', 'placeholder' => 'qty nos', 'name' => 'quantity_nos', 'id' => 'cat_quantity_nos')); ?>                                                    
                                                    </div>
                                                    <div class="col-md-1">
                                                        <?php echo CHtml::textField('', $value['mrp'], array('class' => 'form-control allownumericdecimal ', 'autocomplete' => 'off', 'placeholder' => 'MRP', 'name' => 'mrp', 'id' => 'cat_mrp')); ?>
                                                        
                                                    </div>
                                                    <div class="col-md-2">
                                                        <?php echo CHtml::textField('', $value['amount_after_discount'], array('class' => 'form-control allownumericdecimal amountafter-discount', 'autocomplete' => 'off', 'name' => 'amount_after_discount', 'placeholder' => 'amt after disc', 'id' => 'cat_amount_after_discount')); ?>                                                    
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="add_wrktype toggle_btn1" title="Add Sub Section">        
                                        <i class="toggle-caret fa fa-plus"></i>
                                    </div>
                                    <!-- SUBITEM -->
                                    <div class="worktype_label_sec px-3 mt-2" style="display:none;">
                                        <div class=" panel panel-default">
                                            <div class="panel-heading position-relative">
                                                <div class="clearfix">
                                                    <div class="pull-left">
                                                        <h5>Add Sub Items</h5>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel-body" style="background: #f8fbff;">
                                            <?php
                                                            $form = $this->beginWidget('CActiveForm', array(
                                                                'id' => 'section-form',
                                                                'enableAjaxValidation' => true,
                                                                'enableClientValidation' => true,
                                                                //'action' => Yii::app()->createAbsoluteUrl("salesquotation/salesQuotation/addquotationitems"),
                                                                'htmlOptions' => array('enctype' => 'multipart/form-data'),
                                                                'clientOptions' => array(
                                                                    'validateOnSubmit' => true,
                                                                    'validateOnChange' => true,
                                                                    'validateOnType' => false,
                                                                ),
                                                                    ));
                                                            ?> 
                                                <div class="worktype_label_data">
                                                                                               
                                                    <?php 
                                                    
                                                    foreach ($subitemArray as $key => $value) {
                                                        $sql = 'SELECT * FROM `jp_sales_quotation` '
                                                        . ' WHERE `parent_type` = 1 ' 
                                                        . ' AND `parent_id` = ' . $value['id'];
                                                        
                                                        $wrktypeitemArray = Yii::app()->db->createCommand($sql)->queryAll();
                                                        
                                                        ?> 
                                                                                           
                                                        <div class="worktype_label_row  position-relative">
                                                            <i class="fa fa-trash text-danger delete_subitem text-lg" 
                                                                style="position: absolute;left: 43%;top: 85px"></i>
                                                            <div class="row">
                                                                <div class="col-md-2">
                                                                    <label>Work Type </label>                                                            
                                                                    <input type="hidden" id="worktype_label_id" value="<?php echo $value['id'] ?>">                        
                                                                    <?php
                                                                    echo CHtml::textField('', $value['worktype_label'], array('class' => 'form-control require', 'id' => 'worktype_label', 'placeholder' => 'Work Type'));
                                                                    ?>
                                                                    <div class="errorMessage"></div>
                                                                </div>
                                                                <div class="col-md-2">
                                                                    <label>&nbsp;</label>
                                                                    <?php
                                                                    
                                                                    echo CHtml::dropDownList('', $value['master_cat_id'],
                                                                            CHtml::listData(QuotationCategoryMaster::model()->findAll(), 'id', 'name'),                                                                            
                                                                            array('class' => 'form-control js-example-basic-single field_change require QuotationGenWorktype_master_cat_id', 'empty' => '-Select Category-', 'style' => 'width:100%'));
                                                                    ?>
                                                                </div>
                                                                <div class="col-md-2">
                                                                    <label>&nbsp;</label>
                                                                    <?php
                                                                    echo CHtml::dropDownList('', $value['unit'], CHtml::listData(Unit::model()->findAll(), 'id', 'unit_name'), array('class' => 'form-control js-example-basic-single   require ', 'empty' => '-Select Unit-', 'name' => 'unit', 'style' => 'width:100%', 'id' => 'wrktype_unit')
                                                                    );
                                                                    ?>
                                                                </div>
                                                                <div class="col-md-1">
                                                                    <label>&nbsp;</label>
                                                                    <?php echo CHtml::textField('', $value['quantity'], array('class' => 'form-control allownumericdecimal quantity_sub', 'autocomplete' => 'off', 'placeholder' => 'qty', 'name' => 'quantity', 'id' => 'wrktype_quantity')); ?>                                                            
                                                                </div>
                                                                <div class="col-md-1">
                                                                    <label>&nbsp;</label>
                                                                    <?php echo CHtml::textField('', $value['quantity_nos'], array('class' => 'form-control allownumericdecimal ', 'autocomplete' => 'off', 'placeholder' => 'qty nos', 'name' => 'quantity_nos', 'id' => 'wrktype_quantity_nos')); ?>                                                            
                                                                </div>
                                                                <div class="col-md-2">
                                                                    <label>&nbsp;</label>
                                                                    <?php echo CHtml::textField('', $value['mrp'], array('class' => 'form-control allownumericdecimal ', 'autocomplete' => 'off', 'placeholder' => 'MRP', 'name' => 'mrp', 'id' => 'wrktype_mrp')); ?>                                                            
                                                                </div>
                                                                <div class="col-md-2">
                                                                    <label>&nbsp;</label>
                                                                    <?php echo CHtml::textField('', $value['amount_after_discount'], array('class' => 'form-control allownumericdecimal amountafter-discount', 'autocomplete' => 'off', 'name' => 'amount_after_discount', 'placeholder' => 'amt after disc', 'id' => 'wrktype_amount_after_discount')); ?>                                                            
                                                                </div>
                                                                <div class="col-md-5">
                                                                    <label>&nbsp;</label>
                                                                    <textarea class="form-control mb-2" name="description" id="wrktype_desc"></textarea>
                                                                </div>
                                                            </div>
                                                            <br>
                                                            <div class="sub_worktype_selector" id="quotation_item"></div>
                                                            <div class="subitem_detais">
                                                                <?php
                                                                $i=0;
                                                                foreach ($wrktypeitemArray as $key => $value) {
                                                                                                                                    
                                                                    $unit = Unit::model()->findByPk($value['unit']);
                                                                    echo CHtml::hiddenField('', $value['category_id'], array('class' => 'selected_cat_id' . $i));
                                                                    if($i==0){
                                                                    ?>
                                                                    <div class="row">
                                                                        <div class="col-md-4">
                                                                        <input type="text" class="form-control category_label" value="<?php echo $value['category_label'] ?>" readonly>
                                                                        </div>
                                                                    </div>
                                                                        
                                                                    <?php }?>
                                                                    <br>
                                                                    <div class="worktype_data_sub row  mb-2 clients-form <?php echo ($value['deleted_status'] == 0) ? 'bg-danger':''?>">

                                                                        <input type="hidden" id="entry_id" value="<?php echo $value['id'] ?>">
                                                                        <input type="hidden" id="cat_id" value="<?php echo $value['category_id'] ?>">
                                                                        <input type="hidden" id="subItem_category_label_id"  value="<?php echo $value['id'] ?>"> 
                                                                        <input type="hidden" class="form-control category_id" value="<?php echo $value['category_id'] ?>">
                                                                        <input type="hidden" class="form-control master_id" value="<?php echo $value['master_id'] ?>">
                                                                        <input type="hidden" class="form-control item_id" value="<?php echo $value['item_id'] ?>">
                                                                        <input type="hidden" class="form-control work_type" value="<?php echo $value['work_type'] ?>">

                                                                        <input type="hidden" class="form-control revision_no" value="<?php echo $_GET['rev'] ?>">
                                                                        <input type="hidden" class="form-control parent_type" value="<?php echo $value['parent_type'] ?>">
                                                                        <input type="hidden" class="form-control parent_id" value="<?php echo $value['parent_id'] ?>">
                                                                        <div  class="col-md-2">
                                                                            <?php
                                                                            $value['worktype_label']
                                                                            ?>
                                                                            <input type="text" id = "<?php echo $value['item_id'] ?>" class="form-control worktype_label" value="<?php echo $value['worktype_label'] ?>" readonly>
                                                                        </div>
                                                                        <input class="form-control require" value="<?php echo $value['shutterwork_description'] ?>" id="shutterwork_description" type="hidden">                                
                                                                        <input class="form-control require" value="<?php echo $value['carcass_description'] ?>" id="caracoss_description" type="hidden" value="">                                
                                                                        <input class="form-control require" value="<?php echo $value['shutterwork_material'] ?>" id="shutterwork_material" type="hidden" value="">                                
                                                                        <input class="form-control require" value="<?php echo $value['shutterwork_finish'] ?>" id="shutterwork_finish" type="hidden" value="">                                
                                                                        <input class="form-control require" value="<?php echo $value['carcass_material'] ?>" id="carcass_material" type="hidden" value="">                                
                                                                        <input class="form-control require" value="<?php echo $value['carcass_finish'] ?>" id="carcass_finish" type="hidden" value=""> 
                                                                        <input class="form-control require" value="<?php echo $value['deleted_status'] ?>" id="deleted_status" type="hidden" value=""> 

                                                                        <div  class="col-md-2">

                                                                            <input class="form-control require common-val description" name="description" readonly="readonly" type="text" maxlength="255" value="<?php echo $value['description'] ?>">
                                                                        </div>
                                                                        <div  class="col-md-1">
                                                                            <?php
                                                                            $unit = Unit::model()->findByPk($value['unit']);
                                                                            echo CHtml::dropDownList(
                                                                                    '', 'unit', CHtml::listData(Unit::model()->findAll(), 'id', 'unit_name'), array('class' => 'form-control js-example-basic-single field_change invoice_add require common-val unit_select old_unit', 'empty' => $unit->unit_name, 'id' => 'unit', 'name' => 'unit[]', 'style' => 'width:100%')
                                                                            );
                                                                            ?>
                                                                            <input type="hidden" class="form-control unit" value="<?php echo $value['unit'] ?>">
                                                                           
                                                                        </div>
                                                                        <div  class="col-md-1">
                                                                            <input type="text" class="form-control old_quantity" data-incremntid="<?php echo $key?>" value="<?php echo $value['quantity'] ?>">
                                                                        </div>
                                                                        <div  class="col-md-1">
                                                                            <input type="text" class="form-control old_quantity_nos" value="<?php echo $value['quantity_nos'] ?>">
                                                                        </div>
                                                                        <div  class="col-md-2 text-right">
                                                                            <input type="text" class="form-control mrp" value="<?php echo $value['mrp'] ?>">
                                                                        </div>
                                                                        <div  class="col-md-2 text-right">
                                                                            <input type="text" class="form-control amount_after_discount" value="<?php echo $value['amount_after_discount'] ?>">
                                                                        </div>
                                                                        <div class="col-md-1">
                                                                        <?php if ($value['deleted_status'] == 1) { ?>
                                                                            <button id="<?php echo $value['id'] ?>" data-del ="<?php echo $value['deleted_status'] ?>"   class="removebtn"><span class="fa fa-trash removeQItem"></span></button>
                                                                            <?php } else { ?>
                                                                            <button id="<?php echo $value['id'] ?>" data-del ="<?php echo $value['deleted_status'] ?>"  class="removebtn"><span class="fa fa-refresh restoreQItem"></span></button>
                                                                        <?php } ?>
                                                                        </div>
                                                                    </div> 
                                                                    <br>               
                                                                <?php $i++; } ?>
                                                            </div>
                                                        </div>
                                                        <hr> 
                                                                                                       
                                                    <?php } 
                                                    echo $this->renderPartial('_subitem', array('main_wtype_model'=>$main_wtype_model));
                                                    ?>
                                                </div> <!-- work type label data -->
                                                <div class="panel-footer save-btnHold text-center button-panel">
                                                    <button type="button" class = 'btn btn-info additem'>Save</button>
                                                    <button type="button" class ='btn btn-default btn_close'>Close</button>
                                                </div>
                                                <?php $this->endWidget(); ?>
                                                <div class="clearfix">
                                                    <div class="pull-right">
                                                        <button class="btn btn-primary btn-sm add_more_wtype">Add more Sub Item</button>
                                                        <button class="btn btn-danger btn-sm remove_wtype hide">Remove</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="" id="quotation_item_edit"></div>
                                    <!-- SUBITEM ENDS-->
                                </div>
                            
                                <?php } 
                                echo $this->renderPartial('_subsection', 
                                array('main_cat_model'=>$main_cat_model,'main_wtype_model'=>$main_wtype_model,'rev' => $rev_no,
                                'qid' => $qid,'status'=>$_GET['status']));
                                ?>
                                
                            </div>
                            <div class="clearfix">                                                
                                <div class="pull-right">
                                    <button class="btn btn-primary btn-sm add_more_subsec">Add More Sub Section</button>
                                    <button class="btn btn-danger btn-sm remove_sub hide">Remove</button>
                                </div>
                            </div>
                        </div>
                        <!-- SUBSECTION END -->
                    </div>                                
                </div>
            </div>
            <?php } ?>
            
        </div>
    </div>  
</div>              
<script>
<?php

$amountget = Yii::app()->createAbsoluteUrl("salesquotation/salesQuotation/getamountdetails");
?>
$(function(){
    $(".updatedetails").find(".worktype_label_row").each(function(){
       var cat_id = $(this).find(".subitem_detais").find(".selected_cat_id0").val();       
    })

    var inter_count = $(".inter-rev").attr("data-count");
    var latest_count = $(".latest-rev").attr("data-count");

    if(inter_count == 0 && latest_count ==0 ){                   
        var selected_revision = "<?php echo $_GET['selected_revision']; ?>";
        var qid = "<?php echo $_GET['qid']; ?>";   
        var inter_rev = $(".inter-rev").val(); 
        var url = "<?php echo Yii::app()->createAbsoluteUrl('salesquotation/SalesQuotation/saveInterRevision'); ?>";

        $.ajax({
            url: url,
            data: {            
                qtn_id: qid,
                selected_revision: selected_revision, 
                inter_rev:inter_rev           
            },
            type: "POST",
            dataType: 'json',
            success: function (result) {
                console.log("success");
                
            },
            complete: function(){
                setTimeout(function () {
                    location.reload(true);
                },2000);
            }
        })
    }

    if(inter_count > 0){
        var answer = confirm("do you want to save the revision details?");
        if (answer) {
            $( ".updateRevision" ).trigger( "click" );
        }
    }
    
})

function saveItemUpdate(enbtry_id,qty,quantity_nos,profit_amount,mrp_after_discount,qid,rev){    
    $.ajax({
        url: "<?php echo Yii::app()->createAbsoluteUrl("salesquotation/salesQuotation/saveItemUpdate"); ?>",
        data: {
            enbtry_id: enbtry_id,
            qty: qty,
            quantity_nos:quantity_nos,
            profit_amount:profit_amount,
            mrp_after_discount:mrp_after_discount,
            rev:rev,
            qid:qid
        },
        type: "POST",
        dataType:"json",
        success: function (result) {
            
            $(document).find("#total_after_discount").val(result.total_amount)
            $(document).find("#total_after_discount").attr('data-val',result.data_val);
            $("#sgst_percent").change();
            $("#cgst_percent").change();
            $("#igst_percent").change(); 
        }
    })

}

function getamountold(element, id) {
    
    var item_id = $(element).parents('.worktype_data_sub').find(".worktype_label").val();
    var qty = $(element).parents('.worktype_data_sub').find(".old_quantity").val();  
    var quantity_nos = $(element).parents('.worktype_data_sub').find(".old_quantity_nos").val();      
    var status = '<?php echo isset($_GET['status'])?$_GET['status']:""; ?>';
    var qid = '<?php echo isset($_GET['qid'])?$_GET['qid']:""; ?>';
    var rev = '<?php echo isset($_GET['rev'])?$_GET['rev']:""; ?>';
    var enbtry_id =$(element).parents('.worktype_data_sub').find("#entry_id").val();

    $.ajax({
        url: "<?php echo $amountget; ?>",
        data: {
            item_id: item_id,
            qty: qty,
            status:1
        },
        type: "POST",
        dataType: 'json',
        success: function (result) {
            if (result != '') {
                var amount = result.profit_amount;
                $(element).parents('.worktype_data_sub').find(".amount_after_discount").val(result.profit_amount);
                var mrp_after_discount = parseInt(result.profit_amount) + parseInt((result.profit_amount * result.discount) / 100);
                $(element).parents('.worktype_data_sub').find(".mrp").val(mrp_after_discount);
                    
                $("#sgstp").change();
                $("#cgstp").change();
                $("#igstp").change(); 
                saveItemUpdate(enbtry_id,qty,quantity_nos,result.profit_amount,mrp_after_discount,qid,rev);

            }
        }
    });
}

$(document).on('change', '.old_quantity,.old_quantity_nos,.old_unit', function () {
                
        var element = $(this);        
        var category_id = $(this).parents(".clients-form").find("#cat_id").val();         
        var val = element.attr('data-incremntid');
        var worktype = $(this).parents(".worktype_data_sub").find(".work_type").val();        
        
        getamountold(element, val);
        
        var worktype_label =$(this).parents(".worktype_data_sub").find('.worktype_label').val();        
        
    });
    



</script>