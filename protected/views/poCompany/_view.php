<?php
/* @var $this ProjectsController */
/* @var $data Projects */
?>

<?php
$tblpx = Yii::app()->db->tablePrefix;
$id = $data->id;
?>

<?php if ($index == 0) { ?>
    <thead class="entry-table">
        <tr>
            <th style="width:20px;">Sl No.</th>
            <th>Name</th>
            <th>Phone</th>
            <th>Email ID</th>
            <th>GST NO</th>
            <th>Pin code</th>
            <th>Address</th>
            <th>Description</th>
            <?php
            if (isset(Yii::app()->user->role) && (in_array('/poCompany/update', Yii::app()->user->menuauthlist))) {
                ?>
                <th></th>
            <?php } ?>
        </tr>
    </thead>
<?php } ?>

<tr>
    <td style="width:20px;">
        <?php echo $index + 1; ?>
    </td>
    <td style="width:20px;">
        <?php echo CHtml::encode($data->name); ?>
    </td>
    <td style="width:20px;">
        <?php echo CHtml::encode($data->phone); ?>
    </td>
    <td style="width:30px;">
        <?php echo CHtml::encode($data->email_id); ?>
    </td>
    <td style="width:30px;">
        <?php echo CHtml::encode($data->company_gstnum); ?>
    </td>
    <td style="width:30px;">
        <?php echo CHtml::encode($data->pincode); ?>
    </td>
    <td style="width:30px;">
        <?php echo CHtml::encode($data->address); ?>
    </td>
    <td style="width:30px;">
        <?php echo CHtml::encode($data->description); ?>
    </td>
    <?php
    if (isset(Yii::app()->user->role) && (in_array('/poCompany/update', Yii::app()->user->menuauthlist))) {
        ?>
        <!--                <td  style="width:40px;"><span class="fa fa-edit editVendors" data-toggle="modal" data-target=".edit" data-id="<?php echo $id; ?>"></span></td>-->
        <td style="width:40px;"><span class="fa fa-edit editVendors" data-id="<?php echo $id; ?>"></span></td>
    <?php } ?>
</tr>


<!--div class="row" style=""  >
                <div class="col-md-8 col-sm-7 col-xs-5">
        <div class="list_item"><span>Vendor Name: </span><?php //echo CHtml::encode($data->name); ?></div>
                <div class="list_item"><span>Phone: </span><?php //echo CHtml::encode($data->phone); ?></div>
                <div class="list_item"><span>Email ID: </span><?php //echo CHtml::encode($data->email_id); ?></div>
        <?php //if(Yii::app()->user->role ==1 || Yii::app()->user->role ==2){
        ?>
        <div class="list_item"><span>Expense types: </span><?php // echo Vendors::model()->getExpensetypes($data->vendor_id);  //echo implode(", ", $rendering); ?></div>
        <?php
        //}
        ?>
        
    </div>
    

    <?php //if(Yii::app()->user->role ==1 || Yii::app()->user->role ==2){
    ?>
    <div class="col-md-3 col-sm-1 col-xs-1">
        <span class="fa fa-edit editVendors" data-toggle="modal" data-target=".edit" data-id="<?php //echo $id; ?>"></span>
    </div>
    <?php
    //}
    ?>
</div-->