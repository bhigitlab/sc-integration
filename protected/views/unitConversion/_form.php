<?php
/* @var $this UnitConversionController */
/* @var $model UnitConversion */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'unit-conversion-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'item_id'); ?>
		<?php echo $form->textField($model,'item_id'); ?>
		<?php echo $form->error($model,'item_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'base_unit'); ?>
		<?php echo $form->textField($model,'base_unit'); ?>
		<?php echo $form->error($model,'base_unit'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'conversion_factor'); ?>
		<?php echo $form->textField($model,'conversion_factor'); ?>
		<?php echo $form->error($model,'conversion_factor'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'conversion_unit'); ?>
		<?php echo $form->textField($model,'conversion_unit'); ?>
		<?php echo $form->error($model,'conversion_unit'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'created_date'); ?>
		<?php echo $form->textField($model,'created_date'); ?>
		<?php echo $form->error($model,'created_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'priority'); ?>
		<?php echo $form->textField($model,'priority',array('size'=>1,'maxlength'=>1)); ?>
		<?php echo $form->error($model,'priority'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->