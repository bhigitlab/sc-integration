<?php

class SalesInvoiceController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout = '//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		$controller = Yii::app()->controller->id;
		$hidden_actions =  array('getClient', 'addItem', 'addSubItem', 'getItemById');
		$rules = AccessRulesHelper::getRules($controller, $hidden_actions);
		//Additional action rules
		return $rules;
	}

	public function actionAdmin()
	{
		$model = new SalesInvoice('search');
		$model->unsetAttributes();  // clear any default values
		if (isset($_GET['SalesInvoice']))
			$model->attributes = $_GET['SalesInvoice'];

		$this->render('admin', array(
			'model' => $model,
		));
	}

	public function actionCreate()
	{
		$model = new SalesInvoice;
		$main_item_model = new SalesInvoiceItem();
		$sub_item_model = new SalesInvoiceSubItem('search');
		$sub_item_model->unsetAttributes();  // cl
		$this->performAjaxValidation($model);
		$user = Users::model()->findByPk(Yii::app()->user->id);
		if (isset($_POST['SalesInvoice'])) {
			$model->attributes = $_POST['SalesInvoice'];
			$project_model =  Projects::model()->findByPk($model->project_id);
			$model->client_id = $project_model->client_id;
			$model->invoice_date = date('Y-m-d', strtotime($model->invoice_date));
			$model->created_by = Yii::app()->user->id;
			$model->created_date = date('Y-m-d H:i:s');
			if (!$model->save()) {
				$errors = json_encode($model->getErrors());
				Yii::app()->user->setFlash('error', $errors);
			} else {
				Yii::app()->user->setFlash('success', "Successfully Created");
				$this->redirect(array('salesInvoice/update', 'id' => $model->id));
			}
		}

		$this->render('create', array(
			'model' => $model, 'user_companies' => $user->company_id, 'main_item_model' => $main_item_model, 'sub_item_model' => $sub_item_model
		));
	}

	public function actionUpdate($id)
	{
		$model = $this->loadModel($id);
		$main_item_model = new SalesInvoiceItem();
		$sub_item_model = new SalesInvoiceSubItem('search');
		$sub_item_model->unsetAttributes();  // clear any default values
		if (isset($_GET['SalesInvoiceSubItem']))
			$sub_item_model->attributes = $_GET['SalesInvoiceSubItem'];
		$this->performAjaxValidation($model);
		$user = Users::model()->findByPk(Yii::app()->user->id);
		if (isset($_POST['SalesInvoice'])) {
			$model->attributes = $_POST['SalesInvoice'];
			$project_model =  Projects::model()->findByPk($model->project_id);
			$model->client_id = $project_model->client_id;
			if ($model->save()) {
				Yii::app()->user->setFlash('success', "Successfully Updated");
			} else {
				$errors = $model->getErrors();
				Yii::app()->user->setFlash('error', $errors);
			}
			$this->redirect(array('salesInvoice/update', 'id' => $model->id));
		}

		$this->render('create', array(
			'model' => $model, 'user_companies' => $user->company_id, 'main_item_model' => $main_item_model, 'sub_item_model' => $sub_item_model
		));
	}

	public function actiongetClient()
	{
		$project_id = filter_input(INPUT_POST, 'project_id', FILTER_SANITIZE_SPECIAL_CHARS);
		$client_name = '';
		$result = array();
		if (!empty($project_id)) {
			$model = Projects::model()->findByPk($project_id);
			$client_name = isset($model->client->name) ? $model->client->name : '';
			$result = array('client_id' => $model->client_id, 'client_name' => $client_name);
		}
		echo json_encode($result);
	}

	public function actionaddItem()
	{
		if (isset($_POST['SalesInvoiceItem'])) {
			$model  = new SalesInvoiceItem();
			$message = 'Successfully Created';
			$status = 1;
			if (!empty($_POST['SalesInvoiceItem']['id'])) {
				$id = $_POST['SalesInvoiceItem']['id'];
				$message = 'Successfully Updated';
				$model = SalesInvoiceItem::model()->findByPk($id);
			}
			$model->attributes = $_POST['SalesInvoiceItem'];
			if (!empty($id)) {
				$model->updated_by = Yii::app()->user->id;
				$model->updated_date = date('Y-m-d H:i:s');
			} else {
				$model->created_by = Yii::app()->user->id;
				$model->created_date = date('Y-m-d H:i:s');
			}
			if (!$model->save()) {
				$errors = json_encode($model->getErrors());
				$status = 0;
				// Yii::app()->user->setFlash('error', $errors);
			} else {
				// Yii::app()->user->setFlash('success', $message);
			}
			// $this->redirect(array('salesInvoice/update', 'id' => $model->sales_invoice_id));
			$result = array('status' => $status, 'message' => $message, 'model' => $model->attributes);
			echo json_encode($result);
		}
	}

	public function actionaddSubItem()
	{


		if (isset($_POST['SalesInvoiceSubItem'])) {
			$ids = $_POST['SalesInvoiceSubItem']['id'];
			$data = $_POST['SalesInvoiceSubItem'];
			$transaction = Yii::app()->db->beginTransaction();
			$error = '';
			try {
				foreach ($ids as $key => $id) {
					$model  = new SalesInvoiceSubItem();

					if (!empty($id)) {
						$model = SalesInvoiceSubItem::model()->findByPk($id);
						$model->updated_by = Yii::app()->user->id;
						$model->updated_date = date('Y-m-d H:i:s');
					} else {
						$model->created_by = Yii::app()->user->id;
						$model->created_date = date('Y-m-d H:i:s');
					}
					$model->invoice_id = $data['invoice_id'];
					$model->invoice_item_id = $data['invoice_item_id'];
					$model->item_description = $data['item_description'][$key];
					$model->item_type = $data['item_type'][$key];
					$model->quantity = $data['quantity'][$key];
					$model->unit = $data['unit'][$key];
					$model->rate = $data['rate'][$key];
					$model->length = $data['length'][$key];
					$model->width = $data['width'][$key];
					$model->height = $data['height'][$key];
					$model->amount = $data['amount'][$key];
					if ($model->item_type == 1) {
						$model->length = $model->width = $model->height = 0;
					} elseif ($model->item_type == 2) {
						$model->length = $model->width = $model->height = $model->quantity = 0;
					} elseif ($model->item_type == 3) {
						$model->height = 0;
					} else {
					}

					if (!$model->save()) {
						$errors = json_encode($model->getErrors());
						throw new Exception($errors);
						$success_status = 0;
					} else {

						$success_status = 1;
						$amount_sum = SalesInvoiceSubItem::model()->find(array(
							'select' => 'invoice_item_id, SUM(amount) as amount',
							'condition' => 'invoice_item_id=:invoice_item_id',
							'params' => array(':invoice_item_id' => $model->invoice_item_id)
						));
						$invoice_item = SalesInvoiceItem::model()->findByPk($model->invoice_item_id);
						$invoice_item->total_amount = isset($amount_sum['amount']) ? $amount_sum['amount'] : '';
						if (!$invoice_item->save()) {
							$success_status = 0;
							$errors = json_encode($invoice_item->getErrors());
							throw new Exception($errors);
						}
						$invoice_model = SalesInvoice::model()->findByPk($model->invoice_id);

						$invoice_model->amount = $invoice_model->getItemTotal($invoice_model);
						$invoice_model->total_amount = $invoice_model->getTotal($invoice_model);
						if (!$invoice_model->save()) {
							$success_status = 0;
							$errors = json_encode($invoice_model->getErrors());
							throw new Exception($errors);
						}
					}
				}

				$transaction->commit();
			} catch (Exception $error) {
				$transaction->rollBack();
				$success_status = 0;
				$error =  $error->getMessage();
			} finally {
				if ($success_status == 1) {
					Yii::app()->user->setFlash('success', "Successfully Created");
				} else {
					Yii::app()->user->setFlash('error', $error);
				}
				$this->redirect(array('salesInvoice/update', 'id' => $model->invoice_id));
			}
		}
	}

	public function actiongetItemById()
	{
		$id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_SPECIAL_CHARS);
		if (!empty($id)) {

			$model = SalesInvoiceSubItem::model()->findByPk($id);
			echo json_encode($model->attributes);
		}
	}

	public function actiondeleteInvoice($id)
	{
		$model = $this->loadModel($id);
		$invoice_items = SalesInvoiceItem::model()->findAllByAttributes(array('sales_invoice_id' => $id));
		$invoice_sub_items = SalesInvoiceSubItem::model()->findAllByAttributes(array('invoice_id' => $id));
		$transaction = Yii::app()->db->beginTransaction();

		try {
			if (!empty($invoice_sub_items)) {
				SalesInvoiceItem::model()->deleteAllByAttributes(array('sales_invoice_id' => $id));
			}
			if (!empty($invoice_items)) {
				SalesInvoiceSubItem::model()->deleteAllByAttributes(array('invoice_id' => $id));
			}
			if ($model->delete()) {
				$message = 'Succussfully Deleted';
				$status = 1;
			} else {
				$errors = $model->getErrors();
				$status = 0;
				throw new Exception(json_encode($errors));
			}
			$transaction->commit();
		} catch (Exception $error) {
			$transaction->rollBack();
			$status = 0;
			$message =  $error->getMessage();
		} finally {
			if ($status == 1) {
				Yii::app()->user->setFlash('success', $message);
			} else {
				Yii::app()->user->setFlash('error', $message);
			}
			$this->redirect(array('salesInvoice/admin'));
		}

		// $this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		// if (!isset($_GET['ajax']))
		// 	$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	public function actiondeleteSubItem()
	{
		$id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_SPECIAL_CHARS);
		if (!empty($id)) {
			$model = SalesInvoiceSubItem::model()->findByPk($id);
			$attributes = $model->attributes;

			$invoice_parent_id = $model->invoice_item_id;
			$transaction = Yii::app()->db->beginTransaction();
			$message = 'Succussfully Deleted';
			$status = 1;
			try {
				if ($model->delete()) {
					$amount_sum = SalesInvoiceSubItem::model()->find(array(
						'select' => 'invoice_item_id, SUM(amount) as amount',
						'condition' => 'invoice_item_id=:invoice_item_id',
						'params' => array(':invoice_item_id' => $invoice_parent_id)
					));
					$invoice_item = SalesInvoiceItem::model()->findByPk($attributes['invoice_item_id']);
					$invoice_item->total_amount = isset($amount_sum['amount']) ? $amount_sum['amount'] : '';
					if (!$invoice_item->save()) {
						$message = json_encode($invoice_item->getErrors());
						throw new Exception($message);
					}
					$invoice_model = SalesInvoice::model()->findByPk($attributes['invoice_id']);
					$invoice_model->amount = $invoice_model->getItemTotal($invoice_model);
					$invoice_model->total_amount = $invoice_model->getTotal($invoice_model);
					if (!$invoice_model->save()) {
						$success_status = 0;
						$errors = json_encode($invoice_model->getErrors());
						throw new Exception($errors);
					}
				} else {
					$message = json_encode($model->getErrors());
					throw new Exception($message);
				}
				$transaction->commit();
			} catch (Exception $error) {
				$transaction->rollBack();
				$status = 0;
				$message =  $error->getMessage();
			} finally {
				$result = array('status' => $status, 'message' => $message);
				if ($status == 1) {
					Yii::app()->user->setFlash('success', $message);
				} else {
					Yii::app()->user->setFlash('error', $message);
				}
				echo json_encode($result);
			}
		}
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$invoice_categories = SalesInvoiceItem::model()->findAll(array(
			'condition' => "sales_invoice_id = " . $id,
			'with' => array('salesInvoiceSubItems'),
			'select' => "*",
		));
		$model = $this->loadModel($id);
		$render_datas =  array(
			'model' => $model, 'invoice_categories' => $invoice_categories
		);
		if (isset($_GET['exportpdf'])) {
			$pdfdata = $this->renderPartial('view', $render_datas, true);
			$filename = 'Invoice-No-' . $model->invoice_number . '-' . str_replace(" ", "_", $model->project->name) . '.pdf';
			$mPDF1 = Yii::app()->ePdf->mPDF();
			$mPDF1 = Yii::app()->ePdf->mPDF('', 'A4');
			$mPDF1->WriteHTML($pdfdata);
			$mPDF1->Output($filename, 'D');
		} elseif (isset($_GET['exportexcel'])) {
			$this->redirect(array('salesInvoice/exportExcel', 'id' => $id));
			exit;
		} else {
			$this->render('view', $render_datas);
		}
	}

	public function actionexportExcel($id)
	{
		$arraylabel = array('Company', 'Project Name', 'Client', 'Date', 'Invoice No');
		$invoice_categories = SalesInvoiceItem::model()->findAll(array(
			'condition' => "sales_invoice_id = " . $id,
			'with' => array('salesInvoiceSubItems'),
			'select' => "*",
		));
		$model = $this->loadModel($id);

		$finaldata[0][] = $model->company->name;
		$finaldata[0][] = $model->project->name;;
		$finaldata[0][] = $model->client->name;
		$finaldata[0][] = isset($model->invoice_date) ? date('d-m-Y', strtotime($model->invoice_date)) : '';;
		$finaldata[0][] = isset($model->invoice_number) ? $model->invoice_number : '';
		$finaldata[1][] = '';
		$finaldata[1][] = '';
		$finaldata[1][] = '';
		$finaldata[1][] = '';
		$finaldata[1][] = '';
		$finaldata[2][] = 'Sl.No';
		$finaldata[2][] = 'Description';
		$finaldata[2][] = 'Type';
		$finaldata[2][] = 'Dimension';
		$finaldata[2][] = 'Quantity';
		$finaldata[2][] = 'Unit';
		$finaldata[2][] = 'Rate';
		$finaldata[2][] = 'amount';
		$i = $j = $tot_amount = 0;
		$k = 2;
		foreach ($invoice_categories as $invoice_category) {
			$category_items = $invoice_category->salesInvoiceSubItems;
			$sub_item_count = count($category_items);
			$datas = $invoice_category->attributes;
			extract($datas);
			if ($sub_item_count > 0) {
				$j++;
				$finaldata[$k + 1][] =	sprintf('%02s', $j) . '. ' . $main_title;
				$finaldata[$k + 1][] = '';
				$finaldata[$k + 1][] = '';
				$finaldata[$k + 1][] = '';
				$finaldata[$k + 1][] = '';
				$finaldata[$k + 1][] = '';
				$finaldata[$k + 1][] = '';
				$finaldata[$k + 1][] = '';
				$k++;
			}
			foreach ($category_items as $item) {
				$i++;
				$item_data = $item->attributes;
				extract($item_data);
				$tot_amount += $amount;
				$finaldata[$k + 1][] = $i;
				$finaldata[$k + 1][] = $item_description;
				$finaldata[$k + 1][] = SalesInvoiceSubItem::model()->getType($item->item_type);
				$finaldata[$k + 1][] = SalesInvoiceSubItem::model()->getDimension($item->attributes);
				$finaldata[$k + 1][] = $quantity;
				$finaldata[$k + 1][] = SalesInvoiceSubItem::model()->getUnit($item->attributes);
				$finaldata[$k + 1][] = $rate;
				$finaldata[$k + 1][] = $amount;
				$k++;
			}
		}
		$finaldata[$k + 1][] =	'';
		$finaldata[$k + 1][] =	'';
		$finaldata[$k + 1][] =	'';
		$finaldata[$k + 1][] =	'';
		$finaldata[$k + 1][] =	'';
		$finaldata[$k + 1][] =	'';
		$finaldata[$k + 1][] =	'Total';
		$finaldata[$k + 1][] =	$tot_amount;
		Yii::import('ext.ECSVExport');
		$csv = new ECSVExport($finaldata);
		$csv->setHeaders($arraylabel);
		$csv->setToAppend();
		$contentdata = $csv->toCSV();
		$csvfilename = 'Invoice-No-' . $model->invoice_number . '-' . str_replace(" ", "_", $model->project->name) . date('d_M_Y') . '.csv';
		Yii::app()->getRequest()->sendFile($csvfilename, $contentdata, "text/csv", false);
		exit();
	}


	public function loadModel($id)
	{
		$model = SalesInvoice::model()->findByPk($id);
		if ($model === null)
			throw new CHttpException(404, 'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param SalesInvoice $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'sales-invoice-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
