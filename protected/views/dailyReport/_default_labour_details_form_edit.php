<?php
/* @var $labourDetails array */
/* @var $index int */
?>

<?php if (!empty($labourDetails)): ?>
    <?php foreach ($labourDetails as $index => $detail): ?>
        <div class="row input-group">
            <div class="col-md-3 element-style">
                <p><label style="margin-left:8px;">Labour Type</label></p>
                <select name="Dailyreport[lab_templates][<?php echo $index; ?>][labour_id]" class="labour_worktype form-control select2" onchange="setLabourId(this)">
                    <option value="" disabled>Select Labour</option>
                    <?php foreach ($labourData as $labour): ?>
                        <option value="<?php echo CHtml::encode($labour->type_id); ?>" data-rate="<?php echo CHtml::encode($labour->rate); ?>"
                            <?php echo $labour->type_id == $detail['labour_id'] ? 'selected' : ''; ?>>
                            <?php echo CHtml::encode($labour->worktype); ?>
                        </option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="col-md-3 element-style">
                <p><label>No Of Labours</label></p>
                <input type="number" class="labourcount form-control" min="0" name="Dailyreport[lab_templates][<?php echo $index; ?>][count]" value="<?php echo $detail['labour_count']; ?>" onblur="calculateAmount(this)" onkeyup="validateInput(this)">
                <div class="error"></div>
            </div>
            <div class="col-md-3 element-style">
                <p><label>Amount</label></p>
                <input type="text" class="labour_total_amount form-control" name="Dailyreport[lab_templates][<?php echo $index; ?>][labour_amount]" value="<?php echo $detail['labour_total_rate']; ?>" readonly>
                <div class="error"></div>
            </div>
            <input type="hidden" name="Dailyreport[lab_templates][<?php echo $index; ?>][default_labour_type]" value='1'>
            
            <div class="col-md-2">
           
                <button type="button" class="add-btn btn btn-info btn-sm" onClick="addDynamicRow()">+</button>
           
                <button type="button" class="remove-btn btn btn-danger btn-sm" onClick="handleRemoveButtonClick(this)">-</button>
           
            </div>

           
        </div>
    <?php endforeach; ?>
<?php else: ?>
    <!-- Default empty form if no labourDetails are present -->
    <div class="row input-group">
        <div class="col-md-3 element-style">
            <p><label style="margin-left:8px;">Labour Type</label></p>
            <select name="Dailyreport[lab_templates][0][labour_id]" class="labour_worktype form-control select2" onchange="setLabourId(this)">
                <option value="" disabled>Select Labour Type</option>
                <?php foreach ($labourData as $labour): ?>
                    <option value="<?php echo CHtml::encode($labour->type_id); ?>" data-rate="<?php echo CHtml::encode($labour->rate); ?>">
                        <?php echo CHtml::encode($labour->worktype); ?>
                    </option>
                <?php endforeach; ?>
            </select>
        </div>
        <div class="col-md-3 element-style">
            <p><label>No Of Labours</label></p>
            <input type="number" class="labourcount form-control" name="Dailyreport[lab_templates][0][count]" value="0" onkeyup="calculateAmount(this)">
            <div class="error"></div>
        </div>
        <div class="col-md-3 element-style">
            <p><label>Amount</label></p>
            <input type="text" class="labour_total_amount form-control" name="Dailyreport[lab_templates][0][labour_amount]" readonly>
            <div class="error"></div>
        </div>
        <input type="hidden" name="Dailyreport[lab_templates][0][default_labour_type]" value='1'>
        <div class="col-md-2 ">
            <button type="button" class="add-btn btn btn-info btn-sm" onClick=addDynamicRow()>+</button>
        </div>
    </div>
<?php endif; ?>


