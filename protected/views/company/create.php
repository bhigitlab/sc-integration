<?php
/* @var $this ProjectsController */
/* @var $model Projects */

$this->breadcrumbs = array(
    'Company' => array('index'),
    'Create',
);

?>

<div class="entries-wrapper margin-bottom-10">
    <div class="row">
        <div class="col-xs-12">
            <div class="heading-title">Add Company</div>
            <div class="dotted-line"></div>
        </div>
    </div>
    <?php echo $this->renderPartial('_form', array('model' => $model)); ?>
</div>