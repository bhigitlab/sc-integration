<?php
/* @var $this SiteController */

$this->pageTitle = Yii::app()->name;
?>

<div style="float:left;"><h1 style="text-align: left; margin-left:20px;">BHI-PMS: Dashboard</i></h1></div>


<br clear="all"/>


<div id="dashboard">
    <?php echo CHtml::link('<div class="dicons">' . CHtml::image('images/icons/timesheet.png', '', array('class' => 'dasimage')) . '<span class="dblink">Time sheets</span>
  </div>', array('/timeEntry/index')); ?>  

    <?php if (Yii::app()->user->role == 3) { ?>    
        <?php echo CHtml::link('<div class="dicons">' . CHtml::image('images/icons/task.png', '', array('class' => 'dasimage')) . '<span class="dblink">Tasks</span>
  </div>', array('/tasks/mytask')); ?>    
    <?php } ?>
<?php if (Yii::app()->user->role <= 2) { ?>
<?php echo CHtml::link('<div class="dicons">' . CHtml::image('images/icons/projects.png', '', array('class' => 'dasimage')) . '<span class="dblink">Projects</span>
  </div>', array('/projects/index')); ?>       
 <?php echo CHtml::link('<div class="dicons">' . CHtml::image('images/icons/clients.png', '', array('class' => 'dasimage')) . '<span class="dblink">Clients</span>
  </div>', array('/clients/index')); ?> 
<?php
}
?>
    <?php if (Yii::app()->user->role < 3 || Yii::app()->user->role == 4) { ?>

        <?php echo CHtml::link('<div class="dicons">' . CHtml::image('images/icons/task.png', '', array('class' => 'dasimage')) . '<span class="dblink">Tasks</span>
  </div>', array('/tasks/index')); ?>     

        <?php
        if (Yii::app()->user->role == 1) {
            ?>
            <?php echo CHtml::link('<div class="dicons">' . CHtml::image('images/icons/users.png', '', array('class' => 'dasimage')) . '<span class="dblink">Users</span>
  </div>', array('/users/index')); ?>       

            <?php echo CHtml::link('<div class="dicons">' . CHtml::image('images/icons/settings.png', '', array('class' => 'dasimage')) . '<span class="dblink">Settings</span>
  </div>', '#'); ?>     

            <?php echo CHtml::link('<div class="dicons">' . CHtml::image('images/icons/invoice-icon.png', '', array('class' => 'dasimage')) . '<span class="dblink">Report</span>
  </div>', array('/invoice/report')); ?>     

  <?php echo CHtml::link('<div class="dicons">' . CHtml::image('images/icons/worktype.png', '', array('class' => 'dasimage')) . '<span class="dblink">Work Type</span>
  </div>', array('/workType/index')); ?>

        <?php }
    }

	    if (yii::app()->user->role == 4 OR yii::app()->user->role == 1)
        echo CHtml::link('<div class="dicons">' . CHtml::image('images/icons/payment-icon.gif', '', array('class' => 'dasimage')) . '<br /><span class="dblink">Payments</span>
  </div>', (yii::app()->user->role == 1 ? array('/payments/admin') : array('/payments/index')));
  
  	    if (yii::app()->user->id == 28 OR yii::app()->user->role == 1)
        echo CHtml::link('<div class="dicons">' . CHtml::image('images/icons/electricity.jpg', '', array('class' => 'dasimage')) . '<br /><span class="dblink">Electricity</span>
  </div>', (yii::app()->user->role == 1 ? array('/payments/admin') : array('/electricity/admin')));
  
  
    ?>

    <?php echo CHtml::link('<div class="dicons">' . CHtml::image('images/icons/myprofile.png', '', array('class' => 'dasimage')) . '<div><span class="dblink">My Profile</span></div>
  </div>', array('/users/myprofile')); ?>

    <?php echo CHtml::link('<div class="dicons">' . CHtml::image('images/icons/bug-icon.png', '', array('class' => 'dasimage')) . '<span class="dblink">PMS Bugs</span>
  </div>', array('/issues/index')); ?>

    <?php echo CHtml::link('<div class="dicons">' . CHtml::image('images/icons/logout.png', '', array('class' => 'dasimage')) . '<span class="dblink">Logout</span>
  </div>', array('site/logout')); ?>
    <br clear="all">
</div>
<?php if (Yii::app()->user->role < 3 || Yii::app()->user->role == 4) { ?>
    
    <div class="today_summary">
        <div class="week_sum_heading"> Last 7 days work list</div>
        <?php $this->widget('TodaySummary'); ?>

    </div>
<?php } ?>

<iframe src="https://www.google.com/calendar/embed?height=600&amp;wkst=1&amp;bgcolor=%23FFFFFF&amp;src=bluehorizoninfotech.com_o64ui5j1ti02jqpbae13p2ct80%40group.calendar.google.com&amp;color=%23875509&amp;ctz=Asia%2FCalcutta" style=" border-width:0 " width="500" height="400" frameborder="0" scrolling="no"></iframe>

<br /><br />
<div style="text-align:center;border:1px solid #c1c1c1;">
<iframe width='95%' height='400' frameborder='0' src='https://docs.google.com/a/bluehorizoninfotech.com/spreadsheet/pub?key=0AiPYyXg6QgmUdEhqaFZpVVdQbElxc3RrZ2s4THQ1a2c&output=html' style="margin:10px; float:left;"></iframe>
<br clear="all"/>
</div>
<br /><br />
<br clear="all"/>
<div style="border:1px solid #c2c2c2;margin:10px;text-align:center; padding:10px; margin:0 auto; width:1000px;"><iframe style="margin: 0 auto;" src="https://docs.google.com/a/bluehorizoninfotech.com/document/d/1IsoVH_DU3Y5o7odNRLidHYZADBrhHU4u0rO3oqzKxcw/pub?embedded=true" style=" border-width:0 " width="1000" height="400" frameborder="1" scrolling="auto"></iframe></div>
<?php
/*if(Yii::app()->user->role==1)
{
?>
<iframe width='45%' height='400' frameborder='0' src='https://docs.google.com/a/bluehorizoninfotech.com/spreadsheet/pub?key=0AiPYyXg6QgmUdGlGR3g0dnlQaXBuUWRuRXhHdEYtYVE&output=html'></iframe>
<?php
}
else
{

?>
<iframe width='45%' height='400' frameborder='0' src='https://docs.google.com/a/bluehorizoninfotech.com/spreadsheet/pub?key=0AiPYyXg6QgmUdEdWUDBhTmZtZU1PY1F1ZUlMWlNPWGc&single=true&gid=1&output=html&widget=true'></iframe>
<?php
}*/
?>


