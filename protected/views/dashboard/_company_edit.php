<?php
/* @var $this ExpensesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Vendors',
);
$page = Yii::app()->request->getParam('Vendors_page');
if($page != '') {
	Yii::app()->user->setReturnUrl($page);
} else {
	Yii::app()->user->setReturnUrl(0);
}
?>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<div class="container" id="vendors">
    <div class="clearfix">
        <div class="add-btn pull-right">
           	
        </div>
        <h2>PO Company Edit log</h2>
    </div>
    <div id="vendorform" style="display:none;"></div>

<?php //$this->renderPartial('_search', array('model' => $model)) ?>

        

<div class="">
<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'viewData' => array( 'model' => $model),
	'itemView' => '_company_edit_log', 'template' => '<div class=""><table cellpadding="10" class="table" id="vendortable">{items}</table></div>',
	'ajaxUpdate' => false,
)); ?>


<?php
        Yii::app()->clientScript->registerScript('myjavascript', '
             $(document).ready(function(){
                $("#vendortable").dataTable( {
                    "scrollY": "300px",
                    "scrollCollapse": true,
                    "paging": false,
                     "columnDefs"        : [       
                        { 
                            "searchable"    : false, 
                            "targets"       : [0,3] 
                        },
                        { "bSortable": false, "aTargets": [-1]  }
                    ],
                } );
                var table = $("#vendortable").DataTable();
                $("#Vendors_name").on( "keyup", function () {
                var vendorname = $(this).val();
                    table
                        .columns( 1 )
                        .search( vendorname )
                        .draw();
                } );
                $("#clear").on( "click", function () {
                var selectedText=" ";
                    table
                        .columns( 1 )
                        .search( selectedText )
                        .draw();
                } );
               
                
                
            });

            ');
        ?>



	



</div>
</div>



<style>

.page-body h3{
    margin:4px 0px;
    color:inherit;
    text-align: left;
}
.panel{border:1px solid #ddd;}
.panel-heading{background-color:#eee;height:40px; }

</style>
