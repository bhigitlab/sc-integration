     <style>
      
      table,
      th,
      td,
      tr {
        border-collapse: collapse;
      }
      .w-5 {
        width: 5%;
      }
      .w-10 {
        width: 10%;
      }
      .w-15 {
        width: 15%;
      }
      .w-40 {
        width: 40%;
      }
      .w-50 {
        width: 50%;
      }
      .w-100 {
        width: 100%;
      }
      .border-solid{
        border:1px solid;
      }
      .border-bottom-solid {
        border-bottom: 1px solid;
      }
      .border-top-solid {
        border-top: 1px solid;
      }
      .border-left-solid {
        border-left: 1px solid;
      }
      .text-center {
        text-align: center;
      }
      .text-left {
        text-align: left;
      }
      .text-right {
        text-align: right;
      }
      .whitespace-nowrap {
        white-space: nowrap;
      }
    </style> 
  
  <div class="container"  style="margin:0px 30px">
  <div class="table-div">
  <h2 class="purchase-title">Sales Book</h2>
      <table class="w-100 border-solid">        
          
        <tr>
          <td colspan="2" class="text-left font-14 font-bold">
          <?php 
                $company = Company::model()->findByPk($model->company_id);
                echo $company['name'];
                ?>
          </td>
          <td class="text-left border-left-solid">invoice no. &nbsp;&nbsp;&nbsp;e-Way bill no.</td>
          <td class="text-left border-left-solid">Dated</td>
        </tr>

        <tr>
          <td colspan="2" class="text-left">
            <?php 
                $company = Company::model()->findByPk($model->company_id);
                echo $company['address'];
              ?>
          </td>
          <td class="text-left border-left-solid border-bottom-solid font-bold">
            <?php  echo isset($model->inv_no)?$model->inv_no:''; ?> &nbsp;&nbsp;&nbsp;
          </td>
          <td class="text-left border-left-solid border-bottom-solid font-bold">
            <?php  echo isset($model->date)?date('d-m-Y', strtotime($model->date)):'' ; ?>  
          </td>
        </tr>
        
        <tr>
          <td colspan="2" class="text-left">&nbsp;</td>
          <td class="text-left border-left-solid">Delivery Note</td>
          <td class="text-left border-left-solid">Mode/terms of payment</td>
        </tr>

        <tr>
          <td colspan="2" class="text-left">GSTIN/UIN: 
            <?php 
                $company = Company::model()->findByPk($model->company_id);
                echo $company['company_gstnum'];
              ?></td>
          <td class="text-left border-left-solid border-bottom-solid font-bold"></td>
          <td class="text-left border-left-solid border-bottom-solid font-bold"></td>
        </tr>
        
        <tr>
          <td colspan="2" class="text-left">State Name: Kerala, Code: 32</td>
          <td class="text-left border-left-solid">Reference No. & Date</td>
          <td class="text-left border-left-solid">Other References</td>
        </tr>

        <tr>
          <td colspan="2" class="text-left border-bottom-solid">E-Mail: <?php 
                $company = Company::model()->findByPk($model->company_id);
                echo $company['email_id'];
              ?></td>
          <td class="text-left border-left-solid border-bottom-solid font-bold"></td>
          <td class="text-left border-left-solid border-bottom-solid font-bold"></td>
        </tr>

        <tr>
          <td colspan="2" class="text-left">
            <?php echo isset($project->name)?$project->name:''; ?> </td>
          <td class="text-left border-left-solid">Buyers Order No. </td>
          <td class="text-left border-left-solid">Dated</td>
        </tr>
        
        <tr>
          <td colspan="2" class="text-left font-14 font-bold"></td>
          <td class="text-left border-left-solid border-bottom-solid font-bold"></td>
          <td class="text-left border-left-solid border-bottom-solid font-bold"></td>
        </tr>

        <tr>
          <td colspan="2" class="text-left font-14"> </td>
          <td class="text-left border-left-solid">Dispatch Doc No. </td>
          <td class="text-left border-left-solid">Delivery Note Date </td>
        </tr>
        
        <tr>
          <td colspan="2" class="text-left">GSTIN/UIN : </td>
          <td class="text-left border-left-solid border-bottom-solid font-bold"></td>
          <td class="text-left border-left-solid border-bottom-solid font-bold"></td>
        </tr>

        <tr>
          <td colspan="2" class="text-left">State Name : Kerala, Code: 32</td>
          <td class="text-left border-left-solid">Dispatched through </td>
          <td class="text-left border-left-solid">Destination </td>
        </tr>
        
        <tr>
          <td colspan="2" class="text-left"></td>
          <td class="text-left border-left-solid border-bottom-solid font-bold">&nbsp;</td>
          <td class="text-left border-left-solid border-bottom-solid font-bold">&nbsp;</td>
        </tr>
        
        <tr>
          <td colspan="2" rowspan="5" class="text-left"></td>
          <td colspan="2" class="text-left border-left-solid">Terms of delivery</td>
        </tr>

        <tr><td class="text-left border-left-solid">&nbsp;</td></tr>

        <tr><td class="text-left border-left-solid">&nbsp;</td></tr>

        <tr><td class="text-left border-left-solid">&nbsp;</td></tr>

        <tr><td class="text-left border-left-solid border-bottom-solid font-bold">&nbsp;</td></tr>

      </table>

      <div id="table-scroll" class="table-scroll">
        <div id="faux-table" class="faux-table" aria="hidden"></div>
            <div id=" table-wrap" class="table-wrap">
                <div class="table-responsive">
                    <table border="1" class="table">
                        <thead>
                            <tr>
                                <th>Sl.No</th>
                                <th>Description</th>
                                <th>HSN Code</th>
                                <?php
                                if($model->type == 'quantity_rate') {
                                ?>
                                <th>Quantity</th>
                                <th>Unit</th>
                                <th>Rate</th>
                                <?php } ?>
                                <th>SGST (%)</th>
                                <th>SGST Amount</th>
                                <th>CGST (%)</th>
                                <th>CGST Amount</th>
                                <th>IGST (%)</th>
                                <th>IGST Amount</th>
                                <th>Amount</th>
                                <th>Tax Amount</th>
                            </tr> 
                        </thead>
                        <tbody class="addrow">
                            <?php
                            if(!empty($itemmodel))
                            {
                                foreach($itemmodel as $i => $value) 
                                {
                            ?>

                            <tr  class="tr_class">
                                <td><input type="hidden" name="sl_No[]" value="<?php echo $i+1;?>"> <?php echo $i+1;?></td>
                                <td><input type="hidden" name="description[]" value=""><?php echo $value->description;?> </td>
                                <td><input type="hidden" name="description[]" value=""><?php echo $value->hsn_code;?> </td>
                                <?php
                                if($model->type == 'quantity_rate') {
                                ?>
                                    <td class="text-right"><input type="hidden" name="quantity[]" value="<?php echo $value->quantity;?>"> <?php echo $value->quantity;?> </td>
                                    <td><input type="hidden" name="unit[]" value="<?php echo $value->unit;?>" readonly><?php echo $value->unit;?></td>
                                    <td class="text-right"><input type="hidden" name="rate[]" value="<?php echo $value->rate;?>" readonly><?php echo Controller::money_format_inr($value->rate,2,1);?></td>
                                <?php } ?>
                                <td class="text-right"><?php echo number_format($value->sgst,2);?></td>
                                <td class="text-right"><?php echo Controller::money_format_inr($value->sgst_amount,2,1);?></td>
                                <td class="text-right"><?php echo number_format($value->cgst,2);?></td>
                                <td class="text-right"><?php echo Controller::money_format_inr($value->cgst_amount,2,1);?></td>
                                <td class="text-right"><?php echo number_format($value->igst,2);?></td>
                                <td class="text-right"><?php echo Controller::money_format_inr($value->igst_amount,2,1);?></td>
                                <td class="text-right"><input type="hidden" name="amount[]" value="<?php echo $value->amount;?>" readonly><?php echo Controller::money_format_inr($value->amount,2,1);?></td>
                                <td class="text-right"><input type="hidden" name="amount[]" value="<?php echo $value->amount;?>" readonly><?php echo Controller::money_format_inr($value->tax_amount,2,1);?></td>
                            </tr>
                            <?php
                            }	
                            }
                            ?>
                        </tbody>
                        <tfoot>
                            <?php
                            $colspan1 = 9;
                            $colspan2 = 11;
                            if($model->type == 'quantity_rate') {
                                $colspan1 = 12;
                                $colspan2 = 14;
                            }
                            ?>
			                <tr>
                                <th style="text-align: right;" colspan="<?php echo $colspan1; ?>" class="text-right">TOTAL: </th>
                                <th class="text-right"><div id="grand_total"><?php echo number_format($model->amount, 2); ?></div></th>
                                <th class="text-right"><div id="grand_total"><?php echo number_format($model->tax_amount, 2); ?></div></th>
                            </tr>
                            <tr>
                                <th colspan="<?php echo $colspan2-1; ?>" class="text-right">ROUND OFF:</th>
                                <th class="text-right">
                                    <?php echo number_format($model->round_off, 2, '.', '') ?>
                                </th>
			                </tr>
			                <tr>
                                <th colspan="<?php echo $colspan2-1; ?>" class="text-right">GRAND TOTAL:</th>
                                <th class="text-right"><div id="net_amount"><?php echo number_format($model->tax_amount+$model->amount+($model->round_off), 2, '.', '') ?></div></th>
			                </tr>
                            <tr>
                                <th colspan="<?php echo $colspan2-1; ?>" class="text-right">WRITE OFF:</th>
                                <th class="text-right"><div id="net_amount"><?php echo number_format($writeoff->amount, 2, '.', '') ?></div></th>
			                </tr>
                            <tr>
                                <th colspan="<?php echo $colspan2-1; ?>" class="text-right">BALANCE:</th>
                                <th class="text-right"><div id="net_amount"><?php echo number_format(($model->tax_amount+$model->amount+($model->round_off)) - $writeoff->amount, 2, '.', '') ?></div></th>
			                </tr>
                    </tfoot>
                </table>
            </div>
	    </div>
    </div>
    <table class="w-100 border-solid">
        <tr>
          <td width="40%">Amount Chargeable (in words)</td>
          <td class="font-italic text-right">E.&O.E </td>
        </tr> 

        <tr>
          <td class="font-14 font-bold" colspan="2">INR 
            <?php echo Yii::app()->Controller->displaywords($model->amount); ?> 
          </td>
        </tr>

        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td width="40%">Tax Amount (in words) : </td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td class="font-bold font-14" colspan="2">INR 
          <?php echo Yii::app()->Controller->displaywords($model->tax_amount); ?></td>
        </tr>

        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>

        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>

        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        
      </table>

      <table class="w-100 border-solid">

        <tr>
          <td class="w-50 text-underline">Declaration</td>
          <td class="w-50 text-right border-top solid border-left-solid font-bold font-14">for <?php echo Yii::app()->name;?> </td>
        </tr>

        <tr>
          <td rowspan="2" class="w-50">We declare that this invoice shows the actual price of 
          the goods described and that all particulars are true and correct. </td>
          <td class="w-50 border-left-solid font-bold font-14">&nbsp;</td>
        </tr>

        <tr>
          <td class="w-50 text-right border-left-solid">Authorized Signatory</td>
        </tr>
        
      </table>      
      <h6 class="text-center">This is a Computer Generated Invoice</h6>
    </div>
    </div>
