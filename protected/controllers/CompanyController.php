<?php

class CompanyController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		/* return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','newlist'),
				'users'=>array('@'),
                                'expression' => 'yii::app()->user->role <=1',
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
                                'expression' => 'yii::app()->user->role <=1',

			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		); */
            $accessArr = array();
           $accessauthArr = array();
           $controller = Yii::app()->controller->id;
           if(isset(Yii::app()->user->menuall)){
               if (array_key_exists($controller, Yii::app()->user->menuall)) {
                   $accessArr = Yii::app()->user->menuall[$controller];
               } 
           }

           if(isset(Yii::app()->user->menuauth)){
               if (array_key_exists($controller, Yii::app()->user->menuauth)) {
                   $accessauthArr = Yii::app()->user->menuauth[$controller];
               }
           }
           $access_privlg = count($accessauthArr);
           return array(
               array('allow', // allow all users to perform 'index' and 'view' actions
                   'actions' => $accessArr,
                   'users' => array('@'),
                   'expression' => "$access_privlg > 0",
               ),

               array('allow', // allow admin user to perform 'admin' and 'delete' actions
                   'actions' => $accessauthArr,
                   'users' => array('@'),
                   'expression' => "$access_privlg > 0",
               ),
               array('deny',  // deny all users
                   'users'=>array('*'),
               ),
           );
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Company;
		$model->expense_calculation = null; 

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);
		if(isset($_POST['Company']))
		{
			//echo "<pre>";print_r($_POST['Company']);exit;
			$model->attributes = $_POST['Company'];
            $model->created_by = yii::app()->user->id;
            $model->created_date = date('Y-m-d H:i:s');
            if (!empty($_POST['Company']['po_email_userid'])) {
				$userid = implode(",",$_POST['Company']['po_email_userid']);
				$model->po_email_userid = $userid;
			} else {
				$model->po_email_userid = NULL;
			}
			if (!empty($_POST['Company']['subco_email_userid'])) {
				$userid = implode(",",$_POST['Company']['subco_email_userid']);
				$model->subco_email_userid = $userid;
			} else {
				$model->subco_email_userid = NULL;
			}
                        if (!empty($_POST['Company']['purchase_amount'])) {
				$model->purchase_amount = $_POST['Company']['purchase_amount'];
			} else {
				$model->purchase_amount = NULL;
			} 
                        if (!empty($_POST['Company']['expenses_email'])) {
				$model->expenses_email = $_POST['Company']['expenses_email'];
			} else {
				$model->expenses_email = NULL;
			} 
                        if (!empty($_POST['Company']['expenses_percentage'])) {
				$model->expenses_percentage = $_POST['Company']['expenses_percentage'];
			} else {
				$model->expenses_percentage = NULL;
			}
                        if (!empty($_POST['Company']['invoice_email_userid'])) {
				$userid = implode(",",$_POST['Company']['invoice_email_userid']);
				$model->invoice_email_userid = $userid;
			} else {
				$model->invoice_email_userid = NULL;
			}
			if (!empty($_POST['Company']['bank_details'])) {
				$model->bank_details = $_POST['Company']['bank_details'];
			} else {
				$model->bank_details = NULL;
			}
			$image_name = isset($_FILES['Company']['name']['logo'])?$_FILES['Company']['name']['logo']:'';
            $uploadedFile = CUploadedFile::getInstance($model, 'logo');
                         
            if ($image_name) {
                $filename                   = strtotime("now").'p'.$uploadedFile->getExtensionName();					
            	$newfilename                = rand(1000, 9999) . time();
                $newfilename                = md5($newfilename); //optional
            	$extension                  = $uploadedFile->getExtensionName();
                $model->logo = $newfilename . "." . $extension;
            }
			if($uploadedFile){
            	$image_name = $newfilename . "." . $extension;
            }
			if (!empty($_POST['Company']['invoice_format'])) {
				$model->invoice_format = $_POST['Company']['invoice_format'];
			} else {
				$model->invoice_format = NULL;
			}
			if (!empty($_POST['Company']['sc_quotation_format'])) {
				$model->sc_quotation_format = $_POST['Company']['sc_quotation_format'];
			} else {
				$model->sc_quotation_format = NULL;
			}
			if (!empty($_POST['Company']['purchase_order_format'])) {
				$model->purchase_order_format = $_POST['Company']['purchase_order_format'];
			} else {
				$model->purchase_order_format = NULL;
			}
			if (!empty($_POST['Company']['material_requisition_fomat'])) {
				$model->material_requisition_fomat = $_POST['Company']['material_requisition_fomat'];
			} else {
				$model->material_requisition_fomat = NULL;
			}
			if (!empty($_POST['Company']['warehouse_receipt_format'])) {
				$model->warehouse_receipt_format = $_POST['Company']['warehouse_receipt_format'];
			} else {
				$model->warehouse_receipt_format = NULL;
			}
            if ($model->save()) {
				if($uploadedFile){
					$images_path = realpath(Yii::app()->basePath . '/../uploads/image');
					$file_upload =  $uploadedFile->saveAs($images_path . '/' .$image_name);
					if(!$file_upload){
						Yii::app()->user->setFlash('error', 'Image Error');
						$this->redirect(array('newList'));
					
					}
				}
				if(isset($_REQUEST['popup'])){
					$company_id_val = $model->id;;
					$comma_company_id = ',' . $company_id_val;

					$sql = "UPDATE jp_users
						SET `company_id` = IF(
							company_id = '',
						$company_id_val,
						CONCAT(`company_id`, '$comma_company_id'))
						WHERE userid=".Yii::app()->user->id;
					Yii::app()->db->createCommand($sql)->execute();
				}
				$this->redirect(array('newList'));
			}
		}
                if(isset($_GET['layout']))
                $this->layout = false;

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		if(isset($_GET['layout']))
                $this->layout = false;

		$model=$this->loadModel($id);
		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['Company']))
		{
			$model->attributes=$_POST['Company'];
			if (!empty($_POST['Company']['po_email_userid'])) {
				$userid = implode(",",$_POST['Company']['po_email_userid']);
				$model->po_email_userid = $userid;
			} else {
				$model->po_email_userid = NULL;
			}  
			if (!empty($_POST['Company']['subco_email_userid'])) {
				$userid = implode(",",$_POST['Company']['subco_email_userid']);
				$model->subco_email_userid = $userid;
			} else {
				$model->subco_email_userid = NULL;
			} 
                        if (!empty($_POST['Company']['purchase_amount'])) {
				$model->purchase_amount = $_POST['Company']['purchase_amount'];
			} else {
				$model->purchase_amount = NULL;
			}
                        if (!empty($_POST['Company']['expenses_email'])) {
				$model->expenses_email = $_POST['Company']['expenses_email'];
			} else {
				$model->expenses_email = NULL;
			} 
                        if (!empty($_POST['Company']['expenses_percentage'])) {
				$model->expenses_percentage = $_POST['Company']['expenses_percentage'];
			} else {
				$model->expenses_percentage = NULL;
			}
                        if (!empty($_POST['Company']['invoice_email_userid'])) {
				$userid = implode(",",$_POST['Company']['invoice_email_userid']);
				$model->invoice_email_userid = $userid;
			} else {
				$model->invoice_email_userid = NULL;
			}if (!empty($_POST['Company']['invoice_email_userid'])) {
				$userid = implode(",",$_POST['Company']['invoice_email_userid']);
				$model->invoice_email_userid = $userid;
			} else {
				$model->invoice_email_userid = NULL;
			}if (!empty($_POST['Company']['bank_details'])) {
				$bank_details = $_POST['Company']['bank_details'];
				
				$model->bank_details = $_POST['Company']['bank_details'];
			} else {
				$model->bank_details = NULL;
			}
			if (!empty($_POST['Company']['invoice_format'])) {
				$model->invoice_format = $_POST['Company']['invoice_format'];
			} else {
				$model->invoice_format = NULL;
			}
			if (!empty($_POST['Company']['sc_quotation_format'])) {
				$model->sc_quotation_format = $_POST['Company']['sc_quotation_format'];
			} else {
				$model->sc_quotation_format = NULL;
			}
			if (!empty($_POST['Company']['purchase_order_format'])) {
				$model->purchase_order_format = $_POST['Company']['purchase_order_format'];
			} else {
				$model->purchase_order_format = NULL;
			}
			if (!empty($_POST['Company']['material_requisition_fomat'])) {
				$model->material_requisition_fomat = $_POST['Company']['material_requisition_fomat'];
			} else {
				$model->material_requisition_fomat = NULL;
			}
			if (!empty($_POST['Company']['warehouse_receipt_format'])) {
				$model->warehouse_receipt_format = $_POST['Company']['warehouse_receipt_format'];
			} else {
				$model->warehouse_receipt_format = NULL;
			}
			$image_name = isset($_FILES['Company']['name']['logo'])?$_FILES['Company']['name']['logo']:'';
			$uploadedFile = CUploadedFile::getInstance($model, 'logo');
                         
                            if ($image_name) {
                                $filename                   = strtotime("now").'p'.$uploadedFile->getExtensionName();					
                                $newfilename                = rand(1000, 9999) . time();
                                $newfilename                = md5($newfilename); //optional
                                $extension                  = $uploadedFile->getExtensionName();
                                }
			if($uploadedFile){
            	$image_name = $newfilename . "." . $extension;
            }
			if ($image_name!='') {
				$model->logo = $image_name;
			}else if(isset($_POST['Company']['logo'])){
				$model->logo = $_POST['Company']['logo'];
			}
                            
			if ($model->save()) {
				if($uploadedFile){
					$images_path = realpath(Yii::app()->basePath . '/../uploads/image');
					$file_upload =  $uploadedFile->saveAs($images_path . '/' .$image_name);
					if(!$file_upload){
						Yii::app()->user->setFlash('error', 'Image Error');
						$this->redirect(array('newList'));
					
					}
				} 
				$this->redirect(array('newList'));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Company');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}
	
	public function actionNewlist(){
		$model = new Company('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Company']))
            $model->attributes = $_GET['Company'];
		
        if (Yii::app()->user->role == 1) {
            $this->render('newlist', array(
                'model' => $model,
                'dataProvider' => $model->search(),
            ));
        } else {

            $this->render('newlist', array(
                'model' => $model,
                'dataProvider' => $model->search(Yii::app()->user->id),
            ));
        }
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Company('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Company']))
			$model->attributes=$_GET['Company'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Company the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Company::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Company $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='company-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	public function actionremovecompany() {

        $id = $_REQUEST['id'];
        $model = $this->loadModel($id);
        $transaction = Yii::app()->db->beginTransaction();
        try {
			$sql = "SELECT COUNT(*) FROM `jp_users` "
			    . " WHERE FIND_IN_SET($id, company_id)";
			$user_company = Yii::app()->db->createCommand($sql)->queryScalar();
			if($user_company > 0){
				$success_status = 0;
                throw new Exception('Cannot Delete! Already assigned to a user');
			}
            if (!$model->delete()) {
                $success_status = 0;
                throw new Exception('Some Error Occured');
            } else {
                $success_status = 1;
            }
            $transaction->commit();
        } catch (Exception $error) {
            $transaction->rollBack();
            $success_status = 0;
        } finally {
            if ($success_status == 1) {
                echo json_encode(array('response' => 'success', 'msg' => 'Data Deleted Successfully'));
            } else {
                if (isset($error->errorInfo) && $error->errorInfo[1] == 1451) {
                    echo json_encode(array('response' => 'warning', 'msg' => 'Cannot Delete ! This record alredy in use.'));
                } else {
					echo json_encode(array('response' => 'danger', 'msg' => $error->getMessage()));                    
                }
            }
        }
    }

}
