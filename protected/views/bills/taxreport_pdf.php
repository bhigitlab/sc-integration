<?php
$tblpx = Yii::app()->db->tablePrefix; 
$user = Users::model()->findByPk(Yii::app()->user->id);
$arrVal = explode(',', $user->company_id);
$newQuery = "";
$newQuery1 = "";
$newQuery2 = "";
$newQuery3 = "";
$newQuery4 = "";
$newQuery5 = "";

if($company_id == NULL){
    foreach($arrVal as $arr) {
        if ($newQuery) $newQuery .= ' OR';
        if ($newQuery1) $newQuery1 .= ' OR';
        if ($newQuery2) $newQuery2 .= ' OR';
        if ($newQuery3) $newQuery3 .= ' OR';
        if ($newQuery4) $newQuery4 .= ' OR';
        if ($newQuery5) $newQuery5 .= ' OR';
        $newQuery .= " FIND_IN_SET('".$arr."', {$tblpx}bills.company_id)";
        $newQuery1 .= " FIND_IN_SET('".$arr."', {$tblpx}expenses.company_id)";
        $newQuery2 .= " FIND_IN_SET('".$arr."', {$tblpx}dailyexpense.company_id)";
        $newQuery3 .= " FIND_IN_SET('".$arr."', {$tblpx}dailyvendors.company_id)";
        $newQuery4 .= " FIND_IN_SET('".$arr."', {$tblpx}invoice.company_id)";
        $newQuery5 .= " FIND_IN_SET('".$arr."', {$tblpx}purchase_return.company_id)";
    }
}else {
    $newQuery .= " FIND_IN_SET('".$company_id."', {$tblpx}bills.company_id)";
    $newQuery1 .= " FIND_IN_SET('".$company_id."', {$tblpx}expenses.company_id)";
    $newQuery2 .= " FIND_IN_SET('".$company_id."', {$tblpx}dailyexpense.company_id)";
    $newQuery3 .= " FIND_IN_SET('".$company_id."', {$tblpx}dailyvendors.company_id)";
    $newQuery4 .= " FIND_IN_SET('".$company_id."', {$tblpx}invoice.company_id)";
    $newQuery5 .= " FIND_IN_SET('".$company_id."', {$tblpx}purchase_return.company_id)";
}

if($company_id != NULL){
    $company = Company::model()->findbyPk($company_id);
    $company_name = $company->name;
}else{
    $company_name = "";
}

// total amount calculation purchase
				
$purchase_totalamount = 0;
foreach($purchase as $key=> $data) {
	if($data['type'] =='bills') {
	 $item_amount = Yii::app()->db->createCommand("SELECT sum(billitem_amount) as billitem_amount FROM {$tblpx}billitem WHERE bill_id=".$data['id']." AND ((billitem_cgstpercent > 0 AND billitem_sgstpercent > 0) OR (billitem_igstpercent > 0))")->queryRow();
	 $item_discount = Yii::app()->db->createCommand("SELECT sum(billitem_discountamount) as billitem_discountamount FROM {$tblpx}billitem WHERE bill_id=".$data['id']." AND ((billitem_cgstpercent > 0 AND billitem_sgstpercent > 0) OR (billitem_igstpercent > 0))")->queryRow();
	 $item_taxamount = Yii::app()->db->createCommand("SELECT sum(billitem_taxamount) as billitem_taxamount FROM {$tblpx}billitem WHERE bill_id=".$data['id']." AND ((billitem_cgstpercent > 0 AND billitem_sgstpercent > 0) OR (billitem_igstpercent > 0))")->queryRow();
	 //Controller::money_format_inr($data['bill_totalamount'],2,1)
	 $total_amount = ($item_amount['billitem_amount']- $item_discount['billitem_discountamount'])+$item_taxamount['billitem_taxamount'];
	 } else if($data['type'] =='daybook') {
		 $total_amount = $data['totalamount'];
	 } else if($data['type'] =='dailyexpense') {
		 $total_amount = $data['totalamount'];
	 }  else if($data['type'] =='vendorpayment') {
		$item_taxamount = Yii::app()->db->createCommand("SELECT tax_amount FROM {$tblpx}dailyvendors WHERE 	daily_v_id=".$data['id']."")->queryRow();
		$total_amount = $data['totalamount']+$item_taxamount['tax_amount']; 
	 }
	 $purchase_totalamount += $total_amount;
}

// total amount calculation sales

$sales_totalamount = 0;
foreach($sales as $key=> $value) {
	$sales_totalamount += $value['totalamount'];
}
?>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/pdfstyle.css">

<?php
    $company_address = Company::model()->findBypk(($company_id)?$company_id:Yii::app()->user->company_id);	
?>
   
<h3 class="text-center">Tax Report</h3>
<div class="container pdf_spacing">
<style>
  
.lefttdiv{
        float: left;
        }
        
        .purchase-title{
		border-bottom:1px solid #ddd;
	}
		
	.purchase_items h3{
		font-size:18px;
		color:#333;
		margin:0px;
		padding:0px;
		text-align:left;
		padding-bottom:10px;
	}
	.purchase_items{
		padding:15px;
		box-shadow:0 0 13px 1px rgba(0,0,0,0.25);
	}
	.purchaseitem {
		display: inline-block;
		margin-right: 20px;
	}
	.purchaseitem last-child{margin-right:0px;}
	.purchaseitem label{
		font-size:12px;
	}
	.remark{display:none;}
	.padding-box{padding:3px 0px; min-height:17px; display:inline-block;}
	th{height:auto;}
	.quantity, .rate{max-width:80px;}
	
	.text_align{
		
		text-align: center;
		}
		
		
		*:focus{
		border:1px solid #333;
		box-shadow:0 0 6px 1px rgba(0,0,0,0.25);
	}
	.text-right{
		text-align: right;
		}
</style>
<header class="headerinv">
<header>	
	
	<?php
	if(!empty($purchase)){
	?>
	<h2>Purchase &nbsp;<?php echo (($company_name != ''))?' - '.$company_name:''; ?> - <?php echo  $date_from.' to '.$date_to; ?></h2>
		 <div class="table-responsive">
                <table border="1" class="table">
            <thead>
                <tr>
                    <th>Sl No.</th>
					<th>Payment mode</th>
					<th>Bill date</th>
					<th>Vendor</th>
					<th>GST No</th>
					<th>Commodity</th>
					<th>Commodity Code</th>
					<th>Bill no</th>
					<th>Bill amount</th>
					<?php
					foreach($purchase_head as $key_h => $value_h){
						echo '<th>Tax.val</th>';
						echo '<th>CGST '.$value_h['cgstpercent'].' %</th>';
						echo '<th>SGST '.$value_h['sgstpercent'].' %</th>';
					} 
					?>
					<?php
					foreach($purchase_head_igst as $key_h => $value_h){
						echo '<th>Tax.val</th>';
						echo '<th>IGST '.$value_h['igstpercent'].' %</th>';
					}
					?>
					<th>Other changers</th>
					<th>Net amount</th>
                </tr> 
				<tr>
					   <th colspan="8"></th>
                        <th><?php echo ($purchase_totalamount !='')?Controller::money_format_inr($purchase_totalamount,2,1):'0.00'; ?></th>
                        <?php
						 $net_amount= 0;
						 $net_taxamount = 0;
						 $net_sgst =0;
						 $net_cgst = 0;
						 foreach($purchase_head as $key1 => $value1){
							//if($data['type'] =='bills') {
								$row_cgst1 = Yii::app()->db->createCommand("SELECT SUM(billitem_cgst) as cgst FROM {$tblpx}billitem LEFT JOIN {$tblpx}bills ON {$tblpx}bills.bill_id ={$tblpx}billitem.bill_id WHERE {$tblpx}bills.bill_date BETWEEN '".$date_from."' AND '".$date_to."' AND FORMAT(billitem_cgstpercent,2) = FORMAT(".$value1['cgstpercent'].",2) AND FORMAT(billitem_sgstpercent,2) = FORMAT(".$value1['sgstpercent'].",2) AND billitem_cgstpercent > 0 AND billitem_sgstpercent > 0 AND (".$newQuery.")")->queryRow();
								$row_sgst1 = Yii::app()->db->createCommand("SELECT SUM(billitem_sgst) as sgst FROM {$tblpx}billitem  LEFT JOIN {$tblpx}bills ON {$tblpx}bills.bill_id ={$tblpx}billitem.bill_id WHERE {$tblpx}bills.bill_date BETWEEN '".$date_from."' AND '".$date_to."' AND FORMAT(billitem_cgstpercent,2) = FORMAT(".$value1['cgstpercent'].",2) AND FORMAT(billitem_sgstpercent,2) = FORMAT(".$value1['sgstpercent'].",2) AND billitem_cgstpercent > 0 AND billitem_sgstpercent > 0 AND (".$newQuery.")")->queryRow();
								$row_tax1 = Yii::app()->db->createCommand("SELECT SUM(billitem_amount) as tax_amount FROM {$tblpx}billitem LEFT JOIN {$tblpx}bills ON {$tblpx}bills.bill_id ={$tblpx}billitem.bill_id WHERE {$tblpx}bills.bill_date BETWEEN '".$date_from."' AND '".$date_to."' AND FORMAT(billitem_cgstpercent,2) = FORMAT(".$value1['cgstpercent'].",2) AND FORMAT(billitem_sgstpercent,2) = FORMAT(".$value1['sgstpercent'].",2) AND billitem_cgstpercent > 0 AND billitem_sgstpercent > 0 AND (".$newQuery.")")->queryRow();
								$row_discount = Yii::app()->db->createCommand("SELECT SUM(billitem_discountamount) as discount_amount FROM {$tblpx}billitem  LEFT JOIN {$tblpx}bills ON {$tblpx}bills.bill_id ={$tblpx}billitem.bill_id WHERE {$tblpx}bills.bill_date BETWEEN '".$date_from."' AND '".$date_to."' AND FORMAT(billitem_cgstpercent,2) = FORMAT(".$value1['cgstpercent'].",2) AND FORMAT(billitem_sgstpercent,2) = FORMAT(".$value1['sgstpercent'].",2) AND billitem_cgstpercent > 0 AND billitem_sgstpercent > 0 AND (".$newQuery.")")->queryRow();
								$tax_amount1 = ($row_tax1['tax_amount']-$row_discount['discount_amount']);
								$bill_cgst = $row_cgst1['cgst'];
								$bill_sgst = $row_sgst1['sgst'];
								
								$row_cgst2 = Yii::app()->db->createCommand("SELECT SUM(expense_cgst) as cgst FROM {$tblpx}expenses WHERE date BETWEEN '".$date_from."' AND '".$date_to."' AND FORMAT(expense_cgstp,2) = FORMAT(".$value1['cgstpercent'].",2) AND FORMAT(expense_sgstp,2) = FORMAT(".$value1['sgstpercent'].",2) AND (".$newQuery1.") AND {$tblpx}expenses.bill_id IS NULL AND {$tblpx}expenses.invoice_id IS NULL AND {$tblpx}expenses.type=73 AND (expense_sgstp > 0 AND expense_cgstp > 0)")->queryRow();
								$row_sgst2 = Yii::app()->db->createCommand("SELECT SUM(expense_sgst) as sgst FROM {$tblpx}expenses WHERE date BETWEEN '".$date_from."' AND '".$date_to."'  AND FORMAT(expense_cgstp,2) = FORMAT(".$value1['cgstpercent'].",2) AND FORMAT(expense_sgstp,2) = FORMAT(".$value1['sgstpercent'].",2) AND (".$newQuery1.") AND {$tblpx}expenses.bill_id IS NULL AND {$tblpx}expenses.invoice_id IS NULL AND {$tblpx}expenses.type=73 AND (expense_sgstp > 0 AND expense_cgstp > 0)")->queryRow();
								$row_tax2 = Yii::app()->db->createCommand("SELECT SUM(expense_amount) as tax_amount FROM {$tblpx}expenses WHERE date BETWEEN '".$date_from."' AND '".$date_to."'  AND FORMAT(expense_cgstp,2) = FORMAT(".$value1['cgstpercent'].",2) AND FORMAT(expense_sgstp,2) = FORMAT(".$value1['sgstpercent'].",2) AND (".$newQuery1.") AND {$tblpx}expenses.bill_id IS NULL AND {$tblpx}expenses.invoice_id IS NULL AND {$tblpx}expenses.type=73")->queryRow();
								$tax_amount2 = $row_tax2['tax_amount'];
								$exp_cgst = $row_cgst2['cgst'];
								$exp_sgst = $row_sgst2['sgst'];
								
								$row_cgst3 = Yii::app()->db->createCommand("SELECT SUM(dailyexpense_cgst) as cgst FROM {$tblpx}dailyexpense WHERE date BETWEEN '".$date_from."' AND '".$date_to."' AND FORMAT(dailyexpense_cgstp,2) = FORMAT(".$value1['cgstpercent'].",2) AND FORMAT(dailyexpense_sgstp,2) = FORMAT(".$value1['sgstpercent'].",2) AND (".$newQuery2.") AND {$tblpx}dailyexpense.exp_type=73 AND (dailyexpense_sgstp > 0 AND dailyexpense_cgstp > 0)")->queryRow();
								$row_sgst3 = Yii::app()->db->createCommand("SELECT SUM(dailyexpense_sgst) as sgst FROM {$tblpx}dailyexpense WHERE date BETWEEN '".$date_from."' AND '".$date_to."'  AND FORMAT(dailyexpense_cgstp,2) = FORMAT(".$value1['cgstpercent'].",2) AND FORMAT(dailyexpense_sgstp,2) = FORMAT(".$value1['sgstpercent'].",2) AND (".$newQuery2.") AND {$tblpx}dailyexpense.exp_type=73 AND (dailyexpense_sgstp > 0 AND dailyexpense_cgstp > 0)")->queryRow();
								$row_tax3 = Yii::app()->db->createCommand("SELECT SUM(dailyexpense_amount) as tax_amount FROM {$tblpx}dailyexpense WHERE date BETWEEN '".$date_from."' AND '".$date_to."'  AND FORMAT(dailyexpense_cgstp,2) = FORMAT(".$value1['cgstpercent'].",2) AND FORMAT(dailyexpense_sgstp,2) = FORMAT(".$value1['sgstpercent'].",2) AND (".$newQuery2.") AND {$tblpx}dailyexpense.exp_type=73")->queryRow();
								$tax_amount3 = $row_tax3['tax_amount'];
								$daexp_cgst = $row_cgst3['cgst'];
								$daexp_sgst = $row_sgst3['sgst'];
								
								$row_cgst4 = Yii::app()->db->createCommand("SELECT SUM(cgst_amount) as cgst FROM {$tblpx}dailyvendors WHERE date BETWEEN '".$date_from."' AND '".$date_to."' AND FORMAT(cgst,2) = FORMAT(".$value1['cgstpercent'].",2) AND FORMAT(sgst,2) = FORMAT(".$value1['sgstpercent'].",2) AND (".$newQuery3.") AND {$tblpx}dailyvendors.tax_amount >0 AND sgst >0 AND cgst >0")->queryRow();
								$row_sgst4 = Yii::app()->db->createCommand("SELECT SUM(sgst_amount) as sgst FROM {$tblpx}dailyvendors WHERE date BETWEEN '".$date_from."' AND '".$date_to."'  AND FORMAT(cgst,2) = FORMAT(".$value1['cgstpercent'].",2) AND FORMAT(sgst,2) = FORMAT(".$value1['sgstpercent'].",2) AND (".$newQuery3.") AND {$tblpx}dailyvendors.tax_amount >0 AND sgst >0 AND cgst >0")->queryRow();
								$row_tax4 = Yii::app()->db->createCommand("SELECT SUM(amount) as tax_amount FROM {$tblpx}dailyvendors WHERE date BETWEEN '".$date_from."' AND '".$date_to."'  AND FORMAT(cgst,2) = FORMAT(".$value1['cgstpercent'].",2) AND FORMAT(sgst,2) = FORMAT(".$value1['sgstpercent'].",2) AND (".$newQuery3.") AND sgst >0 AND cgst >0")->queryRow();
								$tax_amount4 = $row_tax4['tax_amount'];
								$vndr_cgst = $row_cgst4['cgst'];
								$vndr_sgst = $row_sgst4['sgst'];
								
								$net_taxamount += $tax_amount1+$tax_amount2+$tax_amount3+$tax_amount4;
								$net_cgst += $bill_cgst+$exp_cgst+$daexp_cgst+$vndr_cgst;
								$net_sgst += $bill_sgst+$exp_sgst+$daexp_sgst+$vndr_sgst;
								
								//print_r($row_cgst);
							///}
							echo '<th>'.Controller::money_format_inr($tax_amount1+$tax_amount2+$tax_amount3+$tax_amount4,2,1).'</th>';
							echo '<th>'.Controller::money_format_inr($bill_cgst+$exp_cgst+$daexp_cgst+$vndr_cgst,2,1).'</th>';
							echo '<th>'.Controller::money_format_inr($bill_sgst+$exp_sgst+$daexp_sgst+$vndr_sgst,2,1).'</th>';
				         }
                        ?>
                        <?php
						  $net_igst = 0;
						  $net_tax  = 0;
						 foreach($purchase_head_igst as $key1 => $value1){ 
								$row_igst1 = Yii::app()->db->createCommand("SELECT SUM(billitem_igst) as igst FROM {$tblpx}billitem LEFT JOIN {$tblpx}bills ON {$tblpx}bills.bill_id ={$tblpx}billitem.bill_id WHERE {$tblpx}bills.bill_date BETWEEN '".$date_from."' AND '".$date_to."' AND FORMAT(billitem_igstpercent,2) = FORMAT(".$value1['igstpercent'].",2) AND (".$newQuery.") AND billitem_igstpercent > 0")->queryRow();
								$row_tax1 = Yii::app()->db->createCommand("SELECT SUM(billitem_amount) as tax_amount FROM {$tblpx}billitem LEFT JOIN {$tblpx}bills ON {$tblpx}bills.bill_id ={$tblpx}billitem.bill_id WHERE {$tblpx}bills.bill_date BETWEEN '".$date_from."' AND '".$date_to."' AND FORMAT(billitem_igstpercent,2) = FORMAT(".$value1['igstpercent'].",2) AND billitem_igstpercent > 0 AND (".$newQuery.")")->queryRow();
								$row_discount = Yii::app()->db->createCommand("SELECT SUM(billitem_discountamount) as discount_amount FROM {$tblpx}billitem  LEFT JOIN {$tblpx}bills ON {$tblpx}bills.bill_id ={$tblpx}billitem.bill_id WHERE {$tblpx}bills.bill_date BETWEEN '".$date_from."' AND '".$date_to."' AND FORMAT(billitem_igstpercent,2) = FORMAT(".$value1['igstpercent'].",2) AND billitem_igstpercent > 0 AND (".$newQuery.")")->queryRow();
								$tax_amount1 = ($row_tax1['tax_amount']-$row_discount['discount_amount']);

								$row_igst2 = Yii::app()->db->createCommand("SELECT SUM(expense_igst) as igst FROM {$tblpx}expenses WHERE date BETWEEN '".$date_from."' AND '".$date_to."' AND FORMAT(expense_igstp,2) = FORMAT(".$value1['igstpercent'].",2) AND (expense_igstp >0) AND {$tblpx}expenses.bill_id IS NULL AND {$tblpx}expenses.invoice_id IS NULL AND {$tblpx}expenses.type=73 AND (".$newQuery1.")")->queryRow();
								$row_tax2 = Yii::app()->db->createCommand("SELECT SUM(expense_amount) as tax_amount FROM {$tblpx}expenses WHERE date BETWEEN '".$date_from."' AND '".$date_to."'  AND FORMAT(expense_igstp,2) = FORMAT(".$value1['igstpercent'].",2) AND (".$newQuery1.") AND {$tblpx}expenses.bill_id IS NULL AND {$tblpx}expenses.invoice_id IS NULL AND {$tblpx}expenses.type=73 AND (expense_igstp >0)")->queryRow();
								$tax_amount2 = $row_tax2['tax_amount'];
								
							    $row_igst3 = Yii::app()->db->createCommand("SELECT SUM(dailyexpense_igst) as igst FROM {$tblpx}dailyexpense WHERE date BETWEEN '".$date_from."' AND '".$date_to."' AND FORMAT(dailyexpense_igstp,2) = FORMAT(".$value1['igstpercent'].",2) AND (dailyexpense_igstp >0) AND {$tblpx}dailyexpense.exp_type=73 AND (".$newQuery2.")")->queryRow();
							    $row_tax3 = Yii::app()->db->createCommand("SELECT SUM(dailyexpense_amount) as tax_amount FROM {$tblpx}dailyexpense WHERE date BETWEEN '".$date_from."' AND '".$date_to."'  AND FORMAT(dailyexpense_igstp,2) = FORMAT(".$value1['igstpercent'].",2) AND (".$newQuery2.") AND {$tblpx}dailyexpense.exp_type=73 AND (dailyexpense_igstp >0)")->queryRow();
								$tax_amount3 = $row_tax3['tax_amount'];
							    
							    
								$row_igst4 = Yii::app()->db->createCommand("SELECT SUM(igst_amount) as igst FROM {$tblpx}dailyvendors WHERE date BETWEEN '".$date_from."' AND '".$date_to."' AND FORMAT(igst,2) = FORMAT(".$value1['igstpercent'].",2) AND (".$newQuery3.") AND igst >0")->queryRow();
								$row_tax4 = Yii::app()->db->createCommand("SELECT SUM(amount) as tax_amount FROM {$tblpx}dailyvendors WHERE date BETWEEN '".$date_from."' AND '".$date_to."'  AND FORMAT(igst,2) = FORMAT(".$value1['igstpercent'].",2) AND (".$newQuery3.") AND igst >0")->queryRow();
								$tax_amount4 = $row_tax4['tax_amount'];

								$net_tax  += $tax_amount1+$tax_amount2+$tax_amount3+$tax_amount4;
								$net_igst += $row_igst2['igst']+$row_igst1['igst']+$row_igst3['igst']+$row_igst4['igst'];
								
							echo '<th>'.Controller::money_format_inr($tax_amount1+$tax_amount2+$tax_amount3+$tax_amount4,2,1).'</th>';
							echo '<th>'.Controller::money_format_inr($row_igst2['igst']+$row_igst1['igst']+$row_igst3['igst']+$row_igst4['igst'],2,1).'</th>';
						}
						?>
                        <th></th>
                        <th><?php echo Controller::money_format_inr(($net_taxamount+$net_cgst+$net_sgst+$net_igst+$net_tax),2,1); ?></th>
				 </tr>
            </thead>
            <tbody class="addrow">
                <?php
                if(!empty($purchase))
                {   $no=0;
                    foreach($purchase as $key=> $data)
                    {
						$no ++;
                ?>

               <tr>
				 <td><?php echo $no; ?></td>
				 <td>
				 <?php
				 if($data['type'] =='bills') {
					 echo "Bills";
				 } else if($data['type'] =='daybook') {
					 echo "Daybook";
				 } else if($data['type'] =='dailyexpense') {
					 echo "Daily Expenses";
				 }  else if($data['type'] =='vendorpayment') {
					 echo "Vendor Payment";
				 }
				 ?>
				 </td>
                         <td><?php echo date('Y-m-d',strtotime($data['date'])); ?> </td>
                         <td><?php echo $data['vendor_name'];?></td>
                         <td><?php echo $data['gst_no'];?></td>
                         <td></td>
                         <td></td>
                         <td><?php echo $data['bill_number'];?></td>
                         <?php
                         $total_amount = 0;
                         if($data['type'] =='bills') {
                         $item_amount = Yii::app()->db->createCommand("SELECT sum(billitem_amount) as billitem_amount FROM {$tblpx}billitem WHERE bill_id=".$data['id']." AND ((billitem_cgstpercent > 0 AND billitem_sgstpercent > 0) OR (billitem_igstpercent > 0))")->queryRow();
                         $item_discount = Yii::app()->db->createCommand("SELECT sum(billitem_discountamount) as billitem_discountamount FROM {$tblpx}billitem WHERE bill_id=".$data['id']." AND ((billitem_cgstpercent > 0 AND billitem_sgstpercent > 0) OR (billitem_igstpercent > 0))")->queryRow();
						 $item_taxamount = Yii::app()->db->createCommand("SELECT sum(billitem_taxamount) as billitem_taxamount FROM {$tblpx}billitem WHERE bill_id=".$data['id']." AND ((billitem_cgstpercent > 0 AND billitem_sgstpercent > 0) OR (billitem_igstpercent > 0))")->queryRow();
                         //Controller::money_format_inr($data['bill_totalamount'],2,1)
                         $total_amount = ($item_amount['billitem_amount']- $item_discount['billitem_discountamount'])+$item_taxamount['billitem_taxamount'];
					     } else if($data['type'] =='daybook') {
							 $total_amount = $data['totalamount'];
						 } else if($data['type'] =='dailyexpense') {
							 $total_amount = $data['totalamount'];
						 }  else if($data['type'] =='vendorpayment') {
							$item_taxamount = Yii::app()->db->createCommand("SELECT tax_amount FROM {$tblpx}dailyvendors WHERE 	daily_v_id=".$data['id']."")->queryRow();
							$total_amount = $data['totalamount']+$item_taxamount['tax_amount']; 
						 }
                         ?>
                         <td><?php  echo Controller::money_format_inr($total_amount,2,1); ?></td>
                         <?php
						 $net_amount= 0;
						 $net_taxamount = 0;
						 $net_sgst =0;
						 $net_cgst = 0;
						 $tax_amount = 0;
						 $purchase_sgst = 0;
						 $purchase_cgst = 0;
						 foreach($purchase_head as $key1 => $value1){
							if($data['type'] =='bills') {
								$row_cgst1 = Yii::app()->db->createCommand("SELECT SUM(billitem_cgst) as cgst FROM {$tblpx}billitem WHERE bill_id=".$data['id']." AND FORMAT(billitem_cgstpercent,2) = FORMAT(".$value1['cgstpercent'].",2) AND FORMAT(billitem_sgstpercent,2) = FORMAT(".$value1['sgstpercent'].",2)")->queryRow();
								$row_sgst1 = Yii::app()->db->createCommand("SELECT SUM(billitem_sgst) as sgst FROM {$tblpx}billitem WHERE bill_id=".$data['id']." AND FORMAT(billitem_cgstpercent,2) = FORMAT(".$value1['cgstpercent'].",2) AND FORMAT(billitem_sgstpercent,2) = FORMAT(".$value1['sgstpercent'].",2)")->queryRow();
								$row_tax1 = Yii::app()->db->createCommand("SELECT SUM(billitem_amount) as tax_amount FROM {$tblpx}billitem WHERE bill_id=".$data['id']." AND FORMAT(billitem_cgstpercent,2) = FORMAT(".$value1['cgstpercent'].",2) AND FORMAT(billitem_sgstpercent,2) = FORMAT(".$value1['sgstpercent'].",2)")->queryRow();
								$row_discount = Yii::app()->db->createCommand("SELECT SUM(billitem_discountamount) as discount_amount FROM {$tblpx}billitem WHERE bill_id=".$data['id']." AND FORMAT(billitem_cgstpercent,2) = FORMAT(".$value1['cgstpercent'].",2) AND FORMAT(billitem_sgstpercent,2) = FORMAT(".$value1['sgstpercent'].",2)")->queryRow();
								$tax_amount = ($row_tax1['tax_amount']-$row_discount['discount_amount']);								
								$purchase_cgst =   $row_cgst1['cgst'];
								$purchase_sgst = $row_sgst1['sgst'];
								
						    } else if($data['type'] =='daybook') {
								
							 	$row_cgst2 = Yii::app()->db->createCommand("SELECT SUM(expense_cgst) as cgst FROM {$tblpx}expenses WHERE exp_id=".$data['id']." AND FORMAT(expense_cgstp,2) = FORMAT(".$value1['cgstpercent'].",2) AND FORMAT(expense_sgstp,2) = FORMAT(".$value1['sgstpercent'].",2) AND (".$newQuery1.") AND {$tblpx}expenses.bill_id IS NULL AND {$tblpx}expenses.invoice_id IS NULL AND {$tblpx}expenses.type=73")->queryRow();
								$row_sgst2 = Yii::app()->db->createCommand("SELECT SUM(expense_sgst) as sgst FROM {$tblpx}expenses WHERE exp_id=".$data['id']." AND FORMAT(expense_cgstp,2) = FORMAT(".$value1['cgstpercent'].",2) AND FORMAT(expense_sgstp,2) = FORMAT(".$value1['sgstpercent'].",2) AND (".$newQuery1.") AND {$tblpx}expenses.bill_id IS NULL AND {$tblpx}expenses.invoice_id IS NULL AND {$tblpx}expenses.type=73")->queryRow();
								$row_taxk = Yii::app()->db->createCommand("SELECT SUM(expense_amount) as tax_amount FROM {$tblpx}expenses WHERE exp_id=".$data['id']." AND FORMAT(expense_cgstp,2) = FORMAT(".$value1['cgstpercent'].",2) AND FORMAT(expense_sgstp,2) = FORMAT(".$value1['sgstpercent'].",2) AND (".$newQuery1.") AND {$tblpx}expenses.bill_id IS NULL AND {$tblpx}expenses.invoice_id IS NULL AND {$tblpx}expenses.type=73")->queryRow();
								$tax_amount = $row_taxk['tax_amount'];
								$purchase_cgst = $row_cgst2['cgst'];
								$purchase_sgst = $row_sgst2['sgst'];
							}  else if($data['type'] =='dailyexpense') {
								
							 	$row_cgst2 = Yii::app()->db->createCommand("SELECT SUM(dailyexpense_cgst) as cgst FROM {$tblpx}dailyexpense WHERE 	dailyexp_id=".$data['id']." AND FORMAT(dailyexpense_cgstp,2) = FORMAT(".$value1['cgstpercent'].",2) AND FORMAT(dailyexpense_sgstp,2) = FORMAT(".$value1['sgstpercent'].",2) AND (".$newQuery2.") AND {$tblpx}dailyexpense.exp_type=73")->queryRow();
								$row_sgst2 = Yii::app()->db->createCommand("SELECT SUM(dailyexpense_sgst) as sgst FROM {$tblpx}dailyexpense WHERE 	dailyexp_id=".$data['id']." AND FORMAT(dailyexpense_cgstp,2) = FORMAT(".$value1['cgstpercent'].",2) AND FORMAT(dailyexpense_sgstp,2) = FORMAT(".$value1['sgstpercent'].",2) AND (".$newQuery2.") AND {$tblpx}dailyexpense.exp_type=73")->queryRow();
								$row_taxk = Yii::app()->db->createCommand("SELECT SUM(dailyexpense_amount) as tax_amount FROM {$tblpx}dailyexpense WHERE 	dailyexp_id=".$data['id']." AND FORMAT(dailyexpense_cgstp,2) = FORMAT(".$value1['cgstpercent'].",2) AND FORMAT(dailyexpense_sgstp,2) = FORMAT(".$value1['sgstpercent'].",2) AND (".$newQuery2.") AND {$tblpx}dailyexpense.exp_type=73")->queryRow();
								$tax_amount = $row_taxk['tax_amount'];
								$purchase_cgst = $row_cgst2['cgst'];
								$purchase_sgst = $row_sgst2['sgst'];
							} else if($data['type'] =='vendorpayment') {
								$row_cgst2 = Yii::app()->db->createCommand("SELECT SUM(cgst_amount) as cgst FROM {$tblpx}dailyvendors WHERE daily_v_id=".$data['id']." AND FORMAT(cgst,2) = FORMAT(".$value1['cgstpercent'].",2) AND FORMAT(sgst,2) = FORMAT(".$value1['sgstpercent'].",2) AND (".$newQuery3.")")->queryRow();
								$row_sgst2 = Yii::app()->db->createCommand("SELECT SUM(sgst_amount) as sgst FROM {$tblpx}dailyvendors WHERE daily_v_id=".$data['id']." AND FORMAT(cgst,2) = FORMAT(".$value1['cgstpercent'].",2) AND FORMAT(sgst,2) = FORMAT(".$value1['sgstpercent'].",2) AND (".$newQuery3.")")->queryRow();
								$row_taxk = Yii::app()->db->createCommand("SELECT SUM(amount) as tax_amount FROM {$tblpx}dailyvendors WHERE daily_v_id=".$data['id']." AND FORMAT(cgst,2) = FORMAT(".$value1['cgstpercent'].",2) AND FORMAT(sgst,2) = FORMAT(".$value1['sgstpercent'].",2) AND (".$newQuery3.")")->queryRow();
								$tax_amount = $row_taxk['tax_amount'];
								$purchase_cgst = $row_cgst2['cgst'];
								$purchase_sgst = $row_sgst2['sgst'];
							}
							
							    $net_taxamount = $tax_amount;
							    
							    $net_sgst = $purchase_sgst;
								$net_cgst = $purchase_cgst;
								$net_amount += $net_taxamount+$purchase_sgst+$purchase_cgst;
						 ?>			
								<td><?php echo (($tax_amount == 0)?'':Controller::money_format_inr($tax_amount,2,1)); ?> </td> 
								<td><?php echo ($purchase_cgst == 0)?'':Controller::money_format_inr($purchase_cgst,2,1); ?></td>
								<td><?php echo ($purchase_sgst == 0)?'':Controller::money_format_inr($purchase_sgst,2,1);?></td>
						<?php }
                         ?>
                         
                          <?php
						 $net_amount_igst= 0;
						 $purchase_igst = 0;
						 foreach($purchase_head_igst as $key1 => $value1){ 
							if($data['type'] =='bills') { 
								$row_igst = Yii::app()->db->createCommand("SELECT SUM(billitem_igst) as igst FROM {$tblpx}billitem WHERE bill_id=".$data['id']." AND FORMAT(billitem_igstpercent,2) = FORMAT(".$value1['igstpercent'].",2)")->queryRow();
								$purchase_igst = $row_igst['igst'];
								$row_tax1 = Yii::app()->db->createCommand("SELECT SUM(billitem_amount) as tax_amount FROM {$tblpx}billitem WHERE bill_id=".$data['id']." AND FORMAT(billitem_igstpercent,2) = FORMAT(".$value1['igstpercent'].",2)")->queryRow();
								$row_discount = Yii::app()->db->createCommand("SELECT SUM(billitem_discountamount) as discount_amount FROM {$tblpx}billitem WHERE bill_id=".$data['id']." AND FORMAT(billitem_igstpercent,2) = FORMAT(".$value1['igstpercent'].",2)")->queryRow();
								$tax_amount = ($row_tax1['tax_amount']-$row_discount['discount_amount']);
							} else if($data['type'] =='daybook') {
								$row_igst = Yii::app()->db->createCommand("SELECT SUM(expense_igst) as igst FROM {$tblpx}expenses WHERE exp_id=".$data['id']." AND FORMAT(expense_igstp,2) = FORMAT(".$value1['igstpercent'].",2) AND {$tblpx}expenses.bill_id IS NULL AND {$tblpx}expenses.invoice_id IS NULL AND {$tblpx}expenses.type=73 AND (".$newQuery1.") AND expense_igst > 0")->queryRow();
								$purchase_igst = $row_igst['igst'];
								$row_taxk = Yii::app()->db->createCommand("SELECT SUM(expense_amount) as tax_amount FROM {$tblpx}expenses WHERE exp_id=".$data['id']." AND FORMAT(expense_igstp,2) = FORMAT(".$value1['igstpercent'].",2) AND (".$newQuery1.") AND {$tblpx}expenses.bill_id IS NULL AND {$tblpx}expenses.invoice_id IS NULL AND {$tblpx}expenses.type=73 AND expense_igst > 0")->queryRow();
								$tax_amount = $row_taxk['tax_amount'];
							} else if($data['type'] =='dailyexpense') {
								$row_igst = Yii::app()->db->createCommand("SELECT SUM(dailyexpense_igst) as igst FROM {$tblpx}dailyexpense WHERE dailyexp_id=".$data['id']." AND FORMAT(dailyexpense_igstp,2) = FORMAT(".$value1['igstpercent'].",2) AND {$tblpx}dailyexpense.exp_type=73 AND (".$newQuery2.")")->queryRow();
								$purchase_igst = $row_igst['igst'];
								$row_taxk = Yii::app()->db->createCommand("SELECT SUM(dailyexpense_amount) as tax_amount FROM {$tblpx}dailyexpense WHERE 	dailyexp_id=".$data['id']." AND FORMAT(dailyexpense_igstp,2) = FORMAT(".$value1['igstpercent'].",2) AND (".$newQuery2.") AND {$tblpx}dailyexpense.exp_type=73")->queryRow();
								$tax_amount = $row_taxk['tax_amount'];
							} else if($data['type'] =='vendorpayment') {
								$row_igst = Yii::app()->db->createCommand("SELECT SUM(igst_amount) as igst FROM {$tblpx}dailyvendors WHERE daily_v_id=".$data['id']." AND FORMAT(igst,2) = FORMAT(".$value1['igstpercent'].",2) AND (".$newQuery3.")")->queryRow();
								$purchase_igst = $row_igst['igst'];
								$row_taxk = Yii::app()->db->createCommand("SELECT SUM(amount) as tax_amount FROM {$tblpx}dailyvendors WHERE daily_v_id=".$data['id']." AND FORMAT(igst,2) = FORMAT(".$value1['igstpercent'].",2) AND (".$newQuery3.")")->queryRow();
								$tax_amount = $row_taxk['tax_amount'];
							}
							
							$net_amount_igst += $purchase_igst+$tax_amount;	 
					    ?>
					    <td><?php  echo ($purchase_igst == 0)?'':Controller::money_format_inr($tax_amount,2,1);?></td>
						<td><?php  echo ($purchase_igst == 0)?'':Controller::money_format_inr($purchase_igst,2,1);?></td>
							 
						<?php
						 } ?>
                         <td></td>
                         <td><?php echo Controller::money_format_inr($net_amount+$net_amount_igst,2); ?></td>
				</tr>
                <?php
                }	
                }
                ?>


            </tbody>
			 <!-- <tfoot>
				 <tr>
					   <th colspan="8"></th>
                        <th><?php echo ($purchase_totalamount !='')?Controller::money_format_inr($purchase_totalamount,2,1):'0.00'; ?></th>
                        <?php
						 $net_amount= 0;
						 $net_taxamount = 0;
						 $net_sgst =0;
						 $net_cgst = 0;
						 foreach($purchase_head as $key1 => $value1){
							//if($data['type'] =='bills') {
								$row_cgst1 = Yii::app()->db->createCommand("SELECT SUM(billitem_cgst) as cgst FROM {$tblpx}billitem LEFT JOIN {$tblpx}bills ON {$tblpx}bills.bill_id ={$tblpx}billitem.bill_id WHERE {$tblpx}bills.bill_date BETWEEN '".$date_from."' AND '".$date_to."' AND FORMAT(billitem_cgstpercent,2) = FORMAT(".$value1['cgstpercent'].",2) AND FORMAT(billitem_sgstpercent,2) = FORMAT(".$value1['sgstpercent'].",2) AND billitem_cgstpercent > 0 AND billitem_sgstpercent > 0 AND (".$newQuery.")")->queryRow();
								$row_sgst1 = Yii::app()->db->createCommand("SELECT SUM(billitem_sgst) as sgst FROM {$tblpx}billitem  LEFT JOIN {$tblpx}bills ON {$tblpx}bills.bill_id ={$tblpx}billitem.bill_id WHERE {$tblpx}bills.bill_date BETWEEN '".$date_from."' AND '".$date_to."' AND FORMAT(billitem_cgstpercent,2) = FORMAT(".$value1['cgstpercent'].",2) AND FORMAT(billitem_sgstpercent,2) = FORMAT(".$value1['sgstpercent'].",2) AND billitem_cgstpercent > 0 AND billitem_sgstpercent > 0 AND (".$newQuery.")")->queryRow();
								$row_tax1 = Yii::app()->db->createCommand("SELECT SUM(billitem_amount) as tax_amount FROM {$tblpx}billitem LEFT JOIN {$tblpx}bills ON {$tblpx}bills.bill_id ={$tblpx}billitem.bill_id WHERE {$tblpx}bills.bill_date BETWEEN '".$date_from."' AND '".$date_to."' AND FORMAT(billitem_cgstpercent,2) = FORMAT(".$value1['cgstpercent'].",2) AND FORMAT(billitem_sgstpercent,2) = FORMAT(".$value1['sgstpercent'].",2) AND billitem_cgstpercent > 0 AND billitem_sgstpercent > 0 AND (".$newQuery.")")->queryRow();
								$row_discount = Yii::app()->db->createCommand("SELECT SUM(billitem_discountamount) as discount_amount FROM {$tblpx}billitem  LEFT JOIN {$tblpx}bills ON {$tblpx}bills.bill_id ={$tblpx}billitem.bill_id WHERE {$tblpx}bills.bill_date BETWEEN '".$date_from."' AND '".$date_to."' AND FORMAT(billitem_cgstpercent,2) = FORMAT(".$value1['cgstpercent'].",2) AND FORMAT(billitem_sgstpercent,2) = FORMAT(".$value1['sgstpercent'].",2) AND billitem_cgstpercent > 0 AND billitem_sgstpercent > 0 AND (".$newQuery.")")->queryRow();
								$tax_amount1 = ($row_tax1['tax_amount']-$row_discount['discount_amount']);
								$bill_cgst = $row_cgst1['cgst'];
								$bill_sgst = $row_sgst1['sgst'];
								
								$row_cgst2 = Yii::app()->db->createCommand("SELECT SUM(expense_cgst) as cgst FROM {$tblpx}expenses WHERE date BETWEEN '".$date_from."' AND '".$date_to."' AND FORMAT(expense_cgstp,2) = FORMAT(".$value1['cgstpercent'].",2) AND FORMAT(expense_sgstp,2) = FORMAT(".$value1['sgstpercent'].",2) AND (".$newQuery1.") AND {$tblpx}expenses.bill_id IS NULL AND {$tblpx}expenses.invoice_id IS NULL AND {$tblpx}expenses.type=73 AND (expense_sgstp > 0 AND expense_cgstp > 0)")->queryRow();
								$row_sgst2 = Yii::app()->db->createCommand("SELECT SUM(expense_sgst) as sgst FROM {$tblpx}expenses WHERE date BETWEEN '".$date_from."' AND '".$date_to."'  AND FORMAT(expense_cgstp,2) = FORMAT(".$value1['cgstpercent'].",2) AND FORMAT(expense_sgstp,2) = FORMAT(".$value1['sgstpercent'].",2) AND (".$newQuery1.") AND {$tblpx}expenses.bill_id IS NULL AND {$tblpx}expenses.invoice_id IS NULL AND {$tblpx}expenses.type=73 AND (expense_sgstp > 0 AND expense_cgstp > 0)")->queryRow();
								$row_tax2 = Yii::app()->db->createCommand("SELECT SUM(expense_amount) as tax_amount FROM {$tblpx}expenses WHERE date BETWEEN '".$date_from."' AND '".$date_to."'  AND FORMAT(expense_cgstp,2) = FORMAT(".$value1['cgstpercent'].",2) AND FORMAT(expense_sgstp,2) = FORMAT(".$value1['sgstpercent'].",2) AND (".$newQuery1.") AND {$tblpx}expenses.bill_id IS NULL AND {$tblpx}expenses.invoice_id IS NULL AND {$tblpx}expenses.type=73")->queryRow();
								$tax_amount2 = $row_tax2['tax_amount'];
								$exp_cgst = $row_cgst2['cgst'];
								$exp_sgst = $row_sgst2['sgst'];
								
								$row_cgst3 = Yii::app()->db->createCommand("SELECT SUM(dailyexpense_cgst) as cgst FROM {$tblpx}dailyexpense WHERE date BETWEEN '".$date_from."' AND '".$date_to."' AND FORMAT(dailyexpense_cgstp,2) = FORMAT(".$value1['cgstpercent'].",2) AND FORMAT(dailyexpense_sgstp,2) = FORMAT(".$value1['sgstpercent'].",2) AND (".$newQuery2.") AND {$tblpx}dailyexpense.exp_type=73 AND (dailyexpense_sgstp > 0 AND dailyexpense_cgstp > 0)")->queryRow();
								$row_sgst3 = Yii::app()->db->createCommand("SELECT SUM(dailyexpense_sgst) as sgst FROM {$tblpx}dailyexpense WHERE date BETWEEN '".$date_from."' AND '".$date_to."'  AND FORMAT(dailyexpense_cgstp,2) = FORMAT(".$value1['cgstpercent'].",2) AND FORMAT(dailyexpense_sgstp,2) = FORMAT(".$value1['sgstpercent'].",2) AND (".$newQuery2.") AND {$tblpx}dailyexpense.exp_type=73 AND (dailyexpense_sgstp > 0 AND dailyexpense_cgstp > 0)")->queryRow();
								$row_tax3 = Yii::app()->db->createCommand("SELECT SUM(dailyexpense_amount) as tax_amount FROM {$tblpx}dailyexpense WHERE date BETWEEN '".$date_from."' AND '".$date_to."'  AND FORMAT(dailyexpense_cgstp,2) = FORMAT(".$value1['cgstpercent'].",2) AND FORMAT(dailyexpense_sgstp,2) = FORMAT(".$value1['sgstpercent'].",2) AND (".$newQuery2.") AND {$tblpx}dailyexpense.exp_type=73")->queryRow();
								$tax_amount3 = $row_tax3['tax_amount'];
								$daexp_cgst = $row_cgst3['cgst'];
								$daexp_sgst = $row_sgst3['sgst'];
								
								$row_cgst4 = Yii::app()->db->createCommand("SELECT SUM(cgst_amount) as cgst FROM {$tblpx}dailyvendors WHERE date BETWEEN '".$date_from."' AND '".$date_to."' AND FORMAT(cgst,2) = FORMAT(".$value1['cgstpercent'].",2) AND FORMAT(sgst,2) = FORMAT(".$value1['sgstpercent'].",2) AND (".$newQuery3.") AND {$tblpx}dailyvendors.tax_amount >0 AND sgst >0 AND cgst >0")->queryRow();
								$row_sgst4 = Yii::app()->db->createCommand("SELECT SUM(sgst_amount) as sgst FROM {$tblpx}dailyvendors WHERE date BETWEEN '".$date_from."' AND '".$date_to."'  AND FORMAT(cgst,2) = FORMAT(".$value1['cgstpercent'].",2) AND FORMAT(sgst,2) = FORMAT(".$value1['sgstpercent'].",2) AND (".$newQuery3.") AND {$tblpx}dailyvendors.tax_amount >0 AND sgst >0 AND cgst >0")->queryRow();
								$row_tax4 = Yii::app()->db->createCommand("SELECT SUM(amount) as tax_amount FROM {$tblpx}dailyvendors WHERE date BETWEEN '".$date_from."' AND '".$date_to."'  AND FORMAT(cgst,2) = FORMAT(".$value1['cgstpercent'].",2) AND FORMAT(sgst,2) = FORMAT(".$value1['sgstpercent'].",2) AND (".$newQuery3.") AND sgst >0 AND cgst >0")->queryRow();
								$tax_amount4 = $row_tax4['tax_amount'];
								$vndr_cgst = $row_cgst4['cgst'];
								$vndr_sgst = $row_sgst4['sgst'];
								
								$net_taxamount += $tax_amount1+$tax_amount2+$tax_amount3+$tax_amount4;
								$net_cgst += $bill_cgst+$exp_cgst+$daexp_cgst+$vndr_cgst;
								$net_sgst += $bill_sgst+$exp_sgst+$daexp_sgst+$vndr_sgst;
								
								//print_r($row_cgst);
							///}
							echo '<th>'.Controller::money_format_inr($tax_amount1+$tax_amount2+$tax_amount3+$tax_amount4,2,1).'</th>';
							echo '<th>'.Controller::money_format_inr($bill_cgst+$exp_cgst+$daexp_cgst+$vndr_cgst,2,1).'</th>';
							echo '<th>'.Controller::money_format_inr($bill_sgst+$exp_sgst+$daexp_sgst+$vndr_sgst,2,1).'</th>';
				         }
                        ?>
                        <?php
						  $net_igst = 0;
						  $net_tax  = 0;
						 foreach($purchase_head_igst as $key1 => $value1){ 
								$row_igst1 = Yii::app()->db->createCommand("SELECT SUM(billitem_igst) as igst FROM {$tblpx}billitem LEFT JOIN {$tblpx}bills ON {$tblpx}bills.bill_id ={$tblpx}billitem.bill_id WHERE {$tblpx}bills.bill_date BETWEEN '".$date_from."' AND '".$date_to."' AND FORMAT(billitem_igstpercent,2) = FORMAT(".$value1['igstpercent'].",2) AND (".$newQuery.") AND billitem_igstpercent > 0")->queryRow();
								$row_tax1 = Yii::app()->db->createCommand("SELECT SUM(billitem_amount) as tax_amount FROM {$tblpx}billitem LEFT JOIN {$tblpx}bills ON {$tblpx}bills.bill_id ={$tblpx}billitem.bill_id WHERE {$tblpx}bills.bill_date BETWEEN '".$date_from."' AND '".$date_to."' AND FORMAT(billitem_igstpercent,2) = FORMAT(".$value1['igstpercent'].",2) AND billitem_igstpercent > 0 AND (".$newQuery.")")->queryRow();
								$row_discount = Yii::app()->db->createCommand("SELECT SUM(billitem_discountamount) as discount_amount FROM {$tblpx}billitem  LEFT JOIN {$tblpx}bills ON {$tblpx}bills.bill_id ={$tblpx}billitem.bill_id WHERE {$tblpx}bills.bill_date BETWEEN '".$date_from."' AND '".$date_to."' AND FORMAT(billitem_igstpercent,2) = FORMAT(".$value1['igstpercent'].",2) AND billitem_igstpercent > 0 AND (".$newQuery.")")->queryRow();
								$tax_amount1 = ($row_tax1['tax_amount']-$row_discount['discount_amount']);

								$row_igst2 = Yii::app()->db->createCommand("SELECT SUM(expense_igst) as igst FROM {$tblpx}expenses WHERE date BETWEEN '".$date_from."' AND '".$date_to."' AND FORMAT(expense_igstp,2) = FORMAT(".$value1['igstpercent'].",2) AND (expense_igstp >0) AND {$tblpx}expenses.bill_id IS NULL AND {$tblpx}expenses.invoice_id IS NULL AND {$tblpx}expenses.type=73 AND (".$newQuery1.")")->queryRow();
								$row_tax2 = Yii::app()->db->createCommand("SELECT SUM(expense_amount) as tax_amount FROM {$tblpx}expenses WHERE date BETWEEN '".$date_from."' AND '".$date_to."'  AND FORMAT(expense_igstp,2) = FORMAT(".$value1['igstpercent'].",2) AND (".$newQuery1.") AND {$tblpx}expenses.bill_id IS NULL AND {$tblpx}expenses.invoice_id IS NULL AND {$tblpx}expenses.type=73 AND (expense_igstp >0)")->queryRow();
								$tax_amount2 = $row_tax2['tax_amount'];
								
							    $row_igst3 = Yii::app()->db->createCommand("SELECT SUM(dailyexpense_igst) as igst FROM {$tblpx}dailyexpense WHERE date BETWEEN '".$date_from."' AND '".$date_to."' AND FORMAT(dailyexpense_igstp,2) = FORMAT(".$value1['igstpercent'].",2) AND (dailyexpense_igstp >0) AND {$tblpx}dailyexpense.exp_type=73 AND (".$newQuery2.")")->queryRow();
							    $row_tax3 = Yii::app()->db->createCommand("SELECT SUM(dailyexpense_amount) as tax_amount FROM {$tblpx}dailyexpense WHERE date BETWEEN '".$date_from."' AND '".$date_to."'  AND FORMAT(dailyexpense_igstp,2) = FORMAT(".$value1['igstpercent'].",2) AND (".$newQuery2.") AND {$tblpx}dailyexpense.exp_type=73 AND (dailyexpense_igstp >0)")->queryRow();
								$tax_amount3 = $row_tax3['tax_amount'];
							    
							    
								$row_igst4 = Yii::app()->db->createCommand("SELECT SUM(igst_amount) as igst FROM {$tblpx}dailyvendors WHERE date BETWEEN '".$date_from."' AND '".$date_to."' AND FORMAT(igst,2) = FORMAT(".$value1['igstpercent'].",2) AND (".$newQuery3.") AND igst >0")->queryRow();
								$row_tax4 = Yii::app()->db->createCommand("SELECT SUM(amount) as tax_amount FROM {$tblpx}dailyvendors WHERE date BETWEEN '".$date_from."' AND '".$date_to."'  AND FORMAT(igst,2) = FORMAT(".$value1['igstpercent'].",2) AND (".$newQuery3.") AND igst >0")->queryRow();
								$tax_amount4 = $row_tax4['tax_amount'];

								$net_tax  += $tax_amount1+$tax_amount2+$tax_amount3+$tax_amount4;
								$net_igst += $row_igst2['igst']+$row_igst1['igst']+$row_igst3['igst']+$row_igst4['igst'];
								
							echo '<th>'.Controller::money_format_inr($tax_amount1+$tax_amount2+$tax_amount3+$tax_amount4,2,1).'</th>';
							echo '<th>'.Controller::money_format_inr($row_igst2['igst']+$row_igst1['igst']+$row_igst3['igst']+$row_igst4['igst'],2,1).'</th>';
						}
						?>
                        <th></th>
                        <th><?php echo Controller::money_format_inr(($net_taxamount+$net_cgst+$net_sgst+$net_igst+$net_tax),2,1); ?></th>
				 </tr>
				 </tfoot> -->
	</table>
	</div>
	
	<?php } ?>
	<br/>
	
	<?php if (!empty($sales)) { ?>
	<h2>Sales &nbsp;<?php echo (($company_name!= ''))?' - '.$company_name:''; ?> - <?php echo  $date_from.' to '.$date_to; ?></h2>
	 <div class="table-responsive">
	<table border="1" class="table">
	  <thead>
    <tr>
        <th>Sl No.</th>
		<th>Date</th>
		<th>Client</th>
		<th>Invoice No</th>
                <th>Return No</th>
		<th>GST No</th>
		<th>Amount</th>
		<?php
		foreach($sales_head as $key_h => $value_h){
			echo '<th>Tax.val</th>';
			echo '<th>CGST '.$value_h['cgst'].' %</th>';
			echo '<th>SGST '.$value_h['sgst'].' %</th>';
			//echo '<th>IGST '.$value_h['igst'].' %</th>';
		}
		?>
		
		
		<?php
		foreach($igst_head_sales as $key_h => $value_h){
			echo '<th>IGST '.$value_h['igst'].' %</th>';
		}
		?>
		
		<th>Net Amount</th>
    </tr>
	<tr>
							<th colspan="6"></th>
							<th><?php echo Controller::money_format_inr($sales_totalamount,2); ?></th>
							<?php
							$net_taxamount = 0;
							$net_sgst = 0;
							$net_cgst = 0;
							$net_finalamount = 0;
							foreach($sales_head as $key_h => $value1){
							
                                                            
							$query = '';
							if($value1['sgst'] == null) {
								$sgst = 0;
								$query .=" AND {$tblpx}inv_list.sgst IS NULL";
							} else{
								$sgst = $value1['sgst'];
								$query .= "AND FORMAT({$tblpx}inv_list.sgst,2) = FORMAT(".$sgst.",2)";
							}
							
							if($value1['cgst'] == null) {
								$cgst = 0;
								$query .=" AND {$tblpx}inv_list.cgst IS NULL";
							} else{
								$cgst = $value1['cgst'];
								$query .= " AND FORMAT({$tblpx}inv_list.cgst,2) = FORMAT(".$cgst.",2)";
							}
							
							//if($data['type'] =='Invoice') {	
                                                            $row_sgst1 = Yii::app()->db->createCommand("SELECT SUM({$tblpx}inv_list.sgst_amount) as sgst FROM {$tblpx}inv_list LEFT JOIN {$tblpx}invoice ON {$tblpx}invoice.invoice_id ={$tblpx}inv_list.inv_id WHERE {$tblpx}invoice.date BETWEEN '".$date_from."' AND '".$date_to."' ".$query." ")->queryRow();
                                                            $row_cgst1 = Yii::app()->db->createCommand("SELECT SUM({$tblpx}inv_list.cgst_amount) as cgst FROM {$tblpx}inv_list LEFT JOIN {$tblpx}invoice ON {$tblpx}invoice.invoice_id ={$tblpx}inv_list.inv_id WHERE {$tblpx}invoice.date BETWEEN '".$date_from."' AND '".$date_to."' ".$query."")->queryRow();
                                                            $row_tax1 = Yii::app()->db->createCommand("SELECT SUM({$tblpx}inv_list.amount) as tax_amount FROM {$tblpx}inv_list LEFT JOIN {$tblpx}invoice ON {$tblpx}invoice.invoice_id ={$tblpx}inv_list.inv_id WHERE {$tblpx}invoice.date BETWEEN '".$date_from."' AND '".$date_to."' ".$query."")->queryRow();
                                                            //$net_finalamount += $row_tax['tax_amount']+$row_cgst['cgst']+$row_sgst['sgst'];
                                                            $sales_taxamount = $row_tax1['tax_amount'];
                                                            $sales_sgst = $row_sgst1['sgst'];
                                                            $sales_cgst = $row_cgst1['cgst'];
                                                        //}else if($data['type'] =='Purchase Return') {
                                                            //echo "jjj";
                                                            $row_cgst2 = Yii::app()->db->createCommand("SELECT SUM(returnitem_cgst) as cgst FROM {$tblpx}purchase_returnitem LEFT JOIN {$tblpx}purchase_return ON {$tblpx}purchase_returnitem.return_id = {$tblpx}purchase_return.return_id  WHERE {$tblpx}purchase_return.return_date BETWEEN '".$date_from."' AND '".$date_to."' AND FORMAT(returnitem_cgstpercent,2) = FORMAT(".$value1['cgst'].",2) AND FORMAT(returnitem_sgstpercent,2) = FORMAT(".$value1['sgst'].",2)")->queryRow();
                                                            $row_sgst2 = Yii::app()->db->createCommand("SELECT SUM(returnitem_sgst) as sgst FROM {$tblpx}purchase_returnitem LEFT JOIN {$tblpx}purchase_return ON {$tblpx}purchase_returnitem.return_id = {$tblpx}purchase_return.return_id  WHERE {$tblpx}purchase_return.return_date BETWEEN '".$date_from."' AND '".$date_to."' AND FORMAT(returnitem_cgstpercent,2) = FORMAT(".$value1['cgst'].",2) AND FORMAT(returnitem_sgstpercent,2) = FORMAT(".$value1['sgst'].",2)")->queryRow();
                                                            $row_tax2 = Yii::app()->db->createCommand("SELECT SUM(returnitem_amount) as tax_amount FROM {$tblpx}purchase_returnitem LEFT JOIN {$tblpx}purchase_return ON {$tblpx}purchase_returnitem.return_id = {$tblpx}purchase_return.return_id  WHERE {$tblpx}purchase_return.return_date BETWEEN '".$date_from."' AND '".$date_to."' AND FORMAT(returnitem_cgstpercent,2) = FORMAT(".$value1['cgst'].",2) AND FORMAT(returnitem_sgstpercent,2) = FORMAT(".$value1['sgst'].",2)")->queryRow();
                                                            $row_discount = Yii::app()->db->createCommand("SELECT SUM(returnitem_discountamount) as discount_amount FROM {$tblpx}purchase_returnitem LEFT JOIN {$tblpx}purchase_return ON {$tblpx}purchase_returnitem.return_id = {$tblpx}purchase_return.return_id  WHERE {$tblpx}purchase_return.return_date BETWEEN '".$date_from."' AND '".$date_to."' AND FORMAT(returnitem_cgstpercent,2) = FORMAT(".$value1['cgst'].",2) AND FORMAT(returnitem_sgstpercent,2) = FORMAT(".$value1['sgst'].",2)")->queryRow();
                                                            $return_taxamount = ($row_tax2['tax_amount']-$row_discount['discount_amount']);								
                                                            $return_cgst =   $row_cgst2['cgst'];
                                                            $return_sgst = $row_sgst2['sgst'];
                                                            
                                                            
                                                            $net_sgst = $sales_sgst+$return_sgst;
                                                            $net_cgst = $sales_cgst+$return_cgst;
                                                            $net_taxamount = $sales_taxamount+$return_taxamount;
                                                            $net_finalamount += $net_taxamount+$net_cgst+$net_sgst;
                                                        //}
							
								echo '<th>'.Controller::money_format_inr($net_taxamount,2,1).'</th>';
								echo '<th>'.Controller::money_format_inr($net_sgst,2,1).'</th>';
								echo '<th>'.Controller::money_format_inr($net_cgst,2,1).'</th>';
							}
							?>
							
							
							<?php
							$net_igst= 0;
							foreach($igst_head_sales as $key_h => $value1){
								$query = '';
								if($value1['igst'] == null) {
									$igst = 0;
									$query .=" AND igst IS NULL";
								} else{
									$igst = $value1['igst'];
									$query .= " AND FORMAT(igst,2) = FORMAT(".$igst.",2)";
								}
								
								$row_igst1 = Yii::app()->db->createCommand("SELECT SUM(igst_amount) as igst FROM {$tblpx}inv_list LEFT JOIN {$tblpx}invoice ON {$tblpx}invoice.invoice_id ={$tblpx}inv_list.inv_id WHERE {$tblpx}invoice.date BETWEEN '".$date_from."' AND '".$date_to."' ".$query." ")->queryRow();
								$sales_igst += $row_igst1['igst'];
                                                                
                                                                $row_igst2 = Yii::app()->db->createCommand("SELECT SUM(returnitem_igst) as igst FROM {$tblpx}purchase_returnitem LEFT JOIN {$tblpx}purchase_return ON {$tblpx}purchase_returnitem.return_id = {$tblpx}purchase_return.return_id  WHERE {$tblpx}purchase_return.return_date BETWEEN '".$date_from."' AND '".$date_to."' AND FORMAT(returnitem_igstpercent,2) = FORMAT(".$value1['igst'].",2)")->queryRow();
                                                                $return_igst = $row_igst2['igst'];
                                                                
                                                                $net_igst = $row_igst2['igst']+$row_igst1['igst'];
                                                                
                                                               
								echo '<th>'.Controller::money_format_inr($row_igst1['igst']+$row_igst2['igst'],2,1).'</th>';
							}
							?>
							
							<th><?php echo Controller::money_format_inr($net_finalamount+$net_igst,2,1); ?></th>
						</tr>
  </thead>

   <tbody>
	     <?php
	     $no=0;
  foreach($sales as $key=> $data) {
	  $no++;
  ?>
 <tr>
				 <td><?php echo $no; ?></td>
				 <td><?php echo date('Y-m-d', strtotime($data['date'])); ?></td>
				<!-- <td><?php // echo $data['project_name']; ?></td> -->
				 <td><?php echo $data['client']; ?></td>
				 <td><?php echo $data['inv_no'];?></td>
                                 <td><?php echo $data['return_no'];?></td>
				 <td><?php echo $data['gst_no'];?></td>
				 <td><?php echo Controller::money_format_inr($data['totalamount'],2,1);?></td>
				 <?php
						$net_amount= 0;
						foreach($sales_head as $key_h => $value1){
     							$query = '';
							if($value1['sgst'] == null) {
								$sgst = 0;
								$query .=" AND sgst IS NULL";
							} else{
								$sgst = $value1['sgst'];
								$query .= "AND FORMAT(sgst,2) = FORMAT(".$sgst.",2)";
							}
							
							if($value1['cgst'] == null) {
								$cgst = 0;
								$query .=" AND cgst IS NULL";
							} else{
								$cgst = $value1['cgst'];
								$query .= " AND FORMAT(cgst,2) = FORMAT(".$cgst.",2)";
							}
							
							/* if($value1['igst'] == null) {
								$igst = 0;
								$query .=" AND igst IS NULL";
							} else{
								$igst = $value1['igst'];
								$query .= " AND FORMAT(igst,2) = FORMAT(".$igst.",2)";
							} */
							if($data['type'] =='Invoice') {
                                                            $row_sgst1 = Yii::app()->db->createCommand("SELECT SUM(sgst_amount) as sgst FROM {$tblpx}inv_list WHERE inv_id=".$data['id']." ".$query." ")->queryRow();
                                                            //$row_igst = Yii::app()->db->createCommand("SELECT SUM(igst_amount) as igst FROM {$tblpx}inv_list WHERE inv_id=".$data['invoice_id']." ".$query." ")->queryRow();
                                                            $row_cgst1 = Yii::app()->db->createCommand("SELECT SUM(cgst_amount) as cgst FROM {$tblpx}inv_list WHERE inv_id=".$data['id']." ".$query."")->queryRow();
                                                            $row_tax = Yii::app()->db->createCommand("SELECT SUM(amount) as tax_amount FROM {$tblpx}inv_list WHERE inv_id=".$data['id']." ".$query."")->queryRow();
                                                            $tax_amount = $row_tax['tax_amount'];
                                                            $sales_cgst = $row_cgst1['cgst'];
							    $sales_sgst = $row_sgst1['sgst'];
                                                            $net_amount += $tax_amount+$sales_cgst+$sales_sgst;
                                                        } else if($data['type'] =='Purchase Return') {
                                                            $row_cgst2 = Yii::app()->db->createCommand("SELECT SUM(returnitem_cgst) as cgst FROM {$tblpx}purchase_returnitem WHERE return_id=".$data['id']." AND FORMAT(returnitem_cgstpercent,2) = FORMAT(".$value1['cgst'].",2) AND FORMAT(returnitem_sgstpercent,2) = FORMAT(".$value1['sgst'].",2)")->queryRow();
                                                            $row_sgst2 = Yii::app()->db->createCommand("SELECT SUM(returnitem_sgst) as sgst FROM {$tblpx}purchase_returnitem WHERE return_id=".$data['id']." AND FORMAT(returnitem_cgstpercent,2) = FORMAT(".$value1['cgst'].",2) AND FORMAT(returnitem_sgstpercent,2) = FORMAT(".$value1['sgst'].",2)")->queryRow();
                                                            $row_tax2 = Yii::app()->db->createCommand("SELECT SUM(returnitem_amount) as tax_amount FROM {$tblpx}purchase_returnitem WHERE return_id=".$data['id']." AND FORMAT(returnitem_cgstpercent,2) = FORMAT(".$value1['cgst'].",2) AND FORMAT(returnitem_sgstpercent,2) = FORMAT(".$value1['sgst'].",2)")->queryRow();
                                                            $row_discount = Yii::app()->db->createCommand("SELECT SUM(returnitem_discountamount) as discount_amount FROM {$tblpx}purchase_returnitem WHERE return_id=".$data['id']." AND FORMAT(returnitem_cgstpercent,2) = FORMAT(".$value1['cgst'].",2) AND FORMAT(returnitem_sgstpercent,2) = FORMAT(".$value1['sgst'].",2)")->queryRow();
                                                            $tax_amount = ($row_tax2['tax_amount']-$row_discount['discount_amount']);								
                                                            $sales_cgst =   $row_cgst2['cgst'];
                                                            $sales_sgst = $row_sgst2['sgst'];
                                                            $net_amount += $tax_amount+$sales_cgst+$sales_sgst;
                                                        }    
				?>
				<td><?php echo (($tax_amount == 0)?'':Controller::money_format_inr($tax_amount,2,1)); ?> </td>
				<td><?php echo ($sales_cgst == 0)?'':Controller::money_format_inr($sales_cgst,2,1); ?></td>
				<td><?php echo ($sales_sgst == 0)?'':Controller::money_format_inr($sales_sgst,2,1); ?></td>
				<!-- <td><?php // echo ($row_igst['igst'] == 0)?'':number_format($row_igst['igst'],2); ?></td> -->
				<?php } ?>
				
				<?php
						$net_amountigst= 0;
						foreach($igst_head_sales as $key_h => $value1){
							
							$query = '';
							if($value1['igst'] == null) {
								$igst = 0;
								$query .=" AND igst IS NULL";
							} else{
								$igst = $value1['igst'];
								$query .= " AND FORMAT(igst,2) = FORMAT(".$igst.",2)";
							}
						if($data['type'] =='Invoice') {	
                                                    $row_igst1 = Yii::app()->db->createCommand("SELECT SUM(igst_amount) as igst FROM {$tblpx}inv_list WHERE inv_id=".$data['id']." ".$query." ")->queryRow();
                                                    $row_tax1 = Yii::app()->db->createCommand("SELECT SUM(amount) as tax_amount FROM {$tblpx}inv_list WHERE inv_id=".$data['id']." ".$query."")->queryRow();
                                                    $net_amountigst += $row_igst1['igst'];
                                                    $sales_igst = $row_igst1['igst'];
                                                 } else if($data['type'] =='Purchase Return') {
                                                    $row_igst2 = Yii::app()->db->createCommand("SELECT SUM(returnitem_igst) as igst FROM {$tblpx}purchase_returnitem WHERE return_id=".$data['id']." AND FORMAT(returnitem_igstpercent,2) = FORMAT(".$value1['igst'].",2)")->queryRow();
                                                    $sales_igst = $row_igst2['igst'];
                                                    $row_tax2 = Yii::app()->db->createCommand("SELECT SUM(returnitem_amount) as tax_amount FROM {$tblpx}purchase_returnitem WHERE return_id=".$data['id']." AND FORMAT(returnitem_igstpercent,2) = FORMAT(".$value1['igst'].",2)")->queryRow();
                                                    $row_discount = Yii::app()->db->createCommand("SELECT SUM(returnitem_discountamount) as discount_amount FROM {$tblpx}purchase_returnitem WHERE return_id=".$data['id']." AND FORMAT(returnitem_igstpercent,2) = FORMAT(".$value1['igst'].",2)")->queryRow();
                                                    $tax_amount = ($row_tax2['tax_amount']-$row_discount['discount_amount']);
                                                    $net_amountigst += $row_igst2['igst'];
                                                 }
							
				?>
				
				<td><?php echo ($sales_igst == 0)?'':Controller::money_format_inr($sales_igst,2,1); ?></td>
				
				
				<?php } ?>
				
				<td><?php echo Controller::money_format_inr($net_amount+$net_amountigst,2); ?></td>
				 </tr>
				 <?php } ?>
				 </tbody>
  <!-- <tfoot>
						<tr>
							<th colspan="6"></th>
							<th><?php echo Controller::money_format_inr($sales_totalamount,2); ?></th>
							<?php
							$net_taxamount = 0;
							$net_sgst = 0;
							$net_cgst = 0;
							$net_finalamount = 0;
							foreach($sales_head as $key_h => $value1){
							
                                                            
							$query = '';
							if($value1['sgst'] == null) {
								$sgst = 0;
								$query .=" AND {$tblpx}inv_list.sgst IS NULL";
							} else{
								$sgst = $value1['sgst'];
								$query .= "AND FORMAT({$tblpx}inv_list.sgst,2) = FORMAT(".$sgst.",2)";
							}
							
							if($value1['cgst'] == null) {
								$cgst = 0;
								$query .=" AND {$tblpx}inv_list.cgst IS NULL";
							} else{
								$cgst = $value1['cgst'];
								$query .= " AND FORMAT({$tblpx}inv_list.cgst,2) = FORMAT(".$cgst.",2)";
							}
							
							//if($data['type'] =='Invoice') {	
                                                            $row_sgst1 = Yii::app()->db->createCommand("SELECT SUM({$tblpx}inv_list.sgst_amount) as sgst FROM {$tblpx}inv_list LEFT JOIN {$tblpx}invoice ON {$tblpx}invoice.invoice_id ={$tblpx}inv_list.inv_id WHERE {$tblpx}invoice.date BETWEEN '".$date_from."' AND '".$date_to."' ".$query." ")->queryRow();
                                                            $row_cgst1 = Yii::app()->db->createCommand("SELECT SUM({$tblpx}inv_list.cgst_amount) as cgst FROM {$tblpx}inv_list LEFT JOIN {$tblpx}invoice ON {$tblpx}invoice.invoice_id ={$tblpx}inv_list.inv_id WHERE {$tblpx}invoice.date BETWEEN '".$date_from."' AND '".$date_to."' ".$query."")->queryRow();
                                                            $row_tax1 = Yii::app()->db->createCommand("SELECT SUM({$tblpx}inv_list.amount) as tax_amount FROM {$tblpx}inv_list LEFT JOIN {$tblpx}invoice ON {$tblpx}invoice.invoice_id ={$tblpx}inv_list.inv_id WHERE {$tblpx}invoice.date BETWEEN '".$date_from."' AND '".$date_to."' ".$query."")->queryRow();
                                                            //$net_finalamount += $row_tax['tax_amount']+$row_cgst['cgst']+$row_sgst['sgst'];
                                                            $sales_taxamount = $row_tax1['tax_amount'];
                                                            $sales_sgst = $row_sgst1['sgst'];
                                                            $sales_cgst = $row_cgst1['cgst'];
                                                        //}else if($data['type'] =='Purchase Return') {
                                                            //echo "jjj";
                                                            $row_cgst2 = Yii::app()->db->createCommand("SELECT SUM(returnitem_cgst) as cgst FROM {$tblpx}purchase_returnitem LEFT JOIN {$tblpx}purchase_return ON {$tblpx}purchase_returnitem.return_id = {$tblpx}purchase_return.return_id  WHERE {$tblpx}purchase_return.return_date BETWEEN '".$date_from."' AND '".$date_to."' AND FORMAT(returnitem_cgstpercent,2) = FORMAT(".$value1['cgst'].",2) AND FORMAT(returnitem_sgstpercent,2) = FORMAT(".$value1['sgst'].",2)")->queryRow();
                                                            $row_sgst2 = Yii::app()->db->createCommand("SELECT SUM(returnitem_sgst) as sgst FROM {$tblpx}purchase_returnitem LEFT JOIN {$tblpx}purchase_return ON {$tblpx}purchase_returnitem.return_id = {$tblpx}purchase_return.return_id  WHERE {$tblpx}purchase_return.return_date BETWEEN '".$date_from."' AND '".$date_to."' AND FORMAT(returnitem_cgstpercent,2) = FORMAT(".$value1['cgst'].",2) AND FORMAT(returnitem_sgstpercent,2) = FORMAT(".$value1['sgst'].",2)")->queryRow();
                                                            $row_tax2 = Yii::app()->db->createCommand("SELECT SUM(returnitem_amount) as tax_amount FROM {$tblpx}purchase_returnitem LEFT JOIN {$tblpx}purchase_return ON {$tblpx}purchase_returnitem.return_id = {$tblpx}purchase_return.return_id  WHERE {$tblpx}purchase_return.return_date BETWEEN '".$date_from."' AND '".$date_to."' AND FORMAT(returnitem_cgstpercent,2) = FORMAT(".$value1['cgst'].",2) AND FORMAT(returnitem_sgstpercent,2) = FORMAT(".$value1['sgst'].",2)")->queryRow();
                                                            $row_discount = Yii::app()->db->createCommand("SELECT SUM(returnitem_discountamount) as discount_amount FROM {$tblpx}purchase_returnitem LEFT JOIN {$tblpx}purchase_return ON {$tblpx}purchase_returnitem.return_id = {$tblpx}purchase_return.return_id  WHERE {$tblpx}purchase_return.return_date BETWEEN '".$date_from."' AND '".$date_to."' AND FORMAT(returnitem_cgstpercent,2) = FORMAT(".$value1['cgst'].",2) AND FORMAT(returnitem_sgstpercent,2) = FORMAT(".$value1['sgst'].",2)")->queryRow();
                                                            $return_taxamount = ($row_tax2['tax_amount']-$row_discount['discount_amount']);								
                                                            $return_cgst =   $row_cgst2['cgst'];
                                                            $return_sgst = $row_sgst2['sgst'];
                                                            
                                                            
                                                            $net_sgst = $sales_sgst+$return_sgst;
                                                            $net_cgst = $sales_cgst+$return_cgst;
                                                            $net_taxamount = $sales_taxamount+$return_taxamount;
                                                            $net_finalamount += $net_taxamount+$net_cgst+$net_sgst;
                                                        //}
							
								echo '<th>'.Controller::money_format_inr($net_taxamount,2,1).'</th>';
								echo '<th>'.Controller::money_format_inr($net_sgst,2,1).'</th>';
								echo '<th>'.Controller::money_format_inr($net_cgst,2,1).'</th>';
							}
							?>
							
							
							<?php
							$net_igst= 0;
							foreach($igst_head_sales as $key_h => $value1){
								$query = '';
								if($value1['igst'] == null) {
									$igst = 0;
									$query .=" AND igst IS NULL";
								} else{
									$igst = $value1['igst'];
									$query .= " AND FORMAT(igst,2) = FORMAT(".$igst.",2)";
								}
								
								$row_igst1 = Yii::app()->db->createCommand("SELECT SUM(igst_amount) as igst FROM {$tblpx}inv_list LEFT JOIN {$tblpx}invoice ON {$tblpx}invoice.invoice_id ={$tblpx}inv_list.inv_id WHERE {$tblpx}invoice.date BETWEEN '".$date_from."' AND '".$date_to."' ".$query." ")->queryRow();
								$sales_igst += $row_igst1['igst'];
                                                                
                                                                $row_igst2 = Yii::app()->db->createCommand("SELECT SUM(returnitem_igst) as igst FROM {$tblpx}purchase_returnitem LEFT JOIN {$tblpx}purchase_return ON {$tblpx}purchase_returnitem.return_id = {$tblpx}purchase_return.return_id  WHERE {$tblpx}purchase_return.return_date BETWEEN '".$date_from."' AND '".$date_to."' AND FORMAT(returnitem_igstpercent,2) = FORMAT(".$value1['igst'].",2)")->queryRow();
                                                                $return_igst = $row_igst2['igst'];
                                                                
                                                                $net_igst = $row_igst2['igst']+$row_igst1['igst'];
                                                                
                                                               
								echo '<th>'.Controller::money_format_inr($row_igst1['igst']+$row_igst2['igst'],2,1).'</th>';
							}
							?>
							
							<th><?php echo Controller::money_format_inr($net_finalamount+$net_igst,2,1); ?></th>
						</tr>
					  </tfoot> -->
  
  </table>
  </div>
	
	<?php } ?>
	
	
</div>


