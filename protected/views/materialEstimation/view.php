<?php
/* @var $this MaterialEstimationController */
/* @var $model MaterialEstimation */

$this->breadcrumbs=array(
	'Material Estimations'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List MaterialEstimation', 'url'=>array('index')),
	array('label'=>'Create MaterialEstimation', 'url'=>array('create')),
	array('label'=>'Update MaterialEstimation', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete MaterialEstimation', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage MaterialEstimation', 'url'=>array('admin')),
);
?>

<h1>View MaterialEstimation #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'project_id',
		'specification',
		'quantity',
		'unit',
		'rate',
		'amount',
		'remarks',
		'created_at',
	),
)); ?>
