<link rel="stylesheet" type="text/css" href="uploads/new_template/css/newstyle.css" />

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="/resources/demos/style.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  
  <script>
  $( function() {
    $( "#datepicker" ).datepicker();
  } );
  </script>

<br/><br/>
	 
<div class="invoicemaindiv" >


               
                 <a style="cursor:pointer; float:right;" id="download" class="save_btn" >
                 <div class="save_pdf"></div>SAVE AS PDF
                 </a><br/><br/>
                
 
 
 
 <form id="pdfvals" method="post" action="<?php echo $this->createAbsoluteUrl('projects/saveperformainvoice'); ?>">

	<div class="invoiceheader">
	  <img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/logo.png" style="height:100px;padding:5px;">
	  <textarea class="txtBox pastweek" name="details" placeholder="Please click to edit" style="float:right;width:50%;height:110px"/>
	  3rd Floor,Dr.Rajkrishnan's Building &#13;
	  Nort Fort Gate,Tripunithura,Kochi &#13;&#10;
	  Call:0484 4030077, 9995107007 &#13;
	  Mail:info@jpventures.in
	  </textarea>
	</div>
	<h2>INVOICE</h2>
	
	<div class="inv_client_det">
	<label>BILL TO : </label>
	<input type="text" class="txtBox pastweek" name="billto" placeholder="Please click to edit"/>
	<label>ROYAL METALLOIDS : </label>
	<input type="text" class="txtBox pastweek" name="royal" placeholder="Please click to edit"/>
	</div>
	
	<div class="rightdiv">
	<table border="1" style="width:100%;">
	<tr>
		<td>
		<label>DATE : </label>
		<input type="text" id="datepicker" class="txtBox pastweek" name="date"  placeholder="Please click to edit" >
		</td>
		</tr>
	<tr>
		<td>
		<label>INVOICE NO : </label>
		<input type="text" class="txtBox pastweek" name="invoiceno" placeholder="Please click to edit"/>
		</td>
	</tr>
	</table>
	</div>
	<br><br><br><br>
	
	<div class="inv_client_det">
	<label>PROJECT NAME : </label>
	<input type="text" class="txtBox pastweek" name="project" placeholder="Please click to edit"/>
	</div>
	<br>
	<br>
	
	<table border="1" style="width:500px">
			<thead>
				<tr>
					<th>Sl.No</th>
					<th>Description</th>
					<th>Quantity</th>
					<th>Unit</th>
					<th>Rate</th>
					<th>Amount</th>
				</tr>
			</thead>
			<tbody class="addrow">
				<tr>
					<td><input type="text" class="txtBox pastweek" name="sl_No[]" placeholder="Please click to edit"/></td>
					<td><textarea class="txtBox pastweek" name="description[]"/>Please click to edit</textarea></td>
					<td><input type="text" class="txtBox pastweek" name="quantity[]" placeholder="Please click to edit"/></td>
					<td><input type="text" class="txtBox pastweek" name="unit[]" placeholder="Please click to edit"/></td>
					<td><input type="text" class="txtBox pastweek" name="rate[]" placeholder="Please click to edit"/></td>
					<td><input type="text" class="txtBox pastweek amt" name="amount[]" placeholder="Please click to edit"/></td>
				</tr>
			</tbody>
	</table>
	
	<div class="addcolumn">+</div>
	
	<table border="1" style="width:100%;">
		<tr>
			<td>SUB TOTAL :<input type="text" class="txtBox pastweek" name="subtot" placeholder="Please click to edit"/></td>
			<td>FEES :<input type="text" class="txtBox pastweek" name="fees" placeholder="Please click to edit"/></td>
			<td>GRAND TOTAL :<input type="text" class="txtBox pastweek grand" name="grand" placeholder="Please click to edit"/></td>
		</tr>
	</table>
	
        
              
</form>

</div>


<script>

    


    $(function () {

        $('.save_btn').click(function () {

            $("form#pdfvals").submit();

        });
        
        $('.addcolumn').on('click', function () {
			
			$('.addrow').append('<tr><td><input type="text" class="txtBox pastweek" name="sl.No[]" placeholder="Please click to edit"/></td><td><input type="text" class="txtBox pastweek" name="description[]" placeholder="Please click to edit"/></td><td><input type="text" class="txtBox pastweek" name="quantity[]" placeholder="Please click to edit"/></td><td><input type="text" class="txtBox pastweek" name="unit[]" placeholder="Please click to edit"/></td><td><input type="text" class="txtBox pastweek" name="rate[]" placeholder="Please click to edit"/></td><td><input type="text" class="txtBox pastweek amt" name="amount[]" placeholder="Please click to edit"/></td> </tr>');
			
		
		 });
		 
		
        
       


        $(".inputSwitch span").on("click", function() {

            var $this = $(this);

            $this.hide().siblings("input").val($this.text()).show();

        });

        $(".inputSwitch input").bind('blur', function() {

                   var $this = $(this);

                   $(this).attr('value', $(this).val());

                   $this.hide().siblings("span").text($this.val()).show();

        }).hide();

 

    });
</script>
