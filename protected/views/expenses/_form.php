<?php
/* @var $this ExpensesController */
/* @var $model Expenses */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'expenses-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'expense_date'); ?>
		<?php echo $form->textField($model,'expense_date'); ?>
		<?php echo $form->error($model,'expense_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'project_id'); ?>
		<?php echo $form->textField($model,'project_id'); ?>
		<?php echo $form->error($model,'project_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'bill_id'); ?>
		<?php echo $form->textField($model,'bill_id'); ?>
		<?php echo $form->error($model,'bill_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'expensetype_id'); ?>
		<?php echo $form->textField($model,'expensetype_id'); ?>
		<?php echo $form->error($model,'expensetype_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'vendor_id'); ?>
		<?php echo $form->textField($model,'vendor_id'); ?>
		<?php echo $form->error($model,'vendor_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'expense_amount'); ?>
		<?php echo $form->textField($model,'expense_amount'); ?>
		<?php echo $form->error($model,'expense_amount'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'expense_sgstp'); ?>
		<?php echo $form->textField($model,'expense_sgstp'); ?>
		<?php echo $form->error($model,'expense_sgstp'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'expense_sgst'); ?>
		<?php echo $form->textField($model,'expense_sgst'); ?>
		<?php echo $form->error($model,'expense_sgst'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'expense_cgstp'); ?>
		<?php echo $form->textField($model,'expense_cgstp'); ?>
		<?php echo $form->error($model,'expense_cgstp'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'expense_cgst'); ?>
		<?php echo $form->textField($model,'expense_cgst'); ?>
		<?php echo $form->error($model,'expense_cgst'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'expense_totalamount'); ?>
		<?php echo $form->textField($model,'expense_totalamount'); ?>
		<?php echo $form->error($model,'expense_totalamount'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'description'); ?>
		<?php echo $form->textField($model,'description',array('size'=>60,'maxlength'=>300)); ?>
		<?php echo $form->error($model,'description'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'receipt_type'); ?>
		<?php echo $form->textField($model,'receipt_type'); ?>
		<?php echo $form->error($model,'receipt_type'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'receipt'); ?>
		<?php echo $form->textField($model,'receipt'); ?>
		<?php echo $form->error($model,'receipt'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'purchase_type'); ?>
		<?php echo $form->textField($model,'purchase_type'); ?>
		<?php echo $form->error($model,'purchase_type'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'paidamount'); ?>
		<?php echo $form->textField($model,'paidamount'); ?>
		<?php echo $form->error($model,'paidamount'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'user_id'); ?>
		<?php echo $form->textField($model,'user_id'); ?>
		<?php echo $form->error($model,'user_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'created_by'); ?>
		<?php echo $form->textField($model,'created_by'); ?>
		<?php echo $form->error($model,'created_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'created_date'); ?>
		<?php echo $form->textField($model,'created_date'); ?>
		<?php echo $form->error($model,'created_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'updated_by'); ?>
		<?php echo $form->textField($model,'updated_by'); ?>
		<?php echo $form->error($model,'updated_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'updated_date'); ?>
		<?php echo $form->textField($model,'updated_date'); ?>
		<?php echo $form->error($model,'updated_date'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->