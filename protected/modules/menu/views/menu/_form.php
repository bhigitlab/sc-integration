<?php
/* @var $this MenuController */
/* @var $model Menu */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'menu-form',
	'enableAjaxValidation'=>false,
)); ?>

	
	<div class="row">
		<div class="subrow col-md-3 col-sm-6">
		<?php echo $form->labelEx($model,'menu_name'); ?>
		<?php echo $form->textField($model,'menu_name',array('class' =>'form-control')); ?>
		<?php echo $form->error($model,'menu_name'); ?>
		</div>
        <div class="subrow col-md-3 col-sm-6">
			<?php echo $form->labelEx($model,'parent_id'); ?>
                <?php echo CHtml::activeDropDownList($model, 'parent_id', Chtml::listData(Menu::model()->findAllByAttributes(array('parent_id'=>"")), 'menu_id', 'menu_name'),
			array('class'=>'form-control','empty'=>'Select Parent Menu Item', 'ajax' => array(
                'type' => 'POST',
                
                'url' => CController::createUrl('menu/getsiblings'),
                'data' => array('parent_id' => 'js:this.value'),
                'success' => 'function(data) {
                        $("#Menu_related").html(data);
			
                     }'
                ))) ?>
		<?php echo $form->error($model,'parent_id'); ?>
		</div>
	</div>
	
	

	
        <?php  /*
        <div class="row">
                <?php echo $form->labelEx($model,'related'); ?>
            
            <?php if($model->isNewRecord){ ?>
            
                <?php echo $form->checkBoxList($model,'related', CHtml::listData(Menu::model()->findAll(array('condition' => 'parent_id=6')),'menu_id','menu_name'), array('template'=>'{input}{label}', 'separator'=>'<br/>', 'labelOptions'=>array('style'=> 'width: 260px;display: inline-block'),)); ?>
                
            <?php } else{
                $parent_id = $model->parent_id;
                $menu_id = $model->menu_id;
                if($parent_id != 0){
                echo $form->checkBoxList($model,'related', CHtml::listData(Menu::model()->findAll(array('condition' => "parent_id=$parent_id and menu_id != $menu_id ")),'menu_id','menu_name'), array('template'=>'{input}{label}', 'separator'=>'<br/>', 'labelOptions'=>array('style'=> 'width: 260px;display: inline-block'),)); 
                
                }
            }?>
            <?php echo $form->error($model,'related'); ?>
        </div>
*/ ?>
    <div class="row">
		<div class="subrow col-md-3 col-sm-6">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->radioButtonList($model, 'status', array('Active', 'Inactive'), array('separator' => '','labelOptions'=>array('style'=>'display:inline'))); ?>
		<?php echo $form->error($model,'status'); ?>
		</div>
		<div class="subrow col-md-3 col-sm-6">
		<?php echo $form->labelEx($model,'controller'); ?>
		<?php echo $form->textField($model,'controller',array('class' =>'form-control')); ?>
		<?php echo $form->error($model,'controller'); ?>
		</div>
	</div>

	 <div class="row">
		<div class="subrow col-md-3 col-sm-6">
		<?php echo $form->labelEx($model,'action'); ?>
		<?php echo $form->textField($model,'action',array('class' =>'form-control')); ?>
		<?php echo $form->error($model,'action'); ?>
		</div>
		<div class="subrow col-md-3 col-sm-6">
		<?php echo $form->labelEx($model,'params'); ?>
		<?php echo $form->textField($model,'params',array('class' =>'form-control')); ?>
		<?php echo $form->error($model,'params'); ?>
		</div>
	</div>

	 <div class="row">
		<div class="subrow col-md-3 col-sm-6">
		<?php echo $form->labelEx($model,'showmenu'); ?>
		<?php echo $form->dropDownList($model,'showmenu',array("0"=>"All Users","1"=>"Guest Users", "2"=>"Authenticated Users"),array('class'=>'form-control','empty'=>'Select Value')); ?>
		<?php echo $form->error($model,'showmenu'); ?>
		</div>
		<div class="subrow col-md-3 col-sm-6">
		<?php echo $form->labelEx($model,'show_list'); ?>
		<?php echo $form->checkBox($model,'show_list'); ?>
		<?php echo $form->error($model,'show_list'); ?>
        </div>
	</div>
        


	<div class="save-btnHold">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=> 'btn-primary')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->

<div style="color:red">
    <h4><?php echo Yii::app()->user->getFlash('error'); ?></h4>
</div>


<style>
	div.form input, div.form textarea, div.form select {
    margin: 0.5em 0;
}
	div.form label {
    display: block;
    font-size: 16px;
    font-weight: normal;
}
</style>


