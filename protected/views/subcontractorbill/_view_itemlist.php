
            <div id="table-scroll" class="table-scroll col-xs-12 subcontractor-bill-table">
               
                <?php  if($billing_pending_quantity >0){ ?>
                    <table border="1" class="table" style="">
                                <thead class="entry-table">
                                <tr bgcolor="#ddd">
                                    <th>Sl.No</th>
                                    <th>Quotation Description</th>
                                    <th>Quotation Quantity</th>
                                    <th>Remaining Quantity</th>
                                    <th>Unit</th>
                                    <th>Executed Quantity</th>
                                    <th>% of completion</th>
                                    <th>Rate</th>
                                    <th>Amount</th>
                                </tr>
                                </thead>
                                <tbody class="addrow">
                                    <?php
                                $i = 1;
                                $l=1;
                                if (!empty($view_datas)) {
                                    $bill_amount =0;
                                    foreach ($view_datas as $view_data) {
                                        $category_details = $view_data['category_details'];
                                        if (!empty($category_details) && isset($view_data['items_list'])) {
                                            $category_id = $category_details['id'];
                                            $category_total = ScquotationItems::model()->getCategoryTotal($category_details['sc_quotaion_id'], $category_id);
                                            
                                            $datas = $view_data['items_list'];
                                            $sql = 'SELECT count(*) as count FROM `jp_scquotation_items` '
                                                    . ' WHERE `item_category_id` = '.$category_id;
                                            $count = Yii::app()->db->createCommand($sql)->queryScalar();
                                            if(isset($_GET['exportpdf'])){
                                                if($count >0 ){
                                            ?>
                                            <tr >
                                                <td colspan="9" >
                                                    <?php echo sprintf("%02d", $i) . '. ' . $category_details['main_title']; ?>
                                                </td>
                                                <!-- <td colspan="5" class="light_bg_grey text-right">
                                                ₹ <?php echo $category_total ?>
                                                </td> -->
                                            </tr>
                                                <?php } }else{?>
                                                    <tr >
                                                        <td colspan="9">
                                                            <?php echo sprintf("%02d", $i) . '. ' . $category_details['main_title']; ?>
                                                        </td>
                                                        <!-- <td colspan="5" class="light_bg_grey text-right">
                                                        ₹ <?php echo $category_total ?>
                                                        </td> -->
                                                    </tr>
                                            <?php } ?>
                                            <?php if ($category_details['main_description']) { ?>
                                                <tr>
                                                    <td colspan="9">
                                                        <?php echo $category_details['main_description']; ?>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                            <?php
                                            $k = 1;
                                            foreach ($datas as $item) {
                                                if(($item['remaining_quantity']>0)||$is_update){
                                                ?>
                                                <tr class="">
                                                <input type="hidden" name="Reminingqty" value="<?=$item['remaining_quantity']; ?>" id="remaining-<?= $item['item_id'] ?>" >

                                                <input type="hidden" name="Subcontractorbill[item][<?=$l;?>][item_type]" value="<?=$item['item_type']; ?>" >
                                                <input type="hidden" name="Subcontractorbill[item][<?=$l;?>][item_description]" value="<?=$item['item_description']; ?>" >
                                                <?php if($item['item_type']=='1'){ ?>
                                                <input type="hidden" name="Subcontractorbill[item][<?=$l;?>][item_amount]" class="item_amount-<?= $item['item_id']; ?>" value="<?=($is_update)?($item['item_rate']*$item['executed_quantity']):($item['item_rate']*$item['remaining_quantity']); ?>" >
                                                <?php }else{ ?>
                                                    <input type="hidden" name="Subcontractorbill[item][<?=$l;?>][item_amount]" class="item_amount-<?= $item['item_id']; ?>" value="<?= $item['item_amount']; ?>" >
                                                <?php } ?>
                                                <input type="hidden" name="Subcontractorbill[item][<?=$l;?>][item_category_id]" value="<?=$category_id; ?>" >
                                                <input type="hidden" name="Subcontractorbill[item][<?=$l;?>][item_category_id]" value="<?=$category_id; ?>" >
                                                <input type="hidden" name="Subcontractorbill[item][<?=$l;?>][item_id]" class="form-control" value="<?php echo $item['item_id']; ?>">
                                                <input type="hidden" name="Subcontractorbill[item][<?=$l;?>][item_rate]" class="form-control" id="item_rate-<?= $item['item_id']; ?>" value="<?php echo $item['item_rate']; ?>">
                                                <input type="hidden" name="Subcontractorbill[item][<?=$l;?>][item_quantity]" class="form-control" id="item_quantity-<?= $item['item_id']; ?>" value="<?php echo $item['item_quantity']; ?>">
                                                <input type="hidden" name="Subcontractorbill[item][<?=$l;?>][remaining_quantity]" class="remaining_qty-<?= $item['item_id']; ?>" id="remaining_qty-<?= $item['item_id']; ?>" value="<?php echo $item['remaining_quantity']; ?>">
                                                <input type="hidden" name="Subcontractorbill[item][<?=$l;?>][executed_quantity]" class="executed_quantity-<?= $item['item_id']; ?>" value="<?php echo ($is_update)?$item['executed_quantity']:$item['remaining_quantity']; ?>">
                                                <input type="hidden" name="Subcontractorbill[item][<?=$l;?>][completed_percentage]" class="percentage_completion-<?= $item['item_id']; ?>" value="100">
                                                <input type="hidden" name="Subcontractorbill[item][<?=$l;?>][billed_qty]" class="billed_qty-<?= $item['item_id']; ?>" id="billed_qty-<?= $item['item_id']; ?>" value="<?php echo $item['billed_quantity']; ?>">
                                                <input type="hidden" name="Subcontractorbill[item][<?=$l;?>][current_bill_qty]" class="current_bill_qty-<?= $item['item_id']; ?>" id="current_bill_qty-<?= $item['item_id']; ?>" value="<?php echo $item['remaining_quantity']; ?>">
                                                <?php if($is_update){ ?>
                                                    <input type="hidden" name="Subcontractorbill[item][<?=$l;?>][billed_qty]" class="billed_qty-<?= $item['item_id']; ?>" id="billed_qty-<?= $item['item_id']; ?>" value="<?php echo $item['billed_quantity']; ?>">
                                                
                                                    <input type="hidden" name="Subcontractorbill[item][<?=$l;?>][current_bill_qty]" class="current_bill_qty-<?= $item['item_id']; ?>" id="current_bill_qty-<?= $item['item_id']; ?>" value="<?php echo $item['executed_quantity']; ?>">
                                                    <input type="hidden" name="Subcontractorbill[item][<?=$l;?>][bill_item_id]" class="bill_item_id-<?= $item['item_id']; ?>" value="<?= $item['id']; ?>">
                                                <?php } ?>
                                                     <?php if (($item['item_amount'] != 0 && !$is_update)||$is_update) { ?>
                                                        <td><?php echo $k; ?></td>
                                                    <?php } ?>

                                                    <?php if (($item['item_amount'] == 0 && !$is_update)) { ?>
                                                        <td colspan="9"><?php echo $item['item_description']; ?></td>
                                                    <?php } else { ?>
                                                        <td><?php echo $item['item_description']; ?></td>
                                                    <?php } ?>

                                                    <?php
                                                    if (((empty($item['item_quantity'])) && (empty($item['item_rate'])))) {
                                                        ?>
                                                        <?php if ($item['item_amount'] != 0) { ?>
                                                            
                                                            <td>-</td>
                                                            <td>-</td>
                                                            <td ><?php echo 'Lumpsum'; ?></td>
                                                            <td>-</td>
                                                            <td><p id="completed_percentage-<?= $item['item_id']; ?>"><?= Yii::app()->Controller->money_format_inr((1)*100,2) ?></p></td>
                                                            <td>-</td>
                                                        <?php } ?>
                                                    <?php } else { ?>
                                                        <?php if (($item['item_amount'] != 0 && !$is_update)||$is_update) { ?>
                                                            <td><p id="item_quantity-"<?= $item['item_id'];?>><?php echo $item['item_quantity']; ?></p></td>
                                                            <td><label id="remaining_quantity-<?= $item['item_id'];?>" class="remaining_qty-<?= $item['item_id']; ?>" ><?=($is_update)?($item['remaining_quantity']-$item['executed_quantity']):($item['remaining_quantity']-$item['remaining_quantity']) ?></label></td>
                                                            <td><?=($item['item_type']=='1')?'Quantity*rate':'Lumpsum'; ?></td>

                                                            <td><input type="text" class="executed_quantity form-control" min="0" max="<?= $item['remaining_quantity']; ?>" id="executed_quantity-<?= $item['item_id']; ?>" value="<?= ($is_update==1)?$item['executed_quantity']:$item['remaining_quantity']; ?>"></td>
                                                            <?php
                                                            if($is_update){ ?>
                                                            <td><p id="completed_percentage-<?= $item['item_id']; ?>"><?= Yii::app()->Controller->money_format_inr(($item['executed_quantity']/$item['item_quantity'])*100,2) ?></p></td>
                                                            <?php }else{
                                                            ?><td><p id="completed_percentage-<?= $item['item_id']; ?>"><?= Yii::app()->Controller->money_format_inr(($item['item_quantity']/$item['item_quantity'])*100,2) ?></p></td>
                                                            <?php } ?>
                                                            <td class="text-right">₹ <?php echo $item['item_rate']; ?></td>
                                                            <?php  
                                                            if($is_update){
                                                                $bill_amount=$bill_amount+($item['item_rate']*$item['executed_quantity']);  
                                                            }else{
                                                                $bill_amount=$bill_amount+($item['item_rate']*$item['remaining_quantity']); 
                                                            }
                                                            ?>
                                                            
                                                         <input type="hidden" id="item_sum-<?= $item['item_id']; ?>" value="<?php echo ($is_update)?($item['item_rate']*$item['executed_quantity']):($item['item_rate']*$item['remaining_quantity']); ?>">
                                                        </br>
                                                        <?php } ?>
                                                    <?php }
                                                            if($item['item_type']==2){
                                                                $bill_amount=$bill_amount+$item['item_amount'];
                                                            } ?>
                                                    <?php if (($item['item_amount'] != 0 &&  !$is_update)|| $is_update) { ?>
                                                        <td class="text-right"><p id="item_amount-<?= $item['item_id']; ?>">₹ <?php echo isset($item['item_amount']) ? Yii::app()->Controller->money_format_inr($item['item_amount'], 2) : '0.00'; ?></p>
                                                        <?php } ?>
                                                    </td>
                                                </tr>
                                                <?php
                                                if (($item['item_amount'] != 0 &&  !$is_update)|| $is_update) {
                                                    $k++;
                                                    $l++;
                                                }
                                            }
                                            }
                                            
                                            if(isset($_GET['exportpdf'])){
                                                if($count >0){
                                                    $i++;
                                                }
                                            }else{
                                                $i++;
                                            }                                            
                                        } ?>
                                        <?php          
                                    }?>
                                    <input type="hidden" name="Subcontractorbill[bill_sum]" id="bill_sum" class="bill_sum" value="<?php echo $bill_amount; ?>">
                                     
                                <?php }
                                ?>
                    </tbody>
                </table>
                <?php } ?>
                      
            </div>

<script>
     $(document).ready(function() {
        var billing_pending_quantity ="<?php echo $billing_pending_quantity; ?>";
        var current_bill_amount=$(".bill_sum").val();
        if(billing_pending_quantity==0){
            current_bill_amount =0;
            //$("#submitButton").attr('disabled', 'disabled');
            $("#errormessage").show()
                    .html('<div class="alert alert-danger">items already billed</div>');
        }else{
            $("#errormessage").hide();
            //$("#submitButton").removeAttr('disabled');;
        }
        $(".total_amount").val(parseFloat(current_bill_amount).toFixed(2));
     });

    $(".executed_quantity").on('change', function () {
        var text =this.id;
        var item_id=text.split("-")[1];
        var new_qty=this.value;
        var old_qty= $(".executed_quantity-"+item_id).val();
       
        //executed qty checking
        var reamining_qty = $("#remaining-"+item_id).val();
        if(parseFloat(new_qty) > parseFloat(reamining_qty)){
           alert('Executed quantity exceeds remaining quantity');
           new_qty = old_qty;
        }

        var diff =old_qty-new_qty;
        
        var quotation_qty = $("#item_quantity-"+item_id).val();
        var completed_percentage =  parseFloat((new_qty/quotation_qty)*100).toFixed( 2 );
        $("#completed_percentage-"+item_id).html(completed_percentage);
        $(".percentage_completion-"+item_id).val(completed_percentage);


        //percentage calculation
        var quotation_qty = $("#item_quantity-"+item_id).val();
        var completed_percentage =  parseFloat((new_qty/quotation_qty)*100).toFixed( 2 );
        $("#completed_percentage-"+item_id).html(completed_percentage);
        $(".percentage_completion-"+item_id).val(completed_percentage);
        //percentage calculation ends
        //remaining quantity        
        if($("#billed_qty-"+item_id).val()>0)
            {
                var item_rem = parseFloat(quotation_qty)-parseFloat($("#billed_qty-"+item_id).val());  
                var qty_diff = parseFloat(item_rem)-parseFloat($(".executed_quantity-"+item_id).val());
                var qty_diff_amount = (qty_diff+diff).toFixed(2);
                $("#remaining_quantity-"+item_id).html(qty_diff_amount);
                $(".remaining_qty-"+item_id).val(qty_diff_amount);
            }else{
                var item_rem = parseFloat(quotation_qty)-parseFloat(new_qty).toFixed(2);
                $("#remaining_quantity-"+item_id).html(item_rem.toFixed(2));
                $(".remaining_qty-"+item_id).val(item_rem.toFixed(2));
            }

        $(".executed_quantity-"+item_id).val(new_qty);
        $("#executed_quantity-"+item_id).val(new_qty);
        var current_bill_amount=$(".bill_sum").val();
        
        var item_rate = $("#item_rate-"+item_id).val();
        var item_amount = new_qty*item_rate;
        var current_item_amount =$("#item_sum-"+item_id).val();
        var item_amount_difference = item_amount-current_item_amount;
        $("#item_amount-"+item_id).html('₹ '+ parseFloat(item_amount).toFixed(2));
        $(".item_amount-"+item_id).val(parseFloat(item_amount).toFixed(2));
        $("#item_sum-"+item_id).val(item_amount);
        
        var final_bill_amount = parseFloat(parseFloat(current_bill_amount) + parseFloat(item_amount_difference));
        $("#bill_sum").val(final_bill_amount);
        $(".total_amount").val(parseFloat(final_bill_amount).toFixed(2));
});
</script>