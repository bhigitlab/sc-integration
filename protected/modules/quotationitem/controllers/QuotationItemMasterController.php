<?php

class QuotationItemMasterController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        $accessArr = array();
        $accessauthArr = array();
        $controller = 'quotationitem/' . Yii::app()->controller->id;

        if (isset(Yii::app()->user->menuall)) {
            if (array_key_exists($controller, Yii::app()->user->menuall)) {
                $accessArr = Yii::app()->user->menuall[$controller];
            }
        }

        if (isset(Yii::app()->user->menuauth)) {
            if (array_key_exists($controller, Yii::app()->user->menuauth)) {
                $accessauthArr = Yii::app()->user->menuauth[$controller];
            }
        }
        $access_privlg = count($accessauthArr);
        return array(
            array(
                'allow', // allow all users to perform 'index' and 'view' actions
                'actions' => $accessArr,
                'users' => array('@'),
                'expression' => "$access_privlg > 0",
            ),
            array(
                'allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => $accessauthArr,
                'users' => array('@'),
                'expression' => "$access_privlg > 0",
            ),
            array(
                'allow', // allow admin user to perform 'admin' and 'delete' actions
                //'actions' => array(''),
                'users' => array('@'),
            ),
            array(
                'deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionIndex($id = false) {
        $model = new QuotationItemMaster('search');
        $model->unsetAttributes();  // clear any default values
        $item_model = QuotationItemMaster::model()->findAll(array('order' => 'quotation_category_id ASC'));
        $item_datas = $this->setQuotationsList($item_model);
        $this->render('admin', array(
            'model' => $model,
            'item_datas' => $item_datas,
        ));
    }

    public function setQuotationsList($items_model) {
        $items = array();
        foreach ($items_model as $item) {
            $items[] = $item->attributes;
        }
        $lists = array();
        $data_items = array();
        $data = array();

        foreach ($items as $key => $item) {
            array_push($data_items, $item);
            $data = array(
                'items_list' => $data_items
            );
        }
        if (!empty($data)) {
            array_push($lists, $data);
        }
        return $lists;
    }

    public function actionAdmin() {
        $dataProvider = new CActiveDataProvider('QuotationItemMaster');
        $this->render('admin', array(
            'dataProvider' => $dataProvider,
        ));
    }

    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    public function actionAdditem() {
        $model = new QuotationItemMaster('search');
        $model->unsetAttributes();  // clear any default values
        $itemdata = $this->getItem();
        $itemfinish = $this->getItemFinish();
        $this->render('create', array(
            'model' => $model,
            'itemdata' => $itemdata,
            'itemfinish' => $itemfinish
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {

        $model = new QuotationItemMaster('search');
        $model->unsetAttributes();  // clear any default values
        $result = '';
        $error = '';
        $images_path = realpath(Yii::app()->basePath . '/../uploads/image');
       if (isset($_POST)) {
            $worktypemaster = isset($_POST['QuotationItemMaster']['work_type_id']) ? $_POST['QuotationItemMaster']['work_type_id'] : '';
            $worktype_data = QuotationWorktype::model()->findByPk($worktypemaster);
            $template_id = $worktype_data->template_id;
            $image_name = isset($_FILES['QuotationItemMaster']['name']['image'])?$_FILES['QuotationItemMaster']['name']['image']:'';
            $uploadedFile = CUploadedFile::getInstance($model, 'image');
            if($uploadedFile){
                 $image_name = strtotime("now").'p'.'.'.$uploadedFile->getExtensionName();
            }
            $transaction = Yii::app()->db->beginTransaction();
            try {
                if (1 == $template_id) {

                    $item_count = count($_POST["worktype_label"]);
                    $data_array = array();
                    for ($count = 0; $count < $item_count; $count++) {
                        $shuttermateraial = '';
                        if (isset($_POST['shutter_material'][$count])) {
                            $shuttermateraial = implode(",", $_POST['shutter_material'][$count]);
                        }
                        $carcassmateraial = '';
                        if (isset($_POST['caracoss_material'][$count])) {
                            $carcassmateraial = implode(",", $_POST['caracoss_material'][$count]);
                        }
                        $input_array = array(
                            'quotation_category_id' => $_POST['QuotationItemMaster']['quotation_category_id'],
                            'work_type_id' => $_POST['QuotationItemMaster']['work_type_id'],
                            'worktype_label' => $_POST['worktype_label'][$count],
                            'work_type_description' => 'shuttercarcass',
                            'shutter_material_id' => $shuttermateraial,
                            'shutter_finish_id' => $_POST['shutter_finish'][$count],
                            'carcass_material_id' => $carcassmateraial,
                            'carcass_finish_id' => $_POST['caracoss_finish'][$count],
                            'shutterwork_description' => $_POST['shutterwork_description'][$count],
                            'caracoss_description' => $_POST['caracoss_description'][$count],
                            'rate' => $_POST['rate'][$count],
                            'profit' => $_POST['profit'][$count],
                            'discount' => $_POST['discount'][$count],
                            'image' => $image_name,
                            'created_date' => date('Y-m-d H:i:s'),
                            'created_by' => Yii::app()->user->id
                        );
                        array_push($data_array, $input_array);
                    }
                    $checking = Yii::app()->db->getCommandBuilder()->
                                            createMultipleInsertCommand('jp_quotation_item_master', $data_array)->execute();
                    if($_FILES['QuotationItemMaster']['name']['image']){
                        $file_upload =  $uploadedFile->saveAs($images_path . '/' .$image_name);
                        if(!$file_upload){
                            $result = '0';
                            throw new Exception('Error while file Upload');
                        }
                    }
                    if ($checking) {
                        $result = '1';
                    } else {
                        $result = '0';
                    }
                } else if (3 == $template_id) {

                    $item_count = count($_POST["worktype_label_additional"]);
                    $data_array = array();
                    for ($count = 0; $count < $item_count; $count++) {
                        $material = '';
                        if (isset($_POST['material'][$count])) {
                            $material = implode(",", $_POST['material'][$count]);
                        }
                    
                        $image_name = isset($_FILES['QuotationItemMaster']['name']['additional_image'][$count])?$_FILES['QuotationItemMaster']['name']['additional_image'][$count]:'';
                        $uploadedFile = CUploadedFile::getInstance($model, 'additional_image['.$count.']');
                        if($uploadedFile){
                            $image_name = strtotime("now").'p'.$count.'.'.$uploadedFile->getExtensionName();
                        }
                        $input_array = array(
                            'quotation_category_id' => $_POST['QuotationItemMaster']['quotation_category_id'],
                            'work_type_id' => $_POST['QuotationItemMaster']['work_type_id'],
                            'worktype_label' => $_POST['worktype_label_additional'][$count],
                            'material_ids' => $material,
                            'finish_id' => $_POST['finish'][$count],
                            'work_type_description' => 'additional',
                            'description' => $_POST['additional_description'][$count],
                            'rate' => $_POST['additional_rate'][$count],
                            'profit' => $_POST['additional_profit'][$count],
                            'discount' => $_POST['additional_discount'][$count],
                            'additional_image' => $image_name,
                            'created_date' => date('Y-m-d H:i:s'),
                            'created_by' => Yii::app()->user->id
                        );
                        array_push($data_array, $input_array);

                        $images_path = realpath(Yii::app()->basePath . '/../uploads');
                        if($_FILES['QuotationItemMaster']['name']['image']){
                            $file_upload =  $uploadedFile->saveAs($images_path . '/' .$image_name);
                            if(!$file_upload){
                                $result = '0';
                                throw new Exception('Error while file Upload');
                            }
                        }
                    }
                    $checking = Yii::app()->db->getCommandBuilder()->
                                    createMultipleInsertCommand('jp_quotation_item_master', $data_array)->execute();
                    if ($checking) {
                        $result = '1';
                    } else {
                        $result = '0';
                    }
                } else {
                    $item_count = count($_POST["worktype_label_other"]);
                    $data_array = array();
                    for ($count = 0; $count < $item_count; $count++) {
                        $input_array = array(
                            'quotation_category_id' => $_POST['QuotationItemMaster']['quotation_category_id'],
                            'work_type_id' => $_POST['QuotationItemMaster']['work_type_id'],
                            'worktype_label' => $_POST['worktype_label_other'][$count],
                            'work_type_description' => 'other',
                            'description' => $_POST['description'][$count],
                            'rate' => $_POST['other_rate'][$count],
                            'profit' => $_POST['other_profit'][$count],
                            'discount' => $_POST['other_discount'][$count],
                            'image' => $image_name,
                            'created_date' => date('Y-m-d H:i:s'),
                            'created_by' => Yii::app()->user->id
                        );
                        array_push($data_array, $input_array);
                    }

                    $checking = Yii::app()->db->getCommandBuilder()->
                                    createMultipleInsertCommand('jp_quotation_item_master', $data_array)->execute();
                    $images_path = realpath(Yii::app()->basePath . '/../uploads');
                    if($_FILES['QuotationItemMaster']['name']['image']){
                        $file_upload =  $uploadedFile->saveAs($images_path . '/' .$image_name);
                        if(!$file_upload){
                            $result = '0';
                        throw new Exception('Error while file Upload');
                        }
                    }
                    if ($checking) {
                        $result = '1';
                    } else {
                        $result = '0';
                    }
                }
                $transaction->commit();
            } catch (Exception $error) {
                $transaction->rollBack();
                $result = 0;
                $error = $error->getMessage();
            } finally {
                if ($result == 1) {
                    Yii::app()->user->setFlash('success', "Item added successfully");
                    $this->redirect(array('quotationItemMaster/index'));
                } else {
                    Yii::app()->user->setFlash('error', $error);
                    $this->redirect(array('QuotationItemMaster/create'));
                }
            }
        }
    }

    public function actioncheckduplicate() {

        $result = '';
        if (isset($_POST)) {

            $label = $_POST['name'];
            $labeldata = QuotationItemMaster::model()->findAll(
                    array("condition" => "worktype_label = '$label'"));
            $count = count($labeldata);
            echo $count;
        }
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);
        $images_path = realpath(Yii::app()->basePath . '/../uploads/image');
        if (isset($_POST['QuotationItemMaster'])) {
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $model->attributes = $_POST['QuotationItemMaster'];
                $shutter_material_id = isset($_POST['QuotationItemMaster']['shutter_material_id']) ? $_POST['QuotationItemMaster']['shutter_material_id'] : '';
                $carcass_material_id = isset($_POST['QuotationItemMaster']['carcass_material_id']) ? $_POST['QuotationItemMaster']['carcass_material_id'] : '';
                $discount = isset($_POST['QuotationItemMaster']['discount']) ? $_POST['QuotationItemMaster']['discount'] : '';
                $material_ids = isset($_POST['QuotationItemMaster']['material_ids']) ? $_POST['QuotationItemMaster']['material_ids'] : '';
                $shutterwork_description = isset($_POST['QuotationItemMaster']['shutterwork_description']) ? $_POST['QuotationItemMaster']['shutterwork_description'] : '';
                $caracoss_description = isset($_POST['QuotationItemMaster']['caracoss_description']) ? $_POST['QuotationItemMaster']['caracoss_description'] : '';
                if ($shutter_material_id) {
                    $model->shutter_material_id = implode(',', $shutter_material_id);
                }
                if ($carcass_material_id) {
                    $model->carcass_material_id = implode(',', $carcass_material_id);
                }
                if ($material_ids) {
                    $model->material_ids = implode(',', $material_ids);
                }
                $model->shutterwork_description = $shutterwork_description;
                $model->caracoss_description = $caracoss_description;
                $model->discount = $discount;
                $model->updated_by = Yii::app()->user->id;
                $model->updated_date = date('Y-m-d H:i:s');
                $image_name = isset($_FILES['QuotationItemMaster']['name']['image'])?$_FILES['QuotationItemMaster']['name']['image']:'';
                $uploadedFile = CUploadedFile::getInstance($model, 'image');
                if($uploadedFile){
                    $image_name = strtotime("now").'p'.'.'.$uploadedFile->getExtensionName();
                }
                if($_FILES['QuotationItemMaster']['name']['image']){
                    $file_upload =  $uploadedFile->saveAs($images_path . '/' .$image_name);
                    if(!$file_upload){
                        $result = '0';
                        throw new Exception('Error while file Upload');
                    }
                }
                if($image_name !='' || (isset($_POST['image_remove'])&& $_POST['image_remove']==1)){
                               $model->image = $image_name;
                }
            if (!$model->save()) {
                    throw new Exception(json_encode($model->getErrors()));
                }
                $transaction->commit();
            } catch (Exception $e) {
                $transaction->rollBack();
                $error = $e->getMessage();
                Yii::app()->user->setFlash('error', "Something went wrong.$error");
                $this->redirect(array('quotationItemMaster/index'));
            } finally {
                Yii::app()->user->setFlash('success', "Item Updated successfully");
                $this->redirect(array('quotationItemMaster/index'));
            }
        }
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return QuotationItemMaster the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = QuotationItemMaster::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param QuotationItemMaster $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'quotation-item-master-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function getItem() {
        $items = Yii::app()->controller->GetItemCategory();
        $itemOptions = '';
        foreach ($items as $id => $value) {
            $name = str_replace("'", "", $value["data"]);
            $itemOptions .= CHtml::tag('option', array('value' => $value["id"]), $name, true);
        }
        return $itemOptions;
    }

    public function getItemFinish() {
        $items = QuotationFinishMaster::model()->findAll();
        $itemFinishOptions = '';
        foreach ($items as $id => $value) {
            $name = str_replace("'", "", $value["name"]);
            $itemFinishOptions .= CHtml::tag('option', array('value' => $value["id"]), $name, true);
        }
        return $itemFinishOptions;
    }

    public function actionRemoveitem() {
        $data = $_REQUEST['data'];
        $id = $data['item_id'];
        $model = $this->loadModel($id);
        $transaction = Yii::app()->db->beginTransaction();
        try {
            if (!$model->delete()) {
                $success_status = 0;
                throw new Exception(json_encode($model->getErrors()));
            } else {
                $success_status = 1;
            }
            $transaction->commit();
        } catch (Exception $error) {
            $transaction->rollBack();
            $success_status = 0;
        } finally {
            if ($success_status == 1) {
                echo json_encode(array('response' => 'success', 'msg' => 'Item Deleted Successfully'));
            } else {
                if ($error->errorInfo[1] == 1451) {
                    echo json_encode(array('response' => 'warning', 'msg' => 'Cannot Delete ! This record alredy in use.'));
                } else {
                    echo json_encode(array('response' => 'error', 'msg' => 'Some Error Occured'));
                }
            }
        }
    }

    public function actionUpdateQuotationItem() {
        $data = $_REQUEST['data'];
        $id = $data['item_id'];
        $model = $this->loadModel($id);
        if ($model->shutter_material_id !== array()) {
            $model->shutter_material_id = explode(',', $model->shutter_material_id);
        }
        if ($model->carcass_material_id !== array()) {
            $model->carcass_material_id = explode(',', $model->carcass_material_id);
        }
        if ($model->material_ids !== array()) {
            $model->material_ids = explode(',', $model->material_ids);
        }
        echo $this->renderPartial('_update_item', array(
            'model' => $model,
        ));
    }
    
    public function actionGetWorktypeLayout() {
        $worktype_id  = isset($_POST['worktypeid'])?$_POST['worktypeid']:'';
        $worktype_data = QuotationWorktype::model()->findByPk($worktype_id);
        $template_id =   $worktype_data->template_id;
        echo $template_id;
    }

    public function actionrateindex(){
        $model = new QuotationItemRateMaster;
        $request_id =  isset($_REQUEST['id'])?$_REQUEST['id']:'';
        if(isset($_REQUEST['id'])){
            $model = QuotationItemRateMaster::model()->findByPk($_REQUEST['id']);
        }else{
            if(isset($_POST['QuotationItemRateMaster'])){  
                $category = $_POST['QuotationItemRateMaster']['master_category_id'];
                $material = $_POST['QuotationItemRateMaster']['material_id'];
                $sql = "SELECT * FROM `jp_quotation_item_rate_master` "
                    . " WHERE `master_category_id` = $category "
                    . " AND `material_id` = $material";
                $check_exist = Yii::app()->db->createCommand($sql)->queryRow();
                if($check_exist){
                    Yii::app()->user->setFlash('error','Rate already added!');
                    $this->redirect(array('quotationItemMaster/rateindex'));
                    exit;
                }
            }
        }
        if(isset($_POST['QuotationItemRateMaster'])){
            
            $model->attributes = $_POST['QuotationItemRateMaster'];
            $model->created_by = Yii::app()->user->id;
            $model->created_date = date('Y-m-d H:i:s');
            if($model->save()){
                if($request_id){
                    Yii::app()->user->setFlash('success','Rate updated successfully!');
                }else{
                    Yii::app()->user->setFlash('success','Rate added successfully!');
                }
                $sql ="UPDATE `jp_quotation_item_master` "
                . " SET `rate`='$model->rate'  "
                . " WHERE `quotation_category_id` = $model->master_category_id "
                . " AND ( FIND_IN_SET($model->material_id,shutter_material_id)  "
                . " OR FIND_IN_SET($model->material_id,material_ids))"; 
                           
                $itemMater = Yii::app()->db->createCommand($sql)->execute();                
                $this->redirect(array('quotationItemMaster/rateindex'));
            }
        }
        
        $this->render('rateindex', array(
            'model' => $model, 
            'dataProvider'=>$model->search(),           
        ));
    }

    public function actiongetmaterialRate(){
        $material = $_POST['material'];
        $category = $_POST['category'];

        $sql = "SELECT rate FROM `jp_quotation_item_rate_master` "
            . " WHERE `master_category_id` = ".$category
            . " AND `material_id` =".$material;
        $rate = Yii::app()->db->createCommand($sql)->queryScalar();

        echo $rate; 

    }

    public function actionremoveRateMaster(){
        $id = $_POST['id'];        
        $model = QuotationItemRateMaster::model()->findByPk($id);

        $sql ="SELECT COUNT(*) FROM `jp_quotation_item_master` "                
                . " WHERE `quotation_category_id` = $model->master_category_id "
                . " AND ( FIND_IN_SET($model->material_id,shutter_material_id)  "
                . " OR FIND_IN_SET($model->material_id,material_ids))";
        $exist_item_count = Yii::app()->db->createCommand($sql)->queryScalar();
        
        if($exist_item_count==0){            
            if($model->delete()){
                Yii::app()->user->setFlash('success', "Deleted successfully");                
            }else{
                Yii::app()->user->setFlash('error', "Some error occured");                
            }
        }else{
            Yii::app()->user->setFlash('error', "Cannot Delete! Already in use");            
        }
        
    }

}
